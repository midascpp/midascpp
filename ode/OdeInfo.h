/**
************************************************************************
* 
* @file                 OdeInfo.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Info for OdeIntegrator
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEINFO_H_INCLUDED
#define ODEINFO_H_INCLUDED

#include <map>
#include <set>
#include "libmda/util/any_type.h"
#include "util/Error.h"

namespace midas::ode
{

/**
 *
 **/
class OdeInfo
   :  public std::map<std::string, libmda::util::any_type>
{
   public:
      using Set = std::set<OdeInfo>;

      //! Get function
      template
         <  typename T
         >
      const T& get
         (  const std::string& aKey
         )  const
      {
         try
         {
            return this->at(aKey).template get<T>();
         }
         catch (  const std::out_of_range&
               )
         {
            MIDASERROR("OdeInfo: Could not find '" + aKey + "'.");
            return this->at(aKey).template get<T>();
         }
      }

};


} /* namespace midas::ode */

#endif /* ODEINFO_H_INCLUDED */
