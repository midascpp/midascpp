/**
************************************************************************
* 
* @file                 OdeWrapper.h
* 
* Created:              22-10-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Wrapper around ODE template parameter
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEWRAPPER_H_INCLUDED
#define ODEWRAPPER_H_INCLUDED

#include "ode/ODE_Macros.h"
#include "util/IsDetected.h"
#include "util/SanityCheck.h"
#include "inc_gen/Warnings.h"


namespace midas::ode
{

/**
 * Wrapper around ODE which includes default implementations for all non-vital methods and keeps track of the number of derivative evaluations.
 **/
template
   <  typename ODE
   >
class OdeWrapper
{
   public:
      //! Aliases
      ODE_ALIAS(ODE);

      template
         <  typename ODE_T
         ,  typename... Args
         >
      using has_derivative_type = decltype(std::declval<ODE_T>().Derivative(std::declval<Args>()...));

      template
         <  typename ODE_T
         ,  typename... Args
         >
      using has_process_type = decltype(std::declval<ODE_T>().ProcessNewVector(std::declval<Args>()...));

      template
         <  typename ODE_T
         ,  typename... Args
         >
      using has_accepted_type = decltype(std::declval<ODE_T>().AcceptedStep(std::declval<Args>()...));

      template
         <  typename ODE_T
         ,  typename... Args
         >
      using has_interpolated_type = decltype(std::declval<ODE_T>().InterpolatedPoint(std::declval<Args>()...));

      template
         <  typename ODE_T
         ,  typename... Args
         >
      using has_shapedzero_type = decltype(std::declval<ODE_T>().ShapedZeroVector(std::declval<Args>()...));

      template
         <  typename VEC_T
         >
      using has_sanity_check_type = decltype(midas::util::ContainerSanityCheck(std::declval<VEC_T>()));

   private:
      //! Pointer to ODE
      ODE* mpOde = nullptr;

      //! Number of derivative evaluations
      size_t mNDeriv = 0;

      //! Number of steps not accepted in WRAP_ProcessNewVector
      size_t mNFailedSteps = 0;

   public:
      //! c-tor
      OdeWrapper
         (  ODE* apOde
         )
         :  mpOde
               (  apOde
               )
      {
         if (  !mpOde
            )
         {
            MIDASERROR("OdeWrapper: mpOde initialized to nullptr!");
         }
      }

      /**
       * WRAP derivative
       * This method must be implemented by the ODE class!
       *
       * @param aT      t
       * @param aY      y(t)
       *
       * @return        dy/dt
       **/
      vec_t WRAP_Derivative
         (  step_t aT
         ,  const vec_t& aY
         )
      {
         constexpr bool is_impl = midas::util::IsDetectedV<has_derivative_type, ODE, step_t, const vec_t&>;
         using ret_t = midas::util::DetectedT<has_derivative_type, ODE, step_t, const vec_t&>;
         constexpr bool correct_ret = std::is_same_v<vec_t, ret_t>;

         static_assert(is_impl, "ODE must implement Derivative(step_t, const vec_t&)!");
         static_assert(correct_ret, "ODE::Derivative must return vec_t.");

         ++mNDeriv;
         return mpOde->Derivative(aT, aY);
      }

      /**
       * WRAP Shaped zero vector
       * This method must be implemented by the ODE class!
       *
       * @return     Vector of zeros with correct shape
       **/
      vec_t WRAP_ShapedZeroVector
         (
         )  const
      {
         constexpr bool is_impl = midas::util::IsDetectedV<has_shapedzero_type, ODE>;
         using ret_t = midas::util::DetectedT<has_shapedzero_type, ODE>;
         constexpr bool correct_ret = std::is_same_v<vec_t, ret_t>;

         static_assert(is_impl, "ODE must implement ShapedZeroVector()!");
         static_assert(correct_ret, "ODE::ShapedZeroVector must return vec_t.");

         return mpOde->ShapedZeroVector();
      }


      /**
       * WRAP ProcessNewVector
       * This method can be implemented in two different ways in ODE, and there is also a default implementation provided here.
       *
       * @param arY           y(t_n)
       * @param aYOld         y(t_{n-1})
       * @param arT           t_n
       * @param aTOld         t_{n-1}
       * @param arDyDt        y'(t_n)
       * @param aDyDtOld      y'(t_{n-1})
       * @param arStep        Used step length. May be modified!
       * @param arNDerivFail  Number of subsequent times the derivative evaluation has failed (throwing an ode::bad_derivative).
       *
       * @return              Accept the step?
       **/
      bool WRAP_ProcessNewVector
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& arT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )
      {
         constexpr bool impl_long = midas::util::IsDetectedV<has_process_type, ODE, vec_t&, const vec_t&, step_t&, step_t, vec_t&, const vec_t&, step_t&, size_t&>;
         constexpr bool impl_short = midas::util::IsDetectedV<has_process_type, ODE, vec_t&>;

         static_assert(!(impl_long && impl_short), "Both long and short implementation of ProcessNewVector provided. Please choose one!");

         bool accept = false;

         // ODE can implement ProcessNewVector in two different ways. Long and short version.
         // Long version takes all the arguments. It is up to the user to define the behavior completely.
         if constexpr   (  impl_long
                        )
         {
            auto step_before = arStep;
            accept = mpOde->ProcessNewVector(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
         }
         // Short version just takes the vector and does something to it (normalizes or whatever).
         // (Additional) sanity check and reset is implemented here. 
         else if constexpr (  impl_short
                           )
         {
            if (  arNDerivFail > 0
               )
            {
               MIDASERROR("OdeWrapper: Failing derivatives cannot be handled in 'short' implementation of ProcessNewVector!");
            }

            // Call implementation
            accept = mpOde->ProcessNewVector(arY);

            // If ContainerSanityCheck is implemented, do this.
            if constexpr   (  midas::util::IsDetectedV<has_sanity_check_type, const vec_t&>
                           )
            {
               accept = accept && midas::util::ContainerSanityCheck(arY);
            }

            // If not accepted, reset and reduce step length.
            if (  !accept
               )
            {
               MidasWarning("OdeWrapper: Processed vector not accepted in 'short' implementation. Reset and reduce step length.");

               arY = aYOld;
               arT = aTOld;
               arDyDt = aDyDtOld;
               arStep /= static_cast<step_t>(2.);
            }
         }
         else
         {
            MidasWarning("ODE: '" + std::string(typeid(ODE).name()) + "' does not implement ProcessNewVector. Use default behavior!");

            if (  arNDerivFail > 0
               )
            {
               MIDASERROR("OdeWrapper: Failing derivatives cannot be handled in default implementation of ProcessNewVector!");
            }

            // If the container has a sanity check, do this.
            if constexpr   (  midas::util::IsDetectedV<has_sanity_check_type, const vec_t&>
                           )
            {
               accept = midas::util::ContainerSanityCheck(arY);

               // If not accepted, reset and reduce step length.
               if (  !accept
                  )
               {
                  MidasWarning("OdeWrapper: Processed vector not accepted in default implementation. Reset and reduce step length.");

                  arY = aYOld;
                  arT = aTOld;
                  arDyDt = aDyDtOld;
                  arStep /= static_cast<step_t>(2.);
               }
            }
            // Else, we just accept the step.
            else
            {
               MidasWarning("Vector type: '" + std::string(typeid(vec_t).name()) + "' has no sanity check. ProcessNewVector will just return true...");
               accept = true;
            }
         }

         // Save statistics for failed steps
         if (  !accept
            )
         {
            ++this->mNFailedSteps;
         }

         // Return accept
         return accept;
      }

      /**
       * WRAP accepted step, i.e. save properties etc. for an accepted step.
       * If not implemented by ODE, this function does nothing.
       *
       * @param aTime      t
       * @param aY         y(t)
       * @return
       *    True if the calculation should stop after this step.
       **/
      bool WRAP_AcceptedStep
         (  step_t aTime
         ,  const vec_t& aY
         )
      {
         constexpr bool is_impl = midas::util::IsDetectedV<has_accepted_type, ODE, step_t, const vec_t&>;
         if constexpr   (  is_impl
                        )
         {
            using ret_t = midas::util::DetectedT<has_accepted_type, ODE, step_t, const vec_t&>;
            constexpr bool correct_ret = std::is_same_v<bool, ret_t>;
            static_assert(correct_ret, "AcceptedStep must return bool");

            return mpOde->AcceptedStep(aTime, aY);
         }
         else
         {
            return false;
         }
      }

      /**
       * WRAP interpolated point, i.e. save properties etc. for an interpolated point.
       * If not implemented by ODE, this function does nothing.
       *
       * @param aTime      t
       * @param aY         y(t)
       **/
      void WRAP_InterpolatedPoint
         (  step_t aTime
         ,  const vec_t& aY
         )
      {
         constexpr bool is_impl = midas::util::IsDetectedV<has_interpolated_type, ODE, step_t, const vec_t&>;

         if constexpr   (  is_impl
                        )
         {
            return mpOde->InterpolatedPoint(aTime, aY);
         }
      }

      //! Get number of derivative calculations
      size_t NDeriv
         (
         )  const
      {
         return this->mNDeriv;
      }

      //! Get number of failed steps (not accepted in WRAP_ProcessNewVector)
      size_t NFailedSteps
         (
         )  const
      {
         return this->mNFailedSteps;
      }
};

} /* namespace midas::ode */

#endif /* ODEWRAPPER_H_INCLUDED */
