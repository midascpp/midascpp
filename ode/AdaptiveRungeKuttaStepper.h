/**
************************************************************************
* 
* @file                 AdaptiveRungeKuttaStepper.h
* 
* Created:              19-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Header for AdaptiveRungeKuttaStepper class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ADAPTIVERUNGEKUTTASTEPPER_H_INCLUDED
#define ADAPTIVERUNGEKUTTASTEPPER_H_INCLUDED

// Include declaration
#include "ode/AdaptiveRungeKuttaStepper_Decl.h"

// Include implementation
#include "ode/AdaptiveRungeKuttaStepper_Impl.h"

#endif /* ADAPTIVERUNGEKUTTASTEPPER_H_INCLUDED */

