
/**
************************************************************************
* 
* @file                 Tsit5Stepper.h
* 
* Created:              01-05-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Header for Tsit5Stepper class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TSIT5STEPPER_H_INCLUDED
#define TSIT5STEPPER_H_INCLUDED

// Include declaration
#include "ode/Tsit5Stepper_Decl.h"

// Include implementation
#include "ode/Tsit5Stepper_Impl.h"

#endif /* TSIT5STEPPER_H_INCLUDED */

