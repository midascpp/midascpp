/**
************************************************************************
* 
* @file                 MidasOdeDriver_Impl.h
* 
* Created:              28-03-2018 (using code from 28-09-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    MidasOdeDriver class and function declarations
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASODEDRIVER_IMPL_H_INCLUDED
#define MIDASODEDRIVER_IMPL_H_INCLUDED

#include "ode/MidasOdeDriver_Decl.h"
#include "ode/ode_exceptions.h"

#include "ode/ODE.h"

/**
 * Constructor from ODE and info
 *
 * @param apOde      ODE pointer
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
MidasOdeDriver<ODE>::MidasOdeDriver
   (  ODE* apOde
   ,  const midas::ode::OdeInfo& aInfo
   )
   :  OdeDriverBase<ODE>
         (  apOde
         ,  aInfo
         )
   ,  mMaxFailedSteps
         (  aInfo.template get<In>("MIDASODEDRIVERMAXFAILEDSTEPS")
         )
{
}

/**
 * Overload of type
 *
 * @return Type
 **/
template
   <  typename ODE
   >
std::string MidasOdeDriver<ODE>::TypeImpl
   (
   )  const
{
   return std::string("MidasOdeDriver");
}

/**
 *
 **/
template
   <  typename ODE
   >
void MidasOdeDriver<ODE>::IntegrateImpl
   (  vec_t& arY
   )
{
   // Misc. initializations
   const Nb eps = std::numeric_limits<Nb>::epsilon();
   
   // Initialize times
   const step_t t0 = this->mTimeInterval.first;
   const step_t t1 = this->mTimeInterval.second;
   const step_t interval = t1 - t0;
   step_t t = t0;

   // Output header
   if (  this->mIoLevel > I_0
      )
   {
      Mout  << " ===== Starting MidasOdeDriver ===== \n"
            << "    ODE:      " << std::string(typeid(ODE).name()) << "\n"
            << "    Stepper:  " << this->mStepper->Type() << "\n"
            << "    t0:       " << t0 << "\n"
            << "    t1:       " << t1 << "\n"
            << std::endl;
   }
   if (  this->mIoLevel > I_10
      )
   {
      Mout  << "    Initial y vector:\n" << arY << "\n"
            << "    Calculating dy/dt at initial point." << std::endl;
   }
   // Initialize and calculate first derivative
   auto dydt = this->mOde.WRAP_Derivative(t, arY);
   if (  this->mIoLevel > I_10
      )
   {
      Mout  << "    Initial dy/dt vector:\n" << dydt << "\n"
            << "    Call WRAP_AcceptedStep for initial point." << std::endl;
   }
   // save data for this point
   bool force_stop = this->mOde.WRAP_AcceptedStep(t, arY);

   if (  force_stop
      )
   {
      MidasWarning("MidasOdeDriver stopping before integration has begun. Is this an error?");
      return;
   }

   // Save time and vectors
   auto t_old = t;
   auto y_old = arY;
   auto dydt_old = dydt;

   // Save initial point (last arg does not matter)
   Data::ClearDatabase();
   Data::Save(t, arY, this->mStepper, t, this->mOde);

   // Set initial step size
   step_t stepsize   =  this->mImprovedInitialStepSize
                     ?  this->ImprovedInitialStepSize(t, arY, dydt, this->mStepper->AbsTol(), this->mStepper->RelTol(), this->mStepper->Order())
                     :  this->mInitialStepSize;
   step_t err(-1.);

   // Save initial stepsize
   auto init_stepsize = stepsize;

   if (  this->mIoLevel >= I_5
      )
   {
      Mout  << "    Initial step size = " << init_stepsize << std::endl;
   }

   /***************************************************************
    * Main loop
    **************************************************************/
   In istep = I_0;
   auto interval_pct = interval / C_100;
   step_t t_dot_out = 0;
   In ndots = I_1;
   while (  true
         )
   {
      if (  this->mIoLevel >= I_5
         )
      {
         Mout  << " === Step " << istep << " ===\n"
               << "    t:                 " << t << "\n"
               << std::flush;
      }
      else if  (  this->mIoLevel > I_0
               && t >= t_dot_out
               )
      {
         Mout  << ".";
         t_dot_out = interval_pct * (ndots++);
      }

      // If stepsize overshoots: decrease so we hit end limit
      if (  libmda::numeric::float_gt(t+stepsize, t1)
         )
      {
         stepsize = t1 - t;

         if (  this->mIoLevel > I_7
            )
         {
            Mout  << " MidasOdeDriver: Step size overshoots end time. Reduce to h = " << stepsize << std::endl;
         }
      }

      // Try steps until the new vector is accepted
      t_old = t;
      y_old = arY;
      dydt_old = dydt;
      size_t nfail = 0;
      size_t n_deriv_fail = 0;
      while (  true
            )
      {
         // Get distance to next output point in OdeDatabase
         auto dist_to_out  =  Data::DenseOutput()
                           ?  Data::NextT() - t   // "next output point" - "current time"
                           :  step_t(-1.);

         bool deriv_success = true;

         // Take step (this should also call WRAP_AcceptedStep)
         try
         {
            err = this->mStepper->Step(t, stepsize, arY, dydt, this->mOde, dist_to_out, force_stop);
         }
         catch (  const midas::ode::bad_derivative& err
               )
         {
            if (  this->mIoLevel >= I_5
               )
            {
               Mout  << " MidasOdeDriver: Bad derivative calculation in stepper!\n"
                     << "    Message: " << std::string(err.what()) << "\n"
                     << std::endl;
            }

            ++n_deriv_fail;
            deriv_success = false;
         }

         if (  deriv_success
            )
         {
            n_deriv_fail = 0;
         }
   
         if (  this->mIoLevel >= I_5
            && deriv_success
            && !this->mStepper->FixedStep()
            )
         {
            Mout  << "    Tried step size:   " << stepsize << "\n"
                  << "    Used step size:    " << this->mStepper->UsedStepSize() << "\n"
                  << std::flush;
         }

         // Process updated vector (normalize, etc.)
         // If the vector is not accepted, stepsize is modified and the vectors are reset to redo the previous step.
         if (  this->mOde.WRAP_ProcessNewVector(arY, y_old, t, t_old, dydt, dydt_old, stepsize, n_deriv_fail)
            )
         {
            nfail = 0;
            break;
         }
         else
         {
            if (  this->mIoLevel >= I_5
               )
            {
               Mout  << " Step failed in ODE::ProcessNewVector!" << std::endl;
            }
            ++nfail;
         }

         if (  nfail >= this->mMaxFailedSteps
            )
         {
            MIDASERROR("MidasOdeDriver: Could not perform successful step in " + std::to_string(nfail) + " tries! Try a smaller initial step size or adjusting the tolerance (1.0e-4 to 1.0-8 is usually fine).");
         }
      }

      if (  this->mIoLevel >= I_10
         )
      {
         Mout  << "    New y vector:\n" << arY << std::endl;
      }

      // Save data
      Data::Save(t, arY, this->mStepper, stepsize, this->mOde);

      // Save step info
      this->SaveStep(stepsize, err);

      // Check if we are done
      auto next_step = this->mStepper->FixedStep() ? init_stepsize : this->mStepper->NextStepSize();
      bool max_steps_reached = (this->mMaxSteps > I_0) && (istep >= this->mMaxSteps);
      bool done = libmda::numeric::float_geq(t, t1);
      bool stepsize_underflow = std::abs(next_step) <= this->mMinStepSize;
      if (  done
         || force_stop
         || max_steps_reached
         || stepsize_underflow
         )
      {
         // Set number of steps (istep starts from zero, so we add 1 to get the number of steps performed).
         this->mNSteps = istep + 1;

         //
         if (  force_stop
            && this->mIoLevel >= I_5
            )
         {
            Mout  << " Integration stopped by ODE::AcceptedStep." << std::endl;
         }

         //
         if (  max_steps_reached
            )
         {
            MidasWarning("MidasOdeDriver: Maximum steps reached!");
         }

         //
         if (  stepsize_underflow
            )
         {
            MidasWarning("MidasOdeDriver: Stepsize too small. I stop here!");
         }

         // Return
         return;
      }

      // Update step size to guess from stepper if not using fixed step size
      if (  !this->mStepper->FixedStep()
         )
      {
         stepsize = next_step;
      }

      // Output endl if we are outputting the flow
      if (  this->mIoLevel >= I_5
         )
      {
         Mout  << std::endl;
      }

      // Increment istep
      ++istep;
   }

   // End line of dots
   if (  this->mIoLevel < I_5
      && this->mIoLevel > I_0
      )
   {
      Mout  << std::endl;
   }
}

#endif /* MIDASODEDRIVER_IMPL_H_INCLUDED */
