/**
************************************************************************
* 
* @file                 MidasOdeDriver.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Header for MidasOdeDriver class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASODEDRIVER_H_INCLUDED
#define MIDASODEDRIVER_H_INCLUDED

// Include declaration
#include "ode/MidasOdeDriver_Decl.h"

// Include implementation
#include "ode/MidasOdeDriver_Impl.h"

#endif /* MIDASODEDRIVER_H_INCLUDED */

