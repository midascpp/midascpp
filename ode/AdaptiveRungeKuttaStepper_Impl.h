/**
************************************************************************
* 
* @file                 AdaptiveRungeKuttaStepper_Impl.h
* 
* Created:              19-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of AdaptiveRungeKuttaStepper base class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ADAPTIVERUNGEKUTTASTEPPER_IMPL_H_INCLUDED

#include "ode/AdaptiveRungeKuttaStepper_Decl.h"

/**
 * Constructor from OdeInfo
 *
 * @param aInfo
 **/
template
   <  typename ODE
   >
AdaptiveRungeKuttaStepper<ODE>::AdaptiveRungeKuttaStepper
   (  const midas::ode::OdeInfo& aInfo
   )
   :  OdeStepperBase<ODE>(aInfo)
   ,  midas::ode::OdeStepsizeController<typename ODE::step_t>(aInfo)
{
}


/**
 * Implementation of Step
 *
 * @param arT              t parameter
 * @param aTrialStep       Trial step size
 * @param arY              y vector
 * @param arDyDt           dy/dt vector
 * @param arOde            ODE
 * @param aDistanceToOutput Distance to next output point
 * @param arForceStop      Set true of the integration should stop after this accepted step
 *
 * @return
 *    Scaled error of accepted step
 **/
template
   <  typename ODE
   >
typename AdaptiveRungeKuttaStepper<ODE>::step_t AdaptiveRungeKuttaStepper<ODE>::StepImpl
   (  step_t& arT
   ,  step_t aTrialStep
   ,  vec_t& arY
   ,  vec_t& arDyDt
   ,  midas::ode::OdeWrapper<ODE>& arOde
   ,  step_t aDistanceToOutput
   ,  bool& arForceStop
   )
{
   if (  this->mIoLevel >= I_10
      )
   {
      Mout  << " === " << this->Type() << ": Step ===" << std::endl;
   }
   
   // Set size of all intermediate vectors
   this->ResizeVectorsImpl(arY);

   auto tmp_y = arY;

   bool stepsize_reduced = false;

   step_t h = aTrialStep;
   step_t err;
   while (  true
         )
   {
      // Calculate derivatives and return updated y vector
      tmp_y = this->CalculateDerivativesImpl(arT, h, arY, arDyDt, arOde);

      // Calculate error
      // Niels: We also do this for fixed step sizes in order to be able to track the error
      err = this->ErrorEstimateImpl(h, arY, tmp_y);

      if (  this->mIoLevel >= I_10
         )
      {
         Mout  << " h:     " << h << "\n"
               << " Error: " << err << "\n"
               << std::flush;
      }

      if (  std::isnan(h)
         || std::isnan(err)
         )
      {
         MIDASERROR("NaN detected in step-size determination!");
      }

      // If we use a fixed step size, we simply break the loop here.
      if (  this->FixedStep()
         )
      {
         break;
      }

      // Check step size (and update if necessary)
      if (  Control::CheckStepsize(err, h)
         )
      {
         break;
      }
      else
      {
         stepsize_reduced = true;
      }

      if (  libmda::numeric::float_numeq_zero(h, arT)
         )
      {
         MIDASERROR("Stepsize underflow in " + this->Type());
      }
   }

   // Increment number of steps
   if (  stepsize_reduced
      )
   {
      ++this->mNBadSteps;
   }
   else
   {
      ++this->mNGoodSteps;
   }


   // Update time
   this->mTOld = arT;
   arT += (this->mUsedStep = h);

   // Calculate derivative at new point (or get it from FSAL)
   auto tmp_dydt = (this->UseFsal() && this->Fsal()) ? this->GetFsalDerivative() : arOde.WRAP_Derivative(arT, tmp_y);

   // Call AcceptedStep before calculating derivatives in PrepareDenseOutput
   // This is important for TDH and MCTDH as the integrals and energy will only be updated to the latest time if it is done like this...
   arForceStop = arOde.WRAP_AcceptedStep(arT, tmp_y);

   // Prepare dense output.
   // NB: Requires mTOld to be set!
   if (  this->mDenseOutput
      && libmda::numeric::float_pos(aDistanceToOutput)
      && libmda::numeric::float_geq(h, aDistanceToOutput)      // Only prepare dense output, if we will need it
      )
   {
      this->PrepareDenseOutputImpl(h, arY, tmp_y, arDyDt, tmp_dydt, arOde);
   }

   // Update vectors
   arY = std::move(tmp_y);
   arDyDt = std::move(tmp_dydt);

   if (  this->mIoLevel >= I_10
      )
   {
      Mout  << " New y vector:\n" << arY << "\n"
            << " New dy/dt vector:\n" << arDyDt << "\n"
            << std::endl;
   }

   return err;
}

/**
 * Implementation of NextStepSize
 *
 * @return
 *    Next step size from OdeStepsizeController
 **/
template
   <  typename ODE
   >
typename AdaptiveRungeKuttaStepper<ODE>::step_t AdaptiveRungeKuttaStepper<ODE>::NextStepSizeImpl
   (
   )  const
{
   return Control::NextStepSize();
}

/**
 * @return
 *    False
 **/
template
   <  typename ODE
   >
bool AdaptiveRungeKuttaStepper<ODE>::Fsal
   (
   )  const
{
   return false;
}

/**
 * @return
 *    Error
 **/
template
   <  typename ODE
   >
typename AdaptiveRungeKuttaStepper<ODE>::vec_t AdaptiveRungeKuttaStepper<ODE>::GetFsalDerivative
   (
   )  const
{
   MIDASERROR(this->Type() + " does not use FSAL! This should not happen...");
   return vec_t();
}

#define ADAPTIVERUNGEKUTTASTEPPER_IMPL_H_INCLUDED
#endif /* ADAPTIVERUNGEKUTTASTEPPER_IMPL_H_INCLUDED */
