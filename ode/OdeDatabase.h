/**
************************************************************************
* 
* @file                 OdeDatabase.h
* 
* Created:              28-03-2018 (using code from 28-09-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Output class and function declarations
* 
* Last modified:        March 28, 2018
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEDATABASE_H_INCLUDED
#define ODEDATABASE_H_INCLUDED

#include <cassert>
#include <vector>
#include "ode/OdeInfo.h"
#include "ode/OdeStepperBase.h"
#include "ode/ODE_Macros.h"
#include "libmda/numeric/float_eq.h"

namespace midas::ode
{

/**
 * Database for holding information on ODE time steps, etc.
 **/
template
   <  typename ODE
   >
class OdeDatabase
{
   public:
      //! Typedefs
      ODE_ALIAS(ODE);

   private:
      /**
       * Number of points to save
       * 
       * -1          Save all points without interpolation
       *  0 || <-1   Save no points
       *  1          Error. Saving one point makes no sense
       *  >1         Save this number of equidistant points (a must if we want to do FFT on autocorrelation functions, etc.)
       **/
      In mNDenseOutputPoints = -I_1;

      //! IoLevel
      In mIoLevel = I_1;

      //! Time step for interpolation
      step_t mDeltaTOut = static_cast<step_t>(1.e-2);

      //! Save full Y vectors? Else, we call ODE::InterpolatedStep to save properties.
      bool mSaveYVectors = false;

      //! Vector of saved t points
      std::vector<step_t> mT;

      //! Vector of saved y vectors. Empty if mSaveYVectors is false.
      std::vector<vec_t> mY;

   public:
      //! Constructor
      explicit OdeDatabase
         (  const midas::ode::OdeInfo& aInfo
         )
         :  mNDenseOutputPoints
               (  aInfo.template get<std::string>("DRIVER") == "GSL"    // For GSL, the database should save all points given to it. The GslOdeDriver will handle the rest.
               ?  -I_1
               :  aInfo.template get<In>("OUTPUTPOINTS")
               )
         ,  mIoLevel
               (  aInfo.template get<In>("IOLEVEL")
               )
         ,  mSaveYVectors
               (  aInfo.template get<bool>("DATABASESAVEVECTORS")
               )
      {
         
         // Initial storage capacity
         auto init_size =  mNDenseOutputPoints == I_0
                        ?  I_0
                        :  mNDenseOutputPoints > I_0
                           ?  mNDenseOutputPoints
                           :  I_500;

         if (  init_size > I_0
            )
         {
            this->mT.reserve(init_size);

            if (  this->mSaveYVectors
               )
            {
               this->mY.reserve(init_size);
            }
         }
         else
         {
            Mout  << " OdeDatabase: init_size = " << init_size << std::endl;
         }

         // Set time step
         const auto& interval = aInfo.template get<std::pair<Nb,Nb>>("TIMEINTERVAL");

         assert(libmda::numeric::float_gt(interval.second, interval.first));

         if (  this->mNDenseOutputPoints == I_1
            )
         {
            MIDASERROR("For dense output, we need more than one point!");
         }
         else if  (  this->mNDenseOutputPoints > I_1
                  )
         {
            this->mDeltaTOut = (interval.second - interval.first) / (this->mNDenseOutputPoints-I_1);
         }
         else if  (  this->mNDenseOutputPoints == -I_1
                  )
         {
            Mout  << " OdeDatabase: Not using dense output. Saving all points witout interpolation." << std::endl;
         }
         else
         {
            Mout  << " OdeDatabase: Not saving any points!" << std::endl;
         }
      }

      //! Save data point
      void Save
         (  step_t aT
         ,  const vec_t& arY
         ,  const std::unique_ptr<OdeStepperBase<ODE> >& aStepper
         ,  step_t aStepSize
         ,  OdeWrapper<ODE>& arOde
         )
      {
         // Do interpolation if we are using dense output.
         // if first iter, we just save
         if (  this->mNDenseOutputPoints > I_0
            && !mT.empty()
            )
         {
            bool saved_points = false;

            // Start t for interpolation (one dt after latest saved point)
            auto t_out = mDeltaTOut * mT.size() + mT.front();

            while (  libmda::numeric::float_geq(aT, t_out)  // if aT < t_old + dt, we do not save any points
                  )
            {
               saved_points = true;

               if (  mT.size() == mT.capacity()
                  )
               {
                  this->IncrCapacity();
               }

               if (  this->mIoLevel > I_5
                  )
               {
                  Mout  << " OdeDatabase: Save point for t = " << t_out << std::endl;
               }

               // Interpolate
               auto y_interp = aStepper->Interpolate(t_out, aStepSize);
               
               // Add time and perhaps y vector
               mT.emplace_back(t_out);
               arOde.WRAP_InterpolatedPoint(t_out, y_interp);

               if (  this->mSaveYVectors
                  )
               {
                  mY.emplace_back(std::move(y_interp));
               }

               // Incr t_out
               t_out = mDeltaTOut * mT.size() + mT.front();
            }

            if (  this->mIoLevel > I_5
               && saved_points
               )
            {
               Mout  << " OdeDatabase: Stop saving points. Next t_out = " << t_out << ", aT = " << aT << std::endl;
            }
         }
         // Save given point without interpolation (unless we save no points)
         else if  (  this->mNDenseOutputPoints != I_0
                  )
         {
            if (  mT.size() == mT.capacity()
               )
            {
               this->IncrCapacity();
            }

            if (  this->mIoLevel > I_5
               )
            {
               Mout  << " OdeDatabase: Save point for t = " << aT << std::endl;
            }

            mT.emplace_back(aT);
            arOde.WRAP_InterpolatedPoint(aT, arY);

            if (  this->mSaveYVectors
               )
            {
               mY.emplace_back(arY);
            }
         }
      }

      //! Set the reserved capacity to twice the size
      void IncrCapacity
         (
         )
      {
         mT.reserve(mT.size()*2);

         if (  this->mSaveYVectors
            )
         {
            mY.reserve(mY.size()*2);
         }
      }

      //! Latest saved time
      step_t LatestT
         (
         )  const
      {
         return mT.back();
      }

      //! Next output point
      step_t NextT
         (
         )  const
      {
         return mT.front() + mT.size() * this->mDeltaTOut;
      }

      //! Delta T
      step_t DeltaT
         (
         )  const
      {
         return mDeltaTOut;
      }

      //! Dense output?
      bool DenseOutput
         (
         )  const
      {
         return mNDenseOutputPoints > I_0;
      }

      //! Get T
      const std::vector<step_t>& GetT
         (
         )  const
      {
         return mT;
      }

      //! Get Y
      const std::vector<vec_t>& GetY
         (
         )  const
      {
         return mY;
      }

      //! Get mSaveYVectors
      bool SaveYVectors
         (
         )  const
      {
         return this->mSaveYVectors;
      }

      //! Capacity
      size_t Capacity
         (
         )  const
      {
         return mT.capacity();
      }

      //! Clear data
      void ClearDatabase
         (
         )
      {
         mT.clear();
         mY.clear();
      }
};

} /* namespace midas::ode */

#endif /* ODEDATABASE_H_INCLUDED */
