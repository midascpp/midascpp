/**
************************************************************************
* 
* @file                 Tsit5Stepper_Decl.h
* 
* Created:              01-05-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of Tsit5Stepper class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TSIT5STEPPER_DECL_H_INCLUDED
#define TSIT5STEPPER_DECL_H_INCLUDED

#include "ode/AdaptiveRungeKuttaStepper.h"
#include "ode/ODE_Macros.h"

/**
 * The 5(4) explicit Runge-Kutta method of Ch. Tsitouras.
 * Allows for dense output of order 4 without additional function evaluations.
 *
 * Ch. Tsitouras: "Runge-Kutta pairs of orders 5(4) satisfying only the first column simplifying assumption",
 * Computers & Mathematics with Applications, 62(2), 770, (2011)
 *
 * NB: This does not seem to work very well for TDH. Perhaps there is a bug in the implementation!
 **/
template
   <  typename ODE
   >
class Tsit5Stepper
   :  public AdaptiveRungeKuttaStepper<ODE>
{
   public:
      //! Aliases
      using Control = typename AdaptiveRungeKuttaStepper<ODE>::Control;
      using Base = typename AdaptiveRungeKuttaStepper<ODE>::Base;
      ODE_ALIAS(Base);

   private:
      //! Intermediates
      vec_t mK2, mK3, mK4, mK5, mK6, mK7;
      
      //! Error vector
      vec_t mYerr;

      //! Old derivative for interpolation
      vec_t mYOld, mDyDtOld;

      //! Implementation of Interpolate
      vec_t InterpolateImpl
         (  step_t aTime
         ,  step_t aStep
         )  const override;

      //! Implementation of Type
      std::string TypeImpl
         (
         )  const override;

      //! Implementation of Order
      In OrderImpl
         (
         )  const override;

      //! Calculate derivatives and return updated y vector
      vec_t CalculateDerivativesImpl
         (  step_t aT
         ,  step_t aStepSize
         ,  const vec_t& aY
         ,  const vec_t& aDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  override;

      //! Calculate error
      step_t ErrorEstimateImpl
         (  step_t aStep
         ,  const vec_t& aOldY
         ,  const vec_t& aNewY
         )  const override;

      //! Prepare dense output
      void PrepareDenseOutputImpl
         (  step_t aStepSize
         ,  const vec_t& aYOld
         ,  const vec_t& aYNew
         ,  const vec_t& aDyDtOld
         ,  const vec_t& aDyDtNew
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  override;

      //! Resize all vector members
      void ResizeVectorsImpl
         (  const vec_t&
         )  override;

      //! Do we use FSAL?
      bool Fsal
         (
         )  const override;

      //! Get saved FSAL derivative
      vec_t GetFsalDerivative
         (
         )  const override;

   public:
      //! Constructor
      explicit Tsit5Stepper
         (  const midas::ode::OdeInfo& aInfo
         );
};

#endif /* TSIT5STEPPER_DECL_H_INCLUDED */


