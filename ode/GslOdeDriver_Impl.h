/**
************************************************************************
* 
* @file                 GslOdeDriver_Impl.h
* 
* Created:              24-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of GslOdeDriver class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifdef ENABLE_GSL

#ifndef GSLODEDRIVER_IMPL_H_INCLUDED
#define GSLODEDRIVER_IMPL_H_INCLUDED

#include "ode/GslOdeDriver_Decl.h"
#include "ode/ODE.h"


/**
 * Constructor
 * @param apOde      ODE
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
GslOdeDriver<ODE>::GslOdeDriver
   (  ODE* apOde
   ,  const midas::ode::OdeInfo& aInfo
   )
   :  OdeDriverBase<ODE>
         (  apOde
         ,  aInfo
         )
   ,  mAy
         (  aInfo.template get<double>("GSLERRORAY")
         )
   ,  mADyDt
         (  aInfo.template get<double>("GSLERRORADYDT")
         )
{
   // We have only implemented interface to methods that do not require a Jacobian.
   const auto& step_type = aInfo.template get<std::string>("GSLSTEPTYPE");
   if (  step_type == "RK2"
      )
   {
      this->mStepperType = gsl_odeiv2_step_rk2;
      this->mOrder = I_2;
   }
   else if  (  step_type== "RK4"
            )
   {
      this->mStepperType = gsl_odeiv2_step_rk4;
      this->mOrder = I_4;
   }
   else if  (  step_type == "RKF45"
            )
   {
      this->mStepperType = gsl_odeiv2_step_rkf45;
      this->mOrder = I_4;
   }
   else if  (  step_type == "RKCK"
            )
   {
      this->mStepperType = gsl_odeiv2_step_rkck;
      this->mOrder = I_4;
   }
   else if  (  step_type == "RK8PD"
            )
   {
      this->mStepperType = gsl_odeiv2_step_rk8pd;
      this->mOrder = I_8;
   }
   else if  (  step_type == "MSADAMS"
            )
   {
      this->mStepperType = gsl_odeiv2_step_msadams;

      // Order varies between 1 and 12, but anyway, this number is only used in step-size determination
      this->mOrder = I_1;
   }
   else
   {
      MIDASERROR("GSL interface to " + step_type + " not implemented!");
   }
};

/**
 * @return
 *    Type
 **/
template
   <  typename ODE
   >
std::string GslOdeDriver<ODE>::TypeImpl
   (
   )  const
{
   return "GslOdeDriver";
}

/**
 * Integrate
 *
 * @param arY     Initial vector
 **/
template
   <  typename ODE
   >
void GslOdeDriver<ODE>::IntegrateImpl
   (  vec_t& arY
   )
{
   // Set up some sizes
   const auto& time_interval = this->mInfo.template get<std::pair<Nb, Nb> >("TIMEINTERVAL");
   double t0 = time_interval.first;
   double t1 = time_interval.second;
   double timespan = t1 - t0;
   In outpoints = this->mInfo.template get<In>("OUTPUTPOINTS");

   if (  outpoints == I_1
      )
   {
      MIDASERROR("GslOdeDriver: One equidistant output point makes no sense...");
   }
   In n_intervals =  outpoints > I_0
                  ?  outpoints - I_1
                  :  I_1;
   double dt = timespan / n_intervals;

   // It is a bit stupid, but we need this derivative and the GSL routine will then recalculate it...
   auto dydt = this->mOde.WRAP_Derivative(t0, arY);

   // Set accepted step
   bool force_stop = this->mOde.WRAP_AcceptedStep(t0, arY);

   // Save in database
   Data::Save(t0, arY, this->mStepper, C_0, this->mOde);

   // Stop now?
   if (  force_stop
      )
   {
      MidasWarning("GslOdeDriver stopping before integration has begun. Is this an error?");
      return;
   }

   // Transfer vector to double*
   size_t n = this->ComplexOde() ? 2*ODE_Size(arY) : ODE_Size(arY);
   auto y = std::make_unique<double[]>(n);
   ODE_DataToPointer(arY, y.get());

   // Create GSL system
   gsl_odeiv2_system sys = { midas::ode::detail::gsl_derivative_wrapper<ODE>, nullptr, n, &this->mOde };

   // Allocate GSL driver
   double abs = this->mInfo.template get<Nb>("ODESTEPPERABSTOL");
   double rel = this->mInfo.template get<Nb>("ODESTEPPERRELTOL");
   double h = this->mInitialStepSize;
   if (  this->mImprovedInitialStepSize
      )
   {
      h = this->ImprovedInitialStepSize(t0, arY, dydt, abs, rel, this->mOrder);
   }
   auto* driver = gsl_odeiv2_driver_alloc_standard_new(&sys, this->mStepperType, h, abs, rel, this->mAy, this->mADyDt);
   auto gsl_nmax = (this->mMaxSteps == -I_1) ? 0 : this->mMaxSteps;
   gsl_odeiv2_driver_set_nmax(driver, gsl_nmax);
   gsl_odeiv2_driver_set_hmin(driver, this->mMinStepSize);

   // Run the driver (save datapoints)
   // Access stepper, control, and evolve via ->s, ->c, ->e
   if (  this->mIoLevel > I_5
      )
   {
      Mout  << " ===== Starting GslOdeDriver ===== \n"
            << "    ODE:   " << std::string(typeid(ODE).name()) << "\n"
            << "    t0:    " << t0 << "\n"
            << "    t1:    " << t1 << "\n"
            << "    h0:    " << h << "\n"
            << "    n_intervals: " << n_intervals << "\n"
            << "    dt:    " << dt << "\n"
            << std::endl;
   }
   typename ODE::step_t t = t0;
   size_t n_tot_steps = 0;
   for(In iinterval=I_0; iinterval<n_intervals; ++iinterval)
   {
      double ti = dt*(iinterval+I_1);

      // Apply driver
      double tmp_t = t;
      int status = gsl_odeiv2_driver_apply(driver, &tmp_t, ti, y.get());
      t = tmp_t;

      // Check status
      if (  status != GSL_SUCCESS
         )
      {
         MIDASERROR("Failed step in gsl_odeiv2_driver_apply. Error code: " + std::to_string(status));
      }

      n_tot_steps += driver->n;

      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " === N_steps: " << driver->n << " ===\n"
               << "    t:                 " << t << "\n"
               << std::flush;
      }

      // Set arY
      ODE_DataFromPointer(arY, y.get());

      // Process updated vector (normalize, etc.)
      // If the vector is not accepted, we cannot procede...
      size_t zero = 0;
      typename ODE::step_t tmp_h = driver->h;
      bool success = this->mOde.WRAP_ProcessNewVector(arY, arY, t, t, arY, arY, tmp_h, zero); // HACK: we do not have old values for reset...
      driver->h = tmp_h;
      if (  success
         )
      {
         // Set data in y pointer
         ODE_DataToPointer(arY, y.get());
      }
      else
      {
         midas::ode::detail::compare_vector_and_pointer<ODE>(arY, y.get());
         MIDASERROR("GslOdeDriver: y-vector check failed!");
      }
      
      // Set accepted step
      force_stop = this->mOde.WRAP_AcceptedStep(t, arY);

      // Save in database
      Data::Save(t, arY, this->mStepper, driver->h, this->mOde);

      if (  force_stop
         )
      {
         Mout  << " GslOdeDriver: Integration stopped by ODE::AcceptedStep." << std::endl;
         break;
      }
   }

   // Set number of steps taken
   this->mNSteps = n_tot_steps;

   // Free GSL stuff
   gsl_odeiv2_driver_free(driver);

   // Finally, set data in arY
   ODE_DataFromPointer(arY, y.get());
};

#endif /* GSLODEDRIVER_IMPL_H_INCLUDED */

#endif   /* ENABLE_GSL */
