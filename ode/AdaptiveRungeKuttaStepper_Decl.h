/**
************************************************************************
* 
* @file                 AdaptiveRungeKuttaStepper_Decl.h
* 
* Created:              19-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of AdaptiveRungeKuttaStepper base class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ADAPTIVERUNGEKUTTASTEPPER_DECL_H_INCLUDED
#define ADAPTIVERUNGEKUTTASTEPPER_DECL_H_INCLUDED

#include "ode/OdeStepperBase.h"
#include "ode/OdeStepsizeController.h"
#include "ode/ODE_Macros.h"

/**
 * Abstract base class for Runge-Kutta steppers with adaptive step-size control.
 **/
template
   <  typename ODE
   >
class AdaptiveRungeKuttaStepper
   :  public OdeStepperBase<ODE>
   ,  private midas::ode::OdeStepsizeController<typename ODE::step_t>
{
   public:
      //! Typedefs
      using Control = midas::ode::OdeStepsizeController<typename ODE::step_t>;
      using Base = OdeStepperBase<ODE>;
      ODE_ALIAS(Base);


   private:
      //! Implementation of Step
      step_t StepImpl
         (  step_t& arT
         ,  step_t aTrialStep
         ,  vec_t& arY
         ,  vec_t& arDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         ,  step_t aDistanceToOutput
         ,  bool& arForceStop
         )  override;

      //! Implementation of NextStepSize
      step_t NextStepSizeImpl
         (
         )  const override;


      /** @name Pure virtual methods needed by StepImpl */
      //!@{
      //! Calculate derivatives for StepImpl
      virtual vec_t CalculateDerivativesImpl
         (  step_t aT
         ,  step_t aStepSize
         ,  const vec_t& aY
         ,  const vec_t& aDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  = 0;

      //! Calculate error
      virtual step_t ErrorEstimateImpl
         (  step_t aStep
         ,  const vec_t& aOldY
         ,  const vec_t& aNewY
         )  const = 0;

      //! Prepare dense output
      virtual void PrepareDenseOutputImpl
         (  step_t aStepSize
         ,  const vec_t& aYOld
         ,  const vec_t& aYNew
         ,  const vec_t& aDyDtOld
         ,  const vec_t& aDyDtNew
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  = 0;

      //! Resize all vector members
      virtual void ResizeVectorsImpl
         (  const vec_t& aShape
         )  = 0;

      //! Do we use FSAL?
      virtual bool Fsal
         (
         )  const;

      //! Get saved FSAL derivative
      virtual vec_t GetFsalDerivative
         (
         )  const;
      //!@}

   public:
      //! Constructor from OdeInfo
      explicit AdaptiveRungeKuttaStepper
         (  const midas::ode::OdeInfo& aInfo
         );

      //! Virtual destructor
      virtual ~AdaptiveRungeKuttaStepper() = default;
};

#endif /* ADAPTIVERUNGEKUTTASTEPPER_DECL_H_INCLUDED */
