/**
************************************************************************
* 
* @file                 OdeStepperBase_Impl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of OdeStepperBase class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODESTEPPERBASE_IMPL_H_INCLUDED
#define ODESTEPPERBASE_IMPL_H_INCLUDED

#include "ode/OdeStepperBase_Decl.h"
#include "ode/Dopr853Stepper.h"
#include "ode/Dopr5Stepper.h"
#include "ode/Tsit5Stepper.h"

#include "ode/ODE.h"

/**
 * Constructor
 *
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
OdeStepperBase<ODE>::OdeStepperBase
   (  const midas::ode::OdeInfo& aInfo
   )
   :  mAbsTol
         (  aInfo.template get<Nb>("ODESTEPPERABSTOL")
         )
   ,  mRelTol
         (  aInfo.template get<Nb>("ODESTEPPERRELTOL")
         )
   ,  mDenseOutput
         (  aInfo.template get<In>("OUTPUTPOINTS") > I_0
         )
   ,  mUseFsal
         (  aInfo.template get<bool>("USEFSAL")
         )
   ,  mErrorType
         (  aInfo.template get<midas::ode::detail::OdeErrorType>("ERRORTYPE")
         )
   ,  mFixedStep
         (  aInfo.template get<bool>("FIXEDSTEPSIZE")
         )
   ,  mIoLevel
         (  aInfo.template get<In>("IOLEVEL")
         )
{
}

/**
 * Factory
 *
 * @param aInfo   OdeInfo
 * @return
 *    Unique pointer to concrete stepper
 **/
template
   <  typename ODE
   >
std::unique_ptr<OdeStepperBase<ODE> > OdeStepperBase<ODE>::Factory
   (  const midas::ode::OdeInfo& aInfo
   )
{
   auto iter = aInfo.find("STEPPER");
   if (  iter == aInfo.end()
      )
   {
      MIDASERROR("'STEPPER' not found in OdeInfo!");
   }

   auto stepper = iter->second.template get<std::string>();

   if (  stepper == "DOPR853"
      )
   {
      return std::make_unique<Dopr853Stepper<ODE> >(aInfo);
   }
   else if  (  stepper == "DOPR5"
            )
   {
      return std::make_unique<Dopr5Stepper<ODE> >(aInfo);
   }
   else if  (  stepper == "TSIT5"
            )
   {
      return std::make_unique<Tsit5Stepper<ODE> >(aInfo);
   }
#ifdef ENABLE_GSL
   else if  (  stepper == "GSL"
            )
   {
      // We do not use a separate stepper in GSL wrapper
      return nullptr;
   }
#endif   /* ENABLE_GSL */
   else
   {
      MIDASERROR("OdeStepper: " + stepper + " not recognized!");
   }

   // Never gets here!
   return nullptr;
}

/**
 * Do step
 *
 * @param arT                 t parameter
 * @param aTrialStep          Trial step size
 * @param arY                 y vector
 * @param arDyDt              dy/dt vector
 * @param arOde               ODE used for calculating derivatives, etc.
 * @param aDistanceToOutput   Distance to next output point
 * @param arForceStop         Stop integration after this accepted step?
 *
 * @return
 *    Scaled error estimate
 **/
template
   <  typename ODE
   >
typename OdeStepperBase<ODE>::step_t OdeStepperBase<ODE>::Step
   (  step_t& arT
   ,  step_t aTrialStep
   ,  vec_t& arY
   ,  vec_t& arDyDt
   ,  midas::ode::OdeWrapper<ODE>& arOde
   ,  step_t aDistanceToOutput
   ,  bool& arForceStop
   )
{
   return this->StepImpl(arT, aTrialStep, arY, arDyDt, arOde, aDistanceToOutput, arForceStop);
}

/**
 * Do interpolation
 *
 * @param aTime      Time
 * @param aStep      Length of interval (step) containing aTime
 * @return
 *    Interpolated y vector
 **/
template
   <  typename ODE
   >
typename OdeStepperBase<ODE>::vec_t OdeStepperBase<ODE>::Interpolate
   (  step_t aTime
   ,  step_t aStep
   )  const
{
   return this->InterpolateImpl(aTime, aStep);
}

/**
 * Type
 *
 * @return
 *    Type name
 **/
template
   <  typename ODE
   >
std::string OdeStepperBase<ODE>::Type
   (
   )  const
{
   return this->TypeImpl();
}

/**
 * Order
 *
 * @return
 *    Order of stepper method
 **/
template
   <  typename ODE
   >
In OdeStepperBase<ODE>::Order
   (
   )  const
{
   return this->OrderImpl();
}

/**
 * Get used step size
 *
 * @return
 *    Used step size
 **/
template
   <  typename ODE
   >
typename OdeStepperBase<ODE>::step_t OdeStepperBase<ODE>::UsedStepSize
   (
   )  const
{
   return mUsedStep;
}

/**
 * Get guess of next step size
 *
 * @return
 *    Next step size
 **/
template
   <  typename ODE
   >
typename OdeStepperBase<ODE>::step_t OdeStepperBase<ODE>::NextStepSize
   (
   )  const
{
   if (  this->mFixedStep
      )
   {
      MIDASERROR("NextStepSize not implemented for fixed step size!");
      return step_t(-1.);
   }
   else
   {
      return this->NextStepSizeImpl();
   }
}

/**
 * Summary
 **/
template
   <  typename ODE
   >
void OdeStepperBase<ODE>::Summary
   (
   )  const
{
   // Step info
   Mout  << " " << this->Type() << " summary:" << "\n"
         << "    # accepted steps:              " << this->mNGoodSteps << "\n"
         << "    # step-size reductions:        " << this->mNBadSteps << "\n"
         << std::flush;

   // Additional overloaded summary
   this->SummaryImpl();
}

/**
 * Default implementation of summary
 **/
template
   <  typename ODE
   >
void OdeStepperBase<ODE>::SummaryImpl
   (
   )  const
{
   // Do nothing...
   return;
}

/**
 * Get tol
 * @return
 *    abstol
 **/
template
   <  typename ODE
   >
Nb OdeStepperBase<ODE>::AbsTol
   (
   )  const
{
   return this->mAbsTol;
}

/**
 * Get tol
 * @return
 *    reltol
 **/
template
   <  typename ODE
   >
Nb OdeStepperBase<ODE>::RelTol
   (
   )  const
{
   return this->mRelTol;
}

/**
 * Get FixedStep
 * @return
 *    bool
 **/
template
   <  typename ODE
   >
bool OdeStepperBase<ODE>::FixedStep
   (
   )  const
{
   return this->mFixedStep;
}

/**
 * Get UseFsal
 * @return
 *    bool
 **/
template
   <  typename ODE
   >
bool OdeStepperBase<ODE>::UseFsal
   (
   )  const
{
   return this->mUseFsal;
}

#endif /* ODESTEPPERBASE_IMPL_H_INCLUDED */
