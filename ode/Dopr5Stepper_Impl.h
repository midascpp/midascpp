/**
************************************************************************
* 
* @file                 Dopr5Stepper_Impl.h
* 
* Created:              28-03-2018 (using code from 28-09-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Implementation of Dopr5Stepper class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DOPR5STEPPER_IMPL_H_INCLUDED
#define DOPR5STEPPER_IMPL_H_INCLUDED

#include "ode/Dopr5Stepper_Decl.h"
#include "ode/ODE.h"
#include "ode/OdeErrorNorm.h"

/**
 * Constructor
 *
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
Dopr5Stepper<ODE>::Dopr5Stepper
   (  const midas::ode::OdeInfo& aInfo
   )
   :  AdaptiveRungeKuttaStepper<ODE>(aInfo)
{
}

/**
 * Overload of CalculateDerivativesImpl
 * NB: In order to use the FSAL property, the OdeDriver cannot modify aDyDt between steps!
 * This should be fine in MidasOdeDriver.
 *
 * @param aT
 * @param aStepSize
 * @param aY
 * @param aDyDt
 * @param arOde
 *
 * @return
 *    Updated y vector
 **/
template
   <  typename ODE
   >
typename Dopr5Stepper<ODE>::vec_t Dopr5Stepper<ODE>::CalculateDerivativesImpl
   (  step_t aT
   ,  step_t aStepSize
   ,  const vec_t& aY
   ,  const vec_t& aDyDt
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   // Coefficients
   static constexpr Nb  c2 = 0.2, c3 = 0.3, c4 = 0.8, c5 = 8.0 / 9.0
                     ,  a21 = 0.2, a31 = 3.0 / 40.0, a32 = 9.0 / 40.0
                     ,  a41 = 44.0 / 45.0, a42 = -56.0 / 15.0, a43 = 32.0 / 9.0, a51 = 19372.0 / 6561.0
                     ,  a52 = -25360.0 / 2187.0, a53 = 64448.0 / 6561.0, a54 = -212.0 / 729.0, a61 = 9017.0 / 3168.0
                     ,  a62 = -355.0 / 33.0, a63 = 46732.0 / 5247.0, a64 = 49.0 / 176.0, a65 = -5103.0 / 18656.0
                     ,  a71 = 35.0 / 384.0, a73 = 500.0 / 1113.0, a74 = 125.0 / 192.0, a75 = -2187.0 / 6784.0
                     ,  a76 = 11.0 / 84.0, e1 = 71.0 / 57600.0, e3 = -71.0 / 16695.0, e4 = 71.0 / 1920.0
                     ,  e5 = -17253.0 / 339200.0, e6 = 22.0 / 525.0, e7 = -1.0 / 40.0;

   vec_t ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a21);
   mK2 = arOde.WRAP_Derivative(aT+c2*aStepSize, ytemp);

   ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a31);
   ODE_Axpy(ytemp, mK2, aStepSize*a32);
   mK3 = arOde.WRAP_Derivative(aT+c3*aStepSize, ytemp);

   ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a41);
   ODE_Axpy(ytemp, mK2, aStepSize*a42);
   ODE_Axpy(ytemp, mK3, aStepSize*a43);
   mK4 = arOde.WRAP_Derivative(aT+c4*aStepSize, ytemp);

   ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a51);
   ODE_Axpy(ytemp, mK2, aStepSize*a52);
   ODE_Axpy(ytemp, mK3, aStepSize*a53);
   ODE_Axpy(ytemp, mK4, aStepSize*a54);
   mK5 = arOde.WRAP_Derivative(aT+c5*aStepSize, ytemp);

   ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a61);
   ODE_Axpy(ytemp, mK2, aStepSize*a62);
   ODE_Axpy(ytemp, mK3, aStepSize*a63);
   ODE_Axpy(ytemp, mK4, aStepSize*a64);
   ODE_Axpy(ytemp, mK5, aStepSize*a65);
   auto xph = aT + aStepSize;
   mK6 = arOde.WRAP_Derivative(xph, ytemp);

   vec_t result = aY;
   ODE_Axpy(result, aDyDt, aStepSize*a71);
   ODE_Axpy(result, mK3, aStepSize*a73);
   ODE_Axpy(result, mK4, aStepSize*a74);
   ODE_Axpy(result, mK5, aStepSize*a75);
   ODE_Axpy(result, mK6, aStepSize*a76);
   mFsalDerivative = arOde.WRAP_Derivative(xph, result);

   ODE_Zero(this->mYerr);
   ODE_Axpy(mYerr, aDyDt, aStepSize*e1);
   ODE_Axpy(mYerr, mK3, aStepSize*e3);
   ODE_Axpy(mYerr, mK4, aStepSize*e4);
   ODE_Axpy(mYerr, mK5, aStepSize*e5);
   ODE_Axpy(mYerr, mK6, aStepSize*e6);
   ODE_Axpy(mYerr, mFsalDerivative, aStepSize*e7);

   return result;
}

/**
 * Save intermediates for dense output
 *
 * @param aStepSize        Step size
 * @param aYOld            Old y vector
 * @param aYNew            New y vector
 * @param aDyDtOld         dy/dt at t = t_old
 * @param aDyDtNew         dy/dt at t = t_old + aStepSize
 * @param arOde            ODE object for derivatives, etc.
 **/
template
   <  typename ODE
   >
void Dopr5Stepper<ODE>::PrepareDenseOutputImpl
   (  step_t aStepSize
   ,  const vec_t& aYOld
   ,  const vec_t& aYNew
   ,  const vec_t& aDyDtOld
   ,  const vec_t& aDyDtNew
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   static constexpr Nb  d1 = -12715105075.0 / 11282082432.0, d3 = 87487479700.0 / 32700410799.0
                     ,  d4 = -10690763975.0 / 1880347072.0, d5 = 701980252875.0 / 199316789632.0
                     ,  d6 = -1453857185.0 / 822651844.0, d7 = 69997945.0 / 29380423.0;

   // DEBUG: Check that mFsalDerivative == aDyDtNew
   if (  gDebug
      )
   {
      auto diff = mFsalDerivative;
      ODE_Axpy(diff, aDyDtNew, param_t(-1.));
      auto err = ODE_Norm(diff);

      Mout  << " DEBUG: Dopr5Stepper: ||mFsalDerivative - aDyDtNew|| = " << err << std::endl;
   }

   auto ydiff = aYNew;
   ODE_Axpy(ydiff, aYOld, param_t(-1.));

   mRcont1 = aYOld;
   mRcont2 = ydiff;

   auto bspl = aDyDtOld;
   ODE_Scale(bspl, aStepSize);
   ODE_Axpy(bspl, ydiff, param_t(-1.));

   mRcont3 = bspl;

   mRcont4 = ydiff;
   ODE_Axpy(mRcont4, mFsalDerivative, -aStepSize);
   ODE_Axpy(mRcont4, bspl, param_t(-1.));

   mRcont5 = aDyDtOld;
   ODE_Scale(mRcont5, d1);
   ODE_Axpy(mRcont5, mK3, d3);
   ODE_Axpy(mRcont5, mK4, d4);
   ODE_Axpy(mRcont5, mK5, d5);
   ODE_Axpy(mRcont5, mK6, d6);
   ODE_Axpy(mRcont5, mFsalDerivative, d7);

   ODE_Scale(mRcont5, aStepSize);
}

/**
 * Calculate error
 *
 * @param aStep
 * @param aOldY
 * @param aNewY
 *
 * @return
 *    Error
 **/
template
   <  typename ODE
   >
typename Dopr5Stepper<ODE>::step_t Dopr5Stepper<ODE>::ErrorEstimateImpl
   (  step_t aStep
   ,  const vec_t& aOldY
   ,  const vec_t& aNewY
   )  const
{
   auto err_squared = midas::ode::OdeErrorNorm2<ODE>(this->mYerr, this->mAbsTol, this->mRelTol, aOldY, aNewY, this->mErrorType);

   return std::sqrt(err_squared);
}

/**
 * Implementation of Interpolate
 * NB: Requires mTOld to be set!
 * 
 * @param aTime      Time
 * @param aStep      Step size
 * @return
 *    Interpolated y for t=aTime
 **/
template
   <  typename ODE
   >
typename Dopr5Stepper<ODE>::vec_t Dopr5Stepper<ODE>::InterpolateImpl
   (  step_t aTime
   ,  step_t aStep
   )  const
{
   step_t s = (aTime - this->mTOld) / aStep;
   step_t s1 = step_t(1.) - s;

   vec_t result = mRcont5;
   ODE_Scale(result, s1);
   ODE_Axpy(result, mRcont4, param_t(1.));
   ODE_Scale(result, s);
   ODE_Axpy(result, mRcont3, param_t(1.));
   ODE_Scale(result, s1);
   ODE_Axpy(result, mRcont2, param_t(1.));
   ODE_Scale(result, s);
   ODE_Axpy(result, mRcont1, param_t(1.));

   return result;
}

/**
 * Implementation of Type
 *
 * @return
 *    Type
 **/
template
   <  typename ODE
   >
std::string Dopr5Stepper<ODE>::TypeImpl
   (
   )  const
{
   return std::string("Dopr5Stepper");
}

/**
 * Implementation of Order
 *
 * @return
 *    Order
 **/
template
   <  typename ODE
   >
In Dopr5Stepper<ODE>::OrderImpl
   (
   )  const
{
   return I_5;
}

/**
 * Resize vector members
 *
 * @param aShape     vec_t of correct shape
 **/
template
   <  typename ODE
   >
void Dopr5Stepper<ODE>::ResizeVectorsImpl
   (  const vec_t& aShape
   )
{
   ODE_SetShape(mK2, aShape);
   ODE_SetShape(mK3, aShape);
   ODE_SetShape(mK4, aShape);
   ODE_SetShape(mK5, aShape);
   ODE_SetShape(mK6, aShape);

   ODE_SetShape(mRcont1, aShape);
   ODE_SetShape(mRcont2, aShape);
   ODE_SetShape(mRcont3, aShape);
   ODE_SetShape(mRcont4, aShape);
   ODE_SetShape(mRcont5, aShape);

   ODE_SetShape(mYerr, aShape);

   ODE_SetShape(mFsalDerivative, aShape);
}


/**
 * Do we use FSAL?
 *
 * @return
 *    true
 **/
template
   <  typename ODE
   >
bool Dopr5Stepper<ODE>::Fsal
   (
   )  const
{
   return true;
}

/**
 * Get saved FSAL derivative
 * @return
 *    Const ref to FSAL vector
 **/
template
   <  typename ODE
   >
typename Dopr5Stepper<ODE>::vec_t Dopr5Stepper<ODE>::GetFsalDerivative
   (
   )  const
{
   return this->mFsalDerivative;
}

#endif /* DOPR5STEPPER_IMPL_H_INCLUDED */

