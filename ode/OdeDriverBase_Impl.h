/**
************************************************************************
* 
* @file                 OdeDriverBase_Impl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of OdeDriverBase class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEDRIVERBASE_IMPL_H_INCLUDED
#define ODEDRIVERBASE_IMPL_H_INCLUDED

#include <fstream>

#include "ode/OdeDriverBase_Decl.h"
#include "ode/MidasOdeDriver.h"

#ifdef ENABLE_GSL
#include "ode/GslOdeDriver.h"
#endif   /* ENABLE_GSL */

/**
 * Constructor
 *
 * @param apOde      Pointer to ODE object
 * @param aInfo      Info for integrator
 **/
template
   <  typename ODE
   >
OdeDriverBase<ODE>::OdeDriverBase
   (  ODE* apOde
   ,  const midas::ode::OdeInfo& aInfo
   )
   :  midas::ode::OdeDatabase<ODE>
         (  aInfo
         )
   ,  mOde
         (  apOde
         )
   ,  mStepper
         (  OdeStepperBase<ODE>::Factory(aInfo)
         )
   ,  mName
         (  aInfo.template get<std::string>("NAME")
         )
   ,  mTimeInterval
         (  aInfo.template get<std::pair<Nb, Nb> >("TIMEINTERVAL")
         )
   ,  mMinStepSize
         (  aInfo.template get<Nb>("MINSTEPSIZE")
         )
   ,  mMaxSteps
         (  aInfo.template get<In>("MAXSTEPS")
         )
   ,  mImprovedInitialStepSize
         (  aInfo.template get<bool>("IMPROVEDINITIALSTEPSIZE")
         )
   ,  mInitialStepSize
         (  aInfo.template get<Nb>("INITIALSTEPSIZE")
         )
   ,  mInfo
         (  aInfo
         )
   ,  mIoLevel
         (  aInfo.template get<In>("IOLEVEL")
         )
{
}

/**
 * Factory for creating concrete instances of OdeDrivers
 *
 * @param apOde      Pointer to ODE object
 * @param aInfo      Info for integrator
 **/
template
   <  typename ODE
   >
std::unique_ptr<OdeDriverBase<ODE> > OdeDriverBase<ODE>::Factory
   (  ODE* apOde
   ,  const midas::ode::OdeInfo& aInfo
   )
{
   auto iter = aInfo.find("DRIVER");
   if (  iter == aInfo.end()
      )
   {
      MIDASERROR("'DRIVER' not found in OdeInfo!");
   }

   auto driver = iter->second.template get<std::string>();

   if (  driver == "MIDAS"
      )
   {
      return std::make_unique<MidasOdeDriver<ODE> >(apOde, aInfo);
   }
#ifdef ENABLE_GSL
   else if  (  driver == "GSL"
            )
   {
      return std::make_unique<GslOdeDriver<ODE> >(apOde, aInfo);
   }
#endif   /* ENABLE_GSL */
   else
   {
      MIDASERROR("OdeDriver: " + driver + " not recognized!");
   }

   // Never gets here!
   return nullptr;
}


/**
 * Integrate interface
 *
 * @param arYStart      Start vector (will be updated)
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::Integrate
   (  vec_t& arYStart
   )
{
   this->IntegrateImpl(arYStart);
}

/**
 * Type interface
 **/
template
   <  typename ODE
   >
std::string OdeDriverBase<ODE>::Type
   (
   )  const
{
   auto type = this->TypeImpl();

   // Add stepper type
   if (  this->mStepper
      )
   {
      type += std::string("_") + this->mStepper->Type();
   }

   return type;
}

/**
 * Set time interval
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::SetTimeInterval
   (  Nb aT0
   ,  Nb aT1
   )
{
   this->mTimeInterval = {{ aT0, aT1 }};
}

/**
 * Reset statistics
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::ResetStatistics
   (
   )
{
   if (  this->mStepper
      )
   {
      // Reset stepper
      this->mStepper->ResetStatistics();
   }

   // Reset driver
   this->ResetStatisticsImpl();
}

/**
 * Default implementation of reset stat
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::ResetStatisticsImpl
   (
   )
{
   // Do nothing...
   return;
}

/**
 * Get database t
 *
 * @return
 *    vector of t's for all (interpolated) steps
 **/
template
   <  typename ODE
   >
const std::vector<typename OdeDriverBase<ODE>::step_t>& OdeDriverBase<ODE>::GetDatabaseT
   (
   )  const
{
   return Data::GetT();
}

/**
 * Get database y
 *
 * @return
 *    vector of y's for all (interpolated) steps
 **/
template
   <  typename ODE
   >
const std::vector<typename OdeDriverBase<ODE>::vec_t>& OdeDriverBase<ODE>::GetDatabaseY
   (
   )  const
{
   return Data::GetY();
}

/**
 * Summary
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::Summary
   (
   )  const
{
   Mout  << " \n\n=== Summary of " << this->Type() << " integration ===" << std::endl;

   // Stepper summary
   if (  this->mStepper
      )
   {
      this->mStepper->Summary();
   }

   // Driver summary
   // Step info
   Mout  << " " << this->Type() << " summary:" << "\n"
         << "    # steps:                       " << this->mNSteps << "\n"
         << "    # failed steps:                " << this->mOde.NFailedSteps() << "\n"
         << "    # derivatives:                 " << this->mOde.NDeriv() << "\n"
         << std::flush;

   // Additional overloaded summary
   this->SummaryImpl();

   Mout  << "==========================================================\n\n" << std::flush;

   // Write saved steps
   if (  !this->mSavedSteps.empty()
      )
   {
      std::string name = this->mName;
      if (  !name.empty()
         )
      {
         name += "_";
      }
      std::ofstream out(name + this->Type() + "_steps.dat");

      // Header
      size_t width = 20;
      out   << std::setw(width) << "#" << std::setw(width) << "Step size" << std::setw(width) << "Scaled error" << std::endl;

      size_t istep = 0;
      for(const auto& step : this->mSavedSteps)
      {
         out   << std::setw(width) << istep++
               << std::setw(width) << step.first
               << std::setw(width) << step.second
               << std::endl;
      }

      out.close();
   }
}

/**
 * Default implementation of summary
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::SummaryImpl
   (
   )  const
{
   return;
}

/**
 * Are we using complex numbers?
 *
 * @return
 *    True/false
 **/
template
   <  typename ODE
   >
constexpr bool OdeDriverBase<ODE>::ComplexOde
   (
   )  const
{
   return midas::type_traits::IsComplexV<param_t>;
}

/**
 * Save step
 *
 * @param aStepSize
 * @param aErr
 **/
template
   <  typename ODE
   >
void OdeDriverBase<ODE>::SaveStep
   (  step_t aStepSize
   ,  step_t aErr
   )
{
   bool save = this->mInfo.template get<bool>("SAVESTEPS");

   if (  save
      )
   {
      // Reserve space
      if (  this->mSavedSteps.empty()
         )
      {
         auto init_size = (mMaxSteps < I_1) ? 100 : mMaxSteps;
         this->mSavedSteps.reserve(init_size);
      }
      else if  (  this->mSavedSteps.size() == this->mSavedSteps.capacity()
               )
      {
         this->mSavedSteps.reserve(this->mSavedSteps.size() * 2);
      }

      // Save point
      this->mSavedSteps.emplace_back(aStepSize, aErr);
   }
}

/**
 * Estimate initial step size
 *
 * @param aT0        Initial t
 * @param arY
 * @param arDyDt
 * @param aAbsTol
 * @param aRelTol
 * @param aOrder     Order of Runge-Kutta method
 * @return
 *    Step size
 **/
template
   <  typename ODE
   >
typename OdeDriverBase<ODE>::step_t OdeDriverBase<ODE>::ImprovedInitialStepSize
   (  step_t aT0
   ,  const vec_t& arY
   ,  const vec_t& arDyDt
   ,  step_t aAbsTol
   ,  step_t aRelTol
   ,  In aOrder
   )
{
   auto zero = arY;
   ODE_Zero(zero);
   step_t d0 = std::sqrt(ODE_OdeMeanNorm2(arY, aAbsTol, aRelTol, arY, zero));
   step_t d1 = std::sqrt(ODE_OdeMeanNorm2(arDyDt, aAbsTol, aRelTol, arY, zero));

   step_t h0   =  libmda::numeric::float_leq(d0, step_t(C_I_10_5)) || libmda::numeric::float_leq(d1, step_t(C_I_10_5))
               ?  static_cast<step_t>(C_I_10_6)
               :  static_cast<step_t>(0.01)*(d0/d1);

   auto y1 = arY;
   ODE_Axpy(y1, arDyDt, h0);
   auto dydt1 = this->mOde.WRAP_Derivative(aT0+h0, y1);
   ODE_Axpy(dydt1, arDyDt, -C_1);
   step_t d2 = std::sqrt(ODE_OdeMeanNorm2(dydt1, aAbsTol, aRelTol, y1, arY));

   step_t max = std::max(d1, d2);

   step_t h1   =  libmda::numeric::float_leq(max, step_t(1.e-15))
               ?  std::max(static_cast<step_t>(C_I_10_6), h0*static_cast<step_t>(C_I_10_3))
               :  std::pow(static_cast<step_t>(0.01) / max, -(aOrder + 1));

   return std::min(100*h0, h1);
}

#endif /* ODEDRIVERBASE_IMPL_H_INCLUDED */
