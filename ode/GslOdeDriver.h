/**
************************************************************************
* 
* @file                 GslOdeDriver.h
* 
* Created:              24-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Header for GslOdeDriver class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifdef ENABLE_GSL

#ifndef GSLODEDRIVER_H_INCLUDED
#define GSLODEDRIVER_H_INCLUDED

// Include declaration
#include "ode/GslOdeDriver_Decl.h"

// Include implementation
#include "ode/GslOdeDriver_Impl.h"

#endif /* GSLODEDRIVER_H_INCLUDED */

#endif   /* ENABLE_GSL */
