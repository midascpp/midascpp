/**
 *******************************************************************************
 * 
 * @file    OdeInfoOutput.h
 * @date    22-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef ODEINFOOUTPUT_H_INCLUDED
#define ODEINFOOUTPUT_H_INCLUDED

#include <iosfwd>
namespace midas::ode
{
   class OdeInfo;

   //! Output of OdeInfo settings, modifiable formatting.
   std::ostream& Output
      (  std::ostream&
      ,  const OdeInfo&
      ,  unsigned int aIndent = 0
      ,  unsigned int aLeftWidth = 30
      ,  const char* aEqualSign = " = "
      );
} /* namespace midas::ode */

//! Output of OdeInfo settings, default Output(...) formatting.
std::ostream& operator<<(std::ostream&, const midas::ode::OdeInfo&);

#endif /* ODEINFOOUTPUT_H_INCLUDED */
