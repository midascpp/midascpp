/**
************************************************************************
* 
* @file                 ODE.h
* 
* Created:              11-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Interface to important methods for ODE::vec_t.
*                       NB: Completely stolen from Ian's it_solver/IES.h
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ODE_H_INCLUDED
#define ODE_H_INCLUDED

#include <type_traits>

#include "it_solver/IES.h"

/**
 *
 **/
#define STRINGYFY(STR) #STR
#define CREATE_INTERFACE(NAME) \
template \
   <  class \
   ,  class=void \
   > \
struct ODE_##NAME##_check \
   :  std::false_type \
{ \
}; \
template \
   <  class... Us \
   > \
struct ODE_##NAME##_check \
   <  ies::detail::pack<Us...> \
   ,  typename std::enable_if_t<ies::detail::type_sink<decltype(NAME(std::declval<Us>()...))>::value> \
   > \
   :  std::true_type \
{ \
}; \
 \
\
template<class... Us> \
struct ODE_##NAME##_type \
{ \
   static const bool value = ODE_##NAME##_check<ies::detail::pack<Us...> >::value; \
   \
   static_assert(value,"No " STRINGYFY(NAME) " function supplied!"); \
   using type = decltype(NAME(std::declval<Us>()...)); \
}; \
 \
template<class... Ts> \
inline auto ODE_##NAME (Ts&&... ts) \
   -> typename ODE_##NAME##_type<Ts...>::type \
{ \
   return NAME(std::forward<Ts>(ts)...); \
} 

CREATE_INTERFACE(Scale);
CREATE_INTERFACE(Zero);
CREATE_INTERFACE(Axpy);
CREATE_INTERFACE(SetShape);
CREATE_INTERFACE(Norm);
CREATE_INTERFACE(Size);
CREATE_INTERFACE(OdeMeanNorm2);
CREATE_INTERFACE(OdeMaxNorm2);
CREATE_INTERFACE(DataFromPointer);
CREATE_INTERFACE(DataToPointer);

#undef CREATE_INTERFACE
#undef STRINGYFY

#endif /* ODE_H_INCLUDED */
