/**
************************************************************************
* 
* @file                 OdeDriverBase_Decl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of OdeDriverBase class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEDRIVERBASE_DECL_H_INCLUDED
#define ODEDRIVERBASE_DECL_H_INCLUDED

#include <string>
#include <memory>

#include "ode/OdeInfo.h"
#include "ode/OdeDatabase.h"
#include "ode/OdeStepperBase.h"
#include "ode/OdeWrapper.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/**
 * Abstract base class for the ODE drivers.
 **/
template
   <  typename ODE
   >
class OdeDriverBase
   :  protected midas::ode::OdeDatabase<ODE>
{
   public:
      //! Aliases
      ODE_ALIAS(ODE);

      using point_t = std::pair<step_t, step_t>;
      using Data = midas::ode::OdeDatabase<ODE>;

   protected:
      //! Wrapper around ODE pointer used for calculating derivatives, normalizing vectors, etc.
      midas::ode::OdeWrapper<ODE> mOde;

      //! Stepper
      std::unique_ptr<OdeStepperBase<ODE> > mStepper = nullptr;

      //! Name
      std::string mName = "";

      //! Time interval
      std::pair<Nb, Nb> mTimeInterval = {C_0, C_0};

      //! Minimum step size
      Nb mMinStepSize = std::numeric_limits<Nb>::epsilon();

      //! Max number of steps (-1 is no bound)
      In mMaxSteps = -I_1;

      //! Number of steps used in integration
      In mNSteps = I_0;

      //! Use improved initial-step-size selection
      bool mImprovedInitialStepSize = true;

      //! Initial step size used if improved stepsize is not set
      Nb mInitialStepSize = C_I_10_2;

      //! Reference to OdeInfo
      const midas::ode::OdeInfo& mInfo;

      //! Saved steps: <step size, scaled error>
      std::vector<point_t> mSavedSteps;

      //! IoLevel
      In mIoLevel = I_1;

   private:
      //! Overloadable implementation of Integrate
      virtual void IntegrateImpl
         (  vec_t& arYStart
         )  = 0;

      //! Overloadable implementation of Type
      virtual std::string TypeImpl
         (
         )  const = 0;

      //! Overloadable implementation of ResetStatistics
      virtual void ResetStatisticsImpl
         (
         );

      //! Overloadable implementation of Summary
      virtual void SummaryImpl
         (
         )  const;

   protected:
      //! Estimate initial step size
      step_t ImprovedInitialStepSize
         (  step_t aT0
         ,  const vec_t& arY
         ,  const vec_t& arDyDt
         ,  step_t aAbsTol
         ,  step_t aRelTol
         ,  In
         );

      //! Save step
      void SaveStep
         (  step_t
         ,  step_t
         );

      //! Are we using complex numbers?
      constexpr bool ComplexOde
         (
         )  const;


   public:
      //! Constructor
      explicit OdeDriverBase
         (  ODE* apOde
         ,  const midas::ode::OdeInfo& aInfo
         );

      //! Virtual destructor
      virtual ~OdeDriverBase() = default;

      //! Integrate interface (calls implementation)
      void Integrate
         (  vec_t& arYStart
         );

      //! Type interface
      std::string Type
         (
         )  const;

      //! Set time interval
      void SetTimeInterval
         (  Nb aT0
         ,  Nb aT1
         );

      //! Reset statistics
      void ResetStatistics
         (
         );

      //! Get database T
      const std::vector<step_t>& GetDatabaseT
         (
         )  const;

      //! Get database Y
      const std::vector<vec_t>& GetDatabaseY
         (
         )  const;

      //! Write summary
      void Summary
         (
         )  const;

      //! Factory
      static std::unique_ptr<OdeDriverBase<ODE>> Factory
         (  ODE* apOde
         ,  const midas::ode::OdeInfo& aInfo
         );
};

#endif /* ODEDRIVERBASE_DECL_H_INCLUDED */
