/**
************************************************************************
* 
* @file                 OdeErrorNorm.h
* 
* Created:              16-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Calculate error norms
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODE_ERROR_NORM_H_INCLUDED
#define ODE_ERROR_NORM_H_INCLUDED

#include "util/Error.h"
#include "ode/ODE.h"

namespace midas::ode
{
namespace detail
{
//! Error-estimate types
enum class OdeErrorType : int
{  MEAN = 0       // Average error norm
,  MAX            // Max error
,  SCALEDMAX      // Max error / n_elements. Not really safe, but was the old default...
};
} /* namespace detail */


/**
 * Calculate error norm
 *
 * @param aErr       Error vector
 * @param aAbsTol
 * @param aRelTol
 * @param aOldY      Old y vector
 * @param aNewY      New y vector
 * @param aType      Error type
 *
 * @return
 *    Error norm
 **/
template
   <  typename ODE
   >
typename ODE::step_t OdeErrorNorm2
   (  const typename ODE::vec_t& aErr
   ,  typename ODE::step_t aAbsTol
   ,  typename ODE::step_t aRelTol
   ,  const typename ODE::vec_t& aOldY
   ,  const typename ODE::vec_t& aNewY
   ,  midas::ode::detail::OdeErrorType aType
   )
{
   using step_t = typename ODE::step_t;

   switch   (  aType
            )
   {
      case midas::ode::detail::OdeErrorType::MEAN:
      {
         return std::max<step_t>(0., ODE_OdeMeanNorm2(aErr, aAbsTol, aRelTol, aOldY, aNewY));
         break;
      }
      case midas::ode::detail::OdeErrorType::MAX:
      {
         return std::max<step_t>(0., ODE_OdeMaxNorm2(aErr, aAbsTol, aRelTol, aOldY, aNewY));
         break;
      }
      // This is not really a good idea...
      case midas::ode::detail::OdeErrorType::SCALEDMAX:
      {
         auto err = std::max<step_t>(0., ODE_OdeMaxNorm2(aErr, aAbsTol, aRelTol, aOldY, aNewY));
         err /= ODE_Size(aErr);
         return err;
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized error-norm type!");
         return step_t(-1.);
      }
   }
}

} /* namespace midas::ode */

#endif /* ODE_ERROR_NORM_H_INCLUDED */
