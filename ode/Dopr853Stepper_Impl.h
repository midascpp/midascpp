/**
************************************************************************
* 
* @file                 Dopr853Stepper_Impl.h
* 
* Created:              28-03-2018 (using code from 19-10-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Implementation of Dopr853Stepper class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DOPR853STEPPER_IMPL_H_INCLUDED
#define DOPR853STEPPER_IMPL_H_INCLUDED

#include "ode/Dopr853Stepper_Decl.h"
#include "ode/ODE.h"
#include "ode/OdeErrorNorm.h"

/**
 * Constructor
 *
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
Dopr853Stepper<ODE>::Dopr853Stepper
   (  const midas::ode::OdeInfo& aInfo
   )
   :  AdaptiveRungeKuttaStepper<ODE>(aInfo)
{
}

/**
 * Calculate error
 *
 * @param aStep
 * @param aOldY
 * @param aNewY
 *
 * @return
 *    Error
 **/
template
   <  typename ODE
   >
typename Dopr853Stepper<ODE>::step_t Dopr853Stepper<ODE>::ErrorEstimateImpl
   (  step_t aStep
   ,  const vec_t& aOldY
   ,  const vec_t& aNewY
   )  const
{
   // Calculate error using "stretched" error estimator of 
   // Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations I - Nonstiff Problems, page 254
   // NB: The multiplication with abs(aStep) is NOT found in the book and is inherited from Kasper Monrad's code (but it works nicely!).
   auto err3_squared = midas::ode::OdeErrorNorm2<ODE>(this->mYerr, this->mAbsTol, this->mRelTol, aOldY, aNewY, this->mErrorType);
   auto err5_squared = midas::ode::OdeErrorNorm2<ODE>(this->mYerr2, this->mAbsTol, this->mRelTol, aOldY, aNewY, this->mErrorType);

   auto denom = std::sqrt(err5_squared + static_cast<step_t>(0.01)*err3_squared);
   if (  libmda::numeric::float_leq(denom, static_cast<step_t>(0.))
      )
   {
      denom = decltype(err3_squared)(1.) / std::sqrt(step_t(ODE_Size(mYerr)));
   }

   auto err = err5_squared / denom;

   return std::abs(aStep)*err;
}

/**
 * Calculate derivatives and return new y vector
 *
 * @param aT
 * @param aStepSize
 * @param aY
 * @param aDyDt
 * @param arOde
 *
 * @return
 *    Updated y vector
 **/
template
   <  typename ODE
   >
typename Dopr853Stepper<ODE>::vec_t Dopr853Stepper<ODE>::CalculateDerivativesImpl
   (  step_t aT
   ,  step_t aStepSize
   ,  const vec_t& aY
   ,  const vec_t& aDyDt
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   vec_t ytemp = aY;
   ODE_Axpy(ytemp, aDyDt, aStepSize*a21);
   mK2 = arOde.WRAP_Derivative(aT+c2*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK2, a32);
   ODE_Axpy(ytemp, aDyDt, a31);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK3 = arOde.WRAP_Derivative(aT+c3*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK3, a43);
   ODE_Axpy(ytemp, aDyDt, a41);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK4 = arOde.WRAP_Derivative(aT+c4*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK4, a54);
   ODE_Axpy(ytemp, mK3, a53);
   ODE_Axpy(ytemp, aDyDt, a51);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK5 = arOde.WRAP_Derivative(aT+c5*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK5, a65);
   ODE_Axpy(ytemp, mK4, a64);
   ODE_Axpy(ytemp, aDyDt, a61);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK6 = arOde.WRAP_Derivative(aT+c6*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK6, a76);
   ODE_Axpy(ytemp, mK5, a75);
   ODE_Axpy(ytemp, mK4, a74);
   ODE_Axpy(ytemp, aDyDt, a71);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK7 = arOde.WRAP_Derivative(aT+c7*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK7, a87);
   ODE_Axpy(ytemp, mK6, a86);
   ODE_Axpy(ytemp, mK5, a85);
   ODE_Axpy(ytemp, mK4, a84);
   ODE_Axpy(ytemp, aDyDt, a81);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK8 = arOde.WRAP_Derivative(aT+c8*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK8, a98);
   ODE_Axpy(ytemp, mK7, a97);
   ODE_Axpy(ytemp, mK6, a96);
   ODE_Axpy(ytemp, mK5, a95);
   ODE_Axpy(ytemp, mK4, a94);
   ODE_Axpy(ytemp, aDyDt, a91);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK9 = arOde.WRAP_Derivative(aT+c9*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK9, a109);
   ODE_Axpy(ytemp, mK8, a108);
   ODE_Axpy(ytemp, mK7, a107);
   ODE_Axpy(ytemp, mK6, a106);
   ODE_Axpy(ytemp, mK5, a105);
   ODE_Axpy(ytemp, mK4, a104);
   ODE_Axpy(ytemp, aDyDt, a101);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK10 = arOde.WRAP_Derivative(aT+c10*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK10, a1110);
   ODE_Axpy(ytemp, mK9, a119);
   ODE_Axpy(ytemp, mK8, a118);
   ODE_Axpy(ytemp, mK7, a117);
   ODE_Axpy(ytemp, mK6, a116);
   ODE_Axpy(ytemp, mK5, a115);
   ODE_Axpy(ytemp, mK4, a114);
   ODE_Axpy(ytemp, aDyDt, a111);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK2 = arOde.WRAP_Derivative(aT+c11*aStepSize, ytemp);

   auto xph = aT+aStepSize;
   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK2, a1211);
   ODE_Axpy(ytemp, mK10, a1210);
   ODE_Axpy(ytemp, mK9, a129);
   ODE_Axpy(ytemp, mK8, a128);
   ODE_Axpy(ytemp, mK7, a127);
   ODE_Axpy(ytemp, mK6, a126);
   ODE_Axpy(ytemp, mK5, a125);
   ODE_Axpy(ytemp, mK4, a124);
   ODE_Axpy(ytemp, aDyDt, a121);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aY, param_t(1.));
   mK3 = arOde.WRAP_Derivative(xph, ytemp);

   ODE_Zero(mK4);
   ODE_Axpy(mK4, aDyDt, b1);
   ODE_Axpy(mK4, mK6, b6);
   ODE_Axpy(mK4, mK7, b7);
   ODE_Axpy(mK4, mK8, b8);
   ODE_Axpy(mK4, mK9, b9);
   ODE_Axpy(mK4, mK10, b10);
   ODE_Axpy(mK4, mK2, b11);
   ODE_Axpy(mK4, mK3, b12);

   // Calculate result
   vec_t result = aY;
   ODE_Axpy(result, mK4, aStepSize);

   // Calculate error
   // NB: This is strictly not necessary for mFixedStep = true, but it does not cost much, and then we can calculate the error.
   mYerr = mK4;
   ODE_Axpy(mYerr, aDyDt, -bhh1);
   ODE_Axpy(mYerr, mK9, -bhh2);
   ODE_Axpy(mYerr, mK3, -bhh3);
   ODE_Zero(mYerr2);
   ODE_Axpy(mYerr2, aDyDt, er1);
   ODE_Axpy(mYerr2, mK6, er6);
   ODE_Axpy(mYerr2, mK7, er7);
   ODE_Axpy(mYerr2, mK8, er8);
   ODE_Axpy(mYerr2, mK9, er9);
   ODE_Axpy(mYerr2, mK10, er10);
   ODE_Axpy(mYerr2, mK2, er11);
   ODE_Axpy(mYerr2, mK3, er12);

   return result;
}

/**
 * Save intermediates for dense output
 * NB: Requires mTOld to be set!
 *
 * @param aStepSize        Step size
 * @param aYOld            Old y vector
 * @param aYNew            New y vector
 * @param aDyDtOld         dy/dt at t = t_old
 * @param aDyDtNew         dy/dt at t = t_old + aStepSize
 * @param arOde            ODE object for derivatives, etc.
 **/
template
   <  typename ODE
   >
void Dopr853Stepper<ODE>::PrepareDenseOutputImpl
   (  step_t aStepSize
   ,  const vec_t& aYOld
   ,  const vec_t& aYNew
   ,  const vec_t& aDyDtOld
   ,  const vec_t& aDyDtNew
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   {
      auto ydiff = aYNew;
      ODE_Axpy(ydiff, aYOld, param_t(-1.));

      mRcont1 = aYOld;

      mRcont2 = ydiff;

      auto bspl = ydiff;
      ODE_Scale(bspl, param_t(-1.));
      ODE_Axpy(bspl, aDyDtOld, aStepSize);

      mRcont3 = bspl;

      mRcont4 = ydiff;
      ODE_Axpy(mRcont4, aDyDtNew, -aStepSize);
      ODE_Axpy(mRcont4, bspl, param_t(-1.));

      ODE_Zero(mRcont5);
      ODE_Axpy(mRcont5, aDyDtOld, d41);
      ODE_Axpy(mRcont5, mK6,    d46);
      ODE_Axpy(mRcont5, mK7,    d47);
      ODE_Axpy(mRcont5, mK8,    d48);
      ODE_Axpy(mRcont5, mK9,    d49);
      ODE_Axpy(mRcont5, mK10,   d410);
      ODE_Axpy(mRcont5, mK2,    d411);
      ODE_Axpy(mRcont5, mK3,    d412);

      ODE_Zero(mRcont6);
      ODE_Axpy(mRcont6, aDyDtOld, d51);
      ODE_Axpy(mRcont6, mK6,    d56);
      ODE_Axpy(mRcont6, mK7,    d57);
      ODE_Axpy(mRcont6, mK8,    d58);
      ODE_Axpy(mRcont6, mK9,    d59);
      ODE_Axpy(mRcont6, mK10,   d510);
      ODE_Axpy(mRcont6, mK2,    d511);
      ODE_Axpy(mRcont6, mK3,    d512);

      ODE_Zero(mRcont7);
      ODE_Axpy(mRcont7, aDyDtOld, d61);
      ODE_Axpy(mRcont7, mK6,    d66);
      ODE_Axpy(mRcont7, mK7,    d67);
      ODE_Axpy(mRcont7, mK8,    d68);
      ODE_Axpy(mRcont7, mK9,    d69);
      ODE_Axpy(mRcont7, mK10,   d610);
      ODE_Axpy(mRcont7, mK2,    d611);
      ODE_Axpy(mRcont7, mK3,    d612);

      ODE_Zero(mRcont8);
      ODE_Axpy(mRcont8, aDyDtOld, d71);
      ODE_Axpy(mRcont8, mK6,    d76);
      ODE_Axpy(mRcont8, mK7,    d77);
      ODE_Axpy(mRcont8, mK8,    d78);
      ODE_Axpy(mRcont8, mK9,    d79);
      ODE_Axpy(mRcont8, mK10,   d710);
      ODE_Axpy(mRcont8, mK2,    d711);
      ODE_Axpy(mRcont8, mK3,    d712);
   }

   vec_t ytemp = aDyDtNew;
   ODE_Scale(ytemp, a1413);
   ODE_Axpy(ytemp, mK3, a1412);
   ODE_Axpy(ytemp, mK2, a1411);
   ODE_Axpy(ytemp, mK10, a1410);
   ODE_Axpy(ytemp, mK9, a149);
   ODE_Axpy(ytemp, mK8, a148);
   ODE_Axpy(ytemp, mK7, a147);
   ODE_Axpy(ytemp, aDyDtOld, a141);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aYOld, param_t(1.));
   mK10 = arOde.WRAP_Derivative(this->mTOld+c14*aStepSize, ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK10, a1514);
   ODE_Axpy(ytemp, aDyDtNew, a1513);
   ODE_Axpy(ytemp, mK3, a1512);
   ODE_Axpy(ytemp, mK2, a1511);
   ODE_Axpy(ytemp, mK8, a158);
   ODE_Axpy(ytemp, mK7, a157);
   ODE_Axpy(ytemp, mK6, a156);
   ODE_Axpy(ytemp, aDyDtOld, a151);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aYOld, param_t(1.));
   mK2 = arOde.WRAP_Derivative(this->mTOld+c15*aStepSize,ytemp);

   ODE_Zero(ytemp);
   ODE_Axpy(ytemp, mK2, a1615);
   ODE_Axpy(ytemp, mK10, a1614);
   ODE_Axpy(ytemp, aDyDtNew, a1613);
   ODE_Axpy(ytemp, mK9, a169);
   ODE_Axpy(ytemp, mK8, a168);
   ODE_Axpy(ytemp, mK7, a167);
   ODE_Axpy(ytemp, mK6, a166);
   ODE_Axpy(ytemp, aDyDtOld, a161);
   ODE_Scale(ytemp, aStepSize);
   ODE_Axpy(ytemp, aYOld, param_t(1.));
   mK3 = arOde.WRAP_Derivative(this->mTOld+c16*aStepSize,ytemp);

   ODE_Axpy(mRcont5, aDyDtNew, d413);
   ODE_Axpy(mRcont5, mK10, d414);
   ODE_Axpy(mRcont5, mK2, d415);
   ODE_Axpy(mRcont5, mK3, d416);
   ODE_Scale(mRcont5, aStepSize);

   ODE_Axpy(mRcont6, aDyDtNew, d513);
   ODE_Axpy(mRcont6, mK10, d514);
   ODE_Axpy(mRcont6, mK2, d515);
   ODE_Axpy(mRcont6, mK3, d516);
   ODE_Scale(mRcont6, aStepSize);

   ODE_Axpy(mRcont7, aDyDtNew, d613);
   ODE_Axpy(mRcont7, mK10, d614);
   ODE_Axpy(mRcont7, mK2, d615);
   ODE_Axpy(mRcont7, mK3, d616);
   ODE_Scale(mRcont7, aStepSize);

   ODE_Axpy(mRcont8, aDyDtNew, d713);
   ODE_Axpy(mRcont8, mK10, d714);
   ODE_Axpy(mRcont8, mK2, d715);
   ODE_Axpy(mRcont8, mK3, d716);
   ODE_Scale(mRcont8, aStepSize);
}

/**
 * Implementation of Interpolate
 * NB: Requires mTOld to be set!
 * 
 * @param aTime      Time
 * @param aStep      Size of interval (step) containing aTime
 * @return
 *    Interpolated y for t=aTime
 **/
template
   <  typename ODE
   >
typename Dopr853Stepper<ODE>::vec_t Dopr853Stepper<ODE>::InterpolateImpl
   (  step_t aTime
   ,  step_t aStep
   )  const
{
   param_t s = (aTime-this->mTOld)/aStep;
   param_t s1 = param_t(1.)-s;

   auto result = mRcont7;
   ODE_Axpy(result, mRcont8, s);
   ODE_Scale(result, s1);
   ODE_Axpy(result, mRcont6, param_t(1.));
   ODE_Scale(result, s);
   ODE_Axpy(result, mRcont5, param_t(1.));
   ODE_Scale(result, s1);
   ODE_Axpy(result, mRcont4, param_t(1.));
   ODE_Scale(result, s);
   ODE_Axpy(result, mRcont3, param_t(1.));
   ODE_Scale(result, s1);
   ODE_Axpy(result, mRcont2, param_t(1.));
   ODE_Scale(result, s);
   ODE_Axpy(result, mRcont1, param_t(1.));

   return result;
}

/**
 * Implementation of Type
 *
 * @return
 *    Type
 **/
template
   <  typename ODE
   >
std::string Dopr853Stepper<ODE>::TypeImpl
   (
   )  const
{
   return std::string("Dopr853Stepper");
}

/**
 * Implementation of Order
 *
 * @return
 *    Order
 **/
template
   <  typename ODE
   >
In Dopr853Stepper<ODE>::OrderImpl
   (
   )  const
{
   return I_8;
}

/**
 * Resize vector members
 *
 * @param aShape     vec_t of correct shape
 **/
template
   <  typename ODE
   >
void Dopr853Stepper<ODE>::ResizeVectorsImpl
   (  const vec_t& aShape
   )
{
   ODE_SetShape(mK2, aShape);
   ODE_SetShape(mK3, aShape);
   ODE_SetShape(mK4, aShape);
   ODE_SetShape(mK5, aShape);
   ODE_SetShape(mK6, aShape);
   ODE_SetShape(mK7, aShape);
   ODE_SetShape(mK8, aShape);
   ODE_SetShape(mK9, aShape);
   ODE_SetShape(mK10, aShape);

   ODE_SetShape(mRcont1, aShape);
   ODE_SetShape(mRcont2, aShape);
   ODE_SetShape(mRcont3, aShape);
   ODE_SetShape(mRcont4, aShape);
   ODE_SetShape(mRcont5, aShape);
   ODE_SetShape(mRcont6, aShape);
   ODE_SetShape(mRcont7, aShape);
   ODE_SetShape(mRcont8, aShape);

   ODE_SetShape(mYerr, aShape);
   ODE_SetShape(mYerr2, aShape);
}

#endif /* DOPR853STEPPER_IMPL_H_INCLUDED */

