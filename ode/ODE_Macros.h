/**
************************************************************************
* 
* @file                 ODE_Macros.h
* 
* Created:              23-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Macros for ODE module
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODE_MACROS_H_INCLUDED
#define ODE_MACROS_H_INCLUDED

#define ODE_ALIAS(SOURCE) \
   using step_t = typename SOURCE::step_t;   \
   using vec_t = typename SOURCE::vec_t;     \
   using param_t = typename SOURCE::param_t; \

#endif /* ODE_MACROS_H_INCLUDED */
