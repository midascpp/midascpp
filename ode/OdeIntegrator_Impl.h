/**
************************************************************************
* 
* @file                 OdeIntegrator_Impl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of OdeIntegrator class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEINTEGRATOR_IMPL_H_INCLUDED
#define ODEINTEGRATOR_IMPL_H_INCLUDED

#include "ode/OdeIntegrator_Decl.h"


/**
 * Constructor
 *
 * @param apOde   Pointer to ODE object used for calculating dy/dt, normalizing y, etc.
 * @param aInfo   OdeInfo
 **/
template
   <  class ODE
   >
OdeIntegrator<ODE>::OdeIntegrator
   (  ODE* apOde
   ,  const midas::ode::OdeInfo& aInfo
   )
   :  mDriver
         (  OdeDriverBase<ODE>::Factory(apOde, aInfo)
         )
{
}

/**
 * Integrate ODE by calling the driver.
 *
 * @param arYStart      y vector at t=t0
 **/
template
   <  class ODE
   >
void OdeIntegrator<ODE>::Integrate
   (  vec_t& arYStart
   )
{
   if (  !this->mDriver
      )
   {
      MIDASERROR("Uninitialized OdeDriver!");
   }
   else
   {
      this->mDriver->Integrate(arYStart);
   }
}

/**
 * Get database t
 *
 * @return
 *    vector of t's for all (interpolated) steps
 **/
template
   <  typename ODE
   >
const std::vector<typename OdeIntegrator<ODE>::step_t>& OdeIntegrator<ODE>::GetDatabaseT
   (
   )  const
{
   if (  !this->mDriver
      )
   {
      MIDASERROR("Uninitialized OdeDriver!");
   }

   return mDriver->GetDatabaseT();
}

/**
 * Get database y
 *
 * @return
 *    vector of y's for all (interpolated) steps
 **/
template
   <  typename ODE
   >
const std::vector<typename OdeIntegrator<ODE>::vec_t>& OdeIntegrator<ODE>::GetDatabaseY
   (
   )  const
{
   if (  !this->mDriver
      )
   {
      MIDASERROR("Uninitialized OdeDriver!");
   }

   return mDriver->GetDatabaseY();
}

/**
 * Write summary.
 **/
template
   <  typename ODE
   >
void OdeIntegrator<ODE>::Summary
   (
   )  const
{
   if (  !this->mDriver
      )
   {
      MIDASERROR("Uninitialized OdeDriver!");
   }
   else
   {
      this->mDriver->Summary();
   }
}

#endif /* ODEINTEGRATOR_IMPL_H_INCLUDED */
