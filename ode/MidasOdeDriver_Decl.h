/**
************************************************************************
* 
* @file                 MidasOdeDriver_Decl.h
* 
* Created:              28-03-2018 (using code from 28-09-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk) 
* 
* Short Description:    MidasOdeDriver class and function declarations
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASODEDRIVER_DECL_H_INCLUDED
#define MIDASODEDRIVER_DECL_H_INCLUDED

#include "ode/OdeDriverBase.h"
#include "ode/ODE_Macros.h"

/**
 * The MidasCpp ODE driver
 **/
template
   <  typename ODE
   >
class MidasOdeDriver
   :  public OdeDriverBase<ODE>
{
   public:
      //! Aliases
      using Base = OdeDriverBase<ODE>;
      ODE_ALIAS(Base);

   protected:
      using Data = typename Base::Data;

   private:
      //! Max number of failed steps
      In mMaxFailedSteps = I_2;


      //! Overload of integration method
      void IntegrateImpl
         (  vec_t& arY
         )  override;

      //! Overload of type method
      std::string TypeImpl
         (
         )  const override;

   public:
      //! Constructor
      explicit MidasOdeDriver
         (  ODE* apOde
         ,  const midas::ode::OdeInfo& aInfo
         );
};

#endif /* MIDASODEDRIVER_DECL_H_INCLUDED */
