/**
************************************************************************
* 
* @file                TinkerMM.h
*
* Created:             31-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TinkerMM datatype
* 
* Copyright:
*
* Last changes:        C. Koenig (24-11-2014)
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
using std::string;
// Standard Headers
#include <vector>
using std::vector;
#include <map>
using std::map;

#include <iostream> 
using std::ostream;


// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "tinker_interface/TinkerMM.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"

/**
   setup_tinker_ function - parameters:
      
      1) name of parameter file, i.e. mm3.prm
      2) length of parameter file name
      3) XYZ coordinates in a vector with all x coor first, the all y...
      4) the size of the xyz vector
      5) for each atom, the number of connections
      6) the connections
      7) maximum number of connections
**/
extern "C" void setup_tinker_(char*,int*);
extern "C" void get_mm_energy_(double*);
extern "C" void update_tinker_geom_(double*,int*);
//extern "C" void get_mm_gradient_(double*,double*);
//extern "C" double hessian_();

/**
*  default constructor
**/
TinkerMM::TinkerMM() : mGradient(I_0),mHessian(I_0)
{
   mTinkerXyzFile="";
   mParamFile="";
   mEnergy=C_0;
   mNatoms=I_0;
   mDoGradient=false;
   mDoHessian=false;
}

/**
* Construct a definition of a single point
* */
TinkerMM::TinkerMM(const string& aS) : mGradient(I_0),mHessian(I_0)
{
   mTinkerXyzFile="";
   mParamFile=aS;
   mEnergy=C_0;
   mNatoms=I_0;
   mDoGradient=false;
   mDoHessian=false;
}

TinkerMM::~TinkerMM()
{
   mEnergy=C_0;
}

void TinkerMM::Setup(const vector<Nuclei> aStructure)
{
   // 1) move file to midasifc_tinker.xyz
   CopyFile(mTinkerXyzFile,"midasifc_tinker.xyz");
   mNatoms=aStructure.size();
   char file_c[mParamFile.size()+I_1];
   for(In i=0;i<mParamFile.size();i++)
      file_c[i]=mParamFile.at(i);
   file_c[mParamFile.size()]='\0';
   In name_size=mParamFile.size();
   setup_tinker_(file_c,&name_size);
}

void TinkerMM::UpdateGeometry(const vector<Nuclei> aStructure)
{
   if(aStructure.size()!=mNatoms)
      MIDASERROR("TinkerMM::UpdateGeometry: different number of atoms - quitting!");

   double *coord=new double[3*mNatoms];
   for(In i=0;i<mNatoms;i++) {
      coord[i]=aStructure[i].X()*C_TANG;
      coord[mNatoms+i]=aStructure[i].Y()*C_TANG;
      coord[2*mNatoms+i]=aStructure[i].Z()*C_TANG;
   }
   Mout << "Structure:" << endl;
   for(In i=0;i<mNatoms;i++) {
      Mout << coord[i] << "   " << coord[mNatoms+i] << "   " << coord[2*mNatoms+i] << endl;
   }
   In n_coord=3*mNatoms;
   update_tinker_geom_(coord,&n_coord);
}

void TinkerMM::Evaluate()
{
   // first the energy => fortran call
   get_mm_energy_(&mEnergy);
   // energy is returned in kJ/mol => convert to au
   // ck: energies given in KC/mol!
   //mEnergy/=C_AUTKJMOL;
   mEnergy/=C_AUTKCMOL;
   return;
}
