!
!     mbh: fortran function for setting up Tinker
!
      subroutine setup_tinker(par_file,par_file_len,coord);
      include 'tinker_interface.i'
!
      integer :: iunit,par_file_len
      character(par_file_len) :: par_file
      integer :: i,j,idx
!
      iunit=12
      open(unit=iunit,file='midasifc_tinker.key',&
         & form='formatted')

!     write parameters
      write(iunit,'(a11,a)')'parameters ',par_file
      write(iunit,'(a)')'VERBOSE'
!      write(iunit,'(a)')'DEBUG'
!     that's it
      close(iunit)
!     now we may initial
      call initial
      filename='midasifc_tinker'
      leng=15
!     call readxyz
      open(unit=iunit,file='midasifc_tinker.xyz',&
         & form='formatted',status='old',err=800)
      call readxyz(iunit)
      close(iunit)
!     a few more init calls
      call getkey
      call control
      call field
      call mechanic
      return
!
800   continue
      write(*,*)'ERROR READING tinker xyz file (in',&
               &' tinker_interface.f90) - quitting!'
      stop
!
      return
      end

!
!     mbh: fortran function for getting 
!          energy from Tinker
!
      subroutine update_tinker_geom(coord,dim_coord)
      include 'tinker_interface.i'
!
      integer :: dim_coord,i
      double precision :: coord(dim_coord)

      do i=1,dim_coord/3
         x(i)=coord(i)
         y(i)=coord(dim_coord/3+i)
         z(i)=coord(2*(dim_coord/3)+i)
      enddo

      return
      end
!
!     mbh: fortran function for getting 
!          energy from Tinker
!
      subroutine get_mm_energy(e)
      include 'tinker_interface.i'
!
      double precision :: e,energy
      external energy
!     just call energy and we are done
!
      e=energy()
      !write(*,'(a,e22.16)')'Energy: ',e
      return
      end

!
!     mbh: fortran function for getting 
!          gradient from Tinker
!
      subroutine get_mm_grad(e,grad,n_coord)
      include 'tinker_interface.i'
!
      double precision :: grad(3*n_coord)
      double precision :: e
!     call gradient
      call gradient(e,grad)
      return
      end

!
!     mbh: fortran function for getting hessian
!          from Tinker
!
      subroutine get_mm_hess(e)
      include 'tinker_interface.i'
!
      return
      end
