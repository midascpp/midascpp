#====================================================================
#  Auto completion for MidasCpp
#====================================================================

# Helper functions for appending the "opts" and "printf_string" variables
#--------------------------------------------------------------------
__append_opts()
{
   for arg in ${@:1}; do
      if [ -z "$opts" ]; then
         opts+="$arg"
      else
         opts+=" $arg"
      fi
   done
}

__append_printf_string()
{
   for arg in ${@:1}; do
      if [ -z "$printf_string" ]; then
         printf_string+="$arg"
      else
         printf_string+=" $arg"
      fi
   done
}

# Implemenation of completion logic
#--------------------------------------------------------------------
__midascpp_completion_logic()
{
   # Loop over arguments and create command
   local i cmd
   local end=$(expr $COMP_CWORD - 1)
   for i in $(seq 0 $end); do
      # Check not option (starts with '-'), and not file. Everything else is a command.
      if [[ ! ${COMP_WORDS[i]} == -* ]] && [ ! -e ${COMP_WORDS[i]} ]; then
         cmd[i]=${COMP_WORDS[i]}
      fi
      # If we already found a file we do not look for anymore
      if [ -f ${COMP_WORDS[i]} ]; then
         __check_for_files=false
      fi
   done
   
   # Identify set of commands to use for completion
   local j=
   local collected_command=
   local commands=
   local command_options=
   local end_j=$(expr ${#cmd[@]} - 1)
   for j in $(seq 0 $end_j); do
      collected_command+=_${cmd[j]}
      commands=\${__commands$collected_command[@]}
      command_options=\${__commands$collected_command\_opts[@]}
   done
   
   # Try to do autocompletion
   if [[ ${cur} == -* ]] ; then
      __append_printf_string $(compgen -W "${opts} ${command_options}" -- ${cur})
   else
      __append_printf_string $(compgen -W "${commands}" -- ${cur})
      
      if [ "$__check_for_files" == true ]; then
         local list_files=$(compgen -f ${cur})
         # Filter only filenames with the correct extensions, or if no extension all filenames
         local w e
         for w in $list_files; do
            if [ -d "$w" ]; then
               continue
            else
               e="${w##*.}"
               if [ ! -z "$__file_extension" ]; then
                  for ext in $__file_extension; do
                     if [ "$e" = "$ext" ] || [ "$ext" == "all" ]; then
                        __append_printf_string "$w"
                     fi
                  done
               else
                  __append_printf_string "$w"
               fi
            fi
         done
      fi
   fi
   
}

# Setup for 'midasdynlib'
#--------------------------------------------------------------------
__midasdynlib_setup()
{
   __append_opts "--help -h"
   __append_opts "--version -v"

   local __commands_midasdynlib=("dump evaluate")
   local __commands_midasdynlib_evaluate_opts=("--precision --fixed --operator -o")

   local __check_for_files=true
   local __file_extention={"all"}
   
   __midascpp_completion_logic
}

# Setup for 'midastools'
#--------------------------------------------------------------------
__midastools_setup()
{
   __append_opts "--help -h"
   __append_opts "--version -v"

   local __commands_midastools=( "datacont_diff_norm"
                                 "datacont_to_ascii"
                                 "tensordatacont_diff_norm"
                                 "tensordatacont_to_datacont"
                                 "comparevibs"
                                 "autocorr_spectrum"
                                 "mctdh_wf_analysis")

   local __check_for_files=true

   __midascpp_completion_logic
}


# Setup for 'midasutil'
#--------------------------------------------------------------------
__midasutil_setup()
{
   __append_opts "--help -h"
   __append_opts "--version -v"

   local __commands_midasutil=("autocomp vim create")
   local __commands_midasutil_vim=("install")
   local __commands_midasutil_autocomp=("install")
   local __commands_midasutil_create=("source")

   local __check_for_files=false
   
   __midascpp_completion_logic
}

# Setup for 'midascpp'
#--------------------------------------------------------------------
__midascpp_setup()
{
   __append_opts "-v --version"
   __append_opts "-h --help"
   __append_opts "--git-info"
   __append_opts "-q  --quit"      
   __append_opts "-i"              
   __append_opts "-d --maindir"
   __append_opts "-s --scratch"
   __append_opts "-n --numthreads --num-threads"
   __append_opts "--iolevel"
   __append_opts "--seed"
   __append_opts "--debug"
   __append_opts "--numeric-limits"
   __append_opts "--spawn-paradigm"
   __append_opts "--socket"

   local __commands_midascpp=("")

   local __check_for_files=true
   local __file_extension="minp inp"

   __midascpp_completion_logic
}

# Setup for 'midascpp_fork_server'
#--------------------------------------------------------------------
__midascpp_fork_server_setup()
{
   __append_opts "-v --verbose"
   __append_opts "-h --help"
   __append_opts "-s --socket"
   __append_opts "-n --num-threads"

   local __commands_midascpp_fork_server=("")

   local __check_for_files=false

   __midascpp_completion_logic
}

# General autocompletion function
#--------------------------------------------------------------------
_midascpp_completion()
{
   local cur prev opts exit_code printf_format printf_string
   exit_code=0
   printf_format="%s"
   printf_string=""
   COMPREPLY=()
   cur="${COMP_WORDS[COMP_CWORD]}"
   prev="${COMP_WORDS[COMP_CWORD-1]}"
   opts=""
   
   # General options for all executables using the 'midascpp executable wrapper'
   __append_opts "--dump-args --dump-ldd --dump-pid --exit-wrapper --use-valgrind --valgrind-options --use-gdb --pre-command"

   # Run setup and completion logic
   __$1_setup
   
   # Save reply
   COMPREPLY=( $(printf "$printf_format" "$printf_string") )

   # Return 
   return $exit_code
}

# Wrappers for 'midastools', 'midasutil', and 'midascpp'
#--------------------------------------------------------------------
_midasdynlib() 
{
   _midascpp_completion "midasdynlib"
}

_midastools() 
{
   _midascpp_completion "midastools"
}

_midasutil() 
{
   _midascpp_completion "midasutil"
}

_midascpp() 
{
   _midascpp_completion "midascpp"
}

_midascpp_fork_server()
{
   _midascpp_completion "midascpp_fork_server"
}

# Set functions to use for auto-completion
#--------------------------------------------------------------------
complete -F _midasdynlib midasdynlib
complete -F _midastools midastools
complete -F _midasutil midasutil
complete -F _midascpp midascpp
