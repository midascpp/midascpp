/**
 *******************************************************************************
 * 
 * @file    TrfZeroConstraint_Decl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFZEROCONSTRAINT_DECL_H_INCLUDED
#define TRFZEROCONSTRAINT_DECL_H_INCLUDED

namespace midas::tdvcc
{

   /**
    * Transformer for g=0. 
    * Doesn't really do anything since it does not have to calculate g matrices and transform with them.
    * These operations are skipped in Tdmvcc::DerivativeImpl based on the zero_g member.
    **/
   template
      <  typename PARAM_T
      >
   class TrfZeroConstraint
   {
      public:
         //! g=0
         inline static constexpr bool zero_g = true;

         //! Not variational
         inline static constexpr bool is_variational = false;

         //! T1 is not expected to be zero
         constexpr bool T1Zero() const { return false; }
      
         //! L1 is not expected to be zero
         constexpr bool L1Zero() const { return false; }

      private:
   };

}; /* namespace midas::tdvcc */

#endif /* TRFZEROCONSTRAINT_DECL_H_INCLUDED */
