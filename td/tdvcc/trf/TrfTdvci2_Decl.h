/**
 *******************************************************************************
 * 
 * @file    TrfTdvci2_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCI2_DECL_H_INCLUDED
#define TRFTDVCI2_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "vcc/vcc2/TwoModeIntermeds.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdvci2
      :  public TrfTdvccBase<PARAM_T>
   {
      public:
         using typename TrfTdvccBase<PARAM_T>::param_t;
         using typename TrfTdvccBase<PARAM_T>::step_t;
         using typename TrfTdvccBase<PARAM_T>::opdef_t;
         using typename TrfTdvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTdvccBase<PARAM_T>::n_modals_t;
         template<typename U> using cont_tmpl = GeneralDataCont<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr TrfType trf_type = TrfType::VCC2H2;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTdvci2(Args&&... args)
            :  TrfTdvccBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         cont_t Transform(step_t aTime, const cont_t& arCoefs) const;

         inline cont_t ExpValTrf(step_t aTime, const cont_t& arAmpls) const {return Transform(aTime, arAmpls);}

         //! For now, there are no intermediates to clear.
         void ClearIntermeds() const {}

   };

} /* namespace midas::tdvcc */


#endif/*TRFTDVCI2_DECL_H_INCLUDED*/
