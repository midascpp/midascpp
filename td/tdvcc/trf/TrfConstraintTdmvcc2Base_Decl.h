/**
 *******************************************************************************
 * 
 * @file    TrfConstraintTdmvcc2Base_Decl.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTTDMVCC2BASE_DECL_H_INCLUDED
#define TRFCONSTRAINTTDMVCC2BASE_DECL_H_INCLUDED

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/oper/LinCombOper.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/trf/TrfConstraintBase.h"
#include "td/tdvcc/trf/ConstraintOpDef.h"
#include "vcc/ModalIntegrals.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfConstraintTdmvcc2Base
      :  public TrfConstraintBase<PARAM_T>
   {
      public:
         //! Alias
         using Base = TrfConstraintBase<PARAM_T>;
         using typename Base::param_t;
         using typename Base::step_t;
         using typename Base::mat_t;
         using typename Base::gmats_t;
         using typename Base::dmats_t;
         using typename Base::modals_t;
         using typename Base::modals_pair_t;
         using typename Base::n_modals_t;
         using modalintegrals_t = ModalIntegrals<param_t>;
         using g_opdef_t = constraint::ConstraintOpDef;

         template<typename T> using cont_tmpl = GeneralDataCont<T>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr TrfType trf_type = TrfType::VCC2H2;
         using intermeds_t = TwoModeIntermeds<param_t, g_opdef_t>;

         //! c-tor
         TrfConstraintTdmvcc2Base
            (  const n_modals_t& aNModals
            ,  const ModeCombiOpRange& aMcr
            )
            :  TrfConstraintBase<PARAM_T>
                  (  aNModals
                  ,  aMcr
                  )
            ,  mGOpDef(aMcr.NumberOfModes())
         {
         }

         /** Transformations with linear combination of one-mode operators **/
         //!@{
         cont_t ErrVec(step_t aTime, const cont_t& arAmpls, bool aSaveIntermeds=true) const;
         cont_t EtaVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t LJac(step_t aTime, const cont_t& arAmpls, const cont_t& arCoefs, bool aReuseIntermeds=true) const;
         //!@}

         //! Update g integrals from vector of matrices
         void UpdateIntegrals
            (  const gmats_t& aGMats
            );

         //! Clear intermediates
         void ClearIntermeds() { mIntermeds = nullptr; }
      
      protected:
         //! Get ModalIntegrals
         const auto& ModInts() const { return mModalIntegrals; }

         //! Get GOpDef
         const auto& GetGOpDef() const { return mGOpDef; }

      private:
         //! g integrals (wrapped in ModalIntegrals class)
         modalintegrals_t mModalIntegrals;

         //! g OpDef
         g_opdef_t mGOpDef;

         //! Intermeds saved from ErrVec calculation
         mutable std::unique_ptr<intermeds_t> mIntermeds = nullptr;

   };

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTTDMVCC2BASE_DECL_H_INCLUDED */

