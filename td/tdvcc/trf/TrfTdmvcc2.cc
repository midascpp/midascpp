/**
 *******************************************************************************
 * 
 * @file    TrfTdmvcc2.cc
 * @date    06-10-2020
 * @author  Andreas Buchgraitz Jensen ()
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvcc2_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instantiation macro.
#define INSTANTIATE_TRFTDMVCC2(PARAM_T) \
   namespace midas::tdvcc \
   {  \
      template class TrfTdmvcc2<PARAM_T>; \
   }  /* namespace midas::tdvcc */ \


// Instantiations.
INSTANTIATE_TRFTDMVCC2(Nb);
INSTANTIATE_TRFTDMVCC2(std::complex<Nb>);

#undef INSTANTIATE_TRFTDMVCC2
#endif/*DISABLE_PRECOMPILED_TEMPLATE*/