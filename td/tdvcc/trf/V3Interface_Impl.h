/**
 *******************************************************************************
 * 
 * @file    V3Interface_Impl.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef V3INTERFACE_IMPL_H_INCLUDED
#define V3INTERFACE_IMPL_H_INCLUDED

#include "mmv/MidasVector.h"

#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "vcc/v3/Scaling.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/IntermedProd.h"
#include "vcc/v3/IntermedProdL.h"
#include "vcc/v3/IntermediateMachine.h"

#include "TimTdvccEvalData.h"

#include "mpi/Impi.h"


namespace midas::tdvcc::detail
{

/**
 * Read in .v3c file with terms for transformation.
 *
 * @note
 *    This _always_ overwrites the current contribs, so only call this if necessary!
 *
 * @param aType
 * @param aMaxExci
 * @param aOperMcLevel
 * @param arIntermeds      Intermediates corresponding to aType
 **/
template
   <  typename PARAM_T
   >
typename V3Interface<PARAM_T>::contribs_t V3Interface<PARAM_T>::InitializeV3
   (  V3Type aType
   ,  In aMaxExci
   ,  In aOperMcLevel
   ,  intermeds_t& arIntermeds
   )  const
{
   // Initialize new contribs and get reference
   contribs_t contribs;

   std::string method = "TIM-TDVCC[" + std::to_string(aMaxExci) + "]";
   if (  this->IoLevel() >= I_7
      )
   {
      Mout  << " Initializing V3Contribs for " << method << " with " << aOperMcLevel << "M operator." << std::endl;
   }

   // Generate name of data file.
   std::ostringstream ss_v3c_file;
   if (  aType == V3Type::VCI
      )
   {
      ss_v3c_file << "VCI";
   }
   else
   {
      ss_v3c_file << "VCC";
   }
   ss_v3c_file << "[" << aMaxExci << "]_H" << aOperMcLevel;
   switch   (  aType
            )
   {
      case V3Type::ERRVEC:
      {
         ss_v3c_file << "-T1.v3c";
         break;
      }
      case V3Type::ETAVEC:
      {
         ss_v3c_file << "-T1_eta0.v3c";
         break;
      }
      case V3Type::LJAC:
      {
         ss_v3c_file << "-T1_JacobianL.v3c";
         break;
      }
      case V3Type::VCI:
      {
         ss_v3c_file << ".v3c";
         break;
      }
      default:
      {
         MIDASERROR("Unspecified V3 transformation type!");
      }
   }

   std::string v3c_file = this->mV3cFilePrefix + ss_v3c_file.str();
 
   // If not found, generate contributions from scratch
   if (  !v3contrib_t::ReadV3cFile(v3c_file, contribs)
      )
   {
      MidasWarning("v3c file '" + v3c_file +"' not found. Will generate contribs from scratch.");
      contribs = this->GenerateV3cFile(aType, aMaxExci, aOperMcLevel, v3c_file);
   }

   // Identify combined intermediates
   bool cp_tensors = false;
   midas::vcc::v3::Scaling init_scaling, max_sc;
   for(auto& contrib : contribs)
   {
      if (  auto* ip = dynamic_cast<intermedprod_t*>(contrib.get())
         )
      {
         auto sc = ip->GetScaling(cp_tensors);
         if (  sc > init_scaling
            )
         {
            init_scaling = sc;
         }
         ip->IdentifyCmbIntermeds(sc, cp_tensors);
         if (  sc > max_sc
            )
         {
            max_sc = sc;
         }
      }
      // For left-hand transformations.
      else if  (  auto* ipl = dynamic_cast<intermedprodl_t*>(contrib.get())
               )
      {
         auto sc = ipl->GetScaling(cp_tensors);
         if (  sc > init_scaling
            )
         {
            init_scaling = sc;
         }
         ipl->IdentifyCmbIntermeds(sc, cp_tensors);
         if (  sc > max_sc
            )
         {
            max_sc = sc;
         }
      }
   }

   if (  this->IoLevel() >= I_10
      )
   {
      Mout  << "    Max scaling before: " << init_scaling << "\n"
            << "    Max scaling after:  " << max_sc << " (including intermediates)." << "\n\n"
            << std::flush;
   }
   
   // Register intermediates
   arIntermeds.Reset();
   for(auto& contrib : contribs)
   {
      if (  auto* ip = dynamic_cast<intermedprod_t*>(contrib.get())
         )
      {
         ip->RegisterIntermeds(arIntermeds);
      }
      else if  (  auto* ipl = dynamic_cast<intermedprodl_t*>(contrib.get())
               )
      {
         ipl->RegisterIntermeds(arIntermeds);
      }
   }

   // Return
   return contribs;
}

/**
 * Generate v3c file from scratch.
 *
 * @note
 *    This duplicates a lot from TransformerV3, which could be separated out as functions in vcc_cpg!
 *
 * @param aType
 * @param aMaxExci
 * @param aOperMcLevel
 * @param aFileName
 * @return
 *    Vector of contributions
 **/
template
   <  typename PARAM_T
   >
typename V3Interface<PARAM_T>::contribs_t V3Interface<PARAM_T>::GenerateV3cFile
   (  V3Type aType
   ,  In aMaxExci
   ,  In aOperMcLevel
   ,  const std::string& aFileName
   )  const
{
   // Init result
   contribs_t result;

   // Aux function for emplacing to result
   auto emplace_contribs = [](const std::vector<CtrProd>& aProds, contribs_t& arContribs)
   {
      for(const auto& p : aProds)
      {
         std::ostringstream os;
         p.WriteV3ContribSpec(os);
         arContribs.emplace_back(v3contrib_t::Factory(os.str()));
      }
   };

   // Generate list of contraction products.
   std::vector<CtrProd> ctr_prods;
   switch   (  aType
            )
   {
      case V3Type::ERRVEC:
      {
         In min_exci = I_2;      // Because we use T1-transformed Hamiltonian
         for(In h_dim=I_1; h_dim<=aOperMcLevel; ++h_dim)
         {
            Commutator h_oper;
            h_oper.SetCoef(C_1);
            h_oper.AddOper(OP::H, h_dim);
            std::vector<Commutator> bch;
            h_oper.BCH(min_exci, aMaxExci, aMaxExci, bch);
            for(const auto& term : bch)
            {
               term.GetCtrProds(I_0, aMaxExci, ctr_prods, true);
            }
         }
         break;
      }
      case V3Type::ETAVEC:
      {
         // Generate list of contraction products for 'H tau exp(T)'
         In min_exci = I_2;      // Because we use T1-transformed Hamiltonian
         for(In h_dim=I_1; h_dim<=aOperMcLevel; ++h_dim)
         {
            for(In e_lvl=1; e_lvl<=aMaxExci; ++e_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::E, e_lvl);
               std::vector<Commutator> bch;
               cmt.BCH(min_exci, aMaxExci, I_0, bch);
               for(const auto& term : bch)
               {
                  term.GetCtrProds(I_0, I_0, ctr_prods);
               }
            }
         }
         break;
      }
      case V3Type::LJAC:
      {
         // Generate list of contraction products for [H,R1], [H,R2], [H,R3], ...
         In min_exci = I_2;      // Because we use T1-transformed Hamiltonian
         for(In h_dim=I_1; h_dim<=aOperMcLevel; ++h_dim)
         {
            for(In e_lvl=I_1; e_lvl<=aMaxExci; ++e_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::E, e_lvl);
               vector<Commutator> bch;
               cmt.BCH(min_exci, aMaxExci, aMaxExci, bch);
               for(const auto& term : bch)
               {
                  term.GetCtrProds(I_1, aMaxExci, ctr_prods, true);
               }
            }
         }
         break;
      }
      case V3Type::VCI:
      {
         // Generate list of contraction products.
         std::vector<BasicOperProd> bops;
         BasicOperProd::GenerateVci(aOperMcLevel, aMaxExci, bops);
         for(const auto& bop : bops)
         {
            bop.GetCtrProds(I_0, aMaxExci, ctr_prods);
         }
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized V3Type!");
      }
   }

   // Emplace to result and write file
   emplace_contribs(ctr_prods, result);
   intermedprod_t::WriteV3cFile(aFileName, result);

   // Return result
   return result;
}


/**
 * Evaluate contribs
 *
 * @note
 *    The result includes the reference MC, which contains the phase derivative.
 *
 * @param aContribs
 * @param aEvalData
 * @return
 *    Transformed vector
 **/
template
   <  typename PARAM_T
   >
typename V3Interface<PARAM_T>::cont_t V3Interface<PARAM_T>::EvaluateContribs
   (  const contribs_t& aContribs
   ,  const evaldata_t& aEvalData
   )  const
{
   if (  aContribs.empty()
      )
   {
      MIDASERROR("EvaluateContribs called for empty contribs_t. Have the V3 contributions been initialized?");
   }

   const auto& mcr = aEvalData.GetModeCombiOpRange();

   cont_t result(mcr, aEvalData.GetNModals(), true);     // NB: With reference to get phase derivative!

   if constexpr(MPI_DEBUG)
   {
      std::ostringstream oss;
      oss   << " V3Interface::EvaluateContribs initialized result vector:\n" << result;
      midas::mpi::WriteToLog(oss.str());
   }

#ifdef VAR_MPI
   auto nproc = midas::mpi::GlobalSize();
#endif /* VAR_MPI */

   auto nmcs = mcr.Size();

   for(In imc=I_0; imc<nmcs; ++imc)
   {
#ifdef VAR_MPI
      // Skip if another rank is doing this MC
      if (  midas::mpi::GlobalRank() != imc % nproc
         ) 
      {
         if constexpr(MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Skip MC " + std::to_string(imc) + " for rank " + std::to_string(midas::mpi::GlobalRank()));
         }
         continue;
      }
#endif /* VAR_MPI */

      // Get ModeCombi
      const auto& mc = mcr.GetModeCombi(imc);

      // Construct TensorSumAccumulator
      midas::vcc::TensorSumAccumulator<param_t> res_vec(aEvalData.GetTensorDims(imc)); 
      if constexpr(MPI_DEBUG)
      {
         std::ostringstream oss;
         oss   << " TensorSumAccumulator before evaluation for MC " << mc << "\n"
               << res_vec.Tensor();
         midas::mpi::WriteToLog(oss.str());
      }

      // loop over contributions
      for(const auto& c : aContribs)
      {
         // check that modecombi has same number of modes as intermed prod returns
         if (  mc.Size() != c->ExciLevel()
            ) 
         {
            continue;
         }
         
         // then evaluate contribution for current MC
         c->Evaluate(aEvalData, mc, res_vec);
      }

      if constexpr(MPI_DEBUG)
      {
         std::ostringstream oss;
         oss   << " TensorSumAccumulator after evaluation for MC " << mc << "\n"
               << res_vec.Tensor();
         midas::mpi::WriteToLog(oss.str());
      }

      auto& tens_out = result.GetModeCombiData(imc);

      // When all contributions are evaluated we save the result
      if (  mc.Empty()
         )
      {
         param_t scalar;
         res_vec.EvaluateSum().GetTensor()->DumpInto(&scalar);
         tens_out.ElementwiseScalarAddition(scalar);
      }
      else
      {
         tens_out = res_vec.EvaluateSum();
      }
   }

#ifdef VAR_MPI
   // Bcast data
   for(In imc=I_0; imc<nmcs; ++imc)
   {
      auto rank = imc % nproc;

      if constexpr(MPI_DEBUG)
      {
         if (  midas::mpi::GlobalRank() == rank
            && result.GetModeCombiData(imc).IsNullPtr()
            )
         {
            MIDASERROR("Result is nullptr on the MPI rank we are broadcasting from!");
         }
      }

      result.GetModeCombiData(imc).MpiBcast(rank);
   }

   if constexpr   (  MPI_DEBUG
                  ) 
   {
      auto norm = result.Norm();
      midas::mpi::detail::WRAP_Bcast(&norm, 1, midas::mpi::DataTypeTrait<decltype(norm)>::Get(), 0, MPI_COMM_WORLD);

      if (  norm != result.Norm()
         )
      {
         MIDASERROR("Did not receive correct result vector in transformer.");
      }
   }
#endif /* VAR_MPI */

   // Return
   return result;
}


/**
 * Extract one-mode amplitudes for T1 transformation.
 *
 * @note
 *    Each part of the vector will have size NModals(m) with the first element equal to zero.
 *
 * @param aAmplitudes      The cluster amplitudes
 * @param aNModals         Number of modals per mode
 * @return
 *    Singles amplitudes in a vector
 **/
template
   <  typename PARAM_T
   >
std::vector<typename V3Interface<PARAM_T>::midasvector_t> V3Interface<PARAM_T>::ExtractOneModeAmplitudes
   (  const cont_t& aAmplitudes
   ,  const std::vector<Uin>& aNModals
   )  const
{
   using ret_t = std::vector<midasvector_t>;

   const auto& nmodals = aNModals;

   ret_t result(nmodals.size());
   for(In i=I_0; i<nmodals.size(); ++i)
   {
      result[i].SetNewSize(nmodals[i]);
      result[i].Zero();
   }

   auto ampl_size = aAmplitudes.Size();
   size_t imode=0;
   for(In imc=I_0; imc<ampl_size; ++imc)
   {
      const auto& tens = aAmplitudes.GetModeCombiData(imc);
      const auto& dims = tens.GetDims();

      if (  dims.size() == 1
         )
      {
         auto* data = tens.template StaticCast<SimpleTensor<param_t>>().GetData();
         auto len = dims[0];

         // Set the remaining elements
         for(In i=I_0; i<len; ++i)
         {
            result[imode][i+1] = data[i];
         }

         ++imode;
      }
      else if  (  dims.size() > 1
               )
      {
         break;
      }
   }

   return result;
}

} /* namespace midas::tdvcc::detail */

#endif /* V3INTERFACE_IMPL_H_INCLUDED */
