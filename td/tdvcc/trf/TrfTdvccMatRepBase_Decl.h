/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRepBase_Decl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCMATREPBASE_DECL_H_INCLUDED
#define TRFTDVCCMATREPBASE_DECL_H_INCLUDED

// Standard headers.
#include <vector>
#include <type_traits>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "util/matrep/OperMat.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;


namespace midas::tdvcc
{

   /************************************************************************//**
    * Base class for MatRep transformers. Templated on BASETRF which allows 
    * inserting the TrfTdmvccBase class between this and TrfTdvccBase to get access 
    * to time-dependent modal integrals, etc.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   class TrfTdvccMatRepBase
      :  public BASETRF_TMPL<PARAM_T>
   {
      public:
         using typename BASETRF_TMPL<PARAM_T>::param_t;
         using typename BASETRF_TMPL<PARAM_T>::step_t;
         using typename BASETRF_TMPL<PARAM_T>::opdef_t;
         using typename BASETRF_TMPL<PARAM_T>::modalintegrals_t;
         using typename BASETRF_TMPL<PARAM_T>::n_modals_t;
         template<typename U> using cont_tmpl = GeneralMidasVector<U>;
         using cont_t = cont_tmpl<param_t>;
         using mat_t = midas::util::matrep::OperMat<param_t>;
         using cl_oper_t = midas::util::matrep::SparseClusterOper;
         using looper_t = midas::util::matrep::ShiftOperBraketLooper;
         using mat_v_t = std::vector<GeneralMidasMatrix<param_t>>;
         static inline constexpr TrfType trf_type = TrfType::MATREP;

         //! Forward all constructor calls to base class.
         template
            <  typename... Args
            ,  typename std::enable_if_t
                  <  sizeof...(Args) == 4
                  && std::is_same_v
                        <  BASETRF_TMPL<PARAM_T>
                        ,  TrfTdvccBase<PARAM_T>
                        >
                  ,  bool
                  >  = true
            >
         TrfTdvccMatRepBase(Args&&... args)
            :  BASETRF_TMPL<PARAM_T>(std::forward<Args>(args)...)
            ,  mOperMat(std::make_unique<mat_t>(this->NModals(), this->Oper(), this->ModInts(), true))
            ,  mShiftOperBraketLooper(this->NModals(), this->Mcr())
            ,  mSparseClusterOper(this->ConstructSparseClusterOper(this->NModals(), this->Mcr()))
         {
         }
         template
            <  typename... Args
            ,  typename std::enable_if_t
                  <  sizeof...(Args) == 5
                  && std::is_same_v
                     <  BASETRF_TMPL<PARAM_T>
                     ,  TrfTdmvccBase<PARAM_T>
                     >
                  ,  bool
                  > = false
            >
         TrfTdvccMatRepBase(Args&&... args)
            :  BASETRF_TMPL<PARAM_T>(std::forward<Args>(args)...)
            ,  mShiftOperBraketLooper(this->NModals(), this->Mcr())
            ,  mSparseClusterOper(this->ConstructSparseClusterOper(this->NModals(), this->Mcr()))
         {
         }

         const mat_t& OperMat() const {return *mOperMat;}

      protected:
         const looper_t& ShiftOperBraketLooper() const {return mShiftOperBraketLooper;}
         const cl_oper_t& SparseClusterOper() const {return mSparseClusterOper;}
         void PrintAnalysisAndTimings() const;
         void PrintScoOptAnalysis() const;
         void PrintOperMatCtorTiming() const;

      private:
         std::map<std::string,Nb> opermat_ctor_timing = {{"CPU", 0.}, {"Wall", 0.}};
         std::map<std::string,Nb> sco_ctor_timing = {{"CPU", 0.}, {"Wall", 0.}};
         std::unique_ptr<mat_t> mOperMat = nullptr;
         looper_t mShiftOperBraketLooper;
         cl_oper_t mSparseClusterOper;
         mutable bool has_printed_sco_opt_anal = false;
         mutable bool has_printed_opermat_timing = false;

         cl_oper_t ConstructSparseClusterOper
            (  const n_modals_t& arNModals
            ,  const ModeCombiOpRange& arMcr
            );

         virtual void UpdateIntegralsImpl
            (
            )  override;

   };

} /* namespace midas::tdvcc */


#endif/*TRFTDVCCMATREPBASE_DECL_H_INCLUDED*/
