/**
 *******************************************************************************
 * 
 * @file    TrfConstraintBase_Decl.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTBASE_DECL_H_INCLUDED
#define TRFCONSTRAINTBASE_DECL_H_INCLUDED

// Std headers
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "td/tdvcc/TdvccEnums.h"
#include "input/ModeCombiOpRange.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfConstraintBase
   {
      public:
         //! Alias
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using real_t = step_t;
         using vec_t = GeneralMidasVector<param_t>;
         using mat_t = GeneralMidasMatrix<param_t>;
         using gmats_t = std::vector<mat_t>;
         using dmats_t = std::vector<mat_t>;
         using modals_t = mat_t;
         using modals_pair_t = std::pair<modals_t, modals_t>;
         using n_modals_t = std::vector<Uin>;
         using regularization_t = std::pair<RegInverseType, real_t>;

         //! c-tor
         TrfConstraintBase
            (  const n_modals_t& aNModals
            ,  const ModeCombiOpRange& aMcr
            )
            :  mrNModals(aNModals)
            ,  mrMcr(aMcr)
         {
            if (  this->Mcr().NumberOfModes() != this->NModals().size()
               )
            {
               MIDASERROR
                  (  "Number of modes mismatch in TrfConstraintMatRepBase c-tor: "
                  +  std::to_string(this->Mcr().NumberOfModes())
                  +  " vs. "
                  +  std::to_string(this->NModals().size())
                  +  "."
                  );
            }
         }

         /** Transformations with linear combination of one-mode operators **/
         //!@{
         modals_pair_t ActiveSpaceModalDerivative
            (  step_t aTime
            ,  const mat_t& aG
            ,  const modals_pair_t& aModals
            ,  PARAM_T aKetFactor
            ,  PARAM_T aBraFactor
            )  const;
         //!@}
         
         /** Queries **/
         //!@{
         const auto& NModals() const { return mrNModals; }
         const auto& Mcr() const { return mrMcr; }
         auto NModes() const { return this->Mcr().NumberOfModes(); }

         auto& IoLevel() { return mIoLevel; }
         const auto& IoLevel() const { return mIoLevel; }

         regularization_t& GOptRegularization() { return this->mGOptRegularization; }
         const regularization_t& GOptRegularization() const { return this->mGOptRegularization; }
         //!@}

      protected:
         //! Construct g matrices from vectors of up and down
         std::vector<mat_t> ConstructGMatrices
            (  const std::vector<vec_t>& aGDown
            ,  const std::vector<vec_t>& aGUp = {}
            )  const;

      private:
         //! Number of modals
         const n_modals_t& mrNModals;

         //! MCR
         const ModeCombiOpRange& mrMcr;

         //! IoLevel
         Uin mIoLevel = 0;

         //! Regularization type for solving g equations
         regularization_t mGOptRegularization = {RegInverseType::XSVD, static_cast<real_t>(1.e-8)};
   };

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTBASE_DECL_H_INCLUDED */