/**
 *******************************************************************************
 * 
 * @file    TrfZeroConstraint.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFZEROCONSTRAINT_H_INCLUDED
#define TRFZEROCONSTRAINT_H_INCLUDED

#include "td/tdvcc/trf/TrfZeroConstraint_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/trf/TrfZeroConstraint_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif /* TRFZEROCONSTRAINT_H_INCLUDED */
