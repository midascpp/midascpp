/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccBase_Impl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCCBASE_IMPL_H_INCLUDED
#define TRFTDMVCCBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/Io.h"
#include "lapack_interface/GESV.h"
#include "util/RegularizedInverse.h"
#include "util/matrep/MatRepTransformers.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   TrfTdmvccBase<PARAM_T>::TrfTdmvccBase
      (  const n_modals_t& arNModalsTdBas
      ,  const n_modals_t& arNModalsPrimBas
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  TrfTdvccBase<PARAM_T>
         (  arNModalsTdBas
         ,  arOpDef
         ,  arModInts
         ,  arMcr
         )
      ,  mrNModalsPrimBas(arNModalsPrimBas)
   {
      this->SanityCheck();
   }

   /************************************************************************//**
    * Checks:
    *    -  Primitive basis dims. must be larger than time-dep. basis dims.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdmvccBase<PARAM_T>::SanityCheck
      (
      )  const
   {
      std::stringstream ss;
      const auto& nm_pr = this->NModalsPrimBas();
      const auto& nm_td = this->NModalsTdBas();
      if (nm_pr.size() != nm_td.size())
      {
         ss << "Size mismatch;\n"
            << "   nm_pr.size() = " << nm_pr.size() << '\n'
            << "   nm_td.size() = " << nm_td.size() << '\n'
            ;
         MIDASERROR(ss.str());
      }
      Uin m = 0;
      for(  auto it_pr = nm_pr.cbegin(), it_td = nm_td.cbegin()
         ;  it_pr != nm_pr.cend() && it_td != nm_td.cend()
         ;  ++it_pr, ++it_td, ++m
         )
      {
         if (*it_td > *it_pr)
         {
            ss << "Prim.basis dim. < td.basis dim. for m = " << m << ".\n"
               << "   nm_pr = " << nm_pr << '\n'
               << "   nm_td = " << nm_td << '\n'
               ;
            MIDASERROR(ss.str());
         }
      }
   }

   /****************************************************************************
    * Update modal integrals
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdmvccBase<PARAM_T>::UpdateIntegrals
      (  const modal_cont_t& aModals
      )
   {
      auto [mi_td, mi_prim_td, mi_td_prim] = midas::util::matrep::TdmvccHalfAndFullyTransformedModalIntegrals(this->ModInts(), aModals);
      this->mTdModalIntegrals = modalintegrals_t(std::move(mi_td));
      this->mHalfTransModalIntegrals = halftransints_t{ std::move(mi_prim_td), std::move(mi_td_prim) };

      // Update H matrix for matrep, perhaps do other things for other derived classes.
      this->UpdateIntegralsImpl();
   }

   /****************************************************************************
    * Base expression for secondary-space part of modal derivatives, i.e. without the g terms.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvccBase<PARAM_T>::modal_cont_t
   TrfTdmvccBase<PARAM_T>::SecondarySpaceModalDerivative
      (  step_t aTime
      ,  const meanfields_t& aMeanFields
      ,  const mat_v_t& aInvDensMats
      ,  const mat_v_t& aProjectors
      ,  PARAM_T aKetFactor
      ,  PARAM_T aBraFactor
      )  const
   {
      auto nmodes = this->NModes();

      auto check = [&nmodes](const std::string& s, const auto& v)
      {
         if (  v.size() != nmodes
            )
         {
            MIDASERROR(" Wrong size of " + s + "(" + std::to_string(v.size()) + " should be " + std::to_string(nmodes) + ").");
         }
      };
      check("mean fields", aMeanFields);
      check("inverse density matrices", aInvDensMats);
      check("projectors", aProjectors);

      modal_cont_t uw_dot(nmodes);

      for(In imode=I_0; imode<nmodes; ++imode)
      {
         auto& u_dot = uw_dot[imode].first;
         auto& w_dot = uw_dot[imode].second;

         // Set to half-transformed residual mean fields transformed with inverse density matrix
         mat_t sigma_u = aMeanFields[imode][1] * aInvDensMats[imode];
         mat_t sigma_w = aInvDensMats[imode] * aMeanFields[imode][3];
         
         // Add one-mode part
         sigma_u += aMeanFields[imode][0];
         sigma_w += aMeanFields[imode][2];

         // Scale
         sigma_u.Scale(aKetFactor);
         sigma_w.Scale(aBraFactor);

         // Project and set results
         u_dot = mat_t(aProjectors[imode] * sigma_u);
         w_dot = mat_t(sigma_w * aProjectors[imode]);
      }

      return uw_dot;
   }

   /****************************************************************************
    * Invert density matrices (using regularization)
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvccBase<PARAM_T>::mat_v_t
   TrfTdmvccBase<PARAM_T>::InverseDensityMatrices
      (  const mat_v_t& aDensMats
      ,  RegInverseType aRegType
      ,  real_t aEpsilon
      ,  bool aVerbose
      )
   {
      auto nmodes = aDensMats.size();
      mat_v_t inv_dens(nmodes);

      for(In imode=I_0; imode<nmodes; ++imode)
      {
         if (  aVerbose
            )
         {
            Mout  << " Construct regularized inverse density matrix for mode " << imode << std::endl;
         }
         switch   (  aRegType
                  )
         {
            case RegInverseType::SVD:
               inv_dens[imode] = midas::math::RegularizedInverse(midas::math::inverse::svd, aDensMats[imode], aEpsilon, aVerbose);
               break;
            case RegInverseType::XSVD:
               inv_dens[imode] = midas::math::RegularizedInverse(midas::math::inverse::xsvd, aDensMats[imode], aEpsilon, aVerbose);
               break;
            case RegInverseType::TIKHONOV:
               inv_dens[imode] = midas::math::RegularizedInverse(midas::math::inverse::svd_tikhonov, aDensMats[imode], aEpsilon, aVerbose);
               break;
            default:
               MIDASERROR("Regularized-inverse type not implemented!");
         }
      }
      if (aVerbose) Mout << std::endl;

      return inv_dens;
   }

   /****************************************************************************
    * Secondary-space projector: Q^m = 1^m - P^m
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvccBase<PARAM_T>::mat_v_t
   TrfTdmvccBase<PARAM_T>::SecondarySpaceProjector
      (  const modal_cont_t& aModals
      ,  bool aNonBiOrtho
      ,  NonOrthoProjectorType aNonOrthoProjectorType
      ,  bool aVerbose
      )
   {
      auto nmodes = aModals.size();
      mat_v_t q(nmodes);

      if (aVerbose) Mout << " Calculate secondary space projectors " << std::endl;
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         auto& qm = q[imode];
         auto nprim = aModals[imode].first.Nrows();

         // Set size of matrix
         qm.SetNewSize(nprim);

         // Set to unit matrix
         qm.Unit();

         // Subtract P^m
         qm -= TrfTdmvccBase<PARAM_T>::ActiveSpaceProjector(aModals, imode, aNonBiOrtho, aNonOrthoProjectorType, aVerbose);
      }
      if (aVerbose) Mout << std::endl;

      return q;
   }

   /****************************************************************************
    * Active-space projector: P^m = U^m W^m (or U^m [W^m U^m]^{-1} W^m in non-biorthogonal case)
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvccBase<PARAM_T>::mat_t
   TrfTdmvccBase<PARAM_T>::ActiveSpaceProjector
      (  const modal_cont_t& aModals
      ,  In aMode
      ,  bool aNonBiOrtho
      ,  NonOrthoProjectorType aNonOrthoProjectorType
      ,  bool aVerbose
      )
   {
      const auto& um = aModals[aMode].first;
      const auto& wm = aModals[aMode].second;

      if (  aNonBiOrtho
         && aNonOrthoProjectorType == NonOrthoProjectorType::ORIGINALBYUW
         )
      {
         // Construct overlap and solve linear equations to invert it
         auto sm = mat_t(wm * um);
         auto lin_sol = GESV(sm, wm);

         // Load solution
         mat_t sinv_wm;
         LoadSolution(lin_sol, sinv_wm);

         if (  aVerbose
            )
         {
            mat_t nort = um * sinv_wm;
            mat_t ort = um * wm;
            Mout  << " Mode " << aMode << " || P_non-ortho - P_ortho || = " << std::sqrt(DiffNorm2(nort, ort)) << std::endl;
            return nort;
         }
         else
         {
            // Return U^m times solution to linear equations
            return mat_t(um * sinv_wm);
         }
      }
      else if  (  aNonBiOrtho
               && aNonOrthoProjectorType == NonOrthoProjectorType::MOOREPENROSETYPEBYU
               )
      {
         // abj OBS: Conjugate() is inefficient as it stores entire new matrix.
         auto ud_u = mat_t(um.Conjugate() * um);
         auto lin_sol = GESV(ud_u, um.Conjugate());

         // Load solution
         mat_t uduinv_ud;
         LoadSolution(lin_sol, uduinv_ud);
         return mat_t(um * uduinv_ud);
      }
      else
      {
         // Just return U^m * W^m
         return mat_t(um * wm);
      }
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDMVCCBASE_IMPL_H_INCLUDED*/
