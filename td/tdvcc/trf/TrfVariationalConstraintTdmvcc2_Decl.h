/**
 *******************************************************************************
 * 
 * @file    TrfVariationalConstraintTdmvcc2_Decl.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFVARIATIONALCONSTRAINTTDMVCC2_DECL_H_INCLUDED
#define TRFVARIATIONALCONSTRAINTTDMVCC2_DECL_H_INCLUDED

#include <unordered_map>
#include "td/tdvcc/trf/TrfConstraintTdmvcc2Base.h"
#include "libmda/util/any_type.h"

namespace midas::tdvcc
{

   /****************************************************************
    * @brief
    *    Transformer for constructing variational g matrices using 
    *    full-space matrix-representation framework.
    ****************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfVariationalConstraintTdmvcc2
      :  public TrfConstraintTdmvcc2Base<PARAM_T>
   {
      public:
         //! Not g=0
         inline static constexpr bool zero_g = false;
         //! Variational
         inline static constexpr bool is_variational = true;

         //! Alias
         using Base = TrfConstraintTdmvcc2Base<PARAM_T>;
         using typename Base::param_t;
         using typename Base::step_t;
         using real_t = step_t;
         using typename Base::mat_t;
         using typename Base::gmats_t;
         using typename Base::dmats_t;
         using typename Base::cont_t;
         using typename Base::n_modals_t;
         using meanfields_t = std::vector<std::array<mat_t, 2>>;  // F, F' in time-dependent basis
         using intermeds_t = std::unordered_map<TdmvccIntermedType, libmda::util::any_type>;
         using vec_t = GeneralMidasVector<param_t>;

         //! C-tor
         TrfVariationalConstraintTdmvcc2
            (  const n_modals_t& aNModals
            ,  const ModeCombiOpRange& aMcr
            );

         //! Parse intermediates
         void ParseIntermediates
            (  const cont_t& aEtaHs
            ,  PARAM_T aCoefsFactor
            ,  meanfields_t&& aMeanFields
            );

         //! Construct g matrices
         gmats_t ConstraintMatrices
            (  step_t aT
            ,  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            ,  const dmats_t& aDensMats
            )  const;

         //! T1 is not expected to be zero (except for 2-mode systems)
         constexpr bool T1Zero() const { return false; }

         //! L1 is expected to be zero for this constraint
         constexpr bool L1Zero() const { return true; }

      private:
         //! Intermediates
         intermeds_t mIntermediates;

         //! Calculate u vector for a given mode
         vec_t UVector(In aMode) const;
   };

}; /* namespace midas::tdvcc */

#endif /* TRFVARIATIONALCONSTRAINTTDMVCC2_DECL_H_INCLUDED */

