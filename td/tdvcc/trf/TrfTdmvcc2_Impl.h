/**
 *******************************************************************************
 * 
 * @file    TrfTdmvcc2_Impl.h
 * @date    06-10-2020
 * @author  Andreas Buchgraitz Jensen (buchgraitz@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCC2_IMPL_H_INCLUDED
#define TRFTDMVCC2_IMPL_H_INCLUDED

#include <numeric>

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "util/Timer.h"
#include "td/tdvcc/trf/TrfTdmvcc2_Decl.h"
#include "util/matrep/SparseClusterOper.h"


namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::cont_t TrfTdmvcc2<PARAM_T>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  bool aSaveIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool onlyonemode = false;
      constexpr bool t2only = false;
      constexpr bool tdmvcc2 = true;
      Vcc2TransReg<param_t,t2only, OpDef, tdmvcc2> trf(&this->Oper(), this->TdModInts(), this->Mcr(), onlyonemode, aSaveIntermeds);
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);
      this->PrintTimings("TrfTdmvcc2", "ErrVec", timer);

      if (  aSaveIntermeds
         )
      {
         this->mIntermeds = std::move(trf).GetSavedIntermediates();
      }
      

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::cont_t TrfTdmvcc2<PARAM_T>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::vcc::vcc2;
      Vcc2TransEta<param_t> trf(&this->Oper(), this->TdModInts(), this->Mcr());
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);
      In shift = trf.TotSizeInput() - trf.TotSizeResult();
      if (shift == 1)
      {
         result.SetNewSize(result.Size() + shift, true);
         param_t prev_val = 0;
         auto assign_prev_val = [&prev_val](param_t& a) -> void
            {
               std::swap(a, prev_val);
            };
         result.LoopOverElements(assign_prev_val);
      }
      else if (shift != 0)
      {
         MIDASERROR("Unexpected shift, not equal to either 0 or 1.");
      }

      this->PrintTimings("TrfTdmvcc2", "EtaVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::cont_t TrfTdmvcc2<PARAM_T>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      ,  bool aReuseIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool t2only = false;
      constexpr bool tdmvcc2 = true;
      using trf_t = Vcc2TransLJac<param_t,t2only, OpDef, tdmvcc2>;

      if (  aReuseIntermeds
         && !this->mIntermeds
         )
      {
         MIDASERROR("LJac called with aReuseIntermeds=true, but mIntermeds is nullptr!");
      }

      auto trf =  aReuseIntermeds
               ?  trf_t(&this->Oper(), this->TdModInts(), this->Mcr(), arAmpls, this->mIntermeds.get())  // Reuse intermediates from ErrVec and extend with Z
               :  trf_t(&this->Oper(), this->TdModInts(), this->Mcr(), arAmpls); // Construct intermediates from scratch

      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arCoefs, result);
      this->PrintTimings("TrfTdmvcc2", "LJac", timer);

      if (  aReuseIntermeds
         )
      {
         this->mIntermeds = std::move(trf).GetSavedIntermediates();
      }
      

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::mat_v_t
   TrfTdmvcc2<PARAM_T>::DensityMatrices
      (  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      )  const
   {
      Timer timer;
      timer.Reset();

      if (this->IoLevel() > 10)
      {
         Mout << "Entering TrfTdmvcc2<" << typeid(PARAM_T).name() << ">::DensityMatrices(...)" << std::endl;
      }
      const In nmodes = this->NModes();
      const auto& nmodals = this->NModalsTdBas();
      In maxamps = -I_1;
      In maxrhovv = -I_1;
      {
         auto tmp_nmodals = nmodals;
         std::sort(tmp_nmodals.begin(), tmp_nmodals.end());
         maxamps = (tmp_nmodals.back()-I_1) * (tmp_nmodals[nmodes-2]-I_1);
         maxrhovv = std::pow(tmp_nmodals.back()-I_1, 2);
      }
      mat_v_t rho(nmodes);

      // Set size of rho matrices and add 1 to the occ-occ element
      for (In m = 0; m < nmodes; ++m)
      {
         auto nmodals = this->NModalsTdBas()[m];
         rho[m].SetNewSize(nmodals, false, true); //n_modals(virtual) + reference
         rho[m][0][0] = param_t(1.);
      }

      // Declare lambda for adding to vir-vir block of a matrix
      auto addtovirvir = [](mat_t& r, vec_t& vv)
      {
         // Assume r is square
         auto nvir = r.Nrows()-I_1;
         for(In a=I_0; a<nvir; ++a)
         {
            for(In b=I_0; b<nvir; ++b)
            {
               r[b+I_1][a+I_1] += vv[b + nvir*a];
            }
         }
      };

      // Set up aux vectors. Some are only needed if the amplitudes are (partially) on disc.
      vec_t s, l, res_vec;
      if (!aClusterAmps.InMem())
      {
         s.Reserve(maxamps);
      }
      if (!aLambdaCoefs.InMem())
      {
         l.Reserve(maxamps);
      }
      res_vec.Reserve(maxrhovv);

      // Loop over all 2-mode combinations
      const auto& mcr = this->Mcr();
      for (auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto addr = it_mc->Address();
         const auto m0 = mc.Mode(0);
         const auto m1 = mc.Mode(1);
         if ( m0 >= m1 )
         {
            MIDASERROR("m0>=m1 in ModeCombi. This should not happen!");
         }
         const auto nvir0 = this->NModalsTdBas()[m0] - 1;
         const auto nvir1 = this->NModalsTdBas()[m1] - 1;
         const auto namps = nvir0*nvir1;
         
         // Get s and l amplitudes for MC if not in memory
         const param_t* s_data = nullptr;
         GetAmplData(s_data, s, namps, aClusterAmps, m0, m1);

         const param_t* l_data = nullptr;
         GetAmplData(l_data, l, namps, aLambdaCoefs, m0, m1);
         // Calculate contribution to occ-occ element as dot product (without complex conj)
         auto occocc = std::inner_product(s_data, s_data+namps, l_data, param_t(0.0));
         rho[m0][0][0] -= occocc;
         rho[m1][0][0] -= occocc;

         // Calculate vir-vir blocks. 
         // The contribution to m0 is s * l^T, but because we switch to column-major ordering 
         // in gemm, we calculate (s^T)^T * (l^T), as we have s^T and l^T in the vectors.
         {
            char transa = 'T';
            char transb = 'N';
            param_t alpha(1.);
            param_t beta(0.);
            int m = nvir0;
            int n = nvir0;
            int k = nvir1;
            int lda = std::max(1, k);
            int ldb = std::max(1, k);
            int ldc = std::max(1, m);
            res_vec.SetNewSize(m*n, false);     // Avoid reallocation if new_size <= old_size
            midas::lapack_interface::gemm
               (  &transa, &transb, &m
               ,  &n, &k, &alpha
               ,  const_cast<param_t*>(s_data), &lda, const_cast<param_t*>(l_data), &ldb
               ,  &beta, res_vec.data(), &ldc
               );
            addtovirvir(rho[m0], res_vec);
         }
         // The contribution to m1 is s^T * l.
         {
            char transa = 'N';
            char transb = 'T';
            param_t alpha(1.);
            param_t beta(0.);
            int m = nvir1;
            int n = nvir1;
            int k = nvir0;
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            res_vec.SetNewSize(m*n, false);   // Avoid reallocation if new_size <= old_size
            midas::lapack_interface::gemm
               (  &transa, &transb, &m
               ,  &n, &k, &alpha
               ,  const_cast<param_t*>(s_data), &lda, const_cast<param_t*>(l_data), &ldb
               ,  &beta, res_vec.data(), &ldc
               );
            addtovirvir(rho[m1], res_vec);
         }
      }

      // this->TimeIt() is checked inside PrintTimings.
      this->PrintTimings("TrfTdmvcc2", "DensityMatrices", timer);
      if (this->IoLevel() > 10)
      {
         Mout << "Exiting TrfTdmvcc2<" << typeid(PARAM_T).name() << ">::DensityMatrices(...)" << std::endl;
      }

      // return
      return rho;
   }

   /************************************************************************//**
    * @brief
    *    Calculate mean fields: {H_1, F_R, H'_1, F'_R}
    * 
    * @note
    *    After calls to ErrVec and LJac, mIntermeds hold all X, XC, Y, 
    *    and Z (L-based) intermediates for the T and L amplitudes that 
    *    they have been called with (probably the same as aClusterAmps and aLambdaCoefs, but be aware!).
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::meanfields_t
   TrfTdmvcc2<PARAM_T>::MeanFieldMatrices
      (  step_t aTime
      ,  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      )  const
   {
      // Set up different timers.
      Timer total_timer, timer_1m, timer_2m, timer_intermeds;
      total_timer.Reset();
      timer_2m.Reset();
      Nb time_2m(0), time_intermeds(0), time_oper_intermeds(0);

      if (this->IoLevel() > 10)
      {
         Mout << "Entering TrfTdmvcc2<" << typeid(PARAM_T).name() << ">::MeanFieldMatrices(...)" << std::endl;
      }
      using namespace midas::util::matrep;
      const auto nmodes = this->NModes();
      meanfields_t result(nmodes);

      timer_1m.Reset();
      // Set one-mode "mean fields"
      {
         auto h1_vec =  TdmvccHalfTransOneModeOpers
                           (  this->Oper()
                           ,  this->HalfTransModalIntegrals()[0]
                           ,  this->HalfTransModalIntegrals()[1]
                           );
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            result[imode][0] = std::move(h1_vec[imode].first);
            result[imode][2] = std::move(h1_vec[imode].second);
         }
      }
      this->PrintTimings("TrfTdmvcc2", "One-mode MeanFields", timer_1m);


      const auto& nmodals_td = this->NModalsTdBas();
      const auto& nmodals_prim = this->NModalsPrimBas();
      const auto& mcr = this->Mcr();
      const auto& oper = this->Oper();

      // Copy relevant MatRep results and (for now) delete what we want to calculate fast.
      for (In imode = I_0; imode < nmodes; ++imode)
      {
         result[imode][1].SetNewSize(nmodals_prim[imode], nmodals_td[imode], false, true);
         result[imode][3].SetNewSize(nmodals_td[imode], nmodals_prim[imode], false, true);
      }

      // Pre-compute certain intermediates.
      timer_intermeds.Reset();

      // Initialize 2-mode intermeds containers.
      this->mIntermeds->InitializeTdmvcc2Intermeds(oper, mcr);
      this->PrintTimings("TwoModeIntermeds", "InitializeTdmvcc2Intermeds", timer_intermeds);

      // Compute and save the intermediate named ''SumLS''
      this->mIntermeds->CalcIntermedSumLS(aClusterAmps, aLambdaCoefs, nmodes, this->Mcr());
      this->PrintTimings("TwoModeIntermeds", "CalcIntermedSumLS", timer_intermeds);

      // Compute and save the intermediate named ''VirFMatSum''
      this->mIntermeds->CalcIntermedVirFMatSum(nmodes, aClusterAmps, aLambdaCoefs, this->Mcr());
      this->PrintTimings("TwoModeIntermeds", "CalcIntermedVirFMatSum", timer_intermeds);

      time_intermeds += timer_intermeds.WallTime();

      // Define variables for timing of OperIntermeds.
      Nb time_w(0), time_ws(0), time_ws_fab(0), time_xl(0), time_3bch(0), time_trace_v(0);

      for (auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto& m = mc.Mode(0);
         const auto& m0 = mc.Mode(1);

         const auto& terms = oper.OpersForMc(mc);

         for (auto& i : terms)
         {
            const auto& c_coef = oper.Coef(i);
            
            const auto& opers = oper.GetOpers(i);
            const auto& oper_m = opers[0];
            const auto& oper_m0 = opers[1];

            MapInPairs MCsOpers;
            MCsOpers.emplace(InPair{m,m0}, InPair{oper_m,oper_m0});
            MCsOpers.emplace(InPair{m0,m}, InPair{oper_m0,oper_m});

            // Calculate intermediates dependend on opers in seperate function call to simplify code (avoid some doubling of code and such)
            timer_intermeds.SetTimer();
            CalculateOperIntermeds(MCsOpers, aLambdaCoefs, aClusterAmps, time_w, time_ws, time_ws_fab, time_xl, time_3bch, time_trace_v, false); // false == not FullActiveBasis
            const Nb time_oper_intermeds_temp = timer_intermeds.WallTime();
            time_intermeds += time_oper_intermeds_temp;
            time_oper_intermeds += time_oper_intermeds_temp;

            // Note the order of these contribs calls are important, later calls assumes intermediates saved in eariler calls.
            timer_2m.SetTimer();
            const auto& F_alpha_a = MeanFieldVirtualContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& F_alpha_a_m0 = MeanFieldVirtualContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);

            const auto& F_prime_i_alpha = MeanFieldPrimeRefContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& F_prime_i_alpha_m0 = MeanFieldPrimeRefContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);
           
            const auto& F_alpha_i = MeanFieldRefContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& F_alpha_i_m0 = MeanFieldRefContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);

            const auto& F_prime_a_alpha = MeanFieldPrimeVirtualContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& F_prime_a_alpha_m0 = MeanFieldPrimeVirtualContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);

            time_2m += timer_2m.WallTime();

            result[m][1].AddMatrixToSubMatrix(F_alpha_a, I_0, nmodals_prim[m], I_1, nmodals_td[m] - I_1);
            result[m0][1].AddMatrixToSubMatrix(F_alpha_a_m0, I_0, nmodals_prim[m0], I_1, nmodals_td[m0] - I_1);

            result[m][1].AddToCol(F_alpha_i, I_0);
            result[m0][1].AddToCol(F_alpha_i_m0, I_0);

            result[m][3].AddToRow(F_prime_i_alpha, I_0);
            result[m0][3].AddToRow(F_prime_i_alpha_m0, I_0);

            result[m][3].AddMatrixToSubMatrix(F_prime_a_alpha, I_1, nmodals_td[m] - I_1, I_0, nmodals_prim[m]);
            result[m0][3].AddMatrixToSubMatrix(F_prime_a_alpha_m0, I_1, nmodals_td[m0] - I_1, I_0, nmodals_prim[m0]);
         }
      }
      if (this->TimeIt())
      {
         Mout << "Time for intermed_w: " << time_w << " s." << std::endl;
         Mout << "Time for intermed_ws: " << time_ws << " s." << std::endl;
         Mout << "Time for intermed_ws_fab: " << time_ws_fab << " s." << std::endl;
         Mout << "Time for intermed_xl: " << time_xl << " s." << std::endl;
         Mout << "Time for intermed_3bch: " << time_3bch << " s." << std::endl;
         Mout << "Time for intermed_trace_v: " << time_trace_v << " s." << std::endl;
         Mout << "Time for OperIntermeds: " << time_oper_intermeds << " s." << std::endl;
         Mout << "Time for all intermeds: " << time_intermeds << " s." << std::endl;
         Mout << "Time for 2 mode MeanFields: " << time_2m << " s." << std::endl;
         Mout << "Time for total MeanFields: " << total_timer.WallTime() << " s." << std::endl;
      }

      if (this->IoLevel() > 10)
      {
         Mout << "Exiting TrfTdmvcc2<" << typeid(PARAM_T).name() << ">::MeanFieldMatrices(...)" << std::endl;
      }
      return result;
   }

   /************************************************************************//**
    * @brief
    *    Calculate mean fields F_R and F'_R are in td-basis, H_1 and H'_1 are
    *    in halftransformed basis: {H_1, F_R, H'_1, F'_R}
    * 
    * @note
    *    After calls to ErrVec and LJac, mIntermeds hold all X, XC, Y, 
    *    and Z (L-based) intermediates for the T and L amplitudes that 
    *    they have been called with (probably the same as aClusterAmps 
    *    and aLambdaCoefs, but be aware!).
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::meanfields_t
   TrfTdmvcc2<PARAM_T>::MeanFieldMatricesFullActiveBasis
      (  step_t aTime
      ,  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      )  const
   {
      Timer total_timer, timer_1m, timer_2m, timer_intermeds;
      total_timer.Reset();
      timer_2m.Reset();
      Nb time_2m(0), time_intermeds(0), time_oper_intermeds(0);

      using namespace midas::util::matrep;
      const auto nmodes = this->NModes();
      meanfields_t result(nmodes);

      // Set one-mode "mean fields"
      timer_1m.Reset();
      {
         auto h1_vec =  TdmvccHalfTransOneModeOpers
                           (  this->Oper()
                           ,  this->HalfTransModalIntegrals()[0]
                           ,  this->HalfTransModalIntegrals()[1]
                           );
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            result[imode][0] = std::move(h1_vec[imode].first);
            result[imode][2] = std::move(h1_vec[imode].second);
         }
      }
      this->PrintTimings("TrfTdmvcc2", "One-mode MeanFields", timer_1m);

      const auto& nmodals_td = this->NModalsTdBas();
      const auto& nmodals_prim = this->NModalsPrimBas();
      const auto& mcr = this->Mcr();
      const auto& oper = this->Oper();

      // Setup Sizes for F_r and F'_r
      for (In imode = I_0; imode < nmodes; ++imode)
      {
         result[imode][1].SetNewSize(nmodals_prim[imode], nmodals_td[imode], false, true);
         result[imode][3].SetNewSize(nmodals_td[imode], nmodals_prim[imode], false, true);
      }

      // Pre-compute certain intermediates.
      timer_intermeds.Reset();
      this->mIntermeds->InitializeTdmvcc2Intermeds(oper, mcr);
      this->PrintTimings("TwoModeIntermeds", "InitializeTdmvcc2Intermeds", timer_intermeds);

      this->mIntermeds->CalcIntermedSumLS(aClusterAmps, aLambdaCoefs, nmodes, this->Mcr());
      this->PrintTimings("TwoModeIntermeds", "CalcIntermedSumLS", timer_intermeds);

      time_intermeds += timer_intermeds.WallTime();

      // Define variables for timing of OperIntermeds.
      Nb time_w(0), time_ws(0), time_ws_fab(0), time_xl(0), time_3bch(0), time_trace_v(0);

      for (auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto& m = mc.Mode(0);
         const auto& m0 = mc.Mode(1);

         const auto& terms = oper.OpersForMc(mc);

         for (auto& i : terms)
         {
            const auto& c_coef = oper.Coef(i);
            
            const auto& opers = oper.GetOpers(i);
            const auto& oper_m = opers[0];
            const auto& oper_m0 = opers[1];

            MapInPairs MCsOpers;
            MCsOpers.emplace(InPair{m,m0}, InPair{oper_m,oper_m0});
            MCsOpers.emplace(InPair{m0,m}, InPair{oper_m0,oper_m});

            // Calculate intermediates dependend on opers in seperate function call
            timer_intermeds.SetTimer();
            CalculateOperIntermeds(MCsOpers, aLambdaCoefs, aClusterAmps, time_w, time_ws, time_ws_fab, time_xl, time_3bch, time_trace_v, true); // true == FullActiveBasis
            const Nb time_oper_intermeds_temp = timer_intermeds.WallTime();
            time_intermeds += time_oper_intermeds_temp;
            time_oper_intermeds += time_oper_intermeds_temp;

            timer_2m.SetTimer();
            const auto& F_m = MeanFieldFullActiveBasisContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& F_m0 = MeanFieldFullActiveBasisContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);

            const auto& Fp_m = MeanFieldPrimeFullActiveBasisContrib(m, m0, oper_m, oper_m0, c_coef, aLambdaCoefs, aClusterAmps);
            const auto& Fp_m0 = MeanFieldPrimeFullActiveBasisContrib(m0, m, oper_m0, oper_m, c_coef, aLambdaCoefs, aClusterAmps);
            time_2m += timer_2m.WallTime();

            result[m][1].AddToCol(F_m, I_0, I_0, I_1, I_1);
            result[m0][1].AddToCol(F_m0, I_0, I_0, I_1, I_1);

            result[m][3].AddToCol(Fp_m, I_0, I_0, I_1, I_1);
            result[m0][3].AddToCol(Fp_m0, I_0, I_0, I_1, I_1);
         }
      }
      if (this->TimeIt())
      {
         Mout << "Time for intermed_w: " << time_w << " s." << std::endl;
         Mout << "Time for intermed_ws: " << time_ws << " s." << std::endl;
         Mout << "Time for intermed_ws_fab: " << time_ws_fab << " s." << std::endl;
         Mout << "Time for intermed_xl: " << time_xl << " s." << std::endl;
         Mout << "Time for intermed_3bch: " << time_3bch << " s." << std::endl;
         Mout << "Time for intermed_trace_v: " << time_trace_v << " s." << std::endl;
         Mout << "Time for OperIntermeds: " << time_oper_intermeds << " s." << std::endl;
         Mout << "Time for all intermeds: " << time_intermeds << " s." << std::endl;
         Mout << "Time for 2 mode MeanFields: " << time_2m << " s." << std::endl;
         Mout << "Time for total MeanFields: " << total_timer.WallTime() << " s." << std::endl;
      }

      return result;
   }

   /************************************************************************//**
    * @brief
    *    Retrives lambdacoefs as vector for lapack_interface::gemm call
    * @param[in,out] aData
    *    aData will be overwritten to point to the interesting data
    * @param[in] ampl
    *    Temporary container
    * @param[in] size
    *    size tells the number of amplitudes there should be
    * @param[in] Ampl
    *    Ampl is the container of data we want a part of 
    *    (aLambdaCoefs or aClusterAmps in above methods)
    * @param[in] m1
    *    First mode. Used to calculate address for ampl.DataIo
    * @param[in] m2
    *    Second mode. Used to calculate address for ampl.DataIo
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdmvcc2<PARAM_T>::GetAmplData
      (  const param_t*& aData
      ,  vec_t ampl
      ,  const Uin size
      ,  const cont_t& Ampl
      ,  const In m1
      ,  const In m2
      )  const
   {         
      std::vector<In> mc = (m1 < m2) ? std::vector<In>{m1, m2} : std::vector<In>{m2, m1};
      using const_iterator = std::vector<ModeCombi>::const_iterator;
      const_iterator it_mc;
      this->Mcr().Find(mc, it_mc);
      const auto addr = it_mc->Address(); 
      if (!Ampl.InMem())
      {
         ampl.SetNewSize(size, false);
         Ampl.DataIo(IO_GET, ampl, size, addr);
         aData = ampl.data();
      }
      else
      {
         aData = Ampl.GetVector()->data() + addr;
      }
   }

   /************************************************************************//**
    * @brief
    *    Retrives lambdacoefs as matrix for lapack_interface::gemm call
    * @param[in] arMat
    *    Matrix to copy stuff into.
    * @param[in] arNvirm1
    *    size tells the number of virtual amplitudes for mode 1 there is
    * @param[in] arNvirm2
    *    size tells the number of virtual amplitudes for mode 2 there is
    * @param[in] Ampl
    *    Ampl is the container of data we want a part of 
    *    (aLambdaCoefs or aClusterAmps in above methods)
    * @param[in] m1
    *    First mode. Used to calculate address for ampl.DataIo
    * @param[in] m2
    *    Second mode. Used to calculate address for ampl.DataIo
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdmvcc2<PARAM_T>::GetAmplMat
      (  mat_t& arMat
      ,  const Uin arNvirm1
      ,  const Uin arNvirm2
      ,  const cont_t& Ampl
      ,  const In m1
      ,  const In m2
      )  const
   {         
      std::vector<In> mc = (m1 < m2) ? std::vector<In>{m1, m2} : std::vector<In>{m2, m1};
      Uin Nrows, Ncols;
      if (m1 < m2)
      {
         Nrows = arNvirm1;
         Ncols = arNvirm2;
      }
      else
      {
         Nrows = arNvirm2;
         Ncols = arNvirm1;
      }
      using const_iterator = std::vector<ModeCombi>::const_iterator;
      const_iterator it_mc;
      this->Mcr().Find(mc, it_mc);
      const auto addr = it_mc->Address(); 
      arMat.SetNewSize(Nrows, Ncols, false, false);
      Ampl.DataIo(IO_GET, arMat, Nrows*Ncols, Nrows, Ncols, addr);
   }

   /************************************************************************//**
    * @brief
    *    Calculates the trace of a matrix which is the product of 3 matrices
    *    in a (little bit) more efficient way than explicitly computing the matrix
    *    and then computing the trace.
    * @return
    *    Trace(A*B*C)
    * @param[in] A
    *    A is the first matrix in the product.
    * @param[in] B
    *    A is the second matrix in the product.
    * @param[in] C
    *    A is the third matrix in the product.
    * @param[in] trans_a
    *    bool whether the matrix A should be transposed or not.
    * @param[in] trans_b
    *    bool whether the matrix B should be transposed or not.
    * @param[in] trans_c
    *    bool whether the matrix C should be transposed or not.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::param_t
   TrfTdmvcc2<PARAM_T>::SmartTripleTrace
      (  const mat_t& A
      ,  const mat_t& B
      ,  const mat_t& C
      ,  const bool trans_a
      ,  const bool trans_b
      ,  const bool trans_c
      )  const
   {
      // Define size of loops below
      const In col_a = trans_a ? A.Nrows() : A.Ncols();
      const In row_a = trans_a ? A.Ncols() : A.Nrows();
      const In col_b = trans_b ? B.Nrows() : B.Ncols();
      const In row_b = trans_b ? B.Ncols() : B.Nrows();
      const In col_c = trans_c ? C.Nrows() : C.Ncols();
      const In row_c = trans_c ? C.Ncols() : C.Nrows();

      // Sanity checks
      if (  row_a != col_c
         || col_a != row_b
         || col_b != row_c
         )
      {
         std::stringstream ss;
         ss << "Bad matrix dimensions in SmartTripleTrace()" << std::endl
            << "cols A: " << col_a << std::endl
            << "rows A: " << row_a << std::endl
            << "cols B: " << col_b << std::endl
            << "rows B: " << row_b << std::endl
            << "cols C: " << col_c << std::endl
            << "rows C: " << row_c << std::endl
            ;
         MIDASERROR(ss.str());
      }

      // Do the actual matrixproduct/trace
      param_t res(0), temp(0);
      for (In i = I_0; i < row_a; i++)
      {
         for (In j = I_0; j < col_a; j++)
         {
            temp = 0;
            for (In l = I_0; l < col_b; l++)
            {
               if (trans_b && trans_c)
               {
                  temp += B[l][j] * C[i][l];
               }
               else if (trans_b && !trans_c)
               {
                  temp += B[l][j] * C[l][i];
               }
               else if (!trans_b && trans_c)
               {
                  temp += B[j][l] * C[i][l];
               }
               else //(!trans_b && !trans_c)
               {
                  temp += B[j][l] * C[l][i];
               }
            }
            if (trans_a)
            {
               temp *= A[j][i];
            }
            else  // (!trans_a)
            {
               temp *= A[i][j];
            }
            res += temp;
         }
      }
      return res;
   }

   /************************************************************************//**
    * @brief
    *    Calculates intermediates that depend on some oper indecies.
    *    Purpose is to make the specific meanfield functions a bit nicer.
    * @param[in] aMCsOpers
    *    Vector containing modecombi's and their respective operator index
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    * @param[in] time_w
    *    Reference to number for counting total time for specific intermed
    * @param[in] time_ws
    *    Reference to number for counting total time for specific intermed
    * @param[in] time_ws_fab
    *    Reference to number for counting total time for specific intermed
    * @param[in] time_xl
    *    Reference to number for counting total time for specific intermed
    * @param[in] time_3bch
    *    Reference to number for counting total time for specific intermed
    * @param[in] time_trace_v
    *    Reference to number for counting total time for specific intermed
    * @param[in] aFullActiveBasis
    *    Boolean for checking wether the intermediate should be calculated or
    *    not based on the method FullActiveBasis/or not.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdmvcc2<PARAM_T>::CalculateOperIntermeds
      (  const MapInPairs& aMCsOpers
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      ,  Nb& time_w
      ,  Nb& time_ws
      ,  Nb& time_ws_fab
      ,  Nb& time_xl
      ,  Nb& time_3bch
      ,  Nb& time_trace_v
      ,  bool aFullActiveBasis
      )  const
   {
      Timer timer_w, timer_ws, timer_ws_fab, timer_xl, timer_3bch, timer_trace_v;
      timer_w.Reset(), timer_ws.Reset(), timer_ws_fab.Reset(), timer_xl.Reset(), timer_3bch.Reset(), timer_trace_v.Reset();
      for (const auto& [mc, opers] : aMCsOpers)
      {
         const auto& m = mc.first;
         const auto& m0 = mc.second;
         const auto& oper_m = opers.first;
         const auto& oper_m0 = opers.second;

         bool CalcWS = (this->mIntermeds->GetIntermedWS(m, m0, oper_m0).Size() == I_0) ? true : false;
         bool CalcWS_fab = (this->mIntermeds->GetIntermedWS_FAB(m0, m, oper_m).Size() == I_0) ? true : false;
         bool CalcXL = (this->mIntermeds->GetIntermedXL(m, m0, oper_m0).Size() == I_0) ? true : false;
         bool Calc3bch = (this->mIntermeds->GetIntermed3bch(m, m0, oper_m0).Size() == I_0) ? true : false;
         // For VSCF start at t=0 V trace is always 0, so not the most efficient, but should be neglible for computations with any reasonable t_end.
         bool CalcTraceIntermedV = (this->mIntermeds->GetTraceVIntermed(m, m0, oper_m0) == param_t(0.0)) ? true : false;

         const auto& h_tilde_m0_mat = this->TdModInts().GetIntegrals(m0, oper_m0);
         const auto& h_tilde_m_mat = this->TdModInts().GetIntegrals(m, oper_m);
         const auto nvirm = this->NModalsTdBas()[m] - 1;
         const auto nvirm0 = this->NModalsTdBas()[m0] - 1;

         vec_t w_s_down(nvirm, param_t(0.0));
         vec_t w_s_down_fab(nvirm, param_t(0.0)); // For FullActiveBasis method
         vec_t x_l_down(nvirm, param_t(0.0));
         vec_t ThirdBCHTerm(nvirm, param_t(0.0));
         vec_t xl_sum(nvirm0, param_t(0.0)); // For 3bch 
         param_t v_trace(0.0);
         for (In m1 = I_0; m1 < this->Mcr().NumberOfModes(); ++m1)
         {
            if (m1 == m0)
            {
               continue;
            }
            const auto nvirm1 = this->NModalsTdBas()[m1] - 1;

            //Calculate W intermed
            if (this->mIntermeds->GetIntermedW(m1, m0, oper_m0).Size() == I_0)
            {
               timer_w.SetTimer();
               vec_t h_tilde_ref_vec;
               h_tilde_ref_vec.SetNewSize(nvirm0);
               h_tilde_m0_mat.GetOffsetCol(h_tilde_ref_vec, 0, 1); 
               mat_t l_m0_m1;
               GetAmplMat(l_m0_m1, nvirm0, nvirm1, aLambdaCoefs, m0, m1);
               if (m0 < m1)
               {
                  h_tilde_ref_vec.LeftMultiplyWithMatrix(l_m0_m1);
               }
               else
               {
                  h_tilde_ref_vec.MultiplyWithMatrix(l_m0_m1);
               }
               time_w += timer_w.WallTime();
               this->mIntermeds->PutIntermedW(m1, m0, oper_m0, h_tilde_ref_vec);
            }

            if (m1 == m)
            {
               continue;
            }

            if (this->mIntermeds->GetIntermedW(m1, m, oper_m).Size() == I_0)
            {
               timer_w.SetTimer();
               vec_t h_tilde_ref_vec;
               h_tilde_ref_vec.SetNewSize(nvirm);
               h_tilde_m_mat.GetOffsetCol(h_tilde_ref_vec, 0, 1); 
               mat_t l_m_m1;
               GetAmplMat(l_m_m1, nvirm, nvirm1, aLambdaCoefs, m, m1);
               if (m < m1)
               {
                  h_tilde_ref_vec.LeftMultiplyWithMatrix(l_m_m1);
               }
               else
               {
                  h_tilde_ref_vec.MultiplyWithMatrix(l_m_m1);
               }
               time_w += timer_w.WallTime();
               this->mIntermeds->PutIntermedW(m1, m, oper_m, h_tilde_ref_vec);
            }

            // Calculate IntermedWS
            if (CalcWS)
            {
               timer_ws.SetTimer();
               mat_t s_m_m1;
               GetAmplMat(s_m_m1, nvirm, nvirm1, aClusterAmps, m, m1);
               vec_t w = this->mIntermeds->GetIntermedW(m1, m0, oper_m0);
               if (m < m1)
               {
                  w.MultiplyWithMatrix(s_m_m1);
               }
               else
               {
                  w.LeftMultiplyWithMatrix(s_m_m1);
               }
               w_s_down += w;
               time_ws += timer_ws.WallTime();
            }
            if (CalcWS_fab && aFullActiveBasis)
            {
               // Do the not-quite-WS intermediate.
               timer_ws_fab.SetTimer();
               mat_t s_m_m1;
               GetAmplMat(s_m_m1, nvirm, nvirm1, aClusterAmps, m, m1);
               vec_t w = this->mIntermeds->GetIntermedW(m1, m, oper_m);
               if (m < m1)
               {
                  w.MultiplyWithMatrix(s_m_m1);
               }
               else
               {
                  w.LeftMultiplyWithMatrix(s_m_m1);
               }
               w_s_down_fab += w;
               time_ws_fab += timer_ws_fab.WallTime();
            }

            if (CalcXL && !aFullActiveBasis)
            {
               timer_xl.SetTimer();
               mat_t l_m_m1;
               GetAmplMat(l_m_m1, nvirm, nvirm1, aLambdaCoefs, m, m1);
               vec_t x_intermed_m1(nvirm1, param_t(0.0));
               this->mIntermeds->GetXintermed(m1, m0, oper_m0, x_intermed_m1);
               if (m < m1)
               {
                  x_intermed_m1.MultiplyWithMatrix(l_m_m1);
               }
               else
               {
                  x_intermed_m1.LeftMultiplyWithMatrix(l_m_m1);
               }
               x_l_down += x_intermed_m1;
               time_xl += timer_xl.WallTime();
            }

            if (Calc3bch)
            {
               timer_3bch.SetTimer();
               // Get pre computed sums of matrix products of the lambdacoefs and clusteramps
               const mat_t& ls_intermed = this->mIntermeds->GetIntermedSumLS(m, m1);
               vec_t x_intermed_m1(nvirm1, param_t(0.0));
               this->mIntermeds->GetXintermed(m1, m0, oper_m0, x_intermed_m1);
               // No transpose of ls_intermed here ! It is handled in CalcIntermedSumLS()
               ThirdBCHTerm += vec_t(ls_intermed * x_intermed_m1);

               // Calculate sum of X*l for (intermediate-)correction term.
               mat_t l_m1_m0;
               GetAmplMat(l_m1_m0, nvirm1, nvirm0, aLambdaCoefs, m1, m0);

               if (m1 < m0)
               {
                  x_intermed_m1.LeftMultiplyWithMatrix(l_m1_m0);
               }
               else
               {
                  x_intermed_m1.MultiplyWithMatrix(l_m1_m0);
               }
               xl_sum += x_intermed_m1;
               time_3bch += timer_3bch.WallTime();
            }

            if (CalcTraceIntermedV)
            {
               timer_trace_v.SetTimer();
               mat_t h_tilde_vir_mat(nvirm0, In(nvirm0));
               h_tilde_vir_mat.AssignToSubMatrix(h_tilde_m0_mat, 1, h_tilde_vir_mat.Nrows(), 1, h_tilde_vir_mat.Ncols());
               mat_t l_m0_m1, s_m0_m1;
               GetAmplMat(l_m0_m1, nvirm0, nvirm1, aLambdaCoefs, m0, m1);
               GetAmplMat(s_m0_m1, nvirm0, nvirm1, aClusterAmps, m0, m1);
               if (m1 < m && m1 < m0)
               {
                  v_trace += SmartTripleTrace(l_m0_m1, h_tilde_vir_mat, s_m0_m1, false, false, true);
               }
               else if (m1 > m && m1 > m0)
               {         
                  v_trace += SmartTripleTrace(l_m0_m1, h_tilde_vir_mat, s_m0_m1, true, false, false);
               }
               else
               {
                  if (m0 > m)
                  {
                     v_trace += SmartTripleTrace(l_m0_m1, h_tilde_vir_mat, s_m0_m1, false, false, true);
                  }
                  else if (m0 < m)
                  {
                     v_trace += SmartTripleTrace(l_m0_m1, h_tilde_vir_mat, s_m0_m1, true, false, false);
                  }
               }
               time_trace_v += timer_trace_v.WallTime();
            }
         }
         if (CalcWS)
         {
            this->mIntermeds->PutIntermedWS(m, m0, oper_m0, w_s_down);
         }
         if (CalcWS_fab && aFullActiveBasis)
         {
            this->mIntermeds->PutIntermedWS_FAB(m0, m, oper_m, w_s_down_fab);
         }
         if (CalcXL && !aFullActiveBasis)
         {
            this->mIntermeds->PutIntermedXL(m, m0, oper_m0, x_l_down);
         }
         if (Calc3bch)
         {
            timer_3bch.SetTimer();
            // Multiply with remaining cluster amps outside m1 loop for efficiency
            mat_t s_m_m0;
            GetAmplMat(s_m_m0, nvirm, nvirm0, aClusterAmps, m, m0);

            if (m < m0)
            {
               xl_sum.MultiplyWithMatrix(s_m_m0);
            }
            else
            {
               xl_sum.LeftMultiplyWithMatrix(s_m_m0);
            }
            ThirdBCHTerm -= xl_sum;

            vec_t x_intermed_m_m0(nvirm, param_t(0.0));
            // To see order of modes in GetXintermed(..) see TwoModeIntermeds_Decl.h
            if ( ! this->mIntermeds->GetXintermed(m, m0, oper_m0, x_intermed_m_m0) )
            {
               MIDASERROR("X intermeds not found !");
            }
            const param_t ls_sum = this->mIntermeds->GetAmplProdIntermedSum(m, m0);
            ThirdBCHTerm += x_intermed_m_m0 * ls_sum;
            time_3bch += timer_3bch.WallTime();
            this->mIntermeds->PutIntermed3bch(m, m0, oper_m0, ThirdBCHTerm);
         }
         if (CalcTraceIntermedV)
         {
            this->mIntermeds->PutTraceVIntermed(m, m0, oper_m0, v_trace);
         }
      }
   }
   
   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F_{alpha^m, i^m}^m part of the meanfields.
    *    This should return a matrix with non-zero elements in the first column
    *    only. So maybe later change to vector to save some space and time 
    *    moving all the zero's around.
    * @param[in] aM
    *    First mode. Same mode as F^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Please document me!
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::vec_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldRefContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {
      const auto& nmodals_prim = this->NModalsPrimBas()[aM];
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;

      const auto& h_tilde_mat = this->TdModInts().GetIntegrals(aM0, aOperMode_M0);

      vec_t PreHalfIntegrals = vec_t(nvirm + 1, param_t(0.0));

      // First BCH term
      const auto h_tilde_i_i = this->TdModInts().GetOccElement(aM0, aOperMode_M0);

      PreHalfIntegrals[0] = h_tilde_i_i;

      // Second BCH term
      const param_t trace_v = this->mIntermeds->GetTraceVIntermed(aM, aM0, aOperMode_M0);

      const vec_t& w_s_down = this->mIntermeds->GetIntermedWS(aM, aM0, aOperMode_M0);
      vec_t x_intermed(nvirm, param_t(0.0));
      // To see order of modes in GetXintermed(..) see TwoModeIntermeds_Decl.h
      if ( ! this->mIntermeds->GetXintermed(aM, aM0, aOperMode_M0, x_intermed) )
      {
         MIDASERROR("X intermeds not found !");
      }

      const param_t ls_sum = this->mIntermeds->GetAmplProdIntermedSum(aM, aM0);
      PreHalfIntegrals[0] += h_tilde_i_i * ls_sum + trace_v;
      for (In modal_idx = I_0; modal_idx < nvirm; ++modal_idx)
      {
         // x_intermed + w_intermed_times_s is also done in Fp_a_alpha
         PreHalfIntegrals[modal_idx + I_1] += x_intermed[modal_idx] + w_s_down[modal_idx];
      }

      // Third BCH term
      // Add contribution from 3rd bch term (+ small sanity check)
      const vec_t& bch3term = this->mIntermeds->GetIntermed3bch(aM, aM0, aOperMode_M0);
      if (bch3term.Size() != nvirm)
      {
         MIDASERROR("Wrong number of elements in 3rd bch term");
      }
      for (In modal_idx = I_0; modal_idx < nvirm; ++modal_idx)
      {
         PreHalfIntegrals[modal_idx + I_1] += bch3term[modal_idx];
      }

      const auto& h_check_mat = this->HalfTransModalIntegrals().at(0).at(aM).at(aOperMode_M);

      return vec_t(aCoefficientMM0 * h_check_mat * PreHalfIntegrals);
   }

   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F_{alpha^m, a^m}^m part of the meanfields.
    *    This should return a matrix with non-zero elements in every place 
    *    expect for the first column only. 
    * @param[in] aM
    *    First mode. Same mode as F^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Coefficient C_{O^m O^m0}^{m m0}
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::mat_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldVirtualContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {  
      // Initial variable assignment
      const auto& nmodals_prim = this->NModalsPrimBas()[aM];
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;

      const auto& h_tilde_mat = this->TdModInts().GetIntegrals(aM0, aOperMode_M0);

      mat_t PreHalfIntegrals(nvirm + 1, nvirm, param_t(0.0));

      vec_t h_tilde_ref_vec;
      h_tilde_ref_vec.SetNewSize(nvirm0);
      h_tilde_mat.GetOffsetCol(h_tilde_ref_vec, 0, 1);

      mat_t s_m_m0, l_m_m0;
      GetAmplMat(s_m_m0, nvirm, nvirm0, aClusterAmps, aM, aM0);
      GetAmplMat(l_m_m0, nvirm, nvirm0, aLambdaCoefs, aM, aM0);

      // Do matrix multiplication for first BCH term 
      const vec_t& h_tilde_l = this->mIntermeds->GetIntermedW(aM, aM0, aOperMode_M0);

      PreHalfIntegrals.AssignRow(h_tilde_l, 0);

      // Done with first BCH term !

      // Variable assignment needed for 2nd BCH term.
      mat_t h_tilde_vir_mat;
      h_tilde_vir_mat.SetNewSize(nvirm0, nvirm0, false, true);
      h_tilde_vir_mat.AssignToSubMatrix(h_tilde_mat, 1, h_tilde_vir_mat.Nrows(), 1, h_tilde_vir_mat.Ncols());

      mat_t V_intermed_mat;
      if (aM0 < aM)
      {
         const mat_t temp_mat(Transpose(mat_t(h_tilde_vir_mat * s_m_m0)));
         V_intermed_mat = std::move(mat_t(temp_mat * l_m_m0));
      }
      else
      {
         const mat_t temp_mat(Transpose(mat_t(l_m_m0 * h_tilde_vir_mat)));
         V_intermed_mat = std::move(mat_t(s_m_m0 * temp_mat));
      }

      const vec_t& ls_down = this->mIntermeds->GetIntermedXL(aM, aM0, aOperMode_M0);

      // Done with 2nd BCH term !

      // Add 2nd BCH term to temporary matrix and do half transformation.
      const auto& h_tilde_i_i = this->TdModInts().GetOccElement(aM0, aOperMode_M0);
      const mat_t& First_sum = this->mIntermeds->GetIntermedVirFMatSum(aM, aM0);
      PreHalfIntegrals.AssignSubMatrixToMatrix(V_intermed_mat + h_tilde_i_i * First_sum, 1, V_intermed_mat.Nrows(), 0, V_intermed_mat.Ncols());
      PreHalfIntegrals.AddToRow(ls_down, I_0);

      const auto& h_check_mat = this->HalfTransModalIntegrals().at(0).at(aM).at(aOperMode_M);

      return mat_t(aCoefficientMM0 * h_check_mat * PreHalfIntegrals);
   }
      
   
   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F'_{i^m, alpha^m}^m part of the meanfields.
    *    This should return a matrix with non-zero elements in the first row
    *    only. So maybe later change to vector to save some space and time 
    *    moving all the zero's around.
    * @param[in] aM
    *    First mode. Same mode as F'^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Coefficient C_{O^m O^m0}^{m m0}
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::vec_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldPrimeRefContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {
      // Initial variable assignment
      const auto& nmodals_prim = this->NModalsPrimBas()[aM];
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;
      vec_t PreHalfIntegrals = vec_t(nvirm + 1, param_t(0.0));

      const auto& h_tilde_mat = this->TdModInts().GetIntegrals(aM0, aOperMode_M0);
      const auto& h_tilde_i_i = this->TdModInts().GetOccElement(aM0, aOperMode_M0);

      PreHalfIntegrals[0] = h_tilde_i_i;

      const vec_t& h_tilde_l = this->mIntermeds->GetIntermedW(aM, aM0, aOperMode_M0);
      if (h_tilde_l.Size() == I_0)
      {
         MIDASERROR("W intermed for some reason does not exist !");
      }

      PreHalfIntegrals.FillPart(h_tilde_l, 1);

      // Done with first BCH term !

      // Starting on second BCH term !
      param_t ls_sum = this->mIntermeds->GetAmplProdIntermedSum(aM, aM0);

      const param_t trace_v = this->mIntermeds->GetTraceVIntermed(aM, aM0, aOperMode_M0);

      const auto& XL_intermed = this->mIntermeds->GetIntermedXL(aM, aM0, aOperMode_M0);
      if (XL_intermed.Size() != nvirm)
      {
         MIDASERROR("Disagreement in number of modals between intermediates and newly caluclated stuff!");
      }

      PreHalfIntegrals[0] += h_tilde_i_i * ls_sum + trace_v;
      for (In modal_idx = I_0; modal_idx < nvirm; ++modal_idx)
      {
         PreHalfIntegrals[modal_idx + I_1] += XL_intermed[modal_idx];
      }

      const auto& h_check_mat = this->HalfTransModalIntegrals().at(1).at(aM).at(aOperMode_M);

      return vec_t(aCoefficientMM0 * PreHalfIntegrals * h_check_mat);
   }

   
   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F'_{a^m, alpha^m}^m part of the meanfields.
    *    This should return a matrix with non-zero elements in every place 
    *    except for the first row only.
    * @param[in] aM
    *    First mode. Same mode as F^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Please document me!
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::mat_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldPrimeVirtualContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;
      mat_t PreHalfIntegrals(nvirm, nvirm + 1, param_t(0.0));

      const auto& h_tilde_mat = this->TdModInts().GetIntegrals(aM0, aOperMode_M0);

      // First BCH is zero !

      // Second BCH
      vec_t x_intermed(nvirm, param_t(0.0));
      // To see order of modes in GetXintermed(..) see TwoModeIntermeds_Decl.h
      if ( ! this->mIntermeds->GetXintermed(aM, aM0, aOperMode_M0, x_intermed) )
      {
         MIDASERROR("X intermeds not found !");
      }

      const auto& h_tilde_i_i = h_tilde_mat[0][0];

      const mat_t& First_sum = this->mIntermeds->GetIntermedVirFMatSum(aM, aM0);

      mat_t h_tilde_vir_mat(nvirm0, In(nvirm0));
      h_tilde_vir_mat.AssignToSubMatrix(h_tilde_mat, 1, h_tilde_vir_mat.Nrows(), 1, h_tilde_vir_mat.Ncols());

      mat_t l_m_m0, s_m_m0;
      GetAmplMat(l_m_m0, nvirm, nvirm0, aLambdaCoefs, aM, aM0);
      GetAmplMat(s_m_m0, nvirm, nvirm0, aClusterAmps, aM, aM0);
      mat_t V_intermed_mat;
      if (aM < aM0)
      {
         const mat_t temp_mat(Transpose(mat_t(l_m_m0 * h_tilde_vir_mat)));
         V_intermed_mat = std::move(mat_t(s_m_m0 * temp_mat));
      }
      else
      {
         const mat_t temp_mat(Transpose(mat_t(h_tilde_vir_mat * s_m_m0)));
         V_intermed_mat = std::move(mat_t(temp_mat * l_m_m0));
      }

      const vec_t& w_intermed_times_s = this->mIntermeds->GetIntermedWS(aM, aM0, aOperMode_M0);
      // x_intermed + w_intermed_times_s is also done in F_alpha_i
      PreHalfIntegrals.AddToCol(x_intermed + w_intermed_times_s, I_0);
      PreHalfIntegrals.AddMatrixToSubMatrix(First_sum * h_tilde_i_i + V_intermed_mat, I_0, nvirm, I_1, nvirm);

      // Third BCH term
      const vec_t& bch3term = this->mIntermeds->GetIntermed3bch(aM, aM0, aOperMode_M0);
      PreHalfIntegrals.AddToCol(bch3term, I_0);

      // Multiply by C-coefficient and transform with halftransformed integrals.
      const auto& h_check_mat = this->HalfTransModalIntegrals().at(1).at(aM).at(aOperMode_M);
      return mat_t(aCoefficientMM0 * PreHalfIntegrals * h_check_mat);
   }

   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F^m part of the meanfields necessary for the full active basis method.
    *    This should return a matrix with non-zero elements in the first column
    *    only. So maybe later change to vector to save some space and time 
    *    moving all the zero's around.
    * @param[in] aM
    *    First mode. Same mode as F^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Coefficient C_{O^m O^m0}^{m m0}
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::vec_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldFullActiveBasisContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {
      const auto& nmodals_prim = this->NModalsPrimBas()[aM];
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;

      const auto& h_tilde_m_mat = this->TdModInts().GetIntegrals(aM, aOperMode_M);

      vec_t result(nvirm, param_t(0));

      const auto h_tilde_m0_i_i = this->TdModInts().GetOccElement(aM0, aOperMode_M0);

      const param_t U_m_m0 = this->mIntermeds->GetAmplProdIntermedSum(aM, aM0);
      const param_t trace_v = this->mIntermeds->GetTraceVIntermed(aM, aM0, aOperMode_M0);
      
      // Get h_{a^m i^m} i.e. get first column without reference part
      vec_t h_tilde_m_vir_vec(nvirm, param_t(0));
      h_tilde_m_mat.GetOffsetCol(h_tilde_m_vir_vec, 0, 1); 

      result += vec_t(h_tilde_m_vir_vec * (h_tilde_m0_i_i + h_tilde_m0_i_i*U_m_m0 + trace_v));

      vec_t x_intermed(nvirm, param_t(0.0));
      // To see order of modes in GetXintermed(..) see TwoModeIntermeds_Decl.h
      if ( ! this->mIntermeds->GetXintermed(aM, aM0, aOperMode_M0, x_intermed) )
      {
         MIDASERROR("X intermeds not found !");
      }
      const vec_t& w_s_down = this->mIntermeds->GetIntermedWS(aM, aM0, aOperMode_M0);
      const vec_t& bch3term = this->mIntermeds->GetIntermed3bch(aM, aM0, aOperMode_M0);

      mat_t h_tilde_m_vir_mat;
      h_tilde_m_vir_mat.SetNewSize(nvirm, nvirm, false, true);
      h_tilde_m_vir_mat.AssignToSubMatrix(h_tilde_m_mat, 1, h_tilde_m_vir_mat.Nrows(), 1, h_tilde_m_vir_mat.Ncols());
      result += vec_t(h_tilde_m_vir_mat * (x_intermed + w_s_down + bch3term));

      return vec_t(aCoefficientMM0 * result);
   }

   /************************************************************************//**
    * @brief
    *    Calculates the necessary matrix and vector products for the 
    *    F'^m part of the meanfields necessary for the full active space method.
    * @param[in] aM
    *    First mode. Same mode as F'^m.
    * @param[in] aM0
    *    Second mode.
    * @param[in] aOperMode_M
    *    Operator term for mode m.
    * @param[in] aOperMode_M0
    *    Operator term for mode m0.
    * @param[in] aCoefficientMM0
    *    Coefficient C_{O^m O^m0}^{m m0}
    * @param[in] aLambdaCoefs
    *    Container for all l coefficients
    * @param[in] aClusterAmps
    *    Container for all s coefficients
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdmvcc2<PARAM_T>::vec_t 
   TrfTdmvcc2<PARAM_T>::MeanFieldPrimeFullActiveBasisContrib
      (  const In aM
      ,  const In aM0
      ,  const In aOperMode_M
      ,  const In aOperMode_M0
      ,  const Nb aCoefficientMM0
      ,  const cont_t& aLambdaCoefs
      ,  const cont_t& aClusterAmps
      )  const
   {
      const auto nvirm = this->NModalsTdBas()[aM] - 1;
      const auto nvirm0 = this->NModalsTdBas()[aM0] - 1;

      const auto& h_tilde_m_mat = this->TdModInts().GetIntegrals(aM, aOperMode_M);
      const auto& h_tilde_m0_mat = this->TdModInts().GetIntegrals(aM0, aOperMode_M0);

      vec_t result(nvirm, param_t(0));

      // First BCH is zero !

      // Second BCH
      vec_t x_intermed(nvirm, param_t(0.0));
      // To see order of modes in GetXintermed(..) see TwoModeIntermeds_Decl.h
      if ( ! this->mIntermeds->GetXintermed(aM, aM0, aOperMode_M0, x_intermed) )
      {
         MIDASERROR("X intermeds not found !");
      }

      const auto& h_tilde_m_i_i = h_tilde_m_mat[0][0];
      const auto& h_tilde_m0_i_i = h_tilde_m0_mat[0][0];

      result += vec_t(h_tilde_m_i_i * x_intermed);

      mat_t h_tilde_m0_vir_mat(nvirm0, In(nvirm0));
      h_tilde_m0_vir_mat.AssignToSubMatrix(h_tilde_m0_mat, 1, h_tilde_m0_vir_mat.Nrows(), 1, h_tilde_m0_vir_mat.Ncols());

      mat_t s_m_m0;
      GetAmplMat(s_m_m0, nvirm, nvirm0, aClusterAmps, aM, aM0);
      vec_t w_intermed = this->mIntermeds->GetIntermedW(aM0, aM, aOperMode_M);
      w_intermed.LeftMultiplyWithMatrix(h_tilde_m0_vir_mat);      
      if (aM < aM0)
      {
         w_intermed.MultiplyWithMatrix(s_m_m0);
      }
      else // aM0 < aM
      {
         w_intermed.LeftMultiplyWithMatrix(s_m_m0);
      }
      result += w_intermed;

      const vec_t& ws_fab = this->mIntermeds->GetIntermedWS_FAB(aM0, aM, aOperMode_M);
      result += vec_t(h_tilde_m0_i_i * ws_fab);

      const vec_t& w_intermed_times_s_m_m0_operm0 = this->mIntermeds->GetIntermedWS(aM, aM0, aOperMode_M0);
      result += vec_t(h_tilde_m_i_i * w_intermed_times_s_m_m0_operm0);

      // Third BCH term
      const vec_t& bch3term = this->mIntermeds->GetIntermed3bch(aM, aM0, aOperMode_M0);
      result += vec_t(h_tilde_m_i_i * bch3term);

      return vec_t(aCoefficientMM0 * result);
   }
   
} /* namespace midas::tdvcc */

#endif/*TRFTDMVCC2_IMPL_H_INCLUDED*/