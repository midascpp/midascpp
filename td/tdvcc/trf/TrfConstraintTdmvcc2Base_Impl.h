/**
 *******************************************************************************
 * 
 * @file    TrfConstraintTdmvcc2Base_Impl.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTTDMVCC2BASE_IMPL_H_INCLUDED
#define TRFCONSTRAINTTDMVCC2BASE_IMPL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"
#include "td/tdvcc/trf/TrfConstraintTdmvcc2Base.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "input/Input.h"

namespace midas::tdvcc
{
   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   void TrfConstraintTdmvcc2Base<PARAM_T>::UpdateIntegrals
      (  const gmats_t& aGMats
      )
   {
      auto nmodes = this->NModes();

      // Copy integrals to correct container type
      std::vector<std::vector<mat_t>> g_ints(nmodes);
      for(In i=I_0; i<nmodes; ++i)
      {
         g_ints[i].emplace_back(aGMats[i]);
      }

      // Move integrals (mark them as T1 transformed to avoid errors)
      this->mModalIntegrals = std::move(modalintegrals_t(std::move(g_ints), modalintegrals_t::TransT::T1));
   }

   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintTdmvcc2Base<PARAM_T>::cont_t
   TrfConstraintTdmvcc2Base<PARAM_T>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  bool aSaveIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool t2only = true;
      constexpr bool onlyonemode = false;
      constexpr bool tdmvcc2 = true;

      Vcc2TransReg<param_t,t2only,g_opdef_t,tdmvcc2> trf(&this->GetGOpDef(), this->ModInts(), this->Mcr(), onlyonemode, aSaveIntermeds);
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);

      if (  aSaveIntermeds
         )
      {
         this->mIntermeds = std::move(trf).GetSavedIntermediates();
      }
      
      return result;
   }

   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintTdmvcc2Base<PARAM_T>::cont_t
   TrfConstraintTdmvcc2Base<PARAM_T>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool t2only = true;

      Vcc2TransEta<param_t,t2only,g_opdef_t> trf(&this->GetGOpDef(), this->ModInts(), this->Mcr());
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);
      In shift = trf.TotSizeInput() - trf.TotSizeResult();
      if (shift == 1)
      {
         result.SetNewSize(result.Size() + shift, true);
         param_t prev_val = 0;
         auto assign_prev_val = [&prev_val](param_t& a) -> void
            {
               std::swap(a, prev_val);
            };
         result.LoopOverElements(assign_prev_val);
      }
      else if (shift != 0)
      {
         MIDASERROR("Unexpected shift, not equal to either 0 or 1.");
      }

      return result;
   }

   /*******************************************************
    * 
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintTdmvcc2Base<PARAM_T>::cont_t
   TrfConstraintTdmvcc2Base<PARAM_T>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      ,  bool aReuseIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool t2only = true;
      constexpr bool tdmvcc2 = true;
      using trf_t = Vcc2TransLJac<param_t,t2only,g_opdef_t,tdmvcc2>;
      if (  aReuseIntermeds
         && !this->mIntermeds
         )
      {
         MIDASERROR("LJac called with aReuseIntermeds=true, but mIntermeds is nullptr!");
      }

      auto trf =  aReuseIntermeds
               ?  trf_t(&this->GetGOpDef(), this->ModInts(), this->Mcr(), arAmpls, this->mIntermeds.get())  // Reuse and extend intermediates
               :  trf_t(&this->GetGOpDef(), this->ModInts(), this->Mcr(), arAmpls);

      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      cont_t result(trf.TotSizeResult());
      trf.Transform(arCoefs, result);

      if (  aReuseIntermeds
         )
      {
         this->mIntermeds = std::move(trf).GetSavedIntermediates();
      }

      return result;
   }

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTTDMVCC2BASE_IMPL_H_INCLUDED */

