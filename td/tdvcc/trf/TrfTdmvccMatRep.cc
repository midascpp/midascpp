/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccMatRep.cc
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDMVCCMATREP(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfTdmvccMatRep<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDMVCCMATREP(Nb);
INSTANTIATE_TRFTDMVCCMATREP(std::complex<Nb>);

#undef INSTANTIATE_TRFTDMVCCMATREP
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
