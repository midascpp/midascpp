/**
 *******************************************************************************
 * 
 * @file    TrfTdvcc2_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCC2_IMPL_H_INCLUDED
#define TRFTDVCC2_IMPL_H_INCLUDED

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdvcc2<PARAM_T>::cont_t TrfTdvcc2<PARAM_T>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  bool aSaveIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      constexpr bool onlyonemode = false;
      Vcc2TransReg<param_t> trf(&this->Oper(), this->ModInts(), this->Mcr(), onlyonemode, aSaveIntermeds);
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);
      this->PrintTimings("TrfTdvcc2", "ErrVec", timer);

      if (  aSaveIntermeds
         )
      {
         this->mIntermeds = std::move(trf).GetSavedIntermediates();
      }

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdvcc2<PARAM_T>::cont_t TrfTdvcc2<PARAM_T>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::vcc::vcc2;
      Vcc2TransEta<param_t> trf(&this->Oper(), this->ModInts(), this->Mcr());
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arAmpls, result);
      In shift = trf.TotSizeInput() - trf.TotSizeResult();
      if (shift == 1)
      {
         result.SetNewSize(result.Size() + shift, true);
         param_t prev_val = 0;
         auto assign_prev_val = [&prev_val](param_t& a) -> void
            {
               std::swap(a, prev_val);
            };
         result.LoopOverElements(assign_prev_val);
      }
      else if (shift != 0)
      {
         MIDASERROR("Unexpected shift, not equal to either 0 or 1.");
      }

      this->PrintTimings("TrfTdvcc2", "EtaVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdvcc2<PARAM_T>::cont_t TrfTdvcc2<PARAM_T>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      ,  bool aReuseIntermeds
      )  const
   {
      using namespace midas::vcc::vcc2;
      using trf_t = Vcc2TransLJac<param_t>;

      if (  aReuseIntermeds
         && !this->mIntermeds
         )
      {
         MIDASERROR("LJac called with aReuseIntermeds=true, but mIntermeds is nullptr!");
      }

      auto trf =  aReuseIntermeds
               ?  trf_t(&this->Oper(), this->ModInts(), this->Mcr(), arAmpls, this->mIntermeds.get())  // Reuse intermediates from ErrVec and extend with Z
               :  trf_t(&this->Oper(), this->ModInts(), this->Mcr(), arAmpls); // Construct intermediates from scratch
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }

      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arCoefs, result);
      this->PrintTimings("TrfTdvcc2", "LJac", timer);

      return result;
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCC2_IMPL_H_INCLUDED*/
