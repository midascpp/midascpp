/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvcc_Decl.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFGENERALTIMTDVCC_DECL_H_INCLUDED
#define TRFGENERALTIMTDVCC_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "V3Interface.h"
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralTensorDataCont;
template<typename> class NiceTensor;

namespace midas::tdvcc
{

/**
 * Transformer class for general-excitation-level TIM-TDVCC
 **/
template
   <  typename PARAM_T
   >
class TrfGeneralTimTdvcc
   :  public detail::V3Interface<PARAM_T>
{
   public:
      /** @name Alias and definitions **/
      //!@{
      using Base = detail::V3Interface<PARAM_T>;

      using param_t = typename Base::param_t;
      using cont_t = typename Base::cont_t;
      using evaldata_t = typename Base::evaldata_t;
      using intermeds_t = typename Base::intermeds_t;
      using v3contrib_t = typename Base::v3contrib_t;
      using intermedprod_t = typename Base::intermedprod_t;
      using intermedprodl_t = typename Base::intermedprodl_t;
      using contribs_t = typename Base::contribs_t;
      using v3_t = typename Base::V3Type;

      using step_t = midas::type_traits::RealTypeT<param_t>;
      using opdef_t = OpDef;
      using modalintegrals_t = ModalIntegrals<param_t>;
      using n_modals_t = std::vector<Uin>;

      static inline constexpr TrfType trf_type = TrfType::GENERAL;
      //!@}
      
      //! c-tor
      TrfGeneralTimTdvcc
         (  const n_modals_t& arNModals
         ,  const opdef_t& arOpDef
         ,  const modalintegrals_t& arModInts
         ,  const ModeCombiOpRange& arMcr
         );

      //! Initialize mContribs
      void InitializeContribs
         (
         );

      /** @name Necessary interface to derivative and expectation values **/
      //!@{
      cont_t ErrVec
         (  step_t aTime
         ,  const cont_t& arAmpls
         )  const;

      cont_t EtaVec
         (  step_t aTime
         ,  const cont_t& arAmpls
         )  const;

      cont_t LJac
         (  step_t aTime
         ,  const cont_t& arAmpls
         ,  const cont_t& arCoefs
         )  const;

      inline cont_t ExpValTrf
         (  step_t aTime
         ,  const cont_t& arAmpls
         )  const
      {
         return ErrVec(aTime, arAmpls);
      }
      //!@}

   private:
      //! Number of time-independent modals for each mode
      const n_modals_t& mrNModals;

      //! OpDef to use in transform
      const opdef_t& mrOpDef;

      //! Integrals
      const modalintegrals_t& mrModInts;

      //! MCR of the cluster amplitudes and l coefs
      const ModeCombiOpRange& mrMcr;

      //! V3 contribs
      std::map<v3_t, contribs_t> mContribs;

      //! Intermediates
      mutable std::map<v3_t, intermeds_t> mIntermediates;

      /** @name Private getters **/
      //!@{
      const auto& NModals() const   {return mrNModals;}
      const auto& Oper() const      {return mrOpDef;}
      const auto& ModInts() const   {return mrModInts;}
      const auto& Mcr() const       {return mrMcr;}
      const auto& Intermeds() const {return mIntermediates;}
      //!@}
};

} /* namespace midas::tdvcc */

#endif /* TRFGENERALTIMTDVCC_DECL_H_INCLUDED */
