/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRepBase_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCMATREPBASE_IMPL_H_INCLUDED
#define TRFTDVCCMATREPBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "util/Timer.h"
#include "input/OpDef.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   typename TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::cl_oper_t TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::ConstructSparseClusterOper
      (  const n_modals_t& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      Timer timer;
      timer.Reset();
      cl_oper_t sco(arNModals, arMcr);
      this->sco_ctor_timing.at("CPU") = timer.CpuTime();
      this->sco_ctor_timing.at("Wall") = timer.WallTime();
      return sco;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   void TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::PrintAnalysisAndTimings
      (
      )  const
   {
      PrintOperMatCtorTiming();
      PrintScoOptAnalysis();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   void TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::PrintScoOptAnalysis
      (
      )  const
   {
      if (  (  this->TimeIt()
            || this->IoLevel() > 5
            )
         && !has_printed_sco_opt_anal
         )
      {
         std::stringstream ss;
         ss << "TrfTdvccMatRepBase<" << midas::type_traits::TypeName<PARAM_T>()
            << ">::ConstructSparseClusterOper(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         for(const auto& [type,time]: this->sco_ctor_timing)
         {
            std::stringstream ss2;
            ss2 << std::left << std::scientific << std::setw(4) << type << " time = " << time << "s\n";
            Mout << ss.str() << ss2.str();
         }
         Mout << std::flush;
         Mout << "SparseClusterOper optimization analysis:\n";
         midas::util::matrep::OptimizationAnalysis(this->SparseClusterOper(), Mout);
         Mout << std::flush;
         this->has_printed_sco_opt_anal = true;
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   void TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::PrintOperMatCtorTiming
      (
      )  const
   {
      if (  (  this->TimeIt()
            || this->IoLevel() > 5
            )
         && !has_printed_opermat_timing
         )
      {
         std::stringstream ss;
         ss << "TrfTdvccMatRepBase<" << midas::type_traits::TypeName<PARAM_T>()
            << ">::ConstructOperMat(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         for(const auto& [type,time]: this->opermat_ctor_timing)
         {
            std::stringstream ss2;
            ss2 << std::left << std::scientific << std::setw(4) << type << " time = " << time << "s\n";
            Mout << ss.str() << ss2.str();
         }
         Mout << std::flush;
         this->has_printed_opermat_timing = true;
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   void TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>::UpdateIntegralsImpl
      (
      )
   {
      if constexpr   (  std::is_same_v<BASETRF_TMPL<PARAM_T>, TrfTdmvccBase<PARAM_T>>
                     )
      {
         // Update mOperMat with new integrals
         mOperMat = std::make_unique<mat_t>(this->NModals(), this->Oper(), this->TdModInts(), true);
      }
      else
      {
         MIDASERROR("This function should only be called for TDMVCC!");
      }
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCCMATREPBASE_IMPL_H_INCLUDED*/
