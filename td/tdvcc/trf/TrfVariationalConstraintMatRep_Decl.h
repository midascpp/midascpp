/**
 *******************************************************************************
 * 
 * @file    TrfVariationalConstraintMatRep_Decl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFVARIATIONALCONSTRAINTMATREP_DECL_H_INCLUDED
#define TRFVARIATIONALCONSTRAINTMATREP_DECL_H_INCLUDED

#include <unordered_map>

#include "td/tdvcc/trf/TrfConstraintMatRepBase.h"
#include "libmda/util/any_type.h"

namespace midas::tdvcc
{

   /****************************************************************
    * @brief
    *    Transformer for constructing variational g matrices using 
    *    full-space matrix-representation framework.
    ****************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfVariationalConstraintMatRep
      :  public TrfConstraintMatRepBase<PARAM_T>
   {
      public:
         //! Not g=0
         inline static constexpr bool zero_g = false;
         //! Variational
         inline static constexpr bool is_variational = true;

         //! Alias
         using Base = TrfConstraintMatRepBase<PARAM_T>;
         using typename Base::param_t;
         using typename Base::step_t;
         using real_t = step_t;
         using typename Base::mat_t;
         using typename Base::fullspace_mat_t;
         using typename Base::gmats_t;
         using typename Base::dmats_t;
         using typename Base::cont_t;
         using typename Base::deriv_cont_t;
         using typename Base::oper_t;
         using typename Base::n_modals_t;
         using intermeds_t = std::unordered_map<TdmvccIntermedType, libmda::util::any_type>;
         using vec_t = GeneralMidasVector<param_t>;

         //! C-tor
         TrfVariationalConstraintMatRep
            (  const n_modals_t& aNModals
            ,  const ModeCombiOpRange& aMcr
            );

         //! Parse intermediates
         void ParseIntermediates
            (  const cont_t& aOmegaHs
            ,  const cont_t& aEtaHs
            ,  fullspace_mat_t&&
            ,  PARAM_T aAmplsFactor
            ,  PARAM_T aCoefsFactor
            );

         //! Construct g matrices
         gmats_t ConstraintMatrices
            (  step_t aT
            ,  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            ,  const dmats_t& aDensMats
            )  const;

         //! Getters
         bool& DirectGUp() { return mDirectGUp; }
         bool DirectGUp() const { return mDirectGUp; }

         //! T1 is only expected to be zero for direct g_up (which is not the variational solution for truncated TDMVCC)
         bool T1Zero() const { return this->DirectGUp(); }

         //! L1 is expected to be zero for this constraint
         constexpr bool L1Zero() const { return true; }

      private:
         //! Intermediates
         intermeds_t mIntermediates;

         //! H matrix
         mutable std::unique_ptr<fullspace_mat_t> mHMatrix = nullptr;

         //! Set g "up" directly without solving linear equations. NB: This is only variational if the full set of M_{down down} equations are solved for g "down"
         bool mDirectGUp = false;
   };

}; /* namespace midas::tdvcc */

#endif /* TRFVARIATIONALCONSTRAINTMATREP_DECL_H_INCLUDED */
