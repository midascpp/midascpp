/**
 *******************************************************************************
 * 
 * @file    TrfConstraintMatRepBase_Decl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTMATREPBASE_DECL_H_INCLUDED
#define TRFCONSTRAINTMATREPBASE_DECL_H_INCLUDED

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "util/matrep/OperMat.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/oper/LinCombOper.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/trf/TrfConstraintBase.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfConstraintMatRepBase
      :  public TrfConstraintBase<PARAM_T>
   {
      public:
         //! Alias
         using Base = TrfConstraintBase<PARAM_T>;
         using typename Base::param_t;
         using typename Base::step_t;
         using typename Base::mat_t;
         using typename Base::gmats_t;
         using typename Base::dmats_t;
         using typename Base::modals_t;
         using typename Base::modals_pair_t;
         using typename Base::n_modals_t;

         using fullspace_mat_t = midas::util::matrep::OperMat<param_t>;
         template<typename T> using cont_tmpl = GeneralMidasVector<T>;
         using cont_t = cont_tmpl<param_t>;
         using deriv_cont_t = ParamsTdmvcc<param_t, cont_tmpl>;
         using oper_t = td::LinCombOper<step_t, param_t, OpDef>;
         using cl_oper_t = midas::util::matrep::SparseClusterOper;
         using looper_t = midas::util::matrep::ShiftOperBraketLooper;
         static inline constexpr TrfType trf_type = TrfType::MATREP;

         //! c-tor
         TrfConstraintMatRepBase
            (  const n_modals_t& aNModals
            ,  const ModeCombiOpRange& aMcr
            )
            :  TrfConstraintBase<PARAM_T>
                  (  aNModals
                  ,  aMcr
                  )
            ,  mFullGMat(this->NModals(), false)
            ,  mShiftOperBraketLooper(this->NModals(), this->Mcr())
            ,  mSparseClusterOper(this->NModals(), this->Mcr())
         {
         }

         //! Update full-space g matrix
         void UpdateIntegrals
            (  const gmats_t& aOneModeGs
            );

         /** Transformations with linear combination of one-mode operators **/
         //!@{
         cont_t ErrVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t EtaVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t LJac(step_t aTime, const cont_t& arAmpls, const cont_t& arCoefs) const;
         //!@}

      protected:
         /** Queries **/
         //!@{
         const auto& FullGMat() const { return mFullGMat; }
         const auto& Looper() const { return mShiftOperBraketLooper; }
         const auto& SpClusterOp() const { return mSparseClusterOper; }
         //!@}

      private:
         //! g matrix (in full space)
         fullspace_mat_t mFullGMat;

         //! Looper for lambda terms
         looper_t mShiftOperBraketLooper;

         //! Cluster operator
         cl_oper_t mSparseClusterOper;
   };

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTMATREPBASE_DECL_H_INCLUDED */
