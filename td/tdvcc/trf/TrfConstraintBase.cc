/**
 *******************************************************************************
 * 
 * @file    TrfConstraintBase.cc
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "td/tdvcc/trf/TrfConstraintBase.h"
#include "td/tdvcc/trf/TrfConstraintBase_Impl.h"
#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFCONSTRAINTBASE(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfConstraintBase<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFCONSTRAINTBASE(Nb);
INSTANTIATE_TRFCONSTRAINTBASE(std::complex<Nb>);

#undef INSTANTIATE_TRFCONSTRAINTBASE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
