/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccBase_Decl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCCBASE_DECL_H_INCLUDED
#define TRFTDMVCCBASE_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "vcc/ModalIntegrals.h"

// Forward declarations.


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdmvccBase
      :  public TrfTdvccBase<PARAM_T>
   {
      public:
         using typename TrfTdvccBase<PARAM_T>::param_t;
         using typename TrfTdvccBase<PARAM_T>::step_t;
         using real_t = step_t;
         using typename TrfTdvccBase<PARAM_T>::opdef_t;
         using typename TrfTdvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTdvccBase<PARAM_T>::n_modals_t;
         using vec_t = GeneralMidasVector<param_t>;
         using mat_t = GeneralMidasMatrix<param_t>;
         using modal_cont_t = std::vector<std::pair<mat_t, mat_t>>;
         using meanfields_t = std::vector<std::array<mat_t, 4>>;  // H_1, F_R, H'_1, F'_R
         using mat_v_t = std::vector<mat_t>;
         using halftransints_t = std::array<std::vector<std::vector<mat_t>>, 2>;
         using regularization_t = std::pair<RegInverseType, real_t>;

         //! c-tor
         TrfTdmvccBase
            (  const n_modals_t& arNModalsTdBas
            ,  const n_modals_t& arNModalsPrimBas
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            );

         //! Update time-dependent modal integrals
         void UpdateIntegrals
            (  const modal_cont_t& aModals
            );

         //! Modal derivative (without g terms) from pre-calculated quantities
         modal_cont_t SecondarySpaceModalDerivative
            (  step_t aTime
            ,  const meanfields_t& aMeanFields
            ,  const mat_v_t& aInvDensMats
            ,  const mat_v_t& aProjectors
            ,  PARAM_T aKetFactor
            ,  PARAM_T aBraFactor
            )  const;

         //! Invert density matrices
         static mat_v_t InverseDensityMatrices
            (  const mat_v_t& aDensMats
            ,  RegInverseType aRegType
            ,  real_t aEpsilon
            ,  bool aVerbose = false
            );

         //! Secondary-space projector (Q = 1 - P)
         static mat_v_t SecondarySpaceProjector
            (  const modal_cont_t& aModals
            ,  bool aNonBiOrtho = true
            ,  NonOrthoProjectorType aNonOrthoProjector = NonOrthoProjectorType::ORIGINALBYUW
            ,  bool aVerbose = false
            );

         //! Active-space projector, P^m = U^m W^m (or U^m [W^m U^m]^{-1} W^m)
         static mat_t ActiveSpaceProjector
            (  const modal_cont_t& aModals
            ,  In aMode
            ,  bool aNonBiOrtho = true
            ,  NonOrthoProjectorType aNonOrthoProjector = NonOrthoProjectorType::ORIGINALBYUW
            ,  bool aVerbose = false
            );

         //! Get modal integrals of time-dependent basis (overrides base class impl to enable using TrfTdvccMatRepBase in TDMVCC)
         const modalintegrals_t& TdModInts() const { return mTdModalIntegrals; }


      protected:
         const n_modals_t& NModalsPrimBas() const {return mrNModalsPrimBas;}
         const n_modals_t& NModalsTdBas() const {return this->NModals();}

         const auto& HalfTransModalIntegrals() const { return this->mHalfTransModalIntegrals; }

      private:
         const n_modals_t& mrNModalsPrimBas;

         //! Time-dependent integrals
         modalintegrals_t mTdModalIntegrals;

         //! Half-transformed integrals { h_{td, prim}, h_{prim, td} }.
         halftransints_t mHalfTransModalIntegrals;

         void SanityCheck() const;
   };

} /* namespace midas::tdvcc */


#endif/*TRFTDMVCCBASE_DECL_H_INCLUDED*/
