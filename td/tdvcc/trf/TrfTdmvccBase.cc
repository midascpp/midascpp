/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccBase.cc
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdmvccBase.h"
#include "td/tdvcc/trf/TrfTdmvccBase_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDMVCCBASE(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfTdmvccBase<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDMVCCBASE(Nb);
INSTANTIATE_TRFTDMVCCBASE(std::complex<Nb>);

#undef INSTANTIATE_TRFTDMVCCBASE
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
