/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRep_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCMATREP_IMPL_H_INCLUDED
#define TRFTDVCCMATREP_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasVector.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   typename TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::cont_t
   TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccErrVec(this->OperMat(), this->SparseClusterOper(), arAmpls);
      this->PrintTimings("TrfTdvccMatRepImpl", "ErrVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   typename TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::cont_t
   TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccEtaVec(this->OperMat(), this->ShiftOperBraketLooper(), this->SparseClusterOper(), arAmpls);
      this->PrintTimings("TrfTdvccMatRepImpl", "EtaVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   typename TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::cont_t
   TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccLJac(this->OperMat(), this->ShiftOperBraketLooper(), this->SparseClusterOper(), arAmpls, arCoefs);
      this->PrintTimings("TrfTdvccMatRepImpl", "LJac", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   typename TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::mat_v_t
   TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>::DensityMatrices
      (  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      )  const
   {
      return midas::util::matrep::VccOneModeDensityMatrices(this->SparseClusterOper(), aClusterAmps, aLambdaCoefs);
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCCMATREP_IMPL_H_INCLUDED*/
