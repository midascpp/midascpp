/**
 *******************************************************************************
 * 
 * @file    TrfTdvccBase_Decl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCBASE_DECL_H_INCLUDED
#define TRFTDVCCBASE_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "input/ModeCombiOpRange.h"

// Forward declarations.
class OpDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;
class Timer;


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdvccBase
   {
      public:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using opdef_t = OpDef;
         using modalintegrals_t = ModalIntegrals<param_t>;
         using n_modals_t = std::vector<Uin>;

         TrfTdvccBase
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            );

         virtual ~TrfTdvccBase() = default;

         Uin IoLevel() const {return mIoLevel;}
         Uin& IoLevel() {return mIoLevel;}
         bool TimeIt() const {return mTimeIt;}
         bool& TimeIt() {return mTimeIt;}

         auto NModes() const { return this->Mcr().NumberOfModes(); }

      protected:
         const n_modals_t& NModals() const {return mrNModals;}
         const opdef_t& Oper() const {return mrOpDef;}
         const modalintegrals_t& ModInts() const {return mrModInts;}
         const ModeCombiOpRange& Mcr() const {return mrMcr;}

         //! Utility for pretty-printing timings. (If this->TimeIt().)
         void PrintTimings
            (  const std::string& arClassName
            ,  const std::string& arFuncName
            ,  Timer& arTimer
            )  const;

         //! Required in TrfTdmvccMatRep
         virtual void UpdateIntegralsImpl
            (
            )
         {
         }

      private:
         const n_modals_t& mrNModals;
         const opdef_t& mrOpDef;
         const modalintegrals_t& mrModInts;
         const ModeCombiOpRange& mrMcr;

         Uin mIoLevel = 0;
         bool mTimeIt = 0;

         void SanityCheck() const;
   };

} /* namespace midas::tdvcc */


#endif/*TRFTDVCCBASE_DECL_H_INCLUDED*/
