/**
 *******************************************************************************
 * 
 * @file    TrfTdvcc2_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCC2_DECL_H_INCLUDED
#define TRFTDVCC2_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdvcc2
      :  public TrfTdvccBase<PARAM_T>
   {
      public:
         using typename TrfTdvccBase<PARAM_T>::param_t;
         using typename TrfTdvccBase<PARAM_T>::step_t;
         using typename TrfTdvccBase<PARAM_T>::opdef_t;
         using typename TrfTdvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTdvccBase<PARAM_T>::n_modals_t;
         template<typename U> using cont_tmpl = GeneralDataCont<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr TrfType trf_type = TrfType::VCC2H2;
         using intermeds_t = TwoModeIntermeds<param_t, opdef_t>;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTdvcc2(Args&&... args)
            :  TrfTdvccBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         cont_t ErrVec(step_t aTime, const cont_t& arAmpls, bool aSaveIntermeds=true) const;
         cont_t EtaVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t LJac(step_t aTime, const cont_t& arAmpls, const cont_t& arCoefs, bool aReuseIntermeds=true) const;

         //! @note Do not save intermeds for ExpValTrf!
         inline cont_t ExpValTrf(step_t aTime, const cont_t& arAmpls) const {return ErrVec(aTime, arAmpls, false);}

         //! Clear intermediates
         void ClearIntermeds() { mIntermeds = nullptr; }
      
      private:
         //! Intermeds saved from ErrVec calculation
         mutable std::unique_ptr<intermeds_t> mIntermeds = nullptr;
   };

} /* namespace midas::tdvcc */


#endif/*TRFTDVCC2_DECL_H_INCLUDED*/
