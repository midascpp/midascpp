/**
 *******************************************************************************
 * 
 * @file    TimTdvccEvalData.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCEVALDATA_H_INCLUDED
#define TIMTDVCCEVALDATA_H_INCLUDED

#include <vector>

#include "inc_gen/TypeDefs.h"

#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/TensorDataCont.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc_cpg/BasicOper.h"


namespace midas::tdvcc
{

/**
 * Class that contains the data necessary for using the V3Contrib framework
 **/
template
   <  typename PARAM_T
   >
class TimTdvccEvalData
{
   public:
      /** @name Alias **/
      //!@{
      using param_t = PARAM_T;
      using intermeds_t = midas::vcc::v3::IntermediateMachine<param_t, NiceTensor, TimTdvccEvalData>;
      using integrals_t = ModalIntegrals<param_t>;
      using mcr_t = ModeCombiOpRange;
      using oper_t = OpDef;
      template<typename U> using cont_tmpl_t = GeneralTensorDataCont<U>;
      using cont_t = cont_tmpl_t<param_t>;
      using n_modals_t = std::vector<Uin>;

      using allow_midasvector_t = std::false_type;
      using allow_nicetensor_t = std::true_type;
      //!@}

      //! c-tor
      TimTdvccEvalData
         (  intermeds_t& arIntermeds
         ,  const integrals_t& arIntegrals
         ,  const oper_t& arOper
         ,  const mcr_t& arMcr
         ,  const n_modals_t& arNModalsVec
         ,  const cont_t* const apAmplitudes
         ,  const cont_t* const apCoefs = nullptr
         )
         :  mrIntermeds(arIntermeds)
         ,  mrIntegrals(arIntegrals)
         ,  mrOper(arOper)
         ,  mrMcr(arMcr)
         ,  mrNModalsVec(arNModalsVec)
         ,  mpAmplitudes(apAmplitudes)
         ,  mpCoefs(apCoefs)
      {
         MidasAssert(mpAmplitudes, "Cluster amplitudes must be initialized in TimTdvccEvalData!");
      }

      /**
       * Interface functions.
       * These are needed in all DATA types used in V3Contrib and IntermediateMachine
       **/
      //!@{

      /**
       * @return
       *    Integrals (ready for performing contractions)
       **/
      const integrals_t* const GetIntegrals
         (
         )  const
      {
         return &mrIntegrals;
      }

      /**
       * Get intermediates (must be template for using with V3)
       *
       * @return
       *    Reference to intermediates
       **/
      template
         <  template<typename> typename VECTOR
         ,  std::enable_if_t<std::is_same_v<VECTOR<param_t>, NiceTensor<param_t> > >* = nullptr
         >
      intermeds_t& GetIntermediates
         (
         )  const
      {
         return mrIntermeds;
      }

      /**
       * @return
       *    Reference to MCR included in the wave function
       **/
      const mcr_t& GetModeCombiOpRange
         (
         )  const
      {
         return mrMcr;
      }

      /**
       * Get dimensions of tensor for mode combi
       *
       * @param aMcIdx
       * @return
       *    Dimensions of amplitude tensor
       **/
      const std::vector<unsigned>& GetTensorDims
         (  In aMcIdx
         )  const
      {
         return this->mpAmplitudes->GetModeCombiData(aMcIdx).GetDims();
      }

      /**
       * Get total number of excitations in the vector
       *
       * @return
       *    Size of amplitudes
       **/
      In NExci
         (
         )  const
      {
         return this->mpAmplitudes->TotalSize();
      }

      /**
       * Get number of excitation amplitudes for a given MC from mpXvec
       *
       * @param aMcIdx    Index of ModeCombi
       * @return
       *    Number of WF params for ModeCombi
       **/
      In NExciForModeCombi
         (  In aMcIdx
         )  const
      {
         return this->mpAmplitudes->GetModeCombiData(aMcIdx).TotalSize();
      }

      /**
       * Get number of time-dependent modals for mode (active + occupied)
       *
       * @param aMode
       * @return
       *    Number of modals
       **/
      In NModals
         (  LocalModeNr aMode
         )  const
      {
         return this->mrNModalsVec[aMode];
      }

      /**
       * Get number of modals as vector
       *
       * @return
       *    N vector
       **/
      const n_modals_t& GetNModals
         (
         )  const
      {
         return this->mrNModalsVec;
      }

      /**
       * Get number of modes
       *
       * @return
       *    M
       **/
      In NModes
         (
         )  const
      {
         return this->mrNModalsVec.size();
      }

      /**
       * Get MC level of the operator.
       * @return
       *    MC level
       **/
      In McLevel
         (
         )  const
      {
         return mrOper.McLevel();
      }

      /**
       * Operator coefficient
       *
       * @return
       *    c_t
       **/
      param_t OperatorCoef
         (  In aTerm
         )  const
      {
         return mrOper.Coef(aTerm);
      }

      /**
       * Terms for MC
       *
       * @param[in]  aMc            MC to find in MCR for operator
       * @param[out] arFirstTerm    First term for aMc
       * @param[out] arNterms       Number of terms for aMc
       * @return
       *    aMc found?
       **/
      bool TermsForMc
         (  const ModeCombi& aMc
         ,  In& arFirstTerm
         ,  In& arNterms
         )  const
      {
         return mrOper.TermsForMc(aMc, arFirstTerm, arNterms);
      }

      /**
       * Get operators for term
       *
       * @param aTerm
       * @return
       *    Vector of operator numbers in term
       **/
      const std::vector<LocalOperNr>& GetOpers
         (  In aTerm
         )  const
      {
         return mrOper.GetOpers(aTerm);
      }


      /**
       * Get excitation vector. Interface.
       *
       * @param aType      Type of vector
       * @return
       *    Pointer to correct vector
       **/
      template
         <  typename V
         >
      const V* const GetExciVec
         (  const OP aType
         )  const
      {
         // Check that we ask for TensorDataCont
         if constexpr   (  std::is_same_v<V, cont_t>
                        )
         {
            switch(aType)
            {
               case OP::T:
                  return mpAmplitudes;
               case OP::L:
                  if (  !mpCoefs
                     )
                  {
                     MIDASERROR("Requesting lambda coefs from TimTdvccEvalData, which is a nullptr!");
                  }
                  return mpCoefs;
               default:
                  MIDASERROR("TimTdvccEvalData: GetExciVec(): Unknown type.");
                  return nullptr;
            }
         }
         else
         {
            MIDASERROR("TimTdvccEvalData only contains ComplexTensorDataCont coefficients.");
            return nullptr;
         }
      }

      /**
       * Is TransformerV3 (and thereby the IntermediateMachine) using NiceTensor?
       *
       * @return
       *    Always true for this data class
       **/
      constexpr bool TensorTransform
         (
         )  const
      {
         return true;
      }

      //!@}


   private:
      //! Intermediates
      intermeds_t& mrIntermeds;

      //! Integrals
      const integrals_t& mrIntegrals;

      //! Operator
      const oper_t& mrOper;

      //! MCR
      const mcr_t& mrMcr;

      //! Number of modals per mode (including occupied)
      const n_modals_t& mrNModalsVec;

      //! Amplitudes
      const cont_t* const mpAmplitudes = nullptr;

      //! L coefs
      const cont_t* const mpCoefs = nullptr;
};

} /* namespace midas::tdvcc */

#endif /* TIMTDVCCEVALDATA_H_INCLUDED */
