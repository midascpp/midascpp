/**
 *******************************************************************************
 * 
 * @file    TrfTdvci2_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCI2_IMPL_H_INCLUDED
#define TRFTDVCI2_IMPL_H_INCLUDED

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdvci2<PARAM_T>::cont_t TrfTdvci2<PARAM_T>::Transform
      (  step_t aTime
      ,  const cont_t& arCoefs
      )  const
   {
      using namespace midas::vcc::vcc2;
      Vcc2TransReg<param_t> trf(&this->Oper(), this->ModInts(), this->Mcr());
      if (this->IoLevel() > 10)
      {
         PassSettingsToTrf(trf, *this);
      }
      trf.DoingVcc() = false;
      Timer timer;
      timer.Reset();
      cont_t result(trf.TotSizeResult());
      trf.Transform(arCoefs, result);
      this->PrintTimings("TrfTdvci2", "Transform", timer);
      return result;
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCI2_IMPL_H_INCLUDED*/
