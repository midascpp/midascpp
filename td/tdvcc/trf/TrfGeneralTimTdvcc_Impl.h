/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvcc_Impl.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFGENERALTIMTDVCC_IMPL_H_INCLUDED
#define TRFGENERALTIMTDVCC_IMPL_H_INCLUDED

#include "TrfGeneralTimTdvcc_Decl.h"
#include "TimTdvccEvalData.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/IntermedProd.h"
#include "vcc/v3/IntermedProdL.h"

namespace midas::tdvcc
{

/**
 * Initialize
 *
 * @note
 *    mContribs cannot be initialized yet, because mV3cFilePrefix is not passed in the c-tor
 **/
template
   <  typename PARAM_T
   >
TrfGeneralTimTdvcc<PARAM_T>::TrfGeneralTimTdvcc
   (  const n_modals_t& arNModals
   ,  const opdef_t& arOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   )
   :  mrNModals(arNModals)
   ,  mrOpDef(arOpDef)
   ,  mrModInts(arModInts)
   ,  mrMcr(arMcr)
{
   mIntermediates[v3_t::ERRVEC] = intermeds_t();
   mIntermediates[v3_t::ETAVEC] = intermeds_t();
   mIntermediates[v3_t::LJAC] = intermeds_t();
}

/**
 * Calculate error vector for amplitude derivative
 *
 * @param aTime
 * @param arAmpls
 * @return
 *    Error vector
 **/
template
   <  typename PARAM_T
   >
typename TrfGeneralTimTdvcc<PARAM_T>::cont_t TrfGeneralTimTdvcc<PARAM_T>::ErrVec
   (  step_t aTime
   ,  const cont_t& arAmpls
   )  const
{
   const auto& contribs = this->mContribs.at(v3_t::ERRVEC);

   // Clear intermediates
   auto& intermeds = this->mIntermediates[v3_t::ERRVEC];
   intermeds.ClearIntermeds();

   // Do T1-transformation of integrals
   auto t1_integrals = this->ModInts();
   t1_integrals.T1Transform(this->ExtractOneModeAmplitudes(arAmpls, mrNModals));

   // Initialize evaldata
   evaldata_t evaldata
      (  intermeds
      ,  t1_integrals
      ,  this->Oper()
      ,  this->Mcr()
      ,  this->NModals()
      ,  &arAmpls
      );

   // Evaluate
   return this->EvaluateContribs(contribs, evaldata);
}

/**
 * Calculate eta vector for l derivative
 *
 * @param aTime
 * @param arAmpls
 * @return
 *    Eta vector
 **/
template
   <  typename PARAM_T
   >
typename TrfGeneralTimTdvcc<PARAM_T>::cont_t TrfGeneralTimTdvcc<PARAM_T>::EtaVec
   (  step_t aTime
   ,  const cont_t& arAmpls
   )  const
{
   const auto& contribs = this->mContribs.at(v3_t::ETAVEC);

   // Clear intermediates
   auto& intermeds = this->mIntermediates[v3_t::ETAVEC];
   intermeds.ClearIntermeds();

   // Do T1-transformation of integrals
   auto t1_integrals = this->ModInts();
   t1_integrals.T1Transform(this->ExtractOneModeAmplitudes(arAmpls, mrNModals));

   // Make reference L vector (Niels: this is special to the V3 impl)
   cont_t ref(this->Mcr(), this->NModals(), true);
   ref.GetModeCombiData(I_0) = NiceScalar<PARAM_T>(1.0);

   // Initialize intermediates and evaldata
   evaldata_t evaldata
      (  intermeds
      ,  t1_integrals
      ,  this->Oper()
      ,  this->Mcr()
      ,  this->NModals()
      ,  &arAmpls
      ,  &ref
      );

   // Evaluate
   return this->EvaluateContribs(contribs, evaldata);
}

/**
 * Calculate Jacobian left-hand transform for l derivative
 *
 * @param aTime
 * @param arAmpls
 * @param arCoefs
 * @return
 *    Transformed vector
 **/
template
   <  typename PARAM_T
   >
typename TrfGeneralTimTdvcc<PARAM_T>::cont_t TrfGeneralTimTdvcc<PARAM_T>::LJac
   (  step_t aTime
   ,  const cont_t& arAmpls
   ,  const cont_t& arCoefs
   )  const
{
   const auto& contribs = this->mContribs.at(v3_t::LJAC);

   // Clear intermediates
   auto& intermeds = this->mIntermediates[v3_t::LJAC];
   intermeds.ClearIntermeds();

   // Do T1-transformation of integrals
   auto t1_integrals = this->ModInts();
   t1_integrals.T1Transform(this->ExtractOneModeAmplitudes(arAmpls, mrNModals));

   // Initialize intermediates and evaldata
   evaldata_t evaldata
      (  intermeds
      ,  t1_integrals
      ,  this->Oper()
      ,  this->Mcr()
      ,  this->NModals()
      ,  &arAmpls
      ,  &arCoefs
      );

   // Evaluate
   return this->EvaluateContribs(contribs, evaldata);
}

/**
 * Initialize mContribs
 **/
template
   <  typename PARAM_T
   >
void TrfGeneralTimTdvcc<PARAM_T>::InitializeContribs
   (
   )
{
   this->mContribs[v3_t::ERRVEC] = this->InitializeV3(v3_t::ERRVEC, this->Mcr().GetMaxExciLevel(), this->Oper().McLevel(), this->mIntermediates[v3_t::ERRVEC]);
   this->mContribs[v3_t::ETAVEC] = this->InitializeV3(v3_t::ETAVEC, this->Mcr().GetMaxExciLevel(), this->Oper().McLevel(), this->mIntermediates[v3_t::ETAVEC]);
   this->mContribs[v3_t::LJAC] = this->InitializeV3(v3_t::LJAC, this->Mcr().GetMaxExciLevel(), this->Oper().McLevel(), this->mIntermediates[v3_t::LJAC]);
}

} /* namespace midas::tdvcc */

#endif /* TRFGENERALTIMTDVCC_IMPL_H_INCLUDED */
