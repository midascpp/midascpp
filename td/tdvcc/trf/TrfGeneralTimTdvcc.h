/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvcc.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFGENERALTIMTDVCC_H_INCLUDED
#define TRFGENERALTIMTDVCC_H_INCLUDED

#include "TrfGeneralTimTdvcc_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "TrfGeneralTimTdvcc_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TRFGENERALTIMTDVCC_H_INCLUDED */
