/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRepBase.cc
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdvccMatRepBase.h"
#include "td/tdvcc/trf/TrfTdvccMatRepBase_Impl.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDVCCMATREPBASE(PARAM_T, BASETRF_TMPL) \
   namespace midas::tdvcc \
   { \
      template class TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDVCCMATREPBASE(Nb, TrfTdvccBase);
INSTANTIATE_TRFTDVCCMATREPBASE(Nb, TrfTdmvccBase);
INSTANTIATE_TRFTDVCCMATREPBASE(std::complex<Nb>, TrfTdvccBase);
INSTANTIATE_TRFTDVCCMATREPBASE(std::complex<Nb>, TrfTdmvccBase);

#undef INSTANTIATE_TRFTDVCCMATREPBASE
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
