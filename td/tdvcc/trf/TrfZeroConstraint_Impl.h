/**
 *******************************************************************************
 * 
 * @file    TrfZeroConstraint_Impl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFZEROCONSTRAINT_IMPL_H_INCLUDED
#define TRFZEROCONSTRAINT_IMPL_H_INCLUDED

#include "libmda/numeric/float_eq.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"

namespace midas::tdvcc
{

}; /* namespace midas::tdvcc */

#endif /* TRFZEROCONSTRAINT_IMPL_H_INCLUDED */
