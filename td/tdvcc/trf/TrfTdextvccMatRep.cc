/**
 *******************************************************************************
 * 
 * @file    TrfTdextvccMatRep.cc
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDEXTVCCMATREP(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfTdextvccMatRep<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDEXTVCCMATREP(Nb);
INSTANTIATE_TRFTDEXTVCCMATREP(std::complex<Nb>);

#undef INSTANTIATE_TRFTDEXTVCCMATREP
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
