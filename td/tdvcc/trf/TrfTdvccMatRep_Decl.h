/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRep_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCMATREP_DECL_H_INCLUDED
#define TRFTDVCCMATREP_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdvccMatRepBase.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename BASETRF_TMPL
      >
   class TrfTdvccMatRepImpl
      :  public TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>
   {
      public:
         using Base = TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>;
         using typename Base::param_t;
         using typename Base::step_t;
         using typename Base::opdef_t;
         using typename Base::modalintegrals_t;
         using typename Base::n_modals_t;
         using typename Base::cont_t;
         using typename Base::mat_v_t;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTdvccMatRepImpl(Args&&... args)
            :  TrfTdvccMatRepBase<PARAM_T, BASETRF_TMPL>(std::forward<Args>(args)...)
         {
         }

         cont_t ErrVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t EtaVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t LJac(step_t aTime, const cont_t& arAmpls, const cont_t& arCoefs) const;

         inline cont_t ExpValTrf(step_t aTime, const cont_t& arAmpls) const {return ErrVec(aTime, arAmpls);}

         //! Calculate density matrices
         mat_v_t DensityMatrices
            (  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;
   };

   /**
    * Aliases used for different contexts
    **/
   template<typename PARAM_T> using TrfTdvccMatRep = TrfTdvccMatRepImpl<PARAM_T, TrfTdvccBase>;
   template<typename PARAM_T> using TrfTdmvccMatRepBase = TrfTdvccMatRepImpl<PARAM_T, TrfTdmvccBase>;

} /* namespace midas::tdvcc */


#endif/*TRFTDVCCMATREP_DECL_H_INCLUDED*/
