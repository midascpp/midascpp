/**
 *******************************************************************************
 * 
 * @file    V3Interface.cc
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "V3Interface.h"
#include "V3Interface_Impl.h"

// Define instatiation macro.
#define INSTANTIATE_V3INTERFACE(PARAM_T)     \
   namespace midas::tdvcc::detail            \
   {                                         \
      template class V3Interface<PARAM_T>;   \
   } /* namespace midas::tdvcc::detail */    \
   

// Instantiations.
INSTANTIATE_V3INTERFACE(Nb);
INSTANTIATE_V3INTERFACE(std::complex<Nb>);

#undef INSTANTIATE_V3INTERFACE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
