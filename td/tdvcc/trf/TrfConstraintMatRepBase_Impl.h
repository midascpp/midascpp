/**
 *******************************************************************************
 * 
 * @file    TrfConstraintMatRepBase_Impl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTMATREPBASE_IMPL_H_INCLUDED
#define TRFCONSTRAINTMATREPBASE_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/trf/TrfConstraintMatRepBase.h"
#include "util/matrep/MatRepTransformers.h"

namespace midas::tdvcc
{
   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   void TrfConstraintMatRepBase<PARAM_T>::UpdateIntegrals
      (  const gmats_t& aGMats
      )
   {
      // Zero matrix
      this->mFullGMat.Zero();

      // Then add sum of one-mode operators
      this->mFullGMat.AddSumOneModeOpers(aGMats, PARAM_T(1.0));
   }
   
   /*******************************************************
    * @note
    *    Assumes mFullGMat has been updated
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintMatRepBase<PARAM_T>::cont_t
   TrfConstraintMatRepBase<PARAM_T>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      return midas::util::matrep::TrfVccErrVec(this->FullGMat(), this->SpClusterOp(), arAmpls);
   }

   /*******************************************************
    * @note
    *    Assumes mFullGMat has been updated
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintMatRepBase<PARAM_T>::cont_t
   TrfConstraintMatRepBase<PARAM_T>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      return midas::util::matrep::TrfVccEtaVec(this->FullGMat(), this->Looper(), this->SpClusterOp(), arAmpls);
   }

   /*******************************************************
    * @note
    *    Assumes mFullGMat has been updated
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintMatRepBase<PARAM_T>::cont_t
   TrfConstraintMatRepBase<PARAM_T>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      )  const
   {
      return midas::util::matrep::TrfVccLJac(this->FullGMat(), this->Looper(), this->SpClusterOp(), arAmpls, arCoefs);
   }

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTMATREPBASE_IMPL_H_INCLUDED */
