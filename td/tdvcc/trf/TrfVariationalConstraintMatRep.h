/**
 *******************************************************************************
 * 
 * @file    TrfVariationalConstraintMatRep.h
 * @date    11-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFVARIATIONALCONSTRAINTMATREP_H_INCLUDED
#define TRFVARIATIONALCONSTRAINTMATREP_H_INCLUDED

#include "td/tdvcc/trf/TrfVariationalConstraintMatRep_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */


#endif /* TRFVARIATIONALCONSTRAINTMATREP_H_INCLUDED */
