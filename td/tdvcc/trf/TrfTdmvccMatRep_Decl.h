/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccMatRep_Decl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCCMATREP_DECL_H_INCLUDED
#define TRFTDMVCCMATREP_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"

// Forward declarations.


namespace midas::tdvcc
{

   /************************************************************************//**
    * MatRep transformer for TDMVCC. Uses the transformer functions from 
    * TrfTdvccMatRep which is templated on TrfTdmvccBase to get the correct 
    * modal integrals.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdmvccMatRep
      :  public TrfTdmvccMatRepBase<PARAM_T>
   {
      public:
         using typename TrfTdmvccBase<PARAM_T>::param_t;
         using typename TrfTdmvccBase<PARAM_T>::step_t;
         using typename TrfTdmvccBase<PARAM_T>::opdef_t;
         using typename TrfTdmvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTdmvccBase<PARAM_T>::n_modals_t;
         using typename TrfTdmvccBase<PARAM_T>::vec_t;
         using typename TrfTdmvccMatRepBase<PARAM_T>::mat_t;
         using typename TrfTdmvccBase<PARAM_T>::mat_v_t;
         using typename TrfTdmvccBase<PARAM_T>::meanfields_t;
         using typename TrfTdmvccBase<PARAM_T>::modal_cont_t;
         using typename TrfTdmvccMatRepBase<PARAM_T>::cont_t;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTdmvccMatRep(Args&&... args)
            :  TrfTdmvccMatRepBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         //! Calculate density matrices
         mat_v_t DensityMatrices
            (  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;

         //! Calculate mean fields: {H_1, F_R, H'_1, F'_R}
         meanfields_t MeanFieldMatrices
            (  step_t aTime
            ,  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;
   };

} /* namespace midas::tdvcc */


#endif/*TRFTDMVCCMATREP_DECL_H_INCLUDED*/
