/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvcc.cc
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "TrfGeneralTimTdvcc.h"
#include "TrfGeneralTimTdvcc_Impl.h"

// Define instatiation macro.
#define INSTANTIATE_TRFGENERALTIMTDVCC(PARAM_T)    \
   namespace midas::tdvcc                          \
   {                                               \
      template class TrfGeneralTimTdvcc<PARAM_T>;  \
   } /* namespace midas::tdvcc */                  \
   

// Instantiations.
INSTANTIATE_TRFGENERALTIMTDVCC(Nb);
INSTANTIATE_TRFGENERALTIMTDVCC(std::complex<Nb>);

#undef INSTANTIATE_TRFGENERALTIMTDVCC

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
