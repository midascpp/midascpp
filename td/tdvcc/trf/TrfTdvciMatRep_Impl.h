/**
 *******************************************************************************
 * 
 * @file    TrfTdvciMatRep_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCIMATREP_IMPL_H_INCLUDED
#define TRFTDVCIMATREP_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasVector.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdvciMatRep<PARAM_T>::cont_t TrfTdvciMatRep<PARAM_T>::Transform
      (  step_t aTime
      ,  const cont_t& arCoefs
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      cont_t result = TrfVci(this->OperMat(), this->NModals(), this->Mcr(), OrganizeMcrSpaceVec(this->NModals(), this->Mcr(), arCoefs));
      this->PrintTimings("TrfTdvciMatRep", "Transform", timer);

      return result;
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCIMATREP_IMPL_H_INCLUDED*/
