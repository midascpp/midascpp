/**
 *******************************************************************************
 * 
 * @file    TrfTdvccUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCUTILS_H_INCLUDED
#define TRFTDVCCUTILS_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "input/ProgramSettings.h"
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "util/IsDetected.h"
#include "util/RegularizedInverse.h"
#include "td/tdvcc/TdvccEnums.h"
#include "input/ModeCombiOpRange.h"

#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/SparseClusterOper.h"

// Forward declarations.
class OpDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;


namespace midas::tdvcc
{
   namespace detail
   {
      template<typename U> using has_iolevel_type = decltype(std::declval<U>().IoLevel());
      template<typename U> using has_timeit_type = decltype(std::declval<U>().TimeIt());
      template<typename U> using has_goptreg_type = decltype(std::declval<U>().GOptRegularization());
      template<typename U> using has_v3c_prefix_t = decltype(std::declval<U>().V3cFilePrefix());
   } /* namespace detail */

   /************************************************************************//**
    * @brief
    *    Passes settings to transformers
    ***************************************************************************/
   template
      <  class TRF_T
      ,  class OTHER_T
      >
   void PassSettingsToTrf
      (  TRF_T& arTarget
      ,  const OTHER_T& arSource
      )
   {
      if constexpr   (  midas::util::IsDetectedV<detail::has_iolevel_type, TRF_T>
                     && midas::util::IsDetectedV<detail::has_iolevel_type, OTHER_T>
                     )
      {
         arTarget.IoLevel() = arSource.IoLevel();
      }
      if constexpr   (  midas::util::IsDetectedV<detail::has_timeit_type, TRF_T>
                     && midas::util::IsDetectedV<detail::has_timeit_type, OTHER_T>
                     )
      {
         arTarget.TimeIt() = arSource.TimeIt();
      }
      if constexpr   (  midas::util::IsDetectedV<detail::has_goptreg_type, TRF_T>
                     && midas::util::IsDetectedV<detail::has_goptreg_type, OTHER_T> 
                     )
      {
         arTarget.GOptRegularization() = arSource.GOptRegularization();
      }
      // Pass prefix for V3c files and initialize V3
      if constexpr   (  midas::util::IsDetectedV<detail::has_v3c_prefix_t, TRF_T>
                     )
      {
         auto default_prefix = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
         std::string source_prefix = "";

         //If source provides prefix, use that. Otherwise, use default.
         if constexpr   (  midas::util::IsDetectedV<detail::has_v3c_prefix_t, OTHER_T>
                        )
         {
            source_prefix = arSource.V3cFilePrefix();
         }

         arTarget.V3cFilePrefix() = source_prefix.empty() ? default_prefix : source_prefix;

         // Initialize!
         arTarget.InitializeContribs();
      }
   }

   /*******************************************************************************
    * @brief
    *    Extract 1-mode amplitudes to vector of MidasVector%s
    * @note
    *    Assumes that SetAddresses has been called on aMcr
    * 
    * @param aC         Container to extract amplitudes from
    * @param aMcr       ModeCombiOpRange
    * @param aNModals   Number of modals per mode (including occupied)
    * @param aInclOcc   Include a zero element for the occupied modal?
    * @return
    *    Vector of vector of one-mode amplitudes (No element)
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<GeneralMidasVector<PARAM_T>> ExtractOneModeAmplitudes
      (  const CONT_TMPL<PARAM_T>& aC
      ,  const ModeCombiOpRange& aMcr
      ,  const std::vector<Uin>& aNModals
      ,  bool aInclOcc = false
      )
   {
      constexpr bool is_dc = std::is_same_v<CONT_TMPL<PARAM_T>, GeneralDataCont<PARAM_T>>;
      constexpr bool is_mv = std::is_same_v<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>>;

      static_assert(is_dc || is_mv, "ExtractOneModeAmplitudes only implemented for DataCont and MidasVector");

      using vec_t = GeneralMidasVector<PARAM_T>;

      std::vector<vec_t> v_mv;
      v_mv.reserve(aMcr.NumModeCombisWithExcLevel(I_1));

      for(auto it = aMcr.Begin(I_1), end = aMcr.End(I_1); it != end; ++it)
      {
         // Extract the NModals(mode)-1 params, leaving a zero at index 0.
         auto nmodals = aNModals.at(it->Mode(I_0));
         auto sizev = aInclOcc ? nmodals : nmodals - I_1;

         vec_t mv(sizev, PARAM_T(0));

         if constexpr   (  is_dc
                        )
         {
            auto offv = aInclOcc ? I_1 : I_0;
            aC.DataIo(IO_GET, mv, nmodals-I_1, it->Address(), I_1, offv);
         }
         else if constexpr (  is_mv
                           )
         {
            auto beg_c = aC.begin().advance(it->Address());
            auto end_c = aC.begin().advance(it->Address()+nmodals-I_1);
            auto beg_v = aInclOcc ? ++mv.begin() : mv.begin();
            std::copy(beg_c, end_c, beg_v);
         }

         v_mv.emplace_back(std::move(mv));
      }
      return v_mv;
   }

   namespace constraint
   {
      /************************************************************************//**
       * @brief
       *    Construct Z^m matrix from density matrix: \f$ Z_{ab} = \delta_{ab} \rho_{ii} - \rho_{ba} \f$
       ***************************************************************************/
      template
         <  typename T
         >
      auto ZMatrix
         (  const GeneralMidasMatrix<T>& aDensMat
         )
      {
         using mat_t = GeneralMidasMatrix<T>;
         auto n = aDensMat.Nrows();
         if (  !aDensMat.IsSquare()
            || n < I_1
            )
         {
            MIDASERROR("Density matrix must be square and have non-zero dimensions!");
         }

         auto zdim = n-I_1;
         mat_t z(zdim, zdim);
         for(In i=I_0; i<zdim; ++i)
         {
            for(In j=I_0; j<zdim; ++j)
            {
               z[i][j] = -aDensMat[j+I_1][i+I_1];
            }
            z[i][i] += aDensMat[I_0][I_0];
         }

         return z;
      }

      /************************************************************************//**
       * @brief
       *    Solve linear equations using regularization.
       ***************************************************************************/
      template
         <  typename T
         >
      auto SolveLinearEquations
         (  const GeneralMidasMatrix<T>& aMat
         ,  const GeneralMidasVector<T>& aRhs
         ,  RegInverseType aRegType
         ,  midas::type_traits::RealTypeT<T> aEpsilon
         ,  bool aVerbose
         )
      {
         using vec_t = GeneralMidasVector<T>;

         if (  !aMat.IsSquare()
            )
         {
            MIDASERROR("Matrix must be square!");
         }
         if (  aRhs.Size() != aMat.Ncols()
            )
         {
            MIDASERROR("Dimensions of matrix and RHS mismatch!");
         }

         switch   (  aRegType
                  )
         {
            case RegInverseType::NONE:
            {
               auto lin_sol = GESV(aMat, aRhs);
               vec_t sol_vec;
               LoadSolution(lin_sol, sol_vec);
               return sol_vec;
               break;
            }
            case RegInverseType::SVD:
            {
               auto inv_mat = midas::math::RegularizedInverse(midas::math::inverse::svd, aMat, aEpsilon, aVerbose);
               return vec_t(inv_mat * aRhs);
               break;
            }
            case RegInverseType::XSVD:
            {
               auto inv_mat = midas::math::RegularizedInverse(midas::math::inverse::xsvd, aMat, aEpsilon, aVerbose);
               return vec_t(inv_mat * aRhs);
               break;
            }
            case RegInverseType::TIKHONOV:
            {
               auto inv_mat = midas::math::RegularizedInverse(midas::math::inverse::svd_tikhonov, aMat, aEpsilon, aVerbose);
               return vec_t(inv_mat * aRhs);
               break;
            }
            default:
            {
               MIDASERROR("Unrecognized regularization type!");
               return vec_t();
            }
         }
      }



      /************************************************************************//**
       * @brief
       *    Solve g "down" equations
       *
       * @param aDensMats     Density matrices for all modes
       * @param aEtaHs        Eta^H vectors for all modes
       * @param aRegType      Regularization type used for inverting Z matrices
       * @param aEpsilon      Regularization epsilon
       * @param aVerbose      Do verbose output
       * @return
       *    Pair of g "down" vectors and Z matrices.
       ***************************************************************************/
      template
         <  typename T
         >
      auto SolveVariationalGDownEquations
         (  const std::vector<GeneralMidasMatrix<T>>& aDensMats
         ,  const std::vector<GeneralMidasVector<T>>& aEtaHs
         ,  RegInverseType aRegType
         ,  midas::type_traits::RealTypeT<T> aEpsilon
         ,  bool aVerbose
         )
      {
         using mat_t = GeneralMidasMatrix<T>;
         using vec_t = GeneralMidasVector<T>;

         auto nmodes = aDensMats.size();
         std::vector<vec_t> gd_vec(nmodes);
         std::vector<mat_t> z_vec(nmodes);
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            // Construct Z matrices and solve down equations for this mode
            z_vec[imode] = ZMatrix(aDensMats[imode]);
            if (  aVerbose
               )
            {
               Mout  << " Solve Z gd = eta for mode " << imode << "\n"
                     << "    Z^m: \n" << z_vec[imode];
            }
            gd_vec[imode] = SolveLinearEquations(z_vec[imode], aEtaHs[imode], aRegType, aEpsilon, aVerbose);
         }

         return std::make_pair(std::move(gd_vec), std::move(z_vec));
      }

      /************************************************************************//**
       * @brief
       *    Construct \f$ \omega^{g_d} \f$ for a given mode.
       *    \f$ [omega^{g_{d},m}]_{a^m} = \langle a^m \vert exp(-T) g_{d} exp(T) \vert ref \rangle = \sum_{m'}\sum_{b^{m'}} g^{m'}_{i^{m'}b^{m'}} ( s^{m m'}_{a^m b^{m'}} - \delta_{m m'} s^m_{a^m} s^m_{b^m} ) \f$
       *
       * @param aGDownVec     Vector of g "down" constraints
       * @param aClusterAmps  Cluster amplitudes
       * @param aMcr          ModeCombiOpRange
       * @param aNModals      Number of modals per mode
       * @param aMode         Mode number
       * @return
       *    omega^{g_down,m}
       ***************************************************************************/
      template
         <  typename T
         ,  template<typename> typename CONT_TMPL
         >
      auto OmegaGDown
         (  const std::vector<GeneralMidasVector<T>>& aGDownVec
         ,  const CONT_TMPL<T>& aClusterAmps
         ,  const ModeCombiOpRange& aMcr
         ,  const std::vector<Uin>& aNModals
         ,  In aMode
         )
      {
//         // DEBUG
//         Mout  << " Calculate OmegaGDown for mode " << aMode << " with s vector:\n" << aClusterAmps << std::endl;

         using vec_t = GeneralMidasVector<T>;
         using mat_t = GeneralMidasMatrix<T>;
         using cont_t = CONT_TMPL<T>;

         auto nmodes = aGDownVec.size();

         if (  aMode >= nmodes
            )
         {
            MIDASERROR("Mode index out of range!");
         }

         auto dim = aGDownVec[aMode].Size();
         
         vec_t result(dim, T(0.0));

         // Add two-mode-amplitude terms
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            if (  imode == aMode
               )
            {
               continue;
            }
            auto mc = (imode < aMode) ? std::vector<In>{imode, aMode} : std::vector<In>{aMode, imode};
            ModeCombiOpRange::const_iterator it;
            if (  !aMcr.Find(mc, it)
               )
            {
               std::ostringstream oss;
               oss   << "ModeCombi: " << mc << " not found in MCR";
               MIDASERROR(oss.str());
            }
            auto addr = it->Address();
            
            In n0 = aNModals[mc[0]]-I_1;
            In n1 = aNModals[mc[1]]-I_1;
            mat_t s_2m(n0, n1);
            if constexpr   (  std::is_same_v<cont_t, GeneralMidasVector<T>>
                           )
            {
               In count = I_0;
               for(In irow=I_0; irow<n0; ++irow)
               {
                  for(In icol=I_0; icol<n1; ++icol)
                  {
                     s_2m[irow][icol] = aClusterAmps[addr+(count++)];
                  }
               }
//               // DEBUG
//               Mout  << " s^" << mc << " = " << s_2m << std::endl;
            }
            else if constexpr (  std::is_same_v<cont_t, GeneralDataCont<T>>
                              )
            {
               MIDASERROR("Implement DataIo!");
            }
            else
            {
               MIDASERROR("Not implemented for cont_t = " + std::string(typeid(cont_t).name()));
            }

            if (  aMode < imode
               )
            {
               result += vec_t(s_2m * aGDownVec[imode]);
            }
            else
            {
               vec_t tmp = aGDownVec[imode];
               tmp.LeftMultiplyWithMatrix(s_2m);
               result += tmp;
            }
         }

//         // Add correction term from one-mode amplitudes (although they should be zero)
//         std::vector<In> mc_1m = {aMode};
//         ModeCombiOpRange::const_iterator it_1m;
//         if (  !aMcr.Find(mc_1m, it_1m)
//            )
//         {
//            std::ostringstream oss;
//            oss   << "ModeCombi: " << mc_1m << " not found in MCR";
//            MIDASERROR(oss.str());
//         }
//         auto addr = it_1m->Address();
//         if constexpr   (  std::is_same_v<cont_t, GeneralMidasVector<T>>
//                        )
//         {
//            T dot(0.0);
//            for(In i=I_0; i<dim; ++i)
//            {
//               dot += aClusterAmps[i+addr] * aGDownVec[aMode][i];
//            }
//            for(In j=I_0; j<dim; ++j)
//            {
//               result -= dot*aClusterAmps[j+addr];
//            }
//         }
//         else
//         {
//            MIDASERROR("Not implemented for cont_t = " + std::string(typeid(cont_t).name()));
//         }

         return result;
      }

      /**
       * u vector (RHS for calculating g_up)
       *
       * @param arHMat        H matrix (NB: Will be modified in place)
       * @param aGDownMats    Matrices of g "down" operator
       * @param arSco         SparseClusterOper
       * @param arTAmpls      T
       * @param arLCoefs      L
       * @return
       *    vector of u vectors for each mode
       **/
      template
         <  typename T
         >
      std::vector<GeneralMidasVector<T>> UVector
         (  midas::util::matrep::OperMat<T>& arHMat
         ,  const std::vector<GeneralMidasMatrix<T>>& aGDownMats
         ,  const midas::util::matrep::SparseClusterOper& arSco
         ,  const GeneralMidasVector<T>& arTAmpls
         ,  const GeneralMidasVector<T>& arLCoefs
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::McrExciIndices;
         using vec_t = GeneralMidasVector<T>;

         // |ref> = (1,0,0,...)
         vec_t ref(arSco.FullDim(), T(0));
         ref[0] = 1;
         // |ket> = exp(T)|ref>
         vec_t ket(arSco.FullDim(), T(0));
         ket[0] = 1;
         ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, std::move(ket), T(+1));
         // <bra| = <ref|(1+L)exp(-T)
         vec_t bra = arSco.RefToFullSpace<false>(arLCoefs);
         bra[0] += 1;
         bra = TrfExpClusterOper<true,false>(arSco, arTAmpls, std::move(bra), T(-1));

         const vec_t H_ket = vec_t(arHMat * ket);
         const vec_t bra_H = vec_t(bra * arHMat);
         const vec_t expmT_H_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, H_ket, T(-1));

         auto& h_minus_gd = arHMat;
         h_minus_gd.AddSumOneModeOpers(aGDownMats, T(-1));
         const vec_t Hmg_ket = vec_t(h_minus_gd * ket);
         const vec_t bra_Hmg = vec_t(bra * h_minus_gd);
         const vec_t expmT_Hmg_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, Hmg_ket, T(-1));

         std::vector<vec_t> v_res;
         v_res.reserve(arSco.NumModes());
         const auto& n_modals = arSco.Dims();
         for(const auto& nm: n_modals)
         {
            const Uin mode = v_res.size();
            std::set<Uin> sm = {mode};
            std::vector<Uin> cr{0};
            std::vector<Uin> an(1);
            vec_t v(nm-1, T(0));
            for(Uin a = 0; a < v.Size(); ++a)
            {
               an.at(0) = a+1;
               // <bra|[H,E^m_ia]|ket>
               const T comm_val = ShiftOperBraket<false>(bra_H, ket, n_modals, sm, cr, an)
                                - ShiftOperBraket<false>(bra, H_ket, n_modals, sm, cr, an);
               //  sum_mu
               //    ( <bra|[E^m_ia, tau_mu]|ket> <mu'|exp(-T)(H-g_d)|ket>
               //    - <bra|[(H-g_d), tau_mu]|ket> <mu'|exp(-T)E^m_ia |ket>
               //    )
               T add = 0;
               MatRepVibOper<T> mr(n_modals);
               mr.ShiftOper(sm, cr, an);
               const auto E_m_ia = std::move(mr).GetMatRep();
               const vec_t bra_E_m_ia = vec_t(bra * E_m_ia);
               const vec_t E_m_ia_ket = vec_t(E_m_ia * ket);
               const vec_t expmT_E_m_ia_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, E_m_ia_ket, T(-1));
               for(const auto& [modes, excis]: McrExciIndices(arSco.Mcr(), arSco.Dims()))
               {
                  if (modes.size() < 2)
                  {
                     continue;
                  }
                  const std::vector<Uin> iref(modes.size(), 0);
                  for(const auto& exci: excis)
                  {
                     T E_mu   = ShiftOperBraket<false>(bra_E_m_ia, ket, n_modals, modes, exci, iref)
                              - ShiftOperBraket<false>(bra, E_m_ia_ket, n_modals, modes, exci, iref);
                     T Hmg_mu = ShiftOperBraket<false>(bra_Hmg, ket, n_modals, modes, exci, iref)
                              - ShiftOperBraket<false>(bra, Hmg_ket, n_modals, modes, exci, iref);
                     T e_Hmg  = ShiftOperBraket<false>(ref, expmT_Hmg_ket, n_modals, modes, iref, exci);
                     T e_E    = ShiftOperBraket<false>(ref, expmT_E_m_ia_ket, n_modals, modes, iref, exci);
                     add += E_mu * e_Hmg - Hmg_mu * e_E;
                  }
               }

               v[a] = comm_val + add;
            }
            v_res.emplace_back(std::move(v));
         }

         return v_res;
      }

   }; /* namespace constraint */

} /* namespace midas::tdvcc */


#endif/*TRFTDVCCUTILS_H_INCLUDED*/
