/**
 *******************************************************************************
 * 
 * @file    TrfVariationalConstraintMatRep_Impl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFVARIATIONALCONSTRAINTMATREP_IMPL_H_INCLUDED
#define TRFVARIATIONALCONSTRAINTMATREP_IMPL_H_INCLUDED

#include "mmv/ContainerMathWrappers.h"
#include "libmda/numeric/float_eq.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep_Decl.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"

namespace midas::tdvcc
{
   /*********************************************************
    * c-tor
    *
    * @param aNModals      Number of time-dependent modals
    * @param aMcr          MCR of included configurations
    *********************************************************/
   template
      <  typename PARAM_T
      >
   TrfVariationalConstraintMatRep<PARAM_T>::TrfVariationalConstraintMatRep
      (  const n_modals_t& aNModals
      ,  const ModeCombiOpRange& aMcr
      )
      :  TrfConstraintMatRepBase<PARAM_T>
            (  aNModals
            ,  aMcr
            )
   {
   }

   /*********************************************************
    * Parse intermediates.
    * Extract omega^H from intermediate derivative of T_1 and eta^H as int. deriv. of L_1.
    *
    * @param aOmegaHs      aAmplsFactor*<mu|exp(-T) H exp(T)|ref>
    * @param aEtaHs        aCoefsFactor*<Psi'|[H,tau_mu]|Psi>
    * @param aHMatrix      Reference to Hamiltonian matrix
    * @param aAmplsFactor  Factor that aOmegaHs has been scaled with
    * @param aCoefsFactor  Factor that aEtaHs has been scaled with
    *********************************************************/
   template
      <  typename PARAM_T
      >
   void TrfVariationalConstraintMatRep<PARAM_T>::ParseIntermediates
      (  const cont_t& aOmegaHs
      ,  const cont_t& aEtaHs
      ,  fullspace_mat_t&& aHMatrix
      ,  PARAM_T aAmplsFactor
      ,  PARAM_T aCoefsFactor
      )
   {
      const auto& mcr = this->Mcr();
      auto nmodes = this->NModes();

      std::vector<vec_t> eta_H_vec = ExtractOneModeAmplitudes(aEtaHs, this->Mcr(), this->NModals());
      auto sfc = PARAM_T(1.0)/aCoefsFactor;
      midas::mmv::WrapScale(eta_H_vec, sfc);
      std::vector<vec_t> omega_H_vec = ExtractOneModeAmplitudes(aOmegaHs, this->Mcr(), this->NModals());
      auto sfa = PARAM_T(1.0)/aAmplsFactor;
      midas::mmv::WrapScale(omega_H_vec, sfa);

      if (  eta_H_vec.size() != nmodes
         || eta_H_vec.size() != omega_H_vec.size()
         )
      {
         Mout  << " N_modes = " << nmodes << "\n"
               << " MCR:\n" << mcr << std::endl;
         MIDASERROR("Some one-mode amplitudes are missing. Check that all one-mode MCs are included in the MCR!");
      }

      // Insert
      this->mIntermediates[TdmvccIntermedType::ETAH] = std::move(eta_H_vec);
      this->mIntermediates[TdmvccIntermedType::OMEGAH] = std::move(omega_H_vec);
      this->mHMatrix = std::make_unique<fullspace_mat_t>(std::move(aHMatrix));
   }

   /*********************************************************
    *
    *********************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfVariationalConstraintMatRep<PARAM_T>::gmats_t
   TrfVariationalConstraintMatRep<PARAM_T>::ConstraintMatrices
      (  step_t aT
      ,  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      ,  const dmats_t& aDensMats
      )  const
   {
      auto nmodes = this->NModes();

      bool verbose = (this->IoLevel() >= 10);

      // 1) Solve "down" equations: Z^m g^m_d = eta^{H,m} for all modes. 
      // RHS (\eta^H) has been calculated as derivative of L_1.
      if (verbose) Mout << " Solve 'down' equations " << std::endl;
      const auto& eta_H_vec = this->mIntermediates.at(TdmvccIntermedType::ETAH).template get<std::vector<vec_t>>();
      auto [gd_vec, z_vec] = constraint::SolveVariationalGDownEquations(aDensMats, eta_H_vec, this->GOptRegularization().first, this->GOptRegularization().second, verbose);

      // 2) Solve "up" equations: Z^{m T} g^m_u = -u^m, (_or_ set elements directly: g^m_u = \omega^H - \omega^{g_d}, which is _not_ fully variational!)
      // \omega^H has been calculated as derivative of T_1
      const auto& omega_H_vec = this->mIntermediates.at(TdmvccIntermedType::OMEGAH).template get<std::vector<vec_t>>();
      std::vector<vec_t> gu_vec(nmodes);
      if (  this->DirectGUp()
         )
      {
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            // Add H term
            gu_vec[imode].SetNewSize(omega_H_vec[imode].Size());
            gu_vec[imode] = omega_H_vec[imode];

            // Subtract g "down" term
            gu_vec[imode] -= constraint::OmegaGDown(gd_vec, aClusterAmps, this->Mcr(), this->NModals(), imode);
         }
      }
      else
      {
         if (verbose) Mout << " Solve 'up' equations " << std::endl;
         auto& hmat = *this->mHMatrix.get();
         auto gd_mats = this->ConstructGMatrices(gd_vec);

         auto u = constraint::UVector(hmat, gd_mats, this->SpClusterOp(), aClusterAmps, aLambdaCoefs);
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            auto zt = Transpose(z_vec[imode]);
            const auto& um = u[imode];
            gu_vec[imode] = constraint::SolveLinearEquations(zt, um, this->GOptRegularization().first, this->GOptRegularization().second, verbose);
            gu_vec[imode].Scale(param_t(-1.0));
         }
         if (verbose) Mout << std::endl;
      }

      // 3) Return result
      return this->ConstructGMatrices(gd_vec, gu_vec);
   }

}; /* namespace midas::tdvcc */

#endif /* TRFVARIATIONALCONSTRAINTMATREP_IMPL_H_INCLUDED */
