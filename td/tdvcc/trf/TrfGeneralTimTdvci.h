/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvci.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFGENERALTIMTDVCI_H_INCLUDED
#define TRFGENERALTIMTDVCI_H_INCLUDED

#include "TrfGeneralTimTdvci_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "TrfGeneralTimTdvci_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TRFGENERALTIMTDVCI_H_INCLUDED */
