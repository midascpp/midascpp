/**
 *******************************************************************************
 * 
 * @file    TrfTdextvccMatRep_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDEXTVCCMATREP_IMPL_H_INCLUDED
#define TRFTDEXTVCCMATREP_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasVector.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdextvccMatRep<PARAM_T>::cont_t TrfTdextvccMatRep<PARAM_T>::HamDerExtAmp
      (  step_t aTime
      ,  const cont_t& arClustAmps
      ,  const cont_t& arExtAmps
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfExtVccHamDerExtAmp(this->OperMat(), this->SparseClusterOper(), arClustAmps, arExtAmps);
      this->PrintTimings("TrfTdextvccMatRep", "HamDerExtAmp", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdextvccMatRep<PARAM_T>::cont_t TrfTdextvccMatRep<PARAM_T>::HamDerClustAmp
      (  step_t aTime
      ,  const cont_t& arClustAmps
      ,  const cont_t& arExtAmps
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfExtVccHamDerClustAmp(this->OperMat(), this->ShiftOperBraketLooper(), this->SparseClusterOper(), arClustAmps, arExtAmps);
      this->PrintTimings("TrfTdextvccMatRep", "HamDerClustAmp", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdextvccMatRep<PARAM_T>::cont_t TrfTdextvccMatRep<PARAM_T>::LeftExpmS
      (  const cont_t& arVec
      ,  const cont_t& arExtAmps
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfExtVccLeftExpmS(this->SparseClusterOper(), arExtAmps, arVec);
      this->PrintTimings("TrfTdextvccMatRep", "LeftExpmS", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTdextvccMatRep<PARAM_T>::cont_t TrfTdextvccMatRep<PARAM_T>::RightExpmS
      (  const cont_t& arVec
      ,  const cont_t& arExtAmps
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfExtVccRightExpmS(this->SparseClusterOper(), arExtAmps, arVec);
      this->PrintTimings("TrfTdextvccMatRep", "RightExpmS", timer);

      return result;
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDEXTVCCMATREP_IMPL_H_INCLUDED*/
