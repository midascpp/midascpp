/**
 *******************************************************************************
 * 
 * @file    TrfTdvciMatRep.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdvciMatRep_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDVCIMATREP(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfTdvciMatRep<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDVCIMATREP(Nb);
INSTANTIATE_TRFTDVCIMATREP(std::complex<Nb>);

#undef INSTANTIATE_TRFTDVCIMATREP
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
