/**
 *******************************************************************************
 * 
 * @file    TrfConstraintBase_Impl.h
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTBASE_IMPL_H_INCLUDED
#define TRFCONSTRAINTBASE_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/trf/TrfConstraintBase.h"

namespace midas::tdvcc
{
   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfConstraintBase<PARAM_T>::modals_pair_t
   TrfConstraintBase<PARAM_T>::ActiveSpaceModalDerivative
      (  step_t aTime
      ,  const mat_t& aG
      ,  const modals_pair_t& aModals
      ,  PARAM_T aKetFactor
      ,  PARAM_T aBraFactor
      )  const
   {
      auto u = aModals.first;
      auto w = aModals.second;

      u.Scale(aKetFactor);
      w.Scale(aBraFactor);

      return std::make_pair(mat_t(u*aG), mat_t(aG*w));
   }

   /*******************************************************
    *
    *******************************************************/
   template
      <  typename PARAM_T
      >
   std::vector<typename TrfConstraintBase<PARAM_T>::mat_t>
   TrfConstraintBase<PARAM_T>::ConstructGMatrices
      (  const std::vector<vec_t>& aGDown
      ,  const std::vector<vec_t>& aGUp
      )  const
   {
      auto n_modes = this->NModes();
      std::vector<mat_t> g_mats(n_modes);
      for(In imode=I_0; imode<n_modes; ++imode)
      {
         auto& gm = g_mats[imode];
         const auto& n = this->mrNModals[imode];
         gm.SetNewSize(n, n, false, true);
         for(In a=I_1; a<n; ++a)
         {
            if (  !aGDown.empty()
               )
            {
               gm[0][a] = aGDown[imode][a-1];
            }
            if (  !aGUp.empty()
               )
            {
               gm[a][0] = aGUp[imode][a-1];
            }
         }
      }

      return g_mats;
   }

}; /* namespace midas::tdvcc */

#endif /* TRFCONSTRAINTBASE_IMPL_H_INCLUDED */

