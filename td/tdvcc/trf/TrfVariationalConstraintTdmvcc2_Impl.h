/**
 *******************************************************************************
 * 
 * @file    TrfVariationalConstraintTdmvcc2_Impl.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFVARIATIONALCONSTRAINTTDMVCC2_IMPL_H_INCLUDED
#define TRFVARIATIONALCONSTRAINTTDMVCC2_IMPL_H_INCLUDED

#include "mmv/ContainerMathWrappers.h"
#include "libmda/numeric/float_eq.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2_Decl.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"

namespace midas::tdvcc
{
   /*********************************************************
    * c-tor
    *
    * @param aNModals      Number of time-dependent modals
    * @param aMcr          MCR of included configurations
    *********************************************************/
   template
      <  typename PARAM_T
      >
   TrfVariationalConstraintTdmvcc2<PARAM_T>::TrfVariationalConstraintTdmvcc2
      (  const n_modals_t& aNModals
      ,  const ModeCombiOpRange& aMcr
      )
      :  TrfConstraintTdmvcc2Base<PARAM_T>
            (  aNModals
            ,  aMcr
            )
   {
   }

   /*********************************************************
    * Parse intermediates.
    * Extract eta^H as int. deriv. of L_1.
    *
    * @param aEtaHs        aCoefsFactor*<Psi'|[H,tau_mu]|Psi>
      @param aCoefsFactor  Factor multiplied on the eta vectors
    * @param aMeanFields   Mean-field matrices (in td-basis)
    *********************************************************/
   template
      <  typename PARAM_T
      >
   void TrfVariationalConstraintTdmvcc2<PARAM_T>::ParseIntermediates
      (  const cont_t& aEtaHs
      ,  PARAM_T aCoefsFactor
      ,  meanfields_t&& aMeanFields
      )
   {
      const auto& mcr = this->Mcr();
      auto nmodes = this->NModes();

      std::vector<vec_t> eta_H_vec = ExtractOneModeAmplitudes(aEtaHs, this->Mcr(), this->NModals());
      auto sf = PARAM_T(1.0)/aCoefsFactor;
      midas::mmv::WrapScale(eta_H_vec, sf);

      if (  eta_H_vec.size() != nmodes
         )
      {
         Mout  << " N_modes = " << nmodes << "\n"
               << " MCR:\n" << mcr << std::endl;
         MIDASERROR("Some one-mode amplitudes are missing. Check that all one-mode MCs are included in the MCR!");
      }

      // Insert
      this->mIntermediates[TdmvccIntermedType::ETAH] = std::move(eta_H_vec);
      this->mIntermediates[TdmvccIntermedType::MEANFIELDS] = std::move(aMeanFields);
   }

   /*********************************************************
    *
    *********************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfVariationalConstraintTdmvcc2<PARAM_T>::gmats_t
   TrfVariationalConstraintTdmvcc2<PARAM_T>::ConstraintMatrices
      (  step_t aT
      ,  const cont_t& aClusterAmps
      ,  const cont_t& aLambdaCoefs
      ,  const dmats_t& aDensMats
      )  const
   {
      auto nmodes = this->NModes();

      bool verbose = (this->IoLevel() >= 10);

      // 1) Solve "down" equations: Z^m g^m_d = eta^{H,m} for all modes. 
      // RHS (\eta^H) has been calculated as derivative of L_1.
      if (verbose) Mout << " Solve 'down' equations " << std::endl;
      const auto& eta_H_vec = this->mIntermediates.at(TdmvccIntermedType::ETAH).template get<std::vector<vec_t>>();
      auto [gd_vec, z_vec] = constraint::SolveVariationalGDownEquations(aDensMats, eta_H_vec, this->GOptRegularization().first, this->GOptRegularization().second, verbose);

      // 2) Solve "up" equations: Z^{m T} g^m_u = -u^m 
      if (verbose) Mout << " Solve 'up' equations " << std::endl;
      std::vector<vec_t> gu_vec(nmodes);
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         auto zt = Transpose(z_vec[imode]);
         auto um = this->UVector(imode);
         gu_vec[imode] = constraint::SolveLinearEquations(zt, um, this->GOptRegularization().first, this->GOptRegularization().second, verbose);
         gu_vec[imode].Scale(param_t(-1.0));
      }
      if (verbose) Mout << std::endl;

      // 3) Return result
      return this->ConstructGMatrices(gd_vec, gu_vec);
   }

   /*********************************************************
    * @brief
    *    Construct the u^m vector from mean-field matrices
    * 
    * @param aMode   The mode to construct u^m for
    * @return
    *    u^m
    *********************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfVariationalConstraintTdmvcc2<PARAM_T>::vec_t
   TrfVariationalConstraintTdmvcc2<PARAM_T>::UVector
      (  In aMode
      )  const
   {
      // Number of virtual modals (size of u^m)
      auto nvir = this->NModals()[aMode] - I_1;

      // Get mean-field matrices (in time-dependent basis) for aMode. Ordered as: {F, F'}.
      const auto& f = this->mIntermediates.at(TdmvccIntermedType::MEANFIELDS).template get<meanfields_t>()[aMode];
      vec_t u(nvir);
      for(In a=I_0; a<nvir; ++a)
      {
         u[a] = f[1][a+I_1][I_0] - f[0][a+I_1][I_0]; // u^m_{a} = F'^m_{ai} - F^m_{ai}
      }

      return u;
   }

}; /* namespace midas::tdvcc */

#endif /* TRFVARIATIONALCONSTRAINTTDMVCC2_IMPL_H_INCLUDED */

