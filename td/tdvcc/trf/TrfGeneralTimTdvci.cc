/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvci.cc
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "TrfGeneralTimTdvci.h"
#include "TrfGeneralTimTdvci_Impl.h"

// Define instatiation macro.
#define INSTANTIATE_TRFGENERALTIMTDVCI(PARAM_T)    \
   namespace midas::tdvcc                          \
   {                                               \
      template class TrfGeneralTimTdvci<PARAM_T>;  \
   } /* namespace midas::tdvcc */                  \
   

// Instantiations.
INSTANTIATE_TRFGENERALTIMTDVCI(Nb);
INSTANTIATE_TRFGENERALTIMTDVCI(std::complex<Nb>);

#undef INSTANTIATE_TRFGENERALTIMTDVCI

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
