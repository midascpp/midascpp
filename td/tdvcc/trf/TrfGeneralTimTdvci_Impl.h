/**
 *******************************************************************************
 * 
 * @file    TrfGeneralTimTdvci_Impl.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFGENERALTIMTDVCI_IMPL_H_INCLUDED
#define TRFGENERALTIMTDVCI_IMPL_H_INCLUDED

#include "TrfGeneralTimTdvci_Decl.h"
#include "TimTdvccEvalData.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/IntermedProd.h"

namespace midas::tdvcc
{

/**
 * Initialize
 *
 * @note
 *    mContribs cannot be initialized yet, because mV3cFilePrefix is not passed in the c-tor
 **/
template
   <  typename PARAM_T
   >
TrfGeneralTimTdvci<PARAM_T>::TrfGeneralTimTdvci
   (  const n_modals_t& arNModals
   ,  const opdef_t& arOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   )
   :  mrNModals(arNModals)
   ,  mrOpDef(arOpDef)
   ,  mrModInts(arModInts)
   ,  mrMcr(arMcr)
{
}

/**
 * Calculate Hamiltonian transformation
 *
 * @param aTime
 * @param arCoefs
 * @return
 *    H*C
 **/
template
   <  typename PARAM_T
   >
typename TrfGeneralTimTdvci<PARAM_T>::cont_t TrfGeneralTimTdvci<PARAM_T>::Transform
   (  step_t aTime
   ,  const cont_t& arCoefs
   )  const
{
   // Clear intermediates
   this->mIntermediates.ClearIntermeds();

   // Initialize evaldata
   evaldata_t evaldata
      (  this->mIntermediates
      ,  this->ModInts()
      ,  this->Oper()
      ,  this->Mcr()
      ,  this->NModals()
      ,  &arCoefs
      );

   // Evaluate
   return this->EvaluateContribs(this->mContribs, evaldata);
}

/**
 * Initialize mContribs
 **/
template
   <  typename PARAM_T
   >
void TrfGeneralTimTdvci<PARAM_T>::InitializeContribs
   (
   )
{
   this->mContribs = this->InitializeV3(v3_t::VCI, this->Mcr().GetMaxExciLevel(), this->Oper().McLevel(), this->mIntermediates);
}

} /* namespace midas::tdvcc */



#endif /* TRFGENERALTIMTDVCI_IMPL_H_INCLUDED */
