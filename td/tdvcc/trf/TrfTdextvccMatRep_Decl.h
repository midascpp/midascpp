/**
 *******************************************************************************
 * 
 * @file    TrfTdextvccMatRep_Decl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDEXTVCCMATREP_DECL_H_INCLUDED
#define TRFTDEXTVCCMATREP_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdvccMatRepBase.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTdextvccMatRep
      :  public TrfTdvccMatRepBase<PARAM_T, TrfTdvccBase>
   {
      public:
         using Base = TrfTdvccMatRepBase<PARAM_T, TrfTdvccBase>;
         using typename Base::param_t;
         using typename Base::step_t;
         using typename Base::opdef_t;
         using typename Base::modalintegrals_t;
         using typename Base::n_modals_t;
         using typename Base::cont_t;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTdextvccMatRep(Args&&... args)
            :  Base(std::forward<Args>(args)...)
         {
         }

         cont_t HamDerExtAmp(step_t aTime, const cont_t& arClustAmps, const cont_t& arExtAmps) const;
         cont_t HamDerClustAmp(step_t aTime, const cont_t& arClustAmps, const cont_t& arExtAmps) const;
         cont_t LeftExpmS(const cont_t& arVec, const cont_t& arExtAmps) const;
         cont_t RightExpmS(const cont_t& arVec, const cont_t& arExtAmps) const;

   };

} /* namespace midas::tdvcc */


#endif/*TRFTDEXTVCCMATREP_DECL_H_INCLUDED*/
