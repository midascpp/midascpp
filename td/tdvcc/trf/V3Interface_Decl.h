/**
 *******************************************************************************
 * 
 * @file    V3Interface_Decl.h
 * @date    13-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef V3INTERFACE_DECL_H_INCLUDED
#define V3INTERFACE_DECL_H_INCLUDED

// Standard headers
#include <memory>
#include <vector>

// Midas headers
#include "inc_gen/TypeDefs.h"

// Forward declarations
template<typename> class GeneralTensorDataCont;
template<typename> class NiceTensor;
template<typename> class GeneralMidasVector;

namespace midas::vcc::v3
{
template<typename, template<typename...> typename> class V3Contrib;
template<typename, template<typename...> typename> class IntermedProd;
template<typename, template<typename...> typename> class IntermedProdL;
template<typename, template<typename> typename, template<typename...> typename> class IntermediateMachine;
} /* namespace midas::vcc::v3 */

namespace midas::tdvcc
{

// Forward declarations in namespace
template<typename> class TimTdvccEvalData;

namespace detail
{

/**
 * Class that handles reading and writing of v3c files
 **/
template
   <  typename PARAM_T
   >
class V3Interface
{
   public:
      /** @name Alias **/
      //!@{
      using param_t = PARAM_T;
      template<typename U> using cont_tmpl = GeneralTensorDataCont<U>;
      using cont_t = cont_tmpl<param_t>;
      using evaldata_t = TimTdvccEvalData<param_t>;
      using intermeds_t = midas::vcc::v3::IntermediateMachine<param_t, NiceTensor, TimTdvccEvalData>;
      using v3contrib_t = midas::vcc::v3::V3Contrib<param_t, TimTdvccEvalData>;
      using intermedprod_t = midas::vcc::v3::IntermedProd<param_t, TimTdvccEvalData>;
      using intermedprodl_t = midas::vcc::v3::IntermedProdL<param_t, TimTdvccEvalData>;
      using contribs_t = std::vector<std::unique_ptr<v3contrib_t> >;
      using midasvector_t = GeneralMidasVector<param_t>;
      //¡@}

      //! Types of transformations
      enum class V3Type : int
      {  ERRVEC = 0     // VCC error vector
      ,  ETAVEC         // VCC eta
      ,  LJAC           // VCC left-hand Jacobian transform
      ,  VCI            // VCI transform
      };

      //! Default c-tor
      V3Interface() = default;

      /** @name Public getters **/
      //!@{
      const std::string& V3cFilePrefix() const  {return mV3cFilePrefix;}
      std::string& V3cFilePrefix()              {return mV3cFilePrefix;}
      Uin IoLevel() const                       {return mIoLevel;}
      Uin& IoLevel()                            {return mIoLevel;}
      bool TimeIt() const                       {return mTimeIt;}
      bool& TimeIt()                            {return mTimeIt;}
      //!@}

   protected:
      //! Initialize V3 contribs of given type
      contribs_t InitializeV3
         (  V3Type aType
         ,  In aMaxExci
         ,  In aOperMcLevel
         ,  intermeds_t& arIntermeds
         )  const;

      //! Generate v3c file from scratch
      contribs_t GenerateV3cFile
         (  V3Type aType
         ,  In aMaxExci
         ,  In aOperMcLevel
         ,  const std::string& aFileName
         )  const;

      //! Evaluate contribs
      cont_t EvaluateContribs
         (  const contribs_t& aContribs
         ,  const evaldata_t& aEvalData
         )  const;

      //! Extract one-mode amplitudes for T1 transformation
      std::vector<midasvector_t> ExtractOneModeAmplitudes
         (  const cont_t& aAmplitudes
         ,  const std::vector<Uin>& aNModals
         )  const;

   private:
      //! File prefix for v3c files
      std::string mV3cFilePrefix = "";

      //! IO level
      Uin mIoLevel = 0;

      //! Do detailed timings
      bool mTimeIt = false;
};

} /* namespace detail */
} /* namespace midas::tdvcc */

#endif /* V3INTERFACE_DECL_H_INCLUDED */

