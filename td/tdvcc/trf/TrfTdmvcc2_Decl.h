/**
 *******************************************************************************
 * 
 * @file    TrfTdmvcc2_Decl.h
 * @date    06-10-2020
 * @author  Andreas Buchgraitz Jensen (buchgraitz@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCC2_DECL_H_INCLUDED
#define TRFTDMVCC2_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

// Forward declarations.
namespace midas::tdvcc
{
   template
      <  typename PARAM_T
      >
   class TrfTdmvcc2
      :  public TrfTdmvccBase<PARAM_T>
   {
      public:
         using typename TrfTdmvccBase<PARAM_T>::param_t;
         using typename TrfTdmvccBase<PARAM_T>::step_t;
         using typename TrfTdmvccBase<PARAM_T>::opdef_t;
         using typename TrfTdmvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTdmvccBase<PARAM_T>::n_modals_t;
         using typename TrfTdmvccBase<PARAM_T>::vec_t;
         using typename TrfTdmvccBase<PARAM_T>::mat_t;
         using typename TrfTdmvccBase<PARAM_T>::mat_v_t;
         using typename TrfTdmvccBase<PARAM_T>::meanfields_t;
         using typename TrfTdmvccBase<PARAM_T>::modal_cont_t;
         template<typename U> using cont_tmpl = GeneralDataCont<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr TrfType trf_type = TrfType::VCC2H2;

         using intermeds_t = TwoModeIntermeds<param_t, opdef_t>;
         using InPair = std::pair<In, In>;
         using MapInPairs = std::map<InPair, InPair>;  

         //! Forward all constructor calls to base class.
         template<class... Args> 
         TrfTdmvcc2(Args&&... args)
            :  TrfTdmvccBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         //! Calculate error vector
         cont_t ErrVec
            (  step_t aTime
            ,  const cont_t& arAmpls
            ,  bool aSaveIntermeds = true
            )  const;

         //! Calculate eta vector
         cont_t EtaVec
            (  step_t aTime
            ,  const cont_t& arAmpls
            )  const;

         //! Calculate LJac
         cont_t LJac
            (  step_t aTime
            ,  const cont_t& arAmpls
            ,  const cont_t& arCoefs
            ,  bool aReuseIntermeds = true
            )  const;

         //! Calculate expectation value (do not save intermediates)
         inline cont_t ExpValTrf(step_t aTime, const cont_t& arAmpls) const {return ErrVec(aTime, arAmpls, false);}

         //! Calculate density matricies
         mat_v_t DensityMatrices
            (  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;

         //! Calculate mean fields: {H_1, F_R, H'_1, F'_R}
         meanfields_t MeanFieldMatrices
            (  step_t aTime
            ,  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;
         
         //! Calculate mean fiels in td basis for full active basis method {F, F'}.
         meanfields_t MeanFieldMatricesFullActiveBasis
            (  step_t aTime
            ,  const cont_t& aClusterAmps
            ,  const cont_t& aLambdaCoefs
            )  const;

         //! Clear intermediates
         void ClearIntermeds() { mIntermeds = nullptr; }
      
      private:
         //! Intermeds saved from ErrVec calculation
         mutable std::unique_ptr<intermeds_t> mIntermeds = nullptr;
         void GetAmplData(const param_t*& aData, vec_t ampl, const Uin size, const cont_t& Ampl, const In m1, const In m2) const; // Retrieves lambdacoefs as param_t pointer for gemm call
         //mat_t GetAmplMat(const Uin Nrows, const Uin Ncols, const cont_t& Ampl, const In m1, const In m2) const; // Retrieves lambdacoefs as vector for gemm call
         void GetAmplMat(mat_t& arMat, const Uin Nrows, const Uin Ncols, const cont_t& Ampl, const In m1, const In m2) const; // Retrieves lambdacoefs as vector for gemm call

         param_t SmartTripleTrace(const mat_t& A, const mat_t& B, const mat_t& C, const bool trans_a = false, const bool trans_b = false, const bool trans_c = false) const;

         //Calculates intermediates dependend on operators.
         void CalculateOperIntermeds
            (  const MapInPairs& aMCsOpers
            ,  const cont_t& aLambdaCoefs
            ,  const cont_t& aClusterAmps
            ,  Nb& time_w
            ,  Nb& time_ws
            ,  Nb& time_ws_fab
            ,  Nb& time_xl
            ,  Nb& time_3bch
            ,  Nb& time_trace_v
            ,  bool aFullActiveBasis = false
            ) const;

         // Functions to calculate contributions to meanfields.
         vec_t MeanFieldRefContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;   
         mat_t MeanFieldVirtualContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;   

         vec_t MeanFieldPrimeRefContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;
         mat_t MeanFieldPrimeVirtualContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;

         // Contributions for full active basis method
         vec_t MeanFieldFullActiveBasisContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;
         vec_t MeanFieldPrimeFullActiveBasisContrib(const In am, const In am0, const In aOperMode_m, const In aOperMode_m0, const Nb aCoefficientMM0, const cont_t& aLambdaCoefs, const cont_t& aClusterAmps) const;
   };
}  /* namespace midas::tdvcc */


#endif/*TRFTDMVCC2_DECL_H_INCLUDED*/