/**
 *******************************************************************************
 * 
 * @file    TrfConstraintBase.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TRFCONSTRAINTBASE_H_INCLUDED
#define TRFCONSTRAINTBASE_H_INCLUDED

#include "td/tdvcc/trf/TrfConstraintBase_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/trf/TrfConstraintBase_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */


#endif /* TRFCONSTRAINTBASE_H_INCLUDED */
