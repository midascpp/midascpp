/**
 *******************************************************************************
 * 
 * @file    TrfTdvccBase_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDVCCBASE_IMPL_H_INCLUDED
#define TRFTDVCCBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   TrfTdvccBase<PARAM_T>::TrfTdvccBase
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  mrNModals(arNModals)
      ,  mrOpDef(arOpDef)
      ,  mrModInts(arModInts)
      ,  mrMcr(arMcr)
   {
      this->SanityCheck();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdvccBase<PARAM_T>::PrintTimings
      (  const std::string& arClassName
      ,  const std::string& arFuncName
      ,  Timer& arTimer
      )  const
   {
      if (this->TimeIt())
      {
         std::stringstream ss;
         ss << arClassName << "<" << midas::type_traits::TypeName<PARAM_T>() << ">::"
            << arFuncName << "(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         arTimer.CpuOut (Mout, ss.str()+"CPU  time = ");
         arTimer.WallOut(Mout, ss.str()+"Wall time = ");
      }
   }

   /************************************************************************//**
    * Checks:
    *    -  Mcr() must contain the empty ModeCombi (for reference element),
    *    otherwise the transformer will e.g. not be able to compute expectation
    *    values.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTdvccBase<PARAM_T>::SanityCheck
      (
      )  const
   {
      std::stringstream ss;
      bool err = false;
      if (this->Mcr().NumEmptyMCs() == 0)
      {
         err = true;
         ss << "ModeCombiOpRange must contain the empty ModeCombi but doesn't.\n"
            << "this->Mcr() = \n" << this->Mcr() << "\n";
      }
      if (  this->Mcr().NumberOfModes() != this->NModals().size()
         )
      {
         err = true;
         ss << "ModeCombiOpRange has different number of modes than NModals vector:\n"
            << "   - NModes(MCR)    = " << this->Mcr().NumberOfModes() << "\n"
            << "   - NModals.size() = " << this->NModals().size() << "\n";
      }

      if (  err
         )
      {
         MIDASERROR(ss.str());
      }
   }

} /* namespace midas::tdvcc */




#endif/*TRFTDVCCBASE_IMPL_H_INCLUDED*/
