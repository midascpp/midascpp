/**
 *******************************************************************************
 * 
 * @file    ConstraintOpDef.h
 * @date    15-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef CONSTRAINTOPDEF_H_INCLUDED
#define CONSTRAINTOPDEF_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Error.h"

namespace midas::tdvcc::constraint
{
   /**
    * @brief Class implementing the necessary functions from OpDef used in the vcc2h2 framework
    */
   class ConstraintOpDef
   {
      public:
         //! c-tor
         ConstraintOpDef
            (  In aNModes
            )
            :  mNModes(aNModes)
         {
         }

         //! Name
         std::string Name() const { return "TDMVCC_G_OPERATOR"; }

         //! MC level
         constexpr In McLevel() const { return I_1; }

         //! Get number of modes
         In NmodesInOp() const { return this->mNModes; }

         //! Get number of terms
         In Nterms() const { return this->mNModes; }

         //! Operator for a mode in term
         In OperForOperMode
            (  In aTerm
            ,  LocalModeNr aOperMode
            )  const
         {
            if (  aTerm >= this->mNModes
               || aOperMode >= this->mNModes
               )
            {
               MIDASERROR("Term or mode exceeds mNModes.");
            }
            return I_0;
         }

         //! Get number of one-mode opers
         constexpr In NrOneModeOpers(In) const { return I_1; }

         //! Is mode active in term. Mode m is only active in the m'th term.
         bool IsModeActiveInTerm
            (  const LocalModeNr aLocalMode
            ,  const In aTerm
            )  const
         {
            return aLocalMode == aTerm;
         }

         //! Get number of active terms
         constexpr In NactiveTerms(In) const { return I_1; }

         //! Get term number of i'th active term for mode
         constexpr In TermActive
            (  In aMode
            ,  In aActiveTermIdx
            )  const
         {
            if (aActiveTermIdx != I_0)
            {
               MIDASERROR("g operator only contains _one_ active term per mode.");
            }
            if (aMode >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return aMode;
         }

         //! Coefficient in front of term. Always one.
         constexpr Nb Coef
            (  In aTerm
            )  const
         {
            if (aTerm >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return 1.0;
         }

         //! Number of factors in a term
         constexpr In NfactorsInTerm
            (  In aTerm
            )  const
         {
            if (aTerm >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return I_1;
         }

         //! Mode that a given factor operates on in a term
         constexpr In ModeForFactor
            (  In aTerm
            ,  In aFactor
            )  const
         {
            if (aFactor != I_0)
            {
               MIDASERROR("g operator contains _one_ factor per term.");
            }
            if (aTerm >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return aTerm;
         }

         //! Operator number for a given factor. Always zero, as we only have _one_ g operator per mode.
         constexpr In OperForFactor
            (  In aTerm
            ,  In aFactor
            )  const
         {
            if (aFactor != I_0)
            {
               MIDASERROR("g operator contains _one_ factor per term.");
            }
            if (aTerm >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return I_0;
         }

         //! Global mode number (equal to local)
         GlobalModeNr GetGlobalModeNr
            (  LocalModeNr aMode
            )  const
         {
            if (aMode >= mNModes)
            {
               MIDASERROR("Mode number out of range.");
            }
            return (int)aMode;
         }

         //! Terms for MC
         bool TermsForMc
            (  const ModeCombi& aMc
            ,  In& arFirstTerm
            ,  In& arNTerms
            )  const
         {
            if (  aMc.Size() != I_1
               )
            {
               arFirstTerm = -I_1;
               arNTerms = -I_1;
               return false;
            }
            else
            {
               auto mode = aMc.Mode(I_0);
               arFirstTerm = mode;
               arNTerms = I_1;
               return true;
            }
         }

         /**
         * returnes Local operator numbers for a given mode combination
         **/
         std::vector<GlobalOperNr> OpersForMc(const ModeCombi& arModeCombi) const
         {
           vector<GlobalOperNr> subset_mc;
           for (In i_term=I_0 ; i_term < Nterms(); i_term++)
           {
              // This class is only used in vcc2h2 (at least for now) so only two modes to check.
              if (IsModeActiveInTerm(arModeCombi.Mode(0), i_term) && IsModeActiveInTerm(arModeCombi.Mode(1), i_term))
              {
                 subset_mc.push_back(i_term);
              }
           }
           return subset_mc;
         }

      private:
         //! Number of modes in operator
         In mNModes = I_0;
   };

}; /* namespace midas::tdvcc::constraint */

#endif /* CONSTRAINTOPDEF_H_INCLUDED */