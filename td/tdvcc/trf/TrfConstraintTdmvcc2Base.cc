/**
 *******************************************************************************
 * 
 * @file    TrfConstraintTdmvcc2Base.cc
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "td/tdvcc/trf/TrfConstraintTdmvcc2Base.h"
#include "td/tdvcc/trf/TrfConstraintTdmvcc2Base_Impl.h"
#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFCONSTRAINTTDMVCC2BASE(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfConstraintTdmvcc2Base<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFCONSTRAINTTDMVCC2BASE(Nb);
INSTANTIATE_TRFCONSTRAINTTDMVCC2BASE(std::complex<Nb>);

#undef INSTANTIATE_TRFCONSTRAINTTDMVCC2BASE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
