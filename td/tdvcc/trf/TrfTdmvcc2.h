/**
 *******************************************************************************
 * 
 * @file    TrfTdmvcc2.h
 * @date    06-10-2020
 * @author  Andreas Buchgraitz Jensen ()
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCC2_H_INCLUDED
#define TRFTDMVCC2_H_INCLUDED

#include "td/tdvcc/trf/TrfTdmvcc2_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/trf/TrfTdmvcc2_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*TRFTDMVCC2_H_INCLUDED*/