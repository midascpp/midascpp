/**
 *******************************************************************************
 * 
 * @file    TrfConstraintMatRepBase.cc
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "td/tdvcc/trf/TrfConstraintMatRepBase.h"
#include "td/tdvcc/trf/TrfConstraintMatRepBase_Impl.h"
#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFCONSTRAINTMATREPBASE(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfConstraintMatRepBase<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFCONSTRAINTMATREPBASE(Nb);
INSTANTIATE_TRFCONSTRAINTMATREPBASE(std::complex<Nb>);

#undef INSTANTIATE_TRFCONSTRAINTMATREPBASE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
