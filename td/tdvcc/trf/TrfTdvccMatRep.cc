/**
 *******************************************************************************
 * 
 * @file    TrfTdvccMatRep.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvccMatRep_Impl.h"
#include "td/tdvcc/trf/TrfTdvccBase.h"
#include "td/tdvcc/trf/TrfTdmvccBase.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_TRFTDVCCMATREPIMPL(PARAM_T, BASETRF_TMPL) \
   namespace midas::tdvcc \
   { \
      template class TrfTdvccMatRepImpl<PARAM_T, BASETRF_TMPL>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFTDVCCMATREPIMPL(Nb, TrfTdvccBase);
INSTANTIATE_TRFTDVCCMATREPIMPL(Nb, TrfTdmvccBase);
INSTANTIATE_TRFTDVCCMATREPIMPL(std::complex<Nb>, TrfTdvccBase);
INSTANTIATE_TRFTDVCCMATREPIMPL(std::complex<Nb>, TrfTdmvccBase);

#undef INSTANTIATE_TRFTDVCCMATREPIMPL
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
