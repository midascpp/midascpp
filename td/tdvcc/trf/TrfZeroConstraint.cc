/**
 *******************************************************************************
 * 
 * @file    TrfZeroConstraint.cc
 * @date    08-06-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"
#include "td/tdvcc/trf/TrfZeroConstraint_Impl.h"

// Define instatiation macro.
#define INSTANTIATE_TRFZEROCONSTRAINT(PARAM_T) \
   namespace midas::tdvcc \
   { \
      template class TrfZeroConstraint<PARAM_T>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_TRFZEROCONSTRAINT(Nb);
INSTANTIATE_TRFZEROCONSTRAINT(std::complex<Nb>);

#undef INSTANTIATE_TRFZEROCONSTRAINT

#endif /*DISABLE_PRECOMPILED_TEMPLATES*/
