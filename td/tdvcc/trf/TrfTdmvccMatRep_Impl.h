/**
 *******************************************************************************
 * 
 * @file    TrfTdmvccMatRep_Impl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTDMVCCMATREP_IMPL_H_INCLUDED
#define TRFTDMVCCMATREP_IMPL_H_INCLUDED

#include "td/tdvcc/trf/TrfTdmvccMatRep_Decl.h"
#include "util/matrep/MatRepTransformers.h"


namespace midas::tdvcc
{

/**
 *
 **/
template
   <  typename PARAM_T
   >
typename TrfTdmvccMatRep<PARAM_T>::mat_v_t
TrfTdmvccMatRep<PARAM_T>::DensityMatrices
   (  const cont_t& aClusterAmps
   ,  const cont_t& aLambdaCoefs
   )  const
{
   return midas::util::matrep::VccOneModeDensityMatrices(this->SparseClusterOper(), aClusterAmps, aLambdaCoefs);
}

/**
 * Calculate mean fields: {H_1, F_R, H'_1, F'_R}
 **/
template
   <  typename PARAM_T
   >
typename TrfTdmvccMatRep<PARAM_T>::meanfields_t
TrfTdmvccMatRep<PARAM_T>::MeanFieldMatrices
   (  step_t aTime
   ,  const cont_t& aClusterAmps
   ,  const cont_t& aLambdaCoefs
   )  const
{
   using namespace midas::util::matrep;

   auto nmodes = this->NModes();
   meanfields_t result(nmodes);

   // Set one-mode "mean fields"
   {
      auto h1_vec =  TdmvccHalfTransOneModeOpers
                        (  this->Oper()
                        ,  this->HalfTransModalIntegrals()[0]
                        ,  this->HalfTransModalIntegrals()[1]
                        );
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         result[imode][0] = std::move(h1_vec[imode].first);
         result[imode][2] = std::move(h1_vec[imode].second);
      }
   }

   // Set residual mean fields
   {
      auto f_res_vec    =  TdmvccHalfTransMeanFieldMatrices
                              (  this->Oper()
                              ,  this->TdModInts()
                              ,  this->HalfTransModalIntegrals()[0]
                              ,  this->HalfTransModalIntegrals()[1]
                              ,  this->SparseClusterOper()
                              ,  aClusterAmps
                              ,  aLambdaCoefs
                              ,  true              // Skip one-mode terms
                              );
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         result[imode][1] = std::move(f_res_vec[imode].first);
         result[imode][3] = std::move(f_res_vec[imode].second);
      }
   }

   return result;
}


} /* namespace midas::tdvcc */




#endif/*TRFTDMVCCMATREP_IMPL_H_INCLUDED*/
