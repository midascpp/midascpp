/**
 *******************************************************************************
 * 
 * @file    TdvccIfcFactory.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCIFCFACTORY_DECL_H_INCLUDED
#define TDVCCIFCFACTORY_DECL_H_INCLUDED

// Standard headers.
#include <memory>

// Midas headers.
#include "td/tdvcc/TdvccIfc.h"
#include "td/tdvcc/TdvccEnums.h"

// Forward declares.

namespace midas::tdvcc
{

   //!
   template<class... Args>
   std::unique_ptr<TdvccIfc> TdvccIfcFactory
      (  CorrType
      ,  TrfType
      ,  ConstraintType
      ,  const std::array<std::vector<Uin>,2>&
      ,  Args&&...
      );

} /* namespace midas::tdvcc */


#endif /* TDVCCIFCFACTORY_DECL_H_INCLUDED */
