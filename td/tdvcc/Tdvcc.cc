/**
 *******************************************************************************
 * 
 * @file    Tdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/Tdvcc.h"
#include "td/tdvcc/Tdvcc_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "vcc/TensorDataCont.h"
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/deriv/DerivTdvcc.h"
#include "td/tdvcc/deriv/DerivTdvci.h"
#include "td/tdvcc/deriv/DerivTdextvcc.h"
#include "td/tdvcc/deriv/DerivTdmvcc.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2.h"


// Define instatiation macro.
#define UNPACK(...) __VA_ARGS__
#define INSTANTIATE_TDVCC_IMPL(VEC_T,TRF_T,DERIV_T) \
   template class Tdvcc<VEC_T,TRF_T,DERIV_T>; \

#define INSTANTIATE_TDVCC(PARAM_T,IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvcc<PARAM_T,GeneralMidasVector>),TrfTdvccMatRep<PARAM_T>,UNPACK(DerivTdvcc<PARAM_T,GeneralMidasVector,TrfTdvccMatRep,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvcc<PARAM_T,GeneralDataCont>),TrfTdvcc2<PARAM_T>,UNPACK(DerivTdvcc<PARAM_T,GeneralDataCont,TrfTdvcc2,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvci<PARAM_T,GeneralMidasVector>),TrfTdvciMatRep<PARAM_T>,UNPACK(DerivTdvci<PARAM_T,GeneralMidasVector,TrfTdvciMatRep,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvci<PARAM_T,GeneralDataCont>),TrfTdvci2<PARAM_T>,UNPACK(DerivTdvci<PARAM_T,GeneralDataCont,TrfTdvci2,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvcc<PARAM_T,GeneralTensorDataCont>),TrfGeneralTimTdvcc<PARAM_T>,UNPACK(DerivTdvcc<PARAM_T,GeneralTensorDataCont,TrfGeneralTimTdvcc,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdvci<PARAM_T,GeneralTensorDataCont>),TrfGeneralTimTdvci<PARAM_T>,UNPACK(DerivTdvci<PARAM_T,GeneralTensorDataCont,TrfGeneralTimTdvci,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdextvcc<PARAM_T,GeneralMidasVector>),TrfTdextvccMatRep<PARAM_T>,UNPACK(DerivTdextvcc<PARAM_T,GeneralMidasVector,TrfTdextvccMatRep,IMAG_TIME>)); \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralMidasVector>),TrfTdmvccMatRep<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralMidasVector,TrfTdmvccMatRep,TrfVariationalConstraintMatRep,IMAG_TIME>)); /* required because Tdmvcc is a public Tdvcc */ \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralMidasVector>),TrfTdmvccMatRep<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralMidasVector,TrfTdmvccMatRep,TrfZeroConstraint,IMAG_TIME>)); /* required because Tdmvcc is a public Tdvcc */ \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralDataCont>),TrfTdmvcc2<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralDataCont,TrfTdmvcc2,TrfVariationalConstraintTdmvcc2,IMAG_TIME>)); /* required because Tdmvcc is a public Tdvcc */ \
      INSTANTIATE_TDVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralDataCont>),TrfTdmvcc2<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralDataCont,TrfTdmvcc2,TrfZeroConstraint,IMAG_TIME>)); /* required because Tdmvcc is a public Tdvcc */ \
   }; /* namespace midas::tdvcc */ \

// Instantiations.
INSTANTIATE_TDVCC(std::complex<Nb>,false);
INSTANTIATE_TDVCC(std::complex<Nb>,true);
INSTANTIATE_TDVCC(Nb,true);

#undef INSTANTIATE_TDVCC
#undef INSTANTIATE_TDVCC_IMPL

#undef UNPACK

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
