/**
 *******************************************************************************
 * 
 * @file    DerivTdvci.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTdvci.h"
#include "td/tdvcc/deriv/DerivTdvci_Impl.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTDVCI(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralMidasVector, TrfTdvciMatRep, false);
INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralMidasVector, TrfTdvciMatRep, true);
INSTANTIATE_DERIVTDVCI(Nb, GeneralMidasVector, TrfTdvciMatRep, true);

INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralDataCont, TrfTdvci2, false);
INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralDataCont, TrfTdvci2, true);
INSTANTIATE_DERIVTDVCI(Nb, GeneralDataCont, TrfTdvci2, true);

INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralTensorDataCont, TrfGeneralTimTdvci, false);
INSTANTIATE_DERIVTDVCI(std::complex<Nb>, GeneralTensorDataCont, TrfGeneralTimTdvci, true);
INSTANTIATE_DERIVTDVCI(Nb, GeneralTensorDataCont, TrfGeneralTimTdvci, true)

#undef INSTANTIATE_DERIVTDVCI


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
