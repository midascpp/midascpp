/**
 *******************************************************************************
 * 
 * @file    DerivTdmvcc.cc
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTdmvcc.h"
#include "td/tdvcc/deriv/DerivTdmvcc_Impl.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTDMVCC(PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralMidasVector, TrfTdmvccMatRep, TrfVariationalConstraintMatRep, false);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralMidasVector, TrfTdmvccMatRep, TrfVariationalConstraintMatRep, true);
INSTANTIATE_DERIVTDMVCC(Nb, GeneralMidasVector, TrfTdmvccMatRep, TrfVariationalConstraintMatRep, true);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralMidasVector, TrfTdmvccMatRep, TrfZeroConstraint, false);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralMidasVector, TrfTdmvccMatRep, TrfZeroConstraint, true);
INSTANTIATE_DERIVTDMVCC(Nb, GeneralMidasVector, TrfTdmvccMatRep, TrfZeroConstraint, true);

INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralDataCont, TrfTdmvcc2, TrfVariationalConstraintTdmvcc2, false);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralDataCont, TrfTdmvcc2, TrfVariationalConstraintTdmvcc2, true);
INSTANTIATE_DERIVTDMVCC(Nb, GeneralDataCont, TrfTdmvcc2, TrfVariationalConstraintTdmvcc2, true);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralDataCont, TrfTdmvcc2, TrfZeroConstraint, false);
INSTANTIATE_DERIVTDMVCC(std::complex<Nb>, GeneralDataCont, TrfTdmvcc2, TrfZeroConstraint, true);
INSTANTIATE_DERIVTDMVCC(Nb, GeneralDataCont, TrfTdmvcc2, TrfZeroConstraint, true);

#undef INSTANTIATE_DERIVTDMVCC


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
