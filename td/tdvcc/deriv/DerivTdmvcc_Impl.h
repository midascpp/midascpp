/**
 *******************************************************************************
 * 
 * @file    DerivTdmvcc_Impl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDMVCC_IMPL_H_INCLUDED
#define DERIVTDMVCC_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Compute the TDMVCC derivative without g terms. These will be added later.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME
      >
   std::pair<  typename DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::param_cont_t
            ,  typename DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::meanfields_t
            >
   DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::operator()
      (  step_t aTime
      ,  const param_cont_t& arParams
      ,  const trf_t& arTrf
      ,  const std::vector<mat_t>& aInvDensMats
      ,  const std::vector<mat_t>& aActiveSpaceProjectors
      ,  const bool aTimeIt
      ,  const bool aFullActiveBasis
      )  const
   {
      // 1) Compute error vector; then extract dot(phase), set ref. elem to zero, multiply by -i.
      auto s_dot = arTrf.ErrVec(aTime, arParams.ClusterAmps());
      PARAM_T phase_dot = ZeroFirstElemAndReturnIt(s_dot);
      phase_dot *= PhaseFactor();
      Scale(s_dot, AmplsFactor());

      // 2) Compute dot(l) = eta + lA.
      auto l_dot = arTrf.EtaVec(aTime, arParams.ClusterAmps());
      Axpy(l_dot, arTrf.LJac(aTime, arParams.ClusterAmps(), arParams.LambdaCoefs()), PARAM_T(+1));
      ZeroFirstElemAndReturnIt(l_dot);
      Scale(l_dot, CoefsFactor());

      // 3) Compute modal derivatives
      meanfields_t mean_fields(arParams.NModes());
      if (  !aFullActiveBasis
         )
      {
         mean_fields = std::move(arTrf.MeanFieldMatrices(aTime, arParams.ClusterAmps(), arParams.LambdaCoefs()));
      }
      else // We are doing TDMVCC utilising FullActiveBasis meanfields.
      {
         if constexpr (trf_t::trf_type == TrfType::VCC2H2)
         {
            mean_fields = std::move(arTrf.MeanFieldMatricesFullActiveBasis(aTime, arParams.ClusterAmps(), arParams.LambdaCoefs()));
         }
         else
         {
            MIDASERROR("Full active basis is only doable for TDMVCC2H2 transformer");
         }
      }

      std::vector<std::pair<mat_t, mat_t>> modals_dot(arParams.NModes());
      if (aFullActiveBasis)
      {
         // Contributions from secondary space is zero for full active basis method.
         const auto& td_modals = arParams.NModalsTdBas();
         for (In i_mode = I_0; i_mode < arParams.NModes(); ++i_mode)
         {
            modals_dot[i_mode].first  = mat_t(In(td_modals[i_mode]), PARAM_T(0));
            modals_dot[i_mode].second = mat_t(In(td_modals[i_mode]), PARAM_T(0));
         }
      }
      else
      {
         modals_dot = arTrf.SecondarySpaceModalDerivative(aTime, mean_fields, aInvDensMats, aActiveSpaceProjectors, this->ModMatKetFactor(), this->ModMatBraFactor());
      }

      return std::make_pair(param_cont_t(std::move(modals_dot), std::move(s_dot), std::move(l_dot), std::move(phase_dot)), mean_fields);
   }

   /************************************************************************//**
    * @brief
    *    Compute the constraint terms to the TDMVCC derivative
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME
      >
   typename DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::param_cont_t
   DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::GContributions
      (  step_t aTime
      ,  const std::vector<mat_t>& aGMats
      ,  const param_cont_t& aParams
      ,  gtrf_t& aGTrf
      )  const
   {
      if constexpr   (  !gtrf_t::zero_g
                     )
      {
         // 0) Check input g matrices and update transformer
         auto nmodes = aParams.NModes();
         if (  aGMats.size() != nmodes
            )
         {
            MIDASERROR("Wrong number of modes: " + std::to_string(nmodes) + " vs. " + std::to_string(aGMats.size()) + ".");
         }
         aGTrf.UpdateIntegrals(aGMats);

         // 1) Add contributions for s amplitudes and phase
         auto s_dot_g = aGTrf.ErrVec(aTime, aParams.ClusterAmps());
         PARAM_T phase_dot_g = ZeroFirstElemAndReturnIt(s_dot_g);
         phase_dot_g *= PARAM_T(-1.0)*PhaseFactor();
         Scale(s_dot_g, PARAM_T(-1.0)*AmplsFactor());

         // 2) Compute dot(l) = eta + lA.
         auto l_dot_g = aGTrf.EtaVec(aTime, aParams.ClusterAmps());
         Axpy(l_dot_g, aGTrf.LJac(aTime, aParams.ClusterAmps(), aParams.LambdaCoefs()), PARAM_T(+1));
         ZeroFirstElemAndReturnIt(l_dot_g);
         Scale(l_dot_g, PARAM_T(-1.0)*CoefsFactor());

         // 3) Add modal-derivative contributions
         modal_cont_t modal_dot_g(nmodes);
         for(Uin imode = 0; imode < nmodes; ++imode)
         {
            modal_dot_g[imode] = aGTrf.ActiveSpaceModalDerivative(aTime, aGMats[imode], aParams.ModalCont()[imode], this->ModMatKetFactor(), this->ModMatBraFactor());
         }

         // Clear intermediates for VCC2H2 (NKM: Mainly to make sure the wrong intermediates are not reused somewhere else!)
         if constexpr   (  gtrf_t::trf_type == TrfType::VCC2H2
                        )
         {
            aGTrf.ClearIntermeds();
         }

         return param_cont_t(std::move(modal_dot_g), std::move(s_dot_g), std::move(l_dot_g), std::move(phase_dot_g));
      }
      else
      {
         MIDASERROR("This function should not be called for g=0");
         return param_cont_t();
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::PhaseFactor
      (
      )  const
   {
      if (!ImagTime())
      {
         return PARAM_T(1);
      }
      else
      {
         return PARAM_T(0);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::AmplsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,-1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdmvcc<PARAM_T, CONT_TMPL, TRF_TMPL, GTRF_TMPL, IMAG_TIME>::CoefsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,+1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

} /* namespace midas::tdvcc */




#endif/*DERIVTDMVCC_IMPL_H_INCLUDED*/
