/**
 *******************************************************************************
 * 
 * @file    DerivTdvci_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDVCI_IMPL_H_INCLUDED
#define DERIVTDVCI_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   void DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::EnableImagTime
      (
      )
   {
      mImagTime = true;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   bool DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::ImagTime
      (
      )  const
   {
      return IMAG_TIME || mImagTime;
   }

   /************************************************************************//**
    * Time derivative of TDVCI parameters.
    * \f{align*}{
    *       \mathbf{\dot{c}} &= -i \mathbf{H} \mathbf{c}
    * \f}
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   typename DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::param_cont_t
   DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::operator()
      (  step_t aTime
      ,  const param_cont_t& arParams
      ,  const trf_t& arTrf
      )  const
   {
      param_cont_t c_dot(arTrf.Transform(aTime, arParams.Coefs()));

      Scale(c_dot, CoefsFactor());

      return c_dot;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::CoefsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,-1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

} /* namespace midas::tdvcc */




#endif/*DERIVTDVCI_IMPL_H_INCLUDED*/
