/**
 *******************************************************************************
 * 
 * @file    DerivTdvci_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDVCI_DECL_H_INCLUDED
#define DERIVTDVCI_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/params/ParamsTdvci.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME = false
      >
   class DerivTdvci
   {
      public:
         static_assert
            (  midas::type_traits::IsComplexV<PARAM_T> || IMAG_TIME
            ,  "PARAM_T must be complex when propagating in real time."
            );

         using step_t = midas::type_traits::RealTypeT<PARAM_T>;
         using param_cont_t = ParamsTdvci<PARAM_T,CONT_TMPL>;
         using trf_t = TRF_TMPL<PARAM_T>;
         static inline constexpr bool imag_time = IMAG_TIME;

         //! Enable imag. time at compile time.
         void EnableImagTime();

         //! Imag. time, either set by mImagTime or IMAG_TIME.
         bool ImagTime() const;

         param_cont_t operator()
            (  step_t aTime
            ,  const param_cont_t& arParams
            ,  const trf_t& arTrf
            )  const;

      private:
         //! For enabling imag. time at compile time.
         bool mImagTime = imag_time;

         //! -i for real time, -1 for imaginary time.
         PARAM_T CoefsFactor() const;
   };

} /* namespace midas::tdvcc */


#endif/*DERIVTDVCI_DECL_H_INCLUDED*/
