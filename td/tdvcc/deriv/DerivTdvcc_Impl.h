/**
 *******************************************************************************
 * 
 * @file    DerivTdvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDVCC_IMPL_H_INCLUDED
#define DERIVTDVCC_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   void DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::EnableImagTime
      (
      )
   {
      mImagTime = true;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   bool DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::ImagTime
      (
      )  const
   {
      return IMAG_TIME || mImagTime;
   }

   /************************************************************************//**
    * Time derivative of TDVCC parameters.
    * \f{align*}{
    *       \dot{\epsilon} &= e_{\text{ref}}
    *    \\ \dot{s}_\mu    &= -i e_\mu
    *    \\ \dot{l}_\nu    &=  i(\eta_\mu + l_\nu A_{\nu \mu})
    * \f}
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   typename DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::param_cont_t
   DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::operator()
      (  step_t aTime
      ,  const param_cont_t& arParams
      ,  const trf_t& arTrf
      )  const
   {
      // First get out the error vector; then extract dot(phase), set ref. elem
      // to zero, multiply by -i.
      auto s_dot = arTrf.ErrVec(aTime, arParams.ClusterAmps());
      PARAM_T phase_dot = ZeroFirstElemAndReturnIt(s_dot);
      phase_dot *= PhaseFactor();
      Scale(s_dot, AmplsFactor());

      // Then compute dot(l) = eta + lA.
      auto l_dot = arTrf.EtaVec(aTime, arParams.ClusterAmps());
      Axpy(l_dot, arTrf.LJac(aTime, arParams.ClusterAmps(), arParams.LambdaCoefs()), PARAM_T(+1));
      ZeroFirstElemAndReturnIt(l_dot);
      Scale(l_dot, CoefsFactor());

      return param_cont_t(std::move(s_dot), std::move(l_dot), std::move(phase_dot));
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::PhaseFactor
      (
      )  const
   {
      if (!ImagTime())
      {
         return PARAM_T(1);
      }
      else
      {
         return PARAM_T(0);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::AmplsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,-1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::CoefsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,+1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

} /* namespace midas::tdvcc */




#endif/*DERIVTDVCC_IMPL_H_INCLUDED*/
