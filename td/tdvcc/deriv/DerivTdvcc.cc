/**
 *******************************************************************************
 * 
 * @file    DerivTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTdvcc.h"
#include "td/tdvcc/deriv/DerivTdvcc_Impl.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTDVCC(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralMidasVector, TrfTdvccMatRep, false);
INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralMidasVector, TrfTdvccMatRep, true);
INSTANTIATE_DERIVTDVCC(Nb, GeneralMidasVector, TrfTdvccMatRep, true);

INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralDataCont, TrfTdvcc2, false);
INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralDataCont, TrfTdvcc2, true);
INSTANTIATE_DERIVTDVCC(Nb, GeneralDataCont, TrfTdvcc2, true);

INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralTensorDataCont, TrfGeneralTimTdvcc, false);
INSTANTIATE_DERIVTDVCC(std::complex<Nb>, GeneralTensorDataCont, TrfGeneralTimTdvcc, true);
INSTANTIATE_DERIVTDVCC(Nb, GeneralTensorDataCont, TrfGeneralTimTdvcc, true);

#undef INSTANTIATE_DERIVTDVCC


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
