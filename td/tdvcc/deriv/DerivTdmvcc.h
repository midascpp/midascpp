/**
 *******************************************************************************
 * 
 * @file    DerivTdmvcc.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDMVCC_H_INCLUDED
#define DERIVTDMVCC_H_INCLUDED

#include "td/tdvcc/deriv/DerivTdmvcc_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/deriv/DerivTdmvcc_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*DERIVTDMVCC_H_INCLUDED*/
