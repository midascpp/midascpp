/**
 *******************************************************************************
 * 
 * @file    DerivTdmvcc_Decl.h
 * @date    19-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDMVCC_DECL_H_INCLUDED
#define DERIVTDMVCC_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename> class GTRF_TMPL
      ,  bool IMAG_TIME = false
      >
   class DerivTdmvcc
   {
      public:
         static_assert
            (  midas::type_traits::IsComplexV<PARAM_T> || IMAG_TIME
            ,  "PARAM_T must be complex when propagating in real time."
            );

         using step_t = midas::type_traits::RealTypeT<PARAM_T>;
         using param_cont_t = ParamsTdmvcc<PARAM_T,CONT_TMPL>;
         using modal_cont_t = typename ParamsTdmvcc<PARAM_T,CONT_TMPL>::modal_cont_t;
         using trf_t = TRF_TMPL<PARAM_T>;
         using gtrf_t = GTRF_TMPL<PARAM_T>;
         using mat_t = GeneralMidasMatrix<PARAM_T>;
         using meanfields_t = typename trf_t::meanfields_t;
         static inline constexpr bool imag_time = IMAG_TIME;

         //! Enable imag. time at compile time.
         void EnableImagTime() {mImagTime = true;}

         //! Imag. time, either set by mImagTime or IMAG_TIME.
         bool ImagTime() const {return IMAG_TIME || mImagTime;}

         std::pair<param_cont_t, meanfields_t> operator()
            (  step_t aTime
            ,  const param_cont_t& arParams
            ,  const trf_t& arTrf
            ,  const std::vector<mat_t>& aInvDensMats
            ,  const std::vector<mat_t>& aActiveSpaceProjectors
            ,  const bool aTimeIt = false
            ,  const bool aFullActiveBasis = false
            )  const;

         param_cont_t GContributions
            (  step_t aTime
            ,  const std::vector<mat_t>& aGMatrices
            ,  const param_cont_t& aParams
            ,  gtrf_t& aGTrf
            )  const;

         //! +1 for real time, 0 for imaginary time (phase makes little sense in imag. time).
         PARAM_T PhaseFactor() const;

         //! -i for real time, -1 for imaginary time.
         PARAM_T AmplsFactor() const;

         //! +i for real time, -1 for imaginary time.
         PARAM_T CoefsFactor() const;

         //! A guess for now, that it's the same as Ampls. -MBH, Feb 2020.
         PARAM_T ModMatKetFactor() const {return this->AmplsFactor();}

         //! A guess for now, that it's the same as Coefs. -MBH, Feb 2020.
         PARAM_T ModMatBraFactor() const {return this->CoefsFactor();}

      private:
         //! For enabling imag. time at compile time.
         bool mImagTime = imag_time;

   };

} /* namespace midas::tdvcc */


#endif/*DERIVTDMVCC_DECL_H_INCLUDED*/
