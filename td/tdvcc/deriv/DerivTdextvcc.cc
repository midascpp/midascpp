/**
 *******************************************************************************
 * 
 * @file    DerivTdextvcc.cc
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTdextvcc.h"
#include "td/tdvcc/deriv/DerivTdextvcc_Impl.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTDEXTVCC(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTDEXTVCC(std::complex<Nb>, GeneralMidasVector, TrfTdextvccMatRep, false);
INSTANTIATE_DERIVTDEXTVCC(std::complex<Nb>, GeneralMidasVector, TrfTdextvccMatRep, true);
INSTANTIATE_DERIVTDEXTVCC(Nb, GeneralMidasVector, TrfTdextvccMatRep, true);

#undef INSTANTIATE_DERIVTDEXTVCC


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
