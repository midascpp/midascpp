/**
 *******************************************************************************
 * 
 * @file    DerivTdextvcc_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DERIVTDEXTVCC_IMPL_H_INCLUDED
#define DERIVTDEXTVCC_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   void DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::EnableImagTime
      (
      )
   {
      mImagTime = true;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   bool DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::ImagTime
      (
      )  const
   {
      return IMAG_TIME || mImagTime;
   }

   /************************************************************************//**
    * Time derivative of TDEVCC parameters.
    * \f{align*}{
    *       \dot{\epsilon} &= e_{\text{ref}}
    *    \\ \dot{s}_\mu    &= -i e_\mu
    *    \\ \dot{l}_\nu    &=  i(\eta_\mu + l_\nu A_{\nu \mu})
    * \f}
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   typename DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::param_cont_t
   DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::operator()
      (  step_t aTime
      ,  const param_cont_t& arParams
      ,  const trf_t& arTrf
      )  const
   {
      // Compute [exp(-S)]*[dH/dsigma] vector.
      // Extract ref. elem (the dot(phase)), and multiply with appropriate factors.
      auto clust_dot = arTrf.LeftExpmS
         (  arTrf.HamDerExtAmp(aTime, arParams.ClusterAmps(), arParams.ExtAmps())
         ,  arParams.ExtAmps()
         );
      PARAM_T phase_dot = ZeroFirstElemAndReturnIt(clust_dot);
      phase_dot *= PhaseFactor();
      Scale(clust_dot, ClustAmpsFactor());

      // Compute [dH/dt]*[exp(-S)] vector.
      // (dot(sigma_ref) _should_ automatically be zero.)
      // Multiply with appropriate factors.
      auto ext_dot = arTrf.RightExpmS
         (  arTrf.HamDerClustAmp(aTime, arParams.ClusterAmps(), arParams.ExtAmps())
         ,  arParams.ExtAmps()
         );
      Scale(ext_dot, ExtAmpsFactor());

      return param_cont_t(std::move(clust_dot), std::move(ext_dot), std::move(phase_dot));
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::PhaseFactor
      (
      )  const
   {
      if (!ImagTime())
      {
         return PARAM_T(1);
      }
      else
      {
         return PARAM_T(0);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::ClustAmpsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,-1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  bool IMAG_TIME
      >
   PARAM_T DerivTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>::ExtAmpsFactor
      (
      )  const
   {
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         if (!ImagTime())
         {
            return PARAM_T(0,+1);
         }
         else
         {
            return PARAM_T(-1,0);
         }
      }
      else
      {
         // It must be imag. time then.
         return PARAM_T(-1);
      }
   }

} /* namespace midas::tdvcc */




#endif/*DERIVTDEXTVCC_IMPL_H_INCLUDED*/
