/**
 *******************************************************************************
 * 
 * @file    TdvccIfcFactory.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCIFCFACTORY_IMPL_H_INCLUDED
#define TDVCCIFCFACTORY_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "td/tdvcc/Tdvcc.h"
#include "td/tdvcc/Tdmvcc.h"
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"
#include "td/tdvcc/deriv/DerivTdvcc.h"
#include "td/tdvcc/deriv/DerivTdmvcc.h"
#include "td/tdvcc/deriv/DerivTdvci.h"
#include "td/tdvcc/deriv/DerivTdextvcc.h"

// Forward declares.
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template<class... Args>
   std::unique_ptr<TdvccIfc> TdvccIfcFactory
      (  CorrType aCorr
      ,  TrfType aTrf
      ,  ConstraintType aConstraint
      ,  const std::array<std::vector<Uin>,2>& arVecNModals
      ,  Args&&... aArgs
      )
   {
      using param_t = TdvccIfc::ifc_param_t;

      // Small util. lambdas for error info.
      auto err_info_trf = [](CorrType c, TrfType t) -> std::string
         {
            std::stringstream ss;
            ss << "TrfType '" << StringFromEnum(t) << "' "
               << "unknown or not applicable for chosen CorrType '" << StringFromEnum(c) << "'."
               ;
            return ss.str();
         };

      std::unique_ptr<TdvccIfc> p(nullptr);
      try
      {
         if (aCorr == CorrType::VCC)
         {
            if (aTrf == TrfType::MATREP)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvcc<param_t, GeneralMidasVector>
                        ,  TrfTdvccMatRep<param_t>
                        ,  DerivTdvcc<param_t, GeneralMidasVector, TrfTdvccMatRep>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else if (aTrf == TrfType::VCC2H2)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvcc<param_t, GeneralDataCont>
                        ,  TrfTdvcc2<param_t>
                        ,  DerivTdvcc<param_t, GeneralDataCont, TrfTdvcc2>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else if (aTrf == TrfType::GENERAL)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvcc<param_t, GeneralTensorDataCont>
                        ,  TrfGeneralTimTdvcc<param_t>
                        ,  DerivTdvcc<param_t, GeneralTensorDataCont, TrfGeneralTimTdvcc>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else
            {
               MIDASERROR(err_info_trf(aCorr, aTrf));
            }
         }
         else if (aCorr == CorrType::VCI)
         {
            if (aTrf == TrfType::MATREP)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvci<param_t, GeneralMidasVector>
                        ,  TrfTdvciMatRep<param_t>
                        ,  DerivTdvci<param_t, GeneralMidasVector, TrfTdvciMatRep>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else if (aTrf == TrfType::VCC2H2)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvci<param_t, GeneralDataCont>
                        ,  TrfTdvci2<param_t>
                        ,  DerivTdvci<param_t, GeneralDataCont, TrfTdvci2>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else if (aTrf == TrfType::GENERAL)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdvci<param_t, GeneralTensorDataCont>
                        ,  TrfGeneralTimTdvci<param_t>
                        ,  DerivTdvci<param_t, GeneralTensorDataCont, TrfGeneralTimTdvci>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            
            else
            {
               MIDASERROR(err_info_trf(aCorr, aTrf));
            }
         }
         else if (aCorr == CorrType::EXTVCC)
         {
            if (aTrf == TrfType::MATREP)
            {
               p = std::make_unique
                  <  Tdvcc
                        <  ParamsTdextvcc<param_t, GeneralMidasVector>
                        ,  TrfTdextvccMatRep<param_t>
                        ,  DerivTdextvcc<param_t, GeneralMidasVector, TrfTdextvccMatRep>
                        >
                  >
                  (  arVecNModals.at(0)
                  ,  std::forward<Args>(aArgs)...
                  );
            }
            else
            {
               MIDASERROR(err_info_trf(aCorr, aTrf));
            }
         }
         else if (aCorr == CorrType::TDMVCC)
         {
            if (aTrf == TrfType::MATREP)
            {
               if (aConstraint == ConstraintType::VARIATIONAL)
               {
                  p = std::make_unique
                     <  Tdmvcc
                           <  ParamsTdmvcc<param_t, GeneralMidasVector>
                           ,  TrfTdmvccMatRep<param_t>
                           ,  TrfVariationalConstraintMatRep<param_t>
                           ,  DerivTdmvcc<param_t, GeneralMidasVector, TrfTdmvccMatRep, TrfVariationalConstraintMatRep>
                           >
                     >
                     (  arVecNModals.at(0)
                     ,  arVecNModals.at(1)
                     ,  std::forward<Args>(aArgs)...
                     );
               }
               else if (aConstraint == ConstraintType::ZERO)
               {
                  p = std::make_unique
                     <  Tdmvcc
                           <  ParamsTdmvcc<param_t, GeneralMidasVector>
                           ,  TrfTdmvccMatRep<param_t>
                           ,  TrfZeroConstraint<param_t>
                           ,  DerivTdmvcc<param_t, GeneralMidasVector, TrfTdmvccMatRep, TrfZeroConstraint>
                           >
                     >
                     (  arVecNModals.at(0)
                     ,  arVecNModals.at(1)
                     ,  std::forward<Args>(aArgs)...
                     );
               }
               else
               {
                  MIDASERROR("Unknown constraint type: '" + StringFromEnum(aConstraint) + "'.");
               }
            }
            else if (aTrf == TrfType::VCC2H2)
            {
               if (aConstraint == ConstraintType::VARIATIONAL)
               {
                  p = std::make_unique
                     <  Tdmvcc
                           <  ParamsTdmvcc<param_t, GeneralDataCont>
                           ,  TrfTdmvcc2<param_t> 
                           ,  TrfVariationalConstraintTdmvcc2<param_t>
                           ,  DerivTdmvcc<param_t, GeneralDataCont, TrfTdmvcc2, TrfVariationalConstraintTdmvcc2>
                           >
                     >
                     (  arVecNModals.at(0)
                     ,  arVecNModals.at(1)
                     ,  std::forward<Args>(aArgs)...
                     );
               }
               else if (aConstraint == ConstraintType::ZERO)
               {
                  p = std::make_unique
                     <  Tdmvcc
                           <  ParamsTdmvcc<param_t, GeneralDataCont>
                           ,  TrfTdmvcc2<param_t>
                           ,  TrfZeroConstraint<param_t>
                           ,  DerivTdmvcc<param_t, GeneralDataCont, TrfTdmvcc2, TrfZeroConstraint>
                           >
                     >
                     (  arVecNModals.at(0)
                     ,  arVecNModals.at(1)
                     ,  std::forward<Args>(aArgs)...
                     );
               }
            }
            else
            {
               MIDASERROR(err_info_trf(aCorr, aTrf));
            }
         }
         else
         {
            MIDASERROR("Unknown CorrType: '"+StringFromEnum(aCorr)+"'.");
         }
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR(std::string("Caught out_of_range: ")+oor.what());
      }

      // Initialize transformers
      p->InitializeTransformers();
      const auto& nmodals = (aCorr == CorrType::TDMVCC) ? arVecNModals.at(1) : arVecNModals.at(0);
      p->InitializeSizeParams(nmodals);

      // Return
      return p;
   }

} /* namespace midas::tdvcc */


#endif /* TDVCCIFCFACTORY_IMPL_H_INCLUDED */
