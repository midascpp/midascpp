/**
 *******************************************************************************
 * 
 * @file    Tdvcc_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_DECL_H_INCLUDED
#define TDVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"
#include "td/oper/LinCombOper.h"
#include "td/tdvcc/TdvccIfc.h"
#include "td/OneModeDensities.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;

namespace midas::tdvcc
{
   /************************************************************************//**
    * Template parameters:
    *    -  VEC_T: class holding data; must have param_t
    *    -  TRF_T: transformer type
    *    -  DERIV_T: given a vec_t (+extra stuff), must compute and return
    *    a vec_t which is the corresponding derivative
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   class Tdvcc
      :  public TdvccIfc
   {
      public:
         using vec_t = VEC_T;
         using param_t = typename vec_t::param_t;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using oper_t = td::LinCombOper<step_t, param_t, OpDef>;

         /******************************************************************//**
          * @name Constructors and friends
          *********************************************************************/
         //!@{
         Tdvcc
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arHamiltonian
            ,  ModalIntegrals<param_t> aModInts
            ,  const ModeCombiOpRange& arMcr
            ,  param_t aOperCoef = 1
            );

         Tdvcc
            (  const std::vector<Uin>& arNModals
            ,  oper_t aHamiltonian
            ,  std::vector<ModalIntegrals<param_t>> aVecModInts
            ,  const ModeCombiOpRange& arMcr
            );
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //!
         Uin SizeParams() const override {return mSizeParams;}
         virtual Uin SizeTotal() const override;
         bool HasPhase() const override;
         bool ImagTime() const override;
         Uin NumHamiltonianContribs() const override {return mHamiltonian.size();}
         const std::vector<Uin>& NModals() const override {return mrNModals;}
         Uin MaxExci() const override;
         CorrType GetCorrType() const override;
         TrfType GetTrfType() const override;
         Uin NElectronicStates() const override {return mNElectronicStates;}
         virtual void InitOneModeDensities
            (  const std::vector<std::vector<GeneralMidasVector<Nb>>>& aVscfModals
            ) override;
         //!@}

         /******************************************************************//**
          * @name Set propagation details
          *********************************************************************/
         //!@{
         //! Enable imaginary time at compile time, even if IMAG_TIME is false.
         void EnableImagTime() override;

         //! Initialize H transformers (must be done after c-tor due to inheritance)
         void InitializeTransformers() override;

         //! Initialize size of paramters
         virtual void InitializeSizeParams
            (  const std::vector<Uin>& arNModals
            )  override;

         //! To be used with restart option
         virtual std::map<ParamID,libmda::util::any_type> ReadParamsFromFile(const std::string& aRestartFilePath) override;
         virtual std::map<ParamID,libmda::util::any_type> PrepInitState(InitType, CorrType, Uin, const std::vector<Uin>&, const std::vector<Uin>&, const std::string& = "", In = -1) override;
         //!@}

         /******************************************************************//**
          * @name ODE interface
          *********************************************************************/
         //!@{
         //! 
         vec_t ShapedZeroVector() const;

         //! Calculate time-derivative.
         vec_t Derivative(step_t, const vec_t&);

         //! Process new vector, deciding whether to accept step.
         bool ProcessNewVector
            (  vec_t& arY
            ,  const vec_t& aYOld
            ,  step_t& arT
            ,  step_t aTOld
            ,  vec_t& arDyDt
            ,  const vec_t& aDyDtOld
            ,  step_t& arStep
            ,  size_t& arNDerivFail
            );

         //! What to do at accepted step (saving props., etc.). Returns "always continue" a.t.m.
         bool AcceptedStep(step_t, const vec_t&);

         //! Like AcceptedStep but at an interpolated point.
         void InterpolatedPoint(step_t, const vec_t&);

         //!@}

      protected:
         using TdvccIfc::ifc_step_t;
         using TdvccIfc::ifc_absval_t;
         using TdvccIfc::ifc_param_t;
         using TdvccIfc::ifc_prop_t;

         const std::vector<Uin>& mrNModals;
         const oper_t mHamiltonian;
         const std::vector<ModalIntegrals<param_t>> mVecModInts;
         const ModeCombiOpRange& mrMcr;
         Uin mSizeParams;
         const Uin mNElectronicStates;
         std::vector<TRF_T> mVecTrf;
         std::unique_ptr<vec_t> mpInitState = nullptr;
         std::unique_ptr<vec_t> mpFinalState = nullptr;
         std::unique_ptr<vec_t> mpVccGroundState = nullptr;
         std::unique_ptr<vec_t> mpInitStateForAutoCorr = nullptr;
         InitType mInitStateForAutoCorrType = InitType::VSCFREF;
         bool mImagTime = DERIV_T::imag_time;
         std::vector<std::tuple<const OpDef*, ModalIntegrals<param_t>>> mVecExpValOpers;
         std::vector<TRF_T> mVecExpValTrfs;
         midas::td::OneModeDensities<param_t> mOneModeDensities;

         const vec_t& InitState() const;
         const vec_t& InitStateForAutoCorr() const;
         const InitType InitStateForAutoCorrType() const;
         const vec_t& FinalState() const;
         const vec_t& VccGroundState() const;
         const TRF_T& GetHamTrf(Uin i) const {return mVecTrf.at(i);}

         static Uin CalcSizeParams(const std::vector<Uin>&, const ModeCombiOpRange&);
         static oper_t TimeIndepOper(const OpDef&, param_t);
         virtual void PassSettingsToTrfs() override;

         void SanityCheckState(const std::map<ParamID, libmda::util::any_type>&) const override;
         void SetInitStateImpl(std::map<ParamID, libmda::util::any_type>&&) override;
         void SetInitStateForAutoCorrImpl(std::map<ParamID, libmda::util::any_type>&&) override;
         void SetVccGroundStateImpl(std::map<ParamID,libmda::util::any_type>&&) override;
         void ExptValReserveImpl(const Uin) override;
         void EnableExptValImpl(const OpDef*, ModalIntegrals<Nb>&&) override;

         //@{
         void SaveProperties(step_t, const vec_t&);
         void SaveStats(step_t, const vec_t&);
         void SaveExpValsImpl(step_t, const vec_t&);
         void SaveAutoCorrelations(step_t, const vec_t&);

         void SaveEnergyAndContribsImpl(step_t, const vec_t&);
         void SavePhaseImpl(const vec_t&);
         void SaveStatePopulationsImpl(const vec_t&);
         void SaveFvciNorm2Impl(const vec_t&);
         void SaveParamNorm2Impl(const vec_t&);
         void SaveParamNorm2InitImpl(const vec_t&);
         void SaveParamNorm2InitVccGs(const vec_t&);
         virtual void SaveAutoCorrImpl(const vec_t&);
         virtual void SaveAutoCorrExtImpl(const vec_t&);
         void SaveFvciVecImpl(const vec_t&);
         virtual void SaveTdModalPlotsImpl(const vec_t&) const;
         virtual void CalculateOneModeDensities(const vec_t&);
         virtual void PrintOutPotential() const override;
         //@}

         void EvolveImpl() override;
         const std::set<ParamID>& GetParamIDs() const override {return vec_t::GetParamIDs();}

      private:
         /******************************************************************//**
          * @name Implementation of ODE interface
          *********************************************************************/
         //!@{
         //! Calculate time-derivative.
         virtual vec_t DerivativeImpl(step_t, const vec_t&, const DERIV_T&) const;

         //! Shaped zeros
         virtual vec_t ShapedZeroVectorImpl() const;

         //! Process new vector
         virtual bool ProcessNewVectorImpl
            (  vec_t& arY
            ,  const vec_t& aYOld
            ,  step_t& arT
            ,  step_t aTOld
            ,  vec_t& arDyDt
            ,  const vec_t& aDyDtOld
            ,  step_t& arStep
            ,  size_t& arNDerivFail
            );
         //!@}

         //! Construct a transformer
         virtual TRF_T ConstructTrf
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            )  const;

         //! Prepare derivative evaluation
         virtual void PrepareDerivativeImpl
            (  const vec_t& aParams
            )
         {
         }
   };

} /* namespace midas::tdvcc */

#endif/*TDVCC_DECL_H_INCLUDED*/
