/**
 *******************************************************************************
 * 
 * @file    Tdvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_IMPL_H_INCLUDED
#define TDVCC_IMPL_H_INCLUDED

// Midas headers.
#include "input/Trim.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "ode/OdeIntegrator.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/anal/AutoCorr.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "util/IsDetected.h"
#include "td/tdvcc/anal/StatePopulations.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "util/CallStatisticsHandler.h"

#include "potentials/MidasPotential.h"
extern std::string gAnalysisDir;
extern GlobalOperatorDefinitions gOperatorDefs;

// Forward declarations
namespace midas::tdvcc
{
   std::map<ParamID,libmda::util::any_type> LoadVccGs(InitType, CorrType, Uin, const std::string&, In = -1);
}

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Tdvcc<VEC_T,TRF_T,DERIV_T>::Tdvcc
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arHamiltonian
      ,  ModalIntegrals<param_t> aModInts
      ,  const ModeCombiOpRange& arMcr
      ,  param_t aHamiltonianCoef
      )
      :  mrNModals(arNModals)
      ,  mHamiltonian(TimeIndepOper(arHamiltonian, aHamiltonianCoef))
      ,  mVecModInts({std::move(aModInts)})
      ,  mrMcr(arMcr)
      ,  mNElectronicStates
            (  gOperatorDefs.ElectronicDofNr() != -I_1
            ?  arHamiltonian.StateTransferIJMax(arHamiltonian.GetLocalModeNr(gOperatorDefs.ElectronicDofNr())) + 1
            :  1
            )
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Tdvcc<VEC_T,TRF_T,DERIV_T>::Tdvcc
      (  const std::vector<Uin>& arNModals
      ,  oper_t aHamiltonian
      ,  std::vector<ModalIntegrals<param_t>> aVecModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  mrNModals(arNModals)
      ,  mHamiltonian(std::move(aHamiltonian))
      ,  mVecModInts(std::move(aVecModInts))
      ,  mrMcr(arMcr)
      ,  mNElectronicStates
            (  gOperatorDefs.ElectronicDofNr() != -I_1
            ?  aHamiltonian.front().Oper().StateTransferIJMax(aHamiltonian.front().Oper().GetLocalModeNr(gOperatorDefs.ElectronicDofNr())) + 1 // Just take it from the first operator
            :  1
            )
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::InitializeTransformers
      (
      )
   {
      if (  !this->mVecTrf.empty()
         )
      {
         MIDASERROR("InitializeTransformers called for non-empty mVecTrfs");
      }
      if (this->mHamiltonian.size() != this->mVecModInts.size())
      {
         std::stringstream ss;
         ss << "this->mHamiltonian.size() (which is " << this->mHamiltonian.size()
            << ") != mVecModInts.size() (which is " << mVecModInts.size()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      this->mVecTrf.reserve(this->mHamiltonian.size());

      auto it_modints = this->mVecModInts.begin(), end_modints = this->mVecModInts.end();
      auto it_op = this->mHamiltonian.begin(), end = this->mHamiltonian.end();
      for(; it_op != end && it_modints != end_modints;  ++it_op, ++it_modints)
      {
         auto trf = this->ConstructTrf(it_op->Oper(), *it_modints);
         PassSettingsToTrf(trf, *this);
         this->mVecTrf.emplace_back(std::move(trf));
      }
      if (it_op != end || it_modints != end_modints)
      {
         MIDASERROR("Something wrong, not both iterators are at end.");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::InitializeSizeParams
      (  const std::vector<Uin>& arNModals
      )
   {
      this->mSizeParams = this->CalcSizeParams(arNModals, mrMcr);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   TRF_T Tdvcc<VEC_T,TRF_T,DERIV_T>::ConstructTrf
      (  const OpDef& arOpDef
      ,  const ModalIntegrals<param_t>& arModInts
      )  const
   {
      if constexpr   (  VEC_T::corr_type == CorrType::TDMVCC
                     )
      {
         MIDASERROR("class Tdvcc should not call ConstructTrf for TDMVCC");
         return TRF_T(this->NModals(), this->NModals(), arOpDef, arModInts, this->mrMcr);
      }
      else
      {
         return TRF_T(this->NModals(), arOpDef, arModInts, this->mrMcr);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   namespace detail
   {
      template<typename U> using has_sizetotal_onearg_t = decltype(U::SizeTotal(std::declval<Uin>()));
   } /* namespace detail */
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin Tdvcc<VEC_T,TRF_T,DERIV_T>::SizeTotal
      (
      )  const
   {
      if constexpr   (  midas::util::IsDetectedV<detail::has_sizetotal_onearg_t, VEC_T>
                     )
      {
         return VEC_T::SizeTotal(SizeParams());
      }
      else
      {
         MIDASERROR("SizeTotal called on Tdvcc with VEC_T not supporting VEC_T::SizeTotal(Uin).");
         return 0;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool Tdvcc<VEC_T,TRF_T,DERIV_T>::HasPhase
      (
      )  const
   {
      return VEC_T::num_phases > 0;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool Tdvcc<VEC_T,TRF_T,DERIV_T>::ImagTime
      (
      )  const
   {
      return DERIV_T::imag_time || mImagTime;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin Tdvcc<VEC_T,TRF_T,DERIV_T>::MaxExci
      (
      )  const
   {
      return mrMcr.GetMaxExciLevel();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   CorrType Tdvcc<VEC_T,TRF_T,DERIV_T>::GetCorrType
      (
      )  const
   {
      return VEC_T::corr_type;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   TrfType Tdvcc<VEC_T,TRF_T,DERIV_T>::GetTrfType
      (
      )  const
   {
      return TRF_T::trf_type;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::InitOneModeDensities
      (  const std::vector<std::vector<GeneralMidasVector<Nb>>>& aVscfModals
      )
   {
      mOneModeDensities = midas::td::OneModeDensities<param_t>
         (  this->Name()
         ,  this->GetGridPoints()
         ,  aVscfModals
         );
      
      // Set the space and time smoothenings if they have been specified
      if (this->SpaceSmoothening())
      {
         mOneModeDensities.SetSpaceSmoothening(this->SpaceSmoothening());
      }
      if (this->TimeSmoothening())
      {
         mOneModeDensities.SetTimeSmoothening(this->TimeSmoothening());
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::CalculateOneModeDensities
      (  const vec_t&   aParams
      )
   {
      if constexpr   (  VEC_T::corr_type == CorrType::VCC
                        && TRF_T::trf_type == TrfType::MATREP
                     )
      {
         MidasWarning("Implementation of DensityMatrices() for VCC has not been tested");
         this->mOneModeDensities.CalcTdvccOneModeDensities(this->GetHamTrf(0).DensityMatrices(aParams.ClusterAmps(), aParams.LambdaCoefs()));
         this->mOneModeDensities.CalcSsOneModeDensities();
         this->mOneModeDensities.CalcStsOneModeDensities();
         if (this->GetWriteOneModeDensities())
         {
            this->mOneModeDensities.WriteOneModeDensitiesForStep(this->GetNumberOfInterpolatedTimePoints());
         }
         if (this->GetNumberOfInterpolatedTimePoints() == this->GetUpdateOneModeDensitiesStep())
         {
            this->mOneModeDensities.WriteOneModeDensitiesToAdga();
         }
      }
      else
      {
         MIDASERROR("DensityMatrices() has not been implementet for correlation methods VCI and EXTVCC and not for Transformers VCC2H2 and GENERAL");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::EnableImagTime
      (
      )
   {
      mImagTime = true;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   Tdvcc<VEC_T,TRF_T,DERIV_T>::ShapedZeroVector
      (
      )  const
   {
      if constexpr   (  std::is_same_v<VEC_T, ParamsTdvcc<param_t, GeneralTensorDataCont>>
                     || std::is_same_v<VEC_T, ParamsTdvci<param_t, GeneralTensorDataCont>>
                     )
      {
         return vec_t(mrMcr, mrNModals);
      }
      else
      {
         return this->ShapedZeroVectorImpl();
         //return vec_t(SizeParams());
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   Tdvcc<VEC_T,TRF_T,DERIV_T>::ShapedZeroVectorImpl
      (
      )  const
   {
      if constexpr   (  vec_t::corr_type == CorrType::TDMVCC
                     )
      {
         MIDASERROR("Tdvcc::ShapedZeroVectorImpl called for TDMVCC!");
         return vec_t();
      }
      if constexpr   (  std::is_same_v<VEC_T, ParamsTdvcc<param_t, GeneralTensorDataCont>>
                     || std::is_same_v<VEC_T, ParamsTdvci<param_t, GeneralTensorDataCont>>
                     )
      {
         return vec_t(mrMcr, mrNModals);
      }
      else
      {
         return vec_t(SizeParams());
      }
   }
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   Tdvcc<VEC_T,TRF_T,DERIV_T>::Derivative
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      LOGCALL("derivative");
      DERIV_T deriv;
      if (ImagTime())
      {
         deriv.EnableImagTime();
      }
      clock_t clock_tot = clock();
      this->PrepareDerivativeImpl(arVec);
      vec_t result;
      if (this->PseudoTDH())
      {
         auto vec_zero_amps = arVec;
         vec_zero_amps.ZeroAmplitudes();
         result = std::move(this->DerivativeImpl(aTime, vec_zero_amps, deriv));
         result.ZeroAmplitudes();
      }
      else
      {
         result = std::move(this->DerivativeImpl(aTime, arVec, deriv));
      }
      if (this->TimeIt())
      {
         Mout
            << "Tdvcc<...>::Derivative(...); "
            << "total CPU time: " << Nb(clock()-clock_tot)/CLOCKS_PER_SEC << " s"
            << std::endl;
      }

      // Clear intermediates for VCC2H2 (NKM: Mainly to make sure the wrong intermediates are not reused somewhere else!)
      for(auto& trf: mVecTrf)
      {
         if constexpr   (  TRF_T::trf_type == TrfType::VCC2H2
                        )
         {
            trf.ClearIntermeds();
         }
      }

      return result;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   Tdvcc<VEC_T,TRF_T,DERIV_T>::DerivativeImpl
      (  step_t aTime
      ,  const vec_t& arVec
      ,  const DERIV_T& arDeriv
      )  const
   {
      if constexpr   (  VEC_T::corr_type == CorrType::TDMVCC
                     )
      {
         MIDASERROR("Should not be called for TDMVCC!");
         return this->ShapedZeroVector();
      }
      else
      {
         vec_t result = this->ShapedZeroVector();
         for(Uin i = 0; i < mHamiltonian.size(); ++i)
         {
            Axpy(result, arDeriv(aTime, arVec, GetHamTrf(i)), mHamiltonian.at(i).Coef(aTime));
         }
         return result;
      }
   }

   /************************************************************************//**
    * Always returns true (meaning accept the step), but if propagating in
    * imaginary time _and_ the haulting threshold is larger than zero, then
    * calculate and store the norm of the last derivative, for checking it
    * agains the threshold in AcceptedStep().
    *
    * @param[in] arY
    * @param[in] aYOld
    * @param[in] arT
    * @param[in] aTOld
    * @param[in] arDyDt
    *    Calculate the norm of this if imag. time and positive haulting
    *    threshold.
    * @param[in] aDyDtOld
    * @param[in] arStep
    * @param[in] arNDerivFail
    * @return
    *    Whether to accepted the step. Always true for now.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool Tdvcc<VEC_T,TRF_T,DERIV_T>::ProcessNewVector
      (  vec_t& arY
      ,  const vec_t& aYOld
      ,  step_t& arT
      ,  step_t aTOld
      ,  vec_t& arDyDt
      ,  const vec_t& aDyDtOld
      ,  step_t& arStep
      ,  size_t& arNDerivFail
      )
   {
      if (this->CheckLastDerivNorm())
      {
         this->LastDerivNorm() = Norm(arDyDt);
      }
      return this->ProcessNewVectorImpl(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
   }

   /************************************************************************//**
    * Always returns true (meaning accept the step), but if propagating in
    * imaginary time _and_ the haulting threshold is larger than zero, then
    * calculate and store the norm of the last derivative, for checking it
    * agains the threshold in AcceptedStep().
    *
    * @param[in] arY
    * @param[in] aYOld
    * @param[in] arT
    * @param[in] aTOld
    * @param[in] arDyDt
    *    Calculate the norm of this if imag. time and positive haulting
    *    threshold.
    * @param[in] aDyDtOld
    * @param[in] arStep
    * @param[in] arNDerivFail
    * @return
    *    Whether to accepted the step. Always true for now.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool Tdvcc<VEC_T,TRF_T,DERIV_T>::ProcessNewVectorImpl
      (  vec_t& arY
      ,  const vec_t& aYOld
      ,  step_t& arT
      ,  step_t aTOld
      ,  vec_t& arDyDt
      ,  const vec_t& aDyDtOld
      ,  step_t& arStep
      ,  size_t& arNDerivFail
      )
   {
      return true;
   }

   /************************************************************************//**
    * @note
    *    For now it always return false (meaning continue) if propagating in
    *    real time.
    *    For imag. time it can check whether to hault based on last derivative
    *    norm and a haulting threshold.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool Tdvcc<VEC_T,TRF_T,DERIV_T>::AcceptedStep
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      LOGCALL("accepted step");
      // Store the time.
      this->SaveAcceptedTime(aTime);

      SaveProperties(aTime, arVec);
      SaveStats(aTime, arVec);
      SaveExpValsImpl(aTime, arVec);

      this->PrintEvolveStatus();

      if (this->CheckLastDerivNorm())
      {
         return this->LastDerivNorm() < this->ImagTimeHaultThr();
      }
      else
      {
         return false;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::InterpolatedPoint
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      LOGCALL("interpolated point");
      this->SaveInterpolatedTime(aTime);
      SaveAutoCorrelations(aTime, arVec);

      if (this->SaveFvciVecs())
      {
         SaveFvciVecImpl(arVec);
      }

      if (this->GetUpdateOneModeDensitiesStep())
      {
         this->CalculateOneModeDensities(arVec);
      }

      if (this->PrintParamsToFile())
      {
         // Ensure proper directories exists
         MidasAssert(this->GetNumberOfInterpolatedTimePoints() - I_1 >= I_0, "Somethings rotten!");
         std::string dir = gAnalysisDir + "/" + this->Name() + "_params" + "/";
         if (!midas::filesystem::IsDir(dir) && midas::filesystem::Mkdir(dir) != I_0)
         {
            MIDASERROR("Directory " + dir + " could not be created");
         }
         dir += std::to_string(this->GetNumberOfInterpolatedTimePoints() - I_1) + "/";
         if (!midas::filesystem::IsDir(dir) && midas::filesystem::Mkdir(dir) != I_0)
         {
            MIDASERROR("Directory " + dir + " could not be created");
         }
         arVec.PrintParamsToFile(dir);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& Tdvcc<VEC_T,TRF_T,DERIV_T>::InitState
      (
      )  const
   {
      if (mpInitState)
      {
         return *mpInitState;
      }
      else
      {
         MIDASERROR("Called InitState() but mpInitState is nullptr!");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& Tdvcc<VEC_T,TRF_T,DERIV_T>::InitStateForAutoCorr
      (
      )  const
   {
      if (UseInitStateForAutoCorr())
      {
         if (mpInitStateForAutoCorr)
         {
            return *mpInitStateForAutoCorr;
         }
         else
         {
            MIDASERROR("Special inital state for autocorrelation function was requested but mpInitStateForAutoCorr is nullptr!");
            static vec_t dummy;
            return dummy;
         }
      }
      else if (mpInitState)
      {
         return *mpInitState;
      }
      else
      {
         MIDASERROR("Called InitStateForAutoCorr() but mpInitState is nullptr!");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const InitType Tdvcc<VEC_T,TRF_T,DERIV_T>::InitStateForAutoCorrType
      (
      )  const
   {
      if (UseInitStateForAutoCorr())
      {
         return mInitStateForAutoCorrType;
      }
      else 
      {
         return InitStateType();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& Tdvcc<VEC_T,TRF_T,DERIV_T>::FinalState
      (
      )  const
   {
      if (mpFinalState)
      {
         return *mpFinalState;
      }
      else
      {
         MIDASERROR("Called FinalState() but mpFinalState is nullptr!");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename Tdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& Tdvcc<VEC_T,TRF_T,DERIV_T>::VccGroundState
      (
      )  const
   {
      if (mpVccGroundState)
      {
         return *mpVccGroundState;
      }
      else
      {
         MIDASERROR("Called VccGroundState() but mpVccGroundState is nullptr!");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin Tdvcc<VEC_T,TRF_T,DERIV_T>::CalcSizeParams
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      Uin size = 0;
      for(const auto& mc: arMcr)
      {
         size += NumParams(mc, arNModals);
      }
      return size;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename Tdvcc<VEC_T,TRF_T,DERIV_T>::oper_t Tdvcc<VEC_T,TRF_T,DERIV_T>::TimeIndepOper
      (  const OpDef& arOpDef
      ,  param_t aCoef
      )
   {
      oper_t op;
      op.emplace_back(arOpDef, aCoef);
      return op;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::PassSettingsToTrfs
      (
      )
   {
      for(auto& trf: mVecTrf)
      {
         PassSettingsToTrf(trf, *this);
      }

      for(auto& etrf: mVecExpValTrfs)
      {
         PassSettingsToTrf(etrf, *this);
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SanityCheckState
      (  const std::map<ParamID,libmda::util::any_type>& arParams
      )  const
   {
      for(const auto& kv: arParams)
      {
         if (  (  kv.first == ParamID::BRA
               || kv.first == ParamID::KET
               )
            && kv.second.template get<GeneralMidasVector<param_t>>().Size() != SizeParams()
            )
         {
            std::stringstream ss;
            ss << "Param. container size of " << StringFromEnum(kv.first)
               << " (which is " << kv.second.template get<GeneralMidasVector<param_t>>().Size()
               << ") != expected SizeParams (which is " << SizeParams()
               << ")."
               ;
            MIDASERROR(ss.str());
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SetInitStateImpl
      (  std::map<ParamID,libmda::util::any_type>&& arParams
      )
   {
      mpInitState = std::make_unique<vec_t>(std::move(arParams), mrMcr, mrNModals);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SetInitStateForAutoCorrImpl
      (  std::map<ParamID,libmda::util::any_type>&& arParams
      )
   {
      // MGH-NB: Any checks on initial state type should go in this function if they ever become necessary.
      mpInitStateForAutoCorr = std::make_unique<vec_t>(std::move(arParams), mrMcr, mrNModals);

      // Sanity check
      if constexpr (VEC_T::corr_type == CorrType::TDMVCC)
      {
         MIDASERROR("I should not be called for TDMVCC!");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SetVccGroundStateImpl
      (  std::map<ParamID,libmda::util::any_type>&& arParams
      )
   {
      mpVccGroundState = std::make_unique<vec_t>(std::move(arParams), mrMcr, mrNModals);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::ExptValReserveImpl
      (  const Uin aSize
      )
   {
      if (mVecExpValOpers.size() > 0)
      {
         MIDASERROR("mVecExpValOpers.size() ("+std::to_string(mVecExpValOpers.size())+") > 0; can only reserve once!");
      }
      mVecExpValOpers.reserve(aSize);
      mVecExpValTrfs.reserve(aSize);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::EnableExptValImpl
      (  const OpDef* apOpDef
      ,  ModalIntegrals<Nb>&& arModInts
      )
   {
      if (mVecExpValOpers.size() < mVecExpValOpers.capacity())
      {
         mVecExpValOpers.emplace_back
            (  std::make_tuple
                  (  apOpDef
                  ,  midas::vcc::modalintegrals::detail::ConvertFromReal<param_t>::Convert
                        (  std::move(arModInts)
                        )
                  )
            );
         const auto& oper = *std::get<0>(mVecExpValOpers.back());
         const auto& modints = std::get<1>(mVecExpValOpers.back());
         if constexpr   (  VEC_T::corr_type == CorrType::TDMVCC
                        )
         {
            mVecExpValTrfs.emplace_back
               (  TRF_T(this->NTdModals(), this->NModals(), oper, modints, mrMcr)
               );
         }
         else
         {
            mVecExpValTrfs.emplace_back
               (  TRF_T(mrNModals, oper, modints, mrMcr)
               );
         }
         //Niels: Settings are now passed in Tdvcc::PassSettingsToTrfs
         //PassSettingsToTrf(mVecExpValTrfs.back(), *this);
      }
      else
      {
         MIDASERROR("Trying to enable more expt.val. opers. than reserved space for (size = "+std::to_string(mVecExpValOpers.size())+", capacity = "+std::to_string(mVecExpValOpers.capacity())+").");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   std::map<ParamID,libmda::util::any_type> Tdvcc<VEC_T,TRF_T,DERIV_T>::ReadParamsFromFile
      (  const std::string& arRestartFilePath
      )
   {
      if (GetCorrType() == CorrType::VCI)
      {
         MIDASERROR("ReadParamsFromFile is not implemented for TDVCI!");
      }

      if (!midas::filesystem::IsDir(arRestartFilePath))
      {
         MIDASERROR("Restart directory does not seem to exist!");
      }

      Mout << "About to restart TDVCC using parameters from: " << arRestartFilePath << std::endl;

      param_t phase(-I_1);
      GeneralMidasVector<param_t> primal_params_vec, dual_params_vec;

      std::vector<std::string> param_objects{"Phase", "PrimalParams", "DualParams"};
      for (auto & param_obj : param_objects)
      {
         std::string file_name = arRestartFilePath + param_obj + ".dat";
         if (!midas::filesystem::IsFile(file_name))
         {
            MIDASERROR("The file " + file_name + " does not exist!");
         }
         std::istringstream params_file_stream = midas::mpi::FileToStringStream(file_name);
         std::string s = "";

         if (param_obj == "Phase")
         {
            phase = midas::input::Get<param_t>(params_file_stream, s);
         } 
         else if (param_obj == "PrimalParams")
         {
            midas::input::GetLine(params_file_stream, s);
            size_t str_beg = s.find_first_of('(');
            primal_params_vec = std::move(midas::util::GeneralMidasVectorFromString<param_t>(s.substr(str_beg)));
         } 
         else if (param_obj == "DualParams")
         {
            midas::input::GetLine(params_file_stream, s);
            size_t str_beg = s.find_first_of('(');
            dual_params_vec = std::move(midas::util::GeneralMidasVectorFromString<param_t>(s.substr(str_beg)));
         } 
         else
         {
            MIDASERROR("Unrecongnized parameter type in Tdvcc::ReadParamsFromFile: " + param_obj);
         }
      }

      // We always return as GeneralMidasVectors since ExtractFromMap in ParamsTdvccUtils 
      // will ensure conversion to correct container type.
      std::map<ParamID, libmda::util::any_type> m;
      m[ParamID::KET] = std::move(primal_params_vec);
      m[ParamID::BRA] = std::move(dual_params_vec);
      m[ParamID::PHASE] = phase;
      return m;
   }

   /************************************************************************//**
    * This function has been moved to Td(m)vcc classes so it has access to 
    * 'param_t' so ExtractFromMap does not get confused trying to get
    * GeneralMidasVector<complex<Nb>> from anytype<GeneralMidasVector<Nb>>
    * or vise versa.
    * Now unit modal_mats are just constructed in Tdmvcc version instead of
    * checking whether it is tdmvcc or not.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   std::map<ParamID,libmda::util::any_type> 
   Tdvcc<VEC_T,TRF_T,DERIV_T>::PrepInitState
      (  InitType aInit
      ,  CorrType aCorr
      ,  Uin aSizeParams
      ,  const std::vector<Uin>& aNPrimModals
      ,  const std::vector<Uin>& aNTdModals
      ,  const std::string& arVccBaseName
      ,  In aSizeInitState
      )
   {
      std::map<ParamID,libmda::util::any_type> m;
      switch(aInit)
      {
         case InitType::VSCFREF:
         {
            m[ParamID::KET] = GeneralMidasVector<param_t>(aSizeParams, param_t(0));
            if (  aCorr == CorrType::VCC
               || aCorr == CorrType::EXTVCC
               || aCorr == CorrType::TDMVCC
               )
            {
               m[ParamID::BRA] = GeneralMidasVector<param_t>(aSizeParams, param_t(0));
            }
            else if (aCorr == CorrType::VCI)
            {
               auto& mv = m[ParamID::KET].template get<GeneralMidasVector<param_t>>();
               mv[0] = 1;
            }

            break;
         }
         case InitType::VCCGS:
         {
            // Definition is inside td/tdvcc/DriverUtils.cc
            m = midas::tdvcc::LoadVccGs(aInit, aCorr, aSizeParams, arVccBaseName, aSizeInitState);
            break;
         }
         default:
         {
            MIDASERROR("Called with InitType '"+StringFromEnum(aInit)+"'; don't know what to do.");
            break;
         }
      }

      return m;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveProperties
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedProps())
      {
         switch(id)
         {
            case PropID::ENERGY:
               SaveEnergyAndContribsImpl(aTime, arVec);
               break;
            case PropID::PHASE:
               SavePhaseImpl(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(id)+"'.");
         }
      }
      for(const auto& id: this->TrackedStatePopulations())
      {
         switch(id)
         {
            case PropID::STATEPOPULATIONS:
               SaveStatePopulationsImpl(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(id)+"'.");
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveStats
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedStats())
      {
         switch(id)
         {
            case StatID::FVCINORM2:
               SaveFvciNorm2Impl(arVec);
               break;
            case StatID::NORM2:
               SaveParamNorm2Impl(arVec);
               break;
            case StatID::DNORM2INIT:
               SaveParamNorm2InitImpl(arVec);
               break;
            case StatID::DNORM2VCCGS:
               SaveParamNorm2InitVccGs(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for StatID '"+StringFromEnum(id)+"'.");
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveExpValsImpl
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      LOGCALL("expval calc");
      std::map<std::string,ifc_prop_t> m;
      for(Uin i = 0; i < this->mVecExpValTrfs.size(); ++i)
      {
         try
         {
            const auto& oper = *std::get<0>(mVecExpValOpers.at(i));
            const auto& name = oper.Name();
            const ifc_prop_t val = ExpVal(aTime, arVec, mVecExpValTrfs[i]);
            m[name] = val;
         }
         catch(const std::out_of_range& oor)
         {
            MIDASERROR("Caught out-of-range, i = "+std::to_string(i)+"; "+std::string(oor.what()));
         }
      }
      this->SaveExpVals(m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrelations
      (  step_t
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedAutoCorr())
      {
         switch(id)
         {
            case PropID::AUTOCORR:
               SaveAutoCorrImpl(arVec);
               break;
            case PropID::AUTOCORR_EXT:
               SaveAutoCorrExtImpl(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(id)+"'.");
               break;
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveEnergyAndContribsImpl
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      LOGCALL("energy + contrib calc");
      // term<{contrib,coef,deriv}> for passing on to base class.
      std::vector<std::tuple<ifc_prop_t, ifc_param_t, ifc_param_t>> v_tpl;
      for(Uin i = 0; i < mHamiltonian.size(); ++i)
      {
         v_tpl.emplace_back
            (  ExpVal(aTime, arVec, mVecTrf[i])
            ,  mHamiltonian.at(i).Coef(aTime)
            ,  mHamiltonian.at(i).Deriv(aTime)
            );
      }
      this->SaveEnergyAndContribs(v_tpl);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SavePhaseImpl
      (  const vec_t& arVec
      )
   {
      this->SavePhase(arVec.Phase());
   }
   
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveStatePopulationsImpl
      (  const vec_t& arVec
      )
   {
      LOGCALL("statepop calc");
      if constexpr   (  std::is_same_v<TRF_T, TrfTdmvcc2<param_t>>
                     )
      {
         MIDASERROR("State populations is not implemented for TDMVCC[2] !");
      }
      auto statepop = StatePopulations<TRF_T>(arVec, this->mrNModals, this->mrMcr, this->mNElectronicStates);

      this->SaveStatePopulations(statepop);
   }

   /************************************************************************//**
    * Convention:
    *    -  PHASE: stores exp(2Im(phase))
    *    -  KET:   stores FVCI norm^2 without phase factor
    *    -  BRA:   zero for now (in principle same as KET for VCI, but for VCC
    *    it could be either same as KET or the norm of the Lambda state)
    *    -  total: product of PHASE and KET values
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveFvciNorm2Impl
      (  const vec_t& arVec
      )
   {
      const ifc_absval_t val = FvciNorm2Ket(arVec, this->mrNModals, this->mrMcr);
      const ifc_absval_t phase = exp(2*std::imag(arVec.Phase()));
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         switch(id)
         {
            case ParamID::PHASE:
               m[id] = phase;
               break;
            case ParamID::KET:
               m[id] = val;
               break;
            case ParamID::BRA:
               m[id] = FvciNorm2Bra(arVec, this->mrNModals, this->mrMcr);
            default:
               break;
         }
      }
      this->SaveStat(StatID::FVCINORM2, phase*val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2Impl
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = Norm2(arVec);
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = Norm2(id, arVec);
      }
      this->SaveStat(StatID::NORM2, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2InitImpl
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = DiffNorm2(arVec, this->InitState());
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = DiffNorm2(id, arVec, this->InitState());
      }
      this->SaveStat(StatID::DNORM2INIT, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2InitVccGs
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = DiffNorm2(arVec, this->VccGroundState());
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = DiffNorm2(id, arVec, this->VccGroundState());
      }
      this->SaveStat(StatID::DNORM2VCCGS, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrImpl
      (  const vec_t& arVec
      )
   {
      LOGCALL("autocorr calc");
      auto ac = AutoCorr(arVec, this->InitStateForAutoCorr(), this->mrNModals, this->mrMcr);
      if constexpr   (  VEC_T::corr_type == CorrType::TDMVCC
                     )
      {
         MidasWarning("Should this ever be called ? We are inside Tdvcc_Impl.h not Tdmvcc_Impl.h");
         this->SaveAutoCorrTdmvcc(PropID::AUTOCORR, ac);
      }
      else
      {
         this->SaveAutoCorr(PropID::AUTOCORR, ac);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrExtImpl
      (  const vec_t& arVec
      )
   {
      auto ac = AutoCorrExt(arVec, this->InitStateForAutoCorr(), this->mrNModals, this->mrMcr);
      if constexpr   (  VEC_T::corr_type == CorrType::TDMVCC
                     )
      {
         this->SaveAutoCorrTdmvcc(PropID::AUTOCORR, ac);
      }
      else
      {
         this->SaveAutoCorr(PropID::AUTOCORR_EXT, ac);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveFvciVecImpl
      (  const vec_t& arVec
      )
   {
      GeneralDataCont<param_t> dc_ket;
      GeneralDataCont<param_t> dc_bra;
      if constexpr   (  std::is_same_v<VEC_T, ParamsTdvcc<param_t, GeneralTensorDataCont>>
                     || std::is_same_v<VEC_T, ParamsTdvci<param_t, GeneralTensorDataCont>>
                     )
      {
         // Niels: It is a little stupid that we convert back and forth between data formats.
         dc_ket = DataContFromTensorDataCont(ConvertKetToFvci(arVec, this->mrNModals, this->mrMcr));
         dc_bra = DataContFromTensorDataCont(ConvertBraToFvci(arVec, this->mrNModals, this->mrMcr));
      }
      else
      {
         dc_ket = GeneralDataCont<param_t>(ConvertKetToFvci(arVec, this->mrNModals, this->mrMcr));
         dc_bra = GeneralDataCont<param_t>(ConvertBraToFvci(arVec, this->mrNModals, this->mrMcr));
      }
      GeneralMidasVector<ifc_param_t> mv_ket(std::move(*dc_ket.GetVector()));
      Scale(mv_ket, exp(ifc_param_t(0,-1)*arVec.Phase()));
      Scale(mv_ket, 1.0/Norm(mv_ket));

      GeneralMidasVector<ifc_param_t> mv_bra(std::move(*dc_bra.GetVector()));
      Scale(mv_bra, exp(ifc_param_t(0,+1)*arVec.Phase()));
      Scale(mv_bra, 1.0/Norm(mv_bra));

      this->SaveFvciVec(std::move(mv_ket), std::move(mv_bra));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::SaveTdModalPlotsImpl
      (  const vec_t& aParams
      )  const
   {
      MIDASERROR("SaveTdModalPlotsImpl should not be called for Tdvcc, no td-modals here !");
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::PrintOutPotential
      (
      )  const
   {
      size_t width = 25;
      std::string working_dir = gAnalysisDir + "/" + this->Name() + "_one_mode_potentials" + "/";
      if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
      {
         MIDASERROR("Directory " + working_dir + " could not be created");
      }

      const auto& mcr = this->mrMcr;
      const auto& hamiltonian = this->mHamiltonian;
      if (hamiltonian.size() != I_1)
      {
         MIDASERROR("Figure out how the vector part of td hamiltonian works and implement me again");
      }
      const auto& oper = hamiltonian.at(0).Oper();
      const auto& one_mode_oper_sets = oper.GetOneModeOperSets();

      for (auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto& m = mc.Mode(0);

         auto grid_points = this->GetGridPointsForPlot(m);
         GeneralMidasVector<Nb> res_pot(grid_points.Size(), Nb(0));
         const auto& terms = oper.OpersForMc(mc);

         if (oper.ScaledPot())
         {
            Nb scal_fac = oper.ScaleFactorForMode(m);
            grid_points.Scale(scal_fac);
         }

         for (auto& i : terms)
         {
            const auto& c_coef = oper.Coef(i);

            const auto& opers = oper.GetOpers(i);
            const auto& oper_m = opers[0];

            const auto global_oper_nr = one_mode_oper_sets.at(m).GetGlobalOperNr(oper_m);
            const auto& one_mode_oper = gOperatorDefs.GetOneModeOper(global_oper_nr);

            for (In q_i = I_0; q_i < grid_points.Size(); ++q_i)
            {
               res_pot[q_i] += c_coef * one_mode_oper->EvaluateFunc(grid_points[q_i]);
            }
         }

         std::string file_name_pot = working_dir + "potential_mode_" + std::to_string(m);
         std::ofstream pot_ofs;
         pot_ofs.open(file_name_pot);
         pot_ofs << std::scientific << std::setprecision(16);
         for (In i = I_0; i < grid_points.size(); ++i)
         {
            pot_ofs << std::setw(width) << grid_points[i] << std::setw(width) << res_pot[i] << std::endl;
         }
         pot_ofs.close();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void Tdvcc<VEC_T,TRF_T,DERIV_T>::EvolveImpl
      (
      )
   {
      OdeIntegrator<Tdvcc<VEC_T,TRF_T,DERIV_T>> ode(this, this->GetOdeInfo());
      mpFinalState = std::make_unique<vec_t>(InitState());
      ode.Integrate(*mpFinalState);
   }

} /* namespace midas::tdvcc */

#endif/*TDVCC_IMPL_H_INCLUDED*/
