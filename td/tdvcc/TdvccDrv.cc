/**
 *******************************************************************************
 * 
 * @file    TdvccDrv.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.
#include <vector>
#include <string>
#include <map>
#include <algorithm>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/BasDef.h"
#include "input/TdvccCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "td/tdvcc/TdvccIfc.h"
#include "td/tdvcc/TdvccIfcFactory.h"
#include "td/tdvcc/DriverUtils.h"
#include "td/tdvcc/TdvccEnums.h"
#include "input/Trim.h"


// Extern/forward declares.
extern std::vector<BasDef> gBasis;
extern GlobalOperatorDefinitions gOperatorDefs;
extern In gVibIoLevel;
extern std::vector<input::TdvccCalcDef> gTdvccCalcDef;
extern std::vector<VscfCalcDef> gVscfCalcDef;
extern std::vector<VccCalcDef> gVccCalcDef;
extern std::string gAnalysisDir;


namespace midas::tdvcc
{
namespace detail
{
/**
 * Construct MCR for TDVCC calculation
 *
 * @param aMaxExci
 * @param aNModes
 * @param aExtRangeModeLabels
 * @return
 *    MCR
 **/
ModeCombiOpRange ConstructMcr
   (  In aMaxExci
   ,  In aNModes
   ,  const std::set<std::string>& aExtRangeModeLabels
   )
{
   if (  aExtRangeModeLabels.empty()
      )
   {
      return ModeCombiOpRange(aMaxExci, aNModes);
   }
   else
   {
      std::set<In> ext_range_mode_nrs;
      for(const auto& l : aExtRangeModeLabels)
      {
         ext_range_mode_nrs.emplace(gOperatorDefs.ModeNr(l, false));
      }
      return ConstructModeCombiOpRangeFromExtendedRangeModes(aMaxExci, aNModes, ext_range_mode_nrs);
   }
}
/**
 * Construct modal limits including electronic DOF
 *
 * @param aOper               OpDef for Hamiltonian
 * @param aModalBasisLimits   Modal-basis limits for vibrational modes
 * @return
 *    Modal-basis limits including electronic DOF (if it exists)
 **/
std::vector<Uin> GetNModals
   (  const OpDef& aOper
   ,  const std::vector<Uin>& aModalBasisLimits
   )
{
   std::vector<Uin> nmodals;
   Uin mode_count = 0;
   auto nmodes = aOper.NmodesInOp();
   auto ndof = nmodes;
   if (  gOperatorDefs.ElectronicDofNr() != -I_1
      )
   {
      ++ndof;
   }
   nmodals.reserve(ndof);
   for(In imode=I_0; imode<nmodes; ++imode)
   {
      auto g_mode = aOper.GetGlobalModeNr(imode);
      if (  g_mode == gOperatorDefs.ElectronicDofNr()
         )
      {
         nmodals.emplace_back(aOper.StateTransferIJMax(imode) + 1);
      }
      else
      {
         nmodals.emplace_back(aModalBasisLimits[mode_count++]);
      }
   }
   return nmodals;
}
} /* namespace detail */

/**
* Run TDVCC calculations
* */
void TdvccDrv()
{
   using step_t = TdvccIfc::ifc_step_t;
   using param_t = TdvccIfc::ifc_param_t;
   using vec_t = GeneralMidasVector<param_t>;

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin TDVCC calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   if (  gDebug
      || gVibIoLevel > I_5
      )
   {
      Mout << " Number of TDVCC calculations to perform: " << gTdvccCalcDef.size() << std::endl;
   }

   // Make a map over oper/basis combinations used in the whole set of TDVCC calculations.
   std::map<std::string,In> oper_bas_map;
   for (In i_calc=gTdvccCalcDef.size()-1; i_calc>=0; --i_calc)
   {
      std::string oper_name  = gTdvccCalcDef[i_calc].GetOper();
      std::string basis_name = gTdvccCalcDef[i_calc].GetBasis();
      std::string oper_bas  = oper_name + "_" + basis_name;
      oper_bas_map[oper_bas]=i_calc;
   }

   if (  gDebug
      || gVibIoLevel > I_7
      )
   {
      Mout << " Calc. nr. | oper_basis: " << std::endl;
      for(const auto& po : oper_bas_map)
      {
          Mout << "  " << po.second << " | " << po.first << std::endl;
      }
   }

   // Stuff for cross-calc. comparison.
   std::vector<std::vector<step_t>> vec_interpol_ts;
   std::vector<std::vector<vec_t>> vec_fvci_vecs_ket;
   std::vector<std::vector<vec_t>> vec_fvci_vecs_bra;

   for(In i_calc=0; i_calc<gTdvccCalcDef.size(); ++i_calc)
   {
      auto& calcdef = gTdvccCalcDef[i_calc];
      // Set IoLevel to be gt gVibIoLevel
      calcdef.SetIoLevel(std::max(calcdef.GetIoLevel(), Uin(gVibIoLevel)));

      // Find VscfCalcDef to initialize from.
      const Uin i_vscf = FindVscf(gVscfCalcDef, calcdef.GetModalBasisFromVscf());
      auto& vscf_calcdef = gVscfCalcDef[i_vscf];

      // Find operator number
      const auto& oper_name = calcdef.GetOper();
      const Uin i_oper = FindOper(gOperatorDefs, oper_name);
      const Uin i_vscf_oper = FindOper(gOperatorDefs, vscf_calcdef.Oper());

      // Find basis number
      MidasAssert(calcdef.GetBasis() == vscf_calcdef.Basis(), "TDVCC must use the same primitive basis as VSCF");
      Uin i_basis = FindBasis(gBasis, calcdef.GetBasis());

      // Generate basis set
      gBasis[i_basis].InitBasis(gOperatorDefs[i_vscf_oper]);

      Mout << "\n\n\n Do TDVCC: \"" << calcdef.GetName() << "\" calculation" << std::endl;

      if (  gDebug
         || gVibIoLevel > I_2
         )
      {
         Mout  << "\n\n";
         Mout  << " Operator:                            "
               << oper_name << std::endl;
         Mout  << " Operator-Type:                       "
               << gOperatorDefs[i_oper].Type() << std::endl;
         Mout  << " Number of terms in operator:         "
               << gOperatorDefs[i_oper].Nterms() << std::endl;
         Mout  << " Basis:                               "
               << gBasis[i_basis].GetmBasName() << std::endl;
         Mout  << " Basis-Type:                          "
               << gBasis[i_basis].GetmBasSetType() << std::endl;
         Mout  << " Number of modes:                     "
               << gOperatorDefs[i_vscf_oper].NmodesInOp() << std::endl;
         Mout  << std::endl;
      }

      // Set up modal basis dimensions.
      std::array<std::vector<Uin>,2> arr_n_modals;
      if (IsTdmMethod(calcdef.GetCorrMethod()))
      {
         const auto [has_td_bas, td_bas] = calcdef.GetTdModalBasis();
         MidasAssert(has_td_bas, "Doing TDM calc ("+StringFromEnum(calcdef.GetCorrMethod())+"), but no TD basis.");
         arr_n_modals = 
            {  calcdef.GetLimitModalBasis()
            ,  td_bas
            };
      }
      else
      {
         arr_n_modals = 
            {  calcdef.GetLimitModalBasis()
            ,  calcdef.GetLimitModalBasis()
            };
      }

      // Check that input for primitive (HO) basis is reasonable
      if (gBasis[i_basis].GetmUseHoBasis())
      {
         for (In i_mode = I_0; i_mode < arr_n_modals.at(0).size(); ++i_mode)
         {
            const In modal_limit = arr_n_modals.at(0).at(i_mode);
            const In prim_limit = gBasis[i_basis].Higher(i_mode);
            if (!(prim_limit >= modal_limit - 1))
            {
            
               std::stringstream ss;
               ss << "Inconsistency in input. Limit modal basis for tdvcc calculation for mode " << i_mode << " "
                  << "is higher than highest order/quantum number for primitive basis." << std::endl
                  << "Primitive basis limit: " << prim_limit << std::endl
                  << "Td basis limit       : " << modal_limit << std::endl
                  ;
               MIDASERROR(ss.str());
            }
         }
      }

      // Insert number of electronic states if not present already
      for(auto& nmodals : arr_n_modals)
      {
         if (  nmodals.size() != gOperatorDefs[i_oper].NmodesInOp()
            )
         {
            Mout  << " Inserting number of electronic states in N vector.\n"
                  << "    Vec. before:    " << nmodals << "\n"
                  << std::flush;
            auto edof = gOperatorDefs.ElectronicDofNr();
            auto it = nmodals.begin() + edof;
            auto local_edof = gOperatorDefs[i_oper].GetLocalModeNr(edof);
            auto nstates = gOperatorDefs[i_oper].StateTransferIJMax(local_edof) + I_1;
            nmodals.insert(it, nstates);
            Mout  << "    Vec. after:     " << nmodals << std::endl;
         }
      }


      // Find and load VSCF.
      std::string vscf_modals_name = vscf_calcdef.GetName() + "_Modals";
      auto modints = GetModalIntegrals(gOperatorDefs[i_oper], gBasis[i_basis], arr_n_modals.at(0), vscf_modals_name);
      const auto vscf_modal_coefs = GetVscfModalCoefs(gOperatorDefs[i_oper], gBasis[i_basis], vscf_modals_name, calcdef.GetLimitModalBasis());

      // Find VCC.
      const VccCalcDef* p_vcc = nullptr;
      if (  !gVccCalcDef.empty()
         )
      {
         const Uin i_vcc = FindVcc(gVccCalcDef, calcdef.GetVccGs());
         p_vcc = &gVccCalcDef[i_vcc];
      }
      // p_vcc might still be nullptr, so remember to check when using it!
      if (  calcdef.GetInitStateType() == tdvcc::InitType::VCCGS
         && !p_vcc
         )
      {
         MIDASERROR("No VCC ground-state calculation found for initializing TDVCC[2] wave packet.");
      }
      // Starting from VCC is not implemented for TDMVCC yet
      // This will require applying the e^{T_1} transformations to the modal matrices,
      // but it is unclear what to do about L_1...
      if (  calcdef.GetInitStateType() == tdvcc::InitType::VCCGS
         && IsTdmMethod(calcdef.GetCorrMethod())
         )
      {
         MIDASERROR("Starting TDMVCC from VCC ground state has not been implemented yet.");
      }

      // Assume this MCR fits what is in the initial VCC calc. if using VCC init.
      auto mcr = detail::ConstructMcr(calcdef.GetMaxExci(), gOperatorDefs[i_oper].NmodesInOp(), calcdef.GetExtRangeModeLabels());

      // Get number of modals
      std::vector<Uin> n_modals;
      if (  IsTdmMethod(calcdef.GetCorrMethod())
         ) 
      {
         n_modals = detail::GetNModals(gOperatorDefs[i_oper], arr_n_modals.at(1));
      }
      else
      {
         //n_modals = detail::GetNModals(gOperatorDefs[i_oper], calcdef.LimitModalBasis());
         n_modals = detail::GetNModals(gOperatorDefs[i_oper], arr_n_modals.at(0));
      }
      
      const Uin size_params = SetAddresses(mcr, n_modals);
      

      // Analysis of Hermitian part of Jacobian, if requested.
      if (p_vcc != nullptr && calcdef.GetAnalyzeHermPartJacVccGs().first)
      {
         const auto& opdef = gOperatorDefs[i_oper];
         const auto& basdef = gBasis[i_basis];
         const auto vcc_gs = LoadVccGs(InitType::VCCGS, CorrType::VCC, size_params, p_vcc->GetName());
         const auto eigpairs = DiagHermPartVccGs
            (  n_modals
            ,  opdef
            ,  GetModalIntegralsReal(opdef, basdef, n_modals, vscf_modals_name)
            ,  mcr
            ,  vcc_gs.at(ParamID::KET).template get<GeneralMidasVector<Nb>>()
            ,  calcdef.GetAnalyzeHermPartJacVccGs().second
            );
         Mout << "Eigen-pair analysis for Herm(VCC-GS Jacobian):" << std::endl;
         PrintEigenpairAnalysis(Mout, eigpairs, n_modals, mcr);
      }

      // The time-dep. pulse, if we're using one.
      using pulse_t = midas::td::GaussPulse<step_t,param_t>;
      std::vector<pulse_t> v_pulse;
      Uin i_pulse = 0;

      // Construct TDVCC object
      std::unique_ptr<TdvccIfc> p_tdvcc = nullptr;
      if (calcdef.GetUsePulse())
      {
         using step_t = TdvccIfc::ifc_step_t;
         using param_t = TdvccIfc::ifc_param_t;
         using oper_t = midas::td::LinCombOper<step_t, param_t, OpDef>;
         const std::vector<std::string>& oper_pulse_names = calcdef.GetOperPulse();
         const std::vector<std::map<midas::td::GaussParamID,Nb>>& oper_pulse_specs = calcdef.GetPulseSpecs();
         oper_t lc_oper;
         lc_oper.emplace_back
            (  gOperatorDefs[i_oper]
            ,  1
            );
         std::vector<ModalIntegrals<param_t>> v_modints = {std::move(modints)};
         for (In pulse_idx = I_0; pulse_idx < oper_pulse_names.size(); pulse_idx++)
         {
            i_pulse = FindOper(gOperatorDefs, oper_pulse_names.at(pulse_idx));
            v_pulse.emplace_back(oper_pulse_specs.at(pulse_idx));
            const pulse_t& pulse = v_pulse.back();

            lc_oper.emplace_back
               (  gOperatorDefs[i_pulse]
               ,  [p=pulse](step_t t)->param_t {return p(t);}
               ,  [p=pulse](step_t t)->param_t {return p.Deriv(t);}
               );
            v_modints.emplace_back(GetModalIntegrals(gOperatorDefs[i_pulse], gBasis[i_basis], arr_n_modals.at(0), vscf_modals_name));
         }
         p_tdvcc = TdvccIfcFactory
            (  calcdef.GetCorrMethod()
            ,  calcdef.GetTransformer()
            ,  calcdef.GetConstraint()
            ,  arr_n_modals
            ,  std::move(lc_oper)
            ,  std::move(v_modints)
            ,  mcr
            );
      }
      else
      {
         p_tdvcc = TdvccIfcFactory
            (  calcdef.GetCorrMethod()
            ,  calcdef.GetTransformer()
            ,  calcdef.GetConstraint()
            ,  arr_n_modals
            ,  gOperatorDefs[i_oper]
            ,  std::move(modints)
            ,  mcr
            );
      }

      Mout << "Passing post-factory settings." << std::endl;
      PostFactorySettings(*p_tdvcc, calcdef, p_vcc, arr_n_modals, gBasis[i_basis], vscf_modal_coefs);
      Mout << "Enable expectation values." << std::endl;
      EnableExpVals(*p_tdvcc, gOperatorDefs, calcdef.GetExptValOpers(), gBasis[i_basis], arr_n_modals.at(0), vscf_modals_name);

      Mout << "TDVCC settings for '" << p_tdvcc->Name() << "':" << std::endl;
      p_tdvcc->PrintSettings(Mout, 3);

      if (!v_pulse.empty())
      {
         const std::vector<std::string>& oper_pulse_names = calcdef.GetOperPulse();
         for (In pulse_idx = I_0; pulse_idx < oper_pulse_names.size(); pulse_idx++)
         {
            i_pulse = FindOper(gOperatorDefs, oper_pulse_names.at(pulse_idx));
            Mout << "Pulse used for operator '" << gOperatorDefs[i_pulse].Name() << "':" << std::endl;
            v_pulse.at(pulse_idx).PrintSettings(Mout, 3, 33);
         }
      }

      // Initalize timer.
      Timer timer;
      timer.Reset();

      // Evolve the TDVCC EOMs
      p_tdvcc->Evolve();

      // Output timings.
      Mout << std::endl;
      timer.CpuOut (Mout, "CPU  time spent on TDVCC integration, '"+p_tdvcc->Name()+"': ");
      timer.WallOut(Mout, "Wall time spent on TDVCC integration, '"+p_tdvcc->Name()+"': ");
      Mout << std::endl;

      // Finalize.
      Mout << "TDVCC propagation done for '" << p_tdvcc->Name() << "':" << std::endl;
      p_tdvcc->Summary(Mout, 3);
      Mout << "Saving results on file..." << std::endl;
      p_tdvcc->PrintToFiles(Mout, gAnalysisDir, 3);
      Mout << "Calculating and writing spectra..." << std::endl;
      p_tdvcc->CalcAndWriteSpectra(Mout, gAnalysisDir, 3, calcdef);

      // Here we start extracting stuff from the object, so this _must_ be at
      // the end, just before it goes out of scope!
      if (p_tdvcc->SaveFvciVecs())
      {
         Mout << "Storing FVCI vectors and interpolated times for later use..." << std::endl;
         vec_interpol_ts.emplace_back(std::move(*p_tdvcc).ExtractInterpolTs());
         vec_fvci_vecs_ket.emplace_back(std::move(*p_tdvcc).ExtractFvciVecsKet());
         vec_fvci_vecs_bra.emplace_back(std::move(*p_tdvcc).ExtractFvciVecsBra());
      }
   }

   if (vec_interpol_ts.size() > 1)
   {
      Mout << "\n\n";
      Out72Char(Mout,'+','-','+');
      Out72Char(Mout,'|',' ','|');
      OneArgOut72(Mout," Cross-calculation comparisons",'|');
      Out72Char(Mout,'|',' ','|');
      Out72Char(Mout,'+','-','+');

      const std::string abs_path = midas::input::RemoveTrailingSlash(gAnalysisDir);
      if (!midas::filesystem::IsDir(abs_path))
      {        
         MIDASERROR("Directory '"+abs_path+"' not found.");
      }        

      Mout << "Cross-comparing FVCI vectors..." << std::endl;
      {
         const auto [success, tbl_ts, tbl_data, tbl_headers] = CrossCompareFvci(vec_interpol_ts, vec_fvci_vecs_ket);
         if (success)
         {
            std::vector<const std::vector<Nb>*> p_tbl_data;
            p_tbl_data.reserve(tbl_data.size());
            for(const auto& t: tbl_data)
            {
               p_tbl_data.emplace_back(&t);
            }
            const std::string file_name = abs_path+"/fvci_comparisons_ket.dat";
            TdvccIfc::PrintTableToFile(file_name, tbl_headers, tbl_ts, p_tbl_data);
            Mout  << std::setw(3) << "" << " - " << std::setw(20) << "FVCI-kets"
                  << "(file: " << file_name << ")\n"
                  ;
         }
      }
      {
         const auto [success, tbl_ts, tbl_data, tbl_headers] = CrossCompareFvci(vec_interpol_ts, vec_fvci_vecs_bra);
         if (success)
         {
            std::vector<const std::vector<Nb>*> p_tbl_data;
            p_tbl_data.reserve(tbl_data.size());
            for(const auto& t: tbl_data)
            {
               p_tbl_data.emplace_back(&t);
            }
            const std::string file_name = abs_path+"/fvci_comparisons_bra.dat";
            TdvccIfc::PrintTableToFile(file_name, tbl_headers, tbl_ts, p_tbl_data);
            Mout  << std::setw(3) << "" << " - " << std::setw(20) << "FVCI-bras"
                  << "(file: " << file_name << ")\n"
                  ;
         }
      }
   }

   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," TDVCC Done",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');

}

} /* namespace midas::tdvcc */
