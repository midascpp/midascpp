/**
 *******************************************************************************
 * 
 * @file    DriverUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCDRIVERUTILS_H_INCLUDED
#define TDVCCDRIVERUTILS_H_INCLUDED

// Standard headers.
#include <complex>
#include <map>
#include <string>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/TdvccEnums.h"
#include "libmda/util/any_type.h"

// Forward declares.
class VscfCalcDef;
class VccCalcDef;
class OpDef;
class BasDef;
class GlobalOperatorDefinitions;
template<typename> class GeneralDataCont;
template<typename> class GeneralMidasVector;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
class TdvccIfc;
namespace midas::input
{
   class TdvccCalcDef;
}

namespace midas::tdvcc
{
   //!
   Uin FindVscf(const std::vector<VscfCalcDef>&, const std::string&);

   //!
   Uin FindVcc(const std::vector<VccCalcDef>&, const std::string&);

   //!
   Uin FindOper(const GlobalOperatorDefinitions&, const std::string&);

   //!
   Uin FindBasis(const std::vector<BasDef>&, const std::string&);

   //! 
   std::pair<std::vector<Uin>,Uin> GetModalOffsets(const OpDef&, const BasDef&);

   //! Read in modal coefficients from file.
   GeneralDataCont<Nb> GetFromDisk(Uin, const std::string&);

   //!
   ModalIntegrals<Nb> GetModalIntegralsReal
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      );
   ModalIntegrals<std::complex<Nb>> GetModalIntegrals
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      );

   //!
   std::vector<GeneralMidasMatrix<Nb>> GetVscfModalCoefs
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::string& arFileName
      ,  const std::vector<Uin> arLimitModalBasis
      );

   //!
   std::map<ParamID,libmda::util::any_type> LoadVccGs(InitType, CorrType, Uin, const std::string&, In = -1);

   //!
   std::map<ParamID,libmda::util::any_type> PrepInitState(InitType, CorrType, Uin, const std::vector<Uin>&, const std::vector<Uin>&, const std::string& = "", In = -1);

   //!
   void PostFactorySettings
      (  TdvccIfc&
      ,  const input::TdvccCalcDef&
      ,  const VccCalcDef* const
      ,  const std::array<std::vector<Uin>,2>&
      ,  const BasDef& arBasDef
      ,  const std::vector<GeneralMidasMatrix<Nb>>& arVscfModals
      );

   //!
   void EnableExpVals
      (  TdvccIfc& arObj
      ,  const GlobalOperatorDefinitions& arGlobOpDefs
      ,  const std::set<std::string>& arOperNames
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arVscfModalsName
      );

   //! Returns {success?, times, vec<data>, headers}
   std::tuple
      <  bool
      ,  std::vector<Nb>
      ,  std::vector<std::vector<Nb>>
      ,  std::vector<std::string>
      >
   CrossCompareFvci
      (  const std::vector<std::vector<Nb>>& arVecTimes
      ,  const std::vector<std::vector<GeneralMidasVector<std::complex<Nb>>>> arVecFvcis
      );

   //! Construct and diagonalize Hermitian part of VCC g.s. Jacobian. Return N lowest eigenpairs.
   std::vector<std::pair<Nb,GeneralMidasVector<Nb>>> DiagHermPartVccGs
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<Nb>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralMidasVector<Nb>& arTAmpls
      ,  const Uin aNumAnalyze
      );

   //! Printout eigenpair analysis.
   void PrintEigenpairAnalysis
      (  std::ostream& arOs
      ,  const std::vector<std::pair<Nb,GeneralMidasVector<Nb>>>& arVecEigPairs
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

} /* namespace midas::tdvcc */


#endif /* TDVCCDRIVERUTILS_H_INCLUDED */
