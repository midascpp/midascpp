/**
 *******************************************************************************
 * 
 * @file    Tdmvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDMVCC_IMPL_H_INCLUDED
#define TDMVCC_IMPL_H_INCLUDED

// Standard headers
#include <set>

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "ode/OdeIntegrator.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/anal/AutoCorr.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "td/tdvcc/TdvccEnums.h"
#include "util/matrep/OperMat.h"
#include "mmv/MidasMatrix.h"
#include "mmv/ContainerMathWrappers.h"
#include "td/tdvcc/DriverUtils.h"
#include "potentials/MidasPotential.h"

// Extern/forward declares.
extern std::string gAnalysisDir;

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::Tdmvcc
      (  const std::vector<Uin>& arNModals
      ,  const std::vector<Uin>& arNTdModals
      ,  const OpDef& arHamiltonian
      ,  ModalIntegrals<param_t> aModInts
      ,  const ModeCombiOpRange& arMcr
      ,  param_t aHamiltonianCoef
      )
      :  Tdvcc<VEC_T,TRF_T,DERIV_T>
            (  arNModals
            ,  arHamiltonian
            ,  aModInts
            ,  arMcr
            ,  aHamiltonianCoef
            )
      ,  mrNTdModals(arNTdModals)
      ,  mGTrf(ConstraintTransformer(arNTdModals, arMcr))
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::Tdmvcc
      (  const std::vector<Uin>& arNModals
      ,  const std::vector<Uin>& arNTdModals
      ,  oper_t aHamiltonian
      ,  std::vector<ModalIntegrals<param_t>> aVecModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  Tdvcc<VEC_T,TRF_T,DERIV_T>
            (  arNModals
            ,  aHamiltonian
            ,  aVecModInts
            ,  arMcr
            )
      ,  mrNTdModals(arNTdModals)
      ,  mGTrf(ConstraintTransformer(arNTdModals, arMcr))
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   TRF_T Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ConstructTrf
      (  const OpDef& arOpDef
      ,  const ModalIntegrals<param_t>& arModInts
      )  const
   {
      return TRF_T(this->NTdModals(), this->NModals(), arOpDef, arModInts, this->mrMcr);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   GTRF_T Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ConstraintTransformer
      (  const std::vector<Uin>& aNTdModals
      ,  const ModeCombiOpRange& aMcr
      )  const
   {
      if constexpr   (  GTRF_T::zero_g
                     )
      {
         return GTRF_T();
      }
      else if constexpr (  GTRF_T::is_variational
                        )
      {
         return GTRF_T(aNTdModals, aMcr);
      }
      else
      {
         MIDASERROR("Not implemented for GTRF_T = " + std::string(typeid(GTRF_T).name()));
         return GTRF_T();
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SanityCheckState
      (  const std::map<ParamID,libmda::util::any_type>& arParams
      )  const
   {
      for(const auto& kv: arParams)
      {
         if (  (  kv.first == ParamID::BRA
               || kv.first == ParamID::KET
               )
            //&& std::get<GeneralMidasVector<param_t>>(kv.second).Size() != this->SizeParams()
            && kv.second.template get<GeneralMidasVector<param_t>>().Size() != this->SizeParams()
            )
         {
            std::stringstream ss;
            ss << "Param. container size of " << StringFromEnum(kv.first)
               << " (which is " << kv.second.template get<GeneralMidasVector<param_t>>().Size()
               << ") != expected SizeParams (which is " << this->SizeParams()
               << ")."
               ;
            MIDASERROR(ss.str());
         }
         else if  (  kv.first == ParamID::MODALS
                  )
         {
            using mat_t = typename VEC_T::mat_t;
            using modal_cont_t = std::vector<std::pair<mat_t,mat_t>>;
            for (auto & mat_pair : kv.second.template get<modal_cont_t>())
            {
               if (  mat_pair.first.Ncols() != mat_pair.second.Nrows()
                  || mat_pair.first.Nrows() != mat_pair.second.Ncols()
                  )
               {
                  std::stringstream ss;
                  ss << "Param. container of " << StringFromEnum(kv.first)
                     << " has problems for dimensions of either U or W matrices." << std::endl
                     << "U.Ncols() = " << mat_pair.first.Ncols() << " != "
                     << "W.Nrows() = " << mat_pair.second.Nrows() << std::endl
                     << "U.Nrows() = " <<  mat_pair.first.Nrows()  << " != "
                     << "W.Ncols() = " <<  mat_pair.second.Ncols()
                     << std::endl;
                  MIDASERROR(ss.str());
               }
            }
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::PassSettingsToTrfs
      (
      )
   {
      // Call Tdvcc impl first
      Tdvcc<VEC_T,TRF_T,DERIV_T>::PassSettingsToTrfs();

      // Pass to g transformer
      PassSettingsToTrf(this->mGTrf, *this);
   }


   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ZeroOneModeAmplDerivatives
      (  vec_t& arDeriv
      )  const
   {
      auto [t1_norms, l1_norms] = arDeriv.ZeroOneModeAmplitudes(this->mrMcr);

      auto size = t1_norms.size();
      if (  l1_norms.size() != size
         )
      {
         MIDASERROR("Sizes of T1 norms and L1 norms mismatch!");
      }

      auto tnorm_p1 = real_t(1.0) + Norm(arDeriv.ClusterAmps());
      auto lnorm_p1 = real_t(1.0) + Norm(arDeriv.LambdaCoefs());

      if (  this->IoLevel() >= 8
         || gDebug
         )
      {
         Mout  << " Zero one mode amplitudes \n" 
               << " 1 + ||T|| = " << tnorm_p1 << "\n"
               << " 1 + ||L|| = " << lnorm_p1 << "\n"
               << std::flush;
      }

      for(In imode=I_0; imode<size; ++imode)
      {
         if (  this->mGTrf.T1Zero()
            && !libmda::numeric::float_numeq_zero(t1_norms[imode], tnorm_p1, 100)
            )
         {
            Mout  << " ||ds^" << imode << "/dt|| = " << t1_norms[imode] << "    (1 + ||T|| = " << tnorm_p1 << ")" << std::endl;
            MidasWarning("T_1 derivative is non-zero!");
         }
         if (  this->mGTrf.L1Zero()
            && !libmda::numeric::float_numeq_zero(l1_norms[imode], tnorm_p1, 100)
            )
         {
            Mout  << " ||dl^" << imode << "/dt|| = " << l1_norms[imode] << "    (1 + ||L|| = " << lnorm_p1 << ")" << std::endl;
            MidasWarning("L_1 derivative is non-zero!");
         }
      }

   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   Uin Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SizeTotal
      (
      )  const
   {
      return vec_t::SizeTotal(this->SizeParams(), this->NModals(), this->NTdModals());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::PrepareDerivativeImpl
      (  const vec_t& aParams
      )
   {
      // Update time-dependent integrals in all transformers
      for(Uin i = 0; i < this->mHamiltonian.size(); ++i)
      {
         this->mVecTrf[i].UpdateIntegrals(aParams.ModalCont());
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   typename Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::vec_t
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::DerivativeImpl
      (  step_t aTime
      ,  const vec_t& arParams
      ,  const DERIV_T& arDeriv
      )  const
   {
      bool verbose = (this->IoLevel() >= 10);
      if (verbose) Mout << "\n CALCULATE DERIVATIVE " << std::endl;

      // 1) Calculate density matrices and projector
      auto densmats = this->GetHamTrf(0).DensityMatrices(arParams.ClusterAmps(), arParams.LambdaCoefs());
      auto inv_reg_dens = TRF_T::InverseDensityMatrices(densmats, this->InvDensRegularization().first, this->InvDensRegularization().second, verbose);
      auto projector = TRF_T::SecondarySpaceProjector(arParams.ModalCont(), this->NonOrthoActiveSpaceProjector(), this->NonOrthoActiveSpaceProjectorType(), verbose); // Q = 1 - P

      // 2) Add Hamiltonian terms (g matrices not included)
      vec_t result = this->ShapedZeroVector();
      typename TRF_T::meanfields_t mean_fields;
      for(Uin i = 0; i < this->mHamiltonian.size(); ++i)
      {
         // Compute derivative and mean fields (returned in pair/tuple)
         auto deriv = arDeriv(aTime, arParams, this->GetHamTrf(i), inv_reg_dens, projector, this->TimeIt(), this->FullActiveBasis());

         // Add term to EOMs
         Axpy(result, std::move(deriv.first), this->mHamiltonian.at(i).Coef(aTime));

         // Add to total mean fields
         if (  mean_fields.empty()
            )
         {
            mean_fields = std::move(deriv.second);
         }
         else
         {
            if (  mean_fields.size() != deriv.second.size()
               )
            {
               MIDASERROR("Sizes of mean-field-matrix vectors mismatch: " + std::to_string(mean_fields.size()) + " vs. " + std::to_string(deriv.second.size()));
            }
            for(In imode=I_0; imode<mean_fields.size(); ++imode)
            {
               // Call WrapAxpy which works for tuples (std::pair, std::array, std::tuple)
               midas::mmv::WrapAxpy(mean_fields[imode], deriv.second[imode], this->mHamiltonian.at(i).Coef(aTime));
            }
         }
      }

      if constexpr   (  !GTRF_T::zero_g
                     )
      {
         // 3) Calculate g matrices using intermediates from H transforms.
         // Parse intermediates depending on H.
         // For MatRep transformers, we pass the full-space H matrix
         if constexpr   (  TRF_T::trf_type == TrfType::MATREP
                        )
         {
            this->mGTrf.ParseIntermediates(result.ClusterAmps(), result.LambdaCoefs(), this->TotalHMat(aTime), arDeriv.AmplsFactor(), arDeriv.CoefsFactor()); 
         }
         // For efficient implementations, we pass mean-field matrices (and perhaps other stuff?)
         else
         {
            // Construct mean fields in time-dependent basis.
            using mat_t = std::decay_t<decltype(mean_fields[0][0])>;
            using tdmf_t = std::array<mat_t,2>;
            using htmf_t = std::array<mat_t,4>;
            std::vector<tdmf_t> td_mean_fields(mean_fields.size());
            Uin imode = 0;
            std::transform
               (  mean_fields.begin()
               ,  mean_fields.end()
               ,  td_mean_fields.begin()
               ,  [&arParams,&imode,&densmats,this] (const htmf_t& half_trans_mf)
               {
                  const auto& u = arParams.ModalCont()[imode].first;
                  const auto& w = arParams.ModalCont()[imode].second;
                  const auto& rho = densmats[imode];

                  mat_t f_tilde, fp_tilde;
                  if (this->FullActiveBasis())
                  {
                     f_tilde   = mat_t(w*(half_trans_mf[0]*rho) + half_trans_mf[1]); // F_tilde  = W*(H_1*rho) + F_R    in FAB F_R is already in td basis
                     fp_tilde  = mat_t((rho*half_trans_mf[2])*u + half_trans_mf[3]); // F'_tilde = (rho*H'_1)*U + F'_R  in FAB F'_R is already in td basis
                  }
                  else
                  {
                     f_tilde   = mat_t(w*(half_trans_mf[0]*rho + half_trans_mf[1])); // F_tilde  = W*(H_1*rho + F_R)
                     fp_tilde  = mat_t((rho*half_trans_mf[2] + half_trans_mf[3])*u); // F'_tilde = (rho*H'_1 + F'_R)*U
                  }

                  ++imode;
                  return tdmf_t
                     {  std::move(f_tilde)
                     ,  std::move(fp_tilde)
                     };
               }
               );
            // Parse TD mean fields to g transformer
            this->mGTrf.ParseIntermediates(result.LambdaCoefs(), arDeriv.CoefsFactor(), std::move(td_mean_fields));
         }
         
         auto gmats = this->mGTrf.ConstraintMatrices(aTime, arParams.ClusterAmps(), arParams.LambdaCoefs(), densmats);

         // 4) Add contributions from g matrices (signs are taken care of inside arDeriv)
         Axpy(result, arDeriv.GContributions(aTime, gmats, arParams, this->mGTrf), param_t(1.0));
      }

      // 5) Zero the T_1 and L_1 derivatives if needed (and check that they are already zero if they should be)
      if (  this->ExplZeroOneModeAmpls()
         )
      {
         this->ZeroOneModeAmplDerivatives(result);
      }

      if (verbose) Mout << " DONE CALCULATING DERIVATIVE \n" << std::endl;

      return result;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   typename TRF_T::mat_t
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::TotalHMat
      (  step_t aTime
      )  const
   {
      using mat_t = typename TRF_T::mat_t;
      if constexpr   (  TRF_T::trf_type == TrfType::MATREP
                     )
      {
         mat_t h(this->NTdModals());
         for(Uin i = 0; i < this->mHamiltonian.size(); ++i)
         {
            Axpy(h, this->GetHamTrf(i).OperMat(), this->mHamiltonian.at(i).Coef(aTime));
         }
         return h;
      }
      else
      {
         MIDASERROR("TotalHMat should only be called for MATREP transformers.");
         return mat_t();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   typename Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::vec_t
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ShapedZeroVectorImpl
      (
      )  const
   {
      return vec_t(this->NTdModals(), this->NModals(), this->SizeParams(), false);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   bool Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ProcessNewVectorImpl
      (  vec_t& arY
      ,  const vec_t& aYOld
      ,  step_t& arT
      ,  step_t aTOld
      ,  vec_t& arDyDt
      ,  const vec_t& aDyDtOld
      ,  step_t& arStep
      ,  size_t& arNDerivFail
      )
   {
      using mat_t = GeneralMidasMatrix<param_t>;
      std::vector<In> NonBiorthogModes = std::vector<In>();
      bool biortho = arY.CheckBiorthonormality(NonBiorthogModes);
      
      bool accept = this->mFailIfNonBiOrtho ? !biortho : true;

      if (  !accept
         )
      {
         arY = aYOld;
         arT = aTOld;
         arDyDt = aDyDtOld;
         arStep /= step_t(2.0);
      }
      else if  (  accept
               && this->NonOrthoActiveSpaceProjectorType() == NonOrthoProjectorType::MOOREPENROSETYPEBYU
               )
      {
         std::vector<mat_t> projectors;
         for (In i_mode = I_0; i_mode < NTdModals().size(); i_mode++)
         {
            projectors.emplace_back(TRF_T::ActiveSpaceProjector(arY.ModalCont(), i_mode, this->NonOrthoActiveSpaceProjector(), this->NonOrthoActiveSpaceProjectorType()));
         }
         arY.UpdateModalMats(projectors);
      }

      return accept;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SaveAutoCorrImpl
      (  const vec_t& arVec
      )
   {
      LOGCALL("autocorr calc");

      if constexpr (TRF_T::trf_type == TrfType::VCC2H2)
      {
         auto [corrA, corrB] = AutoCorrTdmvcc2(arVec, this->InitStateForAutoCorr(), this->mrNTdModals, this->mrMcr, this->InitStateForAutoCorrType(), this->AutoCorrSett(), this->IoLevel());
         this->SaveAutoCorrTdmvcc(PropID::AUTOCORR, std::make_pair(ifc_prop_t(corrA), ifc_prop_t(corrB)));
      }
      else
      {
         auto [corrA, corrB] = AutoCorr(arVec, this->InitStateForAutoCorr(), this->mrNTdModals, this->mrMcr);
         this->SaveAutoCorrTdmvcc(PropID::AUTOCORR, std::make_pair(ifc_prop_t(corrA), ifc_prop_t(corrB)));
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SaveAutoCorrExtImpl
      (  const vec_t& arVec
      )
   {
      auto [corrA, corrB] = AutoCorrExt(arVec, this->InitState(), this->mrNTdModals, this->mrMcr);
      this->SaveAutoCorrTdmvcc(PropID::AUTOCORR_EXT, std::make_pair(ifc_prop_t(corrA), ifc_prop_t(corrB)));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SaveTdModalPlotsImpl
      (  const vec_t& aParams
      )  const
   {
      // Get basis set (BasDef)
      const auto basdef = this->GetBasDef();
      // We only use this to check if the potential has been scaled so we assume time-independent part of H is .at(0)
      const auto& oper = this->mHamiltonian.at(0).Oper();   

      // Setup directories for data
      std::string working_dir = gAnalysisDir + "/" + this->Name() + "_modal_plots" + "/";
      if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
      {
         MIDASERROR("Directory " + working_dir + " could not be created");
      }
      working_dir += std::to_string(this->GetNumberOfInterpolatedTimePoints()) + "/";
      if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
      {
         MIDASERROR("Directory " + working_dir + " could not be created");
      }
      size_t width = 25;

      const auto& n_td_modals = NTdModals();
      const auto& n_vscf_modals = this->NModals();
      const auto& vscf_modal_vectors = this->GetVscfModals();

      // Loop over each mode
      for (In i_mode = I_0; i_mode < n_td_modals.size(); ++i_mode)
      {
         const auto grid_points = this->GetGridPointsForPlot(i_mode);
         // Scaled grid for plotting together with the potential
         auto grid_points_scaled = GeneralMidasVector<Nb>(grid_points);
         if (oper.ScaledPot())
         {
            Nb scal_fac = oper.ScaleFactorForMode(i_mode);
            grid_points_scaled.Scale(scal_fac);
         }

         // Get vscf modals for specific mode
         const auto& vscf_modals_for_i_mode = vscf_modal_vectors[i_mode];

         // Get ModalMats for the transformations.
         auto& modal_mats = aParams.ModalMats(i_mode);
         auto& U = modal_mats.first;
         auto& W = modal_mats.second;

         // Setup file for each mode.
         std::string file_name = working_dir + "mode_" + std::to_string(i_mode);
         std::ofstream ofs;
         ofs.open(file_name);
         ofs << std::scientific << std::setprecision(16);

         std::string dens_file_name = working_dir + "density_mode_" + std::to_string(i_mode);
         std::ofstream ofs_dens;
         ofs_dens.open(dens_file_name);
         ofs_dens << std::scientific << std::setprecision(16);

         // Print header
         ofs << std::setw(width) << "Grid points";
         ofs_dens << std::setw(width) << "Grid points";
         std::vector<std::string> headers{"ket (Re)", "ket (Im)", "bra (Re)", "bra (Im)"};
         std::vector<std::string> dens_headers{"ket", "Bra",}; 
         for (In i_td_modal = I_0; i_td_modal < n_td_modals[i_mode]; ++i_td_modal)
         {
            for (auto& head : headers)
            {
               ofs << std::setw(width) << "Modal " + std::to_string(i_td_modal) + " " + head;
            }
            for (auto& head : dens_headers)
            {
               ofs_dens << std::setw(width) << "Modal " + std::to_string(i_td_modal) + " " + head;
            }
         }
         ofs << std::endl;
         ofs_dens << std::endl;


         // Loop over grid points
         for (In q_idx = I_0; q_idx < grid_points.Size(); ++q_idx)
         {
            const auto q = grid_points[q_idx];
            ofs << std::setw(width) << grid_points[q_idx];
            ofs_dens << std::setw(width) << grid_points[q_idx];

            // Loop over each modal
            for (In i_td_modal = I_0; i_td_modal < n_td_modals[i_mode]; ++i_td_modal)
            {
               param_t modal_ket(0);
               param_t modal_bra(0);
               for (In i_vscf_modal = I_0; i_vscf_modal < n_vscf_modals[i_mode]; ++i_vscf_modal)
               {
                  modal_ket += U[i_vscf_modal][i_td_modal] * vscf_modals_for_i_mode[q_idx][i_vscf_modal];
                  modal_bra += W[i_td_modal][i_vscf_modal] * vscf_modals_for_i_mode[q_idx][i_vscf_modal];
               }
               if constexpr (midas::type_traits::IsComplexV<param_t>)
               {
                  ofs << std::setw(width) << modal_ket.real();
                  ofs << std::setw(width) << modal_ket.imag();
                  ofs << std::setw(width) << modal_bra.real();
                  ofs << std::setw(width) << modal_bra.imag();
                  ofs_dens << std::setw(width) << (std::pow(modal_ket.real(), 2) + std::pow(modal_ket.imag(), 2));
                  ofs_dens << std::setw(width) << (std::pow(modal_bra.real(), 2) + std::pow(modal_bra.imag(), 2));
               }
               else 
               {
                  ofs << std::setw(width) << modal_ket;
                  ofs << std::setw(width) << Nb(0);
                  ofs << std::setw(width) << modal_bra;
                  ofs << std::setw(width) << Nb(0);
                  ofs_dens << std::setw(width) << std::pow(modal_ket, 2);
                  ofs_dens << std::setw(width) << std::pow(modal_bra, 2);
               }
            }
            ofs << std::endl;
            ofs_dens << std::endl;
         }

         ofs.close();
         ofs_dens.close();
      }

   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::CalculateOneModeDensities
      (  const vec_t&   aParams
      )
   {
      this->mOneModeDensities.CalcTdmvccOneModeDensities
         (  this->GetHamTrf(0).DensityMatrices(aParams.ClusterAmps(), aParams.LambdaCoefs())
         ,  aParams.ModalCont()
         );
      this->mOneModeDensities.CalcSsOneModeDensities();
      this->mOneModeDensities.CalcStsOneModeDensities();
      if (this->GetWriteOneModeDensities())
      {
         this->mOneModeDensities.WriteOneModeDensitiesForStep(this->GetNumberOfInterpolatedTimePoints());
      }
      if (this->GetNumberOfInterpolatedTimePoints() == this->GetUpdateOneModeDensitiesStep())
      {
         this->mOneModeDensities.WriteOneModeDensitiesToAdga();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   std::map<ParamID,libmda::util::any_type> Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ReadParamsFromFile
      (  const std::string& arRestartFilePath
      )
   {
      // Read phase, amplitudes and lambda coeffs
      auto m = Tdvcc<VEC_T,TRF_T,DERIV_T>::ReadParamsFromFile(arRestartFilePath);

      // Read modal matrices
      std::string file_name = arRestartFilePath + "ModalMats.dat";
      if (!midas::filesystem::IsFile(file_name))
      {
         MIDASERROR("The file " + file_name + " does not exist!");
      }
      std::istringstream params_file_stream = midas::mpi::FileToStringStream(file_name);
      std::string s = "";

      const auto& td_modals = this->NTdModals();
      const auto& prim_modals = this->NModals();
      const In nmodes = td_modals.size();
      using mat_t = GeneralMidasMatrix<param_t>;
      std::vector<std::pair<mat_t, mat_t> > modals(nmodes);
      for (In imode = I_0; imode < nmodes; ++imode)
      {
         auto& u = modals[imode].first;
         auto& w = modals[imode].second;
         u = mat_t(prim_modals[imode], In(td_modals[imode]), param_t(0.0));
         w = mat_t(td_modals[imode], In(prim_modals[imode]), param_t(0.0));

         // Skip "U^m (m = ..) =" 
         midas::input::GetLine(params_file_stream, s);
         ReadMatrixFromFile(u, params_file_stream, true);

         // Skip "W^m (m = ..) =" 
         midas::input::GetLine(params_file_stream, s);
         ReadMatrixFromFile(w, params_file_stream, true);
      }

      m[ParamID::MODALS] = std::move(modals);

      return m;
   }


   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::SetInitStateForAutoCorrImpl
      (  std::map<ParamID,libmda::util::any_type>&& arParams
      )
   {
      // MGH-NB: Should it be this->mrNTdModals at the end?
      this->mpInitStateForAutoCorr = std::make_unique<vec_t>(std::move(arParams), this->mrMcr, this->mrNModals);

      // We only need to do this check for the 'fast' TDMVCC[2] autocorrelation function
      if constexpr (TRF_T::trf_type == TrfType::VCC2H2)
      {
         bool zeroampls = true;
         bool idnmodals = true;
         const VEC_T& initstate = this->InitStateForAutoCorr();
         param_t one  = param_t(1);
         param_t zero = param_t(0);

         if (initstate.ClusterAmps().Norm() != C_0)
         {
            zeroampls = false;
         }
         if (initstate.LambdaCoefs().Norm() != C_0)
         {
            zeroampls = false;
         }

         const auto& ntd = initstate.NModalsTdBas();
         const auto& npr = initstate.NModalsPrimBas();
         using mat_t = GeneralMidasMatrix<param_t>;
         for (Uin imode = I_0; imode < initstate.NModes(); ++imode)
         {
            const auto& um = initstate.ModalMats(imode).first;
            const auto& wm = initstate.ModalMats(imode).second;
            mat_t id_um(npr[imode], ntd[imode], zero);
            mat_t id_wm(ntd[imode], npr[imode], zero);
            id_um.SetDiagToNb(one);
            id_wm.SetDiagToNb(one);

            step_t diff_um = DiffNorm2(id_um, um);
            step_t diff_wm = DiffNorm2(id_wm, wm);

            if (diff_um != C_0 || diff_wm != C_0)
            {
               idnmodals = false;
               break;
            }

         }

         // VSCF
         if (zeroampls && idnmodals)
         {
            if (this->IoLevel() > 6)
            {
               Mout << "I have detected a VSCF initial state for autocorrelation!" << std::endl;
            }
            this->mInitStateForAutoCorrType = InitType::VSCFREF;
         }
         // TDVCC
         else if (!zeroampls && idnmodals)
         {
            if (this->IoLevel() > 6)
            {
               Mout << "I have detected a TDVCC initial state for autocorrelation!" << std::endl;
            }
            this->mInitStateForAutoCorrType = InitType::TDVCC;
         }
         // TDMVCC
         else 
         {
            if (this->IoLevel() > 6)
            {
               Mout << "I have detected a TDMVCC initial state for autocorrelation!" << std::endl;
            }
            this->mInitStateForAutoCorrType = InitType::TDMVCC;
         }

      }
   }

   /************************************************************************//**
    * This function has been moved to Td(m)vcc classes so it has access to 
    * 'param_t' so ExtractFromMap does not get confused trying to get
    * GeneralMidasVector<complex<Nb>> from anytype<GeneralMidasVector<Nb>>
    * or vise versa.
    * Now unit modal_mats are just constructed in Tdmvcc version instead of
    * checking whether it is tdmvcc or not.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   std::map<ParamID,libmda::util::any_type> 
   Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::PrepInitState
      (  InitType aInit
      ,  CorrType aCorr
      ,  Uin aSizeParams
      ,  const std::vector<Uin>& aNPrimModals
      ,  const std::vector<Uin>& aNTdModals
      ,  const std::string& arVccBaseName
      ,  In aSizeInitState
      )
   {
      // Call Tdvcc version. i.e. construct ket and bra vectors (full of 0's)
      auto m = Tdvcc<VEC_T,TRF_T,DERIV_T>::PrepInitState(aInit, aCorr, aSizeParams, aNPrimModals, aNTdModals, arVccBaseName, aSizeInitState);

      if (  aNPrimModals.size() != aNTdModals.size()
            )
         {
            MIDASERROR("Sizes of primitive and time-dependent modals do not match!");
         }
         using mat_t = GeneralMidasMatrix<param_t>;
         std::vector<std::pair<mat_t, mat_t> > modals(aNPrimModals.size());
         for(Uin i = 0; i < aNPrimModals.size(); ++i)
         {
            In nprim = aNPrimModals[i];
            In ntd = aNTdModals[i];
            auto& u = modals[i].first;
            auto& w = modals[i].second;
            u = mat_t(nprim, ntd);
            u.Unit();
            w = mat_t(ntd, nprim);
            w.Unit();
         }
         m[ParamID::MODALS] = std::move(modals);

      return m;
   }

   /************************************************************************//**
    * Given input filestream read in matrix into argument matrix.
    * aBatch input determines whether to read in matrix in batches as defined by 
    * overloading of operator<< above.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   void Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>::ReadMatrixFromFile
      (  GeneralMidasMatrix<param_t>& arM
      ,  std::istringstream& arStringStream
      ,  bool aBatch
      )  const
   {
      const In arNrows = arM.Nrows();
      const In arNcols = arM.Ncols();
      std::string s;
      GeneralMidasVector<param_t> InputRow;
      if (!aBatch)
      {
         InputRow = GeneralMidasVector<param_t>(arNcols, param_t(0.0));
         for (In row_idx = I_0; row_idx < arNrows; ++row_idx)
         {
            midas::input::GetLine(arStringStream, s);
            InputRow = std::move(midas::util::GeneralMidasVectorFromString<param_t>(s));
            arM.AssignRow(InputRow, row_idx);
         }
      }
      else
      {
         In ncols = 0;
         In nprbat = 5;
         In nbat = (arNcols+nprbat-1)/nprbat;
         In nrest= arNcols;
         In jadd = 0;
         for (In ibat = I_0; ibat < nbat; ibat++) 
         {
            ncols = min(nrest,nprbat);
            for (In row_idx = I_0; row_idx < arNrows; row_idx++)
            {
               midas::input::GetLine(arStringStream, s);
               InputRow = std::move(midas::util::GeneralMidasVectorFromString<param_t>(s));
               for (In col_idx = jadd; col_idx < ncols+jadd; col_idx++) 
               {
                  arM[row_idx][col_idx] = InputRow[col_idx - jadd];
               }
            }
            midas::input::GetLine(arStringStream, s);
            nrest -= ncols;
            jadd += ncols;
         }
      }
   }

} /* namespace midas::tdvcc */

#endif/*TDMVCC_IMPL_H_INCLUDED*/
