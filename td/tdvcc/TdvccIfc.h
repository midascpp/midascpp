/**
 *******************************************************************************
 * 
 * @file    TdvccIfc.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCIFC_H_INCLUDED
#define TDVCCIFC_H_INCLUDED

// Standard headers.
#include <complex>
#include <map>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/anal/AutoCorrSettings.h"
#include "ode/OdeInfo.h"
#include "input/OdeInput.h"
#include "libmda/util/any_type.h"
#include "input/BasDef.h"
#include "td/OneModeDensities.h"

// Forward declares.
class OpDef;
class ModeCombiOpRange;
namespace midas::input
{
   class TdPropertyDef;
}
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;

namespace midas::tdvcc
{

   /************************************************************************//**
    * @brief
    *    Interface base class for running and storing results from a TDVCC
    *    calculation.
    ***************************************************************************/
   class TdvccIfc
   {
      public:
         using ifc_step_t = Nb;
         using ifc_absval_t = Nb;
         using ifc_param_t = std::complex<Nb>;
         using ifc_prop_t = std::complex<Nb>;
         using ifc_regularization_t = std::pair<RegInverseType, ifc_absval_t>;
         using ifc_autocorr_t = AutoCorrSettings;

         TdvccIfc(const TdvccIfc&) = default;
         TdvccIfc(TdvccIfc&&) = default;
         TdvccIfc& operator=(const TdvccIfc&) = default;
         TdvccIfc& operator=(TdvccIfc&&) = default;
         virtual ~TdvccIfc() = default;

         TdvccIfc();

         /******************************************************************//**
          * @name Get/set general settings
          *********************************************************************/
         //!@{
         const std::string& Name() const {return mName;}
         std::string& Name() {return mName;}

         const InitType InitStateType() const {return mInitStateType;} // MGH: Added 3/6/21
         InitType& InitStateType() {return mInitStateType;} // MGH: Added 3/6/21

         bool UseInitStateForAutoCorr() const {return mUseInitStateForAutoCorr;}
         bool& UseInitStateForAutoCorr() {return mUseInitStateForAutoCorr;}

         Uin IoLevel() const {return mIoLevel;}
         Uin& IoLevel() {return mIoLevel;}

         bool TimeIt() const {return mTimeIt;}
         bool& TimeIt() {return mTimeIt;}

         bool SaveFvciVecs() const {return mSaveFvciVecs;}
         bool& SaveFvciVecs() {return mSaveFvciVecs;}

         ifc_absval_t ImagTimeHaultThr() const {return mImagTimeHaultThr;}
         ifc_absval_t& ImagTimeHaultThr() {return mImagTimeHaultThr;}

         ifc_step_t PrintoutInterval() const {return mPrintoutInterval;}
         ifc_step_t& PrintoutInterval() {return mPrintoutInterval;}

         const std::string& V3cFilePrefix() const {return mV3cFilePrefix;}
         std::string& V3cFilePrefix() {return mV3cFilePrefix;}

         bool SaveTdModalPlots() const {return mSaveTdModalPlots;}
         bool& SaveTdModalPlots() {return mSaveTdModalPlots;}

         bool GetUpdateOneModeDensities() const {return mUpdateOneModeDensities;}
         bool& GetUpdateOneModeDensities() {return mUpdateOneModeDensities;}
         
         In GetUpdateOneModeDensitiesStep() const {return mUpdateOneModeDensitiesStep;}
         In& GetUpdateOneModeDensitiesStep() {return mUpdateOneModeDensitiesStep;}
         
         bool GetWriteOneModeDensities() const {return mWriteOneModeDensities;}
         bool& GetWriteOneModeDensities() {return mWriteOneModeDensities;}

         In SpaceSmoothening() const {return mSpaceSmoothening;}
         In& SpaceSmoothening() {return mSpaceSmoothening;}

         In TimeSmoothening() const {return mTimeSmoothening;}
         In& TimeSmoothening() {return mTimeSmoothening;}

         bool PrintParamsToFile() const {return mPrintParamsToFile;}
         bool& PrintParamsToFile() {return mPrintParamsToFile;}

         bool RestartFromFile() const {return mRestartFromFile;}
         bool& RestartFromFile() {return mRestartFromFile;}

         bool PseudoTDH() const {return mPseudoTDH;}
         bool& PseudoTDH() {return mPseudoTDH;}

         // Tdmvcc specific
         bool FullActiveBasis() const {return mFullActiveBasisTdmvcc;}
         bool& FullActiveBasis() {return mFullActiveBasisTdmvcc;}
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         // General functions (pure virtual to force implementation in derived classes)
         virtual Uin SizeParams() const = 0;
         virtual Uin SizeTotal() const = 0;
         virtual bool HasPhase() const = 0;
         virtual bool ImagTime() const = 0;
         virtual Uin NumHamiltonianContribs() const = 0;
         virtual const std::vector<Uin>& NModals() const = 0;
         virtual const std::vector<Uin>& NTdModals() const { static std::vector<Uin> v; return v; }
         virtual Uin MaxExci() const = 0;
         virtual Uin NElectronicStates() const = 0;
         virtual CorrType GetCorrType() const = 0;
         virtual TrfType GetTrfType() const = 0;
         
         // TDMVCC-related functions
         // MGH-NB: The non-const implementations allow for modification of (private) member variables 
         // if the object is not const. This is used by the function PostFactorySettings which is called
         // in TdvccDrv.cc. The const implementations are used for viewing the (private) member variables
         // when the object is const. The functions are not pure virtual since we dont't want to override
         // them in all derived classes. Here, a 'default value implementation' is given. 
         virtual ifc_autocorr_t& AutoCorrSett() { static auto t = midas::tdvcc::AutoCorrSettings(); return t; }
         virtual const ifc_autocorr_t& AutoCorrSett() const { static auto t = midas::tdvcc::AutoCorrSettings(); return t; }
         virtual ifc_regularization_t& InvDensRegularization() { static auto p = std::make_pair(RegInverseType::XSVD, static_cast<ifc_absval_t>(1.e-8)); return p; }
         virtual const ifc_regularization_t& InvDensRegularization() const { static auto p = std::make_pair(RegInverseType::XSVD, static_cast<ifc_absval_t>(1.e-8)); return p; }
         virtual ifc_regularization_t& GOptRegularization() { static auto p = std::make_pair(RegInverseType::XSVD, static_cast<ifc_absval_t>(1.e-8)); return p; }
         virtual const ifc_regularization_t& GOptRegularization() const { static auto p = std::make_pair(RegInverseType::XSVD, static_cast<ifc_absval_t>(1.e-8)); return p; }
         virtual bool& NonOrthoActiveSpaceProjector() { static bool b=true; return b; }
         virtual bool NonOrthoActiveSpaceProjector() const { static bool b=true; return b; }
         virtual bool& ExplZeroOneModeAmpls() { static bool b=true; return b; }
         virtual bool ExplZeroOneModeAmpls() const { static bool b=true; return b; }
         virtual const NonOrthoProjectorType NonOrthoActiveSpaceProjectorType() const { static NonOrthoProjectorType proj = NonOrthoProjectorType::ORIGINALBYUW; return proj; } // ABJ: Added 20/1/22
         virtual NonOrthoProjectorType& NonOrthoActiveSpaceProjectorType() { static NonOrthoProjectorType proj = NonOrthoProjectorType::ORIGINALBYUW; return proj; } // ABJ: Added 20/1/22

         // More general functions (not virtual since they are not intended to be overridden in derived classes)
         const midas::ode::OdeInfo& GetOdeInfo() const {return mOdeInfo;}
         void PrintSettings(std::ostream&, const Uin aIndent = 0) const;
         void Summary(std::ostream&, const Uin aIndent = 0) const;
         void PrintToFiles(std::ostream&, const std::string& arAbsPathDir, const Uin aIndent) const;
         void CalcAndWriteSpectra(std::ostream&, const std::string& arAbsPathDir, const Uin aIndent, const input::TdPropertyDef&) const;
         std::string GenerateFileName(PropID) const;
         std::string GenerateFileName(StatID) const;
         //!@}

         /******************************************************************//**
          * @name Set propagation details
          *********************************************************************/
         //!@{
         void SetInitState(std::map<ParamID,libmda::util::any_type> = {});
         void SetInitStateForAutoCorr(std::map<ParamID,libmda::util::any_type> = {});
         void SetVccGroundState(std::map<ParamID,libmda::util::any_type> = {});
         void SetOdeInfo(midas::ode::OdeInfo);
         virtual void EnableImagTime() = 0;
         void EnableTracking(PropID);
         void EnableTracking(StatID);
         bool Tracks(PropID) const;
         bool Tracks(StatID) const;

         //! _Must_ reserve space for operators to avoid re-allocations!
         void ExptValReserve(const Uin);

         //! Enable expt.val. operator; hard error if enabling more than ExpValReserve()'d.
         void EnableExptVal(const OpDef*, ModalIntegrals<Nb>&&);

         //! Initialize H transformers
         virtual void InitializeTransformers() = 0;

         //! Initialize the size of amplitude parameters
         virtual void InitializeSizeParams
            (  const std::vector<Uin>& arNModals
            )  = 0;

         //! Initialize OneModeDensities member
         virtual void InitOneModeDensities
            (  const std::vector<std::vector<GeneralMidasVector<Nb>>>& aVscfModals
            ) = 0; // Pure virtual
         
         //! To be used with restart option
         virtual std::map<ParamID,libmda::util::any_type> ReadParamsFromFile(const std::string& aRestartFilePath) = 0;

         // Used for Initstate::vscfref or Initstate::vccgs
         virtual std::map<ParamID,libmda::util::any_type> PrepInitState(InitType, CorrType, Uin, const std::vector<Uin>&, const std::vector<Uin>&, const std::string&, In = -1) = 0;
         //!@}

         /******************************************************************//**
          * @name Driver calls
          *********************************************************************/
         //!@{
         void Evolve();
         //!@}

         /******************************************************************//**
          * @name Post-calc. extractions
          *********************************************************************/
         //!@{
         std::vector<ifc_step_t> ExtractInterpolTs() &&;
         std::vector<GeneralMidasVector<ifc_param_t>> ExtractFvciVecsKet() &&;
         std::vector<GeneralMidasVector<ifc_param_t>> ExtractFvciVecsBra() &&;
         //!@}

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{
         template<typename T>
         static void PrintTableToFile
            (  const std::string& arAbsPathFile
            ,  const std::vector<std::string>& arHeaders
            ,  const std::vector<ifc_step_t>& arTs
            ,  const std::vector<const std::vector<T>*>& arVecVals
            );

         virtual void PrintOutPotential() const = 0;
         void SetGridPointsForPlot(const In, const Nb aGridScaling=C_2);
         const GeneralMidasVector<Nb>& GetGridPointsForPlot(const In) const;
         const std::vector<GeneralMidasVector<Nb>>& GetGridPoints() const;         
         //!@}

         /******************************************************************//**
          * @name Stuff for printing potential and time-dependent modals
          *********************************************************************/
         //!@{
         void SetBasDef(const BasDef& arBasDef) {mBasDef = BasDef(arBasDef);}
         const BasDef& GetBasDef() const {return mBasDef;}

         void SetVscfModalCoefs(const std::vector<GeneralMidasMatrix<Nb>>& arVscfModalCoefs) {mVscfModalCoefs = std::move(arVscfModalCoefs);}
         const std::vector<GeneralMidasMatrix<Nb>>& GetVscfModalCoefs() const {return mVscfModalCoefs;}
         const GeneralMidasMatrix<Nb>& GetVscfModalCoefMat(const In i_mode) const {return mVscfModalCoefs[i_mode];}

         void SetVscfModals(const std::vector<Uin>& arNModals);
         const std::vector<std::vector<GeneralMidasVector<Nb>>>& GetVscfModals() const {return mVscfModals;}
         //!@}


      protected:
         const std::set<PropID>& TrackedProps() const;
         const std::set<PropID>& TrackedAutoCorr() const;
         const std::set<PropID>& TrackedStatePopulations() const;
         const std::set<StatID>& TrackedStats() const;
         const std::set<std::string>& TrackedExptVals() const;

         void SaveAcceptedTime(ifc_step_t);
         void SaveInterpolatedTime(ifc_step_t);
         In GetNumberOfInterpolatedTimePoints() const {return mInterpolTs.size();}

         void SaveEnergyAndContribs(const std::vector<std::tuple<ifc_prop_t,ifc_param_t,ifc_param_t>>&);
         void SavePhase(ifc_param_t);
         void SaveStat(StatID, ifc_absval_t, const std::map<ParamID,ifc_absval_t>&);
         void SaveExpVals(const std::map<std::string,ifc_prop_t>&);
         void SaveAutoCorr(PropID, const std::tuple<ifc_prop_t,ifc_prop_t,ifc_prop_t>&);
         void SaveAutoCorrTdmvcc(PropID, const std::pair<ifc_prop_t,ifc_prop_t>&);
         void SaveStatePopulations(const std::vector<ifc_absval_t>&);
         void SaveFvciVec(GeneralMidasVector<ifc_param_t>&&,GeneralMidasVector<ifc_param_t>&&);

         ifc_absval_t LastDerivNorm() const {return mLastDerivNorm;}
         ifc_absval_t& LastDerivNorm() {return mLastDerivNorm;}
         bool CheckLastDerivNorm() const {return ImagTime() && ImagTimeHaultThr() > 0;}

         void PrintEvolveStatus() const;
         virtual void PassSettingsToTrfs() = 0;

      private:
         std::string mName = "default_tdvcc_name";
         InitType mInitStateType = InitType::VSCFREF;
         bool mUseInitStateForAutoCorr = false;
         Uin mIoLevel = 0;
         bool mTimeIt = false;
         bool mSaveFvciVecs = false;
         ifc_absval_t mImagTimeHaultThr = 0.0;
         ifc_absval_t mLastDerivNorm = std::numeric_limits<ifc_absval_t>::max();
         mutable ifc_step_t mNextPrintoutAtTime = 0.0;
         ifc_step_t mPrintoutInterval = 0.0;
         std::string mV3cFilePrefix = "";
         bool mPrintParamsToFile = false;
         bool mRestartFromFile = false;
         bool mSaveTdModalPlots=false;
         midas::td::OneModeDensities<ifc_param_t> mOneModeDensities;
         bool mUpdateOneModeDensities = false;
         In mUpdateOneModeDensitiesStep = I_0;
         bool mWriteOneModeDensities = false;
         In mSpaceSmoothening = I_0;
         In mTimeSmoothening  = I_0;
         // Needed for printing Td-modals, only set to something if mSaveTdModalPlots is true.
         BasDef mBasDef;
         bool mPseudoTDH = false;

         //! Wether we do full active basis tdmvcc or tdmvcc with a secondary space.
         bool mFullActiveBasisTdmvcc = false;

         midas::ode::OdeInfo mOdeInfo;

         //@{
         //! Times for saving; actual steps and interpolated times.
         std::vector<ifc_step_t> mStepTs;
         std::vector<ifc_step_t> mInterpolTs;
         //@}

         //@{
         //! Properties for tracking.
         std::set<PropID> mTrackedProps;
         std::map<PropID,std::vector<ifc_prop_t>> mProps;
         //@}

         //@{
         //! Hamiltonian components. (coef, deriv, exp.val. contrib.) contribs<times<vals>>
         std::vector<std::vector<ifc_prop_t>> mHamContribExpVal;
         std::vector<std::vector<ifc_param_t>> mHamContribCoef;
         std::vector<std::vector<ifc_param_t>> mHamContribDeriv;
         //@}

         //@{
         //! Statistics (norms^2) for tracking. Totals and ParamID contributions.
         std::set<StatID> mTrackedStats;
         std::map<StatID,std::vector<ifc_absval_t>> mStats;
         std::map<StatID,std::map<ParamID,std::vector<ifc_absval_t>>> mStatsContribs;
         //@}

         //@{
         std::set<std::string> mTrackedExptVals;
         std::map<std::string,std::vector<ifc_prop_t>> mExptVals;
         //@}

         //@{
         //! FVCI vectors at interpolated times, if so requested.
         std::vector<GeneralMidasVector<ifc_param_t>> mFvciVecsKet;
         std::vector<GeneralMidasVector<ifc_param_t>> mFvciVecsBra;
         //@}
         
         //@{
         //! Electronic state populations
         std::set<PropID> mTrackedStatePopulations;
         std::vector<std::vector<ifc_absval_t>> mElectronicStatePopulations;
         //@}

         //!
         std::vector<GeneralMidasMatrix<Nb>> mVscfModalCoefs;
         // index order: [mode][grid_point][vscf_modal].
         std::vector<std::vector<GeneralMidasVector<Nb>>> mVscfModals;
         //! Grid points vector for each mode, used for visualization.
         std::vector<GeneralMidasVector<Nb>> mGridPointsForPlot;


         /******************************************************************//**
          * @name Auto-correlation functions
          *********************************************************************/
         //!@{
         std::set<PropID> mTrackedAutoCorr;

         //! The \f$ \langle \Lambda(0) \vert \Psi(t) \rangle \f$ (`<L(0)|VCC(t)>`).
         std::vector<ifc_prop_t> mAutoCorrA;

         //! The \f$ \langle \Lambda(t) \vert \Psi(0) \rangle^* \f$ (`<L(t)|VCC(0)>^*`).
         std::vector<ifc_prop_t> mAutoCorrB;

         //! The average of the two above.
         std::vector<ifc_prop_t> mAutoCorr;

         //! AC at double time using the t/2 trick (S(2t) = <L^*(t)|VCC(t)>)
         std::vector<ifc_prop_t> mHalfTimeAutoCorr;
         //!@}

         /******************************************************************//**
          * @name "Extended" auto-correlation functions
          *
          * Same as mAutoCorrA, etc., _but_ with the bra changed as follows:
          *    -  `<Phi|(1+L)exp(-T)      --> <Phi|exp(L)exp(-T)`    for VCC
          *    -  `<Phi|exp(Sigma)exp(-T) --> <Phi|(1+Sigma)exp(-T)` for ExtVCC
          *    -  no changes for VCI
          *********************************************************************/
         //!@{
         std::vector<ifc_prop_t> mAutoCorrExtA;
         std::vector<ifc_prop_t> mAutoCorrExtB;
         std::vector<ifc_prop_t> mAutoCorrExt;
         std::vector<ifc_prop_t> mHalfTimeAutoCorrExt;
         //!@}

         virtual void SanityCheckState(const std::map<ParamID,libmda::util::any_type>&) const = 0;
         virtual void SetInitStateImpl(std::map<ParamID,libmda::util::any_type>&&) = 0;
         virtual void SetInitStateForAutoCorrImpl(std::map<ParamID,libmda::util::any_type>&&) = 0;
         virtual void SetVccGroundStateImpl(std::map<ParamID,libmda::util::any_type>&&) = 0;
         virtual void ExptValReserveImpl(const Uin) = 0;
         virtual void EnableExptValImpl(const OpDef*, ModalIntegrals<Nb>&&) = 0;
         static midas::ode::OdeInfo DefaultOdeInfo();

         virtual void EvolveImpl() = 0;
         virtual const std::set<ParamID>& GetParamIDs() const = 0;

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{
         static bool AllValuesReal(const std::vector<ifc_prop_t>&);

         static std::array<ifc_absval_t,5> DistAnal(const std::vector<ifc_absval_t>&, const std::vector<TdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalReal(const std::vector<ifc_prop_t>&, const std::vector<TdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalImag(const std::vector<ifc_prop_t>&, const std::vector<TdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalAbs(const std::vector<ifc_prop_t>&, const std::vector<TdvccIfc::ifc_step_t>& arT);

         template<typename T>
         static void PrintEqualityLine
            (  std::ostream& arOs
            ,  const std::string& arLeft
            ,  const T& arRight
            ,  const Uin aIndent
            ,  const Uin aWLeft
            ,  const Uin aWRight
            );

         template<typename T>
         static void SummaryOutputUtil
            (  std::ostream& arOs
            ,  const std::vector<T>& arVec
            ,  const std::vector<ifc_step_t>& arTs
            ,  const std::string& arPrefix
            ,  const Uin aIndent
            ,  const Uin aWLeft
            ,  const Uin aWRight
            );

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepEnergyForWriting() const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepPropForWriting(PropID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_absval_t>*>>
         PrepStatForWriting(StatID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepAutoCorrForWriting(PropID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_absval_t>*>>
         PrepStatePopulationsForWriting() const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepExptValForWriting(const std::string&) const;

         bool PrintPropToFile
            (  const std::string& arAbsPathFile
            ,  PropID aID
            )  const;

         bool PrintStatToFile
            (  const std::string& arAbsPathFile
            ,  StatID aID
            )  const;

         bool PrintAutoCorrToFile
            (  const std::string& arAbsPathFile
            ,  PropID aID
            )  const;

         bool PrintStatePopulationsToFile
            (  const std::string& arAbsPathFile
            ,  PropID aID
            )  const;

         bool PrintExptValToFile
            (  const std::string& arAbsPathFile
            ,  const std::string& arName
            )  const;

         //!@}
   };

} /* namespace midas::tdvcc */

#include "td/tdvcc/TdvccIfc_Impl.h"

#endif /* TDVCCIFC_H_INCLUDED */
