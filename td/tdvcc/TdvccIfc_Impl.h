/**
 *******************************************************************************
 * 
 * @file    TdvccIfc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCIFC_IMPL_H_INCLUDED
#define TDVCCIFC_IMPL_H_INCLUDED

#include "util/type_traits/Complex.h"
#include "mpi/Impi.h"

namespace midas::tdvcc
{
   /****************************************************************************
    *
    ***************************************************************************/
   template<typename T>
   void TdvccIfc::PrintTableToFile
      (  const std::string& arAbsPathFile
      ,  const std::vector<std::string>& arHeaders
      ,  const std::vector<ifc_step_t>& arTs
      ,  const std::vector<const std::vector<T>*>& arVecVals
      )
   {
      const Uin prec = 16;
      const Uin width = 25;
      constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
      const Uin cols = 1 + arVecVals.size() * (is_complex? 2: 1);
      const Uin rows = arTs.size();

      if (arHeaders.size() != cols)
      {
         MIDASERROR("arHeaders.size() (which is "+std::to_string(arHeaders.size())+") != cols (which is "+std::to_string(cols)+").");
      }

      for(Uin j = 0; j < arVecVals.size(); ++j)
      {
         if (arVecVals.at(j) == nullptr)
         {
            MIDASERROR("arVecVals.at("+std::to_string(j)+") is nullptr.");
         }
         if (arVecVals.at(j)->size() != rows)
         {
            MIDASERROR("arVecVals.at("+std::to_string(j)+")->size() (which is "+std::to_string(arVecVals.at(j)->size())+") != rows (which is "+std::to_string(rows)+").");
         }
      }

      midas::mpi::OFileStream ofs(arAbsPathFile);
      if (!ofs)
      {
         MIDASERROR("Something went wrong when opening file '"+arAbsPathFile+"'.");
      }

      ofs << std::scientific << std::setprecision(prec);

      // Headers.
      ofs << std::left;
      ofs << "#";
      for(const auto& s: arHeaders)
      {
         ofs << ' ' << std::setw(width) << s;
      }
      ofs << '\n';

      // Contents.
      for(Uin i = 0; i < rows; ++i)
      {
         ofs << std::right;
         ofs << ' ' << std::setw(width) << arTs.at(i);
         for(Uin j = 0; j < arVecVals.size(); ++j)
         {
            if constexpr(is_complex)
            {
               ofs   << ' ' << std::setw(width) << std::real(arVecVals.at(j)->at(i))
                     << ' ' << std::setw(width) << std::imag(arVecVals.at(j)->at(i))
                     ;
            }
            else
            {
               ofs   << ' ' << std::setw(width) << arVecVals.at(j)->at(i);
            }
         }
         ofs << '\n';
      }
   }

} /* namespace midas::tdvcc */

#endif/*TDVCCIFC_IMPL_H_INCLUDED*/
