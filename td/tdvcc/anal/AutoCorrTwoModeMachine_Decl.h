/**
************************************************************************
* 
* @file    AutoCorrTwoModeMachine_Decl.h
* @date    10-04-2021
* @author  Andreas Buchgraitz Jensen
* @author  Mads Greisen Højlund
*
* @brief   Class for calculating TDMVCC[2] autocorrelation function.
*
* @copyright
*    Ove Christiansen, Aarhus University.
*    The code may only be used and/or copied with the written permission of
*    the author or in accordance with the terms and conditions under which the
*    program was supplied.  The code is provided "as is" without any expressed
*    or implied warranty.
* 
************************************************************************
**/

#ifndef AUTOCORRTWOMODEMACHINE_DECL_H_INCLUDED
#define AUTOCORRTWOMODEMACHINE_DECL_H_INCLUDED

// Standard headers
#include <list>
#include <vector>
#include <utility>
#include <iostream>

// Midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/TdvccIfc.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"

// Forward declarations
//class ModeCombiOpRange;
//class ModeCombi;
//class OpDef;

namespace midas::tdvcc
{
   /***************************************************************************//**
    * Calculates and store intermediates for TDMVCC[2] autocorrelation function.
    *  
    * Things to consider: 
    *    – Examples: nielskm on grendel
    *    – Loop over mc's inside CalcY00Recursive: Iterator-based loop or improved logic?
    *    – Collect loops over all mode pairs -> fewer loops and reuse of e.g. amplitude mats
    *    – Remove pairs from arMcrDoubls that are not needed?
    *    – Pre-calculate and store all Y00(M\...)? Alternatively, some reuse might
    *      be possible without storing everyting. 
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class AutoCorrTwoModeMachine
   {
   public:
      using param_t = PARAM_T;
      using real_t = midas::type_traits::RealTypeT<param_t>;
      using vec_t = GeneralMidasVector<param_t>;
      using mat_t = GeneralMidasMatrix<param_t>;
      using vec_mat_t = GeneralMidasVector<mat_t>;
      using mat_vec_t = GeneralMidasMatrix<vec_t>;
      using state_t = ParamsTdmvcc<PARAM_T, CONT_TMPL>;
      using const_iterator = std::vector<ModeCombi>::const_iterator;
      using settings_t = typename TdvccIfc::ifc_autocorr_t;

   private:
      //@{
      //! Bra state, ket state state and state info
      const state_t& mrBraState;
      const state_t& mrKetState;
      const StateType mBraType;
      const StateType mKetType;
      const std::vector<Uin>& mrNModals;
      const ModeCombiOpRange& mrMcr;
      const ModeCombiOpRange mMcrDoubles;
      const Uin mNModes;
      const ModeCombi mMcAllModes;
      //@}

      //@{
      //! Autocorrelation settings
      const AutoCorrAlgo mAutoCorrAlgo;
      const In mMaxLevel;
      const real_t mThreshold;
      const real_t mDeltaThres;
      const In mNumThres;
      //@}

      //! Print level
      const bool mVerbose;
      
      //! Modal overlap matrices
      vec_mat_t mOverlapMatrices;

      //! Passive overlaps 
      vec_t mPassiveOverlaps;

      //! Fully down-contracted T2 operators (one for each mode pair)
      mat_t mT2Down;

      //! Partially down-contracted T2 operators (two for each mode pair)
      mat_vec_t mT2PartialDown;

      //! Autocorrelation function (without phase factor)
      param_t mAutoCorr;

      //@{
      //! Statistics
      param_t mGeoMeanPassiveOverlaps;
      param_t mAriMeanPassiveOverlaps;
      param_t mGeoMeanT2Down;
      param_t mAriMeanT2Down;
      //@}

   public:
      /*****************************************************************//**
      * @name Constructors etc.
      *********************************************************************/
      //!@{
      //! Constructor from data
      AutoCorrTwoModeMachine
         (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arBraState
         ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arKetState
         ,  const StateType aBraType
         ,  const StateType aKetType
         ,  const std::vector<Uin>& arNModals
         ,  const ModeCombiOpRange& arMcr
         ,  const settings_t& arSettings
         ,  Uin aIoLevel
         );

      //! Default destructor
      ~AutoCorrTwoModeMachine() = default;

      //! Disable copy constructor
      AutoCorrTwoModeMachine(const AutoCorrTwoModeMachine&) = delete;

      //! Enable move constructor
      AutoCorrTwoModeMachine(AutoCorrTwoModeMachine&&) = default;

      //! Disable copy assignment
      AutoCorrTwoModeMachine& operator=(const AutoCorrTwoModeMachine&) = delete;

      //! Enable move assignment
      AutoCorrTwoModeMachine& operator=(AutoCorrTwoModeMachine&&) = default;
      //!@}

      /*****************************************************************//**
      * @name Queries
      *********************************************************************/
      //!@{
      const ModeCombiOpRange& McrDoubles() const {return mMcrDoubles;}
      const ModeCombi& McAllModes() const {return mMcAllModes;}
      //!@}


      /*****************************************************************//**
      * @name Data extraction
      *********************************************************************/
      //!@{
      param_t GetAutoCorr() const {return mAutoCorr;}
      //!@}

   private:
      /*****************************************************************//**
      * @name Functions for calculating various intermediates
      *********************************************************************/
      //!@{
      void InitializeModalOverlaps();
      void InitializeT2Down();
      void CalcOmega00();
      void CalcOmega120();
      void CalcOmega121And122();
      void CalcOmega121();
      void CalcOmega122();
      void CalcOmega1212();

      void CalcY00RecursiveDummy
         (  ModeCombi aMc
         ,  const In aI
         ,  const Uin aLevel
         ,  Uin& aNumCalls
         );

      void CalcY00RecursiveTrunc
         (  const ModeCombi aMc
         ,  const In aI
         ,  const param_t aX
         ,  param_t& arY
         ,  const Uin aLevel
         ,  Uin& aNumCalls
         ,  std::vector<Uin>& arNumTerms
         ,  std::vector<param_t>& arSumTerms
         );

      void CalcY00RecursiveScreen
         (  const ModeCombi aMc
         ,  const In aI
         ,  const param_t aX
         ,  param_t& arY
         ,  const Uin aLevel
         ,  Uin& aNumCalls
         ,  std::vector<Uin>& arNumTerms
         ,  std::vector<param_t>& arSumTerms
         ,  const real_t& arThreshold
         ,  const real_t& arLogThreshold
         ,  const real_t& arLogDelta
         ,  const Uin& arNumBins
         ,  const In aOldBin
         );

      param_t WrapCalcY00Recursive
         (  const ModeCombi& aMc
         ,  const ModeCombiOpRange& arMcrDoubles
         );
      //!@}

      /*****************************************************************//**
      * @name Utility functions
      *********************************************************************/
      //!@{
      param_t GetPassiveOverlapProd
         (  const ModeCombi& arMc
         )  const;

      param_t GetT2Down
         (  const ModeCombi& arMc
         )  const;

      const vec_t& GetT2PartialDown
         (  const Uin aM0
         ,  const Uin aM1
         )  const;

      const mat_t& GetOverlapMatrix
         (  const Uin aM
         )  const;

      mat_t GetAmplMat
         (  const Uin aNrows
         ,  const Uin aNcols
         ,  const CONT_TMPL<PARAM_T>& arAmpl
         ,  const ModeCombi& arMc
         ,  const ModeCombiOpRange& arMcr
         );

      ModeCombiOpRange ExtractDoubles
         (  const ModeCombiOpRange& arMcr
         );

      param_t SumProdElemMat
         (  const mat_t& arMat1
         ,  const mat_t& arMat2
         );

      bool ValidModePair
         (  const ModeCombi& arMc
         )  const;

      template<typename T>
      void PrintVector
         (  std::ostream& arOut
         ,  const std::vector<T>& arVector
         ,  bool aPrintAbs
         )  const;

      long double FullNumTerms
         (  const Uin aNModes
         )  const;

   };  
} /* namespace midas::tdvcc */

#endif /* AUTOCORRTWOMODEMACHINE_DECL_H_INCLUDED */
