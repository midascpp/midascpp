/**
************************************************************************
* 
* @file                 AutoCorrSettings.h
* 
* Created:              24-02-2022
* 
* Author:               Mads Greisen Højlund (madsgh@chem.au.dk)
* 
* Short Description:    Settings for TDMVCC[2] autocorrelation function
* 
* Last modified:        24-02-2022 (Mads Greisen Højlund)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef AUTOCORRSETTINGS_H_INCLUDED
#define AUTOCORRSETTINGS_H_INCLUDED

#include "td/tdvcc/TdvccEnums.h"

namespace midas::tdvcc
{

/**
 * Struct for holding TDMVCC autocorrelation settings
 **/
struct AutoCorrSettings
{
    public:
        // Algorithm for calculating the autocorrelation function (FULL, TRUNCATE, SCREEN)
        AutoCorrAlgo mAlgorithm = AutoCorrAlgo::FULL;

        // Maximum level included in recursive sum when using truncated algorithm
        In mMaxLevel    = -I_1;

        // Relative screening threshold for screening algorithm
        Nb mScreenThres = C_I_10_10;

        // Mulitplicative distance between thresholds
        Nb mDeltaThres  = C_10;

        // Number of thresholds
        In mNumThres    = I_5;

        // Calculate type A autocorrelation function (<0|t>)?
        bool mCalcTypeA = true;

        // Calculate type B autocorrelation function (<t|0>*)?
        bool mCalcTypeB = false;
};


} /* namespace midas::tdvcc */

#endif /* AUTOCORRSETTINGS_H_INCLUDED */