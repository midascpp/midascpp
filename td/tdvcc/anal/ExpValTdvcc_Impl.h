/**
 *******************************************************************************
 * 
 * @file    ExpValTdvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXPVALTDVCC_IMPL_H_INCLUDED
#define EXPVALTDVCC_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"
#include "vcc/TensorDataCont.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      )
   {
      using cont_t = CONT_TMPL<PARAM_T>;
      const cont_t v = arTrf.ExpValTrf(aTime, arState.ClusterAmps());
      PARAM_T result = 0;
      if constexpr(std::is_same_v<cont_t, GeneralDataCont<PARAM_T>>)
      {
         v.DataIo(IO_GET, 0, result);
      }
      else if constexpr (  std::is_same_v<cont_t, GeneralTensorDataCont<PARAM_T>>
                        )
      {
         result = v.GetModeCombiData(I_0).GetScalar();
      }
      else
      {
         result = v[0];
      }

      if constexpr   (  std::is_same_v<cont_t, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         MidasWarning("Converting TensorDataCont to DataCont for calculation of TIM-TDVCC expectation value.");
         result += SumProdElems(DataContFromTensorDataCont(arState.LambdaCoefs()), DataContFromTensorDataCont(v), 1, arState.SizeParams());
      }
      else
      {
         result += SumProdElems(arState.LambdaCoefs(), v, 1, arState.SizeParams());
      }

      return result;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      )
   {
      return Dot(arState.Coefs(), arTrf.ExpValTrf(aTime, arState.Coefs()))/Norm2(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      )
   {
      // The ref element of this vector is the expectation value.
      auto v = arTrf.HamDerExtAmp(aTime, arState.ClusterAmps(), arState.ExtAmps());
      // Just for returning 0'th element, works uniformly for MidasVector/DataCont.
      return ZeroFirstElemAndReturnIt(v);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  TRF_T& arTrf
      )
   {
      using cont_t = CONT_TMPL<PARAM_T>;

      // Update time-dependent integrals
      arTrf.UpdateIntegrals(arState.ModalCont());

      PARAM_T result = 0;
      const cont_t v = arTrf.ExpValTrf(aTime, arState.ClusterAmps());
      if constexpr(std::is_same_v<cont_t, GeneralDataCont<PARAM_T>>)
      {
         v.DataIo(IO_GET, 0, result);
      }
      else
      {
         result = v[0];
      }

      result += SumProdElems(arState.LambdaCoefs(), v, 1, arState.SizeParams());

      return result;
   }


} /* namespace midas::tdvcc */

#endif/*EXPVALTDVCC_IMPL_H_INCLUDED*/
