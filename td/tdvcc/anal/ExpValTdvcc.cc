/**
 *******************************************************************************
 * 
 * @file    ExpValTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/anal/ExpValTdvcc_Impl.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"

#include <complex>

// Define instatiation macro.
#define INSTANTIATE_EXPVAL(STEP_T,PARAM_T, CONT_TMPL, TRF_TMPL)      \
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>&                   \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTdvci<PARAM_T, CONT_TMPL>&                   \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \

#define INSTANTIATE_EXPVAL_EVCC(STEP_T,PARAM_T, CONT_TMPL, TRF_TMPL)\
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>&                \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \

#define INSTANTIATE_EXPVAL_TDMVCC(STEP_T,PARAM_T, CONT_TMPL, TRF_TMPL)\
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>&                  \
         ,  TRF_TMPL<PARAM_T>&                                 \
         );                                                          \

// Instantiations.
namespace midas::tdvcc
{
// MatRep versions
INSTANTIATE_EXPVAL(Nb,Nb,GeneralMidasVector,TrfTdvccMatRep);
INSTANTIATE_EXPVAL(Nb,Nb,GeneralMidasVector,TrfTdvciMatRep);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralMidasVector,TrfTdvccMatRep);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralMidasVector,TrfTdvciMatRep);

// VCC[2]/H2 versions
INSTANTIATE_EXPVAL(Nb,Nb,GeneralDataCont,TrfTdvcc2);
INSTANTIATE_EXPVAL(Nb,Nb,GeneralDataCont,TrfTdvci2);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralDataCont,TrfTdvcc2);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralDataCont,TrfTdvci2);

// General TIM-TDVCC versions
INSTANTIATE_EXPVAL(Nb,Nb,GeneralTensorDataCont,TrfGeneralTimTdvcc);
INSTANTIATE_EXPVAL(Nb,Nb,GeneralTensorDataCont,TrfGeneralTimTdvci);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralTensorDataCont,TrfGeneralTimTdvcc);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralTensorDataCont,TrfGeneralTimTdvci);

// TDEVCC
INSTANTIATE_EXPVAL_EVCC(Nb,Nb,GeneralMidasVector,TrfTdextvccMatRep);
INSTANTIATE_EXPVAL_EVCC(Nb,std::complex<Nb>,GeneralMidasVector,TrfTdextvccMatRep);

// TDMVCC
INSTANTIATE_EXPVAL_TDMVCC(Nb,Nb,GeneralMidasVector,TrfTdmvccMatRep);
INSTANTIATE_EXPVAL_TDMVCC(Nb,std::complex<Nb>,GeneralMidasVector,TrfTdmvccMatRep);
INSTANTIATE_EXPVAL_TDMVCC(Nb,Nb,GeneralDataCont,TrfTdmvcc2);
INSTANTIATE_EXPVAL_TDMVCC(Nb,std::complex<Nb>,GeneralDataCont,TrfTdmvcc2);
}

#undef INSTANTIATE_EXPVAL
#undef INSTANTIATE_EXPVAL_EVCC
#undef INSTANTIATE_EXPVAL_TDMVCC

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
