/**
 *******************************************************************************
 * 
 * @file    StatePopulations.h
 * @date    25-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_STATEPOPULATIONS_H_INCLUDED
#define TDVCC_STATEPOPULATIONS_H_INCLUDED

#include "td/tdvcc/anal/StatePopulations_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/anal/StatePopulations_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*TDVCC_STATEPOPULATIONS_H_INCLUDED*/
