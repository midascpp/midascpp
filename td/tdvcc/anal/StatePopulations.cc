/**
 *******************************************************************************
 * 
 * @file    StatePopulations.cc
 * @date    25-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/StatePopulations.h"
#include "td/tdvcc/anal/StatePopulations_Impl.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"

#include <complex>

// Define instatiation macro.
#define INSTANTIATE_STATEPOPULATIONS(PARAM_T, CONT_TMPL, TRF_TMPL, PARAMS_TMPL)                                    \
template std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations<TRF_TMPL<PARAM_T>>     \
   (  const PARAMS_TMPL<PARAM_T, CONT_TMPL>&                                                       \
   ,  const std::vector<Uin>&                                                                         \
   ,  const ModeCombiOpRange&                                                                         \
   ,  In                                                                                              \
   );                                                                                                 \

// Instantiations.
namespace midas::tdvcc
{
// MatRep versions
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralMidasVector,TrfTdvccMatRep,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralMidasVector,TrfTdvciMatRep,ParamsTdvci);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralMidasVector,TrfTdvccMatRep,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralMidasVector,TrfTdvciMatRep,ParamsTdvci);

INSTANTIATE_STATEPOPULATIONS(Nb,GeneralMidasVector,TrfTdextvccMatRep,ParamsTdextvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralMidasVector,TrfTdextvccMatRep,ParamsTdextvcc);

INSTANTIATE_STATEPOPULATIONS(Nb,GeneralMidasVector,TrfTdmvccMatRep,ParamsTdmvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralMidasVector,TrfTdmvccMatRep,ParamsTdmvcc);

// VCC[2]/H2 versions
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralDataCont,TrfTdvcc2,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralDataCont,TrfTdvci2,ParamsTdvci);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralDataCont,TrfTdvcc2,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralDataCont,TrfTdvci2,ParamsTdvci);

INSTANTIATE_STATEPOPULATIONS(Nb, GeneralDataCont, TrfTdmvcc2, ParamsTdmvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>, GeneralDataCont, TrfTdmvcc2, ParamsTdmvcc);

// General TIM-TDVCC versions
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralTensorDataCont,TrfGeneralTimTdvcc,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(Nb,GeneralTensorDataCont,TrfGeneralTimTdvci,ParamsTdvci);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralTensorDataCont,TrfGeneralTimTdvcc,ParamsTdvcc);
INSTANTIATE_STATEPOPULATIONS(std::complex<Nb>,GeneralTensorDataCont,TrfGeneralTimTdvci,ParamsTdvci);
}

#undef INSTANTIATE_STATEPOPULATIONS
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
