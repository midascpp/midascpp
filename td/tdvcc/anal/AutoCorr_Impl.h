/**
 *******************************************************************************
 * 
 * @file    AutoCorr_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_AUTOCORR_IMPL_H_INCLUDED
#define TDVCC_AUTOCORR_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/Complex.h"
#include "util/Timer.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/SparseClusterOper.h"
#include "input/ModeCombiOpRange.h"
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "vcc/TensorDataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "td/tdvcc/anal/AutoCorrTwoModeMachine.h"


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      )
   {
      const PARAM_T corr_A = Dot(arStateInit.Coefs(), arStateT.Coefs());
      const PARAM_T corr_B = Dot(arStateT.Coefs(), arStateInit.Coefs());
      const PARAM_T corr_2t = SumProdElems(arStateT.Coefs(), arStateT.Coefs());

      return std::make_tuple(corr_A, midas::math::Conj(corr_B), corr_2t);
   }

   /************************************************************************//**
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate" (actually the
    *    same for VCI).
    *    - first:  \f$ \langle \Psi(0) \vert \Psi(t) \rangle \f$ (`<VCI(0)|VCI(t)>`)
    *    - second: \f$ \langle \Psi(t) \vert \Psi(0) \rangle^* \f$ (`<VCI(t)|VCI(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return AutoCorr(arStateT, arStateInit);
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Lambda(0) \vert \Psi(t) \rangle \f$ (`<L(0)|VCC(t)>`)
    *    - second: \f$ \langle \Lambda(t) \vert \Psi(0) \rangle^* \f$ (`<L(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if (arStateT.SizeParams() != arStateInit.SizeParams())
      {
         std::stringstream ss;
         ss << "arStateT.SizeParams() (which is " << arStateT.SizeParams()
            << ") != arStateInit.SizeParams() (which is " << arStateInit.SizeParams()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         MidasWarning("Converting TensorDataCont to DataCont in TDVCC auto-correlation function.");
         ParamsTdvcc<PARAM_T, GeneralDataCont> dc_t(DataContFromTensorDataCont(arStateT.ClusterAmps()), DataContFromTensorDataCont(arStateT.LambdaCoefs()), arStateT.Phase());
         ParamsTdvcc<PARAM_T, GeneralDataCont> dc_init(DataContFromTensorDataCont(arStateInit.ClusterAmps()), DataContFromTensorDataCont(arStateInit.LambdaCoefs()), arStateInit.Phase());

         return AutoCorr(dc_t, dc_init, arNModals, arMcr);
      }
      else
      {
         using midas::util::matrep::ExpTRef;

         auto sumprodelem = []
            (  const CONT_TMPL<PARAM_T>& L
            ,  const CONT_TMPL<PARAM_T>& expDT
            )  ->PARAM_T
         {
            if (Size(L) != Size(expDT))
            {
               // If this fails, you probably need to enhance the ExpTRef impl. to
               // account for non-canonical ModeCombiOpRange of input/result.
               std::stringstream ss;
               ss << "Size(L) (which is "<<Size(L)<<") != Size(expDT) (which is "<<Size(expDT)<<").";
               MIDASERROR(ss.str());
            }
            PARAM_T val = SumProdElems(L, expDT, 1);
            if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
            {
               PARAM_T tmp(0);
               expDT.DataIo(IO_GET, 0, tmp);
               val += tmp;
            }
            else
            {
               val += expDT[0];
            }
            return val;
         };

         // <L(0)|VCC(t)> = <ref|(1+L(0))exp(T(t)-T(0))|ref> exp(-i(e(t)-e(0)))
         auto t_diff = arStateT.ClusterAmps();
         Axpy(t_diff, arStateInit.ClusterAmps(), PARAM_T(-1));
         const CONT_TMPL<PARAM_T> exp_t_diff_A(ExpTRef(arNModals, arMcr, t_diff, arMcr.GetMaxExciLevel()));
         PARAM_T corr_A = sumprodelem(arStateInit.LambdaCoefs(), exp_t_diff_A);

         // <L(t)|VCC(0)> = <ref|(1+L(t))exp(T(0)-T(t))|ref> exp(+i(e(t)-e(0)))
         Scale(t_diff, PARAM_T(-1));
         const CONT_TMPL<PARAM_T> exp_t_diff_B(ExpTRef(arNModals, arMcr, t_diff, arMcr.GetMaxExciLevel()));
         PARAM_T corr_B = sumprodelem(arStateT.LambdaCoefs(), exp_t_diff_B);

         // <L^*(t)|VCC(t)> = <ref|(1+L^*(t))exp(T(t)-T^*(t))|ref> exp(-i(e(t) + e^*(t)))
         auto diff_2t = arStateT.ClusterAmps();
         Axpy(diff_2t, arStateT.ClusterAmps().Conjugate(), PARAM_T(-1)); 

         const CONT_TMPL<PARAM_T> exp_diff_2t(ExpTRef(arNModals, arMcr, diff_2t, arMcr.GetMaxExciLevel()));
         PARAM_T corr_2t = sumprodelem(arStateT.LambdaCoefs().Conjugate(), exp_diff_2t);

         if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
         {
            const PARAM_T phase_diff = arStateT.Phase() - arStateInit.Phase();
            corr_A *= exp(PARAM_T(0,-1)*phase_diff);
            corr_B *= exp(PARAM_T(0,+1)*phase_diff);
            const PARAM_T phase_sum = 2*std::real(arStateT.Phase());
            corr_2t *= exp(PARAM_T(0,-1)*phase_sum);
         }

         return std::make_tuple(corr_A, midas::math::Conj(corr_B), corr_2t);
      }
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Sigma(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Sigma(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if (arStateT.SizeParams() != arStateInit.SizeParams())
      {
         std::stringstream ss;
         ss << "arStateT.SizeParams() (which is " << arStateT.SizeParams()
            << ") != arStateInit.SizeParams() (which is " << arStateInit.SizeParams()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      using midas::util::matrep::ExpTRef;

      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         MidasWarning("Converting TensorDataCont to DataCont in TIM-TDEXTVCC auto-correlation function.");
         ParamsTdextvcc<PARAM_T, GeneralDataCont> dc_t(DataContFromTensorDataCont(arStateT.ClusterAmps()), DataContFromTensorDataCont(arStateT.ExtAmps()), arStateT.Phase());
         ParamsTdextvcc<PARAM_T, GeneralDataCont> dc_init(DataContFromTensorDataCont(arStateInit.ClusterAmps()), DataContFromTensorDataCont(arStateInit.ExtAmps()), arStateInit.Phase());

         return AutoCorr(dc_t, dc_init, arNModals, arMcr);
      }
      else
      {
         // NB! Operator exponentials always extend to full space.
         // <S(0)|VCC(t)> = <ref|exp(Sigma(0))exp(+(T(t)-T(0)))|ref> exp(-i(e(t)-e(0)))
         auto t_diff = arStateT.ClusterAmps();
         Axpy(t_diff, arStateInit.ClusterAmps(), PARAM_T(-1));
         CONT_TMPL<PARAM_T> exp_t_diff = ExpTRef(arNModals, arMcr, t_diff, arMcr.NumberOfModes());
         CONT_TMPL<PARAM_T> exp_s = ExpTRef(arNModals, arMcr, arStateInit.ExtAmps(), arMcr.NumberOfModes());
         PARAM_T corr_A = SumProdElems(exp_s, exp_t_diff);

         // <S(t)|VCC(0)> = <ref|exp(Sigma(t))exp(-(T(t)-T(0)))|ref> exp(+i(e(t)-e(0)))
         Scale(t_diff, PARAM_T(-1));
         exp_t_diff = ExpTRef(arNModals, arMcr, t_diff, arMcr.NumberOfModes());
         exp_s = ExpTRef(arNModals, arMcr, arStateT.ExtAmps(), arMcr.NumberOfModes());
         PARAM_T corr_B = SumProdElems(exp_s, exp_t_diff);

         // <L^*(t)|VCC(t)> = <ref|(1+L^*(t))exp(T(t)-T^*(t))|ref> exp(-i(e(t)+e^*(t)))
         auto diff_2t = arStateT.ClusterAmps();
         Axpy(diff_2t, arStateT.ClusterAmps().Conjugate(), PARAM_T(-1)); 

         const CONT_TMPL<PARAM_T> exp_diff_2t = ExpTRef(arNModals, arMcr, diff_2t, arMcr.NumberOfModes());
         const CONT_TMPL<PARAM_T> exp_s_2t = ExpTRef(arNModals, arMcr, arStateT.ExtAmps().Conjugate(), arMcr.NumberOfModes());
         PARAM_T corr_2t = SumProdElems(exp_s_2t, exp_diff_2t);

         if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
         {
            const PARAM_T phase_diff = arStateT.Phase() - arStateInit.Phase();
            corr_A *= exp(PARAM_T(0,-1)*phase_diff);
            corr_B *= exp(PARAM_T(0,+1)*phase_diff);
            const PARAM_T phase_sum = 2*std::real(arStateT.Phase());
            corr_2t *= exp(PARAM_T(0,-1)*phase_sum);
         }

         return std::make_tuple(corr_A, midas::math::Conj(corr_B), corr_2t);
      }
   }

   /************************************************************************//**
    * @return
    *    Same as for AutoCorr, just here fore the interface.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return AutoCorr(arStateT, arStateInit);
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate", but with
    *    the bra treated as if it was an ExtVCC parameterization, i.e.
    *    `<Phi|exp(L~)exp(-T)` instead of `<Lambda| = <Phi|(1+L)exp(-T)`, where
    *    `L~` is such that `exp(L~) = 1 + L` _within_ the arMcr-space.
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Psi'(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Psi'(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::InvExpTRef;
      // Add 1 to the stored L-coefs. to account for the fact that InvExpTRef
      // expects (1+L) as argument.
      auto one_p_Lprime_coefs_t = arStateT.DualParams();
      auto one_p_Lprime_coefs_0 = arStateInit.DualParams();
      ModeCombiOpRange::const_iterator it;
      if (arMcr.Find(std::vector<In>{}, it))
      {
         const auto addr = it->Address();
         if (0 <= addr && addr < Size(one_p_Lprime_coefs_t))
         {
            if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
            {
               PARAM_T val = 0;

               one_p_Lprime_coefs_t.DataIo(IO_GET, addr, val);
               val += PARAM_T(1);
               one_p_Lprime_coefs_t.DataIo(IO_PUT, addr, val);

               one_p_Lprime_coefs_0.DataIo(IO_GET, addr, val);
               val += PARAM_T(1);
               one_p_Lprime_coefs_0.DataIo(IO_PUT, addr, val);
            }
            else if constexpr (  std::is_same_v<CONT_TMPL<PARAM_T>,GeneralTensorDataCont<PARAM_T>>
                              )
            {
               const auto ref_nr = it->ModeCombiRefNr();
               one_p_Lprime_coefs_t.GetModeCombiData(ref_nr).ElementwiseScalarAddition(PARAM_T(1));
               one_p_Lprime_coefs_0.GetModeCombiData(ref_nr).ElementwiseScalarAddition(PARAM_T(1));
            }
            else
            {
               one_p_Lprime_coefs_t[addr] += PARAM_T(1);
               one_p_Lprime_coefs_0[addr] += PARAM_T(1);
            }
         }
      }

      if constexpr (  std::is_same_v<CONT_TMPL<PARAM_T>,GeneralTensorDataCont<PARAM_T>>
                   )
      {
         return AutoCorr
            (  ParamsTdextvcc<PARAM_T,GeneralDataCont>
                  (  DataContFromTensorDataCont(arStateT.PrimalParams())
                  ,  InvExpTRef(arNModals, arMcr, DataContFromTensorDataCont(one_p_Lprime_coefs_t))
                  ,  arStateT.Phase()
                  )
            ,  ParamsTdextvcc<PARAM_T,GeneralDataCont>
                  (  DataContFromTensorDataCont(arStateInit.PrimalParams())
                  ,  InvExpTRef(arNModals, arMcr, DataContFromTensorDataCont(one_p_Lprime_coefs_0))
                  ,  arStateInit.Phase()
                  )
            ,  arNModals
            ,  arMcr
            );
      }
      else
      {
         return AutoCorr
            (  ParamsTdextvcc<PARAM_T,CONT_TMPL>
                  (  arStateT.PrimalParams()
                  ,  InvExpTRef(arNModals, arMcr, one_p_Lprime_coefs_t)
                  ,  arStateT.Phase()
                  )
            ,  ParamsTdextvcc<PARAM_T,CONT_TMPL>
                  (  arStateInit.PrimalParams()
                  ,  InvExpTRef(arNModals, arMcr, one_p_Lprime_coefs_0)
                  ,  arStateInit.Phase()
                  )
            ,  arNModals
            ,  arMcr
            );
      }
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate", but with
    *    the bra treated as if it was a (traditional) VCC parameterization, i.e.
    *    `<Psi'| = <Phi|(1+Sigma~)exp(-T)` instead of `<Psi'| = <Phi|exp(Sigma)exp(-T)`,
    *    where `Sigma~` is such that `exp(Sigma) = 1 + Sigma~` _within_ the arMcr-space.
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Psi'(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Psi'(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::SparseClusterOper;
      using midas::util::matrep::TrfExpClusterOper;
      using midas::util::matrep::MatRepVibOper;
      using mv_t = GeneralMidasVector<PARAM_T>;

      SparseClusterOper sco(arNModals, arMcr);

      mv_t S_prime_t_full(sco.FullDim());
      mv_t S_prime_0_full(sco.FullDim());
      S_prime_t_full[0] = PARAM_T(1);
      S_prime_0_full[0] = PARAM_T(1);
      if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
      {
         mv_t sigma_t(arStateT.DualParams().Size());
         mv_t sigma_0(arStateInit.DualParams().Size());
         arStateT.DualParams().DataIo(IO_GET, sigma_t, sigma_t.Size());
         arStateInit.DualParams().DataIo(IO_GET, sigma_0, sigma_0.Size());
         S_prime_t_full = TrfExpClusterOper<false,false>(sco, sigma_t, std::move(S_prime_t_full), PARAM_T(1));
         S_prime_0_full = TrfExpClusterOper<false,false>(sco, sigma_0, std::move(S_prime_0_full), PARAM_T(1));
      }
      else
      {
         S_prime_t_full = TrfExpClusterOper<false,false>(sco, arStateT.DualParams(), std::move(S_prime_t_full), PARAM_T(1));
         S_prime_0_full = TrfExpClusterOper<false,false>(sco, arStateInit.DualParams(), std::move(S_prime_0_full), PARAM_T(1));
      }
      S_prime_t_full[0] -= PARAM_T(1);
      S_prime_0_full[0] -= PARAM_T(1);

      return AutoCorr
         (  ParamsTdvcc<PARAM_T,CONT_TMPL>
               (  arStateT.PrimalParams()
               ,  CONT_TMPL<PARAM_T>(MatRepVibOper<PARAM_T>::ExtractToMcrSpace(S_prime_t_full, arMcr, arNModals))
               ,  arStateT.Phase()
               )
         ,  ParamsTdvcc<PARAM_T,CONT_TMPL>
               (  arStateInit.PrimalParams()
               ,  CONT_TMPL<PARAM_T>(MatRepVibOper<PARAM_T>::ExtractToMcrSpace(S_prime_0_full, arMcr, arNModals))
               ,  arStateInit.Phase()
               )
         ,  arNModals
         ,  arMcr
         );
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Sigma(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Sigma(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>>
                     )
      {
         using mat_t = typename ParamsTdmvcc<PARAM_T, CONT_TMPL>::mat_t;

         if (arStateT.SizeParams() != arStateInit.SizeParams())
         {
            std::stringstream ss;
            ss << "arStateT.SizeParams() (which is " << arStateT.SizeParams()
               << ") != arStateInit.SizeParams() (which is " << arStateInit.SizeParams()
               << ")."
               ;
            MIDASERROR(ss.str());
         }

         auto nmodes = arNModals.size();
         const auto& modals_0 = arStateInit.ModalCont();
         const auto& modals_t = arStateT.ModalCont();

         // Construct overlap matrix
         std::vector<mat_t> overlaps_0t(nmodes);
         std::vector<mat_t> overlaps_t0(nmodes);
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            const auto& ut = modals_t[imode].first;
            const auto& w0 = modals_0[imode].second;
            overlaps_0t[imode] = mat_t(w0 * ut);
            const auto& u0 = modals_0[imode].first;
            const auto& wt = modals_t[imode].second;
            overlaps_t0[imode] = mat_t(wt * u0);
         }

         using midas::util::matrep::ExpTRef;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::SparseClusterOper;
         using midas::util::matrep::TrfExpClusterOper;

         // Initialize sparse cluster operator
         SparseClusterOper cluster_oper(arNModals, arMcr);
         const auto& amps_0 = arStateInit.ClusterAmps();
         const auto& amps_t = arStateT.ClusterAmps();

         PARAM_T corr_A(0);
         {
            // Construct full-space matrix representation of overlaps
            MatRepVibOper<PARAM_T> overlap_0t(arNModals);
            overlap_0t.DirProdOneModeOpers(overlaps_0t);
            auto s_0t = std::move(overlap_0t).GetMatRep();

            // NB! Operator exponentials always extend to full space.
            // <Psi'(0)|Psi(t)> = <ref(0)|(1+L(0))exp(-T(0)) * exp(T(t))|ref(t)> * exp(-i(e(t)-e(0)))
            CONT_TMPL<PARAM_T> ket(cluster_oper.FullDim(), PARAM_T(0.0));
            ket[0] = 1;
            ket = TrfExpClusterOper<false,false>(cluster_oper, amps_t, std::move(ket), PARAM_T(+1.0));
            CONT_TMPL<PARAM_T> trans_ket = s_0t * ket;
            CONT_TMPL<PARAM_T> bra(cluster_oper.FullDim(), PARAM_T(0.0));
            bra = cluster_oper.RefToFullSpace(arStateInit.LambdaCoefs());
            bra[0] += 1;
            bra = TrfExpClusterOper<true,false>(cluster_oper, amps_0, std::move(bra), PARAM_T(-1.0));
            corr_A = SumProdElems(bra, trans_ket);
         }

         PARAM_T corr_B(0);
         {
            // Construct full-space matrix representation of overlaps
            MatRepVibOper<PARAM_T> overlap_t0(arNModals);
            overlap_t0.DirProdOneModeOpers(overlaps_t0);
            auto s_t0 = std::move(overlap_t0).GetMatRep();

            // <Psi'(t)|Psi(0)> = <ref(t)|(1+L(t))exp(-T(t)) * exp(T(0)))|ref(0)> exp(+i(e(t)-e(0)))
            CONT_TMPL<PARAM_T> ket(cluster_oper.FullDim(), PARAM_T(0.0));
            ket[0] = 1;
            ket = TrfExpClusterOper<false,false>(cluster_oper, amps_0, std::move(ket), PARAM_T(+1.0));
            CONT_TMPL<PARAM_T> trans_ket = s_t0 * ket;
            CONT_TMPL<PARAM_T> bra(cluster_oper.FullDim(), PARAM_T(0.0));
            bra = cluster_oper.RefToFullSpace(arStateT.LambdaCoefs());
            bra[0] += 1;
            bra = TrfExpClusterOper<true,false>(cluster_oper, amps_t, std::move(bra), PARAM_T(-1.0));
            corr_B = SumProdElems(bra, trans_ket);
         }

         if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
         {
            const PARAM_T phase_diff = arStateT.Phase() - arStateInit.Phase();
            corr_A *= exp(PARAM_T(0,-1)*phase_diff);
            corr_B *= exp(PARAM_T(0,+1)*phase_diff);
         }

         return std::make_pair(corr_A, midas::math::Conj(corr_B));
      }
      else
      {
         MIDASERROR("TDMVCC auto-correlation function is only implemented for CONT_TMPL = GeneralMidasVector (i.e. for MatRep framework).");
         return std::make_pair(PARAM_T(0.0), PARAM_T(0.0));
      }
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Sigma(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Sigma(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      MIDASERROR("Tdmvcc AutoCorrExt not implemented!");
      return std::make_pair(PARAM_T(0.0), PARAM_T(0.0));
   }

   /***************************************************************************
    * Autocorrelation function for TDMVCC[2]
    * 
    * @param[in] arStateT      The current state
    * @param[in] arStateInit   The initial state
    * @param[in] arNModals     The modals per mode
    * @param[in] arMcr         The mode combination range
    * @param[in] aInitType     Initial state type
    * @param[in] arSettings    Settings for calculating the autocorrelation function
    * @return
    *    Pair of autocorrelation functions:
    *    - first : <0|t>
    *    - second: <t|0>*
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrTdmvcc2
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const InitType aInitType
      ,  const AutoCorrSettings& arSettings 
      ,  Uin aIoLevel
      )
   {  
      // Aliases, declarations etc.
      using machine_t = AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>;
      using param_t = PARAM_T;
      param_t corr_A(0.0);
      param_t corr_B(0.0);   
      StateType bratype;
      StateType kettype;
      bool calc_type_a = arSettings.mCalcTypeA;
      bool calc_type_b = arSettings.mCalcTypeB;
      bool verbose = (gIoLevel > 6) || (gDebug);
      
      // Calculate type A autocorrelation function (<0|t>)
      if (calc_type_a)
      {
         if (verbose)
         {
            Mout << "Calculating type A autocorrelation function!" << std::endl;
         }

         bratype = StateType::GENERAL;
         kettype = StateType::GENERAL;
         if (aInitType == InitType::VSCFREF) 
         {
            bratype = StateType::VSCF;
         }

         Timer timer;
         machine_t machine_A(arStateInit, arStateT, bratype, kettype, arNModals, arMcr, arSettings, aIoLevel);
         corr_A = machine_A.GetAutoCorr();

         if (verbose)
         {
            Mout << "Time spent on type A = " << timer.CpuTime(false) << std::endl;
         }
      }

      // Calculate type B autocorrelation function (<t|0>, i.e. must be conjugated before returning)
      if (calc_type_b)
      {
         if (verbose)
         {
            Mout << "Calculating type B autocorrelation function!" << std::endl;
         }

         bratype = StateType::GENERAL;
         kettype = StateType::GENERAL;
         if (aInitType == InitType::VSCFREF) 
         {
            kettype = StateType::VSCF;
         }

         Timer timer;
         machine_t machine_B(arStateT, arStateInit, bratype, kettype, arNModals, arMcr, arSettings, aIoLevel);
         corr_B = machine_B.GetAutoCorr();

         if (verbose)
         {
            Mout << "Time spent on type B = " << timer.CpuTime(false) << std::endl;
         }
      }

      // Multiply by the phase/norm factor
      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         const param_t phase_diff = arStateT.Phase() - arStateInit.Phase();
         if (verbose)
         {
            if (calc_type_a) Mout << "Multiplying type A by phase factor = " << exp(param_t(0,-1)*phase_diff) << std::endl;
            if (calc_type_b) Mout << "Multiplying type B by phase factor = " << exp(param_t(0,+1)*phase_diff) << std::endl;
         }
         corr_A *= exp(param_t(0,-1)*phase_diff);
         corr_B *= exp(param_t(0,+1)*phase_diff);
      }

      // Return result
      return std::make_pair(corr_A, midas::math::Conj(corr_B));
   }

} /* namespace midas::tdvcc */

#endif/*TDVCC_AUTOCORR_IMPL_H_INCLUDED*/
