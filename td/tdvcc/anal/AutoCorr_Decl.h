/**
 *******************************************************************************
 * 
 * @file    AutoCorr_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_AUTOCORR_DECL_H_INCLUDED
#define TDVCC_AUTOCORR_DECL_H_INCLUDED

// Standard headers.
#include <utility>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/anal/AutoCorrSettings.h"


// Forward declarations.
class ModeCombi;
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;
template<typename> class GeneralDataCont;
template<typename> class GeneralTensorDataCont;
namespace midas::tdvcc
{

   // Forward declarations
   template<typename, template<typename> class> class ParamsTdvcc;
   template<typename, template<typename> class> class ParamsTdvci;
   template<typename, template<typename> class> class ParamsTdextvcc;
   template<typename, template<typename> class> class ParamsTdmvcc;

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::tuple<PARAM_T,PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrTdmvcc2
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      ,  const InitType aInitType
      ,  const midas::tdvcc::AutoCorrSettings& arSettings
      ,  Uin aIoLevel
      );

} /* namespace midas::tdvcc */

#endif/*TDVCC_AUTOCORR_DECL_H_INCLUDED*/
