/**
 *******************************************************************************
 * 
 * @file    AutoCorrTwoModeMachine.cc
 * @date    10-04-2021
 * @author  Andreas Buchgraitz Jensen
 * @author  Mads Greisen Højlund
 *
 * @brief   Class for calculating TDMVCC[2] autocorrelation function.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/AutoCorrTwoModeMachine.h"
#include "td/tdvcc/anal/AutoCorrTwoModeMachine_Impl.h"

#include "vcc/TensorDataCont.h"

// Define instantiation macro.
#define INSTANTIATE_AUTOCORRTWOMODEMACHINE(PARAM_T, CONT_TMPL) \
   namespace midas::tdvcc \
   { \
   template class AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>; \
   }; \

// Instantiations.
INSTANTIATE_AUTOCORRTWOMODEMACHINE(Nb, GeneralMidasVector);
INSTANTIATE_AUTOCORRTWOMODEMACHINE(Nb, GeneralDataCont);
INSTANTIATE_AUTOCORRTWOMODEMACHINE(Nb, GeneralTensorDataCont);
INSTANTIATE_AUTOCORRTWOMODEMACHINE(std::complex<Nb>, GeneralMidasVector);
INSTANTIATE_AUTOCORRTWOMODEMACHINE(std::complex<Nb>, GeneralDataCont);
INSTANTIATE_AUTOCORRTWOMODEMACHINE(std::complex<Nb>, GeneralTensorDataCont);

#undef INSTANTIATE_AUTOCORRTWOMODEMACHINE

#endif /*DISABLE_PRECOMPILED_TEMPLATES*/
