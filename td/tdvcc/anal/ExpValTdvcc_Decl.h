/**
 *******************************************************************************
 * 
 * @file    ExpValTdvcc_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXPVALTDVCC_DECL_H_INCLUDED
#define EXPVALTDVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"

namespace midas::tdvcc
{

   // Forward declarations.
   template<typename, template<typename> class> class ParamsTdvcc;
   template<typename, template<typename> class> class ParamsTdvci;
   template<typename, template<typename> class> class ParamsTdextvcc;
   template<typename, template<typename> class> class ParamsTdmvcc;

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  typename TRF_T
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  TRF_T& arTrf
      );

} /* namespace midas::tdvcc */

#endif/*EXPVALTDVCC_DECL_H_INCLUDED*/
