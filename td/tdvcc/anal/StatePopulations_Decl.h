/**
 *******************************************************************************
 * 
 * @file    StatePopulations_Decl.h
 * @date    25-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_STATEPOPULATIONS_DECL_H_INCLUDED
#define TDVCC_STATEPOPULATIONS_DECL_H_INCLUDED

// Standard headers.
#include <utility>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"

// Forward declarations.
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;
template<typename> class GeneralTensorDataCont;
namespace midas::tdvcc
{

   // Forward declarations
   template<typename, template<typename> typename> class ParamsTdvcc;
   template<typename, template<typename> typename> class ParamsTdextvcc;
   template<typename, template<typename> typename> class ParamsTdvci;
   template<typename, template<typename> typename> class ParamsTdmvcc;

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      ,  In
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      ,  In
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      ,  template<typename, template<typename> typename> typename PARAMS_TMPL
      ,  std::enable_if_t  <  std::is_same_v<PARAMS_TMPL<PARAM_T, CONT_TMPL>, ParamsTdvcc<PARAM_T, CONT_TMPL> >
                           || std::is_same_v<PARAMS_TMPL<PARAM_T, CONT_TMPL>, ParamsTdextvcc<PARAM_T, CONT_TMPL> >
                           >* = nullptr
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const PARAMS_TMPL<PARAM_T, CONT_TMPL>& arStateT
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      ,  In
      );

} /* namespace midas::tdvcc */

#endif/*TDVCC_STATEPOPULATIONS_DECL_H_INCLUDED*/
