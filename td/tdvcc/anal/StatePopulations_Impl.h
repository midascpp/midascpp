/**
 *******************************************************************************
 * 
 * @file    StatePopulations_Impl.h
 * @date    25-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_STATEPOPULATIONS_IMPL_H_INCLUDED
#define TDVCC_STATEPOPULATIONS_IMPL_H_INCLUDED

// Midas headers.
#include "input/ProgramSettings.h"
#include "util/type_traits/Complex.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/SparseClusterOper.h"
#include "input/ModeCombiOpRange.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "vcc/TensorDataCont.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "lapack_interface/HEEVD.h"

namespace midas::tdvcc
{
namespace detail
{
   template
      <  typename PARAM_T
      ,  template<typename> typename MAT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T>> DiagonalizeAndSortStatePopulations
      (  const MAT_TMPL<PARAM_T>& aDensMat
      )
   {
      using mat_t = MAT_TMPL<PARAM_T>;

      assert(aDensMat.IsSquare());
      auto dim = aDensMat.Nrows();
      std::vector<midas::type_traits::RealTypeT<PARAM_T>> result(dim);

      // Calculate eigenvalues (we need eigvecs to assign to state)
      auto eig = HEEVD(aDensMat, 'V');
   
      // Get eigenvectors in transformed basis, i.e. initially populated state will be first basis function (at least if starting from VSCF with targeting on electronic DOF)
      mat_t eigvecs(dim,dim);
      LoadEigenvectors(eig, eigvecs);
   
      for(In icol=I_0; icol<dim; ++icol)
      {
         // Find the state index
         auto maxval = std::abs(eigvecs[I_0][icol]);
         In maxelem = I_0;
         for(In irow=I_1; irow<dim; ++irow)
         {
            if (  std::abs(eigvecs[irow][icol]) > maxval
               )
            {
               maxelem = irow;
               maxval = std::abs(eigvecs[irow][icol]);
            }
         }
   
         // Check that this value has not been set before.
         MidasAssert(!result[maxelem], "The same state cannot have two populations. Something is wrong!");
   
         result[maxelem] = eig._eigenvalues[icol];
      }
   
      return result;
   }
} /* namespace detail */

   /************************************************************************//**
    * TDVCI version
    *
    * @note
    *    Niels: This can be done without transformer. Check the MCTDH implementation of density matrices.
    *
    * @return
    * Vector of state populations for all electronic states
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& aStateT
      ,  const std::vector<Uin>& aNModals
      ,  const ModeCombiOpRange& aMcr
      ,  In aNElectronicStates
      )
   {
      using trf_t = TRF_T;
      using real_t = midas::type_traits::RealTypeT<PARAM_T>;
      using mat_t = GeneralMidasMatrix<PARAM_T>;

      auto n = aNElectronicStates;
      if (  n == 1
         )
      {
         return std::vector<real_t>{1.0};
      }
      mat_t densmat(n,n);
      In m_elec = gOperatorDefs.ElectronicDofNr();
      if (  m_elec < I_0
         )
      {
         MIDASERROR("No electronic degree of freedom found for calculating state populations.");
      }

      if constexpr   (  trf_t::trf_type == midas::tdvcc::TrfType::MATREP
                     )
      {
         midas::util::matrep::SparseClusterOper clust_oper(aNModals, aMcr);
         auto v = clust_oper.RefToFullSpace(aStateT.Coefs());

         for(Uin i=I_0; i<n; ++i)
         {
            for(Uin j=I_0; j<n; ++j)
            {
               densmat[i][j] = midas::util::matrep::ShiftOperBraket<true>
                  (  v
                  ,  v
                  ,  aNModals
                  ,  std::set<Uin>{static_cast<Uin>(m_elec)}
                  ,  std::vector<Uin>{i}
                  ,  std::vector<Uin>{j}
                  );
            }
         }
      }
      else
      {
         MidasWarning("TDVCI state populations are calculated inefficiently using elementary operators.");

         OpDef& elementary = gOperatorDefs.GetOperator("Elementary");
         std::vector<Uin> n_opers(aMcr.NumberOfModes(), 0);
         n_opers[m_elec] = 1;

         // Loop over elements
         for(In i=I_0; i<n; ++i)
         {
            for(In j=i; j<n; ++j)
            {
               // 0) Set m,i,j in opdef
               elementary.SetElementaryOperatorTo(m_elec, i, j);

               // 1) Calculate integrals
               auto ints = midas::vcc::modalintegrals::ConstructModalIntegralsElemOper<PARAM_T>(m_elec, i, j, aNModals, n_opers);

               // 2) Construct transformer
               trf_t trf(aNModals, elementary, ints, aMcr);

               if constexpr   (  std::is_same_v<trf_t, TrfGeneralTimTdvci<PARAM_T>>
                              )
               {
                  trf.V3cFilePrefix() = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
                  trf.InitializeContribs();
               }

               // 3) Calculate exp. val.
               densmat[i][j] = ExpVal(C_0, aStateT, trf);

               if (  i != j
                  )
               {
                  densmat[j][i] = midas::math::Conj(densmat[i][j]);
               }
            }
         }
      }

      // Calculate eigenvalues
      return detail::DiagonalizeAndSortStatePopulations(densmat);
   }

   /************************************************************************//**
    * TD(E)VCC version
    *
    * @param[in] aStateT
    *    The state at time t.
    * @param[in] aNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] aMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @param[in] aNElectronicStates
    *    The number of electronic states
    * @return
    *    Vector of state populations for all electronic states
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      ,  template<typename, template<typename> typename> typename PARAMS_TMPL
      ,  std::enable_if_t  <  std::is_same_v<PARAMS_TMPL<PARAM_T, CONT_TMPL>, ParamsTdvcc<PARAM_T, CONT_TMPL> >
                           || std::is_same_v<PARAMS_TMPL<PARAM_T, CONT_TMPL>, ParamsTdextvcc<PARAM_T, CONT_TMPL> >
                           >*
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const PARAMS_TMPL<PARAM_T, CONT_TMPL>& aStateT
      ,  const std::vector<Uin>& aNModals
      ,  const ModeCombiOpRange& aMcr
      ,  In aNElectronicStates
      )
   {
      using params_t = PARAMS_TMPL<PARAM_T, CONT_TMPL>;
      using trf_t = TRF_T;
      using real_t = midas::type_traits::RealTypeT<PARAM_T>;
      using mat_t = GeneralMidasMatrix<PARAM_T>;
      using vec_t = GeneralMidasVector<PARAM_T>;

      auto n = aNElectronicStates;
      if (  n == 1
         )
      {
         return std::vector<real_t>{1.0};
      }

      mat_t densmat(n,n);
      auto m_elec = gOperatorDefs.ElectronicDofNr();
      if (  m_elec < I_0
         )
      {
         MIDASERROR("No electronic degree of freedom found for calculating state populations.");
      }

      if constexpr   (  trf_t::trf_type == midas::tdvcc::TrfType::MATREP
                     )
      {
         using namespace midas::util::matrep;
         SparseClusterOper clust_oper(aNModals, aMcr);

         vec_t ket(clust_oper.FullDim(), PARAM_T(0.));
         ket[0] = 1;
         ket = TrfExpClusterOper<false, false>(clust_oper, aStateT.ClusterAmps(), std::move(ket), PARAM_T(+1.));

         vec_t bra(clust_oper.FullDim(), PARAM_T(0.));
         // If TDVCC
         if constexpr   (  std::is_same_v<params_t, ParamsTdvcc<PARAM_T, CONT_TMPL>>
                        )
         {
            bra = clust_oper.RefToFullSpace(aStateT.LambdaCoefs());
            bra[0] += 1;
            bra = TrfExpClusterOper<true, false>(clust_oper, aStateT.ClusterAmps(), std::move(bra), PARAM_T(-1.));
            //abj: Here we calculate the bra state twice ?? Found in both devel and modal_adapt_tdvcc branches during merge
            MidasWarning("Am I using the correct way to calculate the bra state here?");
         }
         // Else, we use TDEVCC
         else
         {
            bra[0] = 1;
            bra = TrfExpClusterOper<false, false>(clust_oper, aStateT.ExtAmps(), std::move(bra), PARAM_T(+1.));
            //abj: Here we calculate the bra state twice ?? Found in both devel and modal_adapt_tdvcc branches during merge
            bra = TrfExpClusterOper<true, false> (clust_oper, aStateT.ClusterAmps(), std::move(bra), PARAM_T(-1.));
            MidasWarning("Am I using the correct way to calculate the bra state here?");
         }

         for(Uin i=I_0; i<n; ++i)
         {
            for(Uin j=I_0; j<n; ++j)
            {
               densmat[i][j] = midas::util::matrep::ShiftOperBraket<false>
                  (  bra
                  ,  ket
                  ,  aNModals
                  ,  std::set<Uin>{static_cast<Uin>(m_elec)}
                  ,  std::vector<Uin>{i}
                  ,  std::vector<Uin>{j}
                  );
            }
         }
      }
      else
      {
         OpDef& elementary = gOperatorDefs.GetOperator("Elementary");
         std::vector<Uin> n_opers(aMcr.NumberOfModes(), 0);
         n_opers[m_elec] = 1;

         // Loop over elements
         for(In i=I_0; i<n; ++i)
         {
            for(In j=I_0; j<n; ++j)
            {
               // 0) Set m,i,j in opdef
               elementary.SetElementaryOperatorTo(m_elec, i, j);

               // 1) Calculate integrals
               auto ints = midas::vcc::modalintegrals::ConstructModalIntegralsElemOper<PARAM_T>(m_elec, i, j, aNModals, n_opers);

               // 2) Construct transformer
               trf_t trf(aNModals, elementary, ints, aMcr);

               if constexpr   (  std::is_same_v<trf_t, TrfGeneralTimTdvcc<PARAM_T>>
                              )
               {
                  trf.V3cFilePrefix() = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
                  trf.InitializeContribs();
               }

               // 3) Calculate exp. val.
               densmat[i][j] = ExpVal(C_0, aStateT, trf);
            }
         }
      }

      // Symmetrize the density matrix to make it Hermitian
      // Make the diagonal elements real!
      for(In i=I_0; i<n; ++i)
      {
         densmat[i][i] = std::real(densmat[i][i]);

         for(In j=I_0; j<i; ++j)
         {
            densmat[i][j] = C_I_2 * (densmat[i][j] + densmat[j][i]);
            densmat[j][i] = midas::math::Conj(densmat[i][j]);
         } 
      }

      return detail::DiagonalizeAndSortStatePopulations(densmat);
   }

   /************************************************************************//**
    * TDMVCC version
    *
    * @return
    * Vector of state populations for all electronic states
    ***************************************************************************/
   template
      <  typename TRF_T
      ,  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T> > StatePopulations
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& aStateT
      ,  const std::vector<Uin>& aNModals
      ,  const ModeCombiOpRange& aMcr
      ,  In aNElectronicStates
      )
   {
      using trf_t = TRF_T;
      using real_t = midas::type_traits::RealTypeT<PARAM_T>;
      using mat_t = GeneralMidasMatrix<PARAM_T>;

      auto n = aNElectronicStates;
      if (  n == 1
         )
      {
         return std::vector<real_t>{1.0};
      }
      mat_t densmat(n,n);
      In m_elec = gOperatorDefs.ElectronicDofNr();
      if (  m_elec < I_0
         )
      {
         MIDASERROR("No electronic degree of freedom found for calculating state populations.");
      }

      if constexpr   (  trf_t::trf_type == midas::tdvcc::TrfType::MATREP
                     )
      {
         midas::util::matrep::SparseClusterOper clust_oper(aNModals, aMcr);
         densmat = midas::util::matrep::VccOneModeDensityMatrices(clust_oper, aStateT.ClusterAmps(), aStateT.LambdaCoefs())[m_elec];
      }
      else
      {
         MidasWarning("TDMVCC state populations are only available for MATREP transformer.");
         return std::vector<real_t>(n, static_cast<real_t>(0.0));
      }

      // Symmetrize the density matrix to make it Hermitian
      // Make the diagonal elements real!
      for(In i=I_0; i<n; ++i)
      {
         densmat[i][i] = std::real(densmat[i][i]);

         for(In j=I_0; j<i; ++j)
         {
            densmat[i][j] = C_I_2 * (densmat[i][j] + densmat[j][i]);
            densmat[j][i] = midas::math::Conj(densmat[i][j]);
         } 
      }

      // Calculate eigenvalues
      return detail::DiagonalizeAndSortStatePopulations(densmat);
   }

} /* namespace midas::tdvcc */

#endif/*TDVCC_STATEPOPULATIONS_IMPL_H_INCLUDED*/
