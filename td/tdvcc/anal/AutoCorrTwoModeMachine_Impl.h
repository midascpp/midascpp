/**
************************************************************************
* 
* @file    AutoCorrTwoModeMachine_Impl.h
* @date    10-04-2021
* @author  Andreas Buchgraitz Jensen
* @author  Mads Greisen Højlund
*
* @brief   Class for calculating TDMVCC[2] autocorrelation function.
*
* @copyright
*    Ove Christiansen, Aarhus University.
*    The code may only be used and/or copied with the written permission of
*    the author or in accordance with the terms and conditions under which the
*    program was supplied.  The code is provided "as is" without any expressed
*    or implied warranty.
* 
************************************************************************
**/

// Standard headers
#include <map>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <type_traits>
#include <boost/math/special_functions/binomial.hpp>

// Midas headers
#include "td/tdvcc/anal/AutoCorrTwoModeMachine.h"
#include "inc_gen/math_link.h" 
#include "input/OpDef.h"
#include "util/CallStatisticsHandler.h"

namespace midas::tdvcc
{
   /*****************************************************************//**
    * Constructor from data. The identities of arBraState and arKetState
    * depend on the kind of autocorrelation function being calculated.
    * For type A (<0|t>), the bra state is the bra of the initial
    * state while the ket state is the ket of the current state.
    * For type B (<t|0>), the bra state is the bra of the current
    * state while the ket state is the ket of the initial state (note
    * that type B must be complex conjugated before used!).
    * Initial states can be either VSCF or GENERAL (meaning the
    * amplitudes are non-zero).
    * 
    * @param[in] arBraState    Bra TDMVCC[2] state.
    * @param[in] arKetState    Ket TDMVCC[2] state.
    * @param[in] aBraType      Bra state type.
    * @param[in] aKetType      Ket state type.
    * @param[in] arNModals     The number of modals for each mode.
    * @param[in] arMcr         Mode combination range describing the state.
    * @param[in] arSettings    Settings for the autocorrelation function.
    * @param[in] aIoLevel      IO level for TDVCC module.
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::AutoCorrTwoModeMachine
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arBraState
      ,  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arKetState
      ,  const StateType aBraType
      ,  const StateType aKetType
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const settings_t& arSettings
      ,  Uin aIoLevel
      )
      :  mrBraState(arBraState)
      ,  mrKetState(arKetState)
      ,  mBraType(aBraType)
      ,  mKetType(aKetType)
      ,  mrNModals(arNModals)
      ,  mrMcr(arMcr)
      ,  mMcrDoubles(ExtractDoubles(arMcr))
      ,  mNModes(arMcr.NumberOfModes())
      ,  mMcAllModes(ModeCombi(arMcr.ModesContained(), -1))
      ,  mAutoCorrAlgo(arSettings.mAlgorithm)
      ,  mMaxLevel(arSettings.mMaxLevel) 
      ,  mThreshold(arSettings.mScreenThres) 
      ,  mDeltaThres(arSettings.mDeltaThres)
      ,  mNumThres(arSettings.mNumThres)
      ,  mVerbose(aIoLevel > 8)
   {
      // Set printing level
      if (mVerbose)
      {
         Mout << "Constructing AutoCorrTwoModeMachine!" << std::endl;
         Mout << "type        = " << StringFromEnum(mAutoCorrAlgo) << std::endl;
         Mout << "maxlevel    = " << mMaxLevel   << std::endl;
         Mout << "threshold   = " << mThreshold  << std::endl;
         Mout << "delta_thres = " << mDeltaThres << std::endl;
         Mout << "num_thres   = " << mNumThres   << std::endl;
      }

      // Do some checks
      if (mAutoCorrAlgo == AutoCorrAlgo::FULL && mMaxLevel != -1) 
      {
         MIDASERROR("mMaxLevel must be -1 when using AutoCorrAlgo::FULL!");
      }

      if (mNModes == 1) 
      {
         MIDASERROR("AutoCorrTwoModeMachine: I don't work for one-mode systems!");
      }

      ModeCombi mc_allmodes_check(McrDoubles().ModesContained(), -1);
      std::stringstream ss;
      ss << "Modes contained in all doubles != modes contained in all mode combinations."
         << "This should not happen!";
      MidasAssert(McAllModes() == mc_allmodes_check, ss.str());

      // Precalculate overlaps and down-contracted amplitudes
      InitializeModalOverlaps();
      InitializeT2Down();

      // Calculate mean passive overlap
      param_t sum_s_passive(0);
      param_t sum_log_s_passive(0);
      for (const auto& s : mPassiveOverlaps) { sum_s_passive += s; sum_log_s_passive += std::log(s); }
      mGeoMeanPassiveOverlaps = std::exp(sum_log_s_passive/real_t(mNModes));
      mAriMeanPassiveOverlaps = sum_s_passive/real_t(mNModes);
      if (mVerbose)
      {
         Mout << "|geo mean S_passive| = " << std::abs(mGeoMeanPassiveOverlaps) << std::endl;
         Mout << "|ari mean S_passive| = " << std::abs(mAriMeanPassiveOverlaps) << std::endl;
      }

      // Calculate mean down-contracted T2
      param_t sum_T2_down(0);
      param_t sum_log_T2_down(0);
      Uin count = 0;
      for (auto i = 0; i < mrMcr.NumberOfModes(); i++) 
      {
         for (auto j = i + 1; j < mrMcr.NumberOfModes(); j++) 
         {
            sum_T2_down += mT2Down[i][j];
            sum_log_T2_down += std::log(mT2Down[i][j]);
            count++;
         }
      }
      mGeoMeanT2Down = std::exp(sum_log_T2_down/real_t(count));
      mAriMeanT2Down = sum_T2_down/real_t(count);
      if (mVerbose)
      {
         Mout << "|geo mean T2_down|   = " << std::abs(mGeoMeanT2Down) << std::endl;
         Mout << "|ari mean T2_down|   = " << std::abs(mAriMeanT2Down) << std::endl;
      }

      // Print diagnostic (a small diagnotic means the recursive sum will likely converge fast)
      if (mVerbose)
      {
         real_t diag = std::abs(mGeoMeanT2Down)/std::pow(std::abs(mGeoMeanPassiveOverlaps), 2);
         Mout << "|geo mean T2_down|/|geo mean S_passive|**2 = "
              << diag
              << std::endl;
      }
         
      // Set autocorrelation function to zero and add Omega intermediates
      // Only Omega00 is always non-zero. The other are sometimes non-zero according to 
      // state types (VSCF or not) and number of modes.
      mAutoCorr = 0;
      CalcOmega00();
      if (mBraType != StateType::VSCF)
      {
         Mout.Mute();
         if (mKetType != StateType::VSCF)
         {
            CalcOmega120();
            if (mNModes > 2) 
            {
               CalcOmega121And122();
            }
         }
         CalcOmega1212();
         Mout.Unmute();
      }
   }

   /*****************************************************************//**
    * Calculate modal overlaps and save for later
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::InitializeModalOverlaps
      (  
      )
   {   
      LOGCALL("initialize modal overlaps");

      // Set size of mOverlapMatrices (vector of matrices) and mPassiveOverlaps (vector)
      mOverlapMatrices.SetNewSize(mNModes);
      mPassiveOverlaps.SetNewSize(mNModes);

      // Calculate S = W_bra*U_ket for each mode and save result
      for (In mode = 0; mode < mNModes; ++mode)
      {
         // Set size of matrix
         mOverlapMatrices[mode].SetNewSize(mrNModals[mode], mrNModals[mode], false, false);

         // Get W from the bra state and U from the ket state
         const auto& w_bra = mrBraState.ModalMats(mode).second;
         const auto& u_ket = mrKetState.ModalMats(mode).first;     

         // Calculate overlap matrix and save result
         mat_t S = mat_t(w_bra * u_ket);
         MidasAssert(S.IsSquare(), "S = W*U must be square!");
         mOverlapMatrices[mode] = S;
         mPassiveOverlaps[mode] = S[0][0];
      }
   }


   /*****************************************************************//**
    * Calculate fully and partially down-contracted T2 operators 
    * and save for later.
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::InitializeT2Down
      (
      )
   {    
      LOGCALL("initialize T2Down");

      // Set correct size of matrices 
      mT2Down.SetNewSize(mNModes, mNModes, false, false);
      mT2PartialDown.SetNewSize(mNModes, mNModes, false, false);

      // Loop over all two-mode mode combinations
      for (auto it_mc = mrMcr.Begin(2), end = mrMcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto m0 = mc.Mode(0);
         const auto m1 = mc.Mode(1);
         MidasAssert(ValidModePair(mc), "InitializeT2Down: Invalid mode pair!");
         const Uin nvir0 = mrNModals[m0] - 1;
         const Uin nvir1 = mrNModals[m1] - 1;
         const Uin namps = nvir0*nvir1;

         // Get ket state s amplitudes as matrix
         const mat_t s_ampl_mat = GetAmplMat(nvir0, nvir1, mrKetState.ClusterAmps(), mc, mrMcr);

         // Get 'down overlaps' for modes m0 and m1
         const mat_t& S_m0 = GetOverlapMatrix(m0);
         const mat_t& S_m1 = GetOverlapMatrix(m1);
         vec_t S_m0_down = vec_t(nvir0, 0.0);
         vec_t S_m1_down = vec_t(nvir1, 0.0);
         S_m0.GetOffsetRow(S_m0_down, 0, 1);
         S_m1.GetOffsetRow(S_m1_down, 0, 1);
         
         // Do contractions and save result
         S_m0_down.LeftMultiplyWithMatrix(s_ampl_mat);            // S_m0_down <- S_m0_down*s_ampl_mat
         mT2PartialDown[m1][m0].SetNewSize(nvir1, false);         // m0 down => size = nvir1
         mT2PartialDown[m1][m0] = S_m0_down;                      // Down-contracted mode last
         param_t T_mc_down = SumProdElems(S_m0_down, S_m1_down);  // T_mc_down <- S_m0_down*s_ampl_mat*S_m1_down
         mT2Down[m0][m1] = T_mc_down;
         S_m1_down.MultiplyWithMatrix(s_ampl_mat);                // S_m1_down <- s_ampl_mat*S_m1_down
         mT2PartialDown[m0][m1].SetNewSize(nvir0, false);         // m1 down => size = nvir0
         mT2PartialDown[m0][m1] = S_m1_down;                      // Down-contracted mode last
      }
   }

   /*****************************************************************//**
    * Calculate Omega00 intermediate and add to result
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcOmega00
      (
      )
   { 
      // Calculate Y00(arMcAllModes)
      const param_t y00 = WrapCalcY00Recursive(McAllModes(), McrDoubles());

      // Calculate X0
      param_t x0(1.0);

      // Check bra state type
      if (mBraType != StateType::VSCF)
      {
         // Loop over all mode pairs if bra state is non-VSCF
         for (auto it_mc = mrMcr.Begin(2), end = mrMcr.End(2); it_mc != end; ++it_mc)
         {
            // Extract mode combination and do sanity check
            const auto& mc = *it_mc;
            MidasAssert(ValidModePair(mc), "CalcOmega00: Invalid mode pair!");
            const auto m0 = mc.Mode(0);
            const auto m1 = mc.Mode(1);
            const Uin nvir0 = mrNModals[m0] - 1;
            const Uin nvir1 = mrNModals[m1] - 1;
            const Uin namps = nvir0*nvir1;

            // Get l and s amplitudes of *bra* state
            const mat_t s_ampl_mat = GetAmplMat(nvir0, nvir1, mrBraState.ClusterAmps(), mc, mrMcr);
            const mat_t l_ampl_mat = GetAmplMat(nvir0, nvir1, mrBraState.LambdaCoefs(), mc, mrMcr);

            // Subtract contraction of amplitude matrices from result
            x0 -= SumProdElemMat(s_ampl_mat, l_ampl_mat);
         }
      }

      // Add result
      const param_t omega00 = x0*y00;
      mAutoCorr += omega00;
   }


   /*****************************************************************//**
    * Calculate Omega120 intermediates and add to result
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcOmega120
      (  
      )
   { 
      // Loop over all mode pairs
      for (auto it_mc = mrMcr.Begin(2), end = mrMcr.End(2); it_mc != end; ++it_mc)
      {
         // Extract pair and do sanity check
         const auto& pair = *it_mc;
         MidasAssert(ValidModePair(pair), "CalcOmega120: Invalid mode pair!");
         const auto m0 = pair.Mode(0);
         const auto m1 = pair.Mode(1);
         const Uin nvir0 = mrNModals[m0] - 1;
         const Uin nvir1 = mrNModals[m1] - 1;
         const Uin namps = nvir0*nvir1;

         // Calculate Y00(arMcAllModes\pair) 
         ModeCombi mc_allmodes_except_two = mMcAllModes;
         mc_allmodes_except_two.RemoveMode(m0);
         mc_allmodes_except_two.RemoveMode(m1);
         const param_t y00 = WrapCalcY00Recursive(mc_allmodes_except_two, McrDoubles());

         // Get s amplitudes from *ket* state and scale by y00
         mat_t y120 = GetAmplMat(nvir0, nvir1, mrKetState.ClusterAmps(), pair, mrMcr);
         y120.Scale(y00);

         // Check number of modes (the following is non-zero for four or more modes only)
         if (mNModes > 3)
         {
            // Loop over mode pairs
            for (auto it_mc_inner = mrMcr.Begin(2), end_inner = mrMcr.End(2); it_mc_inner != end_inner; ++it_mc_inner)
            {
               // Extract pair and skip iteration if m0 and/or m1 is contained
               const auto& pair_inner = *it_mc_inner;
               if (pair_inner.IncludeMode(m0) || pair_inner.IncludeMode(m1)) {continue;}

               // Do sanity check and extract mode numbers
               MidasAssert(ValidModePair(pair_inner), "CalcOmega120: Invalid mode pair!");
               const auto m0_inner = pair_inner.Mode(0);
               const auto m1_inner = pair_inner.Mode(1);

               // Calculate Y00(arMcAllModes\pair\pair_inner) 
               ModeCombi mc_allmodes_except_four = mc_allmodes_except_two;
               mc_allmodes_except_four.RemoveMode(m0_inner);
               mc_allmodes_except_four.RemoveMode(m1_inner);
               const param_t y00 = WrapCalcY00Recursive(mc_allmodes_except_four, McrDoubles());

               // Get partially down-contracted cluster operators    
               const vec_t& ta = GetT2PartialDown(m0, m0_inner);
               const vec_t& tb = GetT2PartialDown(m1, m1_inner);
               const vec_t& tc = GetT2PartialDown(m0, m1_inner);
               const vec_t& td = GetT2PartialDown(m1, m0_inner);

               // Add to result
               for (In i = 0; i < nvir0; ++i)
               {
                  for (In j = 0; j < nvir1; ++j)
                  {
                     y120[i][j] += y00*(ta[i]*tb[j] + tc[i]*td[j]);
                  }
               }
            } /* end inner loop */
         } /* end if */

         // Get l amplitudes of the *bra* state
         const mat_t l_ampl_mat = GetAmplMat(nvir0, nvir1, mrBraState.LambdaCoefs(), pair, mrMcr);

         // Get forward overlaps as matrices
         const mat_t& S_m0 = GetOverlapMatrix(m0);
         const mat_t& S_m1 = GetOverlapMatrix(m1);
         mat_t S_m0_forward = mat_t(nvir0, nvir0, 0.0);
         mat_t S_m1_forward = mat_t(nvir1, nvir1, 0.0);
         S_m0_forward.AssignToSubMatrix(S_m0, 1, nvir0, 1, nvir0);
         S_m1_forward.AssignToSubMatrix(S_m1, 1, nvir1, 1, nvir1);

         // Do contractions sequentially (S_m1_forward is always square, so Transpose() is safe)
         S_m1_forward.Transpose();
         const mat_t tmp(S_m0_forward * y120 * S_m1_forward);
         const param_t omega120 = SumProdElemMat(l_ampl_mat, tmp);

         // Add result
         mAutoCorr += omega120; 

      } /* end outer loop */
   }

   /*****************************************************************//**
    * Calculate Omega121 and Omega122 intermediates and add to result.
    * These intermediates are bundled together so that the
    * Y00(mc_all_modes_but_three) can be reused without storage.
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcOmega121And122
      (  
      )
   { 
      // Set size of matrix and do sanity check
      MidasAssert(mNModes > 2, "CalcOmega121: I should not be called with less that three modes!");

      // Loop over all mode pairs
      for (auto it_mc = mrMcr.Begin(2), end = mrMcr.End(2); it_mc != end; ++it_mc)
      {
         // Extract pair and do sanity check
         const auto& pair = *it_mc;
         MidasAssert(ValidModePair(pair), "CalcOmega121: Invalid mode pair!");
         const auto m0 = pair.Mode(0);
         const auto m1 = pair.Mode(1);
         const Uin nvir0 = mrNModals[m0] - 1;
         const Uin nvir1 = mrNModals[m1] - 1;
         const Uin namps = nvir0*nvir1;

         // Set up vector to hold y121 and y122
         vec_t y121(nvir1, 0.0);
         vec_t y122(nvir0, 0.0);

         // Loop over all modes except m0 and m1
         ModeCombi mc_allmodes_except_two = mMcAllModes;
         mc_allmodes_except_two.RemoveMode(m0);
         mc_allmodes_except_two.RemoveMode(m1);
         for (In i = 0; i < mc_allmodes_except_two.Size(); ++i)
         {
            // Get mode number
            const auto m = mc_allmodes_except_two.Mode(i);

            // Calculate Y00(arMcAllModes\pair\m)
            ModeCombi mc_allmodes_except_three = mc_allmodes_except_two;
            mc_allmodes_except_three.RemoveMode(m);
            const param_t y00 = WrapCalcY00Recursive(mc_allmodes_except_three, McrDoubles());

            // Get partially down-contracted cluster operators
            const vec_t& t_m1up_mdown = GetT2PartialDown(m1, m);
            const vec_t& t_m0up_mdown = GetT2PartialDown(m0, m);

            // Do y121 += y00*t_m1up_mdown and y122 += y00*t_m0up_mdown
            StandardAxpy(nvir1, y00, t_m1up_mdown, 1, y121, 1);
            StandardAxpy(nvir0, y00, t_m0up_mdown, 1, y122, 1);

         } /* end inner loop */

         // Get l amplitudes of the *bra* state
         const mat_t l_ampl_mat = GetAmplMat(nvir0, nvir1, mrBraState.LambdaCoefs(), pair, mrMcr);

         // Get full overlap matrices, forward overlaps and up overlaps
         const mat_t& S_m0 = GetOverlapMatrix(m0);
         const mat_t& S_m1 = GetOverlapMatrix(m1);
         vec_t S_m0_up = vec_t(nvir0, 0.0);
         vec_t S_m1_up = vec_t(nvir1, 0.0);
         S_m0.GetOffsetCol(S_m0_up, 0, 1);
         S_m1.GetOffsetCol(S_m1_up, 0, 1);
         mat_t S_m0_forward = mat_t(nvir0, nvir0, 0.0);
         mat_t S_m1_forward = mat_t(nvir1, nvir1, 0.0);
         S_m0_forward.AssignToSubMatrix(S_m0, 1, nvir0, 1, nvir0);
         S_m1_forward.AssignToSubMatrix(S_m1, 1, nvir1, 1, nvir1);

         // Calculate Omega 121
         S_m0_up.LeftMultiplyWithMatrix(l_ampl_mat); 
         S_m0_up.LeftMultiplyWithMatrix(S_m1_forward); 
         const param_t omega121 = SumProdElems(S_m0_up, y121);

         // Calculate Omega 122 (note S_m0_forward is always square)
         S_m0_forward.Transpose();
         S_m1_up.MultiplyWithMatrix(l_ampl_mat);
         S_m1_up.MultiplyWithMatrix(S_m0_forward);
         const param_t omega122 = SumProdElems(S_m1_up, y122);

         // Add results
         mAutoCorr += omega121;
         mAutoCorr += omega122;
         
      } /* end outer loop */
   }

   /*****************************************************************//**
    * Calculate Omega1212 intermediates and add to result
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcOmega1212
      (  
      )
   { 
      // Loop over all mode pairs
      for (auto it_mc = mrMcr.Begin(2), end = mrMcr.End(2); it_mc != end; ++it_mc)
      {
         // Extract pair and do sanity check
         const auto& pair = *it_mc;
         MidasAssert(ValidModePair(pair), "CalcOmega1212: Invalid mode pair!");
         const auto m0 = pair.Mode(0);
         const auto m1 = pair.Mode(1);
         const Uin nvir0 = mrNModals[m0] - 1;
         const Uin nvir1 = mrNModals[m1] - 1;
         const Uin namps = nvir0*nvir1;

         // Calculate Y00(arMcAllModes\pair) 
         ModeCombi mc_allmodes_except_two = mMcAllModes;
         mc_allmodes_except_two.RemoveMode(m0);
         mc_allmodes_except_two.RemoveMode(m1);
         const param_t y00 = WrapCalcY00Recursive(mc_allmodes_except_two, McrDoubles());

         // Get l amplitudes of the *bra* state
         const mat_t l_ampl_mat = GetAmplMat(nvir0, nvir1, mrBraState.LambdaCoefs(), pair, mrMcr); 

         // Get 'up overlaps' for modes m0 and m1
         const mat_t& S_m0 = GetOverlapMatrix(m0);
         const mat_t& S_m1 = GetOverlapMatrix(m1);
         vec_t S_m0_up = vec_t(nvir0, 0.0);
         vec_t S_m1_up = vec_t(nvir1, 0.0);
         S_m0.GetOffsetCol(S_m0_up, 0, 1);
         S_m1.GetOffsetCol(S_m1_up, 0, 1);
         
         // Do contractions and add result
         S_m0_up.LeftMultiplyWithMatrix(l_ampl_mat);
         const param_t contraction = SumProdElems(S_m0_up, S_m1_up);
         const param_t omega1212 = contraction*y00;
         mAutoCorr += omega1212;
      }
   }


   /*****************************************************************//**
    * Product of passive overlaps for a given set of modes
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::param_t
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::GetPassiveOverlapProd
      (  const ModeCombi& arMc
      )  const
   {  
      LOGCALL("get passive overlap product");

      param_t result(1.0);
      for (const auto mode : arMc.MCVec()) 
      {
         result *= mPassiveOverlaps[mode];
      }
      return result;
   }



   /*****************************************************************//**
    * Get fully down-contracted T2 operator for a two-mode mode 
    * combination
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::param_t
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::GetT2Down
      (  const ModeCombi& arMc
      )  const
   {  
      LOGCALL("get T2Down");
      MidasAssert(ValidModePair(arMc), "GetT2Down: Invalid mode pair!");
      const auto m0 = arMc.Mode(0);
      const auto m1 = arMc.Mode(1);
      return mT2Down[m0][m1];
   }


   /*****************************************************************//**
    * Get partially down-contracted T2 operator
    * 
    * @param[in] aM0   The mode which *is not* down-contracted
    * @param[in] aM1   The mode which *is* down-contracted
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   const typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::vec_t&
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::GetT2PartialDown
      (  const Uin aM0
      ,  const Uin aM1
      )  const
   {  
      LOGCALL("get T2PartialDown");
      MidasAssert(aM0 != aM1, "GetT2PartialDown: Invalid mode pair!");
      return mT2PartialDown[aM0][aM1];
   }

   /*****************************************************************//**
    * Get overlap matrix for a given mode
    * 
    * @param[in] aM   A mode index
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   const typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::mat_t&
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::GetOverlapMatrix
      (  const Uin aM
      )  const
   {  
      LOGCALL("get overlap mat");
      return mOverlapMatrices[aM];
   }


   /************************************************************************//**
    * Dummy function to test the combinatorics of CalcY00RecursiveDummy. 
    * First call should have aI = -1, aLevel = 0, arNumCalls = 0.
    * 
    * @param[in] aMc           The mode combination for which to calculate Y00.
    * @param[in] aI            Index to avoid overcounting.
    * @param[in] aLevel        Level of recursive call.
    * @param[in] arNumCalls    Total number of calls.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcY00RecursiveDummy
         (  ModeCombi aMc
         ,  const In aI
         ,  const Uin aLevel
         ,  Uin& arNumCalls
         )
   {  
      // Increment call counter
      arNumCalls++;

      // Loop over pairs
      for (In j = aI + 1; j < McrDoubles().Size(); ++j) 
      {  
         const ModeCombi& pair = McrDoubles().GetModeCombi(j);
         if (aMc.Contains(pair)) 
         {
            // Print nice recursion tree
            std::string spacer(aLevel, ' ');
            Mout << spacer << spacer << pair.MCVec() << std::endl;

            // Remove the modes that were used
            ModeCombi mc_reduced = aMc;
            mc_reduced.RemoveMode(pair.Mode(0));
            mc_reduced.RemoveMode(pair.Mode(1));

            // Make recursive call
            CalcY00RecursiveDummy(mc_reduced, j, aLevel + 1, arNumCalls);
         }
      }
   }


   /***************************************************************************//**
    * Recursively calculate the intermediate Y00 for a given mode combination. 
    * First call should have aI = -1, aX = 1, arY = 0, aLevel = 0, arNumCalls = 0.
    * This version uses a simple truncation scheme.
    * 
    * @param[in] aMc           The mode combination for which to calculate Y00.
    * @param[in] aI            Index to avoid overcounting.
    * @param[in] aX            'Local' variable that holds a product of mT2Down%s.
    * @param[in] arY           Variable that (eventually) holds the result
    * @param[in] aLevel        Level of recursive call.
    * @param[in] arNumCalls    Total number of calls.
    * @param[in] arNumTerms    Stores number of terms according to level.
    * @param[in] arSumTerms    Stores aggregate terms according to level.
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcY00RecursiveTrunc
         (  const ModeCombi aMc
         ,  const In aI
         ,  const param_t aX
         ,  param_t& arY
         ,  const Uin aLevel
         ,  Uin& arNumCalls
         ,  std::vector<Uin>& arNumTerms
         ,  std::vector<param_t>& arSumTerms
         )
   {  
      // Increment call counter and add contribution to result
      const param_t term = aX*GetPassiveOverlapProd(aMc);
      arNumTerms.at(aLevel)++;
      arSumTerms.at(aLevel) += term;
      arNumCalls++;
      arY += term;

      // Loop over pairs
      if (mMaxLevel == -1 || aLevel < mMaxLevel) 
      {
         for (In j = aI + 1; j < McrDoubles().Size(); ++j) 
         {  
            const ModeCombi& pair = McrDoubles().GetModeCombi(j);
            if (aMc.Contains(pair))
            {
               // Define local intermediate and remove pair from mode combination
               const param_t x_local = aX*GetT2Down(pair); 
               ModeCombi mc_reduced = aMc;
               mc_reduced.RemoveMode(pair.Mode(0));
               mc_reduced.RemoveMode(pair.Mode(1));

               // Make recursive call
               CalcY00RecursiveTrunc(mc_reduced, j, x_local, arY, aLevel + 1, arNumCalls, arNumTerms, arSumTerms);
            }
         }
      }
   }

   /***************************************************************************//**
    * Recursively calculate the intermediate Y00 for a given mode combination. 
    * First call should have aI = -1, aX = 1, arY = 0, aLevel = 0, arNumCalls = 0.
    * This version uses a simple screening scheme. In addition to screening 
    * according to the supplied threshold, the function bins the terms so that the 
    * results corresponding to a number of looser thresholds can be obtained. 
    * This allows the user to monitor convergence with respect to tightening the
    * threshold
    * 
    * @param[in] aMc            The mode combination for which to calculate Y00.
    * @param[in] aI             Index to avoid overcounting.
    * @param[in] aX             'Local' variable that holds a product of mT2Down%s.
    * @param[in] arY            Variable that (eventually) holds the result
    * @param[in] aLevel         Level of recursive call.
    * @param[in] arNumCalls     Total number of calls.
    * @param[in] arNumTerms     Stores number of terms according to threshold.
    * @param[in] arSumTerms     Stores aggregate terms according to threshold.
    * @param[in] arThreshold    Threshold for including further terms.
    * @param[in] arLogThreshold Base 10 logarithm of arThreshold.
    * @param[in] arLogDelta     Base 10 logarithm of threshold spacing.
    * @param[in] arNumBins      Number of thresholds/bins.
    * @param[in] aOldBin        Bin number from previous call.
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::CalcY00RecursiveScreen
         (  const ModeCombi aMc
         ,  const In aI
         ,  const param_t aX
         ,  param_t& arY
         ,  const Uin aLevel
         ,  Uin& arNumCalls
         ,  std::vector<Uin>& arNumTerms
         ,  std::vector<param_t>& arSumTerms
         ,  const real_t& arThreshold
         ,  const real_t& arLogThreshold
         ,  const real_t& arLogDelta
         ,  const Uin& arNumBins
         ,  const In aOldBin
         )
   {  
      // Calculate term and save stats
      const param_t term = aX*GetPassiveOverlapProd(aMc);
      In nbin = static_cast<In>(std::ceil((std::log10(std::abs(term)) - arLogThreshold)/arLogDelta));
      if (nbin < 0) 
      {
         nbin = 0;
      }
      else if (nbin > arNumBins) 
      {
         nbin = arNumBins; 
      }
      nbin = std::min(nbin, aOldBin);
      arNumTerms.at(aOldBin - 1)++;
      arSumTerms.at(aOldBin - 1) += term;

      // Increment call counter and add contribution to result
      arNumCalls++;
      arY += term;

      // Loop over pairs
      if (std::abs(term) > arThreshold) 
      {
         for (In j = aI + 1; j < McrDoubles().Size(); ++j) 
         {  
            const ModeCombi& pair = McrDoubles().GetModeCombi(j);
            if (aMc.Contains(pair))
            {
               // Define local intermediate and remove pair from mode combination
               const param_t x_local = aX*GetT2Down(pair); 
               ModeCombi mc_reduced = aMc;
               mc_reduced.RemoveMode(pair.Mode(0));
               mc_reduced.RemoveMode(pair.Mode(1));

               // Make recursive call
               CalcY00RecursiveScreen(mc_reduced, j, x_local, arY, aLevel + 1, arNumCalls, arNumTerms, arSumTerms, arThreshold, arLogThreshold, arLogDelta, arNumBins, nbin);
            }
         }
      }
   }

   /***************************************************************************//**
    * Wrapper for CalcY00Recursive. The purpose is to hide the setup of
    * auxiliary variables and to gather the processing of stats in one place.
    * 
    * @param[in] arMc          The mode combination for which to calculate Y00.
    * @param[in] arMcrDoubles  The set of all two-mode mode combinations.
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::param_t
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::WrapCalcY00Recursive
         (  const ModeCombi& arMc
         ,  const ModeCombiOpRange& arMcrDoubles
         )
   {  
      LOGCALL("wrap calc Y00 recursive");
      param_t x = 1;
      param_t y00 = 0;
      Uin numcalls = 0;

      // Prepare data collection
      const Uin nmodes = arMc.Size();
      Uin numbins;
      if      (mAutoCorrAlgo == AutoCorrAlgo::FULL)     
      {
         numbins = nmodes/2 + 1;
      }
      else if (mAutoCorrAlgo == AutoCorrAlgo::TRUNCATE) 
      {
         numbins = mMaxLevel + 1;
      }
      else if (mAutoCorrAlgo == AutoCorrAlgo::SCREEN)   
      {
         numbins = mNumThres;
      }
      else
      {  
         MIDASERROR("I don't know what to do for AutoCorrAlgo " + StringFromEnum(mAutoCorrAlgo) + "!");
      }
      std::vector<Uin>     num_terms(numbins, 0);
      std::vector<Uin>     partial_sum_terms(numbins , 0);
      std::vector<real_t>  frac_num_terms(numbins , 0);
      std::vector<param_t> sum_terms(numbins, 0);
      std::vector<param_t> partial_sums(numbins , 0);
      std::vector<param_t> marginal_change(numbins , 0);
   

      // Select correct version
      if (mKetType == StateType::VSCF)
      {
         y00 = GetPassiveOverlapProd(arMc);
         numcalls++;
      }  
      else if (mAutoCorrAlgo == AutoCorrAlgo::FULL)
      {
         if (mMaxLevel != -1) 
         {
            MIDASERROR("mMaxLevel must be -1 when using AutoCorrAlgo::FULL!");
         }
         CalcY00RecursiveTrunc(arMc, -1, x, y00, 0, numcalls, num_terms, sum_terms);
      }
      else if (mAutoCorrAlgo == AutoCorrAlgo::TRUNCATE)
      {
         CalcY00RecursiveTrunc(arMc, -1, x, y00, 0, numcalls, num_terms, sum_terms);
      }
      else if (mAutoCorrAlgo == AutoCorrAlgo::SCREEN)
      {
         const real_t local_threshold     = mThreshold*std::abs(GetPassiveOverlapProd(arMc));
         const real_t log_local_threshold = std::log10(local_threshold);
         const real_t log_delta           = std::log10(mDeltaThres); 
         const Uin    oldbin              = numbins;
         if (mVerbose)
         {
            Mout << "local_threshold = " << local_threshold << std::endl;
         }
         CalcY00RecursiveScreen(arMc, -1, x, y00, 0, numcalls, num_terms, sum_terms, local_threshold, log_local_threshold, log_delta, numbins, oldbin);
         std::reverse(num_terms.begin(), num_terms.end());
         std::reverse(sum_terms.begin(), sum_terms.end());
      }

      if (mVerbose)
      {
         // Calculate partial sums and marginal changes
         std::partial_sum(sum_terms.begin(), sum_terms.end(), partial_sums.begin());
         std::partial_sum(num_terms.begin(), num_terms.end(), partial_sum_terms.begin());
         for (auto i = 0; i < numbins; ++i) marginal_change.at(i) = sum_terms.at(i)/partial_sums.at(i);
         for (auto i = 0; i < numbins; ++i) frac_num_terms.at(i) = real_t(partial_sum_terms.at(i))/FullNumTerms(nmodes);
         
         // Print some stats
         Mout << "num_terms = " << std::endl;
         PrintVector(Mout, num_terms, false);
         Mout << "tot_num_terms  = " << std::accumulate(num_terms.begin(), num_terms.end(), (long long unsigned int)0) << std::endl;
         Mout << "numcalls       = " << numcalls << std::endl;
         Mout << "full_num_terms = " << FullNumTerms(nmodes) << std::endl << std::endl;

         Mout << "partial_sum_terms = " << std::endl;
         PrintVector(Mout, partial_sum_terms, false);
         Mout << std::endl;

         Mout << "partial_sum_terms/full_num_terms = " << std::endl;
         PrintVector(Mout, frac_num_terms, false);
         Mout << std::endl;

         Mout << "sum_terms = " << std::endl;
         PrintVector(Mout, sum_terms, true);
         Mout << "tot_sum_terms = " << std::accumulate(sum_terms.begin(), sum_terms.end(), param_t(0)) << std::endl;
         Mout << "y00           = " << y00 << std::endl << std::endl;
         
         Mout << "partial_sums = " << std::endl;
         PrintVector(Mout, partial_sums, true);
         Mout << std::endl;

         Mout << "sum_terms[i]/partial_sums[i] = " << std::endl;
         PrintVector(Mout, marginal_change, true);
         Mout << std::endl;

         Mout << "modes = " << nmodes << ", threshold = " << mThreshold << ", result = " << y00 << ", no. terms = " << numcalls << std::endl;
      }

      // Return result
      return y00;
   }

   /****************************************************************************//**
    * Get amplitudes for a given two-mode mode combination as matrix
    * 
    * @param[in] aNrows        Number of rows (number of virtuals for first mode)
    * @param[in] aNcols        Number of colums (number of virtuals for second mode)
    * @param[in] arAmpl        Amplitude container
    * @param[in] arMcModePair  A two-mode mode combination
    * @param[in] arMcr         Mode combination range describing the state
    *******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::mat_t
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::GetAmplMat
      (  const Uin aNrows
      ,  const Uin aNcols
      ,  const CONT_TMPL<PARAM_T>& arAmpl
      ,  const ModeCombi& arMcModePair
      ,  const ModeCombiOpRange& arMcr
      )  
   {       
      LOGCALL("get ampl mat");
      if constexpr (std::is_same_v<GeneralTensorDataCont<PARAM_T>, CONT_TMPL<PARAM_T> >)
      {
         MIDASERROR("Amplitudes should not be saved in TensorDataCont!");
         return mat_t();
      }
      else if constexpr (std::is_same_v<GeneralMidasVector<PARAM_T>, CONT_TMPL<PARAM_T> >)
      {
         MidasAssert(ValidModePair(arMcModePair), "GetAmplMat: Invalid mode pair!");
         const_iterator it_mc;
         arMcr.Find(arMcModePair, it_mc);
         const auto addr = it_mc->Address(); 
         vec_t ampl = arAmpl.GetSubVec(addr, addr + aNrows*aNcols - 1);
         mat_t ampl_mat;
         ampl_mat.SetNewSize(aNrows, aNcols, false, false);
         ampl_mat.MatrixFromVector(ampl, true);
         return ampl_mat;
      }
      else if constexpr (std::is_same_v<GeneralDataCont<PARAM_T>, CONT_TMPL<PARAM_T> >)
      {  
         MidasAssert(ValidModePair(arMcModePair), "GetAmplMat: Invalid mode pair!");
         const_iterator it_mc;
         arMcr.Find(arMcModePair, it_mc);
         const auto addr = it_mc->Address(); 
         vec_t ampl;
         ampl.SetNewSize(aNrows*aNcols, false);
         arAmpl.DataIo(IO_GET, ampl, aNrows*aNcols, addr);
         mat_t ampl_mat;
         ampl_mat.SetNewSize(aNrows, aNcols, false, false);
         ampl_mat.MatrixFromVector(ampl, true);
         return ampl_mat;
      }
      else
      {
         MIDASERROR("I don't know what to do for this kind of amplitude container!");
         return mat_t();
      }
   }


   /*************************************************************************//**
    * Extracts all doubles from a mode combination range into a new 
    * mode combination range.
    ****************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   ModeCombiOpRange AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::ExtractDoubles
      (  const ModeCombiOpRange& arMcr
      )
   {
      LOGCALL("extract doubles");
      // Loop over all two-mode mode combinations and add them to vector
      std::vector<ModeCombi> vec_doubles;
      for (auto it_mc = arMcr.Begin(2), end = arMcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         vec_doubles.emplace_back(mc);
      }

      // Construct new mode combination range using only doubles
      ModeCombiOpRange mcr_doubles;
      mcr_doubles.Insert(vec_doubles);
      
      // Return result
      return mcr_doubles;
   }

   /*****************************************************************//**
    * Calculates sum over element-wise product of two matrices
    ********************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::param_t
   AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::SumProdElemMat
      (  const mat_t& arMat1
      ,  const mat_t& arMat2
      )  
   { 
      LOGCALL("sum prod elem mat");
      
      // Sanity check
      const In nrows = arMat1.Nrows();
      const In ncols = arMat1.Ncols();
      if (ncols != arMat2.Ncols() || nrows != arMat2.Nrows())
      {
         MIDASERROR("SumProdElemMat: Dimensions of matrices mismatch!");
      }

      // Loop over elements
      param_t sum = 0;
      for (In irow = I_0; irow < nrows; ++irow)
      {
         for (In icol = I_0; icol < ncols; ++icol)
         {
            sum += arMat1[irow][icol]*arMat2[irow][icol];
         }
      }

      // Return result
      return sum;
   }


   /****************************************************************************//**
    * Check that a mode combination is a valid mode pair, i.e. (m0, m1) with m0 < m1
    *******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   bool AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::ValidModePair
      (  const ModeCombi& arMc
      )  const  
   { 
      return (arMc.Size() == I_2) && (arMc.Mode(0) < arMc.Mode(1));
   }


   /****************************************************************************//**
    * Overload disambiguation of std::abs for PrintVector 
    * (this should not be necessary?)
    *******************************************************************************/
   namespace util 
   {
      template<typename T>
      auto Abs(T value) -> std::enable_if_t<std::is_same_v<T, Nb>, T> { return std::fabs(value); }

      template<typename T>
      auto Abs(T value) -> std::enable_if_t<std::is_same_v<T, std::complex<Nb>>, T> { return std::abs(value); }
      
      template<typename T>
      auto Abs(T value) -> std::enable_if_t<std::is_same_v<T, In>, T> { return std::abs(value); }

      template<typename T>
      auto Abs(T value) -> std::enable_if_t<std::is_same_v<T, Uin>, T> { return value; }
   } /* namespace util */

   /****************************************************************************//**
    * Print a std::vector in a nice way
    *******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   template
      <  typename T 
      >
   void AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::PrintVector
      (  std::ostream& arOS
      ,  const std::vector<T>& arVector
      ,  bool aPrintAbs
      )  const  
   {
      const auto old_flags = arOS.flags();

      arOS << std::setprecision(16) << std::scientific;

      if constexpr (std::is_same_v<T, std::complex<real_t>>) 
      {
         for (In i = 0; i < arVector.size(); ++i)
         {
            arOS << i << std::showpos << "  (" << arVector.at(i).real() << "," << arVector.at(i).imag() << ")";
            arOS << std::noshowpos;
            if (aPrintAbs) arOS << "  " << util::Abs(arVector.at(i)).real();
            arOS << std::endl;
         }
      }
      else if constexpr (std::is_same_v<T, Uin>) 
      {
         for (In i = 0; i < arVector.size(); ++i)
         {
            arOS << i << "  " << std::setw(15) << std::right << arVector.at(i);
            if (aPrintAbs) arOS << "  " << util::Abs(arVector.at(i));
            arOS << std::endl;
         }
      }
      else
      {
         for (In i = 0; i < arVector.size(); ++i)
         {
            arOS << i << "  " << arVector.at(i);
            if (aPrintAbs) arOS << "  " << util::Abs(arVector.at(i));
            arOS << std::endl;
         }
      }

      arOS.flags(old_flags);
   }

   /****************************************************************************//**
    * Calculate the full number of terms in Y00 for a given number of modes.
    * The equations used are somewhat awkward. They are used to avoid overflow 
    * issues.
    *******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   long double AutoCorrTwoModeMachine<PARAM_T, CONT_TMPL>::FullNumTerms
      (  const Uin aNModes
      )  const  
   { 
      // Check size of input
      if (aNModes > 30) MidasWarning("FullNumTerms might overflow for large input!");

      // Calculate result
      long double result = 0;
      const Uin nlevels = aNModes/2 + 1;
      for (In p = 0; p < nlevels; ++p)
      {
         long double prod = 1;
         for (In k = 0; k < p; ++k) 
         {
            prod *= boost::math::binomial_coefficient<long double>(aNModes - 2*k, 2)/(p - k);
         }
         result += prod;
      }
      return result;
   }

}  /* namespace midas::tdvcc */
