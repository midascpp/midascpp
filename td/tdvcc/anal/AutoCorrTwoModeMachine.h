/**
 *******************************************************************************
 * 
 * @file    AutoCorrTwoModeMachine.h
 * @date    10-04-2021
 * @author  Andreas Buchgraitz Jensen
 * @author  Mads Greisen Højlund
 *
 * @brief   Class for calculating TDMVCC[2] autocorrelation function.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef AUTOCORRTWOMODEMACHINE_H_INCLUDED
#define AUTOCORRTWOMODEMACHINE_H_INCLUDED

#include "td/tdvcc/anal/AutoCorrTwoModeMachine_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/anal/AutoCorrTwoModeMachine_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif /*AUTOCORRTWOMODEMACHINE_H_INCLUDED*/
