/**
 *******************************************************************************
 * 
 * @file    FvciNorm2_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_FVCINORM2_DECL_H_INCLUDED
#define TDVCC_FVCINORM2_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"
#include "vcc/TensorDataCont.h"

// Forward declarations.
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;
template<typename, template<typename> class> class ParamsTdvcc;
template<typename, template<typename> class> class ParamsTdvci;
template<typename, template<typename> class> class ParamsTdextvcc;
template<typename, template<typename> class> class ParamsTdmvcc;

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      );

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      );

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralMidasVector>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralTensorDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralTensorDataCont>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralDataCont>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );


} /* namespace midas::tdvcc */

#endif/*TDVCC_FVCINORM2_DECL_H_INCLUDED*/
