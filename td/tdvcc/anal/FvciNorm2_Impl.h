/**
 *******************************************************************************
 * 
 * @file    FvciNorm2_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_FVCINORM2_IMPL_H_INCLUDED
#define TDVCC_FVCINORM2_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "input/ModeCombiOpRange.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepTransformers.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      )
   {
      return Norm2(arState);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return FvciNorm2Ket(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertKetToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertKetToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertKetToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      )
   {
      return FvciNorm2Ket(arState);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return FvciNorm2Bra(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertBraToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertBraToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertBraToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         auto nmodes = arNModals.size();
         ModeCombiOpRange full_mcr(nmodes, nmodes);

         CONT_TMPL<PARAM_T> fvci(full_mcr, arNModals, true);

         for(In imc=I_0; imc<arMcr.Size(); ++imc)
         {
            const auto& mc_in = arMcr.GetModeCombi(imc);
            In idx = -I_1;
            ModeCombiOpRange::const_iterator it_mc;
            if (  !full_mcr.Find(mc_in, it_mc, idx)
               )
            {
               MIDASERROR("MC not found in full MCR!");
            }
            
            fvci.GetModeCombiData(idx) = arState.Coefs().GetModeCombiData(imc);
         }

         return fvci;
      }
      else
      {
         const auto fvci_size = midas::util::matrep::Product(arNModals);
         CONT_TMPL<PARAM_T> fvci(arState.Coefs());
         std::vector<PARAM_T> zeros(fvci_size - fvci.Size(), PARAM_T(0));
         fvci.Insert(fvci.Size(), zeros.begin(), zeros.end());
         return fvci;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralMidasVector>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return midas::util::matrep::ExpTRef(arNModals, arMcr, arState.ClusterAmps(), arNModals.size());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralTensorDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralTensorDataCont>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      auto dc = GeneralDataCont<PARAM_T>(midas::util::matrep::ExpTRef(arNModals, arMcr, *(DataContFromTensorDataCont(arState.ClusterAmps()).GetVector()), arNModals.size()));

      // ModeCombiOpRange passed to GeneralTensorDataCont constructor
      // must be untruncated to keep FVCI in full space.
      ModeCombiOpRange mcr(arNModals.size(), arNModals.size());
      SetAddresses(mcr, arNModals);
      return GeneralTensorDataCont<PARAM_T>(dc, mcr, arNModals, true);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTdvcc<PARAM_T, GeneralDataCont>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::ExpTRef;
      GeneralMidasVector<PARAM_T> tmp_mv;
      const GeneralMidasVector<PARAM_T>* p_mv = nullptr;
      const auto& t_amps = arState.ClusterAmps();
      if (t_amps.InMem())
      {
         p_mv = t_amps.GetVector();
      }
      else
      {
         tmp_mv.SetNewSize(t_amps.Size());
         t_amps.DataIo(IO_GET, tmp_mv, tmp_mv.Size());
         p_mv = &tmp_mv;
      }
      return GeneralDataCont<PARAM_T>(ExpTRef(arNModals, arMcr, *p_mv, arNModals.size()));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::ExpTRef;
      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         auto dc = GeneralDataCont<PARAM_T>(ExpTRef(arNModals, arMcr, *(DataContFromTensorDataCont(arState.ClusterAmps()).GetVector()), arNModals.size()));
         return GeneralTensorDataCont<PARAM_T>(dc, arMcr, arNModals, true);
      }
      else
      {
         return CONT_TMPL<PARAM_T>(ExpTRef(arNModals, arMcr, arState.ClusterAmps(), arNModals.size()));
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      //using midas::util::matrep::ExpTRef;
      //return CONT_TMPL<PARAM_T>(ExpTRef(arNModals, arMcr, arState.ClusterAmps(), arNModals.size()));
      MidasWarning("TDMVCC ket to FVCI not implemented!");
      return CONT_TMPL<PARAM_T>();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      auto ket = ConvertKetToFvci(arState, arNModals, arMcr);

      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralDataCont<PARAM_T>>
                     )
      {
         return CONT_TMPL<PARAM_T>(ket.GetVector()->Conjugate());
      }
      else
      {
         return ket.Conjugate();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         auto dc_result = GeneralDataCont<PARAM_T>
                           (  midas::util::matrep::Ref1pLExpmT
                              (  arNModals
                              ,  arMcr
                              ,  DataContFromTensorDataCont(arState.ClusterAmps())
                              ,  DataContFromTensorDataCont(arState.LambdaCoefs())
                              ,  arNModals.size()
                              )
                           );

         // ModeCombiOpRange for new ctor must be untruncated to
         // keep FVCI in full space.
         ModeCombiOpRange mcr(arNModals.size(), arNModals.size());
         SetAddresses(mcr, arNModals);
         return CONT_TMPL<PARAM_T>(dc_result, mcr, arNModals, true);
      }
      else
      {
         return CONT_TMPL<PARAM_T>
            (  midas::util::matrep::Ref1pLExpmT
               (  arNModals
               ,  arMcr
               ,  arState.ClusterAmps()
               ,  arState.LambdaCoefs()
               ,  arNModals.size()
               )
            );
      }
   }
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                     )
      {
         auto dc_result = GeneralDataCont<PARAM_T>
                           (  midas::util::matrep::RefExpSExpmT
                              (  arNModals
                              ,  arMcr
                              ,  DataContFromTensorDataCont(arState.ClusterAmps())
                              ,  DataContFromTensorDataCont(arState.ExtAmps())
                              ,  arNModals.size()
                              )
                           );

         // ModeCombiOpRange for new ctor must be untruncated to
         // keep FVCI in full space.
         ModeCombiOpRange mcr(arNModals.size(), arNModals.size());
         SetAddresses(mcr, arNModals);
         return CONT_TMPL<PARAM_T>(dc_result, mcr, arNModals, true);
      }
      else
      {
         return CONT_TMPL<PARAM_T>
            (  midas::util::matrep::RefExpSExpmT
               (  arNModals
               ,  arMcr
               ,  arState.ClusterAmps()
               ,  arState.ExtAmps()
               ,  arNModals.size()
               )
            );
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTdmvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      MidasWarning("TDMVCC bra to FVCI not implemented!");
      return CONT_TMPL<PARAM_T>();
   }

} /* namespace midas::tdvcc */

#endif/*TDVCC_FVCINORM2_IMPL_H_INCLUDED*/
