/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccBase_ctor_Impl.h
 * @date    22-11-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Moving template constructors to separate file, because explicit instantiation 
 *    does not work with clang++.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCC_CTOR_IMPL_H_INCLUDED
#define PARAMSTIMTDVCC_CTOR_IMPL_H_INCLUDED

#include "ParamsTdvccBase_Decl.h"

namespace midas::tdvcc
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
template
   <  typename U
   >
ParamsTdvccBase<PARAM_T,CONT_TMPL>::ParamsTdvccBase
   (  Uin aSize
   ,  typename std::enable_if_t<!std::is_same_v<U, GeneralTensorDataCont<PARAM_T>>>* apEnableIf
   )
   :  mPhase(0)
   ,  mPrimalParams(aSize, param_t(0))
   ,  mDualParams(aSize, param_t(0))
{
   SanityCheck();
}


/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
template
   <  typename U
   >
ParamsTdvccBase<PARAM_T,CONT_TMPL>::ParamsTdvccBase
   (  const ModeCombiOpRange& aMcr
   ,  const std::vector<Uin>& aNModals
   ,  typename std::enable_if_t<std::is_same_v<U, GeneralTensorDataCont<PARAM_T>>>* apEnableIf
   )
   :  mPhase(0)
   ,  mPrimalParams(aMcr, aNModals, true)
   ,  mDualParams(aMcr, aNModals, true)
{
   SanityCheck();
}

} /* namespace midas::tdvcc */

#endif/*PARAMSTIMTDVCC_CTOR_IMPL_H_INCLUDED*/
