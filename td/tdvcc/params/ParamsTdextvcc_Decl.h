/**
 *******************************************************************************
 * 
 * @file    ParamsTdextvcc_Decl.h
 * @date    19-12-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDEXTVCC_DECL_H_INCLUDED
#define PARAMSTDEXTVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/params/ParamsTdvccBase.h"

// Forward declarations.

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Holds the parameters \f$ \epsilon \f$ (phase/energy), \f$ \{t_\mu\},
    *    \{\sigma_\mu\} \f$, relevant for a TIM TD ExtVCC parametrization.
    *
    * Inherits most functionality from base class, but allows for
    * specializations and function overloads that are specific for
    * Extended VCC.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTdextvcc
      :  public ParamsTdvccBase<PARAM_T,CONT_TMPL>
   {
      public:
         using param_t = PARAM_T;
         using absval_t = typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr CorrType corr_type = CorrType::EXTVCC;

         ParamsTdextvcc(const ParamsTdextvcc&) = default;
         ParamsTdextvcc(ParamsTdextvcc&&) = default;
         ParamsTdextvcc& operator=(const ParamsTdextvcc&) = default;
         ParamsTdextvcc& operator=(ParamsTdextvcc&&) = default;
         ~ParamsTdextvcc() = default;

         //! Forward all constructor calls to base class.
         template<class... Args>
         ParamsTdextvcc(Args&&... args)
            :  ParamsTdvccBase<PARAM_T,CONT_TMPL>(std::forward<Args>(args)...)
         {
         }

         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //@{
         //! The stored parameters.
         const cont_t& ClusterAmps() const {return this->PrimalParams();}
         const cont_t& ExtAmps() const {return this->DualParams();}
         //@}

   };

} /* namespace midas::tdvcc */


#endif/*PARAMSTDEXTVCC_DECL_H_INCLUDED*/
