/**
 *******************************************************************************
 * 
 * @file    ParamsTdextvcc.h
 * @date    19-12-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDEXTVCC_H_INCLUDED
#define PARAMSTDEXTVCC_H_INCLUDED

#include "td/tdvcc/params/ParamsTdextvcc_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "td/tdvcc/params/ParamsTdextvcc_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*PARAMSTDEXTVCC_H_INCLUDED*/
