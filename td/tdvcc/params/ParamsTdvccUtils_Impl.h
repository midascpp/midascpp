/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccUtils_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCCUTILS_IMPL_H_INCLUDED
#define PARAMSTDVCCUTILS_IMPL_H_INCLUDED

// Midas headers.
#include "util/Error.h"
#include "input/ModeCombiOpRange.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename OUTPUT_T
      ,  typename INPUT_T
      ,  typename PARAM_T
      ,  bool FATAL
      >
   OUTPUT_T ExtractFromMap
      (  std::map<ParamID,libmda::util::any_type>&& arMap
      ,  const ModeCombiOpRange& aMcr
      ,  const std::vector<Uin> aNModals
      ,  ParamID aKey
      )
   {
      try
      {
         if constexpr   (  std::is_same_v<OUTPUT_T, GeneralTensorDataCont<PARAM_T>>
                        )
         {
            GeneralDataCont<PARAM_T> dc(std::move(arMap.at(aKey).template get<INPUT_T>()));
            return OUTPUT_T(dc, aMcr, aNModals, true);
         }
         else
         {
            return OUTPUT_T(std::move(arMap.at(aKey).template get<INPUT_T>()));
         }
      }
      catch(const std::out_of_range& oor)
      {
         if constexpr(FATAL)
         {
            MIDASERROR("Couldn't find key '"+StringFromEnum(aKey)+"' in map of parameters.");
            return OUTPUT_T();
         }
         else
         {
            throw oor;
         }
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   PARAM_T ZeroFirstElemAndReturnIt
      (  CONT_TMPL<PARAM_T>& arCont
      )
   {
      PARAM_T val = 0;
      if (arCont.Size() < 1)
      {
         MIDASERROR("arCont.Size() = "+std::to_string(arCont.Size())+", which is < 1.");
      }
      if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
      {
         arCont.DataIo(IO_GET, I_0, val);
         arCont.DataIo(IO_PUT, I_0, PARAM_T(0));
      }
      else if constexpr (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                        )
      {
         val = arCont.GetModeCombiData(I_0).GetScalar();
         arCont.GetModeCombiData(I_0).Zero();
      }
      else
      {
         val = arCont[I_0];
         arCont[I_0] = 0;
      }
      return val;
   }


   /************************************************************************//**
    * @brief
    *    Zero one-mode amplitudes
    * 
    * @param[in,out] arVec    Vector of amplitudes
    * @param[in] aMcr         ModeCombiOpRange (@note Must have addresses set!)
    * @return
    *    Vector of norms of zeroed one-mode amplitudes (one norm per mode)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename CONT_TMPL
      >
   std::vector<midas::type_traits::RealTypeT<PARAM_T>> ZeroOneModeAmplitudes
      (  CONT_TMPL<PARAM_T>& arVec
      ,  const ModeCombiOpRange& aMcr
      )
   {
      using absval_t = midas::type_traits::RealTypeT<PARAM_T>;
      constexpr bool is_midasvector = std::is_same_v<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>>;
      [[maybe_unused]] constexpr bool is_datacont = std::is_same_v<CONT_TMPL<PARAM_T>, GeneralDataCont<PARAM_T>>;

      auto nmodes = aMcr.NumberOfModes();
      std::vector<absval_t> norms;
      norms.reserve(nmodes);

      for(auto it = aMcr.Begin(1), end = aMcr.End(1); it != end; ++it)
      {
         auto addr = it->Address();
         auto mode = it->Mode(0);
         auto nextit = it;
         ++nextit;
         auto len = nextit->Address() - addr;

         if constexpr   (  is_midasvector
                        )
         {
            // Calculate norms
            norms.emplace_back(std::sqrt(arVec.Norm2(addr, len)));

            // Zero parameters
            for(In i=I_0; i<len; ++i)
            {
               arVec[addr+i] = PARAM_T(0.0);
            }
         }
         else if constexpr (  is_datacont
                           )
         {
            // Calculate norms
            norms.emplace_back(std::sqrt(arVec.Norm2(addr, len)));

            GeneralMidasVector<PARAM_T> dummy_vec;
            arVec.DataIo(IO_PUT, dummy_vec, len, addr, 1, 0, 1, true, PARAM_T(0.0));
         }
         else
         {
            MIDASERROR("Only implemented for MidasVector and DataCont!");
         }
      }

      return norms;
   }


} /* namespace midas::tdvcc */


#endif/*PARAMSTDVCCUTILS_IMPL_H_INCLUDED*/
