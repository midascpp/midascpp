/**
 *******************************************************************************
 * 
 * @file    ParamsTdvci_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCI_IMPL_H_INCLUDED
#define PARAMSTDVCI_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTdvccUtils.h"

namespace midas::tdvcc
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTdvci<PARAM_T,CONT_TMPL>::ParamsTdvci
   (  cont_t aCoefs
   )
   :  mCoefs(std::move(aCoefs))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTdvci<PARAM_T,CONT_TMPL>::ParamsTdvci
   (  std::map<ParamID,libmda::util::any_type>&& arMap
   ,  const ModeCombiOpRange& aMcr
   ,  const std::vector<Uin>& aNModals
   )
   :  mCoefs(ExtractFromMap<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>, PARAM_T>(std::move(arMap), aMcr, aNModals, ParamID::KET))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTdvci<PARAM_T,CONT_TMPL>::Size
   (
   )  const
{
   return ::Size(mCoefs);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTdvci<PARAM_T,CONT_TMPL>::SizeTotal
   (  Uin aSizeParams
   )
{
   return num_phases + num_param_conts*aSizeParams;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const std::set<ParamID>& ParamsTdvci<PARAM_T,CONT_TMPL>::GetParamIDs
   (
   )
{
   static std::set<ParamID> s_param_ids =
      {  ParamID::KET
      };
   return s_param_ids;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const typename ParamsTdvci<PARAM_T,CONT_TMPL>::cont_t& ParamsTdvci<PARAM_T,CONT_TMPL>::Coefs
   (
   )  const
{
   return mCoefs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvci<PARAM_T,CONT_TMPL>::PrintParamsToFile
   (  const std::string& aDir
   )  const
{
   const Uin prec = 16;

   std::vector<std::string> param_objects{"Coefs"};

   for (auto & param_obj : param_objects)
   {
      std::string file_name = aDir + param_obj + ".dat";
      std::ofstream ofs;
      ofs.open(file_name);
      ofs << std::scientific << std::setprecision(prec);

      if (param_obj == "Coefs")
      {
         ofs << this->Coefs() << std::flush;
      }
      else
      {
         MIDASERROR("Some input is incorrect for PrintParamsToFile in Tdvcc");
      }
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvci<PARAM_T,CONT_TMPL>::Scale
   (  param_t aFactor
   )
{
   ::Scale(mCoefs, aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvci<PARAM_T,CONT_TMPL>::Zero
   (
   )
{
   ::Zero(mCoefs);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvci<PARAM_T,CONT_TMPL>::Axpy
   (  const ParamsTdvci<PARAM_T,CONT_TMPL>& arX
   ,  param_t aA
   )
{
   ::Axpy(mCoefs, arX.Coefs(), aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvci<PARAM_T,CONT_TMPL>::SanityCheck
   (
   )
{
   // If GeneralDataCont, SetSameLabelWhenCopying(true).
   if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
   {
      mCoefs.SetSameLabelWhenCopying(true);
   }
}

} /* namespace midas::tdvcc */


/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arParams
   )
{
   arOs << arParams.Coefs() << std::endl;
   return arOs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Scale
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  PARAM_T aFactor
   )
{
   arA.Scale(aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Zero
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   arA.Zero();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Axpy
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arX
   ,  PARAM_T aA
   )
{
   arY.Axpy(arX, aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void SetShape
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arX
   )
{
   if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                  )
   {
      arY = arX;
      arY.Zero();
   }
   else
   {
      arY = midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>(Size(arX));
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return Norm2(arA.Coefs());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += Norm2(id, arA);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(aID, arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return DiffNorm2(arA.Coefs(), arB.Coefs());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += DiffNorm2(id, arA, arB);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin Size
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return arA.Size();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYNew
   )
{
   return OdeMeanNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs());
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYNew
   )
{
   return OdeMaxNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs());
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   DataToPointer(arA.Coefs(), apPtr);
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   using cont_t = typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::cont_t;
   cont_t coefs = std::is_same_v<cont_t, GeneralTensorDataCont<PARAM_T>> ? arA.Coefs() : cont_t(arA.Size());
   DataFromPointer(coefs, apPtr);
   arA = midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>(std::move(coefs));
}


#endif/*PARAMSTDVCI_IMPL_H_INCLUDED*/
