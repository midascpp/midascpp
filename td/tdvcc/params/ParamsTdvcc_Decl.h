/**
 *******************************************************************************
 * 
 * @file    ParamsTdvcc_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCC_DECL_H_INCLUDED
#define PARAMSTDVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "td/tdvcc/params/ParamsTdvccBase.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"
#include "td/tdvcc/TdvccEnums.h"

// Forward declarations.

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Holds the parameters \f$ \epsilon \f$ (phase/energy), \f$ \{s_\mu\},
    *    \{l_\mu\} \f$, relevant for a TDVCC parametrization.
    *
    * Inherits most functionality from base class, but allows for
    * specializations and function overloads that are specific for
    * (traditional/regular) VCC.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTdvcc
      :  public ParamsTdvccBase<PARAM_T,CONT_TMPL>
   {
      public:
         using param_t = PARAM_T;
         using absval_t = typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr CorrType corr_type = CorrType::VCC;

         ParamsTdvcc(const ParamsTdvcc&) = default;
         ParamsTdvcc(ParamsTdvcc&&) = default;
         ParamsTdvcc& operator=(const ParamsTdvcc&) = default;
         ParamsTdvcc& operator=(ParamsTdvcc&&) = default;
         ~ParamsTdvcc() = default;

         //! Forward all constructor calls to base class.
         template<class... Args>
         ParamsTdvcc(Args&&... args)
            :  ParamsTdvccBase<PARAM_T,CONT_TMPL>(std::forward<Args>(args)...)
         {
         }

         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //@{
         //! The stored parameters.
         const cont_t& ClusterAmps() const {return this->PrimalParams();}
         const cont_t& LambdaCoefs() const {return this->DualParams();}
         //@}

   };

} /* namespace midas::tdvcc */


#endif/*PARAMSTDVCC_DECL_H_INCLUDED*/
