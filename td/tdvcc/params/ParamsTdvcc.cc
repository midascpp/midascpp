/**
 *******************************************************************************
 * 
 * @file    ParamsTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvcc_Impl.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"

// Define instatiation macro.
#define INSTANTIATE_PARAMSTDVCC(PARAM_T, CONT_TMPL, ABSVAL_T) \
   namespace midas::tdvcc \
   { \
      template class ParamsTdvcc<PARAM_T, CONT_TMPL>; \
   } /* namespace midas::tdvcc */ \

// Niels: This does not work on clang++
//#define INSTANTIATE_TENSORDATACONT_CTOR(PARAM_T)   
//   namespace midas::tdvcc                          
//   {                                               
//   template ParamsTimTdvcc<PARAM_T, GeneralTensorDataCont>::ParamsTimTdvcc(const ModeCombiOpRange&, const std::vector<Uin>&, void*); 
//   }
//
//#define INSTANTIATE_CTOR(PARAM_T, CONT_TMPL)   
//   namespace midas::tdvcc                          
//   {                                               
//   template ParamsTimTdvcc<PARAM_T, CONT_TMPL>::ParamsTimTdvcc(Uin, void*); 
//   }

   
// Instantiations.
INSTANTIATE_PARAMSTDVCC(Nb, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTDVCC(Nb, GeneralDataCont, Nb);
INSTANTIATE_PARAMSTDVCC(Nb, GeneralTensorDataCont, Nb);
INSTANTIATE_PARAMSTDVCC(std::complex<Nb>, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTDVCC(std::complex<Nb>, GeneralDataCont, Nb);
INSTANTIATE_PARAMSTDVCC(std::complex<Nb>, GeneralTensorDataCont, Nb);

//INSTANTIATE_TENSORDATACONT_CTOR(Nb);
//INSTANTIATE_TENSORDATACONT_CTOR(std::complex<Nb>);
//INSTANTIATE_CTOR(Nb, GeneralDataCont);
//INSTANTIATE_CTOR(std::complex<Nb>, GeneralDataCont);
//INSTANTIATE_CTOR(Nb, GeneralMidasVector);
//INSTANTIATE_CTOR(std::complex<Nb>, GeneralMidasVector);

#undef INSTANTIATE_PARAMSTDVCC
//#undef INSTANTIATE_TENSORDATACONT_CTOR
//#undef INSTANTIATE_CTOR
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
