/**
 *******************************************************************************
 * 
 * @file    ParamsTdmvcc_Decl.h
 * @date    17-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDMVCC_DECL_H_INCLUDED
#define PARAMSTDMVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/params/ParamsTdvcc.h"
#include "libmda/util/any_type.h"

// Forward declarations.

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Stores phase, cluster amplitudes, Lambda coefficients, and pairs of
    *    {U,W} for each mode; U transforming creation operators (ket) and W
    *    transforming annihilation operators (bra).
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTdmvcc
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         using mat_t = GeneralMidasMatrix<param_t>;
         using midas_vec_t = GeneralMidasVector<param_t>;
         using mat_v_t = std::vector<mat_t>;
         using modal_cont_t = std::vector<std::pair<mat_t,mat_t>>;
         using ampl_cont_t = ParamsTdvcc<PARAM_T,CONT_TMPL>;
         static inline constexpr CorrType corr_type = CorrType::TDMVCC;
         static inline constexpr Uin num_phases = ampl_cont_t::num_phases;
         static inline constexpr Uin num_ampl_conts = ampl_cont_t::num_param_conts;
         static inline constexpr Uin num_modal_mats = 2;

         ParamsTdmvcc(const ParamsTdmvcc&) = default;
         ParamsTdmvcc(ParamsTdmvcc&&) = default;
         ParamsTdmvcc& operator=(const ParamsTdmvcc&) = default;
         ParamsTdmvcc& operator=(ParamsTdmvcc&&) = default;
         ~ParamsTdmvcc() = default;

         //! Phase = 0, no ampl. params, no modal params (unphysical).
         ParamsTdmvcc() = default;

         //! Not to be used ! Simply here to silence compiler. Problem occurs in Tdvcc_Impl.h at "ShapedZeroVectorImpl()".
         ParamsTdmvcc
            (  Uin aSizeParams
            );

         //! Phase = 0, dims. of mod.mats. (identity or zero), sizes of ampl.conts (all zero).
         ParamsTdmvcc
            (  const std::vector<Uin>& arNModalsTdBas
            ,  const std::vector<Uin>& arNModalsPrimBas
            ,  Uin aNAmpls
            ,  bool aIdentityModalMats = false
            );

         //! Constructor from input data
         ParamsTdmvcc
            (  std::map<ParamID, libmda::util::any_type>&&
            ,  const ModeCombiOpRange& 
            ,  const std::vector<Uin>& 
            );

         //! Constructor from data
         ParamsTdmvcc
            (  modal_cont_t aModals
            ,  cont_t aClusterAmps
            ,  cont_t aLambdaCoefs
            ,  param_t aPhase
            );

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         std::vector<Uin> NModalsTdBas() const;
         std::vector<Uin> NModalsPrimBas() const;
         Uin NModes() const {return mModalCont.size();}

         //! Combined size of phase, T-ampls., L-coefs. and all modal matrices.
         Uin Size() const;

         //! Size of each of the T-amps. and L-coefs. (which have same size).
         Uin SizeParams() const;

         //! Size of each of the modal matrices (same size) for that mode.
         Uin SizeModalMat(Uin aMode) const;

         //! Combined size of all pairs of modal matrices.
         Uin SizeModalMats() const;

         //! Total size given size of one amplitude container and number of modals
         static Uin SizeTotal
            (  Uin aSizeParams
            ,  const std::vector<Uin>& aNPrimModals
            ,  const std::vector<Uin>& aNTdModals
            );

         //! The ParamIDs for containers within such an object.
         static const std::set<ParamID>& GetParamIDs();

         //! The subset of ParamIDs pertaining to the modal transformations.
         static const std::set<ParamID>& GetModalParamIDs();

         //! Is the shape (num. ampls., td/prim basis dimensions) same as for this one?
         bool HasSameShape(const ParamsTdmvcc&) const;

         //! Output details about shape.
         void PrintShape(std::ostream& arOs) const;

         //!@}

         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //!@{
         const ampl_cont_t& AmplCont() const {return this->mAmplCont;}
         const modal_cont_t& ModalCont() const {return this->mModalCont;}
         param_t Phase() const {return this->AmplCont().Phase();}
         const cont_t& ClusterAmps() const {return this->AmplCont().ClusterAmps();}
         const cont_t& LambdaCoefs() const {return this->AmplCont().LambdaCoefs();}
         const std::pair<mat_t,mat_t>& ModalMats(Uin aMode) const {return mModalCont.at(aMode);}

         void PrintParamsToFile(const std::string& aDir) const;
         //!@}

         /******************************************************************//**
          * @name Modifications
          *********************************************************************/
         //!@{

         //! Scale all parameters with same factor.
         void Scale(param_t aFactor);

         //! Zero all parameters.
         void Zero();
         //! Zero all amplitudes (for debugging).
         void ZeroAmplitudes();

         //! Zero one-mode amplitudes. Return norms.
         std::array<std::vector<absval_t>, 2> ZeroOneModeAmplitudes
            (  const ModeCombiOpRange& arMcr
            )
         {
            return this->mAmplCont.ZeroOneModeAmplitudes(arMcr);
         }

         //! Axpy for all parameters, i.e. Y = a*X + Y, i.e. *this += a*X.
         void Axpy(const ParamsTdmvcc&, param_t);

         //@{
         // Apply a functor to modal matrices of this an others.
         template<size_t N, class F>
         void ApplyToModalMats
            (  const std::array<const ParamsTdmvcc*,N>& arVecPtrOthers
            ,  ParamID aID
            ,  F&& arFunc
            )
         {
            ApplyToModalMatsImpl(*this, arVecPtrOthers, aID, std::forward<F>(arFunc));
         }
         template<size_t N, class F>
         void ApplyToModalMats
            (  const std::array<const ParamsTdmvcc*,N>& arVecPtrOthers
            ,  ParamID aID
            ,  F&& arFunc
            )  const
         {
            ApplyToModalMatsImpl(*this, arVecPtrOthers, aID, std::forward<F>(arFunc));
         }

         //@}

         //!@}

         //! Check if modals are biorthonormal
         bool CheckBiorthonormality(std::vector<In>& aNonBiorthogModes) const;

         void UpdateModalMats(const std::vector<mat_t>& arProjectors);

      private:
         //! Pairs of {U,W}, U (tall) pertaining to ket/crea.ops., W (wide) to bra/anni.ops.
         modal_cont_t mModalCont;

         //! Correlation parameters and phase.
         ampl_cont_t mAmplCont;

         static modal_cont_t ConstructModalMats
            (  const std::vector<Uin>& arNModalsTdBas
            ,  const std::vector<Uin>& arNModalsPrimBas
            ,  bool aIdentityModalMats = false
            );

         //! true/false: bra or ket modal matrices; true/false: num. columns/rows.
         std::vector<Uin> NModalsImpl
            (  bool aBraNotKet
            ,  bool aNumColsNotRows
            )  const;

         // Apply a functor to modal matrices of this an others.
         template<class SELF_T, size_t N, class F>
         static void ApplyToModalMatsImpl
            (  SELF_T& arSelf
            ,  const std::array<const ParamsTdmvcc*,N>& arVecPtrOthers
            ,  ParamID aID
            ,  F&& arFunc
            );

         void SanityCheck() const;
   };

   /************************************************************************//**
    * For looping through the modal matrices, applying some function to them.
    *
    * Functor must have signature
    * `void F([const] mat_t&, const std::array<const mat_t*,N>&)`.
    *
    ***************************************************************************/
   template<typename PARAM_T, template<typename> class CONT_TMPL>
   template<class SELF_T, size_t N, class F>
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::ApplyToModalMatsImpl
      (  SELF_T& arSelf
      ,  const std::array<const ParamsTdmvcc*,N>& arVecPtrOthers
      ,  ParamID aID
      ,  F&& arFunc
      )
   {
      // Assertions.
      static_assert
         (  std::is_same_v<std::decay_t<SELF_T>,ParamsTdmvcc>
         ,  "SELF_T must be [const] ParamsTdmvcc."
         );

      bool ket_not_bra = false;
      switch(aID)
      {
         case ParamID::MODALKET: ket_not_bra = true;  break;
         case ParamID::MODALBRA: ket_not_bra = false; break;
         default: MIDASERROR("Unexpected ParamID: "+StringFromEnum(aID)); break;
      }
      for(const auto& p: arVecPtrOthers)
      {
         if (!arSelf.HasSameShape(*p))
         {
            std::stringstream ss;
            ss << "arSelf shape:\n";
            arSelf.PrintShape(ss);
            ss << "*p shape:\n";
            p->PrintShape(ss);
            MIDASERROR(ss.str());
         }
      }

      // Loop through modes, set up mat_t array and apply functor.
      for(Uin m = 0; m < arSelf.NModes(); ++m)
      {
         auto&& [ket_self,bra_self] = arSelf.mModalCont.at(m);
         auto&& m_self = ket_not_bra? ket_self: bra_self;
         std::array<const mat_t*,N> a_others;
         for(Uin i = 0; i < a_others.size(); ++i)
         {
            const auto& [ket_oth,bra_oth] = arVecPtrOthers.at(i)->ModalCont().at(m);
            a_others.at(i) = ket_not_bra? &ket_oth: &bra_oth;
         }
         arFunc(m_self, a_others);
      }
   }

} /* namespace midas::tdvcc */

/***************************************************************************//**
 * @name Interface wrappers for ParamsTdmvcc
 ******************************************************************************/
//!@{
template<typename PARAM_T, template<typename> class CONT_TMPL>
std::ostream& operator<<(std::ostream&, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Scale
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arParams
   ,  U arAlpha
   )
{
   Scale(arParams, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Scale(midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Zero(midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Axpy
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arX
   ,  U arAlpha
   )
{
   Axpy(arY, arX, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Axpy(midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void SetShape(midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm2(const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm(const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
Uin Size(const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>&);

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

//!@}


#endif/*PARAMSTDMVCC_DECL_H_INCLUDED*/
