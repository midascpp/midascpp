/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccBase_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCCBASE_IMPL_H_INCLUDED
#define PARAMSTDVCCBASE_IMPL_H_INCLUDED

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"

extern std::string gAnalysisDir;

namespace midas::tdvcc
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTdvccBase<PARAM_T,CONT_TMPL>::ParamsTdvccBase
   (  cont_t aAmps
   ,  cont_t aCoefs
   ,  param_t aPhase
   )
   :  mPhase(std::move(aPhase))
   ,  mPrimalParams(std::move(aAmps))
   ,  mDualParams(std::move(aCoefs))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTdvccBase<PARAM_T,CONT_TMPL>::ParamsTdvccBase
   (  std::map<ParamID,libmda::util::any_type>&& arMap
   ,  const ModeCombiOpRange& aMcr 
   ,  const std::vector<Uin>& aNModals
   )
   :  mPhase(0)
   ,  mPrimalParams(ExtractFromMap<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>, PARAM_T>(std::move(arMap), aMcr, aNModals, ParamID::KET))
   ,  mDualParams(ExtractFromMap<CONT_TMPL<PARAM_T>, GeneralMidasVector<PARAM_T>, PARAM_T>(std::move(arMap), aMcr, aNModals, ParamID::BRA))
{
   try
   {
      mPhase = ExtractFromMap<PARAM_T,PARAM_T,PARAM_T,false>(std::move(arMap), aMcr, aNModals, ParamID::PHASE);
   }
   catch(const std::out_of_range&)
   {
      // Just leave the phase at zero then.
   }
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTdvccBase<PARAM_T,CONT_TMPL>::Size
   (
   )  const
{
   return SizeTotal(SizeParams());
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTdvccBase<PARAM_T,CONT_TMPL>::SizeParams
   (
   )  const
{
   return ::Size(mPrimalParams);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTdvccBase<PARAM_T,CONT_TMPL>::SizeTotal
   (  Uin aSizeParams
   )
{
   return num_phases + num_param_conts*aSizeParams;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const std::set<ParamID>& ParamsTdvccBase<PARAM_T,CONT_TMPL>::GetParamIDs
   (
   )
{
   static std::set<ParamID> s_param_ids =
      {  ParamID::PHASE
      ,  ParamID::KET
      ,  ParamID::BRA
      };
   return s_param_ids;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::param_t ParamsTdvccBase<PARAM_T,CONT_TMPL>::Phase
   (
   )  const
{
   return mPhase;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::cont_t& ParamsTdvccBase<PARAM_T,CONT_TMPL>::PrimalParams
   (
   )  const
{
   return mPrimalParams;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::cont_t& ParamsTdvccBase<PARAM_T,CONT_TMPL>::DualParams
   (
   )  const
{
   return mDualParams;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::PrintParamsToFile
   (  const std::string& aDir
   )  const
{
   const Uin prec = 16;

   std::vector<std::string> param_objects{"Phase", "PrimalParams", "DualParams"};

   for (auto & param_obj : param_objects)
   {
      std::string file_name = aDir + param_obj + ".dat";
      std::ofstream ofs;
      ofs.open(file_name);
      ofs << std::scientific << std::setprecision(prec);

      if (param_obj == "Phase")
      {
         ofs << this->Phase() << std::flush;
      }
      else if (param_obj == "PrimalParams")
      {
         ofs << this->PrimalParams() << std::flush;
      }
      else if (param_obj == "DualParams")
      {
         ofs << this->DualParams() << std::flush;
      }
      else
      {
         MIDASERROR("Some input is incorrect for PrintParamsToFile in Tdvcc");
      }
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::Scale
   (  param_t aFactor
   )
{
   mPhase *= aFactor;
   ::Scale(mPrimalParams, aFactor);
   ::Scale(mDualParams, aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::Zero
   (
   )
{
   mPhase = 0;
   ::Zero(mPrimalParams);
   ::Zero(mDualParams);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::ZeroAmplitudes
   (
   )
{
   ::Zero(mPrimalParams);
   ::Zero(mDualParams);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
std::array<std::vector<typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t>, 2> 
ParamsTdvccBase<PARAM_T,CONT_TMPL>::ZeroOneModeAmplitudes
   (  const ModeCombiOpRange& aMcr
   )
{
   return { midas::tdvcc::ZeroOneModeAmplitudes(mPrimalParams, aMcr), midas::tdvcc::ZeroOneModeAmplitudes(mDualParams, aMcr) };
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::Axpy
   (  const ParamsTdvccBase<PARAM_T,CONT_TMPL>& arX
   ,  param_t aA
   )
{
   mPhase += aA*arX.Phase();
   ::Axpy(mPrimalParams, arX.mPrimalParams, aA);
   ::Axpy(mDualParams, arX.mDualParams, aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTdvccBase<PARAM_T,CONT_TMPL>::SanityCheck
   (
   )
{
   // If GeneralDataCont, SetSameLabelWhenCopying(true).
   if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
   {
      mPrimalParams.SetSameLabelWhenCopying(true);
      mDualParams.SetSameLabelWhenCopying(true);
   }

   // Check sizes.
   std::stringstream err;
   if (::Size(mPrimalParams) != ::Size(mDualParams))
   {
      err
         << "Size(mPrimalParams) (= " << ::Size(mPrimalParams)
         << ") != Size(mDualParams) (= " << ::Size(mDualParams)
         << ")."
         ;
      MIDASERROR(err.str());
   }
}

} /* namespace midas::tdvcc */


/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arParams
   )
{
   arOs  
      << "Phase       =\n" << arParams.Phase() << '\n'
      << "PrimalParams =\n" << arParams.PrimalParams() << '\n'
      << "DualParams =\n" << arParams.DualParams() << '\n'
      << std::flush;
   return arOs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Scale
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  PARAM_T aFactor
   )
{
   arA.Scale(aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Zero
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   arA.Zero();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Axpy
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arX
   ,  PARAM_T aA
   )
{
   arY.Axpy(arX, aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void SetShape
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arX
   )
{
   if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                  )
   {
      arY = arX;
      arY.Zero();
   }
   else
   {
      arY = midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>(arX.SizeParams());
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return Norm2(arA.PrimalParams());
      case ParamID::BRA:
         return Norm2(arA.DualParams());
      case ParamID::PHASE:
         return midas::util::AbsVal2(arA.Phase());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += Norm2(id, arA);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(aID, arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return DiffNorm2(arA.PrimalParams(), arB.PrimalParams());
      case ParamID::BRA:
         return DiffNorm2(arA.DualParams(), arB.DualParams());
      case ParamID::PHASE:
         return midas::util::AbsVal2(arA.Phase() - arB.Phase());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += DiffNorm2(id, arA, arB);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin Size
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   )
{
   return arA.Size();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   )
{
   if (  Size(aDeltaY) != Size(aYOld)
      || Size(aDeltaY) != Size(aYNew)
      )
   {
      std::stringstream err;
      err
         << "Size mismatch; Size(aDeltaY) = " << Size(aDeltaY)
         << ", Size(aYOld) = " << Size(aYOld)
         << ", Size(aYNew) = " << Size(aYNew)
         << "."
         ;
      MIDASERROR(err.str());
   }

   typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t norm2 = 0;

   // Put the phases in temp. containers of size 1, to utilize same
   // implementation as for CONT_TMPL.
   if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                  )
   {
      CONT_TMPL<PARAM_T> dely_phase(1);
      dely_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aDeltaY.Phase());
      CONT_TMPL<PARAM_T> yold_phase(1);
      yold_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aYOld.Phase());
      CONT_TMPL<PARAM_T> ynew_phase(1);
      ynew_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aYNew.Phase());
      norm2 += OdeNorm2(dely_phase, aAbsTol, aRelTol, yold_phase, ynew_phase);
   }
   else
   {
      const CONT_TMPL<PARAM_T> dely_phase(1, aDeltaY.Phase());
      const CONT_TMPL<PARAM_T> yold_phase(1, aYOld.Phase());
      const CONT_TMPL<PARAM_T> ynew_phase(1, aYNew.Phase());
      norm2 += OdeNorm2(dely_phase, aAbsTol, aRelTol, yold_phase, ynew_phase);
   }

   // Then the ampl./coef. containers.
   norm2 += OdeNorm2(aDeltaY.PrimalParams(), aAbsTol, aRelTol, aYOld.PrimalParams(), aYNew.PrimalParams());
   norm2 += OdeNorm2(aDeltaY.DualParams(), aAbsTol, aRelTol, aYOld.DualParams(), aYNew.DualParams());

   return norm2/Size(aDeltaY);
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   )
{
   if (  Size(aDeltaY) != Size(aYOld)
      || Size(aDeltaY) != Size(aYNew)
      )
   {
      std::stringstream err;
      err
         << "Size mismatch; Size(aDeltaY) = " << Size(aDeltaY)
         << ", Size(aYOld) = " << Size(aYOld)
         << ", Size(aYNew) = " << Size(aYNew)
         << "."
         ;
      MIDASERROR(err.str());
   }

   typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t maxnorm2 = 0;

   // Put the phases in temp. containers of size 1, to utilize same
   // implementation as for CONT_TMPL.
   if constexpr   (  std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                  )
   {
      CONT_TMPL<PARAM_T> dely_phase(1);
      dely_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aDeltaY.Phase());
      CONT_TMPL<PARAM_T> yold_phase(1);
      yold_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aYOld.Phase());
      CONT_TMPL<PARAM_T> ynew_phase(1);
      ynew_phase.GetModeCombiData(0) = NiceScalar<PARAM_T>(aYNew.Phase());
      maxnorm2 = std::max(maxnorm2, OdeMaxNorm2(dely_phase, aAbsTol, aRelTol, yold_phase, ynew_phase));
   }
   else
   {
      const CONT_TMPL<PARAM_T> dely_phase(1, aDeltaY.Phase());
      const CONT_TMPL<PARAM_T> yold_phase(1, aYOld.Phase());
      const CONT_TMPL<PARAM_T> ynew_phase(1, aYNew.Phase());
      maxnorm2 = std::max(maxnorm2, OdeMaxNorm2(dely_phase, aAbsTol, aRelTol, yold_phase, ynew_phase));
   }

   // Then the ampl./coef. containers.
   maxnorm2 = std::max(maxnorm2, OdeMaxNorm2(aDeltaY.PrimalParams(), aAbsTol, aRelTol, aYOld.PrimalParams(), aYNew.PrimalParams()));
   maxnorm2 = std::max(maxnorm2, OdeMaxNorm2(aDeltaY.DualParams(), aAbsTol, aRelTol, aYOld.DualParams(), aYNew.DualParams()));

   return maxnorm2;
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   Uin mult = 1;
   Uin i = 0;

   // Phase.
   if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
   {
      mult *= 2;
      apPtr[i+0] = std::real(arA.Phase());
      apPtr[i+1] = std::imag(arA.Phase());
   }
   else
   {
      apPtr[i] = arA.Phase();
   }
   i += mult*1;

   // Primal params.
   DataToPointer(arA.PrimalParams(), apPtr + i);
   i += mult*arA.SizeParams();

   // Dual params.
   DataToPointer(arA.DualParams(), apPtr + i);
   i += mult*arA.SizeParams();
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   using cont_t = typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::cont_t;

   Uin mult = 1;
   Uin i = 0;

   const Uin size = arA.SizeParams();
   typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::param_t phase;
   cont_t amps    = std::is_same_v<cont_t, GeneralTensorDataCont<PARAM_T>> ? arA.PrimalParams() : cont_t(size);
   cont_t coefs   = std::is_same_v<cont_t, GeneralTensorDataCont<PARAM_T>> ? arA.DualParams() : cont_t(size);

   // Phase.
   if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
   {
      mult *= 2;
      phase = PARAM_T(apPtr[i+0], apPtr[i+1]);
   }
   else
   {
      phase = PARAM_T(apPtr[i]);
   }
   i += mult*1;

   // Primal params.
   DataFromPointer(amps, apPtr + i);
   i += mult*size;

   // Dual params.
   DataFromPointer(coefs, apPtr + i);
   i += mult*size;

   arA = midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>(std::move(amps), std::move(coefs), phase);
}


#endif/*PARAMSTDVCCBASE_IMPL_H_INCLUDED*/
