/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccBase_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCCBASE_DECL_H_INCLUDED
#define PARAMSTDVCCBASE_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "libmda/util/any_type.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"
#include "td/tdvcc/TdvccEnums.h"

// Forward declarations.
class ModeCombiOpRange;

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Holds the parameters \f$ \epsilon \f$ (phase/energy), \f$ \{s_\mu\},
    *    \{l_\mu\} \f$, relevant for a TDVCC and related parametrizations.
    *
    * @note
    *    If CONT_TMPL is GeneralDataCont, this class will always
    *    SetSameLabelWhenCopying(true).
    *
    * @note
    *    Tried to use primal/dual params nomenclature in this general base
    *    class.
    *    In the context of (traditional) TDVCC,
    *    - primal: cluster amplitudes
    *    - dual: lambda coefficients
    *
    * Template parameters:
    *    -  PARAM_T: the parameter type (usually complex)
    *    -  CONT_TMPL: vectorized container, e.g. GeneralMidasVector
    *
    * CONT_TMPL must support:
    *    -  Size(CONT_TMPL<PARAM_T>)
    *    -  Scale(CONT_TMPL<PARAM_T>, PARAM_T)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTdvccBase
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr Uin num_param_conts = 2;
         static inline constexpr Uin num_phases = 1;

         static_assert(std::is_floating_point_v<absval_t>, "absval_t must be floating-point.");

         ParamsTdvccBase(const ParamsTdvccBase&) = default;
         ParamsTdvccBase(ParamsTdvccBase&&) = default;
         ParamsTdvccBase& operator=(const ParamsTdvccBase&) = default;
         ParamsTdvccBase& operator=(ParamsTdvccBase&&) = default;
         ~ParamsTdvccBase() = default;

         //! Phase = 0 and no primal/dual params.
         ParamsTdvccBase() = default;

         //! All parameters 0.
         template
            <  typename U = cont_t
            >
         ParamsTdvccBase
            (  Uin aSize
            ,  typename std::enable_if_t<!std::is_same_v<U, GeneralTensorDataCont<param_t>>, void>* = nullptr
            );

         //! Phase = 0 and given number of primal/dual params, all zeroed.
         template
            <  typename U = cont_t
            >
         ParamsTdvccBase
            (  const ModeCombiOpRange& aMcr
            ,  const std::vector<Uin>& aNModals
            ,  typename std::enable_if_t<std::is_same_v<U, GeneralTensorDataCont<param_t>>, void>* = nullptr
            );

         //! Construct from map
         ParamsTdvccBase
            (  std::map<ParamID,libmda::util::any_type>&& arMap
            ,  const ModeCombiOpRange& aMcr 
            ,  const std::vector<Uin>& aNModals
            );

         //! Construct with given amps./coefs. (of same size) and phase.
         ParamsTdvccBase(cont_t aAmps, cont_t aCoefs, param_t aPhase = 0);

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //! Combined size of T-amps., L-coefs. and phase.
         Uin Size() const;

         //! Size of each of the T-amps. and L-coefs. (which have same size).
         Uin SizeParams() const;

         //! How to compute total size given size of one parameter container.
         static Uin SizeTotal(Uin aSizeParams);

         //! The ParamIDs for containers within such an object.
         static const std::set<ParamID>& GetParamIDs();

         //!@}

         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //!@{
         //! The stored phase.
         param_t Phase() const;

         //@{
         //! The stored parameters.
         const cont_t& PrimalParams() const;
         const cont_t& DualParams() const;
         //@}

         //! Prints Phase, PrimalParams and DualParams to files in analysis dir
         virtual void PrintParamsToFile(const std::string& aDir) const;
         //!@}

         /******************************************************************//**
          * @name Modifications
          *********************************************************************/
         //!@{
         //! Scale all parameters (phase, T-amps., L-coefs.) with same factor.
         void Scale(param_t aFactor);

         //! Zero all parameters.
         void Zero();
         //! Zero all amplitudes.
         void ZeroAmplitudes();

         //! Zero one-mode amplitudes. Return norms.
         std::array<std::vector<absval_t>, 2> ZeroOneModeAmplitudes
            (  const ModeCombiOpRange& aMcr
            );

         //! Axpy for all parameters, i.e. Y = a*X + Y, i.e. *this += a*X.
         void Axpy(const ParamsTdvccBase&, param_t);

         //!@}

      private:
         param_t mPhase = 0;

         //@{
         //! Primal/dual params.
         cont_t mPrimalParams;
         cont_t mDualParams;
         //@}

         //!
         void SanityCheck();
   };

} /* namespace midas::tdvcc */

/***************************************************************************//**
 * @name Interface wrappers for ParamsTdvccBase
 ******************************************************************************/
//!@{
template<typename PARAM_T, template<typename> class CONT_TMPL>
std::ostream& operator<<(std::ostream&, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Scale
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arParams
   ,  U arAlpha
   )
{
   Scale(arParams, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Scale(midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Zero(midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Axpy
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arX
   ,  U arAlpha
   )
{
   Axpy(arY, arX, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Axpy(midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void SetShape(midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2(const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm(const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
Uin Size(const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>&);

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

//!@}

#endif/*PARAMSTDVCCBASE_DECL_H_INCLUDED*/
