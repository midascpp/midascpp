/**
 *******************************************************************************
 * 
 * @file    ParamsTdvci_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCI_DECL_H_INCLUDED
#define PARAMSTDVCI_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"
#include "td/tdvcc/TdvccEnums.h"
#include "libmda/util/any_type.h"

// Forward declarations.

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Holds a container of C-coefficients, suitable for TDVCI.
    *
    * @note
    *    If CONT_TMPL is GeneralDataCont, this class will always
    *    SetSameLabelWhenCopying(true).
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTdvci
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr Uin num_param_conts = 1;
         static inline constexpr Uin num_phases = 0;
         static inline constexpr CorrType corr_type = CorrType::VCI;

         static_assert(std::is_floating_point_v<absval_t>, "absval_t must be floating-point.");

         ParamsTdvci(const ParamsTdvci&) = default;
         ParamsTdvci(ParamsTdvci&&) = default;
         ParamsTdvci& operator=(const ParamsTdvci&) = default;
         ParamsTdvci& operator=(ParamsTdvci&&) = default;
         ~ParamsTdvci() = default;

         //! Size 0.
         ParamsTdvci() = default;

         //! All parameters 0.
         template
            <  typename U = cont_t
            >
         ParamsTdvci
            (  Uin aSize
            ,  typename std::enable_if_t<!std::is_same_v<U, GeneralTensorDataCont<param_t>>, void>* = nullptr
            );

         //! All parameters 0.
         template
            <  typename U = cont_t
            >
         ParamsTdvci
            (  const ModeCombiOpRange& aMcr
            ,  const std::vector<Uin>& aNModals
            ,  typename std::enable_if_t<std::is_same_v<U, GeneralTensorDataCont<param_t>>, void>* = nullptr
            );

         //! Construct with given coefs.
         ParamsTdvci(cont_t aCoefs);

         //! Construct form map with key "KET".
         ParamsTdvci(std::map<ParamID,libmda::util::any_type>&&, const ModeCombiOpRange&, const std::vector<Uin>&);

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //! Total size.
         Uin Size() const;

         //! How to compute total size given size of one parameter container.
         static Uin SizeTotal(Uin aSizeParams);

         //! The ParamIDs for containers within such an object.
         static const std::set<ParamID>& GetParamIDs();

         //!@}
         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //!@{
         //! The stored phase. (Must be in interface; always 0 for this class.)
         param_t Phase() const {return 0;}

         //! The stored parameters.
         const cont_t& Coefs() const;

         //! Prints Coefs to files in analysis dir
         virtual void PrintParamsToFile(const std::string& aDir) const;
         //!@}

         /******************************************************************//**
          * @name Modifications
          *********************************************************************/
         //!@{
         //! Scale all parameters with same factor.
         void Scale(param_t aFactor);

         //! Zero all parameters.
         void Zero();
         void ZeroAmplitudes() {this->Zero();}  // Since Phase is always 0 here just call Zero()

         //! Axpy for all parameters, i.e. Y = a*X + Y, i.e. *this += a*X.
         void Axpy(const ParamsTdvci&, param_t);

         //!@}

      private:
         //! Coefficients.
         cont_t mCoefs;

         //!
         void SanityCheck();
   };

} /* namespace midas::tdvcc */

/***************************************************************************//**
 * @name Interface wrappers for ParamsTdvci
 ******************************************************************************/
//!@{
template<typename PARAM_T, template<typename> class CONT_TMPL>
std::ostream& operator<<(std::ostream&, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Scale
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arParams
   ,  U arAlpha
   )
{
   Scale(arParams, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Scale(midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Zero(midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Axpy
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arX
   ,  U arAlpha
   )
{
   Axpy(arY, arX, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Axpy(midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void SetShape(midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2(const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t Norm(const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
Uin Size(const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>&);

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

//!@}

#endif/*PARAMSTDVCI_DECL_H_INCLUDED*/
