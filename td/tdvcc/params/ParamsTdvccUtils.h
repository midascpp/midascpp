/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCCUTILS_H_INCLUDED
#define PARAMSTDVCCUTILS_H_INCLUDED

#include "td/tdvcc/params/ParamsTdvccUtils_Decl.h"
#include "td/tdvcc/params/ParamsTdvccUtils_Impl.h"

#endif/*PARAMSTDVCCUTILS_H_INCLUDED*/
