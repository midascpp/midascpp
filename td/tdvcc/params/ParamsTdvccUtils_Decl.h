/**
 *******************************************************************************
 * 
 * @file    ParamsTdvccUtils_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDVCCUTILS_DECL_H_INCLUDED
#define PARAMSTDVCCUTILS_DECL_H_INCLUDED

// Standard headers.
#include<map>

// Midas headers.
#include "td/tdvcc/TdvccEnums.h"
#include "mmv/MidasVector.h"
#include "libmda/util/any_type.h"

// Forward declarations.
class ModeCombiOpRange;

namespace midas::tdvcc
{
   //!
   template<typename OUTPUT_T, typename INPUT_T, typename PARAM_T, bool FATAL = true>
   OUTPUT_T ExtractFromMap(std::map<ParamID,libmda::util::any_type>&&, const ModeCombiOpRange&, const std::vector<Uin>, ParamID);

   //! Zero element of the container, and return previous value.
   template<typename PARAM_T, template<typename> class CONT_TMPL>
   PARAM_T ZeroFirstElemAndReturnIt(CONT_TMPL<PARAM_T>&);

   //! Zero one-mode amplitudes
   template<typename PARAM_T, template<typename> typename CONT_TMPL>
   std::vector<midas::type_traits::RealTypeT<PARAM_T>> ZeroOneModeAmplitudes(CONT_TMPL<PARAM_T>&, const ModeCombiOpRange&);

} /* namespace midas::tdvcc */

#endif/*PARAMSTDVCCUTILS_DECL_H_INCLUDED*/
