/**
 *******************************************************************************
 * 
 * @file    ParamsTdmvcc_Impl.h
 * @date    17-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTDMVCC_IMPL_H_INCLUDED
#define PARAMSTDMVCC_IMPL_H_INCLUDED

#include <numeric>

#include "libmda/numeric/float_eq.h"

// Midas headers.
#include "td/tdvcc/params/ParamsTdvccUtils.h"
#include "lapack_interface/GEEV.h"

extern std::string gAnalysisDir;

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   ParamsTdmvcc<PARAM_T,CONT_TMPL>::ParamsTdmvcc
      (  const std::vector<Uin>& arNModalsTdBas
      ,  const std::vector<Uin>& arNModalsPrimBas
      ,  Uin aNAmpls
      ,  bool aIdentityModalMats
      )
      :  mModalCont(ConstructModalMats(arNModalsTdBas, arNModalsPrimBas, aIdentityModalMats))
      ,  mAmplCont(aNAmpls)
   {
      SanityCheck();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   ParamsTdmvcc<PARAM_T,CONT_TMPL>::ParamsTdmvcc
      (  std::map<ParamID, libmda::util::any_type>&& aInput
      ,  const ModeCombiOpRange& aMcr
      ,  const std::vector<Uin>& aNModals
      )
      :  mModalCont(ExtractFromMap<modal_cont_t, modal_cont_t, PARAM_T>(std::move(aInput), aMcr, aNModals, ParamID::MODALS))
      ,  mAmplCont(std::move(aInput), aMcr, aNModals)
   {
      SanityCheck();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   ParamsTdmvcc<PARAM_T,CONT_TMPL>::ParamsTdmvcc
      (  modal_cont_t aModals
      ,  cont_t aClusterAmps
      ,  cont_t aLambdaCoefs
      ,  param_t aPhase
      )
      :  mModalCont(std::move(aModals))
      ,  mAmplCont(std::move(aClusterAmps), std::move(aLambdaCoefs), std::move(aPhase))
   {
      SanityCheck();
   }

   /************************************************************************//**
    * @note
    *    Not to be used ! Simply here to silence compiler. Problem occurs in 
    *    Tdvcc_Impl.h at "ShapedZeroVectorImpl()".
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   ParamsTdmvcc<PARAM_T,CONT_TMPL>::ParamsTdmvcc
      (  Uin aSizeParams
      )
   {
      MIDASERROR("This C-tor should not be used!");
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::vector<Uin> ParamsTdmvcc<PARAM_T,CONT_TMPL>::NModalsTdBas
      (
      )  const
   {
      return this->NModalsImpl(false, true);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::vector<Uin> ParamsTdmvcc<PARAM_T,CONT_TMPL>::NModalsPrimBas
      (
      )  const
   {
      return this->NModalsImpl(false, false);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   Uin ParamsTdmvcc<PARAM_T,CONT_TMPL>::Size
      (
      )  const
   {
      return this->AmplCont().Size() + this->SizeModalMats();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   Uin ParamsTdmvcc<PARAM_T,CONT_TMPL>::SizeTotal
      (  Uin aSizeParams
      ,  const std::vector<Uin>& aNPrimModals
      ,  const std::vector<Uin>& aNTdModals
      )
   {
      if (  aNPrimModals.size() != aNTdModals.size()
         )
      {
         MIDASERROR("Modal sizes do not match in ParamsTdmvcc::SizeTotal");
      }
      return ampl_cont_t::SizeTotal(aSizeParams) + std::inner_product(aNPrimModals.begin(), aNPrimModals.end(), aNTdModals.begin(), static_cast<Uin>(0));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   Uin ParamsTdmvcc<PARAM_T,CONT_TMPL>::SizeParams
      (
      )  const
   {
      return this->AmplCont().SizeParams();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   Uin ParamsTdmvcc<PARAM_T,CONT_TMPL>::SizeModalMat
      (  Uin aMode
      )  const
   {
      const auto& m = ModalCont().at(aMode).first;
      return m.Nrows() * m.Ncols();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   Uin ParamsTdmvcc<PARAM_T,CONT_TMPL>::SizeModalMats
      (
      )  const
   {
      Uin s = 0;
      for(Uin m = 0; m < this->NModes(); ++m)
      {
         s += num_modal_mats * this->SizeModalMat(m);
      }
      return s;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   const std::set<ParamID>& ParamsTdmvcc<PARAM_T,CONT_TMPL>::GetParamIDs
      (
      )
   {
      static std::set<ParamID> s_param_ids =
         {  ParamID::PHASE
         ,  ParamID::KET
         ,  ParamID::BRA
         ,  ParamID::MODALKET
         ,  ParamID::MODALBRA
         };
      return s_param_ids;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   const std::set<ParamID>& ParamsTdmvcc<PARAM_T,CONT_TMPL>::GetModalParamIDs
      (
      )
   {
      static std::set<ParamID> s_param_ids =
         {  ParamID::MODALKET
         ,  ParamID::MODALBRA
         };
      return s_param_ids;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   bool ParamsTdmvcc<PARAM_T,CONT_TMPL>::HasSameShape
      (  const ParamsTdmvcc& arOther
      )  const
   {
      return this->SizeParams() == arOther.SizeParams()
         && this->NModalsTdBas() == arOther.NModalsTdBas()
         && this->NModalsPrimBas() == arOther.NModalsPrimBas()
         ;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::PrintShape
      (  std::ostream& arOs
      )  const
   {
      arOs
         << "NModes()         = " << this->NModes() << '\n'
         << "SizeParams()     = " << this->SizeParams() << '\n'
         << "NModalsTdBas()   = " << this->NModalsTdBas() << '\n'
         << "NModalsPrimBas() = " << this->NModalsPrimBas() << '\n'
         ;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::Scale
      (  param_t aFactor
      )
   {
      this->mAmplCont.Scale(aFactor);
      for(const auto& id: GetModalParamIDs())
      {
         this->ApplyToModalMats
            (  std::array<const ParamsTdmvcc*,0>{}
            ,  id
            ,  [aFactor](mat_t& m, const std::array<const mat_t*,0>&) -> void
               {
                  m.Scale(aFactor);
               }
            );
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::Zero
      (
      )
   {
      this->Scale(param_t(0));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::ZeroAmplitudes
      (
      )
   {
      this->mAmplCont.ZeroAmplitudes();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::Axpy
      (  const ParamsTdmvcc& arX
      ,  param_t aA
      )
   {  
      this->mAmplCont.Axpy(arX.AmplCont(), aA);
      for(const auto& id: GetModalParamIDs())
      {
         this->ApplyToModalMats
            (  std::array<const ParamsTdmvcc*,1>{&arX}
            ,  id
            ,  [aA](mat_t& m, const std::array<const mat_t*,1>& a) -> void
               {
                  const mat_t& x = *a.at(0);
                  m += mat_t(aA * x);
               }
            );
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   typename ParamsTdmvcc<PARAM_T,CONT_TMPL>::modal_cont_t
   ParamsTdmvcc<PARAM_T,CONT_TMPL>::ConstructModalMats
      (  const std::vector<Uin>& arNModalsTdBas
      ,  const std::vector<Uin>& arNModalsPrimBas
      ,  bool aIdentityModalMats
      )
   {
      if (arNModalsTdBas.size() != arNModalsPrimBas.size())
      {
         std::stringstream ss;
         ss << "Number of modes mismatch:\n"
            << "   arNModalsTdBas          = " << arNModalsTdBas << '\n'
            << "   arNModalsPrimBas        = " << arNModalsPrimBas << '\n'
            << "   arNModalsTdBas.size()   = " << arNModalsTdBas.size() << '\n'
            << "   arNModalsPrimBas.size() = " << arNModalsPrimBas.size() << '\n'
            ;
         MIDASERROR(ss.str());
      }
      const Uin n_modes = arNModalsTdBas.size();
      modal_cont_t v;
      v.reserve(n_modes);
      for(  auto it_td = arNModalsTdBas.cbegin(), it_prim = arNModalsPrimBas.cbegin()
         ;  it_td != arNModalsTdBas.cend() && it_prim != arNModalsPrimBas.cend()
         ;  ++it_td, ++it_prim
         )
      {
         mat_t mket(*it_prim, *it_td, param_t(0));
         mat_t mbra(*it_td, *it_prim, param_t(0));
         if (aIdentityModalMats)
         {
            mket.SetDiagToNb(param_t(1));
            mbra.SetDiagToNb(param_t(1));
         }
         v.emplace_back(std::move(mket), std::move(mbra));
      }
      return v;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::vector<Uin> ParamsTdmvcc<PARAM_T,CONT_TMPL>::NModalsImpl
      (  bool aBraNotKet
      ,  bool aNumColsNotRows
      )  const
   {
      std::vector<Uin> v;
      v.reserve(this->NModes());
      for(const auto& [mket,mbra]: this->ModalCont())
      {
         const mat_t* const p = aBraNotKet? &mbra: &mket;
         v.emplace_back(aNumColsNotRows? p->Ncols(): p->Nrows());
      }
      return v;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::SanityCheck
      (
      )  const
   {
      // ampl_cont_t is assumed to do it's own sanity checks where necessary.
      // Check proper dimensions of modal matrices.
      std::stringstream ss;
      const auto nm_ket_td = this->NModalsImpl(false, true);
      const auto nm_bra_td = this->NModalsImpl(true, false);
      const auto nm_ket_prim = this->NModalsImpl(false, false);
      const auto nm_bra_prim = this->NModalsImpl(true, true);

      if (nm_ket_td != nm_bra_td)
      {
         ss << "Dimension of time-dependent basis mismatch:\n"
            << "   nm_ket_td = " << nm_ket_td << '\n'
            << "   nm_bra_td = " << nm_bra_td << '\n'
            ;
         MIDASERROR(ss.str());
      }
      if (nm_ket_prim != nm_bra_prim)
      {
         ss << "Dimension of primitive/time-independent basis mismatch:\n"
            << "   nm_ket_prim = " << nm_ket_prim << '\n'
            << "   nm_bra_prim = " << nm_bra_prim << '\n'
            ;
         MIDASERROR(ss.str());
      }
      Uin m = 0;
      for(  auto it_td = nm_ket_td.cbegin(), it_prim = nm_ket_prim.cbegin()
         ;  it_td != nm_ket_td.cend() && it_prim != nm_ket_prim.cend()
         ;  ++it_td, ++it_prim, ++m
         )
      {
         if (*it_prim < *it_td)
         {
            ss << "num.modals(prim) < num.modals(td) for mode = " << m << '\n'
               << "   nm_ket_td   = " << nm_ket_td    << '\n'
               << "   nm_ket_prim = " << nm_ket_prim  << '\n'
               ;
            MIDASERROR(ss.str());
         }
      }
      // MGH: Added 11/05/21 for extra security. 
      if (this->NModes() == 0)
      {
         MIDASERROR("Oops, I have no modes!");
      }
      if (this->NModalsPrimBas().empty())
      {
         MIDASERROR("Oops, I have no primitive modals!");
      }
      if (this->NModalsTdBas().empty())
      {
         MIDASERROR("Oops, I have no time-dependent modals!");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   bool ParamsTdmvcc<PARAM_T,CONT_TMPL>::CheckBiorthonormality
      (  std::vector<In>& aNonBiorthogModes
      )  const
   {
      bool biortho = true;

      const auto& uw_vec = this->ModalCont();
      auto nmodes = this->NModes();

      for(In imode=I_0; imode<nmodes; ++imode)
      {
         const auto& um = uw_vec[imode].first;
         const auto& wm = uw_vec[imode].second;

         mat_t s = wm * um;
         auto nmodals = s.Nrows();
         for(In i=I_0; i<nmodals; ++i)
         {
            s[i][i] -= PARAM_T(1.0);
         }

         auto err = s.Norm();

         if (  !libmda::numeric::float_numeq_zero(err, nmodals, 10)
            )
         {
            biortho = false;
            Mout  << " Modals for mode " << imode << " are not biorthonormal. ||WU-1|| = " << err << std::endl;
            aNonBiorthogModes.emplace_back(imode);
         }
      }

      return biortho;
   }

   /***************************************************************************//**
    * Since ParamsTdmvcc does not inherit from ParamsTdvcc(Base) we implement again
    ******************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::PrintParamsToFile
      (  const std::string& aDir
      )  const
   {
      const Uin prec = 16;

      std::vector<std::string> param_objects{"Phase", "PrimalParams", "DualParams", "ModalMats"};

      for (auto & param_obj : param_objects)
      {
         std::string file_name = aDir + param_obj + ".dat";
         std::ofstream ofs;
         ofs.open(file_name);
         ofs << std::scientific << std::setprecision(prec);

         if (param_obj == "Phase")
         {
            ofs << this->Phase() << std::flush;
         }
         else if (param_obj == "PrimalParams")
         {
            ofs << this->ClusterAmps() << std::flush;
         }
         else if (param_obj == "DualParams")
         {
            ofs << this->LambdaCoefs() << std::flush;
         }
         else if (param_obj == "ModalMats")
         {
            Uin m = 0;
            for(const auto& [mket,mbra]: this->ModalCont())
            {
               ofs 
                  << "U^m (m = " << m << ") = \n" << mket
                  << "W^m (m = " << m << ") = \n" << mbra
                  ;
               ++m;
            }
            ofs << std::flush;
         }
         else
         {
            MIDASERROR("Some input is incorrect for PrintParamsToFile in Tdvcc");
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   void ParamsTdmvcc<PARAM_T,CONT_TMPL>::UpdateModalMats
      (  const std::vector<mat_t>& arProjectors
      ) 
   {
      for (In i_mode = I_0; i_mode < NModes(); i_mode++)
      {
         auto& um = mModalCont.at(i_mode).first;
         auto& wm = mModalCont.at(i_mode).second;
         um = mat_t(arProjectors.at(i_mode) * um);
         wm = mat_t(wm * arProjectors.at(i_mode));
      }
   }

} /* namespace midas::tdvcc */

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arParams
   )
{
   arOs << arParams.AmplCont();
   arOs 
      << "NModes:" << '\n' << arParams.NModes() << '\n'
      << "NModalsTdBas:" << '\n' << arParams.NModalsTdBas() << '\n'
      << "NModalsPrimBas:" << '\n' << arParams.NModalsPrimBas() << '\n'
      ;
   Uin m = 0;
   arOs << "ModalMats:" << std::endl;
   for(const auto& [mket,mbra]: arParams.ModalCont())
   {
      arOs 
         << "U^m (m = " << m << ") = \n" << mket
         << "W^m (m = " << m << ") = \n" << mbra
         ;
      ++m;
   }

   arOs << std::flush;
   return arOs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Scale
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  PARAM_T aFactor
   )
{
   arA.Scale(aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Zero
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   arA.Zero();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Axpy
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arX
   ,  PARAM_T aA
   )
{
   arY.Axpy(arX, aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void SetShape
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arX
   )
{
   arY = midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>
      (  arX.NModalsTdBas()
      ,  arX.NModalsPrimBas()
      ,  arX.SizeParams()
      );
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   if (  aID == ParamID::KET
      || aID == ParamID::BRA
      || aID == ParamID::PHASE
      )
   {
      return Norm2(aID, arA.AmplCont());
   }
   else if  (  aID == ParamID::MODALKET
            || aID == ParamID::MODALBRA
            )
   {
      using paramstdmvcc_t = ParamsTdmvcc<PARAM_T,CONT_TMPL>;
      using absval_t = typename paramstdmvcc_t::absval_t;
      using mat_t = typename paramstdmvcc_t::mat_t;
      absval_t norm2 = 0;
      arA.ApplyToModalMats
         (  std::array<const paramstdmvcc_t*,0>{}
         ,  aID
         ,  [&norm2](const mat_t& m, const std::array<const mat_t*,0>&) -> void
            {
               norm2 += m.Norm2();
            }
         );
      return norm2;
   }
   else
   {
      return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += Norm2(id, arA);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(aID, arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t Norm
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   if (  aID == ParamID::KET
      || aID == ParamID::BRA
      || aID == ParamID::PHASE
      )
   {
      return DiffNorm2(aID, arA.AmplCont(), arB.AmplCont());
   }
   else if  (  aID == ParamID::MODALKET
            || aID == ParamID::MODALBRA
            )
   {
      using paramstdmvcc_t = ParamsTdmvcc<PARAM_T,CONT_TMPL>;
      using absval_t = typename paramstdmvcc_t::absval_t;
      using mat_t = typename paramstdmvcc_t::mat_t;
      absval_t dnorm2 = 0;
      arA.ApplyToModalMats
         (  std::array<const paramstdmvcc_t*,1>{&arB}
         ,  aID
         ,  [&dnorm2](const mat_t& a, const std::array<const mat_t*,1>& arr) -> void
            {
               const mat_t& b = *arr.at(0);
               dnorm2 += DiffNorm2(a,b);
            }
         );
      return dnorm2;
   }
   else
   {
      return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   typename ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += DiffNorm2(id, arA, arB);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin Size
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   )
{
   return arA.Size();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYNew
   )
{
   using paramstdmvcc_t = midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>;
   using absval_t = typename paramstdmvcc_t::absval_t;
   using mat_t = typename paramstdmvcc_t::mat_t;

   absval_t odenorm2 = Size(aDeltaY.AmplCont()) * OdeMeanNorm2
      (  aDeltaY.AmplCont()
      ,  aAbsTol
      ,  aRelTol
      ,  aYOld.AmplCont()
      ,  aYNew.AmplCont()
      );

   for(const auto& id: aDeltaY.GetModalParamIDs())
   {
      aDeltaY.ApplyToModalMats
         (  std::array<const paramstdmvcc_t*,2>{&aYOld, &aYNew}
         ,  id
         ,  [&odenorm2,aAbsTol,aRelTol](const mat_t& dy, const std::array<const mat_t*,2>& arr) -> void
            {
               const mat_t& yold = *arr.at(0);
               const mat_t& ynew = *arr.at(1);
               odenorm2 += OdeNorm2(dy, aAbsTol, aRelTol, yold, ynew);
            }
         );
   }
   return odenorm2/Size(aDeltaY);
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& aYNew
   )
{
   using paramstdmvcc_t = midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>;
   using absval_t = typename paramstdmvcc_t::absval_t;
   using mat_t = typename paramstdmvcc_t::mat_t;

   absval_t odemaxnorm2 = OdeMaxNorm2
      (  aDeltaY.AmplCont()
      ,  aAbsTol
      ,  aRelTol
      ,  aYOld.AmplCont()
      ,  aYNew.AmplCont()
      );

   for(const auto& id: aDeltaY.GetModalParamIDs())
   {
      aDeltaY.ApplyToModalMats
         (  std::array<const paramstdmvcc_t*,2>{&aYOld, &aYNew}
         ,  id
         ,  [&odemaxnorm2,aAbsTol,aRelTol](const mat_t& dy, const std::array<const mat_t*,2>& arr) -> void
            {
               const mat_t& yold = *arr.at(0);
               const mat_t& ynew = *arr.at(1);
               odemaxnorm2 = std::max(odemaxnorm2, OdeMaxNorm2(dy, aAbsTol, aRelTol, yold, ynew));
            }
         );
   }
   return odemaxnorm2;
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   MIDASERROR("Implement me!");
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTdmvcc<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   MIDASERROR("Implement me!");
}


#endif/*PARAMSTDMVCC_IMPL_H_INCLUDED*/
