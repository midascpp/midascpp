/**
 *******************************************************************************
 * 
 * @file    Tdmvcc.cc
 * @date    25-05-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/Tdmvcc.h"
#include "td/tdvcc/Tdmvcc_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/deriv/DerivTdmvcc.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2.h"
#include "td/tdvcc/trf/TrfZeroConstraint.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"

#define UNPACK(...) __VA_ARGS__

// Define instatiation macro.
#define INSTANTIATE_TDMVCC_IMPL(VEC_T,TRF_T,GTRF_T,DERIV_T) \
   template class Tdmvcc<VEC_T,TRF_T,GTRF_T,DERIV_T>; \

#define INSTANTIATE_TDMVCC(PARAM_T,IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      INSTANTIATE_TDMVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralMidasVector>),TrfTdmvccMatRep<PARAM_T>,TrfVariationalConstraintMatRep<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralMidasVector,TrfTdmvccMatRep,TrfVariationalConstraintMatRep,IMAG_TIME>)); \
      INSTANTIATE_TDMVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralMidasVector>),TrfTdmvccMatRep<PARAM_T>,TrfZeroConstraint<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralMidasVector,TrfTdmvccMatRep,TrfZeroConstraint,IMAG_TIME>)); \
      INSTANTIATE_TDMVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralDataCont>),TrfTdmvcc2<PARAM_T>,TrfVariationalConstraintTdmvcc2<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralDataCont,TrfTdmvcc2,TrfVariationalConstraintTdmvcc2,IMAG_TIME>)); \
      INSTANTIATE_TDMVCC_IMPL(UNPACK(ParamsTdmvcc<PARAM_T,GeneralDataCont>),TrfTdmvcc2<PARAM_T>,TrfZeroConstraint<PARAM_T>,UNPACK(DerivTdmvcc<PARAM_T,GeneralDataCont,TrfTdmvcc2,TrfZeroConstraint,IMAG_TIME>)); \
   }; /* namespace midas::tdvcc */ \

// Instantiations.
INSTANTIATE_TDMVCC(std::complex<Nb>,false);
INSTANTIATE_TDMVCC(std::complex<Nb>,true);
INSTANTIATE_TDMVCC(Nb,true);

#undef INSTANTIATE_TDMVCC
#undef INSTANTIATE_TDMVCC_IMPL

#undef UNPACK
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
