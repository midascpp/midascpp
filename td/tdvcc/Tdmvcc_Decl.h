/**
 *******************************************************************************
 * 
 * @file    Tdmvcc_Decl.h
 * @date    20-05-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDMVCC_DECL_H_INCLUDED
#define TDMVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"
#include "td/oper/LinCombOper.h"
#include "td/tdvcc/Tdvcc.h"
#include "mmv/MidasMatrix.h"
#include "input/ModeCombi.h"


// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;

namespace midas::tdvcc
{
   /************************************************************************//**
    * Template parameters:
    *    -  VEC_T: class holding data; must have param_t
    *    -  TRF_T: transformer type
    *    -  GTRF_T: transformer for calculating g matrices and g terms for the EOMs
    *    -  DERIV_T: given a vec_t (+extra stuff), must compute and return
    *    a vec_t which is the corresponding derivative
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class GTRF_T
      ,  class DERIV_T
      >
   class Tdmvcc
      :  public Tdvcc<VEC_T, TRF_T, DERIV_T>
   {
      public:
         using vec_t = VEC_T;
         using param_t = typename vec_t::param_t;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using real_t = typename TdvccIfc::ifc_absval_t;
         using oper_t = td::LinCombOper<step_t, param_t, OpDef>;
         using regularization_t = typename TdvccIfc::ifc_regularization_t;
         using autocorr_t = typename TdvccIfc::ifc_autocorr_t; // MGH: Added 28/5/21
         using typename TdvccIfc::ifc_prop_t;

         /******************************************************************//**
          * @name Constructors and friends
          *********************************************************************/
         //!@{
         Tdmvcc
            (  const std::vector<Uin>& arNModals
            ,  const std::vector<Uin>& arNTdModals
            ,  const OpDef& arHamiltonian
            ,  ModalIntegrals<param_t> aModInts
            ,  const ModeCombiOpRange& arMcr
            ,  param_t aOperCoef = 1
            );

         Tdmvcc
            (  const std::vector<Uin>& arNModals
            ,  const std::vector<Uin>& arNTdModals
            ,  oper_t aHamiltonian
            ,  std::vector<ModalIntegrals<param_t>> aVecModInts
            ,  const ModeCombiOpRange& arMcr
            );
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //!
         const std::vector<Uin>& NTdModals() const override { return mrNTdModals; }
         virtual Uin SizeTotal() const override;
         virtual autocorr_t& AutoCorrSett() override { return this->mAutoCorrSett; } // MGH: Added 28/5/2021
         virtual const autocorr_t& AutoCorrSett() const override { return this->mAutoCorrSett; } // MGH: Added 28/5/2021
         virtual regularization_t& InvDensRegularization() override { return this->mInvDensRegularization; }
         virtual const regularization_t& InvDensRegularization() const override { return this->mInvDensRegularization; }
         virtual regularization_t& GOptRegularization() override { return this->mGOptRegularization; }
         virtual const regularization_t& GOptRegularization() const override { return this->mGOptRegularization; }
         virtual bool& NonOrthoActiveSpaceProjector() override { return this->mNonOrthoActiveSpaceProjector; }
         virtual bool NonOrthoActiveSpaceProjector() const override { return this->mNonOrthoActiveSpaceProjector; }
         virtual bool& ExplZeroOneModeAmpls() override { return this->mExplZeroOneModeAmpls; }
         virtual bool ExplZeroOneModeAmpls() const override { return this->mExplZeroOneModeAmpls; }
         virtual const NonOrthoProjectorType NonOrthoActiveSpaceProjectorType() const override {return this->mNonOrthoProjectorType;} // ABJ: Added 20/1/22
         virtual NonOrthoProjectorType& NonOrthoActiveSpaceProjectorType() override {return this->mNonOrthoProjectorType;} // ABJ: Added 20/1/22
         //!@}

         virtual void SaveAutoCorrImpl(const vec_t&) override;
         virtual void SaveAutoCorrExtImpl(const vec_t&) override;
         virtual void SaveTdModalPlotsImpl(const vec_t&) const override;
         virtual void CalculateOneModeDensities(const vec_t&) override;

         //! To be used with restart option
         virtual std::map<ParamID,libmda::util::any_type> ReadParamsFromFile(const std::string& aRestartFilePath) override;
         void SetInitStateForAutoCorrImpl(std::map<ParamID, libmda::util::any_type>&&) override;
         virtual std::map<ParamID,libmda::util::any_type> PrepInitState(InitType, CorrType, Uin, const std::vector<Uin>&, const std::vector<Uin>&, const std::string& = "", In = -1) override;

      protected:
         void ReadMatrixFromFile(GeneralMidasMatrix<param_t>& arM, std::istringstream& arStringStream, bool aBatch) const;

         void SanityCheckState(const std::map<ParamID, libmda::util::any_type>&) const override;

         void PassSettingsToTrfs() override;

         void ZeroOneModeAmplDerivatives
            (  vec_t& arDeriv
            )  const;

      private:
         //! Number of time-dependent modals for each mode
         const std::vector<Uin>& mrNTdModals;

         //! Constraint transformer
         mutable GTRF_T mGTrf;
         GTRF_T ConstraintTransformer(const std::vector<Uin>&, const ModeCombiOpRange&) const;

         //! Regularization of inverse (density) matrices
         //@{
         regularization_t mInvDensRegularization = { RegInverseType::XSVD, static_cast<real_t>(1.e-8) };
         regularization_t mGOptRegularization = { RegInverseType::XSVD, static_cast<real_t>(1.e-8) };
         //@}

         //! Use non-biorthogonal projector
         bool mNonOrthoActiveSpaceProjector = true;

         //! Which type of non-biorthogonal projector to be used
         NonOrthoProjectorType mNonOrthoProjectorType = NonOrthoProjectorType::ORIGINALBYUW;

         //! Zero one-mode amplitudes explicitly
         bool mExplZeroOneModeAmpls = GTRF_T::is_variational;

         //! Fail if modals become non-biorthonormal
         bool mFailIfNonBiOrtho = false;

         //! Settings for autocorrelation function (added 28/5/21)
         autocorr_t mAutoCorrSett = midas::tdvcc::AutoCorrSettings();

         /******************************************************************//**
          * @name Implementation of ODE interface
          *********************************************************************/
         //!@{
         //! Calculate time-derivative.
         vec_t DerivativeImpl(step_t, const vec_t&, const DERIV_T&) const override;

         //! Shaped vector of zeros
         vec_t ShapedZeroVectorImpl() const override;

         //! Process new vector, deciding whether to accept step.
         bool ProcessNewVectorImpl
            (  vec_t& arY
            ,  const vec_t& aYOld
            ,  step_t& arT
            ,  step_t aTOld
            ,  vec_t& arDyDt
            ,  const vec_t& aDyDtOld
            ,  step_t& arStep
            ,  size_t& arNDerivFail
            )  override;
         //!@}

         //! Construct a transformer
         virtual TRF_T ConstructTrf
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            )  const override;

         //! Prepare derivative evaluation
         virtual void PrepareDerivativeImpl
            (  const vec_t& aParams
            )  override;

         //! Construct total H matrix for g calculation
         typename TRF_T::mat_t TotalHMat
            (  step_t aTime
            )  const;
   };

} /* namespace midas::tdvcc */

#endif/*TDMVCC_DECL_H_INCLUDED*/
