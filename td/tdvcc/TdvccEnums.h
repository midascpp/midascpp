/**
 *******************************************************************************
 * 
 * @file    TdvccEnums.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCENUMS_H_INCLUDED
#define TDVCCENUMS_H_INCLUDED

#include <map>
#include <string>

#include "util/Error.h"
#include "util/UnderlyingType.h"
#include "util/type_traits/TypeName.h"

namespace midas::tdvcc
{
   template<class ENUM>
   const std::map<std::string,ENUM>& GetMapStringToEnum()
   {
      static_assert(!std::is_same_v<ENUM,ENUM>, "Can only call specializations of this.");
      static std::map<std::string,ENUM> m;
      return m;
   }

   template<class ENUM>
   ENUM EnumFromString(const std::string& s)
   {
      try
      {
         return GetMapStringToEnum<ENUM>().at(s);
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Key string '"+s+"' not found for "+midas::type_traits::TypeName<ENUM>()+" enum.");
         return ENUM();
      }
   }

   template<class ENUM>
   std::string StringFromEnum(ENUM e)
   {
      for(const auto& kv: GetMapStringToEnum<ENUM>())
      {
         if (kv.second == e)
         {
            return kv.first;
         }
      }
      MIDASERROR(midas::type_traits::TypeName<ENUM>()+" enum value '"+std::to_string(midas::util::ToUType(e))+"' not found.");
      return "";
   }

   //@{
   //! Initial state types.
   enum class InitType
   {  VSCFREF
   ,  VCCGS
   ,  TDVCC
   ,  TDMVCC
   };
   template<> inline const std::map<std::string, InitType>& GetMapStringToEnum()
   {
      static std::map<std::string, InitType> m;
      m["VSCFREF"] = InitType::VSCFREF;
      m["VCCGS"]   = InitType::VCCGS;
      m["TDVCC"]   = InitType::TDVCC;
      m["TDMVCC"]   = InitType::TDMVCC;
      return m;
   }
   //@}

   //@{
   //! Algorithm type for TDMVCC2 autocorrelation function
   enum class AutoCorrAlgo
   {  FULL
   ,  TRUNCATE
   ,  SCREEN
   };
   template<> inline const std::map<std::string, AutoCorrAlgo>& GetMapStringToEnum()
   {
      static std::map<std::string, AutoCorrAlgo> m;
      m["FULL"]     = AutoCorrAlgo::FULL;
      m["TRUNCATE"] = AutoCorrAlgo::TRUNCATE;
      m["SCREEN"]   = AutoCorrAlgo::SCREEN;
      return m;
   }
   //@}

   //@{
   //! State type (GENERAL = biorthogonal modals and non-zero amplitudes)
   enum class StateType
   {  VSCF
   ,  GENERAL
   };
   template<> inline const std::map<std::string, StateType>& GetMapStringToEnum()
   {
      static std::map<std::string, StateType> m;
      m["VSCF"]    = StateType::VSCF;
      m["GENERAL"] = StateType::GENERAL;
      return m;
   }
   //@}

   //@{
   //! Transformer types.
   enum class TrfType
   {  MATREP
   ,  VCC2H2
   ,  GENERAL
   };
   template<> inline const std::map<std::string, TrfType>& GetMapStringToEnum()
   {
      static std::map<std::string, TrfType> m;
      m["MATREP"]  = TrfType::MATREP;
      m["VCC2H2"]  = TrfType::VCC2H2;
      m["GENERAL"] = TrfType::GENERAL;
      return m;
   }
   //@}

   //@{
   //! Correlation type.
   enum class CorrType
   {  VCC
   ,  VCI
   ,  EXTVCC
   ,  TDMVCC
   };
   template<> inline const std::map<std::string, CorrType>& GetMapStringToEnum()
   {
      static std::map<std::string, CorrType> m;
      m["VCC"]    = CorrType::VCC;
      m["VCI"]    = CorrType::VCI;
      m["EXTVCC"] = CorrType::EXTVCC;
      m["EVCC"]   = CorrType::EXTVCC;    // Alias
      m["TDMVCC"] = CorrType::TDMVCC;
      return m;
   }
   //@}

   //@{
   //! Constraint type (in case of TDMVCC)
   enum class ConstraintType
   {  VARIATIONAL
   ,  ZERO
   };
   template<> inline const std::map<std::string, ConstraintType>& GetMapStringToEnum()
   {
      static std::map<std::string, ConstraintType> m;
      m["VARIATIONAL"] = ConstraintType::VARIATIONAL;
      m["ZERO"] = ConstraintType::ZERO;
      return m;
   }
   //@}

   //! Is CorrType a time-dependent-modals (TDM) method?
   inline bool IsTdmMethod(CorrType t)
   {
      return t == CorrType::TDMVCC;
   }

   //@{
   //! Identifiers for ket, bra, phase parameters.
   enum class ParamID
   {  PHASE
   ,  KET
   ,  BRA
   ,  MODALS
   ,  MODALKET
   ,  MODALBRA
   };
   template<> inline const std::map<std::string, ParamID>& GetMapStringToEnum()
   {
      static std::map<std::string, ParamID> m;
      m["PHASE"] = ParamID::PHASE;
      m["KET"] = ParamID::KET;
      m["BRA"] = ParamID::BRA;
      m["MODALS"] = ParamID::MODALS;
      m["MODALKET"] = ParamID::MODALKET;
      m["MODALBRA"] = ParamID::MODALBRA;
      return m;
   }
   //@}

   //@{
   //! Types of regularized inverses
   enum class RegInverseType : int
   {  SVD = 0
   ,  XSVD
   ,  TIKHONOV
   ,  NONE
   };
   template<> inline const std::map<std::string, RegInverseType>& GetMapStringToEnum()
   {
      static std::map<std::string, RegInverseType> m;
      m["SVD"] = RegInverseType::SVD;
      m["XSVD"] = RegInverseType::XSVD;
      m["TIKHONOV"] = RegInverseType::TIKHONOV;
      m["NONE"] = RegInverseType::NONE;
      return m;
   }
   //@}


   //@{
   //! Types of stored intermediates for variational optimization of g in TDMVCC
   enum class TdmvccIntermedType : int
   {  ETAH = 0
   ,  OMEGAH
   ,  MEANFIELDS
   };
   template<> inline const std::map<std::string, TdmvccIntermedType>& GetMapStringToEnum()
   {
      static std::map<std::string, TdmvccIntermedType> m;
      m["ETAH"] = TdmvccIntermedType::ETAH;
      m["OMEGAH"] = TdmvccIntermedType::OMEGAH;
      m["MEANFIELDS"] = TdmvccIntermedType::MEANFIELDS;
      return m;
   }
   //@}

   //@{
   //! Types of non orthogonal active space projectors for tdmvcc
   enum class NonOrthoProjectorType
   {  ORIGINALBYUW
   ,  MOOREPENROSETYPEBYU
   };
   template<> inline const std::map<std::string, NonOrthoProjectorType>& GetMapStringToEnum()
   {
      static std::map<std::string, NonOrthoProjectorType> m;
      m["ORIGINALBYUW"] = NonOrthoProjectorType::ORIGINALBYUW;
      m["MOOREPENROSETYPEBYU"] = NonOrthoProjectorType::MOOREPENROSETYPEBYU;
      return m;
   }
   //@}



   //@{
   /************************************************************************//**
    * @brief
    *    Properties for monitoring.
    *
    * -  ENERGY: energy exp. val. of the Hamiltonian driving the dynamics.
    * -  PHASE: phase/norm-factor (if applicable to ansatz)
    * -  AUTOCORR: autocorrelation function w.r.t. t0; three "types" are
    *    calculated,
    *    -  `<Psi'(t0)|Psi(t)>`
    *    -  `<Psi'(t)|Psi(t0)>^*`
    *    -  and their average
    *    Note that the three are identical for variational (VCI-like)
    *    parameterizations.
    * -  AUTOCORR_EXT: (expert option!)
    *    -  For VCC: calculates autocorr. funcs. (as for AUTOCORR),
    *    by converting L and T amplitudes to EXTVCC parameterization, i.e.
    *    `<Psi'(t)| = <Phi|exp(L)exp(-T)`.
    *    -  For EXTVCC: calculates autocorr. funcs. (as for AUTOCORR),
    *    by converting Sigma and T amplitudes to VCC parameterization, i.e.
    *    `<Psi'(t)| = <Phi|(1 + Sigma)exp(-T)`.
    *    -  For others, just calculates the same quantities as AUTOCORR.
    *
    ***************************************************************************/
   //! Properties for monitoring.
   enum class PropID
   {  ENERGY
   ,  PHASE
   ,  AUTOCORR
   ,  STATEPOPULATIONS
   ,  AUTOCORR_EXT
   };
   template<> inline const std::map<std::string, PropID>& GetMapStringToEnum()
   {
      static std::map<std::string, PropID> m;
      m["ENERGY"] = PropID::ENERGY;
      m["PHASE"] = PropID::PHASE;
      m["AUTOCORR"] = PropID::AUTOCORR;
      m["STATEPOPULATIONS"] = PropID::STATEPOPULATIONS;
      m["AUTOCORR_EXT"] = PropID::AUTOCORR_EXT;
      return m;
   }
   //@}

   //@{
   //! Properties for monitoring.
   enum class StatID
   {  FVCINORM2
   ,  NORM2
   ,  DNORM2INIT
   ,  DNORM2VCCGS
   };
   template<> inline const std::map<std::string, StatID>& GetMapStringToEnum()
   {
      static std::map<std::string, StatID> m;
      m["FVCINORM2"] = StatID::FVCINORM2;
      m["NORM2"] = StatID::NORM2;
      m["DNORM2INIT"] = StatID::DNORM2INIT;
      m["DNORM2VCCGS"] = StatID::DNORM2VCCGS;
      return m;
   }
   //@}

} /* namespace midas::tdvcc */

template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::InitType> {std::string operator()() const {return "midas::tdvcc::InitType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::AutoCorrAlgo> {std::string operator()() const {return "midas::tdvcc::AutoCorrAlgo";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::StateType> {std::string operator()() const {return "midas::tdvcc::StateType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::TrfType> {std::string operator()() const {return "midas::tdvcc::TrfType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::CorrType> {std::string operator()() const {return "midas::tdvcc::CorrType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::ParamID> {std::string operator()() const {return "midas::tdvcc::ParamID";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::PropID> {std::string operator()() const {return "midas::tdvcc::PropID";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::StatID> {std::string operator()() const {return "midas::tdvcc::StatID";}};

#endif /* TDVCCENUMS_H_INCLUDED */
