/*
************************************************************************
*
* @file                 PrimitiveIntegrals_Impl.h
*
* Created:              16-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Primitive integrals for TD calculations.
*                       Allows for transforming to another primitive basis, 
*                       e.g. VSCF spectral basis.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef PRIMITIVEINTEGRALS_IMPL_H_INCLUDED
#define PRIMITIVEINTEGRALS_IMPL_H_INCLUDED

#include "td/PrimitiveIntegrals_Decl.h"

#include "input/Input.h"

namespace midas::td
{

/**
 * @param[in] apOpDef
 * @param[in] apBasDef
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
std::vector<In> PrimitiveIntegrals<T, VEC_T, MAT_T>::VecNOper
   (  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
{
   std::vector<In> v;
   v.reserve(apBasDef->GetmNoModesInBas());
   for(LocalModeNr m = 0; m < apBasDef->GetmNoModesInBas(); ++m)
   {
      v.push_back(apOpDef->NrOneModeOpers(m));
   }
   return v;
}

/**
 * @param[in] apOpDef
 * @param[in] apBasDef
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
std::vector<Uin> PrimitiveIntegrals<T, VEC_T, MAT_T>::VecNBas
   (  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
{
   std::vector<Uin> v;
   v.reserve(apBasDef->GetmNoModesInBas());
   for(LocalModeNr m = 0; m < apBasDef->GetmNoModesInBas(); ++m)
   {
      GlobalModeNr g_mode  = apOpDef->GetGlobalModeNr(m);
      LocalModeNr bas_mode = apBasDef->GetLocalModeNr(g_mode);
      v.push_back(apBasDef->Nbas(bas_mode));
   }
   return v;
}

/**
 * @param[in] apOpDef
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
std::vector<In> PrimitiveIntegrals<T, VEC_T, MAT_T>::VecUnitOpPositions
   (  const OpDef* const apOpDef
   )
{
   std::vector<In> v;
   v.reserve(apOpDef->NmodesInOp());
   for(LocalModeNr m = 0; m < apOpDef->NmodesInOp(); ++m)
   {
      v.push_back(apOpDef->GetUnitOpForMode(m));
   }
   return v;
}

/**
 * @warning
 *    Before calling this function, you _must_ have set mVecNOper and mVecNBas
 *    correctly up to reflect dimensions of the primitive basis pertaining to
 *    the arPrimInts.
 *
 * @param[in] arPrimInts
 *    The primitive integrals. They _must_ have been calculated already (OneModeInt::CalcInt()).
 * @param[in] aModals
 *    Container with the prim->modal coefficients.
 * @param[in] aModalOffSets
 *    Offsets of each mode's modals in aModals.
 * @param[in] aModalBasisLimits
 *    The dimensions of the modal basis.
 * @param[in] aSkipModes
 *    Skip transformation of integrals for modes in set (but move to mTransformedOneModeInt)
 *    The In%s are LocalModeNr%s of the OpDef used for constructing the PrimitiveIntegrals.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
template
   <  typename U
   >
void PrimitiveIntegrals<T, VEC_T, MAT_T>::TransformModalBasisImpl
   (  const OneModeInt& arPrimInts
   ,  const GeneralDataCont<U>& aModals
   ,  const std::vector<In>& aModalOffSets
   ,  const std::vector<In>& aModalBasisLimits
   ,  const std::set<In>& aSkipModes
   )
{
   // Get number of modes
   In nmodes = this->NModes();

   // Reserve space in transformed integrals
   this->mTransformedOneModeInt.reserve(nmodes);

   // Check if we have limits. Otherwise, we use the same basis size.
   bool has_limits = !aModalBasisLimits.empty();

   // Loop over modes
   GeneralMidasMatrix<U> modals_rows_conj;
   GeneralMidasMatrix<U> modals_cols;
   MidasMatrix integrals;
   mat_t half_trans;
   for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
   {
      //// Get sizes
      In nbas = this->NBas(imode);
      In nop = this->NOper(imode);
      In lim  = has_limits ? std::min(aModalBasisLimits[imode], nbas) : nbas;
      bool skip = (aSkipModes.find(imode) != aSkipModes.end());

      // Set size of modal matrices.
      // In the end;
      // modals_rows_conj shall have the comp.conj. modal coefs. on the rows.
      // modals_cols shall have the modal coefs. on the columns.
      modals_rows_conj.SetNewSize(lim, nbas);
      modals_cols.SetNewSize(nbas, lim);
      integrals.SetNewSize(nbas);
      half_trans.SetNewSize(lim, nbas);

      // Get modals for mode
      // NB: This returns the transposed modal matrix with modals in the rows,
      // but not complex conjugated yet! I.e. C^T, where C has coefs. on the cols.
      aModals.DataIo(IO_GET, modals_rows_conj, nbas*lim, lim, nbas, aModalOffSets[imode]);
      // Then put (C^T)^T --> modal_cols.
      // And (C^T)^* = C^\dagger --> modals_rows_conj.
      modals_cols = Transpose(modals_rows_conj);
      modals_rows_conj.ConjugateElems();

      std::vector<mat_t> oper_ints;
      oper_ints.reserve(nop);
      // Loop over operators
      for(LocalOperNr iop=I_0; iop<nop; ++iop)
      {
         // Calculate
         arPrimInts.GetInt(imode, iop, integrals); 

         if (  skip
            )
         {
            // Emplace integrals without transform
            oper_ints.emplace_back(integrals);
         }
         else
         {
            half_trans = modals_rows_conj * integrals;
            mat_t trans_int = half_trans * modals_cols;
            assert(trans_int.IsSquare());

            // Emplace to vector
            oper_ints.emplace_back(std::move(trans_int));
         }
      }
      
      // Emplace to vector
      this->mTransformedOneModeInt.emplace_back(std::move(oper_ints));
   }

   // Finally, clear the primitive integrals and update mVecNBas.
   this->mPrimitiveOneModeInt = nullptr;
   this->UpdateVecNBas(mTransformedOneModeInt);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
void PrimitiveIntegrals<T, VEC_T, MAT_T>::UpdateVecNBas
   (  const std::vector<std::vector<mat_t>>& aInts
   )
{
   this->mVecNBas = VecNBas(aInts);
}


/**
 * @param[in] arInts
 *    Extracts basis dimensions from dimensions of matrices within this object.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
std::vector<Uin> PrimitiveIntegrals<T, VEC_T, MAT_T>::VecNBas
   (  const std::vector<std::vector<mat_t>>& arInts
   )
{
   std::vector<Uin> v;
   v.reserve(arInts.size());
   for(Uin m = 0; m < arInts.size(); ++m)
   {
      v.push_back(arInts.at(m).at(0).Nrows());
   }
   return v;
}

/**
 * @param apOpDef
 * @param apBasDef
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
PrimitiveIntegrals<T, VEC_T, MAT_T>::PrimitiveIntegrals
   (  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
   :  PrimitiveIntegrals<T, VEC_T, MAT_T>
         (  OneModeInt
               (  apOpDef
               ,  apBasDef
               ,  "InMem"
               ,  true
               )
         ,  true
         )
{
}

/**
 * @param[in] aOneModeInt
 *    OneModeInt object that will be moved to the heap, and pointed to by
 *    mPrimitiveOneModeInt.
 *    The pass-by-value encapsulates both const OneModeInt& and OneModeInt&&,
 *    since aOneModeInt is moved into make_unique.
 *    The OpDef and BasDef of aOneModeInt are used for setting up the num
 *    modes/operators/dimensions of this object.
 * @param[in] aCalcInt
 *    If true, calls OneModeInt::CalcInt(); do this for newly constructed
 *    OneModeInt object.
 *    If false, doesn't CalcInt(). Do this if client has done so already.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
PrimitiveIntegrals<T, VEC_T, MAT_T>::PrimitiveIntegrals
   (  OneModeInt aOneModeInt
   ,  bool aCalcInt
   )
   :  mPrimitiveOneModeInt
         (  std::make_unique<OneModeInt>
               (  std::move(aOneModeInt)
               )
         )
   ,  mVecNOper
         (  VecNOper
               (  this->mPrimitiveOneModeInt->GetmpOpDef()
               ,  this->mPrimitiveOneModeInt->GetmpBasDef()
               )
         )
   ,  mVecNBas
         (  VecNBas
               (  this->mPrimitiveOneModeInt->GetmpOpDef()
               ,  this->mPrimitiveOneModeInt->GetmpBasDef()
               )
         )
   ,  mVecUnitOpPositions
         (  VecUnitOpPositions
               (  this->mPrimitiveOneModeInt->GetmpOpDef()
               )
         )
{
   // Calculate integrals
   if (aCalcInt)
   {
      if (  gDebug
         || gOperIoLevel > I_5
         )
      {
         Mout << " PrimitiveIntegrals: Calculating primitive one-mode integrals of operator: " << this->mPrimitiveOneModeInt->GetmpOpDef()->Name() << std::endl;
      }

      this->mPrimitiveOneModeInt->CalcInt();

      if (  gDebug
         || gOperIoLevel > I_10
         )
      {
         Mout  << " Primitive integrals calculated!" << std::endl;
      }
   }
}

/**
 * @param[in] arOneModeInt
 *    OneModeInt%s, must have had CalcInt() already. Not stored in this object,
 *    but directly transformed into PrimitiveIntegrals::mTransformedOneModeInt.
 * @param[in] aModals
 *    Container with primitive->modal basis coefficients.
 * @param[in] aModalOffSets
 *    Modal offsets in DataCont
 * @param[in] aModalBasisLimits
 *    Limits to the spectral modal basis, i.e. number of modal basis functions
 *    per mode.
 * @param[in] aSkipModes
 *    Skip transformation for modes in set (move integrals to mTransformedOneModeInt)
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
PrimitiveIntegrals<T, VEC_T, MAT_T>::PrimitiveIntegrals
   (  const OneModeInt& arOneModeInt
   ,  const DataCont& aModals
   ,  const std::vector<In>& aModalOffSets
   ,  const std::vector<In>& aModalBasisLimits
   ,  const std::set<In>& aSkipModes
   )
   :  mVecNOper(VecNOper(arOneModeInt.GetmpOpDef(), arOneModeInt.GetmpBasDef()))
   ,  mVecNBas(VecNBas(arOneModeInt.GetmpOpDef(), arOneModeInt.GetmpBasDef()))
   ,  mVecUnitOpPositions
         (  VecUnitOpPositions
               (  arOneModeInt.GetmpOpDef()
               )
         )
{
   TransformModalBasisImpl(arOneModeInt, aModals, aModalOffSets, aModalBasisLimits, aSkipModes);
}


/**
 * @return Are we using transformed basis?
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
bool PrimitiveIntegrals<T, VEC_T, MAT_T>::TransformedBasis
   (
   )  const
{
   // We have transformed-basis integrals?
   bool spec = !mTransformedOneModeInt.empty();

   if (  mPrimitiveOneModeInt
      && !spec
      )
   {
      return false;
   }
   else if  (  spec
            && !mPrimitiveOneModeInt
            )
   {
      return true;
   }
   else
   {
      Mout  << " ERROR in PrimitiveIntegrals!\n"
            << "    Primitive integrals:     " << std::boolalpha << bool(mPrimitiveOneModeInt) << std::noboolalpha << "\n"
            << "    Transformed integrals:   " << std::boolalpha << spec << std::noboolalpha << "\n"
            << std::flush;
      MIDASERROR("This should not happen!");
      return false;
   }
}

/**
 * @param aModeNr
 * @return
 *    Number of basis functions for mode
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
In PrimitiveIntegrals<T, VEC_T, MAT_T>::NBas
   (  LocalModeNr aModeNr
   )  const
{
   return this->mVecNBas.at(aModeNr);
}

/**
 * @return
 *    Vector of NBas
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
const std::vector<Uin>& PrimitiveIntegrals<T, VEC_T, MAT_T>::GetVecNBas
   (
   )  const
{
   return this->mVecNBas;
}

/**
 * @param aModeNr
 * @return
 *    Number of operators for mode
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
In PrimitiveIntegrals<T, VEC_T, MAT_T>::NOper
   (  LocalModeNr aModeNr
   )  const
{
   return this->mVecNOper.at(aModeNr);
}

/**
 * @return
 *    Number of modes
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
In PrimitiveIntegrals<T, VEC_T, MAT_T>::NModes
   (
   )  const
{
   return this->mVecNOper.size();
}

/**
 * Get time-independent integrals
 *
 * @param aModeNr
 * @param aOperNr
 *
 * @return
 *    Matrix of primitive integrals
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::mat_t PrimitiveIntegrals<T, VEC_T, MAT_T>::GetTimeIndependentIntegrals
   (  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  const &
{
   if (  this->TransformedBasis()
      )
   {
      return this->mTransformedOneModeInt[aModeNr][aOperNr];
   }
   else
   {
      GlobalModeNr i_g_mode  = mPrimitiveOneModeInt->GetmpOpDef()->GetGlobalModeNr(aModeNr);
      LocalModeNr i_bas_mode = mPrimitiveOneModeInt->GetmpBasDef()->GetLocalModeNr(i_g_mode);
      In nbas                = mPrimitiveOneModeInt->GetmpBasDef()->Nbas(i_bas_mode);
      MidasMatrix h(nbas);
      this->mPrimitiveOneModeInt->GetInt(aModeNr, aOperNr, h); 

      if constexpr   (  std::is_same_v<mat_t, MidasMatrix>
                     )
      {
         return h;
      }
      else
      {
         // convert
         mat_t h_new(nbas);
         for(In irow=I_0; irow<nbas; ++irow)
         {
            for(In icol=I_0; icol<nbas; ++icol)
            {
               h_new[irow][icol] = param_t(h[irow][icol]);
            }
         }

         return h_new;
      }
   }
}

/**
 * Get overlap integrals
 *
 * @param aOperModeNr
 *
 * @return
 *    Matrix of S integrals
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::mat_t PrimitiveIntegrals<T, VEC_T, MAT_T>::GetOverlapIntegrals
   (  LocalModeNr aOperModeNr
   )  const
{
   auto unit_op = this->mVecUnitOpPositions[aOperModeNr];
   return this->GetTimeIndependentIntegrals(aOperModeNr, unit_op);
}

/*
 * If calling this function on a rvalue ref object, then try to just move the
 * integrals out of the object instead of making a copy.
 * Enables some move efficiency if it's a temp. PrimitiveIntegrals object.
 * Due to impl. of OneModeInt, cannot move those, so if storing primitive
 * integrals, will just call the other GetTimeIndependentIntegrals function.
 *
 * @note
 *    It's okay to start _moving_ the mTransformedOneModeInt matrices out of
 *    this object because the dimensionality info is stored in mVecNBas.
 *
 * @param aModeNr
 * @param aOperNr
 *
 * @return
 *    Matrix of primitive integrals
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::mat_t PrimitiveIntegrals<T, VEC_T, MAT_T>::GetTimeIndependentIntegrals
   (  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  &&
{
   if (  this->TransformedBasis()
      )
   {
      return std::move(this->mTransformedOneModeInt[aModeNr][aOperNr]);
   }
   else
   {
      // Don't know if this is necessary, just to emphasize, we're calling the
      // lvalue ref version.
      const auto& self = *this;
      return self.GetTimeIndependentIntegrals(aModeNr, aOperNr);
   }
}

/**
 * Get time-independent integral
 *
 * @param aModeNr
 * @param aOperNr
 * @param aLeftIdx
 * @param aRightIdx
 *
 * @return
 *    Matrix of primitive integrals
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::param_t PrimitiveIntegrals<T, VEC_T, MAT_T>::GetTimeIndependentIntegral
   (  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   ,  size_t aLeftIdx
   ,  size_t aRightIdx
   )  const
{
   if (  this->TransformedBasis()
      )
   {
      return this->mTransformedOneModeInt[aModeNr][aOperNr][aLeftIdx][aRightIdx];
   }
   else
   {
      Nb h;

      this->mPrimitiveOneModeInt->GetInt(aModeNr, aOperNr, aLeftIdx, aRightIdx, h); 

      return std::move(param_t(h));
   }
}

/**
 * Transform to spectral basis. Delete primitive integrals.
 *
 * @param[in] aModals
 *    Modals from VSCF in DataCont
 * @param[in] aModalOffsets
 *    Modal offsets in DataCont
 * @param[in] aModalBasisLimits
 *    Limits to the spectral modal basis
 * @param[in] aSkipModes
 *    Skip transform for modes in set (but move to mTransformedOneModeInt)
 *    The In%s are LocalModeNr%s of the OpDef used for constructing the PrimitiveIntegrals.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
template
   <  typename U
   >
void PrimitiveIntegrals<T, VEC_T, MAT_T>::TransformModalBasis
   (  const GeneralDataCont<U>& aModals
   ,  const std::vector<In>& aModalOffsets
   ,  const std::vector<In>& aModalBasisLimits
   ,  const std::set<In>& aSkipModes
   )
{
   if (  gDebug
      || gOperIoLevel > I_5
      )
   {
      Mout << " PrimitiveIntegrals: Transform primitive basis for operator '" << this->mPrimitiveOneModeInt->GetmpOpDef()->Name() << "'." << std::endl;
   }

   // Sanity check
   if (  !this->mTransformedOneModeInt.empty()
      )
   {
      MIDASERROR("We can only transform modal basis once!");
   }
   if (  !this->mPrimitiveOneModeInt
      )
   {
      MIDASERROR("We need primitive integrals in order to transform to spectral basis!");
   }

   this->TransformModalBasisImpl(*this->mPrimitiveOneModeInt, aModals, aModalOffsets, aModalBasisLimits, aSkipModes);
}

/**
 * Calculate <phi^m|h^m|phi^m>
 *
 * @param aModalCoef      Modal coefficients of phi^m
 * @param aModeNr
 * @param aOperNr
 * @return
 *    Expectation value
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::param_t PrimitiveIntegrals<T, VEC_T, MAT_T>::OneModeIntBraket
   (  const vec_t& aModalCoef
   ,  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  const
{
   return this->OneModeIntBraket(aModalCoef, aModalCoef, aModeNr, aOperNr);
}

/**
 * Calculate <phi^m_r|h^m|phi^m_s>
 *
 * @param aModalCoefLeft      Modal coefficients of phi^m_r
 * @param aModalCoefRight     Modal coefficients of phi^m_s
 * @param aModeNr
 * @param aOperNr
 * @return
 *    Expectation value
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  template<typename> typename MAT_T
   >
typename PrimitiveIntegrals<T, VEC_T, MAT_T>::param_t PrimitiveIntegrals<T, VEC_T, MAT_T>::OneModeIntBraket
   (  const vec_t& aModalCoefLeft
   ,  const vec_t& aModalCoefRight
   ,  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  const
{
   if (  this->TransformedBasis()
      )
   {
      const auto& integrals = this->mTransformedOneModeInt[aModeNr][aOperNr];

      vec_t half_trans = integrals * aModalCoefRight;

      return Dot(aModalCoefLeft, half_trans);
   }
   else
   {
      In nbas = this->NBas(aModeNr);

      MidasMatrix h_m_p(nbas);
      mPrimitiveOneModeInt->GetInt(aModeNr, aOperNr, h_m_p);
      
      vec_t half_trans = h_m_p*aModalCoefRight;
      return Dot(aModalCoefLeft, half_trans);
   }
}


} /* namespace midas::td */

#endif /* PRIMITIVEINTEGRALS_IMPL_H_INCLUDED */
