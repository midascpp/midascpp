/*
************************************************************************
*
* @file                 McTdHVectorContainer.h
*
* Created:              01-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Container for McTdH data
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHVECTORCONTAINER_H_INCLUDED
#define MCTDHVECTORCONTAINER_H_INCLUDED

// Include declaration
#include "McTdHVectorContainer_Decl.h"

// Include implementation
#include "McTdHVectorContainer_Impl.h"


/***********************************************************************
 * ALIASES
 **********************************************************************/
#include "mmv/MidasVector.h"
#include "vcc/TensorDataCont.h"
#include "tensor/NiceTensor.h"

// Alias for "standard" CAS MCTDH calculations
template
   <  typename T
   >
using CasTdHVectorContainer = midas::mctdh::McTdHVectorContainer<T, NiceTensor, GeneralMidasVector>;

// Alias for restricted-active-space parameterizations
template
   <  typename T
   >
using RasTdHVectorContainer = midas::mctdh::McTdHVectorContainer<T, GeneralTensorDataCont, GeneralMidasVector>;


/***********************************************************************
 * INTERFACE TO ODE AND IES MODULES
 **********************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename SCALAR_T
   >
inline void Axpy
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aOther
   ,  SCALAR_T aScalar
   )
{
   aThis.Axpy(aOther, aScalar);
}

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
inline void Zero
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   )
{
   aThis.Zero();
}

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename SCALAR_T
   >
inline void Scale
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   ,  SCALAR_T aScalar
   )
{
   aThis.Scale(aScalar);
}

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
inline auto Norm
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   )
{
   return aThis.Norm();
}

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
inline size_t Size
   (  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   )
{
   return aThis.TotalSize();
}

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void SetShape
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aThis
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aOther
   )
{
   SetShape(aThis.Modals(), aOther.Modals());
   SetShape(aThis.Coefs(), aOther.Coefs());
}

/**
 * Norm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations I - Nonstiff Problems, page 167-168
 *
 * ||e||^2 = (1/n) * sum_i (e[i] / sc[i])^2
 *
 * with sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol
 *
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename STEP_T
   >
typename midas::type_traits::RealTypeT<PARAM_T> OdeMeanNorm2
   (  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aYOld
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aYNew
   )
{
   using real_t = typename midas::type_traits::RealTypeT<PARAM_T>;
   assert_same_shape(aDeltaY, aYOld);
   assert_same_shape(aDeltaY, aYNew);

   real_t result(0.);

   // Modals contribution
   result += OdeNorm2(aDeltaY.Modals(), aAbsTol, aRelTol, aYOld.Modals(), aYNew.Modals());

   // Coefs contribution
   result += OdeNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs());

   // Divide by total size
   result /= aDeltaY.TotalSize();

   return result;
}

/**
 * MaxNorm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations I - Nonstiff Problems, page 167-168
 *
 * ||e||^2 = max_i (e[i] / sc[i])^2
 *
 * with sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol
 *
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename STEP_T
   >
typename midas::type_traits::RealTypeT<PARAM_T> OdeMaxNorm2
   (  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aYOld
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aYNew
   )
{
   using real_t = typename midas::type_traits::RealTypeT<PARAM_T>;
   assert_same_shape(aDeltaY, aYOld);
   assert_same_shape(aDeltaY, aYNew);

   // Sizes
   auto modals_size = aDeltaY.Modals().size();
   auto coefs_size = aDeltaY.Coefs().TotalSize();
   real_t result(0.);

   // Modals contribution
   result = std::max(result, OdeMaxNorm2(aDeltaY.Modals(), aAbsTol, aRelTol, aYOld.Modals(), aYNew.Modals()));

   // Coefs contribution
   result = std::max(result, OdeMaxNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs()));

   return result;
}

/**
 * Copy data to pointer (used for interfacing with GSL in ode).
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename PTR_T
   >
void DataToPointer
   (  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aVec
   ,  PTR_T* apPtr
   )
{
   auto modals_size = Size(aVec.Modals());
   if constexpr   (  midas::type_traits::IsComplexV<PARAM_T>
                  )
   {
      modals_size *= 2;
   }

   // Put modals first
   DataToPointer(aVec.Modals(), apPtr);

   // Put coefs second
   DataToPointer(aVec.Coefs(), apPtr+modals_size);
}

/**
 * Set data from pointer (used for interfacing with GSL in ode).
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  typename PTR_T
   >
void DataFromPointer
   (  midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aVec
   ,  const PTR_T* apPtr
   )
{
   auto modals_size = Size(aVec.Modals());
   if constexpr   (  midas::type_traits::IsComplexV<PARAM_T>
                  )
   {
      modals_size *= 2;
   }

   // Get modals first
   DataFromPointer(aVec.Modals(), apPtr);

   // Put coefs second
   DataFromPointer(aVec.Coefs(), apPtr+modals_size);
}

#endif /* MCTDHVECTORCONTAINER_H_INCLUDED */
