/*
************************************************************************
*
* @file                 McTdHVectorContainer_Decl.h
*
* Created:              01-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Container for McTdH data. Declarations.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHVECTORCONTAINER_DECL_H_INCLUDED
#define MCTDHVECTORCONTAINER_DECL_H_INCLUDED

#include "util/type_traits/Complex.h"
#include <vector>
#include "tensor/NiceTensor.h"
#include "vcc/TensorDataCont.h"
#include "vcc/VccStateSpace.h"

// Forward decl
class McTdHCalcDef;

template
   <  typename T
   >
class NiceTensor;

template
   <  typename T
   >
class GeneralTensorDataCont;


namespace midas::mctdh
{

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
class McTdHVectorContainer;

} /* namespace midas::mctdh */

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
std::ostream& operator<<(std::ostream&, const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>&);

namespace midas::mctdh
{
/**
 * Container for MCTDH wave functions.
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
class McTdHVectorContainer
{
   public:
      //! Aliases
      using param_t = PARAM_T;
      using coefs_t = COEFS_T<param_t>;
      using modal_t = MODAL_T<param_t>;
      using real_t = typename midas::type_traits::RealTypeT<param_t>;

   private:
      //! Time-dependent modals
      modal_t mModals;

      //! Configuration-space coefficients
      coefs_t mCoefs;


      /** @name IO **/
      //!@{

      //! Write modals
      void WriteModalsImpl
         (  const std::string& aFileName
         )  const;

      //! Write coefs
      void WriteCoefsImpl
         (  const std::string& aFileName
         )  const;

      //! Read modals
      bool ReadModalsImpl
         (  const std::string& aFileName
         );

      //! Read coefs
      bool ReadCoefsImpl
         (  const std::string& aFileName
         );

      //!@}


   public:
      //! Default constructor
      McTdHVectorContainer
         (
         )  = default;

      //! Constructor for NiceTensor case
      template
         <  typename C = COEFS_T<PARAM_T>
         >
      McTdHVectorContainer
         (  const std::vector<unsigned>& aActiveSpace
         ,  size_t aModalsVecSize
         ,  typename std::enable_if_t<std::is_same_v<C, NiceTensor<PARAM_T>>>* = nullptr
         );

      //! Constructor for TensorDataCont case
      template
         <  typename C = COEFS_T<PARAM_T>
         >
      McTdHVectorContainer
         (  const VccStateSpace& aStateSpace
         ,  size_t aModalsVecSize
         ,  typename BaseTensor<PARAM_T>::typeID aType = BaseTensor<PARAM_T>::typeID::SIMPLE
         ,  typename std::enable_if_t<std::is_same_v<C, GeneralTensorDataCont<PARAM_T>>>* = nullptr
         );

      //! Constructor which only initializes modals
      McTdHVectorContainer
         (  size_t aModalsVecSize
         );

      //! Default copy constructor
      McTdHVectorContainer
         (  const McTdHVectorContainer&
         )  = default;

      //! Default move constructor
      McTdHVectorContainer
         (  McTdHVectorContainer&&
         )  = default;
      
      //! Default copy assignment 
      McTdHVectorContainer& operator=
         (  const McTdHVectorContainer&
         )  = default;
      
      //! Default move assignment 
      McTdHVectorContainer& operator=
         (  McTdHVectorContainer&&
         )  = default;

      //! Sanity check
      bool SanityCheck
         (
         )  const;

      //! Axpy. Return ref to this.
      template
         <  typename SCALAR_T
         >
      McTdHVectorContainer& Axpy
         (  const McTdHVectorContainer& aOther
         ,  SCALAR_T aScalar
         );

      //! Scaling. Return ref to this.
      template
         <  typename SCALAR_T
         >
      McTdHVectorContainer& Scale
         (  SCALAR_T aScalar
         );
      
      //! Zero. Return ref to this.
      McTdHVectorContainer& Zero
         (
         );

      //! TotalSize. Total number of elements.
      size_t TotalSize
         (
         )  const;

      //! Norm2
      real_t Norm2
         (
         )  const;

      //! Norm
      real_t Norm
         (
         )  const
      {
         return std::sqrt(this->Norm2());
      }

      //! Reference to coefficients
      coefs_t& Coefs
         (
         )
      {
         return this->mCoefs;
      }

      //! Const reference to coefficients
      const coefs_t& Coefs
         (
         )  const
      {
         return this->mCoefs;
      }

      //! Reference to modals
      modal_t& Modals
         (
         )
      {
         return this->mModals;
      }

      //! Const reference to modals
      const modal_t& Modals
         (
         )  const
      {
         return this->mModals;
      }

      //! Write to disc
      void WriteToDisc
         (  const std::string& aFilePrefix
         )  const;

      //! Read from disc
      bool ReadFromDisc
         (  const std::string& aFilePrefix
         );

      //! Output
      friend std::ostream& operator<< <>(std::ostream&, const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>&);
};

//! Dimensions check
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void assert_same_shape
   (  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>&
   ,  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>&
   );

//! Wave-function overlap
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  template<typename> typename COEFS_U
   >
PARAM_T wave_function_overlap
   (  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>&
   ,  const McTdHVectorContainer<PARAM_T, COEFS_U, MODAL_T>&
   ,  const std::vector<In>& = {}
   ,  const std::set<In>& = {}
   ,  const std::set<In>& = {}
   );

} /* namespace midas::mctdh */

#endif /* MCTDHVECTORCONTAINER_DECL_H_INCLUDED */
