/*
************************************************************************
*
* @file                 McTdHVectorContainer_Impl.h
*
* Created:              15-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Container for McTdH data. Implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHVECTORCONTAINER_IMPL_H_INCLUDED
#define MCTDHVECTORCONTAINER_IMPL_H_INCLUDED

#include <numeric>

#include "td/mctdh/container/McTdHVectorContainer_Decl.h"

#include "util/SanityCheck.h"
#include "mmv/MidasVector.h"
#include "tensor/NiceTensor.h"
#include "td/mctdh/eom/mctdh_tensor_utils.h"

namespace midas::mctdh
{

/**
 * c-tor for NiceTensor
 *
 * @param aActiveSpace     Size of active space = dimensions of coef tensor
 * @param aModalsVecSize   Length of modals vector
 * @param apEnableIf
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
template
   <  typename C
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::McTdHVectorContainer
   (  const std::vector<unsigned>& aActiveSpace
   ,  size_t aModalsVecSize
   ,  typename std::enable_if_t<std::is_same_v<C, NiceTensor<PARAM_T>>>* apEnableIf
   )
   :  mModals
         (  aModalsVecSize
         ,  param_t(0.)
         )
   ,  mCoefs
         (  aActiveSpace
         ,  BaseTensor<param_t>::typeID::SIMPLE
         )
{
}

/**
 * c-tor for GeneralTensorDataCont
 *
 * @param aStateSpace      VccStateSpace for constructing TensorDataCont
 * @param aModalsVecSize   Length of modals vector
 * @param aType            Tensor type (defaulted to SIMPLE)
 * @param apEnableIf
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
template
   <  typename C
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::McTdHVectorContainer
   (  const VccStateSpace& aStateSpace
   ,  size_t aModalsVecSize
   ,  typename BaseTensor<param_t>::typeID aType
   ,  typename std::enable_if_t<std::is_same_v<C, GeneralTensorDataCont<PARAM_T>>>* apEnableIf
   )
   :  mModals
         (  aModalsVecSize
         ,  param_t(0.)
         )
   ,  mCoefs
         (  aStateSpace
         ,  aType
         )
{
}

/**
 * c-tor that only sets modals and calls default c-tor for coefs
 *
 * @param aModalsVecSize   Length of modals vector
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::McTdHVectorContainer
   (  size_t aModalsVecSize
   )
   :  mModals
         (  aModalsVecSize
         ,  param_t(0.)
         )
   ,  mCoefs
         (
         )
{
}

/**
 * Axpy.
 *
 * @param aOther
 * @param aScalar
 * @return
 *    this
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
template
   <  typename SCALAR_T
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::Axpy
   (  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aOther
   ,  SCALAR_T aScalar
   )
{
   ::Axpy(mModals, aOther.Modals(), aScalar);
   ::Axpy(mCoefs, aOther.Coefs(), aScalar);

   return *this;
}

/**
 * Scaling. Return pointer to this.
 *
 * @param aScalar
 * @return
 *    this
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
template
   <  typename SCALAR_T
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::Scale
   (  SCALAR_T aScalar
   )
{
   mModals.Scale(aScalar);
   mCoefs.Scale(aScalar);

   return *this;
}

/**
 * Zero.
 *
 * @return
 *    this
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::Zero
   (
   )
{
   mModals.Zero();
   mCoefs.Zero();

   return *this;
}

/**
 * Norm2. Just viewing this as one big vector of numbers.
 *
 * @return
 *    Norm2
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
typename McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::real_t McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::Norm2
   (
   )  const
{
   real_t result(0.);

   result += mModals.Norm2();

   result += mCoefs.Norm2();

   return result;
}

/**
 * TotalSize. Total number of elements.
 *
 * @return
 *    Total size
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
size_t McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::TotalSize
   (
   )  const
{
   size_t result = Size(mModals) + Size(mCoefs);

   return result;
}

/**
 * Sanity check
 *
 * @return
 *    true if everything is fine
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
bool McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::SanityCheck
   (
   )  const
{
   bool modals_sane = midas::util::ContainerSanityCheck(mModals);
   bool coefs_sane = midas::util::ContainerSanityCheck(mCoefs);

   return modals_sane && coefs_sane;
}

/**
 * Write to disc
 *
 * @param aFilePrefix
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::WriteToDisc
   (  const std::string& aFilePrefix
   )  const
{
   this->WriteModalsImpl(aFilePrefix + "_Modals");
   this->WriteCoefsImpl (aFilePrefix + "_Coefs" );
}

/**
 * Write modals to disc
 *
 * @param aFileName
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::WriteModalsImpl
   (  const std::string& aFileName
   )  const
{
   if constexpr   (  std::is_same_v<modal_t, GeneralMidasVector<param_t> >
                  )
   {
      GeneralDataCont<param_t> dc_out(this->Modals(), "ONDISC", aFileName, true);
   }
   else
   {
      MIDASERROR("McTdHVectorContainer: Cannot write modals of type '" + std::string(typeid(modal_t).name()) + "'.");
   }
}

/**
 * Write coefs to disc
 *
 * @param aFileName
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::WriteCoefsImpl
   (  const std::string& aFileName
   )  const
{
   if constexpr   (  std::is_same_v<coefs_t, NiceTensor<param_t> >
                  || std::is_same_v<coefs_t, GeneralTensorDataCont<param_t> >
                  )
   {
      this->Coefs().WriteToDisc(aFileName);
   }
   else
   {
      MIDASERROR("McTdHVectorContainer: Cannot write coefs of type '" + std::string(typeid(coefs_t).name()) + "'.");
   }
}

/**
 * Read modals from disc.
 *
 * @note mModals must have correct size to begin with!
 *
 * @param aFileName
 * @return
 *    True if success
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
bool McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::ReadModalsImpl
   (  const std::string& aFileName
   )
{
   if constexpr   (  std::is_same_v<modal_t, GeneralMidasVector<param_t> >
                  )
   {
      if (  !InquireFile(aFileName + "_0")
         )
      {
         return false;
      }

      // Get size of vector
      auto size = Size(this->Modals());
      MidasAssert(size != 0, "Cannot read modals into empty vector!");

      // Get handle to file (using DataCont)
      GeneralDataCont<param_t> dc_in;
      dc_in.GetFromExistingOnDisc(size, aFileName);
      dc_in.SaveUponDecon(true);

      // Transfer data from file
      dc_in.DataIo(IO_GET, this->Modals(), size);

      return true;
   }
   else
   {
      MIDASERROR("McTdHVectorContainer: Cannot read modals of type '" + std::string(typeid(modal_t).name()) + "'.");
   }
}

/**
 * Read coefs from disc
 *
 * @param aFileName
 * @return
 *    True if success
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
bool McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::ReadCoefsImpl
   (  const std::string& aFileName
   )
{
   if constexpr   (  std::is_same_v<coefs_t, NiceTensor<param_t> >
                  || std::is_same_v<coefs_t, GeneralTensorDataCont<param_t> >
                  )
   {
      return this->Coefs().ReadFromDisc(aFileName, false); // Disable hard error if file not found
   }
   else
   {
      MIDASERROR("McTdHVectorContainer: Cannot read coefs of type '" + std::string(typeid(coefs_t).name()) + "'.");
      return false;
   }
}

/**
 * Read from disc
 *
 * @param aFilePrefix
 * @return
 *    True if success
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
bool McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>::ReadFromDisc
   (  const std::string& aFilePrefix
   )
{
   return   this->ReadModalsImpl(aFilePrefix + "_Modals")
       &&   this->ReadCoefsImpl (aFilePrefix + "_Coefs" );
}

/** 
 * Dimensions check
 * 
 * @param aFirst
 * @param aSecond
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
void assert_same_shape
   (  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aFirst
   ,  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aSecond
   )
{
   assert_same_shape(aFirst.Coefs(), aSecond.Coefs());

   MidasAssert(Size(aFirst.Modals()) == Size(aSecond.Modals()), "McTdHVectorContainer: assert_same_shape failed for mModals!");
}

/**
 * Wave-function overlap
 *
 * @param aBra       Bra
 * @param aKet       Ket
 * @param aNBasVec   Number of primitive basis functions for each mode. Only needed if not the same for all modes.
 * @param aBraExtRangeModes
 * @param aKetExtRangeModes
 * @return
 *    Overlap: <aBra|aKet>
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   ,  template<typename> typename COEFS_U
   >
PARAM_T wave_function_overlap
   (  const McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& aBra
   ,  const McTdHVectorContainer<PARAM_T, COEFS_U, MODAL_T>& aKet
   ,  const std::vector<In>& aNBasVec
   ,  const std::set<In>& aBraExtRangeModes
   ,  const std::set<In>& aKetExtRangeModes
   )
{
   using bra_coefs_t = COEFS_T<PARAM_T>;
   using ket_coefs_t = COEFS_U<PARAM_T>;

   const auto& bra_modals = aBra.Modals();
   const auto& ket_modals = aKet.Modals();
   auto n_bra_modal_coef = bra_modals.size();
   auto n_ket_modal_coef = ket_modals.size();

   const auto& input_bra_coefs = aBra.Coefs();
   const auto& input_ket_coefs = aKet.Coefs();

   // Convert all coefs to NiceTensor if needed
   NiceTensor<PARAM_T> bra_coefs;
   constexpr bool bra_is_tensor = std::is_same_v<bra_coefs_t, NiceTensor<PARAM_T>>;
   if constexpr   (  !bra_is_tensor
                  )
   {
      static_assert(std::is_same_v<bra_coefs_t, GeneralTensorDataCont<PARAM_T>>, "Coefs must be NiceTensor or TensorDataCont.");
      bra_coefs = NiceTensorFromTensorDataCont(input_bra_coefs, aBraExtRangeModes);
   }
   else
   {
      bra_coefs = input_bra_coefs;
   }

   NiceTensor<PARAM_T> trans_ket_coefs;
   if constexpr   (  std::is_same_v<ket_coefs_t, NiceTensor<PARAM_T>>
                  )
   {
      trans_ket_coefs = input_ket_coefs;
   }
   else
   {
      static_assert(std::is_same_v<ket_coefs_t, GeneralTensorDataCont<PARAM_T>>, "Coefs must be NiceTensor or TensorDataCont.");
      trans_ket_coefs = NiceTensorFromTensorDataCont(input_ket_coefs, aKetExtRangeModes);
   }

   // Assert same number of modes
   MidasAssert(bra_coefs.NDim() == trans_ket_coefs.NDim(), "MCTDH wave functions have different numbers of modes");

   // Transform ket coefs
   bool nbas_avail = !aNBasVec.empty();
   auto nmodes = bra_coefs.NDim();
   auto n_ket_tdmodals = std::accumulate(trans_ket_coefs.GetDims().begin(), trans_ket_coefs.GetDims().end(), 0);
   auto n_bra_tdmodals = std::accumulate(bra_coefs.GetDims().begin(), bra_coefs.GetDims().end(), 0);
   MidasAssert(nbas_avail || (n_ket_modal_coef % n_ket_tdmodals == 0 && n_bra_modal_coef % n_bra_tdmodals == 0), "Number of modal coefs is not divisable by number of modes.");
   auto nbas = nbas_avail ? aNBasVec.front() : n_ket_modal_coef / n_ket_tdmodals;

   // Perform forward contractions with modal overlaps for all modes
   std::vector<In> bra_off;
   std::vector<In> ket_off;
   In bra_modal_coef_count = I_0;
   In ket_modal_coef_count = I_0;
   for(In imode=I_0; imode<nmodes; ++imode)
   {
      if (  nbas_avail
         )
      {
         nbas = aNBasVec[imode];
      }
      auto bra_nact = bra_coefs.Extent(imode);
      auto ket_nact = trans_ket_coefs.Extent(imode);

      // Set offsets for imode
      bra_off.resize(bra_nact);
      ket_off.resize(ket_nact);
      for(In iact=I_0; iact<bra_nact; ++iact)
      {
         bra_off[iact] = bra_modal_coef_count;
         bra_modal_coef_count += nbas;
      }
      for(In iact=I_0; iact<ket_nact; ++iact)
      {
         ket_off[iact] = ket_modal_coef_count;
         ket_modal_coef_count += nbas;
      }

      // Construct overlap matrix (as SimpleTensor)
      auto overlap_ptr = std::make_unique<PARAM_T[]>(bra_nact*ket_nact);

      // Calculate overlaps
      auto* ptr = overlap_ptr.get();
      for(In v=I_0; v<bra_nact; ++v)
      {
         for(In w=I_0; w<ket_nact; ++w)
         {
            PARAM_T val(0.);
            for(In ibas=I_0; ibas<nbas; ++ibas)
            {
               val += midas::math::Conj(bra_modals[bra_off[v]+ibas]) * ket_modals[ket_off[w]+ibas];
            }
            *(ptr++) = val;
         }
      }

      // Do forward contraction
      detail::DoForwardContraction(trans_ket_coefs, overlap_ptr, imode, bra_nact);
   } 

   // Assert same shape after transforms
   assert_same_shape(bra_coefs, trans_ket_coefs);

   // Do dot product
   return dot_product(bra_coefs, trans_ket_coefs);
}

} /* namespace midas::mctdh */


/**
 * Output
 *
 * @param os
 * @param self
 *
 * @return
 *    ostream ref
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
std::ostream& operator<<
   (  std::ostream& os
   ,  const midas::mctdh::McTdHVectorContainer<PARAM_T, COEFS_T, MODAL_T>& self
   )
{
   os << " === McTdHVectorContainer output begin ===\n"
      << "   = Modals:\n"
      << self.mModals  << "\n"
      << "   = Coefs:\n"
      << self.mCoefs   << "\n"
      << " === McTdHVectorContainer output end ===\n"
      << std::flush;

   return os;
}

#endif /* MCTDHVECTORCONTAINER_IMPL_H_INCLUDED */
