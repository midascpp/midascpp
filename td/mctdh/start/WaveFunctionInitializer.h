/*
************************************************************************
*
* @file                 WaveFunctionInitializer.h
*
* Created:              29-11-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class for initializing MCTDH wave functions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef WAVEFUNCTIONINITIALIZER_H_INCLUDED
#define WAVEFUNCTIONINITIALIZER_H_INCLUDED

#include "PrimitiveBasisTransformer.h"
#include "input/McTdHCalcDef.h"
#include "input/Input.h"
#include "tensor/NiceTensor.h"
#include "vcc/TensorDataCont.h"

namespace midas::mctdh
{

/**
 * Class for initializing wave function for MCTDH calculations.
 *
 * This can be done from
 *    -  VSCF
 *    -  VCI
 *    -  TDH
 *    -  MCTDH
 **/
class WaveFunctionInitializer
   :  public PrimitiveBasisTransformer
{
   private:
      //! Alias
      using param_t = std::complex<Nb>;
      using real_t = Nb;

      //! Pointer to CalcDef
      const input::McTdHCalcDef* const mpCalcDef = nullptr;

      //! Type of initial wave function
      input::McTdHCalcDef::initType mType = input::McTdHCalcDef::initType::VCI;

      //! File name for modals
      std::string mModalsFilename = "";

      //! File name for coefficients
      std::string mCoefsFilename = "";

      //! Number of modes in reference calculation
      In mNModesRef = I_0;

      //! Number of modals for each mode in reference calculation. Relevant if truncated!
      std::vector<In> mNModalsRef = {};

      //! Number of modal coefficients in the reference w.f.
      In mNReferenceModalCoefs = I_0;

      //! Index of reference calculation in the relevant g<type>CalcDef
      In mCalcNr = -I_1;


      //! Read modals
      bool ReadModalsImpl
         (  ComplexMidasVector& arModals
         )  const;

      //! Read modals from any type of GeneralMidasVector
      template
         <  typename T
         >
      ComplexMidasVector GetModalCoefsFromDisc
         (  bool aPhaseIncluded
         )  const;

      //! Read coefs into complex NiceTensor
      bool ReadCoefsImpl
         (  NiceTensor<param_t>& arCoefs
         )  const;

      //! Read coefs into complex TensorDataCont
      bool ReadCoefsImpl
         (  ComplexTensorDataCont& arCoefs
         )  const;

      //! Sanity check
      template
         <  typename INT
         >
      void CheckNModals
         (  const std::vector<INT>& 
         )  const;


   public:
      //! c-tor from calcdef
      WaveFunctionInitializer
         (  const input::McTdHCalcDef* const apCalcDef
         ,  const OpDef* const apOpDef
         ,  const BasDef* const apBasDef
         );

      //! Read wave function
      template
         <  typename WaveFunction
         >
      bool ReadInitialWaveFunction
         (  WaveFunction& arY
         )  const;

      //! Get CalcDef
      const input::McTdHCalcDef* const CalcDef
         (
         )  const;
};


/**
 * Read wave function
 *
 * @param arY                    Reference to initial wave function
 *
 * @return
 *    Read in okay?
 **/
template
   <  typename WaveFunction
   >
bool WaveFunctionInitializer::ReadInitialWaveFunction
   (  WaveFunction& arY
   )  const
{
   if (  mType == input::McTdHCalcDef::initType::MCTDH
      )
   {
      // McTdHVectorContainer only needs one file prefix
      arY.Modals().SetNewSize(this->NSpectralModalCoef());
      arY.ReadFromDisc(this->mModalsFilename);

      return true;
   }
   else
   {
      //! Read modals
      Mout  << " MCTDH WaveFunctionInitializer: Reading modals." << std::endl;
      bool modals_read = this->ReadModalsImpl(arY.Modals());

      //! Read coefs
      Mout  << " MCTDH WaveFunctionInitializer: Reading coefs." << std::endl;
      bool coefs_read = this->ReadCoefsImpl(arY.Coefs());

      return modals_read && coefs_read;
   }
}

/**
 * Read modals from any type of GeneralMidasVector
 *
 * @param aPhaseIncluded         Is there a phase included in the saved vector?
 * @return
 *    Vector of complex coefs
 **/
template
   <  typename T
   >
ComplexMidasVector WaveFunctionInitializer::GetModalCoefsFromDisc
   (  bool aPhaseIncluded
   )  const
{
   MidasAssert(!aPhaseIncluded, "Reading modals for TDH initialization has not been fully implemented.");

   size_t size = 0;
   In size_mode_0 = -I_1;
   // Get size of result
   for(In imode=I_0; imode<mNModesRef; ++imode)
   {
      const auto& nbas = mNModalsRef[imode];
      const auto& nact = this->NModalsVec()[imode];

      size += nbas*nact;

      if (  imode == I_0
         )
      {
         size_mode_0 = nbas*nact;
      }
   }
   // Init result
   ComplexMidasVector result(size);

   // Get initial modals
   GeneralDataCont<T> init_modals;
   size_t read_size = this->mNReferenceModalCoefs;
   if (  aPhaseIncluded
      )
   {
      ++read_size;
   }
   init_modals.GetFromExistingOnDisc(read_size, this->mModalsFilename);
   init_modals.SaveUponDecon(true);

   // Loop over modes
   GeneralMidasMatrix<T> modal_mat;
   In count = I_0;
   const auto& offs = this->SpectralModalOffsets();
   for(In imode=I_0; imode<mNModesRef; ++imode)
   {
      const auto& nbas = mNModalsRef[imode];
      const auto& off_m = offs[imode];
      const auto& nact = this->NModalsVec()[imode];
      
      // Set size of matrix
      modal_mat.SetNewSize(nbas, nbas);

      // Read modal coefs into matrix
      init_modals.DataIo(IO_GET, modal_mat, nbas*nbas, nbas, nbas, off_m);

      if (  this->mpCalcDef->ExponentialModalTransform()
         )
      {
         MIDASERROR("Not implemented for exponential modal transform!");
      }
      else
      {
         // Set elements of complex vector
         for(In imodal=I_0; imodal<nact; ++imodal)
         {
            for(In icoef=I_0; icoef<nbas; ++icoef)
            {
               result[count++] = param_t(modal_mat[imodal][icoef]);
            }
         }
      }
   }

   // Get phase and multiply on first modal
   if (  aPhaseIncluded
      )
   {
      T phase;
      init_modals.DataIo(IO_GET, read_size-1, phase);

      if (  !libmda::numeric::float_numeq_zero(std::imag(phase), std::real(phase), 10)
         )
      {
         MidasWarning("Ignoring non-vanishing imaginary part of TDH phase factor in initializing MCTDH modals.");
      }

      for(In i=I_0; i<size_mode_0; ++i)
      {
         result[i] *= std::exp(CC_M_I*std::real(phase));
      }
   }

   return result;
}

/**
 * Sanity check on number of modals from reference calculation
 **/
template
   <  typename INT
   >
void WaveFunctionInitializer::CheckNModals
   (  const std::vector<INT>& aModals
   )  const
{
   if (  aModals.size() != this->NModalsVec().size()
      )
   {
      Mout  << " VCI nmodals size:     " << aModals.size() << "\n"
            << " MCTDH nmodals size:   " << this->NModalsVec().size() << "\n"
            << std::flush;
      MIDASERROR("Reference VCI nmodals vector contains the wrong number of elements!");
   }
   bool nmodals_okay = true;
   for(In i=I_0; i<aModals.size(); ++i)
   {
      if (  aModals[i] != INT(this->NModalsVec()[i])
         )
      {
         nmodals_okay = false;
         break;
      }
   }
   if (  !nmodals_okay
      )
   {
      Mout  << " VCI nmodals:    " << aModals << "\n"
            << " MCTDH nmodals:  " << this->NModalsVec() << "\n"
            << std::flush;
      MIDASERROR("Mismatch in number of modals for VCI reference and MCTDH!");
   }
}

} /* namespace midas::mctdh */

#endif /* WAVEFUNCTIONINITIALIZER_H_INCLUDED */
