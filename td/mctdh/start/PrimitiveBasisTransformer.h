/*
************************************************************************
*
* @file                 PrimitiveBasisTransformer.h
*
* Created:              03-12-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class for transforming integrals to an orthonormal basis
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef PRIMITIVEBASISTRANSFORMER_H_INCLUDED
#define PRIMITIVEBASISTRANSFORMER_H_INCLUDED

#include <vector>
#include <string>
#include "mmv/DataCont.h"

#include "input/Input.h"

// Forward decl
class BasDef;

namespace midas::mctdh
{
/**
 * Class for transforming to spectral basis for a given operator
 *
 **/
class PrimitiveBasisTransformer
{
   private:
      //! Pointer to BasDef
      const BasDef* const mpBasDef = nullptr;

      //! Modals for transforming basis to spectral basis
      DataCont mSpectralBasisModals;

      //! Offsets for spectral-basis modals
      std::vector<In> mSpectralModalOffsets = {};

      //! Basis limits
      std::vector<In> mModalBasisLimits;

      //! Number of modal coefs when accounting for basis limits
      In mNSpectralModalCoef = I_0;

      //! Are we using spectral basis of general operator (not reference VSCF)
      bool mGeneralModalBasis = false;

      //! Number of electronic states
      In mNElectronicStates = I_1;

      //! Mode number for electronic DOF
      GlobalModeNr mElectronicDofNr = -I_1;

      //! Are we doing non-adiabatic dynamics
      bool mNonAdiabatic = false;

      //! Number of modals/basis functions for all DOFs in MCTDH
      std::vector<unsigned> mNModalsVec = {};



   protected:
      //! Read spectral-basis modals
      void ReadSpectralBasisModals
         (  In aNCoefs
         ,  const std::string& aFileName
         );

      //! Calcaulte spectral-basis modals using VSCF
      void CalculateSpectralBasisModals
         (  const std::string& aOperName
         ,  const std::vector<unsigned>& aKeepNRefModals = {}
         ,  In aNRefModalCoefs = I_0
         ,  const std::string& aRefModalsFileName = ""
         );

      //! Transform integrals
      template
         <  typename INTEGRALS
         >
      void TransformIntegrals
         (  std::vector<INTEGRALS>& arIntsVec
         )  const;

      //! Transform initial wave function
      template
         <  typename WaveFunction
         ,  typename INTEGRALS
         ,  typename Offsets
         >
      void TransformInitialWaveFunction
         (  WaveFunction& arY
         ,  const INTEGRALS& aInts
         ,  const Offsets& aModalOffsets
         ,  const std::vector<unsigned>& aActiveSpace
         )  const;

   public:
      //! Constructor from BasDef
      PrimitiveBasisTransformer
         (  const input::McTdHCalcDef* const apCalcDef
         ,  const BasDef* const apBasDef
         ,  const OpDef* const apOpDef
         );

      //! Get modals for spectral-basis transform
      const DataCont& SpectralBasisModals
         (
         )  const;

      //! Get offsets in SpectralBasisModals
      const std::vector<In>& SpectralModalOffsets
         (
         )  const;

      //! Get BasDef
      const BasDef* const GetBasDef
         (
         )  const;

      //! Get limits
      const std::vector<In>& ModalBasisLimits
         (
         )  const;

      //! Get number of electronic states
      In NElectronicStates
         (
         )  const;

      //! Get ElectronicDofNr
      GlobalModeNr ElectronicDofNr
         (
         )  const;

      //! Are we doing non-adiabatic dynamics?
      bool NonAdiabatic
         (
         )  const;

      //! Get vector of number of modals
      const std::vector<unsigned>& NModalsVec
         (
         )  const;

      //! Get number of modal coefs
      In NSpectralModalCoef
         (
         )  const;
};


/**
 * Transform integrals
 *
 * @param arIntsVec        Reference to integrals. Assumes that they inherit from PrimitiveIntegrals (or have same interface).
 **/
template
   <  typename INTEGRALS
   >
void PrimitiveBasisTransformer::TransformIntegrals
   (  std::vector<INTEGRALS>& arIntsVec
   )  const
{
   for(auto& ints : arIntsVec)
   {
      const auto& opdef = ints.Oper().Oper();

      // Debug
      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         Mout  << " Transforming integrals for operator '" << opdef.Name() << "' in primitive basis '" << ints.GetBasDef()->GetmBasName() << "'.\n"
               << "    Modal offsets: " << this->mSpectralModalOffsets << std::endl;
      }

      // Do transform
      ints.TransformModalBasis(this->mSpectralBasisModals, this->mSpectralModalOffsets, this->mModalBasisLimits);

      // Debug
      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         auto nmodes = this->mpBasDef->GetmNoModesInBas();
         for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
         {
            Mout  << " Overlap integrals for mode " << imode << " after PrimitiveBasisTransformer::TransformIntegrals:\n" << ints.GetOverlapIntegrals(imode) << std::endl;
         }
      }
   }

   if (  gDebug
      || gMcTdHIoLevel >= I_10
      )
   {
      Mout  << " Transformation of integrals to spectral basis done!" << std::endl;
   }
}

/**
 * Transform initial wave function to new primitive basis. This transformation reverts the effect of the integral transformation.
 * Thus, the active modals are not transformed among each other, and the configuration-space coefficients should not be modified.
 *
 * NB: This function assumes a linear parameterization of the active modals.
 *     For exp. parameterizations, we want to start with kappa=0.
 *
 * @param arY              Reference to wave function
 * @param aInts            Integrals. For access to overlap matrix.
 * @param aModalOffsets    Offsets in modal vector for spectral-basis modals (i.e. not truncated virtual space)
 * @param aActiveSpace     Dimensions of active space (including electronic DOF in case of non-adiabatic dynamics)
 **/
template
   <  typename WaveFunction
   ,  typename INTEGRALS
   ,  typename Offsets
   >
void PrimitiveBasisTransformer::TransformInitialWaveFunction
   (  WaveFunction& arY
   ,  const INTEGRALS& aInts
   ,  const Offsets& aModalOffsets
   ,  const std::vector<unsigned>& aActiveSpace
   )  const
{
   // Init transpose modal matrix
   MidasMatrix c_t_m;

   // Init overlap matrix
   ComplexMidasMatrix s_m;

   // Init active-modal matrix
   ComplexMidasMatrix act_modals_m;

   // Aux. matrices
   ComplexMidasMatrix half_trans_m;

   const auto& opdef = aInts.Oper().Oper();

   // Get offsetes for primitive basis for reading in modals
   std::vector<std::vector<In>> prim_modal_offsets(aModalOffsets.size());
   for(In i=I_0; i<aModalOffsets.size(); ++i)
   {
      prim_modal_offsets[i].resize(aModalOffsets[i].size());
   }
   size_t n_prim_modal_coef = 0;
   for(LocalModeNr i_op_mode=I_0; i_op_mode<aActiveSpace.size(); ++i_op_mode)
   {
      GlobalModeNr g_mode = opdef.GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = this->mpBasDef->GetLocalModeNr(g_mode);
      In nbas = this->mpBasDef->Nbas(i_bas_mode);
      In nact = aActiveSpace[i_op_mode];

      // Set offsets
      for(In iact=I_0; iact<nact; ++iact)
      {
         prim_modal_offsets[i_op_mode][iact] = n_prim_modal_coef + iact*nbas;
      }

      // Add to number of modal coefs
      n_prim_modal_coef += nbas * nact;
   }

   // Transform modals
   bool has_limits = !this->mModalBasisLimits.empty();
   for(const auto& mode_label : gOperatorDefs.GetModeLabels())
   {
      GlobalModeNr g_mode = gOperatorDefs.ModeNr(mode_label, false);
      LocalModeNr i_bas_mode = this->mpBasDef->GetLocalModeNr(g_mode);
      LocalModeNr i_op_mode = opdef.GetLocalModeNr(g_mode);

      // Get nbas/nact
      In nprimbas = this->mpBasDef->Nbas(i_bas_mode); //aInts.NBas(i_op_mode);
      In nspecbas = has_limits ? this->mModalBasisLimits[i_op_mode] : nprimbas;
      In nact = aActiveSpace[i_op_mode];

      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         Mout  << " Transforming active modals for mode: " << mode_label << ". GlobalModeNr: " << g_mode << ". Basis mode: " << i_bas_mode << ". Oper mode: " << i_op_mode << std::endl;
         Mout  << "    nprimbas = " << nprimbas << ". nspecbas = " << nspecbas << ". nact = " << nact << std::endl;
         Mout  << " aModalOffsets (spectral basis): " << aModalOffsets << "\n"
               << " prim_modal_offsets: " << prim_modal_offsets << "\n"
               << std::endl;
      }

      // Get overlap integrals
      s_m.SetNewSize(nprimbas, nprimbas);
      s_m = aInts.GetOverlapIntegrals(i_op_mode);

      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         Mout  << " Overlap matrix:\n" << s_m << std::endl;
      }

      // Get transposed spectral modal matrix
      c_t_m.SetNewSize(nspecbas, nprimbas);
      this->mSpectralBasisModals.DataIo(IO_GET, c_t_m, nspecbas*nprimbas, nspecbas, nprimbas, this->mSpectralModalOffsets[i_op_mode]);

      // Get active modals into matrix
      act_modals_m.SetNewSize(nprimbas, nact);
      for(In iact=I_0; iact<nact; ++iact)
      {
         for(In ibas=I_0; ibas<nprimbas; ++ibas)
         {
            act_modals_m[ibas][iact] = arY.Modals()[prim_modal_offsets[i_op_mode][iact] + ibas];
         }
      }

      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         Mout  << " Active-modals matrix in primitive basis:\n" << act_modals_m << std::endl;
      }

      // Transform modals. First with S, then with C^T.
      // We use that C^T S C = I  =>  C C^T = S^-1  =>  C C^T S = I
      half_trans_m.SetNewSize(nprimbas, nact);
      half_trans_m = s_m * act_modals_m;
      act_modals_m.SetNewSize(nspecbas, nact);
      act_modals_m = c_t_m * half_trans_m;

      if (  gDebug
         || gMcTdHIoLevel >= I_10
         )
      {
         Mout  << "    Modals transformed to spectral basis:\n" << act_modals_m << std::endl;
      }

      // Put active modals back into vector
      if (  arY.Modals().Size() != this->mNSpectralModalCoef
         )
      {
         arY.Modals().SetNewSize(this->mNSpectralModalCoef);
      }
      for(In iact=I_0; iact<nact; ++iact)
      {
         for(In ibas=I_0; ibas<nspecbas; ++ibas)
         {
            arY.Modals()[aModalOffsets[i_op_mode][iact] + ibas] = act_modals_m[ibas][iact];
         }
      }
   }

   if (  gDebug
      || gMcTdHIoLevel >= I_10
      )
   {
      Mout  << "    Repacked into vector:\n" << arY.Modals() << std::endl;
   }
}

} /* namespace midas::mctdh */

#endif /* PRIMITIVEBASISTRANSFORMER_H_INCLUDED */
