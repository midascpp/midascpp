/*
************************************************************************
*
* @file                 McTdHInterface.h
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Interface to McTdH classes
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHINTERFACE_H_INCLUDED
#define MCTDHINTERFACE_H_INCLUDED

#include "McTdHBase.h"
#include "eom/LinearCasTdHEom.h"
#include "eom/LinearRasTdHEom.h"

// "Standard" MCTDH with complete active space and double-linear parameterization
using LinearCasTdH = midas::mctdh::McTdHBase<midas::mctdh::LinearCasTdHEom>;

// Restricted-active-space MCTDH with double-linear parameterization
using LinearRasTdH = midas::mctdh::McTdHBase<midas::mctdh::LinearRasTdHEom>;

#endif /* MCTDHINTERFACE_H_INCLUDED */
