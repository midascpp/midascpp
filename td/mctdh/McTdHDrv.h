/*
************************************************************************
*
* @file                 McTdHDrv.h
*
* Created:              05-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Declaration of McTdHDrv
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHDRV_H_INCLUDED
#define MCTDHDRV_H_INCLUDED

namespace midas::mctdh
{

void McTdHDrv();

} /* namespace midas::mctdh */


#endif /* MCTDHDRV_H_INCLUDED */

