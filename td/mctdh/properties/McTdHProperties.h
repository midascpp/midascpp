/*
************************************************************************
*
* @file                 McTdHProperties.h
*
* Created:              09-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class that holds calculated properties and property integrals.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHPROPERTIES_H_INCLUDED
#define MCTDHPROPERTIES_H_INCLUDED

//! Include declarations
#include "McTdHProperties_Decl.h"

//! Include implementation
#include "McTdHProperties_Impl.h"

#endif /* MCTDHPROPERTIES_H_INCLUDED */
