/*
************************************************************************
*
* @file                 McTdHProperties_Impl.h
*
* Created:              10-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Implementation of McTdHProperties
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHPROPERTIES_IMPL_H_INCLUDED
#define MCTDHPROPERTIES_IMPL_H_INCLUDED


#include "McTdHProperties_Decl.h"
#include "input/McTdHCalcDef.h"
#include "input/Input.h"
#include "util/FileSystem.h"
#include "td/oper/LinCombOper.h"

#include "mpi/Impi.h"

namespace midas::mctdh::detail
{

/**
 * C-tor
 *
 * @param apCalcDef
 **/
template
   <  typename EOM
   >
McTdHProperties<EOM>::McTdHProperties
   (  const input::McTdHCalcDef* const apCalcDef
   )
   :  midas::td::SpectrumCalculator<param_t>    // Init spectrum calculator from TdPropertyDef
         (  apCalcDef
         )
   ,  mpCalcDef
         (  apCalcDef
         )
   ,  mAnalysisDir
         (  mpCalcDef->GetAnalysisDir().empty() ? gAnalysisDir : mpCalcDef->GetAnalysisDir()
         )
{
#ifdef VAR_MPI
   // Only master node saves integrals
   if (  midas::mpi::IsMaster()
      )
   {
#endif

   // Create analysis directory if not found
   if (  !midas::filesystem::Exists(this->mAnalysisDir)
      )
   {
      Mout  << " McTdHProperties: Analysis directory with path '" << this->mAnalysisDir << "' does not exist. I will construct it!" << std::endl;
      midas::filesystem::Mkdir(this->mAnalysisDir);
   }

   const auto& input_properties = this->mpCalcDef->GetProperties();
   for(const auto& input_prop : input_properties)
   {
      auto filename = this->PropertyFileName(input_prop);
      if (  !filename.empty()
         )
      {
         Mout  << input_prop << " will be written to file '" << filename << "'." << std::endl;
         this->mPropertyStreams[input_prop].open(filename, std::ios_base::out);
         this->mPropertyStreams[input_prop] << std::scientific << std::setprecision(16);
      }
      else
      {
         MidasWarning("MCTDH: Unrecognized property: '" + input_prop + "'.");
      }
   }

   // Set up spectra
   static const std::map<std::string, specID> spectrum_map =
   {  {"AUTOCORR", specID::AUTOCORR}
   ,  {"HALFTIMEAUTOCORR", specID::HALFTIMEAUTOCORR}
   };
   const auto& input_spectra = this->mpCalcDef->GetSpectra();
   for(const auto& input_spec : input_spectra)
   {
      if (  spectrum_map.find(input_spec) != spectrum_map.end()
         )
      {
         this->mSpectra.emplace(spectrum_map.at(input_spec));
      }
      else
      {
         MidasWarning("MCTDH: Unrecognized spectrum: '" + input_spec + "'.");
      }
   }

   // 2) Set up expectation values
   const auto& expt_val_opers = this->mpCalcDef->GetExptValOpers();

   // 2a) Locate basis set
   const auto& basisname = this->mpCalcDef->GetBasis();
   auto ibas = -I_1;
   for(In i=I_0; i<gBasis.size(); ++i)
   {
      if (  gBasis[i].GetmBasName() == basisname
         )
      {
         ibas = i;
         break;
      }
   }
   if (  ibas == -I_1
      )
   {
      MIDASERROR("McTdHProperties: Could not find '" + basisname + "' in gBasis!");
   }

   // 2b) Loop over operator names
   for(const auto& oper : expt_val_opers)
   {
      // Locate OpDef for operator name
      auto iop = -I_1;
      for(In i=I_0; i<gOperatorDefs.GetNrOfOpers(); ++i)
      {
         if (  gOperatorDefs[i].Name() == oper
            )
         {
            iop = i;
            break;
         }
      }
      if (  iop == -I_1
         )
      {
         MIDASERROR("McTdHProperties: Operator '" + oper + "' not found in gOperatorDefs!");
      }

      // Init integrals
      midas::td::TdOperTerm<step_t, param_t, OpDef> tdoper(gOperatorDefs[iop], 1);
      this->mExptValIntegrals[oper] = std::make_unique<integrals_t>(apCalcDef, tdoper, &gBasis[ibas]);

      // Init vector of saved expectation values
      std::string propfile_prefix = this->mAnalysisDir + "/" + this->mpCalcDef->GetName();
      auto filename = propfile_prefix + "_mean_" + oper + ".dat";
      Mout  << "<" << oper << "> will be written to file '" << filename << "'." << std::endl;
      this->mExptValStreams[oper].open(filename);
      this->mExptValStreams[oper] << std::scientific << std::setprecision(16);
   }

   // Write headers for all output files
   this->WriteHeaders();

#ifdef VAR_MPI
   // End bracket for MPI
   }
#endif
}

/**
 * Get file name for property
 * @param aProp         Property
 * @return
 *    Path to file containing the property
 **/
template
   <  typename EOM
   >
std::string McTdHProperties<EOM>::PropertyFileName
   (  const std::string& aProp
   )  const
{
   try
   {
      std::string propfile_prefix = this->mAnalysisDir + "/" + this->mpCalcDef->GetName();

      // Set up properties file streams
      // Map property to file name
      static const std::map<std::string, std::string> suffix_map =
      {  {"ENERGY", "_energy.dat"}
      ,  {"AUTOCORR", "_ac.dat"}
      ,  {"HALFTIMEAUTOCORR", "_t12ac.dat"}
      ,  {"NORM", "_norm.dat"}
      ,  {"REFCOEF", "_refcoef.dat"}
      ,  {"DENSANALYSIS", "_densanalysis.dat"}
      ,  {"STATEPOPULATIONS", "_state_populations.dat"}
      };

      return propfile_prefix + suffix_map.at(aProp);
   }
   catch (  const std::out_of_range& oor
         )
   {
      return std::string();
   }
}

/**
 * Write headers for all output files
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::WriteHeaders
   (
   )
{
   const auto& opdef = gOperatorDefs.GetOperator(this->mpCalcDef->GetOper());
   auto ndof = opdef.NmodesInOp();

   // Output for properties
   for(auto& prop : this->mPropertyStreams)
   {
      const auto& name = prop.first;
      auto& stream = prop.second;

      // Time
      stream   << std::setw(mWidth) << "# t";

      if (  name == "ENERGY"
         )
      {
         stream   << std::setw(mWidth) << "Re[E]"
                  << std::setw(mWidth) << "Im[E]";
      }
      else if  (  name == "AUTOCORR"
               )
      {
         stream   << std::setw(mWidth) << "Re[S]"
                  << std::setw(mWidth) << "Im[S]"
                  << std::setw(mWidth) << "|S|";
      }
      else if  (  name == "HALFTIMEAUTOCORR"
               )
      {
         stream   << std::setw(mWidth) << "Re[S_1/2]"
                  << std::setw(mWidth) << "Im[S_1/2]"
                  << std::setw(mWidth) << "|S_1/2|";
      }
      else if  (  name == "NORM"
               )
      {
         stream   << std::setw(mWidth) << "Re[||Psi||]"
                  << std::setw(mWidth) << "Im[||Psi||]";
      }
      else if  (  name == "REFCOEF"
               )
      {
         stream   << std::setw(mWidth) << "Re[C0]"
                  << std::setw(mWidth) << "Im[C0]"
                  << std::setw(mWidth) << "|C0|";
      }
      else if  (  name == "DENSANALYSIS"
               )
      {
         for(In idof=I_0; idof<ndof; ++idof)
         {
            const auto& label = gOperatorDefs.GetModeLabel(idof);
            stream   << std::setw(mWidth) << "|D_offdiag|_" + label
                     << std::setw(mWidth) << "|D_act_occ|_" + label
                     << std::setw(mWidth) << "rho_max_" + label
                     << std::setw(mWidth) << "rho_min_" + label;
         }
      }
      else if  (  name == "STATEPOPULATIONS"
               )
      {
         if (  gOperatorDefs.ElectronicDofNr() != -I_1
            )
         {
            // Write column titles
            auto nelec = opdef.StateTransferIJMax(opdef.GetLocalModeNr(gOperatorDefs.ElectronicDofNr())) + I_1;
            for(In ielec=I_0; ielec<nelec; ++ielec)
            {
               stream   << std::setw(mWidth) << "P(" + std::to_string(ielec) + ")";
            }

            // Write comment
            stream   << "   (NB: initially occupied state is placed first. The rest are ordered according to energy!)";
         }
         else
         {
            MidasWarning("State populations requested, but no electronic degree of freedom in operator. I will ignore this!");
         }
      }
      else
      {
         MIDASERROR("Unknown property: '" + name + "'.");
      }

      // End line
      stream   << std::endl;
   }

   // Output for expectation values
   for(auto& oper : this->mExptValStreams)
   {
      const auto& name = oper.first;
      auto& stream = oper.second;

      // Time
      stream   << std::setw(mWidth) << "# t";

      // Full expt values
      stream   << std::setw(mWidth) << "Re[<" + name + ">]"
               << std::setw(mWidth) << "Im[<" + name + ">]";

      // One-mode expt values
      for(In idof=I_0; idof<ndof; ++idof)
      {
         const auto& label = gOperatorDefs.GetModeLabel(idof);
         stream   << std::setw(mWidth) << "Re[<" + name + ">]_" + label
                  << std::setw(mWidth) << "Im[<" + name + ">]_" + label;
      }

      // End line
      stream   << std::endl;
   }
}

/**
 * Update integrals and calculate expectation values
 *
 * @param aT
 * @param aY
 * @param aEom
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::CalculateExpectationValuesImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
#ifdef VAR_MPI
   // Only master node saves properties
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   auto ndof = aEom.NDof();

   for(const auto& prop : this->mExptValIntegrals)
   {
      const auto& oper_name = prop.first;
      const auto& integrals = prop.second;

      // For exact propagation, the integrals do not change over time
      if (  !aEom.Exact()
         )
      {
         integrals->Update(aY.Modals(), aEom.ModalOffsets());
      }
      auto exptvals = aEom.ActiveTermExpectationValues(*integrals, aY);
      MidasAssert(exptvals.size() == ndof + 1, "Wrong size of expectation-value vector.");

      auto& stream = mExptValStreams.at(oper_name);

      stream   << std::setw(mWidth) << aT
               << std::setw(mWidth) << std::real(exptvals[0])
               << std::setw(mWidth) << std::imag(exptvals[0]);

      for(In idof=I_0; idof<ndof; ++idof)
      {
         stream   << std::setw(mWidth) << std::real(exptvals[idof+1])
                  << std::setw(mWidth) << std::imag(exptvals[idof+1]);
      }

      stream   << std::endl;
   }
}

/**
 * Calculate properties for an accepted ODE step
 *
 * @param aT
 * @param aY
 * @param aEom
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::CalculateAcceptedStepPropertiesImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
#ifdef VAR_MPI
   // Only master node saves properties
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   for(auto& prop : mPropertyStreams)
   {
      const auto& name = prop.first;
      auto& stream = prop.second;

      // OUtput properties
      if (  name == "ENERGY"
         )
      {
         // Niels: assume H integrals are already updated!
         // This is true when using MidasOdeDriver with AdaptiveRungeKuttaStepper since the derivative is 
         // always calculated at the accepted step before calling WRAP_AcceptedStep.
         // But what about other ODE integrators?
         step_t int_time = aEom.IntegralsTime();
         if (  libmda::numeric::float_neq(int_time, aT)
            && !aEom.Exact()
            )
         {
            MidasWarning("McTdHProperties: H integrals have not been updated at accepted point. Energy cannot be calculated!");
         }
         else
         {
            if (  this->mEnergy.first >= C_0
               )
            {
               this->mPrevEnergy = this->mEnergy;
            }
            auto e = aEom.CalculateEnergy(aT, aY, EOM::no_update_integrals);
            this->mEnergy = std::make_pair(aT, e);
            stream   << std::setw(mWidth) << aT
                     << std::setw(mWidth) << std::real(e)
                     << std::setw(mWidth) << std::imag(e)
                     << std::endl;
         }
      }
      else if  (  name == "NORM"
               )
      {
         step_t int_time = aEom.IntegralsTime();
         if (  libmda::numeric::float_neq(int_time, aT)
            && !aEom.Exact()
            )
         {
            MidasWarning("McTdHProperties: Integrals have not been updated at accepted point. The saved norm is actually calculated at the last derivative evaluation!");
         }
         auto norm = std::sqrt(aEom.WaveFunctionNorm2());
         stream   << std::setw(mWidth) << aT
                  << std::setw(mWidth) << std::real(norm)
                  << std::setw(mWidth) << std::imag(norm)
                  << std::endl;
      }
      else if  (  name == "REFCOEF"
               )
      {
         auto c = aEom.ReferenceCoefficient(aY);
         stream   << std::setw(mWidth) << aT
                  << std::setw(mWidth) << std::real(c)
                  << std::setw(mWidth) << std::imag(c)
                  << std::setw(mWidth) << std::abs(c)
                  << std::endl;
      }
      else if  (  name == "DENSANALYSIS"
               )
      {
         auto data = aEom.DensityMatrixAnalysis(aY);
         auto ndof = aEom.NDof();

         stream   << std::setw(mWidth) << aT;

         for(In idof=I_0; idof<ndof; ++idof)
         {
            stream   << std::setw(mWidth) << data[idof][0]
                     << std::setw(mWidth) << data[idof][1]
                     << std::setw(mWidth) << data[idof][2]
                     << std::setw(mWidth) << data[idof][3];
         }
         stream   << std::endl;
      }
      else if  (  name == "STATEPOPULATIONS"
               )
      {
         if (  gOperatorDefs.ElectronicDofNr() != -I_1
            )
         {
            stream   << std::setw(mWidth) << aT;

            auto data = aEom.ElectronicStatePopulations(aY);
            for(const auto& p : data)
            {
               stream   << std::setw(mWidth) << p;
            }
            stream   << std::endl;
         }
      }
   }
}

/**
 * Calculate interpolated properties
 *
 * @param aT
 * @param aY
 * @param aEom
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::CalculateInterpolatedStepPropertiesImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
#ifdef VAR_MPI
   // Only master node saves properties
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   for(auto& prop : mPropertyStreams)
   {
      const auto& name = prop.first;
      auto& stream = prop.second;

      if (  name == "AUTOCORR"
         )
      {
         auto ac = aEom.CalculateAutoCorr(aT, aY);
         stream   << std::setw(mWidth) << aT
                  << std::setw(mWidth) << std::real(ac)
                  << std::setw(mWidth) << std::imag(ac)
                  << std::setw(mWidth) << std::abs(ac)
                  << std::endl;
      }
      else if  (  name == "HALFTIMEAUTOCORR"
               )
      {
         auto ac = aEom.CalculateHalfTimeAutoCorr(aT, aY);
         stream   << std::setw(mWidth) << C_2*aT
                  << std::setw(mWidth) << std::real(ac)
                  << std::setw(mWidth) << std::imag(ac)
                  << std::setw(mWidth) << std::abs(ac)
                  << std::endl;
      }
   }
}

/**
 * Calculate one-mode densities
 *
 * @param aT
 * @param aY
 * @param aEom
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::CalculateOneModeDensities
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
#ifdef VAR_MPI
   // Only master node calculates spectra
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   this->mOneModeDensities.CalcMctdhOneModeDensities(aEom.GetDensityMatricesVecMat(aY), aY.Modals(), aEom.ModalOffsets());
   this->mOneModeDensities.CalcSsOneModeDensities();
   this->mOneModeDensities.CalcStsOneModeDensities();
   if (this->mpCalcDef->GetWriteOneModeDensities())
   {
      this->mOneModeDensities.WriteOneModeDensitiesForStep(this->GetNumberOfInterpolatedTimePoints());
   }
   if (this->GetNumberOfInterpolatedTimePoints() == this->mpCalcDef->GetUpdateOneModeDensitiesStep())
   {
      this->mOneModeDensities.WriteOneModeDensitiesToAdga();
   }
}

/**
 * Calculate spectrum from auto-correlation functions
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::CalculateSpectrum
   (
   )  const
{
#ifdef VAR_MPI
   // Only master node calculates spectra
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   for(const auto& spec : this->mSpectra)
   {
      switch   (  spec
               )
      {
         case specID::AUTOCORR:
         {
            bool ac_avail = this->mPropertyStreams.find("AUTOCORR") != this->mPropertyStreams.end();
            if (  !ac_avail
               )
            {
               MidasWarning("McTdHProperties: No auto-correlation function available for spectrum calculation!");
               return;
            }
            else
            {
               auto ac_vecs = this->ReadAutoCorrelation(this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_ac.dat");

               if (  ac_vecs.first.empty()
                  || ac_vecs.second.empty()
                  )
               {
                  MidasWarning("No auto-correlation function read. Cannot calculate spectrum!");
               }
               else
               {
                  // Call implementation
                  Spectrum::CalculateAndWriteSpectrum(this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_ac", ac_vecs.first, ac_vecs.second);
               }
            }

            break;
         }
         case specID::HALFTIMEAUTOCORR:
         {
            bool t12ac_avail = this->mPropertyStreams.find("HALFTIMEAUTOCORR") != this->mPropertyStreams.end();
            if (  !t12ac_avail
               )
            {
               MidasWarning("McTdHProperties: No half-time auto-correlation function available for spectrum calculation!");
               return;
            }
            else
            {
               auto ac_vecs = this->ReadAutoCorrelation(this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_t12ac.dat");

               if (  ac_vecs.first.empty()
                  || ac_vecs.second.empty()
                  )
               {
                  MidasWarning("No auto-correlation function read. Cannot calculate spectrum!");
               }
               else
               {
                  // Call implementation
                  Spectrum::CalculateAndWriteSpectrum(this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_t12ac", ac_vecs.first, ac_vecs.second);
               }
            }

            break;
         }
         default:
         {
            MIDASERROR("Unrecognized spectrum!");
         }
      }
   }
}

/**
 * Read auto-correlation functions from file in order to calculate spectra
 *
 * @note Assumes the fist line of the file is a header and that the columns are:  time  Re[S]  Im[S]  |S|
 *
 * @param aFileName
 * @return
 *    Pair of vectors (time and AC)
 **/
template
   <  typename EOM
   >
std::pair<std::vector<typename McTdHProperties<EOM>::param_t>, std::vector<typename McTdHProperties<EOM>::step_t> > McTdHProperties<EOM>::ReadAutoCorrelation
   (  const std::string& aFileName
   )  const
{
   std::pair<std::vector<param_t>, std::vector<step_t> > result;
   result.first.reserve(10);
   result.second.reserve(10);

   if (  InquireFile(aFileName)
      )
   {
      std::ifstream input(aFileName);
      std::string line;

      while (  std::getline(input, line)
            )
      {
         // Skip comments
         if (  line.find('#') != std::string::npos
            )
         {
            continue;
         }

         // Get individual numbers from line
         auto vec = midas::util::VectorFromString<real_t>(line);
         MidasAssert(vec.size() == 4, "Each line must contain 4 numbers.");

         // Get time and AC
         auto time = vec[0];
         param_t ac(vec[1], vec[2]);

         // Reserve space
         if (  result.first.size() == result.first.capacity()
            )
         {
            result.first.reserve(result.first.size()*2);
            result.second.reserve(result.first.size()*2);
         }

         // Emplace to vectors
         result.first.emplace_back(ac);
         result.second.emplace_back(time);
      }

      input.close();
   }
   else
   {
      MidasWarning("Could not find file '" + aFileName + "' containing auto-correlation function.");
   }

   return result;
}

/**
 * Read property for analysis
 *
 * @param aFileName
 * @return
 *    Vector of vectors (matrix form of the file)
 **/
template
   <  typename EOM
   >
std::vector<std::vector<typename McTdHProperties<EOM>::real_t> > McTdHProperties<EOM>::ReadPropertyFile
   (  const std::string& aFileName
   )  const
{
   std::vector<std::vector<real_t> > result;
   result.reserve(10);

   if (  InquireFile(aFileName)
      )
   {
      std::ifstream input(aFileName);
      std::string line;

      while (  std::getline(input, line)
            )
      {
         // Skip comments
         if (  line.find('#') != std::string::npos
            )
         {
            continue;
         }

         // Get individual numbers from line
         result.emplace_back(midas::util::VectorFromString<real_t>(line));

         // Reserve space
         if (  result.size() == result.capacity()
            )
         {
            result.reserve(result.size()*2);
         }
      }

      input.close();
   }
   else
   {
      MidasWarning("Could not find MCTDH property file '" + aFileName + "'.");
   }

   return result;
}

/**
 * Create grid for each mode for use in one-mode densities
 * 
 * @param aEom          The Eom of the calucaltion
 * @param aGridScaling  A scaling factor for the grid size
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::SetGridPoints
   (  const EOM& aEom
   ,  const Nb   aGridScaling
   )
{
   if (this->IsGridPointsSet())
   {
      MIDASERROR("Grid has already been created, this function should ony be called once!");
   }

   // Get Opdef
   const auto& OpDef = gOperatorDefs.GetOperator(this->mpCalcDef->GetOper());

   // Get nr modes
   const Uin nmodes = aEom.GetBasDef()->GetmNoModesInBas();
   this->mGridPoints.reserve(nmodes);

   for (In i_mode = I_0; i_mode < nmodes; ++i_mode)
   {
      // Set up some stuff
      LocalModeNr i_op_mode = OpDef.GetLocalModeNr(i_mode);
      LocalModeNr i_bas_mode = aEom.GetBasDef()->GetLocalModeNr(i_mode);

      // Create grid for mode
      In n_max = aEom.GetBasDef()->GetmHoQnrs()[i_mode]; // use HO turning point for nth eigenstate. Uses the quantum number specified in the HO basis
      Nb omega = OpDef.HoOmega(i_op_mode);
      bool no_omega = libmda::numeric::float_numeq_zero(omega, C_1, 2);
      Nb r_max =  no_omega
               ?  aEom.GetBasDef()->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().second
               :  std::sqrt(C_2*(Nb(n_max)+C_I_2)/(OpDef.HoOmega(i_op_mode)))*aGridScaling; // classical turning point for n in harmonic oscillator scaled by a factor aGridScaling
      Nb r_min =  no_omega
               ?  aEom.GetBasDef()->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().first
               :  -r_max;
      
      // Scale grid bounds further to allow adga to extend its max grid size
      if (this->mpCalcDef->GetUpdateOneModeDensities())
      {
         r_max *= aGridScaling;
         r_min *= aGridScaling;
      }

      auto length = r_max - r_min;
      In n_pts = std::ceil(length * this->mWfGridDensity);
      Nb step = length/Nb(n_pts-I_1);
      GeneralMidasVector<Nb> grid(n_pts);
      for (In i_p=0; i_p<n_pts; ++i_p)
      {
         grid[i_p]=r_min + step*i_p;
      }
      this->mGridPoints.emplace_back(grid);
   }
}

/**
 * Return true if mGridPoints has been created
 **/
template
   <  typename EOM
   >
bool McTdHProperties<EOM>::IsGridPointsSet
   (
   )  const
{
   if (mGridPoints.size() == I_0)
   {
      return false;
   }
   else
   {
      return true;
   }
}

/**
 * Return mGridPoints
 **/
template
   <  typename EOM
   >
const std::vector<GeneralMidasVector<Nb>>& McTdHProperties<EOM>::GetGridPoints
   (
   )  const
{
   if (!this->IsGridPointsSet())
   {
      MIDASERROR("GridPoints has not been created!");
   }
   return mGridPoints;
}

/**
 * Return mGridPoints for specific mode
 * 
 * @param aModeNr
 **/
template
   <  typename EOM
   >
const GeneralMidasVector<Nb>& McTdHProperties<EOM>::GetGridPointsForMode
   (  const In aModeNr
   )  const
{
   if (!this->IsGridPointsSet())
   {
      MIDASERROR("GridPoints has not been created!");
   }
   return mGridPoints[aModeNr];
}

/**
 * Construct mOneModeDensities and set smoothening parameters
 * 
 * @param aName         The Name of the calculation.
 * @param aGridPoints   Grid points for each mode to evaluate the wave function at.
 * @param aVscfModals   VSCF modals used a the primitive basis.
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::ConstructOneModeDensities
   (  const std::string& aName
   ,  const std::vector<GeneralMidasVector<Nb>>& aGridPoints
   ,  const std::vector<std::vector<GeneralMidasVector<Nb>>>& aVscfModals
   )
{
   mOneModeDensities = midas::td::OneModeDensities<param_t>(aName, aGridPoints, aVscfModals);
   
   // Set the space and time smoothenings if they have been specified
   if (this->mpCalcDef->GetSpaceSmoothening())
   {
      mOneModeDensities.SetSpaceSmoothening(this->mpCalcDef->GetSpaceSmoothening());
   }
   if (this->mpCalcDef->GetTimeSmoothening())
   {
      mOneModeDensities.SetTimeSmoothening(this->mpCalcDef->GetTimeSmoothening());
   }
}

/**
 * Accepted step
 *
 * @param aT
 * @param aY
 * @param aEom
 * @return
 *    True if the integrator should stop here.
 **/
template
   <  typename EOM
   >
bool McTdHProperties<EOM>::AcceptedStepImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
   int stop = 0;

#ifdef VAR_MPI
   // Only master node saves data
   if (  midas::mpi::IsMaster()
      )
   {
#endif

   // Save accepted WF
   if (  aEom.CalcDef()->GetSaveAcceptedWfs()
      )
   {
      // If this is the first time we write, save an info file
      std::string info_filename = this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_accepted_wf.info";
      size_t width = 6;
      if (  this->mAcceptedWfCount == 0
         )
      {
         std::set<In> ext_range_modes;
         if (  this->mpCalcDef->GetMethodInfo().Contains("EXTRANGEMODES")
            )
         {
            ext_range_modes = this->mpCalcDef->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");
         }
         std::ofstream output(info_filename);
         output << std::scientific << std::setprecision(16);

         output   << "# Number of modes and electronic states\n"
                  << std::setw(width) << aEom.NModes() << std::setw(width) << aEom.NElectronicStates() << "\n"
                  << "# Number of primitive basis functions for each mode\n"
                  << std::flush;
         for(In imode=I_0; imode<aEom.NModes(); ++imode)
         {
            output   << std::setw(width) << aEom.NBas(imode);
         }
         output   << std::endl;
         output   << "# Number of time-dependent modals for each mode" << std::endl;
         for(In imode=I_0; imode<aEom.NModes(); ++imode)
         {
            output   << std::setw(width) << aEom.NAct(imode);
         }
         output   << std::endl;
         output   << "# Extended modes in MCR (xCS)" << std::endl;
         for(const auto& m : ext_range_modes)
         {
            output   << std::setw(width) << m;
         }
         output   << std::endl;
         output   << "# Time points (WF index and time)\n"
                  << std::setw(width) << this->mAcceptedWfCount << std::setw(width) << aT << "\n"
                  << std::flush;
         output.close();
      }
      // Else, append the time to the info file
      else
      {
         std::ofstream output(info_filename, std::ios_base::app);
         output << std::scientific << std::setprecision(16);
         output   << std::setw(width)  << this->mAcceptedWfCount << std::setw(width) << aT << std::endl;
         output.close();
      }

      // Save WF
      auto name = this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_accepted_wf_" + std::to_string(this->mAcceptedWfCount++);
      aY.WriteToDisc(name);
   }

   // Expectation values
   this->CalculateExpectationValuesImpl(aT, aY, aEom);

   // Other properties
   this->CalculateAcceptedStepPropertiesImpl(aT, aY, aEom);

   if (  aEom.ImagTime()
      && aEom.ImagTimeThreshold() > C_0
      && this->mPrevEnergy.first >= C_0
      )
   {
      auto dt = this->mEnergy.first - this->mPrevEnergy.first;
      auto de = this->mEnergy.second - this->mPrevEnergy.second;

      auto change = de / dt;

      if (  std::abs(change) < aEom.ImagTimeThreshold()
         )
      {
         Mout  << " Energy change per time unit = " << change << ". Stop integration!" << std::endl;
         stop = 1;
      }
   }

#ifdef VAR_MPI
   // End if statement
   }

   // Broadcast bool to other ranks
   midas::mpi::detail::WRAP_Bcast(&stop, 1, midas::mpi::DataTypeTrait<int>::Get(), 0, MPI_COMM_WORLD);
#endif

   bool stop_integration = stop;

   return stop_integration;
}

/**
 * Interpolated step
 *
 * @param aT
 * @param aY
 * @param aEom
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::InterpolatedStepImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  const EOM& aEom
   )
{
#ifdef VAR_MPI
   // Only master node saves properties
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   IncInterpolatedTimePoints();

   // Save interpolated WF
   if (  aEom.CalcDef()->GetSaveInterpolatedWfs()
      )
   {
      // If this is the first time we write, save an info file
      std::string info_filename = this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_interpolated_wf.info";
      size_t width = 6;
      if (  this->mInterpolatedWfCount == 0
         )
      {
         std::set<In> ext_range_modes;
         if (  this->mpCalcDef->GetMethodInfo().Contains("EXTRANGEMODES")
            )
         {
            ext_range_modes = this->mpCalcDef->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");
         }
         std::ofstream output(info_filename);
         output << std::scientific << std::setprecision(16);

         output   << "# Number of modes and electronic states\n"
                  << std::setw(width) << aEom.NModes() << std::setw(width) << aEom.NElectronicStates() << "\n"
                  << "# Number of primitive basis functions for each mode\n"
                  << std::flush;
         for(In imode=I_0; imode<aEom.NModes(); ++imode)
         {
            output   << std::setw(width) << aEom.NBas(imode);
         }
         output   << std::endl;
         output   << "# Number of time-dependent modals for each mode" << std::endl;
         for(In imode=I_0; imode<aEom.NModes(); ++imode)
         {
            output   << std::setw(width) << aEom.NAct(imode);
         }
         output   << std::endl;
         output   << "# Extended modes in MCR (xCS)" << std::endl;
         for(const auto& m : ext_range_modes)
         {
            output   << std::setw(width) << m;
         }
         output   << std::endl;
         const auto& odeinfo = this->mpCalcDef->GetIntegrationInfo().template At<midas::ode::OdeInfo>("ODEINFO");
         const auto& timeinterval = odeinfo.template get<std::pair<Nb, Nb>>("TIMEINTERVAL");
         const auto& npoints = odeinfo.template get<In>("OUTPUTPOINTS");
         auto dt = (timeinterval.second - timeinterval.first) / (npoints - I_1);
       
         output   << "# Delta T (time between output points)\n"
                  << std::setw(width) << dt << "\n"
                  << std::flush;
         output.close();
      }
      auto name = this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_interpolated_wf_" + std::to_string(this->mInterpolatedWfCount++);
      aY.WriteToDisc(name);
   }

   // Other properties
   this->CalculateInterpolatedStepPropertiesImpl(aT, aY, aEom);

   //
   if (this->mpCalcDef->GetUpdateOneModeDensities())
   {
      this->CalculateOneModeDensities(aT, aY, aEom);
   }
}

/**
 * Transform property integrals
 *
 * @param aEom
 * @param aWf
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::TransformPropertyIntegrals
   (  const EOM& aEom
   ,  const vec_t& aWf
   )
{
#ifdef VAR_MPI
   // Only master node saves integrals
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   const DataCont& modals = aEom.SpectralBasisModals();
   const std::vector<In>& offsets = aEom.SpectralModalOffsets();

   for(auto& prop : this->mExptValIntegrals)
   {
      prop.second->TransformModalBasis(modals, offsets);

      // If exact propagation, we just update integrals once
      if (  aEom.Exact()
         )
      {
         prop.second->Update(aWf.Modals(), aEom.ModalOffsets());
      }
   }
}

/**
 * Print a summary and statistics of the calculated properties
 *
 * @note This reads in the property files, calculates min, max, mean, variance, etc.
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::Summary
   (
   )  const
{
   Mout  << std::endl;
   std::string header = "  MCTDH property statistics (" + this->mpCalcDef->GetName() + ")";
   Out72Char(Mout,'+','-','+');
   Out72Char(Mout,'|',' ','|');
   OneArgOut72(Mout, header, '|');
   Out72Char(Mout,'|',' ','|');
   Out72Char(Mout,'+','-','+');
   Mout  << std::endl;

   // Print statistics on properties
   for(const auto& p : this->mPropertyStreams)
   {
      const auto& propname = p.first;
      auto filename = this->PropertyFileName(propname);
      auto data = this->ReadPropertyFile(filename);

      if (  data.empty()
         )
      {
         MidasWarning("Data file '" + filename + "' contains no lines!");
         continue;
      }

      // Only real part interesting
      if (  propname == "NORM"
         )
      {
         // Real part
         this->OutputPropertyStatistics
            (  propname + " (Re)"
            ,  data
            ,  1
            );
      }
      // Real and imaginary parts
      else if  (  propname == "ENERGY"
               )
      {
         // Real part
         this->OutputPropertyStatistics
            (  propname + " (Re)"
            ,  data
            ,  1
            );

         // Imag part
         this->OutputPropertyStatistics
            (  propname + " (Im)"
            ,  data
            ,  2
            );
      }
      // Real, imaginary, and abs value
      else if  (  propname == "AUTOCORR"
               || propname == "HALFTIMEAUTOCORR"
               || propname == "REFCOEF"
               )
      {
         // Real part
         this->OutputPropertyStatistics
            (  propname + " (Re)"
            ,  data
            ,  1
            );

         // Imag part
         this->OutputPropertyStatistics
            (  propname + " (Im)"
            ,  data
            ,  2
            );

         // Imag part
         this->OutputPropertyStatistics
            (  propname + " (abs)"
            ,  data
            ,  3
            );
      }
      // Density analysis
      else if  (  propname == "DENSANALYSIS"
               )
      {
         auto nmodes = In(data.front().size()-1)/I_4;

         // Loop over modes
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            const auto& modename = gOperatorDefs.GetModeLabel(imode);

            // Max occupation number
            this->OutputPropertyStatistics
               (  "Max. occupation number (" + modename + ")"
               ,  data
               ,  imode*I_4 + I_3
               );

            // Min occupation number
            this->OutputPropertyStatistics
               (  "Min. occupation number (" + modename + ")"
               ,  data
               ,  imode*I_4 + I_4
               );
         }
      }
      // State populations
      else if  (  propname == "STATEPOPULATIONS"
               )
      {
         auto nstates = In(data.front().size()) - I_1;

         for(In istate=I_0; istate<nstates; ++istate)
         {
            // Max occupation number
            this->OutputPropertyStatistics
               (  "Electronic state population " + std::to_string(istate)
               ,  data
               ,  istate+I_1
               );
         }

         // Max occupation number
         this->OutputPropertyStatistics
            (  "Sum of electronic state populations"
            ,  data
            ,  [](const std::vector<real_t>& v)
               {
                  real_t result = 0;

                  for(In i=I_1; i<v.size(); ++i)
                  {
                     result += v[i];
                  }

                  return result;
               }
            );
      }
      // Not found!
      else
      {
         MidasWarning("MCTDH property: '" + propname + "' not recognized!");
      }
   }

   // Print statistics on expectation values
   for(const auto& e : this->mExptValStreams)
   {
      const auto& opname = e.first;
      auto filename = this->mAnalysisDir + "/" + this->mpCalcDef->GetName() + "_mean_" + opname + ".dat";
      auto data = this->ReadPropertyFile(filename);

      if (  data.empty()
         )
      {
         MidasWarning("Data file '" + filename + "' contains no lines!");
         continue;
      }

      // Real part of full exptval
      this->OutputPropertyStatistics
         (  "Re[<" + opname + ">]"
         ,  data
         ,  I_1
         );
      // Imag part of full exptval
      this->OutputPropertyStatistics
         (  "Im[<" + opname + ">]"
         ,  data
         ,  I_2
         );

      // Niels: For now, we do not print statistics of partial exptvals.
//      auto nmodes = In(data.front().size() - 3) / I_2;
   }
}

/**
 * Output properties
 *
 * @param aName         Name of property
 * @param aData         Data to analyse
 * @param aCol          Column to analyse
 **/
template
   <  typename EOM
   >
void McTdHProperties<EOM>::OutputPropertyStatistics
   (  const std::string& aName
   ,  const std::vector<std::vector<real_t>>& aData
   ,  In aCol
   )  const
{
   return this->OutputPropertyStatistics
            (  aName
            ,  aData
            ,  [aCol](const std::vector<real_t>& v) { return v[aCol]; }
            );
}

/**
 * Output properties
 *
 * @param aName         Name of property
 * @param aData         Data to analyse
 * @param aF            Function/functor to get data from vector
 **/
template
   <  typename EOM
   >
template
   <  typename F
   >
void McTdHProperties<EOM>::OutputPropertyStatistics
   (  const std::string& aName
   ,  const std::vector<std::vector<real_t>>& aData
   ,  F&& aF
   )  const
{
   // Write a nice header with the name
   Mout  << " ########## " << aName << " ##########" << std::endl;

   // Calculate/get
   //    - First/last element + change
   //    - Min/max/span (with times)
   //    - Mean
   //    - Std. deviation/variance
   auto n = aData.size();
   real_t mean = 0;
   real_t min, max, t_min, t_max;
   real_t val;
   for(size_t i=0; i<aData.size(); ++i)
   {
      // Get value for time
      const auto& v = aData[i];
      const auto& t = v.front();
      val = aF(v);

      if (  i == 0
         )
      {
         min = val;
         max = val;
         t_min = t;
         t_max = t;
      }
      else
      {
         if (  val > max
            )
         {
            max = val;
            t_max = t;
         }
         if (  val < min
            )
         {
            min = val;
            t_min = t;
         }
      }

      mean += val;
   }
   mean /= n;
   real_t span = max - min;
   real_t begin = aF(aData.front());
   real_t end = aF(aData.back());
   real_t change = end - begin;

   // Get sum of squared deviations
   real_t ssd = 0;
   for(const auto& v : aData)
   {
      ssd += midas::util::AbsVal2(aF(v) - mean);
   }

   real_t std_dev = std::sqrt(ssd/n);

   // Print as indented
   size_t width = 30;
   Mout  << std::setw(width) << "   - At t_beg:"   << std::setw(width) << begin << "\n"
         << std::setw(width) << "   - At t_end:"   << std::setw(width) << end << "\n"
         << std::setw(width) << "   - Change:"     << std::setw(width) << change << "\n"
         << std::setw(width) << "   - Min:"        << std::setw(width) << min << " (at t = " << t_min << ")\n"
         << std::setw(width) << "   - Max:"        << std::setw(width) << max << " (at t = " << t_max << ")\n"
         << std::setw(width) << "   - Span:"       << std::setw(width) << span << "\n"
         << std::setw(width) << "   - Mean:"       << std::setw(width) << mean << "\n"
         << std::setw(width) << "   - Std. dev."   << std::setw(width) << std_dev << "\n"
         << std::endl;
}

} /* namespace midas::mctdh::detail */

#endif /* MCTDHPROPERTIES_IMPL_H_INCLUDED */

