/*
************************************************************************
*
* @file                 McTdHProperties_Decl.h
*
* Created:              09-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class that holds calculated properties and property integrals.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHPROPERTIES_DECL_H_INCLUDED
#define MCTDHPROPERTIES_DECL_H_INCLUDED

#include "util/type_traits/Complex.h"

#include "mpi/FileStream.h"

#include "td/SpectrumCalculator.h"
#include "td/OneModeDensities.h"

// Forward decl
namespace midas::input
{
   class McTdHCalcDef;
}

namespace midas::mctdh::detail
{

/**
 *
 **/
template
   <  typename EOM
   >
class McTdHProperties
   :  protected midas::td::SpectrumCalculator<typename EOM::param_t>
{
   public:
      //! Aliases
      using Eom = EOM;
      using param_t = typename Eom::param_t;
      using vec_t = typename Eom::vec_t;
      using mat_t = typename Eom::mat_t;
      using step_t = typename midas::type_traits::RealTypeT<param_t>;
      using integrals_t = typename Eom::integrals_t;
      using real_t = step_t;
      using Spectrum = midas::td::SpectrumCalculator<param_t>;
      using ofstream_t = midas::mpi::OFileStream;

      //! Spectra
      enum class specID : int
      {  AUTOCORR = 0
      ,  HALFTIMEAUTOCORR
      };

   private:
      //! Pointer to McTdHCalcDef
      const input::McTdHCalcDef* const mpCalcDef = nullptr;

      //! File streams for properties
      std::map<std::string, ofstream_t> mPropertyStreams;

      //! Spectra to calculate
      std::set<specID> mSpectra;

      //! Integrals for calculating expectation values. Key is operator name.
      std::map<std::string, std::unique_ptr<integrals_t> > mExptValIntegrals;

      //! File streams for expectation values
      std::map<std::string, ofstream_t> mExptValStreams;

      //! Analysis directory
      std::string mAnalysisDir = "";

      //! Number of accepted-step wave functions written to disc
      size_t mAcceptedWfCount = 0;

      //! Number of interpolated wave functions written to disc
      size_t mInterpolatedWfCount = 0;

      //! Width of output columns
      size_t mWidth = 24;

      //! Energy (and time)
      std::pair<step_t, param_t> mEnergy = {-C_1, C_0};

      //! Prev. energy (and time)
      std::pair<step_t, param_t> mPrevEnergy = {-C_1, C_0};

      //! One-mode density member
      midas::td::OneModeDensities<param_t> mOneModeDensities;

      //! Grid density for plotting wave function
      In mWfGridDensity = I_10;

      //! Grid points for evaluating one-mode densities
      std::vector<GeneralMidasVector<Nb>> mGridPoints;

      //! Increment number of interpolated points
      void IncInterpolatedTimePoints() {++mNInterPoint;}
      
      //! Get the number of interpolated points
      In GetNumberOfInterpolatedTimePoints() {return mNInterPoint;}
      
      //! Number of interpolated points
      In mNInterPoint = 0;
      
      //! Update integrals and calculate expectation values
      void CalculateExpectationValuesImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Calculate properties for an accepted ODE step
      void CalculateAcceptedStepPropertiesImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Calculate interpolated properties
      void CalculateInterpolatedStepPropertiesImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Calculate One-mode densities
      void CalculateOneModeDensities
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Write headers for all output files
      void WriteHeaders
         (
         );

      //! Read auto-correlation functions from file in order to calculate spectra
      std::pair<std::vector<param_t>, std::vector<step_t> > ReadAutoCorrelation
         (  const std::string&
         )  const;

      //! Read general property file
      std::vector<std::vector<real_t> > ReadPropertyFile
         (  const std::string&
         )  const;

      //! Property filename
      std::string PropertyFileName
         (  const std::string&
         )  const;

      //! Print statistics of property
      void OutputPropertyStatistics
         (  const std::string&
         ,  const std::vector<std::vector<real_t>>&
         ,  In
         )  const;

      //! Print statistics of property
      template
         <  typename F
         >
      void OutputPropertyStatistics
         (  const std::string&
         ,  const std::vector<std::vector<real_t>>&
         ,  F&&
         )  const;

   protected:
      //! Create grid for each mode
      void SetGridPoints
         (  const EOM& aEom
         ,  const Nb   aGridScaling = C_2
         );

      bool IsGridPointsSet
         (
         )  const;

      //! Return mGridPoints
      const std::vector<GeneralMidasVector<Nb>>& GetGridPoints
         (
         )  const;
      
      //! Return mGridPoints for specific mode
      const GeneralMidasVector<Nb>& GetGridPointsForMode
         (  const In aModeNr
         )  const;
      
      //! Protected function to construct mOneModeDensities
      void ConstructOneModeDensities
         (  const std::string& aName
         ,  const std::vector<GeneralMidasVector<Nb>>& aGridPoints
         ,  const std::vector<std::vector<GeneralMidasVector<Nb>>>& aVscfModals
         );

      //! Accepted step
      bool AcceptedStepImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Interpolated step
      void InterpolatedStepImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  const EOM& aEom
         );

      //! Transform property integrals
      void TransformPropertyIntegrals
         (  const EOM& aEom
         ,  const vec_t& aWf
         );

      //! Calculate spectrum from auto-correlation functions
      void CalculateSpectrum
         (
         )  const;

      //! Summary of properties
      void Summary
         (
         )  const;


   public:
      //! c-tor from calcdef
      McTdHProperties
         (  const input::McTdHCalcDef* const apCalcDef
         );
};

} /* namespace midas::mctdh::detail */

#endif /* MCTDHPROPERTIES_DECL_H_INCLUDED */
