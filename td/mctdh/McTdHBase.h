/*
************************************************************************
*
* @file                 McTdHBase.h
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    McTdH wrapper class
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHBASE_H_INCLUDED
#define MCTDHBASE_H_INCLUDED

// Include declarations
#include "McTdHBase_Decl.h"

// Include implementation
#include "McTdHBase_Impl.h"

#endif /* MCTDHBASE_H_INCLUDED */
