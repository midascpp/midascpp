/*
************************************************************************
*
* @file                 McTdHBase_Impl.h
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    McTdHBase implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHBASE_IMPL_H_INCLUDED
#define MCTDHBASE_IMPL_H_INCLUDED

#include "McTdHBase_Decl.h"
#include "input/McTdHCalcDef.h"
#include "util/SanityCheck.h"
#include "ode/ode_exceptions.h"
#include "ode/OdeIntegrator.h"
#include "util/Timer.h"
#include "input/FindCalculation.h"

#include "mpi/Impi.h"

namespace midas::mctdh
{

/**
 * Constructor from CalcDef, oper_t, and BasDef
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
template
   <  typename EOM
   >
McTdHBase<EOM>::McTdHBase
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  detail::McTdHProperties<EOM>
         (  apCalcDef
         )
   ,  mName
         (  apCalcDef->GetName()
         )
   ,  mEom
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
{
   // Set up integration scheme from apCalcDef
   static const std::map<std::string, integratorType> integrator_map =
   {  {"VMF", integratorType::VMF}
   ,  {"CMF", integratorType::CMF}
   ,  {"LUBICH", integratorType::LUBICH}
   };

   const auto& scheme = apCalcDef->GetIntegrationInfo().template At<std::string>("SCHEME");
   mIntegrationScheme = integrator_map.at(scheme);
   
   // Initialize mOneModeDensities if td-ADGA is to be used
   if (  apCalcDef->GetInitialWavePacket() == input::McTdHCalcDef::initType::VSCF
      && apCalcDef->GetUpdateOneModeDensities()
      )
   {
      this->InitOneModeDensities(apCalcDef);
   }
}

/**
 * Derivative
 *
 * @param aT
 * @param aY
 *
 * @return
 *    Derivative vector
 **/
template
   <  typename EOM
   >
typename McTdHBase<EOM>::vec_t McTdHBase<EOM>::Derivative
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Call implementation in EOM
   return this->mEom.Derivative(aT, aY);
}

/**
 * Process new vector.
 *
 * @param arY
 * @param aYOld
 * @param arT
 * @param aTOld
 * @param arDyDt
 * @param aDyDtOld
 * @param arStep
 * @param arNDerivFail
 *
 * @return
 *    Step accepted?
 **/
template
   <  typename EOM
   >
bool McTdHBase<EOM>::ProcessNewVector
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )  const
{
   return this->mEom.ProcessNewVector(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
}

/**
 * Save data for accepted step
 *
 * @param aTime
 * @param aY
 * @return
 *    True if the ODE integrator should stop here.
 **/
template
   <  typename EOM
   >
bool McTdHBase<EOM>::AcceptedStep
   (  step_t aTime
   ,  const vec_t& aY
   )
{
   return this->AcceptedStepImpl(aTime, aY, this->mEom);
}

/**
 * Save data for interpolated point
 *
 * @param aTime
 * @param aY
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::InterpolatedPoint
   (  step_t aTime
   ,  const vec_t& aY
   )
{
   this->InterpolatedStepImpl(aTime, aY, this->mEom);
}

/**
 * Return zero vector of correct shape.
 *
 * @return
 *    Zero vector
 **/
template
   <  typename EOM
   >
typename McTdHBase<EOM>::vec_t McTdHBase<EOM>::ShapedZeroVector
   (
   )  const
{
   return this->mEom.ShapedZeroVector();
}

/**
 * Number of modes (from EOM)
 *
 * @return
 *    NModes
 **/
template
   <  typename EOM
   >
Uin McTdHBase<EOM>::NModes
   (
   )  const
{
   return this->mEom.NModes();
}

/**
 * Finalize calculation
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::Finalize
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )  const
{
#ifdef VAR_MPI
   // Only master node does output
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   // Output summary of properties
   Properties::Summary();

   // Calculate spectra
   Properties::CalculateSpectrum();

   // Output wave functions
   this->WaveFunctionOutput(aTs, aYs);
}

/**
 * Wave-function related output
 *
 * @param aTs     Saved time points in OdeIntegrator
 * @param aYs     Saved wave functions in OdeIntegrator
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::WaveFunctionOutput
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )  const
{
   const auto& wf_out_modes = this->mEom.CalcDef()->GetWfOutModes();
   const auto& two_mode_dens = this->mEom.CalcDef()->GetTwoModeDensityPairs();

   if (  !( wf_out_modes.empty()
         && two_mode_dens.empty()
         )
      )
   {
      MidasWarning("MCTDH wave-function output not implemented!");
   }
}


/**
 * Initialize mOneModeDensities member from calcname and vscf modals evaluated at grid.
 * 
 * @param apCalcDef  MCTDH CalcDef
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::InitOneModeDensities
   (  const input::McTdHCalcDef* const apCalcDef
   )
{
   // check which WP is initialized from
   if (apCalcDef->GetInitialWavePacket() != input::McTdHCalcDef::initType::VSCF)
   {
      MIDASERROR("OneModedensities is only implemented when MCTHD is initialized from a vscf calculation!");
   }

   // Get number of modes
   const Uin n_modes = this->mEom.GetBasDef()->GetmNoModesInBas();

   // Set up modal space vectors
   std::vector<Uin> prim_modals; // vscf modals
   for (In i_mode = I_0; i_mode < n_modes; ++i_mode)
   {
      prim_modals.emplace_back(this->mEom.GetBasDef()->Nbas(i_mode));
   }

   // Find VscfCalcDef to initialize from.
   const auto& init_name = apCalcDef->GetInitCalcName();
   const Uin i_vscf = midas::input::FindCalculationIndex(gVscfCalcDef, init_name, "VSCF");
   auto& vscf_calcdef = gVscfCalcDef[i_vscf];
   std::string vscf_modals_name = vscf_calcdef.GetName() + "_Modals";

   // Figure out the size/number of elements to retrieve from vscf modals.
   std::vector<Uin> v_offsets;
   v_offsets.reserve(n_modes);
   Uin offset = 0;
   for(LocalModeNr m = 0; m < n_modes; ++m)
   {
      v_offsets.emplace_back(offset);
      offset += prim_modals[m] * prim_modals[m];
   }
   if (!midas::filesystem::IsFile(vscf_modals_name+"_0"))
   {
      MIDASERROR("Did not find file '"+vscf_modals_name+"_0"+"'.");
   }

   // Get the vscf modals
   GeneralDataCont<Nb> dc_modals;
   dc_modals.GetFromExistingOnDisc(offset, vscf_modals_name);
   dc_modals.SaveUponDecon(true);

   // Vector holding the vscf modals at each grid point
   std::vector<std::vector<GeneralMidasVector<Nb>>> vscf_modals;

   //
   if (!this->IsGridPointsSet())
   {
      this->SetGridPoints(this->mEom);
   }
   const auto& grid_points = this->GetGridPoints();

   // Loop over each mode
   for (In i_mode = I_0; i_mode < n_modes; ++i_mode)
   {
      // Get the VSCF modal coefficients for each modal in a matrix form
      Uin nbas = prim_modals[i_mode];
      GeneralMidasMatrix<Nb> vscf_modal_coefs(nbas, In(nbas));
      dc_modals.DataIo(IO_GET, vscf_modal_coefs, nbas*nbas, nbas, nbas, v_offsets[i_mode]);

      // Grid points to evaluate each modal at
      const auto& grid_points_for_mode = grid_points[i_mode];
      // vector holding vscf modals evaluated at each grid point
      std::vector<GeneralMidasVector<Nb>> vscf_modals_mode_i;

      // Number of vscf modals and basis functions to loop over
      Uin n_vscf_modals = nbas; // number of vscf modals match basis size
      // Loop over grid points
      for (In q_idx = I_0; q_idx < grid_points_for_mode.Size(); ++q_idx)
      {
         const auto& q = grid_points_for_mode[q_idx];
         GeneralMidasVector<Nb> vscf_modals_grid_q(n_vscf_modals);

         // Loop over each modal
         for (In i_vscf_modal = I_0; i_vscf_modal < n_vscf_modals; ++i_vscf_modal)
         {
            Nb vscf_modal = 0;
            for (In ibas = I_0; ibas < nbas; ++ibas)
            {
               // Evaluate the vscf modals at each grid point
               vscf_modal += vscf_modal_coefs[i_vscf_modal][ibas] * this->mEom.GetBasDef()->EvalOneModeBasisFunc(i_mode, ibas, q);
            }
            vscf_modals_grid_q[i_vscf_modal] = vscf_modal;
         }
         vscf_modals_mode_i.emplace_back(vscf_modals_grid_q);
      }
      vscf_modals.emplace_back(vscf_modals_mode_i);
   }

   // Initialize mOneModeDensities
   this->ConstructOneModeDensities(mName, grid_points, vscf_modals);
}


/**
 * Propagate EOMs using variable mean field, i.e. using a standard ODE integrator 
 * on the full set of parameters.
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateVmf
   (  vec_t& arY
   )
{
   // Init timer
   Timer timer;

   // Make integrator and evolve
   const auto& odeinfo = this->mEom.IntegrationInfo().template At<midas::ode::OdeInfo>("ODEINFO");
   MidasAssert(!odeinfo.empty(), "Empty OdeInfo in McTdHBase::PropagateVmf.");
   OdeIntegrator<McTdHBase<EOM>> integrator(this, odeinfo);
   integrator.Integrate(arY);

   // Write summary of integration
   integrator.Summary();

   // Output timing
   timer.CpuOut(Mout, "CPU time spent on MCTDH VMF integration:  ");
   timer.WallOut(Mout, "Wall time spent on MCTDH VMF integration: ");

   // Get vectors of time and y
   const auto& t = integrator.GetDatabaseT();
   const auto& y = integrator.GetDatabaseY();

   // Output properties
   this->Finalize(t, y);
}


/**
 * Propagate EOMs using 2nd-order constant mean field (CMF) scheme described in:
 *
 * M.H. Beck and H.-D. Meyer
 *
 * An efficient and robust integration scheme for the equations of motion of the 
 * multiconfiguration time-dependent Hartree (MCTDH) method
 *
 * Z. Phys. D 42, 113-129 (1997)
 *
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateCmf
   (  vec_t& arY
   )
{
   MIDASERROR("CMF not implemented!");
}

/**
 * Propagate EOMs using Lubich integrator described in:
 *
 * C. Lubich
 *
 * Time Integration in the Multiconfiguration Time-Dependent Hartree Method 
 * of Molecular Quantum Dynamics
 *
 * Applied Mathematics Research eXpress, Vol. 2015, No. 2, pp. 311-328
 *
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateLubich
   (  vec_t& arY
   )
{
   MIDASERROR("Lubich integrator not implemented!");
}


/**
 * Wrapper for propagating EOMs.
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::Propagate
   (
   )
{
   // Prepare integration
   auto wf = this->ShapedZeroVector();
   this->mEom.PrepareIntegration(wf); // gets modals for transforming basis

   // Set energy shift for spectrum if changed
   this->SetSpectrumEnergyShift(this->mEom.SpectrumEnergyShift());

   // Transform property integrals to same basis as H integrals
   this->TransformPropertyIntegrals(this->mEom, wf);

   // Call implementation which does integration and call Finalize with suitable arguments
   switch   (  this->mIntegrationScheme
            )
   {
      case integratorType::VMF:
      {
         this->PropagateVmf(wf);
         break;
      }
      case integratorType::CMF:
      {
         this->PropagateCmf(wf);
         break;
      }
      case integratorType::LUBICH:
      {
         this->PropagateLubich(wf);
         break;
      }
      default:
      {
         MIDASERROR("McTdHBase: Unknown integration scheme!");
      }
   }

   // Write final wave function to disc
   wf.WriteToDisc(this->Name());
}

/**
 * @return
 *    Name of calculation
 **/
template
   <  typename EOM
   >
const std::string& McTdHBase<EOM>::Name
   (
   )  const
{
   return this->mName;
}

} /* namespace midas::mctdh */

#endif /* MCTDHBASE_IMPL_H_INCLUDED */
