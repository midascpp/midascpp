/*
************************************************************************
*
* @file                 McTdHIntegrals.cc
*
* Created:              16-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Time-dependent integrals used for MCTDH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "McTdHIntegrals.h"
#include "input/McTdHCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"

#include "util/CallStatisticsHandler.h"

namespace midas::mctdh
{

namespace detail
{
/**
 * Constructor from calcdefs
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
McTdHIntegralsBase::McTdHIntegralsBase
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  midas::td::PrimitiveIntegrals<std::complex<Nb>>
         (  &aOper.Oper()
         ,  apBasDef
         )
   ,  mOper
         (  aOper
         )
   ,  mpBasDef
         (  apBasDef
         )
   ,  mAssumeFixedElectronicDofModals
         (  !apCalcDef->GetPropagateElectronicDofModals()
         )
{
   // Set size of vectors
   mActiveIntegrals.resize(this->NModes());
   for(LocalModeNr imode=I_0; imode<this->NModes(); ++imode)
   {
      auto n_opers = aOper.Oper().NrOneModeOpers(imode);
      mActiveIntegrals[imode].resize(n_opers);
   }
}

/**
 * Get active-active integral block
 *
 * @param aMode
 * @param aOper
 *
 * @return
 *    Active-active block of time-dependent integrals
 **/
const McTdHIntegralsBase::mat_t& McTdHIntegralsBase::GetActiveIntegrals
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   )  const
{
   return mActiveIntegrals[aMode][aOper];
}


/**
 * Get number of active modals for mode
 *
 * @param aMode
 * @return
 *    NActive
 **/
In McTdHIntegralsBase::NActive
   (  LocalModeNr aMode
   )  const
{
   const auto& actint_m = mActiveIntegrals[aMode];
   MidasAssert(!actint_m.empty(), "No operators for mode " + std::to_string(aMode) + " in McTdHIntegralsBase. Cannot determine NActive!");

   In nact = actint_m.front().Nrows();

   MidasAssert(nact > I_0, "McTdHIntegralsBase thinks NActive = 0 for mode" + std::to_string(aMode) + ". This must be wrong!");

   return nact;
}

/**
 * Get overlap matrix for active modals.
 *
 * @param aOperModeNr      LocalModeNr of mode in the operator that the integrals are working with.
 * @return
 *    S matrix
 **/
const typename McTdHIntegralsBase::mat_t& McTdHIntegralsBase::GetActiveOverlaps
   (  LocalModeNr aOperModeNr
   )  const
{
   auto unit_op = this->mOper.Oper().GetUnitOpForMode(aOperModeNr);

   return mActiveIntegrals[aOperModeNr][unit_op];
}

/**
 * @return
 *    Pointer to Oper
 **/
const typename McTdHIntegralsBase::oper_t& McTdHIntegralsBase::Oper
   (
   )  const
{
   return mOper;
}

/**
 * @return
 *    Pointer to BasDef
 **/
const BasDef* const McTdHIntegralsBase::GetBasDef
   (
   )  const
{
   if (  mpBasDef
      )
   {
      return mpBasDef;
   }
   else
   {
      MIDASERROR("McTdHIIntegralsBase: Uninitialized BasDef!");
      return nullptr;
   }
}


} /* namespace detail */

/**
 * c-tor from calcdefs
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
LinearMcTdHIntegrals::LinearMcTdHIntegrals
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  detail::McTdHIntegralsBase
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
{
   // Set size of vectors
   mHalfTransformedIntegrals.resize(this->NModes());
   for(LocalModeNr imode=I_0; imode<this->NModes(); ++imode)
   {
      auto n_opers = aOper.Oper().NrOneModeOpers(imode);
      mHalfTransformedIntegrals[imode].resize(n_opers);
   }
}

/**
 * Update LinearMcTdHIntegrals
 *
 * @param aModals    Modal vector
 * @param aOffsets   Offsets indexed by [mode][active-modal]
 **/
void LinearMcTdHIntegrals::Update
   (  const modal_t& aModals
   ,  const offset_t& aOffsets
   )
{
   LOGCALL("lin int update");
   In nmodes = this->NModes();

   /***********************************************/
   /* NB: Make MPI parallel here                  */
   /***********************************************/
   for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
   {
      In nbas = this->NBas(imode);
      In n_op = this->NOper(imode);

      const auto& act_offsets = aOffsets[imode];
      In nact = act_offsets.size();

      bool initialized  =  !this->mHalfTransformedIntegrals[imode].empty()
                        && this->mHalfTransformedIntegrals[imode].front().Nrows() > I_0;

      // If we assume fixed modals for electronic DOF and the integrals have been updated once, skip further updates!
      if (  this->mAssumeFixedElectronicDofModals
         && this->mOper.Oper().GetGlobalModeNr(imode) == gOperatorDefs.ElectronicDofNr()
         && initialized
         )
      {
         continue;
      }

      // Loop over operators
      #pragma omp parallel
      {
         mat_t h(nbas, nbas);

         #pragma omp for
         for(In i_op=I_0; i_op<n_op; ++i_op)
         {
            h = this->GetTimeIndependentIntegrals(imode, i_op);

            // Set size
            this->mHalfTransformedIntegrals[imode][i_op].SetNewSize(nbas, nact);

            // Calculate half-transformed integrals
            for(In r=I_0; r<nbas; ++r)
            {
               for(In v=I_0; v<nact; ++v)
               {
                  param_t val(0.);
                  for(In s=I_0; s<nbas; ++s)
                  {
                     val += h[r][s] * aModals[act_offsets[v] + s];
                  }
                  this->mHalfTransformedIntegrals[imode][i_op][r][v] = val;
               }
            }

            // Set size
            this->mActiveIntegrals[imode][i_op].SetNewSize(nact, nact);

            // Transform again
            for(In v=I_0; v<nact; ++v)
            {
               for(In w=I_0; w<nact; ++w)
               {
                  param_t val(0.);
                  for(In r=I_0; r<nbas; ++r)
                  {
                     val += midas::math::Conj(aModals[act_offsets[v] + r]) * this->mHalfTransformedIntegrals[imode][i_op][r][w];
                  }
                  this->mActiveIntegrals[imode][i_op][v][w] = val;
               }
            }
         }
      }

      // Debug
      if (  gDebug
         )
      {
         Mout  << " S matrix for mode " << imode << " after integral update:\n" << this->GetActiveOverlaps(imode) << std::endl;
      }
   }
}

/**
 * Get half-transformed integral block
 *
 * @param aMode
 * @param aOper
 *
 * @return
 *    Half-transformed block of time-dependent integrals
 **/
const LinearMcTdHIntegrals::mat_t& LinearMcTdHIntegrals::GetHalfTransformedIntegrals
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   )  const
{
   return mHalfTransformedIntegrals[aMode][aOper];
}

/**
 * c-tor
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
LinearRasTdHIntegrals::LinearRasTdHIntegrals
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  LinearMcTdHIntegrals
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
{
   // Set mVecNAct
   // NB: This should be exactly equivalent to PrimitiveBasisTransformer::mNModalsVec
   const auto& actspace = apCalcDef->GetActiveSpace();
   bool non_adiabatic = ( gOperatorDefs.ElectronicDofNr() != -I_1 && aOper.Oper().ActiveForMode(gOperatorDefs.ElectronicDofNr() ) );
   auto ndof = apBasDef->GetmNoModesInBas();
   mVecNAct.resize(ndof);
   if (  non_adiabatic
      )
   {
      auto e_nr = gOperatorDefs.ElectronicDofNr();
      auto n_elec = aOper.Oper().StateTransferIJMax(aOper.Oper().GetLocalModeNr(e_nr)) + I_1;
      this->mVecNAct[e_nr] = n_elec;
      In count = 0;
      for(In idof=I_0; idof<ndof; ++idof)
      {
         if (  idof != e_nr
            )
         {
            this->mVecNAct[idof] = actspace[count++];
         }
      }
   }
   else
   {
      for(In idof=I_0; idof<ndof; ++idof)
      {
         mVecNAct[idof] = actspace[idof];
      }
   }

   // Reserve maximum size of integral block
   Uin reserve_size = std::pow(*std::max_element(this->mVecNAct.begin(), this->mVecNAct.end()), 2);
   this->mContractor.Reserve(reserve_size);
}

/**
 * Get occ element of integrals
 *
 * @param aMode
 * @param aOper
 * @return
 *    Occ-occ integral for mode and oper
 **/
typename LinearRasTdHIntegrals::param_t LinearRasTdHIntegrals::GetOccElement
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   )  const
{
   return this->mActiveIntegrals[aMode][aOper][I_0][I_0];
}

/**
 * Get number of operators from size of active integrals
 *
 * @param aMode
 * @return
 *    Number of one-mode operators for mode
 **/
In LinearRasTdHIntegrals::NOper
   (  LocalModeNr aMode
   )  const
{
   return this->mActiveIntegrals[aMode].size();
}

/**
 * Contract a set of coefficients using the integrals of a specific one-mode operator.
 *
 * @note This is not actually used in MCTDH, but must be there in order to use the V3Contrib framework.
 *
 * @param arM1
 * @param arM0
 * @param aOperMode
 * @param aOper
 * @param arY1
 * @param arY0
 * @param aWhich
 * @param aType
 **/
void LinearRasTdHIntegrals::Contract
   (  const ModeCombi& arM1
   ,  const ModeCombi& arM0
   ,  const LocalModeNr aOperMode
   ,  const LocalOperNr aOper
   ,  GeneralMidasVector<param_t>& arY1
   ,  const GeneralMidasVector<param_t>& arY0
   ,  In aWhich
   ,  midas::vcc::IntegralOpT aType
   )  const
{
   LOGCALL("integrals contract MidasVector");
   return this->mContractor.Contract(arM1, arM0, aOperMode, aOper, arY1, arY0, aWhich, this->mActiveIntegrals, this->mVecNAct, aType);
}


/**
 * Make down contraction of NiceTensor with integrals
 * @param aOperMode         The operator mode to be contracted
 * @param aOper             Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx              The index to be contracted
 * @param aType             Type of integral transform
 * @param aTensorPtr        The tensor to be contracted
 * @result                  The result of the contraction in NiceTensor format
 **/
NiceTensor<typename LinearRasTdHIntegrals::param_t> LinearRasTdHIntegrals::ContractDown
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<param_t>& aTensorPtr
   )  const
{
   LOGCALL("integrals contract down");
   return this->mContractor.ContractDown(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mActiveIntegrals, this->mVecNAct);
}

/**
 * Make forward contraction of NiceTensor with integrals
 * @param aOperMode         The operator mode to be contracted
 * @param aOper             Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx              The index to be contracted
 * @param aType             Type of integral transform
 * @param aTensorPtr        The tensor to be contracted
 * @result                  The result of the contraction in NiceTensor format
 **/
NiceTensor<typename LinearRasTdHIntegrals::param_t> LinearRasTdHIntegrals::ContractForward
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<param_t>& aTensorPtr
   )  const
{
   LOGCALL("integrals contract forward");
   return this->mContractor.ContractForward(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mActiveIntegrals, this->mVecNAct);
}

/**
 * Make up contraction of NiceTensor with integrals
 * @param aOperMode         The operator mode to be contracted
 * @param aOper             Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx              The index to be contracted
 * @param aType             Type of integral transform
 * @param aTensorPtr        The tensor to be contracted
 * @result                  The result of the contraction in NiceTensor format
 **/
NiceTensor<typename LinearRasTdHIntegrals::param_t> LinearRasTdHIntegrals::ContractUp
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<param_t>& aTensorPtr
   )  const
{
   LOGCALL("integrals contract up");
   return this->mContractor.ContractUp(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mActiveIntegrals, this->mVecNAct);
}


} /* namespace midas::mctdh */
