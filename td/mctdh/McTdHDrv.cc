/*
************************************************************************
*
* @file                 McTdHDrv.h
*
* Created:              05-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Declaration of McTdHDrv
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "McTdHDrv.h"
#include "McTdHInterface.h"
#include "util/Io.h"
#include "util/Timer.h"

#include "input/Input.h"
#include "input/FindCalculation.h"

#include "eom/LinearCasTdHEom.h"

namespace midas::mctdh
{

/**
 * Driver for MCTDH calculations.
 **/
void McTdHDrv
   (
   )
{
   // Print header
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin MCTDH calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   auto ncalc = gMcTdHCalcDef.size();

   Mout << "Number of MCTDH calculations:    " << ncalc << std::endl;

   // Loop over calculations
   for(auto& calcdef : gMcTdHCalcDef)
   {
      std::string name_str = " Begin MCTDH calculation '" + calcdef.GetName() + "' ";
      Out72Char(Mout,'+','-','+');
      Out72Char(Mout,'|',' ','|');
      OneArgOut72(Mout, name_str, '|');
      Out72Char(Mout,'|',' ','|');
      Out72Char(Mout,'+','-','+');

      // Find operator for reference calculation
      const auto& init_name = calcdef.GetInitCalcName();
      std::string ref_opername = "";
      std::string ref_basname = "";
      switch   (  calcdef.GetInitialWavePacket()
               )
      {
         case input::McTdHCalcDef::initType::VSCF:
         {
            auto i_vscf = midas::input::FindCalculationIndex(gVscfCalcDef, init_name, "VSCF");
            ref_opername = gVscfCalcDef[i_vscf].Oper();
            ref_basname = gVscfCalcDef[i_vscf].Basis();
            break;
         }
         case input::McTdHCalcDef::initType::VCI:
         {
            auto i_vci = midas::input::FindCalculationIndex(gVccCalcDef, init_name, "VCI");
            MidasAssert(gVccCalcDef[i_vci].Vci(), "'" + init_name + "' is not a VCI calculation!");
            ref_opername = gVccCalcDef[i_vci].Oper();
            ref_basname = gVccCalcDef[i_vci].Basis();
            break;
         }
//         case input::McTdHCalcDef::initType::TDH:
//         {
//            auto i_tdh = midas::input::FindCalculationIndex(gTdHCalcDef, init_name, "TDH");
//            ref_opername = gTdHCalcDef[i_tdh].Oper();
//            ref_basname = gTdHCalcDef[i_tdh].Basis();
//            break;
//         }
         case input::McTdHCalcDef::initType::MCTDH:
         {
            auto i_mctdh = midas::input::FindCalculationIndex(gMcTdHCalcDef, init_name, "MCTDH");
            ref_opername = gMcTdHCalcDef[i_mctdh].GetOper();
            ref_basname = gMcTdHCalcDef[i_mctdh].GetBasis();
            break;
         }
         default:
         {
            MIDASERROR("Unknown initType!");
         }
      }
      const auto& ref_opdef = gOperatorDefs.GetOperator(ref_opername);

      // Find and initialize basis (same primitive basis as reference calculation)
      auto basname = calcdef.GetBasis();
      if (  basname.empty()
         )
      {
         // Set default basis to be the one of the reference calculation.
         calcdef.SetBasis(ref_basname);  // To get the ref_basname correct if we restart from this MCTDH calc.
         basname = ref_basname;
      }
      else
      {
         // At this point we only allow the use of identical basis sets. But this may change!
         MidasAssert(basname == ref_basname, "MCTDH must use same primitive basis as reference calculation!");
      }
      In i_bas = -I_1;
      if (  gBasis.size() == 1
         )
      {
         i_bas = I_0;
      }
      else
      {
         for(In i=I_0; i<gBasis.size(); ++i)
         {
            if (  gBasis[i].GetmBasName() == basname
               )
            {
               i_bas = i;
               break;
            }
         }
      }
      if (  i_bas == -I_1
         )
      {
         MIDASERROR("Basis '" + basname + "' not found!");
      }

      BasDef* basdef = &gBasis[i_bas];
      basdef->InitBasis(ref_opdef);

      // Find operator for MCTDH calculation
      const auto& opername = calcdef.GetOper();
      const auto& opdef = gOperatorDefs.GetOperator(opername);

      // Check that opdef and ref_opdef have same number of modes and same mode ordering. Otherwise, we will run into problems...
      auto nmodes = opdef.NmodesInOp();
      auto ref_nmodes = ref_opdef.NmodesInOp();
      MidasAssert(nmodes == ref_nmodes, "MCTDH: Operator for reference calculation has wrong number of modes!");
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         GlobalModeNr g_mode = opdef.GetGlobalModeNr(imode);
         GlobalModeNr ref_g_mode = ref_opdef.GetGlobalModeNr(imode);

         MidasAssert(g_mode == ref_g_mode, "MCTDH: Operator for reference calculation has different mode ordering. This is not supported!");
      }

      // Construct operator
      using step_t = Nb;
      using param_t = std::complex<Nb>;
      using oper_t = midas::td::LinCombOper<step_t, param_t, OpDef>;
      oper_t oper;

      // Emplace Hamiltonian
      oper.emplace_back
         (  opdef
         ,  1
         );

      // If we use pulse, add additional operator term
      if (  calcdef.GetGaussianPulse()
         )
      {
         using pulse_t = midas::td::GaussPulse<step_t, param_t>;
         auto p_pulse = std::make_unique<pulse_t>(calcdef.GetPulseSpecs());
         const auto& pulse = *p_pulse;
         const auto& pulse_opdef = gOperatorDefs.GetOperator(calcdef.GetPulseOper());

         oper.emplace_back
            (  pulse_opdef
            ,  [p=pulse](step_t t) { return p(t); }
            ,  [p=pulse](step_t t) { return p.Deriv(t); }
            );
      }

      // Initialize and propagate MCTDH
      switch   (  calcdef.GetMethodInfo().template At<input::McTdHCalcDef::mctdhID>("PARAMETERIZATION")
               )
      {
         case input::McTdHCalcDef::mctdhID::LCASTDH:
         {
            LinearCasTdH mctdh(&calcdef, oper, basdef);
            mctdh.Propagate();
            break;
         }
         case input::McTdHCalcDef::mctdhID::LRASTDH:
         {
            LinearRasTdH mctdh(&calcdef, oper, basdef);
            mctdh.Propagate();
            break;
         }
         default:
         {
            MIDASERROR("Unknown MCTDH parameterization!");
         }
      }
   }

   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," MCTDH Done",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
}

} /* namespace midas::mctdh */
