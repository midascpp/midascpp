/*
************************************************************************
*
* @file                 LinearCasTdHEom.h
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Transformer for calculating derivatives in "standard"
*                       double-linear MCTDH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef LINEARCASTDHEOM_H_INCLUDED
#define LINEARCASTDHEOM_H_INCLUDED

#include "util/type_traits/Complex.h"
#include "td/mctdh/integrals/McTdHIntegrals.h"
#include "LinearMcTdHEomBase.h"
#include "McTdHIntermediates.h"

// Forward decl
namespace midas::input
{
   class McTdHCalcDef;
}
class BasDef;
class OpDef;

namespace midas::mctdh
{

/**
 * Equation-of-motion for "standard" MCTDH with complete-active-space (CAS) expansion and 
 * linear parameterization of both modals and configuration-space parameters.
 **/
class LinearCasTdHEom
   :  public LinearMcTdHEomBase
               <  std::complex<Nb>        // param_t
               ,  CasTdHVectorContainer   // vec_t
               ,  LinearMcTdHIntegrals    // integrals_t
               >
{
   public:
      // Alias
      using Base = LinearMcTdHEomBase<std::complex<Nb>, CasTdHVectorContainer, LinearMcTdHIntegrals>;
      using param_t = typename Base::param_t;
      using real_t = typename Base::real_t;
      using step_t = typename Base::step_t;
      using vec_t = typename Base::vec_t;
      using coefs_t = typename vec_t::coefs_t;
      using modal_t = typename vec_t::modal_t;
      using integrals_t = typename Base::integrals_t;
      using mat_t = typename Base::mat_t;
      using DataPtr = typename Base::DataPtr;
      using h_mat_t = typename Base::h_mat_t;
      using oper_t = typename Base::oper_t;

      using intermeds_t = LinearCasTdHIntermediates;


   private:
      //! Intermediates. One for each oper in McTdHEomBase::mOper
      std::vector<intermeds_t> mIntermediates;


      //! Implementation of derivative
      vec_t DerivativeImpl
         (  step_t
         ,  const vec_t&
         )  override;

      //! Return zero vector of correct shape.
      vec_t ShapedZeroVectorImpl
         (
         )  const override;

      //! Auto-correlation function
      param_t CalculateAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const override;

      //! Half-time auto-correlation function
      param_t CalculateHalfTimeAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const override;

      //! Expectation values. Full and active-terms.
      std::vector<param_t> ExpectationValueImpl
         (  const integrals_t& aIntegrals
         ,  const vec_t& aY
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Calculate time derivative of coefficients
      coefs_t CoefficientDerivativeImpl
         (  step_t
         ,  const vec_t&
         );

      //! Get H matrix
      const h_mat_t& GetHMatrix
         (  In aOper
         ,  In aTerm
         ,  In aMode
         ,  In aFactor
         )  const override;

      //! Construct density matrix as column-major pointer
      DataPtr ConstructDensityMatrix
         (  LocalModeNr
         ,  const vec_t&
         )  const override;

      //! Reference coefficient
      param_t ReferenceCoefficientImpl
         (  const vec_t&
         )  const override;

      //! Apply operator
      bool ApplyOperatorImpl
         (  const integrals_t&
         ,  vec_t&
         ,  real_t&
         )  override;

      //! Add constraint to modal derivative
      void AddNonProjectedModalDerivativeTerms
         (  LocalModeNr aMode
         ,  const modal_t& aModals
         ,  DataPtr& arDyDt
         )  const override;

      //! Calculate wave-function norm2
      real_t CalculateWfNorm2Impl
         (  const vec_t&
         )  const override;


   public:
      //! Constructor
      LinearCasTdHEom
         (  const input::McTdHCalcDef* const
         ,  const oper_t&
         ,  const BasDef* const
         );
};

} /* namespace midas::mctdh */

#endif /* LINEARCASTDHEOM_H_INCLUDED */
