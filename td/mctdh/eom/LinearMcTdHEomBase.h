/*
************************************************************************
*
* @file                 LinearMcTdHEomBase.h
*
* Created:              05-04-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Base class for L-MCTDH EOMs. Implements methods 
*                       that are independent of CAS or RAS.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef LINEARMCTDHEOMBASE_H_INCLUDED
#define LINEARMCTDHEOMBASE_H_INCLUDED


#include "McTdHEomBase.h"
#include "util/AbsVal.h"
#include "lapack_interface/HEEVD.h"
#include "tensor/NiceTensor.h"

namespace midas::mctdh
{

/**
 * Abstract base class for linear MCTDH methods.
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename VEC_T
   ,  typename INTEGRALS_T
   >
class LinearMcTdHEomBase
   :  public McTdHEomBase
               <  PARAM_T
               ,  VEC_T
               ,  INTEGRALS_T
               >
{
   public:
      //! Aliases
      using Base = McTdHEomBase<PARAM_T, VEC_T, INTEGRALS_T>;
      using param_t = typename Base::param_t;
      using real_t = typename Base::real_t;
      using step_t = typename Base::step_t;
      using vec_t = typename Base::vec_t;
      using coefs_t = typename vec_t::coefs_t;
      using modal_t = typename vec_t::modal_t;
      using integrals_t = typename Base::integrals_t;
      using offset_t = typename Base::offset_t;
      using mat_t = typename Base::mat_t;
      using oper_t = typename Base::oper_t;

      using DataPtr = std::unique_ptr<param_t[]>;
      using h_mat_t = NiceTensor<param_t>;

   private:
      //! Density matrices as column-major pointers
      std::vector<DataPtr> mDensityMatrices;

      //! Use active-space projector that works for non-orthogonal active space
      bool mNonOrthoActiveSpaceProjector = true;

      //! Do projection after transforming with inverse density matrix
      bool mProjectAfterInverseDensity = true;

      //! Fail in ProcessNewVector if active space not orthonormal
      bool mFailIfNonOrtho = false;

      //! Re-normalize wave function in ProcessNewVector
      bool mReNormalize = false;

      /**
       * @param aOper      Which operator in mOper?
       * @param aTerm      Which term in aOper?
       * @param aMode      Which mode?
       * @param aFactor    Factor in term that operators on aMode
       * @return
       *    H (mean-field) matrix for term and mode
       **/
      virtual const h_mat_t& GetHMatrix
         (  In aOper
         ,  In aTerm
         ,  In aMode
         ,  In aFactor
         )  const = 0;

      /**
       * Construct density matrix as column-major pointer
       *
       * @param aMode
       * @param aY
       * @return
       *    Density matrix for aMode as column-major pointer
       **/
      virtual DataPtr ConstructDensityMatrix
         (  LocalModeNr aMode
         ,  const vec_t& aY
         )  const = 0;

      /**
       * Add constraint term to modal derivative
       *
       * @param aMode
       * @param aModals
       * @param arDyDt
       **/
      virtual void AddNonProjectedModalDerivativeTerms
         (  LocalModeNr aMode
         ,  const modal_t& aModals
         ,  DataPtr& arDyDt
         )  const = 0;

      /**
       * Calculate wave-function norm2
       *
       * @param aY      WF vector
       * @return
       *    ||aY||^2
       **/
      virtual real_t CalculateWfNorm2Impl
         (  const vec_t& aY
         )  const = 0;

      /**
       * Calculate sigma = (1-P) H_eff * phi for given mode
       *
       * @note The (1-P) projector can be applied later.
       *
       * @param aT
       * @param aY
       * @param aMode
       **/
      DataPtr ConstructSigma
         (  step_t aT
         ,  const vec_t& aY
         ,  LocalModeNr aMode
         )
      {
         // For now, we just use H-matrix elements. Perhaps later, we can use other types of intermeds.
         return this->ConstructSigmaFromHMatricesImpl(aT, aY, aMode);
      }

      /**
       * Calculate sigma = (1-P) H_eff * phi for given mode using H-matrix elements.
       *
       * @note The (1-P) projector can be applied later.
       *
       * @param aT
       * @param aY
       * @param aMode
       **/
      DataPtr ConstructSigmaFromHMatricesImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  LocalModeNr aMode
         )
      {
         LOGCALL("calls");
      
         // DEBUG
         if (  this->DoIo(I_10)
            )
         {
            Mout  << " ConstructSigma for mode: " << aMode << std::endl;
         }
      
         auto nbas = this->NBas(aMode);
         auto nact = this->NAct(aMode);
      
         auto sigma_size = nbas*nact;
      
         // init tmp vector (for using OpenMP)
         GeneralMidasVector<param_t> sigma_vec(sigma_size);
         sigma_vec.Zero();

         // Tmp vector needed for each operator in the Hamiltonian (otherwise OpenMP initialization will give wrong results)
         auto tmp_sigma_vec = sigma_vec;
      
         // get modals
         const auto& modals = aY.Modals();
         const auto& off_m = this->ModalOffsets()[aMode];
      
         // Construct inverse overlap matrix (if necessary)
         bool inv_overlap_proj = this->NonOrthoActiveSpaceProjector() && !this->ProjectAfterInverseDensity();
      
         mat_t s_m_inv;
         if (  inv_overlap_proj
            )
         {
            s_m_inv = std::move(this->InverseOverlapMatrix(aMode));
         }

         // Loop over operators in McTdHEomBase::mOper
         // We do not parallelize over different operators (this should be done for optimal performance)
         for(In iop = I_0; iop<this->Oper().size(); ++iop)
         {
            const auto& oper_i = this->Oper()[iop];
            const auto& opdef = oper_i.Oper();
            const auto& integrals_i = this->Integrals()[iop];
      
            if (  opdef.UseActiveTermsAlgo()
               )
            {
               In n_act_terms = opdef.NactiveTerms(aMode);
      
               // Loop over active terms
               #pragma omp parallel
               {
                  In i_term = I_0;
                  In nfac = -I_1;
                  In i_fac_thismode = -I_1;
                  In i_op_mode = -I_1;
                  In ioper = -I_1;
                  mat_t trans_td_int;
                  mat_t stabilized_td_int;
                  param_t coef;
      
                  #pragma omp for schedule(dynamic) reduction(ComplexMidasVector_Add : tmp_sigma_vec)
                  for(In i_act_term = I_0; i_act_term<n_act_terms; ++i_act_term)
                  {
                     // Get term index
                     i_term = opdef.TermActive(aMode, i_act_term);
      
                     // Get number of factors
                     nfac = opdef.NfactorsInTerm(i_term);
      
                     // Skip one-mode factors (add after projection)
                     if (  nfac == I_1
                        )
                     {
                        continue;
                     }
      
                     // Get factor for aMode
                     i_fac_thismode = -I_1;
                     for(In i_fac=I_0; i_fac<nfac; ++i_fac)
                     {
                        i_op_mode  = opdef.ModeForFactor(i_term, i_fac);
                        if (  i_op_mode == aMode
                           )
                        {
                           i_fac_thismode = i_fac;
                           break;
                        }
                     }
      
                     // Sanity check
                     if (  i_fac_thismode == -I_1
                        )
                     {
                        MIDASERROR("Factor for mode " + std::to_string(aMode) + " not found in term " + std::to_string(i_term) + ". This should not happen!");
                     }
      
                     // get mean-field matrix (see MCTDH review)
                     const auto& h_mat = this->GetHMatrix(iop, i_term, aMode, i_fac_thismode);
      
                     // Get h_mat_data stored as row-major matrix
                     const auto* h_mat_data = static_cast<const SimpleTensor<param_t>* const>(h_mat.GetTensor())->GetData();
      
                     // Get oper for aMode
                     ioper = opdef.OperForFactor(i_term, i_fac_thismode);
      
                     // Get integral matrices
                     const auto& half_trans_int = integrals_i.GetHalfTransformedIntegrals(aMode, ioper);
                     const auto& pure_td_int = integrals_i.GetActiveIntegrals(aMode, ioper);
      
                     // Stabilize the td_int
                     if (  inv_overlap_proj
                        )
                     {
                        stabilized_td_int.SetNewSize(nact, nact);
                        stabilized_td_int = s_m_inv * pure_td_int;
                     }
      
                     const auto& td_int = inv_overlap_proj ? stabilized_td_int : pure_td_int;
      
                     // Transform td_int with U matrix
                     // If the projection is applied afterwards, we do not need the second term!
                     if (  !this->ProjectAfterInverseDensity()
                        )
                     {
                        trans_td_int.SetNewSize(nbas, nact);
                        for(In r=I_0; r<nbas; ++r)
                        {
                           for(In y=I_0; y<nact; ++y)
                           {
                              param_t val(0.);
                              
                              for(In w=I_0; w<nact; ++w)
                              {
                                 val += modals[off_m[w]+r] * td_int[w][y];
                              }
      
                              trans_td_int[r][y] = val;
                           }
                        }
                     }
      
                     // Add term to sigma_xr = sum_t c_t H_xy (h_halftrans_ry - U_rw * S^-1_wz  h_td_zy)
                     // In case the projection is performed after the inverse-density transform, we only need the first term.
                     coef = opdef.Coef(i_term) * oper_i.Coef(aT);
                     for(In x=I_0; x<nact; ++x)
                     {
                        for(In r=I_0; r<nbas; ++r)
                        {
                           param_t val(0.);
                           
                           for(In y=I_0; y<nact; ++y)
                           {
                              if (  this->ProjectAfterInverseDensity()
                                 )
                              {
                                 val += h_mat_data[x*nact + y] * half_trans_int[r][y];
                              }
                              else
                              {
                                 val += h_mat_data[x*nact + y] * (half_trans_int[r][y] - trans_td_int[r][y]);
                              }
                           }
      
                           // Add to sigma_xr in column-major pointer (scale with oper coef here!)
                           tmp_sigma_vec[x+r*nact] += coef*val;
                        }
                     }
                  }
               }
            }
            else
            {
               MIDASERROR("We should always use active-terms algorithm!");
            }

            // Add to sigma_vec
            sigma_vec += tmp_sigma_vec;
            tmp_sigma_vec.Zero();
         }
      
         // unique_ptr takes ownership of data
         DataPtr sigma(sigma_vec.data());
         sigma_vec.SetPointerToNull(); // Make sure sigma_vec destructor doesn't clear data
      
         // return
         return sigma;
      }

      /**
       * Add one-mode terms of the Hamiltonian to sigma after transforming with inverse density matrix.
       *
       * @param aT
       * @param aY
       * @param aMode
       * @param arSigma
       **/
      void AddOneModeTermsToModalDerivative
         (  step_t aT
         ,  const vec_t& aY
         ,  LocalModeNr aMode
         ,  DataPtr& arSigma
         )  const
      {
         LOGCALL("calls");
      
         // DEBUG
         if (  this->DoIo(I_10)
            )
         {
            Mout  << " AddOneModeTerms for mode: " << aMode << std::endl;
         }
      
         auto nbas = this->NBas(aMode);
         auto nact = this->NAct(aMode);
      
         auto sigma_size = nbas*nact;
      
         // get modals
         const auto& modals = aY.Modals();
         const auto& off_m = this->ModalOffsets()[aMode];
      
         // Construct inverse overlap matrix (if necessary)
         bool inv_overlap_proj = this->NonOrthoActiveSpaceProjector() && !this->ProjectAfterInverseDensity();
      
         mat_t s_m_inv;
         if (  inv_overlap_proj
            )
         {
            s_m_inv = std::move(this->InverseOverlapMatrix(aMode));
         }

         // Loop over operators in McTdHEomBase::mOper
         // We do not parallelize over different operators (this should be done for optimal performance)
         for(In iop = I_0; iop<this->Oper().size(); ++iop)
         {
            const auto& oper_i = this->Oper()[iop];
            const auto& opdef = oper_i.Oper();
            const auto& integrals_i = this->Integrals()[iop];
      
            if (  opdef.UseActiveTermsAlgo()
               )
            {
               In n_act_terms = opdef.NactiveTerms(aMode);
      
               // Loop over active terms
               In i_term = I_0;
               In nfac = -I_1;
               In i_fac_thismode = I_0;
               In i_op_mode = -I_1;
               In ioper = -I_1;
               mat_t trans_td_int;
               mat_t stabilized_td_int;
               param_t coef;
      
               for(In i_act_term = I_0; i_act_term<n_act_terms; ++i_act_term)
               {
                  // Get term index
                  i_term = opdef.TermActive(aMode, i_act_term);
      
                  // Get number of factors
                  nfac = opdef.NfactorsInTerm(i_term);

                  // Skip coupling terms
                  if (  nfac != I_1
                     )
                  {
                     continue;
                  }
      
                  // Get oper for aMode
                  ioper = opdef.OperForFactor(i_term, i_fac_thismode);
      
                  // Get integral matrices
                  const auto& half_trans_int = integrals_i.GetHalfTransformedIntegrals(aMode, ioper);
                  const auto& pure_td_int = integrals_i.GetActiveIntegrals(aMode, ioper);
      
                  // Stabilize the td_int
                  if (  inv_overlap_proj
                     )
                  {
                     stabilized_td_int.SetNewSize(nact, nact);
                     stabilized_td_int = s_m_inv * pure_td_int;
                  }
      
                  const auto& td_int = inv_overlap_proj ? stabilized_td_int : pure_td_int;
      
                  // Transform td_int with U matrix
                  // If the projection is applied afterwards, we do not need the second term!
                  if (  !this->ProjectAfterInverseDensity()
                     )
                  {
                     trans_td_int.SetNewSize(nbas, nact);
                     for(In r=I_0; r<nbas; ++r)
                     {
                        for(In y=I_0; y<nact; ++y)
                        {
                           param_t val(0.);
                           
                           for(In w=I_0; w<nact; ++w)
                           {
                              val += modals[off_m[w]+r] * td_int[w][y];
                           }
      
                           trans_td_int[r][y] = val;
                        }
                     }
                  }
      
                  // Add term to sigma_xr = c_t (h_halftrans_rx - U_rw * S^-1_wz  h_td_zx)
                  // In case the projection is performed after the inverse-density transform, we only need the first term.
                  coef = opdef.Coef(i_term) * oper_i.Coef(aT);
                  for(In x=I_0; x<nact; ++x)
                  {
                     for(In r=I_0; r<nbas; ++r)
                     {
                        auto val = this->ProjectAfterInverseDensity() ? half_trans_int[r][x] : (half_trans_int[r][x] - trans_td_int[r][x]);
      
                        // Add to sigma_xr in column-major pointer (scale with oper coef here!)
                        arSigma[x+r*nact] += coef*val;
                     }
                  }
               }
            }
            else
            {
               MIDASERROR("We should always use active-terms algorithm!");
            }
         }
      }


      /**
       * Transform sigma with (regularized) inverse density matrix
       * NB: This modifies mDensityMatrices
       *
       * @param aMode      Mode
       * @param aY         Wave function to construct density matrix from
       * @param arSigma    Sigma matrix as column-major pointer sigma_vr
       **/
      void InverseDensityMatrixTransform
         (  LocalModeNr aMode
         ,  const vec_t& aY
         ,  DataPtr& arSigma
         )
      {
         auto nbas = this->NBas(aMode);
         auto nact = this->NAct(aMode);
      
         // Get density matrix
         auto& densmat_ptr = this->mDensityMatrices[aMode];
      
         // Invert density matrix
         auto reg = this->CalcDef()->GetMethodInfo().template At<input::McTdHCalcDef::regularizationType>("REGULARIZATIONTYPE");
         auto eps = this->CalcDef()->GetMethodInfo().template At<Nb>("REGULARIZATIONEPSILON"); // NB: Perhaps scale by trace of D^m
      
         switch   (  reg
                  )
         {
            case input::McTdHCalcDef::regularizationType::NONE:
            {
               // setup all variables
               int m = nact; // rows of D
               int n = nact; // columns of D
               int nrhs = nbas; // number of right-hand-sides (columns of sigma)
               int lda = std::max(1,m); // leading dimension of D
               int ldb = std::max(1,std::max(n,m)); // leading dimension of sigma
               auto s = std::make_unique<real_t[]>(std::min(m,n)); // singular values
               real_t rcond = eps; // condition number
               int rank; // output rank
               int lwork = std::max(1, std::max(3*n+std::max(std::max(2*n,nrhs),m), 3*m+std::max(std::max(2*m,nrhs),n)) );// dimension of workspace
               auto work = std::make_unique<param_t[]>(lwork); // workspace
               int info; // info (output)
      
               // call lapack routine (through general wrapper)
               midas::lapack_interface::gelss(&m,&n,&nrhs,densmat_ptr.get(),&lda,arSigma.get(),&ldb,s.get(),&rcond,&rank,work.get(),&lwork,&info);
               if (  info != 0
                  )
               {
                  MidasWarning("GELSS info = " + std::to_string(info));
               }
      
               // Check if we have truncated the density matrix
               if (  rank < nact
                  )
               {
                  MidasWarning("Singular density matrix for mode " + std::to_string(aMode) + ". Dim: " + std::to_string(nact) + " Rank: " + std::to_string(rank) + ".");
               }
      
               if (  this->DoIo(I_12)
                  )
               {
                  Mout  << " Singular values of density matrix:" << std::endl;
                  for(In is=I_0; is<nact; ++is)
                  {
                     Mout  << s[is] << "  ";
                  }
                  Mout  << std::endl;
               }
      
               break;
            }
            case input::McTdHCalcDef::regularizationType::STANDARD:
            {
               // initialize eigenvalues
               auto w = std::make_unique<real_t[]>(nact);
               {
                  LOGCALL("heevd");
      
                  // Compute eigendecomposition of density matrix
                  char jobz = 'V';  // All eigenvalues and eigenvectors
                  char uplo = 'U';  // doesn't matter if 'U' or 'L'
                  int n = nact;
                  int lda = std::max(1, n);
                  int lwork = std::max(1, 2*n + n*n);
                  auto work = std::make_unique<param_t[]>(lwork);
                  int lrwork = 1 + 5*n + 2*n*n;
                  auto rwork = std::make_unique<real_t[]>(lrwork);
                  int liwork = 3 + 5*n;
                  auto iwork = std::make_unique<int[]>(liwork);
                  int info = -1;
      
                  midas::lapack_interface::heevd(&jobz, &uplo, &n, densmat_ptr.get(), &lda, w.get(), work.get(), &lwork, rwork.get(), &lrwork, iwork.get(), &liwork, &info);
      
                  // Sanity check
                  if (  info != 0
                     )
                  {
                     MIDASERROR("MCTDH: Error in eigenvalue decomposition of density matrix! info = " + std::to_string(info));
                  }
               }
      
               // Assemble regularized inverse matrix into column-major pointer
               // I.e. the eigenvalues are modified as w <- (w + eps*exp(-w/eps))^-1
               auto inv_reg_dens = std::make_unique<param_t[]>(nact*nact);
      
               {
                  LOGCALL("assemble inv reg dens");
      
                  #pragma omp parallel for collapse(2)
                  for(In icol=I_0; icol<nact; ++icol)
                  {
                     for(In irow=I_0; irow<nact; ++irow)
                     {
                        param_t val(0.);
                        for(In iev=I_0; iev<nact; ++iev)
                        {
                           val += densmat_ptr[irow+nact*iev] * midas::math::Conj(densmat_ptr[icol+nact*iev]) * real_t(1.)/(w[iev] + eps*std::exp(-w[iev]/eps));
                        }
                        inv_reg_dens[irow + nact*icol] = val;
                     }
                  }
               }
      
               if (  this->DoIo(I_12)
                  )
               {
                  Mout  << " Eigenvalues of density matrix for mode " << aMode << ":" << std::endl;
                  for(In iev=I_0; iev<nact; ++iev)
                  {
                     Mout  << w[iev] << "  ";
                  }
                  Mout  << std::endl;
               }
      
               // Call gemm
               auto c = std::make_unique<param_t[]>(nact*nbas);
               {
                  LOGCALL("inv dens times sigma");
                  char transa = 'N';
                  char transb = 'N';
                  int m = nact;
                  int n = nbas;
                  int k = nact;
                  param_t alpha(1.);
                  int lda = std::max(1, m);
                  int ldb = std::max(1, k);
                  param_t beta(0.);
                  int ldc = std::max(1, m);
      
                  midas::lapack_interface::gemm(&transa, &transb, &m, &n, &k, &alpha, inv_reg_dens.get(), &lda, arSigma.get(), &ldb, &beta, c.get(), &ldc);
               }
      
               // Move c matrix into sigma
               arSigma = std::move(c);
      
               break;
            }
            case input::McTdHCalcDef::regularizationType::IMPROVED:
            {
               MIDASERROR("Not impl!");
               break;
            }
            default:
            {
               MIDASERROR(this->Type() + ": Unknown regularization type!");
            }
         }
      }

      /**
       * Project modal derivative on orthogonal complement to active space.
       *
       * @param aMode
       * @param aY
       * @param arDeriv
       **/
      void ProjectModalDerivative
         (  LocalModeNr aMode
         ,  const vec_t& aY
         ,  DataPtr& arDeriv
         )  const
      {
         LOGCALL("calls");

         auto nact = this->NAct(aMode);
         auto nbas = this->NBas(aMode);
      
         // Get modals
         const auto& modals = aY.Modals();
         const auto& off_m = this->ModalOffsets()[aMode];
      
         // Init result to arDeriv
         auto size = nact*nbas;
         auto proj_deriv = std::make_unique<param_t[]>(size);
         for(In i=I_0; i<size; ++i)
         {
            proj_deriv[i] = arDeriv[i];
         }
      
         // Do S^-1 transform (to the left) if required
         // half_trans will then be U * S^-1
         mat_t s_m_inv;
         mat_t half_trans;
         if (  this->mNonOrthoActiveSpaceProjector
            )
         {
            s_m_inv = std::move(this->InverseOverlapMatrix(aMode));
      
            half_trans.SetNewSize(nbas, nact);
            for(In r=I_0; r<nbas; ++r)
            {
               for(In z=I_0; z<nact; ++z)
               {
                  param_t val(0.);
                  for(In w=I_0; w<nact; ++w)
                  {
                     val += modals[off_m[w]+r] * s_m_inv[w][z];
                  }
      
                  half_trans[r][z] = val;
               }
            }
         }
      
         // Transform arDeriv with U* to obtain U* * arDeriv as column-major pointer
         auto ustar_times_deriv = std::make_unique<param_t[]>(nact*nact);
         {
            auto* ptr = ustar_times_deriv.get();
            for(In v=I_0; v<nact; ++v)
            {
               for(In z=I_0; z<nact; ++z)
               {
                  param_t val(0.);
                  for(In s=I_0; s<nbas; ++s)
                  {
                     val += midas::math::Conj(modals[off_m[z]+s]) * arDeriv[v + nact*s];
                  }
                  *(ptr++) = val;
               }
            }
         }
      
      
         // Transform with U (or U*S^-1) and subtract P term
         auto* proj_deriv_ptr = proj_deriv.get();
         for(In r=I_0; r<nbas; ++r)
         {
            for(In v=I_0; v<nact; ++v)
            {
               param_t val(0.);
               for(In z=I_0; z<nact; ++z)
               {
                  if (  this->mNonOrthoActiveSpaceProjector
                     )
                  {
                     // half_trans[r][z] * ustar_times_deriv[z][v]
                     val += half_trans[r][z] * ustar_times_deriv[z + nact*v];
                  }
                  else
                  {
                     // U[r][z] * ustar_times_deriv[z][v]
                     val += modals[off_m[z]+r] * ustar_times_deriv[z + nact*v];
                  }
               }
               *(proj_deriv_ptr++) -= val;
            }
         }
      
         // Swap
         std::swap(arDeriv, proj_deriv);
      }

      /** @name Overloads from McTdHEomBase that work for linear MCTDH **/
      //!@{
      
      /**
       * Update integrals
       *
       * @param aT      Time
       * @param aY      Wave function
       **/
      void UpdateIntegralsImpl
         (  step_t aT
         ,  const vec_t& aY
         )  override
      {
         for(auto& ints : this->Integrals())
         {
            ints.Update(aY.Modals(), this->ModalOffsets());
         }
      }

      /**
       * @note This function will not update integrals! That will be done when evaluating the derivative the first time!
       *
       * @param arY     Wave function to start integration from
       **/
      void PrepareIntegrationImpl
         (  vec_t& arY
         )  override
      {
         // Set arY to initial wave function
         MidasAssert(arY.TotalSize() == this->InitialWaveFunction().TotalSize(), "Size of initial wave function does not match constructed vector.");
         arY = this->InitialWaveFunction();
      
         // Check orthonormality
         if (  !this->CheckOrthonormality(arY, false, true)
            )
         {
            MIDASERROR(this->Type() + " propagation requires an orthonormal basis!");
         }
      }

      /**
       * Process new vector
       *
       * @param arY
       * @param aYOld
       * @param arT
       * @param aTOld
       * @param arDyDt
       * @param aDyDtOld
       * @param arStep
       * @param arNDerivFail
       * @param aSaneY
       * @param aSaneDyDt
       *
       * @return
       *    Step okay?
       **/
      bool ProcessNewVectorImpl
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& arT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         ,  bool aSaneY
         ,  bool aSaneDyDt
         )  const override
      {
         if (  arNDerivFail > 0
            )
         {
            MIDASERROR("Derivative should not fail for " + this->Type() + ".");
         }

         // Check orthonormality of active spaces
         bool ortho = this->CheckOrthonormality(arY, libmda::numeric::float_eq(arT, this->IntegralsTime()), false);
      
         bool ortho_fail = this->mFailIfNonOrtho && !ortho;

         // Re-normalize if requested
         if (  this->mReNormalize
            )
         {
            auto norm = std::sqrt(this->CalculateWfNorm2Impl(arY));
            arY.Coefs().Scale(1./norm);
         }
      
         // Call base impl with modified aSaneY
         return Base::ProcessNewVectorImpl(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail, aSaneY && !ortho_fail, aSaneDyDt);
      }

      //!@}

   protected:
      /**
       * Pre-calculate density matrices and wave-function norm
       *
       * @param aY
       **/
      void PreCalculateDensityMatrices
         (  const vec_t& aY
         )
      {
         LOGCALL("calls");

         // Calculate density matrices
         #pragma omp parallel for
         for(In imode=I_0; imode<this->NDof(); ++imode)
         {
            this->mDensityMatrices[imode] = this->ConstructDensityMatrix(imode, aY);
         }
      }

      /**
       * Update the wave-function norm2
       *
       * @param aY      The wf vector
       **/
      void UpdateNorm2
         (  const vec_t& aY
         )
      {
         LOGCALL("calls");

         if (  this->CalcDef()->GetMethodInfo().template At<bool>("EXACTNORM2")
            )
         {
            this->mWfNorm2 = this->CalculateWfNorm2Impl(aY);
         }
         else
         {
            real_t norm2 = 0;

            // Take trace of density matrix for mode 0
            auto nact = this->NAct(I_0);
            for(In i=I_0; i<nact; ++i)
            {
               norm2 += std::real(this->DensityMatrixElement(I_0, i, i));
            }

            this->mWfNorm2 = norm2;
         }
      }

      /**
       * Calculate derivative of modals
       *
       * @param aT      Time
       * @param aY      Wave function
       *
       * @return
       *    Modal derivative
       **/
      modal_t ModalDerivativeImpl
         (  step_t aT
         ,  const vec_t& aY
         )
      {
         LOGCALL("calls");
      
         modal_t result(this->mNModalParams);
      
         // ###########################
         // NB: Make MPI parallel here!
         // ###########################
         for(LocalModeNr imode = I_0; imode<this->NDof(); ++imode)
         {
            LOGCALL("mode loop");

            // Get global mode number (from first operator in mrOper)
            auto gmode = this->Oper().front().Oper().GetGlobalModeNr(imode);

            if (  this->AssumeFixedElectronicDofModals()
               && gmode == this->ElectronicDofNr()
               )
            {
               continue;
            }
      
            auto nbas = this->NBas(imode);
            auto nact = this->NAct(imode);
      
            // Sigma matrix (modal transformed by effective Hamiltonian and projector)
            DataPtr sigma = nullptr;
      
            if (  !this->Exact()
               )
            {
               {
                  LOGCALL("calculate sigma");
      
                  sigma = std::move(this->ConstructSigma(aT, aY, imode));
               }
      
               // Transform sigma with inverse density matrix to obtain i*\dot{\phi}
               {
                  LOGCALL("inverse density-matrix transform");
                  this->InverseDensityMatrixTransform(imode, aY, sigma);
               }

               // Add one-mode terms
               {
                  LOGCALL("Add one-mode terms");
                  this->AddOneModeTermsToModalDerivative(aT, aY, imode, sigma);
               }
      
               // Do projection after density transform?
               if (  this->mProjectAfterInverseDensity
                  )
               {
                  LOGCALL("projection");
                  this->ProjectModalDerivative(imode, aY, sigma);
               }
            }

            // Add constraint term
            // This can be done even if Exact = true
            {
               LOGCALL("add constraint");
               this->AddNonProjectedModalDerivativeTerms(imode, aY.Modals(), sigma);
            }
      
            // Set data in result and multiply by -i (or -1 for imag. time)
            if (  sigma
               )
            {
               LOGCALL("set data");
               for(In r=I_0; r<nbas; ++r)
               {
                  for(In v=I_0; v<nact; ++v)
                  {
                     const auto& deriv = sigma[v+r*nact];
                     result[r + this->ModalOffsets()[imode][v]] = this->ImagTime() ? CC_M_1 * deriv : CC_M_I * deriv;
                  }
               }
            }
         }
      
         // Return result
         return result;
      }

      /**
       * Construct inverse overlap matrix
       *
       * @param aMode      Mode
       * @return
       *    Inverse active-space overlap matrix for mode
       **/
      mat_t InverseOverlapMatrix
         (  LocalModeNr aMode
         )  const
      {
         LOGCALL("inverse overlap");

         auto nact = this->NAct(aMode);
         mat_t s_m_inv(nact);
      
         // Construct column-major pointer
         const auto& s_m = this->H0Integrals().GetActiveOverlaps(aMode);
         auto s_data = std::make_unique<param_t[]>(nact*nact);
         auto* s_data_ptr = s_data.get();
         for(In j=I_0; j<nact; ++j)
         {
            for(In i=I_0; i<nact; ++i)
            {
               *(s_data_ptr++) = s_m[i][j];
            }
         }
      
         // Set up variables
         char uplo = 'L';
         int n = nact;
         int lda = std::max(1, n);
         int info;
      
         // Factorize using potrf
         midas::lapack_interface::potrf(&uplo, &n, s_data.get(), &lda, &info);
      
         if (  info != 0
            )
         {
            MIDASERROR("Error in factorization of overlap matrix. POTRF info = " + std::to_string(info));
         }
      
         // Calculate inverse with potri
         midas::lapack_interface::potri(&uplo, &n, s_data.get(), &lda, &info);
      
         if (  info != 0
            )
         {
            MIDASERROR("Error in constructing inverse overlap matrix. POTRI info = " + std::to_string(info));
         }
      
         // Move data to s_m_inv (lower triangle is stored in s_data)
         auto* inv_s_ptr = s_data.get();
         for(In j=I_0; j<nact; ++j)
         {
            inv_s_ptr += j;
            s_m_inv[j][j] = *(inv_s_ptr++);
      
            for(In i=j+I_1; i<nact; ++i)
            {
               s_m_inv[i][j] = *(inv_s_ptr++);
               s_m_inv[j][i] = midas::math::Conj(s_m_inv[i][j]);
            }
         }
      
         return s_m_inv;
      }

      /**
       * Check orthonormallty of active space
       *
       * @param arY
       * @param aUseOverlaps        Use overlaps from time-depenendent integrals
       * @param aReNormalize        Normalize modals
       *
       * @return
       *    Orthonormal?
       **/
      bool CheckOrthonormality
         (  vec_t& arY
         ,  bool aUseOverlaps
         ,  bool aReNormalize
         )  const
      {
         LOGCALL("calls");
      
         bool result = true;
      
         for(In imode=I_0; imode<this->NDof(); ++imode)
         {
            auto nact = this->NAct(imode);
            auto nbas = this->NBas(imode);
            auto off = this->ModalOffsets()[imode];
      
            bool ortho_m = true;
      
            mat_t s_mat;
            if (  aUseOverlaps
               )
            {
               s_mat.SetNewSize(nact, nact);
               s_mat = this->H0Integrals().GetActiveOverlaps(imode);
            }
      
            std::vector<real_t> norms;
            norms.reserve(nact);
      
            for(In v=I_0; v<nact; ++v)
            {
               auto norm2 = aUseOverlaps ? s_mat[v][v] : Dot(arY.Modals(), arY.Modals(), off[v], nbas);
               norms.emplace_back(std::sqrt(std::real(norm2)));
      
               // Check normalization.
               if (  libmda::numeric::float_neq(std::sqrt(std::real(norm2)), step_t(1.), 100)             // Norm must be 1
                  || !libmda::numeric::float_numeq_zero(std::imag(norm2), std::real(norm2), 2)   // Norm must be real
                  )
               {
                  step_t norm = std::sqrt(std::real(norm2));
      
                  if (  this->DoIo(I_10)
                     )
                  {
                     Mout  << " Active modal " << v << " for mode " << imode << " not normalized. <v^m|v^m> = " << norm2 << ", |v^m| = " << norm << std::endl;
                  }
               }
      
               for(In w=v+I_1; w<nact; ++w)
               {
                  // Calculate overlap
                  auto overlap = aUseOverlaps ? s_mat[v][w] : Dot(arY.Modals(), arY.Modals(), off[v], off[w], nbas);
      
                  // Check orthogonality. If not orthogonal, the check fails!
                  if (  !libmda::numeric::float_numeq_zero(std::abs(overlap), step_t(1.), 500)   // Off-diag elements must be 0
                     )
                  {
                     if (  this->DoIo(I_10)
                        )
                     {
                        Mout  << " Active modals " << v << " and " << w << " for mode " << imode << " are not orthogonal. <v^m|w^m> = " << overlap << std::endl;
                     }
                     ortho_m = false;
                  }
               }
            }
      
            // Re-normalize
            if (  aReNormalize
               )
            {
               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << " Re-normalizing active modals for mode: " << imode << std::endl;
               }
      
               for(In v=I_0; v<nact; ++v)
               {
                  for(In r=I_0; r<nbas; ++r)
                  {
                     arY.Modals()[off[v]+r] /= norms[v];
                  }
               }
            }
      
            // Update result
            result = result && ortho_m;
         }
      
         return result;
      }

   public:
      //! c-tor
      LinearMcTdHEomBase
         (  const input::McTdHCalcDef* const apCalcDef
         ,  const oper_t& aOper
         ,  const BasDef* const apBasDef
         )
         :  McTdHEomBase
               <  PARAM_T
               ,  VEC_T
               ,  INTEGRALS_T
               >
               (  apCalcDef
               ,  aOper
               ,  apBasDef
               )
         ,  mDensityMatrices
               (  this->NDof()
               )
         ,  mNonOrthoActiveSpaceProjector
               (  apCalcDef->GetMethodInfo().template At<bool>("NONORTHOPROJECTOR")
               )
         ,  mProjectAfterInverseDensity
               (  apCalcDef->GetMethodInfo().template At<bool>("PROJECTAFTERINVERSEDENSITY")
               )
         ,  mFailIfNonOrtho
               (  apCalcDef->GetMethodInfo().template At<bool>("FAILIFNONORTHO")
               )
         ,  mReNormalize
               (  apCalcDef->GetMethodInfo().template At<bool>("RENORMALIZEWF")
               )
      {
         // Calculate modal offsets and number of modal coefficients
         size_t n_modal_coef = 0;
         for(LocalModeNr i_op_mode=I_0; i_op_mode<this->NDof(); ++i_op_mode)
         {
            In nbas = this->NBas(i_op_mode);
            In nact = this->NAct(i_op_mode);

            // Set offsets
            for(In iact=I_0; iact<nact; ++iact)
            {
               this->ModalOffsets()[i_op_mode][iact] = n_modal_coef + iact*nbas;
            }

            // Add to number of modal coefs
            n_modal_coef += nbas * nact;
         }

         // Set total number of modal params
         this->mNModalParams = n_modal_coef;
      }


      //! Use active-space projector that works for non-orthogonal active space
      bool NonOrthoActiveSpaceProjector
         (
         )  const
      {
         return mNonOrthoActiveSpaceProjector;
      }

      //! Do projection after transforming with inverse density matrix
      bool ProjectAfterInverseDensity
         (
         )  const
      {
         return mProjectAfterInverseDensity;
      }

      //! Get element of density matrix (col-major pointer)
      param_t DensityMatrixElement
         (  LocalModeNr aMode
         ,  In aRow
         ,  In aCol
         )  const
      {
         return this->mDensityMatrices[aMode][aRow + this->NAct(aMode)*aCol];
      }

      //! Get data on density matrices: off-diag norms, act-occ norms, max eigvals, min eigvals
      std::vector<std::vector<real_t> > DensityMatrixAnalysis
         (  const vec_t& aY
         )  const
      {
         LOGCALL("calls");

         std::vector<std::vector<real_t> > result(this->NDof());
         for(auto& v : result)
         {
            v.resize(4);
         }

         for(In imode=I_0; imode<this->NDof(); ++imode)
         {
            auto dens_i = this->ConstructDensityMatrix(imode, aY);
            auto dim_i = this->NAct(imode);

            // Calculate off-diag norms
            auto offdiag_norm2 = real_t(0.0);
            auto actocc_norm2 = real_t(0.0);
            for(In i=I_0; i<dim_i; ++i)
            {
               if (  i > I_0
                  )
               {
                  actocc_norm2 += midas::util::AbsVal2(dens_i[i]);
               }

               for(In j=i+I_1; j<dim_i; ++j)
               {
                  offdiag_norm2 += midas::util::AbsVal2(dens_i[i + dim_i*j]);
               }
            }

            result[imode][0] = std::sqrt(C_2*offdiag_norm2);
            result[imode][1] = std::sqrt(C_2*actocc_norm2);

            // Diagonalize to get eigvals
            auto eig = HEEVD(dens_i.get(), dim_i, 'N');
            result[imode][2] = eig._eigenvalues[dim_i - 1];
            result[imode][3] = eig._eigenvalues[0];
         }

         return result;
      }

      //! Calculate electronic state populations
      std::vector<real_t> ElectronicStatePopulations
         (  const vec_t& aY
         )  const
      {
         auto dim = this->NElectronicStates();
         std::vector<real_t> result(dim, 0.0);

         auto dens = this->ConstructDensityMatrix(this->H0OpDef().GetLocalModeNr(this->ElectronicDofNr()), aY);
         auto eig = HEEVD(dens.get(), dim, 'V');

         // Get eigenvectors in time-dependent basis, i.e. initially populated state will be first basis function (at least if starting from VSCF/VCI with targeting on electronic DOF)
         mat_t eigvecs(dim,dim);
         LoadEigenvectors(eig, eigvecs);

//         // DEBUG
//         Mout  << " ElectronicStatePopulations:\n"
//               << "    eigvecs:\n" << eigvecs << "\n"
//               << std::flush;

         for(In icol=I_0; icol<dim; ++icol)
         {
            // Find the state index
            auto maxval = std::abs(eigvecs[I_0][icol]);
            In maxelem = I_0;
            for(In irow=I_1; irow<dim; ++irow)
            {
               if (  std::abs(eigvecs[irow][icol]) > maxval
                  )
               {
                  maxelem = irow;
                  maxval = std::abs(eigvecs[irow][icol]);
               }
            }

            // Check that this value has not been set before.
            MidasAssert(!result[maxelem], "The same state cannot have two populations. Something is wrong!");

            result[maxelem] = eig._eigenvalues[icol];

//            // DEBUG
//            Mout  << " Assigned eigenvalue: " << eig._eigenvalues[icol] << " to state " << maxelem << std::endl;
         }

         return result;
      }

      /**
       * Return density matrix as a vector of midas matrices
       * 
       * @param aY      The wf vector
       **/
      std::vector<mat_t> GetDensityMatricesVecMat
         ( const vec_t& aY
         ) const
      {
         // Check if density matrices are constructed
         std::vector<mat_t> densmats;
         
         #pragma omp parallel for
         for (In i_mode = I_0; i_mode < this->NDof(); ++i_mode)
         {
            auto i_dens_ptr = this->ConstructDensityMatrix(i_mode, aY);
            auto i_dim = this->NAct(i_mode);
            mat_t densmat(i_dim, i_dim);

            for (In i = I_0; i < i_dim; ++i)
            {
               for (In j = I_0; j < i_dim; ++j)
               {
                  densmat[i][j] = i_dens_ptr[i + j*i_dim];
               }
            }

            densmats.emplace_back(densmat);
         }
         return densmats;
      }
};

} /*namespace midas::mctdh */

#endif /* LINEARMCTDHEOMBASE_H_INCLUDED */
