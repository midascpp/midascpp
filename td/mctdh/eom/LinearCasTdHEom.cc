/*
************************************************************************
*
* @file                 LinearCasTdHEom.cc
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Transformer for calculating derivatives in "standard"
*                       double-linear MCTDH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "LinearCasTdHEom.h"
#include "input/McTdHCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "lapack_interface/GELSS.h"
#include "lapack_interface/POSV.h"
#include "mmv/mmv_omp_pragmas.h"
#include "mctdh_tensor_utils.h"

#include "util/CallStatisticsHandler.h"

namespace midas::mctdh
{

//! Constructor
LinearCasTdHEom::LinearCasTdHEom
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  LinearMcTdHEomBase
         <  std::complex<Nb>        // param_t
         ,  CasTdHVectorContainer   // vec_t
         ,  LinearMcTdHIntegrals    // integrals_t
         >
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
{
   mIntermediates.reserve(this->Integrals().size());
   for(const auto& ints : this->Integrals())
   {
      mIntermediates.emplace_back(apCalcDef, ints);
   }
}


/**
 * Derivative of configuration-space coefficients
 *
 * @param aT
 * @param aY
 *
 * @return
 *    Derivative
 **/
typename LinearCasTdHEom::coefs_t LinearCasTdHEom::CoefficientDerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   )
{
   LOGCALL("calls");

   // DEBUG
   if (  this->DoIo(I_10)
      )
   {
      Mout  << " CoefficientDerivativeImpl at t = " << aT << std::endl;
   }

   // Get i*dot{C} from intermediates
   auto result = this->mIntermediates[0].GetICDot();
   for(In i=I_1; i<this->mIntermediates.size(); ++i)
   {
      result += this->mIntermediates[i].GetICDot();
   }

   // Calculate and save energy. It is much too expensive to do the forward contractions again.
   auto wf_norm2 = this->WaveFunctionNorm2();
   this->mSavedEnergy = std::make_pair(aT, dot_product(aY.Coefs(), result)/wf_norm2);

   if (  this->ImagTime()
      )
   {
      // Subtract E*C to preserve normalization
      result.Axpy(aY.Coefs(), -this->mSavedEnergy.second);

      // Scale by -1
      result.Scale(CC_M_1);
   }
   else
   {
      // Scale by -i
      result.Scale(CC_M_I);
   }

   // Return
   return result;
}

/**
 * Construct density matrix as column-major pointer
 *
 * @param aMode
 * @param aY
 *
 * @return
 *    Column-major pointer
 **/
typename LinearCasTdHEom::DataPtr LinearCasTdHEom::ConstructDensityMatrix
   (  LocalModeNr aMode
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");
   auto nact = this->NAct(aMode);

   coefs_t trans_coefs;

   // Get right coefs
   const auto& right_coefs = aY.Coefs();

   // Perform general multi-index contraction
   const auto& coefs = aY.Coefs();
   auto densmat_ptr = std::make_unique<param_t[]>(nact*nact);
   {
      LOGCALL("tensor contraction and data load-in");
      // Do contraction. NB: This results in a SimpleTensor with row-major data pointer.
      auto densmat_tensor = detail::ContractAllButOne(coefs, right_coefs, aMode);

      // DEBUG
      if (  this->DoIo(I_10)
         )
      {
         Mout  << " Density matrix for mode " << aMode << ":\n" << densmat_tensor << std::endl;
      }

      // Load into column-major pointer
      auto* ptr = densmat_ptr.get();
      auto* data_ptr = static_cast<SimpleTensor<param_t>*>(densmat_tensor.GetTensor())->GetData();
      auto* data_ptr_ref = data_ptr;
      for(In j=I_0; j<nact; ++j)
      {
         data_ptr = data_ptr_ref + j;
         for(In i=I_0; i<nact; ++i)
         {
            *(ptr++) = *data_ptr;
            data_ptr += nact;
         }
      }
   }

   // return result
   return densmat_ptr;
}

/**
 * Calculate derivative. We have only implemented g=0.
 *
 * @param aT
 * @param aY
 *
 * @return
 *    Total derivative
 **/
typename LinearCasTdHEom::vec_t LinearCasTdHEom::DerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   )
{
   LOGCALL("calls");

   // Update integrals, density matrices, and WF norm2.
   // If exact propagation, the integrals are updated once at the beginning.
   if (  this->Exact()
      )
   {
      this->mWfNorm2 = aY.Coefs().Norm2();
   }
   else
   {
      this->UpdateIntegrals(aT, aY);
      this->PreCalculateDensityMatrices(aY);
      this->UpdateNorm2(aY);
   }

   // Update intermediates: Calculate mean-fields and i*dC/dt
   for(auto& intermeds : this->mIntermediates)
   {
      intermeds.Update(aT, aY.Coefs());
   }

   // Init result
   auto result = this->ShapedZeroVector();

   // Calculate configuration-space derivative.
   result.Coefs() = this->CoefficientDerivativeImpl(aT, aY);

   // Calculate modal derivative
   if (  !this->Exact()
      )
   {
      result.Modals() = this->ModalDerivativeImpl(aT, aY);
   }

   // DEBUG
   if (  this->DoIo(I_10)
      )
   {
      Mout  << " Result of derivative calculation:\n" << result << std::endl;
   }

   // Return result
   return result;
}

/**
 * @return
 *    Type
 **/
std::string LinearCasTdHEom::TypeImpl
   (
   )  const
{
   return std::string("MCTDH");
}

/**
 * Construct zero vector
 *
 * @return
 *    Zero vector of correct shape
 **/
typename LinearCasTdHEom::vec_t LinearCasTdHEom::ShapedZeroVectorImpl
   (
   )  const
{
   return vec_t(this->NModalsVec(), this->mNModalParams);
}

/**
 * Calculate auto-correlation function.
 * This is done by taking the dot product between the initial coef tensor and 
 * the time-evoled one transformed by the overlaps between the initial and evolved modals.
 *
 * @param aT
 * @param aY
 *
 * @return
 *    S(t) = <y(0)|y(t)>
 **/
typename LinearCasTdHEom::param_t LinearCasTdHEom::CalculateAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   // Initialize tensor to transform
   auto right = aY.Coefs();

   // Perform forward contractions with modal overlaps for all modes
   for(In imode=I_0; imode<this->NDof(); ++imode)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(imode) == this->ElectronicDofNr()
         )
      {
         continue;
      }
      auto nact = this->NAct(imode);
      auto nbas = this->NBas(imode);
      auto off = this->ModalOffsets()[imode];

      // Construct overlap matrix (as SimpleTensor)
      auto overlap_ptr = std::make_unique<param_t[]>(nact*nact);

      // Calculate overlaps
      auto* ptr = overlap_ptr.get();
      for(In v=I_0; v<nact; ++v)
      {
         for(In w=I_0; w<nact; ++w)
         {
            param_t val(0.);
            for(In ibas=I_0; ibas<nbas; ++ibas)
            {
               val += midas::math::Conj(this->InitialWaveFunction().Modals()[off[v]+ibas]) * aY.Modals()[off[w]+ibas];
            }
            *(ptr++) = val;
         }
      }

      // Do forward contraction
      detail::DoForwardContraction(right, overlap_ptr, imode);
   } 

   // Return dot
   return dot_product(this->InitialWaveFunction().Coefs(), right);
}

/**
 * Calculate half-time auto-correlation function
 *
 * @param aT
 * @param aY
 *
 * @return
 *    S(2t) = <y*(t)|y(t)>
 **/
typename LinearCasTdHEom::param_t LinearCasTdHEom::CalculateHalfTimeAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   // Initialize tensor to transform
   auto right = aY.Coefs();

   // Perform forward contractions with modal overlaps for all modes
   for(In imode=I_0; imode<this->NDof(); ++imode)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(imode) == this->ElectronicDofNr()
         )
      {
         continue;
      }
      auto nact = this->NAct(imode);
      auto nbas = this->NBas(imode);
      auto off = this->ModalOffsets()[imode];

      // Construct overlap matrix (as SimpleTensor)
      auto overlap_ptr = std::make_unique<param_t[]>(nact*nact);

      // Calculate strange non-conjugated overlaps
      auto* ptr = overlap_ptr.get();
      for(In v=I_0; v<nact; ++v)
      {
         for(In w=I_0; w<nact; ++w)
         {
            param_t val(0.);
            for(In ibas=I_0; ibas<nbas; ++ibas)
            {
               val += aY.Modals()[off[v]+ibas] * aY.Modals()[off[w]+ibas];
            }
            *(ptr++) = val;
         }
      }

      // Do forward contraction
      detail::DoForwardContraction(right, overlap_ptr, imode);
   } 

   return detail::NonConjugateDot(aY.Coefs(), right);
}

/**
 * Calculate expectation value
 *
 * @param aIntegrals       Integrals of operator to calculate expectation value for. Assumed updated already!
 * @param aY               Wave function
 *
 * @return
 *    Expectation values. First element is "full" value. Subsequent values only include active terms for each mode.
 **/
std::vector<typename LinearCasTdHEom::param_t> LinearCasTdHEom::ExpectationValueImpl
   (  const integrals_t& aIntegrals
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   // Init intermeds
   intermeds_t intermeds(this->CalcDef(), aIntegrals);

   // Init result
   GeneralMidasVector<param_t> tmp_result(this->NDof() + I_1);

   // Loop over terms in the SOP operator
   const auto& opdef = aIntegrals.Oper().Oper();
   In nterms = opdef.Nterms();

   #pragma omp parallel shared(intermeds)
   {
      // Init temporary
      coefs_t tmp;
      param_t fac(0.);

      #pragma omp for schedule(dynamic) reduction(ComplexMidasVector_Add : tmp_result)
      for(In iterm = I_0; iterm<nterms; ++iterm)
      {
         // Get the forward-contracted tensor, or calculate it if not stored
         // ####################################################################################
         // NB: Remember to check that Intermediates are thread safe!
         // ####################################################################################
         tmp = intermeds.GetForwardContractedTensor(aY.Coefs(), iterm);

         // Result for this term
         fac = opdef.Coef(iterm) * dot_product(aY.Coefs(), tmp);

         // Add to result
         tmp_result[0] += fac;

         // Active-term results
         for(LocalModeNr idof=I_0; idof<this->NDof(); ++idof)
         {
            if (  opdef.IsModeActiveInTerm(idof, iterm)
               )
            {
               tmp_result[idof+1] += fac;
            }
         }
      }
   }

   // Move data to std::vector
   std::vector<param_t> result(tmp_result.Size());
   for(In i=I_0; i<tmp_result.Size(); ++i)
   {
      result[i] = std::move(tmp_result[i]);
   }

   // Divide by norm2
   auto wf_norm2 = this->WaveFunctionNorm2();
   for(auto& exptval : result)
   {
      exptval /= wf_norm2;
   }
   
   // Return
   return result;
}

/**
 * Calculate coefficient of reference Hartree product, i.e. [0,0,...,0]
 *
 * @param aY
 * @return
 *    C0
 **/
typename LinearCasTdHEom::param_t LinearCasTdHEom::ReferenceCoefficientImpl
   (  const vec_t& aY
   )  const
{
   return static_cast<SimpleTensor<param_t>*>(aY.Coefs().GetTensor())->GetData()[0];
}

/**
 * Apply operator to wave packet. Normalize and save norm.
 *
 * @param aIntegrals    Integrals of operator
 * @param arY           The wave packet
 * @param arNorm2       The norm square
 * @return
 *    True if the operator was applied successfully
 **/
bool LinearCasTdHEom::ApplyOperatorImpl
   (  const integrals_t& aIntegrals
   ,  vec_t& arY
   ,  real_t& arNorm2
   )
{
   // Init intermeds
   intermeds_t intermeds(this->CalcDef(), aIntegrals);

   // Init result
   coefs_t trans_coefs(arY.Coefs().GetDims(), BaseTensor<param_t>::typeID::SIMPLE); 

   // Loop over terms in the SOP operator
   const auto& opdef = aIntegrals.Oper().Oper();
   In nterms = opdef.Nterms();


   #pragma omp parallel for shared(intermeds) schedule(dynamic) reduction(ComplexNiceTensor_Add : trans_coefs)
   for(In iterm = I_0; iterm<nterms; ++iterm)
   {
      // Get the forward-contracted tensor, or calculate it if not stored
      // ####################################################################################
      // NB: Remember to check that Intermediates are thread safe!
      // ####################################################################################
      trans_coefs.Axpy(intermeds.GetForwardContractedTensor(arY.Coefs(), iterm), opdef.Coef(iterm));
   }

   // Calculate norm
   arNorm2 = trans_coefs.Norm2();

   // Normalize
   trans_coefs.Scale(1./std::sqrt(arNorm2));

   // Set coefs to trans_coefs
   arY.Coefs() = std::move(trans_coefs);

   // Return
   return true;
}

/**
 * Add constraint to modal derivative
 *
 * @param aMode
 * @param aModals
 * @param arDyDt
 **/
void LinearCasTdHEom::AddNonProjectedModalDerivativeTerms
   (  LocalModeNr aMode
   ,  const modal_t& aModals
   ,  DataPtr& arDyDt
   )  const
{
   // Not implemented. We only use g=0 for full MCTDH.
}

/**
 * Get H matrix
 *
 * @param aOper
 * @param aTerm
 * @param aMode
 * @param aFactor    Factor in aTerm that operators on aMode
 * @return
 *    H (mean-field) matrix
 **/
const typename LinearCasTdHEom::h_mat_t& LinearCasTdHEom::GetHMatrix
   (  In aOper
   ,  In aTerm
   ,  In aMode
   ,  In aFactor
   )  const
{
   return this->mIntermediates[aOper].GetHMatrix(aTerm, aMode);
}

/**
 * Calculate norm2 of wave function
 *
 * @param aY      WF vector
 * @return
 *    norm2
 **/
typename LinearCasTdHEom::real_t LinearCasTdHEom::CalculateWfNorm2Impl
   (  const vec_t& aY
   )  const
{
   // Initialize tensor to transform
   auto right = aY.Coefs();

   // Perform forward contractions with modal overlaps for all modes
   for(In imode=I_0; imode<this->NDof(); ++imode)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(imode) == this->ElectronicDofNr()
         )
      {
         continue;
      }
      auto nact = this->NAct(imode);
      auto nbas = this->NBas(imode);
      auto off = this->ModalOffsets()[imode];

      // Do forward contraction
      detail::DoForwardContraction(right, this->H0Integrals().GetActiveOverlaps(imode), imode);
   } 

   // Return dot
   auto dot = dot_product(aY.Coefs(), right);

   if (  !libmda::numeric::float_numeq_zero(std::imag(dot), std::real(dot), 10)
      )
   {
      MidasWarning("MCTDH: Norm2 attained a non-vanishing imaginary part.");
   }

   return std::real(dot);
}

} /* namespace midas::mctdh */
