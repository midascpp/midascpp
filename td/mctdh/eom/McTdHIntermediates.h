/*
************************************************************************
*
* @file                 McTdHIntermediates.h
*
* Created:              21-11-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Intermediates for MCTDH mean-field construction and 
*                       coefficient transform.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHINTERMEDIATES_H_INCLUDED
#define MCTDHINTERMEDIATES_H_INCLUDED

#include <unordered_map>
#include <vector>

#include "input/Input.h"
#include "util/CallStatisticsHandler.h"
#include "lapack_interface/POSV.h"
#include "mctdh_tensor_utils.h"
#include "tensor/NiceTensor.h"

namespace midas::mctdh
{
namespace detail
{

/**
 *
 **/
template
   <  typename INTEGRALS
   >
class CasTdHIntermediates
{
   public:
      //! aliases
      using integrals_t = INTEGRALS;
      using param_t = typename integrals_t::param_t;
      using step_t = midas::type_traits::RealTypeT<param_t>;
      using mat_t = typename integrals_t::mat_t;
      using tensor_t = NiceTensor<param_t>;


   private:
      using ref_t = std::set<std::pair<In, In>>;

      //! Hasher (inspired by boost::hash_combine)
      struct ref_hasher
      {
         using result_t = std::size_t;

         result_t operator()
            (  const ref_t& aRef
            )  const noexcept
         {
            result_t result = 0;

            for(const auto& mode_oper_pair : aRef)
            {
               result ^= mode_oper_pair.first  + 0x9e3779b9 + (result << 6) + (result >> 2);
               result ^= mode_oper_pair.second + 0x9e3779b9 + (result << 6) + (result >> 2);
            }

            return result;
         }
      };

      //! Const reference to integrals for the given operator
      const integrals_t& mIntegrals;

      //! Are we doing exact propagation
      bool mExact = false;

      //! Assume fixed modals for electronic DOF
      bool mAssumeFixedElectronicDofModals = true;

      //! IoLevel
      In mIoLevel = I_0;

      //! Unordered map of H intermeds
      std::unordered_map<ref_t, tensor_t, ref_hasher> mHIntermeds;

      //! Coef derivative (times i)
      tensor_t mICDot;

      //! Clear data
      void Clear
         (
         )
      {
         mHIntermeds.clear();
         mICDot.Clear();
      }

      /**
       * Construct unique reference key for given term and active mode.
       *
       * @param aOperTerm           Term number in operator
       * @param aActive             The factor or mode that operates on the active mode in the mean-field matrix.
       * @param aActiveMode         If true, aActive is a mode number, else it is a factor number.
       * @return
       *    A unique number that only depends on the operators and modes of the "inactive" factors, i.e. the ones 
       *    where forward contractions are performed in the mean-field-matrix calculation.
       **/
      ref_t HMatrixRef
         (  In aOperTerm
         ,  In aActive
         ,  bool aActiveMode
         )  const
      {
         LOGCALL("ref keys");

         ref_t result;

         const auto& opdef = this->mIntegrals.Oper().Oper();

         In nfac = opdef.NfactorsInTerm(aOperTerm);
         for(In ifac=I_0; ifac<nfac; ++ifac)
         {
            In mode = opdef.ModeForFactor(aOperTerm, ifac);

            bool active = (aActiveMode && (mode == aActive)) || (!aActiveMode && (ifac == aActive));

            // Get operator index. If active mode/factor, we emplace nopers (such that the key becomes independent of this operator)
            In oper;
            if (  active
               )
            {
               oper = I_0;   // We pull active term to the front of the set
            }
            else
            {
               oper = opdef.OperForFactor(aOperTerm, ifac);
               ++oper;  // Increment to start indexing from 1
            }

            result.emplace(std::make_pair(oper, mode));
         }

         return result;
      }

//      /**
//       * Calculate unique reference number for given term and active mode.
//       * Niels: This could perhaps be done in a smarter way resulting in smaller ref numbers, 
//       * but it works for now.
//       *
//       * param aOperTerm           Term number in operator
//       * param aActive             The factor or mode that operates on the active mode in the mean-field matrix.
//       * param aActiveMode         If true, aActive is a mode number, else it is a factor number.
//       * return
//       *    A unique number that only depends on the operators and modes of the "inactive" factors, i.e. the ones 
//       *    where forward contractions are performed in the mean-field-matrix calculation.
//       **/
//      refnum_t RefNumber
//         (  In aOperTerm
//         ,  In aActive
//         ,  bool aActiveMode
//         )  const
//      {
//         LOGCALL("ref numbers");
//
//         refnum_t result = 0;
//
//         const auto* const opdef = this->mIntegrals.GetOpDef();
//         In nmodes = opdef->NmodesInOp();
//
//         In nfac = opdef->NfactorsInTerm(aOperTerm);
//         for(In ifac=I_0; ifac<nfac; ++ifac)
//         {
//            In mode = opdef->ModeForFactor(aOperTerm, ifac);
//            result *= (nmodes + I_1);
//            result += mode + I_1;   // start modes from 1 to be able to distinguish different coupling levels
//
//            In oper = opdef->OperForFactor(aOperTerm, ifac);
//            In nopers = opdef->NrOneModeOpers(mode);
//
//            result *= (nopers + I_1);
//
//            // Oper index only taken into account for inactive factors
//            if (  (  aActiveMode
//                  && mode == aActive
//                  )
//               || (  !aActiveMode
//                  && ifac == aActive
//                  )
//               )
//            {
//               result += nopers;
//            }
//            else
//            {
//               result += oper;
//            }
//         }
//
//         // DEBUG
//         if (  gDebug
//            || this->mIoLevel >= I_12
//            )
//         {
//            std::string type = aActiveMode ? "MODE" : "FACTOR";
//            Mout  << " REFNUM: TERM '" << aOperTerm << "' ACTIVE " << type << " '" << aActive << "' = " << result << std::endl; 
//         }
//
//         return result;
//      }



   public:
      //! c-tor from calcdef
      CasTdHIntermediates
         (  const input::McTdHCalcDef* const apCalcDef
         ,  const integrals_t& aIntegrals
         )
         :  mIntegrals
               (  aIntegrals
               )
         ,  mExact
               (  apCalcDef->GetExact()
               )
         ,  mAssumeFixedElectronicDofModals
               (  !apCalcDef->GetPropagateElectronicDofModals()
               )
         ,  mIoLevel
               (  apCalcDef->GetIoLevel()
               )
      {
      }

      //! Update intermediates
      void Update
         (  step_t aT
         ,  const tensor_t& aTensor
         ,  bool aOnlyCDot = false
         )
      {
         LOGCALL("calls");
         this->Clear();

         const auto& operator_i = this->mIntegrals.Oper();
         const auto& opdef = operator_i.Oper();
         auto nterms = opdef.Nterms();
         auto e_nr = gOperatorDefs.ElectronicDofNr();

         tensor_t icdot(aTensor.GetDims(), BaseTensor<param_t>::typeID::SIMPLE);

         #pragma omp parallel 
         {
            In nfac;
            LocalModeNr mode;
            LocalModeNr aux_mode;
            LocalOperNr oper;
            LocalOperNr aux_oper;
            tensor_t tensor;
            tensor_t aux_tensor;
            ref_t key;
            bool already_calculated = false;

            // Loop over terms
            #pragma omp for schedule(dynamic) reduction(ComplexNiceTensor_Add : icdot)
            for(In iterm=I_0; iterm<nterms; ++iterm)
            {
               LOGCALL("term loop");

               // Copy aTensor to tensor
               tensor = aTensor;

               // Loop over factors. Let the mode of ifac correspond to the active mode
               nfac = opdef.NfactorsInTerm(iterm);
               for(In ifac=I_0; ifac<nfac; ++ifac)
               {
                  LOGCALL("factors");

                  // Get mode and oper for active index
                  mode = opdef.ModeForFactor(iterm, ifac);
                  oper = opdef.OperForFactor(iterm, ifac);

                  if (  !this->mExact
                     && !aOnlyCDot
                     && nfac > I_1     // Skip calculation of H matrix for one-mode term (= density matrix)
                     && !( this->mAssumeFixedElectronicDofModals
                        && e_nr == opdef.GetGlobalModeNr(mode)
                        )
                     )
                  {
                     // Copy tensor to aux_tensor
                     aux_tensor = tensor;

                     // Do forward contractions after active index on aux_tensor
                     // After this, aux_tensor is ready for calculating a mean-field matrix
                     for(In jfac=ifac+I_1; jfac<nfac; ++jfac)
                     {
                        LOGCALL("factors after active mode");
                        aux_mode = opdef.ModeForFactor(iterm, jfac);
                        aux_oper = opdef.OperForFactor(iterm, jfac);
                        const auto& ints = this->mIntegrals.GetActiveIntegrals(aux_mode, aux_oper);
                        detail::DoForwardContraction(aux_tensor, ints, aux_mode);
                     }

                     // Calculate reference key
                     key = std::move(this->HMatrixRef(iterm, ifac, false));

                     // Check if this thread needs to construct a mean-field matrix
                     // If it does, insert an empty tensor such that the other threads will know that 
                     // it is taken care of.
                     #pragma omp critical
                     {
                        LOGCALL("check if already calc");
                        already_calculated = (this->mHIntermeds.find(key) != this->mHIntermeds.end());
                        if (  !already_calculated
                           )
                        {
                           this->mHIntermeds[key] = tensor_t();
                        }
                        else if  (  this->mIoLevel >= I_10
                                 || gDebug
                                 )
                        {
                           Mout  << " H matrix for term " << iterm << " mode " << mode << " already calculated." << std::endl;
                        }
                     }

                     // Save result obtained from aux_tensor
                     if (  !already_calculated
                        )
                     {
                        // Only one thread can write
                        #pragma omp critical
                        {
                           LOGCALL("calc mean field");
                           this->mHIntermeds[key] = detail::ContractAllButOne(aTensor, aux_tensor, mode);
                        }
                     }
                  }

                  // Do forward contraction before proceeding to next active index
                  // After all factors, tensor has been contracted in all modes
                  const auto& ints = this->mIntegrals.GetActiveIntegrals(mode, oper);
                  detail::DoForwardContraction(tensor, ints, mode);
               }

               // Add tensor to mICDot
               //if ( zero constraint || nfac > 1 )
               icdot.Axpy(tensor, operator_i.Coef(aT) * opdef.Coef(iterm));
            }
         }

         // Set ICDot
         this->mICDot = std::move(icdot);
      }

      /**
       * Calculate forward-contracted tensor. If not available, calculate it.
       *
       * @param aTensor
       * @param aOperTerm
       * @param aExcludeMode        Do not do any forward contractions on this mode. Not even overlap!
       * @return
       *    aTensor forward contracted with all operators in aOperTerm and maybe overlap integrals in the remaining modes.
       **/
      tensor_t GetForwardContractedTensor
         (  const tensor_t& aTensor
         ,  In aOperTerm
         ,  In aExcludeMode = -I_1
         )  const
      {
         LOGCALL("calls");

         tensor_t result = aTensor;

         const auto& opdef = this->mIntegrals.Oper().Oper();
         const auto nfac = opdef.NfactorsInTerm(aOperTerm);

         // Set of modes not included in the operator term
         std::set<In> inactive_modes;

         // Loop over factors (one-mode operators)
         {
            LOGCALL("active transforms");
            for(In ifac=I_0; ifac<nfac; ++ifac)
            {
               // Get contraction mode
               auto mode = opdef.ModeForFactor(aOperTerm, ifac);

               // Check if we exclude this mode from contraction
               if (  mode == aExcludeMode
                  )
               {
                  continue;
               }
               // Get operator index
               auto oper = opdef.OperForFactor(aOperTerm, ifac);

               // Get integrals
               const auto& integrals_mat = this->mIntegrals.GetActiveIntegrals(mode, oper);

               // Do forward contraction
               detail::DoForwardContraction(result, integrals_mat, mode);
            }
         }

         // Return result
         return result;
      }

      /**
       * Get mean-field matrix as SimpleTensor
       *
       * @param aOperTerm
       * @param aMode               The active mode
       * @return
       *    Mean-field matrix as NiceTensor (i.e. row-major matrix)
       **/
      const tensor_t& GetHMatrix
         (  In aOperTerm
         ,  In aMode
         )  const
      {
         LOGCALL("calls");

         // Calculate reference number
         auto key = this->HMatrixRef(aOperTerm, aMode, true);

         return this->mHIntermeds.at(key);
      }

      //! Get ICDot intermed
      const tensor_t& GetICDot
         (
         )  const
      {
         return this->mICDot;
      }
};

}  /* namespace detail */
}  /* namespace midas::mctdh */



#include "McTdHEvalData.h"
#include "vcc/v3/IntermediateMachine.h"
#include "td/mctdh/integrals/McTdHIntegrals.h"

namespace midas::mctdh
{

/**
 * Class of intermediates for RASTDH (or MCTDH[n]) calculations
 *
 * @note
 *    In order to use this with exponential paramterization, we could make it a template with INTEGRALS as parameter.
 *    But this requires modification of the IntermediateMachine class (since the DATA template template parameter only 
 *    takes one template argument).
 **/
class LinearRasTdHIntermediates
   :  public midas::vcc::v3::IntermediateMachine
               <  std::complex<Nb>
               ,  NiceTensor
               ,  LinearRasTdHEvalData
               >  
{
   public:
      //! Alias
      using Base  =  midas::vcc::v3::IntermediateMachine
                        <  std::complex<Nb>
                        ,  NiceTensor
                        ,  LinearRasTdHEvalData
                        >;
      using integrals_t = LinearRasTdHIntegrals;
      using param_t = typename integrals_t::param_t;
      using mat_t = typename integrals_t::mat_t;
      using vec_t = RasTdHVectorContainer<param_t>;
      using coefs_t = typename vec_t::coefs_t;
      using modal_t = typename vec_t::modal_t;
      using mfintermeds_t = std::vector<std::vector<NiceTensor<param_t> > >;


   private:
      //! Mean-field intermediates. Access as [oper][term][active_mode]
      mfintermeds_t mMeanFieldIntermeds;

   public:
      //! Reset overload
      void Reset
         (
         )  override
      {
         Base::Reset();

         this->mMeanFieldIntermeds.clear();
      }

      //! ClearIntermeds overload
      void ClearIntermeds
         (  bool aKeepGlobal = false
         )  override
      {
         Base::ClearIntermeds(aKeepGlobal);

         this->mMeanFieldIntermeds.clear();
      }

      //! Get mean-field intermediates
      mfintermeds_t& GetMfIntermediates
         (
         )
      {
         return this->mMeanFieldIntermeds;
      }

      //! Get mean-field intermediates
      const mfintermeds_t& GetMfIntermediates
         (
         )  const
      {
         return this->mMeanFieldIntermeds;
      }
};

using LinearCasTdHIntermediates = detail::CasTdHIntermediates<LinearMcTdHIntegrals>;

} /* namespace midas::mctdh */

#endif /* MCTDHINTERMEDIATES_H_INCLUDED */
