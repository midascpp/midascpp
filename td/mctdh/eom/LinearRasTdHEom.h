/*
************************************************************************
*
* @file                 LinearRasTdHEom.h
*
* Created:              13-03-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Transformer for calculating derivatives in
*                       double-linear MCTDH[n]
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef LINEARRASTDHEOM_H_INCLUDED
#define LINEARRASTDHEOM_H_INCLUDED

#include "util/type_traits/Complex.h"
#include "td/mctdh/integrals/McTdHIntegrals.h"
#include "LinearMcTdHEomBase.h"
#include "McTdHIntermediates.h"
#include "McTdHEvalData.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/IntermedProd.h"

namespace midas::mctdh
{
//! Forward decl.
class LinearRasTdHEom;

template
   <  typename PARAM_T
   ,  template<typename> typename COEFS_T
   ,  template<typename> typename MODAL_T
   >
class McTdHVectorContainer;
} /* namespace midas::mctdh */

template
   <  typename T
   >
using RasTdHVectorContainer = midas::mctdh::McTdHVectorContainer<T, GeneralTensorDataCont, GeneralMidasVector>;

namespace midas::mctdh
{
namespace detail
{

/**
 * Class for performing VCI-like transformations in MCTDH[n]
 **/
class RasTdHTransformer
{
   public:
      //! V3 types
      using param_t = std::complex<Nb>;
      using step_t = Nb;
      using evaldata_t = LinearRasTdHEvalData<param_t>;
      using v3contrib_t = midas::vcc::v3::V3Contrib<param_t, LinearRasTdHEvalData>;
      using intermedprod_t = midas::vcc::v3::IntermedProd<param_t, LinearRasTdHEvalData>;
      using contribs_t = std::vector<std::unique_ptr<v3contrib_t> >;
      using intermeds_t = LinearRasTdHIntermediates;
      using vec_t = typename intermeds_t::vec_t;
      using coefs_t = typename intermeds_t::coefs_t;
      using modal_t = typename intermeds_t::modal_t;
      using integrals_t = typename intermeds_t::integrals_t;
      using mat_t = typename integrals_t::mat_t;
      using v3_key_t = std::pair<In, In>;

   private:
      //! V3 contributions for calculating the VCI transformation. The key is pair<max-exci, oper-mc-level>.
      std::map<v3_key_t, contribs_t> mContribs;

      //! Intermediates
      mutable std::vector<intermeds_t> mIntermediates;

      //! File prefix for .v3c files
      std::string mV3cFilePrefix = "";

      //! Evaluate contrib by contrib as opposed to MC by MC
      bool mEvaluateContribByContrib = false;

      //! Assume fixed modals for electronic DOF
      bool mAssumeFixedElectronicDofModals = true;

      //! IO level
      In mIoLevel = I_1;


      //! Evaluate contributions
      void EvaluateContribs
         (  const v3_key_t&
         ,  const evaldata_t&
         ,  coefs_t&
         ,  const VccStateSpace& aVssOut
         )  const;

   public:
      //! c-tor from calcdef
      RasTdHTransformer
         (  const input::McTdHCalcDef* const apCalcDef
         ,  In aNOpers
         );

      //! c-tor
      RasTdHTransformer
         (  const std::string& = ""
         ,  bool = true
         ,  In = I_1
         ,  In aNOpers = I_1
         );

      //! Initialize V3Contribs
      void InitializeV3
         (  In aMaxExci
         ,  In aOperMcLevel
         ,  In aOperNr
         );

      //! Get intermediates
      const intermeds_t& GetIntermediates
         (  In
         )  const;

      //! Do transformation
      void Transform
         (  step_t aT
         ,  const vec_t& aCoefs
         ,  coefs_t& arResult
         ,  const std::vector<integrals_t>& aIntegrals
         ,  const VccStateSpace& aVssI
         ,  const VccStateSpace& aVssOut
         ,  bool aMeanFields
         ,  bool aV3Trans
         );

      //! Do one-mode transformation
      void OneModeTransform
         (  const coefs_t& aCoefs
         ,  coefs_t& arResult
         ,  const std::vector<mat_t>& aIntegrals
         ,  const VccStateSpace& aVssI
         ,  bool aOnlyUpDown
         )  const;

};

}  /* namespace detail */

/**
 *
 **/
class LinearRasTdHEom
   :  public LinearMcTdHEomBase
               <  std::complex<Nb>        // param_t
               ,  RasTdHVectorContainer   // vec_t
               ,  LinearRasTdHIntegrals   // integrals_t
               >
{
   public:
      //! Types of MCRs
      enum class mcrType : int
      {  I = 0
      ,  X1
      ,  IpX1
      };

      /** @name Aliases **/
      //!@{
      
      //! Basic types inherited from base class
      using Base = LinearMcTdHEomBase<std::complex<Nb>, RasTdHVectorContainer, LinearRasTdHIntegrals>;
      using param_t = typename Base::param_t;
      using real_t = typename Base::real_t;
      using step_t = typename Base::step_t;
      using vec_t = typename Base::vec_t;
      using coefs_t = typename vec_t::coefs_t;
      using modal_t = typename vec_t::modal_t;
      using integrals_t = typename Base::integrals_t;
      using mat_t = typename Base::mat_t;
      using bvec_t = GeneralMidasVector<param_t>;
      using DataPtr = typename Base::DataPtr;
      using h_mat_t = typename Base::h_mat_t;
      using oper_t = typename Base::oper_t;

      //! V3 types
      using transformer_t = detail::RasTdHTransformer;
      using evaldata_t = typename transformer_t::evaldata_t;
      using v3contrib_t = typename transformer_t::v3contrib_t;
      using intermedprod_t = typename transformer_t::intermedprod_t;
      using contribs_t = typename transformer_t::contribs_t;
      using intermeds_t = LinearRasTdHIntermediates;

      //! Gauges
      using g_opt_t = typename input::McTdHCalcDef::GOptType;
      using gauge_t = typename input::McTdHCalcDef::GaugeType;
      using goptinfo_t = std::tuple<std::set<In>, std::map<In, real_t>, In, std::map<In, In> >;

      //!@}
      
   private:
      //! Transformer object for VCI-like transformation
      transformer_t mTransformer;

      //! State space of the included configurations
      VccStateSpace mVssI;

      //! State space of excluded configurations that are one-mode excited w.r.t. an included configuration
      VccStateSpace mVssX1;

      //! Direct sum of mVssI and mVssX1
      VccStateSpace mVssIpX1;

      //! Use non-orthogonal projector in the variational optimization of the g operators
      bool mGOptNonOrthoActiveSpaceProjector = true;

      //! Transform all g matrices by inverse overlaps to account for non-orthogonality
      bool mNonOrthoConstraintOperator = false;

      //! Type of constraint optimization
      g_opt_t mGOptType = g_opt_t::VARIATIONAL;

      //! Choice of gauge for redundant parameters
      gauge_t mGaugeType = gauge_t::ZERO;

      //! g matrices
      std::vector<mat_t> mGMatrices;

      //! Use V3 transform for coef derivative always
      bool mV3Trans = false;

      
      //! Perform coefficient derivative and calculate intermediates
      void CoefficientDerivativeImpl
         (  step_t
         ,  const vec_t&
         ,  coefs_t&
         );

      //! Update g operators
      void UpdateGIntegrals
         (  step_t
         ,  const coefs_t&
         );

      //! Solve g optimization equations
      DataPtr SolveGOptEquations
         (  const mat_t&
         ,  const bvec_t&
         )  const;

      //! Prepare optimization of g operators
      goptinfo_t PrepareGOptimization
         (  const coefs_t&
         ,  const coefs_t&
         )  const;

      //! Construct A matrix for variational optimization of g
      mat_t ConstructAMatrix
         (  const coefs_t&
         ,  const coefs_t&
         ,  const std::set<In>&
         ,  const std::map<In, real_t>&
         ,  In
         ,  const std::map<In, In>&
         )  const;

      //! Construct B vector for variational optimization of g
      modal_t ConstructBVector
         (  step_t
         ,  const coefs_t&
         ,  const std::set<In>&
         ,  In
         ,  const std::map<In, In>&
         )  const;

      //! Transform coefs_t with inverse overlap matrices
      void PerformInverseOverlapTransforms
         (  coefs_t&
         ,  const VccStateSpace&
         )  const;

      //! Get combinations of contractions that add up to an excitation
      const std::vector<std::vector<In>>& ExciContractionCombinations
         (  In aNFac
         )  const;

      //! Get combinations of contractions that shift excitation between modes
      const std::vector<std::vector<In>>& GenContractionCombinations
         (  In aNFac
         )  const;

      /** @name Method overloads **/
      //!@{

      //! Implementation of derivative
      vec_t DerivativeImpl
         (  step_t
         ,  const vec_t&
         )  override;

      //! Get H matrix
      const h_mat_t& GetHMatrix
         (  In aOper
         ,  In aTerm
         ,  In aMode
         ,  In aFactor
         )  const override;

      //! Construct density matrix as column-major pointer
      DataPtr ConstructDensityMatrix
         (  LocalModeNr
         ,  const vec_t&
         )  const override;

      //! Add constraint to modal derivative
      void AddNonProjectedModalDerivativeTerms
         (  LocalModeNr aMode
         ,  const modal_t& aModals
         ,  DataPtr& arDyDt
         )  const override;

      //! Calculate wave-function norm2
      real_t CalculateWfNorm2Impl
         (  const vec_t&
         )  const override;

      //! Return zero vector of correct shape.
      vec_t ShapedZeroVectorImpl
         (
         )  const override;

      //! Auto-correlation function
      param_t CalculateAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const override;

      //! Half-time auto-correlation function
      param_t CalculateHalfTimeAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const override;

      //! Expectation values. Full and active-terms.
      std::vector<param_t> ExpectationValueImpl
         (  const integrals_t& aIntegrals
         ,  const vec_t& aY
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Reference coefficient
      param_t ReferenceCoefficientImpl
         (  const vec_t&
         )  const override;

      //! Apply operator
      bool ApplyOperatorImpl
         (  const integrals_t&
         ,  vec_t&
         ,  real_t&
         )  override;

      //!@}
 
   public:
      //! Constructor from calcdefs
      explicit LinearRasTdHEom
         (  const input::McTdHCalcDef* const
         ,  const oper_t&
         ,  const BasDef* const
         );

      //! Get maximum excitation level
      In MaxExciLevel
         (
         )  const;

      //! V3Trans
      bool V3Trans
         (
         )  const;

      //! Construct ModeCombiOpRange
      static ModeCombiOpRange ConstructModeCombiOpRange
         (  const input::McTdHCalcDef* const
         ,  mcrType
         ,  const std::vector<unsigned>& = {}
         );
};
} /* namespace midas::mctdh */

#endif /* LINEARRASTDHEOM_H_INCLUDED */
