/*
************************************************************************
*
* @file                 LinearRasTdHEom.cc
*
* Created:              13-03-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Implementation of MCTDH[n]
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include <set>

#include "LinearRasTdHEom.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "vcc/v3/Scaling.h"
#include "vcc/VccStateModeCombi.h"

#include "mmv/mmv_omp_pragmas.h"

#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/HESV.h"
#include "lapack_interface/HEEV.h"
#include "lapack_interface/HEEVD.h"

#include "util/AbsVal.h"
#include "util/NumericLimits.h"

#include "td/mctdh/container/McTdHVectorContainer.h"


namespace midas::mctdh
{

namespace detail
{

/**
 * c-tor. Set prefix for .v3c files.
 *
 * @param apCalcDef
 * @param aNOpers
 *    Number of operators in the (time-dependent) Hamiltonian
 **/
RasTdHTransformer::RasTdHTransformer
   (  const input::McTdHCalcDef* const apCalcDef
   ,  In aNOpers
   )
   :  mIntermediates
         (  aNOpers
         )
   ,  mV3cFilePrefix
         (  apCalcDef->GetMethodInfo().template At<std::string>("V3CFILEPREFIX").empty()
         ?  input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/"
         :  apCalcDef->GetMethodInfo().template At<std::string>("V3CFILEPREFIX")
         )
   ,  mEvaluateContribByContrib
         (  apCalcDef->GetMethodInfo().template At<bool>("EVALUATECONTRIBBYCONTRIB")
         )
   ,  mAssumeFixedElectronicDofModals
         (  !apCalcDef->GetPropagateElectronicDofModals()
         )
   ,  mIoLevel
         (  apCalcDef->GetIoLevel()
         )
{
}

/**
 * c-tor. Set prefix for .v3c files.
 *
 * @param aPrefix
 * @param aCByC
 * @param aIoLevel
 * @param aNOpers
 *    Number of operators in the (time-dependent) Hamiltonian
 **/
RasTdHTransformer::RasTdHTransformer
   (  const std::string& aPrefix
   ,  bool aCByC
   ,  In aIoLevel
   ,  In aNOpers
   )
   :  mIntermediates
         (  aNOpers
         )
   ,  mV3cFilePrefix
         (  aPrefix
         )
   ,  mEvaluateContribByContrib
         (  aCByC
         )
   ,  mIoLevel
         (  aIoLevel
         )
{
}

/**
 * Read in .v3c file with terms for VCI transformation.
 * NB: Later we will perhaps need specialized files with mean fields, etc.
 *
 * @param aMaxExci
 * @param aOperMcLevel
 * @param aOperNr       Index of the operator in McTdHEomBase::Oper()
 **/
void RasTdHTransformer::InitializeV3
   (  In aMaxExci
   ,  In aOperMcLevel
   ,  In aOperNr
   )
{
   v3_key_t key = {aMaxExci, aOperMcLevel};
   const In& max_exci = key.first;
   const In& op_mc_level = key.second;

   if (  this->mContribs.find(key) != this->mContribs.end()
      )
   {
      return;
   }
   else
   {
      std::string method = "MCTDH[" + std::to_string(max_exci) + "]";

      // Initialize new contribs and get reference
      mContribs[key] = contribs_t();
      auto& contribs = this->mContribs.at(key);

      if (  this->mIoLevel >= I_7
         )
      {
         Mout  << " Initializing V3Contribs for " << method << " with " << op_mc_level << "M operator." << std::endl;
      }

      // Generate name of data file.
      std::ostringstream ss_v3c_file;
      ss_v3c_file << "VCI[" << max_exci << "]_H" << op_mc_level << ".v3c";
      //CheckMethod(ss_v3c_file.str());
      std::string v3c_file = this->mV3cFilePrefix + ss_v3c_file.str();
 
      // If not found, generate contributions from scratch
      if (  !v3contrib_t::ReadV3cFile(v3c_file, contribs)
         )
      {
         Mout << " " << method << ": Generating list of V3 contributions from scratch." << std::endl;

         // Generate list of contraction products.
         std::vector<BasicOperProd> bops;
         std::vector<CtrProd> ctr_prods;
         BasicOperProd::GenerateVci(op_mc_level, max_exci, bops);
         Mout << "    Basic operator products:" << std::endl;
         for (In i=I_0; i<bops.size(); ++i)
         {
            Mout << "       " << bops[i] << std::endl;
            bops[i].GetCtrProds(I_0, max_exci, ctr_prods);
         }

         // Append contractions products to mContribs
         for (In i=I_0; i<ctr_prods.size(); ++i)
         {
            std::ostringstream os;
            ctr_prods[i].WriteV3ContribSpec(os);
            contribs.emplace_back(v3contrib_t::Factory(os.str()));
         }
         
         // Write .v3c file
         intermedprod_t::WriteV3cFile(v3c_file, contribs);
      }

      // Identify combined intermediates
      bool cp_tensors = false;
      midas::vcc::v3::Scaling init_scaling, max_sc;
      for(auto& contrib : contribs)
      {
         if (  auto* ip = dynamic_cast<intermedprod_t*>(contrib.get())
            )
         {
            auto sc = ip->GetScaling(cp_tensors);
            if (  sc > init_scaling
               )
            {
               init_scaling = sc;
            }
            ip->IdentifyCmbIntermeds(sc, cp_tensors);
            if (  sc > max_sc
               )
            {
               max_sc = sc;
            }
         }
         else
         {
            MIDASERROR(method + ": V3 contributions should only contain IntermedProd.");
         }
      }

      if (  this->mIoLevel >= I_10
         )
      {
         Mout  << "    Max scaling before: " << init_scaling << "\n"
               << "    Max scaling after:  " << max_sc << " (including intermediates)." << "\n\n"
               << std::flush;
      }
      
      // Register intermediates
      this->mIntermediates[aOperNr].Reset();
      for(auto& contrib : contribs)
      {
         if (  auto* ip = dynamic_cast<intermedprod_t*>(contrib.get())
            )
         {
            ip->RegisterIntermeds(this->mIntermediates[aOperNr]);
//            ip->RegisterIntermeds(static_cast<midas::vcc::v3::IntermediateMachine<param_t, NiceTensor, LinearRasTdHEvalData>& >(this->mIntermediates[aOperNr]));
         }
      }
   }
}

/**
 * Get intermediates from transformer
 *
 * @param aOperNr
 *    Index of the operator in McTdHEomBase::Oper()
 *
 * @return
 *    const ref to intermediates
 **/
const typename RasTdHTransformer::intermeds_t& RasTdHTransformer::GetIntermediates
   (  In aOperNr
   )  const
{
   if (  aOperNr >= this->mIntermediates.size()
      )
   {
      MIDASERROR("aOperNr out of range!");
   }
   return this->mIntermediates[aOperNr];
}

/**
 * Do VCI-like transformation
 *
 * @param[in]  aT             Time
 * @param[in]  aY             Wave function (C, phi)
 * @param[out] arResult       On return contains H * C
 * @param[in]  aIntegrals     One-mode integrals for all operators in McTdHEomBase::mOper
 * @param[in]  aVssI          State space of included configurations
 * @param[in]  aVssOut        State space of configurations in result
 * @param[in]  aMeanFields    Prepare mean-field intermediates
 * @param[in]  aV3Trans       Use V3 transformation for coefs derivative always
 **/
void RasTdHTransformer::Transform
   (  step_t aT
   ,  const vec_t& aY
   ,  coefs_t& arResult
   ,  const std::vector<integrals_t>& aIntegrals
   ,  const VccStateSpace& aVssI
   ,  const VccStateSpace& aVssOut
   ,  bool aMeanFields
   ,  bool aV3Trans
   )
{
   LOGCALL("calls");
//   // Resize intermeds
//   this->mIntermediates.resize(aIntegrals.size());

   // Clear intermediates for all operators
   for(auto& intermeds : this->mIntermediates)
   {
      intermeds.ClearIntermeds();
   }

   // If we want to use the V3Contribs for coef derivative
   if (  aV3Trans
      )
   {
      LOGCALL("v3");
      v3_key_t key;
      key.first = aVssI.MCR().GetMaxExciLevel();
      MidasAssert(aVssOut.MCR().GetMaxExciLevel() == key.first, "Different max exci levels in aVssI and aVssOut");

      arResult.Zero();
      auto aux_result = arResult;
      In iop = I_0;
      for(const auto& ints : aIntegrals)
      {
         key.second = ints.Oper().Oper().McLevel();
         if (  this->mContribs.find(key) == this->mContribs.end()
            )
         {
            this->InitializeV3(key.first, key.second, iop);
         }
         evaldata_t eval_data
            (  &aY
            ,  &ints
            ,  aVssI
            ,  this->mIntermediates[iop]
            ,  aT
            );

         this->EvaluateContribs(key, eval_data, aux_result, aVssOut);
         arResult += aux_result;
         ++iop;
      }

      // If we don't need mean fields, stop here!
      if (  !aMeanFields
         )
      {
         return;
      }
   }

   // Init coef derivative for one operator
   coefs_t icdot;
   if (  !aV3Trans
      )
   {
      icdot = arResult;
      icdot.Zero();
   }

   auto nmodes = aVssI.MCR().NumberOfModes();

   auto e_nr = gOperatorDefs.ElectronicDofNr();

   // Loop over operators
   for(In iop=I_0; iop<aIntegrals.size(); ++iop)
   {
      LOGCALL("oper loop");
      // Calculate mean-field intermediates
      // Niels: This is copying a lot from the full MCTDH implementation. It could possibly be done more efficiently in the V3 framework.
      const auto& ints_i = aIntegrals[iop];
      const auto& oper_i = ints_i.Oper();
      const auto& opdef = oper_i.Oper();
      auto nterms = opdef.Nterms();
      auto& mfintermeds_i = this->mIntermediates[iop].GetMfIntermediates();

      // Resize mean-field intermeds (if we need mean fields)
      // Niels: The elements corresponding to one-mode terms will be empty. Not optimal, but easy...
      if (  aMeanFields
         )
      {
         mfintermeds_i.resize(nterms);
         for(In iterm=I_0; iterm<nterms; ++iterm)
         {
            auto nfac = opdef.NfactorsInTerm(iterm);
            if (  nfac > I_1
               )
            {
               mfintermeds_i[iterm].resize(nfac);
            }
         }
      }

      // Zero coef derivative
      if (  !aV3Trans
         )
      {
         icdot.Zero();
      }

      // Parallel section
      #pragma omp parallel
      {
         In nfac;
         LocalModeNr mode;
         LocalModeNr aux_mode;
         LocalOperNr oper;
         LocalOperNr aux_oper;
         coefs_t coefs;
         coefs_t aux_coefs = aY.Coefs();
         aux_coefs.Zero();
         coefs_t tmp_coefs = aY.Coefs();
         tmp_coefs.Zero();

         // Loop over terms in the Hamiltonian
         #pragma omp for schedule(dynamic) reduction(ComplexTensorDataCont_Add : icdot)
         for(In iterm=I_0; iterm<nterms; ++iterm)
         {
            LOGCALL("term loop");

            // Copy coefs
            coefs = aY.Coefs();

            // Loop over factors. Let the mode of ifac correspond to the active mode
            nfac = opdef.NfactorsInTerm(iterm);

            for(In ifac=I_0; ifac<nfac; ++ifac)
            {
               LOGCALL("factors");

               // Get mode and oper for active index
               mode = opdef.ModeForFactor(iterm, ifac);
               oper = opdef.OperForFactor(iterm, ifac);

               // If we need mean fields
               if (  aMeanFields
                  && nfac > I_1        // Skip calculation of H matrices for one-mode terms (= density matrices).
                  && !( this->mAssumeFixedElectronicDofModals
                     && e_nr == opdef.GetGlobalModeNr(mode)
                     )
                  )
               {
                  // Set aux coefs that are transformed to current transformed coefs
                  aux_coefs = coefs;

                  // Do forward contractions after active index on aux_tensor
                  // After this, aux_tensor is ready for calculating a mean-field matrix
                  for(In jfac=ifac+I_1; jfac<nfac; ++jfac)
                  {
                     //LOGCALL("factors after active mode");
                     aux_mode = opdef.ModeForFactor(iterm, jfac);
                     aux_oper = opdef.OperForFactor(iterm, jfac);
                     auto ints = ints_i.GetActiveIntegrals(aux_mode, aux_oper);
                     std::vector<mat_t> ints_vec(nmodes);
                     ints_vec[aux_mode] = std::move(ints);
                     tmp_coefs = coefs;
                     tmp_coefs.Zero();
                     this->OneModeTransform(aux_coefs, tmp_coefs, ints_vec, aVssI, false);
                     aux_coefs = tmp_coefs;
                  }

                  {
                     //LOGCALL("calc mean field");
                     mfintermeds_i[iterm][ifac] = detail::ContractAllButOne(aY.Coefs(), aux_coefs, aVssI, mode);
                  }
               }

               // Do forward contraction before proceeding to next active index
               // After all factors, tensor has been contracted in all modes (only necessary for !aV3Trans)
               if (  ifac < nfac - I_1
                  || !aV3Trans
                  )
               {
                  auto ints = ints_i.GetActiveIntegrals(mode, oper);
                  std::vector<mat_t> ints_vec(nmodes);
                  ints_vec[mode] = std::move(ints);
                  this->OneModeTransform(coefs, aux_coefs, ints_vec, aVssI, false);
                  coefs = aux_coefs;
               }
            }

            // Add to icdot
            if (  !aV3Trans
               )
            {
               icdot.Axpy(coefs, opdef.Coef(iterm));
            }
         }
      }

      // Move into arResult
      if (  !aV3Trans
         )
      {
         arResult.Axpy(icdot, oper_i.Coef(aT));
      }
   }
}

/**
 * Evaluate contributions
 *
 * @param[in]     aKey        Key to the contribs
 * @param[in]     aEvalData   Evaluation data
 * @param[out]    arResult    On return contains H * C
 * @param[in]     aVssOut     StateSpace of result
 **/
void RasTdHTransformer::EvaluateContribs
   (  const v3_key_t& aKey
   ,  const evaldata_t& aEvalData
   ,  coefs_t& arResult
   ,  const VccStateSpace& aVssOut
   )  const
{
   LOGCALL("calls");
   arResult.Zero();

   // Get contribs
   try
   {
#ifdef VAR_MPI
      MidasWarning("MPI not implemented in RasTdHTransformer::EvaluateContribs.");
#endif /* VAR_MPI */
      auto& contribs = this->mContribs.at(aKey);

      if (  this->mEvaluateContribByContrib
         )
      {
         // not mpi, just loop over contributions and evaluate
         // Niels: This may not work if aVssOut differs from the Vss of the input vector!
         for (In i=I_0; i<contribs.size(); ++i)
         {
            // Skip contribs that do not return results of correct exci level
            if (  aVssOut.MCR().NumModeCombisWithExcLevel(contribs[i]->ExciLevel()) == 0
               )
            {
               continue;
            }

            contribs[i]->Evaluate(aEvalData, arResult);
         }
      }
      else
      {
         size_t itens = 0;
         for(const auto& ssmc : aVssOut)
         {
            if (  this->mIoLevel >= I_10
               || gDebug
               )
            {
               Mout  << " EvaluateContribs for MC:\n" << ssmc.MC() << "\n"
                     << " Dimensions of result tensor:   " << ssmc.GetDims() << "\n"
                     << std::flush;
            }

            // Construct TensorSumAccumulator
            midas::vcc::TensorSumAccumulator<param_t> res_vec
                                       (  ssmc.GetDims()
                                       ); 

            // loop over contributions
            for (In i=I_0; i<contribs.size(); ++i)
            {
               // check that modecombi has same number of modes as intermed prod returns
               if (  ssmc.MC().Size() != contribs[i]->ExciLevel()
                  ) 
               {
                  if (  this->mIoLevel >= I_10
                     || gDebug
                     )
                  {
                     Mout  << " Skip contrib '" << *contribs[i] << "' with exci level " << contribs[i]->ExciLevel() << std::endl;
                  }
                  continue; // else we continue loop over mode combination
               }
               
               // then evaluate contribution for current MC
               contribs[i]->Evaluate(aEvalData, ssmc.MC(), res_vec);
            }

            auto& tens_out = arResult.GetModeCombiData(itens++);

            // When all contributions are evaluated we save the result to arDcOut.
            if(ssmc.MC().Size() == 0)
            {
               param_t scalar;
               res_vec.EvaluateSum().GetTensor()->DumpInto(&scalar);
               tens_out.ElementwiseScalarAddition(scalar);
            }
            else
            {
               tens_out = res_vec.EvaluateSum();
            }
         }
      }
   }
   catch (  const std::out_of_range& oor
         )
   {
      MIDASERROR("V3Contribs not found in RasTdHTransformer for MaxExci = " + std::to_string(aKey.first) + ", OperMcLevel = " + std::to_string(aKey.second) + ".");
   }
}

/**
 * Do one-mode transformation with integral matrices using an operator of the form: o = sum_m o^m
 *
 * @param[in]  aCoefs         The coefficients to transform
 * @param[out] arResult       The transformed vector (on exit)
 * @param[in]  aIntegrals     The integrals of the one-mode operators in o = sum_m o^m
 * @param[in]  aVssI          State space of included configurations
 * @param[in]  aOnlyUpDown    Only do up and down contractions, i.e. all integral matrices have zero diagonal blocks.
 * @return
 *    aCoefs transformed by the operator.
 **/
void RasTdHTransformer::OneModeTransform
   (  const coefs_t& aCoefs
   ,  coefs_t& arResult
   ,  const std::vector<mat_t>& aIntegrals
   ,  const VccStateSpace& aVssI
   ,  bool aOnlyUpDown
   )  const
{
   LOGCALL("calls");

   const auto& mcr = aVssI.MCR();

   auto ndof = mcr.NumberOfModes();
   MidasAssert(aIntegrals.size() == ndof, "Wrong number of DOFs in operator!");

   auto mcr_size = mcr.Size();

   MidasAssert(mcr_size == aCoefs.Size(), "Coefs must have same size as MCR[I] (" + std::to_string(aCoefs.Size()) + " vs. " + std::to_string(mcr_size) + ")");
   MidasAssert(arResult.Size() == aCoefs.Size(), "arResult must have same size as aCoefs (" + std::to_string(arResult.Size()) + " vs. " + std::to_string(aCoefs.Size()) + ")");

   arResult.Zero();

   // Find modes that we have integrals for
   std::vector<In> modes;
   for(In imode=I_0; imode<ndof; ++imode)
   {
      if (  aIntegrals[imode].Nrows() != I_0
         )
      {
         MidasAssert(aIntegrals[imode].IsSquare(), "RasTdHTransformer::OneModeTransform: Non-square integral matrix detected!");
         modes.emplace_back(imode);
      }
   }

   // OneModeTransform parallelized over MCs. If not nested OpenMP, this will only be used for Norm2, auto-correlation functions, etc.
   // Niels: Perhaps use,
   // #pragma omp taskloop reduction(ComplexTensorDataCont_Add : arResult)
   // with OMP_NESTED=TRUE to avoid spawning too many threads. But the default for OMP_NESTED is FALSE, so no problem in most cases...
//   #pragma omp parallel for schedule(dynamic) reduction(ComplexTensorDataCont_Add : arResult)
   for(In imc=I_0; imc<mcr_size; ++imc)
   {
      const auto& mc = mcr.GetModeCombi(imc);

      // Do passive and forward contractions if integrals are non-zero
      if (  !aOnlyUpDown
         )
      {
         for(const auto& m : modes)
         {
            // Do forward if m is in mc
            if (  mc.IncludeMode(m)
               )
            {
               auto trans = aCoefs.GetModeCombiData(imc);
               auto idx = mc.IdxNrForMode(m);
               detail::DoForwardContraction(trans, aIntegrals[m], idx, true);
               arResult.GetModeCombiData(imc) += trans;
            }
            // Else do passive
            else
            {
               arResult.GetModeCombiData(imc).Axpy(aCoefs.GetModeCombiData(imc), aIntegrals[m][I_0][I_0]);
            }
         }
      }

      // Do up and down
      for(const auto& m : modes)
      {
         const auto& int_mat = aIntegrals[m];

         // Do down if m is in mc
         if (  mc.IncludeMode(m)
            )
         {
            auto mc_new = mc;
            mc_new.RemoveMode(m);

            // Find result
            ModeCombiOpRange::const_iterator it;
            In res_idx = -I_1;
            if (  mcr.Find(mc_new, it, res_idx)
               )
            {
               // Get integrals
               unsigned dim = int_mat.Ncols() - 1;
               auto ints = std::make_unique<param_t[]>(dim);
               auto* int_ptr = ints.get();
               for(In i=I_1; i<int_mat.Ncols(); ++i)
               {
                  *(int_ptr++) = int_mat[I_0][i];
               }

               // Make tensor
               auto int_tens = NiceSimpleTensor<param_t>(std::vector<unsigned>{dim}, ints.release());
               
               // Get idx number and contract
               In idx = mc.IdxNrForMode(m);
               arResult.GetModeCombiData(res_idx) += aCoefs.GetModeCombiData(imc).ContractDown(idx, int_tens);
            }
         }
         // Else, do up
         else if  (  mc.Size() < mcr.GetMaxExciLevel()
                  )
         {
            auto mc_new = mc;
            mc_new.InsertMode(m);

            // Find result (it may not be contained. If not, just skip!)
            ModeCombiOpRange::const_iterator it;
            In res_idx = -I_1;
            if (  mcr.Find(mc_new, it, res_idx)
               )
            {
               // Get integrals
               unsigned dim = int_mat.Nrows() - 1;
               auto ints = std::make_unique<param_t[]>(dim);
               auto* int_ptr = ints.get();
               for(In i=I_1; i<int_mat.Nrows(); ++i)
               {
                  *(int_ptr++) = int_mat[i][I_0];
               }

               // Make tensor
               auto int_tens = NiceSimpleTensor<param_t>(std::vector<unsigned>{dim}, ints.release());
               
               // Get idx number and contract
               In idx = mc_new.IdxNrForMode(m);
               arResult.GetModeCombiData(res_idx) += aCoefs.GetModeCombiData(imc).ContractUp(idx, int_tens);
            }
         }
      }
   }
}


} /* namespace detail */


/**
 * Constructor from input definitions
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
LinearRasTdHEom::LinearRasTdHEom
   (  const input::McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  LinearMcTdHEomBase
         <  std::complex<Nb>        // param_t
         ,  RasTdHVectorContainer   // vec_t
         ,  LinearRasTdHIntegrals   // integrals_t
         >
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
   ,  mTransformer
         (  apCalcDef
         ,  this->Oper().size()
         )
   ,  mVssI
         (  ConstructModeCombiOpRange(apCalcDef, mcrType::I, this->NModalsVec())
         ,  this->NModalsVec()
         ,  false
         )
   ,  mVssX1
         (  ConstructModeCombiOpRange(apCalcDef, mcrType::X1)
         ,  this->NModalsVec()
         ,  true
         )
   ,  mVssIpX1
         (  ConstructModeCombiOpRange(apCalcDef, mcrType::IpX1, this->NModalsVec())
         ,  this->NModalsVec()
         ,  false
         )
   ,  mGOptNonOrthoActiveSpaceProjector
         (  apCalcDef->GetMethodInfo().template At<bool>("GOPTNONORTHOPROJECTOR")
         )
   ,  mNonOrthoConstraintOperator
         (  apCalcDef->GetMethodInfo().template At<bool>("NONORTHOCONSTRAINTOPERATOR")
         )
   ,  mGOptType
         (  apCalcDef->GetMethodInfo().template At<g_opt_t>("GOPTTYPE")
         )
   ,  mGaugeType
         (  apCalcDef->GetMethodInfo().template At<gauge_t>("GAUGE")
         )
   ,  mGMatrices
         (  this->NDof()
         )
   ,  mV3Trans
         (  apCalcDef->GetMethodInfo().template At<bool>("V3TRANS")
         )
{
   // Set size of g matrices
   if (  mGOptType != g_opt_t::NONE
      || mGaugeType != gauge_t::ZERO
      )
   {
      for(In i=I_0; i<mGMatrices.size(); ++i)
      {
         // g=0 for electronic DOF
         if (  this->H0OpDef().GetGlobalModeNr(i) != this->ElectronicDofNr()
            )
         {
            mGMatrices[i].SetNewSize(this->NAct(i), false, true);
         }
      }
   }

   // Read in V3Contribs for VCI transformer
   if (  this->V3Trans()
      )
   {
      In iop=I_0;
      for(const auto& op : this->Oper())
      {
         this->mTransformer.InitializeV3(this->MaxExciLevel(), op.Oper().McLevel(), iop++);
      }
   }

   // Output MCRs
   if (  this->DoIo(I_10)
      )
   {
      Mout  << " MCR[I]:\n" << this->mVssI.MCR() << "\n\n"
            << " MCR[X1]:\n" << this->mVssX1.MCR() << "\n\n"
            << " MCR[I+X1]:\n" << this->mVssIpX1.MCR() << "\n\n"
            << std::flush;
   }
}

/**
 * @return
 *    Maximum excitation level included in MCR[I] (mode-combination range of the wave function)
 **/
In LinearRasTdHEom::MaxExciLevel
   (
   )  const
{
   return this->mVssI.MCR().GetMaxExciLevel();
}

/**
 * @return
 *    mV3Trans
 **/
bool LinearRasTdHEom::V3Trans
   (
   )  const
{
   return mV3Trans;
}

/**
 * Implementation of derivative
 *
 * @param aT      Time (t)
 * @param aY      WF (y)
 *
 * @return
 *    dy/dt
 **/
typename LinearRasTdHEom::vec_t LinearRasTdHEom::DerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   )
{
   LOGCALL("calls");

   // Update integrals, density matrices, and WF norm2.
   // If exact propagation, the integrals are updated once at the beginning.
   if (  this->Exact()
      && this->mGOptType == g_opt_t::NONE
      && this->mGaugeType == gauge_t::ZERO
      )
   {
      this->mWfNorm2 = aY.Coefs().Norm2();
   }
   else
   {
      this->UpdateIntegrals(aT, aY);
      this->PreCalculateDensityMatrices(aY);
      this->UpdateNorm2(aY);
   }

   auto result = this->ShapedZeroVector();

   // Run coefficient derivative (which also calculates intermediates and g operators)
   this->CoefficientDerivativeImpl(aT, aY, result.Coefs());

   // If not exact, we propagate modals
   if (  !this->Exact()
      || this->mGOptType != g_opt_t::NONE
      || this->mGaugeType != gauge_t::ZERO
      )
   {
      result.Modals() = this->ModalDerivativeImpl(aT, aY);
   }

   // Return result
   return result;
}

/**
 * Perform derivative of configuration-space coefficients, optimize constraint operators, and calculate intermediates for mean-fields.
 *
 * @param[in]  aT          Time
 * @param[in]  aY          Wave function
 * @param[out] arResult    Contains derivative of coefficients on return
 **/
void LinearRasTdHEom::CoefficientDerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  coefs_t& arResult
   )
{
   LOGCALL("calls");

   // Do coef transform
   // NB: This should also calculate intermediates for mean-field construction and RHS for g-operator optimization
   this->mTransformer.Transform(aT, aY, arResult, this->Integrals(), this->mVssI, this->mVssI, !this->Exact(), this->V3Trans());

   // Calculate and save energy. It is much too expensive to do the forward contractions again.
   auto wf_norm2 = this->WaveFunctionNorm2();
   this->mSavedEnergy = std::make_pair(aT, Dot(aY.Coefs(), arResult)/wf_norm2);

   // Calculate g operator
   {
      LOGCALL("update g");
      this->UpdateGIntegrals(aT, aY.Coefs());
   }

   // Multiply g matrices with inverse overlap matrices if necessary
   if (  this->mNonOrthoConstraintOperator
      )
   {
      mat_t sg;
      for(In imode=I_0; imode<this->NDof(); ++imode)
      {
         if (  this->H0OpDef().GetGlobalModeNr(imode) == this->ElectronicDofNr()
            )
         {
            continue;
         }

         auto nact = this->NAct(imode);

         // Get inverse overlap
         auto s_inv = this->InverseOverlapMatrix(imode);

         // Calculate product
         sg.SetNewSize(nact, nact);
         sg = s_inv * this->mGMatrices[imode];

         // Reassign g matrix
         this->mGMatrices[imode] = sg;
      }
   }



   // Subtract g term from coef deriv (if non-zero)
   if (  !( this->mGOptType == g_opt_t::NONE
         && this->mGaugeType == gauge_t::ZERO
         )
      )
   {
      LOGCALL("g trans");

      auto trans_g = aY.Coefs();
      trans_g.Zero();

      this->mTransformer.OneModeTransform(aY.Coefs(), trans_g, this->mGMatrices, this->mVssI, this->mGaugeType == gauge_t::ZERO);

      if (  this->DoIo(I_10)
         )
      {
         Mout  << " Subtracting g term from C derivative:\n" << trans_g << std::endl;
      }

      arResult.Axpy(trans_g, real_t(-1.));
   }

   // If imaginary-time propagation, we subtract a term to ensure normalization
   if (  this->ImagTime()
      )
   {
      // Subtract E*C to preserve normalization
      arResult.Axpy(aY.Coefs(), -this->mSavedEnergy.second);

      // Scale by -1
      arResult.Scale(CC_M_1);
   }
   else
   {
      // Scale by -i
      arResult.Scale(CC_M_I);
   }
}

/**
 * Get H matrix
 *
 * @param aOper
 * @param aTerm
 * @param aMode
 * @param aFactor    Factor in aTerm that operators on aMode
 * @return
 *    H (mean-field) matrix
 **/
const typename LinearRasTdHEom::h_mat_t& LinearRasTdHEom::GetHMatrix
   (  In aOper
   ,  In aTerm
   ,  In aMode
   ,  In aFactor
   )  const
{
   return this->mTransformer.GetIntermediates(aOper).GetMfIntermediates()[aTerm][aFactor];
}

/**
 * Construct density matrix as column-major pointer
 *
 * @param aMode
 * @param aY
 *
 * @return
 *    Column-major pointer
 **/
typename LinearRasTdHEom::DataPtr LinearRasTdHEom::ConstructDensityMatrix
   (  LocalModeNr aMode
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   // Get number of active + occupied modals
   auto ntd = this->NAct(aMode);
   auto nact = ntd - I_1;

   mat_t densmat(ntd);

   const auto& mcr = this->mVssI.MCR();
   const auto& coefs = aY.Coefs();

   if (  mcr.Size() != coefs.Size()
      )
   {
      Mout  << " MCR[I]:\n" << mcr << "\n\n"
            << " coefs:\n" << coefs << "\n\n"
            << std::flush;
      MIDASERROR("Size mismatch in coefs and MCR[I]!");
   }

   for(In i_mc=I_0; i_mc<mcr.Size(); ++i_mc)
   {
      const ModeCombi& mc = mcr.GetModeCombi(i_mc);

      const auto& c = coefs.GetModeCombiData(i_mc);
      
      // Reference. We add coef squared to occ-occ element and continue
      if (  mc.Empty()
         )
      {
         densmat[I_0][I_0] += midas::util::AbsVal2(c.GetScalar());
         continue;
      }

      // If the MC includes aMode, we add density matrices to act-act and off-diagonal blocks
      if (  mc.IncludeMode(aMode)
         )
      {
         // Get index of aMode in current tensor
         auto mode_idx = mc.IdxNrForMode(aMode);

         // Calculate act-act contribution
         {
            auto d_vw = detail::ContractAllButOne(c, c, mode_idx);

            // Load row-major pointer into matrix
            const auto* ptr = d_vw.template StaticCast<SimpleTensor<param_t> >().GetData();
            for(In v=I_0; v<nact; ++v)
            {
               for(In w=I_0; w<nact; ++w)
               {
                  densmat[v+I_1][w+I_1] += *(ptr++);
               }
            }
         }

         // Calculate act-occ contribution (and set occ-act by complex conjugation)
         {
            // Construct MC with aMode removed
            auto mc_right = mc;
            mc_right.RemoveMode(aMode);

            // Find the corresponding tensor
            ModeCombiOpRange::const_iterator it_mc;
            In idx;
            if (  !mcr.Find(mc_right, it_mc, idx)
               )
            {
               Mout  << " Could not find MC:\n" << mc_right << std::endl;
               MIDASERROR("Could not find MC in MCR[I] for density-matrix calculation!");
            }
            const auto& c_right = coefs.GetModeCombiData(idx);

            // Contract all indices of right tensor. Keep mode_idx in left.
            auto d_vi = detail::ContractAllInRight(c, c_right, mode_idx);

            // Load data
            const auto* ptr = d_vi.template StaticCast<SimpleTensor<param_t> >().GetData();
            for(In v=I_0; v<nact; ++v)
            {
               densmat[v+I_1][I_0] += *ptr;
               densmat[I_0][v+I_1] += midas::math::Conj(*ptr);
               ++ptr;
            }
         }
      }
      // Else, the occ-occ element gets a contribution
      else
      {
         densmat[I_0][I_0] += c.Norm2();
      }
   }

   // IO
   if (  this->DoIo(I_10)
      )
   {
      Mout  << "Density matrix for mode " << aMode << "\n"
            << densmat << std::endl;
   }

   // Convert to column-major pointer and return
   return ColMajPtrFromMidasMatrix2(densmat);
}

/**
 * Add constraint to modal derivative
 *
 * @param aMode   Mode
 * @param aModals Modals
 * @param arDyDt  Derivative
 **/
void LinearRasTdHEom::AddNonProjectedModalDerivativeTerms
   (  LocalModeNr aMode
   ,  const modal_t& aModals
   ,  DataPtr& arDyDt
   )  const
{
   LOGCALL("calls");

   // If g is always zero, do nothing
   if (  (  this->mGOptType == g_opt_t::NONE
         && this->mGaugeType == gauge_t::ZERO
         )
      || this->H0OpDef().GetGlobalModeNr(aMode) == this->ElectronicDofNr()
      )
   {
      return;
   }

   // Allocate arDyDt if this is the only term
   auto nbas = this->NBas(aMode);
   auto nact = this->NAct(aMode);
   if (  !arDyDt
      )
   {
      auto sigma_size = nbas*nact;
      arDyDt = std::make_unique<param_t[]>(sigma_size);
      for(In i=I_0; i<sigma_size; ++i)
      {
         arDyDt[i] = param_t(0.0);
      }
   }

   const auto& g = this->mGMatrices[aMode];

   for(In v=I_0; v<nact; ++v)
   {
      for(In r=I_0; r<nbas; ++r)
      {
         param_t val(0.0);
         for(In w=I_0; w<nact; ++w)
         {
            val += aModals[r + this->ModalOffsets()[aMode][w]] * g[w][v];
         }

         // NB: Sigma has reverse indexing (v r)
         arDyDt[v + r*nact] += val;
      }
   }
}


/**
 * Update g operators
 *
 * @param aT            Time
 * @param aCoefs        Configuration-space coefficients
 **/
void LinearRasTdHEom::UpdateGIntegrals
   (  step_t aT
   ,  const coefs_t& aCoefs
   )
{
   LOGCALL("calls");

   // Zero all elements
   for(auto& g : mGMatrices)
   {
      g.Zero();
   }

   // Optimize non-redundant active-occupied block first
   switch   (  mGOptType
            )
   {
      case g_opt_t::VARIATIONAL:
      {
         coefs_t s_trans_coefs;
         bool non_ortho = this->CalcDef()->GetMethodInfo().template At<bool>("GOPTNONORTHOPROJECTOR");
         if (  non_ortho
            )
         {
            s_trans_coefs = aCoefs;
            this->PerformInverseOverlapTransforms(s_trans_coefs, this->mVssI);
         }
         const auto& right_coefs = non_ortho ? s_trans_coefs : aCoefs;

         // Figure out which modes the g operators can be optimized for,
         // i.e. the modes where the choice of g matters
         auto [modes, diag, size, offsets] = this->PrepareGOptimization(aCoefs, right_coefs);

         if (  this->DoIo(I_10)
            )
         {
            Mout  << " Optimize g operators for modes:" << std::endl;
            for(const auto& m : modes)
            {
               Mout << m << " ";
            }
            Mout  << std::endl
                  << " Diagonal elements:" << std::endl;
            for(const auto& d : diag)
            {
               Mout  << d << " ";
            }
            Mout  << std::endl
                  << " Dimension of linear equations:  " << size << "\n"
                  << " Offsets for modes in A and b:" << std::endl;
            for(const auto& o : offsets)
            {
               Mout  << o << " ";
            }
            Mout  << std::endl;
         }

         if (  !modes.empty()
            )
         {
            // Construct A matrix and B vector
            auto a_matrix = this->ConstructAMatrix(aCoefs, right_coefs, modes, diag, size, offsets);
            auto b_vector = this->ConstructBVector(aT, aCoefs, modes, size, offsets);

            if (  !a_matrix.IsHermitian()
               )
            {
               MIDASERROR("A matrix is not Hermitian!");
            }

            // DEBUG
            if (  this->DoIo(I_10)
               )
            {
               Mout  << " A:\n" << a_matrix << "\n"
                     << " b:\n" << b_vector << "\n"
                     << std::flush;
            }

            auto solution = this->SolveGOptEquations(a_matrix, b_vector);
            auto* sol_ptr = solution.get();

            // Load solution to g matrices
            for(const auto& m : modes)
            {
               In nelem = this->mGMatrices[m].Nrows() - I_1;
               for(In ielem=I_0; ielem<nelem; ++ielem)
               {
                  auto val = this->ImagTime() ? CC_M_I * *(sol_ptr++) : *(sol_ptr++);
                  this->mGMatrices[m][ielem+I_1][I_0] = val;
                  this->mGMatrices[m][I_0][ielem+I_1] = midas::math::Conj(val);
               }

               // DEBUG
               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << " g^" << m << ":\n" << this->mGMatrices[m] << std::endl;
               }
            }
         }

         // Break case
         break;
      }
      case g_opt_t::DENSITYMATRIX:
      {
         if (  this->ImagTime()
            )
         {
            MIDASERROR("Density-matrix constraint not impl for imag-time propagation!");
         }
         for(In m=I_0; m<this->NDof(); ++m)
         {
            if (  this->H0OpDef().GetGlobalModeNr(m) == this->ElectronicDofNr()
               )
            {
               continue;
            }

            auto ntd = this->NAct(m);
            auto nact = ntd - I_1;
            mat_t dens(nact);
            bvec_t rhs(nact);

            // Set matrix elements
            for(In icol=I_0; icol<nact; ++icol)
            {
               for(In irow=I_0; irow<nact; ++irow)
               {
                  dens[irow][icol] = -this->DensityMatrixElement(m, irow + I_1, icol + I_1);

                  if (  icol == irow
                     )
                  {
                     dens[irow][icol] += this->DensityMatrixElement(m, I_0, I_0);
                  }
               }
            }

            // Set vector elements
            In iop=I_0;
            for(const auto& op_i : this->Oper())
            {
               const auto& opdef = op_i.Oper();
               In n_act_terms = opdef.NactiveTerms(m);
               for(In i_act_term=I_0; i_act_term<n_act_terms; ++i_act_term)
               {
                  In i_term = opdef.TermActive(m, i_act_term);
                  In nfac = opdef.NfactorsInTerm(i_term);

                  // Get factor for m
                  In i_fac_thismode = -I_1;
                  for(In i_fac=I_0; i_fac<nfac; ++i_fac)
                  {
                     In i_op_mode = opdef.ModeForFactor(i_term, i_fac);
                     if (  i_op_mode == m
                        )
                     {
                        i_fac_thismode = i_fac;
                        break;
                     }
                  }

                  // Sanity check
                  if (  i_fac_thismode == -I_1
                     )
                  {
                     MIDASERROR("Factor for mode " + std::to_string(m) + " not found in term " + std::to_string(i_term) + ". This should not happen!");
                  }

                  // Get h_mat_data stored as row-major matrix
                  param_t* h_mat_data = nullptr;
                  if (  nfac > I_1
                     )
                  {
                     // get mean-field matrix (see MCTDH review)
                     const auto& h_mat = this->mTransformer.GetIntermediates(iop).GetMfIntermediates()[i_term][i_fac_thismode];

                     h_mat_data = static_cast<const SimpleTensor<param_t>* const>(h_mat.GetTensor())->GetData();
                  }

                  // Get oper for m
                  In ioper = opdef.OperForFactor(i_term, i_fac_thismode);

                  // Get integral matrices
                  const auto& td_int = this->Integrals()[iop].GetActiveIntegrals(m, ioper);

                  // Get coef
                  auto coef = op_i.Coef(aT) * opdef.Coef(i_term);

                  for(In v=I_0; v<nact; ++v)
                  {
                     param_t val(0.);
                     for(In k=I_0; k<ntd; ++k)
                     {
                        param_t h_mat_elem_1 = h_mat_data ? h_mat_data[k] : this->DensityMatrixElement(m, 0, k);
                        param_t h_mat_elem_2 = h_mat_data ? h_mat_data[(v+I_1)*ntd + k] : this->DensityMatrixElement(m, v+1, k);
                        val += (midas::math::Conj(h_mat_elem_1 * td_int[v+I_1][k]) - h_mat_elem_2 * td_int[I_0][k]);
                     }

                     // Add to RHS vector
                     rhs[v] += coef*val;
                  }
               }

               ++iop;
            }

            // Solve equations (with regularization)
            auto g_sol = this->SolveGOptEquations(dens, rhs);

            // Set solution
            for(In v=I_0; v<nact; ++v)
            {
               this->mGMatrices[m][I_0][v+I_1] = g_sol[v];
               this->mGMatrices[m][v+I_1][I_0] = midas::math::Conj(g_sol[v]);
            }
         }

         break;
      }
      case g_opt_t::NONE:
      {
         break;
      }
      case g_opt_t::ONEMODEH:
      {
         for(In iop=I_0; iop<this->Oper().size(); ++iop)
         {
            const auto& oper_i = this->Oper()[iop];
            const auto& opdef = oper_i.Oper();
            auto nterms = opdef.Nterms();

            // Loop over terms
            for(In iterm=I_0; iterm<nterms; ++iterm)
            {
               auto nfac = opdef.NfactorsInTerm(iterm);

               // Skip coupling terms
               if (  nfac != I_1
                  )
               {
                  continue;
               }

               // Get mode (skip for electronic DOF)
               auto mode = opdef.ModeForFactor(iterm, I_0);
               if (  this->H0OpDef().GetGlobalModeNr(mode) == this->ElectronicDofNr()
                  )
               {
                  continue;
               }
               auto oper = opdef.OperForFactor(iterm, I_0);
               auto nact = this->NAct(mode);

               // Get integrals
               const auto& ints = this->Integrals()[iop].GetActiveIntegrals(mode, oper);

               auto& gm = this->mGMatrices[mode];

               // Add to occ-act and act-occ elements
               auto coef = opdef.Coef(iterm) * oper_i.Coef(aT);
               for(In v=I_1; v<nact; ++v)
               {
                  gm[v][I_0] += coef * ints[v][I_0];
                  gm[I_0][v] += coef * ints[I_0][v];
               }
            }
         }

         break;
      }
      default:
      {
         MIDASERROR("Unrecognized GOptType!");
      }
   }

   // Set the redundant elements
   switch   (  mGaugeType
            )
   {
      case gauge_t::ZERO:
      {
         break;
      }
      case gauge_t::ONEMODEH:
      {
         for(In iop=I_0; iop<this->Oper().size(); ++iop)
         {
            const auto& oper_i = this->Oper()[iop];
            const auto& opdef = oper_i.Oper();
            auto nterms = opdef.Nterms();

            // Loop over terms
            for(In iterm=I_0; iterm<nterms; ++iterm)
            {
               auto nfac = opdef.NfactorsInTerm(iterm);

               // Skip coupling terms
               if (  nfac != I_1
                  )
               {
                  continue;
               }

               // Get mode (skip for electronic DOF)
               auto mode = opdef.ModeForFactor(iterm, I_0);
               if (  this->H0OpDef().GetGlobalModeNr(mode) == this->ElectronicDofNr()
                  )
               {
                  continue;
               }
               auto oper = opdef.OperForFactor(iterm, I_0);
               auto nact = this->NAct(mode);

               // Get integrals
               const auto& ints = this->Integrals()[iop].GetActiveIntegrals(mode, oper);

               auto& gm = this->mGMatrices[mode];

               // Add to occ-act and act-occ elements
               auto coef = opdef.Coef(iterm) * oper_i.Coef(aT);

               // Add to occ-occ element
               gm[I_0][I_0] += coef * ints[I_0][I_0];

               // Add to act-act block
               for(In v=I_1; v<nact; ++v)
               {
                  for(In w=I_1; w<nact; ++w)
                  {
                     gm[v][w] += coef * ints[v][w];
                  }
               }
            }
         }

         break;
      }
      case gauge_t::DENSITYMATRIX:
      {
         MIDASERROR("Not impl!");
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized GaugeType!");
      }
   }

   if (  this->DoIo(I_10)
      )
   {
      Mout  << " g integrals:" << std::endl;
      for(In m=I_0; m<this->mGMatrices.size(); ++m)
      {
         Mout  << " g^" << m << ":" << std::endl;
         Mout  << this->mGMatrices[m] << std::endl;
      }
   }
}

/**
 * Solve (possibly singular) linear equations for optimizin g operators.
 *
 * @param aA
 * @param aB
 * @return
 *    Solution as std::unique_ptr
 **/
typename LinearRasTdHEom::DataPtr LinearRasTdHEom::SolveGOptEquations
   (  const mat_t& aA
   ,  const bvec_t& aB
   )  const
{
   LOGCALL("calls");

   MidasAssert(aA.IsSquare(), "A matrix for g-operator optimization is not square!");

   auto regtype = this->CalcDef()->GetMethodInfo().template At<input::McTdHCalcDef::GOptRegType>("GOPTREGULARIZATIONTYPE");
   auto eps = this->CalcDef()->GetMethodInfo().template At<Nb>("GOPTREGULARIZATIONEPSILON");

   // If VARIATIONAL, the matrix must be positive (semi-)definite.
   // For DENSITYMATRIX, negative eigenvalues are allowed
   bool allow_neg_eigval = (this->mGOptType == input::McTdHCalcDef::GOptType::DENSITYMATRIX);


   // Scale regularization parameter with abs trace of aA
   if (  this->CalcDef()->GetMethodInfo().template At<bool>("GOPTTRACESCALEEPS")
      )
   {
      auto trace = aA.Trace();
      eps *= std::abs(trace);
   }

   bool fall_through = false;
   switch   (  regtype
            )
   {
      // Solve linear equations using HESV
      case input::McTdHCalcDef::GOptRegType::NONE:
      {
         LOGCALL("none");
         Linear_eq_struct<param_t> lin_sol;

         // Do POSV if matrix should be positive definite
         if (  !allow_neg_eigval
            )
         {
            lin_sol = POSV(aA, aB);
         }

         // If matrix is only Hermitian, or POSV failed, do HESV
         if (  allow_neg_eigval
            || lin_sol.info != 0
            )
         {
            lin_sol = HESV(aA, aB);
         }

         // If HESV also fails, fallthrough to STANDARD regularization
         fall_through = (lin_sol.info != 0);

         if (  !fall_through
            )
         {
            return std::move(lin_sol.solution);
            break;
         }
         else
         {
            [[fallthrough]];
         }
      }
      // Regularize using Tikhonov
      case input::McTdHCalcDef::GOptRegType::TIKHONOV:
      {
         if (  !fall_through
            )
         {
            LOGCALL("tikhonov");
            auto a_reg = aA;
            for(In i=I_0; i<a_reg.Ncols(); ++i)
            {
               a_reg[i][i] += eps;
            }

            Linear_eq_struct<param_t> lin_sol;

            // Do POSV if matrix should be positive definite
            if (  !allow_neg_eigval
               )
            {
               lin_sol = POSV(a_reg, aB);
            }

            // If matrix is only Hermitian, or POSV failed, do HESV
            if (  allow_neg_eigval
               || lin_sol.info != 0
               )
            {
               lin_sol = HESV(a_reg, aB);
            }

            fall_through = (lin_sol.info != 0);

            if (  !fall_through
               )
            {
               return std::move(lin_sol.solution);
               break;
            }
            else
            {
               [[fallthrough]];
            }
         }
      }
      // Solve using least squares (SVD)
      case input::McTdHCalcDef::GOptRegType::LS:
      {
         if (  !fall_through
            )
         {
            LOGCALL("gelss");
            auto ls_sol = GELSS(aA, aB, eps);
            
            MidasAssert(ls_sol.info == 0, "GELSS failed with info = " + std::to_string(ls_sol.info));

            return std::move(ls_sol.B_);
            break;
         }
         else
         {
            [[fallthrough]];
         }
      }
      // Solve using EVD with smooth (exponential) regularization
      case input::McTdHCalcDef::GOptRegType::STANDARD:
      {
         LOGCALL("std");

         if (  fall_through
            )
         {
            MidasWarning("Fall back to STANDARD (eigenvalue-based) regularization.");
         }

         // Solve using regularized linear equations (same as with density matrix)
         Eigenvalue_struct<param_t> evd;
         {
            LOGCALL("heevd");
            evd = HEEVD(aA, 'V', 'U');
         }

         if (  evd._info != 0
            )
         {
            LOGCALL("heev");
            MidasWarning("HEEVD failed. Trying HEEV!");
            evd = HEEV(aA, 'V', 'U');
         }

         if (  evd._info != 0
            )
         {
            MIDASERROR("HEEVD and HEEV failed!");
         }

         auto n = evd._n;
         auto** eigvecs = evd._eigenvectors;
         auto* eigvals = evd._eigenvalues;

         if (  this->DoIo(I_10)
            )
         {
            Mout  << " Solving linear equations for determining g matrix:\n"
                  << "   A:\n" << aA << "\n"
                  << "   b:\n" << aB << "\n"
                  << "   eps:" << eps << "\n"
                  << std::flush;

            Mout  << " Eigenvalues of g-opt matrix:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               Mout  << eigvals[i] << "  ";
            }
            Mout  << std::endl;
         }

         if (  !allow_neg_eigval
            )
         {
            // Check for negative eigenvalues.
            for(In i=I_0; i<n; ++i)
            {
               if (  eigvals[i] < real_t(0)
                  )
               {
                  // Zero the eigenvalue if it is very small
                  if (  libmda::numeric::float_numeq_zero(eigvals[i], eigvals[n-1], 10)
                     )
                  {
                     eigvals[i] = real_t(0.);
                  }
                  else
                  {
                     Mout  << " Found eigenvalue: " << eigvals[i] << std::endl;
                     MIDASERROR("Did not expect negative eigenvalues of matrix for g optimization!");
                  }
               }
            }
         }

         // Assemble inverse A matrix
         auto inv_reg_a = std::make_unique<param_t[]>(n*n);
         {
            LOGCALL("assemble inverse");

            #pragma omp parallel for collapse(2)
            for(In icol=I_0; icol<n; ++icol)
            {
               for(In irow=I_0; irow<n; ++irow)
               {
                  param_t val(0.);
                  for(In iev=I_0; iev<n; ++iev)
                  {
                     if (  allow_neg_eigval
                        && eigvals[iev] < C_0
                        )
                     {
                        val += eigvecs[iev][irow] * midas::math::Conj(eigvecs[iev][icol]) * real_t(1.)/(eigvals[iev] - eps*std::exp(eigvals[iev]/eps));
                     }
                     else
                     {
                        val += eigvecs[iev][irow] * midas::math::Conj(eigvecs[iev][icol]) * real_t(1.)/(eigvals[iev] + eps*std::exp(-eigvals[iev]/eps));
                     }
                  }
                  inv_reg_a[irow+n*icol] = val;
               }
            }
         }

         if (  this->DoIo(I_10)
            )
         {
            Mout << " Inverse regularized A:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  Mout  << inv_reg_a[i + n*j] << "  ";
               }
               Mout  << std::endl;
            }

            if (  gDebug
               )
            {
               mat_t a_inv_times_a(n,n);
               for(In i=I_0; i<n; ++i)
               {
                  for(In j=I_0; j<n; ++j)
                  {
                     param_t val(0.);
                     for(In k=I_0; k<n; ++k)
                     {
                        val += inv_reg_a[i + n*k] * aA[k][j];
                     }
                     a_inv_times_a[i][j] = val;
                  }
               }

               Mout  << " A^{-1}_reg * A = \n" << a_inv_times_a << std::endl;
            }
         }


         // Multiply on b vector and return result
         auto result = std::make_unique<param_t[]>(n);

         {
            LOGCALL("gemv");

            char trans = 'N';
            param_t alpha(1.0);
            int lda = std::max(n, 1);
            int incx = 1;
            param_t beta(0.0);
            int incy = 1;
            midas::lapack_interface::gemv
               (  &trans
               ,  &n
               ,  &n
               ,  &alpha
               ,  inv_reg_a.get()
               ,  &lda
               ,  const_cast<bvec_t&>(aB).data() // does not modify elements!
               ,  &incx
               ,  &beta
               ,  result.get()
               ,  &incy
               );
         }

         // DEBUG
         if (  this->DoIo(I_10)
            )
         {
            Mout  << "   Result:" << std::endl;

            for(In i=I_0; i<n; ++i)
            {
               Mout  << result[i] << "  ";
            }
            Mout << std::endl;

            bvec_t residual = aB;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  residual[i] -= aA[i][j] * result[j];
               }
            }

            auto res_norm = residual.Norm();
            auto b_norm = aB.Norm();

            Mout  << "   |res|: " << res_norm << std::endl;
            Mout  << "   |res|/|RHS|: " << res_norm / b_norm << std::endl;
         }

         return result;

         break;
      }
      // Solve using SVD where all singular values are set to max(s, eps*s_max)
      case input::McTdHCalcDef::GOptRegType::SVD:
      {
         LOGCALL("svd");

         // Compute full SVD with all columns
         auto svd = GESDD(aA, 'A');
         if (  svd.info != 0
            )
         {
            MIDASERROR("SVD of matrix for g optimization failed!");
         }
         auto n = svd.n;
         const auto& u = svd.u;
         const auto& vt = svd.vt;
         const auto& s = svd.s;

         // Construct regularized inverse
         auto inv_reg_a = std::make_unique<param_t[]>(n*n);
         auto* ptr = inv_reg_a.get();
         for(In icol=I_0; icol<n; ++icol)
         {
            for(In irow=I_0; irow<n; ++irow)
            {
               param_t val(0.);
               for(In isv=I_0; isv<n; ++isv)
               {
                  val += midas::math::Conj(vt[isv + n*irow] * u[icol + n*isv]) / std::max(s[isv], eps*s[0]);
               }
               *(ptr++) = val;
            }
         }

         if (  this->DoIo(I_10)
            )
         {
            Mout  << " Solving linear equations for determining g matrix:\n"
                  << "   A:\n" << aA << "\n"
                  << "   b:\n" << aB << "\n"
                  << "   eps:" << eps << "\n"
                  << std::flush;

            Mout  << " Singular values of g-opt matrix:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               Mout  << s[i] << "  ";
            }
            Mout  << std::endl;

            Mout << " Inverse regularized A:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  Mout  << inv_reg_a[i + n*j] << "  ";
               }
               Mout  << std::endl;
            }

            if (  gDebug
               )
            {
               mat_t a_inv_times_a(n,n);
               for(In i=I_0; i<n; ++i)
               {
                  for(In j=I_0; j<n; ++j)
                  {
                     param_t val(0.);
                     for(In k=I_0; k<n; ++k)
                     {
                        val += inv_reg_a[i + n*k] * aA[k][j];
                     }
                     a_inv_times_a[i][j] = val;
                  }
               }

               Mout  << " A^{-1}_reg * A = \n" << a_inv_times_a << std::endl;
            }
         }


         // Multiply on b vector and return result
         auto result = std::make_unique<param_t[]>(n);

         {
            char trans = 'N';
            param_t alpha(1.0);
            int lda = std::max(n, 1);
            int incx = 1;
            param_t beta(0.0);
            int incy = 1;
            midas::lapack_interface::gemv
               (  &trans
               ,  &n
               ,  &n
               ,  &alpha
               ,  inv_reg_a.get()
               ,  &lda
               ,  const_cast<bvec_t&>(aB).data() // does not modify elements!
               ,  &incx
               ,  &beta
               ,  result.get()
               ,  &incy
               );
         }

         // DEBUG
         if (  this->DoIo(I_10)
            )
         {
            Mout  << "   Result:" << std::endl;

            for(In i=I_0; i<n; ++i)
            {
               Mout  << result[i] << "  ";
            }
            Mout << std::endl;

            bvec_t residual = aB;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  residual[i] -= aA[i][j] * result[j];
               }
            }

            auto res_norm = residual.Norm();
            auto b_norm = aB.Norm();

            Mout  << "   |res|: " << res_norm << std::endl;
            Mout  << "   |res|/|RHS|: " << res_norm / b_norm << std::endl;
         }

         return result;

         break;
      }
      // Solve using SVD with smooth (exponential) regularization of singular values
      case input::McTdHCalcDef::GOptRegType::XSVD:
      {
         LOGCALL("xsvd");

         // Compute full SVD with all columns
         auto svd = GESDD(aA, 'A');
         if (  svd.info != 0
            )
         {
            MIDASERROR("SVD of matrix for g optimization failed!");
         }
         auto n = svd.n;
         const auto& u = svd.u;
         const auto& vt = svd.vt;
         const auto& s = svd.s;

         // Construct regularized inverse
         auto inv_reg_a = std::make_unique<param_t[]>(n*n);
         #pragma omp parallel for collapse(2)
         for(In icol=I_0; icol<n; ++icol)
         {
            for(In irow=I_0; irow<n; ++irow)
            {
               param_t val(0.);
               for(In isv=I_0; isv<n; ++isv)
               {
                  val += midas::math::Conj(vt[isv + n*irow] * u[icol + n*isv]) / (s[isv] + eps*std::exp(-s[isv]/eps));
               }
               inv_reg_a[irow+n*icol] = val;
            }
         }

         if (  this->DoIo(I_10)
            )
         {
            Mout  << " Solving linear equations for determining g matrix:\n"
                  << "   A:\n" << aA << "\n"
                  << "   b:\n" << aB << "\n"
                  << "   eps:" << eps << "\n"
                  << std::flush;

            Mout  << " Singular values of g-opt matrix:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               Mout  << s[i] << "  ";
            }
            Mout  << std::endl;

            Mout << " Inverse regularized A:" << std::endl;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  Mout  << inv_reg_a[i + n*j] << "  ";
               }
               Mout  << std::endl;
            }

            if (  gDebug
               )
            {
               mat_t a_inv_times_a(n,n);
               for(In i=I_0; i<n; ++i)
               {
                  for(In j=I_0; j<n; ++j)
                  {
                     param_t val(0.);
                     for(In k=I_0; k<n; ++k)
                     {
                        val += inv_reg_a[i + n*k] * aA[k][j];
                     }
                     a_inv_times_a[i][j] = val;
                  }
               }

               Mout  << " A^{-1}_reg * A = \n" << a_inv_times_a << std::endl;
            }
         }


         // Multiply on b vector and return result
         auto result = std::make_unique<param_t[]>(n);

         {
            char trans = 'N';
            param_t alpha(1.0);
            int lda = std::max(n, 1);
            int incx = 1;
            param_t beta(0.0);
            int incy = 1;
            midas::lapack_interface::gemv
               (  &trans
               ,  &n
               ,  &n
               ,  &alpha
               ,  inv_reg_a.get()
               ,  &lda
               ,  const_cast<bvec_t&>(aB).data() // does not modify elements!
               ,  &incx
               ,  &beta
               ,  result.get()
               ,  &incy
               );
         }

         // DEBUG
         if (  this->DoIo(I_10)
            )
         {
            Mout  << "   Result:" << std::endl;

            for(In i=I_0; i<n; ++i)
            {
               Mout  << result[i] << "  ";
            }
            Mout << std::endl;

            bvec_t residual = aB;
            for(In i=I_0; i<n; ++i)
            {
               for(In j=I_0; j<n; ++j)
               {
                  residual[i] -= aA[i][j] * result[j];
               }
            }

            auto res_norm = residual.Norm();
            auto b_norm = aB.Norm();

            Mout  << "   |res|: " << res_norm << std::endl;
            Mout  << "   |res|/|RHS|: " << res_norm / b_norm << std::endl;
         }

         return result;

         break;
      }
      default:
      {
         MIDASERROR("Unrecognized regularization type for optimization MCTDH constraint operators!");
         return nullptr;
      }
   }
}

/**
 * Prepare optimization of g matrices
 *
 * @param aCoefs        C coefficients
 * @param aTransCoefs   C coefficients possibly transformed with inverse overlaps
 * @return
 *    tuple of: modes to optimize, the diagonal elements of A, the dimension of the optimization problem, and the offsets for modes in A and b.
 **/
typename LinearRasTdHEom::goptinfo_t LinearRasTdHEom::PrepareGOptimization
   (  const coefs_t& aCoefs
   ,  const coefs_t& aTransCoefs
   )  const
{
   LOGCALL("calls");

   if (  this->DoIo(I_10)
      )
   {
      Mout  << " Prepare g optimization." << std::endl;
   }

   MidasAssert(aCoefs.Size() == aTransCoefs.Size(), "Coefs should have same size!");

   std::vector<real_t> diag(this->NDof());

   const auto& ext_range_modes = this->CalcDef()->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");

   // Loop over MCR[X1]
//   Mout  << "    Loop over MCR[X1]." << std::endl;     // DEBUG
   for(const auto& mc_x1 : this->mVssX1.MCR())
   {
//      Mout  << "       MC: " << mc_x1 << std::endl;     // DEBUG
      const auto& mcl = mc_x1.Size();

      // Remove one mode
      for(In imodeidx=I_0; imodeidx<mcl; ++imodeidx)
      {
         const auto& imode = mc_x1.Mode(imodeidx);

         // Make MC with imode removed
         auto mc_i = mc_x1;
         mc_i.RemoveMode(imode);

         if (  this->DoIo(I_10)
            )
         {
            Mout  << "    Calculate norm2 for sub-MC: " << mc_i << std::endl;
         }

         // Calculate diagonal (unit matrix times norm2 of coefs for mc_i).
         In tensor_idx_i = -I_1;
         ModeCombiOpRange::const_iterator it_mc_i;
         if (  !this->mVssI.MCR().Find(mc_i, it_mc_i, tensor_idx_i)
            )
         {
            // This should only happen if we are using xCS for the removed mode
            if (  ext_range_modes.empty()
               || (  !ext_range_modes.empty()
                  && ext_range_modes.find(imode) == ext_range_modes.end()
                  )
               )
            {
               Mout  << " ERROR: Did not find MC: " << mc_i << " in MCR[I]." << std::endl;
               MIDASERROR("LinearRasTdHEom::PrepareGOptimization: Mode combi not found in included configurations!");
            }
         }
         else
         {
            if (  this->DoIo(I_10)
               )
            {
               Mout  << "    Index of tensor for sub-MC: " << tensor_idx_i << std::endl;
            }

            const auto& c_i = aCoefs.GetModeCombiData(tensor_idx_i);
            const auto& c_j = aTransCoefs.GetModeCombiData(tensor_idx_i);
            auto add = dot_product(c_i, c_j);
            if (  this->DoIo(I_10)
               )
            {
               Mout  << "    Add " << add << " to diag[" << imode << "]" << std::endl;
            }
            if (  !libmda::numeric::float_numeq_zero(std::imag(add), std::real(add), 10)
               )
            {
               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << " A matrix diagonal: <C|S^-1|C> = " << add << std::endl;
               }
               MidasWarning("Neglecting imaginary part of A matrix diagonal!");
            }
            diag[imode] += std::real(add);
         }
      }
   }


   if (  this->DoIo(I_10)
      )
   {
      Mout  << "    Diagonal values of A matrix:\n" << diag << std::endl;     // DEBUG
   }

   std::set<In> modes;
   In goptdim = I_0;
   std::map<In, In> offsets;
   std::map<In, real_t> diag_out;

   // Non-zero diagonal elements
   for(In imode=I_0; imode<this->NDof(); ++imode)
   {
      if (  diag[imode] > real_t(0.0)
         && this->H0OpDef().GetGlobalModeNr(imode) != this->ElectronicDofNr() // g=0 for electronic DOF
         )
      {
         diag_out[imode] = diag[imode];
         modes.emplace(imode);
         offsets[imode] = goptdim;
         goptdim += this->NAct(imode) - I_1;
      }
   }

//   Mout  << "    Optimize g for modes:\n" << modes << std::endl;     // DEBUG

   // Return tuple
   return std::tie(modes, diag_out, goptdim, offsets);
}

/**
 * Construct A matrix for variational optimization of g
 *
 * @param aCoefs        C coefficients
 * @param aTransCoefs   C coefficients possibly transformed with inverse overlaps
 * @param aModes        The modes for which we get non-zero blocks
 * @param aDiag         Diagonal elements for non-zero blocks
 * @param aSize         Dimension of matrix
 * @param aOffsets      Offset in matrix for mode
 * @return
 *    A matrix
 **/
typename LinearRasTdHEom::mat_t LinearRasTdHEom::ConstructAMatrix
   (  const coefs_t& aCoefs
   ,  const coefs_t& aTransCoefs
   ,  const std::set<In>& aModes
   ,  const std::map<In, real_t>& aDiag
   ,  In aSize
   ,  const std::map<In, In>& aOffsets
   )  const
{
   LOGCALL("calls");

   if (  this->DoIo(I_10)
      )
   {
      Mout  << " Construct A matrix.\n"
            << "    aSize:    " << aSize << "\n"
            << "    aModes:   " << aModes << "\n"
            << "    aOffsets: " << aOffsets << "\n"
            << std::endl;
   }

   // Init result
   mat_t result(aSize);
   result.Zero();

   const auto& ext_range_modes = this->CalcDef()->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");

   // Loop over MCR[X1]
   auto nmc = this->mVssX1.MCR().Size();
   #pragma omp parallel
   {
      #pragma omp for schedule(dynamic) reduction(ComplexMidasMatrix_Add : result)
      for(In imc=I_0; imc<nmc; ++imc)
      {
         LOGCALL("mc_x1 loop");

         const auto& mc_x1 = this->mVssX1.MCR().GetModeCombi(imc);

         if (  this->DoIo(I_10)
            )
         {
            Mout  << "    MC: " << mc_x1 << std::endl;   // DEBUG
         }
         const auto& mcl = mc_x1.Size();
         for(In imodeidx=I_0; imodeidx<mcl; ++imodeidx)
         {
            const auto& imode = mc_x1.Mode(imodeidx);

            // Skip if not found
            if (  aModes.find(imode) == aModes.end()
               )
            {
               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << "    Skip calculation for imode = " << imode << std::endl; // DEBUG
               }
               continue;
            }

            // Make MC with imode removed
            auto mc_i = mc_x1;
            mc_i.RemoveMode(imode);

            if (  this->DoIo(I_10)
               )
            {
               Mout  << "    imode = " << imode << "\n"
                     << "    Sub-MC_i: " << mc_i << std::endl; // DEBUG
            }

            In tensor_idx_i = -I_1;
            ModeCombiOpRange::const_iterator it_mc_i;
            if (  !this->mVssI.MCR().Find(mc_i, it_mc_i, tensor_idx_i)
               )
            {
               // This should only happen if we are using xCS for the removed mode
               if (  ext_range_modes.empty()
                  || (  !ext_range_modes.empty()
                     && ext_range_modes.find(imode) == ext_range_modes.end()
                     )
                  )
               {
                  Mout  << " ERROR: Did not find MC: " << mc_i << " in MCR[I]." << std::endl;
                  MIDASERROR("LinearRasTdHEom::ConstructAMatrix: Mode combi not found in included configurations!");
               }
               // If this is allowed to happen, just skip the MC
               else
               {
                  continue;
               }
            }

            const auto& c_i = aCoefs.GetModeCombiData(tensor_idx_i);

            if (  this->DoIo(I_10)
               )
            {
               Mout  << "    Tensor index i: " << tensor_idx_i << "\n"
                     << "    Tensor dims: " << c_i.ShowDims() << std::endl;
            }

            auto nelem_i = this->NAct(imode) - I_1;
            auto off_i = aOffsets.at(imode);

            // Off-diagonal (exploit Hermiticity)
            for(In jmodeidx=imodeidx+I_1; jmodeidx<mcl; ++jmodeidx)
            {
               const auto& jmode = mc_x1.Mode(jmodeidx);

               // Skip if not found
               if (  aModes.find(jmode) == aModes.end()
                  )
               {
                  if (  this->DoIo(I_10)
                     )
                  {
                     Mout  << "    Skip calculation for jmode = " << jmode << std::endl; // DEBUG
                  }
                  continue;
               }
            
               // Make MC with jmode removed
               auto mc_j = mc_x1;
               mc_j.RemoveMode(jmode);

               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << "    jmode = " << jmode << "\n"
                        << "    Sub-MC_j: " << mc_j << std::endl; // DEBUG
               }

               In tensor_idx_j = -I_1;
               ModeCombiOpRange::const_iterator it_mc_j;
               if (  !this->mVssI.MCR().Find(mc_j, it_mc_j, tensor_idx_j)
                  )
               {
                  // This should only happen if we are using xCS for the removed mode
                  if (  ext_range_modes.empty()
                     || (  !ext_range_modes.empty()
                        && ext_range_modes.find(jmode) == ext_range_modes.end()
                        )
                     )
                  {
                     Mout  << " ERROR: Did not find MC: " << mc_j << " in MCR[I]." << std::endl;
                     MIDASERROR("LinearRasTdHEom::ConstructAMatrix: Mode combi not found in included configurations!");
                  }
                  // If this is allowed to happen, just skip the MC
                  else
                  {
                     continue;
                  }
               }

               const auto& c_j = aTransCoefs.GetModeCombiData(tensor_idx_j);

               if (  this->DoIo(I_10)
                  )
               {
                  Mout  << "    Tensor index j: " << tensor_idx_j << "\n"
                        << "    Tensor dims: " << c_j.ShowDims() << std::endl;
               }


               // Constract to get transpose of what we need to add
               // Keep jmode in c_i and imode in c_j
               auto transpose_tens_result = detail::ContractAllButOne(c_i, c_j, mc_i.IdxNrForMode(jmode), mc_j.IdxNrForMode(imode));
               auto* trsp_data_ptr = static_cast<SimpleTensor<param_t>*>(transpose_tens_result.GetTensor())->GetData();

               if (  this->DoIo(I_12)
                  )
               {
                  Mout  << "   c_i:\n" << c_i << "\n"
                        << "   c_j:\n" << c_j << "\n"
                        << "   Keep index " << mc_i.IdxNrForMode(jmode) << " in c_i and index " << mc_j.IdxNrForMode(imode) << " in c_j\n"
                        << "   Result of c_i, c_j contraction:\n" << transpose_tens_result << "\n"
                        << std::flush;
               }

               // Add data
               auto nelem_j = this->NAct(jmode) - I_1;
               auto off_j = aOffsets.at(jmode);

               if (  this->DoIo(I_12)
                  )
               {
                  Mout  << " A before adding tmp result:\n" << result << std::endl;
               }
               // Add to A^{m_i, m_j}_{v w}
               // We have the transpose result, i.e. row-major => col-major.
               for(In w=I_0; w<nelem_j; ++w)
               {
                  for(In v=I_0; v<nelem_i; ++v)
                  {
                     result[v + off_i][w + off_j] += *trsp_data_ptr;
                     result[w + off_j][v + off_i] += midas::math::Conj(*trsp_data_ptr);
                     ++trsp_data_ptr;
                  }
               }
               if (  this->DoIo(I_12)
                  )
               {
                  Mout  << " A after adding tmp result:\n" << result << std::endl;
               }
            }
         }
      }
   }  /* end parallel region */

   // Set diagonal elements
   for(const auto& m : aModes)
   {
      auto diag_elem = aDiag.at(m);
      auto nelem_i = this->NAct(m) - I_1;
      auto off_i = aOffsets.at(m);
      for(In elem_i = I_0; elem_i<nelem_i; ++elem_i)
      {
         result[off_i + elem_i][off_i + elem_i] = diag_elem;
      }
   }

//   Mout  << "    A matrix:\n" << result << std::endl;  // DEBUG

   return result;
}

/**
 * Construct B vector for variational optimization of g
 *
 * @note
 *    Cannot be const at this point due to the use of mTransformer
 *
 * @param aT            Time
 * @param aCoefs        C coefficients
 * @param aModes        The modes for which we get non-zero blocks in the A matrix
 * @param aSize         Dimension of vector
 * @param aOffsets      Offset in matrix for mode
 * @return
 *    A matrix
 **/
typename LinearRasTdHEom::bvec_t LinearRasTdHEom::ConstructBVector
   (  step_t aT
   ,  const coefs_t& aCoefs
   ,  const std::set<In>& aModes
   ,  In aSize
   ,  const std::map<In, In>& aOffsets
   )  const
{
   LOGCALL("calls");

   bool non_ortho = this->CalcDef()->GetMethodInfo().template At<bool>("GOPTNONORTHOPROJECTOR");
   const auto& ext_range_modes = this->CalcDef()->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");
   bool v3_oos = this->CalcDef()->GetMethodInfo().template At<bool>("BVECTORV3OOSTRANSFORM");
   bool check = this->CalcDef()->GetMethodInfo().template At<bool>("CHECKBVECTOR");

   // Init result vector
   bvec_t v3_result;
   bvec_t hc_result;

   /*********************************************************
    * Dummy impl. Do out-of-space transform for <w_x|H|psi>
    ********************************************************/
   if (  v3_oos
      || check
      )
   {
      LOGCALL("v3 oos");
      MidasWarning("MCTDH[n]: Using slow out-of-space transform for constructing B vector!");

      // Resize vector
      v3_result.SetNewSize(aSize);
      v3_result.Zero();

      // Construct n+1 level transformer
      // Niels: We construct a new one every time. This is sub-optimal, but we don't want to clear the intermediates of mTransformer.
      auto exci_level = std::min(this->MaxExciLevel() + I_1, this->NDof());

      if (  this->DoIo(I_10)
         )
      {
         Mout  << " Initializing OOS transformer of exci level: " << exci_level << std::endl;
      }
      transformer_t oos_trf(this->CalcDef(), this->Oper().size());
      In iop=I_0;
      for(const auto& op : this->Oper())
      {
         oos_trf.InitializeV3(exci_level, op.Oper().McLevel(), iop++);
      }
   
      // Make input and output vectors
      // (n+1)-order amplitudes will be ZeroTensor%s
      vec_t vec_in(this->mVssIpX1, this->mNModalParams, BaseTensor<param_t>::typeID::ZERO);
      In idx_i = I_0;
      for(const auto& mc_i : this->mVssI.MCR())
      {
         In idx_ipx1 = -I_1;
         ModeCombiOpRange::const_iterator it_mc;
         if (  !this->mVssIpX1.MCR().Find(mc_i, it_mc, idx_ipx1)
            )
         {
            MIDASERROR("MC in MCR[I] must be included in MCR[I+X1]!");
         }

         vec_in.Coefs().GetModeCombiData(idx_ipx1) = aCoefs.GetModeCombiData(idx_i++);
      }

      coefs_t vec_out(this->mVssX1, BaseTensor<param_t>::typeID::ZERO);
   
      // Do transform
      oos_trf.Transform(aT, vec_in, vec_out, this->Integrals(), this->mVssIpX1, this->mVssX1, false, true);

      // Do inverse-overlap transforms
      if (  non_ortho
         )
      {
         this->PerformInverseOverlapTransforms(vec_out, this->mVssX1);
      }

      // Do contractions between vec_out and aCoefs to get B vector
      for(const auto& mc_oos : this->mVssX1.MCR())
      {
         // Loop over modes
         for(const auto& m : mc_oos.MCVec())
         {
            if (  aModes.find(m) == aModes.end()
               )
            {
               continue;
            }
            
            auto mc_c = mc_oos;
            mc_c.RemoveMode(m);

            ModeCombiOpRange::const_iterator it_c;
            In c_idx = I_0;                              
            if (  !this->mVssI.MCR().Find(mc_c, it_c, c_idx)
               )
            {
               // This should only happen if we are using xCS for the removed mode
               if (  ext_range_modes.empty()
                  || (  !ext_range_modes.empty()
                     && ext_range_modes.find(m) == ext_range_modes.end()
                     )
                  )
               {
                  Mout  << " ERROR: Did not find MC: " << mc_c << " in MCR[I]." << std::endl;
                  MIDASERROR("LinearRasTdHEom::ConstructBVector: Mode combi not found in included configurations!");
               }
               // If this is allowed to happen, just skip the mode
               else
               {
                  continue;
               }
            }

            ModeCombiOpRange::const_iterator it_oos;
            In oos_idx = I_0;
            if (  !this->mVssX1.MCR().Find(mc_oos, it_oos, oos_idx)
               )
            {
               MIDASERROR("Out-of-space Hamiltonian matrix element needed for down contraction does not exist!");
            }

            const auto& c = aCoefs.GetModeCombiData(c_idx);
            const auto& h = vec_out.GetModeCombiData(oos_idx);
            In mode_idx = mc_oos.IdxNrForMode(m);
            auto tensor_result = detail::ContractAllInRight(h, c, mode_idx); // calculate complex conjugate result
            auto* result_data = static_cast<SimpleTensor<param_t>*>(tensor_result.GetTensor())->GetData();
            auto len = tensor_result.GetDims()[0];

            // add data
            auto off = aOffsets.at(m);
            for(In i=I_0; i<len; ++i)
            {
               v3_result[off + i] += midas::math::Conj(*(result_data++));
            }
         }
      }

      // Return if not debugging
      if (  !check
         )
      {
         return v3_result;
      }
   }

   /*********************************************************
    * Hard-coded implementation
    ********************************************************/
   if (  !v3_oos
      || check
      )
   {
      LOGCALL("hard-coded");

      if (  non_ortho
         )
      {
         MidasWarning("Non-ortho projector not implemented for hard-coded B-vector.");
      }

      // Resize result
      hc_result.SetNewSize(aSize);
      hc_result.Zero();
      bvec_t tmp_hc_result(hc_result.Size());
      tmp_hc_result.Zero();

      // Get integrals and N_DOF
      const auto& ints = this->Integrals();
      const auto ndof = this->NDof();

      // Function to check if we skip a given mode combi
      auto skip_ctrs_for_mc_x1 = [](In iterm, const std::vector<In>& ctrs, const OpDef& opdef, const ModeCombi& mc_x1)
      {
         for(In i=I_0; i<ctrs.size(); ++i)
         {
            auto m = opdef.ModeForFactor(iterm, i);
            bool m_incl = mc_x1.IncludeMode(m);
            if (  (  ctrs[i] == -I_1 && m_incl     // Skip if we down contract a mode which must be included in the bra MC
                  )
               ||
                  (  ctrs[i] == I_1 && !m_incl     // Skip if we up contract a mode which is not included in the bra MC
                  )
               )
            {
               return true;
            }
         }
         return false;
      };

      // Function to check if we skip a given result mode
      auto skip_result_mode = [](In iterm, const std::vector<In>& ctrs, const OpDef& opdef, In result_mode)
      {
         for(In i=I_0; i<ctrs.size(); ++i)
         {
            if (  opdef.ModeForFactor(iterm, i) == result_mode
               && ctrs[i] == -I_1
               )
            {
               return true;
            }
         }
         return false;
      };

      if (  this->DoIo(I_10)
         )
      {
         Mout  << " ### Hard-coded B-vector ###" << std::endl;
      }

      In n_xcs = ext_range_modes.size();
      auto mcr_size = this->mVssX1.MCR().Size();

      // Loop over operators in the Hamiltonian
      for(In iop=I_0; iop<ints.size(); ++iop)
      {
         const auto& ints_i = ints[iop];
         const auto& opdef = ints_i.Oper().Oper();
         auto nterms = opdef.Nterms();

         if (  this->DoIo(I_10)
            )
         {
            Mout  << "   iop:          " << iop << "\n"
                  << "   nterms:       " << nterms << "\n"
                  << "   mcr_size:     " << mcr_size << "\n"
                  << std::flush;
         }

         #pragma omp parallel
         {
            In nfac;          // Number of factors in a given operator term
            In mcl;           // Mode-combination level of m in MCR[X1]
            In result_mode;   // The mode, m, which the result is calculated for
            In oper_mode;     // Mode for operator
            In aux_oper;      // Operator number
            In c_bra_idx;     // Index of bra coefs tensor
            In c_ket_idx;     // Index of ket coefs tensor
            In off;           // Offset in result vector
            ModeCombi bra_mc; // Mode combi for bra state
            ModeCombi ket_mc; // Mode combi for ket state

            // Do a collapsed OpenMP for loop over Hamiltonian terms and MCs in MCR[X1]
            #pragma omp for schedule(dynamic) collapse(2) reduction(ComplexMidasVector_Add : tmp_hc_result)
            for(In iterm=I_0; iterm<nterms; ++iterm)
            {
               // Loop over one-mode excited MCs
               for(In imc=I_0; imc<mcr_size; ++imc)
               {
                  const auto& mc_x1 = this->mVssX1.MCR().GetModeCombi(imc);
                  mcl = mc_x1.Size();

                  // Get number of factors in term
                  nfac = opdef.NfactorsInTerm(iterm);

                  // Get contractions to carry out
                  const auto& ctr_combis = ext_range_modes.empty() ? this->ExciContractionCombinations(nfac) : this->GenContractionCombinations(nfac);   // NB: extend if implementing dynamic MCR

                  if (  this->DoIo(I_10)
                     )
                  {
                     Mout  << " * iterm:        " << iterm << "\n"
                           << "   mc_x1:        " << mc_x1 << "\n"
                           << "   mcl:          " << mcl << "\n"
                           << "   nfac:         " << nfac << "\n"
                           << "   n_MR:        " << n_xcs << "\n"
                           << "   ctr_combis:   " << ctr_combis << "\n"
                           << std::flush;
                  }

                  // Loop over contractions
                  for(const auto& ctrs : ctr_combis)
                  {
                     if (  this->DoIo(I_10)
                        )
                     {
                        Mout  << "    * ctrs:            " << ctrs << std::endl;
                     }
                     // Skip contractions if the excitation level is larger than n+1
                     In exci_level = std::accumulate(ctrs.begin(), ctrs.end(), I_0);
                     if (  this->DoIo(I_10)
                        )
                     {
                        Mout  <<  "      exci_level:      " << exci_level << std::endl;
                     }

                     if (  exci_level > (this->MaxExciLevel() + I_1)
                        || exci_level < (I_1 - n_xcs)
                        )
                     {
                        if (  this->DoIo(I_10)
                           )
                        {
                           Mout  << "   == SKIP ctrs due to exci level ==" << std::endl;
                        }
                        continue;
                     }

                     // Check if the current contraction combination gives a contribution here
                     if (  skip_ctrs_for_mc_x1(iterm, ctrs, opdef, mc_x1)
                        )
                     {
                        if (  this->DoIo(I_10)
                           )
                        {
                           Mout  << "   == SKIP these contractions for MC ==" << std::endl;
                        }
                        continue;
                     }

                     // Loop over result mode, m
                     for(In imodeidx=I_0; imodeidx<mcl; ++imodeidx)
                     {
                        result_mode = mc_x1.Mode(imodeidx);

                        if (  this->DoIo(I_10)
                           )
                        {
                           Mout  << "       * result_mode:     " << result_mode << std::endl;
                        }

                        // Skip if we do not need the result for this mode
                        if (  aModes.find(result_mode) == aModes.end()
                           )
                        {
                           if (  this->DoIo(I_10)
                              )
                           {
                              Mout  << "   == SKIP result mode (not in aModes) ==" << std::endl;
                           }
                           continue;
                        }

                        // 0) Skip if result_mode is down contracted
                        if (  skip_result_mode(iterm, ctrs, opdef, result_mode)
                           )
                        {
                           if (  this->DoIo(I_10)
                              )
                           {
                              Mout  << "   == SKIP result mode (is down contracted) ==" << std::endl;
                           }
                           continue;
                        }

                        // 1) Construct MCs for bra and ket wfs
                        bra_mc = mc_x1;
                        bra_mc.RemoveMode(result_mode);
                        ket_mc = mc_x1;
                        bool rm_xcs_mode = false;
                        for(In i=I_0; i<ctrs.size(); ++i)
                        {
                           if (  ctrs[i] == I_1
                              )
                           {
                              auto m = opdef.ModeForFactor(iterm, i);
                              rm_xcs_mode = rm_xcs_mode || (ext_range_modes.find(m) != ext_range_modes.end());
                              ket_mc.RemoveMode(m);
                           }
                           else if  (  ctrs[i] == -I_1
                                    )
                           {
                              ket_mc.InsertMode(opdef.ModeForFactor(iterm, i));
                           }
                        }

                        if (  this->DoIo(I_10)
                           )
                        {
                           Mout  << "         bra_mc:       " << bra_mc << "\n"
                                 << "         ket_mc:       " << ket_mc << "\n"
                                 << std::boolalpha
                                 << "         rm_xcs_mode:  " << rm_xcs_mode << "\n"
                                 << std::noboolalpha
                                 << std::flush;
                        }
                        
                        // 2) Get the necessary tensor indices
                        ModeCombiOpRange::const_iterator it_bra_mc;
                        if (  !this->mVssI.MCR().Find(bra_mc, it_bra_mc, c_bra_idx)
                           )
                        {
                           // This should only happen if we are using xCS for the removed mode
                           if (  ext_range_modes.empty()
                              || (  !ext_range_modes.empty()
                                 && ext_range_modes.find(result_mode) == ext_range_modes.end()
                                 )
                              )
                           {
                              Mout  << " Bra coefs not found:\n"
                                    << "    Bra MC:   " << bra_mc << "\n"
                                    << "    m:        " << result_mode << "\n"
                                    << std::flush;
                              MIDASERROR("B-vector: Bra coefs not found!");
                           }
                           // If this is allowed to happen, just skip the result mode
                           else
                           {
                              if (  this->DoIo(I_10)
                                 )
                              {
                                 Mout  << "   == SKIP result mode (bra coefs not found) ==" << std::endl;
                              }
                              continue;
                           }
                        }
                        auto c_bra = aCoefs.GetModeCombiData(c_bra_idx);

                        ModeCombiOpRange::const_iterator it_ket_mc;
                        if (  !this->mVssI.MCR().Find(ket_mc, it_ket_mc, c_ket_idx)
                           )
                        {
                           if (  rm_xcs_mode          // If we have removed xCS modes, this may be okay. NB: This check could be harder!
                              || exci_level <= I_0    // If the total excitation level is less than zero, it may be okay as well.
                              )
                           {
                              if (  this->DoIo(I_10)
                                 )
                              {
                                 Mout  << "   == SKIP result mode (ket coefs not found) ==" << std::endl;
                              }
                              continue;
                           }
                           else
                           {
                              Mout  << " Ket coefs not found:\n"
                                    << "    Ket MC:   " << ket_mc << "\n"
                                    << std::flush;
                              MIDASERROR("B-vector: Ket coefs not found!");
                           }
                        }
                        auto c_ket = aCoefs.GetModeCombiData(c_ket_idx);

                        // 3) Perform contractions
                        param_t coef = opdef.Coef(iterm) * ints_i.Oper().Coef(aT);

                        if (  this->DoIo(I_10)
                           )
                        {
                           Mout  << "         init coef:    " << coef << std::endl;
                        }

                        // Down ctrs
                        for(In ifac=I_0; ifac<nfac; ++ifac)
                        {
                           if (  ctrs[ifac] == -I_1
                              )
                           {
                              // Do down ctr
                              oper_mode = opdef.ModeForFactor(iterm, ifac);
                              aux_oper = opdef.OperForFactor(iterm, ifac);
                              const auto& int_mat = ints_i.GetActiveIntegrals(oper_mode, aux_oper);
                              auto ints_array = std::make_unique<param_t[]>(int_mat.Ncols()-1);
                              auto* int_ptr = ints_array.get();
                              for(In i=I_1; i<int_mat.Ncols(); ++i)
                              {
                                 *(int_ptr++) = int_mat[I_0][i];
                              }

                              // Make tensor
                              auto int_tens = NiceSimpleTensor<param_t>(std::vector<unsigned>{unsigned(int_mat.Ncols()-1)}, ints_array.release());
                              
                              // Get idx number and contract
                              In idx = ket_mc.IdxNrForMode(oper_mode);
                              c_ket = c_ket.ContractDown(idx, int_tens);
                              ket_mc.RemoveMode(oper_mode);

                              if (  this->DoIo(I_10)
                                 )
                              {
                                 Mout  << "         Performed down contraction on c_ket.\n"
                                       << "            oper_mode:    " << oper_mode << "\n"
                                       << "            oper:         " << aux_oper << "\n"
                                       << "            tensor idx:   " << idx << "\n"
                                       << "            new ket_mc:   " << ket_mc << "\n"
                                       << std::flush;
                              }
                           }
                        }
                        // Forward/passive ctrs
                        for(In ifac=I_0; ifac<nfac; ++ifac)
                        {
                           if (  ctrs[ifac] == I_0
                              )
                           {
                              // Do passive or forward ctr on c_ket (depending on whether mode is in MC)
                              oper_mode = opdef.ModeForFactor(iterm, ifac);
                              aux_oper = opdef.OperForFactor(iterm, ifac);
                              if (  ket_mc.IncludeMode(oper_mode)
                                 )
                              {
                                 // Forward ctr
                                 In idx = ket_mc.IdxNrForMode(oper_mode);
                                 const auto& int_mat = ints_i.GetActiveIntegrals(oper_mode, aux_oper);
                                 detail::DoForwardContraction(c_ket, int_mat, idx, true);
                                 if (  this->DoIo(I_10)
                                    )
                                 {
                                    Mout  << "         Performed forward contraction on c_ket.\n"
                                          << "            oper_mode:    " << oper_mode << "\n"
                                          << "            oper:         " << aux_oper << "\n"
                                          << "            tensor idx:   " << idx << "\n"
                                          << std::flush;
                                 }
                              }
                              else
                              {
                                 // Passive ctr (scale coef instead of tensor)
                                 coef *= ints_i.GetActiveIntegrals(oper_mode, aux_oper)[I_0][I_0];
                                 if (  this->DoIo(I_10)
                                    )
                                 {
                                    Mout  << "         Performed passive contraction.\n"
                                          << "            oper_mode:    " << oper_mode << "\n"
                                          << "            oper:         " << aux_oper << "\n"
                                          << "            new coef:     " << coef << "\n"
                                          << std::flush;
                                 }
                              }
                           }
                        }
                        // Up ctrs
                        In up_on_result_oper = -I_1;
                        for(In ifac=I_0; ifac<nfac; ++ifac)
                        {
                           if (  ctrs[ifac] == I_1
                              )
                           {
                              oper_mode = opdef.ModeForFactor(iterm, ifac);
                              aux_oper = opdef.OperForFactor(iterm, ifac);
                              // Do up contraction for result_mode, i.e. mode is not included in bra_mc
                              if (  oper_mode == result_mode
                                 )
                              {
                                 // Set oper index for later. Then the final result will be calculated differently.
                                 up_on_result_oper = aux_oper;

                                 if (  this->DoIo(I_10)
                                    )
                                 {
                                    Mout  << "         Set oper for later up contraction of result mode:\n"
                                          << "            oper_mode:    " << oper_mode << "\n"
                                          << "            oper:         " << aux_oper << "\n"
                                          << std::flush;
                                 }
                              }
                              else
                              {
                                 // Do down contraction on bra coefs with conjugate integrals
                                 const auto& int_mat = ints_i.GetActiveIntegrals(oper_mode, aux_oper);
                                 auto ints_array = std::make_unique<param_t[]>(int_mat.Ncols()-1);
                                 auto* int_ptr = ints_array.get();
                                 for(In i=I_1; i<int_mat.Ncols(); ++i)
                                 {
                                    *(int_ptr++) = midas::math::Conj(int_mat[i][I_0]);     // Get conj column instead of row (in case oper is not Hermitian)
                                 }

                                 // Make tensor
                                 auto int_tens = NiceSimpleTensor<param_t>(std::vector<unsigned>{unsigned(int_mat.Ncols()-1)}, ints_array.release());
                                 
                                 // Get idx number and contract
                                 In idx = bra_mc.IdxNrForMode(oper_mode);
                                 c_bra = c_bra.ContractDown(idx, int_tens);
                                 bra_mc.RemoveMode(oper_mode);

                                 if (  this->DoIo(I_10)
                                    )
                                 {
                                    Mout  << "         Performed up contraction / down on bra:\n"
                                          << "            oper_mode:    " << oper_mode << "\n"
                                          << "            oper:         " << aux_oper << "\n"
                                          << "            tensor idx:   " << idx << "\n"
                                          << "            new bra mc:   " << bra_mc << "\n"
                                          << std::flush;
                                 }
                              }
                           }
                        }

                        // 4a) Scale up integrals with dot between tensors and add to result (scale with accumulated coef)
                        if (  up_on_result_oper != -I_1
                           )
                        {
                           if (  ket_mc != bra_mc
                              )
                           {
                              Mout  << " Ket MC:      " << ket_mc << "\n"
                                    << " Bra MC:      " << bra_mc << "\n"
                                    << " result_mode: " << result_mode << "\n"
                                    << std::flush;
                              MIDASERROR("If result mode is up contracted, ket_mc should be equal to bra_mc!");
                           }
                           const auto& int_mat = ints_i.GetActiveIntegrals(result_mode, up_on_result_oper);
                           In len = int_mat.Nrows() - 1;

                           coef *= dot_product(c_bra, c_ket);

                           // add data
                           off = aOffsets.at(result_mode);
                           for(In i=I_0; i<len; ++i)
                           {
                              tmp_hc_result[off + i] += coef*int_mat[i+I_1][I_0];
                           }

                           if (  this->DoIo(I_10)
                              )
                           {
                              Mout  << "         final coef:      " << coef << "\n"
                                    << "   === Added result with up on result oper ===" << std::endl;
                           }
                        }
                        // 4b) Contract all in right to get conj vector and add to result (scale with accumulated coef)
                        else
                        {
                           auto conj_result = detail::ContractAllInRight(c_ket, c_bra, ket_mc.IdxNrForMode(result_mode));
                           In len = conj_result.GetDims()[0];
                           auto* conj_result_ptr = static_cast<const SimpleTensor<param_t>* const>(conj_result.GetTensor())->GetData();

                           // add data
                           off = aOffsets.at(result_mode);
                           for(In i=I_0; i<len; ++i)
                           {
                              tmp_hc_result[off + i] += coef*midas::math::Conj(*(conj_result_ptr++));
                           }

                           if (  this->DoIo(I_10)
                              )
                           {
                              Mout  << "         final coef:      " << coef << "\n"
                                    << "   === Added result ===" << std::endl;
                           }
                        }
                     }
                  }
               }
            }
         }

         // Add to full result
         hc_result += tmp_hc_result;
         tmp_hc_result.Zero();
      }

      // Debug if we are checking the implementation
      if (  check
         )
      {
         // Subtract the two results
         auto dot = Dot(v3_result, hc_result);
         auto hc_norm = hc_result.Norm();
         bvec_t diff = hc_result - v3_result;

         // Get diff norm and norm of V3 result
         auto diff_norm = diff.Norm();
         auto v3_norm = v3_result.Norm();
         auto angle = std::acos(std::real(dot)/(v3_norm*hc_norm));
         auto angle_div_pi = angle / C_PI;

         size_t prec = 100;

         // Print the difference
         Mout  << " ############### MCTDH[n,V] B-vector check ##################\n"
               << "    Diff norm:      " << diff_norm << "\n"
               << "    V3 Result norm: " << v3_norm << "\n"
               << "    HC Result norm: " << hc_norm << "\n"
               << "    Dot product:    " << dot << "\n"
               << "    |dot|:          " << std::abs(dot) << "\n"
               << "    Rel. diff.:     " << diff_norm / v3_norm << "\n"
               << "    Thresh:         " << midas::util::NumericPrecisionInfo<real_t>::Epsilon() * prec << "\n" 
               << "    Angle:          " << angle_div_pi << " * pi\n"
               << " ############################################################\n\n"
               << std::flush;

         // Issue warning if diff norm is large compared to result norm.
         if (  !libmda::numeric::float_numeq_zero(diff_norm, v3_norm, prec)
            )
         {
            MidasWarning("B-vector check: Relative difference is larger than expected. Check the numbers!", true);
            Mout  << " HC: " << hc_result << "\n"
                  << " V3: " << v3_result << "\n"
                  << std::endl;
         }

         // Return the V3 result, which should be correct!
         return v3_result;
      }
      // Else, return the result
      else
      {
         return hc_result;
      }
   }

   // The function should have returned by now
   MIDASERROR("This should never happen!");
   return v3_result;
}

/**
 * Get combinations of contractions that add up to excitation.
 * The result vector contains integers defining contractions: 1 (up), 0 (forward/passive), -1 (down)
 *
 * @note The implementation is hard-coded for each combination level to gain efficiency.
 *
 * @param aNFac      Number of one-mode operators
 * @return
 *    All possible combinations (including permutations) of contraction types that add up to excitation.
 **/
const std::vector<std::vector<In>>& LinearRasTdHEom::ExciContractionCombinations
   (  In aNFac
   )  const
{
   using result_t = std::vector<std::vector<In>>;

   switch   (  aNFac
            )
   {
      case I_1:
      {
         // For one operator, it must be an up contraction
         static const result_t ex_cmbs_1 = 
         {  { I_1 }                       // One-mode excited
         };

         return ex_cmbs_1;
         break;
      }
      case I_2:
      {
         // For two operators, we have one up and one forward/passive, or two up
         static const result_t ex_cmbs_2 =
         {  { I_1, I_0 }                  // One-mode excited
         ,  { I_0, I_1 }
         ,  { I_1, I_1 }                  // Two-mode excited
         };

         return ex_cmbs_2;
         break;
      }
      case I_3:
      {
         // For three operators, we have one up and two forward/passive, or two up and one down, or higher-order excitations
         static const result_t ex_cmbs_3 =
         {  { I_1, I_0, I_0 }             // One-mode excited
         ,  { I_0, I_1, I_0 }
         ,  { I_0, I_0, I_1 }
         ,  { I_1,   I_1, -I_1 }
         ,  { I_1,  -I_1,  I_1 }
         ,  {-I_1,   I_1,  I_1 }
         ,  { I_1, I_1, I_0 }             // Two-mode excited
         ,  { I_1, I_0, I_1 }
         ,  { I_0, I_1, I_1 }
         ,  { I_1, I_1, I_1 }             // Three-mode excited
         };

         return ex_cmbs_3;
         break;
      }
      case I_4:
      {
         // Etc.
         static const result_t ex_cmbs_4 =
         {  { I_1, I_0, I_0, I_0 }        // One-mode excited
         ,  { I_0, I_1, I_0, I_0 }
         ,  { I_0, I_0, I_1, I_0 }
         ,  { I_0, I_0, I_0, I_1 }
         ,  { I_1,   I_1, -I_1,  I_0 }
         ,  { I_1,  -I_1,  I_1,  I_0 }
         ,  {-I_1,   I_1,  I_1,  I_0 }
         ,  { I_1,   I_1,  I_0, -I_1 }
         ,  { I_1,  -I_1,  I_0,  I_1 }
         ,  {-I_1,   I_1,  I_0,  I_1 }
         ,  { I_1,   I_0,  I_1, -I_1 }
         ,  { I_1,   I_0, -I_1,  I_1 }
         ,  {-I_1,   I_0,  I_1,  I_1 }
         ,  { I_0,   I_1,  I_1, -I_1 }
         ,  { I_0,   I_1, -I_1,  I_1 }
         ,  { I_0,  -I_1,  I_1,  I_1 }
         ,  { I_1, I_1, I_0, I_0 }        // Two-mode excited
         ,  { I_1, I_0, I_1, I_0 }
         ,  { I_1, I_0, I_0, I_1 }
         ,  { I_0, I_1, I_1, I_0 }
         ,  { I_0, I_1, I_0, I_1 }
         ,  { I_0, I_0, I_1, I_1 }
         ,  { I_1, I_1, I_1, -I_1 }
         ,  { I_1, I_1, -I_1, I_1 }
         ,  { I_1, -I_1, I_1, I_1 }
         ,  { -I_1, I_1, I_1, I_1 }
         ,  { I_1, I_1, I_1, I_0 }        // Three-mode excited
         ,  { I_1, I_1, I_0, I_1 }
         ,  { I_1, I_0, I_1, I_1 }
         ,  { I_0, I_1, I_1, I_1 }
         ,  { I_1, I_1, I_1, I_1 }        // Four-mode excited
         };

         return ex_cmbs_4;
         break;
      }
      // Continue this way. Add one up ctr first, then add pairs of up+down and fill with passive/forwards. Then add two ups, and continue.
      default:
      {
         MIDASERROR("Not implemented for " + std::to_string(aNFac) + "-mode coupling terms!");
         static const result_t r;
         return r;
      }
   }
}

/**
 * Get combinations of contractions that shift excitation between modes and is not purely de-excited (MCR[I] is always closed under one-mode deexcitation)!
 * The result vector contains integers defining contractions: 1 (up), 0 (forward/passive), -1 (down)
 *
 * @note The implementation is hard-coded for each combination level to gain efficiency.
 *
 * @param aNFac      Number of one-mode operators
 * @return
 *    All possible combinations (including permutations) of contraction types that add up to shifting excitation.
 **/
const std::vector<std::vector<In>>& LinearRasTdHEom::GenContractionCombinations
   (  In aNFac
   )  const
{
   using result_t = std::vector<std::vector<In>>;

   switch   (  aNFac
            )
   {
      case I_1:
      {
         // For one operator, it must be an up contraction. MCR[I] is always closed under one-mode de-excitation.
         static const result_t gen_cmbs_1 = 
         {  { I_1 }                       // One-mode excited
         };

         return gen_cmbs_1;
         break;
      }
      case I_2:
      {
         // For two operators, we have one up and one forward/passive, or two up
         static const result_t gen_cmbs_2 =
         {  {-I_1, I_1 }                  // Not excited, but shifted
         ,  { I_1,-I_1 }
         ,  { I_1, I_0 }                  // One-mode excited
         ,  { I_0, I_1 }
         ,  { I_1, I_1 }                  // Two-mode excited
         };

         return gen_cmbs_2;
         break;
      }
      case I_3:
      {
         // For three operators, we have one up and two forward/passive, or two up and one down, or higher-order excitations
         static const result_t gen_cmbs_3 =
         {  {-I_1,-I_1, I_1 }             // One-mode de-excited and shifted
         ,  {-I_1, I_1,-I_1 }
         ,  { I_1,-I_1,-I_1 }
         ,  { I_0, I_1,-I_1 }             // Not excited, but shifted
         ,  { I_0,-I_1, I_1 }
         ,  { I_1,-I_1, I_0 }
         ,  { I_1, I_0,-I_1 }
         ,  {-I_1, I_0, I_1 }
         ,  {-I_1, I_1, I_0 }
         ,  { I_1, I_0, I_0 }             // One-mode excited
         ,  { I_0, I_1, I_0 }
         ,  { I_0, I_0, I_1 }
         ,  { I_1,   I_1, -I_1 }
         ,  { I_1,  -I_1,  I_1 }
         ,  {-I_1,   I_1,  I_1 }
         ,  { I_1, I_1, I_0 }             // Two-mode excited
         ,  { I_1, I_0, I_1 }
         ,  { I_0, I_1, I_1 }
         ,  { I_1, I_1, I_1 }             // Three-mode excited
         };

         return gen_cmbs_3;
         break;
      }
      case I_4:
      {
         // Etc.
         static const result_t gen_cmbs_4 =
         {  { I_1,-I_1,-I_1,-I_1 }        // Two-mode de-excited and shifted
         ,  {-I_1, I_1,-I_1,-I_1 }
         ,  {-I_1,-I_1, I_1,-I_1 }
         ,  {-I_1,-I_1,-I_1, I_1 }
         ,  {-I_1,-I_1, I_0, I_1 }        // One-mode de-excited and shifted
         ,  {-I_1,-I_1, I_1, I_0 }
         ,  {-I_1, I_0, I_0, I_1 }
         ,  {-I_1, I_0, I_1, I_0 }
         ,  { I_0,-I_1, I_0, I_1 }
         ,  { I_0,-I_1, I_1, I_0 }
         ,  {-I_1, I_1,-I_1, I_0 }
         ,  {-I_1, I_1, I_0,-I_1 }
         ,  { I_1,-I_1,-I_1, I_0 }
         ,  { I_1,-I_1, I_0,-I_1 }
         ,  { I_0, I_1,-I_1,-I_1 }
         ,  { I_1, I_0,-I_1,-I_1 }
         ,  {-I_1, I_1, I_0, I_0 }        // Not excited, but shifted
         ,  { I_1,-I_1, I_0, I_0 }
         ,  { I_0,-I_1, I_0, I_1 }
         ,  { I_0,-I_1, I_1, I_0 }
         ,  {-I_1, I_0, I_0, I_1 }
         ,  {-I_1, I_0, I_1, I_0 }
         ,  { I_1, I_0,-I_1, I_0 }
         ,  { I_1, I_0, I_0,-I_1 }
         ,  { I_0, I_1,-I_1, I_0 }
         ,  { I_0, I_1, I_0,-I_1 }
         ,  { I_0, I_0,-I_1, I_1 }
         ,  { I_0, I_0, I_1,-I_1 }
         ,  { I_1, I_1,-I_1,-I_1 }
         ,  { I_1,-I_1,-I_1, I_1 }
         ,  {-I_1,-I_1, I_1, I_1 }
         ,  { I_1,-I_1, I_1,-I_1 }
         ,  {-I_1, I_1,-I_1, I_1 }
         ,  {-I_1, I_1, I_1,-I_1 }
         ,  { I_1, I_0, I_0, I_0 }        // One-mode excited
         ,  { I_0, I_1, I_0, I_0 }
         ,  { I_0, I_0, I_1, I_0 }
         ,  { I_0, I_0, I_0, I_1 }
         ,  { I_1,   I_1, -I_1,  I_0 }
         ,  { I_1,  -I_1,  I_1,  I_0 }
         ,  {-I_1,   I_1,  I_1,  I_0 }
         ,  { I_1,   I_1,  I_0, -I_1 }
         ,  { I_1,  -I_1,  I_0,  I_1 }
         ,  {-I_1,   I_1,  I_0,  I_1 }
         ,  { I_1,   I_0,  I_1, -I_1 }
         ,  { I_1,   I_0, -I_1,  I_1 }
         ,  {-I_1,   I_0,  I_1,  I_1 }
         ,  { I_0,   I_1,  I_1, -I_1 }
         ,  { I_0,   I_1, -I_1,  I_1 }
         ,  { I_0,  -I_1,  I_1,  I_1 }
         ,  { I_1, I_1, I_0, I_0 }        // Two-mode excited
         ,  { I_1, I_0, I_1, I_0 }
         ,  { I_1, I_0, I_0, I_1 }
         ,  { I_0, I_1, I_1, I_0 }
         ,  { I_0, I_1, I_0, I_1 }
         ,  { I_0, I_0, I_1, I_1 }
         ,  { I_1, I_1, I_1, -I_1 }
         ,  { I_1, I_1, -I_1, I_1 }
         ,  { I_1, -I_1, I_1, I_1 }
         ,  { -I_1, I_1, I_1, I_1 }
         ,  { I_1, I_1, I_1, I_0 }        // Three-mode excited
         ,  { I_1, I_1, I_0, I_1 }
         ,  { I_1, I_0, I_1, I_1 }
         ,  { I_0, I_1, I_1, I_1 }
         ,  { I_1, I_1, I_1, I_1 }        // Four-mode excited
         };

         return gen_cmbs_4;
         break;
      }
      // Continue this way. 
      default:
      {
         MIDASERROR("Not implemented for " + std::to_string(aNFac) + "-mode coupling terms!");
         static const result_t r;
         return r;
      }
   }
}


/**
 * Perform one-mode transformations with inverse overlap matrices
 *
 * @param arCoefs       The coefs to transform
 * @param aVssI         The state space of the coefs
 **/
void LinearRasTdHEom::PerformInverseOverlapTransforms
   (  coefs_t& arCoefs
   ,  const VccStateSpace& aVssI
   )  const
{
   auto tmp = arCoefs;
   tmp.Zero();

   std::vector<mat_t> inv_overlaps_m;
   for(In idof=I_0; idof<this->NDof(); ++idof)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(idof) == this->ElectronicDofNr()
         )
      {
         continue;
      }

      inv_overlaps_m.clear();
      inv_overlaps_m.resize(this->NDof());

      inv_overlaps_m[idof] = this->InverseOverlapMatrix(idof);

      this->mTransformer.OneModeTransform(arCoefs, tmp, inv_overlaps_m, aVssI, false);
      arCoefs = tmp;
   }
}

/**
 * Return zero vector of correct shape.
 *
 * @return
 *    Zero vector
 **/
typename LinearRasTdHEom::vec_t LinearRasTdHEom::ShapedZeroVectorImpl
   (
   )  const
{
   return vec_t(this->mVssI, this->mNModalParams);
}

/**
 * Auto-correlation function
 *
 * @param aT      Time (t)
 * @param aY      WF (y)
 *
 * @return
 *    S(t)
 **/
typename LinearRasTdHEom::param_t LinearRasTdHEom::CalculateAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   auto trans = aY.Coefs();
   auto tmp = aY.Coefs();
   tmp.Zero();

   // Perform forward contractions with modal overlaps for all modes
   std::vector<mat_t> overlaps;
   overlaps.reserve(this->NDof());
   for(In idof=I_0; idof<this->NDof(); ++idof)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(idof) == this->ElectronicDofNr()
         )
      {
         continue;
      }

      auto nact = this->NAct(idof);
      auto nbas = this->NBas(idof);
      auto off = this->ModalOffsets()[idof];

      // Construct overlap matrix
      mat_t overlap_m(nact);

      // Calculate overlaps
      for(In v=I_0; v<nact; ++v)
      {
         for(In w=I_0; w<nact; ++w)
         {
            param_t val(0.);
            for(In ibas=I_0; ibas<nbas; ++ibas)
            {
               val += midas::math::Conj(this->InitialWaveFunction().Modals()[off[v]+ibas]) * aY.Modals()[off[w]+ibas];
            }
            overlap_m[v][w] = val;
         }
      }

      overlaps.clear();
      overlaps.resize(this->NDof());
      overlaps[idof] = std::move(overlap_m);

      this->mTransformer.OneModeTransform(trans, tmp, overlaps, this->mVssI, false);
      trans = tmp;
   } 


   return Dot(this->InitialWaveFunction().Coefs(), trans);
}

/**
 * Half-time auto-correlation function
 *
 * @param aT      Time (t)
 * @param aY      WF (y)
 *
 * @return
 *    S1/2(t)
 **/
typename LinearRasTdHEom::param_t LinearRasTdHEom::CalculateHalfTimeAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   auto trans = aY.Coefs();
   auto tmp = aY.Coefs();
   tmp.Zero();

   // Perform forward contractions with modal overlaps for all modes
   std::vector<mat_t> overlaps;
   overlaps.reserve(this->NDof());
   for(In idof=I_0; idof<this->NDof(); ++idof)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(idof) == this->ElectronicDofNr()
         )
      {
         continue;
      }

      auto nact = this->NAct(idof);
      auto nbas = this->NBas(idof);
      auto off = this->ModalOffsets()[idof];

      // Construct overlap matrix
      mat_t overlap_m(nact);

      // Calculate strange overlaps
      for(In v=I_0; v<nact; ++v)
      {
         for(In w=I_0; w<nact; ++w)
         {
            param_t val(0.);
            for(In ibas=I_0; ibas<nbas; ++ibas)
            {
               val += aY.Modals()[off[v]+ibas] * aY.Modals()[off[w]+ibas];
            }
            overlap_m[v][w] = val;
         }
      }

      overlaps.clear();
      overlaps.resize(this->NDof());
      overlaps[idof] = std::move(overlap_m);

      this->mTransformer.OneModeTransform(trans, tmp, overlaps, this->mVssI, false);
      trans = tmp;
   } 

   return detail::NonConjugateDot(aY.Coefs(), trans);
}

/**
 * Expectation values. Full and active-terms.
 *
 * @param aIntegrals
 * @param aY
 *
 * @return
 *    Vector of expectation values
 **/
std::vector<typename LinearRasTdHEom::param_t> LinearRasTdHEom::ExpectationValueImpl
   (  const integrals_t& aIntegrals
   ,  const vec_t& aY
   )  const
{
   LOGCALL("calls");

   const auto& opdef = aIntegrals.Oper().Oper();
   auto nterms = opdef.Nterms();
   auto ndof = this->NDof();

   // Init result (as MidasVector to allow OpenMP)
   GeneralMidasVector<param_t> tmp_result(ndof + I_1);

   #pragma omp parallel
   {
      // Init temporary
      coefs_t tmp = aY.Coefs();
      coefs_t trans_coefs;
      param_t fac(0.);
      In nfac;
      In mode;
      In oper;
      mat_t ints;
      std::vector<mat_t> ints_vec;

      #pragma omp for schedule(dynamic) reduction(ComplexMidasVector_Add : tmp_result)
      for(In iterm = I_0; iterm<nterms; ++iterm)
      {
         nfac = opdef.NfactorsInTerm(iterm);
         trans_coefs = aY.Coefs();
         for(In ifac=I_0; ifac<nfac; ++ifac)
         {
            mode = opdef.ModeForFactor(iterm, ifac);
            oper = opdef.OperForFactor(iterm, ifac);
            ints.SetNewSize(this->NAct(mode), this->NAct(mode));
            ints = aIntegrals.GetActiveIntegrals(mode, oper);
            ints_vec.clear();
            ints_vec.resize(ndof);
            ints_vec[mode] = std::move(ints);
            this->mTransformer.OneModeTransform(trans_coefs, tmp, ints_vec, this->mVssI, false); // tmp is Zero'ed in OneModeTransform
            trans_coefs = tmp;
         }

         // Result for this term
         fac = opdef.Coef(iterm) * Dot(aY.Coefs(), trans_coefs);

         // Add to result
         tmp_result[0] += fac;

         // Active-term results
         for(LocalModeNr idof=I_0; idof<ndof; ++idof)
         {
            if (  opdef.IsModeActiveInTerm(idof, iterm)
               )
            {
               tmp_result[idof+1] += fac;
            }
         }
      }
   }

   // Move data to std::vector
   std::vector<param_t> result(tmp_result.Size());
   for(In i=I_0; i<tmp_result.Size(); ++i)
   {
      result[i] = std::move(tmp_result[i]);
   }

   // Divide by norm2
   auto wf_norm2 = this->WaveFunctionNorm2();
   for(auto& exptval : result)
   {
      exptval /= wf_norm2;
   }
   
   return result;
}

/**
 * Type
 * @return
 *    Type
 **/
std::string LinearRasTdHEom::TypeImpl
   (
   )  const
{
   return "MCTDH[" + std::to_string(this->MaxExciLevel()) + "]";
}

/**
 * Reference coefficient
 *
 * @param aY
 * @return
 *    Reference coef
 **/
typename LinearRasTdHEom::param_t LinearRasTdHEom::ReferenceCoefficientImpl
   (  const vec_t& aY
   )  const
{
   const auto& coefs = aY.Coefs();

   return coefs.GetModeCombiData(0).GetScalar();
}

/**
 * Apply operator
 *
 * @param aIntegrals
 * @param arY
 * @param arNorm2
 * @return
 *    Success?
 **/
bool LinearRasTdHEom::ApplyOperatorImpl
   (  const integrals_t& aIntegrals
   ,  vec_t& arY
   ,  real_t& arNorm2
   )
{
   const auto& opdef = aIntegrals.Oper().Oper();
   auto nterms = opdef.Nterms();
   auto ndof = this->NDof();

   coefs_t result_coefs(this->mVssI, BaseTensor<param_t>::typeID::SIMPLE);

   #pragma omp parallel
   {
      // Init temporary
      coefs_t tmp = arY.Coefs();
      coefs_t trans_coefs;
      param_t fac(0.);
      In nfac;
      In mode;
      In oper;
      mat_t ints;
      std::vector<mat_t> ints_vec;

      #pragma omp for schedule(dynamic) reduction(ComplexTensorDataCont_Add : result_coefs)
      for(In iterm = I_0; iterm<nterms; ++iterm)
      {
         nfac = opdef.NfactorsInTerm(iterm);
         trans_coefs = arY.Coefs();
         for(In ifac=I_0; ifac<nfac; ++ifac)
         {
            mode = opdef.ModeForFactor(iterm, ifac);
            oper = opdef.OperForFactor(iterm, ifac);
            ints.SetNewSize(this->NAct(mode), this->NAct(mode));
            ints = aIntegrals.GetActiveIntegrals(mode, oper);
            ints_vec.clear();
            ints_vec.resize(ndof);
            ints_vec[mode] = std::move(ints);
            this->mTransformer.OneModeTransform(trans_coefs, tmp, ints_vec, this->mVssI, false); // tmp is Zero'ed in OneModeTransform
            trans_coefs = tmp;
         }

         result_coefs.Axpy(trans_coefs, opdef.Coef(iterm));
      }
   }

   // Calculate norm
   arNorm2 = result_coefs.Norm2();

   // Normalize
   result_coefs.Scale(1./std::sqrt(arNorm2));

   // Set coefs to result_coefs
   arY.Coefs() = std::move(result_coefs);

   return true;
}

/**
 * Calculate norm2 of wave function
 *
 * @param aY      WF vector
 * @return
 *    norm2
 **/
typename LinearRasTdHEom::real_t LinearRasTdHEom::CalculateWfNorm2Impl
   (  const vec_t& aY
   )  const
{
   auto trans = aY.Coefs();
   auto tmp = aY.Coefs();
   tmp.Zero();

   // Perform forward contractions with modal overlaps for all modes
   std::vector<mat_t> overlaps;
   overlaps.reserve(this->NDof());
   for(In idof=I_0; idof<this->NDof(); ++idof)
   {
      if (  this->AssumeFixedElectronicDofModals()
         && this->H0OpDef().GetGlobalModeNr(idof) == this->ElectronicDofNr()
         )
      {
         continue;
      }
      auto nact = this->NAct(idof);
      auto nbas = this->NBas(idof);
      auto off = this->ModalOffsets()[idof];

      overlaps.clear();
      overlaps.resize(this->NDof());
      overlaps[idof].SetNewSize(nact, nact);
      overlaps[idof] = this->H0Integrals().GetActiveOverlaps(idof);

      this->mTransformer.OneModeTransform(trans, tmp, overlaps, this->mVssI, false);
      trans = tmp;
   } 

   auto dot = Dot(aY.Coefs(), trans);

   if (  !libmda::numeric::float_numeq_zero(std::imag(dot), std::real(dot), 10)
      )
   {
      MidasWarning("MCTDH[n]: Norm2 attained a non-vanishing imaginary part.");
   }

   return std::real(dot);
}


/**
 * Construct MCR
 *
 * @param apCalcDef     McTdHCalcDef
 * @param aType         Type of MCR
 * @param aNModals      Number of modals (for assigning addresses)
 * @return
 *    MCR
 **/
ModeCombiOpRange LinearRasTdHEom::ConstructModeCombiOpRange
   (  const input::McTdHCalcDef* const apCalcDef
   ,  LinearRasTdHEom::mcrType aType
   ,  const std::vector<unsigned>& aNModals
   )
{
   auto ext_range_modes = apCalcDef->GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");
   const auto& opdef = gOperatorDefs.GetOperator(apCalcDef->GetOper());
   auto ndof = opdef.NmodesInOp();
   auto max_exci = apCalcDef->GetMethodInfo().template At<In>("MAXEXCILEVEL");
   In e_nr = gOperatorDefs.ElectronicDofNr();

   // Add "electronic" mode to ext_range_modes if singleset = true
   if (  e_nr != -I_1
      )
   {
      ext_range_modes.emplace(e_nr);
   }

   // In case ext_range_modes is empty, we do the standard thing
   if (  ext_range_modes.empty()
      )
   {
      switch   (  aType
               )
      {
         case LinearRasTdHEom::mcrType::I:
         {
            ModeCombiOpRange mcr(max_exci, ndof);
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr, aNModals);
            }
            return mcr;
            break;
         }
         case LinearRasTdHEom::mcrType::X1:
         {
            ModeCombiOpRange mcr(std::set<Uin>{ static_cast<Uin>(max_exci + I_1) }, ndof);
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr, aNModals);
            }
            return mcr;
            break;
         }
         case LinearRasTdHEom::mcrType::IpX1:
         {
            ModeCombiOpRange mcr(max_exci + I_1, ndof);
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr, aNModals);
            }
            return mcr;
            break;
         }
         default:
         {
            MIDASERROR("Unrecognized LinearRasTdHEom::mcrType!");
            return ModeCombiOpRange();
         }
      }
   }
   else
   {
      switch   (  aType
               )
      {
         case LinearRasTdHEom::mcrType::I:
         {
            auto mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(max_exci, ndof, ext_range_modes);
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr, aNModals);
            }
            return mcr;
            break;
         }
         case LinearRasTdHEom::mcrType::X1:
         {
            // Stupid way! Construct full (including I) and remove I afterwards.
            auto mcr_x1 = ConstructModeCombiOpRangeFromExtendedRangeModes(max_exci + I_1, ndof, ext_range_modes);
            auto mcr_i = ConstructModeCombiOpRangeFromExtendedRangeModes(max_exci, ndof, ext_range_modes);
            mcr_x1.Erase(std::move(mcr_i));
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr_x1, aNModals);
            }
            return mcr_x1;
            break;
         }
         case LinearRasTdHEom::mcrType::IpX1:
         {
            auto mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(max_exci + I_1, ndof, ext_range_modes);
            if (  !aNModals.empty()
               )
            {
               SetAddresses(mcr, aNModals);
            }
            return mcr;
            break;
         }
         default:
         {
            MIDASERROR("Unrecognized LinearRasTdHEom::mcrType!");
            return ModeCombiOpRange();
         }
      }
   }
}

} /* namespace midas::mctdh */
