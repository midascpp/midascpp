/*
************************************************************************
*
* @file                 McTdHEomBase.h
*
* Created:              16-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Base class for EOMs. Contains calculation data.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHEOMBASE_H_INCLUDED
#define MCTDHEOMBASE_H_INCLUDED

#include "input/McTdHCalcDef.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "util/CallStatisticsHandler.h"
#include "td/oper/LinCombOper.h"

#include "td/mctdh/start/WaveFunctionInitializer.h"

namespace midas::mctdh
{

/**
 * Simple base class for EOMs. Holds aliases, number of modes, OpDef, and integrals.
 **/
template
   <  typename PARAM_T
   ,  template<typename> typename VEC_T
   ,  typename INTEGRALS_T
   >
class McTdHEomBase
   :  public midas::mctdh::WaveFunctionInitializer
{
   public:
      //! Aliases
      using param_t = PARAM_T;
      using real_t = midas::type_traits::RealTypeT<param_t>;
      using step_t = real_t;
      using vec_t = VEC_T<PARAM_T>;
      using integrals_t = INTEGRALS_T;
      using offset_t = std::vector<std::vector<In>>;
      using mat_t = GeneralMidasMatrix<param_t>;
      using oper_t = midas::td::LinCombOper<step_t, param_t, OpDef>;

      using Initializer = midas::mctdh::WaveFunctionInitializer;

      //! Structs for energy calculation
      inline static struct update_integrals_t    {} update_integrals;
      inline static struct no_update_integrals_t {} no_update_integrals;

   private:
      //! Number of modes
      In mNModes = I_0;

      //! Number of degrees of freedom (including "electronic" mode)
      In mNDof = I_0;

      //! Pointer to Hamiltonian operator
      const oper_t& mrOper;

      //! Time-dependent one-mode integrals. One set of integrals for each operator in mrOper.
      std::vector<integrals_t> mIntegrals;

      //! Modal offsets in vec_t::mModals. Accessed as: [mode-number][active-modal-number]
      offset_t mModalOffsets;

      //! Initial wave function
      vec_t mInitialWaveFunction;

      //! Are we doing imaginary-time propagation?
      bool mImagTime = false;

      //! Energy-change threshold for stopping integration during imaginary-time propagation
      real_t mImagTimeThreshold = static_cast<real_t>(-1.);

      //! Time for last integral update
      step_t mIntegralsTime = step_t(0.);

      //! Run as exact theory (full active space)
      bool mExact = false;

      //! Assume fixed modals for electronic DOF
      bool mAssumeFixedElectronicDofModals = true;

      //! Norm of initial wave packet after applying operator
      real_t mApplyOperNorm2 = static_cast<real_t>(1.);

      //! Initial energy (real part)
      real_t mInitialEnergy = static_cast<real_t>(0.);

      //! IO level
      In mIoLevel = I_1;


      //! Calculate derivative
      virtual vec_t DerivativeImpl
         (  step_t aT
         ,  const vec_t& aY
         )  = 0;

      //! Return zero vector of correct shape.
      virtual vec_t ShapedZeroVectorImpl
         (
         )  const = 0;

      //! Auto-correlation function
      virtual param_t CalculateAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const
      {
         MidasWarning("CalculateAutoCorrImpl not implemented for " + this->Type() + ".");
         return param_t(0.);
      }

      //! Half-time auto-correlation function
      virtual param_t CalculateHalfTimeAutoCorrImpl
         (  step_t aT
         ,  const vec_t& aY
         )  const
      {
         MidasWarning("CalculateHalfTimeAutoCorrImpl not implemented for " + this->Type() + ".");
         return param_t(0.);
      }

      //! Expectation values. First is full exptval, next include only active terms for each mode.
      virtual std::vector<param_t> ExpectationValueImpl
         (  const integrals_t& aIntegrals
         ,  const vec_t& aY
         )  const
      {
         MidasWarning("ExpectationValueImpl not implemented for " + this->Type() + ".");
         return std::vector<param_t>(0, 0.);
      }

      //! Type
      virtual std::string TypeImpl
         (
         )  const = 0;

      //! Prepare integration: Set wave function and update integrals
      virtual void PrepareIntegrationImpl
         (  vec_t& arY
         )  = 0;

      //! Update integrals
      virtual void UpdateIntegralsImpl
         (  step_t aT
         ,  const vec_t& aY
         )  = 0;

      //! Reference coef
      virtual param_t ReferenceCoefficientImpl
         (  const vec_t&
         )  const = 0;

      //! Apply operator to wf. Re-normalize and save norm.
      virtual bool ApplyOperatorImpl
         (  const integrals_t&
         ,  vec_t&
         ,  real_t&
         )  = 0;

      //! Calculate energy
      param_t CalculateEnergyImpl
         (  const vec_t& aY
         )  const
      {
//         MidasWarning("MCTDH: Calculating energy using ExpectationValue. This is not efficient, and should not happen!");
         param_t result = 0;
         for(const auto& ints : this->mIntegrals)
         {
            result += this->ExpectationValue(ints, aY);
         }
         return result;
      }

      //! Apply operator. Calculate integrals and call impl.
      bool ApplyOperator
         (  vec_t& arY
         ,  real_t& arNorm2
         )
      {
         // Find operator (will throw error if it does not exist)
         const auto& op_name = this->CalcDef()->GetApplyOperator();
         const auto& opdef = gOperatorDefs.GetOperator(op_name);

         // Construct and update integrals
         midas::td::TdOperTerm<step_t, param_t, OpDef> oper(opdef, 1);
         std::vector<integrals_t> oper_ints;
         oper_ints.emplace_back(this->CalcDef(), oper, this->GetBasDef());
         Initializer::TransformIntegrals(oper_ints);
         oper_ints.front().Update(arY.Modals(), this->ModalOffsets());

         // Call implementation
         return this->ApplyOperatorImpl(oper_ints.front(), arY, arNorm2);
      }


   protected:
      //! Number of modal parameters
      size_t mNModalParams = 0;

      //! Energy and time saved from derivative calc.
      std::pair<step_t, param_t> mSavedEnergy;

      //! Wave-function norm (squared)
      real_t mWfNorm2 = static_cast<real_t>(1.);


      //! Process new vector.
      virtual bool ProcessNewVectorImpl
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& arT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         ,  bool aSaneY
         ,  bool aSaneDyDt
         )  const
      {
         if (  !( aSaneY
               && aSaneDyDt
               )
            )
         {
            Mout  << " McTdHEomBase::ProcessNewVector failed!" << "\n"
                  << "   y sane:      " << std::boolalpha << aSaneY << "\n"
                  << "   dy/dt sane:  " << aSaneDyDt << std::noboolalpha << std::endl;
            MidasWarning("McTdHEomBase::ProcessNewVectorImpl: Default sanity check failed! Reset to previous step and reduce step length.");

            arY = aYOld;
            arT = aTOld;
            arDyDt = aDyDtOld;

            arStep /= 2;

            return false;
         }
         else
         {
            return true;
         }
      }

      //! Update integrals
      void UpdateIntegrals
         (  step_t aT
         ,  const vec_t& aY
         )
      {
         this->UpdateIntegralsImpl(aT, aY);
         this->mIntegralsTime = aT;
      }

      //! Get integrals
      std::vector<integrals_t>& Integrals
         (
         )
      {
         return this->mIntegrals;
      }

      //! Get offsets
      offset_t& ModalOffsets
         (
         )
      {
         return this->mModalOffsets;
      }

      //! Check if we want to do IO
      bool DoIo
         (  In aLevel
         )  const
      {
         return (mIoLevel >= aLevel) || gDebug;
      }



   public:
      //! Constructor
      McTdHEomBase
         (  const input::McTdHCalcDef* const apCalcDef
         ,  const oper_t& aOper
         ,  const BasDef* const apBasDef
         )
         :  midas::mctdh::WaveFunctionInitializer
               (  apCalcDef
               ,  &aOper.front().Oper()
               ,  apBasDef
               )
         ,  mNModes
               (  apBasDef->GetmNoModesInBas()
               )
         ,  mNDof
               (  mNModes
               )
         ,  mrOper
               (  aOper
               )
         ,  mModalOffsets
               (  mNDof
               )
         ,  mImagTime
               (  apCalcDef->GetImagTime()
               )
         ,  mImagTimeThreshold
               (  apCalcDef->GetImagTimeThreshold()
               )
         ,  mExact
               (  apCalcDef->GetExact()
               )
         ,  mAssumeFixedElectronicDofModals
               (  !apCalcDef->GetPropagateElectronicDofModals()
               )
         ,  mIoLevel
               (  apCalcDef->GetIoLevel()
               )
      {
         // Initialize integrals
         this->mIntegrals.reserve(mrOper.size());
         for(const auto& op : mrOper)
         {
            this->mIntegrals.emplace_back(apCalcDef, op, apBasDef);
         }

         // Modify nmodes if using single-set formalism for non-adiabatic dynamics
         if (  this->NonAdiabatic()
            )
         {
            // Decrement mNModes, because one of the modes is the electronic DOF
            --mNModes;
         }

         // Sanity check
         MidasAssert(apCalcDef->GetActiveSpace().size() == this->NModes(), "Wrong size of active-space vector!");

         // Set size of offsets. Actual numbers must be set in derived EOM class.
         for(In idof=I_0; idof<this->NDof(); ++idof)
         {
            this->mModalOffsets[idof].resize(this->NAct(idof));
         }

         // Check that the active space is set correctly if the EXACT keyword is set
         if (  this->mExact
            )
         {
            bool fail = false;
            for(In idof=I_0; idof<mNDof; ++idof)
            {
               auto nbas = this->NBas(idof);
               auto nact = this->NAct(idof);

               if (  nbas != nact
                  )
               {
                  Mout  << " dof: " << idof << ", nbas = " << nbas << ", nact = " << nact << " (should be equal!)" << std::endl;
                  fail = true;
               }
            }
            MidasAssert(!fail, "For exact propagation, the active space must have the same size as the full space.");
         }
      }

      //! Virtual destructor
      virtual ~McTdHEomBase() = default;

      //! Calculate derivative
      vec_t Derivative
         (  step_t aT
         ,  const vec_t& aY
         )
      {
         return this->DerivativeImpl(aT, aY);
      }

      //! Process new vector.
      bool ProcessNewVector
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& arT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )  const
      {
         // Check sanity of Y vector
         bool sane_y = midas::util::ContainerSanityCheck(arY);

         // Check sanity of dY/dt vector
         bool sane_dydt = midas::util::ContainerSanityCheck(arDyDt);

         // Call impl
         return this->ProcessNewVectorImpl(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail, sane_y, sane_dydt);
      }

      //! Return zero vector of correct shape.
      vec_t ShapedZeroVector
         (
         )  const
      {
         return this->ShapedZeroVectorImpl();
      }

      //! Auto-correlation function
      param_t CalculateAutoCorr
         (  step_t aT
         ,  const vec_t& aY
         )  const
      {
         // We scale with norm2 here to get: <O psi(0)|O psi(t)>
         return this->mApplyOperNorm2 * this->CalculateAutoCorrImpl(aT, aY);
      }

      //! Half-time auto-correlation function
      param_t CalculateHalfTimeAutoCorr
         (  step_t aT
         ,  const vec_t& aY
         )  const
      {
         // We scale with norm2 here to get: <(O psi(t/2))*|O psi(t/2)>
         return this->mApplyOperNorm2 * this->CalculateHalfTimeAutoCorrImpl(aT, aY);
      }

      //! Expectation value
      param_t ExpectationValue
         (  const integrals_t& aIntegrals
         ,  const vec_t& aY
         )  const
      {
         return this->ExpectationValueImpl(aIntegrals, aY)[0];
      }

      //! Vector of expectation values. First is the "full" exptval, next are values including only active terms for each mode.
      std::vector<param_t> ActiveTermExpectationValues
         (  const integrals_t& aIntegrals
         ,  const vec_t& aY
         )  const
      {
         return this->ExpectationValueImpl(aIntegrals, aY);
      }

      //! Type
      std::string Type
         (
         )  const
      {
         return this->TypeImpl();
      }

      //! Calculate energy
      param_t CalculateEnergy
         (  step_t aTime
         ,  const vec_t& aY
         ,  no_update_integrals_t aNoUpdateIntegrals
         )  const
      {
         if (  libmda::numeric::float_eq(aTime, this->mSavedEnergy.first)
            )
         {
            return this->mSavedEnergy.second;
         }
         else
         {
            MidasWarning("McTdHEomBase::CalculateEnergy. aTime != mSavedEnergy.first. In that case, the integrals really should be updated. Result cannot be trusted!");
            return this->CalculateEnergyImpl(aY);
         }
      }

      //! Calculate energy
      param_t CalculateEnergy
         (  step_t aTime
         ,  const vec_t& aY
         ,  update_integrals_t aUpdateIntegrals
         )
      {
         this->UpdateIntegrals(aTime, aY);

         return this->CalculateEnergyImpl(aY);
      }

      //! Wave-function norm2
      real_t WaveFunctionNorm2
         (
         )  const
      {
         return this->mWfNorm2;
      }

      //! Prepare integration
      void PrepareIntegration
         (  vec_t& arY
         )
      {
         // Read initial wave function
         if (  !Initializer::ReadInitialWaveFunction(this->mInitialWaveFunction)
            )
         {
            MIDASERROR("McTdHEomBase: Error in reading initial wave function!");
         }

         // NB: We need to transform WF first. Else, the S matrices are just I.
         // If we are using a linear parameterization of the modals, transform to spectral basis.
         // For exponential parameterizations, we always want to start with kappa = 0, i.e. the time-independent basis contains the initial active modals 
         // as the first n_act basis functions.
         if (  !this->CalcDef()->ExponentialModalTransform()
            && this->CalcDef()->GetInitialWavePacket() != input::McTdHCalcDef::initType::MCTDH      // For MCTDH, we simply read in the WF
            )
         {
            Mout  << " McTdHEomBase: Transforming initial wave function to spectral basis." << std::endl;
            Initializer::TransformInitialWaveFunction(this->mInitialWaveFunction, this->H0Integrals(), this->mModalOffsets, this->NModalsVec());
         }

         // Transform integrals to given spectral basis
         Mout  << " McTdHEomBase: Transforming H integrals to spectral basis." << std::endl;
         Initializer::TransformIntegrals(this->mIntegrals);

         // Call impl. Set wave function.
         this->PrepareIntegrationImpl(arY);

         // Check if we need the initial energy (from before applying the operator)
         if (  this->CalcDef()->GetSpectrumEnergyShift().first == "E0"
            )
         {
            // We then need to update integrals and calculate energy the stupid way
            this->mInitialEnergy = std::real(this->CalculateEnergy(0., arY, update_integrals));

            Mout  << " MCTDH Initial energy (before applying operator) is set to: " << this->mInitialEnergy << std::endl;
         }

         // Apply operator to initial wave packet
         const auto& op = this->CalcDef()->GetApplyOperator();
         if (  !op.empty()
            )
         {
            Mout  << " McTdHEomBase: Apply operator '" << op << "' to initial wave packet!" << std::endl;
            if (  !this->ApplyOperator(this->mInitialWaveFunction, this->mApplyOperNorm2)
               )
            {
               MIDASERROR("Error in applying operator to initial wave packet!");
            }

            // Set wave function again
            this->PrepareIntegrationImpl(arY);
         }

         // Update integrals if we are doing exact propagation.
         // Otherwise, it will be done before the first derivative evaluation.
         if (  this->Exact()
            )
         {
            this->UpdateIntegrals(0., arY);
         }
      }

      //! Get reference coefficient
      param_t ReferenceCoefficient
         (  const vec_t& aY
         )  const
      {
         return this->ReferenceCoefficientImpl(aY);
      }

      //! Get energy shift for spectrum
      real_t SpectrumEnergyShift
         (
         )  const
      {
         return   this->CalcDef()->GetSpectrumEnergyShift().first == "E0"
                  ?  this->mInitialEnergy
                  :  this->CalcDef()->GetSpectrumEnergyShift().second;
      }
         

      //! Get initial wave function
      const vec_t& InitialWaveFunction
         (
         )  const
      {
         return this->mInitialWaveFunction;
      }

      //! Get integrals (const)
      const std::vector<integrals_t>& Integrals
         (
         )  const
      {
         return this->mIntegrals;
      }

      //! Get integrals of H0 (we assume the first term in the operator is H0)
      const integrals_t& H0Integrals
         (
         )  const
      {
         return this->mIntegrals.front();
      }

      //! Get time for last integrals update
      step_t IntegralsTime
         (
         )  const
      {
         return this->mIntegralsTime;
      }

      //! Get offsets (const)
      const offset_t& ModalOffsets
         (
         )  const
      {
         return this->mModalOffsets;
      }

      //! Get ImagTime
      bool ImagTime
         (
         )  const
      {
         return this->mImagTime;
      }

      //! Get ImagTimeThreshold
      real_t ImagTimeThreshold
         (
         )  const
      {
         return this->mImagTimeThreshold;
      }

      //! Get NModes
      In NModes
         (
         )  const
      {
         return this->mNModes;
      }

      //! Get number of DOFs
      In NDof
         (
         )  const
      {
         return this->mNDof;
      }

      //! Get NBas
      In NBas
         (  LocalModeNr aOperMode
         )  const
      {
         const auto& limits = Initializer::ModalBasisLimits();
         bool has_limits = !limits.empty();
         if (  has_limits
            )
         {
            return limits[aOperMode];
         }
         else if  (  aOperMode < this->NDof()
                  )
         {
            GlobalModeNr g_mode = this->mrOper.front().Oper().GetGlobalModeNr(aOperMode); // Just take mode ordering from first operator in LinCombOper
            LocalModeNr bas_mode = this->GetBasDef()->GetLocalModeNr(g_mode);
            return this->GetBasDef()->Nbas(bas_mode);
         }
         else
         {
            MIDASERROR("Cannot get NBas for mode " + std::to_string(aOperMode));
            return I_0;
         }
      }

      //! Get NAct (active modals)
      In NAct
         (  LocalModeNr aOperMode
         )  const
      {
         if (  aOperMode < this->NDof()
            )
         {
            return this->NModalsVec()[aOperMode];
         }
         else
         {
            MIDASERROR("Cannot get NAct for mode " + std::to_string(aOperMode));
            return I_0;
         }
      }

      //! Get Hamiltonian
      const oper_t& Oper
         (
         )  const
      {
         return this->mrOper;
      }

      //! Get OpDef for H0
      const OpDef& H0OpDef
         (
         )  const
      {
         return this->mrOper.front().Oper();
      }

      //! Get maximum excitation level of Hamiltonian
      In HamiltonianMcLevel
         (
         )  const
      {
         In max_exci = I_0;

         for(const auto& op : this->mrOper)
         {
            max_exci = std::max(max_exci, op.Oper().McLevel());
         }

         return max_exci;
      }

      //! Get IntegrationInfo
      const auto& IntegrationInfo
         (
         )  const
      {
         return this->CalcDef()->GetIntegrationInfo();
      }

      //! Get exact
      bool Exact
         (
         )  const
      {
         return mExact;
      }

      //! Get norm squared of wf after applying initial operator
      real_t ApplyOperNorm2
         (
         )  const
      {
         return mApplyOperNorm2;
      }

      //! Do we assume fixed modals for electronic DOF?
      bool AssumeFixedElectronicDofModals
         (
         )  const
      {
         return this->mAssumeFixedElectronicDofModals;
      }
};

} /* namespace midas::mctdh */

#endif /* MCTDHEOMBASE_H_INCLUDED */
