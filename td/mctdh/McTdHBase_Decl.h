/*
************************************************************************
*
* @file                 McTdHBase_Decl.h
*
* Created:              01-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    McTdH wrapper class and function declarations.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHBASE_DECL_H_INCLUDED
#define MCTDHBASE_DECL_H_INCLUDED

#include "util/type_traits/Complex.h"
#include "properties/McTdHProperties.h"
#include "td/oper/LinCombOper.h"

// Forward declarations
namespace midas::input
{
   class McTdHCalcDef;
}
class BasDef;
class OpDef;

namespace midas::mctdh
{
/**
 * Wrapper class for MCTDH calculations.
 **/
template
   <  typename EOM
   >
class McTdHBase
   :  private detail::McTdHProperties<EOM>
{
   public:
      //! Aliases
      using Properties = detail::McTdHProperties<EOM>;
      using param_t = typename EOM::param_t;
      using vec_t = typename EOM::vec_t;
      using step_t = typename midas::type_traits::RealTypeT<param_t>;
      using oper_t = midas::td::LinCombOper<step_t, param_t, OpDef>;

      //! Integration schemes
      enum class integratorType : int
      {  VMF = 0
      ,  CMF
      ,  LUBICH
      };

   private:
      //! Name of calculation
      std::string mName = "mctdh";

      //! EOM
      EOM mEom;

      //! Type of integrator
      integratorType mIntegrationScheme = integratorType::VMF;

      //! Initialize one-mode densities member
      void InitOneModeDensities
         (  const input::McTdHCalcDef* const apCalcDef
         );

      //! Propagate using variable mean field (VMF) scheme
      void PropagateVmf
         (  vec_t&
         );

      //! Propagate using constant mean field (CMF) scheme
      void PropagateCmf
         (  vec_t&
         );

      //! Propagate using Lubich integrator
      void PropagateLubich
         (  vec_t&
         );

      //! Wave-function related output
      void WaveFunctionOutput
         (  const std::vector<step_t>& aTs
         ,  const std::vector<vec_t>& aYs
         )  const;

      //! Finalize calculation
      void Finalize
         (  const std::vector<step_t>& aTs
         ,  const std::vector<vec_t>& aYs
         )  const;

   public:
      //! Constructor
      McTdHBase
         (  const input::McTdHCalcDef* const
         ,  const oper_t&
         ,  const BasDef* const
         );

      //! Calculate derivative. Call implementation in EOM.
      vec_t Derivative
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Process new vector. Call implementation in EOM.
      bool ProcessNewVector
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& arT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )  const;

      //! Save data for accepted step. Call implementation in EOM and put properties in McTdHProperties.
      bool AcceptedStep
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Save data for interpolated point. Call implementation in EOM and put properties in McTdHProperties.
      void InterpolatedPoint
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Return zero vector of correct shape.
      vec_t ShapedZeroVector
         (
         )  const;

      //! Number of modes
      Uin NModes
         (
         )  const;

      //! Propagate EOM
      void Propagate
         (
         );

      //! Get name
      const std::string& Name
         (
         )  const;
};
} /* namespace midas::mctdh */


#endif /* MCTDHBASE_DECL_H_INCLUDED */
