/*
************************************************************************
*
* @file                 PrimitiveIntegrals_Decl.h
*
* Created:              16-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Primitive integrals for TD calculations.
*                       Allows for transforming to another primitive basis, 
*                       e.g. VSCF spectral basis.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef PRIMITIVEINTEGRALS_DECL_H_INCLUDED
#define PRIMITIVEINTEGRALS_DECL_H_INCLUDED

#include <vector>
#include <string>

#include "mmv/MidasVector.h"
#include "inc_gen/TypeDefs.h"

#include "ni/OneModeInt.h"


//! Forward decl
class OpDef;
class BasDef;

namespace midas::td
{

/**
 * Class for holding time-independent integrals.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T = GeneralMidasVector
   ,  template<typename> typename MAT_T = GeneralMidasMatrix
   >
class PrimitiveIntegrals
{
   public:
      //! Aliases
      using param_t = T;
      using step_t = typename midas::type_traits::RealTypeT<T>;
      template<typename U> using vec_templ_t = VEC_T<U>;
      template<typename U> using mat_templ_t = MAT_T<U>;
      using vec_t = vec_templ_t<param_t>;
      using mat_t = mat_templ_t<param_t>;

   protected:
      /** @name Time-independent integrals **/
      //!@{
      /**
       * This is reset to nullptr if we transform to another basis (e.g. a spectral VSCF basis).
       * In that case, only mTransformedOneModeInt is used in subsequent steps.
       **/
      std::unique_ptr<OneModeInt> mPrimitiveOneModeInt = nullptr;

      //! Time-independent modal integrals in transformed, orthogonal basis. Outer vector is ModeNr, inner vector is OperNr.
      std::vector<std::vector<mat_t> > mTransformedOneModeInt;
      //!@}

   private:
      /** @name Dimensions **/
      //!@{
      //! Number of operators for each mode. Constant regardless of prim/trans. basis.
      const std::vector<In> mVecNOper;

      //! Current basis dim. of each mode. Update when going from prim.->trans. basis.
      std::vector<Uin> mVecNBas;

      //! Positions of unit operators
      const std::vector<In> mVecUnitOpPositions;
      //!@}
      
      /** @name Internal utility functions **/
      //!@{
      static std::vector<In> VecNOper(const OpDef* const, const BasDef* const);
      static std::vector<Uin> VecNBas(const OpDef* const, const BasDef* const);
      static std::vector<Uin> VecNBas(const std::vector<std::vector<mat_t>>&);
      static std::vector<In> VecUnitOpPositions(const OpDef* const);

      //! Trans. prim->modal, stores in mTransformedOneModeInt, sets mPrimitiveOneModeInt = nullptr.
      template<typename U>
      void TransformModalBasisImpl
         (  const OneModeInt& arPrimInts
         ,  const GeneralDataCont<U>& aModals
         ,  const std::vector<In>& aModalOffSets
         ,  const std::vector<In>& aModalBasisLimits
         ,  const std::set<In>& aSkipModes
         );
      //!@}
      
      //! Update VecNBas
      void UpdateVecNBas
         (  const std::vector<std::vector<mat_t>>&
         );

   public:
      //! Delete default c-tor (empty integrals)
      PrimitiveIntegrals() = delete;

      //! Allow move c-tor
      PrimitiveIntegrals(PrimitiveIntegrals&&) = default;

      //! Dis-allow copy c-tor
      PrimitiveIntegrals(const PrimitiveIntegrals&) = delete;

      //! Dis-allow copy assignment
      PrimitiveIntegrals& operator=(const PrimitiveIntegrals&) = delete;


      //! Calculates primitive integrals.
      PrimitiveIntegrals
         (  const OpDef* const apOpDef
         ,  const BasDef* const apBasDef
         );

      //! Assumes prim.ints. already calc'ed unless setting arCalcInt = true.
      PrimitiveIntegrals
         (  OneModeInt aOneModeInt
         ,  bool aCalcInt = false
         );

      //! Directly calculate modal integrals. Assumes prim.ints. already calc'ed in argument.
      PrimitiveIntegrals
         (  const OneModeInt& arOneModeInt
         ,  const DataCont& aModals
         ,  const std::vector<In>& aModalOffSets
         ,  const std::vector<In>& aModalBasisLimits
         ,  const std::set<In>& aSkipModes = {}
         );

      //! Are we using transformed basis?
      bool TransformedBasis
         (
         )  const;

      //! Number of basis functions for mode
      In NBas
         (  LocalModeNr
         )  const;

      //! Vector of number of basis functions
      const std::vector<Uin>& GetVecNBas
         (
         )  const;

      //! Number of operators for mode
      virtual In NOper
         (  LocalModeNr
         )  const;

      //! Number of modes
      In NModes
         (
         )  const;

      //! Transform from primitive to spectral basis. Clears the primitive integrals.
      template<typename U>
      void TransformModalBasis
         (  const GeneralDataCont<U>& aModals
         ,  const std::vector<In>& aModalOffsets
         ,  const std::vector<In>& aModalBasisLimits = {}
         ,  const std::set<In>& aSkipModes = {}
         );

      //! Get time-independent integrals for mode
      mat_t GetTimeIndependentIntegrals
         (  LocalModeNr
         ,  LocalOperNr
         )  const &;

      //! Get time-independent integrals for mode; move if *this is rvalue ref.
      mat_t GetTimeIndependentIntegrals
         (  LocalModeNr
         ,  LocalOperNr
         )  &&;

      //! Get time-independent integral for mode and indices
      param_t GetTimeIndependentIntegral
         (  LocalModeNr
         ,  LocalOperNr
         ,  size_t
         ,  size_t
         )  const;

      //! Get overlap integrals for mode
      mat_t GetOverlapIntegrals
         (  LocalModeNr
         )  const;

      //! Calculate <phi^m_r|h^m|phi^m_s>
      param_t OneModeIntBraket
         (  const vec_t&
         ,  const vec_t&
         ,  LocalModeNr
         ,  LocalOperNr
         )  const;

      //! Calculate <phi^m|h^m|phi^m>
      param_t OneModeIntBraket
         (  const vec_t&
         ,  LocalModeNr
         ,  LocalOperNr
         )  const;
};

} /* namespace midas::td */

#endif /* PRIMITIVEINTEGRALS_DECL_H_INCLUDED */
