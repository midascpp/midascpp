/*
************************************************************************
*
* @file                 TdH.h
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    TdH wrapper class and function declarations.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDH_H_INCLUDED
#define TDH_H_INCLUDED

#include <memory>

#include "TdHBase.h"


/**
 * Wrapper class for TdH objects
 **/
class TdH
{
   public:
      //! Typedefs
      template <typename T>
      using container_t = typename TdHBase::container_t<T>;
      using step_t = typename TdHBase::step_t;
      using param_t = typename TdHBase::param_t;
      using vec_t = typename TdHBase::vec_t;

   private:
      //! Pointer to concrete TDH object
      std::unique_ptr<TdHBase> mTdH = nullptr;

   public:
      //! Default constructor
      TdH() = default;

      //! Constructor from pointer
      TdH
         (  TdHBase*
         );

      //! Constructor from TdHCalcDef, etc.
      TdH
         (  input::TdHCalcDef*
         ,  OpDef*
         ,  BasDef*
         ,  const std::string&
         );

      //! Delete copy constructor
      TdH
         (  const TdH&
         )  = delete;

      //! Default move constructor
      TdH
         (  TdH&&
         )  = default;

      //! Delte copy assign
      TdH& operator=
         (  const TdH&
         )  = delete;

      //! Default move assign
      TdH& operator=
         (  TdH&&
         )  = default;

      //! Derivative
      vec_t Derivative
         (  step_t
         ,  const vec_t&
         );

      //! Process new vector
      bool ProcessNewVector
         (  vec_t&
         ,  const vec_t&
         ,  step_t&
         ,  step_t
         ,  vec_t&
         ,  const vec_t&
         ,  step_t&
         ,  size_t&
         )  const;

      //! Save data for accepted step. NB: Requires Hamiltonian integrals to be updated beforehand!
      bool AcceptedStep
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Save data for interpolated point.
      void InterpolatedPoint
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Return zero vector of correct shape
      vec_t ShapedZeroVector
         (
         )  const;

      //! Set name
      void SetName
         (  const std::string&
         );

      //! Get name
      const std::string& Name
         (
         )  const;

      //! Set reference-oper name
      void SetReferenceOperName
         (  const std::string&
         );

      // Initialize mOneModeDensities of TDH object
      void InitOneModeDensities
         (  const std::string& aCalcName
         );

      //! Evolve TDH EOMs
      void Evolve
         (
         );
};

#endif /* TDH_H_INCLUDED */
