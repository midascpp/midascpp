/*
************************************************************************
*
* @file                 LinearTdH.cc
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad
*
* Short Description:    LinearTdH class implementation
*
* Last modified:        Mon Nov 02, 2015  02:36PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "LinearTdH.h"
#include "input/TdHCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/Input.h"
#include "libmda/numeric/float_eq.h"
#include "util/CallStatisticsHandler.h"

#include "mmv/mmv_omp_pragmas.h"

/**
 * Constructor
 *
 * @param apCalcDef     TdH calcdef
 * @param apOpDef       Operator defs
 * @param apBasDef      Basis defs
 * @param aModalsName   Name of file containing initial modals
 **/
LinearTdH::LinearTdH
   (  input::TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  const std::string& aModalsName
   )
   :  TdHBase
         (  apCalcDef
         ,  apOpDef
         ,  apBasDef
         ,  aModalsName
         )
   ,  mConstraint
         (  mpTdHCalcDef->GetLinearTdHConstraint()
         )
   ,  mNormalize
         (  mpTdHCalcDef->GetLinearTdHNormalizeVectors()
         )
   ,  mOccModalOffsets
         (  mNModes
         )
{
   // Construct initial wave packet
   const auto& limits = this->mpTdHCalcDef->GetModalBasisLimits(); // may be empty
   bool has_limits = !limits.empty();
   In n_prim_modal_coef_occ = I_0;
   In n_modal_coef_occ = I_0;
   for(LocalModeNr i_op_mode=I_0; i_op_mode<mNModes; ++i_op_mode)
   {
      this->mOccModalOffsets[i_op_mode]   = n_modal_coef_occ;  // NB: this will only be correct for truncated bases after transforming the initial wave packet in PrepareIntegrationImpl
      GlobalModeNr i_g_mode               = this->mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode              = this->mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas                             = this->mpBasDef->Nbas(i_bas_mode);
      n_modal_coef_occ                   += has_limits ? std::min(limits[i_op_mode], nbas) : nbas;    // Check if we are limiting the basis
      n_prim_modal_coef_occ              += nbas;
   }

   // Get initial occupied modals
   if (  gDebug
      || this->mIoLevel >= I_8
      )
   {
      Mout  << " LinearTdH: Getting initial modals from file: " << aModalsName << std::endl;
   }
   DataCont init_modals;
   init_modals.GetFromExistingOnDisc(mNInputModalCoef, aModalsName);
   init_modals.SaveUponDecon(true);

   // Construct temporary MidasVector
   In off_dc = I_0;
   In off_v = I_0;
   MidasVector tmp_real_occ_modals(n_prim_modal_coef_occ);
   for (In i_op_mode=0; i_op_mode<mNModes; ++i_op_mode)
   {
      In i_g_mode      = mpOpDef->GetGlobalModeNr(i_op_mode);
      In i_bas_mode    = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas          = mpBasDef->Nbas(i_bas_mode);

      init_modals.DataIo(IO_GET, tmp_real_occ_modals, nbas, off_dc, I_1, off_v);
      off_dc += nbas*nbas;
      off_v += nbas;
      
      if (  gDebug
         || this->mIoLevel >= I_10
         )
      {
         Mout  << " Read initial modal coefficients for mode " << i_g_mode << ":\n" << tmp_real_occ_modals << std::endl;
      }
   }

   // Load MidasVector into mInitialOccModals
   mInitialOccModals.SetNewSize(n_prim_modal_coef_occ);
   for(In i=I_0; i<n_prim_modal_coef_occ; ++i)
   {
      this->mInitialOccModals[i] = param_t(tmp_real_occ_modals[i], C_0);
   }

   if (  this->mIoLevel >= I_10
      )
   {
      Mout  << " mInitialOccModals in primitive basis:\n" << this->mInitialOccModals << std::endl;
   }
}

/**
 * Implementation of process new vector.
 * Simply normalizes the modals.
 *
 * @param arY           y vector
 * @param aYOld         Previous y vector
 * @param arT           t
 * @param aTOld         Previous t
 * @param arDyDt        dy/dt
 * @param aDyDtOld      Previous dy/dt
 * @param arStep        Step size (modified if step is rejected)
 * @param arNDerivFail  Number of failed derivative calculations before this step.
 * @return
 *    Success?
 **/
bool LinearTdH::ProcessNewVectorImpl
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )
{
   if (  arNDerivFail > 0
      )
   {
      MIDASERROR("Derivative should not fail for LinearTdH!!!");
   }

   // Niels: In the old implementation, the vector was not normalized for imag time. But why?
   if (  this->NormalizeModals(arY)
      )
   {
      return true;
   }
   else
   {
      // Reset stuff...
      arY = aYOld;
      arT = aTOld;
      arDyDt = aDyDtOld;
      arStep /= 2.;

      return false;
   }
}

/**
 * Normalize modals.
 *
 * @param aVec    Vector of modals + phase
 * @return
 *    Success?
 **/
bool LinearTdH::NormalizeModals
   (  vec_t& aVec
   )  const
{
   step_t tot_norm(1.);

   for(LocalModeNr i_op_mode=0; i_op_mode<mNModes; ++i_op_mode)
   {
      In nbas = this->NBas(i_op_mode);

      vec_t occ_for_mode(nbas); 
      aVec.PieceIo(IO_GET,occ_for_mode,nbas,mOccModalOffsets[i_op_mode]);
      auto norm = occ_for_mode.Norm();

      if (  this->mNormalize
         )
      {
         Nb norm_coef = C_1/norm;
         occ_for_mode.Scale(norm_coef);
         aVec.PieceIo(IO_PUT,occ_for_mode,nbas,mOccModalOffsets[i_op_mode]);
      }
      else
      {
         tot_norm *= norm;
      }

      // If we do not normalize, check the norm!
      if (  !this->mNormalize
         && libmda::numeric::float_neq(norm, C_1)
         )
      {
         MidasWarning("LinearTdH: Norm of modal vector deviates from 1. We should normalize!");
         if (  gDebug
            || this->mIoLevel >= I_5
            )
         {
            Mout  << " Norm of modal vector " << i_op_mode << ": " << norm << std::endl;
         }
      }
   }

   if (  !this->mNormalize
      && (  gDebug
         || this->mIoLevel >= I_10
         )
      )
   {
      Mout  << " Norm of LinearTdH wf:    " << tot_norm << std::endl;
   }

   return true;
}

/**
 *  Calculate derivative vector (dy/dt)
 *
 *  @param aT           t
 *  @param aY           y
 *  @param arNScreen    Accumulated number of screened terms
 *
 *  @return
 *    dy/dt
 **/
typename LinearTdH::vec_t LinearTdH::DerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  size_t& arNScreen
   )  const
{
   LOGCALL("deriv");
   // Init result
   auto length = aY.size();
   vec_t result(length);

   size_t nscreen = 0;

#ifdef VAR_MPI
   auto iproc = midas::mpi::GlobalRank();
   auto range = this->MpiModeRange(iproc);
   for(In i_op_mode=range.first; i_op_mode<range.second; ++i_op_mode)
#else
   for(LocalModeNr i_op_mode=I_0; i_op_mode<this->mNModes; ++i_op_mode)
#endif /* VAR_MPI */
   {
      In nbas = this->NBas(i_op_mode);

      // Get modal coefficients for current mode
      vec_t occ_for_mode(nbas);
      aY.PieceIo(IO_GET, occ_for_mode, nbas, mOccModalOffsets[i_op_mode]);

      // Construct one-mode Fock matrix (with constraint)
      auto h_eff = this->ConstructEffectiveOneModeHamiltonian(i_op_mode, nscreen);

      // Calculate sigma = h_eff*y for current mode
      vec_t result_for_mode = h_eff*occ_for_mode;

      if (  this->mImagTime
         )
      {
         result_for_mode.Scale(CC_M_1);   // For imaginary time go look here!
      }
      else
      {
         result_for_mode.Scale(CC_M_I);   // Normal time 
      }

      if (  gDebug
         || this->mIoLevel > I_15
         )
      {
         Mout  << " LinearTdH::DerivativeImpl for mode " << i_op_mode << "\n"
               << "  occ_for_mode:\n" 
               << occ_for_mode << "\n"
               << "  h_eff:\n" << h_eff << "\n"
               << "  result_for_mode:\n" << result_for_mode << "\n"
               << std::flush;
      }


      // Set sigma for current mode in result vector
      result.PieceIo(IO_PUT, result_for_mode, nbas, mOccModalOffsets[i_op_mode]);
   }

#ifdef VAR_MPI
   // After this, nscreen is the same for all nodes.
   param_t tmp_phase(0.);
   this->SyncData(result, tmp_phase, nscreen);
#endif /* VAR_MPI */

   // Increment arNScreen
   arNScreen += nscreen;

   // Calculate energy for phase integration. The integrals have been updated already!!!
   auto energy = this->CalculateEnergyImpl();
   if (  this->mImagTime
      )
   {
      energy *= CC_M_I;
   }

   // Get phase derivative
   auto phase_deriv = CC_0;
   switch   (  this->mConstraint
            )
   {
      case midas::tdh::constraintID::ZERO:
      {
         phase_deriv = energy;
         break;
      }
      case midas::tdh::constraintID::FOCK:
      {
         phase_deriv = energy*static_cast<param_t>(I_1-this->mNModes);
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized constraint operator!");
      }
   }

   // Set phase derivative
   result[length-1] = phase_deriv;

   // DEBUG
   if (  this->mIoLevel > 15
      || gDebug
      )
   {
      Mout  << " Modal vector:\n" << aY << "\n"
            << " Derivative:\n" << result << "\n"
            << std::flush;
   }

   // Return result vector
   return result;
}

/**
 * Prepare integration, i.e. transform wave function to the used basis.
 *
 * @param arWf    Initial wave function
 **/
void LinearTdH::PrepareIntegrationImpl
   (  vec_t& arWf
   )
{
   // Transform occ modals with mSpectralBasisModals
   bool spec = this->mpTdHCalcDef->GetSpectralBasis();
   if (  spec
      )
   {
      if (  gDebug
         || this->mIoLevel > I_5
         )
      {
         Mout << " Transforming modals to spectral basis." << std::endl;
      }

      MidasMatrix modals;
      mat_t s_m;
      vec_t prim_modal_m;
      vec_t spec_modal_m;
      vec_t half_trans_m;
      vec_t new_occ_modals(this->mNOccModalCoef);

      In offset_prim = I_0;
      In offset_spec = I_0;
      for(In imode = I_0; imode<mNModes; ++imode)
      {
         // Get nbas for primitive basis
         In i_g_mode      = mpOpDef->GetGlobalModeNr(imode);
         In i_bas_mode    = mpBasDef->GetLocalModeNr(i_g_mode);
         In nprimbas      = mpBasDef->Nbas(i_bas_mode);
         In nspecbas      = this->NBas(imode);

         // Set size of spectral-basis modal
         spec_modal_m.SetNewSize(nspecbas);

         // Set primitive modal vector to size of primitive basis and get coefs
         prim_modal_m.SetNewSize(nprimbas);
         this->mInitialOccModals.PieceIo(IO_GET, prim_modal_m, nprimbas, offset_prim);
         offset_prim += nprimbas;

         // Get overlap matrix
         s_m = this->mIntegrals->GetOverlapIntegrals(imode);

         if (  gDebug
            || this->mIoLevel > I_15
            )
         {
            Mout  << " L-TDH S matrix for basis-set transformation:\n" << s_m << std::endl;
         }

         // Get spectral-basis modals for this mode.
         // NB: This returns the transposed modal matrix with modals in the rows!
         // NB: We still assume that the DataCont has not been truncated before this! Otherwise the offsets are wrong!
         if (  this->mSpectralBasisModals.Size() != this->mNInputModalCoef
            )
         {
            MIDASERROR("LinearTdH: mSpectralBasisModals have been truncated before initial-state transform. This should not happen!");
         }
         modals.SetNewSize(nspecbas, nprimbas);
         this->mSpectralBasisModals.DataIo(IO_GET, modals, nspecbas*nprimbas, nspecbas, nprimbas, this->mPrimitiveModalOffsets[imode]);
         if (  gDebug
            || this->mIoLevel > I_15
            )
         {
            Mout  << " L-TDH modal matrix for basis-set transformation:\n" << modals << std::endl;
         }

         // Transform with overlap metric and modal matrix
         half_trans_m.SetNewSize(nprimbas);
         half_trans_m = s_m * prim_modal_m;
         if (  gDebug
            || this->mIoLevel > I_15
            )
         {
            Mout  << " L-TDH initial modal for mode " << imode << " after S-matrix transform:\n" << half_trans_m << std::endl;
         }

         spec_modal_m = modals * half_trans_m;
         if (  gDebug
            || this->mIoLevel >= I_10
            )
         {
            Mout  << " L-TDH initial modal for mode " << imode << " after basis transform:\n" << spec_modal_m << std::endl;
         }

         // Set transformed modal
         // Truncate basis if requested!
         new_occ_modals.PieceIo(IO_PUT, spec_modal_m, nspecbas, offset_spec);
         offset_spec += nspecbas;
      }

      this->mInitialOccModals = std::move(new_occ_modals);
   }

   assert(arWf.size() == (mInitialOccModals.size() + I_1));

   // Set initial modals and phase
   std::copy(mInitialOccModals.begin(), mInitialOccModals.end(), arWf.begin());
   arWf[arWf.size()-1] = CC_0;

   if (  arWf.Norm() == C_0
      )
   {
      MIDASERROR("Initial modals (+ phase) have zero coefficients!");
   }

   // Normalize
   this->NormalizeModals(arWf);
}

/**
 * Implementation of ShapedZeroVector
 *
 * @return
 *    Vector of correct shape
 **/
typename LinearTdH::vec_t LinearTdH::ShapedZeroVectorImpl
   (
   )  const
{
   return vec_t(this->mNOccModalCoef + I_1, CC_0);
}

/**
 * Construct one-mode Fock matrix (with constraint operator)
 *
 * @param aModeNr    LocalModeNr for given mode
 * @param arNScreen    Accumulated number of screened terms
 * @return
 *    Fock matrix
 **/
typename LinearTdH::mat_t LinearTdH::ConstructEffectiveOneModeHamiltonian
   (  LocalModeNr aModeNr
   ,  size_t& arNScreen
   )  const
{
   LOGCALL("h_eff");
   In nbas = this->NBas(aModeNr);

   // Init result
   mat_t result(nbas, CC_0);

   const auto& zero = this->mpTdHCalcDef->GetScreenZeroCoef();

   size_t nscreen = 0;

   if (  this->mpOpDef->UseActiveTermsAlgo()
      )
   {
      In n_act_terms = this->mpOpDef->NactiveTerms(aModeNr);

      // Loop over active terms t in t_act.
      #pragma omp parallel
      {
         // Pre-allocations
         In i_term = I_0;
         In i_fac_thismode = -I_1;
         param_t zt(0.);
         LocalModeNr i_op_mode = I_0;
         LocalOperNr i_oper = I_0;
         mat_t result_thisterm(nbas);

         #pragma omp for schedule(dynamic) reduction(ComplexMidasMatrix_Add : result), reduction(+ : nscreen)
         for(In i_act_term=I_0; i_act_term<n_act_terms; ++i_act_term)
         {
            i_term = mpOpDef->TermActive(aModeNr, i_act_term);

            // Get c_t
            zt = mpOpDef->Coef(i_term);

            // Screening
            if (  std::abs(zt) < zero
               )
            {
               ++nscreen;
               continue;
            }

            i_fac_thismode = -I_1;

            // Loop over factors in active term t (1 for SHO).
            for(In i_fac=I_0; i_fac<mpOpDef->NfactorsInTerm(i_term); ++i_fac)
            {
               i_op_mode  = mpOpDef->ModeForFactor(i_term, i_fac);
               if (  i_op_mode == aModeNr
                  )
               {
                  i_fac_thismode = i_fac;
               }
               else
               {
                  i_oper = mpOpDef->OperForFactor(i_term, i_fac);

                  // Multiply c_t factor with occupied integral to finally obtain z_t = c_t*Prod(h^{m'})
                  zt *= this->mIntegrals->GetOccIntegral(i_op_mode, i_oper);
               }
            }

            if (  i_fac_thismode > -I_1
               )
            {
               // Set intermediate result to primitive integrals.
               i_oper = this->mpOpDef->OperForFactor(i_term, i_fac_thismode);
               result_thisterm = this->mIntegrals->GetTimeIndependentIntegrals(aModeNr, i_oper);

               // Scale by z_t factor
               result_thisterm.Scale(zt);

               // Modify by constraint operator
               switch   (  this->mConstraint
                        )
               {
                  case midas::tdh::constraintID::ZERO:
                  {
                     // Subtract occupied time-dependent integrals from diagonal if we are using zero constraint
                     for(In idiag=I_0; idiag<nbas; ++idiag)
                     {
                        result_thisterm[idiag][idiag] -= zt*this->mIntegrals->GetOccIntegral(aModeNr, i_oper);
                     }
                     break;
                  }
                  case midas::tdh::constraintID::FOCK:
                  {
                     // With Fock constraint, we don't do anything
                     break;
                  }
                  default:
                  {
                     MIDASERROR("Unrecognized constraint operator!");
                  }
               }

               // Add to result
               result += result_thisterm;
            }
            else
            {
               MIDASERROR("LinearTdH::ConstructEffectiveOneModeHamiltonian: i_fac_thismode = " + std::to_string(i_fac_thismode) + ". This cannot happen!");
            }
         }
      }
   }
   else 
   {
      MIDASERROR("Why should we not use active terms???");
   }

   // Increment screening statistics
   arNScreen += nscreen;

   // Return result
   return result;
}


/**
 * Implementation of CalculateAutoCorr
 *
 * @param aT      t
 * @param aY      y(t) vector
 * @return
 *    S(t)
 **/
typename LinearTdH::param_t LinearTdH::CalculateAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   param_t ac_result = CC_1;
   for(LocalModeNr i_op_mode=0; i_op_mode<mNModes; ++i_op_mode)
   {
      In nbas = this->NBas(i_op_mode);

      vec_t t0_for_mode(nbas); 
      this->mInitialOccModals.PieceIo(IO_GET, t0_for_mode, nbas, mOccModalOffsets[i_op_mode]);
      vec_t occ_for_mode(nbas); 
      aY.PieceIo(IO_GET, occ_for_mode, nbas, mOccModalOffsets[i_op_mode]);

      ac_result *= Dot(t0_for_mode, occ_for_mode);
   }

   // Get phase (we assume F(t=0) = 0)
   param_t phase = aY[aY.size()-1];

   ac_result *= (std::cos(phase)-CC_I*std::sin(phase));

   return ac_result;
}

/**
 * Implementation of CalculateHalfTimeAutoCorr
 *
 * @param aT      t
 * @param aY      y(t) vector
 * @return
 *    S(2*t)
 **/
typename LinearTdH::param_t LinearTdH::CalculateHalfTimeAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   param_t ac_result = CC_1;

   for(LocalModeNr i_op_mode=0; i_op_mode<mNModes; ++i_op_mode)
   {
      In nbas = this->NBas(i_op_mode);

      vec_t halftime_for_mode(nbas); 
      aY.PieceIo(IO_GET, halftime_for_mode, nbas, mOccModalOffsets[i_op_mode]);
      vec_t conjhalf_for_mode = halftime_for_mode.Conjugate();

      ac_result *= Dot(conjhalf_for_mode, halftime_for_mode);
   }

   // Get phase at t and use 2*F(t)
   param_t half_phase = aY[aY.size()-1];
   ac_result *= (std::cos(C_2*half_phase)-CC_I*std::sin(C_2*half_phase));

   return ac_result;
}

/**
 * Finalize. Calculate properties, etc.
 *
 * @param aTs     Vector of ts for all time steps
 * @param aYs     Vector of ys for all time steps
 **/
void LinearTdH::FinalizeImpl
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )
{
   // We have no additional stuff to do for LinearTdH yet...
   return;
}


/**
 * Get coefficient vector for a single modal in the time-independent basis.
 *
 * @param aModeNr    Mode number
 * @param aT         t
 * @param aWfParams  Wave-function parameters
 * @return
 *    Value of wave function at Q = aQ
 **/
typename LinearTdH::vec_t LinearTdH::GetWaveFunctionCoefficientsImpl
   (  LocalModeNr aModeNr
   ,  step_t aT
   ,  const vec_t& aWfParams
   )  const
{
   GlobalModeNr i_g_mode  = this->mpOpDef->GetGlobalModeNr(aModeNr);
   LocalModeNr i_bas_mode = this->mpBasDef->GetLocalModeNr(i_g_mode);
   In nbas = this->NBas(i_bas_mode);

   vec_t occ_for_mode(nbas); 
   aWfParams.PieceIo(IO_GET, occ_for_mode, nbas, mOccModalOffsets[aModeNr]);

   return occ_for_mode;
}

/**
 * Get phase from vector
 *
 * @param aT
 * @param aY
 * @return
 *    F(aT)
 **/
typename LinearTdH::param_t LinearTdH::GetPhaseFactorImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   return aY[aY.size()-I_1];
}


/**
 * Update H integrals
 * @param aY
 **/
void LinearTdH::PrepareDerivativeCalculationImpl
   (  const vec_t& aY
   )
{
   LOGCALL("prepare deriv");
   this->mIntegrals->Update(aY, this->mOccModalOffsets);
}

/**
 * Update property integrals
 * @param aY
 **/
void LinearTdH::UpdatePropertyIntegralsImpl
   (  const vec_t& aY
   )
{
   for(const auto& in : this->mPropertyIntegrals)
   {
      in.second->Update(aY, this->mOccModalOffsets);
   }
}

/**
 * Number of parameters for mode
 *
 * @param aMode
 * @return
 *    NParams for aMode
 **/
In LinearTdH::NParams
   (  LocalModeNr aMode
   )  const
{
   return this->NBas(aMode);
}

/**
 * Offset for mode
 *
 * @param aMode
 * @return
 *    Offset
 **/
In LinearTdH::Offset
   (  LocalModeNr aMode
   )  const
{
   if(aMode < mOccModalOffsets.size())
   {
      return this->mOccModalOffsets[aMode];
   }
   else
   {
      return 0;
   }
}

/**
 * Transform initial state when using spectral basis of general operator.
 *
 * @param aModals
 **/
DataCont LinearTdH::ConstructSpectralBasisTransformation
   (  const DataCont& aModals
   )  const
{
   return aModals;
}
