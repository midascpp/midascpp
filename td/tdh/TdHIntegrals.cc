/*
************************************************************************
*
* @file                 TdHIntegrals.cc
*
* Created:              16-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Time-dependent integrals. Implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "input/TdHCalcDef.h"
#include "input/Input.h"
#include "TdHIntegrals.h"

#include "util/Math.h"
#include "util/CallStatisticsHandler.h"

#include "mpi/Impi.h"

/**
 * @param apCalcDef
 * @param apOpDef
 * @param apBasDef
 **/
TdHIntegralsBase::TdHIntegralsBase
   (  const input::TdHCalcDef* const apCalcDef
   ,  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
   :  midas::td::PrimitiveIntegrals<std::complex<Nb>>
         (  apOpDef
         ,  apBasDef
         )
   ,  mpOpDef(apOpDef)
#ifdef VAR_MPI
   ,  mMpiComm
         (  apCalcDef->GetMpiComm()
         )
#endif /* VAR_MPI */
{
   // Set up size of occupied integrals
   mOneModeIntOcc.resize(apBasDef->GetmNoModesInBas());
   for (LocalModeNr i_op_mode = I_0; i_op_mode < apBasDef->GetmNoModesInBas(); ++i_op_mode)
   {
      auto n_opers = apOpDef->NrOneModeOpers(i_op_mode);
      mOneModeIntOcc[i_op_mode].resize(n_opers);
   }
}

/**
 * @return
 *    Type
 **/
TdHIntegralsBase::tdhintType TdHIntegralsBase::Type
   (
   )  const
{
   return TdHIntegralsBase::tdhintType::BASE;
}

/**
 * Factory
 *
 * @param apCalcDef
 * @param apOpDef
 * @param apBasDef
 *
 * @return
 *    pointer to integrals
 **/
std::unique_ptr<TdHIntegralsBase> TdHIntegralsBase::Factory
   (  input::TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   )
{
   switch   (  apCalcDef->GetParametrization()
            )
   {
      case midas::tdh::tdhID::LINEAR:
      {
         return std::make_unique<TdHIntegralsBase>(apCalcDef, apOpDef, apBasDef);
         break;
      }
      case midas::tdh::tdhID::EXPONENTIAL:
      {
         return std::make_unique<ExponentialTdHIntegrals>(apCalcDef, apOpDef, apBasDef);
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized TDH parametrization!");
         return nullptr;
      }
   }
}

/**
 * Update integrals (interface)
 *
 * @param aY            y vector
 * @param aOffsets      Offsets in y vector (stupid...)
 * @param aKappaNorms   Norms of kappa vectors. Only needed for ExponentialTdHIntegrals
 **/
void TdHIntegralsBase::Update
   (  const vec_t& aY
   ,  const std::vector<In>& aOffsets
   ,  const std::vector<step_t>& aKappaNorms
   )
{
   return this->UpdateImpl(aY, aOffsets, aKappaNorms);
}

/**
 * Update integrals. Default implementation.
 *
 * @param aY         y vector
 * @param aOffsets   Offsets in y vector (stupid...)
 * @param aVoid      Not needed here
 **/
void TdHIntegralsBase::UpdateImpl
   (  const vec_t& aY
   ,  const std::vector<In>& aOffsets
   ,  const std::vector<step_t>& aVoid
   )
{
   LOGCALL("lin int update");
   In nmodes = this->NModes();

#ifdef VAR_MPI
   auto iproc = midas::mpi::GlobalRank();
   auto nproc = midas::mpi::GlobalSize();
   for(In imode=iproc; imode<nmodes; imode+=nproc)
#else
   for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
#endif /* VAR_MPI */
   {
      In nbas = this->NBas(imode);
      In n_op = this->NOper(imode);

      // Get modal coefficients for current mode
      vec_t occ_for_mode(nbas);
      aY.PieceIo(IO_GET, occ_for_mode, nbas, aOffsets[imode]);
      
      #pragma omp parallel for
      for(In i_op=I_0; i_op<n_op; ++i_op)
      {
         // Calculate transformed integral
         this->mOneModeIntOcc[imode][i_op] = this->OneModeIntBraket(occ_for_mode, imode, i_op);
      }
   }

#ifdef VAR_MPI
   // Sync between nodes
   this->SyncTimeDependentIntegrals();
#endif /* VAR_MPI */
}


/**
 * Calculate expectation value.
 * NB: Update must be called before!
 *
 * @param aModeNr    Only include terms active for this mode. If set to -1, all terms are included (default).
 *
 * @return
 *    Expt. value of operator
 **/
typename TdHIntegralsBase::param_t TdHIntegralsBase::ExpectationValue
   (  LocalModeNr aModeNr
   )  const
{
   param_t result = param_t(0.);

   if (  aModeNr == -I_1
      )
   {
      for(In i_term=I_0; i_term<this->mpOpDef->Nterms(); ++i_term)
      {
         // Init z_t to c_t
         param_t zt = this->mpOpDef->Coef(i_term);
         for(In i_fac=I_0; i_fac<mpOpDef->NfactorsInTerm(i_term); ++i_fac)
         {
            LocalModeNr i_op_mode = mpOpDef->ModeForFactor(i_term, i_fac);
            LocalOperNr i_oper    = mpOpDef->OperForFactor(i_term, i_fac);
            zt *= this->GetOccIntegral(i_op_mode, i_oper);
         }

         result += zt;
      }
   }
   else
   {
      // We use active terms in order to exclude operators that do not affect the given mode.
      for(In i_act_term=I_0; i_act_term<this->mpOpDef->NactiveTerms(aModeNr); ++i_act_term)
      {
         In i_term = this->mpOpDef->TermActive(aModeNr, i_act_term);

         // Init z_t to c_t
         param_t zt = this->mpOpDef->Coef(i_term);
         for(In i_fac=I_0; i_fac<this->mpOpDef->NfactorsInTerm(i_term); ++i_fac)
         {
            LocalModeNr i_op_mode = this->mpOpDef->ModeForFactor(i_term, i_fac);
            LocalOperNr i_oper    = this->mpOpDef->OperForFactor(i_term, i_fac);
            zt *= this->GetOccIntegral(i_op_mode, i_oper);
         }
         result += zt;
      }
   }

   return result;
}

/**
 * Get integral
 *
 * @param aModeNr
 * @param aOperNr
 *
 * @return
 *    Occupied time-dependent integral
 **/
typename TdHIntegralsBase::param_t TdHIntegralsBase::GetOccIntegral
   (  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  const
{
   return this->mOneModeIntOcc[aModeNr][aOperNr];
}




/**
 * Constructor for ExponentialTdHIntegrals
 *
 * @param apCalcDef
 * @param apOpDef
 * @param apBasDef
 **/
ExponentialTdHIntegrals::ExponentialTdHIntegrals
   (  const input::TdHCalcDef* const apCalcDef
   ,  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
   :  TdHIntegralsBase(apCalcDef, apOpDef, apBasDef)
{
   // Set up size of virtual-occupied integrals
   size_t nmodes = apBasDef->GetmNoModesInBas();
   mOneModeIntVirOcc.resize(nmodes);
   for(LocalModeNr i_op_mode=I_0; i_op_mode<nmodes; ++i_op_mode)
   {
      In nkappas = this->NBas(i_op_mode) - I_1;
      In nopers = this->NOper(i_op_mode);

      mOneModeIntVirOcc[i_op_mode].resize(nopers, vec_t(nkappas, param_t(0.)));
   }
}

/**
 * Get vir-occ integrals
 *
 * @param aModeNr
 * @param aOperNr
 *
 * @return
 *    v-o integrals
 **/
const typename ExponentialTdHIntegrals::vec_t& ExponentialTdHIntegrals::GetVirOccIntegrals
   (  LocalModeNr aModeNr
   ,  LocalOperNr aOperNr
   )  const
{
   return this->mOneModeIntVirOcc[aModeNr][aOperNr];
}

/**
 * Update time-dependent integrals.
 *
 * @param aKappas       Vector of kappa parameters (and phase as last element)
 * @param aOffsets      Offsets. Where do kappas for mode m begin?
 * @param aKappaNorms   Norms of kappa vectors
 **/
void ExponentialTdHIntegrals::UpdateImpl
   (  const vec_t& aKappas
   ,  const std::vector<In>& aOffsets
   ,  const std::vector<step_t>& aKappaNorms
   )
{
   LOGCALL("exp int update");

   In nmodes = this->NModes();
   assert(aKappaNorms.size() == nmodes);
   assert(aOffsets.size() == nmodes);

#ifdef VAR_MPI
   auto iproc = midas::mpi::GlobalRank();
   auto nproc = midas::mpi::GlobalSize();
   for(In imode=iproc; imode<nmodes; imode+=nproc)
#else
   for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
#endif /* VAR_MPI */
   {
      // Update one-mode integrals for this mode
      this->UpdateModeIntegrals(imode, aKappas, aOffsets, aKappaNorms);
   }

#ifdef VAR_MPI
   // Copy this->mOneModeIntOcc and this->mOneModeIntVirOcc back and forth
   this->SyncTimeDependentIntegrals();
#endif /* VAR_MPI */
}

/**
 * Update integrals for mode
 *
 * @param aMode         Mode to update
 * @param aKappas       Kappa vector
 * @param aOffsets      Offsets
 * @param aKappaNorms   Norms of kappa vectors
 **/
void ExponentialTdHIntegrals::UpdateModeIntegrals
   (  LocalModeNr aMode
   ,  const vec_t& aKappas
   ,  const std::vector<In>& aOffsets
   ,  const std::vector<step_t>& aKappaNorms
   )
{
   LOGCALL("exp int one-mode int update");

   // Get number of basis functions and operators
   In nbas = this->NBas(aMode);
   In nopers = this->NOper(aMode);

   In nkappas = nbas - I_1;

   In off = aOffsets[aMode];

   // Get kappa norm
   const auto& r = aKappaNorms[aMode];
   auto sinc_r = midas::math::sinc(r);
   auto half_sinc_rhalf_sq = param_t(0.5)*std::pow(midas::math::sinc(0.5*r), 2);
   auto cos_r = std::cos(r);

   #pragma omp parallel
   {
      // Pre-allocations (cannot be optimized correctly for OpenMP build)
      vec_t half_trans_vir_occ(nkappas);
      param_t half_trans_occ(0.);
      param_t ii_prod(0.);
      param_t ai_prod(0.);

      #pragma omp for
      for(In ioper=I_0; ioper<nopers; ++ioper)
      {
         // Resize mOneModeIntVirOcc vec_t
         this->mOneModeIntVirOcc[aMode][ioper].SetNewSize(nkappas);
      
         // Calculate half-transformed integrals
         // h_ii
         ii_prod = param_t(0.);
         for(In a=I_0; a<nkappas; ++a)
         {
            ii_prod += this->GetTimeIndependentIntegral(aMode, ioper, 0, a+1) * aKappas[off+a];
         }
         half_trans_occ = sinc_r*ii_prod + cos_r*this->GetTimeIndependentIntegral(aMode, ioper, 0, 0);

         // h_ai
         half_trans_vir_occ(nkappas);
         for(In a=I_0; a<nkappas; ++a)
         {
            ai_prod = param_t(0.);
            for(In b=I_0; b<nkappas; ++b)
            {
               ai_prod += this->GetTimeIndependentIntegral(aMode, ioper, a+1, b+1) * aKappas[off+b];
            }

            half_trans_vir_occ[a] = sinc_r*ai_prod + cos_r*this->GetTimeIndependentIntegral(aMode, ioper, a+1, 0);
         }

         // Calculate transformed integrals
         // h_ii
         param_t h_ii = param_t(0.);
         for(In a=I_0; a<nkappas; ++a)
         {
            h_ii += midas::math::Conj(aKappas[off+a]) * half_trans_vir_occ[a];
         }
         h_ii *= sinc_r;
         h_ii += cos_r*half_trans_occ;
         this->mOneModeIntOcc[aMode][ioper] = std::move(h_ii);

         // h_ai
         ai_prod = param_t(0.);
         for(In b=I_0; b<nkappas; ++b)
         {
            ai_prod -= midas::math::Conj(aKappas[off+b]) * half_trans_vir_occ[b];
         }
         for(In a=I_0; a<nkappas; ++a)
         {
            param_t h_ai   =  ai_prod * half_sinc_rhalf_sq * aKappas[off+a]
                           +  half_trans_vir_occ[a]
                           -  sinc_r*aKappas[off+a] * half_trans_occ;

            this->mOneModeIntVirOcc[aMode][ioper][a] = std::move(h_ai);
         }
      }
   }
}

/**
 * Reset time-independent integrals.
 * NB: This is not very efficient. Could be done without constructing e^k matrices explicitly.
 *
 * @param aModes        Modes to reset
 * @param aKappa        Kappa vector
 * @param aOffsets      Offsets. Where do kappas for mode m begin?
 * @param aKappaNorms   Norms of kappa vectors
 **/
void ExponentialTdHIntegrals::ResetKappa
   (  const std::set<LocalModeNr>& aModes
   ,  const vec_t& aKappa
   ,  const std::vector<In>& aOffsets
   ,  const std::vector<step_t>& aKappaNorms
   )
{
   LOGCALL("reset kappa");

   // 1) Call UpdateModeIntegrals to get occ-occ and vir-occ
   for(const auto& mode : aModes)
   {
      this->UpdateModeIntegrals(mode, aKappa, aOffsets, aKappaNorms);
   }

   // 2) Loop over modes
   for(const auto& mode : aModes)
   {
      auto& ints_m = this->mTransformedOneModeInt[mode];

      // Definitions
      In nbas = this->NBas(mode);
      In nkappas = nbas - I_1;
      In off = aOffsets[mode];
      const auto& r = aKappaNorms[mode];
      auto sinc_r = midas::math::sinc(r);
      auto cos_r = std::cos(r);
      auto half_sinc_r_half_sq = step_t(0.5)*std::pow(midas::math::sinc(step_t(0.5)*r), 2);

      // 3) Loop over operators
      #pragma omp parallel
      {
         // Pre-allocate stuff for each thread. Otherwise serial performance of OpenMP build will be terrible...
         auto noper = ints_m.size();
         vec_t half_trans_occ_vir(nkappas);
         mat_t half_trans_vir_vir(nkappas);
         param_t ia_prod(0.);
         param_t ab_prod(0.);
         param_t prod(0.);

         #pragma omp for
         for(In oper=I_0; oper<noper; ++oper)
         {
            auto& ints_m_op = ints_m[oper];

            // DEBUG
            if (  gDebug
               )
            {
               Mout  << " Primitive integrals for mode " << mode << " oper " << oper << ":\n" << ints_m_op
                     << std::flush;
            }

            // Calculate intermediates for vir-vir block
            ia_prod = param_t(CC_0);      
            for(In b=I_0; b<nkappas; ++b)
            {
               ia_prod += this->GetTimeIndependentIntegral(mode, oper, I_0, b+I_1)*aKappa[off+b];
            }
            for(In a=I_0; a<nkappas; ++a)
            {
               half_trans_occ_vir[a]   =  this->GetTimeIndependentIntegral(mode, oper, I_0, a+I_1)
                                       -  sinc_r*this->GetTimeIndependentIntegral(mode, oper, I_0, I_0)*midas::math::Conj(aKappa[off+a])
                                       -  half_sinc_r_half_sq * midas::math::Conj(aKappa[off+a]) * ia_prod;
            }

            for(In a=I_0; a<nkappas; ++a)
            {
               ab_prod = param_t(CC_0);
               for(In c=I_0; c<nkappas; ++c)
               {
                  ab_prod += this->GetTimeIndependentIntegral(mode, oper, a+I_1, c+I_1) * aKappa[off+c];
               }
               for(In b=I_0; b<nkappas; ++b)
               {
                  half_trans_vir_vir[a][b]   =  this->GetTimeIndependentIntegral(mode, oper, a+I_1, b+I_1)
                                             -  sinc_r*this->GetTimeIndependentIntegral(mode, oper, a+I_1, I_0)*midas::math::Conj(aKappa[off+b])
                                             -  half_sinc_r_half_sq * midas::math::Conj(aKappa[off+b]) * ab_prod;
               }
            }

            // Set vir-vir block
            for(In b=I_0; b<nkappas; ++b)
            {
               prod = param_t(CC_0);
               for(In c=I_0; c<nkappas; ++c)
               {
                  prod += half_trans_vir_vir[c][b] * midas::math::Conj(aKappa[off+c]);
               }
               for(In a=I_0; a<nkappas; ++a)
               {
                  ints_m_op[a+I_1][b+I_1]    =  half_trans_vir_vir[a][b]
                                             -  sinc_r * aKappa[off+a] * half_trans_occ_vir[b]
                                             -  half_sinc_r_half_sq * aKappa[off+a] * prod;
               }
            }

            // Set occ-vir block (in case the operator is non-Hermitian, this is the general way)
            for(In a=I_0; a<nkappas; ++a)
            {
               prod = param_t(CC_0);
               for(In b=I_0; b<nkappas; ++b)
               {
                  prod += half_trans_vir_vir[b][a] * midas::math::Conj(aKappa[off+b]);
               }

               ints_m_op[I_0][a+I_1] = cos_r*half_trans_occ_vir[a] + sinc_r*prod;
            }


            // Set occ and vir-occ blocks (calculated in UpdateImpl)
            ints_m_op[I_0][I_0] = this->mOneModeIntOcc[mode][oper];
            for(In a=I_0; a<nkappas; ++a)
            {
               ints_m_op[a+I_1][I_0] = this->mOneModeIntVirOcc[mode][oper][a];
            }

            // DEBUG
            if (  gDebug
               )
            {
               Mout  << " Transformed integrals for mode " << mode << " oper " << oper << ":\n" << ints_m_op
                     << " VirOcc:   " << this->mOneModeIntVirOcc[mode][oper]
                     << " OccOcc:   " << this->mOneModeIntOcc[mode][oper]
                     << std::endl;
            }
         }
      }
   }
}

/**
 * @return
 *    Type
 **/
TdHIntegralsBase::tdhintType ExponentialTdHIntegrals::Type
   (
   )  const
{
   return TdHIntegralsBase::tdhintType::EXPONENTIAL;
}


/*********************************************************************************************************
 * MPI stuff
 ********************************************************************************************************/
#ifdef VAR_MPI

/**
 * Check if given node should calculate integrals.
 *
 * @param aMode
 * @return
 *    True or false
 **/
bool TdHIntegralsBase::AssignToNode
   (  LocalModeNr aMode
   )  const
{
   // Get number of MPI processes
   auto nproc = midas::mpi::GlobalSize();

   return (midas::mpi::GlobalRank() == aMode % nproc);
}

/**
 * Sync time-dependent integrals between nodes
 **/
void TdHIntegralsBase::SyncTimeDependentIntegrals
   (
   )
{
   auto nproc = midas::mpi::GlobalSize();
   auto nmodes = this->NModes();

   switch   (  this->mMpiComm
            )
   {
      case midas::tdh::mpiComm::BCAST:
      {
         for(LocalModeNr jmode=I_0; jmode<nmodes; ++jmode)
         {
            auto rank = jmode % nproc;
            this->BcastTimeDependentIntegrals(jmode, rank);
         }

         break;
      }
      case midas::tdh::mpiComm::IBCAST:
      {
         // Make vector of requests
         std::vector<std::vector<MPI_Request> > reqs(nmodes);
         for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
         {
            reqs[imode].resize(this->Type() == tdhintType::EXPONENTIAL ? this->NOper(imode) + I_1 : I_1);
         }

         // Ibcast
         for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
         {
            auto rank = imode % nproc;
            this->IbcastTimeDependentIntegrals(imode, rank, reqs[imode]);
         }

         // Wait
         for(LocalModeNr imode=I_0; imode<nmodes; ++imode)
         {
            for(auto& r : reqs[imode])
            {
               midas::mpi::detail::WRAP_Wait(&r, MPI_STATUS_IGNORE);
            }
         }

         break;
      }
      default:
      {
         MIDASERROR("Unrecognized MPI communication!");
      }
   }
}

/**
 * Receive time-dependent integrals (occ-occ)
 *
 * @param aMode      Recv integrals for mode aMode
 * @param aRank      MPI rank to recv from
 **/
void TdHIntegralsBase::RecvTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )
{
   MPI_Status status;

   // Recv occ-occ integrals
   In noper = this->NOper(aMode);
   void* ptr = const_cast<void*>(static_cast<const void*>(this->mOneModeIntOcc[aMode].data()));
   midas::mpi::detail::WRAP_Recv(ptr, noper, midas::mpi::DataTypeTrait<param_t>::Get(), aRank, 0, MPI_COMM_WORLD, &status);
}

/**
 * Send time-dependent integrals (occ-occ)
 *
 * @param aMode      Send mode-aMode ints
 * @param aRank      Send to rank
 **/
void TdHIntegralsBase::SendTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )  const
{
   // Send occ-occ integrals
   In noper = this->NOper(aMode);
   const void* ptr = static_cast<const void*>(this->mOneModeIntOcc[aMode].data());
   midas::mpi::detail::WRAP_Send(ptr, noper, midas::mpi::DataTypeTrait<param_t>::Get(), aRank, 0, MPI_COMM_WORLD);
}

/**
 * Bcast time-dependent integrals (occ-occ)
 *
 * @param aMode      Bcast mode-aMode ints
 * @param aRank      Bcast from rank
 **/
void TdHIntegralsBase::BcastTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )
{
   // Send occ-occ integrals
   In noper = this->NOper(aMode);
   void* ptr = const_cast<void*>(static_cast<const void*>(this->mOneModeIntOcc[aMode].data()));
   midas::mpi::detail::WRAP_Bcast(ptr, noper, midas::mpi::DataTypeTrait<param_t>::Get(), aRank, MPI_COMM_WORLD);
}

/**
 * Ibcast time-dependent integrals (occ-occ)
 *
 * @param aMode      Bcast mode-aMode ints
 * @param aRank      Bcast from rank
 * @param arReq      Vector of requests (only use first element!)
 **/
void TdHIntegralsBase::IbcastTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   ,  std::vector<MPI_Request>& arReq
   )
{
   // Send occ-occ integrals
   In noper = this->NOper(aMode);
   void* ptr = const_cast<void*>(static_cast<const void*>(this->mOneModeIntOcc[aMode].data()));
   midas::mpi::detail::WRAP_Ibcast(ptr, noper, midas::mpi::DataTypeTrait<param_t>::Get(), aRank, MPI_COMM_WORLD, &arReq[0]);
}

/**
 * Receive time-dependent integrals (occ-occ and vir-occ)
 *
 * @param aMode      Recv integrals for mode aMode
 * @param aRank      MPI rank to recv from
 **/
void ExponentialTdHIntegrals::RecvTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )
{
   // Get occ-occ integrals
   TdHIntegralsBase::RecvTimeDependentIntegrals(aMode, aRank);

   // Get vir-occ integrals
   In noper = this->NOper(aMode);
   for(LocalOperNr ioper=I_0; ioper<noper; ++ioper)
   {
      this->mOneModeIntVirOcc[aMode][ioper].MpiRecv(aRank);
   }
}

/**
 * Send time-dependent integrals (occ-occ and vir-occ)
 *
 * @param aMode      Send mode-aMode ints
 * @param aRank      Send to rank
 **/
void ExponentialTdHIntegrals::SendTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )  const
{
   // Send occ-occ
   TdHIntegralsBase::SendTimeDependentIntegrals(aMode, aRank);

   // Send vir-occ
   In noper = this->NOper(aMode);
   for(LocalOperNr ioper=I_0; ioper<noper; ++ioper)
   {
      this->mOneModeIntVirOcc[aMode][ioper].MpiSend(aRank);
   }
}

/**
 * Bcast time-dependent integrals (occ-occ and vir-occ)
 *
 * @param aMode      Bcast integrals for mode aMode
 * @param aRank      Source rank
 **/
void ExponentialTdHIntegrals::BcastTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   )
{
   // Get occ-occ integrals
   TdHIntegralsBase::BcastTimeDependentIntegrals(aMode, aRank);

   // Get vir-occ integrals
   In noper = this->NOper(aMode);
   for(LocalOperNr ioper=I_0; ioper<noper; ++ioper)
   {
      this->mOneModeIntVirOcc[aMode][ioper].MpiBcast(aRank);
   }
}

/**
 * Ibcast time-dependent integrals (occ-occ and vir-occ)
 *
 * @param aMode      Bcast integrals for mode aMode
 * @param aRank      Source rank
 * @param arReq      Vector of MPI_Request%s
 **/
void ExponentialTdHIntegrals::IbcastTimeDependentIntegrals
   (  LocalModeNr aMode
   ,  In aRank
   ,  std::vector<MPI_Request>& arReq
   )
{
   // Get occ-occ integrals (uses first element of arReq!!!)
   TdHIntegralsBase::IbcastTimeDependentIntegrals(aMode, aRank, arReq);

   // Get vir-occ integrals
   In noper = this->NOper(aMode);
   for(LocalOperNr ioper=I_0; ioper<noper; ++ioper)
   {
      this->mOneModeIntVirOcc[aMode][ioper].MpiIbcastElements(aRank, &arReq[ioper+I_1]);
   }
}


#endif /* VAR_MPI */
