/*
************************************************************************
*
* @file                 ExponentialTdH.cc
*
* Created:              06-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    ExponentialTdH class implementation
*
* Last modified:        
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "ExponentialTdH.h"
#include "input/TdHCalcDef.h"
#include "ode/ode_exceptions.h"
#include "util/CallStatisticsHandler.h"
#include "mmv/mmv_omp_pragmas.h"
#include "util/Math.h"
#include "input/Input.h"

/**
 * Constructor
 *
 * @param apCalcDef     TdH calcdef
 * @param apOpDef       Operator defs
 * @param apBasDef      Basis defs
 * @param aModalsName   Name of file containing initial modals
 **/
ExponentialTdH::ExponentialTdH
   (  input::TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  const std::string& aModalsName
   )
   :  TdHBase
         (  apCalcDef
         ,  apOpDef
         ,  apBasDef
         ,  aModalsName
         )
   ,  mNKappas
         (  mNOccModalCoef - mNModes
         )
   ,  mKappaOffsets
         (  mNModes
         )
   ,  mDerivativeProjectionTerms
         (  apCalcDef->GetExpTdHDerivativeProjectionTerms()
         )
   ,  mKappaNormThreshold
         (  apCalcDef->GetKappaNormThreshold()
         )
   ,  mMaxFailedDerivatives
         (  apCalcDef->GetExpTdHMaxFailedDerivatives()
         )
   ,  mKappaNorms
         (  mNModes
         )
{
   // Set kappa offsets
   In off = I_0;
   for(LocalModeNr imode=I_0; imode<mNModes; ++imode)
   {
      this->mKappaOffsets[imode] = off;
      off += (this->NBas(imode) - I_1);
   }
}

/**
 *  Calculate derivative vector (dy/dt)
 *  NB: PrepareDerivativeCalculation must have been called first (as done in TdHBase)!
 *
 *  @param aT     t
 *  @param aY     y
 *  @param arNScreen     Accumulated number of screened terms
 *
 *  @return
 *    dy/dt
 **/
typename ExponentialTdH::vec_t ExponentialTdH::DerivativeImpl
   (  step_t aT
   ,  const vec_t& aY
   ,  size_t& arNScreen
   )  const
{
   LOGCALL("deriv");

   vec_t result(mNKappas+I_1);
   param_t phase(0.);

   size_t nscreen = 0;

#ifdef VAR_MPI
   In iproc = midas::mpi::GlobalRank();
   auto range = this->MpiModeRange(iproc);
   for(In imode=range.first; imode<range.second; ++imode)
#else
   for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
#endif /* VAR_MPI */
   {
      // Set up some stuff
      In nkappas = this->NBas(imode) - I_1;
      In off = this->mKappaOffsets[imode];
      const auto& r = this->mKappaNorms[imode];
      auto sinc_r = midas::math::sinc(r);

      bool r_eq_pi_half = libmda::numeric::float_eq(r, step_t(C_PI/C_2), 1000000);

      // DEBUG
      if (  this->mIoLevel > 14
         || gDebug
         || r_eq_pi_half
         )
      {
         Mout  << "    Mode:           " << imode << "\n"
               << "    R_m:            " << r << "\n"
               << "    sinc(R_m):      " << sinc_r << "\n"
               << std::flush;
      }

      // Get kappa for mode
      vec_t kappa_for_mode(nkappas);
      aY.PieceIo(IO_GET, kappa_for_mode, nkappas, off);

      // Calculate b vector
      vec_t b_vec = this->CalculateBVector(imode, nscreen);
      param_t phase_term(0.); // This is zero for r=0

      // If R is zero, the derivative is simply b_vec
      if (  libmda::numeric::float_numeq_zero(r, step_t(1.))
         )
      {
         result.PieceIo(IO_PUT, b_vec, nkappas, off);
      }
      else
      {
         param_t gamma_b_dot = Dot(kappa_for_mode, b_vec) / r;
         auto cos_r = std::cos(r);

         if (  libmda::numeric::float_numeq_zero(cos_r, C_1)
            )
         {
            MidasWarning("ExponentialTdH: cos(R) is zero. We have a singularity in kappadot!");
            Mout  << " R:        " << r << "\n"
                  << " cos(R):   " << cos_r << "\n"
                  << " Im[y]:    " << std::imag(gamma_b_dot) << "\n"
                  << std::flush;
         }

         // This is a debug option anyway, so the Mout statements are not enclosed
         if (  this->mDerivativeProjectionTerms
            )
         {
            // Calculate P1 projection
            auto sinc_2r = midas::math::sinc(C_2*r);
            vec_t sinc2r_kdot_p1 = kappa_for_mode;
            sinc2r_kdot_p1.Scale(CC_I*std::imag(gamma_b_dot)/r);

            Mout  << " P1 term: sinc(2R) kdot_P1 = RHS_P1 \n"
                  << "    sinc(2R)     = " << sinc_2r << "\n"
                  << "    ||RHS_P1||   = " << sinc2r_kdot_p1.Norm() << "\n"
                  << std::flush;

            // Calculate P2 projection
            vec_t kdot_p2 = kappa_for_mode;
            kdot_p2.Scale(std::real(gamma_b_dot)/r);

            Mout  << " P2 term: kdot_P2 = RHS_P2 \n"
                  << "    ||RHS_P2||   = " << kdot_p2.Norm() << "\n"
                  << std::flush;

            // Calculate Q projection
            vec_t sincr_kdot_q = b_vec;
            Axpy(sincr_kdot_q, kappa_for_mode, -gamma_b_dot/r);

            Mout  << " Q term: sinc(R) kdot_Q = RHS_Q \n"
                  << "    sinc(R)     = " << sinc_r << "\n"
                  << "    ||RHS_Q||   = " << sincr_kdot_q.Norm() << "\n"
                  << std::flush;

            // Sum the contributions (in kdot_p2)
            Axpy(kdot_p2, sincr_kdot_q, param_t(1.)/sinc_r);
            Axpy(kdot_p2, sinc2r_kdot_p1, param_t(1.)/sinc_2r);

            
            // Set result
            result.PieceIo(IO_PUT, kdot_p2, nkappas, off);
         }
         else
         {
            // Niels: We assume that r is not equal to pi/2
            b_vec.Scale(r);
            param_t coef = CC_I*std::imag(gamma_b_dot)/cos_r + std::real(gamma_b_dot)*sinc_r - gamma_b_dot;
            Axpy(b_vec, kappa_for_mode, coef);
            b_vec.Scale(param_t(1.)/std::sin(r));

            result.PieceIo(IO_PUT, b_vec, nkappas, off);

            // DEBUG
            if (  this->mIoLevel > 14
               || gDebug
               || r_eq_pi_half
               )
            {
               Mout  << "    cos(R):         " << cos_r << "\n"
                     << "    y:              " << gamma_b_dot << "\n"
                     << "    coef:           " << coef << "\n"
                     << std::flush;
            }
         }

         // Set phase term
         phase_term = std::tan(r) * std::imag(gamma_b_dot);
//         phase_term = std::pow(sinc_r, 2) * std::imag(Dot(kappa_for_mode, b_vec));
      }

      // Add term to phase factor (b_vec contains kappa_dot at this point)
      phase += phase_term;

      // DEBUG
      if (  this->mIoLevel > 14
         || gDebug
         )
      {
         Mout  << "    ||kappa_dot||:  " << b_vec.Norm() << "\n"
               << "    phase term:     " << phase_term << "\n"
               << "    acc. phase:     " << phase << "\n"
               << std::flush;
      }
   }

#ifdef VAR_MPI
   // After this, phase and nscreen are the same for all nodes.
   this->SyncData(result, phase, nscreen);
#endif /* VAR_MPI */

   // Increment arNScreen
   arNScreen += nscreen;

   // Add energy contribution to phase
   phase += this->CalculateEnergyImpl();

   // Set phase
   result[result.size()-I_1] = phase;

   // Scale by -i, if we do imaginary-time propagation
   if (  this->mImagTime
      )
   {
      result.Scale(CC_M_I);
   }

   // DEBUG
   if (  this->mIoLevel > 15
      || gDebug
      )
   {
      Mout  << " Kappa vector:\n" << aY << "\n"
            << " Derivative:\n" << result << "\n"
            << std::flush;
   }

   return result;
}

/**
 * Calculate b vector for mode.
 * b_a = -i * tilde{F}_{ai}      (the Fock operator is in the time-dependent basis)
 *
 * @param aModeNr
 * @param arNScreen     Accumulated number of screened terms
 * @return
 *    b vector for mode
 */
typename ExponentialTdH::vec_t ExponentialTdH::CalculateBVector
   (  LocalModeNr aModeNr
   ,  size_t& arNScreen
   )  const
{
   LOGCALL("b-vec");
   In nkappas = this->NBas(aModeNr) - I_1;
   const auto& zero = this->mpTdHCalcDef->GetScreenZeroCoef();

   // Init result
   vec_t result(nkappas, param_t(0.));

   // Tmp for screening
   size_t nscreen = 0;

   if (  this->mpOpDef->UseActiveTermsAlgo()
      )
   {
      In n_act_terms = this->mpOpDef->NactiveTerms(aModeNr);
      // Loop over active terms t in t_act.
      #pragma omp parallel
      {
         // Pre-allocations
         In i_term = I_0;
         In i_fac_thismode = -I_1;
         param_t zt(0.);
         LocalModeNr i_op_mode = I_0;
         LocalOperNr i_oper = I_0;

         #pragma omp for schedule(dynamic) reduction(ComplexMidasVector_Add : result), reduction(+ : nscreen)
         for(In i_act_term=I_0; i_act_term<n_act_terms; ++i_act_term)
         {
            i_term = this->mpOpDef->TermActive(aModeNr, i_act_term);

            // Get c_t
            zt = this->mpOpDef->Coef(i_term);

            // Screening
            if (  std::abs(zt) < zero
               )
            {
               ++nscreen;
               continue;
            }

            i_fac_thismode = -I_1;

            // Loop over factors in active term t (1 for SHO).
            for(In i_fac=I_0; i_fac<this->mpOpDef->NfactorsInTerm(i_term); ++i_fac)
            {
               i_op_mode  = this->mpOpDef->ModeForFactor(i_term, i_fac);
               if (  i_op_mode == aModeNr
                  )
               {
                  i_fac_thismode = i_fac;
               }
               else
               {
                  i_oper = this->mpOpDef->OperForFactor(i_term, i_fac);

                  // Multiply c_t factor with occupied integral to finally obtain z_t = c_t*Prod(h^{m'})
                  zt *= this->mIntegrals->GetOccIntegral(i_op_mode, i_oper);
               }
            }

            if (  i_fac_thismode > -I_1
               )
            {
               // Get integrals
               i_oper = this->mpOpDef->OperForFactor(i_term, i_fac_thismode);
               const auto& h_ai = static_cast<ExponentialTdHIntegrals*>(this->mIntegrals.get())->GetVirOccIntegrals(aModeNr, i_oper);

               Axpy(result, h_ai, zt);
            }
            else
            {
               MIDASERROR("ExponentialTdH::CalculateBVector: i_fac_thismode = " + std::to_string(i_fac_thismode) + ". This cannot happen!");
            }
         }
      }
   }
   else 
   {
      MIDASERROR("Why should we not use active terms???");
   }

   // Remember to scale by -i
   result.Scale(CC_M_I);

   // Increment screening
   arNScreen += nscreen;

   return result;
}


/**
 * Implementation of process new vector.
 * Simply normalizes the modals.
 *
 * @param arY           y vector
 * @param aYOld         Previous y vector
 * @param arT           t
 * @param aTOld         Previous t
 * @param arDyDt        dy/dt
 * @param aDyDtOld      Previous dy/dt
 * @param arStep        Step size (modified if step is rejected)
 * @param arNDerivFail  Number of failed derivative calculations before this step.
 * @return
 *    Success?
 **/
bool ExponentialTdH::ProcessNewVectorImpl
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )
{
   // If the derivative has failed
   if (  arNDerivFail > 0
      )
   {
      // Try reducing the step size
      if (  arNDerivFail <= this->mMaxFailedDerivatives
         )
      {
         if (  gDebug
            || this->mIoLevel >= I_10
            )
         {
            Mout  << " ExponentialTdH::ProcessNewVectorImpl:\n"
                  << "    arNDerivFail:   " << arNDerivFail << "\n"
                  << "    Max:            " << this->mMaxFailedDerivatives << "\n"
                  << std::flush;
         }

         arStep /= step_t(2.);

         // Reset to previous step
         arY = aYOld;
         arT = aTOld;
         arDyDt = aDyDtOld;
      }
      // If that has not worked, reset kappa.
      else
      {
         if (  gDebug
            || this->mIoLevel >= I_8
            )
         {
            Mout  << " ExponentialTdH: Kappa norm exceeded threshold in a derivative calculation. Reset kappa!\n"
                  << std::flush;
         }

         // Kappas and time to save for later calculation of autocorrelation functions, etc.
         auto t_save = aTOld;
         auto y_save = aYOld;

         // Reset to last accepted step
         arT = aTOld;
         arY = aYOld;

         // Zero reset kappas in arY and the others in y_save
         for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
         {
            // If found, we zero in arY
            if (  this->mFailingModes.find(imode) != this->mFailingModes.end()
               )
            {
               this->ZeroKappaVector(arY, imode);
            }
            // Else, we zero in y_save
            else
            {
               this->ZeroKappaVector(y_save, imode);
            }
         }

         // Reset the phase (it is also singular!!!)
         arY[arY.size()-I_1] = param_t(0.);

         // Save t_save, y_save point
         this->mResetKappas.insert(std::make_pair<step_t, vec_t>(std::move(t_save), std::move(y_save)));

         // Transform integrals with old Y vector
         auto r_old = this->CalculateKappaNorms(aYOld);
         static_cast<ExponentialTdHIntegrals*>(this->mIntegrals.get())->ResetKappa(this->mFailingModes, aYOld, this->mKappaOffsets, r_old);
         for(const auto& oper_name : this->mExptValOpers)
         {
            static_cast<ExponentialTdHIntegrals*>(this->mPropertyIntegrals[oper_name].get())->ResetKappa(this->mFailingModes, aYOld, this->mKappaOffsets, r_old);
         }

         // Update dy/dt
         arDyDt = this->Derivative(arT, arY);
      }

      // Reset failing modes
      this->mFailingModes.clear();

      return false;
   }
   else
   {
      return true;
   }
}


/**
 * Prepare integration. Do not do anything since the kappa vector is always zero 
 * to begin with (when we use spectral basis of reference VSCF, as we always do at this point).
 *
 * @param arWf    Initial wave function
 **/
void ExponentialTdH::PrepareIntegrationImpl
   (  vec_t& arWf
   )
{
}


/**
 * Implementation of ShapedZeroVector.
 *
 * @return
 *    Zero vector of correct size
 **/
typename ExponentialTdH::vec_t ExponentialTdH::ShapedZeroVectorImpl
   (
   )  const
{
   return vec_t(this->mNKappas + I_1, CC_0);
}


/**
 * Implementation of CalculateAutoCorr
 *
 * @param aT      t
 * @param aY      y(t) vector
 * @return
 *    S(t)
 **/
typename ExponentialTdH::param_t ExponentialTdH::CalculateAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   param_t result(1.);
   auto f = param_t(0.);

   // If no reset kappas, life is simple...
   if (  this->mResetKappas.empty()
      )
   {
      // Calculate R_m factors and set result = prod_m cos(R_m)
      auto r_vec = this->CalculateKappaNorms(aY);
      for(const auto& r : r_vec)
      {
         result *= std::cos(r);
      }

      f = aY[mNKappas];
   }
   // Else, we need to do time-ordered e^K transforms
   else
   {
      for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
      {
         auto coefs = this->GetWaveFunctionCoefficientsImpl(imode, aT, aY);

         // The contribution from mode imode is the occ element of the transformed vector
         result *= coefs[I_0];
      }

      f = this->GetPhaseFactorImpl(aT, aY);
   }

   // Multiply accumulated phase.
   // Here, we only take real part of F(t).
   result *= std::exp(CC_M_I*std::real(f));

   return result;
}


/**
 * Implementation of CalculateHalfTimeAutoCorr
 *
 * @param aT      t
 * @param aY      y(t) vector
 * @return
 *    S(2*t)
 **/
typename ExponentialTdH::param_t ExponentialTdH::CalculateHalfTimeAutoCorrImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   param_t result(1.);
   param_t f(0.);
   if (  this->mResetKappas.empty()
      )
   {
      // Calculate R_m factors and set result = prod_m ( cos(R_m)^2 + sinc(R_m)^2 kappa_+^T kappa_+ )
      auto r_vec = this->CalculateKappaNorms(aY);
      for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
      {
         In off = this->mKappaOffsets[imode];
         In nbas = this->NBas(imode);
         In nkappas = nbas - I_1;

         // Calculate kappa_+^T kappa_+ (NOT conjugate, just transpose)
         param_t ktk(0.);
         for(In a=I_0; a<nkappas; ++a)
         {
            ktk += std::pow(aY[off+a], 2);
         }

         const auto& r = r_vec[imode];
         
         result *= (std::pow(std::cos(r), 2) + std::pow(midas::math::sinc(r), 2)*ktk);
      }
      f = aY[mNKappas];
   }
   else
   {
      for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
      {
         auto coefs = this->GetWaveFunctionCoefficientsImpl(imode, aT, aY);
         auto size = coefs.size();

         // Calculate mode-imode contribution
         param_t dot(0.);
         for(In p=I_0; p<size; ++p)
         {
            dot += std::pow(coefs[p], 2);
         }

         // Multiply mode-imode contribution on result
         result *= dot;
      }

      f = this->GetPhaseFactorImpl(aT, aY);
   }

   // Multiply accumulated phase times 2
   result *= std::exp(step_t(2.)*CC_M_I*std::real(f));

   return result;
}

/**
 * Get coefficient vector for modal in time-independent basis
 *
 * @param aModeNr
 * @param aT
 * @param aWfParams
 * @return
 *    Value of wave function
 **/
typename ExponentialTdH::vec_t ExponentialTdH::GetWaveFunctionCoefficientsImpl
   (  LocalModeNr aModeNr
   ,  step_t aT
   ,  const vec_t& aWfParams
   )  const
{
   // Set up some sizes
   In nbas = this->NBas(aModeNr);
   In nkappas = nbas - I_1;
   In off = this->mKappaOffsets[aModeNr];

   // Init result
   vec_t coef_vec(nbas, param_t(0.));

   // If we have no reset kappas, this is simple
   if (  this->mResetKappas.empty()
      )
   {
      auto r_vec = this->CalculateKappaNorms(aWfParams);
      const auto& r = r_vec[aModeNr];
      auto sinc_r = midas::math::sinc(r);

      // Set vector parameters
      coef_vec[0] = std::cos(r);
      for(In a=I_0; a<nkappas; ++a)
      {
         coef_vec[a+I_1] = sinc_r * aWfParams[off+a];
      }
   }
   // Else, we need to perform all e^K transformations (time-ordered)
   else
   {
      coef_vec = this->DoResetExpKappaTransforms(aModeNr, aT, aWfParams);
   }

   return coef_vec;
}

/**
 * Get phase from vector
 *
 * @param aT
 * @param aY
 * @return
 *    F(aT)
 **/
typename ExponentialTdH::param_t ExponentialTdH::GetPhaseFactorImpl
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   param_t result = aY[aY.size()-I_1];

   for(const auto& k : this->mResetKappas)
   {
      /**
       * At time t=k.first the kappa vector was reset and y=k.second contains the zeroed kappa parameters.
       * However, the y vector for aT=k.first stored in the OdeDatabase is the full one without reset.
       * Thus, we only do the transformation for k.first < aT.
       **/
      if (  k.first < aT
         )
      {
         // Reference to reset kappa
         const auto& rkap = k.second;

         // Add phase
         result += rkap[rkap.size()-I_1];
      }
      else
      {
         break;
      }
   }

   return result;
}

/**
 * Transform vector with e^K for all reset kappa vectors and the last one 
 * in order to obtain a vector of modal coefs.
 *
 * @param aModeNr       m
 * @param aT            t
 * @param aY            kappa_m(t) (which has been reset during the propagation)
 *
 * @return
 *    Vector of modal coefs
 **/
typename ExponentialTdH::vec_t ExponentialTdH::DoResetExpKappaTransforms
   (  LocalModeNr aModeNr
   ,  step_t aT
   ,  const vec_t& aY
   )  const
{
   In nbas = this->NBas(aModeNr);
   In nkappas = nbas - I_1;
   In off = this->mKappaOffsets[aModeNr];

   step_t r = std::sqrt(std::real(Dot(aY, aY, off, nkappas)));
   step_t sinc_r = midas::math::sinc(r);

   // Init result as first column of last e^K matrix (defined by kappa = aY)
   vec_t result(nbas);
   result[I_0] = std::cos(r);
   for(In a=I_0; a<nkappas; ++a)
   {
      result[a+I_1] = aY[off+a] * sinc_r;
   }

   // Revese loop through resets to get later times first!
   for(auto rit = this->mResetKappas.crbegin(); rit != this->mResetKappas.crend(); ++rit)
   {
      const auto& k = *rit;

      /**
       * At time t=k.first the kappa vector was reset and y=k.second contains the zeroed kappa parameters.
       * However, the y vector for aT=k.first stored in the OdeDatabase is the full one without reset.
       * Thus, we only do the transformation for k.first < aT.
       **/
      if (  k.first < aT
         )
      {
         // Reference to reset kappa
         const auto& rkap = k.second;

         // Transform: result = e^K * result
         this->ExpKappaTransform(aModeNr, rkap, result, true);
      }
   }

   return result;
}

/**
 * Transform vector with e^K
 *
 * @param aModeNr    m
 * @param aKappa     Kappa vector
 * @param arVec      Vector to transform
 * @param aFromLeft  If true: arVec = e^K * arVec. Else: arVec = arVec^T * e^K
 **/
void ExponentialTdH::ExpKappaTransform
   (  LocalModeNr aModeNr
   ,  const vec_t& aKappa
   ,  vec_t& arVec
   ,  bool aFromLeft
   )  const
{
   // Set up some sizes
   In nbas = this->NBas(aModeNr);
   In nkappas = nbas - I_1;
   In off = this->mKappaOffsets[aModeNr];

   assert(arVec.size() == nbas);

   // Check that the kappa norm is larger than 0
   step_t knorm2 = std::abs(aKappa.Norm2(off, nkappas));

   if (  !knorm2
      )
   {
      return;
   }
   else
   {
      // Make temporary vector
      vec_t tmp = arVec;

      // Calculate intermediates
      param_t prod = param_t(0.);
      for(In a=I_0; a<nkappas; ++a)
      {
         prod  += aFromLeft
               ?  -midas::math::Conj(aKappa[off+a])*arVec[a+I_1]
               :  aKappa[off+a]*arVec[a+I_1];
      }
      step_t r = std::sqrt(std::real(Dot(aKappa, aKappa, off, nkappas)));

      auto sinc_r = midas::math::sinc(r);
      auto half_sinc_r_half_sq = step_t(0.5)*std::pow(midas::math::sinc(step_t(0.5)*r), 2);

      // Set occ element of transformed vector
      tmp[I_0] = std::cos(r)*arVec[I_0] + sinc_r * prod;
      
      // Set virtual elements of transformed vector
      for(In a=I_0; a<nkappas; ++a)
      {
         auto k_a =  aFromLeft
                  ?  aKappa[off+a]
                  :  -midas::math::Conj(aKappa[off+a]);

         tmp[a+I_1]  =  arVec[a+I_1]
                     +  sinc_r * k_a * arVec[I_0]
                     +  half_sinc_r_half_sq * k_a * prod;
      }

      // Swap arVec and tmp
      arVec = std::move(tmp);
   }
}

/**
 * Save kappa norms
 * 
 * @param aT
 * @param aY
 **/
void ExponentialTdH::SavePropertiesImpl
   (  step_t aT
   ,  const vec_t& aY
   )
{
   if (  this->mProperties.find(TdHBase::propID::KAPPANORM) != this->mProperties.end()
      )
   {
      // Reserve space
      this->mSavedKappaNorms.reserve(this->mTs.capacity());

      // Save
      this->mSavedKappaNorms.emplace_back(this->CalculateKappaNorms(aY));
   }
}

/**
 * Finalize. Calculate properties, etc.
 *
 * @param aTs     Vector of ts for all time steps
 * @param aYs     Vector of ys for all time steps
 **/
void ExponentialTdH::FinalizeImpl
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )
{
   // Write KappaNorms
   if (  this->mProperties.find(TdHBase::propID::KAPPANORM) != this->mProperties.end()
      )
   {
      // Column width of output
      size_t width = 24;

      std::string filename = gAnalysisDir + "/" + this->mName + "_kappa_norms.dat";
      std::ofstream ofs(filename);
      ofs << std::scientific << std::setprecision(16);

      // Print header
      ofs   << std::setw(width) << "# t";
      for(LocalModeNr imode=0; imode<this->mNModes; ++imode)
      {
         GlobalModeNr i_g_mode = this->mpOpDef->GetGlobalModeNr(imode);
         std::ostringstream oss;
         oss << "R_" << i_g_mode;
         ofs   << std::setw(width) << oss.str();
      }
      ofs   << std::endl;
   
      // Loop over datapoints
      auto n = this->mTs.size();
      for (In i=0; i<n; ++i)
      {
         const auto& t_i = this->mTs[i];
         const auto& norms_i = this->mSavedKappaNorms[i];

         // Print time
         ofs   << std::setw(width) << t_i;

         // Loop over modes
         for(const auto& r : norms_i)
         {
            // Only write real part
            ofs   << std::setw(width) << r;
         }
         ofs   << std::endl;
      }

      ofs.close();
   }
}

/**
 * Calculate kappa norms
 *
 * @param aY   Vector of kappas (and phase)
 * @return
 *    Kappa norms
 **/
std::vector<typename ExponentialTdH::step_t> ExponentialTdH::CalculateKappaNorms
   (  const vec_t& aY
   )  const
{
   std::vector<step_t> result(this->mNModes);

   for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
   {
      In nkappas = this->NBas(imode) - I_1;
      In off = this->mKappaOffsets[imode];
      result[imode] = std::sqrt(std::real(Dot(aY, aY, off, nkappas)));
   }

   return result;
}

/**
 * Update kappa norms. Throw exception if too large.
 *
 * @param aY   Vector of kappas and phase
 **/
void ExponentialTdH::UpdateKappaNorms
   (  const vec_t& aY
   )
{
   // Update kappa norms
   this->mKappaNorms = this->CalculateKappaNorms(aY);

   // Check kappa norms
   this->mFailingModes = this->CheckKappaNorms(this->mKappaNorms);

   if (  !this->mFailingModes.empty()
      )
   {
      // Make exception
      std::ostringstream warning;
      for(const auto& imode : this->mFailingModes)
      {
         warning << "R_" << imode << " = " << this->mKappaNorms[imode] << std::endl;
      }
      auto string = warning.str();
      throw midas::ode::bad_derivative(string);
   }
}

/**
 * Check kappa norms
 *
 * @param aNorms     Norms of kappa vectors
 * @return
 *    Set of bad modes
 **/
std::set<LocalModeNr> ExponentialTdH::CheckKappaNorms
   (  const std::vector<step_t>& aNorms
   )  const
{
   std::set<LocalModeNr> bad_norms;
   for(In imode=I_0; imode<this->mNModes; ++imode)
   {
      if (  aNorms[imode] > step_t(0.5*C_PI)*this->mKappaNormThreshold
         )
      {
         if (  gDebug
            || this->mIoLevel >= I_8
            )
         {
            Mout  << " Mode " << imode << ": R_m = " << aNorms[imode] << std::endl;
         }

         bad_norms.insert(imode);
      }
   }

   return bad_norms;
}


/**
 * Update H integrals and kappa norms
 * @param aY
 **/
void ExponentialTdH::PrepareDerivativeCalculationImpl
   (  const vec_t& aY
   )
{
   LOGCALL("prepare deriv");

   this->UpdateKappaNorms(aY);

   // Update integrals
   this->mIntegrals->Update(aY, this->mKappaOffsets, this->mKappaNorms);
}

/**
 * Update property integrals
 * @param aY
 **/
void ExponentialTdH::UpdatePropertyIntegralsImpl
   (  const vec_t& aY
   )
{
   LOGCALL("update prop integrals");

   // Update kappa norms (just to be safe)
   this->UpdateKappaNorms(aY);

   for(const auto& in : this->mPropertyIntegrals)
   {
      in.second->Update(aY, this->mKappaOffsets, this->mKappaNorms);
   }
}

/**
 * Zero part of the kappa vector
 *
 * @param arY     Kappa vector
 * @param aMode   Mode number to zero
 **/
void ExponentialTdH::ZeroKappaVector
   (  vec_t& arY
   ,  LocalModeNr aMode
   )  const
{
   auto nkappas = this->NBas(aMode) - I_1;
   auto off = this->mKappaOffsets[aMode];

   for(In i=I_0; i<nkappas; ++i)
   {
      arY[i+off] = param_t(0.);
   }
}

/**
 * Number of kappa parameters for mode
 *
 * @param aMode
 * @return
 *    NKappas for aMode
 **/
In ExponentialTdH::NParams
   (  LocalModeNr aMode
   )  const
{
   return this->NBas(aMode) - I_1;
}

/**
 * Offset for mode
 *
 * @param aMode
 * @return
 *    Offset
 **/
In ExponentialTdH::Offset
   (  LocalModeNr aMode
   )  const
{
   if(aMode < mKappaOffsets.size())
   {
      return this->mKappaOffsets[aMode];
   }
   else
   {
      return 0;
   }
}

/**
 * Transform initial state when using spectral basis of general operator.
 *
 * @param aModals
 **/
DataCont ExponentialTdH::ConstructSpectralBasisTransformation
   (  const DataCont& aModals
   )  const
{
   if (  this->mpTdHCalcDef->GetSpectralBasisOper() == ""
      || this->mpTdHCalcDef->GetSpectralBasisOper() == this->mReferenceOperName
      )
   {
      // We do not modify the initial state in this case.
      return aModals;
   }
   else
   {
      MIDASERROR("X-TDH only accepts spectral basis of reference operator at this point. But feel free to implement alternatives!");
      return DataCont();
   }
}
