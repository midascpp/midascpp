/*
************************************************************************
*
* @file                 TdH.cc
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    TdH wrapper class implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "TdH.h"
#include "ode/OdeIntegrator.h"
#include "util/Timer.h"
#include "mmv/DataCont.h"

/**
 *    Constructor from pointer
 *
 * @param apTdH      Pointer to TdHBase
 **/
TdH::TdH
   (  TdHBase* apTdH
   )
   :  mTdH
         (  apTdH
         )
{
   if (  !mTdH
      )
   {
      MIDASERROR("TdH cannot be initialized to nullptr!");
   }
}

/**
 * Constructor from calcdef, etc.
 *
 * @param apCalcDef
 * @param apOpDef
 * @param apBasDef
 * @param aInitialModalsFilename
 **/
TdH::TdH
   (  input::TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  const std::string& aInitialModalsFilename
   )
   :  mTdH
         (  TdHBase::Factory(apCalcDef, apOpDef, apBasDef, aInitialModalsFilename)
         )
{
   if (  !mTdH
      )
   {
      MIDASERROR("TdH cannot be initialized to nullptr!");
   }
}

/**
 * Calculate derivative
 *
 * @param aT               t
 * @param aY               y(t) vector
 * @return
 *    dy/dt vector
 **/
typename TdH::vec_t TdH::Derivative
   (  step_t aT
   ,  const vec_t& aY
   )
{
   return this->mTdH->Derivative(aT, aY);
}

/**
 * Process vector
 * 
 * @param arY           y(t)
 * @param aYOld         Prev y
 * @param arT           t
 * @param aTOld         Prev t
 * @param arDyDt        dy/dt
 * @param aDyDtOld      Prev dy/dt
 * @param arStep        Stepsize
 * @param arNDerivFail  Number of failed derivative calculations before this step.
 * @return
 *    Accept the step?
 **/
bool TdH::ProcessNewVector
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )  const
{
   return this->mTdH->ProcessNewVector(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
}

/**
 * Save data for accepted step.
 * NB: Requires Hamiltonian integrals to be updated beforehand!
 *
 * @param aT
 * @param aY
 **/
bool TdH::AcceptedStep
   (  step_t aT
   ,  const vec_t& aY
   )
{
   return this->mTdH->AcceptedStep(aT, aY);
}

/**
 * Save data for interpolated point.
 *
 * @param aT
 * @param aY
 **/
void TdH::InterpolatedPoint
   (  step_t aT
   ,  const vec_t& aY
   )
{
   this->mTdH->InterpolatedPoint(aT, aY);
}

/**
 * @return
 *    Zero vector of correct size
 **/
typename TdH::vec_t TdH::ShapedZeroVector
   (
   )  const
{
   return this->mTdH->ShapedZeroVector();
}

/**
 * Set name
 *
 * @param aName   name
 **/
void TdH::SetName
   (  const std::string& aName
   )
{
   this->mTdH->SetName(aName);
}

/**
 * Get name
 *
 * @return 
 *    Name
 **/
const std::string& TdH::Name
   (
   )  const
{
   return this->mTdH->Name();
}

/**
 * Set name of ref oper
 *
 * @param aName   name
 **/
void TdH::SetReferenceOperName
   (  const std::string& aName
   )
{
   this->mTdH->SetReferenceOperName(aName);
}

/**
 * Initialize the OneModeDensities member
 *
 * @param aCalcName   Name of the calculation
 **/
void TdH::InitOneModeDensities
   (  const std::string& aCalcName
   )
{
   mTdH->InitOneModeDensities(aCalcName);
}

/**
 * Evolve the TDH equations of motion.
 **/
void TdH::Evolve
   (
   )
{
   if (  gDebug
      )
   {
      Mout  << " In TdH::Evolve()" << std::endl;
   }

   // Init timer
   Timer timer;
   timer.Reset();

   // Prepare integration
   auto wf = this->mTdH->ShapedZeroVector();
   this->mTdH->PrepareIntegration(wf);

   // Make integrator and evolve
   OdeIntegrator<TdH> integrator(this, this->mTdH->GetOdeInfo());
   integrator.Integrate(wf);

   // Output timing
   timer.CpuOut(Mout, "CPU time spent on TDH integration:  ");
   timer.WallOut(Mout, "Wall time spent on TDH integration: ");

   // Write wave function to disc
   if constexpr   (  std::is_same_v<vec_t, GeneralMidasVector<param_t> >
                  )
   {
      if (  wf.Size() == I_0
         )
      {
         MidasWarning("TDH wave function is empty after propagation!");
      }

      std::string name = this->Name() + "_final_modals";
      GeneralDataCont<param_t> dc_out(wf, "ONDISC", name, true);
   }
   else
   {
      MidasWarning("TdH: Cannot save wave function of type '" + std::string(typeid(vec_t).name()) + "' to disc.");
   }

   // Write summary of integration
   integrator.Summary();

   // Get vectors of time and y
   const auto& t = integrator.GetDatabaseT();
   const auto& y = integrator.GetDatabaseY();

   // Calculate properties, do output and stuff
   this->mTdH->Finalize(t, y);
}
