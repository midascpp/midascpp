/**
************************************************************************
* 
* @file                 TdHDrv.cc
* 
* Created:              30-06-2011
* 
* Author:               Mikkel Bo Hansen (mbh@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Driver for MidasCPP TdH calculations
* 
* Last modified:        June 18, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/VscfCalcDef.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/BasDef.h"
#include "input/TdHCalcDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "TdH.h"

namespace midas::tdh
{

/**
* Run tdh calculations
* */
void TdHDrv()
{

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin TDH calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   if (  gDebug
      || gVibIoLevel > I_5
      )
   {
      Mout << " Number of TDH calculations to perform: " << gTdHCalcDef.size() << std::endl;
   }

   // Make a map over oper/basis combinations used in the whole set of tdh calculations.
   std::map<std::string,In> oper_bas_map;
   for (In i_calc=gTdHCalcDef.size()-1; i_calc>=0; --i_calc)
   {
      std::string oper_name  = gTdHCalcDef[i_calc].GetOper();
      std::string basis_name = gTdHCalcDef[i_calc].GetBasis();
      std::string oper_bas  = oper_name + "_" + basis_name;
      oper_bas_map[oper_bas]=i_calc;
   }

   if (  gDebug
      || gVibIoLevel > I_7
      )
   {
      Mout << " Calc. nr. | oper_basis: " << std::endl;
      for(const auto& po : oper_bas_map)
      {
          Mout << "  " << po.second << " | " << po.first << std::endl;
      }
   }

   for(In i_calc=0; i_calc<gTdHCalcDef.size(); ++i_calc)
   {
      // Set IoLevel to be gt gVibIoLevel
      gTdHCalcDef[i_calc].SetIoLevel(std::max(gTdHCalcDef[i_calc].GetIoLevel(), gVibIoLevel));

      //
      if (  gTdHCalcDef[i_calc].GetInitialWavePacket() != input::TdHCalcDef::initType::VSCF
         )
      {
         MIDASERROR("TdHDrv is only set up for initialization from VSCF!");
      }

      // Find VscfCalcDef to initialize from.
      // If there is only one, we start from that!
      In i_vscf = -I_1;
      if (  gVscfCalcDef.size() > 1
         )
      {
         const auto& vscf_name = gTdHCalcDef[i_calc].GetVscfName();
         if (  gDebug
            || gVibIoLevel > I_5
            )
         {
            Mout  << " Will initialize from VSCF: " << vscf_name << std::endl;
         }

         for(In i=I_0; i<gVscfCalcDef.size(); ++i)
         {
            const auto& vn = gVscfCalcDef[i].GetName();
            if (  gDebug
               || gVibIoLevel > I_8
               )
            {
               Mout  << " Checking VSCF with name: " << vn << std::endl;
            }

            if (  vn == vscf_name
               )
            {
               i_vscf = i;
               break;
            }
         }
         if (  i_vscf == -I_1
            )
         {
            MIDASERROR("VSCF calculation '" + vscf_name + "' not found!");
         }
      }
      else
      {
         i_vscf = I_0;
      }

      auto& vscf_calcdef = gVscfCalcDef[i_vscf];

      // Find operator number
      std::string oper_name  = gTdHCalcDef[i_calc].GetOper();
      std::string vscf_oper_name = vscf_calcdef.Oper();
      In i_oper=-I_1;
      In i_vscf_oper=-I_1;
      for(In i=0; i<gOperatorDefs.GetNrOfOpers(); ++i)
      {
         if (  gOperatorDefs[i].Name() == oper_name
            )
         {
            i_oper=i;
         }
         if  (  gOperatorDefs[i].Name() == vscf_oper_name
             )
         {
            i_vscf_oper = i;
         }
      }

      // Find basis number
      std::string basis_name = gTdHCalcDef[i_calc].GetBasis();
      std::string vscf_basis_name = vscf_calcdef.Basis();

      // Set default basis
      if (  basis_name.empty()
         )
      {
         gTdHCalcDef[i_calc].SetBasis(vscf_basis_name);
         basis_name = vscf_basis_name;
      }
      else
      {
         // Currently, we only allow identical bases. But this may change!
         MidasAssert(basis_name == vscf_basis_name, "TDH must use the same primitive basis as VSCF");
      }

      In i_basis=-1;
      for(In i=I_0; i<gBasis.size(); ++i)
      {
         if (  gBasis[i].GetmBasName() == basis_name
            )
         {
            i_basis=i;
         }
      }

      if (  i_basis == -I_1
         || i_oper == -I_1
         )
      {
         MIDASERROR("Basis or Operator not found in TdHDrv");
      }

      // Generate basis set
      gBasis[i_basis].InitBasis(gOperatorDefs[i_vscf_oper]);

      if (  (  gBasis[i_basis].GetmUseBsplineBasis()
            || gBasis[i_basis].GetmUseGaussianBasis()
            )
         && !gTdHCalcDef[i_calc].GetSpectralBasis()
         )
      {
         if (  gDebug
            )
         {
            Mout  << " Set 'mSpectralBasis = true' in TdHCalcDef because we use non-orthogonal basis set." << std::endl;
         }
         gTdHCalcDef[i_calc].SetSpectralBasis(true);
      }

      Mout << "\n\n\n Do TdH: \"" << gTdHCalcDef[i_calc].GetName() << "\" calculation" << std::endl;
      std::string oper_bas  = oper_name + "_" + basis_name;
      In i_first_calc = oper_bas_map[oper_bas];
      if (  gDebug
         )
      {
         Mout << " i_calc: " << i_calc << " i_first_calc: " << i_first_calc << std::endl;
      }

      if (  gDebug
         || gVibIoLevel > I_2
         )
      {
         Mout  << "\n\n";
         Mout  << " Operator:                            "
               << oper_name << std::endl;
         Mout  << " Operator-Type:                       "
               << gOperatorDefs[i_oper].Type() << std::endl;
         Mout  << " Number of terms in operator:         "
               << gOperatorDefs[i_oper].Nterms() << std::endl;
         Mout  << " Basis:                               "
               << gBasis[i_basis].GetmBasName() << std::endl;
         Mout  << " Basis-Type:                          "
               << gBasis[i_basis].GetmBasSetType() << std::endl;
         Mout  << " Number of modes:                     "
               << gOperatorDefs[i_vscf_oper].NmodesInOp() << std::endl;
      }

      // Construct TdH object
      std::string vscf_modals_name = vscf_calcdef.GetName() + "_Modals";
      TdH tdh(&gTdHCalcDef[i_calc], &gOperatorDefs[i_oper], &gBasis[i_basis], vscf_modals_name);

      // Set name of calculation
      tdh.SetName(gTdHCalcDef[i_calc].GetName());

      // Set name of reference operator
      tdh.SetReferenceOperName(vscf_oper_name);

      if (gTdHCalcDef[i_calc].GetUpdateOneModeDensities())
      {
         // Initialize the OneModeDensities member
         tdh.InitOneModeDensities(tdh.Name());
      }
      // Evolve the TDH EOMs
      tdh.Evolve();
   }

   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," TDH Done",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');

}

} /* namespace midas::tdh */
