/*
************************************************************************
*
* @file                 TdHBase.h
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    TdHBase class and function declarations.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHBASE_H_INCLUDED
#define TDHBASE_H_INCLUDED

#include <vector>
#include <set>
#include <utility>

#include "TdHIntegrals.h"

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"

#include "ni/OneModeInt.h"
#include "ode/OdeInfo.h"

#include "mmv/MidasVector.h"
#include "TdHMpi.h"
#include "td/SpectrumCalculator.h"
#include "util/Math.h"
#include "td/OneModeDensities.h"

//! Forward decl.
namespace midas::input
{
   class TdHCalcDef;
}
class OpDef;
class BasDef;
class LinearTdH;
class ExponentialTdH;

/**
 *
 **/
class TdHBase
   :  private midas::td::SpectrumCalculator<std::complex<Nb> >
{
   public:
      //! Typedefs
      template<typename T>
      using container_t = GeneralMidasVector<T>;
      using step_t = Nb;
      using param_t = std::complex<Nb>;
      using real_t = Nb;
      using vec_t = container_t<param_t>;
      using mat_t = GeneralMidasMatrix<param_t>;

      using Spectrum = midas::td::SpectrumCalculator<param_t>;

      //! Property tags
      enum class propID : int
      {
         ENERGY = 0
      ,  PHASE
      ,  AUTOCORR
      ,  HALFTIMEAUTOCORR
      ,  KAPPANORM
      };

      //! Spectrum tags
      enum class specID : int
      {  AUTOCORR = 0
      ,  HALFTIMEAUTOCORR
      };


   protected:
      //! TDH calcdef
      input::TdHCalcDef* mpTdHCalcDef = nullptr;

      //! Operator def
      OpDef* mpOpDef = nullptr;

      //! Basis def
      BasDef* mpBasDef = nullptr;

      //! Name of calculation
      std::string mName = "TdH_0";

      //! Number of modes
      In mNModes = I_0;

      //! Name of file containing initial modals
      std::string mInitialModalsName;

      //! Name of operator used for reference calculation
      std::string mReferenceOperName = "";

      //! DataCont used for transforming initial state to spectral basis
      DataCont mSpectralBasisModals;

      //! IO level
      In mIoLevel = I_1;

      //! Do imaginary-time propagation
      bool mImagTime = false;

      //! Number of parameters for describing the time-dependent modals (C coefs for LinearTdH and kappa for ExponentialTdH)
      In mNOccModalCoef = I_0;

      //! Number of modal coefficients in initial modals (whole set of modals)
      In mNInputModalCoef = I_0;

      //! Number of screened terms
      size_t mNScreenedTerms = 0;

      //! Offsets in input modals
      std::vector<In> mPrimitiveModalOffsets;

      //! Integrals of H operator
      std::unique_ptr<TdHIntegralsBase> mIntegrals;

      //! Time for last integrals update
      step_t mIntegralsTime = step_t(0.);


      /** @name Wave-function analysis */
      //!@{
      //! Set of properties to calculate
      std::set<propID> mProperties;

      //! Set of spectra to calculate
      std::set<specID> mSpectra;

      //! Set of modes to output wave function for
      std::set<std::string> mWfOutModes;

      //! Set of mode pairs to output densities for
      std::set<std::set<std::string> > mTwoModeDensityPairs;

      //! Set of modes to output primitive basis for
      std::set<std::string> mWritePrimitiveBasisForModes;

      //! Grid density for plotting wave function
      In mWfGridDensity = I_10;

      //! Time points for saved properties
      std::vector<step_t> mTs;

      //! Saved energies
      std::vector<param_t> mEnergies;

      //! Saved phases
      std::vector<param_t> mPhases;

      //! Saved autocorr points
      std::vector<param_t> mAutoCorrs;

      //! Saved half-time autocorr points
      std::vector<param_t> mHalftimeAutoCorrs;

      //! Initial energy (kept for energy shift)
      param_t mInitialEnergy = param_t(0.);

      //! Operators to calculate expectation values for
      std::set<std::string> mExptValOpers;

      //! Integrals for mExptValOpers
      std::map<std::string, std::unique_ptr<TdHIntegralsBase> > mPropertyIntegrals;

      //! Saved expectation values
      std::map<std::string, std::vector<std::vector<param_t> > > mExptVals;

      //! OneModeDensities member
      midas::td::OneModeDensities<param_t> mOneModeDensities;

      //! GridPoints for each mode
      std::vector<GeneralMidasVector<Nb>> mGridPoints;

      //! A scaling factor to scale the grid the WF is evaluated at if a HO basis is used
      Nb mHoGridScaling = C_2;

      //! Number of interpolated points
      In mNInterPoint = 0;
      //!@}

   private:
      //! Calculate derivative vector
      virtual vec_t DerivativeImpl
         (  step_t aT
         ,  const vec_t& aY
         ,  size_t& arNScreen
         )  const = 0;

      //! Process new y vector (normalize, etc.)
      virtual bool ProcessNewVectorImpl
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& aT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )  = 0;

      //! Shaped zero vector
      virtual vec_t ShapedZeroVectorImpl
         (
         )  const = 0;

      //! Prepare integration
      virtual void PrepareIntegrationImpl
         (  vec_t&
         )  = 0;

      //! Calculate autocorrelation function
      virtual param_t CalculateAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const = 0;

      //! Calculate half-time autocorrelation function
      virtual param_t CalculateHalfTimeAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const = 0;

      //! Get coefficient vector for wave function in the time-independent basis (primitive or spectral)
      virtual vec_t GetWaveFunctionCoefficientsImpl
         (  LocalModeNr
         ,  step_t
         ,  const vec_t&
         )  const = 0;

      //! Get generalized phase factor (F) for wave function
      virtual param_t GetPhaseFactorImpl
         (  step_t
         ,  const vec_t&
         )  const = 0;

      //! Finalize calculation. Calculate properties, etc.
      virtual void FinalizeImpl
         (  const std::vector<step_t>& aTs
         ,  const std::vector<vec_t>& aYs
         )  = 0;

      //! Update H integrals (impl)
      virtual void PrepareDerivativeCalculationImpl
         (  const vec_t&
         )  = 0;

      //! Update property integrals
      virtual void UpdatePropertyIntegralsImpl
         (  const vec_t&
         )  = 0;

      //! Get number of parameters for mode
      virtual In NParams
         (  LocalModeNr
         )  const = 0;
      
      //! Get offset in vector for mode
      virtual In Offset
         (  LocalModeNr
         )  const = 0;

      //! Get transformation matrix for spectral basis
      virtual DataCont ConstructSpectralBasisTransformation
         (  const DataCont&
         )  const = 0;

      //! Save properties. Integrals are updated already!
      void SaveProperties
         (  step_t aT
         ,  const vec_t& aY
         );

      //! Save properties for concrete TDH implementation
      virtual void SavePropertiesImpl
         (  step_t aT
         ,  const vec_t& aY
         );

      //! Type
      virtual midas::tdh::tdhID TypeImpl
         (
         )  const = 0;


   protected:
      //! Update H integrals (wrapper)
      void PrepareDerivativeCalculation
         (  step_t aT
         ,  const vec_t& aY
         );

      //! Get number of basis functions for mode
      In NBas
         (  LocalModeNr
         )  const;

      //! Calculate energy
      param_t CalculateEnergyImpl
         (
         )  const;

#ifdef VAR_MPI
      //! How to communicate
      midas::tdh::mpiComm mMpiComm = midas::tdh::mpiComm::BCAST;

      //! Get range of modes to calculate for given MPI rank
      std::pair<In, In> MpiModeRange
         (  In
         )  const;

      //! Sync data after MPI for loop over modes
      void SyncData
         (  vec_t&
         ,  param_t&
         ,  size_t&
         )  const;

#endif /* VAR_MPI */

   public:
      //! Constructor
      TdHBase
         (  input::TdHCalcDef*
         ,  OpDef*
         ,  BasDef*
         ,  const std::string&
         );

      //! Virtual destructor
      virtual ~TdHBase() = default;

      //! Interface to DerivativeImpl
      vec_t Derivative
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Save data for accepted step. NB: Requires mIntegrals to be updated beforehand!
      bool AcceptedStep
         (  step_t aTime
         ,  const vec_t& aY
         );

      //! Save data for interpolated point.
      void InterpolatedPoint
         (  step_t aTime
         ,  const vec_t& aY
         );
      
      //! Increment number of interpolated points
      void IncInterpolatedTimePoints() {++mNInterPoint;}
      
      //! Get the number of interpolated points
      In GetNumberOfInterpolatedTimePoints() {return mNInterPoint;}

      //! Interface to ProcessNewVectorImpl
      bool ProcessNewVector
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& aT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         );

      //! Get vector of correct shape
      vec_t ShapedZeroVector
         (
         )  const;

      //! Interface to PrepareIntegration
      void PrepareIntegration
         (  vec_t&
         );

      //! Get OdeInfo from calcdef
      const midas::ode::OdeInfo& GetOdeInfo
         (
         )  const;

      //! Calculate energy
      param_t CalculateEnergy
         (  step_t
         ,  const vec_t&
         );

      //! Calculate autocorrelation function
      param_t CalculateAutoCorr
         (  step_t
         ,  const vec_t&
         )  const;

      //! Calculate half-time autocorrelation function
      param_t CalculateHalfTimeAutoCorr
         (  step_t
         ,  const vec_t&
         )  const;

      //! Get coefficient vector for wave function in the time-independent basis (primitive or spectral)
      vec_t GetWaveFunctionCoefficients
         (  LocalModeNr
         ,  step_t
         ,  const vec_t&
         )  const;

      //! Output wave function
      void WriteWaveFunction
         (  const std::vector<step_t>&
         ,  const std::vector<vec_t>&
         )  const;

      std::vector<GeneralMidasVector<real_t>> CalculateOneModeDensities
         (  const step_t&  
         ,  const vec_t&
         );
      
      void SetGridPoints
         (  const Nb aGridScaling = C_2
         );
      
      const std::vector<GeneralMidasVector<real_t>>& GetGridPoints
         (
         )  const;

      //! Finalize
      void Finalize
         (  const std::vector<step_t>& aTs
         ,  const std::vector<vec_t>& aYs
         );

      //! Set name
      void SetName
         (  const std::string&
         );

      //! Set name of reference operator
      void SetReferenceOperName
         (  const std::string&
         );

      //! Get name
      const std::string& Name
         (
         )  const
      {
         return this->mName;
      }

      //! Get type
      midas::tdh::tdhID Type
         (
         )  const
      {
         return this->TypeImpl();
      }

      void InitOneModeDensities
         (  const std::string& aCalcName
         );

      void CalculateOneModeDensities
         (  const std::vector<GeneralMidasVector<real_t>>& TdhOneModeDens
         );


      //! Factory
      static std::unique_ptr<TdHBase> Factory
         (  input::TdHCalcDef* apCalcDef
         ,  OpDef* apOpDef
         ,  BasDef* apBasDef
         ,  const std::string& aInitModalsFile
         );
};

#endif /* TDHBASE_H_INCLUDED */
