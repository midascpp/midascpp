/*
************************************************************************
*
* @file                 ExponentialTdH.h
*
* Created:              06-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    ExponentialTdH class and function declarations.
*
* Last modified:        
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef EXPONENTIALTDH_H_INCLUDED
#define EXPONENTIALTDH_H_INCLUDED

#include "TdHBase.h"

/**
 * Exponential TDH parametrization
 **/
class ExponentialTdH
   :  public TdHBase
{
   public:
      // Typdefs
      using param_t = typename TdHBase::param_t;
      using step_t = typename TdHBase::step_t;
      using vec_t = typename TdHBase::vec_t;

   private:
      //! Number of kappa parameters
      In mNKappas;

      //! Offsets in mKappasAndPhase
      std::vector<In> mKappaOffsets;

      //! Calculate the projection terms inividually in the derivative?
      bool mDerivativeProjectionTerms = false;

      //! Threshold (fraction of pi/2) for resetting time-independent basis
      step_t mKappaNormThreshold = static_cast<step_t>(0.95);

      //! Max number of times the derivative can fail before resetting kappa
      In mMaxFailedDerivatives = I_1;

      /**
       * Map of kappas that have been used to transform the time-independent integrals
       * when the kappa norm has exceeded the threshold.
       * Key is the time point and the value is the kappa vector.
       * NB: The parts of the vectors which have not been reset are just zero.
       **/
      std::map<step_t, vec_t> mResetKappas;

      //! Saved kappa norms (for analysis)
      std::vector<std::vector<step_t> > mSavedKappaNorms;


      /** @name Temporary objects used during propagation **/
      //!@{

      //! Norms of kappa vectors
      std::vector<step_t> mKappaNorms;

      //! Set of modes with too large kappa norms (be sure to reset this one after use)
      std::set<LocalModeNr> mFailingModes;
      //!@}


      //! Implementation of Derivative
      vec_t DerivativeImpl
         (  step_t
         ,  const vec_t&
         ,  size_t&
         )  const override;

      //! Implementation of ProcessNewVector
      bool ProcessNewVectorImpl
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& aT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )  override;

      //! Implementation of ShapedZeroVector
      vec_t ShapedZeroVectorImpl
         (
         )  const override;

      //! Implementation of PrepareIntegration
      void PrepareIntegrationImpl
         (  vec_t&
         )  override;

      //! Implementation of CalculateAutoCorr
      param_t CalculateAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Implementation of CalculateHalfTimeAutoCorr
      param_t CalculateHalfTimeAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Get coefficient vector for modal in time-independent basis
      vec_t GetWaveFunctionCoefficientsImpl
         (  LocalModeNr
         ,  step_t
         ,  const vec_t&
         )  const override;

      //! Get generalized phase factor (F) for wave function
      param_t GetPhaseFactorImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Save properties
      virtual void SavePropertiesImpl
         (  step_t aT
         ,  const vec_t& aY
         )  override;

      //! Implementation of Finalize
      void FinalizeImpl
         (  const std::vector<step_t>&
         ,  const std::vector<vec_t>&
         )  override;

      //! Update H integrals
      void PrepareDerivativeCalculationImpl
         (  const vec_t&
         )  override;

      //! Update property integrals
      void UpdatePropertyIntegralsImpl
         (  const vec_t&
         )  override;

      //! Number of parameters for mode
      In NParams
         (  LocalModeNr
         )  const override;

      //! Offset for mode
      In Offset
         (  LocalModeNr
         )  const override;

      //! Get transformation matrix for spectral basis
      DataCont ConstructSpectralBasisTransformation
         (  const DataCont&
         )  const override;

      //! Type
      midas::tdh::tdhID TypeImpl
         (
         )  const override
      {
         return midas::tdh::tdhID::EXPONENTIAL;
      }


      //! Calculate kappa norms
      std::vector<step_t> CalculateKappaNorms
         (  const vec_t&
         )  const;

      //! Update and check kappa norms
      void UpdateKappaNorms
         (  const vec_t&
         );

      //! Check kappa norms. Return bad modes.
      std::set<LocalModeNr> CheckKappaNorms
         (  const std::vector<step_t>&
         )  const;

      //! Calculate b vector
      vec_t CalculateBVector
         (  LocalModeNr
         ,  size_t&
         )  const;

      //! Transform vector with e^K for all reset kappa vectors
      vec_t DoResetExpKappaTransforms
         (  LocalModeNr
         ,  step_t
         ,  const vec_t&
         )  const;

      //! Transform vector with e^K
      void ExpKappaTransform
         (  LocalModeNr
         ,  const vec_t&
         ,  vec_t&
         ,  bool
         )  const;

      //! Zero part of the kappa vector
      void ZeroKappaVector
         (  vec_t&
         ,  LocalModeNr
         )  const;

   public:
      //! Constructor
      ExponentialTdH
         (  input::TdHCalcDef*
         ,  OpDef*
         ,  BasDef*
         ,  const std::string&
         );
};

#endif /* EXPONENTIALTDH_H_INCLUDED */
