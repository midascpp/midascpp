/*
************************************************************************
*
* @file                 TdHDrv.h
*
* Created:              10-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Declaration of TdHDrv
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHDRV_H_INCLUDED
#define TDHDRV_H_INCLUDED

namespace midas::tdh
{

void TdHDrv();

} /* namespace midas::tdh */


#endif /* TDHDRV_H_INCLUDED */
