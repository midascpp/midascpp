/*
************************************************************************
*
* @file                 OneModeDensities.h
*
* Created:              29-11-2021
*
* Author:               Nicolai Machholdt Høyer (hoyer@chem.au.dk)
*
* Short Description:    Class for calculating the one-mode density across
*                       different td methods
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ONEMODEDENSITIES_H_INCLUDED
#define ONEMODEDENSITIES_H_INCLUDED

#include "td/OneModeDensities_Decl.h"
#include "td/OneModeDensities_Impl.h"


// Define instantiation macro.
#define INSTANTIATE_ONEMODEDENSITIES(PARAM_T) \
   namespace midas::td \
   {  \
      template class OneModeDensities<PARAM_T>; \
   }  /* namespace midas::td*/ \


// Instantiations.
INSTANTIATE_ONEMODEDENSITIES(Nb);
INSTANTIATE_ONEMODEDENSITIES(std::complex<Nb>);

#undef INSTANTIATE_ONEMODEDENSITIES


#endif /* ONEMODEDENSITIES_H_INCLUDED */
