/*
************************************************************************
*
* @file                 OneModeDensities_Decl.h
*
* Created:              29-11-2021
*
* Author:               Nicolai Machholdt Høyer (hoyer@chem.au.dk)
*
* Short Description:    Class for calculating the one-mode density across
*                       different td methods
*
* Last modified:        01-03-2022
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ONEMODEDENSITIES_DECL_H_INCLUDED
#define ONEMODEDENSITIES_DECL_H_INCLUDED

#include <vector>
#include <string>

// Midas headers.
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "td/oper/LinCombOper.h"
#include "input/BasDef.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"

namespace midas::td
{

template
   < typename PARAM_T
   >
class OneModeDensities
{
   public:
      //! Aliases
      using param_t = PARAM_T;
      using real_t = Nb;
      using vec_param_t = GeneralMidasVector<param_t>;
      using vec_real_t = GeneralMidasVector<real_t>;
      using mat_param_t = GeneralMidasMatrix<param_t>;
   
   private:
      //! Default number of space grid points to average over in each direction (total steps are 2*mNq+1)
      In mNq = I_20;
      //! Default number of historical time steps to average over (total steps are mNt+1)
      In mNt = I_5;
      
      //! Calculation name
      std::string mCalcName;

      //! Vector holding grid points the densities are evaluated at for each mode
      std::vector<vec_real_t> mGridPoints;
      //! Vector holding the primitive modals evaulated at grid points for each mode
      std::vector<std::vector<vec_real_t>> mPrimitiveModals;

      //! Vectors for holding one-mode densities
      std::vector<std::vector<vec_real_t>> mOneModeDensities;
      std::vector<std::vector<vec_real_t>> mSsOneModeDensities;
      std::vector<std::vector<vec_real_t>> mStsOneModeDensities;

   public:
      /******************************************************************//**
      * @name Constructors, destructors and copy/move assignment
      **********************************************************************/
      //!@{
      //! Default constructor
      OneModeDensities() = default;

      //! Constructor from name
      OneModeDensities
         (  const std::string& aCalcName
         );
      
      //! Constructor from name and grid points
      OneModeDensities
         (  const std::string& aCalcName
         ,  const std::vector<vec_real_t>& aGridPoints
         );
      
      //! Constructor from name, grid points, and primitive modals
      OneModeDensities
         (  const std::string& aCalcName
         ,  const std::vector<vec_real_t>& aGridPoints
         ,  const std::vector<std::vector<vec_real_t>>& aPrimitiveModals
         );

      //! Copy contructor
      OneModeDensities(const OneModeDensities&) = default;

      //! Move constructor
      OneModeDensities(OneModeDensities&&) = default;

      // Destructor
      ~OneModeDensities() = default;

      //! Copy assignment
      OneModeDensities& operator=(const OneModeDensities&) = default;

      //! Move assignment
      OneModeDensities& operator=(OneModeDensities&&) = default;
      //!@}


      /******************************************************************//**
      * @name Manage smoothening parameters
      **********************************************************************/
      //!@{
      //! Set the SpaceSmoothening parameter
      void SetSpaceSmoothening
         (  const In aSmoothening
         );

      //! Set the TimeSmoothening parameter
      void SetTimeSmoothening
         (  const In aSmoothening
         );
      //!@}


      /******************************************************************//**
      * @name Update the one-mode densities
      **********************************************************************/
      //!@{
      //! In TDH the one-mode densities are readily available and simply passed into SupplyOneModeDensities at each time-step
      void SupplyOneModeDensities
         (  const std::vector<vec_real_t>& aTdhOneModeDensities
         );
      
      //! Calculate one-mode densities at time-step MCTDH
      void CalcMctdhOneModeDensities
         (  const std::vector<mat_param_t>& aDensityMatrices
         ,  const vec_param_t& aMctdhModals
         ,  const std::vector<std::vector<In>>& aModalOffsets
         );

      //! Calculate one-mode densities at time-step TDVCC
      void CalcTdvccOneModeDensities
         (  const std::vector<mat_param_t>& aDensityMatrices
         );

      //! Calculate one-mode densities at time-step TDMVCC
      void CalcTdmvccOneModeDensities
         (  const std::vector<mat_param_t>& aDensityMatrices
         ,  const std::vector<std::pair<mat_param_t,mat_param_t>>& aTdmvccModalMatrices
         );
      
      //! Calculate space-smoothened one-mode densities at time-step
      void CalcSsOneModeDensities();

      //! Calculate space-time-smothened one-mode densities at time-step
      void CalcStsOneModeDensities();
      //!@}


      /******************************************************************//**
      * @name Write to file
      **********************************************************************/
      //!@{
      //! Write one-mode densities to VibCalc for use in ADGA calculation
      void WriteOneModeDensitiesToAdga() const;

      //! Write one-mode densities to file for specific time-step
      void WriteOneModeDensitiesForStep(const In aStep) const;
      
      //! Write one-mode densities to file for all time-steps
      void WriteOneModeDensities() const;
      //!@}


      /******************************************************************//**
      * @name Queries
      **********************************************************************/
      //!@{
      //! Getters
      const std::vector<vec_real_t>& GetOneModeDensities()     const { return mOneModeDensities[mOneModeDensities.size()-1];    }
      const std::vector<vec_real_t>& GetSsOneModeDensities()   const { return mSsOneModeDensities[mSsOneModeDensities.size()-1];  }
      const std::vector<vec_real_t>& GetStsOneModeDensities()  const { return mStsOneModeDensities[mStsOneModeDensities.size()-1]; }
      //!@}

};

} /* namespace midas::td */

#endif /* ONEMODEDENSITIES_DECL_H_INCLUDED */
