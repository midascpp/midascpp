/*
************************************************************************
*
* @file                 SpectrumCalculator.h
*
* Created:              12-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class for calculating spectra from auto-correlation functions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPECTRUMCALCULATOR_H_INCLUDED
#define SPECTRUMCALCULATOR_H_INCLUDED

#include "input/TdPropertyDef.h"

#include "util/type_traits/Complex.h"

#include "fft_wrapper/FFTWrapper.h"

#include "mmv/MidasVector.h"

namespace midas::td
{
/**
 *
 **/
template
   <  typename T
   >
class SpectrumCalculator
{
   //! We need complex numbers here
   static_assert(midas::type_traits::IsComplexV<T>, "FFT can only be performed on complex numbers!");

   public:
      using param_t = T;
      using step_t = typename midas::type_traits::RealTypeT<param_t>;

      template
         <  template<typename...> typename TVec
         ,  template<typename...> typename AcVec
         >
      using fft_t = std::pair<TVec<step_t>, AcVec<param_t>>;

   private:
      //! Convolute auto-correlation function to prevent Gibbs' phenomenon
      bool mConvoluteAutoCorr = true;

      //! Level of zero-padding
      In mPaddingLevel = I_16;

      //! Shift spectrum towards lower energies by this number
      step_t mSpectrumEnergyShift = C_0;

      //! Screening for spectrum output
      step_t mSpectrumOutputScreeningThresh = C_0;

      //! Interval for spectrum output
      std::pair<step_t, step_t> mSpectrumInterval = std::pair<step_t, step_t>(C_0, C_0);

      //! Normalize max peak to 1 in spectrum
      bool mNormalize = false;

      //! Abs all intensities
      bool mAbsIntensities = false;

      //! Lifetime used for Lorenzian broadening (negative value means no broadening)
      step_t mLifeTime = -C_1;

      /**
       * Convolute autocorrelation function to prevent Gibbs' phenomenon
       *
       * @param arAc             Autocorrelation function for a set of time points
       * @param aTs              Vector of time points
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      void ConvoluteAutoCorr
         (  AcVec<param_t>& arAc
         ,  const TVec<step_t>& aTs
         )  const
      {
         auto dt = aTs.back() - aTs.front();
      
         auto n = arAc.size();
         for(In itime=I_0; itime<n; ++itime)
         {
            const auto& t = aTs[itime];
      
            arAc[itime] *= libmda::numeric::float_geq(std::abs(t), dt)
                        ?  step_t(0.)
                        :  std::cos((C_PI*t) / (step_t(2.)*dt));
         }
      }
      
      /**
       * Transform autocorrelation function to shift the spectrum.
       *
       * @param arAc             Autocorrelation function for a set of time points
       * @param aTs              Vector of time points
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      void ShiftAutoCorr
         (  AcVec<param_t>& arAc
         ,  const TVec<step_t>& aTs
         )  const
      {
         auto n = arAc.size();
         auto shift = mSpectrumEnergyShift;
         assert(aTs.size() == n);
         for(In itime=I_0; itime<n; ++itime)
         {
            const auto& t = aTs[itime];
      
            arAc[itime] *= std::exp(CC_I*shift*t);
         }
      }

      /**
       * Add life-time broadening to auto-correlation function.
       * This results in convolution with Lorenzian functions with FWHM 2/tau (where tau is the lifetime).
       *
       * @param arAc
       * @param aTs
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      void LifeTimeBroadening
         (  AcVec<param_t>& arAc
         ,  const TVec<step_t>& aTs
         )  const
      {
         auto n = arAc.size();
         auto tau = mLifeTime;
         assert(aTs.size() == n);
         for(In itime=I_0; itime<n; ++itime)
         {
            const auto& t = aTs[itime];
      
            arAc[itime] *= std::exp(-std::abs(t)/tau);
         }
      }
      
      /**
       * Do FFT of autocorrelation function
       *
       * @param aFilePrefix   Name of spectrum file
       * @param aAc           Autocorrelation function for a set of time points
       * @param aTs           Vector of time points
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      void AutoCorrFft
         (  const std::string& aFilePrefix
         ,  const AcVec<param_t>& aAc
         ,  const TVec<step_t>& aTs
         )  const
      {
         step_t t_end = aTs.back();
         size_t n_points = aTs.size();

         MidasAssert(aAc.size() == n_points, "Number of points in aAc and aTs is not the same...");
      
         auto fft_data = this->DoFft<AcVec, TVec>(aAc, t_end);
      
         const auto& freqs  = fft_data.first;
         auto& fft_ac = fft_data.second;
      
         if (  this->mNormalize
            )
         {
            // Find max element.
            Nb max = C_0;
            for(size_t i=0; i<fft_ac.size(); ++i)
            {
               max = std::max(max, std::abs(fft_ac[i]));
            }
            // Then normalize
            for(size_t i=0; i<fft_ac.size(); ++i)
            {
               fft_ac[i] /= max;
            }
         }
      
         std::string fft_filename = aFilePrefix + "_spectrum.dat";
         std::ofstream fft_ofs(fft_filename);
         fft_ofs << std::scientific << std::setprecision(16);
         size_t width = 24;
      
         // Get shift
         auto e_shift = this->mSpectrumEnergyShift;
         auto spec_screen = this->mSpectrumOutputScreeningThresh;
         auto interval = this->mSpectrumInterval;
         bool constrain_interval = (interval.first != interval.second);
      
         // Print header
         fft_ofs  << std::setw(width) << "# freq"
                  << std::setw(width) << "Re[I]"
                  << std::setw(width) << "Im[I]"
                  << std::setw(width) << "|I|";
         if (  e_shift
            )
         {
            fft_ofs  << "     NB: Frequencies have been shifted by: " << e_shift << " a.u.";
         }
         fft_ofs  <<  std::endl;
      
         // Print data
         for (In i=0; i<fft_ac.size(); ++i)
         {
            auto freq = freqs[i];
            auto intensity = std::abs(fft_ac[i]);
      
            if (  std::abs(intensity) > spec_screen
               && (  !constrain_interval
                  || (  freq >= interval.first
                     && freq <= interval.second
                     )
                  )
               )
            {
               fft_ofs  << std::setw(width) << freqs[i]
                        << std::setw(width) << std::real(fft_ac[i])
                        << std::setw(width) << std::imag(fft_ac[i])
                        << std::setw(width) << std::abs(fft_ac[i])
                        << std::endl;
            }
         }
      
         fft_ofs.close();
      }
      
      /**
       * Do FFT on vector.
       *
       * @param aVec       Vector of samples (NB: at equally spaced samling points!) to Fourier transform
       * @param aTEnd      End time
       * @return
       *    pair: first = vector of freqencies, second = vector of parameters
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      fft_t<TVec, AcVec> DoFft
         (  const AcVec<param_t>& aVec
         ,  step_t aTEnd
         )  const
      {
         // Perform FFT (backward transform)
         FFTWrapper<param_t, AcVec> fft;
         auto fft_result = fft.Compute(aVec, aTEnd, FFTWrapper<param_t, AcVec>::transformType::BACKWARD, this->mPaddingLevel);
         auto size_out = fft_result.first.size();
      
         // Init result
         fft_t<TVec, AcVec> result;
         auto& freq_vec = result.first;
         auto& fft_vec = result.second;
         freq_vec = TVec<step_t>(size_out);
         fft_vec = AcVec<param_t>(size_out);

         // Move data to containers in result
         for(In i=I_0; i<size_out; ++i)
         {
            freq_vec[i] = std::move(fft_result.first[i]);
            fft_vec[i] = std::move(fft_result.second[i]);
         }

         // Convert frequencies to angular freqencies (look at the definitions)
         auto two_pi = C_2*C_PI;
         for(In i=I_0; i<size_out; ++i)
         {
            if (  this->mAbsIntensities
               )
            {
               fft_vec[i] = std::abs(fft_vec[i]);
            }
            freq_vec[i] *= two_pi;
         }

         // Return result
         return result;
      }



   public:
      //! Constructor from TdPropertyDef
      SpectrumCalculator
         (  const input::TdPropertyDef* const apCalcDef
         )
         :  mConvoluteAutoCorr
               (  apCalcDef->GetConvoluteAutoCorr()
               )
         ,  mPaddingLevel
               (  apCalcDef->GetPaddingLevel()
               )
         ,  mSpectrumEnergyShift
               (  apCalcDef->GetSpectrumEnergyShift().second
               )
         ,  mSpectrumOutputScreeningThresh
               (  apCalcDef->GetSpectrumOutputScreeningThresh()
               )
         ,  mSpectrumInterval
               (  apCalcDef->GetSpectrumInterval()
               )
         ,  mNormalize
               (  apCalcDef->GetNormalizeSpectrum()
               )
         ,  mAbsIntensities
               (  apCalcDef->GetAbsIntensities()
               )
         ,  mLifeTime
               (  apCalcDef->GetLifeTime()
               )
      {
      }

      //! Constructor from values
      SpectrumCalculator
         (  bool aConvolute
         ,  In aPad
         ,  step_t aShift
         ,  step_t aScreen
         ,  const std::pair<step_t, step_t>& aInterval
         ,  bool aNormalize
         ,  bool aAbsInt
         ,  step_t aLifeTime
         )
         :  mConvoluteAutoCorr
               (  aConvolute
               )
         ,  mPaddingLevel
               (  aPad
               )
         ,  mSpectrumEnergyShift
               (  aShift
               )
         ,  mSpectrumOutputScreeningThresh
               (  aScreen
               )
         ,  mSpectrumInterval
               (  aInterval
               )
         ,  mNormalize
               (  aNormalize
               )
         ,  mAbsIntensities
               (  aAbsInt
               )
         ,  mLifeTime
               (  aLifeTime
               )
      {
      }

      /**
       * Calculate and write spectrum to file
       *
       * @param aFilePrefix      Name of spectrum
       * @param arAc
       * @param aTs
       **/
      template
         <  template<typename...> typename AcVec
         ,  template<typename...> typename TVec
         >
      void CalculateAndWriteSpectrum
         (  const std::string& aFilePrefix
         ,  AcVec<param_t>& arAc
         ,  const TVec<step_t>& aTs
         )  const
      {
         // Do life-time broadening
         if (  this->mLifeTime > C_0
            )
         {
            this->LifeTimeBroadening(arAc, aTs);
         }

         // Shift the spectrum
         if (  this->mSpectrumEnergyShift
            )
         {
            this->ShiftAutoCorr(arAc, aTs);
         }

         // Convolute the spectrum to prevent Gibbs' phenomenon
         if (  this->mConvoluteAutoCorr
            )
         {
            this->ConvoluteAutoCorr(arAc, aTs);
         }

         // Do FFT
         this->AutoCorrFft(aFilePrefix, arAc, aTs);
      }

      //! Set energy shift
      void SetSpectrumEnergyShift
         (  step_t aShift
         )
      {
         this->mSpectrumEnergyShift = aShift;
      }
};

} /* namespace midas::td */

#endif /* SPECTRUMCALCULATOR_H_INCLUDED */
