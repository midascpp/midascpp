/*
************************************************************************
*
* @file                 SpectralBasisGeneration.cc
*
* Created:              06-12-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Function for generating spectral basis for a given Hermitian operator
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "td/SpectralBasisGeneration.h"

#include "ni/OneModeInt.h"

#include "vscf/Vscf.h"

namespace midas::td
{

/**
 * Run VSCF to generate spectral-basis modals
 *
 * @param aOperName     Name of operator
 * @param apBasDef      Basis to run calculation in
 * @param aIoLevel      IoLevel for VSCF
 *
 * @return
 *    Number of coefs and name of file with modals
 **/
std::pair<In, std::string>  GenerateSpectralBasisModals
   (  const std::string& aOperName
   ,  const BasDef* const apBasDef
   ,  In aIoLevel
   )
{
   // 1) Run VSCF on operator
   // 1a) Check if operator exists and find OpDef
   // Find OpDef for name
   auto iop = -I_1;
   for(In i=I_0; i<gOperatorDefs.GetNrOfOpers(); ++i)
   {
      if (  gOperatorDefs[i].Name() == aOperName
         )
      {
         iop = i;
         break;
      }
   }
   if (  iop == -I_1
      )
   {
      MIDASERROR("Operator '" + aOperName + "' not found!");
   }

   if (  aIoLevel > I_5
      )
   {
      Mout  << " Running VSCF calculation on operator '" << aOperName << "' to generate orthonormal spectral-basis modals." << std::endl;
   }

   // Find pointer to operator
   OpDef* spec_oper = &gOperatorDefs[iop];

   // Construct one-mode integrals
   OneModeInt one_mode_int(spec_oper, apBasDef, "InMem", false);
   one_mode_int.CalcInt();

   // Construct calcdef
   VscfCalcDef vscf_calcdef;
   std::vector<In> occvec(apBasDef->GetmNoModesInBas(), I_0);
   vscf_calcdef.SetOccVec(occvec);
   std::string vscfname = "vscf_" + spec_oper->Name();
   vscf_calcdef.SetName(vscfname);
   vscf_calcdef.SetOper(spec_oper->Name());
   vscf_calcdef.SetBasis(apBasDef->GetmBasName());
   vscf_calcdef.SetSave(true);
   vscf_calcdef.SetIoLevel(aIoLevel);

   // Construct vscf object
   Vscf vscf(&vscf_calcdef, spec_oper, apBasDef, &one_mode_int);
   vscf.StartGuess();
   vscf.Solve();
   vscf.Finalize();

   // Return modals
   auto ncoefs = vscf.pModals()->Size();
   auto label = vscf.pModals()->Label();

   return std::make_pair(ncoefs, label);
}

} /* namespace midas::td */
