/**
 *******************************************************************************
 * 
 * @file    GaussPulse_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef GAUSSPULSE_DECL_H_INCLUDED
#define GAUSSPULSE_DECL_H_INCLUDED

// Standard headers.
#include <cmath>
#include <map>

// Midas headers.
#include "inc_gen/TypeDefs.h"

// Forward declarations.

namespace midas::td
{
   //! Enum IDs for the GaussPulse parameters.
   enum class GaussParamID
   {  AMPL
   ,  TPEAK
   ,  SIGMA
   ,  FREQ
   ,  PHASE
   ,  FWHM
   };

   /************************************************************************//**
    * @brief
    *    A Gaussian pulse, for use with e.g. TdOperTerm.
    *
    * The template parameters determine:
    * -  STEP_T: time type (function argument), and implicitly also the type of
    *    the parameters. Must be real.
    * -  EVAL_T: the return type of evaluating the function/derivative. The
    *    functions are real-valued but this allows for returning them as complex.
    *    (Modify this if you like.)
    *
    * Formula (`f(t) = A exp(-(t - t0)/(2s^2))cos(w(t-t0) - p)`)
    * \f[
    *    f(t) 
    *    =  A  \exp\left(-\frac{t - t_\text{peak})^2}{2\sigma^2}\right)
    *          \cos(\omega (t-t_\text{peak}) - \phi)
    * \f]
    *
    * Derivative (`df/dt = -A exp(-(t-t0)/(2s^2))((t-t0)/s^2 cos(wt-p) + w sin(w(t-t0)-p)`):
    * \f[
    *    \dot{f}(t)
    *    = -A  \exp\left(-\frac{t - t_\text{peak})^2}{2\sigma^2}\right)
    *          \left(
    *             \frac{t - t_\text{peak}}{\sigma^2} \cos(\omega (t-t_\text{peak}) - \phi)
    *             +  \omega \sin(\omega (t-t_\text{peak}) - \phi)
    *          \right)
    * \f]
    *
    * Useful relations:
    * -  Full width at half maximum is 
    *    \f$ \text{FWHM} = 2\sqrt{2\ln 2}\sigma \approx 2.355 \sigma \f$
    * -  Area under Gaussian profile:
    *    \f$ \int_{-\infty}^{\infty} p(t)dt = A \sigma \sqrt{2\pi} \f$.
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   class GaussPulse
   {
      public:
         using step_t = STEP_T;
         using param_t = step_t;
         using eval_t = EVAL_T;

         GaussPulse() = default;
         GaussPulse(const GaussPulse&) = default;
         GaussPulse(GaussPulse&&) = default;
         GaussPulse& operator=(const GaussPulse&) = default;
         GaussPulse& operator=(GaussPulse&&) = default;
         ~GaussPulse() = default;

         //! Construct from parameters.
         GaussPulse
            (  param_t aAmpl
            ,  param_t aTpeak
            ,  param_t aSigma
            ,  param_t aFreq
            ,  param_t aPhase
            );

         //! Construct from map of parameters.
         GaussPulse(const std::map<GaussParamID,param_t>&);

         /******************************************************************//**
          * @name Settings
          *********************************************************************/
         //!@{

         //@{
         //! Directly set the parameters.
         void SetAmpl(param_t aVal) {mAmpl = aVal;}
         void SetTpeak(param_t aVal) {mTpeak = aVal;}
         void SetSigma(param_t aVal) {mSigma = aVal;}
         void SetFreq(param_t aVal) {mFreq = aVal;}
         void SetPhase(param_t aVal) {mPhase = aVal;}
         //@}

         //! Set \f$ \sigma \f$ to match given FWHM value.
         void SetFwhm(param_t aVal) {mSigma = aVal/this->FwhmSigmaRatio();}
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         param_t Ampl() const {return mAmpl;}
         param_t Tpeak() const {return mTpeak;}
         param_t Sigma() const {return mSigma;}
         param_t Freq() const {return mFreq;}
         param_t Phase() const {return mPhase;}
         param_t Fwhm() const {return Sigma()*this->FwhmSigmaRatio();}

         //! Print settings to ostream.
         void PrintSettings(std::ostream&, const Uin aIndent, const Uin aWidth) const;
         //!@}

         /******************************************************************//**
          * @name Function evaluations
          *********************************************************************/
         //!@{
         eval_t operator()(step_t) const;
         eval_t Deriv(step_t) const;
         //!@}

      private:
         constexpr param_t FwhmSigmaRatio
            (
            )  const
         {
            return 2*sqrt(2*log(2));
         }

         //! Max amplitude of Gaussian profile, i.e. at mTpeak. (\f$ A \f$)
         param_t mAmpl = 1.0;

         //! Peak time, i.e. time where Gaussian profile attains max. value. (\f$ t_\text{peak} \f$)
         param_t mTpeak = 0.0;

         //! Standard deviation of Gaussian profile. (\f$ \sigma \f$)
         param_t mSigma = 1.0;

         //! Angular frequency of cosine factor. (\f$ \omega \f$)
         param_t mFreq = 0.0;

         //! Phase shift of cosine factor. (\f$ \phi \f$)
         param_t mPhase = 0.0;
   };

} /* namespace midas::td */


#endif/*GAUSSPULSE_DECL_H_INCLUDED*/
