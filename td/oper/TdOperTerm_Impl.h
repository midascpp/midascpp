/**
 *******************************************************************************
 * 
 * @file    TdOperTerm_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDOPERTERM_IMPL_H_INCLUDED
#define TDOPERTERM_IMPL_H_INCLUDED

// Midas headers.
#include "util/Error.h"
#include "util/AbsVal.h"

namespace midas::td
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::TdOperTerm
      (  const oper_t& arOper
      ,  lin_coef_t aConstCoef
      )
      :  mCoef([coef = aConstCoef](step_t)->lin_coef_t {return coef;})
      ,  mDeriv([](step_t)->lin_coef_t {return 0;})
      ,  mpOper(&arOper)
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::TdOperTerm
      (  const oper_t& arOper
      ,  func_t aCoef
      ,  func_t aDeriv
      )
      :  mCoef(std::move(aCoef))
      ,  mDeriv(std::move(aDeriv))
      ,  mpOper(&arOper)
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::TdOperTerm
      (  const oper_t& arOper
      ,  func_t aCoef
      )
      :  mCoef(std::move(aCoef))
      ,  mDeriv(CentralDifference())
      ,  mpOper(&arOper)
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   const typename TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::oper_t& TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::Oper
      (
      )  const
   {
      if (mpOper == nullptr)
      {
         MIDASERROR("Tried to return *mpOper, but mpOper == nullptr.");
      }
      return *mpOper;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   typename TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::lin_coef_t TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::Coef
      (  step_t aTime
      )  const
   {
      if (mCoef)
      {
         return mCoef(aTime);
      }
      else
      {
         MIDASERROR("mCoef does not contain a valid target.");
         return lin_coef_t(0);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   typename TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::lin_coef_t TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::Deriv
      (  step_t aTime
      )  const
   {
      if (mDeriv)
      {
         return mDeriv(aTime);
      }
      else
      {
         MIDASERROR("mDeriv does not contain a valid target.");
         return lin_coef_t(0);
      }
   }

   /************************************************************************//**
    * @note
    *    The `dt_[pm] = t_p_dt - t` is to account for roundoff errors. The
    *    `volatile` is so that the compiler won't optimize it away, because
    *    it's mathematically equal to `dt`.
    *    It improves the accuracy a little in some cases, maybe by an order of
    *    magnitude.
    *    It seems though that you should expect an accuracy of about sqrt(eps)
    *    using this formula, even for something well-behaved like cos(t) or
    *    t^2.
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T
      >
   typename TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::func_t TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>::CentralDifference
      (  
      )  const
   {
      func_t deriv = [&f = this->mCoef, dfactor = sqrt(mDisp2)](step_t t)->lin_coef_t
         {
            step_t dt = dfactor * std::max(step_t(1), midas::util::AbsVal(t));
            const volatile step_t t_p_dt = t + dt;
            const volatile step_t t_m_dt = t - dt;
            const step_t dt_eff = t_p_dt - t_m_dt;
            return (f(t_p_dt) - f(t_m_dt))/dt_eff;
         };
      return deriv;
   }

} /* namespace midas::td */


#endif/*TDOPERTERM_IMPL_H_INCLUDED*/
