/**
 *******************************************************************************
 * 
 * @file    LinCombOper_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef LINCOMBOPER_DECL_H_INCLUDED
#define LINCOMBOPER_DECL_H_INCLUDED

// Standard headers.
#include <functional>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/oper/TdOperTerm.h"

// Forward declarations.
class OpDef;

namespace midas::td
{
   /************************************************************************//**
    * @brief
    *    It's just a vector TdOperTerm%s, although some std::vector features
    *    might be disabled due to TdOperTerm having a deleted default
    *    constructor.
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T = OpDef
      >
   class LinCombOper
      :  public std::vector<TdOperTerm<STEP_T,LIN_COEF_T,OPER_T>>
   {
      using step_t = STEP_T;
      using lin_coef_t = LIN_COEF_T;
      using oper_t = OPER_T;

      public:
         template<class... Args>
         LinCombOper(Args&&... args)
            :  std::vector<TdOperTerm<step_t,lin_coef_t,oper_t>>(std::forward<Args>(args)...)
         {
         }
   };

} /* namespace midas::td */


#endif/*LINCOMBOPER_DECL_H_INCLUDED*/
