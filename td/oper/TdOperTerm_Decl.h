/**
 *******************************************************************************
 * 
 * @file    TdOperTerm_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDOPERTERM_DECL_H_INCLUDED
#define TDOPERTERM_DECL_H_INCLUDED

// Standard headers.
#include <functional>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"

// Forward declarations.
class OpDef;

namespace midas::td
{
   /************************************************************************//**
    * @brief
    *    Holds a pointer to an OpDef, a (possibly time-dependent) coefficient,
    *    and the time-derivative of the coefficient (implemented with a
    *    central-difference-formula as fallback option).
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename LIN_COEF_T
      ,  class OPER_T = OpDef
      >
   class TdOperTerm
   {
      public:
         static_assert(std::is_floating_point_v<STEP_T>, "STEP_T must be (real) floating-point.");
         using step_t = STEP_T;
         using lin_coef_t = LIN_COEF_T;
         using func_t = std::function<lin_coef_t(step_t)>;
         using oper_t = OPER_T;

         //! Don't allow default construction, since oper_t* will be nullptr.
         TdOperTerm() = delete;

         //@{
         //! Copies okay (doesn't manage the pointed oper_t); just take care ptr won't dangle.
         TdOperTerm(const TdOperTerm&) = default;
         TdOperTerm& operator=(const TdOperTerm&) = default;
         TdOperTerm(TdOperTerm&&) = default;
         TdOperTerm& operator=(TdOperTerm&&) = default;
         ~TdOperTerm() = default;
         //@}

         //! Construct term with constant coefficient. (Time-der. automatically zero.)
         TdOperTerm(const oper_t&, lin_coef_t);

         //! Construct term with given time-dep. coefficient and derivative.
         TdOperTerm(const oper_t&, func_t aCoef, func_t aDeriv);

         //! Construct term with given time-dep. coefficient and _fallback_ (cent.diff.) derivative.
         TdOperTerm(const oper_t&, func_t aCoef);

         //! Get reference to the time-indep. oper. of the term. (Hard error if nullptr.)
         const oper_t& Oper() const;

         //! Value of linear coefficient at given time.
         lin_coef_t Coef(step_t) const;

         //! Value of time derivative of coefficient at given time.
         lin_coef_t Deriv(step_t) const;

      private:
         func_t mCoef;
         func_t mDeriv;
         const oper_t* mpOper = nullptr;

         //! Disp = sqrt(eps); generally good trade-off of formula accuracy/round-off noise.
         inline static constexpr step_t mDisp2 = std::numeric_limits<step_t>::epsilon();

         //! 2-point central difference derivative approximation formula for mCoef.
         func_t CentralDifference() const;
   };

} /* namespace midas::td */


#endif/*TDOPERTERM_DECL_H_INCLUDED*/
