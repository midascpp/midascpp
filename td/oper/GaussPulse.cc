/**
 *******************************************************************************
 * 
 * @file    GaussPulse.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/oper/GaussPulse.h"
#include "td/oper/GaussPulse_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_GAUSSPULSE(STEP_T,EVAL_T) \
   namespace midas::td \
   { \
      template class GaussPulse<STEP_T,EVAL_T>; \
   } /* namespace midas::td */ \
   

// Instantiations.
INSTANTIATE_GAUSSPULSE(Nb,Nb);
INSTANTIATE_GAUSSPULSE(Nb,std::complex<Nb>);

#undef INSTANTIATE_GAUSSPULSE
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
