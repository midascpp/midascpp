/**
 *******************************************************************************
 * 
 * @file    LinCombOper.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/oper/LinCombOper.h"
#include "td/oper/LinCombOper_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_LINCOMBOPER(STEP_T,LIN_COEF_T) \
   namespace midas::td \
   { \
   } /* namespace midas::td */ \
   

// Instantiations.
INSTANTIATE_LINCOMBOPER(Nb,Nb);
INSTANTIATE_LINCOMBOPER(Nb,std::complex<Nb>);

#undef INSTANTIATE_LINCOMBOPER
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
