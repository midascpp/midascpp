/*
************************************************************************
*
* @file                 OneModeDensities_Impl.h
*
* Created:              29-11-2021
*
* Author:               Nicolai Machholdt Høyer (hoyer@chem.au.dk)
*
* Short Description:    Class for calculating the one-mode density across
*                       different td methods
*
* Last modified:        01-03-2022
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ONEMODEDENSITIES_IMPL_H_INCLUDED
#define ONEMODEDENSITIES_IMPL_H_INCLUDED

#include "OneModeDensities_Decl.h"
#include "input/BasDef.h"


// Extern/forward declares.
extern std::string gAnalysisDir;
extern std::string gMainDir;


namespace midas::td
{

/************************************************************************//**
 * Constructor from name of calculation.
 * 
 * @param   aCalcName   Name of the TD calculation.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
OneModeDensities<PARAM_T>::OneModeDensities
   (  const std::string& aCalcName
   )
   :  mCalcName(aCalcName)
{
}

/************************************************************************//**
 * Constructor from name and grid points.
 * 
 * @param   aCalcName   Name of the TD calculation.
 * @param   aGridPoints Vector holding the grid points for each mode.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
OneModeDensities<PARAM_T>::OneModeDensities
   (  const std::string& aCalcName
   ,  const std::vector<vec_real_t>& aGridPoints
   )
   :  mCalcName(aCalcName)
   ,  mGridPoints(aGridPoints)
{
}

/************************************************************************//**
 * Constructor from name and primitive modals evaluated at a grid.
 * 
 * @param   aCalcName         Name of the TD calculation.
 * @param   aGridPoints       Vector holding the grid points for each mode.
 * @param   aPrimitiveModals  Vector holding the value of each primitive modal
 *                            at a grid point for each mode.
 *
 ***************************************************************************/
template
   < typename PARAM_T
   >
OneModeDensities<PARAM_T>::OneModeDensities
   (  const std::string& aCalcName
   ,  const std::vector<vec_real_t>& aGridPoints
   ,  const std::vector<std::vector<vec_real_t>>& aPrimitiveModals
   )
   :  mCalcName(aCalcName)
   ,  mGridPoints(aGridPoints)
   ,  mPrimitiveModals(aPrimitiveModals)
{
   // check that VSC modals are not empty
   if (mPrimitiveModals.size() == 0)
   {
      MIDASERROR("The primitive modal vector used to construct OneModeDensities is empty!");
   }
   for (In i_mode = I_0; i_mode < mPrimitiveModals.size(); ++i_mode)
   {
      if (mPrimitiveModals[i_mode].size() == 0)
      {
         MIDASERROR("The grid has a size of 0");
      }
   }
}

/************************************************************************//**
 * Set the space-smoothening parameter.
 * 
 * @param   aSmoothening   The number of grid points in each direction of the
 *                         grid to smoothen the density over.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::SetSpaceSmoothening(const In aSmoothening)
{
   mNq = aSmoothening;
}

/************************************************************************//**
 * Set the time-smoothening parameter.
 * 
 * @param   aSmoothening   The number of historic steps in each direction to
 *                         smoothen the density over.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::SetTimeSmoothening(const In aSmoothening)
{
   mNt = aSmoothening;
}

/************************************************************************//**
 * Supply the one-mode densities at the current time-step.
 * 
 * @param   aOneModeDensities A vector of one-mode densities.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::SupplyOneModeDensities
   (  const std::vector<vec_real_t>& aOneModeDensities
   )
{
   for (In i_mode = I_0; i_mode < aOneModeDensities.size(); ++i_mode)
   {
      // Check that the size of the vector is the same size as in previous time step
      if (  mOneModeDensities.size() != 0
         && aOneModeDensities[i_mode].Size() != mOneModeDensities[mOneModeDensities.size()-1][i_mode].Size() )
      {
         MIDASERROR("The size of one_mode_dens_for_mode_i is different from last iteration");
      }
   }
   mOneModeDensities.emplace_back(std::move(aOneModeDensities));
}

/************************************************************************//**
 * Calculate the one-mode densities of a MCTDH wave function.
 * 
 * @param   aDensityMatrices  The MCTDH density matrix.
 * @param   aMctdhModals      The MCTDH active modals for each mode.
 * @param   aModalOffsets     Vector of offsets to navigate aMctdhModals.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::CalcMctdhOneModeDensities
   (  const std::vector<mat_param_t>& aDensityMatrices
   ,  const vec_param_t&                              aMctdhModals
   ,  const std::vector<std::vector<In>>&             aModalOffsets
   )
{
   // Check that the primitive modals have been created
   if (mPrimitiveModals.size() == 0)
   {
      MIDASERROR("The primitive modals have not been created yet");
   }
   
   // Vector to hold the one-mode densities for each mode
   std::vector<vec_real_t> one_mode_dens;
   one_mode_dens.reserve(mPrimitiveModals.size());

   // Loop over each mode
   for (In i_mode = I_0; i_mode < mPrimitiveModals.size(); ++i_mode)
   {
      // Get primitive modals for specific mode
      const auto& prim_modals_for_mode_i = mPrimitiveModals[i_mode];

      // Get density matrix (rho)
      const auto& densmat_for_mode_i = aDensityMatrices[i_mode];

      // Get mctdh modals and offsets for mode
      const auto& offsets = aModalOffsets[i_mode];

      // Create vector to hold one-mode densities for mode
      vec_real_t one_mode_dens_for_mode_i(prim_modals_for_mode_i.size());

      // Loop over grid points
      for (In q_idx = I_0; q_idx < prim_modals_for_mode_i.size(); ++q_idx)
      {
         // dumm loop to create td_modals
         const auto& prim_modals_for_q = prim_modals_for_mode_i[q_idx];
         vec_param_t td_modals(offsets.size());
         for (In i_td_modal = I_0; i_td_modal < offsets.size(); ++i_td_modal)
         {
            param_t td_modal(0);
            for (In i_prim_modal = I_0; i_prim_modal < prim_modals_for_q.Size(); ++i_prim_modal)
            {
               td_modal += prim_modals_for_q[i_prim_modal] * aMctdhModals[offsets[i_td_modal] + i_prim_modal];
            }
            td_modals[i_td_modal] = td_modal;
         }

         // Contract one_mode_dens_for_q = phi[p]^T D[p,q] phi[q]
         // one_mode_dens_for_q_check[p] = D[p,q] phi[q]
         vec_param_t one_mode_dens_for_q_check = vec_param_t(densmat_for_mode_i * td_modals);            
         // one_mode_dens_for_q = phi[p]^T one_mode_dens_for_q_check[p]
         param_t one_mode_dens_for_q = SumProdElems(one_mode_dens_for_q_check, td_modals, I_0, one_mode_dens_for_q_check.size());

         // Copy one-mode density into vector for storage
         if constexpr (midas::type_traits::IsComplexV<param_t>)
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q.real();
         }
         else
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q;
         }
      }
      // Check that the size of the vector is the same size as in previous time step
      if (  mOneModeDensities.size() != 0
         && one_mode_dens_for_mode_i.Size() != mOneModeDensities[mOneModeDensities.size()-1][i_mode].Size() )
      {
         MIDASERROR("The size of one_mode_dens_for_mode_i is different from last iteration");
      }
      one_mode_dens.emplace_back(std::move(one_mode_dens_for_mode_i));
   }
   // Move one-mode density to storage
   mOneModeDensities.emplace_back(std::move(one_mode_dens));
}

/************************************************************************//**
 * Calculate the one-mode densities of a TDVCC wave function.
 * 
 * @param   aDensityMatrices  The TDVCC density matrix.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::CalcTdvccOneModeDensities
   (  const std::vector<mat_param_t>& aDensityMatrices
   )
{
   // Check that the primitive modals have been created
   if (mPrimitiveModals.size() == 0)
   {
      MIDASERROR("The primitive modals have not been created yet");
   }
   
   // Vector to hold the one-mode densities for each mode
   std::vector<vec_real_t> one_mode_dens;
   one_mode_dens.reserve(mPrimitiveModals.size());

   // Loop over each mode
   for (In i_mode = I_0; i_mode < mPrimitiveModals.size(); ++i_mode)
   {
      // Get primitive modals for specific mode
      const auto& prim_modals_for_mode_i = mPrimitiveModals[i_mode];

      // Get density matrix (rho)
      const auto& dens = aDensityMatrices[i_mode];

      // Create vector to hold one-mode densities for mode
      vec_real_t one_mode_dens_for_mode_i(prim_modals_for_mode_i.size());

      // Loop over grid points
      for (In q_idx = I_0; q_idx < prim_modals_for_mode_i.size(); ++q_idx)
      {
         // Contract one_mode_dens_for_q = phi[alpha]^T D[alpha,beta] phi[beta]
         // one_mode_dens_for_q_check[alpha] = D[alpha,beta] phi[beta]
         vec_param_t one_mode_dens_for_q_check = vec_param_t(dens * prim_modals_for_mode_i[q_idx]);            
         // one_mode_dens_for_q = phi[alpha]^T one_mode_dens_for_q_check[alpha]
         param_t one_mode_dens_for_q = SumProdElems(one_mode_dens_for_q_check, (vec_param_t)prim_modals_for_mode_i[q_idx], I_0, one_mode_dens_for_q_check.size());

         // Copy one-mode density into vector for storage
         if constexpr (midas::type_traits::IsComplexV<param_t>)
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q.real();
         }
         else
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q;
         }
      }
      // Check that the size of the vector is the same size as in previous time step
      if (  mOneModeDensities.size() != 0
         && one_mode_dens_for_mode_i.Size() != mOneModeDensities[mOneModeDensities.size()-1][i_mode].Size() )
      {
         MIDASERROR("The size of one_mode_dens_for_mode_i is different from last iteration");
      }
      one_mode_dens.emplace_back(std::move(one_mode_dens_for_mode_i));
   }
   // Move one-mode density to storage
   mOneModeDensities.emplace_back(std::move(one_mode_dens));
}

/************************************************************************//**
 * Calculate the one-mode densities of a TDMVCC wave function.
 * 
 * @param   aDensityMatrices     The TDMVCC density matrix.
 * @param   aTdmvccModalMatrices The U and W matrices for each mode.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::CalcTdmvccOneModeDensities
   (  const std::vector<mat_param_t>& aDensityMatrices
   ,  const std::vector<std::pair<mat_param_t,mat_param_t>>& aTdmvccModalMatrices
   )
{
   // Check that the primitive modals have been created
   if (mPrimitiveModals.size() == 0)
   {
      MIDASERROR("The primitive modals have not been created yet");
   }
   
   // Vector to hold the one-mode densities for each mode
   std::vector<vec_real_t> one_mode_dens;
   one_mode_dens.reserve(mPrimitiveModals.size());

   // Loop over each mode
   for (In i_mode = I_0; i_mode < mPrimitiveModals.size(); ++i_mode)
   {
      // Get primitive modals for specific mode
      const auto& prim_modals_for_mode_i = mPrimitiveModals[i_mode];

      // Get density matrix (rho)
      const auto& densmat_for_mode_i = aDensityMatrices[i_mode];

      // Get ModalMats for the transformations.
      const auto& modal_mats = aTdmvccModalMatrices[i_mode];
      const auto& U = modal_mats.first;
      const auto& W = modal_mats.second;

      // Contract D[alpha,beta] = U[alpha,q] rho[q,p] W[p,beta]
      // D^check[q,beta] = rho[q,p] W[p,beta]
      const auto& dens_check = mat_param_t(densmat_for_mode_i * W);
      // D[alpha,beta] = U[alpha,q] D^check[q,beta] 
      const auto& dens = mat_param_t(U * dens_check);

      // Create vector to hold one-mode densities for mode
      vec_real_t one_mode_dens_for_mode_i(prim_modals_for_mode_i.size());

      // Loop over grid points
      for (In q_idx = I_0; q_idx < prim_modals_for_mode_i.size(); ++q_idx)
      {
         // Contract one_mode_dens_for_q = phi[alpha]^T D[alpha,beta] phi[beta]
         // one_mode_dens_for_q_check[alpha] = D[alpha,beta] phi[beta]
         vec_param_t one_mode_dens_for_q_check = vec_param_t(dens * prim_modals_for_mode_i[q_idx]);            
         // one_mode_dens_for_q = phi[alpha]^T one_mode_dens_for_q_check[alpha]
         param_t one_mode_dens_for_q = SumProdElems(one_mode_dens_for_q_check, (vec_param_t)prim_modals_for_mode_i[q_idx], I_0, one_mode_dens_for_q_check.size());

         // Copy one-mode density into vector for storage
         if constexpr (midas::type_traits::IsComplexV<param_t>)
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q.real();
         }
         else
         {
            one_mode_dens_for_mode_i[q_idx] = one_mode_dens_for_q;
         }
      }
      // Check that the size of the vector is the same size as in previous time step
      if (  mOneModeDensities.size() != 0
         && one_mode_dens_for_mode_i.Size() != mOneModeDensities[mOneModeDensities.size()-1][i_mode].Size() )
      {
         MIDASERROR("The size of one_mode_dens_for_mode_i is different from last iteration");
      }
      one_mode_dens.emplace_back(std::move(one_mode_dens_for_mode_i));
   }
   // Move one-mode density to storage
   mOneModeDensities.emplace_back(std::move(one_mode_dens));
}


/************************************************************************//**
 * Calculate the space-smoothening of the one-mode densities at current step.
 * 
 * The space-smoothening is carried out over mNq grid points in each direction
 * The total number of grid points to smoothen over is 2*mNq+1.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::CalcSsOneModeDensities()
{
   if (mOneModeDensities.size() == I_0)
   {
      MIDASERROR("mOneModeDensities is empty!");
   }
   // Get one-mode densities for this time-step
   const auto& one_mode_dens = mOneModeDensities[mOneModeDensities.size()-1];

   // Vector to hold the ss-one-mode densities for each mode
   std::vector<vec_real_t> ss_one_mode_dens;
   ss_one_mode_dens.reserve(one_mode_dens.size());
   
   // Loop over each mode
   for (In i_mode = I_0; i_mode < one_mode_dens.size(); ++i_mode)
   {
      // Get one-mode density for mode
      const auto& one_mode_dens_for_mode_i = one_mode_dens[i_mode];

      // Vector for to hold ss-one-mode densities for current mode
      vec_real_t ss_one_mode_dens_for_mode_i(one_mode_dens_for_mode_i.Size());
      // Loop over grid points
      for (In q_idx = I_0; q_idx < one_mode_dens_for_mode_i.Size(); ++q_idx)
      {
         real_t ss_mode_dens(0);
         // Calculate the space-smoothened one-mode density
         if (q_idx >= mNq && q_idx <= one_mode_dens_for_mode_i.Size()-mNq-1)
         {
            for (In ss_q_idx = q_idx-mNq; ss_q_idx < q_idx+mNq+1; ++ss_q_idx)
            {
               if (ss_q_idx < 0 || ss_q_idx >= one_mode_dens_for_mode_i.Size())
                  MIDASERROR("Index ss_q_idx is out of bounds: " + std::to_string(ss_q_idx));

               ss_mode_dens += one_mode_dens_for_mode_i[ss_q_idx];
            }
         }
         else if (q_idx < mNq && q_idx <= one_mode_dens_for_mode_i.Size()-mNq-1)
         {
            for (In ss_q_idx = I_0; ss_q_idx < q_idx+mNq+1; ++ss_q_idx)
            {
               if (ss_q_idx < 0 || ss_q_idx >= one_mode_dens_for_mode_i.Size())
                  MIDASERROR("Index ss_q_idx is out of bounds: " + std::to_string(ss_q_idx));

               ss_mode_dens += one_mode_dens_for_mode_i[ss_q_idx];
            }
         }
         else if (q_idx >= mNq && q_idx > one_mode_dens_for_mode_i.Size()-mNq-1)
         {
            for (In ss_q_idx = q_idx-mNq; ss_q_idx < one_mode_dens_for_mode_i.Size(); ++ss_q_idx)
            {
               if (ss_q_idx < 0 || ss_q_idx >= one_mode_dens_for_mode_i.Size())
                  MIDASERROR("Index ss_q_idx is out of bounds: " + std::to_string(ss_q_idx));

               ss_mode_dens += one_mode_dens_for_mode_i[ss_q_idx];
            }
         }
         else
         {
            MIDASERROR("Current grid point is not a part of the grid?");
         }

         // divide by the number of points used in the space-smoothening
         if ( !(q_idx >= mNq && q_idx <= one_mode_dens_for_mode_i.Size()-mNq-1) )
         {
            if (q_idx < mNq)
            {
               ss_mode_dens /= mNq+1+q_idx;
            }
            else if (q_idx > one_mode_dens_for_mode_i.Size()-mNq-1)
            {
               ss_mode_dens /= mNq+(one_mode_dens_for_mode_i.Size() - q_idx);
            }
         }
         else
         {
            ss_mode_dens /= 2*mNq+1;
         }

         // Copy ss-one-mode density into vector for storage
         ss_one_mode_dens_for_mode_i[q_idx] = ss_mode_dens;
      }
      ss_one_mode_dens.emplace_back(std::move(ss_one_mode_dens_for_mode_i));
   }
   // Move ss-one-mode density to storage
   mSsOneModeDensities.emplace_back(std::move(ss_one_mode_dens));
}

/************************************************************************//**
 * Calculate the space-time-smoothening over the space-smoothened one-mode
 * densities for the current step.
 * 
 * The time-smoothening is carried out over the current and mNt hstoric steps.
 * The total number of steps in the smoothening is mNq.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::CalcStsOneModeDensities()
{
   if (mSsOneModeDensities.size() == I_0)
   {
      MIDASERROR("mSsOneModeDensities is empty!");
   }
   if (mStsOneModeDensities.size() == I_0)
   {
      mStsOneModeDensities.emplace_back(mSsOneModeDensities[0]);
   }
   else
   {
      // Vector to hold sts-modal densities at this time-step
      std::vector<vec_real_t> sts_one_mode_dens;
      sts_one_mode_dens.reserve(mSsOneModeDensities[0].size());
      
      // Determine number of time steps which are averaged over
      In div;
      if (mSsOneModeDensities.size() > mNt)
      {
         div = mNt + C_1;
      }
      else
      {
         div = In(mSsOneModeDensities.size());
      }

      // Loop backwards in time through mSsOneModeDensities
      for (auto t_back = mSsOneModeDensities.rbegin(); t_back != mSsOneModeDensities.rend() && t_back != mSsOneModeDensities.rbegin()+mNt+1 ; ++t_back)
      {
         // Get ss-one-mode densities for specific time-step
         const auto& ss_one_mode_dens_for_time_t = *t_back;

         // Loop over each mode
         for (In i_mode = I_0; i_mode < ss_one_mode_dens_for_time_t.size(); ++i_mode)
         {
            if (t_back == mSsOneModeDensities.rbegin())
            {
               sts_one_mode_dens.emplace_back(ss_one_mode_dens_for_time_t[i_mode]);
            }
            else if (t_back+1 == mSsOneModeDensities.rend() || t_back+1 == mSsOneModeDensities.rbegin()+mNt+1)
            {
               // If final t_back divide by div
               sts_one_mode_dens[i_mode] += ss_one_mode_dens_for_time_t[i_mode];
               sts_one_mode_dens[i_mode] /= div;
            }
            else
            {
               sts_one_mode_dens[i_mode] += ss_one_mode_dens_for_time_t[i_mode];
            }
         }
      }
      // Move sts-one-mode density to storage
      mStsOneModeDensities.emplace_back(std::move(sts_one_mode_dens));
   }
}

/************************************************************************//**
 * Write the curret space-time-smoothened one-mode density to a file readable
 * by ADGA and place it in the Vibcalc/analysis/ folder.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::WriteOneModeDensitiesToAdga() const
{
   if (mStsOneModeDensities.size() == I_0)
   {
      MIDASERROR("There is no one-mode densities to write");
   }
   if (mGridPoints.size() == I_0)
   {
      MIDASERROR("The grid points has not been supplied");
   }

   // Setup directories for data
   std::string working_dir = gMainDir + "/VibCalc/";
   if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
   {
      MIDASERROR("Directory " + working_dir + " could not be created");
   }
   working_dir += "analysis/";
   if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
   {
      MIDASERROR("Directory " + working_dir + " could not be created");
   }
   In width = 30;
   
   // Get sts data for current step
   const auto& sts_one_mode_dens = mStsOneModeDensities.back();

   // Loop over each mode
   for (In i_mode = I_0; i_mode < sts_one_mode_dens.size(); ++i_mode)
   {
      // Get grid points for specific mode
      const auto& grid_points = mGridPoints[i_mode];

      // Get data for specific mode
      const auto& sts_one_mode_dens_for_mode_i = sts_one_mode_dens[i_mode];

      // Setup file for each mode.
      std::string file_name = working_dir + "MeanDens_Prop1_Q" + std::to_string(i_mode) + ".mplot";
      std::ofstream ofs;
      ofs.open(file_name);
      ofs << std::scientific << std::setprecision(22);

      // Loop over grid points
      for (In q_idx = I_0; q_idx < grid_points.Size(); ++q_idx)
      {
         ofs << grid_points[q_idx];
         ofs << std::setw(width) << sts_one_mode_dens_for_mode_i[q_idx];
         ofs << std::endl;
      }
   }
}

/************************************************************************//**
 * Write the all the saved one-mode densities for a given step to file.
 * 
 * @param   aStep       Time-step for which the densities should be written.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::WriteOneModeDensitiesForStep(const In aStep) const
{
   if (mOneModeDensities.size() == 0)
   {
      MIDASERROR("There are no one-mode densities to write");
   }
   if (aStep >= mOneModeDensities.size())
   {
      MIDASERROR("The requested step is out of bounds");
   }
   if (mGridPoints.size() == I_0)
   {
      MIDASERROR("The grid points has not been supplied");
   }

   // Setup directories for data
   std::string working_dir = gAnalysisDir + "/" + mCalcName + "_one_mode_density_plots" + "/";
   if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
   {
      MIDASERROR("Directory " + working_dir + " could not be created");
   }
   working_dir += std::to_string(aStep) + "/";
   if (!midas::filesystem::IsDir(working_dir) && midas::filesystem::Mkdir(working_dir) != I_0)
   {
      MIDASERROR("Directory " + working_dir + " could not be created");
   }
   size_t width = 25;

   // Get data for specific time step
   const auto& one_mode_dens = mOneModeDensities[aStep-1];
   const auto& ss_one_mode_dens = mSsOneModeDensities[aStep-1];
   const auto& sts_one_mode_dens = mStsOneModeDensities[aStep-1];

   // Loop over each mode
   for (In i_mode = I_0; i_mode < one_mode_dens.size(); ++i_mode)
   {
      // Get grid points for specific mode
      const auto& grid_points = mGridPoints[i_mode];

      // Get data for specific mode
      const auto& one_mode_dens_for_mode_i = one_mode_dens[i_mode];
      const auto& ss_one_mode_dens_for_mode_i = ss_one_mode_dens[i_mode];
      const auto& sts_one_mode_dens_for_mode_i = sts_one_mode_dens[i_mode];

      if (grid_points.Size() != one_mode_dens_for_mode_i.Size())
      {
         MIDASERROR("Number of grid points do not match number of points in one-mode densities!");
      }

      // Setup file for each mode.
      std::string file_name = working_dir + "one_mode_density_mode_" + std::to_string(i_mode);
      std::ofstream ofs;
      ofs.open(file_name);
      ofs << std::scientific << std::setprecision(16);
      // Print header
      ofs << std::setw(width) << "Grid points";
      ofs << std::setw(width) << "One-mode density";
      ofs << std::endl;
      
      std::string ss_file_name = working_dir + "ss_one_mode_density_mode_" + std::to_string(i_mode);
      std::ofstream ss_ofs;
      ss_ofs.open(ss_file_name);
      ss_ofs << std::scientific << std::setprecision(16);
      // Print header
      ss_ofs << std::setw(width) << "Grid points";
      ss_ofs << std::setw(width) << "Space-smoothened one-mode density";
      ss_ofs << std::endl;
   
      std::string sts_file_name = working_dir + "sts_one_mode_density_mode_" + std::to_string(i_mode);
      std::ofstream sts_ofs;
      sts_ofs.open(sts_file_name);
      sts_ofs << std::scientific << std::setprecision(16);
      // Print header
      sts_ofs << std::setw(width) << "Grid points";
      sts_ofs << std::setw(width) << "Space-time-smoothened one-mode density";
      sts_ofs << std::endl;

      // Loop over grid points
      for (In q_idx = I_0; q_idx < grid_points.Size(); ++q_idx)
      {
         ofs     << std::setw(width) << grid_points[q_idx];
         ss_ofs  << std::setw(width) << grid_points[q_idx];
         sts_ofs << std::setw(width) << grid_points[q_idx];
         ofs     << std::setw(width) << one_mode_dens_for_mode_i[q_idx];
         ss_ofs  << std::setw(width) << ss_one_mode_dens_for_mode_i[q_idx];
         sts_ofs << std::setw(width) << sts_one_mode_dens_for_mode_i[q_idx];
         ofs     << std::endl;
         ss_ofs  << std::endl;
         sts_ofs << std::endl;
      }
   }
}

/************************************************************************//**
 * Write all currently saved one-mode densities to file.
 * 
 ***************************************************************************/
template
   < typename PARAM_T
   >
void OneModeDensities<PARAM_T>::WriteOneModeDensities() const
{
   for (In time_step = I_1; time_step < mStsOneModeDensities.size()+1; ++time_step)
   {
      WriteOneModeDensitiesForStep(time_step);
   }
   
}

} /* namespace midas::td */

#endif /* ONEMODEDENSITIES_IMPL_H_INCLUDED */
