/*
************************************************************************
*
* @file                 PrimitiveIntegrals.h
*
* Created:              23-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Primitive integrals for TD calculations.
*                       Allows for transforming to another primitive basis, 
*                       e.g. VSCF spectral basis.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef PRIMITIVEINTEGRALS_H_INCLUDED
#define PRIMITIVEINTEGRALS_H_INCLUDED

#include "td/PrimitiveIntegrals_Decl.h"
#include "td/PrimitiveIntegrals_Impl.h"

#endif /* PRIMITIVEINTEGRALS_H_INCLUDED */
