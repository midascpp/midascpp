/**
 *******************************************************************************
 * 
 * @file    DetailedExample.h
 * @date    08-07-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Here goes a brief description of the file. It ends at the next paragraph
 *    or doxygen keyword.
 *
 * After the brief description comes the detailed description (automatically
 * interpreted as details, when there are no keywords, and it's in a separate
 * paragraph). The purpose of this file is to provide some templates for
 * doxygen usage in Midas. E.g. you can copy and modify this header when
 * creating new files.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef EXAMPLE_H_INCLUDED
#define EXAMPLE_H_INCLUDED

/**
 * @brief   An example class for showing some `doxygen` formatting.
 *
 * The class has some examples of how to provide brief and detailed
 * descriptions of variables and functions. Detailed function descriptions are
 * put in the implementation file, so that the header file is kept relatively
 * clean. Likewise, you would not normally not have to put detailed
 * descriptions for variables - a brief one *should* suffice. Also, use the
 * grouping function to document multiple variables/functions that are similar,
 * instead of copying a lot of self-explanatory/redundant descriptions.
 *
 * Here is a short list summing up what `doxygen` features you will encounter
 * in the DetailedExampleClass files.
 *    - Making lists.
 *    - Code formatting using backticks, like `this`.
 *    - Detailed descriptions; a comment block like the current one.
 *    - Brief descriptions; a one-liner starting with `//!` or the keyword
 *      \@brief within a detailed comment block.
 *    - Autolinking is done automatically if `doxygen` recognizes the
 *      class/function/variable name. E.g. DetailedExampleClass,
 *      DetailedExampleClass::mVar1, Function1() const, Function1(const int&,
 *      double) const. For variables and functions you may have to be explicit
 *      about scope resolution and argument types to resolve overload
 *      ambiguity.  If you want to speak about several DetailedExampleClass%es,
 *      use the `%` to be able to write the class name correctly.
 *    - Grouping of variables, with and withoug a group name, e.g.
 *      DetailedExampleClass::mVarInGroupA_1 and
 *      DetailedExampleClass::mVarInGroupB_1.
 *    - Parameter and return value descriptions for functions, using the
 *      \@param and \@return keywords. Notes and warnings using the keywords
 *      \@note and \@warning.
 *    - Emphasized text; _italics_, *italics*, __bold__, **bold**.
 *    - Using formulas such as in the description of Function1() const.
 **/
class DetailedExampleClass
{
   private:
      //! Brief description of first variable.
      int mVar1;

      //! Brief description of second variable.
      int mVar2;

      //! Brief description can go here.
      /**
       * ... and then the details can go here.
       **/
      int mVarWithDetailedDescription1;

      /**
       * @brief Brief description can be marked by the \@brief keyword.
       *
       * Details follow in this paragraph, i.e. the one following the \@brief
       * keyword.
       **/
      int mVarWithDetailedDescription2;

      /**
       * @name VariableGroupA
       *
       * This group of variables has a name and a group description. Since
       * `DISTRIBUTE_GROUP_DOC   = YES` in `Doxyfile_example` the description
       * of the first member is copied to all other members.
       * Note that the grouping syntax is
       * @code 
       *    //!@{ ... //!@} 
       * @endcode 
       * for a _named group_ and 
       * @code 
       *    //@{ ... //@} 
       * @endcode 
       * for an _unnamed group_!
       **/
      //!@{
      //! Description that gets copied to all members of group.
      int mVarInGroupA_1;
      int mVarInGroupA_2;
      int mVarInGroupA_3;
      //!@}

      //@{
      //! Brief description for unnamed group (gets copied to all members).
      /**
       * Details (also gets copied).
       **/
      int mVarInGroupB_1;
      int mVarInGroupB_2;
      //! The common description is not copied, if you provide a new one.
      int mVarInGroupB_3;
      //@}


   public:
      //! A brief description for a function.
      void Function1() const;

      //! A brief description for another function.
      double Function1(const int&, double) const;

      /**
       * @name Getters
       * Sometimes a lot of simple functions behave very similar and have
       * obvious functionalities, reflected by their names. If so it's probably
       * neater to group them together.
       * This should of course only be done if they _are_ in fact
       * _self-explanatory_!
       **/
      //!@{
      int GetVar1() const;
      int GetVar2() const;
      int GetVar3() const;
      //!@}

      /**
       * @name Setters
       **/
      //!@{
      void SetVar1(int) const;
      void SetVar2(int) const;
      void SetVar3(int) const;
      //!@}
};


#endif/*EXAMPLE_H_INCLUDED*/
