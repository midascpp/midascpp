/**
 *******************************************************************************
 * 
 * @file    DetailedExample.cc
 * @date    08-07-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    This is the implementation file corresponding to DetailedExample.h.
 *
 * In this file we have detailed descriptions for the functions. You'll see how
 * to use keywords for parameters, return values, and also how to typeset
 * mathematical formulas.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "DetailedExample.h"

/**
 * This is boring function that does nothing, but we'll use the opportunity to
 * display some math. There are two possibilities for rendering formulas in
 * `doxygen`;
 *    - The default is to use TeX in some way, but for some reason the
 *      resulting formulas images are of poor/coarse-grained resolution
 *      (probably due to efficiency concerns). Anyway, this works out of the
 *      box, i.e.  `doxygen` does this automatically.
 *    - Otherwise, you can use [MathJax](https://www.mathjax.org), 
 *      '_A JavaScript display engine for mathematics that works in all
 *      browsers._' 
 *      This is done by setting `USE_MATHJAX = YES` in the Doxyfile and
 *      providing a valid path to the MathJax script in the `MATHJAX_RELPATH`
 *      variable. As of this writing, MathJax is enabled, and the MathJax path
 *      is the online, publicly available script.
 *      In this way, `doxygen` just leaves the formulas _as they are_, and then
 *      leaves it for the browser to convert it to typeset formulas when
 *      opening the `.html` documents. But note that when using the online
 *      script, it requires an internet connection. If you don't have that,
 *      either set `USE_MATHJAX = NO` to get the TeX generated formulas, or
 *      download MathJax for yourself and link to it in the Doxyfile.
 *
 * Anyway, here are some different ways of typesetting formulas.
 *    - You can do it inline. Five of the most significant numbers are \f$ e,
 *      i, \pi, 1, 0 \in \mathbb{C} \f$.
 *    - You can make a one-liner. Here is a formula involving all of them,
 *      \f[
 *          e^{i\pi} + 1 = 0.
 *      \f]
 *    - You can use TeX environments, e.g. 'align'. Let there be light;
 *      \f{align*}{
 *          \nabla \cdot \mathbf{E}  &= \frac{\rho}{\varepsilon_0}               \\
 *          \nabla \cdot \mathbf{B}  &= 0                                        \\
 *          \nabla \times \mathbf{E} &= -\frac{\partial \mathbf{B}}{\partial t}  \\
 *          \nabla \times \mathbf{B}
 *             &=   \mu_0 \left( \mathbf{J} 
 *                + \varepsilon_0\frac{\partial \mathbf{E}}{\partial t} \right)
 *      \f}
 **/
void DetailedExampleClass::Function1() const
{
}

/**
 * This function adds the first argument to a copy of the second and then
 * returns the resulting value.
 *
 * @param a
 *    This is an integer that will be passed by reference. It is constant since
 *    we will only use it to add to the other, but the integer itself is not
 *    modified.
 * @param b
 *    Here we pass by value, so the function works on a newly created copy of
 *    b, not b itself.
 * @return
 *    The sum of the two arguments.
 **/
double DetailedExampleClass::Function1(const int& a, double b) const
{
   b += a;
   return b;
}

/**
 * Since this function (GetVar1() const) belongs to a group (not seen here in
 * the implementation file, but seen in the header), it's documentation get's
 * copied to the other group members. So in real life we would probably write
 * something like '_This functions returns the variable corresponding to the
 * function name._' here.
 * 
 * @return
 *    The value of the variable, as indicated by the function name.
 **/
int DetailedExampleClass::GetVar1() const
{
}

int DetailedExampleClass::GetVar2() const
{
}

int DetailedExampleClass::GetVar3() const
{
}

/**
 * This functions sets the variable corresponding to the function name.
 *
 * @param i
 *    The value that will be assigned to the variable.
 **/
void DetailedExampleClass::SetVar1(int i) const
{
}

void DetailedExampleClass::SetVar2(int i) const
{
}

void DetailedExampleClass::SetVar3(int i) const
{
}

