/**
 *******************************************************************************
 * 
 * @file    Example.cc
 * @date    07-09-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Brief file description. Copy-paste-modify this block to your own files.
 *
 * Detailed file description.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "Example.h"

/**
 * Detailed function description.
 **/
void ExampleClass::Func() const
{
   // Code...
}

/**
 * Detailed function description.
 *
 * @return
 *    Description of returned object.
 **/
int ExampleClass::FuncWithReturn() const
{
   // Code...
   return 0;
}

/**
 * Detailed function description.
 *
 * @param aInt
 *    Description of argument.
 * @param arDbl
 *    Description of argument.
 * @return
 *    Description of returned object.
 **/
int ExampleClass::FuncWithReturnAndArgs const(int aInt, double& arDbl)
{
   // Code...
   return 0;
}

/**
 * Detailed function description (is copied to the other members of the group).
 **/
void ExampleClass::Func1InGroupA() const
{                       
}                       
                        
void ExampleClass::Func2InGroupA() const
{                       
}                       
                        
void ExampleClass::Func3InGroupA() const
{
}
