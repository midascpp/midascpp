/**
 *******************************************************************************
 * 
 * @file    Example.h
 * @date    07-09-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Brief file description. Copy-paste-modify this block to your own files.
 *
 * Detailed file description.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef EXAMPLE_H_INCLUDED
#define EXAMPLE_H_INCLUDED

#include<vector>

/**
 * @brief
 *    Brief class description.
 *
 * Detailed class description.
 **/
class ExampleClass
{
   private:
      //! Brief variable description (one-liner).
      int mVar;

      //! Brief variable description (one-liner).
      /**
       * Details (if really necessary).
       **/
      int mVarWithDetailedDescription;

      /**
       * @name VariableGroupA
       * 
       * Group description (shown in overview of class members).
       **/
      //!@{
      int mVar1InGroupA;
      int mVar2InGroupA;
      int mVar3InGroupA;
      //!@}

      //@{
      //! Brief description (copied to all members).
      int mVar1InGroupB;
      int mVar2InGroupB;
      int mVar3InGroupB;
      //@}


   public:
      //! Brief function description (one-liner).
      void Func() const;

      //! Brief function description (one-liner).
      int FuncWithReturn() const;

      //! Brief function description (one-liner).
      int FuncWithReturnAndArgs const(int aInt, double& arDbl);

      /**
       * @name FunctionGroupA
       *
       * Group description (shown in overview of class members).
       **/
      //!@{
      void Func1InGroupA() const;
      void Func2InGroupA() const;
      void Func3InGroupA() const;
      //!@}
};


#endif/*EXAMPLE_H_INCLUDED*/
