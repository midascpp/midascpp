/**
************************************************************************
*
*  @file                ModelPot.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Base class for the potentials
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#ifndef MODELPOT_H_INCLUDED
#define MODELPOT_H_INCLUDED

#include <vector>
#include <map>
#include <string>

#include "util/Io.h"
#include "util/AbstractFactory.h"
#include "mmv/MidasVector.h"

namespace midas
{
namespace potentials
{

struct provides
{
   enum : int
      {  nothing            = 0
      ,  unknown            = 0
      ,  value              = (1<<0)
      ,  derivatives        = (1<<1)
      ,  first_derivatives  = (1<<1)
      ,  hessian            = (1<<2)
      ,  second_derivatives = (1<<2)
      ,  error              = (1<<31)
      };

   provides(int a_value)
      : m_value(a_value)
   {
   }

   operator bool()
   {
      return bool(m_value);
   }

   int m_value;
};

inline provides operator |(const provides& lhs, const provides& rhs)
{
   return provides{lhs.m_value | rhs.m_value};
}

inline provides operator &(const provides& lhs, const provides& rhs)
{
   return provides{lhs.m_value & rhs.m_value};
}

inline provides operator |(int lhs, const provides& rhs)
{
   return provides{lhs | rhs.m_value};
}

inline provides operator &(int lhs, const provides& rhs)
{
   return provides{lhs & rhs.m_value};
}

inline provides operator |(const provides& lhs, int rhs)
{
   return provides{lhs.m_value | rhs};
}

inline provides operator &(const provides& lhs, int rhs)
{
   return provides{lhs.m_value & rhs};
}

} /* namespace potentials */
} /* namespace midas */

using namespace midas;

class Nuclei;
class ModelPot;

using ModelPotInfo = std::map<std::string, std::string>;

/**
 * Factory stuff
 *
 * Define an AbstractFactory, and a AbstractFactoryRegistration for the ModelPot dynamic heirarchy.
 * See description of ModelPot below for usage of factory types for registering new types in the heirarchy.
 **/
using ModelPotFactory = AbstractFactory<ModelPot*(const ModelPotInfo&), std::string>;

template<class A>
using ModelPotRegistration = AbstractFactoryRegistration<ModelPot*(const ModelPotInfo&), A, std::string>;

/**
 * Base class for all model potentials.
 * Concrete model potentials should all overload the following potential evaluation functions:
 *
 *    1) Evaluation functions:
 *       private: Nb EvalPotImpl(const std::vector<Nuclei>&) const
 *     
 *    2) First derivatives function:
 *       private: MidasVector EvalDerImpl(const std::vector<Nuclei>&) const
 *
 *    3) Second derivatives function:
 *       private: MidasMatrix EvalHessImpl(const std::vector<Nuclei>&) const 
 *
 * In the .cc file for each model potential they should register for the ModelPot Factory 
 * through the ModelPotRegistration. Fx. for registering FilePot, we put in FilePot.cc:
 *    
 *    ModelPotRegistration<FilePot> registerFilePot("FILEPOT");
 *
 * This line will add FilePot to the ModelPot Factory, and make it constructible with the key "FILEPOT".
 **/
class ModelPot
{
   public:
      using properties_type = std::vector<std::string>;
      using provides_type   = std::vector<potentials::provides>;
   
   private:
      properties_type mProperties;
      provides_type   mProvides;

      //! Overloadable function to evaluate the potential.
      virtual Nb EvalPotImpl(const std::string& name, const std::vector<Nuclei>&) const = 0;

      //! Overloadable function to evaluate the first derivative of the potential.
      virtual MidasVector EvalDerImpl(const std::string& name, const std::vector<Nuclei>&) const
      {
         MIDASERROR("EvalDer not implemented for model potential of type : " + this->Type() + ".");
         return MidasVector{};
      }

      //! Overloadable function to evaluate the second derivate of the potential.
      virtual MidasMatrix EvalHessImpl(const std::string& name, const std::vector<Nuclei>&) const 
      {
         MIDASERROR("EvalHess not implemented for model potential of type : " + this->Type() + ".");
         return MidasMatrix{};
      }

      //! Assert property name
      void AssertPropertyName(const std::string& aProperty) const
      {
         for(int i = 0; i < mProperties.size(); ++i)
         {
            if(aProperty == mProperties[i])
            {
               return;
            }
         }

         MIDASERROR("No property named : '" + aProperty + "' in ModelPot of type '" + this->Type() + "'.");
      }

      //! Sanity check of internal variables
      bool Check() const
      {
         return mProperties.size() == mProvides.size();
      }

   protected:
      //properties_type mProperties; // TMP TEST
      //! Default constructor. Protected to only be callable from dervied classes.
      ModelPot
         (  const properties_type& aProperties = properties_type{"GROUND_STATE_ENERGY"}
         ,  const provides_type&   aProvides   = provides_type{potentials::provides::value}
         )
         :  mProperties(aProperties)
         ,  mProvides  (aProvides)
      {
      }

      //! Add a property name to ModelPot
      void AddPropertyName(const std::string& aName)
      {
         mProperties.emplace_back(aName);
      }

      //! Add a provides tag to ModelPot
      void AddProvides(potentials::provides aProvides)
      {
         mProvides.emplace_back(aProvides);
      }

   public:
      //! Pure virtual destructor, to make class abstract.
      virtual ~ModelPot() = 0; 
      
      //! Factory
      static std::unique_ptr<ModelPot> Factory(const ModelPotInfo&);
      
      //! Get the type of potential
      virtual std::string Type() const = 0;

      //!
      potentials::provides Provides(const std::string& aName) const
      {
         for(int i = 0; i < mProperties.size(); ++i)
         {
            if(aName == mProperties[i])
            {
               return mProvides[i];
            }
         }

         return potentials::provides::error;
      }
      
      In GetPropertyNr(const std::string& aName) const
      {
         auto it = std::find(mProperties.begin(), mProperties.end(),aName);
         if(it != mProperties.end())
         {
            return std::distance(mProperties.begin(), it);
         }
         MIDASERROR("Could not find property "+aName+" for MODEL_SP potential");
         return -1;
      }

      //! Evaluate the potential
      Nb EvalPot(const std::string& name, const std::vector<Nuclei>& molecule) const 
      { 
         AssertPropertyName(name);
         return EvalPotImpl(name, molecule); 
      }

      //! Evaluate the cartesian first derivative of the potential
      MidasVector EvalDer(const std::string& name, const std::vector<Nuclei>& molecule) const 
      {
         AssertPropertyName(name);
         return EvalDerImpl(name, molecule);
      }
      
      //! Evaluate the cartesian second derivative of the potential
      MidasMatrix EvalHess(const std::string& name, const std::vector<Nuclei>& molecule) const 
      {
         AssertPropertyName(name);
         return EvalHessImpl(name, molecule);
      }
};

//! Implementation of destructor
inline ModelPot::~ModelPot() {}

#endif /* MODELPOT_H_INCLUDED */
