/**
************************************************************************
*
*  @file                FilePot.h
* 
*  Created:             25/04/2008
* 
*  Author:              Eduard Matito (eduard@chem.au.dk)
* 
*  Short Description:   Declares class Mop for computing the potential
*                       from the mop file
* 
*  Last modified: Tue Jun 08, 2010  04:00PM
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef FILEPOT_H_INCLUDED
#define FILEPOT_H_INCLUDED

// 
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "potentials/ModelPot.h"
#include "pes/molecule/MoleculeInfo.h"
#include "potentials/MidasPotential.h"

using namespace midas;

/**
 * Model potential from potential file (.mop) 
 * and molecular reference structure (.mol).
 **/
class FilePot 
   : public ModelPot
{
   private:
      //! Reference structure and coordinates.
      molecule::MoleculeInfo mMolecule;
      //! Actual operator.
      std::vector<MidasPotential> mOperators;
      
      //! Calculate cartesian xyz displacement
      MidasVector CalculateQDisplacements(const std::vector<Nuclei>& arGeom) const;

      //! Overload of evaluate function.
      virtual Nb EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& arGeom) const;
      
      //! Overload of evaluate function.
      virtual MidasVector EvalDerImpl(const std::string& aName, const std::vector<Nuclei>& arGeom) const;
      
      //! Overload of evaluate function.
      virtual MidasMatrix EvalHessImpl(const std::string& aName, const std::vector<Nuclei>& arGeom) const;

   public:
      //! Constructor from ModelPotInfo.
      FilePot(const ModelPotInfo& aInfo);

      //! Destructor (defaulted).
      ~FilePot() = default;
      
      //! Get the type of potential
      virtual std::string Type() const { return "FILEPOT"; }
};

#endif /* FILEPOT_H_INCLUDED */
