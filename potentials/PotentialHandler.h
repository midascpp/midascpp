#ifndef POTENTIALHANDLER_H_INCLUDED
#define POTENTIALHANDLER_H_INCLUDED

#include <string>

#include "potentials/ModelPot.h"

namespace midas
{
namespace potentials
{

/**
 * Class for handling of model potentials.
 **/
class PotentialHandler
{
   public: 
      //!@{
      //! Define some types.
      using potential_key_type = int;
      using potential_type     = std::unique_ptr<ModelPot>;
      using potential_map_type = std::map<potential_key_type, potential_type>;
      //!@}
   
   private:
      //! Map of loaded potentials.
      potential_map_type mMap;
      //!

      //! Generate a unique ID.
      static int GenerateUid()
      {
         static int nextId = 0;
         return nextId++;
      }

   public:
      //! Default constructor.
      PotentialHandler() = default;

      //! Default move c-tor.
      PotentialHandler(PotentialHandler&&) = default;
      
      //! Default move assignment.
      PotentialHandler& operator=(PotentialHandler&&) = default;

      //! Load potential into handler.
      potential_key_type LoadPotential
         (  const ModelPotInfo& aInfo
         ,  const potential_key_type& aKey = GenerateUid()
         );

      //! Get a previously loaded potential.
      const potential_type& GetPotential
         (  const potential_key_type& aKey
         )  const;
};

} /* namespace potentials */
} /* namespace midas */

#endif /* POTENTIALHANDLER_H_INCLUDED */
