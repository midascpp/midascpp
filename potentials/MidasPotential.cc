/**
************************************************************************
*
*  @file                MidasPotential.cc
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a midas potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/
#include <map>
#include <vector>
#include <string>
#include <fstream>

#include "mmv/MidasVector.h"
#include "potentials/MidasPotential.h"
#include "pes/PesInfo.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"


/**
 * Initializes the function, term and coefficient containers
 * from the file mFileName.
 **/
void MidasPotential::InitializeContainers(const std::vector<std::string>& aModeList)
{
   //Drop any old information
   mFunctionMap.clear();
   mOperatorTerms.clear();
   mFunctions.clear();
   mDerFunctions.clear();
   mDDerFunctions.clear();
   mGenFunctions.clear();
   mGenDerFunctions.clear();
   mGenDDerFunctions.clear();
   
   // Read new operator
   MidasOperatorReader oper_data(mFileName);
   oper_data.ReadOperator(mFileName);
   
   // Get function containers
   FunctionContainer<Nb> function_container;
   oper_data.GetFunctionContainer(function_container);
   FunctionContainer<taylor<Nb, 1, 1>> der_function_container;
   oper_data.GetFunctionContainer(der_function_container);
   FunctionContainer<taylor<Nb, 2, 2>> dder_function_container;
   oper_data.GetFunctionContainer(dder_function_container);
   
   // Get scaling factors
   
   oper_data.GetScalingVector(mScalings);
   
   MidasVector correct_order_scaling(mScalings);
   //Initialize the mOperatorTerms container
   const auto& modenames = oper_data.GetModeNames();
   for(int i = 0; i < aModeList.size(); ++i)
   {
      auto tmpit = std::find(modenames.begin(), modenames.end(), aModeList[i]);
      if(tmpit == modenames.end())
      {
         MIDASERROR("Modename '" + aModeList[i] + "' not found.");
      }
      else 
      {
         int dist = int(std::distance(modenames.begin(), tmpit));
         correct_order_scaling[i] = mScalings[dist];
      }
   }
   
   mScalings = correct_order_scaling;
   
   // Save the number of modes in the operator
   mNumModes  = modenames.size();
   
   for(  auto iter = oper_data.GetFrontOfOperators()
      ;  iter != oper_data.GetEndOfOperators()
      ;  ++iter
      )
   {
      std::vector<In> mc = GetMCVectorFromTerm(iter->first.first, aModeList);
      std::vector<In> functions = FindAndAddFunctionsToMap(iter->first.second, function_container, der_function_container, dder_function_container);
      
      //Ensure correct order
      std::vector<std::pair<In, In>> tmp;
      for(int i = 0; i < mc.size(); ++i)
      {
         tmp.emplace_back(std::make_pair(mc[i], functions[i]));
      }
      std::sort(tmp.begin(), tmp.end(), [&](const auto& a, const auto& b){return a.first < b.first;});
      mc.clear();
      functions.clear();
      for(auto& it :tmp)
      {
         mc.emplace_back(it.first);
         functions.emplace_back(it.second);
      }
      
      auto inserted = mOperatorTerms.insert(std::make_pair(mc, std::vector<std::pair<std::vector<In>, Nb> >()));
      //Assign the function vector and its coefficient
      inserted.first->second.push_back(std::make_pair(functions, iter->second));
   }
}

/**
 * Reads in one operator term, and tries to find the functions in the class containers.
 * If a function is not found, it is added to the containers.
 *
 * @param arFunctions    A vector containing all of the operator terms
 * @param arFuncCont     A function container needed to create the Nb functions
 * @param arDFuncCont    A function container needed to create the taylor<Nb, 1, 1> functions
 * @param arDDFuncCont   A function container needed to create the taylor<Nb, 2, 2>  functions
 *
 * @return   Returns a std::vector<In> containing the indecies of the functions used in the term.
 **/
std::vector<In> MidasPotential::FindAndAddFunctionsToMap(
   const std::vector<std::string>& arFunctions,
   const FunctionContainer<Nb>& arFuncCont,
   const FunctionContainer<taylor<Nb, 1, 1>>& arDFuncCont,
   const FunctionContainer<taylor<Nb, 2, 2>>& arDDFuncCont)
{
   std::vector<In> function_vector;
   for(auto& func : arFunctions)
   {
      In size_map = mFunctionMap.size();
      auto inserted = mFunctionMap.insert(std::make_pair(func, size_map));
      if(inserted.second)
      {
         //If we insert use the size_map for the position
         function_vector.push_back(size_map);
         //And add the function to the containers
         mFunctions.emplace_back(func, arFuncCont);
         mDerFunctions.emplace_back(func, arDFuncCont);
         mDDerFunctions.emplace_back(func, arDDFuncCont);
         mGenFunctions.emplace_back(func, arFuncCont);
         mGenDerFunctions.emplace_back(func, arDFuncCont);
         mGenDDerFunctions.emplace_back(func, arDDFuncCont);
      }
      else
      {
         //If we didn't insert, simply use the second value as position
         function_vector.push_back(inserted.first->second);
      }
   }
   return function_vector;
}

/**
 * Generate the MC vector for a given operator term
 *
 * @param arModeNames    A list of all the mode names in the specific operator term
 * @param arAllModeNames A list of all the mode names in the operator file
 *
 * @return   Returns a std::vector<In> containing the indecies of the modes in the operator term.
 **/
std::vector<In> MidasPotential::GetMCVectorFromTerm(const std::vector<std::string>& arModeNames, const std::vector<std::string>& arAllModeNames)
{
   std::vector<In> mode_coupling_vector;
   for(auto& variable : arModeNames)
   {
      auto tmpit = std::find(arAllModeNames.begin(), arAllModeNames.end(), variable);
      if(tmpit == arAllModeNames.end())
      {
         MIDASERROR("Modename '" + variable + "' not found.");
      }
      else 
      {
         mode_coupling_vector.push_back(std::distance(arAllModeNames.begin(), tmpit));
      }
   }
   return mode_coupling_vector;
}


/**
 * Evaluates the potential for a set of points in the given MC
 *
 * @param arMode     The MC vector
 * @param arGrid     The set of points on which to evaluate the potential
 * @param arPotVals  The resulting potential values to be returned
 *
 **/
void MidasPotential::EvaluatePotential
   (  const std::vector<In>& arMode
   ,  const std::vector<MidasVector>& arGrid
   ,  MidasVector& arPotVals
   )  const
{
   arPotVals.SetNewSize(arGrid.size());
   for(Uin i = I_0; i < arGrid.size(); ++i)
   {
      arPotVals[i] = EvaluatePotential(arMode, arGrid[i]);
   }
}

/**
 * Evaluates the potential for a given MC at a given point
 *
 * @param arSetOfModes  The MC vector
 * @param arSetOfCoord  The point at which to evaluate the potential
 *
 * @return   Returns the potential value at the point (Nb)
 **/
Nb MidasPotential::EvaluatePotential(const std::vector<In>& arSetOfModes,
                     const MidasVector& arSetOfCoord) const
{
   Nb result = 0.0;
   const auto term_to_eval = mOperatorTerms.find(arSetOfModes);
   if(term_to_eval != mOperatorTerms.end())
   {
      for(In i = 0; i < term_to_eval->second.size(); ++i)
      {
         Nb tmp = term_to_eval->second[i].second;
         for(In j = 0; j < term_to_eval->second[i].first.size(); ++j)
         {
            tmp *= mFunctions[term_to_eval->second[i].first[j]].EvaluateFuncForVar(arSetOfCoord[j]*mScalings[arSetOfModes[j]]);
         }
         result += tmp;
      }
   }
   return result;
}

/**
 * Get the operator term coefficients for a given MC, part of pes/ItGridBoxes.cc interface.
 *
 * @param arModeCombi  The MC vector
 *
 * @return   Returns the coefficients for the MC as a std::vector<Nb>
 **/
std::vector<Nb> MidasPotential::CoefficientsForModeCombi
   (  const std::vector<In>& arModeCombi
   )  const
{
   std::vector<Nb> result;
   const auto terms = mOperatorTerms.find(arModeCombi);
   if(terms != mOperatorTerms.end())
   {
      for(auto term : terms->second)
      {
         result.push_back(term.second);
      }
   }
   return result;
}

/**
 * Get the operator functions for a given ModeCombi, part of pes/ItGridBoxes.cc interface.
 *
 * @param arModeCombi  The MC vector
 *
 * @return  Returns the a vector with functions to evalute each of the operator terms as elements
 **/
std::vector<std::vector<MidasFunctionWrapper<Nb>>> MidasPotential::FunctionsForModeCombi
   (  const std::vector<In>& arModeCombi
   ) const
{
   std::vector<std::vector<MidasFunctionWrapper<Nb>>> result;
   const auto terms = mOperatorTerms.find(arModeCombi);
   if(terms != mOperatorTerms.end())
   {
      for(auto term : terms->second)
      {
         std::vector<MidasFunctionWrapper<Nb>> tmp;
         for(auto func : term.first)
         {
            tmp.emplace_back(mFunctions[func]);
         }
         result.emplace_back(tmp);
      }
   }
   return result;
}

/**
 * Get the scaling factors for a given ModeCombi, part of pes/ItGridBoxes.cc interface.
 *
 * @param arModeCombi  The MC vector
 *
 * @return  Returns the a vector with the scaling factors for the given MC
 **/
std::vector<Nb> MidasPotential::ScalingFactorsForModeCombi
   (  const std::vector<In>& arModeCombi
   ) const
{
   std::vector<Nb> result;
   if(mScalings.size() != 0)
   {
      result.reserve(arModeCombi.size());
      for(const auto& m: arModeCombi)
      {
         if (m >= mScalings.Size())
         {
            MIDASERROR( "Out-of-range: m = "+std::to_string(m)+
                        ", mScalingFactors.Size() = "+std::to_string(mScalings.Size())+
                        ".");
         }
         result.push_back(mScalings[m]);
      }
   }
   return result;
}

/**
 * Calculate the potential energy at a given point
 *
 * @param aDisplacement   Displacement in the coordinated used to construct the potential.
 *
 * @return  The potential value at the given point
 **/
Nb MidasPotential::EvaluatePotential(MidasVector aDisplacement) const
{
   // If we have scaling factors apply them before evaluation
   if(mScalings.size() != 0)
   {
      for(In i = 0; i < aDisplacement.size(); ++i)
      {
         aDisplacement[i] *= mScalings[i];
      }
   }

   // Calculate value of potential
   Nb value = C_0;
   // Loop over all the mode combinations in the potential
   //for(int j = 0; j < 3; ++j)
   //{
   for( auto mc_iter : mOperatorTerms )
   {
      //if(mc_iter.first.size() != j) continue;
      // Loop over each SoP term in the MC
      for( auto term_iter : mc_iter.second)
      {
         Nb product = term_iter.second;
         // Loop over each part of the SoP and evaluate the function
         for( int i = 0; i < term_iter.first.size(); ++i )
         {
            product *= mGenFunctions[term_iter.first[i]].EvaluateFunctionVec(
               std::vector<Nb>{aDisplacement[mc_iter.first[i]]});
         }
         //product *= term_iter.second;
         value += product;
      }
   }
   //}
   
   return value;
}

/**
 * Calculate the gradient of the potential energy at a given point
 *
 * @param aDisplacement   Displacement in the coordinated used to construct the potential.
 *
 * @return  The gradient of the potential at the given point
 **/
MidasVector MidasPotential::EvaluateGradient(MidasVector aDisplacement) const
{
   // If we have scaling factors apply them before evaluation
   if(mScalings.size() != 0)
   {
      for(In i = 0; i < mNumModes; ++i)
      {
         aDisplacement[i] *= mScalings[i];
      }
   }

   // Calculate value of potential
   MidasVector gradient(mNumModes, C_0);
   taylor<Nb, 1, 1> eps(0, 0);
   
   // Loop over all the modes in the operator
   for( In i = 0; i < mNumModes; ++i)
   {
      // Loop over all the mode combinations in the potential
      for( auto mc_iter : mOperatorTerms )
      {
         // If the mode is not in the operator don't do anything for this MC
         if( std::find(mc_iter.first.begin(), mc_iter.first.end(), i) == mc_iter.first.end())
         {
            continue;
         }
         // Loop over each SoP term in the MC
         for( auto term_iter : mc_iter.second)
         {
            taylor<Nb, 1, 1> product(term_iter.second);
            // Loop over each part of the SoP and evaluate the function
            for( In j = 0; j < mc_iter.first.size(); ++j )
            {
               if(mc_iter.first[j] == i)
               {
                  product *= mGenDerFunctions[term_iter.first[j]].EvaluateFunctionVec(
                     std::vector<taylor<Nb, 1, 1>>{aDisplacement[mc_iter.first[j]]+eps});
               }
               else
               {
                  product *= mGenFunctions[term_iter.first[j]].EvaluateFunctionVec(
                     std::vector<Nb>{aDisplacement[mc_iter.first[j]]});
               }
            }
            gradient[i] += product[I_1];
         }
      }
      
   }
   
   // Make the gradient have same scaling as the Q coordinates given to us
   if(mScalings.size() != 0)
   {
      for(In i = 0; i < mNumModes; ++i)
      {
         gradient[i] *= mScalings[i];
      }
   }
   
   return gradient;
}

/**
 * Calculate the hessian of the potential energy at a given point
 *
 * @param aDisplacement   Displacement in the coordinated used to construct the potential.
 *
 * @return  The hessian of the potential at the given point
 **/
MidasMatrix MidasPotential::EvaluateHessian(MidasVector aDisplacement) const
{
   // If we have scaling factors apply them before evaluation
   if(mScalings.size() != 0)
   {
      for(In i = 0; i < mNumModes; ++i)
      {
         aDisplacement[i] *= mScalings[i];
      }
   }
   MidasMatrix hessian(mNumModes, C_0);
   taylor<Nb, 2, 2> eps_2_1(0, 0);
   taylor<Nb, 2, 2> eps_2_2(0, 1);
   
   for( In i = 0; i < mNumModes; ++i)
   {
      for( auto mc_iter : mOperatorTerms )
      {
         // If the mode is not in the operator don't do anything for this MC
         if( std::find(mc_iter.first.begin(), mc_iter.first.end(), i) == mc_iter.first.end())
         {
            continue;
         }
            
         // Loop over each SoP term in the MC
         for( auto term_iter : mc_iter.second)
         {
            // Product_1/2 keeps track of the the (df(x)/dx)g(y) and f(x)(dg(y)/dy) part respectively
            taylor<Nb, 2, 2> product(term_iter.second);
            // Loop over each part of the SoP and evaluate the function
            for( In k = 0; k < mc_iter.first.size(); ++k )
            {
               if(mc_iter.first[k] == i)
               {
                  product *= mGenDDerFunctions[term_iter.first[k]].EvaluateFunctionVec(
                     std::vector<taylor<Nb, 2, 2>>{aDisplacement[mc_iter.first[k]]+eps_2_1});
               }
               else
               {
                  product *= mGenFunctions[term_iter.first[k]].EvaluateFunctionVec(
                     std::vector<Nb>{aDisplacement[mc_iter.first[k]]});
               }
            }
            // Since libtaylor is actually calculating the Taylor expansion 
            // we need to multiply this factor by two to get the second derivative
            hessian[i][i] += product[I_3]*C_2;
         }
      }
   }
   
   for( In i = 0; i < mNumModes; ++i)
   {
      for( In j = i + 1; j < mNumModes; ++j )
      {
         for( auto mc_iter : mOperatorTerms )
         {
            if(mc_iter.first.size() < I_2)
            {
               continue;
            }
            
            // If the first mode is not in the operator don't do anything for this MC
            if( std::find(mc_iter.first.begin(), mc_iter.first.end(), i) == mc_iter.first.end())
            {
               continue;
            }
            
            // If the second mode is not in the operator don't do anything for this MC
            if( std::find(mc_iter.first.begin(), mc_iter.first.end(), j) == mc_iter.first.end())
            {
               continue;
            }
            
            // Loop over each SoP term in the MC
            for( auto term_iter : mc_iter.second)
            {
               // Product_1/2 keeps track of the the (df(x)/dx)g(y) and f(x)(dg(y)/dy) part respectively
               taylor<Nb, 2, 2> product(term_iter.second);
               // Loop over each part of the SoP and evaluate the function
               for( In k = 0; k < mc_iter.first.size(); ++k )
               {
                  if(mc_iter.first[k] == i)
                  {
                     product *= mGenDDerFunctions[term_iter.first[k]].EvaluateFunctionVec(
                        std::vector<taylor<Nb, 2, 2>>{aDisplacement[mc_iter.first[k]]+eps_2_1});
                  }
                  else if(mc_iter.first[k] == j)
                  {
                     product *= mGenDDerFunctions[term_iter.first[k]].EvaluateFunctionVec(
                        std::vector<taylor<Nb, 2, 2>>{aDisplacement[mc_iter.first[k]]+eps_2_2});
                  }
                  else
                  {
                     product *= mGenFunctions[term_iter.first[k]].EvaluateFunctionVec(
                        std::vector<Nb>{aDisplacement[mc_iter.first[k]]});
                  }
               }
               hessian[i][j] += product[I_4];
               hessian[j][i] += product[I_4];
            }
         }
      }
   }
   
   // Make the hessian have same scaling as the Q coordinates given to us
   if(mScalings.size() != 0)
   {
      for(In i = 0; i < mNumModes; ++i)
      {
         for(In j = i + 1; j < mNumModes; ++j)
         {
            hessian[i][j] *= mScalings[j]*mScalings[i];
            hessian[j][i] *= mScalings[j]*mScalings[i];
         }
         hessian[i][i] *= mScalings[i]*mScalings[i];
      }
   }   
   
   return hessian;
}
