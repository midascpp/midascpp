/**
*************************************************************************
*
* @file                MorsePot.cc
*
* Created:             30/10/2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Definitions of class members for MorsePot
*
* Last modified:       30/17/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <string>
#include <algorithm>

// midas headers
#include "potentials/MorsePot.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "mmv/MidasVector.h"
#include "nuclei/Nuclei.h"

////
// Register for factory
////
ModelPotRegistration<MorsePot> registerMorsePot("MORSEPOT");

/**
 * @brief
 **/
MorsePot::MorsePot(const ModelPotInfo& aInfo) : ModelPot({}, {})
{
   auto iter = aInfo.find("MODELPOTDEFS");
   if(iter != aInfo.end() && iter->second != "")
   {
      SetFromString(iter->second);
   }
   else
   {
      //If no #2 MorseDefs are given consider the standard H2+ molecule
      //with De=0.1026 au, Re=1.9972 au, a=0.732 au
      mDes.emplace_back(0.1026);
      mAs.emplace_back(0.732);
      mReqs.emplace_back(1.9972);
      mMorsePotDefs.emplace_back("Default H2+ molecule");
      AddPropertyName("GROUND_STATE_ENERGY");
      AddProvides(potentials::provides::value | potentials::provides::first_derivatives | potentials::provides::second_derivatives);
   }
}

namespace detail{
   Nb FindAndMakeNbOfTermMorse(std::vector<std::vector<std::string>>& arTerms, const std::string& arName)
   {
      auto it = std::find_if(arTerms.begin(), arTerms.end(), [arName](auto& term){return term.front() == arName;});
      if(it != arTerms.end())
      {
         return midas::util::FromString<Nb>(it->back());
      }
      else
      {
         MIDASERROR("Could not find \""+arName+"\" in MorsePot Definition");
      }
      return C_0; // Never happens
   }
} // namespace detail

/**
 * @brief Set the private members from a string
 * @param aAux the input from #2 MorseDefs
 **/
void MorsePot::SetFromString(const std::string& aAux)
{
   //Seperate the input into a vector on each newline
   std::vector<std::string> definitions = midas::util::SeperateLinesIntoVector(aAux);
   for(auto& defin : definitions)
   {
      // Transform to lower case
      std::for_each(defin.begin(),defin.end(),[](char & c){c = std::tolower(c);});
      //Remove all spaces
      defin.erase(std::remove_if(defin.begin(), defin.end(), ::isspace), defin.end());
      //Save the definition
      mMorsePotDefs.emplace_back(defin);
      //Split the string with each comma
      auto terms_tmp = midas::util::StringVectorFromString(defin, ',');
      
      //Further split the resulting strings into vectors with each = sign
      //If the length of the resulting vector is not two we discard it
      std::vector<std::vector<std::string>> terms;
      for(const auto& term : terms_tmp)
      {
         terms.emplace_back(midas::util::StringVectorFromString(term, '='));
         if(terms.back().size() != 2)
         {
            terms.pop_back();
         }
      }
      
      // Find electronic dissociation energy
      mDes.emplace_back(detail::FindAndMakeNbOfTermMorse(terms, "de"));
      if (gDebug) Mout << " D_e found to be: "  << mDes.back() << endl;
      
      // Find a coefficient
      mAs.emplace_back(detail::FindAndMakeNbOfTermMorse(terms, "acoeff"));
      if (gDebug) Mout << " a found to be: "  << mAs.back() << endl;
      
      // Find equilibrium distance
      mReqs.emplace_back(detail::FindAndMakeNbOfTermMorse(terms, "req"));
      if (gDebug) Mout << " R_eq found to be: "  << mReqs.back() << endl;
      
      // Find property name
      auto it = std::find_if(terms.begin(), terms.end(), [](auto& term){return term.front() == "name";});
      if(it != terms.end())
      {
         std::for_each(it->back().begin(),it->back().end(),[](char & c){c = std::toupper(c);});
         AddPropertyName(it->back());
         AddProvides(potentials::provides::value);
         if (gDebug) Mout << " property name found to be: "  << it->back() << endl;
      }
      else
      {
         MIDASERROR("Could not find \"name\" in Morse Definition");
      }
   }
}


/**
 * @brief evaluate the potential
 * @param x point to evaluate potential
 * @param i the index of the definitions in the parameter vectors
 **/
Nb MorsePot::EvalPot(Nb x, In i) const
{
   Nb tmp = (C_1 - std::exp(-mAs[i]*(x-mReqs[i])));
   Nb vmorse = mDes[i] * std::pow(tmp, C_2);
   return vmorse;
}

/**
 * @brief evaluate the potential
 * @param x point to evaluate potential
 * @param i the index of the definitions in the parameter vectors
 **/
MidasVector MorsePot::EvalDer(Nb x, In i) const
{
   MidasVector der(1);
   Nb tmp1 =  2*mAs[i]*std::exp(-mAs[i]*(x-mReqs[i]));
   Nb tmp2 = -2*mAs[i]*std::exp(-2*mAs[i]*(x-mReqs[i]));
   der.at(0) = mDes[i] * (tmp1 + tmp2);
   return der;
}

/**
 * Evaluate hessian.
 *
 * @param x   point to evaluate at
 * @param i the index of the definitions in the parameter vectors
 **/
MidasMatrix MorsePot::EvalHess(Nb x, In i) const
{
   MidasMatrix hess(I_1, I_1);
   hess.at(0, 0) = 2 * mDes[i] * mAs[i] * mAs[i] * (std::exp(-mAs[i] * (x - mReqs[i])) - std::exp(-2 * mAs[i] * (x - mReqs[i])));
   return hess;
}

/**
 * Evaluate potential for nuclei positions.
 *
 * @param aName         The name of the operator/property to evaluate.
 * @param aNucVect      Molecular structure
 **/
Nb MorsePot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalPot(aNucVect.front().Distance(aNucVect.back()), GetPropertyNr(aName));
}

/**
 * Evaluate first derivative of potential for nuclei positions.
 * 
 * @param aName       The name of the operator/property to evaluate.
 * @param aNucVect    Molecular structure 
 **/
MidasVector MorsePot::EvalDerImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalDer(aNucVect.front().Distance(aNucVect.back()), GetPropertyNr(aName));
}

/**
 * Evaluate second derivative of potential for nuclei positions.
 * 
 * @param aName       The name of the operator/property to evaluate.
 * @param aNucVect    Molecular structure 
 **/
MidasMatrix MorsePot::EvalHessImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalHess(aNucVect.front().Distance(aNucVect.back()), GetPropertyNr(aName));
}

/**
 * @brief overload output for MorsePot class
 * @param arOut output stream
 * @param arPot potential to be outputtet
 **/
ostream& operator<<(ostream& arOut, const MorsePot& arPot)
{  
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(ios::showpoint);

   arOut << " Morse Potential Data: \n"
         << " Parameters: \n" 
         << " De    = " << arPot.mDes << "\n"
         << " a     = " << arPot.mAs << "\n"
         << " Req   = " << arPot.mReqs << "\n"
         << " Bond length (a.u.)   "  << "   " << " Potential value  "  << std::endl;

   return arOut;
}
