/**
************************************************************************
*
*  @file                DoubleWellPot.h
* 
*  Created:             08/03/2008
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class for computing double-well potentials
*                       Parameterization from J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef DOUBLEWELLPOT_H_INCLUDED
#define DOUBLEWELLPOT_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

/**
 * @class DoubleWellPot
 * @brief implemenents a double well harmonic potential
 **/
class DoubleWellPot 
   : public ModelPot
{
   private: 
      std::vector<Nb> mHarmFreqs;         ///< Harm. freq.
      std::vector<Nb> mAs;                ///< A constant
      std::vector<Nb> mAlphas;            ///< Alpha constant
      std::vector<Nb> mMinVals;            ///< Min. value of the potential
      std::vector<Nb> mRedMasses;         ///< Reduced mass of system
      std::vector<std::string> mPotDefs;  ///< Stores potential definition
        
      void SetFromString(const std::string& aAux);  ///< Set parameters from a set of definition strings
      Nb EvalPotImpl(const std::string& name, const vector<Nuclei>&) const;  ///< For interface for ModelPot
      
      ///> Helper function for evaluation
      Nb EvalPot(Nb x, In i) const;         

   public:
      DoubleWellPot(const ModelPotInfo&); ///< Constructor used by ModePot Heirachy

      std::string Type() const { return "DOUBLEWELLPOT"; }
      
      friend ostream& operator<<(ostream& arOut, const DoubleWellPot& arPot);   ///< Output overloading
};

/**
 * @brief 
 **/
ostream& operator<<(ostream& arOut, const DoubleWellPot& arPot);   ///< Output overloading

#endif /* DOUBLEWELLPOT_H_INCLUDED */
