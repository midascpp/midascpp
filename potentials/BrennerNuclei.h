/**
*************************************************************************
*
* @file                BrennerNuclei.cc
*
* Created:             30/10/2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Definitions of class members for BrennerNuclei
*                      Implements a new Nuclei class (Brenner Nuclei)
*                      needed for storing nearest neighbors.
*  
* Last modified:       18/12/2007
*     
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*     
************************************************************************
**/

#ifndef BRENNER_NUCLEI_H
#define BRENNER_NUCLEI_H

// std headers
#include <string>
#include <map>
#include <utility>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"

class BrennerNuclei 
{
   private:
      std::string Type;
      Nb X;
      Nb Y;
      Nb Z;
      std::map<In,Nb> NearestNeighbor;
   
   public:
      BrennerNuclei(std::string aS, Nb aX, Nb aY, Nb aZ);
      std::string GetType() 
      {
         return Type;
      }
      
      Nb GetX() {return X;}
      Nb GetY() {return Y;}
      Nb GetZ() {return Z;}
      
      void SetX(Nb aD) {X=aD;}
      void SetY(Nb aD) {Y=aD;}
      void SetZ(Nb aD) {Z=aD;}
      
      void AddNN(In aI,Nb aD)
      {
         NearestNeighbor.insert(std::make_pair(aI,aD));
      }

      std::map<In,Nb>& GetNN() 
      {
         return NearestNeighbor;
      }

      void PrInMap();
      Nb Dot(BrennerNuclei aN);
      Nb GetMass();
      void Translate(std::vector<Nb>);
      
      void ClearNN() 
      {
         NearestNeighbor.clear();
      }
      
      friend std::ostream& operator<<(std::ostream&, BrennerNuclei);
};

#endif /* BRENNER_NUCLEI_H */
