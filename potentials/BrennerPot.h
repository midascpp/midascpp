/**
*************************************************************************
*
* @file                BrennerPot.h
*
* Created:             18/12/2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Definitions of class members for BrennerPot
*                      Implements a new Nuclei class (Brenner Nuclei)
*                      needed for storing nearest neighbors.
*  
* Last modified:       18/12/2007
*     
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*     
************************************************************************
**/

#ifndef BRENNER_H
#define BRENNER_H

// std headers
#include <string>
#include <fstream>
#include <vector>
#include <utility>

#include "potentials/BrennerNuclei.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/Spline3D.h"
#include "nuclei/Nuclei.h"
#include "mmv/MidasVector.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "potentials/ModelPot.h"

//Added wrapper class for the ModelPot heirachy
class BrennerPotModel 
   : public ModelPot
{
   private:
      //! Overload of evaluate function.
      virtual Nb EvalPotImpl(const std::vector<Nuclei>& arGeom) const;

      //! Overload of evaluate function.
      virtual MidasVector EvalDerImpl(const std::vector<Nuclei>& arGeom) const;
   public:
      BrennerPotModel(const ModelPotInfo&);
      
      //! Destructor (defaulted).
      ~BrennerPotModel() = default;
      
      //! Get the type of potential
      virtual std::string Type() const { return "BRENNERPOT"; }
};

class BrennerPot{
   private:
      std::vector<BrennerNuclei> Coord;
      std::vector<BrennerNuclei> OldCoord;
      Nb Energy;
      std::vector<Spline3D> mFSplines;
      std::vector<Spline3D> mTSplines;
      std::vector<Spline2D> mPSplines;
      MidasVector mFCCDerivs;
      MidasVector mFCHDerivs;
      MidasVector mFCCX;
      MidasVector mFCCY;
      MidasVector mFCCZ;
      MidasVector mFCCVal;
      MidasVector mFCHX;
      MidasVector mFCHY;
      MidasVector mFCHZ;
      MidasVector mFCHVal;
      MidasVector mTCCDerivs;
      MidasVector mTCCX;
      MidasVector mTCCY;
      MidasVector mTCCZ;
      MidasVector mTCCVal;
      MidasVector mPCCDerivs;
      MidasVector mPCHDerivs;
      MidasVector mPCCX;
      MidasVector mPCCY;
      MidasVector mPCCVal;
      MidasVector mPCHX;
      MidasVector mPCHY;
      MidasVector mPCHVal;

   public:
      BrennerPot();
      void SetNewCoordinates(vector<Nuclei>);
      void GetCoordFromFile(ifstream&);
      void PrintCoord();
      //void PrintCoord(ofstream&);
      void NearestNeighbors();
      Nb CalcEnergy();
      void AddRepulsionTerm(Nb&,In);
      void AddAttractionTerm(Nb&,In);
      Nb GetH(BrennerNuclei,string,string);
      Nb Get(In,In);
      void Set(In, In, Nb);
      In size() {return Coord.size();}
      void Update(vector<Nb>,Nb);
      void Restore();
      void MoveToCOM();
      Nb CalcContribution(BrennerNuclei,BrennerNuclei,BrennerNuclei);
      Nb GetP(In,In);
      Nb GetPParameter(In,In,string,string);
      Nb GetPiRC(In,In);
      Nb GetFParameter(In,In,In,string,string);
      Nb GetNC(In,In);
      Nb GetNH(In,In);
      Nb CalcCosDihedralAngle(In,In,In,In);
      Nb GetF(In,In);
      Nb GetDihedralTerm(In,In);
      Nb GetThresh1(string,string);
      Nb GetThresh2(string,string);
      Nb GetRe(string,string);
      Nb GetDe(string,string);
      Nb GetS(string,string);
      Nb GetBeta(string,string);
      Nb GetR1(string,string);
      Nb GetR2(string,string);
      Nb GetAlpha(string,string);
      Nb GetA(string,string);
      Nb GetQ(string,string);
      void MakeFSplines();
      void MakeTSplines();
      void MakePSplines();
      Nb GetFParameter(Nb,Nb,Nb,string,string);        
      void TestSplines();
      std::vector<Nb> CalcGradient();
};
#endif
