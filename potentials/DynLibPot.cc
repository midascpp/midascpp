#include "potentials/DynLibPot.h"

#include "nuclei/Nuclei.h"
#include "include/midaspot/Dll.h"

////
// Register for factory
////
ModelPotRegistration<DynLibPot> registerDynLibPot("DYNLIBPOT");

/**
 * Convert 'provides' variable from dynamic-library-provides type to ModelPot-provides type.
 *
 * @param dynlib_provides    'Provides' from dynlib.
 *
 * @return   Returns ModelPot-provides type corresponding to input.
 **/
potentials::provides DynLibPot::ConvertProvides
   (  PROVIDES_TYPE dynlib_provides
   )
{
   potentials::provides provides = potentials::provides::nothing;

   if(dynlib_provides & PROVIDES_VALUE)
   {
      provides = provides | potentials::provides::value;
   }
   if(dynlib_provides & PROVIDES_GRADIENT)
   {
      provides = provides | potentials::provides::first_derivatives;
   }
   if(dynlib_provides & PROVIDES_HESSIAN)
   {
      provides = provides | potentials::provides::second_derivatives;
   }

   return provides;
}

/**
 * Load cartesian coordinates from vector of Nuclei.
 *
 * @param aStructure   Vector of nuclei.
 * @param aCart        
 **/
void DynLibPot::LoadCartesian
   (  const std::vector<Nuclei>& aStructure
   ,  cartesian& aCart
   )  const
{
   //
   int counter = 0;
   int natoms  = mSurface.natoms;
   
   // Geometry conversion factor (Midas uses Bohr, but MidasDynlib might use AAngstrom)
   int conversion_factor = 1.0;
   if(mSurface.geometry_units == AANGSTROM)
   {
      conversion_factor = C_TANG;
   }
   
   // Loop over atoms
   for(const auto& atom : aStructure)
   {
      switch(mSurface.matrix_type)
      {
         case MATRIX_TYPE_COL_MAJOR:
            ((double*)aCart.coord)[counter    ] = atom.X() * conversion_factor;
            ((double*)aCart.coord)[counter + 1] = atom.Y() * conversion_factor;
            ((double*)aCart.coord)[counter + 2] = atom.Z() * conversion_factor;
            counter += 3;
            break;
         case MATRIX_TYPE_ROW_MAJOR:
            ((double*)aCart.coord)[counter             ] = atom.X() * conversion_factor;
            ((double*)aCart.coord)[counter + 1 * natoms] = atom.Y() * conversion_factor;
            ((double*)aCart.coord)[counter + 2 * natoms] = atom.Y() * conversion_factor;
            counter += 1;
            break;
         default:
            MIDASERROR("Unknown matrix_type.");
      }
   }
}

/** 
 * Overloadable function to evaluate the potential.
 **/
Nb DynLibPot::EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const
{
   if(!(mSurface.provides & PROVIDES_PROPERTY))
   {
      MIDASERROR("PROPERTY NOT PROVIDED.");
   }

   cartesian   cart;
   singlepoint sp;
   
   allocate_cartesian  (&mSurface, &cart);
   allocate_singlepoint(&mSurface, &sp, PROVIDES_VALUE);

   this->LoadCartesian(aStructure, cart);
   
   if(evaluate_property(&mSurface, &cart, &sp, aName.c_str(), PROVIDES_VALUE) == EXIT_FAILURE)
   {
      MIDASERROR("Could not evaluate DynlibPot");
   }
   
   double value = 0.0; 
   switch(mSurface.float_type)
   {
      case TYPE_DOUBLE:
         value = *((double*)sp.value);
         break;
      case TYPE_FLOAT:
         value = *((float*) sp.value);
         break;
      default:
         MIDASERROR("Unknown float_type.");
   }
   
   deallocate_cartesian  (&cart);
   deallocate_singlepoint(&sp);

   return value;
}

/** 
 * Overloadable function to evaluate the first derivative of the potential.
 **/
MidasVector DynLibPot::EvalDerImpl(const std::string& aName, const std::vector<Nuclei>&) const
{
   MIDASERROR("NOT IMPLEMENTED.");
   return MidasVector{};
}

/**
 * Overloadable function to evaluate the second derivate of the potential.
 **/
MidasMatrix DynLibPot::EvalHessImpl(const std::string& aName, const std::vector<Nuclei>&) const
{
   MIDASERROR("NOT IMPLEMENTED.");
   return MidasMatrix{};
}

/**
 * Constructor from ModelPotInfo.
 **/
DynLibPot::DynLibPot
   (  const ModelPotInfo& aInfo
   )
   :  ModelPot(ModelPot::properties_type{}, ModelPot::provides_type{})
{  
   std::string dynlib_filename;
   auto iter = aInfo.find("DYNLIBFILE");
   if(iter != aInfo.end())
   {
      dynlib_filename = iter->second;
   }
   else
   {
      MIDASERROR("No dynamic library filename was given.");
   }

   // Try to load surface from dynamic library
   if(initialize_surface(&mSurface, dynlib_filename.c_str()) == EXIT_FAILURE)
   {
      MIDASERROR("Could not initialize dynamic library surface.");
   }
   
   //
   if(mSurface.float_type & FLOAT32)
   {
      MIDASERROR("ONLY IMPLEMENTED FOR FLOAT64");
   }
   
   // Loop over functions in surface
   for(int i = 0; i < mSurface.nfunctions; ++i)
   {
      property_interface_t* prop = property_introspection(&mSurface, i);

      ModelPot::AddPropertyName(std::string{prop->name});
      ModelPot::AddProvides(this->ConvertProvides(prop->provides));
   }
}

/**
 * Destructor. Will finalize surface by closing dynamic library.
 **/
DynLibPot::~DynLibPot
   (
   )
{
   finalize_surface(&mSurface);
}
