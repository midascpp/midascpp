/**
************************************************************************
*
*  @file                AmmModelPot.h
* 
*  Created:             02/04/2008
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class for computing model potential for ammonia
*                       Parameterization from S. N. Yurchenko et al., JCP 123(2005), 134308
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef AMMPOT_H
#define AMMPOT_H

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

class AmmModelPot 
   : public ModelPot
{
   private: 
      In mParMax;                         ///< Maximum number of parameters
      MidasVector mParam;                 ///< Parameter set
      std::vector<std::string> mParamLab; ///< Label for the parameters
      std::vector<In> mIvar;              ///< variance??
      In mPotType;                        ///< Potential type (=0 for CBS**-5 =1 for CBS**-4)
      bool mHasBeenInit;                  ///< Initialization flag
      
      Nb EvalPotImpl(const std::string& name, const std::vector<Nuclei>&) const;
      
      Nb  brent(const Nb ax, const Nb bx, const Nb cx, const Nb tol, Nb &xmin);
      Nb LineSearch(Nb aA, Nb aB, Nb aC);          ///< Line search in 1 direction
      void InitParms();                            ///< Initialize the parameters

   public:
      AmmModelPot();                                     ///< Default constructor
      void SetPotType(In aPotType) {mPotType=aPotType;}  ///< Set potential parametrization 
      void PrintParms();                                 ///< Print out the parameters
      Nb Potential(MidasVector& arGeom) const;           ///< Function for potential evaluation
      Nb GetMinValue();                                  ///< Get the minimum value
      Nb f(const Nb aX);                                 ///< Symmetric stretch coordinate evaluation
};
#endif
