/**
************************************************************************
*
*  @file                ArNtwopPot.h
* 
*  Created:             04/04/2008
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class for computing model potential for (ArN2)^+ interaction
*                       Parameterization from R. Candori et al. J. Chem. Phys. 115(2001), 8888
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef ARNTWOPOT_H_INCLUDED
#define ARNTWOPOT_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

class ArNtwopPot 
   : public ModelPot
{
   private: 
      In mIpes;                  ///< The pes that will be generated

      Nb EPSI0;                  ///< Variables originally present in the common block
      Nb RM0;
      Nb BETA;
      Nb C4000;
      Nb X3;
      Nb X4;
      Nb B1;
      Nb B2;
      Nb B3;
      Nb B4;
      Nb C6000;
      Nb EPSI0ndp;
      Nb RM0ndp;
      Nb BETAndp;
      Nb C4000ndp;
      Nb X3ndp;
      Nb X4ndp;
      Nb B1ndp;
      Nb B2ndp;
      Nb B3ndp;
      Nb B4ndp;
      Nb C6000ndp;
      Nb A202;
      Nb ALFA202;
      Nb C4202;
      Nb C6202;
      Nb Q202;
      Nb A202ndp;
      Nb ALFA202ndp;
      Nb C6202ndp; 
      Nb C6022; 
      Nb A022; 
      Nb ALFA022; 
      Nb GAMMA022;
      Nb A220;
      Nb ALFA220;
      Nb A222; 
      Nb ALFA222;
      Nb C224;

      Nb EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& arGeom) const; ///< Interface for ModelPot

      /// Potential terms
      Nb v000(Nb& aR) const;        ///< v000 function
      Nb v022(Nb& aR) const;        ///< v022 function
      Nb v202(Nb& aR) const;        ///< v202 function
      Nb v220(Nb& aR) const;        ///< v220 function
      Nb v222(Nb& aR) const;        ///< v222 function
      Nb v224(Nb& aR) const;        ///< v224 function
      void Spline();                ///< Spline function
      void Splinendp();             ///< Splinendp function
      Nb v000nduep(Nb& aR) const;   ///< v000nduep function
      Nb v202nduep(Nb& aR) const;   ///< v202nduep function
      
   public:
      ArNtwopPot();                                 ///< Default constructor
      void SetPotType(In aIpes) {mIpes=aIpes;}      ///< Set the pes
      Nb GetIpes() {return mIpes;}                  ///< Get the pes to be constructed
      
      Nb Pot(Nb aR, Nb aTheta) const;               ///< Function for potential evaluation in Jacobi coordinates
      
};

#endif /* ARNTWOPOT_H_INCLUDED */
