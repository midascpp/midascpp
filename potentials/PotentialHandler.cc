#include "potentials/PotentialHandler.h"

namespace midas
{
namespace potentials
{
namespace detail
{

//!@{
//! Small helper for easy change of key type
template<class T>
std::string to_string(T&& t)
{
   return std::to_string(std::forward<T>(t));
}

const std::string& to_string(const std::string& str)
{
   return str;
}
//!@}

} /* namespace detail */

/** 
 * Load potential into handler.
 *
 * @param aKey    Lookup key for potential.
 * @param aInfo   Info to create potential.
 **/
PotentialHandler::potential_key_type PotentialHandler::LoadPotential
   (  const ModelPotInfo& aInfo
   ,  const potential_key_type& aKey
   )
{
   if(mMap.find(aKey) == mMap.end())
   {
      mMap.emplace(aKey, ModelPot::Factory(aInfo));
   }
   else
   {
      throw std::runtime_error("Key : '" + detail::to_string(aKey) + "' already in use.");
   }

   return aKey;
}

/**
 * Get a previously loaded potential.
 *
 * @param aKey    Lookup key.
 *
 * @return        Returns found potential, or throws an exception.
 **/
const PotentialHandler::potential_type& PotentialHandler::GetPotential
   (  const potential_key_type& aKey
   )  const
{
   auto iter = mMap.find(aKey);
  
   if(iter == mMap.end())
   {
      throw std::runtime_error("Potential not found for key : '" + detail::to_string(aKey) + "'.");
   }

   return iter->second;
}

} /* namespace potentials */
} /* namespace midas */
