/**
*************************************************************************
*
* @file                DoubleWellPot.cc
*
* Created:             06/03/2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for computing double-well potentials
*                      Parameterization from J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107
*
* Last modified: Thu Dec 03, 2009  09:51AM ove
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <string>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "inc_gen/Const.h"
#include "potentials/DoubleWellPot.h"
#include "util/conversions/VectorFromString.h"

////
// Register for factory
////
ModelPotRegistration<DoubleWellPot> registerDoubleWellPot("DOUBLEWELLPOT");

/**
 * @brief
 **/
DoubleWellPot::DoubleWellPot(const ModelPotInfo& aInfo): ModelPot({},{})
{
   auto iter = aInfo.find("MODELPOTDEFS");
   //Mout << "Iter is now : \"" << iter->second << "\"" << std::endl;
   if(iter != aInfo.end() && iter->second != "")
   {
      SetFromString(iter->second);
   }
   else
   {
      // If no specific definition is given,
      // set paramerts to the default NH3 Invection problem, 
      // fitted in J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107
      mHarmFreqs.emplace_back(974.2/C_AUTKAYS);
      Nb b_val=2032.0e0/C_AUTKAYS;
      Nb rho=0.60;
      mAs.emplace_back(b_val*exp(rho)/(exp(rho)-rho-C_1));
      mAlphas.emplace_back((exp(rho)/(C_2*mAs.back()))*pow(mHarmFreqs.back(),C_2));
      mMinVals.emplace_back(b_val*(rho+C_1)/(exp(rho)-rho-C_1));
      mRedMasses.emplace_back(2.48658405611024369*C_FAMU);
      mPotDefs.emplace_back("Default NH3 Inversion");
   
      AddPropertyName("GROUND_STATE_ENERGY");
      AddProvides(potentials::provides::value);
   }
}

namespace detail{
   Nb FindAndMakeNbOfTermDoubleWell(std::vector<std::vector<std::string>>& arTerms, const std::string& arName)
   {
      auto it = std::find_if(arTerms.begin(), arTerms.end(), [arName](auto& term){return term.front() == arName;});
      if(it != arTerms.end())
      {
         return midas::util::FromString<Nb>(it->back());
      }
      else
      {
         MIDASERROR("Could not find \""+arName+"\" in Double Well Definition");
      }
      return C_0; // Never happens
   }
} // namespace detail

/**
 * @brief Set the private members from a string
 * @param aAux string that defines doublewell potential
 **/
void DoubleWellPot::SetFromString(const std::string& aAux)
{
   //Seperate the input into a vector on each newline
   std::vector<std::string> definitions = midas::util::SeperateLinesIntoVector(aAux);
   for(auto& defin : definitions)
   {
      // Transform to lower case
      std::for_each(defin.begin(),defin.end(),[](char & c){c = std::tolower(c);});
      //Remove all spaces
      defin.erase(std::remove_if(defin.begin(), defin.end(), ::isspace), defin.end());
      //Save the definition
      mPotDefs.emplace_back(defin);
      //Split the string with each comma
      auto terms_tmp = midas::util::StringVectorFromString(defin, ',');
      
      //Further split the resulting strings into vectors with each = sign
      //If the length of the resulting vector is not two we discard it
      std::vector<std::vector<std::string>> terms;
      for(const auto& term : terms_tmp)
      {
         terms.emplace_back(midas::util::StringVectorFromString(term, '='));
         if(terms.back().size() != 2)
         {
            terms.pop_back();
         }
      }
      
      // Find Rho
      Nb rho=detail::FindAndMakeNbOfTermDoubleWell(terms, "rho");
      if (gDebug) Mout << " rho found to be: "  << rho << endl;
      
      // Find b, the barrier height
      Nb b_val=detail::FindAndMakeNbOfTermDoubleWell(terms, "b_val")/C_AUTKAYS;
      if (gDebug) Mout << " b_val found to be: "  << b_val << " au = "<< b_val*C_AUTKAYS << " cm-1" << endl;
   
      // Find Omega, harmonic frequency
      mHarmFreqs.emplace_back(detail::FindAndMakeNbOfTermDoubleWell(terms, "freq")/C_AUTKAYS);
      if (gDebug) Mout << " freq found to be: "  << mHarmFreqs.back() << " au = " << mHarmFreqs.back()*C_AUTKAYS << " cm-1" << endl;
      
      // Find mu, the reduced mass
      mRedMasses.emplace_back(detail::FindAndMakeNbOfTermDoubleWell(terms, "red_mass")*C_FAMU);
      if (gDebug) Mout << " reduced mass found to be: "  << mRedMasses.back() << " au = " << mRedMasses.back()/C_FAMU << " AMU" << endl;
      
      //fill the private members
      mAs.emplace_back(b_val*exp(rho)/(exp(rho)-rho-C_1));
      mMinVals.emplace_back(b_val*(rho+C_1)/(exp(rho)-rho-C_1));
      mAlphas.emplace_back((exp(rho)/(C_2*mAs.back()))*pow(mHarmFreqs.back(),C_2));
      
      // Find the name of the property
      auto it = std::find_if(terms.begin(), terms.end(), [](auto& term){return term.front() == "name";});
      if(it != terms.end())
      {
         std::for_each(it->back().begin(),it->back().end(),[](char & c){c = std::toupper(c);});
         AddPropertyName(it->back());
         AddProvides(potentials::provides::value);
         if (gDebug) Mout << " property name found to be: "  << it->back() << endl;
      }
      else
      {
         MIDASERROR("Could not find \"name\" in Double Well Definition");
      }
      
   }
}

/**
 * @brief To streamline the interfaces we provide the following function
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param aNucVect molecule as a set of nuclei
 *
 * @return    Returns the value of the potential.
 **/
Nb DoubleWellPot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   ) const
{
   if(aNucVect.size() != 2)
   {
      MIDASERROR("DoubleWell potential only inplemented for two nuclei!!");
   }
   return EvalPot(aNucVect.front().Distance(aNucVect.back()), GetPropertyNr(aName));
}

/**
 * @brief Evaluate the potential over an assigned point
 * @param x evaluate potential at this point
 * @param i the index of the definitions in the parameter vectors
 **/
Nb DoubleWellPot::EvalPot(Nb x, In i) const
{
   Nb x_p = x*std::sqrt(mRedMasses[i]);
   Nb tmp = pow(x_p,C_2);
   Nb v_pot = C_I_2*pow(mHarmFreqs[i],C_2)*tmp + mAs[i]*exp(-mAlphas[i]*tmp) - mMinVals[i];
   return v_pot;
}

/**
 * @brief Overload output for DoubleWellPot class
 * @param arOut output stream
 * @param arPot potential to output
 **/
ostream& operator<<(std::ostream& arOut, const DoubleWellPot& arPot)
{  
   arOut.setf(std::ios::scientific);
   arOut.setf(std::ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(std::ios::showpoint);

   arOut << " Double Well Potential Data: \n"
         << " Parameters:  \n"
         << " Harm. Freqs.:           " << arPot.mHarmFreqs << "\n"
         << " Alphas:                 " << arPot.mAlphas << "\n"
         << " A constants:               " << arPot.mAs
         << std::endl;

   return arOut;
}
