/**
*************************************************************************
*
* @file                BrennerPot.cc
*
* Created:             30/10/2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Definitions of class members for BrennerPot
*                       Implements a new Nuclei class (Brenner Nuclei)
*                       needed for storing nearest neighbors.
*
* Last modified:       18/12/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <sstream>

// midas headers
#include "potentials/ModelPot.h"
#include "potentials/BrennerPot.h"
#include "potentials/BrennerNuclei.h"
#include "potentials/DataTable.h"
#include "nuclei/Nuclei.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/ProgramSettings.h"
#include "pes/Pes.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/Spline3D.h"
#include "util/Io.h"
#include "util/Timer.h"

const Nb AUTOANGS=0.5291772108;
const Nb AUTOEV=27.2113845;
const Nb PI=acos(-1.0);

/**
 * Constructor
 **/
BrennerPotModel::BrennerPotModel
   (  const ModelPotInfo& aInfo
   )
   : ModelPot
      (  {"GROUND_STATE_ENERGY"}
      ,  {potentials::provides::value | potentials::provides::first_derivatives}
      )
{
}

/**
 *
 **/
Nb BrennerPotModel::EvalPotImpl(const std::vector<Nuclei>& arGeom) const
{
   BrennerPot driver;
   driver.SetNewCoordinates(arGeom);
   return driver.CalcEnergy();
}

/**
 *
 **/
MidasVector BrennerPotModel::EvalDerImpl(const std::vector<Nuclei>& arGeom) const
{
   BrennerPot driver;
   driver.SetNewCoordinates(arGeom);
   return MidasVector(driver.CalcGradient());
}

/**
* Constructor
**/
BrennerPot::BrennerPot() {
   Coord.clear();
   OldCoord.clear();
   Energy=0.0;
   // First for the F spline
   mFCCDerivs.SetNewSize(0);
   mFCHDerivs.SetNewSize(0);
   mFCCX.SetNewSize(0);
   mFCCY.SetNewSize(0);
   mFCCZ.SetNewSize(0);
   mFCCVal.SetNewSize(0);
   mFCHX.SetNewSize(0);
   mFCHY.SetNewSize(0);
   mFCHZ.SetNewSize(0);
   mFCHVal.SetNewSize(0);
   // Then the T spline
   mTCCDerivs.SetNewSize(0);
   // and the P spline
   mPCCDerivs.SetNewSize(0);
   mPCHDerivs.SetNewSize(0);
   // Do the splines
   MakeFSplines();
   MakeTSplines();
   MakePSplines();
}
   
void BrennerPot::SetNewCoordinates(vector<Nuclei> aN) {
   // just transform the Nucleis to BrennerNucleis
   Coord.clear();
   OldCoord.clear();
   for(int i=0;i<aN.size();i++) {
      if(gDebug)
         Mout << "Coord = " << aN[i] << endl;
      BrennerNuclei n(aN[i].AtomLabel(),aN[i].X(),aN[i].Y(),aN[i].Z());
      Coord.push_back(n);
      OldCoord.push_back(n);
   }
   // Also make NN list for all nuc.
   NearestNeighbors();
}

/**
*  Function to make the required F splines to use
*  for all the points
**/
void BrennerPot::MakeFSplines() {
   // Do the F function for CC
   mFCCX.SetNewSize(I_4);
   mFCCY.SetNewSize(I_4);
   mFCCZ.SetNewSize(I_9);
   mFCCX[I_0]=C_0;
   mFCCX[I_1]=C_1;
   mFCCX[I_2]=C_2;
   mFCCX[I_3]=C_3;
   mFCCY[I_0]=C_0;
   mFCCY[I_1]=C_1;
   mFCCY[I_2]=C_2;
   mFCCY[I_3]=C_3;
   mFCCZ[I_0]=C_1;
   mFCCZ[I_1]=C_2;
   mFCCZ[I_2]=C_3;
   mFCCZ[I_3]=C_4;
   mFCCZ[I_4]=C_5;
   mFCCZ[I_5]=C_6;
   mFCCZ[I_6]=C_7;
   mFCCZ[I_7]=C_8;
   mFCCZ[I_8]=C_9;
   // Now make the function value vector
   mFCCVal.SetNewSize(144);
   ifstream params;
   string datadir=input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR");
   string filename=datadir+"/"+"FFunctionParameters.param";
   params.open(filename.c_str());
   string line;
   while(getline(params,line)) {
      if(line.find("Carbon-Carbon parameters")!=line.npos)
         break;
   }
   for(In i=I_0;i<mFCCVal.Size();i++) {
      In j,k,l;
      Nb val;
      getline(params,line);
      istringstream iss(line);
      iss >> j >> k >> l >> val;
      mFCCVal[i]=val;
   }
   Spline3D FCCSpline(SPLINEINTTYPE::NATURAL,mFCCX,mFCCY,mFCCZ,mFCCVal,C_0,C_0);
   mFCCDerivs.SetNewSize(144);
   FCCSpline.GetYigDerivs(mFCCDerivs);
   mFSplines.push_back(FCCSpline);
   // Do the same for CH
   mFCHX.SetNewSize(4);
   mFCHY.SetNewSize(4);
   mFCHZ.SetNewSize(9);
   mFCHX[I_0]=C_0;
   mFCHX[I_1]=C_1;
   mFCHX[I_2]=C_2;
   mFCHX[I_3]=C_3;
   mFCHY[I_0]=C_0;
   mFCHY[I_1]=C_1;
   mFCHY[I_2]=C_2;
   mFCHY[I_3]=C_3;
   mFCHZ[I_0]=C_1;
   mFCHZ[I_1]=C_2;
   mFCHZ[I_2]=C_3;
   mFCHZ[I_3]=C_4;
   mFCHZ[I_4]=C_5;
   mFCHZ[I_5]=C_6;
   mFCHZ[I_6]=C_7;
   mFCHZ[I_7]=C_8;
   mFCHZ[I_8]=C_9;
   // Now make the function value vector
   mFCHVal.SetNewSize(144);
   while(getline(params,line)) {
      if(line.find("Carbon-Hydrogen parameters")!=line.npos)
         break;
   }
   for(In i=I_0;i<mFCHVal.Size();i++) {
      In j,k,l;
      Nb val;
      getline(params,line);
      istringstream iss(line);
      iss >> j >> k >> l >> val;
      mFCHVal[i]=val;
   }
   Spline3D FCHSpline(SPLINEINTTYPE::NATURAL,mFCHX,mFCHY,mFCHZ,mFCHVal,C_0,C_0);
   mFCHDerivs.SetNewSize(144);
   FCHSpline.GetYigDerivs(mFCHDerivs);
   mFSplines.push_back(FCHSpline);
}

/**
*  Function to make the required P splines to use
*  for all the points
**/
void BrennerPot::MakePSplines() {
   // Do the F function for CC
   mPCCX.SetNewSize(I_4);
   mPCCY.SetNewSize(I_4);
   mPCCX[I_0]=C_0;
   mPCCX[I_1]=C_1;
   mPCCX[I_2]=C_2;
   mPCCX[I_3]=C_3;
   mPCCY[I_0]=C_0;
   mPCCY[I_1]=C_1;
   mPCCY[I_2]=C_2;
   mPCCY[I_3]=C_3;
   // Now make the function value vector
   mPCCVal.SetNewSize(16,C_0);
   ifstream params;
   string datadir=input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR");
   string filename=datadir+"/"+"PFunctionParameters.param";
   params.open(filename.c_str());
   string line;
   while(getline(params,line)) {
      if(line.find("Carbon-Carbon parameters")!=line.npos)
         break;
   }
   for(In i=I_0;i<mPCCVal.Size();i++) {
      In j,k;
      Nb val;
      getline(params,line);
      istringstream iss(line);
      iss >> j >> k >> val;
      mPCCVal[i]=val;
   }
   Spline2D PCCSpline(SPLINEINTTYPE::NATURAL,mPCCX,mPCCY,mPCCVal,C_0,C_0);
   mPCCDerivs.SetNewSize(16);
   PCCSpline.GetYigDerivs(mPCCDerivs);
   mPSplines.push_back(PCCSpline);
   // That was for CC, now for CH
   mPCHX.SetNewSize(I_4);
   mPCHY.SetNewSize(I_4);
   mPCHX[I_0]=C_0;
   mPCHX[I_1]=C_1;
   mPCHX[I_2]=C_2;
   mPCHX[I_3]=C_3;
   mPCHY[I_0]=C_0;
   mPCHY[I_1]=C_1;
   mPCHY[I_2]=C_2;
   mPCHY[I_3]=C_3;
   mPCHVal.SetNewSize(16,C_0);
   // Now make the function value vector
   while(getline(params,line)) {
      if(line.find("Carbon-Hydrogen parameters")!=line.npos)
         break;
   }
   for(In i=I_0;i<mPCHVal.Size();i++) {
      In j,k;
      Nb val;
      getline(params,line);
      istringstream iss(line);
      iss >> j >> k >> val;
      mPCHVal[i]=val;
   }
   Spline2D PCHSpline(SPLINEINTTYPE::NATURAL,mPCHX,mPCHY,mPCHVal,C_0,C_0);
   mPCHDerivs.SetNewSize(16);
   PCHSpline.GetYigDerivs(mPCHDerivs);
   mPSplines.push_back(PCHSpline);
}

/**
*  Function to make the required T splines to use
*  for all the points
**/
void BrennerPot::MakeTSplines() {
   // Do the T function for CC
   mTCCX.SetNewSize(I_4);
   mTCCY.SetNewSize(I_4);
   mTCCZ.SetNewSize(I_9);
   mTCCX[I_0]=C_0;
   mTCCX[I_1]=C_1;
   mTCCX[I_2]=C_2;
   mTCCX[I_3]=C_3;
   mTCCY[I_0]=C_0;
   mTCCY[I_1]=C_1;
   mTCCY[I_2]=C_2;
   mTCCY[I_3]=C_3;
   mTCCZ[I_0]=C_1;
   mTCCZ[I_1]=C_2;
   mTCCZ[I_2]=C_3;
   mTCCZ[I_3]=C_4;
   mTCCZ[I_4]=C_5;
   mTCCZ[I_5]=C_6;
   mTCCZ[I_6]=C_7;
   mTCCZ[I_7]=C_8;
   mTCCZ[I_8]=C_9;
   // Now make the function value vector
   mTCCVal.SetNewSize(144,C_0);
   ifstream params;
   string datadir=input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR");
   string filename=datadir+"/"+"TFunctionParameters.param";
   params.open(filename.c_str());
   string line;
   while(getline(params,line)) {
      if(line.find("Carbon-Carbon parameters")!=line.npos)
         break;
   }
   for(In i=I_0;i<mTCCVal.Size();i++) {
      In j,k,l;
      Nb val;
      getline(params,line);
      istringstream iss(line);
      iss >> j >> k >> l >> val;
      mTCCVal[i]=val;
   }
   Spline3D TCCSpline(SPLINEINTTYPE::NATURAL,mTCCX,mTCCY,mTCCZ,mTCCVal,C_0,C_0);
   mTCCDerivs.SetNewSize(144);
   TCCSpline.GetYigDerivs(mTCCDerivs);
   mTSplines.push_back(TCCSpline);
}

void BrennerPot::TestSplines() {
   // a debug function to test the splines
   Mout << "F Splines, CC:" << endl;
   for(In i=0;i<mFCCX.Size();i++) {
      for(In j=0;j<mFCCY.Size();j++) {
         for(In k=0;k<mFCCZ.Size();k++) {
            MidasVector Yog(I_1,C_0);
            MidasVector Xog1(I_1,mFCCX[i]);
            MidasVector Xog2(I_1,mFCCY[j]);
            MidasVector Xog3(I_1,mFCCZ[k]);
            MidasVector DerivLeft(I_3,C_0);
            MidasVector DerivRight(I_3,C_0);
            GetYog(Yog, Xog1, Xog2, Xog3, mFCCX, mFCCY, mFCCZ, mFCCVal,
                  mFCCDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
            Mout << "F(" << mFCCX[i] << "," << mFCCY[j] << "," << mFCCZ[k] << ") = " << Yog[0] << endl;
         }
      }
   }
   Mout << "F Splines, CH:" << endl;
   for(In i=0;i<mFCHX.Size();i++) {
      for(In j=0;j<mFCHY.Size();j++) {
         for(In k=0;k<mFCHZ.Size();k++) {
            MidasVector Yog(I_1,C_0);
            MidasVector Xog1(I_1,mFCHX[i]);
            MidasVector Xog2(I_1,mFCHY[j]);
            MidasVector Xog3(I_1,mFCHZ[k]);
            MidasVector DerivLeft(I_3,C_0);
            MidasVector DerivRight(I_3,C_0);
            GetYog(Yog, Xog1, Xog2, Xog3, mFCHX, mFCHY, mFCHZ, mFCHVal,
                  mFCHDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
            Mout << "F(" << mFCHX[i] << "," << mFCHY[j] << "," << mFCHZ[k] << ") = " << Yog[0] << endl;
         }
      }
   }
   Mout << "P Splines, CC:" << endl;
   for(In i=0;i<mPCCX.Size();i++) {
      for(In j=0;j<mPCCY.Size();j++) {
         Nb Yog=C_0;
         Nb Xog1=mPCCX[i];
         Nb Xog2=mPCCY[j];
         MidasVector DerivLeft(I_3,C_0);
         MidasVector DerivRight(I_3,C_0);
         GetYog(Yog, Xog1, Xog2, mPCCX, mPCCY, mPCCVal,
                  mPCCDerivs, SPLINEINTTYPE::NATURAL, C_0, C_0);
         Mout << "P(" << mPCCX[i] << "," << mPCCY[j] << ") = " << Yog << endl;
      }
   }
   Mout << "P Splines, CH:" << endl;
   for(In i=0;i<mPCHX.Size();i++) {
      for(In j=0;j<mPCHY.Size();j++) {
         Nb Yog=C_0;
         Nb Xog1=mPCCX[i];
         Nb Xog2=mPCCY[j];
         MidasVector DerivLeft(I_3,C_0);
         MidasVector DerivRight(I_3,C_0);
         GetYog(Yog, Xog1, Xog2, mPCCX, mPCCY, mPCCVal,
                  mPCCDerivs, SPLINEINTTYPE::NATURAL, C_0, C_0);
         Mout << "P(" << mPCHX[i] << "," << mPCHY[j] << ") = " << Yog << endl;
      }
   }
   Mout << "T Splines, CC:" << endl;
   for(In i=0;i<mTCCX.Size();i++) {
      for(In j=0;j<mTCCY.Size();j++) {
         for(In k=0;k<mTCCZ.Size();k++) {
            MidasVector Yog(I_1,C_0);
            MidasVector Xog1(I_1,mTCCX[i]);
            MidasVector Xog2(I_1,mTCCY[j]);
            MidasVector Xog3(I_1,mTCCZ[k]);
            MidasVector DerivLeft(I_3,C_0);
            MidasVector DerivRight(I_3,C_0);
            GetYog(Yog, Xog1, Xog2, Xog3, mTCCX, mTCCY, mTCCZ, mTCCVal,
                  mTCCDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
            Mout << "T(" << mTCCX[i] << "," << mTCCY[j] << "," << mTCCZ[k] << ") = " << Yog[0] << endl;
         }
      }
   }
}

void BrennerPot::NearestNeighbors() {
   // for each atom make a list of nearest atoms
   // including their distance
   for(In i=0;i<Coord.size();i++) {
      Coord[i].ClearNN();
      for(In j=I_0;j<Coord.size();j++) {
         if(i==j) continue;
         Nb x2=Coord[i].GetX()-Coord[j].GetX();
         x2*=x2;
         Nb y2=Coord[i].GetY()-Coord[j].GetY();
         y2*=y2;
         Nb z2=Coord[i].GetZ()-Coord[j].GetZ();
         z2*=z2;
         Nb dist=sqrt(x2+y2+z2);
         Nb thresh1=GetThresh1(Coord[i].GetType(),Coord[j].GetType());
         Nb thresh2=GetThresh2(Coord[i].GetType(),Coord[j].GetType());
         if( dist<thresh1 || (dist>thresh1 && dist<thresh2) ) {
            Coord[i].AddNN(j,dist);
         }
      }
   }
}

void BrennerPot::PrintCoord() {
   Mout.setf(ios_base::scientific);
   midas::stream::ScopedPrecision(16, Mout);
   for(In i=0;i<Coord.size();i++)
      Mout << Coord[i].GetType() << " " << Coord[i].GetX() << " " << Coord[i].GetY()
           << " " << Coord[i].GetZ() << endl;
}

//void BrennerPot::PrintCoord(ofstream& aSs) {
//   aSs.setf(ios_base::scientific);
//   midas::stream::ScopedPrecision(16, aSs);
//   for(In i=0;i<Coord.size();i++)
//      aSs << Coord[i].GetType() << " " << Coord[i].GetX() << " " << Coord[i].GetY()
//           << " " << Coord[i].GetZ() << endl;
//}

/**
* Calculate energy
**/
Nb BrennerPot::CalcEnergy() {
   Nb vatt=C_0,vrep=C_0;
   // loop over all atom sites
   Timer ener_time;
   for(In i=0;i<Coord.size();i++) {
      // do repulsion term
      AddRepulsionTerm(vrep,i);
      // do attraction term
      AddAttractionTerm(vatt,i);
   }
   if(gDebug) {
      Mout << "Rep. energy = " << vrep*C_AUTEV << endl;
      Mout << "Att. energy = " << vatt*C_AUTEV << endl;
      Mout << "Tot. energy = " << (vrep+vatt)*C_AUTEV << endl;
   }
   return vrep+vatt;
}

void BrennerPot::AddRepulsionTerm(Nb& aEner,In aI) {
   // loop over nearest neighbors
   for(map<In,Nb>::iterator i=Coord[aI].GetNN().begin(); i!=Coord[aI].GetNN().end(); i++) {
      In nuc=i->first;
      // test
      if(nuc<=aI)
         continue;
      Nb dist=i->second;
      Nb Alpha=GetAlpha(Coord[aI].GetType(),Coord[nuc].GetType());
      Nb A=GetA(Coord[aI].GetType(),Coord[nuc].GetType());
      Nb Q=GetQ(Coord[aI].GetType(),Coord[nuc].GetType());
      // now calculate the function, f
      Nb R1=GetR1(Coord[aI].GetType(),Coord[nuc].GetType());
      Nb R2=GetR2(Coord[aI].GetType(),Coord[nuc].GetType());
      Nb rep=(1.0+Q/dist)*A*exp(-Alpha*dist);
      Nb f;
      if(dist<R1)
         f=1.0;
      else if(dist>R1 && dist<R2)
         f=0.5*(1.0+cos((dist-R1)/(R2-R1)));
      else f=0.0;
      //cout << "f = " << f << endl;
      rep*=f;
      //cout << "Repulsion = " << rep << endl;
      aEner+=rep;
   }
}

void BrennerPot::AddAttractionTerm(Nb& aEner,In aI) {
   // loop over nearest neighbors
   Nb result=0.0;
   for(map<In,Nb>::iterator j=Coord[aI].GetNN().begin(); j!=Coord[aI].GetNN().end(); j++) {
      Nb vatt=0.0;
      In nuc_j=j->first;
      if(nuc_j<=aI)
         continue;
      Nb dist_ij=j->second;
      // loop over nearest neighbors again. We will get zeros anyway... (the k sum in eq. 8)
      Nb b_ij=1.0,b_ji=1.0,bar_ij=0.0;
      // Make merged NN list
      map<In,Nb> merged_list;
      for(map<In,Nb>::iterator k=Coord[aI].GetNN().begin(); k!=Coord[aI].GetNN().end(); k++)
         merged_list.insert(std::make_pair(k->first,k->second));
      for(map<In,Nb>::iterator k=Coord[nuc_j].GetNN().begin(); k!=Coord[nuc_j].GetNN().end(); k++)
         merged_list.insert(std::make_pair(k->first,k->second));
      for(map<In,Nb>::iterator k=merged_list.begin(); k!=merged_list.end(); k++) {
      //for(In k=0;k<Coord.size();k++) {
         In nuc_k=k->first;
         if(nuc_k==aI || nuc_k==nuc_j) continue;
         // calculate b_ij contribution from k
         b_ij+=CalcContribution(Coord[aI],Coord[nuc_j],Coord[nuc_k]);
         b_ji+=CalcContribution(Coord[nuc_j],Coord[aI],Coord[nuc_k]);
      }
      // now get the P function and calculate \bar{b_ij}
      b_ij+=GetP(aI,nuc_j);
      b_ji+=GetP(nuc_j,aI);
      b_ij=1.0/sqrt(b_ij);
      b_ji=1.0/sqrt(b_ji);
      if(gDebug)
         cout << "b_ji,b_ij = " << b_ji << ", " << b_ij << " Piterm = " << GetPiRC(aI,nuc_j) << endl;
      // according to the equation, the following should be divided by 2.
      bar_ij+=(b_ij+b_ji);
      bar_ij*=0.5;
      // OK, got the bar, now add a few extra terms.
      bar_ij+=C_I_2*GetPiRC(aI,nuc_j);
      //cout << "BTOT before = " << (bar_ij-C_I_2*GetPiRC(aI,nuc_j))*C_2 << endl;
      //cout << "BTOT after = " << bar_ij*C_2 << endl;
      bar_ij+=GetDihedralTerm(aI,nuc_j);
      // Calculate attraction...
      Nb R1=GetR1(Coord[aI].GetType(),Coord[nuc_j].GetType());
      Nb R2=GetR2(Coord[aI].GetType(),Coord[nuc_j].GetType());
      // calculate dist
      Nb f;
      if(dist_ij<R1)
         f=1.0;
      else if(dist_ij>R1 && dist_ij<R2)
         f=0.5*(1.0+cos((dist_ij-R1)/(R2-R1)));
      else
         f=0.0;
      if(Coord[aI].GetType()=="C" && Coord[nuc_j].GetType()=="C") {
         vatt+=(12388.79197798/AUTOEV)*exp(-4.7204523127*AUTOANGS*dist_ij);
         vatt+=(17.56740646509/AUTOEV)*exp(-1.4332132499*AUTOANGS*dist_ij);
         vatt+=(30.71493208065/AUTOEV)*exp(-1.3826912506*AUTOANGS*dist_ij);
      }
      else if((Coord[aI].GetType()=="C" && Coord[nuc_j].GetType()=="H")
           || (Coord[aI].GetType()=="H" && Coord[nuc_j].GetType()=="C")) 
      {
         vatt+=(32.3551866587/AUTOEV)*exp(-1.43445805925*AUTOANGS*dist_ij);
      }
      else {
         vatt+=(29.632593/AUTOEV)*exp(-1.71589217*AUTOANGS*dist_ij);
      }
      vatt*=(f*bar_ij);
      result+=vatt;
      //cout << "E(k) = " << vatt*C_AUTEV << ", B = " << C_2*f*bar_ij << ", Vatt = " << C_I_2*vatt*C_AUTEV/(f*bar_ij) << endl;
      if(gDebug) {
         cout << "Bar_ij = " << bar_ij << ", f = " << f << endl;
         cout << "dist_ij = " << dist_ij*AUTOANGS << " => Att = " << 0.5*vatt*C_AUTEV << endl;
      }
   }
   aEner-=result;
}

/**
* Calculate f^c_ik(r_ik)*G(cos(theta_ijk))*exp(lambda_ijk)
*
* aN1 = nuc_i
* aN2 = nuc_j
* aN3 = nuc_k
**/
Nb BrennerPot::CalcContribution(BrennerNuclei aN1, BrennerNuclei aN2, BrennerNuclei aN3) {
   // start by calculating f^c_ik(r_ik)
   Nb R1=GetR1(aN1.GetType(),aN3.GetType());
   Nb R2=GetR2(aN1.GetType(),aN3.GetType());
   // calculate dist
   Nb x2=aN1.GetX()-aN3.GetX();
   x2*=x2;
   Nb y2=aN1.GetY()-aN3.GetY();
   y2*=y2;
   Nb z2=aN1.GetZ()-aN3.GetZ();
   z2*=z2;
   Nb dist=sqrt(x2+y2+z2);
   Nb f;
   if(dist<R1)
      f=1.0;
   else if(dist>R1 && dist<R2)
      f=0.5*(1.0+cos((dist-R1)/(R2-R1)));
   else {
      f=0.0;
      return 0.0;
   }
   // now calculate G function. This becomes a headache (at least if the
   // revised angular function should be implemented, as in eq. 11 in BrennerPots
   // 2002 paper in j. phys.: cond. matter.
   // only do the G function for now (this is bad enough)!
   // start by finding the angle. aN1 is the central atom
   vector<Nb> v_ij(3,0.0);
   vector<Nb> v_ik(3,0.0);
   // x,y,z components of bond i-j
   v_ij[0]=aN1.GetX()-aN2.GetX();
   v_ij[1]=aN1.GetY()-aN2.GetY();
   v_ij[2]=aN1.GetZ()-aN2.GetZ();
   // x,y,z components of bond i-k
   v_ik[0]=aN1.GetX()-aN3.GetX();
   v_ik[1]=aN1.GetY()-aN3.GetY();
   v_ik[2]=aN1.GetZ()-aN3.GetZ();
   Nb dotprod=0.0,norm_ij=0.0,norm_ik=0.0;
   for(In i=0;i<3;i++) {
      dotprod+=v_ij[i]*v_ik[i];
      norm_ij+=v_ij[i]*v_ij[i];
      norm_ik+=v_ik[i]*v_ik[i];
   }
   Nb cos_theta=dotprod/(sqrt(norm_ij)*sqrt(norm_ik));
   // We have cos(theta), evaluate G!
   DataTable DT;
   vector<Nb> V;
   if(aN1.GetType()=="C") {
      // we have a carbon, follow brenner's c-code for getting the parameters here!
      // first determine the "i_angle"
      Nb Pi=acos(-1.0e+0);
      if(cos_theta>cos(0.6082*Pi) && cos_theta<1.0) {
         // pretty sure that we have a bond w. 0<angle<109.47
         V=DT.GetCParameters(4);
      }
      else if(cos_theta<=cos(0.6082*Pi) && cos_theta>=-0.5e+0) {
         // pretty sure that we have a bond w. 109.47=<angle=<120
         V=DT.GetCParameters(3);
      }
      else if(cos_theta<-0.5e+0 && cos_theta>=-1.0e+0) {
         // pretty sure that we have a bond w. 120<angle=<180
         V=DT.GetCParameters(2);
      }
      else {
         cout << "Hmm... That's a strange bond? I am getting cos_theta = " << cos_theta << endl;
      }
   }
   else if(aN1.GetType()=="C") {
      // we have a carbon, follow brenner's c-code for getting the parameters here!
      // first determine the "i_angle"
      Nb Pi=acos(-1.0e+0);
      if(cos_theta>cos(0.6082*Pi) && cos_theta<1.0) {
         // pretty sure that we have a bond w. 0<angle<109.47
         V=DT.GetHParameters(4);
      }
      else if(cos_theta<=cos(0.6082*Pi) && cos_theta>=-0.5e+0) {
         // pretty sure that we have a bond w. 109.47=<angle=<120
         V=DT.GetHParameters(3);
      }
      else if(cos_theta<-0.5e+0 && cos_theta>=-1.0e+0) {
         // pretty sure that we have a bond w. 120<angle=<180
         V=DT.GetHParameters(2);
      }
      else {
         std::cout << "Hmm... That's a strange bond? I am getting cos_theta = " << cos_theta << endl;
      }
   }
   else {
      Mout << "Only C and H implemented..." << endl;
      exit(10);
   }
   Nb cos_theta2=cos_theta*cos_theta;
   Nb cos_theta3=cos_theta2*cos_theta;
   Nb cos_theta4=cos_theta2*cos_theta2;
   Nb cos_theta5=cos_theta2*cos_theta3;
   Nb G;
   G=V[1]+V[2]*cos_theta+V[3]*cos_theta2+V[4]*cos_theta3+V[5]*cos_theta4+V[6]*cos_theta5;
   std::cout.setf(ios::scientific);
   midas::stream::ScopedPrecision(12, std::cout);
   if(gDebug)
   {
      std::cout << "angle = " << acos(cos_theta)*180.0/acos(-1.0) << " => G = " << G << endl;
   }
   // we now have f, and G(cos(theta)), we just need exp(lambda) and we are done!
   // is lambda only 4.0 for HHH and 0 otherwise???
   Nb lambda=0.0;
   if(aN1.GetType()=="H" && aN2.GetType()=="H" && aN3.GetType()=="H")
      lambda=4.0;
   return f*G*exp(lambda);
}

/**
*  For atom aI, return number of Hydrogens
**/
Nb BrennerPot::GetNC(In aI, In aJ) {
   Nb NCarbon=0.0;
   //for(In i=0;i<Coord.size();i++) {
   for(map<In,Nb>::iterator i=Coord[aI].GetNN().begin(); i!=Coord[aI].GetNN().end(); i++) {
      if(Coord[i->first].GetType()=="C" && aI!=i->first && aJ!=i->first) {
      /* old
      if(Coord[i].GetType().find("C")!=Coord[i].GetType().npos
         && i!=aI && i!=aJ) {
         Nb x2=Coord[i].GetX()-Coord[aI].GetX();
         x2*=x2;
         Nb y2=Coord[i].GetY()-Coord[aI].GetY(); 
         y2*=y2;
         Nb z2=Coord[i].GetZ()-Coord[aI].GetZ();
         z2*=z2;
         Nb dist=sqrt(x2+y2+z2);
      */
         Nb dist=i->second;
         Nb R1=GetR1("C","C");
         Nb R2=GetR2("C","C");
         Nb f;
         if(dist<R1)
            f=1.0;
         else if(dist>R1 && dist<R2)
            f=0.5*(1.0+cos((dist-R1)/(R2-R1)));
         else f=0.0;
         NCarbon+=f;
      }
   }
   return NCarbon;
}

/**
*  For atom aI, return number of Hydrogens
**/
Nb BrennerPot::GetNH(In aI, In aJ) {
   Nb NHydrogen=0.0;
   //for(In i=0;i<Coord.size();i++) {
   for(map<In,Nb>::iterator i=Coord[aI].GetNN().begin(); i!=Coord[aI].GetNN().end(); i++) {
      if(Coord[i->first].GetType()=="H" && aI!=i->first && aJ!=i->first) {
      /* old
      if(Coord[i].GetType().find("H")!=Coord[i].GetType().npos
         && i!=aI && i!=aJ) {
         Nb x2=Coord[i].GetX()-Coord[aI].GetX();
         x2*=x2;
         Nb y2=Coord[i].GetY()-Coord[aI].GetY();
         y2*=y2;
         Nb z2=Coord[i].GetZ()-Coord[aI].GetZ();
         z2*=z2;
         Nb dist=sqrt(x2+y2+z2);
      */
         Nb dist=i->second;
         Nb R1=GetR1("C","H");
         Nb R2=GetR2("C","H");
         Nb f;
         if(dist<R1)
            f=1.0;
         else if(dist>R1 && dist<R2)
            f=0.5*(1.0+cos((dist-R1)/(R2-R1)));
         else f=0.0;
         NHydrogen+=f;
      }
   }
   return NHydrogen;
}

Nb BrennerPot::GetPiRC(In aN1,In aN2) {
   Nb f_ik,f_jk,i_term=0.0,j_term=0.0; 
   map<In,Nb> merged_list;
   for(map<In,Nb>::iterator k=Coord[aN1].GetNN().begin(); k!=Coord[aN1].GetNN().end(); k++)
      merged_list.insert(std::make_pair(k->first,k->second));
   for(map<In,Nb>::iterator k=Coord[aN2].GetNN().begin(); k!=Coord[aN2].GetNN().end(); k++)
      merged_list.insert(std::make_pair(k->first,k->second));
   for(map<In,Nb>::iterator i=merged_list.begin(); i!=merged_list.end(); i++) {
      if(Coord[i->first].GetType()!="C")
         continue;
      if(i->first==aN1 || i->first==aN2)
         continue;
      // first f_ik
      Nb R1=GetR1(Coord[aN1].GetType(),Coord[i->first].GetType());
      Nb R2=GetR2(Coord[aN1].GetType(),Coord[i->first].GetType());
      Nb x2=Coord[aN1].GetX()-Coord[i->first].GetX();
      x2*=x2;
      Nb y2=Coord[aN1].GetY()-Coord[i->first].GetY();
      y2*=y2;
      Nb z2=Coord[aN1].GetZ()-Coord[i->first].GetZ();
      z2*=z2;
      Nb dist=sqrt(x2+y2+z2);
      if(dist<R1)
         f_ik=1.0;
      else if(dist>R1 && dist<R2)
         f_ik=0.5*(1.0+cos((dist-R1)/(R2-R1)));
      else
         f_ik=0.0;
      // Do f_jk
      R1=GetR1(Coord[aN2].GetType(),Coord[i->first].GetType());
      R2=GetR2(Coord[aN2].GetType(),Coord[i->first].GetType());
      x2=Coord[aN2].GetX()-Coord[i->first].GetX();
      x2*=x2;
      y2=Coord[aN2].GetY()-Coord[i->first].GetY();
      y2*=y2;
      z2=Coord[aN2].GetZ()-Coord[i->first].GetZ();
      z2*=z2;
      dist=sqrt(x2+y2+z2);
      if(dist<R1)
         f_jk=1.0;
      else if(dist>R1 && dist<R2)
         f_jk=0.5*(1.0+cos((dist-R1)/(R2-R1)));
      else 
         f_jk=0.0;
      // And the capital F, (here f_x_ik)
      Nb x_ik=GetNC(aN1,i->first)+GetNH(aN1,i->first)-f_ik;
      Nb f_x_ik;
      if(x_ik<2)
         f_x_ik=1.0;
      else if(x_ik>3.0)
         f_x_ik=0.0;
      else
         f_x_ik=0.5*cos(2.0*PI*(x_ik-2.0));
      // And the capital F, (here f_x_jk)
      Nb x_jk=GetNC(aN2,i->first)+GetNH(aN2,i->first)-f_jk;
      Nb f_x_jk;
      if(x_jk<2)
         f_x_jk=1.0;
      else if(x_jk>3.0)
         f_x_jk=0.0;
      else
         f_x_jk=0.5*cos(2.0*PI*(x_jk-2.0));
      // ok, add to the two terms
      i_term+=f_ik*f_x_ik;
      j_term+=f_jk*f_x_jk;
   }
   Nb n_ij_conj=1.0+i_term*i_term+j_term*j_term;
   Nb n_i=GetNH(aN1,aN2)+GetNC(aN1,aN2);
   Nb n_j=GetNH(aN2,aN1)+GetNC(aN2,aN1);
   // At this point use the FSpline using the current point...
   Nb result=C_0;
   MidasVector Yog(I_1,C_0);
   MidasVector Xog1(I_1,n_i);
   MidasVector Xog2(I_1,n_j);
   MidasVector Xog3(I_1,n_ij_conj);
   if(Coord[aN1].GetType()=="C" && Coord[aN2].GetType()=="C") {
      // CC
      MidasVector DerivLeft(I_3,C_0);
      MidasVector DerivRight(I_3,C_0);
      GetYog(Yog, Xog1, Xog2, Xog3, mFCCX, mFCCY, mFCCZ, mFCCVal,
             mFCCDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
      result=Yog[0];
   }
   if((Coord[aN1].GetType()=="C" && Coord[aN2].GetType()=="H") ||
      (Coord[aN1].GetType()=="H" && Coord[aN2].GetType()=="C")) {
      // CC
      MidasVector DerivLeft(I_3,C_0);
      MidasVector DerivRight(I_3,C_0);
      GetYog(Yog, Xog1, Xog2, Xog3, mFCHX, mFCHY, mFCHZ, mFCHVal,
             mFCHDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
      result=Yog[0];
   }
   //cout << "MBH: RAD = " << result << endl;
   return result;
}

Nb BrennerPot::GetDihedralTerm(In aN1,In aN2) {
   // Now get the dihedral term according to eq. 18.
   Nb result=0.0;
   // Try a merged map here!!!
   map<In,Nb> merged_list;
   for(map<In,Nb>::iterator k=Coord[aN1].GetNN().begin(); k!=Coord[aN1].GetNN().end(); k++)
      merged_list.insert(std::make_pair(k->first,k->second));
   for(map<In,Nb>::iterator k=Coord[aN2].GetNN().begin(); k!=Coord[aN2].GetNN().end(); k++)
      merged_list.insert(std::make_pair(k->first,k->second));
   for(map<In,Nb>::iterator i=merged_list.begin(); i!=merged_list.end(); i++) {
      if(i->first==aN1 || i->first==aN2)
         continue;
      for(map<In,Nb>::iterator j=merged_list.begin(); j!=merged_list.end(); j++) {
         if(j->first==aN1 || j->first==aN2)
            continue; 
          // start by calculating cos^2(theta)
         Nb cos2_theta=CalcCosDihedralAngle(aN1,aN2,i->first,j->first);
         cos2_theta*=cos2_theta;
         result+=(1.0-cos2_theta)*GetF(aN1,i->first)*GetF(aN2,j->first);
      }
   }
   // get n_ij_conj, reuse the merged map here...
   Nb f_ik,f_jk,i_term=0.0,j_term=0.0; 
   for(map<In,Nb>::iterator i=merged_list.begin(); i!=merged_list.end(); i++) {
      if(Coord[i->first].GetType()!="C")
         continue;
      if(i->first==aN1 || i->first==aN2)
         continue;
      // first f_ik
      Nb R1=GetR1(Coord[aN1].GetType(),Coord[i->first].GetType());
      Nb R2=GetR2(Coord[aN1].GetType(),Coord[i->first].GetType());
      Nb x2=Coord[aN1].GetX()-Coord[i->first].GetX();
      x2*=x2;
      Nb y2=Coord[aN1].GetY()-Coord[i->first].GetY();
      y2*=y2;
      Nb z2=Coord[aN1].GetZ()-Coord[i->first].GetZ();
      z2*=z2;
      Nb dist=sqrt(x2+y2+z2);
      if(dist<R1)
         f_ik=1.0;
      else if(dist>R1 && dist<R2)
         f_ik=0.5*(1.0+cos((dist-R1)/(R2-R1)));
      else
         f_ik=0.0;
      // Do f_jk
      R1=GetR1(Coord[aN2].GetType(),Coord[i->first].GetType());
      R2=GetR2(Coord[aN2].GetType(),Coord[i->first].GetType());
      x2=Coord[aN2].GetX()-Coord[i->first].GetX();
      x2*=x2;
      y2=Coord[aN2].GetY()-Coord[i->first].GetY();
      y2*=y2;
      z2=Coord[aN2].GetZ()-Coord[i->first].GetZ();
      z2*=z2;
      dist=sqrt(x2+y2+z2);
      if(dist<R1)
         f_jk=1.0;
      else if(dist>R1 && dist<R2)
         f_jk=0.5*(1.0+cos((dist-R1)/(R2-R1)));
      else 
         f_jk=0.0;
      // And the capital F, (here f_x_ik)
      Nb x_ik=GetNC(aN1,i->first)+GetNH(aN1,i->first)-f_ik;
      Nb f_x_ik;
      if(x_ik<2)
         f_x_ik=1.0;
      else if(x_ik>3.0)
         f_x_ik=0.0;
      else
         f_x_ik=0.5*cos(2.0*PI*(x_ik-2.0));
      // And the capital F, (here f_x_jk)
      Nb x_jk=GetNC(aN2,i->first)+GetNH(aN2,i->first)-f_jk;
      Nb f_x_jk;
      if(x_jk<2)
         f_x_jk=1.0;
      else if(x_jk>3.0)
         f_x_jk=0.0;
      else
         f_x_jk=0.5*cos(2.0*PI*(x_jk-2.0));
      // ok, add to the two terms
      i_term+=f_ik*f_x_ik;
      j_term+=f_jk*f_x_jk;
   }
   Nb n_ij_conj=1.0+i_term*i_term+j_term*j_term;
   Nb n_i=GetNH(aN1,aN2)+GetNC(aN1,aN2);
   Nb n_j=GetNH(aN2,aN1)+GetNC(aN2,aN1);
   // finally multiply with T_ij(): n_C, n_H, and n_conj as with F term?
   MidasVector Yog(I_1,C_0);
   MidasVector Xog1(I_1,n_i);
   MidasVector Xog2(I_1,n_j);
   MidasVector Xog3(I_1,n_ij_conj);
   if(Coord[aN1].GetType()=="C" && Coord[aN2].GetType()=="C") {
      // CC
      MidasVector DerivLeft(I_3,C_0);
      MidasVector DerivRight(I_3,C_0);
      GetYog(Yog, Xog1, Xog2, Xog3, mTCCX, mTCCY, mTCCZ, mTCCVal,
             mTCCDerivs, SPLINEINTTYPE::NATURAL, DerivLeft, DerivRight);
      result*=Yog[0];
   }
   else 
      result*=0.0;
   return result;
}

Nb BrennerPot::CalcCosDihedralAngle(In i, In j, In k, In l) {
   vector<Nb> e_jik(3,0.0),e_ijl(3,0.0);
   e_jik[0]=(Coord[i].GetY()-Coord[j].GetY())*(Coord[k].GetZ()-Coord[i].GetZ());
   e_jik[0]-=(Coord[k].GetY()-Coord[i].GetY())*(Coord[i].GetZ()-Coord[j].GetZ());
   e_jik[1]=-(Coord[i].GetX()-Coord[j].GetX())*(Coord[k].GetZ()-Coord[i].GetZ());
   e_jik[1]=+(Coord[k].GetX()-Coord[i].GetX())*(Coord[i].GetZ()-Coord[j].GetZ());
   e_jik[2]=(Coord[i].GetX()-Coord[j].GetX())*(Coord[k].GetY()-Coord[i].GetY());
   e_jik[2]=-(Coord[k].GetX()-Coord[i].GetX())*(Coord[i].GetY()-Coord[j].GetY());
   // that was r_ji X r_ik
   e_ijl[0]=(Coord[j].GetY()-Coord[i].GetY())*(Coord[l].GetZ()-Coord[j].GetZ());
   e_ijl[0]-=(Coord[l].GetY()-Coord[j].GetY())*(Coord[j].GetZ()-Coord[i].GetZ());
   e_ijl[1]=-(Coord[j].GetX()-Coord[i].GetX())*(Coord[l].GetZ()-Coord[j].GetZ());
   e_ijl[1]=+(Coord[l].GetX()-Coord[j].GetX())*(Coord[j].GetZ()-Coord[i].GetZ());
   e_ijl[2]=(Coord[j].GetX()-Coord[i].GetX())*(Coord[l].GetY()-Coord[j].GetY());
   e_ijl[2]=-(Coord[l].GetX()-Coord[j].GetX())*(Coord[j].GetY()-Coord[i].GetY());
   // their lengths
   Nb l_e_jik=e_jik[0]*e_jik[0]+e_jik[1]*e_jik[1]+e_jik[2]*e_jik[2];
   l_e_jik=sqrt(l_e_jik);
   Nb l_e_ijl=e_ijl[0]*e_ijl[0]+e_ijl[1]*e_ijl[1]+e_ijl[2]*e_ijl[2];
   l_e_ijl=sqrt(l_e_ijl);
   Nb e_e_dot=e_jik[0]*e_ijl[0]+e_jik[1]*e_ijl[1]+e_jik[2]*e_ijl[2];
   return e_e_dot/(l_e_jik*l_e_ijl);
}

Nb BrennerPot::GetF(In aN1, In aN2) {
   Nb x2=Coord[aN1].GetX()-Coord[aN2].GetX();
   x2*=x2;
   Nb y2=Coord[aN1].GetY()-Coord[aN2].GetY();
   y2*=y2;
   Nb z2=Coord[aN1].GetZ()-Coord[aN2].GetZ();
   z2*=z2;
   Nb dist=sqrt(x2+y2+z2);
   Nb R1=GetR1(Coord[aN1].GetType(),Coord[aN2].GetType());
   Nb R2=GetR2(Coord[aN1].GetType(),Coord[aN2].GetType());
   Nb f;
   if(dist<R1)
      f=1.0;
   else if(dist>R1 && dist<R2)
      f=0.5*(1.0+cos((dist-R1)/(R2-R1)));
   else f=0.0;
   return f; 
}

Nb BrennerPot::GetThresh1(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 1.7/AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.3/AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.3/AUTOANGS;
      else if(aS2=="H")          //HH
         return 1.1/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetThresh2(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 2.0/AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.8/AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.8/AUTOANGS;
      else if(aS2=="H")          //HH
         return 1.7/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetRe(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 1.315/AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.1199/AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.1199/AUTOANGS;
      else if(aS2=="H")          //HH
         return 0.74144/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetDe(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 6.325/AUTOEV;
      else if(aS2=="H")          //CH 
         return 3.6422/AUTOEV;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 3.6422/AUTOEV;
      else if(aS2=="H")          //HH
         return 4.7509/AUTOEV;
   }
   return 0.0;
}

Nb BrennerPot::GetS(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 1.29;
      else if(aS2=="H")          //CH 
         return 1.7386;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.7386;
      else if(aS2=="H")          //HH
         return 2.3432;
   }
   return 0.0;
}

Nb BrennerPot::GetBeta(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 1.5*AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.9583*AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.9583*AUTOANGS;
      else if(aS2=="H")          //HH
         return 1.9436*AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetR1(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 1.7/AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.3/AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.3/AUTOANGS;
      else if(aS2=="H")          //HH
         return 1.1/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetR2(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          //CC
         return 2.0/AUTOANGS;
      else if(aS2=="H")          //CH 
         return 1.8/AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          //HC
         return 1.8/AUTOANGS;
      else if(aS2=="H")          //HH
         return 1.7/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetAlpha(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          // CC
         return 4.746539060659529*AUTOANGS;
      else if(aS2=="H")          // CH
         return 4.102549828548784*AUTOANGS;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          // HC
         return 4.102549828548784*AUTOANGS;
      else if(aS2=="H")    // HH
         return 3.536298648376465*AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetA(string aS1, string aS2) {
   if(aS1=="C")
   {
      if(aS2=="C")          // CC
         return 10953.54416216992/AUTOEV;
      else if(aS2=="H")          // CH
         return 149.9409872288120/AUTOEV;
   }
   else if(aS1=="H")
   {
      if(aS2=="C")          // HC
         return 149.9409872288120/AUTOEV;
      else if(aS2=="H")    // HH
         return 32.81735574722296/AUTOEV;
   }
   return 0.0;
}

Nb BrennerPot::GetQ(string aS1, string aS2) 
{
   if(aS1=="C")
   {
      if(aS2=="C")          // CC
         return 0.3134602960832605/AUTOANGS;
      else if(aS2=="H")          // CH
         return 0.3407757282257080/AUTOANGS;
   }
   else if(aS2=="H")
   {
      if(aS2=="C")          // HC
         return 0.3407757282257080/AUTOANGS;
      else if(aS2=="H")    // HH
         return 0.3704714870452888/AUTOANGS;
   }
   return 0.0;
}

Nb BrennerPot::GetP(In aI, In aJ) {
   Nb NCarbon=C_0,NHydrogen=C_0;
   NCarbon=GetNC(aI,aJ);
   NHydrogen=GetNH(aI,aJ);
   // use the bicubic spline
   Nb result=C_0;
   Nb Yog=C_0;
   if(Coord[aI].GetType()=="C" && Coord[aJ].GetType()=="C") 
   {
      // CC
      GetYog(Yog, NHydrogen, NCarbon, mPCCX, mPCCY, mPCCVal,
             mPCCDerivs, SPLINEINTTYPE::NATURAL, C_0, C_0);
      result=Yog;
   }
   else if (Coord[aI].GetType()=="C" && Coord[aJ].GetType()=="H") 
   {
      // CH
      GetYog(Yog, NHydrogen, NCarbon, mPCHX, mPCHY, mPCHVal,
             mPCHDerivs, SPLINEINTTYPE::NATURAL, C_0, C_0);
      result=Yog;
   }
   else
      result=C_0;
   return result;
}

Nb BrennerPot::Get(In aI, In aJ) {
   if(aJ==0)
      return Coord[aI].GetX();
   if(aJ==1)
      return Coord[aI].GetY();
   if(aJ==2)
      return Coord[aI].GetZ();
   else
      exit(10);
}

void BrennerPot::Set(In aI, In aJ, Nb aD) {
   if(aJ==0)
      Coord[aI].SetX(Coord[aI].GetX()+aD);
   else if(aJ==1)
      Coord[aI].SetY(Coord[aI].GetY()+aD);
   else if(aJ==2)
      Coord[aI].SetZ(Coord[aI].GetZ()+aD);
   else
      exit(10);
   NearestNeighbors();
}

void BrennerPot::Update(vector<Nb> aV, Nb aD) {
   if(gDebug)
      cout << "Updating structure with ss = " << aD << endl;
   for(In i=0;i<Coord.size();i++) {
      OldCoord[i].SetX(Coord[i].GetX());
      OldCoord[i].SetY(Coord[i].GetY());
      OldCoord[i].SetZ(Coord[i].GetZ());
      Coord[i].SetX(Coord[i].GetX()+aD*aV[i*3]);
      Coord[i].SetY(Coord[i].GetY()+aD*aV[i*3+I_1]);
      Coord[i].SetZ(Coord[i].GetZ()+aD*aV[i*3+I_2]);
   }
   NearestNeighbors();
}

void BrennerPot::Restore() {
   for(In i=0;i<Coord.size();i++) {
      Coord[i].SetX(OldCoord[i].GetX());
      Coord[i].SetY(OldCoord[i].GetY());
      Coord[i].SetZ(OldCoord[i].GetZ());
   }
   NearestNeighbors();
}

void BrennerPot::MoveToCOM() {
   // start by determining the vector from origo to COM
   vector<Nb> com(3,0.0);
   Nb total_mass=0.0;
   for(In i=0;i<Coord.size();i++) {
      Nb mass=Coord[i].GetMass();
      total_mass+=mass;
      com[0]+=Coord[i].GetX()*mass;
      com[1]+=Coord[i].GetY()*mass;
      com[2]+=Coord[i].GetZ()*mass;
   }
   com[0]=com[0]/total_mass;
   com[1]=com[1]/total_mass;
   com[2]=com[2]/total_mass;
   for(In i=0;i<Coord.size();i++) {
      Coord[i].Translate(com);
   }
}

vector<Nb> BrennerPot::CalcGradient() {
   vector<Nb> result;
   result.clear();
   for(In i=0;i<Coord.size();i++) {
      for(In j=0;j<3;j++) {
         Set(i,j,-0.000001);
         Nb left=CalcEnergy();
         Set(i,j,0.000002);
         Nb right=CalcEnergy();
         result.push_back((right-left)/0.000002);
         // restore coord.
         Set(i,j,-0.000001);
      }
   }
   return result;
}
