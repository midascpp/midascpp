#ifndef DATATABLE_H_INCLUDED
#define DATATABLE_H_INCLUDED

// std headers
#include <cstdlib>
#include <map>
#include <vector>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"

class DataTable 
{
   private:
      std::map<In,std::vector<Nb> > CParameters;
      std::map<In,std::vector<Nb> > HParameters;

   public:
      DataTable();
      std::vector<Nb> GetCParameters(In);
      std::vector<Nb> GetHParameters(In);
};

#endif /* DATATABLE_H_INCLUDED */
