#include "include/midaspot/Dll.h"

#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif 

/* ==========================================================================
 * Some helpers.
 * ========================================================================== */
/**
 * Wrapper for dlopen to check for loading errors.
 **/
void* detail_dlopen_wrap(const char* dll)
{
#ifdef DEBUG_DYNAMIC_INTERFACE
   printf("Opening DLL handle: %s\n", dll);
#endif 

   void *handle = dlopen(dll, RTLD_NOW);

#ifdef DEBUG_DYNAMIC_INTERFACE
   printf("DLL handle: %p\n", handle);
#endif 

   if (handle == nullptr)
   {
      printf("%s\n", dlerror());
      exit(EXIT_FAILURE);
   }

   return handle;
}

/**
 * Wrapper for dlclose.
 **/
void detail_dlclose_wrap(void* handle)
{
#ifdef DEBUG_DYNAMIC_INTERFACE
   printf("Closing DLL handle: %p.\n", handle);
#endif

   dlclose(handle);
}

/**
 * Wrapper for dlsym, which checks for loading errors.
 **/
void* detail_dlsym_wrap(void* handle, const char* func_name, int harderror)
{
#ifdef DEBUG_DYNAMIC_INTERFACE
   printf("DLL symbol link: %s\n", func_name);
#endif 

   void* func = dlsym(handle, func_name);

   if (!func && harderror)
   {
      printf("%s\n", dlerror());
      exit(EXIT_FAILURE);
   }
   
   return func;
}

/**
 * Wrapper for deallocating singlepoint struct based on size of float type.
 **/
EXIT_STATUS detail_allocate_singlepoint(struct singlepoint* sp, int natoms, int provides, int size)
{
#ifdef DEBUG_DYNAMIC_INTERFACE
   printf("ALLOCATING SINGLEPOINT SIZE %d\n", size);
#endif 

   if(provides & PROVIDES_PROPERTY)
   {
      sp->value = malloc(size);
   }
   else
   {
      sp->value = nullptr;
   }

   if(provides & PROVIDES_GRADIENT)
   {
      sp->gradient = malloc(3 * natoms * size);
   }
   else
   {
      sp->gradient = nullptr;
   }

   if(provides & PROVIDES_HESSIAN)
   {
      sp->hessian = malloc(((3 * natoms * (3 * natoms + 1) / 2)) * size);
   }
   else
   {
      sp->hessian = nullptr;
   }

   return EXIT_SUCCESS;
}

/**
 * Set iopt from provides.
 **/
int detail_set_iopt(PROVIDES_TYPE provides)
{
   if(provides & PROVIDES_HESSIAN)
   {
      return 2;
   }
   else if(provides & PROVIDES_GRADIENT)
   {
      return 1;
   }
   else if(provides & PROVIDES_VALUE)
   {
      return 0;
   }

   return 0;
}

/* ==========================================================================
 * Interface
 * ========================================================================== */
/**
 * Initialize surface struct by opening dll file and loading functions.
 *
 * @param s   Surface struct to initialize.
 * @param dll Path of dynamic library file.
 *
 * @return    Returns EXIT_SUCCESS on success, EXIT_FAILURE on error.
 **/
EXIT_STATUS initialize_surface(struct surface* s, const char* dll)
{
   /* Initialize all values */
   s->handle    = nullptr;
   s->interface = nullptr;
   s->functions = nullptr;
   
   /* Load dynamic handle */
   s->handle = detail_dlopen_wrap(dll);
   
   /* Try to load definitions or use defaults */
   /* a: surface, b: function type, c: function name, d: default value, e: hard-error */
   #define bind_function(a, b, c, d, e) \
      { \
         b fptr = (b)detail_dlsym_wrap(a->handle, #c, e); \
         if(fptr) \
         { \
            a->c = fptr(); \
         } \
         else \
         { \
            a->c = d; \
         } \
      } \
   
   bind_function(s, float_type_fptr    , float_type    , TYPE_DOUBLE          , false);
   bind_function(s, matrix_type_fptr   , matrix_type   , MATRIX_TYPE_COL_MAJOR, false);
   bind_function(s, geometry_units_fptr, geometry_units, BOHR                 , false);
   bind_function(s, natoms_fptr        , natoms        , 0                    , true);

   #undef bind_function
   
   /* Load evaluation functions */
   int* nfunc_ptr = (int*)detail_dlsym_wrap(s->handle, "iface_nfunctions", true);
   if(!nfunc_ptr)
   {
      return EXIT_FAILURE;
   }
   else
   {
      s->nfunctions = *nfunc_ptr;
   }

   property_interface_t* ifunc_ptr = (property_interface_t*)detail_dlsym_wrap(s->handle, "iface_functions", true);
   if(!ifunc_ptr)
   {  
      return EXIT_FAILURE;
   }
   else
   {
      s->interface = ifunc_ptr;
   }

   s->functions = (evaluate_fptr*)malloc(s->nfunctions * sizeof(evaluate_fptr));
   
   for(int i = 0; i < s->nfunctions; ++i)
   {
      s->functions[i] = (evaluate_fptr)detail_dlsym_wrap(s->handle, s->interface[i].function_str, true);
      if(!s->functions[i])
      {
         return EXIT_FAILURE;
      }

      s->provides |= s->interface[i].provides;
   }
   
   return EXIT_SUCCESS;
}

/**
 * Finalize surface struct. Will close dynamic library handle.
 *
 * @param s    Surface struct to finalize.
 *
 * @return     Returns EXIT_SUCCESS on success.
 **/
EXIT_STATUS finalize_surface(struct surface* s)
{
   if(s->handle)
   {
      detail_dlclose_wrap(s->handle);
   }

   if(s->functions)
   {
      free(s->functions);
   }

   return EXIT_SUCCESS;
}

/**
 * Allocate cartesian struct.
 *
 * @param s   Surface struct defining the surface.
 * @param c   Pointer to the cartesian struct to allocate.
 * 
 * @return    Returns EXIT_SUCCESS on success.
 **/
EXIT_STATUS allocate_cartesian(const struct surface* s, struct cartesian* c)
{
   c->natoms = s->natoms;

   switch(s->float_type)
   {
      case TYPE_DOUBLE:
         c->coord  = malloc(3 * c->natoms * sizeof(double));
         break;
      case TYPE_FLOAT:
         c->coord  = malloc(3 * c->natoms * sizeof(float));
         break;
   }

   return EXIT_SUCCESS;
}

/**
 * Allocate singlepoint struct.
 *
 * @param s         Surface struct defining the surface.
 * @param sp        Pointer to the singlepoint to allocate.
 * @param provides  What is provided by the function, i.e. what do we need to allocate.
 *
 * @return   Returns EXIT_SUCCESS on success, EXIT_FAILURE on error.
 **/
EXIT_STATUS allocate_singlepoint(const struct surface* s, struct singlepoint* sp, int provides)
{
   int natoms   = s->natoms;
   
   // Be default we take everything that surface provides
   if(provides == PROVIDES_NOTHING)
   {
      provides = s->provides;
   }
   
   // Check that surface provides everything we request
   if (  ((provides & s->provides ) ^ provides) != 0  )
   {
      return EXIT_FAILURE; // Return error
   } 
   
   // Allocate the 'singlepoint'
   switch(s->float_type)
   {
      case TYPE_DOUBLE:
         return detail_allocate_singlepoint(sp, natoms, provides, sizeof(double));
         break;
      case TYPE_FLOAT:
         return detail_allocate_singlepoint(sp, natoms, provides, sizeof(float));
         break;
   }

   return EXIT_SUCCESS;
}

/**
 * Deallocate cartesian struct.
 *
 * @param c   The cartesian struct to deallocate.
 *
 * @return    Returns EXIT_SUCCESS on success.
 **/
EXIT_STATUS deallocate_cartesian(struct cartesian* c)
{
   if(c->coord)
   {
      free(c->coord);
   }
   
   return EXIT_SUCCESS;
}

/**
 * Deallocate singplepoint struct.
 *
 * @param sp   The singlepoint to dealocate.
 *
 * @return     Returns EXIT_SUCCESS on success.
 **/
EXIT_STATUS deallocate_singlepoint(struct singlepoint* sp)
{
   if(sp->value)
   {
      free(sp->value);
   }
   if(sp->gradient)
   {
      free(sp->gradient);
   }
   if(sp->hessian)
   {
      free(sp->hessian);
   }

   return EXIT_SUCCESS;
}

/**
 * Allocate what is needed for property.
 **/
EXIT_STATUS allocate_property(const struct surface* s, struct cartesian* c, struct singlepoint* sp, PROVIDES_TYPE provides)
{
   EXIT_STATUS alloc_cart_status = allocate_cartesian  (s, c);
   EXIT_STATUS alloc_sp_status   = allocate_singlepoint(s, sp, provides);

   return alloc_cart_status & alloc_sp_status;
}

/**
 * Evaluate singlepoint.
 **/
EXIT_STATUS evaluate_property(const struct surface* s, struct cartesian* c, struct singlepoint* sp, const char* name, PROVIDES_TYPE provides)
{
   // Find property function
   int ifunc = -1;
   for(int i = 0; i < s->nfunctions; ++i)
   {
      if(strcmp(s->interface[i].name, name) == 0)
      {
         ifunc = i;
         break;
      }
   }

   if(ifunc == -1)
   {
      return EXIT_FAILURE;
   }

   // 
   int iopt = detail_set_iopt(provides);

   // Some error checking
   if(iopt == 0 && (!sp->value))
   {
      return EXIT_FAILURE;
   }
   else if(iopt == 1 && (!sp->value || !sp->gradient))
   {
      return EXIT_FAILURE;
   }
   else if(iopt == 2 && (!sp->value || !sp->gradient || !sp->hessian))
   {
      return EXIT_FAILURE;
   }
   
   // Evaluate function
   return s->functions[ifunc](c, sp, iopt);
}

/**
 * De-allocate cartesian and singlepoint.
 * 
 * @return  Returns EXIT_SUCCESS on success, EXIT_FAILURE on error.
 **/
EXIT_STATUS deallocate_property(struct cartesian* c, struct singlepoint* sp)
{
   EXIT_STATUS ret_c  = deallocate_cartesian(c);
   EXIT_STATUS ret_sp = deallocate_singlepoint(sp);
   return (ret_c == 0 && ret_sp == 0)? EXIT_SUCCESS: EXIT_FAILURE;
}

/**
 *
 **/
property_interface_t* property_introspection(const struct surface* s, int prop_num)
{
   if(prop_num < s->nfunctions)
   {
      return s->interface + prop_num;
   }
   return nullptr;
}

/** 
 * Get name of property number 'i'
 **/
const char* property_introspection_name(const struct surface* s, int prop_num)
{
   property_interface_t* prop = property_introspection(s, prop_num);
   if(prop)
   {
      return prop->name;
   }
   return nullptr;
}

/**
 *
 **/

#ifdef __cplusplus
}
#endif 
