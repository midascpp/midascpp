/**
************************************************************************
*
*  @file                MidasPotential.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a midas potential
* 
*  Last modified:      11-03-2020
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#ifndef MIDASPOTENTIAL_H_INCLUDED
#define MIDASPOTENTIAL_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "mmv/MidasVector.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"

/**
*  Container and evaluater for the midas potentials
*  Reads in a given .mop file and stores all potential functions, 
*  coefficients and terms.
**/
class MidasPotential
{
   private:
      std::string                mFileName;
      In                         mNumModes;
      MidasVector                mScalings;
      
      // Map for determining the position of each type of function for the operator
      // Only used for initialization of the mOperatorTerms
      std::map<std::string, In>  mFunctionMap;
      
      // Container for each MC's set of function positions and coefficients
      std::map<std::vector<In>, std::vector<std::pair<std::vector<In>, Nb> > >   mOperatorTerms;

      // Containers for the actual functions
      std::vector<MidasFunctionWrapper<Nb> >                mFunctions;
      std::vector<MidasFunctionWrapper<taylor<Nb, 1, 1>>>   mDerFunctions;
      std::vector<MidasFunctionWrapper<taylor<Nb, 2, 2>>>   mDDerFunctions;
      
      std::vector<GenericFunctionWrapper<Nb> >                mGenFunctions;
      std::vector<GenericFunctionWrapper<taylor<Nb, 1, 1>>>   mGenDerFunctions;
      std::vector<GenericFunctionWrapper<taylor<Nb, 2, 2>>>   mGenDDerFunctions;

      // Does all the work of converting the read in potential for
      // use in the PES code
      void InitializeContainers(const std::vector<std::string>&);
      
      std::vector<In> FindAndAddFunctionsToMap(const std::vector<std::string>&,
                                               const FunctionContainer<Nb>&,
                                               const FunctionContainer<taylor<Nb, 1, 1>>&,
                                               const FunctionContainer<taylor<Nb, 2, 2>>&);

      std::vector<In> GetMCVectorFromTerm(const std::vector<std::string>&, const std::vector<std::string>&);
      
   public:
      
      MidasPotential() {}
      /**
       *
       **/
      MidasPotential( const std::string& aFileName, 
                      const std::vector<std::string>& aModeList ) 
         : mFileName(aFileName)
      {
         InitializeContainers(aModeList);
      }
      
      void ReReadOperator(const std::vector<std::string>& aModeList)
      {
         InitializeContainers(aModeList);
      }
      
      void CleanAndReread(const std::string& aFileName, 
                          const std::vector<std::string>& aModeList)
      {
         mFileName = aFileName;
         InitializeContainers(aModeList);
      }
      
      /**
       * Evaluate the potential for a set of points and return the values in the third argument
       **/
      void EvaluatePotential(const std::vector<In>&, const std::vector<MidasVector>&, MidasVector&) const;
      
      /**
       * Evaluate the potential for a given point and return its value
       **/
      Nb EvaluatePotential(const std::vector<In>&, const MidasVector&) const;
      
      // Interface for pes/ItGridBoxes.cc
      std::vector<Nb> CoefficientsForModeCombi(  const std::vector<In>&)  const;
      std::vector<std::vector<MidasFunctionWrapper<Nb>>> FunctionsForModeCombi(const std::vector<In>&) const;
      std::vector<Nb> ScalingFactorsForModeCombi(const std::vector<In>&) const;

      
      // Interface for FilePot
      Nb EvaluatePotential(MidasVector aDisplacement) const;
      MidasVector EvaluateGradient(MidasVector aDisplacement) const;
      MidasMatrix EvaluateHessian(MidasVector aDisplacement) const;
};

#endif /* MIDASPOTENTIAL_H_INCLUDED */
