/**
************************************************************************
*
*  @file                MorsePot.h
* 
*  Created:             01/10/2007
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class Morse for computing the Morse
*                       Potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#ifndef MORSEPOT_H_INCLUDED
#define MORSEPOT_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

class Nuclei;

/**
 * @class MorsePot
 * @brief Implements morse potential
 *        V(r) = D_e * (1 - exp(-a*(r - r_e)) )^2
 **/
class MorsePot 
   : public ModelPot
{
   private:
      //! Stores potential Definitions
      std::vector<std::string> mMorsePotDefs;

      //! Depths of the potentials
      std::vector<Nb> mDes;         
      //! Widths of the potentials
      std::vector<Nb> mAs;                 
      //! Equilibrium distances
      std::vector<Nb> mReqs;            
      
      //! Set the potential parameters from a string
      void SetFromString(const std::string& aAux);
      
      //! Evaluate MorsePotential interface function.
      Nb EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const; 
      
      //! Evaluate MorsePotential interface function.
      MidasVector EvalDerImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const; 
      
      //! Evaluate MorsePotential interface function.
      MidasMatrix EvalHessImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const; 

   public:
      //! Constructor from string
      MorsePot(const ModelPotInfo&);           
      
      //! Get the type of modelpot
      std::string Type() const { return "MORSEPOT"; }    
      
      //! Evaluate the potential on a given point
      Nb EvalPot(Nb x, In i) const; 

      //! Evaluate derivative of the potential on a given point
      MidasVector EvalDer(Nb x, In i) const; 
      
      //! Evaluate derivative of the potential on a given point
      MidasMatrix EvalHess(Nb x, In i) const; 
      
      //! Output overloading
      friend ostream& operator<<(ostream& arOut, const MorsePot& arPot);   
};

/**
 *
 **/
ostream& operator<<(ostream& arOut, const MorsePot& arPot);   ///< Output overloading

#endif /* MORSEPOT_H_INCLUDED */
