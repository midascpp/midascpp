/**
*************************************************************************
*
* @file                AmmModelPot.cc
*
* Created:             02/04/2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for NH3 potential
*                      Parameterization for CBS**-5 potential
*                      (S. N. Yurchenko et al. JCP 123(2005), 134308)
*
* Last modified: Tue Jun 30, 2009  09:46AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <iostream>
#include <string>

// midas headers
#include "potentials/AmmModelPot.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "nuclei/Nuclei.h"

//Utility functions
namespace midas::potential::detail
{
   /**
   * Small utility routine for swapping numbers
   * */
   void shft3(Nb &a, Nb &b, Nb &c, const Nb d)
   {
      a=b;
      b=c;
      c=d;
   }
}

/**
* @brief Default constructor
*        By default consider a model for the NH3 inversion problem
*        as fitted in the article above
**/
AmmModelPot::AmmModelPot()
   : ModelPot({"GROUND_STATE_ENERGY"}, {potentials::provides::value})
   , mParMax(307)
   , mPotType(I_0)
{
   InitParms();
   //mParam.SetNewSize(mParMax,false);
   //mIvar.resize(mParMax);
   //mParamLab.resize(mParMax);
}

/**
 * Initialize the parameter set
 * */
void AmmModelPot::InitParms()
{
   mParam.SetNewSize(mParMax,false);
   mIvar.resize(mParMax);
   mParamLab.resize(mParMax);
   for (In i=I_0; i<mIvar.size(); i++)
      mIvar[i]=I_0;
   //now fill the other arrays
   //label for the parameters
   mParamLab[0] ="rho";
   mParamLab[1] ="re";
   mParamLab[2] ="a";
   mParamLab[3] ="ve";
   mParamLab[4] ="f1a";
   mParamLab[5] ="f2a";
   mParamLab[6] ="f3a";
   mParamLab[7] ="f4a";
   mParamLab[8] ="f5a";
   mParamLab[9] ="f6a";
   mParamLab[10] ="f7a";
   mParamLab[11] ="f8a";
   mParamLab[12] ="f1a1";
   mParamLab[13] ="f2a1";
   mParamLab[14] ="f3a1";
   mParamLab[15] ="f4a1";
   mParamLab[16] ="f5a1";
   mParamLab[17] ="f6a1";
   mParamLab[18] ="f0a11";
   mParamLab[19] ="f1a11";
   mParamLab[20] ="f2a11";
   mParamLab[21] ="f3a11";
   mParamLab[22] ="f4a11";
   mParamLab[23] ="f0a12";
   mParamLab[24] ="f1a12";
   mParamLab[25] ="f2a12";
   mParamLab[26] ="f3a12";
   mParamLab[27] ="f4a12";
   mParamLab[28] ="f0a14";
   mParamLab[29] ="f1a14";
   mParamLab[30] ="f2a14";
   mParamLab[31] ="f3a14";
   mParamLab[32] ="f4a14";
   mParamLab[33] ="f0a44";
   mParamLab[34] ="f1a44";
   mParamLab[35] ="f2a44";
   mParamLab[36] ="f3a44";
   mParamLab[37] ="f4a44";
   mParamLab[38] ="f0a111";
   mParamLab[39] ="f1a111";
   mParamLab[40] ="f2a111";
   mParamLab[41] ="f3a111";
   mParamLab[42] ="f0a112";
   mParamLab[43] ="f1a112";
   mParamLab[44] ="f2a112";
   mParamLab[45] ="f3a112";
   mParamLab[46] ="f0a114";
   mParamLab[47] ="f1a114";
   mParamLab[48] ="f2a114";
   mParamLab[49] ="f3a114";
   mParamLab[50] ="f0a123";
   mParamLab[51] ="f1a123";
   mParamLab[52] ="f2a123";
   mParamLab[53] ="f3a123";
   mParamLab[54] ="f0a124";
   mParamLab[55] ="f1a124";
   mParamLab[56] ="f2a124";
   mParamLab[57] ="f3a124";
   mParamLab[58] ="f0a144";
   mParamLab[59] ="f1a144";
   mParamLab[60] ="f2a144";
   mParamLab[61] ="f3a144";
   mParamLab[62] ="f0a155";
   mParamLab[63] ="f1a155";
   mParamLab[64] ="f2a155";
   mParamLab[65] ="f3a155";
   mParamLab[66] ="f0a455";
   mParamLab[67] ="f1a455";
   mParamLab[68] ="f2a455";
   mParamLab[69] ="f3a455";
   mParamLab[70] ="f0a1111";
   mParamLab[71] ="f1a1111";
   mParamLab[72] ="f2a1111";
   mParamLab[73] ="f0a1112";
   mParamLab[74] ="f1a1112";
   mParamLab[75] ="f2a1112";
   mParamLab[76] ="f0a1114";
   mParamLab[77] ="f1a1114";
   mParamLab[78] ="f2a1114";
   mParamLab[79] ="f0a1122";
   mParamLab[80] ="f1a1122";
   mParamLab[81] ="f2a1122";
   mParamLab[82] ="f0a1123";
   mParamLab[83] ="f1a1123";
   mParamLab[84] ="f2a1123";
   mParamLab[85] ="f0a1124";
   mParamLab[86] ="f1a1124";
   mParamLab[87] ="f2a1124";
   mParamLab[88] ="f0a1125";
   mParamLab[89] ="f1a1125";
   mParamLab[90] ="f2a1125";
   mParamLab[91] ="f0a1144";
   mParamLab[92] ="f1a1144";
   mParamLab[93] ="f2a1144";
   mParamLab[94] ="f0a1155";
   mParamLab[95] ="f1a1155";
   mParamLab[96] ="f2a1155";
   mParamLab[97] ="f0a1244";
   mParamLab[98] ="f1a1244";
   mParamLab[99] ="f2a1244";
   mParamLab[100] ="f0a1255";
   mParamLab[101] ="f1a1255";
   mParamLab[102] ="f2a1255";
   mParamLab[103] ="f0a1444";
   mParamLab[104] ="f1a1444";
   mParamLab[105] ="f2a1444";
   mParamLab[106] ="f0a1455";
   mParamLab[107] ="f1a1455";
   mParamLab[108] ="f2a1455";
   mParamLab[109] ="f0a4444";
   mParamLab[110] ="f1a4444";
   mParamLab[111] ="f2a4444";
   mParamLab[112] ="f0a44444";
   mParamLab[113] ="f1a44444";
   mParamLab[114] ="f2a44444";
   mParamLab[115] ="f0a33455";
   mParamLab[116] ="f1a33455";
   mParamLab[117] ="f2a33455";
   mParamLab[118] ="f0a33445";
   mParamLab[119] ="f1a33445";
   mParamLab[120] ="f2a33445";
   mParamLab[121] ="f0a33345";
   mParamLab[122] ="f1a33345";
   mParamLab[123] ="f2a33345";
   mParamLab[124] ="f0a33344";
   mParamLab[125] ="f1a33344";
   mParamLab[126] ="f2a33344";
   mParamLab[127] ="f0a33334";
   mParamLab[128] ="f1a33334";
   mParamLab[129] ="f2a33334";
   mParamLab[130] ="f0a33333";
   mParamLab[131] ="f1a33333";
   mParamLab[132] ="f2a33333";
   mParamLab[133] ="f0a25555";
   mParamLab[134] ="f1a25555";
   mParamLab[135] ="f2a25555";
   mParamLab[136] ="f0a24455";
   mParamLab[137] ="f1a24455";
   mParamLab[138] ="f2a24455";
   mParamLab[139] ="f0a24445";
   mParamLab[140] ="f1a24445";
   mParamLab[141] ="f2a24445";
   mParamLab[142] ="f0a23333";
   mParamLab[143] ="f1a23333";
   mParamLab[144] ="f2a23333";
   mParamLab[145] ="f0a13455";
   mParamLab[146] ="f1a13455";
   mParamLab[147] ="f2a13455";
   mParamLab[148] ="f0a13445";
   mParamLab[149] ="f1a13445";
   mParamLab[150] ="f2a13445";
   mParamLab[151] ="f0a13345";
   mParamLab[152] ="f1a13345";
   mParamLab[153] ="f2a13345";
   mParamLab[154] ="f0a12355";
   mParamLab[155] ="f1a12355";
   mParamLab[156] ="f2a12355";
   mParamLab[157] ="f0a11334";
   mParamLab[158] ="f1a11334";
   mParamLab[159] ="f2a11334";
   mParamLab[160] ="f0a11333";
   mParamLab[161] ="f1a11333";
   mParamLab[162] ="f2a11333";
   mParamLab[163] ="f0a11255";
   mParamLab[164] ="f1a11255";
   mParamLab[165] ="f2a11255";
   mParamLab[166] ="f0a11245";
   mParamLab[167] ="f1a11245";
   mParamLab[168] ="f2a11245";
   mParamLab[169] ="f0a11234";
   mParamLab[170] ="f1a11234";
   mParamLab[171] ="f2a11234";
   mParamLab[172] ="f0a11233";
   mParamLab[173] ="f1a11233";
   mParamLab[174] ="f2a11233";
   mParamLab[175] ="f0a11135";
   mParamLab[176] ="f1a11135";
   mParamLab[177] ="f2a11135";
   mParamLab[178] ="f0a11134";
   mParamLab[179] ="f1a11134";
   mParamLab[180] ="f2a11134";
   mParamLab[181] ="f0a11123";
   mParamLab[182] ="f1a11123";
   mParamLab[183] ="f2a11123";
   mParamLab[184] ="f0a555555";
   mParamLab[185] ="f1a555555";
   mParamLab[186] ="f2a555555";
   mParamLab[187] ="f0a444444";
   mParamLab[188] ="f1a444444";
   mParamLab[189] ="f2a444444";
   mParamLab[190] ="f0a335555";
   mParamLab[191] ="f1a335555";
   mParamLab[192] ="f2a335555";
   mParamLab[193] ="f0a334455";
   mParamLab[194] ="f1a334455";
   mParamLab[195] ="f2a334455";
   mParamLab[196] ="f0a334445";
   mParamLab[197] ="f1a334445";
   mParamLab[198] ="f2a334445";
   mParamLab[199] ="f0a333555";
   mParamLab[200] ="f1a333555";
   mParamLab[201] ="f2a333555";
   mParamLab[202] ="f0a333333";
   mParamLab[203] ="f1a333333";
   mParamLab[204] ="f2a333333";
   mParamLab[205] ="f0a244555";
   mParamLab[206] ="f1a244555";
   mParamLab[207] ="f2a244555";
   mParamLab[208] ="f0a244455";
   mParamLab[209] ="f1a244455";
   mParamLab[210] ="f2a244455";
   mParamLab[211] ="f0a233445";
   mParamLab[212] ="f1a233445";
   mParamLab[213] ="f2a233445";
   mParamLab[214] ="f0a233444";
   mParamLab[215] ="f1a233444";
   mParamLab[216] ="f2a233444";
   mParamLab[217] ="f0a233345";
   mParamLab[218] ="f1a233345";
   mParamLab[219] ="f2a233345";
   mParamLab[220] ="f0a233344";
   mParamLab[221] ="f1a233344";
   mParamLab[222] ="f2a233344";
   mParamLab[223] ="f0a233335";
   mParamLab[224] ="f1a233335";
   mParamLab[225] ="f2a233335";
   mParamLab[226] ="f0a223355";
   mParamLab[227] ="f1a223355";
   mParamLab[228] ="f2a223355";
   mParamLab[229] ="f0a222335";
   mParamLab[230] ="f1a222335";
   mParamLab[231] ="f2a222335";
   mParamLab[232] ="f0a222334";
   mParamLab[233] ="f1a222334";
   mParamLab[234] ="f2a222334";
   mParamLab[235] ="f0a222333";
   mParamLab[236] ="f1a222333";
   mParamLab[237] ="f2a222333";
   mParamLab[238] ="f0a222255";
   mParamLab[239] ="f1a222255";
   mParamLab[240] ="f2a222255";
   mParamLab[241] ="f0a222245";
   mParamLab[242] ="f1a222245";
   mParamLab[243] ="f2a222245";
   mParamLab[244] ="f0a222233";
   mParamLab[245] ="f1a222233";
   mParamLab[246] ="f2a222233";
   mParamLab[247] ="f0a222224";
   mParamLab[248] ="f1a222224";
   mParamLab[249] ="f2a222224";
   mParamLab[250] ="f0a145555";
   mParamLab[251] ="f1a145555";
   mParamLab[252] ="f2a145555";
   mParamLab[253] ="f0a134444";
   mParamLab[254] ="f1a134444";
   mParamLab[255] ="f2a134444";
   mParamLab[256] ="f0a133444";
   mParamLab[257] ="f1a133444";
   mParamLab[258] ="f2a133444";
   mParamLab[259] ="f0a133345";
   mParamLab[260] ="f1a133345";
   mParamLab[261] ="f2a133345";
   mParamLab[262] ="f0a133334";
   mParamLab[263] ="f1a133334";
   mParamLab[264] ="f2a133334";
   mParamLab[265] ="f0a133333";
   mParamLab[266] ="f1a133333";
   mParamLab[267] ="f2a133333";
   mParamLab[268] ="f0a124555";
   mParamLab[269] ="f1a124555";
   mParamLab[270] ="f2a124555";
   mParamLab[271] ="f0a124455";
   mParamLab[272] ="f1a124455";
   mParamLab[273] ="f2a124455";
   mParamLab[274] ="f0a123455";
   mParamLab[275] ="f1a123455";
   mParamLab[276] ="f2a123455";
   mParamLab[277] ="f0a123345";
   mParamLab[278] ="f1a123345";
   mParamLab[279] ="f2a123345";
   mParamLab[280] ="f0a113555";
   mParamLab[281] ="f1a113555";
   mParamLab[282] ="f2a113555";
   mParamLab[283] ="f0a113345";
   mParamLab[284] ="f1a113345";
   mParamLab[285] ="f2a113345";
   mParamLab[286] ="f0a112355";
   mParamLab[287] ="f1a112355";
   mParamLab[288] ="f2a112355";
   mParamLab[289] ="f0a112335";
   mParamLab[290] ="f1a112335";
   mParamLab[291] ="f2a112335";
   mParamLab[292] ="f0a112233";
   mParamLab[293] ="f1a112233";
   mParamLab[294] ="f2a112233";
   mParamLab[295] ="f0a111444";
   mParamLab[296] ="f1a111444";
   mParamLab[297] ="f2a111444";
   mParamLab[298] ="f0a111234";
   mParamLab[299] ="f1a111234";
   mParamLab[300] ="f2a111234";
   mParamLab[301] ="f0a111233";
   mParamLab[302] ="f1a111233";
   mParamLab[303] ="f2a111233";
   mParamLab[304] ="f0a111123";
   mParamLab[305] ="f1a111123";
   mParamLab[306] ="f2a111123";

   //parameters of the potential
   if (mPotType==I_0) 
   {
      mParam[0] =112.0966;
      mParam[1] =1.0103131;
      mParam[2] =2.15;
      mParam[3] =-12419377.330;
      mParam[4] =0.00;
      mParam[5] =324077.;
      mParam[6] =-401792.;
      mParam[7] =1133218.;
      mParam[8] =-2662929.;
      mParam[9] =4561862.;
      mParam[10] =0.00;
      mParam[11] =0.00;
      mParam[12] =-33671.7;
      mParam[13] =41558.;
      mParam[14] =-349715.;
      mParam[15] =1246471.;
      mParam[16] =-2439730.;
      mParam[17] =0.00;
      mParam[18] =38730.1;
      mParam[19] =-17346.;
      mParam[20] =57866.;
      mParam[21] =-462520.;
      mParam[22] =979992.;
      mParam[23] =-403.29;
      mParam[24] =4673.;
      mParam[25] =41577.;
      mParam[26] =-199083.;
      mParam[27] =606382.;
      mParam[28] =-3651.0;
      mParam[29] =-17081.;
      mParam[30] =-44594.;
      mParam[31] =156439.;
      mParam[32] =-427396.;
      mParam[33] =16833.16;
      mParam[34] =67883.;
      mParam[35] =-110704.;
      mParam[36] =324179.;
      mParam[37] =-536600.;
      mParam[38] =277.2;
      mParam[39] =-9581.;
      mParam[40] =61987.;
      mParam[41] =-262799.;
      mParam[42] =-291.8;
      mParam[43] =2294.;
      mParam[44] =26099.;
      mParam[45] =-103127.;
      mParam[46] =-2068.4;
      mParam[47] =-6903.;
      mParam[48] =-77502.;
      mParam[49] =233336.;
      mParam[50] =-186.3;
      mParam[51] =4547.;
      mParam[52] =16600.;
      mParam[53] =-88441.;
      mParam[54] =1956.59;
      mParam[55] =5405.;
      mParam[56] =-2685.;
      mParam[57] =0.00;
      mParam[58] =-1648.6;
      mParam[59] =-6830.;
      mParam[60] =-19840.;
      mParam[61] =85887.;
      mParam[62] =-2938.1;
      mParam[63] =-9414.;
      mParam[64] =-14906.;
      mParam[65] =-119102.;
      mParam[66] =1551.7;
      mParam[67] =-57777.;
      mParam[68] =35451.;
      mParam[69] =388038.;
      mParam[70] =3694.9;
      mParam[71] =-6990.;
      mParam[72] =19038.;
      mParam[73] =-545.7;
      mParam[74] =0.00;
      mParam[75] =14424.;
      mParam[76] =-678.5;
      mParam[77] =0.00;
      mParam[78] =-59594.;
      mParam[79] =-189.9;
      mParam[80] =0.00;
      mParam[81] =14272.;
      mParam[82] =-254.3;
      mParam[83] =2729.;
      mParam[84] =0.00;
      mParam[85] =895.1;
      mParam[86] =2070.;
      mParam[87] =-7392.;
      mParam[88] =1399.4;
      mParam[89] =5090.;
      mParam[90] =0.00;
      mParam[91] =-1150.3;
      mParam[92] =-7205.;
      mParam[93] =0.00;
      mParam[94] =-2615.7;
      mParam[95] =-8539.;
      mParam[96] =0.00;
      mParam[97] =508.2;
      mParam[98] =0.00;
      mParam[99] =-27657.;
      mParam[100] =1321.9;
      mParam[101] =921.;
      mParam[102] =0.00;
      mParam[103] =-765.1;
      mParam[104] =-3922.;
      mParam[105] =0.00;
      mParam[106] =-1581.9;
      mParam[107] =-8474.;
      mParam[108] =-93060.;
      mParam[109] =395.6;
      mParam[110] =8421.;
      mParam[111] =13895.;
      mParam[112] =-127.4;
      mParam[113] =1827.;
      mParam[114] =9830.;
      mParam[115] =-714.;
      mParam[116] =0.00;
      mParam[117] =-108147.;
      mParam[118] =809.8;
      mParam[119] =3330.;
      mParam[120] =0.00;
      mParam[121] =1791.;
      mParam[122] =4219.;
      mParam[123] =0.00;
      mParam[124] =-2410.4;
      mParam[125] =-11427.;
      mParam[126] =50287.;
      mParam[127] =-183.1;
      mParam[128] =0.00;
      mParam[129] =0.00;
      mParam[130] =1749.4;
      mParam[131] =0.00;
      mParam[132] =0.00;
      mParam[133] =-52.8;
      mParam[134] =-1133.;
      mParam[135] =-17384.;
      mParam[136] =584.9;
      mParam[137] =0.00;
      mParam[138] =-53324.;
      mParam[139] =1070.1;
      mParam[140] =4655.;
      mParam[141] =-37568.;
      mParam[142] =-374.9;
      mParam[143] =0.00;
      mParam[144] =0.00;
      mParam[145] =1127.;
      mParam[146] =7287.;
      mParam[147] =27647.;
      mParam[148] =-395.0;
      mParam[149] =-2037.;
      mParam[150] =21076.;
      mParam[151] =-679.2;
      mParam[152] =0.00;
      mParam[153] =0.00;
      mParam[154] =221.;
      mParam[155] =0.00;
      mParam[156] =0.00;
      mParam[157] =624.3;
      mParam[158] =2211.;
      mParam[159] =0.00;
      mParam[160] =0.00;
      mParam[161] =0.00;
      mParam[162] =0.00;
      mParam[163] =807.3;
      mParam[164] =0.00;
      mParam[165] =-27721.;
      mParam[166] =0.00;
      mParam[167] =-3018.;
      mParam[168] =43059.;
      mParam[169] =0.00;
      mParam[170] =0.00;
      mParam[171] =0.00;
      mParam[172] =0.00;
      mParam[173] =0.00;
      mParam[174] =0.00;
      mParam[175] =0.00;
      mParam[176] =-7232.;
      mParam[177] =0.00;
      mParam[178] =245.9;
      mParam[179] =0.00;
      mParam[180] =0.00;
      mParam[181] =-400.;
      mParam[182] =0.00;
      mParam[183] =0.00;
      mParam[184] =0.00;
      mParam[185] =2257.;
      mParam[186] =24161.;
      mParam[187] =127.07;
      mParam[188] =4563.;
      mParam[189] =42425.;
      mParam[190] =121.1;
      mParam[191] =0.00;
      mParam[192] =-19542.;
      mParam[193] =867.;
      mParam[194] =4471.;
      mParam[195] =0.00;
      mParam[196] =-1066.;
      mParam[197] =-6945.;
      mParam[198] =0.00;
      mParam[199] =560.;
      mParam[200] =-4979.;
      mParam[201] =0.00;
      mParam[202] =619.;
      mParam[203] =7177.;
      mParam[204] =0.00;
      mParam[205] =-648.;
      mParam[206] =-14993.;
      mParam[207] =0.00;
      mParam[208] =-493.8;
      mParam[209] =-8199.;
      mParam[210] =56324.;
      mParam[211] =1130.;
      mParam[212] =0.00;
      mParam[213] =0.00;
      mParam[214] =-440.;
      mParam[215] =-3746.;
      mParam[216] =0.00;
      mParam[217] =0.00;
      mParam[218] =0.00;
      mParam[219] =-144810.;
      mParam[220] =0.00;
      mParam[221] =0.00;
      mParam[222] =0.00;
      mParam[223] =0.00;
      mParam[224] =0.00;
      mParam[225] =0.00;
      mParam[226] =0.00;
      mParam[227] =11246.;
      mParam[228] =0.00;
      mParam[229] =0.00;
      mParam[230] =0.00;
      mParam[231] =0.00;
      mParam[232] =-578.;
      mParam[233] =0.00;
      mParam[234] =0.00;
      mParam[235] =0.00;
      mParam[236] =0.00;
      mParam[237] =0.00;
      mParam[238] =-1648.;
      mParam[239] =0.00;
      mParam[240] =0.00;
      mParam[241] =-2032.;
      mParam[242] =-34578.;
      mParam[243] =211698.;
      mParam[244] =0.00;
      mParam[245] =0.00;
      mParam[246] =0.00;
      mParam[247] =-458.;
      mParam[248] =5104.;
      mParam[249] =0.00;
      mParam[250] =-1719.;
      mParam[251] =-27172.;
      mParam[252] =82494.;
      mParam[253] =-336.8;
      mParam[254] =-4362.;
      mParam[255] =0.00;
      mParam[256] =661.;
      mParam[257] =0.00;
      mParam[258] =0.00;
      mParam[259] =0.00;
      mParam[260] =0.00;
      mParam[261] =0.00;
      mParam[262] =0.00;
      mParam[263] =0.00;
      mParam[264] =0.00;
      mParam[265] =0.00;
      mParam[266] =5970.;
      mParam[267] =-72281.;
      mParam[268] =1027.;
      mParam[269] =10494.;
      mParam[270] =0.00;
      mParam[271] =254.;
      mParam[272] =0.00;
      mParam[273] =0.00;
      mParam[274] =-733.;
      mParam[275] =0.00;
      mParam[276] =0.00;
      mParam[277] =0.00;
      mParam[278] =0.00;
      mParam[279] =0.00;
      mParam[280] =-828.;
      mParam[281] =-2148.;
      mParam[282] =0.00;
      mParam[283] =0.00;
      mParam[284] =0.00;
      mParam[285] =0.00;
      mParam[286] =0.00;
      mParam[287] =0.00;
      mParam[288] =0.00;
      mParam[289] =0.00;
      mParam[290] =0.00;
      mParam[291] =0.00;
      mParam[292] =0.00;
      mParam[293] =0.00;
      mParam[294] =0.00;
      mParam[295] =-266.;
      mParam[296] =0.00;
      mParam[297] =0.00;
      mParam[298] =0.00;
      mParam[299] =0.00;
      mParam[300] =0.00;
      mParam[301] =0.00;
      mParam[302] =0.00;
      mParam[303] =0.00;
      mParam[304] =0.00;
      mParam[305] =0.00;
      mParam[306] =0.00;
   } //use CBS**-5 potential
   else if (mPotType==I_1) 
   {
      mParam[  0]=0.1120966000000000e+03;
      mParam[  1]=0.1010313100000000e+01;
      mParam[  2]=0.2150000000000000e+01;
      mParam[  3]=-.1241937728500000e+08;
      mParam[  4]=0.0000000000000000e+00;
      mParam[  5]=0.3244220000000000e+06;
      mParam[  6]=-.4071380000000000e+06;
      mParam[  7]=0.1080994000000000e+07;
      mParam[  8]=-.1428982000000000e+07;
      mParam[  9]=0.0000000000000000e+00;
      mParam[ 10]=0.0000000000000000e+00;
      mParam[ 11]=0.0000000000000000e+00;
      mParam[ 12]=-.3372790000000000e+05;
      mParam[ 13]=0.4421500000000000e+05;
      mParam[ 14]=-.3357710000000000e+06;
      mParam[ 15]=0.6602320000000000e+06;
      mParam[ 16]=0.0000000000000000e+00;
      mParam[ 17]=0.0000000000000000e+00;
      mParam[ 18]=0.3873702000000000e+05;
      mParam[ 19]=-.1738500000000000e+05;
      mParam[ 20]=0.5398200000000000e+05;
      mParam[ 21]=-.4445780000000000e+06;
      mParam[ 22]=0.9461520000000000e+06;
      mParam[ 23]=-.3998700000000000e+03;
      mParam[ 24]=0.4630000000000000e+04;
      mParam[ 25]=0.4152000000000000e+05;
      mParam[ 26]=-.1884060000000000e+06;
      mParam[ 27]=0.5681370000000000e+06;
      mParam[ 28]=-.3662540000000000e+04;
      mParam[ 29]=-.1641800000000000e+05;
      mParam[ 30]=-.5027500000000000e+05;
      mParam[ 31]=0.9881200000000000e+05;
      mParam[ 32]=0.0000000000000000e+00;
      mParam[ 33]=0.1681990000000000e+05;
      mParam[ 34]=0.6799100000000000e+05;
      mParam[ 35]=-.1115950000000000e+06;
      mParam[ 36]=0.4493500000000000e+06;
      mParam[ 37]=-.1615658000000000e+07;
      mParam[ 38]=0.2360000000000000e+03;
      mParam[ 39]=-.9644000000000000e+04;
      mParam[ 40]=0.6157500000000000e+05;
      mParam[ 41]=-.2432320000000000e+06;
      mParam[ 42]=-.2830600000000000e+03;
      mParam[ 43]=0.2221400000000000e+04;
      mParam[ 44]=0.2337100000000000e+05;
      mParam[ 45]=-.9192500000000000e+05;
      mParam[ 46]=-.2045000000000000e+04;
      mParam[ 47]=-.7800000000000000e+04;
      mParam[ 48]=-.6198400000000000e+05;
      mParam[ 49]=0.1720410000000000e+06;
      mParam[ 50]=-.2956000000000000e+03;
      mParam[ 51]=0.4640000000000000e+04;
      mParam[ 52]=0.1979500000000000e+05;
      mParam[ 53]=-.1224450000000000e+06;
      mParam[ 54]=0.1917940000000000e+04;
      mParam[ 55]=0.6090000000000000e+04;
      mParam[ 56]=-.7625000000000000e+04;
      mParam[ 57]=0.3629700000000000e+05;
      mParam[ 58]=-.1644000000000000e+04;
      mParam[ 59]=-.7108000000000000e+04;
      mParam[ 60]=-.3184600000000000e+05;
      mParam[ 61]=0.2926890000000000e+06;
      mParam[ 62]=-.2876600000000000e+04;
      mParam[ 63]=-.1037300000000000e+05;
      mParam[ 64]=-.5586500000000000e+05;
      mParam[ 65]=0.3529900000000000e+06;
      mParam[ 66]=0.1455400000000000e+04;
      mParam[ 67]=-.5894800000000000e+05;
      mParam[ 68]=0.1484720000000000e+06;
      mParam[ 69]=-.5831380000000000e+06;
      mParam[ 70]=0.3738400000000000e+04;
      mParam[ 71]=-.6268000000000000e+04;
      mParam[ 72]=0.3231000000000000e+05;
      mParam[ 73]=-.5538000000000000e+03;
      mParam[ 74]=0.1149000000000000e+04;
      mParam[ 75]=0.0000000000000000e+00;
      mParam[ 76]=-.4620000000000000e+03;
      mParam[ 77]=-.6090000000000000e+04;
      mParam[ 78]=-.3352600000000000e+05;
      mParam[ 79]=-.1856000000000000e+03;
      mParam[ 80]=0.9000000000000000e+03;
      mParam[ 81]=0.1268400000000000e+05;
      mParam[ 82]=-.1759000000000000e+03;
      mParam[ 83]=0.2405000000000000e+04;
      mParam[ 84]=0.0000000000000000e+00;
      mParam[ 85]=0.8429000000000000e+03;
      mParam[ 86]=0.3132000000000000e+04;
      mParam[ 87]=-.8760000000000000e+04;
      mParam[ 88]=0.1547800000000000e+04;
      mParam[ 89]=0.4588000000000000e+04;
      mParam[ 90]=0.0000000000000000e+00;
      mParam[ 91]=-.1247900000000000e+04;
      mParam[ 92]=-.5767000000000000e+04;
      mParam[ 93]=0.0000000000000000e+00;
      mParam[ 94]=-.2715700000000000e+04;
      mParam[ 95]=-.1209200000000000e+05;
      mParam[ 96]=0.3062300000000000e+05;
      mParam[ 97]=0.3587000000000000e+03;
      mParam[ 98]=0.0000000000000000e+00;
      mParam[ 99]=-.1237600000000000e+05;
      mParam[100]=0.1280900000000000e+04;
      mParam[101]=0.1695000000000000e+04;
      mParam[102]=0.0000000000000000e+00;
      mParam[103]=-.7521000000000000e+03;
      mParam[104]=-.6379000000000000e+04;
      mParam[105]=0.2231900000000000e+05;
      mParam[106]=-.1930100000000000e+04;
      mParam[107]=-.1513400000000000e+05;
      mParam[108]=0.9086600000000000e+05;
      mParam[109]=0.4498000000000000e+03;
      mParam[110]=0.7496000000000000e+04;
      mParam[111]=0.0000000000000000e+00;
      mParam[112]=-.2800000000000000e+03;
      mParam[113]=0.4496000000000000e+04;
      mParam[114]=0.0000000000000000e+00;
      mParam[115]=-.5211000000000000e+03;
      mParam[116]=0.0000000000000000e+00;
      mParam[117]=-.1325000000000000e+06;
      mParam[118]=0.8166000000000000e+03;
      mParam[119]=0.0000000000000000e+00;
      mParam[120]=0.0000000000000000e+00;
      mParam[121]=0.1476000000000000e+04;
      mParam[122]=0.3277000000000000e+04;
      mParam[123]=0.0000000000000000e+00;
      mParam[124]=-.2183000000000000e+04;
      mParam[125]=-.3027000000000000e+04;
      mParam[126]=-.8418900000000000e+05;
      mParam[127]=0.0000000000000000e+00;
      mParam[128]=0.0000000000000000e+00;
      mParam[129]=0.0000000000000000e+00;
      mParam[130]=0.1891000000000000e+04;
      mParam[131]=0.0000000000000000e+00;
      mParam[132]=0.0000000000000000e+00;
      mParam[133]=-.9790000000000001e+02;
      mParam[134]=0.0000000000000000e+00;
      mParam[135]=0.0000000000000000e+00;
      mParam[136]=0.4180000000000000e+03;
      mParam[137]=0.0000000000000000e+00;
      mParam[138]=0.0000000000000000e+00;
      mParam[139]=0.7580000000000000e+03;
      mParam[140]=0.5605000000000000e+04;
      mParam[141]=0.0000000000000000e+00;
      mParam[142]=-.3652000000000000e+03;
      mParam[143]=0.0000000000000000e+00;
      mParam[144]=0.0000000000000000e+00;
      mParam[145]=0.1058000000000000e+04;
      mParam[146]=0.6803000000000000e+04;
      mParam[147]=0.0000000000000000e+00;
      mParam[148]=-.4672000000000000e+03;
      mParam[149]=0.0000000000000000e+00;
      mParam[150]=0.0000000000000000e+00;
      mParam[151]=-.6534000000000000e+03;
      mParam[152]=0.0000000000000000e+00;
      mParam[153]=0.0000000000000000e+00;
      mParam[154]=0.1168000000000000e+03;
      mParam[155]=0.0000000000000000e+00;
      mParam[156]=0.0000000000000000e+00;
      mParam[157]=0.6043000000000000e+03;
      mParam[158]=0.2735000000000000e+04;
      mParam[159]=0.0000000000000000e+00;
      mParam[160]=0.0000000000000000e+00;
      mParam[161]=0.0000000000000000e+00;
      mParam[162]=0.0000000000000000e+00;
      mParam[163]=0.6630000000000000e+03;
      mParam[164]=0.0000000000000000e+00;
      mParam[165]=0.0000000000000000e+00;
      mParam[166]=0.0000000000000000e+00;
      mParam[167]=0.0000000000000000e+00;
      mParam[168]=0.0000000000000000e+00;
      mParam[169]=0.0000000000000000e+00;
      mParam[170]=0.0000000000000000e+00;
      mParam[171]=0.0000000000000000e+00;
      mParam[172]=0.0000000000000000e+00;
      mParam[173]=0.0000000000000000e+00;
      mParam[174]=0.0000000000000000e+00;
      mParam[175]=-.4669000000000000e+03;
      mParam[176]=0.0000000000000000e+00;
      mParam[177]=0.0000000000000000e+00;
      mParam[178]=0.2040000000000000e+03;
      mParam[179]=0.0000000000000000e+00;
      mParam[180]=0.0000000000000000e+00;
      mParam[181]=0.0000000000000000e+00;
      mParam[182]=0.0000000000000000e+00;
      mParam[183]=0.0000000000000000e+00;
      mParam[184]=0.0000000000000000e+00;
      mParam[185]=0.1133900000000000e+05;
      mParam[186]=0.0000000000000000e+00;
      mParam[187]=0.0000000000000000e+00;
      mParam[188]=0.1471500000000000e+05;
      mParam[189]=0.0000000000000000e+00;
      mParam[190]=0.0000000000000000e+00;
      mParam[191]=0.0000000000000000e+00;
      mParam[192]=0.0000000000000000e+00;
      mParam[193]=0.0000000000000000e+00;
      mParam[194]=0.0000000000000000e+00;
      mParam[195]=0.0000000000000000e+00;
      mParam[196]=-.3544000000000000e+03;
      mParam[197]=0.0000000000000000e+00;
      mParam[198]=0.0000000000000000e+00;
      mParam[199]=0.0000000000000000e+00;
      mParam[200]=0.5457000000000000e+04;
      mParam[201]=0.0000000000000000e+00;
      mParam[202]=0.0000000000000000e+00;
      mParam[203]=0.0000000000000000e+00;
      mParam[204]=0.0000000000000000e+00;
      mParam[205]=0.8060000000000000e+03;
      mParam[206]=-.1815800000000000e+05;
      mParam[207]=0.0000000000000000e+00;
      mParam[208]=0.0000000000000000e+00;
      mParam[209]=-.1161000000000000e+05;
      mParam[210]=0.0000000000000000e+00;
      mParam[211]=0.1800000000000000e+03;
      mParam[212]=0.0000000000000000e+00;
      mParam[213]=0.0000000000000000e+00;
      mParam[214]=0.0000000000000000e+00;
      mParam[215]=-.2272000000000000e+04;
      mParam[216]=0.0000000000000000e+00;
      mParam[217]=-.8580000000000000e+03;
      mParam[218]=0.0000000000000000e+00;
      mParam[219]=0.0000000000000000e+00;
      mParam[220]=0.0000000000000000e+00;
      mParam[221]=0.0000000000000000e+00;
      mParam[222]=0.0000000000000000e+00;
      mParam[223]=0.0000000000000000e+00;
      mParam[224]=0.0000000000000000e+00;
      mParam[225]=0.0000000000000000e+00;
      mParam[226]=0.0000000000000000e+00;
      mParam[227]=0.0000000000000000e+00;
      mParam[228]=0.0000000000000000e+00;
      mParam[229]=0.0000000000000000e+00;
      mParam[230]=0.0000000000000000e+00;
      mParam[231]=0.0000000000000000e+00;
      mParam[232]=-.6340000000000000e+03;
      mParam[233]=0.0000000000000000e+00;
      mParam[234]=0.0000000000000000e+00;
      mParam[235]=0.0000000000000000e+00;
      mParam[236]=0.0000000000000000e+00;
      mParam[237]=0.0000000000000000e+00;
      mParam[238]=-.7410000000000000e+03;
      mParam[239]=0.0000000000000000e+00;
      mParam[240]=0.0000000000000000e+00;
      mParam[241]=-.1117000000000000e+04;
      mParam[242]=0.0000000000000000e+00;
      mParam[243]=0.0000000000000000e+00;
      mParam[244]=0.0000000000000000e+00;
      mParam[245]=0.0000000000000000e+00;
      mParam[246]=0.0000000000000000e+00;
      mParam[247]=0.3510000000000000e+03;
      mParam[248]=-.6604000000000000e+04;
      mParam[249]=0.0000000000000000e+00;
      mParam[250]=0.0000000000000000e+00;
      mParam[251]=-.2468200000000000e+05;
      mParam[252]=0.0000000000000000e+00;
      mParam[253]=0.0000000000000000e+00;
      mParam[254]=-.8761000000000000e+04;
      mParam[255]=0.0000000000000000e+00;
      mParam[256]=-.1107000000000000e+03;
      mParam[257]=0.0000000000000000e+00;
      mParam[258]=0.0000000000000000e+00;
      mParam[259]=0.0000000000000000e+00;
      mParam[260]=0.0000000000000000e+00;
      mParam[261]=0.0000000000000000e+00;
      mParam[262]=0.0000000000000000e+00;
      mParam[263]=0.0000000000000000e+00;
      mParam[264]=0.0000000000000000e+00;
      mParam[265]=0.0000000000000000e+00;
      mParam[266]=0.0000000000000000e+00;
      mParam[267]=0.0000000000000000e+00;
      mParam[268]=0.0000000000000000e+00;
      mParam[269]=0.1642500000000000e+05;
      mParam[270]=0.0000000000000000e+00;
      mParam[271]=0.0000000000000000e+00;
      mParam[272]=0.0000000000000000e+00;
      mParam[273]=0.0000000000000000e+00;
      mParam[274]=0.0000000000000000e+00;
      mParam[275]=0.0000000000000000e+00;
      mParam[276]=0.0000000000000000e+00;
      mParam[277]=0.0000000000000000e+00;
      mParam[278]=0.0000000000000000e+00;
      mParam[279]=0.0000000000000000e+00;
      mParam[280]=0.0000000000000000e+00;
      mParam[281]=0.0000000000000000e+00;
      mParam[282]=0.0000000000000000e+00;
      mParam[283]=0.0000000000000000e+00;
      mParam[284]=0.0000000000000000e+00;
      mParam[285]=0.0000000000000000e+00;
      mParam[286]=0.0000000000000000e+00;
      mParam[287]=0.0000000000000000e+00;
      mParam[288]=0.0000000000000000e+00;
      mParam[289]=0.0000000000000000e+00;
      mParam[290]=0.0000000000000000e+00;
      mParam[291]=0.0000000000000000e+00;
      mParam[292]=0.0000000000000000e+00;
      mParam[293]=0.0000000000000000e+00;
      mParam[294]=0.0000000000000000e+00;
      mParam[295]=0.0000000000000000e+00;
      mParam[296]=0.0000000000000000e+00;
      mParam[297]=0.0000000000000000e+00;
      mParam[298]=0.0000000000000000e+00;
      mParam[299]=0.0000000000000000e+00;
      mParam[300]=0.0000000000000000e+00;
      mParam[301]=0.0000000000000000e+00;
      mParam[302]=0.0000000000000000e+00;
      mParam[303]=0.0000000000000000e+00;
      mParam[304]=0.0000000000000000e+00;
      mParam[305]=0.0000000000000000e+00;
      mParam[306]=0.0000000000000000e+00;
   }
   else
   {
      MIDASERROR(" Potential type not implemented ");
   }
   mHasBeenInit=true;

   //print the parameters
   //PrintParms();
   return;
}
/**
 * Print out the parameters
 * */
void AmmModelPot::PrintParms()
{
   //print out the parameters
   Mout << " Parameter for the NH3 potential, from JCP, 123(2005), 134308 " << endl;
   Mout << endl;
   midas::stream::ScopedPrecision(22, Mout);
   for (In i=I_0; i<mParam.Size(); i++)
   {
      Mout.setf(ios::scientific);
      Mout.setf(ios::uppercase);
      Mout << "   " << mParamLab[i] << "=          " << mParam[i]  << endl;
   }
   Mout << endl;
}
/**
 * Evaluate the potential using the function
 * from S. N. Yurchenko et al., JCP 123(2005), 134308
 * Accept distances in Aangstrom and angles in degrees
 * In input, r1,r2,r3, theta1, theta2, theta3
 * Energy in a.u.
 * */
Nb AmmModelPot::Potential(MidasVector& arGeom) const
{

   Nb r14, r24, r34, alpha1, alpha2, alpha3;
   Nb y1, y2, y3, y4, y5, alpha/*, rho*/;
   //Nb v, v0, rhoe, v1, v2, v3, v4, v5, v6; v is unused variable
   Nb v0, rhoe, v1, v2, v3, v4, v5, v6;
   Nb sinrho, coro;
   //Nb fea, fea1, fea11, fea12, fea14, fea44; fea s unused variable                   
   Nb fea1, fea11, fea12, fea14, fea44;                    
   Nb fea111,  fea112,  fea114,  fea123;                   
   Nb fea124,  fea144,  fea155,  fea455;                   
   Nb fea1111, fea1112, fea1114, fea1122;                   
   Nb fea1123, fea1124, fea1125, fea1144;                   
   Nb fea1155, fea1244, fea1255, fea1444;                   
   Nb fea1455, fea4444;
   Nb fea44444,  fea33455,  fea33445, fea33345, fea33344;
   Nb fea33334,  fea33333,  fea25555, fea24455, fea24445,  fea23333;
   Nb fea13455,  fea13445,  fea13345, fea12355, fea11334,  fea11333;
   Nb fea11255,  fea11245,  fea11234, fea11233, fea11135,  fea11134;
   Nb fea11123,  fea555555, fea444444,fea335555,fea334455, fea334445;
   Nb fea333555, fea333333, fea244555,fea244455,fea233445, fea233444;
   Nb fea233345, fea233344, fea233335,fea223355,fea222335, fea222334;
   Nb fea222333, fea222255, fea222245,fea222233,fea222224, fea145555;
   Nb fea134444, fea133444, fea133345,fea133334,fea133333, fea124555;
   Nb fea124455, fea123455, fea123345,fea113555,fea113345, fea112355;
   Nb fea112335, fea112233, fea111444,fea111234,fea111233, fea111123;

   //Nb s1,s2,s3,s4,s5,s6; s6 is unused variable
   Nb s1,s2,s3,s4,s5;
                    
   Nb rhoedg,re14,aa1,ve;  
   Nb f1a,f2a,f3a,f4a,f5a,f6a,f7a,f8a; 
   Nb f1a1,f2a1,f3a1,f4a1,f5a1,f6a1;  
   Nb f0a11,f1a11,f2a11,f3a11,f4a11; 
   Nb f0a12,f1a12,f2a12,f3a12,f4a12; 
   Nb f0a14,f1a14,f2a14,f3a14,f4a14; 
   Nb f0a44,f1a44,f2a44,f3a44,f4a44; 
   Nb f0a111,f1a111,f2a111,f3a111; 
   Nb f0a112,f1a112,f2a112,f3a112; 
   Nb f0a114,f1a114,f2a114,f3a114; 
   Nb f0a123,f1a123,f2a123,f3a123; 
   Nb f0a124,f1a124,f2a124,f3a124; 
   Nb f0a144,f1a144,f2a144,f3a144; 
   Nb f0a155,f1a155,f2a155,f3a155; 
   Nb f0a455,f1a455,f2a455,f3a455; 
   Nb f0a1111,f1a1111,f2a1111; 
   Nb f0a1112,f1a1112,f2a1112; 
   Nb f0a1114,f1a1114,f2a1114; 
   Nb f0a1122,f1a1122,f2a1122; 
   Nb f0a1123,f1a1123,f2a1123; 
   Nb f0a1124,f1a1124,f2a1124; 
   Nb f0a1125,f1a1125,f2a1125; 
   Nb f0a1144,f1a1144,f2a1144; 
   Nb f0a1155,f1a1155,f2a1155; 
   Nb f0a1244,f1a1244,f2a1244; 
   Nb f0a1255,f1a1255,f2a1255; 
   Nb f0a1444,f1a1444,f2a1444; 
   Nb f0a1455,f1a1455,f2a1455; 
   Nb f0a4444,f1a4444,f2a4444; 
   Nb f0a44444 ,f1a44444;           
   Nb f2a44444 ,f0a33455 ,f1a33455 ,f2a33455 ,f0a33445 ,f1a33445;
   Nb f2a33445 ,f0a33345 ,f1a33345 ,f2a33345 ,f0a33344 ,f1a33344;
   Nb f2a33344 ,f0a33334 ,f1a33334 ,f2a33334 ,f0a33333 ,f1a33333;
   Nb f2a33333 ,f0a25555 ,f1a25555 ,f2a25555 ,f0a24455 ,f1a24455;
   Nb f2a24455 ,f0a24445 ,f1a24445 ,f2a24445 ,f0a23333 ,f1a23333;
   Nb f2a23333 ,f0a13455 ,f1a13455 ,f2a13455 ,f0a13445 ,f1a13445;
   Nb f2a13445 ,f0a13345 ,f1a13345 ,f2a13345 ,f0a12355 ,f1a12355;
   Nb f2a12355 ,f0a11334 ,f1a11334 ,f2a11334 ,f0a11333 ,f1a11333;
   Nb f2a11333 ,f0a11255 ,f1a11255 ,f2a11255 ,f0a11245 ,f1a11245;
   Nb f2a11245 ,f0a11234 ,f1a11234 ,f2a11234 ,f0a11233 ,f1a11233;
   Nb f2a11233 ,f0a11135 ,f1a11135 ,f2a11135 ,f0a11134 ,f1a11134;
   Nb f2a11134 ,f0a11123 ,f1a11123 ,f2a11123 ,f0a555555,f1a555555;
   Nb f2a555555,f0a444444,f1a444444,f2a444444,f0a335555,f1a335555;
   Nb f2a335555,f0a334455,f1a334455,f2a334455,f0a334445,f1a334445;
   Nb f2a334445,f0a333555,f1a333555,f2a333555,f0a333333,f1a333333;
   Nb f2a333333,f0a244555,f1a244555,f2a244555,f0a244455,f1a244455;
   Nb f2a244455,f0a233445,f1a233445,f2a233445,f0a233444,f1a233444;
   Nb f2a233444,f0a233345,f1a233345,f2a233345,f0a233344,f1a233344;
   Nb f2a233344,f0a233335,f1a233335,f2a233335,f0a223355,f1a223355;
   Nb f2a223355,f0a222335,f1a222335,f2a222335,f0a222334,f1a222334;
   Nb f2a222334,f0a222333,f1a222333,f2a222333,f0a222255,f1a222255;
   Nb f2a222255,f0a222245,f1a222245,f2a222245,f0a222233,f1a222233;
   Nb f2a222233,f0a222224,f1a222224,f2a222224,f0a145555,f1a145555;
   Nb f2a145555,f0a134444,f1a134444,f2a134444,f0a133444,f1a133444;
   Nb f2a133444,f0a133345,f1a133345,f2a133345,f0a133334,f1a133334;
   Nb f2a133334,f0a133333,f1a133333,f2a133333,f0a124555,f1a124555;
   Nb f2a124555,f0a124455,f1a124455,f2a124455,f0a123455,f1a123455;
   Nb f2a123455,f0a123345,f1a123345,f2a123345,f0a113555,f1a113555;
   Nb f2a113555,f0a113345,f1a113345,f2a113345,f0a112355,f1a112355;
   Nb f2a112355,f0a112335,f1a112335,f2a112335,f0a112233,f1a112233;
   Nb f2a112233,f0a111444,f1a111444,f2a111444,f0a111234,f1a111234;
   Nb f2a111234,f0a111233,f1a111233,f2a111233,f0a111123,f1a111123;
   Nb f2a111123;


   rhoedg     = mParam[0];
   rhoe=C_PI*rhoedg/1.8e+02;
   //Mout << " rhoedg: " << rhoedg << endl;
   //Mout << " rhoe:  " << rhoe << endl;
   //Mout << " C_PI: " << C_PI << " 180: " << 1.8e+02 << endl;   

   re14       = mParam[1];
   aa1        = mParam[2];
   ve         = mParam[3];
   f1a        = mParam[4];
   f2a        = mParam[5];
   f3a        = mParam[6];
   f4a        = mParam[7];
   f5a        = mParam[8];
   f6a        = mParam[9];
   f7a        = mParam[10];
   f8a        = mParam[11];
   f1a1       = mParam[12];
   f2a1       = mParam[13];
   f3a1       = mParam[14];
   f4a1       = mParam[15];
   f5a1       = mParam[16];
   f6a1       = mParam[17];
   f0a11      = mParam[18];
   f1a11      = mParam[19];
   f2a11      = mParam[20];
   f3a11      = mParam[21];
   f4a11      = mParam[22];
   f0a12      = mParam[23];
   f1a12      = mParam[24];
   f2a12      = mParam[25];
   f3a12      = mParam[26];
   f4a12      = mParam[27];
   f0a14      = mParam[28];
   f1a14      = mParam[29];
   f2a14      = mParam[30];
   f3a14      = mParam[31];
   f4a14      = mParam[32];
   f0a44      = mParam[33];
   f1a44      = mParam[34];
   f2a44      = mParam[35];
   f3a44      = mParam[36];
   f4a44      = mParam[37];
   f0a111     = mParam[38];
   f1a111     = mParam[39];
   f2a111     = mParam[40];
   f3a111     = mParam[41];
   f0a112     = mParam[42];
   f1a112     = mParam[43];
   f2a112     = mParam[44];
   f3a112     = mParam[45];
   f0a114     = mParam[46];
   f1a114     = mParam[47];
   f2a114     = mParam[48];
   f3a114     = mParam[49];
   f0a123     = mParam[50];
   f1a123     = mParam[51];
   f2a123     = mParam[52];
   f3a123     = mParam[53];
   f0a124     = mParam[54];
   f1a124     = mParam[55];
   f2a124     = mParam[56];
   f3a124     = mParam[57];
   f0a144     = mParam[58];
   f1a144     = mParam[59];
   f2a144     = mParam[60];
   f3a144     = mParam[61];
   f0a155     = mParam[62];
   f1a155     = mParam[63];
   f2a155     = mParam[64];
   f3a155     = mParam[65];
   f0a455     = mParam[66];
   f1a455     = mParam[67];
   f2a455     = mParam[68];
   f3a455     = mParam[69];
   f0a1111    = mParam[70];
   f1a1111    = mParam[71];
   f2a1111    = mParam[72];
   f0a1112    = mParam[73];
   f1a1112    = mParam[74];
   f2a1112    = mParam[75];
   f0a1114    = mParam[76];
   f1a1114    = mParam[77];
   f2a1114    = mParam[78];
   f0a1122    = mParam[79];
   f1a1122    = mParam[80];
   f2a1122    = mParam[81];
   f0a1123    = mParam[82];
   f1a1123    = mParam[83];
   f2a1123    = mParam[84];
   f0a1124    = mParam[85];
   f1a1124    = mParam[86];
   f2a1124    = mParam[87];
   f0a1125    = mParam[88];
   f1a1125    = mParam[89];
   f2a1125    = mParam[90];
   f0a1144    = mParam[91];
   f1a1144    = mParam[92];
   f2a1144    = mParam[93];
   f0a1155    = mParam[94];
   f1a1155    = mParam[95];
   f2a1155    = mParam[96];
   f0a1244    = mParam[97];
   f1a1244    = mParam[98];
   f2a1244    = mParam[99];
   f0a1255    = mParam[100];
   f1a1255    = mParam[101];
   f2a1255    = mParam[102];
   f0a1444    = mParam[103];
   f1a1444    = mParam[104];
   f2a1444    = mParam[105];
   f0a1455    = mParam[106];
   f1a1455    = mParam[107];
   f2a1455    = mParam[108];
   f0a4444    = mParam[109];
   f1a4444    = mParam[110];
   f2a4444    = mParam[111];
   f0a44444   = mParam[112];
   f1a44444   = mParam[113];
   f2a44444   = mParam[114];
   f0a33455   = mParam[115];
   f1a33455   = mParam[116];
   f2a33455   = mParam[117];
   f0a33445   = mParam[118];
   f1a33445   = mParam[119];
   f2a33445   = mParam[120];
   f0a33345   = mParam[121];
   f1a33345   = mParam[122];
   f2a33345   = mParam[123];
   f0a33344   = mParam[124];
   f1a33344   = mParam[125];
   f2a33344   = mParam[126];
   f0a33334   = mParam[127];
   f1a33334   = mParam[128];
   f2a33334   = mParam[129];
   f0a33333   = mParam[130];
   f1a33333   = mParam[131];
   f2a33333   = mParam[132];
   f0a25555   = mParam[133];
   f1a25555   = mParam[134];
   f2a25555   = mParam[135];
   f0a24455   = mParam[136];
   f1a24455   = mParam[137];
   f2a24455   = mParam[138];
   f0a24445   = mParam[139];
   f1a24445   = mParam[140];
   f2a24445   = mParam[141];
   f0a23333   = mParam[142];
   f1a23333   = mParam[143];
   f2a23333   = mParam[144];
   f0a13455   = mParam[145];
   f1a13455   = mParam[146];
   f2a13455   = mParam[147];
   f0a13445   = mParam[148];
   f1a13445   = mParam[149];
   f2a13445   = mParam[150];
   f0a13345   = mParam[151];
   f1a13345   = mParam[152];
   f2a13345   = mParam[153];
   f0a12355   = mParam[154];
   f1a12355   = mParam[155];
   f2a12355   = mParam[156];
   f0a11334   = mParam[157];
   f1a11334   = mParam[158];
   f2a11334   = mParam[159];
   f0a11333   = mParam[160];
   f1a11333   = mParam[161];
   f2a11333   = mParam[162];
   f0a11255   = mParam[163];
   f1a11255   = mParam[164];
   f2a11255   = mParam[165];
   f0a11245   = mParam[166];
   f1a11245   = mParam[167];
   f2a11245   = mParam[168];
   f0a11234   = mParam[169];
   f1a11234   = mParam[170];
   f2a11234   = mParam[171];
   f0a11233   = mParam[172];
   f1a11233   = mParam[173];
   f2a11233   = mParam[174];
   f0a11135   = mParam[175];
   f1a11135   = mParam[176];
   f2a11135   = mParam[177];
   f0a11134   = mParam[178];
   f1a11134   = mParam[179];
   f2a11134   = mParam[180];
   f0a11123   = mParam[181];
   f1a11123   = mParam[182];
   f2a11123   = mParam[183];
   f0a555555  = mParam[184];
   f1a555555  = mParam[185];
   f2a555555  = mParam[186];
   f0a444444  = mParam[187];
   f1a444444  = mParam[188];
   f2a444444  = mParam[189];
   f0a335555  = mParam[190];
   f1a335555  = mParam[191];
   f2a335555  = mParam[192];
   f0a334455  = mParam[193];
   f1a334455  = mParam[194];
   f2a334455  = mParam[195];
   f0a334445  = mParam[196];
   f1a334445  = mParam[197];
   f2a334445  = mParam[198];
   f0a333555  = mParam[199];
   f1a333555  = mParam[200];
   f2a333555  = mParam[201];
   f0a333333  = mParam[202];
   f1a333333  = mParam[203];
   f2a333333  = mParam[204];
   f0a244555  = mParam[205];
   f1a244555  = mParam[206];
   f2a244555  = mParam[207];
   f0a244455  = mParam[208];
   f1a244455  = mParam[209];
   f2a244455  = mParam[210];
   f0a233445  = mParam[211];
   f1a233445  = mParam[212];
   f2a233445  = mParam[213];
   f0a233444  = mParam[214];
   f1a233444  = mParam[215];
   f2a233444  = mParam[216];
   f0a233345  = mParam[217];
   f1a233345  = mParam[218];
   f2a233345  = mParam[219];
   f0a233344  = mParam[220];
   f1a233344  = mParam[221];
   f2a233344  = mParam[222];
   f0a233335  = mParam[223];
   f1a233335  = mParam[224];
   f2a233335  = mParam[225];
   f0a223355  = mParam[226];
   f1a223355  = mParam[227];
   f2a223355  = mParam[228];
   f0a222335  = mParam[229];
   f1a222335  = mParam[230];
   f2a222335  = mParam[231];
   f0a222334  = mParam[232];
   f1a222334  = mParam[233];
   f2a222334  = mParam[234];
   f0a222333  = mParam[235];
   f1a222333  = mParam[236];
   f2a222333  = mParam[237];
   f0a222255  = mParam[238];
   f1a222255  = mParam[239];
   f2a222255  = mParam[240];
   f0a222245  = mParam[241];
   f1a222245  = mParam[242];
   f2a222245  = mParam[243];
   f0a222233  = mParam[244];
   f1a222233  = mParam[245];
   f2a222233  = mParam[246];
   f0a222224  = mParam[247];
   f1a222224  = mParam[248];
   f2a222224  = mParam[249];
   f0a145555  = mParam[250];
   f1a145555  = mParam[251];
   f2a145555  = mParam[252];
   f0a134444  = mParam[253];
   f1a134444  = mParam[254];
   f2a134444  = mParam[255];
   f0a133444  = mParam[256];
   f1a133444  = mParam[257];
   f2a133444  = mParam[258];
   f0a133345  = mParam[259];
   f1a133345  = mParam[260];
   f2a133345  = mParam[261];
   f0a133334  = mParam[262];
   f1a133334  = mParam[263];
   f2a133334  = mParam[264];
   f0a133333  = mParam[265];
   f1a133333  = mParam[266];
   f2a133333  = mParam[267];
   f0a124555  = mParam[268];
   f1a124555  = mParam[269];
   f2a124555  = mParam[270];
   f0a124455  = mParam[271];
   f1a124455  = mParam[272];
   f2a124455  = mParam[273];
   f0a123455  = mParam[274];
   f1a123455  = mParam[275];
   f2a123455  = mParam[276];
   f0a123345  = mParam[277];
   f1a123345  = mParam[278];
   f2a123345  = mParam[279];
   f0a113555  = mParam[280];
   f1a113555  = mParam[281];
   f2a113555  = mParam[282];
   f0a113345  = mParam[283];
   f1a113345  = mParam[284];
   f2a113345  = mParam[285];
   f0a112355  = mParam[286];
   f1a112355  = mParam[287];
   f2a112355  = mParam[288];
   f0a112335  = mParam[289];
   f1a112335  = mParam[290];
   f2a112335  = mParam[291];
   f0a112233  = mParam[292];
   f1a112233  = mParam[293];
   f2a112233  = mParam[294];
   f0a111444  = mParam[295];
   f1a111444  = mParam[296];
   f2a111444  = mParam[297];
   f0a111234  = mParam[298];
   f1a111234  = mParam[299];
   f2a111234  = mParam[300];
   f0a111233  = mParam[301];
   f1a111233  = mParam[302];
   f2a111233  = mParam[303];
   f0a111123  = mParam[304];
   f1a111123  = mParam[305];
   f2a111123  = mParam[306];

   r14    = arGeom[0];  
   r24    = arGeom[1];  
   r34    = arGeom[2];
   alpha1 = arGeom[3];  
   alpha2 = arGeom[4];  
   alpha3 = arGeom[5];


   //rhoe=C_PI*rhoedg/1.8e+02;


   y1=C_1-exp(-aa1*(r14-re14));
   y2=C_1-exp(-aa1*(r24-re14));
   y3=C_1-exp(-aa1*(r34-re14));
   y4=(C_2*alpha1-alpha2-alpha3)/sqrt(C_6);
   y5=(alpha2-alpha3)/sqrt(C_2);

   //Mout << " y1: " << y1 << endl;
   //Mout << " y2: " << y2 << endl;
   //Mout << " y3: " << y3 << endl;
   //Mout << " y4: " << y4 << endl;
   //Mout << " y5  " << y5 << endl;

   alpha=(alpha1+alpha2+alpha3)/C_3;
   //rho=C_PI-asin(C_2*sin(alpha*C_I_2)/sqrt(C_3));
   //Mout << " alpha: " << alpha << endl;
   //Mout << " rho:   " << rho << endl;

   if (C_2*sin(alpha*C_I_2)/sqrt(C_3) >= C_1 )
      sinrho=C_1;
   else
      sinrho = C_2*sin(alpha*C_I_2)/sqrt(C_3);
   //Mout << "sinrho: " << sinrho << endl;
   //Mout << " rhoe:  " << rhoe << endl;
   coro=(sin(rhoe)-sinrho);


   //potential energy function values
   //Mout << "f1a: " << f1a << endl;
   //Mout << "f2a: " << f2a << endl;
   //Mout << "f3a: " << f3a << endl;
   //Mout << "f4a: " << f4a << endl;
   //Mout << "f5a: " << f5a << endl;
   //Mout << "f6a: " << f6a << endl;
   //Mout << "f7a: " << f7a << endl;
   //Mout << "f8a: " << f8a << endl;
   //Mout << "ve:  " << ve  << endl;
   //Mout << "coro: " << coro << endl;
   v0=ve-f1a*coro+f2a*pow(coro,C_2)+f3a*pow(coro,C_3)+f4a*pow(coro,C_4)+f5a*pow(coro,C_5) 
        +f6a*pow(coro,C_6)+f7a*pow(coro,C_7)+f8a*pow(coro,C_8);
   //Mout << "v0: " << v0 << endl;

   fea1= f1a1*coro+f2a1*pow(coro,C_2)+f3a1*pow(coro,C_3)+f4a1*pow(coro,C_4)+f5a1*pow(coro,C_5)+f6a1*pow(coro,C_6);
   //Mout << " fea1: " << fea1 << endl;

   fea11=   f0a11+f1a11*coro+f2a11*pow(coro,C_2)+f3a11*pow(coro,C_3)+f4a11*pow(coro,C_4);
   fea12=   f0a12+f1a12*coro+f2a12*pow(coro,C_2)+f3a12*pow(coro,C_3)+f4a12*pow(coro,C_4);
   fea14=   f0a14+f1a14*coro+f2a14*pow(coro,C_2)+f3a14*pow(coro,C_3)+f4a14*pow(coro,C_4);
   fea44=   f0a44+f1a44*coro+f2a44*pow(coro,C_2)+f3a44*pow(coro,C_3)+f4a44*pow(coro,C_4);
  
   fea111= f0a111+f1a111*coro+f2a111*pow(coro,C_2)+f3a111*pow(coro,C_3);
   fea112= f0a112+f1a112*coro+f2a112*pow(coro,C_2)+f3a112*pow(coro,C_3);
   fea114= f0a114+f1a114*coro+f2a114*pow(coro,C_2)+f3a114*pow(coro,C_3);
   fea123= f0a123+f1a123*coro+f2a123*pow(coro,C_2)+f3a123*pow(coro,C_3);
   fea124= f0a124+f1a124*coro+f2a124*pow(coro,C_2)+f3a124*pow(coro,C_3);
   fea144= f0a144+f1a144*coro+f2a144*pow(coro,C_2)+f3a144*pow(coro,C_3);
   fea155= f0a155+f1a155*coro+f2a155*pow(coro,C_2)+f3a155*pow(coro,C_3);
   fea455= f0a455+f1a455*coro+f2a455*pow(coro,C_2)+f3a455*pow(coro,C_3);
 
   fea1111= f0a1111+f1a1111*coro+f2a1111*pow(coro,C_2);
   fea1112= f0a1112+f1a1112*coro+f2a1112*pow(coro,C_2);
   fea1114= f0a1114+f1a1114*coro+f2a1114*pow(coro,C_2);
   fea1122= f0a1122+f1a1122*coro+f2a1122*pow(coro,C_2);
   fea1123= f0a1123+f1a1123*coro+f2a1123*pow(coro,C_2);
   fea1124= f0a1124+f1a1124*coro+f2a1124*pow(coro,C_2);
   fea1125= f0a1125+f1a1125*coro+f2a1125*pow(coro,C_2);
   fea1144= f0a1144+f1a1144*coro+f2a1144*pow(coro,C_2);
   fea1155= f0a1155+f1a1155*coro+f2a1155*pow(coro,C_2);
   fea1244= f0a1244+f1a1244*coro+f2a1244*pow(coro,C_2);
   fea1255= f0a1255+f1a1255*coro+f2a1255*pow(coro,C_2);
   fea1444= f0a1444+f1a1444*coro+f2a1444*pow(coro,C_2);
   fea1455= f0a1455+f1a1455*coro+f2a1455*pow(coro,C_2);
   fea4444= f0a4444+f1a4444*coro+f2a4444*pow(coro,C_2);
 
   fea44444 = f0a44444  + f1a44444 *coro+ f2a44444 *pow(coro,C_2);
   fea33455 = f0a33455  + f1a33455 *coro+ f2a33455 *pow(coro,C_2);
   fea33445 = f0a33445  + f1a33445 *coro+ f2a33445 *pow(coro,C_2);
   fea33345 = f0a33345  + f1a33345 *coro+ f2a33345 *pow(coro,C_2);
   fea33344 = f0a33344  + f1a33344 *coro+ f2a33344 *pow(coro,C_2);
   fea33334 = f0a33334  + f1a33334 *coro+ f2a33334 *pow(coro,C_2);
   fea33333 = f0a33333  + f1a33333 *coro+ f2a33333 *pow(coro,C_2);
   fea25555 = f0a25555  + f1a25555 *coro+ f2a25555 *pow(coro,C_2);
   fea24455 = f0a24455  + f1a24455 *coro+ f2a24455 *pow(coro,C_2);
   fea24445 = f0a24445  + f1a24445 *coro+ f2a24445 *pow(coro,C_2);
   fea23333 = f0a23333  + f1a23333 *coro+ f2a23333 *pow(coro,C_2);
   fea13455 = f0a13455  + f1a13455 *coro+ f2a13455 *pow(coro,C_2);
   fea13445 = f0a13445  + f1a13445 *coro+ f2a13445 *pow(coro,C_2);
   fea13345 = f0a13345  + f1a13345 *coro+ f2a13345 *pow(coro,C_2);
   fea12355 = f0a12355  + f1a12355 *coro+ f2a12355 *pow(coro,C_2);
   fea11334 = f0a11334  + f1a11334 *coro+ f2a11334 *pow(coro,C_2);
   fea11333 = f0a11333  + f1a11333 *coro+ f2a11333 *pow(coro,C_2);
   fea11255 = f0a11255  + f1a11255 *coro+ f2a11255 *pow(coro,C_2);
   fea11245 = f0a11245  + f1a11245 *coro+ f2a11245 *pow(coro,C_2);
   fea11234 = f0a11234  + f1a11234 *coro+ f2a11234 *pow(coro,C_2);
   fea11233 = f0a11233  + f1a11233 *coro+ f2a11233 *pow(coro,C_2);
   fea11135 = f0a11135  + f1a11135 *coro+ f2a11135 *pow(coro,C_2);
   fea11134 = f0a11134  + f1a11134 *coro+ f2a11134 *pow(coro,C_2);
   fea11123 = f0a11123  + f1a11123 *coro+ f2a11123 *pow(coro,C_2);
   fea555555= f0a555555 + f1a555555*coro+ f2a555555*pow(coro,C_2);
   fea444444= f0a444444 + f1a444444*coro+ f2a444444*pow(coro,C_2);
   fea335555= f0a335555 + f1a335555*coro+ f2a335555*pow(coro,C_2);
   fea334455= f0a334455 + f1a334455*coro+ f2a334455*pow(coro,C_2);
   fea334445= f0a334445 + f1a334445*coro+ f2a334445*pow(coro,C_2);
   fea333555= f0a333555 + f1a333555*coro+ f2a333555*pow(coro,C_2);
   fea333333= f0a333333 + f1a333333*coro+ f2a333333*pow(coro,C_2);
   fea244555= f0a244555 + f1a244555*coro+ f2a244555*pow(coro,C_2);
   fea244455= f0a244455 + f1a244455*coro+ f2a244455*pow(coro,C_2);
   fea233445= f0a233445 + f1a233445*coro+ f2a233445*pow(coro,C_2);
   fea233444= f0a233444 + f1a233444*coro+ f2a233444*pow(coro,C_2);
   fea233345= f0a233345 + f1a233345*coro+ f2a233345*pow(coro,C_2);
   fea233344= f0a233344 + f1a233344*coro+ f2a233344*pow(coro,C_2);
   fea233335= f0a233335 + f1a233335*coro+ f2a233335*pow(coro,C_2);
   fea223355= f0a223355 + f1a223355*coro+ f2a223355*pow(coro,C_2);
   fea222335= f0a222335 + f1a222335*coro+ f2a222335*pow(coro,C_2);
   fea222334= f0a222334 + f1a222334*coro+ f2a222334*pow(coro,C_2);
   fea222333= f0a222333 + f1a222333*coro+ f2a222333*pow(coro,C_2);
   fea222255= f0a222255 + f1a222255*coro+ f2a222255*pow(coro,C_2);
   fea222245= f0a222245 + f1a222245*coro+ f2a222245*pow(coro,C_2);
   fea222233= f0a222233 + f1a222233*coro+ f2a222233*pow(coro,C_2);
   fea222224= f0a222224 + f1a222224*coro+ f2a222224*pow(coro,C_2);
   fea145555= f0a145555 + f1a145555*coro+ f2a145555*pow(coro,C_2);
   fea134444= f0a134444 + f1a134444*coro+ f2a134444*pow(coro,C_2);
   fea133444= f0a133444 + f1a133444*coro+ f2a133444*pow(coro,C_2);
   fea133345= f0a133345 + f1a133345*coro+ f2a133345*pow(coro,C_2);
   fea133334= f0a133334 + f1a133334*coro+ f2a133334*pow(coro,C_2);
   fea133333= f0a133333 + f1a133333*coro+ f2a133333*pow(coro,C_2);
   fea124555= f0a124555 + f1a124555*coro+ f2a124555*pow(coro,C_2);
   fea124455= f0a124455 + f1a124455*coro+ f2a124455*pow(coro,C_2);
   fea123455= f0a123455 + f1a123455*coro+ f2a123455*pow(coro,C_2);
   fea123345= f0a123345 + f1a123345*coro+ f2a123345*pow(coro,C_2);
   fea113555= f0a113555 + f1a113555*coro+ f2a113555*pow(coro,C_2);
   fea113345= f0a113345 + f1a113345*coro+ f2a113345*pow(coro,C_2);
   fea112355= f0a112355 + f1a112355*coro+ f2a112355*pow(coro,C_2);
   fea112335= f0a112335 + f1a112335*coro+ f2a112335*pow(coro,C_2);
   fea112233= f0a112233 + f1a112233*coro+ f2a112233*pow(coro,C_2);
   fea111444= f0a111444 + f1a111444*coro+ f2a111444*pow(coro,C_2);
   fea111234= f0a111234 + f1a111234*coro+ f2a111234*pow(coro,C_2);
   fea111233= f0a111233 + f1a111233*coro+ f2a111233*pow(coro,C_2);
   fea111123= f0a111123 + f1a111123*coro+ f2a111123*pow(coro,C_2);
 

   v1 = (y3+y2+y1)*fea1;
   //Mout << " y1: " << y1 << endl;
   //Mout << " y2: " << y2 << endl;
   //Mout << " y3: " << y3 << endl;
   //Mout << "fea1: " << fea1 << endl;
   //Mout << "v1: " << v1 << endl;

   v2 = (y2*y3+y1*y3+y1*y2)*fea12                                                    
       +(pow(y2,C_2)+pow(y3,C_2)+pow(y1,C_2))*fea11                                                        
       +(-sqrt(C_3)*y3*y5/C_2-y3*y4/C_2+y1*y4+sqrt(C_3)*y2*y5/C_2-y2*y4/C_2)*fea14
       +(pow(y5,C_2)+pow(y4,C_2))*fea44;
   //Mout << "v2: " << v2 << endl;

   v3 = (y1*y3*y4+y1*y2*y4-C_2*y2*y3*y4+sqrt(C_3)*y1*y2*y5-sqrt(C_3)*y1*y3*y5)*fea124        
        +(C_3/C_4*y3*pow(y4,C_2)-sqrt(C_3)*y3*y4*y5/C_2+y1*pow(y5,C_2)+y2*pow(y5,C_2)/C_4+
        C_3/C_4*y2*pow(y4,C_2)+sqrt(C_3)*y2*y4*y5/C_2+y3*pow(y5,C_2)/C_4)*fea155  
        +(y2*pow(y3,C_2)+y1*pow(y3,C_2)+pow(y1,C_2)*y3+y1*pow(y2,C_2)+pow(y2,C_2)*y3+pow(y1,C_2)*y2)*fea112+                             
        (-pow(y4,C_3)/C_3+y4*pow(y5,C_2))*fea455+fea123*y1*y2*y3                                                
        +(y1*pow(y4,C_2)+C_3/C_4*y3*pow(y5,C_2)+C_3/C_4*y2*pow(y5,C_2)+y2*pow(y4,C_2)/C_4-sqrt(C_3)*y2*y4*y5/C_2+
        sqrt(C_3)*y3*y4*y5/C_2+y3*pow(y4,C_2)/C_4)*fea144 
        +(pow(y3,C_3)+pow(y2,C_3)+pow(y1,C_3))*fea111                                                                  
        +(-pow(y2,C_2)*y4/C_2-pow(y3,C_2)*y4/C_2+sqrt(C_3)*
        pow(y2,C_2)*y5/C_2+pow(y1,C_2)*y4-sqrt(C_3)*pow(y3,C_2)*y5/C_2)*fea114;
   //Mout << "v3: " << v3 << endl;


   s2 = (pow(y4,C_4)+pow(y5,C_4)+C_2*pow(y4,C_2)*pow(y5,C_2))*fea4444+(C_3/C_8*sqrt(C_3)*
        y2*pow(y5,C_3)-C_3/C_8*sqrt(C_3)*y3*pow(y4,C_2)*y5-C_3/C_8*sqrt(C_3)*y3*
        pow(y5,C_3)-C_9/C_8*y2*y4*pow(y5,C_2)-y3*pow(y4,C_3)/C_8-y2*pow(y4,C_3)/C_8-C_9/C_8*
        y3*y4*pow(y5,C_2)+y1*pow(y4,C_3)+C_3/C_8*sqrt(C_3)*y2*pow(y4,C_2)*y5)*fea1444+(C_3/C_4*
        pow(y2,C_2)*pow(y4,C_2)+C_3/C_4*pow(y3,C_2)*pow(y4,C_2)+pow(y1,C_2)*pow(y5,C_2)+pow(y3,C_2)*pow(y5,C_2)/
        C_4-sqrt(C_3)*pow(y3,C_2)*y4*y5/C_2+sqrt(C_3)*pow(y2,C_2)*y4*y5/C_2+pow(y2,C_2)
        *pow(y5,C_2)/C_4)*fea1155; 
   //Mout << "s2: " << s2 << endl;

   s1 = s2+(pow(y3,C_2)*pow(y4,C_2)/C_4+C_3/C_4*pow(y3,C_2)*pow(y5,C_2)+pow(y1,C_2)*pow(y4,C_2)+pow(y2,C_2)*
        pow(y4,C_2)/C_4+sqrt(C_3)*pow(y3,C_2)*y4*y5/C_2-sqrt(C_3)*pow(y2,C_2)*y4*y5/C_2
        +C_3/C_4*pow(y2,C_2)*pow(y5,C_2))*fea1144+(pow(y1,C_3)*y4+sqrt(C_3)*pow(y2,C_3)*y5/C_2
        -sqrt(C_3)*pow(y3,C_3)*y5/C_2-pow(y2,C_3)*y4/C_2-pow(y3,C_3)*y4/C_2)*fea1114+(pow(y2,C_4)
        +pow(y1,C_4)+pow(y3,C_4))*fea1111+(sqrt(C_3)*y1*y3*y4*y5+C_3/C_2*y2*y3*pow(y5,C_2)
        -y2*y3*pow(y4,C_2)/C_2+y1*y2*pow(y4,C_2)-sqrt(C_3)*y1*y2*y4*y5+y1*y3*pow(y4,C_2))
        *fea1244; 
   //Mout << "s1: " << s1 << endl;

   s2 = s1+(y1*y3*pow(y5,C_2)+y1*y2*pow(y5,C_2)-sqrt(C_3)*y1*y3*y4*y5-y2*y3*pow(y5,C_2)/
        C_2+C_3/C_2*y2*y3*pow(y4,C_2)+sqrt(C_3)*y1*y2*y4*y5)*fea1255+(-y1*pow(y3,C_2)
        *y4/C_2+pow(y1,C_2)*y3*y4-sqrt(C_3)*y1*pow(y3,C_2)*y5/C_2-sqrt(C_3)*y2
        *pow(y3,C_2)*y5/C_2+pow(y1,C_2)*y2*y4+sqrt(C_3)*pow(y2,C_2)*y3*y5/C_2-pow(y2,C_2)*y3*y4
        /C_2+sqrt(C_3)*y1*pow(y2,C_2)*y5/C_2-y2*pow(y3,C_2)*y4/C_2-y1*pow(y2,C_2)*y4/C_2
        )*fea1124+(pow(y1,C_2)*y2*y5+sqrt(C_3)*y1*pow(y3,C_2)*y4/C_2+sqrt(C_3)*y1*
        pow(y2,C_2)*y4/C_2-sqrt(C_3)*y2*pow(y3,C_2)*y4/C_2-sqrt(C_3)*pow(y2,C_2)*y3*y4/C_2
        -pow(y2,C_2)*y3*y5/C_2+y2*pow(y3,C_2)*y5/C_2-y1*pow(y3,C_2)*y5/C_2+y1*pow(y2,C_2)*y5
        /C_2-pow(y1,C_2)*y3*y5)*fea1125; 
   //Mout << "s2: " << s2 << endl;

   v4 = s2+(y2*pow(y3,C_3)+pow(y1,C_3)*y3+pow(y1,C_3)*y2+y1*pow(y2,C_3)+y1*pow(y3,C_3)+pow(y2,C_3)*y3)*fea1112
        +(pow(y2,C_2)*pow(y3,C_2)+pow(y1,C_2)*pow(y3,C_2)+pow(y1,C_2)*pow(y2,C_2))*fea1122+(y1*pow(y2,C_2)*y3+pow(y1,C_2)*
        y2*y3+y1*y2*pow(y3,C_2))*fea1123+(C_5/C_8*y2*y4*pow(y5,C_2)+sqrt(C_3)*
        y2*pow(y5,C_3)/C_8-sqrt(C_3)*y3*pow(y4,C_2)*y5/C_8+sqrt(C_3)*y2*pow(y4,C_2)*y5/C_8
        -C_3/C_8*y2*pow(y4,C_3)+y1*y4*pow(y5,C_2)-sqrt(C_3)*y3*pow(y5,C_3)/C_8+C_5/C_8
        *y3*y4*pow(y5,C_2)-C_3/C_8*y3*pow(y4,C_3))*fea1455;
   //Mout << "v4: " << v4 << endl;

 
   s3 = (pow(y4,C_5)-C_2*pow(y4,C_3)*pow(y5,C_2)-C_3*y4*pow(y5,C_4))*fea44444+(-C_4*y3*y4*
        pow(y5,C_3)*sqrt(C_3)+C_9*y1*pow(y4,C_2)*pow(y5,C_2)-C_3/C_2*y1*pow(y4,C_4)+C_4*y2*y4
        *pow(y5,C_3)*sqrt(C_3)+C_3*y2*pow(y4,C_4)+C_5/C_2*y1*pow(y5,C_4)+C_3*y3*pow(y4,C_4)+y2
        *pow(y5,C_4)+y3*pow(y5,C_4))*fea25555+(-y2*pow(y4,C_4)+y3*pow(y4,C_2)*pow(y5,C_2)-C_2*y2*y4*pow(y5,C_3)
        *sqrt(C_3)-y3*pow(y4,C_4)-C_7/C_2*y1*pow(y4,C_2)*pow(y5,C_2)-C_3/C_4*y1*pow(y5,C_4)
        +C_2*y3*y4*pow(y5,C_3)*sqrt(C_3)+y2*pow(y4,C_2)*pow(y5,C_2)+C_5/C_4*y1*pow(y4,C_4))*fea24455;
   //Mout << "s3: " << s3 << endl;


   s2 = s3+(y2*pow(y4,C_3)*y5-C_3*y3*y4*pow(y5,C_3)+C_2/C_3*y3*pow(y4,C_4)*sqrt(C_3)
        +C_3/C_4*y1*pow(y5,C_4)*sqrt(C_3)+C_3*y2*y4*pow(y5,C_3)-C_7/C_12*y1*pow(y4,C_4)
        *sqrt(C_3)+C_3/C_2*y1*pow(y4,C_2)*pow(y5,C_2)*sqrt(C_3)-y3*pow(y4,C_3)*y5+C_2
        /C_3*y2*pow(y4,C_4)*sqrt(C_3))*fea24445+(-pow(y2,C_2)*pow(y5,C_3)+pow(y3,C_2)*pow(y4,C_2)*y5+pow(y3,C_2)*
        pow(y5,C_3)+C_4/C_9*pow(y2,C_2)*pow(y4,C_3)*sqrt(C_3)-C_5/C_9*pow(y1,C_2)*pow(y4,C_3)*
        sqrt(C_3)+C_4/C_9*pow(y3,C_2)*pow(y4,C_3)*sqrt(C_3)-pow(y2,C_2)*pow(y4,C_2)*y5-pow(y1,C_2)*y4
        *pow(y5,C_2)*sqrt(C_3))*fea33445+(pow(y3,C_2)*y4*pow(y5,C_2)-pow(y1,C_2)*pow(y4,C_3)/C_3-pow(y3,C_2)
        *pow(y4,C_3)/C_3+pow(y1,C_2)*y4*pow(y5,C_2)+pow(y2,C_2)*y4*pow(y5,C_2)-pow(y2,C_2)*pow(y4,C_3)/C_3)*fea33455;
   //Mout << "s2: " << s2 << endl;
   
   s1 = s2+(-pow(y2,C_3)*y4*y5+pow(y3,C_3)*y4*y5+pow(y2,C_3)*pow(y5,C_2)*sqrt(C_3)/C_3+pow(y1,C_3)*
        pow(y4,C_2)*sqrt(C_3)/C_2+pow(y3,C_3)*pow(y5,C_2)*sqrt(C_3)/C_3-pow(y1,C_3)*pow(y5,C_2)*
        sqrt(C_3)/C_6)*fea33345+(pow(y3,C_3)*pow(y4,C_2)+pow(y3,C_3)*pow(y5,C_2)+pow(y2,C_3)*pow(y4,C_2)+pow(y2,C_3)
        *pow(y5,C_2)+pow(y1,C_3)*pow(y5,C_2)+pow(y1,C_3)*pow(y4,C_2))*fea33344+(pow(y3,C_4)*y4+sqrt(C_3)*pow(y3,C_4)
        *y5+pow(y2,C_4)*y4-C_2*pow(y1,C_4)*y4-sqrt(C_3)*pow(y2,C_4)*y5)*fea33334+(pow(y2,C_5)+pow(y3,C_5)
        +pow(y1,C_5))*fea33333+(-C_4/C_9*y1*y2*pow(y4,C_3)*sqrt(C_3)-y1*y2*pow(y5,C_3)
        +y1*y3*pow(y4,C_2)*y5+y2*y3*y4*pow(y5,C_2)*sqrt(C_3)-y1*y2*pow(y4,C_2)*y5+C_5/C_9
        *y2*y3*pow(y4,C_3)*sqrt(C_3)-C_4/C_9*y1*y3*pow(y4,C_3)*sqrt(C_3)+y1*y3*pow(y5,C_3))*fea13445+
        (y2*y3*y4*pow(y5,C_2)+y1*y2*y4*pow(y5,C_2)-y2*y3*pow(y4,C_3)/C_3-y1*y2
        *pow(y4,C_3)/C_3-y1*y3*pow(y4,C_3)/C_3+y1*y3*y4*pow(y5,C_2))*fea13455; 
   //Mout << "s1: " << s1 << endl;

   s3 = s1+(pow(y1,C_2)*y3*pow(y5,C_2)+pow(y2,C_2)*y3*pow(y4,C_2)+pow(y2,C_2)*y3*pow(y5,C_2)+y1*pow(y2,C_2)*pow(y5,C_2)
        +pow(y1,C_2)*y2*pow(y5,C_2)+y1*pow(y2,C_2)*pow(y4,C_2)+y2*pow(y3,C_2)*pow(y4,C_2)+y1*pow(y3,C_2)*pow(y4,C_2)+pow(y1,C_2)
        *y3*pow(y4,C_2)+pow(y1,C_2)*y2*pow(y4,C_2)+y1*pow(y3,C_2)*pow(y5,C_2)+y2*pow(y3,C_2)*pow(y5,C_2))*fea11255
        +(C_2/C_3*pow(y1,C_2)*y3*pow(y4,C_2)*sqrt(C_3)+y1*pow(y3,C_2)*pow(y5,C_2)*sqrt(C_3)/C_2
        +y1*pow(y2,C_2)*pow(y5,C_2)*sqrt(C_3)/C_2+pow(y2,C_2)*y3*pow(y5,C_2)*sqrt(C_3)/C_2-y1
        *pow(y2,C_2)*y4*y5+y2*pow(y3,C_2)*y4*y5+y1*pow(y3,C_2)*y4*y5-pow(y2,C_2)*y3*y4*y5+y2*pow(y3,C_2)
        *pow(y4,C_2)*sqrt(C_3)/C_6+y1*pow(y3,C_2)*pow(y4,C_2)*sqrt(C_3)/C_6+y1*pow(y2,C_2)*pow(y4,C_2)
        *sqrt(C_3)/C_6+C_2/C_3*pow(y1,C_2)*y2*pow(y4,C_2)*sqrt(C_3)+y2*pow(y3,C_2)*pow(y5,C_2)
        *sqrt(C_3)/C_2+pow(y2,C_2)*y3*pow(y4,C_2)*sqrt(C_3)/C_6)*fea13345;
   //Mout << "s3: " << s3 << endl;

   s4 = s3+(pow(y1,C_2)*y2*y4*y5+pow(y1,C_2)*y3*pow(y4,C_2)*sqrt(C_3)/C_3+pow(y1,C_2)*y2*pow(y4,C_2)
        *sqrt(C_3)/C_3-y1*pow(y2,C_2)*pow(y4,C_2)*sqrt(C_3)/C_6+y2*pow(y3,C_2)*y4*y5-
        pow(y2,C_2)*y3*y4*y5-pow(y1,C_2)*y3*y4*y5+y2*pow(y3,C_2)*pow(y4,C_2)*sqrt(C_3)/C_3+y1*pow(y2,C_2)
        *pow(y5,C_2)*sqrt(C_3)/C_2-y1*pow(y3,C_2)*pow(y4,C_2)*sqrt(C_3)/C_6+pow(y2,C_2)*y3*
        pow(y4,C_2)*sqrt(C_3)/C_3+y1*pow(y3,C_2)*pow(y5,C_2)*sqrt(C_3)/C_2)*fea11245;
   //Mout << "s4: " << s4 << endl;

   s2 = s4+(-pow(y1,C_3)*y2*y5+pow(y1,C_3)*y3*y5+pow(y2,C_3)*y3*y5/C_2-y1*pow(y2,C_3)*y4*sqrt(C_3)/C_2-
        y1*pow(y2,C_3)*y5/C_2-y2*pow(y3,C_3)*y5/C_2+y1*pow(y3,C_3)*y5/C_2+pow(y2,C_3)
        *y3*y4*sqrt(C_3)/C_2+y2*pow(y3,C_3)*y4*sqrt(C_3)/C_2-y1*pow(y3,C_3)*y4*
        sqrt(C_3)/C_2)*fea11135+(pow(y1,C_3)*y3*y4-pow(y2,C_3)*y3*y4/C_2+pow(y1,C_3)*y2*y4
        -y2*pow(y3,C_3)*y4/C_2-y1*pow(y3,C_3)*y4/C_2+y1*pow(y2,C_3)*y5*sqrt(C_3)/C_2+pow(y2,C_3)
        *y3*y5*sqrt(C_3)/C_2-y2*pow(y3,C_3)*y5*sqrt(C_3)/C_2-y1*pow(y2,C_3)*y4/
        C_2-y1*pow(y3,C_3)*y5*sqrt(C_3)/C_2)*fea11134;
   //Mout << "s2: " << s2 << endl;

   v5 = s2+(y1*pow(y2,C_4)+pow(y1,C_4)*y3+pow(y1,C_4)*y2+pow(y2,C_4)*y3+y2*pow(y3,C_4)+y1*pow(y3,C_4))*fea23333+
        (-C_2*pow(y2,C_2)*pow(y3,C_2)*y4+pow(y1,C_2)*pow(y2,C_2)*y4-sqrt(C_3)*pow(y1,C_2)*pow(y3,C_2)
        *y5+sqrt(C_3)*pow(y1,C_2)*pow(y2,C_2)*y5+pow(y1,C_2)*pow(y3,C_2)*y4)*fea11334+(pow(y1,C_2)*pow(y3,C_3)
        +pow(y1,C_3)*pow(y3,C_2)+pow(y2,C_2)*pow(y3,C_3)+pow(y1,C_2)*pow(y2,C_3)+pow(y1,C_3)*pow(y2,C_2)+pow(y2,C_3)*pow(y3,C_2))*fea11333
        +(y1*y2*y3*pow(y4,C_2)+y1*y2*y3*pow(y5,C_2))*fea12355+(-y1*y2*pow(y3,C_2)*y4/C_2
        -y1*pow(y2,C_2)*y3*y4/C_2-sqrt(C_3)*y1*y2*pow(y3,C_2)*y5/C_2+pow(y1,C_2)*y2*y3*
        y4+sqrt(C_3)*y1*pow(y2,C_2)*y3*y5/C_2)*fea11234+(y1*pow(y2,C_3)*y3+y1*y2*pow(y3,C_3)
        +pow(y1,C_3)*y2*y3)*fea11123+(pow(y1,C_2)*pow(y2,C_2)*y3+y1*pow(y2,C_2)*pow(y3,C_2)+pow(y1,C_2)*y2*pow(y3,C_2))
        *fea11233;
   //Mout << "v5: " << v5 << endl;
 
   s3 = (pow(y2,C_3)*pow(y4,C_3)*sqrt(C_3)-pow(y2,C_3)*pow(y4,C_2)*y5+pow(y3,C_3)*pow(y4,C_2)*y5-C_5/C_3
        *pow(y2,C_3)*y4*pow(y5,C_2)*sqrt(C_3)+pow(y3,C_3)*pow(y4,C_3)*sqrt(C_3)-C_5/C_3*pow(y3,C_3)
        *y4*pow(y5,C_2)*sqrt(C_3)-pow(y2,C_3)*pow(y5,C_3)+pow(y3,C_3)*pow(y5,C_3)-C_8/C_3*pow(y1,C_3)*y4*pow(y5,C_2)
        *sqrt(C_3))*fea333555+(pow(y1,C_4)*pow(y5,C_2)*sqrt(C_3)/C_2+pow(y2,C_4)*y4*y5
        +pow(y2,C_4)*pow(y4,C_2)*sqrt(C_3)/C_3+pow(y3,C_4)*pow(y4,C_2)*sqrt(C_3)/C_3-pow(y3,C_4)*y4
        *y5-pow(y1,C_4)*pow(y4,C_2)*sqrt(C_3)/C_6)*fea222245+(y1*pow(y3,C_5)+y1*pow(y2,C_5)+pow(y2,C_5)
        *y3+pow(y1,C_5)*y3+pow(y1,C_5)*y2+y2*pow(y3,C_5))*fea133333+(pow(y1,C_4)*y3*y4-C_2*pow(y2,C_4)
        *y3*y4+pow(y1,C_4)*y2*y4+y1*pow(y2,C_4)*y5*sqrt(C_3)+y1*pow(y3,C_4)*y4-C_2*y2*pow(y3,C_4)
        *y4+pow(y1,C_4)*y2*y5*sqrt(C_3)-y1*pow(y3,C_4)*y5*sqrt(C_3)-pow(y1,C_4)*y3*y5*sqrt(C_3)+y1*pow(y2,C_4)*y4)*fea133334+
        (-y1*y2*y3*pow(y4,C_3)/C_3+y1*y2*y3*y4*pow(y5,C_2))*fea123455;
   //Mout << "s3: " << s3 << endl;

   s4 = s3+(C_2/C_3*sqrt(C_3)*y1*pow(y2,C_2)*pow(y3,C_2)*y4-pow(y1,C_2)*pow(y2,C_2)*y3*y5-
        sqrt(C_3)*pow(y1,C_2)*pow(y2,C_2)*y3*y4/C_3+pow(y1,C_2)*y2*pow(y3,C_2)*y5-sqrt(C_3)*pow(y1,C_2)
        *y2*pow(y3,C_2)*y4/C_3)*fea112335+(y1*pow(y2,C_2)*y3*pow(y5,C_2)+y1*y2*pow(y3,C_2)*pow(y5,C_2)
        +y1*y2*pow(y3,C_2)*pow(y4,C_2)+y1*pow(y2,C_2)*y3*pow(y4,C_2)+pow(y1,C_2)*y2*y3*pow(y4,C_2)+pow(y1,C_2)*y2*y3
        *pow(y5,C_2))*fea112355;
   //Mout << "s4: " << s4 << endl;

   s2 = s4+(pow(y2,C_3)*pow(y3,C_2)*y5-pow(y1,C_3)*pow(y2,C_2)*y5/C_2-pow(y1,C_2)*pow(y3,C_3)*y5/C_2-pow(y2,C_2)
        *pow(y3,C_3)*y5+pow(y1,C_3)*pow(y2,C_2)*y4*sqrt(C_3)/C_2-pow(y1,C_2)*pow(y2,C_3)*y4*sqrt(C_3)
        /C_2+pow(y1,C_3)*pow(y3,C_2)*y5/C_2+pow(y1,C_2)*pow(y2,C_3)*y5/C_2+pow(y1,C_3)*pow(y3,C_2)*y4*sqrt(C_3)/C_2-
        pow(y1,C_2)*pow(y3,C_3)*y4*sqrt(C_3)/C_2)*fea222335+(-pow(y1,C_2)*pow(y2,C_2)
        *pow(y5,C_2)*sqrt(C_3)/C_2-pow(y1,C_2)*pow(y3,C_2)*pow(y5,C_2)*sqrt(C_3)/C_2-pow(y1,C_2)*
        pow(y2,C_2)*pow(y4,C_2)*sqrt(C_3)/C_6-pow(y1,C_2)*pow(y2,C_2)*y4*y5-C_2/C_3*pow(y2,C_2)*pow(y3,C_2)
        *pow(y4,C_2)*sqrt(C_3)+pow(y1,C_2)*pow(y3,C_2)*y4*y5-pow(y1,C_2)*pow(y3,C_2)*pow(y4,C_2)*sqrt(C_3)/
        C_6)*fea113345+(pow(y2,C_2)*pow(y3,C_2)*pow(y5,C_2)+pow(y2,C_2)*pow(y3,C_2)*pow(y4,C_2)+pow(y1,C_2)*pow(y2,C_2)*pow(y5,C_2)
        +pow(y1,C_2)*pow(y3,C_2)*pow(y4,C_2)+pow(y1,C_2)*pow(y3,C_2)*pow(y5,C_2)+pow(y1,C_2)*pow(y2,C_2)*pow(y4,C_2))*fea223355;
   //Mout << "s2: " << s2 << endl;

   s3 = s2+(y1*y2*pow(y3,C_2)*pow(y4,C_2)*sqrt(C_3)/C_6+y1*y2*pow(y3,C_2)*y4*y5+y1*y2
        *pow(y3,C_2)*pow(y5,C_2)*sqrt(C_3)/C_2+C_2/C_3*pow(y1,C_2)*y2*y3*pow(y4,C_2)*sqrt(C_3)
        -y1*pow(y2,C_2)*y3*y4*y5+y1*pow(y2,C_2)*y3*pow(y4,C_2)*sqrt(C_3)/C_6+y1*pow(y2,C_2)*y3*
        pow(y5,C_2)*sqrt(C_3)/C_2)*fea123345+(-pow(y1,C_3)*pow(y2,C_2)*y5*sqrt(C_3)/C_2-
        pow(y1,C_3)*pow(y2,C_2)*y4/C_2-pow(y1,C_3)*pow(y3,C_2)*y4/C_2-pow(y1,C_2)*pow(y2,C_3)*y4/C_2+pow(y1,C_3)*
        pow(y3,C_2)*y5*sqrt(C_3)/C_2-pow(y1,C_2)*pow(y3,C_3)*y4/C_2+pow(y2,C_3)*pow(y3,C_2)*y4-pow(y1,C_2)*
        pow(y2,C_3)*y5*sqrt(C_3)/C_2+pow(y2,C_2)*pow(y3,C_3)*y4+pow(y1,C_2)*pow(y3,C_3)*y5*sqrt(C_3)/
        C_2)*fea222334+(C_3*pow(y3,C_2)*pow(y4,C_4)+C_5/C_2*pow(y1,C_2)*pow(y5,C_4)+pow(y2,C_2)*pow(y5,C_4)
        +C_3*pow(y2,C_2)*pow(y4,C_4)-C_4*pow(y3,C_2)*y4*pow(y5,C_3)*sqrt(C_3)+pow(y3,C_2)*pow(y5,C_4)+C_9
        *pow(y1,C_2)*pow(y4,C_2)*pow(y5,C_2)-C_3/C_2*pow(y1,C_2)*pow(y4,C_4)+C_4*pow(y2,C_2)*y4*pow(y5,C_3)*sqrt(C_3))*fea335555
        +(pow(y1,C_3)*pow(y2,C_3)+pow(y1,C_3)*pow(y3,C_3)+pow(y2,C_3)*pow(y3,C_3))*fea222333;
   //Mout << "s3: " << s3 << endl;

   s4 = s3+(y3*pow(y4,C_5)/C_5-y2*pow(y4,C_4)*y5*sqrt(C_3)/C_2-C_2/C_5*y1*pow(y4,C_5)
        -C_2*y1*pow(y4,C_3)*pow(y5,C_2)-C_3/C_10*y2*pow(y5,C_5)*sqrt(C_3)+y3*pow(y4,C_3)*pow(y5,C_2)
        +y3*pow(y4,C_4)*y5*sqrt(C_3)/C_2+y2*pow(y4,C_3)*pow(y5,C_2)+C_3/C_10*y3*pow(y5,C_5)
        *sqrt(C_3)+y2*pow(y4,C_5)/C_5)*fea244455+(pow(y2,C_5)*y4-C_2*pow(y1,C_5)*y4-sqrt(C_3)*
        pow(y2,C_5)*y5+pow(y3,C_5)*y4+sqrt(C_3)*pow(y3,C_5)*y5)*fea222224;
   //Mout << "s4: " << s4 << endl;

   s5 = s4+(-y3*pow(y5,C_5)*sqrt(C_3)/C_5+y2*pow(y5,C_5)*sqrt(C_3)/C_5+y1*y4*
        pow(y5,C_4)-C_7/15.e0*y2*pow(y4,C_5)+y2*pow(y4,C_4)*y5*sqrt(C_3)/C_3-y3*pow(y4,C_4)*y5*
        sqrt(C_3)/C_3+y3*y4*pow(y5,C_4)+y2*y4*pow(y5,C_4)+C_2*y1*pow(y4,C_3)*pow(y5,C_2)-C_7/
        15.e0*y3*pow(y4,C_5)-y1*pow(y4,C_5)/15.e0)*fea145555;
   //Mout << "s5: " << s5 << endl;

   s1 = s5+(-sqrt(C_3)*y1*y2*pow(y3,C_3)*y5/C_2+pow(y1,C_3)*y2*y3*y4+sqrt(C_3)
        *y1*pow(y2,C_3)*y3*y5/C_2-y1*pow(y2,C_3)*y3*y4/C_2-y1*y2*pow(y3,C_3)*y4/C_2)*fea111234+
        (y3*pow(y4,C_4)*y5/C_3+y3*pow(y4,C_5)*sqrt(C_3)/18.e0-y2*pow(y4,C_4)*y5/C_3
        -y2*y4*pow(y5,C_4)*sqrt(C_3)/C_2-y3*pow(y4,C_2)*pow(y5,C_3)+C_2/C_9*y1*pow(y4,C_5)*sqrt(C_3)+
        y2*pow(y4,C_5)*sqrt(C_3)/18.e0+y2*pow(y4,C_2)*pow(y5,C_3)-C_2/C_3*y1*pow(y4,C_3)
        *pow(y5,C_2)*sqrt(C_3)-y3*y4*pow(y5,C_4)*sqrt(C_3)/C_2)*fea244555+(y1*y2*pow(y4,C_2)*
        pow(y5,C_2)-C_3/C_4*y2*y3*pow(y4,C_4)-y1*y2*pow(y5,C_4)-y1*y3*pow(y5,C_4)+C_5/C_4
        *y2*y3*pow(y5,C_4)+y1*y3*pow(y4,C_2)*pow(y5,C_2)-C_7/C_2*y2*y3*pow(y4,C_2)*pow(y5,C_2)-C_2*y1
        *y2*pow(y4,C_3)*y5*sqrt(C_3)+C_2*y1*y3*pow(y4,C_3)*y5*sqrt(C_3))*fea124455;
   //Mout << "s1: " << s1 << endl;
        
   s3 = s1+(pow(y2,C_6)+pow(y1,C_6)+pow(y3,C_6))*fea333333+(y1*pow(y2,C_4)*y3+pow(y1,C_4)*y2*y3+y1*
        y2*pow(y3,C_4))*fea111123+fea112233*pow(y1,C_2)*pow(y2,C_2)*pow(y3,C_2)+(pow(y1,C_4)*pow(y4,C_2)+pow(y2,C_4)
        *pow(y4,C_2)+pow(y2,C_4)*pow(y5,C_2)+pow(y3,C_4)*pow(y4,C_2)+pow(y1,C_4)*pow(y5,C_2)+pow(y3,C_4)*pow(y5,C_2))*fea222255;
   //Mout << "s3: " << s3 << endl;

   s4 = s3+(C_3*y1*y3*pow(y5,C_4)+y1*y3*pow(y4,C_4)+C_9*y2*y3*pow(y4,C_2)*pow(y5,C_2)-C_3/
        C_2*y2*y3*pow(y5,C_4)-C_4*y1*y3*pow(y4,C_3)*y5*sqrt(C_3)+y1*y2*pow(y4,C_4)+C_4*y1
        *y2*pow(y4,C_3)*y5*sqrt(C_3)+C_3*y1*y2*pow(y5,C_4)+C_5/C_2*y2*y3*pow(y4,C_4))*fea134444+
        (-y1*pow(y3,C_2)*pow(y5,C_3)*sqrt(C_3)/C_3-C_7/C_3*pow(y1,C_2)*y3*y4*pow(y5,C_2)
        +C_5/C_3*y1*pow(y2,C_2)*pow(y4,C_2)*y5*sqrt(C_3)-13.e0/C_3*pow(y2,C_2)*y3*y4*
        pow(y5,C_2)-C_4/C_3*y2*pow(y3,C_2)*pow(y5,C_3)*sqrt(C_3)-C_7/C_3*pow(y1,C_2)*y2*y4*pow(y5,C_2)
        -16.e0/C_3*y1*pow(y3,C_2)*y4*pow(y5,C_2)+C_4/C_3*pow(y1,C_2)*y3*pow(y4,C_2)*y5*sqrt(C_3)+
        C_4/C_3*pow(y2,C_2)*y3*pow(y5,C_3)*sqrt(C_3)+C_3*pow(y1,C_2)*y2*pow(y4,C_3)+y2*pow(y3,C_2)
        *pow(y4,C_3)+y1*pow(y2,C_2)*pow(y5,C_3)*sqrt(C_3)/C_3+pow(y2,C_2)*y3*pow(y4,C_3)-13.e0/C_3
        *y2*pow(y3,C_2)*y4*pow(y5,C_2)-C_5/C_3*y1*pow(y3,C_2)*pow(y4,C_2)*y5*sqrt(C_3)-C_4/C_3
        *pow(y1,C_2)*y2*pow(y4,C_2)*y5*sqrt(C_3)+C_3*pow(y1,C_2)*y3*pow(y4,C_3)-16e0/C_3*y1*
        pow(y2,C_2)*y4*pow(y5,C_2))*fea233444;
   //Mout << "s4: " << s4 << endl;

   s5 = s4+(C_2*y1*pow(y3,C_2)*pow(y5,C_3)+C_4*y2*pow(y3,C_2)*pow(y5,C_3)+C_4*pow(y2,C_2)*y3*y4*
        pow(y5,C_2)*sqrt(C_3)-C_2*y1*pow(y2,C_2)*pow(y5,C_3)+pow(y1,C_2)*y3*y4*pow(y5,C_2)*sqrt(C_3)+
        C_6*y1*pow(y3,C_2)*pow(y4,C_2)*y5-C_6*y1*pow(y2,C_2)*pow(y4,C_2)*y5-C_3*pow(y1,C_2)*y3*pow(y4,C_2)*
        y5+pow(y1,C_2)*y2*y4*pow(y5,C_2)*sqrt(C_3)+C_4*y1*pow(y3,C_2)*y4*pow(y5,C_2)*sqrt(C_3)-
        C_3*pow(y1,C_2)*y2*pow(y4,C_3)*sqrt(C_3)-C_4*pow(y2,C_2)*y3*pow(y5,C_3)+C_3*pow(y1,C_2)*y2*pow(y4,C_2)
        *y5-pow(y1,C_2)*y2*pow(y5,C_3)+pow(y1,C_2)*y3*pow(y5,C_3)-C_3*pow(y1,C_2)*y3*pow(y4,C_3)*sqrt(C_3)
        +C_4*y2*pow(y3,C_2)*y4*pow(y5,C_2)*sqrt(C_3)+C_4*y1*pow(y2,C_2)*y4*pow(y5,C_2)*sqrt(C_3))*fea113555;
   //Mout << "s5: " << s5 << endl;

   s2 = s5+(-C_2/C_3*pow(y3,C_2)*pow(y4,C_4)*sqrt(C_3)-C_3/C_2*pow(y1,C_2)*pow(y4,C_2)*pow(y5,C_2)
        *sqrt(C_3)-C_3/C_4*pow(y1,C_2)*pow(y5,C_4)*sqrt(C_3)-pow(y2,C_2)*pow(y4,C_3)*y5+C_7
        /C_12*pow(y1,C_2)*pow(y4,C_4)*sqrt(C_3)+pow(y3,C_2)*pow(y4,C_3)*y5+C_3*pow(y3,C_2)*y4*pow(y5,C_3)
        -C_2/C_3*pow(y2,C_2)*pow(y4,C_4)*sqrt(C_3)-C_3*pow(y2,C_2)*y4*pow(y5,C_3))*fea334445+(
        -C_3*y1*y3*pow(y4,C_3)*y5+C_2/C_3*y1*y2*pow(y5,C_4)*sqrt(C_3)-y1*y3*y4*pow(y5,C_3)
        +C_2/C_3*y1*y3*pow(y5,C_4)*sqrt(C_3)+C_3*y1*y2*pow(y4,C_3)*y5-C_7/C_12
        *y2*y3*pow(y5,C_4)*sqrt(C_3)+C_3/C_2*y2*y3*pow(y4,C_2)*pow(y5,C_2)*sqrt(C_3)+y1*
        y2*y4*pow(y5,C_3)+C_3/C_4*y2*y3*pow(y4,C_4)*sqrt(C_3))*fea124555+(C_2*pow(y3,C_2)
        *y4*pow(y5,C_3)*sqrt(C_3)-C_7/C_2*pow(y1,C_2)*pow(y4,C_2)*pow(y5,C_2)+pow(y2,C_2)*pow(y4,C_2)*pow(y5,C_2)
        -pow(y2,C_2)*pow(y4,C_4)-pow(y3,C_2)*pow(y4,C_4)-C_2*pow(y2,C_2)*y4*pow(y5,C_3)*sqrt(C_3)-C_3/C_4
        *pow(y1,C_2)*pow(y5,C_4)+C_5/C_4*pow(y1,C_2)*pow(y4,C_4)+pow(y3,C_2)*pow(y4,C_2)*pow(y5,C_2))*fea334455;
   //Mout << "s2: " << s2 << endl;

   s3 = s2+(-C_6*pow(y4,C_2)*pow(y5,C_4)+C_9*pow(y4,C_4)*pow(y5,C_2)+pow(y5,C_6))*fea555555+(y2*pow(y3,C_3)
        *pow(y4,C_2)+y2*pow(y3,C_3)*pow(y5,C_2)+y1*pow(y3,C_3)*pow(y4,C_2)+y1*pow(y2,C_3)*pow(y4,C_2)+pow(y1,C_3)*y2*pow(y4,C_2)
        +y1*pow(y2,C_3)*pow(y5,C_2)+pow(y1,C_3)*y3*pow(y5,C_2)+pow(y1,C_3)*y3*pow(y4,C_2)+pow(y1,C_3)*y2*pow(y5,C_2)+pow(y2,C_3)
        *y3*pow(y4,C_2)+y1*pow(y3,C_3)*pow(y5,C_2)+pow(y2,C_3)*y3*pow(y5,C_2))*fea233344+(y1*pow(y2,C_3)*pow(y5,C_2)*sqrt(C_3)/C_6
        -pow(y2,C_3)*y3*pow(y5,C_2)*sqrt(C_3)/C_3-y2*pow(y3,C_3)*pow(y5,C_2)
        *sqrt(C_3)/C_3+pow(y1,C_3)*y2*y4*y5-pow(y1,C_3)*y2*pow(y5,C_2)*sqrt(C_3)/C_3-pow(y1,C_3)
        *y3*y4*y5-pow(y1,C_3)*y3*pow(y5,C_2)*sqrt(C_3)/C_3-y1*pow(y3,C_3)*pow(y4,C_2)*sqrt(C_3)
        /C_2+y1*pow(y3,C_3)*pow(y5,C_2)*sqrt(C_3)/C_6-pow(y2,C_3)*y3*y4*y5+y2*pow(y3,C_3)*y4*
        y5-y1*pow(y2,C_3)*pow(y4,C_2)*sqrt(C_3)/C_2)*fea233345+(-C_3*pow(y2,C_3)*y4*pow(y5,C_2)
        +pow(y3,C_3)*pow(y4,C_3)-C_3*pow(y3,C_3)*y4*pow(y5,C_2)-C_3*pow(y1,C_3)*y4*pow(y5,C_2)+pow(y2,C_3)*pow(y4,C_3)+pow(y1,C_3)
        *pow(y4,C_3))*fea111444+(y1*pow(y2,C_3)*pow(y3,C_2)+pow(y1,C_3)*pow(y2,C_2)*y3+pow(y1,C_2)*pow(y2,C_3)*y3+
        y1*pow(y2,C_2)*pow(y3,C_3)+pow(y1,C_2)*y2*pow(y3,C_3)+pow(y1,C_3)*y2*pow(y3,C_2))*fea111233;
   //Mout << "s3: " << s3 << endl;

   s4 = s3+(C_9*pow(y4,C_2)*pow(y5,C_4)-C_6*pow(y4,C_4)*pow(y5,C_2)+pow(y4,C_6))*fea444444+(-C_5
        /C_3*y1*pow(y2,C_2)*pow(y4,C_2)*y5*sqrt(C_3)+y1*pow(y2,C_2)*pow(y4,C_3)-C_4/C_3*pow(y1,C_2)*
        y3*pow(y4,C_2)*y5*sqrt(C_3)-C_2*pow(y1,C_2)*y2*pow(y4,C_3)-y1*pow(y2,C_2)*pow(y5,C_3)*sqrt(C_3)
        /C_3+C_4/C_3*pow(y2,C_2)*y3*y4*pow(y5,C_2)-C_4/C_3*pow(y2,C_2)*y3*pow(y5,C_3)*sqrt(C_3)-
        C_2*pow(y1,C_2)*y3*pow(y4,C_3)+C_7/C_3*y1*pow(y2,C_2)*y4*pow(y5,C_2)-C_2/C_3*pow(y1,C_2)
        *y3*y4*pow(y5,C_2)+y1*pow(y3,C_2)*pow(y4,C_3)+C_4/C_3*y2*pow(y3,C_2)*pow(y5,C_3)*sqrt(C_3)
        +y1*pow(y3,C_2)*pow(y5,C_3)*sqrt(C_3)/C_3+C_4/C_3*pow(y1,C_2)*y2*pow(y4,C_2)*y5*sqrt(C_3)+
        C_4/C_3*y2*pow(y3,C_2)*y4*pow(y5,C_2)+C_5/C_3*y1*pow(y3,C_2)*pow(y4,C_2)*y5*sqrt(C_3)-
        C_2/C_3*pow(y1,C_2)*y2*y4*pow(y5,C_2)+C_7/C_3*y1*pow(y3,C_2)*y4*pow(y5,C_2))*fea133444;
   //Mout << "s4: " << s4 << endl;
   
   s5 = s4+(-pow(y1,C_3)*y2*y4*y5+C_2/C_3*pow(y2,C_3)*y3*pow(y5,C_2)*sqrt(C_3)+y1*pow(y3,C_3)
        *pow(y4,C_2)*sqrt(C_3)/C_2+pow(y1,C_3)*y3*pow(y4,C_2)*sqrt(C_3)/C_2+pow(y1,C_3)*y3*
        pow(y5,C_2)*sqrt(C_3)/C_6+pow(y1,C_3)*y2*pow(y5,C_2)*sqrt(C_3)/C_6+pow(y1,C_3)*y3*y4*y5
        +y1*pow(y2,C_3)*pow(y5,C_2)*sqrt(C_3)/C_6+pow(y1,C_3)*y2*pow(y4,C_2)*sqrt(C_3)/C_2+C_2
        /C_3*y2*pow(y3,C_3)*pow(y5,C_2)*sqrt(C_3)-y1*pow(y2,C_3)*y4*y5+y1*pow(y2,C_3)*pow(y4,C_2)*sqrt(C_3)/C_2+
        y1*pow(y3,C_3)*pow(y5,C_2)*sqrt(C_3)/C_6+y1*pow(y3,C_3)*y4*y5)*fea133345; 
   //Mout << "s5: " << s5 << endl;

   v6 = s5+(-pow(y2,C_2)*y3*pow(y4,C_2)*y5+pow(y1,C_2)*y3*y4*pow(y5,C_2)*sqrt(C_3)/C_3+y2*pow(y3,C_2)*
        pow(y4,C_2)*y5+y2*pow(y3,C_2)*pow(y5,C_3)-y1*pow(y2,C_2)*pow(y5,C_3)+C_4/C_3*pow(y2,C_2)*y3*y4*
        pow(y5,C_2)*sqrt(C_3)+C_4/C_3*y2*pow(y3,C_2)*y4*pow(y5,C_2)*sqrt(C_3)-y1*pow(y2,C_2)*pow(y4,C_2)*
        y5+C_4/C_3*y1*pow(y3,C_2)*y4*pow(y5,C_2)*sqrt(C_3)-pow(y2,C_2)*y3*pow(y5,C_3)+y1*pow(y3,C_2)*
        pow(y5,C_3)+pow(y1,C_2)*y2*y4*pow(y5,C_2)*sqrt(C_3)/C_3-pow(y1,C_2)*y2*pow(y4,C_3)*sqrt(C_3)+
        y1*pow(y3,C_2)*pow(y4,C_2)*y5-pow(y1,C_2)*y3*pow(y4,C_3)*sqrt(C_3)+C_4/C_3*y1*pow(y2,C_2)*
        y4*pow(y5,C_2)*sqrt(C_3))*fea233445+(y2*pow(y3,C_4)*y4*sqrt(C_3)-pow(y1,C_4)*y2*
        y5+pow(y2,C_4)*y3*y4*sqrt(C_3)-pow(y1,C_4)*y3*y4*sqrt(C_3)+y2*pow(y3,C_4)*y5-C_2*
        y1*pow(y2,C_4)*y5+C_2*y1*pow(y3,C_4)*y5-pow(y1,C_4)*y2*y4*sqrt(C_3)+pow(y1,C_4)*y3*y5-pow(y2,C_4)*
        y3*y5)*fea233335+(pow(y2,C_2)*pow(y3,C_4)+pow(y1,C_4)*pow(y3,C_2)+pow(y1,C_2)*pow(y2,C_4)+pow(y2,C_4)*pow(y3,C_2)
        +pow(y1,C_2)*pow(y3,C_4)+pow(y1,C_4)*pow(y2,C_2))*fea222233;
   //Mout << "v6: " << v6 << endl;

   //collect the potential and convert to a.u.
   Nb v_pot=v0+v1+v2+v3+v4+v5+v6;
   //Mout << " v_pot: " << v_pot << endl;
   return v_pot/C_AUTKAYS;
}
/**
 * Evaluate the potential over an assigned point
 * */
Nb AmmModelPot::EvalPotImpl(const std::string& name, const vector<Nuclei>& arGeom) const
{
   Nb v_pot=C_0;
   //it is assumed the first coordinate pertains to the nitrogen atom, otherwise find it
   In n_nuc=arGeom.size();
   //Mout << "n_nuc: " << n_nuc << endl;
   if(n_nuc!=I_4)
      MIDASERROR(" Wrong nr. of nuclei in AmmModelPot");
   In n_pos=I_0;
   for (In i=I_0; i<n_nuc; i++)
   {
      std::string lab=arGeom[i].AtomLabel();
      if (lab.find("N")!= lab.npos)
      {
         n_pos=i;
         break;
      }
   }
   //Mout << " Nitrogen atom position: " << n_pos << endl;
   //units are supposed to be a.u.
   MidasVector curr_str(I_6,C_0);
   In k=I_0;
   //find the distances
   for (In i=I_0; i<n_nuc; i++)
   {
      if (i==n_pos) continue;
      //convert distances to Aangstrom
      Nb r_n=arGeom[n_pos].Distance(arGeom[i])*C_TANG;
      //Mout << " r_n: " << r_n << endl; 
      curr_str[k]=r_n;
      k++;
   }
   //then find the angles (radiants)
   for (In i=I_0; i<n_nuc; i++)
   {
      if (i==n_pos) continue;
      for (In j=i+I_1; j<n_nuc; j++)
      {
         if (j==n_pos) continue;
         Nb ang=arGeom[n_pos].Angle(arGeom[i],arGeom[j]);
         curr_str[k]=ang*C_PI/180.e0;
         k++;
      }
   }
   //Mout << " current structure: " << endl;
   //Mout << " r12: " << curr_str[I_0] << endl;
   //Mout << " r13: " << curr_str[I_1] << endl;
   //Mout << " r14: " << curr_str[I_2] << endl;
   //Mout << " alpha1: " << curr_str[I_3]*180e0/C_PI << endl;
   //Mout << " alpha2: " << curr_str[I_4]*180e0/C_PI << endl;
   //Mout << " alpha3: " << curr_str[I_5]*180e0/C_PI << endl;
   //subtract the value at the minimum
   //v_pot=Potential(curr_str)-GetMinValue();
   v_pot=Potential(curr_str);
   return v_pot;
}
/**
 * Get the value at the global minimum. Parameters as from the article
 * */
Nb  AmmModelPot::GetMinValue()
{
   //consider just two sets of data:
   MidasVector curr_str(I_6,C_0);
   curr_str[I_0]=mParam[I_1];
   curr_str[I_1]=mParam[I_1];
   curr_str[I_2]=mParam[I_1];
   curr_str[I_3]=mParam[I_0]*C_PI/180.e0;
   curr_str[I_4]=mParam[I_0]*C_PI/180.e0;
   curr_str[I_5]=mParam[I_0]*C_PI/180.e0;
   //Mout << " structure at the minimum point: " << curr_str << endl;
   Nb v_pot=Potential(curr_str);
   //Mout << " potential at the minimum: " << v_pot << endl;
   return v_pot;
}
/**
 * Do a line search minimization for finding the saddle point at the D_3h geometry
 * Hand made for this specific case. Calls nr routines
 * **/
Nb AmmModelPot::LineSearch(Nb aA, Nb aB, Nb aC)
{
   Nb tol=sqrt(C_10*C_NB_EPSILON);
   Nb r_min=C_0;
   //Mout << " call brent: " << endl;
   Nb f_min=brent(aA,aB,aC,tol,r_min);
   Mout << " r_min from brent: " << r_min << endl;
   Mout << " f_min value from brent: " << f_min << endl;
   return r_min;
}
/**
 * function evaluation
 * */
Nb AmmModelPot::f(const Nb aX)
{
   //here the D3h geometry is enforced. The bracketing is in the bond length
   Nb ang=C_PI*C_2/C_3;
   //Mout << " aX: " << aX << endl;
   //Mout << " ang: " << C_PI*ang/180.e0 << endl;
   MidasVector curr_str(I_6,C_0);
   curr_str[I_0]=aX;
   curr_str[I_1]=aX;
   curr_str[I_2]=aX;
   curr_str[I_3]=ang;
   curr_str[I_4]=ang;
   curr_str[I_5]=ang;
   //Mout << " current structure is: " << endl;
   //Mout << curr_str << endl;
   Nb v_pot=Potential(curr_str);
   //Mout << " v_pot: " << v_pot << endl;
   return v_pot;
}

/**
* Given a bracketing triplet of abscissa, isolate the minimum to a fractional precision
* of tol, using the Brent's method. From Numerical Recipes, pag. 408
**/
Nb AmmModelPot::brent(const Nb ax, const Nb bx, const Nb cx, const Nb tol, Nb &xmin)
{
   const In ITMAX=100;
   const Nb CGOLD=0.3819660;
   const Nb ZEPS=C_NB_EPSILON*1.0e-3;
   In iter;
   Nb a,b,d=C_0,etemp,fu,fv,fw,fx;
   Nb p,q,r,tol1,tol2,u,v,w,x,xm;
   Nb e=C_0;

        a=(ax < cx ? ax : cx);
        b=(ax > cx ? ax : cx);
        x=w=v=bx;
        fw=fv=fx=f(x);
        Mout << "f(x): " << f(x) << endl;
        for (iter=0;iter<ITMAX;iter++) {
                xm=0.5*(a+b);
                tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
                if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
                        xmin=x;
                        return fx;
                }
                if (fabs(e) > tol1) {
                        r=(x-w)*(fx-fv);
                        q=(x-v)*(fx-fw);
                        p=(x-v)*q-(x-w)*r;
                        q=2.0*(q-r);
                        if (q > 0.0) p = -p;
                        q=fabs(q);
                        etemp=e;
                        e=d;
                        if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
                                d=CGOLD*(e=(x >= xm ? a-x : b-x));
                        else {
                                d=p/q;
                                u=x+d;
                                if (u-a < tol2 || b-u < tol2)
                                        d=SIGN(tol1,xm-x);
                        }
                } else {
                        d=CGOLD*(e=(x >= xm ? a-x : b-x));
                }
                u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
                fu=f(u);
                if (fu <= fx) {
                        if (u >= x) a=x; else b=x;
                        midas::potential::detail::shft3(v,w,x,u);
                        midas::potential::detail::shft3(fv,fw,fx,fu);
                } else {
                        if (u < x) a=u; else b=u;
                        if (fu <= fw || w == x) {
                                v=w;
                                w=u;
                                fv=fw;
                                fw=fu;
                        } else if (fu <= fv || v == x || v == w) {
                                v=u;
                                fv=fu;
                        }
                }
        }
        MIDASERROR("Too many iterations in brent");
        xmin=x;
        return fx;
}
