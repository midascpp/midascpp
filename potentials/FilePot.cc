/**
*************************************************************************
*
* @file                FilePot.cc
* 
* Created:             25/04/2008
* 
* Author:              Eduard Matito (eduard@chem.au.dk)
* 
* Short Description:   Definitions of class members for FilePot
*
* Last modified: Tue Jun 08, 2010  04:01PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#include "potentials/FilePot.h"

// std headers
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/PesInfo.h"
#include "pes/PesFuncs.h"
#include "potentials/MidasPotential.h"
#include "input/AbsPath.h"
#include "util/conversions/VectorFromString.h"

////
// Register for factory
////
ModelPotRegistration<FilePot> registerFilePot("FILEPOT");

/**
 * Create matrix with masses on diagonal.
 *
 * @return   Returns MidasMatrix with atomic masses on diagonal.
 **/
MidasMatrix CreateMassMatrix
   (  const molecule::MoleculeInfo& aMolecule
   ,  const std::vector<Nuclei>&    arGeom
   )
{
   const auto& normalcoord = aMolecule.NormalCoord();
   
   MidasMatrix mass_matrix(normalcoord.Ncols(), C_0);
   for(int i = 0; i < normalcoord.Ncols()/3; ++i)
   {
      auto idx  = i * 3;
      auto mass = arGeom[i].GetMass() * sqrt(C_FAMU);

      mass_matrix[idx    ][idx    ] = mass;
      mass_matrix[idx + 1][idx + 1] = mass;
      mass_matrix[idx + 2][idx + 2] = mass;
   }

   return mass_matrix;
}

/**
 * Calculate Q displacements for input geometry compared to at reference geometry.
 * This is done by first calculating cartesian xyz displacements, 
 * then transforming these with the normalcoordinates (L-mat) to Q-space.
 *
 * What is stored in MidasCpp is:
 *
 * \f[
 *    \bold{U} = \bold{M}^{-\frac{1}{2}} \bold{L}
 * \f]
 * 
 * (we are storing it in row form, so it is actually $U^T$ we are storing in "MoleculeInfo")
 * which means transforming from cartesian displacements to q-displacements is carried out using the following relation:
 * 
 * \f[
 *    \bold{q} = \bold{L}^T \bold{M}^{\frac{1}{2}} \bold{d} = \bold{U}^T \bold{M} \bold{d}
 * \f]
 *
 * @param arGeom   The geometry to calculate displacement for.
 *
 * @return   Returns displacements.
 **/
MidasVector FilePot::CalculateQDisplacements
   (  const std::vector<Nuclei>& arGeom
   )  const
{
   // Calculate cartesian displacement.
   const auto& reference_geom = mMolecule.GetCoord();
   MidasVector displacements(I_3 * arGeom.size());
   for(In iatom = 0; iatom < arGeom.size(); ++iatom)
   {
      In idx = iatom * 3;
      
      displacements[idx    ] = arGeom[iatom].X() - reference_geom[iatom].X();
      displacements[idx + 1] = arGeom[iatom].Y() - reference_geom[iatom].Y();
      displacements[idx + 2] = arGeom[iatom].Z() - reference_geom[iatom].Z();

      auto mass_factor = std::sqrt(C_FAMU) * arGeom[iatom].GetMass();
      
      displacements[idx    ] *= mass_factor;
      displacements[idx + 1] *= mass_factor;
      displacements[idx + 2] *= mass_factor;
   }
   
   // Calculate q-displacement from cartesian displacement and normalcoordinates (the L-matrix) as defined above.
   const auto& normalcoord = mMolecule.NormalCoord();
   MidasVector q(normalcoord.Nrows());
 
   q = normalcoord * displacements;

   return q;
}

/**
 * Evaluate potential at given geometry.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 *
 * @return   Return the value of the potential in the given point.
 **/
Nb FilePot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   In prop_nr = GetPropertyNr(aName);
   // Get q displacements
   auto q = CalculateQDisplacements(arGeom);

   return mOperators[prop_nr].EvaluatePotential(q);
}

/**
 * Evaluate first derivatives of file potential in cartesian xyz space.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 * 
 * @return    Returns the first derivatives.
 **/
MidasVector FilePot::EvalDerImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   In prop_nr = GetPropertyNr(aName);
   auto q = CalculateQDisplacements(arGeom);
   
   auto int_grad = mOperators[prop_nr].EvaluateGradient(q);
   
   const auto& normalcoord = mMolecule.NormalCoord();
   
   MidasVector xyz_grad(normalcoord.Ncols(), C_0);
   MidasMatrix mass_matrix = CreateMassMatrix(mMolecule, arGeom);

   xyz_grad = int_grad * normalcoord * mass_matrix;
   
   return xyz_grad;
}

/**
 * Evaluate second derivatives of file potential in cartesian xyz space.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 * 
 * @return    Returns the second derivatives.
 **/
MidasMatrix FilePot::EvalHessImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   In prop_nr = GetPropertyNr(aName);
   auto q = CalculateQDisplacements(arGeom);

   auto int_hess = mOperators[prop_nr].EvaluateHessian(q);
   
   const auto& normalcoord = mMolecule.NormalCoord();

   MidasMatrix xyz_hess(normalcoord.Ncols(), C_0);
   MidasMatrix mass_matrix = CreateMassMatrix(mMolecule, arGeom);

   xyz_hess = mass_matrix * Transpose(normalcoord) * int_hess * normalcoord * mass_matrix;

   return xyz_hess;
}

/**
 * Constructor
 *
 * @param aInfo   Information on filepot.
 **/
FilePot::FilePot
   (  const ModelPotInfo& aInfo
   )
   :  ModelPot({}, {})
   ,  mMolecule
      ( molecule::MoleculeFileInfo
         {  midas::input::SearchForFile(
               const_cast<ModelPotInfo&>(aInfo)["MOLECULEFILE"]
            )
         ,  "MIDAS"
         }
      )
{
   std::vector<std::string> operators = midas::util::SeperateLinesIntoVector(const_cast<ModelPotInfo&>(aInfo)["OPERATORFILE"]);
   for(const auto& oper : operators)
   {
      auto split = midas::util::StringVectorFromString(oper,' ');
      mOperators.emplace_back(split.front(), mMolecule.GetModeLabels());
      AddPropertyName(split.back());
      AddProvides(potentials::provides::value | potentials::provides::first_derivatives | potentials::provides::second_derivatives);
   }
}
