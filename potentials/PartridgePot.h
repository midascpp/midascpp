/**
************************************************************************
*
*  @file                PartridgePot.h
* 
*  Created:             01/10/2007
* 
*  Author:              D. Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class PartridgePot for storing the
*                       Partridge-Schwenke potential for water
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef NASAMODELPOT_H_INCLUDED
#define NASAMODELPOT_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

class PartridgePot 
   : public ModelPot
{
   private:
      In** idx;        ///< Contains the parameters for the Partridge-Schwenke potential
      In ** idxm;
      In ** idxD;
      MidasVector c5zA;
      MidasVector cbasis;
      MidasVector ccore;
      MidasVector crest;
      MidasVector cmass;
      MidasVector coefD;
      Nb reoh;
      Nb thetae;
      Nb b1;
      Nb roh;
      Nb alphaoh;
      Nb deohA;
      Nb phh1A;
      Nb phh2;
      Nb f5z;
      Nb fbasis;
      Nb fcore;
      Nb frest;
      Nb b1D; 
      Nb a; 
      Nb b;
      Nb c0;
      Nb c1;
      Nb c2;
      bool mHasBeenInit;
      
      void InitConsts();                                       ///< Initialize constants
      void InitArrays();                                       ///< Initialize arrays
      void Init();                                             ///< Initialize 

      //! Check the molecular structure
      MidasMatrix CheckMolecularStructure(const std::vector<Nuclei>& arGeom) const;
      
      //! Compute the Partridge-Schwenke potential
      void Pot(const MidasMatrix& r1, MidasMatrix&  dr1, Nb& e1) const; 
      
      //! Evaluate the potential for a given geometry
      Nb EvalPotImpl(const std::string& aName, const vector<Nuclei>& arGeom) const;
      
      //! Evaluate the first derivative of potential for a given geometry
      MidasVector EvalDerImpl(const std::string& aName, const vector<Nuclei>& arGeom) const;

   public:
      //! Default Constructor
      PartridgePot();
      
      //! Constructor from ModelPotInfo
      PartridgePot(const ModelPotInfo&);
      
      //! Destructor
      ~PartridgePot();

      //! Get type of model potential
      std::string Type() const { return "NASAMODELPOT"; }

      //! Output overloading
      friend ostream& operator<<(ostream& arOut, const PartridgePot& arPot);   
};

#endif /*  NASAMODELPOT_H_INCLUDED */
