/**
*************************************************************************
*
* @file                PartridgePot.cc
*
* Created:             01/10/2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Definitions of class members for the 
*                      Partridge-Schwenke water potential
*
* Last modified:       01/17/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/


// std headers
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>
#include <ios>
#include <iomanip>
#include <sstream>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/ProgramSettings.h"
#include "pes/Pes.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "potentials/PartridgePot.h"
#include "mpi/Impi.h"

////
// Register for factory
////
ModelPotRegistration<PartridgePot> registerPartridgePot("PARTRIDGEPOT");

/**
 * Default constructor
 **/
PartridgePot::PartridgePot
   (
   )
   :  ModelPot({"GROUND_STATE_ENERGY"}, {potentials::provides::value | potentials::provides::first_derivatives})
{
   //allocate space only
   idx = new In* [245];
   for (In i=0; i<245; i++) 
   {
      idx[i] = new In [3];
   }
   idxm = new In* [9];
   for (In i=0; i<9; i++)   
   {
      idxm[i] = new In [3];
   }
   idxD = new In* [84];
   for (In i=0; i<84; i++)  
   {
      idxD[i] = new In [3];
   }
   mHasBeenInit=false;
}

/**
 *
 **/
PartridgePot::PartridgePot
   (  const ModelPotInfo& aInfo
   )
   :  PartridgePot()
{
   Init();
}

/**
* Deconstructor
* */
PartridgePot::~PartridgePot()
{
   for (In i=I_0;i<245;i++) delete[] idx[i];
   delete[] idx;
   for (In i=I_0;i<9;i++) delete[] idxm[i];
   delete[] idxm;
   for (In i=I_0;i<84;i++) delete[] idxD[i];
   delete[] idxD;
}

/**
 * Initialize object
 * */
void PartridgePot::Init()
{
   InitConsts();
   InitArrays();
   mHasBeenInit=true;
}

/**
* Initialize the constants
* */
void PartridgePot::InitConsts()
{
   //two body parameters
   reoh    = 0.958649E+00;
   thetae  = 104.3475E+00;
   b1      = 2.0E+00;
   roh     = 0.9519607159623009E+00;
   alphaoh = 2.587949757553683E+00;
   deohA   = 42290.92019288289E+00;
   phh1A   = 16.94879431193463E+00;
   phh2    = 12.66426998162947E+00;
   
   //scaling factors for contributions to empirical potential
   f5z     = 0.99967788500000E+00;
   fbasis  = 0.15860145369897E+00;
   fcore   = -1.6351695982132E+00;
   frest   = C_1;
   //f5z     = C_1;
   //fbasis  = C_0;
   //fcore   = C_0;
   //frest   = C_0;
   
   b1D     = C_1;
   a       = 0.2999;
   b       = -0.6932;
   c0      = 1.0099;
   c1      = -0.1801;
   c2      = 0.0892;
}
/**
* Initialize the arrays
* */
void PartridgePot::InitArrays()
{
   string datadir=input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR");
   string filename="nasa_parms";
   string datafile=datadir+"/"+filename;
   //Mout << " reading file " << datafile << endl; 
   //ifstream nasa_parms_file(datafile.c_str(),ios::in);
   auto nasa_parms_file = midas::mpi::FileToStringStream(datafile);
   if (!nasa_parms_file)
      MIDASERROR(" nasa_parms file not found ");
   string lab="idx";
   bool found=SearchInFile(lab, nasa_parms_file,"idx_array","nasa_parms");
   if (!found)
      MIDASERROR(" idx data not found ");
   string end_lab="EndOfData";
   string s;
   vector<string> idx_s;
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {
      //Mout << " string s read into: " << s << endl; 
      istringstream s_input(s);
      string s_new;
      while(s_input>>s_new) 
      {
         //Mout << " s_new: " << s_new << endl;
         idx_s.push_back(s_new);
      }
      getline(nasa_parms_file,s);
   }
   //print out array idx:
   //Mout << " Array of strings idx_s: " << endl;
   //for (vector<string>::const_iterator vsi=idx_s.begin(); vsi!=idx_s.end(); vsi++)
      //Mout << " " << *vsi;
   //Mout << endl;
   //check the dimension of idx_s
   In size=idx_s.size();
   if (size!=735)
      MIDASERROR(" idx array not read properly ");
   vector<string>::const_iterator vsi=idx_s.begin();
   for (In j=0; j<3; j++)
      for (In i=0; i<245; i++)
      {
         //Mout << " vsi: " << *vsi << endl; 
         idx[i][j]=InFromString(*vsi);
         vsi++;
      }
   //Mout << " Array idx read into: " << endl;
   //for (In j=0; j<3; j++)
   //{
      //for (In i=0; i<245; i++)
         //Mout << " " << setw(6) << idx[i][j];
      //Mout << endl;
   //}
   //read into array c5zA
   idx_s.clear();
   lab="c5zA";
   found=SearchInFile(lab, nasa_parms_file,"c5zA_array","nasa_parms");
   if (!found)
      MIDASERROR(" c5zA data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {
      istringstream s_input(s);
      string s_new;
      while(s_input>>s_new) idx_s.push_back(s_new);
      getline(nasa_parms_file,s);
   }
   if (idx_s.size()!=245)
      MIDASERROR(" c5zA array not read properly ");
   c5zA.SetNewSize(245);
   for (In i=0; i<c5zA.Size(); i++) c5zA[i]=NbFromString(idx_s[i]);
   //read into array cbasis
   idx_s.clear();
   lab="cbasis";
   found=SearchInFile(lab, nasa_parms_file,"cbasis_array","nasa_parms");
   if (!found)
      MIDASERROR(" cbasis data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {                 
      istringstream s_input(s);    
      string s_new;                      
      while(s_input>>s_new) idx_s.push_back(s_new);
      getline(nasa_parms_file,s);                    
   }                                                    
   if (idx_s.size()!=245)
      MIDASERROR(" cbasis array not read properly ");
   cbasis.SetNewSize(245);
   for (In i=0; i<cbasis.Size(); i++) cbasis[i]=NbFromString(idx_s[i]);
   //Mout << " Array cbasis read into: " << endl;
   //Mout << cbasis << endl;
   //read into array ccore
   idx_s.clear();
   lab="ccore";
   found=SearchInFile(lab, nasa_parms_file,"ccore_array","nasa_parms");
   if (!found) 
      MIDASERROR(" ccore data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {                               
      istringstream s_input(s);                  
      string s_new;                                    
      while(s_input>>s_new) idx_s.push_back(s_new);          
      getline(nasa_parms_file,s);                                  
   }                                                                  
   if (idx_s.size()!=245)  
      MIDASERROR(" ccore array not read properly ");
   ccore.SetNewSize(245);
   for (In i=0; i<ccore.Size(); i++) ccore[i]=NbFromString(idx_s[i]);
   //Mout << " Array ccore read into: " << endl;
   //Mout << ccore << endl;
   //read into array ccrest
   idx_s.clear();
   lab="crest";
   found=SearchInFile(lab, nasa_parms_file,"crest_array","nasa_parms");
   if (!found) 
      MIDASERROR(" crest data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {                                 
      istringstream s_input(s);                  
      string s_new;                                    
      while(s_input>>s_new) idx_s.push_back(s_new);          
      getline(nasa_parms_file,s);                                  
   }                                                                  
   if (idx_s.size()!=245)  
      MIDASERROR(" crest array not read properly ");
   crest.SetNewSize(245);
   for (In i=0; i<crest.Size(); i++) crest[i]=NbFromString(idx_s[i]);
   //Mout << " Array crest read into: " << endl;
   //Mout << crest << endl;
   //read into array idxm
   lab="idxm";
   found=SearchInFile(lab, nasa_parms_file,"idxm_array","nasa_parms");
   if (!found)
      MIDASERROR(" idxm data not found ");
   end_lab="EndOfData";
   idx_s.clear();
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {
      istringstream s_input(s);
      string s_new; 
      while(s_input>>s_new) idx_s.push_back(s_new);
      getline(nasa_parms_file,s);
   }  
   //check the dimension of idx_s
   size=idx_s.size();
   if (size!=27)
      MIDASERROR(" idx array not read properly ");

   In p_t=0;
   for (In j=0; j<3; j++)
      for (In i=0; i<9; i++)
      {
         idxm[i][j]=InFromString(idx_s[p_t]);
         p_t++;
      }  
   //Mout << " Array idxm read into: " << endl;
   //for (In j=0; j<3; j++)
   //{
      //for (In i=0; i<9; i++)
         //Mout << " " << idxm[i][j];
      //Mout << endl;
   //}
   //read into array cmass
   idx_s.clear();
   lab="cmass";
   found=SearchInFile(lab, nasa_parms_file,"cmass_array","nasa_parms");
   if (!found) 
      MIDASERROR(" cmass data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {                               
      istringstream s_input(s);                  
      string s_new;                                    
      while(s_input>>s_new) idx_s.push_back(s_new);          
      getline(nasa_parms_file,s);                                  
   }                                                                  
   if (idx_s.size()!=9)  
      MIDASERROR(" cmass array not read properly ");
   cmass.SetNewSize(9);
   for (In i=0; i<cmass.Size(); i++) cmass[i]=NbFromString(idx_s[i]);
   //Mout << " Array cmass read into: " << endl;
   //Mout << cmass << endl;
   //read array idxD
   lab="idxD";
   found=SearchInFile(lab, nasa_parms_file,"idxD_array","nasa_parms");
   if (!found)
      MIDASERROR(" idxD data not found ");
   end_lab="EndOfData";
   idx_s.clear();
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {
      istringstream s_input(s);
      string s_new; 
      while(s_input>>s_new) idx_s.push_back(s_new);
      getline(nasa_parms_file,s);
   }  
   //check the dimension of idx_s
   size=idx_s.size();
   if (size!=252)
      MIDASERROR(" idxD array not read properly ");
   p_t=0;
   for (In j=0; j<3; j++)
      for (In i=0; i<84; i++)
      {
         idxD[i][j]=InFromString(idx_s[p_t]);
         p_t++;
      }  
   //Mout << " Array idxD read into: " << endl;
   //for (In j=0; j<3; j++)
   //{
      //for (In i=0; i<245; i++)
         //Mout << " " << idx[i][j];
      //Mout << endl;
   //}
   //read coefD array
   lab="coefD";
   idx_s.clear();
   found=SearchInFile(lab, nasa_parms_file,"coefD_array","nasa_parms");
   if (!found)
      MIDASERROR(" coefD data not found ");
   end_lab="EndOfData";
   getline(nasa_parms_file,s);
   while(!(s.find(end_lab)!=s.npos) && !nasa_parms_file.eof())
   {   
      istringstream s_input(s);
      string s_new;  
      while(s_input>>s_new) idx_s.push_back(s_new);
      getline(nasa_parms_file,s);             
   }   
   if (idx_s.size()!=84)
      MIDASERROR(" coefD array not read properly ");
   coefD.SetNewSize(84);
   for (In i=0; i<coefD.Size(); i++) 
   {
      coefD[i]=NbFromString(idx_s[i]);
   }
   //Mout << " Array coefD read into: " << endl;
   //Mout << coefD << endl;
   //Mout << " Initialization of Partridge parameters done..." << endl;
}


/**
 * Check that the molecular structure fits the potential,
 * i.e. that the input molecule is a water monomer.
 * If not will throw a MIDASERROR.
 *
 * @param   arGeom  The nuclear geometry.
 *
 * @return  Returns MidasMatrix with geometry, which is needed for PartridgePot::Pot().
 **/
MidasMatrix PartridgePot::CheckMolecularStructure
   (  const std::vector<Nuclei>& arGeom
   )  const
{
   In n_nuc=arGeom.size();
   if(n_nuc!=3)
      MIDASERROR(" Wrong nr. of nuclei in WaterPot");
   //it is assumed that atom #1 is Oxigen check it
   bool o_fnd=false;
   bool h1_fnd=false;
   bool h2_fnd=false;
   In p_o=-1;
   In p_h1=-1;
   In p_h2=-1;
   for (In i=0; i<n_nuc; i++)
   {
      string lab=arGeom[i].AtomLabel();
      if (!o_fnd && lab.substr(0,1)=="O")
      {
         p_o=i;
         o_fnd=true;
      }
      else if (!h1_fnd && lab.substr(0,1)=="H")
      {
         p_h1=i;
         h1_fnd=true;
      }
      else if(!h2_fnd && lab.substr(0,1)=="H")
      {
         p_h2=i;
         h2_fnd=true;
      }
      else
      {
        MIDASERROR(" labels not correct in input structure ");
      }
   }

   if (!(o_fnd && h1_fnd && h2_fnd))
   {
      MIDASERROR(" geometry not properly read into ");
   }

   if (gDebug) 
   {
      Mout << " Geometry input in PartridgePot::EvalPot(): " << endl;
      for (In i=0; i<n_nuc; i++)
         Mout << arGeom[i]<< endl;
      Mout << endl;
   }
   
   //now fill the arrays:
   MidasMatrix r1(3,C_0);
   r1[0][0]=arGeom[p_o].X();
   r1[1][0]=arGeom[p_o].Y();
   r1[2][0]=arGeom[p_o].Z();
   r1[0][1]=arGeom[p_h1].X();
   r1[1][1]=arGeom[p_h1].Y();
   r1[2][1]=arGeom[p_h1].Z();
   r1[0][2]=arGeom[p_h2].X();
   r1[1][2]=arGeom[p_h2].Y();
   r1[2][2]=arGeom[p_h2].Z();
   // scale to Aangstrom
   r1.Scale(C_TANG);

   return r1;
}

/** 
 * calculates the energy (e1) and the derivatives (dr1) of a water monomer
 * (coordinates in the array r1). The potential has been developed by
 * "H. Partridge and D. W. Schwenke, J. Chem. Phys. 106, 4618 (1997)".
 * Some extra code for the calculation of the energy derivatives has been
 * added  by C. J. Burnham.
 *
 * @param r1     The geometry.
 * @param dr1    On output holds energy derivatives.
 * @param e1     On output holds energy.
 **/ 
void PartridgePot::Pot
   (  const MidasMatrix& r1
   ,  MidasMatrix&  dr1
   ,  Nb& e1
   )  const
{
   MidasVector ROH1(3,C_0);
   for (In i=0; i<3; i++)  ROH1[i]=r1[i][1]-r1[i][0];
   MidasVector ROH2(3,C_0);
   for (In i=0; i<3; i++)  ROH2[i]=r1[i][2]-r1[i][0];
   MidasVector RHH(3,C_0);
   for (In i=0; i<3; i++)  RHH[i]=r1[i][1]-r1[i][2];

   //Mout << " Midasvector ROH1:  " << endl;
   //Mout << ROH1 << endl;
   //Mout << " Midasvector ROH2:  " << endl;
   //Mout << ROH2 << endl;
   //Mout << " MidasVector RHH:   " << endl;
   //Mout << RHH << endl;

   Nb dROH1 = ROH1.Norm();
   Nb dROH2 = ROH2.Norm();
   Nb dRHH  = RHH.Norm();
   //Mout << "  dROH1: " << dROH1 << "  dROH2: " << dROH2 << "  dROHH: " << dRHH;

   Nb costh;
   costh=( ROH1[0]*ROH2[0] + ROH1[1]*ROH2[1] + ROH1[2]*ROH2[2] ) / (dROH1*dROH2);
  
     
   MidasVector c5z(245,C_0);
   for (In i=0; i< 245; i++) 
      c5z[i]=f5z*c5zA[i]+fbasis*cbasis[i]+fcore*ccore[i]+frest*crest[i];
   //Mout << " c5z vector: " << endl;
   //Mout << c5z << endl;

   Nb deoh = f5z*deohA;
   Nb phh1 = f5z*phh1A;
   phh1 *= exp(phh2);
   Nb costhe = -.24780227221366464506E+00;
   Nb exp1 = exp(-alphaoh*(dROH1-roh));
   Nb exp2 = exp(-alphaoh*(dROH2-roh));
   Nb Va = deoh*(exp1*( exp1 - C_2 ) + exp2 * ( exp2 - C_2 ));
   Nb Vb  = phh1 * exp( -phh2 * dRHH );
   Nb dVa1= C_2 * alphaoh * deoh * exp1 * (C_1 - exp1 ) / dROH1;
   Nb dVa2= C_2 * alphaoh * deoh * exp2 * (C_1 - exp2 ) / dROH2;
   Nb dVb = -phh2 * Vb / dRHH;
   Nb x1 = (dROH1-reoh)/reoh;
   Nb x2 = (dROH2-reoh)/reoh;
   Nb x3 = costh - costhe;
      
   MidasMatrix fmat(I_16,I_3);

   for (In i=0; i<3; i++) fmat[0][i] = C_0; 
   for (In i=0; i<3; i++) fmat[1][i] = C_1; 
   for (In j=2; j<=15; j++) 
   {
      fmat[j][0] = fmat[j-1][0] *x1;
      fmat[j][1] = fmat[j-1][1] *x2;
      fmat[j][2] = fmat[j-1][2] *x3;
   }
   //Mout << " Matrix fmat: " << endl;
   //Mout << fmat << endl;

   Nb efac = exp(-b1 *((dROH1-reoh)*(dROH1-reoh) + (dROH2-reoh)*(dROH2-reoh)));
   //Mout << "efac        "  <<efac  << endl;
   Nb sum0=C_0;  
   Nb sum1=C_0;
   Nb sum2=C_0;
   Nb sum3=C_0;

   In inI, inJ, inK;

   for (In j=1; j<245; j++) 
   {
      inI = idx[j][0];
      inJ = idx[j][1];
      inK = idx[j][2];
      sum0 = sum0 + c5z[j] *  ( fmat[inI][0] * fmat[inJ][1] + fmat[inJ][0]*fmat[inI][1]) *fmat[inK][2];
      sum1 = sum1 + c5z[j] *  ( (inI-1) * fmat[inI-1][0] * fmat[inJ][1] + (inJ-1) * fmat[inJ-1][0] * fmat[inI][1]) * fmat[inK][2];
      sum2 = sum2 + c5z[j] *  ( (inJ-1)*fmat[inI][0]*fmat[inJ-1][1] + (inI-1)*fmat[inJ][0]*fmat[inI-1][1]  )*fmat[inK][2];
      sum3 = sum3 + c5z[j] *  ( fmat[inI][0]*fmat[inJ][1] + fmat[inJ][0]*fmat[inI][1]) * (inK-1)*fmat[inK-1][2];
   }
   //do the energy
   //Mout << " sum0  " << sum0  << endl;
   //Mout << " sum1  " << sum1  << endl;
   //Mout << " sum2  " << sum2  << endl;
   //Mout << " sum3  " << sum3  << endl;
   //Mout << " c5z[0]  " << c5z[0]  << '\n';
   Nb Vc=C_2*c5z[0]+efac*sum0;
   //Mout << "Vc   " << Vc  << endl;
   //Mout << "Va   " << Va  << endl;
   //Mout << "Vb   " << Vb  << endl;
   e1=Va+Vb+Vc;
   e1+=0.44739574026257;   //correction
   e1/=C_AUTKAYS; //cm-1 --> a.u.

   //calculate the derivatives
   Nb dVcdr1  = (-C_2*b1*efac*(dROH1-reoh)*sum0+efac*sum1/reoh)/dROH1;
   Nb dVcdr2  = (-C_2*b1*efac*(dROH2-reoh)*sum0+efac*sum2/reoh)/dROH2;
   Nb dVcdcth = efac*sum3;

   for (In i=0; i< 3; i++) 
   {
      dr1[i][1]= dVa1 * ROH1[i]   + dVb * RHH[i] + dVcdr1 * ROH1[i] + dVcdcth * (ROH2[i] / (dROH1*dROH2) - costh * ROH1[i] / (dROH1 * dROH1));
      dr1[i][2]= dVa2 * ROH2[i]   - dVb * RHH[i] + dVcdr2 * ROH2[i] + dVcdcth * (ROH1[i] / (dROH1*dROH2) - costh * ROH2[i] / (dROH2 * dROH2));
      dr1[i][0]= -(dr1[1][i]+dr1[2][i]);
   }
   
   // Conversion  cm-1 --> Kcal/mol
   for (In i=0; i< 3; i++) 
   {
      for (In j=0; j<3 ; j++) 
      {
         dr1[i][j] /= C_AUTKAYS;   //check again...
      }
   }
}     

/**
 * Evaluate the potential over an assigned geometry
 **/
Nb PartridgePot::EvalPotImpl
   (  const std::string& name
   ,  const vector<Nuclei>& arGeom
   )  const
{
   auto r1 = CheckMolecularStructure(arGeom);

   MidasMatrix dr1(3, C_0);
   Nb e1 = C_0;
   Pot(r1, dr1, e1);
   
   return e1;
}

/**
 * Evaluate the potential over an assigned geometry
 **/
MidasVector PartridgePot::EvalDerImpl
   (  const std::string& aName
   ,  const vector<Nuclei>& arGeom
   )  const
{
   auto r1 = CheckMolecularStructure(arGeom);

   MidasMatrix dr1(3, C_0);
   Nb e1 = C_0;
   Pot(r1, dr1, e1);

   // load derivative into MidasVector
   MidasVector der(3*3);
   for(int i = 0; i < dr1.Nrows(); ++i)
   {
      int idx = 3 * i;
      for(int j = 0; j < dr1.Ncols(); ++j)
      {
         der.at(idx + j) = dr1[i][j];
      }
   }

   return der;
}

/**
 * Output overloading
**/
ostream& operator<<(ostream& arOut, const PartridgePot& arPot)
{
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(ios::showpoint);
   if (!arPot.mHasBeenInit) 
   {
      Mout << " PartridgePot class has not been initialized yet " << endl;
      return arOut;
   }
   Mout << " Private members of class PartridgePot: " << endl;
   Mout << " List of constants: " << endl;
   Mout << " reoh: " << arPot.reoh << endl;
   Mout << " thetae: " << arPot.thetae<< endl;
   Mout << " b1: " << arPot.b1<< endl;
   Mout << " roh: " << arPot.roh<< endl;
   Mout << " alphaoh: " << arPot.alphaoh<< endl;
   Mout << " deohA: " << arPot.deohA<< endl;
   Mout << " phh1A: " << arPot.phh1A<< endl;
   Mout << " phh2: " << arPot.phh2<< endl;
   Mout << " f5z: " << arPot.f5z<< endl;
   Mout << " fbasis: " << arPot.fbasis<< endl;
   Mout << " fcore: " << arPot.fcore<< endl;
   Mout << " frest: " << arPot.frest<< endl;
   Mout << " b1D: " << arPot.b1D<< endl;
   Mout << " a: " << arPot.a<< endl; 
   Mout << " b: " << arPot.b<< endl;
   Mout << " c0: " << arPot.c0<< endl;
   Mout << " c1: " << arPot.c1<< endl;
   Mout << " c2: " << arPot.c2<< endl;
   Mout << " Arrays: " << endl;
   Mout << endl;
   Mout << " Array idx: " << endl;
   for (In j=0; j<3; j++)
   {
      Mout << " column: " << j << endl;
      for (In i=0; i<245; i++)
         Mout << " " << arPot.idx[i][j];
      Mout << endl;
   }
   Mout << " Array c5zA: " << endl;
   Mout << arPot.c5zA << endl;
   Mout << " Array cbasis: " << endl;
   Mout << arPot.cbasis << endl;
   Mout << " Array ccore: " << endl;
   Mout << arPot.ccore << endl;
   Mout << " Array crest: " << endl;
   Mout << arPot.crest << endl;
   return arOut;
}
