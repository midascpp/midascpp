/**
************************************************************************
*
*  @file                DataTable.cc
* 
*  Created:             
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

// std headers
#include <cstdlib>
#include <map>
#include <vector>

// midas headers
#include "DataTable.h"

// using declarations
using std::vector;
using std::map;

/**
*  Default constructor. Set up parameters.
**/
DataTable::DataTable() {
   // Carbon parameters
   CParameters.clear();
   // The first values are just zero (WHY?)
   vector<Nb> values(7,0.0);
   CParameters.insert(std::make_pair(0,values));
   // now make first row
   values[0]=0.0;
   values[1]=0.2817216000000E+00;
   values[2]=0.1062912000000E+01;
   values[3]=0.2136736000000E+01;
   values[4]=0.2533952000000E+01;
   values[5]=0.1554736000000E+01;
   values[6]=0.3863296000000E+00;
   // 2nd and 3rd row are alike
   CParameters.insert(std::make_pair(1,values));
   CParameters.insert(std::make_pair(2,values));
   // make 3rd row
   values[0]=0.0;
   values[1]=0.6900668660000E+00;
   values[2]=0.5460691360000E+01;
   values[3]=0.2301345680000E+02;
   values[4]=0.5491519344000E+02;
   values[5]=0.6862037040000E+02;
   values[6]=0.3470897779200E+02;
   CParameters.insert(std::make_pair(3,values));
   // make 4th row
   values[0]=0.0;
   values[1]=0.3754490870000E+00;
   values[2]=0.1407252749388E+01;
   values[3]=0.2255103926323E+01;
   values[4]=0.2028902219952E+01;
   values[5]=0.1426981217906E+01;
   values[6]=0.5063107994308E+00;
   CParameters.insert(std::make_pair(4,values));
   // make 4th row
   values[0]=0.0;
   values[1]=0.2718558000000E+00;
   values[2]=0.4892727456293E+00;
   values[3]=-0.4328199017473E+00;
   values[4]=-0.5616795197048E+00;
   values[5]=0.1270874966906E+01;
   values[6]=-0.3750409108350E-01;
   CParameters.insert(std::make_pair(5,values));
   // that was the C parameters, do the same for the H parameters
   HParameters.clear();
   // The first values are just zero (WHY?)
   values.clear();
   values.assign(7,0.0);
   HParameters.insert(std::make_pair(0,values));
   // make 2nd row
   values[0]=0.0;
   values[1]=270.467795364007301;
   values[2]=1549.701314596994564;
   values[3]=3781.927258631323866;
   values[4]=4582.337619544424228;
   values[5]=2721.538161662818368;
   values[6]=630.658598136730774;
   HParameters.insert(std::make_pair(1,values));
   // make 3rd row
   values[0]=0.0;
   values[1]=16.956325544514659;
   values[2]=-21.059084522755980;
   values[3]=-102.394184748124742;
   values[4]=-210.527926707779059;
   values[5]=-229.759473570467513;
   values[6]=-94.968528666251945;
   HParameters.insert(std::make_pair(2,values));
   // make 4th row
   values[0]=0.0;
   values[1]=19.065031149937783;
   values[2]=2.017732531534021;
   values[3]=-2.566444502991983;
   values[4]=3.291353893907436;
   values[5]=-2.653536801884563;
   values[6]=0.837650930130006;
   HParameters.insert(std::make_pair(3,values));
}

vector<Nb> DataTable::GetCParameters(In aI) {
   map<In,vector<Nb> >::iterator it=CParameters.find(aI);
   return it->second;
}

vector<Nb> DataTable::GetHParameters(In aI) {
   map<In,vector<Nb> >::iterator it=HParameters.find(aI);
   return it->second;
}
