/**
************************************************************************
*
*  @file                ArNtwopPot.cc
* 
*  Created:             04/04/2008
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class for computing model potential for (ArN2)^+ interaction
*                       Parameterization from R. Candori et al. J. Chem. Phys. 115(2001), 8888
*  Last modified: Thu Dec 03, 2009  09:44AM ove
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#include "potentials/ArNtwopPot.h"

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "inc_gen/Const.h"
#include "nuclei/Nuclei.h"

namespace midas::potential::detail
{
   /// Helper functions for QL algorithm
   void tred2(MidasMatrix& a, MidasVector& d, MidasVector& e);
   void tqli(MidasVector& d, MidasVector& e, MidasMatrix& z);
}


/**
* @brief Default constructor
*        By default consider a model for the (ArN2)^+ collisional problem
*        as parametrized by R. Candori et al. J. Chem. Phys. 115(2001), 8888
**/
ArNtwopPot::ArNtwopPot() : ModelPot({"GROUND_STATE_ENERGY"}, {potentials::provides::value})
{
   //The selected pes is by default the mIpes=I_0
   mIpes=I_0;
   //initialization of the common variables
   C6022=C_0; 
   
   //V000 (MSV)  (Ar+ - N2) system
   RM0   = 3.1300e0;                                           
   EPSI0 = 120.e0;                                
   BETA  = 5.500e0;                                  
   C4000 = 12.672e3*1.28e0;                            
   C6000 = 24.0e3;
   X3    = 1.1000e0;                                            
   X4    = 1.5500e0;   
   
   //V000 (MSV)  (Ar - N2+)
   RM0ndp   = 3.4900e0;
   EPSI0ndp = 89.6e0;
   BETAndp  = 5.50e0;
   C4000ndp = 11.808e3*1.551e0;
   C6000ndp = 51.500e3;
   X3ndp    = 1.100e0;
   X4ndp    = 1.550e0;
   
   //V022 (Ar+ - N2) system
   //For the present system this term has been taken to be zero at all R
   A022=C_0;
   ALFA022=C_0;
   GAMMA022=C_0;  
   
   //V202  (Ar+ - N2)
   //polarizability of N2
   Nb at=1.43e0;
   Nb ap=2.43e0;
   //Nb am=1.76e0;
   Nb an6=(ap-at)/(ap+C_2*at);
   Nb an4=an6*C_2;

   A202=2.1e6;
   ALFA202=3.300e0;
   C4202=C4000*an4;
   Q202=-4435.3e0;
   
   //V202  (Ar - N2+)
   //polarizability of N2+
   Nb atndp=1.48e0;
   Nb apndp=2.96e0;
   //Nb amndp=1.973e0;
   Nb an6ndp=(apndp-atndp)/(apndp+C_2*atndp);
   
   A202ndp=5.5e6;
   ALFA202ndp=3.300e0;
   C6202ndp=C6000ndp*an6ndp;
   
   //V220 (Ar+ - N2) system
   A220=A202;
   ALFA220=ALFA202;
   
   //V222 (Ar+ - N2) system
   A222=-A202;
   ALFA222=ALFA202;
   
   // V224 quadrupolo-quadrupolo)  (Ar+ - N2) system
   // -1.1  N2
   // 2.91  Ar+ (da Nikitin,)
   C224=-1.1e0*2.91e0*1129.0e0;
   
   //Setup Spline parameters
   Spline();
   Splinendp();
}

/**
* Compute the potential at a given point
* Converted from the original arn2.f routine
* by R. Candori et al. J. Chem. Phys. 115(2001), 8888 
* */
Nb ArNtwopPot::Pot(Nb aR, Nb atheta) const
{
   //This subroutine computes five adiabatic energy surfaces for the system (Ar-N2)+ in terms of Jacobi coordinates:
   //R(Angstconst
   //theta(degrees)
   //Charge transfer and spin-orbit interactions are also considered
   //zero energy exit channel Ar + N2+(X) N=0
   //entrance channel:
   //ipes = 1                Ar+(P3/2,3/2) + N2(X)
   //ipes = 2                Ar+(P3/2,1/2) + N2(X)
   //ipes = 3                Ar+(P1/2,1/2) + N2(X)
   //ipes = 4                 Ar  + N2+(A)
   //exit channel:
   //ipes = 0                Ar + N2+(X) (N=0)
   //N vibrational states of N2+ 
   //VI potenial energy curves of entrance and exit channels in meV
   In n_p=I_10;
   In g = -I_2*178/I_3;

   MidasMatrix vmatso(n_p,C_0);
   MidasVector d(n_p,C_0);
   MidasVector e(n_p,C_0);
   
   Nb p000=v000(aR);
   Nb p202=v202(aR);
   Nb p022=v022(aR);
   Nb p220=v220(aR);
   Nb p222=v222(aR);
   Nb p224=v224(aR);

   Nb p000ndp=v000nduep(aR);
   Nb p202ndp=v202nduep(aR);

   //Nb thetarad=theta*C_PI/180.0e0 //not needed if theta in rad already
   Nb thetarad=atheta;
   //harmonic expressions
   Nb c20 = C_I_2*(C_3*cos(thetarad)*cos(thetarad)-C_1);
   Nb c21 = - sqrt(C_3/C_2)*cos(thetarad)*sin(thetarad);
   Nb c22 = sqrt(C_3/C_8)*sin(thetarad)*sin(thetarad);

   Nb var1 = p220/sqrt(C_5) - p222*sqrt(C_2/C_7)
        + p224*C_3*sqrt(C_2/35.0e0);
   Nb var2 = p220/sqrt(C_5) + sqrt(C_2/C_7)*p222       
        + p224/sqrt(70.0e0);
   Nb var3 = -p220/sqrt(C_5) + p222/sqrt(14.0e0) +
          p224*C_2*sqrt(C_2/35.0e0);

   //spin-orbit effect
   Nb var00 = p000 + p202*c20;
   Nb var20 = p022 + var1*c20;
 
   Nb varA = var00 - var20/C_5;
   Nb varB = var00 + var20*(C_2/C_5);
   Nb varC = sqrt(C_3)*var3*c21/C_5;
   Nb varD = -sqrt(C_6)*var2*c22/C_5;

   //XSigma and APi states of Ar - N2+ system
   Nb vsign2 = p000ndp+sqrt(C_5)*c20*p202ndp;
   Nb vpin2 = vsign2 + 1136.0e0;

   //charge transfer couplings 
   Nb vct   = 78801.0e0 * aR * exp(-2.028e0*aR) +
           82762.0e0 * aR * exp(-2.028e0*aR) * c20;
   Nb vctpi = 74841.0e0 * aR * exp(-2.028e0*aR) *
          sin(thetarad) * sin(thetarad);

   //full Hamiltoniam matrix
   vmatso[I_0][I_0] = varB + 179.0e0 - g/2.0e0;
   vmatso[I_0][I_1] = C_0;
   vmatso[I_0][I_2] = -g/sqrt(C_2);
   vmatso[I_0][I_3] = C_0;
   vmatso[I_0][I_4] = -varC;
   vmatso[I_0][I_5] = varC;
   vmatso[I_0][I_6] = vct;
   vmatso[I_0][I_7] = vctpi;
   vmatso[I_0][I_8] = C_0;
   vmatso[I_0][I_9] = C_0;
   vmatso[I_1][I_1] = varA + g/C_2 + 179.0e0 - g/C_2;
   vmatso[I_1][I_2] = varD;
   vmatso[I_1][I_3] = varC;
   vmatso[I_2][I_2] = varA - g/C_2 + 179.0e0 - g/C_2;
   vmatso[I_2][I_3] = -varC;
   vmatso[I_3][I_3] = varB + 179.0e0 - g/C_2;
   vmatso[I_3][I_5] = -g/sqrt(C_2);
   vmatso[I_3][I_8] = vct;
   vmatso[I_3][I_9] = vctpi;
   vmatso[I_4][I_4] = varA + g/C_2 + 179.0e0 - g/C_2;
   vmatso[I_4][I_5] = varD;
   vmatso[I_5][I_5] = varA - g/C_2 + 179.0e0 - g/C_2;
   vmatso[I_6][I_6] = vsign2;
   vmatso[I_7][I_7] = vpin2;
   vmatso[I_8][I_8] = vmatso[I_6][I_6];
   vmatso[I_9][I_9] = vmatso[I_7][I_7];
   //make symmetrix
   for (In i=I_0; i<vmatso.Nrows(); i++)
      for (In j=I_0; j<vmatso.Ncols(); j++)
         vmatso[j][i]=vmatso[i][j];
   //then diagonalize
   midas::potential::detail::tred2(vmatso,d,e);
   midas::potential::detail::tqli(d,e,vmatso);

   for(In i=I_0; i<n_p-I_1; i++)
   {
      Nb val_min=d[i];
      In i_min=i;
      for (In i_e=i+I_1; i_e<n_p; i_e++)
      {
         if (d[i_e]<val_min)
         {
            val_min=i_e;
            i_min=i_e;
         }
      }
      if (i_min !=i)
      {
         d[i_min] = d[i];
         d[i] = val_min;
      }
   }

   //ipes = 0 exit channel: Ar + N2+(X) (N=0)
   Nb v_pot=C_0;
   if (mIpes==I_0)
   {
      v_pot=d[1];
      return v_pot/(C_10_3*C_AUTEV);
   }
   //ipes = 2 entrance channel: Ar+(P3/2,3/2) + N2(X)
   else if (mIpes==I_1)
   { 
      v_pot = d[3];
      return v_pot/(C_10_3*C_AUTEV);
   }
   //ipes = 3 entrance channel: Ar+(P3/2,1/2) + N2(X)
   else if (mIpes==I_2)
   {
      v_pot= d[5];
      return v_pot/(C_10_3*C_AUTEV);
   }
   //ipes = 4 entrance channel: Ar+(P1/2,1/2) + N2(X)
   else if (mIpes==I_3)
   {
      v_pot = d[I_7];
      return v_pot/(C_10_3*C_AUTEV);
   }
   //ipes = 5 exit channel: Ar  + N2+(A)
   else if (mIpes==I_4)
   {
      v_pot = d[I_9];
      return v_pot/(C_10_3*C_AUTEV);
   }
   return C_0; //failure
}
/**
 * Function V000
 * */
Nb  ArNtwopPot::v000(Nb& R) const                   
{
   Nb X=R/RM0;                                                          
   Nb v_val=C_0;
   Nb SP=C_0;
   if (X <= X3)
   {
      //morse
      SP=-BETA*(X-C_1);
      v_val=EPSI0*(exp(C_2*SP)-C_2*exp(SP));
      return v_val;
   }
   else if (X<= X4)
   {
      //spline2
      SP=B3+(X-X3)*B4;
      SP=B2+(X-X4)*SP;
      SP=B1+(X-X3)*SP;
      v_val=EPSI0*SP;
      return v_val;
   }
   //van der Waals
   Nb R2=R;                                                    
   v_val=-C4000/pow(R2,C_4);                                  
   return v_val;
}
/**
 * V022 function
 * */
Nb ArNtwopPot::v022(Nb& R) const
{
   Nb v_val=-A022*exp(-ALFA022*R-GAMMA022*R*R)+C6022/pow(R,C_6);
   return v_val;
}
/**
 * V202 function
 * */
Nb ArNtwopPot::v202(Nb& R) const
{
   Nb v_val=A202*exp(-ALFA202*R)-C4202/pow(R,C_4)+Q202/pow(R,C_3);
   return v_val;
}
/**
 * V220 function
 * */
Nb ArNtwopPot::v220(Nb& R) const
{
   Nb v_val=A220*exp(-ALFA220*R);          
   return v_val;
}
/**
 * V222 function
 * */
Nb ArNtwopPot::v222(Nb& R) const
{
   Nb v_val=A222*exp(-ALFA222*R);
   return v_val;
}
/**
 * V224 function
 * */
Nb ArNtwopPot::v224(Nb& R) const
{
   Nb R70=8.3666003e0;
   Nb v_val=R70*C224/pow(R,C_5);
   return v_val;
}
/**
 * Spline function
 * */
void ArNtwopPot::Spline()
{        
   Nb SP=-BETA*(X3-C_1);                                  
   Nb V1 =EPSI0*(exp(C_2*SP)-C_2*exp(SP));
   SP=exp(-BETA*(X3-C_1));
   Nb DV1 = EPSI0*(-C_2*BETA*SP*(SP-C_1))/RM0;
   Nb R1=X4*RM0;
   Nb V2=-(C4000/pow(R1,C_4));                                   
   Nb DV2 = C_4*C4000/pow(R1,C_5); 
   B1 = V1/EPSI0;                                         
   B2 = (V2/EPSI0-B1)/(X4-X3);                             
   B3 = (DV1*RM0/EPSI0-B2)/(X3-X4);                          
   B4 = (DV2*RM0/EPSI0-B2-B3*(X4-X3))/pow((X4-X3),C_2);      
   return;
}

namespace midas::potential::detail
{
/**
 * Routine tqli, taken from Numerical Recipes, pag. 485
 * QL algorithm with implicit shift
 * */
void tqli(MidasVector& d, MidasVector& e, MidasMatrix& z)
{
   In m,l,iter,i,k;
   Nb s,r,p,g,f,dd,c,b;

   In n=d.Size();
   for (i=I_1;i<n;i++) e[i-I_1]=e[i];
   e[n-I_1]=C_0;
   for (l=I_0;l<n;l++) 
   {
      iter=I_0;
      do 
      {
         for (m=l;m<n-I_1;m++) 
         {
            dd=fabs(d[m])+fabs(d[m+I_1]);
            if (fabs(e[m])+dd == dd) break;
         }
         if (m != l) 
         {
            if (iter++ == I_30) MIDASERROR (" Too many iterations in tqli ");
            g=(d[l+I_1]-d[l])/(C_2*e[l]);
            r=pythag(g,C_1);
            g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
            s=c=C_1;
            p=C_0;
            for (i=m-I_1;i>=l;i--) 
            {
               f=s*e[i];
               b=c*e[i];
               e[i+I_1]=(r=pythag(f,g));
               if (r == C_0) 
               {
                  d[i+I_1] -= p;
                  e[m]=C_0;
                  break;
               }
               s=f/r;
               c=g/r;
               g=d[i+I_1]-p;
               r=(d[i]-g)*s+C_2*c*b;
               d[i+I_1]=g+(p=s*r);
               g=c*r-b;
               // Next loop can be omitted if eigenvectors not wanted
               for (k=I_0;k<n;k++) 
               {
                  f=z[k][i+I_1];
                  z[k][i+I_1]=s*z[k][i]+c*f;
                  z[k][i]=c*z[k][i]-s*f;
               }
            }
            if (r == C_0 && i >= l) continue;
            d[l] -= p;
            e[l]=g;
            e[m]=C_0;
         }
      } while (m != l);
   }
   return;
}
/**
* Routine tred2, taken from Numerical Recipes, pag. 485
* Householder reduction of a real, symmetric matrix
* */
void tred2(MidasMatrix& a, MidasVector& d, MidasVector& e)
{
   In l,k,j,i;
   Nb scale,hh,h,g,f;

   In n=d.Size();
   for (i=n-I_1;i>I_0;i--) 
   {
      l=i-I_1;
      h=scale=C_0;
      if (l > I_0) 
      {
         for (k=I_0;k<l+I_1;k++)
            scale += fabs(a[i][k]);
         if (scale == C_0)
            e[i]=a[i][l];
         else 
         {
            for (k=I_0;k<l+I_1;k++) 
            {
               a[i][k] /= scale;
               h += a[i][k]*a[i][k];
            }
            f=a[i][l];
            g=(f >= C_0 ? -sqrt(h) : sqrt(h));
            e[i]=scale*g;
            h -= f*g;
            a[i][l]=f-g;
            f=C_0;
            for (j=I_0;j<l+I_1;j++) 
            {
               // Next statement can be omitted if eigenvectors not wanted
               a[j][i]=a[i][j]/h;
               g=C_0;
               for (k=I_0;k<j+I_1;k++)
                  g += a[j][k]*a[i][k];
               for (k=j+I_1;k<l+I_1;k++)
                  g += a[k][j]*a[i][k];
               e[j]=g/h;
               f += e[j]*a[i][j];
            }
            hh=f/(h+h);
            for (j=I_0;j<l+I_1;j++) 
            {
               f=a[i][j];
               e[j]=g=e[j]-hh*f;
               for (k=I_0;k<j+I_1;k++)
                  a[j][k] -= (f*e[k]+g*a[i][k]);
            }
         }
      }
      else
      {
         e[i]=a[i][l];
         d[i]=h;
      }
   }
   // Next statement can be omitted if eigenvectors not wanted
   d[I_0]=C_0;
   e[I_0]=C_0;
   // Contents of this loop can be omitted if eigenvectors not
   // wanted except for statement d[i]=a[i][i];
   for (i=I_0;i<n;i++) 
   {
      l=i;
      if (d[i] != C_0) 
      {
         for (j=I_0;j<l;j++) 
         {
            g=C_0;
            for (k=I_0;k<l;k++)
               g += a[i][k]*a[k][j];
            for (k=I_0;k<l;k++)
               a[k][j] -= g*a[k][i];
         }
      }
      d[i]=a[i][i];
      a[i][i]=C_1;
      for (j=0;j<l;j++) a[j][i]=a[i][j]=C_0;
   }
}

} // namespace midas::potential::detail

/**
 * function V000nduep
 * */
Nb ArNtwopPot::v000nduep(Nb& R) const
{
   Nb X=R/RM0ndp;
   Nb v_val=C_0;
   Nb SP=C_0;
   if (X <=X3ndp)
   {
      //morse
      SP=-BETAndp*(X-C_1);
      v_val=EPSI0ndp*(exp(C_2*SP)-C_2*exp(SP));
      return v_val;
   }
   else if (X <X4ndp)
   {
      //spline2
      SP=B3ndp+(X-X3ndp)*B4ndp;
      SP=B2ndp+(X-X4ndp)*SP;
      SP=B1ndp+(X-X3ndp)*SP;
      v_val=EPSI0ndp*SP;
      return v_val;
   }
   else
   {
      //van der Waals
      Nb R2=R;
      v_val=-C4000ndp/pow(R2,C_4);
      return v_val;
   }
   return v_val;
}
/**
 * function V202nduep
 * */
Nb ArNtwopPot::v202nduep(Nb& R) const
{
   Nb v_val=A202ndp*exp(-ALFA202ndp*R)-C6202ndp/pow(R,C_6);
   v_val=v_val/sqrt(C_5);
   return v_val;
}
/**
 * Function Splinendp
 * */
void ArNtwopPot::Splinendp()
{
   Nb SP=-BETAndp*(X3ndp-C_1);
   Nb V1=EPSI0ndp*(exp(C_2*SP)-C_2*exp(SP));
   SP=exp(-BETAndp*(X3ndp-C_1));
   Nb DV1=EPSI0ndp*(-C_2*BETAndp*SP*(SP-C_1))/RM0ndp;
   Nb R1=X4ndp*RM0ndp;
   Nb V2=-(C4000ndp/pow(R1,C_4));
   Nb DV2 = C_4*C4000ndp/pow(R1,C_5);
   B1ndp = V1/EPSI0ndp;
   B2ndp = (V2/EPSI0ndp-B1ndp)/(X4ndp-X3ndp);
   B3ndp = (DV1*RM0ndp/EPSI0ndp-B2ndp)/(X3ndp-X4ndp);
   B4ndp = (DV2*RM0ndp/EPSI0ndp-B2ndp-B3ndp
           *(X4ndp-X3ndp))/pow((X4ndp-X3ndp),C_2);
   return;
}
/**
 * Evaluate the potential over an assigned geometry
 * */
Nb ArNtwopPot::EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& arGeom) const
{
   Nb v_pot=C_0;
   //the program pot then uses Jacobi coordinates
   In n_nuc=arGeom.size();
   Mout << "n_nuc: " << n_nuc << endl;
   if(n_nuc!=I_3)
      MIDASERROR(" Wrong nr. of nuclei in ArNtwopPot ");
   In n1_pos=-I_1;
   In n2_pos=-I_1;
   In ar_pos=-I_1;
   bool ar_fnd=false;
   bool n1_fnd=false;
   bool n2_fnd=false;

   for (In i=I_0; i<n_nuc; i++)
   {
      std::string lab=arGeom[i].AtomLabel();
      if (!ar_fnd && lab.substr(0,2)=="Ar")
      {
         ar_pos=i;
         ar_fnd=true;
      }
      else if (!n1_fnd && lab.substr(0,1)=="N")
      {
         n1_pos=i;
         n1_fnd=true;
      }
      else if(!n2_fnd && lab.substr(0,1)=="N")
      {
         n2_pos=i;
         n2_fnd=true;
      }
      else
      {
         MIDASERROR(" labels not correct in input structure ");
      }
   }
   if (!(ar_fnd && n1_fnd && n2_fnd))
      MIDASERROR(" geometry not properly read into ");
   if (gDebug)
   {
      Mout << " Geometry input in ArNtwopPot::EvalPot(): " << endl;
      for (In i=0; i<n_nuc; i++)
         Mout << arGeom[i]<< endl;
      Mout << endl;
   }

   //units are supposed to be a.u. and converted to Jacobi coordinates
   //find the c.m. of the n2 molecule
   Vector3D rg_vec;
   std::vector<Nuclei> n2_geom;
   n2_geom.push_back(arGeom[n1_pos]);
   n2_geom.push_back(arGeom[n2_pos]);
   FindCenterOfMass(n2_geom,rg_vec);
   Vector3D r_vec(arGeom[n1_pos],arGeom[n2_pos]); //r vector
   Vector3D R_vec(arGeom[ar_pos].X()-rg_vec.X(),arGeom[ar_pos].Y()-rg_vec.Y(),arGeom[ar_pos].Z()-rg_vec.Z()); //R vector
   Mout << " Center of mass for the n2 molecule: " << rg_vec << endl;
   Mout << " R_vec: " << R_vec << endl;
   Mout << " r_vec: " << r_vec << endl;
   Nb dist=R_vec.Length();
   Nb theta=R_vec.Angle(r_vec);
   Mout << " dist: " << dist << " theta: " << theta << endl;
   //convert to Aangstrom
   dist*=C_TANG;
   v_pot=Pot(dist,theta);
   return v_pot;
}
