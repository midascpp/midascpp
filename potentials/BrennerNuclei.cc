/**
*************************************************************************
*
* @file                BrennerNuclei.cc
*
* Created:             30/10/2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Definitions of class members for BrennerNuclei
*                      Implements a new Nuclei class (Brenner Nuclei)
*                      needed for storing nearest neighbors.
*  
* Last modified:       18/12/2007
*     
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*     
************************************************************************
**/

// std headers
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <map>

// midas headers
#include "potentials/BrennerPot.h"
#include "potentials/BrennerNuclei.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"

/**
* BrennerNuclei constructor
**/
BrennerNuclei::BrennerNuclei(string aS, Nb aX, Nb aY, Nb aZ) {
   Type=aS;
   X=aX;
   Y=aY;
   Z=aZ;
   NearestNeighbor.clear();
}

void BrennerNuclei::PrInMap() {
   for(std::map<In,Nb>::iterator it=NearestNeighbor.begin();
      it!=NearestNeighbor.end(); it++)
      cout << "NUC = " << (*it).first << " DIST = " << (*it).second << endl;
}

Nb BrennerNuclei::Dot(BrennerNuclei aN) {
   Nb result=0.0;
   result+=X*aN.GetX();
   result+=Y*aN.GetY();
   result+=Z*aN.GetZ();
   return result;
}

Nb BrennerNuclei::GetMass() {
   if(Type.find("C") != Type.npos)
      return 6.0;
   else if(Type.find("H") != Type.npos)
      return 1.0;
   else
      exit(10);
}

void BrennerNuclei::Translate(vector<Nb> aV) {
   if(aV.size()==3) {
      X+=aV[0];
      Y+=aV[1];
      Z+=aV[2];
   }
   else
      exit(10);
}

ostream& operator<<(ostream& aOs, BrennerNuclei aN) {
   aOs << endl;
   aOs << "(X,Y,Z) = (" << aN.GetX() << "," << aN.GetY() << "," << aN.GetZ() << ")" << endl;
   aOs << "With the following neighbors:" << endl;
   for(std::map<In,Nb>::iterator it=aN.GetNN().begin();
      it!=aN.GetNN().end(); it++)
      aOs << "NUC = " << (*it).first << " DIST = " << (*it).second << endl;
   return aOs;
}
