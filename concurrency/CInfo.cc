#include "concurrency/Info.h"

#include <sstream>

#include "util/Io.h"
#include "concurrency/TypeDefs.h"

namespace midas
{
namespace concurrency
{

// Initialize Info members.
std::unique_ptr<thread_pool> Info::mPool;
std::thread::id Info::mMasterThreadID = std::this_thread::get_id();

/**
 * Initialize Thread concurrency.
 *
 * @param aNthreads   The number of thread pool worker threads to spawn.
 **/
void Initialize
   (  int aNthreads
   )
{
   Info::mPool     = std::unique_ptr<thread_pool>(new thread_pool(aNthreads));
}

/**
 * Is current thread the master thread?
 *
 * @return Returns true if thread is master, false otherwise.
 **/ 
bool IsMasterThread
   (
   )
{
   return (Info::mMasterThreadID == std::this_thread::get_id());
}

/**
 * Output nice concurrency header.
 *
 * @param aOs    The output stream to print to.
 **/
void OutputConcurrencyHeader
   (  std::ostream& aOs
   )
{
   constexpr char box   = 'O';
   constexpr char space = ' ';
   Out72Char  (aOs, box, box  , box);
   Out72Char  (aOs, box, space, box);
   OneArgOut72(aOs, " Nr. threads in worker pool : " + std::to_string(Info::mPool->get_nthreads()), box);
   OneArgOut72(aOs, " Master thread              : " + ThreadIDString(), box);
   Out72Char  (aOs, box, space, box);
   Out72Char  (aOs, box, box  , box);
}

/** 
 * Set number of active threads. 
 * Default will set number of active threads to number of actual threads in the work pool.
 *
 * @param aActive   The number of active threads that is going to be used. -1 will set to use all available threads!
 **/
void SetActive(int aActive)
{
   if constexpr(CONCURRENCY_DEBUG)
   {
      std::cout << " [CONCURRENCY] : Setting number of active threads : " << aActive << std::endl;
      assert(Info::mPool);
   }
   
   Info::mPool->set_active(aActive);
}

/** 
 * Get thread id as a string.
 *
 * @return Returns thread id as string.
 **/
std::string ThreadIDString()
{
   auto thread_id = std::this_thread::get_id();
   std::stringstream ss;
   ss << thread_id;
   std::string thread_id_str = ss.str();

   return thread_id_str;
}

} /* namespace concurrency */
} /* namespace midas */
