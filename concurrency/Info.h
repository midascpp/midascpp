/**
************************************************************************
*
* @file                Info.h
*
* Created:              ?
*
* Authors:              ? 
*
* Short Description:    ?
*
* Last modified:        ?
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef INFO_H_INCLUDED
#define INFO_H_INCLUDED

#include <memory>
#include <thread>
#include <iostream>
#include <fstream>
#include <cassert>

#include "concurrency/TypeDefs.h"
#include "util/paral/thread_pool.h"

namespace midas
{
namespace concurrency
{

/**
 * Struct to hold information on threading.
 **/
struct Info
{
   //! ID of master thread (i.e. not work pool thread or output thread).
   static std::thread::id mMasterThreadID;
   
   //! Pool of worker threads
   static std::unique_ptr<thread_pool> mPool;
};

/* ==========================================================================
 *
 * General threading interface.
 *
 * ========================================================================== */
//! Initialize thread pool
void Initialize(int aNthreads);

//! Are we currently in master thread?
bool IsMasterThread();

//! Print nice header on concurrency settings.
void OutputConcurrencyHeader
   (  std::ostream& aOs
   );

/* ==========================================================================
 *
 * Thread pool interface.
 *
 * ========================================================================== */
//! Post job to the thread pool. Will return future to the result of the job.
template<class F>
auto Post(F&& f)
{
   if constexpr(CONCURRENCY_DEBUG)
   {
      std::cout << " [CONCURRENCY] : Posting task for thread pool. " << std::endl;
      assert(Info::mPool);
   }

   return Info::mPool->post(std::forward<F>(f));
}

//! Set number of active threads. Default will set number of active threads to number of actual threads in the work pool.
void SetActive(int aActive = -1);

//! Get Thread ID as string.
std::string ThreadIDString();

//!
template<class T>
void WriteToLog(const T& msg)
{
   std::ofstream tout(ThreadIDString(), std::ios::app);

   tout << msg;
}

} /* namespace concurrency */
} /* namespace midas */

#endif /* INFO_H_INCLUDED */
