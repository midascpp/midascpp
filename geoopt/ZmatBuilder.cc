#include "geoopt/ZmatBuilder.h"

/**
 *   @brief Transforms from a Z-Matrix representation to cartesian coordinates
 *
 *   Purpose: Uses the NeRF algorithm described in 
 *          
 *            Parsons, J., Holmes, J. B., Rojas, J. M., Tsai, J. and Strauss, C. E. M. (2005), 
 *            Practical conversion from torsion space to Cartesian space for in silico protein synthesis. 
 *            J. Comput. Chem., 26: 1063-1068. doi:10.1002/jcc.20237
 *
 *            to transform from a Z-Matrix representation to cartesian coordinates
 *
 **/
void ZmatBuilder::traf2xyz
   (  Nb* d
   ,  const Nb* c
   ,  const Nb* b
   ,  const Nb* a
   ,  const Nb& arBond
   ,  const Nb& arAngle
   ,  const Nb& arTorsion
   ) const
{
    
   // local
   Nb d2[3], M[3*3], bc[3], n[3];
   Nb dnrm;
   int idx, jdx;

   Nb deg2rad = M_PI / 180.0;

   // GS: I don't really know why the minus sign has to be here (bond),
   //     but otherwise it does not work properly.
   d2[0] = -arBond * cos(-arAngle*deg2rad);
   d2[1] = -arBond * cos(-arTorsion*deg2rad) * sin(-arAngle*deg2rad);
   d2[2] = -arBond * sin(-arTorsion*deg2rad) * sin(-arAngle*deg2rad);


   // bc = BC/|BC|
   bc[0] = c[0] - b[0];
   bc[1] = c[1] - b[1];
   bc[2] = c[2] - b[2];

   dnrm = std::sqrt( bc[0]*bc[0] 
                   + bc[1]*bc[1] 
                   + bc[2]*bc[2] 
                   );

   bc[0] = bc[0] / dnrm;
   bc[1] = bc[1] / dnrm;
   bc[2] = bc[2] / dnrm;


   // n = AB x bc / |AB x bc|
   n[0] = (b[1]-a[1])*bc[2] - (b[2]-a[2])*bc[1];
   n[1] = (b[2]-a[2])*bc[0] - (b[0]-a[0])*bc[2];
   n[2] = (b[0]-a[0])*bc[1] - (b[1]-a[1])*bc[0];

   dnrm = std::sqrt( n[0]*n[0]
                   + n[1]*n[1]
                   + n[2]*n[2]
                   );

   n[0] = n[0] / dnrm; 
   n[1] = n[1] / dnrm;
   n[2] = n[2] / dnrm;

   // Build matrix
   M[0 + 3*0] = bc[0]; 
   M[1 + 3*0] = bc[1];
   M[2 + 3*0] = bc[2];

   M[0 + 3*1] = n[1]*bc[2] - n[2]*bc[1];
   M[1 + 3*1] = n[2]*bc[0] - n[0]*bc[2];
   M[2 + 3*1] = n[0]*bc[1] - n[1]*bc[0];

   M[0 + 3*2] = n[0];
   M[1 + 3*2] = n[1];
   M[2 + 3*2] = n[2];


   // D = M*D2 + C
   for (idx = 0; idx < 3; idx++)
   {
      d[idx] = c[idx];
      for (jdx = 0; jdx < 3; jdx++)
      {
         d[idx] = d[idx] +  M[idx + 3*jdx] * d2[jdx];
      }
   }

}

/**
 *   @brief Transforms from a Z-Matrix representation to cartesian coordinates
 *
 *   @param apXyz holds the cartesian coordinates on output
 *   @param arNatoms holds the number of atoms in the molecule          
 *   @param apNa    = indeces of atom ot which atoms are related by distance
 *   @param apNb    = indeces of atom ot which atoms are related by angle
 *   @param apNc    = indeces of atom ot which atoms are related by dihedral angle
 *   @param rad2deg = conversion factpr for radiant to degree
 *   @param apGeo   = internal coordintates 
 *
 **/
void ZmatBuilder::zmat2xyz
   (  Nb* apXyz
   ,  const int& arNatoms
   ,  const int* apNa
   ,  const int* apNb
   ,  const int* apNc
   ,  const Nb& rad2deg
   ,  const Nb* apGeo
   ) const
{

   // local
   int iat;
   Nb newX, newY, centerX, centerY, pointX, pointY;
   Nb alpha;

   // set first atom in origin
   apXyz[0 + 3*0] = 0.0;  // 3*0 kept to indicate how the matrix is stored
   apXyz[1 + 3*0] = 0.0;
   apXyz[2 + 3*0] = 0.0;

   // set second atom along the x axis
   if (arNatoms > 1) 
   {
      apXyz[0 + 3*1] = -apGeo[0 + 3*1] ;
      apXyz[1 + 3*1] = 0.0;
      apXyz[2 + 3*1] = 0.0;
   }

   // now the third atom
   if (arNatoms > 2)
   {
      // 1) add it along the x axis again
      apXyz[0 + 3*2] = apXyz[0 + 3*1] + apGeo[0 + 3*2];
      apXyz[1 + 3*2] = 0.0;
      apXyz[2 + 3*2] = 0.0;

      // 2) rotate atom to get right angle
      alpha = apGeo[1 + 3*2] * M_PI / 180.0;

      centerX = apXyz[0 + 3*2]; 
      centerY = apXyz[1 + 3*2];
      pointX  = apXyz[0 + 3*3]; 
      pointY  = apXyz[1 + 3*3];

      newX = centerX + (  cos(-alpha) * (pointX-centerX) - sin(-alpha) * (pointY-centerY));
      newY = centerY + (  sin(-alpha) * (pointX-centerX) + cos(-alpha) * (pointY-centerY));

      apXyz[0 + 3*2] = newX;
      apXyz[1 + 3*2] = newY;
      apXyz[2 + 3*2] = 0.0;
   }

   // add remaining atoms with NeRF algorithm
   for (iat = 3; iat < arNatoms; iat++)
   {
      traf2xyz( &apXyz[0 + 3*iat], &apXyz[0 + 3*apNa[iat]], &apXyz[0 + 3*apNb[iat]], &apXyz[0 + 3*apNc[iat]]
              ,  apGeo[0 + 3*iat],  apGeo[1 + 3*iat], apGeo[2 + 3*iat]
              );
   }

}

/**
 * 
 * @brief Calculates the angle between atoms i,j and k
 *
 * Purpose: Calculates the angle between atoms i,j and k
 *
 *   @param apXyz:   (on input) cartesian coordinates
 *   @param arIat: The index for atom i involved to form the angle
 *   @param arJat: The index for atom j involved to form the angle
 *   @param arKat: The index for atom k involved to form the angle
 *   @param arAngle: (on output) self explenatory
 *
 **/
void ZmatBuilder::GetAngle
   (  const Nb* apXyz
   ,  const int& arIat
   ,  const int& arJat
   ,  const int& arKat
   ,  Nb& arAngle
   ) const
{

   // local
   Nb d2ij, d2jk, d2ik, xy, dtemp;

   d2ij = (apXyz[0 + arIat*3]-apXyz[0 + arJat*3])*(apXyz[0 + arIat*3]-apXyz[0 + arJat*3]) 
        + (apXyz[1 + arIat*3]-apXyz[1 + arJat*3])*(apXyz[1 + arIat*3]-apXyz[1 + arJat*3])
        + (apXyz[2 + arIat*3]-apXyz[2 + arJat*3])*(apXyz[2 + arIat*3]-apXyz[2 + arJat*3]);
   d2jk = (apXyz[0 + arJat*3]-apXyz[0 + arKat*3])*(apXyz[0 + arJat*3]-apXyz[0 + arKat*3])
        + (apXyz[1 + arJat*3]-apXyz[1 + arKat*3])*(apXyz[1 + arJat*3]-apXyz[1 + arKat*3]) 
        + (apXyz[2 + arJat*3]-apXyz[2 + arKat*3])*(apXyz[2 + arJat*3]-apXyz[2 + arKat*3]);
   d2ik = (apXyz[0 + arIat*3]-apXyz[0 + arKat*3])*(apXyz[0 + arIat*3]-apXyz[0 + arKat*3]) 
        + (apXyz[1 + arIat*3]-apXyz[1 + arKat*3])*(apXyz[1 + arIat*3]-apXyz[1 + arKat*3]) 
        + (apXyz[2 + arIat*3]-apXyz[2 + arKat*3])*(apXyz[2 + arIat*3]-apXyz[2 + arKat*3]);

   xy = std::sqrt(d2ij * d2jk);

   dtemp = 0.5 * (d2ij + d2jk - d2ik) / xy;

   if (dtemp >  1.0) dtemp =  1.0;
   if (dtemp < -1.0) dtemp = -1.0;

   arAngle = acos( dtemp );
}

/**
 * Purpose: determines the angles between the points
 *              (a1,a2), (0,0) and (b1,b2)
 * output: rcos
 **/
void ZmatBuilder::DetermineAngle
   (  Nb& a1
   ,  Nb& a2
   ,  Nb& b1
   ,  Nb& b2
   ,  Nb& rcos
   ) const
{
    
   // constants
   const Nb eps = 1.0e-6;
   const Nb twopi = 2.0 * M_PI;

   // local
   Nb anorm, bnorm, sinth, costh;


   if ( std::abs(a1) < eps && std::abs(a2) < eps)
   {
      rcos = 0.0;
   }
   else if ( std::abs(b1) < eps && std::abs(b2) < eps) 
   {
      rcos = 0.0;
   }
   else
   {
      anorm = 1.0 / std::sqrt(a1*a1 + a2*a2);
      bnorm = 1.0 / std::sqrt(b1*b1 + b2*b2);
      a1 *= anorm;
      a2 *= anorm;
      b1 *= bnorm;
      b2 *= bnorm;

      sinth = (a1*b2) - (a2*b1);
      costh = a1*b1 + a2*b2;

      if (costh >  1.0) costh =  1.0;
      if (costh < -1.0) costh = -1.0;

      rcos = std::acos(costh);

      if ( std::abs(rcos) < 4.0e-4) 
      {
         rcos = 0.0;
      }
      else if (sinth > 0.0) 
      {
        rcos = twopi - rcos;
      }

      rcos = -rcos;
   }
}

/**
 * Purpose: Calculates the dihedral angle between atoms i, j, k and l
 *
 **/
void ZmatBuilder::GetDihedral
   (  const Nb* xyz
   ,  const int& iat
   ,  const int& jat
   ,  const int& kat
   ,  const int& lat
   ,  Nb& angle
   ) const
{

   // constants 
   const Nb twopi = 2.0 * M_PI;

   // local
   Nb xi1, xj1, xl1, yi1, yj1, yl1, zi1, zj1, zl1;
   Nb xi2, xl2, yi2, yj2, yl2;
   Nb yi3, yj3, yl3;
   Nb dist, ddd, yxdist;
   Nb cosa, cosph, sinph, costh, sinth;

   xi1 = xyz[0 + iat*3] - xyz[0 + kat*3]; 
   xj1 = xyz[0 + jat*3] - xyz[0 + kat*3];
   xl1 = xyz[0 + lat*3] - xyz[0 + kat*3];
   yi1 = xyz[1 + iat*3] - xyz[1 + kat*3];
   yj1 = xyz[1 + jat*3] - xyz[1 + kat*3];
   yl1 = xyz[1 + lat*3] - xyz[1 + kat*3];
   zi1 = xyz[2 + iat*3] - xyz[2 + kat*3];
   zj1 = xyz[2 + jat*3] - xyz[2 + kat*3];
   zl1 = xyz[2 + lat*3] - xyz[2 + kat*3];

   // rotate around z axis to put kj along y axis
   dist = std::sqrt(xj1*xj1 + yj1*yj1 + zj1*zj1);
   cosa = zj1 / dist;

   if (cosa >  1.0) cosa =  1.0;
   if (cosa < -1.0) cosa = -1.0;

   ddd = 1.0 - cosa*cosa;

   if (ddd <= 0.0) 
   {  
      xi2 = yi1;
      xl2 = xl1;
      yi2 = yi1;
      yl2 = yl1;
      costh = cosa;
      sinth = 0.0;
   }
   else
   {
      yxdist = dist * std::sqrt(ddd);

      if (yxdist > 1.0e-9)
      {
         cosph = yj1 / yxdist;
         sinph = xj1 / yxdist;

         xi2 = xi1 * cosph - yi1 * sinph;
         xl2 = xl1 * cosph - yl1 * sinph;
         yi2 = xi1 * sinph + yi1 * cosph;
         yj2 = xj1 * sinph + yj1 * cosph;
         yl2 = xl1 * sinph + yl1 * cosph;

         // rotate kj around the x axis so kj is along the z axis
         costh = cosa;
         sinth = yj2 / dist;
      }
      else
      {
         xi2 = yi1;
         xl2 = xl1;
         yi2 = yi1;
         yl2 = yl1;
         costh = cosa;
         sinth = 0.0;
      }
   }

   yi3 = yi2 * costh - zi1 * sinth;
   yl3 = yl2 * costh - zl1 * sinth;

   DetermineAngle(xl2, yl3, xi2, yi3, angle);

   if (angle < 0.0    ) angle = twopi + angle;
   if (angle >= twopi ) angle = 0.0;
}

/**
 * Purpose: converts from cartesian to internal coordinates
 *
 *    @param xyz     = (on input) cartesian coordinates
 *    @param natoms  = number of atoms
 *    @param na      = indeces of atom ot which atoms are related by distance
 *    @param nb      = indeces of atom ot which atoms are related by angle
 *    @param nc      = indeces of atom ot which atoms are related by dihedral angle
 *    @param rad2deg = conversion factor from radient to degree
 *    @param geo     = (on output) internal coordinates in a.u., radians and radians
 *
 **/
void ZmatBuilder::xyzgeo
   (  const Nb* xyz
   ,  const int natoms
   ,  int* na
   ,  int* nb
   ,  int* nc
   ,  const Nb rad2deg
   ,  Nb* geo
   ) const
{
   
   // local
   int iat, jat, kat, lat;
   Nb angle = C_0;

   for (iat = 1; iat < natoms; iat++)
   {
      jat = na[iat];
      kat = nb[iat];
      lat = nc[iat];

      geo[iat*3 + 0] = std::sqrt(  (xyz[iat*3 + 0]-xyz[jat*3 + 0]) * (xyz[iat*3 + 0]-xyz[jat*3 + 0])
                                +  (xyz[iat*3 + 1]-xyz[jat*3 + 1]) * (xyz[iat*3 + 1]-xyz[jat*3 + 1])
                                +  (xyz[iat*3 + 2]-xyz[jat*3 + 2]) * (xyz[iat*3 + 2]-xyz[jat*3 + 2])
                                );

      if (iat < 2) continue;   
      angle = C_0;
      GetAngle(xyz, iat, jat, kat, angle);
      geo[iat*3 + 1] = angle * rad2deg;

      if (iat < 3) continue;
      angle = C_0;
      GetDihedral(xyz, iat, jat, kat, lat, angle);
      geo[iat*3 + 2] = angle * rad2deg;

   }

   geo[0*3 + 0] = 0.0;  // first atom all values are zero
   geo[0*3 + 1] = 0.0; 
   geo[0*3 + 2] = 0.0; 

   if (natoms > 1) 
   {
      geo[1*3 + 1] = 0.0; 
      geo[1*3 + 2] = 0.0; 
   }

   if (natoms > 2)
   {
      geo[2*3 + 2] = 0.0; 
   }
}

/**
 *
 * Purpose : Determine the internal coordinates of a molecule
 *
 *        The rules to determine the connectivity are:
 *
 *        atom i is defined as being at a distance from the nearest 
 *        atom j, with atom j already having been defined.
 *
 *        atom i makes an angle with atom j and atom k, which 
 *        already has been defined, and is the nearest to atom j 
 *
 *        atom i makes a dihedral angle with atoms j, k and l, with l
 *        having been definies and being the nearest atom to k 
 *
 *
 *   @param xyz     = (on input) cartesian coordinates
 *   @param natoms  = number of atoms 
 *   @param na      = indeces of atom ot which atoms are related by distance
 *   @param nb      = indeces of atom ot which atoms are related by angle
 *   @param nc      = indeces of atom ot which atoms are related by dihedral angle
 *   @param rad2deg = conversion factpr for radiant to degree
 *   @param geo     = (on output) internal coordintates
 *
 **/
void ZmatBuilder::xyz2zmat
   (  const Nb* xyz
   ,  const int& natoms
   ,  int* na
   ,  int* nb
   ,  int* nc
   ,  const Nb& rad2deg
   ,  Nb* geo
   ) const
{

   // local
   int  iat, jat, kat;
   Nb  r, dsum;

   for (iat = 0; iat < natoms; iat++)
   {
      na[iat] = 1;
      nb[iat] = 2;
      nc[iat] = 3;

      if (iat == 0) continue;
      
      dsum = 1.E30; // Init to some big value
      kat  = -1;    // init to special value to catch if we failed to find nearest neighbour
      for (jat = 0; jat < iat; jat++)
      {
         r = (xyz[iat*3 + 0] - xyz[jat*3 + 0])*(xyz[iat*3 + 0] - xyz[jat*3 + 0])  
           + (xyz[iat*3 + 1] - xyz[jat*3 + 1])*(xyz[iat*3 + 1] - xyz[jat*3 + 1])
           + (xyz[iat*3 + 2] - xyz[jat*3 + 2])*(xyz[iat*3 + 2] - xyz[jat*3 + 2]);

         if (r < dsum && na[jat] != jat && nb[jat] != jat)
         {
            dsum = r;
            kat = jat;
         }
      }
      // atom i is nearest to atom k
      if (kat >= 0) 
      {
         na[iat] = kat;
         if (iat > 1) nb[iat] = na[kat];
         if (iat > 2) nc[iat] = nb[kat];
      }
      else
      {
         MIDASERROR("xyz2zmat:>> Failed to find nearest neighbour!");
      }
   }

   // find any atom to relate to na(iat) 
   na[0] = -1;
   nb[0] = -1;
   nc[0] = -1;
   if (natoms > 1)
   {
      nb[1] = -1;
      nc[1] = -1;
   }
   if (natoms > 2)
   {
      nc[2] = -1;
   }

   xyzgeo(xyz, natoms, na, nb, nc, rad2deg, geo);
}
