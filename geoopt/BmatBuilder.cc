#include "geoopt/BmatBuilder.h"
#include "mlearn/symmetryfunctions.h"

/**
 *   @brief Get difference vector of two 3D vectors 
 *
 *   @param arVec is the difference vector which will be returned
 *   @param arRab is the norm of the returned difference vector     
 *   @param apVec1 is a pointer to the first vector
 *   @param apVec2 is a pointer to the second vector
 *
 **/
void BmatBuilder::GetDifferenceVector
   (  std::vector<Nb>& arVec
   ,  Nb& arRab
   ,  const Nb* apVec1
   ,  const Nb* apVec2
   ) const
{
   for (int idx = 0; idx < 3; idx++)
   {
      arVec[idx] = ( apVec2[idx] - apVec1[idx]);
      arRab += arVec[idx] * arVec[idx]; 
   }
   arRab = std::sqrt(arRab);

   arVec[0] /= arRab;
   arVec[1] /= arRab;
   arVec[2] /= arRab;
}

/**
 *   @brief Calculates the cross product of two 3D vectors 
 *
 *   @param arVec contains the cross product on output
 *   @param arU is the first vector
 *   @param arV is the second vector
 *
 **/
void BmatBuilder::Cross
   (  std::vector<Nb>& arVec
   ,  const std::vector<Nb>& arU
   ,  const std::vector<Nb>& arV
   ) const
{
   arVec[0] = arU[1]*arV[2] - arU[2]*arV[1];
   arVec[1] = arU[2]*arV[0] - arU[0]*arV[2];
   arVec[2] = arU[0]*arV[1] - arU[1]*arV[0];
}

/**
 *   @brief Calculates a normal vector
 *
 *   @param arVec contains the normal vector
 *   @param arU is the first vector
 *   @param arV is the second vector
 *
 **/
void BmatBuilder::GetNormalVector
   (  std::vector<Nb>& arVec
   ,  const std::vector<Nb>& arU
   ,  const std::vector<Nb>& arV
   ) const
{
   // calculate cross product vec = u x v   
   Cross(arVec, arU, arV);

   // normalize vector
   Nb norm = std::inner_product(std::begin(arVec), std::end(arVec), std::begin(arVec), 0.0);
   norm = std::sqrt(norm);
   std::transform
      (  arVec.begin()
      ,  arVec.end()
      ,  arVec.begin()
      ,  [&norm](Nb aNb) { return aNb / norm; }
      );
}

Nb BmatBuilder::GetDihed
   (  const vector<Nb>& u
   ,  const vector<Nb>& v
   ,  const vector<Nb>& w
   ) const
{
   vector<Nb> x(3);
   vector<Nb> z(3);
   vector<Nb> t(3);
   
   GetNormalVector(z, u, v);
   GetNormalVector(x, w, v);
   GetNormalVector(t, z, v);
   
   // cos(tau)
   Nb cos = std::inner_product(std::begin(z), std::end(z), std::begin(x), 0.0);

   // sin(tau)
   Nb sin = std::inner_product(std::begin(t), std::end(t), std::begin(x), 0.0);

   Nb tau = std::atan2(sin,cos);

   return tau;
}


/**
 *    @brief Generates a certain row of the Wilson B-matrix
 *
 *    Purpose: Generates a certain row of the Wilson B-matrix, which
 *             is used to transform from cartesian displacements to
 *             internal coordinates.
 *
 *             The literature used for implementing the matrix elements is:
 *             S. Califano, Vibrational States, Wiley, London 1976, Chap. 4.3
 *             and J. Chem. Phys. 117, 9160 (2002)
 *
 *   @param arCoord      Cartesian coordinates order  x1,y1,z1, x2,y2,z2, ....
 *   @param arRefCoord   Cartesian coordinates for the reference structure 
 *   @param arIrow       Index of row which should be generated
 *   @param apBmat       Pointer to the B matrix, for which a row should be generated
 *   @param arVal        Actual value for the internal coordinate
 *   @param arItype      Specifies the type of internal coordinate e.g. streching (1), bending (2), ...
 *   @param arIatoms     A vector with the atom indices, which are involved in defining the internal coordinate
 *   @param arNcart      Dimension of the cartesian coordinates (size of coord)    
 **/ 
void BmatBuilder::GenerateRow 
   (  const std::vector<Nb>& arCoord
   ,  const std::vector<Nb>& arRefCoord
   ,  const int& arIrow
   ,  Nb* apBmat
   ,  Nb& arVal 
   ,  const int& arItype 
   ,  const std::vector<int>& arIatoms
   ,  const int& arNcart
   ) const
{
   const bool locdbg = false;

   int icart = arIatoms[0] * 3;
   int jcart = arIatoms[1] * 3;
   int kcart = ((arItype > 1) ? arIatoms[2] * 3  : -1);
   int lcart = ((arItype > 2) ? arIatoms[3] * 3  : -1);

   switch (arItype)
   {
      // ------------- G2 symmetry function -------------------
      case (82):
      {
         In natoms = arCoord.size() / 3;

         for (int jdx = 0; jdx < arIatoms.size(); jdx++)
         {  
            int jcart = arIatoms[jdx] * 3;
 
            for (int ixyz = 0; ixyz < 3; ixyz++)
            {
               Nb Aij = SymmetryFunctions::dG2<Nb>(arCoord,arIatoms[0],arIatoms[jdx],ixyz,natoms);
               apBmat[arIrow*arNcart + jcart + ixyz] = Aij;  
            }
         }

         break;
      }
      // ------------- G4 symmetry function -------------------
      case (84):
      {
         In natoms = arCoord.size() / 3;

         for (int jdx = 0; jdx < arIatoms.size(); jdx++)
         {  
            int jcart = arIatoms[jdx] * 3;
 
            for (int ixyz = 0; ixyz < 3; ixyz++)
            {
               Nb Aij = SymmetryFunctions::dG4<Nb>(arCoord,arIatoms[0],arIatoms[jdx],ixyz,natoms);
               apBmat[arIrow*arNcart + jcart + ixyz] = Aij;  
            }
         }

         break;
      }

      // --------------------- stretch ------------------------
      case (1):
      {
         Nb rab = 0.0;
         vector<Nb> vec(3); 
         GetDifferenceVector(vec, rab, &arCoord[icart], &arCoord[jcart]);

         // Fill in row in B-Matrix
         apBmat[arIrow*arNcart + icart + 0] = -vec[0];
         apBmat[arIrow*arNcart + icart + 1] = -vec[1];
         apBmat[arIrow*arNcart + icart + 2] = -vec[2];

         apBmat[arIrow*arNcart + jcart + 0] = vec[0];
         apBmat[arIrow*arNcart + jcart + 1] = vec[1];
         apBmat[arIrow*arNcart + jcart + 2] = vec[2];
     
         if (locdbg) Mout << "stretch " << rab << " i,j " << icart << " " << jcart << " vec " << vec << std::endl;

         arVal = rab;

         break;         
      }

      // ------------------- inverse -----------------------
      case (-1):
      {
         Nb rab = 0.0;
         vector<Nb> vec(3); 
         GetDifferenceVector(vec, rab, &arCoord[icart], &arCoord[jcart]);
         
         Nb q   = 1.0 / rab;
         Nb rm2 = q*q;
         vec[0] = -vec[0]*rm2;
         vec[1] = -vec[1]*rm2;
         vec[2] = -vec[2]*rm2;

         // Fill in row in B-Matrix
         apBmat[arIrow*arNcart + icart + 0] = -vec[0];
         apBmat[arIrow*arNcart + icart + 1] = -vec[1];
         apBmat[arIrow*arNcart + icart + 2] = -vec[2];

         apBmat[arIrow*arNcart + jcart + 0] = vec[0];
         apBmat[arIrow*arNcart + jcart + 1] = vec[1];
         apBmat[arIrow*arNcart + jcart + 2] = vec[2];
     
         if (locdbg) Mout << "inverse " << q << " i,j " << icart << " " << jcart << " vec " << vec << std::endl;

         arVal = q;

         break;
      }


      // --------------------- bend ------------------------
      case (2):
      {
         Nb rab = 0.0;
         vector<Nb> vvec(3);
         GetDifferenceVector(vvec, rab, &arCoord[icart], &arCoord[jcart]);

         Nb rac = 0.0;
         vector<Nb> uvec(3);
         GetDifferenceVector(uvec, rac, &arCoord[icart], &arCoord[kcart]);

         Nb cosa = std::inner_product(std::begin(vvec), std::end(vvec), std::begin(uvec), 0.0);
         Nb sina = std::sqrt( 1.0 - cosa*cosa);

         vector<Nb> bab(3);
         vector<Nb> bac(3);
         for (int idx = 0; idx < 3; idx++)
         {
            bab[idx] = ( vvec[idx] * cosa - uvec[idx] ) / (rab * sina) ;
            bac[idx] = ( uvec[idx] * cosa - vvec[idx] ) / (rac * sina) ;

            apBmat[arIrow*arNcart + icart + idx] = -( bab[idx] + bac[idx] ) ;
            
            apBmat[arIrow*arNcart + jcart + idx] = bab[idx] ;

            apBmat[arIrow*arNcart + kcart + idx] = bac[idx] ;
         }

         Nb angle = std::acos(cosa);

         if (locdbg) Mout << "bend " << angle << std::endl;
   
         arVal = angle;

         break;
      }

      // ------------------- out of plane ---------------------
      case (3):
      {
         vector<Nb> u(3), v(3), w(3);

         In ind1 = icart; //l
         In ind2 = jcart; //k
         In ind3 = kcart; //i
         In ind4 = lcart; //j

         Nb r1 = C_0;
         GetDifferenceVector(u, r1, &arCoord[ind4], &arCoord[ind1]);

         Nb r2 = C_0;
         GetDifferenceVector(v, r2, &arCoord[ind4], &arCoord[ind2]);

         Nb r3 = C_0;
         GetDifferenceVector(w, r3, &arCoord[ind4], &arCoord[ind3]);

         Nb cfi1 = std::inner_product(std::begin(v), std::end(v), std::begin(w), 0.0); 
         Nb sfi1 = std::sqrt(1.0 - cfi1*cfi1);  

         std::vector<Nb> x(3), z(3);
         GetNormalVector(z, v, w);

         Nb steta = std::inner_product(std::begin(u), std::end(u), std::begin(z), 0.0);  
         Nb cteta = std::sqrt( 1.0 - steta*steta);

         Nb cfi2 = std::inner_product(std::begin(w), std::end(w), std::begin(u), 0.0); 
         Nb cfi3 = std::inner_product(std::begin(v), std::end(v), std::begin(u), 0.0); 

         Nb den = cteta*sfi1*sfi1;
         Nb st2 = (cfi1*cfi2-cfi3) / (r2*den);
         Nb st3 = (cfi1*cfi3-cfi2) / (r3*den);

         vector<Nb> uu(3), vv(3), ww(3), zz(3);
         for (int idx = 0; idx < 3; idx++)
         {
            vv[idx] = z[idx] * st2;
            ww[idx] = z[idx] * st3;
         }

         GetNormalVector(x, z, u);
         GetNormalVector(z, u, x);

         for (int idx = 0; idx < 3; idx++)
         {
            uu[idx] = z[idx] / r1;
            zz[idx] = -uu[idx] - vv[idx] - ww[idx];
         }

         // Assign values to the Wilson B-Matrix
         for (int idx = 0; idx < 3; idx++)
         { 
            apBmat[arIrow*arNcart + ind1 + idx] = uu[idx] ;
            
            apBmat[arIrow*arNcart + ind2 + idx] = vv[idx] ;

            apBmat[arIrow*arNcart + ind3 + idx] = ww[idx] ;

            apBmat[arIrow*arNcart + ind4 + idx] = zz[idx] ;
         }

         Nb cx = -1.0;
         if(steta < 0.0 ) cx = 1.0;

         Nb angle = -cx * std::acos(cteta);

         if (locdbg) Mout << "out of plane " << angle << std::endl;

         arVal = angle;

         break;
      }

      // --------------------- dihedral -----------------------
      case (4):
      {
         vector<Nb> u(3);
         vector<Nb> v(3);
         vector<Nb> w(3);

         In ind1 = icart; // k,l,i,j
         In ind2 = jcart;
         In ind3 = kcart;
         In ind4 = lcart;

         Nb r1 = 0.0;
         GetDifferenceVector(u, r1, &arCoord[ind2], &arCoord[ind1]);

         Nb r2 = 0.0;
         GetDifferenceVector(v, r2, &arCoord[ind2], &arCoord[ind3]);

         Nb r3 = 0.0;
         GetDifferenceVector(w, r3, &arCoord[ind4], &arCoord[ind3]);
         
         Nb cosa1 = std::inner_product(std::begin(u), std::end(u), std::begin(v), 0.0);
         Nb cosa2 = std::inner_product(std::begin(v), std::end(v), std::begin(w), 0.0);
   
         Nb sina1 = std::sqrt( 1.0 - cosa1*cosa1 );
         Nb sina2 = std::sqrt( 1.0 - cosa2*cosa2 );

         vector<Nb> x(3);
         vector<Nb> z(3);

         GetNormalVector(z, u, v);
         GetNormalVector(x, w, v);

         vector<Nb> uu(3), zz(3), vv(3), ww(3);
         
         for (int idx = 0; idx < 3; idx++)
         { 
            uu[idx] = z[idx] / (r1 * sina1);
            zz[idx] = x[idx] / (r3 * sina2);
            vv[idx] = (r1*cosa1 / r2 - 1.0) * uu[idx] - r3*cosa2 / r2 *zz[idx];
            ww[idx] = -uu[idx] - vv[idx] - zz[idx];

            apBmat[arIrow*arNcart + ind1 + idx] = uu[idx] ;
            
            apBmat[arIrow*arNcart + ind2 + idx] = vv[idx] ;

            apBmat[arIrow*arNcart + ind3 + idx] = ww[idx] ;

            apBmat[arIrow*arNcart + ind4 + idx] = zz[idx] ;
         }

         // define same vectors for the reference structure
         vector<Nb> u_ref(3);
         vector<Nb> v_ref(3);
         vector<Nb> w_ref(3);
 
         Nb r1_ref = 0; 
         GetDifferenceVector(u_ref, r1_ref, &arRefCoord[ind2], &arRefCoord[ind1]);
         
         Nb r2_ref = 0; 
         GetDifferenceVector(v_ref, r2_ref, &arRefCoord[ind2], &arRefCoord[ind3]);
         
         Nb r3_ref = 0; 
         GetDifferenceVector(w_ref, r3_ref, &arRefCoord[ind4], &arRefCoord[ind3]);

         // calculate the dihedral angle for the current structure
         Nb tau = GetDihed(u, v, w);

         // and for the reference structure
         Nb tau_ref = GetDihed(u_ref, v_ref, w_ref);

         // define it relatively
         Nb dihed;
         if ((tau_ref > 0) && (tau <= -M_PI + tau_ref))
         {
             dihed = tau + 2*M_PI - tau_ref;
         }
         else if ((tau_ref < 0) && (tau > M_PI + tau_ref))
         {
             dihed = tau - 2*M_PI - tau_ref;
         }
         else 
         {
             dihed = tau - tau_ref;
         }
 
         // invert the sign to be consistent with gradients
         arVal = -dihed;
         
         break;
      }


      // ------- linear coplanar bending ---------------------
      case ( 5 ) :
      {
         vector<Nb> u(3);
         vector<Nb> v(3);
         vector<Nb> x(3);

         Nb r1 = C_0;
         GetDifferenceVector(u, r1, &arCoord[kcart], &arCoord[jcart]);

         Nb r2 = C_0;
         GetDifferenceVector(v, r2, &arCoord[lcart], &arCoord[jcart]);

         Nb r3 = C_0;
         GetDifferenceVector(x, r3, &arCoord[icart], &arCoord[jcart]);

         Nb cosa1 = std::inner_product(std::begin(u), std::end(u), std::begin(v), 0.0);
         Nb cosa2 = std::inner_product(std::begin(x), std::end(x), std::begin(v), 0.0);

         Nb angle = M_PI - std::acos(cosa1) - std::acos(cosa2);

         std::vector<Nb> w(3), z(3), y(3), t(3);

         GetNormalVector(w, v, u);
         GetNormalVector(z, u, w);
         GetNormalVector(y, w, v);
         GetNormalVector(w, x, v);
         GetNormalVector(u, w, x);
         GetNormalVector(t, v, w);

         std::vector<Nb> uu(3), vv(3), zz(3), ww(3);

         // internal coordinate positive if atom b moves towards atom d
         for (int idx = 0; idx < 3; idx++)
         {
            uu[idx] = z[idx] / r1;
            vv[idx] = u[idx] / r3;
            zz[idx] = (y[idx] + t[idx]) / r2;
            ww[idx] = -u[idx] - vv[idx] - zz[idx];

            apBmat[arIrow*arNcart + icart + idx] = uu[idx] ;
            
            apBmat[arIrow*arNcart + jcart + idx] = vv[idx] ;

            apBmat[arIrow*arNcart + kcart + idx] = ww[idx] ;

            apBmat[arIrow*arNcart + lcart + idx] = zz[idx] ;
         }

         // TODO: Check if values are correct. Not tested yet!!!
         break;
      }
   }

}
