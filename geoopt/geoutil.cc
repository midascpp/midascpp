/**
 ************************************************************************
 * 
 * @file                geoutil.cc 
 *
 * Created:             16-10-2018
 *
 * Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
 *
 * Short Description:   General functions to do stuff to molecules
 *                      taken out of GeoOpt.cc and GeoDatabase.cc for
 *                      better organisation.
 * 
 * Last modified: 
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
**/

#include "geoopt/geoutil.h"
#include "geoopt/GenerFuncs.h"

/**
 *  @brief Calculates the Wilson B matrix  
 *
 *  Purpose: Calculates the Wilson B matrix given a pair of cartesian and internal coordinates 
 *
 *  @param bmat will on output contain the Wilson B matrix
 *
 *  @param cartcoord is a vector in which the cartesian coordinates will be stored  
 *
 *  @param interncoord is the current representation in internal coordinates
 * 
 * */
void GetBMatrix
   (  Nb* bmat
   ,  const std::vector<Nb>& cartcoord
   ,  std::vector<InternalCoord>& interncoord
   )
{
   const bool locdbg = false;

   int ncart   = cartcoord.size();
   int nintern = interncoord.size();

   BmatBuilder bmatbuilder;

   // init B Matrix
   for (int idx = 0; idx < nintern*ncart; idx++)
   {
      bmat[idx] = 0.0;
   }

   if (locdbg) Mout << "Construct rows of B matrix" << std::endl;

   // Check for torsion angle
   // and read the reference geometry to define it relatively
   std::vector<Nb> ref_coord = GetRefCoords(interncoord, gSystemDefs);

   // construct B Matrix
   for (int intc = 0; intc < nintern; intc++)
   {
      int itype = interncoord[intc].itype;
      Nb dval = C_0;;

      bmatbuilder.GenerateRow( cartcoord, ref_coord, intc, bmat, dval, itype
                             , interncoord[intc].iconnect, ncart);

      interncoord[intc].val = dval;
   }

}


void GetDelocalizedCoordinates
   (  std::vector<Nb>& eigcoord
   ,  std::vector<Nb>& cartcoord 
   ,  std::vector<InternalCoord>& interncoord
   )
{

   const bool locdbg = false;

   int ncart   = cartcoord.size();
   int nintern = interncoord.size();

   std::unique_ptr<Nb[]> bmat(new Nb[ncart*nintern]);
   std::unique_ptr<Nb[]> cmat(new Nb[nintern*nintern]);

   GetBMatrix(bmat.get(), cartcoord, interncoord);

   char nn = 'N';
   char nt = 'T';
   Nb one  = 1.0;
   Nb zero = 0.0;

   midas::lapack_interface::gemm( &nt, &nn, &nintern, &nintern, &ncart
                                , &one, bmat.get(), &ncart
                                , bmat.get(), &ncart, &zero
                                , cmat.get(), &nintern );

   char jobz = 'V';
   char uplo = 'U';
   int lwork = -1;
   int info = 0;
   Nb dlen;


   std::unique_ptr<Nb[]> eig(new Nb[nintern]);

   // first run to get dimension for work
   midas::lapack_interface::syev( &jobz, &uplo, &nintern, cmat.get(), &nintern, eig.get(), &dlen, &lwork, &info);
   
   // allocate memory for work
   lwork = int(dlen);
   std::unique_ptr<Nb[]> work(new Nb[lwork]);
   
   // do eigen decomposition
   midas::lapack_interface::syev( &jobz, &uplo, &nintern, cmat.get(), &nintern, eig.get(), work.get(), &lwork, &info);

   for (int idx = nintern - 1; idx >= 0; idx--)
   {
      if (eig[idx] > 1e-12) 
      {
         eigcoord.push_back(eig[idx]);
      }
   }


}

/**
 *  @brief Performs a small distortion to update the structure 
 *
 *  Purpose: Performs a small distortion of the molecule in internal coordinates
 *           and returns the cartesian representation 
 *
 *  @param cartcoord is a vector in which the cartesian coordinates will be stored  
 *
 *  @param interncoord is the current representation in internal coordinates
 * 
 *  @param deltacoord is the distortion in internal coordinates
 *
 * */
void UpdateCoord
   (  vector<Nb>& cartcoord
   ,  vector<InternalCoord>& interncoord
   ,  vector<InternalCoord>& deltacoord  
   )
{

   const bool locdbg = false;

   int ncart = cartcoord.size();
   int nintern = interncoord.size();

   char nn = 'n';
   char nt = 't';
   int nvec = 1;
 
   Nb one = 1.0;
   Nb zero = 0.0;

   std::unique_ptr<Nb[]> bmat( new Nb[ncart*nintern]);

   // Get Wilson B-Matrix
   GetBMatrix(bmat.get(), cartcoord, interncoord);   

   vector<Nb> deltax(ncart);
   vector<Nb> deltaq(nintern);
   vector<Nb> ddq(nintern);      

   for (int icoord = 0; icoord < nintern; icoord++)
   {
      deltaq[icoord] = deltacoord[icoord].val;
   }

   
   int nsteps = 1;

   vector<Nb> deltaq_step = deltaq;

   Mout << "deltaq_step " << deltaq_step << std::endl;

   for (int istep = 1; istep <= nsteps; istep++)
   {
      deltaq = deltaq_step;

      for (int iter = 0; iter < 10; iter++)
      {
         // 1.) calculate coordinate update in cartesian coordinates dx
         
         // Build Pseudo-Inverse
         PseudoInvert(bmat.get(), ncart, nintern);

         // Do the transformation
         midas::lapack_interface::gemm( &nt, &nn, &ncart, &nvec, &nintern
                                      , &one, bmat.get(), &nintern
                                      , &deltaq[0], &nintern, &zero
                                      , &deltax[0], &ncart );

         if (locdbg) 
         {
            Mout << "delta q " << deltaq << std::endl;
            Mout << "delta x " << deltax << std::endl;
         }

         // 2.) Calculate new set of cartesian coordinates x' = x + dx
         std::transform(cartcoord.begin(), cartcoord.end(), deltax.begin(), cartcoord.begin(), std::plus<Nb>());

         // 3.) Get new set of internal coordinates
         vector<InternalCoord> newintern;
         newintern = interncoord;
         GetBMatrix(bmat.get(), cartcoord, newintern);   

         // 5.) Calculate difference of intended and actually achieved update ddq
         for (int icoord = 0; icoord < nintern; icoord++)
         {
            ddq[icoord] = deltacoord[icoord].val - (interncoord[icoord].val - newintern[icoord].val);
         }

         // 6.) Set new delta
         for (int icoord = 0; icoord < nintern; icoord++)
         {
            deltaq[icoord] = ddq[icoord];
         }

         // 7.) Check convergence
         Nb rmsd = std::sqrt( std::inner_product( ddq.begin(), ddq.end(), ddq.begin(), 0 ) / Nb(nintern) );

         Mout << "istep " << istep <<  " iter " << iter << " rmsd " << rmsd << std::endl;

         if (rmsd < 1e-10) break;
      }

      // 3.) Get new set of internal coordinates
      GetBMatrix(bmat.get(), cartcoord, interncoord);   
   }

}
 
/**
 *  @brief Centers a molecule to its center of mass 
 *
 *  Purpose: Centers a molecule represented using 
 *           cartesian coordinates to its center of mass     
 *
 *  @param coord is a vector with the cartesian coordinates sorted as  
 *         ( A_x A_y A_y B_x B_y B_z ) where A and B represent different atoms
 *
 *  @param amass is a vector containint the atomic masses
 *
 * */
void CenterMolecule
   (  std::vector<Nb>& coord
   ,  const std::vector<Nb>& amass
   )
{

   int natoms = amass.size();
   int ni = 3;
   int inc = 1;

   // copy coordinates to xyz  
   int ixyz = 0;
   Nb totmass = C_0;
   std::unique_ptr<Nb[]> xyz(new Nb[3*natoms]);
   for (int iat = 0; iat < natoms; iat++)
   {
      xyz[iat*3 + 0] = coord[iat*3 + 0];  
      xyz[iat*3 + 1] = coord[iat*3 + 1];  
      xyz[iat*3 + 2] = coord[iat*3 + 2];  
      totmass += amass[iat];
   }

   std::unique_ptr<Nb[]> trans(new Nb[natoms]);
   for (int iat = 0; iat < natoms; iat++)
   {
      trans[iat] = amass[iat] / totmass;
   }

   std::unique_ptr<Nb[]> R(new Nb[3]);
   for (int ix = 0; ix < 3; ix++)
   {
      R[ix] = midas::lapack_interface::dot(&natoms, &xyz[ix], &ni, trans.get(), &inc);
   }

   for (int iat = 0; iat < natoms; ++iat)
   {
      coord[iat*3 + 0] -= R[0];
      coord[iat*3 + 1] -= R[1];
      coord[iat*3 + 2] -= R[2];
   }

}

/**
 *  @brief Builds a projector to project out rotation and translation  
 *
 *  Purpose: Builds a projector to project out rotation and translation
 *
 *  @param Projector contains on output the projection operator
 *
 *  @param coord is a vector with the cartesian coordinates   
 *
 *  @param amass is a vector containing the atomic masses   
 *
 *  @param natoms specifies the number of atoms    
 * 
 * */
void BuildProjector
   (  std::unique_ptr<Nb[]>& Projector
   ,  const vector<Nb>& coord
   ,  const vector<Nb>& amass
   ,  int natoms
   )
{

   int ndim = coord.size();

   std::unique_ptr<Nb[]> v(new Nb[3*ndim]);

   for (int i = 0; i < ndim*ndim; i++){Projector[i] = C_0;}

   for (int i = 0; i < ndim; i++)
   {
      Projector[i*ndim + i] = 1.0;
   }

   Nb one  =  1.0;
   Nb zero =  0.0;
   Nb mone = -1.0;

   //--------------------------------------------------------+
   // Part to project out translation
   //--------------------------------------------------------+
   std::unique_ptr<Nb[]> xm(new Nb[natoms]);
   
   Nb totm = C_0;

   for (int iat = 0; iat < natoms; ++iat)
   {
      totm += amass[iat];
      xm[iat] = std::sqrt(amass[iat]);
   }
   totm = std::sqrt(totm);
   for (int iat = 0; iat < natoms; iat++){xm[iat] /= totm;}

   int i1 = 1;
   int i3 = 3;
   char uplo = 'l';

   for (int ix = 0; ix < 3; ix++)
   {
      for (int i = 0; i < 3*ndim; i++) {v[i] = C_0;}
      midas::lapack_interface::copy(&natoms, xm.get(), &i1, &v[ix], &i3);
      midas::lapack_interface::syr(&uplo, &ndim, &mone, v.get(), &i1, Projector.get(), &ndim);
   }

   //--------------------------------------------------------+
   // Part to project out rotation
   //--------------------------------------------------------+
   {
      char nn = 'N';
      char nt = 'T';
      char jobz = 'V';
      int n = 3;
      int m = 9;
      int lwork = -1;
      int info = 0;
      Nb dlen;

      // allocate memory
      std::unique_ptr<Nb[]> xi( new Nb[3*3]);
      std::unique_ptr<Nb[]> tmp(new Nb[3*3*natoms]);
      std::unique_ptr<Nb[]> w(  new Nb[3]);
      std::unique_ptr<Nb[]> xyz(new Nb[3*natoms]);

      // copy coordinates to xyz and mass weight them 
      int ixyz = 0;
      for (int iat = 0; iat < natoms; iat++)
      {
         xyz[iat*3 + 0] = coord[ixyz++] * std::sqrt(amass[iat]); 
         xyz[iat*3 + 1] = coord[ixyz++] * std::sqrt(amass[iat]); 
         xyz[iat*3 + 2] = coord[ixyz++] * std::sqrt(amass[iat]); 
      }

      // build inverse sqrt moment of inertia
      Nb dd = midas::lapack_interface::dot(&ndim, xyz.get(), &i1, xyz.get(), &i1);
      for (int i = 0; i < 3*3; i++) {xi[i] = C_0;}
      xi[0*3 + 0] = dd;
      xi[1*3 + 1] = dd;
      xi[2*3 + 2] = dd;
      midas::lapack_interface::gemm(&nn, &nt, &n, &n, &natoms, &mone, xyz.get(), &n, xyz.get(), &n, &one, xi.get(), &n);

      // first run to get dimension for work
      midas::lapack_interface::syev( &jobz, &uplo, &n, xi.get(), &n, w.get(), &dlen, &lwork, &info);
   
      // allocate memory for work
      lwork = int(dlen);
      std::unique_ptr<Nb[]> work(new Nb[lwork]);
   
      // do eigen decomposition
      midas::lapack_interface::syev( &jobz, &uplo, &n, xi.get(), &n, w.get(), work.get(), &lwork, &info);
      std::copy(xi.get(), xi.get() + 3*3, tmp.get());

      for (int ix = 0; ix < 3; ix++)
      {
         w[ix] = 1.0 / std::sqrt(w[ix]);
         w[ix] = std::sqrt(w[ix]);
         midas::lapack_interface::scal(&n, &w[ix], &tmp[ix*3], &i1); 
      }
      midas::lapack_interface::gemm(&nn, &nt, &n, &n, &n, &one, tmp.get(), &n, tmp.get(), &n, &zero, xi.get(), &n);

      // I^-1/2 is now on xi

      // build set of rotational vectors l for each R_alpha
      for (int i = 0; i < 3*3*natoms; i++) {tmp[i] = 0.0;}

      for (int ix = 1; ix <= 3; ix++)
      {
         int j = ix % 3;
         int k = (ix+1) % 3;
         midas::lapack_interface::copy(&natoms, &xyz[ix-1], &n, &tmp[j*3+k], &m);
         midas::lapack_interface::copy(&natoms, &xyz[ix-1], &n, &tmp[k*3+j], &m);
         midas::lapack_interface::scal(&natoms, &mone, &tmp[k*3+j], &m);
      }

      // l_(R_a,ci) = I^-1/2(a,d) tmp(d,ci)
      char sidel = 'l';
      midas::lapack_interface::symm(&sidel, &uplo, &n, &ndim, &one, xi.get(), &n, tmp.get(), &n, &zero, v.get(), &n);
      // l(R_alpha,ndim) now on v

      // update the projection matrix
      midas::lapack_interface::syrk(&uplo, &nt, &ndim, &n, &mone, v.get(), &n, &one, Projector.get(), &ndim);
   }

}

/**
 *  @brief Projects out rotation and translation from hessian  
 *
 *  Purpose: Projects out rotation and translation from hessian 
 *
 *  @param hessian is on input the hessian containing contr. from rotation and translation
 *         and on output the hessian, where these contr. a projected out.
 *
 *  @param coord is a vector with the cartesian coordinates   
 *
 *  @param amass is a vector containing the atomic masses   
 * 
 * */
void CleanHessian
   (  std::unique_ptr<Nb[]>& hessian
   ,  const vector<Nb>& coord
   ,  const vector<Nb>& amass
   )
{
   int natoms = coord.size() / 3;
   int ndim = coord.size();

   Nb one = 1.0;
   Nb zero = 0.0;
   Nb mone = -1.0;

   std::unique_ptr<Nb[]> Projector(new Nb[ndim*ndim]);

   BuildProjector(Projector, coord, amass, natoms);

   //--------------------------------------------------------+
   // Do the projection
   //--------------------------------------------------------+
   std::unique_ptr<Nb[]> tmp(new Nb[ndim*ndim]);   
 
   char sider = 'r';
   char sidel = 'l';
   char uplo = 'l';

   midas::lapack_interface::symm(&sider, &uplo, &ndim, &ndim, &one, Projector.get(), &ndim
                                , hessian.get(), &ndim, &zero,tmp.get(), &ndim );

   midas::lapack_interface::symm(&sidel, &uplo, &ndim, &ndim, &one, Projector.get(), &ndim
                                , tmp.get(), &ndim, &zero, hessian.get(), &ndim );

}

/**
 *  @brief Projects out rotation and translation from a gradient  
 *
 *  Purpose: Projects out rotation and translation from a gradient
 *
 *  @param grad is on input the gradient containing contr. from rotation and translation
 *         and on output the gradient, where these contr. a projected out.
 *
 *  @param coord is a vector with the cartesian coordinates   
 *
 *  @param amass is a vector containing the atomic masses   
 * 
 * */
void CleanGrad
   (  vector<Nb>& grad
   ,  const vector<Nb>& coord
   ,  const vector<Nb>& amass
   )
{
   int natoms = amass.size();
   int ndim = grad.size();

   Nb one = 1.0;
   Nb zero = 0.0;

   std::unique_ptr<Nb[]> Projector(new Nb[ndim*ndim]);

   BuildProjector(Projector, coord, amass, natoms);

   //--------------------------------------------------------+
   // Do the projection
   //--------------------------------------------------------+
   int inc = 1;
   char uplo = 'l';

   std::unique_ptr<Nb[]> tmp1(new Nb[ndim]);   
   std::unique_ptr<Nb[]> tmp2(new Nb[ndim]);   
 
   for (int i = 0; i < ndim; i++) {tmp1[i] = grad[i];}

   std::copy(tmp1.get(), tmp1.get() + ndim, tmp2.get());
   midas::lapack_interface::symv( &uplo, &ndim, &one, Projector.get(), &ndim 
                                , tmp2.get(), &inc, &zero, tmp1.get(), &inc);

   for (int i = 0; i < ndim; i++) {grad[i] = tmp1[i];}

}

/**
 *  @brief Does a normal mode analysis   
 *
 *  Purpose: Does a normal mode analysis  
 *
 * */
void NormalModeAnalysis
   (  std::vector<Nb>& xyz 
   ,  std::vector<Nb>& xyz_cart
   ,  std::vector<InternalCoord>& interncoord
   ,  const std::vector<Nb>& qmass
   ,  const std::vector<Nb>& amass
   ,  std::vector<Nb>& grad
   ,  std::unique_ptr<Nb[]>& hessian 
   ,  std::unique_ptr<Nb[]>& hess_out
   ,  const bool save_hessian 
   ,  const bool intcoord
   ,  const bool have_gradient
   ,  int natoms
   ,  int nintern
   )
{

   const bool prtHess = false;

   int ncart = 3 * natoms;

   std::unique_ptr<Nb[]> hess_cart(new Nb[ncart*ncart]);
   if (intcoord) 
   {
      // transform hessian in internal coordinates to hessian in cartesian coordinates

      // scratch space
      std::unique_ptr<Nb[]> scr(new Nb[ncart*nintern]);

      // get Wilson B Matrix
      std::unique_ptr<Nb[]> bmat(new Nb[ncart*nintern]); 
      GetBMatrix(bmat.get(), xyz_cart, interncoord);

      // 1.) Part form B H B^T
      char nn = 'N';
      char nt = 'T';
      Nb one  = 1.0;
      Nb zero = 0.0;
      
      midas::lapack_interface::gemm( &nn, &nn, &ncart, &nintern, &nintern
                                   , &one, bmat.get(), &ncart
                                   , hessian.get(), &nintern, &zero
                                   , scr.get(), &ncart );

      midas::lapack_interface::gemm( &nn, &nt, &ncart, &ncart, &nintern
                                   , &one, scr.get(), &ncart
                                   , bmat.get(), &ncart, &zero
                                   , hess_cart.get(), &ncart );

      // 2.) Gradient correction...
      // K_jk = \sum_i [g_q]_i B'_ijk
      // with B'_ijk = d^2 q_i / (dx_j dx_k)
      if (have_gradient)
      {
         std::vector<Nb> xyztmp = xyz_cart;

         Nb dxyz = 0.0005;

         for (int iat = 0; iat < natoms; iat++)
         {
            for (int ixyz = 0; ixyz < 3; ixyz++)
            {
               int iixyz = 3*iat + ixyz;

               // We use numerical differentiation to get the derivative of the B-matrix
               
               // Get B+
               xyztmp[iixyz] = xyz_cart[iixyz] + dxyz;
               std::unique_ptr<Nb[]> bmatp(new Nb[nintern*ncart]);
               GetBMatrix(bmatp.get(), xyztmp, interncoord);

               // Get B- 
               xyztmp[iixyz] = xyz_cart[iixyz] - dxyz;
               std::unique_ptr<Nb[]> bmatm(new Nb[nintern*ncart]);
               GetBMatrix(bmatm.get(), xyztmp, interncoord);

               // with (B+ - B-)/(2d) we have the derivative for a fixed iixyz and all jjxyz and idim

               for (int jat = 0; jat <= iat; jat++)
               {
                  for (int jxyz = 0; jxyz < 3; jxyz++)
                  {
                     int jjxyz = 3*jat + jxyz;

                     Nb gdBdxi = C_0;
                     for (int idim = 0; idim < nintern; idim++)
                     {
                        int iadr = idim*ncart + jjxyz;
                        gdBdxi += grad[idim] * (bmatp[iadr] - bmatm[iadr]) / (2.0 * dxyz);
                     }

                     hess_cart[iixyz*ncart + jjxyz] += gdBdxi;
                     if (jjxyz != iixyz) 
                     {
                        hess_cart[jjxyz*ncart + iixyz] += gdBdxi;
                     }
                  }
               }
            }
         } 
      }
   }
   else
   {
      std::copy(hessian.get(), hessian.get() + ncart*ncart, hess_cart.get()); 
   }

   if (save_hessian)
   {
      std::copy(hess_cart.get(), hess_cart.get() + ncart*ncart, hess_out.get());
   }

   if (prtHess)
   {
      Mout << "Hessian" << std::endl;
      Mout << ncart << std::endl;
      for (int icart = 0; icart < ncart; icart++)
      {
         for (int jcart = 0; jcart < ncart; jcart++)
         {
            Mout << hess_cart[icart*ncart + jcart] << " ";
         }
         Mout << std::endl;
      }
   }

   // mass weight hessian H_w = M^{-1/2} H M^{-1/2} 
   for (int idx = 0; idx < ncart; idx++)
   {
      for (int jdx = 0; jdx < ncart; jdx++)
      {
         hess_cart[jdx*ncart + idx] /= std::sqrt(qmass[idx]) * std::sqrt(qmass[jdx]); 
      }
   }

   // remove translation and rotation 
   CleanHessian(hess_cart, xyz_cart, amass);

   char jobz = 'V';
   char uplo = 'U';
   int n = ncart;
   int lwork = -1;
   int info = 0;
   Nb dlen;
   std::unique_ptr<Nb[]> eig(new Nb[ncart]);
   

   // first run to get dimension for work
   midas::lapack_interface::syev( &jobz, &uplo, &n, hess_cart.get(), &n, eig.get(), &dlen, &lwork, &info);
   
   // allocate memory for work
   lwork = int(dlen);
   std::unique_ptr<Nb[]> work(new Nb[lwork]);
   
   // do eigen decomposition
   midas::lapack_interface::syev( &jobz, &uplo, &n, hess_cart.get(), &n, eig.get(), work.get(), &lwork, &info);
 
   ios::fmtflags oflags( Mout.flags() );  // save current output flag
 
   // Print frequencies: v = \sqrt(eig) 
   for (int idx = 0; idx < ncart; idx++)
   {
      Nb sqrtau2amu = 42.6952981642285;
      Nb au2wav = 219474.63067;

      Nb wav;
      if (eig[idx] > 0.0)
      {
         wav = std::sqrt( eig[idx]) * au2wav / sqrtau2amu ;
      }
      else
      {
         wav = -std::sqrt(-eig[idx]) * au2wav / sqrtau2amu ;
      }

      Mout << "  v" << std::left  << std::setw(3)  << idx  << std::right << std::fixed << std::setw(12) << std::setprecision(2) << wav << std::endl;
   }

   Mout.flags( oflags );   // restore output flags

}


void WriteInternalCoordinates
   (  ostream& out
   ,  std::vector<InternalCoord>& icoord  
   )
{

   for (In idx = 0; idx < icoord.size(); idx++)
   {
   
      In itype = icoord[idx].itype;
      In iatom = icoord[idx].iatom;
      Nb val = icoord[idx].val;
   
      out << "itype "   << itype 
          << " val "    << (val >= 0 ? " ":"")  << std::scientific << val 
          << " iat "    << iatom 
          << " atoms ";

      for(std::vector<int>::iterator it = icoord[idx].iconnect.begin(); it != icoord[idx].iconnect.end(); ++it) 
      {
        out << *it << " ";
      }

      out << std::endl;
   }

}

std::vector<InternalCoord> ReadInternalCoordinates
   (  std::istream& inp  
   )
{

   std::vector<InternalCoord> icoord;
   std::string s;

   while(midas::input::GetLine(inp, s) )
   {
      std::istringstream str_stream(s);
      
      std::string stype, sval, satoms, siat;

      char ch;
      Nb val;
      In itype;
      In iatom;
      std::vector<In> iconnect;

      InternalCoord newicoord;

      str_stream >> stype >> itype >> sval >> val >> siat >> iatom >> satoms; 

      /* Running loop till the end of the stream */
      std::string temp; 
      int found; 
      while (!str_stream.eof()) 
      { 
  
         /* extracting word by word from stream */
         str_stream >> temp; 
 
         /* Checking the given word is integer or not */
         if (std::stringstream(temp) >> found) 
         {
            iconnect.push_back(found);
         }             
  
         temp = ""; 
      } 

      newicoord.itype = itype;
      newicoord.iatom = iatom;
      newicoord.val = val;
      newicoord.iconnect = iconnect;

      icoord.push_back(newicoord);
   } 

   return icoord;
}

std::vector<Nb> TransformHessian
   (  std::vector<Nb>& aCartHess
   ,  const std::vector<Nb>& aCartCoord
   ,  const std::vector<InternalCoord>& aInternCoord
   ,  const int& aNatoms
   ,  const int& aNintern
   ,  const int& aNcart
   )
{
   // Return vector
   std::vector<Nb> hess(aNintern*aNintern);

   char nn = 'N';
   char nt = 'T';
   In nvec = 1 ;
   Nb one  = 1.0;
   Nb zero = 0.0;

   int ncart = aNcart;
   int nintern = aNintern;

   std::vector<InternalCoord> interncoord = aInternCoord;

   // scratch space
   In nscr = std::max(ncart*nintern,nintern*nintern);
   std::unique_ptr<Nb[]> scr(new Nb[nscr]);
   
   //*************************************************************+
   // H_i = B^+^T (H_x - K) B^+
   //*************************************************************+
  
   // get Wilson B Matrix
   std::unique_ptr<Nb[]> bmat(new Nb[ncart*nintern]);
   GetBMatrix(bmat.get(),aCartCoord,interncoord);   
   
   // And build its PseudoInverse B^+
   std::unique_ptr<Nb[]> bmatp(new Nb[ncart*nintern]); 
   GetBMatrix(bmatp.get(),aCartCoord,interncoord);   
   PseudoInvert(bmatp.get(),ncart,nintern);

   // 1.) Part form B^+^T H B^+
   midas::lapack_interface::gemm( &nn, &nn, &nintern, &ncart, &ncart
                                , &one, bmatp.get(), &nintern
                                , &aCartHess[0], &ncart, &zero
                                , scr.get(), &nintern );
   
   midas::lapack_interface::gemm( &nn, &nt, &nintern, &nintern, &ncart
                                , &one, scr.get(), &nintern
                                , bmatp.get(), &nintern, &zero
                                , &hess[0], &nintern );
   
   
   // Build projection P = B B^+  to ensure physical meaningful gradient
   std::unique_ptr<Nb[]> proj(new Nb[nintern*nintern]);
   midas::lapack_interface::gemm( &nt, &nt, &nintern, &nintern, &ncart
                                , &one, bmat.get(), &ncart
                                , bmatp.get(), &nintern, &zero
                                , proj.get(), &nintern );
   
   // Do the projection H = P H P
   midas::lapack_interface::gemm( &nn, &nn, &nintern, &nintern, &nintern
                                , &one, proj.get(), &nintern
                                , &hess[0], &nintern, &zero
                                , scr.get(), &nintern );
   
   midas::lapack_interface::gemm( &nn, &nt, &nintern, &nintern, &nintern
                                , &one, scr.get(), &nintern
                                , proj.get(), &nintern, &zero
                                , &hess[0], &nintern );


   return hess;
}


std::vector<Nb> GetRefCoords(const std::vector<InternalCoord>& coord_def, const std::vector<SystemDef>& arSystemDefs)
{
   std::vector<Nb> ref_coords;
  
   // Check if we need to calculate at least one torsion/dihedral angle
   for (In i = 0; i < coord_def.size(); ++i)
   {
      if(coord_def[i].itype == I_4)
      {
         // if so, read and return ref coordinates vector
         
         // check that we have a single system 
         if (arSystemDefs.size() == I_0)
         {
            MIDASERROR("Information about the system is not provided, .mmol file is not found");
         }
         else if (arSystemDefs.size() > I_1)
         {
            MIDASERROR("The use of multiple systems and GPR is not yet implemented");
         }
         
         molecule::MoleculeFileInfo file_info = arSystemDefs[0].GetMoleculeFileInfo();
         midas::molecule::MoleculeInfo molecule(file_info);
         
         std::vector<Nuclei> nuclei = molecule.GetCoord();
         for (auto nucleus : nuclei)
         {
            ref_coords.push_back(nucleus.X());
            ref_coords.push_back(nucleus.Y());
            ref_coords.push_back(nucleus.Z());
         }
         break;
      }
   }
  
   // return empty if not found
   return ref_coords;
}
