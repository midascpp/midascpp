#include <numeric>
#include <random>

#include "geoopt/GeoDatabase.h"
#include "mlearn/symmetryfunctions.h"
#include "geoopt/GenerFuncs.h"


/**
 *  @brief Will generate a GeoDatabase from plus and generate add. test structures 
 *
 *  Purpose: Will generate a GeoDatabase from plus and generate add. structures by scanning
 *           along the internal coordinates
 *
 *  @param file contains the filename for the file with the molecular structures  
 *
 *  @param nintermed determines how many additional structures should be generated 
 *
 **/
GeoDatabase::GeoDatabase
   (  const std::string& file
   ,  const In& nintermed
   )
{
   // 1.) read coordinates and transform to internal coordinates
   mType = MINT;
   ReadCoordinates(file);

   // 2.) create additional structures by scaning along the internal coordinates
   In nintern = mCoordinates[0].size();

   vector<string> elemsym = mSymbols[0];

   for (In icoord = 0; icoord < nintern; icoord++)
   {

      for (In istep = 0; istep < 2; istep++)
      {
         Nb sign = 1.0;
         if (istep == 1) sign = -1.0;
         if (istep == 3) sign = -1.0;

         vector<InternalCoord> delta;
         vector<InternalCoord> interncoord;
         interncoord = mInternCoord[0];
         delta = mInternCoord[0];
     
         if (istep >= 2 && interncoord[icoord].itype < 2) continue;

         switch (interncoord[icoord].itype)
         {
            case(1):
            {
               delta[icoord].val = sign * interncoord[icoord].val * 0.15; 
               break;
            }
            case(2):
            {
               if (istep > 2) 
               {
                  delta[icoord].val = sign * interncoord[icoord].val * 0.15; 
               }
               else
               {
                  delta[icoord].val = sign * interncoord[icoord].val * 0.10; 
               }
               break;
            }
            case(4):
            {
               if (istep > 2) 
               {
                  delta[icoord].val = sign * interncoord[icoord].val * 0.2; 
               }
               else
               {
                  if (std::abs(interncoord[icoord].val) < 182.0 && std::abs(interncoord[icoord].val) > 178.0 )
                  {
                     delta[icoord].val = sign * interncoord[icoord].val * 0.005;
                  }
                  if (std::abs(interncoord[icoord].val) < 122.0 && std::abs(interncoord[icoord].val) > 118.0 )
                  {
                     delta[icoord].val = sign * interncoord[icoord].val * 0.008;
                  }
                  else
                  {
                     delta[icoord].val = sign * interncoord[icoord].val * 0.05; 
                  }
               }
               break;
            }
         }

         for (In jcoord = 0; jcoord < nintern; jcoord++)
         {
            if (jcoord != icoord) delta[jcoord].val = 0.0; 
         }

         vector<Nb> cartcoord = this->GetCartCoord(0);
         UpdateCoord(cartcoord, interncoord, delta);
   
         mInternCoord.push_back( std::move(interncoord) );
         mCoordinates.push_back(GetCoord(interncoord));
         mCartCoord.push_back(cartcoord);
         mEnergies.push_back(0.0);
         mSymbols.push_back(elemsym);
      }
   }
 
}

/**
 *  @brief Return set of coordinates from a definition of internal coordinates
 *
 **/
std::vector<Nb> GeoDatabase::GetCoord
   ( const vector<InternalCoord>& aInternCoord
   ) const
{
   In nintern = aInternCoord.size();

   vector<Nb> coord(nintern);

   for (In icoord = 0; icoord < nintern; ++icoord)
   {
      coord[icoord] = aInternCoord[icoord].val;
   }

   return coord;
}


/**
 *  @brief Reads in coordinates from file 
 *
 **/
void GeoDatabase::ReadCoordinates
   (  const std::string& aFileName
   )
{
   std::vector<In> dumivec(0);
   this->ReadCoordinates(aFileName, false, dumivec, dumivec, dumivec);
}

/**
 *  @brief Reads in coordinates from file 
 *
 **/
void GeoDatabase::ReadCoordinates
   (  const std::string& aFileName
   ,  const bool& enforce_interndef
   ,  const std::vector<InternalCoord>& aIcoordDef
   )
{
   std::vector<In> dumivec(0);
   this->ReadCoordinates(aFileName, false, dumivec, dumivec, dumivec, enforce_interndef, aIcoordDef);
}


/**
 *  @brief Reads in coordinates from file 
 *
 **/
void GeoDatabase::ReadCoordinates
   (  const std::string& aFileName
   ,  const bool& enforce_connectivity
   ,  const std::vector<In>& aNa 
   ,  const std::vector<In>& aNb 
   ,  const std::vector<In>& aNc 
   )
{
   std::vector<InternalCoord> icoorddef;

   ReadCoordinates
      (  aFileName
      ,  enforce_connectivity
      ,  aNa
      ,  aNb
      ,  aNc
      ,  false 
      ,  icoorddef
      );
}

/**
 *  @brief Reads in coordinates from file 
 *
 **/
void GeoDatabase::ReadCoordinates
   (  const std::string& aFileName
   ,  const bool& enforce_connectivity
   ,  const std::vector<In>& aNa 
   ,  const std::vector<In>& aNb 
   ,  const std::vector<In>& aNc 
   ,  const bool& enforce_interndef
   ,  const std::vector<InternalCoord>& icoorddef
   )
{
   // constants
   const bool locdbg = false;

   const Nb Ang2Bohr = C_1 / C_TANG;

   // open/preprocess file
   std::string s;

   std::ifstream coord_file;
   coord_file.open (aFileName, std::ifstream::in);  

   // For enforcing same connectivity in the database
   bool HasConnectivity = false;
   const bool ReuseConnectivity = true;

   std::vector<In> oldna;
   std::vector<In> oldnb;
   std::vector<In> oldnc;

   if (coord_file.good()) 
   {
      // read atom lines
      In n_atoms = I_0;
      while(midas::input::GetLine(coord_file, s) )
      {
         Nb x, y, z, energy;
         std::string label, dummy1, dummy2;
         In natoms = 0;
         In ngrad = 0;
         In nhess = 0;         

         // TODO: We do no sanity checks and assume the file is in the correct format

         // 1) read number of Coordinates
         natoms = midas::util::FromString<In>(s);

         if (locdbg) Mout << "natoms " << natoms << std::endl;

         // 2) read energy for current structure
         {
            midas::input::GetLine(coord_file, s);
            std::istringstream str_stream(s);
            str_stream >> dummy1 >> dummy2 >> energy;
         }

         // 3) read coordinates
         std::vector<Nb> newcoord;
         std::vector<Nb> cartcoord;
         std::vector<InternalCoord> interncoord;
         std::vector<Nb> amass;
         std::vector<std::string> newsymbols;
         for (In idx = 0; idx < natoms; idx++)
         {
            midas::input::GetLine(coord_file, s);

            std::istringstream str_stream(s);
            str_stream >> label >> x >> y >> z;
 
            newsymbols.push_back(label);
            newcoord.push_back(x * Ang2Bohr);
            newcoord.push_back(y * Ang2Bohr);
            newcoord.push_back(z * Ang2Bohr);

            std::string sym = label;
            std::transform(sym.begin(), sym.end(), sym.begin(), tolower);
            amass.push_back(masses[sym]);

         }

         switch (mType)
         {
            case XYZ:
            {
               // NOP
               break;
            }
            case XYZCENTER:
            {
               // Center molecule to center of mass
               CenterMolecule(newcoord, amass);
               break;
            }
         
            case ZMAT:
            case MINT:
            {
               // Convert to internal coordinates
               std::vector<In> na(natoms), nb(natoms), nc(natoms);
         
               std::vector<Nb> geo(3*natoms);
         
               // save cartesian coordinates
               cartcoord = newcoord;

               IdentifyConnectifity
                  (  geo
                  ,  newcoord
                  ,  enforce_connectivity
                  ,  ReuseConnectivity
                  ,  HasConnectivity
                  ,  na
                  ,  nb
                  ,  nc
                  ,  oldna
                  ,  oldnb
                  ,  oldnc
                  ,  aNa
                  ,  aNb
                  ,  aNc
                  ,  natoms
                  ,  rad2deg
                  );

               if (mType == ZMAT) 
               {
                  // overwrite cartesian coordinates
                  for (In iat = 0; iat < natoms; iat++)
                  {
                      newcoord[iat*3 + 0] = geo[iat*3 + 0] ; 
                      newcoord[iat*3 + 1] = geo[iat*3 + 1] ; 
                      newcoord[iat*3 + 2] = geo[iat*3 + 2] ; 
                  }
         
                  mZmatNa.push_back(na);
                  mZmatNb.push_back(nb);
                  mZmatNc.push_back(nc);
               }
               else if (mType == MINT) 
               {
                  if (enforce_interndef)
                  {
                     // Use user definied defintion of internal coordinates
                     DefineInternals(interncoord, icoorddef, newcoord, cartcoord);
                  }
                  else
                  { 
                     std::vector<In> itype;

                     // Convert Z-Matrix representation to minimal set of internal coordinates
                     Zmat2Int( newcoord, &geo[0], natoms, &na[0], &nb[0], &nc[0]
                             , interncoord, itype);
                  }

                  // overwrite cartesian coordinates
                  newcoord.resize(interncoord.size());
                  for (In intc = 0; intc < interncoord.size(); intc++)
                  {
                     newcoord[intc] = interncoord[intc].val; 
                  }
               }
               break;
            }
            case DIST:
            case INV:
            case EIGDIST:
            {

               SetupDistances
                  (  cartcoord
                  ,  newcoord
                  ,  interncoord
                  ,  natoms
                  );

               break;
            }
            case SYMG2:
            case SYMG4:
            {
               CreateSymmetryFunctions
                  (  cartcoord
                  ,  newcoord
                  ,  interncoord
                  ,  natoms
                  );

               break;
            }
         }

         // Test if we have already read this set of coordinates once or
         // if it is very similar to existing data. This can fuck up the
         // Gaussian Process Regression
         if (mScreen)
         {
            bool skipit = false;

            for (In idx = 0; idx < mCoordinates.size(); idx++)
            {
               Nb rmsd = 0.0;

               for (In idim = 0; idim < newcoord.size(); idim++)
               {
                  rmsd += std::pow( newcoord[idim] - mCoordinates[idx][idim], 2.0 );
               }
               rmsd /= Nb(newcoord.size());
               rmsd = std::sqrt(rmsd);

               if (rmsd < mTolrmsd) skipit = true;
            }

            if (skipit) 
            {
               // TODO Get ride of this searching in the file...

               // Save stream position since we might rewind...
               std::streampos oldpos = coord_file.tellg();

               // Read next line to test if we have gradient information
               midas::input::GetLine(coord_file, s);

               if (s.find("Gradient") != std::string::npos)
               {
                  for (In idx = 0; idx < natoms; idx++)
                  {
                     midas::input::GetLine(coord_file, s);
                  }
               }
               else
               {
                  coord_file.seekg(oldpos);
               }

               // Save stream position since we might rewind...
               oldpos = coord_file.tellg();

               // Read next line to test if we have Hessiam information
               midas::input::GetLine(coord_file, s);

               if (s.find("Hessian") != std::string::npos)
               {
                  auto str_vec = midas::util::StringVectorFromString(s,' ');
                  MidasAssert(str_vec.size() == 2, "Problem when reading Hessian information.");
                  nhess = midas::util::FromString<In>(str_vec[1]);

                  for (In ihessline = 0; ihessline < nhess; ihessline++)
                  {
                     midas::input::GetLine(coord_file, s);
                  }

               }
               else
               {
                  coord_file.seekg(oldpos);
               }

               continue;
            }
         }

         mNatoms = natoms;
         mCoordinates.push_back(newcoord);        
         mSymbols.push_back(newsymbols); 
         mEnergies.push_back(energy);

         // Add new set of internal coordinates
         if (  mType == MINT 
            || mType == DIST 
            || mType == EIGDIST 
            || mType == INV
            || mType == SYMG2 
            || mType == SYMG4 
            )
         {
            mInternCoord.push_back( interncoord );
            mCartCoord.push_back( cartcoord );
         }
         else
         {
            mCartCoord.push_back(newcoord);
         }
    
         // Save stream position since we might rewind...
         std::streampos oldpos = coord_file.tellg();
         // Read next line to test if we have gradient information
         midas::input::GetLine(coord_file, s);
 
         // Add Gradient
         if (s.find("Gradient") != std::string::npos)
         {
            std::vector<Nb> grad = ReadGradient(coord_file, natoms);

            mGradient.push_back(grad);
            In jmol = std::count_if(mImapGrad.begin(), mImapGrad.end(), [this](int i) {return i != NOT_INIT;});   
            mImapGrad.push_back(jmol);
         }
         else
         {
            mImapGrad.push_back(NOT_INIT);
            coord_file.seekg(oldpos);
         }

         // Save stream position since we might rewind...
         oldpos = coord_file.tellg();
         // Read next line to test if we have gradient information
         midas::input::GetLine(coord_file, s);

         // Add Hessian
         if (s.find("Hessian") != std::string::npos)
         {
            auto str_vec = midas::util::StringVectorFromString(s,' ');
            MidasAssert(str_vec.size() == 2, "Problem when reading Hessian information.");
            nhess = midas::util::FromString<In>(str_vec[1]);

            // Read Hessian from file 
            std::vector<Nb> hess = ReadHessian(coord_file,nhess,natoms); 
            mHessian.push_back(hess); 

            In jmol = std::count_if(mImapHess.begin(), mImapHess.end(), [this](In i) {return i != NOT_INIT;});   
            mImapHess.push_back(jmol);
         }
         else
         {
            mImapHess.push_back(NOT_INIT);
            coord_file.seekg(oldpos);
         }

      }

      // close/postprocess file
      coord_file.close();
   }
}

std::vector<Nb> GeoDatabase::ReadGradient 
   (  std::ifstream& aFile 
   ,  const In& arNatoms 
   )
{
   const bool locdbg = false;
   const Nb Ang2Bohr = C_1 / C_TANG;

   std::string s;

   // Read in Gradient in cartesian coordinates
   std::vector<Nb> cartgrad;
   for (In idx = 0; idx < arNatoms; idx++)
   {
      Nb dx, dy, dz;
   
      midas::input::GetLine(aFile, s);
   
      std::istringstream str_stream(s);
      str_stream >> dx >> dy >> dz;
   
      cartgrad.push_back(dx * Ang2Bohr);
      cartgrad.push_back(dy * Ang2Bohr);
      cartgrad.push_back(dz * Ang2Bohr);
   
   }
   
   In imol = mCoordinates.size() - I_1;  // Current index of molecule
   std::vector<Nb> grad;
   
   if (  mType == MINT 
      || mType == DIST 
      || mType == INV
      || mType == SYMG2 
      || mType == SYMG4 
      )
   {
      // Convert cartesian gradient to internal coordinates
   
      int nintern = mInternCoord[imol].size();
      int ncart = 3 * arNatoms;
   
      // Get Wilson B-matrix 
      std::unique_ptr<Nb[]> bmat  = this->GetBMatrix(imol);
   
      // and build its PseudoInverse
      std::unique_ptr<Nb[]> bmatp = this->GetBMatrix(imol);
      PseudoInvert(bmatp.get(),ncart,nintern);

      grad.resize(nintern);
   
      // g_int = B^+ g_x
      midas::lapack_interface::gemm( &nn, &nn, &nintern, &nvec, &ncart
                                   , &one, bmatp.get(), &nintern
                                   , &cartgrad[0], &ncart, &zero
                                   , &grad[0], &nintern );
   
      // Build projection P = B B^+  to ensure physical meaningful gradient
      std::unique_ptr<Nb[]> proj(new Nb[nintern*nintern]);
      midas::lapack_interface::gemm( &nt, &nt, &nintern, &nintern, &ncart
                                   , &one, bmat.get(), &ncart
                                   , bmatp.get(), &nintern, &zero
                                   , proj.get(), &nintern );
   
      std::vector<Nb> temp = grad;
      midas::lapack_interface::gemm( &nn, &nn, &nintern, &nvec, &nintern
                                   , &one, proj.get(), &nintern
                                   , &temp[0], &nintern, &zero
                                   , &grad[0], &nintern );
   
      if (locdbg) Mout << "Gradient: " << grad << std::endl;
   
   }
   else
   {
      grad = cartgrad;
   }

   return grad;
}

std::vector<Nb> GeoDatabase::ReadHessian
   (  std::ifstream& aFile 
   ,  const In& arNhess
   ,  const In& arNatoms 
   )
{
   const Nb Ang2Bohr = C_1 / C_TANG;

   std::string s;

   // Read in Hessian in cartesian coordinates
   In imol = mCoordinates.size() - I_1;  // Current index of molecule
   In ncart = 3 * mNatoms;
   std::vector<Nb> carthess(ncart*ncart);
   
   for (In ihessline = 0; ihessline < arNhess; ihessline++)
   {
      midas::input::GetLine(aFile, s);
   
      std::vector<std::string> hess_line = midas::util::StringVectorFromString(s);
   
      In n = midas::util::FromString<In>(hess_line.at(0)) - I_1;
      In m = midas::util::FromString<In>(hess_line.at(1)) - I_1;
   
      for(In i = 0; i < hess_line.size() - I_2; ++i)
      {
         Nb val = midas::util::FromString<Nb>(hess_line.at(i+I_2)) * Ang2Bohr;
         carthess[(m*5+i)*ncart + n] = val;
      }
   
   }
   
   if (  mType == MINT 
      || mType == DIST 
      || mType == INV
      || mType == SYMG2 
      || mType == SYMG4 
      )
   {
      // Convert cartesian hessian to internal coordinates
      In nintern = mInternCoord[imol].size();
      std::vector<Nb> hess(nintern*nintern);
   
      // scratch space
      In nscr = std::max(ncart*nintern,nintern*nintern);
      std::unique_ptr<Nb[]> scr(new Nb[nscr]);
   
      //*************************************************************+
      // H_i = B^+^T (H_x - K) B^+
      //*************************************************************+
   
      // 0.) Add Gradient correction...
      // K_jk = \sum_i [g_q]_i B'_ijk
      // with B'_ijk = d^2 q_i / (dx_j dx_k)
      //if (this->HasGradientInfo(imol))
      if (false)
      {
         std::vector<Nb> grad = mGradient[mImapGrad[imol]];
         std::vector<Nb> xyz_cart = mCartCoord[imol];
         Nb dxyz = 0.0005; 
   
         for (int iat = 0; iat < arNatoms; iat++)
         {
            for (int ixyz = 0; ixyz < 3; ixyz++)
            {
               int iixyz = 3*iat + ixyz;
   
               // We use numerical differentiation to get the derivative of the B-matrix
               std::vector<Nb> xyztmp = mCartCoord[imol];
               
               // Get B^(p)
               xyztmp[iixyz] = xyz_cart[iixyz] + dxyz;
               std::unique_ptr<Nb[]> bmatp = this->GetBMatrix(imol, xyztmp);
   
               // Get B^(m) 
               xyztmp[iixyz] = xyz_cart[iixyz] - dxyz;
               std::unique_ptr<Nb[]> bmatm = this->GetBMatrix(imol, xyztmp);
   
               // with (B^(p) - B^(m))/(2d) we have the derivative for a fixed iixyz and all jjxyz and idim
   
               for (int jat = 0; jat <= iat; jat++)
               {
                  int iend = 3;
   
                  for (int jxyz = 0; jxyz < iend; jxyz++)
                  {
                     int jjxyz = 3*jat + jxyz;
   
                     Nb gdBdxi = C_0;
                     for (int idim = 0; idim < nintern; idim++)
                     {
                        int iadr = idim*ncart + jjxyz;
                        gdBdxi += grad[idim] * (bmatp[iadr] - bmatm[iadr]) / (2.0 * dxyz);
                     }
   
                     int ijxyz = std::max(iixyz,jjxyz)*ncart + std::min(iixyz,jjxyz);
                     carthess[ijxyz] -= gdBdxi;
   
                     if (jjxyz != iixyz)
                     {
                        int jixyz = std::min(iixyz,jjxyz)*ncart + std::max(iixyz,jjxyz);
                        carthess[jixyz] -= gdBdxi;
                     }
                  }
               }
   
            }
         } 
      }
   
      // get Wilson B Matrix
      std::unique_ptr<Nb[]> bmat = this->GetBMatrix(imol);
   
      // And build its PseudoInverse B^+
      std::unique_ptr<Nb[]> bmatp = this->GetBMatrix(imol);
      PseudoInvert(bmatp.get(),ncart,nintern);

      // 1.) Part form B^+^T H B^+
      midas::lapack_interface::gemm( &nn, &nn, &nintern, &ncart, &ncart
                                   , &one, bmatp.get(), &nintern
                                   , &carthess[0], &ncart, &zero
                                   , scr.get(), &nintern );
   
      midas::lapack_interface::gemm( &nn, &nt, &nintern, &nintern, &ncart
                                   , &one, scr.get(), &nintern
                                   , bmatp.get(), &nintern, &zero
                                   , &hess[0], &nintern );
   
   
      // Build projection P = B B^+  to ensure physical meaningful gradient
      std::unique_ptr<Nb[]> proj(new Nb[nintern*nintern]);
      midas::lapack_interface::gemm( &nt, &nt, &nintern, &nintern, &ncart
                                   , &one, bmat.get(), &ncart
                                   , bmatp.get(), &nintern, &zero
                                   , proj.get(), &nintern );
   
      // Do the projection H = P H P
      midas::lapack_interface::gemm( &nn, &nn, &nintern, &nintern, &nintern
                                   , &one, proj.get(), &nintern
                                   , &hess[0], &nintern, &zero
                                   , scr.get(), &nintern );
   
      midas::lapack_interface::gemm( &nn, &nt, &nintern, &nintern, &nintern
                                   , &one, scr.get(), &nintern
                                   , proj.get(), &nintern, &zero
                                   , &hess[0], &nintern );
   
      return hess; 
   }
   else
   {
      return carthess;
   }
}

void GeoDatabase::SetupDistances
   (  std::vector<Nb>& aCartCoord
   ,  std::vector<Nb>& aNewCoord
   ,  std::vector<InternalCoord>& aInternCoord
   ,  const In& aNatoms
   )
{
   // save cartesian coordinates
   aCartCoord = aNewCoord;
   
   for (In iat = 0; iat < aNatoms; iat++)
   {
      for (In jat = 0; jat < iat; jat++)
      {
         InternalCoord newicoord;
   
         // calculate distance
         Nb dist = std::sqrt(  (aNewCoord[iat*3 + 0]-aNewCoord[jat*3 + 0]) * (aNewCoord[iat*3 + 0]-aNewCoord[jat*3 + 0])
                            +  (aNewCoord[iat*3 + 1]-aNewCoord[jat*3 + 1]) * (aNewCoord[iat*3 + 1]-aNewCoord[jat*3 + 1])
                            +  (aNewCoord[iat*3 + 2]-aNewCoord[jat*3 + 2]) * (aNewCoord[iat*3 + 2]-aNewCoord[jat*3 + 2])
                            );
   
         // add stretch
         if (mType == INV)
         {
            newicoord.val      = 1.0/dist;
            newicoord.itype    = -1;
         }
         else
         {
            newicoord.val      = dist;
            newicoord.itype    = 1;
         }
         newicoord.iatom    = iat;
         newicoord.iconnect = std::vector<In>{jat, iat} ;         
   
         aInternCoord.push_back(newicoord);
      }
   }
   
   // overwrite cartesian coordinates
   if (mType == DIST || mType == INV)
   {
      aNewCoord.resize(aInternCoord.size());
      for (In intc = 0; intc < aInternCoord.size(); intc++)
      {
         aNewCoord[intc] = aInternCoord[intc].val; 
      }
   }
   else
   {
      std::vector<Nb> eig;
      GetDelocalizedCoordinates(eig,aCartCoord,aInternCoord);
   
      aNewCoord.resize(eig.size());
      for (In intc = 0; intc < eig.size(); intc++)
      {
         aNewCoord[intc] = eig[intc];
      }
   }
}

void GeoDatabase::CreateSymmetryFunctions
   (  std::vector<Nb>& aCartCoord
   ,  std::vector<Nb>& aNewCoord
   ,  std::vector<InternalCoord>& aInternCoord
   ,  const In& aNatoms
   )
{
   // save cartesian coordinates
   aCartCoord = aNewCoord;

   for (In iat = 0; iat < aNatoms; iat++)
   {
      std::vector<In> iconnect(1,iat);

      Nb SymmetryFunction = 0.0;
      int icode = 0;

      if (mType == SYMG2) 
      {
         SymmetryFunction = SymmetryFunctions::G2(aNewCoord,iconnect,iat,aNatoms); 
         icode = 82;
      }
      if (mType == SYMG4) 
      {
         SymmetryFunction = SymmetryFunctions::G4(aNewCoord,iconnect,iat,aNatoms); 
         icode = 84;
      }

      InternalCoord newicoord;

      newicoord.val      = SymmetryFunction;
      newicoord.itype    = icode;
      newicoord.iatom    = iat;
      newicoord.iconnect = iconnect;         
   
      aInternCoord.push_back(newicoord);
   }
 
   // overwrite cartesian coordinates
   aNewCoord.resize(aInternCoord.size());
   for (In idx = 0; idx < aInternCoord.size(); idx++)
   {
      aNewCoord[idx] = aInternCoord[idx].val; 
   }

}

void GeoDatabase::IdentifyConnectifity
   (  std::vector<Nb>& geo
   ,  std::vector<Nb>& newcoord
   ,  const bool& enforce_connectivity
   ,  const bool& ReuseConnectivity
   ,  bool& HasConnectivity
   ,  std::vector<In>& na 
   ,  std::vector<In>& nb 
   ,  std::vector<In>& nc 
   ,  std::vector<In>& oldna 
   ,  std::vector<In>& oldnb 
   ,  std::vector<In>& oldnc 
   ,  const std::vector<In>& aNa 
   ,  const std::vector<In>& aNb 
   ,  const std::vector<In>& aNc
   ,  const In& arNatoms 
   ,  const Nb& rad2deg
   ) const
{
   ZmatBuilder zmatbuilder;
   
   if ( enforce_connectivity ) 
   {
      // the user specified a connectivity and we trust him that he knows what he is doing
      
      std::copy(aNa.begin(), aNa.begin() + arNatoms, &na[0]);                 
      std::copy(aNb.begin(), aNb.begin() + arNatoms, &nb[0]);                 
      std::copy(aNc.begin(), aNc.begin() + arNatoms, &nc[0]);                 
   
      zmatbuilder.xyzgeo(&newcoord[0], arNatoms, &na[0], &nb[0], &nc[0], rad2deg, &geo[0]);
   }
   else if ( ReuseConnectivity && HasConnectivity ) 
   {
      // We reuse the connectivity from the first run
   
      std::copy(oldna.begin(), oldna.begin() + arNatoms, &na[0]);                 
      std::copy(oldnb.begin(), oldnb.begin() + arNatoms, &nb[0]);                 
      std::copy(oldnc.begin(), oldnc.begin() + arNatoms, &nc[0]);                 
   
      zmatbuilder.xyzgeo(&newcoord[0], arNatoms, &na[0], &nb[0], &nc[0], rad2deg, &geo[0]);
   }
   else if ( ReuseConnectivity && !HasConnectivity ) 
   {
      zmatbuilder.xyz2zmat(&newcoord[0], arNatoms, &na[0], &nb[0], &nc[0], rad2deg, &geo[0]);
   
      // Save connectivity for the following molecules
      oldna.resize(arNatoms);
      oldnb.resize(arNatoms);
      oldnc.resize(arNatoms);
   
      std::copy(na.begin(), na.begin() + arNatoms, oldna.begin());              
      std::copy(nb.begin(), nb.begin() + arNatoms, oldnb.begin());
      std::copy(nc.begin(), nc.begin() + arNatoms, oldnc.begin());
   
      HasConnectivity = true;
   }
   else
   {
      zmatbuilder.xyz2zmat(&newcoord[0], arNatoms, &na[0], &nb[0], &nc[0], rad2deg, &geo[0]);
   }
}


/**
 *  @brief Checks if two structures are equivalent up to a threshold 
 **/
bool GeoDatabase::IsEquivalent
   (  const Nb& aThrSim
   ,  const In& aIndex
   ,  const GeoDatabase& aRefGeo
   ,  const In& aIndexInRef
   ) const
{
   Nb diff2 = C_0;

   std::vector<Nb> coordref = aRefGeo.GetCoordinates(aIndexInRef);

   for (In icoord = 0; icoord < mCoordinates[aIndex].size(); icoord++)
   {
      diff2 += std::pow( coordref[icoord] - mCoordinates[aIndex][icoord], 2.0 );
   }

   Nb rmsd = std::sqrt( diff2 / Nb(mCoordinates[aIndex].size()) );

   if (rmsd < aThrSim)
   {
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *  @brief Calculates the RMSD of structure to database 
 **/
std::vector<Nb> GeoDatabase::GetRMSD
   (  const GeoDatabase& aMol
   ,  const In& aIndex
   ) const
{
   vector<Nb> coord = aMol.GetCoordinates(aIndex);

   vector<Nb> vecrmsd;
   vecrmsd.reserve(mCoordinates.size());

   for (In imol = 0; imol < mCoordinates.size(); imol++)
   {
      Nb rmsd = C_0;

      for (In icoord = 0; icoord < mCoordinates[imol].size(); icoord++)
      {
         rmsd += std::pow( coord[icoord] - mCoordinates[imol][icoord], 2.0 );
      }
      rmsd /= Nb(coord.size());
      rmsd = std::sqrt(rmsd);
      
      vecrmsd.push_back(rmsd);
   }

   return vecrmsd;
}

/**
 *  @brief Checks if a similar structure is already in the database 
 **/
bool GeoDatabase::ContainsMolecule
   (  const Nb& aThrSim
   ,  const GeoDatabase& aMol
   ,  const In& aIndex
   ) const
{

   const bool locdbg = false;

   vector<Nb> coord = aMol.GetCoordinates(aIndex);

   for (In imol = 0; imol < mCoordinates.size(); imol++)
   {
      Nb rmsd = C_0;

      for (In icoord = 0; icoord < mCoordinates[imol].size(); icoord++)
      {
         rmsd += std::pow( coord[icoord] - mCoordinates[imol][icoord], 2.0 );
      }
      rmsd /= Nb(coord.size());
      rmsd = std::sqrt(rmsd);

      if (locdbg) Mout << "rmsd " << rmsd << std::endl;

      if (rmsd < aThrSim)
      {
         return true;
      }
   }

   return false;
}

/**
 *  @brief Prints the molecule 
 **/
void GeoDatabase::PrintMolecule
   (  const In& aIndex
   ,  const Nb& aVarVal
   ,  const bool& aDo2XYZ 
   ) const
{
   this->PrintMolecule(Mout, aIndex, aVarVal, aDo2XYZ);
}

/**
 *  @brief Prints the molecule 
 **/
void GeoDatabase::PrintMolecule
   (  std::ostream& aOut
   ,  const In& aIndex
   ,  const Nb& aVarVal
   ,  const bool& aDo2XYZ 
   ) const
{
   const Nb Ang2Bohr = C_1 / C_TANG; 

   In isymb = 0;
   In ndim = mCoordinates[aIndex].size();
   In natoms = mSymbols[aIndex].size();  

   std::vector<Nb> coord = mCoordinates[aIndex];

   switch (mType)
   {
      case XYZ:
      case XYZCENTER:
      {
         for (In icoord = 0; icoord < ndim; icoord++)
         {
            coord[icoord] /= Ang2Bohr;
         }
         break;         
      }
      case ZMAT:
      {
         for (In icoord = 0; icoord < ndim; icoord+=3)
         {
            for (In ixyz = 0; ixyz < 3; ixyz++)
            {
               if (ixyz == 0) 
               {
                  coord[icoord] /= Ang2Bohr;
               }
               else if (ixyz == 2 && coord[icoord] > 180.0)
               {
                  coord[icoord] -= 360.00;
               }
            }
         }

         break;         
      }
      case MINT:
      case DIST:
      case INV:
      {

         if (aDo2XYZ)
         {
            if (mCartCoord.size() > 0)
            {
               ndim = mCartCoord[aIndex].size();
               coord.resize(ndim);
               for (In icoord = 0; icoord < ndim; icoord++)
               {
                  coord[icoord] = mCartCoord[aIndex][icoord] /  Ang2Bohr;
               }
            }
            else
            {
               coord.resize(3*natoms);
            }

         }
         else
         {
            Nb rad2deg = 57.29578e+00;
            const bool do_conversion = false;

            for (In irun = 0; irun <= 4; irun++)
            {

               for (In icoord = 0; icoord < mInternCoord[aIndex].size(); icoord++)
               {

                  In itype = mInternCoord[aIndex][icoord].itype;
                  Nb val = mInternCoord[aIndex][icoord].val;
                  
                  if (itype == 1 && irun != 1) continue;
                  if (itype == -1 && irun != 0) continue;
                  if (itype == 2 && irun != 2) continue;
                  if (itype == 3 && irun != 3) continue;
                  if (itype == 4 && irun != 4) continue;

                  if (do_conversion)
                  {
                     switch (itype)
                     {
                        case 1:
                        case -1:
                        {
                           val /= Ang2Bohr; 
                           break;
                        }
                        case 2:
                        {
                           val *= rad2deg;
                           break;
                        }
                        case 4:
                        {
                           val *= rad2deg;
                           if (val > 180.0) val -= 360.00;
                           break;
                        }
                     }
                  }

                  aOut << "index "   << std::left << std::setw(3) << icoord
                       << " itype "  << itype 
                       << " val "    << ((val > 0.0) ? " " : "")  << val 
                       << " atoms "  << mInternCoord[aIndex][icoord].iconnect << std::endl;
               }

            }

            return;
         }

         break;
      }
      case EIGDIST:
      {
         if (aDo2XYZ)
         {
            if (mCartCoord.size() > 0)
            {
               ndim = mCartCoord[aIndex].size();
               coord.resize(ndim);
               for (In icoord = 0; icoord < ndim; icoord++)
               {
                  coord[icoord] = mCartCoord[aIndex][icoord] /  Ang2Bohr;
               }
            }
            else
            {
               coord.resize(3*natoms);
            }

         }
         else
         {
            for (In icoord = 0; icoord < coord.size(); icoord++)
            {
               aOut << coord[icoord] << std::endl;
            }

            return;
         }
         break;
      }
      case SYMG2:
      case SYMG4:
      {
         std::string gstr = "Gi2";
         if (mType == SYMG4) gstr = "Gi4";

         for (int iat = 0; iat < mInternCoord[aIndex].size(); iat++)
         {
            aOut << "atom " << iat << " : " << gstr << " = " << mInternCoord[aIndex][iat].val << std::endl;
         }

         return;

         break;
      }
   }

   if (mType == ZMAT && aDo2XYZ) 
   {
      // convert to cartesian coordinates for printing

      std::unique_ptr<Nb[]> xyz(new Nb[3*natoms]);

      ZmatBuilder zmatbuilder;

      zmatbuilder.zmat2xyz( xyz.get()
                          , natoms
                          , &mZmatNa[aIndex][0]
                          , &mZmatNb[aIndex][0]
                          , &mZmatNc[aIndex][0]
                          , rad2deg
                          , &coord[0] 
                          );

      for (In iat = 0; iat < natoms; iat++)
      {
          coord[iat*3 + 0] = xyz[iat*3 + 0] ; 
          coord[iat*3 + 1] = xyz[iat*3 + 1] ; 
          coord[iat*3 + 2] = xyz[iat*3 + 2] ; 
      }

   }
 
   aOut << natoms << std::endl;
   aOut << "Energy = " <<  std::fixed << std::showpoint << std::setprecision(14) <<  mEnergies[aIndex]  << std::endl;
   for (In icoord = 0; icoord < ndim; icoord += 3)
   {
      aOut << mSymbols[aIndex][isymb++] << "   ";
      for (In ixyz = 0; ixyz < 3; ixyz++)
      {
         Nb xyz = coord[icoord + ixyz];

         ios::fmtflags f( aOut.flags() );

         aOut << (xyz >= 0 ? " ":"") << std::fixed << std::showpoint << std::setprecision(12) << xyz << "   "; 

         aOut.flags(f);
      }
      aOut << std::endl;
   }

}

/**
 *  @brief Returns the atomic masses used
 *
 **/
std::vector<Nb> GeoDatabase::GetAtomicMasses
   (  const In& aIndex
   ) const 
{
   std::vector<Nb> amass;
   In natoms = mSymbols[aIndex].size();
   for (In iat = 0; iat < natoms; iat++)
   {
      std::string sym = mSymbols[aIndex][iat];
      std::transform(sym.begin(), sym.end(), sym.begin(), tolower);
      amass.push_back(masses.at(sym));
   }
   return amass;
}

/**
 *  @brief Returns a vector of the masses for each coordinate 
 *
 **/
std::vector<Nb> GeoDatabase::GetCoordinateMasses
   (  const In& aIndex
   ) const
{
   In natoms = mSymbols[aIndex].size();
   std::vector<Nb> qmass;
   for (In iat = 0; iat < natoms; iat++)
   {
      std::string sym = mSymbols[aIndex][iat];
      std::transform(sym.begin(), sym.end(), sym.begin(), tolower);
      for (In j = 0; j < 3; j++)
      {
         qmass.push_back(masses.at(sym));
      }
   }
   return qmass;
}

/**
 *  @brief Defines Internal coordinates based on user input 
 *
 **/
void GeoDatabase::DefineInternals
   (  std::vector<InternalCoord>& arInternCoord 
   ,  const std::vector<InternalCoord>& arIcoordDef
   ,  std::vector<Nb>& arCoord
   ,  std::vector<Nb>& arCartCoord
   )
{
   // Use provided definition of internal coordinates
   int ncart   = arCartCoord.size();
   int nintern = arIcoordDef.size();
   
   BmatBuilder bmatbuilder;
   std::unique_ptr<Nb[]> bmat( new Nb[nintern * ncart] );
   
   // init B Matrix
   for (In idx = 0; idx < nintern*ncart; idx++) {bmat[idx] = 0.0;}
   
   // construct B Matrix
 
   // Check for torsion angle
   // and read the reference geometry to define it relatively
   std::vector<Nb> ref_coord = GetRefCoords(arIcoordDef, gSystemDefs);

   for (In intc = 0; intc < nintern; intc++)
   {
      InternalCoord newicoord;
   
      In itype = arIcoordDef[intc].itype;
      Nb dval  = arIcoordDef[intc].val;
      std::vector<In> iconnect = arIcoordDef[intc].iconnect; 
      In iat = arIcoordDef[intc].iatom;
   
      bmatbuilder.GenerateRow( arCoord, ref_coord, intc, bmat.get(), dval, itype
                             , iconnect, ncart);
   
      // Add new internal coordinate 
      newicoord.itype    = itype;
      newicoord.val      = dval;
      newicoord.iconnect = iconnect;
      newicoord.iatom    = iat;
   
      arInternCoord.push_back(newicoord);
   }
}

/**
 *  @brief Converts from Z-matrix representation to internal coordinates 
 *
 **/
void GeoDatabase::Zmat2Int
   (  vector<Nb>& coord
   ,  Nb* geo
   ,  const In& arNatoms
   ,  const In* na
   ,  const In* nb
   ,  const In* nc
   ,  std::vector<InternalCoord>&  interncoord
   ,  std::vector<In>& itype
   )
{
   In ndim = 3*arNatoms;

   In nstretch = arNatoms - 1;
   In nbend    = arNatoms - 2; 
   In ndihed   = arNatoms - 3;
   In nintern  = nstretch + nbend + ndihed;  

   BmatBuilder bmatbuilder;
   std::unique_ptr<Nb[]> bmat( new Nb[nintern * ndim] );

   InternalCoord newicoord;

   // extract from Z-Matrix internal coordinates (stretch, bend, torsion)
   for (In iat = 0; iat < arNatoms; iat++)
   {
      newicoord.iatom = iat;  // needed to extract connectivity later

      if (iat > 0) 
      {
         // add stretch
         newicoord.val      = geo[iat*3 + 0];
         newicoord.itype    = 1;
         newicoord.iconnect = std::vector<In>{na[iat], iat} ;         

         interncoord.push_back(newicoord);
      }

      if (iat > 1)
      {
         // add bend
         newicoord.val      = geo[iat*3 + 1];
         newicoord.itype    = 2;
         newicoord.iconnect = std::vector<In>{na[iat], nb[iat], iat} ;         

         interncoord.push_back(newicoord);
      }

      if (iat > 2)
      {
         // add dihedral
         newicoord.val      = geo[iat*3 + 2];
         newicoord.itype    = 4;
         newicoord.iconnect = std::vector<In>{na[iat], nb[iat], nc[iat], iat} ;         

         interncoord.push_back(newicoord);
      }

   }

   // init B Matrix
   for (In idx = 0; idx < nintern*ndim; idx++)
   {
      bmat[idx] = 0.0;
   }

   // Check for torsion angle
   // and read the reference geometry to define it relatively
   std::vector<Nb> ref_coord = GetRefCoords(interncoord, gSystemDefs);

   // construct B Matrix
   for (In intc = 0; intc < nintern; intc++)
   {
      In itype = interncoord[intc].itype;
      Nb dval = interncoord[intc].val;

      bmatbuilder.GenerateRow( coord, ref_coord, intc, bmat.get(), dval, itype
                             , interncoord[intc].iconnect, ndim);

      // We recalculated value (In case of dihedrals it can change so that it is closer to already calculated once)
      interncoord[intc].val = dval;
   }


}


/**
 *  @brief Return Wilson B matrix 
 *
 * */
std::unique_ptr<Nb[]> GeoDatabase::GetBMatrix
   (  const In& aIndex
   ) 
{
   return this->GetBMatrix(aIndex,mCartCoord[aIndex]); 
}

/**
 *  @brief Return Wilson B matrix 
 *
 * */
std::unique_ptr<Nb[]> GeoDatabase::GetBMatrix
   (  const In& aIndex
   ,  const vector<Nb>& cartcoord
   ) 
{

   if (  mType == MINT 
      || mType == DIST 
      || mType == INV
      || mType == SYMG2
      || mType == SYMG4
      )
   {
      int ncart   = cartcoord.size();
      int nintern = mInternCoord[aIndex].size();

      BmatBuilder bmatbuilder;
      std::unique_ptr<Nb[]> bmat( new Nb[nintern * ncart] );

      // init B Matrix
      for (int idx = 0; idx < ncart*nintern; idx++)
      {
         bmat[idx] = 0.0;
      }

      Nb* bptr = bmat.get();

      // Check for torsion angle
      // and read the reference geometry to define it relatively
      std::vector<Nb> ref_coord = GetRefCoords(mInternCoord[aIndex], gSystemDefs);

      // construct B Matrix
      for (int intc = 0; intc < nintern; intc++)
      {
         In itype = mInternCoord[aIndex][intc].itype;
         Nb dval = mInternCoord[aIndex][intc].val;

         bmatbuilder.GenerateRow( cartcoord, ref_coord, intc, bptr, dval, itype
                                , mInternCoord[aIndex][intc].iconnect, ncart);

         mInternCoord[aIndex][intc].val = dval;
      }

      return bmat;
   }
   else
   {
      MIDASERROR("No B matrix available for this type of coordinates!");
   }

   std::unique_ptr<Nb[]> bmat( nullptr );

   return bmat;
}

/**
 *  @brief Save connectivity of atoms on file 
 *
 *  Purpose: Save connectivity of atoms for specific molecule on file 
 *
 *  @param aFileName specifies the filename   
 *
 *  @param aImol specifies the index of the molecule to be saved
 *
 * */
void GeoDatabase::SaveConnectivity
   (  const std::string& aFileName 
   ,  const In& aImol
   ) const
{

   switch (mType)
   {
      case MINT:  
      {
         ofstream out;
         out.open (aFileName);

         for (In icoord = 0; mInternCoord[aImol].size(); icoord++)
         {
           In itype = mInternCoord[aImol][icoord].itype;

           out << "itype "   << itype 
               << " atoms "  << mInternCoord[aImol][icoord].iconnect << std::endl;
         } 
         out.close();  
         break; 
      }
      default:
      {
         MIDASERROR("GeoDatabase::SaveConnectivty:> I have no connectivity to dump to file!");
         break;
      }
   }

}

/**
 *  @brief Gets connectivity of atoms for specific molecule 
 *
 *  Purpose: Gets connectivity of atoms for specific molecule 
 *
 *  @param aNa tells which atoms are related via a bond i and aNa[i]
 *
 *  @param aNb tells which atoms are related via an angle i, aNa[i] and aNb[i] 
 *
 *  @param aNc tells which atoms are related via a dihedral  angle i, aNa[i], aNb[i]  and aNc[i]
 *
 *  @param aImol specifies for which molecule in the database the connectivity should be returned
 * */
void GeoDatabase::GetConnectivity
   (  std::vector<In>& aNa
   ,  std::vector<In>& aNb
   ,  std::vector<In>& aNc
   ,  const In& aImol
   ) const
{
   switch (mType)
   {
      case MINT: 
      case DIST:
      case INV:
      case EIGDIST:   
      {

         aNa.resize(mNatoms, 0);
         aNb.resize(mNatoms, 0);
         aNc.resize(mNatoms, 0);

         for (In icoord = 0; icoord < mInternCoord[aImol].size(); icoord++)
         {

            In itype = mInternCoord[aImol][icoord].itype;
            In iatom = mInternCoord[aImol][icoord].iatom;    

            switch (itype)
            {
               case 1:
               case -1:
               {
                  aNa[iatom] = mInternCoord[aImol][icoord].iconnect[0];
                  break;
               }
               case 2:
               {
                  aNa[iatom] = mInternCoord[aImol][icoord].iconnect[0];
                  aNb[iatom] = mInternCoord[aImol][icoord].iconnect[1];
                  break;
               }
               case 4:
               {
                  aNa[iatom] = mInternCoord[aImol][icoord].iconnect[0];
                  aNb[iatom] = mInternCoord[aImol][icoord].iconnect[1];
                  aNc[iatom] = mInternCoord[aImol][icoord].iconnect[2];
                  break;
               }
            }

         }
 
         break; 
      }
      default:
      {
         MIDASERROR("GeoDatabase::GetConnectivty:> I have no connectivity to extract!");
         break;
      }
   }
   
}

/**
 *  @brief Checks if Gradient information is available for a particular molecule
 *
 *  @param aImol is the index of the molecule in the database 
 *
 **/
bool GeoDatabase::HasGradientInfo
   (  const In& aImol
   ) const
{
   bool is_not_same = mImapGrad[aImol] != NOT_INIT;
   return (is_not_same);
}

/**
 *  @brief Checks if Hessian information is available for a particular molecule
 *
 *  @param aImol is the index of the molecule in the database 
 *
 **/
bool GeoDatabase::HasHessianInfo
   (  const In& aImol
   ) const 
{
   bool is_not_same = mImapHess[aImol] != NOT_INIT;
   return (is_not_same);
}

/**
 *  @brief Converts to a tuple of Property and MLDescriptor 
 *
 *  Purpose: Converts to a tuple of Property and molecular descriptor, which can be used
 *           in ML algorithms like GPR. 
 *
 **/
std::tuple<std::vector<MLDescriptor<Nb>>,std::vector<Nb> > GeoDatabase::Convert2Descriptors() const
{
   In nmol = mCoordinates.size();

   std::vector<MLDescriptor<Nb>> TrainSetX; 
   std::vector<Nb> TrainSetY;
     
   for (In imol = 0; imol < nmol; ++imol)
   {
      MLDescriptor<Nb> mol(mCoordinates[imol],MLDescriptor<Nb>::SCALAR);
      TrainSetX.push_back(mol);
      TrainSetY.push_back(mEnergies[imol]);

      if (this->HasGradientInfo(imol))
      {
         In nelem = mCoordinates[imol].size();

         for (In ielem = 0; ielem < nelem; ++ielem)
         {
            mol.DefineAsGradElem(ielem);
            TrainSetX.push_back(mol);
            TrainSetY.push_back(mGradient[mImapGrad[imol]][ielem]);
         }
      }

      if (this->HasHessianInfo(imol))
      {
         In nelem = mCoordinates[imol].size();

         for (In ielem = 0; ielem < nelem; ++ielem)
         {
            //for (In jelem = 0; jelem < nelem; ++jelem)
            for (In jelem = 0; jelem <= ielem; ++jelem)
            {
               mol.DefineAsHessElem(ielem,jelem);
               TrainSetX.push_back(mol);
               TrainSetY.push_back(mHessian[mImapHess[imol]][ielem*nelem + jelem]);
            }
         }
         
      }
   } 
   return std::make_tuple(TrainSetX, TrainSetY);
}  

