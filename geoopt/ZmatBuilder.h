/**
************************************************************************
* 
* @file                ZmatBuilder.h 
*
* Created:             28-05-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Implementing a builder for a Z-matrix 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ZMATBUILDER_H
#define ZMATBUILDER_H

#include <numeric>
#include <vector> 
using std::vector;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"

class ZmatBuilder
{
  private:

   void traf2xyz
      (  Nb* d
      ,  const Nb* c
      ,  const Nb* b
      ,  const Nb* a
      ,  const Nb& bond
      ,  const Nb& angle
      ,  const Nb& torsion
      ) const;


   void GetAngle
      (  const Nb* xyz
      ,  const int& iat
      ,  const int& jat
      ,  const int& kat
      ,  Nb& angle
      ) const;

   void DetermineAngle 
      (  Nb& a1
      ,  Nb& a2
      ,  Nb& b1
      ,  Nb& b2
      ,  Nb& rcos
      ) const;

   void GetDihedral
      (  const Nb* xyz
      ,  const int& iat
      ,  const int& jat
      ,  const int& kat
      ,  const int& lat
      ,  Nb& angle
      ) const;



  public:   
   ZmatBuilder() = default;      ///< Constructor 

   void xyzgeo
      (  const Nb* xyz
      ,  const int natoms
      ,  int* na
      ,  int* nb
      ,  int* nc
      ,  const Nb rad2deg
      ,  Nb* geo
      ) const;

   void zmat2xyz
      (  Nb* xyz
      ,  const int& natoms
      ,  const int* na
      ,  const int* nb
      ,  const int* nc
      ,  const Nb& rad2deg
      ,  const Nb* geo
      ) const;

   void xyz2zmat
      (  const Nb* xyz
      ,  const int& natoms
      ,  int* na
      ,  int* nb
      ,  int* nc
      ,  const Nb& rad2deg
      ,  Nb* geo
      ) const;
   
};



#endif
