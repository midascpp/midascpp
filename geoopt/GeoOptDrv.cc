/**
************************************************************************
* 
* @file                GeoOptDrv.cc
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Driver for MidasCPP geometry optimizations 
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// My headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "input/Input.h"
#include "input/GeoOptCalcDef.h"
#include "geoopt/GeoOpt.h"

/**
* Run optimization
* */
void GeoOptDrv()
{

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Begin Geometry Optimization ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
  Mout << "\n\n";


  for (In i_calc =0; i_calc < gGeoOptCalcDef.size(); i_calc++) 
  {  

/*
***********   Create GeoOpt object  **************************
*/
    Mout << "\n\n\n Do GeoOpt:  " << gGeoOptCalcDef[i_calc].GetName() << "\n" << endl;

    GeoOpt geoopt(&gGeoOptCalcDef[i_calc]);

/*
***********   Start Geo opt  **************************
*/

    Timer time_it;

    geoopt.Optimize();  

    string s_time = " CPU time used in geometry optimization :";
    time_it.CpuOut(cout,s_time);

  }

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Geometry optimization done ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
}

