#include <memory>
#include "geoopt/GenerFuncs.h"
#include"lapack_interface/math_wrappers.h"

/**
 *    @brief Calculates Pseudo Inverse 
 *
 *    Purpose: Calculates the Pseudo Inverse for column major "lapack-style"-matrices
 *
 *   @param apmat      Pointer to the matrix with arNcart x arNintern elements (will be overwritten by inverse)
 *   @param arNcart    Dimension of the cartesian coordinates     
 *   @param arNintern  Dimension of the internal coordinates     
 **/ 
void PseudoInvert
   (  Nb* apmat
   ,  const int& arNcart
   ,  const int& arNintern
   ) 
{

   Nb one  = 1.0;
   Nb zero = 0.0;

   int nmin = std::min(arNcart, arNintern);    

   std::unique_ptr<Nb[]> sval(new Nb[nmin]);
   std::unique_ptr<Nb[]> sinv(new Nb[nmin]);
   
   std::unique_ptr<Nb[]> tmp(new Nb[arNcart*arNintern]);
   std::copy(apmat, apmat + arNcart*arNintern, tmp.get());
   
   char jobu  = 'A';
   char jobvt = 'A';
   int m = arNcart;
   int n = arNintern;
   int info = 0;
   int lwork = -1; 
   
   // first run to get proper lwork
   Nb dwork = 1.0;
   Nb dum = 1.0;
   midas::lapack_interface::gesvd(&jobu, &jobvt, &m, &n, &dum, &m
                                 , &dum, &dum, &m, &dum, &n
                                 , &dwork, &lwork, &info );
   lwork = int(dwork);
   
   std::unique_ptr<Nb[]> u(new Nb[m*m]);
   std::unique_ptr<Nb[]> vt(new Nb[n*n]);
   std::unique_ptr<Nb[]> work(new Nb[lwork]);
   
   midas::lapack_interface::gesvd(&jobu, &jobvt, &m, &n, tmp.get(), &m
                                 , sval.get(), u.get(), &m, vt.get(), &n
                                 , work.get(), &lwork, &info );
   
   // form Inverse of S  
   Nb rcond = 1.e-12;
   Nb sig;
   for (int i = 0; i < nmin; ++i) 
   {
      sig = sval[i];
      sinv[i] = ((sig > rcond) ? 1.0 / sig  : 0.0);
   }

   //---------------------------------------------------------------------------+
   //  B+ = V S+ U^T
   //---------------------------------------------------------------------------+
   
   // 1.) X = V S+
   std::unique_ptr<Nb[]> xmat(new Nb[n*m]);
   std::unique_ptr<Nb[]> mmat(new Nb[n*m]);

   // fill n x m matrix
   for (int j = 0; j < m; ++j)
   {
      for (int i = 0; i < n; ++i)
      {
         if ( i == j ) 
         {
            mmat[j*n + i] = sinv[i];
         }
         else
         {
            mmat[j*n + i] = 0.0;
         }
      }
   }


   char nc = 'N';
   char nt = 'T';

   midas::lapack_interface::gemm( &nt, &nc, &n, &m, &n, &one 
                                , vt.get(), &n, mmat.get(), &n 
                                , &zero, xmat.get(), &n );   


   // 2.) B+ = X U^T
   midas::lapack_interface::gemm( &nc, &nt, &n, &m, &m, &one 
                                , xmat.get(), &n, u.get(), &m 
                                , &zero, apmat, &n );   


}
