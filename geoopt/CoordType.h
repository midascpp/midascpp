
#ifndef COORD_TYPE
#define COORD_TYPE
namespace geo_coordtype
{
   enum CoordType { XYZ, ZMAT, XYZCENTER, MINT, DIST, EIGDIST, INV, SYMG2, SYMG4 };
}

#endif
