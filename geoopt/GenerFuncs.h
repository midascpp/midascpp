/**
************************************************************************
* 
* @file                GenerFuncs.h 
*
* Created:             26-08-2020
*
* Author:              Denis Artiukhin (artiukhin@chem.au.dk) 
*
* Short Description:   Functions factored out of the BMatBuilder
*                      for a better organization
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERFUNCS_INCLUDED
#define GENERFUNCS_INCLUDED

#include "inc_gen/TypeDefs.h"

void PseudoInvert
   (  Nb* mat
   ,  const int& ncart
   ,  const int& nintern
   );


#endif /* GENERFUNCS_INCLUDED */
