/**
************************************************************************
* 
* @file                GeoOpt.h 
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Implementing a geometry optimizer
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GEOOPT_H
#define GEOOPT_H

void GeoOptDrv();

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "input/EcorCalcDef.h"
#include "mlearn/GauPro.h"
#include "mlearn/mlutil.h" 

#include "geoopt/GeoDatabase.h"
#include "geoopt/CoordType.h"
#include "geoopt/geoutil.h"

using namespace geo_coordtype;

class GeoOpt
{
  private:

   string mDatabase;
   string mCoordFile;

   bool mDoSample   = false;
   bool mDoEnergy   = false;
   bool mDoGrad     = false;
   bool mDoHessian  = false;
   bool mDoOpt      = false;

   bool mAddGrad = false;
   string mFileAddGrad;

   bool mDoOnlyIcoord = false;

   bool mOnlySimCheck = false;

   CoordType mCoordType;

   Nb mDamping = 1.0;

   bool mSaveCovar = false;
   bool mReadCovar = false;

   string mFileCovarSave;
   string mFileCovarRead;

   string mKernelType;

   //****************** Actual methods **************************

   void FrequencyAnalysis(GauPro<Nb>&, GeoDatabase&);

   void WriteGradient
      (  ostream& out
      ,  vector<vector<Nb>>& coord
      ,  vector<string>& symbols
      ,  vector<vector<Nb>>& grad
      ,  vector<Nb>& energies 
      );

   void WriteGradient
      (  ostream& out
      ,  vector<Nb>& coord
      ,  vector<string>& symbols
      ,  vector<Nb>& grad
      ,  Nb energy 
      );

   // ugly quick way to use masses
   std::map<std::string, Nb> masses =   {{"h"  , 1.00797}  , {"he" , 4.0026}  , {"li" ,  6.939}   , {"be" , 9.0122}  ,  {"b"  , 10.811}  ,
                                         {"c"  , 12.01115} , {"n"  , 14.0067} , {"o"  , 15.9994}  , {"f"  , 18.9984} ,  {"ne" , 20.183}  ,
                                         {"na" , 22.9898}  , {"mg" , 24.312}  , {"al" , 26.9815}  , {"si" , 28.086}  ,  {"p"  , 30.9738} ,
                                         {"s"  , 2.064}    , {"cl" , 35.453}  , {"ar" , 39.948}   , {"k"  , 39.102}  ,  {"ca" , 40.08}   ,
                                         {"sc" , 44.956}   , {"ti" , 47.90}   , {"v"  , 50.942}   , {"cr" , 51.996}  ,  {"mn" , 54.9380} , 
                                         {"fe" , 55.85}    , {"co" , 58.9332} , {"ni" , 58.71}    , {"cu" , 63.54}   ,  {"zn" , 65.37}   ,
                                         {"ga" , 69.72}    , {"ge" , 72.59}   , {"as" , 74.9216}  , {"se" , 78.96}   ,  {"br" , 79.909}  ,
                                         {"kr" , 83.80}    , {"rb" , 85.47}   , {"sr" , 87.62}    , {"y"  , 88.905}  ,  {"zr" , 91.22}   ,
                                         {"nb" , 92.906}   , {"mo" , 95.94}   , {"tc" , 99.0}     , {"ru" , 101.07}  ,  {"rh" , 102.905} ,
                                         {"pd" , 106.4}    , {"ag" , 107.87}  , {"cd" , 112.40}   , {"in" , 114.82}  ,  {"sn" , 118.69}  ,
                                         {"sb" , 121.75}   , {"te" , 127.60}  , {"i"  , 126.904}  , {"xe" , 131.30}  ,  {"cs" , 132.905} ,
                                         {"ba" , 137.33}   , {"la" , 138.91}  , {"ce" , 140.115}  , {"pr" , 140.908} ,  {"nd" , 144.24}  ,
                                         {"pm" , 146.92}   , {"sm" , 150.36}  , {"eu" , 151.965}  , {"gd" , 157.25}  ,  {"tb" , 158.925} ,
                                         {"dy" , 162.50}   , {"ho" , 164.93}  , {"er" , 167.26}   , {"tm" , 168.93}  ,  {"yb" , 173.04}  ,
                                         {"lu" , 174.97}   , {"hf" , 178.49}  , {"ta" , 180.95}   , {"w"  , 183.85}  ,  {"re" , 186.21}  ,
                                         {"os" , 190.2}    , {"ir" , 192.22}  , {"pt" , 195.08}   , {"au" , 196.97}  ,  {"hg" , 200.59}  ,
                                         {"tl" , 204.38}   , {"pb" , 207.2}   , {"bi" , 208.980}  , {"po" , 208.98}  ,  {"at" , 209.99}  ,
                                         {"rn" , 222.02}   , {"fr" , 223.02}  , {"ra" , 226.03}   , {"ac" , 227.03}  ,  {"th" , 232.04}  ,
                                         {"pa" , 231.04}   , {"u"  , 238.03}  , {"np" , 37.0482}  , {"pu" , 244}     ,  {"am" , 243}     ,
                                         {"cm" , 247}      , {"bk" , 247}     , {"cf" , 251}      , {"es" , 252}     ,  {"fm" , 257}     ,
                                         {"md" , 258}      , {"no" , 259}     , {"lr" , 262} };


  public:
   GeoOpt(GeoOptCalcDef* apCalcDef);      ///< Constructor 

   void Optimize();                            

   void CreateSample();

};



#endif
