/**
************************************************************************
* 
* @file                GeoDatabase.h 
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Implementing 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GEODB_H
#define GEODB_H

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "input/Input.h"
#include "geoopt/BmatBuilder.h"
#include "geoopt/ZmatBuilder.h"
#include "geoopt/geoutil.h"

#include "mlearn/MLDescriptor.h"

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

struct GeoElement
{
   
};

class GeoDatabase
{

  private:

   // Used shortcuts and constants
   const In NOT_INIT = -99;

   //Nb rad2deg = 57.29578e+00;
   const Nb rad2deg = 1.0;

   char nn = 'N';
   char nt = 'T';
   In nvec = 1 ;
   Nb one  = 1.0;
   Nb zero = 0.0;

   // Parameters 
   bool mScreen = true;
   Nb   mTolrmsd = 1e-3;


   // begin main information needen
   CoordType mType;

   std::vector<std::vector<Nb>> mCoordinates;         ///< Main storage for coordinate information
   std::vector<std::vector<std::string>> mSymbols;    ///< Storage for elment symbols
   std::vector<Nb> mEnergies;                         ///< Storage for energies

   std::vector<std::vector<Nb>> mGradient;            ///< Optional space for a Gradient
   std::vector<std::vector<Nb>> mHessian;             ///< Optional space for a Hessian

   std::vector<In> mImapGrad;                         ///< Maps index of coordinates to index in mGradient
   std::vector<In> mImapHess;                         ///< Maps index of coordinates to index in mHessian

   // vector holding information which atoms define the internal coordinate 
   // and type of the coordinate (if internal coordinates are used)
   std::vector<std::vector<InternalCoord>> mInternCoord;
   std::vector<std::vector<Nb>>            mCartCoord;

   // arrays which hold connectivity information if Z-matrix representation is used
   std::vector<std::vector<In>> mZmatNa;
   std::vector<std::vector<In>> mZmatNb;
   std::vector<std::vector<In>> mZmatNc;

   In mNatoms = 0;
   // end begin main information needen

   // ugly quick way to use masses
   std::map<std::string, Nb> masses =   {{"h"  , 1.00797}  , {"he" , 4.0026}  , {"li" ,  6.939}   , {"be" , 9.0122}  ,  {"b"  , 10.811}  ,
                                         {"c"  , 12.01115} , {"n"  , 14.0067} , {"o"  , 15.9994}  , {"f"  , 18.9984} ,  {"ne" , 20.183}  ,
                                         {"na" , 22.9898}  , {"mg" , 24.312}  , {"al" , 26.9815}  , {"si" , 28.086}  ,  {"p"  , 30.9738} ,
                                         {"s"  , 2.064}    , {"cl" , 35.453}  , {"ar" , 39.948}   , {"k"  , 39.102}  ,  {"ca" , 40.08}   ,
                                         {"sc" , 44.956}   , {"ti" , 47.90}   , {"v"  , 50.942}   , {"cr" , 51.996}  ,  {"mn" , 54.9380} , 
                                         {"fe" , 55.85}    , {"co" , 58.9332} , {"ni" , 58.71}    , {"cu" , 63.54}   ,  {"zn" , 65.37}   ,
                                         {"ga" , 69.72}    , {"ge" , 72.59}   , {"as" , 74.9216}  , {"se" , 78.96}   ,  {"br" , 79.909}  ,
                                         {"kr" , 83.80}    , {"rb" , 85.47}   , {"sr" , 87.62}    , {"y"  , 88.905}  ,  {"zr" , 91.22}   ,
                                         {"nb" , 92.906}   , {"mo" , 95.94}   , {"tc" , 99.0}     , {"ru" , 101.07}  ,  {"rh" , 102.905} ,
                                         {"pd" , 106.4}    , {"ag" , 107.87}  , {"cd" , 112.40}   , {"in" , 114.82}  ,  {"sn" , 118.69}  ,
                                         {"sb" , 121.75}   , {"te" , 127.60}  , {"i"  , 126.904}  , {"xe" , 131.30}  ,  {"cs" , 132.905} ,
                                         {"ba" , 137.33}   , {"la" , 138.91}  , {"ce" , 140.115}  , {"pr" , 140.908} ,  {"nd" , 144.24}  ,
                                         {"pm" , 146.92}   , {"sm" , 150.36}  , {"eu" , 151.965}  , {"gd" , 157.25}  ,  {"tb" , 158.925} ,
                                         {"dy" , 162.50}   , {"ho" , 164.93}  , {"er" , 167.26}   , {"tm" , 168.93}  ,  {"yb" , 173.04}  ,
                                         {"lu" , 174.97}   , {"hf" , 178.49}  , {"ta" , 180.95}   , {"w"  , 183.85}  ,  {"re" , 186.21}  ,
                                         {"os" , 190.2}    , {"ir" , 192.22}  , {"pt" , 195.08}   , {"au" , 196.97}  ,  {"hg" , 200.59}  ,
                                         {"tl" , 204.38}   , {"pb" , 207.2}   , {"bi" , 208.980}  , {"po" , 208.98}  ,  {"at" , 209.99}  ,
                                         {"rn" , 222.02}   , {"fr" , 223.02}  , {"ra" , 226.03}   , {"ac" , 227.03}  ,  {"th" , 232.04}  ,
                                         {"pa" , 231.04}   , {"u"  , 238.03}  , {"np" , 37.0482}  , {"pu" , 244}     ,  {"am" , 243}     ,
                                         {"cm" , 247}      , {"bk" , 247}     , {"cf" , 251}      , {"es" , 252}     ,  {"fm" , 257}     ,
                                         {"md" , 258}      , {"no" , 259}     , {"lr" , 262} };


   // and the same for the charges
   std::map<std::string, In> charges = {{"h"  , 1 }  , {"he" , 2 }    , {"li" , 3  } , {"be" , 4 } ,  {"b"  , 5  }   ,
                                        {"c"  , 6 }  , {"n"  , 7 }    , {"o"  , 8  } , {"f"  , 9 } ,  {"ne" , 10 }   ,
                                        {"na" , 11}  , {"mg" , 12}    , {"al" , 13 } , {"si" , 14} ,  {"p"  , 15 }   ,
                                        {"s"  , 16}  , {"cl" , 17}    , {"ar" , 18 } , {"k"  , 19} ,  {"ca" , 20 }   ,
                                        {"sc" , 21}  , {"ti" , 22}    , {"v"  , 23 } , {"cr" , 24} ,  {"mn" , 25 }   ,
                                        {"fe" , 26}  , {"co" , 27}    , {"ni" , 28 } , {"cu" , 29} ,  {"zn" , 30 }   ,
                                        {"ga" , 31}  , {"ge" , 32}    , {"as" , 33 } , {"se" , 34} ,  {"br" , 35 }   ,
                                        {"kr" , 36}  , {"rb" , 37}    , {"sr" , 38 } , {"y"  , 39} ,  {"zr" , 40 }   ,
                                        {"nb" , 41}  , {"mo" , 42}    , {"tc" , 43 } , {"ru" , 44} ,  {"rh" , 45 }   ,
                                        {"pd" , 46}  , {"ag" , 47}    , {"cd" , 48 } , {"in" , 49} ,  {"sn" , 50 }   ,
                                        {"sb" , 51}  , {"te" , 52}    , {"i"  , 53 } , {"xe" , 54} ,  {"cs" , 55 }   ,
                                        {"ba" , 56}  , {"la" , 57}    , {"ce" , 58 } , {"pr" , 59} ,  {"nd" , 60 }   ,
                                        {"pm" , 61}  , {"sm" , 62}    , {"eu" , 63 } , {"gd" , 64} ,  {"tb" , 65 }   ,
                                        {"dy" , 66}  , {"ho" , 67}    , {"er" , 68 } , {"tm" , 69} ,  {"yb" , 70 }   ,
                                        {"lu" , 71}  , {"hf" , 72}    , {"ta" , 73 } , {"w"  , 74} ,  {"re" , 75 }   ,
                                        {"os" , 76}  , {"ir" , 77}    , {"pt" , 78 } , {"au" , 79} ,  {"hg" , 80 }   ,
                                        {"tl" , 81}  , {"pb" , 82}    , {"bi" , 83 } , {"po" , 84} ,  {"at" , 85 }   ,
                                        {"rn" , 86}  , {"fr" , 87}    , {"ra" , 88 } , {"ac" , 89} ,  {"th" , 90 }   ,
                                        {"pa" , 91}  , {"u"  , 92}    , {"np" , 93 } , {"pu" , 94} ,  {"am" , 95 }   ,
                                        {"cm" , 96}  , {"bk" , 97}    , {"cf" , 98 } , {"es" , 99} ,  {"fm" , 100}   ,
                                        {"md" , 101} , {"no" , 102}   , {"lr" , 103} };



   void Zmat2Int
      (  vector<Nb>& coord
      ,  Nb* geo
      ,  const In& natoms
      ,  const In* na
      ,  const In* nb
      ,  const In* nc
      ,  std::vector<InternalCoord>& intern_coord
      ,  std::vector<In>& itype
      );

   void DefineInternals
      (  std::vector<InternalCoord>& arInternCoord 
      ,  const std::vector<InternalCoord>& arIcoordDef
      ,  std::vector<Nb>& arCoord
      ,  std::vector<Nb>& arCartCoord
      );


   void IdentifyConnectifity
      (  std::vector<Nb>& geo
      ,  std::vector<Nb>& newcoord
      ,  const bool& enforce_connectivity
      ,  const bool& ReuseConnectivity
      ,  bool& HasConnectivity
      ,  std::vector<In>& na
      ,  std::vector<In>& nb
      ,  std::vector<In>& nc
      ,  std::vector<In>& oldna
      ,  std::vector<In>& oldnb
      ,  std::vector<In>& oldnc
      ,  const std::vector<In>& aNa
      ,  const std::vector<In>& aNb
      ,  const std::vector<In>& aNc
      ,  const In& natoms
      ,  const Nb& rad2deg
      ) const;


    std::vector<Nb> ReadGradient
      (  std::ifstream&
      ,  const In&
      );

    std::vector<Nb> ReadHessian
      (  std::ifstream&
      ,  const In&
      ,  const In&
      );

    void SetupDistances
      (  std::vector<Nb>& 
      ,  std::vector<Nb>& 
      ,  std::vector<InternalCoord>& 
      ,  const In& 
      );

    void CreateSymmetryFunctions
      (  std::vector<Nb>& 
      ,  std::vector<Nb>& 
      ,  std::vector<InternalCoord>& 
      ,  const In& 
      );

  public:

   GeoDatabase
      (  const CoordType& aCoordType 
      ) : mType(aCoordType)
   {
   }

   GeoDatabase
      (  const CoordType& aCoordType
      ,  const std::string& aFileName
      ) : mType(aCoordType)
   {  
      ReadCoordinates(aFileName);
   }

   GeoDatabase
      (  const CoordType& aCoordType
      ,  const std::string& aFileName
      ,  const std::vector<In>& aNa 
      ,  const std::vector<In>& aNb 
      ,  const std::vector<In>& aNc 
      ) : mType(aCoordType) 
   {  
      ReadCoordinates(aFileName, true, aNa, aNb, aNc);
   }

   GeoDatabase
      (  const CoordType& aCoordType
      ,  const std::string& aFileName
      ,  const bool& use_idef
      ,  const std::vector<InternalCoord>& icoorddef
      ) : mType(aCoordType) 
   {  
      ReadCoordinates(aFileName, use_idef, icoorddef);
   }

   GeoDatabase
      (  const CoordType& aCoordType
      ,  const std::string& aFileName
      ,  const std::vector<In>& aNa 
      ,  const std::vector<In>& aNb 
      ,  const std::vector<In>& aNc
      ,  const bool use_idef
      ,  const std::vector<InternalCoord>& icoorddef
      ,  const Nb& aTolrmsd
      ) : mTolrmsd(aTolrmsd)
        , mType(aCoordType)
   {  
      ReadCoordinates(aFileName, true, aNa, aNb, aNc, use_idef, icoorddef);
   }

   GeoDatabase
      (  const CoordType& aCoordType
      ,  const std::vector<std::vector<Nb>>& aCoord
      ,  const std::vector<std::vector<std::string>>& aSymbols
      ,  const std::vector<Nb>& aEnergies
      ,  const std::vector<std::vector<InternalCoord>>& aIntCoord
      ,  const std::vector<std::vector<Nb>>& aCartCoord
      ,  const std::vector<std::vector<In>>& aZmatNa
      ,  const std::vector<std::vector<In>>& aZmatNb
      ,  const std::vector<std::vector<In>>& aZmatNc
      ,  const In& aNatoms 
      ) : mType(aCoordType)
        , mCoordinates(aCoord)
        , mSymbols(aSymbols)
        , mEnergies(aEnergies)
        , mInternCoord(aIntCoord)
        , mCartCoord(aCartCoord)
        , mZmatNa(aZmatNa)
        , mZmatNb(aZmatNb)
        , mZmatNc(aZmatNc)
        , mNatoms(aNatoms)
   {
   }

   GeoDatabase
      (  const std::string&
      ,  const In&
      );

   std::unique_ptr<Nb[]> GetBMatrix(const In&);
   std::unique_ptr<Nb[]> GetBMatrix(const In&, const vector<Nb>&);

   void ReadCoordinates(const std::string&);
   void ReadCoordinates(const std::string&, const bool&, const std::vector<In>&, const std::vector<In>&, const std::vector<In>&);
   void ReadCoordinates(const std::string&, const bool&, const std::vector<In>&, const std::vector<In>&, const std::vector<In>&, const bool&, const std::vector<InternalCoord>&);
   void ReadCoordinates(const std::string&, const bool&, const std::vector<InternalCoord>&);

   void PrintMolecule(const In&, const Nb&, const bool& ) const;
   void PrintMolecule(std::ostream&, const In&, const Nb&, const bool&) const;

   std::vector<std::vector<Nb>> GetTrajectory() const           {return mCoordinates;}
   std::vector<Nb> GetCoordinates(const In& aIndex) const       {return mCoordinates[aIndex];}
   std::vector<Nb> GetEnergies() const                          {return mEnergies;}
   std::vector<std::string> GetSymbols(const In& aIndex) const  {return mSymbols[aIndex];}
   In GetCoordSize(const In& aIndex) const                      {return mCoordinates[aIndex].size();}
   In GetNumAtoms() const                                       {return mNatoms;}
   In GetSize() const                                           {return mCoordinates.size();}
   Nb GetEnergy(const In& aIndex) const                         {return mEnergies[aIndex];}

   std::vector<Nb> GetAtomicMasses(const In& index) const;
   std::vector<Nb> GetCoordinateMasses(const In& index) const;

   std::vector<Nb> GetCartCoord(const In& aIndex) const
   {
      switch (mType)
      {
         case XYZ: 
         case XYZCENTER:
         {
            return mCoordinates[aIndex];
            break;
         }
         case MINT: 
         case ZMAT:
         case DIST:
         case EIGDIST:
         case INV:
         case SYMG2:
         case SYMG4:
         {
            return mCartCoord[aIndex]; 
            break;
         }
      }

      vector<Nb> novec; // to eliminate a Warning
      return novec;
   }

   std::vector<InternalCoord> GetIntCoord
      (  const In& aIndex
      ) const
   {
      if ( !(  mType == MINT 
            || mType == DIST 
            || mType == INV 
            || mType == SYMG2 
            || mType == SYMG4
            ) 
         )
      {
         MIDASERROR("Internal Coordinates requested, but not available!");
      }
 
      return mInternCoord[aIndex]; 
   }

   bool UseInternalCoordinates() const 
   {
      switch (mType)
      {
         case XYZ: 
         case XYZCENTER:
         case EIGDIST:
         {
            return false;
            break;
         }
         case ZMAT:
         case MINT: 
         case DIST:
         case INV:
         case SYMG2:
         case SYMG4:
         {
            return true; 
            break;
         }
      }

      return false;  // Only to please the compiler not to give a Warning
   }

   std::vector<Nb> GetCoord
      (  const vector<InternalCoord>& aInternCoord
      ) const;

   bool IsEquivalent
      (  const Nb& aThrSim
      ,  const In& aIndexMy
      ,  const GeoDatabase& aRefGeo
      ,  const In& aIndexInRef
      ) const;

   bool ContainsMolecule 
      (  const Nb& aThrSim
      ,  const GeoDatabase& aMol 
      ,  const In& aIndex
      ) const;

   vector<Nb> GetRMSD
      (  const GeoDatabase& aMol
      ,  const In& aIndex
      ) const;
      
   std::string GetCoordinateType
      ( const In& aImol
      , const In& aIndex
      ) const
   {
      if (mType == MINT || mType == DIST || mType == INV) 
      {
         switch (mInternCoord[aImol][aIndex].itype)
         {
            case 1:
            {
               return "bond";
               break;
            }
            case 2:
            {
               return "angle";
               break;
            }
            case 4:
            {
               return "dihedral angle";
               break;
            }
         }        
      }

      return "Unknown";
   }

   void SaveConnectivity(const std::string&, const In&) const;

   void GetConnectivity(std::vector<In>&, std::vector<In>&, std::vector<In>&, const In&) const;

   bool HasGradientInfo(const In& aImol) const; 
   bool HasHessianInfo(const In& aImol) const; 

   std::tuple<std::vector<MLDescriptor<Nb>>,std::vector<Nb>> Convert2Descriptors() const;  


};



#endif
