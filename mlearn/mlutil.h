
#ifndef MLUTIL_INCLUDED
#define MLUTIL_INCLUDED

#include <fstream>
#include "util/Error.h"
#include "input/GetLine.h"
#include "mlearn/GPKernel.h"
#include "mlearn/GauPro.h"

namespace mlearn
{


template <class T>
auto GetKernelTypeFromString
   (  const std::string& kerneltype
   )
{

   const map<std::string,typename GPKernel<T>::KernelType> input_kernel =
   {
      {"OUEXP",    GPKernel<T>::KernelType::OUEXP}     ,
      {"MATERN32", GPKernel<T>::KernelType::MATERN32}  ,
      {"MATERN52", GPKernel<T>::KernelType::MATERN52}  ,
      {"SQEXP",    GPKernel<T>::KernelType::SQEXP}     ,
      {"SC32",     GPKernel<T>::KernelType::SC32}      ,        
      {"SC52",     GPKernel<T>::KernelType::SC52}      ,        
      {"POLYNOM",  GPKernel<T>::KernelType::POLYNOM}   ,        
      {"PERIODIC", GPKernel<T>::KernelType::PERIODIC}  ,        
      {"SCEXP",    GPKernel<T>::KernelType::SCEXP}     ,
      {"RBF",      GPKernel<T>::KernelType::RBF}       ,        
      {"RATQUAD",  GPKernel<T>::KernelType::RATQUAD}   ,
      {"ANOVA",    GPKernel<T>::KernelType::ANOVA}     ,
      {"SOAP",     GPKernel<T>::KernelType::SOAP}
   };

   auto ktype = input_kernel.at(kerneltype);        

   return ktype;
}

template <class T>
auto GetCovAlgoFromString
   (  const std::string& algotype
   )
{

   const map<std::string,typename GauPro<T>::CovAlgo> input_algo =
   {
      {"SVD",      GauPro<T>::CovAlgo::USESVD}      ,
      {"CHOL",     GauPro<T>::CovAlgo::USECHOL}     ,
      {"LU",       GauPro<T>::CovAlgo::USELU}       ,
      {"CONJGRAD", GauPro<T>::CovAlgo::USECONJGRAD} ,    
      {"BKD",      GauPro<T>::CovAlgo::USEBKD}      ,   
      {"SPARSE",   GauPro<T>::CovAlgo::USESPARSE}     
   };

   auto type = input_algo.at(algotype);        

   return type;
}

template <class T>
std::vector<In> GetNumberOfHyperParameters
   ( const std::vector<std::string>& aKernelType
   , const In& nintern
   , const In& aNumLayer
   )
{

   std::vector<In> NumberOfParameters(aNumLayer);

   for (In ilayer = 0; ilayer < aNumLayer; ilayer++)
   {
      auto ktype = GetKernelTypeFromString<T>(aKernelType[ilayer]);

      In nparam = 0;

      switch (ktype)
      {
         case GPKernel<T>::KernelType::OUEXP:
         case GPKernel<T>::KernelType::MATERN32:
         case GPKernel<T>::KernelType::MATERN52:
         case GPKernel<T>::KernelType::SQEXP:
         {
            nparam = 2;
            break;
         }
         case GPKernel<T>::KernelType::SC32:
         case GPKernel<T>::KernelType::SC52:
         case GPKernel<T>::KernelType::SCEXP:
         {
            nparam = nintern + 1;
            break;
         }
         case GPKernel<T>::KernelType::POLYNOM:
         {
            nparam = 4;
            break;
         }
         case GPKernel<T>::KernelType::PERIODIC:
         case GPKernel<T>::KernelType::ANOVA:
         case GPKernel<T>::KernelType::RATQUAD:
         {
            nparam  = 3;
            break;
         }
         case  GPKernel<T>::KernelType::SOAP: 
         {
            nparam = nintern + 1;
            break;
         }
         case GPKernel<T>::KernelType::RBF:
         {
            nparam = nintern;
            break;
         }
      }

      NumberOfParameters[ilayer] = nparam;

   }
   return NumberOfParameters;
}


template <class T>
std::vector<std::vector<T>> GetInitialHyperParameter
   (  const std::vector<In> aNumHparam
   ,  bool readfromfile
   ,  const std::vector<std::string>& aKernelType
   ,  const std::string& fhparam = "hparam.in"  
   ,  const std::vector<std::vector<T>> coord = std::vector<vector<T>>()
   ,  const std::vector<T> energies = std::vector<T>()
   ,  const T& aShift = 0.0
   )
{

   In numlayer = aNumHparam.size();
   std::vector<std::vector<T>> hparam(numlayer);

   // read initial hyper parameters if avaialable
   if (readfromfile) 
   {
      std::ifstream infile(fhparam);

      if (infile.good())
      {
         for (In ilayer = 0; ilayer < numlayer; ilayer++)
         {
            hparam[ilayer].resize(0);
            T value;
            for (In ihp = 0; ihp < aNumHparam[ilayer]; ihp++)
            {
               infile >> value; 
               hparam[ilayer].push_back(value);
            }
         }

         infile.close();
      }
      else
      {
         MIDASERROR("FILE WITH HYPERPARAMETERS " + fhparam + " NOT FOUND!");
      }
   }
   else
   {

      for (In ilayer = 0; ilayer < numlayer; ilayer++)
      {

         auto ktype = GetKernelTypeFromString<T>(aKernelType[ilayer]);

         if (coord.empty())
         {
            hparam[ilayer].insert(hparam[ilayer].end(), aNumHparam[ilayer], 2.0);
         }
         else
         {
            // rather emprical start guess based on coodinates
            int ndata  = coord.size();
            int ncoord = coord[0].size(); 

            switch (ktype)
            {
               case GPKernel<T>::KernelType::SC32:
               case GPKernel<T>::KernelType::SC52:
               case GPKernel<T>::KernelType::SCEXP:
               {
                  hparam[ilayer].push_back(1e-7);
                  for (int icoord = 0; icoord < ncoord; icoord++)
                  {
                     T param = 0.0;
                     for (int idata = 0; idata < ndata; idata++)
                     {
                        param += coord[idata][icoord];
                     }
                     param /= static_cast<T>(ndata);
                     param = std::max(std::abs(param),1e-2);
                     hparam[ilayer].push_back(param);
                  }

                  if (!energies.empty() )
                  {
                     if (ilayer == 0)
                     {
                        T average = accumulate( energies.begin(), energies.end(), 0.0)/energies.size(); 
                        hparam[ilayer][0] = std::sqrt(std::abs(average - aShift));
                     }
                     else
                     {
                        hparam[ilayer][0] = 1.0;
                     }
                  }
                  break;
               }
               default:
               {
                  hparam[ilayer].insert(hparam[ilayer].end(), aNumHparam[ilayer], 2.0);
                  break;
               }
            }
 

            Mout << "initial hyper parameters (guess): " << hparam[ilayer] << std::endl;
         }
      }
   }

   return hparam;

}

} // namespace mlearn

#endif /* MLUTIL_INCLUDED */

