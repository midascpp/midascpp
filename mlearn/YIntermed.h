/**
************************************************************************
* 
* @file                
*
* Created:            09-01-2019      
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for better organisation for calculating 
*                     a set of Spherical Harmonics 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SPHHARM_INCLUDED
#define SPHHARM_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>

//#include "specialfunctions/sphericalharmonics.h"
#include "util/CallStatisticsHandler.h"

template <class T>
class YIntermed
{

   private:

      int mLmax = 0;    
      int mNcenter = 0;

      std::vector<std::vector<T>> mCenter; 
 
      std::vector<std::vector<std::vector<std::complex<T>>>> mYlm;

      std::vector<T> mNormsqs;
      std::vector<T> mNorms;


      void AllocateMemory( const int& lmax
                         , const int& ncenter
                         )

      {
         // Reserve memory for common intermediates like norms ||R||
         mNorms.resize(ncenter);
         mNormsqs.resize(ncenter);

         // Reserve memory for the Ylm intermediate 
         mYlm.resize(ncenter);
         for (int idx = 0; idx < mNcenter; idx++)
         {
            for(int lval = 0; lval <= lmax; ++lval)
            {
               mYlm[idx].resize(lmax + 1);
               int mmax = 2*lmax + 1;
               
               mYlm[idx][lval].resize(mmax, std::complex<T>(0.0,0.0));
            }
         }

      }


   public:


      ///> Constructor(s)
      YIntermed()
      {
         mLmax = 0;
         mNcenter = 0;
      }

      YIntermed(  const int& lmax
               ,  const std::vector<std::vector<T>>& centers 
               )
      {
         // Set data
         mLmax = lmax;
         mCenter = centers;

         mNcenter = mCenter.size();
        
         // Reserve memory for spherical harmonics
         AllocateMemory(mLmax, mNcenter); 

         // Calculate Spherical Harmonics
         CalculateSphHarm(mLmax, 0, mNcenter - 1);

      }

      YIntermed(  const int& lmax
               ,  const int& ncenter 
               )
      {
         // Set data
         mLmax = lmax;
         mNcenter = ncenter;
       
         mCenter.resize(ncenter);
 
         // Reserve memory for spherical harmonics
         AllocateMemory(mLmax, mNcenter); 

      }

      ///> Delete copy- and move constructor
      YIntermed(const YIntermed&)  = delete;
      YIntermed(YIntermed&&)       = delete;
    
      ///> Destructor
      ~YIntermed() = default;

      void reset( const int& lmax
                , const std::vector<std::vector<T>>& centers
                )
      {
         // Set data
         mLmax = lmax;
         mCenter = centers;

         mNcenter = mCenter.size();
        
         // Reserve memory for spherical harmonics
         AllocateMemory(mLmax, mNcenter); 

         // Calculate Spherical Harmonics
         CalculateSphHarm(mLmax, 0, mNcenter - 1);
      
      } 

      /**
       *  @brief Sets the positions 
       *
       *  Purpose: Sets the positions for which for which the Spherical Harmonic shell be calculated 
       *
       *  @param centers defines the positions 
       *
       **/
      void SetPositions(const std::vector<std::vector<T>>& centers)
      {
         mCenter = centers;
         if (mNcenter != centers.size())
         {
            mNcenter = centers.size();  
            // Resize memory demands
            AllocateMemory(mLmax, mNcenter); 
         }
      }

      /**
       *  @brief Calculates the Spherical Harmonics 
       *
       *  Purpose: Calculates the Spherical Harmonics for the previously defined centers and the angular momentum
       *
       *  @param lmax defines the angular momentum for which the Spherical Harmonic shell be calculated 
       *  @param iposst is the start index for the range of position for which the Spherical Harmonics should be calculated
       *  @param iposnd is the end index for the range of position for which the Spherical Harmonics should be calculated
       *
       **/
      void CalculateSphHarm(  const int& lmax
                           ,  const int& iposst
                           ,  const int& iposnd 
                           )
      {
         for( int idx = iposst; idx <= iposnd; idx++)
         {

            std::vector<T> ri1 = mCenter[idx];
            mNormsqs[idx] = std::inner_product(std::begin(ri1), std::end(ri1), std::begin(ri1), 0.0);
            mNorms[idx] = std::sqrt(mNormsqs[idx]);

            LOGCALL("Calculating Ylmn");
            
            for(int lval = 0; lval <= lmax; ++lval)
            {
               for(int mval = -lval; mval <= lval; ++mval)
               {
                  mYlm[idx][lval][mval+lval] = SphericalHarmonicY(lval,mval,ri1);
               }
            }
            
         }
      }


      ///> overload () to access Spherical Harmonics as (pos,l,m)
      std::complex<T> operator()(int ipos, int lval, int mval) const 
      {
         return mYlm[ipos][lval][mval+lval];
      }

      /**
       *  @brief Returns the squared norm of a position vector 
       *
       *  Purpose: Returns the squared norm of a position vector used to calculate the spherical harmonics
       *
       *  @param idx is the index of the position vector
       *
       **/
      T GetRNormSq(int idx) const
      {
         return mNormsqs[idx];
      }

      /**
       *  @brief Returns the norm of a position vector 
       *
       *  Purpose: Returns the norm of a position vector used to calculate the spherical harmonics
       *
       *  @param idx is the index of the position vector
       *
       **/
      T GetRNorm(int idx) const
      {
         return mNorms[idx];
      }


};


#endif /* SPHHARM_INCLUDED */
