

#ifndef MATERN32_INCLUDED
#define MATERN32_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class MATERN32_KERN : public GPKernelBase<T>
{

   private:

      const bool mHasHyperParamDeriv = true;
      const bool mHasKernelDeriv = true;
      const bool mHasKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      MATERN32_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      std::string GetName() const override
      {
         return "Matern (nu = 3/2)";
      }

      ///> Destructor
      ~MATERN32_KERN()
      {
      }

      bool HasHyperParamDeriv() const override   {return mHasHyperParamDeriv;}
      bool HasGradient()  const override         {return mHasKernelDeriv;}
      bool HasHessian()   const override         {return mHasKernelHessian;}

      std::vector<T> GetHyperParameters() const override           {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam) override
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T width = mHparam[1];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         T arg = sqrt(3.0) * r / width;

         return sig * sig * ( 1.0 + arg ) * std::exp(-arg) + noise ;

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);

         std::vector<T> grad(nsize);

         T sig = mHparam[0];
         T width = mHparam[1];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         T ret = C_0;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * ( 1.0 + std::sqrt(3.0) * r / width ) *  exp(- std::sqrt(3.0) * r / width);
               break;
            }
            case(1):
            {
               ret =  sig * sig * 3.0 * r * r / (width*width*width) * exp(- std::sqrt(3.0) * r / width) ;
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig   = mHparam[0];
         T width = mHparam[1];
         T sig2  = sig * sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }

         r = std::sqrt(r);
         T arg = sqrt(3.0) * r / width;

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig2 * exp(-arg) * 3.0 * (xi[i] - xk[i]) / (width * width) ; 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig = mHparam[0];
         T width = mHparam[1];
         T sig2 = sig*sig;

         T sum = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            sum += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         T r = std::sqrt(sum);
         T r2 = r*r;
         T l2 = width*width;
         T l3 = l2*width;
         T arg = sqrt(3.0) * r / width;

         if (sum < 1.e-18)
         {
            for (int i = 0; i < nsize*nsize; ++i)
            {
               hessian[i] = C_0;
            }
         }
         else
         { 
            for (int i = 0; i < nsize; ++i)
            {
               hessian[i*nsize + i] = -sig2 * exp(-arg) * ( 3.0/l2 - 3.0 * std::sqrt(3.0) * (xi[i] - xk[i])*(xi[i] - xk[i]) / (l3*r) );

               for (int j = 0; j < i; ++j)
               {
                  T dval = sig2 * exp(-arg) * ( 3.0 * std::sqrt(3.0) * (xi[i] - xk[i])*(xi[j] - xk[j]) / (l3*r) );

                  hessian[j*nsize + i] = dval;
                  hessian[i*nsize + j] = dval;
               }
            }
         }
      }

};


#endif /* KERNEL_INCLUDED */
