

#ifndef SOAP_INCLUDED
#define SOAP_INCLUDED

#include <boost/math/special_functions/bessel.hpp>
#include "mlearn/GPKernelBase.h"


template <class T>
class SOAP_KERN : public GPKernelBase<T>
{

   private:

      const bool mHasHyperParamDeriv = true;
      const bool mHasKernelDeriv = true;
      const bool mHasKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      SOAP_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~SOAP_KERN()
      {
      }

      std::string GetName() const override
      {
         return "Ornstein Uhlenbeck"; 
      }

      bool HasHyperParamDeriv() const override  {return mHasHyperParamDeriv;}
      bool HasGradient()        const override  {return mHasKernelDeriv;}
      bool HasHessian()         const override  {return mHasKernelHessian;}

      std::vector<T> GetHyperParameters() const override            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam) override
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         // init a few shortcuts
         int lmax = xi.GetLmax();
         int nati = xi.GetNatoms();
         int natj = xj.GetNatoms();

         // get alpha via the hyper parameters
         T alpha = std::pow(mHparam[0],2.0);
         T beta  = std::pow(mHparam[0],2.0);

         // TODO change to std::vector<MidasMatrix> see mmv/MidasMatrix_Decl.h

         std::vector<std::vector<std::vector<std::complex<T>>>> Ilmn(lmax+1);

         T kernel[nati][natj];
  
         /*************************************************************************
         * Preallocate memory
         **************************************************************************/

         // Reserve memory for the I intermediate 
         for (int l = 0; l <= lmax; l++)
         {
            int mmax = 2*lmax + 1;
            Ilmn[l].resize(mmax);
            for (int m = 0; m < mmax; m++)
            {
               Ilmn[l][m].resize(mmax, std::complex<T>(0.0,0.0));
            }
         }

         /*************************************************************************
         * Precalculate spherical harmonics 
         **************************************************************************/

         std::vector<YIntermed<T>> Ylm_I(nati);
         for (int iat = 0; iat < nati; iat++)
         {
            std::vector<std::vector<T>> neighb_i = xi.GetNNeighbourspositions(iat);
            Ylm_I[iat].reset(lmax,neighb_i);
         }   
 
         std::vector<YIntermed<T>> Ylm_J(natj);
         for (int jat = 0; jat < natj; jat++)
         {
            std::vector<std::vector<T>> neighb_j = xj.GetNNeighbourspositions(jat);
            Ylm_J[jat].reset(lmax,neighb_j);
         }
         
         /*************************************************************************
         * Loop over atom pairs and calculate kernel values 
         **************************************************************************/

         // Loop over atoms in xi
         for (int iat = 0; iat < nati; iat++)
         {
            
            // Get nearest neighbours of i
            std::vector<std::vector<T>> neighb_i = xi.GetNNeighbourspositions(iat);

            T normi = kernel_pair_value(alpha, beta, Ilmn, Ylm_I[iat], Ylm_I[iat], neighb_i, neighb_i, lmax);

            // Loop over atoms in xj
            for (int jat = 0; jat < natj; jat++)
            {
               
               // Get nearrest neihbours of j
               std::vector<std::vector<T>> neighb_j = xj.GetNNeighbourspositions(jat);

               // Do k(p_i,p'_j) for specific atom pair (i,j)
               T normj = kernel_pair_value(alpha, beta, Ilmn, Ylm_J[jat], Ylm_J[jat], neighb_j, neighb_j, lmax);

               T normij = std::sqrt(normi*normj);

               kernel[iat][jat] = kernel_pair_value(alpha, beta, Ilmn, Ylm_I[iat], Ylm_J[jat], neighb_i, neighb_j, lmax) / normij; 

            }

         }
 
         // For the covariance matrix we return K(d,d') = \sum_{i \in I} \sum_{j \in J} C(p_i,p_j') 
         // for the atom sets I and J, where is the vector containing the atomic densities
         // See Eq. 12 in Gaussian Approximation Potentials: a brief tutorial introduction
         T covar = 0.0;
         for (int iat = 0; iat < nati; iat++)
         {
            for (int jat = 0; jat < natj; jat++)
            {
               covar += kernel[iat][iat];
            }
         }

         return covar;

      }

      T kernel_pair_value
         (  const T& alpha
         ,  const T& beta
         ,  std::vector<std::vector<std::vector<std::complex<T>>>>& Ilmn
         ,  const YIntermed<T>& Ylm_i
         ,  const YIntermed<T>& Ylm_j
         ,  const std::vector<std::vector<T>>& neighb_i
         ,  const std::vector<std::vector<T>>& neighb_j
         ,  const int& lmax          
         )
      {
         const bool locdbg = false;
      
         // define shortcuts
         int nneighboursi = neighb_i.size();
         int nneighboursj = neighb_j.size();
      
         // Initialize Ilmm' to zero
         for (int l = 0; l <= lmax; l++)
         {
            int mmax = 2*lmax + 1;
            
            for (int m = 0; m < mmax; m++)
            {
               std::fill( Ilmn[l][m].begin(), Ilmn[l][m].end(), std::complex<T>(0.0,0.0));
            }
         }
      
         //////////////////////////////////////////////////////////////////////////////////
         // Iterate over neighbour pairs and calculate I intermediate
         //////////////////////////////////////////////////////////////////////////////////
      
         // iterate over I (just the vectors pointing from i to neighbour)
         for( int idx = 0; idx < nneighboursi; idx++) 
         {
            // Get |ri|^2 and ri
            T norm1sq = Ylm_i.GetRNormSq(idx);  
            T norm1   = Ylm_i.GetRNorm(idx); 
      
            // iterate over J (just the vectors pointing from j to neighbour)
            for( int jdx = 0; jdx < nneighboursj; jdx++) 
            { 
               // Get |rj|^2 and rj
               T norm2sq = Ylm_j.GetRNormSq(jdx);   
               T norm2   = Ylm_j.GetRNorm(jdx); 
            
               // Calculate i_l(\alpha ri x rj)  
               T twoarirj = (2.0*alpha*beta*norm1*norm2)/(alpha + beta);
               std::vector<T> il;
               {
                  //il = ModSphBes1kndArr(lmax,twoarirj);
                  il = ModSphBes1kndRecur(lmax,twoarirj);
               }
               T paramterm = 2.0 * std::pow((alpha*beta), 0.5) * (1.0/(alpha + beta));
               T prefac = 4.0 * M_PI * std::sqrt(std::pow(paramterm, 3.0)) * std::exp(-1.0*alpha*beta*(1.0/(alpha + beta))*(norm1sq + norm2sq));
      
               // loop over l
               for(int lval = 0; lval <= lmax; ++lval)
               {
                  LOGCALL("Inner Loop");  
                  //T prefac2 = prefac * il[lval];
                  T prefac2 = prefac * boost::math::sph_bessel (lval, twoarirj );
      
                  // loop over m
                  for(int mval = -lval; mval <= lval; ++mval)
                  {
                     // loop over m'  NOTE: We exploit that I_mm' is symmetric and we only need to reference one triangle
                     for(int mdval = -lval; mdval <= mval; ++mdval)
                     {  
      
                        // And finally I_{m,m'}^l = 4 \pi exp ( -\alpha ( |ri|^2 + |rj|^2 )/2  )* i_l(\alpha ri x rj) * Y_{l,m}(^ri) * Y*_{l,m}(^rj)
                        Ilmn[lval][mval+lval][mdval+lval] += prefac2 * Ylm_i(idx,lval,mval) * std::conj(Ylm_j(jdx,lval,mdval)) ;
         
                     }
                  }
               }
            }
         }
         
         // k(p_i,p_j) definition
         T kernel_ij = 0.0;
         
         for (int lval = 0; lval <= lmax; ++lval)
         {
            LOGCALL("kernel calculation");
            for (int mval = -lval; mval <= lval; ++mval)
            {
               // The matrix is symmetric and therefore we don't need to reference all elements...
               for(int mdval = -lval; mdval <= mval; ++mdval)
               {
                  T dfac = 2.0;
                  if (mval == mdval) dfac = 1.0;
                  kernel_ij  += dfac * std::norm(Ilmn[lval][mval+lval][mdval+lval]); 
         
               }
            }     
         }
         
         return kernel_ij;
      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         // TODO 3. derive analytical expression 
         T stepsize = 0.001;
         bool addnoise = false;
         T noise = C_0;
         T temp = mHparam[0];
         
         mHparam[0] = temp + stepsize;
         T val1 = this->Compute(xi, xj, addnoise, noise, lderi, lderj);
         
         mHparam[0] = temp - stepsize;
         T val2 = this->Compute(xi, xj, addnoise, noise, lderi, lderj); 
         
         T retval = (val1 - val2)/(2.0 * stepsize);

         mHparam[0] = temp;

         return retval;

      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )  override
      {
         MIDASERROR("Gradient not implemented for this kernel!");
         std::vector<T> vec;
         return vec;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )  override
      {
         MIDASERROR("Hessian not implemented for this kernel!");
      }

};


#endif /* KERNEL_INCLUDED */
