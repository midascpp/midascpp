

#ifndef RBF_INCLUDED
#define RBF_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class RBF_KERN : public GPKernelBase<T>
{

   private:

      const bool mHasHyperParamDeriv = true;
      const bool mHasKernelDeriv = true;
      const bool mHasKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      RBF_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~RBF_KERN() 
      {
      }

      std::string GetName() const override
      {
         return "RBF"; 
      }

      bool HasHyperParamDeriv() const override   {return mHasHyperParamDeriv;}
      bool HasGradient() const override          {return mHasKernelDeriv;}
      bool HasHessian() const override           {return mHasKernelHessian;} 

      std::vector<T> GetHyperParameters() const override            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam) override
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         int imode = 10 * lderi + lderj;

         int icoord,jcoord,kcoord,lcoord;
         int np = -1;

         T kernel(0);

         switch (imode)
         {
            case (0):
            {
               // Energy/Energy datapoint
               kernel = rbf_kern00(xi,xj);
               break;
            }
            case (10):
            case (1):
            {
               // Energy/Gradient datapoint
               icoord = 0;

               if (imode == 10) 
               {
                  icoord = xi.GetDerivCoord1();
                  np = 0;
               }
               if (imode == 1) 
               {
                  icoord = xj.GetDerivCoord1(); 
                  np = 1;
               }

               kernel = rbf_kern10(xi,xj,icoord,np);
               break;
            }
            case (11):
            case (20):
            case (2):
            {
               // Gradient/Gradient or Energy Hessian datapoint
               if (imode == 11)
               {
                  jcoord = xj.GetDerivCoord1();
                  icoord = xi.GetDerivCoord1();
                  np = 1;
               }
               else if (imode == 20)
               {
                  jcoord = xi.GetDerivCoord2();
                  icoord = xi.GetDerivCoord1();
                  np = 0;
               }
               else if (imode == 2)
               {
                  jcoord = xj.GetDerivCoord2();
                  icoord = xj.GetDerivCoord1();
                  np = 2;
               }

               kernel = rbf_kern20(xi,xj,jcoord,icoord,np);
               break;
            }
            case (12):
            case (21):
            {
               if (imode == 12) 
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xj.GetDerivCoord1();
                  kcoord = xj.GetDerivCoord2();
                  np = 2;
               }
               else
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xi.GetDerivCoord2();
                  kcoord = xj.GetDerivCoord1();
                  np = 1;
               }
               kernel = rbf_kern30(xi,xj,kcoord,jcoord,icoord,np);
               break;
            }
            case (22):
            {
               // Hessian/Hessian datapoint
               lcoord = xi.GetDerivCoord2();
               kcoord = xi.GetDerivCoord1();
               jcoord = xj.GetDerivCoord2();
               icoord = xj.GetDerivCoord1();
               np = 2;

               kernel = rbf_kern40(xi,xj,lcoord,kcoord,jcoord,icoord,np);
               break;
            }
            default:
            {
               Mout << "imode " << imode << std::endl;
               MIDASERROR("rbf_kernel:> Unknown computation mode!");
               break;
            }
         }

         T noise = C_0;
         if (addnoise) noise = noiseval;

         kernel += noise;

         return kernel;

      }

      inline T rbf_kern00
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);
      
         T r = (xi[0] - xj[0]) * (xi[0] - xj[0]) / (mHparam[0] * mHparam[0]); 
         for (int i = 1; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]) / (mHparam[i] * mHparam[i]); 
         }
      
         return exp(-0.5 * r );
      }
      
      inline T rbf_kern10
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& icoord
         ,  const int& nj
         )
      {
         /*****
          *  Specialized SQEXP Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
          *  \begin{equation*}
          *    \frac{\partial k({\bf{x}},{\bf{x}}')}{\partial {{x}}_i} = - l_i^{-2} \left(x_i-x_i'\right) k({\bf{x}},{\bf{x}}')
          *  \end{equation*}
          *
          *
          *****/
         T lii2 = 1.0 / ( mHparam[icoord] * mHparam[icoord]);  
      
         T sign = std::pow(-1.0,nj);
      
         T k00 = rbf_kern00(xi,xj);
      
         return sign * -1.0 * (xi[icoord] - xj[icoord]) * lii2 * k00;
      }
      
      inline T rbf_kern20
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         /*****
          *  Specialized SQEXP Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
          *  \begin{equation*}
          *    \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {x}_j \partial {x}_i} =  - \delta_{ij}l_i^{-2}  k({\bf{x}},{\bf{x}}') 
          *                                                                           + l_i^{-2}  \left(x_i-x_i'\right) \frac{\partial k({\bf{x}},{\bf{x}}')}{\partial x_j}
          *  \end{equation*}
          *
          *
          *****/
      
         T sign = std::pow(-1.0,np);
      
         T hi2 = mHparam[icoord] * mHparam[icoord];
         T lii2 = 1.0 / hi2; 
         T k10  = rbf_kern10(xi,xj,jcoord,n0);
         T ret = - (xi[icoord] - xj[icoord]) * lii2 * k10;
      
         if (icoord == jcoord)
         {
            T lij2 = lii2;
            T k00 = rbf_kern00(xi,xj);
      
            ret += - k00 * lij2;
         }
      
         return sign * ret;
      }
      
      inline T rbf_kern30
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& nj
         )
      {
      
         int n0 = 0;
      
         /*****
          *  Specialized SQEXP Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
          *  \begin{equation*}
          *    \frac{\partial^3 k({\bf{x}},{\bf{x}}')}{\partial {x}_k \partial {x}_j \partial {x}_i} =  
          *       - \delta_{ij}l_i^{-2}  \frac{\partial k({\bf{x}},{\bf{x}}')}{\partial {x}_k} 
          *       - \delta_{ik}l_i^{-2}  \frac{\partial k({\bf{x}},{\bf{x}}')}{\partial {x}_j} 
          *       - l_i^{-2}  \left(x_i-x_i'\right) \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {x}_k \partial {x}_j}
          *  \end{equation*}
          *
          *
          *****/
      
         T sign = std::pow(-1.0,nj);
      
         T hi2  = mHparam[icoord] * mHparam[icoord];
         T lii2 = 1.0 / hi2; 
         T k20  = rbf_kern20(xi,xj,kcoord,jcoord,n0);
      
         T ret = - (xi[icoord] - xj[icoord]) * lii2 * k20;
      
         if (icoord == kcoord)
         {
            T lik2 = lii2;
            T k10j = rbf_kern10(xi,xj,jcoord,n0);
            ret += - k10j * lik2;
         }
      
         if (icoord == jcoord)
         {
            T lij2 = lii2;
            T k10k = rbf_kern10(xi,xj,kcoord,n0);
            ret += - k10k * lij2;
         }  
      
         return sign * ret; 
      }
      
      inline T rbf_kern40
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& lcoord
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         /*****
          *  Specialized SQEXP Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
          *  \begin{equation*}
          *    \frac{\partial^4 k({\bf{x}},{\bf{x}}')}{\partial {x}_l \partial {x}_k \partial {x}_j \partial {x}_i} =  
          *       - \delta_{ij}l_i^{-2}  \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {x}_l \partial {x}_k} 
          *       - \delta_{ik}l_i^{-2}  \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {x}_l \partial {x}_j} 
          *       - \delta_{il}l_i^{-2}  \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {x}_k \partial {x}_j} 
          *       - l_i^{-2}  \left(x_i-x_i'\right) \frac{\partial^3 k({\bf{x}},{\bf{x}}')}{\partial {x}_l  \partial {x}_k \partial {x}_j}
          *  \end{equation*}
          *
          *****/
         T sign = std::pow(-1.0,np);
      
         T hi2  = mHparam[icoord] * mHparam[icoord];
         T lii2 = 1.0 / hi2; 
         T k30  = rbf_kern30(xi,xj,lcoord,kcoord,jcoord,n0);
      
         T ret = - (xi[icoord] - xj[icoord]) * lii2 * k30;
      
         if (icoord == lcoord)
         {
            T lil2 = lii2;
            T k20kj = rbf_kern20(xi,xj,kcoord,jcoord,n0);
            ret += - k20kj * lil2;
         }  
      
         if (icoord == kcoord)
         {
            T lik2 = lii2;
            T k20lj = rbf_kern20(xi,xj,lcoord,jcoord,n0);
            ret += - k20lj * lik2;
         }
      
         if (icoord == jcoord)
         {
            T lij2 = lii2;
            T k20lk = rbf_kern20(xi,xj,lcoord,kcoord,n0);
            ret += - k20lk * lij2;
         }
      
         return sign * ret;
      }


      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         int imode = 10 * lderi + lderj;
         //int imode = 10 * xi.GetDerivativeOrder() + xj.GetDerivativeOrder();

         int icoord,jcoord,kcoord,lcoord;
         int np;

         T hderiv = C_0;

         switch (imode)
         {
            case (0):
            {
               // Energy/Energy datapoint
               np = 0;
               hderiv = rbf_h10(xi,xj,hidx,np);
               break;
            }
            case (10):
            case (1):
            {
               // Energy/Gradient datapoint
               icoord = 0;
               np = 0;

               if (imode == 10) {icoord = xi.GetDerivCoord1();}
               if (imode == 1) 
               {
                  icoord = xj.GetDerivCoord1(); 
                  np = 1;
               }

               hderiv = rbf_h20(xi,xj,icoord,hidx,np);
               break;
            }
            case (11):
            case (20):
            case (2):
            {
               // Gradient/Gradient or Energy Hessian datapoint
               if (imode == 11)
               {
                  jcoord = xj.GetDerivCoord1();
                  icoord = xi.GetDerivCoord1();
                  np = 1;
               }
               else if (imode == 20)
               {
                  jcoord = xi.GetDerivCoord2();
                  icoord = xi.GetDerivCoord1();
                  np = 0;  
               }
               else // if (imode == 2)
               {
                  jcoord = xj.GetDerivCoord2();
                  icoord = xj.GetDerivCoord1();
                  np = 2;
               }

               hderiv = rbf_h30(xi,xj,jcoord,icoord,hidx,np);
               break;
            }
            case (12):
            case (21):
            {
               // Gradient/Hessian datapoint
               if (imode == 12) 
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xj.GetDerivCoord1();
                  kcoord = xj.GetDerivCoord2();
                  np = 2;
               }
               else
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xi.GetDerivCoord2();
                  kcoord = xj.GetDerivCoord1();
                  np = 1;
               }
               hderiv = rbf_h40(xi,xj,kcoord,jcoord,icoord,hidx,np);
               break;
            }
            case (22):
            {
               // Hessian/Hessian datapoint
               lcoord = xj.GetDerivCoord2();
               kcoord = xj.GetDerivCoord1();
               jcoord = xi.GetDerivCoord2();
               icoord = xi.GetDerivCoord1();
               np = 2;

               hderiv = rbf_h50(xi,xj,lcoord,kcoord,jcoord,icoord,hidx,np);
               break;
            }
            default:
            {
               Mout << "imode " << imode << std::endl;
               MIDASERROR("rbf_hderiv:> Unknown computation mode!");
               break;
            }
         }

         return hderiv;
      }

      inline T rbf_h10
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& np
         )
      {
      
         T k00 = rbf_kern00(xi,xj);
      
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         T hi = mHparam[hidx];
         T li3 = 1.0 / (hi*hi*hi);
         ret =  (xi[hidx] - xj[hidx]) * (xi[hidx] - xj[hidx]) * li3 * k00;
      
         return sign * ret;
      }
      
      inline T rbf_h20
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         int j = hidx;
         T hi  = mHparam[icoord];
         T hi2 = hi * hi;
      
         //-------------------------------------------------------------------+
         //  -l_i^{-2} (x_i-x_i') d^2 k(x,x')/ dl_j
         //-------------------------------------------------------------------+
         T h10  = rbf_h10(xi,xj,hidx,n0);
         T lii2 = 1.0 / hi2; 
         T dxij = (xi[icoord] - xj[icoord]);
       
         ret = - lii2 * dxij * h10 ;
      
         //-------------------------------------------------------------------+
         //  2 \delta_ij l_i^{-3} (x_i-x_i')  k(x,x')
         //-------------------------------------------------------------------+
         if (icoord == j)
         {
            T lij3 =  1.0 / (hi * hi2);
            T k00  = rbf_kern00(xi,xj);
            ret += 2.0 * lij3 * dxij * k00;
         }
      
         return sign * ret;
      }
      
      inline T rbf_h30
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         int k = hidx;
         T hi  = mHparam[icoord];
         T hi2 = hi * hi;
      
         //-------------------------------------------------------------------+
         //  -l_i^{-2} (x_i-x_i') d^2 k(x,x')/ dl_k dx_j
         //-------------------------------------------------------------------+
         T lii2 = 1.0 / hi2;
         T h20  = rbf_h20(xi,xj,jcoord,hidx,n0);
         T dxij = xi[icoord]-xj[icoord];
      
         ret = - lii2 * dxij * h20 ;
      
         //-------------------------------------------------------------------+
         //  2 \delta_ik l_i^{-3} (x_i-x_i') d k(x,x')/ dx_j
         //-------------------------------------------------------------------+
         if (icoord == k)
         {
            T lki3 = 1.0 / (hi * hi2);
            T k10  = rbf_kern10(xi,xj,jcoord,n0);
            ret += 2.0 * lki3 * dxij * k10;
         } 
         
         //-------------------------------------------------------------------+
         //  \delta_ij -l_i^{-2}  d^2 k(x,x')/ dl_k 
         //-------------------------------------------------------------------+
         if (icoord == jcoord)
         {
            T lij2  = 1.0 / hi2;
            T h10 = rbf_h10(xi,xj,hidx,n0);
            ret += - lij2 * h10;
         }
      
         //-------------------------------------------------------------------+
         //  2 \delta_il \delta_ij l_i^{-3}  k(x,x') 
         //-------------------------------------------------------------------+
         if (icoord == k && icoord == jcoord)
         {
            T lkij3 = 1.0 / (hi * hi2);
            T k00   = rbf_kern00(xi,xj);
            ret += 2.0 * lkij3 * k00;
         }
      
         return sign * ret;
      }
      
      inline T rbf_h40
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         int l = hidx;
      
         T hi  = mHparam[icoord];
         T hi2 = hi * hi;
      
         //-------------------------------------------------------------------+
         //  -l_i^{-2} (x_i-x_i') d^3 k(x,x')/ dl_l dx_k dx_j
         //-------------------------------------------------------------------+
         T lii2  = 1.0 / hi2; 
         T dxij  = (xi[icoord]-xj[icoord]);
         T h30kj = rbf_h30(xi,xj,jcoord,kcoord,hidx,n0);
      
         ret = - lii2 * dxij * h30kj;
      
         //-------------------------------------------------------------------+
         // 2 \delta_li l_i^{-3} (x_i-x_i') d^2 k(x,x')/ dx_k dx_j
         //-------------------------------------------------------------------+
         if (icoord == l)
         {
            T liil3 = 1.0 / (hi * hi2);
            T k20kj = rbf_kern20(xi,xj,jcoord,kcoord,n0);
            ret += 2.0 * liil3 * dxij * k20kj;
         }
      
         //-------------------------------------------------------------------+
         // \delta_ik  -l_i^{-2}  d^2 k(x,x')/ dl_l dx_j
         //-------------------------------------------------------------------+
         if (icoord == kcoord)
         {
            T lik2  = 1.0 / hi2;
            T h20j  = rbf_h20(xi,xj,jcoord,hidx,n0);
            ret += - lik2 * h20j;
         }
      
         //-------------------------------------------------------------------+
         // 2 \delta_ik \delta_ik  l_i^{-3}  d k(x,x')/ dx_j
         //-------------------------------------------------------------------+
         if (icoord == kcoord && kcoord == l)
         {
            T likl3 = 1.0 / (hi * hi2);
            T k10j  = rbf_kern10(xi,xj,jcoord,n0);
            ret += 2.0 * likl3 * k10j;
         }
         
         //-------------------------------------------------------------------+
         // \delta_ij  -l_i^{-2}  d^2 k(x,x')/ dl_l dx_k
         //-------------------------------------------------------------------+
         if (icoord == jcoord)
         {
            T lij2 = 1.0 / hi2; 
            T h20k = rbf_h20(xi,xj,kcoord,hidx,n0);
            ret += - lij2 * h20k;
         }
      
         //-------------------------------------------------------------------+
         // 2 \delta_mi \delta_ij  l_i^{-3}  d k(x,x')/ dx_k
         //-------------------------------------------------------------------+
         if (icoord == jcoord && jcoord == l)
         {
            T lijl3 = 1.0 / (hi * hi2);
            T k10k  = rbf_kern10(xi,xj,kcoord,n0);
            ret += 2.0 * lijl3 * k10k;
         }
      
         return sign * ret;
      }
      
      inline T rbf_h50
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& lcoord
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         int m = hidx;
      
         T hi  = mHparam[icoord];
         T hi2 = hi * hi; 
      
         //-------------------------------------------------------------------+
         //  -l_i^{-2} (x_i-x_i') d^4 k(x,x')/ dl_m dx_l dx_k dx_j
         //-------------------------------------------------------------------+
         T lii2   = 1.0 / hi2;
         T h40lkj = rbf_h40(xi,xj,lcoord,kcoord,jcoord,hidx,n0);
         T dxij   = (xi[icoord]-xj[icoord]);
      
         ret = - lii2 * dxij * h40lkj;
      
         const bool is_im = (icoord == m);
         const bool is_ij = (icoord == jcoord); 
         const bool is_il = (icoord == lcoord); 
         const bool is_ik = (icoord == kcoord); 
      
         //-------------------------------------------------------------------+
         // 2 \delta_mi l_i^{-3} (x_i - x_i') d^3 k(x,x')/ dx_l dx_k dx_j
         //-------------------------------------------------------------------+
         if (is_im)
         {
            T lmi3  =  1.0 / (hi2 * hi);
            T k30lkj = rbf_kern30(xi,xj,lcoord,kcoord,jcoord,n0);
            ret += 2.0 * lmi3 * dxij * k30lkj;
         }
      
         //-------------------------------------------------------------------+
         // - \delta_li l_i^{-2}  d^3 k(x,x')/ dl_m dx_k dx_j
         //-------------------------------------------------------------------+
         if (is_il)
         {
            T lil2 = 1.0 / hi2;
            T h30kj  = rbf_h30(xi,xj,kcoord,jcoord,hidx,n0);
            ret += - lil2 * h30kj;
         }
      
         //-------------------------------------------------------------------+
         // 2 \delta_mi \delta_li l_i^{-3} d^2 k(x,x')/ dx_k dx_j
         //-------------------------------------------------------------------+
         if (is_il && is_im)
         {
            T lmil3 = 1.0 / (hi2 * hi) ;
            T k20kj  = rbf_kern20(xi,xj,kcoord,jcoord,n0);
            ret += 2.0 * lmil3 * k20kj;
         }
      
         //-------------------------------------------------------------------+
         // - \delta_ki l_i^{-2} d^3 k(x,x')/dl_m  dx_l dx_j
         //-------------------------------------------------------------------+
         if (is_ik)
         {
            T lik2 = 1.0 / hi2;
            T h30lj  = rbf_h30(xi,xj,lcoord,jcoord,hidx,n0);
            ret += - lik2 * h30lj;
         }
      
         //-------------------------------------------------------------------+
         // 2 \delta_mi \delta_ki l_i^{-3} d^2 k(x,x')/ dx_l dx_j
         //-------------------------------------------------------------------+
         if (is_ik && is_im)
         {
            T lmik3 = 1.0 / (hi2 * hi);
            T k20lj  = rbf_kern20(xi,xj,lcoord,jcoord,n0);
            ret += 2.0 * lmik3 * k20lj;
         }
      
         //-------------------------------------------------------------------+
         // - \delta_ji l_i^{-2} d^3 k(x,x')/dl_m  dx_l dx_k
         //-------------------------------------------------------------------+
         if (is_ij)
         {
            T lij2 = 1.0 / hi2;
            T h30lk  = rbf_h30(xi,xj,lcoord,kcoord,hidx,n0);
            ret += - lij2 * h30lk;
         }
      
         //-------------------------------------------------------------------+
         // 2 \delta_mi \delta_ij l_i^{-3} d^2 k(x,x')/ dx_l dx_k
         //-------------------------------------------------------------------+
         if (is_ij && is_im)
         {
            T lmij3 = 1.0 / (hi2 * hi);
            T k20lk  = rbf_kern20(xi,xj,lcoord,kcoord,n0);
            ret += 2.0 * lmij3 * k20lk;
         }
      
         return sign * ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]) / std::pow(mHparam[i], 2.0);
         }

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - exp(-0.5 * r ) * (xi[i] - xk[i]) / (std::pow(mHparam[i], 2.0)); 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]) / std::pow(mHparam[i], 2.0);
         }
  
         for (int i = 0; i < nsize; ++i)
         {
            hessian[i*nsize + i] = - exp(-0.5 * r) * ( 1.0 / std::pow(mHparam[i], 2.0) 
                                                     - (xi[i] - xk[i]) * (xi[i] - xk[i]) / (std::pow(mHparam[i], 4.0))
                                                     ); 

            for (int j = 0; j < i; ++j)
            {
               T dval =   exp(-0.5* r) * ( (xi[i] - xk[i])*(xi[j] - xk[j]) / (std::pow(mHparam[i], 2.0) * std::pow(mHparam[j], 2.0)));

               hessian[j*nsize + i] = dval;
               hessian[i*nsize + j] = dval;
            }
         }
      }
};


#endif /* KERNEL_INCLUDED */
