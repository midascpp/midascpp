

#ifndef ANOVA_INCLUDED
#define ANOVA_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class ANOVA_KERN : public GPKernelBase<T>
{

   private:

      const bool mHasHyperParamDeriv = true;
      const bool mHasKernelDeriv = true;
      const bool mHasKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      ANOVA_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~ANOVA_KERN()
      {
      }

      std::string GetName() const override
      {
         return "ANOVA"; 
      }

      bool HasHyperParamDeriv() const override   {return mHasHyperParamDeriv;}
      bool HasGradient() const override          {return mHasKernelDeriv;}
      bool HasHessian()  const override          {return mHasKernelHessian;}

      std::vector<T> GetHyperParameters() const override {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam) override
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T alpha = mHparam[1];
         T dexp  = mHparam[2];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         int num = 4;

         T ret = C_0;
         for (int k = 0; k < num; k++)
         {
            for (int i = 0; i < nsize; ++i) 
            {
               ret += std::pow(std::exp(-alpha* (std::pow(xi[i],k) - std::pow(xj[i],k)) * (std::pow(xi[i],k) - std::pow(xj[i],k))),dexp);
            }
         }

         return sig * sig * ret + noise;
      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         MIDASERROR("Hyper parameter gradient not implemented for this kernel!");
         return static_cast<T>(0.0);
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )  override
      {
         MIDASERROR("Gradient not implemented for this kernel!");
         std::vector<T> vec;
         return vec;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )  override
      {
         MIDASERROR("Hessian not implemented for this kernel!");
      }

};


#endif /* KERNEL_INCLUDED */
