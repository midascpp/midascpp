

#ifndef SC52_INCLUDED
#define SC52_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class SC52_KERN : public GPKernelBase<T>
{

   private:

      const bool mHasHyperParamDeriv = true;
      const bool mHasKernelDeriv = true;
      const bool mHasKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      SC52_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~SC52_KERN() = default;

      std::string GetName() const override
      {
         return "SC 5/2";
      }

      bool HasHyperParamDeriv() const override   {return mHasHyperParamDeriv;}
      bool HasGradient() const override          {return mHasKernelDeriv;}
      bool HasHessian() const override           {return mHasKernelHessian;}

      std::vector<T> GetHyperParameters() const override            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam) override
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         int imode = 10 * lderi + lderj;

         switch (imode)
         {
            case (0):
            {
               // Energy/Energy datapoint
               return sc52_kern00(xi,xj,addnoise,noiseval);
               break;
            }
            case (10):
            case (01):
            {
               // Energy/Gradient datapoint
               T sign = 1.0;
               int icoord = 0;
               if (imode == 10) {icoord = xi.GetDerivCoord1(); sign =  1.0;}
               if (imode == 01) {icoord = xj.GetDerivCoord1(); sign = -1.0;}

               return sign * sc52_kern10(xi,xj,icoord);
               break;
            }
            case (11):
            {
               // Gradient/Gradient datapoint
               int icoord = xi.GetDerivCoord1();
               int jcoord = xj.GetDerivCoord1();

               return sc52_kern11(xi,xj,icoord,jcoord);
               break;
            }
            default:
            {
               MIDASERROR("sc52_kernel:> Unknown computation mode!");
               break;
            }
         }

         return 0;

      }

      T sc52_kern00
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool& addnoise  
         ,  const Nb& noiseval 
         )
      {
         int nsize = 0;
      
         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }
      
         T sig   = mHparam[0];
         T noise = C_0;
         if (addnoise) noise = noiseval;
      
         /*****
          *  Specialized Matern 5/2 Kernel:
          *  -----------------------------
          * 
          *   k({\bf{x}},{\bf{x}}') = \sigma^2 \left( 1 
          *                                         + \sqrt{5     \sum_{i=1}^d \frac{(x_i - x_i')^2}{l_i^2}}
          *                                         + \frac{5}{3} \sum_{i=1}^d \frac{(x_i - x_i')^2}{l_i^2}  
          *                                   \right)
          *                                   \exp{\left(- \sqrt{5 \sum_{i=1}^d \frac{(x_i - x_i')^2}{l_i^2}}  \right)}
          *****/
      
      
         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]) / std::pow(mHparam[1 + i], 2.0);
         }
         r = std::sqrt(r);
      
         T arg = sqrt(5.0) * r;
         T sig2 = sig*sig;
      
         return sig2 * ( 1.0 + arg + arg*arg/3.0 ) * std::exp(-arg) + noise ;
      }
      
      inline T sc52_kern10
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         ,  const int& icoord
         )
      {
      
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);
      
         T sig   = mHparam[0];
         T sig2  = sig * sig;
      
         /*****
          *  Specialized Matern 5/2 Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
              \begin{equation*}
               \frac{\partial k({\bf{x}},{\bf{x}}')}{\partial {\bf{x}}_i}
                      = -  \sigma^2 \frac{ 5 \left( x_i - x_i' \right) }{ 3 l_i^2 }
                           \exp{\left(- \sqrt{5 \sum_{i=k}^d \frac{(x_k - x_k')^2}{l_k^2}}  \right)}
                           \left( 1 + \sqrt{5 \sum_{i=k}^d \frac{(x_k - x_k')^2}{l_k^2}}  \right)
              \end{equation*}
          *****/
      
      
         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]) / std::pow(mHparam[1 + i], 2.0)  ;
         }
      
         r = std::sqrt(r);
         T arg = sqrt(5.0) * r ;
         T l2 = std::pow(mHparam[1 + icoord], 2.0);
      
         return - sig2 * 5.0 * (xi[icoord] - xk[icoord]) / (3.0 * l2) * std::exp(-arg) * ( arg + 1.0); 
      }
      
      T sc52_kern11
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& icoord
         ,  const int& jcoord
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);
      
         T sig  = mHparam[0];
         T sig2 = sig*sig;
         T li2  = std::pow(mHparam[1 + icoord], 2.0);
         T lj2  = std::pow(mHparam[1 + jcoord], 2.0);
      
         /*****
          *  Specialized Matern 5/2 Kernel:
          *  -----------------------------
          *
          *  LaTex:
          *
             \begin{equation*}
              \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial{\bf{x}}_j  \partial {\bf{x}}_i}
                     =  \sigma^2 \frac{ 25 \left( x_i - x_i' \right) \left( x_j - x_j' \right)  }{ 3 l_j^2 l_i^2  }
                          \exp{\left(- \sqrt{5 \sum_{i=k}^d \frac{(x_k - x_k')^2}{l_k^2}}  \right)}
             \end{equation*}
             \begin{equation*}
               \frac{\partial^2 k({\bf{x}},{\bf{x}}')}{\partial {\bf{x}}_i^2}
                = -\sigma^2 \frac{5}{3l_i^2} \exp{\left(- \sqrt{5 \sum_{i=k}^d \frac{(x_k - x_k')^2}{l_k^2}}  \right)}
                   \left[ 1 + \sqrt{5 \sum_{i=k}^d \frac{(x_k - x_k')^2}{l_k^2}} - \frac{5\left( x_i - x_i' \right)^2 }{l_i^2}  \right]
             \end{equation*}
      
          *****/
      
         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]) / std::pow(mHparam[1 + i], 2.0);
         }
         r = std::sqrt(r);
         T arg = sqrt(5.0) * r ;
      
         if (icoord == jcoord)
         {
            T dxii2 = (xi[icoord] - xj[icoord]) * (xi[icoord] - xj[icoord]);  
            return sig2 * 5.0 / ( 3.0 * li2 ) * exp(-arg) * (1.0 + arg - 5.0 * dxii2/li2 ) ;
         }
         else
         {
            T dxij2 = (xi[icoord] - xj[icoord]) * (xi[jcoord] - xj[jcoord]);  
            return -sig2 * 25.0 / ( 3.0 * lj2 * li2 ) * exp(-arg) * dxij2;
         }
      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);

         T sig = mHparam[0];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]) / std::pow(mHparam[1 + i], 2.0);
         }
         r = std::sqrt(r);

         T arg = sqrt(5.0) * r ;
         T ret = C_0;
         T sig2 = sig * sig;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * ( 1.0 + arg + arg*arg / 3.0 ) * std::exp(-arg);
               break;
            }
            default:
            {
               int i = hidx - 1;
               T l3 = std::max(std::pow(mHparam[hidx], 3.0),std::numeric_limits<T>::min());
               ret = sig2 * 5.0 * std::pow(xj[i] - xi[i], 2.0) / (3.0 * l3 ) * std::exp(-arg) * ( 1.0 + arg );
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig   = mHparam[0];
         T sig2  = sig * sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]) / std::pow(mHparam[1 + i], 2.0)  ;
         }

         r = std::sqrt(r);
         T arg = sqrt(5.0) * r ;

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig2 * 5.0 * (xi[i] - xk[i]) / (3.0 * std::pow(mHparam[1 + i], 2.0)) * std::exp(-arg) * ( arg + 1.0); 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )  override
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig = mHparam[0];
         T sig2 = sig*sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]) / std::pow(mHparam[1 + i], 2.0);
         }
         r = std::sqrt(r);
         T arg = sqrt(5.0) * r ;

         for (int i = 0; i < nsize; ++i)
         {
            hessian[i*nsize + i] = sig2 * 5.0 / ( 3.0 * std::pow(mHparam[1 + i], 4.0) ) * exp(-arg)
                                   * ( std::pow(mHparam[1 + i], 2.0) * arg 
                                      + 5.0 * (xk[i] - xi[i]) * (xk[i] - xi[i]) 
                                      - std::pow(mHparam[1 + i], 2.0)
                                     ) ; 

            T l2 = std::pow(mHparam[1 + i], 2.0);

            for (int j = 0; j < i; ++j)
            {

               T dval = sig2 * 25.0 / ( 3.0 * std::pow(mHparam[1 + j], 2.0) * l2 ) * exp(-arg) * (xk[i] - xi[i]) * (xk[j] - xi[j]);

               hessian[j*nsize + i] = dval;
               hessian[i*nsize + j] = dval;
            }
         }
      }
};


#endif /* KERNEL_INCLUDED */
