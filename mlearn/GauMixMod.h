/**
************************************************************************
* 
* @file                
*
* Created:            17-11-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for doing a Gaussian Mixture Model
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef GAUMIX_INCLUDED
#define GAUMIX_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>


template <class T>
class GauMixMod
{

   private:

      bool mIsInit = false;

      std::vector<std::vector<T>> mData; 
      std::vector<std::vector<T>> mMeans; 
      std::vector<std::vector<T>> mResp; 

      std::vector<std::unique_ptr<T[]>> mSig;

      std::vector<T> mFrac;
      std::vector<T> mLndets;

      T mLogLike = 0.0;

      int mMixNum = 2;
      int mDim = 0;
      int mDataNum = 0;

      T mJiter = 1e-12;

   public:

      ///> Constructor 
      GauMixMod(  std::vector<std::vector<T>>& data
               ,  int nummix
               )
      {

         // Insert data and first guess for manes
         mData = data;
         mMeans = InitialGuess(data,nummix);

         // get all dimensions
         mDataNum = data.size();
         mDim     = data[0].size();
         mMixNum  = nummix;

         // Initialize arrays and so on...
         init();

         // Do already one EM step
         //estep();
         //mstep();

      }

      ///> Constructor 
      GauMixMod(  std::vector<std::vector<T>>& data
               ,  std::vector<std::vector<T>>& means
               ,  int nummix
               )
      {

         // Insert data and first guess for manes
         mData = data;
         mMeans = means;

         // get all dimensions
         mDataNum = data.size();
         mDim     = data[0].size();
         mMixNum  = nummix;

         // Initialize arrays and so on...
         init();

         // Do already one EM step
         //estep();
         //mstep();

      }

      ///> Delete copy- and move constructor
      GauMixMod(const GauMixMod&)  = delete;
      GauMixMod(GauMixMod&&)       = delete;
    
      ///> Destructor
      ~GauMixMod() = default;


      /***
       *      
       *  @brief Carries out the Expectation step 
       *      
       ***/
      T estep()
      {

         int k,m,n;
         T tmp, sum, max, oldloglike;
         std::vector<T> vec(mDim);
         oldloglike = mLogLike;

         //-----------------------------------------------------------------------+
         // For each Gaussian calculate the reposibility matrix
         //-----------------------------------------------------------------------+
         for (k = 0; k < mMixNum; k++) 
         {
            
            int m = mDim;
            int info = 0;
            int nrhs = 1;
            char uplo = 'U';

            // Calculate Cholesky decompostion of the Convariance Matrix
            std::unique_ptr<T[]> SigChol(new T[mDim*mDim]);
            std::memcpy( SigChol.get(), mSig[k].get(), sizeof(T)*mDim*mDim );
            midas::lapack_interface::potrf(&uplo, &m, SigChol.get(), &m, &info);
            if (info != 0) Mout << "GauMix: Problem in Cholesky decomposition" << std::endl;

            // and the log of its determinant
            // det(A) = det(L) det(L^T) = Prod_i L_ii
            // log det(A) = 2 sum_i L_ii
            for (int i = 0; i < mDim; i++)
            {
               mLndets[k] += 2.0 * SigChol[i * mDim + i];
            }

            // Add everything together:
            //   - 1/2 * [ (x-m) S^-1 (x-m) - M log(2pi) - log det(S) ] 
            for (n = 0; n < mDataNum; n++) 
            {
               // pre-calculate (x-m)
               for (m = 0; m < mDim; m++) 
               {
                  vec[m] = mData[n][m] - mMeans[k][m];
               }
               // solve S v = (x - m)
               midas::lapack_interface::potrs(&uplo, &m, &nrhs, SigChol.get(), &m, &vec[0], &m, &info);
               if (info != 0) Mout << "GauMix: Problem in solving linear system" << std::endl;

               // calculate (x - m) S^-1 (x - m) = (x - m) * v
               for (sum = 0.0, m = 0; m < mDim; m++) 
               {
                  sum +=  (mData[n][m] - mMeans[k][m]) * vec[m]; //std::pow(vec[m],2.0);
               }
               
               mResp[n][k] = -0.5 * (sum + mLndets[k]) + log(mFrac[k]);
            }
         }

         // At this point the logs of the p_nk's are unnomralized. 
         // => Normalize them with log-sum-exp and compute log likelihood
         mLogLike = 0.0;
         for (n = 0; n < mDataNum; n++) 
         {
            max = -99.9e99;
            for (k = 0; k < mMixNum; k++) 
            {  
               if (mResp[n][k] > max) max = mResp[n][k];
            }
            for (sum = 0.0, k = 0; k < mMixNum; k++) 
            {
               sum += exp(mResp[n][k]-max);
            }
            tmp = max + log(sum);
            for (k = 0; k < mMixNum; k++) 
            {
               mResp[n][k] = exp(mResp[n][k] - tmp);
            }
            mLogLike +=tmp;
         }

         return mLogLike - oldloglike;
      }

      /***
       *      
       *  @brief Performs an initial guess for the means of the Gaussians 
       *      
       ***/
      std::vector<std::vector<T>> InitialGuess( std::vector<std::vector<T>>& data
                                              , int numcenter
                                              )
      {

         // The data points are just randomly assigned to one Gaussian
         // and the means of this Gaussians are then calculated

         int ndatanum = data.size();
         int ndim = data[0].size(); 

         std::vector<int> assign(ndatanum);
         std::vector<int> count(numcenter,0);

         for (int idx = 0; idx < ndatanum; idx++ )
         {  
             assign[idx] = idx%numcenter;
         }

         std::shuffle ( assign.begin(), assign.end(), midas::util::detail::get_mersenne() );
  
         std::vector<std::vector<T>> means;

         means.resize(numcenter);
         for (int idx = 0; idx < numcenter; idx++)
         {  
            means[idx].resize(ndim);
         }
 
         for (int n = 0; n < ndatanum; n++)
         {
            for (int m = 0; m < ndim; m++)
            {
               means[assign[n]][m] += data[n][m];
            }
            count[assign[n]] += 1;
         }

         for (int k = 0; k < numcenter; k++)
         {
            if (count[k] > 0)
            {
               for (int m = 0; m < ndim; m++)
               {
                  means[k][m] /= count[k];
               }
            }
         }

         return means;
      }

      /***
       *      
       *  @brief Carries out the Maximatizion step 
       *      
       ***/
      void mstep() 
      {
         const bool locdbg = false;

         int j, n, k, m;
         Nb wgt, sum;

         if (locdbg) 
         {
            Mout << "Means before update" << std::endl;
            for (k = 0; k < mMixNum; k++)
            {
               Mout << " Mean " << k << std::endl;
               Mout << mMeans[k] << std::endl;
            }
         }

         for (k = 0; k < mMixNum; k++) 
         {

            // 1) ^P(k) = 1/N \sum_n p_nk
            wgt = 0.0;
            for (n = 0; n < mDataNum; n++) 
            {
               wgt += mResp[n][k];
            }
            //if (wgt < 1e-15) wgt = 1e-15;
            mFrac[k] = wgt / mDataNum;

            // 2.a) ^m_k = \sum_n p_nk x_n / \sum_n p_nk
            // 2.b) ^S_k = \Sum_n p_nk(x_n - ^m_k) diprod (x_n - ^m_k) / \sum_n p_nk
            
            for (m = 0; m < mDim; m++) 
            {
               for (sum = 0.0, n = 0; n < mDataNum; n++) 
               {
                  sum += mResp[n][k] * mData[n][m];
               }
               mMeans[k][m] = sum / wgt; // 2.a

               for (j = 0; j < mDim; j++) 
               {
                  for (sum = 0.0, n = 0; n < mDataNum; n++) 
                  {
                     sum += mResp[n][k] *
                             (mData[n][m] - mMeans[k][m]) * (mData[n][j] - mMeans[k][j]);
                  }
                  mSig[k][j*mDim + m ] = sum / wgt; // 2.b
               }
               mSig[k][m*mDim + m ] += mJiter; // For numerical stability
            }
         }

         if (locdbg) 
         {
            Mout << "Means after update" << std::endl;
            for (k = 0; k < mMixNum; k++)
            {
               Mout << " Mean " << k << std::endl;
               Mout << mMeans[k] << std::endl;
            }
         }
      }

      /***
       *      
       *  @brief Optimizes the Gaussian Mixture Modell 
       *      
       ***/
      void optimize
         (  int maxiter
         ,   T eps
         )
      {
         bool converged = false;
         T dE;
         int iter = 0;

         for (iter = 0; iter < maxiter; iter++)
         {
            dE = estep();
            mstep();

            Mout << "  iter " << iter << " dE " << dE << std::endl;

            if (std::abs(dE) < eps) 
            {
               converged = true;
               break;
            }
         }

         if (converged) 
         {
            Mout << "  Gaussian Mixture Model converged in " << iter << " cycles" << std::endl;
         }
         else
         {
            Mout << "  Gaussian Mixture Model NOT converged in " << iter << " cycles" << std::endl;
         }
         Mout << std::endl;
      }

      /***
       *      
       *  @brief Returns the mean of Gaussian k 
       *      
       ***/
      std::vector<T> GetMean(int k)
      {
         return mMeans[k];
      }

      /***
       *      
       *  @brief Returns the means of the Gaussians 
       *      
       ***/
      std::vector<std::vector<T>> GetMean()
      {
         return mMeans;
      }

   private:

      void init()
      {
         int i, j, k, n;

         // Alloctae memory and initialize responsibility matrix
         mResp.resize(mDataNum);
         for (n = 0; n < mDataNum; n++)
         {
            mResp[n].resize(mMixNum);
            for (k = 0; k < mMixNum; k++)
            {
               mResp[n][k] = 0.0;
            }
         }

         mFrac.resize(mMixNum);
         mLndets.resize(mMixNum);
         
         // Initialize co-variance matrix of the Gaussians
         mSig.resize(mMixNum);

         for (k = 0; k < mMixNum; k++)
         {
            mSig[k].reset(new T[mDim*mDim]);
         }

         for (k = 0; k < mMixNum; k++) 
         {
            mFrac[k] = 1.0 / mMixNum;
            for (i = 0; i < mDim; i++) 
            {
               for (j = 0; j < mDim; j++) 
               {  
                  mSig[k][j*mDim + i] = 0.0;
               }
               mSig[k][i*mDim + i] = 1.0e-10;
            }
         }

      }
};


#endif /* GAUPRO_INCLUDED */
