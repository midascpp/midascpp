/**
************************************************************************
* 
* @file                
*
* Created:            20-11-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for doing K means clustering 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef KMEANS_INCLUDED
#define KMEANS_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#include "mlearn/GPKernel.h"

template <class T>
class Kmeans 
{

   private:

      bool mUseKernel = false;

      std::vector<std::vector<T>> mData; 
      std::vector<std::vector<T>> mMeans; 

      std::vector<int> mAssign, mCount;

      int mNumCenter = 2;
      int mDim = 0;
      int mDataNum = 0;

      GPKernel<T> mKernel;
      std::unique_ptr<T[]> mGramMat;
      std::unique_ptr<T[]> mDistMat;
      bool mHasGramMat = false;
      
   public:


      ///> Constructor(s) 
      Kmeans(  std::vector<std::vector<T>>& data
            ,  std::vector<std::vector<T>>& means
            ,  int numcenter
            )
      {
         // Set data
         mData = data;
         mMeans = means;

         // Get dimensions
         mDataNum    = data.size();
         mDim        = data[0].size();
         mNumCenter  = numcenter;

         // allocate memory
         mAssign.resize(mDataNum);
         mCount.resize(mNumCenter);

         // Do an initial guess for the means of the cluster centroids
         InitialGuess();

         // Do already one EM step
         //estep();
         //mstep();

      }

      Kmeans(  std::vector<std::vector<T>>& data
            ,  int numcenter
            )
      {
         // Set data
         mData = data;

         // Get dimensions
         mDataNum    = data.size();
         mDim        = data[0].size();
         mNumCenter  = numcenter;

         // allocate memory
         mAssign.resize(mDataNum);
         mCount.resize(mNumCenter);

         mMeans.resize(mNumCenter);
         for (int k = 0; k < mNumCenter; k++)
         {
            mMeans[k].resize(mDim);
         }

         // Do an initial guess for the means of the cluster centroids
         InitialGuess();

      }

      Kmeans(  std::vector<std::vector<T>>& data
            ,  std::vector<std::vector<T>>& means
            ,  int numcenter
            ,  GPKernel<T>& kernel
            ) : mKernel(kernel)
      {

         // Set data
         mData = data;
         mMeans = means;

         // Get dimensions
         mDataNum   = data.size();
         mDim       = data[0].size();
         mNumCenter = numcenter;

         // allocate memory
         mAssign.resize(mDataNum);
         mCount.resize(mNumCenter);

         // Do already one EM step
         estep();
         mstep(); // actually not (yet?) needed

         // Actually two, but we use the first one without kernel 
         // as inital guess for the assignment
         mUseKernel = true;
         estep();
      }

      ///> Delete copy- and move constructor
      Kmeans(const Kmeans&)  = delete;
      Kmeans(Kmeans&&)       = delete;
    
      ///> Destructor
      ~Kmeans() = default;

      /***
       *      
       *  @brief Performs a simple initial guess for the location of the Cluster centroids 
       *      
       ***/
      void InitialGuess()
      {
         const bool RandAssign = false;       
 
         if (RandAssign)
         {
            for (int idx = 0; idx < mDataNum; idx++ )
            {  
                mAssign[idx] = idx%mNumCenter;
            }

            std::shuffle ( mAssign.begin(), mAssign.end(), midas::util::detail::get_mersenne() );
     
            mMeans.resize(mNumCenter);
            for (int idx = 0; idx < mNumCenter; idx++)
            {
               mMeans[idx].resize(mDim);  
            }
 
            mstep();
         }
         else
         {
            std::vector<int> indices(mDataNum);
         
            for (int idx = 0; idx < mDataNum; idx++)
            {
               indices[idx] = idx;
            }

            std::shuffle ( indices.begin(), indices.end(), midas::util::detail::get_mersenne() );

            mMeans.resize(mNumCenter);
            for (int idx = 0; idx < mNumCenter; idx++)
            {
               mMeans[idx] = mData[indices[idx]]; 
            }
 
         }
      }

      /***
       *      
       *  @brief Carries out the Expectation step 
       *      
       ***/
      In estep()
      {
         int m, n, k, kmin;

         if (mUseKernel) 
         {
            SetupGramMatrix();
         }

         kmin = 0;

         Nb dmin, d;
         int nchg = 0;

         // Set cluster sizes to zero 
         for (k = 0; k < mNumCenter; k++) 
         {
            mCount[k] = 0;
         }

         // 
         for (int idx = 0; idx < mDataNum; idx++) 
         {
            if (mUseKernel) 
            {
               
               /**********

                 We calculate the distance of the i-th data to its centrioid as
   
                   ||phi(x_i) - m_k||^2 = K_ii - 2 \sum_j I(x_j) K_ij / sum_j (I_j) + \sum_jl I(x_j) I(x_l) K_jl / sum_jl I(x_j) I(x_l)

                   phi(x_i) : position in feature space
                   K_ij     : Kernel/Gram matrix 
                   I_j      : Assigment: Return 1 if x_j is in cluster otherwise returns 0 

               ***********/

               dmin = 9.99e99;
               for (k = 0; k < mNumCenter; k++)
               {
                  T term1  = C_0;
                  T denom1 = C_0;
                  for (int j = 0; j < mDataNum; j++)
                  {
                     if (mAssign[j] == k)
                     {
                        term1  += 2.0 * mGramMat[idx * mDataNum + j];
                        denom1 += 1.0;
                     }
                  }

                  T term2  = C_0;
                  T denom2 = C_0;
                  for (int j = 0; j < mDataNum; j++)
                  {
                     for (int l = 0; l < mDataNum; l++)
                     {
                        if (mAssign[j] == k && mAssign[l] == k)
                        {
                           term2  += mGramMat[j * mDataNum + l];
                           denom2 += 1.0;
                        }
                     }
                  } 

                  mDistMat[k * mDataNum + idx] = mGramMat[idx * mDataNum + idx];
                  if (denom1 > C_0) mDistMat[k * mDataNum + idx] -= term1 / denom1;
                  if (denom2 > C_0) mDistMat[k * mDataNum + idx] += term2 / denom2;  

                  // get minimal distance of datapoint from clusters
                  if ( mDistMat[k * mDataNum + idx] < dmin ) {dmin = mDistMat[k * mDataNum + idx]; kmin = k;}
               }

               // (re)assign centroids
               if (kmin != mAssign[idx]) nchg++;
               mAssign[idx] = kmin;
               mCount[kmin]++;
            }
            else
            {
               dmin = 9.99e99;
               for (k = 0; k < mNumCenter; k++) 
               {
                  // Calculate Euclidean distance between data point and centroid
                  for (d = 0.0, m = 0; m < mDim; m++) 
                  {
                     d += std::pow(mData[idx][m] - mMeans[k][m],2.0);
                  }
                  d = std::sqrt(d);
                  if (d < dmin) {dmin = d; kmin = k;}
               }
               // (re)assign centroids
               if (kmin != mAssign[idx]) nchg++;
               mAssign[idx] = kmin;
               mCount[kmin]++;
            }
         }

         return nchg;
      }

      /***
       *      
       *  @brief Carries out the Maximatizion step 
       *      
       ***/
      void mstep() 
      {

         const bool locdbg = true;

         int j, n, k, m;

         if (locdbg && !mUseKernel) 
         {
            Mout << "Means before update" << std::endl;
            for (k = 0; k < mNumCenter; k++)
            {
               Mout << " Mean " << k << std::endl;
               Mout << mMeans[k] << std::endl;
            }
         }

         // reset means of centroids
         for (k = 0; k < mNumCenter; k++) 
         {  
            for (m = 0; m < mDim; m++) 
            {  
               mMeans[k][m] = 0.0;
            }
         }

         // calculate new means of centroids
         if (mUseKernel) 
         {
            // Kernel K means clustering

            // We don't need this step for kernel K means since it is handled in estep
            // However, I'm wondering if it might give us to get the information on 
            // the centroid center. However, it is not easy to get in kernel K means
            // since it is the center in the feature space .... 

         }
         else
         {
            // K means clustering
            
            for (n = 0; n < mDataNum; n++) 
            {
               for (m = 0; m < mDim; m++) 
               {  
                  mMeans[mAssign[n]][m] += mData[n][m];
               }
            }

            for (k = 0; k < mNumCenter; k++) 
            {
               if (mCount[k] > 0) 
               {  
                  for (m = 0; m < mDim; m++) 
                  {  
                     mMeans[k][m] /= mCount[k];
                  }  
               }
            }

            if (locdbg) 
            {
               Mout << "Means after update" << std::endl;
               for (k = 0; k < mNumCenter; k++)
               {
                  Mout << " Mean " << k << std::endl;
                  Mout << mMeans[k] << std::endl;
               }
            }
         }
      }


      /***
       *      
       *  @brief Optimizes the K means clustering 
       *      
       ***/
      void optimize(int maxiter, bool verbose = false)
      {
         bool converged = false;
         int nchg;
         int iter = 0;

         for (iter = 0; iter < maxiter; iter++)
         {
            nchg = estep();
            mstep(); 

            Mout << "  iter " << iter << " nchg " << nchg << std::endl;

            if (verbose) 
            {
               Mout << "Assignment " << mAssign << std::endl;
            }

            if (nchg == 0) 
            {
               converged = true;
               break;
            }
         }

         if (converged) 
         {
            Mout << "  K-means clustering converged in " << iter << " cycles" << std::endl;
         }
         else
         {
            Mout << "  K-means clustering NOT converged in " << iter << " cycles" << std::endl;
         }
         Mout << std::endl;

         Mout << "Assignment " << mAssign << std::endl;

      }

      /***
       *      
       *  @brief Returns the center of the centroid k 
       *      
       ***/
      std::vector<T> GetMean(int k)
      {
         return mMeans[k];
      }

      /***
       *      
       *  @brief Returns the center of the centroids 
       *      
       ***/
      std::vector<std::vector<T>> GetMean()
      {
         return mMeans;
      }

      /***
       *      
       *  @brief Returns the cluster assignment
       *
       *  Purpose: Returns the cluster assignment. index in data -> Cluster No. 
       **/
      std::vector<int> GetAssignment()
      {
         return mAssign;
      }

      /***
       *      
       *  @brief Returns indices of empty clusters
       *
       **/
      std::vector<int> GetEmptyCluster()
      {
         std::vector<int> isempty;

         for (int icluster = 0; icluster < mNumCenter; icluster++)
         {
            if( !(std::find(mAssign.begin(), mAssign.end(), icluster) != mAssign.end()) ) 
            {
               isempty.push_back(icluster); 
            }
         }

         return isempty;
      }

   private:

      void SetupGramMatrix()
      {
         if (mHasGramMat == false)
         {
            mGramMat.reset(new T[mDataNum*mDataNum]);
            mDistMat.reset(new T[mDataNum*mNumCenter]);

            // Not so nice that we need to create a temporary copy of the data...
            std::vector<MLDescriptor<T>> data;
            for (In idata = 0; idata < mData.size(); idata++)
            {
               MLDescriptor<T> point(mData[idata], MLDescriptor<T>::SCALAR);
               data.push_back(point);
            }

            std::vector<T> noise(mDataNum,C_0);
            mKernel.Compute(mGramMat, data, data, noise, 0, data.size(), data.size(), true);

            mHasGramMat = true;
         }
      }

};


#endif /* GAUPRO_INCLUDED */
