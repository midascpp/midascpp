#ifndef KERNELBASE_INCLUDED
#define KERNELBASE_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#if defined(_OPENMP)
#include <omp.h>
#endif

#include "mlearn/MLDescriptor.h"      
#include "mlearn/YIntermed.h"
#include "util/CallStatisticsHandler.h"

template <class T>
class GPKernelBase
{

   public:

      ///> Default Constructor
      GPKernelBase() = default;

      ///> Destructor
      virtual ~GPKernelBase() = default;

      virtual T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const Nb& noise
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         MIDASERROR("Base class was called!");
         return static_cast<T>(0.0);
      }

      virtual T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         MIDASERROR("Base class was called!");
         return static_cast<T>(0.0);
      }

      virtual std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         )
      {
         MIDASERROR("Base class was called!");
         std::vector<T> vec;
         return vec;
      }

      virtual void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& pointsi
         ,  const MLDescriptor<T>& pointsj
         )
      {
         MIDASERROR("Base class was called!");
      }

      virtual std::string GetName() const
      {
         MIDASERROR("Base class was called!");
         return "";
      }

      int ValidateArguments
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         )
      {
         int nsize = 0;

         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         return nsize;
      }

      virtual bool HasHyperParamDeriv() const   {return false;}
      virtual bool HasGradient() const         {return false;}
      virtual bool HasHessian() const          {return false;}

      virtual std::vector<T> GetHyperParameters() const
      {
         MIDASERROR("Base class was called!");
         std::vector<T> vec;
         return vec;
      }

      virtual void SetHyperParameters(const std::vector<T>& hparam)
      {
         MIDASERROR("Base class was called!");
      }
};


#endif /* KERNEL_INCLUDED */
