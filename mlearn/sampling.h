
#ifndef MLSAMPLE_INCLUDED
#define MLSAMPLE_INCLUDED


namespace mlearn
{

template <class T>
std::vector<In> Sample
   (  const std::unique_ptr<T[]>& aDistMat
   ,  const In& aNumCenter 
   ,  const In& aNumData
   )
{

   const bool locdbg = true;

   In NumCenter = aNumCenter;

   if (aNumCenter > aNumData) 
   {
      MidasWarning("Sampling: Number of samples larger than data set. I will fall back to selecting all");
      NumCenter = aNumData;
   }


   std::vector<In> centers;
   centers.reserve(NumCenter);

   centers.push_back(0); // We always add the first point

   std::list<In> not_selected;
   for (In i = 1; i < aNumData; i++) {not_selected.push_back(i);}

   // Get Maximal diagonal element. For a normalized kernel this will be one
   // Note: For most kernel the diagonal elements should be the same, 
   //       but you could add an additional noise hyper parameter which makes them
   //       different. Therefore this loop is kept
   T dmax = aDistMat[0];
   for (In i = 1; i < aNumData; i++) {dmax = std::max(dmax,aDistMat[i*aNumData + i]);}

   do 
   {
      //
      // Get average distance to centers
      //
      std::vector<std::pair<In,T>> DistVec;
      for (auto const& idx : not_selected) 
      {
         T dist = C_0;
         for (In k = 0; k < centers.size(); k++)
         {
            In j = centers[k];
            In i = idx;
            dist += aDistMat[i*aNumData + j];
         }
         dist /= centers.size();

         // The kernel has high value if structures are most similar, therefore
         // we take the difference from the maximum value to enlarge diversity 
         dist = dmax - dist;

         DistVec.push_back(std::make_pair(idx, dist));
      }

      //
      // sort distances and pick largest one to add and remove index from not_selected list
      //
      std::sort(DistVec.begin(), DistVec.end(), [](const std::pair<In, T> &a,const std::pair<In, T> &b) {return a.second > b.second; }  );
      
      In iadd = DistVec[0].first;

      centers.push_back(iadd);
      not_selected.remove(iadd);

      if (locdbg)
      {
         Mout << "Distance " <<  DistVec[0].second << std::endl;
      }

   } while (centers.size() < NumCenter);

   return centers;
}

} // namespace mlearn

#endif /* MLSAMPLE_INCLUDED */

