/**
************************************************************************
* 
* @file                MLTask.h 
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   User Interface to carry out different ML tasks
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MLTASK_H
#define MLTASK_H

void MLDrv();

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
//#include "input/ElecBasDef.h"
#include "mlearn/GauPro.h"
#include "mlearn/GauMixMod.h"
#include "mlearn/Kmeans.h"
#include "geoopt/GeoDatabase.h"

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

//#include "mlearn/GPKernel.h"

class MLTask
{
  private:

   std::vector<In> mNa;
   std::vector<In> mNb;
   std::vector<In> mNc;

   std::string mDatabase;     ///> File for the training points
   std::string mCoordFile;    ///> File for the test point

   bool mGhost = false;

   bool mDoGauPro     = false;
   Nb   mGPRShift     = 0.0;

   In   mNumInducingPoints = -1;
   In   mNumLayer = 1;

   bool mDoSOAP       = false;

   bool mDoOnlyIcoord = false;

   bool mOnlySimCheck = false;

   bool mDoKmeans    = false;
   int  mNumCenter = 1;

   bool mDoHOpt = true;

   In mMaxIter;
   Nb mGradEps;
   Nb mFuncEps;

   bool mDoGauMixMod = false;
   int  mNumMix = 1;

   CoordType mCoordType;

   bool mSaveCovar = false;
   bool mReadCovar = false;

   bool mSaveWeights = false;
   bool mReadWeights = false;

   std::string mFileCovarSave;
   std::string mFileCovarRead;

   std::string mFileWeightsSave;
   std::string mFileWeightsRead;

   std::vector<std::string> mKernelType;

   bool mNormalizeKernel = false;
  
   bool mAdaptNoise = false;

   bool mUseRegInv = false;

   Nb mRegEps = 1e-10;

   std::string mCovAlgoType = "CHOL";

   std::string mMeanFunction = "ZERO";
   std::vector<std::string> mMeanFunctionFiles;

   Nb mNoise = 1e-8;

   int mLmax = 8;

   int mPredictMode = 0;

   std::string mIcoordDef;
   bool mUseIcoordDef = false;

   bool mDumpIcoordDef = false;
   std::string mDumpIcoordDefFile = "icoord.def";

   bool mScaleSigma2 = false;

   bool mDoSample = false;
   In mNumSample;

   bool mPrintConditionNumber = false;

   void SimCheck();
   void GPR();
   void SOAP();
   void GenerateDescriptor();
   void DoKmeans();
   void DoGauMixMod();
   void Sample();


  public:
   MLTask(MLCalcDef* apCalcDef);      ///< Constructor 
   MLTask();                          ///< Constructor 

   ///> Keep copy constructor
   MLTask(const MLTask&)  = default;
   ///> Keep move constructor
   MLTask(MLTask&&)       = default;

   ///> Destructor
   ~MLTask() = default;

   void Run();                            

   void Initialize(MLCalcDef* apCalcDef);

   GauPro<Nb> CreateGPR
      (  const std::string& 
      ,  GPMeanFunction<Nb>& 
      ,  const std::string& 
      ,  const bool&
      ,  const bool&
      ,  const Nb& 
      ) const;

   GauPro<Nb> CreateGPR
      (  const std::string& 
      ,  GPMeanFunction<Nb>& 
      ,  const std::string& 
      ,  std::vector<In>&
      ,  std::vector<In>&
      ,  std::vector<In>&
      ,  const bool&
      ,  const bool&
      ,  const Nb& 
      ) const;

   GauPro<Nb> CreateGPR
      (  GeoDatabase& 
      ,  GPMeanFunction<Nb>& 
      ,  const std::string&
      ,  const bool& 
      ,  const bool&
      ,  const Nb& 
      ) const;


};



#endif
