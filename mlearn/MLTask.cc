/**
************************************************************************
* 
* @file                MLTask.cc
*
* Created:             03-07-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Implementing class for various 
*                      Machine learning tasks
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>
#include<list>
#include<algorithm>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/GeoOptCalcDef.h"
#include "mlearn/MLTask.h"

#include "pes/molecule/MoleculeFile.h"
#include "mmv/MidasMatrix.h"

#include "mlearn/GauMixMod.h"
#include "mlearn/Kmeans.h"
#include "mlearn/mlutil.h"
//#include "mlearn/svm.h"
#include "mlearn/MLDescriptor.h"

#include "geoopt/geoutil.h"



void MLTask::Initialize(MLCalcDef* apCalcDef)
{
   mGhost = false;

   mDatabase  = apCalcDef->GetDatabase();
   mCoordFile = apCalcDef->GetCoordFile();

   // Modus for the calculation
   mDoGauPro  = apCalcDef->GetCompModeGauPro();

   mDoKmeans     = apCalcDef->GetUseKmeans();
   mNumCenter    = apCalcDef->GetNumCenter();   

   mDoGauMixMod  = apCalcDef->GetUseGauMixMod();
   mNumMix       = apCalcDef->GetNumMix();   

   // Just for testing (remove later!!)
   mDoSOAP = apCalcDef->GetCompModeSOAP();

   // When not invoked for optimization
   mDoOnlyIcoord = apCalcDef->GetOnlyCoordAna();
   mOnlySimCheck = apCalcDef->GetOnlySimCheck();

   // Stuff for the GPR
   mNumLayer = apCalcDef->GetNumLayer();

   if (mNumLayer == 0)
   {
      // Default Kernel
      mNumLayer = 1;
      mKernelType.push_back("SCEXP");
   }
   else
   {
      for (In ikern = 0; ikern < mNumLayer; ikern++)
      {
         mKernelType.push_back(apCalcDef->GetKernel(ikern));
      }
   }

   mSaveCovar     = apCalcDef->GetSaveCovar();
   mReadCovar     = apCalcDef->GetReadCovar();
   mFileCovarSave = apCalcDef->GetFileCovarSave();
   mFileCovarRead = apCalcDef->GetFileCovarRead();

   mMeanFunction = apCalcDef->GetMeanFunction();
   mMeanFunctionFiles = apCalcDef->GetMeanFunctionFiles();

   // Coordinates
   mCoordType = apCalcDef->GetCoordinateType();

   // noise level in GPR
   mNoise = apCalcDef->GetNoise();

   // Set L value
   mLmax = apCalcDef->GetLmax();

   // Set if hyper parameters should be optimized
   mDoHOpt = apCalcDef->GetDoHOpt();

   // Set parameter for hyper parameter optimization
   mMaxIter = apCalcDef->GetMaxIter();
   mGradEps = apCalcDef->GetGradEps();
   mFuncEps = apCalcDef->GetFuncEps();

   // Set the prediction mode e.g. (energy or energy + gradient ord energy + gradient + hessian)
   mPredictMode = apCalcDef->GetPredictMode();

   // Which Algorithm we should use for decomposing the Co-variance matrix
   mCovAlgoType = apCalcDef->GetCoVarAlgo();

   mIcoordDef = apCalcDef->GetIcoordDefFile();
   mUseIcoordDef = apCalcDef->UseIcoordDefintion();

   mDumpIcoordDef = apCalcDef->GetDumpIcoordDef();
   mDumpIcoordDefFile = apCalcDef->GetDumpIcoordDefFile();

   mGPRShift = apCalcDef->GetGPRShift();

   mNumInducingPoints = apCalcDef->GetNumInducingPoints();


   mSaveWeights = apCalcDef->GetSaveWeights();
   mReadWeights = apCalcDef->GetReadWeights();
   mFileWeightsSave = apCalcDef->GetFileWeightsSave();
   mFileWeightsRead = apCalcDef->GetFileWeightsRead();

   mNormalizeKernel = apCalcDef->GetmNormalizeKernel();

   mAdaptNoise = apCalcDef->GetmAdaptNoise();

   mUseRegInv = apCalcDef->GetmUseRegInv();

   mRegEps = apCalcDef->GetmRegEps();

   mScaleSigma2 = apCalcDef->GetScaleSigma2();

   mNumSample = apCalcDef->GetNumberOfSamples();
   mDoSample = apCalcDef->GetCompModeSample();

   mPrintConditionNumber = apCalcDef->GetPrintConditionNumber();
}

/**
*  Constructor 
* */
MLTask::MLTask(MLCalcDef* apCalcDef)
{
   Initialize(apCalcDef);
}

/**
*  Constructor 
* */
MLTask::MLTask()
{
   mGhost = true;
}


void MLTask::SimCheck()
{

   GeoDatabase database(this->mCoordType, this->mDatabase);
   GeoDatabase molecule(this->mCoordType, this->mCoordFile); 
   
   if (database.ContainsMolecule(1e-3, molecule, 0) )
   {
      Mout << "Molecule already in database" << std::endl;
   }
   else
   {
      Mout << "Molecule is unique" << std::endl;
   }

}

void MLTask::GenerateDescriptor()
{

   GeoDatabase molecule(this->mCoordType, this->mCoordFile); 
   
   Mout << "coordinates" << std::endl;
 
   int ndata = molecule.GetSize();
   std::ofstream molfile;
   string file = "intern_coord";
   
   molfile.open (file, ios::out);

   for (int imol = 0; imol < ndata; imol++)
   {
      Mout << " Molecule " << imol << std::endl;
      molecule.PrintMolecule(imol, 0.0, false);
      molecule.PrintMolecule(molfile, imol, 0.0, false);
   }
   
   molfile.close();

}


void MLTask::SOAP()
{
   const bool locdbg = false;
   const Nb Ang2Bohr = C_1 / C_TANG;

   Mout << "Entering the SOAP testing..." << std::endl;

   int lmax = mLmax; 

   Mout << "lmax = " << lmax << std::endl;

   // Read in a database as a vector of MLDescriptor
   std::vector<MLDescriptor<Nb>> database;   
   std::vector<Nb> ener;
   //read in the whole xyz file of molecules
   std::ifstream coord_database;
   coord_database.open(this->mDatabase);
   if(!coord_database){
      Mout << "The file could not be accessed!" << std::endl;
   }

   std::string s;

   // read atom lines
   In n_atoms = I_0;
   while(midas::input::GetLine(coord_database, s) )
   {
      Nb x, y, z, energy;
      std::string label, dummy1, dummy2;
      In natoms = 0;
      
      // TODO: We do no sanity checks and assume the file is in the correct format

      // 1) read number of Coordinates
      {
         std::istringstream str_stream(s);
         str_stream >> natoms;
      }

      if (locdbg) Mout << "natoms " << natoms << std::endl;

      // 2) read energy for current structure
      {
         midas::input::GetLine(coord_database, s);
         std::istringstream str_stream(s);
         str_stream >> dummy1 >> dummy2 >> energy;
      }

      ener.push_back(energy);

      // 3) read coordinates
      std::vector<Nb> positions;
      std::vector<std::string> elements;
      for (In idx = 0; idx < natoms; idx++)
      {
         midas::input::GetLine(coord_database, s);

         std::istringstream str_stream(s);
         str_stream >> label >> x >> y >> z;
 
         positions.push_back(x * Ang2Bohr);
         positions.push_back(y * Ang2Bohr);
         positions.push_back(z * Ang2Bohr);

         std::string sym = label;
         std::transform(sym.begin(), sym.end(), sym.begin(), tolower);

         elements.push_back(sym);
      }

      if (locdbg) Mout << "Add new MLDescriptor" << std::endl;

      MLDescriptor<Nb> newmldp(positions, MLDescriptor<Nb>::ATOMISTIC, lmax, elements); 

      ////TODO Check if newmldp is already in database
      bool acceptit = true;
      //for (imol = 0; imol < database.size(); imol++)
      //{
      //   bool issimilar = database[imol].issimilar(newmldp);

      //   if (issimilar)
      //   {
      //      acceptit = false;
      //      break;
      //   }
      //}

      if (acceptit) database.push_back(newmldp);
   }

   if (locdbg) Mout << "Read test point" << std::endl;
   
   // Test point
   MLDescriptor<Nb> molecule(this->mCoordFile,MLDescriptor<Nb>::ATOMISTIC,lmax);

   int ndata = database.size();

   std::vector<Nb> noise(ndata,1e-7);

   if (locdbg) Mout << "Generate Kernel" << std::endl;

   GPKernel<Nb>::KernelType ktype = GPKernel<Nb>::KernelType::SOAP;

   std::vector<Nb> hparam = {0.6};

   GPKernel<Nb> kern(hparam, ktype);

   if (locdbg) Mout << "Setup Gaussian Process" << std::endl;

   Nb minener = 0.0;
   GPConstMean<Nb> GPZERO(C_0);
   GauPro<Nb> gp(kern, &GPZERO, GauPro<Nb>::CovAlgo::USESVD);

   if (locdbg) Mout << "Add training data" << std::endl;
   gp.SetData(database, ener, noise);

   std::vector<MLDescriptor<Nb>> points;
   points.push_back(molecule);

   // predict energy
   if (locdbg) Mout << "Predict" << std::endl;
   std::tuple<std::vector<Nb>, std::vector<Nb>> gpout = gp.Predict(points);

   std::vector<Nb> energuess = std::get<0>(gpout);
   std::vector<Nb> varguess  = std::get<1>(gpout);   
   
   Mout << "Predicted energy: " << energuess[0] << " +/- " << std::abs(varguess[0]) << std::endl;
}

/**
* Create a Gaussian Process 
* */
GauPro<Nb> MLTask::CreateGPR
   (  const std::string& aFileDataBase
   ,  GPMeanFunction<Nb>& aMeanFunction
   ,  const std::string& aFileHyperParam
   ,  const bool& verbose = false
   ,  const bool& override_noise = false
   ,  const Nb& aNoiseVal = 1e-8 
   ) const
{
   std::vector<In> na;
   std::vector<In> nb;
   std::vector<In> nc;

   return CreateGPR( aFileDataBase
                   , aMeanFunction
                   , aFileHyperParam
                   , na
                   , nb
                   , nc
                   , verbose
                   , override_noise
                   , aNoiseVal
                   );
}

/**
* Create a Gaussian Process 
* */
GauPro<Nb> MLTask::CreateGPR
   (  const std::string& aFileDataBase
   ,  GPMeanFunction<Nb>& aMeanFunction
   ,  const std::string& aFileHyperParam
   ,  std::vector<In>& aNa
   ,  std::vector<In>& aNb
   ,  std::vector<In>& aNc
   ,  const bool& verbose = false
   ,  const bool& override_noise = false
   ,  const Nb& aNoiseVal = 1e-8 
   ) const
{
   bool use_idef = mUseIcoordDef;
   std::vector<InternalCoord> icoorddef;

   if (use_idef) 
   {
      ifstream fcoord(mIcoordDef);
      if (fcoord.good())
      {
         icoorddef = ReadInternalCoordinates(fcoord); 
         fcoord.close();
      }
      else
      {
         MIDASERROR("Could not determine file with defintion of internal coordinates!");
      }
   }

   // training data 
   GeoDatabase geodb(this->mCoordType, aFileDataBase, use_idef, icoorddef);

   // Get connectivity
   if (this->mCoordType == geo_coordtype::MINT || 
       this->mCoordType == geo_coordtype::DIST || 
       this->mCoordType == geo_coordtype::ZMAT
      )
   {
      geodb.GetConnectivity(aNa,aNb,aNc,0);
   }

   return CreateGPR( geodb
                   , aMeanFunction
                   , aFileHyperParam
                   , verbose
                   , override_noise
                   , aNoiseVal
                   );
}

/**
* Create a Gaussian Process 
* */
GauPro<Nb> MLTask::CreateGPR
   (  GeoDatabase& aGeoDatabase
   ,  GPMeanFunction<Nb>& aMeanFunction
   ,  const std::string& aFileHyperParam
   ,  const bool& verbose = false
   ,  const bool& override_noise = false
   ,  const Nb& aNoiseVal = 1e-8 
   ) const 
{
   const bool useClustering = false;
   const bool doPrtTrainSet = false;

   //-------------------------------------------------+
   // Get Training data 
   //-------------------------------------------------+

   std::vector<vector<Nb>> coord = aGeoDatabase.GetTrajectory();
   std::vector<Nb>         ener  = aGeoDatabase.GetEnergies();

   // Use clustering for analysis purposes
   std::vector<vector<Nb>> coord_subset;
   std::vector<Nb> ener_subset;
   if (useClustering && coord.size() >= 1000)  // Clustering only gives something when much data is available 
   {
      int aMaxIter = 1000;
      int ncenter = coord.size() / 4;

      Kmeans<Nb> Clusters(coord,ncenter);
      Clusters.optimize(aMaxIter,true);  

      coord_subset.resize(ncenter);
      ener_subset.resize(ncenter);

      std::vector<int> assign = Clusters.GetAssignment();
      for (int imol = 0; imol < coord.size() ; imol++)
      {
         int icluster = assign[imol];
         coord_subset[icluster] = coord[imol];
         ener_subset[icluster] = ener[imol];
      }
   }


   if (mDumpIcoordDef) 
   {
      std::vector<InternalCoord> icoord = aGeoDatabase.GetIntCoord(0);
      std::string ficoorddef = gMainDir + "/icoord.def";
      std::ofstream fout;
      fout.open(ficoorddef);
      WriteInternalCoordinates(fout,icoord);
      fout.close();
   }   

   std::tuple<std::vector<MLDescriptor<Nb>>,std::vector<Nb>> TrainSet = aGeoDatabase.Convert2Descriptors();
   std::vector<MLDescriptor<Nb>>  database = std::get<0>(TrainSet);
   std::vector<Nb>                property = std::get<1>(TrainSet); 
   int ndata = property.size(); 

   if (doPrtTrainSet) 
   {
      In nmol = coord.size();
      for (int i = 0; i < nmol; i++)
      {
         Mout << "Energy " << ener[i] << " Coordinates " << coord[0].size()  << std::endl;
         aGeoDatabase.PrintMolecule(Mout, i, 0.0 ,false); 
      }
   }

   // sort data according to similarity to target structure
   if (verbose) 
   {
      std::vector<Nb> rmsd;
      try
      {
         bool use_idef = mUseIcoordDef;
         std::vector<InternalCoord> icoorddef;

         if (use_idef) 
         {
            ifstream fcoord(mIcoordDef);
            if (fcoord.good())
            {
               icoorddef = ReadInternalCoordinates(fcoord); 
               fcoord.close();
            }
            else
            {
               MIDASERROR("Could not determine file with defintion of internal coordinates!");
            }
         }

         GeoDatabase molecule(this->mCoordType, this->mCoordFile, use_idef, icoorddef);
         rmsd = aGeoDatabase.GetRMSD(molecule, 0);

         Mout << "Similarity to target structure:" << std::endl;
         Mout << std::endl;
         for (int imol = 0; imol < rmsd.size(); imol++)
         {
            Mout << "  Structure " << std::setw(4) << imol << " RMSD = " << rmsd[imol] << std::endl;
         }
         Mout << std::endl;
      }
      catch(...)
      {
         MIDASERROR("Problem with reading file for test point");
      }
   }

   if (verbose) 
   {
      Mout << " Finished reading in training data: I have " << ndata << " points"  << std::endl;
   }

   //-------------------------------------------------+
   // Get Hyper parameters
   //-------------------------------------------------+

   int nintern = coord[0].size();   
   std::vector<In> nhparam = mlearn::GetNumberOfHyperParameters<Nb>(mKernelType,nintern,mNumLayer); 

   // read initial hyper parameters if avaialable
   std::ifstream infile(aFileHyperParam);
   bool havehparam = infile.good();

   std::vector<std::vector<Nb>> hparam = mlearn::GetInitialHyperParameter<Nb>(nhparam,havehparam,mKernelType,aFileHyperParam,coord,ener,mGPRShift);

   if (verbose)
   {
      for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
      {
         Mout << std::endl << " Hyper Parameter: " << "(Layer " << ilayer << ")" << std::endl;
         for (In ihp = 0; ihp < hparam[ilayer].size(); ihp++)
         {
            Mout << "  " << std::setw(4) << ihp << " "
                         << (hparam[ilayer][ihp] >= 0 ? " ":"") << std::setw(10) << hparam[ilayer][ihp] << std::endl;
         }
         Mout << std::endl;
      }
   }

   //-------------------------------------------------+
   // Create Kernel
   //-------------------------------------------------+

   std::vector<GPKernel<Nb>> KernList(mNumLayer);

   // define kernel
   for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
   {
      GPKernel<Nb>::KernelType ktype = mlearn::GetKernelTypeFromString<Nb>(mKernelType[ilayer]);
      GPKernel<Nb> kern(hparam[ilayer], ktype, mNormalizeKernel);

      KernList[ilayer] = kern;
   }

   //-------------------------------------------------+
   // Create Gaussian Process and add data
   //-------------------------------------------------+

   // define Gaussian process
   GauPro<Nb>::CovAlgo covalgo = mlearn::GetCovAlgoFromString<Nb>(mCovAlgoType);
   GauPro<Nb> gp( KernList
                , &aMeanFunction
                , covalgo
                , mGPRShift
                , mScaleSigma2
                , mNumLayer
                , mAdaptNoise
                , mUseRegInv
                , mRegEps
                );

   // Noise term
   Nb noiseval = mNoise;
   if (override_noise) noiseval = aNoiseVal;
   std::vector<Nb> noise(ndata, noiseval);

   // Add data
   gp.SetData(database, property, noise);

   // Set number of inducing points if Sparse GPR is used
   if (covalgo == GauPro<Nb>::CovAlgo::USESPARSE)
   {
      In numpoints = property.size();
      if (mNumInducingPoints > 0)
      {
         numpoints = mNumInducingPoints;
      } 
      gp.SetSizeOfSubSet(numpoints);
   }

   return gp;
}

/**
* Do Gaussian Process Regression 
* */
void MLTask::GPR()
{

   std::unique_ptr<Nb[]> Weights(nullptr);

   bool use_idef = mUseIcoordDef;
   std::vector<InternalCoord> icoorddef;

   if (use_idef) 
   {
      ifstream fcoord(mIcoordDef);
      if (fcoord.good())
      {
         icoorddef = ReadInternalCoordinates(fcoord); 
         fcoord.close();
      }
      else
      {
         MIDASERROR("Could not determine file with defintion of internal coordinates!");
      }
   }

   // training data 
   GeoDatabase geodb(this->mCoordType, this->mDatabase, use_idef, icoorddef);

   // Get connectivity
   if (this->mCoordType == geo_coordtype::MINT || 
       this->mCoordType == geo_coordtype::DIST || 
       this->mCoordType == geo_coordtype::ZMAT
      )
   {
      geodb.GetConnectivity(mNa,mNb,mNc,0);
   }

   // file with hyper parameters
   std::string fhparam = gMainDir + "/hparam.in";

   // define mean function
   GPMeanFunction<Nb> GPMean; 

   if (mMeanFunction == "ZERO")
   {
      GPMean = GPConstMean<Nb>(C_0);
   }
   else if (mMeanFunction == "HESSIAN")
   {
      std::vector<Nb> ener = geodb.GetEnergies();
      
      Nb energy = *(std::min_element(ener.begin(), ener.begin() + ener.size()));
      std::string fhess = gMainDir + "/" + mMeanFunctionFiles[0];  
      std::vector<Nb> xyz_cart = geodb.GetCartCoord(0);

      std::vector<InternalCoord> interncoord = geodb.GetIntCoord(0);

      int natoms  = geodb.GetNumAtoms();
      int ncart   = 3 * natoms;
      int nintern = interncoord.size();

      GPMean = GPHessMean<Nb>(energy,fhess,true,xyz_cart,interncoord,nintern,ncart,natoms);   
   }
   else
   {
      MIDASERROR("Unknown mean function for Gaussian Process");
   }

   // Create Gaussian Process
   GauPro<Nb> gp = CreateGPR( geodb
                            , GPMean
                            , fhparam
                            , true
                            );

   // Train (or use results from previous training)
   if (! mReadCovar && !mReadWeights)
   {
      if (mDoHOpt)
      {
         gp.OptimizeHparams( true
                           , Mout
                           , mMaxIter
                           , mGradEps
                           , mFuncEps
                           );

         // dump optimized set to file
         std::ofstream outfile("hparam.in"); 
         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         {
            std::vector<Nb> hparam_opt = gp.GetHyperParameters(ilayer);
            for (int iparam = 0; iparam < hparam_opt.size(); iparam++)
            {
               outfile << std::scientific << std::setprecision(12) << hparam_opt[iparam] << std::endl;
            }
         }
         outfile.close();
      }
   }
   else
   {
      if (mReadCovar) 
      {
         Mout << "  Read Covariance Matrix:" << std::endl;
         Mout << "  -----------------------" << std::endl;
         Mout << "  Marginal likelihood: " << gp.GetMarginalLikelihood() << std::endl;
         gp.ReadCovarianceMatrix(mFileCovarRead);
         MidasWarning("Covariance matrix read from file. Only very limited sanity checks. You know what you are doing!");
      }
      if (mReadWeights)
      {
         gp.ReadWeights(Weights,mFileWeightsRead);
      }
   }

   if (mSaveCovar)
   {
      std::ofstream outcovar;
      outcovar.open (mFileCovarSave, ios::out | ios::trunc);
      gp.StoreCovarianceMatrix(outcovar);
      outcovar.close();
   }

   if (mSaveWeights)
   {
      gp.StoreWeights(mFileWeightsSave);
   }

   //................................................... 
   // Predict energy via GPR 
   //...................................................
   Mout << "..................................................................." << std::endl;
   Mout << "                  Predict energy via GPR                           " << std::endl;
   Mout << "..................................................................." << std::endl;
   Mout << std::endl;


   // read coordinates
   GeoDatabase molecule( this->mCoordType
                       , this->mCoordFile
                       , mNa
                       , mNb
                       , mNc
                       , use_idef
                       , icoorddef
                       , C_0
                       );

   // new coordinates and also energy..
   std::vector<std::vector<Nb>>  xnew    = molecule.GetTrajectory(); 
   std::vector<Nb>               newener = molecule.GetEnergies();   
   std::vector<string>           symbols = molecule.GetSymbols(0);     

   int npoints = xnew.size();
   std::vector<MLDescriptor<Nb>> points;
   points.reserve(npoints);

   for (int imol = 0; imol < xnew.size(); imol++)
   {
      MLDescriptor<Nb> point(xnew[imol],MLDescriptor<Nb>::SCALAR);
      points.push_back(point);
   }

   //------------------------------------------------------------------------------+
   // Do prediction
   //------------------------------------------------------------------------------+
   int imode = mPredictMode;
   int imol  = 0;
   int nintern = xnew[0].size(); 

   std::tuple<std::vector<Nb>, std::vector<Nb>> gpout = gp.Predict(  points
                                                                  ,  imode
                                                                  ,  false
                                                                  ,  mReadWeights
                                                                  ,  Weights
                                                                  );

   std::vector<Nb> guess     = std::get<0>(gpout);
   std::vector<Nb> varguess  = std::get<1>(gpout);   

   ios::fmtflags mflag( Mout.flags() );

   // Open file to dump predictions
   std::ofstream PredictionFile;
   PredictionFile.open("GPR_Predict.out");

   // Loop over predictions for the verious points
   for (In imol = 0; imol < points.size(); imol++)
   {
      int nprop = 1;
      int ndim = points[imol].size();
      if (imode > 0) nprop = 1 + ndim;
      if (imode > 1) nprop = 1 + ndim + ndim*ndim; 

      // Reset output format for Mout
      Mout.flags(mflag);

      Nb delta = guess[imol*nprop] - newener[imol];

      Mout << "Predicted energy: " << std::scientific << std::setprecision(12) 
           << guess[imol*nprop] 
           << " +/- " << std::abs(varguess[imol*nprop]) 
           << " Difference to correct val: " <<  delta << std::endl;

      molecule.PrintMolecule(imol, varguess[imol*nprop], true);

      Mout << std::endl << "Internal Coordinates" << std::endl << std::endl;
      molecule.PrintMolecule(imol, varguess[imol*nprop], false);

      // Save geom. info in prediction file
      molecule.PrintMolecule(PredictionFile, imol, varguess[imol*nprop], true);

      if (imode > 0) 
      {
         const bool HasInternalCoord = molecule.UseInternalCoordinates();

         //----------------------------------------------------------------------+
         // Print predicted molecular gradient in internal coordinates
         //----------------------------------------------------------------------+
         int ioff = imol*nprop;

         if (HasInternalCoord)
         {
            std::vector<InternalCoord> icoord = molecule.GetIntCoord(imol);

            Mout << std::endl << "Predicted Gradient (internal coordinates): " << std::endl << std::endl;
            for (int idx = 0; idx < ndim; idx++)
            {
               Nb dgrad = guess[idx + ioff + 1];
               Mout << " " << std::setw(4) << idx << " " 
                    << (dgrad >= 0 ? " ":"") << dgrad 
                    << " itype " << icoord[idx].itype << " atoms " << icoord[idx].iconnect << std::endl;
            }

            //----------------------------------------------------------------------+
            // Print predicted molecular gradient in cartesian coordinates
            //----------------------------------------------------------------------+
            int ncart = 3 * molecule.GetNumAtoms();

            Mout << std::endl << "Predicted Gradient (Cartesian coordinates): " << std::endl << std::endl;
            std::vector<Nb> grad(ndim);
            std::copy(guess.begin() + ioff + 1, guess.begin() + 1 + ioff + ndim, grad.begin());

            std::vector<Nb> grad_cart(ncart);
            std::unique_ptr<Nb[]> bmat  = molecule.GetBMatrix(imol);

            char nn = 'N';
            int nvec = 1 ;
            Nb one  = 1.0;
            Nb zero = 0.0;

            // g_int = B g_x
            midas::lapack_interface::gemm( &nn, &nn, &ncart, &nvec, &nintern
                                         , &one, bmat.get(), &ncart
                                         , &grad[0], &nintern, &zero
                                         , &grad_cart[0], &ncart );

            std::vector<Nb> coord = molecule.GetCartCoord(imol);
            std::vector<Nb> amass = molecule.GetAtomicMasses(imol);
            //CleanGrad(grad_cart,coord,amass);

            std::vector<std::string> elem = molecule.GetSymbols(imol);

            PredictionFile << " Gradient " << molecule.GetNumAtoms() << std::endl;

            for (int iat = 0; iat < molecule.GetNumAtoms(); iat++)
            {
               Nb dx = grad_cart[iat*3 + 0];
               Nb dy = grad_cart[iat*3 + 1];
               Nb dz = grad_cart[iat*3 + 2];

               Mout << "  " << elem[iat] << "  "
                    << (dx >= 0 ? " ":"") << dx << " " 
                    << (dy >= 0 ? " ":"") << dy << " " 
                    << (dz >= 0 ? " ":"") << dz << std::endl;

               // Also save it in the Prediction file
               PredictionFile << " "
                    << (dx >= 0 ? " ":"") << dx * C_TANG << " " 
                    << (dy >= 0 ? " ":"") << dy * C_TANG << " " 
                    << (dz >= 0 ? " ":"") << dz * C_TANG << std::endl;
            }

         }
         else
         {
            //----------------------------------------------------------------------+
            // Print predicted molecular gradient in cartesian coordinates
            //----------------------------------------------------------------------+
            int ncart = 3 * molecule.GetNumAtoms();
            std::vector<std::string> elem = molecule.GetSymbols(imol);

            PredictionFile << " Gradient " << molecule.GetNumAtoms() << std::endl;

            Mout << std::endl << "Predicted Gradient (Cartesian coordinates): " << std::endl << std::endl;
            for (int iat = 0; iat < molecule.GetNumAtoms(); iat++)
            {
               Nb dx = guess[1 + iat*3 + 0];
               Nb dy = guess[1 + iat*3 + 1];
               Nb dz = guess[1 + iat*3 + 2];

               Mout << "  " << elem[iat] << "  "
                    << (dx >= 0 ? " ":"") << dx << " " 
                    << (dy >= 0 ? " ":"") << dy << " " 
                    << (dz >= 0 ? " ":"") << dz << std::endl;

               // Also save it in the Prediction file
               PredictionFile << " "  
                    << (dx >= 0 ? " ":"") << dx * C_TANG << " " 
                    << (dy >= 0 ? " ":"") << dy * C_TANG << " " 
                    << (dz >= 0 ? " ":"") << dz * C_TANG << std::endl;
            }
            
         }
         
      }

      if (imode > 1)
      {
         int ioff = imol*nprop;

         const bool HasInternalCoord = molecule.UseInternalCoordinates();

         // Make a copy of the coordinates
         std::vector<Nb> xyz      = molecule.GetCoordinates(imol);
         std::vector<Nb> xyz_cart = molecule.GetCartCoord(imol);

         int natoms  = molecule.GetNumAtoms();
         int nintern = molecule.GetCoordSize(imol);
         int ncart   = 3 * natoms;

         std::vector<Nb> qmass = molecule.GetCoordinateMasses(imol);
         std::vector<Nb> amass = molecule.GetAtomicMasses(imol);

         std::vector<InternalCoord> interncoord;
         if (HasInternalCoord) interncoord = molecule.GetIntCoord(imol);

         // Copy Gradient information
         std::vector<Nb> grad(nintern);
         for (int idx = 0; idx < nintern; idx++) 
         {
            grad[idx] = guess[idx + ioff + 1];
         }


         // Copy Hessian information
         std::unique_ptr<Nb[]> hessian(new Nb[nintern*nintern]);
         for (int idx = 1; idx <= nintern; idx++) 
         {
            for (int jdx = 1; jdx <= idx; jdx++)
            {
               Nb dval = guess[nintern + ioff + idx*(idx-1)/2 + jdx];
               hessian[(idx-1)*nintern + jdx-1] = dval;
               hessian[(jdx-1)*nintern + idx-1] = dval;
            }
         }

         Mout << std::endl << "Normal mode analysis:" << std::endl << std::endl;

         // Do normal mode analysis
         std::unique_ptr<Nb[]> hess_cart(new Nb[ncart*ncart]); 
         NormalModeAnalysis( xyz, xyz_cart, interncoord
                           , qmass, amass
                           , grad, hessian, hess_cart, true
                           , HasInternalCoord, false, natoms, nintern);

         // Save cartesian Hessian
         In nlines = (ncart + 5 - 1) / 5 * ncart; // No. of lines, which will be printed
         PredictionFile << " Hessian " << nlines << std::endl;

         for (In i = 0; i < ncart; i++)
         {
            In ic = 0;
            In maxcol = 0;
            In mincol = 0;           
 
            while (maxcol < ncart)
            {
               mincol = maxcol + 1;
               maxcol = std::min(maxcol+5, ncart);
               ic = ic + 1;

               PredictionFile << "   " << i + 1 << " " << ic << " ";
               for (In j = mincol - 1; j < maxcol; j++)
               {
                  Nb dval = hess_cart[i*ncart + j] * C_TANG;
                  PredictionFile << "  "  << (dval >= 0 ? " ":"") << dval << " ";
               }
               PredictionFile << std::endl;
            }
         }
      }

      // Reset output format for Mout
      Mout.flags(mflag);

   }

   PredictionFile.close(); 
}

void MLTask::DoKmeans()
{

   // Read in data 
   GeoDatabase geodb(this->mCoordType, this->mDatabase);
   std::vector<std::vector<Nb>> data = geodb.GetTrajectory();
   std::vector<std::vector<Nb>> means;   

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Start doing K means clustering  ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";
  
   Kmeans<Nb> Clusters(data,mNumCenter);

   char c='|';

   int ndata = data.size();
   int ndim  = data[0].size();

   Out72Char(Mout,'+','=','+');
   TwoArgOut(Mout, "Number of data points: ", 58, ndata, 10,c);
   TwoArgOut(Mout, "Dim. per data point:   ", 58, ndim,  10,c);
   Out72Char(Mout,'+','=','+');

   //Clusters.optimize(10000); 

}

void MLTask::DoGauMixMod()
{

   // Read in data 
   GeoDatabase geodb(this->mCoordType, this->mDatabase);
   std::vector<std::vector<Nb>> data = geodb.GetTrajectory();
   std::vector<std::vector<Nb>> means;   
   In dim = data[0].size();

   means.resize(mNumMix);
   for (In idx = 0; idx < mNumMix; idx++)
   {
      means[idx].resize(dim);
   }
   

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Start Gaussian Mixture Modell clustering  ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";
  
   char c='|';

   int ndata = data.size();
   int ndim  = data[0].size();

   Out72Char(Mout,'+','=','+');
   TwoArgOut(Mout, "Number of data points: ", 58, ndata, 10,c);
   TwoArgOut(Mout, "Dim. per data point:   ", 58, ndim,  10,c);
   Out72Char(Mout,'+','=','+');
  
   GauMixMod<Nb> Clusters(data,mNumMix);

   Clusters.optimize(100,1e-6); 

}

void MLTask::Sample()
{

   bool use_idef = mUseIcoordDef;
   std::vector<InternalCoord> icoorddef;

   if (use_idef) 
   {
      ifstream fcoord(mIcoordDef);
      if (fcoord.good())
      {
         icoorddef = ReadInternalCoordinates(fcoord); 
         fcoord.close();
      }
      else
      {
         MIDASERROR("Could not determine file with defintion of internal coordinates!");
      }
   }

   // training data 
   GeoDatabase geodb(this->mCoordType, this->mDatabase, use_idef, icoorddef);

   // Get connectivity
   if (this->mCoordType == geo_coordtype::MINT || 
       this->mCoordType == geo_coordtype::DIST || 
       this->mCoordType == geo_coordtype::ZMAT
      )
   {
      geodb.GetConnectivity(mNa,mNb,mNc,0);
   }

   std::vector<vector<Nb>> coord = geodb.GetTrajectory();
   std::vector<Nb>         ener  = geodb.GetEnergies();

   
   std::tuple<std::vector<MLDescriptor<Nb>>,std::vector<Nb>> TrainSet = geodb.Convert2Descriptors();
   std::vector<MLDescriptor<Nb>>  Xdata = std::get<0>(TrainSet);
   std::vector<Nb>                Ydata = std::get<1>(TrainSet); 

   In NumData = ener.size();

   // file with hyper parameters
   std::string fhparam = gMainDir + "/hparam.in";

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Start Sampling process  ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   //-------------------------------------------------+
   // Get Hyper parameters
   //-------------------------------------------------+

   if (mNumLayer > 1) MidasWarning("Only one layer supported in Sampling!");

   In ilayer = 0;

   int nintern = Xdata[0].size();   
   std::vector<In> nhparam = mlearn::GetNumberOfHyperParameters<Nb>(mKernelType,nintern,mNumLayer); 

   bool havehparam = false;
   std::string fparam;

   std::vector<std::vector<Nb>> hparam = mlearn::GetInitialHyperParameter<Nb>(nhparam,havehparam,mKernelType,fparam,coord,ener,mGPRShift);

   //-------------------------------------------------+
   // Create Kernel and Gram Matrix 
   //-------------------------------------------------+

   GPKernel<Nb>::KernelType ktype = mlearn::GetKernelTypeFromString<Nb>(mKernelType[ilayer]);
   GPKernel<Nb> kernel(hparam[ilayer], ktype, mNormalizeKernel);
  
   std::unique_ptr<Nb[]> kern(new Nb[NumData*NumData]);  
   std::vector<Nb> input_noise(NumData,mNoise);
   kernel.Compute(kern, Xdata, Xdata, input_noise, 0, NumData, NumData, true);

   //-------------------------------------------------+
   // Sample and postprocess 
   //-------------------------------------------------+

   std::vector<In> IndSubSet = mlearn::Sample(kern,mNumSample,NumData);

   for (In k = 0; k < std::min(mNumSample,NumData); k++)
   {
      In imol = IndSubSet[k];
      Mout << " Molecule " << imol << std::endl;
      geodb.PrintMolecule(imol, 0.0, false);
   }

   char c='|';

   Out72Char(Mout,'+','=','+');
   TwoArgOut(Mout, "Number of data points: ", 58, NumData, 10,c);
   Out72Char(Mout,'+','=','+');

}

/**
* Run Machine Learning task(s)
* */
void MLTask::Run()
{

   const bool locdbg = false;

   if (mDoOnlyIcoord || mOnlySimCheck)
   {

      if (mDoOnlyIcoord)
      {
         this->GenerateDescriptor();
      }

      if (mOnlySimCheck)
      {
         this->SimCheck(); 
      }

      return;
   }

   if ( mDoSample )
   {
      this->Sample();
   }

   if ( mDoGauPro ) 
   {
      this->GPR();
   }

   if ( mDoSOAP )
   {
      this->SOAP();
   }

   if ( mDoKmeans ) 
   {
      this->DoKmeans();
   }

   if ( mDoGauMixMod )
   {
      this->DoGauMixMod();
   }

}

