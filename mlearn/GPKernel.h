#ifndef KERNELDERIVED_INCLUDED
#define KERNELDERIVED_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "util/InterfaceOpenMP.h"

#include "mlearn/MLDescriptor.h"      
#include "mlearn/YIntermed.h"

#include "mlearn/GPKernelBase.h"
#include "mlearn/kernel/sqexp_kernel.h"
#include "mlearn/kernel/anova_kernel.h"
#include "mlearn/kernel/matern32_kernel.h"
#include "mlearn/kernel/matern52_kernel.h"
#include "mlearn/kernel/ouexp_kernel.h"
#include "mlearn/kernel/periodic_kernel.h"
#include "mlearn/kernel/polynom_kernel.h"
#include "mlearn/kernel/ratquad_kernel.h"
#include "mlearn/kernel/sc32_kernel.h"
#include "mlearn/kernel/sc52_kernel.h"
#include "mlearn/kernel/scexp_kernel.h"
#include "mlearn/kernel/sqexp_kernel.h"
#include "mlearn/kernel/soap_kernel.h"
#include "mlearn/kernel/rbf_kernel.h"


#include "util/CallStatisticsHandler.h"

template <class T>
class GPKernel : public GPKernelBase<T>
{
   public:

      enum KernelType { SQEXP, OUEXP, PERIODIC, RATQUAD, ANOVA, POLYNOM
                      , MATERN32, MATERN52
                      , SC32, SC52, SCEXP
                      , SOAP, RBF };

   private:

      KernelType mKernType;               ///> Type of the Kernel

      GPKernelBase<T>* mKernel = nullptr;    ///> Pointer to the kernel of type mKernType

      std::string mMyID = "NA";

      bool mNormalize = false;

   public:

      ///> Default Constructor
      GPKernel() = default;

      ///> Constructor 
      GPKernel
         (  std::vector<T>& aHparam
         ,  KernelType aKtype
         ,  const bool& aNormalize = false 
         )
      {
         // GS: the last argument (ID) is helpful for debugging
         init_kernel(aHparam, aKtype, aNormalize, "init");
      }

      /**
       *  @brief Initializes the used kernel  
       *
       *  Purpose: Initializes the used kernel to be used 
       *
       *  @param aHparam contains the hyper parameters for this kernel
       *  @param aKtype determines the type of the kernel e.g. sqaured exponential SQEXP
       *  @param aNormalize determines if the kernel should be normalized, which increases numerical stability but can affect accuracy
       *  @param aID is string holding an ID for the kernel. This is more a debug option.
       *         
       * */
      void init_kernel
         (  std::vector<T>& aHparam
         ,  KernelType aKtype
         ,  const bool& aNormalize = false 
         ,  const std::string& aID = "NA"
         )
      {
         mKernType = aKtype;
         mNormalize = aNormalize;
         mMyID = aID;

         switch (aKtype)
         {
            case SQEXP:
            {
               mKernel = new SQEXP_KERN<T>(aHparam);
               break;
            }
            case OUEXP:
            {
               mKernel = new OUEXP_KERN(aHparam); 
               break;
            }
            case RATQUAD:
            {
               mKernel = new RATQUAD_KERN(aHparam);
               break;
            }
            case PERIODIC:
            {
               mKernel = new PERIODIC_KERN(aHparam);
               break;
            }
            case ANOVA:
            {
               mKernel = new ANOVA_KERN(aHparam);
               break;
            }
            case POLYNOM:
            {
               mKernel = new POLYNOM_KERN(aHparam);
               break;
            }
            case MATERN32:
            {
               mKernel = new MATERN32_KERN(aHparam);
               break;
            }
            case MATERN52:
            {
               mKernel = new MATERN52_KERN(aHparam);
               break;
            }
            case SC32:
            {
               mKernel = new SC32_KERN(aHparam);
               break;
            }
            case SC52:
            {
               mKernel = new SC52_KERN(aHparam);
               break;
            }
            case SCEXP:
            {
               mKernel = new SCEXP_KERN(aHparam);
               break;
            }
            case SOAP:
            {
               mKernel = new SOAP_KERN(aHparam);
               break;
            }
            case RBF:
            {
               mKernel = new RBF_KERN(aHparam);
               break;
            }
            default:
            {
               MIDASERROR("Unknown kernel type in kernel constructor!");
            }
         }

      }

      ///> Destructor
      ~GPKernel()
      {
         if (mKernel != NULL)
         {
            delete mKernel;
         }
      }

      ///> Copy constructor 
      GPKernel<T>(const GPKernel<T> &other) 
      {
         mKernType  = other.mKernType;  
         mNormalize = other.mNormalize;  
         std::vector<T> hparam = other.GetHyperParameters();

         if (mKernel != NULL)
         {
            delete mKernel;
         }

         init_kernel(hparam,mKernType,mNormalize,"copy");
      }

      /**
       *  @brief Makes a deep copy  
       *
       * */
      GPKernel<T>& operator=(const GPKernel<T> &other)
      {
         if (this != &other)
         {
            mKernType  = other.mKernType;
            mNormalize = other.mNormalize;
            std::vector<T> hparam = other.GetHyperParameters();

            if (mKernel != NULL)
            {
               delete mKernel;
            }

            init_kernel(hparam,mKernType,mNormalize,"deepcopy");         
         }
         
         return *this; 
      }

      /**
       *  @brief Returns the kernel name 
       *
       *  Purpose: Return the name of the kernel e.g. "Squared Exponential" 
       *
       * */
      std::string GetName() const override
      {
         return mKernel->GetName(); 
      }

      /**
       *  @brief Returns the hyper parameters 
       *
       *  Purpose: Returns the hyper parameters of the  e.g.  
       *
       * */
      std::vector<T> GetHyperParameters() const override
      {
         return mKernel->GetHyperParameters();
      }

      /**
       *  @brief Sets the hyper parameters 
       *
       *  Purpose: Sets the hyper parameters of the  e.g.  
       *
       * @param  aHparam is a vector containing the hyper parameters
       *
       * */
      void SetHyperParameters(const std::vector<T>& aHparam) override
      {
         mKernel->SetHyperParameters(aHparam); 
      }

      /**
       *  @brief Returns if a hyper parameter gradient is available 
       * */
      bool HasHyperParamDeriv() const override   {return mKernel->HasHyperParamDeriv();}
      /**
       *  @brief Returns if the kernel gradient is available 
       * */
      bool HasGradient() const override         {return mKernel->HasGradient();}
      /**
       *  @brief Returns if the kernel Hessian is available 
       * */
      bool HasHessian() const override          {return mKernel->HasHessian();}


      /**
       *  @brief Computes the co-variance matrix K 
       *
       *  Purpose: Is used to compute the co-variance matrix K for a given kernel 
       *
       * @param  aCov is a unique_ptr to the allocated memory for K 
       * @param  aPointsi are the set of data points i 
       * @param  aPointsj are the set of data points j 
       * @param  aNoise specifies the applied noise term
       * @param  aIstart is an optional argument for the start index in aPointsi and aPointsj  
       * @param  aIend is an optional argument for the last index in aPointsi   
       * @param  aJend is an optional argument for the last index in aPointsj 
       * @param  ais_symmetric is a boolean to specify if the matrix is symmetric/square
       * @param  use_scaling is a boolean to enforce a user defined scaling of the co-variance matrix
       * @param  alpha is the optional scaling factor for the co-variance matrix
       * @param  aDoNormalize enforces that the kernel will be normalized
       * */
      void Compute
         (  std::unique_ptr<T[]>& aCov
         ,  const std::vector<MLDescriptor<T>>& aPointsi
         ,  const std::vector<MLDescriptor<T>>& aPointsj
         ,  const std::vector<Nb>& aNoise
         ,  const int& aIstart
         ,  const int& aIend
         ,  const int& aJend 
         ,  const bool& ais_symmetric  
         ,  const bool& use_scaling = false
         ,  const T& alpha = static_cast<T>(1.0)
         ,  const bool& aDoNormalize = false 
         )
      {
         LOGCALL("GP::Compute");

         T zero = static_cast<T>(0.0);

         bool addnoise = false;

         int lderi = 0;   // derivative order for i
         int lderj = 0;   // derivative order for j

         int ndimj = aJend - aIstart; //aPointsj.size();
         int ndimi = aIend - aIstart; //aPointsi.size();

         const bool& do_normalize = mNormalize || aDoNormalize;

         T dscal = static_cast<T>(1.0);
         if (use_scaling) dscal = alpha; 

         #pragma omp parallel for \
          default(none) \
          shared(aIstart,aJend,aIend,aPointsi,aPointsj,aCov,zero,ndimj,ndimi,ais_symmetric,dscal, do_normalize, aNoise) \
          firstprivate(lderi,lderj,addnoise)
         for (int jdx = aIstart; jdx < aJend; jdx++)
         {
            lderj = aPointsj[jdx].GetDerivativeOrder();

            T normsqj = static_cast<T>(1);
            if (do_normalize) normsqj = this->Compute( aPointsj[jdx]
                                                     , aPointsj[jdx]
                                                     , addnoise
                                                     , zero 
                                                     , lderj
                                                     , lderj
                                                     , false
                                                     );

            int iend = aIend; 
            if (ais_symmetric) iend = jdx + 1;
            for (int idx = aIstart; idx < iend; idx++)
            {
               lderi = aPointsi[idx].GetDerivativeOrder();
               addnoise = (idx == jdx);

               T noise = static_cast<T>(0);
               if (addnoise) noise = aNoise[idx];

               T dval = this->Compute( aPointsi[idx]
                                     , aPointsj[jdx]
                                     , addnoise
                                     , noise
                                     , lderi 
                                     , lderj
                                     , false
                                     );
               T normsqi = static_cast<T>(1);
               if (do_normalize) normsqi = this->Compute( aPointsi[idx]
                                                        , aPointsi[idx]
                                                        , addnoise
                                                        , zero 
                                                        , lderi
                                                        , lderi
                                                        , false
                                                        );

               if (ais_symmetric)
               {
                  aCov[jdx * ndimi + idx] = dscal * dval / std::sqrt(normsqi * normsqj) ;
                  aCov[idx * ndimj + jdx] = dscal * dval / std::sqrt(normsqi * normsqj) ;
               }
               else
               {
                  aCov[jdx * ndimi + idx] = dscal * dval / std::sqrt(normsqi * normsqj) ;
               }

            }
         }
      }

      /**
       *  @brief Computes the derivative of co-variance matrix K w.r.t to hyper parameters
       *
       *  Purpose: Is used to compute the derivative of the co-variance matrix K 
       *           w.r.t hyperparameter hidx
       *
       * @param  aCov is a unique_ptr to the allocated memory for K 
       * @param  aHidx index for the hyper parameter
       * @param  aPointsi are the set of data points i 
       * @param  aPointsj are the set of data points j
       * @param  aNthreads specifies how many threads can be use for OMP 
       * */
      void ComputeHderiv
         (  std::unique_ptr<T[]>& aCov
         ,  const int& aHidx
         ,  const std::vector<MLDescriptor<T>>& aPointsi
         ,  const std::vector<MLDescriptor<T>>& aPointsj
         ,  const int& aNthreads
         )
      {

         const bool locdbg = false;
         const bool& do_normalize = mNormalize;

         LOGCALL("GP::ComputeHderiv");

         int ndim = aPointsj.size();

         int lderi,lderj;

         if (locdbg) Mout << "ComputeHderiv:> I will use " << aNthreads << " threads" << std::endl;

         #pragma omp parallel for num_threads(aNthreads) \
          default(none) \
          shared(aCov,aPointsi,aPointsj,ndim,aHidx,do_normalize) \
          private(lderi,lderj) 
         for (int jdx = 0; jdx < ndim; jdx++)
         {
            lderj = aPointsj[jdx].GetDerivativeOrder();

            T normsqj = static_cast<T>(1);
            T dKjj = static_cast<T>(1);
            if (do_normalize) 
            {  
               normsqj = this->Compute( aPointsj[jdx]
                                      , aPointsj[jdx]
                                      , false 
                                      , 0.0
                                      , lderj
                                      , lderj
                                      , false
                                      );

               dKjj = this->ComputeHderiv( aPointsi[jdx]
                                         , aPointsj[jdx]
                                         , aHidx
                                         , lderj
                                         , lderj
                                         );
            }

            for (int idx = 0; idx <= jdx; idx++)
            {
               lderi = aPointsi[idx].GetDerivativeOrder();

               T dval = this->ComputeHderiv( aPointsi[idx]
                                           , aPointsj[jdx]
                                           , aHidx
                                           , lderi
                                           , lderj
                                           );

               T normsqi = static_cast<T>(1);
               T crossterm = static_cast<T>(0);
               if (do_normalize) 
               {  
                  normsqi = this->Compute( aPointsi[idx]
                                         , aPointsi[idx]
                                         , false 
                                         , 0.0
                                         , lderi
                                         , lderi
                                         , false
                                         );

                  T dKii = this->ComputeHderiv( aPointsi[idx]
                                              , aPointsj[idx]
                                              , aHidx
                                              , lderi
                                              , lderi
                                              );

                  T Kij = this->Compute( aPointsi[idx]
                                       , aPointsi[jdx]
                                       , false
                                       , 0.0
                                       , lderi
                                       , lderj
                                       , false
                                       );

                  crossterm = - 0.5 * Kij * std::pow(normsqi * normsqj, -1.5) * (dKii * normsqj + normsqi * dKjj);
               }

               aCov[jdx * ndim + idx] = dval / std::sqrt(normsqi * normsqj) + crossterm; 
               aCov[idx * ndim + jdx] = dval / std::sqrt(normsqi * normsqj) + crossterm; 
            }
         }
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           aXi and aXj
       *
       * @param  aXi is the data points i 
       * @param  aXj is the data points j
       * @param  aAddnoise specifies if the noise term is added
       * @param  aNoise specifies the added noise 
       * @param  aLderi is the derivative order for the point xi
       * @param  aLderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& aXi
         ,  const MLDescriptor<T>& aXj
         ,  const bool &aAddnoise
         ,  const Nb& aNoise
         ,  const int& aLderi
         ,  const int& aLderj
         )  override
      {
         return this->Compute(aXi,aXj,aAddnoise,aNoise,aLderi,aLderj,mNormalize);
      }
      
      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           aXi and aXj
       *
       * @param  aXi is the data points i 
       * @param  aXj is the data points j
       * @param  aAddnoise specifies if the noise term is added
       * @param  aNoise specifies the added noise 
       * @param  aLderi is the derivative order for the point xi
       * @param  aLderj is the derivative order for the point xj
       * @param  aNormalize specifies if the kernel should be normalized 
       * */
      T Compute
         (  const MLDescriptor<T>& aXi
         ,  const MLDescriptor<T>& aXj
         ,  const bool &aAddnoise
         ,  const Nb& aNoise
         ,  const int& aLderi
         ,  const int& aLderj
         ,  const bool& aNormalize 
         )
      {
         if (aNormalize)
         {
            T Kij = mKernel->Compute(aXi, aXj, aAddnoise, aNoise, aLderi, aLderj);
            T Kii = mKernel->Compute(aXi, aXi, aAddnoise, aNoise, aLderi, aLderi);
            T Kjj = mKernel->Compute(aXj, aXj, aAddnoise, aNoise, aLderj, aLderj);

            return Kij / std::sqrt(Kii * Kjj);
         }
         else
         {
            return mKernel->Compute(aXi, aXj, aAddnoise, aNoise, aLderi, aLderj);
         }
      }


      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           aXi and aXj w.r.t hyper parameter hidx
       *
       * @param  aXi is the data points i 
       * @param  aXj is the data points j
       * @param  aHidx index of the hyper parameter
       * @param  aLderi is the derivative order for the point xi
       * @param  aLderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& aXi
         ,  const MLDescriptor<T>& aXj
         ,  const int& aHidx
         ,  const int& aLderi
         ,  const int& aLderj
         )  override
      {
         return mKernel->ComputeHderiv(aXi, aXj, aHidx, aLderi, aLderj);
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  aXi is the data points i 
       * @param  aXj is the data points j
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& aXi
         ,  const MLDescriptor<T>& aXj
         )  override
      {
         return mKernel->Gradient(aXi, aXj);
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  aHessian is a unique_ptr to the memory for the hessian
       * @param  aPointsi is the data points i 
       * @param  aPointsj is the data points j
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& aHessian
         ,  const MLDescriptor<T>& aPointsi
         ,  const MLDescriptor<T>& aPointsj
         )  override
      {
         mKernel->GetHessian(aHessian, aPointsi, aPointsj);
      }

};


#endif /* KERNEL_INCLUDED */
