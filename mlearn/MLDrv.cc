/**
************************************************************************
* 
* @file                MLDrv.cc
*
* Created:             03-07-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Interface to use the Machine Learning algorithms
*                      in Midas in a Standalone fashion 
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// My headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "input/Input.h"
#include "input/MLCalcDef.h"
#include "mlearn/MLTask.h"

/**
* Run the interface
* */
void MLDrv()
{

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Start Machine Learning interface ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
  Mout << "\n\n";


  for (In i_calc =0; i_calc < gMLCalcDef.size(); i_calc++) 
  {  

/*
***********   Create ML task object  **************************
*/
    Mout << "\n\n\n Do ML task:  " << gMLCalcDef[i_calc].GetName() << "\n" << endl;

    MLTask mltask(&gMLCalcDef[i_calc]);

/*
***********   Start ML task  **************************
*/

    Timer time_it;

    mltask.Run();  

    string s_time = " CPU time used for ML task :";
    time_it.CpuOut(cout,s_time);

  }

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Machine Learning interface done ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
}

