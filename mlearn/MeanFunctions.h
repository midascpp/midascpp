/**
************************************************************************
* 
* @file                
*
* Created:            12-09-2019         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for mean functions used in GPR 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MEAN_INCLUDED
#define MEAN_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>

#include "geoopt/geoutil.h"  // GS: I don't like to include it here

template <class T>
class GPMeanFunction
{

   public:

      ///> Constructor(s) 
      GPMeanFunction(){}

      ///> Destructor
      virtual ~GPMeanFunction() = default;

      virtual T Evaluate
         (  MLDescriptor<T>& point
         )
      {
         return C_0;
      }

}; 

template <class T>
class GPConstMean : public GPMeanFunction<T>
{

   private:

      T mConstVal = C_0;

   public:

      GPConstMean(T val)  
      { 
         mConstVal = val;
      }
 
      ///> Destructor
      ~GPConstMean() = default;

      virtual T Evaluate
         (  MLDescriptor<T>& point
         )
      {
         return mConstVal;
      }

};


template <class T>
class GPHessMean : public GPMeanFunction<T>
{
   private:

      T mEref = C_0;
      std::vector<T> mHess;

   public:

      GPHessMean
         (  T energy  
         ,  std::vector<T>& hess
         )
      {
         mEref = energy;
         mHess = hess;
      }

      ///> Destructor
      ~GPHessMean() = default;

      GPHessMean
         (  T energy  
         ,  const std::string& fhess
         ,  bool ToInternal 
         ,  const std::vector<T>& cartcoord
         ,  const std::vector<InternalCoord>& interncoord
         ,  const int& nintern
         ,  const int& ncart
         ,  const int& natoms
         )
      {
         mEref = energy;

         std::ifstream hess_file;
         hess_file.open (fhess, std::ifstream::in);
         std::string s;

         std::vector<T> hess;

         while(midas::input::GetLine(hess_file, s) )
         {
            In ndim;
         
            // Get dimension(s)
            std::istringstream str_stream(s);
            str_stream >> ndim;

            In maxblk = ndim / 6;

            // Resize vector
            hess.resize(ndim*ndim);

            In jstart = 0;
            for (In iblk = 0; iblk < maxblk; iblk++)
            {
               // Read dummy line
               midas::input::GetLine(hess_file, s);

               // Read hessian values
               for (In idx = 0; idx < ndim; idx++)
               {
                  In index;
                  midas::input::GetLine(hess_file, s);
                  std::istringstream str_stream(s);

                  str_stream >> index; 

                  In ic = 0;
                  while (!str_stream.eof())
                  {
                     T dval;
                     str_stream >> dval;
                     hess[idx*ndim + jstart + ic] = dval;
                     ic++;
                  }
               }
               jstart += 6; 
            }
         }

         if (ToInternal)
         {
            mHess = TransformHessian(hess,cartcoord,interncoord,natoms,nintern,ncart);
         }
         else
         {
            mHess = hess;
         }

      }

      virtual T Evaluate
         (  MLDescriptor<T>& point
         )
      {

         const bool locdbg = false;

         // Calculate mean function as 
         // --------------------------
         //   m = E_0 + q^T H q
         //
         // Thus we ignore the gradient...
         

         int ndim = point.size();
         int n1 = 1;
         T qHq = C_0;

         T zero = 0.0;
         T one  = 1.0;
         char nn = 'N';

         T* tmp = new T[ndim];

         // 1.) z =H q
         midas::lapack_interface::gemm( &nn, &nn, &ndim, &n1, &ndim, &one 
                                      , &mHess[0], &ndim, &point[0], &ndim 
                                      , &zero, tmp, &ndim );   
        
         // 2.) q^T z
         qHq = midas::lapack_interface::dot(&ndim, &point[0], &n1, tmp, &n1);  
 
         delete[] tmp;

         if (locdbg) Mout << "GPHessMean " << mEref + qHq << std::endl;

         return mEref + qHq;

      }

};


#endif /* GAUPRO_INCLUDED */
