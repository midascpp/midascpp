#include <numeric>
#include <cmath>

namespace SymmetryFunctions
{


template <class T>
T GetDistance
   (  const std::vector<T>& aCoord
   ,  const In& aIat
   ,  const In& aJat
   )
{
   T Rij = std::sqrt(  (aCoord[aIat*3 + 0]-aCoord[aJat*3 + 0]) * (aCoord[aIat*3 + 0]-aCoord[aJat*3 + 0])
                    +  (aCoord[aIat*3 + 1]-aCoord[aJat*3 + 1]) * (aCoord[aIat*3 + 1]-aCoord[aJat*3 + 1])
                    +  (aCoord[aIat*3 + 2]-aCoord[aJat*3 + 2]) * (aCoord[aIat*3 + 2]-aCoord[aJat*3 + 2])
                    );

   return Rij;
}

template <class T>
T GetCosTheta
   (  const std::vector<T>& aCoord
   ,  const In& aIat
   ,  const In& aJat
   ,  const In& aKat
   )
{
   T Rij = GetDistance(aCoord,aIat,aJat);
   T Rik = GetDistance(aCoord,aIat,aKat);

   T costheta = ( (aCoord[aIat*3 + 0]-aCoord[aJat*3 + 0]) * (aCoord[aIat*3 + 0]-aCoord[aKat*3 + 0])
                + (aCoord[aIat*3 + 1]-aCoord[aJat*3 + 1]) * (aCoord[aIat*3 + 1]-aCoord[aKat*3 + 1]) 
                + (aCoord[aIat*3 + 2]-aCoord[aJat*3 + 2]) * (aCoord[aIat*3 + 2]-aCoord[aKat*3 + 2]) 
                )
                / (Rij * Rik);

   return costheta;
}

template <class T>
T DotProductRijRik
   (  const std::vector<T>& aCoord
   ,  const In& aIat
   ,  const In& aJat
   ,  const In& aKat
   )
{
   T Ret = 0.0;
   for (int ixyz = 0; ixyz < 3; ixyz++)
   {
      Ret += (aCoord[aIat*3 + ixyz]-aCoord[aJat*3 + ixyz]) * (aCoord[aIat*3 + ixyz]-aCoord[aKat*3 + ixyz]);
   } 
   return Ret;
}

/**
 *
 * @brief Allow a smooth cutoff 
 *
 **/
template <class T>
T CutOff
   (  const T& Rij
   ,  const T& Rcutoff
   ) 
{
   if (Rij <= Rcutoff)
   {
      return 0.5 * (std::cos(M_PI*Rij/Rcutoff) + 1.0);
   }
   else
   {
      return 0.0;
   }
}

/**
 *
 * @brief Derivative of cutoff function 
 *
 **/
template <class T>
T dCutOff
   (  const T& Rij
   ,  const T& Rcutoff
   ,  const T& dxij
   )
{
   if (Rij <= Rcutoff)
   {
      return 0.5 * (M_PI * dxij / (Rcutoff*Rij))  * std::sin(M_PI*Rij/Rcutoff);
   }
   else
   {
      return 0.0;
   }
}


/**
 *
 * @brief Calculates the Behler type symmetry function G2
 *
 * @param aCoord contain nuclear coordiantes 
 * @param aIneighbour returns a list of neighbours involved
 * @param aIat specifies the atom for which the symmetry function should be calculated
 * @param aNatoms specifies the number of atoms
 * @param arRcutoff is a vector containing the cuttoffs for a linearcombination of differen symmetry functions
 * @param arEta is a vector containing the eta parameters for a linearcombination of differen symmetry functions
 *
 **/
template <class T>
T G2 
   (  const std::vector<T>& aCoord
   ,  std::vector<In>& aIneighbour
   ,  const In& aIat
   ,  const In& aNatoms
   ,  const std::vector<T>& arRcutoff = std::vector<T>(1,10.)
   ,  const std::vector<T>& arEta = std::vector<T>(1,-2.30259)
   )
{

   T G2_val = static_cast<T>(0.0);

   for (In idx = 0; idx < arEta.size(); idx++)
   {
      for (In jat = 0; jat < aNatoms; jat++)
      {
         T Rij = GetDistance(aCoord, aIat, jat);
      
         if (Rij <= arRcutoff[idx])
         {
            aIneighbour.push_back(jat);

            G2_val += std::exp(-std::pow(std::exp(arEta[idx]) * Rij / arRcutoff[idx],2.0)) * CutOff(Rij,arRcutoff[idx]); 
         }
      }
   }

   return G2_val;
}

/**
 *
 * @brief Calculates derivative of the Behler type symmetry function G2
 *
 * @param aCoord contain nuclear coordiantes 
 * @param aIat specifies the atom for which the symmetry function should be calculated
 * @param aDerJat is the atom index alon which should be differentiated
 * @param aDerI is the coordinate (x,y,z) = (0,1,2) which should be differentiated
 * @param aNatoms specifies the number of atoms
 * @param arRcutoff is a vector containing the cuttoffs for a linearcombination of differen symmetry functions
 * @param arEta is a vector containing the eta parameters for a linearcombination of differen symmetry functions
 *
 **/
template <class T>
T dG2 
   (  const std::vector<T>& aCoord
   ,  const In& aIat
   ,  const In& aDerJat
   ,  const In& aDerI
   ,  const In& aNatoms
   ,  const std::vector<T>& arRcutoff = std::vector<T>(1,10.)
   ,  const std::vector<T>& arEta = std::vector<T>(1,-2.30259)
   )
{
   T dG2_val = static_cast<T>(0.0);

   for (In idx = 0; idx < arEta.size(); idx++)
   {
      if (aIat == aDerJat)
      {
         // Special case if differentiated along the central atom
         for (In jat = 0; jat < aNatoms; jat++)
         {
            if (jat == aIat) continue;

            T Rij = GetDistance(aCoord,aIat,jat);
      
            if (Rij <= arRcutoff[idx])
            {
               T dxij = aCoord[aIat*3 + aDerI] - aCoord[jat*3 + aDerI]; 
               T g2   = std::exp(-std::pow(std::exp(arEta[idx]) * Rij / arRcutoff[idx],2.0));
               T dg2  = - 2.0 * std::pow(std::exp(arEta[idx])/arRcutoff[idx],2.0) * dxij * g2; 

               dG2_val -= dg2 * CutOff(Rij,arRcutoff[idx]) + g2 * dCutOff(Rij,arRcutoff[idx],dxij); 
            }
         }
      }
      else
      {
         T Rij = GetDistance(aCoord, aIat, aDerJat);
         
         if (Rij <= arRcutoff[idx])
         {
            T dxij = aCoord[aIat*3 + aDerI] - aCoord[aDerJat*3 + aDerI]; 
            T g2   = std::exp(-std::pow(std::exp(arEta[idx]) * Rij / arRcutoff[idx],2.0));
            T dg2  = - 2.0 * std::pow(std::exp(arEta[idx])/arRcutoff[idx],2.0) * dxij * g2; 

            dG2_val = dg2 * CutOff(Rij,arRcutoff[idx]) + g2 * dCutOff(Rij,arRcutoff[idx],dxij); 
         }
      }
   }

   return dG2_val;
}

/**
 *
 * @brief Calculates the Behler type symmetry function G4
 *
 * @param aCoord contain nuclear coordiantes 
 * @param aIneighbour returns a list of neighbours involved
 * @param aIat specifies the atom for which the symmetry function should be calculated
 * @param aNatoms specifies the number of atoms
 * @param arRcutoff is a vector containing the cuttoffs for a linearcombination of different symmetry functions
 * @param arEta is a vector containing the eta parameters for a linear combination of different symmetry functions
 * @param arXi is a vector containing the xi paramteters for a linear combination of different symmetry functions
 * @param arLambda is a vector containing the lambda paramteters for a linear combination of different symmetry functions
 *
 **/
template <class T>
T G4 
   (  const std::vector<T>& aCoord
   ,  std::vector<In>& aIneighbour
   ,  const In& aIat
   ,  const In& aNatoms
   ,  const std::vector<T>& arRcutoff = std::vector<T>(1,10.)
   ,  const std::vector<T>& arEta = std::vector<T>(1,-2.30259)
   ,  const std::vector<T>& arXi = std::vector<T>(1,2.0)
   ,  const std::vector<T>& arLambda = std::vector<T>(1,-1.0)  // +1 or -1
   ) 
{

   T G4_val = static_cast<T>(0.0);

   for (In idx = 0; idx < arEta.size(); idx++)
   {
      for (In jat = 0; jat < aNatoms; jat++)
      {
  
         if (aIat == jat) continue;

         T Rij = GetDistance(aCoord,aIat,jat);

         if (Rij <= arRcutoff[idx]) aIneighbour.push_back(jat);
      
         for (In kat = 0; kat < aNatoms; kat++)
         {

            if (aIat == kat) continue;
            if (jat == kat)  continue;

            T Rik = GetDistance(aCoord,aIat,kat);
            T Rjk = GetDistance(aCoord,jat,kat);
            T costheta = GetCosTheta(aCoord,aIat,jat,kat);

            T RC2 = arRcutoff[idx]*arRcutoff[idx];

            G4_val += std::pow(2.0,1.0-arXi[idx]) * std::pow(1.0 + arLambda[idx] * costheta, arXi[idx]) 
                    * std::exp(-std::exp(arEta[idx]) * (Rij*Rij/RC2 + Rik*Rik/RC2 + Rjk*Rjk/RC2) ) 
                    * CutOff(Rij,arRcutoff[idx]) * CutOff(Rik,arRcutoff[idx]) * CutOff(Rjk,arRcutoff[idx]) ; 

         }


      }
   }

   return G4_val;
}

/**
 *
 * @brief Calculates the Behler type symmetry function G4
 *
 * @param aCoord contain nuclear coordiantes 
 * @param aIat specifies the atom for which the symmetry function should be calculated
 * @param aDerJat specifies the atom alon which distances it should be differentiated 
 * @param aDerI is the coordinate (x,y,z) = (0,1,2) which should be differentiated
 * @param aNatoms specifies the number of atoms
 * @param arRcutoff is a vector containing the cuttoffs for a linearcombination of different symmetry functions
 * @param arEta is a vector containing the eta parameters for a linear combination of different symmetry functions
 * @param arXi is a vector containing the xi paramteters for a linear combination of different symmetry functions
 * @param arLambda is a vector containing the lambda paramteters for a linear combination of different symmetry functions
 *
 **/
template <class T>
T dG4 
   (  const std::vector<T>& aCoord
   ,  const In& aIat
   ,  const In& aDerJat
   ,  const In& aDerI
   ,  const In& aNatoms
   ,  const std::vector<T>& arRcutoff = std::vector<T>(1,10.)
   ,  const std::vector<T>& arEta = std::vector<T>(1,-2.30259)
   ,  const std::vector<T>& arXi = std::vector<T>(1,2.0)
   ,  const std::vector<T>& arLambda = std::vector<T>(1,-1.0)  // +1 or -1
   ) 
{
   T dG4_val = static_cast<T>(0.0);

   for (In idx = 0; idx < arEta.size(); idx++)
   {
      if (aIat == aDerJat)
      {
         // special case if the along central atom positions is differentiated
         
         for (In jat = 0; jat < aNatoms; jat++)
         {
  
            if (aIat == jat) continue;

            T Rij = GetDistance(aCoord,aIat,jat);

            T dxij = aCoord[aIat*3 + aDerI] - aCoord[jat*3 + aDerI]; 
         
            for (In kat = 0; kat < aNatoms; kat++)
            {

               if (aIat == kat) continue;
               if (jat == kat)  continue;

               T Rik  = GetDistance(aCoord,aIat,kat);
               T Rjk  = GetDistance(aCoord,jat,kat);

               T dxik = aCoord[aIat*3 + aDerI] - aCoord[kat*3 + aDerI]; 

               T costheta = GetCosTheta(aCoord,aIat,jat,kat);
           
               T RC2 = arRcutoff[idx]*arRcutoff[idx];

               T DotRijRik = DotProductRijRik(aCoord,aIat,jat,kat);

               T dcostheta = 1.0/(Rij*Rik) * (dxik + dxij)  - DotRijRik * ( dxij/(Rij*Rij*Rij*Rik) + dxik/(Rij*Rik*Rik*Rik) )  ;  

               dG4_val += std::pow(2.0,1.0-arXi[idx]) * std::pow(1.0 + arLambda[idx] * costheta, arXi[idx] - 1) * std::exp(-std::exp(arEta[idx])/RC2 * (Rij*Rij + Rik*Rik + Rjk*Rjk) )
                        * ( CutOff(Rij,arRcutoff[idx]) * CutOff(Rik,arRcutoff[idx]) * CutOff(Rjk,arRcutoff[idx])  
                        * ( arLambda[idx] * arXi[idx] * dcostheta - 2.0 * std::exp(arEta[idx]) * (1.0 + arLambda[idx] * costheta) * (dxij + dxik)/RC2 )  
                          + (1.0 + arLambda[idx] * costheta) * ( dCutOff(Rij,arRcutoff[idx],-dxij) * CutOff(Rik,arRcutoff[idx]) * CutOff(Rjk,arRcutoff[idx])
                                                        + CutOff(Rij,arRcutoff[idx]) * dCutOff(Rik,arRcutoff[idx],-dxik) * CutOff(Rjk,arRcutoff[idx])
                                                        )
                          );

            }
         }
      }
      else
      {
         T Rij = GetDistance(aCoord,aIat,aDerJat); 

         T dxij = aCoord[aIat*3 + aDerI] - aCoord[aDerJat*3 + aDerI]; 
      
         for (In kat = 0; kat < aNatoms; kat++)
         {

            if (aIat == kat)     continue;
            if (aDerJat == kat)  continue;

            T Rik = GetDistance(aCoord,aIat,kat);
            T Rjk = GetDistance(aCoord,aDerJat,kat);

            T dxjk = aCoord[aDerJat*3 + aDerI] - aCoord[kat*3 + aDerI]; 
            T dxik = aCoord[aIat*3 + aDerI] - aCoord[kat*3 + aDerI]; 

            T costheta = GetCosTheta(aCoord,aIat,aDerJat,kat);

            T DotRijRik = DotProductRijRik(aCoord,aIat,aDerJat,kat);

            T dcostheta = -1.0/(Rij*Rik) * dxik + DotRijRik * dxij/(Rij*Rij*Rij*Rik);  

            T RC2 = arRcutoff[idx]*arRcutoff[idx];
            
            dG4_val += 2.0 
                     * std::pow(2.0,1.0-arXi[idx]) * std::pow(1.0 + arLambda[idx] * costheta, arXi[idx] - 1) * std::exp(-std::exp(arEta[idx])/RC2 * (Rij*Rij + Rik*Rik + Rjk*Rjk) )
                     * ( CutOff(Rij,arRcutoff[idx]) * CutOff(Rik,arRcutoff[idx]) * CutOff(Rjk,arRcutoff[idx]) 
                     * ( arLambda[idx] * arXi[idx] * dcostheta - 2.0 * std::exp(arEta[idx]) * (1.0 + arLambda[idx] * costheta) * (-dxij + dxjk)/RC2 )
                       + (1.0 + arLambda[idx] * costheta) * ( dCutOff(Rij,arRcutoff[idx],dxij) * CutOff(Rik,arRcutoff[idx]) * CutOff(Rjk,arRcutoff[idx])
                                                     + CutOff(Rij,arRcutoff[idx]) * CutOff(Rik,arRcutoff[idx]) * dCutOff(Rjk,arRcutoff[idx],-dxjk)
                                                     )
                        );
 

         }
      }
   }

   return dG4_val;
}

} // namespace SymmetryFunctions
