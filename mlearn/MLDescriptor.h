#ifndef MLDESCRIPTOR_INCLUDED
#define MLDESCRIPTOR_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <functional>
      

#include "specialfunctions/sphericalharmonics.h"
#include "specialfunctions/bessel.h"
#include "util/CallStatisticsHandler.h"
#include "inc_gen/Const.h"

template <class T>
class MLDescriptor
{

   public:

      enum Type { SCALAR, ATOMISTIC  };

      enum DataType {ENERGY, GRADIENT, HESSIAN};


   private:

      // At the moment we use the atomic charges as guesses. Probably not completely stupid choice, but also not fancy... 
      std::map<std::string, T> mAlphaGuess = {{"h"  , 1 }  , {"he" , 2 }    , {"li" , 3  } , {"be" , 4 } ,  {"b"  , 5  }   ,
                                              {"c"  , 6 }  , {"n"  , 7 }    , {"o"  , 8  } , {"f"  , 9 } ,  {"ne" , 10 }   ,
                                              {"na" , 11}  , {"mg" , 12}    , {"al" , 13 } , {"si" , 14} ,  {"p"  , 15 }   ,
                                              {"s"  , 16}  , {"cl" , 17}    , {"ar" , 18 } , {"k"  , 19} ,  {"ca" , 20 }   };

      Type mType = SCALAR;
      DataType mDataType = ENERGY;

      int mDerivCoord1 = -1;
      int mDerivCoord2 = -1;

      std::vector<T> mData;

      std::vector<T> mAlpha;

      std::vector<std::vector<T>> mPositions;

      int mLmax = 8;
      int mNatoms = 0;

      T mCutoff =  10;
                         
      /**
       *  @brief Returns an initial alpha value 
       *
       *  Purpose: Returns an initial alpha value to describe the atomistic density
       *
       *  @param label is a string with the element sybmol e.g. H, O, etc. 
       *
       **/

      T GetInitialAlpha(std::string label)
      {
         std::transform(label.begin(), label.end(), label.begin(), tolower); 


         if ( mAlphaGuess.find(label) == mAlphaGuess.end() ) 
         {
            MIDASERROR("MLDescriptor> I have no tabulated alpha for this atom!");
            return mAlphaGuess["h"]; // Just to please compiler -> No warning
         } 
         else 
         {
            return mAlphaGuess[label]; 
         }
      }
 
   public:

      ///> Default Constructor
      MLDescriptor() = default;

      ///> Constructor(s) 
      MLDescriptor
         (  const std::vector<T>& data
         ,  Type dtype
         ,  int lmax = 8  
         ,  std::vector<std::string> elemlist = std::vector<std::string>()
         ) : mType(dtype)
      {

         switch (mType)
         {
            case SCALAR:
            {
               mData = data; 
               break;
            }
            case ATOMISTIC:
            {
               mLmax = lmax;
               mNatoms = data.size() / 3;
               mData = data;
               mPositions.resize(mNatoms);
               
               for (int iat = 0; iat < mNatoms; iat++)
               {
                  mPositions[iat].resize(3);
                  mPositions[iat][0] = mData[iat*3 + 0];
                  mPositions[iat][1] = mData[iat*3 + 1];
                  mPositions[iat][2] = mData[iat*3 + 2];
               }
	            mAlpha.resize(mNatoms, 1.0);    
               for (std::vector<std::string>::iterator label = elemlist.begin() ; label != elemlist.end(); ++label)
               {
                  if ( mAlphaGuess.find(*(label)) == mAlphaGuess.end() )
                  {
                     MIDASERROR("MLDescriptor> I have no tabulated alpha for this atom!");
                  }

                  T alpha = mAlphaGuess[*(label)];

                  mAlpha.push_back(alpha); 
               }
               break;
            }
            default:
            {
               MIDASERROR("Unknown type in descriptor constructor!");
            }
         }

      }

      MLDescriptor
         (  std::string fcoord 
         ,  Type dtype
         ,  int lmax = 8
         ) : mType(dtype)
           , mLmax(lmax)
      {
         const T Ang2Bohr = 1.0/C_TANG;

         if (mType != ATOMISTIC) MIDASERROR("When reading from file only atomic descriptor allowed!");

         std:: string s;
         std::ifstream coord_file;
         coord_file.open (fcoord, std::ifstream::in);
  
         // read atom lines
         mNatoms  = I_0;
         while(midas::input::GetLine(coord_file, s) )
         {
            Nb x, y, z, energy;
            std::string label, dummy1, dummy2;
            
            // 1) read number of Coordinates
            std::istringstream str_stream(s);
            str_stream >> mNatoms;

            // 2) read energy 
            midas::input::GetLine(coord_file, s);
            str_stream.str(s);
            str_stream >> dummy1 >> dummy2 >> energy;

            // 3) read coordinates
            std::vector<std::string> elems;
            mPositions.resize(mNatoms);
            for (In iat = 0; iat < mNatoms; iat++)
            {
               midas::input::GetLine(coord_file, s);

               std::istringstream str_stream(s);
               str_stream >> label >> x >> y >> z;
 
               mPositions[iat].resize(3);
               mPositions[iat][0] = x * Ang2Bohr;
               mPositions[iat][1] = y * Ang2Bohr;
               mPositions[iat][2] = z * Ang2Bohr;

               T alpha = GetInitialAlpha(label);

               mAlpha.push_back(alpha);
            }

         }

         // close/postprocess file
         coord_file.close();
      }

      ///> Destructor
      ~MLDescriptor() = default;

      int size() const {return mData.size();}

      T& operator [](int idx) {return mData[idx];}

      T operator [](int idx) const {return mData[idx];}

      /**
       *  @brief Returns the nearest neigbours of atom k 
       *
       *  Purpose: Returns the nearest neigbours for atom k in the index list
       *
       *  @param index is the index of the atom for which the neighbours should be determined 
       *
       **/
      std::vector<std::vector<T>> GetNNeighbourspositions(int &index) const
      {

         if (mType != ATOMISTIC) MIDASERROR("GetNNeighbourspositions only implemented for atomic descriptor!");
	
         std::vector<std::vector<T>> neighbourspositions;
      	for(int k = 0; k < mPositions.size(); ++k )
      	{

            if (index == k) continue;   

            std::vector<T> diff(mPositions[index].size());


            std::transform( mPositions[k].begin()
                          , mPositions[k].end()
                          , mPositions[index].begin()
                          , diff.begin()
                          , std::minus<T>());


            T norm = std::sqrt( std::inner_product( diff.begin(), diff.end(), diff.begin(), 0.0 ) );

      	   if ( norm <= mCutoff)
      	   {
      			neighbourspositions.push_back(diff);
      		}
      	}

      	return neighbourspositions;
      }

      /**
       *  @brief Returns the maximal number of nearest neighbours in this descriptor 
       *
       *  Purpose: Returns the maximal number of nearest neighbours in this descriptor 
       *
       **/
      size_t GetMaxNeighbours() const
      {
         size_t nmax = 0;
         for (int iat = 0; iat < mNatoms; iat++)
         {
            std::vector<std::vector<T>> neighbours = this->GetNNeighbourspositions(iat);
            nmax = std::max(nmax,neighbours.size());
         }

         return nmax;
      }

      int GetNatoms() const
      {
         return mNatoms;
      }

      int GetLmax() const
      {
         return mLmax;
      }


      T GetAlpha() const
      {
         //TODO Prepare for having more than one alpha
         return mAlpha[0];
      }


      /**
       *  @brief Returns the order of the derivative of this data point 
       *
       **/
      int GetDerivativeOrder() const
      {
         switch (this->mDataType)
         {
            case ENERGY:
            {
               return 0;
               break;
            }
            case GRADIENT:
            {
               return 1;
               break;
            }
            case HESSIAN:
            {
               return 2;
               break;
            }
            default:
            {
               return -1; // Error code
               break;
            }
         } 
      }

      /**
       *  @brief Return index of coordinate along which we take the 1. derivative 
       *
       **/
      int GetDerivCoord1() const
      {
         return this->mDerivCoord1;
      }

      /**
       *  @brief Return index of coordinate along which we take the 2. derivative 
       *
       **/
      int GetDerivCoord2() const
      {
         return this->mDerivCoord2;
      }

      void DefineAsGradElem(int idercoord)
      {
         mDataType = GRADIENT;
         mDerivCoord1 = idercoord;
         mDerivCoord2 = -1;
      }

      void DefineAsHessElem( int idercoord
                           , int jdercoord
                           )
      {
         mDataType = HESSIAN;
         mDerivCoord1 = idercoord;
         mDerivCoord2 = jdercoord;
      }
};


#endif /* MLDESCRIPTOR_INCLUDED */
