/**
************************************************************************
* 
* @file                
*
* Created:             
*
* Author:              
*
* Short Description:   
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<cstring>
#include<cstdlib>

#include "util/Io.h"
#include "util/Timer.h"
#include "util/Timer.h"
#include "ecor/ri_svd.h"
#include "ecor/reducerank.h"
#include "ecor/RIDecomposer.h"


#include "inc_gen/Warnings.h"
#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/TensorDecomposer.h"
#include "input/TensorDecompInput.h"

#include "tensor/hooi.h"

void RIDecomposer::Decompose(const NiceTensor<Nb>& target)
{
   const bool docheckerr = false;
   const Nb maxerr = false;

   /*-----------------------------------------------------------------
                    SVD guided start guess
   -------------------------------------------------------------------*/
   Timer svd_time;

   Mout << " start decomposition of RI integrals using SVD" << std::endl;

   NiceTensor<Nb> cp_bqint = ri_svd(6,target,false,1.e-12,true); 

   tucker_struct<Nb> tucker_tensor = hooi(target,1.e-14,20);

   svd_time.CpuOut(Mout, " Time for SVD guided decomposition (CPU)  ");
   svd_time.WallOut(Mout," Time for SVD guided decomposition (WALL) ");

   Timer svd_comp;

   Mout << " Compress high rank approximation...." << std::endl;

   if(!mDecompInfoSet.empty())
   {

     // do the decomposition
     for(auto& decompinfo : mDecompInfoSet)
     {

         midas::tensor::TensorDecomposer decomposer(decompinfo);

         //NiceTensor<Nb> intermediate = decomposer.Decompose(cp_bqint);
         NiceTensor<Nb> intermediate = cp_bqint;

         Timer timreduce;

         NiceTensor<Nb> newtensor = reducerank(intermediate, 1.e-14, 1.e-7, 20, docheckerr, maxerr, &decomposer);


         NiceTensor<Nb> diff = intermediate - newtensor;
  
         std::vector<unsigned int> dims = diff.GetDims();
         CanonicalTensor<Nb> diff_cp = *dynamic_cast<CanonicalTensor<Nb>*>(diff.GetTensor());

         SimpleTensor<Nb> intermed(diff_cp);
         Mout << "error-norm: " << intermed.Norm() <<  std::endl;
         //Mout << intermed << std::endl;

         timreduce.CpuOut( Mout, " Time for reduction (CPU)   ");
         timreduce.WallOut(Mout, " Time for reduction (WALL)  ");

         // Get info on tensor in canonical format
         CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(newtensor.GetTensor());
         unsigned int rank = canon->GetRank(); 
         Mout << " Rank after reduction: " << rank << std::endl;

      }
   }
   svd_comp.CpuOut(Mout, " Time for reduction (CPU)  ");
   svd_comp.WallOut(Mout," Time for reduction (WALL) ");

 
   /*-----------------------------------------------------------------
                    CP decomposition 
   -------------------------------------------------------------------*/
   if(!mDecompInfoSet.empty())
   {

      // do the decomposition
      for(auto& decompinfo : mDecompInfoSet)
      {
         midas::tensor::TensorDecomposer decomposer(decompinfo);

         Timer cp_time;   

         Mout << " start decompistion of RI integrals" << std::endl;

         // compose tensor
         NiceTensor<Nb> newtensor = decomposer.Decompose(target);
         CanonicalTensor<Nb> newtensor_cp = *dynamic_cast<CanonicalTensor<Nb>*>(newtensor.GetTensor());

         // make new SimpleTensor out of it 
         //SimpleTensor<Nb> intermed(newtensor_cp);
         NiceTensor<Nb> fitted_tensor( new SimpleTensor<Nb>(newtensor_cp) );
          
         NiceTensor<Nb> diff = target - fitted_tensor;
         Mout << "error-norm: " << diff.Norm() <<  std::endl;

         cp_time.CpuOut(Mout, " Time for CP decomposition (CPU)  ");
         cp_time.WallOut(Mout," Time for CP decomposition (WALL) ");

         // Get info on tensor in canonical format
         CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(newtensor.GetTensor());
         unsigned int rank = canon->GetRank(); 
         Mout << " Rank after decomposition: " << rank << std::endl;
      }
   }
   else
   {
      throw std::runtime_error("No DecompInfo in RIDecomposer");
   }

}

