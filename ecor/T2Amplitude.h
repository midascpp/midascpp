
/**
************************************************************************
* 
* @file                
*
* Created:            25-01-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for T2 amplitudes in the ABC format
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef T2Amplitude_H_INCLUDED
#define T2Amplitude_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <string>

#include "ecor/DFIntegrals.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorLibrarian.h"
#include "ecor/TurboInfo.h"
//#include "ecor/ri_svd.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/TensorDecomposer.h"
#include "input/TensorDecompInput.h"
#include "input/MolStruct.h"
#include "util/CallStatisticsHandler.h"

class T2Amplitude : RI4Integral
{
   private:
//      const unsigned int mMaxmem = 1000 * 1024*128; // buffer of max 1000 MiB

      std::map<std::string, std::vector<Nb>> mOrben;
      LaplaceQuadrature<Nb> mLapQuad;

      void CheckMemBuffer(unsigned int&, long&, unsigned int&, ofstream&, std::vector<NiceTensor<Nb>>&, std::vector<std::tuple<unsigned int,unsigned int>>&, std::vector<long>& );
      void ClearMemBuffer(unsigned int&, long&, unsigned int&, ofstream&, std::vector<NiceTensor<Nb>>&, std::vector<std::tuple<unsigned int,unsigned int>>&, std::vector<long>& );

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      T2Amplitude(RI3Integral&, RI3Integral&, std::map<std::string, std::vector<Nb>>, LaplaceQuadrature<Nb>&&, const bool);

      ///> Delete copy- and move constructor
      T2Amplitude(const T2Amplitude&)  = delete;
      T2Amplitude(T2Amplitude&&)       = delete;

      ///> Destructor
      ~T2Amplitude()
      {
      }

      /******** Methods ********/
      void Update(std::map<std::string, std::vector<Nb>>&, const unsigned int);
 
      std::tuple<Nb,Nb> EMP2(const bool);

      Nb RCEMP2J();
      Nb RCEMP2K();
      Nb ABCEMP2J();
      Nb ABCEMP2K();
 
};

#endif /* T2Amplitude_H_INCLUDED */
