#ifndef T1MOINTEGRALS_H_INCLUDED
#define T1MOINTEGRALS_H_INCLUDED

#include <functional>

#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorLibrarian.h"
#include "ecor/ReferenceState.h"
#include "util/Timer.h"

struct T1Integrals
{
   TensorLibrarian<Nb> g_T1;
   TensorLibrarian<Nb> L_T1;
   TensorLibrarian<Nb> F_T1;

   // Constructor 
   T1Integrals()=delete;

   T1Integrals (const NiceTensor<Nb>& ao_one_electron,
                const NiceTensor<Nb>& ao_two_electron,
                const map<std::string, NiceTensor<Nb>>& mo_coefficients,
                const NiceTensor<Nb>& t1_amplitudes):
      g_T1(4, multiindex_t{'v','o'}, std::vector<indexpermutation_t>{indexpermutation_t{ indexswap_t{0,2},
                                                                                         indexswap_t{1,3} }}
     //// Uncomment code below to blacklist vvvv block
//           ,std::vector<std::function<bool(const std::vector<unsigned int>&)>>
//            {
//               [](const std::vector<unsigned int>& key)
//               {
//                  return key==std::vector<unsigned int>{'v','v','v','v'};
//               }
//            }
          )

     ,L_T1(4, multiindex_t{'v','o'}, std::vector<indexpermutation_t>{indexpermutation_t{ indexswap_t{0,2},
                                                                                         indexswap_t{1,3} }})
     ,F_T1(2, multiindex_t{'v','o'}, std::vector<indexpermutation_t>{})
   {
      namespace X=contraction_indices;

      // T1 transformation matrices
      std::map<char, NiceTensor<Nb>> matrix_X;
      std::map<char, NiceTensor<Nb>> matrix_Y;
      
      cout << "Initializing T1 transformation matrices" << endl;
      matrix_X['o'] = mo_coefficients.at("o");
      matrix_X['v'] = (mo_coefficients.at("v") - contract(mo_coefficients.at("o")[X::p,X::k], t1_amplitudes[X::q,X::k]));
      matrix_Y['o'] = (mo_coefficients.at("o") + contract(mo_coefficients.at("v")[X::p,X::k], t1_amplitudes[X::k,X::q]));
      matrix_Y['v'] = mo_coefficients.at("v");



      // Initializing F_T1
      cout << "Initializing F_T1" << endl;
//      Timer f_time;
//      f_time.Reset();
//      NiceTensor<Nb> fock_partial = ao_one_electron 
//                              + (2.0*ao_two_electron.ContractForward(2, matrix_X.at('o')).ContractForward(3, matrix_Y.at('o'))[X::p,X::q,X::i,X::i]
//                              - ao_two_electron.ContractForward(1, matrix_Y.at('o')).ContractForward(2, 
//                                                                   matrix_X.at('o'))[X::p,X::i,X::i,X::q]).ToSimpleTensor();

      NiceTensor<Nb> fock_partial = ao_one_electron
                                 + (2.0*NiceTensor<Nb>(contract(contract(ao_two_electron[X::p,X::q,X::k,X::l], matrix_X.at('o')[X::k,X::r]),
                                                         matrix_Y.at('o')[X::l,X::s]))[X::p,X::q,X::i,X::i] 
                                       - NiceTensor<Nb>(contract(contract(ao_two_electron[X::p,X::k,X::l,X::s], matrix_Y.at('o')[X::k,X::q]), 
                                                         matrix_X.at('o')[X::l,X::r]))[X::p,X::i,X::i,X::q]).ToSimpleTensor();

      for(const auto& initpair: F_T1.InitializationLoop())
      {

         initpair.second = new NiceTensor<Nb>(NiceTensor<Nb>(contract(
                                                  contract(fock_partial[X::p,X::q], matrix_X[initpair.first[0]][X::p,X::k])
                                                                                  , matrix_Y[initpair.first[1]][X::q,X::l])));
      }
//      std::string f_out = "CPU time of F_T1 = ";
//      f_time.CpuOut(cout, f_out);




      // Initializing g_T1
      cout << "Initializing g_T1" << endl;
//      Timer g_time;
//      g_time.Reset();
      for(const auto& initpair: g_T1.InitializationLoop())
      {
//         initpair.second = new NiceTensor<Nb>(ao_two_electron.ContractForward(0, 
//                  matrix_X.at(initpair.first[0])).ContractForward(1, 
//                     matrix_Y.at(initpair.first[1])).ContractForward(2, 
//                        matrix_X.at(initpair.first[2])).ContractForward(3, 
//                           matrix_Y.at(initpair.first[3])));
         initpair.second = new NiceTensor<Nb>(contract(contract(contract(contract(
                                             ao_two_electron[X::l,X::m,X::n,X::o], matrix_X.at(initpair.first[0])[X::l,X::p]),
                                                                                   matrix_Y.at(initpair.first[1])[X::m,X::q]),
                                                                                   matrix_X.at(initpair.first[2])[X::n,X::r]),
                                                                                   matrix_Y.at(initpair.first[3])[X::o,X::s]));
      }
//      std::string g_out = "CPU time of g_T1 = ";
//      g_time.CpuOut(cout, g_out);

      
      // Initializing L_T1
      cout << "Initializing L_T1" << endl;
//      Timer l_time;
//      l_time.Reset();

      NiceTensor<Nb> ao_L = (2.0*ao_two_electron[X::p,X::q,X::r,X::s] - ao_two_electron[X::p,X::s,X::r,X::q]);

      for(const auto& initpair: L_T1.InitializationLoop())
      {
//         initpair.second = new NiceTensor<Nb>(ao_L.ContractForward(0, 
//                  matrix_X.at(initpair.first[0])).ContractForward(1, 
//                     matrix_Y.at(initpair.first[1])).ContractForward(2, 
//                        matrix_X.at(initpair.first[2])).ContractForward(3, 
//                           matrix_Y.at(initpair.first[3])));
         initpair.second = new NiceTensor<Nb>(contract(contract(contract(contract(
                                             ao_L[X::l,X::m,X::n,X::o], matrix_X.at(initpair.first[0])[X::l,X::p]),
                                                                                   matrix_Y.at(initpair.first[1])[X::m,X::q]),
                                                                                   matrix_X.at(initpair.first[2])[X::n,X::r]),
                                                                                   matrix_Y.at(initpair.first[3])[X::o,X::s]));
      }
//      std::string l_out = "CPU time of L_T1 = ";
//      l_time.CpuOut(cout, l_out);
//
      cout << "T1 transformation complete" << endl;
      cout << endl;
   }

   T1Integrals (const T1Integrals& other):
      g_T1( other.g_T1 )
     ,L_T1( other.L_T1 )
     ,F_T1( other.F_T1 )
   {
   }

   T1Integrals (T1Integrals&& other):
      g_T1()
     ,L_T1()
     ,F_T1()
   {
      std::swap(F_T1, other.F_T1);
      std::swap(g_T1, other.g_T1);
      std::swap(L_T1, other.L_T1);
   }

   T1Integrals& operator=(const T1Integrals& other)
   {
      // Mmmmh I read all over that I should never explicitly call the constructor,
      // but I don't see any other clean way of cleaning up before copying.
      this->~T1Integrals();

      g_T1 = other.g_T1;
      L_T1 = other.L_T1;
      F_T1 = other.F_T1;

      return *this;
   }

   T1Integrals& operator=(T1Integrals&& other)
   {
      std::swap(g_T1, other.g_T1);
      std::swap(L_T1, other.L_T1);
      std::swap(F_T1, other.F_T1);

      return *this;
   }

//   T1Integrals& update_integrals(NiceTensor<Nb>& t1)
//   {
//      this->g_T1 = 
//      this->L_T1 = 
//      this->F_T1 = 
//
//      return *this;
//   }
};
#endif /* T1MOINTEGRALS_H_INCLUDED */
