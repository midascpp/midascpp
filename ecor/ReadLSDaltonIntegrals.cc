#include "ReadLSDaltonIntegrals.h"

#include<vector>
#include<string>
#include<fstream>

#include "inc_gen/TypeDefs.h"
#include "tensor/CanonicalIntermediate.h"
#include "tensor/SimpleTensor.h"

void fill_simpletensorNb_from_file(NiceTensor<Nb>& tensor, const std::string& filename)
{
   std::ifstream infile;
   infile.exceptions( std::ios::badbit | std::ios::failbit );

   SimpleTensor<Nb>* simplehandle=dynamic_cast<SimpleTensor<Nb>*>(tensor.GetTensor());
   try
   {
      infile.open(filename, std::ios::in | std::ios::binary | std::ios::ate );
      infile.seekg(0, std::ios::beg);
      infile.read(reinterpret_cast<char*>(simplehandle->GetData()), simplehandle->TotalSize()*sizeof(Nb));
      infile.close();
   }
   catch(const std::ifstream::failure& e)
   {
      std::cerr << "Error while reading << " << filename << " >>" << std::endl;
      throw;
   }
}

NiceTensor<Nb> ReadLSDaltonOneElectronIntegrals(const std::string& filename, const unsigned int nbas)
{
   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(nbas),
                                      static_cast<unsigned int>(nbas)}
         )
   );
   fill_simpletensorNb_from_file(result, filename);
   return result;
}

NiceTensor<Nb> ReadLSDaltonTwoElectronIntegrals(const std::string& filename, const unsigned int nbas)
{
   NiceTensor<Nb> result(new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(nbas),
                                      static_cast<unsigned int>(nbas),
                                      static_cast<unsigned int>(nbas),
                                      static_cast<unsigned int>(nbas)}
   ));
   fill_simpletensorNb_from_file(result, filename);
   return result;
}

NiceTensor<Nb> ReadLSDaltonTHCIntegrals(const std::string& filename, const unsigned int nbas)
{
   std::ifstream input(filename, std::ios::in | std::ios::binary );

   int rank;
   read_binary(rank, input);

   int nbas_new;
   read_binary(nbas_new, input);

   if(nbas_new!=nbas)
   {
      throw std::runtime_error("NBas in file <"+filename+"> ("+std::to_string(nbas_new)+") different from previous NBas ("+std::to_string(nbas)+")");
   }

   Nb* x_matrix_transpose(new Nb[rank*nbas]);
   read_binary_array(x_matrix_transpose, rank*nbas, input);

   Nb** x_matrices(new Nb*[4]);
   x_matrices[0]=new Nb[rank*nbas];
   for(int i=0;i<nbas;++i)
   {
      for(int j=0;j<rank;++j)
      {
         x_matrices[0][i+nbas*j]=x_matrix_transpose[i*rank+j];
      }
   }
   delete[] x_matrix_transpose;

   // Make three more copies...
   for(int i=1;i<4;++i)
   {
      x_matrices[i]=new Nb[rank*nbas];
      memcpy(reinterpret_cast<void*>(x_matrices[i]),
             reinterpret_cast<void*>(x_matrices[0]),
             rank*nbas*sizeof(Nb));
   }

   Nb* z_matrix(new Nb[rank*rank]);
   read_binary_array(z_matrix, rank*rank, input);

   std::cout << nbas << std::endl;
   // Pack everything together
   NiceTensor<Nb> intermediate(
         new CanonicalIntermediate<Nb>(
            std::vector<unsigned int>{
               static_cast<unsigned int>(nbas), static_cast<unsigned int>(nbas),
               static_cast<unsigned int>(nbas), static_cast<unsigned int>(nbas)},
            x_matrices, rank, rank, std::vector<bool>{false, false, true, true}, z_matrix));

   NiceTensor<Nb> result( new CanonicalTensor<Nb>(
         dynamic_cast<CanonicalIntermediate<Nb>*>(intermediate.GetTensor())->ToCanonical()));

   return result;
}
