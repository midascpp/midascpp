
/**
************************************************************************
* 
* @file                
*
* Created:            26-05-2016         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for holding information necessary to 
*                     read and interprete Turbomole integral files
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TURBOINFO_HEADER
#define TURBOINFO_HEADER 

#include <iostream> 
#include <string> 
#include <vector> 
#include <string> 
#include <map>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "tensor/NiceTensor.h"

struct BasInfo
{
   unsigned int natoms;
   unsigned int nshells;

   In* Shells;
   In* Nbfat;
   In* Ishst;
   In* Ishnd;
};


class TurboInfo 
{
   private:
      /********************* Private members *********************/

      // general dimensions
      In mNOrb;
      In mNOcc;
      In mNVirt;
      In mNOct;
      In mNVirct;
      In mNAtoms;
      In mNAux;
      In mMXbfat;
      In mNPack;
      In mNXshell;
      In mNshsh;
      In mNBas;
      In mNShell;

      // index arrays 
      In* mIadr;
      In* mNijpair;

      BasInfo mXbas;
      BasInfo mBas;

      //Cmo's 
      std::map<std::string, NiceTensor<Nb>> mCmo; // CMO coefficients from Turbomole

      // internal info on status
      bool mIsInit = false; 

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      TurboInfo()                  = default;

      ///> Delete copy- and move constructor
      TurboInfo(const TurboInfo&)  = delete;
      TurboInfo(TurboInfo&&)       = delete;

      ///> Destructor
      ~TurboInfo() 
       {
         if (mIsInit)
         { 
            delete[] mIadr; 
            delete[] mNijpair;
            delete[] mBas.Shells;
            delete[] mBas.Nbfat;
            delete[] mBas.Ishst;
            delete[] mBas.Ishnd;
            delete[] mXbas.Shells;     
            delete[] mXbas.Nbfat;
            delete[] mXbas.Ishst;
            delete[] mXbas.Ishnd;
         }
      }
 
      /********** Getters **********/
      const NiceTensor<Nb>& GetCmoOcc()     const{return mCmo.at("o");}
      const NiceTensor<Nb>& GetCmoVir()     const{return mCmo.at("v");}     
 
      /******** ReadInfo ********/
      void ReadInfo(const std::string&);
      NiceTensor<Nb> Read3RI(const std::string&);
      NiceTensor<Nb> Read3RI(const std::string&, unsigned int, unsigned int);
      NiceTensor<Nb> ReadVPQ(const std::string&);
      NiceTensor<Nb> ReadCmocc(const std::string&, const int iat = -1);
      NiceTensor<Nb> ReadCmoct(const std::string&, const int iat = -1);
      NiceTensor<Nb> ReadCmvir(const std::string&, const int iat = -1);

   public:
      /******** Setters and Getters ********/
      In NAux()       const {return mNAux;}
      In NBas()       const {return mNBas;}
      In NOcc()       const {return mNOcc;}
      In NOct()       const {return mNOct;}
      In NVirt()      const {return mNVirt;}
      In NAtoms()     const {return mNAtoms;}
   
      In NBasAt(const int iat) const 
      {
         if (mIsInit) 
         {
            if (iat < 0)
            {
               // return number of all basis functions
               return mNBas;
            }
            else
            {
               return mBas.Nbfat[iat];
            }
         }
         else
         {
            return -1;
         }
      }

      In NBasOff(const int iat) const
      {
         if (mIsInit)
         {
            if (iat < 0) 
            {
               return 0;
            }
            else
            {
               int iist = mBas.Ishst[iat];
               return mBas.Shells[iist] + 1;
            }
         }
         else
         {
            return -1;
         }
      }

      /******** Operations ********/
      ///> Delete copy- and move assignment operators
      TurboInfo& operator=(const TurboInfo&) = delete;
      TurboInfo& operator=(TurboInfo&&)      = delete;
};
#endif // TURBOINFO_HEADER
