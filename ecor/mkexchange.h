
#include<cstring>
#include<cstdlib>

#ifndef MKEXCHANGE
#define MKEXCHANGE
         
#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"
#include "ecor/TurboInfo.h"
#include "tensor/TensorDecompInfo.h"
#include "ecor/ri_svd.h"
#include "ecor/reducerank.h"

namespace X=contraction_indices;


NiceTensor<Nb> mkexchange(const bool use_mobasis, const bool use_risvd, const Nb thrsvd, 
                          unsigned int nmaxrank, TurboInfo& turboinfo, midas::tensor::TensorDecompInfo::Set decompinfoset)
{

   // constants
   const bool locdbg = false;
   const bool docheckerr = false;
   const Nb maxerr = 0.0;

   // locals
   unsigned int nbasi,nbasj,nbask,nbasl;
   unsigned int iboff,jboff,kboff,lboff;
   unsigned int nrank = 0;

   // set some shortcuts
   In nocc = turboinfo.NOcc();
   In noct = turboinfo.NOct();
   In nvir = turboinfo.NVirt();
   In nfroz = nocc - noct;
   In natoms = turboinfo.NAtoms();
   In nbas = turboinfo.NBas();
   In naux = turboinfo.NAux();

   // read (P|Q)^{-1}
   NiceTensor<Nb> vpqinv = turboinfo.ReadVPQ("MIDAS_VPQINV");


   // allocate maximal needed memory for mode matrices... (lots of memory)
   unsigned int ndim = 4;
   std::vector<unsigned int> dims;
   if (use_mobasis)
   {
      dims = {static_cast<unsigned int>(nvir)
             ,static_cast<unsigned int>(noct)
             ,static_cast<unsigned int>(nvir)
             ,static_cast<unsigned int>(noct)} ;
   }
   else
   {
      dims = {static_cast<unsigned int>(nbas)
             ,static_cast<unsigned int>(nbas)
             ,static_cast<unsigned int>(nbas)
             ,static_cast<unsigned int>(nbas)} ;

   }

   Nb** mode_matrices = new Nb*[ndim];
   mode_matrices[0] = new Nb[dims[0]*nmaxrank];
   mode_matrices[1] = new Nb[dims[1]*nmaxrank];
   mode_matrices[2] = new Nb[dims[2]*nmaxrank];
   mode_matrices[3] = new Nb[dims[3]*nmaxrank];

   int nranktot = 0;
   int nrun = 1;   

   // get decompinfo
   TensorDecompInfo decompinfo = *(decompinfoset.begin());
   // get decomposer
   midas::tensor::TensorDecomposer decomposer(decompinfo);

   for (unsigned int iat = 0; iat<natoms; ++iat)
   {
      nbasi = turboinfo.NBasAt(iat);
      iboff = turboinfo.NBasOff(iat);
      NiceTensor<Nb> cmvi = turboinfo.ReadCmvir("MIDAS_CMO",iat);

      for (unsigned int jat = 0; jat<natoms; ++jat)
      //for (unsigned int jat = 0; jat<=iat; ++jat)
      {
         nbasj = turboinfo.NBasAt(jat);
         jboff = turboinfo.NBasOff(jat);
         NiceTensor<Nb> cmoj = turboinfo.ReadCmoct("MIDAS_CMO",jat);

         // read in (Q, mu nu) for atom pair iat,jat
         if (locdbg) Mout << "Read integrals for atoms " << iat << " and " << jat << " (I)" << std::endl;
         NiceTensor<Nb> bqij = turboinfo.Read3RI("MIDAS_3ATX",iat,jat);

         // do decomposition
         if (use_risvd) 
         {
            // use two step procedure where we create an inital high rank tensor with large accuracy
            bqij =  ri_svd(0,bqij,true,thrsvd,true,naux,nbasi,nbasj); 
         }
         CanonicalTensor<Nb>* bqij_canon=dynamic_cast<CanonicalTensor<Nb>*>(bqij.GetTensor());
         unsigned int rankij = bqij_canon->GetRank();
         if (rankij > 10)
         { 
            bqij = reducerank(bqij, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);
         }

         if (use_mobasis) 
         {
            bqij = bqij.ContractForward(1, cmvi[X::q,X::p])
                       .ContractForward(2, cmoj[X::q,X::p]);

            // reduce rank since a smaller one should be sufficient now 
            if (rankij > 10) 
            {
               bqij = reducerank(bqij, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);
            }
         }
         
         // multiply with V^{-1}_{PQ}
         NiceTensor<Nb> cpij = contract(vpqinv[X::b,X::a], bqij[X::b,X::c,X::d]);

         for (unsigned int kat = 0; kat<natoms; ++kat)
         //for (unsigned int kat = 0; kat<=iat; ++kat)
         {
            nbask = turboinfo.NBasAt(kat);
            kboff = turboinfo.NBasOff(kat);
            NiceTensor<Nb> cmvk = turboinfo.ReadCmvir("MIDAS_CMO",kat);

            //unsigned int lmaxat;
            //if (kat == iat) {lmaxat = jat;} else { lmaxat = kat;}
            for (unsigned int lat = 0; lat<natoms; ++lat)
            //for (unsigned int lat = 0; lat<=lmaxat; ++lat)
            {
               nbasl = turboinfo.NBasAt(lat);
               lboff = turboinfo.NBasOff(lat);
               NiceTensor<Nb> cmol = turboinfo.ReadCmoct("MIDAS_CMO",lat);

               // read in (Q, mu nu) for atom pair lat,kat
               if (locdbg) Mout << "Read integrals for atoms " << kat << " and " << lat << " (II)" << std::endl;
               NiceTensor<Nb> bqkl = turboinfo.Read3RI("MIDAS_3ATX",kat,lat);

               // do decomposition
               if (locdbg) Mout << "do decomposition" << std::endl;
               if (use_risvd) 
               {
                  // use two step procedure where we create an inital high rank tensor with large accuracy
                  bqkl =  ri_svd(0,bqkl,true,thrsvd,true,naux,nbask,nbasl); 
               }

               CanonicalTensor<Nb>* bqkl_canon=dynamic_cast<CanonicalTensor<Nb>*>(bqkl.GetTensor());
               unsigned int rankkl = bqkl_canon->GetRank();
               if (rankkl > 10)
               {
                  bqkl = reducerank(bqkl, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);
               }

               if (use_mobasis) 
               {
                  bqkl = bqkl.ContractForward(1, cmvk[X::q,X::p])
                             .ContractForward(2, cmol[X::q,X::p]);

                  // reduce rank since a smaller one should be sufficient now 
                  if (rankkl > 10)
                  {
                     bqkl = reducerank(bqkl, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);
                  }
               }

               // assemble
               if (locdbg) Mout << "assemble" << std::endl;
               NiceTensor<Nb> kint = contract(bqkl[X::p,X::a,X::b], cpij[X::p,X::c,X::d]);

               if (locdbg) 
               {
                  if (use_mobasis)
                  {
                     Mout << "kint (MO basis)" << std::endl;
                  }
                  else
                  {
                     Mout << "kint (AO basis)" << std::endl;
                  }
                  Mout << kint << std::endl;
               }

               if (locdbg) Mout << "do reduction" << std::endl;
               kint = decomposer.Decompose(kint);

               if (locdbg) 
               {
                  Mout << "kint" << std::endl;
                  Mout << kint << std::endl;
               }

               /*******************************************
                    patch sub tensor onto larger one
               ********************************************/

               // Get handles
               if (locdbg) Mout << "Get rank of canonical tensor" << std::endl;
               CanonicalTensor<Nb>* k_canon;
               k_canon=dynamic_cast<CanonicalTensor<Nb>*>(kint.GetTensor());
               Nb** matrices = k_canon->GetModeMatrices();
               unsigned int rank = k_canon->GetRank();

               nrank = std::max<In>(rank,nrank);


               // exploit permutationl symmetry
        
               //  if (use_mobasis) 
               //  {
               //     // we only have the trivial symmatry (ai|bj) = (bj|ai) but still something :-)
               //     unsigned int iadr, iadrloc, idimloc;
               //     unsigned int nperm = 2;
               //    
               //     std::vector<unsigned int> idim1 = {0,1,2,3};
               //     std::vector<unsigned int> idim2 = {2,3,0,1};
               //     std::vector<std::vector<unsigned int> > ivecdim ={idim1, idim2};

               //     for (unsigned int iperm = 0; iperm<nperm; ++iperm) 
               //     {
               //        for (unsigned int irank = 0; irank<rank; ++irank)
               //        {
               //           for (unsigned int idim = 0; idim<ndim; ++idim) 
               //           {
               //              // now enter non zero elements (which are all in the MO basis)
               //              for (unsigned int ibas = 0; ibas < dims[idim]; ++ibas)
               //              {
               //                 idimloc = ivecdim[iperm][idim];
               //                 iadr = (irank + nranktot)*dims[idim] + ibas;
               //                 iadrloc = irank*dims[idimloc] + ibas;
               //                 mode_matrices[idim][iadr] = matrices[idimloc][iadrloc];
               //              }
               //           
               //           }
               //        }
   
               //        nranktot += rank;
               //     }
               //  }

               //std::vector<unsigned int> invec = {static_cast<unsigned int>(kboff)
               //                                  ,static_cast<unsigned int>(lboff)
               //                                  ,static_cast<unsigned int>(iboff)
               //                                  ,static_cast<unsigned int>(jboff)};

               //std::sort(invec.begin(), invec.end());
               //std::vector<std::vector<unsigned int> > ioff;
               //do 
               //{
               //   ioff.push_back(invec);
               //} 
               //while (std::next_permutation(invec.begin(), invec.end()));
 
               //unsigned int nperm = ioff.size();

               //std::cout << "nperm " << nperm << std::endl;
 

               //std::map< unsigned int, unsigned int> nbasmap; 

               //nbasmap[iboff] = nbasi; nbasmap[jboff] = nbasj;
               //nbasmap[kboff] = nbask; nbasmap[lboff] = nbasl;

 
               //for (unsigned int iperm = 0; iperm<nperm; ++iperm) 
               //{
                  unsigned int iadr, iadrloc;
                  std::vector<unsigned int> dimsloc;
                  std::vector<unsigned int> iioff;

                  if (locdbg) Mout << "Set dimensions for copy operation" << std::endl;

                  if (use_mobasis)
                  {
                     dimsloc = {static_cast<unsigned int>(nvir)
                               ,static_cast<unsigned int>(noct)
                               ,static_cast<unsigned int>(nvir)
                               ,static_cast<unsigned int>(noct)} ;

                     iioff = {static_cast<unsigned int>(0)
                             ,static_cast<unsigned int>(0)
                             ,static_cast<unsigned int>(0)
                             ,static_cast<unsigned int>(0)} ;

                  }
                  else
                  {
                     dimsloc = {static_cast<unsigned int>(nbask)
                               ,static_cast<unsigned int>(nbasl)
                               ,static_cast<unsigned int>(nbasi)
                               ,static_cast<unsigned int>(nbasj)} ;

                     iioff = {static_cast<unsigned int>(kboff)
                             ,static_cast<unsigned int>(lboff)
                             ,static_cast<unsigned int>(iboff)
                             ,static_cast<unsigned int>(jboff)} ;
                  }

                  //std::vector<unsigned int> iioff = ioff[iperm];

                  //std::vector<unsigned int> dimsloc = {static_cast<unsigned int>(nbasmap[iioff[0]])
                  //                                    ,static_cast<unsigned int>(nbasmap[iioff[1]])
                  //                                    ,static_cast<unsigned int>(nbasmap[iioff[2]])
                  //                                    ,static_cast<unsigned int>(nbasmap[iioff[3]])} ;


                  std::cout << " i j k l " <<  iat << " " << jat << " " << kat << " " << lat << " iioff " << iioff << std::endl;

                  for (unsigned int irank = 0; irank<rank; ++irank)
                  {
                     for (unsigned int idim = 0; idim<ndim; ++idim) 
                     {
                        // first zero out all elements (not necessary if we are in the MO basis)
                        if (!use_mobasis) 
                        {
                           for (unsigned int isbf = 0; isbf < dims[idim]; ++isbf)
                           {
                              iadr = (irank + nranktot)*nbas + isbf;
                              mode_matrices[idim][iadr] = 0.0;
                           }
                        }

                        // now enter non zero elements (which are all in the MO basis)
                        for (unsigned int ibas = 0; ibas < dimsloc[idim]; ++ibas)
                        {
                           iadr = (irank + nranktot)*dims[idim] + ibas + iioff[idim];
                           iadrloc = irank*dimsloc[idim] + ibas;
                           mode_matrices[idim][iadr] = matrices[idim][iadrloc];
                        }
                        
                     }
                  }

                  nranktot += rank;
               //}

               if (nranktot > nmaxrank/natoms * pow(nrun,2.0) ) 
               {
                  Mout << "rank reduction from " << nranktot << std::endl;

                  // if total rank exceeds a certain value do rank reduction...
                  Nb** tmp = new Nb*[ndim];
                  tmp[0] = new Nb[dims[0]*nmaxrank];
                  tmp[1] = new Nb[dims[1]*nmaxrank];
                  tmp[2] = new Nb[dims[2]*nmaxrank];
                  tmp[3] = new Nb[dims[3]*nmaxrank];

                  NiceTensor<Nb> intermediate (new CanonicalTensor<Nb>(dims, nranktot, tmp));
                 
                  for (int idim = 0; idim<ndim; ++idim)
                  {
                     memcpy(reinterpret_cast<void*>(tmp[idim]),reinterpret_cast<void*>(mode_matrices[idim]),dims[idim]*nmaxrank*sizeof(Nb));
                  }
 
                  intermediate = reducerank(intermediate, 1.e-14, 1.e-7, 20, docheckerr, maxerr, &decomposer);

                  CanonicalTensor<Nb>* canon=dynamic_cast<CanonicalTensor<Nb>*>(intermediate.GetTensor());
                  Nb** matrices = canon->GetModeMatrices(); 
                  unsigned int nrank = canon->GetRank();

                  Mout << " (I) rank after reduction: " << nrank << std::endl;
                  nranktot = nrank;

                  for (int idim = 0; idim<ndim; ++idim)
                  {
                     memcpy(reinterpret_cast<void*>(mode_matrices[idim]),reinterpret_cast<void*>(matrices[idim]),dims[idim]*nmaxrank*sizeof(Nb));
                  }
                  
               }
               nrun += 1;

            } // atom l
         } // atom k 
      } // atom j
   } // atom i 

   // patch everything together to the full integral tensor
   NiceTensor<Nb> full_k (new CanonicalTensor<Nb>(dims, nranktot, mode_matrices));

   // do rank reduction
   //full_k = decomposer.Decompose(full_k);
   Mout << " rank reduction of full_k" << std::endl;
   full_k = reducerank(full_k, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);

   CanonicalTensor<Nb>* k_canon=dynamic_cast<CanonicalTensor<Nb>*>(full_k.GetTensor());
   unsigned int rank = k_canon->GetRank();

   Mout << " rank after incr. built: " << nranktot << std::endl;
   Mout << " rank after reduction:   " << rank << std::endl;

   return full_k;
}

#endif /* MKEXCHANGE */
