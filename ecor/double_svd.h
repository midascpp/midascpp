#include<cstring>
#include<cstdlib>

#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"

// Decompose NxNxNxN tensor into a canonical form of rank N^5
// (total storage cost is 4N^6). Accurate but ridiculously oversized.
template <class T>
NiceTensor<T> double_svd(const NiceTensor<T>& target, const T threshold)
{
   std::vector<unsigned int> dims=target.GetDims();
   const unsigned int ndim(target.NDim());
   if((ndim!=4) || !std::all_of(dims.begin(), dims.end(), [&dims](unsigned int v){return v==dims[0];}))
   {
      throw std::runtime_error("double_svd only works for 4th hyper-cubic order tensors!");
   }
   const size_t totalsize(target.TotalSize());

   // Transform NxNxNxN tensor into N^2 x N^2 matrix
   T* tmp(new T[totalsize]);
   memcpy(reinterpret_cast<void*>(tmp),
          reinterpret_cast<void*>(dynamic_cast<SimpleTensor<T>*>(target.GetTensor())->GetData()),
          totalsize*sizeof(T));

   NiceTensor<T> matricized_target(new SimpleTensor<T>(std::vector<unsigned int>{dims[0]*dims[1],dims[2]*dims[3]}, tmp));

   // Do first SVD: g_{ijkl}=\sum_r a_{(ij),r} b_{(kl),r}
   NiceTensor<T> svd(matricized_target.ToCanonicalTensor(threshold));

   // Get handles
   CanonicalTensor<T>* svd_canon=dynamic_cast<CanonicalTensor<T>*>(svd.GetTensor());
   T** matrices     =svd_canon->GetModeMatrices();
   unsigned int rank=svd_canon->GetRank();

   unsigned int total_rank=0;

   // Allocate output
   T** result_mode_matrices=new T*[ndim];
   result_mode_matrices[0]=new T[dims[0]* totalsize];
   result_mode_matrices[1]=new T[dims[1]* totalsize];
   result_mode_matrices[2]=new T[dims[2]* totalsize];
   result_mode_matrices[3]=new T[dims[3]* totalsize];

   // Pointers to output matrices
   T* ptr_out[4];
   for(int i=0;i<4;++i)
   {
      ptr_out[i]=result_mode_matrices[i];
   }

   // For each rank obtained from the first SVD
   for(int j=0;j<rank;++j)
   {
      // Left (first and second dimensions)
      // Transform a_{(ij),r} -> a^r_{i,j}
      T* tmp_left(new T[dims[0]*dims[1]]);
      memcpy(reinterpret_cast<void*>(tmp_left),
             reinterpret_cast<void*>(matrices[0]+j*dims[0]*dims[1]),
             dims[0]*dims[1]*sizeof(T));
      // Make SimpleTensor out of new matrix and decompose a^r_{i,j}=\sum a0^r_{i,s} a1^r_{j,s}
      NiceTensor<T> left_cp(
            (NiceTensor<T>(
                  new SimpleTensor<T>(std::vector<unsigned int>{dims[0],dims[1]}, tmp_left))
            ).ToCanonicalTensor(threshold) );
      unsigned int left_rank=dynamic_cast<CanonicalTensor<T>*>(left_cp.GetTensor())->GetRank();

      // Right (third and fourth dimensions)
      // Transform b_{(kl),r} -> b^r_{k,l}
      T* tmp_right(new T[dims[2]*dims[3]]);
      memcpy(reinterpret_cast<void*>(tmp_right),
             reinterpret_cast<void*>(matrices[1]+j*dims[2]*dims[3]),
             dims[2]*dims[3]*sizeof(T));
      // Make SimpleTensor out of new matrix and decompose b^r_{k,l}=\sum b0^r_{k,s} b1^r_{l,s}
      NiceTensor<T> right_cp( (NiceTensor<T>(new SimpleTensor<T>(std::vector<unsigned int>{dims[2],dims[3]}, tmp_right))).ToCanonicalTensor(threshold) );
      unsigned int right_rank=dynamic_cast<CanonicalTensor<T>*>(right_cp.GetTensor())->GetRank();

      total_rank+=left_rank*right_rank;

      // Copy left_matrices into final tensor
      // We replicate each *matrix* right_rank times
      T** left_matrices=dynamic_cast<CanonicalTensor<T>*>(left_cp.GetTensor())->GetModeMatrices();
      for(int i=0;i<2;++i)
      {
         for(int iright=0;iright<right_rank;++iright)
         {
            T* ptr_in=left_matrices[i];
            for(int irank=0;irank<left_rank;++irank)
            {
               for(int k=0;k<dims[i];++k)
               {
                  *(ptr_out[i]++)=*(ptr_in++);
               }
            }
         }
      }

      // Copy right_matrices into final tensor
      // We replicate each *row* left_rank times
      T** right_matrices=dynamic_cast<CanonicalTensor<T>*>(right_cp.GetTensor())->GetModeMatrices();
      for(int i=0;i<2;++i)
      {
         T* ptr_row=right_matrices[i];
         for(int irank=0;irank<right_rank;++irank)
         {
            T* ptr_in;
            for(int ileft=0;ileft<left_rank;++ileft)
            {
               ptr_in=ptr_row;
               for(int k=0;k<dims[i+2];++k)
               {
                  *(ptr_out[2+i]++)=*(ptr_in++);
               }
            }
            ptr_row=ptr_in;
         }
      }
   }

   // If any of the SVD's yielded a lower rank than the maximum estimated rank,
   // we trim off the last rows by reallocating
   for(unsigned int idim=0;idim<4;++idim)
   {
      result_mode_matrices[idim] =
         reinterpret_cast<double*>(realloc(result_mode_matrices[idim], total_rank*dims[idim]*sizeof(T)));
   }

   // Pack everything into a CanonicalTensor and return
   return NiceTensor<T> (new CanonicalTensor<T>(dims, total_rank, result_mode_matrices));
}
