/**
************************************************************************
* 
* @file                
*
* Created:             
*
* Author:              
*
* Short Description:  
*                     
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream> 
#include <string> 
#include <vector> 
#include <string> 

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorDecompInfo.h"


class RIDecomposer
{
   private:
      /********************* Private members *********************/
      TensorDecompInfo::Set   mDecompInfoSet;                  ///< Set of TensorDecompInfo to control decompositions

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      RIDecomposer()                     = default;

      ///> Delete copy- and move constructor
      RIDecomposer(const RIDecomposer&)  = delete;
      RIDecomposer(RIDecomposer&&)       = delete;

      ///> Destructor
      ~RIDecomposer()                    = default;
      

      /******** Decompose ********/
      void Decompose(const NiceTensor<Nb>& target);


   public:
      /******** Setters and Getters ********/

      ///> Get/Set the set of TensorDecompInfo
      TensorDecompInfo::Set&  GetDecompInfoSet()                              { return mDecompInfoSet; }
      void                    SetDecompInfoSet(TensorDecompInfo::Set& aSet)   { mDecompInfoSet = aSet; }


      /******** Operations ********/
      ///> Delete copy- and move assignment operators
      RIDecomposer& operator=(const RIDecomposer&) = delete;
      RIDecomposer& operator=(RIDecomposer&&)      = delete;
};

