/**
************************************************************************
* 
* @file                
*
* Created:            25-01-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class to hold intermediates in Memory and on disk 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef IntermedCont_H_INCLUDED
#define IntermedCont_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <string>

#include "tensor/NiceTensor.h"


class IntermedCont 
{
   private:

      struct xinfo
      {
      
         bool isincore = false;
         bool isondisk = false;
      
         long iadrfil = -1;
         long iadrmem = -1;
      
      };

      unsigned int mMaxbuff;        ///> Maximal size of buffer
      unsigned int mMaxelem;        ///> Maximal number of elements we need to store
      unsigned int mElemSize;       ///> Maximal size of the individual element

      unsigned int mInmem;          ///> Current index in memory pool
      long mIadr;                   ///> Current start address in file

      std::vector<xinfo> mList;                      ///> Simple Book keeping of elements 
      std::vector<unsigned int> mInMemory;           ///> Simple Book keeping of elements 
      std::vector<std::shared_ptr<Nb>> mBuffer;      ///> Actual buffer holding intermediates in memory  

      bool mDoCopy;       ///> Do a true copy or just add a new shared pointer to memory location

      fstream mFint;
      std::string mFile;


      long SaveRawData
         (  Nb* data
         ,  unsigned int iadrstart
         ,  unsigned int ndata
         )
      {
         // constants
         const bool locdbg = false;
      
         // set to start position for this intermediate
         mFint.seekg(iadrstart);
      
         // write header
         mFint.write((const char*)&ndata, sizeof(unsigned int));
      
         // write data
         mFint.write((const char*)&data[0], ndata * sizeof(Nb));
      
         long iadr = mFint.tellp(); 
      
         return iadr;
      }
      
      void ReadRawData
         (  long iadr
         ,  Nb* data
         ,  unsigned int &ndata
         )
      {
      
         // set to start position for this intermediate
         mFint.seekg(iadr);
      
         // read header
         mFint.read( reinterpret_cast<char*>(&ndata), sizeof(unsigned int) );
      
         mFint.read( reinterpret_cast<char*>(&data[0]), ndata * sizeof(Nb) ); 
      
      }

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      IntermedCont
         (  unsigned int maxbuff
         ,  unsigned int maxelem
         ,  unsigned int nsize
         ,  std::string file
         )
      {
         mMaxbuff = maxbuff;
         mMaxelem = maxelem;
         mElemSize = nsize;

         mFile = file;
         mFint.open(file, std::fstream::in | std::fstream::out | std::fstream::app | ios::binary);

         // allocate memory
         mBuffer.resize(maxbuff);
         for (unsigned int i = 0; i < maxbuff; i++)
         {
            mBuffer[i].reset(new Nb[nsize]);
         } 
         mList.resize(maxelem);
         mInMemory.resize(maxelem);

         // init counter
         mInmem = 0;
         mIadr = 0;

         mDoCopy = false;
      }

      ///> Delete copy- and move constructor
      IntermedCont(const IntermedCont&)  = delete;
      IntermedCont(IntermedCont&&)       = delete;

      ///> Destructor
      ~IntermedCont()
      {
         // close intermediate file
         mFint.close(); 

         // and delete it
         std::set<std::string> files2delete;
         files2delete.insert(mFile);
         set<std::string>::iterator iter;
         for (iter = files2delete.begin(); iter != files2delete.end(); ++iter)
         {        
            std::string file = *iter;
            const char* cfile = file.c_str();
            remove(cfile);
         }   
      }

      /******** Methods ********/

      void AddIntermediate
         (  unsigned int ielem
         ,  Nb* pintermed
         ,  unsigned int nelem
         )
      {

         // constants
         const bool locdbg = false;

         if (locdbg) Mout << "Add Intermediate to Memory/Disk" << std::endl;

         // set buffer info
         mList[ielem].isincore = true;
         mList[ielem].iadrmem  = static_cast<long>(mInmem);

         // Sanity check
         if (nelem > mElemSize)
         {
            MIDASERROR("Element size too small");
         }

         if (locdbg) Mout << "I will copy " << nelem << " elements" << std::endl;

         // Add intermediate to buffer
         memcpy(reinterpret_cast<void*>(mBuffer[mInmem].get()),
                reinterpret_cast<void*>(pintermed), nelem * sizeof(Nb));

         if (locdbg) Mout << "end copy" << std::endl;

         mInMemory[mInmem] = ielem; 

         // increment buffer counter or dump everything to file
         if (mInmem < mMaxbuff - 1 )
         {
            mInmem += 1;
         }
         else if (ielem != mMaxelem - 1)
         {
            // if the buffer will be full in the next cycle 
            // and still elements will be written 
            // dump intermediates to file
            
            LOGCALL("IO to dump data intermed");

            if (locdbg) Mout << "Save intermediate on disk" << std::endl;

            unsigned int idx;

            for (unsigned int i = 0; i < mInmem + 1; ++i)
            {
               idx = mInMemory[i];

               mList[idx].isondisk = true;
               mList[idx].iadrfil  = mIadr;
               mList[idx].isincore = false;
               mList[idx].iadrmem  = static_cast<long>(-1);

               long ipos = SaveRawData(mBuffer[i].get(), mIadr, mElemSize);
               mIadr = ipos;

            }
            mInmem = 0;
         }
      }

      void GetIntermediate
         (  std::shared_ptr<Nb> &intermed
         ,  unsigned int cdpair
         )
      {
         // constants
         const bool locdbg = false;

         if (locdbg) Mout << "Rad Intermediate from Memory/Disk" << std::endl;
         
         if (mList[cdpair].isincore)
         {
            // We still have the intermediate in the write buffer, 
            // so we can just point to this location 
            long idx = mList[cdpair].iadrmem;
            if (mDoCopy) 
            {
               // Copy the result to the new location the pointer points to
               memcpy(reinterpret_cast<void*>(intermed.get()),
                      reinterpret_cast<void*>(mBuffer[idx].get()), mElemSize * sizeof(Nb));
            }
            else
            {
               // Just point to the same location at the Buffer
               intermed = mBuffer[idx];
            }
         
         }
         else if (mList[cdpair].isondisk)
         {
            // We read the intermediate from disk... slow process but better
            // than the recomputation each time...
            unsigned int mdata;
            long iadr = mList[cdpair].iadrfil;
            intermed.reset(new Nb[mElemSize]);
            ReadRawData(iadr, intermed.get(), mdata);

            if (mElemSize != mdata)
            {
               Mout << "Read data: " << mdata << " expected data: " << mElemSize << std::endl;
               MIDASERROR("Dimensions for reading do not agree!");
            }
         }
         else
         {
            MIDASERROR("Try to read intermediate from disk, which does not exsit!");
         }
      }

 
};

#endif /* IntermedCont_H_INCLUDED */
