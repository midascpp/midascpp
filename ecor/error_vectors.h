#ifndef ERROR_VECTORS_H_INCLUDED
#define ERROR_VECTORS_H_INCLUDED

#include<map>
#include<vector>

#include"tensor/NiceTensor.h"
#include"tensor/Scalar.h"
#include"tensor/EnergyDifferencesTensor.h"

/** Standard error vector **/
template <class T>
std::vector<NiceTensor<T>> cc2_error_vector(
      const std::vector<NiceTensor<T>>& t,
      const T1Integrals& integrals,
      const std::map<std::string, std::vector<T>>& orbital_energies,
      const bool& do_cp,
      const Nb& cp_threshold)
{
   namespace X=contraction_indices;

   NiceTensor<T> energy_differences(new EnergyDifferencesTensor<T>(2,
                                orbital_energies.at("v"),
                                orbital_energies.at("o")));
   const TensorLibrarian<Nb>& f = integrals.F_T1;
   const TensorLibrarian<Nb>& g = integrals.g_T1;
  
   NiceTensor<Nb> u = 2.0*t[2] - t[2][X::p,X::s,X::r,X::q];
   if(do_cp) {u = u.ToCanonicalTensor(cp_threshold);}

   std::vector<NiceTensor<T>> result(3);

   /***** Zeroth error vector (not important) *****/
   result[0] = (new Scalar<Nb>(0.0));

   /***** Singles error vector *****/
   result[1] = (NiceTensor<Nb>(contract(u[X::c,X::k,X::d,X::q], g["vvov"][X::p,X::d,X::k,X::c])
                   -contract(u[X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c])
                   +contract(u[X::p,X::q,X::c,X::k], f["ov"][X::k,X::c]))).ToSimpleTensor()
                   +f["vo"];

   /***** Doubles error vector *****/
   result[2] = (t[2]*energy_differences + g["vovo"]);

   return result;
}



template <class T>
std::vector<NiceTensor<T>> ccsd_error_vector(
      const std::vector<NiceTensor<T>>& t,
      const T1Integrals& integrals,
      const std::map<std::string, std::vector<T>>& orbital_energies,
      const bool& do_cp,
      const Nb& cp_threshold)
{
   namespace X=contraction_indices;

   const TensorLibrarian<Nb>& l = integrals.L_T1;
   const TensorLibrarian<Nb>& f = integrals.F_T1;
   const TensorLibrarian<Nb>& g = integrals.g_T1;
   
   std::vector<NiceTensor<T>> result(3);

   NiceTensor<Nb> u = (2.0*t[2][X::p,X::q,X::r,X::s] - t[2][X::p,X::s,X::r,X::q]);
   if(do_cp) {u = u.ToCanonicalTensor(cp_threshold);}

   
   /***** Zeroth error vector (not important) *****/
   result[0] = (new Scalar<Nb>(0.0));

   /***** Singles error vector *****/
   result[1] = (NiceTensor<Nb>(contract(u[X::c,X::k,X::d,X::q], g["vvov"][X::p,X::d,X::k,X::c])
                   -contract(u[X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c])
                   +contract(u[X::p,X::q,X::c,X::k], f["ov"][X::k,X::c]))).ToSimpleTensor()
                   +f["vo"];

   /***** Doubles error vector *****/
   result[2] = g["vovo"] + contract(t[2][X::c,X::q,X::d,X::s], g["vvvv"][X::p,X::c,X::r,X::d]);          // A term
   if(do_cp) {result[2] = result[2].ToCanonicalTensor(cp_threshold);}

   {
      NiceTensor<Nb> iB = g["oooo"] + contract(t[2][X::c,X::q,X::d,X::s], g["ovov"][X::p,X::c,X::r,X::d]);
      if(do_cp) {iB = iB.ToCanonicalTensor(cp_threshold);}
      result[2] += contract(t[2][X::p,X::k,X::r,X::l], iB[X::k,X::q,X::l,X::s]);                   // B term added
   }

   {
      NiceTensor<Nb> iC1 = g["oovv"] - 0.5*contract(t[2][X::r,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::s]);
      if(do_cp) {iC1 = iC1.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> iC2 = contract(t[2][X::r,X::k,X::c,X::s], iC1[X::k,X::q,X::p,X::c]);
      result[2] += -0.5*iC2 - iC2[X::p,X::s,X::r,X::q] - 0.5*iC2[X::r,X::s,X::p,X::q] - iC2[X::r,X::q,X::p,X::s];
   }

   {
      NiceTensor<Nb> iD = l["voov"] + 0.5*contract(u[X::p,X::q,X::d,X::l], l["ovov"][X::l,X::d,X::r,X::s]);
      if(do_cp) {iD = iD.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> term_D = 0.5*contract(u[X::r,X::s,X::c,X::k], iD[X::p,X::q,X::k,X::c]);
      result[2] += term_D + term_D[X::r,X::s,X::p,X::q];                                           // D term added
   }

   {
      const NiceTensor<Nb> iE1 = (f["vv"] - contract(u[X::p,X::k,X::d,X::l], g["ovov"][X::l,X::d,X::k,X::q])).ToSimpleTensor();
      const NiceTensor<Nb> term_E1 = contract(t[2][X::p,X::q,X::c,X::s], iE1[X::r,X::c]);
      result[2] += term_E1 + term_E1[X::r,X::s,X::p,X::q];                                         // E1 term added
   }

   {
      const NiceTensor<Nb> iE2 = (f["oo"] + contract(u[X::c,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::c])).ToSimpleTensor();
      const NiceTensor<Nb> term_E2 = -1.0*contract(t[2][X::p,X::q,X::r,X::k], iE2[X::k,X::s]);
      result[2] += term_E2 + term_E2[X::r,X::s,X::p,X::q];                                         // E2 term added
   }

   return result;
}



template <class T>
std::vector<NiceTensor<T>> cc3_error_vector(
      const std::vector<NiceTensor<T>>& t,
      const T1Integrals& integrals,
      const std::map<std::string, std::vector<T>>& orbital_energies,
      const bool& do_cp,
      const Nb& cp_threshold)
{
   namespace X=contraction_indices;

   NiceTensor<T> energy_differences(new EnergyDifferencesTensor<T>(3,
                                orbital_energies.at("v"),
                                orbital_energies.at("o")));

   const TensorLibrarian<Nb>& l = integrals.L_T1;
   const TensorLibrarian<Nb>& f = integrals.F_T1;
   const TensorLibrarian<Nb>& g = integrals.g_T1;
   
   std::vector<NiceTensor<T>> result(4);

   NiceTensor<Nb> u = (2.0*t[2][X::p,X::q,X::r,X::s] - t[2][X::p,X::s,X::r,X::q]);
   NiceTensor<Nb> v = t[3] - t[3][X::p,X::u,X::r,X::s,X::t,X::q];
   if(do_cp)
   {
      u = u.ToCanonicalTensor(cp_threshold);
      v = v.ToCanonicalTensor(cp_threshold);
   }

   
   /***** Zeroth error vector (not important) *****/
   result[0] = (new Scalar<Nb>(0.0)); // ccsd_error_vector[0]
   
   /***** Singles error vector *****/
   result[1] = (NiceTensor<Nb>(contract(u[X::c,X::k,X::d,X::q], g["vvov"][X::p,X::d,X::k,X::c])
                   -contract(u[X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c])
                   +contract(u[X::p,X::q,X::c,X::k], f["ov"][X::k,X::c]))).ToSimpleTensor()
                   +f["vo"];

   // Add T3 term to result[1]
   result[1] += (NiceTensor<Nb>(contract(v[X::p,X::q,X::b,X::j,X::c,X::k], l["ovov"][X::j,X::b,X::k,X::c]))).ToSimpleTensor();
   

   /***** Doubles error vector *****/
   result[2] = g["vovo"] + contract(t[2][X::c,X::q,X::d,X::s], g["vvvv"][X::p,X::c,X::r,X::d]);          // A term
   if(do_cp) {result[2] = result[2].ToCanonicalTensor(cp_threshold);}

   {
      NiceTensor<Nb> iB = g["oooo"] + contract(t[2][X::c,X::q,X::d,X::s], g["ovov"][X::p,X::c,X::r,X::d]);
      if(do_cp) {iB = iB.ToCanonicalTensor(cp_threshold);}
      result[2] += contract(t[2][X::p,X::k,X::r,X::l], iB[X::k,X::q,X::l,X::s]);                   // B term added
   }

   {
      NiceTensor<Nb> iC1 = g["oovv"] - 0.5*contract(t[2][X::r,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::s]);
      if(do_cp) {iC1 = iC1.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> iC2 = contract(t[2][X::r,X::k,X::c,X::s], iC1[X::k,X::q,X::p,X::c]);
      result[2] += -0.5*iC2 - iC2[X::p,X::s,X::r,X::q] - 0.5*iC2[X::r,X::s,X::p,X::q] - iC2[X::r,X::q,X::p,X::s];  // C term added
   }

   {
      NiceTensor<Nb> iD = l["voov"] + 0.5*contract(u[X::p,X::q,X::d,X::l], l["ovov"][X::l,X::d,X::r,X::s]);
      if(do_cp) {iD = iD.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> term_D = 0.5*contract(u[X::r,X::s,X::c,X::k], iD[X::p,X::q,X::k,X::c]);
      result[2] += term_D + term_D[X::r,X::s,X::p,X::q];                                           // D term added
   }

   {
      const NiceTensor<Nb> iE1 = (f["vv"] - contract(u[X::p,X::k,X::d,X::l], g["ovov"][X::l,X::d,X::k,X::q])).ToSimpleTensor();
      const NiceTensor<Nb> term_E1 = contract(t[2][X::p,X::q,X::c,X::s], iE1[X::r,X::c]);
      result[2] += term_E1 + term_E1[X::r,X::s,X::p,X::q];                                         // E1 term added
   }

   {
      const NiceTensor<Nb> iE2 = (f["oo"] + contract(u[X::c,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::c])).ToSimpleTensor();
      const NiceTensor<Nb> term_E2 = -1.0*contract(t[2][X::p,X::q,X::r,X::k], iE2[X::k,X::s]);
      result[2] += term_E2 + term_E2[X::r,X::s,X::p,X::q];                                         // E2 term added
   }

   // Add T3 terms to result[2]
   {
      NiceTensor<Nb> w = 2.0*t[3] - t[3][X::p,X::u,X::r,X::s,X::t,X::q] - t[3][X::p,X::q,X::r,X::u,X::t,X::s];
      if(do_cp) {w = w.ToCanonicalTensor(cp_threshold);}

      NiceTensor<Nb> iF = contract(v[X::p,X::q,X::r,X::s,X::c,X::k], f["ov"][X::k,X::c]) 
                                   + contract(w[X::r,X::s,X::c,X::q,X::d,X::k], g["vvov"][X::p,X::c,X::k,X::d]) 
                                   - contract(w[X::r,X::s,X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c]);
      if(do_cp) {iF = iF.ToCanonicalTensor(cp_threshold);}

      result[2] += iF + iF[X::r,X::s,X::p,X::q];
   }

   /***** Triples error vector *****/
   result[3] = t[3]*energy_differences;
   
   {
      NiceTensor<Nb> iG = contract(t[2][X::p,X::q,X::d,X::s], g["vovv"][X::t,X::u,X::r,X::d]) 
                              - contract(t[2][X::p,X::q,X::r,X::l], g["vooo"][X::t,X::u,X::l,X::s]);
      if(do_cp) {iG = iG.ToCanonicalTensor(cp_threshold);}

      result[3] += iG + iG[X::p,X::q,X::t,X::u,X::r,X::s] + iG[X::r,X::s,X::p,X::q,X::t,X::u] 
                      + iG[X::t,X::u,X::p,X::q,X::r,X::s] + iG[X::r,X::s,X::t,X::u,X::p,X::q] + iG[X::t,X::u,X::r,X::s,X::p,X::q];
   } 

   return result;
}




template <class T>
std::vector<NiceTensor<T>> ccsdt_error_vector(
      const std::vector<NiceTensor<T>>& t,
      const T1Integrals& integrals,
      const std::map<std::string, std::vector<T>>& orbital_energies,
      const bool& do_cp,
      const Nb& cp_threshold)
{
   namespace X=contraction_indices;

   NiceTensor<T> energy_differences(new EnergyDifferencesTensor<T>(3,
                                orbital_energies.at("v"),
                                orbital_energies.at("o")));

   const TensorLibrarian<Nb>& l = integrals.L_T1;
   const TensorLibrarian<Nb>& f = integrals.F_T1;
   const TensorLibrarian<Nb>& g = integrals.g_T1;
   
   std::vector<NiceTensor<T>> result(4);

   NiceTensor<Nb> u = (2.0*t[2][X::p,X::q,X::r,X::s] - t[2][X::p,X::s,X::r,X::q]);
   NiceTensor<Nb> v = t[3] - t[3][X::p,X::u,X::r,X::s,X::t,X::q];
   if(do_cp)
   {
      u = u.ToCanonicalTensor(cp_threshold);
      v = v.ToCanonicalTensor(cp_threshold);
   }

   
   /***** Zeroth error vector (not important) *****/
   result[0] = (new Scalar<Nb>(0.0)); // ccsd_error_vector[0]
   
   /***** Singles error vector *****/
   result[1] = (NiceTensor<Nb>(contract(u[X::c,X::k,X::d,X::q], g["vvov"][X::p,X::d,X::k,X::c])
                   -contract(u[X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c])
                   +contract(u[X::p,X::q,X::c,X::k], f["ov"][X::k,X::c]))).ToSimpleTensor()
                   +f["vo"];

   // Add T3 term to result[1]
   result[1] += NiceTensor<Nb>(contract(v[X::p,X::q,X::b,X::j,X::c,X::k], l["ovov"][X::j,X::b,X::k,X::c])).ToSimpleTensor();
   

   /***** Doubles error vector *****/
   result[2] = g["vovo"] + contract(t[2][X::c,X::q,X::d,X::s], g["vvvv"][X::p,X::c,X::r,X::d]);          // A term
   if(do_cp) {result[2] = result[2].ToCanonicalTensor(cp_threshold);}

   {
      NiceTensor<Nb> iB = g["oooo"] + contract(t[2][X::c,X::q,X::d,X::s], g["ovov"][X::p,X::c,X::r,X::d]);
      if(do_cp) {iB = iB.ToCanonicalTensor(cp_threshold);}
      result[2] += contract(t[2][X::p,X::k,X::r,X::l], iB[X::k,X::q,X::l,X::s]);                   // B term added
   }

   {
      NiceTensor<Nb> iC1 = g["oovv"] - 0.5*contract(t[2][X::r,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::s]);
      if(do_cp) {iC1 = iC1.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> iC2 = contract(t[2][X::r,X::k,X::c,X::s], iC1[X::k,X::q,X::p,X::c]);
      result[2] += -0.5*iC2 - iC2[X::p,X::s,X::r,X::q] - 0.5*iC2[X::r,X::s,X::p,X::q] - iC2[X::r,X::q,X::p,X::s];  // C term added
   }

   {
      NiceTensor<Nb> iD = l["voov"] + 0.5*contract(u[X::p,X::q,X::d,X::l], l["ovov"][X::l,X::d,X::r,X::s]);
      if(do_cp) {iD = iD.ToCanonicalTensor(cp_threshold);}
      const NiceTensor<Nb> term_D = 0.5*contract(u[X::r,X::s,X::c,X::k], iD[X::p,X::q,X::k,X::c]);
      result[2] += term_D + term_D[X::r,X::s,X::p,X::q];                                           // D term added
   }

   {
      const NiceTensor<Nb> iE1 = (f["vv"] - contract(u[X::p,X::k,X::d,X::l], g["ovov"][X::l,X::d,X::k,X::q])).ToSimpleTensor();
      const NiceTensor<Nb> term_E1 = contract(t[2][X::p,X::q,X::c,X::s], iE1[X::r,X::c]);
      result[2] += term_E1 + term_E1[X::r,X::s,X::p,X::q];                                         // E1 term added
   }

   {
      const NiceTensor<Nb> iE2 = (f["oo"] + contract(u[X::c,X::l,X::d,X::q], g["ovov"][X::p,X::d,X::l,X::c])).ToSimpleTensor();
      const NiceTensor<Nb> term_E2 = -1.0*contract(t[2][X::p,X::q,X::r,X::k], iE2[X::k,X::s]);
      result[2] += term_E2 + term_E2[X::r,X::s,X::p,X::q];                                         // E2 term added
   }

   // Add T3 terms to result[2]
   {
      NiceTensor<Nb> w = 2.0*t[3] - t[3][X::p,X::u,X::r,X::s,X::t,X::q] - t[3][X::p,X::q,X::r,X::u,X::t,X::s];
      if(do_cp) {w = w.ToCanonicalTensor(cp_threshold);}

      NiceTensor<Nb> iF = contract(v[X::p,X::q,X::r,X::s,X::c,X::k], f["ov"][X::k,X::c]) 
                                   + contract(w[X::r,X::s,X::c,X::q,X::d,X::k], g["vvov"][X::p,X::c,X::k,X::d]) 
                                   - contract(w[X::r,X::s,X::p,X::k,X::c,X::l], g["ooov"][X::k,X::q,X::l,X::c]);
      if(do_cp) {iF = iF.ToCanonicalTensor(cp_threshold);}

      result[2] += iF + iF[X::r,X::s,X::p,X::q];
   }

   /***** Triples error vector *****/
   // add <\mu_3|[\tilde{H},T_2]|HF>
   {
      NiceTensor<Nb> iG = contract(t[2][X::p,X::q,X::d,X::s], g["vovv"][X::t,X::u,X::r,X::d]) 
                              - contract(t[2][X::p,X::q,X::r,X::l], g["vooo"][X::t,X::u,X::l,X::s]);
      if(do_cp) {iG = iG.ToCanonicalTensor(cp_threshold);}

      result[3] += iG + iG[X::p,X::q,X::t,X::u,X::r,X::s] + iG[X::r,X::s,X::p,X::q,X::t,X::u] 
                      + iG[X::t,X::u,X::p,X::q,X::r,X::s] + iG[X::r,X::s,X::t,X::u,X::p,X::q] + iG[X::t,X::u,X::r,X::s,X::p,X::q];
   } 

   // add ½<\mu_3|[[\tilde{H},T_2],T_2]|HF>
   {
      
   }

   // add <\mu_3|[\tilde{H},T_3]|HF>
   {
      
   }

   // add <\mu_3|[[\tilde{H},T_3],T_2]|HF>
   {
      
   }

   return result;
}

#endif /* ERROR_VECTORS_H_INCLUDED */
