/**
 ************************************************************************
 * 
 * @file                diis.h
 *
 * Created:             26.01.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * Short Description:   DIIS update for data structured as std::vector<NiceTensor>
 * 
 * Last modified:       ........
 * 
 * Detailed Description: ...
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */
#ifndef TENSORNLSOLVER_H_INCLUDED
#define TENSORNLSOLVER_H_INCLUDED

#include<vector>
#include<list>

#include "tensor/NiceTensor.h"

//! In-place DIIS update.
template <class T>
void diis_update(std::list<std::vector<NiceTensor<T>>>& target,
                 std::list<std::vector<NiceTensor<T>>>& error_vectors,
                 int max_size);

// DIIS update, no truncation.
template <class T>
void diis_update(std::list<std::vector<NiceTensor<T>>>& target,
                 std::list<std::vector<NiceTensor<T>>>& error_vectors);

#include"diis_Impl.h"
#endif // TENSORNLSOLVER_H_INCLUDED
