/**
************************************************************************
* 
* @file                ReferenceState.cc
*
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Orbital Info class 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/EcorCalcDef.h"
#include "ecor/ReferenceState.h"
#include "ecor/ReadLSDaltonIntegrals.h"
//#inbclude "ecor/EcorTransformer.h"
//#include "tensor/TensorEigSolver.h"
//#include "tensor/TensorMacroSolver.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Timer.h"
#include "util/read_write_binary.h"
//#include "util/LinkFileIo_Impl.h"
#include "tensor/NiceTensor.h"
#include "tensor/ContractionIndices.h"
#include "tensor/EnergyDenominatorTensor.h"
#include"tensor/EnergyDifferencesTensor.h"

using std::ostream;
using std::string;
using std::vector;
namespace X=contraction_indices;

/**
* Constructor from input
* */
ReferenceState::ReferenceState(
      const bool NotInit,
      const In aNBas, const In aNOrb,
      const In aNOcc, const In aNVir,
      const In aNFrozenOcc, const In aNFrozenVir,
      const string orbitals_filename,
      const string energies_filename,
      const string overlap_integrals_filename,
      const string oneelectron_integrals_filename,
      const string twoelectron_integrals_filename)
  :mNBas(aNBas)
  ,mNOrb(aNOrb)
  ,mNOcc(aNOcc)
  ,mNVir(aNVir)
  ,mNFrozenOcc(aNFrozenOcc)
  ,mNFrozenVir(aNFrozenVir)
{

   if (NotInit) 
   {
      Mout << " No input from dalton. Maybe something is wrong " << endl;
   }
   else
   {
      Mout << " Read in Orbital Info from files " << endl; 

      OutputReferenceState(); 

      // Read in orbital energies 
      /******************* Orbitals from Dalton *******************************/ 
      {
         ifstream energy_file(energies_filename, ios::in | ios::binary); 
         energy_file.exceptions( std::ios::badbit | std::ios::failbit );
         Nb tmp_energy_occ      [mNOcc      ];
         Nb tmp_energy_vir      [mNVir      ];
         Nb tmp_energy_frozenocc[mNFrozenOcc];
         Nb tmp_energy_frozenvir[mNFrozenVir];
         // WARNING!!! THE ORDERING MIGHT BE WRONG HERE!!!!!!!!!!!!!!!!!!!!
         // FROZEN OCCUPIED AND VIRTUAL NEED TESTING
         energy_file.read(reinterpret_cast<char*>(tmp_energy_frozenocc), mNFrozenOcc*sizeof(Nb) );
         energy_file.read(reinterpret_cast<char*>(tmp_energy_occ),       mNOcc      *sizeof(Nb) );
         energy_file.read(reinterpret_cast<char*>(tmp_energy_vir),       mNVir      *sizeof(Nb) );
         energy_file.read(reinterpret_cast<char*>(tmp_energy_frozenvir), mNFrozenVir*sizeof(Nb) );
         energy_file.close();

         mEnergies["o"].assign(tmp_energy_occ,       tmp_energy_occ      +mNOcc);
         mEnergies["v"].assign(tmp_energy_vir,       tmp_energy_vir      +mNVir);
         mEnergies["fo"].assign(tmp_energy_frozenocc, tmp_energy_frozenocc+mNFrozenOcc);
         mEnergies["fv"].assign(tmp_energy_frozenvir, tmp_energy_frozenvir+mNFrozenVir);
      }

      
      // Read in orbitals
      /******************* Orbitals from Dalton *******************************/ 
      ifstream cmo_file(orbitals_filename, ios::binary); 
      cmo_file.exceptions( std::ios::badbit | std::ios::failbit );

      Nb* tmp_cmo_occ      (new Nb[mNBas*mNOcc      ]);
      cmo_file.read(reinterpret_cast<char*>(tmp_cmo_occ),       mNBas*mNOcc      *sizeof(Nb) );
      mCmo["o"]=NiceTensor<Nb>( (new SimpleTensor<Nb>(
               std::vector<unsigned int>{static_cast<unsigned int>(mNOcc), static_cast<unsigned int>(mNBas)}, tmp_cmo_occ)))[X::q,X::p];

      Nb* tmp_cmo_vir      (new Nb[mNBas*mNVir      ]);
      cmo_file.read(reinterpret_cast<char*>(tmp_cmo_vir),       mNBas*mNVir      *sizeof(Nb) );
      mCmo["v"]=NiceTensor<Nb>( (new SimpleTensor<Nb>(
               std::vector<unsigned int>{static_cast<unsigned int>(mNVir), static_cast<unsigned int>(mNBas)}, tmp_cmo_vir)))[X::q,X::p];

      if(mNFrozenOcc>0)
      {
         Nb* tmp_cmo_frozenocc(new Nb[mNBas*mNFrozenOcc]);
         cmo_file.read(reinterpret_cast<char*>(tmp_cmo_frozenocc), mNBas*mNFrozenOcc*sizeof(Nb) );
         mCmo["fo"]=NiceTensor<Nb>( (new SimpleTensor<Nb>(
               std::vector<unsigned int>{static_cast<unsigned int>(mNFrozenOcc), static_cast<unsigned int>(mNBas)}, tmp_cmo_frozenocc)))[X::q,X::p];
      }

      if(mNFrozenVir>0)
      {
         Nb* tmp_cmo_frozenvir(new Nb[mNBas*mNFrozenVir]);
         cmo_file.read(reinterpret_cast<char*>(tmp_cmo_frozenvir), mNBas*mNFrozenVir*sizeof(Nb) );
         mCmo["fv"]=NiceTensor<Nb>( (new SimpleTensor<Nb>(
                  std::vector<unsigned int>{static_cast<unsigned int>(mNFrozenVir), static_cast<unsigned int>(mNBas)}, tmp_cmo_frozenvir)))[X::q,X::p];
      }
      cmo_file.close();

      // Read in integrals
      {
         /*************** ONE-ELECTRON INTEGRALS ******************/
         mOverlapIntegrals    =ReadLSDaltonOneElectronIntegrals(overlap_integrals_filename,     mNBas);
         mOneElectronIntegrals=ReadLSDaltonOneElectronIntegrals(oneelectron_integrals_filename, mNBas);

         /*************** TWO-ELECTRON INTEGRALS ******************/
         // The type of two-electron integrals is given by file name
         // Either just a block of integrals
         if(twoelectron_integrals_filename=="LSDALTONERI3DIMBLOCKS.data")
         {
            mTwoElectronIntegrals=ReadLSDaltonTwoElectronIntegrals(twoelectron_integrals_filename, mNBas);
         }
         // Or tensor hypercontraction two-electron integrals
         else if(twoelectron_integrals_filename=="THC.restart")
         {
            mTwoElectronIntegrals=ReadLSDaltonTHCIntegrals(twoelectron_integrals_filename, mNBas);
         }
         else
         {
            throw std::runtime_error("Invalid file name for two electron integrals: <"+twoelectron_integrals_filename+">");
         }
      }

      // Make sure orbitals and integrals are sane
      AssertCorrectOrbitals();
   }
}


// /**
// * Setup orbital info from reading in from file. 
// * */
void ReferenceState::OutputReferenceState() const 
{
   Mout<<" Orbital Info " << endl; 
   char c='|';

   Out72Char(Mout,'+','=','+');
   TwoArgOut(Mout," Nr of orbitals of correlated calculation ",  58,mNOrb,10,c);
   TwoArgOut(Mout," Nr of basis functions ",                     58,mNBas,10,c); 
   TwoArgOut(Mout," Nr of occupied orbitals ",                   58,mNOcc,10,c); 
   TwoArgOut(Mout," Nr of virtual  orbitals ",                   58,mNVir,10,c); 
   TwoArgOut(Mout," Nr of frozen occupied orbitals",             58,mNFrozenOcc,10,c); 
   TwoArgOut(Mout," Nr of frozen virtual  orbitals",             58,mNFrozenVir,10,c); 
   TwoArgOut(Mout," Nr of occupied orbitals (incl. frozen orbs)",58,NOccTot(),10,c); 
   TwoArgOut(Mout," Nr of virtual  orbitals (incl. frozen orbs)",58,NVirTot(),10,c); 
   TwoArgOut(Mout," Nr of orbitals in total (inc. frozen orbs)", 58,NOrbTot(),10,c); 
   Out72Char(Mout,'+','=','+');
}
 
NiceTensor<Nb> ReferenceState::GetCmoFull()
{
   SimpleTensor<Nb>* result=new SimpleTensor<Nb>(std::vector<unsigned int>{static_cast<unsigned int>(mNOrb), static_cast<unsigned int>(mNOrb)});

   Nb* ptr_out=result->GetData();

   if(mNFrozenOcc>0)
   {
      NiceTensor<Nb> mCmo_fc_reordered = mCmo["fc"][X::q,X::p];
      Nb* ptr_in=dynamic_cast<SimpleTensor<Nb>*>(mCmo_fc_reordered.GetTensor())->GetData();
      for(size_t i=0;i<mNFrozenOcc*mNBas;++i)
      {
         *( ptr_out++ ) = * ( ptr_in++ );
      }
   }
   {
      NiceTensor<Nb> mCmo_o_reordered = mCmo["o"][X::q,X::p];
      Nb* ptr_in=dynamic_cast<SimpleTensor<Nb>*>(mCmo_o_reordered.GetTensor())->GetData();
      for(size_t i=0;i<mNOcc*mNBas;++i)
      {
         *( ptr_out++ ) = * ( ptr_in++ );
      }
   }
   {
      NiceTensor<Nb> mCmo_v_reordered = mCmo["v"][X::q,X::p];
      Nb* ptr_in=dynamic_cast<SimpleTensor<Nb>*>(mCmo_v_reordered.GetTensor())->GetData();
      for(size_t i=0;i<mNVir*mNBas;++i)
      {
         *( ptr_out++ ) = * ( ptr_in++ );
      }
   }
   if(mNFrozenVir>0)
   {
      NiceTensor<Nb> mCmo_fv_reordered = mCmo["fv"][X::q,X::p];
      Nb* ptr_in=dynamic_cast<SimpleTensor<Nb>*>(mCmo_fv_reordered.GetTensor())->GetData();
      for(size_t i=0;i<mNFrozenVir*mNBas;++i)
      {
         *( ptr_out++ ) = * ( ptr_in++ );
      }
   }
   return NiceTensor<Nb>(result)[X::q,X::p];
}

/**
* Various checks of orbitals and integrals 
* */
void ReferenceState::AssertCorrectOrbitals()
{
#define DIFF_NORM_THRESHOLD 1.e-10

      Mout << " Ecor::CalcRefEnergy: Normalization check " << endl;
//   if (gIoLevel > 6)
//   {
//      Mout << "-----------------------------------------------------------------------------------" << endl;   
//      Mout << "Cmo Occ as NiceTensor \n" << mCmo["o"] << endl;
//      Mout << "Cmo Vir as NiceTensor \n" << mCmo["v"] << endl;
//      if(mNFrozenOcc==0){Mout << "No frozen occ" << endl;}
//         else{Mout << "Cmo Frozen occ as NiceTensor \n" << mCmo["fc"] << endl;}
//      if(mNFrozenVir==0){Mout << "No frozen vir" << endl;}
//         else{Mout << "Cmo Frozen vir as NiceTensor \n" << mCmo["fv"] << endl;}
//      Mout << "-----------------------------------------------------------------------------------" << endl;
//   }

   // Check normalization by checking if c^T S c - 1 is equal to 0
   Nb diff_norm;
   {
      NiceTensor<Nb> c=GetCmoFull();
      NiceTensor<Nb> i=NiceTensor<Nb>(new SimpleTensor<Nb>(identity_tensor<Nb>(NOrb())));
      //  c S c^T - I
      NiceTensor<Nb> error = contract(contract(c[X::k,X::p], mOverlapIntegrals[X::k,X::l]), c[X::l,X::q]) 
                           - i;

      diff_norm = error.Norm();
//      if (gIoLevel > 6)
//      {
//         Mout << "c^T S c - 1 \n" << error << endl;
//      }

   }

   if(diff_norm < DIFF_NORM_THRESHOLD)
   {
 //     if (gIoLevel > 6)
      {
         Mout << "The norm of c^T S c - 1 is " << diff_norm << " which is smaller than the threshold of " << DIFF_NORM_THRESHOLD << endl;
         Mout << "The MO's are orthonormal" << endl;
      }
   }
   else
   {
      Mout << "The norm of c^T S c - 1 is " << diff_norm << " which is larger than the threshold of " << DIFF_NORM_THRESHOLD << endl;
      Mout << "ERROR! The MO's are NOT orthonormal with respect to the given threshold. Check the numbers!" << endl;
      throw std::runtime_error("Input MOs were not orthonormal");
   }
   Mout << "-----------------------------------------------------------------------------------" << endl;


   //Check if the Fock matrix is diagonal with the orbital energies as elements.
#define FOCK_THRESHOLD 1.e-10
   NiceTensor<Nb> d_o = contract(mCmo.at("o")[X::p,X::k],mCmo.at("o")[X::q,X::k]);
   const NiceTensor<Nb>& h = mOneElectronIntegrals;
   const NiceTensor<Nb>& g = mTwoElectronIntegrals;
      //Declare diagonal Fock matrix from orbital energies
   NiceTensor<Nb> f_canonical_o(new SimpleTensor<Nb>(diagonal_tensor(mEnergies.at("o"))));
   NiceTensor<Nb> f_canonical_v(new SimpleTensor<Nb>(diagonal_tensor(mEnergies.at("v"))));
      //Calculate Fock matrix from integrals and MO coefficients
   NiceTensor<Nb> fock_matrix_ao = h + 2.0*contract(g[X::p,X::q,X::r,X::s], d_o[X::r,X::s]) - contract(g[X::p,X::q,X::r,X::s], d_o[X::q,X::r]);
   NiceTensor<Nb> fock_matrix_ov = contract(contract(fock_matrix_ao[X::p,X::q], mCmo.at("o")[X::p,X::m]),mCmo.at("v")[X::q,X::n]);
   NiceTensor<Nb> fock_matrix_oo = contract(contract(fock_matrix_ao[X::p,X::q], mCmo.at("o")[X::p,X::m]),mCmo.at("o")[X::q,X::n]);
   NiceTensor<Nb> fock_matrix_vv = contract(contract(fock_matrix_ao[X::p,X::q], mCmo.at("v")[X::p,X::m]),mCmo.at("v")[X::q,X::n]);
      //Calculate errors
   Nb error_ov = fock_matrix_ov.Norm();
   Nb error_oo = (fock_matrix_oo-f_canonical_o).Norm();
   Nb error_vv = (fock_matrix_vv-f_canonical_v).Norm();

   if(error_ov < FOCK_THRESHOLD && error_oo < FOCK_THRESHOLD && error_vv < FOCK_THRESHOLD)
   {
      Mout << "The error norm of the occ-vir block of the Fock Matrix is " << error_ov << endl;
      Mout << "The error norm of the occ-occ block is " << error_oo << endl;
      Mout << "The error norm of the vir-vir block is " << error_vv << endl;
      Mout << "The Threshold is " << FOCK_THRESHOLD << ". \nThe Fock Matrix is canonical!"  << endl;
   }
   else
   {
      Mout << "WARNING: The norm of the occ-vir block of the Fock Matrix is " << error_ov << endl;
      Mout << "The error norm of the occ-occ block is " << error_oo << endl;
      Mout << "The error norm of the vir-vir block is " << error_vv << endl;
      Mout << "The threshold is " << FOCK_THRESHOLD << ".\nCheck the numbers!" << endl;
      Mout << "F_oo from integrals\n" << fock_matrix_oo << endl << endl;
      Mout << "The occupied orbital energies = " << mEnergies.at("o") << endl << endl;
      Mout << "F_vv from integrals\n" << fock_matrix_vv << endl << endl;
      Mout << "The virtual orbital energies = " << mEnergies.at("v") << endl << endl;
      Mout << "F_ov from integrals\n" << fock_matrix_ov << endl << endl;
      cout << "WARNING: The norm of the occ-vir block of the Fock Matrix is " << error_ov << endl;
      cout << "The error norm of the occ-occ block is " << error_oo << endl;
      cout << "The error norm of the vir-vir block is " << error_vv << endl;
      cout << "The threshold is " << FOCK_THRESHOLD << ".\nCheck the numbers!" << endl;
      cout << "F_oo from integrals\n" << fock_matrix_oo << endl << endl;
      cout << "The occupied orbital energies = " << mEnergies.at("o") << endl << endl;
      cout << "F_vv from integrals\n" << fock_matrix_vv << endl << endl;
      cout << "The virtual orbital energies = " << mEnergies.at("v") << endl << endl;
      cout << "F_ov from integrals\n" << fock_matrix_ov << endl << endl << endl;

      cout << "Continue program? [y/n]" << endl;
      char yn;
      std::cin >> yn;

      if(yn=='y'){cout << "Continuing..." << endl;}
      if(yn=='n'){exit(1);}
   }
}



Nb ReferenceState::Energy()
{
   NiceTensor<Nb> d_o = contract(mCmo["o"][X::p,X::k], mCmo["o"][X::q,X::k]);
   NiceTensor<Nb>& h = mOneElectronIntegrals;
   NiceTensor<Nb>& g = mTwoElectronIntegrals;

   Nb result;

   // GS: The devision of occupied and frozen occupied makes no sense on the HF level
   //     I think this here is conceptional wrong. The full HF energy has to be added
   //     with an without frozen core approximation

   // The HF energy is divided in two parts: The occupied energy and the frozen occupied energy
   NiceTensor<Nb> e_HF_o = 2.0*contract(h[X::m,X::n], d_o[X::m,X::n])
                           + 2.0*contract(contract(g[X::m,X::n,X::o,X::p], d_o[X::m,X::n]),
                                          d_o[X::o,X::p]) 
                           - contract(contract(g[X::m,X::n,X::o,X::p], 
                                    d_o[X::m,X::p]), d_o[X::n,X::o]);

   // The frozen occupied energy is only calculated and added if NFrozenOcc() != 0
   if(NFrozenOcc() != 0)
   {
      NiceTensor<Nb> d_fc = contract(mCmo["fc"][X::p,X::k], mCmo["fc"][X::q,X::k]);

      NiceTensor<Nb> e_HF_fc = 2.0*contract(h[X::m,X::n], d_fc[X::m,X::n]) 
                                 + 2.0*contract(contract(g[X::m,X::n,X::o,X::p], d_fc[X::m,X::n]), 
                                   d_fc[X::o,X::p])
                                 - contract(contract(g[X::m,X::n,X::o,X::p], 
                                          d_fc[X::m,X::p]), d_fc[X::n,X::o]);
      
      result = (e_HF_o + e_HF_fc).GetScalar();
   }
   else
   {
      result = e_HF_o.GetScalar();
   }
  
   return result;
}

Nb ReferenceState::MP2Energy() const
{
   // MO integrals
   const NiceTensor<Nb>& c_o=mCmo.at("o");
   const NiceTensor<Nb>& c_v=mCmo.at("v");

   NiceTensor<Nb> g_mo=mTwoElectronIntegrals.ContractForward(0, c_v)
                                            .ContractForward(1, c_o)
                                            .ContractForward(2, c_v)
                                            .ContractForward(3, c_o)
                                            .ToSimpleTensor()
                                            ;
   NiceTensor<Nb> denominators(
//      new EnergyDenominatorTensor<Nb>(
      new ExactEnergyDenominatorTensor<Nb>(
         2
        ,mEnergies.at("v")
        ,mEnergies.at("o")
//        ,8
      )
   );

   NiceTensor<Nb> minus_mp2_energy =
      contract( (g_mo*g_mo)[X::a, X::i, X::b, X::j], denominators[X::a, X::i, X::b, X::j] );

   return - ( minus_mp2_energy.GetScalar() );
}



