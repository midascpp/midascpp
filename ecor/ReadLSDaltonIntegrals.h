#include "tensor/NiceTensor.h"
#include "inc_gen/TypeDefs.h"

NiceTensor<Nb> ReadLSDaltonTHCIntegrals(const std::string&, const unsigned int);
NiceTensor<Nb> ReadLSDaltonOneElectronIntegrals(const std::string&, const unsigned int);
NiceTensor<Nb> ReadLSDaltonTwoElectronIntegrals(const std::string&, const unsigned int);
