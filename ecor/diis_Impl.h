#include<stdexcept>

#include"tensor/NiceTensor_vector.h"
#include"lapack_interface/math_wrappers.h"

/*! @param solutions contains n elements, the last one having been obtained as
 *                   `solutions[n-1] = solutions[n-2] + update[n-1]`. `solutions[n-1]`
 *                   is overwritten with the new solution.
 *  @param errors contains n elements. The last element is the error vector associated
 *                with update[n-1] on input. `errors[n-1]`
 *                   is overwritten with the new error vector.
 *  @param max_size Truncation parameter. Maximum number of solutions to be kept.
 */
template <class T>
void diis_update(std::list<std::vector<NiceTensor<T>>>& solutions,
                 std::list<std::vector<NiceTensor<T>>>& errors,
                 int max_size)
{
   int n=errors.size();
   if(solutions.size()!=n)
   {
      throw std::runtime_error("diis_update takes N errors and N solutions");
   }

   /* Compute DIIS Lower Triangular (Fortran Lower triangular!)
    * part of the DIIS matrix         *
    *    / <e1,e1> <e1,e2> <e1,e3> ... -1 \   *
    *    | <e2,e1> <e2,e2> <e2,e3> ... -1 |   *
    *    | <e3,e1> <e3,e2> <e3,e3> ... -1 |   *
    *        ...     ...     ...   ... ...    *
    *    \    -1      -1      -1   ...  0 /   */

   T* mat=new T[(n+1)*(n+2)/2];
   T* mat_ptr=mat;
   T* coeffs=new T[n+1];

   int i=0;
   for(auto error_left=errors.begin(); error_left!=errors.end(); ++error_left)
   {
      for(auto error_right=error_left; error_right!=errors.end(); ++error_right)
      {
         *(mat_ptr++)=dot_product(*error_left, *error_right);
      };
      // Last element in column is -1
      *(mat_ptr++)=static_cast<T>(-1);
      // RHS is 0
      coeffs[i++]=static_cast<T>(0);
   }
   // Last element is 0
   *(mat_ptr++)=static_cast<T>(0);

   // The RHS for the last row is -1.
   coeffs[n]=static_cast<T>(-1);

   char l='L';
   int nn=n+1;
   int nrhs=1;
   int info;
   midas::lapack_interface::sptrfs(&l, &nn, &nrhs, mat, coeffs, &nn, &info);

   // Repack coefficients
   std::vector<T> coeffs_v;
   for(int i=0;i<n;++i)
   {
      coeffs_v.push_back(coeffs[i]);
   }
   delete[] coeffs;
   delete[] mat;

   // Overwrite last solution and last error
   solutions.back()=linear_combination(coeffs_v, solutions);
   errors   .back()=linear_combination(coeffs_v, errors);

   // Pop first solution and error if we are to truncate
   if(n>max_size)
   {
      solutions.pop_front();
      errors   .pop_front();
   }
   return;
}

template <class T>
void diis_update(std::list<std::vector<NiceTensor<T>>>& solutions,
                 std::list<std::vector<NiceTensor<T>>>& errors)
{
   diis_update(solutions, errors, errors.size());
}
