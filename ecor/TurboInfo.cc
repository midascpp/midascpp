
/**
************************************************************************
* 
* @file                
*
* Created:            26-05-2016 
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:  Class for holding information necessary to 
*                     read and interprete Turbomole integral files 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<cstring>
#include<cstdlib>

#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "util/read_write_binary.h"
#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"
#include "ecor/TurboInfo.h"
#include "tensor/ContractionIndices.h"


namespace X=contraction_indices;


void TurboInfo::ReadInfo(const std::string& filename)
{
   // constants
   const bool locdbg = true;
 
   //
   // Read header file with all information how the (Q|mu nu) are stored 
   //
   std::ifstream input(filename, std::ios::in | std::ios::binary );

   //////////////////////////
   // general dimensions:   
   ////////////////////////// 

   // number of auxiliary functions
   read_binary(mNAux, input);

   // number of basis functions
   read_binary(mNBas, input);

   // number of atoms
   read_binary(mNAtoms, input);

   // maximal number of auxiliary functions at a atom
   read_binary(mMXbfat, input);

   // number of entries for the packed AO format 
   read_binary(mNPack, input);

   // Read number of auxiliary shells
   read_binary(mNXshell, input);
 
   // number of occupied orbitals
   read_binary(mNOcc, input);

   // number of virutal orbitals
   read_binary(mNVirt, input);
  
   // number of active occupied orbitals
   read_binary(mNOct, input);

   // number of active virtual orbitals
   read_binary(mNVirct, input);

   // number of orbitals
   read_binary(mNOrb, input);

   // dimension for exmax
   read_binary(mNshsh, input);

   // Read number of basis function shells
   read_binary(mNShell, input);

   if (locdbg == true)
   { 

      Mout << "Info from Turbomole integral file:" << endl;
      char c='|'; 

      Out72Char(Mout,'+','=','+');
      TwoArgOut(Mout, "Number of aux. functions:   ", 58, mNAux,   10,c); 
      TwoArgOut(Mout, "Number of Atomic orbitals:  ", 58, mNBas,   10,c); 
      TwoArgOut(Mout, "Number of packed elements:  ", 58, mNPack,  10,c); 
      TwoArgOut(Mout, "Max. Nr. of Aux. per atom:  ", 58, mMXbfat, 10,c); 
      TwoArgOut(Mout, "Number of aux. bf. shells:  ", 58, mNXshell,10,c); 
      TwoArgOut(Mout, "Number of orbitals:         ", 58, mNOrb,   10,c); 
      TwoArgOut(Mout, "Number of act. occupied:    ", 58, mNOct,   10,c); 
      TwoArgOut(Mout, "Number of act. virtual:     ", 58, mNVirct, 10,c); 
      Out72Char(Mout,'+','=','+');

      Mout << std::endl;
   }

   //////////////////////////
   // index arrys:   
   //////////////////////////
 
   // read in index array for reading packed AO integrals
   mIadr = new In[mNBas*mNBas];
   read_binary_array(mIadr, mNBas*mNBas , input);

   // read in number of shell pairs 
   mNijpair = new In[mNBas];
   read_binary_array(mNijpair, mNBas, input);

   /***********************************************************
    * info for auxiliary basis  
    ***********************************************************/ 
   mXbas.natoms = mNAtoms;
   mXbas.nshells = mNXshell;

   // read in info on aux. shells
   mXbas.Shells = new In[mNXshell+1];
   read_binary_array(mXbas.Shells, mNXshell+1, input);

   // read in number of aux. basis functions per atom
   mXbas.Nbfat = new In[mNAtoms];
   read_binary_array(mXbas.Nbfat, mNAtoms, input);

   // start indices of bf in aux. shells
   mXbas.Ishst = new In[mNAtoms];
   read_binary_array(mXbas.Ishst, mNAtoms, input);

   // end indices of bf in aux. shells
   mXbas.Ishnd = new In[mNAtoms]; 
   read_binary_array(mXbas.Ishnd, mNAtoms, input);

   /***********************************************************
    * info for orbital basis  
    ***********************************************************/ 
   mBas.natoms = mNAtoms;
   mBas.nshells = mNShell;

   // read in info on aux. shells
   mBas.Shells = new In[mNShell+1];
   read_binary_array(mBas.Shells, mNShell+1, input);

   // read in number of aux. basis functions per atom
   mBas.Nbfat = new In[mNAtoms];
   read_binary_array(mBas.Nbfat, mNAtoms, input);

   // start indices of bf in aux. shells
   mBas.Ishst = new In[mNAtoms];
   read_binary_array(mBas.Ishst, mNAtoms, input);

   // end indices of bf in aux. shells
   mBas.Ishnd = new In[mNAtoms]; 
   read_binary_array(mBas.Ishnd, mNAtoms, input);

   input.close(); 

   // set flag that we have read input
   mIsInit = true;

   // Note: All index array are using the Fortran index convention starting from 1
   //       therefore we have to correct for that here

   // start correct for index convention

   /**************************************************
   * For Auxiliary basis 
   ***************************************************/
   for (int kat = 0; kat < mNAtoms; ++kat)
   {
      mXbas.Ishst[kat] = mXbas.Ishst[kat] - 1;
      mXbas.Ishnd[kat] = mXbas.Ishnd[kat] - 1;
   }
   for (int kshl = 0; kshl < mNXshell+1; ++kshl)
   {
      mXbas.Shells[kshl] = mXbas.Shells[kshl] - 1;
   }

   /**************************************************
   * For orbital basis 
   ***************************************************/
   for (int iat = 0; iat < mNAtoms; ++iat)
   {
      mBas.Ishst[iat] = mBas.Ishst[iat] - 1;
      mBas.Ishnd[iat] = mBas.Ishnd[iat] - 1;
   }
   for (int kshl = 0; kshl < mNShell+1; ++kshl)
   {
      mBas.Shells[kshl] = mBas.Shells[kshl] - 1;
   }

   for (int isbf = 0; isbf < mNBas; ++isbf)
   {
      for (int jsbf = 0; jsbf < mNBas; ++jsbf)
      {
         mIadr[mNBas*jsbf + isbf] = mIadr[mNBas*jsbf + isbf] - 1;
      }
   }
}


NiceTensor<Nb>TurboInfo::Read3RI(const std::string& filename, unsigned int iat, unsigned int jat)
{
   // constants
   const bool locdbg = false;
 
   ///////////////////////////////////////////////////////////////////////
   // 
   // Read in 3 index RI-AO integrals (Q|mu nu) for atom iat and jat.
   // -> restricts mu nu to the basis functions at iat and jat
   //
   ///////////////////////////////////////////////////////////////////////
   std::ifstream input(filename, std::ios::in | std::ios::binary );

   int naux = mNAux; // might be changed to a subset in the future
   int nbas1 = mBas.Nbfat[iat]; 
   int nbas2 = mBas.Nbfat[jat];

   Nb* bqmunu(new Nb[naux*nbas1*nbas2]);
   Nb* bqint(new Nb[mNPack]); 

   int iadrnew = 0;
   int ipack = -1;
   
   for (int kat = 0; kat < mNAtoms; ++kat)
   {

      int kkst = mXbas.Ishst[kat];
      int kknd = mXbas.Ishnd[kat];

      for (int kbfx = mXbas.Shells[kkst] + 1; kbfx <= mXbas.Shells[kkst] + mXbas.Nbfat[kat]; ++kbfx)  
      {

         // read (Q,mu nu) at this atom for this Q
         if (locdbg) Mout << "Read (Q,mu nu) for Q: " << kbfx << std::endl;
         read_binary_array(bqint, mNPack, input);
   
         int isbfloc = 0;
         int iist = mBas.Ishst[iat];
         int iind = mBas.Ishnd[iat];

         for (int isbf = mBas.Shells[iist] + 1; isbf <= mBas.Shells[iist] + mBas.Nbfat[iat]; ++isbf)
         {
            int jsbfloc = 0;
            int jjst = mBas.Ishst[jat];
            int jjnd = mBas.Ishnd[jat];

            for (int jsbf = mBas.Shells[jjst] + 1; jsbf <= mBas.Shells[jjst] + mBas.Nbfat[jat]; ++jsbf)
            {
               if (isbf < jsbf) 
               {
                  ipack = mIadr[mNBas*jsbf + isbf];   // index in packed AO block
               }
               else
               {
                  ipack = mIadr[mNBas*isbf + jsbf];   // index in packed AO block
               }

               iadrnew = kbfx*nbas1*nbas2 + isbfloc*nbas2 + jsbfloc ;   // index in new array/matrix row major 

               if (ipack >= 0)
               {
                  // we have a non vanishing basis function/shell pair (mu nu)
                  bqmunu[iadrnew] = bqint[ipack];
               }
               else
               {
                  // we have a screened out basis function/shell pair (mu nu).... add zero
                  bqmunu[iadrnew] = 0.0;
               }
               jsbfloc += 1;
            }
            isbfloc += 1;
         }
      }
   }

   input.close();

   // clean up
   delete[] bqint;

   // Make a Tensor out of the matrix/array
   if (locdbg) Mout << "Make tensor out of array" << std::endl;
   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(naux),
                                      static_cast<unsigned int>(nbas1),
                                      static_cast<unsigned int>(nbas2)}
	  , bqmunu)
   );

   return result;
}

NiceTensor<Nb> TurboInfo::Read3RI(const std::string& filename)
{

   // constants
   const bool locdbg = true;
 
   ///////////////////////////////////////////////////////////////////////
   // 
   // Read in 3 index RI-AO integrals (Q|mu nu) 
   //
   ///////////////////////////////////////////////////////////////////////
   std::ifstream input(filename, std::ios::in | std::ios::binary );

   // 
   // construct full (Q|mu nu)
   //
   Nb* bqmunu(new Nb[mNAux*mNBas*mNBas]);

   int iadrnew = 0;
   int ipack = -1;

   //Nb* bqint(new Nb[mMXbfat*mNPack]); 
   Nb* bqint(new Nb[mNPack]); 

   for (int kat = 0; kat < mNAtoms; ++kat)
   {

      int kkst = mXbas.Ishst[kat];
      int kknd = mXbas.Ishnd[kat];

      for (int kbfx = mXbas.Shells[kkst] + 1; kbfx <= mXbas.Shells[kkst] + mXbas.Nbfat[kat]; ++kbfx)  
      {

         // read (Q,mu nu) at this atom for this Q
         read_binary_array(bqint, mNPack, input);

         for (int isbf = 0; isbf < mNBas; ++isbf)
         {
            for (int jsbf = 0; jsbf < mNBas; ++jsbf)
            {
               if (isbf < jsbf) 
               {
                  ipack = mIadr[mNBas*jsbf + isbf];   // index in packed AO block
               }
               else
               {
                  ipack = mIadr[mNBas*isbf + jsbf];   // index in packed AO block
               }

               if (ipack == -2) 
               {
                  // special index
                  //throw std::runtime_error("unset index for ipack");
               }

               iadrnew = kbfx*mNBas*mNBas + isbf*mNBas + jsbf ;   // index in new array/matrix row major

               if (iadrnew >= mNAux*mNBas*mNBas || iadrnew < 0)
               {
                  throw std::runtime_error("iadrnew out of bounds"); 
               }

               if (ipack >= 0)
               {
                  // we have a non vanishing basis function/shell pair (mu nu)
                  bqmunu[iadrnew] = bqint[ipack];
               }
               else
               {
                  // we have a screened out basis function/shell pair (mu nu).... add zero
                  bqmunu[iadrnew] = 0.0;
               }

            }
         }
      }
   }

   input.close();

   // clean up
   delete[] bqint;
 
   // Make a Tensor out of the matrix/array
   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mNAux),
                                      static_cast<unsigned int>(mNBas),
                                      static_cast<unsigned int>(mNBas)}
	  , bqmunu)
   );

   return result;
}

NiceTensor<Nb> TurboInfo::ReadVPQ(const std::string& filename)
{
   std::ifstream input(filename, std::ios::in | std::ios::binary );

   // read in packed matrix
   int nmatx = mNAux*(mNAux+1)/2;

   Nb* vpqinv_pack(new Nb[nmatx]);

   read_binary_array(vpqinv_pack, nmatx, input);

   input.close();

   // unpack matrix
   Nb* vpqinv(new Nb[mNAux*mNAux]);

   int ipq = 0;
   for (int iaux = 0; iaux<mNAux; ++iaux)
   {
      for (int jaux = 0; jaux<=iaux; ++jaux)
      {
         ipq = (iaux+1)*iaux/2 + jaux;
         vpqinv[jaux*mNAux + iaux] = vpqinv_pack[ipq];
         vpqinv[iaux*mNAux + jaux] = vpqinv_pack[ipq];
      }
   }

   // Make a Tensor out of the matrix/array
   NiceTensor<Nb> matrix_transpose(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mNAux),
                                      static_cast<unsigned int>(mNAux)}
	  , vpqinv)
   );

   delete[] vpqinv_pack;

   NiceTensor<Nb> result = matrix_transpose[X::q,X::p];
 
   return result;
}


NiceTensor<Nb> TurboInfo::ReadCmocc(const std::string& filename, const int iat /* = -1 */ )
{
   ifstream input(filename, ios::in | ios::binary); 

   // locals
   int nbasloc;
   bool only_atom;

   // read in all CMO coefficients
   Nb* cmo(new Nb[mNBas*mNBas]);

   if (iat == -1)
   {
      // we use the full AO basis
      nbasloc = mNBas;
   }
   else
   {
      // we use only the AO function at the atom iat
      if (iat >= 0 && iat < mNAtoms)
      {
         nbasloc = mBas.Nbfat[iat];
      }
      else
      {
         Mout << "index for atom " << iat << " number of atoms " << mNAtoms << std::endl; 
         throw std::runtime_error("Index for atom out of range!");
      }
      only_atom = true;
   }

   read_binary_array(cmo,mNBas*mNBas,input);

   input.close();

   // get occupied subset
   Nb* tmp_cmo_occ (new Nb[mNBas*mNOcc]);
   for (int iocc = 0; iocc < mNOcc; ++iocc) 
   {
      int ibfst = 0;
      int ibfnd = mNBas;

      if (only_atom) 
      {
         int iist = mBas.Ishst[iat];
   
         ibfst = mBas.Shells[iist] + 1;
         ibfnd = ibfst + mBas.Nbfat[iat]; 
      }

      int isbfloc = 0;
      for (int isbf = ibfst; isbf < ibfnd; ++isbf)
      {
         tmp_cmo_occ[isbfloc + nbasloc*iocc] = cmo[isbf + mNBas*iocc];
         isbfloc += 1;
      }
   }

   // clean up
   delete[] cmo;


   NiceTensor<Nb> result;

   result = NiceTensor<Nb>( (new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mNOcc), 
                                      static_cast<unsigned int>(nbasloc)}, tmp_cmo_occ)))[X::q,X::p];
   

   return result;
}

NiceTensor<Nb> TurboInfo::ReadCmoct(const std::string& filename, const int iat /* = -1 */ )
{
   // constants
   const bool locdbg = false;

   ifstream input(filename, ios::in | ios::binary); 

   // locals
   int nbasloc;
   bool only_atom = false;

   if (iat == -1)
   {
      // we use the full AO basis
      nbasloc = mNBas;
   }
   else
   {
      // we use only the AO function at the atom iat
      if (iat >= 0 && iat < mNAtoms)
      {
         nbasloc = mBas.Nbfat[iat];
      }
      else
      {
         Mout << "index for atom " << iat << " number of atoms " << mNAtoms << std::endl; 
         throw std::runtime_error("Index for atom out of range!");
      }
      only_atom = true;
   }

   // read in all CMO coefficients
   Nb* cmo(new Nb[mNBas*mNBas]);

   read_binary_array(cmo,mNBas*mNBas,input);

   input.close();

   // number of frozen orbitals
   In nfroz = mNOcc - mNOct;

   // get active occupied subset
   Nb* tmp_cmo_oct (new Nb[mNBas*mNOct]);
   for (int ioct = 0; ioct < mNOct; ++ioct) 
   {
      int ibfst = 0;
      int ibfnd = mNBas;

      if (only_atom) 
      {
         int iist = mBas.Ishst[iat];
   
         ibfst = mBas.Shells[iist] + 1;
         ibfnd = ibfst + mBas.Nbfat[iat]; 
      }

      int isbfloc = 0;
      for (int isbf = ibfst; isbf < ibfnd; ++isbf)
      {
         tmp_cmo_oct[isbfloc + nbasloc*ioct] =  cmo[isbf + mNBas*(ioct+nfroz)];
         isbfloc += 1;
      }
   }
   NiceTensor<Nb> result;

   result = NiceTensor<Nb>( (new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mNOct), 
                                      static_cast<unsigned int>(nbasloc)}, tmp_cmo_oct)))[X::q,X::p];
   
   if (locdbg) 
   {
      Mout << "coefficients for active occupied orbitals" << std::endl;
      Mout << result << std::endl;
   }

   // clean up
   delete[] cmo;

   return result;

}

NiceTensor<Nb> TurboInfo::ReadCmvir(const std::string& filename, const int iat /* = -1 */ )
{
   // constants
   const bool locdbg = false;

   ifstream input(filename, ios::in | ios::binary); 

   // locals
   int nbasloc;
   bool only_atom = false;

   if (iat == -1)
   {
      // we use the full AO basis
      nbasloc = mNBas;
   }
   else
   {
      // we use only the AO function at the atom iat
      if (iat >= 0 && iat < mNAtoms)
      {
         nbasloc = mBas.Nbfat[iat];
      }
      else
      {
         Mout << "index for atom " << iat << " number of atoms " << mNAtoms << std::endl; 
         throw std::runtime_error("Index for atom out of range!");
      }
      only_atom = true;
   }

   // read in all CMO coefficients
   Nb* cmo(new Nb[mNBas*mNBas]);

   read_binary_array(cmo,mNBas*mNBas,input);

   input.close();

   // get virtual subset
   Nb* tmp_cmo_vir (new Nb[mNBas*mNVirt]);
   for (int ivir = 0; ivir < mNVirt; ++ivir)
   {
      int ibfst = 0;
      int ibfnd = mNBas;

      if (only_atom) 
      {
         int iist = mBas.Ishst[iat];
   
         ibfst = mBas.Shells[iist] + 1;
         ibfnd = ibfst + mBas.Nbfat[iat]; 
      }

      int isbfloc = 0;
      for (int isbf = ibfst; isbf < ibfnd; ++isbf)
      {
         tmp_cmo_vir[isbfloc + nbasloc*ivir] = cmo[isbf + (ivir+mNOcc)*mNBas];
         isbfloc += 1;
      }
   }
   NiceTensor<Nb> result;

   result = NiceTensor<Nb> ( (new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mNVirt), 
                                      static_cast<unsigned int>(nbasloc)}, tmp_cmo_vir)))[X::q,X::p];
   
   if (locdbg) 
   {
      Mout << "coefficients for virtual  orbitals" << std::endl;
      Mout << result << std::endl;
   }

   // clean up
   delete[] cmo;

   return result;

}

