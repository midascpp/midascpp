/**
************************************************************************
* 
* @file                ReferenceState.h 
*
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Store orbital information - and data.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef REFERENCESTATE_H_INCLUDED
#define REFERENCESTATE_H_INCLUDED

#include <iostream> 
#include <string> 
#include <vector> 
#include <map> 
#include <tuple> 

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "input/EcorCalcDef.h"
#include "input/Input.h"
#include "ecor/T1Integrals.h"

// Include tensor header
#include "tensor/NiceTensor.h"
#include "tensor/ContractionIndices.h"

class ReferenceState 
{
   private:
      In mNBas;             ///< Nr of basis functions (size of AO basis). 
      In mNOrb;             ///< Nr of orbitals (not necessarily = n basis) 
      In mNOcc;             ///< Nr of occupied orbitals (not including frozen core)  
      In mNVir;             ///< Nr of virtual orbitals (not including frozen virtuals). 
      In mNFrozenOcc;       ///< Nr of frozen core/frozen occupied  orbitals 
      In mNFrozenVir;       ///< Nr of frozen virtual orbitals 

   //Orbital Energies
      std::map<std::string, std::vector<Nb>> mEnergies;   // from Dalton

   //Cmo's 
      std::map<std::string, NiceTensor<Nb>> mCmo;   // CMO coefficients from dalton

   // AO Integrals
      NiceTensor<double> mOverlapIntegrals;
      NiceTensor<double> mOneElectronIntegrals;
      NiceTensor<double> mTwoElectronIntegrals;

   public:
      ReferenceState(const bool mNotInit,
                     const In aNBas,const In aNOrb,
                     const In aNOcc, const In aNVir,
                     const In aNFrozenOcc, const In aNFrozenVir,
                     const std::string orbitals_file,
                     const std::string energies_file,
                     const std::string overlap_integrals_file,
                     const std::string oneelectron_integrals_file,
                     const std::string twoelectron_integrals_file); ///< Constructor 
     
      void OutputReferenceState() const;       ///< Output orbital info

      /********** Getters **********/
      In NBas()       const {return mNBas;}
      In NOrb()       const {return mNOrb;}
      In NOcc()       const {return mNOcc;}
      In NVir()       const {return mNVir;}
      In NFrozenOcc() const {return mNFrozenOcc;}
      In NFrozenVir() const {return mNFrozenVir;}
      In NOccTot()    const {return mNOcc+mNFrozenOcc;}
      In NVirTot()    const {return mNVir+mNFrozenVir;}
      In NBas2()      const {return mNBas*mNBas;}
      In NOrb2()      const {return mNOrb*mNOrb;}
      In NOrbTot()    const {return NOccTot()+NVirTot();}
      In NOrbTot2()   const {return NOrbTot()*NOrbTot();}
   
      const std::map<std::string, NiceTensor<Nb>>& GetCmo() const{return mCmo;}
      const NiceTensor<Nb>& GetCmoOcc()     const{return mCmo.at("o");}
      const NiceTensor<Nb>& GetCmoVir()     const{return mCmo.at("v");}
      const NiceTensor<Nb>& GetCfrozenOcc() const{return mCmo.at("fc");}
      const NiceTensor<Nb>& GetCfrozenVir() const{return mCmo.at("fv");}

      const NiceTensor<Nb>& GetOverlapIntegrals()     const{return mOverlapIntegrals;}
      const NiceTensor<Nb>& GetOneElectronIntegrals() const{return mOneElectronIntegrals;}
      const NiceTensor<Nb>& GetTwoElectronIntegrals() const{return mTwoElectronIntegrals;}

      const std::map<std::string, std::vector<Nb>>& GetEnergies() const{return mEnergies;};

      NiceTensor<Nb> GetCmoFull();

      /********* Workers ********/
      void AssertCorrectOrbitals();   // Various checks on orbitals and integrals 

      Nb Energy(); 

      Nb MP2Energy() const;

      T1Integrals TransformIntegrals(const NiceTensor<Nb>& t1_amplitudes) const
      {
         return T1Integrals(mOneElectronIntegrals, mTwoElectronIntegrals, mCmo, t1_amplitudes);
      }

      ReferenceState& ToCanonicalReference(const Nb& cp_threshold)
      {
//         mOneElectronIntegrals = mOneElectronIntegrals.ToCanonicalTensor(1.e-14);
//         mTwoElectronIntegrals = mTwoElectronIntegrals.ToCanonicalTensor(cp_threshold);
//         mCmo.at("o") = mCmo.at("o").ToCanonicalTensor(1.e-14);
//         mCmo.at("v") = mCmo.at("v").ToCanonicalTensor(1.e-14);

         // Output CP two-electron integrals
//         std::ofstream output_g("CPTwoElectronIntegrals.dat", std::ios::out | std::ios::binary );
//         mTwoElectronIntegrals.Write(output_g);
//         output_g.close();
//         exit(1);

         // Write two-electron integrals
         std::ofstream output_g("SimpleTwoElectronIntegrals.dat", std::ios::out | std::ios::binary );
         mTwoElectronIntegrals.Write(output_g);
         output_g.close();
         exit(1);

         std::ifstream input("CPTwoElectronIntegrals.dat", std::ios::in | std::ios::binary );
         NiceTensor<Nb> g(input);
         std::cout << "Two-electron integrals diff norm = " << (mTwoElectronIntegrals-g.ToSimpleTensor()).Norm() << std::endl;
         mTwoElectronIntegrals = g;
         input.close();

         return *this;
      }

};

#endif /* REFERENCESTATE_H_INCLUDED */
