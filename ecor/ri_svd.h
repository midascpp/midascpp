#include<cstring>
#include<cstdlib>

#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"

#include "lapack_interface/GESVD.h"
#include "mmv/MidasMatrix.h"

/**
 *  @brief Decomposes RI integrals to CP like format 
 *
 *  Purpose: Decompses the RI integral tensor (Q,mu nu) to a CP like format
 *           using SVDs on slices defined by the auxiliary index. The resulting
 *           rank will be extremly large and has afterwards to be combined
 *           with a rank reduction algorithm
 *
 * @param  iolevel Specifies the print level for this routine 
 * @param  target RI integral tensor (Q,mu nu), which should be decompsed
 * @param  do_screen Flag which enables screening via the norm of subblocks
 * @param  thr Threshold for the SVDs
 * @param  use_aoidx Flag if the slices should be defined by an AO index instead of the auxiliary index
 * @param  naux Number of auxiliary function
 * @param  nsbfi Number of basis functions mu 
 * @param  nsbfj Number of basis functions nu 
 * @return Returns the RI integral tensor in CP format
 * */
template <class T>
NiceTensor<T> ri_svd
   ( In iolevel
   , const NiceTensor<T>& target
   , const bool do_screen
   , const T thr
   , const bool use_aoidx
   , const int naux
   , const int nsbfi
   , const int nsbfj
   )
{
   return ri_svd(iolevel, target, do_screen, thr, use_aoidx, false, 0, naux, nsbfi, nsbfj);
}

/**
 *  Decomposes RI integrals to CP like format 
 *
 *  Purpose: Decompses the RI integral tensor (Q,mu nu) to a CP like format
 *           using SVDs on slices defined by the auxiliary index. The resulting
 *           rank will be extremly large and has afterwards to be combined
 *           with a rank reduction algorithm
 *
 * @param  iolevel Specifies the print level for this routine 
 * @param  target RI integral tensor (Q,mu nu), which should be decompsed
 * @param  do_screen Flag which enables screening via the norm of subblocks
 * @param  thr Threshold for the SVDs
 * @param  use_aoidx Flag if the slices should be defined by an AO index instead of the auxiliary index
 * @return Returns the RI integral tensor in CP format
 * */
template <class T>
NiceTensor<T> ri_svd
   (  In iolevel
   ,  const NiceTensor<T>& target
   ,  const bool do_screen
   ,  const T thr
   ,  const bool use_aoidx
   )
{
   std::vector<unsigned int> dims=target.GetDims();
   const unsigned int naux = dims[0];
   const unsigned int nsbfi = dims[1];
   const unsigned int nsbfj = dims[2];   
 
   return ri_svd(iolevel, target, do_screen, thr, use_aoidx, false, 0, naux, nsbfi, nsbfj);
}

/**
 *  Decomposes RI integrals to CP like format 
 *
 *  Purpose: Decompses the RI integral tensor (Q,mu nu) to a CP like format
 *           using SVDs on slices defined by the auxiliary index. The resulting
 *           rank will be extremly large and has afterwards to be combined
 *           with a rank reduction algorithm
 *
 * @param  iolevel Specifies the print level for this routine 
 * @param  target RI integral tensor (Q,mu nu), which should be decompsed
 * @param  do_screen Flag which enables screening via the norm of subblocks
 * @param  thr Threshold for the SVDs
 * @param  use_aoidx Flag if the slices should be defined by an AO index instead of the auxiliary index
 * @param  doguess Use routine to produce start guess with the rankguess important vectors
 * @param  rankguess Maximal rank for the start guess when doguess is active
 * @param  naux  Number of auxiliary function
 * @param  nsbfi Number of basis functions mu 
 * @param  nsbfj Number of basis functions nu 
 * @return Returns the RI integral tensor in CP format
 * */
template <class T>
NiceTensor<T> ri_svd
   (  In iolevel
   ,  const NiceTensor<T>& target
   ,  const bool do_screen
   ,  const T thr
   ,  const bool use_aoidx
   ,  const bool doguess
   ,  const int rankguess
   ,  const int naux
   ,  const int nsbfi
   ,  const int nsbfj
   )
{

   // constants
   const bool locdbg = false;

   // locals
   std::vector<unsigned int> dims=target.GetDims();
   const unsigned int ndim(target.NDim());

   int nscreen = 0;
   int nranktot = 0;
   Nb trace;
   Nb norm;

   // sanity check
   if(ndim!=3)
   {
      throw std::runtime_error("ri_svd only works for RI type tensors (Q,mu nu)!");
   }

   int nmxdim = naux;      // if at some point we decide to define slices in a different way
   int mxrankperiter;      // maximal ranks which can be added per iteration (only important when doguess)
   NiceTensor<Nb> block;

   if (use_aoidx) 
   {
      nmxdim = nsbfj;
      mxrankperiter = std::min<In>(nsbfi,naux); 
      if (doguess) mxrankperiter = rankguess / nmxdim;
   }
   else
   {
      nmxdim = naux;
      mxrankperiter = std::min<In>(nsbfi,nsbfj);
      if (doguess) mxrankperiter = rankguess / nmxdim;
   }

   // allocate maximal needed memory for mode matrices... (lots of memory)
   unsigned int maxrank = naux*std::min<In>(nsbfi,nsbfj);
   
   T** mode_matrices = new T*[ndim];
   mode_matrices[0] = new T[maxrank*dims[0]];
   mode_matrices[1] = new T[maxrank*dims[1]];
   mode_matrices[2] = new T[maxrank*dims[2]];

   // memory for blocks (mu nu)    
   int ncopy = dims[1]*dims[2];    
   if (use_aoidx) ncopy = dims[0]*dims[1];

   T* tmp(new T[ncopy]);

   T dfac = 0.01;
   T thrsvd = thr;

   if (doguess) 
   {
      // if this routine is only called to genrate a rough estimate
      // overwrite some settings like threshold for screening
      dfac = 1.0;
      thrsvd = 0.01;
   }

   for (int idx = 0; idx<nmxdim; ++idx)
   {

      if (use_aoidx)
      {
         // copy (Q,mu) block

         // Note: In principle it would be the best if the slices are allways defined by the index with the smalles
         //       dimension, but at the moment we only use the last index...
         block = target.Slice(std::vector<std::pair<unsigned int,unsigned int>>{ {0,naux}, {0,nsbfi}, {idx,idx+1} });
      }
      else
      {
         // copy non-vanishing part of (mu nu) to intermediate
         block = target.Slice(std::vector<std::pair<unsigned int,unsigned int>>{ {idx,idx+1}, {0,nsbfi}, {0,nsbfj} });
      }

      if (locdbg)
      {
         Mout << "block" << std::endl;
         Mout << block << std::endl;
      }

      if (do_screen)
      {
         // calculate the trace of (mu nu) block
         SimpleTensor<T>* simplehandle=dynamic_cast<SimpleTensor<T>*>(block.GetTensor());
         T* data = simplehandle->GetData();

         norm = simplehandle->Norm();
      }
      else
      {
         norm = 10; // deactivate screening
      }

      //-----------------------------------------------+
      // do the svd on block (mu nu) or (Q,mu)
      //-----------------------------------------------+

      if (norm > thrsvd * dfac)   
      {
         // do SVD only if the trace is above a certain threshold

         // a) copy integral block

         if (use_aoidx)
         {
            T* scratch(new T[ncopy]);
            memcpy(reinterpret_cast<void*>(scratch),
                   reinterpret_cast<void*>(dynamic_cast<SimpleTensor<T>*>(block.GetTensor())->GetData()),ncopy*sizeof(T));
            // transpose
            for (int iaux = 0; iaux < naux; ++iaux)
            {
               for (int isbfi = 0; isbfi < nsbfi; ++isbfi)
               {
                  tmp[isbfi*naux + iaux] = scratch[iaux*nsbfi + isbfi];
               }
            }
            delete[] scratch;
         }
         else
         {
            memcpy(reinterpret_cast<void*>(tmp),
                   reinterpret_cast<void*>(dynamic_cast<SimpleTensor<T>*>(block.GetTensor())->GetData()),ncopy*sizeof(T));
         }

         // b) do SVD
         int ndim1 = dims[1]; int ndim2 = dims[2];
         if (use_aoidx) 
         {
            ndim1 = dims[0]; ndim2 = dims[1];
         }

         SVD_struct<T> svd = GESVD(tmp, static_cast<int>(ndim1), static_cast<int>(ndim2), 'A');


         // c) get rank of the SVD: Cut if smaller than thr
         unsigned int svd_rank = 0;
         for (unsigned int irank = 0; irank<svd.Min(); ++irank) 
         {
            if (doguess && (svd_rank >= mxrankperiter) ) break;
            if (svd.s[irank] < thrsvd) break;
            svd_rank += 1;
         } 


         if (locdbg) 
         {

            Mout << "S" << std::endl;
            for (int i = 0; i<svd.Min(); ++i) 
            {
               Mout << i << " " << svd.s[i] << std::endl;
            } 

            T* tmp_u(new T[ndim1*ndim1]);
               
            size_t counter = 0;
            for (int i = 0; i<ndim1;++i) 
            {
               for (int j = 0; j<ndim1;++j)
               {
                  tmp_u[j*ndim1 + i] = svd.u[counter++];
               }
            }

            NiceTensor<T> umat( new SimpleTensor<T>(std::vector<unsigned int>{static_cast<unsigned int>(ndim1),
                                                                              static_cast<unsigned int>(ndim1)},tmp_u));

            Mout << "U" << std::endl;
            Mout << umat << std::endl;


            T* tmp_vt(new T[ndim2*ndim2]);
            
            counter = 0;
            for (int i = 0; i<ndim2;++i) 
            {
               for (int j = 0; j<ndim2;++j)
               {
                  tmp_vt[j*ndim2 + i] = svd.vt[counter++];
               }
            }
 
            NiceTensor<T> vtmat( new SimpleTensor<T>(std::vector<unsigned int>{static_cast<unsigned int>(ndim2),
                                                                               static_cast<unsigned int>(ndim2)},tmp_vt));

            Mout << "V^T" << std::endl;
            Mout << vtmat << std::endl;

         }

         int iadr;

         // generate mode matrices for the full tensor
         if (locdbg) Mout << " generate mode matrices. add rank: " << svd_rank <<  std::endl;
         size_t counter_u = 0;
         for (unsigned int irank = 0; irank<svd_rank; ++irank)
         {
            if (use_aoidx)
            {
               // for auxiliary index
               for (int iaux = 0; iaux<naux; ++iaux)
               {
                  iadr = (nranktot + irank)*naux + iaux;
                  mode_matrices[0][iadr] = svd.u[counter_u++];
               }

               // for AO indices
               size_t counter_vt = irank;
               for (int isbf = 0; isbf<nsbfi; ++isbf)
               {
                  iadr = (nranktot + irank)*nsbfi + isbf;
                  mode_matrices[1][iadr] = svd.vt[counter_vt];
                  counter_vt += svd.Min();
               }                                                   
               // absorb singular values in last AO index
               for (int jsbf = 0; jsbf<nsbfj;++jsbf)
               {
                  iadr = (nranktot + irank)*nsbfj + jsbf; 
                  mode_matrices[2][iadr] = 0.0;
               }
               iadr = (nranktot + irank)*nsbfj + idx;
               mode_matrices[2][iadr] = svd.s[irank];
              
            }
            else
            {
               // for auxiliary index (absorb singular values here)
               for (int iaux = 0; iaux<naux;++iaux)
               {
                  iadr = (nranktot + irank)*naux + iaux; 
                  mode_matrices[0][iadr] = 0.0;
               }
               iadr = (nranktot + irank)*naux + idx;
               mode_matrices[0][iadr] = svd.s[irank];

               // for AO indices
               size_t counter_vt = irank;
               for (int isbf = 0; isbf<nsbfi; ++isbf)
               {
                  iadr = (nranktot + irank)*nsbfi + isbf; 
                  mode_matrices[1][iadr] = svd.vt[counter_vt];   // This for me weird assignment of U and V^T is due
                  counter_vt += svd.Min();
                                                                  // to the row vs column major issue when using Lapack routines 
               }
               for (int isbf = 0; isbf<nsbfj; ++isbf)
               {
                  iadr = (nranktot + irank)*nsbfj + isbf;         // This for me weird assignment of U and V^T is due
                  mode_matrices[2][iadr] = svd.u[counter_u++];    // to the row vs column major issue when using Lapack routines
               }
            }
         }

         nranktot += svd_rank;

      }
      else
      {
         nscreen += 1;
      }

   }

   // clean up 
   delete[] tmp;

   if (iolevel > 5)
   {
      Mout << " Number of screened out blocks: " << nscreen << std::endl;
      Mout << " Rank using RI-SVD prodecure:   " << nranktot << std::endl;
   }

   // If any of the SVD's yielded a lower rank than the maximum estimated rank,
   // we trim off the last rows by reallocating
   if (maxrank>nranktot) 
   {
      if (locdbg) Mout << "Trim of unused memory" << std::endl;
      for(unsigned int idim=0; idim < ndim; ++idim)
      {
         mode_matrices[idim] = reinterpret_cast<T*>(realloc(mode_matrices[idim], nranktot*dims[idim]*sizeof(T)));
      }    
   }

   NiceTensor<T> result (new CanonicalTensor<T>(dims, nranktot, mode_matrices));

   if (locdbg) 
   {
      Mout << "result" << std::endl;
      Mout << result << std::endl;
   }   

   // Pack everything into a CanonicalTensor and return
   return result;
}
