/**
************************************************************************
* 
* @file                
*
* Created:            22-06-2016         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for using atom dependend 3 Index RI/DF
*                     integrals
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef DFINTEGRALS_H_INCLUDED
#define DFINTEGRALS_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <string>

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorLibrarian.h"
#include "ecor/TurboInfo.h"
//#include "ecor/ri_svd.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/TensorDecomposer.h"
#include "input/TensorDecompInput.h"
#include "input/MolStruct.h"
#include "util/CallStatisticsHandler.h"
#include "tensor/LaplaceQuadrature.h"


struct xinfo
{

   bool isincore = false;
   bool isondisk = false;

   long iadrfil = -1;
   long iadrmem = -1;

};

class RI3Integral 
{
   private:
      /************************ Private members ************************************************/

      In mNAux;                                 ///< number of auxiliary functions
      In mNAtoms;                               ///< number of atoms at this 3 index integral

      In mNdim;                                 ///< number of dimensions (in principle always 3) 
      std::vector<unsigned int> mDims;          ///< vector containing the lengthes for each dimension

      std::vector<unsigned int> mIatoms;        ///< index of atoms assigned to integral
      std::vector<std::string>  mElems;         ///< element symbol assigned to atoms in iatoms
      std::map<unsigned int, Vector3D> mXyz;    ///< coordinates of the atoms
      std::map<unsigned int, Nb> mCharges;      ///< Charges of the atoms

      std::vector<unsigned int> mRanks;         ///< ranks for each pair 

      std::vector<std::tuple<unsigned int, unsigned int>> mPairList;  ///< List for the atom pairs included

      NiceTensor<Nb> mIqij;                     ///< Tensor for holding (Q,mu nu)
      Nb** mModeMatrices;                       ///< Array holding the mode matrices 
      bool mIsOvlpMetric = true;

      bool mDoCP;                               ///< Flag which tells if we use a CP decomposition (in most cases yes) 
      
      bool mVPQINV;                             ///< Was (Q,mu nu) multiplied with V_{PQ}^{-1}


      TensorDecompInfo::Set   mDecompInfoSet;   ///< Set of TensorDecompInfo to control decompositions
      TensorDecompInfo        mDecompInfo;      ///< Actual DecompInfo 


      /************************ Private methods ************************************************/
      void SaveModeMatrices(const unsigned int, const unsigned int, Nb**, std::vector<unsigned int>);
      void ReadModeMatrices(const unsigned int, const unsigned int, Nb**, std::vector<unsigned int>);
      void DeleteModeMatrices();

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      RI3Integral(const In, const In, const bool, const bool, const Nb&, TurboInfo&, TensorDecompInfo::Set);
      RI3Integral(const bool, const bool, const Nb&, const Nb&, const Nb&, TurboInfo&, TensorDecompInfo::Set, MolStruct*);

      ///> Delete copy- and move constructor
      RI3Integral(const RI3Integral&)  = delete;
      RI3Integral(RI3Integral&&)       = delete;

      ///> Destructor
      ~RI3Integral() = default;

      /******** Operations ********/
      ///> Delete copy- and move assignment operators
      RI3Integral& operator=(const RI3Integral&) = delete;
      RI3Integral& operator=(RI3Integral&&)      = delete;
 
      /******** Getters ********/
      In NAux()        const {return mNAux;} 
      In NAtoms()      const {return mNAtoms;} 

      bool HasVPQInv() const {return mVPQINV;}

      bool IsOvlpMetric()  const {return mIsOvlpMetric;}

      NiceTensor<Nb> Get3Idx() {return mIqij;}

      const std::vector<unsigned int>& GetDims() const;
      Nb GetDistance(unsigned int, unsigned int) const;

      const std::vector<unsigned int>& GetRanks() const;

      const std::vector<std::tuple<unsigned int,unsigned int>>& GetPairList()  const;

      /******** Methods ********/
      void ContractWithVPQInv(NiceTensor<Nb>&);
      void Transform( const unsigned int idx, NiceTensor<Nb>);
      void ReduceRank();
      void ReduceRank(const Nb, const unsigned int);
      void Append(RI3Integral&);

      NiceTensor<Nb> ContractWith3idx(RI3Integral&);
 
};

class RI4Integral
{
   private:
      /************************ Private members ************************************************/

      TensorDecompInfo::Set   mDecompInfoSet;        ///< Set of TensorDecompInfo to control decompositions
      TensorDecompInfo        mDecompInfo;           ///< Actual DecompInfo 


      std::vector<unsigned int> mPairRankOffset;     ///< vector containing the offsets for the ranks for each atom pair

      In mNdim = 4;                                  ///< order of the factor matrices (always 4)
      std::unique_ptr<Nb> mWmat[4];                  ///< Array holding the factor matrices

      std::string mAlgoVPQ = "Eigen";                ///< Selects the algorithm to decompose the V_PQ matrix
      bool mIsOvlpMetric = true;

      std::unique_ptr<Nb> mMat12;                    ///< Array holding the combination matrix
      std::unique_ptr<Nb> mScr12;                    ///< Scratch Array needed in Matrix multiplication if the combination matrix is stored in packed format
      bool mIsMpacked = false;                       ///< Flag which tells if mMat12 is stored in packed format? 
      const int mScrBlock = 500;                     ///< Defines the Block size for the scracth array mScr12

      std::set<std::string> mFilesToDelte;           ///< Set holding all files which should be deleted at destruction

      /*************************** Private Methods **********************************************/
      void InvertSymMatrix(Nb*, Nb*, std::string, In);
      void DecomposeRIMatrix(Nb*, std::string, const bool, In);

      long SaveRawData
         (  Nb*
         ,  unsigned int
         ,  unsigned int
         ,  fstream&
         );
      void ReadRawData
         (  fstream&
         ,  long
         ,  Nb*
         ,  unsigned int&
         ); 

   protected:
      /************************ Protected members ************************************************/
      In mNAtoms;                                                      ///< Number of atoms at this 4 index integral
      In mNAux;                                                        ///< Number of auxiliary functions

      std::vector<std::tuple<unsigned int, unsigned int>> mPairList;   ///< List for the atom pairs included
      std::vector<unsigned int> mPairRanks;                            ///< vector containing the rank for each atom pair

      bool mHaveAOInts = false;                      ///< Are integrals still in the AO or already in the MO basis?
      TurboInfo mTurboInfo;                          ///< Info from Turbomole 3 index integrals                  

      std::vector<unsigned int> mDims;               ///< vector containing the lengthes for each dimension 
      std::vector<unsigned int> mRank;               ///< vector containing the ranks for each dimension

      bool mIsAtomicBatched = true;                  ///< Flag which specifies if the integrals are considered as atomic batched or full tensor 
      std::unique_ptr<Nb> mAuxModeMat;               ///< Array holding the mode matrices for the auxiliary index (only needed if batching is used)
      std::unique_ptr<Nb> mUmatJ;                    ///< Array holding the left singular vectors of the coulomb metric with absorbed (sig)^(1/2) 


      /************* Protected Methods. Mainly to calculate the THC-MP2 energy in differen flavours ******************/

      NiceTensor<Nb> GetHalfMP2MOints
         (  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );

      NiceTensor<Nb> ContractWithCombiMat
         (  NiceTensor<Nb>&
         ,  const unsigned int
         ,  char
         );
      NiceTensor<Nb> ContractWithCombiMat
         (  NiceTensor<Nb>&
         ,  const unsigned int
         ,  char
         ,  const unsigned int
         ,  const unsigned int
         );

      NiceTensor<Nb> CombineFactorMatrices
         (  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> CombineFactorMatrices
         (  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> CombineFactorMatrices
         (  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> CombineFactorMatrices
         (  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> CombineFactorMatrices
         (  NiceTensor<Nb>&
         ,  unsigned int
         ,  unsigned int
         );

      NiceTensor<Nb> ContractMOIndices
         (  NiceTensor<Nb>&
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndices
         (  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndices
         (  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndices
         (  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndices
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );

      NiceTensor<Nb> ContractMOIndicesWith
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndicesWith
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      NiceTensor<Nb> ContractMOIndicesWith
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const bool
         );

      NiceTensor<Nb> ContractMOIndicesOf
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );

      NiceTensor<Nb> ContractAlongOcc
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const unsigned int
         );  

      void AddToH
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  std::vector<unsigned int>&
         );
      void AddToH
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  std::vector<unsigned int>&
         ,  const unsigned int
         ,  const unsigned int
         );
      void AddToH
         (  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  std::vector<unsigned int>&
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );

      NiceTensor<Nb> CreateHzjrIntermediate
         (  const unsigned int
         ,  const unsigned int
         ,  Nb*
         ,  Nb*
         ,  Nb
         ,  const bool
         );

      /************** For saving and reading intermediates represented as NiceTensor **********/
      void SaveIntermediate
         (  NiceTensor<Nb>&
         ,  std::string
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int
         );
      long SaveIntermediate
         (  NiceTensor<Nb>&
         ,  ofstream&);

      NiceTensor<Nb> ReadIntermediate
         (  std::string
         ,  const unsigned int
         ,  const unsigned int
         ,  const unsigned int)
         ;
      NiceTensor<Nb> ReadIntermediate
         (  ifstream&
         ,  long);


      unsigned int GetStartRank(const unsigned int);
      unsigned int GetEndRank(const unsigned int);
      unsigned int GetMaxRank();

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      RI4Integral(RI3Integral&,RI3Integral&,const bool, const bool);
      RI4Integral(std::string);

      ///> Delete copy- and move constructor
      RI4Integral(const RI4Integral&)  = delete;
      RI4Integral(RI4Integral&&)       = delete;

      ///> Destructor
      ~RI4Integral()
      {
         // delete generated files
         {
            LOGCALL("Delete intermediates");
            set<std::string>::iterator iter;
            for (iter = mFilesToDelte.begin(); iter != mFilesToDelte.end(); ++iter)
            {
               std::string file = *iter;
               const char* cfile = file.c_str();
               remove(cfile);
            }
         }
      }

      /******** Operations ********/
      ///> Delete copy- and move assignment operators
      RI4Integral& operator=(const RI4Integral&) = delete;
      RI4Integral& operator=(RI4Integral&&)      = delete;
 
      /******** Getters ********/
      const std::vector<unsigned int>& GetDims()  const;
      const std::vector<unsigned int>& GetRanks() const;
   
      Nb* GetCombinationMatrix() const;

      /******** Methods ********/
      NiceTensor<Nb> ToCanonicalTensor(); 

      void Transform( const unsigned int idx, Nb*, In, In);

      void SaveIntegral(std::string);
      void ReadIntegral(std::string);

      std::tuple<Nb,Nb> THCEMP2(std::map<std::string, std::vector<Nb>>&, const unsigned int, const bool);

      Nb THCEMP2J(std::map<std::string, std::vector<Nb>>&, LaplaceQuadrature<Nb>&);
      Nb THCEMP2K(std::map<std::string, std::vector<Nb>>&, LaplaceQuadrature<Nb>&);

      void GetUMatrix(Nb*, int, unsigned int); 
};


#endif /* DFINTEGRALS_H_INCLUDED */
