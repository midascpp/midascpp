/**
************************************************************************
* 
* @file                Ecor.cc
*
* Created:             19-02-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Ecor class methods.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>
#include<list>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/EcorCalcDef.h"
#include "ecor/Ecor.h"
#include "ecor/error_vectors.h"
//#include "ecor/EcorTransformer.h"
//#include "tensor/TensorEigSolver.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "diis.h"
#include "tensor/NiceTensor_vector.h"
//#include "tensor/TensorMacroSolver.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Timer.h"
#include "util/Io.h"

namespace X=contraction_indices;

/**
* Default "zero" Constructor 
* */
Ecor::Ecor(EcorCalcDef* apCalcDef, MolStruct* apMolStruct):
   mpMolStruct(apMolStruct),
   mpEcorCalcDef(apCalcDef),
   mReferenceState(apCalcDef->HaveDalton() 
                  ,apCalcDef->NBas()
                  ,apCalcDef->NOrb()
                  ,apCalcDef->NOcc()
                  ,apCalcDef->NVir()
                  ,apCalcDef->NFrozenOcc()
                  ,apCalcDef->NFrozenVir()
                  ,apCalcDef->OrbitalsFile()
                  ,apCalcDef->EnergiesFile()
                  ,apCalcDef->OverlapIntegralsFile()
                  ,apCalcDef->OneElectronIntegralsFile()
                  ,apCalcDef->TwoElectronIntegralsFile()
                  )
{
}


/**
* "Solve"
* */
void Ecor::Solve()
{
   /********* Definitions ********/
   const std::map<std::string, std::vector<Nb>>& orbital_energies = mReferenceState.GetEnergies();
   
   const bool& do_cp = mpEcorCalcDef->GetDoCP();

   unsigned int cc_order = 1;       // Re-defined according to CCMODEL in switch below


   /********* Calculate Reference state energy and nuclear energy **********/
   Nb ref_energy = mReferenceState.Energy(); 
   Nb nuc_energy = NuclearEnergy(); 
   Nb correlation_energy=0.0;

   Nb hf_energy = ref_energy+nuc_energy;

   /******** Convert ReferenceState to Canonical *********/
   if(do_cp)
   {
      std::cout << "#######CONVERTING AO TWO-ELECTRON INTEGRALS TO CANONICAL FORMAT#########" << std::endl;
      mReferenceState.ToCanonicalReference(1.e-8);
      std::cout << "Two electron ints rank = " << 
               dynamic_cast<CanonicalTensor<Nb>*>(mReferenceState.GetTwoElectronIntegrals().GetTensor())->GetRank() << std::endl;
   }

   

   /******** Declare function to calculate error vectors depending on the CC model ********/
   std::function<std::vector<NiceTensor<Nb>>(const std::vector<NiceTensor<Nb>>&, 
                                             const T1Integrals&, 
                                             const std::map<std::string, std::vector<Nb>>&,
                                             const bool&,
                                             const Nb&)> calculate_error_vector;
   
   // Constant reference to CCMODEL read from input
   const std::string& input_cc_model = mpEcorCalcDef->GetCCModel();

   std::cout << "CCMODEL read from input = " << input_cc_model << endl;

   {
      // Enumerate CC models and create map from string to CCMODEL (because switch cannot take a std::string)
      enum CCMODEL {CC2, CCSD, CC3};

      const std::map<std::string, CCMODEL> models =
      {
         {"CC2", CC2},
         {"CCSD", CCSD},
         {"CC3", CC3},
      };
      
      // Define CCMODEL from EcorCalcDef
      CCMODEL cc_model = models.at(input_cc_model);
      
      // Choose the corresponding error vector function (you need to choose a specific instance of the template - in this case T=Nb)
      switch(cc_model)
      {
         case CC2:
         {
            calculate_error_vector = cc2_error_vector<Nb>;
            cc_order = 2;
            break;
         }
         case CCSD:
         {
            calculate_error_vector = ccsd_error_vector<Nb>;
            cc_order = 2;
            break;
         }
         case CC3:
         {
            calculate_error_vector = cc3_error_vector<Nb>;
            cc_order = 3;
            break;
         }
      }
   }


   /**** Initialize CC amplitudes *****/
   std::list<std::vector<NiceTensor<Nb>>> t;


   /**** Declare Starting Amplitudes ****/
      NiceTensor<Nb> zeros(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mReferenceState.NVir()),
                                      static_cast<unsigned int>(mReferenceState.NOcc())}));

   T1Integrals mo_integrals = mReferenceState.TransformIntegrals(zeros);         // This needs a fix. mo_integrals are not used again. 
                                                                                 // Move constructor for T1Integrals???
   
   std::vector<NiceTensor<Nb>> t_guess;
   // Restart
   if(mpEcorCalcDef->Restart())
   {
      std::cout << "READING RESTART AMPLITUDES FROM FILE" << std::endl;
      std::ifstream input("amplitudes.dat", std::ios::in | std::ios::binary );
      t_guess = read_NiceTensor_vector<Nb>(input);
      input.close();
   }
   else
   {
      std::cout << "-=-=-=-=-INITIALIZING T_GUESS-=-=-=-=-" << std::endl;
      t_guess = starting_amplitudes(cc_order, mo_integrals,
                                      orbital_energies.at("v"),
                                      orbital_energies.at("o"),
                                      do_cp);
      if(do_cp)
      {
         std::cout << "t_guess[2] rank = " << dynamic_cast<CanonicalTensor<Nb>*>(t_guess[2].GetTensor())->GetRank() << std::endl;
      }
      std::cout << std::endl;
      std::cout << std::endl;
   }





   /**** Initialize T1-transformed integrals ****/
   T1Integrals integrals = mo_integrals;




   /**** Initialize error vectors *****/
   // Vector of vectors to store previous iterations for DIIS
   std::list<std::vector<NiceTensor<Nb>>> error_vectors;





   /********* Energy differences denominators ***********/
   std::vector<NiceTensor<Nb>> denominators{NiceTensor<Nb>(new Scalar<Nb>(1.0))};

   for(int iorder=1; iorder<=cc_order; ++iorder)
   {
      denominators.push_back(NiceTensor<Nb>(new EnergyDenominatorTensor<Nb>(iorder,orbital_energies.at("v"), orbital_energies.at("o"), 4)));
   }

   
   /**** Calculate MP2 energy ****/
   Nb mp2_energy = hf_energy + mReferenceState.MP2Energy();

   // Test if MP2 amplitudes are used as t_guess[2]
   Nb test_mp2_energy = hf_energy+CorrelationEnergy(t_guess, integrals);
   Nb mp2_energy_error = mp2_energy - test_mp2_energy;
   
   midas::stream::ScopedPrecision(15, std::cout);
   std::cout << "MP2 energy error using t_guess[2]:   " << mp2_energy_error << std::endl;

//   if(mp2_energy_error > 1.e-6) 
//   {
//      std::cout << "WARNING: CorrelationEnergy() does not return MP2 energy in first iteration! Check t_guess!" << std::endl;
//      exit(1);
//   }


   /******** CC iterative solver ******/
   // Have a look at diis.h
   #define CC_THRESHOLD 1.e-10
   std::cout << " ######## Starting CC equation solver ###########" << std::endl;
   int iiter=0;
   bool converged(false);
   Nb error_norm_sum = 1.0;         // Sum of error vector norms used to check convergence for arbitrary CC order
   Nb f = 1.e-2;                    // "Fudge-factor" used to scale T_CP during the iterations
   Nb t_cp = 1.e-2;
   
   if(do_cp)
   {
      std::cout << "-=-=-=-=-CP DECOMPOSITION PARAMETERS-=-=-=-=-" << std::endl;
      std::cout << "f =              " << f << std::endl;
      std::cout << "T_CC =   " << CC_THRESHOLD << std::endl;
      std::cout << std::endl;
   }

   /****** First iteration *****/
   cout << "-=-=-=-=-ITERATION NUMBER " << ++iiter << "-=-=-=-=-" << endl;

   /*** Calculate error vectors ***/
   error_vectors.push_back(calculate_error_vector( t_guess, integrals, orbital_energies, do_cp, 1.e-3));
//   error_vectors.push_back(calculate_error_vector( t_guess, integrals, orbital_energies, false, 1.e-3));

   std::vector<NiceTensor<Nb>>& last_error = error_vectors.back();
   if(do_cp)
   {
      // Re-compress error vectors
      for(int iorder=2; iorder<=cc_order; ++iorder)
      {
         std::cout << "error_vector[" << iorder << "] rank before recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
         last_error[iorder] = last_error[iorder].ToCanonicalTensor(1.e-3);
         std::cout << "error_vector[" << iorder << "] rank after recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
      }
   }
   
   /*** quasi-Newton update ***/
   std::vector<NiceTensor<Nb>> dt = quasi_newton_update( error_vectors.back(), denominators );
   if(do_cp)
   {
      // Re-compress update
      for(int iorder=2; iorder<=cc_order; ++iorder)
      {
         std::cout << "quasi_newton_update[" << iorder << "] rank before recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(dt[iorder].GetTensor())->GetRank() << std::endl;
         dt[iorder] = dt[iorder].ToCanonicalTensor(1.e-6);
         std::cout << "quasi_newton_update[" << iorder << "] rank after recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(dt[iorder].GetTensor())->GetRank() << std::endl;
      }
   }
   t.push_back(t_guess + dt );
   if(do_cp)
   {
      // Re-compress cluster amplitudes
      for(int iorder=2; iorder<=cc_order; ++iorder)
      {
         std::cout << "t[" << iorder << "] rank before recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(t.back()[iorder].GetTensor())->GetRank() << std::endl;
         t.back()[iorder] = t.back()[iorder].ToCanonicalTensor(1.e-6);
         std::cout << "t[" << iorder << "] rank after recompression = " 
                   << dynamic_cast<CanonicalTensor<Nb>*>(t.back()[iorder].GetTensor())->GetRank() << std::endl;
      }
   }
   
   /*** Update T1-transformed integrals ***/
   integrals = mReferenceState.TransformIntegrals(t.back()[1]);

   /*** Calculate correlation energy ***/
   correlation_energy=CorrelationEnergy(t.back(), integrals);

   /*** Set T_CP larger than or equal to the accuracy of the AO integrals ***/
   if(f*dt[2].Norm() > 1.e-8){t_cp = f*dt[2].Norm();}
   else{t_cp = 1.e-8;}

   /*** Output ***/
   for(int iorder = 1; iorder<=cc_order; ++iorder)
   {
      std::cout << "error_vector[" << iorder << "] norm =   " << error_vectors.back()[iorder].Norm() << std::endl;
   }
   std::cout << "CC energy =                      " << hf_energy + correlation_energy << std::endl;
   std::cout << "T_CP after first iteration =     " << t_cp << std::endl;
   std::cout << std::endl;
   std::cout << std::endl;
   std::cout << std::endl;


   /********** Begin the iterations *********/
   while(!converged)
   {
      std::cout << "-=-=-=-=-=-=-=- ITERATION NUMBER " << ++iiter <<" -=-=-=-=-=-=-=-" << std::endl;
      
      const std::vector<NiceTensor<Nb>>& last_t = t.back();

      /*** Calculate error vectors ***/
      error_vectors.push_back(calculate_error_vector( last_t, integrals, orbital_energies, do_cp, t_cp));
//      error_vectors.push_back(calculate_error_vector( last_t, integrals, orbital_energies, false, t_cp));
   
      std::vector<NiceTensor<Nb>>& last_error = error_vectors.back();

      if(do_cp)
      {
         // Re-compress error vectors
         for(int iorder=2; iorder<=cc_order; ++iorder)
         {
            std::cout << "Error_vector[" << iorder << "] rank before recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
            last_error[iorder] = last_error[iorder].ToCanonicalTensor(1.e-3);
            std::cout << "Error_vector[" << iorder << "] rank after recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
            std::cout << std::endl;
         }
      }
      
      /*** quasi-Newton update ***/
      std::vector<NiceTensor<Nb>> dt = quasi_newton_update( last_error, denominators );
      if(do_cp)
      {
         // Update t_cp
         if(f*dt[2].Norm() > 1.e-8){t_cp = f*dt[2].Norm();}
         else{t_cp = 1.e-8;}
         std::cout << "New T_CP after QN update = " << t_cp << std::endl;
         std::cout << std::endl;
         
         // Re-compress update
         for(int iorder=2; iorder<=cc_order; ++iorder)
         {
            std::cout << "Quasi_newton_update[" << iorder << "] rank before recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(dt[iorder].GetTensor())->GetRank() << std::endl;
            dt[iorder] = dt[iorder].ToCanonicalTensor(t_cp);
            std::cout << "Quasi_newton_update[" << iorder << "] rank after recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(dt[iorder].GetTensor())->GetRank() << std::endl;
            std::cout << std::endl;
         }
      }

      t.push_back( last_t + dt );

      /*** DIIS overwrites the last t and error vector ***/
      diis_update(t, error_vectors, 3);

      if(do_cp)
      {
         for(int iorder=2; iorder<=cc_order; ++iorder)
         {
            // Re-compress error vectors
            std::cout << "Error_vector[" << iorder << "] rank after DIIS before recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
            last_error[iorder] = last_error[iorder].ToCanonicalTensor(t_cp);
            std::cout << "Error_vector[" << iorder << "] rank after DIIS after recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(last_error[iorder].GetTensor())->GetRank() << std::endl;
            std::cout << std::endl;

            // Re-compress cluster amplitudes
            std::cout << "New t[" << iorder << "] rank before recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(t.back()[iorder].GetTensor())->GetRank() << std::endl;
            t.back()[iorder] = t.back()[iorder].ToCanonicalTensor(t_cp);
            std::cout << "New t[" << iorder << "] rank after recompression = " 
                      << dynamic_cast<CanonicalTensor<Nb>*>(t.back()[iorder].GetTensor())->GetRank() << std::endl;
            std::cout << std::endl;
         }
      }

      const std::vector<NiceTensor<Nb>>& new_t = t.back();

      /*** Update integrals ***/
      integrals = mReferenceState.TransformIntegrals(new_t[1]);

      /*** Calculate correlation energy ***/
      correlation_energy=CorrelationEnergy(new_t, integrals);

      /*** Output ***/
      midas::stream::ScopedPrecision(15, std::cout);
      std::cout << "-=-=-=-RESULTS OF ITERATION " << iiter << " -=-=-=-" << std::endl;
      std::cout << "Correlation energy =             " << correlation_energy << std::endl;
      std::cout << "Electronic CC energy =           " << ref_energy+correlation_energy << std::endl;
      std::cout << "Total CC energy =                " << hf_energy+correlation_energy<< std::endl;
      std::cout <<  std::endl;
      for(int iorder = 1; iorder<=cc_order; ++iorder)
      {
         std::cout << "error_vector[" << iorder << "] norm =   " << last_error[iorder].Norm() << std::endl;
      }
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      
      /*** Do checks for convergence ***/
      error_norm_sum = 0;
      for(int iorder=1; iorder<=cc_order; ++iorder)
      {
         error_norm_sum += last_error[iorder].Norm();
      }

      converged = ( error_norm_sum < CC_THRESHOLD ); 
   }

   /***** Write amplitudes for restart *****/
   std::ofstream output("amplitudes.dat", std::ios::out | std::ios::binary );
   write_NiceTensor_vector(t.back(), output);
   output.close();

   /***** Final output *****/
   midas::stream::ScopedPrecision(15, std::cout);
   std::cout << "############# FINAL RESULTS OF CC CALCULATION ##############" << std::endl;
   std::cout << "HF  energy =                        " << hf_energy << std::endl;
   std::cout << "MP2 energy =                        " << mp2_energy << std::endl;
   std::cout << input_cc_model << " energy =         " << hf_energy+correlation_energy << std::endl;
   std::cout << endl;
}




/**
* Calculate correlation energy
* */
Nb Ecor::CorrelationEnergy(const std::vector<NiceTensor<Nb>>& t_amplitudes, const T1Integrals& integrals) const
{
   Nb result=0.0;

   const NiceTensor<Nb>& l_ovov = integrals.L_T1["ovov"];
  
   result = (contract(t_amplitudes[2][X::p,X::q,X::r,X::s], l_ovov[X::q,X::p,X::s,X::r]) 
           + contract(
                contract(l_ovov[X::q,X::p,X::s,X::r],t_amplitudes[1][X::p,X::q])
           , t_amplitudes[1][X::r,X::s])).GetScalar();

   return result;
}


/**
* Calculate CCSD(T) correction energy
* */
Nb Ecor::CCSDt_correction(const std::vector<NiceTensor<Nb>>& ccsd_amplitudes) const
{
   // Declare triples denominators
   const std::map<std::string, std::vector<Nb>>& orbital_energies = mReferenceState.GetEnergies();
   
   NiceTensor<Nb> triples_denominators = NiceTensor<Nb>(new EnergyDenominatorTensor<Nb>
                                                (3,orbital_energies.at("v"), orbital_energies.at("o"), 8));
   
   // Calculate MO integrals
   NiceTensor<Nb> zeros(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(mReferenceState.NVir()), static_cast<unsigned int>(mReferenceState.NOcc())}));

   T1Integrals mo_integrals(mReferenceState.TransformIntegrals(zeros));

   // Calculate the energy correction
   const TensorLibrarian<Nb>& g = mo_integrals.g_T1;
   const TensorLibrarian<Nb>& l = mo_integrals.L_T1;
   const NiceTensor<Nb>& t2 = ccsd_amplitudes[2];

   NiceTensor<Nb> t1_bar = 2.0*ccsd_amplitudes[1];
   NiceTensor<Nb> t2_bar = 4.0*ccsd_amplitudes[2] - 2.0*ccsd_amplitudes[2][X::p,X::s,X::r,X::q];

   NiceTensor<Nb> star_t3 = -1.0*(contract(t2[X::p,X::q,X::d,X::s], g["vovv"][X::t,X::u,X::r,X::d]) 
                                - contract(t2[X::p,X::q,X::r,X::l], g["vooo"][X::t,X::u,X::l,X::s]) 
                                + contract(t2[X::p,X::q,X::d,X::u], g["vovv"][X::r,X::s,X::t,X::d]) 
                                - contract(t2[X::p,X::q,X::t,X::l], g["vooo"][X::r,X::s,X::l,X::u]) 
                                + contract(t2[X::r,X::s,X::d,X::q], g["vovv"][X::t,X::u,X::p,X::d]) 
                                - contract(t2[X::r,X::s,X::p,X::l], g["vooo"][X::t,X::u,X::l,X::q]) 
                                + contract(t2[X::r,X::s,X::d,X::u], g["vovv"][X::p,X::q,X::t,X::d]) 
                                - contract(t2[X::r,X::s,X::t,X::l], g["vooo"][X::p,X::q,X::l,X::u]) 
                                + contract(t2[X::t,X::u,X::d,X::q], g["vovv"][X::r,X::s,X::p,X::d]) 
                                - contract(t2[X::t,X::u,X::p,X::l], g["vooo"][X::r,X::s,X::l,X::q]) 
                                + contract(t2[X::t,X::u,X::d,X::s], g["vovv"][X::p,X::q,X::r,X::d]) 
                                - contract(t2[X::t,X::u,X::r,X::l], g["vooo"][X::p,X::q,X::l,X::s]) 
                                 )*triples_denominators;


   NiceTensor<Nb> star_T1 = contract(star_t3[X::p,X::q,X::c,X::k,X::d,X::l], l["ovov"][X::k,X::c,X::l,X::d]) 
                          - contract(star_t3[X::p,X::l,X::c,X::k,X::d,X::q], l["ovov"][X::k,X::c,X::l,X::d]);
                                                                                
   NiceTensor<Nb> star_T2 = contract(star_t3[X::p,X::q,X::c,X::s,X::d,X::k], l["vvov"][X::r,X::c,X::k,X::d]) 
                          - contract(star_t3[X::p,X::k,X::c,X::s,X::d,X::q], g["ovvv"][X::k,X::d,X::r,X::c]) 
                          - contract(star_t3[X::p,X::q,X::r,X::k,X::c,X::l], l["ooov"][X::k,X::s,X::l,X::c]) 
                          + contract(star_t3[X::p,X::l,X::r,X::k,X::c,X::q], g["ooov"][X::k,X::s,X::l,X::c]);

   Nb result = (contract(t1_bar[X::p,X::q], star_T1[X::p,X::q])
              + contract(t2_bar[X::p,X::q,X::r,X::s], star_T2[X::p,X::q,X::r,X::s])
               ).GetScalar();
   
   return result;
}



/**
* Calculate nuclear energy
* */
Nb Ecor::NuclearEnergy() const
{

   Nb result;
   In ianuc;
   In ibnuc;
   In numat;

   numat = mpMolStruct->NumberOfNuclei();

   result = C_0;

   if (numat < 1)
   { 
      MidasWarning("No nuclei: Can't compute nuclear energy! Contribution is missing in HF energy!");
   }


   for (ianuc = I_1; ianuc <= numat; ianuc++)
   {
      Vector3D origina = mpMolStruct->GetNucleusCoordinates3D(ianuc-I_1);
      for (ibnuc = ianuc+1; ibnuc <= numat; ibnuc++)
      {
         Vector3D originb = mpMolStruct->GetNucleusCoordinates3D(ibnuc-I_1);
         Nb dist = origina.Dist(originb);
         result += mpMolStruct->GetCharge(ianuc-I_1) * mpMolStruct->GetCharge(ibnuc-I_1)/dist;
      }
   }

   return result;
}


// Declare starting amplitudes
std::vector<NiceTensor<Nb>> starting_amplitudes(const unsigned int order,
      const T1Integrals& integrals,
      const std::vector<Nb>& e_vir,
      const std::vector<Nb>& e_occ,
      const bool& to_cp_tensors)
{
   std::vector<NiceTensor<Nb>> t(order+1);

   t[0]=NiceTensor<Nb>(new Scalar<Nb>(1.0));

   // t[1] is a SimpleTensor of 0s. 
   // t[2] is the MP2 doubles amplitudes that can be recompressed.
   // Higher-order amplitudes are either SimpleTensors or CanonicalTensors of 0s.
   for(unsigned int iorder=1;iorder<=order;++iorder)
   {
      if(iorder==1)
      {
         t[iorder]=NiceTensor<Nb>(new SimpleTensor<Nb>(edt_dims(iorder, e_vir.size(), e_occ.size() )));
      }
      else if(iorder==2)
      {
         NiceTensor<Nb> denominators(new EnergyDenominatorTensor<Nb>(2,e_vir, e_occ, 4));
         t[iorder]=(( -1.0 ) * integrals.g_T1["vovo"]*denominators);
         if(to_cp_tensors)
         {
            t[iorder] = t[iorder].ToCanonicalTensor(1.e-2);
         }
      }
      else
      {
         if(to_cp_tensors) {t[iorder] = NiceTensor<Nb>(new CanonicalTensor<Nb>(edt_dims(iorder, e_vir.size(), e_occ.size()), 0));}
         else {t[iorder] = NiceTensor<Nb>(new SimpleTensor<Nb>(edt_dims(iorder, e_vir.size(), e_occ.size())));}
      }
   }
   
   return t;
}


// Quasi-Newton update
std::vector<NiceTensor<Nb>> quasi_newton_update(
      const std::vector<NiceTensor<Nb>>& error_vector,
      const std::vector<NiceTensor<Nb>>& denominators)
{
   std::vector<NiceTensor<Nb>> t_update;

   for(unsigned int iorder=0;iorder<error_vector.size();++iorder)
   {
      t_update.push_back( static_cast<Nb>(-1) * (error_vector[iorder]*denominators[iorder]) );
   }
   
   return t_update;
}
