/**
************************************************************************
* 
* @file                
*
* Created:            20-03-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for T1 ampltiudes
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <stdio.h>

#include "util/InterfaceOpenMP.h"

#include "ecor/T1Amplitude.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "tensor/LaplaceQuadrature.h"
#include "ecor/IntermedCont.h"

namespace X=contraction_indices;

/**
 * * Constructor 
 * * */
T1Amplitude::T1Amplitude
   (  RI3Integral& bqij
   ,  RI3Integral& bqkl
   ,  std::map<std::string, std::vector<Nb>> orben
   ,  LaplaceQuadrature<Nb>&& lapquad 
   ,  const bool doBatching
   )
   :  RI4Integral(   bqij
                  ,  bqkl
                  ,  doBatching 
                  ,  doBatching
                  )
   ,  mOrben(orben)
   ,  mLapQuad(std::move(lapquad))
   
   
{
}


void T1Amplitude::CalcIterm(NiceTensor<Nb>& fock)
{
   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1} and do not absorb weights
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, mOrben.at("v"), mOrben.at("oct"), mLapQuad);
   unsigned int nlap = mLapQuad.NumPoints();

   // unsigned int natompairs = mNAtoms * mNAtoms; // replace later with prescreend list 
   unsigned int natompairs = mPairList.size();
   for (unsigned int ilap = 0; ilap < nlap; ilap++)
   {
      for (unsigned int cdpair = 0; cdpair < natompairs; cdpair++)
      {
         // ContractMOIndicesOf      
         //NiceTensor<Nb>  akr2z = ContractMOIndicesWith(fock, eabij, 2, ilap, ilap+1, jrank, jrankend, true); 
      }      
   } 

   
}


