/**
 *************************************************************************
 * 
 * @file                reducerank.h
 *
 * Created:             28.06.2016
 *
 * Author:              Gunnar Schmitz (Gunnar.Schmitz@chem.au.dk)
 *
 * \brief               reducerank: Performs Rank Reduction of a CP tensor 
 * 
 * 
 * Detailed Description: The rank reduction algorithm works by converting 
 *                       the CP format tensor into a Tucker format tensor 
 *                       by a HOOID like procedure. The core tensor of
 *                       the Tucker type tensor is afterwards decompsed 
 *                       via a CP-ALS algorithm. 
 *
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 *************************************************************************
 **/

#include<cstring>
#include<cstdlib>
#include<stack>
#include<numeric>

#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/GESVD.h"

#include "tensor/TensorDecomposer.h"
#include "util/CallStatisticsHandler.h"


namespace X=contraction_indices;

template <class T>
NiceTensor<T> reducerank
   (  const NiceTensor<T>& target
   ,  const T thrsvd
   ,  const T thrcp
   ,  const unsigned int maxiter
   ,  const bool docheckerr
   ,  const T maxerr
   ,  std::vector<unsigned int> istart
   ,  std::vector<unsigned int> batln
   ,  midas::tensor::TensorDecomposer* decomposer = NULL)
{
   /*
    *  split tensor into blocks and perform reduction on each block and combine afterwards again
    */
   
   // Check if target is in CP format!!
   if (target.Type() != BaseTensor<T>::typeID::CANONICAL)
   {
      throw std::runtime_error("For rank reduction tensor must be in CP format!");
   }


   // get info on underlying canonical tensor
   std::vector<unsigned int> dims = target.GetDims();
   const unsigned int ndim(target.NDim()); 
   CanonicalTensor<T>* target_canon = dynamic_cast<CanonicalTensor<T>*>(target.GetTensor());
   T** matrices = target_canon->GetModeMatrices();
   unsigned int nrankold = target_canon->GetRank();
   
   // create new Canonical tensor on which we will operate
   T** modemat_in  = new T*[ndim];
   T** modemat_out = new T*[ndim];
   for (unsigned imode = 0; imode < ndim; ++imode)
   {
      modemat_in[imode]  = new T[dims[imode]*nrankold];
      modemat_out[imode] = new T[dims[imode]*nrankold];
      memcpy(reinterpret_cast<void*>(modemat_in[imode]),reinterpret_cast<void*>(matrices[imode]),dims[imode]*nrankold*sizeof(T));
   }

   unsigned int nranknew,nrankcut; 

   nranknew = 0;
   unsigned int nsum = 0; 
   for (unsigned int iblock = 0; iblock < batln.size(); ++iblock)
   {

      // get number of ranks we want to cut out for this iblock
      nrankcut = batln[iblock];

      // copy mode matrices of subtensor
      T** tmp = new T*[ndim];
      for (unsigned imode = 0; imode < ndim; ++imode)
      {
         tmp[imode]  = new T[dims[imode]*nrankcut];

         for (unsigned int irank = 0; irank < nrankcut; ++irank)
         {
            for (unsigned int ibas = 0; ibas < dims[imode]; ++ibas)
            {
               tmp[imode][irank*dims[imode] + ibas] = modemat_in[imode][(istart[iblock]+irank)*dims[imode] + ibas];
            }
         }
      }
      nsum += istart[iblock];

      // create subtensor and reduce its rank
      NiceTensor<T> tensor(new CanonicalTensor<T>(dims,nrankcut,tmp));
      tensor = reducerank(tensor,thrsvd,thrcp,maxiter,docheckerr,maxerr,decomposer);
      //tensor = decomposer->Decompose(tensor);

      // get handle
      CanonicalTensor<T>* tensor_canon = dynamic_cast<CanonicalTensor<T>*>(tensor.GetTensor());
      T** matrices = tensor_canon->GetModeMatrices();
      unsigned int nrankadd = tensor_canon->GetRank();

      // add to full tensor
      for (unsigned int imode = 0; imode < ndim; ++imode)
      {
         for (unsigned int irank = 0; irank < nrankadd; ++irank)
         {
            for (unsigned int ibas = 0; ibas < dims[imode]; ++ibas)
            {
               modemat_out[imode][(irank+nranknew)*dims[imode] + ibas] = matrices[imode][irank*dims[imode] + ibas];
            }
         }
      }
      nranknew += nrankadd;
      Mout << " add rank, nrankcut " << nrankadd <<  " " << nrankcut <<  std::endl;
   }

   // If we were really lucky and the rank is reduced trim of unsed memory by reallocating
   if (nranknew < nrankold) 
   {
      for(unsigned int imode = 0; imode < ndim; ++imode)
      {
         modemat_out[imode] = reinterpret_cast<T*>(realloc(modemat_out[imode], nranknew*dims[imode]*sizeof(T)));
      }    
   }

   // pack everything to a tensor and return
   NiceTensor<T> result(new CanonicalTensor<T>(dims,nranknew,modemat_out));
 
   return result;
}

/**
 * @brief Performs a rank reduction of a tensor in CP format (C2T/T2C)
 *
 *  The rank reduction algorithm works by converting the CP format tensor into
 *  a Tucker format tensory by a HOSVD like procedure. The core tensor of
 *  the Tucker type tensor is afterwards decompsed via a CP-ALS algorithm. 
 * 
 *
 * @param  target     Tensor in CP format, which rank should be reduced 
 * @param  thrsvd     Threshold for the truncated SVDs in the HOSVD like procedure
 * @param  thrcp      Threhsold for the CP-ALS algorithm if no decomposer was chosen (decomposer == NULL)
 * @param  maxiter    Maximal number of iterations in the ALS
 * @param  docheckerr Use an error estimate based criterion instead of thrsvd to control the algorithm 
 * @param  maxerr     Maximal allowed error for the error bound 
 * @param  decomposer Optional parameter with specifices a given midas::tensor::TensorDecomposer 
 * @return Returns a Tensor in CP format with (hopefully) reduced rank.
 * */
template <class T>
NiceTensor<T> reducerank
   (  const NiceTensor<T>& target
   ,  const T thrsvd
   ,  const T thrcp
   ,  const unsigned int maxiter
   ,  const bool docheckerr
   ,  const T maxerr
   ,  midas::tensor::TensorDecomposer* decomposer = NULL
   )
{

   // constants
   const bool locdbg   = false;
   const bool dbg_svd  = false;  
   const bool doprtmat = false;  

   // locals
   std::vector<unsigned int> dims=target.GetDims();
   const unsigned int ndim(target.NDim());
   T thrcutsvd;

   thrcutsvd = thrsvd;  // if docheckerr this threshold is not fix

   // use provided decomposer?
   bool use_decomposer = decomposer != NULL;

   if (std::accumulate(dims.begin(), dims.end(), 0) < 5)
   {
      // quick exit if the tensor has low dimensions (it then still could have large rank, but this would be very strange)
      return target;
   } 

   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   // Calculate truncated SVD of the mode matrices of target
   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   // Check if target is in CP format!!
   if (target.Type() != BaseTensor<T>::typeID::CANONICAL)
   {
      throw std::runtime_error("For rank reduction tensor must be in CP format!");
   }

   // 1) get info on underlying canonical tensor
   if (locdbg) Mout << "Get info on underlying Canonical Tensor" << std::endl;
   CanonicalTensor<T>* target_canon = dynamic_cast<CanonicalTensor<T>*>(target.GetTensor());
   T** mode_matrices     = target_canon->GetModeMatrices();
   unsigned int nrankold = target_canon->GetRank();

   if (nrankold < 5) 
   {
      return target;
   }

   // Get maximum local rank "nlrankmax" 
   unsigned int nlrankmax = nrankold;
   
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      unsigned int ndimranks = I_1;
      for (unsigned int jmode = 0; jmode < ndim; ++jmode)
      {
         if (jmode != imode) ndimranks = ndimranks * dims[jmode];
      }
      nlrankmax = std::max<In>(nlrankmax,std::min<In>(ndimranks,dims[imode]));
   }
   
   if (locdbg) 
   {
      Mout << "Old rank:         " << nrankold  << std::endl; 
      Mout << "Max. local rank:  " << nlrankmax << std::endl; 
      Mout << "Dimensions:       " << dims      << std::endl; 
   }

   // allocate memory for the projected mode_matrices
   T** proj_modemat = new T*[ndim];
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      proj_modemat[imode] = new T[nrankold*nlrankmax];
   }
 
   // memory for the projection matrices U and singular values
   T** umat(new T*[ndim]);
   T** sval(new T*[ndim]);

   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      umat[imode] = new T[dims[imode]*nlrankmax];
      sval[imode] = new T[std::min<In>(dims[imode],nlrankmax)];
   }

   // 2) Do the ndim SVDs
   if (locdbg) Mout << "Do the SVDs" << std::endl;
   In ranks[ndim];

   T** matrix = new T*[ndim];
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      matrix[imode] = new T[dims[imode]*nrankold];
   }
  
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      LOGCALL("HOSVD guess");

      memcpy(reinterpret_cast<void*>(matrix[imode]),reinterpret_cast<void*>(mode_matrices[imode]),dims[imode]*nrankold*sizeof(T));

      if (locdbg) Mout << " GESVD I " << dims[imode] << " " << nrankold << std::endl; 
      SVD_struct<T> svdi = GESVD(matrix[imode], static_cast<int>(dims[imode]), static_cast<int>(nrankold), 'A');

      if (locdbg) 
      {
         Mout << "Mode: " << imode << std::endl;
         Mout << "Singular values: " << std::endl;
         for (unsigned int irank = 0; irank<svdi.Min(); ++irank)
         {
            Mout << irank << " " << svdi.s[irank] << std::endl;
         }
      }

      if (docheckerr) 
      {
         // We want to truncate the SVDs so that we are not above maxerr 
         // for this prupose we have first to collect all singular vaules
         for (unsigned int irank = 0; irank < svdi.Min(); ++irank)
         {
            sval[imode][irank] = svdi.s[irank];
         }
         ranks[imode] = svdi.Min();
      }
      else
      {
         // get rank/truncate SVD and save singular values
         In svd_currank = 0;
         for (unsigned int irank = 0; irank < svdi.Min(); ++irank)
         {
            if (svdi.s[irank] > thrcutsvd)
            {
               svd_currank += I_1;
            }
            sval[imode][irank] = svdi.s[irank];
         }
         ranks[imode] = std::max<In>(svd_currank, I_1); 
      }

      // fill in projection matrices
      size_t counter = 0;
      for (unsigned int irank = 0; irank < ranks[imode]; ++irank)
      {
         for (unsigned int idim = 0; idim<dims[imode]; ++idim)
         {
            umat[imode][irank*dims[imode] + idim] = svdi.u[counter++];
         }
      }
 
   }
   
   if (docheckerr) 
   {
      // choose threshold for SVDs so that we are just below maxerr   

      // calculate estimate for the error of the tucker decomposition
      int maxdim = 0;
      for (unsigned int imode = 0; imode < ndim; ++imode)
      {
         maxdim = std::max<In>(maxdim,ranks[imode]);
      }

      thrcutsvd = 1e-18; // init
      std::vector<bool> skipit(ndim,false);
      for (unsigned int icut = 1; icut < maxdim; ++icut)
      {

         for (unsigned int imode = 0; imode < ndim; ++imode)
         {
            T sig2 = 0.0;
            if (icut < ranks[imode])
            {
               for (unsigned int irank = ranks[imode]; irank >= ranks[imode] - icut; --irank)
               {
                  sig2 += sval[imode][irank] * sval[imode][irank];
               }
               // Contribution of this mode to error estimate must be below maxerr/ndim
               if (sig2 * sqrt(nlrankmax) < maxerr/ndim)
               {
                  thrcutsvd = sval[imode][ranks[imode]-icut];
               }
               else
               {
                  skipit[imode] = true;
               }
            }   
         }
         if (std::all_of(std::begin(skipit), std::end(skipit), [](bool exit){ return exit; })) 
         {
            break;
         }

      }
      Mout << "thrcutsvd " << thrcutsvd << std::endl;
   }

   // clean up a bit
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      delete[] matrix[imode];
   }
   delete[] matrix;

   // 3) main loop
   for (unsigned int iter = 0; iter<maxiter; ++iter)
   {
      // Project the mode matrices
      for (unsigned int imode = 0; imode<ndim; imode++)
      {
         //++++++++++++++++++++++++++++++++++++++++++++++
         //    ~b = (U^k)^T b^k
         //++++++++++++++++++++++++++++++++++++++++++++++

         char nc = 'T'; char nt = 'N';
         T alpha = static_cast<T>(1.0); T beta  = static_cast<T>(0.0);
         int m = ranks[imode];  
         int n = nrankold;     
         int k = dims[imode];  

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha,
                                        umat[imode], &k,
                                        mode_matrices[imode], &k,
                                        &beta, proj_modemat[imode], &m );

         if (doprtmat)
         {
            T* prtMat = new T[dims[imode]*nlrankmax];
            memcpy(reinterpret_cast<void*>(prtMat),
                reinterpret_cast<void*>(umat[imode]),dims[imode]*ranks[imode]*sizeof(T));

            NiceTensor<T> prtTensor ( new SimpleTensor<T>  (std::vector<unsigned int>{static_cast<unsigned int>(dims[imode]),
                                                                                      static_cast<unsigned int>(ranks[imode])},prtMat));
            Mout << "U (iter): " << iter << std::endl;
            Mout << "|| U ||: " << prtTensor.Norm();
            Mout << prtTensor << std::endl;
            
         }

      }

      // calculate estimate for the error of the tucker decomposition
      T sig2 = C_0;
      for (unsigned int kmode = 0; kmode < ndim; ++kmode)
      {
         for (unsigned int irank = ranks[kmode]; irank < std::min<In>(dims[kmode],nlrankmax); ++irank)
         {
            sig2 += sval[kmode][irank] * sval[kmode][irank];
         }
      }
      T errtuck = sqrt(sig2) * sqrt(nlrankmax);

      if (docheckerr) 
      {
         // we do not use thrsvd is criterion but this error estimate to stop
         // the algorithm
         if (errtuck < maxerr)
         {
            break;
         }
      }
      else
      {
         if (errtuck < thrcutsvd * 0.1) 
         {
            Mout << " exit after " << iter <<  " iterations" << std::endl;
            break;    // leave earlier if we have already a good accuracy
         }
      }

      for (unsigned int kmode = 0; kmode < ndim; ++kmode)
      {
         LOGCALL("HOOI loop");

         // set dimensions for this kmode {nrank1, ..., ndimk, ..., nrank3}
         std::vector<unsigned int> dimsin;    // dimensions in the input tensor
         std::vector<unsigned int> dimsout;   // dimensions in the output matrix
         std::vector<unsigned int> noffin;    // offsets for the input tensor  
         std::vector<unsigned int> noffout;   // offsets for the output matrix  

         std::vector<unsigned int> ranksloc;

         dimsout.push_back(dims[kmode]);
         for (unsigned int jmode = 0; jmode < ndim; ++jmode)
         {
            if (jmode == kmode)
            {
               dimsin.push_back(dims[kmode]);
            }
            else
            {
               dimsin.push_back(  ranks[jmode]);
               dimsout.push_back( ranks[jmode]);

               ranksloc.push_back(ranks[jmode]);
            }

         }
         std::reverse(dimsout.begin(), dimsout.end());  // transpose it (Fortran vs. C++ issue)
      

         // create vector with offsets
         unsigned int nlastin  = dimsin[ndim-1];
         unsigned int nlastout = dimsout[ndim-1];

         noffin.push_back(nlastin);
         noffout.push_back(nlastout);
         for (int idx = ndim-2; idx > 0; --idx)
         {
            unsigned int ncurin = dimsin[idx]*nlastin;
            noffin.push_back(ncurin);
            nlastin = ncurin;

            unsigned int ncurout = dimsout[idx]*nlastout;
            noffout.push_back(ncurout);
            nlastout = ncurout;
         }   
         std::reverse(noffin.begin(), noffin.end());
         std::reverse(noffout.begin(),noffout.end());
         noffin.push_back(1);  // only as helper
         noffout.push_back(1); // only as helper

         if (locdbg) 
         {
            Mout << "Dimensions: " << dimsin << std::endl;
            Mout << "Ranks: " << ranksloc << std::endl;
         }
 
         // a) Construct partially projected tensor 
         T** modemat = new T*[ndim];
         for (unsigned int jmode = 0; jmode<ndim; ++jmode)
         {
            if (jmode == kmode)
            {
               modemat[kmode] = new T[dims[kmode]*nrankold];
               memcpy(reinterpret_cast<void*>(modemat[kmode]),
                      reinterpret_cast<void*>(mode_matrices[kmode]),dims[kmode]*nrankold*sizeof(T));
            }
            else
            {
               modemat[jmode] = new T[ranks[jmode]*nrankold];
               memcpy(reinterpret_cast<void*>(modemat[jmode]),
                      reinterpret_cast<void*>(proj_modemat[jmode]),ranks[jmode]*nrankold*sizeof(T));
            }
         }

         std::vector<unsigned int> newdim(dimsin.begin(), dimsin.begin() + ndim);
         CanonicalTensor<T> intermed_cp(newdim, nrankold, modemat) ;
         SimpleTensor<T> intermed(intermed_cp);  

         // b) Reshape into a matrix

         // b.1 get second dimension
         In ndimranks = I_1;
         for (unsigned int jmode = 0; jmode < ndim; ++jmode) 
         {
            if (jmode != kmode) ndimranks = ndimranks * ranks[jmode];
         }

         // b.2 copy data of the tensor
         const unsigned int  nsize = dims[kmode]*ndimranks;

         T* tmp(new T[nsize]);
         memcpy(reinterpret_cast<void*>(tmp),
                reinterpret_cast<void*>(intermed.GetData()),nsize*sizeof(T));

         // b.3 do the matrix unfolding
         if (locdbg) Mout << "Dims for unfolding " <<  dims[kmode] << " " <<  ndimranks << std::endl;
         T* mat = new T[dims[kmode]*ndimranks];
         unsigned int iadrsrc = 0;
         unsigned int iadrdes = 0;

         /*
            Realization of arbitrary many nested loops via the basic counting principle to
            ensure an reshaping for any kind of tensor. This might effect a bit the
            performance, but other parts are much more rate limiting...
         */
         int i,j;

         std::vector<unsigned int> index(ndim);
         //std::vector<unsigned int> len = {dims[kmode],ranksloc[0],ranksloc[1]};   
         std::vector<unsigned int> len(dimsout.rbegin(), dimsout.rend());

         // init
         for (unsigned int idx = 0; idx < ndim; ++idx)
         {
            index[idx] = 0;
         }

         while(true)
         {
            LOGCALL("Tensor unfolding");

            for(i = 0 ; i < ndim; ++i)
            {

               //----------------------------------------------------------+
               // copy data
               //----------------------------------------------------------+

               // construct index vector
               std::stack<unsigned int> stidx;
               std::vector<unsigned int> ivecin;
               std::vector<unsigned int> ivecout;

               for (int ix = ndim-1; ix >= 0; --ix)
               {
                  if (ix > 0) stidx.push(index[ix]);
                  ivecout.push_back(index[ix]);
               }  

               for (unsigned int i = 0; i<ndim; i++)
               {
                  if (i == kmode) 
                  {
                     ivecin.push_back(index[0]);
                  }
                  else
                  {
                     unsigned int elem = stidx.top();
                     stidx.pop();
                     ivecin.push_back(elem);
                  }
               }
   
               // calculate address as dot product of the index and offset vector
               iadrsrc = std::inner_product(begin(noffin), end(noffin), begin(ivecin), 0);

               // also calculate the address in the output array
               iadrdes = std::inner_product(begin(noffout),end(noffout), begin(ivecout), 0);

               mat[iadrdes] = tmp[iadrsrc]; 
            }
            
            // increment indices
            for(j = ndim-1 ; j>=0 ; j--)
            {
               if(++index[j] < len[j])
               {
                  break;
               }
               else
               {
                  index[j]=0;
               }
            }
            if(j<0)
            {
               break;
            }
         }  
         delete[] tmp;  

         if (dbg_svd) 
         {
            T* prtMat = new T[dims[kmode]*ndimranks];
            memcpy(reinterpret_cast<void*>(prtMat),
                reinterpret_cast<void*>(mat),dims[kmode]*ndimranks*sizeof(T));

            NiceTensor<T> prtTensor ( new SimpleTensor<T>  (std::vector<unsigned int>{static_cast<unsigned int>(ndimranks),
                                                                                      static_cast<unsigned int>(dims[kmode])},prtMat));
            Mout << "Matrix for SVD:" << std::endl;
            Mout << prtTensor << std::endl;
         }

         // c) Caclulate truncated SVD of the matricization
         if (locdbg) Mout << " GESVD II " << dims[kmode] << " " << ndimranks << std::endl; 
         SVD_struct<T> svdk = GESVD(mat, static_cast<int>(dims[kmode]), static_cast<int>(ndimranks), 'A');

         if (locdbg) 
         {
            Mout << "Mode: " << kmode << std::endl;
            Mout << "Dims: " << svdk.Min() << " " << dims[kmode] << " " << ndimranks << std::endl;
            Mout << "Singular values: " << std::endl;
            for (unsigned int irank = 0; irank<svdk.Min(); ++irank)
            {
               Mout << irank << " " << svdk.s[irank] << std::endl;
            }
         }

         // get rank/truncate SVD
         In svd_currank = 0;
         for (unsigned int irank = 0; irank < svdk.Min(); ++irank)
         {
            if (svdk.s[irank] > thrcutsvd)
            { 
               svd_currank += 1;
            }
            sval[kmode][irank] = svdk.s[irank];
         }

         // d) Update the subspace for kmode
         size_t counter = 0;
         for (unsigned int irank = 0; irank < svd_currank; ++irank)
         {
            for (unsigned int idim = 0; idim < dims[kmode]; ++idim)
            {
               umat[kmode][irank*dims[kmode] + idim] = svdk.u[counter++];
            }
         }
         ranks[kmode] = std::max<In>(svd_currank,I_1);

         // clean up a bit
         delete[] mat;

      }
   }

   // Do the final projection with the converged Side matrices 
   for (unsigned int imode = 0; imode<ndim; imode++)
   {
      char nc = 'T'; char nt = 'N';
      T alpha = static_cast<T>(1.0); T beta  = static_cast<T>(0.0);
      int m = ranks[imode];  
      int n = nrankold;     
      int k = dims[imode];  

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha,
                                     umat[imode], &k,
                                     mode_matrices[imode], &k,
                                     &beta, proj_modemat[imode], &m );
   }

   // 4) Construct core tensor
   std::vector<unsigned int> dimcore;
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      dimcore.push_back(ranks[imode]);
   }
 
   // fill in mode matrices
   T** modemat = new T*[ndim];
   for (unsigned int jmode = 0; jmode < ndim; ++jmode)
   {
      modemat[jmode] = new T[ranks[jmode]*nrankold];
      memcpy(reinterpret_cast<void*>(modemat[jmode]),
             reinterpret_cast<void*>(proj_modemat[jmode]),ranks[jmode]*nrankold*sizeof(T));
   }

   CanonicalTensor<T> beta_cp(dimcore, nrankold, modemat);
   NiceTensor<T> beta( new CanonicalTensor<T>(beta_cp) );  

   // 5) Refit core tensor via CP-ALS algorithm to obtain (hopefully) a lower rank
   unsigned int nsumdim = std::accumulate(dimcore.begin(), dimcore.end(), 0);
   if (nsumdim > 5)
   {
      LOGCALL("CP-ALS Step");

      if (use_decomposer)
      {
         // use provided decompsoer
         Mout << " dimensions of core tensor " << dimcore << std::endl;
         beta = decomposer->Decompose(beta); 
      }
      else
      {
         // refit CP Tensor using "standard" algorithm
         beta = beta.ToCanonicalTensor(thrcp);
      }
   }

   CanonicalTensor<T>* canon=dynamic_cast<CanonicalTensor<T>*>(beta.GetTensor());
   T** matrices = canon->GetModeMatrices();
   unsigned int newrank = canon->GetRank();

   Mout << "New (reduced) rank " << newrank << std::endl;

   // 6) Set the final mode matrices  
   if (locdbg) Mout << "Set final mode matrices" << std::endl;
   T** final_modemat = new T*[ndim];
   for (unsigned int imode = 0; imode < ndim; imode++)
   {

      // allocate memory   
      final_modemat[imode] = new T[dims[imode]*newrank];

      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      //    b_i_k,r^{(k)} = sum_r_k U_{r_k,i_k} b_{r_k,r}^{(k)} 
      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      char nc = 'N'; char nt = 'N';
      T alpha = static_cast<T>(1.0); T beta  = static_cast<T>(0.0);
      int m = dims[imode];  
      int n = newrank;     
      int k = ranks[imode];  

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha,
                                     umat[imode], &m,
                                     matrices[imode], &k,
                                     &beta, final_modemat[imode], &m );
   }

   // 7) pack everything together and return
   NiceTensor<T> result (new CanonicalTensor<T>(dims, newrank, final_modemat));

   // clean up before leaving
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      delete[] proj_modemat[imode];
      delete[] umat[imode];
      delete[] sval[imode];
   }

   delete[] umat;
   delete[] sval;
   delete[] proj_modemat;

   // finally return
   return result; 
}
