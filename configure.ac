#
# Process this file with autoconf to produce a configure script.
#
# Written by Mikkel Bo Hansen <mbh@chem.au.dk>
#
# Last modified by   Mads Greisen Højlund
#                    <mgh@chem.au.dk>
#                    September 2021
#

AC_INIT([MidasCpp], [2019.04.0], [midascpp-support@chem.au.dk], [midascpp.tar.gz], [https://gitlab.com/midascpp/midascpp])
AC_CONFIG_SRCDIR(mains/midascpp.cc)
AC_CONFIG_HEADER(config.h)
AC_CONFIG_AUX_DIR(config)
AC_PREFIX_DEFAULT("/usr/local/midas")
AC_PROG_MAKE_SET
AC_CANONICAL_HOST

#set prefix equal to the default if not given
if test "x${prefix}" = "xNONE"; then
   prefix=${ac_default_prefix}
fi

INSTALL_PREFIX=${prefix}

AC_ARG_ENABLE([lto], AS_HELP_STRING([--enable-lto],
	[enable link-time optimization (currently only for GCC)]))

AC_ARG_ENABLE([mpi], AS_HELP_STRING([--enable-mpi=vendor],
	[enable parallel version using mpich or openmpi]))

# OpenMP. Default is disable
AC_ARG_ENABLE([openmp], 
              AS_HELP_STRING([--enable-openmp], [enable OpenMP threading; default: disabled]),
              ,
              [enable_openmp="no"]
              )

AC_ARG_ENABLE([tinker], AS_HELP_STRING([--enable-tinker=path],
	[enable the MidasCpp interface to Tinker]))

AC_ARG_ENABLE([dalton], AS_HELP_STRING([--enable-dalton=path],
	[enable the MidasCpp interface to Dalton]))

AC_ARG_ENABLE([cfour], AS_HELP_STRING([--enable-cfour=path],
	[enable the MidasCpp interface to CFOUR]))

AC_ARG_ENABLE([cfour_par], AS_HELP_STRING([--enable-cfour-par=path],
	[enable the MidasCpp interface to parallel version of CFOUR]))

AC_ARG_ENABLE([aces], AS_HELP_STRING([--enable-aces=path],
	[enable the MidasCpp interface to the AMB version of ACES II]))

AC_ARG_ENABLE([molpro], AS_HELP_STRING([--enable-molpro=path],
	[enable the MidasCpp interface to MOLPRO]))

AC_ARG_ENABLE([turbomole], AS_HELP_STRING([--enable-turbomole=path],
	[enable the MidasCpp interface to TURBOMOLE]))

AC_ARG_ENABLE([orca], AS_HELP_STRING([--enable-orca=path],
	[enable the MidasCpp interface to ORCA]))

AC_ARG_ENABLE([tana], AS_HELP_STRING([--enable-tana=path],
              [enable the MidasCpp interface to Tana. If <path> is empty, MidasCpp will download and build Tana.]))

AC_ARG_ENABLE([midas-memdebug], AS_HELP_STRING([--enable-midas-memdebug],
   [enable internal MidasCpp memory leaks and memory corruption checks]))

AC_ARG_ENABLE([scratch-dir], AS_HELP_STRING([--enable-scratch-dir=path],
	[place where MidasCpp may place temporary files [[/tmp]]]))

AC_ARG_ENABLE([build-dir], AS_HELP_STRING([--enable-build-dir=path],
              [place where MidasCpp should put object files, etc. (default: current directory)]),
              [],
              [enable_build_dir=$(pwd)]
              )
AC_SUBST([enable_build_dir])

AC_ARG_ENABLE([debug-build], 
              AS_HELP_STRING([--enable-debug-build],
                             [build MidasCpp with debug flags in addition to the release build]),
              [if test "x$enable_debug_build" == "xyes"; then debug_build_comment=""; else debug_build_comment="#"; fi],
              [debug_build_comment="#"])
AC_SUBST([debug_build_comment])

AC_ARG_ENABLE([make-dir],
              AS_HELP_STRING([--enable-make-dir=path],
                             [place for 'make' to put some files needed internally (default: $(pwd)/.make)]
                             ),
              [],
              [enable_make_dir=$(pwd)/.make]
              )
AC_SUBST([enable_make_dir])

AC_ARG_ENABLE([reuse-file-search],
              AS_HELP_STRING([--enable-reuse-file-search],
                             [reuse last source file search when running 'make' (see Makefile.config); default: disabled]
                             ),
              [if test "x$enable_reuse_file_search" == "xyes"; then enable_reuse_file_search="true"; fi],
              [enable_reuse_file_search="false"]
              )
AC_SUBST([enable_reuse_file_search])

AC_ARG_ENABLE([progress-display],
              AS_HELP_STRING([--enable-progress-display],
                             [display compilation progress when running 'make' (see Makefile.config); default: enabled]
                             ),
              [if test "x$enable_progress_display" == "xno"; then enable_progress_display="false"; fi],
              [enable_progress_display="true"]
              )
AC_SUBST([enable_progress_display])

AC_ARG_ENABLE([color-support],
              AS_HELP_STRING([--enable-color-support],
                             [display compilation progress with color formatted output (see Makefile.config); default: enabled]
                             ),
              [if test "x$enable_color_support" == "xno"; then enable_color_support="false"; fi],
              [enable_color_support="true"]
              )
# We don't AC_SUBST the color-support variable just yet, since it needs
# checking that the terminal actually supports it.

AC_ARG_ENABLE([verbose-make],
              AS_HELP_STRING([--enable-verbose-make],
                             [have 'make' print detailed compilation and linking commands; default: disabled]
                             ),
              [if test "x$enable_verbose_make" == "xyes"; then enable_verbose_make="true"; fi],
              [enable_verbose_make="false"]
              )
AC_SUBST([enable_verbose_make])

# Options to disable libraries even though they are found
AC_ARG_ENABLE([lapack],
           AS_HELP_STRING([--enable-lapack],
                          [link against LAPACK library if found; default: enabled]),
                          , 
                          [enable_lapack=yes]
           )

AC_ARG_WITH([lapack-url],
            AS_HELP_STRING([--with-lapack-url],
                           [Set lapack url.]),
                           , 
                           []
            )

AC_ARG_ENABLE([gsl],
           AS_HELP_STRING([--enable-gsl],
                          [link against GSL library if found; default: enabled]),
                          , 
                          [enable_gsl=yes]
           )

AC_ARG_WITH([gsl-url],
            AS_HELP_STRING([--with-gsl-url],
                           [Set GSL url.]),
                           , 
                           []
            )

AC_ARG_ENABLE([fftw],
           AS_HELP_STRING([--enable-fftw],
                          [link against FFTW library if found; default: enabled]),
                          , 
                          [enable_fftw=yes]
           )

AC_ARG_WITH([fftw-url],
            AS_HELP_STRING([--with-fftw-url],
                           [Set FFTW url.]),
                           , 
                           []
            )

AC_ARG_ENABLE([boost],
            AS_HELP_STRING([--enable-boost],
                           [link against boost library if found; default: enabled]),
                           , 
                           [enable_boost=yes]
            )

AC_ARG_WITH([boost-url],
            AS_HELP_STRING([--with-boost-url],
                           [Set boost url.]),
                           , 
                           []
            )

AC_ARG_ENABLE([ccache],
            AS_HELP_STRING([--disable-ccache],
                           [Disable compiling with 'ccache' even if 'ccache' is found in PATH.]),
                           [disable_ccache=yes],
                           []
            )

AC_ARG_ENABLE([strip],
            AS_HELP_STRING([--disable-strip],
                           [Disable stripping of executables with 'strip' even if 'strip' is found in PATH.]),
                           [disable_strip=yes],
                           []
            )

AC_ARG_WITH([cutee-url],
            AS_HELP_STRING([--with-cutee-url],
                           [Set Cutee url.]),
                           , 
                           []
            )

AC_ARG_WITH([stackd-url],
            AS_HELP_STRING([--with-stackd-url],
                           [Set Stackd url.]),
                           , 
                           []
            )

# Check for programs.

CHECK_GNU_MAKE
if test "$ifGNUmake" = "#"; then
	AC_MSG_ERROR([No working version of GNU make found!])
fi

AC_PROG_RANLIB
AC_PROG_INSTALL
AC_CHECK_PROGS(LD,ld,false)
AC_CHECK_PROGS(AR,ar,false)
AC_CHECK_PROGS(STRIP,strip,true)
AC_CHECK_PROGS(ETAGS,etags,true)
AC_CHECK_PROGS(CTAGS,ctags,true)
STRIP=
if test "x$disable_strip" != "xyes"; then
   AC_CHECK_TOOL([STRIP],[strip])
fi
if test "x$disable_ccache" != "xyes"; then
   AC_CHECK_PROG(CCACHE,ccache,ccache)
fi
#AC_PROG_LN_S
#AC_PROG_AWK
#AC_PROG_YACC
#AC_PROG_LEX

AC_SUBST([INSTALL])

AC_SUBST([host_os])

#
# Check build type.
#     $1 : String to check against. E.g. "darwin" or "linux".
#  
# Usage:
#    if test -n $(is_build_type linux); then
#       ... code if linux ...
#    fi
#
function is_build_type() {
   is_build_type_result=`echo ${build_type} | grep "${1}"`
   echo $is_build_type_result
}

# ACX_ macros are defined in aclocal.m4
# Setup compilers for different architechtures

case $host in 
	*i*86*-linux*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="i386-linux"
      ext_dynlib="so"
      LDD="ldd"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
	*i*86*-apple-darwin*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="i386-darwin"
      ext_dynlib="dylib"
      LDD="otool -L"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
	*x86_64*-apple-darwin*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="x86_64-darwin"
      ext_dynlib="dylib"
      LDD="otool -L"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
   *arm*-apple-darwin*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="arm64-darwin"
      ext_dynlib="dylib"
      LDD="otool -L"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
	*ia64*-linux*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="linux_ia64"
      ext_dynlib="so"
      LDD="ldd"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
	*x86_64*-linux*)
	   try_cc="gcc clang icc pgcc"
	   try_fc="gfortran ifort pgf90"
	   try_cxx="g++ clang++ icpc pgCC"
      build_type="x86_64-linux"
      ext_dynlib="so"
      LDD="ldd"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
	*ibm*-aix*)
	   try_cc="cc gcc"
	   try_fc="xlf xlf90 f90 f77 g77"
      build_type="ibm_aix"
      LDD="ldd"
      AC_MSG_WARN([Linking errors related to TOC overflow has been encountered,])
      AC_MSG_WARN([adding "-Wl,-bbigtoc" to linking flags. Remove in])
      AC_MSG_WARN([Makefile.config if not needed.])
      LDFLAGS=$LDFLAGS" -Wl,-bbigtoc "
      CPPFLAGS="-DSYS_AIX "$CPPFLAGS
	   ;;
	*) 	
	   AC_MSG_WARN([Unknown architecture! This might not work...])
	   try_cc="gcc icc xlc_r pgcc"
	   try_cxx="g++ icpc xlC_r pgCC"
	   try_fc="gfortran ifort xlf90 pgf90"
      build_type="unknown"
      ext_dynlib="so"
      LDD="ldd"
      CPPFLAGS="-DSYS_LINUX "$CPPFLAGS
	   ;;
esac

# 
# Make some chouices based on OS type
#
if test -n "$(is_build_type darwin)"; then
   # Darwin / MacOS
   STAT="stat"
   STAT_MODE="$STAT -f %p"
   DYNLIB_NAME="install_name"
else
   # Linux or unknown (we assume Linux)
   STAT="stat"
   STAT_MODE="$STAT -c %a"
   DYNLIB_NAME="soname"
fi

#mbh: if LDFLAGS: save flags and clear! Some compilers (ibm)
#     cannot find out what to do. For same reason: start with
#     Fortran below
# Save flags
saved_ld_flags=${LDFLAGS}
saved_f_flags=${FFLAGS}
saved_fc_flags=${FCFLAGS}
saved_c_flags=${CFLAGS}
saved_cc_flags=${CCFLAGS}
saved_cxx_flags=${CXXFLAGS}
AC_PROG_FC([$try_fc])
AC_PROG_CXX([$try_cxx])
AC_PROG_CC([$try_cc])
# Reset flags as these might be set by compiler checking script (if they are empty on entry I think)
FFLAGS=${saved_f_flags}
FCFLAGS=${saved_fc_flags}
CFLAGS=${saved_c_flags}
CCFLAGS=${saved_cc_flags}
CXXFLAGS=${saved_cxx_flags}

# Enable Link-time optimization (LTO)
if test "x$enable_lto" != "x"; then
   # GCC flags
   CFLAGS="${CFLAGS} -flto"
   FCFLAGS="${FCFLAGS} -flto"
   CXXFLAGS="${CXXFLAGS} -flto"
fi

# Enable MPI
if test "x$enable_mpi" != "x"; then 
   AC_MSG_NOTICE([Checking if MPI is available])
   AC_LANG_PUSH(C++)
   ACX_MPI([],AC_MSG_ERROR([MPI not found for C++: $enable_mpi]))
   AC_LANG_POP
   AC_LANG_PUSH(C)
   ACX_MPI([],AC_MSG_ERROR([MPI not found for C: $enable_mpi]))
   AC_LANG_POP
   AC_LANG_PUSH(Fortran)
   ACX_MPI([],AC_MSG_ERROR([MPI not found for Fortran: $enable_mpi]))
   AC_LANG_POP
   # get back-end compilers
   savedFC=$FC
   savedCC=$CC
   savedCXX=$CXX
	FC=`$MPIFC -showme -v | awk '{print $1}'`
	CC=`$MPICC -showme -v | awk '{print $1}'`
	CXX=`$MPICXX -showme -v | awk '{print $1}'`
   if test "$savedFC" != "$FC"; then
      AC_MSG_WARN([Fortran compiler has been switched to the MPI back-end one. ($savedFC -> $FC)])
   fi
   if test "$savedCC" != "$CC"; then
      AC_MSG_WARN([C compiler has been switched to the MPI back-end one. ($savedCC -> $CC)])
   fi
   if test "$savedCXX" != "$CXX"; then
      AC_MSG_WARN([C++ compiler has been switched to the MPI back-end one. ($savedCXX -> $CXX)])
   fi
   CPPFLAGS=$CPPFLAGS" -DVAR_MPI"
   if test "$FC == gfortran"; then
      LIBS=$LIBS" -lgfortran"
   fi
fi

# Setup complation flags based on compiler
AC_MSG_NOTICE(I have type = $build_type)
ACX_SUBST_BUILD_FLAGS([$build_type])
AC_SUBST([build_type])

#if MPI, set compilers to MPI ones
MPIRUN="0"
if test "x$enable_mpi" != "x"; then
   FC=$MPIFC
   CC=$MPICC
   CXX=$MPICXX
   MPIRUN="1"
fi

AC_LANG_PUSH([C++])

AC_CACHE_SAVE

###############################################################################
# External libraries
###############################################################################
EXTERNAL=""


######  LAPACK - Check local installation, otherwise build shipped version ########
lapack_version=3.6.0
have_lapack=no
# Search for local library
# We are not using shipped, check enable_lapack
if test "x$enable_lapack" != "xno"; then
   if test "x$enable_lapack" == "xyes"; then
      have_lapack="yes"
      ACX_BLAS(, have_lapack="no")
      ACX_LAPACK(, have_lapack="no")
      #AX_LAPACK_VERSION

      if test "x$have_lapack" == "xno"; then
         AC_MSG_NOTICE([LAPACK not found on the system!])
      else
         AC_MSG_WARN([System LAPACK is used. This can produce incorrect
                      results! Versions older than 3.6.0 are known to be
                      unreliable - check your version!  Reconfigure with
                      --enable-lapack=3.6.0 (or newer) if necessary.])
         # Append found BLAS/LAPACK libs to LIBS.
         # (We omit $FCLIBS, which has never been used in this configure
         # script. Maybe that's a mistake.. hmm... -MBH, Mar 2020.)
         # See
         #     config/acx_blas.m4
         #     config/acx_lapack.m4
         LIBS=$LAPACK_LIBS" "$BLAS_LIBS" "$LIBS
      fi
   else
      lapack_version=$enable_lapack
   fi
   
   if test "x$have_lapack" == "xno"; then
      # No lapack was found, so we add it to list of libraries to build
      EXTERNAL=$EXTERNAL" lapack-"$lapack_version
      LIBS=$LIBS" -lblas -llapack"
      AC_MSG_NOTICE(['make' will download and build lapack-$lapack_version])
   fi
fi

# Set linking flags for downloaded lapack. (No include flags, since it's Fortran.)
if test "x$enable_lapack" != "xno"; then
   if test "x$have_lapack" == "xno"; then
      LDFLAGS=$LDFLAGS" -L"$PWD"/extlibs/lapack-"$lapack_version"-build/midascpp_install/lib"
      LD_LIBRARY_PATH_EXE=$LD_LIBRARY_PATH_EXE":"$INSTALL_PREFIX"/extlibs/lapack/lib"
   fi

   case "x$FC" in
      x*ifort* )
         LIBS=$LIBS" -lifcore"
         ;;
      x*gfortran* )
         LIBS=$LIBS" -lgfortran"
         ;;
   esac
fi

######  FFTW3 - Check local installation ########
fftw_version=3.3.4
# Search for local 
have_fftw="no"
if test "x$enable_fftw" != "xno"; then
   if test "x$enable_fftw" == "xyes"; then
      AC_SEARCH_LIBS(fftw_execute, fftw3, have_fftw="yes",  have_fftw="no")
      AC_CHECK_HEADER(fftw3.h, have_fftw="yes",  have_fftw="no",   )
      if test "x$have_fftw" == "xno"; then
         AC_MSG_NOTICE([FFTW not found on the system!])
      fi
   else
      fftw_version=$enable_fftw
   fi
   
   if test "x$have_fftw" == "xno"; then
      # No fftw was found, so we add it to list of libraries to build
      EXTERNAL=$EXTERNAL" fftw-"$fftw_version
      AC_MSG_NOTICE(['make' will download and build fftw-$fftw_version])
   fi
fi

# Set linking flags
if test "x$enable_fftw" != "xno"; then
   CPPFLAGS=$CPPFLAGS" -DENABLE_FFTW"

   if test "x$have_fftw" == "xno"; then
      LDFLAGS=$LDFLAGS" -L"$PWD"/extlibs/fftw-"$fftw_version"/midascpp_install/lib"
      LIBS=$LIBS" -lfftw3"
      INCLUDE_DIR=$INCLUDE_DIR" -I"$PWD"/extlibs/fftw-"$fftw_version"/midascpp_install/include"
      LD_LIBRARY_PATH_EXE=$LD_LIBRARY_PATH_EXE":"$INSTALL_PREFIX"/extlibs/fftw/lib"
   fi
fi

######  GSL - Check local installation ########
gsl_version="1.16"
# Search for local
have_gsl="no"
if test "x$enable_gsl" != "xno"; then
   if test "x$enable_gsl" == "xyes"; then
      AC_SEARCH_LIBS(cos,            m,        have_m="yes",        have_m="no")
      AC_SEARCH_LIBS(cblas_dgemm,    gslcblas, have_gslcblas="yes", have_gslcblas="no")
      AC_SEARCH_LIBS(gsl_blas_dgemm, gsl,      have_gsl="yes",      have_gsl="no")
      if test "x$have_gslcblas" == "xno"; then
         AC_MSG_NOTICE([GSL CBLAS not found on the system!])
      fi
      if test "x$have_gsl" == "xno"; then
         AC_MSG_NOTICE([GSL not found on the system!])
      fi
   else
      gsl_version=$enable_gsl
   fi

   if test "x$have_gsl" == "xno"; then
      # No gsl was found, so we add it to list of libraries to build
      EXTERNAL=$EXTERNAL" gsl-"$gsl_version
      AC_MSG_NOTICE(['make' will download and build gsl-$gsl_version])
   fi
fi

# Set linking flags
if test "x$enable_gsl" != "xno"; then
   CPPFLAGS=$CPPFLAGS" -DENABLE_GSL"

   if test "x$have_gsl" == "xno"; then
      LDFLAGS=$LDFLAGS" -L"$PWD"/extlibs/gsl-"$gsl_version"/midascpp_install/lib"
      LIBS=$LIBS" -lgsl -lgslcblas"
      INCLUDE_DIR=$INCLUDE_DIR" -I"$PWD"/extlibs/gsl-"$gsl_version"/midascpp_install/include"
      LD_LIBRARY_PATH_EXE=$LD_LIBRARY_PATH_EXE":"$INSTALL_PREFIX"/extlibs/gsl/lib"
   fi
fi

######  boost - Check local installation ########
# Version 1.53 includes the boost/odeint library
boost_version=1.68.0
#Search local
have_boost="no"
if test "x$enable_boost" != "xno"; then
   if test "x$enable_boost" == "xyes"; then
      AX_BOOST_BASE([$boost_version], have_boost="yes", have_boost="no")
      if test "x$have_boost" == "xno"; then
         AC_MSG_NOTICE([boost library not found on the system!])
      fi
   else
      boost_version=$enable_boost
   fi

   if test "x$have_boost" == "xno"; then
      EXTERNAL=$EXTERNAL" boost-"$boost_version
      AC_MSG_NOTICE(['make' will download and build boost-$boost_version])
   fi
fi

if test "x$enable_boost" != "xno"; then
   CPPFLAGS=$CPPFLAGS" -DENABLE_BOOST"
   
   if test "x$have_boost" == "xno"; then
      # The boost directory has format boost_x_y_z instead of boost-x.y.z; deal
      # with that.
      boost_version_underscores=$(echo $boost_version | sed 's/\./_/g')
      INCLUDE_DIR=$INCLUDE_DIR" -I"$PWD"/extlibs/boost_"${boost_version_underscores}
   fi
fi

######  cutee - Check local installation ########
# check version of cmake
cmake_version_req=3.9
AC_CHECK_PROG(cmake_found, cmake, yes, no)
if test "x$cmake_found" == "xyes"; then
   cmake_version=`cmake --version | head -n 1 | cut -d ' ' -f 3`
   AX_COMPARE_VERSION([$cmake_version], [ge], [$cmake_version_req], [cmake_version_found="yes"], [cmake_version_found="no"])
else
   AC_MSG_ERROR([Cmake not found on system. (Needs version $cmake_version_req or newer.)], [1])
fi

if test "x$cmake_version_found" == "xno"; then
   AC_MSG_ERROR([Cmake version wrong/too old. (Needs version $cmake_version_req or newer.)], [1])
fi

cutee_version=2.0.5
#Search local
have_cutee="no"
if test "x$have_cutee" == "xno"; then
   AC_MSG_NOTICE([cutee library not found on the system!])
   EXTERNAL=$EXTERNAL" cutee-"$cutee_version
   AC_MSG_NOTICE(['make' will download and build cutee-$cutee_version])
   
   LDFLAGS=$LDFLAGS" -L"$PWD"/extlibs/cutee-"$cutee_version"-build/midascpp_install/lib"
   INCLUDE_DIR=$INCLUDE_DIR" -I"$PWD"/extlibs/cutee-"$cutee_version"-build/midascpp_install/include"
   LD_LIBRARY_PATH_EXE=$LD_LIBRARY_PATH_EXE":"$INSTALL_PREFIX"/extlibs/cutee/lib"
   
   LIBS=$LIBS" -lcutee"
fi

stackd_version=1.0.0
#Search local
have_stackd="no"
if test "x$have_stackd" == "xno"; then
   AC_MSG_NOTICE([stackd library not found on the system!])
   EXTERNAL=$EXTERNAL" stackd-"$stackd_version
   AC_MSG_NOTICE(['make' will download and build stackd-$stackd_version])
   
   LDFLAGS=$LDFLAGS" -L"$PWD"/extlibs/stackd-"$stackd_version"-build/midascpp_install/lib"
   INCLUDE_DIR=$INCLUDE_DIR" -I"$PWD"/extlibs/stackd-"$stackd_version"-build/midascpp_install/include"
   LD_LIBRARY_PATH_EXE=$LD_LIBRARY_PATH_EXE":"$INSTALL_PREFIX"/extlibs/stackd/lib"
   
   LIBS=$LIBS" -lstackd"
fi

tana_version=10.3
#Search local
have_tana="no"
if test "x$enable_tana" = "xyes"; then
   if test "x$have_tana" == "xno"; then
      AC_MSG_NOTICE([The Tana (ElVibRot) program is not found on the system!])
      EXTERNAL=$EXTERNAL" tana-"$tana_version
      AC_MSG_NOTICE(['make' will download and build tana-$tana_version])
      
      PATH_EXE=$PATH_EXE":"$INSTALL_PREFIX"/extlibs/tana"
   fi
fi

####### some general setup/clean-up w.r.t. external libraries ########
AC_LANG_POP([C++])

# Checks for header files.
AC_HEADER_STDC

AC_CHECK_FUNCS([gethostname])

# Updating Makefile.config appropriately according to which shipped libraries
# are to be included.
if test "x$USE_SHIPPED_LAPACK" = "x0"; then
   FCLIBS=""
fi

MAKEFILE_CONFIG_SHIPPED_LAPACK=""
MAKEFILE_CONFIG_SHIPPED_LAPACK_BLAS=""

if test "x$USE_SHIPPED_LAPACK" = "x1"; then
   MAKEFILE_CONFIG_SHIPPED_LAPACK=$LIB_SHIPPED_LAPACK
   MAKEFILE_CONFIG_SHIPPED_LAPACK_BLAS=$LIB_SHIPPED_LAPACK_BLAS
fi

AC_SUBST([EXTERNAL])
AC_SUBST([USE_SHIPPED_LAPACK])

AC_SUBST([MAKEFILE_CONFIG_SHIPPED_LAPACK])
AC_SUBST([MAKEFILE_CONFIG_SHIPPED_LAPACK_BLAS])

###############################################################################
# OpenMP
###############################################################################
if test "x$enable_openmp" == "xyes"; then
   if test "x$try_cxx" == "xicpc"; then
      CPPFLAGS=$CPPFLAGS" -qopenmp"
      LDFLAGS=$LDFLAGS" -qopenmp"
   else
      CPPFLAGS=$CPPFLAGS" -fopenmp"
      LDFLAGS=$LDFLAGS" -fopenmp"
   fi
fi

###############################################################################
# Check for 'wget' or 'curl'
###############################################################################
# 
# If needed will set DOWNLOADCMD variable
#
AC_ARG_VAR(DOWNLOADCMD, [Command for downloading external packages ('wget' or 'curl').])

if test "x$EXTERNAL" != "x"; then
   if test "x$DOWNLOADCMD" == "x"; then
      AC_CHECK_PROGS(DOWNLOADCMD, wget curl, no)
   fi
      
   if test "x$DOWNLOADCMD" == "xwget"; then
      DOWNLOADCMD="wget --progress=dot -O \$(2) \$(1)"
   elif test "x$DOWNLOADCMD" == "xcurl"; then
      DOWNLOADCMD="curl -L --fail --output \$(2) \$(1)"
   else
      AC_MSG_FAILURE([Need to download packages, but neither 'wget' or 'curl' is available.])
   fi
else
   AC_MSG_NOTICE([No external packages for download. Will not check for 'wget' or 'curl'.])
fi

AC_SUBST([DONWLOADCMD])

###############################################################################
# GCCTOOLCHAIN
###############################################################################
# For passing --gcc-toolchain="..." correctly to libraries using `libtool`
# (e.g. GSL), we need to deal with it separately.
# Clang needs the --gcc-toolchain on some systems/builds.
# Just leave it empty if not needed.
AC_ARG_VAR(GCCTOOLCHAIN, [Set to '--gcc-toolchain=/path/to/gcc', if needed by clang. (But also set it in CFLACS, CXXFLAGS, and/or LDFLAGS.)])
AC_SUBST([GCCTOOLCHAIN])

###############################################################################
# Check for 'tput' color support.
###############################################################################
if test "x$enable_color_support" == "xtrue"
then
   AC_MSG_NOTICE([Checking for 'tput' color support])
   # Check in following order:
   # - check 'colors' option works (prints num colors)
   # - check 'sgr0' (normal text works), so we can make text normal again
   # Then check for 'bold', 'setaf 2' or 'setf 2' (green text).
   # Remember to call 'sgr0' (normal in the end), so terminal goes back to
   # normal.
   #if tput colors && tput sgr0 && tput bold && tput setaf 2 && tput sgr0
   if tput colors && tput sgr0
   then
      if tput bold && tput sgr0
      then
         AC_MSG_NOTICE(['tput bold' available for color support])
         enable_color_support=true
         color_format_end="tput sgr0"
         color_format_beg="tput bold"
         if tput setaf 2 && tput sgr0
         then
            AC_MSG_NOTICE(['tput setaf 2' available for color support])
            color_format_beg=${color_format_beg}" && tput setaf 2"
         elif tput setf 2 && tput sgr0
         then
            AC_MSG_NOTICE(['tput setf 2' available for color support])
            color_format_beg=${color_format_beg}" && tput setf 2"
         else
            AC_MSG_WARN(['tput' detected, but neither 'tput setaf' nor 'tput setf' works])
         fi
      else
         AC_MSG_WARN(['tput' detected, but 'tput bold' does not work; disabling color support])
         enable_color_support=false
      fi
   else
      enable_color_support=false
      AC_MSG_NOTICE(['tput' not available for color support])
   fi
fi
AC_SUBST([enable_color_support])
AC_SUBST([color_format_beg])
AC_SUBST([color_format_end])


###############################################################################
# ES program variable
###############################################################################
n_miss=0

# check dalton program
AC_PATH_PROG(enable_dalton,dalton.x,)
if test "x$enable_dalton" = "x"; then 
   AC_MSG_WARN([Dalton NOT found on system!])
   eval progs_missing$n_miss="DALTON"
   let n_miss++
fi
#and dalton basis
dalton_basis=`echo $enable_dalton | sed -e 's/bin\/dalton.x/basis/g'`
dalton_basis_check=$dalton_basis"/cc-pVDZ"
AC_CHECK_FILE($dalton_basis_check)
if test "x$dalton_basis_check" = "x"; then
   AC_MSG_WARN([Dalton basis library NOT found on system!])
fi
AC_SUBST([enable_dalton])
AC_SUBST([dalton_basis])

# check cfour program
AC_PATH_PROG(enable_cfour,xcfour,)
if test "x$enable_cfour" = "x"; then 
   AC_MSG_WARN([CFOUR NOT found on system!])
   eval progs_missing$n_miss="CFOUR"
   let n_miss++
fi
enable_cfour=`echo $enable_cfour | sed -e 's/\/xcfour//g'`
#and cfour basis
enable_cfour=`echo $enable_cfour | sed -e 's/\/xcfour//g'`
cfour_basis=`echo $enable_cfour | sed -e 's/bin*/basis\/GENBAS/g'`
AC_CHECK_FILE($cfour_basis)
if test "x$cfour_basis" = "x"; then
   AC_MSG_WARN([CFOUR basis library NOT found on system!])
fi
AC_SUBST([enable_cfour])
AC_SUBST([cfour_basis])

# check cfour program parallel version
AC_PATH_PROG(enable_cfour_par,xgetrankinfo,)
if test "x$enable_cfour_par" = "x"; then 
   AC_MSG_WARN([CFOUR parallel version NOT found on system!])
   eval progs_missing$n_miss="CFOUR-PARALLEL"
   let n_miss++
fi
#and cfour parallel basis
cfour_basis_par=`echo $enable_cfour_par | sed -e 's/bin*/basis\/GENBAS/g'`
AC_CHECK_FILE($cfour_basis_par)
if test "x$cfour_basis_par" = "x"; then
   AC_MSG_WARN([CFOUR parallel basis library NOT found on system!])
fi
enable_cfour_par=`echo $enable_cfour_par | sed -e 's/\/xgetrankinfo//g'`
AC_SUBST([enable_cfour_par])
AC_SUBST([cfour_basis_par])

# check aces program
AC_PATH_PROG(enable_aces,xaces,)
if test "x$enable_aces" = "x"; then 
   AC_MSG_WARN([ACES NOT found on system!])
   eval progs_missing$n_miss="\"ACES II (amb)\""
   let n_miss++
fi
#and aces basis
aces_basis=`echo $enable_aces | sed -e 's/bin*/basis\/GENBAS/g'`
AC_CHECK_FILE($aces_basis)
if test "x$aces_basis" = "x"; then
   AC_MSG_WARN([ACES basis library NOT found on system!])
fi
enable_aces=`echo $enable_aces | sed -e 's/\/xaces//g'`
AC_SUBST([enable_aces])
AC_SUBST([aces_basis])

#check for molpro program
AC_SUBST([enable_molpro])

#check for turbomole program
AC_SUBST([enable_turbomole])

#check for orca program
AC_SUBST([enable_orca])

if test "x$enable_tinker" != "x"; then 
   AC_MSG_NOTICE([Checking if Tinker directory exists])
   # tinker_path is set by ACX_TINKER (see config/acx_tinker.m4)
   ACX_TINKER([],AC_MSG_ERROR([I need to be able to recompile Tinker!]))
   CPPFLAGS=$CPPFLAGS" -DVAR_HAS_TINKER"
   LDFLAGS=$LDFLAGS" -L"$tinker_path"/source/ -ltinker"
   case "x$FC" in
      x*ifort* )
         if test "x$ifcore_set" != "xtrue"; then
            LIBS=$LIBS" -lifcore"
            ifcore_set="true"
         fi
         ;;
      x*gfortran* )
         LIBS=$LIBS" -lgfortran"
         ;;
   esac
else
   tinker_path=""
   enable_tinker=0
fi
AC_SUBST([enable_tinker])
AC_SUBST([tinker_path])

# scratch check
if test "x$enable_scratch_dir" = "x"; then
   AC_MSG_WARN([place for temporary files not found, using /tmp])
   enable_scratch_dir="/tmp"
fi

AC_SUBST([enable_scratch_dir])
AC_SUBST([HOME])

AC_CACHE_SAVE

# the CPPFLAGS
INSTALL_BINDIR=${prefix}"/bin"
INSTALL_EXECDIR=${prefix}"/libexec"
INSTALL_LIBDIR=${prefix}"/extlibs"
INSTALL_INCLUDEDIR=${prefix}"/include"
INSTALL_SHAREDIR=${prefix}"/share"

#if midas-memdebug is enabled, define MIDAS_MEM_DEBUG 
AS_IF([test "x$enable_midas_memdebug" = "xyes"], [
  CPPFLAGS=$CPPFLAGS" -DMIDAS_MEM_DEBUG"
])

#mbh: add the saved linking flags
LDFLAGS=$saved_ld_flags$LDFLAGS

#check for libpthread
ACX_PTHREAD([])
LIBS=$LIBS$PTHREAD_LIBS
AC_SUBST([LDFLAGS])

INCLUDE_DIR=$INCLUDE_DIR" -I"`pwd`
SOURCE_DIR=$(pwd)
AC_SUBST([SOURCE_DIR])
AC_SUBST([INCLUDE_DIR])
AC_SUBST([CPPFLAGS])
AC_SUBST([INSTALL_PREFIX])
AC_SUBST([INSTALL_BINDIR])
AC_SUBST([INSTALL_EXECDIR])
AC_SUBST([INSTALL_LIBDIR])
AC_SUBST([INSTALL_INCLUDEDIR])
AC_SUBST([INSTALL_SHAREDIR])
AC_SUBST([MPIRUN])
AC_SUBST([BOOST_LDLIBS])
AC_SUBST([BOOST_CPPFLAGS])
AC_SUBST([LD_LIBRARY_PATH_EXE])
AC_SUBST([PATH_EXE])
AC_SUBST([LDD])
AC_SUBST([CCACHE])
AC_SUBST([ext_dynlib])
AC_SUBST([with_lapack_url])
AC_SUBST([with_gsl_url])
AC_SUBST([with_fftw_url])
AC_SUBST([with_boost_url])
AC_SUBST([with_cutee_url])
AC_SUBST([with_stackd_url])
AC_SUBST([STAT])
AC_SUBST([STAT_MODE])
AC_SUBST([STRIP])
AC_SUBST([DYNLIB_NAME])

AC_CONFIG_FILES([Makefile.config scripts/dalton_for_midas.sh
                 scripts/midas
                 test_suite/ES_PROGRAMS
                 bin/midascpp.config])
AC_OUTPUT

if test $n_miss -gt 0; then
   AC_MSG_NOTICE([Some programs were not found in system.])
   AC_MSG_NOTICE([To include them, do one of three things:])
   echo
   echo "   1: Update your PATH system variable and rerun configure."
   echo "   2: Rerun configure with e.g.: --enable-dalton=@<:@path@:>@."
   echo "      there are similar ones for other programs (./configure --help)."
   echo "   3: Manually edit the shell files, e.g. dalton_for_midas.sh"
   echo
   it=0
   while test $it -lt $n_miss; do
      echo -en "   \c"
      eval echo "\$progs_missing$it not found on system."
      let it++
   done
fi

echo
echo "configure is done, now build the program by typing:"
echo "      > make complete"
echo ""
echo "As an alternative, to compile more than 1 job at a time type:"
echo "      > make -j@<:@NUM_JOBS@:>@ complete"
echo
