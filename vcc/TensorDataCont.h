/**
************************************************************************
* 
* @file                TensorDataCont.h
*
* Created:             16-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Holds excitation vector amplitudes in a tensor format
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORDATACONT_H_INCLUDED
#define TENSORDATACONT_H_INCLUDED

// Include declaration
#include "vcc/TensorDataCont_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "vcc/TensorDataCont_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

// Aliases
using TensorDataCont = GeneralTensorDataCont<Nb>;
using ComplexTensorDataCont = GeneralTensorDataCont<std::complex<Nb>>;

#endif /* TENSORDATACONT_H_INCLUDED */
