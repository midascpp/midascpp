/**
************************************************************************
* 
* @file                TensorDataCont_Impl.h
*
* Created:             19-10-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Holds excitation vector amplitudes in a tensor format
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORDATACONT_IMPL_H_INCLUDED
#define TENSORDATACONT_IMPL_H_INCLUDED

#include "TensorDataCont_Decl.h"

#include <functional>

#include "util/Isums.h" // for IntBinCoef
#include "util/Io.h"
#include "tensor/NiceTensor_vector.h"
#include "tensor/TensorDecomposer.h"
#include "tensor/Scalar.h"
#include "vcc/VccStateSpace.h"
#include "input/VccCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/VccStateModeCombi.h"
#include "util/RandomNumberGenerator.h"

#include "mmv/mmv_omp_pragmas.h"

/** 
 * Create simple tensor and add to tensor xvec
 *
 * @param num_modals      Size of tensor
 * @param ptr             Pointer with tensor data (the simple tensor will take ownership)
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::CreateSimple
   (  const std::vector<unsigned>& num_modals
   ,  std::unique_ptr<T[]>& ptr
   ) 
{ 
   // load ptr into SimpleTensor format (SimpleTensor now takes ownership of ptr, and NiceTensor takes ownership of SimpleTensor)
   this->emplace_back(static_cast<BaseTensor<T>*>(new SimpleTensor<T>(num_modals, ptr.release()))); 
}

/** 
 * Create random simple tensor and add to tensor xvec
 *
 * @param num_modals      Size of tensor
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::CreateRandomSimple
   (  const std::vector<unsigned>& num_modals
   ) 
{ 
   this->emplace_back(make_nice_random_simple<T>(num_modals)); 
}

/**
 * Construct excitation vector data in simple tensor form.
 *
 * @param aData      The data to be converted to simple tensor form.
 * @param aExciRange The range of ModeCombi%s defining the TensorDataCont.
 * @param aNmodals   The number of modals per mode for each mode.
 * @param aRef
 *    Remove reference when constructing TensorDataCont from DataCont which
 *    does not include reference. (true: include reference, false: exclude
 *    reference).
 **/
template
   <  typename T
   >
template
   <  typename U
   ,  typename INT
   >
void GeneralTensorDataCont<T>::ConstructXvecDataFromDataCont
   (  const GeneralDataCont<U>& aData
   ,  const ModeCombiOpRange& aExciRange
   ,  const std::vector<INT>& aNmodals
   ,  const bool aRef
   )
{
   static_assert(std::numeric_limits<INT>::is_integer);

   // reserve space in vector for all mode combis
   auto num_total_mode_combi = aExciRange.Size() - (aRef ? 0 : 1); // -1 to get rid of reference
   this->reserve(num_total_mode_combi);
   // reserve space in modal vector
   auto num_exci = aExciRange.GetMaxExciLevel();
   std::vector<unsigned> num_modals;
   num_modals.reserve(num_exci);
   
   // loop over mode combis
   for(auto& mode_combi : aExciRange)
   {
      const auto num_exci = mode_combi.Size();
      if(!aRef && (num_exci == 0)) // if we asked to remove the reference we do so
      {
         //Mout << " EMPTY MC: CONTINUE " << std::endl;
         continue; // if empty MC we continue
      }

      // find modal sizes for mode combi, i.e. the size of the mode combi tensor
      In size = 1;
      num_modals.resize(mode_combi.Size());
      for(In mode_idx = 0; mode_idx < num_modals.size(); ++mode_idx)
      {
         num_modals[mode_idx] = static_cast<unsigned>(aNmodals.at(mode_combi.Mode(mode_idx))-I_1); // number of exci modals for mode in mode combi (-1 to remove reference)
         size *= num_modals[mode_idx];
      }


      // load elements into ptr
      auto u_ptr = std::unique_ptr<U[]>(new U[size]);
      In addr = (mode_combi).Address() - (aRef ? 0 : 1);
      for(In element_idx = 0; element_idx < size; ++element_idx) // load in values
      {
         aData.DataIo(IO_GET, addr, u_ptr[element_idx]);
         ++addr;
      }

      // Load into new ptr if T != U
      std::unique_ptr<T[]> ptr = nullptr;
      if constexpr   (  !std::is_same_v<T, U>
                     )
      {
         ptr = std::make_unique<T[]>(size);
         for(In i=I_0; i<size; ++i)
         {
            ptr[i] = T(u_ptr[i]);
         }
      }
      // Else, we swap the pointers such that in any case 'ptr' contains the data
      else
      {
         std::swap(ptr, u_ptr);
      }
      
      // add tensor
      if(num_exci == 0) // reference
      {
         this->emplace_back(static_cast<BaseTensor<T>* >(new Scalar<T>(ptr[0])));
      }
      else 
      {
         this->CreateSimple(num_modals, ptr);
      }
   }
   
   // assert result
   assert(this->size() == num_total_mode_combi); // assert both size and num_total_mode_combis are the same, 
                                                 // or we have not handled enough elements
}

/**
 * Construct TensorDataCont of specific shape filled with zeros
 *
 * @param aExciRange                      The ModeCombiOpRange
 * @param aNmodals                        The number of modals for each mode
 * @param aRef                            Include reference?
 **/
template
   <  typename T
   >
template
   <  typename INT
   >
void GeneralTensorDataCont<T>::ConstructZeroTensorDataCont
   (  const ModeCombiOpRange& aExciRange
   ,  const std::vector<INT>& aNmodals
   ,  const bool aRef
   )
{
   static_assert(std::numeric_limits<INT>::is_integer, "INT must be integer type!");

   // reserve space in vector for all mode combis
   auto num_total_mode_combi = aExciRange.Size() - (aRef ? 0 : 1); // -1 to get rid of reference
   this->reserve(num_total_mode_combi);
   // reserve space in modal vector
   auto num_exci = aExciRange.GetMaxExciLevel();
   std::vector<unsigned> num_modals;
   num_modals.reserve(num_exci);
   
   // loop over mode combis
   for(auto& mode_combi : aExciRange)
   {
      const auto num_exci = mode_combi.Size();
      if(!aRef && (num_exci == 0)) // if we asked to remove the reference we do so
      {
         //Mout << " EMPTY MC: CONTINUE " << std::endl;
         continue; // if empty MC we continue
      }

      // find modal sizes for mode combi, i.e. the size of the mode combi tensor
      unsigned size = 1;
      num_modals.resize(mode_combi.Size());
      for(int mode_idx = 0; mode_idx < num_modals.size(); ++mode_idx)
      {
         num_modals[mode_idx] = static_cast<unsigned>(aNmodals.at(mode_combi.Mode(mode_idx))-I_1); // number of exci modals for mode in mode combi (-1 to remove reference)
         size *= num_modals[mode_idx];
      }
      
      // create zero-pointer
      auto ptr = std::unique_ptr<T[]>(new T[size]);
      auto addr = (mode_combi).Address() - (aRef ? 0 : 1); //
      for(int element_idx = 0; element_idx < size; ++element_idx) // load in values
      {
         ptr[element_idx] = C_0;
      }
      
      // add tensor
      if(num_exci == 0) // reference
      {
         this->emplace_back(static_cast<BaseTensor<T>* >(new Scalar<T>(ptr[0])));
      }
      else
      {
         this->CreateSimple(num_modals, ptr);
      }
   }
   
   // assert result
   assert(this->size() == num_total_mode_combi); // assert both size and num_total_mode_combis are the same, 
                                                 // or we have not handled enough elements
   
}

/**
 * Construct TensorDataCont from a single size giving size of 'tensor-stack'.
 * Will default construct all NiceTensor%s (i.e. all NiceTensor%s will point to
 * nullptr).
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  int aSize
   )
   :  std::vector<NiceTensor<T> >(aSize)
{
}

/**
 * 
 **/
template
   <  typename T
   >
template
   <  typename U
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  const GeneralDataCont<U>& aData
   ,  const ModeCombiOpRange& aExciRange
   ,  const VccCalcDef* apVccCalcDef
   ,  const bool aRef
   )
{
   std::vector<In> nmodals;
   apVccCalcDef->Nmodals(nmodals);
   ConstructXvecDataFromDataCont(aData, aExciRange, nmodals, aRef);
}

/**
 * Constructs a TensorDataCont of specific shape filled with zeros
 **/
template
   <  typename T
   >
template
   <  typename INT
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  const ModeCombiOpRange& aExciRange
   ,  const std::vector<INT>& arNmodals
   ,  const bool aRef
   )
{
   ConstructZeroTensorDataCont(aExciRange, arNmodals, aRef);
}

/**
 * Constructs a TensorDataCont of specific shape (as defined by VccStateSpace)
 * with all NiceTensor%s being zero of required type (as specified by the
 * typeID).
 *
 * @param arVSS         VccStateSpace (ModeCombi and dimensions information).
 * @param arType        Required type of NiceTensors.
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  const VccStateSpace& arVSS
   ,  const typename BaseTensor<T>::typeID& arType
   )
   :  std::vector<NiceTensor<T> >(I_0)
{
   // Reserve space in std::vector for all MCs.
   const auto& n_mc_total = arVSS.NumMCs();
   this->reserve(n_mc_total);

   // Use VccStateSpace to loop through all ModeCombis.
   for(const auto& mc: arVSS)
   {
      // Create new zero NiceTensor of given type, and dimensions. 
      // (Except for empty/reference MC, then make it a Scalar.)
      const auto& type = mc.MC().Empty() ? BaseTensor<T>::typeID::SCALAR : arType;

      // Niels: This is only necessary to be able to compile with In=long.
      auto mc_dims = mc.GetDims();
      std::vector<unsigned> tensor_dims( mc_dims.begin(), mc_dims.end() );

      this->emplace_back(tensor_dims, type);
   }

   // Assert result, i.e. that we have handled the proper amount of elements.
   MidasAssert(this->size() == n_mc_total, "Size of TDC doesn't match expected size.");
}

/**
 * Constructs a TensorDataCont of specific shape (as defined by VccStateSpace)
 * with all NiceTensor%s being zero of required type (as specified by the
 * typeID), except for the one that contains the index address, which is then a
 * standard unit tensor of required type.
 *
 * @param arVSS      VccStateSpace (ModeCombi and dimensions information).
 * @param aIndex     The (absolute) index of the element that should be 1.
 * @param arType     Required type of NiceTensors.
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  const VccStateSpace& arVSS
   ,  const Uin aIndex
   ,  const typename BaseTensor<T>::typeID& arType
   )
   :  std::vector<NiceTensor<T> >(I_0)
{
   // Reserve space in std::vector for all MCs.
   const auto& n_mc_total = arVSS.NumMCs();
   this->reserve(n_mc_total);

   // Use VccStateSpace to loop through all ModeCombis.
   for(const auto& mc: arVSS)
   {
      // Create new NiceTensor of given type, and dimensions. 
      // (Except for empty/reference MC, then make it a Scalar.)
      const auto& type = mc.MC().Empty() ? BaseTensor<T>::typeID::SCALAR : arType;

      // If we've reached the tensor containg the index/address, make it a unit
      // NiceTensor. Otherwise a zero NiceTensor.
      if(mc.ContainsAddress(aIndex))
      {
         // Niels: This is only necessary to be able to compile with In=long.
         auto mc_dims = mc.GetDims();
         std::vector<unsigned> tensor_dims( mc_dims.begin(), mc_dims.end() );

         // Niels: This is only necessary to be able to compile with In=long.
         auto mc_address_vec = mc.GetIndexVecFromAddress(aIndex);
         std::vector<unsigned> tensor_addresses( mc_address_vec.begin(), mc_address_vec.end() );

         this->emplace_back(tensor_dims, tensor_addresses, type);
      }
      else
      {
         auto mc_dims = mc.GetDims();
         std::vector<unsigned> tensor_dims( mc_dims.begin(), mc_dims.end() );

         this->emplace_back(tensor_dims, type);
      }
   }

   // Assert result, i.e. that we have handled the proper amount of elements.
   MidasAssert(this->size() == n_mc_total, "Size of TDC doesn't match expected size.");
}

/**
 * 
 **/
template
   <  typename T
   >
template
   <  typename U
   ,  typename INT
   >
GeneralTensorDataCont<T>::GeneralTensorDataCont
   (  const GeneralDataCont<U>& aData
   ,  const ModeCombiOpRange& aExciRange
   ,  const std::vector<INT>& aNmodals
   ,  const bool aRef
   )
{
   static_assert(std::numeric_limits<INT>::is_integer);
   ConstructXvecDataFromDataCont(aData, aExciRange, aNmodals, aRef);
}

/**
 * 
 **/
template
   <  typename T
   >
NiceTensor<T>& GeneralTensorDataCont<T>::GetModeCombiData
   (  const In aMC
   )
{
   return this->at(aMC);
}

/**
 * 
 **/
template
   <  typename T
   >
const NiceTensor<T>& GeneralTensorDataCont<T>::GetModeCombiData
   (  const In aMC
   )  const
{
   return this->at(aMC);
}

/**
 * Calculate and return the total size of the TensorDataCont.  This is done by
 * accumulating the individual sizes of each NiceTensor.
 *
 * @return             The total size of the TensorDataCont.                    
 **/
template
   <  typename T
   >
In GeneralTensorDataCont<T>::TotalSize
   (
   ) const
{
   In total_size = 0;
   for(In i = 0; i < this->Size(); ++i)
   {
      total_size += static_cast<In>( this->GetModeCombiData(i).TotalSize() );
   }
   return total_size;
}

/**
 * Zero the TensorDataCont, i.e. Zero all NiceTensors
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Zero
   (
   )
{
   for(size_t i = 0; i < this->size(); ++i)
   {
      this->at(i).Zero();
   }
}

/**
 * Make all the individual NiceTensor%s point to a ZeroTensor, as opposed to
 * Zero(), which sets each element equal to 0.0, but doesn't change the tensor
 * format.
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::ToZeroTensors
   (
   )
{
   for(In i=I_0; i<this->size(); ++i)
   {
      this->at(i).ToZeroTensor();
   }
}

/**
 * @return
 *    Complex conjugate
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T> GeneralTensorDataCont<T>::Conjugate
   (
   )  const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      auto result = *this;
      for(auto& t : result)
      {
         t.Conjugate();
      }
      return result;
   }
   else
   {
      return *this;
   }
}

/**
 * Calculate the squared norm of TensorDataCont. Norm2() is calculated as the
 * sum of the norm^2 of all single NiceTensors.
 *
 * @return         Return the squared norm of the TensorDataCont.
 **/
template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t GeneralTensorDataCont<T>::Norm2
   (
   ) const
{
   real_t norm2 = 0.0;
   for(int i = 0; i < this->Size(); ++i)
   {
      norm2 += this->GetModeCombiData(i).Norm2();
   }
   return norm2;
}

/**
 * Get the largest tensor norm in the TensorDataCont
 * @return     Return the largest norm
 */
template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t GeneralTensorDataCont<T>::MaxNorm
   (
   ) const
{
   real_t result = C_0;

   for(const auto& tens : *this)
   {
      result = std::max<real_t>(result, tens.Norm());
   }

   return result;
}

/**
 * Scale the TensorDataCont in-place by a number.
 *
 * @param aNb      Number to Scale with.
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::Scale
   (  U aNb
   )
{
   for(int i = 0; i < this->Size(); ++i)
   {
      this->GetModeCombiData(i).Scale(aNb);
   }
}

/**
 * Normalize the TensorDataCont in-place such that norm becomes 1.
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Normalize
   ( 
   )
{
   auto norm_inv = 1.0/this->Norm();
   for(int i = 0; i < this->Size(); ++i)
   {
      this->GetModeCombiData(i).Scale(norm_inv);
   }
}

/**
 * Balance mode vectors of CanonicalTensor%s in TensorDataCont
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Balance
   (
   )
{
   for(size_t i = 0; i < this->Size(); ++i)
   {
      auto& tens = this->GetModeCombiData(i);
      switch(tens.Type())
      {
         case BaseTensor<T>::typeID::CANONICAL:
         {
            static_cast<CanonicalTensor<T>*>(tens.GetTensor())->BalanceModeVectors();
            break;
         }
         case BaseTensor<T>::typeID::DIRPROD:
         {
            MidasWarning("We need to implement BalanceModeVectors for TensorDirectProduct!");
            break;
         }
         case BaseTensor<T>::typeID::SUM:
         {
            MidasWarning("We need to implement BalanceModeVectors for TensorSum!");
            break;
         }
         case BaseTensor<T>::typeID::SCALAR:   // fall-through to default
         case BaseTensor<T>::typeID::SIMPLE:   // fall-through to default
         default:
         {
            // Do nothing...
         }
      }
   }
}

/**
 * Set TensorDataCont to its absolute value
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Abs
   (
   )
{
   for(In i=I_0; i<this->Size(); ++i)
   {
      this->GetModeCombiData(i).Abs();
   }
}


/**
 * Decompose the TensorDataCont to the specs defined in class.
 *
 * @param aDecompInfos     Set of decomposition information.
 * @param apDisable        Disable for complex numbers
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::DecomposeImpl
   (  const TensorDecompInfo::Set& aDecompInfos
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   #pragma omp parallel
   {
   using midas::tensor::TensorDecomposer;
   TensorDecomposer decomposer(aDecompInfos);

   #pragma omp for schedule(dynamic)
   for(size_t i = 0; i < this->Size(); ++i)
   {
      #ifdef VAR_MPI
      // Distribute with MPI
      if (  midas::mpi::GlobalRank() != i % midas::mpi::GlobalSize()
         )
      {
         continue;
      }
      #endif

      if(decomposer.CheckDecomposition(this->GetModeCombiData(i)))
      {
         this->GetModeCombiData(i) = decomposer.Decompose(this->GetModeCombiData(i));
      }
   }
   }
}

/**
 * Decompose the TensorDataCont to the specs defined in class with argument thresholds.
 *
 * @param aDecompInfos   Set of decomposition information.
 * @param aThresholds    Vector of requested accuracies
 * @param apDisable        Disable for complex numbers
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::DecomposeImpl
   (  const TensorDecompInfo::Set& aDecompInfos
   ,  const std::vector<real_t>& aThresholds
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   assert(  aThresholds.size() == this->Size() );

   #pragma omp parallel
   {
   using midas::tensor::TensorDecomposer;
   TensorDecomposer decomposer(aDecompInfos);

   #pragma omp for schedule(dynamic)
   for(size_t i = 0; i < this->Size(); ++i)
   {
      #ifdef VAR_MPI
      // Distribute with MPI
      if (  midas::mpi::GlobalRank() != i % midas::mpi::GlobalSize()
         )
      {
         continue;
      }
      #endif

      if(decomposer.CheckDecomposition(this->GetModeCombiData(i)))
      {
         const auto& thresh = aThresholds[i];
         this->GetModeCombiData(i) = decomposer.Decompose(this->GetModeCombiData(i), thresh);
      }
   }
   }
}

/**
 * Decompose all tensors to same threshold.
 *
 * @param aDecompInfos
 * @param aThresh
 * @param apDisable        Disable for complex numbers
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::DecomposeImpl
   (  const TensorDecompInfo::Set& aDecompInfos
   ,  real_t aThresh
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   #pragma omp parallel
   {
   using midas::tensor::TensorDecomposer;
   TensorDecomposer decomposer(aDecompInfos);

   #pragma omp for schedule(dynamic)
   for(size_t i = 0; i < this->Size(); ++i)
   {
      #ifdef VAR_MPI
      // Distribute with MPI
      if (  midas::mpi::GlobalRank() != i % midas::mpi::GlobalSize()
         )
      {
         continue;
      }
      #endif

      if(decomposer.CheckDecomposition(this->GetModeCombiData(i)))
      {
         this->GetModeCombiData(i) = decomposer.Decompose(this->GetModeCombiData(i), aThresh);
      }
   }
   }
}

/**
 * Decompose the TensorDataCont to the specs defined in class.
 *
 * @param aDecompInfos   Set of decomposition information.
 * @param aGuess         TensorDataCont used as starting guess for decompositions
 * @param apDisable        Disable for complex numbers
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::DecomposeImpl
   (  const TensorDecompInfo::Set& aDecompInfos
   ,  const GeneralTensorDataCont<T>& aGuess
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   assert( aGuess.Size() == this->Size() );

   #pragma omp parallel
   {
      using midas::tensor::TensorDecomposer;
      TensorDecomposer decomposer(aDecompInfos);

      #pragma omp for schedule(dynamic)
      for(size_t i = 0; i < this->Size(); ++i)
      {
         #ifdef VAR_MPI
         // Distribute with MPI
         if (  midas::mpi::GlobalRank() != i % midas::mpi::GlobalSize()
            )
         {
            continue;
         }
         #endif

         if(decomposer.CheckDecomposition(this->GetModeCombiData(i)))
         {
            this->GetModeCombiData(i) = decomposer.Decompose(this->GetModeCombiData(i), aGuess.GetModeCombiData(i));
         }
      }
   }
}

/**
 * Decompose the TensorDataCont to the specs defined in class with argument thresholds.
 *
 * @param aDecompInfos   Set of decomposition information.
 * @param aGuess         TensorDataCont used as starting guess for decompositions
 * @param aThresholds    Vector of requested accuracies.
 * @param apDisable        Disable for complex numbers
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::DecomposeImpl
   (  const TensorDecompInfo::Set& aDecompInfos
   ,  const GeneralTensorDataCont<T>& aGuess
   ,  const std::vector<real_t>& aThresholds
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   assert( aGuess.Size() == this->Size() );
   assert( aThresholds.size() == this->Size() );

   #pragma omp parallel
   {
      using midas::tensor::TensorDecomposer;
      TensorDecomposer decomposer(aDecompInfos);

      #pragma omp for schedule(dynamic)
      for(size_t i = 0; i < this->Size(); ++i)
      {
         #ifdef VAR_MPI
         // Distribute with MPI
         if (  midas::mpi::GlobalRank() != i % midas::mpi::GlobalSize()
            )
         {
            continue;
         }
         #endif

         if(decomposer.CheckDecomposition(this->GetModeCombiData(i)))
         {
            this->GetModeCombiData(i) = decomposer.Decompose(this->GetModeCombiData(i), aGuess.GetModeCombiData(i), aThresholds[i]);
         }
      }
   }
}

/**
 * Recompose the TensorDataCont to all SimpleTensors.
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Recompose
   (
   )
{
   for(int i = 0; i < this->Size(); ++i)
   {
      if(this->GetModeCombiData(i).Type() != BaseTensor<T>::typeID::SIMPLE)
      {
         this->GetModeCombiData(i) = this->GetModeCombiData(i).ToSimpleTensor();
      }
   }
}

/**
 * Get the smallest rank of the decomposed tensors
 *
 * @return Returns the smallest rank. Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
In GeneralTensorDataCont<T>::GetMinRank() const
{
   In result = -I_1;

   // Set result to the rank of the first CanonicalTensor
   In j = I_0;
   for(; j<this->Size(); ++j)
   {
      if(this->GetModeCombiData(j).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         result = static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(j).GetTensor())->GetRank();
         break;
      }
   }

   // Check the remaining tensors to find the minimum rank
   for(In i=j; i<this->Size(); ++i)
   {
      if(this->GetModeCombiData(i).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         result = std::min<In>(result, static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(i).GetTensor())->GetRank());
      }
   }

   return result;
}
/**
 * Get the smallest rank of the decomposed tensors.
 *
 * @return
 *    Returns the smallest ranks of different tensor orders in std::vector.
 *    1.-order tensors in vec[1] etc. Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
std::vector<In> GeneralTensorDataCont<T>::GetMinRankVec() const
{
   const auto& max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<In> result(max_order,-I_1);

   // Set result to the rank of the first CanonicalTensor
   for(In k=I_1; k<=max_order; ++k)
   {
      In j = I_0;
      for(; j<this->Size(); ++j)
      {
         const auto& tensor = this->GetModeCombiData(j);

         if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL && tensor.NDim() == k)
         {
            result.at(tensor.NDim()-I_1) = static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank();
            break;
         }
      }

      // Check the remaining tensors to find the minimum rank
      for(In i=j; i<this->Size(); ++i)
      {
         const auto& tensor = this->GetModeCombiData(i);

         if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL && tensor.NDim() == k)
         {
            result.at(k-I_1) = std::min<In>(result.at(k-I_1)
                  , static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank());
         }
      }
   }

   return result;
}

/**
 * Get the largest rank of the decomposed tensors
 *
 * @return Returns the largest rank. Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
In GeneralTensorDataCont<T>::GetMaxRank() const
{
   In result = -I_1;

   // Set result to the rank of the first CanonicalTensor
   In j = I_0;
   for(; j<this->Size(); ++j)
   {
      if(this->GetModeCombiData(j).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         result = static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(j).GetTensor())->GetRank();
         break;
      }
   }

   // Check the remaining tensors to find the max rank
   for(In i=j; i<this->Size(); ++i)
   {
      if(this->GetModeCombiData(i).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         result = std::max<In>(result, static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(i).GetTensor())->GetRank());
      }
   }

   return result;
}

/**
 * Get the largest ranks of the decomposed tensors pr. tensor order
 * 
 * @return 
 *    Returns the largest ranks of different tensor orders in std::vector.
 *    1.-order tensors in vec[1] etc. Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
std::vector<In> GeneralTensorDataCont<T>::GetMaxRankVec() const
{
   const auto& max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<In> result(max_order,-I_1);

   for(In k=I_1; k<=max_order; ++k)
   {
      // Set result to the rank of the first CanonicalTensor
      In j = I_0;
      for(; j<this->Size(); ++j)
      {
         const auto& tensor = this->GetModeCombiData(j);
         if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL && tensor.NDim() == k)
         {
            result.at(tensor.NDim()-I_1) = static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank();
            break;
         }
      }

      // Check the remaining tensors to find the minimum rank
      for(In i=j; i<this->Size(); ++i)
      {
         const auto& tensor = this->GetModeCombiData(i);
         if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL && tensor.NDim() == k)
         {
            result.at(k-I_1) = std::max<In>(result.at(k-I_1)
                  , static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank());
         }
      }
   }

   return result;
}

/*
 * Get the average rank of the decomposed tensors
 * 
 * @return The average rank. Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t GeneralTensorDataCont<T>::AverageRank() const
{
   In rank_sum = I_0;
   In tensor_count = I_0;

   for(In i = I_0; i<this->Size(); ++i)
   {
      if(this->GetModeCombiData(i).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         ++tensor_count;
         rank_sum += static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(i).GetTensor())->GetRank();
      }
   }

   return ((tensor_count < I_1) ? static_cast<real_t>(-C_1) : (static_cast<real_t>(rank_sum)/static_cast<real_t>(tensor_count))); 
}

/*
 * Get the average rank of the decomposed tensors
 *
 * @return 
 *    The average rank for each tensor order.
 *    Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
std::vector<typename GeneralTensorDataCont<T>::real_t> GeneralTensorDataCont<T>::AverageRankVec() const
{
   const auto& max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<In> rank_sum(max_order,I_0);
   std::vector<In> tensor_count(max_order,I_0);
   std::vector<real_t> result(max_order);

   for(In i = I_0; i<this->Size(); ++i)
   {
      const auto& tensor = this->GetModeCombiData(i);
      if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         ++(tensor_count.at(tensor.NDim()-I_1));
         rank_sum.at(tensor.NDim()-I_1) += static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank();
      }
   }

   for(In i=I_0; i<max_order; ++i)
   {
      if(tensor_count.at(i) > I_0)
      {
         result.at(i) = static_cast<real_t>(rank_sum.at(i)) / static_cast<real_t>(tensor_count.at(i));
      }
      else result.at(i) = -C_1;
   }
   return result; 
}

/*
 * Get the average difference between the actual ranks of the decomposed
 * tensors and their theoretical max-ranks.
 *
 * @return 
 *    The average rank difference between actual rank and theoretical max-rank.
 *    Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t GeneralTensorDataCont<T>::AverageRankDifference() const
{
   In rank_diff_sum = I_0;
   In tensor_count = I_0;

   for(In i = I_0; i<this->Size(); ++i)
   {
      if(this->GetModeCombiData(i).Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         ++tensor_count;

         const auto& dims = this->GetModeCombiData(i).GetDims();
         const auto& rank = static_cast<CanonicalTensor<T>* >(this->GetModeCombiData(i).GetTensor())->GetRank();
         const auto  theoretical_max_rank = this->GetModeCombiData(i).TotalSize() / *std::max_element(dims.begin(), dims.end());
         rank_diff_sum += (theoretical_max_rank - rank);
      }
   }

   return (tensor_count < I_1) ? static_cast<real_t>(-C_1) : (static_cast<real_t>(rank_diff_sum)/static_cast<real_t>(tensor_count)); 
}

/*
 * Get the average difference between the actual ranks of the decomposed
 * tensors and their theoretical max-ranks.
 *
 * @return 
 *    The average rank difference between actual rank and theoretical max-rank
 *    per tensor order.  Returns -1 if no tensors are decomposed.
 */
template
   <  typename T
   >
std::vector<typename GeneralTensorDataCont<T>::real_t> GeneralTensorDataCont<T>::AverageRankDifferenceVec() const
{
   const auto& max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<In> rank_diff_sum(max_order,I_0);
   std::vector<In> tensor_count(max_order,I_0);
   std::vector<real_t> result(max_order);

   for(In i = I_0; i<this->Size(); ++i)
   {
      const auto& tensor = this->GetModeCombiData(i);
      if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         ++(tensor_count.at(tensor.NDim()-I_1));

         const auto& dims = tensor.GetDims();
         const auto& rank = static_cast<CanonicalTensor<T>* >(tensor.GetTensor())->GetRank();
         const auto  theoretical_max_rank = tensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
         rank_diff_sum.at(tensor.NDim()-I_1) += (theoretical_max_rank - rank);
      }
   }

   for(In i=I_0; i<max_order; ++i)
   {
      if(tensor_count.at(i) > I_0)
      {
         result.at(i) = static_cast<real_t>(rank_diff_sum.at(i)) / static_cast<real_t>(tensor_count.at(i));
      }
      else result.at(i) = -C_1;
   }
   return result; 
}

/**
 * Calculates the compression factor (Decomp / Full) for each tensor order.
 * Includes full tensors in the sum to get the total compression.
 *
 * @return Compression factors as std::vector<real_t>
 **/
template
   <  typename T
   >
std::vector<typename GeneralTensorDataCont<T>::real_t> GeneralTensorDataCont<T>::DataCompressionFactors() const
{
   const auto& max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<real_t> full_tensor_size(max_order,C_0);
   std::vector<real_t> cp_tensor_size(max_order,C_0);
   std::vector<real_t> result(max_order);

   for(In i=I_0; i<this->Size(); ++i)
   {
      const auto& tensor = this->GetModeCombiData(i);
      const auto& tensor_dims = tensor.GetDims();
      if(tensor.Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         full_tensor_size.at(tensor.NDim()-I_1) += tensor.TotalSize();
         cp_tensor_size.at(tensor.NDim()-I_1)   += static_cast<CanonicalTensor<T>*>(tensor.GetTensor())->NElem();
      }
      else if(tensor.Type() == BaseTensor<T>::typeID::SIMPLE)
      {
         full_tensor_size.at(tensor.NDim()-I_1) += tensor.TotalSize();
         cp_tensor_size.at(tensor.NDim()-I_1)   += tensor.TotalSize();
      }
      else MIDASERROR("TensorDataCont::DataCompressionFactors(): Did not recognize tensor type!");
   }

   for(In i=I_0; i<max_order; ++i)
   {
      if(full_tensor_size.at(i) > C_0)
      {
         result.at(i) = cp_tensor_size.at(i) / full_tensor_size.at(i);
      }
      else result.at(i) = -C_1;
   }

   return result;
}

/**
 * Get ranks ordered by exci level.
 *
 * @return vector of vectors of ranks for each exci level.
 **/
template
   <  typename T
   >
std::vector<std::vector<In>> GeneralTensorDataCont<T>::GetRanksVec
   (
   )  const
{
   auto size = this->Size();
   auto max_order = this->back().NDim(); // Assumes logical ordering of mode-combis
   std::vector<std::vector<In>> result(max_order);

   // Reserve space (too much...)
   for(size_t k=0; k<max_order; ++k)
   {
      result.at(k).reserve(size);
   }

   // Go through tensors
   for(size_t i=0; i<size; ++i)
   {
      auto& tens = this->GetModeCombiData(i);
      auto order = tens.NDim();

      // Safety check
      if (  order > result.size()
         )
      {
         MidasWarning("TensorDataCont::GetRanks: Last tensor does not have highest order!");
         result.resize(order);
      }

      // Get tensor type
      switch(tens.Type())
      {
         case BaseTensor<T>::typeID::CANONICAL:
         {
            result[order-I_1].push_back(static_cast<CanonicalTensor<T>*>(tens.GetTensor())->GetRank());
            break;
         }
         case BaseTensor<T>::typeID::SUM:
         {
            result[order-I_1].push_back(static_cast<TensorSum<T>*>(tens.GetTensor())->Rank());
            break;
         }
         case BaseTensor<T>::typeID::DIRPROD:
         {
            result[order-I_1].push_back(static_cast<TensorDirectProduct<T>*>(tens.GetTensor())->Rank());
            break;
         }
         case BaseTensor<T>::typeID::ZERO:
         {
            result[order-I_1].push_back(I_0);
            break;
         }
         default:
         {
            MidasWarning(tens.ShowType() + " does not have a rank...");
            result[order-I_1].push_back(-I_1);
            break;
         }
      }
   }

   // Return result
   return result;
}

/**
 * Get all ranks
 *
 * @return
 *    All ranks in one vector
 **/
template
   <  typename T
   >
std::vector<In> GeneralTensorDataCont<T>::GetRanks
   (
   )  const
{
   auto size = this->Size();
   std::vector<In> result(size);

   for(size_t i=0; i<size; ++i)
   {
      auto& tens = this->GetModeCombiData(i);

      auto rank = I_0;
      switch(tens.Type())
      {
         case BaseTensor<T>::typeID::CANONICAL:
         {
            rank = static_cast<CanonicalTensor<T>*>(tens.GetTensor())->GetRank();
            break;
         }
         case BaseTensor<T>::typeID::SUM:
         {
            rank = static_cast<TensorSum<T>*>(tens.GetTensor())->Rank();
            break;
         }
         case BaseTensor<T>::typeID::DIRPROD:
         {
            rank = static_cast<TensorDirectProduct<T>*>(tens.GetTensor())->Rank();
            break;
         }
         case BaseTensor<T>::typeID::ZERO:
         {
            rank = I_0;
            break;
         }
         default:
         {
            MidasWarning(tens.ShowType() + " does not have a rank... Using R = -1.");
            rank = -I_1;
            break;
         }
      
      }

      result[i] = rank;
   }

   return result;
}

/**
 * Print the order and extents of all tensors in TensorDataCont.
 * Mostly used for debugging purposes.
 *
 * @param aOs   The std::ostream to print the data to.
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::PrintOrderAndExtents
   (  std::ostream& aOs
   )  const
{
   for(In i = 0; i < this->Size(); ++i)
   {
      const auto& tensor = this->GetModeCombiData(i);
      aOs << " tensor : " << std::setw(5) << i << " extent : " << tensor.GetDims() << std::endl;
   }
}

/**
 * Check that all tensors are of a specific type
 * 
 * @param aType   BaseTensor<T>::typeID
 * @return
 *    True if all are of given type
 **/
template
   <  typename T
   >
bool GeneralTensorDataCont<T>::AllOfType
   (  const typename BaseTensor<T>::typeID& aType
   )  const
{
   for(In i=0; i < this->Size(); ++i)
   {
      if (  this->GetModeCombiData(i).Type() != aType )
      {
         return false;
      }
   }

   return true;
}

/**
 * Calculates sum of product of elements as the dot/inner/scalar product 
 * of two TensorDataCont%s, `<T^*|U>`, looping through the individual 
 * tensors in the TensorDataCont.
 *
 * Calls SumProdElems(const NiceTensor<T>&,const NiceTensor<T>&) for each
 * pair of tensors.
 *
 * @param arT    The TensorDataCont to be summed over.
 * @param arU    The other TensorDataCont to be summed over.
 * @return       Returns the sum.
 **/
template
   <  typename T
   >
T SumProdElems
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   )
{
   //Assert that TensorDataCont dimensions are correct:
   MidasAssert(arT.Size() == arU.Size(), "Size mismatch of TensorDataCont's in SumProdElems");
   //Loop through individual tensors, consecutively adding to final result:
   T result = 0.0;
   if constexpr   (  std::is_same_v<T, std::complex<float>>
                  )
   {
      #pragma omp parallel for reduction(ComplexFloat_Add:result)
      for(Uin i = 0; i < arT.Size(); ++i)
      {
         result += sum_prod_elem(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }
   else if constexpr (  std::is_same_v<T, std::complex<double>>
                     )
   {
      #pragma omp parallel for reduction(ComplexDouble_Add:result)
      for(Uin i = 0; i < arT.Size(); ++i)
      {
         result += sum_prod_elem(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }
   else
   {
      #pragma omp parallel for reduction(+:result)
      for(Uin i = 0; i < arT.Size(); ++i)
      {
         result += sum_prod_elem(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }
   return result;
}


/**
 * Calculates dot/inner/scalar product of two TensorDataCont%s, `<T|U>`,
 * looping through the individual tensors in the TensorDataCont.  Calls
 * dot_product(const NiceTensor<T>&,const NiceTensor<T>&) for each pair of
 * tensors.
 *
 * @param arT    The TensorDataCont to be dotted.
 * @param arU    The other TensorDataCont to be dotted.
 * @return       Returns the dot.
 **/
template
   <  typename T
   >
T Dot
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   )
{
   //Assert that TensorDataCont dimensions are right:
   MidasAssert(arT.Size() == arU.Size(), "Size mismatch of TensorDataCont's in Dot.");

   //Loop through individual tensors, consecutively adding to final result:
   T result = 0.0;
   if constexpr   (  std::is_same_v<T, std::complex<float>>
                  )
   {
      #pragma omp parallel for reduction(ComplexFloat_Add:result)
      for(In i=I_0; i<arT.Size(); ++i)
      {
         result += dot_product(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }
   else if constexpr (  std::is_same_v<T, std::complex<double>>
                     )
   {
      #pragma omp parallel for reduction(ComplexDouble_Add:result)
      for(In i=I_0; i<arT.Size(); ++i)
      {
         result += dot_product(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }
   else
   {
      #pragma omp parallel for reduction(+:result)
      for(In i=I_0; i<arT.Size(); ++i)
      {
         result += dot_product(arT.GetModeCombiData(i), arU.GetModeCombiData(i));
      }
   }

   return result;
}

/**
 * Performs `Y += aA*arX`, with Y being this object. Loops through the
 * individual tensors in the TensorDataCont.
 *
 * @param aX  The TensorDataCont's to be multiplied and added to the calling object.
 * @param aA  The scalar with which to multiply arX.
 **/
template
   <  typename T
   >
template
   <  typename U
   >
void GeneralTensorDataCont<T>::Axpy
   (  const GeneralTensorDataCont<T>& aX
   ,  U aA
   )
{
   //Assert that TensorDataCont dimensions are right:
   MidasAssert(Size() == aX.Size(), "Size mismatch of TensorDataCont's in Axpy.");

   //Loop through individual tensors, consecutively doing Axpy with NiceTensors.
   for(In i=I_0; i<Size(); ++i)
   {
      this->GetModeCombiData(i).Axpy(aX.GetModeCombiData(i), aA);
   }
}

/**
 * Calculate linear combination of list of TensorDataConts
 *
 * @param aData      TensorDataConts
 * @param aCoeffs    Coefficients
 **/
template
   <  typename T
   ,  typename U
   ,  template<typename...> typename CONTAINER_T
   ,  template<typename...> typename CONTAINER_U
   >
GeneralTensorDataCont<T> LinComb
   (  const CONTAINER_T<GeneralTensorDataCont<T>>& aData
   ,  const CONTAINER_U<U>& aCoeffs
   )
{
   // Assert same number of coeffs and vectors
   MidasAssert(aData.size() == aCoeffs.size(), "Size mismatch of data and coefs in LinComb.");

   // Construct result of correct shape by setting it to first vector
   auto ivec = aData.begin();
   auto result = (*ivec);
   result.Scale(aCoeffs.front());
   ++ivec;

   // Add the rest of the terms
   In i = I_1;
   for(; ivec!=aData.end(); ++ivec)
   {
      result.Axpy(*ivec, aCoeffs[i]);
      ++i;
   }

   return result;
}

/**
 * In-place orthogonalization of the calling TensorDataCont wrt. the
 * TensorDataCont in the argument, i.e. T -= U <T|U>/<U|U>, with T being this
 * object and U the argument.
 *
 * @param arU
 *    The (constant) TensorDataCont with respect to which the calling object is
 *    orthogonalized.
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::Orthogonalize
   (  const GeneralTensorDataCont<T>& arU
   )
{
   // Calculate the factor <T|U>/<U|U>.
   auto factor = Dot(*this, arU)/arU.Norm2();

   // Subtract U <T|U>/<U|U> from the calling object (T).
   this->Axpy(arU, -factor);
}

/**
 * Add a reference as the first element of TensorDataCont.
 *
 * @param aData TensorDataCont to construct the rest from
 * @param aRef  Reference to add
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T> GeneralTensorDataCont<T>::AddReference
   (  const GeneralTensorDataCont<T>& aData
   ,  T aRef
   )
{
   GeneralTensorDataCont<T> new_data(aData.Size() + 1);
   new_data[0] = NiceScalar<T>(aRef);
   for(int i = 1; i < new_data.Size(); ++i)
   {
      new_data[i] = aData[i - 1];
   }
   return new_data;
}

/**
 * 
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>& GeneralTensorDataCont<T>::operator+=
   (  const GeneralTensorDataCont<T>& arRight
   )
{
   if(this->Size() == arRight.Size())
   {
      for(int i=0; i<this->Size(); ++i)
      {
         this->at(i) += arRight.at(i);
      }
   }
   else
   {
      MIDASERROR("GeneralTensorDataCont::operator+=() ARGUMENTS HAVE DIFFERENT SIZES");
   }
   return *this;
}

/**
 * 
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>& GeneralTensorDataCont<T>::operator-=
   (  const GeneralTensorDataCont<T>& arRight
   )
{
   if(this->Size() == arRight.Size())
   {
      for(int i=0; i<this->Size(); ++i)
      {
         this->at(i) -= arRight.at(i);
      }
   }
   else
   {
      MIDASERROR("GeneralTensorDataCont::operator-=() ARGUMENTS HAVE DIFFERENT SIZES");
   }
   return *this;
}

/**
 *
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>& GeneralTensorDataCont<T>::operator*=
   (  const GeneralTensorDataCont<T>& arRight
   )
{
   if(this->Size() == arRight.Size())
   {
      for(int i=0; i<this->Size(); ++i)
      {
         this->at(i) *= arRight.at(i);
      }
   }
   else
   {
      MIDASERROR("GeneralTensorDataCont::operator*=() ARGUMENTS HAVE DIFFERENT SIZES");
   }
   return *this;
}

/**
 *
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>& GeneralTensorDataCont<T>::operator/=
   (  const GeneralTensorDataCont<T>& arRight
   )
{
   if(this->Size() == arRight.Size())
   {
      for(int i=0; i<this->Size(); ++i)
      {
         this->at(i) /= arRight.at(i);
      }
   }
   else
   {
      MIDASERROR("GeneralTensorDataCont::operator/=() ARGUMENTS HAVE DIFFERENT SIZES");
   }
   return *this;
}

/**
 * Adds aScalar to each element in the NiceTensor%s of TensorDataCont.
 *
 * @param aScalar
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T>& GeneralTensorDataCont<T>::ElementwiseScalarAddition
   (  T aScalar
   )
{
   for(int i=0; i<this->Size(); ++i)
   {
      auto& tens = this->GetModeCombiData(i);
      const auto& dims = tens.GetDims();

      switch(tens.GetTensor()->Type())
      {
         case BaseTensor<T>::typeID::SIMPLE:
         {
            tens += NiceSimpleTensor<T>(dims, aScalar);
            break;
         }
         case BaseTensor<T>::typeID::CANONICAL:
         {
            // Add rank-1 CanonicalTensor
            tens += NiceTensor<T>( const_value_cptensor<T>(dims, aScalar, I_1) );
            break;
         }
         case BaseTensor<T>::typeID::SCALAR:
         {
            tens.ElementwiseScalarAddition(aScalar);
            break;
         }
         default:
            MIDASERROR("GeneralTensorDataCont::ElementwiseScalarAddition() NOT IMPLEMENTED FOR "+tens.ShowType());
      }
   }   

   return *this;
}

/**
 * Write TensorDataCont to file.
 *
 * @param arFileName    The output file
 * @param aStreamType   How to output for MPI?
 **/
template
   <  typename T
   >
void GeneralTensorDataCont<T>::WriteToDisc
   (  const std::string& arFileName
   ,  midas::mpi::OFileStream::StreamType aStreamType
   )  const
{
   midas::mpi::OFileStream output( arFileName, aStreamType, std::ios::out | std::ios::binary );
   write_NiceTensor_vector<T>(*this, output);
   output.close();
}

/**
 * Read TensorDataCont from file and assign to `*this`.
 * @param arFileName The input file
 * @param aHardErr      Throw error if file not found
 * @return
 *    True for success
 **/
template
   <  typename T
   >
bool GeneralTensorDataCont<T>::ReadFromDisc
   (  const std::string& arFileName
   ,  bool aHardErr
   )
{
   if (  InquireFile(arFileName)
      )
   {
      std::vector<NiceTensor<T>> vec;
      std::ifstream input( arFileName, std::ios::in | std::ios::binary );
      input.exceptions( std::ifstream::failbit | std::ifstream::badbit );
      vec = read_NiceTensor_vector<T>(input);
      input.close();

      GeneralTensorDataCont<T> tdc(vec.size());

      for(In i=I_0; i<vec.size(); ++i)
      {
         tdc.GetModeCombiData(i) = vec.at(i);
      }

      *this = tdc;

      return true;
   }
   else if  (  aHardErr
            )
   {
      MIDASERROR("GeneralTensorDataCont::ReadFromDisk: File \""+arFileName+"\" not found!");
   }

   return false;
}


/**
 * Check if the total number of elements is larger than 
 * the capacity of an In.
 * NB: This does not always work if the total size of one tensor 
 * larger than the capacity of an In.
 *
 * @return     True if overflow.
 **/
template
   <  typename T
   >
bool GeneralTensorDataCont<T>::CheckOverflow
   (
   )  const
{
   In size = I_0;
   In size_old = I_0;
   
   auto ntens = this->Size();

   // Accumulate size
   for(unsigned itens=0; itens<ntens; ++itens)
   {
      size_old = size;
      size += static_cast<In>(this->GetModeCombiData(itens).TotalSize());

      // If size is not larger than size_old, we have overflow.
      if (  size < size_old   )
      {
         return true;
      }
   }

   return false;
}

/**
 * Sanity check of CanonicalTensor instances
 *
 * @return  True if everything is fine
 **/
template
   <  typename T
   >
bool GeneralTensorDataCont<T>::SanityCheck
   (
   )  const
{
   bool sane = true;
   for(size_t i=0; i<this->Size(); ++i)
   {
      auto& tens = this->GetModeCombiData(i);

      sane = sane && tens.SanityCheck();
   }

   return sane;
}



/**
 * Output tensor xvec.
 *
 * @param aOs             The output stream
 * @param aTensorDataCont The tensor xvec to ouput
 **/
template
   <  typename T
   >
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const GeneralTensorDataCont<T>& aTensorDataCont
   )
{
   for(In i = 0; i < aTensorDataCont.Size(); ++i)
   {
      aOs << aTensorDataCont.at(i) << std::endl;
   }
   return aOs;
}

/**
 * Convert TensorDataCont to DataCont.
 *
 * @param aIn TensorDataCont to convert
 **/
template
   <  typename T
   >
GeneralDataCont<T> DataContFromTensorDataCont
   (  const GeneralTensorDataCont<T>& aIn
   )
{
   // find total size of DataCont
   In size = 0;
   for(int i = 0; i < aIn.Size(); ++i)
   {
      size += aIn.GetModeCombiData(i).TotalSize();
   }
   
   // create DataCont and load in data
   GeneralDataCont<T> datacont_out(size);
   T* ptr = datacont_out.GetVector()->data();
   for(int i = 0; i < aIn.Size(); ++i)
   {
      aIn.GetModeCombiData(i).GetTensor()->DumpInto(ptr);
      ptr += aIn.GetModeCombiData(i).TotalSize();
   }
   
   // return result
   return datacont_out; 
}

/**
 * Convert TensorDataCont to NiceTensor, i.e. assemble full tensor of e.g. CI coefficients from the MC-based expansion.
 *
 * @param aIn                 TensorDataCont to be converted
 * @param aExtRangeModes      Modes for which the MCR is extended
 * @return
 *    NiceTensor of parameters (zero elements for the parts not included in the MCR)
 **/
template
   <  typename T
   >
NiceTensor<T> NiceTensorFromTensorDataCont
   (  const GeneralTensorDataCont<T>& aIn
   ,  const std::set<In>& aExtRangeModes
   )
{
   // 1) Figure out the MCR, i.e. NModes and MaxExciLevel
   In size = aIn.Size();
   In nmodes = I_0;
   std::vector<unsigned> nmodals;
   for(In i=I_0; i<size; ++i)
   {
      const auto& tens_i = aIn.GetModeCombiData(i);
      auto ndim_i = tens_i.NDim();

      // Skip the reference
      if (  ndim_i == I_0
         )
      {
         continue;
      }
      // Break if we have reaced the two-mode combis
      else if  (  ndim_i > I_1
               )
      {
         break;
      }

      // Add one mode per one-mode combi
      ++nmodes;

      // Number of modals for this mode is the total size of the one-mode excitations + 1.
      nmodals.emplace_back(tens_i.TotalSize() + 1);
   }

   // Get max exci (we assume the last tensor is of highest dimensionality)
   In max_exci = aIn.GetModeCombiData(size-I_1).NDim();

//   Mout  << " Converting TensorDataCont to NiceTensor.\n"
//         << "    NModes:      " << nmodes << "\n"
//         << "    MaxExci:     " << max_exci << "\n"
//         << "    NModals:     " << nmodals << "\n"
//         << "    xCS modes:   " << aExtRangeModes << "\n"
//         << "    TensorDataCont:\n" << aIn << "\n"
//         << std::flush;

   // 2) Init MCR and result tensor
   ModeCombiOpRange mcr;
   if (  aExtRangeModes.empty()
      )
   {
      mcr = ModeCombiOpRange(static_cast<Uin>(max_exci), static_cast<Uin>(nmodes));
   }
   else
   {
      Uin eff_max_exci = Uin(max_exci) - aExtRangeModes.size();
      mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(eff_max_exci, static_cast<Uin>(nmodes), aExtRangeModes);
   }

   auto mcr_size = mcr.Size();
   MidasAssert(mcr_size == size, "MCR has more MCs than tensors in input TensorDataCont.");
   auto result = NiceSimpleTensor<T>(nmodals);

   // 3) Loop over MCs and set the coefs
   for(In imc=I_0; imc<mcr_size; ++imc)
   {
      const auto& mc = mcr.GetModeCombi(imc);
      MidasAssert(mc.Size() == aIn.GetModeCombiData(imc).NDim(), "MC size and tensor dim does not match!");
//      Mout  << " Setting elements for MC: " << mc << std::endl;
      const auto& mcvec = mc.MCVec();

      // Special case for reference
      if (  mcvec.empty()
         )
      {
         MidasAssert(imc == I_0, "Only expected empty MC as the first element of MCR vector");
         const auto& itens = aIn.GetModeCombiData(imc);
         MidasAssert(itens.Type() == BaseTensor<T>::typeID::SCALAR, "Expected Scalar tensor for reference MC.");
         static_cast<SimpleTensor<T>*>(result.GetTensor())->GetData()[0] = itens.GetScalar();
      }
      else
      {
         // Use ArrayHopper to iterate through result
         std::vector<unsigned> hopper_dims(mcvec.size());   // Dims to hop over
         std::vector<unsigned> subdims(nmodes, 1);       // Dimensions of sub-tensor to set data for. Set to one for all indices but the ones iterated over...
         std::vector<unsigned> tensor_start_elem(nmodes, 0); // Start element of tensor (set 1 for modes in MC).
         for(In idim=I_0; idim<mcvec.size(); ++idim)
         {
            hopper_dims[mcvec.size()-idim-1] = mcvec[idim]; // We iterate over the last modes first (generalized row-major...)
            subdims[mcvec[idim]] = nmodals[mcvec[idim]] - 1;
            tensor_start_elem[mcvec[idim]] = 1;
         }
         unsigned start_idx = tensor_start_elem[0];      // Calculate offset in result (SimpleTensor)
         for(unsigned i=1; i<nmodes; ++i)
         {
            start_idx *= nmodals[i];
            start_idx += tensor_start_elem[i];
         }

//         Mout  << " Init ArrayHopper.\n"
//               << "   start_idx:    " << start_idx << "\n"
//               << "   result dims:  " << result.GetDims() << "\n"
//               << "   subdims:      " << subdims << "\n"
//               << "   hopper dims:  " << hopper_dims << "\n"
//               << std::flush;

         // Initialize ArrayHopper
         ArrayHopper<T> hopper
            (  static_cast<SimpleTensor<T>*>(result.GetTensor())->GetData()+start_idx // pointer to first element of sub-tensor
            ,  result.GetDims() // dimensions of total tensor
            ,  subdims  // dimensions of the sub-block to hop over
            ,  hopper_dims // dimensions that we iterate through
            );

         // Set elements of result
         const auto& tens_in = aIn.GetModeCombiData(imc);
         NiceTensor<T> converted_tensor;
         bool is_simple = (tens_in.Type() == BaseTensor<T>::typeID::SIMPLE);
         if (  !is_simple
            )
         {
            converted_tensor = tens_in.ToSimpleTensor();
         }
         const auto& tens_ref = is_simple ? tens_in : converted_tensor;
         
//         Mout  << " Coefs for MC:\n" << tens_ref << std::endl;

         const auto* ptr_in = tens_ref.template StaticCast<SimpleTensor<T>>().GetData();
         for(auto& elem : hopper)
         {
//            Mout  << " Counter:  " << elem
//                  << " elem:     " << elem << "\n"
//                  << "*ptr_in:   " << *ptr_in << "\n"
//                  << std::flush;
            elem = T(*(ptr_in++));
         }
      }
   }

//   Mout  << " MCR:\n" << mcr << "\n"
//         << " result:\n" << result << "\n"
//         << std::flush;


   // 4) Return result
   return result;
}

/**
 * Calculate angle between two TensorDataCont%s
 *
 * @param arT     The first TensorDataCont
 * @param arU     The second TensorDatacont
 *
 * @return        The angle between arT and arU
 **/
template
   <  typename T
   >
T Angle
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   )
{
   auto t_norm = arT.Norm();
   auto u_norm = arU.Norm();
   
   return Angle(arT, arU, t_norm, u_norm);
}

/**
 * Calculate angle between two TensorDataCont%s
 * using pre-calculated norms.
 *
 * @param arT     The first TensorDataCont
 * @param arU     The second TensorDatacont
 * @param aTnorm The norm of arT
 * @param aUnorm The norm of arU
 *
 * @return        The angle between arT and arU
 **/
template
   <  typename T
   >
T Angle
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   ,  typename GeneralTensorDataCont<T>::real_t aTnorm
   ,  typename GeneralTensorDataCont<T>::real_t aUnorm
   )
{
   auto cos = Dot(arT,arU)/(aTnorm*aUnorm);

   return std::acos(cos);
}

/**
 * Calculate absolute-valued TensorDataCont
 *
 * @param arT  The input TensorDataCont
 * @return     TensorDataCont of absolute values
 **/
template
   <  typename T
   >
GeneralTensorDataCont<T> Abs
   (  const GeneralTensorDataCont<T>& arT
   )
{
   auto result = arT;
   result.Abs();

   return result;
}


template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t Norm
   (  const GeneralTensorDataCont<T>& arT
   )
{
   return arT.Norm();
}

template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t Norm2
   (  const GeneralTensorDataCont<T>& arT
   )
{
   return arT.Norm2();
}

template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t DiffNorm2
   (  const GeneralTensorDataCont<T>& aLeft
   ,  const GeneralTensorDataCont<T>& aRight
   )
{
   using real_t = typename GeneralTensorDataCont<T>::real_t;

   real_t result = 0;

   assert(aLeft.Size() == aRight.Size());
   auto size = aLeft.Size();

//   T dot;
//   real_t lnorm2, rnorm2;
/*   private(dot, lnorm2, rnorm2)  \ */

   #pragma omp parallel for      \
   schedule(dynamic)             \
   default(none)                 \
   shared(size, aLeft, aRight)   \
   reduction(+ : result)
   for(In i=I_0; i<size; ++i)
   {
      const auto& ltens = aLeft.GetModeCombiData(i);
      const auto& rtens = aRight.GetModeCombiData(i);
		result += safe_diff_norm2(ltens.GetTensor(), rtens.GetTensor());
      //lnorm2 = ltens.Norm2();
      //rnorm2 = rtens.Norm2();
      //dot = dot_product(ltens, rtens);
      //result += diff_norm2_new(ltens.GetTensor(), lnorm2, rtens.GetTensor(), rnorm2, dot);
   }

   return result;
}

template
   <  typename T
   >
void Normalize
   (  GeneralTensorDataCont<T>& arT
   )
{
   arT.Normalize();
}

template
   <  typename T
   >
void Orthogonalize
   (  GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   )
{
   arT.Orthogonalize(arU);
}

template
   <  typename T
   ,  typename U
   >
void Scale
   (  GeneralTensorDataCont<T>& arT
   ,  U aNb
   )
{
   arT.Scale(aNb);
}

template
   <  typename T
   >
void Zero
   (  GeneralTensorDataCont<T>& arT
   )
{
   arT.Zero();
}

template
   <  typename T
   ,  typename U
   >
void Axpy
   (  GeneralTensorDataCont<T>& arY
   ,  const GeneralTensorDataCont<T>& arX
   ,  U aA
   )
{
   arY.Axpy(arX, aA);
}

template
   <  typename T
   >
size_t Size
   (  const GeneralTensorDataCont<T>& aT
   )
{
   return aT.TotalSize();
}

template
   <  typename T
   >
inline void SetShape
   (  GeneralTensorDataCont<T>& arThis
   ,  const GeneralTensorDataCont<T>& aShape
   )
{
   arThis = aShape;
   arThis.Zero();
}

//! Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   )
{
   typename GeneralTensorDataCont<PARAM_T>::real_t result(0.);

   for(In i=I_0; i<aDeltaY.Size(); ++i)
   {
      result += OdeNorm2(aDeltaY.GetModeCombiData(i), aAbsTol, aRelTol, aYOld.GetModeCombiData(i), aYNew.GetModeCombiData(i));
   }

   return result;
}

//! Mean Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeMeanNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   )
{
   return OdeNorm2(aDeltaY, aAbsTol, aRelTol, aYOld, aYNew) / aDeltaY.TotalSize();
}

//! Max Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeMaxNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   )
{
   typename GeneralTensorDataCont<PARAM_T>::real_t result(0.);

   for(In i=I_0; i<aDeltaY.Size(); ++i)
   {
      result = std::max(result, OdeNorm2(aDeltaY.GetModeCombiData(i), aAbsTol, aRelTol, aYOld.GetModeCombiData(i), aYNew.GetModeCombiData(i)));
   }

   return result;
}

//! Data to pointer
template
   <  typename T
   ,  typename U
   >
void DataToPointer
   (  const GeneralTensorDataCont<T>& aThis
   ,  U* apPtr
   )
{
   U* ptr_ref = apPtr;

   for(In i=I_0; i<aThis.Size(); ++i)
   {
      DataToPointer(aThis.GetModeCombiData(i), ptr_ref);

      size_t nelem = aThis.GetModeCombiData(i).TotalSize();

      if constexpr   (  midas::type_traits::IsComplexV<T>
                     && !  midas::type_traits::IsComplexV<U>
                     )
      {
         nelem *= 2;
      }

      ptr_ref += nelem;
   }
}

//! Data to pointer
template
   <  typename T
   ,  typename U
   >
void DataFromPointer
   (  GeneralTensorDataCont<T>& arThis
   ,  const U* apPtr
   )
{
   const U* ptr_ref = apPtr;

   for(In i=I_0; i<arThis.Size(); ++i)
   {
      DataFromPointer(arThis.GetModeCombiData(i), ptr_ref);

      size_t nelem = arThis.GetModeCombiData(i).TotalSize();

      if constexpr   (  midas::type_traits::IsComplexV<T>
                     && !  midas::type_traits::IsComplexV<U>
                     )
      {
         nelem *= 2;
      }

      ptr_ref += nelem;
   }
}
   
//! Assert same shape
template
   <  typename T
   >
void assert_same_shape
   (  const GeneralTensorDataCont<T>& aFirst
   ,  const GeneralTensorDataCont<T>& aSecond
   )
{
   MidasAssert(aFirst.Size() == aSecond.Size(), "TensorDataConts have different lengths.");
   auto size = aFirst.Size();

   for(In i=I_0; i<size; ++i)
   {
      assert_same_shape(aFirst.GetModeCombiData(i), aSecond.GetModeCombiData(i));
   }
}

//! Random TensorDataCont
template
   <  typename T
   ,  typename INT
   >
GeneralTensorDataCont<T> RandomTensorDataCont
   (  const ModeCombiOpRange& aExciRange
   ,  const std::vector<INT>& aNmodals
   ,  bool aRef
   )
{
   static_assert(std::numeric_limits<INT>::is_integer, "INT must be integer type!");

   GeneralTensorDataCont<T> result(aExciRange, aNmodals, aRef);

   // Loop and replace by random tensors
   for(In i=I_0; i<result.Size(); ++i)
   {
      auto& tens = result.GetModeCombiData(i);
      const auto& dims = tens.GetDims();

      if (  dims.empty()
         )
      {
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            using real_t = midas::type_traits::RealTypeT<T>;
            tens = NiceScalar<T>(midas::util::RandomSignedFloat<real_t>(), midas::util::RandomSignedFloat<real_t>());
         }
         else
         {
            tens = NiceScalar<T>(midas::util::RandomSignedFloat<T>());
         }
      }
      else
      {
         tens = make_nice_random_simple<T>(dims);
      }
   }

   return result;
}

#endif /* TENSORDATACONT_IMPL_H_INCLUDED */
