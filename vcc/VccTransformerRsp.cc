/**
 ************************************************************************
 * 
 * @file                VccTransformerRsp.cc
 *
 * Created:             14-01-2008
 *                      (Originally part of Vcc.cc)
 *
 * Author:              Ove Christiansen (ove@chem.au.dk)
 *                      Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementing response related functions of class
 *                      Transformer.
 * 
 * Last modified: Tue Sep 08, 2009  01:10PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */


// Standard headers:
#include<vector>
using std::vector;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "operator/OperProd.h"
#include "input/ModeCombi.h"
#include "vcc/Vcc.h"
#include "vcc/Xvec.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/VccTransformer.h"
#include "vcc/vcc2/Vcc2TransBase.h"

/**
* Transforming arDcIn by H/H0 to arDcOut in basis of 
* VCI reference and orthogonal complement. 
* */
void VccTransformer::VciRspTransH(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb)
{
   mXvec.GetXvecData().NewLabel(arDcIn.Label()+"_xvec",false); 
   mTransXvec.GetXvecData().NewLabel(arDcOut.Label()+"_transxvec",false);
   //
   Nb a_ref_comp=aNb;
   bool l_eta=false;
   if (aNb > C_0) l_eta=true;
   //if (aI==I_3)
   //{
   //   DataCont tmp(arDcIn.Size()+I_1);
   //   Nb zero=C_0;
   //   //Mout << " arDcOc " << arDcOc<< endl;
   //   arDcIn.Reassign(tmp,I_1,-I_1); // At the moment arDcPrim  = (0,oc)
   //   tmp.DataIo(IO_PUT,I_0,zero); // To make el. welldef.
   //   //Mout << " Oc in expanded format " << arDcPrim << endl;
   //   PrimOrtoTrans(arDcOut,tmp,zero,-I_1);
   //   return;
   //}
   if (aI!=I_0)
      PrimOrtoTrans(arDcIn,mXvec.GetXvecData(),a_ref_comp,I_1);

   if (gDebug || mpVccCalcDef->IoLevel() > I_5)
   {
      Mout << "VciRspTransH arDcin               Norm       " << arDcIn.Norm() << endl;
      Mout << "VciRspTransH arDcin               Label      " << arDcIn.Label() << endl;
      Mout << "VciRspTransH arDcOut              Label      " << arDcOut.Label() << endl;
      Mout << "VciRspTransH mXvec                Label      " << mXvec.GetXvecData().Label() << endl;
      Mout << "VciRspTransH mTransXvec           Label      "
           << mTransXvec.GetXvecData().Label() << endl;
      Mout << "VciRspTransH arDcin               size       " << arDcIn.Size() << endl;
      Mout << "VciRspTransH arDcOut              size       " << arDcOut.Size() << endl;
      Mout << "VciRspTransH GetXvecData()            size begin " << mXvec.GetXvecData().Size() << endl;
      Mout << "VciRspTransH mTransXvecData       size begin "
           << mTransXvec.GetXvecData().Size() << endl;
      Mout << "VciRspTransH arDcin               storage    " << arDcIn.Storage() << endl;
      Mout << "VciRspTransH arDcOut              storage    " << arDcOut.Storage() << endl;
      Mout << "VciRspTransH mXvec.GetXvecData()      storage    " << mXvec.GetXvecData().Storage() << endl;
      Mout << "VciRspTransH mTransXvec.GetXvecData() storage    " 
           << mTransXvec.GetXvecData().Storage() << endl;
      Mout << "VciRspTransH Norm Xvec : " << mXvec.GetXvecData().Norm() << endl;
      Mout << "VciRspTransH Norm arDcin: " << arDcIn.Norm() << endl;
      Mout << "XVEC IS: " << endl << mXvec.GetXvecData() << endl;
   }
   if (mpVccCalcDef->SumNprim()&&aI==I_1) SumNpurify(mXvec.GetXvecData());

   // Do transformation in primitive HP basis 
   if (aI==I_0) 
   {
      Nb one=C_1;
      arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
      mXvec.GetXvecData().DataIo(IO_PUT,I_0,one);
      if (mpVccCalcDef->GetRspTrueHDiag() && !mpVccCalcDef->GetRspTrueADiag()) TransformerTrueHDiag(aJ,aNb);
      else if (mpVccCalcDef->GetRspTrueADiag()) VciRspTransTrueAdiag(aJ,aNb);
      else TransformerH0(aJ,aNb);
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);
   }
   if (aI==I_1)
   {
      if (this->UseVcc2Trans())
      {
         const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::VCI);
         p_vcc2trans->Transform(this->Vcc2GetCCoefs(), mTransXvec.GetXvecData());
      }
      else
      {
         //Mout << "General transformer..." << endl;
         // clock_t t0 = clock();
         GenTransformer();
         // Mout << "Time: " << Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
      }
   }
 
   if (mpVccCalcDef->SumNprim()&&aI==I_1) SumNpurify(mTransXvec.GetXvecData());
   if (aI!=I_0)
      PrimOrtoTrans(arDcOut,mTransXvec.GetXvecData(),aNb,-I_1);
   // Get correct expt. val.
   // Check by seidler - new transformer framework
   if (mpOpDef != mpVcc->pOpDef())
      MIDASERROR("New trf framework. Error (1) in VciRspTransH().");
   string working_oper = mpOpDef->Name();
   Nb ref_e=C_0;
   if (working_oper == mpVcc->pOpDefOrig()->Name()) {
      ref_e=mpVcc->GetRspRefE();
   }
   else {
      for(In i_op=I_0;i_op<mpVccCalcDef->NrspOps();i_op++) {
         if ( mpVccCalcDef->GetRspOp(i_op) == working_oper && !l_eta) {
            ref_e=mpVccCalcDef->GetRspOpExpVal(i_op);
            //if(gDebug)
            Mout << "Expectation value for operator: " << working_oper << " = " << ref_e 
                 << " will be subtracted from result vector." << endl
                 << "Oper from which value has been taken: " << mpVccCalcDef->GetRspOp(i_op)
                 << endl;
            
            break;
         }
      }
   }

   if (aI==I_0) ref_e=-ref_e;
   if(gDebug)
      Mout << "Will subtract: " << ref_e << " from vector..." << endl;
   arDcOut.Axpy(arDcIn,-ref_e); // -<0|OP|0>delta_ij*c_j 

   if (gDebug || mpVccCalcDef->IoLevel() > I_5)
      Mout << "VciRspTransH: Norm TransXvec : " << mTransXvec.GetXvecData().Norm() << endl
           << "VciRspTransH: Norm arDcOut: "  << arDcOut.Norm() << endl;
}

/**
* MBH:
* Transforming arDcIn by H to arDcOut in basis of 
* VCI reference and orthogonal complement. 
* add aNb to result afterwards.
* */
void VccTransformer::VciRspTransF(DataCont& arDcIn,DataCont& arDcOut, Nb& aNb)
{
   if(gDebug)
      Mout << "In VccTransformer::GeneralFTransformation" << endl;
   mXvec.GetXvecData().NewLabel(arDcIn.Label()+"_xvec",false); 
   mTransXvec.GetXvecData().NewLabel(arDcOut.Label()+"_transxvec",false);
   //
   Nb a_ref_comp=C_0;
   PrimOrtoTrans(arDcIn,mXvec.GetXvecData(),a_ref_comp,I_1);

   if (mpVccCalcDef->SumNprim()) SumNpurify(mXvec.GetXvecData());

   // Do transformation in primitive HP basis 
   if (this->UseVcc2Trans())
   {
      const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::VCI);
      p_vcc2trans->Transform(Vcc2GetCCoefs(), mTransXvec.GetXvecData());
   }
   else
   {
      //Mout << "General transformer..." << endl;
      // clock_t t0 = clock();
      GenTransformer();
      // Mout << "Time: " << Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
   }

   Nb ref_e=C_0;
   if (mpOpDef->Name() == mpVcc->pOpDefOrig()->Name())
      ref_e=mpVcc->GetRspRefE();
   if (mpVccCalcDef->SumNprim()) SumNpurify(mTransXvec.GetXvecData());
   PrimOrtoTrans(arDcOut,mTransXvec.GetXvecData(),a_ref_comp,-I_1);
   arDcOut.Axpy(arDcIn,-ref_e+aNb); // -aNb*delta_ij*<0|OP|0>
}
/**
* Transforming arDcIn by the exact H diagonal to arDcOut in basis of 
* VCI reference and orthogonal complement.
* The diagonal of the Hamiltonian matrix is defined as 
* \f[ 
*  \mathbf{H}_{\rm diag}^{\rm oc} = \mathbf{H}_{\rm diag}^{\rm p} -
* \frac{2}{1+c_0} \mathbf{c} \otimes \mathbf{v}  + \frac{v_0 +
* \mathbf{c} \mathbf{v}}{(1+c_0)^2} \mathbf{c} \otimes \mathbf{c}
* \f]
* where the elements of the \f$\mathbf{v}\f$ vector are \f$v_i = c_i  + \delta_{0i}\f$,
* and \f$\mathbf{c}\f$ is the VCI solution vector.
**/
void VccTransformer::VciRspTransTrueAdiag(const In& aJ, const Nb& aNb)
{
   // Let's denote the vector to be transformed as b
   Mout << "Vcc::VciRspTransTrueAdiag diagonal transformer" << endl;

   DataCont c_vec;
   In i_ref=I_0;
   string vecname = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i_ref); // existing VCI solution vec name
   c_vec.GetFromExistingOnDisc(mXvec.GetXvecData().Size(),vecname);
   c_vec.SaveUponDecon();
   DataCont v_vec(c_vec);
   Nb val;
   c_vec.DataIo(IO_GET,I_0,val);
   Nb c0 = val;
   Nb v0 = c0+C_1;
   v_vec.DataIo(IO_PUT,I_0,v0);
   Nb const1 = C_2/(C_1+c0); 
   Nb const2 = -(v0+Dot(c_vec,v_vec))/((C_1+c0)*(C_1+c0));
   val = C_0;
   TransformerTrueHDiag(aJ,val); // mTransXvec = -Hdiag*vec_of_ones
   v_vec.DirProd(c_vec,C_0,I_1);// Now v_vec should be v_vec \otimes c_vec 
   mTransXvec.GetXvecData().Axpy(v_vec,const1);  // Add the first term and obtain mTransXvec = -Hdiag + Term_1 
   DataCont tmp(c_vec); //tmp=c_vec 
   tmp.DirProd(c_vec,C_0,I_1);// Now tmp should be c_vec \otimes c_vec 
   mTransXvec.GetXvecData().Axpy(tmp,const2);  //  Add the second term and obtain arDcOut = -Hdiag + Term_1 +Term_2
   // mTransXvec is -<Psi_k|H|Psi_k>
}

/**
* Primitive to orthogonal transformer 
*   if (aI>I_0) // oc to prim
*   else if (aI<I_0) // prim to oc
**/
void VccTransformer::PrimOrtoTrans(DataCont& arDcOc,DataCont& arDcPrim,Nb& arRefComp,In aI)
{
   In nvecsize = mTransXvec.Size();
   DataCont ref_vec;
   In i_ref=I_0;
   string vecname = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i_ref);
   ref_vec.GetFromExistingOnDisc(nvecsize,vecname);
   ref_vec.SaveUponDecon();
   Nb c=C_NB_MAX;
   Nb zero=C_0;
   ref_vec.DataIo(IO_GET,I_0,c);
   //Mout << " Vcc::PrimOrtoTrans: Reference state HP component " << c << endl;
   //Mout << " Vcc::PrimOrtoTrans: Reference state vec " << ref_vec << endl;
   if (c<=C_0) 
   {
      Mout << "Reference c= "<< c << endl;
      MIDASERROR("PrimOrtoTrans only applies for reference state comp > 0");
   } 
   if (c<=C_NB_EPSILON*C_10_2) MidasWarning("In vci_rsp reference state comp close to zero 0");

   if (nvecsize!=arDcOc.Size()+I_1) MIDASERROR("Did I misunderstand something");
   if (aI>I_0) // oc to prim
   {
      //Mout << " Vcc::PrimOrtoTrans: arRefComp inp " << arRefComp << endl;
      //Mout << " arDcOc " << arDcOc<< endl;
      arDcOc.Reassign(arDcPrim,I_1,-I_1); // At the moment arDcPrim  = (0,oc)
      arDcPrim.DataIo(IO_PUT,I_0,zero); // To make el. welldef.
      //Mout << " Oc in expanded format " << arDcPrim << endl;
      Nb x=Dot(arDcPrim,ref_vec);
      Nb m_x=-x;
      Nb m_x_over_1_p_c = m_x/(1+c);
      Nb plus_ref = m_x_over_1_p_c + arRefComp;
      //Mout << " x=" << x << " ,m_x= "<< m_x << ", m_x_over_1_p_c = " << m_x_over_1_p_c << endl;
      arDcPrim.Axpy(ref_vec,plus_ref); // b_k - x/(1+c)*c_k + b_0*c_k 
      Nb ref_comp = m_x+arRefComp*c;
      arDcPrim.DataIo(IO_PUT,I_0,ref_comp); // (-x+c*b_0,b_k-x/(1+c)*c_k  
      //Mout << " Resulting primi " << arDcPrim << endl;
   }
   else if (aI<I_0) // prim to oc
   {
      arRefComp = Dot(arDcPrim,ref_vec);
      //Mout << " Vcc::PrimOrtoTrans: arRefComp out " << arRefComp << endl;
      Nb a_0;
      arDcPrim.DataIo(IO_GET,I_0,a_0);
      Nb y = arRefComp-c*a_0;
      //Nb fac = -(a_0+y)/(C_1+c);
      Nb fac = -(y/(C_1+c)+a_0);
      //Mout << " arDcPrim before axpy " << arDcPrim<< endl;
      arDcPrim.Axpy(ref_vec,fac); // a_k - c_k/(1+c)*(a_0+y) 
      //Mout << " arDcPrim after axpy " << arDcPrim<< endl;
      arDcOc.Reassign(arDcPrim,I_1,I_1); 
      //Mout << " arDcOc final " << arDcOc<< endl;
   }
   else MIDASERROR("Undefined task in Vcc::PrimOrtoTrans ");
}

/**
*  Direct transformation of arDcIn with VCC Jacobian.
*  If aI == 0 use zeroth order Hamiltonian. Othewise use full H.
**/
void VccTransformer::VccRspTransJacobian(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, Nb& arNb, bool aRight)
{
   if (gDebug || mpVccCalcDef->RspIoLevel() > I_5)
   {
      Mout << "In Vcc::VccRspTransJacobian():" << endl
           << "      arDcin         Label      " << arDcIn.Label() << endl
           << "      arDcOut        Label      " << arDcOut.Label() << endl
           << "      arDcin         size       " << arDcIn.Size() << endl
           << "      arDcOut        size       " << arDcOut.Size() << endl
           << "      arDcin         storage    " << arDcIn.Storage() << endl
           << "      arDcOut        storage    " << arDcOut.Storage() << endl
           << "                     aI         " << aI << endl
           << "                     aJ         " << aJ << endl
           << "      mXvec          size       " << mXvec.Size() << endl
           << "      mTransXvec     size       " << mTransXvec.Size() << endl;
   }
   
   // If aI = 0 we use the 0th order Hamiltonian.
   if (I_0 == aI)
   {
      if (gDebug || mpVccCalcDef->RspIoLevel() < I_5)
         Mout << "      Transform using 0th order Hamiltonian"
              << "         arDcIn.Size() =     " << arDcIn.Size() << endl
              << "         mXvec.Size()  =     " << mXvec.GetXvecData().Size() << endl
              << "         mTransXvec.Size() = " << mTransXvec.GetXvecData().Size() << endl;
      arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
      if (mpVccCalcDef->GetRspTrueHDiag()) 
      {
         TransformerTrueHDiag(aJ,arNb);
      }
      else 
      {
         TransformerH0(aJ,arNb);
      }
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);
      Nb e_vcc = mpVcc->GetRspRefE();
      arDcOut.Axpy(arDcIn, e_vcc);
      return;
   }

   // If mpExpTXvec is not initialized, assume that mTransXvec can be used.
   if (NULL == mpExpTXvec)
      mpExpTXvec = &mTransXvec;

   if (I_0 == aI && !aRight)
   {
      MIDASERROR("VccTransformer::VccRspTransJacobian(): aI==0 not supported with left transformation.");
   }
   
   // Get VCC amplitudes for ground state.
   string s = mpVccCalcDef->GetName() + "_Vcc_vec_0";
   In n_amp = mpExpTXvec->Size() - I_1;
   DataCont tmp_amp_dc;
   tmp_amp_dc.GetFromExistingOnDisc(n_amp, s);
   tmp_amp_dc.SaveUponDecon();

   // 
   //To check terms: custom amplitudes
   //
   //CustomAmp(tmp_amp_dc); 

   // Storage mode OnDisc to save mem?
   
   // We need to put the reference at index zero in input, output and amplitude
   // vectors to get correspondence with Xvec addresses.
   DataCont amp_dc(n_amp + I_1);                 // Reference index is zero since we don't want
   tmp_amp_dc.Reassign(amp_dc, I_1, -I_1);       // a contribution.

   // If needed, perform T1 transformation of integrals.
   // In principle it's stupid to do this in every iteration.
   // (The VCC[2] transformer will take care of T1-trans on its own, and will
   // always take the raw modal integrals for construction.)
   if (mT1TransH && !this->UseVcc2Trans())
   {
      if (mpVccCalcDef->IoLevel() > I_5)
         Mout << " T1 transforming integrals." << endl;
      amp_dc.Reassign(mXvec.GetXvecData(), I_0, -I_1);
      UpdateModalIntegrals(true, &mXvec);
   }
   else
      UpdateModalIntegrals(false);
   
   arDcIn.Reassign(mTransXvec.GetXvecData(), I_1, -I_1);
   Nb zero = C_0;
   mTransXvec.GetXvecData().DataIo(IO_PUT, I_0, zero);
   DataCont temp_out_dc(n_amp +I_1);
   temp_out_dc.NewLabel("tmp");

   if (this->UseVcc2Trans())
   {
      amp_dc.Reassign(mXvec.GetXvecData(), I_0, -I_1); // Amps in mXvec as supposed by transformer.
      if (aRight)
      {
         const auto p_vcc2trans = FactoryVcc2Trans(VCC2RSP_T::RJAC);
         p_vcc2trans->Transform(Vcc2GetRCoefs(), arDcOut);
      }
      else
      {
         const auto p_vcc2trans = FactoryVcc2Trans(VCC2RSP_T::LJAC);
         p_vcc2trans->Transform(Vcc2GetLCoefs(), temp_out_dc);
         Mout << " after VccRspTransJacobianTwoModeL()" << endl;
      }
   }
   else
   {
      if (aRight)
      {
         ExpT(mTransXvec, mTransXvec.GetXvecData(), mXvec, mXvec.GetXvecData(), *mpExpTXvec, amp_dc, false);
         mVciVcc = false;
         GenTransformer();
         DataCont tmp(mTransXvec.GetXvecData());    // Output from transformer is input for exp(-T).
         ExpT(mTransXvec, tmp, mTransXvec, mTransXvec.GetXvecData(), *mpExpTXvec, amp_dc, true);

         //Copy to output without reference element.
         arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);

         // Subtract c_mu * E_VCC.
         Nb e_vcc = mpVcc->GetRspRefE();
         arDcOut.Axpy(arDcIn, -e_vcc);
      }
      else
      {
         Mout << " VCC response: Using two-mode left Jacobian transformer anyway." << endl;
         amp_dc.Reassign(mXvec.GetXvecData(), I_0, -I_1); // Amps in mXvec as supposed by transformer.
         const auto p_vcc2trans = FactoryVcc2Trans(VCC2RSP_T::LJAC);
         p_vcc2trans->Transform(Vcc2GetLCoefs(), temp_out_dc);
         Mout << " after VccRspTransJacobianTwoModeL" << endl;
      }
   }
   if (!aRight) arDcOut.Reassign(temp_out_dc,I_1,I_1,C_0,-I_1);

   // Restore mpExpTXvec value.
   if (&mTransXvec == mpExpTXvec)
      mpExpTXvec = NULL;

   if (gDebug)
      Mout << " Norm of arDcOut: " << arDcOut.Norm() << endl;
}

/**
 * Transform arDcIn with VCC Jacobian stored row by row on disc and save result in ArDcOut.
 * If aI == 0 use zeroth order Hamiltonian. Otherwise use full H.
 **/
void VccTransformer::VccRspTransJacobianOnDisc(DataCont& arDcIn,DataCont& arDcOut,
      In aI, In aJ, Nb& arNb)
{
   if (gDebug || mpVccCalcDef->RspIoLevel() > I_5)
   {
      Mout << "In Vcc::VccRspTransJacobianOnDisc():" << endl
           << "      arDcin         Label      " << arDcIn.Label() << endl
           << "      arDcOut        Label      " << arDcOut.Label() << endl
           << "      arDcin         size       " << arDcIn.Size() << endl
           << "      arDcOut        size       " << arDcOut.Size() << endl
           << "      arDcin         storage    " << arDcIn.Storage() << endl
           << "      arDcOut        storage    " << arDcOut.Storage() << endl
           << "                          aI    " << aI << endl
           << "                          aJ    " << aJ << endl;
   }

   // If aI = 0 we are only supposed to use 0th order Hamiltonian.
   if (I_0 == aI)
   {
      if (gDebug || mpVccCalcDef->RspIoLevel() > I_5)
         Mout << "      Transform using 0th order Hamiltonian"
            << "         arDcIn.Size() =     " << arDcIn.Size() << endl
            << "         mXvec.Size()  =     " << mXvec.GetXvecData().Size() << endl
            << "         mTransXvec.Size() = " << mTransXvec.GetXvecData().Size() << endl;
      arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
      
      if (mpVccCalcDef->GetRspTrueHDiag())
      {
         TransformerTrueHDiag(aJ,arNb);
      }
      else
      {
         TransformerH0(aJ,arNb);
      }
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);
      return;
   }

   string s = mpVccCalcDef->GetName() + "_Vcc_Jacobian";
   In vecsize = mTransXvec.Size() - I_1;

   DataCont A_dc;
   A_dc.GetFromExistingOnDisc(vecsize*vecsize, s);
   A_dc.SaveUponDecon();
   A_dc.TransformMx(vecsize, vecsize, arDcIn, arDcOut);
}

void VccTransformer::CalcVccRspEta0(DataCont& aRes)
{
   if (mpVccCalcDef->TwoModeTrans())
   {
      const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::ETA);
      p_vcc2trans->Transform(this->Vcc2GetTAmps(), aRes);
   }
   else
   {
      MIDASERROR("VccTransformer::CalcVccRspEta0(): Only supported for two-mode transformer.");
   }
}
