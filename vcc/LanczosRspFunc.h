/**
************************************************************************
* 
* @file                LanczosRspFunc.h
*
* Created:             06-08-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Interfaces for Lanczos type response functions.
* 
* Last modified: Tue Nov 10, 2009  04:03PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSRSPFUNC_H
#define LANCZOSRSPFUNC_H 

#include <map>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosChainDefBase.h"
#include "input/RspFunc.h"

///> Put some constants here for lack of a better place.
const In LANCZOS_ERROR          =  0;
const In LANCZOS_ORTHO_NONE     =  1;
const In LANCZOS_ORTHO_FULL     =  2;
const In LANCZOS_ORTHO_PERIODIC =  3;
const In LANCZOS_ORTHO_PARTIAL  =  4;
const In LANCZOS_SOLVER_LIN     = 16;
const In LANCZOS_SOLVER_EIG     = 32;
const In LANCZOS_SOLVER_CF      = 64;

class LanczosChainDef;
class LanczosChain;
class NHermLanczosChainDef;
class NHermLanczosChain;
class BandLanczosChainDef;
class BandLanczosChain;
class NHBandLanczosChainDef;
class NHBandLanczosChain;
class Vcc;
class VccCalcDef;
class VccTransformer;

class LanczosRspFunc
{
   protected:
      string                        mName;         ///< Identifier for response function.
                                                   ///< Should also used as prefix for generated files.
                           
      Vcc*                          mpVcc;         ///< Pointer to Vcc class.
      VccCalcDef*                   mpVccCalcDef;  ///< Pointer to VCC calculation definition.
      VccTransformer*               mpTrf;         ///< Pointer to transformer object.
      vector<LanczosChain>*         mpChains;      ///< Vector of Lanczos chains at our disposal.
      vector<NHermLanczosChain>*    mpChainsNH;
      vector<BandLanczosChain>*     mpChainsBand;
      vector<NHBandLanczosChain>*   mpChainsNHBand;
      bool                          mBand;
      bool                          mNHerm;
      string                        mType;
      
      const LanczosChain* FindChain(const LanczosChainDef& aDef) const;
      const NHermLanczosChain* FindChainNH(const NHermLanczosChainDef& aDef) const;
      const BandLanczosChain* FindChain(const BandLanczosChainDef& aDef) const;
      const NHBandLanczosChain* FindChain(const NHBandLanczosChainDef& aDef) const;
      ///< Utility function. Returns index in aChains which is compatible
      ///< with aDef. If no chain is found, -1 is returned.
      
      /* Static members for translating between integer and string representations of
       * various Lanczos related constants. */
      static std::map<In, string> mConstStringMap;
      static std::map<string, In> mStringConstMap;
      
   public:
      static LanczosRspFunc* Factory(const In aType, const std::map<string,string>& aParams, 
                                     const bool aBand, const bool aNHerm);
      ///< Factory for generating Lanczos response functions.
      
      LanczosRspFunc(const std::map<string,string>& aParams);
      LanczosRspFunc(const std::map<string,string>& aParams, const string aType, 
                     const bool aBand, const bool aNHerm);
      ///< Takes care of mName initialization.

      virtual ~LanczosRspFunc() {};

      void Initialize(Vcc* apVcc, VccCalcDef* apVccCalcDef, VccTransformer* apTrf,
                       vector<LanczosChain>* apChains)
      {mpVcc=apVcc; mpVccCalcDef=apVccCalcDef; mpTrf=apTrf; mpChains=apChains;}
      void Initialize(Vcc* apVcc, VccCalcDef* apVccCalcDef, VccTransformer* apTrf,
                       vector<NHermLanczosChain>* apChains)
      {mpVcc=apVcc; mpVccCalcDef=apVccCalcDef; mpTrf=apTrf; mpChainsNH=apChains;}
      void Initialize(Vcc* apVcc, VccCalcDef* apVccCalcDef, VccTransformer* apTrf,
                       vector<BandLanczosChain>* apChains)
      {mpVcc=apVcc; mpVccCalcDef=apVccCalcDef; mpTrf=apTrf; mpChainsBand=apChains;}
      void Initialize(Vcc* apVcc, VccCalcDef* apVccCalcDef, VccTransformer* apTrf,
                       vector<NHBandLanczosChain>* apChains)
      {mpVcc=apVcc; mpVccCalcDef=apVccCalcDef; mpTrf=apTrf; mpChainsNHBand=apChains;}

      string GetType() {return mType;}
      ///< Set values of internal variables which may be needed by some kinds of response
      ///< functions.
      
      virtual const string& GetName() const     {return mName;}
      virtual bool GetBand() const              {return mBand;}
      virtual bool GetNHerm() const             {return mNHerm;}
      virtual void SetName(const string& aS)    {mName = aS;}
      virtual void SetNHerm(const bool& aNHerm) {mNHerm = aNHerm;}

      virtual vector<LanczosChainDefBase*> GetChains(string& aString)            const = 0;
      //virtual void GetChains(vector<LanczosChainDef>& aDefs)        const = 0;
      //virtual void GetChains(BandLanczosChainDef& aDef)             const = 0;
      ///< Add the Lanczos chains required by response function to aDefs.
      ///< These will then be evaluated by the Lanczos response driver.  
      
      virtual void Evaluate() = 0;
      ///< Evaluate the response function. All available chains are provided in aChains.
      ///< Use the FindChain member function to locate the specific chains needed.

      virtual std::ostream& Print(std::ostream& aOut) const = 0;
      friend std::ostream& operator<<(std::ostream& aOut, const LanczosRspFunc& aObj);

      static void InitConstStringMaps();
      ///< Intialixe static maps.
      
      static In ConstForString(const string& aS);
      ///< Return value for constant identidier.
      
      static const string& StringForConst(In aC);
      ///< Return string identifier for constant.

      virtual vector<RspFunc> ConvertToRspFunc() const = 0;
      virtual void SaveCombinedRspFunc() const {};
};

#endif // LANCZOSRSPFUNC_H
