/**
************************************************************************
* 
* @file                BandLanczosLinRspFunc.cc
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen  (mrgodtliebsen@hotmail.com)
*
* Short Description:   Implementation of BandLanczosLinRspFunc class.
* 
* Last modified: Thu Dec 09, 2010  04:30PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <utility>
#include <map>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "vcc/BandLanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/BandLanczosLinRspFunc.h"
#include "mmv/MidasMatrix.h"
#include "input/CombinedRspFunc.h"
#include "analysis/ResponseAnalysisInterface.h"

// using declarations
using std::pair;
using std::map;
using std::string;

BandLanczosLinRspFunc::BandLanczosLinRspFunc
   (  const map<string,string>& aParams
   ,  const bool aBand
   ,  const bool aNHerm
   )
   :  LanczosRspFunc(aParams,"BandLanczosLinRsp",aBand,aNHerm)
{
   if(gIoLevel > 5)
   {
      Mout << " BandLanczosLinRspFunc::BandLanczosLinRspFunc()" << endl;
      Mout << "      Parameters:" << endl;

      for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
      {
         Mout << "    " << it->first << " -> " << it->second << endl;
      }
   }

   InitOperNames(aParams);
   InitFrqRange(aParams);
   InitGamma(aParams);
   InitChainLen(aParams);
   InitBlockSize(aParams);
   InitOrtho(aParams);
   InitDefThresh(aParams);
   InitSolver(aParams);
   InitSaveChain(aParams);
   InitSaveInterval(aParams);
   InitTestChain(aParams);
   InitIncludeIm(aParams);
   InitIncludeFTerm(aParams);
   InitImBiOrtho(aParams);
   InitDeflate(aParams);
   InitConvThresh(aParams);
   InitConvVal(aParams);

   mSolvedEig = false;
   mHasImEigVals = false;
}

void BandLanczosLinRspFunc::ConstructionError(const string aMsg,
                                          const map<string,string>& aParams)
{
   Mout << " BandLanczosLinRspFunc construction error:" << endl
   << aMsg << endl << endl
   << " Constructor parameters:" << endl;
   for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;
   MIDASERROR("");
}


void BandLanczosLinRspFunc::InitOperNames(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("OPERS");
   if(it == aParams.end())
      ConstructionError(" No operators specified.", aParams);
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-I_1] != ')')
      ConstructionError(" Operators must be enclosed in (...).", aParams);
   string opstr = s.substr(I_1,s.size()-I_2);
   vector<string> ops = SplitString(opstr, ",");

   for(In i=0; i<ops.size(); i++)
   {
      mOpers.push_back(ops[i]);
   }
}

void BandLanczosLinRspFunc::InitFrqRange(const map<string,string>& aParams)
{
   // If FRQ specified just initialize a single frequency value.
   map<string,string>::const_iterator it = aParams.find("FRQ");
   if (it != aParams.end())
   {
      istringstream is(it->second);
      is >> mFrqStart;
      mFrqStart /= C_AUTKAYS;
      mFrqEnd = mFrqStart;
      mFrqStep = C_0;
      return;
   }
   
   // If FRQ_RANGE is specified initialize a range
   it = aParams.find("FRQ_RANGE");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout  << " No FRQ or FRQ_RANGE keyword set, so I'm using default values: " << endl
               << "     FrqStart = 0 cm^{-1}" << endl
               << "     FrqEnd   = 4500 cm^{-1} " << endl
               << "     FrqStep  = 1 cm^{-1}" << endl;
      }

      mFrqStart = C_0/C_AUTKAYS;
      mFrqEnd = 4500/C_AUTKAYS;
      mFrqStep = C_1/C_AUTKAYS;
      return;
   }
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-I_1] != ')')
      ConstructionError(" Frequency range must be enclosed in (...).", aParams);
   string frqstr = s.substr(I_1,s.size()-I_2);
   vector<string> frqs = SplitString(frqstr, ":");
   if(frqs.size() != 3)
      ConstructionError(" Frequency range must be specified as (start:end:step).", aParams);
   istringstream is(frqs[0]);
   is >> mFrqStart;
   is.clear();
   is.str(frqs[1]);
   is >> mFrqEnd;
   is.clear();
   is.str(frqs[2]);
   is >> mFrqStep;

   mFrqStart /= C_AUTKAYS;
   mFrqEnd /= C_AUTKAYS;
   mFrqStep /= C_AUTKAYS;
}

void BandLanczosLinRspFunc::InitGamma(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("GAMMA");
   if (it == aParams.end())
   {      
      if(gIoLevel > 5) 
      {
         Mout  << " No GAMMA keyword set, so I'm using default values: " << endl
               << "     Gamma = 5 cm^{-1}" << endl;
      }

      mGamma = C_5/C_AUTKAYS;
      return;
   }   
   istringstream is(it->second);
   is >> mGamma;
   mGamma /= C_AUTKAYS;
}

void BandLanczosLinRspFunc::InitChainLen(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("CHAINLEN");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No CHAINLEN keyword set, so I'm using default values: " << endl
              << "      ChainLength = 250 " << endl;
      }

      mChainLen = 250;
      return;
   }
   istringstream is(it->second);
   is >> mChainLen;
}

void BandLanczosLinRspFunc::InitBlockSize(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("BLOCK");
   if(it == aParams.end())
   {
      if(mNHerm)
      {
         map<string,string>::const_iterator itr = aParams.find("RBLOCK");
         if(itr == aParams.end())
         {
            if(gIoLevel > 5) 
            {
               Mout << " No RBLOCK keyword set, so I'm using default values: " << endl
                    << "      RBlock = #operators = " << mOpers.size() << endl;
            }

            mBlockSize = mOpers.size();
         }
         else
         {
            istringstream isr(itr->second);
            isr >> mBlockSize;
         }
         map<string,string>::const_iterator itl = aParams.find("LBLOCK");
         if(itl == aParams.end())   
         {
            if(gIoLevel > 5) 
            {
               Mout << " No LBLOCK keyword set, so I'm using default values: " << endl
                    << "      LBlock = #operators = " << mOpers.size() << endl;
            }

            mLBlockSize = mOpers.size();
            return;
         }
         else
         {
            istringstream isl(itl->second);
            isl >> mLBlockSize;
            return;
         }
      }
      else
      {
         if(gIoLevel > 5) 
         {
            Mout << " No BLOCK keyword set, so I'm using default values: " << endl
                 << "      Block = #operators = " << mOpers.size() << endl;
         }

         mBlockSize = mOpers.size();
         mLBlockSize = mOpers.size();
         return;
      }
   }

   istringstream is(it->second);
   is >> mBlockSize;
   if(mNHerm)
      mLBlockSize = mBlockSize;
}

void BandLanczosLinRspFunc::InitOrtho(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("ORTHO");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No ORTHO keyword set, so I'm using default values: " << endl
              << "      Ortho = \"FULL\"     [1]" << endl;
      }

      mOrthoChain = 1;
      return;
   }
   if(it != aParams.end())
   {
      string s = "ORTHO_" + it->second;
      
      if(ConstForString(s) == LANCZOS_ERROR)
         ConstructionError(" Unknown ORTHO parameter.", aParams);

      mOrthoChain = LanczosRspFunc::ConstForString(s);
   }
}

void BandLanczosLinRspFunc::InitDefThresh(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("DEFTHRESH");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No DEFTHRESH keyword set, so I'm using default values: " << endl
              << "      DefThresh = " << C_10*C_NB_EPSILON <<  "    (10 X Computer precision)" <<endl;
      }
      
      mDefThresh = C_10*C_NB_EPSILON;
      return;
   }

   istringstream is(it->second);
   is >> mDefThresh;
}

void BandLanczosLinRspFunc::InitSolver(const map<string,string>& aParams)
{
   if(gIoLevel > 5) 
   {
      Mout << " BandLanczosLinRspFunc::InitSolver(): " << endl
           << "      There is only one solver implementet " << endl
           << "      at the moment, so I'm redundant. " << endl;
   }
}

void BandLanczosLinRspFunc::InitSaveChain(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("SAVECHAIN");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No SAVECHAIN keyword set, so I'm using default values: " << endl
              << "      SaveChain = \"FALSE\"" << endl;
      }

      mSaveChain = false;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mSaveChain=true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      mSaveChain=false;
   }        
   else
   {
      ConstructionError(" Unknown SAVECHAIN parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitSaveInterval(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("SAVEINTERVAL");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No SAVEINTERVAL keyword set, so I'm using default values: " << endl
              << "      SaveInterval = \"0\"" << endl;
      }

      mSaveInterval = 0;
      return;
   }
   
   istringstream is(it->second);
   is >> mSaveInterval;
}

void BandLanczosLinRspFunc::InitTestChain(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("TESTCHAIN");
   if(it == aParams.end())
   {
      mTestChain = false;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mTestChain=true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      mTestChain=false;
   }        
   else
   {
      ConstructionError(" Unknown TESTCHAIN parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitIncludeIm(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("INCLUDEIM");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No INCLUDEIM keyword set, so I'm using default values: " << endl
              << "      IncludeIm = \"TRUE\"" << endl;
      }

      mIncludeIm = true;
      mImBiOrtho = true;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mIncludeIm = true;
      mImBiOrtho = true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      mIncludeIm = false;
   }        
   else
   {
      ConstructionError(" Unknown INCLUDEIM parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitIncludeFTerm(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("INCLUDEFTERM");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No INCLUDEFTERM keyword set, so I'm using default values: " << endl
              << "      IncludeFTerm = \"TRUE\"" << endl;
      }

      mIncludeFTerm = true;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mIncludeFTerm=true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      mIncludeFTerm=false;
   }        
   else
   {
      ConstructionError(" Unknown INCLUDEFTERM parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitImBiOrtho(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("IMBIORTHO");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No IMBIORTHO keyword set, so I'm using default values: " << endl
              << "      ImBiOrtho = \"TRUE\"" << endl;
      }

      mImBiOrtho = true;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mImBiOrtho=true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      if(mIncludeIm)
         Mout << " Warning! IncludeIm=true, but ImBiOrtho=false. May yield wierd results!" << endl;
      mImBiOrtho=false;
   }        
   else
   {
      ConstructionError(" Unknown IMBIORTHO parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitDeflate(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("DEFLATE");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No DEFLATE keyword set, so I'm using default values: " << endl
              << "      Deflate = \"TRUE\"" << endl;
      }

      mDeflate = true;
      return;
   }
   else if(it != aParams.end() && it->second=="true")
   {
      mDeflate=true;
   }
   else if(it !=aParams.end() && it->second=="false")
   {
      mDeflate=false;
   }        
   else
   {
      ConstructionError(" Unknown DEFLATE parameter.", aParams);
   }
}

void BandLanczosLinRspFunc::InitConvThresh(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("CONVTHRESH");
   if(it == aParams.end())
   {
      if(gIoLevel > 5) 
      {
         Mout << " No CONVTHRESH keyword set, so I'm using default values: " << endl
              << "      ConvThresh = " << C_0 << endl;
      }
      
      mConvThresh = C_0;
      return;
   }

   istringstream is(it->second);
   is >> mConvThresh;
}

void BandLanczosLinRspFunc::InitConvVal(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("CONVVAL");
   if(it == aParams.end())
   {
      if(mConvThresh != C_0)
      {
         if(gIoLevel > 5) 
         {
            Mout << " No CONVVAL keyword set, so I'm using default values: " << endl
                 << "      ConvVal = " << 1000/C_AUTKAYS << endl;
         }
      }

      mConvVal = 1000/C_AUTKAYS;
      return;
   }

   istringstream is(it->second);
   is >> mConvVal;
   mConvVal /= C_AUTKAYS;
}

vector<LanczosChainDefBase*> BandLanczosLinRspFunc::GetChains(string& aString) const
{
   vector<LanczosChainDefBase*> aDefs;
   aDefs.push_back(LanczosChainDefBase::Factory(mNHerm, mChainLen, mBlockSize, mOrthoChain, mSaveChain, mSaveInterval, mTestChain, mDefThresh, mLBlockSize, mDeflate, mConvThresh, mConvVal));
   for(In i=0; i<mOpers.size(); i++)
      aDefs.back()->AddOper(mOpers[i]);

   if(!mNHerm)
      aString = "bandlan";
   else
      aString = "bandlan_nh";
   
   return aDefs;
}
   
void BandLanczosLinRspFunc::DoEigSolutions()
{
   if(mSolvedEig)
   {
      return;
   }
   else if(!mNHerm)
   {
      mReEigVals.SetNewSize(mChainLen);
      mReRightEigVecs.SetNewSize(mChainLen, mChainLen);
      mReEigVals.Zero();
      mReRightEigVecs.Zero();

      BandLanczosChainDef chaindef(mChainLen, mBlockSize, mOrthoChain,mSaveChain,mTestChain,mDefThresh);
         for(In i=0; i<mOpers.size(); i++)
            chaindef.AddOper(mOpers[i]);

      const BandLanczosChain* chain = FindChain(chaindef);
      chain->BandDiag(mChainLen, mReEigVals, mReRightEigVecs);

      mSolvedEig=true;
      return;
   }
   else
   {
      mReEigVals.SetNewSize(mChainLen);
      mImEigVals.SetNewSize(mChainLen);
      mReRightEigVecs.SetNewSize(mChainLen, mChainLen);
      mReLeftEigVecs.SetNewSize(mChainLen, mChainLen);
      mReEigVals.Zero();
      mImEigVals.Zero();
      mReRightEigVecs.Zero();
      mReLeftEigVecs.Zero();

      NHBandLanczosChainDef chaindef(mChainLen, mBlockSize, mLBlockSize, mOrthoChain, mSaveChain, mSaveInterval, mDefThresh);
         for(In i=0; i<mOpers.size(); i++)
            chaindef.AddOper(mOpers[i]);

      const NHBandLanczosChain* chain = FindChain(chaindef);
      chain->NHBandDiag(mChainLen, mReRightEigVecs, mReEigVals, mImEigVals, mReLeftEigVecs);
      
      CheckForImEigVals();
      BiOrthoEigVecs();
      
      mSolvedEig=true;
      return;
   }
}

void BandLanczosLinRspFunc::BiOrthoEigVecs()
{
   if(!mImBiOrtho)
   {
      for(In i=I_0; i<mReRightEigVecs.Ncols(); ++i)
      {
         Nb norm = C_0;
         for(In j=I_0; j<mReRightEigVecs.Nrows(); ++j)
            norm += mReRightEigVecs[j][i]*mReLeftEigVecs[j][i];
         
         // only scale left vectors. If this gives numerical problems then scale both with sqrt(norm).
         mReLeftEigVecs.ScaleCol(C_1/norm,i);
         /*mReRightEigVecs.ScaleCol(C_1/sqrt(norm),i);
         mReLeftEigVecs.ScaleCol(C_1/sqrt(norm),i);*/
      }
   }
   else
   {
      for(In i=I_0; i<mReRightEigVecs.Ncols(); ++i)
      {
         if(mImEigVals[i] != C_0)
         {
            if((fabs(mGamma-mImEigVals[i])/mGamma)<1e-3)
            {
               Mout << "Warning Im part of eigval: " << mReEigVals[i] << ": " << mImEigVals[i] << " is CLOSE TO GAMMA! " << endl;
               Mout << "This is at: " << mReEigVals[i]*C_AUTKAYS << " cm^-1 " << endl;
            }

            Nb renorm = C_0;
            Nb imnorm = C_0;
            for(In j=I_0; j<mReRightEigVecs.Nrows(); ++j)
            {
               renorm += mReRightEigVecs[j][i]*mReLeftEigVecs[j][i] - mImRightEigVecs[j][i]*mImLeftEigVecs[j][i];
               imnorm += mReRightEigVecs[j][i]*mImLeftEigVecs[j][i] + mImRightEigVecs[j][i]*mReLeftEigVecs[j][i];
            }
            
            MidasVector NewReLeftVec(mReLeftEigVecs.Nrows());
            MidasVector NewImLeftVec(mReLeftEigVecs.Nrows());
            for(In j=I_0; j<NewReLeftVec.Size(); ++j)
            {
               NewReLeftVec[j] = renorm*mReLeftEigVecs[j][i] + imnorm*mImLeftEigVecs[j][i];
               NewImLeftVec[j] = -imnorm*mReLeftEigVecs[j][i] + renorm*mImLeftEigVecs[j][i];
            }
            Nb normsqr = renorm*renorm + imnorm*imnorm;
            NewReLeftVec.Scale(C_1/normsqr);
            NewImLeftVec.Scale(C_1/normsqr);
            mReLeftEigVecs.AssignCol(NewReLeftVec,i);
            mImLeftEigVecs.AssignCol(NewImLeftVec,i);
         }
         else
         {       
            Nb norm = C_0;
            for(In j=I_0; j<mReRightEigVecs.Nrows(); ++j)
               norm += mReRightEigVecs[j][i]*mReLeftEigVecs[j][i];
            
            if(norm > 0)
            {
               mReRightEigVecs.ScaleCol(C_1/sqrt(norm),i);
               mReLeftEigVecs.ScaleCol(C_1/sqrt(norm),i);
            }
            else
            {
               Mout << " NORM IS NEGATIVE! " << endl;
               mReRightEigVecs.ScaleCol(C_1/sqrt(fabs(norm)),i);
               mReLeftEigVecs.ScaleCol(-C_1/sqrt(fabs(norm)),i);
            }
         }
      }
   }
}

void BandLanczosLinRspFunc::CheckEigVecNorm()
{
   string filename = "eigvecnorm.dat";
   ofstream normfile(filename.c_str(), ios_base::trunc);
   
   for(In i=I_0; i<mReRightEigVecs.Ncols(); ++i)
   {
      Nb norm=C_0;
      for(In j=I_0; j<mReRightEigVecs.Nrows(); ++j)
         norm += mReRightEigVecs[j][i]*mReLeftEigVecs[j][i];
      norm = sqrt(norm);
      normfile << " Norm " << i << " = "  << norm << endl;
   }
}

void BandLanczosLinRspFunc::TransformStoX(const MidasVector& aEigVec, DataCont& aX) const
{
   if(mNHerm)
   {
      NHBandLanczosChainDef chaindef(mChainLen, mBlockSize, mLBlockSize, mOrthoChain, mSaveChain, mSaveInterval, mDefThresh);
      for(In i=0; i<mOpers.size(); i++)
         chaindef.AddOper(mOpers[i]);

      const NHBandLanczosChain* chain = FindChain(chaindef);

      chain->TransformStoX(aEigVec, aX);
   }
   else
   {
      BandLanczosChainDef chaindef(mChainLen, mBlockSize, mOrthoChain,mSaveChain,mTestChain,mDefThresh);
      for(In i=0; i<mOpers.size(); i++)
         chaindef.AddOper(mOpers[i]);

      const BandLanczosChain* chain = FindChain(chaindef);

      chain->TransformStoX(aEigVec, aX);
   }
}

MidasVector* BandLanczosLinRspFunc::GetReEigVals()
{
   DoEigSolutions();
   return &mReEigVals;
}

MidasVector* BandLanczosLinRspFunc::GetImEigVals()
{
   DoEigSolutions();
   return &mImEigVals;
}

MidasMatrix* BandLanczosLinRspFunc::GetReRightEigVecs()
{
   DoEigSolutions();
   return &mReRightEigVecs;
}

MidasMatrix* BandLanczosLinRspFunc::GetImRightEigVecs()
{
   DoEigSolutions();
   return &mImRightEigVecs;
}

MidasMatrix* BandLanczosLinRspFunc::GetReLeftEigVecs()
{
   DoEigSolutions();
   return &mReLeftEigVecs;
}

MidasMatrix* BandLanczosLinRspFunc::GetImLeftEigVecs()
{
   DoEigSolutions();
   return &mImLeftEigVecs;
}

void BandLanczosLinRspFunc::Evaluate()
{ 
   In nc = mpTrf->NexciXvec()-I_1;
   if(mChainLen > nc)
   {
      mChainLen = nc;
      Mout  << " In BandLanczosLinRspFunc::Evaluate(): " << endl
            << " ChainLength was to long, so it was reduced to the dimension of the excitation space: " 
            << nc << "."  <<endl;
   }

   if(!mNHerm)
   {
      Mout << " Evaluating band Lanczos Linear response " << endl;
      BandLanczosChainDef chaindef(mChainLen, mBlockSize, mOrthoChain,mSaveChain, mTestChain, mDefThresh);
      for(In i=0; i<mOpers.size(); i++)
         chaindef.AddOper(mOpers[i]);
      
      const BandLanczosChain& chain = *FindChain(chaindef);

      CalcZ(chain);
      CalcQRsp(chain);
      CalcLinRsp(chain);
   }
   else if(mNHerm)
   {
      Mout << " Evaluating non-hermitian band Lanczos Linear response " << endl;
      NHBandLanczosChainDef chaindef(mChainLen, mBlockSize, mLBlockSize, mOrthoChain, mSaveChain, mSaveInterval, mDefThresh);
      for(In i=0; i<mOpers.size(); i++)
         chaindef.AddOper(mOpers[i]);
      
      const NHBandLanczosChain& chain = *FindChain(chaindef);
      
      CalcRightZ(chain);
      CalcLeftZ(chain);
      CalcNHLinRsp(chain);
   }
   else
      MIDASERROR("BandLanczosLinRspFunc::Evaluate() : Bool NHerm not set.");
}

void BandLanczosLinRspFunc::CalcZ(const BandLanczosChain& aChain)
{
   mRightZ.SetNewSize(aChain.GetBlock(),mOpers.size(),false,true);

   for(In i=I_0; i<aChain.GetBlock(); i++)
   {
      DataCont q;
      aChain.GetStartVector(i,q);
      for(In j=I_0; j<mOpers.size(); j++)
      {
         DataCont b;
         aChain.GetEtaVector(j,b);
         mRightZ[i][j] = Dot(q,b);
      }
   }
}

void BandLanczosLinRspFunc::CalcRightZ(const NHBandLanczosChain& aChain)
{
   mRightZ.SetNewSize(aChain.GetRBlock(), mOpers.size(), false, true);
   const vector<Nb>* delta=aChain.GetDelta();

   for(In i = I_0; i<mRightZ.Nrows(); ++i)
   {
      DataCont v;
      aChain.GetLeftStartVector(i,v);
      for(In j=I_0; j<mOpers.size(); ++j)
      {
         DataCont xi;
         aChain.GetXiVector(j,xi);
         mRightZ[i][j] = Dot(v,xi)/(*delta)[i];
      }
   }
}

void BandLanczosLinRspFunc::CalcLeftZ(const NHBandLanczosChain& aChain)
{
   mLeftZ.SetNewSize(aChain.GetLBlock(), mOpers.size(), false, true);
   const vector<Nb>* delta=aChain.GetDelta();

   for(In i = I_0; i<mLeftZ.Nrows(); ++i)
   {
      DataCont w;
      aChain.GetRightStartVector(i,w);
      for(In j=I_0; j<mOpers.size(); ++j)
      {
         DataCont eta;
         aChain.GetEtaVector(j,eta);
         mLeftZ[i][j] = Dot(w,eta)/(*delta)[i];
      }
   }
}

void BandLanczosLinRspFunc::CalcQRsp(const BandLanczosChain& aChain)
{
   DoEigSolutions();

   In n_frq = In((mFrqEnd - mFrqStart)/mFrqStep) + I_1;
   
   mReQRsp.reserve(n_frq);
   mImQRsp.reserve(n_frq);

   MidasMatrix ReQRsp;
   MidasMatrix ImQRsp;
   ReQRsp.SetNewSize(aChain.GetBlock(), false, true);
   ImQRsp.SetNewSize(aChain.GetBlock(), false, true);
   
   for(In i=0; i<n_frq; i++)
   {
      Nb frq = mFrqStart + i*mFrqStep;

      for(In j=0; j<ReQRsp.Nrows(); j++)
      {
         for(In k=0; k<ReQRsp.Ncols(); k++)
         {
            ReQRsp[j][k] = 0;
            ImQRsp[j][k] = 0;
            for(In l=0; l<mReRightEigVecs.Nrows(); l++)
            {
               Nb divider1 = (mReEigVals[l] - frq)*(mReEigVals[l] - frq) + mGamma*mGamma;
               Nb divider2 = (mReEigVals[l] + frq)*(mReEigVals[l] + frq) + mGamma*mGamma;
               ReQRsp[j][k] += mReRightEigVecs[j][l]*mReRightEigVecs[k][l]*((mReEigVals[l] - frq)/divider1 + (mReEigVals[l] + frq)/divider2);
               ImQRsp[j][k] += mReRightEigVecs[j][l]*mReRightEigVecs[k][l]*(mGamma/divider1 - mGamma/divider2);
            }
         }
      }
      mFrqRange.push_back(frq);
      mReQRsp.push_back(ReQRsp);
      mImQRsp.push_back(ImQRsp);
   }
}

void BandLanczosLinRspFunc::CalcLinRsp(const BandLanczosChain& aChain)
{
   In n_frq = In((mFrqEnd - mFrqStart)/mFrqStep) + I_1;
   MidasMatrix ReLinRsp;
   MidasMatrix ImLinRsp;
   
   ReLinRsp.SetNewSize(aChain.GetNumEta(),false,true);
   ImLinRsp.SetNewSize(aChain.GetNumEta(),false,true);

   for(In m=0; m<n_frq; m++)
   {
      for(In i=0; i<ReLinRsp.Nrows(); i++)
      {
         for(In j=0; j<ReLinRsp.Ncols(); j++)
         {
            ReLinRsp[i][j] = 0;
            ImLinRsp[i][j] = 0;
            for(In k=0; k<mReQRsp[m].Nrows(); k++)
            {
               for(In l=0; l<mReQRsp[m].Ncols(); l++)
               {
                  ReLinRsp[i][j] += (mReQRsp[m])[k][l]*mRightZ[k][i]*mRightZ[l][j];
                  ImLinRsp[i][j] += (mImQRsp[m])[k][l]*mRightZ[k][i]*mRightZ[l][j];
               }
            }
         }
      }
      mReLinRsp.push_back(ReLinRsp);
      mImLinRsp.push_back(ImLinRsp);
   }
   
   std::ofstream response_file("bandlan_rsp");
   PrintResponseToStream(response_file);
}

void BandLanczosLinRspFunc::CalcNHLinRsp(const NHBandLanczosChain& aChain)
{
   if(mpVcc->GetMethod() != VCC_METHOD_VCC && mpVcc->GetMethod() != VCC_METHOD_VCI)
      MIDASERROR("BandLanczosLinRspFunc::CalcNHLinRsp() : Only implementet for VCI and VCC");

   DoEigSolutions();
   
   if(mpVcc->GetMethod() == VCC_METHOD_VCC && mIncludeFTerm)
      MakeFtrans(aChain);

   In n_frq = In((mFrqEnd - mFrqStart)/mFrqStep) +I_1;

   mReQRsp.reserve(n_frq);
   mImQRsp.reserve(n_frq);

   MidasMatrix ReRspa;
   MidasMatrix ImRspa;
   MidasMatrix ReFRsp;
   MidasMatrix ImFRsp;
   MidasMatrix ReLinRsp;
   MidasMatrix ImLinRsp;
   MidasMatrix ImLinFRsp;
   MidasMatrix ImLinNoFRsp;
   
   ReRspa.SetNewSize(aChain.GetLBlock(), aChain.GetRBlock(), false, true);
   ImRspa.SetNewSize(aChain.GetLBlock(), aChain.GetRBlock(), false, true);
   ReFRsp.SetNewSize(aChain.GetRBlock(), false, true);
   ImFRsp.SetNewSize(aChain.GetRBlock(), false, true);
   ReLinRsp.SetNewSize(aChain.GetNumEta(), false, true);
   ImLinRsp.SetNewSize(aChain.GetNumEta(), false, true);
   ImLinFRsp.SetNewSize(aChain.GetNumEta(), false, true);
   ImLinNoFRsp.SetNewSize(aChain.GetNumEta(), false, true);
   
   const vector<Nb>* delta=aChain.GetDelta();
   Nb rsp_frq=C_0/C_AUTKAYS;
   
   for(In j=I_0; j<ReRspa.Nrows(); ++j)
      for(In k=I_0; k<ReRspa.Ncols(); ++k)
      {
         ReRspa[j][k] = C_0;
         ImRspa[j][k] = C_0;
         ReFRsp[j][k] = C_0;
         ImFRsp[j][k] = C_0;
         for(In l=I_0; l<mReRightEigVecs.Ncols(); ++l)
            ReRspa[j][k] += mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*(*delta)[j]*(C_1/(mReEigVals[l]-rsp_frq) + C_1/(mReEigVals[l]+rsp_frq));
         if(mIncludeFTerm)
            for(In l=I_0; l<mReFtrans.Nrows(); ++l)
               for(In m=I_0; m<mReFtrans.Ncols(); ++m)
                  ReFRsp[j][k] += mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*(C_1/(mReEigVals[l]*mReEigVals[m]-mReEigVals[l]*rsp_frq+mReEigVals[m]*rsp_frq-rsp_frq*rsp_frq) + C_1/(mReEigVals[l]*mReEigVals[m]+mReEigVals[l]*rsp_frq-mReEigVals[m]*rsp_frq-rsp_frq*rsp_frq));
      }

   for(In  j=I_0; j<ReLinRsp.Nrows(); ++j)
      for(In k=I_0; k<ReLinRsp.Ncols(); ++k)
      {
         ReLinRsp[j][k] = C_0;
         ImLinRsp[j][k] = C_0;
         for(In l=I_0; l<ReRspa.Nrows(); ++l)
            for(In m=I_0; m<ReRspa.Ncols(); ++m)
               ReLinRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ReRspa[l][m] + mLeftZ[l][k]*mRightZ[m][j]*ReRspa[l][m];
         
         if(mIncludeFTerm)
            for(In l=I_0; l<ReFRsp.Nrows(); ++l)
               for(In m=I_0; m<ReFRsp.Nrows(); ++m)
                  ReLinRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ReFRsp[l][m];
         ReLinRsp[j][k] /= C_2;
      }
   
   for(In j=I_0; j<ReLinRsp.Nrows(); ++j)
   {
      Mout << " Static Response for operators: " << j << "," << j << " = " << ReLinRsp[j][j] << endl;
   }

   Mout << " I'm starting " << endl;
   if(!mIncludeIm || !mHasImEigVals)
   {
      //#pragma omp parallel for
      for(In i=0; i<n_frq; ++i)
      {
         /*MidasMatrix ReRspa;
         MidasMatrix ReRspb;
         MidasMatrix ImRspa;
         MidasMatrix ImRspb;
         MidasMatrix ReFRsp;
         MidasMatrix ImFRsp;
         MidasMatrix ReLinRsp;
         MidasMatrix ImLinRsp;
         ReRspa.SetNewSize(mChainLen, mChainLen, false, true);
         ImRspa.SetNewSize(mChainLen, mChainLen, false, true);
         if(mIncludeIm)
         {
            ReRspb.SetNewSize(mChainLen, mChainLen, false, true);
            ImRspb.SetNewSize(mChainLen, mChainLen, false, true);
         }
         ReFRsp.SetNewSize(mChainLen, false, true);
         ImFRsp.SetNewSize(mChainLen, false, true);
         ReLinRsp.SetNewSize(aChain.GetNumEta(), false, true);
         ImLinRsp.SetNewSize(aChain.GetNumEta(), false, true);*/
         
         Nb frq = mFrqStart + i*mFrqStep;
         // This can probably be optimized! continues >>
         for(In j=I_0; j<ReRspa.Nrows(); ++j)
            for(In k=I_0; k<ReRspa.Ncols(); ++k)
            {
               ReRspa[j][k] = C_0;
               ImRspa[j][k] = C_0;
               ReFRsp[j][k] = C_0;
               ImFRsp[j][k] = C_0;
               for(In l=I_0; l<mReRightEigVecs.Nrows(); ++l)
               {
                  Nb divider1 = (mReEigVals[l] - frq)*(mReEigVals[l] - frq) + mGamma*mGamma;
                  Nb divider2 = (mReEigVals[l] + frq)*(mReEigVals[l] + frq) + mGamma*mGamma;
                  ReRspa[j][k] += mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*(*delta)[j]*((mReEigVals[l] - frq)/divider1 + (mReEigVals[l] + frq)/divider2);
                  ImRspa[j][k] += mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*(*delta)[j]*(mGamma/divider1 - mGamma/divider2);
               }
               if(mIncludeFTerm)
                  for(In l=I_0; l<mReFtrans.Nrows(); ++l)
                     for(In m=I_0; m<mReFtrans.Ncols(); ++m)
                     {
                        Nb Omega1 = mReEigVals[l]*mReEigVals[m]-mReEigVals[l]*frq+frq*mReEigVals[m]-frq*frq+mGamma*mGamma; 
                        Nb Omega2 = mReEigVals[l]*mReEigVals[m]+mReEigVals[l]*frq-frq*mReEigVals[m]-frq*frq+mGamma*mGamma; 
                        Nb Gamma1 = -mReEigVals[l]*mGamma - C_2*frq*mGamma + mGamma*mReEigVals[m];
                        Nb Gamma2 = mReEigVals[l]*mGamma - C_2*frq*mGamma - mGamma*mReEigVals[m];
   
                        ReFRsp[j][k] += mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*(Omega1/(Omega1*Omega1 + Gamma1*Gamma1) + Omega2/(Omega2*Omega2 + Gamma2*Gamma2));
                        ImFRsp[j][k] += mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*(-Gamma1/(Omega1*Omega1 + Gamma1*Gamma1) - Gamma2/(Omega2*Omega2 + Gamma2*Gamma2));
                     }
            }

         // << continued. This can probably be optimized !...
         for(In  j=I_0; j<ReLinRsp.Nrows(); ++j)
            for(In k=I_0; k<ReLinRsp.Ncols(); ++k)
            {
                  ReLinRsp[j][k] = C_0;
                  ImLinRsp[j][k] = C_0;
                  ImLinFRsp[j][k] = C_0;
                  ImLinNoFRsp[j][k] = C_0;
                  for(In l=I_0; l<ReRspa.Nrows(); ++l)
                     for(In m=I_0; m<ReRspa.Ncols(); ++m)
                     {
                        ReLinRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ReRspa[l][m] + mLeftZ[l][k]*mRightZ[m][j]*ReRspa[l][m];
                        ImLinRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ImRspa[l][m] + mLeftZ[l][k]*mRightZ[m][j]*ImRspa[l][m];
                        ImLinNoFRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ImRspa[l][m] + mLeftZ[l][k]*mRightZ[m][j]*ImRspa[l][m];
                     } 
                  if(mIncludeFTerm)
                     for(In l=I_0; l<ReFRsp.Nrows(); ++l)
                        for(In m=I_0; m<ReFRsp.Nrows(); ++m)
                        {
                           ReLinRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ReFRsp[l][m];
                           ImLinRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ImFRsp[l][m];
                           ImLinFRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ImFRsp[l][m];
                        }
                  ReLinRsp[j][k] /= C_2;
                  ImLinRsp[j][k] /= C_2;
                  ImLinFRsp[j][k] /= C_2;
                  ImLinNoFRsp[j][k] /= C_2;
            }
   
         mFrqRange.push_back(frq);
         mReLinRsp.push_back(ReLinRsp);
         mImLinRsp.push_back(ImLinRsp);
         mImLinFRsp.push_back(ImLinFRsp);
         mImLinNoFRsp.push_back(ImLinNoFRsp);
         Mout << "." << std::flush;
      }
   }
   else
   {
      for(In i=0; i<n_frq; ++i)
      {
         Nb frq = mFrqStart + i*mFrqStep;
         for(In j=I_0; j<ReRspa.Nrows(); ++j)
         {
            for(In k=I_0; k<ReRspa.Ncols(); ++k)
            {
               ReRspa[j][k] = C_0;
               ImRspa[j][k] = C_0;
               ReFRsp[j][k] = C_0;
               ImFRsp[j][k] = C_0;
               for(In l=I_0; l<mReRightEigVecs.Nrows(); ++l)
               {
                  Nb Omega1 = mReEigVals[l] - frq;
                  Nb Omega2 = mReEigVals[l] + frq;
                  Nb Gamma1 = mGamma - mImEigVals[l];
                  Nb Gamma2 = -mGamma - mImEigVals[l];
                  Nb divider1 = Omega1*Omega1 + Gamma1*Gamma1;
                  Nb divider2 = Omega2*Omega2 + Gamma2*Gamma2;
                  
                  ReRspa[j][k] += (*delta)[j]*(mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Omega1 
                                             - mReRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Gamma1 
                                             - mImRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Gamma1 
                                             - mImRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Omega1)/divider1; 
                  
                  ReRspa[j][k] += (*delta)[j]*(mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Omega2 
                                             - mReRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Gamma2 
                                             - mImRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Gamma2 
                                             - mImRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Omega2)/divider2;
                  
                  ImRspa[j][k] += (*delta)[j]*(mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Gamma1 
                                             + mReRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Omega1 
                                             + mImRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Omega1 
                                             - mImRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Gamma1)/divider1;
                  
                  ImRspa[j][k] += (*delta)[j]*(mReRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Gamma2 
                                             + mReRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Omega2 
                                             + mImRightEigVecs[j][l]*mReLeftEigVecs[k][l]*Omega2 
                                             - mImRightEigVecs[j][l]*mImLeftEigVecs[k][l]*Gamma2)/divider2;
               }
               if(mIncludeFTerm)
               {
                  for(In l=I_0; l<mReFtrans.Nrows(); ++l)
                  {
                     for(In m=I_0; m<mReFtrans.Ncols(); ++m)
                     {
                        // old !
                        //Nb Omega1 = mReEigVals[l]*mReEigVals[m]+mReEigVals[l]*frq-mImEigVals[l]*mImEigVals[m]-mImEigVals[l]*mGamma-frq*mReEigVals[m]-frq*frq+mGamma*mImEigVals[m]+mGamma*mGamma;
                        //Nb Omega2 = mReEigVals[m]*mReEigVals[l]+mReEigVals[m]*frq-mImEigVals[m]*mImEigVals[l]-mImEigVals[m]*mGamma-frq*mReEigVals[l]-frq*frq+mGamma*mImEigVals[l]+mGamma*mGamma;
                        //Nb Gamma1 = -mReEigVals[l]*mImEigVals[m]-mReEigVals[l]*mGamma-mImEigVals[l]*mReEigVals[m]-mImEigVals[l]*frq+frq*mImEigVals[m]+C_2*frq*mGamma+mGamma*mReEigVals[m];
                        //Nb Gamma2 = -mReEigVals[m]*mImEigVals[l]-mReEigVals[m]*mGamma-mImEigVals[m]*mReEigVals[l]-mImEigVals[m]*frq+frq*mImEigVals[l]+C_2*frq*mGamma+mGamma*mReEigVals[l];
                        // trying out new ones 
                        Nb Omega1 = mReEigVals[l]*mReEigVals[m]-mReEigVals[l]*frq-mImEigVals[l]*mImEigVals[m]+mImEigVals[l]*mGamma+frq*mReEigVals[m]-frq*frq-mGamma*mImEigVals[m]+mGamma*mGamma;
                        Nb Omega2 = mReEigVals[m]*mReEigVals[l]-mReEigVals[m]*frq-mImEigVals[m]*mImEigVals[l]+mImEigVals[m]*mGamma+frq*mReEigVals[l]-frq*frq-mGamma*mImEigVals[l]+mGamma*mGamma;
                        Nb Gamma1 = -mReEigVals[l]*mImEigVals[m]+mReEigVals[l]*mGamma-mImEigVals[l]*mReEigVals[m]+mImEigVals[l]*frq-frq*mImEigVals[m]+C_2*frq*mGamma-mGamma*mReEigVals[m];
                        Nb Gamma2 = -mReEigVals[m]*mImEigVals[l]+mReEigVals[m]*mGamma-mImEigVals[m]*mReEigVals[l]+mImEigVals[m]*frq-frq*mImEigVals[l]+C_2*frq*mGamma-mGamma*mReEigVals[l];
                  
                        ReFRsp[j][k] += (mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega1
                                       - mReFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma1
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma1
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega1
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma1
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega1
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega1
                                       + mImFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma1)/(Omega1*Omega1 + Gamma1*Gamma1);

                        ReFRsp[j][k] += (mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega2
                                       - mReFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma2
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma2
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega2
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma2
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega2
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega2
                                       + mImFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma2)/(Omega2*Omega2 + Gamma2*Gamma2);
                        
                        ImFRsp[j][k] += (mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma1
                                       + mReFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega1
                                       + mReFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega1
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma1
                                       + mImFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega1
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma1
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma1
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega1)/(Omega1*Omega1 + Gamma1*Gamma1);
                        
                        ImFRsp[j][k] += (mReFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma2
                                       + mReFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega2
                                       + mReFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega2
                                       - mReFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma2
                                       + mImFtrans[l][m]*mReLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Omega2
                                       - mImFtrans[l][m]*mReLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Gamma2
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mReLeftEigVecs[k][m]*Gamma2
                                       - mImFtrans[l][m]*mImLeftEigVecs[j][l]*mImLeftEigVecs[k][m]*Omega2)/(Omega2*Omega2 + Gamma2*Gamma2);
                     }   
                  }
               }
            }
         }

         for(In  j=I_0; j<ReLinRsp.Nrows(); ++j)
         {
            for(In k=I_0; k<ReLinRsp.Ncols(); ++k)
            {
               ReLinRsp[j][k] = C_0;
               ImLinRsp[j][k] = C_0;
               ImLinFRsp[j][k] = C_0;
               ImLinNoFRsp[j][k] = C_0;
               for(In l=I_0; l<ReRspa.Nrows(); ++l)
               {
                  for(In m=I_0; m<ReRspa.Ncols(); ++m)
                  {
                     ReLinRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ReRspa[l][m]+mLeftZ[l][k]*mRightZ[m][j]*ReRspa[l][m];
                     ImLinRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ImRspa[l][m]+mLeftZ[l][k]*mRightZ[m][j]*ImRspa[l][m];
                     ImLinNoFRsp[j][k] += mLeftZ[l][j]*mRightZ[m][k]*ImRspa[l][m]+mLeftZ[l][k]*mRightZ[m][j]*ImRspa[l][m];
                  }
               }
               if(mIncludeFTerm)
               {
                  for(In l=I_0; l<ReFRsp.Nrows(); ++l)
                  {
                     for(In m=I_0; m<ReFRsp.Nrows(); ++m)
                     {
                        ReLinRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ReFRsp[l][m];
                        ImLinRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ImFRsp[l][m];
                        ImLinFRsp[j][k] += -mRightZ[l][j]*mRightZ[m][k]*ImFRsp[l][m];
                     }
                  }
               }
               
               ReLinRsp[j][k] /= C_2;
               ImLinRsp[j][k] /= C_2;
               ImLinFRsp[j][k] /= C_2;
               ImLinNoFRsp[j][k] /= C_2;
            }
         }
   
         mFrqRange.push_back(frq);
         mReLinRsp.push_back(ReLinRsp);
         mImLinRsp.push_back(ImLinRsp);
         mImLinFRsp.push_back(ImLinFRsp);
         mImLinNoFRsp.push_back(ImLinNoFRsp);
         Mout << "." << std::flush;
      }
   }

   std::ofstream response_file("nhbandlan_rsp");
   PrintResponseToStream(response_file);
}

void BandLanczosLinRspFunc::MakeFtrans(const NHBandLanczosChain& aChain)
{
   Mout << " Now making F-matrix transformations for Non-Hermitian Band Lanczos Linear Response. " << endl;
   In init_chainidx = I_0;
   MidasMatrix Fvv;
   Fvv.SetNewSize(mChainLen,mChainLen,false,false);
   DataCont Multipliers;
   string MultiName = mpVcc->pVccCalcDef()->GetName()+"_mul0_vec_0";
   Multipliers.GetFromExistingOnDisc(mpVcc->NrspPar(), MultiName);
   if(mpVcc->GetMethod() == VCC_METHOD_VCC && !Multipliers.ChangeStorageTo("InMem", true, false, true))
   {
      Mout << "Calculating 0th order Multipliers in BandLanczosLinRspFunc::MakeFtrans()." << endl;
      mpVcc->CalculateEta0();
      mpVcc->SolveZerothOrderEq();
      Multipliers.ChangeStorageTo("InMem", true, false, true);
   }
   Multipliers.SaveUponDecon(true);
   Multipliers.ChangeStorageTo("OnDisc");

   for(In i=I_0; i<mChainLen ; ++i)
   {
      time_t t0 = time(NULL);
      DataCont vi;
      ostringstream vios;
      vios << aChain.GetName() << "_v" << i;
      vi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vios.str());
      if(!vi.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading v(i) in BandLanczosLinRspFunc::MakeFtrans().");

      DataCont Fvi;
      ostringstream fvos;
      fvos << aChain.GetName() << "_Fv" << i;
      Fvi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, fvos.str());
      if(!mpVcc->pVccCalcDef()->RspRestart() || !Fvi.ChangeStorageTo("InMem", true, false, true))
      {
         Fvi.SetNewSize(mpTrf->NexciXvec()-I_1);
         Fvi.Zero();
         mpTrf->CalcVccRspFR(Multipliers, vi, Fvi);
         Fvi.NewLabel(fvos.str());
         Fvi.SaveUponDecon("true");
      }
      for(In j=i; j<mChainLen; ++j)
      {
         DataCont vj;
         ostringstream vjos;
         vjos << aChain.GetName() << "_v" << j;
         vj. GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vjos.str());
         if(!vj.ChangeStorageTo("InMem", true, false, true))
            MIDASERROR("Error reading v(j) in BandLanczosLinRspFunc::MakeFtrans().");

         Fvv[i][j] = Dot(Fvi, vj);
         Fvv[j][i] = Fvv[i][j];
      }
      if(i == init_chainidx)
      {
         Mout << " Required iterations: " << mChainLen-init_chainidx << endl
              << " First iteration: " << time(NULL)-t0<< " s" << endl
              << " Progress (one . is 10 iterations):" << endl << " ";
      }
      else if((i-init_chainidx)%10 == I_0)
         Mout << "." << std::flush;
   }

   // Kan evt optimeres ved fQrst at regne Transpose(mReRightEigVecs) og Transpose(mImRightEigVecs).
   // Kan også optimeres ved ikke at bruge transpose og istedet skrive det ud explicit...
   if(!mIncludeIm || !mHasImEigVals)
   {
      mReFtrans.SetNewSize(mChainLen, mChainLen, false, false);
      mReFtrans = Transpose(mReRightEigVecs)*Fvv*mReRightEigVecs;
   }
   else
   {
      mReFtrans.SetNewSize(mChainLen, mChainLen, false, false);
      mReFtrans = Transpose(mReRightEigVecs)*Fvv*mReRightEigVecs - Transpose(mImRightEigVecs)*Fvv*mImRightEigVecs;
      mImFtrans.SetNewSize(mChainLen, mChainLen, false, false);
      mImFtrans = Transpose(mReRightEigVecs)*Fvv*mImRightEigVecs + Transpose(mImRightEigVecs)*Fvv*mReRightEigVecs;
   }
}

void BandLanczosLinRspFunc::CheckForImEigVals()
{
   mHasImEigVals = false;
   // evt check for im eigvals foer du saetter en masse nuller!
   mImRightEigVecs.SetNewSize(mChainLen, mChainLen);
   mImLeftEigVecs.SetNewSize(mChainLen, mChainLen);
   mImRightEigVecs.Zero();
   mImLeftEigVecs.Zero();

   for(In i=I_0; i<mReEigVals.Size(); ++i)
      if(mImEigVals[i] != C_0)
      {
         MidasVector TempRe(mChainLen);
         MidasVector TempIm(mChainLen);

         mReRightEigVecs.GetCol(TempRe, i);
         mReRightEigVecs.GetCol(TempIm, i+I_1);

         mReRightEigVecs.AssignCol(TempRe, i+I_1);
         mImRightEigVecs.AssignCol(TempIm, i);
         TempIm.ChangeSign();
         mImRightEigVecs.AssignCol(TempIm, i+I_1);

         mReLeftEigVecs.GetCol(TempRe, i);
         mReLeftEigVecs.GetCol(TempIm, i+I_1);

         mReLeftEigVecs.AssignCol(TempRe, i+I_1);
         mImLeftEigVecs.AssignCol(TempIm, i);
         TempIm.ChangeSign();
         mImLeftEigVecs.AssignCol(TempIm, i+I_1);
         
         mHasImEigVals = true;
         ++i;
      }
}

void BandLanczosLinRspFunc::PrintResponseToStream(std::ostream& os) const
{
   for(int i=0; i<mReLinRsp.size(); ++i)
   {
      os << "Linear response for frq=" << mFrqRange[i] << " + i*" << mGamma<< " (au)    " << mFrqRange[i]*C_AUTKAYS << " + i*" << mGamma*C_AUTKAYS << "(cm^-1)\n";
      os << mReLinRsp[i] << "\n\n";
      os << mImLinRsp[i] << "\n";
   }
}

ostream& BandLanczosLinRspFunc::Print(ostream& aOut) const
{
   for(In i=0; i<mOpers.size(); ++i)
      for(In j=0; j<mOpers.size(); ++j)
         aOut << " <<" << mOpers[i] << ";" << mOpers[j] << ">> (BandLanczos)" << endl;

   aOut << "    Name (file prefix): '" << mName << "'" << endl;

   if(mFrqStart == mFrqEnd)
      aOut << "    Frequency:       " << mFrqStart*C_AUTKAYS << "cm-1" << endl;
   else
      aOut << "   Frequency range:     (" << mFrqStart*C_AUTKAYS << ":" << mFrqEnd*C_AUTKAYS
           << ":" << mFrqStep*C_AUTKAYS << ") cm-1" << endl;

   aOut << "    Gamma:              " << mGamma*C_AUTKAYS << " cm-1" << endl
        << "    Chain length:       " << mChainLen << endl
        << "    Orthogonalization:  " << LanczosRspFunc::StringForConst(mOrthoChain) << endl;
   
   return aOut;
}

vector<RspFunc> BandLanczosLinRspFunc::ConvertToRspFunc() const
{  
   vector<RspFunc> result;
   //Do stuff.
   MIDASERROR("NOT IMPLEMENTED");
   return result;
}

void BandLanczosLinRspFunc::SaveCombinedRspFunc() const
{
   //
   // init combined rsp func matrix
   //
   //CombinedRspFunc combined_rsp[mOpers.size()][mOpers.size()];
   CombinedRspFunc** combined_rsp = new CombinedRspFunc*[mOpers.size()];
   for(int i = 0; i < mOpers.size(); ++i)
   {
      combined_rsp[i] = new CombinedRspFunc[mOpers.size()];
   }
   for(int i = 0; i < mOpers.size(); ++i)
   {
      for(int j = 0; j < mOpers.size(); ++j)
      {
         std::vector<std::string> opers{mOpers[i], mOpers[j]};
         combined_rsp[i][j].Operators() = opers;
      }
   }
   
   //
   // then add every rsp function
   //
   for(int rsp_idx = 0; rsp_idx < mReLinRsp.size(); ++rsp_idx) // loop over points
   {
      for(int i = 0; i < mOpers.size(); ++i)
      {
         for(int j = 0; j < mOpers.size(); ++j)
         {
            RspFunc rsp_func(1);
            std::vector<std::string> opers{mOpers[i], mOpers[j]};
            rsp_func.Operators() = opers;

            //rsp_func.AddFrq(-mFrqRange[rsp_idx]);
            rsp_func.AddFrq( mFrqRange[rsp_idx]);
            rsp_func.SetGamma(mGamma);
            rsp_func.SetValue(-mReLinRsp[rsp_idx][i][j]); // sign convention is opposite for bandlan
            rsp_func.SetImValue(-mImLinRsp[rsp_idx][i][j]);

            rsp_func.SetHasBeenEval(true);
         
            combined_rsp[i][j].AddRspFunc(rsp_func);
         }
      }
   }

   //
   // then write to file
   //
   midas::analysis::rsp().MakeAnalysisDir();
   std::ofstream rsp_file(analysis::rsp().main_path);
   for(int i = 0; i < mOpers.size(); ++i)
   {
      for(int j = 0; j < mOpers.size(); ++j)
      {
         rsp_file << combined_rsp[i][j] << std::endl;
      }
   }

   // clean-up
   for(int i = 0; i < mOpers.size(); ++i)
   {
      delete[] combined_rsp[i];
   }
   delete[] combined_rsp;
}
