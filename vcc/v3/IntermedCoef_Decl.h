#ifndef INTERMEDCOEF_DECL_H_INCLUDED
#define INTERMEDCOEF_DECL_H_INCLUDED

#include "IntermediateOperIter.h"

namespace midas::vcc::v3
{

/**
 * Coefficient supporting iteration.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCoef 
   :  public IntermediateOperIter<T, DATA>
{
   public:
      //!
      using evaldata_t = DATA<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      std::vector<In>            mModes;      ///< General mode numbers in coefficient.
      ModeCombi                  mMc;         ///< Concrete modes in coefficient.
      std::vector<std::pair<In, In> > mCGmodeMap;  ///< Concrete modes to general modes map.
      In                         mCurTerm;    ///< Term in OpDef to read next.
      In                         mFinalTerm;  ///< Last term in OpDef corresponding to current MC.
   
   public:
      IntermedCoef();
      IntermedCoef(const std::string& aSpec);
      IntermedCoef(const IntermedCoef<T, DATA>& aOrig);
      std::unique_ptr<Intermediate<T, DATA> > Clone
         (
         )  const override
      {
         return std::make_unique<IntermedCoef<T, DATA> >(*this);
      }
      
      void AssignConcreteModes(const std::vector<In>& aCmodes) override;
      void GetConcreteModes(std::vector<In>& aCmodes) const override;
      void GetConcreteOpers(std::vector<In>& aCopers) const override;
      bool Evaluate(const evaldata_t& aEvalData, GeneralMidasVector<T>& aRes) const override;
      bool Evaluate(const evaldata_t& aEvalData, NiceTensor<T>& aRes) const override;
      std::ostream& Print(std::ostream& aOut) const override;
      void Latex(std::ostream& aOut) const override;
      void WriteSpec(std::ostream& aOut) const override;
      In HighestModeNo() const override;

      In ExciLevel() const override {return I_0;}

      void GetModes(std::vector<In>& aModes) const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }

      void GetOccModes(std::vector<In>& aModes) const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }
      
      void GetOperOccModes(std::vector<In>& aModes) const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }

      void GetExciModes
         (  std::vector<In>& aModes
         )  const override
      {
      };

      void GetPureExciModes
         (  std::vector<In>& aModes
         )  const override
      {
      };

      void GetOperExciModes
         (  std::vector<In>& aModes
         )  const override
      {
      };

      bool InitOperIter(const evaldata_t& aEvalData) override;
      bool InitOperIterTensor(const evaldata_t& aEvalData) override;
      bool AssignNextOpers(const evaldata_t& aEvalData, std::vector<LocalOperNr>& Copers) override;
      bool AssignNextOpersTensor(const evaldata_t& aEvalData, std::vector<LocalOperNr>& Copers) override;
      void AssignOpers(const std::vector<LocalOperNr>& aCopers, std::vector<In>& aGenOpers) const override;
      bool CmpKind(const Intermediate<T, DATA>* const aIntermed) const override;

      void GetExciMc(ModeCombi& aMc) const override {}
      
      void GetPureExciVectors(std::vector<std::vector<In> >& aExciVectors) const override {}
      void GetOperExciVectors(std::vector<std::vector<In> >& aOperExciVectors) const override {}
      void GetNoOperOccVectors(std::vector<std::vector<In> >& aNoOperOccVectors) const override {}
      
      bool OnlyAmps() const override {return true;}
      
      Scaling GetScaling(bool=false) const override;
      Scaling GetCost(bool=false) const override;
      Scaling GetNumber() const override;
      
      bool Norm2(const evaldata_t& aEvalData, real_t& aNorm2) const override;
      
      std::string Type() const override { return "IntermedCoef"; }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDCOEF_DECL_H_INCLUDED */
