/**
************************************************************************
* 
* @file                IntermedProd.h
*
* Created:             14-03-2019
*
* Author:              Niels Kristian Madsen (nielksm@chem.au.dk)
*
* Short Description:   Basic interface for IntermedProd
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDPROD_H_INCLUDED
#define INTERMEDPROD_H_INCLUDED

#include "IntermedProd_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "IntermedProd_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* INTERMEDPROD_H_INCLUDED */
