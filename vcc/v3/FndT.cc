/**
************************************************************************
* 
* @file                FndT.cc
*
* Created:             17-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing FndT class.
* 
* Last modified: Thu Aug 06, 2009  01:13PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "util/Io.h"
#include "FndT.h"
#include "util/MultiIndex.h"
#include "input/Input.h"
#include "mmv/MMVT.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/TensorSumAccumulator.h"

#include "util/CallStatisticsHandler.h"


namespace midas::vcc::v3
{

/**
 * aSpec should contains one std::string;
 * aSpec[0]: 'FnTl'
 *           F:    Just to indincate the Fock operator.
 *           n:    Coupling level in the Hamiltonian used to evaluate F.
 *           T:    Excitation operator. May be 'T','R', or similar.
 *           l:    Excitation level of T
 **/
FndT::FndT(const std::vector<string>& aSpec): V3Contrib()
{
   const std::string& spec = aSpec[I_0];
   if (spec[I_0] != 'F')
      MIDASERROR("FndT:FndT(): aSpec[0] must start with 'F'.");

   istringstream is(spec.substr(I_1, I_1));  // Suppose only one-digit n.
   is >> mFockLevel;

   mType = BasicOper::TypeForChar(spec[I_2]);
   if (mType!=OP::T && mType!=OP::R && mType!=OP::P && mType!=OP::Q && mType!=OP::S && mType!=OP::E)
   {
      Mout << " FndT::FndT(const std::vector<string>& aSpec):" << std::endl
           << "    spec = " << spec << std::endl
           << "    Type = " << mType << " is not an excitation operator." << std::endl;
      MIDASERROR("");
   }

   is.clear();
   is.str(spec.substr(I_3, I_1));      // Suppose only one-digit excitation level.
   is >> mTLevel;
}

/**
 * Evaluate <mu| Fn(d) T |ref>.
 * Result is added to aDcOut.
 * aDcOut must follow addresses given by aEvalData.GetModeCombiOpRange()
 **/
void FndT::Evaluate(const evaldata_t& aEvalData, GeneralDataCont<param_t>& aDcOut)
{
   LOGCALL("calls");
   if (mType == OP::E)
   {
      EvaluateLeft(aEvalData, aDcOut);
      return;
   }
   

   // Check if Hamiltonian contains requested mode coupling level.
   if (aEvalData.McLevel() < mFockLevel)
   {
      return;
   }
      
   GeneralMidasVector<param_t> amps;
   GeneralMidasVector<param_t> tmp_res;
   GeneralMidasVector<param_t> res;
   const ModeCombiOpRange& mcr_p = aEvalData.GetModeCombiOpRange();
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp)
   {
      LOGCALL("mc loop");
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if (mp.Size() != mTLevel)
         continue;

      
      In n_amps = aEvalData.NExciForModeCombi(i_mp);
      amps.SetNewSize(n_amps);
      aEvalData.template GetExciVec<GeneralDataCont<param_t>>(mType)->DataIo(IO_GET, amps, n_amps, mp.Address());


      // Do down contractions.
      for (In im=I_0; im<mTLevel; ++im)
      {
         LOGCALL("contract loop");
         In mode = mp.Mode(im);
         res.SetNewSize(amps.Size() / (aEvalData.NModals(mode)-I_1));
         res.Zero();
         ModeCombi mc_res(mp);
         mc_res.RemoveMode(mode);

         aEvalData.GetFockIntegrals()->Contract(mc_res, mp, mode, mFockLevel, res, amps, -I_1, IntegralOpT::STANDARD);

         // Find address of result MC and store result.
         ModeCombiOpRange::const_iterator it;
         In idx;
         if (! aEvalData.GetModeCombiOpRange().Find(mc_res,it,idx))
            MIDASERROR("FndT::Evaluate(): Down contraction result MC doesn't exist.");
         aDcOut.DataIo(IO_PUT, res, res.Size(), it->Address(), I_1, I_0, I_1, false, C_0, true);
      }
   }
}

/**
 * Evaluate for GeneralMidasVector<param_t> corresponding to specific ModeCombi.
 * This is a little different, since we need to identify all terms that contribute 
 * to the specific block of the result vector.
 **/
void FndT::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  GeneralMidasVector<param_t>& arDcOut
   )
{
   if (  this->mType == OP::E
      )
   {
      MIDASERROR("If this is thrown, implement FndT::EvaluateLeft for MidasVector. - Niels");
      return;
   }

   this->EvaluateMp(aEvalData, aMc, arDcOut);
}

/**
 *
 **/
void FndT::EvaluateMp                                                      
   (  const evaldata_t& aEvalData   /* all data */                           
   ,  const ModeCombi& aMc        /* mode combi */                         
   ,  GeneralMidasVector<param_t>& arDcOut        /* residual-vector block */              
   )                                                                       
{                                                                          
   LOGCALL("calls");                                                       
   /* Check if Hamiltonian contains requested mode coupling level. */      
   if (  aEvalData.McLevel() < this->mFockLevel                   
      )                                                                    
   {                                                                       
      return;                                                              
   }                                                                       
                                                                           
   /* Check that aMc contains one mode less than mTLevel */                
   if (  aMc.Size() != (this->mTLevel - I_1)                               
      )                                                                    
   {                                                                       
      return;                                                              
   }                                                                       
                                                                           
   /* Get number of modes (assumes the modes are ordered as 0,1,2,...) */  
   auto nmodes = gOperatorDefs.GetNrOfModes();                             
                                                                           
   /* Get pointer to amplitudes */                                         
   GeneralMidasVector<param_t> amps;
   GeneralMidasVector<param_t> res(arDcOut.Size());
                                                                           
   /* Loop over MCs of order mTLevel that contain aMc */                   
   for(In mode=0; mode<nmodes; ++mode)                                     
   {                                                                       
      LOGCALL("contract loop");                                            
      /* Continue if mode is already included in aMc */                    
      if (  aMc.IncludeMode(mode)                                          
         )                                                                 
      {                                                                    
         continue;                                                         
      }                                                                    
                                                                           
      /* Construct higher-order MC */                                      
      auto mc = aMc;                                                       
      mc.InsertMode(mode);                                                 
                                                                           
      /* Find index of amplitudes to contract */                           
      ModeCombiOpRange::const_iterator it_amp;                          
      In amp_idx = I_0;                                                    
      if ( !aEvalData.GetModeCombiOpRange().Find(mc, it_amp, amp_idx)                   
         )                                                                 
      {                                                                    
         MIDASERROR("FndT::EvaluateMp: Amplitudes needed for down contraction do not exist!");
      }                                                                    
      
      In n_amps = aEvalData.NExciForModeCombi(amp_idx);
      amps.SetNewSize(n_amps);
      aEvalData.template GetExciVec<GeneralDataCont<param_t>>(mType)->DataIo(IO_GET, amps, n_amps, it_amp->Address());
                                                                           
      /* Perform contraction and add to result */
      res.Zero();
      aEvalData.GetFockIntegrals()->Contract(aMc, mc, mode, mFockLevel, res, amps, -I_1, IntegralOpT::STANDARD);

      arDcOut += res;
   }                                                                  
}

/**
 * Evaluate for TensorDataCont
 **/
void FndT::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralTensorDataCont<param_t>& arDcOut
   )
{
   if (  this->mType == OP::E
      )
   {
      this->EvaluateLeft(aEvalData, arDcOut);
      return;
   }

   // Check if Hamiltonian contains requested mode coupling level.
   if (  aEvalData.McLevel() < this->mFockLevel
      )
   {
      return;
   }

   // Get amplitudes
   const auto* amps = aEvalData.template GetExciVec<GeneralTensorDataCont<param_t>>(this->mType);

   // Loop over mode combinations
   const auto& mcr = aEvalData.GetModeCombiOpRange();
   for(size_t imc = 0, size = mcr.Size(); imc < size; ++imc)
   {
      const auto& mc = mcr.GetModeCombi(imc);
      if (  mc.Size() != this->mTLevel
         )
      {
         continue;
      }
      auto mc_res = mc;

      // Do down contractions
      for(size_t imode=0; imode < this->mTLevel; ++imode)
      {
         auto mode = mc.Mode(imode);

         auto result = aEvalData.GetFockIntegrals()->ContractDown(mode, this->mFockLevel, imode, IntegralOpT::STANDARD, amps->GetModeCombiData(imc));

         // Create MC of result
         mc_res = mc;
         mc_res.RemoveMode(mode);

         // Find MC and add to result
         In idx = I_0;
         ModeCombiOpRange::const_iterator it_mc;
         if (  aEvalData.GetModeCombiOpRange().Find(mc_res, it_mc, idx)
            )
         {
            // Add to result
            arDcOut.GetModeCombiData(idx) += result;
         }
         else
         {
            MIDASERROR("FndT::Evaluate: Mode combi of result does not exist!");
         }
      }
   }
}


/**
 * Evaluate for NiceTensor corresponding to specific ModeCombi.
 * This is a little different, since we need to identify all terms that contribute 
 * to the specific block of the residual vector.
 **/
void FndT::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  NiceTensor<param_t>& arDcOut
   )
{
   if (  this->mType == OP::E
      )
   {
      this->EvaluateLeft(aEvalData, aMc, arDcOut);
   }
   else
   {
      this->EvaluateMp(aEvalData, aMc, arDcOut);
   }
}



/**
 * Evaluate for TensorSumAccumulator
 **/
void FndT::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  TensorSumAccumulator<param_t>& arDcOut
   )
{
   if (  this->mType == OP::E
      )
   {
      this->EvaluateLeft(aEvalData, aMc, arDcOut);
   }
   else
   {
      this->EvaluateMp(aEvalData, aMc, arDcOut);
   }
}

/**
 * Evaluate all terms that contribute to a given mode combination.
 **/
#define CREATE_EVALUATEMP(DATATYPE, DC)                                    \
void FndT::EvaluateMp                                                      \
   (  const evaldata_t& aEvalData   /* all data */                           \
   ,  const ModeCombi& aMc        /* mode combi */                         \
   ,  DATATYPE& arDcOut           /* residual-vector block */              \
   )                                                                       \
{                                                                          \
   LOGCALL("calls");                                                       \
   /* Check if Hamiltonian contains requested mode coupling level. */      \
   if (  aEvalData.McLevel() < this->mFockLevel                   \
      )                                                                    \
   {                                                                       \
      return;                                                              \
   }                                                                       \
                                                                           \
   /* Check that aMc contains one mode less than mTLevel */                \
   if (  aMc.Size() != (this->mTLevel - I_1)                               \
      )                                                                    \
   {                                                                       \
      return;                                                              \
   }                                                                       \
                                                                           \
   /* Get number of modes (assumes the modes are ordered as 0,1,2,...) */  \
   auto nmodes = gOperatorDefs.GetNrOfModes();                             \
   assert(nmodes == aEvalData.NModes());                      \
                                                                           \
   /* Get pointer to amplitudes */                                         \
   const auto* amps = aEvalData.template GetExciVec<DC>(this->mType);                \
                                                                           \
   /* Loop over MCs of order mTLevel that contain aMc */                   \
   for(In mode=0; mode<nmodes; ++mode)                                     \
   {                                                                       \
      LOGCALL("contract loop");                                            \
      /* Continue if mode is already included in aMc */                    \
      if (  aMc.IncludeMode(mode)                                          \
         )                                                                 \
      {                                                                    \
         continue;                                                         \
      }                                                                    \
                                                                           \
      /* Construct higher-order MC */                                      \
      auto mc = aMc;                                                       \
      mc.InsertMode(mode);                                                 \
                                                                           \
      /* Find index of amplitudes to contract */                           \
      ModeCombiOpRange::const_iterator it_amp;                             \
      In amp_idx = I_0;                                                    \
      if (  !aEvalData.GetModeCombiOpRange().Find(mc, it_amp, amp_idx)                   \
         )                                                                 \
      {                                                                    \
         MIDASERROR("FndT::EvaluateMp: Amplitudes needed for down contraction do not exist!");         \
      }                                                                    \
                                                                           \
      /* Get index to contract */                                          \
      auto contract_idx = mc.IdxNrForMode(mode);                           \
                                                                           \
      /* Perform contraction and add to result */                          \
      arDcOut += aEvalData.GetFockIntegrals()->ContractDown(mode, this->mFockLevel, contract_idx, IntegralOpT::STANDARD, amps->GetModeCombiData(amp_idx)); \
   }                                                                       \
}

CREATE_EVALUATEMP(NiceTensor<param_t>, GeneralTensorDataCont<param_t>);
CREATE_EVALUATEMP(TensorSumAccumulator<param_t>, GeneralTensorDataCont<param_t>);

#undef CREATE_EVALUATEMP // undef


/**
 * Evaluate <L| Fn(d) tau |ref>, i.e. this is a transformation form the left.
 * Result is added to aDcOut.
 * aDcOut must follow addresses given by aEvalData.GetModeCombiOpRange()
 **/
void FndT::EvaluateLeft
   (  const evaldata_t& aEvalData
   ,  GeneralDataCont<param_t>& aDcOut
   )
{

   // Check if Hamiltonian contains requested mode coupling level.
   if (aEvalData.McLevel() < mFockLevel)
   {
      return;
   }
      
   GeneralMidasVector<param_t> coefs;
   GeneralMidasVector<param_t> tmp_res;
   GeneralMidasVector<param_t> res;
   const ModeCombiOpRange mcr_p = aEvalData.GetModeCombiOpRange();

   // Loop over input vector. Excitation level is T-level - 1.
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp)
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if (mp.Size() != mTLevel-I_1)
         continue;

      
      In n_coefs = aEvalData.NExciForModeCombi(i_mp);
      coefs.SetNewSize(n_coefs);
      aEvalData.template GetExciVec<GeneralDataCont<param_t>>(OP::L)->DataIo(IO_GET, coefs, n_coefs, mp.Address());


      // Do "opposite down" contractions, i.e. up contractions with transposed integrals.
      for (In mode=I_0; mode<aEvalData.NModes(); ++mode)
      {
         if (mp.IncludeMode(mode))
            continue;
         
         res.SetNewSize(coefs.Size() * (aEvalData.NModals(mode)-I_1));
         res.Zero();
         ModeCombi mc_res(mp);
         mc_res.InsertMode(mode);
         aEvalData.GetFockIntegrals()->Contract(mc_res, mp, mode, mFockLevel, res, coefs, I_1, IntegralOpT::TRANSP);

         // Find address of result MC and store result.
         ModeCombiOpRange::const_iterator it;
         In idx;
         if (! aEvalData.GetModeCombiOpRange().Find(mc_res,it,idx))
            MIDASERROR("FndT::EvaluateLeft(): Up contraction result MC doesn't exist.");
         aDcOut.DataIo(IO_PUT, res, res.Size(), it->Address(), I_1, I_0, I_1, false, C_0, true);
      }
   }
}

/**
 *
 **/
void FndT::EvaluateLeft
   (  const evaldata_t& aEvalData
   ,  GeneralTensorDataCont<param_t>& arDcOut
   )
{
   MidasWarning("FndT::EvaluateLeft for TensorDataCont has not been tested!");

   // Check if Hamiltonian contains requested mode coupling level.
   if (aEvalData.McLevel() < mFockLevel)
   {
      return;
   }

   // Get amplitudes
   const auto* l_coefs = aEvalData.template GetExciVec<GeneralTensorDataCont<param_t>>(OP::L);

   // Loop over mode combinations
   const auto& mcr = aEvalData.GetModeCombiOpRange();
   size_t size = mcr.Size();
   for(size_t imc = 0; imc < size; ++imc)
   {
      const auto& mc = mcr.GetModeCombi(imc);
      if (  mc.Size() != this->mTLevel-I_1
         )
      {
         continue;
      }

      // Do up contractions
      for(size_t imode=0; imode < aEvalData.NModes(); ++imode)
      {
         if (  mc.IncludeMode(imode)
            )
         {
            continue;
         }

         // Create MC of result
         auto mc_res = mc;
         mc_res.InsertMode(imode);
         auto mode_idx = mc_res.IdxNrForMode(imode);

         auto result = aEvalData.GetFockIntegrals()->ContractUp(imode, this->mFockLevel, mode_idx, IntegralOpT::TRANSP, l_coefs->GetModeCombiData(imc));

         // Find MC and add to result
         In idx = I_0;
         ModeCombiOpRange::const_iterator it_mc;
         if (  aEvalData.GetModeCombiOpRange().Find(mc_res, it_mc, idx)
            )
         {
            // Add to result
            arDcOut.GetModeCombiData(idx) += result;
         }
         else
         {
            MIDASERROR("FndT::EvaluateLeft: Mode combi of result does not exist!");
         }
      }
   }
}

/**
 * Evaluate all terms that contribute to a given mode combination for left transform.
 **/
#define CREATE_EVALUATELEFT(DATATYPE, DC)                                      \
void FndT::EvaluateLeft                                                    \
   (  const evaldata_t& aEvalData   /* all data */                           \
   ,  const ModeCombi& aMc        /* mode combi */                         \
   ,  DATATYPE& arDcOut           /* residual-vector block */              \
   )                                                                       \
{                                                                          \
   /* Check if Hamiltonian contains requested mode coupling level. */      \
   if (  aEvalData.McLevel() < this->mFockLevel                   \
      )                                                                    \
   {                                                                       \
      return;                                                              \
   }                                                                       \
                                                                           \
   /* Check that aMc contains mTLevel number of modes */                   \
   if (  aMc.Size() != this->mTLevel                                       \
      )                                                                    \
   {                                                                       \
      return;                                                              \
   }                                                                       \
                                                                           \
   /* Get pointer to amplitudes */                                         \
   const auto* l_coefs = aEvalData.template GetExciVec<DC>(OP::L);                   \
                                                                           \
   /* NB: Loop over modes in aMc and remove one by one + contract up!!! */ \
   /* Loop over MCs of order mTLevel that contain aMc */                   \
   for(size_t i_m=0; i_m<aMc.Size(); ++i_m)                                \
   {                                                                       \
      LOGCALL("contract loop");                                            \
                                                                           \
      /* Construct lower-order MC */                                       \
      auto mode = aMc.Mode(i_m);                                           \
      auto mc = aMc;                                                       \
      mc.RemoveMode(mode);                                                 \
                                                                           \
      /* Find index of amplitudes to contract */                           \
      ModeCombiOpRange::const_iterator it_l;                               \
      In l_idx = I_0;                                                      \
      if (  !aEvalData.GetModeCombiOpRange().Find(mc, it_l, l_idx)                       \
         )                                                                 \
      {                                                                    \
         MIDASERROR("FndT::EvaluateLeft: L coefficients needed for down contraction do not exist!");         \
      }                                                                    \
                                                                           \
      /* Get index to up-contract */                                       \
      auto contract_idx = aMc.IdxNrForMode(mode);                          \
                                                                           \
      /* Perform contraction and add to result */                          \
      arDcOut += aEvalData.GetFockIntegrals()->ContractUp(mode, this->mFockLevel, contract_idx, IntegralOpT::TRANSP, l_coefs->GetModeCombiData(l_idx)); \
   }                                                                       \
}

CREATE_EVALUATELEFT(NiceTensor<param_t>, GeneralTensorDataCont<param_t>);
CREATE_EVALUATELEFT(TensorSumAccumulator<param_t>, GeneralTensorDataCont<param_t>);

#undef CREATE_EVALUATELEFT // undef

void FndT::WriteSpec(std::ostream& aOut) const
{
   aOut << "FndT 'F" << mFockLevel << BasicOper::CharForType(mType) << mTLevel;
}

std::ostream& FndT::Print(std::ostream& aOut) const
{
   aOut << "F" << mFockLevel << "(d) " << BasicOper::CharForType(mType) << mTLevel;
   return aOut;
}

} /* namespace midas::vcc::v3 */
