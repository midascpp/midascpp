/**
************************************************************************
* 
* @file                IntermedT_Impl.h
*
* Created:             01-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   T contraction intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDT_IMPL_H_INCLUDED
#define INTERMEDT_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>
#include <vector>
#include <set>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "Intermediate.h"
#include "IntermedProd.h"
#include "vcc/ModalIntegrals.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "vcc_cpg/CtrT.h"

#include "libmda/numeric/float_eq.h"
#include "libmda/util/copy_unique_ptr.h"

namespace midas::vcc::v3
{

/**
 * Default c-tor
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedT<T, DATA>::IntermedT
   (
   )
   :  Intermediate<T, DATA>()
   ,  mType(OP::T)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedT<T, DATA>::IntermedT
   (  const std::string& aSpec
   ,  OP aType
   )
   :  Intermediate<T, DATA>()
   ,  mType(aType)
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, this->mExci);
   detail::ReadVector(pos, this->mForw);
   detail::ReadVector(pos, this->mDown);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedT<T, DATA>::IntermedT
   (  const IntermedT<T, DATA>& aOrig
   )
   :  Intermediate<T, DATA>(aOrig)
   ,  mExci(aOrig.mExci)
   ,  mForw(aOrig.mForw)
   ,  mDown(aOrig.mDown)
   ,  mType(aOrig.mType)
   ,  mCidx(aOrig.mCidx)
   ,  mExciMc(aOrig.mExciMc)
   ,  mTotMc(aOrig.mTotMc)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedT<T, DATA>::IntermedT
   (  const CtrT& aCtrT
   )
   :  Intermediate<T, DATA>()
   ,  mType
         (  aCtrT.Type()
         )
{
   aCtrT.GetExciModes(this->mExci);
   aCtrT.GetForwModes(this->mForw);
   aCtrT.GetDownModes(this->mDown);
}

/**
 * Assign concrete modes and operators in the order:
 *     modes: [exci modes][forw modes][down modes]
 *     opers: [... -1 ...][forw opers][down opers]
 * @param aCmodes    Concrete modes to assign.
 * @param aCopers    Concrete opers to assign.
 * @return           True if assignment was successful, false otherwise.
 **/
namespace intermed_t::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetGlobalMcExci
   (
   )
{
   static std::vector<In> global_mc_exci;

   return global_mc_exci;
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetGlobalMcTot
   (
   )
{
   static std::vector<In> global_mc_tot;

   return global_mc_tot;
}

}  /* namespace intermed_t::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   this->mCidx.clear();
   
   intermed_t::detail::GetGlobalMcExci<T, DATA>().resize(0);
   intermed_t::detail::GetGlobalMcTot<T, DATA>().resize(0);
   intermed_t::detail::GetGlobalMcExci<T, DATA>().reserve(this->mExci.size()+this->mForw.size());
   intermed_t::detail::GetGlobalMcTot<T, DATA>().reserve(this->mExci.size()+this->mForw.size()+this->mDown.size());

   for (In m=I_0; m<this->mExci.size(); ++m)
   {
      this->mCidx.emplace_back(aCmodes[this->mExci[m]], -I_1);
      intermed_t::detail::GetGlobalMcExci<T, DATA>().emplace_back(aCmodes[this->mExci[m]]);
   }
   std::sort(this->mCidx.begin(), this->mCidx.end()); // sort concrete exci modes
   for (In m=I_0; m<this->mForw.size(); ++m)
   {
      this->mCidx.emplace_back(aCmodes[this->mForw[m]], aCopers[this->mForw[m]]);
      intermed_t::detail::GetGlobalMcExci<T, DATA>().emplace_back(aCmodes[this->mForw[m]]);
   }
   std::sort(this->mCidx.begin()+this->mExci.size(), this->mCidx.end()); // sort concrete forw modes
   intermed_t::detail::GetGlobalMcTot<T, DATA>() = intermed_t::detail::GetGlobalMcExci<T, DATA>();
   for (In m=I_0; m<this->mDown.size(); ++m)
   {
      this->mCidx.emplace_back(aCmodes[this->mDown[m]], aCopers[this->mDown[m]]);
      intermed_t::detail::GetGlobalMcTot<T, DATA>().emplace_back(aCmodes[this->mDown[m]]);
   }
   std::sort(this->mCidx.begin()+this->mExci.size()+this->mForw.size(), this->mCidx.end()); // sort concrete down modes

   std::sort(intermed_t::detail::GetGlobalMcExci<T, DATA>().begin(), intermed_t::detail::GetGlobalMcExci<T, DATA>().end());
   std::sort(intermed_t::detail::GetGlobalMcTot<T, DATA>().begin(), intermed_t::detail::GetGlobalMcTot<T, DATA>().end());

   intermed_t::detail::GetGlobalMcTot<T, DATA>().erase
      (  std::unique
            (  intermed_t::detail::GetGlobalMcTot<T, DATA>().begin()
            ,  intermed_t::detail::GetGlobalMcTot<T, DATA>().end()
            )
      ,  intermed_t::detail::GetGlobalMcTot<T, DATA>().end()
      );
   if (  intermed_t::detail::GetGlobalMcTot<T, DATA>().size() != this->mCidx.size()
      )                                // If some modes are present twice, this is
   {                                   // not a valid assesment.
      return false;                    // ian: this can happen at IntermedProdL.cc line 418
   }
   
   this->mExciMc.ReInit(intermed_t::detail::GetGlobalMcExci<T, DATA>()); // set concrete excited modes
   this->mTotMc.ReInit(intermed_t::detail::GetGlobalMcTot<T, DATA>());   // set all concrete modes used in intermed
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes.clear();
   aCmodes.resize(this->mCidx.size());
   for (In i=I_0; i<this->mCidx.size(); ++i)
   {
      aCmodes[i] = this->mCidx[i].first;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   aCopers.clear();
   aCopers.resize(this->mCidx.size());
   for (In i=this->mExci.size(); i<this->mCidx.size(); ++i)
   {
      aCopers[i] = this->mCidx[i].second;
   }
}

/**
 * Evaluate IntermedT for MidasVector result.
 * @param aEvalData      Data for evaluation.
 * @param aRes           Result of evaluation.
 * @return               True if intermediate could be evaluated, false otherwise.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   // Get amplitudes.
   ModeCombiOpRange::const_iterator it_mc;
   In i_mc;
   if (  !aEvalData.GetModeCombiOpRange().Find(this->mTotMc, it_mc, i_mc)
      )
   {
      return false;
   }
   In addr = it_mc->Address(); // get starting addr for mode combi
   In n_amps = aEvalData.NExciForModeCombi(i_mc); // get number of amplitudes in mode combi
   GeneralMidasVector<T> vec_out(n_amps);
   GeneralMidasVector<T> vec_in;
   aEvalData.template GetExciVec<GeneralDataCont<T> >(this->mType)->DataIo(IO_GET, vec_out, n_amps, addr);

   // Do down contractions.
   ModeCombi mc_out(this->mTotMc);
   ModeCombi mc_in(this->mTotMc);

   for (In i=this->mExci.size()+this->mForw.size(); i<this->mCidx.size(); ++i)
   {
      In mode = this->mCidx[i].first;
      In oper = this->mCidx[i].second;
      vec_in.Reassign(vec_out, vec_out.Size()); 
      vec_out.SetNewSize(vec_in.Size() / (aEvalData.NModals(mode) - I_1));
      mc_out.RemoveMode(mode);
      vec_out.Zero(); // zero vec_out as required by Contract
      aEvalData.GetIntegrals()->Contract(mc_out, mc_in, mode, oper, vec_out, vec_in, -I_1, IntegralOpT::STANDARD);
      mc_in.RemoveMode(mode);
   }

   // Do forwards contractions.
   for (In i=this->mExci.size(); i<this->mExci.size()+this->mForw.size(); ++i)
   {
      In mode = this->mCidx[i].first;
      In oper = this->mCidx[i].second;
      vec_in.Reassign(vec_out, vec_out.Size());
      vec_out.Zero(); // zero vec_out as required by Contract
      aEvalData.GetIntegrals()->Contract(mc_in, mc_in, mode, oper, vec_out, vec_in, I_0, IntegralOpT::STANDARD);
   }
   aRes.Reassign(vec_out);
   return true;
}

/**
 * Evaluate IntermedT for NiceTensor result.
 * @param aEvalData      Data for evaluation.
 * @param aRes           Result of evaluateion.
 * @return               True if intermediate could be evaluated, false otherwise.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   ModeCombiOpRange::const_iterator it_mc;
   In i_mc;
   if (  !aEvalData.GetModeCombiOpRange().Find(this->mTotMc, it_mc, i_mc)
      )
   {
      return false;
   }

   // init data and mc
   auto result = aEvalData.template GetExciVec<GeneralTensorDataCont<T> >(this->mType)->GetModeCombiData(i_mc);
   ModeCombi mode_combi(this->mTotMc);

   // do down contractions
   for (In i = this->mExci.size()+this->mForw.size(); i < this->mCidx.size(); ++i)
   {
      In mode = this->mCidx[i].first;
      In oper = this->mCidx[i].second;
      result = aEvalData.GetIntegrals()->ContractDown(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::STANDARD, result);
      mode_combi.RemoveMode(mode);
   }
                  
   // do forward contractions
   for (In i = this->mExci.size(); i < this->mExci.size()+this->mForw.size(); ++i)
   {
      In mode = this->mCidx[i].first;
      In oper = this->mCidx[i].second;
      result = aEvalData.GetIntegrals()->ContractForward(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::STANDARD, result);
   }

   // return result
   aRes = std::move(result);
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedT<T, DATA>::ExciLevel
   (
   )  const
{
   return this->mExci.size() + this->mForw.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::GetModes
   (  std::vector<In>& aModes 
   )  const
{
   std::copy(this->mExci.begin(), this->mExci.end(), std::back_inserter(aModes));
   std::copy(this->mForw.begin(), this->mForw.end(), std::back_inserter(aModes));
   std::copy(this->mDown.begin(), this->mDown.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedT<T, DATA>::HighestModeNo
   (
   )  const
{
   In high = -I_1;
   for (In i=I_0; i<this->mExci.size(); ++i)
      if (this->mExci[i] > high)
         high = this->mExci[i];
   for (In i=I_0; i<this->mForw.size(); ++i)
      if (this->mForw[i] > high)
         high = this->mForw[i];
   for (In i=I_0; i<this->mDown.size(); ++i)
      if (this->mDown[i] > high)
         high = this->mDown[i];
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::GetExciModes
   (  std::vector<In>& aModes 
   )  const
{
   std::copy(this->mExci.begin(), this->mExci.end(), std::back_inserter(aModes));
   std::copy(this->mForw.begin(), this->mForw.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedT<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   aOut << BasicOper::CharForType(this->mType, true);

   if (  this->mExci.empty()
      && this->mForw.empty()
      && this->mDown.empty()
      )
   {
      aOut << "(ref)";
      return aOut;
   }
   if (  !this->mExci.empty()
      )
   {
      aOut << "(e";
      std::copy(this->mExci.begin(), this->mExci.end(), std::ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   if (  !this->mForw.empty()
      )
   {
      aOut << "(f";
      std::copy(this->mForw.begin(), this->mForw.end(), std::ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   if (  !this->mDown.empty()
      )
   {
      aOut << "(d";
      std::copy(this->mDown.begin(), this->mDown.end(), std::ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   aOut << "{id:" << this->mRefNo << "}";
   
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   if (  !this->mForw.empty()
      )
   {
      aOut << "{}^{";
      for (In i=I_0; i<this->mForw.size(); ++i)
         aOut << "m_{" << this->mForw[i] << "}";
      aOut << "}";
   }
   
   aOut << BasicOper::CharForType(this->mType, true);
   
   if (  !this->mExci.empty()
      )
   {
      aOut << "^{";
      for (In i=I_0; i<this->mExci.size(); ++i)
         aOut << "m_{" << this->mExci[i] << "}";
      aOut << "}";
   }

   if (  !this->mDown.empty()
      )
   {
      aOut << "_{";
      for (In i=I_0; i<this->mDown.size(); ++i)
         aOut << "m_{" << this->mDown[i] << "}";
      aOut << "}";
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedT<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << BasicOper::CharForType(this->mType, true);
   aOut << this->mExci << this->mForw << this->mDown;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedT<T, DATA>* const other = dynamic_cast<const IntermedT<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   if (  this->mExci.size() == other->mExci.size()
      && this->mForw.size() == other->mForw.size()
      && this->mDown.size() == other->mDown.size()
      && this->mType == other->mType
      )
   {
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedT<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
  Scaling cost = GetCost(aCpTensors);
  Scaling number = GetNumber();
  return Scaling(number.mM, number.mO, cost.mN, cost.mR);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedT<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{
   In m = this->mExci.size() + this->mForw.size() + this->mDown.size();   // Total number of modes.
   
   if (  aCpTensors
      )
   {
      // The cost is determined by the first forward contraction as long as (2+D) < 2N,
      // where D is the order of the amplitude tensor and N is the number of modals per mode.
      // The cost of the forward contraction is 2*R*N^2
      In n = I_0;
      In r = I_0;
      if (  this->mForw.size() != I_0
         )
      {
         n = I_2;
         r = I_1;
      }
      else if  (  this->mDown.size() != I_0
               )
      {
         n = I_1;
         r = I_1;
      }

      return Scaling(I_0, I_0, n, r);
   }
   else
   {
      In n = I_0;                         // Scaling wrt. number of modals.
      if (this->mDown.size() != I_0)
         n = m;                           // First down scales as N^(#modes).
      else if (this->mForw.size() != I_0)
         n = m+I_1;                       // If no downs, first forward scales as N(#modes+1)

      return Scaling(I_0, I_0, n);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedT<T, DATA>::GetNumber
   (
   )  const
{
   In m = this->mExci.size() + this->mForw.size() + this->mDown.size();   // Scaling wrt. number of modes.
   In o = this->mForw.size() + this->mDown.size();                  // Scaling wrt. number of operators.
   In n = this->mExci.size() + this->mForw.size();                  // Scaling wrt. number of modals.
   
   return Scaling(m, o, n);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      using ContT = typename std::remove_reference_t<decltype(*aEvalData.GetIntegrals())>::ContT;
      aNorm2 = 1.0;

      ModeCombiOpRange::const_iterator it_mc;
      In i_mc;
      if (  !aEvalData.GetModeCombiOpRange().Find(this->mTotMc, it_mc, i_mc)
         ) 
      { 
         return false; // if we cant find it we return false
      }
      
      // norm of amplitudes
      aNorm2 *= aEvalData.GetNorm2(this->mType,i_mc);

      // norm of down contractions
      for(concrete_iter iter = begin_concrete_down(); iter!=end_concrete_down(); ++iter)
      {
         aNorm2 *= aEvalData.GetIntegrals()->GetNorm2(iter->first,iter->second,ContT::DOWN);
      }
      
      // norm of forward contractions
      for(concrete_iter iter = begin_concrete_forw(); iter!=end_concrete_forw(); ++iter)
      {
         aNorm2 *= aEvalData.GetIntegrals()->GetNorm2(iter->first,iter->second,ContT::FORWARD);
      }

      return true; // if calculated we return true
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedT<T, DATA>::AmpsNorm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      ModeCombiOpRange::const_iterator it_mc;
      In i_mc;
      if (!aEvalData.GetModeCombiOpRange().Find(this->mTotMc, it_mc, i_mc)) 
      { 
         return false; // if we cant find it we return false
      }
      
      aNorm2 = aEvalData.GetNorm2(this->mType,i_mc);

      return true; // if calculated we return true
   }
   else
   {
      return false;
   }
}

} /* namespace midas::vcc:v3 */

#endif /* INTERMEDT_IMPL_H_INCLUDED */
