/**
************************************************************************
* 
* @file                IntermedExci_Impl.h
*
* Created:             10-5-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   IntermedExci intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDEXCI_IMPL_H_INCLUDED
#define INTERMEDEXCI_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>
#include <vector>
#include <set>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MMVT.h"
#include "VccEvalData.h"
#include "IntermedExci.h"
#include "vcc/TensorSumAccumulator.h"
#include "vcc/ModalIntegrals.h"
#include "EvalDataTraits.h"

namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedExci<T, DATA>::IntermedExci
   (  const std::string& aSpec
   )
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, mExci);
   detail::ReadVector(pos, mForw);
   detail::ReadVector(pos, mDown);
   mCexci.resize(mExci.size());
   mCforw.resize(mForw.size());
   mCdown.resize(mDown.size());
}


/**
 *
 **/
namespace intermed_exci::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetMcTot
   (
   )
{
   static std::vector<In> mc_tot;
   return mc_tot;
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetMcExci
   (
   )
{
   static std::vector<In> mc_exci;
   return mc_exci;
}
} /* namespace intermed_exci::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedExci<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   // Vectors for initializing MC member variables.
   const In n_tot = mCexci.size()+mCforw.size()+mCdown.size();
   const In n_exci = mCexci.size()+mCforw.size();

   intermed_exci::detail::GetMcTot<T, DATA>().resize(0);
   intermed_exci::detail::GetMcExci<T, DATA>().resize(0);

   if (  intermed_exci::detail::GetMcTot<T, DATA>().capacity() < n_tot
      )
   {
      intermed_exci::detail::GetMcTot<T, DATA>().reserve(n_tot);
   }
   if (  intermed_exci::detail::GetMcExci<T, DATA>().capacity() < n_exci
      )
   {
      intermed_exci::detail::GetMcExci<T, DATA>().reserve(n_exci);
   }

   // Assign the excited modes (purely excited and forwards).
   for (In i=I_0; i<mExci.size(); ++i)
   {
      mCexci[i] = std::make_pair(aCmodes[mExci[i]], aCopers[mExci[i]]);
      intermed_exci::detail::GetMcExci<T, DATA>().push_back(aCmodes[mExci[i]]);
   }
   for (In i=I_0; i<mForw.size(); ++i)
   {
      mCforw[i] = std::make_pair(aCmodes[mForw[i]], aCopers[mForw[i]]);
      intermed_exci::detail::GetMcExci<T, DATA>().push_back(aCmodes[mForw[i]]);
   }
   std::sort(intermed_exci::detail::GetMcExci<T, DATA>().begin(), intermed_exci::detail::GetMcExci<T, DATA>().end());
  
   // Assign down contracted modes. 
   intermed_exci::detail::GetMcTot<T, DATA>() = intermed_exci::detail::GetMcExci<T, DATA>();
   for (In i=I_0; i<mDown.size(); ++i)
   {
      mCdown[i] = std::make_pair(aCmodes[mDown[i]], aCopers[mDown[i]]);
      intermed_exci::detail::GetMcTot<T, DATA>().push_back(aCmodes[mDown[i]]);
   }
   std::sort(intermed_exci::detail::GetMcTot<T, DATA>().begin(), intermed_exci::detail::GetMcTot<T, DATA>().end());

   intermed_exci::detail::GetMcTot<T, DATA>().erase(std::unique(intermed_exci::detail::GetMcTot<T, DATA>().begin(), intermed_exci::detail::GetMcTot<T, DATA>().end()), intermed_exci::detail::GetMcTot<T, DATA>().end());
   if (intermed_exci::detail::GetMcTot<T, DATA>().size() != n_tot)
      return false;

   mExciMc.ReInit(intermed_exci::detail::GetMcExci<T, DATA>());
   mTotMc.ReInit(intermed_exci::detail::GetMcTot<T, DATA>());
   return true; 
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedExci<T, DATA>::GetModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));
   std::copy(mForw.begin(), mForw.end(), std::back_inserter(aModes));
   std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedExci<T, DATA>::GetExciModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));
   std::copy(mForw.begin(), mForw.end(), std::back_inserter(aModes));
}

/**
 * Perform required forward and up contractions using transposed integrals.
 * Result is added to aRes.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedExci<T, DATA>::ActLeft
   (  const evaldata_t& aEvalData
   ,  const GeneralMidasVector<T>& aInVec
   ,  GeneralMidasVector<T>& aRes
   )
{
   // Do forward contractions using transposed integrals.
   GeneralMidasVector<T> vec_in(aInVec.Size());
   GeneralMidasVector<T> vec_out(aInVec);
   for (In i=I_0; i<mCforw.size(); ++i)
   {
      In mode = mCforw[i].first;
      In oper = mCforw[i].second;
      vec_in = vec_out;
      vec_out.Zero();
      aEvalData.GetIntegrals()->Contract(mExciMc, mExciMc, mode, oper, vec_out, vec_in, I_0, IntegralOpT::TRANSP);
   }

   // Do up contractions using transposed integrals.
   ModeCombi mc_in;
   ModeCombi mc_out(mExciMc);
   for (In i=I_0; i<mCdown.size(); ++i)
   {
      In mode = mCdown[i].first;
      In oper = mCdown[i].second;
      vec_in.Reassign(vec_out);
      vec_out.SetNewSize(vec_out.Size() * (aEvalData.NModals(mode) - I_1));
      vec_out.Zero();
      mc_in = mc_out;
      mc_out.InsertMode(mode);
      aEvalData.GetIntegrals()->Contract(mc_out, mc_in, mode, oper, vec_out, vec_in, I_1, IntegralOpT::TRANSP);
   }
   aRes += vec_out;
   return true;
}

/**
 * Act left for NiceTensor framework
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedExci<T, DATA>::ActLeft                                                                                              
   (  const evaldata_t& aEvalData                                                                                         
   ,  const NiceTensor<T>& aInVec                                                                                      
   ,  NiceTensor<T>& aRes                                                                                             
   )                                                                                                                    
{                                                                                                                       
   auto result = aInVec;                                                                                                
                                                                                                                        
   auto mode_combi = this->mExciMc;                                                                                     
                                                                                                                        
   /* Do forward contractions with transposed integrals */                                                              
   for(size_t i=I_0; i<mCforw.size(); ++i)                                                                              
   {                                                                                                                    
      In mode = mCforw[i].first;                                                                                        
      In oper = mCforw[i].second;                                                                                       
      result = aEvalData.GetIntegrals()->ContractForward(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::TRANSP, result);              
   }                                                                                                                    
                                                                                                                        
   /* Do down contractions (which are actually up contractions when acting left) */                                     
   for (In i=I_0; i<mCdown.size(); ++i)                                                                                 
   {                                                                                                                    
      In mode = mCdown[i].first;                                                                                        
      In oper = mCdown[i].second;                                                                                       
      mode_combi.InsertMode(mode);                                                                                      
      result = aEvalData.GetIntegrals()->ContractUp(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::TRANSP, result);                   
   }                                                                                                                    
                                                                                                                        
   aRes += result;                                                                                                      
                                                                                                                        
   return true;                                                                                                         
}                                                                                                                       

/**
 * Act left for NiceTensor framework
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedExci<T, DATA>::ActLeft                                                                                              
   (  const evaldata_t& aEvalData                                                                                         
   ,  const NiceTensor<T>& aInVec                                                                                      
   ,  TensorSumAccumulator<T>& aRes                                                                                             
   )                                                                                                                    
{                                                                                                                       
   auto result = aInVec;                                                                                                
                                                                                                                        
   auto mode_combi = this->mExciMc;                                                                                     
                                                                                                                        
   /* Do forward contractions with transposed integrals */                                                              
   for(size_t i=I_0; i<mCforw.size(); ++i)                                                                              
   {                                                                                                                    
      In mode = mCforw[i].first;                                                                                        
      In oper = mCforw[i].second;                                                                                       
      result = aEvalData.GetIntegrals()->ContractForward(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::TRANSP, result);              
   }                                                                                                                    
                                                                                                                        
   /* Do down contractions (which are actually up contractions when acting left) */                                     
   for (In i=I_0; i<mCdown.size(); ++i)                                                                                 
   {                                                                                                                    
      In mode = mCdown[i].first;                                                                                        
      In oper = mCdown[i].second;                                                                                       
      mode_combi.InsertMode(mode);                                                                                      
      result = aEvalData.GetIntegrals()->ContractUp(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::TRANSP, result);                   
   }                                                                                                                    
                                                                                                                        
   aRes += result;                                                                                                      
                                                                                                                        
   return true;                                                                                                         
}                                                                                                                       


/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedExci<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "e" << this->mExci << this->mForw << this->mDown;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedExci<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "LaTeX goes here...";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedExci<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   if (  !mForw.empty()
      )
   {
      aOut << "hf(";
      std::copy(mForw.begin(), mForw.end(), std::ostream_iterator<In>(aOut, ""));
      aOut << ") ";
   }

   if ( !mDown.empty()
      )
   {
      aOut << "hd(";
      std::copy(mDown.begin(), mDown.end(), std::ostream_iterator<In>(aOut, ""));
      aOut << ") ";
   }

   std::vector<In> modes;
   GetModes(modes);
   std::sort(modes.begin(), modes.end());
   aOut << "e(";
   std::copy(modes.begin(), modes.end(), std::ostream_iterator<In>(aOut, ""));
   aOut << ")";

   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedExci<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   MIDASERROR("IntermedExci<T, DATA>::GetScaling(): Not implemented");
   return Scaling();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedExci<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{
   MIDASERROR("IntermedExci<T, DATA>::GetCost(): Not implemented");
   return Scaling();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedExci<T, DATA>::GetNumber
   (
   )  const
{
   MIDASERROR("IntermedExci<T, DATA>::GetNumber(): Not implemented");
   return Scaling();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedExci<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      using ContT = typename std::remove_reference_t<decltype(*aEvalData.GetIntegrals())>::ContT;
      aNorm2=1.0;

      for (In i=I_0; i<mCforw.size(); ++i)
      {
         aNorm2*=aEvalData.GetIntegrals()->GetNorm2(mCforw[i].first,mCforw[i].second,ContT::FORWARD);
      }
      
      for (In i=I_0; i<mCdown.size(); ++i) 
      {
         aNorm2*=aEvalData.GetIntegrals()->GetNorm2(mCdown[i].first,mCdown[i].second,ContT::DOWN); 
      }

      return true;
   }
   else
   {
      return false;
   }
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDEXCI_IMPL_H_INCLUDED */
