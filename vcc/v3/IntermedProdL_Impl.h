/**
************************************************************************
* 
* @file                IntermedProdL_Impl.h
*
* Created:             19-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating + cleanup
*
* Short Description:   Implementing IntermedProdL class.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDPRODL_IMPL_H_INCLUDED
#define INTERMEDPRODL_IMPL_H_INCLUDED

#include "vcc/v3/IntermedProdL_Decl.h"

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <typeinfo>
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MMVT.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc/v3/IntermedExci.h"
#include "vcc/v3/IntermedL.h"
#include "vcc/v3/IntermedCmbL.h"
#include "vcc/DirProd.h"
#include "vcc/GenDownCtr.h"
#include "vcc/TensorGeneralDownContraction.h"
#include "vcc/TensorProductAccumulator.h"
#include "vcc/TransformerV3.h"
#include "input/OpDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "tensor/Scalar.h"

namespace midas::vcc::v3
{

/**
 * Construct from std::string specifications.
 * aSpec   : Vector of five std::strings.
 * aSpec[0]: Coefficient.
 * aSpec[1]: Sums.
 * aSpec[2]: Intermediates.
 * aSpec[3]: ModeGuides.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProdL<T, DATA>::IntermedProdL
   (  const std::vector<std::string>& aSpec
   )
   :  V3Contrib<T, DATA>
         (
         )
   ,  mAssumeOpItStored
         (  false
         )
{
   const std::string& coef  = aSpec[0];
   const std::string& sums  = aSpec[1];
   const std::string& ims   = aSpec[2];
   const std::string& mgs   = aSpec[3];

   // Read coef.
   std::istringstream coef_ss(coef);
   coef_ss >> mCoef;

   // Read sums.
   std::string::const_iterator pos = sums.begin();
   while (  pos != sums.end()
         )
   {
      if (  *pos == ' '
         )
      {
         pos++;
         continue;
      }
      
      if (  *pos++ == 's'
         )
      {
         mSums.push_back(ModeSum());
         mSums.back().Init(pos);
      }
      else
      {
         std::ostringstream os;
         os << "IntermedProdL<T, DATA>::IntermedProdL(): Unknown sum identifier"
            << " in sum specification for product: '" << std::endl << aSpec[2] << "'" << std::endl;
         MIDASERROR(os.str());
      }
   }

   // Read intermediate specification.
   pos = ims.begin();
   while (  pos != ims.end()
         )
   {
      if (  *pos == ' '
         )
      {
         pos++;
         continue;
      }

      std::string::const_iterator begin = pos;
      while (*pos != ' ' && pos != ims.end())
         pos++;
      mIntermeds.push_back(Intermediate<T, DATA>::Factory(std::string(begin, pos)));
   }

   mOpIterIntermed = detail::GetImFromVec<IntermediateOperIter<T, DATA> >(mIntermeds);
   if (  !mOpIterIntermed
      )
   {
      std::ostringstream os;
      os << "IntermedProdL<T, DATA>::IntermedProdL(): No IntermediateOperIter found in" << std::endl
         << "intermediate spec.: '" << ims << "'" << std::endl;
      MIDASERROR(os.str());
   }

   mIntermedTau = detail::GetImFromVec<IntermedExci<T, DATA> >(mIntermeds);
   if (  !mIntermedTau
      )
   {
      std::ostringstream os;
      os << "IntermedProdL<T, DATA>::IntermedProdL(): No IntermedExci found in" << std::endl
         << "intermediate spec.: '" << ims << "'" << std::endl;
      MIDASERROR(os.str());
   }

   mIntermedL = detail::GetImFromVec<IntermedL<T, DATA> >(mIntermeds);
   if (  !mIntermedL
      )
   {
      std::ostringstream os;
      os << "IntermedProdL<T, DATA>::IntermedProdL(): No IntermedL found in" << std::endl
         << "intermediate spec.: '" << ims << "'" << std::endl;
      MIDASERROR(os.str());
   }

   if (mgs == "")
      MIDASERROR("IntermedProdL<T, DATA>::IntermedProdL(): No mode guides specified.");
   else
      InitModeGuides(mgs);

   UpdateInternalState();
}

/**
 * Copy constructor
 *
 * @param aOrig
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProdL<T, DATA>::IntermedProdL
   (  const IntermedProdL& aOrig
   )
   :  V3Contrib<T, DATA>
         (  aOrig
         )
   ,  screened(aOrig.screened)
   ,  not_screened(aOrig.not_screened)
   ,  total(aOrig.total)
   ,  mCoef(aOrig.mCoef)
   ,  mSums(aOrig.mSums)
   ,  mModeGuides(aOrig.mModeGuides)
   ,  mCmodes(aOrig.mCmodes)
   ,  mAssumeOpItStored(aOrig.mAssumeOpItStored)
{
   mIntermedL = midas::util::DynamicCastUniquePtr<IntermedL<T, DATA> >(aOrig.mIntermedL->Clone());
   mIntermedTau = midas::util::DynamicCastUniquePtr<IntermedExci<T, DATA> >(aOrig.mIntermedTau->Clone());
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());
   for (In i=I_0; i<aOrig.mIntermeds.size(); ++i)
   {
      mIntermeds.emplace_back(aOrig.mIntermeds[i]->Clone());
   }
}

/**
 * Copy assignment
 *
 * @param aOrig
 * @return
 *    Reference to this
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProdL<T, DATA>& IntermedProdL<T, DATA>::operator=
   (  const IntermedProdL<T, DATA>& aOrig
   )
{
   // Check for self assignment
   if (  this == &aOrig
      )
   {
      return *this;
   }

   // Copy members
   mCoef             = aOrig.mCoef;
   mSums             = aOrig.mSums;
   mModeGuides       = aOrig.mModeGuides;
   mCmodes           = aOrig.mCmodes;
   mAssumeOpItStored = aOrig.mAssumeOpItStored;
   screened=aOrig.screened;
   not_screened=aOrig.not_screened;
   total=aOrig.total;

   // Clone pointers
   mIntermedL = midas::util::DynamicCastUniquePtr<IntermedL<T, DATA> >(aOrig.mIntermedL->Clone());
   mIntermedTau = midas::util::DynamicCastUniquePtr<IntermedExci<T, DATA> >(aOrig.mIntermedTau->Clone());
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());

   // Make sure the vector is clear, then reserve and emplace
   mIntermeds.clear();
   mIntermeds.reserve(aOrig.mIntermeds.size());
   for (In i=I_0; i<aOrig.mIntermeds.size(); ++i)
   {
      mIntermeds.emplace_back(aOrig.mIntermeds[i]->Clone());
   }
   
   return *this;
}

/**
 * Init mode guides specified in std::string as: "(012)(102)(201)".
 * Mode guides assign concrete modes in the bra state to the mode 
 * numbers specified in the guide vectors.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::InitModeGuides
   (  const std::string& aGuides
   )
{
   std::string::const_iterator pos = aGuides.begin();
   while (  pos != aGuides.end()
         )
   {
      std::vector<In> guide;
      detail::ReadVector(pos, guide);
      mModeGuides.emplace_back(guide);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::UpdateInternalState
   (
   )
{
   std::vector<In> all_modes;

   mExciLevel = mModeGuides[0].size();
   mOpIterIntermed->GetModes(all_modes);
   mIntermedTau->GetModes(all_modes);
   for (In i=I_0; i<mIntermeds.size(); ++i)
      mIntermeds[i]->GetModes(all_modes);
   std::sort(all_modes.begin(), all_modes.end());
   all_modes.erase(std::unique(all_modes.begin(), all_modes.end()), all_modes.end());

   mCmodes.resize(all_modes.back() + I_1);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  template<typename> typename VECTOR
   >
void IntermedProdL<T, DATA>::RegisterIntermeds
   (  IntermediateMachine<T, VECTOR, DATA>& aInMachine
   )
{
   mOpIterIntermed->RegisterInternalIntermeds(aInMachine);
   aInMachine.RegisterIntermediate(*mOpIterIntermed);
   mIntermedL->RegisterInternalIntermeds(aInMachine);
   aInMachine.RegisterIntermediate(*mIntermedL);
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->RegisterInternalIntermeds(aInMachine);
      aInMachine.RegisterIntermediate(*mIntermeds[i]);
   }
}

/**
 * Evaluate intermediate product.
 * Result is added to aDcOut.
 * aDcOut must follow addresses given by aEvalData.GetModeCombiOpRange()
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralDataCont<T>& arDcOut
   )
{
   In tau_level = mModeGuides[0].size();    // Total excitation level of tau operator.

   GeneralMidasVector<T> res_vec;
   const ModeCombiOpRange& mcr = aEvalData.GetModeCombiOpRange();
   for (In i_mc=I_0; i_mc<mcr.Size(); ++i_mc) // loop over mode combi
   {
      const ModeCombi& mc = mcr.GetModeCombi(i_mc);
      if (mc.Size() != tau_level)
         continue;

      In n_coefs = aEvalData.NExciForModeCombi(i_mc);
      res_vec.SetNewSize(n_coefs);
      res_vec.Zero();
      EvaluateTauMc(aEvalData, mc, res_vec);

      // Add result to arDcOut.
      arDcOut.DataIo(IO_PUT, res_vec, res_vec.Size(), mc.Address(), I_1, I_0, I_1, false, C_0, true);
   }
}

/**
 * Evaluate for TensorDataCont
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralTensorDataCont<T>& arOut
   )
{
   In tau_level = mModeGuides[0].size();    // Total excitation level of tau operator.

   const auto& mcr = aEvalData.GetModeCombiOpRange();
   for(size_t i_mc=0; i_mc<mcr.Size(); ++i_mc)
   {
      const auto& mc = mcr.GetModeCombi(i_mc);
      if (  mc.Size() != tau_level
         )
      {
         continue;
      }

      // Construct result vector
      const auto& dims = aEvalData.GetTensorDims(i_mc);
      bool is_scalar = ( dims.size() == 0 );
      bool cp_scalar = false;
      if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                     )
      {
         cp_scalar = aEvalData.DecomposedTensors();
      }
      NiceTensor<T> res_vec   (  is_scalar
                              ?  static_cast<BaseTensor<T>*>(new Scalar<T>(0., cp_scalar))
                              :  static_cast<BaseTensor<T>*>(new SimpleTensor<T>(dims))
                              );

      // Evaluate
      this->EvaluateTauMc(aEvalData, mc, res_vec);

      // Add result to arOut
      if (  mc.Size() == 0
         )
      {
         T scalar = C_0;
         res_vec.GetTensor()->DumpInto(&scalar);
         arOut.GetModeCombiData(i_mc).ElementwiseScalarAddition(scalar);
      }
      else
      {
         arOut.GetModeCombiData(i_mc) += res_vec;
      }
   }
}

/**
 * Evaluate for TensorSumAccumulator<T>
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  TensorSumAccumulator<T>& arOut
   )
{
   this->EvaluateTauMc(aEvalData, aMc, arOut);
}

/**
 * Evaluate for NiceTensor
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  NiceTensor<T>& arOut
   )
{
   this->EvaluateTauMc(aEvalData, aMc, arOut);
}


/**
 * Evaluate this product for the tau specified by aTauMc.
 **/
#define CREATE_EVALUATETAUMC(RETURNTYPE)                       \
template                                                       \
   <  typename T                                               \
   ,  template<typename...> typename DATA                         \
   >                                                           \
void IntermedProdL<T, DATA>::EvaluateTauMc                     \
   (  const evaldata_t& aEvalData                              \
   ,  const ModeCombi& aTauMc                                  \
   ,  RETURNTYPE& arRes                                        \
   )                                                           \
{                                                              \
   for(size_t i=0; i<this->mModeGuides.size(); ++i)            \
   {                                                           \
      /* Distribute concrete modes according to mode guide */  \
      for(size_t m=0; m<aTauMc.Size(); ++m)                    \
      {                                                        \
         mCmodes[mModeGuides[i][m]] = aTauMc.Mode(m);          \
      }                                                        \
                                                               \
                                                               \
      this->DoSum(aEvalData, I_0, I_0, I_0, aTauMc, arRes);    \
   }                                                           \
}                                                              \

CREATE_EVALUATETAUMC(GeneralMidasVector<T>);
CREATE_EVALUATETAUMC(NiceTensor<T>);
CREATE_EVALUATETAUMC(TensorSumAccumulator<T>);

#undef CREATE_EVALUATETAUMC

/**
 * Do restricted summation
 **/
#define CREATE_DOSUM(RETURNTYPE)                                                                \
template                                                                                        \
   <  typename T                                                                                \
   ,  template<typename...> typename DATA                                                          \
   >                                                                                            \
void IntermedProdL<T, DATA>::DoSum                                                              \
   (  const evaldata_t& aEvalData                                                               \
   ,  In aSum                                                                                   \
   ,  In aSumIdx                                                                                \
   ,  In aFirstMode                                                                             \
   ,  const ModeCombi& aTauMc                                                                   \
   ,  RETURNTYPE& arRes                                                                         \
   )                                                                                            \
{                                                                                               \
   if (aSum >= mSums.size())                                                                    \
   {                                                                                            \
      LoopOperators(aEvalData, aTauMc, arRes);                                                  \
      return;                                                                                   \
   }                                                                                            \
                                                                                                \
   ModeSum& sum = mSums[aSum];                                                                  \
                                                                                                \
   /* Update restriction in this sum. */                                                        \
   if (aSumIdx == I_0)                                                                          \
      for (In m=I_0; m<sum.mRestrict.size(); ++m)                                               \
         sum.mCRestrict[m] = mCmodes[sum.mRestrict[m]];                                         \
                                                                                                \
   for (In cmode=aFirstMode; cmode<aEvalData.NModes(); ++cmode)                    \
   {                                                                                            \
      if (sum.mCRestrict.end() != find(sum.mCRestrict.begin(), sum.mCRestrict.end(), cmode))    \
         continue;                                                                              \
      mCmodes[sum.mModes[aSumIdx]] = cmode;                                                     \
                                                                                                \
      if (aSumIdx == sum.mModes.size()-I_1)                                                     \
         DoSum(aEvalData, aSum+I_1, I_0, I_0, aTauMc, arRes);                                   \
      else                                                                                      \
         DoSum(aEvalData, aSum, aSumIdx+I_1, cmode+I_1, aTauMc, arRes);                         \
   }                                                                                            \
}                                                                                               \

CREATE_DOSUM(GeneralMidasVector<T>);
CREATE_DOSUM(NiceTensor<T>);
CREATE_DOSUM(TensorSumAccumulator<T>);

#undef CREATE_DOSUM


/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::LoopOperators
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aTauMc
   ,  GeneralMidasVector<T>& arRes
   )
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<GeneralMidasVector>();
      mOpIterIntermed->AssignConcreteModes(mCmodes);
      if (  !intermeds.InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }

      // Get L intermediate MC.
      std::vector<In> dummy_ops(mCmodes.size(), -I_1);
      if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, dummy_ops)
         )
      {
         return;
      }
      ModeCombi l_mc;
      mIntermedL->GetExciMc(l_mc);
      
      // MC for excited modes in tau, i.e. the result MC of the generalized down contraction.
      if (  !mIntermedTau->AssignConcreteModesOpers(mCmodes, dummy_ops)
         )
      {
         return;
      }
      ModeCombi down_res_mc;
      mIntermedTau->GetExciMc(down_res_mc);
      
      // Generate MC and initialize result vector for product of ordinary intermediates.
      ModeCombi imp_mc;
      imp_mc.InFirstOnly(l_mc, down_res_mc);
      In imp_size = I_1;
      for (In i=I_0; i<imp_mc.Size(); ++i)
         imp_size *= (aEvalData.NModals(imp_mc.Mode(i)) - I_1);
      GeneralMidasVector<T> imp_res(imp_size);

      GenDownCtr<T, DATA> gd_ctr(aEvalData, l_mc, imp_mc);
      
      GeneralMidasVector<T> opit_res;
      GeneralMidasVector<T> l_res;
      GeneralMidasVector<T> down_res; // Storing result gen. down. ctr. <L| * "intermediate prod.".
      std::vector<In> opers(mCmodes.size());

      while (  intermeds.GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, opit_res)
            )
      {
         // check if we can assign modes... if not continue to next term
         // NB PACK THIS INTO SINGLE FUNCTION CALL THAT RETURNS BOOL
         // why is the below call made ?? modes are already assigned from before :S
         if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, opers)
            ) //assign intermedL
         {
            continue;
         }
         
         bool assign_failed=false;
         for(int i=0; i<mIntermeds.size(); ++i) // assign intermeds
         {
            if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, opers)
               )
            {
               assign_failed=true;
               break; //break for
            }
         }
         if (  assign_failed
            )
         {
            continue; // if we cant assign continue while
         }
         
         mIntermedTau->AssignConcreteModesOpers(mCmodes, opers); // assign intermedtau
         
         incr_total();
         // if we do screening calculate norm and screen if nescessary
         // pack this into another function call

         if constexpr   (  midas::vcc::v3::DataAllowsScreeningV<evaldata_t>
                        )
         {
            if (  aEvalData.ScreenIntermeds()
               )
            {
               /* calculate intermedprodl norm */
               Nb norm = opit_res.Norm2();
               Nb new_norm=1.0;
               intermeds.GetIntermedNorm2(*mIntermedL, aEvalData, new_norm);
               norm*=new_norm;
               for(In i=0; i<mIntermeds.size(); ++i)
               {
                  if (  !intermeds.GetIntermedNorm2(*mIntermeds[i], aEvalData, new_norm))
                  {
                     MIDASERROR("this should not happen :S");
                     return;
                  }
                  norm *= new_norm;
               }
               mIntermedTau->Norm2(aEvalData,new_norm);
               norm*=new_norm;

               /* check norm */
               if(norm<=aEvalData.ScreenIntermedsThresh())
               {
                  incr_screened();
                  continue; // if to small continue while
               }
            }
         }
         incr_not_screened();

         // if not screened, calculate term
         if (  !intermeds.GetIntermed(*mIntermedL, aEvalData, l_res)
            )
         {
            continue;
         }
         
         imp_res.Zero();
         this->EvalIntermeds(aEvalData, imp_mc, imp_res, opit_res);

         down_res.SetNewSize(l_res.Size() / imp_res.Size());
         down_res.Zero();
         gd_ctr.Contract(l_res, imp_res, down_res);

         if (  !mIntermedTau->ActLeft(aEvalData, down_res, arRes)
            )
         {
            MIDASERROR("IntermedProdL<T, DATA>::LoopOperators(): mIntermedTau->ActLeft() failed.");
         }
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}


/**
 * Loop over operators with NiceTensor framework (is this ever used without TensorSumAccumulator<T>?)
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::LoopOperators                                                               
   (  const evaldata_t& aEvalData                                                                 
   ,  const ModeCombi& aTauMc                                                                   
   ,  NiceTensor<T>& arRes                                                                          
   )                                                                                            
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      mOpIterIntermed->AssignConcreteModes(mCmodes);
      if (  !intermeds.InitIntermedOperIter(mOpIterIntermed, aEvalData)                          
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
                                                                                                   
      /* Get L intermediate MC. */                                                                 
      std::vector<In> dummy_ops(mCmodes.size(), -I_1);                                             
      if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, dummy_ops)                              
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
      ModeCombi l_mc;                                                                              
      mIntermedL->GetExciMc(l_mc);                                                                 
                                                                                                   
      /* MC for excited modes in tau, i.e. the result MC of the generalized down contraction. */   
      if (  !mIntermedTau->AssignConcreteModesOpers(mCmodes, dummy_ops)                            
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
      ModeCombi down_res_mc;                                                                       
      mIntermedTau->GetExciMc(down_res_mc);                                                        
                                                                                                   
      /* Generate MC of modes that must be contracted */                                           
      ModeCombi imp_mc;                                                                            
      imp_mc.InFirstOnly(l_mc, down_res_mc);                                                       
                                                                                                   
      /* Initialize wrapper for multi-index down contraction */                                    
      TensorGeneralDownContraction<T, DATA> gd_ctr(aEvalData, l_mc, imp_mc);                                
                                                                                                   
      NiceTensor<T> opit_res;                                                                     
      NiceTensor<T> l_res;                                                                        
      std::vector<In> opers(mCmodes.size());                                                       

      while (  intermeds.GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, opit_res)       
            )                                                                                      
      {                                                                                            
         /* check if we can assign modes... if not continue to next term */                        
         if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, opers)                               
            ) /* assign intermedL */                                                               
         {                                                                                         
            continue;                                                                              
         }                                                                                         
                                                                                                   
         bool assign_failed=false;                                                                 
         for(size_t i=0; i<mIntermeds.size(); ++i) /* assign intermeds */                          
         {                                                                                         
            if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, opers)                         
               )                                                                                   
            {                                                                                      
               assign_failed=true;                                                                 
               break; /* break for */                                                              
            }                                                                                      
         }                                                                                         
                                                                                                   
         /* if we can't assign, continue while */                                                  
         if (  assign_failed                                                                       
            )                                                                                      
         {                                                                                         
            continue;                                                                              
         }                                                                                         
                                                                                                   
         mIntermedTau->AssignConcreteModesOpers(mCmodes, opers); /* assign intermedtau */          
                                                                                                   
         incr_total();                                                                             
         incr_not_screened();                                                                      
                                                                                                   
         /* Get tensor type for initialization */
         auto tens_type = arRes.Type();

         std::vector<unsigned> l_dims(l_mc.Size());
         for(size_t i=0; i<l_dims.size(); ++i)
         {
            l_dims[i] = aEvalData.NModals(l_mc.Mode(i)) - I_1;
         }

         /* Initialize l_res */
         l_res = l_dims.empty() ? NiceScalar<T>(0., tens_type == BaseTensor<T>::typeID::CANONICAL) : NiceTensor<T>(l_dims, tens_type);                                                                   

         /* calculate L term */                                                                    
         if (  intermeds.GetIntermed(*mIntermedL, aEvalData, l_res)                              
            )                                                                                      
         {                                                                                         
            /* Evaluate contraction product */                                                        
            std::vector<unsigned> imp_dims(imp_mc.Size());
            for(size_t i=0; i<imp_dims.size(); ++i)
            {
               imp_dims[i] = aEvalData.NModals(imp_mc.Mode(i)) - 1;
            }
            auto imp_res = imp_dims.empty() ? NiceScalar<T>(0., tens_type == BaseTensor<T>::typeID::CANONICAL) : NiceTensor<T>(imp_dims, tens_type);
            this->EvalIntermeds(aEvalData, imp_mc, imp_res, opit_res);                                
                                                                                                      
            /* Perform general down contraction of <L| and [ctr. prod.] */                            
            auto down_res = gd_ctr.Contract(l_res, imp_res);                                          
                                                                                                      
            /* Act to the left with the excitation (tau) operator */                                  
            if (  !mIntermedTau->ActLeft(aEvalData, down_res, arRes)                                   
               )                                                                                      
            {                                                                                         
               MIDASERROR("IntermedProdL<T, DATA>::LoopOperators(): mIntermedTau->ActLeft() failed.");         
            }                                                                                         
         }                                                                                         
      }                                                                                            
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}                                                                                               

/**
 * Loop over operators with NiceTensor framework using TensorSumAccumulator<T>
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::LoopOperators                                                               
   (  const evaldata_t& aEvalData                                                                 
   ,  const ModeCombi& aTauMc                                                                   
   ,  TensorSumAccumulator<T>& arRes                                                                          
   )                                                                                            
{                                                                                               
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      mOpIterIntermed->AssignConcreteModes(mCmodes);                                               
      if (  !intermeds.InitIntermedOperIter(mOpIterIntermed, aEvalData)                          
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
                                                                                                   
      /* Get L intermediate MC. */                                                                 
      std::vector<In> dummy_ops(mCmodes.size(), -I_1);                                             
      if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, dummy_ops)                              
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
      ModeCombi l_mc;                                                                              
      mIntermedL->GetExciMc(l_mc);                                                                 
                                                                                                   
      /* MC for excited modes in tau, i.e. the result MC of the generalized down contraction. */   
      if (  !mIntermedTau->AssignConcreteModesOpers(mCmodes, dummy_ops)                            
         )                                                                                         
      {                                                                                            
         return;                                                                                   
      }                                                                                            
      ModeCombi down_res_mc;                                                                       
      mIntermedTau->GetExciMc(down_res_mc);                                                        
                                                                                                   
      /* Generate MC of modes that must be contracted */                                           
      ModeCombi imp_mc;                                                                            
      imp_mc.InFirstOnly(l_mc, down_res_mc);                                                       
                                                                                                   
      /* Initialize wrapper for multi-index down contraction */                                    
      TensorGeneralDownContraction<T, DATA> gd_ctr(aEvalData, l_mc, imp_mc);                                
                                                                                                   
      NiceTensor<T> opit_res;                                                                     
      NiceTensor<T> l_res;                                                                        
      std::vector<In> opers(mCmodes.size());                                                       

      while (  intermeds.GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, opit_res)       
            )                                                                                      
      {                                                                                            
         /* check if we can assign modes... if not continue to next term */                        
         if (  !mIntermedL->AssignConcreteModesOpers(mCmodes, opers)                               
            ) /* assign intermedL */                                                               
         {                                                                                         
            continue;                                                                              
         }                                                                                         
                                                                                                   
         bool assign_failed=false;                                                                 
         for(size_t i=0; i<mIntermeds.size(); ++i) /* assign intermeds */                          
         {                                                                                         
            if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, opers)                         
               )                                                                                   
            {                                                                                      
               assign_failed=true;                                                                 
               break; /* break for */                                                              
            }                                                                                      
         }                                                                                         
                                                                                                   
         /* if we can't assign, continue while */                                                  
         if (  assign_failed                                                                       
            )                                                                                      
         {                                                                                         
            continue;                                                                              
         }                                                                                         
                                                                                                   
         mIntermedTau->AssignConcreteModesOpers(mCmodes, opers); /* assign intermedtau */          
                                                                                                   
         incr_total();                                                                             
         incr_not_screened();                                                                      
                                                                                                   
         /* Get tensor type for initialization */
         auto tens_type = arRes.Tensor().Type();

         std::vector<unsigned> l_dims(l_mc.Size());
         for(size_t i=0; i<l_dims.size(); ++i)
         {
            l_dims[i] = aEvalData.NModals(l_mc.Mode(i)) - I_1;
         }

         /* Initialize l_res */
         l_res = l_dims.empty() ? NiceScalar<T>(0., tens_type == BaseTensor<T>::typeID::CANONICAL) : NiceTensor<T>(l_dims, tens_type);                                                                   

         /* calculate L term */                                                                    
         if (  intermeds.GetIntermed(*mIntermedL, aEvalData, l_res)                              
            )                                                                                      
         {                                                                                         
            /* Evaluate contraction product */
            std::vector<unsigned> imp_dims(imp_mc.Size());
            for(size_t i=0; i<imp_dims.size(); ++i)
            {
               imp_dims[i] = aEvalData.NModals(imp_mc.Mode(i)) - 1;
            }
            std::unique_ptr<TensorSumAccumulator<T> > imp_res = nullptr;
            if constexpr   (  midas::vcc::v3::DataAllowsDecompositionV<evaldata_t>
                           )
            {
               imp_res = std::make_unique<TensorSumAccumulator<T> >
                                          (  imp_dims
                                          ,  aEvalData.GetDecomposer()
                                          ,  aEvalData.GetAllowedRank()
                                          );
            }
            else
            {
               imp_res = std::make_unique<TensorSumAccumulator<T> >
                                          (  imp_dims
                                          );
            }
            this->EvalIntermeds(aEvalData, imp_mc, *imp_res, opit_res);                                

            // Evaluate sum after sum accumulation
            auto imp_tens = imp_res->EvaluateSum();

            /* Perform general down contraction of <L| and [ctr. prod.] */                            
            auto down_res = gd_ctr.Contract(l_res, imp_tens);

            /* Act to the left with the excitation (tau) operator */                                  
            if (  !mIntermedTau->ActLeft(aEvalData, down_res, arRes)                                   
               )                                                                                      
            {                                                                                         
               MIDASERROR("IntermedProdL<T, DATA>::LoopOperators(): mIntermedTau->ActLeft() failed.");         
            }                                                                                         
         }
      }                                                                                            
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}                                                                                               

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  GeneralMidasVector<T>& arRes
   ,  const GeneralMidasVector<T>& aOpItRes
   )
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<GeneralMidasVector>();

      // Initialize quantities for doing direct products of intermediates.
      const In n_in = mIntermeds.size();
      std::vector<GeneralMidasVector<T>> vecs(n_in+I_1);
      std::vector<ModeCombi> mcs(n_in+I_1);
      In cur_vec = I_0;

      T coef = mCoef;
  
      // Take care of value of intermediate used for looping over operators. 
      if (aOpItRes.Size() == I_1)
         coef *= aOpItRes[I_0];
      else
      {
         vecs[cur_vec].Reassign(aOpItRes);
         mOpIterIntermed->GetExciMc(mcs[cur_vec]);
         cur_vec++;
      }


      // Loop over and evaluate remaining intermediates.
      for (In i=I_0; i<mIntermeds.size(); ++i)
      {
         if (! intermeds.GetIntermed(*mIntermeds[i], aEvalData, vecs[cur_vec]))
         {
            return;
         }

         if (vecs[cur_vec].Size() == I_1)
         {
            coef *= vecs[cur_vec][I_0];
         }
         else
         {
            mIntermeds[i]->GetExciMc(mcs[cur_vec]);
            cur_vec++;
         }
      }

      // Test for simple cases.
      if (I_0 == cur_vec)
      {
         GeneralMidasVector<T> one(I_1, C_1);
         arRes += coef * one;
         return;
      }
      else if (I_1 == cur_vec)
      {
         arRes += coef * vecs[I_0];
         return;
      }
 
      // Do direct product...
      vecs.erase(vecs.begin()+cur_vec, vecs.end());   // The last vectors are not used.
      
      std::vector<In> res_modals(aMp.Size());
      for (In i=I_0; i<aMp.Size(); ++i)
         res_modals[i] = aEvalData.NModals(aMp.Mode(i))-I_1;

      std::vector<std::vector<In> > modals(cur_vec);
      for (In i=I_0; i<cur_vec; ++i)
      {
         modals[i].resize(mcs[i].Size());
         for (In k=I_0; k<mcs[i].Size(); ++k)
            modals[i][k] = aEvalData.NModals(mcs[i].Mode(k))-I_1;
      } 
  
      dirprod::DirProd2(arRes, aMp, res_modals, vecs, mcs, modals, coef);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}


/**
 * Evaluate intermediates for NiceTensor
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  NiceTensor<T>& arRes
   ,  const NiceTensor<T>& aOpItRes
   )
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      if (  aOpItRes.IsNullPtr()
         )
      {
         MidasWarning("IntermedProdL<T, DATA>::EvalIntermeds(): aOpItRes is not initialized!");
      }

      // create tensor product accumulator to take care of direct products
      const In n_in = mIntermeds.size();
      TensorProductAccumulator<T> tensor_accumulator(mCoef, n_in);
      ModeCombi op_mc;
      mOpIterIntermed->GetExciMc(op_mc);
      tensor_accumulator.AddTensor(std::move(const_cast<NiceTensor<T>&>(aOpItRes)), std::move(op_mc));

      // Loop over and evaluate remaining intermediates.
      for(size_t i = 0; i < mIntermeds.size(); ++i)
      {
         NiceTensor<T> intermed_result;
         ModeCombi mc;

         // get/calc intermed
         if (  !intermeds.GetIntermed(*mIntermeds[i], aEvalData, intermed_result)
            )
         {
            return;
         }

         mIntermeds[i]->GetExciMc(mc); // get intermed mode combi
         tensor_accumulator.AddTensor(std::move(intermed_result), std::move(mc));
      }

      // Do direct product
      tensor_accumulator.DirProd(arRes, aMp);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}

/**
 * Evaluate intermediates for TensorSumAccumulator<T>
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  TensorSumAccumulator<T>& arRes
   ,  const NiceTensor<T>& aOpItRes
   )
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      // create tensor product accumulator to take care of direct products
      const In n_in = mIntermeds.size();
      std::unique_ptr<TensorProductAccumulator<T> > tensor_accumulator = nullptr;
      if constexpr   (  midas::vcc::v3::DataAllowsDecompositionV<evaldata_t>
                     )
      {
         tensor_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                 (  mCoef
                                 ,  n_in
                                 ,  aEvalData.TensorProductScreening().first
                                 ,  aEvalData.TensorProductScreening().second
                                 ,  aEvalData.TensorProductLowRankLimit()
                                 );
      }
      else
      {
         tensor_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                 (  mCoef
                                 ,  n_in
                                 );
      }
      ModeCombi op_mc;
      mOpIterIntermed->GetExciMc(op_mc);
      tensor_accumulator->AddTensor(std::move(const_cast<NiceTensor<T>&>(aOpItRes)), std::move(op_mc));

      // Loop over and evaluate remaining intermediates.
      for(size_t i = 0; i < mIntermeds.size(); ++i)
      {
         NiceTensor<T> intermed_result;
         ModeCombi mc;

         // get/calc intermed
         if (  !intermeds.GetIntermed(*mIntermeds[i], aEvalData, intermed_result)
            )
         {
            return;
         }

         mIntermeds[i]->GetExciMc(mc); // get intermed mode combi
         tensor_accumulator->AddTensor(std::move(intermed_result), std::move(mc));
      }

      // Do direct product
      tensor_accumulator->DirProd(arRes, aMp);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}


/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::GetAllModes
   (  std::vector<In>& aModes
   )  const
{
   std::vector<In> modes;                           // All modes in intermediate product.
   mOpIterIntermed->GetModes(modes);
   mIntermedTau->GetModes(modes);
   for (In i=I_0; i<mIntermeds.size(); ++i)
      mIntermeds[i]->GetModes(modes);
   std::sort(modes.begin(), modes.end());
   modes.erase(std::unique(modes.begin(), modes.end()), modes.end());
   std::copy(modes.begin(), modes.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::GetAllOccModes
   (  std::vector<In>& aModes
   )  const
{
   std::vector<In> occ_modes;                       // All occupied modes in this intermediate product.
   mOpIterIntermed->GetOccModes(occ_modes);
   mIntermedTau->GetOccModes(occ_modes);
   for (In i=I_0; i<mIntermeds.size(); ++i)
      mIntermeds[i]->GetOccModes(occ_modes);
   std::sort(occ_modes.begin(), occ_modes.end());
   occ_modes.erase(std::unique(occ_modes.begin(), occ_modes.end()), occ_modes.end());
   std::copy(occ_modes.begin(), occ_modes.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::GetAllExciModes
   (  std::vector<In>& aModes
   )  const
{
   std::vector<In> exci_modes;                      // Excited modes in intermediate product.
   mOpIterIntermed->GetExciModes(exci_modes);
   mIntermedTau->GetExciModes(exci_modes);
   for (In i=I_0; i<mIntermeds.size(); ++i)
      mIntermeds[i]->GetExciModes(exci_modes);
   std::sort(exci_modes.begin(), exci_modes.end());
   exci_modes.erase(std::unique(exci_modes.begin(), exci_modes.end()), exci_modes.end());
   std::copy(exci_modes.begin(), exci_modes.end(), std::back_inserter(aModes));
}  

namespace detail
{
/**
 * Utility function used by IntermedProd::IdentifyCmbIntermeds().
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
int IdentifyCmbIntermeds_CmpUtil_L
   (  const std::pair<std::unique_ptr<IntermedProdL<T, DATA> >, std::vector<Scaling> >& aL
   ,  const std::pair<std::unique_ptr<IntermedProdL<T, DATA> >, std::vector<Scaling> >& aR
   )
{
   return aL.second < aR.second;
}
} /* namespace detail */

/**
 * Public interface function for identifying cmb. intermediates.
 * The specific choice of cmb. intermediates is determined by the lowest
 * possible scaling wrt. the number of modes.
 *
 * aMaxScaling will be set to the maximum resulting scaling including intermediates.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::IdentifyCmbIntermeds
   (  Scaling& aMaxScaling
   ,  bool aCpTensors
   )
{
   aMaxScaling.Reset();
   std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > > final_list;
   std::vector<Scaling> tmp;
   IdentifyCmbIntermeds(midas::util::DynamicCastUniquePtr<IntermedProdL<T, DATA> >(this->Clone()), tmp, final_list, aCpTensors);
   std::sort(final_list.begin(), final_list.end(), detail::IdentifyCmbIntermeds_CmpUtil_L<T, DATA>);
   
   *this = *(final_list[0].first);
   UpdateInternalState();
   aMaxScaling = final_list[0].second.front();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::IdentifyCmbIntermeds
   (  std::unique_ptr<IntermedProdL>&& aImProd
   ,  std::vector<Scaling>& aScalings
   ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
   ,  bool aCpTensors
   )  const
{
   using imp_t = std::unique_ptr<IntermedProdL<T, DATA> >;

   std::vector<In> all_exci_modes;
   std::vector<In> all_occ_modes;
   GetAllExciModes(all_exci_modes);
   GetAllOccModes(all_occ_modes);

   bool improved = false;
   for (In i_sum=I_0; i_sum<aImProd->mSums.size(); ++i_sum)
   {
      const ModeSum& sum = aImProd->mSums[i_sum];
      if (! sum.mRestrict.empty())
         continue;

      // Find intermediate corresponding to this sum.
      for (In im=I_0; im<aImProd->mIntermeds.size(); ++im)
      {
         std::vector<In> exci_modes;
         std::vector<In> occ_modes;
         aImProd->mIntermeds[im]->GetExciModes(exci_modes);
         aImProd->mIntermeds[im]->GetOccModes(occ_modes);
         std::sort(exci_modes.begin(), exci_modes.end());
         std::sort(occ_modes.begin(), occ_modes.end());
         std::vector<In> tmp;
         std::set_intersection(sum.mModes.begin(), sum.mModes.end(), all_occ_modes.begin(), all_occ_modes.end(), std::back_inserter(tmp));
         if (  std::includes(exci_modes.begin(), exci_modes.end(), sum.mModes.begin(), sum.mModes.end())
            && !std::includes(all_occ_modes.begin(), all_occ_modes.end(), sum.mModes.begin(), sum.mModes.end()) && tmp.empty()
            )
         {
            improved = AddCmbIntermedL(std::forward<imp_t&>(aImProd), aScalings, aFinalList, i_sum, im, all_occ_modes, aCpTensors);
         }
         else if  (  std::includes(occ_modes.begin(), occ_modes.end(), sum.mModes.begin(), sum.mModes.end())
                  && !std::includes(all_exci_modes.begin(), all_exci_modes.end(), sum.mModes.begin(), sum.mModes.end())
                  )
         {
            improved = AddCmbIntermed(std::forward<imp_t&>(aImProd), aScalings, aFinalList, i_sum, im, all_exci_modes, aCpTensors);
         }
      }
   }

   // If we have improved the scaling, this intermediate product is no longer relevant.
   // Otherwise, add to list of candidates for final product.
   if (  !improved
      )
   {
      aImProd->mAssumeOpItStored = true;
      Scaling sc = aImProd->GetScaling(aCpTensors);
      aImProd->mAssumeOpItStored = false;
      aScalings.push_back(sc);
      std::sort(aScalings.rbegin(), aScalings.rend());
      aFinalList.emplace_back(std::make_pair(std::move(aImProd), aScalings));
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedProdL<T, DATA>::AddCmbIntermed
   (  std::unique_ptr<IntermedProdL>& aImProd
   ,  std::vector<Scaling>& aScalings
   ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
   ,  In aSumIdx
   ,  In aImIdx
   ,  const std::vector<In>& aExciModes
   ,  bool aCpTensors
   )  const
{
   std::vector<In> occ_it;
   std::vector<In> occ_im;
   std::vector<In> occ_cmn;
   std::vector<In> occ_restrict;
   aImProd->mOpIterIntermed->GetOperOccModes(occ_it);
   aImProd->mIntermeds[aImIdx]->GetOperOccModes(occ_im);
   std::sort(occ_it.begin(), occ_it.end());
   std::sort(occ_im.begin(), occ_im.end());
   std::set_intersection(occ_it.begin(), occ_it.end(), occ_im.begin(), occ_im.end(),
                    std::back_inserter(occ_cmn));
   std::set_intersection(occ_cmn.begin(), occ_cmn.end(), aExciModes.begin(), aExciModes.end(),
                    std::back_inserter(occ_restrict));
   auto cmb = std::make_unique<IntermedCmb<T, DATA> >
      (  midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aImProd->mOpIterIntermed->Clone())
      ,  aImProd->mIntermeds[aImIdx]->Clone()
      ,  occ_restrict
      );
  
   // Check if scaling is reduced. 
   cmb->SetAssumeOpItStored(true);
   aImProd->mAssumeOpItStored = true;
   Scaling cmb_sc(cmb->GetScaling(aCpTensors));
   Scaling imp_sc(aImProd->GetScaling(aCpTensors));
   cmb->SetAssumeOpItStored(false);
   aImProd->mAssumeOpItStored = false;
   Scaling max_sc(imp_sc);
   if (! aScalings.empty())
      max_sc = std::max(imp_sc, aScalings.front());

   if (  cmb_sc < max_sc
      )
   {
      auto cp = midas::util::DynamicCastUniquePtr<IntermedProdL<T, DATA> >(aImProd->Clone());
      cp->mOpIterIntermed = std::move(cmb);
      cp->mIntermeds.erase(cp->mIntermeds.begin()+aImIdx);
      cp->mSums.erase(cp->mSums.begin()+aSumIdx);
      std::vector<Scaling> cp_sc(aScalings);
      cp_sc.push_back(cmb_sc);
      std::sort(cp_sc.rbegin(), cp_sc.rend());
      IdentifyCmbIntermeds(std::move(cp), cp_sc, aFinalList, aCpTensors);
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedProdL<T, DATA>::AddCmbIntermedL
   (  std::unique_ptr<IntermedProdL>& aImProd
   ,  std::vector<Scaling>& aScalings
   ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
   ,  In aSumIdx
   ,  In aImIdx
   ,  const std::vector<In>& aOccModes
   ,  bool aCpTensors
   )  const
{
   auto im_t = aImProd->mIntermeds[aImIdx]->Clone();
   if (  !dynamic_cast<IntermedT<T, DATA>* >(im_t.get())
      )
   {
      Mout << " CmbL: Hit up contraction!" << std::endl;
      return false;
   }

   // If already combined, we do not combine further
   if (  dynamic_cast<IntermedCmbL<T, DATA>* >(aImProd->mIntermedL.get())
      )
   {
      return false;
   }
   
   auto cmb = std::make_unique<IntermedCmbL<T, DATA> >
      (  midas::util::DynamicCastUniquePtr<IntermedL<T, DATA> >(aImProd->mIntermedL->Clone())
      ,  midas::util::DynamicCastUniquePtr<IntermedT<T, DATA> >(std::move(im_t))
      ,  aOccModes
      );

   // Check if scaling is reduced.
   aImProd->mAssumeOpItStored = true;
   Scaling cmb_sc(cmb->GetScaling(aCpTensors));
   Scaling imp_sc(aImProd->GetScaling(aCpTensors));
   aImProd->mAssumeOpItStored = false;
   Scaling max_sc(imp_sc);
   if (! aScalings.empty())
      max_sc = std::max(imp_sc, aScalings.front());

   if (cmb_sc < max_sc)
   {
      auto cp = midas::util::DynamicCastUniquePtr<IntermedProdL<T, DATA> >(aImProd->Clone());
      cp->mIntermedL = std::move(cmb);
      cp->mIntermeds.erase(cp->mIntermeds.begin()+aImIdx);
      cp->mSums.erase(cp->mSums.begin()+aSumIdx);
      std::vector<Scaling> cp_sc(aScalings);
      cp_sc.emplace_back(cmb_sc);
      std::sort(cp_sc.rbegin(), cp_sc.rend());
      IdentifyCmbIntermeds(std::move(cp), cp_sc, aFinalList, aCpTensors);
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedProdL<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   if (  aCpTensors
      )
   {
      MIDASERROR("Scaling of IntermedProdL not implemented for CP tensors!");
   }
   // Scaling wrt. number of modes.
   std::vector<In> modes;
   GetAllModes(modes);
   In m = modes.size();            // Scaling: M^(#modes).

   // Scaling wrt. number of operators in O^(#modes with assoc. operator index).
   std::vector<In> op_modes;
   mOpIterIntermed->GetOperOccModes(op_modes);
   mOpIterIntermed->GetOperExciModes(op_modes);
   In o = op_modes.size();

   // If the mOpIterIntermediate is not stored, some extra work may be required. 
   In iter_extra_m = I_0;
   In iter_extra_o = I_0;
   if (  !mAssumeOpItStored && mOpIterIntermed->RefNo() == -I_1
      )
   {
      iter_extra_m = mOpIterIntermed->GetCost(aCpTensors).mM;
      iter_extra_o = mOpIterIntermed->GetCost(aCpTensors).mO;
   }

   // If the mIntermedL is not stored, some extra work may be required. 
   In l_extra_m = I_0;
   In l_extra_o = I_0;
   if (  !mAssumeOpItStored
      && mIntermedL->RefNo() == -I_1
      )
   {
      l_extra_m = mIntermedL->GetCost(aCpTensors).mM;
      l_extra_o = mIntermedL->GetCost(aCpTensors).mO;
   }

   m += std::max(iter_extra_m, l_extra_m);
   o += std::max(iter_extra_o, l_extra_o);

   // Scaling wrt. number of modals: 
   In n = mIntermedL->GetNumber().mN;
   if (  !mAssumeOpItStored
      && mIntermedL->RefNo() == -I_1
      )
   {
      n = mIntermedL->GetScaling(aCpTensors).mN;
   }

   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      if (mIntermeds[i]->RefNo() == -I_1)
         n = std::max(n, mIntermeds[i]->GetScaling(aCpTensors).mN);
      else
         n = std::max(n, mIntermeds[i]->GetNumber().mN);
   }

   // Analyze scaling due to tau operator.
   In n_tau = mIntermedTau->ExciLevel();
   if (mIntermedTau->Nforw() != I_0)
      n_tau++;
   n = std::max(n, n_tau);

   return Scaling(m, o, n);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "\\LaTeX output not implemented for class IntermedProdL.";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedProdL<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   midas::stream::ScopedPrecision(5, aOut);
   aOut << std::fixed << std::showpos << mCoef << std::noshowpos << std::scientific;
   std::ostringstream os;
   for (In i=I_0; i<mSums.size(); ++i)
      os << " " << mSums[i];
   aOut << std::setw(30) << std::left << os.str();

   os.str("");
   os << *mIntermedL << " ";
   os << *mOpIterIntermed << " ";
   for (In i=I_0; i<mIntermeds.size(); ++i)
      os << *mIntermeds[i] << " ";
   if (  mIntermedTau
      )
      os << *mIntermedTau << " ";
   aOut << std::setw(60) << os.str();

   os.str(""); 
   for (In i=I_0; i<mModeGuides.size(); ++i)
      os << mModeGuides[i];
   aOut << std::setw(50) << os.str();

   // What about scaling with CP tensors?
   aOut << "   " << GetScaling();
           
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProdL<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   midas::stream::ScopedPrecision(16, aOut);
   aOut << "IPL '" << std::scientific << std::showpos << mCoef << std::noshowpos << "' '";
   std::ostringstream os;
   for (In i=I_0; i<mSums.size(); ++i)
   {
      mSums[i].WriteSpec(os);
      os << " ";
   }
   aOut << std::setw(50) << std::left << os.str();
   aOut << "' '";
   os.str("");
   mIntermedL->WriteSpec(os);
   os << " ";
   mOpIterIntermed->WriteSpec(os);
   os << " ";
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->WriteSpec(os);
      os << " ";
   }
   mIntermedTau->WriteSpec(os);
   aOut << std::setw(60) << os.str();
   aOut << "' '";
   for (In i=I_0; i<mModeGuides.size(); ++i)
      aOut << mModeGuides[i];
   aOut << "'";
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDPRODL_IMPL_H_INCLUDED */
