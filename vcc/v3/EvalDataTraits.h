/**
************************************************************************
* 
* @file    EvalDataTraits.h
*
* @date    20-03-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief   Utilities for checking the DATA template parameter in the v3 code.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef EVALDATATRAITS_H_INCLUDED
#define EVALDATATRAITS_H_INCLUDED

#include "util/IsDetected.h"
#include "vcc_cpg/BasicOper.h"

namespace midas::vcc::v3
{

namespace detail
{
// Check if DATA implements GetModalEnergies()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_modalenergies_type = decltype(std::declval<DATA>().GetModalEnergies(std::declval<Args>()...));

// Check if DATA implements GetOccModalOffset()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_occmodaloffset_type = decltype(std::declval<DATA>().GetOccModalOffset(std::declval<Args>()...));

// Check if DATA implements GetFockIntegrals()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_fockintegrals_type = decltype(std::declval<DATA>().GetFockIntegrals(std::declval<Args>()...));

// Check if DATA implements AllowAmplitudeScreening()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_allowamplitudescreening_type = decltype(std::declval<DATA>().AllowAmplitudeScreening(std::declval<Args>()...));

// Check if DATA implements ScreenIntermeds()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_screenintermeds_type = decltype(std::declval<DATA>().ScreenIntermeds(std::declval<Args>()...));

// Check if DATA implements ScreenIntermedsAmpsThresh()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_screenintermedsampsthresh_type = decltype(std::declval<DATA>().ScreenIntermedsAmpsThresh(std::declval<Args>()...));

// Check if DATA implements ScreenIntermedsThresh()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_screenintermedsthresh_type = decltype(std::declval<DATA>().ScreenIntermedsThresh(std::declval<Args>()...));

// Check if DATA implements GetNorm2
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_getnorm2_type = decltype(std::declval<DATA>().GetNorm2(std::declval<Args>()...));

// Check if DATA implements DecomposedTensors()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_decomposedtensors_type = decltype(std::declval<DATA>().DecomposedTensors(std::declval<Args>()...));

// Check if DATA implements TensorProductScreening
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_tensorproductscreening_type = decltype(std::declval<DATA>().TensorProductScreening(std::declval<Args>()...));

// Check if DATA implements TensorProductLowRankLimit
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_tensorproductlowranklimit_type = decltype(std::declval<DATA>().TensorProductLowRankLimit(std::declval<Args>()...));

// Check if DATA implements GetDecomposer
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_decomposer_type = decltype(std::declval<DATA>().GetDecomposer(std::declval<Args>()...));

// Check if DATA implements GetAllowedRank()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_allowedrank_type = decltype(std::declval<DATA>().GetAllowedRank(std::declval<Args>()...));

// Check if DATA implements GetGenDownCtrRecompressResult()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_gendownctrrecompress_type = decltype(std::declval<DATA>().GetGenDownCtrRecompressResult(std::declval<Args>()...));

// Check if DATA implements GetDecompInfo()
template
   <  typename DATA
   ,  typename... Args
   >
using data_has_getdecompinfo_type = decltype(std::declval<DATA>().GetDecompInfo(std::declval<Args>()...));

/**
 * Checks whether a DATA type allows for perturbative VCC methods in the V3 framework
 **/
template
   <  typename DATA
   >
struct DataAllowsVccPert
{
   static constexpr bool value   =  midas::util::IsDetectedV<data_has_modalenergies_type, DATA>
                                 && midas::util::IsDetectedV<data_has_occmodaloffset_type, DATA>
                                 && midas::util::IsDetectedV<data_has_fockintegrals_type, DATA>;
};

/**
 * Checks whether a DATA type allows for screening in the V3 framework
 **/
template
   <  typename DATA
   >
struct DataAllowsScreening
{
   static constexpr bool value   =  midas::util::IsDetectedV<data_has_allowamplitudescreening_type, DATA>
                                 && midas::util::IsDetectedV<data_has_screenintermeds_type, DATA>
                                 && midas::util::IsDetectedV<data_has_screenintermedsthresh_type, DATA>
                                 && midas::util::IsDetectedV<data_has_screenintermedsampsthresh_type, DATA>
                                 && midas::util::IsDetectedV<data_has_getnorm2_type, DATA, const OP, const In>;
};

/**
 * Check whether a DATA type allows for tensor decomposition in the V3 framework
 **/
template
   <  typename DATA
   >
struct DataAllowsDecomposition
{
   static constexpr bool value   =  midas::util::IsDetectedV<data_has_decomposedtensors_type, DATA>
                                 && midas::util::IsDetectedV<data_has_tensorproductscreening_type, DATA>
                                 && midas::util::IsDetectedV<data_has_tensorproductlowranklimit_type, DATA>
                                 && midas::util::IsDetectedV<data_has_decomposer_type, DATA>
                                 && midas::util::IsDetectedV<data_has_allowedrank_type, DATA>
                                 && midas::util::IsDetectedV<data_has_gendownctrrecompress_type, DATA>
                                 && midas::util::IsDetectedV<data_has_getdecompinfo_type, DATA>;
};

} /* namespace detail */

/**
 * Check if a given DATA class allows for using perturbative VCC methods such as VCC[2pt3]
 **/
template
   <  typename DATA
   >
inline constexpr bool DataAllowsVccPertV = detail::DataAllowsVccPert<DATA>::value;

/**
 * Check if a given DATA class allows for amplitude screening in the V3 framework
 **/
template
   <  typename DATA
   >
inline constexpr bool DataAllowsScreeningV = detail::DataAllowsScreening<DATA>::value;

/**
 * Check if a given DATA class allows for tensor decomposition in the V3 framework
 **/
template
   <  typename DATA
   >
inline constexpr bool DataAllowsDecompositionV = detail::DataAllowsDecomposition<DATA>::value;

/**
 * Check if data allows computation with given container type
 **/
template
   <  typename DATA
   >
inline constexpr bool DataAllowsMidasVectorV = DATA::allow_midasvector_t::value;
template
   <  typename DATA
   >
inline constexpr bool DataAllowsNiceTensorV = DATA::allow_nicetensor_t::value;

} /* namespace midas::vcc:v3 */

#endif /* EVALDATATRAITS_H_INCLUDED */
