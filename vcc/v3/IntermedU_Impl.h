/**
************************************************************************
* 
* @file                IntermedU_Impl.h
*
* Created:             07-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Up (u) contraction intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDU_IMPL_H_INCLUDED
#define INTERMEDU_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "Intermediate.h"
#include "IntermedU.h"
#include "IntermedProd.h"
#include "vcc/ModalIntegrals.h"
#include "tensor/Scalar.h"

#include "libmda/numeric/float_eq.h"

namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedU<T, DATA>::IntermedU
   ( const std::string& aSpec
   )
   :  Intermediate<T, DATA>()
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, mUps);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedU<T, DATA>::IntermedU
   (  const std::vector<In>& aModes
   )
   :  Intermediate<T, DATA>()
   ,  mUps(aModes)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedU<T, DATA>::IntermedU
   (  const IntermedU<T, DATA>& aOrig
   )
   :  Intermediate<T, DATA>(aOrig)
   ,  mUps(aOrig.mUps)
   ,  mCidx(aOrig.mCidx)
   ,  mExciMc(aOrig.mExciMc)
{
}

/**
 *
 **/
namespace intermed_u::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetMc
   (
   )
{
   static std::vector<In> mc;

   return mc;
}
}  /* namespace intermed_u::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedU<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   this->mCidx.clear();
   intermed_u::detail::GetMc<T, DATA>().resize(mUps.size());
   for (In m=I_0; m<mUps.size(); ++m)
   {
      mCidx.emplace_back(aCmodes[mUps[m]], aCopers[mUps[m]]);
      intermed_u::detail::GetMc<T, DATA>()[m] = aCmodes[mUps[m]];
   }
   std::sort(intermed_u::detail::GetMc<T, DATA>().begin(), intermed_u::detail::GetMc<T, DATA>().end());
   intermed_u::detail::GetMc<T, DATA>().erase(std::unique(intermed_u::detail::GetMc<T, DATA>().begin(), intermed_u::detail::GetMc<T, DATA>().end()), intermed_u::detail::GetMc<T, DATA>().end());
   if (intermed_u::detail::GetMc<T, DATA>().size() != mUps.size())
      return false;  // Some concrete modes are duplicated.
   mExciMc.ReInit(intermed_u::detail::GetMc<T, DATA>());
   std::sort(mCidx.begin(), mCidx.end());
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedU<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes.clear();
   aCmodes.resize(mCidx.size());
   for (In i=I_0; i<mCidx.size(); ++i)
      aCmodes[i] = mCidx[i].first;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedU<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   aCopers.clear();
   aCopers.resize(mCidx.size());
   for (In i=I_0; i<mCidx.size(); ++i)
      aCopers[i] = mCidx[i].second;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedU<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   GeneralMidasVector<T> vec_out(I_1, C_1);
   GeneralMidasVector<T> vec_in(I_1, C_1);
   ModeCombi mc_out;
   ModeCombi mc_in;

   for (In i_up=I_0; i_up<mCidx.size(); ++i_up)
   {
      In mode = mCidx[i_up].first;
      In oper = mCidx[i_up].second;
      vec_in.Reassign(vec_out, vec_out.Size());
      vec_out.SetNewSize(vec_in.Size() * (aEvalData.NModals(mode)-I_1));
      vec_out.Zero();
      mc_out.InsertMode(mode);
      aEvalData.GetIntegrals()->Contract(mc_out, mc_in, mode, oper, vec_out, vec_in, I_1);
      mc_in.InsertMode(mode);
   }
   aRes.Reassign(vec_out);
   
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedU<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   ModeCombi mode_combi;

   bool cp_scalar = false;

   if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                  )
   {
      cp_scalar = aEvalData.DecomposedTensors();
   }
   
   auto result = NiceTensor<T>(new Scalar<T>(1.0, cp_scalar));

   for (In i_up = I_0; i_up < mCidx.size(); ++i_up)
   {
      In mode = mCidx[i_up].first;
      In oper = mCidx[i_up].second;
      mode_combi.InsertMode(mode);
      result = aEvalData.GetIntegrals()->ContractUp(mode, oper, mode_combi.IdxNrForMode(mode), IntegralOpT::STANDARD, result);
   }
   
   // return result
   aRes = std::move(result);

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedU<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   aOut << "u";
   aOut << "(";
   std::copy(mUps.begin(), mUps.end(), std::ostream_iterator<In>(aOut, ""));
   aOut << ")";
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedU<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "u^{";
   for (In i=I_0; i<mUps.size(); ++i)
      aOut << "m_{" << mUps[i] << "}";
   aOut << "}";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedU<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "u" << this->mUps;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedU<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedU<T, DATA>* const other = dynamic_cast<const IntermedU<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   return mUps.size() == other->mUps.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedU<T, DATA>::HighestModeNo
   (
   )  const
{
   In high = -I_1;
   for (In i=I_0; i<mUps.size(); ++i)
      if (mUps[i] > high)
         high = mUps[i];
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedU<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   if (  aCpTensors
      )
   {
      return Scaling(mUps.size(), mUps.size(), I_0);
   }
   else
   {
      return Scaling(mUps.size(), mUps.size(), mUps.size());
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedU<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{     
   if (  aCpTensors
      )
   {
      return Scaling(I_0, I_0, I_0);
   }
   else
   {
      return Scaling(I_0, I_0, mUps.size());
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedU<T, DATA>::GetNumber
   (
   )  const
{
   return Scaling(mUps.size(), mUps.size(), mUps.size());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedU<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      using ContT = typename std::remove_reference_t<decltype(*aEvalData.GetIntegrals())>::ContT;
      aNorm2 = 1.0;
      for(concrete_iter iter=begin_concrete(); iter!=end_concrete(); ++iter)
      {
         aNorm2*=aEvalData.GetIntegrals()->GetNorm2(iter->first,iter->second,ContT::UP);
      }
      
      return true;
   }
   else
   {
      return false;
   }
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDU_IMPL_H_INCLUDED */
