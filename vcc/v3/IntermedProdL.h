/**
************************************************************************
* 
* @file                IntermedProdL.h
*
* Created:             15-03-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Representing a product of intermediates
*                      for left-hand transformations.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDPRODL_H_INCLUDED
#define INTERMEDPRODL_H_INCLUDED

#include "IntermedProdL_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "IntermedProdL_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* INTERMEDPRODL_H_INCLUDED */
