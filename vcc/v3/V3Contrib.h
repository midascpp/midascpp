/**
************************************************************************
* 
* @file                V3Contrib.h
*
* Created:             14-03-2019
*
* Author:              Niels Kristian Madsen (nielksm@chem.au.dk)
*
* Short Description:   Basic interface for contributions used by V3
*                      transformer.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef V3CONTRIB_H_INCLUDED
#define V3CONTRIB_H_INCLUDED

#include "V3Contrib_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "V3Contrib_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* V3CONTRIB_H_INCLUDED */
