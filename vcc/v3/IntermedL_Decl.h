/**
************************************************************************
* 
* @file                IntermedL.h
*
* Created:             02-12-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Interface for IntermedL used in left-hand
*                      transformations. 
*                      Intermediate for getting elements of L vector
* 
* Last modified: Mon Mar 22, 2010  12:58PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDL_DECL_H_INCLUDED
#define INTERMEDL_DECL_H_INCLUDED

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "Intermediate.h"
#include "Scaling.h"

namespace midas::vcc::v3
{

template
   <  typename T
   >
class VccEvalData;

/**
 * Intermediate corresponding to a set of coefficients in the
 * <Lambda| bra state of a left-hand transformation.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedL
   :  public Intermediate<T, DATA>
{
   public:
      using evaldata_t = DATA<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      std::vector<In>   mModes;    ///< Modes specifying coefficients.
      ModeCombi         mExciMc;   ///< MC for concrete excited modes.
   
   public:
      IntermedL(): Intermediate<T, DATA>() {} 
      IntermedL(const std::string& aSpec);
      IntermedL(const std::vector<In>& aModes);
      IntermedL(const IntermedL& aOrig);
      virtual std::unique_ptr<Intermediate<T, DATA> > Clone
         (
         )  const override
      {
         return std::make_unique<IntermedL<T, DATA> >(*this);
      }
      
      virtual bool AssignConcreteModesOpers(const std::vector<In>& aCmodes, const std::vector<In>& aCopers) override;
      virtual void GetConcreteModes(std::vector<In>& aCmodes) const override;

      //! Evaluate interface
      //!@{
      virtual bool Evaluate
         (  const evaldata_t&
         ,  GeneralMidasVector<T>&
         )  const override;

      virtual bool Evaluate
         (  const evaldata_t&
         ,  NiceTensor<T>&
         )  const override;
      //!@}

      virtual std::ostream& Print(std::ostream& aOut) const override;
      virtual void Latex(std::ostream& aOut) const override;
      virtual void WriteSpec(std::ostream& aOut) const override;
      virtual In HighestModeNo() const override;
      virtual bool CmpKind(const Intermediate<T, DATA>* const aIntermed) const override;
      virtual Scaling GetScaling(bool=false) const override;
      virtual Scaling GetCost(bool=false) const override;
      virtual Scaling GetNumber() const override;
      
      virtual void GetOperExciModes(std::vector<In>& aModes) const override {}
      virtual void GetOccModes(std::vector<In>& aModes) const override {}
      virtual void GetOperOccModes(std::vector<In>& aModes) const override {}
      virtual bool OnlyAmps() const override {return false;} 
      virtual void GetConcreteOpers(std::vector<In>& aCopers) const override {};

      virtual void GetExciMc
         (  ModeCombi& aMc
         )  const override
      {
         aMc = mExciMc;
      }

      virtual In ExciLevel
         (
         )  const override
      {
         return mModes.size();
      }

      virtual void GetModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }
      
      virtual void GetExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }
     
      virtual void GetPureExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mModes.begin(), mModes.end(), std::back_inserter(aModes));
      }
      
      virtual bool Norm2(const evaldata_t&, real_t& aNorm) const override;
      
      virtual std::string Type() const override { return "IntermedL"; }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDL_DECL_H_INCLUDED */
