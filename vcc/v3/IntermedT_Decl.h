#ifndef INTERMEDT_DECL_H_INCLUDED
#define INTERMEDT_DECL_H_INCLUDED

#include "Intermediate.h"

namespace midas::vcc::v3
{

/**
 * @class IntermedT
 *    Class representing an amplitude vector which may be down and forward contracted.
 *    The basic notation is t(e01)(f23)(d45) which means that a t(e012345) amplitude
 *    vector has been forward contracted on modes 2,3 and down contracted on modes 4,5.
 *    The special case t()()() corresponds to the reference coefficient.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedT
   :  public Intermediate<T, DATA>
{
   using evaldata_t = DATA<T>;
   using real_t = midas::type_traits::RealTypeT<T>;
   using concrete_iter = std::vector<std::pair<In, In>>::const_iterator;

   private:
      std::vector<In>                 mExci;     ///< Excited modes
      std::vector<In>                 mForw;     ///< Forward contracted modes
      std::vector<In>                 mDown;     ///< Down contracted modes
      OP                              mType;     ///< The type of excitation vector (T, R, P, ...).
      std::vector<std::pair<In, In> > mCidx;     ///< Concrete modes and operators indices.
      ModeCombi                       mExciMc;   ///< MC for concrete excited modes.
      ModeCombi                       mTotMc;    ///< MC for all modes in excitation operator.
      
      std::vector<In> mMcTotTemp;
      
      concrete_iter begin_concrete_down() const
      { 
         concrete_iter iter = mCidx.begin();
         std::advance(iter,mExci.size()+mForw.size());
         return iter;
      }
      concrete_iter end_concrete_down() const
      { 
         return mCidx.end(); 
      }
      
      concrete_iter begin_concrete_forw() const
      {  
         concrete_iter iter = mCidx.begin();
         std::advance(iter,mExci.size());
         return iter;
      }
      concrete_iter end_concrete_forw() const
      {
         concrete_iter iter = mCidx.begin();
         std::advance(iter,mExci.size()+mForw.size());
         return iter;
      }  
      
   public:
      IntermedT();
      IntermedT(const std::string& aSpec, OP aType=OP::T);
      IntermedT(const CtrT& aCtrT);
      IntermedT(const IntermedT& aOrig);
      std::unique_ptr<Intermediate<T, DATA> > Clone() const override {return std::make_unique<IntermedT<T, DATA> >(*this);}
      
      bool AssignConcreteModesOpers(const std::vector<In>& aCmodes, const std::vector<In>& aCopers) override;
      void GetConcreteModes(std::vector<In>& aCmodes) const override;
      void GetConcreteOpers(std::vector<In>& aCopers) const override;
      void GetExciMc(ModeCombi& aMc) const override {aMc = mExciMc;}
      bool Evaluate(const evaldata_t& aEvalData, GeneralMidasVector<T>& aRes) const override;
      bool Evaluate(const evaldata_t& aEvalData, NiceTensor<T>& aRes) const override;
      In ExciLevel() const override;
      void GetModes(std::vector<In>& aModes) const override;
      In HighestModeNo() const override;
      void GetExciModes(std::vector<In>& aModes) const override;
      std::ostream& Print(std::ostream& aOut) const override;
      void Latex(std::ostream& aOut) const override;
      void WriteSpec(std::ostream& aOut) const override;
      
      void GetOccModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
      }

      void GetOperOccModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
      }
      
      void GetPureExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));
      }
      
      void GetOperExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mForw.begin(), mForw.end(), std::back_inserter(aModes));
      }

      In NpureExci() const {return mExci.size();}
      In Nforw() const {return mForw.size();}
      In Ndown() const {return mDown.size();}
      
      void GetForwModes
         (  std::vector<In>& aModes
         )  const
      {
         std::copy(mForw.begin(), mForw.end(), std::back_inserter(aModes));
      }

      void GetDownModes
         (  std::vector<In>& aModes
         )  const
      {
         std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
      }
      
      void SetType
         (  OP aType
         )
      {
         this->mType = aType;
      }
      
      bool CmpKind
         (  const Intermediate<T, DATA>* const aIntermed
         )  const override;
      
      bool OnlyAmps
         (
         )  const override
      {
         return mType == OP::T;
      }
      
      Scaling GetScaling(bool=false) const override;
      Scaling GetCost(bool=false) const override;
      Scaling GetNumber() const override;

      bool Norm2(const evaldata_t&, real_t&) const override;
      bool AmpsNorm2(const evaldata_t&, real_t&) const override;

      std::string Type() const override { return "IntermedT"; }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDT_DECL_H_INCLUDED */
