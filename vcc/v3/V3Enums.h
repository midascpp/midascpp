#ifndef V3ENUMS_H
#define V3ENUMS_H

enum V3 { V3UNKNOWN=0, GEN=1, VCC1PT2=2, VCC2=4, VCC2PT3=8, VCC3PT4F=16, VCC3PT4=32 };

struct V3_MAP
{
   static std::map<V3,std::string> create_map()
   {
      std::map<V3,std::string> m;
      m[V3::V3UNKNOWN] = "V3UNKNOWN";
      m[V3::GEN]       = "GEN";
      m[V3::VCC1PT2]   = "VCC1PT2";
      m[V3::VCC2]      = "VCC2";
      m[V3::VCC2PT3]   = "VCC2PT3";
      m[V3::VCC3PT4F]  = "VCC3PT4F";
      m[V3::VCC3PT4]   = "VCC3PT4";
      return m;
   }
   static const std::map<V3,std::string> MAP;
};

#endif /* V3ENUMS_H */
