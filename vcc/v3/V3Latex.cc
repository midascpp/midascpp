/**
 ************************************************************************
 * 
 * @file                V3Latex.h
 *
 * Created:             01-10-2008
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementation of LaTeX output from V3
 *                      transformer.
 * 
 * Last modified: Wed Sep 30, 2009  11:12AM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cctype>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "Intermediate.h"
#include "IntermediateOperIter.h"
#include "IntermedProd.h"
#include "IntermediateMachine.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "input/OpDef.h"
#include "util/DynamicCastUniquePtr.h"
#include "mpi/Impi.h"

namespace midas::vcc::v3
{

namespace latex
{
using intermeds_t = IntermediateMachine<Nb, GeneralMidasVector, VccEvalData>;
using intermedprod_t = IntermedProd<Nb, VccEvalData>;
} /* namespace latex */


void LatexHeader
   (  std::ostream& aOut
   ,  const VccCalcDef* const apVccCalcDef
   ,  const OpDef* const apOpDef
   )
{
   const In max_exci = apVccCalcDef->MaxExciLevel();
   aOut << "\\documentclass[12pt]{article}" << std::endl
        << "\\usepackage{amsbsy}" << std::endl
        << "\\usepackage{longtable}" << std::endl
        << "\\usepackage[landscape,a4paper,pdftex]{geometry}" << std::endl
        << "\\begin{document}" << std::endl
        << "\\noindent" << std::endl
        << "Maximum H coupling level: "   << apOpDef->McLevel() << "\\\\" << std::endl
        << "Maximum T excitation level: " << max_exci << "\\\\" << std::endl
        << "Using $T_1$-transformed H: "  << StringForBool(apVccCalcDef->T1TransH())
        << "\\\\ \\\\" << std::endl << std::endl;

   // Columns: Coef., Sums, Ctr. Prod., Scaling
   aOut << "{\\bf Intermediate products:}" << std::endl
        << "\\begin{longtable}[l]{lllll}" << std::endl
        << "\\hline \\hline" << std::endl;
}

/**
 * Print contraction products for this commutator sorted according to basic operator product.
 **/
void LatexCommutator
   (  std::ostream& aOut
   ,  Commutator& aCmt
   ,  std::vector<BasicOperProd>& aBops
   ,  std::vector<std::vector<CtrProd> >& aCprs
   ,  In aProjLvl
   ,  latex::intermeds_t& aImMachine
   ,  In& aCount
   )
{
   // Check if we should print anything.
   bool print_cmt = false;
   std::vector<bool> print_bop(aBops.size(), false);
   for (In i=I_0; i<aBops.size(); ++i)
      for (In j=I_0; j<aCprs[i].size(); ++j)
         if (aCprs[i][j].ExciLevel() == aProjLvl)
         {
            print_cmt = true;
            print_bop[i] = true;
         }

   // Do the actual printing.
   if (print_cmt)
   {
      aOut << "\\multicolumn{4}{l}{$ \\qquad";
      aCmt.Latex(aOut);
      aOut << " $} \\\\" << std::endl;
   }

   for (In i=I_0; i<aBops.size(); ++i)
   {
      //if (print_bop[i] & aBops[i].Nfactors()!=I_1)
      if (print_bop[i] && (aBops[i].Nfactors()!=I_1))
      {
         aOut << "\\multicolumn{4}{l}{$ \\qquad \\qquad";
         aBops[i].Latex(aOut);
         aOut << " $} \\\\" << std::endl;
      }

      for (In j=I_0; j<aCprs[i].size(); ++j)
         if (aCprs[i][j].ExciLevel() == aProjLvl)
         {
            aOut << "\\qquad \\qquad \\qquad ";
            ostringstream os;
            aCprs[i][j].WriteV3ContribSpec(os);
            auto ip = midas::util::DynamicCastUniquePtr<latex::intermedprod_t>(V3Contrib<Nb, VccEvalData>::Factory(os.str()));
            ip->RegisterIntermeds(aImMachine);
            ip->Latex(aOut);
            aCount++;
            aOut << std::endl;
         }
   }
}

/**
 * Print intermediate products products for this commutator. Sum restrictions are removed
 * and therefore no sorting according to basic operator product.
 * aCmb: Identify combined intermediates.
 **/
void LatexCommutatorNoSumR
   (  std::ostream& aOut
   ,  Commutator& aCmt
   ,  std::vector<std::vector<CtrProd> >& aCprs
   ,  In aProjLvl
   ,  latex::intermeds_t& aImMachine
   ,  In& aCount
   ,  bool aCmb
   )
{
   // Remove sum restrictions.
   std::vector<CtrProd> cps;
   for (In i=I_0; i<aCprs.size(); ++i)
      for (In j=I_0; j<aCprs[i].size(); ++j)
         if (aCprs[i][j].ExciLevel() == aProjLvl)
            aCprs[i][j].RemoveSumRestrictions(cps);

   if (cps.empty())
      return;
   
   CtrProd::RemoveDuplicates(cps, true);

   // Do the actual printing.
   aOut << "\\multicolumn{4}{l}{$ \\qquad";
   aCmt.Latex(aOut);
   aOut << " $} \\\\" << std::endl;
           
   Scaling dummy_scaling; 
   for (In k=I_0; k<cps.size(); ++k)
   {
      aOut << "\\qquad \\qquad \\qquad ";
      ostringstream os;
      cps[k].WriteV3ContribSpec(os);
      auto ip = midas::util::DynamicCastUniquePtr<latex::intermedprod_t>(V3Contrib<Nb, VccEvalData>::Factory(os.str()));
      if (aCmb)
         ip->IdentifyCmbIntermeds(dummy_scaling);
      ip->RegisterIntermeds(aImMachine);
      ip->Latex(aOut);
      aCount++;
      aOut << std::endl;
   }
}

/**
 * Generate a LaTeX document with a table of V3 contributions for a general VCC transformation.
 **/
void LatexTableGenVcc
   (  const VccCalcDef* const apVccCalcDef
   ,  const OpDef* const apOpDef
   )
{
   Mout << " Generating LaTeX table of V3 contributions." << std::endl;

   const In max_exci = apVccCalcDef->MaxExciLevel();
   const In min_exci = apVccCalcDef->T1TransH()? I_2:I_1;

   latex::intermeds_t im_mach;
   latex::intermeds_t im_mach_nr;
   latex::intermeds_t im_mach_cmb;
  
   In count = I_0;
   In count_nr = I_0;
   In count_cmb = I_0;
   
   // Open latex file.
   auto stream_type = midas::mpi::OFileStream::StreamType::MPI_MASTER;
   midas::mpi::OFileStream ltx("V3contribs.tex", stream_type, ios_base::trunc);               // For initial contraction prods.
   midas::mpi::OFileStream ltx_nr("V3contribs_norestrict.tex", stream_type, ios_base::trunc); // Prods. with no sum restrict.
   midas::mpi::OFileStream ltx_cmb("V3contribs_cmb.tex", stream_type, ios_base::trunc);       // Prods. using cmb. intermeds.
  
   LatexHeader(ltx, apVccCalcDef, apOpDef);
   LatexHeader(ltx_nr, apVccCalcDef, apOpDef);
   LatexHeader(ltx_cmb, apVccCalcDef, apOpDef);
   
   std::vector<CtrProd> ctr_prods; 
   std::ostringstream os;
   for (In h_dim=I_1; h_dim<=apOpDef->McLevel(); ++h_dim)
   {
      // Generate BCH expansion.
      Commutator h_oper;
      h_oper.SetCoef(C_1);
      h_oper.AddOper(OP::H, h_dim);
      std::vector<Commutator> bch;
      h_oper.BCH(min_exci, max_exci, max_exci, bch);

      // Generate list of basic operator products for each commutator.
      std::vector<std::vector<BasicOperProd> > bops;
      bops.resize(bch.size());
      for (In i=I_0; i<bch.size(); ++i)
         bch[i].Expand(max_exci, bops[i]);

      // Get list of contraction products for each basic operator product.
      std::vector<std::vector<std::vector<CtrProd> > > cprs;
      cprs.resize(bch.size());
      for (In i=I_0; i<bch.size(); ++i)
      {
         cprs[i].resize(bops.size());
         for (In j=I_0; j<bops[i].size(); ++j)
         {
            bops[i][j].GetCtrProds(I_0, max_exci, cprs[i][j], false);
            for (In k=I_0; k<cprs[i][j].size(); ++k)
               cprs[i][j][k].ContractModeNumbers();
         }
      }
      
      // Now start printing for this H-coupling level.
      // Loop over maximum VCC excitation levels.
      for (In vcc_n=I_1; vcc_n<=max_exci; ++vcc_n)
      {
         os.str("");
         os << "\\multicolumn{5}{l}{$\\textbf{\\em{H}}_{" << h_dim << "}"
            << "\\textbf{, VCC[" << vcc_n << "]}$} \\\\" << std::endl
            << "\\hline" << std::endl;
         ltx << os.str();
         ltx_nr << os.str();
         ltx_cmb << os.str();

         // Loop over projection excitation levels.
         for (In mu_n=I_0; mu_n<=vcc_n; ++ mu_n)
         {
            os.str("");
            if (I_0 == mu_n)
            {
               os << "$ \\langle \\Phi_{\\boldsymbol{\\mathrm i}} | $ \\\\" << std::endl;
            }
            else
            {
               os << "$ \\langle \\mu^{";
               for (In m=I_0; m<mu_n+I_1; ++m)
                  os << "m'_{" << m << "}";
               os << "} | $ \\\\" << std::endl;
            }
            ltx << os.str();
            ltx_nr << os.str();
            ltx_cmb << os.str();

            if (mu_n<vcc_n && vcc_n!=I_1)
            {
               // Only print contributions including at least one T_(vcc_n) operator
               // since other contribs are printed at lower VCC levels.
               for (In i=I_0; i<bch.size(); ++i)
                  if (bch[i].MaxExciOperLevel() <= vcc_n && bch[i].IncludesExciOperLevel(vcc_n))
                  {
                     LatexCommutator(ltx, bch[i], bops[i], cprs[i], mu_n, im_mach, count);
                     LatexCommutatorNoSumR(ltx_nr, bch[i], cprs[i], mu_n, im_mach_nr,
                                           count_nr, false);
                     LatexCommutatorNoSumR(ltx_cmb, bch[i], cprs[i], mu_n, im_mach_cmb,
                                           count_cmb, true);
                  }
            }
            else
            {
               // Print all contributions at this projection level.
               for (In i=I_0; i<bch.size(); ++i)
                  if (bch[i].MaxExciOperLevel() <= vcc_n)
                  {
                     LatexCommutator(ltx, bch[i], bops[i], cprs[i], mu_n, im_mach, count);
                     LatexCommutatorNoSumR(ltx_nr, bch[i], cprs[i], mu_n, im_mach_nr,
                                           count_nr, false);
                     LatexCommutatorNoSumR(ltx_cmb, bch[i], cprs[i], mu_n, im_mach_cmb,
                                           count_cmb, true);
                  }
            }
         }
      }
   }

   os.str("");
   os << "\\hline \\hline" << std::endl
      << "\\end{longtable}" << std::endl << std::endl
      << "Total number of contributions: ";
   ltx << os.str() << count << std::endl << std::endl;
   ltx_nr << os.str() << count_nr << std::endl << std::endl;
   ltx_cmb << os.str() << count_cmb << std::endl << std::endl;
  
   im_mach.ListRegisteredIntermedsLatex(ltx);
   im_mach_nr.ListRegisteredIntermedsLatex(ltx_nr);
   im_mach_cmb.ListRegisteredIntermedsLatex(ltx_cmb);

   ltx << "\\end{document}" << std::endl;
   ltx_nr << "\\end{document}" << std::endl;
   ltx_cmb << "\\end{document}" << std::endl;

   ltx.close();
   int sys_ret = system("pdflatex V3contribs > pdflatex.err");
   sys_ret = system("pdflatex V3contribs_norestrict > pdflatex.err");
   sys_ret = system("pdflatex V3contribs_cmb > pdflatex.err");
   sys_ret = system("pdflatex V3Contribs > pdflatex.err");
   sys_ret = system("pdflatex V3contribs_norestrict > pdflatex.err");
   sys_ret = system("pdflatex V3contribs_cmb > pdflatex.err");
   if(sys_ret)
   {
      Mout << " system call failed " << std::endl;
   }
}

} /* namespace midas::vcc::v3 */
