/**
************************************************************************
* 
* @file                InstantiateV3.cc
*
* Created:             29-03-2019
*
* Author:              Niels Kristian Madsen (nielksm@chem.au.dk)
*
* Short Description:   Instantiation of templates used in V3 hierarchy
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <type_traits>
#include <complex>

#include "VccEvalData.h"
#include "td/tdvcc/trf/TimTdvccEvalData.h"
#include "td/mctdh/eom/McTdHEvalData.h"
#include "td/mctdh/eom/McTdHIntermediates.h"

#include "util/Io_fwd.h"

/**
 * Instantiate V3Contrib
 **/
#include "V3Contrib.h"
#include "V3Contrib_Impl.h"

#define INSTANTIATE_V3CONTRIB(T, DATA)       \
template class midas::vcc::v3::V3Contrib<T, DATA>;             \
namespace midas::vcc::v3 \
{ \
template std::ostream& operator<<(std::ostream&, const midas::vcc::v3::V3Contrib<T, DATA>&); \
}

// For VCC
INSTANTIATE_V3CONTRIB(Nb, midas::vcc::v3::VccEvalData);

// For TDVCC
INSTANTIATE_V3CONTRIB(Nb, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_V3CONTRIB(std::complex<Nb>, midas::tdvcc::TimTdvccEvalData);

// For MCTDH[n]
INSTANTIATE_V3CONTRIB(std::complex<Nb>, midas::mctdh::LinearRasTdHEvalData);

#undef INSTANTIATE_V3CONTRIB

/**
 * Instantiate IntermedProd
 **/
#include "IntermedProd.h"
#include "IntermedProd_Impl.h"

#define INSTANTIATE_INTERMEDPROD(T, DATA)             \
template class midas::vcc::v3::IntermedProd<T, DATA>;

// For VCC
INSTANTIATE_INTERMEDPROD(Nb, midas::vcc::v3::VccEvalData);

// For TDVCC
INSTANTIATE_INTERMEDPROD(Nb, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_INTERMEDPROD(std::complex<Nb>, midas::tdvcc::TimTdvccEvalData);

// For MCTDH[n]
INSTANTIATE_INTERMEDPROD(std::complex<Nb>, midas::mctdh::LinearRasTdHEvalData);

#undef INSTANTIATE_INTERMEDPROD

#define  INSTANTIATE_INTERMEDPROD_3(T, VECTOR, DATA)             \
template void midas::vcc::v3::IntermedProd<T, DATA>::RegisterIntermeds(midas::vcc::v3::IntermediateMachine<T, VECTOR, DATA>&);

// For VCC
INSTANTIATE_INTERMEDPROD_3(Nb, GeneralMidasVector, midas::vcc::v3::VccEvalData);
INSTANTIATE_INTERMEDPROD_3(Nb, NiceTensor, midas::vcc::v3::VccEvalData);

// For TDVCC
INSTANTIATE_INTERMEDPROD_3(Nb, NiceTensor, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_INTERMEDPROD_3(std::complex<Nb>, NiceTensor, midas::tdvcc::TimTdvccEvalData);

// For MCTDH[n]
INSTANTIATE_INTERMEDPROD_3(std::complex<Nb>, NiceTensor, midas::mctdh::LinearRasTdHEvalData);

#undef  INSTANTIATE_INTERMEDPROD_3

#define INSTANTIATE_INTERMEDPROD_DISABLE(T, DATA)             \
template void midas::vcc::v3::IntermedProd<T, DATA>::EvaluateLeft(const typename midas::vcc::v3::IntermedProd<T, DATA>::evaldata_t&, GeneralDataCont<T>&, std::enable_if_t<std::is_same_v<DATA<T>, VccEvalData<Nb> > >*);                                                              \
template void midas::vcc::v3::IntermedProd<T, DATA>::EvaluateMpLeft(const typename midas::vcc::v3::IntermedProd<T, DATA>::evaldata_t&, const ModeCombi&, const GeneralMidasVector<T>&, GeneralDataCont<T>&, std::enable_if_t<std::is_same_v<DATA<T>, VccEvalData<Nb> > >*);          \
template void midas::vcc::v3::IntermedProd<T, DATA>::DoSumLeft(const typename midas::vcc::v3::IntermedProd<T, DATA>::evaldata_t&, In, In, In, const ModeCombi&, const GeneralMidasVector<T>&, GeneralDataCont<T>&, std::enable_if_t<std::is_same_v<DATA<T>, VccEvalData<Nb> > >*);   \
template void midas::vcc::v3::IntermedProd<T, DATA>::LoopOperatorsLeft(const typename midas::vcc::v3::IntermedProd<T, DATA>::evaldata_t&, const ModeCombi&, const GeneralMidasVector<T>&, GeneralDataCont<T>&, std::enable_if_t<std::is_same_v<DATA<T>, VccEvalData<Nb> > >*);       \

INSTANTIATE_INTERMEDPROD_DISABLE(Nb, midas::vcc::v3::VccEvalData);

#undef INSTANTIATE_INTERMEDPROD_DISABLE

/**
 * Instantiate IntermedProdL
 **/
#include "IntermedProdL.h"
#include "IntermedProdL_Impl.h"

#define INSTANTIATE_INTERMEDPRODL(T, DATA)             \
template class midas::vcc::v3::IntermedProdL<T, DATA>;

INSTANTIATE_INTERMEDPRODL(Nb, midas::vcc::v3::VccEvalData);

// For TDVCC
INSTANTIATE_INTERMEDPRODL(Nb, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_INTERMEDPRODL(std::complex<Nb>, midas::tdvcc::TimTdvccEvalData);

#undef INSTANTIATE_INTERMEDPRODL

#define  INSTANTIATE_INTERMEDPRODL_3(T, VECTOR, DATA)             \
template void midas::vcc::v3::IntermedProdL<T, DATA>::RegisterIntermeds(midas::vcc::v3::IntermediateMachine<T, VECTOR, DATA>&);

INSTANTIATE_INTERMEDPRODL_3(Nb, GeneralMidasVector, midas::vcc::v3::VccEvalData);
INSTANTIATE_INTERMEDPRODL_3(Nb, NiceTensor, midas::vcc::v3::VccEvalData);

// For TDVCC
INSTANTIATE_INTERMEDPRODL_3(Nb, NiceTensor, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_INTERMEDPRODL_3(std::complex<Nb>, NiceTensor, midas::tdvcc::TimTdvccEvalData);

#undef  INSTANTIATE_INTERMEDPRODL_3


#endif /* DISABLE_PRECOMPILED_TEMPLATES */
