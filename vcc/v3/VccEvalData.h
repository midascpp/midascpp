/**
************************************************************************
* 
* @file                VccEvalData.h
*
* Created:             16-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating + cleanup
*
* Short Description:   Container for storing quantities needed in
*                      evaluation of V3 contributions.
* 
* Last modified: Thu Oct 15, 2009  02:30PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef EVALDATA_H_INCLUDED
#define EVALDATA_H_INCLUDED

#include <vector>
#include <iostream>
#include <memory>
#include <type_traits>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/Xvec.h"
#include "vcc/TensorDataCont.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "mmv/DataCont.h"
#include "vcc_cpg/BasicOper.h"
#include "tensor/TensorDecomposer.h"
#include "vcc/TransformerV3.h"
#include "vcc/Vcc.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/IntegralContractor.h"

namespace midas::vcc
{

namespace v3
{

template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
class IntermediateMachine;

/**
 * Forward declaration of VccEvalData
 **/
template
   <  typename T = Nb
   >
class VccEvalData;

/**
 * @class VccEvalData
 *    All data structures needed for the evaluation of an intermediate product.
 **/
template
   <
   >
class VccEvalData<Nb>
{
   using TensorDecomposer = midas::tensor::TensorDecomposer;
   using TransformerV3 = midas::vcc::TransformerV3;

   template
      <  template<typename> typename VECTOR
      >
   using intermeds_t = IntermediateMachine<Nb, VECTOR, VccEvalData>;

   /**
    *
    **/
   struct DataStruct
   {
      DataCont*  mpData;
      std::vector<Nb> mNorms2;

      DataStruct(DataCont* ptr): mpData(ptr), mNorms2() { }

      DataStruct& operator=(DataCont* cont) { mpData = cont; return *this; }

      operator bool() { return mpData; }
   };

   private:
      TransformerV3*                   mpTransformer;
      const Xvec* const                mpXvec;           ///< result vector
      const VccCalcDef* const          mpVccCalcDef;
      const Vcc* const                 mpVcc;
      const OpDef* const               mpOpDef;
      const ModalIntegrals<Nb>* const  mpInts;
      const ModalIntegrals<Nb>* const  mpFockInts;       ///< Fock operator integrals.

      DataStruct           mpAmps;           ///< T amplitudes.
      DataStruct           mpPvec;           ///< P vector coefficients used in VCC response.
      DataStruct           mpQvec;           ///< Q vector coefficients used in VCC response.
      DataStruct           mpRvec;           ///< R vector coefficients used in VCC response.
      DataStruct           mpSvec;           ///< S vector coefficients used in VCC response.
      DataStruct           mpLvec;           ///< Coefficients used for left-hand transformations.
      TensorDataCont* mpTensorAmps; 
      TensorDataCont* mpTensorPvec; 
      TensorDataCont* mpTensorQvec; 
      TensorDataCont* mpTensorRvec; 
      TensorDataCont* mpTensorSvec; 
      TensorDataCont* mpTensorLvec;

      TensorDecomposer*    mDecomposer;
      In                   mAllowedRank;

   public:
      using allow_midasvector_t = std::true_type;
      using allow_nicetensor_t = std::true_type;

      /**
       *
       **/
      VccEvalData
         (  TransformerV3* apTransformer
         ,  const Xvec* const aXvec
         ,  const VccCalcDef* const aVccCalcDef
         ,  const Vcc* const aVcc
         ,  const OpDef* const aOpDef
         ,  const ModalIntegrals<Nb>* const aInts
         ,  const ModalIntegrals<Nb>* const aFockInts = nullptr
         )
         :  mpTransformer(apTransformer)
         ,  mpXvec(aXvec)
         ,  mpVccCalcDef(aVccCalcDef)
         ,  mpVcc(aVcc)
         ,  mpOpDef(aOpDef)
         ,  mpInts(aInts)
         ,  mpFockInts(aFockInts)
         ,  mpAmps(nullptr)
         ,  mpPvec(nullptr)
         ,  mpQvec(nullptr)
         ,  mpRvec(nullptr)
         ,  mpSvec(nullptr)
         ,  mpLvec(nullptr)
         ,  mpTensorAmps(nullptr)
         ,  mpTensorPvec(nullptr)
         ,  mpTensorQvec(nullptr)
         ,  mpTensorRvec(nullptr)
         ,  mpTensorSvec(nullptr)
         ,  mpTensorLvec(nullptr)
         ,  mDecomposer(nullptr)
         ,  mAllowedRank(0)
      { 
         MidasAssert(mpTransformer, "Transformer is nullptr for VccEvalData!");
         MidasAssert(mpXvec, "Xvec is nullptr for VccEvalData!");
         MidasAssert(mpVccCalcDef, "VccCalcDef is nullptr for VccEvalData!");
         MidasAssert(mpVcc, "Vcc is nullptr for VccEvalData!");
      }
   
   private:
      /**
       *
       **/
      void CalcNorm2(DataStruct& aVec, std::string str) // std::string for debug
      {
         aVec.mNorms2.resize((*mpXvec).NexciTypes());
         for (In i_mc=I_0; i_mc<(*mpXvec).NexciTypes(); ++i_mc)
         {
            In addr   = mpXvec->ModeCombiAddress(i_mc);
            In n_exci = mpXvec->NexciForModeCombi(i_mc);
            aVec.mNorms2[i_mc] = aVec.mpData->Norm2(addr, n_exci);
         }
      }

      /**
       *
       **/
      const DataCont* const GetExciVecImpl
         (  const OP aType
         )  const
      {
         //Mout << " Getting EVALDATA OLD type: " << aType << std::endl;
         switch (aType)
         {
            case OP::T:
               return mpAmps.mpData;
            case OP::P:
               return mpPvec.mpData;
            case OP::Q:
               return mpQvec.mpData;
            case OP::R:
               return mpRvec.mpData;
            case OP::S:
               return mpSvec.mpData;
            case OP::L:
               return mpLvec.mpData;
            default:
               MIDASERROR("EvalData:.template GetExciVecImpl(): Unknown type.");
               return nullptr;
         }
      }

      /**
       *
       **/
      const TensorDataCont* const GetExciVecNewImpl
         (  const OP aType
         )  const
      {
         //Mout << " Getting EVALDATA NEW type: " << aType << std::endl;
         switch (aType)
         {
            case OP::T:
               return mpTensorAmps;
            case OP::P:
               return mpTensorPvec;
            case OP::Q:
               return mpTensorQvec;
            case OP::R:
               return mpTensorRvec;
            case OP::S:
               return mpTensorSvec;
            case OP::L:
               return mpTensorLvec;
            default:
               MIDASERROR("EvalData:.template GetExciVecNewImpl(): Unknown type.");
               return nullptr;
         }
      }

   
   public:
      
      /**
       * Interface functions.
       * These are needed in all DATA types used in V3Contrib and IntermediateMachine
       **/
      //!@{

      //! Get pointer to integrals
      const ModalIntegrals<Nb>* const GetIntegrals
         (
         )  const
      {
         return mpInts;
      }
      
      /**
       * Get intermediate machine from mpTransformer
       **/
      template
         <  template<typename> typename VECTOR
         >
      intermeds_t<VECTOR>& GetIntermediates
         (
         )  const
      {
         MidasAssert(mpTransformer, "Cannot get IntermediateMachine without transformer!");

         constexpr bool is_vec = std::is_same_v<VECTOR<Nb>, GeneralMidasVector<Nb> >;
         constexpr bool is_tens = std::is_same_v<VECTOR<Nb>, NiceTensor<Nb> >;

         static_assert(is_vec || is_tens, "VccEvalData::GetIntermediates only works for GeneralMidasVector or NiceTensor");

         if constexpr   (  is_vec
                        )
         {
            return mpTransformer->GetImMachine();
         }
         else if constexpr (  is_tens
                           )
         {
            return mpTransformer->GetTensorImMachine();
         }
      }

      /**
       * Get ModeCombiOpRange from Xvec
       **/
      const ModeCombiOpRange& GetModeCombiOpRange
         (
         )  const
      {
         return mpXvec->GetModeCombiOpRange();
      }

      /**
       * Get dimensions of tensor for mode combi
       *
       * @param aMcIdx
       * @return
       *    Dimensions of amplitude tensor
       **/
      const std::vector<unsigned>& GetTensorDims
         (  In aMcIdx
         )  const
      {
         return this->mpXvec->GetTensorXvecData().GetModeCombiData(aMcIdx).GetDims();
      }

      /**
       * Get total number of excitations in the vector
       *
       * @return
       *    Size of Xvec
       **/
      In NExci
         (
         )  const
      {
         return mpXvec->Size();
      }

      /**
       * Get number of excitation amplitudes for a given MC from mpXvec
       *
       * @param aMcIdx    Index of ModeCombi
       * @return
       *    Number of WF params for ModeCombi
       **/
      In NExciForModeCombi
         (  In aMcIdx
         )  const
      {
         return mpXvec->NexciForModeCombi(aMcIdx);
      }

      /**
       * Number of excitation types.
       *
       * @return
       *    NexciTypes from mpXvec
       **/
      In NExciTypes
         (
         )  const
      {
         return mpXvec->NexciTypes();
      }

      /**
       * Get number of modals for mode
       *
       * @param aMode
       * @return
       *    Number of modals
       **/
      In NModals
         (  LocalModeNr aMode
         )  const
      {
         return this->mpVccCalcDef->Nmodals(aMode);
      }

      /**
       * Get number of modals as vector
       *
       * @return
       *    N vector
       **/
      const auto& GetNModals
         (
         )  const
      {
         return this->mpVccCalcDef->GetNModals();
      }

      /**
       * Get number of modes in operator
       *
       * @return
       *    M
       **/
      In NModes
         (
         )  const
      {
         return this->mpOpDef->NmodesInOp();
      }

      /**
       * Get MC level of the operator
       * @return
       *    MC level
       **/
      In McLevel
         (
         )  const
      {
         return this->mpOpDef->McLevel();
      }

      /**
       * Operator coefficient
       * Wrapper that allows for defining evaldata_t without OpDef.
       *
       * @return
       *    c_t
       **/
      Nb OperatorCoef
         (  In aTerm
         )  const
      {
         return this->mpOpDef->Coef(aTerm);
      }

      /**
       * Terms for MC
       *
       * @param[in]  aMc            MC to find in MCR for operator
       * @param[out] arFirstTerm    First term for aMc
       * @param[out] arNterms       Number of terms for aMc
       * @return
       *    aMc found?
       **/
      bool TermsForMc
         (  const ModeCombi& aMc
         ,  In& arFirstTerm
         ,  In& arNterms
         )  const
      {
         return this->mpOpDef->TermsForMc(aMc, arFirstTerm, arNterms);
      }

      /**
       * Get operators for term
       *
       * @param aTerm
       * @return
       *    Vector of operator numbers in term
       **/
      const std::vector<LocalOperNr>& GetOpers
         (  In aTerm
         )  const
      {
         return this->mpOpDef->GetOpers(aTerm);
      }

      /**
       * Get excitation vector. Interface.
       *
       * @param aType      Type of vector
       * @return
       *    Pointer to correct vector
       **/
      template
         <  typename VEC
         >
      const VEC* const GetExciVec
         (  const OP aType
         )  const
      {
         constexpr bool is_tdc = std::is_same_v<VEC, TensorDataCont>;
         constexpr bool is_dc = std::is_same_v<VEC, DataCont>;

         static_assert(is_tdc || is_dc, "VccEvalData only contains pointers to TensorDataCont and DataCont");

         if constexpr   (  is_tdc
                        )
         {
            return this->GetExciVecNewImpl(aType);
         }
         else if constexpr (  is_dc
                           )
         {
            return this->GetExciVecImpl(aType);
         }
      }

      //! Is TransformerV3 (and thereby the IntermediateMachine) using NiceTensor?
      bool TensorTransform
         (
         )  const
      {
         return mpTransformer->TensorTransform();
      }

      //!@}


      /** @name Functions called when using real arithmetic (allowing CP decomposition, screening, and perturbative VCC methods) **/
      //!@{

      /**
       * Do decompositions of tensors?
       *
       * @return
       *    True/false
       **/
      bool DecomposedTensors
         (
         )  const
      {
         return this->mAllowedRank != 0;
      }

      //! Get modal energies
      const DataCont& GetModalEnergies
         (
         )  const
      {
         return mpVcc->GetEigVal();
      }

      //! Get occupied-modal offsets
      const std::vector<In>& GetOccModalOffset
         (
         )  const
      {
         return mpVcc->GetOccModalOffSet();
      }

      //! Get Fock integrals
      const ModalIntegrals<Nb>* const GetFockIntegrals
         (
         )  const
      {
         return this->mpFockInts;
      }

      /**
       * @return
       *    Try screening based on amplitudes?
       **/
      bool AllowAmplitudeScreening
         (
         )  const
      {
         return mpVccCalcDef->ScreenIntermedsAmps() && mpTransformer->GetType() == TRANSFORMER::VCCJAC;
      }

      /**
       * @return
       *    Screen intermediates
       **/
      bool ScreenIntermeds
         (
         )  const
      {
         return this->mpVccCalcDef->ScreenIntermeds();
      }

      /**
       * Threshold for screening amplitudes
       *
       * @return
       *    Threshold
       **/
      Nb ScreenIntermedsAmpsThresh
         (
         )  const
      {
         return mpVccCalcDef->ScreenIntermedsAmpsThresh();
      }

      /**
       * Threshold for screening intermeds
       *
       * @return
       *    Threshold
       **/
      Nb ScreenIntermedsThresh
         (
         )  const
      {
         return mpVccCalcDef->ScreenIntermedsThresh();
      }

      /**
       * Get norm2 of intermed.
       **/
      Nb GetNorm2
         (  const OP aType
         ,  const In aMc
         )  const
      {
         switch (aType)
         {
            case OP::T:
               return mpAmps.mNorms2[aMc];
            case OP::P:
               return mpPvec.mNorms2[aMc];
            case OP::Q:
               return mpQvec.mNorms2[aMc];
            case OP::R:
               return mpRvec.mNorms2[aMc];
            case OP::S:
               return mpSvec.mNorms2[aMc];
            case OP::L:
               return mpLvec.mNorms2[aMc];
            default:
               MIDASERROR("EvalData::GetNorm2(): Unknown type.");
               return 0;
         }
      }
      
      /**
       *
       **/
      const auto& TensorProductScreening
         (
         )  const
      {
         return mpTransformer->TensorProductScreening();
      }

      /**
       *
       **/
      In TensorProductLowRankLimit
         (
         )  const
      {
         return mpTransformer->TensorProductLowRankLimit();
      }

      /**
       *
       **/
      TensorDecomposer* GetDecomposer
         (
         )  const
      {
         return mDecomposer;
      }

      /**
       *
       **/
      In GetAllowedRank
         (
         )  const
      {
         return mAllowedRank;
      }

      /**
       *
       **/
      bool GetGenDownCtrRecompressResult
         (
         )  const
      {
         return mpVccCalcDef->GetGenDownCtrRecompressResult();
      }

      /**
       *
       **/
      const auto& GetDecompInfo
         (
         )  const
      {
         return mpTransformer->GetDecompInfo();
      }
      //!@}


      /** @name Functions called by TransformerV3 **/
      //!@{

      /**
       *
       **/
      void CalcNorms2() 
      {
         if (  mpXvec
            )
         {
            if (  mpAmps.mpData  ) CalcNorm2(mpAmps,"AMPS");
            if (  mpPvec.mpData  ) CalcNorm2(mpPvec,"PVEC");
            if (  mpQvec.mpData  ) CalcNorm2(mpQvec,"QVEC");
            if (  mpRvec.mpData  ) CalcNorm2(mpRvec,"RVEC");
            if (  mpSvec.mpData  ) CalcNorm2(mpSvec,"SVEC");
            if (  mpLvec.mpData  ) CalcNorm2(mpLvec,"LVEC");
         }
      }

      /**
       *
       **/
      void AssignDataVec
         (  const OP aType
         ,  DataCont* aData
         )
      {
         switch   (  aType
                  )
         {
            case OP::T:
               mpAmps = aData;
               break;
            case OP::P:
               mpPvec = aData;
               break;
            case OP::Q:
               mpQvec = aData;
               break;
            case OP::R:
               mpRvec = aData;
               break;
            case OP::S:
               mpSvec = aData;
               break;
            case OP::L:
               mpLvec = aData;
               break;
            default:
               MIDASERROR("EvalData::AssignDataVec(): Unknown type.");
         }
      }
      
      /**
       *
       **/
      void AssignTensorDataVec
         (  const OP aType
         ,  TensorDataCont* aData
         )
      {
         switch   (  aType
                  )
         {
            case OP::T:
               mpTensorAmps = aData;
               break;
            case OP::P:
               mpTensorPvec = aData;
               break;
            case OP::Q:
               mpTensorQvec = aData;
               break;
            case OP::R:
               mpTensorRvec = aData;
               break;
            case OP::S:
               mpTensorSvec = aData;
               break;
            case OP::L:
               mpTensorLvec = aData;
               break;
            default:
               MIDASERROR("EvalData::AssignDataVec(): Unknown type.");
         }
      }

      /**
       *
       **/
      void AssignDecomposer
         (  TensorDecomposer* aDecomposer
         )
      {
         mDecomposer = aDecomposer;
      }
      
      /**
       *
       **/
      void AssignAllowedRank
         (  In aAllowedRank
         )
      {
         mAllowedRank = aAllowedRank;
      }
      //!@}
};

} /* namespace v3 */

} /* namespace midas::vcc */

#endif /* EVALDATA_H_INCLUDED */
