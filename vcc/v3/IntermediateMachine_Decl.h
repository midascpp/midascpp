/**
************************************************************************
* 
* @file                IntermediateMachine_Decl.h
*
* Created:             09-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Class responsible for providing intermediates.
*                      Intermediates may be stores or calculated
*                      on the fly.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATEMACHINE_DECL_H_INCLUDED
#define INTERMEDIATEMACHINE_DECL_H_INCLUDED

#include <vector>
#include <map>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/v3/IntermediateRestrictions.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermediateOperIter.h"

#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "tensor/NiceTensor.h"

namespace midas::vcc::v3
{

namespace detail
{
/**
 * Address type
 **/
template
   <  typename T
   >
struct AddressTypeTraits
{
   using type = In;
};

/**
 * Pair of addresses, (begin, end), for a DataCont intermediate.
 **/
template
   <  typename T
   >
struct AddressTypeTraits<GeneralDataCont<T> >
{
   using type = std::pair<In, In>;
};

/**
 * Type of container the data is stored in for a given vector.
 **/
template
   <  typename T
   >
struct ContainerTypeTraits
{
   using type = T;
};

/**
 * For MidasVector, the data is stored in a DataCont
 **/
template
   <  typename T
   >
struct ContainerTypeTraits<GeneralMidasVector<T> >
{
   using type = GeneralDataCont<T>;
};

/**
 * For NiceTensor, the data is stored in a vector of NiceTensor%s
 **/
template
   <  typename T
   >
struct ContainerTypeTraits<NiceTensor<T> >
{
   using type = std::vector<NiceTensor<T> >;
};

} /* namespace detail */

/**
 * Maps vector of operators -> pairs of addresses.
 **/
template
   <  class T
   >
using OperAddrMap = std::map<std::vector<In>, T>;

/**
 * Maps vector of modes to operators.
 **/
template
   <  class T
   >
using ModeOperMap = std::map<std::vector<In>, OperAddrMap<T> >;

/**
 * Maps vector of modes to vector of (operators, address) pairs.
 * This is used for iterating over operators.
 **/
template
   <  class T
   >
using ModeOperVectorMap = std::map<std::vector<In>, std::vector<std::pair<std::vector<LocalOperNr>, T> > >;






/**
 * IntermediateMachine class declaration.
 *
 * T can be real or complex numbers.
 **/
template
   <  typename T = Nb
   ,  template<typename> typename VECTOR = GeneralMidasVector
   ,  template<typename...> typename DATA = VccEvalData
   >
class IntermediateMachine
{
   public:
      /** @name Static assertions **/
      //!@{
      static_assert(std::is_same_v<VECTOR<T>, GeneralMidasVector<T>> || std::is_same_v<VECTOR<T>, NiceTensor<T>>, "IntermediateMachine only works for MidasVector/DataCont or NiceTensor.");
      //!@}

      /** @name Alias **/
      //!@{
      using param_t = T;
      using real_t = midas::type_traits::RealTypeT<param_t>;
      using vec_t = VECTOR<param_t>;
      using data_t = typename detail::ContainerTypeTraits<vec_t>::type;
      using addr_t = typename detail::AddressTypeTraits<data_t>::type;
      using intermed_t = Intermediate<T, DATA>;
      using intermedptr_t = std::unique_ptr<intermed_t>;
      using opit_t = IntermediateOperIter<T, DATA>;
      using opitptr_t = std::unique_ptr<opit_t>;
      using evaldata_t = DATA<param_t>;
      //!@}

   private:   
      //! The registered "ordinary" intermediates.
      std::vector<intermedptr_t> mStoredIntermeds;
     
      //! Counts how many times an intermediate has been registered.
      std::vector<In> mStoredIntermedsCount;
   
      //! Maps modes -> opers -> addr. of intermediate.
      std::vector<ModeOperMap<addr_t> > mIntermedAddr;
   
      //! mIntermedVals[ref.no] contains intermediates.
      std::vector<data_t> mIntermedVals;
      
      //! Actual used size of mIntermedVals.
      std::vector<In> mIntermedValsSize;
    
      //! The registered "operator iterator" intermediates.
      std::vector<opitptr_t> mStoredOpIt;
   
      //! The number of times a given "operator iterator" intermediate has been registered.
      std::vector<In> mStoredOpItCount;
      
      /**
       * Maps modes -> vector of (operators,addr) pairs.
       * Used for IntermediateOperIter intermediates.
       **/
      std::vector<ModeOperVectorMap<addr_t> > mOpItAddr;
      
      //! Values of saved Intermediate operators
      std::vector<data_t> mOpItVals;
   
      //! ??
      std::vector<In> mOpItValsSize;
   
      //! Pointing to next set of operators for an IntermediateOperIter intermediate.
      typename std::vector<std::pair<std::vector<LocalOperNr>, addr_t> >::iterator mOperIter;
      
      //! Pointing to last set of operators for an IntermediateOperIter intermediate.
      typename std::vector<std::pair<std::vector<LocalOperNr>, addr_t> >::iterator mOperIterEnd;
   
      //! Restrictions on the types of saved intermediates
      IntermediateRestrictions mRestrictions;
      

   public:
      //! Default c-tor
      IntermediateMachine
         (
         )  = default;

      //! Virtual d-tor
      virtual ~IntermediateMachine
         (
         )  = default;
      
      //! Disable copy constructor
      IntermediateMachine
         (  const IntermediateMachine&
         )  = delete;
      
      //! Disable copy assignment
      IntermediateMachine& operator=
         (  const IntermediateMachine&
         )  = delete;

      //! Enable move constructor
      IntermediateMachine
         (  IntermediateMachine&&
         )  = default;
      
      //! Enable move assignment
      IntermediateMachine& operator=
         (  IntermediateMachine&&
         )  = default;
   
      /**
       * Register intermediate. The intermediate reference number should be set in the
       * intermediate and also returned in the case of succes.
       **/
      In RegisterIntermediate
         (  intermed_t& arIntermed
         );
   
      //! Put value of intermediate specified by aIntermed in arRes.
      bool GetIntermed
         (  const intermed_t& aIntermed
         ,  const evaldata_t& aEvalData
         ,  vec_t& arRes
         );
      
      //! put norm 2 of aIntermed into arNorm2
      bool GetIntermedNorm2
         (  const intermed_t& aIntermed
         ,  const evaldata_t& aEvalData
         ,  real_t& arNorm2
         );
   
      /**
       * Initialialize iteration over intermediate operators.
       * Returns false on failure.
       **/
      bool InitIntermedOperIter
         (  opitptr_t& aIntermed
         ,  const evaldata_t& aEvalData
         );
   
      /**
       * Get next operator set. Called repeatedly after InitIntermedOperIter().
       * Returns false when no more operator sets are available.
       **/
      bool GetIntermedNextOpers
         (  opitptr_t& aIntermed
         ,  const evaldata_t& aEvalData
         ,  std::vector<In>& aGenOpers
         ,  vec_t& arRes
         );
   
      //! Clear all stored intermediates.
      virtual void ClearIntermeds
         (  bool aKeepGlobal=false
         );
   
      //! Reset intermediate machine. All information on registered intermediates is deleted. 
      virtual void Reset
         (
         );
      
      //! Utility function for listing intermediates that are saved
      void ListRegisteredIntermeds
         (  std::ostream& aOut
         )  const;
      
      //! Utility function for listing in a latex format intermediates that are saved 
      void ListRegisteredIntermedsLatex
         (  std::ostream& aOut
         )  const; 
      
      //! Set intermediate restrictions
      void SetRestrictions
         (  const IntermediateRestrictions& aRestrict
         )
      {
         this->mRestrictions = aRestrict;
      }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDIATEMACHINE_DECL_H_INCLUDED */
