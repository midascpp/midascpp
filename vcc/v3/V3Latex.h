#ifndef V3LATEX_H_INCLUDED
#define V3LATEX_H_INCLUDED

//! Forward decl
class VccCalcDef;
class OpDef;

namespace midas::vcc::v3
{

void LatexTableGenVcc
   (  const VccCalcDef* const apVccCalcDef
   ,  const OpDef* const apOpDef
   );

} /* namespace midas::vcc::v3 */

#endif /* V3LATEX_H_INCLUDED */
