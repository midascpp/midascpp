#ifndef INTERMEDU_DECL_H_INCLUDED
#define INTERMEDU_DECL_H_INCLUDED

#include "Intermediate.h"

namespace midas::vcc::v3
{

/**
 * Intermediate representing up contractions
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedU
   :  public Intermediate<T, DATA>
{
   using evaldata_t = DATA<T>;
   using real_t = midas::type_traits::RealTypeT<T>;
   using concrete_iter = std::vector<std::pair<In, In> >::const_iterator;

   private:
      std::vector<In>            mUps;      ///< Up-contracted modes.
      std::vector<std::pair<In, In> > mCidx;     ///< Concrete modes and operators indices.
      ModeCombi                  mExciMc;   ///< MC for concrete excited modes.
      
      concrete_iter begin_concrete() const { return mCidx.begin(); }
      concrete_iter end_concrete()   const { return mCidx.end();   }
   
   public:
      IntermedU(): Intermediate<T, DATA>() {} 
      IntermedU(const std::string& aSpec);
      IntermedU(const std::vector<In>& aModes);
      IntermedU(const IntermedU& aOrig);
      std::unique_ptr<Intermediate<T, DATA> > Clone() const override {return std::make_unique<IntermedU<T, DATA> >(*this);}
      
      bool AssignConcreteModesOpers(const std::vector<In>& aCmodes, const std::vector<In>& aCopers) override;
      void GetConcreteModes(std::vector<In>& aCmodes) const override;
      void GetConcreteOpers(std::vector<In>& aCopers) const override;
      void GetExciMc(ModeCombi& aMc) const override {aMc = mExciMc;}
      bool Evaluate(const evaldata_t& aEvalData, GeneralMidasVector<T>& aRes) const override;
      bool Evaluate(const evaldata_t& aEvalData, NiceTensor<T>& aRes) const override;
      std::ostream& Print(std::ostream& aOut) const override;
      void Latex(std::ostream& aOut) const override;
      void WriteSpec(std::ostream& aOut) const override;
      bool CmpKind(const Intermediate<T, DATA>* const aIntermed) const override;
      In HighestModeNo() const override;
      
      In ExciLevel() const override {return mUps.size();}
      
      void GetModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mUps.begin(), mUps.end(), std::back_inserter(aModes));
      }
      
      void GetExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mUps.begin(), mUps.end(), std::back_inserter(aModes));
      }
      
      void GetPureExciModes
         (  std::vector<In>& aModes
         )  const override
      {
      }

      void GetOperExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mUps.begin(), mUps.end(), std::back_inserter(aModes));
      }
      
      void GetOccModes
         (  std::vector<In>& aModes
         )  const override
      {
      }
      
      void GetOperOccModes
         (  std::vector<In>& aModes
         )  const override
      {
      }
      
      bool OnlyAmps
         (
         )  const override
      {
         return true;
      }
      
      Scaling GetScaling(bool=false) const override;
      Scaling GetCost(bool=false) const override;
      Scaling GetNumber() const override;
      
      bool Norm2(const evaldata_t&, real_t&) const override;
      
      std::string Type() const override { return "IntermedU"; }
};


} /* namespace midas::vcc::v3 */

#endif /* INTERMEDU_DECL_H_INCLUDED */
