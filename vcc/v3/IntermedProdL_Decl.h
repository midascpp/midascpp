/**
************************************************************************
* 
* @file                IntermedProdL.h
*
* Created:             19-10-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Representing a product of intermediates
*                      for left-hand transformations.
* 
* Last modified: Mon Mar 22, 2010  04:00PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDPRODL_DECL_H_INCLUDED
#define INTERMEDPRODL_DECL_H_INCLUDED

#include <vector>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/ModeSum.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermedI.h"
#include "vcc/v3/IntermediateOperIter.h"
#include "vcc/v3/IntermedExci.h"
#include "vcc/v3/IntermedL.h"

namespace midas::vcc::v3
{

/**
 * Product term for VCC left-hand transformation
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedProdL
   :  public V3Contrib<T, DATA>
{
   private:
      using evaldata_t = DATA<T>;
      using Base = V3Contrib<T, DATA>;
      using real_t = midas::type_traits::RealTypeT<T>;

      unsigned long screened = 0;
      unsigned long not_screened = 0;
      unsigned long total = 0;
      
      void incr_screened()     { ++screened; }
      void incr_not_screened() { ++not_screened; }
      void incr_total()        { ++total; }
   
      In mExciLevel;
   
   protected:
      // These members define the contraction product.
      real_t                                          mCoef;               ///< Coefficient.
      std::vector<ModeSum>                            mSums;               ///< Sums over modes and operators.
      std::unique_ptr<IntermedL<T, DATA> >            mIntermedL;          ///< Left-hand coefficients.
      std::unique_ptr<IntermedExci<T, DATA> >         mIntermedTau;        ///< Intermediate corresponding to tau operator.
      std::unique_ptr<IntermediateOperIter<T, DATA> > mOpIterIntermed;     ///< Intermediate for iterating over operators.
      std::vector<IntermedPtr<T, DATA> >              mIntermeds;          ///< Remaining intermediates to be evaluated.
      std::vector<std::vector<In> >                   mModeGuides;         ///< Distribution of concrete modes from tau oper.
   
      // These members are used in the evaluation of the contraction product.
      std::vector<In>       mCmodes;             ///< Concrete modes associated with mode indices..
      bool                  mAssumeOpItStored;   ///< Assume that mOpIterIntermed is stored by
                                                 ///< intermediate machine. Affects scaling results. 
   
      void InitModeGuides
         (  const std::string& aGuides
         );

      void UpdateInternalState();
    
      void GetAllModes(std::vector<In>& aModes) const;
      void GetAllOccModes(std::vector<In>& aModes) const;
      void GetAllExciModes(std::vector<In>& aModes) const;
      
      //! Evaluate product for specific tau operator
      //!@{
      void EvaluateTauMc
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aTauMc
         ,  GeneralMidasVector<T>& aRes
         );
   
      void EvaluateTauMc
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aTauMc
         ,  NiceTensor<T>& aRes
         );
   
      void EvaluateTauMc
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aTauMc
         ,  TensorSumAccumulator<T>& aRes
         );
      //!@}
   
      //! Run recursive summation with restrictions
      //!@{
      void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aTauMc
         ,  GeneralMidasVector<T>& aRes
         );
   
      void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aTauMc
         ,  NiceTensor<T>& aRes
         );
   
      void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aTauMc
         ,  TensorSumAccumulator<T>& aRes
         );
      //!@}
      
   
      //! Loop over operators that contribute to the relevant MC
      //!@{
      void LoopOperators
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  GeneralMidasVector<T>&
         );
   
      void LoopOperators
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  NiceTensor<T>&
         );
   
      void LoopOperators
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  TensorSumAccumulator<T>&
         );
      //!@}
   
      //! Evaluate intermediates in the product
      //!@{
      void EvalIntermeds
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  GeneralMidasVector<T>&
         ,  const GeneralMidasVector<T>&
         );
   
      void EvalIntermeds
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  NiceTensor<T>&
         ,  const NiceTensor<T>&
         );
   
      void EvalIntermeds
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  TensorSumAccumulator<T>&
         ,  const NiceTensor<T>&
         );
      //!@}
   
      void IdentifyCmbIntermeds
         (  std::unique_ptr<IntermedProdL>&& aImProd
         ,  std::vector<Scaling>& aScalings
         ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
         ,  bool aCpTensors
         )  const;
      
      bool AddCmbIntermed
         (  std::unique_ptr<IntermedProdL>& aImProd
         ,  std::vector<Scaling>& aScalings
         ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
         ,  In aSumIdx
         ,  In aImIdx
         ,  const std::vector<In>& aExciModes
         ,  bool aCpTensors
         )  const;
      
      bool AddCmbIntermedL
         (  std::unique_ptr<IntermedProdL>& aImProd
         ,  std::vector<Scaling>& aScalings
         ,  std::vector<std::pair<std::unique_ptr<IntermedProdL>, std::vector<Scaling> > >& aFinalList
         ,  In aSumIdx
         ,  In aImIdx
         ,  const std::vector<In>& aOccModes
         ,  bool aCpTensors
         )  const;
      
   public: 
      IntermedProdL(const IntermedProdL&);
      IntermedProdL() = delete;
      IntermedProdL(const std::vector<std::string>& aSpec);
      IntermedProdL& operator=(const IntermedProdL&);
      ~IntermedProdL() = default;
      virtual std::unique_ptr<Base> Clone() const override {return std::make_unique<IntermedProdL>(*this);}
     
      //! Evaluate product. Results are added to the output data container.
      //!@{
      void Evaluate
         (  const evaldata_t&
         ,  GeneralDataCont<T>&
         )  override;
   
      void Evaluate
         (  const evaldata_t&
         ,  GeneralTensorDataCont<T>&
         )  override;
   
      void Evaluate
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  TensorSumAccumulator<T>&
         )  override;
   
      void Evaluate
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  NiceTensor<T>&
         )  override;
      //!@}
   
      
      template
         <  template<typename> typename VECTOR
         >
      void RegisterIntermeds
         (  IntermediateMachine<T, VECTOR, DATA>& aInMachine
         );
   
      //! Identify intermediates and modify this product to use them.
      virtual void IdentifyCmbIntermeds
         (  Scaling& aMaxScaling
         ,  bool=false
         );
      
      virtual Scaling GetScaling
         (  bool=false
         )  const;
      
      void Latex(std::ostream& aOut) const override; 
      std::ostream& Print(std::ostream& aOut) const override;
   
      void WriteSpec(std::ostream& aOut) const override;
   
      In Screened()    const override { return screened; }
      In NotScreened() const override { return not_screened; }
      In Total()       const override { return total; }
   
      //!
      std::string Type() const override { return "IntermedProdL"; }
   
      //!
      In ExciLevel() const override { return mExciLevel; }
};

} /* namespace midas::vcc:v3 */

#endif /* INTERMEDPRODL_DECL_H_INCLUDED */
