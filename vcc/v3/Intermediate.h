/**
************************************************************************
* 
* @file                Intermediate.h
*
* Created:             01-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Interfaces for intermediates.
* 
* Last modified: Mon Mar 22, 2010  04:31PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATE_H_INCLUDED
#define INTERMEDIATE_H_INCLUDED

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

#include "libmda/util/type_info.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Error.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "vcc_cpg/CtrT.h"
#include "vcc/v3/Scaling.h"
#include "tensor/NiceTensor.h"

namespace midas::vcc::v3
{
// Forward decl
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
class IntermediateMachine;

template
   <  typename T
   >
class VccEvalData;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCoef;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedI;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedU;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedT;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedExci;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedL;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCmb;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCmbL;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class Intermediate;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
using IntermedPtr = std::unique_ptr<Intermediate<T, DATA> >;

} /* namespace midas::vcc::v3 */

namespace midas::vcc::v3
{
namespace detail
{

inline void ReadVector
   (  std::string::const_iterator& aPos
   ,  std::vector<In>& aVec
   )
{
   aVec.clear();
   if (*aPos != '(')
      MIDASERROR("ReadVector() format error. No initial '('.");
 
   if (*(aPos+I_1) == ')')
   {
      aPos += I_2;
      return;
   }
   
   std::string i_str; 
   do
   {
      aPos++;
      if (isdigit(*aPos))
         i_str += *aPos;
      else if (*aPos==',' || *aPos==')')
      {
         std::istringstream is(i_str);
         In i_int;
         is >> i_int;
         aVec.push_back(i_int);
         i_str.clear();
      }
      else
         MIDASERROR("ReadVector(): Strange character encountered.");
   } while (*aPos != ')');
   aPos++;
}

} /* namespace detail */


/**
 * Base class for intermediates
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class Intermediate
{
   using evaldata_t = DATA<T>;
   using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      In mRefNo = -I_1;          ///< Reference no. used by IntermediateMachine.

   public:
      Intermediate
         (
         )  = default;

      Intermediate
         (  const Intermediate& aOrig
         )  = default;

      virtual ~Intermediate
         (
         )  = default;
    
      void SetRefNo
         (  In aRefNo
         )
      {
         mRefNo = aRefNo;
      }

      In RefNo
         (
         )  const
      {
         return mRefNo;
      }

      virtual IntermedPtr<T, DATA> Clone() const = 0;
      
      /**
       * Given vectors aCmodes and aCopers, concrete mode numbers are obtained
       * as "mode(concrete) = aCmodes[mode(general)]" and similar for operators.
       * Thus, the dimension of aCmodes and aCopers must be at least the highest
       * general mode number in the intermediate.
       * Returns false if assignment doesn't make sense, e.g. if some modes
       * are present more than once.
       **/
      virtual bool AssignConcreteModesOpers
         (  const std::vector<In>& aCmodes
         ,  const std::vector<In>& aCopers
         )  = 0;
     
      /** 
       * Put concrete modes in aCmodes in a unique way, i.e. if modes correspond to the same
       * intermediate, they should be ordered in the same way. This can be used by the
       * Intermediate machine to recognize if intermediate has already been calculated.
       **/
      virtual void GetConcreteModes
         (  std::vector<In>& aCmodes
         )  const = 0;
      
      /**
       * Put operators in aCopers in a unique way, i.e. if operators correspond to the same
       * intermediate, they should be ordered in the same way.
       **/
      virtual void GetConcreteOpers
         (  std::vector<In>& aCopers
         )  const = 0;
      
      //! Returns MC with concrete excited modes.
      virtual void GetExciMc
         (  ModeCombi& aMc
         )  const = 0;
     
      //! Evaluate intermediate. arRes is overwritten with the new result. 
      virtual bool Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralMidasVector<T>& arRes
         )  const = 0;
      
      //! Evaluate intermediate. arRes is overwritten with the new result. 
      virtual bool Evaluate
         (  const evaldata_t& aEvalData
         ,  NiceTensor<T>& arRes
         )  const
      {
         MIDASERROR("Evaluate for NiceTensor NOT IMPLEMENTED for this intermediate...");
         return false;
      }
      
      //! Returns total excitation level of intermediate.
      virtual In ExciLevel
         (
         )  const = 0;

      //! Append general modes in intermediate to aModes.
      virtual void GetModes
         (  std::vector<In>& aModes
         )  const = 0;
      
      virtual In HighestModeNo
         (
         )  const = 0;

      //! Append excited general modes in intermediate to aModes.
      virtual void GetExciModes
         (  std::vector<In>& aModes
         )  const = 0;
    
      //! Append excited modes without associated operator index to aModes.
      virtual void GetPureExciModes
         (  std::vector<In>& aModes
         )  const = 0;
      
      //! Append excited modes with associated operator index to aModes.
      virtual void GetOperExciModes
         (  std::vector<In>& aModes
         )  const = 0;

      //! Append all occupied modes to aModes.
      virtual void GetOccModes
         (  std::vector<In>& aModes
         )  const = 0;
      
      //! Append all occupied modes with associated operator index to aModes.
      virtual void GetOperOccModes
         (  std::vector<In>& aModes
         )  const = 0;
      
      /**
       * Write an exact specification of the intermediate to aOut. It must be possible
       * to completely reconstruct the intermediate from this specification by calling the
       * Intermediate::Factory() function.
       **/
      virtual void WriteSpec
         (  std::ostream& aOut
         )  const = 0;
     
      //! Print LaTeX code for this intermediate.
      virtual void Latex
         (  std::ostream& aOut
         )  const
      {
         aOut << std::endl << "LaTeX output not implemented for this Intermediate derivative" << std::endl;
      }
      
      /**
       * Used to dispatch to the correct derived object when using the << operator
       * with a base class pointer or reference.
       **/
      virtual std::ostream& Print
         (  std::ostream& aOut
         )  const = 0;

      //! Check if two intermediates are of the same kind, i.e. only differ in the general mode numbers.
      virtual bool CmpKind
         (  const Intermediate* const aIntermed
         )  const = 0;
      
      //! Allow registration of internal intermediates used to evaluate this intermediate.
      virtual void RegisterInternalIntermeds
         (  IntermediateMachine<T, GeneralMidasVector, DATA>& aInMachine
         )
      {
      }

      //! Allow registration of internal intermediates used to evaluate this intermediate.
      virtual void RegisterInternalIntermeds
         (  IntermediateMachine<T, NiceTensor, DATA>& aInMachine
         )
      {
      }

      virtual bool OnlyAmps() const = 0;
      ///< Returns true if intermediate depends only on T amplitudes.
      
      virtual Scaling GetScaling(bool=false) const = 0;
      ///< Total scaling if all intermediates are to be evaluated.

      virtual Scaling GetCost(bool=false) const = 0;
      ///< Scaling of the cost of a single intermediate when all the "exposed" modes and
      ///< operators have been specified.
      
      virtual Scaling GetNumber() const = 0;
      ///< Scaling of the total number of intermediates.
      
      //! Factory for generating intermediates according to specification in aSpec.
      static std::unique_ptr<Intermediate> Factory(const std::string& aSpec);

      virtual bool Norm2(const evaldata_t&, real_t&) const = 0;
      ///< Get norm 2 of intermediate, for screening purposes
      
      virtual bool AmpsNorm2(const evaldata_t&, real_t&) const
      {
         MIDASERROR("AmpNorm2(): Should not be called for type " + libmda::util::type_of(*this) );
         return false; // should not get here...
      }
      ///< Get norm 2 of intermediate, for screening purposes

      virtual std::string Type() const = 0;
      ///< Get char* with Intermediate type
};

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& operator<<
   (  std::ostream& aOut
   ,  const midas::vcc::v3::Intermediate<T, DATA>& aIntermed
   )
{
   return aIntermed.Print(aOut);
}

} /* namespace midas::vcc::v3 */


//! Include headers for derived classes needed for Factory implementation. Intermediate must be declared before this, however.
#include "IntermedCoef.h"
#include "IntermedI.h"
#include "IntermedU.h"
#include "IntermedT.h"
#include "IntermedExci.h"
#include "IntermedCmb.h"
#include "IntermedCmbL.h"

namespace midas::vcc::v3
{

/**
 * Factory for generating intermediates according to specification in aSpec.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::unique_ptr<Intermediate<T, DATA> > Intermediate<T, DATA>::Factory
   (  const std::string& aSpec
   )
{
   // Read until we hit something not a letter or digit.
   std::string kind;
   std::string::const_iterator c = aSpec.begin();
   while (  std::isalnum(*c)
         )
   {
      kind += *c;
      c++;
   }
   std::transform(kind.begin(), kind.end(), kind.begin(), (int(*)(int))std::tolower);
   
   std::string spec(c, aSpec.end());
   if      (kind == "c")      return std::make_unique<IntermedCoef<T, DATA> >(spec);
   else if (kind == "i")      return std::make_unique<IntermedI<T, DATA> >(spec);
   else if (kind == "u")      return std::make_unique<IntermedU<T, DATA> >(spec);
   else if (kind == "t")      return std::make_unique<IntermedT<T, DATA> >(spec, OP::T);
   else if (kind == "r")      return std::make_unique<IntermedT<T, DATA> >(spec, OP::R);
   else if (kind == "p")      return std::make_unique<IntermedT<T, DATA> >(spec, OP::P);
   else if (kind == "q")      return std::make_unique<IntermedT<T, DATA> >(spec, OP::Q);
   else if (kind == "s")      return std::make_unique<IntermedT<T, DATA> >(spec, OP::S);
   else if (kind == "e")      return std::make_unique<IntermedExci<T, DATA> >(spec);
   else if (kind == "l")      return std::make_unique<IntermedL<T, DATA> >(spec);
   else if (kind == "cmb")    return std::make_unique<IntermedCmb<T, DATA> >(spec);
   else if (kind == "cmbl")   return std::make_unique<IntermedCmbL<T, DATA> >(spec);
   else
   {
      std::ostringstream os;
      os << "Intermediate::Factory(): Unknown kind of intermediate: '" << kind << "'" << std::endl;
      MIDASERROR(os.str());
   }
   return nullptr;
}

namespace detail
{

/**
 * Function for getting an intermediate pointer of type IM from a vector of pointers to the
 * Intermediate base class. First pointer to the specific type is deleted from the vector
 * and returned. Otherwise, nullptr is returned.
 **/
template
   <  typename IM
   ,  typename IMBASE
   > 
std::unique_ptr<IM> GetImFromVec
   (  std::vector<std::unique_ptr<IMBASE> >& aIntermeds
   )
{
   std::unique_ptr<IM> im = nullptr;
   auto it = aIntermeds.begin();
   for(; it != aIntermeds.end(); ++it)
   {
      if (  dynamic_cast<IM*>((*it).get())
         )
      {
         im = std::move(midas::util::DynamicCastUniquePtr<IM>(std::move(*it)));
         aIntermeds.erase(it);
         break;
      }
   }
   return im;
}

} /* namespace detail */

} /* namespace midas::vcc::v3 */


#endif /* INTERMEDIATE_H_INCLUDED */
