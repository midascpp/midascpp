/**
************************************************************************
* 
* @file                ModeSum.h
*
* Created:             20-08-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class describing a sum over modes and operators.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MODESUM_H
#define MODESUM_H

#include <iostream>
#include <vector>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

namespace midas::vcc::v3
{

class ModeSum
{
   public:
      std::vector<In> mModes;           ///< Sum over these modes without overcounting.
      std::vector<In> mRestrict;        ///< Restrict summation to not include these modes.
      std::vector<In> mCRestrict;       ///< Concrete modes to be restricted.
     
      ModeSum() {} 
      ModeSum(std::vector<In> aModes): mModes(aModes) {}

      void Init(std::string::const_iterator& aPos);
      void Clear() {mModes.clear(), mRestrict.clear(), mCRestrict.clear();}
      void WriteSpec(std::ostream& aOut) const;

      void Latex(std::ostream& aOut) const;
      friend std::ostream& operator<<(std::ostream& aOut, const ModeSum& aSum);
};

} /* namespace midas::vcc::v3 */

#endif // MODESUM_H
