/**
************************************************************************
* 
* @file                IntermedI_Impl.h
*
* Created:             07-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Occupied (i) contraction intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDI_IMPL_H_INCLUDED
#define INTERMEDI_IMPL_H_INCLUDED


#include "IntermedI_Decl.h"

// std headers
#include <iostream>
#include <algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "Intermediate.h"
#include "IntermedProd.h"
#include "vcc/TransformerV3.h"
#include "vcc/ModalIntegrals.h"
#include "tensor/Scalar.h"
#include "EvalDataTraits.h"

namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedI<T, DATA>::IntermedI
   (  const std::string& aSpec
   )
   :  Intermediate<T, DATA>()
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, mOcc);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedI<T, DATA>::IntermedI
   (  const std::vector<In>& aModes
   )
   :  Intermediate<T, DATA>()
   ,  mOcc(aModes)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedI<T, DATA>::IntermedI
   (  const IntermedI& aOrig
   )
   :  Intermediate<T, DATA>(aOrig)
   ,  mOcc(aOrig.mOcc)
   ,  mCidx(aOrig.mCidx) 
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedI<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   mCidx.clear();
   for (In m=I_0; m<mOcc.size(); ++m)
      mCidx.emplace_back(aCmodes[mOcc[m]], aCopers[mOcc[m]]);
   std::sort(mCidx.begin(), mCidx.end());
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedI<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes.clear();
   aCmodes.resize(mCidx.size());
   for (In i=I_0; i<mCidx.size(); ++i)
      aCmodes[i] = mCidx[i].first;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedI<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   aCopers.clear();
   aCopers.resize(mCidx.size());
   for (In i=I_0; i<mCidx.size(); ++i)
      aCopers[i] = mCidx[i].second;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedI<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   T occ(C_1);
   for (In i=I_0; i<mCidx.size(); ++i)
   {
      occ *= aEvalData.GetIntegrals()->GetOccElement(mCidx[i].first, mCidx[i].second);
   }

   aRes.SetNewSize(I_1);
   aRes[I_0] = occ;
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedI<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   T occ(C_1);
   for (In i = I_0; i < mCidx.size(); ++i)
   {
      occ *= aEvalData.GetIntegrals()->GetOccElement(mCidx[i].first, mCidx[i].second);
   }

   bool cp_scalar = false;

   if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                  )
   {
      cp_scalar = aEvalData.DecomposedTensors();
   }

   aRes = NiceTensor<T>(new Scalar<T>(occ, cp_scalar));
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedI<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   aOut << "i";
   aOut << "(";
   std::copy(mOcc.begin(), mOcc.end(), std::ostream_iterator<In>(aOut, ""));
   aOut << ")";
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedI<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "i_{";
   for (In i=I_0; i<mOcc.size(); ++i)
      aOut << "m_{" << mOcc[i] << "}";
   aOut << "}";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedI<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "i" << mOcc;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedI<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedI<T, DATA>* const other = dynamic_cast<const IntermedI<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   return mOcc.size() == other->mOcc.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedI<T, DATA>::HighestModeNo
   (
   )  const
{
   In high = -I_1;
   for (In i=I_0; i<mOcc.size(); ++i)
      if (mOcc[i] > high)
         high = mOcc[i];
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedI<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   return Scaling(mOcc.size(), mOcc.size(), I_0);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedI<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{
   return Scaling(I_0, I_0, I_0);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedI<T, DATA>::GetNumber
   (
   )  const
{
   return Scaling(mOcc.size(), mOcc.size(), I_0);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedI<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      using ContT = typename std::remove_reference_t<decltype(*aEvalData.GetIntegrals())>::ContT;
      aNorm2 = 1.0;
      for(concrete_iter iter=begin_concrete(); iter!=end_concrete(); ++iter) // same as evaluate
         aNorm2 *= aEvalData.GetIntegrals()->GetNorm2(iter->first, iter->second, ContT::PASSIVE);

      return true;
   }
   else
   {
      return false;
   }
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDI_IMPL_H_INCLUDED */
