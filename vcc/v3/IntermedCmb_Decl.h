#ifndef INTERMEDCMB_DECL_H_INCLUDED
#define INTERMEDCMB_DECL_H_INCLUDED

#include "IntermediateOperIter.h"

namespace midas::vcc::v3
{

/**
 * Combining an IntermediateOperIter object with an ordinary Intermediate object.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCmb 
   :  public IntermediateOperIter<T, DATA>
{
   using evaldata_t = DATA<T>;
   using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      std::unique_ptr<IntermediateOperIter<T, DATA> > mOpIterIntermed = nullptr;
      IntermedPtr<T, DATA>                            mIntermed = nullptr;

      std::vector<In> mSumModes;        ///< Modes to be summed.
     
      // The "exposed" modes, i.e. modes to be seen by other
      // objects making use of this intermediate.
      std::vector<In> mOcc;                        ///< Occupied modes with associated operator.
      std::vector<std::vector<In> > mOccNoOper;    ///< Occupied modes with no associated operator.
      std::vector<std::vector<In> > mExci;         ///< Excited modes with no associated operator.
      std::vector<std::vector<In> > mExciOper;     ///< Excited modes with associated operator.
     
      std::vector<In> mCmodes;          ///< Concrete modes. mCmodes[gen. mode] = concrete mode.
      ModeCombi       mExciMc;          ///< MC for excited modes.

      std::vector<std::pair<In,In> > mCGmap;
      ///< Mapping of concrete to general mode indices for modes with associated operators.
      ///< mCGmap[i].first  = concrete mode.
      ///< mCGmap[i].second = general mode.

      std::map<std::vector<LocalOperNr>, std::unique_ptr<GeneralMidasVector<T> > > mCopers;
      ///< Mapping a vector of operators to the MidasVector containg the corresponding values.
      
      typename std::map<std::vector<LocalOperNr>, std::unique_ptr<GeneralMidasVector<T> > >::iterator mCopersCur;
      ///< Iterator pointing to the current set of operators.
      
      /***********************************************************************************
       * new tensor stuff
       ***********************************************************************************/
      std::map<std::vector<LocalOperNr>, TensorSumAccumulator<T> > mCopersTensorTemp;
      std::map<std::vector<LocalOperNr>, NiceTensor<T> > mCopersTensor;
      ///< Mapping a vector of operators to the MidasVector containg the corresponding values.
      
      typename std::map<std::vector<LocalOperNr>, NiceTensor<T> >::iterator mCopersCurTensor;
      ///< Iterator pointing to the current set of operators.
      
      void SumModeTensor(const evaldata_t& aEvalData, In aIdx, In aCurCmode);
      ///< Do the summation over modes in mSumModes[aIdx].
      
      void LoopOpersTensor
         (  const evaldata_t& aEvalData
         );
      ///< Loop over the operators when all modes are set.
      
      /***********************************************************************************
       * new tensor stuff end
       ***********************************************************************************/

      bool mFirstOperSet = false;
      ///< Used to make AssignNextOpers() work with first set of operators.
      
      bool mAssumeOpItStored = false;
      ///< Assume mOpIterIntermed is stored when calculating the cost using GetCost().
      ///< This is useful when identifying intermediates to lower the scaling.

      void GetIntermedSpec(std::string::const_iterator& aPos, std::string& aSpec);
      ///< Read an intermediate specification enclosed in [...].

      void UpdateInternalState();
      ///< Update private variables to be consistent with the input specification.

      void SumMode(const evaldata_t& aEvalData, In aIdx, In aCurCmode);
      ///< Do the summation over modes in mSumModes[aIdx].
      
      void LoopOpers
         (  const evaldata_t& aEvalData
         );
      ///< Loop over the operators when all modes are set.
 
      bool CmpVecKind
         (  const std::vector<std::vector<In> >& a1
         ,  const std::vector<std::vector<In> >& a2
         )  const;
      ///< Compare the sizes of two vectors of vectors.

      void LatexVecVec(std::ostream& aOut, const std::vector<std::vector<In> >& aVec) const;
      ///< Print LaTeX code for vector of vector of integers.
      
   public:
      IntermedCmb();
      IntermedCmb(const std::string& aSpec);
      IntermedCmb(std::unique_ptr<IntermediateOperIter<T, DATA> >&& aOpIter, IntermedPtr<T, DATA>&& aIm, const std::vector<In>& aNoSum);
      IntermedCmb(const IntermedCmb&);
      IntermedCmb& operator=(const IntermedCmb&);
      std::unique_ptr<Intermediate<T, DATA> > Clone() const override {return std::make_unique<IntermedCmb<T, DATA> >(*this);}
      
      void PrintState(std::ostream& aOut) const;
      void WriteSpec(std::ostream& aOut) const override;
      void AssignConcreteModes(const std::vector<In>& aCmodes) override;
      void GetConcreteModes(std::vector<In>& aCmodes) const override;
      void GetConcreteOpers(std::vector<In>& aCopers) const override;
      std::ostream& Print(std::ostream& aOut) const override;
      void PrintScaling(std::ostream& aOut) const;
      void Latex(std::ostream& aOut) const override;
      void GetModes(std::vector<In>& aModes) const override;
      void GetExciModes(std::vector<In>& aModes) const override;
      In HighestModeNo() const override;
      bool Evaluate(const evaldata_t& aEvalData, GeneralMidasVector<T>& aRes) const override;
      void GetOccModes(std::vector<In>& aModes) const override;
      In ExciLevel() const override;

      void GetPureExciModes(std::vector<In>& aModes) const override
      {
         MIDASERROR("IntermedCmb::GetPureExciModes(): Not supported.");
      }
      
      void GetOperExciModes(std::vector<In>& aModes) const override;

      void GetPureExciVectors(std::vector<std::vector<In> >& aExciVectors) const override
      {
         std::copy(mExci.begin(), mExci.end(), std::back_inserter(aExciVectors));
      }
      
      void GetOperExciVectors(std::vector<std::vector<In> >& aOperExciVectors) const override
      {
         std::copy(mExciOper.begin(), mExciOper.end(), std::back_inserter(aOperExciVectors));
      }

      void GetNoOperOccVectors(std::vector<std::vector<In> >& aNoOperOccVectors) const override
      {
         std::copy(mOccNoOper.begin(), mOccNoOper.end(), std::back_inserter(aNoOperOccVectors));
      }

      void GetOperOccModes(std::vector<In>& aModes) const override
      {
         std::copy(mOcc.begin(), mOcc.end(), std::back_inserter(aModes));
      }

      void GetExciMc(ModeCombi& aMc) const override {aMc = mExciMc;}

      bool CmpKind(const Intermediate<T, DATA>* const aIntermed) const override;

      bool InitOperIter(const evaldata_t& aEvalData) override;
      bool AssignNextOpers(const evaldata_t& aEvalData, std::vector<LocalOperNr>& aCopers) override;
      void AssignOpers(const std::vector<LocalOperNr>& aCopers, std::vector<In>& aGenOpers) const override;
      /***********************************************************************************
       * new tensor stuff
       ***********************************************************************************/
      bool InitOperIterTensor(const evaldata_t& aEvalData) override;
      bool AssignNextOpersTensor(const evaldata_t& aEvalData, std::vector<LocalOperNr>& aCopers) override;
      bool Evaluate(const evaldata_t& aEvalData, NiceTensor<T>& aRes) const override;
      
      /***********************************************************************************
       * new tensor stuff end
       ***********************************************************************************/

      void RegisterInternalIntermeds(IntermediateMachine<T, GeneralMidasVector, DATA>& aInMachine) override;
      void RegisterInternalIntermeds(IntermediateMachine<T, NiceTensor, DATA>& aInMachine) override;

      bool OnlyAmps() const override
      {
         return (mOpIterIntermed->OnlyAmps() && mIntermed->OnlyAmps());
      }

      Scaling GetScaling(bool=false) const override;
      ///< Get total scaling.
      
      Scaling GetCost(bool=false) const override;
      ///< Cost of generating intermediate.
      
      Scaling GetNumber() const override;
      ///< Number of intermediates.

      void SetAssumeOpItStored(bool aB) {mAssumeOpItStored = aB;}
      
      bool Norm2(const evaldata_t& aEvalData, real_t& aNorm2) const override;
      
      std::string Type() const override { return "IntermedCmb"; }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDCMB_DECL_H_INCLUDED */
