/**
************************************************************************
* 
* @file                FTCommutator.h
*
* Created:             17-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   V3 contribution representing [F,T].
* 
* Last modified: Tue May 05, 2009  01:34PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FTCOMMUTATOR_H
#define FTCOMMUTATOR_H

#include <vector>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "V3Contrib.h"
#include "vcc_cpg/BasicOper.h"
#include "VccEvalData.h"

namespace midas::vcc::v3
{

class FTCommutator
   :  public V3Contrib<Nb, VccEvalData>
{
   private:
      using Base = V3Contrib<Nb, VccEvalData>;
      using param_t = Nb;
   
   protected:
      In    mExciLevel;   ///< Excitation level of T in [F,T].
      OP    mType;        ///< Excitation vector type (T, R, P, etc.)
   
      void CalcEnergyDiff(const evaldata_t& aEvalData, const ModeCombi& aMc, GeneralMidasVector<param_t>& aDiffVec, const In aCurModeIdx, const In aRep);
      
   public: 
      FTCommutator(In aExciLevel, OP aType=OP::T): mExciLevel(aExciLevel), mType(aType) {}
      FTCommutator(): V3Contrib<Nb, VccEvalData>() 
      {
      }
   
      FTCommutator(const std::vector<string>& aSpec);
      ~FTCommutator() = default;
      
      void Evaluate(const evaldata_t& aEvalData, GeneralDataCont<param_t>& aDcOut) override;
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, GeneralMidasVector<param_t>& aRes) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
   
      void Evaluate(const evaldata_t& aEvalData, GeneralTensorDataCont<param_t>& aDcOut) override;
      ///< Evaluate products. Results are added to the aDcOut data container.
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, NiceTensor<param_t>& aRes) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, TensorSumAccumulator<param_t>& aRes) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
   
      void WriteSpec(std::ostream& aOut) const override;
   
      std::unique_ptr<Base> Clone() const override {return std::make_unique<FTCommutator>(*this);}
      
      std::ostream& Print(std::ostream& aOut) const override; 
      
      In Screened()    const override { return I_0; }
      In NotScreened() const override { return I_0; }
      In Total()       const override { return I_0; }
   
      In ExciLevel() const override { return this->mExciLevel; }
   
      std::string Type() const override { return "FTCommutator"; }
};


} /* namespace midas::vcc::v3 */

#endif // FTCOMMUTATOR
