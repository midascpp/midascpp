/**
************************************************************************
* 
* @file                Scaling.cc
*
* Created:             24-03-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class Scaling.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "Scaling.h"

namespace midas::vcc::v3
{

void Scaling::Reset()
{
   mM = -I_1;
   mO = -I_1;
   mN = -I_1;
   mR = -I_1;
}

const Scaling operator*(const Scaling& aL, const Scaling& aR)
{
   Scaling sc;
   if (  aL.mM != -I_1
      && aR.mM != -I_1
      )
   {
      sc.mM = aL.mM+aR.mM;
   }
   if (  aL.mO != -I_1
      && aR.mO != -I_1
      )
   {
      sc.mO = aL.mO+aR.mO;
   }
   if (  aL.mN != -I_1
      && aR.mN != -I_1
      )
   {
      sc.mN = aL.mN+aR.mN;
   }
   if (  aL.mR != -I_1
      && aR.mR != -I_1
      )
   {
      sc.mR = aL.mR+aR.mR;
   }

   return sc;
}

int operator<(const Scaling& aL, const Scaling& aR)
{
   // M scaling is most important
   if (  aL.mM != aR.mM
      )
   {
      return aL.mM < aR.mM;
   }
   else if  (  aL.mO != aR.mO
            )
   {
      return aL.mO < aR.mO;
   }
   else if  (  aL.mN != aR.mN
            )
   {
      return aL.mN < aR.mN;
   }
   else
   {
      return aL.mR < aR.mR;
   }
}

int operator>(const Scaling& aL, const Scaling& aR)
{
   if (aL.mM != aR.mM)
      return aL.mM > aR.mM;
   else if (aL.mO != aR.mO)
      return aL.mO > aR.mO;
   else if (aL.mR != aR.mR)
      return aL.mR > aR.mR;
   else 
      return aL.mN > aR.mN;
}

std::ostream& operator<<(std::ostream& aOut, const Scaling& aSc)
{
   if (aSc.mM != -I_1)
      aOut << "M^" << aSc.mM;
   if (aSc.mO != -I_1)
      aOut << " O^" << aSc.mO;
   if (aSc.mN != -I_1)
      aOut << " N^" << aSc.mN;
   if (aSc.mR != -I_1)
      aOut << " R^" << aSc.mR;
   return aOut;
}

void Scaling::Latex(std::ostream& aOut) const
{
   if (mM > I_0)
   {
      aOut << "M";
      if (mM > I_1)
         aOut << "^{" << mM << "}";
   }
   if (mO > I_0)
   {
      aOut << " O";
      if (mO > I_1)
         aOut << "^{" << mO << "}";
   }
   if (mN > I_0)
   {
      aOut << " N";
      if (mN > I_1)
         aOut << "^{" << mN << "}";
   }
   if (mR > I_0)
   {
      aOut << " R";
      if (mR > I_1)
         aOut << "^{" << mR << "}";
   }
}

} /* namespace midas::vcc::v3 */
