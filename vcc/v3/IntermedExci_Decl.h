/**
************************************************************************
* 
* @file                IntermedExci_Decl.h
*
* Created:             10-5-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   IntermedExci is a dummy intermediate describing
*                      and excitation operator used in left-hand
*                      transformations.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDEXCI_DECL_H_INCLUDED
#define INTERMEDEXCI_DECL_H_INCLUDED

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "Intermediate.h"
#include "Scaling.h"

template
   <  class T
   >
class NiceTensor;

namespace midas::vcc
{

template
   <  class T
   >
class TensorSumAccumulator;

namespace v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedExci
   :  public Intermediate<T, DATA>
{
   public:
      using evaldata_t = DATA<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      std::vector<In> mExci;
      std::vector<In> mForw;
      std::vector<In> mDown;
      std::vector<std::pair<In,In> > mCexci;
      std::vector<std::pair<In,In> > mCforw;
      std::vector<std::pair<In,In> > mCdown;
     
      ModeCombi mExciMc;
      ModeCombi mTotMc;
      
   public:
      IntermedExci
         (  const std::string& aSpec
         );
      
      std::unique_ptr<Intermediate<T, DATA> > Clone
         (
         )  const override
      {
         return std::make_unique<IntermedExci<T, DATA> >(*this);
      }
      
      In ExciLevel
         (
         )  const override
      {
         return mExci.size() + mForw.size();
      }
      
      void GetExciMc
         (  ModeCombi& aMc
         )  const override
      {
         aMc = this->mExciMc;
      }
         
      bool AssignConcreteModesOpers
         (  const std::vector<In>& aCmodes
         ,  const std::vector<In>& aCopers
         )  override;

      void GetModes
         (  std::vector<In>& aModes
         )  const override;

      void GetExciModes
         (  std::vector<In>& aModes
         )  const override;

      void WriteSpec
         (  std::ostream& aOut
         )  const override;

      void Latex
         (  std::ostream& aOut
         )  const override;

      std::ostream& Print
         (  std::ostream& aOut
         )  const override;

      Scaling GetScaling
         (  bool=false
         )  const override;

      Scaling GetCost
         (  bool=false
         )  const override;

      Scaling GetNumber
         (
         )  const override;
      
      void GetOccModes
         (  std::vector<In>& aModes
         )  const override
      {
         std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
      }
      
      bool Norm2
         (  const evaldata_t&
         ,  real_t&
         )  const override;
      
      std::string Type
         (
         )  const override
      {
         return "IntermedExci";
      }

      /**
       * The following functions are specific to the IntermedExci class, i.e. they are
       * not part of the Intermediate interface.
       **/

      void GetTotMc
         (  ModeCombi& aMc
         )  const
      {
         aMc = this->mTotMc;
      }
      
      void GetDownModes
         (  std::vector<In>& aModes
         )  const
      {
         std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));
      }
    
      In Ndown
         (
         )  const
      {
         return mDown.size();
      }

      In Nforw
         (
         )  const
      {
         return mForw.size();
      }
      
      //! Act to the left with excitation operator
      //!@{
      bool ActLeft
         (  const evaldata_t&
         ,  const GeneralMidasVector<T>&
         ,  GeneralMidasVector<T>&
         );

      bool ActLeft
         (  const evaldata_t&
         ,  const NiceTensor<T>&
         ,  NiceTensor<T>&
         );

      bool ActLeft
         (  const evaldata_t&
         ,  const NiceTensor<T>&
         ,  TensorSumAccumulator<T>&
         );
      //!@}
      
      /**
       * The following functions are not necessary for this dummy intermediate.
       **/
      
      void GetConcreteModes
         (  std::vector<In>& aCmodes
         )  const override
      {
         MIDASERROR("IntermedExci::GetConcreteModes(): Not implemented.");
      }
      
      void GetConcreteOpers
         (  std::vector<In>& aCopers
         )  const override
      {
         MIDASERROR("IntermedExci::GetConcreteOpers(): Not implemented.");
      }
      
      bool Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralMidasVector<T>& aRes
         )  const override
      {
         MIDASERROR("IntermedExci::Evaluate(): Not implemented.");
         return false;
      }

      In HighestModeNo
         (
         )  const override
      {
         MIDASERROR("IntermedExci::HighestModeNo(): Not implemented.");
         return I_0;
      }

      void GetPureExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         MIDASERROR("IntermedExci::GetPureExciModes(): Not implemented.");
      }
      
      void GetOperExciModes
         (  std::vector<In>& aModes
         )  const override
      {
         MIDASERROR("IntermedExci::GetOperExciModes(): Not implemented.");
      }
      
      void GetOperOccModes
         (  std::vector<In>& aModes
         )  const override
      {
         MIDASERROR("IntermedExci::GetOperOccModes(): Not implemented.");
      }
      
      bool CmpKind
         (  const Intermediate<T, DATA>* const aIntermed
         )  const override
      {
         MIDASERROR("IntermedExci::CmpKind(): Not implemented.");
         return false;
      }
      
      bool OnlyAmps
         (
         )  const override
      {
         MIDASERROR("IntermedExci::OnlyAmps(): Not implemented.");
         return false;
      }
};

} /* namespace v3 */
} /* namespace midas::vcc */

#endif /* INTERMEDEXCI_DECL_H_INCLUDED */
