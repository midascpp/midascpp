/**
************************************************************************
* 
* @file                IntermediateMachine.h
*
* Created:             14-03-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Include header for IntermediateMachine
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATEMACHINE_H_INCLUDED
#define INTERMEDIATEMACHINE_H_INCLUDED

#include "IntermediateMachine_Decl.h"
#include "IntermediateMachine_Impl.h"

#endif /* INTERMEDIATEMACHINE_H_INCLUDED */
