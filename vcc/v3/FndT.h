/**
************************************************************************
* 
* @file                FndT.h
*
* Created:             17-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   V3 contribution representing T down contracted
*                      with f_n
* 
* Last modified: Thu Aug 06, 2009  01:37PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FNDT_H_INCLUDED
#define FNDT_H_INCLUDED

// std headers
#include <vector>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "V3Contrib.h"
#include "vcc_cpg/BasicOper.h"
#include "VccEvalData.h"

namespace midas::vcc::v3
{

class FndT
   :  public V3Contrib<Nb, VccEvalData>
{
   private:
      using Base = V3Contrib<Nb, VccEvalData>;
      using param_t = Nb;

      void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  GeneralMidasVector<param_t>& arDcOut
         );
      void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  NiceTensor<param_t>& arDcOut
         );
      void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  TensorSumAccumulator<param_t>& arDcOut
         );
   
   protected:
      In   mFockLevel;   ///< The mode coupling level of the Hamiltonian in the Fock operator.
      In   mTLevel;      ///< Excitation level of T in [F,T].
      OP   mType;        ///< Type of excitation vector to use (T, R, etc.)
      
      void EvaluateLeft
         (  const evaldata_t&
         ,  GeneralDataCont<param_t>&
         );
   
      void EvaluateLeft
         (  const evaldata_t&
         ,  GeneralTensorDataCont<param_t>&
         );
   
      void EvaluateLeft
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  NiceTensor<param_t>&
         );
   
      void EvaluateLeft
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  TensorSumAccumulator<param_t>&
         );
   
   public: 
      FndT(): mFockLevel(I_0), mTLevel(I_0), mType(OP::T) 
      {
      }
   
      FndT(const std::vector<string>& aSpec);
      
      ~FndT() = default;
      std::unique_ptr<Base> Clone() const override { return std::make_unique<FndT>(*this); }
      
      void Evaluate(const evaldata_t& aEvalData, GeneralDataCont<param_t>& aDcOut) override;
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, GeneralMidasVector<param_t>& aDcOut) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
   
      void Evaluate(const evaldata_t& aEvalData, GeneralTensorDataCont<param_t>& aDcOut) override;
      ///< Evaluate products. Results are added to the aDcOut data container.
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, NiceTensor<param_t>& aRes) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
      void Evaluate(const evaldata_t& aEvalData, const ModeCombi& aMc, TensorSumAccumulator<param_t>& aRes) override;
      ///< Evaluate this contribution. Result is added to the aDcOut data container.
   
   
      void WriteSpec(std::ostream& aOut) const override;
      
      std::ostream& Print(std::ostream& aOut) const override; 
      
      In Screened()    const override { return I_0; }
      In NotScreened() const override { return I_0; }
      In Total()       const override { return I_0; }
   
      //! The terms contribute to mTLevel-1
      In ExciLevel() const override
      {
         return this->mType == OP::E ? this->mTLevel : this->mTLevel - I_1;
      }
   
      //!
      std::string Type() const override { return "FndT"; }
};

} /* namespace midas::vcc::v3 */

#endif // FNDT_H_INCLUDED
