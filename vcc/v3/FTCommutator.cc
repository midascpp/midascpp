/**
************************************************************************
* 
* @file                FTCommutator.cc
*
* Created:             17-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing FTCommutator class.
* 
* Last modified: Thu Aug 06, 2009  01:03PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "FTCommutator.h"
#include "util/MultiIndex.h"
#include "mmv/MMVT.h"
#include "vcc/Vcc.h"
#include "vcc/TensorSumAccumulator.h"

#include "tensor/EnergyDifferencesTensor.h"
#include "util/CallStatisticsHandler.h"

namespace midas::vcc::v3
{

/**
 * aSpec should contains one std::string;
 * aSpec[0]: 'Tl', where T is an exci operator and l is the excitation level.
 **/
FTCommutator::FTCommutator(const std::vector<string>& aSpec): V3Contrib()
{
   const std::string& spec = aSpec[I_0];
   mType = BasicOper::TypeForChar(spec[I_0]);
   if (mType!=OP::T && mType!=OP::R && mType!=OP::P && mType!=OP::Q && mType!=OP::S && mType!=OP::E)
   {
      Mout << " FTCommutator::FTCommutator(const std::vector<string>& aSpec):" << std::endl
           << "    spec = " << spec << std::endl
           << "    Type = " << mType << " is not an excitation operator." << std::endl;
      MIDASERROR("");
   }
   
   istringstream is(spec.substr(I_1, spec.size()-I_1));
   is >> mExciLevel;
}

/**
 * Calculate vector of sums of modal energy differences for excitations within given MC,
 * i.e. sum_{m in mc} e(modal)_a(m) - e(modal)_i(m).
 * The variables aCurModeIdx and aRep are used for recursion. Initially, aCurModeIdx should
 * be the highest possible index in aMc and aRep should be 1.
 **/
void FTCommutator::CalcEnergyDiff
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  GeneralMidasVector<param_t>& aDiffVec
   ,  const In aCurModeIdx
   ,  const In aRep
   )
{
   if (aCurModeIdx == -I_1)
      return;
   
   In n_modals = aEvalData.NModals(aMc.Mode(aCurModeIdx));
   const auto& offset = aEvalData.GetOccModalOffset();
   const auto& eigval = aEvalData.GetModalEnergies();

   In modal = I_1;
   In i=I_0;
   param_t val;
   while (i < aDiffVec.Size())
   {
      if (modal >= n_modals)
         modal = I_1;
      for (In j=I_0; j<aRep; ++j)
      {
         eigval.DataIo(IO_GET, offset[aMc.Mode(aCurModeIdx)] + modal, val);
         aDiffVec[i] += val; //aEvalData.mpVcc->GetEigVal(aMc.Mode(aCurModeIdx), modal);
         i++;
      }
      modal++;
   }

   CalcEnergyDiff(aEvalData, aMc, aDiffVec, aCurModeIdx-I_1, aRep*(n_modals-I_1));
}

/**
 * Evaluate [F,T]
 * Result is added to aDcOut.
 * aDcOut must follow addresses given by aEvalData.GetModeCombiOpRange()
 **/
void FTCommutator::Evaluate(const evaldata_t& aEvalData, GeneralDataCont<param_t>& aDcOut)
{
   LOGCALL("calls");

   GeneralMidasVector<param_t> amps;
   GeneralMidasVector<param_t> diff_vec;
   const ModeCombiOpRange mcr_p = aEvalData.GetModeCombiOpRange();
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp)
   {
      LOGCALL("mc loop");
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if (mp.Size() != mExciLevel)
         continue;

      
      In n_amps = aEvalData.NExciForModeCombi(i_mp);
      amps.SetNewSize(n_amps);
      if (mType == OP::E)
      {
         aEvalData.template GetExciVec<GeneralDataCont<param_t>>(OP::L)->DataIo(IO_GET, amps, n_amps, mp.Address());
      }
      else
      {
         aEvalData.template GetExciVec<GeneralDataCont<param_t>>(mType)->DataIo(IO_GET, amps, n_amps, mp.Address());
      }
      
      // 
      diff_vec.SetNewSize(n_amps);
      diff_vec.Zero();
      CalcEnergyDiff(aEvalData, mp, diff_vec, mp.Size()-I_1, I_1);

      for (In i=I_0; i<n_amps; ++i)
         amps[i] *= diff_vec[i];
      
      // Add result to arDcOut.
      aDcOut.DataIo(IO_PUT, amps, amps.Size(), mp.Address(), I_1, I_0, I_1, false, C_0, true);
   }
}

/**
 * Evaluate [F,T] for specific mode combination, and
 * put result in an output MidasVector.
 * This function is used when the outer transformer evaluation loop is over MCs.
 *
 * @param aEvalData     The evaluation data.
 * @param aMc           The MC to evaluate for.
 * @param arDcOut       On output holds result of FTCommutator evaluation.
 **/
void FTCommutator::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  GeneralMidasVector<param_t>& arDcOut
   )
{
   LOGCALL("calls");

   GeneralMidasVector<param_t> amps;
   GeneralMidasVector<param_t> diff_vec;
   LOGCALL("mc loop");
   if (aMc.Size() != mExciLevel)
      return;

   
   In i_mp = I_0;
   ModeCombiOpRange::const_iterator it_mc;
   if (  aEvalData.GetModeCombiOpRange().Find(aMc, it_mc, i_mp)   // This sets i_mc to the RefNr of aMc
      )
   {
      In n_amps = aEvalData.NExciForModeCombi(i_mp);
      amps.SetNewSize(n_amps);
      if (mType == OP::E)
      {
         aEvalData.template GetExciVec<GeneralDataCont<param_t>>(OP::L)->DataIo(IO_GET, amps, n_amps, aMc.Address());
      }
      else
      {
         aEvalData.template GetExciVec<GeneralDataCont<param_t>>(mType)->DataIo(IO_GET, amps, n_amps, aMc.Address());
      }

      // Calculate energy differences
      diff_vec.SetNewSize(n_amps);
      diff_vec.Zero();
      CalcEnergyDiff(aEvalData, aMc, diff_vec, aMc.Size()-I_1, I_1);
      
      // Multiply onto amplitudes
      for (In i=I_0; i<n_amps; ++i)
      {
         amps[i] *= diff_vec[i];
      }
      
      // Add result to arDcOut.
      arDcOut += amps;
   }

}

/**
 * Evaluate [F,T] for TensorDataCont
 **/
void FTCommutator::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralTensorDataCont<param_t>& arDcOut
   )
{
   // Get ModeCombiOpRange
   const auto& mc_op_range = aEvalData.GetModeCombiOpRange();
   const auto& nmodals = aEvalData.GetNModals();
   const auto& eigvals = aEvalData.GetModalEnergies();
   const auto& offsets = aEvalData.GetOccModalOffset();

   // Get amplitudes
   const auto* amps = aEvalData.template GetExciVec<GeneralTensorDataCont<param_t> >(  this->mType == OP::E
                                             ?  OP::L 
                                             :  this->mType
                                             );

   // Loop over mode combis
   auto size = mc_op_range.Size();
   for(size_t i_mc = 0; i_mc < size; ++i_mc)
   {
      const auto& mc = mc_op_range.GetModeCombi(i_mc);
      const auto& mcvec = mc.MCVec();

      // The commutator only contributes to the tensors of order 'mExciLevel'
      if (  mc.Size() != this->mExciLevel
         )
      {
         continue;
      }

      // Set result to amplitudes
      auto res_tens = amps->GetModeCombiData(i_mc);

      // Multiply EnergyDifferencesTensor
      switch(res_tens.Type())
      {
         case BaseTensor<param_t>::typeID::SIMPLE:
         {
            res_tens *= NiceTensor<param_t>(new ExactEnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         case BaseTensor<param_t>::typeID::CANONICAL:
         {
            res_tens *= NiceTensor<param_t>(new EnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         default:
         {
            MIDASERROR("FTCommutator: Cannot perform elementwise product for type " + res_tens.ShowType());
         }
      }

      // Add to result tensor
      arDcOut.GetModeCombiData(i_mc) += res_tens;
   }
}

/**
 * Evaluate [F,T] for NiceTensor
 **/
void FTCommutator::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  NiceTensor<param_t>& arDcOut
   )
{
   const auto& nmodals = aEvalData.GetNModals();
   const auto& eigvals = aEvalData.GetModalEnergies(); // Niels: Due to stupid IO design, this cannot be const...
   const auto& offsets = aEvalData.GetOccModalOffset();
   const auto& mcvec = aMc.MCVec();

   // Get amplitudes
   const auto* amps = aEvalData.template GetExciVec<GeneralTensorDataCont<param_t> >(  this->mType == OP::E
                                             ?  OP::L 
                                             :  this->mType
                                             );

   // Figure out the number of the ModeCombi
   In i_mc = I_0;
   ModeCombiOpRange::const_iterator it_mc;
   if (  aEvalData.GetModeCombiOpRange().Find(aMc, it_mc, i_mc)   // This sets i_mc to the RefNr of aMc
      )
   {
      // Construct result
      auto res_tens = amps->GetModeCombiData(i_mc);

      // Multiply EnergyDifferencesTensor
      switch(res_tens.Type())
      {
         case BaseTensor<param_t>::typeID::SIMPLE:
         {
            res_tens *= NiceTensor<param_t>(new ExactEnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         case BaseTensor<param_t>::typeID::CANONICAL:
         {
            res_tens *= NiceTensor<param_t>(new EnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         default:
         {
            MIDASERROR("FTCommutator: Cannot perform elementwise product for type " + res_tens.ShowType());
         }
      }

      // Add to result tensor
      arDcOut += res_tens;
   }
}

/**
 * Evaluate [F,T] for TensorSumAccumulator
 **/
void FTCommutator::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  TensorSumAccumulator<param_t>& arDcOut
   )
{
   LOGCALL("calls");
   const auto& nmodals = aEvalData.GetNModals();
   const auto& eigvals = aEvalData.GetModalEnergies();
   const auto& offsets = aEvalData.GetOccModalOffset();
   const auto& mcvec = aMc.MCVec();

   // Get amplitudes
   const auto* amps = aEvalData.template GetExciVec<GeneralTensorDataCont<param_t>>(  this->mType == OP::E
                                             ?  OP::L 
                                             :  this->mType
                                             );

   // Figure out the number of the ModeCombi
   In i_mc = I_0;
   ModeCombiOpRange::const_iterator it_mc;
   if (  aEvalData.GetModeCombiOpRange().Find(aMc, it_mc, i_mc)   // This sets i_mc to the RefNr of aMc
      )
   {
      // Construct result
      auto res_tens = amps->GetModeCombiData(i_mc);

      // Multiply EnergyDifferencesTensor
      switch(res_tens.Type())
      {
         case BaseTensor<param_t>::typeID::SIMPLE:
         {
            res_tens *= NiceTensor<param_t>(new ExactEnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         case BaseTensor<param_t>::typeID::CANONICAL:
         {
            res_tens *= NiceTensor<param_t>(new EnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, 0.));
            break;
         }
         default:
         {
            MIDASERROR("FTCommutator: Cannot perform elementwise product for type " + res_tens.ShowType());
         }
      }

      // Add to result tensor
      arDcOut += res_tens;
   }
}


void FTCommutator::WriteSpec(std::ostream& aOut) const
{
   aOut << "FT '";
   aOut << BasicOper::CharForType(mType);
   aOut << mExciLevel << "'";
}

std::ostream& FTCommutator::Print(std::ostream& aOut) const
{
   aOut << "[F, ";
   aOut << BasicOper::CharForType(mType);
   aOut << mExciLevel << "] -> modal energy differences.";
   return aOut;
}

} /* namespace midas::vcc::v3 */
