/**
************************************************************************
* 
* @file                IntermedL_Impl.h
*
* Created:             02-12-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Left-hand vector intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDL_IMPL_H_INCLUDED
#define INTERMEDL_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MMVT.h"
#include "VccEvalData.h"
#include "EvalDataTraits.h"
#include "IntermedL_Decl.h"

#include "libmda/numeric/float_eq.h"


namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedL<T, DATA>::IntermedL
   (  const std::string& aSpec
   )
   :  Intermediate<T, DATA>()
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, mModes);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedL<T, DATA>::IntermedL
   (  const std::vector<In>& aModes
   )
   :  Intermediate<T, DATA>()
   ,  mModes(aModes)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedL<T, DATA>::IntermedL
   (  const IntermedL<T, DATA>& aOrig
   )
   :  Intermediate<T, DATA>(aOrig)
   ,  mModes(aOrig.mModes)
   ,  mExciMc(aOrig.mExciMc)
{
}

/**
 *
 **/
namespace intermed_l::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetGlobalMc
   (
   )
{
   static std::vector<In> global_mc;

   return global_mc;
}
}  /* namespace intermed_l::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedL<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   intermed_l::detail::GetGlobalMc<T, DATA>().resize(mModes.size());
   for (In m=I_0; m<mModes.size(); ++m)
   {
      intermed_l::detail::GetGlobalMc<T, DATA>()[m] = aCmodes[mModes[m]];
   }
   std::sort(intermed_l::detail::GetGlobalMc<T, DATA>().begin(), intermed_l::detail::GetGlobalMc<T, DATA>().end());
   for (In i=I_1; i<intermed_l::detail::GetGlobalMc<T, DATA>().size(); ++i)
   {
      if (  intermed_l::detail::GetGlobalMc<T, DATA>()[i] == intermed_l::detail::GetGlobalMc<T, DATA>()[i-I_1]
         )
      {
         return false;             // Some concrete modes are duplicated...
      }
   }
                                   // ian: note to self; does this ever happen ? 
                                   // ian: answer; yes it does :S
   mExciMc.ReInit(intermed_l::detail::GetGlobalMc<T, DATA>());
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedL<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes.clear();
   aCmodes.resize(mExciMc.Size());
   for (In i=I_0; i<mExciMc.Size(); ++i)
   {
      aCmodes[i] = mExciMc.Mode(i);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   ModeCombiOpRange::const_iterator it;
   In idx;
   if (  !aEvalData.GetModeCombiOpRange().Find(mExciMc, it, idx)
      )
   {
      return false;
   }
   In size = aEvalData.NExciForModeCombi(idx);
   aRes.SetNewSize(size);
   aEvalData.template GetExciVec<GeneralDataCont<T> >(OP::L)->DataIo(IO_GET, aRes, size, it->Address());
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   ModeCombiOpRange::const_iterator it;
   In idx;
   if (  !aEvalData.GetModeCombiOpRange().Find(mExciMc, it, idx)
      )
   {
      return false;
   }
   aRes = aEvalData.template GetExciVec<GeneralTensorDataCont<T> >(OP::L)->GetModeCombiData(idx);

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedL<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   aOut << "L(";
   std::copy(mModes.begin(), mModes.end(), std::ostream_iterator<In>(aOut, ""));
   aOut << ")";
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedL<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "L^{";
   for (In i=I_0; i<mModes.size(); ++i)
      aOut << "m_{" << mModes[i] << "}";
   aOut << "}";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedL<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "l" << mModes;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedL<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedL<T, DATA>* const other = dynamic_cast<const IntermedL<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   return mModes.size() == other->mModes.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedL<T, DATA>::HighestModeNo
   (
   )  const
{
   In high = -I_1;
   for (In i=I_0; i<mModes.size(); ++i)
   {
      if (  mModes[i] > high
         )
      {
         high = mModes[i];
      }
   }
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedL<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   if (  aCpTensors
      )
   {
      MIDASERROR("IntermedL scaling not implemented for CP tensors!");
   }
   return Scaling(mModes.size(), I_0, mModes.size());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedL<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{     
   if (  aCpTensors
      )
   {
      MIDASERROR("IntermedL scaling not implemented for CP tensors!");
   }
   return Scaling(I_0, I_0, mModes.size());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedL<T, DATA>::GetNumber
   (
   )  const
{
   return Scaling(mModes.size(), I_0, mModes.size());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedL<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   if constexpr   (  DataAllowsScreeningV<evaldata_t>
                  )
   {
      aNorm2=1.0;
      ModeCombiOpRange::const_iterator it_mc;
      In i_mc;
      if (  !aEvalData.GetModeCombiOpRange().Find(mExciMc, it_mc, i_mc)
         ) 
      {
         return false;
      }
      aNorm2*=aEvalData.GetNorm2(OP::L,i_mc);

      return true;
   }
   else
   {
      return false;
   }
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDL_IMPL_H_INCLUDED */
