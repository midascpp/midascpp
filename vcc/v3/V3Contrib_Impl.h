/**
************************************************************************
* 
* @file                V3Contrib_Impl.h
*
* Created:             27-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Implementing V3Contrib functions.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef V3CONTRIB_IMPL_H_INCLUDED
#define V3CONTRIB_IMPL_H_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "V3Contrib_Decl.h"
#include "VccEvalData.h"
#include "EvalDataTraits.h"
#include "IntermedProd.h"
#include "IntermedProdL.h"
#include "FndT.h"
#include "FTCommutator.h"

#include "util/MidasSystemCaller.h"

#include "mpi/Impi.h"

namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void V3Contrib<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << std::endl << "LaTeX output not implemented for this V3Contrib derivative." << std::endl;
}

/**
 * Factory
 *
 * @param aSpec
 * @return
 *    pointer to V3Contrib
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::unique_ptr<V3Contrib<T, DATA> > V3Contrib<T, DATA>::Factory
   (  const std::string& aSpec
   )
{
   // Read type of V3Contrib to be created.
   std::string id;
   In pos = I_0;
   while (  aSpec[pos] != ' '
         && pos < aSpec.size()
         )
   {
      id += aSpec[pos++];
   }

   // Extract std::strings enclosed in '...'
   std::vector<std::string> spec_strs;
   while (  pos < aSpec.size()
         )
   {
      std::string s;
      while (aSpec[pos++] != '\'' && pos<aSpec.size());
      while (aSpec[pos] != '\'' && pos<aSpec.size())
         s += aSpec[pos++];
      pos++;
      spec_strs.push_back(s);
   }

   // Check for IntermedProd and IntermedProdL
   if (  id == "IP"
      )
   {
      return std::make_unique<IntermedProd<T, DATA> >(spec_strs);
   }
   else if  (  id == "IPL"
            )
   {
      return std::make_unique<IntermedProdL<T, DATA> >(spec_strs);
   }

   // FTCommutator and FndT are only available for real numbers
   if constexpr   (  DataAllowsVccPertV<evaldata_t>
                  && std::is_same_v<evaldata_t, VccEvalData<Nb> >    // This has to be there because FndT and FTCommutator have not been templated
                  )
   {
      if (  id == "FT"
         )
      {
         return std::make_unique<FTCommutator>(spec_strs);
      }
      else if  (  id == "FndT"
               )
      {
         return std::make_unique<FndT>(spec_strs);
      }
   }
   else if  (  id == "FT"
            || id == "FndT"
            )
   {
      MIDASERROR("V3 contributions 'FT' and 'FndT' are not available for this evaldata_t: '" + std::string(typeid(evaldata_t).name()) + "'.");
   }

   // If we get here, throw an error!
   Mout << " V3Contrib::Factory(): Unknown V3 contribution." << std::endl
        << " aSpec = '" << aSpec << "'" << std::endl; 
   MIDASERROR("Unknown V3 contribution!");
   return nullptr;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool V3Contrib<T, DATA>::ReadV3cFile
   (  const std::string& aFilename
   ,  std::vector<std::unique_ptr<V3Contrib> >& aContribs
   )
{
   if (  !InquireFile(aFilename)
      )
   {
      Mout  << " Could not find v3c file: '" << aFilename 
            << "'. Try to unpack 'v3c.tar.gz' in data directory '" << input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") << "'." << std::endl;
      std::string tarfile = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + std::string("/v3c.tar.gz");
      if (  !InquireFile(tarfile)
         )
      {
         MIDASERROR("v3c.tar.gz not found in '" + input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "'.");
      }

      system_command_t command = {"tar", "-xzvf", tarfile, "-C", input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR")};
      MIDASSYSTEM(command);

      // Try to find the file again. Generate from scratch if not found.
      if (  !InquireFile(aFilename)
         )
      {
         Mout << " Error reading list of V3 contributions from file: " << std::endl
              << "    " << aFilename << std::endl;
         return false;
      }
   }

   // Read file
   std::ifstream file(aFilename.c_str());
   std::string s;
   while (  std::getline(file, s)
         )
   {
      if (s=="" || (s[0]=='/' && s[1]=='/'))
         continue;
      else
      {
         auto v3c = V3Contrib<T, DATA>::Factory(s);
         if (  !v3c
            )
         {
            Mout << std::endl
                 << "Error:" << std::endl
                 << "In file: " << aFilename << std::endl
                 << "V3 contrib spec: " << s << std::endl
                 << "V3Contrib::Factory() returns NULL." << std::endl;
            MIDASERROR("Error reading V3C file!");
         }
         aContribs.emplace_back(std::move(v3c));
      }
   }
   
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void V3Contrib<T, DATA>::WriteV3cFile
   (  const std::string& aFilename
   ,  std::vector<std::unique_ptr<V3Contrib> >& aContribs
   )
{
   Mout << " Saving V3 contributions to file: " << std::endl
        << "    " << aFilename << std::endl;
   midas::mpi::OFileStream file(aFilename.c_str(), midas::mpi::OFileStream::StreamType::MPI_MASTER, ios_base::trunc);
   file << "// List of V3 contributions." << std::endl;
   for (In i=I_0; i<aContribs.size(); ++i)
   {
      aContribs[i]->WriteSpec(file);
      file << std::endl;
   }
}

} /* namespace midas::vcc::v3 */


#endif /* V3CONTRIB_IMPL_H_INCLUDED */
