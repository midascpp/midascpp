/**
************************************************************************
* 
* @file                Scaling.h
*
* Created:             24-03-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class for giving scaling wrt. no. of modes,
*                      operators, and modals.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SCALING_H
#define SCALING_H

#include <iostream>
#include <vector>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

namespace midas::vcc::v3
{

class Scaling
{
   public:
      In mM;         // Scaling: (#modes)^(mM)
      In mO;         // Scaling: (#operators)^(mO)
      In mN;         // Scaling: (#modals)^(mN)

      //! Scaling wrt CP rank of the amplitude tensor
      In mR;

      Scaling
         (  In aM=-I_1
         ,  In aO=-I_1
         ,  In aN=-I_1
         ,  In aR=-I_1
         )
         :  mM(aM)
         ,  mO(aO)
         ,  mN(aN)
         ,  mR(aR)
      {
      }
     
      void Reset();
      
      friend const Scaling operator*(const Scaling& aL, const Scaling& aR);
      friend int operator<(const Scaling& aL, const Scaling& aR);
      friend int operator>(const Scaling& aL, const Scaling& aR);
      friend int operator<=(const Scaling& aL, const Scaling& aR) {return !(aL>aR);}
      friend std::ostream& operator<<(std::ostream& aOut, const Scaling& aSum);
      void Latex(std::ostream& aOut) const;
};

} /* namespace midas::vcc::v3 */

#endif // SCALING_H
