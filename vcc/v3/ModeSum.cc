/**
************************************************************************
* 
* @file                ModeSum.cc
*
* Created:             21-08-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class ModeSum.
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "ModeSum.h"
#include "vcc/v3/Intermediate.h"


namespace midas::vcc::v3
{

void ModeSum::Init(std::string::const_iterator& aPos)
{
   detail::ReadVector(aPos, mModes);
   if (*aPos == '(')
      detail::ReadVector(aPos, mRestrict);
   mCRestrict.resize(mRestrict.size());
}

void ModeSum::WriteSpec(std::ostream& aOut) const
{
   using ::operator<<;
   aOut << "s" << mModes << mRestrict;
}

void ModeSum::Latex(std::ostream& aOut) const
{
   aOut << "\\sum_{";

   for (In i=I_0; i<mModes.size()-I_1; ++i)
      aOut << "m_{" << mModes[i] << "}<";
   if (! mModes.empty())
      aOut << "m_{" << mModes.back() << "}";

   if (!mRestrict.empty())
   {
      aOut << " \\neq \\{";
      for (In i=I_0; i<mRestrict.size()-I_1; ++i)
         aOut << "m_{" << mRestrict[i] << "},";
      if (! mRestrict.empty())
         aOut << "m_{" << mRestrict.back() << "}";
      aOut << "\\}";
   }

   aOut << "}";
}

std::ostream& operator<<(std::ostream& aOut, const ModeSum& aSum)
{
   aOut << "S(";
   std::copy(aSum.mModes.begin(), aSum.mModes.end()-I_1, std::ostream_iterator<In>(aOut, "<"));
   if (! aSum.mModes.empty())
      aOut << aSum.mModes.back();
   if (!aSum.mRestrict.empty())
   {
      aOut << "\\";
      std::copy(aSum.mRestrict.begin(), aSum.mRestrict.end(), std::ostream_iterator<In>(aOut, ""));
   }
   aOut << ")";
   
   return aOut;
}

} /* namespace midas::vcc::v3 */
