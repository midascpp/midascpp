/**
************************************************************************
* 
* @file    IntermediateRestrictions.h
*
* @date    28-10-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Struct that contains restrictions on the types of saved
*     intermediates in IntermediateMachine.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef INTERMEDIATERESTRICTIONS_H_INCLUDED
#define INTERMEDIATERESTRICTIONS_H_INCLUDED

#include <utility>

struct IntermediateRestrictions
{
   //! Minimum number of down contractions
   In mMinDown = I_1;

   //! Min. and max. number of forward contractions
   std::pair<In,In> mForward = { I_0, I_0 };
};

#endif /* INTERMEDIATERESTRICTIONS_H_INCLUDED */
