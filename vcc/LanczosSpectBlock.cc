/**
************************************************************************
* 
* @file                LanczosSpectBlock.cc
*
* Created:             10-11-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   LanczosSpectBlock implementation.
* 
* Last modified: Mon Jan 18, 2010  03:59PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosSpectBlock.h"
#include "vcc/VccTransformer.h"
#include "vcc/Vcc.h"

using namespace std;

LanczosSpectBlock::LanczosSpectBlock(Nb aBegin, Nb aEnd):
   mBegin(aBegin), mEnd(aEnd), mIntegrInty(C_0), mNstates(I_0)
{}

ostream& operator<<(ostream& aOut, const LanczosSpectBlock& aBlock)
{
   midas::stream::ScopedPrecision(2, aOut);
   aOut << "    " << aBlock.mBegin*C_AUTKAYS << " - " << aBlock.mEnd*C_AUTKAYS << " cm-1 ("
        << aBlock.mNstates << " states included)" << endl;
   midas::stream::ScopedPrecision(16, aOut);
   aOut << "    Integrated intensity = " << aBlock.mIntegrInty << " (arb. u.)" << endl;
   midas::stream::ScopedPrecision(3, aOut);
   aOut << "    Integrated intensity = " 
        << C_100*aBlock.mIntegrInty/aBlock.mMaxInt << " (% of largest intensity)" << endl;

   if (aBlock.mVcc == NULL)
      return aOut;

   // Get transformer so that we can get the Xvec.
   VccTransformer* trf = dynamic_cast<VccTransformer*>(aBlock.mVcc->GetRspTransformer());
   if (NULL == trf)
      MIDASERROR("LanczosSpectBlock, operator<<: Error in getting Xvec.");
   
   // Output detailed data on weights.
   const In nout = I_20;
   DataCont& dc_weights = (DataCont&)(aBlock.mWeights);
   vector<Nb> weights(nout);
   vector<In> addr(nout);
   dc_weights.AddressOfExtrema(addr, weights, nout, I_1); 
   
   aOut << "    Largest weights: ";
   for (In i=I_0; i<nout; ++i)
   {
      midas::stream::ScopedPrecision(3, aOut);
      aOut << fixed << weights[i] << " ";
      aBlock.mVcc->ModesAndLevelsOut(aOut, addr[i]+I_1, trf->GetTransXvec(), false);
      if (i != nout-I_1)
         aOut << " + ";
   }
   aOut << endl
        << "                     ";
   for (In i=I_0; i<nout; ++i)
   {
      midas::stream::ScopedPrecision(3, aOut);
      aOut << fixed << weights[i] << " ";
      aBlock.mVcc->ModesAndLevelsOutLatex(aOut, addr[i]+I_1, trf->GetTransXvec());
      if (i != nout-I_1)
         aOut << " + ";
   }
   
   return aOut;
}

