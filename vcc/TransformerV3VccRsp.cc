/**
 ************************************************************************
 * 
 * @file                TransformerV3VccRsp.cc
 *
 * Created:             01-10-2008
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementation of VCC Response transformer stuff.
 * 
 * Last modified: Mon May 17, 2010  02:20PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <ctime>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/Vcc.h"
#include "vcc/TransformerV3.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "vcc/GenDownCtr.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "v3/IntermedProdL.h"
#include "input/OpDef.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "tensor/EnergyDifferencesTensor.h"

namespace midas::vcc
{

/**
 * Initialization commmon to all VCC response eigenvalue problems.  The
 * transformer assumes the amplitudes to be stored in mXvec.GetXvecData() so
 * put them there. Also, since the amplitudes don't change, we can do the T1
 * transformation once and for all.
 *
 * @param arFileName    Name of the file where the VCC amplitudes are found
 **/
void TransformerV3::BasicVccRspInit
   (  const std::string& arFileName
   )
{
   //Mout << " In BasicVccRspInit..." << std::endl;

   // Convert xvecs to TensorDataCont if we are using the tensor solver
   if (  this->mpVccCalcDef->UseTensorNlSolver()
      )
   {
      // Make sure Xvecs are in TensorDataCont format
      this->mXvec.ConvertToTensorDataCont();
      this->mTransXvec.ConvertToTensorDataCont();

      TensorDataCont tdc;
      tdc.ReadFromDisc( arFileName );

      this->mXvec.Assign( tdc, Xvec::load_in, Xvec::add_ref );
   }
   else
   {
      // Make sure Xvecs are in DataCont format
      this->mXvec.ConvertToDataCont();
      this->mTransXvec.ConvertToDataCont();
      
      // Get VCC amplitudes for reference state.
      In n_amp = mTransXvec.Size() - I_1;
      DataCont amp_dc;
      amp_dc.GetFromExistingOnDisc(n_amp, arFileName);
      amp_dc.SaveUponDecon();
      amp_dc.Reassign(mXvec.GetXvecData(), I_1, -I_1);   // Xvec has reference at index zero.
      param_t zero = C_0;
      mXvec.GetXvecData().DataIo(IO_PUT, I_0, zero);
   }

   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the amplitudes
   bool purify =  mpVccCalcDef->SumNprim()
               || mpVccCalcDef->Emaxprim()
               || mpVccCalcDef->Zeroprim();
   if (  purify   )
   {
      this->mXvec.ConvertToDataCont();
      if (mpVccCalcDef->SumNprim()) SumNpurify(mXvec.GetXvecData());
      if (mpVccCalcDef->Emaxprim()) Emaxpurify(mXvec.GetXvecData());
      if (mpVccCalcDef->Zeroprim()) Zeropurify(mXvec.GetXvecData());
      this->mXvec.ConvertToTensorDataCont();
   }
   
   // Update integrals.
   if (mpVccCalcDef->T1TransH())
      UpdateModalIntegrals(true, &mXvec);
}

/**
 * Initialization commmon to all VCC response eigenvalue problems.  The
 * transformer assumes the amplitudes to be stored in mXvec.GetXvecData() so
 * put them there. Also, since the amplitudes don't change, we can do the T1
 * transformation once and for all.
 *
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::BasicVccRspInit
   (  const TensorDataCont& arAmplitudes
   )
{
   this->mXvec.ConvertToTensorDataCont();
   this->mTransXvec.ConvertToTensorDataCont();

   this->mXvec.Assign( arAmplitudes, Xvec::load_in, Xvec::add_ref );

   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the amplitudes
   bool purify =  mpVccCalcDef->SumNprim()
               || mpVccCalcDef->Emaxprim()
               || mpVccCalcDef->Zeroprim();
   if (  purify   )
   {
      this->mXvec.ConvertToDataCont();
      if (mpVccCalcDef->SumNprim()) SumNpurify(mXvec.GetXvecData());
      if (mpVccCalcDef->Emaxprim()) Emaxpurify(mXvec.GetXvecData());
      if (mpVccCalcDef->Zeroprim()) Zeropurify(mXvec.GetXvecData());
      this->mXvec.ConvertToTensorDataCont();
   }

   
   // Update integrals.
   if (mpVccCalcDef->T1TransH())
      UpdateModalIntegrals(true, &mXvec);
}

/**
 * Initialize general VCC Jacobian transformer
 *
 * @param arFileName    Name of the file containing the VCC amplitudes
 **/
void TransformerV3::InitGenVccJacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))   
      Mout << " Initializing V3 transformer for VCC Jacobian transformations." << std::endl
           << "    H dim:                  " << mpOpDef->McLevel() << std::endl
           << "    Max. excitation level:  " << mMaxExciLevel << std::endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << std::endl;

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitGenVccJacobian();
}

/**
 * Initialize general VCC Jacobian transformer
 *
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitGenVccJacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))   
      Mout << " Initializing V3 transformer for VCC Jacobian transformations." << std::endl
           << "    H dim:                  " << mpOpDef->McLevel() << std::endl
           << "    Max. excitation level:  " << mMaxExciLevel << std::endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << std::endl;

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitGenVccJacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitGenVccJacobian
   (
   )
{
   ClearContribs();

   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << "_Jacobian.v3c";
   //CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      
      vector<CtrProd> ctr_prods;
      
      // Generate list of contraction products for [H,R1], [H,R2], [H,R3], ...
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=max_exci; ++r_lvl)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h_dim);
            cmt.AddOper(OP::R, r_lvl);
            vector<Commutator> bch;
            cmt.BCH(min_exci, max_exci, max_exci, bch);
            Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
            for (In i=I_0; i<bch.size(); ++i)
            {
               Mout << "." << std::flush;
               bch[i].GetCtrProds(I_1, max_exci, ctr_prods);
            }
            Mout << std::endl;
         }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for IntermedProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC1pt2Jacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[1pt2] Jacobian (for 2-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2, T1TransH == false, or dimension of H > 2");

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitVCC1pt2Jacobian();
}

/**
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitVCC1pt2Jacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[1pt2] Jacobian (for 2-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2, T1TransH == false, or dimension of H > 2");

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitVCC1pt2Jacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitVCC1pt2Jacobian
   (
   )
{
   ClearContribs();

   string v3c_file = mpVccCalcDef->V3FilePrefix() + "VCC[1pt2]_Jacobian.v3c";
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1R1"));
      cmts.push_back(Commutator(C_1, "H1R1T2"));
      cmts.push_back(Commutator(C_1, "H2R1T2"));
      cmts.push_back(Commutator(C_1, "H1R2"));
      cmts.push_back(Commutator(C_1, "H2R2"));
      //cmts.push_back(Commutator(C_1, "H2R2T2"));  // This will not contribute.
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_1, ctr_prods);
      
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H2R1"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_2, ctr_prods);
     
      CtrProdsToV3(ctr_prods);
      mContribs.push_back(v3contrib_t::Factory("FT 'R2'")); 
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   RegisterIntermeds();
   if (  this->mpVccCalcDef->IoLevel()>I_10  )
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC2Jacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2] Jacobian (for 2-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2, T1TransH == false, or dimension of H > 2");

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitVCC2Jacobian();
}

/**
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitVCC2Jacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2] Jacobian (for 2-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2, T1TransH == false, or dimension of H > 2");

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitVCC2Jacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitVCC2Jacobian
   (
   )
{
   ClearContribs();

   string v3c_file = mpVccCalcDef->V3FilePrefix() + "VCC[2]-Explicit_Jacobian.v3c";
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      cmts.push_back(Commutator(C_1, "H1R1"));
      cmts.push_back(Commutator(C_1, "H2R1"));
      cmts.push_back(Commutator(C_1, "H1R1T2"));
      cmts.push_back(Commutator(C_1, "H2R1T2"));
      cmts.push_back(Commutator(C_1, "H1R2"));
      cmts.push_back(Commutator(C_1, "H2R2"));
      cmts.push_back(Commutator(C_1, "H2R2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_2, ctr_prods);
     
      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   RegisterIntermeds();
   if (  this->mpVccCalcDef->IoLevel()>I_10  )
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC2pt3Jacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2pt3] Jacobian (for 3-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 3");

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitVCC2pt3Jacobian();
}

/**
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitVCC2pt3Jacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2pt3] Jacobian (for 3-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 3");

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitVCC2pt3Jacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitVCC2pt3Jacobian
   (
   )
{
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[2pt3]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_Jacobian.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1R1"));
      cmts.push_back(Commutator(C_1, "H3T3R1"));
      cmts.push_back(Commutator(C_1, "H3R3"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_1, ctr_prods);
     
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1,     "H1R1T2"));
      cmts.push_back(Commutator(C_1,     "H1R1T3"));
      cmts.push_back(Commutator(C_1/C_2, "H1R1T2T2"));
      cmts.push_back(Commutator(C_1,     "H2R1"));
      cmts.push_back(Commutator(C_1,     "H2R1T3"));
      cmts.push_back(Commutator(C_1/C_2, "H2R1T2T2"));
      cmts.push_back(Commutator(C_1,     "H3R1T2"));
      cmts.push_back(Commutator(C_1/C_2, "H3R1T2T2"));
      cmts.push_back(Commutator(C_1, "H1R2"));
      cmts.push_back(Commutator(C_1, "H1R3"));
      cmts.push_back(Commutator(C_1, "H1R2T2"));
      cmts.push_back(Commutator(C_1, "H2R3"));
      cmts.push_back(Commutator(C_1, "H2R2T2"));
      cmts.push_back(Commutator(C_1, "H3R2"));
      cmts.push_back(Commutator(C_1, "H3R2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_2, ctr_prods);

      // Commutators present for all excitation levels.
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H3R1"));
      cmts.push_back(Commutator(C_1, "H2R1T2"));
      cmts.push_back(Commutator(C_1, "H2R2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_3, ctr_prods);

      CtrProdsToV3(ctr_prods);
      
      mContribs.push_back(v3contrib_t::Factory("FT 'R3'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3R3'")); 
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_7)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
   
   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4FJacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4F] Jacobian (up to 4-mode Hamiltonian.)"
           << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, or dimension of H > 4");

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitVCC3pt4FJacobian();
}

/**
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4FJacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4F] Jacobian (up to 4-mode Hamiltonian.)"
           << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, or dimension of H > 4");

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitVCC3pt4FJacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitVCC3pt4FJacobian
   (
   )
{
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4F]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_Jacobian.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;

      vector<CtrProd> ctr_prods;

      // Generate <mu_0| to <mu_2| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
         for (In r=I_1; r<I_5; ++r)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h);
            cmt.AddOper(OP::R, r);
            vector<Commutator> bch;
            cmt.BCH(I_2, I_4, I_4, bch);
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_1, I_2, ctr_prods);
            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4)) || (comm.ContainsOper(OP::R,I_4))) && (comm.MaxPtOrder() > I_4 ))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_3, I_3, ctr_prods);

            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (comm.MaxPtOrder() > I_3 )
                  q = bch.erase(q) - I_1;
            } 
            if (h == I_1) 
            {
               vector<Commutator> cmts;
               cmts.push_back(Commutator(C_1, "H1R4"));
               cmts.push_back(Commutator(C_1, "H1R1T4"));
               cmts.push_back(Commutator(C_1, "H1R2T3"));
               cmts.push_back(Commutator(C_1, "H1R3T2"));
               DeleteCommutatorInVec(cmts,bch);
            }
            //Mout << " Commutator for mu_4 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_4, I_4, ctr_prods);
         }

      CtrProdsToV3(ctr_prods);

      mContribs.push_back(v3contrib_t::Factory("FT 'R4'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3R4'")); 
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4R4'")); //H4 contribution 
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }

   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4Jacobian
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4] Jacobian (up to 4-mode Hamiltonian.)"
           << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       //mpVccCalcDef->R1TransH() != false ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, R1TransH == true, or dimension of H > 4");

   this->BasicVccRspInit(arFileName);
   this->FinalizeInitVCC3pt4Jacobian();
}

/**
 * @param arAmplitudes    VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4Jacobian
   (  const TensorDataCont& arAmplitudes
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4] Jacobian (up to 4-mode Hamiltonian.)"
           << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       //mpVccCalcDef->R1TransH() != false ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, R1TransH == true, or dimension of H > 4");

   this->BasicVccRspInit(arAmplitudes);
   this->FinalizeInitVCC3pt4Jacobian();
}

/**
 *
 **/
void TransformerV3::FinalizeInitVCC3pt4Jacobian
   (
   )
{
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_Jacobian.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;

      vector<CtrProd> ctr_prods;

      // Generate <mu_0| to <mu_1| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
         for (In r=I_1; r<I_5; ++r)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h);
            cmt.AddOper(OP::R, r);
            vector<Commutator> bch;
            cmt.BCH(I_2, I_4, I_4, bch);
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_1, I_1, ctr_prods);
            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_5 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::R,I_3))))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_2, I_2, ctr_prods);

            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_4 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::R,I_3))))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_3, I_3, ctr_prods);

            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (comm.MaxPtOrder() > I_3 )
                  q = bch.erase(q) - I_1;
            } 
            if (h == I_1) 
            {
               vector<Commutator> cmts;
               cmts.push_back(Commutator(C_1, "H1R4"));
               cmts.push_back(Commutator(C_1, "H1R1T4"));
               cmts.push_back(Commutator(C_1, "H1R2T3"));
               cmts.push_back(Commutator(C_1, "H1R3T2"));
               DeleteCommutatorInVec(cmts,bch);
            }
            //Mout << " Commutator for mu_4 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_4, I_4, ctr_prods);
         }

      CtrProdsToV3(ctr_prods);

      mContribs.push_back(v3contrib_t::Factory("FT 'R4'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3R4'")); 
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4R4'")); //H4 contribution 
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }

   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4FJacobianL
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4F] left-hand Jacobian transformations" << std::endl
           << " (for  up to 4-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_4 ||
         mpVccCalcDef->T1TransH() != true ||
         mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false,  or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4F]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_JacobianL.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;

      vector<CtrProd> ctr_prods;

      // Generate <mu_0| to <mu_2| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
         for (In e=I_1; e<I_5; ++e)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h);
            cmt.AddOper(OP::E, e);
            vector<Commutator> bch;
            cmt.BCH(I_2, I_4, I_4, bch);
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_1, I_2, ctr_prods);
            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4)) || (comm.ContainsOper(OP::E,I_4))) && (comm.MaxPtOrder() > I_4 ))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_3, I_3, ctr_prods);

            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (comm.MaxPtOrder() > I_3 )
                  q = bch.erase(q) - I_1;
            } 
            if (h == I_1) 
            {
               vector<Commutator> cmts;
               cmts.push_back(Commutator(C_1, "H1E4"));
               cmts.push_back(Commutator(C_1, "H1E1T4"));
               cmts.push_back(Commutator(C_1, "H1E2T3"));
               cmts.push_back(Commutator(C_1, "H1E3T2"));
               DeleteCommutatorInVec(cmts,bch);
            }
            //Mout << " Commutator for mu_4 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_4, I_4, ctr_prods);
         }

      
      CtrProdsToV3(ctr_prods);
     
      mContribs.push_back(v3contrib_t::Factory("FT 'E4'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3E4'")); 
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4E4'")); //H4 contribution 
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
   
   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4JacobianL
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4] left-hand Jacobian transformations" << std::endl
           << " (for  up to 4-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_4 ||
         mpVccCalcDef->T1TransH() != true ||
         mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false,  or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_JacobianL.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;

      vector<CtrProd> ctr_prods;

      // Generate <mu_0| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
         for (In e=I_1; e<I_5; ++e)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h);
            cmt.AddOper(OP::E, e);
            vector<Commutator> bch;
            cmt.BCH(I_2, I_4, I_4, bch);
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_1, I_1, ctr_prods);

      // Generate <mu_2| equations.
            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::E,I_4)) && (comm.MaxPtOrder() > I_5 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::E,I_3))))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_2, I_2, ctr_prods);

      // Generate <mu_3| equations.
            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::E,I_4)) && (comm.MaxPtOrder() > I_4 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::E,I_3))))
                  q = bch.erase(q) -I_1;
            } 
            //Mout << " Commutator for mu_3 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_3, I_3, ctr_prods);

            for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            {
               Commutator comm = *q;
               if (comm.MaxPtOrder() > I_3 )
                  q = bch.erase(q) - I_1;
            } 
            if (h == I_1) 
            {
               vector<Commutator> cmts;
               cmts.push_back(Commutator(C_1, "H1E4"));
               cmts.push_back(Commutator(C_1, "H1E1T4"));
               cmts.push_back(Commutator(C_1, "H1E2T3"));
               cmts.push_back(Commutator(C_1, "H1E3T2"));
               DeleteCommutatorInVec(cmts,bch);
            }
            //Mout << " Commutator for mu_4 " << std::endl;
            //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
            //{
            //   Commutator comm = *q;
            //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
            //} 
            for (In i=I_0; i<bch.size(); ++i)
               bch[i].GetCtrProds(I_4, I_4, ctr_prods);
         }

      
      CtrProdsToV3(ctr_prods);
     
      mContribs.push_back(v3contrib_t::Factory("FT 'E4'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3E4'")); 
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4E4'")); //H4 contribution 
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
   
   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC2pt3JacobianL
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 for VCC[2pt3] left-hand Jacobian transformations" << std::endl
           << " (for 3-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 3");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[2pt3]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_JacobianL.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1E1"));
      cmts.push_back(Commutator(C_1, "H3E1T3"));
      cmts.push_back(Commutator(C_1, "H3E3"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_1, ctr_prods);
     
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1,     "H1E1T2"));
      cmts.push_back(Commutator(C_1,     "H1E1T3"));
      cmts.push_back(Commutator(C_1/C_2, "H1E1T2T2"));
      cmts.push_back(Commutator(C_1,     "H2E1"));
      cmts.push_back(Commutator(C_1,     "H2E1T3"));
      cmts.push_back(Commutator(C_1/C_2, "H2E1T2T2"));
      cmts.push_back(Commutator(C_1,     "H3E1T2"));
      cmts.push_back(Commutator(C_1/C_2, "H3E1T2T2"));
      cmts.push_back(Commutator(C_1,     "H1E2"));
      cmts.push_back(Commutator(C_1,     "H1E3"));
      cmts.push_back(Commutator(C_1,     "H1E2T2"));
      cmts.push_back(Commutator(C_1,     "H2E3"));
      cmts.push_back(Commutator(C_1,     "H2E2T2"));
      cmts.push_back(Commutator(C_1,     "H3E2"));
      cmts.push_back(Commutator(C_1,     "H3E2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_2, ctr_prods);

      // Commutators present for all excitation levels.
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H3E1"));
      cmts.push_back(Commutator(C_1, "H2E1T2"));
      cmts.push_back(Commutator(C_1, "H2E2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_1, I_3, ctr_prods);

      CtrProdsToV3(ctr_prods);
      
      mContribs.push_back(v3contrib_t::Factory("FT 'E3'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3E3'")); 
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
   
   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 * Do vcc Jacobian transformationusing DataConts
 * @param arDcIn         Input DataCont
 * @param arDcOut        Output DataCont
 * @param aI             switch parameter
 * @param aJ             switch parameter
 * @param aNb
 **/
void TransformerV3::VccTransJac
   ( DataCont& arDcIn
   , DataCont& arDcOut
   , In aI
   , In aJ
   , param_t& aNb
   )
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   clock_t t0 = clock();

   // aI controls diagonal.
   if (I_0 == aI)
   {
      arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
      if (mpVccCalcDef->GetTrueHDiag())
      {
         TransformerTrueHDiag(aJ, aNb);
      }
      else
      {
         TransformerH0(aJ, aNb);
      }
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1, -aNb);

      param_t shift = this->mpVcc->GetUseVccRspRefE() ? this->mpVcc->GetRspRefE() : this->mEvmp0;
      if (  this->mpVcc->GetUseVccRspRefE()
         )
      {
         Mout  << " TransformerV3::VccTransJac: add ref energy: e_vcc = " << shift << " after H diag transform." << std::endl;
      }
      arDcOut.Axpy(arDcIn, shift);  // Niels: This will only work if we are NOT using an inverse transform in TransformerH0... Who did this?

      if (  aJ < -I_2
         )
      {
         MIDASERROR("Niels: The implementation of VCC Jac diag with VCC ref energy does not work for inverse transforms...");
      }

      // The above code may have destroyed the internal state of the transformer.
      // Make sure it is set as expected by future transformations using the full Jacobian.
      const std::string amplitude_file = this->mpVccCalcDef->GetName() + "_Vcc_vec_0";
      BasicVccRspInit(amplitude_file);
      return;
   }
  
   mImMachine->ClearIntermeds(true);
   mTensorImMachine->ClearIntermeds(true);
   evaldata_t eval_data(this, &mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt(), &mFockInts);
   eval_data.AssignDataVec(OP::T, &mXvec.GetXvecData());
  
   // Put arDcIn containing eigenvector coefficients in new data container consistent
   // with Xvec indexing.
   DataCont in_vec(mTransXvec.Size());
   in_vec.NewLabel("temp_in_vec");
   arDcIn.Reassign(in_vec, I_1, -I_1);

   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the input.
   if (mpVccCalcDef->SumNprim()) SumNpurify(in_vec);
   if (mpVccCalcDef->Emaxprim()) Emaxpurify(in_vec);
   if (mpVccCalcDef->Zeroprim()) Zeropurify(in_vec);
   
   if (mType == TRANSFORMER::VCCJAC) 
      eval_data.AssignDataVec(OP::R, &in_vec); // Right-hand transformation.
   else if (mType == TRANSFORMER::LVCCJAC)
      eval_data.AssignDataVec(OP::L, &in_vec); // Left-hand transformation.
   else
      MIDASERROR("TransformerV3::VccTransJac(): Wrong mType. Something is very wrong.");

   // Do actual transformation.
   EvaluateContribs(eval_data, mTransXvec.GetXvecData());
   
   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the output.
   if (mpVccCalcDef->SumNprim()) SumNpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim()) Emaxpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim()) Zeropurify(mTransXvec.GetXvecData());
   
   // Store result in arDcOut.
   arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);

   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::VccTransJac() time: "
           << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
}

/**
 * Do vcc Jacobian transformation using TensorDataConts.
 * mXvec holds here the T-amplitudes and are is not overwritten in this function!
 * @param arDcIn         Input TensorDataCont
 * @param arDcOut        Output TensorDataCont
 * @param aI             switch parameter
 * @param aJ             switch parameter
 * @param aNb
 * @param arDecompThresholds  Decomposition thresholds for each MC
 * @param arLaplaceQuad       LaplaceQuadrature%s for 0th-order Jacobian transform
 **/
void TransformerV3::VccTransJac
   (  const TensorDataCont& arDcIn
   ,  TensorDataCont& arDcOut
   ,  In aI
   ,  In aJ
   ,  param_t& aNb
   ,  const std::vector<param_t>& arDecompThresholds
   ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad
   )
{
   MidasAssert(this->mTensorTransform, "This function should only be called when using tensors. Otherwise mTensorImMachine will not be ready!");
   clock_t t0 = clock();

   bool ref_incl = arDcIn.GetModeCombiData(I_0).Type() == BaseTensor<param_t>::typeID::SCALAR;

   // aI controls diagonal.
   if (  I_0 == aI
      )
   {
      // Niels: Should we perhaps change this such that true diag has its own aI-key?
      // TransformerTrueHDiag only gives a good approximation to the VCC Jacobian if aNb = E_VSCF!!!
      if (  this->mpVccCalcDef->GetTrueHDiag()
         )
      {
         // Load into mXvec
         this->mXvec.ConvertToTensorDataCont();
         
         if (  ref_incl
            )
         {
            // arDcIn contains ref already
            this->mXvec.Assign( arDcIn, Xvec::load_in, Xvec::no_add_ref );
         }
         else
         {
            // arDcIn does not contain reference
            this->mXvec.Assign( arDcIn, Xvec::load_in, Xvec::add_ref );
         }
   
         this->mXvec.ConvertToDataCont(); // convert to DataCont
         this->mTransXvec.ConvertToDataCont();  // convert to DataCont
         this->TransformerTrueHDiag(aJ, aNb);   // Transform mXvec and store the result in mTransXvec
         this->mTransXvec.ConvertToTensorDataCont();  // Convert mTransXvec to TensorDataCont
         this->mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);  // Load mTransXvec without ref. into output vector.

         // Restore transformer state
         // The above code destroyed the internal state of the transformer by overwriting mXvec.
         // Make sure it is set as expected by future transformations using the full Jacobian.
         this->BasicVccRspInit(this->mAmplitudeFile);
      }
      else
      {
         auto shift = aNb;

         if (  this->mpVcc->GetUseVccRspRefE()
            )
         {
            auto delta_e = this->mEvmp0 - this->mpVcc->GetRspRefE();
            if (  !libmda::numeric::float_numeq_zero(delta_e, this->mEvmp0, 2)
               )
            {
               Mout  << " VccTransJac: Calling TensorTransformerA0 with a modified shift from the Rsp ref energy." << "\n"
                     << "    E_VMP0:      " << this->mEvmp0 << "\n"
                     << "    Rsp Ref E:   " << this->mpVcc->GetRspRefE() << "\n"
                     << "    Delta E:     " << delta_e << "\n"
                     << std::flush;
            }

            shift -= delta_e;
         }


         arDcOut = this->TensorTransformerA0(arDcIn, aJ, shift, arLaplaceQuad);
      }
      return;
   }
  
   // clear intermediates (keeping T intermeds, which can be saved across Transformations)
   this->mTensorImMachine->ClearIntermeds(true);
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   evaldata_t eval_data(this, &mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt(), &mFockInts);
   eval_data.AssignTensorDataVec(OP::T, &mXvec.GetTensorXvecData()); // mXvec holds the ground-state amplitudes
   eval_data.AssignDecomposer(&decomposer);
   eval_data.AssignAllowedRank(mAllowedRank);
  
   // Put arDcIn containing eigenvector coefficients in new data container consistent with Xvec indexing.
   // Include reference if not included already.
   TensorDataCont in_vec = ref_incl ? arDcIn : TensorDataCont::AddReference(arDcIn, 0.);

   // Set R or L vectors in eval_data
   if (mType == TRANSFORMER::VCCJAC) 
   {
      eval_data.AssignTensorDataVec(OP::R, &in_vec); // Right-hand transformation.
   }
   else if (mType == TRANSFORMER::LVCCJAC)
   {
      eval_data.AssignTensorDataVec(OP::L, &in_vec); // Left-hand transformation.
   }
   else
      MIDASERROR("TransformerV3::VccTransJac(): Wrong mType. Something is very wrong.");

   // Do actual transformation.
   this->EvaluateContribsNew(eval_data, mTransXvec.GetTensorXvecData(), arDecompThresholds);
   
   // Store result in arDcOut.
   this->mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);

   // DEBUG: Compare to DataCont transform
   if (  this->mpVccCalcDef->CheckTensorVccTransJac()
      )
   {
      this->mXvec.ConvertToDataCont();
      this->mTransXvec.ConvertToDataCont();
      auto dc_in = DataContFromTensorDataCont(arDcIn);
      auto dc_out = DataContFromTensorDataCont(arDcOut);
      auto norm = dc_out.Norm();
      auto dc_out_new = dc_out;
      this->VccTransJac(dc_in, dc_out_new, aI, aJ, aNb);
      dc_out_new.Axpy(dc_out, -C_1);
      auto err = dc_out_new.Norm();
      Mout  << " VCCTRANSJAC DEBUG COMPARE TO DATACONT TRANSFORM:" << "\n"
            << "    Transformation error:    " << err << "\n"
            << "    Norm of dc_out:          " << norm << "\n"
            << std::flush;
      if (  !libmda::numeric::float_numeq_zero(err, C_1, 2)
         )
      {
         MIDASERROR("Error in VccTransJac compared to DataCont transform.");
      }
      this->mXvec.ConvertToTensorDataCont();
      this->mTransXvec.ConvertToTensorDataCont();

      // Use the debug vector as output to see if the results change
      if (  this->mpVccCalcDef->TensorVccJacUseDebugVector()
         )
      {
         this->mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);
      }
   }

   // Timer output
   if (  this->mpVccCalcDef->TimeIt()
      )
   {
      Mout << " TransformerV3::VccTransJac() time: "
           << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
}

/**
 *
 **/
void TransformerV3::InitGenVccJacobianL
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))   
   {
      Mout << " Initializing V3 transformer for left-hand VCC Jacobian transformations." << std::endl
           << "    H dim:                  " << mpOpDef->McLevel() << std::endl
           << "    Max. excitation level:  " << mMaxExciLevel << std::endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << std::endl;
   }

   BasicVccRspInit(arFileName);
   ClearContribs();

   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << "_JacobianL.v3c";
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
   {
      Mout << " V3 WARNING: Check of left-Jacobian method disabled!" << std::endl;
      MidasWarning("V3 WARNING: Left-hand Jacobian is experimental!");
   }
   //CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
  
   //CtrProd::msUseRforL = true; 
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      
      vector<CtrProd> ctr_prods;
      
      // Generate list of contraction products for [H,R1], [H,R2], [H,R3], ...
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In e_lvl=I_1; e_lvl<=max_exci; ++e_lvl)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h_dim);
            cmt.AddOper(OP::E, e_lvl);
            vector<Commutator> bch;
            cmt.BCH(min_exci, max_exci, max_exci, bch);
            Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
            for (In i=I_0; i<bch.size(); ++i)
            {
               Mout << "." << std::flush;
               bch[i].GetCtrProds(I_1, max_exci, ctr_prods, true);
            }
            Mout << std::endl;
         }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for IntermedProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   //CtrProd::msUseRforL = false; 
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * Default implementation
 **/
void TransformerV3::InitGenVccRspEta0
   (
   )
{
   this->InitGenVccRspEta0(this->mAmplitudeFile);
}

/**
 * @param arFileName    File containing VCC amplitudes
 **/
void TransformerV3::InitGenVccRspEta0
   (  const std::string& arFileName
   )
{
   BasicVccRspInit(arFileName);
   ClearContribs();

   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << "_eta0.v3c";
   Mout << " V3 WARNING: Check of eta0 method disabled!" << std::endl;
   MidasWarning("V3 WARNING: eta0 calculated using experimental code!");
   //CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Calculating contributions from scratch." << std::endl;
      vector<CtrProd> ctr_prods;
      
      // Generate list of contraction products for 'H tau exp(T)'
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In e_lvl=1; e_lvl<=max_exci; ++e_lvl)
         {
            Commutator cmt;
            cmt.SetCoef(C_1);
            cmt.AddOper(OP::H, h_dim);
            cmt.AddOper(OP::E, e_lvl);
            vector<Commutator> bch;
            cmt.BCH(min_exci, max_exci, I_0, bch);
            Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
            for (In i=I_0; i<bch.size(); ++i)
            {
               Mout << "." << std::flush;
               bch[i].GetCtrProds(I_0, I_0, ctr_prods);
            }
            Mout << std::endl;
         }

      CtrProdsToV3(ctr_prods, false);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(false);
   RegisterIntermeds();
}

/**
 *
 **/
void TransformerV3::CalcVccRspEta0
   (  DataCont& aRes
   )
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   if (mpVccCalcDef->V3type() != V3::GEN &&
       mpVccCalcDef->V3type() != V3::VCC2PT3 &&
       mpVccCalcDef->V3type() != V3::VCC3PT4F &&
       mpVccCalcDef->V3type() != V3::VCC3PT4)
      MIDASERROR(" Eta0 vector not supported for this VCC method.");

   InitGenVccRspEta0();
   
   // Setup structures needed for evaluation. 
   DataCont ref_vec(mXvec.Nexci(), C_1); // The reference coefficient.
                                         // ian: we only need 1 coef, 
                                         // but we need to allocate complete 
                                         // because of EvalData norm calculation
   evaldata_t eval_data(this,&mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt());
   eval_data.AssignDataVec(OP::T, &mXvec.GetXvecData());
   
   eval_data.AssignDataVec(OP::L, &ref_vec); // ian: apparently coefficient is an l vec... :S

   // Do actual calculation. 
   EvaluateContribs(eval_data, mTransXvec.GetXvecData());
   
   // Store result in aRes.
   aRes.Reassign(mTransXvec.GetXvecData(), I_1, I_1);
}

/**
 *
 **/
void TransformerV3::CalcVccRspEta0
   (  TensorDataCont& aRes
   )
{
   MidasAssert(this->mTensorTransform, "This function should only be called when using tensors. Otherwise mTensorImMachine will not be ready!");
   if (  mpVccCalcDef->V3type() != V3::GEN
      && mpVccCalcDef->V3type() != V3::VCC2PT3
      && mpVccCalcDef->V3type() != V3::VCC3PT4F
      && mpVccCalcDef->V3type() != V3::VCC3PT4
      )
   {
      MIDASERROR(" Eta0 vector not supported for this VCC method.");
   }

   this->InitGenVccRspEta0();
   
   // Setup structures needed for evaluation. 
   TensorDataCont ref_vec = aRes.GetModeCombiData(0).IsScalar() ? aRes : TensorDataCont::AddReference(aRes);
   ref_vec.Zero();
   ref_vec.GetModeCombiData(I_0) = NiceScalar<param_t>(C_1, mAllowedRank != 0); // Set ref to 1
                                                                           // ian: we only need 1 coef, 
                                                                           // but we need to allocate complete 
                                                                           // because of EvalData norm calculation

//   Mout  << " Calculate Eta0 with min decomp thresh:  " << midas::tensor::GetDecompThreshold(mDecompInfo, true) << std::endl;
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   evaldata_t eval_data(this, &mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt());
   eval_data.AssignDecomposer(&decomposer);
   eval_data.AssignAllowedRank(mAllowedRank);
   eval_data.AssignTensorDataVec(OP::T, &mXvec.GetTensorXvecData());
   
   eval_data.AssignTensorDataVec(OP::L, &ref_vec); // ian: apparently coefficient is an l vec... :S

   // Do actual calculation. 
   EvaluateContribsNew(eval_data, mTransXvec.GetTensorXvecData());

   // Store result in aRes.
   mTransXvec.Assign(aRes, Xvec::load_out, Xvec::add_ref);

}

/**
 * Default implementation
 **/
void TransformerV3::InitVccRspB
   (
   )
{
   this->InitVccRspB(this->mAmplitudeFile);
}

/**
 * @param arFileName    Name of file containing VCC amplitudes
 **/
void TransformerV3::InitVccRspB
   (  const std::string& arFileName
   )
{
   switch (mpVccCalcDef->V3type())
   {
      case V3::GEN:
         InitGenVccRspB(arFileName);
         break;
      case V3::VCC2PT3:
         InitVCC2pt3RspB(arFileName);
         break;
      case V3::VCC3PT4F:
         InitVCC3pt4FRspB(arFileName);
    return;
      case V3::VCC3PT4:
         InitVCC3pt4RspB(arFileName);
         break;
      default:
         MIDASERROR("TransformerV3::InitVccRspB(): Unsupported V3 type.");
   }
}

/**
 * Generate V3 contribution for evaluation of transformation with the VCC response
 * B matrix ( <u|exp(-T) [[H,R],S] exp(T) |ref> ).
 *
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitGenVccRspB
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))   
   {
      Mout << " Initializing V3 transformer for VCC Response B matrix transformations." << std::endl
           << "    H dim:                  " << mpOpDef->McLevel() << std::endl
           << "    Max. excitation level:  " << mMaxExciLevel << std::endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << std::endl;
   }

   BasicVccRspInit(arFileName); 
   ClearContribs();

   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << "_B.v3c";
   //Mout << " V3 WARNING: Check of B matrix method disabled!" << std::endl;
   MidasWarning("V3 WARNING: B matrix transformation is experimental!");
   //CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      
      vector<CtrProd> ctr_prods;
      
      // Generate list of contraction products for [[H,R],S] BCH expansion.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=max_exci; ++r_lvl)
            for (In s_lvl=I_1; s_lvl<=max_exci; ++s_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::S, s_lvl);
               vector<Commutator> bch;
               cmt.BCH(min_exci, max_exci, max_exci, bch);
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, max_exci, ctr_prods);
               }
               Mout << std::endl;
            }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for IntermedProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

void TransformerV3::InitVCC2pt3RspB
   (  const std::string& arFileName
   )
{
   Mout << " Initializing V3 transformer for VCC[2pt3] B matrix transformation" << std::endl
        << " (for 3-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 3");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[2pt3]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_B.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1R1S1"));
      cmts.push_back(Commutator(C_1, "H3R1S3"));
      cmts.push_back(Commutator(C_1, "H3R3S1"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_1, ctr_prods);
     
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H1R1S2"));
      cmts.push_back(Commutator(C_1, "H1R2S1"));
      cmts.push_back(Commutator(C_1, "H2R1S1"));
      cmts.push_back(Commutator(C_1, "H2R1S3"));
      cmts.push_back(Commutator(C_1, "H2R3S1"));
      cmts.push_back(Commutator(C_1, "H2R2S2"));
      cmts.push_back(Commutator(C_1, "H3R2S1"));
      cmts.push_back(Commutator(C_1, "H3R1S2"));
      cmts.push_back(Commutator(C_1, "H3R2S2"));
      cmts.push_back(Commutator(C_1, "H3R1S1T2"));
      cmts.push_back(Commutator(C_1, "H3R1S2T2"));
      cmts.push_back(Commutator(C_1, "H3R2S1T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_2, ctr_prods);

      // Commutators present for all excitation levels.
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H2R1S2"));
      cmts.push_back(Commutator(C_1, "H2R2S1"));
      cmts.push_back(Commutator(C_1, "H2R1S1T2"));
      cmts.push_back(Commutator(C_1, "H3R1S1"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_3, ctr_prods);

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4FRspB
   (  const std::string& arFileName
   )
{
   Mout << " Initializing V3 transformer for VCC[3pt4F] response B matrix transformations." << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4F]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_B.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Generate list of contraction products for [[H,R],tau] BCH expansion for <mu_0| to <mu_3|.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=I_4; ++r_lvl)
            for (In s_lvl=I_1; s_lvl<=I_4; ++s_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::S, s_lvl);
               vector<Commutator> bch;
               cmt.BCH(I_2, I_4, I_4, bch);
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, I_2, ctr_prods);
               }
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (((comm.ContainsOper(OP::T, I_4)) || (comm.ContainsOper(OP::E,I_4))) || ((comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_4 )))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_3 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_3, I_3, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (comm.MaxPtOrder() > I_3 )
                     q = bch.erase(q) - I_1;
               } 
               //if (h == I_1) 
               //{
               //   vector<Commutator> cmts;
               //   cmts.push_back(Commutator(C_1, "H1R4"));
               //   cmts.push_back(Commutator(C_1, "H1R1T4"));
               //   cmts.push_back(Commutator(C_1, "H1R2T3"));
               //   cmts.push_back(Commutator(C_1, "H1R3T2"));
               //   DeleteCommutatorInVec(cmts,bch);
               //}
               Mout << " Commutator for mu_4 " << std::endl;
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               } 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_4, I_4, ctr_prods);
            }


      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4RspB
   (  const std::string& arFileName
   )
{
   Mout << " Initializing V3 transformer for VCC[3pt4] response B matrix transformations." << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_B.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Generate list of contraction products for [[H,R],tau] BCH expansion for <mu_0| to <mu_3|.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=I_4; ++r_lvl)
            for (In s_lvl=I_1; s_lvl<=I_4; ++s_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::S, s_lvl);
               vector<Commutator> bch;
               cmt.BCH(I_2, I_4, I_4, bch);
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, I_1, ctr_prods);
               }
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::S,I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_5 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::S,I_3) || comm.ContainsOper(OP::R,I_3))))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_2 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_2, I_2, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::S,I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_4 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::S,I_3) || comm.ContainsOper(OP::R,I_3))))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_3 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_3, I_3, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (comm.MaxPtOrder() > I_3 )
                     q = bch.erase(q) - I_1;
               } 
               //if (h == I_1) 
               //{
               //   vector<Commutator> cmts;
               //   cmts.push_back(Commutator(C_1, "H1R4"));
               //   cmts.push_back(Commutator(C_1, "H1R1T4"));
               //   cmts.push_back(Commutator(C_1, "H1R2T3"));
               //   cmts.push_back(Commutator(C_1, "H1R3T2"));
               //   DeleteCommutatorInVec(cmts,bch);
               //}
               Mout << " Commutator for mu_4 " << std::endl;
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               } 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_4, I_4, ctr_prods);
            }


      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * Evaluate B*R*S = <mu| exp(-T) [[H,R],S] exp(T) |ref>. Results for excited <mu| are stored
 * in arDcOut and result for <mu|=<ref| is stored in aRef.
 **/
void TransformerV3::VccTransRspB
   ( DataCont& arDcInR
   , DataCont& arDcInS
   , DataCont& arDcOut
   , param_t& aRefOut
   )
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   clock_t t0 = clock();

   mImMachine->ClearIntermeds();

   // Put arDcInR and arDcInS in new data containers consistent with Xvec indexing.
   DataCont r_vec(mTransXvec.Size());
   arDcInR.Reassign(r_vec, I_1, -I_1);
   DataCont s_vec(mTransXvec.Size());
   arDcInS.Reassign(s_vec, I_1, -I_1);

   // Do actual transformation.
   evaldata_t eval_data(this,&mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt(), &mFockInts);
   eval_data.AssignDataVec(OP::T, &mXvec.GetXvecData());
   eval_data.AssignDataVec(OP::R, &r_vec);
   eval_data.AssignDataVec(OP::S, &s_vec);
   
   EvaluateContribs(eval_data, mTransXvec.GetXvecData());

   // Store result in aRef and arDcOut.
   mTransXvec.GetXvecData().DataIo(IO_GET, I_0, aRefOut);
   arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);

   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::VccTransRspB() time: "
         << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
}

/**
 * Evaluate B*R*S = <mu| exp(-T) [[H,R],S] exp(T) |ref>. Results for excited <mu| are stored
 * in arDcOut and result for <mu|=<ref| is stored in aRef.
 **/
void TransformerV3::VccTransRspB
   (  const TensorDataCont& arInR
   ,  const TensorDataCont& arInS
   ,  TensorDataCont& arOut
   ,  param_t& aRefOut
   )
{
   MidasAssert(this->mTensorTransform, "This function should only be called when using tensors. Otherwise mTensorImMachine will not be ready!");
   this->mTensorImMachine->ClearIntermeds();

   // Add reference if none is found
   const auto& s0 = arInS.GetModeCombiData(0);
   const auto& r0 = arInR.GetModeCombiData(0);
   bool r_ref_incl = r0.IsScalar() || r0.TotalSize() == 0;
   bool s_ref_incl = s0.IsScalar() || s0.TotalSize() == 0;

   auto r_vec = r_ref_incl ? arInR : TensorDataCont::AddReference(arInR, C_1);
   auto s_vec = s_ref_incl ? arInS : TensorDataCont::AddReference(arInS, C_1);;

   // Do actual transformation.
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   evaldata_t eval_data
      (  this
      ,  &mTransXvec
      ,  mpVccCalcDef
      ,  mpVcc
      ,  mpOpDef
      ,  pOneModeModalBasisInt()
      ,  &mFockInts
      );
   eval_data.AssignDecomposer(&decomposer);
   eval_data.AssignAllowedRank(mAllowedRank);
   eval_data.AssignTensorDataVec(OP::T, &mXvec.GetTensorXvecData());
   eval_data.AssignTensorDataVec(OP::R, &r_vec);
   eval_data.AssignTensorDataVec(OP::S, &s_vec);
   
   this->EvaluateContribsNew(eval_data, mTransXvec.GetTensorXvecData());

   // Store result in aRef and arDcOut.
   aRefOut = mTransXvec.GetTensorXvecData().GetModeCombiData(0).GetScalar();
   mTransXvec.Assign(arOut, Xvec::load_out, Xvec::add_ref);
}

/**
 * Evaluate F*R*S = <Lamda| exp(-T) [[H,R],S] exp(T) |ref>.
 * The result is returned.
 **/
typename TransformerV3::param_t TransformerV3::CalcVccRspFRS
   (  DataCont& arDcInMult
   ,  DataCont& arDcInR
   ,  DataCont& arDcInS
   )
{
   if (mpVccCalcDef->GetNumVccF() != C_0)
      return CalcVccRspFRSNum(arDcInR, arDcInS);

   DataCont b_res(arDcInMult.Size());
   param_t b_res_ref = C_0;

   InitVccRspB();
   VccTransRspB(arDcInR, arDcInS, b_res, b_res_ref);

   return b_res_ref + Dot(arDcInMult, b_res);
}

/**
 * Evaluate F*R*S = <Lamda| exp(-T) [[H,R],S] exp(T) |ref>.
 * The result is returned.
 **/
typename TransformerV3::param_t TransformerV3::CalcVccRspFRS
   (  const TensorDataCont& arMult
   ,  const TensorDataCont& arR
   ,  const TensorDataCont& arS
   )
{
   if (  mpVccCalcDef->GetNumVccF() != C_0
      )
   {
      MIDASERROR("Numerical F transform not implemented for TensorDataCont!");
   }

   TensorDataCont b_res = arMult;
   b_res.Zero();
   param_t b_res_ref = C_0;

   this->InitVccRspB();
   this->VccTransRspB(arR, arS, b_res, b_res_ref);

   return b_res_ref + Dot(arMult, b_res);
}

typename TransformerV3::param_t TransformerV3::CalcVccRspFRSNum(DataCont& arDcInR, DataCont& arDcInS)
{
   Mout << " Calculating F*R*S using numerical F matrix." << std::endl;
   In vecsize = arDcInR.Size();
   DataCont F_dc;
   F_dc.SaveUponDecon();
   F_dc.GetFromExistingOnDisc(vecsize*vecsize, mpVccCalcDef->GetName()+"_Vcc_F");

   DataCont fr(vecsize);
   F_dc.TransformMx(vecsize, vecsize, arDcInR, fr);
   return Dot(fr, arDcInS);
}

/**
 * Default implementation
 **/
void TransformerV3::InitVccRspFR
   (
   )
{
   this->InitVccRspFR(this->mAmplitudeFile);
}

/**
 * @param arFileName    Name of file containing VCC amplitudes
 **/
void TransformerV3::InitVccRspFR
   (  const std::string& arFileName
   )
{
   switch (mpVccCalcDef->V3type())
   {
      case V3::GEN:
         InitGenVccRspFR(arFileName);
         break;
      case V3::VCC2PT3:
         InitVCC2pt3RspFR(arFileName);
         break;
      case V3::VCC3PT4F:
         InitVCC3pt4FRspFR(arFileName);
    return;
      case V3::VCC3PT4:
         InitVCC3pt4RspFR(arFileName);
         break;
      default:
         MIDASERROR("TransformerV3::InitVccRspFB(): Unsupported V3 type.");
   }
}

/**
 * Initialize F*R = <Lamda| exp(-T) [[H,R],tau_mu] exp(T) |ref> transformation.
 *
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitGenVccRspFR
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC response F*R vector evaluation."
           << std::endl
           << "    H dim:                  " << mpOpDef->McLevel() << std::endl
           << "    Max. excitation level:  " << mMaxExciLevel << std::endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << std::endl;

   BasicVccRspInit(arFileName); 
   ClearContribs();

   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << "_FR.v3c";
   if(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()))
   {
      Mout << " V3 WARNING: Check of F*R evaluation method disabled!" << std::endl;
      MidasWarning("V3 WARNING: F*R evaluation transformation is experimental!");
   }
   //CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      
      vector<CtrProd> ctr_prods;
      
      // Generate list of contraction products for [[H,R],tau] BCH expansion.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=max_exci; ++r_lvl)
            for (In e_lvl=I_1; e_lvl<=max_exci; ++e_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::E, e_lvl);
               vector<Commutator> bch;
               cmt.BCH(min_exci, max_exci, max_exci, bch);
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, max_exci, ctr_prods);
               }
               Mout << std::endl;
            }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for IntermedProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   IdentifyCmbIntermeds(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   ChangeOutputStatus(TRANSFORMER::SPECIAL,mpVccCalcDef->V3type());
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC2pt3RspFR
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2pt3] response F*R vector evaluation." << std::endl
           << " (for 3-mode Hamiltonian.)" << std::endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 3");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[2pt3]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_FR.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1R1E1"));
      cmts.push_back(Commutator(C_1, "H3R1E3"));
      cmts.push_back(Commutator(C_1, "H3R3E1"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_1, ctr_prods);
    
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H1R1E2"));
      cmts.push_back(Commutator(C_1, "H1R2E1"));
      cmts.push_back(Commutator(C_1, "H2R1E1"));
      cmts.push_back(Commutator(C_1, "H2R1E3"));
      cmts.push_back(Commutator(C_1, "H2R3E1"));
      cmts.push_back(Commutator(C_1, "H2R2E2"));
      cmts.push_back(Commutator(C_1, "H3R1E2"));
      cmts.push_back(Commutator(C_1, "H3R2E1"));
      cmts.push_back(Commutator(C_1, "H3R2E2"));
      cmts.push_back(Commutator(C_1, "H3R1E1T2"));
      cmts.push_back(Commutator(C_1, "H3R1E2T2"));
      cmts.push_back(Commutator(C_1, "H3R2E1T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_2, ctr_prods);

      // Commutators present for all excitation levels.
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H2R1E2"));
      cmts.push_back(Commutator(C_1, "H2R2E1"));
      cmts.push_back(Commutator(C_1, "H2R1E1T2"));
      cmts.push_back(Commutator(C_1, "H3R1E1"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_3, ctr_prods);
  
      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   ChangeOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type());
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4FRspFR
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4F] response F*R vector evaluation." << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 3, T1TransH == false, or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4F]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_FR.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Generate list of contraction products for [[H,R],tau] BCH expansion for <mu_0| to <mu_3|.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=I_4; ++r_lvl)
            for (In e_lvl=I_1; e_lvl<=I_4; ++e_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::E, e_lvl);
               vector<Commutator> bch;
               cmt.BCH(I_2, I_4, I_4, bch);
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, I_2, ctr_prods);
               }
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (((comm.ContainsOper(OP::T, I_4)) || (comm.ContainsOper(OP::S,I_4))) || ((comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_4 )))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_3 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_3, I_3, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (comm.MaxPtOrder() > I_3 )
                     q = bch.erase(q) - I_1;
               } 
               //if (h == I_1) 
               //{
               //   vector<Commutator> cmts;
               //   cmts.push_back(Commutator(C_1, "H1R4"));
               //   cmts.push_back(Commutator(C_1, "H1R1T4"));
               //   cmts.push_back(Commutator(C_1, "H1R2T3"));
               //   cmts.push_back(Commutator(C_1, "H1R3T2"));
               //   DeleteCommutatorInVec(cmts,bch);
               //}
               Mout << " Commutator for mu_4 " << std::endl;
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               } 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_4, I_4, ctr_prods);
            }


      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   ChangeOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type());
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 * @param arFileName    Name of file containing the VCC amplitudes
 **/
void TransformerV3::InitVCC3pt4RspFR
   (  const std::string& arFileName
   )
{
   if(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4] response F*R vector evaluation." << std::endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4, T1TransH == false, or dimension of H > 4");

   BasicVccRspInit(arFileName);
   ClearContribs();

   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4]_H" << mpOpDef->McLevel();
   ss_v3c_file << "_FR.v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << std::endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
     
      // Generate list of contraction products for [[H,R],tau] BCH expansion for <mu_0| to <mu_3|.
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
         for (In r_lvl=I_1; r_lvl<=I_4; ++r_lvl)
            for (In e_lvl=I_1; e_lvl<=I_4; ++e_lvl)
            {
               Commutator cmt;
               cmt.SetCoef(C_1);
               cmt.AddOper(OP::H, h_dim);
               cmt.AddOper(OP::R, r_lvl);
               cmt.AddOper(OP::E, e_lvl);
               vector<Commutator> bch;
               cmt.BCH(I_2, I_4, I_4, bch);
               Mout << "    BCH of " << cmt << " -> " << bch.size() << " commutators: ";
               for (In i=I_0; i<bch.size(); ++i)
               {
                  Mout << "." << std::flush;
                  bch[i].GetCtrProds(I_0, I_1, ctr_prods);
               }
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::E,I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_5 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::E,I_3) || comm.ContainsOper(OP::R,I_3))))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_2 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_2, I_2, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
               if (((comm.ContainsOper(OP::T, I_4) || comm.ContainsOper(OP::E,I_4) || comm.ContainsOper(OP::R,I_4)) && (comm.MaxPtOrder() > I_4 )) || (comm.ContainsOper(OP::H,I_4) && (comm.ContainsOper(OP::T,I_3) || comm.ContainsOper(OP::E,I_3) || comm.ContainsOper(OP::R,I_3))))
                     q = bch.erase(q) -I_1;
               } 
               //Mout << " Commutator for mu_3 " << std::endl;
               //for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               //{
               //   Commutator comm = *q;
               //   Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               //} 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_3, I_3, ctr_prods);

               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  if (comm.MaxPtOrder() > I_3 )
                     q = bch.erase(q) - I_1;
               } 
               //if (h == I_1) 
               //{
               //   vector<Commutator> cmts;
               //   cmts.push_back(Commutator(C_1, "H1R4"));
               //   cmts.push_back(Commutator(C_1, "H1R1T4"));
               //   cmts.push_back(Commutator(C_1, "H1R2T3"));
               //   cmts.push_back(Commutator(C_1, "H1R3T2"));
               //   DeleteCommutatorInVec(cmts,bch);
               //}
               Mout << " Commutator for mu_4 " << std::endl;
               for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
               {
                  Commutator comm = *q;
                  Mout << "comm=" << comm << " comm.MaxPtOrder()= " << comm.MaxPtOrder() << std::endl;
               } 
               for (In i=I_0; i<bch.size(); ++i)
                  bch[i].GetCtrProds(I_4, I_4, ctr_prods);
            }


      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   ChangeOutputStatus(TRANSFORMER::SPECIAL, mpVccCalcDef->V3type());
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      ListRegisteredIntermeds();
   }
}

/**
 *
 **/
void TransformerV3::CalcVccRspFR
   (  DataCont& arDcInMults
   ,  DataCont& arDcInR
   ,  DataCont& arDcOut
   )
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   InitVccRspFR();

   clock_t t0 = clock();

   mImMachine->ClearIntermeds();

   // Put arDcInMults and arDcInR in new data containers consistent with Xvec indexing.
   DataCont mult_vec(mTransXvec.Size());
   arDcInMults.Reassign(mult_vec, I_1, -I_1);
   param_t one = C_1;
   mult_vec.DataIo(IO_PUT, I_0, one);            // Reference state.
   DataCont r_vec(mTransXvec.Size());
   arDcInR.Reassign(r_vec, I_1, -I_1);

   // Do actual transformation.
   evaldata_t eval_data(this,&mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt(), &mFockInts);
   eval_data.AssignDataVec(OP::T, &mXvec.GetXvecData());
   eval_data.AssignDataVec(OP::R, &r_vec);
   eval_data.AssignDataVec(OP::L, &mult_vec);
   
   EvaluateContribs(eval_data, mTransXvec.GetXvecData());

   arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);

   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::VccTransRspFR() time: "
         << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
}

/**
 * Perform F transformation on right eigenvector
 *
 * @param arMults    0th-order multipliers
 * @param arR        Right eigenvector
 * @param arOut      Output vector (result)
 **/
void TransformerV3::CalcVccRspFR
   (  const TensorDataCont& arMults
   ,  const TensorDataCont& arR
   ,  TensorDataCont& arOut
   )
{
   MidasAssert(this->mTensorTransform, "This function should only be called when using tensors. Otherwise mTensorImMachine will not be ready!");
   // Initialize contribs
   this->InitVccRspFR();

   this->mTensorImMachine->ClearIntermeds();

   // Add reference if none is found
   const auto& m0 = arMults.GetModeCombiData(0);
   const auto& r0 = arR.GetModeCombiData(0);
   bool r_ref_incl = r0.IsScalar() || r0.TotalSize() == 0;
   bool m_ref_incl = m0.IsScalar() || m0.TotalSize() == 0;

   auto r_vec = r_ref_incl ? arR : TensorDataCont::AddReference(arR, C_1);
   auto mult_vec = m_ref_incl ? arMults : TensorDataCont::AddReference(arMults, C_1);;

   // Do actual transformation.
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);

   evaldata_t eval_data
      (  this
      ,  &mTransXvec
      ,  mpVccCalcDef
      ,  mpVcc
      ,  mpOpDef
      ,  pOneModeModalBasisInt()
      ,  &mFockInts
      );
   eval_data.AssignDecomposer(&decomposer);
   eval_data.AssignAllowedRank(mAllowedRank);
   eval_data.AssignTensorDataVec(OP::T, &mXvec.GetTensorXvecData());
   eval_data.AssignTensorDataVec(OP::R, &r_vec);
   eval_data.AssignTensorDataVec(OP::L, &mult_vec);
   
   this->EvaluateContribsNew(eval_data, mTransXvec.GetTensorXvecData());

   // Remove reference when loading out
   this->mTransXvec.Assign(arOut, Xvec::load_out, Xvec::add_ref);
}

/**
 *
 **/
void TransformerV3::TestLtransAlgos()
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   Mout << " Testing left-hand transformer algorithms..." << std::endl << std::endl
      << " Using oper:" << std::endl << *mpOpDef << std::endl;

   // Generate random amplitudes and coefficients.
   In namps = mTransXvec.Size();
   DataCont dc_in(namps, C_0);
   param_t val = C_0;
   srand((unsigned)time(0)); 
   for (In i=I_0; i<namps; ++i)
   {
      val = param_t((rand() % I_10) + I_1)/C_10;
      mXvec.GetXvecData().DataIo(IO_PUT, i, val);
      val = param_t((rand() % I_10) + I_1)/C_10;
      dc_in.DataIo(IO_PUT, i, val);
   }
   val = C_0;
   dc_in.DataIo(IO_PUT, I_0, val);
  
   // Update integrals.
   if (mpVccCalcDef->T1TransH())
      UpdateModalIntegrals(true, &mXvec);

   // Setup evaluation data and intermediate machine.
   evaldata_t eval_data(this,&mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt());
   eval_data.AssignDataVec(OP::T, &mXvec.GetXvecData());
   eval_data.AssignDataVec(OP::L, &dc_in);
   mImMachine->Reset();

   // Generate list of contraction products for [H,R1], [H,R2], [H,R3], ...
   clock_t t0 = clock();
   Mout << " Generating list of V3 contributions from scratch." << std::endl;
   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH()? I_2:I_1;
   vector<CtrProd> ctr_prods_old;                              // Using old code.
   vector<CtrProd> ctr_prods_new;                              // Using new code.

   for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
      for (In e_lvl=1; e_lvl<=max_exci; ++e_lvl)
      {
         Commutator cmt;
         cmt.SetCoef(C_1);
         cmt.AddOper(OP::H, h_dim);
         cmt.AddOper(OP::E, e_lvl);
         vector<Commutator> bch;
         cmt.BCH(min_exci, max_exci, max_exci, bch);
         Mout << "    BCH of " << cmt << " -> " << setw(2) << right << bch.size()
              << " commutators: " << std::flush;
         for (In i=I_0; i<bch.size(); ++i)
         {
            CtrProd::msUseRforL = true; 
            bch[i].GetCtrProds(I_1, max_exci, ctr_prods_old, false);
            CtrProd::msUseRforL = false; 
            bch[i].GetCtrProds(I_1, max_exci, ctr_prods_new, false);
            Mout << "." << std::flush;
         }
         Mout << std::endl;
      }

/* --- Example of how to check a single basic operator product.
   BasicOperProd bop(C_1, "T2T2H5T2T2E1", true);
   Mout << " Getting ctr. prods. from basic operator product: " << bop << std::endl;
   CtrProd::msUseRforL = true;
   bop.GetCtrProds(min_exci, max_exci, ctr_prods_old, false);
   CtrProd::msUseRforL = false;
   bop.GetCtrProds(min_exci, max_exci, ctr_prods_new, false);
*/   
   Mout << " Total time: " << param_t(clock() - t0) / CLOCKS_PER_SEC << " s." << std::endl;

   // Evaluate each contraction product.
   Mout << " Comparing intermediate products..." << std::endl;
   Mout << " Total # of products: " << ctr_prods_old.size() << std::endl;
   DataCont dc_out1(eval_data.NExci());
   DataCont dc_out2(eval_data.NExci());
   In nfails = I_0;
   for (In i=I_0; i<ctr_prods_old.size(); ++i)
   {
      // Generate V3 contributions.
      ostringstream os;
      CtrProd::msUseRforL = true; 
      ctr_prods_old[i].WriteV3ContribSpec(os);
      auto v3_old = v3contrib_t::Factory(os.str());
      CtrProd::msUseRforL = false; 
      os.str("");
      ctr_prods_new[i].WriteV3ContribSpec(os);
      auto v3_new = v3contrib_t::Factory(os.str());
  
      // Evaluate. 
      dc_out1.Zero();
      dc_out2.Zero(); 
      v3_old->Evaluate(eval_data, dc_out1);
      v3_new->Evaluate(eval_data, dc_out2);
     
      // Test if results are identical.
      Mout << " Product no. " << setw(4) << right << i << "/"
           << setw(4) << right << ctr_prods_old.size()-I_1 << ": ";
      if (CmpDCs(dc_out1, dc_out2, true, false))
         Mout << "Success." << std::endl;
      else
      {
         nfails++;
         Mout << "Failure." << std::endl
              << "    cmp: " << *v3_old << std::endl
              << "         " << *v3_new << std::endl;
         CmpDCs(dc_out1, dc_out2, true, true);
      }
   }
   Mout << " Intermediate product comparison: Total number of failures: "
        << nfails << std::endl << std::endl; 

   Mout << " Testing removal of sum restrictions..." << std::endl;
   vector<CtrProd> ctr_prods_nsr;
   In nsr_idx = I_0;
   nfails = I_0;
   for (In i=I_0; i<ctr_prods_new.size(); ++i)
   {
      // Evaluate V3 contrib.
      ostringstream os;
      ctr_prods_new[i].WriteV3ContribSpec(os);
      auto v3 = v3contrib_t::Factory(os.str());
      dc_out1.Zero();
      v3->Evaluate(eval_data, dc_out1);
      
      // Remove sum restrictions and evaluate each resulting product.
      vector<CtrProd> tmp_cp;
      ctr_prods_new[i].RemoveSumRestrictions(tmp_cp);
      std::vector<std::unique_ptr<v3contrib_t>> v3_nsr;
      dc_out2.Zero();
      for (In k=I_0; k<tmp_cp.size(); ++k)
      {
         os.str("");
         tmp_cp[k].WriteV3ContribSpec(os);
         v3_nsr.push_back(v3contrib_t::Factory(os.str()));
         v3_nsr.back()->Evaluate(eval_data, dc_out2);
         nsr_idx++;
      }
      std::copy(tmp_cp.begin(), tmp_cp.end(), back_inserter(ctr_prods_nsr));
      
      // Test if results are identical.
      Mout << " Product no. " << setw(4) << right << i << "/"
           << setw(4) << right << ctr_prods_new.size()-I_1 << ": "; 
      if (CmpDCs(dc_out1, dc_out2, true, false))
         Mout << "Success." << std::endl;
      else
      {
         nfails++;
         Mout << "Failure." << std::endl;
         Mout << "    orig:   " << *v3 << std::endl;
         for (In k=I_0; k<v3_nsr.size(); ++k)
            Mout << "    nsr " << setw(2) << right << k << ": " << *v3_nsr[k] << std::endl;
         CmpDCs(dc_out1, dc_out2, true, true);
      }
   }
   Mout << " Removal of sum restrictions: Total number of failures: " << nfails << std::endl << std::endl;

   Mout << " Testing combined intermediates..." << std::endl;
   v3::Scaling max_m_sc_bef;
   v3::Scaling max_n_sc_bef;
   v3::Scaling max_m_sc;
   v3::Scaling max_n_sc;
   v3::IntermediateMachine<param_t, GeneralMidasVector, midas::vcc::v3::VccEvalData> im_mach;
   nfails = I_0;
   for (In i=I_0; i<ctr_prods_nsr.size(); ++i)
   {
      dc_out1.Zero();
      dc_out2.Zero();

      ostringstream os;
      ctr_prods_nsr[i].WriteV3ContribSpec(os);
      auto v3 = v3contrib_t::Factory(os.str());
      v3->Evaluate(eval_data, dc_out1);

      // Identify combined intermediates.
      auto ipl = midas::util::DynamicCastUniquePtr<v3::IntermedProdL<param_t, midas::vcc::v3::VccEvalData>>(v3->Clone());
      MidasAssert(ipl.get() != nullptr, "Expected to get IntermedProdL. Dynamic cast failed!");
      v3::Scaling sc = ipl->GetScaling();
      if (sc.mM >= max_m_sc_bef.mM)
         max_m_sc_bef = max(sc, max_m_sc_bef);
      if (sc.mN > max_n_sc_bef.mN)
         max_n_sc_bef = max(sc, max_n_sc_bef);
      ipl->IdentifyCmbIntermeds(sc);
      if (sc.mM > max_m_sc.mM)
         max_m_sc = max(sc, max_m_sc);
      if (sc.mN > max_n_sc.mN)
         max_n_sc = max(sc, max_n_sc);
      ipl->Evaluate(eval_data, dc_out2);
      ipl->RegisterIntermeds(im_mach);

      Mout << setw(4) << right << i << "/"
           << setw(4) << right << ctr_prods_nsr.size()-I_1 << ": ";
      if (CmpDCs(dc_out1, dc_out2, true, false))
         Mout << "Success." << std::endl;
      else
      {
         nfails++;
         Mout << "Failure." << std::endl
              << "    cmp: " << *v3 << std::endl
              << "         " << *ipl << std::endl;
         CmpDCs(dc_out1, dc_out2, true, true);
      }
   }
   Mout << " Combined intermediates test: Total number of failures: " << nfails << std::endl << std::endl;

   Mout << " Final list of contributions:" << std::endl;
   for (In i=I_0; i<ctr_prods_nsr.size(); ++i)
   {
      ostringstream os;
      ctr_prods_nsr[i].WriteV3ContribSpec(os);
      auto v3 = v3contrib_t::Factory(os.str());
      Mout << " " << *v3 << std::endl;
   }
   
   im_mach.ListRegisteredIntermeds(Mout);

   Mout << std::endl
        << " Scaling: " << std::endl
        << "    Max M before: " << max_m_sc_bef << std::endl
        << "    Max M after:  " << max_m_sc << std::endl
        << "    Max N before: " << max_n_sc_bef << std::endl
        << "    Max N after:  " << max_n_sc << std::endl;

   MIDASERROR(" Testing done."); 
}

/**
 * Transform with 0th-order Jacobian by first generating LaplaceQuadratures.
 **/
GeneralTensorDataCont<typename TransformerV3::param_t> TransformerV3::TensorTransformerA0
   (  const GeneralTensorDataCont<param_t>& arTdcIn
   ,  const In& arJ
   ,  const param_t& arNb
   ,  const laplaceinfo_t& arLapInfo
   )
{
   // Declare pointers
   Vcc* const vcc = this->pVcc(); 
   const auto& mc_op_range = this->GetTransXvecModeCombiOpRange();   // Niels: If VCCJAC, mXvec is initalized out of space...
   const auto& nmodals = vcc->pVccCalcDef()->GetNModals();
   const auto& eigvals = vcc->GetEigVal();
   const auto& offsets = vcc->GetOccModalOffSet();

   // Check that the input matches the ModeCombiOpRange
   bool incl_ref = arTdcIn.GetModeCombiData(I_0).Type() == BaseTensor<param_t>::typeID::SCALAR;
   if (  !(    arTdcIn.Size() == mc_op_range.Size()-I_1
            || (incl_ref && arTdcIn.Size() == mc_op_range.Size())
          )
      )
   {
      MIDASERROR("Input TensorDataCont has wrong size compared to ModeCombiOpRange!");
   }

   // Get smallest tensor order that is decomposed
   In min_decomp_order = C_IN_MAX;
   for(const auto& info : this->mDecompInfo)
   {
      min_decomp_order = std::min(min_decomp_order, info.at("DECOMPTOORDER").get<In>());
   }

   // Generate LaplaceQuadrature vector
   std::vector<LaplaceQuadrature<param_t>> lapvec;

   size_t count = 0;
   for(const auto& mc : mc_op_range)
   {
      if (  mc.Size() == 0
         )
      {
         // Increment count if ref is included in input vector
         if (  incl_ref
            )
         {
            ++count;
         }
         continue;
      }

      bool decomp =  (  mc.Size() >= min_decomp_order
                     && min_decomp_order >= I_0 
                     && arTdcIn.GetModeCombiData(count).Type() != BaseTensor<param_t>::typeID::SIMPLE
                     );

      if (  decomp
         )
      {
         lapvec.emplace_back(arLapInfo, mc.MCVec(), nmodals, eigvals, offsets, arNb);
      }
      else
      {
         lapvec.emplace_back(LaplaceQuadrature<Nb>());
      }

      ++count;
   }


   return this->TensorTransformerA0(arTdcIn, arJ, arNb, lapvec);
}

/**
 *    Transform an input TensorDataCont with the 0th-order Jacobian
 *    or its inverse and return the result.
 *
 *    @param arTdcIn
 *       Input TensorDataCont.
 *    @param arJ
 *       -  arJ < -2 => Inverse transformation  \f$ (A_0-E)^{-1}*\mathbf{X}_{in} \f$
 *       -  arJ >= -2 => Standard transformation \f$ (A_0-E)*\mathbf{X}_{in} \f$
 *    @param arNb
 *       Subtract this number from all elements of the Jacobian
 *    @param arLaplaceQuad
 *       Vector of LaplaceQuadrature%s
 *
 *    @return
 *       The transformed TensorDataCont
 **/
GeneralTensorDataCont<typename TransformerV3::param_t> TransformerV3::TensorTransformerA0
   (  const GeneralTensorDataCont<param_t>& arTdcIn
   ,  const In&             arJ
   ,  const param_t&             arNb
   ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad
   )
{
   // Declare needed pointers, etc.
   Vcc* const vcc = this->pVcc(); 
   const auto& mc_op_range = this->GetTransXvecModeCombiOpRange();   // Niels: If VCCJAC, mXvec is initalized out of space...
   const auto& nmodals = vcc->pVccCalcDef()->GetNModals();
   const auto& eigvals = vcc->GetEigVal();
   const auto& offsets = vcc->GetOccModalOffSet();

   bool incl_ref = arTdcIn.GetModeCombiData(I_0).Type() == BaseTensor<param_t>::typeID::SCALAR;

   // Check that the input matches the ModeCombiOpRange
   if (  !(    arTdcIn.Size() == mc_op_range.Size()-I_1
            || (incl_ref && arTdcIn.Size() == mc_op_range.Size())
          )
      )
   {
      MIDASERROR("Input TensorDataCont has wrong size compared to ModeCombiOpRange!");
   }

   // Check if the LaplaceQuadratures are empty
   bool have_quad_points = !arLaplaceQuad.empty();
   In num_points = I_4; // default

   if (  have_quad_points
      && arTdcIn.Size() != arLaplaceQuad.size()
      )
   {
      MIDASERROR("Wrong size of LaplaceQuadrature vector for TensorTransformerA0!");
   }
   
   // Copy input TensorDataCont to result
   auto result = arTdcIn;

   // Get smallest tensor order that is decomposed
   In min_decomp_order = C_IN_MAX;
   for(const auto& info : this->mDecompInfo)
   {
      min_decomp_order = std::min(min_decomp_order, info.at("DECOMPTOORDER").get<In>());
   }

   // Loop over mode combinations and transform result
   In count = I_0;
   In mc_size = I_0;
   for(const auto& mc : mc_op_range)
   {
      mc_size = mc.Size();
      const auto& mcvec = mc.MCVec();
      bool decomp =  (  mc_size >= min_decomp_order
                     && min_decomp_order >= I_0 
                     && result.GetModeCombiData(count).Type() != BaseTensor<param_t>::typeID::SIMPLE
                     );

      // Ignore reference!
      if (  mc_size == I_0
         )
      {
         // Increment count if ref is included in result
         if (  incl_ref
            )
         {
            ++count;
         }
         continue;
      }

      // If inverse transformation
      if(arJ < -I_2)
      {
         result.GetModeCombiData(count)   *= decomp
                                          ?  have_quad_points
                                             ?  NiceTensor<param_t>(new EnergyDenominatorTensor<param_t>(mcvec, nmodals, eigvals, offsets, arLaplaceQuad[count], arNb))
                                             :  NiceTensor<param_t>(new EnergyDenominatorTensor<param_t>(mcvec, nmodals, eigvals, offsets, num_points, arNb))
                                          :  NiceTensor<param_t>(new ExactEnergyDenominatorTensor<param_t>(mcvec, nmodals, eigvals, offsets, arNb));
      }
      // Else: 0th-order Jacobian diagonal
      else
      {
         result.GetModeCombiData(count) *= decomp ? NiceTensor<param_t>(new EnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, arNb))
                                                  : NiceTensor<param_t>(new ExactEnergyDifferencesTensor<param_t>(mcvec, nmodals, eigvals, offsets, arNb));
      }

      ++count;
   }

   return result;
}

} /* namespace midas::vcc */
