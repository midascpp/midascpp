/**
************************************************************************
* 
* @file                LanczosQuadRspFunc.cc
*
* Created:             25-08-2009
*
* Author:              Mikkel Bo Hansen  (mbh@chem.au.dk)
*
* Short Description:   Implementation of LanczosQuadRspFunc class.
* 
* Last modified: Tue Nov 10, 2009  04:02PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <map>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "vcc/LanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosQuadRspFunc.h"
#include "util/Timer.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"

using namespace std;

// sort according to the 1) the string, 2) the frq
bool Compare(pair<string,Nb> p1,pair<string,Nb> p2) {
   if(p1.first!=p2.first)
      return p1.first>p2.first;
   else
      return p1.second>p2.second;
}

LanczosQuadRspFunc::LanczosQuadRspFunc(const std::map<string,string>& aParams, 
                                       const bool aBand, const bool aNHerm):
   LanczosRspFunc(aParams,"LanczosQuadRsp",aBand,aNHerm),
   mSolver(LANCZOS_SOLVER_LIN), mOrthoChain(LANCZOS_ORTHO_NONE)
{
   Mout << " LanczosQuadRspFunc::LanczosQuadRspFunc()" << endl
        << "    Parameters:" << endl;

   for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
      Mout << "    " << it->first << " -> " << it->second << endl;

   InitOperNames(aParams);
   InitFrqPairs(aParams);
   InitGamma(aParams);
   InitChainLen(aParams);
   InitOrtho(aParams);
   InitSolver(aParams);
   mManyFrq=false;
   if(mFrqPairs.size()*6*2>mChainLen)
      mManyFrq=true;
}

void LanczosQuadRspFunc::ConstructionError(const string aMsg,
                                          const map<string,string>& aParams)
{
   Mout << " LanczosQuadRspFunc construction error:" << endl
        << aMsg << endl << endl
        << " Constructor parameters:" << endl;
   
   for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;

   MIDASERROR("");
}

void LanczosQuadRspFunc::InitOperNames(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("OPERS");
   if (it == aParams.end())
      ConstructionError(" No operators specified.", aParams);
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Operators must be enclosed in (...).", aParams);
   string opstr = s.substr(1,s.size()-2);
   vector<string> ops = SplitString(opstr, ",");
   if (ops.size() != 3)
      ConstructionError(" Exactly three operators must be specified.", aParams);
   mOper1 = ops[0];
   mOper2 = ops[1];
   mOper3 = ops[2];
}

void LanczosQuadRspFunc::InitFrqPairs(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("FRQ_PAIR");
   if (it != aParams.end())
   {
      // translate value to a vector of frq_pair
      vector<string> all_pairs=SplitString((string&)it->second," ");
      for(In i=0;i<all_pairs.size();i++)
      {
         string s=all_pairs[i];
         // each pair is enclosed in parentheses
         if (s[0] != '(' || s[s.size()-1] != ')')
            ConstructionError(" Operators must be enclosed in (...).", aParams);
         s=s.substr(1,s.size()-2);
         vector<string> pair=SplitString(s,",");
         if(pair.size()!=2)
            ConstructionError(" two frequencies must be specified.",aParams);
         s=pair[0];
         Nb frq1,frq2;
         {
            istringstream iss(s);
            iss >> frq1;
         }
         s=pair[1];
         {
            istringstream iss(s);
            iss >> frq2;
         }
         mFrqPairs.push_back(make_pair(frq1,frq2));
      }
      return;
   }
   it = aParams.find("FRQ_RANGE");
   if (it != aParams.end())
   {
      string s=it->second; 
      if (s[0] != '(' || s[s.size()-1] != ')')
         ConstructionError(" Frq_range must be enclosed in (...).", aParams);
      string frq_str = s.substr(1,s.size()-2);
      vector<string> frq_vec = SplitString(frq_str, ":");
      if(frq_vec.size()!=3)
         ConstructionError(" Frq range like: 0:5000:1...", aParams);
      In frq_start,frq_end,frq_step;
      frq_start=atoi(frq_vec[0].c_str());
      frq_end=atoi(frq_vec[1].c_str());
      frq_step=atoi(frq_vec[2].c_str());
      for(In i=frq_start;i<=frq_end;i+=frq_step) 
         mFrqPairs.push_back(make_pair(((Nb)i/C_AUTKAYS),((Nb)i/C_AUTKAYS)));
   }

   if (it == aParams.end())
      ConstructionError(" Only FRQ_PAIR is valid in the quadratic case.", aParams);
}

vector<LanczosChainDefBase*> LanczosQuadRspFunc::GetChains(string& aString) const
{
   vector<LanczosChainDefBase*> aDefs;
   aDefs.push_back(LanczosChainDefBase::Factory(mNHerm,mOper1,mChainLen,mOrthoChain));
   aDefs.back()->AddXoper(mOper1);
   aDefs.push_back(LanczosChainDefBase::Factory(mNHerm,mOper2,mChainLen,mOrthoChain));
   aDefs.back()->AddXoper(mOper2);
   aDefs.push_back(LanczosChainDefBase::Factory(mNHerm,mOper3,mChainLen,mOrthoChain));
   aDefs.back()->AddXoper(mOper3);
   
   aString = "lan";
   return aDefs;
}

void LanczosQuadRspFunc::InitGamma(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("GAMMA");
   if (it == aParams.end())
      ConstructionError(" GAMMA not specified", aParams);
   istringstream is(it->second);
   is >> mGamma;
   mGamma /= C_AUTKAYS;
}

void LanczosQuadRspFunc::InitChainLen(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("CHAINLEN");
   if (it == aParams.end())
      ConstructionError(" CHAINLEN not specified", aParams);
   istringstream is(it->second);
   is >> mChainLen;
}

void LanczosQuadRspFunc::InitOrtho(const map<string,string>& aParams)
{
   // Search for orthogonalization parameter.
   map<string,string>::const_iterator it = aParams.find("ORTHO");
   if(it == aParams.end())
   {
      Mout << " No ORTHO keyword set, so I'm using default values: " << endl
           << "      Ortho = \"FULL\"     [1]" << endl;

      mOrthoChain = 1;
      return;
   }
   if (it != aParams.end())
   {
      string s = "ORTHO_" + it ->second;
           
      if (ConstForString(s) == LANCZOS_ERROR)
         ConstructionError(" Unknown ORTHO parameter.", aParams);
   
      mOrthoChain = LanczosRspFunc::ConstForString(s);
   }
}

void LanczosQuadRspFunc::InitSolver(const map<string,string>& aParams)
{
   // Search for solver parameter.
   map<string,string>::const_iterator it = aParams.find("SOLVER");
   if(it == aParams.end())
   {
      Mout << " No SOLVER keyword set, so I'm using default values: " << endl
           << "      Solver = \"LINEAR\"     [16]" << endl;

      mSolver = 16;
      return;
   }
   
   if (it != aParams.end())
   {
      string s = "SOLVER_" + it->second;

      if (mSolver == LANCZOS_ERROR)
         ConstructionError(" Unknown SOLVER parameter.", aParams);
      if (mSolver == LANCZOS_SOLVER_EIG)
         ConstructionError(" Parameter EIG not possible for QR.", aParams);
      
      mSolver = LanczosRspFunc::ConstForString(s);
   }
}

void LanczosQuadRspFunc::Evaluate()
{

   // Get chains to use.
   Timer time_quad;
   map<string,LanczosChain> chains;
   vector<string> op_vec;
   op_vec.push_back(mOper1);
   op_vec.push_back(mOper2);
   op_vec.push_back(mOper3);
   for(In i=0;i<3;i++) {
      LanczosChainDef chaindef(op_vec[i], mChainLen, mOrthoChain);
      chains.insert(make_pair(op_vec[i],*FindChain(chaindef)));
   }

   switch (mSolver)
   {
      case LANCZOS_SOLVER_LIN:
         VciRspFctLin(chains);
         break;
      //case LANCZOS_SOLVER_EIG:
         //VciRspFctEig(chains);
         //break;
      default:
         MIDASERROR("LanczosQuadRspFunc::Evaluate(): Unknown mSolver.");
   }

   midas::stream::ScopedPrecision(15, Mout);
   Mout << endl << " Evaluated quadratic response function: " << endl << *this;
   time_quad.CpuOut(Mout,"   CPU  time in LQR  :  ");
   time_quad.WallOut(Mout,"   Wall time in LQR  :  ");

   WriteDataToFile();
   Mout << endl;
}

void LanczosQuadRspFunc::VciRspFctLin(map<string,LanczosChain>& aChains)
{

   // Vectors used for solving linear equations.
   MidasVector e(mChainLen, C_0);          // Unit vector.
   MidasVector yre(mChainLen);             // Solution vector for (T-wI)*y = e.
   MidasVector yim(mChainLen);             // Solution vector for (T-wI)*y = e.
   e[I_0] = C_1;

   mpTrf->SetType(TRANSFORMER::VCIRSP);
   // if many frequencies, make transformation on q vecs
   for(In k=I_0;k<3;k++) {
      string op1;
      if(k==0)
         op1=mOper1;
      else if(k==1)
         op1=mOper2;
      else
         op1=mOper3;
      for(In i=I_0;i<3;i++) {
         string op2;
         if(i==0)
            op2=mOper1;
         else if(i==1)
            op2=mOper2;
         else
            op2=mOper3;
         map<string,LanczosChain>::iterator il=aChains.find(op2);
         LanczosChain c=il->second;
         DataCont cont;
         cont.SetNewSize(I_0);
         cont.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1,c.GetFilePrefix()+"_q0_trans_"+op1);
         if(mManyFrq && !cont.ChangeStorageTo("InMem",true)) {
            // Need to change transformer operator
            string s_basis = mpVccCalcDef->Basis();
            In i_basis = I_0;
            for (In i = I_0; i < gBasis.size(); ++i) 
            {
               if (gBasis[i].GetmBasName() == s_basis)
               {
                  i_basis = i;
                  break;
               }
            }
            In i_oper=gOperatorDefs.GetOperatorNr(op1);
            if(i_oper == -1)
               MIDASERROR("Operator "+op1+" not found");
            // Update primitive integrals.
            string storage = "InMem";
            if (I_0 != mpVccCalcDef->IntStorage())
               storage = "OnDisc";
            OneModeInt one_mode_int(&gOperatorDefs[i_oper], &gBasis[i_basis], storage, true);
            one_mode_int.CalcInt();
            // ... Done
            //mpTrf->SetType(TRANSFORMER::VCIRSP);
            mpTrf->PutOperatorToWorkingOperatorAndUpdateModalIntegrals(mpVcc, &gOperatorDefs[i_oper], &one_mode_int);
            for(In j=0;j<mChainLen;j++) {
               MidasVector vec=c.GetSpecificQVec(j);
               TransVecWithFmat(op1,vec);
               DataCont trans_c(mpTrf->NexciXvec()-I_1,C_0,"OnDisc",c.GetFilePrefix()+"_q"+std::to_string(j)+"_trans_"+op1);
               trans_c.DataIo(IO_PUT,vec,mpTrf->NexciXvec()-I_1);
               trans_c.SaveUponDecon(true);
            }
            // Restore original operator
            mpTrf->RestoreOrigOperAndUpdateModalIntegrals(mpVcc);
         }
      }
   }

   // Calculate response function for specified frequency pairs
   In n_frq = mFrqPairs.size();
   for (In i=I_0; i<n_frq; i++)
   {
      // the frequencies
      Nb frq2=mFrqPairs[i].first;
      Nb frq3=mFrqPairs[i].second;
      Nb frq1=C_M_1*(frq2+frq3);
      // loop over permutations
      vector<pair<string,Nb> > opers(3);
      opers[0]=make_pair(mOper1,frq1);
      opers[1]=make_pair(mOper2,frq2);
      opers[2]=make_pair(mOper3,frq3);
      sort(opers.begin(),opers.end(),Compare);
      In real_permut=0;
      Nb result=C_0;
      do { 
         real_permut++;
         // extra loop for handling ex and deex
         for(In j=0;j<2;j++) {
            string op=opers[1].first;
            Nb frq;
            if(j==0)
               frq=opers[1].second;
            else
               frq=-opers[1].second;
            // calculate Q(O2)_y(O2) intermediate => chain for O2
            map<string,LanczosChain>::iterator il=aChains.find(op);
            LanczosChain c=il->second;
            // use this chain to solve TD eq. for +\omega_y
            MidasVector alpha(mChainLen, C_0);
            MidasVector beta(mChainLen-I_1, C_0);
            c.GetAlphaRange(alpha, I_0, mChainLen);
            c.GetBetaRange(beta, I_0, mChainLen-I_1);
            TriDiagLinEqSol(alpha, beta, beta, e, yre, yim, frq, mGamma);
            // transform with chain
            MidasVector intermed1=c.TransVecWithChain(yre);
            Nb etan1=c.GetEtaNorm();
            // transform with F^O1
            op=opers[0].first;
            if(mManyFrq) {
               intermed1=TransVecWithTransQVec(c.GetFilePrefix(),yre,op);
            }
            else
               TransVecWithFmat(op,intermed1);
            // calculate Q(O3)_y(O3) => last chain
            op=opers[2].first;
            if(j==0)
               frq=-opers[2].second;
            else
               frq=opers[2].second;
            il=aChains.find(op);
            c=il->second;
            alpha.Zero();
            beta.Zero();
            yre.Zero();
            yim.Zero();
            e.Zero();
            e[0]=C_1;
            c.GetAlphaRange(alpha, I_0, mChainLen);
            c.GetBetaRange(beta, I_0, mChainLen-I_1);
            TriDiagLinEqSol(alpha, beta, beta, e, yre, yim, frq, mGamma);
            MidasVector intermed2=c.TransVecWithChain(yre);
            Nb etan2=c.GetEtaNorm();
            // remember that the chains are based on normalized
            // eta vector!
            result+=etan1*etan2*Dot(intermed1,intermed2);
         }
      } while (next_permutation(opers.begin(),opers.end(),Compare));
      // If some operators are equal, not all permut are
      // computed => scale rsp func.
      if(gDebug)
         Mout << "I made " << real_permut << " of the possible 6 permutations." << endl;
      Nb fact=(Nb)(I_6/real_permut);
      result*=fact/C_2;
      mResults.push_back(result);
   }
}

void LanczosQuadRspFunc::TransVecWithFmat(string aOp,MidasVector& aV)
{
   // Need to change transformer operator
   string s_basis = mpVccCalcDef->Basis();
   In i_basis = I_0;
   for (In i = I_0; i < gBasis.size(); ++i)
   {
      if (gBasis[i].GetmBasName() == s_basis)
      {
         i_basis = i;
         break;
      }
   }
   In i_oper=gOperatorDefs.GetOperatorNr(aOp);
   if(i_oper == -1)
      MIDASERROR("Operator "+aOp+" not found");
   /*for(In i=0;i<gOperatorDefs.GetNrOfOpers();i++) {
      if(gDebug) {
         Mout << "Checking operator: " << gOperatorDefs[i].Name() << endl;
         Mout << "In search for    : " << aOp << endl;
      }
      if(gOperatorDefs[i].Name()==aOp) {
         i_oper=i;
         break;
      }
   }*/
   // Update primitive integrals.
   string storage = "InMem";
   if (I_0 != mpVccCalcDef->IntStorage())
      storage = "OnDisc";
   OneModeInt one_mode_int(&gOperatorDefs[i_oper], &gBasis[i_basis], storage, true);
   one_mode_int.CalcInt();
   // ... Done
   //mpTrf->SetType(TRANSFORMER::VCIRSP);
   mpTrf->PutOperatorToWorkingOperatorAndUpdateModalIntegrals(mpVcc, &gOperatorDefs[i_oper], &one_mode_int);
   // now make transform
   In size=mpTrf->NexciXvec()-I_1;
   DataCont tmp(aV);
   DataCont helper(size);
   Nb dummy=C_0;
   if(gDebug) {
      Mout << "Op in transform: " << mpTrf->GetOpDef()->Name() << endl;
      Mout << "Should be: " << aOp << endl;
   }
   mpTrf->VciRspTransH(tmp,helper,I_1,I_0,dummy);
   helper.DataIo(IO_GET,aV,size);
   // Restore original operator
   mpTrf->RestoreOrigOperAndUpdateModalIntegrals(mpVcc);
   return;
}

MidasVector LanczosQuadRspFunc::TransVecWithTransQVec(string aPrefix, MidasVector& aV, string aOp)
{
   In size=mpTrf->NexciXvec()-I_1;
   // Get chain
   //MidasMatrix trans_q_vec(size,mChainLen);
   MidasVector result(size,C_0);
   for (In i=I_0; i<mChainLen; i++)
   {
      DataCont q(I_0);
      string s=aPrefix+"_q"+std::to_string(i)+"_trans_"+aOp;
      q.GetFromExistingOnDisc(size,s);
      q.SaveUponDecon(true);
      MidasVector v(size);
      q.DataIo(IO_GET,v,size);
      // trans_q_vec.AssignCol(v,i);
      result=result+v*aV[i];
   }
   //return trans_q_vec*aV;
   return result;
}

void LanczosQuadRspFunc::WriteDataToFile()
{
   In n_frq = mFrqPairs.size();

   string filename = gAnalysisDir+"/"+mName + ".dat";
   if(gDebug)
      Mout << " Generating file: '" << filename << "'" << endl;
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << scientific << setprecision(15)
        << "# Quadratic response function <<" << mOper1 << "," 
        << mOper2 << "," << mOper3 << ">>" 
        << " generated using Lanczos chain." << endl
        << "# Chain length: " << mChainLen << endl
        << "# gamma (damping factor): " << mGamma*C_AUTKAYS << " cm-1" << endl
        << "# Columns: freq1. (cm-1)        freq2. (cm-1)        Real part (au)" << endl;


   for (In i=I_0; i<n_frq; ++i)
      file << mFrqPairs[i].first*C_AUTKAYS << "   "
           << mFrqPairs[i].second*C_AUTKAYS
           << "   " << mResults[i] << endl;

   file_stream.close();
}

ostream& LanczosQuadRspFunc::Print(ostream& aOut) const
{
   aOut << "   <<" << mOper1 << ";" << mOper2 << ";" << mOper3 << ">> (Lanczos)" << endl
        << "   Name (file prefix): '" << mName << "'" << endl;

   for(In i=I_0;i<mFrqPairs.size();i++)
      aOut << "   Frequency 1:        " << mFrqPairs[i].first << " au " << endl
           << "   Frequency 2:        " << mFrqPairs[i].second << " au " << endl
           << "   Result     :        " << mResults[i] << endl
           << "   Chain length:       " << mChainLen << endl
           << "   Orthogonalization:  " << LanczosRspFunc::StringForConst(mOrthoChain) << endl;

   return aOut;
}

vector<RspFunc> LanczosQuadRspFunc::ConvertToRspFunc() const
{
   vector<RspFunc> result;
   RspFunc helper(I_3);
   helper.SetOp(mOper1,I_0);
   helper.SetOp(mOper2,I_1);
   helper.SetOp(mOper3,I_2);
   for (In i=I_0; i<mFrqPairs.size(); ++i) {
      Nb frq=mFrqPairs[i].first;
      helper.SetFrq(frq,I_1);
      frq=mFrqPairs[i].second;
      helper.SetFrq(frq,I_2);
      helper.SetValue(mResults[i]);
      helper.SetHasBeenEval(true);
      result.push_back(helper);
   }
   return result;
}
