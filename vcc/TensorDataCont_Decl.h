/**
************************************************************************
* 
* @file                TensorDataCont_Decl.h
*
* Created:             19-10-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Holds excitation vector amplitudes in a tensor format
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORDATACONT_DECL_H_INCLUDED
#define TENSORDATACONT_DECL_H_INCLUDED

#include <vector>
#include <memory>
#include <iostream>
#include <set>
#include <list>

#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "tensor/BaseTensor.h"

#include "mmv/DataCont.h"
#include "tensor/TensorDecompInfo.h"

#include "util/type_traits/Complex.h"

#include "mpi/Impi.h"

// Forward declarations.
class VccStateSpace;
class VccCalcDef;
class ModeCombiOpRange;

namespace midas
{
namespace tensor
{
   class TensorDecompInfo;
} /* namespace midas::tensor */
}

template
   <  typename T
   >
class GeneralTensorDataCont;

template
   <  typename T
   >
std::ostream& operator<<(std::ostream&, const GeneralTensorDataCont<T>&);

/**
 * @brief New tensor data-vector format.
 **/
template
   <  typename T
   >
class GeneralTensorDataCont 
   :  private std::vector<NiceTensor<T> >
{
   public:
      using real_t = typename midas::type_traits::RealTypeT<T>;

   private:
      /******************************* private methods *************************************/
      //! Create and emplace back a SimpleTensor (used by ConstructXvecDataFromDataCont).
      void CreateSimple(const std::vector<unsigned>&, std::unique_ptr<T[]>&); ///< create a simple tensor

      //! Create and emplace back a random SimpleTensor
      void CreateRandomSimple(const std::vector<unsigned>&);

      //! construct mXvecData from input DataCont, an excitation range and a set of modals, with a bool designating whether to include reference.
      template
         <  typename U
         ,  typename INT
         >
      void ConstructXvecDataFromDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const std::vector<INT>&, bool); 

      //! construct GeneralTensorDataCont<T> of given shape filled with zero-tensors
      template
         <  typename INT
         >
      void ConstructZeroTensorDataCont(const ModeCombiOpRange&, const std::vector<INT>&, bool); 

      //! Decompose the TensorDataCont according to the decomposition data members
      template
         <  typename U = T
         >
      void DecomposeImpl
         (  const TensorDecompInfo::Set&
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Decompose the TensorDataCont according to the decomposition data members with argument thresholds
      template
         <  typename U = T
         >
      void DecomposeImpl
         (  const TensorDecompInfo::Set&
         ,  const std::vector<real_t>&
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Decompose with just one threshold
      template
         <  typename U = T
         >
      void DecomposeImpl
         (  const TensorDecompInfo::Set&
         ,  real_t
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Decompose the TensorDataCont using an argument as initial guess
      template
         <  typename U = T
         >
      void DecomposeImpl
         (  const TensorDecompInfo::Set&
         ,  const GeneralTensorDataCont<T>&
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Decompose the TensorDataCont using arguments as initial guess and thresholds
      template
         <  typename U = T
         >
      void DecomposeImpl
         (  const TensorDecompInfo::Set&
         ,  const GeneralTensorDataCont<T>&
         ,  const std::vector<real_t>&
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );
      

   public:
      /******************************* public methods *************************************/
      //! Constructor for default constructing TensorDataCont of given size 
      GeneralTensorDataCont(int = 0);

      //! Constructor from DataCont, ModeCombiOpeRange, and VccCalcDef, and a ref/noref bool.
      template
         <  typename U
         >
      GeneralTensorDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const VccCalcDef*, bool = false);
      
      //! Constructor from DataCont, ModeCombiOpeRange, and a set of modals, and a ref/noref bool.
      template
         <  typename U
         ,  typename INT
         >
      GeneralTensorDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const std::vector<INT>&, bool = false);

      //! Constructor from ModeCombiOpRange, set of modals, ref/noref bool. All NiceTensor elements are set to zero
      template<typename INT>
      GeneralTensorDataCont(const ModeCombiOpRange&, const std::vector<INT>&, bool = false);
      
      //! Constructor from VccStateSpace, typeID. All tensors zero.
      GeneralTensorDataCont(const VccStateSpace&, const typename BaseTensor<T>::typeID&);

      //! Constructor from VccStateSpace, index, typeID. One unit tensor (1 at index), rest 0.
      GeneralTensorDataCont(const VccStateSpace&, const Uin, const typename BaseTensor<T>::typeID&);

      //! Default copy constructor
      GeneralTensorDataCont(const GeneralTensorDataCont<T>&) = default;

      //! Default move constructor
      GeneralTensorDataCont(GeneralTensorDataCont&&) = default;
      
      //! Default copy assignment 
      GeneralTensorDataCont& operator=(const GeneralTensorDataCont&) = default;
      
      //! Default move assignment 
      GeneralTensorDataCont& operator=(GeneralTensorDataCont&&) = default;

      //! Access data for the i'th mode combination
      NiceTensor<T>& GetModeCombiData( const In );
      
      //! Access data for the i'th mode combination (const version)
      const NiceTensor<T>& GetModeCombiData( const In ) const;

      //! Get the size of the TensorDataCont, i.e. the number of stacked tensors
      In Size() const { return this->size(); }

      //! Get the total size of the TensorDataCont, i.e. the combined size of all NiceTensors
      In TotalSize() const;

      //! Zero the whole TensorDataCont
      void Zero();

      //! Convert all NiceTensors to ZeroTensor.
      void ToZeroTensors();

      //! Calculate complex conjugate
      GeneralTensorDataCont Conjugate
         (
         )  const;

      //! Get norm of TensorDataCont
      real_t Norm2() const;
      real_t Norm() const { return std::sqrt(this->Norm2()); };

      //! Get largest tensor norm in the TensorDataCont: \f$\max_m{||e^m||}\f$
      real_t MaxNorm() const;

      //! Scale the whole TensorDataCont with a number.
      template
         <  typename U
         >
      void Scale
         (  U
         );

      //! Normalize the whole TensorDataCont (such that Norm() is 1)
      void Normalize();

      //! Balance the norms of the mode vectors of the CanonicalTensor%s in the TensorDataCont
      void Balance();

      //! Set elements to their absolute value (only works with SimpleTensor)
      void Abs();

      //! Decompose (wrapper). Due to variadic template, this cannot easily be disabled like the DecomposeImpl methods.
      template
         <  typename... Ts
         >
      void Decompose
         (  Ts&&... ts
         )
      {
         static_assert(!midas::type_traits::IsComplexV<T>, "GeneralTensorDataCont::Decompose only available for real numbers.");

         // Call implementation (which distributes MPI processes)
         this->DecomposeImpl(std::forward<Ts>(ts)...);

         #ifdef VAR_MPI
         auto nproc = midas::mpi::GlobalSize();

         // Sync data (bcast from the rank that performed the decomposition)
         for(size_t i=0; i<this->Size(); ++i)
         {
            this->GetModeCombiData(i).MpiBcast(i % nproc);
         }
         #endif
      }
      
      //! Recompose the TensorDataCont to all SimpleTensors
      void Recompose();

      //! Perform in-place Axpy operation, i.e. Y += aA*aX, Y being this object.
      template
         <  typename U
         >
      void Axpy
         (  const GeneralTensorDataCont&
         ,  U
         ); 

      //! In-place orthogonalization of the calling TDC wrt. the argument TDC.
      void Orthogonalize
         (  const GeneralTensorDataCont& arU
         );


      /******************************** Operations ****************************************/
      //! In-place addition (element-wise)
      GeneralTensorDataCont& operator+=
         (  const GeneralTensorDataCont&
         );

      //! In-place subtraction (element-wise)
      GeneralTensorDataCont& operator-=
         (  const GeneralTensorDataCont&
         );

      //! In-place multiplication (element-wise)
      GeneralTensorDataCont& operator*=
         (  const GeneralTensorDataCont&
         );

      //! In-place division (element-wise, only works for SimpleTensor%s)
      GeneralTensorDataCont& operator/=
         (  const GeneralTensorDataCont&
         );

      //! In-place scalar addition to each element (only works with SimpleTensor instances)
      GeneralTensorDataCont& ElementwiseScalarAddition
         (  T
         );

      /********************************* Tensor statistics ********************************/
      //! Get the smallest rank of a decomposed tensor (per order)
      std::vector<In>   GetMinRankVec() const;
      In                GetMinRank()    const;

      //! Get the largest rank of a decomposed tensor (per order)
      std::vector<In>   GetMaxRankVec() const;
      In                GetMaxRank()    const;

      //! Get the average rank (per order)
      std::vector<real_t> AverageRankVec() const;
      real_t AverageRank() const;

      //! Get average difference between actual rank and theoretical max-rank (per order)
      std::vector<real_t> AverageRankDifferenceVec() const;
      real_t AverageRankDifference() const;

      //! Get all ranks sorted by exci level
      std::vector<std::vector<In>> GetRanksVec
         (
         )  const;

      //! Get all ranks in one big vector
      std::vector<In> GetRanks
         (
         )  const;


      //! Calculate data compression factor of CP tensors
      std::vector<real_t> DataCompressionFactors
         (
         )  const;

      //! Print order and extents of all tensors
      void PrintOrderAndExtents
         (  std::ostream&
         )  const;

      //! Check tensor type
      bool AllOfType
         (  const typename BaseTensor<T>::typeID&
         )  const;

      /******************************* Disc IO ********************************************/
      //! Write TensorDataCont to disc
      void WriteToDisc
         (  const std::string&
         ,  midas::mpi::OFileStream::StreamType aStreamType = midas::mpi::OFileStream::StreamType::MPI_MASTER
         )  const;

      //! Read from disc (assign *this to TensorDataCont read from file)
      bool ReadFromDisc
         (  const std::string&
         ,  bool = true
         );

      /******************************* Misc. **********************************************/
      //! Check if TotalSize is larger than the capacity of an In
      bool CheckOverflow
         (
         )  const;

      //! Check sanity of tensors
      bool SanityCheck
         (
         )  const;

      /******************************* friends (hopefully few!) ***************************/
      //! Output operator for TensorDataCont
      friend std::ostream& operator<< <>
         (  std::ostream&
         ,  const GeneralTensorDataCont<T>&
         ); 
      
      /******************************* static function ************************************/
      //! Add a reference to the TensorDataCont as the first elemnent
      static GeneralTensorDataCont<T> AddReference
         (  const GeneralTensorDataCont&
         ,  T aRef = 1.0
         );
};

//! Random TensorDataCont
template
   <  typename T
   ,  typename INT
   >
GeneralTensorDataCont<T> RandomTensorDataCont
   (  const ModeCombiOpRange&
   ,  const std::vector<INT>&
   ,  bool
   );

//! Returns <T^*|U>
template
   <  typename T
   >
T SumProdElems
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   );

//! Returns <T|U>.
template
   <  typename T
   >
T Dot
   (  const GeneralTensorDataCont<T>& arT
   ,  const GeneralTensorDataCont<T>& arU
   );

//! Returns acos(<T|U>/(||T||*||U||))
template
   <  typename T
   >
T Angle
   (  const GeneralTensorDataCont<T>&
   ,  const GeneralTensorDataCont<T>&
   );

//! Returns acos(<T|U>/(||T||*||U||)). Norms are passed as params.
template
   <  typename T
   >
T Angle
   (  const GeneralTensorDataCont<T>&
   ,  const GeneralTensorDataCont<T>&
   ,  typename GeneralTensorDataCont<T>::real_t
   ,  typename GeneralTensorDataCont<T>::real_t
   );

//! Returns Abs(T)
template
   <  typename T
   >
GeneralTensorDataCont<T> Abs
   (  const GeneralTensorDataCont<T>&
   );

//! arY += aA*arX
template
   <  typename T
   ,  typename U
   >
void Axpy
   (  GeneralTensorDataCont<T>& arY
   ,  const GeneralTensorDataCont<T>& arX
   ,  U aA
   );

//! Linear combination from list of GeneralTensorDataConts
template
   <  typename T
   ,  typename U
   ,  template<typename...> typename CONTAINER_T
   ,  template<typename...> typename CONTAINER_U
   >
GeneralTensorDataCont<T> LinComb
   (  const CONTAINER_T<GeneralTensorDataCont<T>>&
   ,  const CONTAINER_U<U>&
   );

//! Convert GeneralTensorDataCont to DataCont
template
   <  typename T
   >
GeneralDataCont<T> DataContFromTensorDataCont
   (  const GeneralTensorDataCont<T>& aIn
   );

//! Convert GeneralTensorDataCont to NiceTensor (assumes TensorDataCont is constructed from simple MCR with NModes and MaxExciLevel)
template
   <  typename T
   >
NiceTensor<T> NiceTensorFromTensorDataCont
   (  const GeneralTensorDataCont<T>& aIn
   ,  const std::set<In>& aExtRangeModes = {}
   );

// Global "wrapper" functions for some of the TensorDataCont member functions.
// Used (only?) in the 'iterative equation solver' framework.
template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t Norm(const GeneralTensorDataCont<T>& arT);                                // |T|

template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t Norm2(const GeneralTensorDataCont<T>& arT);                               // |T|^2

template
   <  typename T
   >
typename GeneralTensorDataCont<T>::real_t DiffNorm2(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&);

template
   <  typename T
   >
void Normalize(GeneralTensorDataCont<T>& arT);                                 // T = T/|T|

template
   <  typename T
   >
void Orthogonalize(GeneralTensorDataCont<T>& arT, const GeneralTensorDataCont<T>& arU);  // T -= U<T|U>/<U|U>

template
   <  typename T
   ,  typename U
   >
void Scale(GeneralTensorDataCont<T>& arT, U aNb);                       // T *= Nb

template
   <  typename T
   >
void Zero(GeneralTensorDataCont<T>& arT);                                      // T = 0

template
   <  typename T
   >
size_t Size(const GeneralTensorDataCont<T>& aT);

template
   <  typename T
   >
void SetShape
   (  GeneralTensorDataCont<T>&
   ,  const GeneralTensorDataCont<T>&
   );

//! Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   );

//! Mean Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeMeanNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   );

//! Max Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename GeneralTensorDataCont<PARAM_T>::real_t OdeMaxNorm2
   (  const GeneralTensorDataCont<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const GeneralTensorDataCont<PARAM_T>& aYOld
   ,  const GeneralTensorDataCont<PARAM_T>& aYNew
   );

//! Data to pointer
template
   <  typename T
   ,  typename U
   >
void DataToPointer
   (  const GeneralTensorDataCont<T>& aThis
   ,  U* apPtr
   );

//! Data to pointer
template
   <  typename T
   ,  typename U
   >
void DataFromPointer
   (  GeneralTensorDataCont<T>& arThis
   ,  const U* apPtr
   );
   
//! Assert same shape
template
   <  typename T
   >
void assert_same_shape
   (  const GeneralTensorDataCont<T>& aFirst
   ,  const GeneralTensorDataCont<T>& aSecond
   );


#endif /* TENSORDATACONT_DECL_H_INCLUDED */
