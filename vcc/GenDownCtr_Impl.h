/**
 ************************************************************************
 * 
 * @file                GenDownCtr_Impl.h
 *
 * Created:             15-12-2009
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
 *
 * Short Description:   Implementation of generalized down contraction.
 * 
 * Last modified:
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef GENDOWNCTR_IMPL_H_INCLUDED
#define GENDOWNCTR_IMPL_H_INCLUDED

#include <vector>

#include "inc_gen/Const.h"
#include "GenDownCtr_Decl.h"
#include "v3/VccEvalData.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "DirProd.h"

namespace midas::vcc
{

template
   <  typename T
   ,  template<typename...> typename DATA
   >
In GenDownCtr<T, DATA>::mNtot = I_0;
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In GenDownCtr<T, DATA>::mNgen = I_0;
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In GenDownCtr<T, DATA>::mNsimple = I_0;
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In GenDownCtr<T, DATA>::mN1D = I_0;

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::ClearStatistics()
{
   mNtot    = I_0;
   mNgen    = I_0;
   mNsimple = I_0;
   mN1D     = I_0;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::PrintStatistics()
{
   Mout << " GenDownCtr statistics:" << std::endl
        << "    # Total contractions:   " << mNtot << std::endl
        << "    # General contractions: " << mNgen << " (" << Nb(mNgen)/mNtot * 100 << " %)" << std::endl
        << "    # Simple contractions:  " << mNsimple
                                        << " (" << Nb(mNsimple)/mNtot * 100 << " %)" << std::endl
        << "    # 1D contractions:      " << mN1D << " (" << Nb(mN1D)/mNtot * 100 << " %)" << std::endl;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
GenDownCtr<T, DATA>::GenDownCtr
   (
   )
   :  mAlgo
         (  GEN_DOWN_CTR_UNKNOWN
         )
   ,  m1DNbef(I_0)
   ,  m1DNafter(I_0)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
GenDownCtr<T, DATA>::GenDownCtr
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aInMc
   ,  const ModeCombi& aDownMc
   )
   :  mAlgo
         (  GEN_DOWN_CTR_UNKNOWN
         )
   ,  m1DNbef(I_0)
   ,  m1DNafter(I_0)
{
   this->Init(aEvalData, aInMc, aDownMc);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::Init
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aInMc
   ,  const ModeCombi& aDownMc
   )
{
   if (  aInMc == aDownMc
      )
   {
      mAlgo = GEN_DOWN_CTR_SIMPLE;
   }
   else if  (  aDownMc.Size() == I_1
            )
   {
      mAlgo = GEN_DOWN_CTR_1D;
      this->Init1D(aEvalData, aInMc, aDownMc);
   }
   else
   {
      mAlgo = GEN_DOWN_CTR_GEN;
      this->InitGeneral(aEvalData, aInMc, aDownMc);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::Contract
   (  const GeneralMidasVector<T>& aInVec
   ,  const GeneralMidasVector<T>& aDownVec
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   mNtot++;
   switch (mAlgo)
   {
      case GEN_DOWN_CTR_GEN:
         mNgen++;
         CtrGeneral(aInVec, aDownVec, aRes);
         break;
      case GEN_DOWN_CTR_SIMPLE:
         mNsimple++;
         CtrSimple(aInVec, aDownVec, aRes);
         break;
      case GEN_DOWN_CTR_1D:
         mN1D++;
         Ctr1D(aInVec, aDownVec, aRes);
         break;
      default:
         MIDASERROR("GenDownCtr<T, DATA>::Contract(): Unknown algorithm.");
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::InitGeneral
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aInMc
   ,  const ModeCombi& aDownMc
   )
{
   mGenInMc = aInMc;
   
   // Setup vector with number of virtual modals. Used for general down contraction.
   mGenNmodals.resize(aInMc.Size());
   for (In i=I_0; i<aInMc.Size(); ++i)
      mGenNmodals[i] = aEvalData.NModals(aInMc.Mode(i)) - I_1;

   ModeCombi res_mc;
   res_mc.InFirstOnly(aInMc, aDownMc);
   
   // Setup vectors connecting indices in imp_mc and down_res_mc to l_mc.
   // Used for generalized down contraction.
   mGenDownIdx.resize(aDownMc.Size());
   for (In i=I_0; i<aDownMc.Size(); ++i)
      for (In k=I_0; k<aInMc.Size(); ++k)
         if (aDownMc.Mode(i) == aInMc.Mode(k))
            mGenDownIdx[i] = k;
   mGenResIdx.resize(res_mc.Size());
   for (In i=I_0; i<res_mc.Size(); ++i)
      for (In k=I_0; k<aInMc.Size(); ++k)
         if (res_mc.Mode(i) == aInMc.Mode(k))
            mGenResIdx[i] = k;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::CtrGeneral
   (  const GeneralMidasVector<T>& aInVec
   ,  const GeneralMidasVector<T>& aDownVec
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   // Loop over result.
   std::vector<In> ivec(mGenInMc.Size());
   In in_addr = I_0;
   while (true)
   {
      // Get address in down vector.
      In down_addr = I_0;
      for (In i=I_0; i<mGenDownIdx.size(); ++i)
         down_addr = down_addr*mGenNmodals[mGenDownIdx[i]] + ivec[mGenDownIdx[i]];
     
      // Get address in result vector.
      In res_addr = I_0;
      for (In i=I_0; i<mGenResIdx.size(); ++i)
         res_addr = res_addr*mGenNmodals[mGenResIdx[i]] + ivec[mGenResIdx[i]];
    
      aRes[res_addr] += aInVec[in_addr] * aDownVec[down_addr];
      
      if (++in_addr < aInVec.Size())
         dirprod::IncIvec(ivec, mGenNmodals);
      else
         break;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::CtrSimple
   (  const GeneralMidasVector<T>& aInVec
   ,  const GeneralMidasVector<T>& aDownVec
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   for (In i=I_0; i<aInVec.Size(); ++i)
      aRes[I_0] += aInVec[i] * aDownVec[i];
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::Init1D
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aInMc
   ,  const ModeCombi& aDownMc
   )
{
   In down_mode = aDownMc.Mode(I_0);

   In i=I_0;
   m1DNbef = I_1;
   for (i=I_0; i<aInMc.Size(); ++i)
   {
      In mode = aInMc.Mode(i);
      if (mode == down_mode)
         break;
      m1DNbef *= aEvalData.NModals(mode) - I_1;
   }

   m1DNafter = I_1;
   for (++i; i<aInMc.Size(); ++i)
      m1DNafter *= aEvalData.NModals(aInMc.Mode(i)) - I_1; 
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void GenDownCtr<T, DATA>::Ctr1D
   (  const GeneralMidasVector<T>& aInVec
   ,  const GeneralMidasVector<T>& aDownVec
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   In n_down = aDownVec.Size();

   In offset1 = I_0;           // "Outer" offset into input vector.
   In offset2 = I_0;           // "Inner" offset into input vector.
   In i_in;                    // Index into input vector.
   In res_offset = I_0;        // Offset into result vector.
   In i_res = I_0;             // Index into result vector.
   for (In i_bef=I_0; i_bef<m1DNbef; ++i_bef)
   {
      offset2 = offset1;
      for (In i_down=I_0; i_down<n_down; ++i_down)
      {
         i_in = offset2;
         i_res = res_offset;
         for (In i_after=I_0; i_after<m1DNafter; ++i_after)
         {
            aRes[i_res++] += aInVec[i_in++] * aDownVec[i_down];
         }
         offset2 += m1DNafter;
      }
      offset1 += (m1DNafter * n_down);
      res_offset += m1DNafter;
   }
}

} /* namespace midas::vcc */

#endif /* GENDOWNCTR_IMPL_H_INCLUDED */
