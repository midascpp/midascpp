/**
************************************************************************
* 
* @file                LanczosLinRspFunc.h
*
* Created:             25-08-2009
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Lanczos quadratic response function class.
* 
* Last modified: Tue Nov 10, 2009  03:47PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSQUADRSPFUNC_H
#define LANCZOSQUADRSPFUNC_H 

#include <map>
using std::map;
#include <string>
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "input/RspFunc.h"

class LanczosQuadRspFunc: public LanczosRspFunc
{
   protected:
      string mOper1;                   ///< First operator.
      string mOper2;                   ///< Second operator.
      string mOper3;                   ///< Second operator.
 
      vector<pair<Nb,Nb> > mFrqPairs;  ///< Start of frequency interval.
      Nb mGamma;                       ///< Gamma value (half lifetime).
      In mSolver;                      ///< Method for obtaining the reposne function.
      In mChainLen;                    ///< Chain length to be used.
      In mOrthoChain;                  ///< Orthogonalize q vector in Lanczos chain.
      bool mManyFrq;
     
      vector<Nb> mResults;             ///< results

      // Functions for initializing members from aParams. 
      void InitOperNames(const map<string,string>& aParams);
      void InitFrqPairs(const map<string,string>& aParams);
      void InitGamma(const map<string,string>& aParams);
      void InitChainLen(const map<string,string>& aParams);
      void InitOrtho(const map<string,string>& aParams);
      void InitSolver(const map<string,string>& aParams);
      
      void ConstructionError(const string aMsg, const map<string,string>& aParams);
      ///< Used  for printing error messages during construction.

      void TransVecWithFmat(string,MidasVector&);

      MidasVector TransVecWithTransQVec(string,MidasVector&,string);
      
      void VciRspFctLin(map<string,LanczosChain>& aChain);
      //void VciRspFctEig(const LanczosChain& aChain);
      void WriteDataToFile();
         
   public:
      LanczosQuadRspFunc(const map<string,string>& aParams, const bool aBand, const bool aNHerm);

      vector<LanczosChainDefBase*> GetChains(string& aString) const;
      void Evaluate();

      const string& GetOper1() {return mOper1;}
      const string& GetOper2() {return mOper2;}
      const string& GetOper3() {return mOper2;}
      
      vector<pair<Nb,Nb> > GetFrqPairs() {return mFrqPairs;}
     
      //void AddReVals(MidasVector& aRe) {aRe += mReVals;}
      
      ostream& Print(ostream& aOut) const;

      vector<RspFunc> ConvertToRspFunc() const;
};

#endif // LANCZOSQUADRSPFUNC_H
