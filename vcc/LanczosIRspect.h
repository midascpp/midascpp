/**
************************************************************************
* 
* @file                LanczosIRspect.h
*
* Created:             14-08-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Lanczos IR spectrum class definition.
* 
* Last modified: Mon Jan 18, 2010  03:37PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSIRSPEC_H
#define LANCZOSIRSPEC_H

#include <string>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

class VccCalcDef;
class Vcc;

class LanczosIRspect
{
   protected:
      string     mName;            ///< Name of this spectrum. Used as file prefix.
      string     mXdip;            ///< Name of <<xdip;xdip>> response function.
      string     mYdip;            ///< Name of <<ydip;ydip>> response function.
      string     mZdip;            ///< Name of <<zdip;zdip>> response function.
      bool       mAnalyze;         ///< Do analysis?
      vector<Nb> mAnalysisBlocks;  ///< Intervals to be analyzed.
      
      vector<Nb> mPeaks;        // Stores frequencies of recognized peaks.
      vector<Nb> mPeaks_inty;
      
      bool       mPrintIRTable; ///< Should a latex table be made?
      Nb         mWeightLimit;  ///< For storing the limit for weights entering the latex table
      Nb         mPeakLimit;    ///< For storing the limit for peaks entering the latex table
   
      void IdentifyPeaks(const MidasVector& aInty,
                         Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);
      ///< Identify peaks by investigating the curvature of the spectrum.
      ///< EXPERIMENTAL!

      void AnalyzeSpect(VccCalcDef* aVccCalcDef, Vcc* aVcc,
                        const MidasVector& aInty,
                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);
      ///< Analyze which configurations contribute to the spectrum.
     
      void InitAnalysisBlocksFromMinima(const MidasVector& aInty,
                                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);
      
      void GenerateGnuPlot() const;
      ///< Generate Gnuplot input file and run gnuplot.
      
   public:
      LanczosIRspect(const string& aName,
                              const string& aXdip, const string& aYdip, const string& aZdip,
                              const string& aAnalyze, const string& aAnalysisBlocks, const bool aPrintIR,
                              const Nb aWeightLim, const Nb aPeakLim);

      void Create(VccCalcDef* aVccCalcDef, Vcc* aVcc);
      ///< Create the spectrum.
      
      friend std::ostream& operator<<(std::ostream& aOut, const LanczosIRspect& aSpec);
};

#endif //LANCZOSIRSPEC_H
