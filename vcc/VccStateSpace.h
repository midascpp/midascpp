/**
************************************************************************
* 
* @file                 VccStateSpace.h
*
* Created:              26-04-2016
*
* Author:               Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:    Defines the structure of the coefficient/amplitude
*                       vectors in a VCC (Vibrational Correlation Calculation).
* 
* Last modified:        26-04-2016 (Mads Boettger Hansen)
* 
* Detailed Description: Holds a ModeCombiOpRange which defines the range of
*                       mode combinations considered for the vibrational
*                       correlation calculation (VCC), as well as a vector
*                       containing the number of modals for each mode.
*                       Together these two objects define the "structure" of
*                       the coefficient/amplitude vectors in the space of
*                       vibrational states, i.e. they define the shape of all
*                       multi-dimensional arrays/tensors considered.  Initially
*                       intended for use with TensorDataCont and related
*                       classes.
*                       Since the state space structure is fixed during any
*                       given calculation, the VccStateSpace class is designed
*                       not to be altered after construction, and all relevant
*                       data should be set when constructing the object.
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCCSTATESPACE_H_INCLUDED
#define VCCSTATESPACE_H_INCLUDED

#include <iosfwd>
#include <memory>
#include <vector>
#include "input/ModeCombiOpRange.h"
#include "util/Error.h"

// Forward declarations.
class VccStateModeCombi;   // Needed by VccStateSpace::const_iterator.

class VccStateSpace 
{
   private:
      // -----------------------------------------------------------------------
      //    Private data members
      // -----------------------------------------------------------------------
      // The MCR and the dimension of each mode defines the space.
      // Members are constant to ensure that sanity check is always valid.
      // mDims holds the number of modals per mode minus 1 for the reference.
      // The type of mNModals matches that of the corresponding object in the
      // VscfCalcDef (usually used when constructing a VccStateSpace object),
      // while the type of mDims matches that of the corresponding object in
      // e.g. BaseTensor.
      const ModeCombiOpRange  mMCR;

      // IMPORTANT: Let this be the first member after the ModeCombiOpRange has
      // been trimmed!
      // (Then sanity check is performed as the first thing when constructing
      // the class; member initialization is performed in order of declaration
      // in class definition.)
      // Sanity status for internal use. Starts out false, but the sanity check
      // (hopefully) sets it to true, and makes an error otherwise.
      // After construction the VccStateSpace object is ASSUMED to be sane, so
      // no further range and/or sanity checking.
      const bool mSanity;

      // Dimensions.
      const std::vector<Uin>  mDims;

      // The addresses are the accumulated sizes of tensors for the given
      // dimensions and mode combinations.
      const std::vector<Uin>  mAddresses;

      // -----------------------------------------------------------------------
      //    Private member functions
      // -----------------------------------------------------------------------
      // Check validity of the argument MCR and vector of #modals per mode.
      template
         <  typename INT
         ,  std::enable_if_t<std::is_integral_v<INT> >* = nullptr
         >
      bool SanityCheck
         (  const ModeCombiOpRange&
         ,  const std::vector<INT>&
         )  const;

      // Functions for setting the data members from the constructor arguments.
      // No out-or-range checks, etc., since they assume the arguments to have
      // been validated by SanityCheck/mSanity already.
      template
         <  typename INT
         ,  std::enable_if_t<std::is_integral_v<INT> >* = nullptr
         >
      std::vector<Uin> DimsFromNModals
         (  const std::vector<INT>&
         )  const;

      std::vector<Uin> AddressesFromDims(const ModeCombiOpRange&, const std::vector<Uin>&) const;
      ModeCombiOpRange EraseRefModeCombi(ModeCombiOpRange aMCR, bool aEraseRef) const;

   public:
      // -----------------------------------------------------------------------
      //    Constructors, etc.
      // -----------------------------------------------------------------------
      // Delete some constructors that we do not want to use, due to the
      // rigidness of this class. Use a const-ref to the object in stead.
      VccStateSpace() = delete;
      VccStateSpace(const VccStateSpace&) = delete;
      VccStateSpace(VccStateSpace&&) = delete;
      VccStateSpace& operator=(const VccStateSpace&) = delete;
      VccStateSpace& operator=(VccStateSpace&&) = delete;
      ~VccStateSpace();

      // Constructor from MCR and N_modals, e.g. from VscfCalcDef.
      template
         <  typename INT
         ,  std::enable_if_t<std::is_integral_v<INT> >* = nullptr
         >
      VccStateSpace
         (  const ModeCombiOpRange& arMCR
         ,  const std::vector<INT>& arNModals
         ,  bool aEraseRef = false
         )
         :  mMCR(EraseRefModeCombi(arMCR, aEraseRef))
         ,  mSanity(SanityCheck(arMCR, arNModals))
         ,  mDims(DimsFromNModals(arNModals))
         ,  mAddresses(AddressesFromDims(mMCR, mDims))
      {
      }

      // -----------------------------------------------------------------------
      //    Member access
      // -----------------------------------------------------------------------
      // Const-refs to the essential members of this class, but prefer using
      // the iterator instead of the ModeCombiOpRange reference!
      const ModeCombiOpRange& MCR() const;
      const std::vector<Uin>& Dims() const;
      const std::vector<Uin>& Addresses() const;

      // Other information.
      // Numbers of MCs, empty/in-total.
      // The reference number of the MC containing a given address.
      Uin NumEmptyMCs() const;
      Uin NumMCs() const;
      Uin LocateAddress(Uin aAddress) const;

      // -----------------------------------------------------------------------
      //    Iterator
      // -----------------------------------------------------------------------
      // Users of this class should not modify it, so only const_iterator
      // provided.
      // The iterator holds a VccStateModeCombi, therefore consult that class
      // for the public interface available upon dereferencing the iterator.
      class const_iterator
      {
         public:
            // -----------------------------------------------------------------
            //    Type aliases
            // -----------------------------------------------------------------
            // Type aliases for compliance with general iterator interface.
            // (Equivalent to inheriting from std::iterator<...> with proper
            // template arguments.)
            using value_type        = VccStateModeCombi;
            using difference_type   = ptrdiff_t;
            using pointer           = const value_type*;
            using reference         = const value_type&;
            using iterator_category = std::bidirectional_iterator_tag;
            // The iterator_category can actually be "upgraded" to
            // std::random_access_iterator_tag if taking the effort to
            // implement the required functionalities for that.
            // The pointer and reference types are const because the user
            // should not be able to modify the VccStateModeCombi. The iterator
            // is allowed to, however, e.g. in order to in/decrement.

         private:
            // -----------------------------------------------------------------
            //    Private data members
            // -----------------------------------------------------------------
            std::unique_ptr<value_type> mpVSMC;

         public:
            // -----------------------------------------------------------------
            //    Constructors, etc.
            // -----------------------------------------------------------------

            // The regular constructors.
            const_iterator() = delete;
            const_iterator(const const_iterator&);
            const_iterator(const_iterator&&) = default;
            const_iterator& operator=(const const_iterator&) = delete;
            const_iterator& operator=(const_iterator&&) = default;
            ~const_iterator() = default;

            // Constructor from a VccStateModeCombi.
            const_iterator(VccStateModeCombi&&);

            // -----------------------------------------------------------------
            //    Iterator logic
            // -----------------------------------------------------------------
            // Comparison operators.
            bool operator==(const const_iterator& arIter) const;
            bool operator!=(const const_iterator& arIter) const;

            // Increment/decrement operators.
            const_iterator& operator++();
            const_iterator  operator++(int);
            const_iterator& operator--();
            const_iterator  operator--(int);

            // Dereference operators.
            reference operator*()  const;
            pointer   operator->() const;
      };

      // -----------------------------------------------------------------------
      //    Iterator related functions
      // -----------------------------------------------------------------------
      const_iterator begin() const;
      const_iterator end()   const;
};

// -----------------------------------------------------------------------------
//    Non-member functions related to class
// -----------------------------------------------------------------------------
// Outputting.
std::ostream& operator<<(std::ostream&, const VccStateSpace&);

/**
 *
 **/
template
   <  typename INT
   ,  std::enable_if_t<std::is_integral_v<INT> >*
   >
std::vector<Uin> VccStateSpace::DimsFromNModals
   (  const std::vector<INT>& arNModals
   )  const
{
   // Check sanity. 
   // (Should actually never be invoked due to MIDASERROR in SanityCheck.)
   MidasAssert(mSanity, "VccStateSpace has not passed sanity check.");

   // Make vec. of size 0, then reserve according to input.
   std::vector<Uin> vec(I_0);
   vec.reserve(arNModals.size());

   // Copy data, subtract 1 (for the reference), convert to unsigned int.
   for(const auto& n: arNModals)
   {
      vec.push_back(static_cast<Uin>(n - I_1));
   }

   return vec;
}

// Check validity of the argument MCR and vector of #modals per mode.
template
   <  typename INT
   ,  std::enable_if_t<std::is_integral_v<INT> >*
   >
bool VccStateSpace::SanityCheck
   (  const ModeCombiOpRange& arMCR
   ,  const std::vector<INT>& arNModals
   )  const
{
   // Assume true and correct if found not to be.
   // Give MidasWarnings along the way, and then put one MIDASERROR at the end,
   // if sanity has been set to false.
   bool sane = true;

   // Check validity of the number of modals per mode, i.e. all >= 1.
   for(const auto& n: arNModals)
   {
      if(n < I_1)
      {
         MidasWarning("A #modals-per-mode is < 1 (value: "+std::to_string(n)+").");
         sane = false;
      }
      else if(n == I_1)
      {
         MidasWarning("A #modals-per-mode is == 1, giving a dimension of 0.");
         // Give a warning, but still technically valid.
      }
   }

   // Check that the mode numbers in the MCR does not go out of range wrt. to
   // the size of the n_modals vector.
   // Could use the (private) mNmodes in ModeCombiOpRange, but not sure it's
   // reliable. Assumes that an MC is always ordered, so that front() gives the
   // smallest mode number and back() gives the largest.
   for(const auto& mc: arMCR)
   {
      if(!mc.Empty())
      {
         const auto& f = mc.MCVec().front();
         const auto& b = mc.MCVec().back();
         const auto& s = arNModals.size();
         if(f < I_0)
         {
            MidasWarning("Out-of-range mode: "+std::to_string(f)+" < 0.");
            sane = false;
         }
         if(b >= s)
         {
            MidasWarning("Out-of-range mode: "+std::to_string(b)+" >= #dims = "+std::to_string(s)+".");
            sane = false;
         }
      }
   }

   // Is it sane?
   if(!sane)
   {
      MIDASERROR("Invalid MCR and vector of N_modals passed to constructor. Check warnings.");
   }
   return sane;
}

#endif /* VCCSTATESPACE_H_INCLUDED */
