/**
************************************************************************
* 
* @file                VccRsp.cc
*
* Created:             17-10-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Vcc Response aspects 
* 
* Last modified: Mon May 17, 2010  02:14PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>
#include<algorithm>
#include<set>
#include<memory>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/RspCalcDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpInfo.h"
#include "util/MidasStream.h"
#include "util/Io.h"
#include "util/MultiIndex.h"
#include "util/MakeTables.h"
#include "util/Timer.h"
#include "Vcc.h"
#include "vcc/Xvec.h"
#include "vcc/ExpHvci.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "vcc/VccTransformer.h"
#include "vcc/TransformerV3.h"
#include "vcc/LanczosChain.h"
#include "vcc/NHermLanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosLinRspFunc.h"
#include "vcc/LanczosIRspect.h"
#include "vcc/BandLanczosChain.h"
#include "vcc/NHBandLanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/BandLanczosLinRspFunc.h"
#include "vcc/BandLanczosIRspect.h"
#include "vcc/BandLanczosRamanSpect.h"
#include "vcc/v3/IntermediateMachine.h"
#include "input/GlobalOperatorDefinitions.h"
#include "analysis/ResponseAnalysisInterface.h"
#include "mpi/FileStream.h"

/**
* Set the response functions to be non-variational 
* */
void Vcc::MakeRspFuncsNonVar()
{
   mpVccCalcDef->SetVariational(false);
}
/**
* The number of VCI/VCC response parameters. 
* */
void Vcc::PrepareNrspPar()
{
   // peter: nvecsize used to be mXvec.Size() and not mTransXvec.Size().
   In nvecsize = mRspTrf->NexciTransXvec();
   if (nvecsize>0) nvecsize--;
   SetNrspParHalf(nvecsize);
   // Both VCI and VCC get away with half dimension. nvecsize*=2;  
   SetNrspPar(nvecsize);
   //mNrspParHalf = nvecsize - I_1;
   Mout << " Nr of response parameters =      " << NrspPar() << endl;
   Mout << " Nr of exci space parameters =    " << NrspParHalf() << endl;
}

/**
* Calculate the M matrix for a Franck-Condon calculation
* */
void Vcc::FcCalc()
{
   Mout << endl << endl << endl
        << " Calculation of transition matrices." << endl;
   
   // Open output file.
   string filnam = gAnalysisDir+"/MIDAS_FC_FILE";

   midas::mpi::OFileStream midas_fc_file(filnam);

   midas::stream::ScopedPrecision(25, midas_fc_file);
   midas_fc_file.setf(ios::scientific);
   midas_fc_file.setf(ios::showpoint);
   midas_fc_file.setf(ios::uppercase);

   In nroots = mpVccCalcDef->VciRoots();
   In n_opers = mpVccCalcDef->NrFcVciCalcOps();

   // Starting outputting to FC file 
   midas_fc_file << "# Vci Total energies:" << endl;
   for (In i=I_0;i<nroots;i++)
      midas_fc_file << " " << i << "  " << setw(30) << mpVccCalcDef->GetVciEigVal(i) << endl; 

   // Starting outputting to temperatur averaging files 
   bool temp_fc_analysis=true;
   string base=gAnalysisDir+"/Vci_gs_fc_";
   string name=base+"Info_Energy";

   midas::mpi::OFileStream energies(name);
   
   name=base+"Info_Prop";
   
   midas::mpi::OFileStream prop_info(name);
   
   prop_info << nroots  << endl;
   prop_info << n_opers << endl;
   energies.setf(ios::scientific);
   energies.setf(ios::showpoint);
   energies.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, energies);
   energies << " VCI energies relative to ";
   if (nroots>I_0) 
   {
      Nb e_0 = mpVccCalcDef->GetVciEigVal(I_0); 
      energies << right << e_0 << endl; 
      for (In i=I_0;i<nroots;i++)
         energies << " " << right << (mpVccCalcDef->GetVciEigVal(i)-e_0) <<endl; 
   }

   for (In i_op=I_0;i_op<n_opers;i_op++)
   {
      string oper_name = mpVccCalcDef->FcVciCalcOp(i_op);
      //Mout << " Operator name is: " << oper_name << endl;
      In i_oper=gOperatorDefs.GetOperatorNr(oper_name);
      if (i_oper==-I_1)
      {
         Mout << " The operator with label " << oper_name << " is not found." << endl;
         Mout << " The available operators are:" << endl;
         for (In i=0;i<gOperatorDefs.GetNrOfOpers();i++) Mout << " " << gOperatorDefs[i].Name() << endl;
         MIDASERROR(" Operator could not be found " );
      }
      string basis_name = mpVccCalcDef->Basis();
      In i_basis = I_0;
      for (In i = I_0; i < gBasis.size(); i++)
      {
         if (gBasis[i].GetmBasName() == basis_name)
         {
            i_basis = i;
         }
      }
   
      // Prepare operator information. 
      //
      // Calculate integrals.
      bool save = true;
      string storage = "InMem";
      if (mpVccCalcDef->IntStorage()!=I_0) storage = "OnDisc";
      OneModeInt one_mode_int(&gOperatorDefs[i_oper],&gBasis[i_basis],storage,save);
      one_mode_int.CalcInt();
  
      // Setup transformer to be used.
      VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
      In nvecsize = trf.NexciTransXvec();
      
      trf.PutOperatorToWorkingOperatorAndUpdateModalIntegrals(this, &gOperatorDefs[i_oper], &one_mode_int);
   
      midas_fc_file << "# The <phi^e_i|" << oper_name << "| phi^e_j>  matrix for i>=j =0"
                    << "-" << nroots-I_1 << endl;
      Nb sum_norm_square=C_0;

      MidasVector prop_val(nroots);
      for (In i=I_0;i<nroots;i++)
      {
         // Vector to be transformed
         DataCont vec;
         string vecname = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i);
         vec.GetFromExistingOnDisc(nvecsize,vecname);
         vec.SaveUponDecon();
   
         // The Vector direct VCI transformed with operator
         DataCont trans_vec;
         string trans_vec_name = mpVccCalcDef->GetName() + "_Vci_vec_"
                        + std::to_string(i) + "_" + oper_name + "_trans";
         trans_vec.SetNewSize(nvecsize);
         trans_vec.NewLabel(trans_vec_name);
         trans_vec = vec;
         Nb a_nb=C_0;
         trf.TransH(vec,trans_vec,I_1,I_0,a_nb);
   
         // Dot basis vectors with the transformed vector. 
         for (In j=I_0;j<=i;j++)
         {
            Nb m_ji = C_0;
            if (j!=i)
            {
               DataCont vec2;
               string vecname = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(j);
               vec2.GetFromExistingOnDisc(nvecsize,vecname);
               vec2.SaveUponDecon();
               m_ji = Dot(trans_vec,vec2);
            }
            else
            {
               m_ji = Dot(trans_vec,vec);
               prop_val[i]=m_ji;
            }
            // Vci Vector 
            // The matrix element
            midas_fc_file << setw(5) << i << setw(5) << j;
            midas_fc_file << setw(35) << m_ji << endl;
            //Mout << setw(5) << i << setw(5) << j;
            //Mout << setw(35) << m_ji << endl;
            sum_norm_square += m_ji*m_ji;
         }
      } 
      Mout << endl << " This operator FC elements norm square = " << sum_norm_square << endl;
      trf.RestoreOrigOperAndUpdateModalIntegrals(this); // Does this matter? Don't think were ever gonna use trf again.

      // Save info on files for temperature averaging 
      if (temp_fc_analysis)
      {
         string name=base+"Info_Property_"+std::to_string(i_op);
         midas::mpi::OFileStream prop_file(name);

         prop_file.setf(ios::scientific);
         prop_file.setf(ios::showpoint);
         prop_file.setf(ios::uppercase);
         midas::stream::ScopedPrecision(22, prop_file);
         prop_file << "# " << oper_name  << " VCI averages  " << endl;

         for (In i_roots=I_0;i_roots<nroots;i_roots++)
         {
            prop_file << " " << right << prop_val[i_roots] << endl;
         }
      }
   }
   Mout << " Transition Matrix calculations are done!" << endl;
   Mout << " Matrices are stored on file '" << filnam << "'" << endl;
}

/**
* Calculate the Rhs transform 
* */
void Vcc::CalculateRhs
   (  DataCont& arRhs
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   if (gDebug) Mout << "\n\n Calculate correlated RHS for: ";
   if (mCorrMethod==VCC_METHOD_VCI)
   {
      if(gDebug) Mout << "VCI" << endl; 
      CalculateVciEta(arRhs,arOperName,arOperNr,arExptValue); 
   }
   else if (mCorrMethod==VCC_METHOD_VCC)
   {
      if(gDebug) Mout << "VCC" << endl; 
      this->CalculateXi(arRhs,arOperName,arOperNr,arExptValue);
   }
   else
   {
      Mout << "?" << endl;
      MIDASERROR("Unknown or unsupported method.");
   }
}

/**
* Calculate the Rhs transform - Tensor
* */
void Vcc::CalculateRhs
   (  TensorDataCont& arRhs
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   if (  gDebug
      )
   {
      Mout << "\n\n Calculate correlated RHS for: ";
   }
   if (  mCorrMethod==VCC_METHOD_VCI
      )
   {
      if (  gDebug
         )
      {
         Mout << "VCI" << std::endl; 
      }
      MIDASERROR("VCI not implemented for TensorDataCont!");
//      CalculateVciEta(arRhs, arOperName, arOperNr, arExptValue); 
   }
   else if  (  mCorrMethod==VCC_METHOD_VCC
            )
   {
      if (  gDebug
         )
      {
         Mout << "VCC" << std::endl; 
      }

      // Set shape of arRhs
      arRhs = ConstructTensorDataCont();

      this->CalculateXi(arRhs, arOperName, arOperNr, arExptValue);
   }
   else
   {
      Mout << "?" << std::endl;
      MIDASERROR("Unknown or unsupported method.");
   }
}

/**
* Calculate the Vcc Eta vector 
* */
void Vcc::CalculateEta
   (  DataCont& arEta
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   if (  gDebug
      )
   {
      Mout << "Vcc::CalculateEta" << endl; 
   }

   if (  mCorrMethod == VCC_METHOD_VCI
      )
   {
      this->CalculateVciEta(arEta,arOperName,arOperNr,arExptValue); 
   }
   else if  (  mCorrMethod == VCC_METHOD_VCC
            )
   {
      this->CalculateVccEtaX(arEta,arOperName,arOperNr);
      arExptValue = C_0;
   }
   else
   {
      MIDASERROR("Vcc::CalculateEta(): Unknown or unsupported method.");
   }
}

/**
 * Calculate Vcc Eta vector
 **/
void Vcc::CalculateEta
   (  TensorDataCont& arEta
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   if (  gDebug
      )
   {
      Mout << "Vcc::CalculateEta" << endl; 
   }

   if (  mCorrMethod == VCC_METHOD_VCI
      )
   {
      MIDASERROR("TensorDataCont Vcc::CalculateEta not implemented for VCI!");
   }
   else if  (  mCorrMethod == VCC_METHOD_VCC
            )
   {
      // Initialize eta to right shape and type
      arEta = ConstructTensorDataCont();

      this->CalculateVccEtaX(arEta, arOperName, arOperNr);
      arExptValue = C_0;
   }
   else
   {
      MIDASERROR("Vcc::CalculateEta(): Unknown or unsupported method.");
   }
}

/**
* Calculate the Eta transform for VCI.
* Vector is saved on disc, expectation value is appended as last value in data container.
* */
void Vcc::CalculateVciEta
   (  DataCont& arEta
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   if (gDebug) Mout << " Calculation of VCI eta vector and expectation value." << endl;
   In nvecsize = NrspPar();
   if (nvecsize!= arEta.Size())
      MIDASERROR("In VCC::CalculateVciEta() size mismatch.");

   string filename = mpVccCalcDef->GetName() + "_rsp_vci_eta_" + arOperName;
   DataCont etavec;
   if (mpVccCalcDef->RspRestart())
   {
      // Attempt to read eta vector from disc.
      Mout << " Restart. Attempting to read eta vector form file:" << endl
           << "    " << filename << endl;
      etavec.GetFromExistingOnDisc(nvecsize+I_1, filename);
      if (etavec.ChangeStorageTo("InMem", true))
      {
         Mout << "Success." << endl;
         arEta.Reassign(etavec, I_0, I_1, C_0, nvecsize);
         etavec.DataIo(IO_GET, nvecsize, arExptValue);
         return;
      }
      Mout << " Failure. Calculating eta vector from scratch..." << endl;
   }
   
   // Get operator.
   //Mout << " Operator name is: " << arOperName << endl;
   In i_oper=gOperatorDefs.GetOperatorNr(arOperName);
   if (i_oper == -I_1)
   {
      Mout << " The operator with label " << arOperName << " is not found." << endl;
      Mout << " The available operators are:" << endl;
      for (In i=I_0; i<gOperatorDefs.GetNrOfOpers(); i++)
         Mout << " " << gOperatorDefs[i].Name() << endl;
      MIDASERROR(" Operator could not be found." );
   }
   if (i_oper!=arOperNr)
      MIDASERROR("Operator check failed in CalculateVciEta");

   // Get basis.
   std::string basis_name = mpVccCalcDef->Basis();
   In i_basis = I_0;
   for (In i = I_0; i < gBasis.size(); i++)
   {
      if (gBasis[i].GetmBasName() == basis_name)
      {
         i_basis = i;
      }
   }
   
   // Calculate integrals.
   bool save = true;
   string storage = "InMem";
   if (mpVccCalcDef->IntStorage()!=I_0)
      storage = "OnDisc";
   OneModeInt one_mode_int(&gOperatorDefs[i_oper],&gBasis[i_basis],storage,save);
   one_mode_int.CalcInt();
  
   mRspTrf->PutOperatorToWorkingOperatorAndUpdateModalIntegrals(this, &gOperatorDefs[i_oper], &one_mode_int);
   
   // Set up vector to be transformed.
   string vecname = mpVccCalcDef->GetName() + "_Vci_vec_orto_rep";
   storage="InMem";
   DataCont vec(nvecsize,C_0,storage,vecname); // Set to zero vector. Reference state is
                                               // added by transformer.
   Nb a_nb=C_1;
   mRspTrf->SetType(TRANSFORMER::VCIRSP);
   //Mout << " before transform " << endl; 
   mRspTrf->Transform(vec,arEta,I_1,I_0,a_nb); // Std. transformer. 
   //Mout << " after transform " << endl; 

   mRspTrf->RestoreOrigOperAndUpdateModalIntegrals(this);
   
   arExptValue = a_nb;

   // Save result on disc.
   etavec.NewLabel(filename);
   etavec.SetNewSize(nvecsize+I_1);
   etavec.Reassign(arEta, I_0, I_1, C_0, nvecsize);
   etavec.DataIo(IO_PUT, nvecsize, arExptValue);
   etavec.SaveUponDecon(true);
   
   if (mpVccCalcDef->IoLevel() > I_5)
   {
      midas::stream::ScopedPrecision(16, Mout);
      Mout << " Size of reference CI space " << nvecsize << endl 
           << " Size of vector             " << vec.Size() << endl 
           << " Size of Eta vector         " << arEta.Size() << endl 
           << " Norm of vec in oc  =       " << vec.Norm() << endl
           << " Norm of eta vector =       " << arEta.Norm() << endl
           << " Expectation value  =       " << arExptValue << endl
           << " Eta calculation is done! " << endl;
   }
}

/**
* MBH: CalculateSigma: make F^X * U^i transformations
**/
void Vcc::CalculateSigma
   (  DataCont& arEigVec
   ,  DataCont& arSigma
   ,  const std::string& arOperName
   ,  const In& arOperNr
   ,  Nb& arExptValue
   ) 
{
   Mout << endl << endl << endl
        << " Calculation of correlated Sigma vector/expt values " << endl;
   
   // Open output file.
   string oper_name = arOperName; 
   if(gDebug) 
      Mout << " Operator name is: " << oper_name << endl;
   In i_oper=gOperatorDefs.GetOperatorNr(oper_name);
   if (i_oper==-I_1)
   {
      Mout << " The operator with label " << oper_name << " is not found." << endl;
      Mout << " The available operators are:" << endl;
      for (In i=0;i<gOperatorDefs.GetNrOfOpers();i++)
         Mout << " " << gOperatorDefs[i].Name() << endl;
      MIDASERROR(" Operator could not be found.");
   }
   if (i_oper!=arOperNr)
      MIDASERROR("Operator check failed in CalculateSigma");

   std::string basis_name = mpVccCalcDef->Basis();
   In i_basis = I_0;
   for (In i = I_0; i < gBasis.size(); i++)
   {
      if (gBasis[i].GetmBasName() == basis_name)
      {
         i_basis = i;
      }
   }
   
   //
   // Prepare operator information 
   //
   // Calculate integrals.
   bool save = true;
   string storage = "InMem";
   if (mpVccCalcDef->IntStorage()!=I_0) storage = "OnDisc";
   OneModeInt one_mode_int(&gOperatorDefs[i_oper],&gBasis[i_basis],storage,save);
   one_mode_int.CalcInt();
  
   mRspTrf->PutOperatorToWorkingOperatorAndUpdateModalIntegrals(this, &gOperatorDefs[i_oper], &one_mode_int);
   
   // inputvector is to be transformed now 
   In nvecsize = NrspPar();
   if (nvecsize!= arSigma.Size()) MIDASERROR("In VCC::CalculateSigma size mismatch");
   
   Nb a_nb=C_0;
   mRspTrf->SetType(TRANSFORMER::VCIRSP);
   mRspTrf->Transform(arEigVec,arSigma,I_1,I_0,a_nb); // Std. transformer. 
 
   mRspTrf->RestoreOrigOperAndUpdateModalIntegrals(this);
   
   arExptValue = a_nb;

   Mout << " Size of reference CI space " << nvecsize << endl; 
   Mout << " Size of vector " << arEigVec.Size() << endl; 
   Mout << " Size of sigma vector " << arSigma.Size() << endl; 

   Mout << " Norm of vec in oc  = " << arEigVec.Norm() << endl;
   Mout << " Norm of sigma vector = " << arSigma.Norm() << endl;
   if(gDebug)
      Mout << "Sigma at end:" << endl << arSigma << endl;
   Mout << " Sigma calculation is done! " << endl;
}

/**
* CalculateSigma: make F^X * U^i transformations
**/
void Vcc::CalculateSigma
   (  const TensorDataCont& arEigVec
   ,  TensorDataCont& arSigma
   ,  const std::string& arOperName
   ,  const In& arOperNr
   ,  Nb& arExptValue
   ) 
{
   MIDASERROR("CalculateSigma is only for VCI which is not implemented for TensorDataCont!");
}

void Vcc::GeneralFTransformation(DataCont& arDcIn, DataCont& arDcOut, Nb& aNb) 
{
   // only set type if not correct already
   if(mRspTrf->GetType()!=TRANSFORMER::VCIRSP)
      mRspTrf->SetType(TRANSFORMER::VCIRSP);
   mRspTrf->VciRspTransF(arDcIn,arDcOut,aNb);
}

/**
* Check Vci reference state that c_o > 
* */
void Vcc::CheckVciRefForRsp() 
{
   In nvecsize = mRspTrf->NexciTransXvec();
   DataCont ref_vec;
   In i_ref=I_0;
   string vecname = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i_ref);
   ref_vec.GetFromExistingOnDisc(nvecsize,vecname);
   ref_vec.SaveUponDecon();
   Nb c=C_NB_MAX;
   ref_vec.DataIo(IO_GET,I_0,c);
   if (fabs(c)<=C_NB_MIN) MIDASERROR("Reference state must not be zero in VCI.");
   if (c<C_0) 
   {
      Mout << " Reference state component should be positive in Vci." << endl;
      Mout << " Scaling VCI reference state with -1." << endl;
      ref_vec.Scale(C_M_1);
   }
}

In Vcc::PrepareRspTargetStates()
{
   if (gDebug) 
   {
      Mout << "Entered Vcc::PrepareRspTargetStates(): " << std::endl;
   }

   return PrepareTargetStates
      (  gAnalysisDir + "/RspTargetVectors"
      ,  "RspTargetVector"
      ,  mRspTrf->GetTransXvec()
      ,  true
      );
}

void Vcc::RspModesAndLevelsOut(ostream& arOut, const In& aAddr, bool aAllOut)
{
   mRspTrf->ConvertTransXvecToDataCont();
   ModesAndLevelsOut(arOut, aAddr+1, mRspTrf->GetTransXvec(), aAllOut);
}

/**
* Prepare data structures needed for correlated response calculations.
**/
void Vcc::PrepareRspTransformer()
{
   if (gDebug)
   {
      Mout << "Vcc::PrepareRspTransformer():" << std::endl;
   }

   Vcc* vcc_ptr = this;
   if (VCC_METHOD_VCI == mCorrMethod)
   {
      if (mpVccCalcDef->V3trans())
      {
         mRspTrf = std::make_unique<midas::vcc::TransformerV3>(vcc_ptr, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
         mRspTrf->SetType(TRANSFORMER::UNKNOWN);
         return;
      }
      else
      {
         mRspTrf = std::make_unique<VccTransformer>(vcc_ptr, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
         mRspTrf->SetType(TRANSFORMER::UNKNOWN);
      }
   }
   else if (VCC_METHOD_VCC == mCorrMethod)
   {
      // For debugging purposes.
      if (mpVccCalcDef->CmpV3OrigVccJac())
      {
         CompareVccRspTransformers();
         MIDASERROR("Testing done."); // not an error... we just want to stop here!
      }
      
      if (mpVccCalcDef->V3trans())
      {
         mRspTrf = std::make_unique<midas::vcc::TransformerV3>(vcc_ptr, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0, I_0, true);
         mRspTrf->SetType(TRANSFORMER::UNKNOWN);
         static_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->SetDecompInfo(this->mpVccCalcDef->GetVccRspTrfDecompInfo());
         return;
      } 

      // Setup for out-of-space calculation no matter what is requested (except for two-mode
      // which is always in-space).
      bool oos_save = mpVccCalcDef->VccOutOfSpace();
      if (mpVccCalcDef->TwoModeTrans() || mpVccCalcDef->OneModeTrans())
         mpVccCalcDef->SetVccOutOfSpace(false);
      else
         mpVccCalcDef->SetVccOutOfSpace(true);
      mRspTrf = std::make_unique<VccTransformer>(vcc_ptr, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
      mpVccCalcDef->SetVccOutOfSpace(oos_save);
         
      mRspTrf->SetType(TRANSFORMER::UNKNOWN);
      
      if (mpVccCalcDef->TwoModeTrans() || mpVccCalcDef->OneModeTrans())
      {
         if (gDebug)
         {
            if (mpVccCalcDef->OneModeTrans())
               Mout << "Vcc::PrepareRspTransformer(): "
                    << "Initializing one-mode jacobian transformer stuff... ";
            if (mpVccCalcDef->TwoModeTrans())
               Mout << "Vcc::PrepareRspTransformer(): "
                    << "Initializing two-mode jacobian transformer stuff... ";
         }
         mRspTrf->TwoModeVccRspInit();
      }
   }
}

void Vcc::SetRspTransformerAmplitudeFile
   (  const std::string& arFileName
   )
{
   if(!(mpVccCalcDef->V3trans()))
   {
      MIDASERROR("Expected V3 transformer!");
   }
   dynamic_cast<midas::vcc::TransformerV3*>(this->mRspTrf.get())->SetAmplitudeFile(arFileName);
}

void Vcc::DeleteRspTransformer()
{
   mRspTrf = nullptr;
}

Transformer* Vcc::GetRspTransformer(bool aRight)
{
   if (VCC_METHOD_VCI == mCorrMethod)
   {
      if (aRight)
         mRspTrf->SetType(TRANSFORMER::VCIRSP);
      else
         MIDASERROR("Left VCI transformation not implemented.");
   }
   else if (VCC_METHOD_VCC == mCorrMethod)
   {
      if (C_0 == mpVccCalcDef->GetNumVccJacobian())
      {
         if (aRight)
         {
            mRspTrf->SetType(TRANSFORMER::VCCJAC);
            mRspTrf->ReuseVcc2Trans(VccTransformer::VCC2RSP_T::RJAC);
         }
         else
         {
            mRspTrf->SetType(TRANSFORMER::LVCCJAC);
            mRspTrf->ReuseVcc2Trans(VccTransformer::VCC2RSP_T::LJAC);
         }
      }
      else
      {
         if (aRight)
            mRspTrf->SetType(TRANSFORMER::VCCJACDISC);
         else
            MIDASERROR("Numerical left transformation not implemented yet.");
      }
   }
   return mRspTrf.get();
}

void Vcc::CleanupRspTransformer(Transformer* aTrf)
{
   if (aTrf != mRspTrf.get())
   {
      MIDASERROR("Argument aTrf != &mRspTrf, so aTrf is not affected by this call.");
   }
   switch(mRspTrf->GetType())
   {
      case TRANSFORMER::VCCJAC:
         mRspTrf->ClearVcc2Trans(VccTransformer::VCC2RSP_T::RJAC);
         break;
      case TRANSFORMER::LVCCJAC:
         mRspTrf->ClearVcc2Trans(VccTransformer::VCC2RSP_T::LJAC);
         break;
      default:
         break;
   }
   mRspTrf->SetType(TRANSFORMER::UNKNOWN); 
}  

/**
 * Print relative weight of one-mode, two-mode etc. excitations.
 */
void Vcc::RspExciLevelWeight(vector<DataCont>& aEigVecs, In aRoots)
{
   const Xvec& xvec = mRspTrf->GetTransXvec();
   In exci_levels = xvec.NexciLevels();

   Mout << " Relative weight of excitation levels:" << endl;
   Out72Char(Mout, '+', '-', '+');
   Mout << " Root:    ";
   for (In i=I_1; i<exci_levels; i++)
      Mout << i << "                ";
   Mout << "Sum:" << endl;

   for (In i=I_0; i<aRoots; i++)
   {
      Mout << setw(5) << i;
      Nb norm2 = aEigVecs[i].Norm2();  // peter: I don't think normalization is necessary.
      if (norm2 == C_0)
      {
         Mout << "    Dummy (zero norm)" << endl;
         continue;
      }
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      midas::stream::ScopedPrecision(6, Mout);
      Nb tot = C_0;
      for (In level=1; level<exci_levels; level++)
      {
         In addr = xvec.AddressForExciLevel(level);
         In n = xvec.AddressForExciLevel(level+1) - addr;
         Nb sum = aEigVecs[i].Norm2(addr - I_1, n);
         Nb weight = sum/norm2;
         Mout << setw(17) << weight;
         tot += weight;
      }
      Mout << setw(17) << tot;
      Mout << endl;
   }
}

/**
 * Calculate aRes = F*R, where F = <Lambda| [[H_0, tau_nu], tau_mu] |VCC> and
 * R is an eigenvector of the Jacobian for state 'aState'.
 **/
void Vcc::CalculateFR
   (  DataCont& aRes
   ,  In aState
   )
{
   Mout << " Calculating F*R for state " << aState << endl;
   
   // Get right-hand eigenvector.
   string s = mpVccCalcDef->GetName() + "_rsp_eigvec_" + std::to_string(aState);
   DataCont eigvec;
   eigvec.GetFromExistingOnDisc(NrspPar(),s); 
   eigvec.SaveUponDecon();

   // Get zeroth order langrangian multipliers.
   s = mpVccCalcDef->GetName() + "_mul0_vec_0";
   DataCont mults;
   mults.GetFromExistingOnDisc(NrspPar(),s); 
   mults.SaveUponDecon();

   mRspTrf->SetType(TRANSFORMER::SPECIAL); 
   mRspTrf->CalcVccRspFR(mults, eigvec, aRes);
}


/**
 * Calculate aRes = F*R, where F = <Lambda| [[H_0, tau_nu], tau_mu] |VCC> and
 * R is an eigenvector of the Jacobian for state 'aState'.
 **/
void Vcc::CalculateFR
   (  TensorDataCont& aRes
   ,  In aState
   )
{
   Mout << " Calculating F*R for state " << aState << endl;
   
   // Get right-hand eigenvector.
   std::string s = mpVccCalcDef->GetName() + "_rsp_eigvec_tensor_" + std::to_string(aState);
   TensorDataCont eigvec;
   eigvec.ReadFromDisc(s);

   // Get zeroth order langrangian multipliers.
   s = mpVccCalcDef->GetName() + "_mul0_vec_tensor_0";
   TensorDataCont mults;
   mults.ReadFromDisc(s);

   // Set shape of aRes
   aRes = ConstructTensorDataCont();

   mRspTrf->SetType(TRANSFORMER::SPECIAL); 
   dynamic_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->CalcVccRspFR(mults, eigvec, aRes);
}

/**
 * Add non-variational contribution to gs->x transition matrix element.
 * aRspFunc: The response function index.
 * aReigVec: Right hand side Jacobian eigenvector for state aState.
 * aEtaR:    Value of eta^X times right-hand eigenvector
 *           (already calculated for variational wave functions.)
 **/
Nb Vcc::ResidContractGtoXnonVar(In aRspFunc, DataCont& aReigVec, Nb aEtaR)
{
   RspFunc& rsp_fct = mpVccCalcDef->GetRspFunc(aRspFunc);

   // Get final state and corresponding energy.
   In nroots = mpVccCalcDef->GetRspNeig();
   In state = rsp_fct.RightState();
   const string& oper_name = rsp_fct.GetRspOp(I_0);
   Nb eigval = C_0;
   DataCont eigval_dc;
   eigval_dc.GetFromExistingOnDisc(nroots, mpVccCalcDef->GetName() + "_rsp_eigval");
   eigval_dc.SaveUponDecon();
   eigval_dc.DataIo(IO_GET,state,eigval);
   
   /***********************************************
    * Calculate t_fo^X = 'left eigenvector' * xi. *
    ***********************************************/ 
   // Left eigenvector
   string s;
   s = mpVccCalcDef->GetName() + "_rsp_eigvec_left_" + std::to_string(state);
   //if(!InquireFile(s))
   //{
   //   Mout << " VCC rsp: " << aRspFunc << ": Left-hand eigenvector is a dummy state." << endl;
   //   return C_0;
   //}
   DataCont lefteig;
   lefteig.GetFromExistingOnDisc(NrspPar(), s); 
   lefteig.SaveUponDecon();
   if (lefteig.Norm() == C_0)
   {
      Mout << " VCC rsp: " << aRspFunc << ": Left-hand eigenvector is a dummy state." << endl;
      return C_0;                                       // Left eigenvector is a dummy state.
   }

   // Get xi vector.
   s = mpVccCalcDef->GetName() + "_xi_vec_" + oper_name;
   DataCont xi;
   xi.GetFromExistingOnDisc(NrspPar(),s); 
   xi.SaveUponDecon();
    
   Nb tfox = Dot(lefteig, xi) / Dot(lefteig, aReigVec);
 
   /****************************************************************************
    * Calculate t_of^X.
    * There are two possibilities::
    *    tofx = aEtaR + F * tX(-omega) * aReigvec   if no M vector is used
    * or
    *    tofx = aEtaR + M * xi^X                    if M vector is used.
    ****************************************************************************/
   Nb tofx = C_0;
   if (rsp_fct.UseMvec())
   {
      DataCont mvec;
      GetMvec(state, eigval, mvec);

      tofx = aEtaR + Dot(mvec, xi);
   }
   else
   {
      // Find and get first order amplitude responses.
      DataCont tx;
      GetFirstOrderRsp(oper_name, -eigval, tx);
      tx.SaveUponDecon();
      
      // Get zeroth order langrangian multipliers.
      s = mpVccCalcDef->GetName() + "_mul0_vec_0";
      DataCont mults;
      mults.GetFromExistingOnDisc(NrspPar(),s); 
      mults.SaveUponDecon();
      
      mRspTrf->SetType(TRANSFORMER::SPECIAL); 
      Nb ftr = mRspTrf->CalcVccRspFRS(mults, tx, aReigVec);

      Mout  << " aEtaR:    " << aEtaR << "\n"
            << " ftr:      " << ftr << "\n"
            << std::flush;

      tofx = aEtaR + ftr;
   }
   
   if (tofx*tfox < C_0)
   {
      Mout << " VCC rsp: " << aRspFunc << ": tofx*tfox < 0: tofx=" << tofx << " tfox=" << tfox
           << " tofx*tfox=" << tofx*tfox << endl;
      return C_0;
   }
  
   Mout << " VCC rsp: eigval=" << eigval*C_AUTKAYS
        << "   tofx=" << tofx << "   tfox=" << tfox << endl;


   return sqrt(tofx*tfox);
}


/**
 * Add non-variational contribution to gs->x transition matrix element.
 * aRspFunc: The response function index.
 * aReigVec: Right hand side Jacobian eigenvector for state aState.
 * aEtaR:    Value of eta^X times right-hand eigenvector
 *           (already calculated for variational wave functions.)
 **/
Nb Vcc::ResidContractGtoXnonVar
   (  In aRspFunc
   ,  const TensorDataCont& aReigVec
   ,  Nb aEtaR
   )
{
   RspFunc& rsp_fct = mpVccCalcDef->GetRspFunc(aRspFunc);

   // Get final state and corresponding energy.
   In nroots = mpVccCalcDef->GetRspNeig();
   In state = rsp_fct.RightState();
   const string& oper_name = rsp_fct.GetRspOp(I_0);
   Nb eigval = C_0;
   DataCont eigval_dc;
   eigval_dc.GetFromExistingOnDisc(nroots, mpVccCalcDef->GetName() + "_rsp_eigval_re_tensor");
   eigval_dc.SaveUponDecon();
   eigval_dc.DataIo(IO_GET,state,eigval);
   
   /***********************************************
    * Calculate t_fo^X = 'left eigenvector' * xi. *
    ***********************************************/ 
   // Left eigenvector
   std::string s = mpVccCalcDef->GetName() + "_rsp_eigvec_left_tensor_" + std::to_string(state);

   TensorDataCont lefteig;
   lefteig.ReadFromDisc(s);

   // Get xi vector.
   s = mpVccCalcDef->GetName() + "_xi_vec_" + oper_name + "_tensor";
   TensorDataCont xi;
   xi.ReadFromDisc(s);
    
   Nb tfox = Dot(lefteig, xi) / Dot(lefteig, aReigVec);
 
   /****************************************************************************
    * Calculate t_of^X.
    * There are two possibilities::
    *    tofx = aEtaR + F * tX(-omega) * aReigvec   if no M vector is used
    * or
    *    tofx = aEtaR + M * xi^X                    if M vector is used.
    ****************************************************************************/
   Nb tofx = C_0;
   if (  rsp_fct.UseMvec()
      )
   {
      TensorDataCont mvec;
      this->GetMvec(state, eigval, mvec);

      tofx = aEtaR + Dot(mvec, xi);
   }
   else
   {
      // Find and get first order amplitude responses.
      TensorDataCont tx;
      GetFirstOrderRsp(oper_name, -eigval, tx);
      
      // Get zeroth order langrangian multipliers.
      s = mpVccCalcDef->GetName() + "_mul0_vec_tensor_0";
      TensorDataCont mults;
      mults.ReadFromDisc(s);
      
      mRspTrf->SetType(TRANSFORMER::SPECIAL); 
      Nb ftr = dynamic_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->CalcVccRspFRS(mults, tx, aReigVec);

      Mout  << " aEtaR:    " << aEtaR << "\n"
            << " ftr:      " << ftr << "\n"
            << std::flush;

      tofx = aEtaR + ftr;
   }
   
   if (  tofx*tfox < C_0
      )
   {
      Mout << " VCC rsp: " << aRspFunc << ": tofx*tfox < 0: tofx=" << tofx << " tfox=" << tfox
           << " tofx*tfox=" << tofx*tfox << std::endl;
      return C_0;
   }
  
   Mout << " VCC rsp: eigval=" << eigval*C_AUTKAYS
        << "   tofx=" << tofx << "   tfox=" << tfox << std::endl;


   return sqrt(tofx*tfox);
}

/**
 * Calculate <<X;Y>>(w) for a non-variational wave function.
 * Presently only implemented and tested for VCC.
 **/
void Vcc::LrfContractNonVar
   (  In aRspFunc
   )
{
   RspFunc& rsp_fct = mpVccCalcDef->GetRspFunc(aRspFunc);

   // Get response function data.
   string operx = rsp_fct.GetRspOp(I_0);
   string opery = rsp_fct.GetRspOp(I_1);
   Nb     frq   = rsp_fct.GetRspFrq(I_1);
   
   // Get etaX vectors.
   DataCont etax;
   etax.GetFromExistingOnDisc(NrspPar(), mpVccCalcDef->GetName() + "_eta_vec_" + operx);
   etax.SaveUponDecon();

   DataCont etay;
   etay.GetFromExistingOnDisc(NrspPar(), mpVccCalcDef->GetName() + "_eta_vec_" + opery);
   etay.SaveUponDecon();
   
   // Get zeroth order langrangian multipliers.
   DataCont mults;
   mults.GetFromExistingOnDisc(NrspPar(), mpVccCalcDef->GetName() + "_mul0_vec_0"); 
   mults.SaveUponDecon();
   
   if(!rsp_fct.ComplexRspFunc() && !mpVccCalcDef->ForceComplexRsp())
   {
      /**
       * evaluate response function non-complex case
       **/
      // Get first order amplitude response vectors.
      DataCont txpf;                         // t^X(+w)
      DataCont txmf;                         // t^X(-w)
      DataCont typf;                         // t^Y(+w)
      DataCont tymf;                         // t^Y(-w)
      GetFirstOrderRsp(operx,frq,txpf);
      GetFirstOrderRsp(operx,-frq,txmf);
      GetFirstOrderRsp(opery,frq,typf);
      GetFirstOrderRsp(opery,-frq,tymf);
 
      // Calculate value.
      Nb val = C_0;
      val += Dot(etax,typf);
      val += Dot(etax,tymf);
      val += Dot(etay,txpf);
      val += Dot(etay,txmf);
      mRspTrf->SetType(TRANSFORMER::SPECIAL); 
      val += mRspTrf->CalcVccRspFRS(mults, txmf, typf);
      val += mRspTrf->CalcVccRspFRS(mults, txpf, tymf);
      val /= C_2;

      // Store value.
      rsp_fct.SetValue(val);
   }
   else
   {
      /**
       * complex case
       **/
      Nb gamma = rsp_fct.Gamma();
      ComplexVector<DataCont,DataCont> txpf;  // t^X(+w + i\gamma)
      ComplexVector<DataCont,DataCont> txmf;  // t^X(-w - i\gamma)
      ComplexVector<DataCont,DataCont> typf;  // t^Y(+w + i\gamma)
      ComplexVector<DataCont,DataCont> tymf;  // t^Y(-w - i\gamma)
      GetFirstOrderRsp(operx,frq,gamma,txpf);
      GetFirstOrderRsp(operx,-frq,-gamma,txmf);
      GetFirstOrderRsp(opery,frq,gamma,typf);
      GetFirstOrderRsp(opery,-frq,-gamma,tymf);
      
      // calculate re_val
      Nb re_val = C_0;
      re_val += Dot(etax,typf.Re());
      re_val += Dot(etax,tymf.Re());
      re_val += Dot(etay,txmf.Re());
      re_val += Dot(etay,txpf.Re());
      mRspTrf->SetType(TRANSFORMER::SPECIAL); 
      re_val += mRspTrf->CalcVccRspFRS(mults, txmf.Re(), typf.Re());
      re_val -= mRspTrf->CalcVccRspFRS(mults, txmf.Im(), typf.Im());
      re_val += mRspTrf->CalcVccRspFRS(mults, txpf.Re(), tymf.Re());
      re_val -= mRspTrf->CalcVccRspFRS(mults, txpf.Im(), tymf.Im());
      re_val /= C_2;

      rsp_fct.SetValue(re_val);
      
      // calculate im_val
      Nb im_val = C_0;
      im_val += Dot(etax,typf.Im());
      im_val += Dot(etax,tymf.Im());
      im_val += Dot(etay,txmf.Im());
      im_val += Dot(etay,txpf.Im());
      im_val += mRspTrf->CalcVccRspFRS(mults, txmf.Re(), typf.Im());
      im_val += mRspTrf->CalcVccRspFRS(mults, txmf.Im(), typf.Re());
      im_val += mRspTrf->CalcVccRspFRS(mults, txpf.Re(), tymf.Im());
      im_val += mRspTrf->CalcVccRspFRS(mults, txpf.Im(), tymf.Re());
      im_val /= C_2;

      rsp_fct.SetImValue(im_val);

      // associate files needed for analysis
      rsp_fct.AssociateFile({operx}, analysis::rsp().linear_rsp_vec + "+re", txpf.Re().Label());
      rsp_fct.AssociateFile({operx}, analysis::rsp().linear_rsp_vec + "+im", txpf.Im().Label());
      rsp_fct.AssociateFile({operx}, analysis::rsp().linear_rsp_vec + "-re", txmf.Re().Label());
      rsp_fct.AssociateFile({operx}, analysis::rsp().linear_rsp_vec + "-im", txmf.Im().Label());
      rsp_fct.AssociateFile({opery}, analysis::rsp().linear_rsp_vec + "+re", typf.Re().Label());
      rsp_fct.AssociateFile({opery}, analysis::rsp().linear_rsp_vec + "+im", typf.Im().Label());
      rsp_fct.AssociateFile({opery}, analysis::rsp().linear_rsp_vec + "-re", tymf.Re().Label());
      rsp_fct.AssociateFile({opery}, analysis::rsp().linear_rsp_vec + "-im", tymf.Im().Label());

      // HACK to save eta vectors in analysis rsp (probably permanent :CCCC )
      DataCont etax_copy(etax);
      etax_copy.NewLabel(analysis::rsp().analysis_dir + "/" + etax.Label());
      etax_copy.SaveUponDecon(true);
      DataCont etay_copy(etax);
      etay_copy.NewLabel(analysis::rsp().analysis_dir + "/" + etay.Label());
      etay_copy.SaveUponDecon(true);
      
      rsp_fct.AssociateFile({operx}, analysis::rsp().eta_vec, etax_copy.Label());
      rsp_fct.AssociateFile({opery}, analysis::rsp().eta_vec, etay_copy.Label());
   }
   
   //
   rsp_fct.SetHasBeenEval(true);
}

/**
 * Calculate <<X;Y>>(w) for a non-variational wave function.
 * Presently only implemented and tested for VCC.
 **/
void Vcc::TensorLrfContractNonVar
   (  In aRspFunc
   )
{
   RspFunc& rsp_fct = mpVccCalcDef->GetRspFunc(aRspFunc);

   // Get response function data.
   std::string operx = rsp_fct.GetRspOp(I_0);
   std::string opery = rsp_fct.GetRspOp(I_1);
   Nb frq = rsp_fct.GetRspFrq(I_1);

   // Get etaX vectors.
   TensorDataCont etax;
   etax.ReadFromDisc(mpVccCalcDef->GetName() + "_eta_vec_" + operx + "_tensor");

   TensorDataCont etay;
   etay.ReadFromDisc(mpVccCalcDef->GetName() + "_eta_vec_" + opery + "_tensor");
   
   // Get zeroth order langrangian multipliers.
   TensorDataCont mults;
   mults.ReadFromDisc(mpVccCalcDef->GetName() + "_mul0_vec_tensor_0");
   
   if (  !rsp_fct.ComplexRspFunc()
      && !mpVccCalcDef->ForceComplexRsp()
      )
   {
      /**
       * evaluate response function non-complex case
       **/
      // Get first order amplitude response vectors.
      TensorDataCont txpf;                         // t^X(+w)
      TensorDataCont txmf;                         // t^X(-w)
      TensorDataCont typf;                         // t^Y(+w)
      TensorDataCont tymf;                         // t^Y(-w)
      this->GetFirstOrderRsp(operx, frq, txpf);
      this->GetFirstOrderRsp(operx,-frq, txmf);
      this->GetFirstOrderRsp(opery, frq, typf);
      this->GetFirstOrderRsp(opery,-frq, tymf);
 
      // Calculate value.
      Nb val = C_0;
      val += Dot(etax, typf);
      val += Dot(etax, tymf);
      val += Dot(etay, txpf);
      val += Dot(etay, txmf);
      mRspTrf->SetType(TRANSFORMER::SPECIAL); 
      val += dynamic_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->CalcVccRspFRS(mults, txmf, typf);
      val += dynamic_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->CalcVccRspFRS(mults, txpf, tymf);
      val /= C_2;

      // Store value.
      rsp_fct.SetValue(val);
   }
   else
   {
      MIDASERROR("Complex rsp func not implemented for TensorDataCont yet!");
   }
   
   //
   rsp_fct.SetHasBeenEval(true);
}

void Vcc::MatchLeftRightRspEigVecs(MidasVector& aEigVals, vector<DataCont>& aEigVecs)
{
   if (VCC_METHOD_VCC != mCorrMethod)
      MIDASERROR("Vcc::MatchLeftRightRspEigVecs(): This is not a VCC calculation. Something is wrong.");

   Mout << " Matching VCC left- and right-hand Jacobian eigenvectors..." << endl;
   
   In right_roots = mpVccCalcDef->GetRspNeig();
   In left_roots = aEigVals.Size();

   // --- Debugging output to see if things work...
   Mout << "    aEigVecs labels" << endl;
   for (In i=I_0; i<aEigVecs.size(); ++i)
      Mout << "       " << setw(2) << i << ": " << aEigVecs[i].Label()
           << "   " << aEigVecs[i].Storage() << endl;

   Mout << "    Match matrix:" << endl;
   Mout << showpos;
   

   for (In r_idx=I_0; r_idx<right_roots; ++r_idx)
   {
      DataCont rdc;
      rdc.GetFromExistingOnDisc(NrspPar(),mpVccCalcDef->GetName() + "_rsp_eigvec_" + std::to_string(r_idx));
      rdc.ChangeStorageTo("InMem");
      rdc.SaveUponDecon();

      Mout << "       r(" << r_idx << ") ";
      for (In l_idx=I_0; l_idx<left_roots; ++l_idx)
         Mout << fabs(Dot(rdc, aEigVecs[l_idx])) << " ";
      Mout << endl;
   }
   Mout << "\n" << noshowpos;
   // --- end debugging output ---
   
   //DataCont r_eigval_dc;
   //r_eigval_dc.GetFromExistingOnDisc(6,mpVccCalcDef->Name() + "_rsp_eigval");
   //r_eigval_dc.ChangeStorageTo("InMem");
   //r_eigval_dc.SaveUponDecon();
   //auto r_eigval = r_eigval_dc.GetVector();
   
   bool degeneracy_warning = false;

   // for loop to do the work
   for (In r_idx=I_0; r_idx<right_roots; ++r_idx)
   {
      DataCont rdc;
      rdc.GetFromExistingOnDisc(NrspPar(),
                                mpVccCalcDef->GetName() + "_rsp_eigvec_" + std::to_string(r_idx));
      rdc.ChangeStorageTo("InMem");
      rdc.SaveUponDecon();

      std::map<Nb,In> found;
      for (In l_idx=I_0; l_idx<left_roots; ++l_idx)
      {
         Nb dot = fabs(Dot(rdc, aEigVecs[l_idx]));

         if (dot >= 1/C_100)  //  NB NB ! A rather arbitrary limit. ! NB NB
         {
            //found.emplace(dot,l_idx); // <- should do this when c++11 is properly implemented
            found.insert(std::make_pair(dot,l_idx));
         }
      }

      if(found.size() > 1)
      {
         Mout << " Matching left- and right-hand eigenvectors:\n" 
              << "    More possible matches found.\n"
              << "    There is a possible generacy.\n"
              << " Energies:";
         for(auto iter = found.begin(); iter!=found.end(); ++iter)
              Mout << " " << aEigVals[iter->second];
              
         Mout << "\n Will use the one with best overlap.\n"
              << std::endl;
         degeneracy_warning = true;
      }

      auto iter = found.rbegin();

      if (found.size() == 0)
      {
         // Insert dummy left-hand vector to preserve solution numbering.
         aEigVals.SetNewSize(aEigVals.Size()+I_1);
         aEigVecs.push_back(DataCont(NrspPar(), C_0, aEigVecs[I_0].Storage()));
         aEigVecs.back().NewLabel(mpVccCalcDef->GetName() + "_rsp_eigvec_left_"
                                  + std::to_string(left_roots));
         aEigVecs.back().SaveUponDecon();
         for (In i=left_roots-I_1; i>=r_idx; --i)
         {
            aEigVecs[i+I_1] = aEigVecs[i];
            aEigVals[i+I_1] = aEigVals[i];
         }
         left_roots++;
         aEigVecs[r_idx].Zero();
         aEigVals[r_idx] = C_0;
      }
      else if (iter->second != r_idx)
      {
         // Swap left eigenvectors at indices 'iter->second' (the left vector with best overlap)
         // and 'r_idx' to make left- and right-hand solution numbers correspond.
         DataCont tmp(aEigVecs[r_idx]);
         aEigVecs[r_idx] = aEigVecs[iter->second];
         aEigVecs[iter->second] = tmp;
         Nb val = aEigVals[r_idx];
         aEigVals[r_idx] = aEigVals[iter->second];
         aEigVals[iter->second] = val;
      }
   }
   // end for loop 

   if(degeneracy_warning)
      Mout << " ATTENTION: No action was taken to make sure that the same left vector was not matched with with two right vectors. " << std::endl;

   // --- Debugging output to see if things work...
   Mout << "    aEigVecs labels" << endl;
   for (In i=I_0; i<aEigVecs.size(); ++i)
      Mout << "       " << setw(2) << i << ": " << aEigVecs[i].Label()
           << "   " << aEigVecs[i].Storage() << endl;

   Mout << "    Match matrix:" << endl;
   Mout << showpos;
   for (In r_idx=I_0; r_idx<right_roots; ++r_idx)
   {
      DataCont rdc;
      rdc.GetFromExistingOnDisc(NrspPar(),
                                mpVccCalcDef->GetName() + "_rsp_eigvec_" + std::to_string(r_idx));
      rdc.ChangeStorageTo("InMem");
      rdc.SaveUponDecon();

      Mout << "       r(" << r_idx << ") ";
      for (In l_idx=I_0; l_idx<left_roots; ++l_idx)
         Mout << fabs(Dot(rdc, aEigVecs[l_idx])) << " ";
      Mout << endl;
   }
   Mout << noshowpos;
   // --- end debugging output ---
}

void Vcc::CalcNumVccJacobian()
{
   const double stepsize = mpVccCalcDef->GetNumVccJacobian();
   Mout << endl
        << " *** Calculating VCC Jacobian numerically using finite difference ***\n"
        << "     Step size = " << stepsize << endl;

   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   
   // Get cluster amplitudes for ground state from disc.
   string s = mpVccCalcDef->GetName() + "_Vcc_vec_0";
   string e = mpVccCalcDef->GetName() + "_Vcc_eta_vec";
   In vecsize = trf.NexciTransXvec() - I_1;
   DataCont amp_dc;
   amp_dc.GetFromExistingOnDisc(vecsize, s);
   amp_dc.SaveUponDecon();

   s = mpVccCalcDef->GetName() + "_Vcc_Jacobian";    // Jacobian on disc...
   DataCont A_dc;
   A_dc.NewLabel(s);
   A_dc.ChangeStorageTo("OnDisc");                // Stores nothing on disc since size = 0.
   A_dc.SetNewSize(vecsize*vecsize);              // Change size but no change to file on disc.
   A_dc.SaveUponDecon();

   // If restart is set, check if data are already on disc.
   if (mpVccCalcDef->RspRestart())
   {
      if (A_dc.ChangeStorageTo("OnDisc", true, true))
      {
         Mout << "    Restart: Using Jacobian already present on disc.\n"
              << "    Step size not checked." << endl;
         return;
      }
      else
         Mout << "    Restart: Failed to read Jacobian from disc. Creating new one." << endl;
   }
   A_dc.Zero();      // The SetNewSize() didn't write anything to disc. Put zeros to disc
                     // for stride to work.

   // Find numerical derivatives wrt. each amplitude giving the columns in the Jacobian.
   DataCont eq_ph(vecsize);                   // CC eqs. evaluated at ground state vec.
   DataCont eq_mh(vecsize);                   //    +/- step size
   MidasVector A_col(vecsize);                // Column in Jacobian.
   DataCont eta_num(vecsize);
   eta_num.NewLabel(e);
   eta_num.ChangeStorageTo("OnDisc");
   eta_num.SaveUponDecon();
   for (In i = I_0; i < vecsize; i++)
   {
      Nb energy_ph; 
      Nb energy_mh;
      Nb orig_amp;
      amp_dc.DataIo(IO_GET, i, orig_amp);
      Nb t0 = orig_amp + stepsize;
      amp_dc.DataIo(IO_PUT, i, t0);
      trf.VccTransH(amp_dc, eq_ph, I_1, I_0, energy_ph);
      t0 = orig_amp - stepsize;
      amp_dc.DataIo(IO_PUT, i, t0);
      trf.VccTransH(amp_dc, eq_mh, I_1, I_0, energy_mh);
      amp_dc.DataIo(IO_PUT, i, orig_amp);                // Restore orig. amplitude.
      
      eq_ph.Axpy(eq_mh, -C_1);                           // This can be optimized to be done
      eq_ph.Scale(C_1/(C_2*stepsize));                   // in one step.

      Nb eta_num_i = (energy_ph - energy_mh)/(C_2*stepsize); 
      eta_num.DataIo(IO_PUT, i, eta_num_i);                // fill the numerical eta vector. 

      // Intermediate vector A_col should not be necessary. 
      eq_ph.DataIo(IO_GET, A_col, vecsize);
      A_dc.DataIo(IO_PUT, A_col, vecsize, i, vecsize);   // Put using stride to store column.
      if (i % I_10 == 0)
         Mout << "    Column " << i << " of " << vecsize << " done." << endl;
   }
   Mout << "    ...Done" << endl;

   // Brute force diagonalization of Jacobian.
   /* 
   MidasMatrix Adisc(vecsize, vecsize);
   A_dc.DataIo(IO_GET, Adisc, vecsize*vecsize, vecsize, vecsize);
   MidasMatrix eigvecs(vecsize, vecsize);
   MidasVector eigvals(vecsize);
   Diag(Adisc, eigvecs, eigvals, "DGEEVX"); 
   eigvals *= C_AUTKAYS;
   tmr.CpuOut(Mout, "CPU time used for brute force diag.: ");

   Mout << "Jacobian eigenvalues and eigenvectors by brute force DGEEVX diagonalization:" << endl;
   for (In i=0; i<mpVccCalcDef->GetRspNeig(); i++)
   {
      MidasVector eigv(vecsize);
      eigvecs.GetCol(eigv, i);
      Mout << "   No. " << setw(2) << i << ":  " << eigvals[i] << endl;
      //Mout << "            " << eigv << endl;
   }
   */
}


void Vcc::CalcNumVccF()
{
   const double stepsize = mpVccCalcDef->GetNumVccF();
   Mout << endl
        << " *** Calculating VCC response F matrix numerically using finite difference ***\n"
           << "     Step size = " << stepsize << endl;

   In vecsize = NrspPar();
  
   // Get cluster amplitudes for ground state from disc.
   string s = mpVccCalcDef->GetName() + "_Vcc_vec_0";
   DataCont amp_dc;
   amp_dc.GetFromExistingOnDisc(vecsize, s);
   amp_dc.ChangeStorageTo("OnDisc");   // To make sure our changes are used by the transformer.
   amp_dc.SaveUponDecon();

   // Get zeroth order multipliers from disc.
   DataCont mults;
   string vecname=mpVccCalcDef->GetName()+"_mul0_vec_0";
   mults.GetFromExistingOnDisc(vecsize,vecname);
   mults.SaveUponDecon(true);

   s = mpVccCalcDef->GetName() + "_Vcc_F";    // F matrix on disc...
   DataCont F_dc;
   F_dc.NewLabel(s);
   F_dc.ChangeStorageTo("OnDisc");                // Stores nothing on disc since size = 0.
   F_dc.SetNewSize(vecsize*vecsize);              // Change size but no change to file on disc.
   F_dc.SaveUponDecon();

   // If restart is set, check if data are already on disc.
   if (mpVccCalcDef->RspRestart())
   {
      if (F_dc.ChangeStorageTo("OnDisc", true, true))
      {
         Mout << "    Restart: Using F matrix already present on disc.\n"
              << "    Step size not checked." << endl;
         return;
      }
      else
         Mout << "    Restart: Failed to read F matrix from disc. Creating new one." << endl;
   }

   F_dc.Zero();      // The SetNewSize() didn't write anything to disc. Put zeros to disc
                     // for stride to work.

   // Find numerical derivatives wrt. each amplitude giving the columns in the F matrix
   DataCont eta_ph(vecsize);
   DataCont eta_mh(vecsize);
   DataCont jac_ph(vecsize);
   DataCont jac_mh(vecsize);
   MidasVector F_col(vecsize);                // Column of F.
   Nb dummy = C_0;                            // Used when calling transformer.
   for (In i = I_0; i < vecsize; i++)
   {
      Nb orig_amp = C_0;
      amp_dc.DataIo(IO_GET, i, orig_amp);
      Nb t = orig_amp + stepsize;
      amp_dc.DataIo(IO_PUT, i, t);

      mRspTrf->CalcVccRspEta0(eta_ph);
      mRspTrf->SetType(TRANSFORMER::LVCCJAC);
      mRspTrf->Transform(mults, jac_ph, I_1, I_0, dummy);

      t = orig_amp - stepsize;
      amp_dc.DataIo(IO_PUT, i, t);
      
      mRspTrf->CalcVccRspEta0(eta_mh);
      mRspTrf->SetType(TRANSFORMER::LVCCJAC);
      mRspTrf->Transform(mults, jac_mh, I_1, I_0, dummy);
     
      // Restore amplitude vector. 
      amp_dc.DataIo(IO_PUT, i, orig_amp);

      // Calculate column of F matrix.
      eta_ph.Axpy(eta_mh, -C_1);
      eta_ph.Scale(C_1/(C_2*stepsize));
      jac_ph.Axpy(jac_mh, -C_1);
      jac_ph.Scale(C_1/(C_2*stepsize));
      jac_ph.Axpy(eta_ph, C_1);
      jac_ph.DataIo(IO_GET, F_col, vecsize);
      F_dc.DataIo(IO_PUT, F_col, vecsize, i, vecsize);   // Put using stride to store column.
   }
   Mout << "    ...Done" << endl;
}

void Vcc::LanczosChainRspDrv()
{
   Mout << endl;
   Mout << " Starting response function calculations using Lanczos chains." << endl;
   if (mCorrMethod != VCC_METHOD_VCI && mCorrMethod != VCC_METHOD_VCC)
      MIDASERROR(" Vcc::LanczosChainRspDrv(): Only implemented for VCI and VCC.");
 
   // Get list of Lanczos chain definitions
   vector<LanczosChainDef*> chain_defs;
   vector<NHermLanczosChainDef*> nh_chain_defs;
   vector<BandLanczosChainDef*> band_chain_defs;
   vector<NHBandLanczosChainDef*> nhband_chain_defs;
   string type;
   In n_fcts = mpVccCalcDef->NlanczosRspFuncs();

   for(In i_fct=I_0; i_fct<n_fcts; i_fct++)
   {
      LanczosRspFunc* blc = mpVccCalcDef->GetLanczosRspFunc(i_fct);
      vector<LanczosChainDefBase*> tmp=blc->GetChains(type);

      if(type == "lan")
      {   
         for(In i=I_0; i<tmp.size(); ++i)
         {        
            LanczosChainDef* lan_tmp = dynamic_cast<LanczosChainDef*>(tmp[i]);
            In j = I_0;
            for(j = I_0; j<chain_defs.size(); j++)
               if(chain_defs[j]->Combine((*lan_tmp)))
                  break;

            if(j == chain_defs.size())
            {
               chain_defs.push_back(lan_tmp);
               tmp[i] = nullptr;
            }
         }
      }
      else if(type == "lan_nh")
      {  
         for(In i=I_0; i<tmp.size(); ++i)
         { 
            NHermLanczosChainDef* nhlan_tmp = dynamic_cast<NHermLanczosChainDef*>(tmp[i]);
            In j = I_0;
            for(j = I_0; j<nh_chain_defs.size(); j++)
               if(nh_chain_defs[j]->Combine((*nhlan_tmp)))
                  break;
      
            if(j == nh_chain_defs.size())
            {
               nh_chain_defs.push_back(nhlan_tmp);
               tmp[i] = nullptr;
            }
         }
      }
      else if(type == "bandlan")
      {   
         for(In i=I_0; i<tmp.size(); ++i)
         {
            BandLanczosChainDef* blan_tmp = dynamic_cast<BandLanczosChainDef*>(tmp[i]);
            In j = I_0;
            for(j = I_0; j<band_chain_defs.size(); j++)
               if(band_chain_defs[j]->Combine((*blan_tmp)))
                  break;
      
            if(j == band_chain_defs.size())
            {
               band_chain_defs.push_back(blan_tmp);
               tmp[i] = nullptr;
            }
         }
      }
      else if(type == "bandlan_nh")
      {  
         for(In i=I_0; i<tmp.size(); ++i)
         { 
            NHBandLanczosChainDef* nhblan_tmp = dynamic_cast<NHBandLanczosChainDef*>(tmp[i]);
            In j = I_0;
            for(j = I_0; j<nhband_chain_defs.size(); j++)
               if(nhband_chain_defs[j]->Combine((*nhblan_tmp)))
                  break;
      
            if(j == nhband_chain_defs.size())
            {
               nhband_chain_defs.push_back(nhblan_tmp);
               tmp[i] = nullptr;
            }
         }
      }
      else
      {
         MIDASERROR("I R Error in VccRsp::LanczosChainRspDrv()");
      }

      for(size_t i=0; i<tmp.size(); ++i)
      {
         if(tmp[i])
         {
            delete tmp[i];
         }
      }
   }

   ///> Output required chain definitions
   if(chain_defs.size()>0)
   {   
      Mout << endl;
      Mout << " There are " << chain_defs.size() << " required Lanczos chains: " << endl;
   }
   for(In i=0; i<chain_defs.size(); i++)
   {
      Mout << (*chain_defs[i]) << endl;
   }
   if(nh_chain_defs.size()>0)
   {
      Mout << endl;
      Mout << " There are " << nh_chain_defs.size() << " required Non-Hermitian Lanczos chains: " << endl;
   }
   for(In i=0; i<nh_chain_defs.size(); i++)
   {
      //Mout << (*nh_chain_defs[i]) << endl;
   }
   if(band_chain_defs.size()>0)
   {  
      Mout << endl;
      Mout << " There are " << band_chain_defs.size() << " required Band Lanczos chains: " << endl;
   }
   for(In i=0; i<band_chain_defs.size(); i++)
   {
      Mout << (*band_chain_defs[i]) << endl;
   }
   if(nhband_chain_defs.size()>0)
   {
      Mout << endl;
      Mout << " There are " << nhband_chain_defs.size() << " required Non-Hermitian Band Lanczos chains: " << endl;
   }
   for(In i=0; i<nhband_chain_defs.size(); i++)
   {
      //Mout << (*nhband_chain_defs[i]) << endl;
   }
   Mout << endl;
 
   ///> Generate list of Lanczos Chains and evaluate these.
   vector<LanczosChain> lcs_chains;
   vector<NHermLanczosChain> nhlcs_chains;
   vector<BandLanczosChain> blcs_chains;
   vector<NHBandLanczosChain> nhblcs_chains;
   for(In i=I_0; i<chain_defs.size(); i++)
   {
      lcs_chains.push_back(LanczosChain((*chain_defs[i]), this, mRspTrf.get()));
      lcs_chains.back().Evaluate();
   }
   for(In i=I_0; i<nh_chain_defs.size(); i++)
   {
      nhlcs_chains.push_back(NHermLanczosChain((*nh_chain_defs[i]), this, mRspTrf.get()));
      nhlcs_chains.back().Evaluate();
   }
   for(In i=I_0; i<band_chain_defs.size(); i++)
   {
      blcs_chains.push_back(BandLanczosChain((*band_chain_defs[i]), this, mRspTrf.get()));
      blcs_chains.back().Evaluate();
   }
   for(In i=I_0; i<nhband_chain_defs.size(); i++)
   {
      nhblcs_chains.push_back(NHBandLanczosChain((*nhband_chain_defs[i]), this, mRspTrf.get()));
      nhblcs_chains.back().Evaluate();
   }
 
   ///> Evaluate response functions
   Mout << " Evaluating Lanczos response functions..." << endl;
   In prefixid = I_0;
   for(In i_fct=I_0; i_fct<n_fcts; i_fct++)
   {
      LanczosRspFunc* blc = mpVccCalcDef->GetLanczosRspFunc(i_fct);
      if(!blc->GetBand() && !blc->GetNHerm())
         blc->Initialize(this, mpVccCalcDef, mRspTrf.get(), &lcs_chains);
      else if(!blc->GetBand() && blc->GetNHerm())
         blc->Initialize(this, mpVccCalcDef, mRspTrf.get(), &nhlcs_chains);
      else if(blc->GetBand() && !blc->GetNHerm())
         blc->Initialize(this, mpVccCalcDef, mRspTrf.get(), &blcs_chains);
      else if(blc->GetBand() && blc->GetNHerm())
         blc->Initialize(this, mpVccCalcDef, mRspTrf.get(), &nhblcs_chains);
      else
         MIDASERROR("Error in VccRsp::LanczosChainRspDrv()");

      if(blc->GetName() == "")
      {
         blc->SetName("blcs_rsp" + std::to_string(prefixid));
         prefixid++;
      }
      blc->Evaluate();
      blc->SaveCombinedRspFunc();
   }

   ///> Generate requested Lanczos IR spectra
   if(mpVccCalcDef->NlanczosIRspects() > I_0)
      Mout << " Generating Lanczos IR spectra..." << endl;

   for (In i=I_0; i<mpVccCalcDef->NlanczosIRspects(); i++)
   {
      LanczosIRspect* ir = mpVccCalcDef->GetLanczosIRspect(i);
      ir->Create(mpVccCalcDef, this);
   }

   ///> Generate requested Band Lanczos IR spectra.
   if (mpVccCalcDef->NBandLanczosIRspects() > I_0)
      Mout << " Generating Band Lanczos IR spectra..." << endl;

   for (In i=I_0; i<mpVccCalcDef->NBandLanczosIRspects(); i++)
   {
      BandLanczosIRspect* ir = mpVccCalcDef->GetBandLanczosIRspect(i);
      ir->Create(mpVccCalcDef, this);
   }
   
   ///> Generate requested Band Lanczos Raman spectra.
   if (mpVccCalcDef->NBandLanczosRamanSpects() > I_0)
      Mout << " Generating Band Lanczos Raman spectra..." << endl;

   for (In i=I_0; i<mpVccCalcDef->NBandLanczosRamanSpects(); i++)
   {
      BandLanczosRamanSpect* ram = mpVccCalcDef->GetBandLanczosRamanSpect(i);
      ram->Create(mpVccCalcDef, this);
   }
   
   // Clean up
   for(In i=I_0; i<chain_defs.size(); i++)
      delete chain_defs[i];   
   for(In i=I_0; i<nh_chain_defs.size(); i++)
      delete nh_chain_defs[i];
   for(In i=I_0; i<band_chain_defs.size(); i++)
      delete band_chain_defs[i];
   for(In i=I_0; i<nhband_chain_defs.size(); i++)
      delete nhband_chain_defs[i];

   Mout << " Response function calculations using Lanczos chains done." << endl << endl;
} 

/**
 * Note: Original transformer assumes VCC equations to be solved.
 * Therefore, this test is only valid after the reference has been converged.
 **/
void Vcc::CompareVccRspTransformers()
{
   Mout << "Comparing VCC response transformers..." << endl;

   // Set up original and new transformer.
   bool oos_save = mpVccCalcDef->VccOutOfSpace();
   mpVccCalcDef->SetVccOutOfSpace(true);
   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   mpVccCalcDef->SetVccOutOfSpace(oos_save);
   midas::vcc::TransformerV3 v3(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   trf.SetType(TRANSFORMER::VCCJAC);
   v3.SetType(TRANSFORMER::VCCJAC);
             
   // Initialize input coefficients.
   In namps = trf.NexciTransXvec();
   DataCont dc_inp(namps-I_1, C_0, "InMem", "test_trf_in");
   Nb val = C_0;
   srand((unsigned)time(0));
   for (In i=I_0; i<namps-I_1; ++i)
   {
      val = Nb((rand() % I_10) + I_1)/C_10;
      dc_inp.DataIo(IO_PUT, i, val);
   }

   // Data containers for output.
   DataCont dc_orig(namps-I_1, C_0, "InMem", "test_trf_orig");
   DataCont dc_v3(namps-I_1, C_0, "InMem", "test_trf_v3");
   Nb e_orig = C_0;
   Nb e_v3 = C_0;
 
   // Compare...
   Mout << "Calling original transformer." << endl;
   trf.Transform(dc_inp, dc_orig, I_1, I_0, e_orig);
   Mout << "Calling V3 transformer." << endl;
   v3.Transform(dc_inp, dc_v3, I_1, I_0, e_v3);

   trf.CmpDCs(dc_orig, dc_v3, true);
   Mout << endl << endl;
}

/**
 * Check V3 left-hand transformer algorithm.
 **/
void Vcc::CheckV3Left()
{
   //OpDef* fakeop = pOpDef()->GenerateFakeOper();
   //midas::vcc::TransformerV3 v3(this, mpVccCalcDef, fakeop, &mOneModeModalBasisInt, mEvmp0);
   Vcc* vcc_ptr = this;
   midas::vcc::TransformerV3 v3(vcc_ptr, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   v3.TestLtransAlgos();
   //delete fakeop;
}

/**
 *
 **/
void Vcc::ConstructJacobian(VccTransformer& aTrf, MidasMatrix& aJac)
{  
   In ncoef = aTrf.NexciTransXvec() - I_1;
   DataCont unit_vec(ncoef, C_0, "InMem", "unit_vec");
   DataCont res(ncoef, C_0, "InMem", "res_vec");

   aJac.SetNewSize(ncoef, ncoef, false);
  
   Mout << "Constructing Jacobian of dimension " << ncoef << "." << endl;
   for (In i=I_0; i<ncoef; ++i)
   {
      Nb one = C_1;
      unit_vec.DataIo(IO_PUT, i, one);
     
      Nb dummy = C_0;
      aTrf.Transform(unit_vec, res, I_1, I_0, dummy);

      MidasVector res_vec(res.Size());
      res.DataIo(IO_GET, res_vec, res.Size());
      
      if (aTrf.GetType() == TRANSFORMER::VCCJAC)
      {
         aJac.AssignCol(res_vec, i);             // Righ-hand transformer -> We get columns.
      }
      else
      {
         aJac.AssignRow(res_vec, i);             // Left-hand transformer -> We get rows.
      }
      
      Nb zero = C_0;
      unit_vec.DataIo(IO_PUT, i, zero);
      Mout << "." << std::flush;
   }
   Mout << endl;
}

/**
 *
 **/
void Vcc::CmpLJacTransformers()
{
   Mout << "Comparing VCC Jacobian right-hand and left-hand transformers..." << endl;
   
   std::unique_ptr<VccTransformer> ptr_trf1(dynamic_cast<VccTransformer*>(GetRspTransformer()->Clone()));
   VccTransformer& trf1 = *ptr_trf1;
   trf1.SetType(TRANSFORMER::VCCJAC);
   
   std::unique_ptr<VccTransformer> ptr_trf2(dynamic_cast<VccTransformer*>(GetRspTransformer()->Clone()));
   VccTransformer& trf2 = *ptr_trf2;
   trf2.SetType(TRANSFORMER::LVCCJAC);
   
   MidasMatrix jac1(1);
   MidasMatrix jac2(1);

   Mout << "construct1" << endl;
   ConstructJacobian(trf1, jac1);
   Mout << "construct2" << endl;
   ConstructJacobian(trf2, jac2);

   Mout << "JAC1:" << endl << jac1;
   Mout << "JAC2:" << endl << jac2;

   Mout << endl << "Comparing elements:" << endl;
   
   bool compare_ok = true;
   for (In i=I_0; i<jac1.Nrows(); ++i)
   {
      for (In j=I_0; j<jac1.Ncols(); ++j)
      {
         Nb diff = jac1[i][j] - jac2[i][j];
         Nb rel = fabs(diff / jac1[i][j]);
         if (rel > 1.0e-13)
         {
            Mout << "(i,j) = (" << i << "," << j << "):   jac1=" << jac1[i][j]
                 << "   jac2=" << jac2[i][j] << "   diff=" << diff << "   rel=" << rel << endl;
            compare_ok = false;
         }
      }
   }
   
   if(compare_ok)
   {
      Mout << " Comparison SUCCESS! " << std::endl;
   }
   else
   {
      Mout << " Comparison FAILED :C " << std::endl;
   }
   Mout << "Comparison done." << endl;
   exit(42);
}


/**
 *
 **/
void Vcc::CalculateEta0()
{
   this->CalculateEta0Impl<DataCont>();
}

/**
 *
 **/
void Vcc::CalculateTensorEta0()
{
   this->CalculateEta0Impl<TensorDataCont>();
}


/**
 *
 **/
void Vcc::CalculateXi
   (  DataCont& arXi
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   this->CalculateXiImpl(arXi, arOperName, arOperNr, arExptValue);
}

/**
 *
 **/
void Vcc::CalculateXi
   (  TensorDataCont& arXi
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   this->CalculateXiImpl(arXi, arOperName, arOperNr, arExptValue);
}
