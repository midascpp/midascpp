/**
************************************************************************
* 
* @file                LanczosLinRspFunc.h
*
* Created:             10-08-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Lanczos linear response function class.
* 
* Last modified: Mon Jan 18, 2010  02:20PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSLINRSPFUNC_H
#define LANCZOSLINRSPFUNC_H 

#include <map>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosChain.h"
#include "vcc/NHermLanczosChain.h"
#include "vcc/BandLanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "input/RspFunc.h"

class LanczosLinRspFunc: public LanczosRspFunc
{
   protected:
      string mOper1;       ///< First operator.
      string mOper2;       ///< Second operator.
      
      bool mImInclude;     ///< If true, contributions from ImEigVals will be included
      bool mTHasImEigVal;  
      Nb mFrqStart;        ///< Start of frequency interval.
      Nb mFrqEnd;          ///< End of frequency interval.
      Nb mFrqStep;         ///< Frequency step size.
      Nb mGamma;           ///< Gamma value (half lifetime).
      In mSolver;          ///< Method for obtaining the reposne function.
      In mChainLen;        ///< Chain length to be used.
      In mOrthoChain;      ///< Orthogonalize q vector in Lanczos chain.
      
      MidasMatrix Fqq;
      MidasVector mEigVals;
      MidasVector mWeights;
      MidasMatrix mEigVecs;
      MidasMatrix mLeigVecs;
      MidasVector mImEigVals;
      MidasVector mReVals; ///< Real part of values of response function.
      MidasVector mImVals; ///< Imaginary part of values of response function.
      
      MidasVector mFContribReVals;
      MidasVector mFContribImVals;
      // Functions for initializing members from aParams. 
      void InitOperNames(const std::map<string,string>& aParams);
      void InitFrqRange(const std::map<string,string>& aParams);
      void InitGamma(const std::map<string,string>& aParams);
      void InitChainLen(const std::map<string,string>& aParams);
      void InitOrtho(const std::map<string,string>& aParams);
      void InitSolver(const std::map<string,string>& aParams);
      void InitImStatus(const std::map<string,string>& aParams);      

      void ConstructionError(const std::string aMsg, const std::map<string,string>& aParams);
      ///< Used for printing error messages during construction.
      void VccRspFctEig(const NHermLanczosChain& aChain);
      void VciRspFctLin(const LanczosChain& aChain);
      void VciRspFctEig(const LanczosChain& aChain);
      void VciRspFctCf(const LanczosChain& aChain);
      void WriteDataToFile();
      

      void WriteMatrix(const NHermLanczosChain& aChain);  
   public:
      LanczosLinRspFunc(const std::map<string,string>& aParams, const bool aBand, const bool aNHerm);
      
      vector<LanczosChainDefBase*> GetChains(string& aString) const;
      void Evaluate();

      const string& GetOper1() {return mOper1;}
      const string& GetOper2() {return mOper2;}
      
      Nb GetFrqStart() {return mFrqStart;}
      Nb GetFrqEnd()   {return mFrqEnd;}
      Nb GetFrqStep()  {return mFrqStep;}
      Nb GetGamma()    {return mGamma;}
     
      void AddReVals(MidasVector& aRe) {aRe += mReVals;}
      void AddImVals(MidasVector& aIm) {aIm += mImVals;}
      
      void AddFImContribs(MidasVector& aIm) {aIm += mFContribImVals;}
      void GetEigSolutions(MidasVector& aEigVals, MidasMatrix& aEigVecs) const;
      void GetEigSolutions(MidasVector& aWeights, MidasVector& aEigVals, MidasVector& aImEigVals, MidasMatrix& aEigVecs, MidasMatrix& aLEigVecs) const;
      ///< Get eigensolutions (and weights in vcc case) of associated chain. Used for analysis.

      void TransformStoX(const MidasVector& aS, DataCont& aX) const;
      void TransformStoX(const MidasVector& aR, const MidasVector& aL, DataCont& aRT, DataCont& aLT) const;
      ///< Transform eigensolutions of associated T to approximate eigensolutions of
      ///< full repsonse matrix. Used for analysis.
      void TestVccEigvectors(MidasMatrix& Tmatrix, MidasMatrix& LeftEigVecs, MidasMatrix& RightEigVecs,
                                          MidasVector& ReEigvals, MidasVector ImEigvals);
      
      void BiorthoEigenVecs(MidasMatrix& aSetOfLeftEigenVecs, MidasMatrix& aSetOfRightEigenVecs,MidasVector& ImEigs);
      void MakeTransformsForVcc(MidasMatrix& aReFQSTransMatrix, MidasMatrix& aImFQSTransMatrix,
                                 MidasMatrix& aSetOfRightEVecs, MidasVector& aSetOfImEigVals,
                                 const NHermLanczosChain& aChain);
      void CalculateVccResponse(MidasMatrix& aReFQSTransMatrix, MidasMatrix& aImFQSTransMatrix,
                                             Nb aFactor, Nb anotherFactor,MidasVector& aRealEtaVec, 
                                             MidasVector& aImEtaVec,MidasVector& aRealTmVec, MidasVector& aImTmVec,
                                             MidasVector& aRealTVec, MidasVector& aImTVec, Nb& aRealPart,Nb& aImPart,
                                             Nb& aFRePart, Nb& aFImPart);
      std::ostream& Print(std::ostream& aOut) const;
      void PrintResponseToStream(std::ostream&) const;

      vector<RspFunc> ConvertToRspFunc() const;
};

#endif // LANCZOSLINRSPFUNC_H
