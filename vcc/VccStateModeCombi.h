/**
************************************************************************
* 
* @file                 VccStateModeCombi.h
*
* Created:              09-05-2016
*
* Author:               Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:    References to a mode combination and its corresponding
*                       dimensions (number of modals per mode).
* 
* Last modified:        09-05-2016 (Mads Boettger Hansen)
* 
* Detailed Description: A class holding a (const reference to a) specific ModeCombi
*                       (from a ModeCombiOpRange) and a (const reference to a)
*                       vector with the dimensions of the modes (i.e. number of
*                       modals per mode, minus 1 for the reference state).
*                       Initially intended for use with the VccStateSpace
*                       class, which indeed defines the space of VCC states
*                       through the ModeCombiOpRange and the dimensions/'num.
*                       modes per mode'.
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCCSTATEMODECOMBI_H_INCLUDED
#define VCCSTATEMODECOMBI_H_INCLUDED

#include "input/ModeCombiOpRange.h"

// Forward declarations (full definition only needed in implementation file).
class VccStateSpace;    // Needed for safe, named constructors.

class VccStateModeCombi 
{
   private:
      // -----------------------------------------------------------------------
      //    Type aliases
      // -----------------------------------------------------------------------
      // Define the iterator type to be whatever is used in ModeCombiOpRange.
      using iter_type = ModeCombiOpRange::const_iterator;

      // -----------------------------------------------------------------------
      //    Private data members
      // -----------------------------------------------------------------------
      // An iterator to a ModeCombi from a ModeCombiOpRange. This is what will
      // be used to control the iterator logics in the VccStateSpace iterator.
      // Refs-to-const for the full vectors of dimensions (so that we can
      // always produce the specific MC dimensions/N_modes per mode) and the
      // addresses (so we can produce addresses and tensor sizes).
      // A mode combination reference number allows us to find the right
      // addresses.
      iter_type               mMCiter;
      const std::vector<Uin>& mrDims;
      const std::vector<Uin>& mrAddresses;
      Uin                     mRefNumber;

      // -----------------------------------------------------------------------
      //    Private member functions, constructors
      // -----------------------------------------------------------------------
      // Constructor from the relevant data member types. Private since it's
      // unsafe in the sense that it's the callers responsibility to ensure the
      // iterator and vectors match.
      VccStateModeCombi(const VccStateSpace&, iter_type&&, Uin);

   public:
      // -----------------------------------------------------------------------
      //    Constructors, etc.
      // -----------------------------------------------------------------------
      // The regular constructors, etc., some defaulted, some deleted.
      VccStateModeCombi() = delete;
      VccStateModeCombi& operator=(const VccStateModeCombi&) = delete;
      VccStateModeCombi& operator=(VccStateModeCombi&&) = delete;
      VccStateModeCombi(const VccStateModeCombi&);
      VccStateModeCombi(VccStateModeCombi&&);
      ~VccStateModeCombi();

      // Named constructors implied as static functions. Takes as argument a
      // VccStateSpace reference (which is safe) and uses it to delegate
      // construction to the private constructor.
      static VccStateModeCombi ConstructBegin(const VccStateSpace&);
      static VccStateModeCombi ConstructEnd(const VccStateSpace&);

      // -----------------------------------------------------------------------
      //    Iterator logic
      // -----------------------------------------------------------------------
      // These operators utilize the MC iterator, and are themselves utilized
      // by VccStateSpace iterators. Functionality corresponding to a
      // bidirectional iterator required (more or less).
      // Comparison operators.
      bool operator==(const VccStateModeCombi& arOther) const;
      bool operator!=(const VccStateModeCombi& arOther) const;

      // Increment/decrement operators.
      VccStateModeCombi& operator++();
      VccStateModeCombi  operator++(int);
      VccStateModeCombi& operator--();
      VccStateModeCombi  operator--(int);

      // No implementation of operators *, -> for this class because it's more
      // than just an iterator. Instead, use member access functions. (Also,
      // having an operator->() might cause problems due to drill-down
      // behaviour, making it impossible to use the VccStateSpace iterator's ->
      // to access a VccStateModeCombi.)

      // -----------------------------------------------------------------------
      //    Member access
      // -----------------------------------------------------------------------
      // The ModeCombi reference held by the object.
      const ModeCombi& MC() const;

      // Address and (tensor) size of the MC, as calculated/expected by the
      // VccStateSpace from which it originates.
      // AddressBegin: address of first (tensor) element.
      // AddressEnd:   address of first-beyond-last (tensor) element.
      Uin RefNumber() const;
      Uin AddressBegin() const;
      Uin AddressEnd() const;
      Uin TensorSize() const;
      bool ContainsAddress(Uin aAddress) const;

      // Functions for returning vectors of dimensions/#modes per mode. (Newly
      // constructed at each call, so don't call excessively.)
      // These functions won't do any out-of-range checks, since they assume
      // that the ModeCombi and the dimension/#modes references fit together
      // (which should be checked by VccStateSpace).
      std::vector<Uin> GetDims() const;
      std::vector<In>  GetNModals() const;
      std::vector<Uin> GetIndexVecFromAddress(Uin aAddr, bool aRelAddr = false) const;
};

// -----------------------------------------------------------------------------
//    Non-member functions related to class
// -----------------------------------------------------------------------------
// Strings with modes and dimensions, taking vectors of strings for formatting.
std::string StringOfModes  (const VccStateModeCombi&, const std::vector<std::string>& = {"(", ",", ")"});
std::string StringOfDims   (const VccStateModeCombi&, const std::vector<std::string>& = {"{", ",", "}"});
std::string StringOfNModals(const VccStateModeCombi&, const std::vector<std::string>& = {"{", ",", "}"});

#endif /* VCCSTATEMODECOMBI_H_INCLUDED */
