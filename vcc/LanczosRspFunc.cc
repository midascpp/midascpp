/**
************************************************************************
* 
* @file                LanczosRspFunc.cc
*
* Created:             06-08-2009
*
* Author:              Peter Seidler      (seidler@chem.au.dk) Original Implementation
*                      Ian H. Godtliebsen (ian@chem.au.dk)     Combined lanczos interface
*
* Short Description:   Basic Lanczos response function stuff.
* 
* Last modified: Tue Nov 10, 2009  04:02PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <map>
#include <algorithm>
#include <cctype>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosChain.h"
#include "vcc/LanczosLinRspFunc.h"
#include "vcc/LanczosQuadRspFunc.h"
#include "vcc/BandLanczosLinRspFunc.h"

using namespace std;

map<In, string> LanczosRspFunc::mConstStringMap;
map<string, In> LanczosRspFunc::mStringConstMap;

/**
 * Factory for generating LanczosRspFunc objects.
 * aOrder : Order of response function. 2=linear, 3=quadratic etc.
 * aParams: Map of (key,value) pairs specifying response function.
 * aBand  : Use Banded lanczos?
 * aNHerm : NHerm lanczos?
 *
 * aBand=true, aNHerm=true  : Non-hermitian banded lanczos rsp
 * aBand=true, aNHerm=false : Hermitian banded lanczos rsp
 * aBand=false, aNHerm=true : Non-hermitian lanczos rsp
 * aBand=false, aNHerm=false: Hermitian lanczos rsp
 **/
LanczosRspFunc* LanczosRspFunc::Factory(const In aOrder, 
                                        const map<string,string>& aParams, 
                                        const bool aBand, 
                                        const bool aNHerm)
{
   if(aBand)
      switch (aOrder)
      {
         case I_2:
            return new BandLanczosLinRspFunc(aParams,aBand,aNHerm);
         default:
            Mout << "LanczosRspFunc::Factory(): aOrder=" << aOrder << " not supported for banded-Lanczos." << endl;
            MIDASERROR("");
      }
   else   
      switch (aOrder)
      {
         case I_2:
            return new LanczosLinRspFunc(aParams,aBand,aNHerm);
         case I_3:
            return new LanczosQuadRspFunc(aParams,aBand,aNHerm);
         default:
            Mout << "LanczosRspFunc::Factory(): aOrder=" << aOrder << " not supported." << endl;
            MIDASERROR("");
      }
      
   return NULL; 
}

/**
 * Constructor from parameter map (ian: is this used?)
 **/
LanczosRspFunc::LanczosRspFunc(const std::map<string,string>& aParams)
{
   // Search for name parameter.
   map<string,string>::const_iterator it = aParams.find("NAME");
   if (it != aParams.end())
      mName = it->second;
}

/**
 * Constructor
 **/
LanczosRspFunc::LanczosRspFunc(const std::map<string,string>& aParams, 
                               const string aType,
                               const bool aBand, 
                               const bool aNHerm)
{
   // Search for name parameter.
   map<string,string>::const_iterator it = aParams.find("NAME");
   if (it != aParams.end())
      mName = it->second;

   mType = aType;
   mBand = aBand;
   mNHerm = aNHerm;
}

/**
 * Find lanczos chain
 **/
const LanczosChain* LanczosRspFunc::FindChain(const LanczosChainDef& aDef) const
{
   for (In i=I_0; i<mpChains->size(); ++i)
      if ((mpChains->at(i)).Compatible(aDef))
         return &(mpChains->at(i));

   Mout << " LanczosRspFunc::FindChain():" << endl
        << " Chain with definition" << endl
        << "    " << aDef << endl
        << " not found." << endl;
   MIDASERROR("");
   return NULL;
}

/**
 * Find non-hermitian lanczos chain
 **/
const NHermLanczosChain* LanczosRspFunc::FindChainNH(const NHermLanczosChainDef& aDef) const
{
   for (In i=I_0; i<mpChainsNH->size(); ++i)
   {   
       //Mout << "mpChainsNH->size() " << mpChainsNH->size() << " Itt. " << i << endl;
       if ((mpChainsNH->at(i)).Compatible(aDef))
         return &(mpChainsNH->at(i));
   }

   Mout << " LanczosRspFunc::FindChainNH():" << endl
   << " Non-Hermitian Chain with definition" << endl;
   //<< "    " << aDef << endl
   Mout << " not found." << endl;
   MIDASERROR("");
   return NULL;
}

/**
 * Find band-lanczos chain
 **/
const BandLanczosChain* LanczosRspFunc::FindChain(const BandLanczosChainDef& aDef) const
{
   for(In i=I_0; i<mpChainsBand->size(); i++)
      if((mpChainsBand->at(i)).Compatible(aDef))
         return &(mpChainsBand->at(i));

   Mout  << " LanczosRspFunc::FindChain():" << endl
         << " Band Chain with definition" << endl
         << "    " << aDef << endl
         << " not found." << endl;
   MIDASERROR("");
   return NULL;
}

/**
 * Find non-hermitian band-lanczos chain
 **/
const NHBandLanczosChain* LanczosRspFunc::FindChain(const NHBandLanczosChainDef& aDef) const
{
   for(In i=I_0; i<mpChainsNHBand->size(); i++)
      if((mpChainsNHBand->at(i)).Compatible(aDef))
         return &(mpChainsNHBand->at(i));

   Mout  << " LanczosRspFunc::FindChain():" << endl
         << " NH-Band Chain with definition" << endl
         << "    " << aDef << endl
         << " not found." << endl;
   MIDASERROR("");
   return NULL;
}

/**
 * overload for operator<<
 **/
ostream& operator<<(ostream& aOut, const LanczosRspFunc& aObj)
{
   return aObj.Print(aOut);
}

/**
 * static function for initializing "ConstStringMaps"
 **/
void LanczosRspFunc::InitConstStringMaps()
{
   mConstStringMap.insert(make_pair(LANCZOS_ERROR,      string("ERROR")));
   mConstStringMap.insert(make_pair(LANCZOS_ORTHO_NONE, string("ORTHO_NONE")));
   mConstStringMap.insert(make_pair(LANCZOS_ORTHO_FULL, string("ORTHO_FULL")));
   mConstStringMap.insert(make_pair(LANCZOS_ORTHO_PERIODIC, string("ORTHO_PERIODIC")));
   mConstStringMap.insert(make_pair(LANCZOS_ORTHO_PARTIAL, string("ORTHO_PARTIAL")));
   mConstStringMap.insert(make_pair(LANCZOS_SOLVER_LIN, string("SOLVER_LIN")));
   mConstStringMap.insert(make_pair(LANCZOS_SOLVER_EIG, string("SOLVER_EIG")));
   mConstStringMap.insert(make_pair(LANCZOS_SOLVER_CF, string("SOLVER_CF")));
   
   mStringConstMap.insert(make_pair(string("ERROR"), LANCZOS_ERROR));
   mStringConstMap.insert(make_pair(string("ORTHO_NONE"), LANCZOS_ORTHO_NONE));
   mStringConstMap.insert(make_pair(string("ORTHO_FULL"), LANCZOS_ORTHO_FULL));
   mStringConstMap.insert(make_pair(string("ORTHO_PERIODIC"), LANCZOS_ORTHO_PERIODIC));
   mStringConstMap.insert(make_pair(string("ORTHO_PARTIAL"), LANCZOS_ORTHO_PARTIAL));
   mStringConstMap.insert(make_pair(string("SOLVER_LIN"), LANCZOS_SOLVER_LIN));
   mStringConstMap.insert(make_pair(string("SOLVER_EIG"), LANCZOS_SOLVER_EIG));
   mStringConstMap.insert(make_pair(string("SOLVER_CF"), LANCZOS_SOLVER_CF));
}

/**
 * Get Constant from string
 **/
In LanczosRspFunc::ConstForString(const string& aS)
{
   string s(aS);
   transform(s.begin(), s.end(), s.begin(), static_cast<int (*)(int)>(toupper));
   map<string,In>::iterator it = mStringConstMap.find(s);
   if (it == mStringConstMap.end())
      return LANCZOS_ERROR;
   else
      return it->second;
}

/**
 * Get String from Constant
 **/
const string& LanczosRspFunc::StringForConst(In aC)
{
   map<In,string>::iterator it = mConstStringMap.find(aC);
   if (it == mConstStringMap.end())
      return mConstStringMap[LANCZOS_ERROR];
   else
      return it->second;
}
