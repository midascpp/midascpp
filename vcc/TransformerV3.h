/**
 ************************************************************************
 * 
 * @file                TransformerV3.h
 *
 * Created:             30-09-2008
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Doing transformations using intermediates.
 * 
 * Last modified: Mon May 17, 2010  12:30PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef TRANSFORMERV3_H_INCLUDED
#define TRANSFORMERV3_H_INCLUDED

#include <string>
#include <vector>
#include <set>
#include <math.h>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/VccTransformer.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/LaplaceQuadrature.h"

// Forward declarations
template<class T>
class LaplaceQuadrature;

namespace midas::vcc
{

namespace v3
{
template
   <  typename T
   >
class VccEvalData;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class V3Contrib;

template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
class IntermediateMachine;
} /* namespace v3 */

namespace detail
{
/**
 *
 **/
class CallStat
{
   private:
      Timer mTimer;
      In    mCalls;
      Nb    mCpuTime;
      Nb    mWallTime;

      bool  mRunning;

   public:
      CallStat(): mTimer(), mCalls(I_0), 
            mCpuTime(C_0), mWallTime(C_0), mRunning(false)
      { }
      
      void Start() { 
         if(!mRunning) { mTimer.Reset(); ++mCalls; mRunning=true; } 
         else          { Mout << " CALLSTAT already running " << std::endl; } 
      }
      void Stop () { 
         if(mRunning) { mCpuTime+=mTimer.CpuTime(); mWallTime+=mTimer.WallTime(); mRunning=false; }
         else         { Mout << " CALLSTAT not running " << std::endl; } 
      }
      
      Nb TotCpuTime()   const { return mCpuTime; } 
      Nb TotWallTime()  const { return mWallTime; }

      Nb CpuTimePerCall()  const { return mCpuTime/Nb(mCalls); }
      Nb WallTimePerCall() const { return mWallTime/Nb(mCalls); }

      In Calls()           const { return mCalls; }

      void Reset() { mCalls=I_0; mCpuTime=C_0; mWallTime=C_0; }
};

} /* namespace detail */

/**
 *
 **/
class TransformerV3
   :  public VccTransformer
{
   private:
      using decompinfo_t = ::midas::tensor::TensorDecompInfo;
      using decompinfo_set_t = decompinfo_t::Set;
      using laplaceinfo_t = ::midas::tensor::LaplaceInfo;

      using param_t = Nb;

      using evaldata_t = midas::vcc::v3::VccEvalData<param_t>;

      using v3contrib_t = midas::vcc::v3::V3Contrib<param_t, midas::vcc::v3::VccEvalData>;

      // Copying is dangerous since we have a vector of pointers.
      // Could be done if necessary.
      TransformerV3(const TransformerV3&) = delete;
      TransformerV3& operator=(const TransformerV3&) = delete;
      
      void PrintType(std::ostream&)           const;
      void ScreeningOutput(std::ostream&)     const;
      void TransformStatOutput(std::ostream&) const;

      std::vector<std::unique_ptr<v3contrib_t > > mContribs;   ///< Contributions to be evaluated.
      std::unique_ptr<v3::IntermediateMachine<param_t, GeneralMidasVector, v3::VccEvalData> > mImMachine;  ///< Intermediate machine to hold saved intermediates
      std::unique_ptr<v3::IntermediateMachine<param_t, NiceTensor, v3::VccEvalData> > mTensorImMachine;  ///< Intermediate machine to hold saved intermediates for NiceTensor calculations

      //! Ints. over the Fock oper. For VCC with pert. treatment of higher exc. levels.
      ModalIntegrals<param_t> mFockInts;
      
      MidasMatrix     mOutputStatus;  ///<Matrix needed for checking output status, so we dont flood output
      detail::CallStat mTransformStat; ///<Get timings on calls to Transform()
      In              mMaxExciLevel;

      //! Name of file containing the VCC amplitudes used for initializing Jacobian transformers
      std::string mAmplitudeFile;

      decompinfo_set_t mDecompInfo; ///< decomp info for tensor transformer.
      In mAllowedRank = I_0;                         ///< Allowed rank for TensorSums in tensor decomp transformer.
      bool mTensorTransform = false;    ///< Do transforms with tensor framework

      //! Screening thresholds for tensor products
      std::pair<param_t, param_t> mTensorProductScreening = std::make_pair(C_0, C_0);

      //! Adapt the screening to the decomposition thresholds (negative value turns it off)
      param_t mAdaptiveTensorProductScreening = -C_1;

      //! Low-rank limit for constructing direct products in CP format
      In mTensorProductLowRankLimit = I_0;

      //! Use scalings for CP-VCC contractions in determining intermediates
      bool mUseCpScalingForIntermeds = false;

      std::unique_ptr<Transformer> mPreDiagTransformer = std::unique_ptr<Transformer>(nullptr);
   
      bool CheckOutputStatus(In aType, In aV3Type);   ///<A function checking if we need to output 
                                                      ///<information of what the transformer is doing.
      
      void ChangeOutputStatus(In aType, In aV3Type);  ///<A function for changing the outputstatus of a given method.
   
      static std::set<std::string> mTestedMethods;
      ///< List of methods that have been tested and are believed to work.
      ///< Recognition of a method is based on the filename of the corresponding .v3c file.
      
      void InitTestedMethods();
      bool CheckMethod(const std::string& aFilename) const; 
   
      void CtrProdsToV3(vector<CtrProd>& aProds, bool aVerbose=true);
      ///< Translate vector of contraction products to V3 intermediate products.
   
      // Operations on Commutator vectors
      void DeleteCommutatorInVec(const vector<Commutator>& aCmts1, vector<Commutator>& aCmts2);
      ///< erase the elements of the vector aCmts1 from the vector aCmts2
      
      void IdentifyCmbIntermeds(bool aVerbose=true);
      ///< Identify intermediates for all contributions.

      //! Register interface
      void RegisterIntermeds
         (
         );

      //! List registered intermeds
      void ListRegisteredIntermeds
         (
         )  const;

      void BasicVccRspInit(const std::string&); ///< Initialization common to all VCC rsp. calcs.
      void BasicVccRspInit(const TensorDataCont&); ///< Initialization common to all VCC rsp. calcs.
      void ListContribs();              ///< Write list of contributions.
      void ClearContribs();             ///< Clear list of contributions.
      
      // Initialization of VCI transformer.
      void InitGenVci();
    
      // Initialization of VCC error vector transformers.
      void InitGenVcc();
      void InitVCC1pt2();
      void InitVCC2();
      void InitVCC2pt3();
      void InitVCC3pt4F();
      void InitVCC3pt4();
     
      // Initialization of VCC Jacobian transformers. 
      void InitGenVccJacobian(const std::string&);
      void InitVCC1pt2Jacobian(const std::string&);
      void InitVCC2Jacobian(const std::string&);
      void InitVCC2pt3Jacobian(const std::string&);
      void InitVCC3pt4FJacobian(const std::string&);
      void InitVCC3pt4Jacobian(const std::string&);

      // Initialize from amplitudes in memory
      void InitGenVccJacobian(const TensorDataCont&);
      void InitVCC1pt2Jacobian(const TensorDataCont&);
      void InitVCC2Jacobian(const TensorDataCont&);
      void InitVCC2pt3Jacobian(const TensorDataCont&);
      void InitVCC3pt4FJacobian(const TensorDataCont&);
      void InitVCC3pt4Jacobian(const TensorDataCont&);

      // Do the things common to the two other initialization functions (filename vs. TensorDataCont)
      void FinalizeInitGenVccJacobian();
      void FinalizeInitVCC1pt2Jacobian();
      void FinalizeInitVCC2Jacobian();
      void FinalizeInitVCC2pt3Jacobian();
      void FinalizeInitVCC3pt4FJacobian();
      void FinalizeInitVCC3pt4Jacobian();
     
      // Initialization of left-hand VCC Jacobian transformers. 
      void InitGenVccJacobianL(const std::string&);
      void InitVCC2pt3JacobianL(const std::string&);
      void InitVCC3pt4FJacobianL(const std::string&);
      void InitVCC3pt4JacobianL(const std::string&);
   
      // Initialization of further VCC response related transformers.
      void InitGenVccRspEta0();
      void InitGenVccRspEta0(const std::string&);
      void InitVccRspB();
      void InitVccRspB(const std::string&);
      void InitGenVccRspB(const std::string&);
      void InitVCC2pt3RspB(const std::string&);
      void InitVCC3pt4FRspB(const std::string&);
      void InitVCC3pt4RspB(const std::string&);
      void InitVccRspFR();
      void InitVccRspFR(const std::string&);
      void InitGenVccRspFR(const std::string&);
      void InitVCC2pt3RspFR(const std::string&);
      void InitVCC3pt4FRspFR(const std::string&);
      void InitVCC3pt4RspFR(const std::string&);
      
      // The actual transformer functions. 
      void HTrans(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t aNb);
      void VciRspTrans(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t& aNb);
      void VccTrans(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t& aNb);
      void VccTransJac(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t& aNb);
      void VccTransRspB(DataCont& arDcInR, DataCont& arDcInS, DataCont& arDcOut, param_t& aRefOut);
      void VccTransRspB
         (  const TensorDataCont& arInR
         ,  const TensorDataCont& arInS
         ,  TensorDataCont& arOut
         ,  param_t& aRefOut
         );
      
      // the actual transformer function. TensorDataCont
      void VccTrans
         (  const TensorDataCont& arDcIn
         ,  TensorDataCont& arDcOut
         ,  In aI
         ,  In aJ
         ,  param_t& aNb
         ,  const std::vector<param_t>& arDecompThresholds = {}
         );
      void VccTransJac
         (  const TensorDataCont& arDcIn
         ,  TensorDataCont& arDcOut
         ,  In aI
         ,  In aJ
         ,  param_t& aNb
         ,  const std::vector<param_t>& arDecompThresholds = {}
         ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad = {}
         );
    
      void EvaluateContribs(evaldata_t& aEvalData, DataCont& aRes);
      ///< Function for evaluating all the contributions in mContribs.
      
      void EvaluateContribsNew
         (  evaldata_t& aEvalData
         ,  DataCont& aRes
         );
      ///< Function for evaluating all the contributions in mContribs. Loop over MCs first.
      
      void EvaluateContribs(evaldata_t& aEvalData, TensorDataCont& aRes);
      ///< Function for evaluating all the contributions in mContribs.
      
      void EvaluateContribsNew
         (  evaldata_t& aEvalData
         ,  TensorDataCont& aRes
         ,  const std::vector<param_t>& arDecompThresholds = {}
         );
      ///< Function for evaluating all the contributions in mContribs. Loop over MCs first.
   
      param_t CalcVccRspFRSNum(DataCont& arDcInR, DataCont& arDcInS);
      ///< Calculate F*R*S using F matrix calculated numerically.
         
   public:
      //! Constructors
      TransformerV3
         (  const VccTransformer&
         ,  bool = false
         );

      TransformerV3
         (  const TransformerV3& aVccTrf
         ,  In aPreconLevel
         );

      TransformerV3
         (  const TransformerV3& aVccTrf
         ,  In aPreconLevel
         ,  const TensorDataCont& arAmplitudes
         );

      TransformerV3
         (  Vcc* apVcc
         ,  VccCalcDef* apVccCalcDef
         ,  OpDef* apOpDef
         ,  const ModalIntegrals<param_t>* const apIntegralsOrig
         ,  param_t aEvmp0
         ,  In aISO=I_0
         ,  bool aRspTrf = false
         );

      /*********************************************************************
       * The basic Transformer interface.
       *********************************************************************/
      void Transform
         (  DataCont& aIn
         ,  DataCont& aOut
         ,  In aI
         ,  In aJ
         ,  param_t& aNb
         ); ///< transform DataCont
      void Transform
         (  const TensorDataCont& aIn
         ,  TensorDataCont& aOut
         ,  In aI
         ,  In aJ
         ,  param_t& aNb
         ,  const std::vector<param_t>& arDecompThresholds = {}
         ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad = {}   // LaplaceQuadrature for 0th-order Jacobian transform
         ); ///< transform TensorDataCont

      //! Transform with 0th-order Jacobian diagonal (or inverse)
      GeneralTensorDataCont<param_t> TensorTransformerA0
         (  const GeneralTensorDataCont<param_t>&
         ,  const In&
         ,  const param_t& arNb=C_0
         ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad = {}
         );

      // Generate LaplaceQuadratures before transform
      GeneralTensorDataCont<param_t> TensorTransformerA0
         (  const GeneralTensorDataCont<param_t>&
         ,  const In&
         ,  const param_t&
         ,  const laplaceinfo_t&
         );


      void PreparePreDiag();
      void PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, param_t& aNb); 
      void RestorePreDiag() {}
      
      void SetType(TRANSFORMER aType);
      void SetType(TRANSFORMER aType, const std::string&); // enum with TRANSFORMERs is placed in VccTransformer.h
      ///< Set the type of transformer. This will initialize the
      ///< list of contributions as well.

      //! Set transformer to VCCJAC from amplitudes in TensorDataCont format.
      //, Used for Newton-Raphson in TensorNlSolver
      void SetToVccJac(const TensorDataCont&);
   
      void CalcVccRspEta0(DataCont& aRes);
      ///< Calculate VCC eta0 vector = <ref| [H_0, tau_nu] |VCC>.

      void CalcVccRspEta0(TensorDataCont& aRes);
      ///< Calculate VCC eta0 vector = <ref| [H_0, tau_nu] |VCC>.
      
      //! Calculate F*R*S where F=<Lambda| [[H_0, tau_nu], tau_mu] |VCC>.
      param_t CalcVccRspFRS
         (  DataCont& arDcInMult
         ,  DataCont& arDcInR
         ,  DataCont& arDcInS
         );

      //! Calculate F*R*S where F=<Lambda| [[H_0, tau_nu], tau_mu] |VCC>.
      param_t CalcVccRspFRS
         (  const TensorDataCont& arMult
         ,  const TensorDataCont& arR
         ,  const TensorDataCont& arS
         );
      
      void CalcVccRspFR
         (  DataCont& arDcInMults
         ,  DataCont& arDcInR
         ,  DataCont& arDcOut
         );
      ///< Calculate F*R vector where F=<Lambda| [[H_0, tau_nu], tau_mu] |VCC>.

      void CalcVccRspFR
         (  const TensorDataCont& arMults
         ,  const TensorDataCont& arR
         ,  TensorDataCont& arOut
         );
      ///< Calculate F*R vector where F=<Lambda| [[H_0, tau_nu], tau_mu] |VCC>.
   
      void TestLtransAlgos();
      ///< Test the two different algorithms for doing left-hand transformations against each other.
      
      std::ostream& Print(std::ostream& ostream) const;
      ///< Print out transformer

      virtual Transformer* Clone() const;
      ///< Clone transformer
      
      virtual Transformer* CloneImprovedPrecon(In aPreconLevel) const;
      ///< Clone transformer for improved preconditioner
      
      virtual std::string VccType() const { return "TransformerV3"; }

      ///> Update the tensor decomposition thresholds
      void UpdateDecompThresholds(const param_t&);

      ///> Get the (largest) decomposition threshold in mDecompInfo
      param_t GetMaxDecompThreshold() const;

      //! Get max exci level
      const In& GetMaxExciLevel() const { return this->mMaxExciLevel; }

      //! Get the tensor types (SIMPLE or CANONICAL) from mDecompInfo. Throw error if mixed!
      BaseTensor<param_t>::typeID GetTensorType() const;

      //! Get/Set the decompinfo set
      const decompinfo_set_t&  GetDecompInfo() const   { return mDecompInfo; }
      void SetDecompInfo(const decompinfo_set_t& aInfo) { this->mDecompInfo = aInfo; }

      //! Get allowed rank
      In GetAllowedRank() const { return mAllowedRank; }

      //! Get tensor-product screening
      const std::pair<param_t, param_t>& TensorProductScreening() const { return mTensorProductScreening; }

      //! Get tensor-product low-rank limit
      In TensorProductLowRankLimit() const { return mTensorProductLowRankLimit; }

      //! Get/Set mAmplitudeFile
      //!@{
      void SetAmplitudeFile(const std::string& aF) { mAmplitudeFile = aF; }
      const std::string& GetAmplitudeFile() const  { return mAmplitudeFile; }
      //!@}
      
      //! Get ImMachines
      auto& GetImMachine() { return *mImMachine; }
      auto& GetTensorImMachine() { return *mTensorImMachine; }

      //! Are we using tensors?
      bool TensorTransform()  const       { return mTensorTransform; }
         
};

} /* namespace midas::vcc */

#endif  /* TRANSFORMERV3_H_INCLUDED */
