/**
************************************************************************
* 
* @file    Vcc_Impl.h
*
* @date    26-01-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of VCC template functions.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef VCC_IMPL_H_INCLUDED
#define VCC_IMPL_H_INCLUDED

#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"
#include "vcc/TransformerV3.h"
#include "vcc/VccStateSpace.h"

/**
* Calculate the Vcc eta0 vector 
* */
template
   <  class VEC_T
   >
void Vcc::CalculateEta0Impl()
{
   Mout << " Calculating VCC eta0 vector = <ref| [H_0, tau_nu] |VCC>." << endl;

   // Declare pointer to vec
   std::unique_ptr<VEC_T> eta_vec = nullptr;
   std::string name = mpVccCalcDef->GetName() + "_eta0_vec";

   // Initialize vector
   // If DataCont
   if constexpr   (  std::is_same_v<VEC_T, DataCont>
                  )
   {
      string storage="OnDisc";
      eta_vec = std::make_unique<DataCont>(NrspPar(), C_0, storage, name);  // TEST PUT TO ONES 
      eta_vec->SaveUponDecon(true);
   }
   // If TensorDataCont
   else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                     )
   {
      // Construct VccStateSpace
      std::vector<In> nmodals;
      this->pVccCalcDef()->Nmodals(nmodals);
      VccStateSpace vss(mRspTrf->GetXvecModeCombiOpRange(), nmodals, true); // exclude ref

      // Get tensor type ID
      auto type_id   =  this->pVccCalcDef()->GetVccRspDecompInfo().empty()
                     ?  BaseTensor<Nb>::typeID::SIMPLE
                     :  BaseTensor<Nb>::typeID::CANONICAL;

      // Initialize eta_vec
      eta_vec = std::make_unique<TensorDataCont>(vss, type_id);
   }
   // Else: error
   else
   {
      auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
      MIDASERROR("Vcc::CalculateEta0 not implemented for type: " + type);
   }
 
   // Do transform
   mRspTrf->SetType(TRANSFORMER::SPECIAL); 
   mRspTrf->CalcVccRspEta0(*eta_vec);

   // If TensorDataCont, we need to write it explicitly
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      eta_vec->WriteToDisc(name + "_tensor", midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
   }

   // Check norm
   Mout  << " Norm of eta vector:   " << eta_vec->Norm() << std::endl;
}


/**
 * Calculate the Vcc Xi vector 
 **/
template
   <  class VEC_T
   >
void Vcc::CalculateXiImpl
   (  VEC_T& arXi
   ,  const std::string& arOperName
   ,  In& arOperNr
   ,  Nb& arExptValue
   )
{
   Mout << std::endl
        << " Calculation of VCC xi vector = <mu| exp(-T) X |VCC>" << std::endl
        << " and <ref|X|VCC> expectation value for operator "
        << arOperName << std::endl;

   // Check that operator arguments are consistent.
   In i_oper=gOperatorDefs.GetOperatorNr(arOperName);
   if (  i_oper == -I_1
      )
   {
      Mout << " The operator with label " << arOperName << " is not found." << std::endl;
      Mout << " The available operators are:" << std::endl;
      for (In i=I_0; i<gOperatorDefs.GetNrOfOpers(); ++i)
      {
         Mout << " " << gOperatorDefs[i].Name() << std::endl;
      }

      MIDASERROR("Operator could not be found." );
   }

   if (  i_oper != arOperNr
      )
   {
      MIDASERROR("Operator check failed in Vcc::CalculateXi()");
   }

   std::string basis_name = mpVccCalcDef->Basis();
   In i_basis = I_0;
   for(In i=I_0; i<gBasis.size(); ++i)
   {
      if (  gBasis[i].GetmBasName() == basis_name
         )
      {
         i_basis = i;
      }
   }
   
   // Calculate integrals.
   bool save = true;
   std::string storage = "InMem";
   if (  mpVccCalcDef->IntStorage() != I_0
      )
   {
      storage = "OnDisc";
   }
   OneModeInt one_mode_int(&gOperatorDefs[i_oper], &gBasis[i_basis], storage, save);
   one_mode_int.CalcInt();
 
   // Change operator. 
   this->PutOperatorToWorkingOperator(&gOperatorDefs[i_oper], &one_mode_int);
   this->mRspTrf->SetOpDef(this->pOpDef());
   
   // Vector is to be transformed now 
   std::unique_ptr<VEC_T> amps = nullptr;
   std::string s = mpVccCalcDef->GetName();

   In nvecsize = NrspPar();
   Nb a_nb=C_1;
   if constexpr   (  std::is_same_v<VEC_T, DataCont>
                  )
   {
      // Check size
      if (  nvecsize != arXi.Size()
         )
      {
         MIDASERROR("In VCC::CalculateXi() size mismatch.");
      }

      // Setup DataCont amplitudes
      amps = std::make_unique<DataCont>();

      // Get VCC amplitudes for ground state.
      amps->GetFromExistingOnDisc(nvecsize, s + "_Vcc_vec_0");
      amps->SaveUponDecon();

      mRspTrf->SetType(TRANSFORMER::VCC);                // Use std. VCC transformer with new operator.
      mRspTrf->Transform(*amps, arXi, I_1, I_0, a_nb); 
   }
   else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                     )
   {
      // Check size
      auto tot_size = arXi.TotalSize();
      if (  nvecsize != tot_size
         )
      {
         MIDASERROR("In VCC::CalculateXi() size mismatch for TensorDataCont. N_params = " + std::to_string(nvecsize) + ", TensorDataCont::TotalSize() = " + std::to_string(tot_size));
      }
      amps = std::make_unique<TensorDataCont>();
      amps->ReadFromDisc(s + "_Vcc_vec_tensor_0_0");

      dynamic_cast<midas::vcc::TransformerV3*>(this->mRspTrf.get())->SetType(TRANSFORMER::VCC);                // Use std. VCC transformer with new operator.
      dynamic_cast<midas::vcc::TransformerV3*>(this->mRspTrf.get())->Transform(*amps, arXi, I_1, I_0, a_nb); 
   }
   // Else: error
   else
   {
      auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
      MIDASERROR("Vcc::CalculateXi not implemented for type: " + type);
   }

   // Change operator back to original one.
   RestoreOrigOper();
   mRspTrf->SetOpDef(pOpDef());
   
   // Set expectation value
   arExptValue = a_nb;

   Mout << " Size of reference CC space: " << nvecsize << std::endl; 
   Mout << " Size of amplitude vector:   " << amps->Size() << std::endl; 
   Mout << " Size of xi vector:          " << arXi.Size() << std::endl; 
   Mout << " Norm of amplitude vector:   " << amps->Norm() << std::endl;
   Mout << " Norm of xi vector:          " << arXi.Norm() << std::endl;
   Mout.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(16, Mout);
   Mout << " <HF|X|CC> =                 " << arExptValue << std::endl;
   Mout << " Xi calculation is done." << std::endl;
}


/**
 * Calculate VCC etaX vector: <Lambda| [X,tau_nu] |VCC>.
 **/
template
   <  class VEC_T
   >
void Vcc::CalculateVccEtaX
   (  VEC_T& arEta
   ,  const std::string& arOperName
   ,  In aOperNr
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");


   Mout << " Calculation of VCC etaX vector = <Lambda| [X,tau_nu] |VCC>"
        << " for operator X=" << arOperName << "." << std::endl;

   // Check that operator arguments are consistent.
   In i_oper = gOperatorDefs.GetOperatorNr(arOperName);
   if (  i_oper == -I_1
      )
   {
      Mout << " The operator with label " << arOperName << " is not found." << std::endl;
      Mout << " The available operators are:" << std::endl;
      for (In i=I_0; i<gOperatorDefs.GetNrOfOpers(); ++i)
      {
         Mout << " " << gOperatorDefs[i].Name() << endl;
      }
      MIDASERROR("Operator could not be found.");
   }
   if (  i_oper != aOperNr
      )
   {
      MIDASERROR("Operator check failed in Vcc::CalculateVccEtaX()");
   }

   // Find basis set
   const auto& basis_name = mpVccCalcDef->Basis();
   In i_basis = 0;
   for (In i=0; i<gBasis.size(); ++i)
   {
      if (  gBasis[i].GetmBasName() == basis_name
         )
      {
         i_basis = i;
      }
   }
   
   // Calculate integrals.
   bool save = true;
   std::string storage = "InMem";
   if (  mpVccCalcDef->IntStorage() != I_0
      )
   {
      storage = "OnDisc";
   }

   OneModeInt one_mode_int(&gOperatorDefs[i_oper], &gBasis[i_basis], storage, save);
   one_mode_int.CalcInt();
 
   // Change operator. 
   this->PutOperatorToWorkingOperator(&gOperatorDefs[i_oper], &one_mode_int);
   this->mRspTrf->SetOpDef(pOpDef());
   
   //  Calculate <ref| [X,tau_mu] |VCC>.
   std::unique_ptr<VEC_T> eta0 = nullptr;
   if constexpr   (  is_dc
                  )
   {
      auto nvecsize = NrspPar();

      eta0 = std::make_unique<DataCont>(nvecsize, C_0);
   }
   else if constexpr (  is_tensor
                     )
   {
      // Construct VccStateSpace
      std::vector<In> nmodals;
      this->pVccCalcDef()->Nmodals(nmodals);
      VccStateSpace vss(mRspTrf->GetXvecModeCombiOpRange(), nmodals, true); // exclude ref

      // Get tensor type ID
      auto type_id   =  this->pVccCalcDef()->GetVccRspDecompInfo().empty()
                     ?  BaseTensor<Nb>::typeID::SIMPLE
                     :  BaseTensor<Nb>::typeID::CANONICAL;

      // Initialize eta_vec
      eta0 = std::make_unique<TensorDataCont>(vss, type_id);
   }

   this->mRspTrf->SetType(TRANSFORMER::SPECIAL); 
   this->mRspTrf->CalcVccRspEta0(*eta0);

   // Get zeroth order multipliers from disc.
   auto mults = std::make_unique<VEC_T>();
   std::string vecname = mpVccCalcDef->GetName() + "_mul0_vec";
   if constexpr   (  is_dc
                  )
   {
      auto nvecsize = NrspPar();
      vecname += "_0";
      mults->GetFromExistingOnDisc(nvecsize, vecname);
      mults->SaveUponDecon(true);
   }
   else if constexpr (  is_tensor
                     )
   {
      vecname += "_tensor_0";
      mults->ReadFromDisc(vecname);
   }

   // Calculate tbar_nu * <nu| exp(-T) [X, tau_mu] exp(T) |ref>."
   this->mRspTrf->SetType(TRANSFORMER::LVCCJAC);
   Nb dummy = C_0;

   if constexpr   (  is_dc
                  )
   {
      this->mRspTrf->Transform(*mults, arEta, I_1, I_0, dummy);
   }
   else if constexpr (  is_tensor
                     )
   {
      dynamic_cast<midas::vcc::TransformerV3*>(mRspTrf.get())->Transform(*mults, arEta, I_1, I_0, dummy);
   }
  
   arEta.Axpy(*eta0, C_1);
  
   Mout << "Change operator back to original one." << std::endl;
   this->RestoreOrigOper();
   this->mRspTrf->SetOpDef(pOpDef());
}


#endif /* VCC_IMPL_H_INCLUDED */
