#ifndef DIRPRODSPECIAL_IMPL_H_INCLUDED
#define DIRPRODSPECIAL_IMPL_H_INCLUDED

#include "input/ModeCombi.h"
#include "tensor/SimpleTensor.h"


namespace midas::vcc::dirprod
{

/**
 * Check whether all modes in one mc (aMc1) are less than the modes of another (aMc2).
 * Used to check whether we can do a specially optimized direct product.
 *
 * @param aMc1                         ModeCombi to check.
 * @param aMc2                         ModeCombi to check against.
 * @return                             True if all modes in aMc1 are less than all modes in aMc2. 
 **/
inline bool ModesStrictlyLessThan
   (  const ModeCombi& aMc1
   ,  const ModeCombi& aMc2
   )
{
   for(int i = 0; i < aMc1.Size(); ++i)
   {
      for(int j = 0; j < aMc2.Size(); ++j)
      {
         if (  aMc1.Mode(i) >= aMc2.Mode(j)  // Niels: For LVCCJAC, the MCs can be equal
            )
         {
            return false;
         }
      }
   }
   return true;
}

/**
 * Special direct product for 2 SimpleTensor, where the MC for one is Strictly
 * less than the other.
 *
 * @param aRes                            Result of direct product.
 * @param aInTensor0                      Tensor0 to be direct product'ed together.
 * @param aInTensor1                      Tensor1 to be direct product'ed together.
 * @param aCoef                           Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecial2
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();

   auto sz0 = aInTensor0.TotalSize();
   auto sz1 = aInTensor1.TotalSize();
   
   T* in_ptr0_inner;
   T* in_ptr1_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < sz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < sz1; ++i1)
      {
         (*result_ptr) += aCoef* (*in_ptr0_inner)* (*in_ptr1_inner);
         ++result_ptr;
         ++in_ptr1_inner;
      }
      ++in_ptr0_inner;
   }
}

/**
 * Special direct product for 3 SimpleTensor, where the MC for one is Strictly
 * less than the other.
 *
 * @param aRes                            Result of direct product.
 * @param aInTensor0                      Tensor0 to be direct product'ed together.
 * @param aInTensor1                      Tensor1 to be direct product'ed together.
 * @param aInTensor2                      Tensor2 to be direct product'ed together.
 * @param aCoef                           Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecial3
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const NiceTensor<T>& aInTensor2
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();
   T* in_ptr2 = static_cast<SimpleTensor<T>* >(aInTensor2.GetTensor())->GetData();

   auto sz0 = aInTensor0.TotalSize();
   auto sz1 = aInTensor1.TotalSize();
   auto sz2 = aInTensor2.TotalSize();
   
   T* in_ptr0_inner;
   T* in_ptr1_inner;
   T* in_ptr2_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < sz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < sz1; ++i1)
      {
         in_ptr2_inner = in_ptr2;
         for (unsigned int i2 = 0; i2 < sz2; ++i2)
         {
            (*result_ptr) += aCoef* (*in_ptr0_inner)* (*in_ptr1_inner)* (*in_ptr2_inner);
            ++result_ptr;
            ++in_ptr2_inner;
         }
         ++in_ptr1_inner;
      }
      ++in_ptr0_inner;
   }
}

/**
 * Special direct product for 4 SimpleTensor, where the MC for one is Strictly
 * less than the other.
 *
 * @param aRes                            Result of direct product.
 * @param aInTensor0                      Tensor0 to be direct product'ed together.
 * @param aInTensor1                      Tensor1 to be direct product'ed together.
 * @param aInTensor2                      Tensor2 to be direct product'ed together.
 * @param aInTensor3                      Tensor3 to be direct product'ed together.
 * @param aCoef                           Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecial4
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const NiceTensor<T>& aInTensor2
   ,  const NiceTensor<T>& aInTensor3
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();
   T* in_ptr2 = static_cast<SimpleTensor<T>* >(aInTensor2.GetTensor())->GetData();
   T* in_ptr3 = static_cast<SimpleTensor<T>* >(aInTensor3.GetTensor())->GetData();

   auto sz0 = aInTensor0.TotalSize();
   auto sz1 = aInTensor1.TotalSize();
   auto sz2 = aInTensor2.TotalSize();
   auto sz3 = aInTensor3.TotalSize();
   
   T* in_ptr0_inner;
   T* in_ptr1_inner;
   T* in_ptr2_inner;
   T* in_ptr3_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < sz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < sz1; ++i1)
      {
         in_ptr2_inner = in_ptr2;
         for (unsigned int i2 = 0; i2 < sz2; ++i2)
         {
            in_ptr3_inner = in_ptr3;
            for (unsigned int i3 = 0; i3 < sz3; ++i3)
            {
               (*result_ptr) += aCoef* (*in_ptr0_inner)* (*in_ptr1_inner)* (*in_ptr2_inner)* (*in_ptr3_inner);
               ++result_ptr;
               ++in_ptr3_inner;
            }
            ++in_ptr2_inner;
         }
         ++in_ptr1_inner;
      }
      ++in_ptr0_inner;
   }
}

/**
 * Special direct product for 5 SimpleTensor, where the MC for one is Strictly
 * less than the other.
 *
 * @param aRes                            Result of direct product.
 * @param aInTensor0                      Tensor0 to be direct product'ed together.
 * @param aInTensor1                      Tensor1 to be direct product'ed together.
 * @param aInTensor2                      Tensor2 to be direct product'ed together.
 * @param aInTensor3                      Tensor3 to be direct product'ed together.
 * @param aInTensor4                      Tensor4 to be direct product'ed together.
 * @param aCoef                           Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecial5
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const NiceTensor<T>& aInTensor2
   ,  const NiceTensor<T>& aInTensor3
   ,  const NiceTensor<T>& aInTensor4
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();
   T* in_ptr2 = static_cast<SimpleTensor<T>* >(aInTensor2.GetTensor())->GetData();
   T* in_ptr3 = static_cast<SimpleTensor<T>* >(aInTensor3.GetTensor())->GetData();
   T* in_ptr4 = static_cast<SimpleTensor<T>* >(aInTensor4.GetTensor())->GetData();

   auto sz0 = aInTensor0.TotalSize();
   auto sz1 = aInTensor1.TotalSize();
   auto sz2 = aInTensor2.TotalSize();
   auto sz3 = aInTensor3.TotalSize();
   auto sz4 = aInTensor4.TotalSize();
   
   T* in_ptr0_inner;
   T* in_ptr1_inner;
   T* in_ptr2_inner;
   T* in_ptr3_inner;
   T* in_ptr4_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < sz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < sz1; ++i1)
      {
         in_ptr2_inner = in_ptr2;
         for (unsigned int i2 = 0; i2 < sz2; ++i2)
         {
            in_ptr3_inner = in_ptr3;
            for (unsigned int i3 = 0; i3 < sz3; ++i3)
            {
               in_ptr4_inner = in_ptr4;
               for (unsigned int i4 = 0; i4 < sz4; ++i4)
               {
                  (*result_ptr) += aCoef* (*in_ptr0_inner)* (*in_ptr1_inner)* (*in_ptr2_inner)* (*in_ptr3_inner)* (*in_ptr4_inner);
                  ++result_ptr;
                  ++in_ptr4_inner;
               }
               ++in_ptr3_inner;
            }
            ++in_ptr2_inner;
         }
         ++in_ptr1_inner;
      }
      ++in_ptr0_inner;
   }
}

/**
 * Special direct product for 6 SimpleTensor, where the MC for one is Strictly
 * less than the other.
 *
 * @param aRes                            Result of direct product.
 * @param aInTensor0                      Tensor0 to be direct product'ed together.
 * @param aInTensor1                      Tensor1 to be direct product'ed together.
 * @param aInTensor2                      Tensor2 to be direct product'ed together.
 * @param aInTensor3                      Tensor3 to be direct product'ed together.
 * @param aInTensor4                      Tensor4 to be direct product'ed together.
 * @param aInTensor5                      Tensor5 to be direct product'ed together.
 * @param aCoef                           Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecial6
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const NiceTensor<T>& aInTensor2
   ,  const NiceTensor<T>& aInTensor3
   ,  const NiceTensor<T>& aInTensor4
   ,  const NiceTensor<T>& aInTensor5
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();
   T* in_ptr2 = static_cast<SimpleTensor<T>* >(aInTensor2.GetTensor())->GetData();
   T* in_ptr3 = static_cast<SimpleTensor<T>* >(aInTensor3.GetTensor())->GetData();
   T* in_ptr4 = static_cast<SimpleTensor<T>* >(aInTensor4.GetTensor())->GetData();
   T* in_ptr5 = static_cast<SimpleTensor<T>* >(aInTensor5.GetTensor())->GetData();

   auto sz0 = aInTensor0.TotalSize();
   auto sz1 = aInTensor1.TotalSize();
   auto sz2 = aInTensor2.TotalSize();
   auto sz3 = aInTensor3.TotalSize();
   auto sz4 = aInTensor4.TotalSize();
   auto sz5 = aInTensor5.TotalSize();
   
   T* in_ptr0_inner;
   T* in_ptr1_inner;
   T* in_ptr2_inner;
   T* in_ptr3_inner;
   T* in_ptr4_inner;
   T* in_ptr5_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < sz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < sz1; ++i1)
      {
         in_ptr2_inner = in_ptr2;
         for (unsigned int i2 = 0; i2 < sz2; ++i2)
         {
            in_ptr3_inner = in_ptr3;
            for (unsigned int i3 = 0; i3 < sz3; ++i3)
            {
               in_ptr4_inner = in_ptr4;
               for (unsigned int i4 = 0; i4 < sz4; ++i4)
               {
                  in_ptr5_inner = in_ptr5;
                  for (unsigned int i5 = 0; i5 < sz5; ++i5)
                  {
                     (*result_ptr) += aCoef* (*in_ptr0_inner)* (*in_ptr1_inner)* (*in_ptr2_inner)* (*in_ptr3_inner)* (*in_ptr4_inner)* (*in_ptr5_inner);
                     ++result_ptr;
                     ++in_ptr5_inner;
                  }
                  ++in_ptr4_inner;
               }
               ++in_ptr3_inner;
            }
            ++in_ptr2_inner;
         }
         ++in_ptr1_inner;
      }
      ++in_ptr0_inner;
   }
}

/**
 * Do special direct product of all SimpleTensor's.
 *
 * @param aRes      Result of direct product.
 * @param aResMc    Modecombination of result.
 * @param aInTensor Vector of tensors to be direct product'ed together.
 * @param aInMcs    Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef     Coefficient to be multiplied to each element of result.
 * @return          True if a special direct product was carried out, false otherwise.
 **/
template
   <  typename T
   >
bool DirProdTensorSimpleSpecial
   (  NiceTensor<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   )
{
   if(aInTensor.size() == 2)
   {
      if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
        )
      {
         DirProdTensorSimpleSpecial2(aRes, aInTensor[0], aInTensor[1], aCoef);
         return true;
      }
      if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
        )
      {
         DirProdTensorSimpleSpecial2(aRes, aInTensor[1], aInTensor[0], aCoef);
         return true;
      }
      else
      {
         return false;
      }
   }
   if(aInTensor.size() == 3)
   {
      if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
            return true;
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
            return true;
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
            return true;
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
            return true;
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
            return true;
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           )
         {
            DirProdTensorSimpleSpecial3(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
            return true;
         }
         else
         {
            return false;
         }
      }
      else
      {
         return false;
      }
   }
   if(aInTensor.size() == 4)
   {
      if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
               return true;
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              )
            {
               DirProdTensorSimpleSpecial4(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
               return true;
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      else
      {
         return false;
      }
   }
   if(aInTensor.size() == 5)
   {
      if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                  return true;
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 )
               {
                  DirProdTensorSimpleSpecial5(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      else
      {
         return false;
      }
   }
   if(aInTensor.size() == 6)
   {
      if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
        && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
        && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
        && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
        && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[4], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[3], aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[4], aInMcs[5]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[2], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[5]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[5], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[5], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[5], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[5]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[5], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[5]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[5]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[5], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[5], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[5], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[3], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[4], aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      if(ModesStrictlyLessThan(aInMcs[5], aInMcs[0]) 
        && ModesStrictlyLessThan(aInMcs[5], aInMcs[1]) 
        && ModesStrictlyLessThan(aInMcs[5], aInMcs[2]) 
        && ModesStrictlyLessThan(aInMcs[5], aInMcs[3]) 
        && ModesStrictlyLessThan(aInMcs[5], aInMcs[4]) 
        )
      {
         if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
           && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[3], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[2], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[3], aInMcs[4]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[0], aInTensor[4], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[4], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[1], aInTensor[4], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[4]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[4], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[4]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[4]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[4], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[4], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[2], aInTensor[4], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[3], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         if(ModesStrictlyLessThan(aInMcs[4], aInMcs[0]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[1]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[2]) 
           && ModesStrictlyLessThan(aInMcs[4], aInMcs[3]) 
           )
         {
            if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[1], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[2], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[0], aInTensor[3], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
              && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[2], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[0], aInTensor[3], aInTensor[2], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[2], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[1], aInTensor[3], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[2], aInMcs[3]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[1], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[0], aInTensor[3], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[3]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[3]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[0], aInTensor[3], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[1], aInTensor[3], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[2], aInTensor[3], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            if(ModesStrictlyLessThan(aInMcs[3], aInMcs[0]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[1]) 
              && ModesStrictlyLessThan(aInMcs[3], aInMcs[2]) 
              )
            {
               if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                 && ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[1], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[0], aInTensor[2], aInTensor[1], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[1], aInMcs[2]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[2]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[0], aInTensor[2], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[1], aInTensor[2], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               if(ModesStrictlyLessThan(aInMcs[2], aInMcs[0]) 
                 && ModesStrictlyLessThan(aInMcs[2], aInMcs[1]) 
                 )
               {
                  if(ModesStrictlyLessThan(aInMcs[0], aInMcs[1]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[0], aInTensor[1], aCoef);
                     return true;
                  }
                  if(ModesStrictlyLessThan(aInMcs[1], aInMcs[0]) 
                    )
                  {
                     DirProdTensorSimpleSpecial6(aRes, aInTensor[5], aInTensor[4], aInTensor[3], aInTensor[2], aInTensor[1], aInTensor[0], aCoef);
                     return true;
                  }
                  else
                  {
                     return false;
                  }
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         }
         else
         {
            return false;
         }
      }
      else
      {
         return false;
      }
   }
   
   return false;
}

} /* namespace midas::vcc::dirprod */

#endif /* DIRPRODSPECIAL_IMPL_H_INCLUDED */
