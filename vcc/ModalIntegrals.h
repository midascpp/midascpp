/**
 *******************************************************************************
 * 
 * @file    ModalIntegrals.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For contractions and related manipulations of integrals uses in
 *    vibrational calculations.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MODALINTEGRALS_H_INCLUDED
#define MODALINTEGRALS_H_INCLUDED

#include "vcc/ModalIntegrals_Decl.h"
#include "vcc/ModalIntegrals_Impl.h"

#endif/*MODALINTEGRALS_H_INCLUDED*/
