/**
 *******************************************************************************
 * 
 * @file    ModalIntegrals_Decl.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For contractions and related manipulations of integrals uses in
 *    vibrational calculations.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MODALINTEGRALS_DECL_H_INCLUDED
#define MODALINTEGRALS_DECL_H_INCLUDED

#include <map>
#include <string>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"
#include "vcc/IntegralContractor.h"

// Forward declarations.
class ModeCombi;
template<typename> class GeneralMidasMatrix;
template<typename> class NiceTensor;

/***************************************************************************//**
 * @brief
 *    For contractions and related manipulations of integrals uses in
 *    vibrational calculations.
 ******************************************************************************/
template
   <  typename T
   >
class ModalIntegrals
   :  private midas::vcc::detail::IntegralContractor<T>
{
   public:
      using Contractor = midas::vcc::detail::IntegralContractor<T>;

      using param_t = typename Contractor::param_t;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      template<typename U> using vec_templ_t = typename Contractor::template vec_templ_t<U>;
      template<typename U> using mat_templ_t = typename Contractor::template mat_templ_t<U>;
      using vec_t = vec_templ_t<param_t>;
      using mat_t = mat_templ_t<param_t>;

      using ContT = typename Contractor::ContT;

      //! The types of (un)transformed integrals/matrix elements.
      enum class TransT {RAW, T1, R1};

      //@{
      //! Default copy/move/destruct.
      ModalIntegrals(const ModalIntegrals<T>&) = default;
      ModalIntegrals(ModalIntegrals<T>&&) = default;
      ModalIntegrals<T>& operator=(ModalIntegrals<T>&&) = default;
      ~ModalIntegrals() = default;
      //@}

      //! Copy assign. is basically like default, but special care needed for mContractContainer.
      ModalIntegrals<T>& operator=(const ModalIntegrals<T>&);

      //! Empty constructor.
      ModalIntegrals() = default;

      //! Construct from integral containers (vectors: outer = modes, inner = operators).
      ModalIntegrals(std::vector<std::vector<mat_t>>&&, TransT aType = TransT::RAW);

      //! Set associated name; see comments at ModalIntegrals<T>::mName.
      void SetName(const std::string&);

      /*********************************************************************//**
       * @name Queries
       ************************************************************************/
      //!@{
      //! Number of modes.
      Uin NModes() const;
      
      //! Number of operators for given mode.
      Uin NOper(LocalModeNr) const;

      //! Num. modals for given mode, i.e. the num. rows/cols. of integral matrices.
      Uin NModals(LocalModeNr) const;

      //! Get the norm squared of the integrals for a mode, operator and contraction type.
      absval_t GetNorm2(Uin aMode, Uin aOper, ContT aType) const;

      //! The transformation type/status of the stored integrals, for checking.
      TransT GetType() const;

      //! The transformation type/status of the stored integrals, for printing.
      std::string GetTypeString() const;

      //! The name associated with the object; see comments at ModalIntegrals<T>::mName.
      const std::string& Name() const;
      //!@}

      /*********************************************************************//**
       * @name Integral access
       ************************************************************************/
      //!@{
      //! The integral matrix for given (mode,operator).
      const mat_t& GetIntegrals(LocalModeNr, LocalOperNr) const;

      //! Get all integrals
      const std::vector<std::vector<mat_t> >& GetIntegrals
         (
         )  const;

      //! A single integral matrix element.
      param_t GetElement(LocalModeNr, LocalOperNr, Uin, Uin) const;

      //! The occupied-occupied integral matrix element.
      param_t GetOccElement(LocalModeNr, LocalOperNr) const;

      /*********************************************************************//**
       * @name Transformations
       *
       * T1/R1-transform the integrals using the given T1/R1-parameters.
       ************************************************************************/
      //!@{
      void T1Transform(const std::vector<vec_t>&);
      void R1Transform(const std::vector<vec_t>&);
      //!@}

      /*********************************************************************//**
       * @name Contractions
       ************************************************************************/
      //!@{
      //! Contract a set of coefficients using the integrals of a specific one-mode operator.
      void Contract
         (  const ModeCombi& arM1
         ,  const ModeCombi& arM0
         ,  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  vec_t& arY1
         ,  const vec_t& arY0
         ,  In aWhich
         ,  midas::vcc::IntegralOpT aType = midas::vcc::IntegralOpT::STANDARD
         )  const;

      //@{
      //! Perform one-mode operator contraction with NiceTensor framework.
      NiceTensor<T> ContractDown   (const In, const In, const In, midas::vcc::IntegralOpT, const NiceTensor<T>&) const;
      NiceTensor<T> ContractForward(const In, const In, const In, midas::vcc::IntegralOpT, const NiceTensor<T>&) const;
      NiceTensor<T> ContractUp     (const In, const In, const In, midas::vcc::IntegralOpT, const NiceTensor<T>&) const;
      //@}
      //!@}

   private:
      //! The stored integrals. vector(modes) of vector(opers) of square matrices.
      std::vector<std::vector<mat_t>> mIntegrals;

      //! Norms squared of the subblocks of the integrals, corresponding to ContT types.
      std::vector<std::vector<std::vector<absval_t>>> mIntNorms2;

      //! The current type of integrals stored, i.e. how have they been transformed.
      TransT mType = TransT::RAW;

      //! Number of modals for each mode
      std::vector<Uin> mNModalsVec;

      /*********************************************************************//**
       * @brief Name associated with object.
       *
       * Currently (Jan 2019) it's an unnecessary dummy variable. I introduced
       * for being able, in the future, to retrieve backwards functionality
       * from the former Integrals class, where the name is associated with the
       * DataCont when it's saved to disk.
       ************************************************************************/
      std::string mName = "";

      //! TransT -> string for print outs. Initialized in impl. file.
      static const std::map<TransT, std::string> mTypeToString;

      //! Causes a hard error if object is inconsistent.
      void SanityCheck() const;

      //! Updates the norms squared; must be called after anything modifying the integrals.
      void UpdateIntNorms2();

      /*********************************************************************//**
       * @name Transformation implementations
       *
       * Implementation details for the T1- and R1-transformations.
       ************************************************************************/
      //!@{
      //! Impl. of the T1Transform; controls mode/operator-loops, sanity checks.
      void T1TransformImpl(const std::vector<vec_t>&);
      //! Impl. of the T1Transform; individual matrix modifications.
      static void T1TransformImpl(mat_t&, const vec_t&);
      //! Impl. of the R1Transform; controls mode/operator-loops, sanity checks.
      void R1TransformImpl(const std::vector<vec_t>&);
      //! Impl. of the R1Transform; individual matrix modifications.
      static void R1TransformImpl(mat_t&, const vec_t&);
      //!@}

      /*********************************************************************//**
       * @name Contraction details
       ************************************************************************/
      //!@{

      //! Calculate the max. size needed for mContractContainer.
      Uin ReserveSizeForContractContainer() const;

      //!@}
};

//! Output operator.
template<typename T>
std::ostream& operator<<(std::ostream&, const ModalIntegrals<T>&);

#endif/*MODALINTEGRALS_DECL_H_INCLUDED*/
