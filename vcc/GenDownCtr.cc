#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "GenDownCtr_Decl.h"
#include "GenDownCtr_Impl.h"

#include "v3/VccEvalData.h"

namespace midas::vcc
{

// define 
#define INSTANTIATE_GENDOWNCTR(T, DATA) \
template class GenDownCtr<T, DATA>;
// concrete instantiations
INSTANTIATE_GENDOWNCTR(Nb, v3::VccEvalData)
#undef INSTANTIATE_GENDOWNCTR


} /* namespace midas::vcc */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

