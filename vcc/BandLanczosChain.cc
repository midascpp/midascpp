/**
************************************************************************
* 
* @file                BandLanczosChain.cc
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen (mrgodtliebsen@hotmail.com)
*
* Short Description:   Implementation of BandLanczosChainDef and
*                      BandLanczosChain classes.
* 
* Last modified: Thu Dec 09, 2010  04:24PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// midas headers
#include "util/Timer.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "BandLanczosChain.h"
#include "mmv/Diag.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/MidasSystemCaller.h"
#include "util/FileSystem.h"
#include "util/Os.h"

In sign(Nb aNum)
{
if(aNum >= 0)
   return 1;
else if(aNum < 0)
   return -1;

MIDASERROR(" BandLanczosChain.cc: Sign function mishap!");
return 0;
}

BandLanczosChainDef::BandLanczosChainDef(In aLength, In aBlock, In aOrtho, bool aSave, bool aTest,
                                          Nb aDtol, Nb aConv, Nb aConvVal, bool aDeflate):
                                          LanczosChainDefBase(aLength, aOrtho),
                                          mBlock(aBlock), mSave(aSave), mTest(aTest),
                                          mDtol(aDtol), mConv(aConv), mConvVal(aConvVal),
                                          mDeflate(aDeflate) { }

bool BandLanczosChainDef::Combine(const BandLanczosChainDef& aOther)
{
   if(mOpers.size() == (aOther.mOpers).size() && mOrtho == aOther.mOrtho && mBlock == aOther.mBlock && mDtol == aOther.mDtol)
   {
      for(In i=0; i<mOpers.size(); i++)
      {   
         if(mOpers[i] != (aOther.mOpers)[i])
            return false;
      }
   }
   else
      return false;

   mLength = max(mLength, aOther.mLength);
   return true;
}

bool BandLanczosChainDef::Compatible(const BandLanczosChainDef& aOther) const
{
   //Mout << (*this);
   if (mOpers.size() != (aOther.mOpers).size() || mOrtho != aOther.mOrtho || mBlock != aOther.mBlock || mLength < aOther.mLength || mDtol != aOther.mDtol)
      return false;
   else
      for(In i = I_0; i<mOpers.size(); i++)
         if(mOpers[i] != (aOther.mOpers)[i])
            return false;

   return true;
}

ostream& operator<<(ostream& aOut, const BandLanczosChainDef& aDef)
{
   aOut  << " Band Lanczos chain definition: " << endl
         << "     Chainlength:   " << aDef.GetLength() << endl
         << "     Block size:    " << aDef.GetBlock() << endl
         << "     Ortho scheme:  " << aDef.GetOrtho() << endl
         << "     DefThresh:     " << aDef.GetDtol() << endl
         << "     DiagMethod:    " << aDef.GetDiagMethod() << endl
         << "     For operators: ";
         for(In i=I_0; i<(aDef.GetOpers()).size(); i++)
         {
            aOut << aDef.GetOper(i) << " ";
         }
   aOut  << endl
         << "     Save Chain:    " << aDef.GetSave() << endl;

   return aOut;
}

BandLanczosChain::BandLanczosChain(const BandLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf): mDef(aDef), mpVcc(apVcc), mpTrf(apTrf) 
{ 
   mFilePrefix = "bandchain_";
   for(In i=0; i<(mDef.GetOpers()).size(); i++)
      mFilePrefix += mDef.GetOper(i);
   
   mFilePrefix += "_ortho_" + std::to_string(mDef.GetOrtho());
   mFilePrefix += "_block_" + std::to_string(mDef.GetBlock());
   
   In nc = mpTrf->NexciXvec()-I_1;
   
   ///> If chainlength is larger than response space, the chainlength is reduced.
   if(mDef.GetLength() > nc)
   {
      mDef.SetLength(nc);
      Mout << " Length of Band Lanczos chain "<< GetName() <<" has been changed to: " << nc << "." << endl;
   }
   mT.SetNewSize(mDef.GetLength(), false, true);
   mDef.SetDiagMethod("DSBEVD");
}

void BandLanczosChain::Evaluate()
{
   Mout << endl;
   Mout << " Evaluating Band Lanczos chain with chain definition: "<< endl  << mDef << endl;
   
   In            nc = mpTrf->NexciXvec()-I_1;   ///< Number of Response functions.
   vector<In>  DefIndex;                     ///< Holds index for the deflated vectors.
   bool          notconverged=true;            ///< Checks for convergence. (Not used)
   bool         fullydeflated=false;            ///< Checks for full deflation.
   In            pc=mDef.GetBlock();            ///< Counts \#vectors minus \#deflations.
   Nb            qjNorm;                       ///< Holds old qj-norm to store in T-Matrix.
   Nb          CheckDef;                     ///< Holds qj-norm to check for deflation.
   In            j=I_0;                        ///< Iteration counter.
   In          init_chainidx=I_0;            ///< 
   Timer       timer_band;                    ///< Timer.
   Nb          time_trans=0;
   Nb          time_ortho=0;

   Mout << " Size of Q-vectors, and the response space: " << nc << endl;

   ///> Reserve chainlength + pc in mQ
   mQ.reserve(mDef.GetLength() + pc);
   
   if(mpVcc->pVccCalcDef()->RspRestart() && Restart(j, pc,fullydeflated))
   {
      ///> Restart if keyword is set and restart files are present
      Mout  << " Band Lanczos response chain: Restarted using old data." << endl
            << " Current chain length: " << j << endl;
   }
   else
   {
      ///> else make \#mDef.GetBlock() start vectors.
      MakeStartVectors(nc, mDef.GetOpers());
   }
   mpTrf->SetType(TRANSFORMER::VCIRSP);
   
   string restart_filename = mFilePrefix+"_restart";
   ofstream restart_file(restart_filename.c_str(), ios::app);   

   ///> Initialization for periodic and partial ortho scheme.
   if(mDef.GetOrtho() == LANCZOS_ORTHO_PERIODIC || mDef.GetOrtho() == LANCZOS_ORTHO_PARTIAL)
   {
      mW.SetNewSize(mDef.GetLength(), false, true);
      mOrthoCount = 0;
   }
   
   ///> Calculate BandLanczos vectors
   while(notconverged && pc>I_0 && j<mDef.GetLength())
   {
      time_t t0 = time(NULL);
      qjNorm = mQ[j].Norm();
      
      Timer timer_ortho;
      switch(mDef.GetOrtho())
      {
         case LANCZOS_ORTHO_NONE:
         {
            if(j==0)
               Mout << "Not doing any reothogonalizations." << endl;
            break;
         }
         case LANCZOS_ORTHO_FULL:
         {
            if (j==0)
               Mout << "Using Full Orthogonalization scheme." << endl;
            Full_Ortho(mQ[j], j, pc, I_3);
            break;
         }
         case LANCZOS_ORTHO_PERIODIC:
         {
            if (j==0)
               Mout << "Using experimental Periodic Orthogonalization scheme." << endl;
            Periodic_Ortho(mQ[j], mW, j, pc, qjNorm, mOrthoCount);
            break;
         }
         case LANCZOS_ORTHO_PARTIAL:
         {
            if(j==0)
               Mout << "Using experimental Partial Orthogonalization scheme." << endl;
            Partial_Ortho(mQ[j], mW, j, pc, qjNorm, mOrthoCount, mOrthoIndex);
            break;
         }
         default:
         {
            ///> Output for developers :P
            MIDASERROR(" Error in orthogonalization scheme switch in BandLanczosChain::Evaluate(). ");
         }
      }
      time_ortho+=timer_ortho.CpuTime();

      CheckDef = mQ[j].Norm();

      ///> Check for deflation
      if(CheckDef < mDef.GetDtol())
      {
         ///> Deflation
         Mout  << " A deflation occured on iteration " << j << "." << endl
               << " I'm deleting the vector and restarting the iteration with the next one." << endl;
         if(j-pc > I_0)
            DefIndex.push_back(j - pc);
         
         ///> Change storage to disc for vector j-pc, which isn't used in further iterations.
         if(j-pc>=I_0)
            mQ[j-pc].ChangeStorageTo("OnDisc");

         pc--;
         
         ///> Check to se if all vectors are deflated. If so, then STOP.
         if(pc == I_0)
         {
            Mout << " Fully Deflated " << endl;
            fullydeflated = true;
         }
         else
         {
            ///> Delete deflated vector and move the rest one index back.
            for(In k=j; k<j+pc; k++)
               mQ[k]=mQ[k+I_1];
            mQ.pop_back(); ///< Delete the dulicated vector k+pc
         }
      }
      else
      {
         ///> No deflation.
         ///> Give a label to vector j.
         ostringstream os;
         os << mFilePrefix+"_q" << j;
         (mQ[j]).NewLabel(os.str());
         (mQ[j]).SaveUponDecon(true);
         
         if(j-pc>=I_0)
            mT[j][j-pc] = qjNorm;

         mQ[j].Normalize();
         
         ///> Make vectors j+1...j+pc-1 orthonormal to vector j.
         for(In k=j+I_1; k<j+pc; k++)
         {
            if(k-pc >= I_0)
            {
               mT[j][k-pc] = Dot(mQ[j],mQ[k]);
               mQ[k].Axpy(mQ[j], -mT[j][k-pc]);
            }
            else
               mQ[k].Axpy(mQ[j], -Dot(mQ[j],mQ[k]));
         }
         
         ///> Makes transformation q_j -> Aq_j = q_j+pc
         TransformBand(mQ[j], nc, j, time_trans);
         
         ///> Save T[j][j]
         mT[j][j] = Dot(mQ[j],mQ[j+pc]);
         mQ[j+pc].Axpy(mQ[j], -mT[j][j]);

         ///> Make Vector Aqj orthonormal to vectors j-pc...j-1.
         In k_0 = max(I_0, (j-pc));
         for(In k=k_0; k<j; k++)
         {
            mT[k][j] = mT[j][k]; ///< If mT is complex, here should be a complex conjugation.
            mQ[j+pc].Axpy(mQ[k], -mT[k][j]);
         }
         
         ///> Calculate d entries in T-matrix.
         for(In k=I_0; k<DefIndex.size(); k++)
         {
            mT[DefIndex[k]][j] = Dot(mQ[DefIndex[k]],mQ[j+pc]);
            mT[j][DefIndex[k]] = mT[DefIndex[k]][j];
            mQ[j+pc].Axpy(mQ[k], -mT[k][j]);
         }
         
         ///> Change storage to disc for vector j-pc, which isn't used in further iterations.
         if(j-pc>=I_0)
            mQ[j-pc].ChangeStorageTo("OnDisc");

         ///> Print Progress
         if (j == init_chainidx)
         {
            Mout << " Required iterations: " << mDef.GetLength()-init_chainidx << endl
                 << " First iteration: " << time(NULL)-t0 << " s"<< endl 
                 << " Progress (one . is 10 iterations):" << endl << " ";
         }
         else if ( (j-init_chainidx + 1)%10 == I_0)
         {
            Mout << "." << std::flush;
         }
         
         if(mDef.GetConv()!=C_0)
            notconverged=TestConvergence(j,pc,DefIndex);

         restart_file << j << " " << pc << endl;
         j++;
      }
   }

   Mout << endl;
   
   restart_file << j << " " << pc << endl;
   restart_file.close();
   
   ///> Midas output.   
   Mout << "Stopped after " << j << " iterations." << endl;
   if(fullydeflated)
      Mout << "The Krylov Space is fully deflated." << endl;
   else
      Mout << "The Krylov Space is NOT fully deflated!" << endl;
   
   ///> Make Q^(j)
   for(In i=j-pc; i<j+pc; i++)
   {
      if(i >= I_0)
      {
         mQ[i].SaveUponDecon(true);
         mQ[i].ChangeStorageTo("OnDisc");
      }
   }
   mQ.resize(j);

   if(gTime)
   {
      string cpu_time = " CPU time used in evaluation of Band Lanczos chain: ";
      string wall_time = " Wall time used in evaluation of Band Lanczos chain: ";
      timer_band.CpuOut(Mout,cpu_time);
      timer_band.WallOut(Mout,wall_time);
      Mout << " CPU time used in Transformations: " << time_trans << "\n"
           << " CPU time used in Orthogonalizations: " << time_ortho << endl;
   }
   
   //Save T-matrix to disc for Restart.
   //T-matrix is stored in a sparse form.
   if(mDef.GetSave())
   {        
      string Tfilename = mFilePrefix + "_tmat";
      ofstream t_matrix_file(Tfilename.c_str(), ios_base::trunc);
      t_matrix_file << scientific << setprecision(20);
      for(In i=0; i<mT.Nrows(); i++)
      {
         In k=min(mT.Ncols(), (i+mDef.GetBlock()+I_1));
         for(In j=i; j<k; j++)
            if(mT[i][j] != 0)
               t_matrix_file << i << " " << j << " " << mT[i][j] << endl;
      }
      ///> This will give some few duplicated lines, but this doesn't matter 
      ///> because they will be overridden when the T-matrix is reloaded in the Restart() func.
      for(In k=0; k < DefIndex.size(); ++k)
         for(In j=DefIndex[k]; j<mT.Ncols(); ++j)
            t_matrix_file << DefIndex[k] << " " << j << " " << mT[DefIndex[k]][j] << endl;
   }
   
   ///> Debug and benchmark outputs
   if(gDebug || mDef.GetTest())
   {
      Mout << " Making Band Lanczos Test files:    q_orthogonality " << endl
           << "                                    residuals       " << endl
           << "                                    T_test          " << endl
           << "                                    W               " << endl;
      CalcOrtho();
      Test_T(j,nc);
      TestResidual(j, nc);
      
      ///> Setup file containing W-matrix
      string filename = mFilePrefix + "_W";
      ofstream w_matrix_file(filename.c_str(), ios_base::trunc);
      w_matrix_file << scientific << setprecision(20);
      w_matrix_file << mW << endl;
   }

   if(mDef.GetSave())
      SaveChain();
   
   Mout << "I am Band Lanczos. I'm done evaluating the chain. Thanks for visiting :) !" << endl;
}

/**
 *
**/
void BandLanczosChain::MakeStartVectors
   (  const In nc
   ,  const std::vector<std::string>& aOpers
   )
{
   In diff = aOpers.size() - mDef.GetBlock();

   for (In j=I_0; j<aOpers.size(); j++)
   {
      for (In iop=I_0; iop<gOperatorDefs.GetNrOfOpers(); iop++)
      {
         if (gOperatorDefs[iop].Name() == aOpers[j])
         {
            Nb exptval = C_0;
            DataCont eta(nc, C_0, "InMem", gOperatorDefs[iop].Name(), true);
            mpVcc->CalculateVciEta(eta, aOpers[j], iop, exptval);
            mEta.push_back(eta);
            break;
         }
      }
   }

   for(In i=I_0; i<mDef.GetBlock(); i++)
   {
      Mout << " Making Band Lanczos starting vector " << (i+I_1) << " from operators: " <<endl;
      Mout << "      " << aOpers[i] << endl;
      DataCont q(mEta[i]);
      mQ.push_back(q);
      
      ///> If \#operators > blocksize, the algortihm puts the rest 
      ///> of the operator Eta's in the last starting vector.
      if(diff != 0 && i == mDef.GetBlock()-I_1)
         for(In j=i+1; j<aOpers.size() ;j++)
         {
            Mout << "      " << aOpers[j] << endl;
            mQ[i].Axpy(mEta[j], I_1);
         }
      mQ[i].Normalize();
   }

   for(In i=I_0; i<aOpers.size(); i++)
   {
      ostringstream os;
      os << mFilePrefix + "_eta" << i;
      mEta[i].NewLabel(os.str());
      mEta[i].ChangeStorageTo("OnDisc");
   }
}

void BandLanczosChain::TransformBand(DataCont& aqj, const In anc, const In aJ, Nb& time)
{
   Timer time_trans;
   Nb dummy=C_0;
   DataCont Aqj(anc,C_0,"InMem", "temp" , false);
   mpTrf->Transform(aqj, Aqj, I_1, I_0, dummy);
   mQ.push_back(Aqj);
   mQ.back().NewLabel(mFilePrefix+"_unused_q" + std::to_string(aJ+mDef.GetBlock()));
   time += time_trans.CpuTime();
}

bool BandLanczosChain::Restart(In& aJ, In& aPc, bool& aFullyDeflated)
{
   Mout << " Attempting to restart Band Lanczos chain calculation using old data." << endl;
   // Get the restart information
   midas::filesystem::Copy(gSaveDir + "/" + mFilePrefix + ".tar.gz", mFilePrefix + ".tar.gz");
   system_command_t cmd = {"tar", "xvf", mFilePrefix + ".tar.gz"};
   MIDASSYSTEM(cmd);
   
   //
   In l, m;
   In restart;
   string restart_filename = mFilePrefix+"_restart";
   if(!InquireFile(restart_filename))
   {
      Mout << " No restart file found, so I'm evaluating the whole chain."  << endl;
      return false;
   }
   ifstream restart_file(restart_filename.c_str());   
   
   string tmat_filename = mFilePrefix+"_tmat";
   if(!InquireFile(tmat_filename))
   {
      Mout << " No tmat file found, so I'm evaluating the whole chain." << endl;  
      return false;
   }
   ifstream tmat_file(tmat_filename.c_str());

   Mout << " Restart and tmat files found. Assuming all q-vector files are present. " << endl
        << " If they are not calculation will fail! " << endl;
   
   while(!restart_file.eof())
   {
      restart_file >> restart;
      restart_file >> aPc;
   }

   if(aPc == 0)
      aFullyDeflated = true;

   aJ = min(mDef.GetLength(), restart);

   while(!tmat_file.eof())
   {
      tmat_file >> l;
      tmat_file >> m;
      tmat_file >> mT[l][m];
      mT[m][l] = mT[l][m];
   }

   for(In i=0; i<(aJ-aPc); i++)
   {
      DataCont q;
      mQ.push_back(q);
      mQ.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_q"+std::to_string(i));
      mQ.back().SaveUponDecon(true);
   }
   
   for(In i=(aJ-aPc); i<aJ; i++)
   {
      DataCont q;
      mQ.push_back(q);
      mQ.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_q"+std::to_string(i));
      mQ.back().SaveUponDecon(true);
      if(!mQ.back().ChangeStorageTo("InMem",true))
      {
         Mout << " Band Lanczos restart failed to read q" << i << " file." << endl;
         mQ.clear();
         aJ = 0;
         aPc = mDef.GetBlock();
         return false;
      }
   }

   for(In i=aJ; i<(aJ+aPc); i++)
   {
      DataCont q;
      mQ.push_back(q);
      mQ.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_unused_q"+std::to_string(i));
      mQ.back().SaveUponDecon(true);
      if(! mQ.back().ChangeStorageTo("InMem", true))
      {
         Mout << " Band Lanczos restart failed to read unused vector " << i << " file." << endl;
         mQ.clear();
         aJ = 0;
         aPc = mDef.GetBlock();
         return false;
      }
   }

   // LAV IF DER TJEKKER OM mQ HAR RETTE STOERRELSE

   for(In i=0; i<(mDef.GetOpers()).size(); i++)
   {
      DataCont eta;
      mEta.push_back(eta);
      mEta.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_eta"+std::to_string(i));
      mEta.back().SaveUponDecon(true);
   }

   return true;
}

void BandLanczosChain::SaveChain()
{
   Mout << " Saving chain: " << mFilePrefix << " for restart." << endl;
   
   // Create tar command.
   system_command_t cmd = {"tar", "-cvf", mFilePrefix + ".tar.gz"};
   auto match = midas::filesystem::SearchDir(midas::os::Getcwd(), std::regex(mFilePrefix));
   for(const auto& f : match)
   {
      cmd.emplace_back(f);
   }
   
   // Run tar command
   MIDASSYSTEM(cmd);
   
   // Copy back tar file
   midas::filesystem::Copy(mFilePrefix + ".tar.gz", gSaveDir + "/" + mFilePrefix + ".tar.gz");
}

void BandLanczosChain::GetStartVector(const In aVec, DataCont& aQ) const
{
      ostringstream os;
      os << mFilePrefix << "_q" << aVec;
      aQ.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! aQ.ChangeStorageTo("InMem", true, false, true))
      {
         MIDASERROR("Error reading q(j) in BandLanczosChain::GetStartVector.");
      }
}

void BandLanczosChain::GetEtaVector(const In aVec, DataCont& aEta) const
{
      ostringstream os;
      os << mFilePrefix << "_eta" << aVec;
      aEta.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! aEta.ChangeStorageTo("InMem", true, false, true))
      {
         MIDASERROR("Error reading eta(j) in BandLanczosChain::GetEtaVector.");
      }
}

void BandLanczosChain::GetT(MidasMatrix& aMat) const
{
   aMat.SetNewSize(mT.Nrows(), mT.Ncols(), false);
   
   for(In i=I_0; i<mT.Nrows(); i++)
   {
      for(In j=I_0; j<mT.Ncols(); j++)
      {
         aMat[i][j] = mT[i][j];
      }
   }
}

void BandLanczosChain::Full_Ortho(DataCont& aVec, const In aJ, const In pc, const In repeat)
{
   if(aJ-pc>0)
   {
      for(In i_repeat=I_0; i_repeat<repeat; i_repeat++)
      {
         for(In k=I_0; k<aJ-pc; k++)
         {
            ostringstream os;
            os << mFilePrefix << "_q" << k;
            DataCont q;
            q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
            if(! q.ChangeStorageTo("InMem", true, false, true))
            {
               MIDASERROR("Error reading q(" + std::to_string(k) + ") in orthogonalization step (file: '" + os.str() + "').");
            }
            aVec.Orthogonalize(q);
         }
      }
   }
}

void BandLanczosChain::Periodic_Ortho(DataCont& aVec, MidasMatrix& aW, const In aJ, const In pc, const Nb aqjNorm, In& aOrthoCount)
{
   Nb Epsilon = C_NB_EPSILON;
   Nb Threshold = sqrt(Epsilon); ///< This threshold could be set differently.
   bool Do_Ortho = false;

   aW[aJ][aJ] = I_1;
   for(In i=I_1; i<pc+1; i++)
   {
      if(aJ-i >=0)
      {
         aW[aJ][aJ-i] = Epsilon;
      }
      if(aJ+i < aW.Ncols())
      {
         aW[aJ][aJ+i] = Epsilon;
      }
   }
   
   if(aOrthoCount != 0)
   {
      Do_Ortho = true;
      aOrthoCount--;
   }
   else
   {
      for(In k=I_0; k<aJ-pc; k++)
      {
         Nb sum_1 = C_0;
         Nb sum_2 = C_0;
   
         for(In l=(k-pc); l<(k+pc); l++)
         {
            if(l>=0)
            {
            sum_1 = sum_1 + mT[k][l]*aW[aJ-pc][l];
            }
         }
         
         for(In j=(aJ-2*pc); j<aJ; j++)
         {
            if(j>=0)
            {
            sum_2 = sum_2 + mT[aJ-pc][j]*aW[j][k];
            }
         }
         
         aW[aJ][k] = mT[k][k+pc]*aW[aJ-pc][k+pc] + sum_1 - sum_2;
         aW[aJ][k] = (aW[aJ][k] + sign(aW[aJ][k])*Epsilon)/aqjNorm; ///< The error estimate should perhaps be higher,
                                                                    ///< but it have been producing rather good data like this. 
         if(std::fabs(aW[aJ][k])>Threshold)
         {
            Do_Ortho = true;
            aOrthoCount = 2*pc-1;
         }
      }
   }

   if(Do_Ortho)
   {
      Mout << " Doing Periodic Orthogonalization on vector " << aJ << endl;
      if(aJ-pc>0)
      {
         for(In i_repeat=I_0; i_repeat<I_3; i_repeat++)
         {
            for(In k=I_0; k<aJ-pc; k++)
            {
               ostringstream os;
               os << mFilePrefix << "_q" << k;
               DataCont q;
               q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
               if(! q.ChangeStorageTo("InMem", true, false, true)) // ?
               {
                  MIDASERROR("Error reading q(j) in orthogonalization step.");
               }
               aVec.Orthogonalize(q);
               aW[aJ][k] = Epsilon; 
            }
         }
      }
   }
}

void BandLanczosChain::Partial_Ortho(DataCont& aVec, MidasMatrix& aW, const In aJ, const In pc, const Nb aqjNorm, In& aOrthoCount, MidasMatrix& aOrthoIndex)
{
   ///> THIS IS EXPERIMENTAL CODE!
   Nb Epsilon = C_NB_EPSILON;
   //Nb Threshold = C_I_10_12;
   Nb Threshold = sqrt(Epsilon);
   Nb EtaThresh = sqrt(Epsilon)*sqrt(sqrt(Epsilon));
   bool Do_Ortho = false;
   list<In> kIndex;
   In kIndexCount=0;

   aW[aJ][aJ] = I_1;
   for(In i=I_1; i<pc+1; i++)
   {
      if(aJ-i >=0)
      {
         aW[aJ][aJ-i] = Epsilon;
      }
      if(aJ+i < aW.Ncols())
      {
         aW[aJ][aJ+i] = Epsilon;
      }
   }
   
   //Mout << " pc = " << pc << endl; // Tror maaske den skal bruge den startene pc hele tiden... ellers kommer der fejl efter def...

   if(aOrthoCount != 0)
   {
      Do_Ortho = true;
      aOrthoCount--;
   }
   else
   {
      for(In k=I_0; k<aJ-pc; k++)
      {
         Nb sum_1 = C_0;
         Nb sum_2 = C_0;
   
         for(In l=(k-pc); l<(k+pc); l++)
            if(l>=0)
               sum_1 = sum_1 + mT[k][l]*aW[aJ-pc][l];
         
         for(In j=(aJ-2*pc); j<aJ; j++)
            if(j>=0)
               sum_2 = sum_2 + mT[aJ-pc][j]*aW[j][k];
         
         aW[aJ][k] = mT[k][k+pc]*aW[aJ-pc][k+pc] + sum_1 - sum_2;
         aW[aJ][k] = (aW[aJ][k] + sign(aW[aJ][k])*Epsilon)/aqjNorm;
   
         if(std::fabs(aW[aJ][k])>Threshold)
         {
            Do_Ortho = true;
            aOrthoCount = 2*pc-1;
            kIndex.push_back(k);
            ++kIndexCount;
         }
      }

      if(kIndexCount > 0)
      {
         aOrthoIndex.SetNewSize(kIndexCount, I_2, false, false);
         In OrthoIndexCount = 0;
         for(list<In>::iterator it=kIndex.begin(); it!=kIndex.end(); it++)
         {
            for(In i = (*it)-1; i>=0; --i)
               if(aW[aJ][i] < EtaThresh)
               {
                  aOrthoIndex[OrthoIndexCount][1] = i + I_1;
                  break;
               }
           for(In i = (*it)+1; i<aJ-pc; ++i)
               if(aW[aJ][i] < EtaThresh)
               {
                  aOrthoIndex[OrthoIndexCount][2] = i - I_1;
                  break;
               }
           ++OrthoIndexCount;  
         }
      }
   }

   if(Do_Ortho)
   {
      Mout << " Doing Partial Orthogonalization on vector " << aJ << endl;
      for(In i = I_0; i<aOrthoIndex.Nrows();++i)
         for(In i_repeat=I_0; i_repeat<I_3; i_repeat++)
         {
            for(In k=In(aOrthoIndex[i][1]); k<In(aOrthoIndex[i][2])+I_1; k++)
            {
               ostringstream os;
               os << mFilePrefix << "_q" << k;
               DataCont q;
               q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
               if(! q.ChangeStorageTo("InMem", true, false, true)) // ?
               {
                  MIDASERROR("Error reading q(j) in orthogonalization step.");
               }
               if(i_repeat == I_0)
                  Mout << "      Orthogonalizing on vector q" << k << "." << endl;
               aVec.Orthogonalize(q);
               aW[aJ][k] = Epsilon; 
            }
         }
   }
}

void BandLanczosChain::BandDiag(In aDim, MidasVector& aEigVals, MidasMatrix& aEigVecs) const
{
   MidasMatrix tsolve;
   tsolve.Reassign(mT, aDim, aDim);
   Timer time_diag;

   Diag(tsolve, aEigVecs, aEigVals, mDef.GetDiagMethod(), false, true, mDef.GetBlock());

   if(gTime)
   {
      string cpu_time = " CPU time used for diagonalizing T-Matrix: ";
      string wall_time = " Wall time used for diagonalizing T-Matrix: ";
      time_diag.CpuOut(Mout,cpu_time);
      time_diag.WallOut(Mout,wall_time);
   }
   
   ///> Write eigenvalues to file
   string filename = gAnalysisDir+"/"+mFilePrefix+"_eigvals";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file  << fixed << setprecision(15)
         << "Column 1: Eigenvalues of T (cm-1)" << endl;
   for(In i = I_0; i<aDim; i++)
   {
      file << aEigVals[i]*C_AUTKAYS << endl;
   }

   return;
}

void BandLanczosChain::TransformStoX(const MidasVector& aEigVec, DataCont& aX) const
{
   aX.SetNewSize(mpTrf->NexciXvec() -I_1);
   aX.Zero();
   aX.ChangeStorageTo("InMem");

   for(In i=I_0; i<aEigVec.Size(); ++i)
   {
      ostringstream os;
      os <<mFilePrefix << "_q" << i;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! q.ChangeStorageTo("InMem", true, false, true))
      {
         MIDASERROR("BandLanczosChain::TransformStoX(): Error reading q(" + std::to_string(i) + ") (file : '" + os.str() + "').");
      }
      aX.Axpy(q, aEigVec[i]);
   }
}

bool BandLanczosChain::TestConvergence(const In ItNum, const In pc ,const vector<In>& DefIndex)
{
   // This function doesn't quite work as intended, yet...
   
   Nb init_C_0 = C_0;
   MidasMatrix TestT(ItNum, init_C_0); // Flyt evt ned under if-saetningen
   MidasMatrix TestEigenVecs(ItNum, init_C_0); // -||-
   MidasVector TestEigenVals(ItNum, init_C_0); // -||-
   bool notconverged=true;
   In NumEigenTest = ItNum - mDef.GetBlock();
   Nb res=C_0;
   Nb sum=C_0;

   Mout << " I will now test convergence of the Band Lanczos chain: " << endl;

   if((pc == 0 || ItNum%10==I_0) && NumEigenTest > mDef.GetBlock())
   {
      notconverged=false;

      for(In i=I_0; i<ItNum; i++) // FIX TIL KUN AT TAGE DE INDGANGE MED NOGET I...
         for(In j=I_0; j<ItNum; j++)
            TestT[i][j] = mT[i][j];
      
      //Mout << "Diag method: " << mDef.GetDiagMethod() << endl;
      Diag(TestT, TestEigenVecs, TestEigenVals, mDef.GetDiagMethod(), false, true);
      
      for(In i=I_0; i<NumEigenTest; i++)
      {
         //Mout << " mDef.GetConvVal() " << mDef.GetConvVal() << endl;
         //Mout << " TestEigenVals[i] " << TestEigenVals[i] << endl;
         if(i!=I_0 && TestEigenVals[i] > mDef.GetConvVal())
            break;
         
         res = C_0;
         sum = C_0;

         //Calculate residual for eigenpair i
         for(In j=(NumEigenTest + 1); j<ItNum; j++) // From j+1 to j+p
         {
            for(In k=j-pc; k<NumEigenTest ;k++)
               sum +=mT[j][k]*TestEigenVecs[k][i];
            
            // Account for deflation errors...
            for(In k=I_0; k<DefIndex.size(); k++)
              sum += mT[j][DefIndex[k]]*TestEigenVecs[DefIndex[k]][i];

            res += sum*sum;
         }

         res = sqrt(res);
         if(res <= mDef.GetConv())
         {
            Mout << "   eigval " << i << "=" << TestEigenVals[i]
            << " res = " << res << " CONVERGED." << endl;
         }
         else
         {
            notconverged = true;
            Mout << "   eigval " << i << "=" << TestEigenVals[i]
            << " res = " << res << " NOT CONVERGED." << endl;
            //Mout << i << ": Eigenvalue " << TestEigenVals[i] 
            //<< " is NOT CONVERGED with a residual of " << res << "." << endl;  
         }
      }

   }

   return notconverged;
}

void BandLanczosChain::TestResidual(const In ItNum, const In anc)
{
   Nb init_C_0 = C_0;
   MidasMatrix TestT(ItNum, init_C_0);
   MidasMatrix TEigenVecs(ItNum, init_C_0);
   MidasVector TestEigenVals(ItNum, init_C_0);
   MidasVector Residuals(ItNum, init_C_0);
   MidasMatrix X(anc,ItNum);
   vector<DataCont> EigenVecs;
   MidasMatrix QSave(anc, ItNum);

   Mout << " I r testing residuals " << endl;

   for(In i=I_0; i<ItNum; i++) // FIX TIL KUN AT TAGE DE INDGANGE MED NOGET I...
   {
      for(In j=I_0; j<ItNum; j++)
      {
         TestT[i][j] = mT[i][j];
      }
   }

   Diag(TestT, TEigenVecs, TestEigenVals, mDef.GetDiagMethod(), false, true);

   for(In i=0; i<ItNum; i++)
   {
      ostringstream os;
      os << mFilePrefix << "_q" << i;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! q.ChangeStorageTo("InMem", true, false, true))
      {
         MIDASERROR("Error reading q(j) in Residual Test step.");
      }
      
      QSave.AssignCol(*(q.GetVector()),i);
   }

   X = QSave*TEigenVecs;

   for(In i=0; i<ItNum; i++)
   {  
      Nb dummy=C_0;
      DataCont Ax(anc);
      MidasVector dummyvec(anc);
      X.GetCol(dummyvec,i);
      DataCont EigenVec(dummyvec);
      mpTrf->Transform(EigenVec, Ax, I_1, I_0, dummy);
      
      Ax.Axpy(EigenVec, -TestEigenVals[i]);
      Residuals[i] = Ax.Norm();
   }

   string filename = mFilePrefix + "_residuals";
   ofstream residual_file(filename.c_str(), ios_base::trunc);
   residual_file << scientific << setprecision(20);
   
   for(In i=0; i<ItNum; i++)
   {
      residual_file << TestEigenVals[i] << "  " << Residuals[i] << endl;
   }
}

void BandLanczosChain::CalcOrtho()
{
   Mout << " I R Calculating Q matrix orthonormality " << endl;
   In nc = mpTrf->NexciXvec() - I_1;
   // Setup file containing dot products..
   string filename = mFilePrefix + "_q_orthogonality";
   ofstream orto_file(filename.c_str(), ios_base::trunc);
   orto_file << scientific << setprecision(20);
   
   ostringstream os;
   for (In i=I_0; i<mQ.size(); i++)
   {
      DataCont qi;
      os.str("");
      os << mFilePrefix << "_q" << i;
      qi.GetFromExistingOnDisc(nc, os.str());
      qi.SaveUponDecon(true);
      for (In j=I_0; j<=i; j++)
      {
         DataCont qj;
         ostringstream os;
         os << mFilePrefix << "_q" << j;
         qj.GetFromExistingOnDisc(nc, os.str());
         qj.SaveUponDecon(true);
         orto_file << i << " " << j << " " << Dot(qi,qj) << endl;
      }
   }
}

void BandLanczosChain::Test_T(const In ItNum, const In anc)
{
   Mout << " I R Testing T-matrix " << endl;
   MidasMatrix T_test;
   MidasMatrix Diff;
   Nb diff_norm;
   Nb diff_norm2;

   T_test.SetNewSize(mDef.GetLength(), false, true);
   Diff.SetNewSize(mDef.GetLength(), false, true);
   
   for(In i=0; i<ItNum; i++)
   {
      Nb dummy=C_0;
      DataCont qj(anc);
      mpTrf->Transform(mQ[i], qj, I_1, I_0, dummy);
      for(In j=0; j<ItNum; j++)
      {
         T_test[i][j] = Dot(qj,mQ[j]);
      }
   }
   
   Diff = T_test - mT;
   diff_norm = Diff.Norm();
   diff_norm2 = Diff.Norm2();

   string filename1 = mFilePrefix + "_T_test";
   ofstream t_test_matrix_file(filename1.c_str(), ios_base::trunc);
   t_test_matrix_file << scientific << setprecision(20);
   t_test_matrix_file << T_test << endl;

   string filename2 = mFilePrefix + "_Diff";
   ofstream diff_matrix_file(filename2.c_str(), ios_base::trunc);
   diff_matrix_file << scientific << setprecision(20);
   diff_matrix_file << diff_norm << endl;
   diff_matrix_file << diff_norm2 << endl << endl;
   diff_matrix_file << Diff <<endl;
}
