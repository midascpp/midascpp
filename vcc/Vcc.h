/**
************************************************************************
* 
* @file                Vcc.h 
*
* Created:             03-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Vcc class and function declarations.
* 
* Last modified: Mon May 17, 2010  01:34PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCC_H
#define VCC_H

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "ni/OneModeInt.h"
#include "input/VccCalcDef.h"
#include "vscf/Vscf.h"
#include "vcc/Xvec.h"
#include "util/MultiIndex.h"

// Forward declaration
class TensorNlSolver;
class VccTransformer;

// Types of correlated calculations.
const In VCC_METHOD_UNDEFINED = 0;
const In VCC_METHOD_VCI       = 1;
const In VCC_METHOD_VCC       = 2;

/**
 *
 **/
class Vcc
   :  public Vscf
{
   private:
      In          mCorrMethod;              ///< Type of correlated calc. being done at the moment.
                                            ///< Should be one of the VCC_METHOD_* constants.
      VccCalcDef* mpVccCalcDef;             ///< A pointer to the CalcDef 
      Nb          mEvmp0;                   ///< The zeroth order Vmp energy;

      //! Use VCC energy as reference for Jacobian-diagonal approximations
      bool mUseVccRspRefE = false;
      
      std::unique_ptr<VccTransformer> mRspTrf;  ///< Temporary(really?) solution to do response calcs. in
                                                ///< new transformer framework.
      /*********************************************************************//**
       * @name Results from last ground state calc
       *
       * Previously (prior to May 2019) all results from the equation solver
       * are "lost" from memory when exiting the DoVcc() method. (May be
       * accessible on disk, but this is an inconvenient/fragile way of
       * storing/reaccessing the date.)
       * These attributes introduce a way of storing and retrieving results
       * form the last calculation if so desired.
       *
       * Mostly intended for small-scale testing purposes. Not implemented for
       * all methods (MBH, May 2019).
       ************************************************************************/
      //!@{
      //! Did the last ground state calculation converge?
      bool mGsCalcConverged = false;

      //! The final energy in the last (ground state) calculation.
      Nb mGsFinalEnergy = 0;

      //! Whether to store ground state vector in memory after the calculation.
      bool mGsStoreObjectsInMem = false;

      //! The g.s. vec. from last calc., if mGsStoreObjectsInMem and there's room for it in mem.
      std::unique_ptr<DataCont> mGsFinalVec = nullptr;

      //! The Xvec MCR from last calc, if mGsStoreObjectsInMem.
      std::unique_ptr<ModeCombiOpRange> mGsCalcMcr = nullptr;

      //! Store convergence, energy, and possibly heavy objects in *this for later use.
      void StoreGsCalcResults(bool, Nb, const DataCont&, const ModeCombiOpRange&);

      MidasVector mSavedFvciVecKet;
      MidasVector mSavedFvciVecBra;
      //!@}
     
      In FCGetOtherCalcDef(); 
      In FCGetOperIdx();


      //! Return TensorDataCont of correct shape
      TensorDataCont ConstructTensorDataCont
         (  bool aAddRef = false
         )  const;

   public:
      Vcc(VccCalcDef* aVccCalcDef, OpDef* apOpDef, BasDef* apBasDef, OneModeInt* apOneModeInt);

      /*********************************************************************
       * Different types of calculation.
       *********************************************************************/
      void DoExpHvci();                      ///< Do ExpHvci calculation 
      void DoVmp();                          ///< Do Vmp calculation 
      void DoVapt();                         ///< Do Vapt calculation 
      void DoVci();                          ///< Do Vci calculation 
      void DoVciH2();                        ///< Do VciH2 calculation 
      void DoVcc();                          ///< Do Vcc calculation 
      void DoStandardVcc(VccTransformer*);   ///< Do VCC calculation with the "old" solver
      void DoTensorVcc(VccTransformer*);     ///< Do VCC calculation with TensorNlSolver
      void DoSubspaceSolverCalc();           ///< SubspaceSolver calculations (type and trf in calcdef)
      // *******************************************************************

      void NextOrderVmpWf(VccTransformer& aTrf, DataCont* apPertVect,const In& arOrder,
                          const MidasVector& arEvmps);
      ///< Generate wave function in next order.

      /*********************************************************************
       * Method for targeting.
       *********************************************************************/
      void AutoTarget();                 ///< Prepare target state info from CalcDef.
      In PrepareTargetStates(const string& aTargetsFile, const string& aTargetVectors,
                             const Xvec& arXvec, const bool aResponse = false);
      ///< Prepare target states. Return no. of states actually generated.
      bool TargetAvailableForState(In aNeq,DataCont& arDc);
      void RestartWithTargets(vector<DataCont>& apVecs, In aNroots, In aNvecSize);
      void TargetOut(MidasVector& arEigVec);
      // *******************************************************************
      
      /*********************************************************************
       * Response related methods.                               
       *********************************************************************/
      void PrepareRspTransformer();
      void SetRspTransformerAmplitudeFile
         (  const std::string&
         );
      void MakeRspFuncsNonVar();
      void DeleteRspTransformer();
      Transformer* GetRspTransformer(bool aRight=true) override;
      void CleanupRspTransformer(Transformer* aTrf) override;
      
      void CheckVciRefForRsp();                  ///< Check VCI reference state.
      void PrepareNrspPar() override;            ///< Set the number of response parameters 
      In PrepareRspTargetStates() override;      ///< Prepare response target vectors.
      //void DensityMatrixAnalysis();              ///< Density Analysis, natural modals etc. 
      
      void RspModesAndLevelsOut(ostream& arOut, const In& aAddr, bool aAllOut) override;
      ///< Output modes corresponding to address in response eigenvector.
      ///< The response eigenvector does not contain the ground state so 1 is added to address. 

      void RspExciLevelWeight(vector<DataCont>& aEigVecs, In aRoots) override;
      ///< Prepare data structures for VCC response calculation.
      
      void MatchLeftRightRspEigVecs(MidasVector& aEigVals, vector<DataCont>& aEigVecs) override;

      void LanczosChainRspDrv() override;
      ///< Controlling evaluation of response functions using Lanczos chains.
     
      void CalcNumVccJacobian();                 ///< Calculate VCC Jacobian numerically.
      void CalcNumVccF() override;               ///< Calculate VCC response F matric numerically.
      // *******************************************************************

      void SetMethod(In aM) {mCorrMethod = aM;}
      In GetMethod() {return mCorrMethod;}
      ///< Get the vcc energy (or vmp energy to some order).
      void ModesAndLevelsOut(ostream& arOut, const In& aI, const Xvec& arXvec,
                             bool aAllOut = true);
      void ModesAndLevelsOutLatex(ostream& arOut, const In& aI, const Xvec& arXvec);
      ///< For an address aI output the modes and levels excited. 
      void FcCalc(); ///< Calculate matrix for FC factors

      //! Calculate Rhs vector.
      void CalculateRhs
         (  DataCont& arRhs
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         )  override;

      //! Calculate Rhs vector.
      void CalculateRhs
         (  TensorDataCont& arRhs
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         )  override;

      //! Calculate VCC XI vector.
      template
         <  class VEC_T
         >
      void CalculateXiImpl
         (  VEC_T& arXi
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         );

      //! Calculate VCC XI vector.
      void CalculateXi
         (  DataCont&
         ,  const std::string&
         ,  In&
         ,  Nb&
         )  override;

      //! Calculate VCC XI vector.
      void CalculateXi
         (  TensorDataCont&
         ,  const std::string&
         ,  In&
         ,  Nb&
         )  override;

      //! Calculate Eta^Y vector.
      void CalculateEta
         (  DataCont& arEta
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         )  override;

      //! Calculate Eta^Y vector.
      void CalculateEta
         (  TensorDataCont& arEta
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         )  override;

      //! Calculate VCC Eta0 (H) vector.
      template
         <  class VEC_T=DataCont
         >
      void CalculateEta0Impl();
      void CalculateEta0() override;
      void CalculateTensorEta0() override;
      
      //! Calculate VCI Eta vector.
      void CalculateVciEta
         (  DataCont& arEta
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         );

      //! Calculate VCC etaX vector.
      template
         <  class VEC_T = DataCont
         >
      void CalculateVccEtaX
         (  VEC_T& arEta
         ,  const string& arOperName
         ,  In arOperNr
         );

      void CalculateFR
         (  DataCont& aRes
         ,  In aState
         )  override;

      void CalculateFR
         (  TensorDataCont& aRes
         ,  In aState
         )  override;
      
      Nb ResidContractGtoXnonVar(In aRspFunc, DataCont& aReigVec, Nb aEtaR) override;
      Nb ResidContractGtoXnonVar(In aRspFunc, const TensorDataCont& aReigVec, Nb aEtaR) override;
      
      //! Contract linear response function
      void LrfContractNonVar
         (  In aRspFunc
         )  override;

      //! Contract linear response function
      void TensorLrfContractNonVar
         (  In aRspFunc
         )  override;
      
      void CalculateSigma
         (  DataCont& arEigVec
         ,  DataCont& arSigma
         ,  const std::string& arOperName
         ,  const In& arOperNr
         ,  Nb& arExptValue
         )  override;
      ///< Calculate sigma vector 

      void CalculateSigma
         (  const TensorDataCont& arEigVec
         ,  TensorDataCont& arSigma
         ,  const std::string& arOperName
         ,  const In& arOperNr
         ,  Nb& arExptValue
         )  override;
      ///< Calculate sigma vector 

      void GeneralFTransformation(DataCont&,DataCont&,Nb&) override; 

      VccCalcDef* pVccCalcDef() const {return mpVccCalcDef;}
      void UpdateEvmp0();
      Nb GetEvmp0() {return mEvmp0;}

      void FranckCondon();
      virtual bool RspRestartFromPrev(Transformer* apTrf, DataCont& arDc, const In& arIEq,
            const In& arNVecSize, string aStorage) override;
      bool RestartFromPrev(const Xvec& arXvec, DataCont& arDc, const In& arIEq,
            const In& arNVecSize, string aStorage, string aFilePrefix, const bool aInclRef=true);
      virtual void WriteRspRestartInfo(Transformer* apTrf, const In& arNVecSize) override;
      void WriteRestartInfo(const In& arNVecSize, string aFilePrefix, string aVecName);

      ///> Restart tensor VCC calculation
      bool RestartFromPrev
         (  TensorDataCont&
         ,  TensorNlSolver&
         ,  const std::string&
         )  const;

      void CompareTransformers(const bool aVcc);
      ///< Compare original and V3 VCC or VCI transformers. For debugging.
      
      void CompareVccRspTransformers();
      ///< Compare original and V3 VCC response transformers. For debugging.

      void CheckV3Left();
      ///< Check V3 left-hand transformer.
      
      void ConstructJacobian(VccTransformer& aTrf, MidasMatrix& aJac);
      // Construct the Jacobian using aTrf object and store it a aJac.

      void CmpLJacTransformers();
      // Compare left- and right-hand general Jacobian transformers. For debugging.
      
      void TestLA(In aCalc);
      MidasMatrix PrintJacBlock(In aRow, In aCol, MidasMatrix aJac, In aSin, In aDoub);
      
      //void OneBodyDensity(const Xvec& arXvec, const DataCont& arDC);

      void PrintWeightInfo(const vector<vector<In> >& ar1,DataCont& ar2, In ar3 = I_0) override;

      //! Analyze the VCC solution vector
      void VccSolutionVectorAnalysis(DataCont&, VccTransformer*, const In&);

      //! Get/Set UseVccRspRefE
      void SetUseVccRspRefE(bool aB) { mUseVccRspRefE = aB; }
      bool GetUseVccRspRefE() const  { return mUseVccRspRefE; }

      /*********************************************************************//**
       * @name Results from last ground state calc
       ************************************************************************/
      //!@{
      bool GsCalcConverged() const {return mGsCalcConverged;}
      Nb GsFinalEnergy() const {return mGsFinalEnergy;}
      void GsStoreObjectsInMem(bool a) {mGsStoreObjectsInMem = a;}
      const DataCont& GsFinalVec() const {if(!mGsStoreObjectsInMem){MIDASERROR("mGsStoreObjectsInMem == nullptr");} return *mGsFinalVec;}
      const ModeCombiOpRange& GsCalcMcr() const {if(!mGsCalcMcr){MIDASERROR("mGsCalcMcr == nullptr");} return *mGsCalcMcr;}
      const MidasVector& SavedFvciVecKet() const {return mSavedFvciVecKet;}
      const MidasVector& SavedFvciVecBra() const {return mSavedFvciVecBra;}
      //!@}
};

#endif

// Include implementation
#include "vcc/Vcc_Impl.h"
