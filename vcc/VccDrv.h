/**
************************************************************************
* 
* @file                VccDrv.h 
*
* Created:             03-11-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   VccDrv header.
* 
* Last modified: Mon May 17, 2010  01:34PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCCDRV_H_INCLUDED
#define VCCDRV_H_INCLUDED

namespace midas
{
namespace vcc
{

//! Driver for Vibrational Correlation Calculation (VCC)
void VccDrv();

} /* namespace vcc */
} /* namespace midas */

#endif /* VCCDRV_H_INCLUDED */
