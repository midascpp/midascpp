/**
************************************************************************
* 
* @file                TensorSumAccumulator.h
*
* Created:             21-03-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORSUMACCUMULATOR_H_INCLUDED
#define TENSORSUMACCUMULATOR_H_INCLUDED

#include "TensorSumAccumulator_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "TensorSumAccumulator_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TENSORSUMACCUMULATOR_H_INCLUDED */
