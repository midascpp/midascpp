/**
************************************************************************
* 
* @file                ExpHvci.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing ExpHvci class methods.
* 
* Last modified: Mon Apr 21, 2008  01:28PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/ModeCombiOpRange.h"
#include "util/Io.h"
#include "util/MultiIndex.h"
#include "vcc/Vcc.h"
#include "vcc/ExpHvci.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/Diag.h"
#include "ni/OneModeInt.h"
#include "vcc/VccTransformer.h"

/**
* Default "zero" Constructor 
* */
ExpHvci::ExpHvci(const VccCalcDef* const apVccCalcDef, const OpDef* const apOpDef, const BasDef* apBasDef,
      OneModeInt* const apOneModeInt, Vcc* const apVcc, const string& aDiagMeth,
      const In& aMaxExciLevel) : mH(0),
   mpVccCalcDef(apVccCalcDef),mpVcc(apVcc),mpOpDef(apOpDef),
   mpBasDef(apBasDef),mpOneModeInt(apOneModeInt),mDiagMeth(aDiagMeth)
{
   // Save info 
   mMaxExciLevel = aMaxExciLevel;
   mHamDim  = 0;
}

/**
* Diagonalize H 
* */
void ExpHvci::DiagonalizeHexp()
{
   if (mpVccCalcDef->IoLevel() > 5 || gDebug) Mout << " In ExpHvci::Diagonalize" << endl;
   MidasMatrix eig_vec(mHamDim);
   MidasVector eig_val(mHamDim);
   Diag(mH,eig_vec,eig_val,mDiagMeth,true);

   Mout << " Eigen values for explicit Hamiltonian Vci with max exci level =  " << mMaxExciLevel << endl;
   Mout << " Nr.   Eigen value              (Eigen value (cm-1))      Relative to lowest      (Relative to Lowest (cm-1))" << endl;
   for (In i=0;i<mHamDim;i++) Mout << " ExpHvci_" << setw(5) << i << " " 
      << setw(24) << eig_val[i] << " (" << setw(22) << eig_val[i]*C_AUTKAYS << ") "
      << setw(24) << eig_val[i]-eig_val[0] << "(" << setw(23) << (eig_val[i]-eig_val[0])*C_AUTKAYS << ")"<< endl;
   if (gDebug && mpVccCalcDef->IoLevel() > 20) 
   {
      Mout << " Eigen vector matrix" << endl;
      Mout << eig_vec << endl;
   }

   if (mpVccCalcDef->TestTransformer())
   {
      // Seidler: Dangerous cast from const to non-const. Init will change once the final frame-
      // work is done though.
      VccTransformer trf(mpVcc, (VccCalcDef*)mpVccCalcDef, mpVcc->pOpDef(), mpVcc->pIntegrals(),
                      mpVcc->GetEvmp0());
      
      if (mHamDim != trf.NexciXvec() || mHamDim != trf.NexciTransXvec())
      {
         Mout << " ExpHvci and Vci dimensions are not identical.  Skip transf. test " << endl;
         Mout << " ExpHvci hamdim:    " << mHamDim << endl;
         Mout << " Vci dim n_x:       " << trf.NexciXvec()  << endl;
         Mout << " Vci dim n_trans_x: " << trf.NexciTransXvec()  << endl;
      }
      else
      {
         Mout << " Testing linear transformations and testing calculated eigen values " << endl;
         In ntestl = 0;                   // = 0 amDim;
         In ntesth = min(I_1,mHamDim-1);  // = mHamDim-1;
         for (In i=ntestl;i<=ntesth;i++)
         {
            if (gDebug) Mout << " Root " << i << " eigval " << eig_val[i] << endl;
            MidasVector eigv(mHamDim);
            MidasVector trans(mHamDim);
            eig_vec.GetCol(eigv,i);
            if (gDebug) Mout << " The eigen vector\n " << eigv << endl;
            trf.AssignXvec(eigv);
            trf.GenTransformer();
            trf.GetTransXvec(trans);
            if (gDebug) Mout << " The transformed vector:\n " << eigv << endl;
            trans -= eig_val[i]*eigv;
            Mout << " TESTTRANSFORMER: Root " << i << " " << eig_val[i];
            Mout << " Residual norm " << trans.Norm() << endl;
         }

         bool construct_ham = false;
         if (construct_ham)
         {
            Mout << "\n\n\n\n TESTING BY EXPLICIT CONSTRUCTING THE HAMILTONIAN " << endl;
            eig_vec.Unit();
            
            for (In i=0;i<mHamDim;i++)
            {
               MidasVector eigv(mHamDim);
               MidasVector trans(mHamDim);
               eig_vec.GetCol(eigv,i);
               trf.AssignXvec(eigv);
               trf.GenTransformer();
               trf.GetTransXvec(trans);
               eig_vec.AssignCol(trans,i);
            }
            if (gDebug) Mout << " Hamiltonian constructed \n " << eig_vec << endl;
            eig_vec -= mH;
            Mout << " Norm of H - H  should be zero : " << eig_vec.Norm()
               << " largest abs difference: " << eig_vec.LargestAbsElement() << endl;
            if (gDebug) Mout << " Difference hamiltonian: \n " << eig_vec <<endl;
         }
      }
   }
}
/**
* Construct H matrix
* */
void ExpHvci::ConstructHexp() 
{
   if (mpVccCalcDef->IoLevel() > 5 || gDebug) Mout << " In ExpHvci::Construct " << endl;
   In ndim = mpOneModeInt->GetmNoModesInInt();

   vector<In> max_ex_pr_mode;
   mpVccCalcDef->GetMaxExPrMode(max_ex_pr_mode);
   ModeCombiOpRange range = ConstructModeCombiOpRange(mMaxExciLevel,max_ex_pr_mode,ndim);

   vector<In> occ_vec(ndim);      // The reference full occupation vector = 0
   for (In i=0;i<ndim;i++) occ_vec[i] =0;
   vector<In> occmax_vec(ndim);      // The maximum full occupation vector.
   if (mpVccCalcDef->UsePrimitiveBasis())
   {
      for (LocalModeNr i_op_mode=0;i_op_mode<ndim;i_op_mode++) 
      {
         GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
         LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
         In nbas       = mpBasDef->Nbas(i_bas_mode);
         occmax_vec[i_op_mode]   = nbas - I_1;
      }
   }
   else
   {
      for (In i_op_mode=0;i_op_mode<ndim;i_op_mode++) 
         occmax_vec[i_op_mode]   = mpVccCalcDef->Nmodals(i_op_mode) - I_1;
   }

   string lowhig = "LOWHIG";
   bool exci_only=true;

   In n_mode_combi = range.Size();
   In idx1=-1;

   // First count to find dimension of problem.
   for (In i_mode_combi1=I_0;i_mode_combi1<n_mode_combi;i_mode_combi1++)
   {
      ModeCombi modes_excited = range.GetModeCombi(i_mode_combi1);
      In n_excited1 = modes_excited.Size();
      if (mpVccCalcDef->IoLevel() > 5 || gDebug) 
      {
         Mout << " 1: The modes excited are: ";
         for (In i=I_0;i<n_excited1;i++) Mout << " " << modes_excited.Mode(i);
         Mout << endl;
      }

      vector<In> occ1(n_excited1);
      vector<In> occmax1(n_excited1);

      for (In i=I_0;i<n_excited1;i++) 
      {
         In mode   = modes_excited.Mode(i);
         occ1[i]    = occ_vec[mode]; 
         occmax1[i] = occmax_vec[mode];
      }
      if (mpVccCalcDef->IoLevel() > 50 || gDebug )
      {
         Mout << " partial occ ";
         for (In i=0;i<n_excited1;i++) Mout << " " << occ1[i];
         Mout << endl;
         Mout << " partial occmax ";
         for (In i=0;i<n_excited1;i++) Mout << " " << occmax1[i];
         Mout << endl;
      }

      MultiIndex mi_for_modecombi1(occ1,occmax1,lowhig,exci_only);
      In n_vcc = mi_for_modecombi1.Size();
      idx1+=n_vcc;
   }

   // Open file for dumping Hamiltonian elements and excitation vectors 
   std::ofstream h_exci_vec_fstr{"h_exci_vec.dat"};
   h_exci_vec_fstr << std::setprecision(17) << std::scientific;

   // Set Hamiltonian size
   mHamDim = idx1+I_1;
   mH.SetNewSize(mHamDim,false);
   Mout << " Hamiltonian dimension = " << mHamDim << endl;


   // Reinit 
   idx1=-1;

   // Loop left index by mode_combi & exci for mode_combi.
   for (In i_mode_combi1=0;i_mode_combi1<n_mode_combi;i_mode_combi1++)
   {
      ModeCombi modes_excited = range.GetModeCombi(i_mode_combi1);
      In n_excited1 = modes_excited.Size();
      if (mpVccCalcDef->IoLevel() > 5 || gDebug)
      {
         Mout << " 1: The modes excited are: ";
         for (In i=0;i<n_excited1;i++) Mout << " " << modes_excited.Mode(i);
         Mout << endl;
      }

      vector<In> occ1(n_excited1);
      vector<In> occmax1(n_excited1);

      for (In i=0;i<n_excited1;i++) 
      {
         In mode   = modes_excited.Mode(i);
         occ1[i]    = occ_vec[mode]; 
         occmax1[i] = occmax_vec[mode];
      }
      if (mpVccCalcDef->IoLevel() > 50 || gDebug )
      {
         Mout << " partial occ ";
         for (In i=0;i<n_excited1;i++) Mout << " " << occ1[i];
         Mout << endl;
         Mout << " partial occmax ";
         for (In i=0;i<n_excited1;i++) Mout << " " << occmax1[i];
         Mout << endl;
      }

      MultiIndex mi_for_modecombi1(occ1,occmax1,lowhig,exci_only);

      vector<In> occ21(occ1);             // The active part.
      vector<In> ivec1(occ_vec);  // The full occupation vector.

      In n_vcc = mi_for_modecombi1.Size();
      for (In i1=0;i1<n_vcc;i1++)
      {
         mi_for_modecombi1.IvecForIn(occ21,i1);
         for (In i=0;i<n_excited1;i++) 
         {
            In mode   = modes_excited.Mode(i);
            ivec1[mode] = occ21[i]; 
         }
         idx1++;

         // Loop right index by mode_combi & exci for mode_combi.
         In idx2=-1;
         for (In i_mode_combi2=0;i_mode_combi2<n_mode_combi;i_mode_combi2++)
         {
            ModeCombi modes_excited2 = range.GetModeCombi(i_mode_combi2);
            In n_excited2 = modes_excited2.Size();
            if (mpVccCalcDef->IoLevel() > 5 || gDebug) 
            {
               Mout << " 2: The modes excited are: ";
               for (In i=0;i<n_excited2;i++) Mout << " " << modes_excited2.Mode(i);
               Mout << endl;
            }
      
            vector<In> occ2(n_excited2);
            vector<In> occmax2(n_excited2);
      
            for (In i=0;i<n_excited2;i++) 
            {
               In mode   = modes_excited2.Mode(i);
               occ2[i]    = occ_vec[mode]; 
               occmax2[i] = occmax_vec[mode];
            }
            if (mpVccCalcDef->IoLevel() > 50 || gDebug )
            {
               Mout << " partial occ ";
               for (In i=0;i<n_excited2;i++) Mout << " " << occ2[i];
               Mout << endl;
               Mout << " partial occmax ";
               for (In i=0;i<n_excited2;i++) Mout << " " << occmax2[i];
               Mout << endl;
            }
      
            MultiIndex mi_for_modecombi2(occ2,occmax2,lowhig,exci_only);
      
            vector<In> occ22(occ2);             // The active part.
            vector<In> ivec2(occ_vec);  // The full occupation vector.
      
            In n_vcc = mi_for_modecombi2.Size();
            for (In i2=0;i2<n_vcc;i2++)
            {
               mi_for_modecombi2.IvecForIn(occ22,i2);
               for (In i=0;i<n_excited2;i++) 
               {
                  In mode   = modes_excited2.Mode(i);
                  ivec2[mode] = occ22[i]; 
               }
               idx2++;
            
               if (mpVccCalcDef->IoLevel() > 50 || gDebug )
               {
                  Mout << " ivec1    ";
                  for (In i=0;i<ndim;i++) Mout << " " << ivec1[i];
                  Mout << endl;
                  Mout << " ivec2    ";
                  for (In i=0;i<ndim;i++) Mout << " " << ivec2[i];
                  Mout << endl;
                  Mout << " idx1 " << idx1 << " idx2 " << idx2 << endl;
               }


               mH[idx1][idx2] = C_0;
               if (mpVccCalcDef->UsePrimitiveBasis())
               {
                  for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
                  {
                     Nb cont = mpOpDef->Coef(i_term);
                     for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
                     {
                        if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term))
                        {
                           if (ivec1[i_op_mode] != ivec2[i_op_mode])
                           {
                              cont = C_0;
                              break;
                           }
                        }
                        else 
                        {
                            LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                            Nb int_val;
                            mpOneModeInt->GetInt(i_op_mode,i_oper,
                              ivec1[i_op_mode],ivec2[i_op_mode],int_val);
                            cont *= int_val;
                        }
                     }
                     mH[idx1][idx2] += cont;
                  }
               }
               else
               {
                  for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
                  {
                     Nb cont = mpOpDef->Coef(i_term);
                     for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
                     {
                        if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term))
                        {
                           if (ivec1[i_op_mode] != ivec2[i_op_mode])
                           {
                              cont = C_0;
                              break;
                           }
                        }
                        else 
                        {
                            LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                            cont *= mpVcc->pIntegrals()->GetElement(i_op_mode,i_oper,ivec1[i_op_mode],ivec2[i_op_mode]);
                        }
                     }
                     mH[idx1][idx2] += cont;
                     if (gDebug)
                     {
                        Mout << " term = " << i_term << " idx1 " << idx1 << " idx2 " 
                              << idx2 << " cont " << cont << " tot " << mH[idx1][idx2] << endl;
                     }
                  }
               }

               // ---------------------------------------------------
               // Write out file with Hamiltonian elements and excitation vectors
               // ---------------------------------------------------
               if(mH[idx1][idx2] != 0.0)
               {
                  h_exci_vec_fstr << std::showpos << mH[idx1][idx2] << std::noshowpos << " " << ivec1 << " " << ivec2 << std::endl;
               }
            }
         }
      }
   }
   if (gDebug && gIoLevel > 20) Mout << " hamiltonian : " << endl << mH << endl;
   bool output_h=true;
   if (output_h)
   {
      ofstream ham_out("EXPHVCI.MAT");
      ham_out.setf(ios_base::scientific, ios_base::floatfield);
      ham_out.precision(16);
      for (In i1=0;i1<mHamDim;i1++)
      {
         for (In i2=0;i2<mHamDim;i2++)
         {
            ham_out << setw(28) << mH[i1][i2] << "   " << i1 << " " << i2 << endl;
         }
      }
   }
}
