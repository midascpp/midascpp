/**
************************************************************************
* 
* @file    IntegralContractor.h
*
* @date    01-04-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*          Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* @brief
*     Class for performing contractions with integrals
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef INTEGRALCONTRACTOR_H_INCLUDED
#define INTEGRALCONTRACTOR_H_INCLUDED

#include "tensor/NiceTensor.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"

namespace midas::vcc
{
//! Integral types
enum class IntegralOpT : unsigned int { STANDARD=0, TRANSP, CONJ };

namespace detail
{

/**
 * Class for performing contractions on DataCont and NiceTensor with integrals
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T = GeneralMidasVector
   ,  template<typename> typename MAT_T = GeneralMidasMatrix
   >
class IntegralContractor
{
   public:
      using param_t = T;
      template<typename U> using vec_templ_t = VEC_T<U>;
      template<typename U> using mat_templ_t = MAT_T<U>;
      using vec_t = vec_templ_t<param_t>;
      using mat_t = mat_templ_t<param_t>;

      //! Contraction types. (NUM_OF_TYPES _only_ to count the types.)
      enum ContT: unsigned int { PASSIVE=0, DOWN, UP, FORWARD, NUM_OF_TYPES };

      /*********************************************************************//**
       * @name Contractions
       ************************************************************************/
      //!@{

      /***************************************************************************//**
       * @warning
       *    Due to the use of mContractContainer
       *    I think this function is **not thread safe!!**
       *
       * @param[in] arM1
       *    ModeCombi for contraction _result_ parameters.
       * @param[in] arM0
       *    ModeCombi for contraction _input_ parameters.
       * @param[in] aOperMode
       *    Operator mode for which contraction is performed.
       * @param[in] aOper
       *    Operator term for contraction.
       * @param[out] arY1
       *    Resulting parameters. This vector _must_ be zero and have the correct
       *    size prior to calling this function.
       * @param[in] arY0
       *    Input parameters.
       * @param[in] aWhich
       *    -I_1: Down,   I_0: Forward,   I_1: Up
       * @param[in] aIntegrals
       *    Integrals to contract with
       * @param[in] aNModals
       *    Dimensions of integral matrices
       * @param[in] aIntType
       *    Transpose, conj, etc. the integrals?
       ******************************************************************************/
      void Contract
         (  const ModeCombi& arM1
         ,  const ModeCombi& arM0
         ,  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  vec_t& arY1
         ,  const vec_t& arY0
         ,  In aWhich
         ,  const std::vector<std::vector<mat_t> >& aIntegrals
         ,  const std::vector<Uin>& aNModals
         ,  IntegralOpT aIntType = IntegralOpT::STANDARD
         )  const
      {
         if (gDebug) 
         {
            Mout << " In Contractor start " << endl;
            if (aWhich==I_1) Mout << " Up contracting" << endl;
            if (aWhich==-I_1) Mout << " Down contracting" << endl;
            if (aWhich==I_0) Mout << " Forward contracting" << endl;
            Mout << " arM0 " << arM0 << " size of arY0 " << arY0.Size() << endl;
            Mout << " arM1 " << arM1 << " size of arY1 " << arY1.Size() << endl;
         }
         In ihit;
         if (aWhich==-I_1)
         {
            ihit = arM0.IdxNrForMode(aOperMode);
            if (gDebug) Mout << " Index ihit " << ihit << " mode  " << 
               aOperMode << " should be " << arM0.Mode(ihit) << endl;
            if (aOperMode != arM0.Mode(ihit)) 
               MIDASERROR( "Contractor something is wrong with ihit index ");
         }
         else 
         {
            ihit = arM1.IdxNrForMode(aOperMode);
            if (gDebug) Mout << " Index ihit " << ihit << " mode  " << 
               aOperMode << " should be " << arM1.Mode(ihit) << endl;
            if (aOperMode != arM1.Mode(ihit)) 
               MIDASERROR( "Contractor something is wrong with ihit index ");
         }
      
         In nmoda = aNModals[aOperMode];
         In nint = nmoda;
         if (aWhich == 0) nint *= nmoda;
         this->ContractContainer().SetNewSize(nint, false);
         switch   (  aIntType
                  )
         {
            case IntegralOpT::STANDARD:
            {
               if (aWhich==-I_1) this->GetModalInt(aOperMode,aOper,aIntegrals,"ip");
               if (aWhich==I_1) this->GetModalInt(aOperMode,aOper,aIntegrals,"pi");
               if (aWhich==I_0) this->GetModalInt(aOperMode,aOper,aIntegrals,"all");
               break;
            }
            case IntegralOpT::TRANSP:
            {
               if (aWhich==-I_1) this->GetTranspModalInt(aOperMode,aOper,aIntegrals,"ip");
               if (aWhich==I_1) this->GetTranspModalInt(aOperMode,aOper,aIntegrals,"pi");
               if (aWhich==I_0) this->GetTranspModalInt(aOperMode,aOper,aIntegrals,"all");
               break;
            }
            case IntegralOpT::CONJ:
            {
               if (aWhich==-I_1) this->GetConjModalInt(aOperMode,aOper,aIntegrals,"ip");
               if (aWhich==I_1) this->GetConjModalInt(aOperMode,aOper,aIntegrals,"pi");
               if (aWhich==I_0) this->GetConjModalInt(aOperMode,aOper,aIntegrals,"all");
               break;
            }
            default:
            {
               MIDASERROR("Integral type not recognized!");
            }
         }
      
         In n_after   = I_1; // number of elements after contraction idx
         In n_bef     = I_1; // number of elements before contraction idx
         In n_a_after = I_1;
         for (In i=0;i<ihit;i++) 
         {
            In i_op_mode =  arM0.Mode(i);
            In nmod      = aNModals[i_op_mode];
            n_bef *= nmod-I_1;
         }
         if (aWhich == -I_1) 
         {
            In ndim = arM0.Size();
            for (In i=ihit+I_1;i<ndim;i++) 
            {
               In i_op_mode =  arM0.Mode(i); // M0 since down contract.
               In nmod      = aNModals[i_op_mode];
               n_after *= nmod-I_1;
            }
         }
         else 
         {
            In ndim = arM1.Size();
            for (In i=ihit+I_1;i<ndim;i++) 
            {
               In i_op_mode =  arM1.Mode(i); // M1 since up contract (or forward, both will do).
               In nmod      = aNModals[i_op_mode];
               n_after *= nmod-I_1;
            }
         }
         In n=I_1;
         if (aWhich!=I_1) n = (aNModals[arM0.Mode(ihit)]-I_1);
         if (aWhich==I_1) n = (aNModals[arM1.Mode(ihit)]-I_1);
         n_a_after = n_after*n;
      
         if (aWhich==I_0) // Forward contraction.
         {
            In i_a_off=I_0;
            for (In i_bef=I_0;i_bef<n_bef;i_bef++)
            {
               In i_a_off2 = i_a_off;
               for (In a=I_0;a<n;a++)
               {
                  In i_b_off2 = i_a_off;
                  for (In b=I_0;b<n;b++)
                  {
                     In i_a = i_a_off2;
                     In i_b = i_b_off2;
                     In i_ab = nmoda*(a+I_1)+b+I_1;
                     for (In i_after=I_0;i_after<n_after;i_after++)
                     {
                        arY1[i_a++] += arY0[i_b++]*this->ContractContainer()[i_ab];
                     }
                     i_b_off2 += n_after;
                  }
                  i_a_off2 += n_after; 
               }
               i_a_off += n_a_after;
            }
         }
         else if (aWhich==I_1) // Up contraction.
         {
            In i_a_off=I_0;
            In i_0_off=I_0;
            for (In i_bef=I_0;i_bef<n_bef;i_bef++)
            {
               In i_a_off2 = i_a_off;
               for (In a=I_0;a<n;a++)
               {
                  In i_a = i_a_off2;
                  In i_0 = i_0_off; 
                  In i_a0 = a+I_1;
                  for (In i_after=I_0;i_after<n_after;i_after++)
                  {
                     arY1[i_a++] += arY0[i_0++]*this->ContractContainer()[i_a0];
                     
                  }
                  i_a_off2 += n_after; 
               }
               i_a_off += n_a_after;
               i_0_off += n_after;
            }
         }
         else if (aWhich==-I_1) // Down contraction.
         {
            bool done_stuff = false;
            In i_a_off=I_0;
            In i_0_off=I_0;
            for (In i_bef=I_0;i_bef<n_bef;i_bef++)
            {
               In i_a_off2 = i_a_off;
               for (In a=I_0;a<n;a++)
               {
                  In i_a = i_a_off2;
                  In i_0 = i_0_off; 
                  In i_a0 = a+I_1;
                  for (In i_after=I_0;i_after<n_after;i_after++)
                  {
                     arY1[i_0++] += arY0[i_a++]*this->ContractContainer()[i_a0];
                  }
                  i_a_off2 += n_after; 
               }
               i_a_off += n_a_after;
               i_0_off += n_after;
            }
         }
      }

      //@{
      /**
       * Make down contraction of NiceTensor with integrals
       * @param aOperMode         The operator mode to be contracted
       * @param aOper             Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
       * @param aIdx              The index to be contracted
       * @param aIntType          Operator to perform on integrals (transpose, conjugate, etc.)
       * @param aTensorPtr        The tensor to be contracted
       * @param aIntegrals        Integrals
       * @param aNModals          Number of modals per mode
       * @result                  The result of the contraction in NiceTensor format
       **/
      NiceTensor<param_t> ContractDown
         (  const In aOperMode
         ,  const In aOper
         ,  const In aIdx
         ,  IntegralOpT aIntType
         ,  const NiceTensor<param_t>& aTensorPtr
         ,  const std::vector<std::vector<mat_t> >& aIntegrals
         ,  const std::vector<Uin>& aNModals
         )  const
      {
         // get modal integrals
         const auto& n_modals = aNModals[aOperMode];
         auto&& contract_container = this->ContractContainer();
         contract_container.SetNewSize(n_modals - 1, false);
         switch   (  aIntType
                  )
         {
            case IntegralOpT::STANDARD:
               this->GetModalInt(aOperMode, aOper, aIntegrals, "ip", true);  
               break;
            case IntegralOpT::TRANSP:
               this->GetTranspModalInt(aOperMode, aOper, aIntegrals, "ip", true);  
               break;
            case IntegralOpT::CONJ:
               this->GetConjModalInt(aOperMode, aOper, aIntegrals, "ip", true);  
               break;
            default:
               MIDASERROR("Did not recognize integral transform type!");
         }
      
         // make simple tensor from data (will take ownership over pointer to data)
         auto length = contract_container.Size();
         auto capacity = contract_container.Capacity();
         auto integrals = make_nice_simple(std::vector<unsigned>{n_modals - 1}, contract_container.data());
         contract_container.SetPointerToNull();
         
         // do contraction and return result
         auto result = aTensorPtr.ContractDown(aIdx, integrals);

         // Move data back to contract_container to avoid re-allocations
         contract_container.SetData(integrals.template StaticCast<SimpleTensor<param_t> >().ReleaseData(), length, capacity);

         return result;
      }

      /**
       * Make forward contraction of NiceTensor with integrals.
       * @param aOperMode          The operator mode to be contracted.
       * @param aOper              Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
       * @param aIdx               The index to be contracted.
       * @param aIntType           Operator to perform on integrals (transpose, conjugate, etc.)
       * @param aTensorPtr         The tensor to be contracted.
       * @param aIntegrals        Integrals
       * @param aNModals          Number of modals per mode
       * @result                   The result of the contraction in NiceTensor format.
       **/
      NiceTensor<param_t> ContractForward
         (  const In aOperMode
         ,  const In aOper
         ,  const In aIdx
         ,  IntegralOpT aIntType
         ,  const NiceTensor<param_t>& aTensorPtr
         ,  const std::vector<std::vector<mat_t> >& aIntegrals
         ,  const std::vector<Uin>& aNModals
         )  const
      {
         // get modal integrals
         const auto& n_modals = aNModals[aOperMode];
         auto len = n_modals - 1;
         auto&& contract_container = ContractContainer();
         contract_container.SetNewSize(len * len, false);
         switch   (  aIntType
                  )
         {
            case IntegralOpT::STANDARD:
               this->GetModalInt(aOperMode, aOper, aIntegrals, "all", true);  
               break;
            case IntegralOpT::TRANSP:
               this->GetTranspModalInt(aOperMode, aOper, aIntegrals, "all", true);  
               break;
            case IntegralOpT::CONJ:
               this->GetConjModalInt(aOperMode, aOper, aIntegrals, "all", true);  
               break;
            default:
               MIDASERROR("Did not recognize integral transform type!");
         }

         // make simple tensor from data (will take ownership over pointer to data)
         auto length = contract_container.Size();
         auto capacity = contract_container.Capacity();
         auto integrals = make_nice_simple(std::vector<unsigned>{len, len}, contract_container.data());
         contract_container.SetPointerToNull();
         
         // do contraction
         auto result = aTensorPtr.ContractForward(aIdx, integrals);

         // Move data back to contract_container to avoid re-allocations
         contract_container.SetData(integrals.template StaticCast<SimpleTensor<param_t> >().ReleaseData(), length, capacity);

         return result;
      }

      /**
       * Make up contraction of NiceTensor with integrals
       * @param aOperMode      The operator mode to be contracted
       * @param aOper          Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
       * @param aIdx           The index to be contracted
       * @param aIntType           Operator to perform on integrals (transpose, conjugate, etc.)
       * @param aTensorPtr     The tensor to be contracted
       * @param aIntegrals        Integrals
       * @param aNModals          Number of modals per mode
       * @result               The result of the contraction in NiceTensor format
       **/
      NiceTensor<param_t> ContractUp
         (  const In aOperMode
         ,  const In aOper
         ,  const In aIdx
         ,  IntegralOpT aIntType
         ,  const NiceTensor<param_t>& aTensorPtr
         ,  const std::vector<std::vector<mat_t> >& aIntegrals
         ,  const std::vector<Uin>& aNModals
         )  const
      {
         // get modal integrals
         const auto& n_modals = aNModals[aOperMode];
         auto&& contract_container = ContractContainer();
         contract_container.SetNewSize(n_modals - 1, false);
         switch   (  aIntType
                  )
         {
            case IntegralOpT::STANDARD:
               this->GetModalInt(aOperMode, aOper, aIntegrals, "pi", true);  
               break;
            case IntegralOpT::TRANSP:
               this->GetTranspModalInt(aOperMode, aOper, aIntegrals, "pi", true);  
               break;
            case IntegralOpT::CONJ:
               this->GetConjModalInt(aOperMode, aOper, aIntegrals, "pi", true);  
               break;
            default:
               MIDASERROR("Did not recognize integral transform type!");
         }
      
         // make simple tensor from data (will take ownership over pointer to data)
         auto length = contract_container.Size();
         auto capacity = contract_container.Capacity();
         auto integrals = make_nice_simple(std::vector<unsigned>{n_modals - 1}, contract_container.data());
         contract_container.SetPointerToNull();
         
         // do contraction and return result
         auto result = aTensorPtr.ContractUp(aIdx, integrals);

         // Move data back to contract_container to avoid re-allocations
         contract_container.SetData(integrals.template StaticCast<SimpleTensor<param_t> >().ReleaseData(), length, capacity);

         return result;
      }
      //@}
      //!@}

   private:
      /*********************************************************************//**
       * @brief
       *    Mutable utility container for avoiding reallocations in contractions.
       *
       * @warning
       *    I don't think this is thread safe!!! If multiple threads use
       *    ModalIntegrals<T>::Contract() simultaneously they'd all call
       *    mContractContainer and thus will be modifying it simultaneously,
       *    right??  Modify accordingly if needed in threaded code.
       ************************************************************************/
      mutable vec_t mContractContainer;

      //! Get temp. storage of integrals (Contract etc.). NOT THREAD SAFE, I think!
      vec_t& ContractContainer
         (
         )  const
      {
         return mContractContainer;
      }

      //! Conjugate elements of mContractContainer
      void ConjugateContractContainer
         (
         )  const
      {
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            for(auto& v : mContractContainer)
            {
               v = midas::math::Conj(v);
            }
         }
         return;
      }

      //@{
      /***************************************************************************//**
       * @param[in] aOperMode
       *    The mode.
       * @param[in] aOper
       *    The operator number wihtin that mode.
       * @param[in] aIntegrals
       *    Integrals to get block from
       * @param[in] aWhich
       *    Can be "ip" (0'th row), "pi" (0'th col), "all" (entire matrix, row by row).
       * @param[in] aDoOffSet
       *    If true uses an offset of 1 when retrieving elements, i.e. doesn't
       *    include the occupied matrix element.
       ******************************************************************************/
      void GetModalInt
         (  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  const std::vector<std::vector<mat_t>>& aIntegrals
         ,  const std::string& aWhich = "all"
         ,  bool aDoOffSet = false
         )  const
      {
         const auto& mat = aIntegrals[aOperMode][aOper];

         const In offset = aDoOffSet? I_1: I_0;
      
         if (aWhich=="ip")
         { 
            mat.GetOffsetRow(this->ContractContainer(), I_0, offset);
         }
         else if (aWhich =="pi")
         {
            mat.GetOffsetCol(this->ContractContainer(), I_0, offset);
         }
         else if (aWhich == "all") 
         {
            this->ContractContainer().MatrixRowByRow(mat, offset, offset);
         }
         else
         {
            MIDASERROR("IntegralContractor::GetModalInt(): Don't understand aWhich option - not equal ip,pi,all");
         }
      }

      /***************************************************************************//**
       * Same as GetModalInt(), but transposed.
       *
       * @param[in] aOperMode
       *    The mode.
       * @param[in] aOper
       *    The operator number wihtin that mode.
       * @param[in] aIntegrals
       *    Integrals to get block from
       * @param[in] aWhich
       *    Can be "ip" (0'th col), "pi" (0'th row), "all" (entire matrix, col by col).
       * @param[in] aDoOffSet
       *    If true uses an offset of 1 when retrieving elements, i.e. doesn't
       *    include the occupied matrix element.
       ******************************************************************************/
      void GetTranspModalInt
         (  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  const std::vector<std::vector<mat_t>>& aIntegrals
         ,  const std::string& aWhich = "all"
         ,  bool aDoOffSet = false
         )  const
      {
         if (aWhich=="ip")
         { 
            this->GetModalInt(aOperMode, aOper, aIntegrals, "pi", aDoOffSet);
         }
         else if (aWhich =="pi")
         {
            this->GetModalInt(aOperMode, aOper, aIntegrals, "ip", aDoOffSet);
         }
         else if (aWhich == "all") 
         {
            const auto& mat = aIntegrals[aOperMode][aOper];
            const In offset = aDoOffSet? I_1: I_0;
            this->ContractContainer().MatrixColByCol(mat, offset, offset);
         }
         else
         {
            MIDASERROR("IntegralContractor::GetModalInt(): Don't understand aWhich option - not equal ip,pi,all");
         }
      }

      /***************************************************************************//**
       * Same as GetModalInt(), but conjugate-transposed.
       *
       * @param[in] aOperMode
       *    The mode.
       * @param[in] aOper
       *    The operator number wihtin that mode.
       * @param[in] aIntegrals
       *    Integrals to get block from
       * @param[in] aWhich
       *    Can be "ip" (0'th col), "pi" (0'th row), "all" (entire matrix, col by col).
       * @param[in] aDoOffSet
       *    If true uses an offset of 1 when retrieving elements, i.e. doesn't
       *    include the occupied matrix element.
       ******************************************************************************/
      void GetConjModalInt
         (  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  const std::vector<std::vector<mat_t>>& aIntegrals
         ,  const std::string& aWhich = "all"
         ,  bool aDoOffSet = false
         )  const
      {
         this->GetTranspModalInt(aOperMode, aOper, aIntegrals, aWhich, aDoOffSet);
         this->ConjugateContractContainer();
      }
      //@}
      //!@}


   public:
      //! default c-tor
      IntegralContractor() = default;

      //! copy assign
      IntegralContractor& operator=
         (  const IntegralContractor& arOther
         )
      {
         if (this != &arOther)
         {
            mContractContainer.SetNewSize(arOther.mContractContainer.Size());
            mContractContainer = arOther.mContractContainer;
         }

         return *this;
      }

      /*********************************************************************//**
       * @name Contraction details
       ************************************************************************/
      //!@{

      //! Reserve size of mContractContainer
      void Reserve
         (  Uin aSize
         )  const
      {
         mContractContainer.Reserve(aSize);
      }

      //!@}
   
};

} /* namespace detail */
} /* namespace midas::vcc */

#endif /* INTEGRALCONTRACTOR_H_INCLUDED */
