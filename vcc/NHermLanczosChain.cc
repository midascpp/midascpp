/**
************************************************************************
* 
* @file                NHermLanczosChain.cc
*
* Created:             25-04-2010
*
* Author:              Bo Thomsen (bo@thomsen.tdcadsl.dk)
*
* Short Description:   Implementation of NHermLanczosChainDef and
*                      NHermLanczosChain classes.
* 
* Last modified: Mon Jan 18, 2010  02:05PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <algorithm>
#include <ctime>

#include "inc_gen/TypeDefs.h"

#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "mmv/Diag.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/NHermLanczosChain.h"
#include "input/Input.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"

using namespace std;

NHermLanczosChainDef::NHermLanczosChainDef(const string& aOper, In aLength, In aOrtho,
                                 vector<string>* const aXopers):
   LanczosChainDefBase(aLength,aOrtho),mOper(aOper)
{
   if (aXopers != NULL)
      mXopers = *aXopers;
}

bool NHermLanczosChainDef::Combine(const NHermLanczosChainDef& aOther)
{
   if (mOper != aOther.mOper ||
          mOrtho != aOther.mOrtho)
          return false;

   mLength = max(mLength, aOther.mLength);
   copy(aOther.mXopers.begin(), aOther.mXopers.end(), back_inserter(mXopers));
   sort(mXopers.begin(), mXopers.end());
   mXopers.erase(unique(mXopers.begin(), mXopers.end()), mXopers.end());
   return true;
}

bool NHermLanczosChainDef::Compatible(const NHermLanczosChainDef& aOther) const
{
   //Mout << aOther.mOper << " " << aOther.mOrtho << " " << aOther.mLength << endl;
   //Mout << mOper << " " << mOrtho << " " << mLength << endl;
   if (mOper != aOther.mOper ||
      mOrtho != aOther.mOrtho ||
      mLength < aOther.mLength)
      return false;

   for (In i=I_0; i<aOther.mXopers.size(); i++)
      if (find(mXopers.begin(), mXopers.end(), aOther.mXopers[i]) == mXopers.end())
        return false;

   return true;
}

ostream& operator<<(ostream& aOut, const NHermLanczosChainDef& aDef)
{
   //aOut << "oper: '" << aDef.mOper << "' length: " << aDef.mLength << " x-opers: ";
   aOut << "oper: '" << aDef.GetOper() << "' length: " << aDef.GetLength() << " x-opers: ";
   for (In i=I_0; i<aDef.mXopers.size()-I_1; ++i)
      aOut << "'" << aDef.mXopers[i] << "',";
   aOut << "'" << aDef.mXopers.back() << "'";
   aOut << " ortho: " << LanczosRspFunc::StringForConst(aDef.mOrtho);
   return aOut;
}

NHermLanczosChain::NHermLanczosChain(const NHermLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf):
   mDef(aDef),
   mpVcc(apVcc), mpTrf(apTrf)
{
   mFilePrefix = "NHlcschn_" + mDef.GetOper() += "_ortho" + std::to_string(mDef.GetOrtho());
   mRestartLen = I_0;
}

void NHermLanczosChain::Evaluate()
{
   Mout << endl;
   Mout << "Evaluating. . ." << endl;
        
   In               nc = mpTrf->NexciXvec()-I_1;                               // Number of response parameters. 
   DataCont         qj(nc, C_0, "InMem", mFilePrefix+"_qj", true);             // q(j).
   //DataCont        qjm1(nc, C_0, "InMem", mFilePrefix+"_qjm1", true);         // q(j-1).
   DataCont         A_qj(nc);                                                // A*q(j).
   DataCont         pj(nc, C_0, "InMem", mFilePrefix+"_wj",true);
   //DataCont        pjm1(nc, C_0, "InMem", mFilePrefix+"_wjm1",true);
   DataCont          pj_A(nc);
   DataCont          r(nc);                                                        // Intermediate vectors;
   DataCont          s(nc);
   Nb                dotprod;        
   mTalpha.SetNewSize(mDef.GetLength());                                        //Setting length of storage vectors
   mTbeta.SetNewSize(mDef.GetLength()+I_1);
   mTgamma.SetNewSize(mDef.GetLength()+I_1);
   Evals.SetNewSize(mDef.GetLength());
   Evectors.SetNewSize(mDef.GetLength(), mDef.GetLength(), false, false);        
   In init_chainidx = 0;   

   if(mpVcc->pVccCalcDef()->RspRestart() && Restart(init_chainidx,qj, pj, r, s))
   {
      Mout << "Restart from " << init_chainidx << " length chain succesful!" << endl;
   }
   else
   { 
      mTbeta[I_0] = C_0;
      mTgamma[I_0] = C_0;
      mXiNorm = CalcEta(mDef.GetOper(), qj);
      Mout << "Made Xi vector" << endl;
      mEtaNorm = mXiNorm;
      if(mpVcc->GetMethod() == VCC_METHOD_VCC)
      {
         mEtaNorm = CreateP0(qj, mDef.GetXopers()[0], pj);
      }
      else
      {
         pj=qj;
      }
      Mout << "Made Eta vector" << endl;
      // Mout << "Initial dot : " << Dot(pj,qj) << " NormProds : " << mXiNorm*mEtaNorm << endl;
      //Set/find q0 and p0
      qj.NewLabel(mFilePrefix+"_qjm1", true, true);
      pj.NewLabel(mFilePrefix+"_pjm1", true, true);
      //Mout << qj.Label() << endl;
      Transfor(qj, A_qj,mpVcc->GetMethod(),I_RIGHT);
      Transfor(pj, pj_A,mpVcc->GetMethod(),I_LEFT);        
      r = A_qj;
      s = pj_A;
      WriteInitialValues();
   }
   for(In j = init_chainidx; j < mDef.GetLength(); j++)
   {
      time_t t0 = time(NULL);
      //Mout << "Begin iteration : " << j << endl;
      mTalpha[j] = Dot(pj,r);
      //Mout << "Alpha " << j << " : " << mTalpha[j] << endl;
      r.Axpy(qj, -mTalpha[j]);
      s.Axpy(pj, -mTalpha[j]);                                
      SaveVec(qj, j, "_q");
      SaveVec(pj, j, "_p");
      //Mout << "|r| = " << r.Norm() << " |s| = " << s.Norm();
      if(r.Norm() <= C_NB_EPSILON || s.Norm() <= C_NB_EPSILON)
      {
         Mout << "Norms of new vectors are too low" << endl;
         Mout << "|r| = " << r.Norm() << " |s| = " << s.Norm();
         break;
      }               
      dotprod = Dot(s,r);
      //Mout << "Dot product in iteration " << j << " : "<< dotprod << endl;
      //Mout << "Dot product " << j << " : " << dotprod << endl;
      if(fabs(dotprod) <= C_NB_EPSILON){ 
         Mout << "Trial vectors orthogonal!" << endl;
         break;
      }
      mTbeta[j+I_1]   =        sqrt(fabs(dotprod));
      mTgamma[j+I_1]  =        dotprod/mTbeta[j+I_1];
      //Mout << "Beta " << j+I_1 << " : " << mTbeta[j+I_1] << endl;
      //Mout << "Gamma " << j+I_1 << " : " << mTgamma[j+I_1] << endl;
      r.Scale(1/mTbeta[j+I_1]);
      s.Scale(1/mTgamma[j+I_1]);
      WriteValuesToFile(j,ios::app);          
      Orthogonalize(r, s, j);
      r.NewLabel("r");
      s.NewLabel("s");                
      Transfor(r, A_qj,mpVcc->GetMethod(),I_RIGHT);
      Transfor(s, pj_A,mpVcc->GetMethod(),I_LEFT);
      A_qj.Axpy(qj, -mTgamma[j+I_1]);
      pj_A.Axpy(pj, -mTbeta[j+I_1]);
      qj = r;
      pj = s;
      r = A_qj;
      s = pj_A;

      if (j == init_chainidx)
         Mout << " Required iterations: " << mDef.GetLength()-init_chainidx << endl
              << " First iteration: " << time(NULL)-t0 << " s"
              << endl << " Progress (one . is 10 iterations):" << endl << " ";
      else if ( (j-init_chainidx)%10 == I_0)
         Mout << "." << std::flush;
   }
   //mTalpha[mDef.GetLength()-I_1] = Dot(pj,r);
   //Mout << "Alpha " << mDef.GetLength()-I_1 << " : " << mTalpha[mDef.GetLength()-I_1] << endl;
   //WriteValuesToFile(mDef.GetLength()-I_1, ios::app);
   //Mout << mTalpha << endl;
   //Mout << mTbeta << endl;
   //Mout << mTgamma << endl;
   //Mout << "Done making the chain" << endl;
   //MakeMatrix(mTalpha, mTbeta, mTgamma);
   //Mout << "Done making the matrix" << endl;
   //Mout << BandMatrix << endl;
   //ExpCalcBandMat(mDef.GetLength());
   //Diagonalize();
   //Mout << "Done with diagonalizing step" << endl;
   //CalcEvecs();
   //CalcRes();
   //Output();
}

bool NHermLanczosChain::Restart(In& aLength, DataCont& aQj, DataCont& aPj, DataCont& aR, DataCont& aS)
{
   ifstream chain_file(mFilePrefix.c_str(), ios_base::in);
   string s;
   aLength = I_0;
   getline(chain_file,s);
   istringstream temp(s);
   temp >> mEtaNorm;
   temp >> mXiNorm;
   temp >> mDotProd;
   while (getline(chain_file,s) && aLength<mDef.GetLength())
   {
      istringstream is(s);
      is >> mTalpha[aLength];
      is >> mTbeta[aLength];
      is >> mTgamma[aLength];
      aLength++;
   }
   mRestartLen = aLength;
   Mout << "aLength after reading alpha, beta and gammas : " << aLength << endl;
   chain_file.close();
   stringstream aNumber;
   aNumber << (aLength-I_1);
   string FileName = mFilePrefix+"_q"+aNumber.str();
   DataCont qj;
   qj.SetNewSize(I_0);
   qj.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, FileName.c_str());
   FileName.clear();
   FileName = mFilePrefix+"_p"+aNumber.str();
   DataCont pj;
   pj.SetNewSize(I_0);
   pj.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, FileName.c_str());
   
   if (! qj.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read q(j) file." << endl;
      aLength = I_0;
      return false;
   }

   if (! pj.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read p(j) file." << endl;
      aLength = I_0;
      return false;
   }
   /*Transfor(qj, aR ,mpVcc->GetMethod(),I_RIGHT);
   Transfor(pj, aS ,mpVcc->GetMethod(),I_LEFT);
   */
   ostringstream aNumberm1;
   aNumberm1 << (aLength-I_2);
   FileName = mFilePrefix+"_q"+aNumberm1.str();
   DataCont qjm1;
   qjm1.SetNewSize(I_0);
   qjm1.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, FileName.c_str());
   FileName.clear();
   FileName = mFilePrefix+"_p"+aNumberm1.str();
   DataCont pjm1;
   pjm1.SetNewSize(I_0);
   pjm1.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, FileName.c_str());

   if (! qjm1.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read q(j-1) file." << endl;
      aLength = I_0;
      return false;
   }

   if (! pjm1.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read p(j-1) file." << endl;
      aLength = I_0;
      return false;
   }
   Transfor(qj, aR ,mpVcc->GetMethod(),I_RIGHT);
   Transfor(pj, aS ,mpVcc->GetMethod(),I_LEFT);
   Mout << "Dot prods : " << Dot(pj,aR) << " " << Dot(qj,aS) << endl;
   aQj = qj;
   aPj = pj;
   //aR.Axpy(qjm1, -mTgamma[aLength-I_1]);
   //aR.Axpy(pjm1, -mTbeta[aLength-I_1]);

   return true;
}

void NHermLanczosChain::WriteInitialValues()
{
   ofstream file_stream;
   file_stream.open(mFilePrefix.c_str(), ios::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << scientific << setprecision(20) << mEtaNorm << "   " << mXiNorm <<  "   " << mDotProd << endl;
   file_stream.close();
}

void NHermLanczosChain::WriteValuesToFile(In aIdx, ios_base::openmode mode)
{
   ofstream file_stream;
   file_stream.open(mFilePrefix.c_str(), mode);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << scientific << setprecision(20) << mTalpha[aIdx] << "   " << mTbeta[aIdx] <<  "   " << mTgamma[aIdx] << endl;
   file_stream.close();
}

void NHermLanczosChain::CalcEvecs()
{
   MidasVector checkortho;
   checkortho.SetNewSize(mDef.GetLength()-I_1);
   checkortho.Zero();
   ostringstream name1;
   name1 << mFilePrefix << "_p" << I_0;
   DataCont p0;
   p0.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, name1.str());
   if (! p0.ChangeStorageTo("InMem",true, false, true))
   {
      Error("Error reading q(j) in orthogonalization step.");
   }
                                 
   for(In i = I_0; i < mDef.GetLength();i++)
   {
      ostringstream name;
      DataCont Evec;
      name << "Right_Evec_" << i;
      Evec.NewLabel(name.str());
      Evec.SetNewSize(mpTrf->NexciXvec()-I_1);
      Evec.Zero();
      for(In j = I_0; j < mDef.GetLength(); j++)
      {
         ostringstream name;
         name << mFilePrefix << "_q" << j;
         DataCont q;
         q.NewLabel("q");
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, name.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
         {
            Error("Error reading q(j) in orthogonalization step.");
         }
         if(i == I_0 && j != I_0)
         {
            checkortho[j-I_1] = Dot(p0,q);
         }
         Evec.Axpy(q, Evectors[j][i]);
      }
      Evec.ChangeStorageTo("OnDisc");
      Evec.SaveUponDecon(true);

   }
   Nb Ave, Avedev, Stddev, Var, Skew, Curt;
   Mout << "Tridiag analysis" << endl;
   checkortho.StatAnalysis(Ave, Avedev, Stddev, Var, Skew, Curt);
   Mout << "Average : " << Ave << endl;
   Mout << "Average dev : "<< Avedev << endl;
   Mout << "Stddev : "<< Stddev << endl;
   Mout << "Var : "<< Var << endl;
   Mout << "Min : " << checkortho.FindMinValue() << endl;
   Mout << "Max : " << checkortho.FindMaxValue() << endl;
}

void NHermLanczosChain::CalcRes()
{
   EvalsRes.SetNewSize(Evals.Size());
   for(In i = I_0; i < Evals.Size(); i++)
   {
      ostringstream name;
      DataCont Evec;
      name << "Right_Evec_" << i;
      Evec.NewLabel("Evec");
      Evec.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1,name.str());
      DataCont TransEvec;
      TransEvec.SetNewSize(mpTrf->NexciXvec()-I_1);
      TransEvec.Zero();
      Transfor(Evec, TransEvec, mpVcc->GetMethod(),I_RIGHT);
      TransEvec.Axpy(Evec, -Evals[i]);
      EvalsRes[i] = TransEvec.Norm();
   }
}

void NHermLanczosChain::ExpCalcBandMat(In aJ)
{
   MidasMatrix BandMatForOut(aJ,aJ, C_0);
   MidasMatrix OrthoCheck(aJ,aJ, C_0);
   for (In j=I_0; j<aJ; j++)
   {
      ostringstream pos;
      DataCont p, transp;
      pos << mFilePrefix << "_p" << j;
      p.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, pos.str());
      if (! p.ChangeStorageTo("InMem",true, false, true))
         Error("Error reading q(j) in orthogonalization step.");
      transp.SetNewSize(p.Size());
      transp.NewLabel("transp");
      Transfor(p, transp, mpVcc->GetMethod(),I_LEFT);
      for (In k=I_0; k<aJ; k++)
      {
         ostringstream qos;
         qos << mFilePrefix << "_q" << k;
         DataCont q;
         q.NewLabel("q");
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, qos.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
            Error("Error reading q(j) in orthogonalization step.");
         BandMatForOut[j][k] = Dot(transp,q);
         OrthoCheck[j][k] = Dot(p,q);
      }
   }
   for(In j=I_0; j<aJ; j++)
   {
      Mout << "Alpha value" << j << ": Lanczos " << mTalpha[j]<< " Explicit " << BandMatForOut[j][j] << " Diff " << fabs(mTalpha[j]-BandMatForOut[j][j]) << endl;
   }
   Mout << endl;
   for(In j=I_1; j<aJ; j++)
   {
      Mout << "Beta value " << j << " : Lanczos " << mTbeta[j]<< " Explicit " << BandMatForOut[j][j-I_1] << " Diff " << fabs(mTbeta[j]-BandMatForOut[j][j-I_1]) << endl;
   }
   Mout << endl;
   for(In j=I_1; j<aJ; j++)
   {
      Mout << "Gamma value " << j << " : Lanczos " << mTgamma[j]<< " Explicit " << BandMatForOut[j-I_1][j] << " Diff " << fabs(fabs(mTgamma[j])-fabs(BandMatForOut[j-I_1][j])) << endl;
   }
   MidasVector NoTriEle(aJ*aJ-aJ*I_3+I_2);
   In h = 0;
   for(In j=I_0; j<aJ; j++)
   {
      for(In k=I_0; k<aJ; k++)
      {
         if(j-k != I_0 && j-k != I_1 && k-j != I_1)
         {
            NoTriEle[h] = BandMatForOut[j][k];
            h = h + I_1;
         }
      }
   }
   MidasVector orthostat(aJ*aJ);
   for(In j=I_0;j<aJ;j++)
   {   
      OrthoCheck[j][j] = OrthoCheck[j][j] - C_1;
   }
   h = 0;
   for(In j=I_0;j<aJ;j++)
   {
      for(In k=I_0;k<aJ;k++)
      {
         orthostat[h] = OrthoCheck[j][k];
         h = h + I_1;
      }
   }
   Nb Ave, Avedev, Stddev, Var, Skew, Curt;
   Mout << "Tridiag analysis" << endl;
   NoTriEle.StatAnalysis(Ave, Avedev, Stddev, Var, Skew, Curt);
   Mout << "Average : " << Ave << endl;
   Mout << "Average dev : "<< Avedev << endl;
   Mout << "Stddev : "<< Stddev << endl;
   Mout << "Var : "<< Var << endl;
   Mout << "Min : " << NoTriEle.FindMinValue() << endl;
   Mout << "Max : " << NoTriEle.FindMaxValue() << endl;
   Mout << "Orthostat analysis : " << endl;
   orthostat.StatAnalysis(Ave, Avedev, Stddev, Var, Skew, Curt);
   Mout << "Average : " << Ave << endl;
   Mout << "Average dev : "<< Avedev << endl;
   Mout << "Stddev : "<< Stddev << endl;
   Mout << "Var : "<< Var << endl;
   Mout << "Min : " << orthostat.FindMinValue() << endl;
   Mout << "Max : " << orthostat.FindMaxValue() << endl;
   
   //Mout << "This is the constructed T matrix, calculated from vectors" << endl;
   //Mout << BandMatForOut << endl;
   //BandMatrix = BandMatForOut;
   //Mout << OrthoCheck << endl;
}

void NHermLanczosChain::SaveVec(DataCont& aQj, In aJ, string Suffix)
{
   DataCont qj(aQj);
   ostringstream os;
   os << mFilePrefix+Suffix << aJ;
   //Mout << os.str() << endl;
   qj.NewLabel(os.str());
   qj.ChangeStorageTo("OnDisc");
   qj.SaveUponDecon(true);
}


Nb NHermLanczosChain::CreateP0(DataCont& q0, const string& aOper, DataCont& aDc)
{
   In iop = I_0;;
   for (iop=I_0; iop<gOperatorDefs.GetNrOfOpers(); iop++) 
      if (gOperatorDefs[iop].Name() == aOper)
         break;
   if (iop == gOperatorDefs.GetNrOfOpers())
      Error("LanczosChain::CalcEta(): Operator '" + aOper + "' not found.");
   Nb exptval = C_0;
   {
      DataCont mults;
      string vecname=mpVcc->pVccCalcDef()->GetName()+"_mul0_vec_0";
      mults.GetFromExistingOnDisc(mpVcc->NrspPar(),vecname);
      if(! mults.ChangeStorageTo("InMem",true, false, true)) // The multiplies haven't been calculated yet
      {
         Mout << "Calculating multiplies!!" << endl;
         mpVcc->CalculateEta0();
         mpVcc->SolveZerothOrderEq();
         mults.ChangeStorageTo("InMem",true, false, true);
      }
      mults.SaveUponDecon(true);
   }
   mpVcc->CalculateVccEtaX(aDc, aOper, iop);
   Nb etanorm = aDc.Norm();
   DataCont tempstore = aDc;
   aDc.Scale(C_1/etanorm);
   //Mout << "Eta norm " << etanorm << " eta*etanormalized(= Etanorm) " << Dot(tempstore,aDc) << " Difference : " << fabs(etanorm)-fabs(Dot(tempstore,aDc)) << endl; 
   Nb norm = Dot(aDc, q0);
   mDotProd = norm;
   aDc.Scale(C_1/sqrt(fabs(norm)));
   q0.Scale(C_1/sqrt(fabs(norm)));
   //Mout << "Eta*Xi = " << norm << endl;
   return etanorm;
}


void NHermLanczosChain::Transfor(DataCont& InVec, DataCont& OutVec, In type, In side)
{
   Nb dummy = C_0;
   if(type == VCC_METHOD_VCI)
   {
      mpTrf->SetType(TRANSFORMER::VCIRSP);
      mpTrf->Transform(InVec, OutVec, I_1, I_0, dummy);
   }
   else if(type == VCC_METHOD_VCC && side == I_RIGHT)
   {
      mpTrf->SetType(TRANSFORMER::VCCJAC);
      mpTrf->Transform(InVec, OutVec, I_1, I_0, dummy);
   }
   else if(type == VCC_METHOD_VCC && side == I_LEFT)
   {
      mpTrf->SetType(TRANSFORMER::LVCCJAC);
      mpTrf->Transform(InVec, OutVec, I_1, I_0, dummy);
   }
   else
   {
      Mout << "Error in calculation, type does not match with supported values" << endl;
   }
}

void NHermLanczosChain::Diagonalize(){
   //Diag(BandMatrix, Evectors, Evals,"NR_JACOBI",false,true)      
   vector<Nb> helper;
   MidasMatrix hey;
   hey.SetNewSize(mDef.GetLength(),mDef.GetLength(),false, false);
   Diag(BandMatrix, Evectors, Evals,"DGEEV",helper,hey,true,true);
}

/*void NHermLanczosChain::Diago(MidasVector& EVals,MidasMatrix& left, MidasMatrix& right) const
{
   //Diag(BandMatrix, Evectors, Evals,"NR_JACOBI",false,true)      
   vector<Nb> helper;
   MidasMatrix hey;
   hey.SetNewSize(mDef.GetLength(),mDef.GetLength(),false, false);
   Diag(BandMatrix, right, EVals, left, "DGEEV",helper, hey,true,true);
}*/

void NHermLanczosChain::MakeMatrix(MidasVector& Alphas, MidasVector& Betas, MidasVector& Gammas){
   BandMatrix.SetNewSize(Alphas.Size(), false, false);
   BandMatrix.Zero();        
   BandMatrix[I_0][I_0] = Alphas[I_0];
   for(In i = I_1; i < Alphas.Size(); i++)
   {
      BandMatrix[i-I_1][i] = Gammas[i];
      BandMatrix[i][i] = Alphas[i];
      BandMatrix[i][i-I_1] = Betas[i];
   }
}

void NHermLanczosChain::Orthogonalize(DataCont& aVec, DataCont& bVec, In aJ)
{
   for (In i_repeat=I_0;i_repeat<I_1;i_repeat++)
   {
      for (In k=I_0; k<aJ; k++)
      {
         ostringstream os;
         os << mFilePrefix << "_q" << k;
         DataCont q;
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
            Error("Error reading q(j) in orthogonalization step.");
         ostringstream pos;
         pos << mFilePrefix << "_p" << k;
         DataCont p;
         p.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, pos.str());
         if (! p.ChangeStorageTo("InMem",true, false, true))
            Error("Error reading p(j) in orthogonalization step.");
         Nb dot = Dot(p,aVec);
         Nb dot2 =Dot(bVec,q);
         aVec.Axpy(q, -dot);
         bVec.Axpy(p, -dot2);
         Nb norm = Dot(aVec,bVec);
         if(fabs(norm) > C_0)
         {
            if(norm > C_0)
            {
               bVec.Scale(C_1/sqrt(norm));
               aVec.Scale(C_1/sqrt(norm));
            }
            else
            {
               bVec.Scale(C_1/sqrt(fabs(norm)));
               aVec.Scale(-C_1/sqrt(fabs(norm)));
            }
         }
      }
   }
}

void NHermLanczosChain::Output()
{
   DataCont eigval_dc;
   In nexci=mDef.GetLength()-I_1;
   eigval_dc.GetFromExistingOnDisc(nexci, mpVcc->Name() + "_rsp_eigval");
   for(In i = I_0; i < Evals.Size(); i++)
   {
      Mout << "Eigenvalue " << i << " from NHermLanczosChain : " << Evals[i] << " Residual : " << EvalsRes[i] << endl;
      //Nb eigval = C_0;
      //eigval_dc.DataIo(IO_GET,i,eigval);
      //Mout << "Difference in " << i << "th eigenvalue : " << Evals[i] << " - " << eigval << " = " << fabs(Evals[i]-eigval) << endl;
   }
   //eigval_dc.GetFromExistingOnDisc(215, mpVcc->Name() + "_rsp_eigval");
   /*if (! eigval_dc.ChangeStorageTo("InMem",true))
      Error("Error reading eigval_dc in eigval step.");
   for(In i = I_0; i < eigval_dc.Size(); i++)
   {
      Nb eigval = C_0;
      eigval_dc.DataIo(IO_GET,i,eigval);
      Mout << "Difference in " << i << "th eigenvalue : " << Evals[i] << " - " << eigval << " = " << fabs(Evals[i]-eigval) << endl;
   }*/
}

Nb NHermLanczosChain::CalcEta(const string& aOper, DataCont& aDc)
{
   In iop = I_0;;
   for (iop=I_0; iop<gOperatorDefs.GetNrOfOpers(); iop++)
      if (gOperatorDefs[iop].Name() == aOper)
         break;
  
   if (iop == gOperatorDefs.GetNrOfOpers())
      Error("LanczosChain::CalcEta(): Operator '" + aOper + "' not found.");
  
   Nb exptval = C_0;                          // Not used but required as argument.
   if (mpVcc->GetMethod() == VCC_METHOD_VCI)
   {
      mpVcc->CalculateVciEta(aDc, aOper, iop, exptval);
   }
   else if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      mpVcc->CalculateXi(aDc, aOper, iop, exptval);
   }
   DataCont tempstore = aDc;
   Nb norm = aDc.Norm();
   aDc.Scale(C_1/norm);
   return norm;
}

void NHermLanczosChain::TransformStoX(const MidasVector& aS, DataCont& aX,bool qTrans) const
{
   aX.SetNewSize(mpTrf->NexciXvec()-I_1);
   aX.Zero();
   aX.ChangeStorageTo("InMem");
   if(qTrans)
   {
      for (In i=I_0; i<aS.Size(); ++i)
      {
         ostringstream os;
         os << mFilePrefix << "_q" << i;
         DataCont q;
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
            Error("LanczosChain::TransformStoX(): Error reading q(j).");
         aX.Axpy(q, aS[i]);
      }
   }
   else
   {
      for (In i=I_0; i<aS.Size(); ++i)
      {
         ostringstream os;
         os << mFilePrefix << "_p" << i;
         DataCont q;
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
            Error("LanczosChain::TransformStoX(): Error reading q(j).");
         aX.Axpy(q, aS[i]);
      }
   }
   aX.Scale(C_1/aX.Norm());
}
