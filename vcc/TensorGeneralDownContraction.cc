#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>

#include "TensorGeneralDownContraction.h"
#include "TensorGeneralDownContraction_Impl.h"

#include "v3/VccEvalData.h"
#include "td/tdvcc/trf/TimTdvccEvalData.h"
#include "td/mctdh/eom/McTdHEvalData.h"


namespace midas::vcc
{

// define 
#define INSTANTIATE_TENSORGENERALDOWNCONTRACTION(T, DATA) \
template class TensorGeneralDownContraction<T, DATA>;
// concrete instantiations
INSTANTIATE_TENSORGENERALDOWNCONTRACTION(Nb, v3::VccEvalData);
INSTANTIATE_TENSORGENERALDOWNCONTRACTION(Nb, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_TENSORGENERALDOWNCONTRACTION(std::complex<Nb>, midas::tdvcc::TimTdvccEvalData);
INSTANTIATE_TENSORGENERALDOWNCONTRACTION(std::complex<Nb>, midas::mctdh::LinearRasTdHEvalData);
#undef INSTANTIATE_TENSORGENERALDOWNCONTRACTION


} /* namespace midas::vcc */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

