/**
************************************************************************
* 
* @file                Vcc.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Vcc class methods.
* 
* Last modified: Wed Apr 07, 2010  11:02AM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>
#include<algorithm>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "vscf/Vscf.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "operator/OperProd.h"
#include "input/BasDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "util/Io.h"
#include "util/MultiIndex.h"
#include "util/Plot.h"
#include "util/MidasStream.h"
#include "mpi/FileStream.h"
#include "vcc/Vcc.h"
#include "vcc/Xvec.h"
#include "vcc/ExpHvci.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "vcc/VccTransformer.h"
#include "vcc/TransformerV3.h"
#include "vcc/TensorDataCont.h"
#include "vcc/VccStateSpace.h"
#include "util/conversions/VectorFromString.h"
#include "util/MidasSystemCaller.h"
#include "util/read_write_binary.h"
#include "vcc/v3/IntermediateMachine.h"
#include "it_solver/nl_solver/TensorNlSolver.h"
#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory.h"
#include "vcc/subspacesolver/VibCorrSubspaceSolver.h"
#include "vcc/subspacesolver/ExtVccSolver.h"
#include "td/tdvcc/DriverUtils.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "mpi/Impi.h"

// using declarations
using std::stringstream;
using std::string;
using std::vector;
using std::find;

/**
* Default "zero" Constructor 
* */
Vcc::Vcc
   (  VccCalcDef* apVccCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  OneModeInt* apOneModeInt
   )
   : 
   Vscf
      (  apVccCalcDef
      ,  apOpDef
      ,  apBasDef
      ,  apOneModeInt
      )
   , mRspTrf(nullptr)
{
   mpVccCalcDef  = apVccCalcDef;
   mEvmp0 = C_0;
   
   mCorrMethod = VCC_METHOD_UNDEFINED;
}

void Vcc::UpdateEvmp0()
{
   mEvmp0 = C_0;
   for (In i_op_mode=I_0;i_op_mode<pOpDef()->NmodesInOp();i_op_mode++)
      mEvmp0 += GetOccEigVal(i_op_mode);
}

/**
* From the CalcDef generate Target Vector info
* */
void Vcc::AutoTarget()
{
   bool reference  = mpVccCalcDef->Reference();
   bool allfund    = mpVccCalcDef->AllFundamentals();
   bool allfirsto  = mpVccCalcDef->AllFirstOvertones();
   bool fund       = mpVccCalcDef->Fundamentals();
   bool firsto     = mpVccCalcDef->FirstOvertones();
   In n_sel_fund   = mpVccCalcDef->Nfundamentals();
   In n_sel_firsto = mpVccCalcDef->NfirstOvertones();
   if (allfirsto && firsto) 
   {
      MIDASERROR("Confusion, both all first-overtones and only selected");
   }
   if (allfund&& fund)
   {
      MIDASERROR("Confusion, both all fundamentals and only selected");
   }
   
   string name = pVscfCalcDef()->GetmVscfAnalysisDir() + "/TargetVectors";
   midas::mpi::OFileStream target_ascii(name, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);

   In n_modes = pOpDef()->NmodesInOp();
   In n_targs = I_0; 
   if (reference) n_targs += I_1;
   if (allfund)   n_targs += n_modes;
   if (allfirsto) n_targs += n_modes;
   if (fund)      n_targs += n_sel_fund;  
   if (firsto)    n_targs += n_sel_firsto;
   target_ascii << n_targs << std::endl;
   In i_state = I_0;
   if (reference) 
   {
      Mout << " AutoTargetting Reference state " << endl;
      target_ascii << I_0 << " " << I_1 << endl;
      midas::stream::ScopedPrecision(25, target_ascii);
      target_ascii.setf(ios::scientific);
      target_ascii << C_1 << endl;
      for (In i=I_0;i<n_modes;i++) target_ascii << I_0 << " "; 
      target_ascii << endl;
      i_state++;
   }
   if (allfund || allfirsto) 
   {
      vector<In> modes_occ(n_modes);
      for (In j=I_1;j<=I_2;j++)
      {
         if (j==1) Mout << " AutoTargetting all fundamentals " << endl;
         if (j==2) Mout << " AutoTargetting all first overtones " << endl;
         if (j==I_1 && !allfund)   continue;
         if (j==I_2 && !allfirsto) continue;
         for (In i_st=I_0;i_st<n_modes;i_st++)
         {
            target_ascii << i_state++ << " " << I_1 << endl;
            midas::stream::ScopedPrecision(25, target_ascii);
            target_ascii.setf(ios::scientific);
            target_ascii << C_1 << endl;
            for (In i=I_0;i<n_modes;i++) modes_occ[i]=I_0;
            modes_occ[i_st] = j;
            for (In i=I_0;i<n_modes;i++) target_ascii << modes_occ[i] << " "; 
            target_ascii << endl;
         }
      }
   }
   else if (fund || firsto) 
   {
      vector<In> modes_occ(n_modes);
      In i_current=I_0;
      for (In j=I_1;j<=I_2;j++)
      {
         In n_now = I_0;
         if (j==1) Mout << " AutoTargetting fundamentals " << endl;
         if (j==2) Mout << " AutoTargetting first overtones " << endl;
         if (j==I_1 && !fund)   continue;
         if (j==I_2 && !firsto) continue;
         if (j==I_1) n_now=n_sel_fund;
         if (j==I_2) n_now=n_sel_firsto;
         for (In i_st=I_0;i_st<n_now;i_st++)
         {
            target_ascii << i_state++ << " " << I_1 << endl;
            midas::stream::ScopedPrecision(25, target_ascii);
            target_ascii.setf(ios::scientific);
            target_ascii << C_1 << endl;
            for (In i=I_0;i<n_modes;i++) modes_occ[i]=I_0;
            if (j==I_1) i_current=mpVccCalcDef->Fundamental(i_st);
            if (j==I_2) i_current=mpVccCalcDef->FirstOvertone(i_st);
            modes_occ[i_current] = j;
            for (In i=I_0;i<n_modes;i++) target_ascii << modes_occ[i] << " "; 
            target_ascii << endl;
         }
      }
   }
   if (i_state != n_targs) MIDASERROR("Something went wrong in the auto-targetting");

   mpVccCalcDef->SetTargetSpace(true); // Use target space from now on. 
}
/**
* Prepare Target Space Vectors from file with format as TargetVectors file
* described in manual.
* Return the number of target space vectors actually generated.
* aTargetsFile:    Filename of file describing targets.
* aTargetVectors: Filename of Data Container files containing generated
*                 target vectors (suffix will be appended).
* aResponse:      Are we targeting for a response calculation?
* */
In Vcc::PrepareTargetStates
   (  const string& aTargetsFile
   ,  const string& aTargetVectors
   ,  const Xvec& arXvec
   ,  const bool aResponse
   )
{
   Mout << "\n\n Prepare target space." << endl;
   if (aResponse)
   {
      Mout << " Response flag is on. No reference component in generated vectors." << endl;
   }
   In nvecsize = arXvec.Size();
   if (aResponse)                         // For response we have no coefficient for reference.
      nvecsize--;
   
   // Check for target vectors already generated.
   In idx = 0;
   while(true)
   {
      std::string filename     = aTargetVectors + "_Rsp_" + std::to_string(idx);
      std::string filename_new = aTargetVectors + "_"     + std::to_string(idx);
      if(!InquireDataContFile(filename) )
      {
         break;
      }
      
      DataCont target_vec;
      target_vec.GetFromExistingOnDisc(nvecsize,filename);
      target_vec.NewLabel(filename_new);
      target_vec.Normalize();
      target_vec.SaveUponDecon(true);

      ++idx;
   }
   if(idx)
   {
      Mout << " Have successfully found " << idx << " RspTargetVectors. \n"
           << " Will not construct any new ones " << std::endl;
      return idx;
   }

   // If none were found generate some new
   auto target_ascii = midas::mpi::FileToStringStream(aTargetsFile);

   string targ_components = aTargetsFile + "_CompAdd";

   midas::mpi::OFileStream tv_components(targ_components, midas::mpi::OFileStream::StreamType::MPI_MASTER);
   
   In n_ts = I_0;
   target_ascii >> n_ts;
   Mout << " There should be " << n_ts << " target states." << endl;
   vector<In> states_corresponds(n_ts);

   In n_modes = pOpDef()->NmodesInOp();
   In i_st;                               // State number.
   In n_contribs;
   Nb coef;
   In n_neglected = I_0;
   for (In i_ts=I_0; i_ts<n_ts; i_ts++)   // Loop over target states, i_ts = current target.
   {
      target_ascii >> i_st >> n_contribs;
      if (i_st != i_ts)                   // Current target should match number in file.
      {
         Mout << " Target state nr. must begin with state zero "
              << "and increase by one for each state." << endl;
         MIDASERROR("Error in Target State numbering.");
      }
      In i_st_in = i_st;                  // Save nr. of state given in file.
      i_st -= n_neglected;                // Subtract the number of neglected states       
      if (mpVccCalcDef->IoLevel() > I_11)
      {
         Mout << " state nr = " << i_st << " with " << n_contribs << " contributions." << std::endl;
      }
      string s2 = aTargetVectors + "_" + std::to_string(i_st);
      string storage_mode = "OnDisc";
      DataCont target_vec(nvecsize, C_0, storage_mode, s2, false);

      In n_in = I_0;                             // No. of contribs. actually put in target vector.
      for (In i_c=I_0; i_c<n_contribs; i_c++)    // For each contribution...
      {
         target_ascii >> coef;                   // ...read in coefficient...
         vector<In> ivec(n_modes);
         for (In i_c=I_0; i_c<n_modes; i_c++)    // ...and occupation vector.
            target_ascii >> ivec[i_c];
         In i_add = arXvec.AddressForOccVec(ivec);
         if (aResponse)                          // If we are targeting in a response calculation
            i_add--;                             // we have no reference.
         if (mpVccCalcDef->IoLevel() > I_11) 
         {
             Mout << " coef:  " << coef << std::endl;
             Mout << " ivec:  " << ivec << std::endl;
             Mout << " i_add: " << i_add << std::endl;
         }
         if (i_add >= I_0)
         {
            target_vec.DataIo(IO_PUT, i_add, coef);
            n_in++;
            tv_components << i_add << endl;
         }
      }
   
      if (n_in == n_contribs)                    // Was all contributions put in vector?
      {
         target_vec.Normalize();
         target_vec.SaveUponDecon();
         if (gDebug || mpVccCalcDef->IoLevel() > I_14)
         {
            Mout << " target vec nr. " << i_st << ": " << target_vec << std::endl;
         }
         states_corresponds[i_ts] = i_st;
      }
      else
      {
         Mout << " Some component(s) of target vector state " << i_st_in
              << " not in exci space. Will be ignored." << endl;
         n_neglected++;
         states_corresponds[i_ts] = -I_1; 
      }
   } // end loop over n target states
   Mout << " Target Vectors have been produced." << endl;
   
   if (n_neglected > I_0)
   {
      Mout << " Some of the target vectors were outside of the excitation space -" << endl
           << " These states were ignored. " << endl
           << " Note that the numbering of the set of states have been altered during "
           << "the calculation." << endl;
      MidasWarning("Numbering of target vector altered since some were outside "
                          "the excitation space.");  
      MidasWarning("Check the " + aTargetsFile + "_Compared file.");
   }
   
   string targ_compar = aTargetsFile + "_Compared";
   
   midas::mpi::OFileStream rel(targ_compar);

   for (In i_ts = I_0;i_ts<n_ts;i_ts++)
   {
      rel << " " << i_ts;
      rel << " " << states_corresponds[i_ts];
      if (states_corresponds[i_ts]<I_0) rel << " Outside space - not targetted";
      rel << endl;
   }
   //tv_components_stream.close();
   return n_ts - n_neglected;
}

/**
* Do ExpH vci calculation
* */
void Vcc::DoExpHvci()
{
   string diag_method;
   if (mpVccCalcDef->DiagMeth()=="LAPACK" ) 
      diag_method="DSYEVD";
   else if (mpVccCalcDef->DiagMeth()=="MIDAS")
      diag_method="MIDAS_JACOBI";
   else if (mpVccCalcDef->DiagMeth()=="DSYEVD" || 
         mpVccCalcDef->DiagMeth()=="MIDAS_JACOBI") 
      diag_method=mpVccCalcDef->DiagMeth();
   else
   {
      MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override! ");
      diag_method="DSYEVD";
   }
   ExpHvci vci(mpVccCalcDef, pOpDef(), pBasDef(), pOneModeInt(),this,
               diag_method,mpVccCalcDef->ExpHvciExciLevel());
   vci.ConstructHexp();
   vci.DiagonalizeHexp();
}

/**
 * Do Vcc calculation
 **/
void Vcc::DoVcc
   (
   )
{
   if (gDebug)
      Mout << " In Vcc::DoVcc()" << endl;

   // Transformer setup.
   std::unique_ptr<VccTransformer> trf = nullptr;
   if (mpVccCalcDef->V3trans())
   {
      trf = std::unique_ptr<VccTransformer>( new midas::vcc::TransformerV3(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0) );
   }
   else
   {
      trf = std::unique_ptr<VccTransformer>( new VccTransformer(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0) );
   }
   trf->SetType(TRANSFORMER::VCC);

   Mout << " Dimensionality of result vector (mTransXvec): " << trf->NexciTransXvec() << endl
        << " Dimensionality of input vector (mXvec):       " << trf->NexciXvec() << endl;
   
   if(mpVccCalcDef->UseTensorNlSolver())
   {   
      if(!(mpVccCalcDef->V3trans()))
      {
         MIDASERROR("TensorNlSolver only works with V3 transformer");
      }
      // Solve the equations with TensorNlSolver
      this->DoTensorVcc(trf.get());
   }
   else
   {
      // Use "old" solver
      this->DoStandardVcc(trf.get());
   }
}

/**
 * Do VCC calculation using TensorNlSolver.
 * @param apTrf            Transformer to use.
 **/
void Vcc::DoTensorVcc
   (  VccTransformer* apTrf
   )
{
   // Initialize tensor non-linear solver
   TensorNlSolver nlsolver;
   nlsolver.SetTransformer(dynamic_cast<midas::vcc::TransformerV3*>(apTrf));
   nlsolver.SetDecompInfoSet(mpVccCalcDef->GetDecompInfoSet());
   nlsolver.SetResidualThreshold(mpVccCalcDef->GetItEqResidThr());
   nlsolver.SetMaxNormConvCheck(mpVccCalcDef->MaxNormConvCheck());
   nlsolver.SetEnergyThreshold(mpVccCalcDef->GetItEqEnerThr());
   nlsolver.SetMaxIter(mpVccCalcDef->GetItEqMaxIter());
   nlsolver.SetAdaptiveDecompThreshold(mpVccCalcDef->AdaptiveDecompThreshold() >= C_0);
   nlsolver.SetAdaptiveTrfDecompThreshold(mpVccCalcDef->AdaptiveTrfDecompThreshold() >= C_0);
   nlsolver.SetFirstDecompThreshold(mpVccCalcDef->FirstDecompThreshold());
   nlsolver.SetThresholdScaling(mpVccCalcDef->AdaptiveDecompThreshold());
   nlsolver.SetTrfRelThreshScal(mpVccCalcDef->AdaptiveTrfDecompThreshold());
   nlsolver.SetAdaptiveDecompGuess(mpVccCalcDef->AdaptiveDecompGuess());
   nlsolver.SetLaplaceInfo(mpVccCalcDef->GetLaplaceInfo());
   nlsolver.SetCheckLaplaceFit(mpVccCalcDef->CheckLaplaceFit());
   nlsolver.SetSaveRankInfo(mpVccCalcDef->GetSaveRankInfo());
   nlsolver.SetFullRankAnalysis(mpVccCalcDef->FullRankAnalysis());
   nlsolver.SetIdja(mpVccCalcDef->GetIdja() > C_0);
   nlsolver.SetIdjaThresh(mpVccCalcDef->GetIdja());
   nlsolver.SetIdjaUnitMatrixGuess(mpVccCalcDef->IdjaUnitMatrixGuess());
   nlsolver.SetTrueADiag(mpVccCalcDef->GetTrueHDiag());
   nlsolver.SetCheckIdja(mpVccCalcDef->CheckIdja());
   nlsolver.SetAllowBackstep(mpVccCalcDef->AllowBackstep());
   nlsolver.SetBacktrackingThresh(mpVccCalcDef->BacktrackingThresh());
   nlsolver.SetMaxBacktracks(mpVccCalcDef->GetMaxBacktracks());
   nlsolver.SetMaxBacksteps(mpVccCalcDef->GetMaxBacksteps());
   nlsolver.SetNewtonRaphson(mpVccCalcDef->NewtonRaphson());
   nlsolver.SetNewtonRaphsonInfo(mpVccCalcDef->GetNrInfo());
   nlsolver.SetCheckNewtonRaphson(mpVccCalcDef->CheckNewtonRaphson());
   nlsolver.SetCrop(mpVccCalcDef->GetCrop());
   nlsolver.SetDiis(mpVccCalcDef->Diis());
   nlsolver.SetImprovedPreconInSubspaceMatrix(mpVccCalcDef->ImprovedPreconInSubspaceMatrix());
   nlsolver.SetNSubspaceVecs(mpVccCalcDef->GetNSubspaceVecs());
   nlsolver.SetPreconSavedResiduals(mpVccCalcDef->PreconSavedResiduals());
   nlsolver.SetIndividualMcDecompThresh(mpVccCalcDef->IndividualMcDecompThresh());
   nlsolver.SetIndividualMcDecompThreshScaling(mpVccCalcDef->IndividualMcDecompThreshScaling());
   nlsolver.SetScaleThresholdWithTensorOrder(mpVccCalcDef->TensorOrderThresholdScaling() > C_0);
   nlsolver.SetTensorOrderThresholdScaling(mpVccCalcDef->TensorOrderThresholdScaling());
   nlsolver.SetUsePrecon(mpVccCalcDef->UsePrecon());
   nlsolver.SetBreakBeforeTransform(mpVccCalcDef->BreakBeforeTransform());

   // Initialize trial vectors
   std::vector<In> nmodals;
   mpVccCalcDef->Nmodals(nmodals);
   VccStateSpace state_space(apTrf->GetXvecModeCombiOpRange(), nmodals, true); // Exclude ref

   auto tensor_type  =  this->mpVccCalcDef->GetDecompInfoSet().empty()
                     ?  BaseTensor<Nb>::typeID::SIMPLE
                     :  BaseTensor<Nb>::typeID::CANONICAL;

   TensorDataCont cctrials(state_space, tensor_type);

   // Read in restart vector
   bool restart = this->mpVccCalcDef->Restart();
   bool restart_success = false;
   if (  restart  )
   {
      if (  this->RestartFromPrev(cctrials, nlsolver, "TENSORVCC")
         )
      {
         Mout << " VCC restart vector read in succesfully."  << std::endl;
         Mout << " Norm of restart vector: " << cctrials.Norm()
              << ", size: " << cctrials.Size() << std::endl;
         restart_success=true;
      }
      else
      {
         Mout << " Restart failed." << std::endl; 
         Mout << " Solving VCC equations with default guess." << std::endl;
      }
   }
   nlsolver.SetRestart(restart && restart_success);

   // Write restart info file
   In nvecsize = apTrf->NexciTransXvec() - I_1; // The Xvec contains the reference as well.
   WriteRestartInfo(nvecsize, pVscfCalcDef()->GetmVscfAnalysisDir() + "/TENSORVCC", "_Vcc_vec_");

   // Solve the equations
   Timer tensor_solver_time;
   bool converged = nlsolver.Solve(cctrials);
   if (  converged
      )
   {
      Mout << std::endl;
      Mout << " Non-linear VCC equations converged in " << nlsolver.GetNIter() << " iterations!" << std::endl;
      Mout << std::endl;
   }
   else
   {
      Mout << " Vcc not converged to requested threshold." << endl;
      MidasWarning(" Vcc not converged, Vcc name = " + mpVccCalcDef->GetName());
   }
   Mout << std::endl;
   if (  gTime )
   {
      tensor_solver_time.CpuOut(Mout,  "CPU time spent in TensorNlSolver:  ");
      tensor_solver_time.WallOut(Mout, "Wall time spent in TensorNlSolver: ");
   }

   // Only do solution-vector analysis and conversion to DataCont if we have no overflow in nvecsize
   if (  !cctrials.CheckOverflow()   )
   {
      // Convert to DataCont
      auto ccvec = DataContFromTensorDataCont(cctrials);

      // Solution vector analysis
      this->VccSolutionVectorAnalysis(ccvec, apTrf, nvecsize);

      // Save vcc amplitudes to disc, e.g. for use in response calculations
      const std::string str = mpVccCalcDef->GetName() + "_Vcc_vec_"+std::to_string(0);
      ccvec.NewLabel(str,false);
      ccvec.SaveUponDecon();

      StoreGsCalcResults(converged, nlsolver.GetEnergy(), ccvec, apTrf->GetXvecModeCombiOpRange());
   }
   else
   {
      auto warning   =  std::string("Skipped VccSolutionVectorAnalysis due to overflow of In in TensorDataCont::TotalSize()! ")
                     +  std::string("Compile Midas with In = long to perform solution-vector analysis for this calculation.");
      MidasWarning(warning);
   }

   // Output transformer post-information
   Mout << dynamic_cast<midas::vcc::TransformerV3*>(apTrf) << std::endl;

   // Set energy
   this->mpVccCalcDef->SetDone(nlsolver.GetEnergy());
}


/**
 * Do Standard VCC calculation using the "old" solver.
 * @param apTrf              Transformer to use.
 **/
void Vcc::DoStandardVcc
   (  VccTransformer* apTrf
   )
{
   // Initialize common part of equation solver
   ItEqSol eqsol(mpVccCalcDef->VecStorage(), pVscfCalcDef()->GetmVscfAnalysisDir());
   eqsol.SetTransformer(apTrf);
   eqsol.SetResidThreshold(mpVccCalcDef->GetItEqResidThr());
   eqsol.SetEnerThreshold(mpVccCalcDef->GetItEqEnerThr());
   eqsol.SetTrustDown(mpVccCalcDef->TrustScaleDown());
   eqsol.SetTrustUp(mpVccCalcDef->TrustScaleUp());
   eqsol.SetMinTrustStep(mpVccCalcDef->MinTrustScale());
   eqsol.SetNiterMax(mpVccCalcDef->GetItEqMaxIter());
   eqsol.SetRedDimMax(mpVccCalcDef->GetRedDimMax());
   eqsol.SetBreakDim(mpVccCalcDef->GetRedBreakDim());
   eqsol.SetIoLevel(mpVccCalcDef->IoLevel());
   eqsol.SetDiis(mpVccCalcDef->Diis());
   eqsol.SetMaxDiis(mpVccCalcDef->GetNSubspaceVecs());
   eqsol.SetPreDiag(mpVccCalcDef->PreDiag());
   eqsol.SetImprovedPrecond(mpVccCalcDef->GetImprovedPrecond());
   eqsol.SetPrecondExciLevel(mpVccCalcDef->GetPrecondExciLevel());
   eqsol.SetTrueHDiag(mpVccCalcDef->GetTrueHDiag());
   eqsol.SetSecOrd(mpVccCalcDef->NewtonRaphson());
   eqsol.SetNpreDiag(mpVccCalcDef->NpreDiag());
   eqsol.SetTimeIt(mpVccCalcDef->TimeIt());
   eqsol.SetMicroResFac(mpVccCalcDef->GetItEqMicroThrFac()); 
   eqsol.SetMicroMax(mpVccCalcDef->GetItEqMaxIterMicro()); 
   In nroots = I_1; 
   eqsol.SetNeq(nroots);
   string diag_method;
   if (mpVccCalcDef->DiagMeth()=="LAPACK" ) 
      diag_method="DSYSV";
   else if (mpVccCalcDef->DiagMeth()=="MIDAS")
      diag_method="MIDAS_CG";
   else if (mpVccCalcDef->DiagMeth()=="DSYSV" || 
         mpVccCalcDef->DiagMeth()=="MIDAS_CG" || 
         mpVccCalcDef->DiagMeth()=="SVD") 
      diag_method=mpVccCalcDef->DiagMeth();
   else
   {
      MidasWarning("Only DSYSV (LAPACK), MIDAS_CG (MIDAS), or SVD are allowed DiagMeth. Override! ");
      diag_method="DSYSV";
   }
   eqsol.SetDiagMeth(diag_method);
   if (mpVccCalcDef->GetLevel2Solver()=="MIDAS" ||  
         mpVccCalcDef->GetLevel2Solver()=="LAPACK") 
      eqsol.SetLevel2Solver(diag_method);
   else if (mpVccCalcDef->GetLevel2Solver()=="DSYSV" || 
         mpVccCalcDef->GetLevel2Solver()=="MIDAS_CG" || 
         mpVccCalcDef->GetLevel2Solver()=="SVD") 
      eqsol.SetLevel2Solver(mpVccCalcDef->GetLevel2Solver());
   else
   {
      MidasWarning("Only DSYSV (LAPACK), MIDAS_CG (MIDAS), or SVD are allowed Level2Solver. Override! ");
      eqsol.SetLevel2Solver("DSYSV");
   }
   In nroots2 = nroots; // ==I_1
   eqsol.SetNeq(nroots2);
   // The solution vectors which are trial vector for the real calc.
   vector<DataCont> stvecs;
   stvecs.reserve(mpVccCalcDef->GetRedDimMax());

   // Find VciH2 start vector
   bool start_cih2eig = mpVccCalcDef->H2Start(); //  && !mpVccCalcDef->Restart();
   if (start_cih2eig)
   {
      Mout << " Make start of Vcc from VciH2 data possible " << endl;
      In nvecsize = apTrf->NexciTransXvec();
      DataCont tmp_datacont;
      for (In i_eq=I_0;i_eq<nroots2;i_eq++) // nroots2==I_1
      {
         stvecs.push_back(tmp_datacont);
         string s2 = mpVccCalcDef->GetName()+"_VciH2_"+std::to_string(i_eq);
         stvecs[i_eq].NewLabel(s2,false); // container assumed empty at this stage
         string storage_mode = "OnDisc";
         stvecs[i_eq].ChangeStorageTo(storage_mode); //Lets keep it on disc only
         stvecs[i_eq].SetNewSize(nvecsize);
         Mout << " Norm of read start vector: " << stvecs[i_eq].Norm() << endl;

         if (!stvecs[i_eq].IntermediateNormalize(I_0)) 
         {
            Mout << " No intermediate normalization possible in this case." << endl;
            start_cih2eig = false;
         }
         else
         {
            DataCont tmp(stvecs[i_eq]);
            // Change original to correct name but store orig. vmp label.
            string lbl = stvecs[i_eq].Label();
            s2 = mpVccCalcDef->GetName()+"_VccStartVec_"+std::to_string(i_eq);
            stvecs[i_eq].NewLabel(s2);
            tmp.NewLabel(lbl);       // Set copy to have the orig. vmp label.
            tmp.SaveUponDecon(true); // Ensure that file is kept after this scope.
            // Do not keep stvecs file, as it is not useful after use.
            stvecs[i_eq].SaveUponDecon(false); 

            DataCont vcc_rep(stvecs[i_eq]);
            apTrf->VccFromVci(vcc_rep,stvecs[i_eq]);
            Mout << " Norm of start vector in vcc rep: " << stvecs[i_eq].Norm() << endl;
         }
      }
   }
   // Find vmp start vector, no reason to check if cih2 is used as start.
   bool start_vmp = mpVccCalcDef->VmpStart() && mpVccCalcDef->Vmp(); // && !mpVccCalcDef->Restart();

   // If we have choice choow cih2 as start (so h2 can be started from vmp)
   if (start_cih2eig) start_vmp = false; 
   if (start_vmp && mpVccCalcDef->Vmp()) 
   {
      Mout << " Make start of Vcc from Vmp data possible." << endl;
      In nvecsize = apTrf->NexciTransXvec(); 
      DataCont tmp_datacont;
      for (In i_eq=I_0;i_eq<nroots2;i_eq++) // nroots2==I_1
      {
         stvecs.push_back(tmp_datacont);
         string s2 = mpVccCalcDef->GetName()+"_Vmp_sum_wf";
         stvecs[i_eq].NewLabel(s2,false);
         string storage_mode = "OnDisc";
         stvecs[i_eq].ChangeStorageTo(storage_mode); //Lets keep it on disc only
         stvecs[i_eq].SetNewSize(nvecsize);
         Mout << " Norm of read start vector: " << stvecs[i_eq].Norm() << endl;
         DataCont vcc_rep(stvecs[i_eq]);
         // Now stvecs is no longer vmp, but vmp in cc rep! thus destroyed for other use!
         apTrf->VccFromVci(vcc_rep,stvecs[i_eq]); 
         // Do not keep stvecs file, as it is not useful except for this concrete use.
         stvecs[i_eq].SaveUponDecon(false); 
         Mout << " Norm of start vector in VCC rep: " << stvecs[i_eq].Norm() << endl;
      }
   }

   In nvecsize = apTrf->NexciTransXvec() - I_1; // The Xvec contains the reference as well.
   
   eqsol.SetNonLinEq();
   eqsol.SetNeq(nroots);
   eqsol.SetDim(nvecsize);
   eqsol.SetResidThreshold(mpVccCalcDef->GetItEqResidThr());

   vector<DataCont> ccvecs;                    // The solution vectors on return.
   ccvecs.reserve(mpVccCalcDef->GetRedDimMax());

   string storage_mode = "OnDisc";
   if (mpVccCalcDef->VecStorage()==I_0) storage_mode = "InMem";

   bool restart = true;
   bool include_ref=false; // In restart, skip the ref conf when mapping
   for (In i_eq=I_0;i_eq<nroots;i_eq++) //nroots==I_1
   {
      string s2 = mpVccCalcDef->GetName() + "_Vcc_vec_"+std::to_string(i_eq);
      DataCont tmp_datacont;
      ccvecs.push_back(tmp_datacont);
      ccvecs[i_eq].NewLabel(s2,false);
      bool res_suc = false;
      if (mpVccCalcDef->Restart()) 
      {
         if (RestartFromPrev(apTrf->GetTransXvec(),ccvecs[i_eq],i_eq,nvecsize,storage_mode,"VCC",include_ref))
         {
            Mout << " Restart vector read in succesfully for root " << i_eq << "." << endl;
            Mout   << " Norm of restart vector: " << ccvecs[i_eq].Norm()
               << ", size: " << ccvecs[i_eq].Size() << endl;
            res_suc=true;
         } 
         else          
         {
            Mout << " Restart failed." << endl; 
            Mout << " Restart flag turned off and start vector guess is default/vmp/vcih2." 
               << endl; 
            ccvecs[i_eq].ChangeStorageTo("OnDisc");
            ccvecs[i_eq].SetNewSize(nvecsize);
         }
      }
      else
      {
         ccvecs[i_eq].ChangeStorageTo(storage_mode);
         ccvecs[i_eq].SetNewSize(nvecsize);
      }

      if (!res_suc)
      {
         if (start_cih2eig || start_vmp)
         {
            ccvecs[i_eq].Reassign(stvecs[i_eq],I_1,I_1);
            if (start_vmp) Mout << " Start from Vmp vector ." << endl;
            if (start_cih2eig) Mout << " Start from VCI H2 eigenvector." << endl;
            Mout << " Norm of restart vector: " << ccvecs[i_eq].Norm() << endl;
            res_suc = true;
         }
         else
         {
            Mout << " Set vector #" << i_eq << " to zero vector first, later standard guess." << endl; 
            ccvecs[i_eq].Zero();
         }
      } 
      restart = restart && res_suc;

      ccvecs[i_eq].SaveUponDecon();
      if (mpVccCalcDef->SizeIt()) 
      {
         Mout << " Vcc::DoStandardVcc() ccvecs - Total size: " << ccvecs[i_eq].SizeOut()<<endl;
      }
   }

   WriteRestartInfo(nvecsize, pVscfCalcDef()->GetmVscfAnalysisDir() + "/VCC", "_Vcc_vec_");

   //stvecs; // Start vectors has been moved into ccvecs if necessary.

   MidasVector eig_vals;


   eqsol.SetRestart(restart);
   bool converged = eqsol.Solve(eig_vals,ccvecs);
   if (converged)
   {
      Mout << " Vcc converged to requested threshold." << endl;
   }
   else
   {
      Mout << " Vcc not converged to requested threshold." << endl;
      string s1 = " Vcc not converged, Vcc name = " + mpVccCalcDef->GetName();
      MidasWarning(s1);
   }

   StoreGsCalcResults(converged, eig_vals[I_0], ccvecs[I_0], apTrf->GetXvecModeCombiOpRange());

   // Analysis of solution
   if (mpVccCalcDef->IoLevel() > I_0)
   {
      Mout << endl
           << " Final Vcc energy          " << setw(35) << eig_vals[I_0] << " a.u. " << endl
           << " Final Vcc energy          " << setw(35) << eig_vals[I_0]*C_AUTEV << " eV" << endl
           << " Final Vcc energy          " << setw(35) << eig_vals[I_0]*C_AUTKAYS << " cm-1"
           << endl
           << " Norm of Vcc vector        " << setw(35) << ccvecs[I_0].Norm() << endl
           << " Vcc Correlation energy    " << setw(35) << eig_vals[I_0]-GetEtot()
           << " MaxExci = " << mpVccCalcDef->MaxExciLevel()
           << " IsOrder = " << mpVccCalcDef->IsOrder() << endl
           << " Vcc corr. energy pr. mode " << setw(35)
           << (eig_vals[I_0]-GetEtot())/pOpDef()->NmodesInOp()
           << " MaxExci = " << mpVccCalcDef->MaxExciLevel()
           << " IsOrder = " << mpVccCalcDef->IsOrder() << endl;
      if (gDebug)
         Mout << " Vcc vector: " << ccvecs[I_0] << endl; 

      this->VccSolutionVectorAnalysis(ccvecs[I_0], apTrf, nvecsize);
      
      mpVccCalcDef->SetDone(eig_vals[I_0]);
   }
   
   // output transformer statistics
   Mout << "\n" << apTrf << std::endl;
}



/**
* Do VciH2 calculation
* */
void Vcc::DoVciH2()
{
   Mout << " In Vcc::DoVciH2()..." << endl;

   // Excitation space set up
   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   trf.SetType(TRANSFORMER::VCIH2);
   In nvecsize = trf.NexciTransXvec();
     
   // Initialize common part of equation solver
   ItEqSol eqsol(mpVccCalcDef->VecStorage(), pVscfCalcDef()->GetmVscfAnalysisDir());
   eqsol.SetTransformer(&trf);
   eqsol.SetImprovedPrecond(mpVccCalcDef->GetImprovedPrecond());
   eqsol.SetPrecondExciLevel(mpVccCalcDef->GetPrecondExciLevel());
   eqsol.SetResidThreshold(mpVccCalcDef->GetItEqResidThr());
   eqsol.SetEnerThreshold(mpVccCalcDef->GetItEqEnerThr());
   eqsol.SetNiterMax(mpVccCalcDef->GetItEqMaxIter());
   eqsol.SetRedDimMax(mpVccCalcDef->GetRedDimMax());
   eqsol.SetBreakDim(mpVccCalcDef->GetRedBreakDim());
   eqsol.SetIoLevel(mpVccCalcDef->IoLevel());
   eqsol.SetDiis(mpVccCalcDef->Diis());
   eqsol.SetMaxDiis(mpVccCalcDef->GetNSubspaceVecs());
   eqsol.SetTimeIt(mpVccCalcDef->TimeIt());
   eqsol.SetOlsen(mpVccCalcDef->Olsen());
   eqsol.SetTrueHDiag(mpVccCalcDef->GetTrueHDiag());
   if (mpVccCalcDef->DiagMeth()=="LAPACK" ) 
      eqsol.SetDiagMeth("DSYEVD");
   else if (mpVccCalcDef->DiagMeth()=="MIDAS")
      eqsol.SetDiagMeth("MIDAS_JACOBI");
   else if (mpVccCalcDef->DiagMeth()=="DSYEVD" || 
         mpVccCalcDef->DiagMeth()=="MIDAS_JACOBI") 
      eqsol.SetDiagMeth(mpVccCalcDef->DiagMeth());
   else
   {
      MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override! ");
      eqsol.SetDiagMeth("DSYEVD");
   }
   if (mpVccCalcDef->GetLevel2Solver()=="MIDAS")
      eqsol.SetLevel2Solver("MIDAS_CG");
   else if (mpVccCalcDef->GetLevel2Solver()=="LAPACK") 
      eqsol.SetLevel2Solver("DSYSV");
   else if (mpVccCalcDef->GetLevel2Solver()=="DSYSV" || 
         mpVccCalcDef->GetLevel2Solver()=="MIDAS_CG" || 
         mpVccCalcDef->GetLevel2Solver()=="SVD") 
      eqsol.SetLevel2Solver(mpVccCalcDef->GetLevel2Solver());
   else
   {
      MidasWarning("Only DSYSV (LAPACK), MIDAS_CG (MIDAS), or SVD are allowed Level2Solver. Override! ");
      eqsol.SetLevel2Solver("DSYSV");
   }
   In nroots2 = I_1;
   if (mpVccCalcDef->VciH2()) nroots2 = max(nroots2,mpVccCalcDef->VciH2Roots());
   if (nroots2 > nvecsize) 
   {
      nroots2 = nvecsize;
      Mout << " Number of roots reset to the dimension of the space " << endl;
   }
   In n_targs = I_0;
   if (mpVccCalcDef->TargetSpace()) 
   {
      n_targs = PrepareTargetStates(pVscfCalcDef()->GetmVscfAnalysisDir() + "/TargetVectors", "TargetVector", trf.GetTransXvec());
      if (n_targs> nvecsize) n_targs = nvecsize;
      nroots2 = n_targs;
      Mout << " Number of roots reset to the number of target states " << endl;
   }
   eqsol.SetNeq(nroots2);
   vector<DataCont> vcih2_vecs;
   vcih2_vecs.reserve(mpVccCalcDef->GetRedDimMax());
   // The solution vectors which are trial vector for the real calc.

   // Try to start with the VCI eigenvalue of (H-eps)^2 as guess.
   // in the same space

   bool start_cih2eig = mpVccCalcDef->H2Start()  && !mpVccCalcDef->Restart();
   bool start_vmp     = mpVccCalcDef->VmpStart() && mpVccCalcDef->Vmp();

   string storage_mode = "OnDisc";
   if (mpVccCalcDef->VecStorage()==I_0) storage_mode = "InMem";

   bool state_found = false;
   In   i_state_found = -I_1;

   if (start_cih2eig || mpVccCalcDef->VciH2())
   {
      eqsol.SetEigEq();
      eqsol.SetDim(nvecsize);
      Mout << " Dimensionality of Vci-space for (H-E)^2 calc: " 
         << nvecsize << endl;
      // E is stored ias mLambda in Transformer.
      trf.SetLambda(mpVccCalcDef->GetEfinal()); // Either defaul (VSCF) energy is used
      if (mpVccCalcDef->VciH2()&&mpVccCalcDef->H2LevelShiftSet())
         trf.SetLambda(mpVccCalcDef->H2LevelShift());
         // ..  or input value.
      Mout << " Level shift E: " << trf.GetLambda() << endl;

      // Set threshold not to tight if only for restart.
      eqsol.SetResidThreshold(sqrt(mpVccCalcDef->GetItEqResidThr())/C_10);
      if (mpVccCalcDef->VciH2()) 
         eqsol.SetResidThreshold(mpVccCalcDef->GetItEqResidThr());

      bool res_suc = false;
      for (In i_eq=I_0;i_eq<nroots2;i_eq++)
      {
         string s2 = mpVccCalcDef->GetName()+"_VciH2_"+std::to_string(i_eq);

         bool robust = true;
         bool check = true;
         bool act_restart = mpVccCalcDef->Restart();
         res_suc = false;
         DataCont tmp_datacont;
         if (mpVccCalcDef->Restart())
         {
            vcih2_vecs.push_back(tmp_datacont);
            vcih2_vecs[i_eq].NewLabel(s2,false);
            vcih2_vecs[i_eq].ChangeStorageTo("OnDisc");
            vcih2_vecs[i_eq].SetNewSize(nvecsize);
            if (vcih2_vecs[i_eq].ChangeStorageTo(storage_mode,robust,check))
            {
               Mout << " Restart vector readin succeeded. " << endl; 
               Mout << " Norm of restart vector " << vcih2_vecs[i_eq].Norm() << endl;
               res_suc = true;
            }
            else
            {
               Mout << " Restart vector readin failed! " << endl; 
               Mout << " Start vector guess is default or Vmp " << endl; 
               act_restart = false;
            }
         }

         // Save vmp vector to ensure it is not gone after this calc, and prepare.
         if (!res_suc && start_vmp && mpVccCalcDef->Vmp()) 
         {
            {
               vcih2_vecs.push_back(tmp_datacont);
               string smp = mpVccCalcDef->GetName()+"_Vmp_sum_wf";
               vcih2_vecs[i_eq].SetNewSize(I_0);
               vcih2_vecs[i_eq].NewLabel(smp,false);
               vcih2_vecs[i_eq].ChangeStorageTo("OnDisc");
               vcih2_vecs[i_eq].SetNewSize(nvecsize);
               if (vcih2_vecs[i_eq].ChangeStorageTo("OnDisc",robust,check))
               {
                  res_suc = true;
                  Mout << " Vmp start vector read in succesfull from " << smp << endl; 
                  Mout << " Norm of vmp start vector " << vcih2_vecs[i_eq].Norm() << endl;
               }
               else 
                  Mout << " Vmp start failed - start vector guess as default will be attempted."
                       << endl;
            }
            if (res_suc) // Do a little effort, to ensure that vmp file is kept untouched.
            {
               DataCont tmp(vcih2_vecs[i_eq]); // make a copy.
               string lbl = vcih2_vecs[i_eq].Label();//change original to correct name but store orig vmp label
               vcih2_vecs[i_eq].NewLabel(s2);
               tmp.NewLabel(lbl); // set copy to have the orig vmp label
               tmp.SaveUponDecon(true); // ensure that file is kept after this scope

               vcih2_vecs[i_eq].ChangeStorageTo(storage_mode); // and then set the storage mode and size to the correct ones. 
               vcih2_vecs[i_eq].SetNewSize(nvecsize);
            }
         }
         if (!res_suc)
         {
            vcih2_vecs.push_back(tmp_datacont);
            Mout << " Default start vector "<< endl;
            vcih2_vecs[i_eq].NewLabel(s2);
            vcih2_vecs[i_eq].ChangeStorageTo(storage_mode);
            vcih2_vecs[i_eq].SetNewSize(nvecsize);
            if (i_eq==I_0 && nroots2 == I_1)
            {
               Mout << " Set vector to unit vector " << endl; 
               vcih2_vecs[i_eq].SetToUnitVec(i_eq);
               res_suc = true;
            }
            else
            {
               Mout << " Set vector to first zero vector, later standard guess " << endl; 
               vcih2_vecs[i_eq].Zero();
               res_suc = true;
            }
         }
         // if TargetSpace
         if (!act_restart && mpVccCalcDef->TargetSpace())
            GetTargetForState("TargetVector", i_eq, vcih2_vecs[i_eq]);
         
         vcih2_vecs[i_eq].SaveUponDecon(true);
         eqsol.SetRestart(res_suc);
         // Mout << " ensure saved upon decon " << vcih2_vecs[i_eq].Label() << endl;
      }
      MidasVector eig_vals;

      bool restart = res_suc;
      if (restart && mpVccCalcDef->TargetSpace()) 
         RestartWithTargets(vcih2_vecs,nroots2,nvecsize);

      restart = restart || mpVccCalcDef->TargetSpace();
      eqsol.SetRestart(restart);
      if (mpVccCalcDef->TargetSpace())
      {
         eqsol.SetTargetSpace(mpVccCalcDef->TargetSpace());
         eqsol.SetTargetFileName("TargetVector_");
         eqsol.SetNtargs(n_targs);
         eqsol.SetTargetingMethod(mpVccCalcDef->GetTargetingMethod());
         eqsol.SetOverlapMin(mpVccCalcDef->GetOverlapMin());
         eqsol.SetOverlapSumMin(mpVccCalcDef->GetOverlapSumMin());
         eqsol.SetEnergyDiffMax(mpVccCalcDef->GetEnergyDiffMax());
         eqsol.SetResidThrForOthers(mpVccCalcDef->GetResidThrForOthers());
         eqsol.SetEnerThrForOthers(mpVccCalcDef->GetEnerThrForOthers());
      }
      // Size Output
      if (mpVccCalcDef->SizeIt()) 
         for (In i_eq=I_0;i_eq<nroots2;i_eq++)
            Mout << " DoVciH2 vcih2_vecs i_eq = " << i_eq << " - Total size: " << vcih2_vecs[i_eq].SizeOut()<<endl;
      // Solve equations
      if (eqsol.Solve(eig_vals,vcih2_vecs)) 
      {
         Mout << " VciH2 converged to requested threshold " << endl;
      }
      else 
      {
         Mout << " VciH2 not converged to requested threshold " << endl;
         string s1 = " VciH2 not converged, name = " + mpVccCalcDef->GetName();
         MidasWarning(s1);
      }

      Nb conv = C_AUTKAYS;
      //Nb eig_val_0 = eig_vals[I_0];
      Nb cmax = C_0;
      for (In i_eq=I_0;i_eq<nroots2;i_eq++)
      {
         Nb eig_val = eig_vals[i_eq];
         Nb resid;
         Mout << endl << endl << endl
              << " VciH2_Root_" << i_eq << " EigenValue                              " 
              << eig_val << " orig units "<<endl;
         {
            DataCont tmp1(vcih2_vecs[i_eq]);
            trf.TransH(vcih2_vecs[i_eq],tmp1,I_1);
            eig_val = Dot(vcih2_vecs[i_eq],tmp1);
            tmp1.Axpy(vcih2_vecs[i_eq],-eig_val);
            resid = tmp1.Norm();
            tmp1.SaveUponDecon(false);
         }
         //if (i_eq==I_0) eig_val_0 = eig_val;
         Nb abse = fabs(eig_vals[i_eq]);
         if (eig_vals[i_eq]<C_0) Mout << " ??? negative eigenvalue of (H-lambda)^2 ?!?!? " << endl;
         Nb lplus  = trf.GetLambda() + sqrt(abse);
         Nb lminus = trf.GetLambda() - sqrt(abse);
         Nb tol = mpVccCalcDef->GetItEqResidThr();
         bool plus  = false;
         bool minus = false;
         if (fabs(lplus-eig_val)<fabs(lminus-eig_val)) plus = true;
         else minus = true;

         Mout << " Ordinary VCI residual with this eigenvector and energy       = "
              << resid << endl;
         if (plus && fabs(lplus-eig_val)<tol)
            Mout << " Eigenvalue lies above level shift and is (level-shift + red. eig) = "
                 << lplus << endl
                 << " Diff between (level-shift + red-eig and <i|H|i> below) = "
                 << lplus - eig_val << endl;
         else if (minus && fabs(lminus-eig_val)<tol)
            Mout << " Eigenvalue lies above level shift (level-shift + red. eig) = "
                 << lminus << endl
                 << " Diff between (level-shift + red-eig and <i|H|i> below) = "
                 << lminus - eig_val << endl;
         else 
         {
            Mout << " Not very clear where eigen value is relative to level shift " << endl
                 << " H eigen value should be (+) " << lplus << " or (-) " 
                 << lminus << " calc. from (H-lambda)^2 eigenvalues " << endl;
            Nb lamd = (eig_val-trf.GetLambda())*(eig_val-trf.GetLambda());
            Mout << " (H-lambda)^2 eigen value should be " << lamd 
                 << " from <i|H|i> exp. value (i should be H-lambda eigenstate) " << endl;
         }

         Mout << "\n\n\n <i|H|i>_i=_" << i_eq;
         Mout << " EigenValue                              " 
               << eig_val << " orig units ";
         //Mout << "\n <i|H|i>_i=_" << i_eq;
         //Mout << " EigenValue, rel. to lowest              " 
            //<< eig_val-eig_val_0 << " orig units ";
         Mout << "\n <i|H|i>_i=_" << i_eq;
         Mout << " EigenValue                              " 
            << eig_val*conv << " au to cm-1 conversion ";
         // Mout << "\n <i|H|i>_i=_" << i_eq;
         //Mout << " EigenValue, rel. to lowest              " 
            //<< (eig_val-eig_val_0)*conv << " au to cm-1 conversion ";
         //Mout << endl;


         bool output = true;
         if (output)
         {
            nvecsize = trf.NexciTransXvec() - I_1; 
            In n_out = max(I_5,mpVccCalcDef->IoLevel());
            n_out = min(n_out,nvecsize);
            vector<Nb> largest_coef(n_out);
            vector<In> add_largest_coef(n_out);
            vcih2_vecs[i_eq].AddressOfExtrema(
                  add_largest_coef,largest_coef,n_out,I_2);
            //Mout << " Largest coefficients " << largest_coef << endl;
            //Mout << " Addresses of Largest coefficients " 
            //<< add_largest_coef << endl;

            if (add_largest_coef[I_0]==I_0) 
            {
               if (fabs(largest_coef[I_0])> cmax)
               {
                  mpVccCalcDef->SetDone(eig_val);
                  state_found = true;
                  i_state_found = i_eq;
                  cmax = fabs(largest_coef[I_0]);
               }
            }

            Mout << "\n Largest elements of vector:\n " << endl;
            Mout << "  ";
            for (In j=I_0;j<n_out;j++)
            {
               Mout << setw(23) << left;
               if (j!=-I_1) 
               {
                  Mout << largest_coef[j];
                  if (largest_coef[j]<C_0) Mout << " ";
               } 
               else 
               {
                  Mout << C_1;
               }
               Mout << "* Psi_" <<  setw(9) << left;
               if (j!=-I_1) Mout << add_largest_coef[j];
               else Mout << "Ref";
               if (j!=-I_1)ModesAndLevelsOut(Mout,add_largest_coef[j],trf.GetTransXvec());
               else ModesAndLevelsOut(Mout,I_0,trf.GetTransXvec());
               Mout << endl;
               if (j<n_out-I_1 && largest_coef[j+I_1]>=C_0) Mout << " +";
               if (j<n_out-I_1 && largest_coef[j+I_1]<C_0) Mout << " ";
            }
         }
         //vcih2_vecs[i_eq].DumpToDisc();
      }
      if (state_found) 
      { 
         Mout << "\n\n The Vci energy corresponding to the reference state is found as ";
         Mout << mpVccCalcDef->GetEfinal() << " state " << i_state_found << endl;
         Mout << left << endl;
         Mout << " VciH2 Correlation energy " << setw(30) 
            << mpVccCalcDef->GetEfinal()-GetEtot() << 
            " MaxExci = " << mpVccCalcDef->MaxExciLevel() << 
            " IsOrder = " << mpVccCalcDef->IsOrder() << endl;
         Mout << " VciH2 CorrEnerPrMode     " << setw(30) 
            << (mpVccCalcDef->GetEfinal()-GetEtot())/pOpDef()->NmodesInOp() << 
            " MaxExci = " << mpVccCalcDef->MaxExciLevel() << 
            " IsOrder = " << mpVccCalcDef->IsOrder() << endl;
      }
      else
      { 
         Mout << " No VciH2 state corresponding to the reference state is found" << endl;
         Mout << " No state found with the reference state as largest component " << endl;
         mpVccCalcDef->SetDone(C_10_10);
         string s1 = " VciH2 reference state could not be identified as the most ";
         s1 = s1  + " important among the converged state for calc. " + mpVccCalcDef->GetName();
         MidasWarning(s1);
      }
      if (mpVccCalcDef->TargetSpace()) TargetOut(eig_vals);
   }


   // Delete the vectors, but ensure they are on disc.
   //if (nroots2 >I_1) i_state_found = I_1; // a line for testing the feature
   if (state_found && i_state_found > I_0)
   {
      Mout << " Swapping VciH2 vectors such that the target state is stored as number 0 " << endl;
      DataCont tmp(vcih2_vecs[i_state_found]);
      for (In i=i_state_found;i>I_0;i--)
      {
         vcih2_vecs[i] = vcih2_vecs[i-I_1];
      }
      vcih2_vecs[I_0] = tmp;
   }
   // Delete the vectors, but ensure they are on disc.
   for (In i=I_0;i<nroots2;i++)
   {
      vcih2_vecs[i].ChangeStorageTo("OnDisc");
      //Mout << " ensure saved upon decon " << vcih2_vecs[i].Label() << " norm is " << vcih2_vecs[i].Norm() << endl;
      //string s2 = mpVccCalcDef->Name()+"_VciH2_"+std::to_string(i);
      //vcih2_vecs[i].NewLabel(s2);
      vcih2_vecs[i].SaveUponDecon(true);
   }
}
/**
* Do Vci calculation
* */
void Vcc::DoVci()
{
   if (gDebug)
   {
      Mout << " In Vcc::DoVci()" << std::endl;
   }
   
   // Transformer setup.
   VccTransformer* trf = NULL;
   if (mpVccCalcDef->V3trans())
   {
      trf = new midas::vcc::TransformerV3(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   }
   else
   {
      trf = new VccTransformer(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   }
   trf->SetType(TRANSFORMER::VCI);

   In nvecsize = trf->NexciTransXvec();
   Mout << " Dimension of VCI space: " << nvecsize << std::endl;
   
   In n_roots = mpVccCalcDef->VciRoots();
   In n_targs = I_0;
   if (mpVccCalcDef->TargetSpace()) 
   {
      n_targs = PrepareTargetStates(pVscfCalcDef()->GetmVscfAnalysisDir() + "/TargetVectors", "TargetVector", trf->GetTransXvec());
      if (n_targs > nvecsize)
         n_targs = nvecsize;
      n_roots = n_targs;
      Mout << " Number of roots reset to the number of target states." << endl;
      mpVccCalcDef->SetVci(true, n_roots);
   }
   if (n_roots > nvecsize) 
   {
      n_roots = nvecsize;
      Mout << " Number of roots reset to the dimension of the space." << endl;
      mpVccCalcDef->SetVci(true, n_roots);
   }

   In n_calc=I_1;
   if (mpVccCalcDef->DoLambdaScan())
   {
      n_calc = mpVccCalcDef->Nlambdas();
      trf->SetLambdaCalc(true);
   }

   vector<Nb> energies;
   
   bool restart = mpVccCalcDef->Restart();

   for (In i_calc=I_0;i_calc<n_calc;i_calc++)
   {
      ItEqSol eqsol(mpVccCalcDef->VecStorage(), pVscfCalcDef()->GetmVscfAnalysisDir());
      eqsol.SetEigEq();
      eqsol.SetNeq(n_roots);
      eqsol.SetTransformer(trf);
      eqsol.SetDim(nvecsize);
      eqsol.SetResidThreshold(mpVccCalcDef->GetItEqResidThr());
      eqsol.SetEnerThreshold(mpVccCalcDef->GetItEqEnerThr());
      eqsol.SetNiterMax(mpVccCalcDef->GetItEqMaxIter());
      eqsol.SetRedDimMax(mpVccCalcDef->GetRedDimMax());
      eqsol.SetBreakDim(mpVccCalcDef->GetRedBreakDim());
      eqsol.SetIoLevel(mpVccCalcDef->IoLevel());
      eqsol.SetTimeIt(mpVccCalcDef->TimeIt());
      eqsol.SetHarmonicRR(mpVccCalcDef->GetHarmonicRR());
      if (mpVccCalcDef->DiagMeth()=="LAPACK" ) 
         eqsol.SetDiagMeth("DSYEVD");
      else if (mpVccCalcDef->DiagMeth()=="MIDAS")
         eqsol.SetDiagMeth("MIDAS_JACOBI");
      else if (mpVccCalcDef->DiagMeth()=="DSYEVD" || 
            mpVccCalcDef->DiagMeth()=="MIDAS_JACOBI") 
         eqsol.SetDiagMeth(mpVccCalcDef->DiagMeth());
      else
      {
         MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override! ");
         eqsol.SetDiagMeth("DSYEVD");
      }
      if (mpVccCalcDef->GetLevel2Solver()=="MIDAS")
         eqsol.SetLevel2Solver("MIDAS_CG");
      else if (mpVccCalcDef->GetLevel2Solver()=="LAPACK") 
         eqsol.SetLevel2Solver("DSYSV");
      else if (mpVccCalcDef->GetLevel2Solver()=="DSYSV" || 
            mpVccCalcDef->GetLevel2Solver()=="MIDAS_CG" || 
            mpVccCalcDef->GetLevel2Solver()=="SVD") 
         eqsol.SetLevel2Solver(mpVccCalcDef->GetLevel2Solver());
      else
      {
         MidasWarning("Only DSYSV (LAPACK), MIDAS_CG (MIDAS), or SVD are allowed Level2Solver. Override! ");
         eqsol.SetLevel2Solver("DSYSV");
      }
      eqsol.SetHarmonicRR(mpVccCalcDef->GetHarmonicRR());
      if (mpVccCalcDef->GetHarmonicRR())
      {
         eqsol.SetDiagMeth("DGGEV");
         eqsol.SetLevel2Solver("DSYSV");
      }
      eqsol.SetPreDiag(mpVccCalcDef->PreDiag());
      eqsol.SetImprovedPrecond(mpVccCalcDef->GetImprovedPrecond());
      eqsol.SetPrecondExciLevel(mpVccCalcDef->GetPrecondExciLevel());
      eqsol.SetTrueHDiag(mpVccCalcDef->GetTrueHDiag());
      eqsol.SetNpreDiag(mpVccCalcDef->NpreDiag());
      eqsol.SetOlsen(mpVccCalcDef->Olsen());
      eqsol.SetSolVecFileName(mpVccCalcDef->GetName() + "_Vci_vec_");
      eqsol.SetNresvecs(mpVccCalcDef->GetNresvecs());
      eqsol.SetEnerShift(mpVccCalcDef->GetEnerShift());
      
      // if lambda calc. put the current lambda in.
      if (mpVccCalcDef->DoLambdaScan()) 
      {
         trf->SetLambda(mpVccCalcDef->GetLambda(i_calc));
         Mout << " VCI calculation for lambda = " << mpVccCalcDef->GetLambda(i_calc) << endl;
         Mout << " restart = " << StringForBool(restart) << "." << endl;
      }
   
      // The solution vectors on return.
      vector<DataCont> civecs;
      civecs.reserve(std::max(n_roots, mpVccCalcDef->GetRedDimMax()));
      string storage_mode = "OnDisc";
      if (mpVccCalcDef->VecStorage()==I_0)
         storage_mode = "InMem";
      DataCont tmp_datacont;
      for (In i_eq=I_0;i_eq<n_roots;i_eq++)
      {
         civecs.push_back(tmp_datacont);
         string s = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i_eq);
         civecs[i_eq].NewLabel(s);

         bool res_suc = false;
         if (restart) 
         {
            if (RestartFromPrev(trf->GetTransXvec(),civecs[i_eq],i_eq,nvecsize,storage_mode,"VCC"))
            {
               Mout << " Restart vector read in succesfully for root " << i_eq << "." << endl;
               Mout   << " Norm of restart vector: " << civecs[i_eq].Norm()
                    << ", size: " << civecs[i_eq].Size() << endl;
               res_suc = true;
            }
            else
            {
               Mout << " Restart failed." << endl; 
               Mout << " Set vector to first zero vector, later standard guess." << endl; 
               civecs[i_eq].ChangeStorageTo("OnDisc");
               civecs[i_eq].SetNewSize(nvecsize);
               civecs[i_eq].Zero();
            }
         }
         else
         {
            civecs[i_eq].ChangeStorageTo(storage_mode);
            civecs[i_eq].SetNewSize(nvecsize);
            civecs[i_eq].Zero();
         }
         if (!res_suc && mpVccCalcDef->TargetSpace())
            GetTargetForState("TargetVector", i_eq, civecs[i_eq]);
         civecs[i_eq].SaveUponDecon(true);
      }
      In num_vecs=mpVccCalcDef->GetNresvecs();
      if (num_vecs>n_roots)
      {
         for (In i_eq=n_roots;i_eq<num_vecs;i_eq++)
         {
            civecs.push_back(tmp_datacont);
            string s = mpVccCalcDef->GetName() + "_Vci_vec_"+std::to_string(i_eq);
            civecs[i_eq].NewLabel(s);

            if (RestartFromPrev(trf->GetTransXvec(),civecs[i_eq],i_eq,nvecsize,storage_mode,"VCC"))
            {
               Mout << " Restart (trial) vector read in succesfully for root " << i_eq << "." << endl;
               Mout   << " Norm of restart vector: " << civecs[i_eq].Norm()
                  << ", size: " << civecs[i_eq].Size() << endl;
            }
            else
            {
               Mout << " Restart failed." << endl; 
               Mout << " Set vector to first zero vector, later standard guess." << endl; 
               civecs[i_eq].ChangeStorageTo("OnDisc");
               civecs[i_eq].SetNewSize(nvecsize);
               civecs[i_eq].Zero();
            }
            civecs[i_eq].SaveUponDecon(true);
         }
      }

      if (restart && mpVccCalcDef->TargetSpace()) 
         RestartWithTargets(civecs, n_roots, nvecsize);
      WriteRestartInfo(nvecsize, pVscfCalcDef()->GetmVscfAnalysisDir() + "/VCC", "_Vci_vec_");
       
      MidasVector eig_vals;
      restart = restart || mpVccCalcDef->TargetSpace();
      eqsol.SetRestart(restart);
      if (mpVccCalcDef->TargetSpace())
      {
         eqsol.SetTargetSpace(mpVccCalcDef->TargetSpace());
         eqsol.SetTargetFileName("TargetVector_");
         eqsol.SetNtargs(n_targs);
         eqsol.SetTargetingMethod(mpVccCalcDef->GetTargetingMethod());
         eqsol.SetOverlapMin(mpVccCalcDef->GetOverlapMin());
         eqsol.SetOverlapSumMin(mpVccCalcDef->GetOverlapSumMin());
         eqsol.SetEnergyDiffMax(mpVccCalcDef->GetEnergyDiffMax());
         eqsol.SetResidThrForOthers(mpVccCalcDef->GetResidThrForOthers());
         eqsol.SetEnerThrForOthers(mpVccCalcDef->GetEnerThrForOthers());
      }
      if (mpVccCalcDef->SizeIt()) 
         for (In i_eq=I_0;i_eq<n_roots;i_eq++)
            Mout << " DoVci() i_eq = " << i_eq << " - Total size: " << civecs[i_eq].SizeOut()
                 << endl;

      bool sequence=mpVccCalcDef->EigValSeqSet();
      Timer time_solve;
      if (sequence)
      {
         In n_roots_at_a_time = mpVccCalcDef->EigValSeq();
         In n_seq       = n_roots/n_roots_at_a_time;
         Mout << endl << " Sequence calculation of eigenvalues with " << n_roots_at_a_time
              << " roots at a time in " << n_seq << " batches." << endl;
         In n_rest = n_roots - n_seq*n_roots_at_a_time;
         if (n_rest!=I_0)
            n_seq++;
         In n_roots_now = I_0;
         for (In i_seq=I_0; i_seq<n_seq; i_seq++)
         {
            if (i_seq!=n_seq-I_1 || n_rest == I_0)
               n_roots_now += n_roots_at_a_time;
            if (i_seq==n_seq-I_1 && n_rest!= I_0)
               n_roots_now += n_rest;
            eqsol.SetNeq(n_roots_now);
            if (mpVccCalcDef->TargetSpace())
               eqsol.SetNtargs(n_roots_now);
            Mout << endl << " Sequence nr. " << i_seq << " with " << n_roots_now
                 << " roots." << endl;
            eqsol.Solve(eig_vals, civecs);
            eqsol.SetRestart(true); // Now restart should certainly be set. 
         }
      }
      else
      {
         eqsol.Solve(eig_vals, civecs);
      }
      if (gTime)
      {
         string s_time = " Wall time used in Solve ";
         time_solve.WallOut(Mout,s_time);
      }
      restart = true;
      Mout << endl << endl
           << " Results of reduced space iterative VCI eigenvalue solution in original"
           << " units and with au to cm-1 conversion." << endl;
      if (mpVccCalcDef->DoLambdaScan())
         Mout << " Results are for mLambda = " << trf->GetLambda() << "." << endl;
      In n_out = min(I_5,nvecsize);
      Mout << endl << endl
           << " Analysis of eigen vector inclues the " << n_out << " largest elements of vector "
           << endl
           << " The first address set refers to modes and modals excited!" << endl
           << " Nb! modals space ordering with occupied first (no matter which as zero)." << endl
           << " The second is converted to primitive assuming 1-1 correspondence with" << endl
           << " modals (ok for low correlated systems)." << endl;
      n_roots=civecs.size();

      if (this->pVccCalcDef()->MatRepFvciAnalysis())
      {
         const auto& mcr = trf->GetXvecModeCombiOpRange();
         const auto& n_modals_in = trf->GetNModals();
         const std::vector<Uin> n_modals(n_modals_in.begin(), n_modals_in.end());
         const midas::tdvcc::ParamsTdvci<Nb,GeneralDataCont> params(civecs.at(0));
         DataCont tmp_dc = midas::tdvcc::ConvertKetToFvci(params, n_modals, mcr);
         mSavedFvciVecKet.SetNewSize(Size(tmp_dc));
         tmp_dc.DataIo(IO_GET, mSavedFvciVecKet, Size(mSavedFvciVecKet));
         mSavedFvciVecBra = mSavedFvciVecKet.Conjugate();
      }

      for (In i=I_0;i<n_roots;i++)
      {
         energies.push_back(eig_vals[i]);
         if (i==I_0 && mpVccCalcDef->DoRsp()) 
         {
            mpVccCalcDef->SetDone(eig_vals[I_0]);
            string s_war = mpVccCalcDef->GetName()
                         + " VCI calc. rsp. reference energy set to first state but there are"
                         + " more states.";
            if (n_roots>I_1) MidasWarning(s_war);
         }
         Mout << endl << endl << endl << " Vci_Root_" << i;
         In sznb = sizeof(Nb);
         In nprec = sznb*I_2;
         midas::stream::ScopedPrecision(nprec, Mout);
         if (mpVccCalcDef->DoLambdaScan()) 
            Mout << " EigenValue for lambda = " << setw(23)
                 << trf->GetLambda() << " is: " << eig_vals[i] << " orig units.";
         else
         {
            Mout << " EigenValue                              "  << eig_vals[i] << " orig units";
         }
         Mout << endl
              << " Vci_Root_" << i << " EigenValue, rel. to lowest              " 
              << eig_vals[i]-eig_vals[I_0] << " orig units" << endl
              << " Vci_Root_" << i << " EigenValue                              " 
              << eig_vals[i]*C_AUTKAYS << " cm-1" << endl
              << " Vci_Root_" << i << " EigenValue, rel. to lowest              " 
              << (eig_vals[i]-eig_vals[I_0])*C_AUTKAYS << " cm-1" << endl;
         if (gDebug)
         {
            Mout << " Vci_Root_" << i << " eig vec:" << std::endl << civecs[i] << std::endl;
         }
         
         if (i==0) 
         {
            Mout << " Vci Correlation energy   " << setw(30)
                 << eig_vals[i]-GetEtot()
                 << " MaxExci = " << mpVccCalcDef->MaxExciLevel()
                 << " IsOrder = " << mpVccCalcDef->IsOrder() << endl;
            Mout << "(energy - VSCF reference state energy)" << endl;
            Mout << " Vci corr. energy / mode  " << setw(30) 
                 << (eig_vals[I_0]-GetEtot())/pOpDef()->NmodesInOp() 
                 << " MaxExci = " << mpVccCalcDef->MaxExciLevel() 
                 << " IsOrder = " << mpVccCalcDef->IsOrder() << endl;
         }
         Mout << endl << " Largest elements of vector: " << endl << endl;
         vector<Nb> largest_coef(n_out);
         vector<In> add_largest_coef(n_out);
         civecs[i].AddressOfExtrema(add_largest_coef,largest_coef,n_out,I_2);
         for (In j=I_0;j<n_out;j++)
         {
            if (j==I_0) Mout << "  ";
            Mout << setw(23) << left << largest_coef[j];
            if (largest_coef[j]<C_0) Mout << " ";
            Mout << "* Psi_" <<  setw(9) << left << add_largest_coef[j];
            ModesAndLevelsOut(Mout,add_largest_coef[j],trf->GetTransXvec());
            Mout << endl;
            if (j<n_out-I_1 && largest_coef[j+I_1]>=C_0) Mout << " +";
            if (j<n_out-I_1 && largest_coef[j+I_1]<C_0) Mout << " ";
         }
         if(mpVccCalcDef->GetMolMCs().size() != I_0)
            trf->GetTransXvec().PrintWeightsInMCs(mpVccCalcDef->GetMolMCs(), civecs[i]);


         if (mpVccCalcDef->RepVciAsVcc())
         {
            Mout << endl << endl
                 << " The state is represented as a Vcc wave function:" << endl;

            if (!civecs[i].IntermediateNormalize(I_0)) 
               Mout << " No intermediate normalization possible in this case." << endl;
            else
            {
               DataCont vcc_rep(civecs[i]);
               trf->VccFromVci(civecs[i],vcc_rep);
               //Mout << "vcc_rep = " << vcc_rep << endl;
               ///< Nb afterwards the civecs is intermediate normalized.
               //if (true || mpVccCalcDef->IoLevel()>I_10)
               //{
               //   Mout << " Vci eigen vector:      " << civecs[i] << endl << endl
               //        << " in Vcc representation: " << vcc_rep << endl;
               //}
               trf->SetVciVcc(true);
               Nb evcc = trf->TransCorrE(vcc_rep,I_1);
               trf->SetVciVcc(false);
               Mout << " Vcc energy = " << evcc << " diff to vci energy " 
                  << evcc-eig_vals[i] << " orig units "<<endl;
               for (In iout=I_0;iout<I_2;iout++)
               {
                  n_out = min(I_10,nvecsize);
                  vector<Nb> largest_coef(n_out);
                  vector<In> add_largest_coef(n_out);
                  if (iout ==I_0)
                  {
                     civecs[i].AddressOfExtrema(add_largest_coef,largest_coef,n_out,I_2);
                     Mout << "\n Vci vector in intermediate norm " << endl;
                  }
                  if (iout ==I_1)
                  {
                     vcc_rep.AddressOfExtrema(add_largest_coef,largest_coef,n_out,I_2);
                     Mout << "\n Vcc vector in intermediate norm " << endl;
                  }
                  for (In j=I_0;j<n_out;j++)
                  {
                     if (j==I_0) 
                     {
                        Mout << " ";
                        if (largest_coef[j]>C_0) Mout << " ";
                     }
                     Mout << setw(23) << left << largest_coef[j];
                     if (largest_coef[j]<C_0)
                        Mout << " ";
                     Mout << "* Psi_" <<  setw(9) << left << add_largest_coef[j];
                     ModesAndLevelsOut(Mout,add_largest_coef[j],trf->GetTransXvec());
                     Mout << endl;
                     if (j<n_out-I_1 && largest_coef[j+I_1]>=C_0)
                        Mout << " +";
                     if (j<n_out-I_1 && largest_coef[j+I_1]<C_0)
                        Mout << " ";
                  }
               }
               if (mpVccCalcDef->TestTransformer())
               {
                  // Do not use T1 transformed integrals as this will skrew up the integrals
                  // for later calcluations.
                  bool t1 = trf->T1TransH();
                  trf->SetT1TransH(false);
                  DataCont vcc_rep2;
                  vcc_rep2.SetNewSize(vcc_rep.Size()-I_1,false);
                  vcc_rep2.ChangeStorageTo(vcc_rep.Storage());
                  vcc_rep2.Zero();
                  vcc_rep2.NewLabel("vcc_test2");
                  DataCont vcc_rep2_trans(vcc_rep2); 
                  // Set up trans. datacont and try out.
                  vcc_rep2.Reassign(vcc_rep,I_1,I_1);
                  vcc_rep2_trans.NewLabel("vcc_test2_trans");
                  Nb ener = C_10_6;
                  trf->VccTransH(vcc_rep2,vcc_rep2_trans,I_1,I_1,ener);
                  MidasVector tmp(vcc_rep2.Size());
                  civecs[i].DataIo(IO_GET,tmp,vcc_rep2.Size(),I_1);
                  Mout << "\n A second Vcc energy = " << ener 
                     << " diff to vci energy " 
                  << ener-eig_vals[i] << " orig units "<<endl;
                  Nb tn  = vcc_rep2.Norm();
                  Mout << " Norm of Vcc vector " << vcc_rep2.Norm() << endl;
                  tn = (tn*tn+C_1);
                  Nb ttn = vcc_rep2_trans.Norm();
                  Mout << " ||e_cc|| " 
                     << ttn << " norm (1,t) " << tn 
                     << " rel " << ttn/tn << endl;
                  Nb anb=C_0;
                  vcc_rep2_trans.DataIo(IO_PUT,tmp,vcc_rep2.Size(),I_0,I_1,
                        I_0,I_1,false,anb,true,-ener);
                  ttn = vcc_rep2_trans.Norm();
                  Mout << " ||e_cc|| after -E*VCI " 
                     << ttn << " norm (1,t) " << tn 
                     << " rel " << ttn/tn << endl;
                  trf->VccTransH(vcc_rep2,vcc_rep2_trans,I_1,I_0,ener);
                  ttn = vcc_rep2_trans.Norm();
                  Mout << "\n A third Vcc energy = " << ener 
                     << " diff to vci energy " 
                  << ener-eig_vals[i] << " orig units "<<endl;
                  Mout << " ||e_cc|| after exp(-T) " 
                     << ttn << " norm (1,t) " 
                     << tn << " rel " << ttn/tn << endl;
                  trf->SetT1TransH(t1);
               }
            }
         }
      }
      if (mpVccCalcDef->TargetSpace()) TargetOut(eig_vals);
      mpVccCalcDef->StoreVciEigVals(eig_vals);

      bool test_lineq=false;
      if (test_lineq)
      {
         eqsol.SetLinEq();
         vector<DataCont> rhsvecs;                     // The rhs vectors
         rhsvecs.reserve(mpVccCalcDef->GetRedDimMax());
         DataCont tmp_datacont;
         for (In i_eq=I_0;i_eq<n_roots;i_eq++)
         {
            rhsvecs.push_back(tmp_datacont);
            //rhsvecs[i_eq].SetToUnitVec(i_eq);
            rhsvecs[i_eq] = civecs[i_eq];
            rhsvecs[i_eq].Scale(eig_vals[i_eq]);
            string s2 = "Vci_lineq_vec_"+std::to_string(i_eq);
            rhsvecs[i_eq].NewLabel(s2);
            rhsvecs[i_eq].ChangeStorageTo(storage_mode);
            rhsvecs[i_eq].SetNewSize(nvecsize);
         }
         eqsol.Solve(eig_vals,civecs,rhsvecs);
      }
      //trf->OneBodyDensity(trf->GetTransXvec(),civecs[I_0]);
   }

   if (mpVccCalcDef->DoLambdaScan()) 
   {
      trf->SetLambda(C_0);
      trf->SetLambdaCalc(false);
      Mout << endl << endl
           << " Calculated energies as function of Lambda in H = H_0 + lambda U." << endl;
      for (In i=I_0;i<n_roots;i++)
      {
         Mout << endl << endl << " Vci root number " << i << endl;
         for (In i_calc=I_0;i_calc<n_calc;i_calc++)
         {
            In add = i_calc*n_roots + i;
            Mout << setw(30) << mpVccCalcDef->GetLambda(i_calc);
            Mout << setw(30) << energies[add] << endl;
         }
      }

      bool makefig = true;
      if (n_calc < I_2) makefig = false;
      if (makefig)
      {
         string* labels   = new string [n_roots];
         MidasVector* xs  = new MidasVector [n_roots];
         MidasVector* ys  = new MidasVector [n_roots];
         In*          nda = new In [n_roots];

         for (In i=I_0;i<n_roots;i++)
         {
            labels[i] = "Vci_root_" + std::to_string(i);
            nda[i] = n_calc;
            xs[i].SetNewSize(n_calc);
            ys[i].SetNewSize(n_calc);
            for (In i_calc=I_0;i_calc<n_calc;i_calc++)
            {
               xs[i][i_calc] = mpVccCalcDef->GetLambda(i_calc);
               ys[i][i_calc] = energies[i_calc*n_roots+i];
            }
         }
         string name  = mpVccCalcDef->GetName() + "_lambda";
         // Peter: This needs to be tested
         //MakePsFig(name,labels,xs,ys,n_roots,nda);
         MakeMultiPlotPs(name, "Energy", "", labels, xs, ys, n_roots);

         delete[] nda; 
         delete[] xs;
         delete[] ys;
         delete[] labels;
      }
   }
   // output transformer summary
   Mout << "\n" << trf << std::endl;
   delete trf;
   Mout << endl << endl << " Vcc::DoVci() Done." << endl << endl;
}

/**
* For an address aI find the modes and levels excited. 
* */
void Vcc::ModesAndLevelsOut(ostream& arOut, 
      const In& aI,const Xvec& arXvec, bool aAllOut)
{
   In mcnr =  arXvec.FindModeCombiForInt(aI);
   const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
   const ModeCombi& modes = mcr.GetModeCombi(mcnr);
   In nmodes = modes.Size();
   vector<In> occ(nmodes);
   vector<In> occmax(nmodes);
   for (In i=I_0;i<nmodes;i++) 
   {
      occ[i]      = I_0;
      In i_op_mode = modes.Mode(i);
      In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
      occmax[i]   = nmod-I_1;
   }
   string lowhig = "LOWHIG";
   bool exci_only=true; 
   MultiIndex mi(occ,occmax,lowhig,exci_only);
   vector<In> idx(nmodes);
   In redadd = aI-arXvec.ModeCombiAddress(mcnr);
   mi.IvecForIn(idx,redadd);

   ostringstream out;
   out << "(";
   for (In i=I_0;i<nmodes;i++) 
   {
      out << modes.Mode(i)<<":"<<idx[i]; 
      if (i!=nmodes-I_1) out <<", ";
   }
   out << ")";
   string out_string = out.str();
   arOut << left << setw(20) <<  out_string;

   if (aAllOut)
   {
      In nmodes_all = pVscfCalcDef()->GetNmodesInOcc();
      vector<In> all(nmodes_all);
      for (In i=I_0;i<nmodes_all;i++) 
      {
         all[i] = pVscfCalcDef()->GetOccMode(i);
      }
      for (In i=I_0;i<nmodes;i++) 
      {
         In mode = modes.Mode(i);
         In iocc = pVscfCalcDef()->GetOccMode(mode);
         if (idx[i]>iocc) 
         {
            all[mode] =idx[i]; 
         }
         else 
         {
            all[mode] =idx[i]-I_1; 
         }
      }
      ostringstream out;
      out << " [";
      for (In i=I_0;i<nmodes_all;i++) 
      {
         out << all[i];
         if (i!=nmodes_all-I_1) out <<",";
      }
      out << "]";
      string out_string = out.str();
      arOut << right << setw(20) <<  out_string;
   }
}

void Vcc::ModesAndLevelsOutLatex(ostream& arOut, 
      const In& aI,const Xvec& arXvec)
{
   In mcnr =  arXvec.FindModeCombiForInt(aI);
   const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
   const ModeCombi& modes = mcr.GetModeCombi(mcnr);
   In nmodes = modes.Size();
   vector<In> occ(nmodes);
   vector<In> occmax(nmodes);
   for (In i=I_0;i<nmodes;i++) 
   {
      occ[i] = I_0;
      In i_op_mode = modes.Mode(i);
      In nmod = mpVccCalcDef->Nmodals(i_op_mode);
      occmax[i] = nmod-I_1;
   }
   string lowhig = "LOWHIG";
   bool exci_only=true; 
   MultiIndex mi(occ,occmax,lowhig,exci_only);
   vector<In> idx(nmodes);
   In redadd = aI-arXvec.ModeCombiAddress(mcnr);
   mi.IvecForIn(idx,redadd);

   for (In i=I_0;i<nmodes;i++) 
   {
      if (idx[i] > I_1)
         arOut << "\\,"<< idx[i];
      arOut << "\\nu_{" << modes.Mode(i)+I_1 << "}";
      if (i!=nmodes-I_1)
         arOut << " ";
   }
}

/**
* Do Vmp calculation
* */
void Vcc::DoVmp()
{
   In n_order = mpVccCalcDef->VmpMaxOrder();
   Mout << " In Vcc::DoVmp(): Maximum wave function order requested is: " << n_order << endl;
   
   In is_order = n_order;
   if (mpVccCalcDef->Iso())
      is_order = mpVccCalcDef->IsOrder();

   // Make sure we don't setup transformer for out-of-space calculations.
   bool oos_save = mpVccCalcDef->VccOutOfSpace();
   mpVccCalcDef->SetVccOutOfSpace(false);
   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0, is_order);
   mpVccCalcDef->SetVccOutOfSpace(oos_save);
   
   MidasVector en_vmps(n_order+2,C_0);
   MidasVector e2np1_vmps(2*n_order+2,C_0);

   en_vmps[I_0]    = mEvmp0;
   e2np1_vmps[I_0] = mEvmp0;

   // Allocate space for the perturbation vectors.
   DataCont* pert_vect       = new DataCont [n_order+I_1]; // from 0 to n_order
   DataCont* pert_vect_trans = new DataCont [n_order+I_1]; // from 0 to n_order

   // Prepare info for the datacontainers.
   string mpvec_storage = "OnDisc";                     
   if (mpVccCalcDef->VecStorage()<I_1)
      mpvec_storage = "InMem";
   // NB Size must be consistent with the mXvec size. 
   In mpvec_size = trf.NexciXvec();       
   Mout << endl
        << " The size of the Vmp vectors is:                  " << mpvec_size  << endl
        << " The Vmp vectors are kept:                        " << mpvec_storage << endl
        << " In the transformation the vector storage is:     " << trf.GetXvecStorage() << endl;
   
   for (In i_order=I_0;i_order<=n_order;i_order++)
   {
      string name = mpVccCalcDef->GetName()+"_Vmp" + std::to_string(i_order);
      pert_vect[i_order].NewLabel(name,false);
      pert_vect[i_order].ChangeStorageTo(mpvec_storage);
      name = name+"_trans";
      pert_vect_trans[i_order].NewLabel(name,false);
      pert_vect_trans[i_order].ChangeStorageTo(mpvec_storage);
   }

   // Set start vector to correct name. 
   if (n_order >= I_0) 
   {
      pert_vect[I_0].SetNewSize(mpvec_size);
      pert_vect[I_0].Zero();
      Nb c = C_1;
      pert_vect[I_0].DataIo(IO_PUT,I_0,c);
   }

   Timer time_it;
   for (In i_order=I_1;i_order<=n_order+I_1;i_order++)
   {
      // Make the sigma = U C transformation
      pert_vect_trans[i_order-I_1].SetNewSize(mpvec_size,false);
      trf.AssignXvec(pert_vect[i_order-I_1]);
      trf.VmpTransformer();
      trf.GetTransXvec(pert_vect_trans[i_order-I_1]);
      
      // Get the energy - the first element of sigma_k = (H-H0)Psi_k-1
      Nb e_vmp;
      pert_vect_trans[i_order-I_1].DataIo(IO_GET,I_0,e_vmp);
      en_vmps[i_order] = e_vmp;

      // Create next order vector and manipulate it.... Actually do it allways.
      if (i_order <= n_order) 
      {
         pert_vect[i_order] = pert_vect_trans[i_order-I_1];
         NextOrderVmpWf(trf, pert_vect, i_order, en_vmps);
      }
      if (i_order == n_order)
      {
         Mout << " i_order            = " << i_order << endl;
         Mout << " en_vmps[i_order]   = " << en_vmps[i_order] << endl;
         Mout << " en_vmps[i_order+1] = " << en_vmps[i_order+1] << endl;
         en_vmps[i_order+I_1] = trf.TransCorrE(pert_vect[i_order],I_1);
      }
      if (mpVccCalcDef->TimeIt()) 
      {
         string s_time = " CPU  time used in VMP iteration " + std::to_string(i_order) + ":";
         time_it.CpuOut(Mout,s_time);
      }
   }


   Mout << endl << " Summary of energies in various orders, the n+1 energies are:" << endl;
   Nb e_accum = 0;
   Nb conv = C_AUTKAYS;
   In sznb = sizeof(Nb);
   In nprec = sznb*I_2;
   In nspace = nprec+I_4; 
   In nspace2 = Mout.precision()+I_1;
   for (In n=I_0;n<=n_order+I_1;n++)
   {
      e_accum += en_vmps[n];
      Mout << "O_" << setw(5)  << left << n << setw(nspace2) << right << en_vmps[n] 
         << " " << setw(nspace2) << e_accum << " ";
      Mout << setw(nspace2) << en_vmps[n]*conv << " " << setw(nspace2) << e_accum*conv << endl;
   }

   string name = pVscfCalcDef()->GetmVscfAnalysisDir() + "/" + mpVccCalcDef->GetName() + "_vmp_res";
   midas::mpi::OFileStream mpout(name);
   
   mpout.setf(ios::scientific);
   mpout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(nprec, mpout);
   mpout.setf(ios::showpoint);

   Mout << endl << " Summary of energies in various orders, the 2n+1 energies are:" << endl;
   e_accum = C_0;
   if (n_order >= I_0) 
   {
      In i2npi = I_0;
      e2np1_vmps[i2npi] = en_vmps[i2npi];
      e_accum += e2np1_vmps[i2npi];
      Mout << "O_" << setw(5)  << left << i2npi << setw(nspace2) << right << e2np1_vmps[i2npi] 
         << " " << setw(nspace2) << e_accum << " ";
      Mout << setw(nspace2) << e2np1_vmps[i2npi]*conv << " " << setw(nspace2) 
         << e_accum*conv << endl;
      MidasVector e_pade(e2np1_vmps);
      mpout << setw(5)  << left << i2npi << setw(nspace) << right << e2np1_vmps[i2npi] 
         << " " << setw(nspace) << e_accum << " ";
      mpout << setw(nspace) << e2np1_vmps[i2npi]*conv << " " << setw(nspace) 
         << e_accum*conv << endl;
   }
   if (n_order >= I_1) 
   {
      In i2npi = I_1;
      e2np1_vmps[i2npi] = en_vmps[i2npi];
      e_accum += e2np1_vmps[i2npi];
      Mout << "O_" << setw(5)  << left << i2npi << setw(nspace2) << right << e2np1_vmps[i2npi] 
         << " " << setw(nspace2) << e_accum << " ";
      Mout << setw(nspace2) << e2np1_vmps[i2npi]*conv << " " << setw(nspace2) 
         << e_accum*conv << endl;
      mpout << setw(5)  << left << i2npi << setw(nspace) << right << e2np1_vmps[i2npi] 
         << " " << setw(nspace) << e_accum << " ";
      mpout << setw(nspace) << e2np1_vmps[i2npi]*conv << " " << setw(nspace) 
         << e_accum*conv << endl;
   }
   In i2npi=I_0;
   for (In n=I_1;n<=n_order;n++)
   {
      for (In i=I_0;i<=I_1;i++)
      {
         i2npi  = I_2*n + i;
         // C^n+i-1 U C^n
         Nb ethis = Dot(pert_vect[n+i-I_1],pert_vect_trans[n]);
         //Mout << " C^n+i-1 U C^n " << ethis << endl;

         for (In k=I_1;k<=i2npi-I_2;k++)
         {
            for (In m=max(I_1,n+i-k);m<=min(n+i-I_1,i2npi-k-I_1);m++)
            {
               In i1 = m;
               In i2 = i2npi-m-k;
               Nb dotpt= Dot(pert_vect[i1],pert_vect[i2]);
               ethis -= e2np1_vmps[k]*dotpt;
            }
         }
         e2np1_vmps[i2npi] = ethis;
         e_accum += ethis;
         Mout << "O_" << setw(5)  << left << i2npi << setw(nspace2) << right 
            << e2np1_vmps[i2npi] << " " << setw(nspace2) << e_accum << " ";
         Mout << setw(nspace2) << e2np1_vmps[i2npi]*conv << " " << setw(nspace2) 
            << e_accum*conv << endl;
         mpout << setw(5)  << left << i2npi << setw(nspace) << right 
            << e2np1_vmps[i2npi] << " " << setw(nspace) << e_accum << " ";
         mpout << setw(nspace) << e2np1_vmps[i2npi]*conv << " " << setw(nspace) 
            << e_accum*conv << endl;
      }
   }

   // Patch to put the last eigenvalue vector into the summary part
   mpVccCalcDef->SetDone(e_accum);

   Mout << endl << " Summary of wave function norms in various orders:" << endl;
   for (In n=I_0;n<=n_order;n++)
   {
      Nb normn = pert_vect[n].Norm();
      Mout << "O_" << setw(5)  << left << n << setw(25) 
         << right << normn << endl;
   }
   if (n_order >I_0)
   {
      DataCont pert_vect_sum(pert_vect[n_order]);
      string name = mpVccCalcDef->GetName()+"_Vmp_sum_wf";
      pert_vect_sum.NewLabel(name);
      for (In n=n_order-I_1;n>=I_0;n--)
      {
         pert_vect_sum.Axpy(pert_vect[n],C_1);
      }
      pert_vect_sum.SaveUponDecon(true);
      Mout << endl << " Norm of the sum over perturbed wave functions: "
           << pert_vect_sum.Norm() << endl;
   }

   if (mpVccCalcDef->Pade())
   {
      In n_tot = 2*n_order+1;
      MidasVector e_pade(n_tot+I_1,C_0);

      bool test1 = false; // checks, but note that they are numerically very unstable.
      if (test1)
      {
         if (n_tot >= I_0) e2np1_vmps[I_0] = C_0;
         if (n_tot >= I_1) e2np1_vmps[I_1] = C_0;
         if (n_tot >= I_2) e2np1_vmps[I_2] = -0.060297;
         if (n_tot >= I_3) e2np1_vmps[I_3] = -0.016482;
         if (n_tot >= I_4) e2np1_vmps[I_4] = -0.005924;
         if (n_tot >= I_5) e2np1_vmps[I_5] = -0.002540;
         if (n_tot >= I_6) e2np1_vmps[I_6] = -0.001226;
         if (n_tot >= I_7) e2np1_vmps[I_7] = -0.000629;
         if (n_tot >= I_8) e2np1_vmps[I_8] = -0.000330;
         if (n_tot >= I_9) e2np1_vmps[I_9] = -0.000173;
         if (n_tot >= I_10) e2np1_vmps[I_10] = -0.000088;
         if (n_tot >= I_11) e2np1_vmps[I_11] = -0.000043;
         if (n_tot >= I_12) e2np1_vmps[I_12] = -0.000020;
         if (n_tot >= I_13) e2np1_vmps[I_13] = -0.000009;
      }
      bool test2 = false;
      if (test2)
      {
         if (n_tot >= I_0) e2np1_vmps[I_0] = C_0;
         if (n_tot >= I_1) e2np1_vmps[I_1] = C_0;
         if (n_tot >= I_2) e2np1_vmps[I_2] = -0.061081;
         if (n_tot >= I_3) e2np1_vmps[I_3] = -0.020769;
         if (n_tot >= I_4) e2np1_vmps[I_4] = -0.009118;
         if (n_tot >= I_5) e2np1_vmps[I_5] = -0.004283;
         if (n_tot >= I_6) e2np1_vmps[I_6] = -0.002107;
         if (n_tot >= I_7) e2np1_vmps[I_7] = -0.000984;
         if (n_tot >= I_8) e2np1_vmps[I_8] = -0.000429;
         if (n_tot >= I_9) e2np1_vmps[I_9] = -0.000156;
         if (n_tot >= I_10) e2np1_vmps[I_10] = -0.000034;
         if (n_tot >= I_11) e2np1_vmps[I_11] = -0.000013;
         if (n_tot >= I_12) e2np1_vmps[I_12] = -0.000026;
         if (n_tot >= I_13) e2np1_vmps[I_13] = -0.000025;
      }

      if (n_tot >= I_0) e_pade[I_0] = e2np1_vmps[I_0];
      if (n_tot >= I_1) e_pade[I_1] = e2np1_vmps[I_1] + e2np1_vmps[I_0];
      if (n_tot >= I_2) e_pade[I_2] = e2np1_vmps[I_2] + e2np1_vmps[I_1] + e2np1_vmps[I_0];
      if (n_tot >= I_3)
      {
         for (In ip = I_3;ip<=n_tot;ip++)
         {
            In n_pade_1 = (ip-I_1)/I_2;
            In eo       = ip%I_2; // eo=0, [n,n], eo=1 [n,n-1]
            In n_pade_2 = n_pade_1 - eo;
            //Mout << " Calculate ["<< n_pade_1 << ","<<n_pade_2 << "]"<<endl;

            In n_vdim = n_pade_1;// - I_1 + eo;
            MidasVector v(n_vdim);
            MidasMatrix m(n_vdim);
   
            for (In i=I_0;i<n_vdim;i++)  // from e_2 to e_n+1 or e_3 to e_n+1
               v[i] = e2np1_vmps[i+I_3-eo];
            for (In i=I_0;i<n_vdim;i++)  // from e_2 to e_n+1 or e_3 to e_n+1
               for (In j=I_0;j<n_vdim;j++)  // from e_2 to e_n+1 or e_3 to e_n+1
                  m[i][j] = e2np1_vmps[i+j+I_3-eo] - e2np1_vmps[i+j+I_4-eo];
            //Mout << " v = " << v << endl;
            //Mout << " M = " << m << endl;
   
            MidasVector x(n_vdim);
            Nb resid;
            In iter;
            Nb tol_red_lineq = C_NB_EPSILON*C_10_2;
            bool solved = m.ConjGrad(x,v,resid,iter,tol_red_lineq);
            if (!solved)
            {
               Mout << " Numerical problems in solution of Pade equations " << endl;
            }

            e_pade[ip] = e2np1_vmps[I_0] + e2np1_vmps[I_1] + Dot(x,v);
            if (eo==I_0) e_pade[ip] += e2np1_vmps[I_2];
            Mout << "Pade ["<< n_pade_1 << ","<<n_pade_2 << "] = "<< e_pade[ip] << endl;
         }
      }
      Nb eprev = C_0;
      for (In i=I_0;i<=n_tot;i++)
      {
         Nb e_accum = e_pade[i];
         Nb e_this = e_pade[i] - eprev;
         Mout << "P_" << setw(5)  << left << i << setw(nspace2) << right 
            << e_this << " " << setw(nspace2) << e_accum << " ";
         Mout << setw(nspace2) << e_this*conv << " " << setw(nspace2) 
            << e_accum*conv << endl;
         eprev = e_pade[i];
         //mpout << setw(5)  << left << i << setw(nspace) << right 
          //  << e_this << " " << setw(nspace) << e_accum << " ";
         //mpout << setw(nspace) << e_pade[i2npi]*conv << " " << setw(nspace) 
            //<< e_accum*conv << endl;
      }
   }

   delete[] pert_vect;
   delete[] pert_vect_trans;

}


/**
* Find next order Vmp wave function
* */
void Vcc::NextOrderVmpWf(VccTransformer& aTrf, DataCont* apPertVect, const In& arOrder,
                         const MidasVector& arEvmps)
{
   string name_base = mpVccCalcDef->GetName();
   for (In i_order=I_1;i_order<=arOrder-1;i_order++)
   {
      // sigma_k - E_j psi_(n-k)
      Nb me = -arEvmps[i_order];
      In n_k = arOrder - i_order;
      //string name = name_base + ".Vmp_" + std::to_string(n_k);
      //mTransXvec.mXvecData.NewLabel(name,false); // Set Transvec to appropriate file name 
      apPertVect[arOrder].Axpy(apPertVect[n_k],me);
      //mXvec.mXvecData.Axpy(mTransXvec.mXvecData,me);
   }
   // Remember to give a new name for the transformed vector, otherwise problems.
   // But it's not done?
   //string name_trans = name_base + ".tmp";
   //mTransXvec.mXvecData.NewLabel(name_trans,false);
   aTrf.AssignXvec(apPertVect[arOrder]);
   aTrf.TransformerH0(-I_1,mEvmp0);
   aTrf.GetTransXvec(apPertVect[arOrder]);
}
/**
* Output energies together with overlaps
* */
void Vcc::TargetOut(MidasVector& arEigVec)
{
   Nb c_thr1 = C_I_2+C_I_4;
   Nb c_thr2 = C_I_2;
   In n = arEigVec.Size();
   //ifstream overlaps(string(gAnalysisDir+"/Target_Overlaps").c_str());
   auto overlaps = midas::mpi::FileToStringStream(string(pVscfCalcDef()->GetmVscfAnalysisDir() + "/Target_Overlaps"));
   In j;
   vector<Nb> svec(n);
   for (In i=I_0;i<n;i++) overlaps >> j >> svec[i];
   Mout << "\n\n State  Energy                  Differential Energy    Overlap =";
   Mout << " |<T|VCI>|^2 " << endl;
   for (In i=I_0;i<n;i++) 
   {
     Mout << " T_" << left << setw(I_3) << i << " " 
          << right << setw(23) << arEigVec[i] 
          << " " << right << setw(23) << arEigVec[i]-arEigVec[I_0];
     midas::stream::ScopedPrecision(8, Mout);
     Mout << " " << svec[i];
     if (svec[i]<c_thr1 && svec[i]>c_thr2) Mout << " LOW OVERLAP ";
     else if (svec[i]<=C_I_2) Mout << " CRITICALLY SMALL OVERLAP ";
     Mout << endl;
   }
   Mout << " With au to cm-1 factor: " << endl;
   Mout << " State  Energy                  Differential Energy    Overlap =";
   Mout << " |<T|VCI>|^2 " << endl;
   for (In i=I_0;i<n;i++) 
   {
     Mout << " T_" << left << setw(3) << i << " " 
          << right << setw(23) << arEigVec[i]*C_AUTKAYS << " " 
          << right << setw(23) << (arEigVec[i]-arEigVec[I_0])*C_AUTKAYS;
     midas::stream::ScopedPrecision(8, Mout);
     Mout << " " << svec[i];
     if (svec[i]<c_thr1 && svec[i]>c_thr2) Mout << " LOW OVERLAP ";
     else if (svec[i]<=C_I_2) Mout << " CRITICALLY SMALL OVERLAP ";
     Mout << endl;
   }
}
/**
* Restart vectors are in, but if TargetSpace check that
* the vectors in are those that correspond to targets else
* get old restarts.
* */
void Vcc::RestartWithTargets(vector<DataCont>& apVecs, In aNroots, In aNvecSize)
{
   Mout << "\n\n Test if restart vectors have sufficient overlaps with Targets " 
        << endl;
   Nb thr_target = C_I_2;
   Mout << " Threshold target is " << thr_target << endl;
   vector<In> ok_overlap(aNroots);
   vector<Nb> overlaps(aNroots);
   vector<In> used_ci(aNroots);
   for (In i_eq=I_0;i_eq<aNroots;i_eq++)
   {
      ok_overlap[i_eq] = -I_1; // -I_1 means not found
      overlaps[i_eq] = -C_NB_MAX; 
      used_ci[i_eq] = -I_1;
   }
   for (In i_eq=I_0;i_eq<aNroots;i_eq++)
   {
      DataCont target; 
      string name = "TargetVector_"+std::to_string(i_eq);
      target.GetFromExistingOnDisc(aNvecSize,name);
      target.SaveUponDecon();
      for (In i_eq2=I_0;i_eq2<aNroots;i_eq2++)
      {
         Nb c = Dot(target,apVecs[i_eq2]);
         Nb s = c*c;
         if (s>thr_target) 
         {
            ok_overlap[i_eq] = i_eq2;
            overlaps[i_eq] = s;
            used_ci[i_eq2] = i_eq; 
            break;
         }
         else if (s>overlaps[i_eq]) overlaps[i_eq]= s;
      }
   }
   In n_not_rep = I_0;
   Mout << "\n\n Targets and Existing vectors compared: \n" << endl;
   for (In i_eq=I_0;i_eq<aNroots;i_eq++)
   {
      Mout << " Target " << i_eq;
      if (ok_overlap[i_eq]==-I_1) Mout << " not ";
      Mout << " found among vci with sufficient overlap: " 
           << ok_overlap[i_eq] << " s = " << overlaps[i_eq] << endl;
      if (ok_overlap[i_eq]==-I_1) n_not_rep++;
   }
   for (In i_eq=I_0;i_eq<aNroots;i_eq++)
   {
      Mout << " VCI " << i_eq;
      if (used_ci[i_eq]==-I_1) Mout << " not ";
      Mout << " used to represent target vector "
           << used_ci[i_eq] << endl;
   }
   if (n_not_rep>I_0)
   {
      bool rescue = true;
      Nb thr_rescue = C_I_10; // At least 10% must be there. 
      if (rescue)
      {
         Mout << " Rescue - try some old VCI even if overlap is small: " << endl;
         for (In i_eq=I_0;i_eq<aNroots;i_eq++)
         {
            DataCont target; 
            string name = "TargetVector_"+std::to_string(i_eq);
            target.GetFromExistingOnDisc(aNvecSize,name);
            target.SaveUponDecon();
            for (In i_eq2=I_0;i_eq2<aNroots;i_eq2++)
            {
               if (used_ci[i_eq2]==-I_1) // not used yet. 
               {
                  Nb c = Dot(target,apVecs[i_eq2]);
                  Nb s = c*c;
                  if (s>thr_rescue) 
                  {
                     ok_overlap[i_eq] = i_eq2;
                     overlaps[i_eq] = s;
                     used_ci[i_eq2] = i_eq; 
                     n_not_rep--;
                     Mout << " Target " << i_eq;
                     Mout << " found among vci with small overlap: " 
                          << ok_overlap[i_eq] << " s = " << overlaps[i_eq] << endl;
                     break;
                  }
                  else if (s>overlaps[i_eq]) overlaps[i_eq]= s;
               }
            }
         }
      }

      if (n_not_rep>I_0)
      {
         Mout << " Putting remaining target vectors on Ci vector list " << endl;
         // Loop targets not found and put those targets on the FIRST VCI
         // not used to represent a target state....
         for (In i_eq=I_0;i_eq<aNroots;i_eq++)
         {
            if (ok_overlap[i_eq]==-I_1) 
            {
               DataCont target; 
               string name = "TargetVector_"+std::to_string(i_eq);
               target.GetFromExistingOnDisc(aNvecSize,name);
               target.SaveUponDecon();
               for (In i_eq2=I_0;i_eq2<aNroots;i_eq2++)
               {
                  if (used_ci[i_eq2]==-I_1) 
                  {
                     used_ci[i_eq2]   = i_eq;
                     ok_overlap[i_eq] = i_eq2;
                     overlaps[i_eq] = C_1;
                     Mout << " Put target " << i_eq << " as vci start vector " 
                          << i_eq2 << endl;
                     apVecs[i_eq2] = target; 
                     break;
                  }
               }
            }
         }
         if (mpVccCalcDef->IoLevel() > I_11)
         {
            Mout << "\n\n New status:  \n" << std::endl;
            for (In i_eq = I_0; i_eq < aNroots; i_eq++)
            {
               Mout << " Target " << i_eq;
               if (ok_overlap[i_eq] == -I_1)
               {
                  Mout << " not ";
               }
               Mout << " - start vector nr  " 
                    << ok_overlap[i_eq] << "  and overlap " << overlaps[i_eq] << std::endl;
               if (ok_overlap[i_eq] == -I_1)
               {
                  n_not_rep++;
               }
            }
            for (In i_eq = I_0; i_eq < aNroots; i_eq++)
            {
               Mout << " VCI nr " << i_eq;
               if (used_ci[i_eq] == -I_1)
               {
                  Mout << " not ";
               }
               Mout << " used to represent target vector "
                    << used_ci[i_eq] << std::endl;
            }
         }
      }
   }
}

void Vcc::CompareTransformers
   (  const bool aVcc
   )
{
   Mout << "Comparing original and V3 ";
   if (aVcc)
      Mout << "VCC";
   else
      Mout << "VCI";
   Mout << " transformers..." << endl;
   
   //OpDef* fakeop = pOpDef()->GenerateFakeOper();
   //Mout << "Generated test operator:" << endl << *fakeop << endl;

   // Set up original and new transformer.
   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   if (aVcc)
   {
      trf.SetType(TRANSFORMER::VCC);
   }
   else
   {
      trf.SetType(TRANSFORMER::VCI);
   }
   midas::vcc::TransformerV3 v3(trf);

   // Initialize wavafunction parameters.
   In n_params = trf.NexciTransXvec();
   if (aVcc)
      n_params--;
   DataCont dc_inp(n_params, C_0, "InMem", "test_trf_in");
   dc_inp.Zero();
   srand((unsigned)time(0));
   Nb val = C_0;
   for (In i=I_0; i<n_params; ++i)
   {
      val = Nb((rand() % I_10) + I_1)/C_10;
      dc_inp.DataIo(IO_PUT, i, val);
   }

   // Data containers for output.
   DataCont dc_orig(n_params, C_0, "InMem", "test_trf_orig");
   DataCont dc_v3(n_params, C_0, "InMem", "test_trf_v3");
   Nb e_orig = C_0;
   Nb e_v3 = C_0;

   // Compare...
   Mout << "Calling original transformer." << endl;
   trf.Transform(dc_inp, dc_orig, I_1, I_0, e_orig);
   Mout << "Calling V3 transformer." << endl;
   v3.Transform(dc_inp, dc_v3, I_1, I_0, e_v3);

   if (aVcc)
   {
      Mout << "   Energy(orig) = " << e_orig << endl;
      Mout << "   Energy(v3)   = " << e_v3 << endl;
      Mout << "   Energy diff. = " << e_orig - e_v3 << endl;
   }
   trf.CmpDCs(dc_orig, dc_v3, aVcc);
 
   if (aVcc)
   {
      Nb thr = pow(C_10,-(C_10+C_3));
      if (sizeof(Nb) == sizeof(long double))
         thr = pow(C_10,-(C_10+C_5));

      if ((e_orig-e_v3)/e_v3 > thr)
         Mout << "ENERGY COMPARE: SOMETHING WRONG!" << endl;
      else
         Mout << "ENERGY COMPARE: EVERYTHING SEEMS OK." << endl;
   }
   Mout << endl << endl;

   //delete fakeop;
}

/**
* Wrapper function to RestartFromPrev for Rsp
* */
bool Vcc::RspRestartFromPrev(Transformer* apTrf, DataCont& arDc, const In& arIEq,
      const In& arNVecSize,  string aStorage)
{
   bool res_suc=false;
   bool include_ref=false;
   VccTransformer* vcc_trf = dynamic_cast<VccTransformer*>(apTrf);
   if (NULL == vcc_trf)
      Mout << " Not a vcc transformer." << endl;
   else
      res_suc=RestartFromPrev(vcc_trf->GetTransXvec(), arDc, arIEq, arNVecSize, aStorage, 
            "VCCRSP", include_ref);

   return res_suc;
}
/**
* Map small vec to large vec in restart.
*                
* */
bool Vcc::RestartFromPrev
   (  const Xvec& arXvec
   ,  DataCont& arDc
   ,  const In& arIEq
   ,  const In& arNVecSize
   ,  string aStorage
   ,  string aFilePrefix
   ,  const bool aInclRef
   )
{
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Entering Vcc::RestartFromPrev(...)");
   }

   In m_prev;
   vector<In> occmax_vec_prev;
   In exci_level_prev;
   In vecsize_prev;
   string calc_base_name_prev;
   string vec_name;
   // Read in the restart info file
   std::string info_file = pVscfCalcDef()->GetmVscfAnalysisDir() + "/" + aFilePrefix + "RESTART.INFO";
   if (InquireFile(info_file))
   {
      string inf_s;
      //ifstream restart_info(info_file.c_str());
      auto restart_info = midas::mpi::FileToStringStream(info_file);
   
      getline(restart_info,inf_s);
      getline(restart_info,inf_s);
      {
         istringstream line(inf_s);
         line >> m_prev;
      } 
      getline(restart_info,inf_s);
      occmax_vec_prev.reserve(m_prev);
      getline(restart_info,inf_s);
      {
         istringstream line(inf_s);
         occmax_vec_prev = midas::util::VectorFromString<In>(inf_s);
      }
      getline(restart_info,inf_s);
      getline(restart_info,inf_s);
      {
         istringstream line(inf_s); 
         line >> exci_level_prev;
      } 
      getline(restart_info,inf_s);
      getline(restart_info,inf_s);
      {
         istringstream line(inf_s); 
         line >> vecsize_prev;
      } 
      getline(restart_info,inf_s);
      getline(restart_info,calc_base_name_prev);
      vec_name = calc_base_name_prev + std::to_string(arIEq);

      midas::mpi::Barrier();
      
      //restart_info.close();
   }
   else
   {
      Mout << " *RESTART.INFO file is not available!" << endl;
      midas::mpi::Barrier();
      return false;
   }

   if(gDebug)
   {
      cout << "Rank " << midas::mpi::GlobalRank() << " has m_prev = " << m_prev << endl;
   }

   // Printing data in the restart info file
   bool check_prev=true;
   if (check_prev)
   {
      Mout << " Summary of the previous correlated calculation:\n" << endl;
      Mout << " Nr. of modes         : " << m_prev << endl;
      Mout << " Nr. of modals/mode   : ";
      for (In i=I_0;i<m_prev;i++) Mout << occmax_vec_prev[i] << " ";
      Mout << "\n   Excitation    level: " << exci_level_prev << endl;
      Mout << " Vector size          : " << vecsize_prev << endl;
      Mout << " Vector name          : " << calc_base_name_prev << endl;
      //Mout << "-------------\n Current name base    : " <<  mpVccCalcDef->Name()  << endl; //DEP
      //Mout << " Current vector size  : " <<  arNVecSize            << endl;
   }

   // Compare the modal bases: number of modals/mode
   In ndim = mpVccCalcDef->GetNmodesInOcc(); //DEP
   if (ndim != m_prev)
   {
      MIDASERROR("Restart failed. Number of modes is different in the previous calculation.");
   }

   string vec_file_name= vec_name + "_0";
   if (!InquireFile(vec_file_name))
   {
      Mout << vec_file_name << "_0 file is not available!" << endl;
      return false;
   }

   vector<In> occ_vec(ndim);      // The reference full occupation vector = 0
   for (In i=0;i<ndim;i++) occ_vec[i] =0;
   vector<In> occmax_vec(ndim);      // The maximum full occupation vector.
   vector<In> max_occmax_vecs(ndim);      // The maximum full occupation vector.
   //if (mpVccCalcDef->UsePrimitiveBasis())
   //{
   //   for (In i_op_mode=0;i_op_mode<ndim;i_op_mode++)
   //   {
   //      In i_g_mode   = mpOpDef->GlobalModeNr(i_op_mode);
   //      In i_bas_mode = mpBasDef->LocalModeNr(i_g_mode);
   //      In nbas       = mpBasDef->Nbas(i_bas_mode);
   //      occmax_vec[i_op_mode]   = nbas - I_1;
   //   }
   //}
   //else
   //{
   for (In i_op_mode=0;i_op_mode<ndim;i_op_mode++)
      occmax_vec[i_op_mode]   = mpVccCalcDef->Nmodals(i_op_mode) - I_1; //DEP
   //}

   bool same_modal_basis=true;
   for (In i=I_0;i<m_prev;i++)
   {
      occmax_vec_prev[i]=occmax_vec_prev[i]-I_1;
      if (occmax_vec[i]!=occmax_vec_prev[i]) same_modal_basis=false;
      max_occmax_vecs[i]=max(occmax_vec[i],occmax_vec_prev[i]);
   }
   //  // Compare the mode coupling levels
   //  In nexcilevel = mpVccCalcDef->MaxExciLevel();
   //  In max_common_exci_level=min(nexcilevel,exci_level_prev);

   bool res_suc = false;

   // In this case, the numbers of modals per mode are the same.
   if (same_modal_basis)
   {
      // In this case, excitation level must be gt the one in the previous calc.
      // The vector name must be different, too: ..._EXC_N_...
      if (arNVecSize>vecsize_prev)
      {
         arDc.ChangeStorageTo(aStorage,false,false,true);
         Mout << " I will use what is available - rest is put to zero." << endl; 
         DataCont tmpdc;
         tmpdc.GetFromExistingOnDisc(vecsize_prev,vec_name);
         MidasVector tmpvec(vecsize_prev);
         tmpdc.DataIo(IO_GET,tmpvec,vecsize_prev);
         arDc.SetNewSize(arNVecSize);
         arDc.Zero();
         arDc.DataIo(IO_PUT,tmpvec,vecsize_prev,I_0);
         res_suc = true;
      }
      // In this case, excitation level must be lt the one in the previous calc.
      // The vector name must be different, too: ..._EXC_N_...
      else if (arNVecSize<vecsize_prev)
      {
         arDc.ChangeStorageTo("OnDisc");
         Mout << " I will truncate the vector available on disc." << endl; 
         string label=arDc.Label(); 
         arDc.NewLabel(vec_name);
         arDc.SetNewSize(arNVecSize);
         arDc.NewLabel(label);
         res_suc = true;
         arDc.ChangeStorageTo(aStorage,false,false,true);
      }
      // In this case, excitation level is the same as previously.
      else
      {
         arDc.ChangeStorageTo("OnDisc");
         arDc.SetNewSize(arNVecSize);
         bool robust = true;
         bool check = true;
         Mout << " I will just check the vector available on disc." << endl; 
         res_suc = arDc.ChangeStorageTo("OnDisc",robust,check); // should be true on success
         arDc.ChangeStorageTo(aStorage,false,false,true);
      }
   }
   else
   {
      DataCont tmpdc;
      tmpdc.GetFromExistingOnDisc(vecsize_prev,vec_name);
      MidasVector tmpvec(vecsize_prev);
      tmpdc.DataIo(IO_GET,tmpvec,vecsize_prev);

      if (vec_name==arDc.Label())
      {
         tmpdc.Delete();
         arDc.NewLabel(vec_name);
      }
      arDc.ChangeStorageTo(aStorage);
      arDc.SetNewSize(arNVecSize);

      In idx_prev=I_0;
      In idx_curr=I_0;

      string lowhig = "LOWHIG";
      bool exci_only=true;
      In i_mode_combi_p = -1;

      const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
      //const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
      //In n_mode_combi_p = mcr.Size();
      //Mout << " Total number of modecombi's: n_mode_combi_p= " << n_mode_combi_p << endl ;

      for(auto ip = mcr.begin(), end = mcr.end(); ip != end; ++ip)
      {
         i_mode_combi_p++;

         // could be replaced by if (ip==mcr.begin()...
         if (i_mode_combi_p==I_0 && !aInclRef) continue;

         //Mout << "---------------\n i_mode_combi_p: " << i_mode_combi_p << endl ;
         const ModeCombi& mp = *ip; // mTransXvec.GetModeCombi(i_mode_combi_p);
         //Mout << " mp: " << mp << endl ;
         ////In n_exci_mp = arXvec.NexciForModeCombi(i_mode_combi_p);
         ////Mout << " n_exci_mp: " << n_exci_mp << endl ;
         In ndim0 = mp.Size();
         //Mout << " ndim0: " << ndim0 << endl ;
         vector<In> occ0(ndim0);
         vector<In> occmax0(ndim0);
         for (In i=0;i<ndim0;i++)
         {
            occ0[i] = I_0;
            In i_op_mode =  mp.Mode(i);
            //In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
            //occmax0[i]   = nmod-I_1;
            if (ndim0<exci_level_prev) occmax0[i]   = max_occmax_vecs[i_op_mode];
            else occmax0[i]   = occmax_vec[i_op_mode];
         }
         MultiIndex mi0(occ0,occmax0,lowhig,exci_only); //MULTIINDEX SHOULD BE AVAIL
         In n0 = mi0.Size();
         //Mout << " Size of the MultiIndex: " << n0 << endl ;

         // Get the vector to transfo
         MidasVector civec_mp(n0);

         if (ndim0>exci_level_prev)
         {
            civec_mp.Zero();
            if (aInclRef) arDc.DataIo(IO_PUT,civec_mp,n0,mp.Address());
            else arDc.DataIo(IO_PUT,civec_mp,n0,mp.Address()-I_1);
            continue;
         }

         vector<In> tmpi(occ0); // Index vector for excitations

         // Outer loop through all the mode combinations
         for (In i_exci=I_0;i_exci<n0;i_exci++)
         {
            // Get the i_exci-th occupation vector tmpi
            mi0.IvecForIn(tmpi,i_exci);

            bool in_prev=true;
            bool in_curr=true;
            for (In k=I_0;k<ndim0;k++)
            {
               In i_op_mode =  mp.Mode(k);
               if (tmpi[k]>occmax_vec_prev[i_op_mode]) in_prev=false;
               if (tmpi[k]>occmax_vec[i_op_mode]) in_curr=false;
            }

            // If ==, both should be true
            if (in_prev==in_curr)
            {
               //Mout << "idx_prev: " << idx_prev << "idx_curr: " << idx_curr << endl;
               civec_mp[idx_curr]=tmpvec[idx_prev];
               //Mout << " civec_mp[idx_curr]: " <<  civec_mp[idx_curr] << endl ;
               idx_prev++;
               idx_curr++;
            }
            // If in_prev==true, => in_curr=false 
            else if (in_prev==true)
            {
               //Mout << "idx_prev: " << idx_prev << endl;
               idx_prev++;
            }
            // => in_prev=false in_curr=true 
            else
            {
               //Mout << "idx_curr: " << idx_curr << endl;
               civec_mp[idx_curr]=C_0;
               //Mout << " civec_mp[idx_curr]: " <<  civec_mp[idx_curr] << endl ;
               idx_curr++;
            }
            //Mout << " tmpi: " << tmpi << endl ;
         }
         //Mout << "civec_mp " << civec_mp << endl; 
         civec_mp.SetNewSizeWithinCapacity(idx_curr);
         //Mout << "civec_mp " << civec_mp << endl; 
         //Mout << "civec_mp.size " << civec_mp.Size() << endl;

         if (aInclRef) arDc.DataIo(IO_PUT,civec_mp,idx_curr,mp.Address());
         else arDc.DataIo(IO_PUT,civec_mp,idx_curr,mp.Address()-I_1);

         idx_curr=I_0;
      }
      Mout << " Mapping the previous vector has been finished successfully."  << endl;
      res_suc = true;
   }
   if (res_suc) return true;
   else return false;
}

/**
 * Restart tensor VCC calculation
 * @param    arTrials            The trial vector that the data is read into
 * @param    arNlSolver          The TensorNlSolver
 * @param    arFilePrefix        The file prefix for the *RESTART.INFO file
 **/
bool Vcc::RestartFromPrev
   (  TensorDataCont& arTrials
   ,  TensorNlSolver& arNlSolver
   ,  const std::string& arFilePrefix
   )  const
{
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Entering Vcc::RestartFromPrev()");
   }

   Mout << "Trying restart..." << std::endl;
   
   // Declare data read from TENSORVCCRESTART.INFO
   In n_modes_prev;
   std::vector<In> basis_size_prev;
   In exci_level_prev;
   In vec_size_prev;
   std::string calc_base_name_prev;
   std::string calc_base_name_novec_prev;
   std::string vec_name;
   std::string decomp_thresh_name_prev;
   std::string relnorms_name_prev;

   // Declare info file name
   const std::string info_file_name = pVscfCalcDef()->GetmVscfAnalysisDir() + "/" + arFilePrefix + "RESTART.INFO";

   // Read data from info file
   if(InquireFile(info_file_name))
   {
      //std::ifstream info_file(info_file_name);
      auto info_file = midas::mpi::FileToStringStream(info_file_name);

      std::string tmp_s;

      std::getline(info_file, tmp_s);
      std::getline(info_file, tmp_s);
      n_modes_prev = midas::util::FromString<In>(tmp_s);

      std::getline(info_file, tmp_s);
      std::getline(info_file, tmp_s);
      basis_size_prev = midas::util::VectorFromString<In>(tmp_s);

      std::getline(info_file, tmp_s);
      std::getline(info_file, tmp_s);
      exci_level_prev = midas::util::FromString<In>(tmp_s);

      std::getline(info_file, tmp_s);
      std::getline(info_file, tmp_s);
      vec_size_prev = midas::util::FromString<In>(tmp_s);

      std::getline(info_file, tmp_s);
      std::getline(info_file, calc_base_name_prev);

      vec_name = calc_base_name_prev + "tensor_0";

      std::getline(info_file, tmp_s);
      std::getline(info_file, calc_base_name_novec_prev);

      std::getline(info_file, tmp_s);
      std::getline(info_file, decomp_thresh_name_prev);

      std::getline(info_file, tmp_s);
      std::getline(info_file, relnorms_name_prev);

      //info_file.close();
   }
   else
   {
      MidasWarning(info_file_name + " not found!");
      return false;
   }

   // Set base name in TensorNlSolver
   arNlSolver.SetRestartName(calc_base_name_novec_prev);

   // Print data from info file
   Mout << " Summary of the previous correlated calculation:\n" << std::endl;
   Mout << " Nr. of modes         : " << n_modes_prev << std::endl;
   Mout << " Nr. of modals/mode   : ";
   for(const auto& modals : basis_size_prev) Mout << modals << " ";
   Mout << "\n   Excitation    level: " << exci_level_prev << std::endl;
   Mout << " Vector size          : " << vec_size_prev << std::endl;
   Mout << " Tensor vector name   : " << vec_name << std::endl;
   Mout << " Decomp threshold name: " << decomp_thresh_name_prev << std::endl;

   // Perform checks
   const In n_modes_new = this->mpVccCalcDef->GetNmodesInOcc();
   if(n_modes_new!=n_modes_prev) MIDASERROR("RESTART FAILED! Number of modes is different in the previous calculation.");

   //const std::string vec_file_name = vec_name + "_0";    // WARNING: Only one file pr. vector!
   const std::string vec_file_name = vec_name;
   if(!InquireDataContFile(vec_file_name))
   {
      MidasWarning(vec_file_name + " not found!");
      return false;
   }

   std::vector<In> basis_size_new(n_modes_new);
   this->mpVccCalcDef->Nmodals(basis_size_new);
   bool same_modal_basis = true;
   for(In i=I_0; i<n_modes_new; ++i) same_modal_basis = (same_modal_basis && (basis_size_prev.at(i) == basis_size_new.at(i)));

   const In exci_level_new = this->mpVccCalcDef->MaxExciLevel();

   // Read in data
   bool restart_success = false;
   if (  same_modal_basis  )
   {
      TensorDataCont tmp_tdc;
      tmp_tdc.ReadFromDisc(vec_file_name);

      auto restart_size = std::min(arTrials.Size(),tmp_tdc.Size());

      // This should work in all cases regardless of exci_range_new and exci_range_prev
      for(In imc=I_0; imc<restart_size; ++imc)
      {
         arTrials.GetModeCombiData(imc) = tmp_tdc.GetModeCombiData(imc);
      }
      restart_success = true;
   }
   else
   {
      MIDASERROR("Restart with different modal-basis size is not implemented for TensorNlSolver!");
   }

   // Read in tensor-decomp. thresholds and parameters
   if (  !this->mpVccCalcDef->GetDecompInfoSet().empty()
      && this->mpVccCalcDef->AdaptiveDecompThreshold()
      )
   {
      // Read amplitude decomposition threshold
      std::ifstream decomp_thresh_input(decomp_thresh_name_prev);
      //auto decomp_thresh_input = midas::mpi::FileToStringStream(decomp_thresh_name_prev);
      Nb amp_decomp_thresh = C_0;
      if (  decomp_thresh_input.good()
         )
      {
         std::string thresh_string;
         std::getline(decomp_thresh_input, thresh_string);
         amp_decomp_thresh = midas::util::FromString<Nb>(thresh_string);

         Mout << "Read dynamic decomposition threshold for first iteration of restart calculation = " << amp_decomp_thresh << std::endl;
      }
      else
      {
         amp_decomp_thresh = C_I_10_6;
         MidasWarning("No file with CP decomposition threshold found. I will set the starting threshold to the default value of " + std::to_string(amp_decomp_thresh) + ", but this will most likely affect convergence!");
      }
      //decomp_thresh_input.close();

      // Read relative error-vector norms
      auto vecsize = arTrials.Size();
      std::vector<Nb> relnorms(vecsize);
      std::ifstream relnorms_input(relnorms_name_prev, ios::in | ios::binary);
      //auto relnorms_input = midas::mpi::FileToStringStream(relnorms_name_prev, ios::in | ios::binary);
      if (  relnorms_input.good()
         )
      {
         // Use function from util/read_write_binary.h
         read_binary_array(&relnorms[0], vecsize, relnorms_input);

         // Test input
         auto minmax = std::minmax_element(relnorms.begin(), relnorms.end());
         auto ave = std::accumulate(relnorms.begin(), relnorms.end(), C_0) / static_cast<Nb>(vecsize);
         Mout  << " Analyze relative error-vector norms for restart:" << "\n"
               << "   Min: " << *minmax.first << "\n"
               << "   Max: " << *minmax.second << "\n"
               << "   Ave: " << ave << "\n"
               << std::flush;
      }
      else
      {
         // Set relnorms to an empty vector such that TensorNlSolver::Solve automatically generates the default values
         MidasWarning("Could not find file containing relative error-vector norms for TensorNlSolver restart. I will use the inverse-Jacobian norms instead: 1/[A_0^-1].");
         relnorms.clear();
      }
      //relnorms_input.close();
      

      // Set threshold and norms in solver if we are restarting
      if (  restart_success
         )
      {
         arNlSolver.SetFirstDecompThreshold(amp_decomp_thresh);
         arNlSolver.SetRelativeErrorVectorNorms(relnorms);
      }
   }

   return restart_success;
}


/**
* Wrapper function to WriteRestartInfo for Rsp
*                
* */
void Vcc::WriteRspRestartInfo(Transformer* apTrf, const In& arNVecSize)
{
   VccTransformer* vcc_trf = dynamic_cast<VccTransformer*>(apTrf);
   if (NULL == vcc_trf)
   {
      Mout << " Not a vcc transformer." << endl;
   }
   else
   {
      WriteRestartInfo(arNVecSize, pVscfCalcDef()->GetmVscfAnalysisDir() + "/VCCRSP", "_rsp_eigvec_");
   }
}
/**   
* Write VCCRESTART.INFO, TENSORVCCRESTART.INFO, or VCCRSPRESTART.INFO files for a later, restarted calculation.
* */  
void Vcc::WriteRestartInfo(const In& arNVecSize, string aFilePrefix, string aVecName)
{   
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Entering Vcc::WriteToRestartInfo()");
   }
   
   if(!midas::mpi::IsMaster())
   {
      if constexpr (MPI_DEBUG)
      {
         midas::mpi::WriteToLog("Leaving Vcc::WriteToRestartInfo()");
      }
      return;
   }
   
   std::string info_file= aFilePrefix + "RESTART.INFO";

   if (InquireFile(info_file))
   { 
      system_command_t cmd = {"cp", info_file, info_file + ".OLD"};
      MIDASSYSTEM(cmd);  //copy file as *.OLD
   }

   // Write the restart info file
   midas::mpi::OFileStream file(info_file, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios_base::out);

   file << "Number of modes:" << endl; 
   In ndim=mpVccCalcDef->GetNmodesInOcc();
   file << ndim << endl;
   file << "Number of modals per mode:" << endl; 
   for (In i=I_0;i<ndim;i++) file << mpVccCalcDef->Nmodals(i) << " ";
   file << endl;
   file << "Mode coupling level in VCC:" << endl; 
   file << mpVccCalcDef->MaxExciLevel() << endl;
   file << "Size of vectors in VCC:" << endl; 
   file << arNVecSize << endl;
   file << "Vcc base name:" << endl; 
   string vec_name = mpVccCalcDef->GetName() + aVecName;
   file <<  vec_name  << endl;
   file << "Vcc base name (no vec name):" << endl;
   file << mpVccCalcDef->GetName() << endl;
   file << "Vcc decomp threshold name:" << std::endl;
   const std::string thresh_name = mpVccCalcDef->GetName() + "_Vcc_decomp_threshold";
   file << thresh_name << std::endl;
   file << "Vcc relative error-vector norms name:" << std::endl;
   const std::string relnorms_name = mpVccCalcDef->GetName() + "_Vcc_relative_errvec_norms";
   file << relnorms_name << std::endl;
   
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Leaving Vcc::WriteToRestartInfo()");
   }
}
/*
* Do Vapt calculation
* */
void Vcc::DoVapt()
{
   In n_order = mpVccCalcDef->VaptMaxOrder();
   Mout << " In Vcc::DoVapt(): Maximum wave function order requested is: " << n_order << endl;
   
   In is_order = n_order;
   if (mpVccCalcDef->Iso())
      is_order = mpVccCalcDef->IsOrder();

   // Make sure we don't setup transformer for out-of-space calculations.
   bool oos_save = mpVccCalcDef->VccOutOfSpace();
   mpVccCalcDef->SetVccOutOfSpace(false);
   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0, is_order);
   trf.SetType(TRANSFORMER::VCI);
   mpVccCalcDef->SetVccOutOfSpace(oos_save);
   
   MidasVector en_vapts(n_order+2,C_0);
   en_vapts[I_0]    = mEvmp0; // e0=e0,RSPT

   // Allocate space for the perturbation vectors.
   DataCont* pert_vect        = new DataCont [n_order+I_1]; // Phi,   from 0 to n_order
   DataCont* sigma            = new DataCont [n_order+I_1]; // sigma, from 0 to n_order
   DataCont* eigenstates_corr = new DataCont [n_order+I_1]; // E,     from 0 to n_order

   //for (In i_op_mode=I_0;i_op_mode<pOpDef()->NmodesInOp();i_op_mode++)
   //   Mout << GetOccEigVal(i_op_mode);
   //Mout << "  ****" << endl;

   // Prepare info for the datacontainers.
   string vaptvec_storage = "OnDisc";                     
   if (mpVccCalcDef->VecStorage()<I_1)
      vaptvec_storage = "InMem";
   // NB Size must be consistent with the mXvec size. 
   In vaptvec_size = trf.NexciXvec();       
   Mout << endl
        << " The size of the Vapt vectors is:                  " << vaptvec_size  << endl
        << " The Vapt vectors are kept:                        " << vaptvec_storage << endl
        << " In the transformation the vector storage is:     " << trf.GetXvecStorage() << endl;
   
   for (In i_order=I_0;i_order<=n_order;i_order++)
   {
      string name = mpVccCalcDef->GetName()+"_Vapt" + std::to_string(i_order);
      pert_vect[i_order].NewLabel(name,false);
      pert_vect[i_order].ChangeStorageTo(vaptvec_storage);
      name = mpVccCalcDef->GetName()+"_VaptEig" + std::to_string(i_order);
      eigenstates_corr[i_order].NewLabel(name,false);
      eigenstates_corr[i_order].ChangeStorageTo(vaptvec_storage);
   }

   // Unlike RSPT all quantities are "bar-quantities"
   // i.e. accumulated corrections and accumulated sigma
   // Nb sigma=H*Phi
   In i_type=I_0; // get H0
   In i_dummy=I_0;
   Nb nb_dummy=C_0;
   Nb cm1 = -C_1;
   Nb c1 = C_1;
   DataCont unit; 
   DataCont auxiliar; 
   DataCont Hdiag; 
   unit.SetNewSize(vaptvec_size);
   auxiliar.SetNewSize(vaptvec_size);
   Hdiag.SetNewSize(vaptvec_size);
   unit.PutToNumber(cm1); //construct unit vector
   // Set start vector to correct name. 
   if (n_order >= I_0) 
   {
      eigenstates_corr[I_0].SetNewSize(vaptvec_size);
      pert_vect[I_0].SetNewSize(vaptvec_size);
      pert_vect[I_0].Zero();
      pert_vect[I_0].DataIo(IO_PUT,I_0,c1); // Phi0, zero-order is the same than for RSPT

      sigma[I_0].SetNewSize(vaptvec_size);
      sigma[I_0].Zero();
      i_type=I_1; // get H to transform
      trf.Transform(pert_vect[I_0], sigma[I_0], i_type, i_dummy, nb_dummy); //sigma0=H*Phi0
      //< notice sigma0 is actually the column of H we are interested in

      mpVccCalcDef->SetTrueHDiag(true); // set transform to be Diag{H} instead of H
      i_type=I_0;
      i_dummy=I_1;
      trf.Transform(unit, Hdiag, i_type, i_dummy, nb_dummy); // gets the diag of H

      mpVccCalcDef->SetTrueHDiag(false); // set the matrix to transform to be full H/H0 again
      i_type=I_0;  //
      i_dummy=I_1; // set H0 to transform
      trf.Transform(unit, eigenstates_corr[I_0], i_type, i_dummy, nb_dummy); // Gets the diag of H
      eigenstates_corr[I_0].DataIo(IO_PUT,I_0,c1); // set first to one: avoids INF & makes the
                                                   // energy available as 0th element of sigma
   }
   Mout << "vaptvec_size: " << vaptvec_size << endl;
   Mout << " en_vapts[" << I_0 << "]   = " << en_vapts[I_0] << endl;

   Timer time_it;
   for (In i_order=I_1;i_order<=n_order;i_order++)
   {
      // 1) calculate the wavefunction (E-e)^{-1}(E*Phi-sigma)
      pert_vect[i_order]=pert_vect[i_order-I_1]; // assign new Phi
      pert_vect[i_order].DirProd(eigenstates_corr[i_order-I_1],C_0,I_1); // E*Phi
      pert_vect[i_order].Axpy(sigma[i_order-I_1],-C_1); // E*Phi-sigma
      auxiliar=eigenstates_corr[i_order-I_1];    //consider for efficiency avoid auxiliar, and use eigen
      auxiliar.Shift(-en_vapts[i_order-I_1]); // E-e
      pert_vect[i_order].DirProd(auxiliar,C_0,-I_1);   // (E-e)^{-1}(E*Phi-sigma)
      pert_vect[i_order].DataIo(IO_PUT,I_0,c1); // Set first to unit to avoid INF

      // 2) Make the sigma = H*Phi transformation
      i_type=I_1; // get H to transform
      trf.Transform(pert_vect[i_order], sigma[i_order], i_type, i_dummy, nb_dummy);

      // 3) Get the energy e = <Phi0|sigma>
      Nb e_vapt;
      sigma[i_order].DataIo(IO_GET,I_0,e_vapt);
      en_vapts[i_order] = e_vapt;

      // 4) Update other eigenstates corrections, E
      eigenstates_corr[i_order]=pert_vect[i_order]; // E<-Phi
      eigenstates_corr[i_order].DirProd(sigma[I_0],C_0,I_1); //sigma0*Phi=H_s*Phi_s
      eigenstates_corr[i_order].Axpy(Hdiag,-C_1); // -H_ii + H_s*Phi_s
      eigenstates_corr[i_order].Scale(-C_1,I_0,-I_1); // E=H_ii-H_s*Phi_s

      //Mout << " en_vapts[" << i_order << "]   = " << en_vapts[i_order] << endl;
      if (mpVccCalcDef->TimeIt()) 
      {
         string s_time = " CPU  time used in VAPT iteration " + std::to_string(i_order) + ":";
         time_it.CpuOut(Mout,s_time);
      }
   }

   // Patch to put the last eigenvalue vector into the summary part
   mpVccCalcDef->SetDone(en_vapts[n_order]);

   Mout << endl << " Summary of energies in various orders, the n energies are:" << endl;
   Nb e_corr = C_0;
   Nb conv = C_AUTKAYS;
   In sznb = sizeof(Nb);
   In nprec = sznb*I_2;
   //In nspace = nprec+I_4; 
   In nspace2 = Mout.precision()+I_7;
   for (In n=I_0;n<=n_order;n++)
   {
      if (n == I_0) e_corr=en_vapts[n];
      else
         e_corr = en_vapts[n]-en_vapts[n-I_1];
      Mout << "O_" 
           << setw(5)       << left  << n 
           << setw(nspace2) << right << e_corr
           << " " 
           << setw(nspace2) << right << en_vapts[n]
           << " " 
           << setw(nspace2) << right << e_corr*conv
           << " " 
           << setw(nspace2) << right << en_vapts[n]*conv << endl;
   }

   string name = pVscfCalcDef()->GetmVscfAnalysisDir() + "/" + mpVccCalcDef->GetName() + "_vapt_res";
   midas::mpi::OFileStream mpout(name);
   
   mpout.setf(ios::scientific);
   mpout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(nprec, mpout);
   mpout.setf(ios::showpoint);

   Mout << endl << " Summary of wave function norms in various orders (accumulated value):" << endl;
   for (In n=I_0;n<=n_order;n++)
   {
      Nb normn = pert_vect[n].Norm();
      Nb normn1;
      if ( n > I_0 ) normn1 = pert_vect[n-1].Norm();
      else
         normn1 = C_0;
      Mout << "O_" 
         << setw(5)  << left << n 
         << setw(25) << right << normn-normn1
         << " " 
         << setw(25) << right << normn << endl;
   }
   if (n_order >I_0)
   {
      DataCont pert_vect_sum(pert_vect[n_order]);
      name = mpVccCalcDef->GetName()+"_Vapt_sum_wf";
      pert_vect_sum.NewLabel(name);
      for (In n=n_order-I_1;n>=I_0;n--)
      {
         pert_vect_sum.Axpy(pert_vect[n],C_1);
      }
      pert_vect_sum.SaveUponDecon(true);
      Mout << endl << " Norm of the sum over perturbed wave functions: "
           << pert_vect_sum.Norm() << endl;
   }

   delete[] pert_vect;
   delete[] eigenstates_corr;
   delete[] sigma;

}


/**
 *
 **/
void Vcc::DoSubspaceSolverCalc
   (
   )
{
   const auto& cd = *this->pVccCalcDef();
   auto n_modals_in = cd.GetNModals();
   std::vector<Uin> n_modals(n_modals_in.begin(), n_modals_in.end());
   auto n_active_modals = (cd.NActiveModals().size() > 0) ? cd.NActiveModals() : n_modals;
   ModeCombiOpRange mcr(cd.MaxExciLevel(), n_modals.size());

   // Modal integrals to use.
   // As a fallback use the ones from this object, unless we can find some that
   // have been dumped using VscfCalcDef::DumpModalsForMatRep().
   ModalIntegrals<Nb>* p_integrals = pIntegrals();
   std::unique_ptr<ModalIntegrals<Nb>> p_dumped_ints(nullptr);
   const std::string& vscf_ref = this->pVccCalcDef()->VscfReference();
   const std::string dump_file_label = VscfCalcDef::DumpModalsForMatRepFileLabel(vscf_ref);
   if (midas::mpi::filesystem::IsFileOnAll(dump_file_label+"_0"))
   {
      if (this->pVccCalcDef()->IoLevel() > 5)
      {
         Mout << "Vcc::DoSubspaceSolver; Reading dumped modals from DataCont, label = "
            << dump_file_label << std::endl;
      }
      const auto& n_modals_int = this->pVccCalcDef()->GetNModals();
      p_dumped_ints = std::make_unique<ModalIntegrals<Nb>>(midas::tdvcc::GetModalIntegralsReal
         (  *this->pOpDef()
         ,  *this->pBasDef()
         ,  std::vector<Uin>(n_modals_int.begin(), n_modals_int.end())
         ,  dump_file_label
         ));
      if (p_dumped_ints)
      {
         p_integrals = p_dumped_ints.get();
      }
   }

   // Temporarily change the name to only depend on oper, basis, vscf.
   // MatRep solver's OperMat can then reuse integrals between calcs.
   // It'll check 2 elements for consistency so there _is_ a safe-guard even in
   // case of non-intentional name clashes.
   const std::string old_integrals_name = p_integrals->Name();
   std::stringstream ss_ints_name;
   ss_ints_name
      << this->pOpDef()->Name() << "_"
      << this->pBasDef()->GetmBasName() << "_"
      << this->pVccCalcDef()->VscfReference()
      ;
   p_integrals->SetName(ss_ints_name.str());

   // Get solver.
   auto p_solver = midas::vcc::subspacesolver::VibCorrSubspaceSolverFactory<Nb>
      (  cd.SubspaceSolverCorrType()
      ,  cd.SubspaceSolverTrfType()
      ,  n_active_modals
      ,  n_modals
      ,  *pOpDef()
      ,  *p_integrals
      ,  mcr
      );

   if (p_solver)
   {
      midas::vcc::subspacesolver::SetGeneralSettings(*p_solver, cd, &this->GetEigVal(), &this->GetOccModalOffSet());
      Mout << "Solver settings:" << std::endl;
      p_solver->PrintSettings(Mout, 3);
      p_solver->Solve();
      Mout << std::endl;

      std::stringstream ss_conv;
      ss_conv
         << "Non-linear " << midas::tdvcc::StringFromEnum(cd.SubspaceSolverCorrType()) << " equations "
         << (p_solver->Conv()? "converged in": "not converged after")
         << " " << p_solver->NIter() << " iterations!"
         ;
      if (!p_solver->Conv())
      {
         MidasWarning(ss_conv.str() + " (" + p_solver->Name() + ")");
      }
      else
      {
         Mout << ss_conv.str();
      }
      Mout << std::endl;

      Mout << "\nSolver convergence info:" << std::endl;
      p_solver->PrintConvInfo(Mout, 3);
      p_solver->WriteSolVecsToFile(Mout, this->Name(), 3);
      Mout << std::endl;

      if (this->pVccCalcDef()->MatRepFvciAnalysis())
      {
         mSavedFvciVecKet = p_solver->FvciVec(false);
         mSavedFvciVecBra = p_solver->FvciVec(true);
      }

      // Set energy.
      this->pVccCalcDef()->SetDone(p_solver->ExpVal());
   }
   else
   {
      MIDASERROR("p_solver = nullptr, unexpectedly. (CorrType = '"+midas::tdvcc::StringFromEnum(cd.SubspaceSolverCorrType())+"')");
   }

   // Reset pIntegrals name.
   pIntegrals()->SetName(old_integrals_name);
}

/**
 * Output data about the largest components in the VCC solution vector.
 *
 * @param aVec
 *    The solution vector
 * @param apTrf
 *    Pointer to the VCC transformer
 * @param aNvecsize
 *    Size of the solution vector (without reference configuration)
 **/
void Vcc::VccSolutionVectorAnalysis(DataCont& aVec, VccTransformer* apTrf, const In& aNvecsize)
{
   In n_out = max(I_5,mpVccCalcDef->IoLevel());
   n_out = min(n_out,aNvecsize);
   Mout << endl << endl
        << " Analysis of solution vector - the "
        << n_out << " largest elements of vector." << endl
        << " The first address set refers to modes and modals excited! " << endl
        << " NB: modals space ordering with occupied (no matter which) as zero." << endl
        << " The second is converted to primitive assuming 1-1 correspondence with modals"
        << endl
        << " (ok for low correlated systems)." << endl << endl;

   vector<Nb> largest_coef(n_out);
   Xvec xvec = apTrf->GetTransXvec();
   xvec.ConvertToDataCont();
   vector<In> add_largest_coef(n_out);
   aVec.AddressOfExtrema(add_largest_coef,largest_coef,n_out,I_2);
   Mout << "  ";
   for (In j=-I_1;j<n_out;j++)
   {
      Mout << setw(23) << left;
      if (j!=-I_1) 
      {
         Mout << largest_coef[j];
         if (largest_coef[j]<C_0) Mout << " ";
      } 
      else 
      {
         Mout << C_1;
      }
      Mout << "* Psi_" <<  setw(9) << left;
      if (j!=-I_1) Mout << add_largest_coef[j];
      else Mout << "Ref";
      if (j!=-I_1)ModesAndLevelsOut(Mout,add_largest_coef[j]+I_1,xvec);
      else ModesAndLevelsOut(Mout,I_0,xvec);
      Mout << endl;
      if (j<n_out-I_1 && largest_coef[j+I_1]>=C_0) Mout << " +";
      if (j<n_out-I_1 && largest_coef[j+I_1]<C_0) Mout << " ";
   }
   if(mpVccCalcDef->GetMolMCs().size() != I_0)
      xvec.PrintWeightsInMCs(mpVccCalcDef->GetMolMCs(), aVec,I_1);
   Mout << std::endl;
}

/**
 * * Perform density matrix analysis 
 * * */
/*
void Vcc::DensityMatrixAnalysis()
{
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Vcc Density matrix calculation and analysis ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');

   // GS density analysis 
   vector<MidasMatrix> one_d_densities_vscf_bas(mpOneModeInt->NmodesInInt());  // hold densities 
   use_prim=false; 
   CalcGsDensityMatrixVscfBasis(one_d_densities_vscf_bas,); // calculate Densities 
   NaturalModalAnalysis(one_d_densities_vscf_bas); // Analyse Densities 
}
 */ 


void Vcc::PrintWeightInfo(const vector<vector<In> >& ar1,DataCont& ar2, In ar3)
{
   mRspTrf->ConvertTransXvecToDataCont();
   mRspTrf->GetTransXvec().PrintWeightsInMCs(ar1, ar2, ar3);
}

/**
 * Return a TensorDataCont of the correct shape for the given calculation.
 *
 * @param aAddRef       Include reference MC.
 *
 * @return
 *    TensorDataCont
 **/
TensorDataCont Vcc::ConstructTensorDataCont
   (  bool aAddRef
   )  const
{
   // Construct VccStateSpace
   std::vector<In> nmodals;
   this->pVccCalcDef()->Nmodals(nmodals);
   VccStateSpace vss(mRspTrf->GetXvecModeCombiOpRange(), nmodals, !aAddRef); // exclude ref?

   // Get tensor type ID
   auto type_id   =  this->pVccCalcDef()->GetVccRspDecompInfo().empty()
                  ?  BaseTensor<Nb>::typeID::SIMPLE
                  :  BaseTensor<Nb>::typeID::CANONICAL;


   return TensorDataCont(vss, type_id);
}
 
void Vcc::TestLA(In aCalc)
   //Test for the left transformation with the Jacobian 
{
   Mout << "In Vcc::TestLA()" << endl;

   //which block to print?
   In rblock = I_1;
   In cblock = I_1;


   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   trf.SetType(TRANSFORMER::LVCCJAC);

   In vecsize = trf.NexciTransXvec() - I_1;
   In sin_size = trf.GetTransXvec().SinSize();
   In doub_size = trf.GetTransXvec().DoubSize();
   if (sin_size + doub_size != vecsize) MIDASERROR("TestLA: something wrong with vecsize");
   MidasVector lvec(vecsize +I_1, C_0);
   //MidasMatrix diff(vecsize,vecsize,C_0);
   Nb norm= C_0;
   // CalculateEtaVec()
   DataCont eta_dc(lvec);
   Mout << "eta_dc.Size() " << eta_dc.Size() << endl;
   trf.CalcVccRspEta0(eta_dc);
   Mout << "eta_dc: " << eta_dc << endl;
   string eta_name = mpVccCalcDef->GetName() + "_Vcc_eta_vec";
   //DataCont eta_num_dc;
   //In n_size= eta_dc.Size(); 
   //eta_num_dc.GetFromExistingOnDisc(n_size,eta_name);
   //MidasVector eta(n_size);
   //Mout << "eta_num_dc: " << eta_num_dc << endl;


   // 
   //construction of the jacobian by Left-jacobian transformation for test purpose
   //

   Mout << "Computation of jacobian matrix by left-transformer" << endl;
   MidasMatrix sigma_left(vecsize,vecsize,C_0);
   for (In i=0; i < vecsize; i++)
   {
      // Fill in numbers in test_l
      DataCont LvecData(vecsize, C_0);
      Nb val=C_1;
      //for (In j=I_0; j < vecsize; j++)
      //{
      //   if (j<sin_size)
      //   {
      //      val =C_0;
      //      LvecData.DataIo(IO_PUT,j,val);
      //   }
      //   if (j>sin_size)
      //   {
      //      val =C_1;
      //      LvecData.DataIo(IO_PUT,j,val);
      //   }
      //}
      //if (i >= sin_size) val = C_1;
      LvecData.DataIo(IO_PUT,i,val);
       Mout << "LvecData\n" << LvecData << endl;

      DataCont Gamma(vecsize,C_0);
      In ia = I_1;
      In ib = I_0;
      Nb nc = C_0;
      trf.VccRspTransJacobian(LvecData, Gamma, ia, ib, nc, false);
      MidasVector gamma(vecsize, C_0);
      Gamma.DataIo(IO_GET,gamma,vecsize); 
      sigma_left.AssignRow(gamma,i);
      //Mout << "transformer?" << (!mpVccCalcDef->TwoModeTrans()) << endl;
   }

   //Mout << "test_res: "<< endl; 
   //Mout << "left Jacobian \n "<< sigma_left << endl;
   MidasMatrix sigma_left_11 = PrintJacBlock(rblock,cblock,sigma_left, sin_size,doub_size);
   //Mout << "Left-Jacobian \n "<< sigma_left_11 << endl;
   //MidasMatrix diff(row_num,col_num, C_0); 


   if (C_0 != gVccCalcDef[aCalc].GetNumVccJacobian())
   {
      string name = mpVccCalcDef->GetName() + "_Vcc_Jacobian";
      DataCont Jac_dc;
      In n_size= vecsize*vecsize;
      Jac_dc.GetFromExistingOnDisc(n_size,name);
      MidasMatrix a(vecsize,vecsize,C_0);
      //Mout << "Jacob (num)\n "<< Jac_dc << endl;
      Jac_dc.DataIo(IO_GET, a,n_size,vecsize,vecsize);
      Jac_dc.SaveUponDecon();
      // Mout << "Jacobian (num)\n "<< a << endl;
      // Mout << "Jacobian (num)\n "<< endl;
      MidasMatrix a_11 = PrintJacBlock(rblock,cblock,a, sin_size,doub_size);
      //Mout << "Jacobian (num)\n "<< a_11 << endl;
      //Mout << "Jacobian \n "<< sigma_left_11 << endl;
      //Mout << "Jacobian - NumJacobian "<< endl;
      MidasMatrix diff = sigma_left_11;
      diff -= a_11;
      //Mout << diff << endl;
      norm = diff.Norm();
   }
   else if (C_0 == gVccCalcDef[aCalc].GetNumVccJacobian())
   {
      //
      //construction of the jacobian by Right-jacobian transformation for test purpose
      //

      Mout << "Computation of jacobian matrix by right-transformer" << endl;

      MidasMatrix sigma_right(vecsize,vecsize,C_0);
      if (mpVccCalcDef->V3trans())
      {
         midas::vcc::TransformerV3 trf1(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
         trf1.SetType(TRANSFORMER::VCCJAC);

         for (In i=0; i < vecsize; i++)
         {
            // Fill in numbers in test_l
            DataCont LvecData(vecsize, C_0);
            Nb val=C_1;
            LvecData.DataIo(IO_PUT,i,val);
            //Mout << "LvecData\n" << LvecData << endl;

            DataCont Gamma(vecsize,C_0);
            In ia = I_1;
            In ib = I_0;
            Nb nc = C_0;

            trf1.Transform(LvecData, Gamma, ia, ib, nc);
            MidasVector gamma(vecsize, C_0);
            Gamma.DataIo(IO_GET,gamma,vecsize); 
            sigma_right.AssignCol(gamma,i);
         }
      }
      else 
      {
         VccTransformer trf1(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
         trf1.SetType(TRANSFORMER::VCCJAC);

         for (In i=0; i < vecsize; i++)
         {
            // Fill in numbers in test_l
            DataCont LvecData(vecsize, C_0);
            Nb val=C_1;
            LvecData.DataIo(IO_PUT,i,val);
            //Mout << "LvecData\n" << LvecData << endl;

            DataCont Gamma(vecsize,C_0);
            In ia = I_1;
            In ib = I_0;
            Nb nc = C_0;

            trf1.VccRspTransJacobian(LvecData, Gamma, ia, ib, nc);
            MidasVector gamma(vecsize, C_0);
            Gamma.DataIo(IO_GET,gamma,vecsize); 
            sigma_right.AssignCol(gamma,i);
         }
      }
      //Mout << "right Jacobian \n "<< sigma_right << endl;
      //Mout << "left Jacobian - right Jacobian "<< endl;
      //MidasMatrix diff = sigma_left - sigma_right;
      MidasMatrix sigma_right_11 = PrintJacBlock(rblock,cblock,sigma_right,sin_size,doub_size);
      //Mout << "Left-Jacobian \n "<< sigma_left_11 << endl;
      //Mout << "Right-Jacobian \n "<< sigma_right_11 << endl;
      //Mout << "Left-Jacobian - Right-Jacobian "<< endl;
      MidasMatrix diff= sigma_left_11 - sigma_right_11;
      //Mout << diff << endl;
      norm = diff.Norm();
   }

   //Mout << "gamma "<< endl;
   //Mout << gamma << endl;

   Mout << "norm of diff matrix: " << norm << endl;
   //MIDASERROR(" get out of here, TestLa"); 
}

MidasMatrix Vcc::PrintJacBlock(In aRow, In aCol, MidasMatrix aJac, In aSin, In aDoub)
{
   Mout << "PrintJacBlock (" << aRow <<"," << aCol << ")" << endl;
   In sizes[I_2];
   sizes[I_0]=aSin;
   sizes[I_1]=aDoub;
   In brow = aRow - I_1;
   In bcol = aCol - I_1;

   In max_row= sizes[brow]; 
   In max_col= sizes[bcol]; 

   MidasMatrix jac(max_row,max_col, C_0);
   for (In i_row=I_0; i_row < max_row; i_row++) 
   {
      for (In i_col=I_0; i_col < max_col; i_col++)
      {
         jac[i_row][i_col]=aJac[i_row + brow*aSin][i_col + bcol*aSin];   
      }
   }
   //Mout << "jac_"<< aRow << aCol << "\n" << jac << endl;
   return jac;
}


/***************************************************************************//**
 *
 ******************************************************************************/
void Vcc::StoreGsCalcResults
   (  bool aConverged
   ,  Nb aEnergy
   ,  const DataCont& arVec
   ,  const ModeCombiOpRange& arMcr
   )
{
   mGsCalcConverged = aConverged;
   mGsFinalEnergy = aEnergy;
   if (mGsStoreObjectsInMem)
   {
      mGsCalcMcr.reset(new ModeCombiOpRange(arMcr));

      // Doing it with temp. MidasVector to make sure the new one is only in
      // mem, even if the other is on disk.
      MidasVector mv(arVec.Size());
      arVec.DataIo(IO_GET, mv, mv.Size());
      // Add a 0 value at reference index, if ModeCombiOpRange contains {} ModeCombi.
      // Questionable way of calculating it:
      // -  take address of last ModeCombi from range, then add calculated size
      //    of that MC.
      // -  assume it's either equal to arVec.Size() or 1 more.
      // -  in the latter case preprend with a 0 for the reference, otherwise
      //    MIDASERROR due to failed assumptions.
      if (mGsCalcMcr->Size() > 0)
      {
         const auto& mc = *mGsCalcMcr->rbegin();
         Uin exp_size = mc.Address() + NumParams(mc, pVccCalcDef()->GetNModals());
         if (exp_size == mv.Size() + 1)
         {
            mv.Insert(0, Nb(0));
         }
         else if(exp_size != mv.Size())
         {
            MIDASERROR("exp_size ("+std::to_string(exp_size)+") != mv.Size() ("+std::to_string(mv.Size())+") + 0 or 1.");
         }
      }
      mGsFinalVec.reset(new DataCont(mv, "InMem"));
   }
   else
   {
      mGsFinalVec.reset();
   }
}
