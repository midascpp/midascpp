/**
 *******************************************************************************
 * 
 * @file    ModalIntegrals_Impl.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For contractions and related manipulations of integrals uses in
 *    vibrational calculations.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MODALINTEGRALS_IMPL_H_INCLUDED
#define MODALINTEGRALS_IMPL_H_INCLUDED

#include <complex>

#include "util/Error.h"
#include "input/ModeCombi.h"
#include "mmv/MidasMatrix.h"
#include "tensor/NiceTensor.h"

// Initalize static members of class.
template<typename T>
const std::map<typename ModalIntegrals<T>::TransT, std::string> ModalIntegrals<T>::mTypeToString =
   {  {ModalIntegrals<T>::TransT::RAW,   "RAW"}
   ,  {ModalIntegrals<T>::TransT::T1,    "T1"}
   ,  {ModalIntegrals<T>::TransT::R1,    "R1"}
   };

/***************************************************************************//**
 * Because MidasVector<T>::operator=(const MidasVector<T>&) causes a hard error
 * if the two MidasVectors differ in size prior to the assignment, we need to
 * manually adjust the size prior to assignment, instead of just calling the
 * default assignment operator.
 *
 * @param[in] arOther
 *    The object to copy.
 * @return
 *    This object, after assignment.
 ******************************************************************************/
template
   <  typename T
   >
ModalIntegrals<T>& ModalIntegrals<T>::operator=
   (  const ModalIntegrals<T>& arOther
   )
{
   if (this != &arOther)
   {
      Contractor::operator=(arOther);
      mIntegrals = arOther.mIntegrals;
      mIntNorms2 = arOther.mIntNorms2;
      mType = arOther.mType;
      mName = arOther.mName;
      mNModalsVec = arOther.mNModalsVec;
   }
   return *this;
}

/***************************************************************************//**
 * @param[in] arIntegrals
 *    Integrals to be moved into this object.
 * @param[in] aType
 *    The transformation type of arIntegrals, useful e.g. if you are
 *    deliberately constructing an object with already T1/R1-transformed
 *    integrals.
 ******************************************************************************/
template
   <  typename T
   >
ModalIntegrals<T>::ModalIntegrals
   (  std::vector<std::vector<mat_t>>&& arIntegrals
   ,  TransT aType
   )
   :  mIntegrals(std::move(arIntegrals))
   ,  mType(aType)
{
   SanityCheck();
   UpdateIntNorms2();

   this->mNModalsVec.reserve(this->NModes());
   for(In i=I_0; i<this->NModes(); ++i)
   {
      Uin nmodals = 0;
      try
      {
         const auto& v_opers = mIntegrals.at(i);
         nmodals = v_opers.empty()  ?  0  :  v_opers.at(0).Nrows();
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out_of_range: "+std::string(oor.what()));
      }
      this->mNModalsVec.emplace_back(nmodals);
   }

   Contractor::Reserve(ReserveSizeForContractContainer());
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::SetName
   (  const std::string& arName
   )
{
   mName = arName;
}

/***************************************************************************//**
 * @return
 *    The number of modes for which the object stores integrals.
 ******************************************************************************/
template
   <  typename T
   >
Uin ModalIntegrals<T>::NModes
   (
   )  const
{
   return mIntegrals.size();
}

/***************************************************************************//**
 * @note
 *    Hard error if mode number is out of range.
 *
 * @param[in] aMode
 *    A mode number.
 * @return
 *    The number of operator terms for that mode, i.e. how many integral
 *    matrices does this object store for that mode.
 ******************************************************************************/
template
   <  typename T
   >
Uin ModalIntegrals<T>::NOper
   (  LocalModeNr aMode
   )  const
{
   try
   {
      return mIntegrals.at(aMode).size();
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out_of_range: "+std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 * @note
 *    Hard error if mode number is out of range.
 *
 * @param[in] aMode
 *    A mode number.
 * @return
 *    The number of modals for that mode, i.e. the number of rows/columns of
 *    the integrals matrices (they are square). This is the same for all
 *    operator terms of any given mode.
 ******************************************************************************/
template
   <  typename T
   >
Uin ModalIntegrals<T>::NModals
   (  LocalModeNr aMode
   )  const
{
   try
   {
      return mNModalsVec.at(aMode);
   }
   catch (  const std::out_of_range& err
         )
   {
      MIDASERROR("Cannot get NModals for mode '" + std::to_string(aMode) + "'. mNModalsVec.size() = " + std::to_string(mNModalsVec.size()) + ".");
      return 0;
   }
}

/***************************************************************************//**
 * The aType determines of which subblock of the given (mode, operator)-matrix
 * of integral to get the norm squared:
 *     P |  U
 *     --+------
 *       |
 *     D |  F
 *       |
 *
 * @note
 *    Hard error if anything is out of range.
 *
 * @param[in] aMode
 *    A mode number.
 * @param[in] aOper
 *    An operator number for that mode.
 * @param[in] aType
 *    Contraction type, see ModalIntegrals<T>::ContT.
 * @return
 *    The (Frobenius) norm squared of the elements of the block of the (mode,
 *    operator) integrals; which block depends on the value of aType.
 ******************************************************************************/
template<typename T>
typename ModalIntegrals<T>::absval_t ModalIntegrals<T>::GetNorm2
   (  Uin aMode
   ,  Uin aOper
   ,  ContT aType
   )  const
{
   try
   {
      return mIntNorms2.at(aMode).at(aOper).at(aType);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
typename ModalIntegrals<T>::TransT ModalIntegrals<T>::GetType
   (
   )  const
{
   return mType;
}

template
   <  typename T
   >
std::string ModalIntegrals<T>::GetTypeString
   (
   )  const
{
   try
   {
      return mTypeToString.at(GetType());
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      return mTypeToString.at(TransT::RAW); // Just to quench compiler warning.
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
const std::string& ModalIntegrals<T>::Name
   (
   )  const
{
   return mName;
}

/***************************************************************************//**
 * @note
 *    Hard error if mode or operator number is out of range.
 *
 * @param[in] aMode
 *    The mode number.
 * @param[in] aOper
 *    The operator number for given mode.
 * @return
 *    The (square) matrix (of size `NModals(aMode)`) containing the
 *    operator matrix elements for the operator and mode.
 ******************************************************************************/
template
   <  typename T
   >
const typename ModalIntegrals<T>::mat_t& ModalIntegrals<T>::GetIntegrals
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   )  const
{
   try
   {
      return mIntegrals.at(aMode).at(aOper);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      // Quenches compiler warning...
      static mat_t m;
      return m;
   }
};

/***************************************************************************//**
 * @return
 *    The vector of vectors of matrices containing all integrals
 ******************************************************************************/
template
   <  typename T
   >
const std::vector<std::vector<typename ModalIntegrals<T>::mat_t> >& ModalIntegrals<T>::GetIntegrals
   (
   )  const
{
   return mIntegrals;
};

/***************************************************************************//**
 * @note
 *    Hard error if mode/operator number or any indicies is out of range.
 *
 * @param[in] aMode
 *    The mode number.
 * @param[in] aOper
 *    The operator number for given mode.
 * @param[in] aLeft
 * @param[in] aRight
 * @return
 *    The (aLeft,aRight) matrix element of the (aMode, aOper)'th integral
 *    matrix.
 ******************************************************************************/
template
   <  typename T
   >
typename ModalIntegrals<T>::param_t ModalIntegrals<T>::GetElement
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   ,  Uin aLeft
   ,  Uin aRight
   )  const
{
   if (aLeft >= NModals(aMode) || aRight >= NModals(aMode))
   {
      MIDASERROR
         (  "Indices out of range: aLeft = "+std::to_string(aLeft)
         +  ", aRight = "+std::to_string(aRight)
         +  ", one of which is >= "+std::to_string(NModals(aMode))
         );
      return param_t(0);
   }
   else
   {
      return GetIntegrals(aMode, aOper)[aLeft][aRight];
   }
}

/***************************************************************************//**
 * @note
 *    Hard error if mode/operator number or occupied/occupied indicies are out
 *    of range.
 *
 * @note
 *    The occupied index is defined/hardcoded to be _zero_, i.e. returns the
 *    (0,0) matrix element.
 *
 * @param[in] aMode
 *    The mode number.
 * @param[in] aOper
 *    The operator number for given mode.
 * @return
 *    The (occ,occ) matrix element of the (aMode, aOper)'th integral
 *    matrix.
 ******************************************************************************/
template
   <  typename T
   >
typename ModalIntegrals<T>::param_t ModalIntegrals<T>::GetOccElement
   (  LocalModeNr aMode
   ,  LocalOperNr aOper
   )  const
{
   return GetElement(aMode, aOper, I_0, I_0);
}

/***************************************************************************//**
 * Convert the stored integrals/matrix elements from the _raw_ ones,
 * \f$ h^{m,o}_{rs} \f$ in \f$ \sum_{rs} h^{m,o}_{rs} E^m_{rs} \f$,
 * to the T1-transformed ones,
 * \f$ \widetilde{h}^{m,o}_{pq} \f$ in 
 * \f$ \widetilde{h}^{m,o} = \sum_{pq} \widetilde{h}^{m,o}_{pq} E^m_{pq} \f$,
 * which are related through
 * \f[
 *    \widetilde{h}^{m,o} = e^{-T^m_1} h^{m,o} e^{T^m_1}.
 * \f]
 *
 * Since it's a one-mode operator, we may use the resolution of identity,
 * \f[
 *    I  = \sum_{r} a^{m \dagger}_r \vert \text{vac} \rangle \langle \text{vac} \vert a^m_r
 *       = \sum_{r} \vert r \rangle \langle r \vert
 *       ,
 * \f]
 * to achieve
 * \f{align*}{
 *    \widetilde{h}^{m,o}_{pq}
 *    &= \langle p \vert \widetilde{h}^{m,o} \vert q \rangle
 *    \\
 *    &= \langle p \vert e^{-T^m_1} h^{m,o} e^{T^m_1} \vert q \rangle
 *    \\
 *    &= \sum_{rs} 
 *       \langle p \vert e^{-T^m_1} \vert r \rangle
 *       \langle r \vert h^{m,o}    \vert s \rangle
 *       \langle s \vert e^{T^m_1}  \vert q \rangle
 *    \\
 *    &= \sum_{rs} 
 *       ( \delta_{pr} - \delta_{ri} t^m_p )
 *       h^{m,o}_{rs}
 *       ( \delta_{sq} + \delta_{qi} t^m_s )
 * \f}
 * where we use the implicit notation that \f$ t^m_r = 0 \f$ if \f$ r = i \f$
 * (i.e. if it's the occupied index), and otherwise it's just the regular
 * T1-amplitude \f$ t^m_a \f$ for \f$ r = a \f$.
 *
 * We get
 * \f{align*}{
 *    \widetilde{h}^{m,o}_{ii}
 *    &= h^{m,o}_{ii}
 *    +  \left( \sum_{c} h^{m,o}_{ic} t^m_c \right)
 *    \\
 *    \widetilde{h}^{m,o}_{ai}
 *    &= h^{m,o}_{ai}
 *    +  \left( \sum_{c} h^{m,o}_{ac} t^m_c \right)
 *    -  \left(h^{m,o}_{ii} + \sum_{c} h^{m,o}_{ic} t^m_c \right) t^m_a
 *    \\
 *    \widetilde{h}^{m,o}_{ib}
 *    &= h^{m,o}_{ib}
 *    \\
 *    \widetilde{h}^{m,o}_{ab}
 *    &= h^{m,o}_{ab}
 *    -  h^{m,o}_{ib} t^m_a
 *    \\
 * \f}
 * Thus, the transformation \f$ h^{m,o}_{rs} \to h^{m,o}_{rs} \f$ of each
 * matrix can be performed in-place by first transforming the left-most column,
 * \f$ h^{m,o}_{ri} \f$, then the \f$ h^{m,o}_{ab} \f$ part, and the 
 * \f$ h^{m,o}_{ia} \f$ block remains untouched.
 *
 * @note
 *    Only _raw_ modal integrals are allowed to be T1-transformed. Hard error
 *    if trying to again transform integrals that were already transformed.
 *
 * @param[in] arT1
 *    The T1-amplitudes for each mode; the T1-vector for each mode `m` must be
 *    of size `NModals(m)`; however, in calculations there are only
 *    `NModals(m)-1` T1-amplitudes (none for the occupied modal). Thus,
 *    let the 0'th element of each T1-vector be some dummy number (it isn't
 *    used for anything), and fill in the T1 amps. in the rest of the vector.
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::T1Transform
   (  const std::vector<vec_t>& arT1
   )
{
   if (mType == TransT::RAW)
   {
      T1TransformImpl(arT1);
   }
   else
   {
      MIDASERROR
         (  "Can only T1-transform "+mTypeToString.at(TransT::RAW)+" integrals. "
         +  "These are of type "+GetTypeString()+"."
         );
   }
}

/***************************************************************************//**
 * Convert the stored integrals/matrix elements from the _T1-transformed_ ones,
 * \f$ \widetilde{h}^{m,o}_{rs} \f$ in \f$ \sum_{rs} \widetilde{h}^{m,o}_{rs} E^m_{rs} \f$,
 * to the R1-transformed ones,
 * \f$ \bar{h}^{m,o}_{pq} \f$ in 
 * \f$ \bar{h}^{m,o} = \sum_{pq} \bar{h}^{m,o}_{pq} E^m_{pq} \f$,
 * which are related through
 * \f[
 *    \bar{h}^{m,o} = [\widetilde{h}^{m,o}, R^m_1].
 * \f]
 *
 * Since it's a one-mode operator, we may use the resolution of identity,
 * \f[
 *    I  = \sum_{s} a^{m \dagger}_s \vert \text{vac} \rangle \langle \text{vac} \vert a^m_s
 *       = \sum_{s} \vert s \rangle \langle s \vert
 *       ,
 * \f]
 * to achieve
 * \f{align*}{
 *    \bar{h}^{m,o}_{pq}
 *    &= \langle p \vert [\widetilde{h}^{m,o}, R^m_1] \vert q \rangle
 *    \\
 *    &= \sum_s
 *       \left(
 *       \langle p \vert \widetilde{h}^{m,o} \vert s \rangle
 *       \langle s \vert R^m_1 \vert q \rangle
 *       -
 *       \langle p \vert R^m_1 \vert s \rangle
 *       \langle s \vert \widetilde{h}^{m,o} \vert q \rangle
 *       \right)
 *    \\
 *    &= \sum_s
 *       \left(
 *       \widetilde{h}^{m,o}_{ps} r^m_s \delta_{iq}
 *       -
 *       r^m_p \delta_{is} \widetilde{h}^{m,o}_{sq}
 *       \right)
 *    \\
 *    &= \left(
 *       \sum_c
 *       \widetilde{h}^{m,o}_{pc} r^m_c 
 *       \right)
 *       \delta_{iq}
 *       -
 *       r^m_p \widetilde{h}^{m,o}_{iq}
 * \f}
 * where we use the implicit notation that \f$ r^m_s = 0 \f$ if \f$ s = i \f$
 * (i.e. if it's the occupied index), and otherwise it's just the regular
 * R1-coefficient \f$ r^m_a \f$ for \f$ s = a \f$.
 *
 * We get
 * \f{align*}{
 *    \bar{h}^{m,o}_{ii}
 *    &= \sum_{c} \widetilde{h}^{m,o}_{ic} r^m_c
 *    \\
 *    \bar{h}^{m,o}_{ai}
 *    &= \sum_{c} \widetilde{h}^{m,o}_{ac} r^m_c
 *    -  \widetilde{h}^{m,o}_{ii} r^m_a
 *    \\
 *    \bar{h}^{m,o}_{ib}
 *    &= 0
 *    \\
 *    \bar{h}^{m,o}_{ab}
 *    &= - \widetilde{h}^{m,o}_{ib} r^m_a
 * \f}
 * Thus, the transformation \f$ \widetilde{h}^{m,o}_{rs} \to \bar{h}^{m,o}_{rs} \f$
 * of each matrix can be performed in-place by first transforming the 
 * \f$ \widetilde{h}^{m,o}_{ai} \f$ block, then the \f$ \widetilde{h}^{m,o}_{ab} \f$
 * block, then the \f$ \widetilde{h}^{m,o}_{ii} \f$, and lastly set the 
 * \f$ \widetilde{h}^{m,o}_{ib} \f$ block to zero.
 *
 * @note
 *    Only _T1-transformed_ modal integrals are allowed to be R1-transformed.
 *    Hard error if trying to transform raw or already R1-transformed ones.
 *
 * @param[in] arR1
 *    The R1-coefficients for each mode; the R1-vector for each mode `m` must be
 *    of size `NModals(m)`; however, in calculations there are only
 *    `NModals(m)-1` R1-coefficients (none for the occupied modal). Thus,
 *    let the 0'th element of each R1-vector be some dummy number (it isn't
 *    used for anything), and fill in the R1 coefs. in the rest of the vector.
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::R1Transform
   (  const std::vector<vec_t>& arR1
   )
{
   if (mType == TransT::T1)
   {
      R1TransformImpl(arR1);
   }
   else
   {
      MIDASERROR
         (  "Can only R1-transform "+mTypeToString.at(TransT::T1)+" integrals. "
         +  "These are of type "+GetTypeString()+"."
         );
   }
}

/***************************************************************************//**
 * @warning
 *    Due to the use of Contractor::mContractContainer
 *    I think this function is **not thread safe!!**
 *
 * @param[in] arM1
 *    ModeCombi for contraction _result_ parameters.
 * @param[in] arM0
 *    ModeCombi for contraction _input_ parameters.
 * @param[in] aOperMode
 *    Operator mode for which contraction is performed.
 * @param[in] aOper
 *    Operator term for contraction.
 * @param[out] arY1
 *    Resulting parameters. This vector _must_ be zero and have the correct
 *    size prior to calling this function.
 * @param[in] arY0
 *    Input parameters.
 * @param[in] aWhich
 *    -I_1: Down,   I_0: Forward,   I_1: Up
 * @param[in] aType
 *    Operation to perform on integrals before contraction
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::Contract
   (  const ModeCombi& arM1
   ,  const ModeCombi& arM0
   ,  const LocalModeNr aOperMode
   ,  const LocalOperNr aOper
   ,  vec_t& arY1
   ,  const vec_t& arY0
   ,  In aWhich
   ,  midas::vcc::IntegralOpT aType
   )  const
{
   Contractor::Contract(arM1, arM0, aOperMode, aOper, arY1, arY0, aWhich, this->GetIntegrals(), this->mNModalsVec, aType);
}

/**
 * Make down contraction of NiceTensor with integrals
 * @param aOperMode         The operator mode to be contracted
 * @param aOper             Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx              The index to be contracted
 * @param aType             Operation to perform on integrals before contraction
 * @param aTensorPtr        The tensor to be contracted
 * @result                  The result of the contraction in NiceTensor format
 **/
template
   <  typename T
   >
NiceTensor<T> ModalIntegrals<T>::ContractDown
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<T>& aTensorPtr
   )  const
{
   return Contractor::ContractDown(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mIntegrals, this->mNModalsVec);
}

/**
 * Make forward contraction of NiceTensor with integrals.
 * @param aOperMode          The operator mode to be contracted.
 * @param aOper              Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx               The index to be contracted.
 * @param aType             Operation to perform on integrals before contraction
 * @param aTensorPtr         The tensor to be contracted.
 * @result                   The result of the contraction in NiceTensor format.
 **/
template
   <  typename T
   >
NiceTensor<T> ModalIntegrals<T>::ContractForward
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<T>& aTensorPtr
   )  const
{
   return Contractor::ContractForward(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mIntegrals, this->mNModalsVec);
}

/**
 * Make up contraction of NiceTensor with integrals
 * @param aOperMode      The operator mode to be contracted
 * @param aOper          Specific operator ((DDQ), Q, Q^2, ...). (I think. -MBH)
 * @param aIdx           The index to be contracted
 * @param aType             Operation to perform on integrals before contraction
 * @param aTensorPtr     The tensor to be contracted
 * @result               The result of the contraction in NiceTensor format
 **/
template
   <  typename T
   >
NiceTensor<T> ModalIntegrals<T>::ContractUp
   (  const In aOperMode
   ,  const In aOper
   ,  const In aIdx
   ,  midas::vcc::IntegralOpT aType
   ,  const NiceTensor<T>& aTensorPtr
   )  const
{
   return Contractor::ContractUp(aOperMode, aOper, aIdx, aType, aTensorPtr, this->mIntegrals, this->mNModalsVec);
}


/***************************************************************************//**
 * Checks
 *    -  That all integral matrices of a given mode have same (square) size.
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::SanityCheck
   (
   )  const
{
   auto err = [](std::string s, Uin m, Uin o)->std::string
      {
         return s+" (mode = "+std::to_string(m)+", oper = "+std::to_string(o)+")";
      };

   for(Uin i_mode = 0; i_mode < mIntegrals.size(); ++i_mode)
   {
      const auto& v_opers = mIntegrals.at(i_mode);
      Uin exp_size = v_opers.empty()? 0: v_opers.at(0).Nrows();
      for(Uin i_oper = 0; i_oper < v_opers.size(); ++i_oper)
      {
         const auto& mat = v_opers.at(i_oper);
         if (mat.Nrows() != mat.Ncols())
         {
            MIDASERROR(err("Non-square integral matrix", i_mode, i_oper));
         }
         else if (mat.Nrows() != exp_size)
         {
            MIDASERROR
               (err  (  "Matrix size; expected "+std::to_string(exp_size)
                     +  ", got "+std::to_string(mat.Nrows())
                     ,  i_mode
                     ,  i_oper
                     )
               );
         }
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::UpdateIntNorms2
   (
   )
{
   // Shape/allocate. Resizes should have no effect if already of right size.
   mIntNorms2.resize(NModes());
   mIntNorms2.shrink_to_fit();
   for (LocalModeNr i_mode = I_0; i_mode < NModes(); ++i_mode) // loop over modes
   {
      auto& v_m = mIntNorms2.at(i_mode);
      v_m.resize(NOper(i_mode));
      v_m.shrink_to_fit();
      for (In i_op = I_0; i_op < NOper(i_mode); ++i_op) // loop opers in mode
      {
         auto& v_m_o = v_m.at(i_op);
         v_m_o.resize(ContT::NUM_OF_TYPES, 0);
         v_m_o.shrink_to_fit();
         const auto& ints = mIntegrals.at(i_mode).at(i_op);
         v_m_o[ContT::PASSIVE] = ints.Norm2Block(0, 1, 0, 1);
         v_m_o[ContT::DOWN]    = ints.Norm2Block(0, 1, 1, ints.Ncols());
         v_m_o[ContT::UP]      = ints.Norm2Block(1, ints.Nrows(), 0, 1);
         v_m_o[ContT::FORWARD] = ints.Norm2Block(1, ints.Nrows(), 1, ints.Ncols());
      }
   }
}

/***************************************************************************//**
 * @see
 *    T1Transform(const std::vector<vec_t>&)
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::T1TransformImpl
   (  const std::vector<vec_t>& arT1
   )
{
   if (arT1.size() != NModes())
   {
      MIDASERROR("NModes; "+std::to_string(arT1.size())+" != "+std::to_string(NModes()));
   }
   else
   {
      for(Uin m = 0; m < NModes(); ++m)
      {
         if (NModals(m) > 0)
         {
            const auto& t1 = arT1[m];
            if (t1.Size() != NModals(m))
            {
               MIDASERROR("Dims.; "+std::to_string(t1.Size())+" != "+std::to_string(NModals(m)));
            }
            else
            {
               for(Uin o = 0; o < NOper(m); ++o)
               {
                  T1TransformImpl(mIntegrals[m][o], t1);
               }
            }
         }
      }

      UpdateIntNorms2();
      mType = TransT::T1;
   }
}

/***************************************************************************//**
 * See T1Transform(const std::vector<vec_t>&) for description of the math.
 *
 * @param[in,out] arMat
 *    The integrals matrix to modify; _must_ be of square, non-zero size.
 * @param[in] arT1
 *    T1-amplitudes for the mode that the arMat belongs to.
 *    _Must_ have size equal to arMat dimension.
 *    0'th element can be anything; isn't used (so it can just be zero).
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::T1TransformImpl
   (  mat_t& arMat
   ,  const vec_t& arT1
   )
{
   // Define i = 0 locally, to make code more comparable to theory.
   const Uin i = 0;

   // Dot of virtual part of matrix row and vector, and updates row
   // simultaneously.
   auto dot_and_update_row = [&arMat, &arT1](Uin row, param_t t_row) -> param_t
      {
         param_t dot = 0;
         for(Uin b = 1; b < arMat.Ncols(); ++b)
         {
            dot += arMat[row][b]*arT1[b];
            arMat[row][b] -= arMat[i][b]*t_row;
         }
         return dot;
      };

   // Update the h_ii; this is the value that must be used when updating the
   // h_ai block. Passing 0 to lambda function, means h_ib block is untouched.
   arMat[i][i] += dot_and_update_row(i, 0);
   const auto& h_ii_plus_dot_i = arMat[i][i];

   // Go through matrix row-by-row, updating h_ai and h_ab blocks.
   for(Uin a = 1; a < arMat.Nrows(); ++a)
   {
      arMat[a][i] += dot_and_update_row(a, arT1[a]) - h_ii_plus_dot_i*arT1[a];
   }
}

/***************************************************************************//**
 * @see
 *    R1Transform(const std::vector<vec_t>&)
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::R1TransformImpl
   (  const std::vector<vec_t>& arR1
   )
{
   if (arR1.size() != NModes())
   {
      MIDASERROR("NModes; "+std::to_string(arR1.size())+" != "+std::to_string(NModes()));
   }
   else
   {
      for(Uin m = 0; m < NModes(); ++m)
      {
         if (NModals(m) > 0)
         {
            const auto& t1 = arR1[m];
            if (t1.Size() != NModals(m))
            {
               MIDASERROR("Dims.; "+std::to_string(t1.Size())+" != "+std::to_string(NModals(m)));
            }
            else
            {
               for(Uin o = 0; o < NOper(m); ++o)
               {
                  R1TransformImpl(mIntegrals[m][o], t1);
               }
            }
         }
      }

      UpdateIntNorms2();
      mType = TransT::R1;
   }
}

/***************************************************************************//**
 * See R1Transform(const std::vector<vec_t>&) for description of the math.
 *
 * @param[in,out] arMat
 *    The integrals matrix to modify; _must_ be of square, non-zero size.
 * @param[in] arR1
 *    R1-coefficients for the mode that the arMat belongs to.
 *    _Must_ have size equal to arMat dimension.
 *    0'th element can be anything; isn't used (so it can just be zero).
 ******************************************************************************/
template
   <  typename T
   >
void ModalIntegrals<T>::R1TransformImpl
   (  mat_t& arMat
   ,  const vec_t& arR1
   )
{
   // Define i = 0 locally, to make code more comparable to theory.
   const Uin i = 0;

   // Dot of virtual part of matrix row and vector, and updates row
   // simultaneously.
   auto dot_and_update_row = [&arMat, &arR1](Uin row, param_t r_row) -> param_t
      {
         param_t dot = 0;
         for(Uin b = 1; b < arMat.Ncols(); ++b)
         {
            dot += arMat[row][b] * arR1[b];
            arMat[row][b] = -arMat[i][b]*r_row;
         }
         return dot;
      };

   // Update in this order, in order to not overwrite former matrix element
   // until they are no longer needed.
   //    - h_ai block (up) (uses h_ii (passive) and h_ab (forward) blocks)
   //    - h_ab block (forward) (uses h_ib (down) block)
   //    - h_ii block (passive) (uses h_ib (down) block)
   //    - h_ib block (down) - just set to zero
   for(Uin a = 1; a < arMat.Nrows(); ++a)
   {
      arMat[a][i] = dot_and_update_row(a, arR1[a]) - arMat[i][i]*arR1[a];
   }
   arMat[i][i] = dot_and_update_row(i, 0);
}

/***************************************************************************//**
 * Contractor::mContractContainer will at most need to store a full (mode,oper) matrix,
 * meaning we just have to find the mode with the most modals, and then it'll
 * be the square of that.
 *
 * @return
 *    The maximum size needed to be reserved for Contractor::mContractContainer.
 ******************************************************************************/
template
   <  typename T
   >
Uin ModalIntegrals<T>::ReserveSizeForContractContainer
   (
   )  const
{
   Uin max_n_modals = 0;
   for(Uin m = 0; m < NModes(); ++m)
   {
      max_n_modals = std::max(max_n_modals, NModals(m));
   }
   return max_n_modals*max_n_modals;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<typename T>
std::ostream& operator<<
   (  std::ostream& aOut
   ,  const ModalIntegrals<T>& aInts
   )
{
   aOut << "Integrals:" << std::endl;
   for (In mode=I_0; mode<aInts.NModes(); ++mode)
   {
      aOut << "Mode: " << mode << std::endl;
      for (In oper=I_0; oper<aInts.NOper(mode); ++oper)
      {
         aOut << "Oper: " << oper << std::endl;
         aOut << aInts.GetIntegrals(mode,oper);
      }
      aOut << std::endl;
   }
   return aOut;
}


#endif/*MODALINTEGRALS_IMPL_H_INCLUDED*/
