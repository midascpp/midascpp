#ifndef TENSORSUMACCUMULATOR_IMPL_H_INCLUDED
#define TENSORSUMACCUMULATOR_IMPL_H_INCLUDED

#include "vcc/TensorSumAccumulator_Decl.h"
#include "util/CallStatisticsHandler.h"
#include "tensor/Scalar.h"

namespace midas::vcc
{

/**
 * Constructor
 * @param aDimensions                Dimensions of the tensor sum.
 * @param aDecomposer                
 * @param aAllowedRank
 * @param aDecompThreshold
 **/
template
   <  typename T
   >
TensorSumAccumulator<T>::TensorSumAccumulator
   (  const std::vector<unsigned>& aDimensions
   ,  const midas::tensor::TensorDecomposer* const aDecomposer 
   ,  In aAllowedRank
   ,  real_t aDecompThreshold
   )
   :  mDimensions(aDimensions)
   ,  mTensor()
   ,  mDecomposer(aDecomposer)
   ,  mAllowedRank(aAllowedRank)
   ,  mDecompThreshold(aDecompThreshold)
{
   // Check that we do not attempt decompositions with other template parameters than Nb
   // (since TensorDecomposer is hard coded to Nb!).
   if constexpr   (  !std::is_same_v<T, Nb>
                  )
   {
      if (  aDecomposer
         || aAllowedRank != I_0
         || aDecompThreshold != -C_1
         )
      {
         MIDASERROR("TensorSumAccumulator<T> can only hold decomposer and have non-zero allowed rank and mDecompThreshold = -1 for T=Nb.");
      }
   }

   // Initialize tensor
   mTensor  =  mDimensions.size()   ?  mAllowedRank   ?  NiceTensorSum<T>(mDimensions) 
                                                      :  NiceSimpleTensor<T>(mDimensions) 
                                    :  NiceScalar<T>(static_cast<T>(0.0), mAllowedRank != 0);

}

/**
 * Add another tensor to the sum.
 * @param aTensor         Tensor to add.
 * @return                Reference to this, so operators can be chained.
 **/
template
   <  typename T
   >
TensorSumAccumulator<T>& TensorSumAccumulator<T>::operator+=
   (  const NiceTensor<T>& aTensor
   )
{
   LOGCALL("begin");

   mTensor += aTensor;
   
   // If we allow decompositions, do additional checks
   if constexpr   (  std::is_same_v<T, Nb>
                  )
   {
      if (  mAllowedRank 
         && mTensor.Type() == BaseTensor<T>::typeID::SUM 
         && mAllowedRank != -1
         && mTensor.template StaticCast<TensorSum<T> >().Rank() > mAllowedRank
         )
      {
         LOGCALL("recomp");
         auto tensor = std::move(mTensor);
         mTensor = NiceTensorSum<T>(mDimensions);
         assert(mDecomposer);

         // DEBUG: WRITE TENSOR FOR TESTING DECOMP ALGORITHMS
         #ifdef MIDAS_WRITE_CPVCC_ERRVECS
         if (  mTensor.NDim() > 2
            )
         {
            std::string basename = "errvec_before_recomp_";
            std::string name = "";

            // Find unique name
            for(In i=0; i<1000; ++i)
            {
               name = basename + std::to_string(i);
               if (  !InquireFile(name)
                  )
               {
                  break;
               }
            }

            // Write tensor
            auto canonical = NiceTensor<T>(tensor.template StaticCast<TensorSum<T>>().ConstructModeMatrices());
            auto rank = canonical.template StaticCast<CanonicalTensor<T>>().GetRank();
            midas::mpi::OFileStream output( name, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::out | std::ios::binary );
            canonical.Write(output);
            output.close();
            MIDASERROR("Tensor of rank " + std::to_string(rank) + " written to file!");
         }
         #endif /* MIDAS_WRITE_CPVCC_ERRVECS */

         mTensor += mDecomposer->Decompose(tensor, this->mDecompThreshold);
      }
   }

   return *this;
}

/**
 * Axpy
 **/
template
   <  typename T
   >
void TensorSumAccumulator<T>::Axpy
   (  const NiceTensor<T>& other
   ,  const T& coef
   )
{
   LOGCALL("begin");
   mTensor.Axpy(other, coef);
   
   // If we allow decompositions, do additional checks
   if constexpr   (  std::is_same_v<T, Nb>
                  )
   {
      if ( mAllowedRank 
         && mTensor.Type() == BaseTensor<T>::typeID::SUM 
         && mAllowedRank != -1
         && mTensor.template StaticCast<TensorSum<T> >().Rank() > mAllowedRank
         )
      {
         LOGCALL("recomp");
         auto tensor = std::move(mTensor);
         mTensor = NiceTensorSum<T>(mDimensions);
         assert(mDecomposer);

         // the coef has been taken into account from the first Axpy
         mTensor += mDecomposer->Decompose(tensor, this->mDecompThreshold);
      }
   }
}

/**
 * Evaluate the sum and return the result.
 * @return                 The result of the evaluation in either simple or canonical format.
 **/
template
   <  typename T
   >
NiceTensor<T> TensorSumAccumulator<T>::EvaluateSum
   (
   )
{
   // If we allow decompositions, check if it is necessary
   if constexpr   (  std::is_same_v<T, Nb>
                  )
   {
      if (  mAllowedRank
         && mTensor.Type() != BaseTensor<T>::typeID::SCALAR
         )
      {
         LOGCALL("recomp");
         assert(mDecomposer);

         if (  gDebug
            )
         {
            Mout  << " TensorSumAccumulator<T>::EvaluateSum()" << std::endl;
         }
         return mDecomposer->Decompose(mTensor, this->mDecompThreshold);
      }
   }

   // Fallback is to return mTensor without modification
   return mTensor;
}

/**
 * Get reference to internal tensor.
 * @return            Reference to tensor
 **/
template
   <  typename T
   >
NiceTensor<T>& TensorSumAccumulator<T>::Tensor
   (
   )
{
   return mTensor;
}


/**
 * Get const reference to internal tensor.
 * @return            Reference to tensor
 **/
template
   <  typename T
   >
const NiceTensor<T>& TensorSumAccumulator<T>::Tensor
   (
   )  const
{
   return mTensor;
}

} /* namespace midas::vcc */

#endif /* TENSORSUMACCUMULATOR_IMPL_H_INCLUDED */
