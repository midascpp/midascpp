/**
************************************************************************
* 
* @file                ExpHvci.h 
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   ExpHvci class and function declarations.
* 
* Last modified: Mon Oct 30, 2006  09:25AM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef EXPHVCI_H
#define EXPHVCI_H

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasMatrix.h"
#include "ni/OneModeInt.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/MultiIndex.h"
#include "vcc/Vcc.h"
#include "ni/OneModeInt.h"

class ExpHvci
{
   private:
      //Logical iteration info
      In mHamDim;                                       ///< Dimension of Hamiltonian matrix. 
      In mMaxExciLevel;                                 ///< excitation space 0 - mMaxExciLevel
      MidasMatrix mH;                                   ///< The Hamiltonian matrix.
      const VccCalcDef* const mpVccCalcDef;
      Vcc*        mpVcc;
      const OpDef* const mpOpDef;
      const BasDef* mpBasDef;
      OneModeInt* const mpOneModeInt;
      const string mDiagMeth;
   public:
      ExpHvci(const VccCalcDef* const apVccCalcDef, const OpDef* const apOpDef, const BasDef* apBasDef, 
            OneModeInt* const apOneModeInt, Vcc* const apVcc, const string& aDiagMeth,const In& aMaxExciLevel);       ///< constructor 
      void DiagonalizeHexp();                           ///< Diagonalize H.
      void ConstructHexp();                             ///< Construct the vci H explicitly
};

#endif

