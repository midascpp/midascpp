/**
 ************************************************************************
 * 
 * @file                DirProd.cc
 *
 * Created:             15-03-2019
 *
 * Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * Short Description:   General and specific handling of direct products.
 * 
 * Last modified:
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "DirProd.h"
#include "DirProd_Impl.h"

namespace midas::vcc::dirprod
{

#define INSTANTIATE_DIRPROD(T)                                          \
template                                                                \
void DirProd2                                                           \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const ModeCombi& aResMc                                           \
   ,  const std::vector<In>& aNmodals                                   \
   ,  const std::vector<GeneralMidasVector<T> >& aInVecs                \
   ,  const std::vector<ModeCombi>& aInMcs                              \
   ,  const std::vector<std::vector<In> >& aInNmodals                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdGen                                                         \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const std::vector<In>& aNmodals                                   \
   ,  const std::vector<GeneralMidasVector<T> >& aInVecs                \
   ,  const std::vector<std::vector<In> >& aConnect                     \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
bool DirProd_2vecs                                                      \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const ModeCombi& aMcRes                                           \
   ,  const GeneralMidasVector<T>& aInVec1                              \
   ,  const GeneralMidasVector<T>& aInVec2                              \
   ,  const ModeCombi& aMc1                                             \
   ,  const ModeCombi& aMc2                                             \
   ,  const std::vector<In>& aNmodals1                                  \
   ,  const std::vector<In>& aNmodals2                                  \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
bool DirProd_3vecs                                                      \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const ModeCombi& aMcRes                                           \
   ,  const GeneralMidasVector<T>& aInVec1                              \
   ,  const GeneralMidasVector<T>& aInVec2                              \
   ,  const GeneralMidasVector<T>& aInVec3                              \
   ,  const ModeCombi& aMc1                                             \
   ,  const ModeCombi& aMc2                                             \
   ,  const ModeCombi& aMc3                                             \
   ,  const std::vector<In>& aNmodals1                                  \
   ,  const std::vector<In>& aNmodals2                                  \
   ,  const std::vector<In>& aNmodals3                                  \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProd_vec                                                        \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const GeneralMidasVector<T>& aInVec1                              \
   ,  const GeneralMidasVector<T>& aInVec2                              \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProd_vec                                                        \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const GeneralMidasVector<T>& aInVec1                              \
   ,  const GeneralMidasVector<T>& aInVec2                              \
   ,  const GeneralMidasVector<T>& aInVec3                              \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProd_1m_2m_aba                                                  \
   (  GeneralMidasVector<T>& arRes                                      \
   ,  const GeneralMidasVector<T>& aInVec1                              \
   ,  const GeneralMidasVector<T>& aInVec2                              \
   ,  const std::vector<In>& aNmodals1                                  \
   ,  const std::vector<In>& aNmodals2                                  \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimple                                                \
   (  NiceTensor<T>& arRes                                              \
   ,  const ModeCombi& aResMc                                           \
   ,  const std::vector<NiceTensor<T> >& aInTensor                      \
   ,  const std::vector<ModeCombi>& aInMcs                              \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorCanonical                                             \
   (  NiceTensor<T>& arRes                                              \
   ,  const ModeCombi& aResMc                                           \
   ,  const std::vector<NiceTensor<T> >& aInTensor                      \
   ,  const std::vector<ModeCombi>& aInMcs                              \
   ,  const T aCoef                                                     \
   );

INSTANTIATE_DIRPROD(float);
INSTANTIATE_DIRPROD(double);
INSTANTIATE_DIRPROD(std::complex<float>);
INSTANTIATE_DIRPROD(std::complex<double>);

#undef INSTANTIATE_DIRPROD

} /* namespace midas::vcc::dirprod */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
