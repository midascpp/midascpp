/**
************************************************************************
* 
* @file                TensorProductAccumulator.h
*
* Created:             15-03-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORPRODUCTACCUMULATOR_H_INCLUDED
#define TENSORPRODUCTACCUMULATOR_H_INCLUDED

#include "TensorProductAccumulator_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "TensorProductAccumulator_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TENSORPRODUCTACCUMULATOR_H_INCLUDED */
