/**
 ************************************************************************
 * 
 * @file                DirProd.h
 *
 * Created:             31-03-2009
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
 *
 * Short Description:   General and specific handling of direct products.
 * 
 * Last modified:
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef DIRPROD_H_INCLUDED
#define DIRPROD_H_INCLUDED

#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "tensor/NiceTensor.h"

namespace midas::vcc::dirprod
{

template
   <  typename T
   >
void DirProd2
   (  GeneralMidasVector<T>& arRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<In>& aNmodals
   ,  const std::vector<GeneralMidasVector<T> >& aInVecs
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const std::vector<std::vector<In> >& aInNmodals
   ,  const T aCoef = T(C_1)
   );
///< Wrapper function for doing direct products. May use specialized or general routines.

template
   <  typename T
   >
void DirProdGen
   (  GeneralMidasVector<T>& arRes
   ,  const std::vector<In>& aNmodals
   ,  const std::vector<GeneralMidasVector<T> >& aInVecs
   ,  const std::vector<std::vector<In> >& aConnect
   ,  const T aCoef
   );
///< Generate direct product of vectors in aInVecs and add to arRes.

/**
 * Increment modal index vector aIvec by one according to the number of modals
 * in each mode.
 *
 * @param aIvec        Vector to increment.
 * @param aNmodals     Max size for each index.
 **/
template
   <  typename I
   >
void IncIvec
   (  std::vector<I>& aIvec
   ,  const std::vector<I>& aNmodals
   )
{
   for (In i=aIvec.size()-I_1; i>=0; --i)
   {
      if (++aIvec[i] == aNmodals[i])
      {
         aIvec[i] = I_0;
      }
      else
      {
         break;
      }
   }
}

template
   <  typename T
   >
bool DirProd_2vecs
   (  GeneralMidasVector<T>& arRes
   ,  const ModeCombi& aMcRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const ModeCombi& aMc1
   ,  const ModeCombi& aMc2
   ,  const std::vector<In>& aNmodals1
   ,  const std::vector<In>& aNmodals2
   ,  const T aCoef
   );
///< Call special functions for direct product between two vectors.

template
   <  typename T
   >
bool DirProd_3vecs
   (  GeneralMidasVector<T>& arRes
   ,  const ModeCombi& aMcRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const GeneralMidasVector<T>& aInVec3
   ,  const ModeCombi& aMc1
   ,  const ModeCombi& aMc2
   ,  const ModeCombi& aMc3
   ,  const std::vector<In>& aNmodals1
   ,  const std::vector<In>& aNmodals2
   ,  const std::vector<In>& aNmodals3
   ,  const T aCoef
   );
///< Call special functions for direct product between two vectors.

template
   <  typename T
   >
void DirProd_vec
   (  GeneralMidasVector<T>& arRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const T aCoef
   );
///< Direct product between two vectors.

template
   <  typename T
   >
void DirProd_vec
   (  GeneralMidasVector<T>& arRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const GeneralMidasVector<T>& aInVec3
   ,  const T aCoef
   );
///< Direct product between three vectors.

template
   <  typename T
   >
void DirProd_1m_2m_aba
   (  GeneralMidasVector<T>& arRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const std::vector<In>& aNmodals1
   ,  const std::vector<In>& aNmodals2
   ,  const T aCoef
   );
///< Direct product betwenn two vectors: (m1) x (m0,m2) -> (m0,m1,m2).

/**
 * Do direct product of all SimpleTensor%s.
 *
 * @param arRes      Result of direct product.
 * @param aResMc    Modecombination of result.
 * @param aInTensor Vector of tensors to be direct product'ed together.
 * @param aInMcs    Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef     Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimple
   (  NiceTensor<T>& arRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   );

/**
 * Do direct product of all CanonicalTensor%s.
 *
 * @param arRes     Result of direct product.
 * @param aResMc    Modecombination of result.
 * @param aInTensor Vector of tensors to be direct product'ed together.
 * @param aInMcs    Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef     Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorCanonical
   (  NiceTensor<T>& arRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   );

} /* namespace midas::vcc::dirprod */

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "DirProd_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* DIRPROD_H_INCLUDED */
