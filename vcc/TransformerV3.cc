/**
 ************************************************************************
 * 
 * @file                TransformerV3.cc
 *
 * Created:             01-10-2008
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementation of transformer using
 *                      intermediates.
 * 
 * Last modified: Mon Apr 12, 2010  11:59AM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cctype>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/TransformerV3.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermediateOperIter.h"
#include "vcc/v3/IntermedProd.h"
#include "vcc/v3/IntermedProdL.h"
#include "vcc/v3/FTCommutator.h"
#include "vcc/v3/V3Latex.h"
#include "vcc/Vcc.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/CtrProd.h"
#include "inc_gen/math_link.h"
#include "input/OpDef.h"
#include "vcc/v3/V3Enums.h"
#include "vcc/ModalIntegralsFuncs.h"

#include "tensor/NiceTensor.h"
#include "tensor/TensorDecomposer.h"

#include "libmda/util/output_call_addr.h"
#include "libmda/util/scope_guard.h"

#include "mpi/Impi.h"

const std::map<V3,std::string> V3_MAP::MAP = V3_MAP::create_map();

namespace midas::vcc
{

std::set<std::string> TransformerV3::mTestedMethods;

/**
 *
 **/
void TransformerV3::InitTestedMethods()
{
   mTestedMethods.clear();
  
   // VCC without T1 transformed H
   mTestedMethods.insert("vcc[1]_h2.v3c");
   mTestedMethods.insert("vcc[2]_h2.v3c");
   mTestedMethods.insert("vcc[3]_h2.v3c");
   mTestedMethods.insert("vcc[4]_h2.v3c");
   mTestedMethods.insert("vcc[5]_h2.v3c");
   mTestedMethods.insert("vcc[6]_h2.v3c");
   mTestedMethods.insert("vcc[1]_h3.v3c");
   mTestedMethods.insert("vcc[2]_h3.v3c");
   mTestedMethods.insert("vcc[3]_h3.v3c");
   mTestedMethods.insert("vcc[4]_h3.v3c");
   mTestedMethods.insert("vcc[5]_h3.v3c");
   mTestedMethods.insert("vcc[6]_h3.v3c");
   mTestedMethods.insert("vcc[2]_h4.v3c");
   mTestedMethods.insert("vcc[3]_h4.v3c");
   mTestedMethods.insert("vcc[4]_h4.v3c");
   mTestedMethods.insert("vcc[5]_h4.v3c");
   mTestedMethods.insert("vcc[6]_h4.v3c");
   mTestedMethods.insert("vcc[2]_h5.v3c");
   mTestedMethods.insert("vcc[3]_h5.v3c");
   mTestedMethods.insert("vcc[4]_h5.v3c");
   mTestedMethods.insert("vcc[5]_h5.v3c");
   
   // VCC with T1 transformed H.
   mTestedMethods.insert("vcc[1]_h2-t1.v3c");
   mTestedMethods.insert("vcc[2]_h2-t1.v3c");
   mTestedMethods.insert("vcc[3]_h2-t1.v3c");
   mTestedMethods.insert("vcc[4]_h2-t1.v3c");
   mTestedMethods.insert("vcc[5]_h2-t1.v3c");
   mTestedMethods.insert("vcc[6]_h2-t1.v3c");
   mTestedMethods.insert("vcc[1]_h3-t1.v3c");
   mTestedMethods.insert("vcc[2]_h3-t1.v3c");
   mTestedMethods.insert("vcc[3]_h3-t1.v3c");
   mTestedMethods.insert("vcc[4]_h3-t1.v3c");
   mTestedMethods.insert("vcc[5]_h3-t1.v3c");
   mTestedMethods.insert("vcc[6]_h3-t1.v3c");
   mTestedMethods.insert("vcc[2]_h4-t1.v3c");
   mTestedMethods.insert("vcc[3]_h4-t1.v3c");
   mTestedMethods.insert("vcc[4]_h4-t1.v3c");
   mTestedMethods.insert("vcc[5]_h4-t1.v3c");
   mTestedMethods.insert("vcc[6]_h4-t1.v3c");
   mTestedMethods.insert("vcc[2]_h5-t1.v3c");
   mTestedMethods.insert("vcc[3]_h5-t1.v3c");
   mTestedMethods.insert("vcc[4]_h5-t1.v3c");
   mTestedMethods.insert("vcc[5]_h5-t1.v3c");
    
   // VCC Jacobians without T1 transformed integrals.
   mTestedMethods.insert("vcc[1]_h2_jacobian.v3c");
   mTestedMethods.insert("vcc[2]_h2_jacobian.v3c");
   mTestedMethods.insert("vcc[1]_h3_jacobian.v3c");
   
   // VCC Jacobians with T1 transformed integrals.
   mTestedMethods.insert("vcc[1]_h2-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[2]_h2-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[3]_h2-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[4]_h2-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[1]_h3-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[2]_h3-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[3]_h3-t1_jacobian.v3c");
   mTestedMethods.insert("vcc[4]_h3-t1_jacobian.v3c");
   
   // VCC Jacobian left WITHOUT T1 transformed integrals
   mTestedMethods.insert("vcc[1]_h2_jacobianl.v3c");
   mTestedMethods.insert("vcc[1]_h3_jacobianl.v3c");
   
   // VCC Jacobian left WITH T1 transformed integrals
   mTestedMethods.insert("vcc[1]_h2-t1_jacobianl.v3c");
   mTestedMethods.insert("vcc[1]_h3-t1_jacobianl.v3c");

   // VCI methods.
   mTestedMethods.insert("vci[1]_h2.v3c");
   mTestedMethods.insert("vci[2]_h2.v3c");
   mTestedMethods.insert("vci[3]_h2.v3c");
   mTestedMethods.insert("vci[4]_h2.v3c");
   mTestedMethods.insert("vci[5]_h2.v3c");
   mTestedMethods.insert("vci[6]_h2.v3c");
   mTestedMethods.insert("vci[1]_h3.v3c");
   mTestedMethods.insert("vci[2]_h3.v3c");
   mTestedMethods.insert("vci[3]_h3.v3c");
   mTestedMethods.insert("vci[4]_h3.v3c");
   mTestedMethods.insert("vci[5]_h3.v3c");
   mTestedMethods.insert("vci[6]_h3.v3c");
   mTestedMethods.insert("vci[2]_h4.v3c");
   mTestedMethods.insert("vci[3]_h4.v3c");
   mTestedMethods.insert("vci[4]_h4.v3c");
   mTestedMethods.insert("vci[5]_h4.v3c");
   mTestedMethods.insert("vci[6]_h4.v3c");
   mTestedMethods.insert("vci[2]_h5.v3c");
   mTestedMethods.insert("vci[3]_h5.v3c");
   mTestedMethods.insert("vci[4]_h5.v3c");
   mTestedMethods.insert("vci[5]_h5.v3c");
   mTestedMethods.insert("vci[6]_h5.v3c");
}

/**
 * copy constructor
 * @param aVccTrf          transformer to copy 
 * @param aRspTrf          true if this is a response transformer
 **/
TransformerV3::TransformerV3
   (  const VccTransformer& aVccTrf
   ,  bool aRspTrf
   )
   :  VccTransformer(aVccTrf)
   ,  mImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, GeneralMidasVector, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTensorImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, NiceTensor, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTransformStat()
   ,  mMaxExciLevel(mpVccCalcDef->MaxExciLevel())
   ,  mAmplitudeFile(mpVccCalcDef->GetName() + "_Vcc_vec_0")
   ,  mDecompInfo(mpVccCalcDef->GetTransformerDecompInfoSet())
   ,  mAllowedRank
         (  aRspTrf  ?  mpVccCalcDef->RspTransformerAllowedRank()
                     :  mpVccCalcDef->GetTransformerAllowedRank()
         )
   ,  mTensorTransform
         (  mpVccCalcDef->Vcc()
         && mpVccCalcDef->UseTensorNlSolver()
         )
   ,  mTensorProductScreening
         (  aRspTrf  ?  mpVccCalcDef->RspTensorProductScreening()
                     :  mpVccCalcDef->TensorProductScreening()
         )
   ,  mAdaptiveTensorProductScreening
         (  aRspTrf  ?  -C_1
                     :  mpVccCalcDef->AdaptiveTensorProductScreening()
         )
   ,  mTensorProductLowRankLimit
         (  aRspTrf  ?  mpVccCalcDef->RspTensorProductLowRankLimit()
                     :  mpVccCalcDef->TensorProductLowRankLimit()
         )
   ,  mUseCpScalingForIntermeds(mpVccCalcDef->UseCpScalingForIntermeds())
{
   InitTestedMethods();
   mOutputStatus.SetNewSize(I_9,I_6);
   mOutputStatus.Zero();
   SetType(mType);
   if (  mTensorTransform
      )
   {
      this->mTensorImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
   else
   {
      this->mImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
}

/**
 * "copy" constructor from othre transformer and sub-excitation level
 * @param aVccTrf          transformer to "copy"
 * @param aPreconLevel     excitation level of newly constructed transformer
 **/
TransformerV3::TransformerV3
   ( const TransformerV3& aVccTrf
   , In aPreconLevel
   )
   :  VccTransformer(const_cast<TransformerV3&>(aVccTrf), aPreconLevel)
   ,  mImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, GeneralMidasVector, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTensorImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, NiceTensor, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTransformStat()
   ,  mMaxExciLevel(aPreconLevel)
   ,  mAmplitudeFile(mpVccCalcDef->GetName() + "_Vcc_vec_0")
   ,  mDecompInfo(mpVccCalcDef->GetTransformerDecompInfoSet())
   ,  mAllowedRank(aVccTrf.GetAllowedRank())
   ,  mTensorTransform
         (  mpVccCalcDef->Vcc()
         && mpVccCalcDef->UseTensorNlSolver()
         )
   ,  mTensorProductScreening(aVccTrf.TensorProductScreening())
   ,  mTensorProductLowRankLimit(aVccTrf.TensorProductLowRankLimit())
   ,  mUseCpScalingForIntermeds(mpVccCalcDef->UseCpScalingForIntermeds())
{
   InitTestedMethods();
   mOutputStatus.SetNewSize(I_9,I_6);
   mOutputStatus.Zero();
   SetType(mType);
   if (  mTensorTransform
      )
   {
      this->mTensorImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
   else
   {
      this->mImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
}

/**
 * "copy" constructor from other transformer and sub-excitation level
 * @param aVccTrf          transformer to "copy"
 * @param aPreconLevel     excitation level of newly constructed transformer
 * @param arAmplitudes     VCC amplitudes used for initializing the Jacobian
 **/
TransformerV3::TransformerV3
   (  const TransformerV3& aVccTrf
   ,  In aPreconLevel
   ,  const TensorDataCont& arAmplitudes
   )
   :  VccTransformer(const_cast<TransformerV3&>(aVccTrf), aPreconLevel, false)   // No "out-of-space" mXvec
   ,  mImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, GeneralMidasVector, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTensorImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, NiceTensor, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTransformStat()
   ,  mMaxExciLevel(aPreconLevel)
   ,  mAmplitudeFile(mpVccCalcDef->GetName() + "_Vcc_vec_0")
   ,  mDecompInfo(mpVccCalcDef->GetTransformerDecompInfoSet())
   ,  mAllowedRank(aVccTrf.GetAllowedRank())
   ,  mTensorTransform
         (  mpVccCalcDef->Vcc()
         && mpVccCalcDef->UseTensorNlSolver()
         )
   ,  mTensorProductScreening(aVccTrf.TensorProductScreening())
   ,  mTensorProductLowRankLimit(aVccTrf.TensorProductLowRankLimit())
   ,  mUseCpScalingForIntermeds(mpVccCalcDef->UseCpScalingForIntermeds())
{
   InitTestedMethods();
   mOutputStatus.SetNewSize(I_9,I_6);
   mOutputStatus.Zero();
   if (  mTensorTransform
      )
   {
      this->mTensorImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
   else
   {
      this->mImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }

   switch(this->mType)
   {
      case TRANSFORMER::VCCJAC:
      {
         // Niels: It is assumed that the amplitude TensorDataCont only contains the
         // amplitudes of the reduced space (aPreconLevel)
         this->SetToVccJac(arAmplitudes);
         return;
      }
      case TRANSFORMER::LVCCJAC:
      {
         MIDASERROR("Initialization of VCC Jacobian Left transformer from amplitudes in memory has not been implemented!");
         return;
      }
      default:
      {
         this->SetType(mType);
         return;
      }
   }

}

/**
 *
 **/
TransformerV3::TransformerV3
   (  Vcc* apVcc
   ,  VccCalcDef* apVccCalcDef
   ,  OpDef* apOpDef
   ,  const ModalIntegrals<param_t>* const apIntegralsOrig
   ,  param_t aEvmp0
   ,  In aISO
   ,  bool aRspTrf
   )
   :  VccTransformer(apVcc, apVccCalcDef, apOpDef, apIntegralsOrig, aEvmp0, aISO)
   ,  mImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, GeneralMidasVector, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTensorImMachine
         (  std::make_unique<v3::IntermediateMachine<param_t, NiceTensor, midas::vcc::v3::VccEvalData> >()
         )
   ,  mTransformStat()
   ,  mMaxExciLevel(mpVccCalcDef->MaxExciLevel())
   ,  mAmplitudeFile(mpVccCalcDef->GetName() + "_Vcc_vec_0")
   ,  mDecompInfo(apVccCalcDef->GetTransformerDecompInfoSet())
   ,  mAllowedRank
         (  aRspTrf  ?  mpVccCalcDef->RspTransformerAllowedRank()
                     :  mpVccCalcDef->GetTransformerAllowedRank()
         )
   ,  mTensorTransform
         (  mpVccCalcDef->Vcc()
         && mpVccCalcDef->UseTensorNlSolver()
         )
   ,  mTensorProductScreening
         (  aRspTrf  ?  mpVccCalcDef->RspTensorProductScreening()
                     :  mpVccCalcDef->TensorProductScreening()
         )
   ,  mAdaptiveTensorProductScreening
         (  aRspTrf  ?  -C_1
                     :  mpVccCalcDef->AdaptiveTensorProductScreening()
         )
   ,  mTensorProductLowRankLimit
         (  aRspTrf  ?  mpVccCalcDef->RspTensorProductLowRankLimit()
                     :  mpVccCalcDef->TensorProductLowRankLimit()
         )
   ,  mUseCpScalingForIntermeds
         (  mpVccCalcDef->UseCpScalingForIntermeds()
         )
{
   InitTestedMethods();
   mOutputStatus.SetNewSize(I_9,I_6);
   mOutputStatus.Zero();

   if (  mTensorTransform
      )
   {
      this->mTensorImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
   else
   {
      this->mImMachine->SetRestrictions( this->mpVccCalcDef->GetIntermedRestrict() );
   }
}

/**
 * Register interface
 **/
void TransformerV3::RegisterIntermeds
   (
   )
{
   this->mImMachine->Reset();
   this->mTensorImMachine->Reset();

   for (In i=I_0; i<mContribs.size(); ++i)
   {
      if (  auto* ip = dynamic_cast<v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>*>(mContribs[i].get())
         )
      {
         if (  this->mTensorTransform
            )
         {
            ip->RegisterIntermeds(*this->mTensorImMachine);
         }
         else
         {
            ip->RegisterIntermeds(*this->mImMachine);
         }
      }

      if (  auto* ipl = dynamic_cast<v3::IntermedProdL<param_t, midas::vcc::v3::VccEvalData>*>(mContribs[i].get())
         )
      {
         if (  this->mTensorTransform
            )
         {
            ipl->RegisterIntermeds(*this->mTensorImMachine);
         }
         else
         {
            ipl->RegisterIntermeds(*this->mImMachine);
         }
      }
   }
}

/**
 * List registered intermeds interface
 **/
void TransformerV3::ListRegisteredIntermeds
   (
   )  const
{
   if (  this->mTensorTransform
      )
   {
      this->mTensorImMachine->ListRegisteredIntermeds(Mout);
   }
   else
   {
      this->mImMachine->ListRegisteredIntermeds(Mout);
   }
}

/**
 * Set transformer to VCCJAC from a given set of VCC amplitudes
 *
 * @param arAmplitudes     VCC amplitudes in TensorDataCont format
 **/
void TransformerV3::SetToVccJac
   (  const TensorDataCont& arAmplitudes
   )
{
   mTransformStat.Reset(); // we are changing type, so we reset our timing statistics
   mType = TRANSFORMER::VCCJAC;
   const V3 v3type = mpVccCalcDef->V3type();

   switch (v3type)
   {
      case V3::GEN:
         InitGenVccJacobian(arAmplitudes);
         return;
      case V3::VCC1PT2:
         InitVCC1pt2Jacobian(arAmplitudes);
         return;
      case V3::VCC2:
         InitVCC2Jacobian(arAmplitudes);
         return;
      case V3::VCC2PT3:
         InitVCC2pt3Jacobian(arAmplitudes);
         return;
      case V3::VCC3PT4F:
         InitVCC3pt4FJacobian(arAmplitudes);
         return;
      case V3::VCC3PT4:
         InitVCC3pt4Jacobian(arAmplitudes);
         return;
      default:
         MIDASERROR("TransformerV3::SetToVccJac(): Unknown VCC V3 type.");
   }

}

/**
 * set type of transformation
 * @param aType      type of transformation
 **/
void TransformerV3::SetType
   (  TRANSFORMER aType
   )
{
   this->SetType(aType, this->mAmplitudeFile);
}

/**
 * set type of transformation
 * @param aType      type of transformation
 * @param arFileName Name of file containing VCC amplitudes
 **/
void TransformerV3::SetType
   (  TRANSFORMER aType
   ,  const std::string& arFileName
   )
{
   mTransformStat.Reset(); // we are changing type, so we reset our timing statistics
   mType = aType;
   const V3 v3type = mpVccCalcDef->V3type();
   if (mType == TRANSFORMER::VCI || mType == TRANSFORMER::VCIRSP)
   {
      switch (v3type)
      {
         case V3::GEN:
            InitGenVci();
            return;
         default:
            MIDASERROR("TransformerV3::SetType(): Unknown VCI V3 type.");
      }
   }
   else if (mType == TRANSFORMER::VCC)
   {
      switch (v3type)
      {
         case V3::GEN:
            InitGenVcc();
            return;
         case V3::VCC1PT2:
            InitVCC1pt2();
            return;
         case V3::VCC2:
            InitVCC2();
            return;
         case V3::VCC2PT3:
            InitVCC2pt3();
            return;
         case V3::VCC3PT4F:
            InitVCC3pt4F();
            return;
         case V3::VCC3PT4:
            InitVCC3pt4();
            return;
         default:
            MIDASERROR("TransformerV3::SetType(): Unknown VCC V3 type.");
      }
   }
   else if (mType == TRANSFORMER::VCCJAC)
   {
      switch (v3type)
      {
         case V3::GEN:
            InitGenVccJacobian(arFileName);
            return;
         case V3::VCC1PT2:
            InitVCC1pt2Jacobian(arFileName);
            return;
         case V3::VCC2:
            InitVCC2Jacobian(arFileName);
            return;
         case V3::VCC2PT3:
            InitVCC2pt3Jacobian(arFileName);
            return;
         case V3::VCC3PT4F:
            InitVCC3pt4FJacobian(arFileName);
            return;
         case V3::VCC3PT4:
            InitVCC3pt4Jacobian(arFileName);
            return;
         default:
            MIDASERROR("TransformerV3::SetType(): Unknown VCC V3 type.");
      }
   }
   else if (mType == TRANSFORMER::LVCCJAC)
   {
      switch (v3type)
      {
         case V3::GEN:
            InitGenVccJacobianL(arFileName);
            return;
         case V3::VCC2PT3:
            InitVCC2pt3JacobianL(arFileName);
            return;
         case V3::VCC3PT4F:
            InitVCC3pt4FJacobianL(arFileName);
            return;
         case V3::VCC3PT4:
            InitVCC3pt4JacobianL(arFileName);
            return;
         default:
            MIDASERROR("TransformerV3::SetType():\n"
                  "Left Jacobian transformation not supported for this VCC type.");
      }
   }
   else if (mType == TRANSFORMER::SPECIAL)
   {
      // Do nothing
   }
   else if (mType != TRANSFORMER::UNKNOWN)
   {
      std::ostringstream osstr;
      osstr << "TransformerV3::SetType(): Unsupported transformer type: " << mType << endl;
      MIDASERROR(osstr.str());
   }
}

/**
 * transform an input DataCont and save result in output DataCont
 * @param aIn          input DataCont to be transformed
 * @param aOut         output DataCont, afterwards holds transformed result
 * @param aI           switch parameter
 * @param aJ           switch parameter
 * @param aNb          for some transformations return energy (e.g. VCC)
 **/
void TransformerV3::Transform
   ( DataCont& aIn
   , DataCont& aOut
   , In aI
   , In aJ
   , param_t& aNb
   )
{
   mTransformStat.Start(); // start timer
   scope_on_exit{ mTransformStat.Stop(); }; // stop timer on scope exit
   
   // HACK!: WE CONVERT HERE FOR NOW
   mXvec.ConvertToDataCont();
   mTransXvec.ConvertToDataCont();
   
   // do prediag if flag is set
   if (mTrfUsingPreDiag)
   {
      PreDiagTransform(aIn, aOut, aI, aJ, aNb);
      mTransformStat.Stop(); // stop timer
      return; 
   }
   
   // do transform according to type
   switch(mType)
   {
      case TRANSFORMER::VCC:
         VccTrans(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCCJAC:
         VccTransJac(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::LVCCJAC:
         VccTransJac(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCI:
         HTrans(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCIRSP:
         VciRspTrans(aIn, aOut, aI, aJ, aNb);
         break;
      default:
         MIDASERROR(" TransformerV3::Transform(): mType not implemented.");
   }
}

/**
 * transform an input TensorDataCont and save result in output DataCont
 * @param aIn          input TensorDataCont to be transformed
 * @param aOut         output TensorDataCont, afterwards holds transformed result
 * @param aI           switch parameter
 * @param aJ           switch parameter
 * @param aNb          for some transformations return energy (e.g. VCC)
 * @param arDecompThresholds     Decomposition thresholds for each MC
 * @param arLaplaceQuad          LaplaceQuadrature%s for inverse 0th-order Jacobian transform
 **/
void TransformerV3::Transform
   (  const TensorDataCont& aIn
   ,  TensorDataCont& aOut
   ,  In aI
   ,  In aJ
   ,  param_t& aNb
   ,  const std::vector<param_t>& arDecompThresholds
   ,  const std::vector<LaplaceQuadrature<param_t>>& arLaplaceQuad
   )
{
   mTransformStat.Start(); // start timer
   scope_on_exit{ mTransformStat.Stop(); }; // stop timer on scope exit
   
   // HACK: We convert here for now. No biggie as, if Xvec's are already in TensorDataCont format, it will not do anything
   mXvec.ConvertToTensorDataCont();
   mTransXvec.ConvertToTensorDataCont();

   switch(mType)
   {
      case TRANSFORMER::VCC:
         VccTrans(aIn, aOut, aI, aJ, aNb, arDecompThresholds);
         break;
      case TRANSFORMER::VCCJAC:
         VccTransJac(aIn, aOut, aI, aJ, aNb, arDecompThresholds, arLaplaceQuad);
         break;
      case TRANSFORMER::LVCCJAC:
         VccTransJac(aIn, aOut, aI, aJ, aNb, arDecompThresholds, arLaplaceQuad);
         break;
      default:
         MIDASERROR(" TransformerV3::Transform(): mType = " + std::to_string(mType) + " not implemented.");
   }
}

/**
 * prepare transformer to do prediagonalization transforms
 **/
void TransformerV3::PreparePreDiag
   (
   )
{
   mPreDiagTransformer=std::unique_ptr<Transformer>(this->Clone());
   mPreDiagTransformer->TrfUsingPreDiag(false);
   switch(mType)
   {
      case TRANSFORMER::VCC:
      {
         //Mout << " In PreparePreDiag - Use VCC Jacobian as transformer " << endl; 
         dynamic_cast<TransformerV3*>(mPreDiagTransformer.get())->SetType(TRANSFORMER::VCCJAC);
         break; 
      }
      default:
         MIDASERROR(" TransformerV3::PreparePreDiag -  mType not implemented."+TRANSFORMER_MAP::MAP.at(mType));
   }
}

/**
 * Make prediagonalization transform 
 */
void TransformerV3::PreDiagTransform
   ( DataCont& arIn
   , DataCont& arOut
   , In aI
   , In aJ
   , param_t& aNb
   )
{
   //Mout << " In TransformerV3::PreDiagTransform " << endl; 
   //Mout << " mTrfUsingPreDiag " << mTrfUsingPreDiag <<  endl;
   //Mout << " mType " << TRANSFORMER_MAP::MAP.at(mType) <<  endl;
   switch(mType)
   {
      case TRANSFORMER::VCC:
      {
         //Mout << " Make a PreDiagTransform - use VCI transform " << endl;  /// use rsp for vcc 
         //Mout << " Make a PreDiagTransform - use VCCJAC transform " << endl;  /// use rsp for vcc 
         std::unique_ptr<Transformer> trans(this->Clone()); 
         dynamic_cast<TransformerV3*>(trans.get())->SetType(TRANSFORMER::VCCJAC); 
         trans->TrfUsingPreDiag(false);
         //Mout << " Making reassigns In " << endl; 
         //param_t zero=C_0; 
         // TEST containers... 
         //DataCont in(mXvec.GetXvecData()); 
         //DataCont out(mTransXvec.GetXvecData()); 
         //trans->Transform(in, out, I_1, aJ , aparam_t); // enforce aI=I_1 
         //Mout << "arIn norm PreDiagTransform " << arIn.Norm() << " aNb " << aNb << endl; 
         trans->Transform(arIn, arOut, I_1, aJ, aNb); // enforce aI=I_1, aJ=
         //mPreDiagTransformer->Transform(arIn, arOut, I_1, aJ, aNb); // enforce aI=I_1, aJ=
         //Mout << " Making reassigns Out " << endl; 
         //Mout << " Check energy " << mpVcc->GetEtot() << endl; 
         //out.Axpy(in,-mpVcc->GetEtot());
         //Mout << "out norm " << out.Norm() << endl; 
         break;
      }
      default:
         MIDASERROR(" TransformerV3::Transform(): mType not implemented.");
   }
}

/**
 * check V3 transformation method type
 * @param aFilename      method to check
 * @return               true if tmethod has been checked, else false
 **/
bool TransformerV3::CheckMethod
   ( const string& aFilename
   ) const
{
   if(mpVccCalcDef->CheckMethod())
   {
      string file(aFilename);
      transform(file.begin(), file.end(), file.begin(), (int(*)(int))tolower);
      
      if (mTestedMethods.find(file) != mTestedMethods.end())
         return true;

      MIDASERROR(" Error:"
               , " The method identified by the filename: '",aFilename,"'"
               , " has not been tested and verified to give correct results."
               , " You may perform this calc. by adding '#3 DISABLECHECKMETHOD' under '#2 Vcc'"
               );
   }
   return false;
}

/**
 *
 **/
void TransformerV3::CtrProdsToV3(vector<CtrProd>& aProds, bool aVerbose)
{
   if (aVerbose)
      Mout << "    Translating contraction products to V3 intermediate products." << endl
           << "    # contraction products: " << aProds.size() << " (one . per 10 products):"
           << endl << "    ";

   for (In i=I_0; i<aProds.size(); ++i)
   {
      if (aVerbose && (i+I_1)%10 == I_0)
         Mout << "." << std::flush;
      ostringstream os;
      aProds[i].WriteV3ContribSpec(os);
      mContribs.push_back(v3contrib_t::Factory(os.str()));
   }
   if (aVerbose)
      Mout << endl;
}

/**
 * identify combined intermediates to reduce computational scaling of transformations
 * @param aVerbose                do verbose output
 **/
void TransformerV3::IdentifyCmbIntermeds
   ( bool aVerbose
   )
{
   Timer timer;
   if (aVerbose)
   {
      Mout << " Identifying intermediates for reducing scaling." << endl;
   }

   bool cp_tensors = !mDecompInfo.empty() && this->mUseCpScalingForIntermeds;

   v3::Scaling init_scaling;
   v3::Scaling max_sc;
   for (In i=I_0; i<mContribs.size(); ++i)
   {
      // For right-hand transformations.
      if (  auto* ip = dynamic_cast<v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>*>(mContribs[i].get())
         )
      {
         v3::Scaling sc = ip->GetScaling(cp_tensors);
         if (sc > init_scaling)
            init_scaling = sc;
         ip->IdentifyCmbIntermeds(sc, cp_tensors);
         if (sc > max_sc)
            max_sc = sc;
      }

      // For left-hand transformations.
      if (  auto* ipl = dynamic_cast<v3::IntermedProdL<param_t, midas::vcc::v3::VccEvalData>*>(mContribs[i].get())
         )
      {
         v3::Scaling sc = ipl->GetScaling(cp_tensors);
         if (sc > init_scaling)
            init_scaling = sc;
         ipl->IdentifyCmbIntermeds(sc, cp_tensors);
         if (sc > max_sc)
            max_sc = sc;
      }
   }

   if (aVerbose)
   {
      Mout << "    Max scaling before: " << init_scaling << endl
           << "    Max scaling after:  " << max_sc << " (including intermediates)." << "\n\n";
      Mout << "    Time used in identifying combined intermediates: \n";
      timer.CpuOut(Mout, "       CpuTime : ",true);
      timer.CpuOut(Mout, "       WallTime: ",true);
   }
   ChangeOutputStatus(mType, mpVccCalcDef->V3type());
}

/**
 *
 **/
void TransformerV3::ListContribs()
{
   Mout << " V3 contributions:" << endl;
   for (In i=I_0; i<mContribs.size(); ++i)
      Mout << "    i: " << setw(4) << i << "   " << *mContribs[i] << endl;
   Mout << " Total # of contributions: " << mContribs.size() << endl;
}

/**
 *
 **/
void TransformerV3::PrintType(std::ostream& aOs) const
{
   LineDelimeter2(aOs);
   aOs << " Type  : " << TRANSFORMER_MAP::MAP.at(mType) << " ("<<mType<<")\n"
       << " V3Type: " << V3_MAP::MAP.at(mpVccCalcDef->V3type()) << " (" << mpVccCalcDef->V3type() << ")\n";
   LineDelimeter2(aOs);
}

/**
 *
 **/
void TransformerV3::ScreeningOutput(std::ostream& aOs) const
{
   unsigned long int screened=I_0;
   unsigned long int not_screened=I_0;
   unsigned long int total=I_0;
   for(In i=I_0; i<mContribs.size(); ++i)
   {
      screened+=mContribs[i]->Screened();
      not_screened+=mContribs[i]->NotScreened();
      total+=mContribs[i]->Total();
   }
   
   aOs << " Screening Summary \n";
   LineDelimeter2(aOs);
   aOs << " Terms screened     : " << screened << "\n"
       << " Terms not screened : " << not_screened << "\n"
       << " Terms int total    : " << total << "\n";
   LineDelimeter2(aOs);
}

/**
 *
 **/
void TransformerV3::TransformStatOutput(std::ostream& aOs) const
{
   midas::stream::ScopedPrecision(5, aOs);
   aOs << " Timings:\n";
   LineDelimeter2(aOs);
   aOs << " Transforms: " << mTransformStat.Calls() << "\n"
       << " CPU  Time : " << mTransformStat.TotCpuTime()  << " CPU  Time per Transform: " << mTransformStat.CpuTimePerCall()  << "\n"
       << " Wall Time : " << mTransformStat.TotWallTime() << " Wall Time per Transform: " << mTransformStat.WallTimePerCall() << "\n";
   LineDelimeter2(aOs);
}

/**
 *
 **/
void TransformerV3::ClearContribs()
{
   mContribs.clear();
}

/**
 *
 **/
bool TransformerV3::CheckOutputStatus(In aType, In aV3Type)
{
   if(mOutputStatus[int(std::log2(aType))][int(std::log2(aV3Type))] == C_0)
   {
      return true;
   }
   return false;
}

/**
 *
 **/
void TransformerV3::ChangeOutputStatus(In aType, In aV3Type)
{
   if (aType==0) MIDASERROR(" aType == 0 in ChangeOutputStatus !!??"); 
   if (aV3Type==0) MIDASERROR(" aV3Type == 0 in ChangeOutputStatus !!??"); 
   mOutputStatus[int(std::log2(aType))][int(std::log2(aV3Type))] = C_1;
}

/**
 *
 **/
void TransformerV3::InitGenVci()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCI with general parameters:" << endl
           << "    H dim:                  " << mpOpDef->McLevel() << endl
           << "    Max. excitation level:  " << mMaxExciLevel << endl;
  
   ClearContribs();
   const In max_exci = mMaxExciLevel;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCI[" << max_exci << "]_H" << mpOpDef->McLevel() << ".v3c";
   CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
 
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << endl;

      // Generate list of contraction products.
      vector<BasicOperProd> bops;
      vector<CtrProd> ctr_prods;
      BasicOperProd::GenerateVci(mpOpDef->McLevel(), max_exci, bops);
      Mout << "    Basic operator products:" << endl;
      for (In i=I_0; i<bops.size(); ++i)
      {
         Mout << "       " << bops[i] << endl;
         bops[i].GetCtrProds(I_0, max_exci, ctr_prods);
      }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for CtrProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   
   this->RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 *
 **/
void TransformerV3::InitGenVcc()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC with general parameters:" << endl
           << "    H dim:                  " << mpOpDef->McLevel() << endl
           << "    Max. excitation level:  " << mMaxExciLevel << endl
           << "    Using T1 transformed H: " << StringForBool(mpVccCalcDef->T1TransH()) << endl;
   
   if (mpVccCalcDef->V3latex())
      midas::vcc::v3::LatexTableGenVcc(this->mpVccCalcDef, this->mpOpDef);
   
   ClearContribs();
   const In max_exci = mMaxExciLevel;
   const In min_exci = mpVccCalcDef->T1TransH() ? I_2 : I_1;

   // Generate name of data file.
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[" << max_exci << "]_H" << mpOpDef->McLevel();
   if (mpVccCalcDef->T1TransH())
      ss_v3c_file << "-T1";
   ss_v3c_file << ".v3c";
   CheckMethod(ss_v3c_file.str());
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
 
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      clock_t t0 = clock();
      Mout << " Generating list of V3 contributions from scratch." << endl;

      // Generate list of contraction products.
      vector<CtrProd> ctr_prods;
      for (In h_dim=I_1; h_dim<=mpOpDef->McLevel(); ++h_dim)
      {
         Commutator h_oper;
         h_oper.SetCoef(C_1);
         h_oper.AddOper(OP::H, h_dim);
         vector<Commutator> bch;
         h_oper.BCH(min_exci, max_exci, max_exci, bch);
         Mout << "    H" << h_dim << " -> " << bch.size() << " commutators: ";
         for (In i=I_0; i<bch.size(); ++i)
         {
            Mout << "." << std::flush;
            bch[i].GetCtrProds(I_0, max_exci, ctr_prods, true);
         }
         Mout << endl;
      }

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
      Mout << "    Time for CtrProd gen.: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 *
 **/
void TransformerV3::InitVCC1pt2()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[1pt2] (for 2-mode Hamiltonian.)" << endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2 or T1TransH == false or dimension of H > 2");

   ClearContribs();
   string v3c_file = mpVccCalcDef->V3FilePrefix() + "VCC[1pt2].v3c";
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1"));
      cmts.push_back(Commutator(C_1, "H1T2"));
      cmts.push_back(Commutator(C_1, "H2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_1, ctr_prods);
     
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1,     "H2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_2, ctr_prods);

      CtrProdsToV3(ctr_prods);
      mContribs.push_back(v3contrib_t::Factory("FT 'T2'")); 
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   RegisterIntermeds();

   if (  this->mpVccCalcDef->IoLevel()>I_10  )
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 *
 **/
void TransformerV3::InitVCC2()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2] (for 2-mode Hamiltonian.)" << endl;

   if (mMaxExciLevel != I_2 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_2)
      MIDASERROR("MaxExci != 2 or T1TransH == false or dimension of H > 2");

   ClearContribs();
   string v3c_file = mpVccCalcDef->V3FilePrefix() + "VCC[2]-Explicit.v3c";
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_2|
      cmts.push_back(Commutator(C_1,     "H1"));
      cmts.push_back(Commutator(C_1,     "H2"));
      cmts.push_back(Commutator(C_1,     "H1T2"));
      cmts.push_back(Commutator(C_1,     "H2T2"));
      cmts.push_back(Commutator(C_1/C_2, "H2T2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_2, ctr_prods);

      CtrProdsToV3(ctr_prods);
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   RegisterIntermeds();
   if (  this->mpVccCalcDef->IoLevel() > I_10   )
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }
}

/**
 *
 **/
void TransformerV3::InitVCC2pt3()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[2pt3] (up to 3-mode Hamiltonian.)" << endl;

   if (mMaxExciLevel != I_3 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_3)
      MIDASERROR("MaxExci != 3 or T1TransH == false or dimension of H > 3");

   ClearContribs();
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[2pt3]_H" << mpOpDef->McLevel();
   ss_v3c_file << ".v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << endl;
      vector<Commutator> cmts;
      vector<CtrProd> ctr_prods;
      
      // Commutators only present up to and including <mu_1|
      cmts.push_back(Commutator(C_1, "H1"));
      cmts.push_back(Commutator(C_1, "H3T3"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_1, ctr_prods);
     
      // Commutators only present up to and including <mu_2|
      cmts.clear();
      cmts.push_back(Commutator(C_1,     "H1T2"));
      cmts.push_back(Commutator(C_1,     "H1T3"));
      cmts.push_back(Commutator(C_1/C_2, "H1T2T2"));
      cmts.push_back(Commutator(C_1,     "H2"));
      cmts.push_back(Commutator(C_1,     "H2T3"));
      cmts.push_back(Commutator(C_1/C_2, "H2T2T2"));
      cmts.push_back(Commutator(C_1,     "H3T2"));
      cmts.push_back(Commutator(C_1/C_2, "H3T2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_2, ctr_prods);

      // Commutators present for all excitation levels.
      cmts.clear();
      cmts.push_back(Commutator(C_1, "H3"));
      cmts.push_back(Commutator(C_1, "H2T2"));
      for (In i=I_0; i<cmts.size(); ++i)
         cmts[i].GetCtrProds(I_0, I_3, ctr_prods);

      CtrProdsToV3(ctr_prods);
      
      mContribs.push_back(v3contrib_t::Factory("FT 'T3'")); 
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3T3'")); 
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();
   
   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }

   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 *
 **/
void TransformerV3::InitVCC3pt4F()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4F] (up to 4-mode Hamiltonian.)" << endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4 or T1TransH == false or dimension of H > 4");

   ClearContribs();
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[3pt4F]_H" << mpOpDef->McLevel();
   ss_v3c_file << ".v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << endl;
   
      vector<CtrProd> ctr_prods;
      // Generate <mu_0| to <mu_2| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
      {
         Commutator h_oper;
         h_oper.SetCoef(I_1);
         h_oper.AddOper(OP::H, h);
         vector<Commutator> bch;
         h_oper.BCH(I_2, I_4, I_4, bch);
         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_0, I_2, ctr_prods, true);
         
         for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
         {
            Commutator comm = *q;
            if ((comm.ContainsOper(OP::T, I_4)) && (comm.MaxPtOrder() > I_4 ))
               q = bch.erase(q) -I_1;
         } 
         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_3, I_3, ctr_prods, true);

         for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
         {
            Commutator comm = *q;
            if (comm.MaxPtOrder() > I_3 )
               q = bch.erase(q) - I_1;
         } 

         // Delete terms that will be overcounted
         if (h == I_1) 
         {
            vector<Commutator> cmts;
            cmts.push_back(Commutator(C_1, "H1T4"));
            cmts.push_back(Commutator(C_1, "H1T2T3"));
            DeleteCommutatorInVec(cmts,bch);
         }

         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_4, I_4, ctr_prods, true);
      }

      CtrProdsToV3(ctr_prods);

      mContribs.push_back(v3contrib_t::Factory("FT 'T4'"));
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3T4"));
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4T4")); //H4 contribution for mu_3
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }

   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 *
 **/
void TransformerV3::InitVCC3pt4()
{
   if(CheckOutputStatus(mType, mpVccCalcDef->V3type()))
      Mout << " Initializing V3 transformer for VCC[3pt4] (up to 4-mode Hamiltonian.)" << endl;

   if (mMaxExciLevel != I_4 ||
       mpVccCalcDef->T1TransH() != true ||
       mpOpDef->McLevel() > I_4)
      MIDASERROR("MaxExci != 4 or T1TransH == false or dimension of H > 4");

   ClearContribs();
   ostringstream ss_v3c_file;
   ss_v3c_file << "VCC[turbo]_H" << mpOpDef->McLevel();
   ss_v3c_file << ".v3c";
   string v3c_file = mpVccCalcDef->V3FilePrefix() + ss_v3c_file.str();
   if (! v3contrib_t::ReadV3cFile(v3c_file, mContribs))
   {
      Mout << " Generating list of V3 contributions from scratch." << endl;
   
      vector<CtrProd> ctr_prods;
      // Generate <mu_0| to <mu_1| equations.
      for (In h=I_1; h<=mpOpDef->McLevel(); ++h)
      {
         Commutator h_oper;
         h_oper.SetCoef(I_1);
         h_oper.AddOper(OP::H, h);
         vector<Commutator> bch;
         h_oper.BCH(I_2, I_4, I_4, bch);
         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_0, I_1, ctr_prods, true);
         
         //generate <mu_2| equations.
         for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
         {
            Commutator comm = *q;
            if ((comm.ContainsOper(OP::T, I_4) && (comm.MaxPtOrder() > I_5 )) || (comm.ContainsOper(OP::H,I_4) && comm.ContainsOper(OP::T,I_3)))
               q = bch.erase(q) -I_1;
         } 
         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_2, I_2, ctr_prods, true);

         for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
         {
            Commutator comm = *q;
            if ((comm.ContainsOper(OP::T, I_4) && (comm.MaxPtOrder() > I_4 )) || (comm.ContainsOper(OP::H,I_4) && comm.ContainsOper(OP::T,I_3)))
               q = bch.erase(q) -I_1;
         } 
         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_3, I_3, ctr_prods, true);

         for (vector<Commutator>::iterator q = bch.begin(); q != bch.end(); q++) 
         {
            Commutator comm = *q;
            if (comm.MaxPtOrder() > I_3 )
               q = bch.erase(q) - I_1;
         } 

         // Delete terms that will be overcounted
         if (h == I_1) 
         {
            vector<Commutator> cmts;
            cmts.push_back(Commutator(C_1, "H1T4"));
            cmts.push_back(Commutator(C_1, "H1T2T3"));
            DeleteCommutatorInVec(cmts,bch);
         }

         for (In i=I_0; i<bch.size(); ++i)
            bch[i].GetCtrProds(I_4, I_4, ctr_prods, true);
      }

      CtrProdsToV3(ctr_prods);

      mContribs.push_back(v3contrib_t::Factory("FT 'T4'"));
      mContribs.push_back(v3contrib_t::Factory("FndT 'F3T4"));
      if (mpOpDef->McLevel() > I_3)
      {
         mContribs.push_back(v3contrib_t::Factory("FndT 'F4T4")); //H4 contribution for mu_3
      }
      v3::IntermedProd<param_t, midas::vcc::v3::VccEvalData>::WriteV3cFile(v3c_file, mContribs);
   }
   IdentifyCmbIntermeds(CheckOutputStatus(mType, mpVccCalcDef->V3type()));
   RegisterIntermeds();

   if (mpVccCalcDef->IoLevel()>I_10)
   {
      ListContribs();
      this->ListRegisteredIntermeds();
   }

   mFockInts = midas::vcc::modalintegrals::InitFockFromVcc(*mpVcc, mpVcc->Name()+"_fock_integrals");
}

/**
 *
 **/
void TransformerV3::HTrans(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t aNb)
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");

   clock_t t0 = clock();
   if (mLambdaCalc)
      MIDASERROR("TransformerV3::HTrans(): Lambda calculation not supported.");

   mXvec.GetXvecData() = arDcIn;
   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mXvec.GetXvecData());

   if (I_0 == aI)
   {
      // Transform with diagonal.
      if (mpVccCalcDef->GetTrueHDiag())
         TransformerTrueHDiag(aJ, aNb);
      else
         TransformerH0(aJ, aNb);
   }
   else if (I_1 == aI)
   {
      // Perform full transformation.
      this->mImMachine->ClearIntermeds();
      evaldata_t eval_data(this,&mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt());
      eval_data.AssignDataVec(OP::T, &(mXvec.GetXvecData()));
      EvaluateContribsNew(eval_data, mTransXvec.GetXvecData());
   }
   else
   {
      MIDASERROR("TransformerV3::HTrans(): Unknown aI" + std::to_string(aI));
   }
   
   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mTransXvec.GetXvecData());
   arDcOut = mTransXvec.GetXvecData();
   
   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::HTrans() time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
}

/**
 *
 **/
void TransformerV3::VciRspTrans(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, param_t& aNb)
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   clock_t t0 = clock();

   param_t refcomp = aNb;              // Reference component.
  
   // This is kind of a hack to recognize if we are calculating the VCI eta vector.
   // Copied from original implementation if VccTransformer.
   bool etavec = false;
   if (aNb > C_0)
      etavec = true;
 
   // Transform to orthogonal complement basis unless diagonal transformation
   // is requested.
   if (aI != I_0)
      PrimOrtoTrans(arDcIn, mXvec.GetXvecData(), refcomp, I_1);

   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mXvec.GetXvecData());

   if (aI == I_0)
   {
      // Transform using diagonal.
      arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
      param_t one = C_1;
      mXvec.GetXvecData().DataIo(IO_PUT, I_0, one);         // Take care of reference component.
      if (mpVccCalcDef->GetTrueHDiag())
         TransformerTrueHDiag(aJ, aNb);
      else
         TransformerH0(aJ, aNb);
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);
   }
   else if (aI == I_1)
   {
      // Do full transformation with Hamiltonian.
      this->mImMachine->ClearIntermeds();
      evaldata_t eval_data(this, &mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt());
      eval_data.AssignDataVec(OP::T, &(mXvec.GetXvecData()));
      EvaluateContribsNew(eval_data, mTransXvec.GetXvecData());
   }
   else
   {
      MIDASERROR("TransformerV3::VciRspTrans(): Unknown aI parameter.");
   }

   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mTransXvec.GetXvecData());

   if (aI != I_0)
      PrimOrtoTrans(arDcOut, mTransXvec.GetXvecData(), aNb, -I_1);

   string opname = mpOpDef->Name();
   param_t exptval = C_0;
   if (opname == mpVcc->pOpDefOrig()->Name())
   {
      exptval = mpVcc->GetRspRefE();
   }
   else if (!etavec)
   {
      for (In i=I_0; i<mpVccCalcDef->NrspOps(); ++i)
         if (mpVccCalcDef->GetRspOp(i) == opname)
         {
            exptval = mpVccCalcDef->GetRspOpExpVal(i);
            Mout << "Expectation value for operator: " << opname << " = " << exptval
                 << " will be subtracted from result vector." << endl
                 << "Oper from which value has been taken: " << mpVccCalcDef->GetRspOp(i)
                 << endl;
            break;
         }
   }
  
   if (aI==I_0)
      exptval = -exptval;
   arDcOut.Axpy(arDcIn, -exptval);    // - <ref| oper |ref> * delta_ij * c_j.
   
   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::HTrans() time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
}

/**
 * Do Vcc transformations on DataCont
 * @param arDcIn
 * @param arDcOut
 * @param aI
 * @param aJ
 * @param aNb
 **/
void TransformerV3::VccTrans
   (  DataCont& arDcIn
   ,  DataCont& arDcOut
   ,  In aI
   ,  In aJ
   ,  param_t& aNb
   )
{
   MidasAssert(!this->mTensorTransform, "This function should only be called when using DataCont. Otherwise mImMachine will not be ready!");
   //cout << " doing vcc trans " << endl;
   clock_t t0 = clock();

   // arDcIn contains amplitudes. mXvec also contains the reference at index 0.
   arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
   param_t one = C_1;
   mXvec.GetXvecData().DataIo(IO_PUT, I_0, one);     // Reference coefficient.

   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the input.
   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mXvec.GetXvecData());
   
   if (mpVccCalcDef->T1TransH())
      UpdateModalIntegrals(mXvec.MaxExciLevel() ? true : false, &mXvec);
   else
      UpdateModalIntegrals(false);
   
   // aI controls diagonal.
   if (I_0 == aI)
   {
      if (mpVccCalcDef->GetTrueHDiag())
         TransformerTrueHDiag(aJ, aNb);
      else
         TransformerH0(aJ, aNb);
      mTransXvec.GetXvecData().DataIo(IO_GET,I_0,aNb);
      arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1, -aNb);
      return;
   }

   // Perform evaluation of error vector.
   this->mImMachine->ClearIntermeds();
   
   evaldata_t eval_data
      (  this
      ,  &mTransXvec
      ,  mpVccCalcDef
      ,  mpVcc
      ,  mpOpDef
      ,  pOneModeModalBasisInt()
      ,  &mFockInts
      );
   eval_data.AssignDataVec(OP::T, &(mXvec.GetXvecData()));
   //EvaluateContribs(eval_data, mTransXvec.GetXvecData());
   EvaluateContribsNew(eval_data, mTransXvec.GetXvecData());
   
   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the output.
   if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mTransXvec.GetXvecData());
   
   // Store result in aNb and arDcOut.
   mTransXvec.GetXvecData().DataIo(IO_GET, I_0, aNb);
   arDcOut.Reassign(mTransXvec.GetXvecData(), I_1, I_1);

   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::VccTrans() time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
   //MIDASERROR("DEBUG STOP!");
}

/**
 * Do Vcc transformations on DataCont
 * @param arDcIn
 * @param arDcOut
 * @param aI
 * @param aJ
 * @param aNb
 * @param arDecompThresholds
 *    Decomposition thresholds for each MC
 **/
void TransformerV3::VccTrans
   (  const TensorDataCont& arDcIn
   ,  TensorDataCont& arDcOut
   ,  In aI
   ,  In aJ
   ,  param_t& aNb
   ,  const std::vector<param_t>& arDecompThresholds
   )
{
   MidasAssert(this->mTensorTransform, "This function should only be called when using tensors. Otherwise mTensorImMachine will not be ready!");

   //cout << " doing vcc trans " << endl;
   clock_t t0 = clock();

   // arDcIn contains amplitudes. mXvec also contains the reference at index 0.
   //arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
   mXvec.Assign(arDcIn, Xvec::load_in, Xvec::add_ref);
   //param_t one = C_1;
   //mXvec.GetXvecData().DataIo(IO_PUT, I_0, one);     // Reference coefficient.

   // Hack to limit sum of quantum numbers in each MC.
   // This cleans the input.
   //if (mpVccCalcDef->SumNprim() && aI==I_1) SumNpurify(mXvec.GetXvecData());
   //if (mpVccCalcDef->Emaxprim() && aI==I_1) Emaxpurify(mXvec.GetXvecData());
   //if (mpVccCalcDef->Zeroprim() && aI==I_1) Zeropurify(mXvec.GetXvecData());
   
   if (mpVccCalcDef->T1TransH())
      UpdateModalIntegrals(mXvec.MaxExciLevel() ? true : false, &mXvec);
   else
      UpdateModalIntegrals(false);
   
   // aI controls diagonal.
   if (I_0 == aI)
   {
      // Niels: Should we perhaps change this such that true diag has its own aI-key?
      if (mpVccCalcDef->GetTrueHDiag())
      {
         this->mXvec.Assign(arDcIn, Xvec::load_in, Xvec::add_ref);  // Load input vector into mXvec. Add ref. 
         this->mXvec.ConvertToDataCont(); // convert to DataCont
         this->mTransXvec.ConvertToDataCont();  // convert to DataCont
         TransformerTrueHDiag(aJ, aNb);   // Transform mXvec and store the result in mTransXvec
         this->mTransXvec.ConvertToTensorDataCont();  // Convert mTransXvec to TensorDataCont
         this->mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);  // Load mTransXvec without ref. into output vector.
      }
      else
      {
         TransformerH0(aJ, aNb);
         aNb = mTransXvec.Ref();
         mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);
         arDcOut.ElementwiseScalarAddition(-aNb);
      }
      return;
   }

   // Perform evaluation of error vector.
   this->mTensorImMachine->ClearIntermeds();

   // DEBUG
   if (  gDebug
      )
   {
      Mout  << " +++ Constructing TensorDecomposer from TensorDecompInfo::Set +++" << std::endl;
      for(const auto& info : mDecompInfo)
      {
         Mout << info << std::endl;
      }
   }

   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   evaldata_t eval_data(this, &mTransXvec, mpVccCalcDef, mpVcc, mpOpDef, pOneModeModalBasisInt(), &mFockInts);
   eval_data.AssignTensorDataVec(OP::T, &(mXvec.GetTensorXvecData()));
   eval_data.AssignDecomposer(&decomposer);
   eval_data.AssignAllowedRank(mAllowedRank);
   EvaluateContribsNew(eval_data, mTransXvec.GetTensorXvecData(), arDecompThresholds);
   
   aNb = mTransXvec.Ref();
   mTransXvec.Assign(arDcOut, Xvec::load_out, Xvec::add_ref);

   if (mpVccCalcDef->TimeIt())
      Mout << " TransformerV3::VccTrans() time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
}

/**
 * evaluate contributions of V3Contribs for tranformation
 * @param aEvalData           data to evaluate for
 * @param aRes                result of evaluation (additively collected)  
 **/
void TransformerV3::EvaluateContribs
   ( evaldata_t& aEvalData
   , DataCont& aRes
   )
{
   aEvalData.CalcNorms2();
   aRes.Zero();
#ifdef VAR_MPI
   int SENDER;
   int job = 0;
   int ALL_DONE = -1;

   // take care of the case where only one cpu has been req.
   if(midas::mpi::GlobalSize() == 1) 
   { // if only 1 mpi process wo do not do master/slave
      for (In i=I_0; i<mContribs.size(); ++i)
      {
      clock_t t0 = clock();
      mContribs[i]->Evaluate(aEvalData, aRes);
      if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
               << "   time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << endl;
      }
   } 
   else if (midas::mpi::GlobalRank() == 0) 
   { // if more mpi preocesses gMpiRank == 0 will become MASTER
      // Setup master 
      aRes.Zero();
      for (In i=I_0; i<mContribs.size(); ++i)
      {
         midas::mpi::detail::WRAP_Recv(&SENDER, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         if(gDebug) printf("Master: Received request from process %i\n", SENDER);
         midas::mpi::detail::WRAP_Send(&i, 1, MPI_INT, SENDER, 0, MPI_COMM_WORLD);
         if(gDebug) printf("Master: Sent job %i to process %i\n", i, SENDER);
      }
      for (int j = 1; j < midas::mpi::GlobalSize(); j++)
      {
         midas::mpi::detail::WRAP_Recv(&SENDER, 1, MPI_INT, j, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         midas::mpi::detail::WRAP_Send(&ALL_DONE, 1, MPI_INT, j, 0, MPI_COMM_WORLD);
      }
      /* When no more jobs, Master receives slave contributions and 
         send back a complete vector: */
      for (int j = 1; j < midas::mpi::GlobalSize(); j++)
      {
         DataCont buf = aRes;
         buf.Zero();
         midas::mpi::detail::WRAP_Recv(buf.GetVector()->data(), aRes.Size(), MPI_DOUBLE, j, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         aRes.Axpy(buf,C_1);
      }
      //MPI_Barrier(MPI_COMM_WORLD);
      for (int j = 1; j < midas::mpi::GlobalSize(); j++)
      {
         midas::mpi::detail::WRAP_Send(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, j, 0, MPI_COMM_WORLD);
      }
   } 
   else 
   { // and gMpiRank != 0 will become SLAVES
      // setup slaves
      while (1)
      {
         if(gDebug) printf("Process %i; will request job\n", midas::mpi::GlobalRank());
         auto rank = midas::mpi::GlobalRank();
         midas::mpi::detail::WRAP_Send(&rank, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
         midas::mpi::detail::WRAP_Recv(&job, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         if (job == ALL_DONE){
            if(gDebug) printf("Process %i, received all jobs done!\n", midas::mpi::GlobalRank());
            break;
         }
         if(gDebug) printf("Process %i; will calculate on contrib %i\n", midas::mpi::GlobalRank(), job);
         clock_t tc = clock();
         mContribs[job]->Evaluate(aEvalData, aRes);
   
         if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5){
            Mout << " i: " << setw(4) << job << "   " << *mContribs[job]
                 << "   time: " << param_t(clock()-tc)/CLOCKS_PER_SEC << " s" << endl;
         }
      }
      /* Slaves send their contribution and receive complete vector from master: */
      midas::mpi::detail::WRAP_Send(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
      midas::mpi::detail::WRAP_Recv(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
   }
   midas::mpi::Barrier();
#else /* VAR_MPI */
   // not mpi, just loop over contributions and evaluate
   for (In i=I_0; i<mContribs.size(); ++i)
   {
      clock_t t0 = clock();
      mContribs[i]->Evaluate(aEvalData, aRes);
      if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
              << "   time: "  << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
#endif /* VAR_MPI */
   return;
}

/**
 *
 **/
void TransformerV3::EvaluateContribsNew
   (  evaldata_t& aEvalData
   ,  DataCont& aRes
   )
{
   // Some initialization
   aEvalData.CalcNorms2();
   aRes.Zero();
   
   MidasVector res_vec;
   const ModeCombiOpRange& mcr_p = aEvalData.GetModeCombiOpRange();

#ifndef VAR_MPI
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp) // loop over mode combinations in mcr
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      
      // Setup correct size for result vector
      In n_exci = aEvalData.NExciForModeCombi(i_mp); 
      res_vec.SetNewSize(n_exci);
      res_vec.Zero();
      
      // loop over contributions
      for (In i=I_0; i<mContribs.size(); ++i)
      {
         // check that modecombi hase same number of modes as intermed prod returns
         if ( mp.Size() != mContribs[i]->ExciLevel() ) 
         {
            continue; // else we continue loop over mode combination
         }

         // then evaluate contribution for current MC
         clock_t t0 = clock();
         mContribs[i]->Evaluate(aEvalData, mp, res_vec);
         if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         {
            Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
                 << "    time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
         }
      }
      
      // Add result to arDcOut.
      aRes.DataIo(IO_PUT, res_vec, res_vec.Size(), mp.Address(), I_1, I_0, I_1, false, C_0, true);
   }
#else
   // Calculate contributions for each rank.
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp) // loop over mode combinations in mcr
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if(midas::mpi::GlobalRank() != i_mp % midas::mpi::GlobalSize()) 
      {
         continue;
      }
      
      // Setup correct size for result vector
      In n_exci = aEvalData.NExciForModeCombi(i_mp); 
      res_vec.SetNewSize(n_exci);
      res_vec.Zero();
      
      // loop over contributions
      for (In i=I_0; i<mContribs.size(); ++i)
      {
         // check that modecombi hase same number of modes as intermed prod returns
         if ( mp.Size() != mContribs[i]->ExciLevel() ) 
         {
            continue; // else we continue loop over mode combination
         }

         // then evaluate contribution for current MC
         clock_t t0 = clock();
         mContribs[i]->Evaluate(aEvalData, mp, res_vec);
         if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         {
            Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
                 << "    time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
         }
      }
      
      // Add result to arDcOut.
      aRes.DataIo(IO_PUT, res_vec, res_vec.Size(), mp.Address(), I_1, I_0, I_1, false, C_0, true);
   }
   

   // Then let master rank collect all results and broadcast full result out to all other ranks.
   if(midas::mpi::IsMaster())
   {
      // Master receives all part-results from slaves
      for (int j = 1; j < midas::mpi::GlobalSize(); j++)
      {
         DataCont buf = aRes;
         buf.Zero();
         midas::mpi::detail::WRAP_Recv(buf.GetVector()->data(), aRes.Size(), MPI_DOUBLE, j, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         aRes.Axpy(buf,C_1);
      }

      // Then broadcasts out result.
      midas::mpi::detail::WRAP_Bcast(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
   }
   else
   {
      // SLaves sends back part result
      midas::mpi::detail::WRAP_Send(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);

      // Then receives complete vector from master
      midas::mpi::detail::WRAP_Bcast(aRes.GetVector()->data(), aRes.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
   }

#endif /* VAR_MPI */
   
   // Unset some stuff
   return;
}

/**
 * evaluate contributions of V3Contribs for tranformation
 * @param aEvalData           data to evaluate for
 * @param aRes                result of evaluation (additively collected)  
 **/
void TransformerV3::EvaluateContribs
   (  evaldata_t& aEvalData
   ,  TensorDataCont& aRes
   )
{
   aEvalData.CalcNorms2();
   aRes.Zero();
   // not mpi, just loop over contributions and evaluate
   for (In i=I_0; i<mContribs.size(); ++i)
   {
      clock_t t0 = clock();
      mContribs[i]->Evaluate(aEvalData, aRes);
      if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
              << "   time: "  << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   return;
}

/**
 * Evaluate contributions of V3Contribs for tranformation
 * @param aEvalData              Data to evaluate for
 * @param aRes                   Result of evaluation (additively collected)  
 * @param arDecompThresholds     Individual decomposition thresholds for each MC residual
 **/
void TransformerV3::EvaluateContribsNew
   (  evaldata_t& aEvalData
   ,  TensorDataCont& aRes
   ,  const std::vector<param_t>& arDecompThresholds
   )
{
   aEvalData.CalcNorms2();
   aRes.Zero();
   
   const ModeCombiOpRange& mcr_p = aEvalData.GetModeCombiOpRange();
   
   const auto& mcr_size = mcr_p.Size();

   const bool individual_thresh = !arDecompThresholds.empty();
   if (  individual_thresh
      && arDecompThresholds.size() != aEvalData.NExciTypes()-I_1
      )
   {
      MIDASERROR("Size mismatch between number of excitation types and number of decomposition thresholds!");
   }

   // Copy original screening
   const bool adapt_screen = this->mAdaptiveTensorProductScreening > C_0 && !this->mDecompInfo.empty();
   const auto screen_ref = this->mTensorProductScreening;

#ifndef VAR_MPI
   // loop over mode combinations in mcr
   // not mpi, just loop over contributions and evaluate
   for (In i_mp = I_0; i_mp < mcr_size; ++i_mp) 
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);

      auto thresh = this->mDecompInfo.empty() ? -C_1 : midas::tensor::GetDecompThreshold( this->mDecompInfo );

      if (  individual_thresh
         && i_mp != I_0
         )
      {
         thresh = arDecompThresholds[i_mp-I_1];

         this->UpdateDecompThresholds( thresh );
      }

      if (  adapt_screen
         )
      {
         param_t new_screen = thresh * this->mAdaptiveTensorProductScreening;
         this->mTensorProductScreening.first = std::max(screen_ref.first, new_screen);
         this->mTensorProductScreening.second = std::max(screen_ref.second, new_screen);
      }

      if (  gDebug   
         || mpVccCalcDef->IoLevel() >= I_10
         )
      {
         Mout << "Initializing TensorSumAccumulator for mode combi " << i_mp << " with threshold " << thresh << std::endl;
      }

      // Construct TensorSumAccumulator
      midas::vcc::TensorSumAccumulator<param_t> res_vec
                                 (  aEvalData.GetTensorDims(i_mp)
                                 ,  aEvalData.GetDecomposer()
                                 ,  mAllowedRank 
                                 ); 
      
      // loop over contributions
      for (In i=I_0; i<mContribs.size(); ++i)
      {
         // check that modecombi has same number of modes as intermed prod returns
         if (  mp.Size() != mContribs[i]->ExciLevel()
            ) 
         {
            if (  gDebug   
               || mpVccCalcDef->IoLevel() >= I_10
               )
            {
               Mout  << " Skip contrib '" << *mContribs[i] << "' with exci level " << mContribs[i]->ExciLevel() << std::endl;
            }
            continue; // else we continue loop over mode combination
         }
         
         // then evaluate contribution for current MC
         clock_t t0 = clock();
         mContribs[i]->Evaluate(aEvalData, mp, res_vec);
         if (  mpVccCalcDef->TimeIt()
            && mpVccCalcDef->IoLevel() > I_5
            )
         {
            Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
                 << "    time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
            
            // More debug
            Mout  << " res_vec:\n" << res_vec.Tensor().ToSimpleTensor() << std::endl;
         }
      }

      // When all contributions are evaluated we save the result to arDcOut.
      if(mp.Size() == 0) // REMOVE THIS SPECIAL CASE?? IT IS VERY UGLY!!!!!! NB NB NB NB 
                         // Is it actually needed ?
      {
         param_t scalar;
         res_vec.EvaluateSum().GetTensor()->DumpInto(&scalar);
         aRes.GetModeCombiData(i_mp).ElementwiseScalarAddition(scalar);
      }
      else
      {
         aRes.GetModeCombiData(i_mp) = res_vec.EvaluateSum();
      }

//      // DEBUG
//      MidasWarning("Niels: Remove debug output from EvaluateContribsNew!!!");
//      Mout  << " res_vec of norm = " << res_vec.Tensor().Norm() << " before EvaluateSum:\n" << res_vec.Tensor().ToSimpleTensor() << std::endl;
//      Mout  << " aRes after decomp to thresh = " << thresh << " :\n" << aRes.GetModeCombiData(i_mp).ToSimpleTensor() << std::endl;
   }

#else
   // LOOP MPI
   auto nproc = midas::mpi::GlobalSize();
   
   for (In i_mp = I_0; i_mp < mcr_p.Size(); ++i_mp) 
   {
      if(midas::mpi::GlobalRank() != i_mp % nproc) 
      {
         continue;
      }

      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);

      auto thresh = this->mDecompInfo.empty() ? -C_1 : midas::tensor::GetDecompThreshold( this->mDecompInfo );

      if (  individual_thresh
         && i_mp != I_0
         )
      {
         thresh = arDecompThresholds[i_mp-I_1];

         this->UpdateDecompThresholds( thresh );
      }

      if (  adapt_screen
         )
      {
         param_t new_screen = thresh * this->mAdaptiveTensorProductScreening;
         this->mTensorProductScreening.first = std::max(screen_ref.first, new_screen);
         this->mTensorProductScreening.second = std::max(screen_ref.second, new_screen);
      }

      if (  gDebug   )
      {
         Mout << "Initializing TensorSumAccumulator for mode combi " << i_mp << " with threshold " << thresh << std::endl;
      }

      midas::vcc::TensorSumAccumulator<param_t> res_vec  
         (  aEvalData.GetTensorDims(i_mp)
         ,  aEvalData.GetDecomposer()
         ,  mAllowedRank 
         ); 
      
      // loop over contributions
      for (In i=I_0; i<mContribs.size(); ++i)
      {
         // check that modecombi hase same number of modes as intermed prod returns
         if ( mp.Size() != mContribs[i]->ExciLevel() ) 
         {
            continue; // else we continue loop over mode combination
         }
         
         // then evaluate contribution for current MC
         clock_t t0 = clock();
         mContribs[i]->Evaluate(aEvalData, mp, res_vec);
         if (mpVccCalcDef->TimeIt() && mpVccCalcDef->IoLevel()>I_5)
         {
            Mout << " contrib: " << setw(4) << i << "   " << *mContribs[i]
                 << "    time: " << param_t(clock()-t0)/CLOCKS_PER_SEC << " s" << std::endl;
         }
      }

      // When all contributions are evaluated we save the result to arDcOut.
      if(mp.Size() == 0) // REMOVE THIS SPECIAL CASE?? IT IS VERY UGLY!!!!!! NB NB NB NB 
                         // Is it actually needed ?
      {
         param_t scalar;
         res_vec.EvaluateSum().GetTensor()->DumpInto(&scalar);
         aRes.GetModeCombiData(i_mp).ElementwiseScalarAddition(scalar);
      }
      else
      {
         aRes.GetModeCombiData(i_mp) = res_vec.EvaluateSum();
      }
   }

   // Bcast data
   for(In i_mp=I_0; i_mp < mcr_p.Size(); ++i_mp)
   {
      auto rank = i_mp % nproc;
      aRes.GetModeCombiData(i_mp).MpiBcast(rank);
   }

   if constexpr (MPI_DEBUG) 
   {
      auto norm = aRes.Norm();
      midas::mpi::detail::WRAP_Bcast(&norm, 1, midas::mpi::DataTypeTrait<decltype(norm)>::Get(), 0, MPI_COMM_WORLD);

      if(norm != aRes.Norm())
      {
         MIDASERROR("Did not receive correct result vector in transformer.");
      }
   }
#endif /* VAR_MPI */

   // Restore original tensor screening thresholds
   this->mTensorProductScreening = screen_ref;

   return;
}

/**
 *
 **/
void TransformerV3::DeleteCommutatorInVec(const vector<Commutator>& aCmts1, vector<Commutator>& aCmts2)
{
   for (vector<Commutator>::const_iterator p = aCmts1.begin(); p != aCmts1.end(); p++) 
   {
      for (vector<Commutator>::iterator q = aCmts2.begin(); q != aCmts2.end(); q++) 
      {
         if (CompareCommutator(*p,*q))
         {
            q = aCmts2.erase(q) -I_1;
         }
      } 
   }
}

/**
 *
 **/
std::ostream& TransformerV3::Print(std::ostream& ostream) const
{  
   ostream << " TransformerV3 Summary PrintOut: \n";
   LineDelimeter1(ostream);
   PrintType(ostream);
   ScreeningOutput(ostream);
   TransformStatOutput(ostream);
   LineDelimeter1(ostream);
   ostream << "\n";
   return ostream;
}

/**
 *
 **/
Transformer* TransformerV3::Clone() const
{
   TransformerV3* ptr = new TransformerV3(*static_cast<VccTransformer const* const>(this));
   ptr->SetType(mType);
   return ptr;
}

/**
 *
 **/
Transformer* TransformerV3::CloneImprovedPrecon(In aPreconLevel) const
{
   Mout << " Cloning transforver v3 " << std::endl;
   TransformerV3* ptr = new TransformerV3(*this, aPreconLevel);
//   TransformerV3* ptr = new TransformerV3(*static_cast<VccTransformer const* const>(this), aPreconLevel);
   Mout << " setting type " << std::endl;
   ptr->SetType(mType);
   Mout << " returning " << std::endl;
   return ptr;
}

/**
 * Updates the thresholds in mDecompInfo
 * @param arNewThreshold  The new threshold
 **/
void TransformerV3::UpdateDecompThresholds(const param_t& arNewThreshold)
{
   midas::tensor::UpdateDecompThresholds(this->mDecompInfo, arNewThreshold);
}

/**
 * Get the (largest) decomposition threshold in mDecompInfo
 **/
typename TransformerV3::param_t TransformerV3::GetMaxDecompThreshold() const
{
   return midas::tensor::GetDecompThreshold(this->mDecompInfo, false);
}

/**
 * Get tensor type from mDecompInfo
 *
 * @return
 *    typeID of the tensors contained in the input and output TensorDataCont%s
 **/
BaseTensor<typename TransformerV3::param_t>::typeID TransformerV3::GetTensorType() const
{
   if (  this->mDecompInfo.empty()
      )
   {
      return BaseTensor<param_t>::typeID::SIMPLE;
   }
   else
   {
      // Check that all tensors are decomposed
      for(const auto& info : this->mDecompInfo)
      {
         In order = info.at("DECOMPTOORDER").get<In>();
         if(order > I_1 && !(order > this->mMaxExciLevel)) MIDASERROR("TensorDataCont will contain more than one tensor type!");
      }

      // For now we assume all decomposed tensors are CanonicalTensors
      return BaseTensor<param_t>::typeID::CANONICAL;
   }
}

} /* namespace midas::vcc */

