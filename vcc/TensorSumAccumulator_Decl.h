#ifndef TENSORSUMACCUMULATOR_DECL_H_INCLUDED
#define TENSORSUMACCUMULATOR_DECL_H_INCLUDED

#include "tensor/NiceTensor.h"
#include "tensor/TensorDecomposer.h"

#include "util/type_traits/Complex.h"

namespace midas::vcc
{

/**
 * @class TensorSumAccumulator
 **/
template
   <  typename T
   >
class TensorSumAccumulator
{
   public:
      using real_t = midas::type_traits::RealTypeT<T>;

   private:
      std::vector<unsigned> mDimensions;                                   ///< Dimensions of tensor sum. 
      NiceTensor<T> mTensor;                                               ///< NiceTensor to accumulate result in.

      /** @name Members that are only relevant for T=Nb, i.e. for decomposition */
      //!@{
      const midas::tensor::TensorDecomposer* const mDecomposer = nullptr;  ///< Decomposition info in case we are doing decomps.
      In mAllowedRank = I_0;                                               ///< Allowed rank before recompression
      real_t mDecompThreshold = -C_1;                                      ///< Decomp threshold (set such that the threshold from mDecomposer is used as default)
      //!@}

   public:
      ///> Ctor from dimensions, decomposer, allowed rank, and decomp threshold
      TensorSumAccumulator
         (  const std::vector<unsigned>&
         ,  const midas::tensor::TensorDecomposer* const = nullptr
         ,  In = I_0
         ,  real_t = -C_1
         );
      
      ///> operator += (accumulate tensor sums).
      TensorSumAccumulator& operator+=(const NiceTensor<T>&);

      //! Axpy
      void Axpy(const NiceTensor<T>&, const T&);

      ///> Evaluate the sum and return the result.
      NiceTensor<T> EvaluateSum();

      ///> Get reference to Internal tensor
      NiceTensor<T>& Tensor();

      ///> Get const reference to Internal tensor
      const NiceTensor<T>& Tensor() const;

      /** @name Methods that are only relevant for decompositions */
      //!@{

      //! Get reference to internal Decomposer
      template
         <  typename U = T
         >
      const midas::tensor::TensorDecomposer& GetDecomposer
         (  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const
      {
         assert(mDecomposer);
         return *mDecomposer; 
      }

      //! SetDecompThreshold
      template
         <  typename U = T
         >
      void SetDecompThreshold
         (  real_t aThresh
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )
      {
         mDecompThreshold = aThresh;
      }

      //!@}
};

} /* namespace midas::vcc */

#endif /* TENSORSUMACCUMULATOR_DECL_H_INCLUDED */
