/**
 *******************************************************************************
 * 
 * @file    TwoModeIntermeds.h
 * @date    31-01-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refactorization
 *
 * @brief
 *    Organizing intermediates for specific VCC[2] calculation.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TWOMODEINTERMEDS_H_INCLUDED
#define TWOMODEINTERMEDS_H_INCLUDED

#include "vcc/vcc2/TwoModeIntermeds_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "vcc/vcc2/TwoModeIntermeds_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*TWOMODEINTERMEDS_H_INCLUDED*/
