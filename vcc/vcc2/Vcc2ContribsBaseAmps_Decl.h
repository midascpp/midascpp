/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsBaseAmps_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSBASEAMPS_DECL_H_INCLUDED
#define VCC2CONTRIBSBASEAMPS_DECL_H_INCLUDED

// Midas headers.
#include "util/type_traits/Complex.h"
#include "vcc/vcc2/Vcc2ContribsBase.h"

// Forward declarations.
template<typename> class GeneralDataCont;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class contains (additionally to Vcc2ContribsBase) the
    * T-amplitudes (or C-coefficients) used in most contributions (except for
    * the eta-vector calculation), and thus also the ModeCombiOpRange which
    * provides the structure of the T-amps. and like containers.
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2ContribsBaseAmps
      :  public Vcc2ContribsBase<T, OPDEF_T>
   {
      protected:
         using typename Vcc2ContribsBase<T, OPDEF_T>::value_t;
         using typename Vcc2ContribsBase<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2ContribsBase<T, OPDEF_T>::opdef_t;
         using absval_t = midas::type_traits::RealTypeT<value_t>;
         using datacont_t = GeneralDataCont<value_t>;

         Vcc2ContribsBaseAmps(const Vcc2ContribsBaseAmps&) = delete;
         Vcc2ContribsBaseAmps& operator=(const Vcc2ContribsBaseAmps&) = delete;
         Vcc2ContribsBaseAmps(Vcc2ContribsBaseAmps&&) = default;
         Vcc2ContribsBaseAmps& operator=(Vcc2ContribsBaseAmps&&) = default;
         ~Vcc2ContribsBaseAmps() override = default;

         //! Takes the T-amplitudes (or C-coefs.) used in most contributions.
         Vcc2ContribsBaseAmps
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            );

         /******************************************************************//**
          * @name Const access
          *********************************************************************/
         //!@{
         const ModeCombiOpRange& Mcr() const {return mrMcr;}
         const datacont_t& TAmps() const {return mrTAmps;}
         const std::vector<absval_t>& TAmpsNorms2() const {return mTAmpsNorms2;}
         Uin NexciForModeCombi(const Uin) const;
         const std::vector<Uin>& McAddresses() const {return mMcAddresses;}
         //!@}

         //! Calculates norms^2 of parameters corresponding to address/index ranges.
         static std::vector<absval_t> CalcNorms2(const datacont_t&, const std::vector<Uin>&);

      private:
         //! The ModeCombi%s associated with the calculation and parameter containers.
         const ModeCombiOpRange& mrMcr;

         //! Where parameters for each ModeCombi are found in containers (mrTAmps, etc.).
         const std::vector<Uin> mMcAddresses;

         //! The T-amplitudes (or C-coefficients) used in most method.
         const datacont_t& mrTAmps;

         //! Norms^2 of parameters of each ModeCombi block.
         const std::vector<absval_t> mTAmpsNorms2;

         //! Calculate ModeCombi addresses based on ModeCombi modes and number of modals.
         static std::vector<Uin> VecMcAddresses(const ModeCombiOpRange&, const std::vector<Uin>&);

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSBASEAMPS_DECL_H_INCLUDED*/
