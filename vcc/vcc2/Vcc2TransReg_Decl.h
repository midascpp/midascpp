/**
 *******************************************************************************
 * 
 * @file    Vcc2TransReg_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSREG_DECL_H_INCLUDED
#define VCC2TRANSREG_DECL_H_INCLUDED

#include <memory>

// Midas headers.
#include "vcc/vcc2/Vcc2TransBase.h"

// Forward declarations.
namespace midas::vcc::vcc2
{
   template<typename, typename> class Vcc2ContribsReg;
} /* namespace midas::vcc::vcc2 */

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      ,  bool T2ONLY = false
      ,  typename OPDEF_T = OpDef
      ,  bool TDMVCC2 = false
      >
   class Vcc2TransReg final
      :  public Vcc2TransBase<T, OPDEF_T>
   {
      public:
         using typename Vcc2TransBase<T, OPDEF_T>::value_t;
         using typename Vcc2TransBase<T, OPDEF_T>::absval_t;
         using typename Vcc2TransBase<T, OPDEF_T>::energy_t;
         using typename Vcc2TransBase<T, OPDEF_T>::opdef_t;
         using typename Vcc2TransBase<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2TransBase<T, OPDEF_T>::midasvector_t;
         using typename Vcc2TransBase<T, OPDEF_T>::datacont_t;

         Vcc2TransReg(const Vcc2TransReg&) = delete;
         Vcc2TransReg& operator=(const Vcc2TransReg&) = delete;
         Vcc2TransReg(Vcc2TransReg&&) = default;
         Vcc2TransReg& operator=(Vcc2TransReg&&) = default;
         ~Vcc2TransReg() override = default;

         Vcc2TransReg
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const bool aOnlyOneMode = false
            ,  const bool aSaveIntermeds = false
            );

         bool& DoingVcc() {return mDoingVcc;}
         bool DoingVcc() const {return mDoingVcc;}

         //! Get saved intermediates
         auto GetSavedIntermediates() && { return std::move(this->mSavedIntermediates); }
         const auto& GetSavedIntermediates() & { return this->mSavedIntermediates; }

      private:
         using vcc2contribsreg_t = midas::vcc::vcc2::Vcc2ContribsReg<value_t,OPDEF_T>;
         using typename Vcc2TransBase<T, OPDEF_T>::intermeds_t;
         using typename Vcc2TransBase<T, OPDEF_T>::fclock_t;
         using typename Vcc2TransBase<T, OPDEF_T>::fscr_t;

         bool mDoingVcc = true;

         //! Saved intermediates
         bool mSaveIntermeds = false;
         mutable std::unique_ptr<intermeds_t> mSavedIntermediates = nullptr;

         std::string PrettyClassName() const override;
         std::string PrettyTransformerType() const override;

         //!
         void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const override;

         void TwoModeTransformerImpl
            (  const vcc2contribsreg_t& arVcc2Contribs
            ,  intermeds_t& arIntermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
         void OneModeTransformerImpl
            (  const vcc2contribsreg_t& arVcc2Contribs
            ,  intermeds_t& arIntermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSREG_DECL_H_INCLUDED*/
