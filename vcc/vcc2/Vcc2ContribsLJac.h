/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsLJac.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSLJAC_H_INCLUDED
#define VCC2CONTRIBSLJAC_H_INCLUDED

#include "vcc/vcc2/Vcc2ContribsLJac_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "vcc/vcc2/Vcc2ContribsLJac_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*VCC2CONTRIBSLJAC_H_INCLUDED*/
