/**
 *******************************************************************************
 * 
 * @file    Vcc2TransBase_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSBASE_DECL_H_INCLUDED
#define VCC2TRANSBASE_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "vcc/IntegralContractor.h"

// Forward declarations.
class ModeCombiOpRange;
class OpDef;
template<typename> class ModalIntegrals;
template<typename,typename> class TwoModeIntermeds;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T
      >
   class Vcc2TransBase
   {
      public:
         using value_t = T;
         using absval_t = midas::type_traits::RealTypeT<value_t>;
         using energy_t = value_t;
         using opdef_t = OPDEF_T;
         using modalintegrals_t = ModalIntegrals<value_t>;
         using midasvector_t = GeneralMidasVector<value_t>;
         using datacont_t = GeneralDataCont<value_t>;
         using intop_t = ::midas::vcc::IntegralOpT;

         Vcc2TransBase(const Vcc2TransBase&) = delete;
         Vcc2TransBase& operator=(const Vcc2TransBase&) = delete;
         Vcc2TransBase(Vcc2TransBase&&) = default;
         Vcc2TransBase& operator=(Vcc2TransBase&&) = default;
         virtual ~Vcc2TransBase() = default;

         void Transform(const datacont_t& arIn, datacont_t& arOut) const;

         /******************************************************************//**
          * @name Transformer settings
          *********************************************************************/
         //!@{

         Uin& IoLevel() {return mIoLevel;}
         Uin IoLevel() const {return mIoLevel;}

         bool& TimeIt() {return mTimeIt;}
         bool TimeIt() const {return mTimeIt;}

         absval_t& ScreenNorm2Thr() {return mScreenNorm2Thr;}
         absval_t ScreenNorm2Thr() const {return mScreenNorm2Thr;}

         bool OnlyOneMode() const {return mOnlyOneMode;}
         //!@}

         //@{
         //! The size the input/result container must have.
         Uin TotSizeInput() const {return McAddresses().back();}
         Uin TotSizeResult() const {return McAddresses().back() + AddrOffsetDcOut();}
         //@}

      protected:
         using intermeds_t = TwoModeIntermeds<value_t,OPDEF_T>;
         using fclock_t = Nb;
         using fscr_t = Nb;

         Vcc2TransBase
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const bool aOnlyOneMode = false
            ,  const Uin aIoLevel = 0
            ,  const bool aTimeIt = false
            );

         const opdef_t* const pOpDef() const {return mpOpDef;}
         const modalintegrals_t& RawModInts() const {return mrRawModInts;}
         const ModeCombiOpRange& Mcr() const {return mrMcr;}
         Uin NexciForModeCombi(const Uin aMc) const {return mMcAddresses[aMc+I_1] - mMcAddresses[aMc];}
         Uin NModals(const LocalModeNr aM) const {return mNModals[aM];}
         const std::vector<Uin>& NModals() const {return mNModals;}
         const std::vector<Uin>& McAddresses() const {return mMcAddresses;}

         void InitializeXIntermeds
            (  intermeds_t&
            ,  const modalintegrals_t&
            ,  const datacont_t&
            )  const;
         void InitializeZIntermeds
            (  intermeds_t&
            ,  const modalintegrals_t&
            ,  const datacont_t&
            ,  const bool aLJac = false
            )  const;

         std::vector<midasvector_t> Extract1ModeParams(const datacont_t&) const;

         //! Delete this at some point...
         std::vector<In> NModalsIn() const {return std::vector<In>(mNModals.begin(), mNModals.end());}

         void SanityCheckDc(const datacont_t&, const std::string&, const In = 0) const;

      private:
         const opdef_t* const mpOpDef;
         const modalintegrals_t& mrRawModInts;
         const ModeCombiOpRange& mrMcr;
         const std::vector<Uin> mNModals;
         const std::vector<Uin> mMcAddresses;
         const bool mOnlyOneMode = false;

         Uin mIoLevel = 0;
         bool mTimeIt = false;
         absval_t mScreenNorm2Thr = -1.0;

         virtual std::string PrettyClassName() const = 0;
         virtual std::string PrettyTransformerType() const = 0;

         //! Extract the number of modals for each mode from a modal integrals object.
         static std::vector<Uin> VecNModals(const modalintegrals_t&);

         //! Calculate ModeCombi addresses based on ModeCombi modes and number of modals.
         static std::vector<Uin> VecMcAddresses(const ModeCombiOpRange&, const std::vector<Uin>&);

         //!
         virtual void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const = 0;

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;

         virtual In AddrOffsetDcOut() const {return 0;}
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSBASE_DECL_H_INCLUDED*/
