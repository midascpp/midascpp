/**
 *******************************************************************************
 * 
 * @file    Vcc2TransReg_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSREG_IMPL_H_INCLUDED
#define VCC2TRANSREG_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/TypeName.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/vcc2/TwoModeIntermeds.h"
#include "vcc/vcc2/Vcc2ContribsReg.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::Vcc2TransReg
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const bool aOnlyOneMode
   ,  const bool aSaveIntermeds
   )
   :  Vcc2TransBase<T, OPDEF_T>
         (  apOpDef
         ,  arRawModInts
         ,  arMcr
         ,  aOnlyOneMode
         )
   ,  mSaveIntermeds(aSaveIntermeds)
{
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
std::string Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::PrettyClassName
   (
   )  const
{
   return std::string("Vcc2TransReg<")+midas::type_traits::TypeName<T>()+">";
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
std::string Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::PrettyTransformerType
   (
   )  const
{
   if (this->DoingVcc())
   {
      return "<mu|exp(-T)Xexp(T)|ref> (VCC; err.vec. if X = H)";
   }
   else
   {
      return "<mu|X|VCI> (VCI)";
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
void Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::TransformImpl
   (  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   // T1-transform a copy of raw modal integrals in VCC case.
   // Or just directly use the provided raw ones in VCI case.
   const modalintegrals_t* p_mod_ints = &this->RawModInts();
   std::unique_ptr<modalintegrals_t> p_t1_mod_ints(nullptr);
   if (this->DoingVcc() && !T2ONLY)
   {
      p_t1_mod_ints.reset(new modalintegrals_t(this->RawModInts()));
      p_t1_mod_ints->T1Transform(this->Extract1ModeParams(arIn));
      p_mod_ints = p_t1_mod_ints.get();
   }

   // Intermediates.
   clock_t t0 = clock();
   intermeds_t intermeds(this->NModalsIn(), this->pOpDef(), this->Mcr(), !this->DoingVcc());
   this->InitializeXIntermeds(intermeds, *p_mod_ints, arIn);
   if (!this->DoingVcc())
      this->InitializeZIntermeds(intermeds, *p_mod_ints, arIn);
   intermeds.InitializeXCintermeds(this->pOpDef());
   if constexpr (TDMVCC2)
   {
      intermeds.InitializeTdmvcc2IntermedsBeforeLJac(this->pOpDef());
   }
   if (this->TimeIt())
      Mout << "   Intermediates init.: " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;

   // Vcc2Contribs object.
   vcc2contribsreg_t vcc2contribsreg
      (  this->pOpDef()
      ,  *p_mod_ints
      ,  this->Mcr()
      ,  arIn
      ,  this->ScreenNorm2Thr()
      ,  this->DoingVcc()
      );

   // Calculate contributions.
   if (!this->OnlyOneMode())
   {
      TwoModeTransformerImpl(vcc2contribsreg, intermeds, arIn, arOut);
   }
   else
   {
      OneModeTransformerImpl(vcc2contribsreg, intermeds, arIn, arOut);
   }

   // Store intermediates?
   if (  this->mSaveIntermeds
      )
   {
      this->mSavedIntermediates = std::make_unique<intermeds_t>(std::move(intermeds));
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
void Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::TwoModeTransformerImpl
   (  const vcc2contribsreg_t& arVcc2Contribs
   ,  intermeds_t& arIntermeds
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   clock_t t_tot = clock();
   clock_t t0 = clock();
   
   const ModeCombiOpRange& mcr = this->Mcr();

   clock_t t_tmp;
   clock_t t1 = 0;
   clock_t t2 = 0;
   clock_t t3 = 0;
   // Do <ref| part.
   t0 = clock();
   value_t ref_ref = C_0; // The <ref|H|ref> needs to be stored for TwoModeM0M1T2 in VCI case.
   t_tmp = clock();
   arVcc2Contribs.TwoModeRefRef(ref_ref);
   t1 = clock() - t_tmp;
   value_t ref = ref_ref;
   if (!this->DoingVcc())
   {
      // For VCI the reference state has a coefficient. Take care of it.
      value_t ref_coef;
      arIn.DataIo(IO_GET, I_0, ref_coef);
      ref *= ref_coef;
   }
   t_tmp = clock();
   arVcc2Contribs.TwoModeRefT2(ref, arIntermeds);
   t2 = clock() - t_tmp;
   
   // For VCI only.
   t_tmp = clock();
   if (!this->DoingVcc())
      arVcc2Contribs.TwoModeRefT1(ref, arIntermeds);
   t3 = clock() - t_tmp;
 
   arOut.DataIo(IO_PUT, I_0, ref);
   if (this->TimeIt())
   {
      Mout << "   <ref| part:          " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl
           << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl
           << "       ...  T1|ref>     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl
           << "       ...  T2|ref>     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
   }

   t1 = 0;
   t2 = 0;
   t3 = 0;
   t0 = clock();
   // Do <m0| part. Loop over 1-ModeCombis.
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);
     
      t_tmp = clock();
      arVcc2Contribs.TwoModeM0ref(m0, res);
      if (!this->DoingVcc())
      {
         // For VCI the reference state has a coeffecient. Take care of it.
         value_t ref_coef;
         arIn.DataIo(IO_GET, I_0, ref_coef);
         res *= ref_coef;
      }
      t1 += clock() - t_tmp;

      t_tmp = clock();
      // For VCI only.
      if (!this->DoingVcc())
         arVcc2Contribs.TwoModeM0T1(mc, res, arIntermeds);
      t2 += clock() - t_tmp;
      
      t_tmp = clock();
      arVcc2Contribs.TwoModeM0T2(m0, res, arIntermeds);
      t3 += clock() - t_tmp;
      
      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0| part:           " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T1|ref>     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T2|ref>     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
   }
   
   t1 = 0;
   t2 = 0;
   t3 = 0;
   clock_t t4 = 0;
   clock_t t_m0m1t2_0 = 0;
   clock_t t_m0m1t2_1 = 0;
   clock_t t_m0m1t2t2_0 = 0;
   clock_t t_m0m1t2t2_1 = 0;
   In totterms_t2_0 = 0;
   In screened_t2_0 = 0;
   In totterms_t2_1 = 0;
   In screened_t2_1 = 0;
   In totterms_t2t2 = 0;
   In screened_t2t2 = 0;
   t0 = clock();
   if (this->DoingVcc())
      ref_ref = C_0;        // Don't include passive terms in TwoModeM0M1T2().
   // Do <m0,m1| part... Loop over 2-ModeCombis.
   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;
      
      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);
      
      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);

      t_tmp = clock();
      arVcc2Contribs.TwoModeM0M1ref(mc, res);
      if (!this->DoingVcc())
      {
         // For VCI the reference state has a coeffecient. Take care of it.
         value_t ref_coef;
         arIn.DataIo(IO_GET, I_0, ref_coef);
         res *= ref_coef;
      }
      t1 += clock() - t_tmp;
      
      t_tmp = clock();
      // For VCI only.
      if (!this->DoingVcc())
         arVcc2Contribs.TwoModeM0M1T1(mc, res);
      t2 += clock() - t_tmp;
      
      t_tmp = clock();
      arVcc2Contribs.TwoModeM0M1T2
         (  mc
         ,  res
         ,  arIntermeds
         ,  ref_ref
         ,  &t_m0m1t2_0
         ,  &t_m0m1t2_1
         ,  &totterms_t2_0
         ,  &screened_t2_0
         ,  &totterms_t2_1
         ,  &screened_t2_1
         );
      t3 += clock() - t_tmp;
     
      t_tmp = clock(); 
      // For VCC only.
      if (this->DoingVcc())
         arVcc2Contribs.template TwoModeM0M1T2T2<false>
            (  mc 
            ,  res
            ,  arIntermeds
            ,  nullptr
            ,  &t_m0m1t2t2_0
            ,  &t_m0m1t2t2_1
            ,  &totterms_t2t2
            ,  &screened_t2t2
            );
      t4 += clock() - t_tmp;
      
      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0,m1| part:        " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T1|ref>     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T2|ref>     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_m0m1t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2_0 << "/" << totterms_t2_0
                                         << " (" << fscr_t(screened_t2_0)/totterms_t2_0 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_m0m1t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2_1 << "/" << totterms_t2_1
                                         << " (" << fscr_t(screened_t2_1)/totterms_t2_1 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "       ...T2^2|ref>     " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_m0m1t2t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 1:     " << fclock_t(t_m0m1t2t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2t2 << "/" << totterms_t2t2
                                         << " (" << fscr_t(screened_t2t2)/totterms_t2t2 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "   Total:               " << fclock_t(clock() - t_tot) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "Done." << endl;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
void Vcc2TransReg<T, T2ONLY, OPDEF_T,TDMVCC2>::OneModeTransformerImpl
   (  const vcc2contribsreg_t& arVcc2Contribs
   ,  intermeds_t& arIntermeds
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   clock_t t_tot = clock();
   clock_t t0 = clock();
   
   const ModeCombiOpRange& mcr = this->Mcr();

   clock_t t_tmp;
   clock_t t1 = 0;
   clock_t t2 = 0;
   clock_t t3 = 0;
   // Do <ref| part.
   t0 = clock();
   value_t ref_ref = C_0; // The <ref|H|ref> needs to be stored for TwoModeM0M1T2 in VCI case.
   t_tmp = clock();
   arVcc2Contribs.TwoModeRefRef(ref_ref);
   t1 = clock() - t_tmp;
   value_t ref = ref_ref;
   if (!this->DoingVcc())
   {
      // For VCI the reference state has a coefficient. Take care of it.
      value_t ref_coef;
      arIn.DataIo(IO_GET, I_0, ref_coef);
      ref *= ref_coef;
   }
   
   // For VCI only.
   t_tmp = clock();
   if (!this->DoingVcc())
      arVcc2Contribs.TwoModeRefT1(ref, arIntermeds);
   t3 = clock() - t_tmp;
 
   arOut.DataIo(IO_PUT, I_0, ref);
   if (this->TimeIt())
   {
      Mout << "   <ref| part:          " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl
           << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl
           << "       ...  T1|ref>     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
   }

   t1 = 0;
   t2 = 0;
   t0 = clock();
   // Do <m0| part.
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);
     
      t_tmp = clock();
      arVcc2Contribs.TwoModeM0ref(m0, res);
      if (!this->DoingVcc())
      {
         // For VCI the reference state has a coeffecient. Take care of it.
         value_t ref_coef;
         arIn.DataIo(IO_GET, I_0, ref_coef);
         res *= ref_coef;
      }
      t1 += clock() - t_tmp;

      t_tmp = clock();
      // For VCI only.
      if (!this->DoingVcc())
         arVcc2Contribs.TwoModeM0T1(mc, res, arIntermeds);
      t2 += clock() - t_tmp;
      
      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0| part:           " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T1|ref>     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
   }

   t1 = 0;
   t2 = 0;
   t3 = 0;
   clock_t t4 = 0;
   clock_t t_m0m1t2_0 = 0;
   clock_t t_m0m1t2_1 = 0;
   clock_t t_m0m1t2t2_0 = 0;
   clock_t t_m0m1t2t2_1 = 0;
   In totterms_t2_0 = 0;
   In screened_t2_0 = 0;
   In totterms_t2_1 = 0;
   In screened_t2_1 = 0;
   In totterms_t2t2 = 0;
   In screened_t2t2 = 0;
   t0 = clock();
   // Do <m0,m1| part...
   if (this->DoingVcc())
      ref_ref = C_0;        // Don't include passive terms in TwoModeM0M1T2().
   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;
      
      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);
      
      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);

      t_tmp = clock();
      arVcc2Contribs.TwoModeM0M1ref(mc, res);
      if (!this->DoingVcc())
      {
         // For VCI the reference state has a coeffecient. Take care of it.
         value_t ref_coef;
         arIn.DataIo(IO_GET, I_0, ref_coef);
         res *= ref_coef;
      }
      t1 += clock() - t_tmp;
      
      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0,m1| part:        " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...    |ref>     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T1|ref>     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       ...  T2|ref>     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_m0m1t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2_0 << "/" << totterms_t2_0
                                         << " (" << fscr_t(screened_t2_0)/totterms_t2_0 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_m0m1t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2_1 << "/" << totterms_t2_1
                                         << " (" << fscr_t(screened_t2_1)/totterms_t2_1 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "       ...T2^2|ref>     " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_m0m1t2t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 1:     " << fclock_t(t_m0m1t2t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_t2t2 << "/" << totterms_t2t2
                                         << " (" << fscr_t(screened_t2t2)/totterms_t2t2 * 100 << " %)"
                                         << endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "   Total:               " << fclock_t(clock() - t_tot) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "Done." << endl;
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSREG_IMPL_H_INCLUDED*/
