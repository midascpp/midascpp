/**
 *******************************************************************************
 * 
 * @file    Vcc2TransRJac_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSRJAC_IMPL_H_INCLUDED
#define VCC2TRANSRJAC_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/TypeName.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/vcc2/Vcc2ContribsReg.h"
#include "vcc/vcc2/Vcc2ContribsRJac.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Vcc2TransRJac<T, OPDEF_T>::Vcc2TransRJac
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const bool aOnlyOneMode
   ,  const Uin aIoLevel
   ,  const bool aTimeIt
   )
   :  Vcc2TransFixedAmps<T, false, OPDEF_T>
         (  apOpDef
         ,  arRawModInts
         ,  arMcr
         ,  arTAmps
         ,  aOnlyOneMode
         ,  aIoLevel
         ,  aTimeIt
         )
   ,  mPairT2IntermedsVccEnergy(ConstructT2Intermeds(arTAmps))
{
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >  
std::string Vcc2TransRJac<T, OPDEF_T>::PrettyClassName
   (
   )  const
{  
   return std::string("Vcc2TransRJac<")+midas::type_traits::TypeName<T>()+">";
}  
   
/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
std::string Vcc2TransRJac<T, OPDEF_T>::PrettyTransformerType
   (
   )  const
{
   return "<mu|exp(-T)[X,R]exp(T)|ref> (right-hand VCC Jacobian if X = H)";
}

/***************************************************************************//**
 * Calculates the "VCC energy" \f$ E_{\text{VCC}}(T) \f$ (although not
 * technically the energy for arbitrary amplitudes \f$T\f$);
 * \f{align}{
 *    E_{\text{VCC}}(T)
 *    &\equiv
 *    \langle\Phi\vert \exp(-T) H \exp(T) \vert\Phi\rangle
 *    \\&=
 *    \langle\Phi\vert \widetilde{H} \vert\Phi\rangle
 *    +
 *    \langle\Phi\vert \widetilde{H} T_2 \vert\Phi\rangle
 * \f}
 * with the first term corresponding to Vcc2ContribsReg<T, OPDEF_T>::TwoModeRefRef()
 * (`template<bool aRsp = false>`), and the second corresponding to
 * Vcc2ContribsReg<T, OPDEF_T>::TwoModeRefT2(). The latter incidentally also computes
 * and assigns the Y-intermediates as a side-effect.
 *
 * @note
 *    See note at ConstructIntermeds(). Also the returned "VCC energy" is not
 *    meaningful if OnlyOneMode() is true, but in that case it's not used for
 *    anything either.
 *
 * @param[in] arParams
 *    Intended to be the (fixed) T-amps. that define the R-Jacobian
 *    transformer.
 * @return
 *    Pair containing of newly constructed T2 intermeds_t object _and_ the "VCC
 *    energy", calculated in part as a side-effect of initializing the Y
 *    intermediates. "Empty" (no intermediates initialized) if OnlyOneMode().
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >  
std::pair<typename Vcc2TransRJac<T, OPDEF_T>::intermeds_t, typename Vcc2TransRJac<T, OPDEF_T>::energy_t>
Vcc2TransRJac<T, OPDEF_T>::ConstructT2Intermeds
   (  const datacont_t& arParams
   )  const
{
   if (gDebug || this->IoLevel() > I_10 || this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; computing T-based intermediates" << std::endl;
   }
   clock_t t0 = clock();

   intermeds_t intermeds(this->NModalsIn(), this->pOpDef(), this->Mcr());
   energy_t e_vcc = 0;
   if (!this->OnlyOneMode())
   {
      this->InitializeXIntermeds(intermeds, this->T1ModInts(), arParams);
      intermeds.InitializeXCintermeds(this->pOpDef());
      // This TwoModeRefT2 call calculates Y intermediates and E_VCC(T).
      midas::vcc::vcc2::Vcc2ContribsReg<value_t,opdef_t> vcc2contribsreg
         (  this->pOpDef()
         ,  this->T1ModInts()
         ,  this->Mcr()
         ,  arParams
         );
      vcc2contribsreg.TwoModeRefRef(e_vcc);
      vcc2contribsreg.TwoModeRefT2(e_vcc, intermeds);
   }

   if (this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; T-based intermediates: " << fclock_t(clock() - t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   return std::make_pair(std::move(intermeds),e_vcc);
}  
   
/***************************************************************************//**
 * @note
 *    At the moment this will construct (uninitialized) intermediates
 *    regardless if OnlyOneMode() is true, although it might not be relevant.
 *    We'll live with this for now...
 *    (Also, if client has set up the ModeCombiOpRange consistently with doing
 *    1-mode transformations only, then the loop constructs in the intermeds_t
 *    object shouldn't have any unnecessary overhead.) -MBH, Feb 2019.
 *
 * @param[in] arParams
 *    Intended to be the R-coefs.
 * @return
 *    Newly constructed intermeds_t object. "Empty" (no intermediates
 *    initialized if OnlyOneMode()), otherwise with intermediates according to
 *    given parameters.
 *    Y intermediates not calculated yet; intended to be calculated later, by 
 *    TwoModeRefT2() in TwoModeTransformerImpl().
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >  
typename Vcc2TransRJac<T, OPDEF_T>::intermeds_t Vcc2TransRJac<T, OPDEF_T>::ConstructR2Intermeds
   (  const datacont_t& arParams
   )  const
{
   intermeds_t intermeds(this->NModalsIn(), this->pOpDef(), this->Mcr());
   if (!this->OnlyOneMode())
   {
      this->InitializeXIntermeds(intermeds, this->T1ModInts(), arParams);
      intermeds.InitializeXCintermeds(this->pOpDef());
   }
   return intermeds;
}  
   
/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2TransRJac<T, OPDEF_T>::TransformImpl
   (  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   // R1-transform a copy of the T1 modal integrals.
   const auto& t1_mod_ints = this->T1ModInts();
   modalintegrals_t r1_mod_ints(t1_mod_ints);
   r1_mod_ints.R1Transform(this->Extract1ModeParams(arIn));

   // R2 intermediates. (The T intermediates were calculated at construction.)
   clock_t t0 = clock();
   std::unique_ptr<intermeds_t> p_r2_intermeds(nullptr);
   if (!this->OnlyOneMode())
   {
      p_r2_intermeds.reset(new intermeds_t(ConstructR2Intermeds(arIn)));
   }
   if (this->TimeIt())
      Mout << "   Intermediates init.: " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;

   // Vcc2Contribs object.
   vcc2contribsrjac_t vcc2contribsrjac
      (  this->pOpDef()
      ,  this->T1ModInts()
      ,  this->Mcr()
      ,  this->TAmps()
      ,  r1_mod_ints
      ,  arIn
      ,  this->ScreenNorm2Thr()
      );

   // Calculate contributions.
   if (!this->OnlyOneMode())
   {
      if (p_r2_intermeds)
      {
         TwoModeTransformerImpl(vcc2contribsrjac, *p_r2_intermeds, arIn, arOut);
      }
      else
      {
         MIDASERROR("p_r2_intermeds == nullptr, unexpected!");
      }
   }
   else
   {
      OneModeTransformerImpl(vcc2contribsrjac, arIn, arOut);
   }

   //// Finalize here or in base?
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2TransRJac<T, OPDEF_T>::TwoModeTransformerImpl
   (  const vcc2contribsrjac_t& arVcc2Contribs
   ,  intermeds_t& arR2Intermeds
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   const auto& vcc2contribsrjac = arVcc2Contribs;
   const auto& t2_intermeds = this->T2Intermeds();
   auto& r2_intermeds = arR2Intermeds;

   clock_t t_tot = clock();
   clock_t t0 = clock();
   
   // Do <ref| passive part.
   t0 = clock(); 
   value_t r1_ref = C_0;
   vcc2contribsrjac.template TwoModeRefRef<true>(r1_ref);
   
   // Do <ref| H R2 |ref>.
   // This also updates r2_intermeds.mY.
   value_t ref = r1_ref;
   vcc2contribsrjac.TwoModeRefT2(ref, r2_intermeds);
   if (this->TimeIt())
      Mout << "   <ref| part:          " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
   
   // Do <m0|
   t0 = clock();
   clock_t t1 = 0;
   clock_t t2 = 0;
   clock_t t3 = 0;
   const ModeCombiOpRange& mcr = this->Mcr();
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);

      // The R_1 part...
      clock_t t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0ref<true>(m0, res);
      t1 += clock() - t_tmp;
      t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0T2<true>(m0, res, t2_intermeds);
      t2 += clock() - t_tmp;
      
      // The R_2 part...
      t_tmp = clock();
      vcc2contribsrjac.TwoModeM0T2(m0, res, r2_intermeds);
      t3 += clock() - t_tmp;

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address()+AddrOffsetDcOut());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0| part:           " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       [H,R1]    |ref>  " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       [H,R1] T2 |ref>  " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       H R2      |ref>  " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
   }
   
   // Do <m0,m1| ...
   t0 = clock();
   t1 = 0;
   t2 = 0;
   t3 = 0;
   clock_t t4 = 0;
   clock_t t5 = 0;
   clock_t t_r1_t2_0 = 0;
   clock_t t_r1_t2_1 = 0;
   In totterms_r1_t2_0 = 0;
   In screened_r1_t2_0 = 0;
   In totterms_r1_t2_1 = 0;
   In screened_r1_t2_1 = 0;
   clock_t t_r2_t2_0 = 0;
   clock_t t_r2_t2_1 = 0;
   In totterms_r2_t2_0 = 0;
   In screened_r2_t2_0 = 0;
   In totterms_r2_t2_1 = 0;
   In screened_r2_t2_1 = 0;
   clock_t t_r2_t2t2_0 = 0;
   clock_t t_r2_t2t2_1 = 0;
   In totterms_r2_t2t2 = 0;
   In screened_r2_t2t2 = 0;
   const energy_t& e_vcc = this->EVccForTAmps();
   
   value_t ref_norsp = C_0;
   vcc2contribsrjac.template TwoModeRefRef<false>(ref_norsp);
   
   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);
      
      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      
      // The R_1 part...
      clock_t t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0M1ref<true>(mc, res);
      t1 += clock() - t_tmp;
      
      t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0M1T2<true>
         (  mc
         ,  res
         ,  t2_intermeds
         ,  r1_ref
         ,  &t_r1_t2_0, &t_r1_t2_1
         ,  &totterms_r1_t2_0
         ,  &screened_r1_t2_0
         ,  &totterms_r1_t2_1
         ,  &screened_r1_t2_1
         );
      t2 += clock() - t_tmp;
      
      // The R_2 part...
      t_tmp = clock();
      vcc2contribsrjac.TwoModeM0M1T2
         (  mc
         ,  res
         ,  r2_intermeds
         ,  ref_norsp
         ,  &t_r2_t2_0
         ,  &t_r2_t2_1
         ,  &totterms_r2_t2_0
         ,  &screened_r2_t2_0
         ,  &totterms_r2_t2_1
         ,  &screened_r2_t2_1
         );
      t3 += clock() - t_tmp;
      t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0M1T2T2<true>
         (  mc
         ,  res
         ,  t2_intermeds
         ,  &r2_intermeds
         ,  &t_r2_t2t2_0
         ,  &t_r2_t2t2_1
         ,  &totterms_r2_t2t2
         ,  &screened_r2_t2t2
         );
      t4 += clock() - t_tmp;
      
      t_tmp = clock();
      midasvector_t amps((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      this->TAmps().DataIo(IO_GET, amps, amps.Size(), mc.Address());
      res -= ref * amps;
      t5 += clock() - t_tmp;

      // Subtract energy.
      midasvector_t r2_coefs((this->NModals(m0)-I_1)*(this->NModals(m1)-I_1), C_0);
      arIn.DataIo(IO_GET, r2_coefs, r2_coefs.Size(), mc.Address());
      res -= e_vcc * r2_coefs;

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address()+AddrOffsetDcOut());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0,m1| part:        " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       [H,R1]    |ref>  " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;

      Mout << "       [H,R1] T2 |ref>  " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r1_t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r1_t2_0 << "/"
                                         << totterms_r1_t2_0
                                         << " (" << fscr_t(screened_r1_t2_0)/totterms_r1_t2_0 * 100
                                         << " %)"<< endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_r1_t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r1_t2_1 << "/"
                                         << totterms_r1_t2_1
                                         << " (" << fscr_t(screened_r1_t2_1)/totterms_r1_t2_1 * 100
                                         << " %)"<< endl;

      Mout << "       H R2      |ref>  " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r2_t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2_0 << "/"
                                         << totterms_r2_t2_0
                                         << " (" << fscr_t(screened_r2_t2_0)/totterms_r2_t2_0 * 100
                                         << " %)"<< endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_r2_t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2_1 << "/"
                                         << totterms_r2_t2_1
                                         << " (" << fscr_t(screened_r2_t2_1)/totterms_r2_t2_1 * 100
                                         << " %)"<< endl;

      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "       H R2 T2   |ref>  " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r2_t2t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 1:     " << fclock_t(t_r2_t2t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2t2 << "/"
                                         << totterms_r2_t2t2
                                         << " (" << fscr_t(screened_r2_t2t2)/totterms_r2_t2t2 * 100
                                         << " %)" << endl;

      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "     exp(-T) transform: " << fclock_t(t5) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "   Total:               " << fclock_t(clock() - t_tot) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "Done." << endl;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2TransRJac<T, OPDEF_T>::OneModeTransformerImpl
   (  const vcc2contribsrjac_t& arVcc2Contribs
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   const auto& vcc2contribsrjac = arVcc2Contribs;

   clock_t t_tot = clock();
   clock_t t0 = clock();
   
   // Do <ref| passive part.
   t0 = clock(); 
   value_t r1_ref = C_0;
   vcc2contribsrjac.template TwoModeRefRef<true>(r1_ref);
   
   value_t ref = r1_ref;
   
   // Do <m0|
   t0 = clock();
   clock_t t1 = 0;
   clock_t t2 = 0;
   clock_t t3 = 0;
   const ModeCombiOpRange& mcr = this->Mcr();
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);

      // The C_1 part...
      clock_t t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0ref<true>(m0, res);
      t1 += clock() - t_tmp;
      

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address()+AddrOffsetDcOut());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0| part:           " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       [H,R1]    |ref>  " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;
   }
   
   // Do <m0,m1| ...
   t0 = clock();
   t1 = 0;
   t2 = 0;
   t3 = 0;
   clock_t t4 = 0;
   clock_t t5 = 0;
   clock_t t_r1_t2_0 = 0;
   clock_t t_r1_t2_1 = 0;
   In totterms_r1_t2_0 = 0;
   In screened_r1_t2_0 = 0;
   In totterms_r1_t2_1 = 0;
   In screened_r1_t2_1 = 0;
   clock_t t_r2_t2_0 = 0;
   clock_t t_r2_t2_1 = 0;
   In totterms_r2_t2_0 = 0;
   In screened_r2_t2_0 = 0;
   In totterms_r2_t2_1 = 0;
   In screened_r2_t2_1 = 0;
   clock_t t_r2_t2t2_0 = 0;
   clock_t t_r2_t2t2_1 = 0;
   In totterms_r2_t2t2 = 0;
   In screened_r2_t2t2 = 0;
   const energy_t& e_vcc = this->EVccForTAmps();
   
   value_t ref_norsp = C_0;
   vcc2contribsrjac.template TwoModeRefRef<false>(ref_norsp);
   
   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);
      
      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      
      // The R_1 part...
      clock_t t_tmp = clock();
      vcc2contribsrjac.template TwoModeM0M1ref<true>(mc, res);
      t1 += clock() - t_tmp;
      
      t_tmp = clock();
      midasvector_t amps((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      this->TAmps().DataIo(IO_GET, amps, amps.Size(), mc.Address());
      res -= ref * amps;
      t5 += clock() - t_tmp;

      // Subtract energy.
      midasvector_t r2_coefs((this->NModals(m0)-I_1)*(this->NModals(m1)-I_1), C_0);
      arIn.DataIo(IO_GET, r2_coefs, r2_coefs.Size(), mc.Address());
      res -= e_vcc * r2_coefs;

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address()+AddrOffsetDcOut());
   }
   if (this->TimeIt())
   {
      Mout << "   <m0,m1| part:        " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "       [H,R1]    |ref>  " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl;

      Mout << "       [H,R1] T2 |ref>  " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r1_t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r1_t2_0 << "/"
                                         << totterms_r1_t2_0
                                         << " (" << fscr_t(screened_r1_t2_0)/totterms_r1_t2_0 * 100
                                         << " %)"<< endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_r1_t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r1_t2_1 << "/"
                                         << totterms_r1_t2_1
                                         << " (" << fscr_t(screened_r1_t2_1)/totterms_r1_t2_1 * 100
                                         << " %)"<< endl;

      Mout << "       H R2      |ref>  " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r2_t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2_0 << "/"
                                         << totterms_r2_t2_0
                                         << " (" << fscr_t(screened_r2_t2_0)/totterms_r2_t2_0 * 100
                                         << " %)"<< endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "            part 1:     " << fclock_t(t_r2_t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2_1 << "/"
                                         << totterms_r2_t2_1
                                         << " (" << fscr_t(screened_r2_t2_1)/totterms_r2_t2_1 * 100
                                         << " %)"<< endl;

      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "       H R2 T2   |ref>  " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 0:     " << fclock_t(t_r2_t2t2_0) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "            part 1:     " << fclock_t(t_r2_t2t2_1) / CLOCKS_PER_SEC << " s" << endl;
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      Mout << "                        " << "Screening: " << screened_r2_t2t2 << "/"
                                         << totterms_r2_t2t2
                                         << " (" << fscr_t(screened_r2_t2t2)/totterms_r2_t2t2 * 100
                                         << " %)" << endl;

      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout << "     exp(-T) transform: " << fclock_t(t5) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "   Total:               " << fclock_t(clock() - t_tot) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "Done." << endl;
   }
}



} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSRJAC_IMPL_H_INCLUDED*/
