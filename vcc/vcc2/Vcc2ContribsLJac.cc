/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsLJac.cc
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/vcc2/Vcc2ContribsLJac.h"
#include "vcc/vcc2/Vcc2ContribsLJac_Impl.h"

#include "input/OpDef.h"
#include "td/tdvcc/trf/ConstraintOpDef.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_VCC2CONTRIBSLJAC(T, OPDEF_T) \
   namespace midas::vcc::vcc2 \
   { \
      template class Vcc2ContribsLJac<T, OPDEF_T,false>; \
      template class Vcc2ContribsLJac<T, OPDEF_T,true>; \
   } /* namespace midas::vcc::vcc2 */ \


// Instantiations.
INSTANTIATE_VCC2CONTRIBSLJAC(Nb, OpDef);
INSTANTIATE_VCC2CONTRIBSLJAC(std::complex<Nb>, OpDef);
INSTANTIATE_VCC2CONTRIBSLJAC(Nb, midas::tdvcc::constraint::ConstraintOpDef);
INSTANTIATE_VCC2CONTRIBSLJAC(std::complex<Nb>, midas::tdvcc::constraint::ConstraintOpDef);

#undef INSTANTIATE_VCC2CONTRIBSLJAC

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
