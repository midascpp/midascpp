/**
 *******************************************************************************
 * 
 * @file    Vcc2TransEta.cc
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransEta_Impl.h"

#include "input/OpDef.h"
#include "td/tdvcc/trf/ConstraintOpDef.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_VCC2TRANSETA(T, OPDEF_T) \
   namespace midas::vcc::vcc2 \
   { \
      template class Vcc2TransEta<T, false, OPDEF_T>; \
      template class Vcc2TransEta<T, true, OPDEF_T>; \
   } /* namespace midas::vcc::vcc2 */ \


// Instantiations.
INSTANTIATE_VCC2TRANSETA(Nb, OpDef);
INSTANTIATE_VCC2TRANSETA(std::complex<Nb>, OpDef);
INSTANTIATE_VCC2TRANSETA(Nb, midas::tdvcc::constraint::ConstraintOpDef);
INSTANTIATE_VCC2TRANSETA(std::complex<Nb>, midas::tdvcc::constraint::ConstraintOpDef);

#undef INSTANTIATE_VCC2TRANSETA
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
