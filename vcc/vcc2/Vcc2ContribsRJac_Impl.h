/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsRJac_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSRJAC_IMPL_H_INCLUDED
#define VCC2CONTRIBSRJAC_IMPL_H_INCLUDED

// Midas headers.
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Vcc2ContribsRJac<T, OPDEF_T>::Vcc2ContribsRJac
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const modalintegrals_t& arR1ModInts
   ,  const datacont_t& arRCoefs
   ,  const absval_t aScreenNorm2
   )
   :  Vcc2ContribsReg<T, OPDEF_T>
         (  apOpDef
         ,  arModInts
         ,  arMcr
         ,  arTAmps
         ,  aScreenNorm2
         ,  true // <-- VCC, not VCI
         )
   ,  mrR1Ints(arR1ModInts)
   ,  mrRCoefs(arRCoefs)
   ,  mrRCoefsNorms2(this->CalcNorms2(this->RCoefs(), this->McAddresses()))
{
   SanityCheck();
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsRJac<T, OPDEF_T>::SanityCheck
   (
   )  const
{
   std::stringstream ss;

   // Check Vcc2ContribsReg base is setup for VCC.
   if (!this->DoingVccNotVci())
   {
      MIDASERROR("Only intended to work for VCC, not VCI.");
   }

   // Check R1 integrals.
   this->CheckTransType(this->R1Ints(), modalintegrals_t::TransT::R1, "R1");

   // Check that sizes of T-amps./R-coefs. container match.
   Uin tamps_size = this->TAmps().Size();
   Uin rcoef_size = this->RCoefs().Size();
   if (tamps_size != rcoef_size)
   {
      ss << "T-amps. container size (= " << tamps_size
         << ") != R-coef. container size (= " << rcoef_size
         << ")."
         ;
      MIDASERROR(ss.str());
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSRJAC_IMPL_H_INCLUDED*/
