/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsLJac_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSLJAC_DECL_H_INCLUDED
#define VCC2CONTRIBSLJAC_DECL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "vcc/vcc2/Vcc2ContribsBaseAmps.h"

// Forward declarations.
class ModeCombi;
template<typename> class GeneralMidasVector;
template<typename,typename> class TwoModeIntermeds;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class is for calculating contributions to the left-hand
    * Jacobian transformation, with elements
    * \f[
    *    \gamma_\nu = \langle\Phi\vert L \exp(-T_2)[\widetilde{H},\tau_\nu]\exp(T_2) \vert\Phi\rangle
    * \f]
    * where the Hamiltonian modal integrals have been \f$ T_1 \f$-transformed.
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T = OpDef
      ,  bool TDMVCC2 = false
      >
   class Vcc2ContribsLJac
      :  public Vcc2ContribsBaseAmps<T, OPDEF_T>
   {
      public:
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::value_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::absval_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::opdef_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::datacont_t;
         using typename Vcc2ContribsBase<T, OPDEF_T>::intop_t;
         using midasvector_t = GeneralMidasVector<value_t>;
         using twomodeintermeds_t = TwoModeIntermeds<value_t,opdef_t>;

         Vcc2ContribsLJac(const Vcc2ContribsLJac&) = delete;
         Vcc2ContribsLJac& operator=(const Vcc2ContribsLJac&) = delete;
         Vcc2ContribsLJac(Vcc2ContribsLJac&&) = default;
         Vcc2ContribsLJac& operator=(Vcc2ContribsLJac&&) = default;
         ~Vcc2ContribsLJac() override = default;

         //! Construct with operator, T-amplitudes and L-coefficients.
         Vcc2ContribsLJac
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const datacont_t& arLCoefs
            );

         /******************************************************************//**
          * @name Contributions
          *********************************************************************/
         //!@{
         void TwoModeLHTau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeLM0TauHp(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeLM0HfTau(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeLM0cH1Tau(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeLM0cH2Tau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeLHddTTau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeLTauHddT(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeL1Tau2(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeL2Vcc1pt2(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeLm0m1THTau1(const ModeCombi& aM0, midasvector_t& aRes, twomodeintermeds_t& aIntermeds) const;
         void TwoModeLm1m2THTau1(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeL2HTau2(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeL2THTau2(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeL2Tau2HT(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeLm1m2HTau2(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeL2H1XTau1(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeL2HTTau2(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         //!@}

      protected:
         //! The L-coefficients.
         const datacont_t& LCoefs() const {return mrLCoefs;}

      private:
         //! The L-coefficients.
         const datacont_t& mrLCoefs;

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;

         /******************************************************************//**
          * @name Contribution utilities
          *********************************************************************/
         //!@{
         void LXContractor
            (  const midasvector_t& aLvec
            ,  const ModeCombi& aL_mc
            ,  const In a_m
            ,  const midasvector_t& aXint
            ,  midasvector_t& aRes
            ,  const ModeCombi& aR_mc
            )  const;
         //!@}
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSLJAC_DECL_H_INCLUDED*/
