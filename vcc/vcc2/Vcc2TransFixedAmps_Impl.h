/**
 *******************************************************************************
 * 
 * @file    Vcc2TransFixedAmps_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSFIXEDAMPS_IMPL_H_INCLUDED
#define VCC2TRANSFIXEDAMPS_IMPL_H_INCLUDED

#include "util/type_traits/TypeName.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
Vcc2TransFixedAmps<T, T2ONLY, OPDEF_T>::Vcc2TransFixedAmps
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const bool aOnlyOneMode
   ,  const Uin aIoLevel
   ,  const bool aTimeIt
   )
   :  Vcc2TransBase<T, OPDEF_T>
         (  apOpDef
         ,  arRawModInts
         ,  arMcr
         ,  aOnlyOneMode
         ,  aIoLevel
         ,  aTimeIt
         )
   ,  mrTAmps(arTAmps)
   ,  mT1ModInts(ConstructT1ModInts(arRawModInts, arTAmps))
{
   this->SanityCheckDc(mrTAmps, "mrTAmps in Vcc2TransFixedAmps");
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
std::string Vcc2TransFixedAmps<T, T2ONLY, OPDEF_T>::PrettyClassName
   (
   )  const
{
   return std::string("Vcc2TransFixedAmps<")+midas::type_traits::TypeName<T>()+">";
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
typename Vcc2TransFixedAmps<T, T2ONLY, OPDEF_T>::modalintegrals_t Vcc2TransFixedAmps<T, T2ONLY, OPDEF_T>::ConstructT1ModInts
   (  modalintegrals_t aModInts
   ,  const datacont_t& arTAmps
   )  const
{
   if constexpr (!T2ONLY)
   {
      if (gDebug || this->IoLevel() > I_10 || this->TimeIt())
      {
         Mout  << this->PrettyClassName() << "; T1-transforming integrals" << std::endl;
      }
      clock_t t0 = clock();
      aModInts.T1Transform(this->Extract1ModeParams(arTAmps));
      if (this->TimeIt())
      {
         Mout  << this->PrettyClassName() << "; T1-transform: " << fclock_t(clock() - t0)/CLOCKS_PER_SEC << " s" << std::endl;
      }
   }
   return aModInts;
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSFIXEDAMPS_IMPL_H_INCLUDED*/
