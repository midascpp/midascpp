/**
 *******************************************************************************
 * 
 * @file    Vcc2TransEta_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSETA_IMPL_H_INCLUDED
#define VCC2TRANSETA_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/TypeName.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/vcc2/Vcc2ContribsEta.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *    
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >  
Vcc2TransEta<T, T2ONLY, OPDEF_T>::Vcc2TransEta
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr      
   ,  const bool aOnlyOneMode
   )  
   :  Vcc2TransBase<T, OPDEF_T>
         (  apOpDef
         ,  arRawModInts                 
         ,  arMcr                        
         ,  aOnlyOneMode
         ) 
{     
}     

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
std::string Vcc2TransEta<T, T2ONLY, OPDEF_T>::PrettyClassName
   (
   )  const
{
   return std::string("Vcc2TransEta<")+midas::type_traits::TypeName<T>()+">";
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
std::string Vcc2TransEta<T, T2ONLY, OPDEF_T>::PrettyTransformerType
   (
   )  const
{
   return "<ref|exp(-T)[X,tau_nu]exp(T)|ref> (eta vec. if X = H)";
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
void Vcc2TransEta<T, T2ONLY, OPDEF_T>::TransformImpl
   (  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   // T1-transform a copy of raw modal integrals.
   std::unique_ptr<modalintegrals_t> t1_mod_ints = nullptr;
   if constexpr (!T2ONLY)
   {
      t1_mod_ints = std::make_unique<modalintegrals_t>(this->RawModInts());
      t1_mod_ints->T1Transform(this->Extract1ModeParams(arIn));
   }
   const auto& mod_ints = T2ONLY ? this->RawModInts() : *t1_mod_ints;

   vcc2contribseta_t vcc2contribseta
      (  this->pOpDef()
      ,  mod_ints
      );

   // Calculate contributions.
   if (!this->OnlyOneMode())
   {
      TwoModeTransformerImpl(vcc2contribseta, arIn, arOut);
   }
   else
   {
      OneModeTransformerImpl(vcc2contribseta, arIn, arOut);
   }

   // Finalize here or in base?
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
void Vcc2TransEta<T, T2ONLY, OPDEF_T>::TwoModeTransformerImpl
   (  const vcc2contribseta_t& arVcc2Contribs
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   const auto& vcc2contribseta = arVcc2Contribs;
   const ModeCombiOpRange& mcr = this->Mcr();

   OneModeTransformerImpl(vcc2contribseta, arIn, arOut);

   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;
      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);

      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      vcc2contribseta.TwoModeEta2(mc,res);
      In addr = mc.Address()+AddrOffsetDcOut();
      arOut.DataIo(IO_PUT, res, res.Size(), addr);
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   >
void Vcc2TransEta<T, T2ONLY, OPDEF_T>::OneModeTransformerImpl
   (  const vcc2contribseta_t& arVcc2Contribs
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   const auto& vcc2contribseta = arVcc2Contribs;
   const ModeCombiOpRange& mcr = this->Mcr();
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);
      vcc2contribseta.TwoModeEta1(mc,res);
      In addr = mc.Address()+AddrOffsetDcOut();
      arOut.DataIo(IO_PUT, res, res.Size(), addr);
   }
}


} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSETA_IMPL_H_INCLUDED*/
