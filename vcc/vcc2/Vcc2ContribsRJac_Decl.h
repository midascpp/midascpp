/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsRJac_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSRJAC_DECL_H_INCLUDED
#define VCC2CONTRIBSRJAC_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2ContribsReg.h"

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class is for calculating contributions to the right-hand
    * Jacobian transformation, with elements
    * \f{align}{
    *    \rho_\mu 
    *    &= \langle \mu \vert \exp(-T_2) [ \widetilde{H}, R ] \exp(T_2) \vert \Phi \rangle
    *    \\
    *    &= \langle \mu \vert \exp(-T_2) \bar{H} \exp(T_2) \vert \Phi \rangle
    *    +  \langle \mu \vert \exp(-T_2) [ \widetilde{H}, R_2 ] \exp(T_2) \vert \Phi \rangle
    * \f}
    * where \f$ \widetilde{H}, \bar{H} \f$ are \f$T1\f$- and
    * \f$R1\f$-transformed, respectively.
    *
    * The contributions to for this right-hand Jacobian transformation have the
    * same form as those for the VCC error vector, but with other uses of
    * T-amplitudes and T1/R1-transformed integrals. The use of R1-transformed
    * integrals is triggered by calling methods with `aRsp == true`, but this
    * also entails some other algorithmic changes needed for the R-Jacobian
    * transformation -- you'll have to look into the theory and/or code for the
    * details. :O
    *
    * @note
    *    Client is responsible for correctly setting up intermediates
    *    (TwoModeIntermeds), that are consistent with the other data members of
    *    this object. The intermediates shall be calculated before calling
    *    these methods, _except_ for the _Y intermediates_ which are calculated
    *    by TwoModeRefT2() and stored in the argument TwoModeIntermeds, to be
    *    later used in TwoModeM0M1T2T2().
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2ContribsRJac
      :  public Vcc2ContribsReg<T, OPDEF_T>
   {
      public:
         using typename Vcc2ContribsReg<T, OPDEF_T>::value_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::absval_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::eref_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::twomodeintermeds_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::midasvector_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::datacont_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2ContribsReg<T, OPDEF_T>::opdef_t;
         using typename Vcc2ContribsBase<T, OPDEF_T>::intop_t;

         Vcc2ContribsRJac(const Vcc2ContribsRJac&) = delete;
         Vcc2ContribsRJac& operator=(const Vcc2ContribsRJac&) = delete;
         Vcc2ContribsRJac(Vcc2ContribsRJac&&) = default;
         Vcc2ContribsRJac& operator=(Vcc2ContribsRJac&&) = default;
         ~Vcc2ContribsRJac() override = default;

         //! Construct with T-amps., T1-integrals and R-coefs., R1-integrals.
         Vcc2ContribsRJac
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const modalintegrals_t& arR1ModInts
            ,  const datacont_t& arRCoefs
            ,  const absval_t aScreenNorm2 = absval_t(0)
            );

      protected:
         //!{
         //! Access to the R-Jacobian specific members.
         const modalintegrals_t& R1Ints() const override {return mrR1Ints;}
         const datacont_t& RCoefs() const override {return mrRCoefs;}
         const std::vector<absval_t>& RCoefsNorms2() const override {return mrRCoefsNorms2;}
         //!}

      private:
         //! The R1-transformed modal integrals.
         const modalintegrals_t& mrR1Ints;

         //! The R-coefficients.
         const datacont_t& mrRCoefs;

         //! Norms^2 of R-coefficients of each ModeCombi block.
         const std::vector<absval_t> mrRCoefsNorms2;

         //! Is this a Vcc2ContribsRJac object? (Yes!)
         bool IsRJac() const override {return true;}

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSRJAC_DECL_H_INCLUDED*/
