/**
 *******************************************************************************
 * 
 * @file    Vcc2TransEta_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSETA_DECL_H_INCLUDED
#define VCC2TRANSETA_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransBase.h"

// Forward declarations.
namespace midas::vcc::vcc2
{
   template<typename,typename> class Vcc2ContribsEta;
} /* namespace midas::vcc::vcc2 */

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      ,  bool T2ONLY = false
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2TransEta final
      :  public Vcc2TransBase<T, OPDEF_T>
   {
      public:
         using typename Vcc2TransBase<T, OPDEF_T>::value_t;
         using typename Vcc2TransBase<T, OPDEF_T>::absval_t;
         using typename Vcc2TransBase<T, OPDEF_T>::energy_t;
         using typename Vcc2TransBase<T, OPDEF_T>::opdef_t;
         using typename Vcc2TransBase<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2TransBase<T, OPDEF_T>::midasvector_t;
         using typename Vcc2TransBase<T, OPDEF_T>::datacont_t;

         Vcc2TransEta(const Vcc2TransEta&) = delete;
         Vcc2TransEta& operator=(const Vcc2TransEta&) = delete;
         Vcc2TransEta(Vcc2TransEta&&) = default;
         Vcc2TransEta& operator=(Vcc2TransEta&&) = default;
         ~Vcc2TransEta() override = default;

         Vcc2TransEta
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const bool aOnlyOneMode = false
            );

      private:
         using vcc2contribseta_t = midas::vcc::vcc2::Vcc2ContribsEta<value_t,opdef_t>;

         std::string PrettyClassName() const override;
         std::string PrettyTransformerType() const override;

         In AddrOffsetDcOut() const override {return -1;}

         //!
         void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const override;

         void TwoModeTransformerImpl
            (  const vcc2contribseta_t& arVcc2Contribs
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
         void OneModeTransformerImpl
            (  const vcc2contribseta_t& arVcc2Contribs
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;

   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSETA_DECL_H_INCLUDED*/
