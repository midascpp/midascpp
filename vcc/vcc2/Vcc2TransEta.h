/**
 *******************************************************************************
 * 
 * @file    Vcc2TransEta.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSETA_H_INCLUDED
#define VCC2TRANSETA_H_INCLUDED

#include "vcc/vcc2/Vcc2TransEta_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "vcc/vcc2/Vcc2TransEta_Impl.h"
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*VCC2TRANSETA_H_INCLUDED*/
