/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsReg_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSREG_DECL_H_INCLUDED
#define VCC2CONTRIBSREG_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2ContribsBaseAmps.h"

// Forward declarations.
template<typename> class GeneralMidasVector;
template<typename,typename> class TwoModeIntermeds;
class ModeCombi;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class is for calculating contributions to the VCC error
    * vector (for a "regular" state specific calculation), with elements
    * \f[
    *    e_\mu = \langle \mu \vert \exp(-T_2) \widetilde{H} \exp(T_2) \vert \Phi \rangle
    * \f]
    * where the Hamiltonian modal integrals has been \f$ T_1 \f$-transformed.
    *
    * (_Or_, some of the same methods can be used for calculating contributions
    * to a VCI transformations, in which case the modal integrals have not been
    * transformed.)
    *
    * The same methods are used for right-hand Jacobian transformations (but
    * with other input parameters; R-coefficients instead of/in addition to
    * T-amplitudes, R1-transformed integrals instead of T1-transformed ones);
    * the behaviour of using R1-trans. modal integrals instead of T1-trans.
    * ones (plus some other algorithmic changes in the methods) is determined
    * by providing booleans to the method calls.
    * Calling these methods with `aRsp == true` from an object of this class
    * will cause a hard error, however. Use the derived class Vcc2ContribsRJac
    * for that.
    *
    * Additionally, there are some members for screening away certain terms in
    * some contributions.
    *
    * @note
    *    Client is responsible for correctly setting up intermediates
    *    (TwoModeIntermeds), that are consistent with the other data members of
    *    this object. The intermediates shall be calculated before calling
    *    these methods, _except_ for the _Y intermediates_ which are calculated
    *    by TwoModeRefT2() and stored in the argument TwoModeIntermeds, to be
    *    later used in TwoModeM0M1T2T2().
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2ContribsReg
      :  public Vcc2ContribsBaseAmps<T, OPDEF_T>
   {
      public:
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::value_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::absval_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::opdef_t;
         using typename Vcc2ContribsBaseAmps<T, OPDEF_T>::datacont_t;
         using typename Vcc2ContribsBase<T, OPDEF_T>::intop_t;
         using eref_t = value_t;
         using midasvector_t = GeneralMidasVector<value_t>;
         using twomodeintermeds_t = TwoModeIntermeds<value_t,opdef_t>;

         Vcc2ContribsReg(const Vcc2ContribsReg&) = delete;
         Vcc2ContribsReg& operator=(const Vcc2ContribsReg&) = delete;
         Vcc2ContribsReg(Vcc2ContribsReg&&) = default;
         Vcc2ContribsReg& operator=(Vcc2ContribsReg&&) = default;
         ~Vcc2ContribsReg() override = default;

         //! Operator, T-amps. (or C-coefs.), screening, and VCC/VCI bool.
         Vcc2ContribsReg
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const absval_t aScreenNorm2 = absval_t(0)
            ,  const bool aDoingVccNotVci = true
            );

         /******************************************************************//**
          * @name Contributions
          *********************************************************************/
         //!@{
         template<bool aRsp = false>
         void TwoModeRefRef(value_t& aRes) const;
         void TwoModeRefT1(value_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         void TwoModeRefT2(value_t& aRes, twomodeintermeds_t& aIntermeds) const;
         template<bool aRsp = false>
         void TwoModeM0ref(const LocalModeNr aM0, midasvector_t& aRes) const;
         void TwoModeM0T1(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const;
         template<bool aRsp = false>
         void TwoModeM0T2
            (  const In aM0
            ,  midasvector_t& aRes
            ,  const twomodeintermeds_t& aIntermeds
            )  const;
         template<bool aRsp = false>
         void TwoModeM0M1ref(const ModeCombi& aMc, midasvector_t& aRes) const;
         void TwoModeM0M1T1(const ModeCombi& aMc, midasvector_t& aRes) const;
         template<bool aRsp = false>
         void TwoModeM0M1T2
            (  const ModeCombi& aMc
            ,  midasvector_t& aRes
            ,  const twomodeintermeds_t& aIntermeds
            ,  const value_t ref_ref = value_t(0)
            ,  clock_t* const aTmr0=nullptr
            ,  clock_t* const aTmr1=nullptr
            ,  In* const aTotTerms0=nullptr
            ,  In* const aScreened0=nullptr
            ,  In* const aTotTerms1=nullptr
            ,  In* const aScreened1=nullptr
            )  const;
         template<bool aRsp = false>
         void TwoModeM0M1T2T2
            (  const ModeCombi& aMc
            ,  midasvector_t& aRes
            ,  const twomodeintermeds_t& aIntermeds
            ,  const twomodeintermeds_t* const aC2Intermeds=nullptr
            ,  clock_t* const aTmr0=nullptr
            ,  clock_t* const aTmr1=nullptr
            ,  In* const aTotTerms=nullptr
            ,  In* const aScreened=nullptr
            )  const;
         //!@}

      protected:
         /******************************************************************//**
          * @name Get/set
          *********************************************************************/
         //!@{
         //! Calling this causes a hard error.
         virtual const modalintegrals_t& R1Ints() const;

         //! Calling this causes a hard error.
         virtual const datacont_t& RCoefs() const;

         //! Calling this causes a hard error.
         virtual const std::vector<absval_t>& RCoefsNorms2() const;

         //! The screening threshold used.
         absval_t ScreenNorm2() const {return mScreenNorm2;}

         //! Whether the object is intended to do VCC or VCI.
         bool DoingVccNotVci() const {return mDoingVccNotVci;}
         //!@}

      private:
         using modalintegrals_cont_t = typename modalintegrals_t::ContT;

         //! The screening threshold used.
         const absval_t mScreenNorm2;

         //! Whether the object is intended to do VCC or VCI.
         const bool mDoingVccNotVci;

         //! Is this a Vcc2ContribsRJac object? (No!)
         virtual bool IsRJac() const {return false;}

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSREG_DECL_H_INCLUDED*/
