/**
************************************************************************
* 
* @file                TwoModeIntermeds_Impl.h
*
* Created:             21-5-2007
*
* Author:              Peter Seidler (seidler@chem.au.dk)
* Author:              Andreas Buchgraitz Jensen (buchgraitz@chem.au.dk) 
*
* Short Description:   Organizing intermediates for specific VCC[2]
*                      calculation.
* 
* Last modified: 16 Nov 2020
* 
* Detailed  Description: 
*     Houses intermediates and their methods for all VCC[2] related 
*     computations i.e. VCC[2], TDVCC[2], TDVCI[2], TDMVCC[2]
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <map>

// My headers:
#include "vcc/vcc2/TwoModeIntermeds.h"
#include "inc_gen/math_link.h" 
#include "input/OpDef.h"
#include "td/tdvcc/trf/ConstraintOpDef.h"

template<typename T, typename OPDEF_T>
In TwoModeIntermeds<T, OPDEF_T>::Nmodals
   (  LocalModeNr aMode
   )  const
{
   return mNmodals[aMode];
}

template<typename T, typename OPDEF_T>
TwoModeIntermeds<T, OPDEF_T>::TwoModeIntermeds()
{
}

template<typename T, typename OPDEF_T>
TwoModeIntermeds<T, OPDEF_T>::TwoModeIntermeds(std::vector<In> aNmodals, const OPDEF_T* aOpDef,
                                   const ModeCombiOpRange& arMcr, bool aZ)
{
   Initialize(std::move(aNmodals), aOpDef, arMcr, aZ);
}

/**
 * Allocate space for intermediates.
 * Z intermediates are only relevant for VCI and are optional.
 *
 * @param[in] aNmodals
 *    The number of modals for each mode.
 * @param[in] aOpDef
 *    The operator.
 * @param[in] arMcr
 *    Shall contain the ModeCombi%s of the excitation amplitude/coef. range.
 * @param[in] aZ
 *    Optionally calculate Z intermediates.
 */
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::Initialize(std::vector<In> aNmodals, const OPDEF_T* aOpDef, 
                                  const ModeCombiOpRange& arMcr, bool aZ)
{
   mNmodals = std::move(aNmodals);

   // Allocate space for Y intermediates.
   In n_modes = aOpDef->NmodesInOp();
   mY.resize(n_modes);
   for (auto it=mY.begin(); it!=mY.end(); ++it)
   {
      it->resize(n_modes);
      for (auto y=it->begin(); y!=it->end(); ++y)
         *y = C_0;
   }
   
   // Initialize X / XC intermediate addresses.
   mXaddr.resize(aOpDef->NmodesInOp());
   mXC.resize(aOpDef->NmodesInOp());
   In i=I_0;
   for (auto it=mXaddr.begin(); it!=mXaddr.end(); ++it)
   {
      mXC[i].resize(aOpDef->NmodesInOp());
      it->resize(aOpDef->NmodesInOp());
      for (auto jt=it->begin(); jt!=it->end(); ++jt)
         (*jt) = -I_1;
      ++i;
   }

  
   In x_addr = I_0; 
   for(auto it_mc = arMcr.Begin(2), end = arMcr.End(2); it_mc != end; ++it_mc)
   {
      const auto& mc = *it_mc;

      LocalModeNr m0 = mc.Mode(I_0);
      LocalModeNr m1 = mc.Mode(I_1);
      In opers0 = aOpDef->NrOneModeOpers(m0);
      In opers1 = aOpDef->NrOneModeOpers(m1);
      In modals0 = this->Nmodals(m0);
      In modals1 = this->Nmodals(m1);
      In nvirm0 = modals0 - I_1;
      In nvirm1 = modals1 - I_1;

      mXaddr[m0][m1] = x_addr;
      x_addr += opers1 * (modals0);   // Need space for modals0-1 coefficients and 1 norm^2.
      mXaddr[m1][m0] = x_addr;
      x_addr += opers0 * (modals1);

   }
   mX.SetNewSize(x_addr);
   mX.Zero();

   if (aZ)
   {
      this->InitializeZ(aOpDef);
   }
}

/************************************************************************//**
 * @brief
 *    Initializes the TwoModeIntermediates used specifically for the
 *    Tdmvcc2 implementation.
 ***************************************************************************/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::InitializeTdmvcc2Intermeds(const opdef_t& aOpDef, const ModeCombiOpRange& aMcr)
{
   if constexpr (std::is_same_v<OPDEF_T, midas::tdvcc::constraint::ConstraintOpDef>)
   {
      return;
   }
   else
   {
      const auto nmodes = aOpDef.NmodesInOp();
      mIntermedW.SetNewSize(nmodes, nmodes, false, false);
      mIntermedXL.SetNewSize(nmodes, nmodes, false, false);
      mIntermedWS.SetNewSize(nmodes, nmodes, false, false);
      mIntermedWS_FAB.SetNewSize(nmodes, nmodes, false, false);
      mTraceV.SetNewSize(nmodes, nmodes, false, false);
      mIntermed3bch.SetNewSize(nmodes, nmodes, false, false);
      mIntermedVirFMatSum.SetNewSize(nmodes, nmodes, false, false);

      // Get the maximum number of oper indices to set the size of vectors in Mcr loop below
      In max_oper = -I_1;
      for (In i = I_0; i < aOpDef.Nterms(); ++i)
      {
         if (! aOpDef.IsCouplingTerm(i) )
         {
            continue; //skip all one mode terms
         }
         const auto& opers = aOpDef.GetOpers(i);
         const auto oper_m = opers[0];
         const auto oper_m0 = opers[1];
         if (max_oper <= oper_m0 || max_oper <= oper_m)  // <= because we += I_1
         {
            max_oper = (oper_m0 > oper_m) ? oper_m0 : oper_m;
            max_oper += I_1;  // Add 1 to have access to index [oper_m0]
         }
      }

      for (auto it_mc = aMcr.Begin(2), end = aMcr.End(2); it_mc != end; ++it_mc)
      {
         const auto& mc = *it_mc;
         const auto m = mc.Mode(0);
         const auto m0 = mc.Mode(1);

         mIntermedW[m][m0].SetNewSize(max_oper, false);
         mIntermedXL[m][m0].SetNewSize(max_oper, false);
         mIntermedWS[m][m0].SetNewSize(max_oper, false);
         mIntermedWS_FAB[m][m0].SetNewSize(max_oper, false);
         mTraceV[m][m0].SetNewSize(max_oper, false);
         mIntermed3bch[m][m0].SetNewSize(max_oper, false);

         mIntermedW[m0][m].SetNewSize(max_oper, false);
         mIntermedXL[m0][m].SetNewSize(max_oper, false);
         mIntermedWS[m0][m].SetNewSize(max_oper, false);
         mIntermedWS_FAB[m0][m].SetNewSize(max_oper, false);
         mTraceV[m0][m].SetNewSize(max_oper, false);
         mIntermed3bch[m0][m].SetNewSize(max_oper, false);
         for (In oper_idx = I_0; oper_idx < max_oper; ++oper_idx)
         {
            mIntermedW[m][m0][oper_idx].SetNewSize(0, false);
            mIntermedXL[m][m0][oper_idx].SetNewSize(0, false);
            mIntermedWS[m][m0][oper_idx].SetNewSize(0, false);
            mIntermedWS_FAB[m][m0][oper_idx].SetNewSize(0, false);
            mTraceV[m][m0][oper_idx] = T(0.0);
            mIntermed3bch[m][m0][oper_idx].SetNewSize(0, false);

            mIntermedW[m0][m][oper_idx].SetNewSize(0, false);
            mIntermedXL[m0][m][oper_idx].SetNewSize(0, false);
            mIntermedWS[m0][m][oper_idx].SetNewSize(0, false);
            mIntermedWS_FAB[m0][m][oper_idx].SetNewSize(0, false);
            mTraceV[m0][m][oper_idx] = T(0.0);
            mIntermed3bch[m0][m][oper_idx].SetNewSize(0, false);
         }
      }
      mAmplProdSum.SetNewSize(nmodes, nmodes, false, true);
   }
}

/************************************************************************//**
 * @brief
 *    Initializes the TwoModeIntermediates used specifically for the
 *    Tdmvcc2 implementation. And need Initializing before LJac transformer
 *    is called.
 ***************************************************************************/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::InitializeTdmvcc2IntermedsBeforeLJac(const opdef_t* aOpDef)
{
   const auto nmodes = aOpDef->NmodesInOp();
   mAmplProd.SetNewSize(nmodes, nmodes, false, true);
}

/**
 * Allocate space for Z intermediates.
 *
 * @param[in] aOpDef
 *    The operator.
 */
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::InitializeZ(const OPDEF_T* aOpDef)
{
   if (  this->ZInitialized()
      )
   {
      MIDASERROR("Z intermediates should only be initialized once!");
   }

   mZ.resize(aOpDef->NmodesInOp());
   LocalModeNr i_m = I_0;
   for (auto m = mZ.begin(); m != mZ.end(); ++m)
   {
      m->resize(aOpDef->NrOneModeOpers(i_m++), value_t(0));
   }
}

/**
 * Generating (m1, oper1)XC(m0) intermediates.
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::InitializeXCintermeds(const OPDEF_T* aOpDef)
{
   midasvector_t x_intermed;
   for (LocalModeNr m1=I_0; m1<aOpDef->NmodesInOp(); ++m1)
   {
      std::list<In> act_terms;
      for (In i_term=I_0; i_term<aOpDef->NactiveTerms(m1); ++i_term)
      {
         In term = aOpDef->TermActive(m1, i_term);
         if (aOpDef->NfactorsInTerm(term) != I_1)
            act_terms.push_back(term);
      }

      while (false == act_terms.empty())
      {
         In first_term = act_terms.front();

         // Set up std::vectors to store combined X intermediates.
         // Reserve space for coefficients and norm^2. Then set size to hold only the
         // coefficients. Norm will be put in later.
         std::vector<std::unique_ptr<midasvector_t>> xc_intermeds(aOpDef->NmodesInOp());
         std::vector<bool> xc_intermeds_exist(aOpDef->NmodesInOp());
         for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
         {
            xc_intermeds[m0] = std::make_unique<midasvector_t>();
            In modals = this->Nmodals(m0);
            xc_intermeds[m0]->Reserve(modals);
            xc_intermeds[m0]->SetNewSize(modals-I_1);
            xc_intermeds[m0]->Zero();
            xc_intermeds_exist[m0] = false;
         }
         
         LocalOperNr cur_oper = I_0;     // Operator specific for intermediate currently being created.
         if (aOpDef->ModeForFactor(first_term, I_0) == m1)
            cur_oper = aOpDef->OperForFactor(first_term, I_0);
         else
            cur_oper = aOpDef->OperForFactor(first_term, I_1);

         // Now generate (m1, cur_oper)XC(m0).
         std::list<In>::iterator it_term = act_terms.begin();
         while (it_term != act_terms.end())
         {
            In term = *it_term;

            LocalOperNr oper1 = I_0;
            LocalModeNr m2 = I_0;
            LocalOperNr oper2 = I_0;
            if (aOpDef->ModeForFactor(term, I_0) == m1)
            {
               oper1 = aOpDef->OperForFactor(term, I_0);
               m2 = aOpDef->ModeForFactor(term, I_1);
               oper2 = aOpDef->OperForFactor(term, I_1);
            }
            else
            {
               oper1 = aOpDef->OperForFactor(term, I_1);
               m2 = aOpDef->ModeForFactor(term, I_0);
               oper2 = aOpDef->OperForFactor(term, I_0);
            }
            
            if (oper1 != cur_oper)
            {
               ++it_term;
               continue;
            }
            it_term = act_terms.erase(it_term);
            
            value_t coef = aOpDef->Coef(term);
            for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
            {
               if (m0 == m2)
                  continue;
               
               // Adding (m2,oper2)X(m0) to (m1, oper1)XC(m0).
               if (GetXintermed(m0, m2, oper2, x_intermed))
               {
                  xc_intermeds_exist[m0] = true;
                  *(xc_intermeds[m0]) += coef * x_intermed;
               }
            }
         }

         for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
            if (xc_intermeds_exist[m0])
            {
               absval_t norm2 = xc_intermeds[m0]->Norm2();
               In modals = this->Nmodals(m0);
               xc_intermeds[m0]->SetNewSize(modals, true);
               (*xc_intermeds[m0])[modals-I_1] = norm2;
               mXC[m0][m1].push_back(std::make_pair(cur_oper, std::move(xc_intermeds[m0])));
            }
      }
   }
}

template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, const midasvector_t& aVec)
{
   // Test for consistency.
   if (-I_1 == mXaddr[aM0][aM1])
   {
      Mout << "TwoModeIntermed::PutXintermed()" << endl
           << "   aM0 =  " << aM0 << endl
           << "   aM1 =  " << aM1 << endl
           << "   oper = " << oper << endl;
      Error("TwoModeIntermed::PutXintermed(): Trying to put non-existing intermed");
   }

   In addr = mXaddr[aM0][aM1];
   In n_mod = this->Nmodals(aM0);
   addr += oper * (n_mod);

   for (In i=I_0; i<(n_mod-I_1); ++i)
   {
      mX[addr+i] = aVec[i];
   }

   mX[addr+n_mod-I_1] = aVec.Norm2();
}

template<typename T, typename OPDEF_T>
bool TwoModeIntermeds<T, OPDEF_T>::GetXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, midasvector_t& aVec) const
{
   In addr = mXaddr[aM0][aM1];
   if (-I_1 == addr)
   {
      return false;
   }

   In n_mod = this->Nmodals(aM0);
   addr += oper * n_mod;

   aVec.SetNewSize(n_mod-I_1);
   for (In i=I_0; i<(n_mod-I_1); ++i)
      aVec[i] = mX[addr+i];

   return true;
}

/**
 * @param[in] aM0
 * @param[in] aM1
 * @param[in] oper
 * @param[out] aNorm2
 *    If function returns false, this argument is unmodified.
 *    If function returns true, this is the norm^2 of the (aM0,aM1,oper) X-intermeds.
 *    Note (MBH, Jan 2019): aNorm2 is set to the _real_ part an mX element
 *    (which has value_t, can be complex), so we rely on mX[add+n_mod-I_1]
 *    actually being the norm^2, i.e. with only a real component. Scary...
 * @return
 *    Whether any X-intermeds. are stored, i.e. whether the function call was successful.
 **/
template<typename T, typename OPDEF_T>
bool TwoModeIntermeds<T, OPDEF_T>::GetXnorm2(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, absval_t& aNorm2) const
{
   In addr = mXaddr[aM0][aM1];
   if (-I_1 == addr)
      return false;

   In n_mod = this->Nmodals(aM0);
   addr += oper * n_mod;
   aNorm2 = std::real(mX[addr+n_mod-I_1]);
   return true;
}

template<typename T, typename OPDEF_T>
bool TwoModeIntermeds<T, OPDEF_T>::GetXCintermedsVector
   (  const LocalModeNr aM0
   ,  const LocalModeNr aM1
   ,  std::vector<midasvector_t>& aXC
   )  const
{
   bool res = false;
   const auto nOper = mXC[aM0][aM1].size();
   aXC.resize(nOper);
   for (auto it = mXC[aM0][aM1].begin(); it != mXC[aM0][aM1].end(); ++it)
   {
      In op1 = it->first;
      In coefs = it->second->Size() - I_1;
      aXC[op1].SetNewSize(coefs);
      for (In i=I_0; i<coefs; ++i)
      {
         aXC[op1][i] = (*(it->second))[i];
      }
      res = true;
   }
   return res;
}

template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutAmplProdintermed
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const value_t aVal
   )
{
   mAmplProd[aM][aM0] = aVal;
}

template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutAmplProdIntermedSum
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const value_t aVal
   )
{
   mAmplProdSum[aM][aM0] = aVal;
}

/**
 * @brief
 *    Stores intermediates of type (tilde{h}^{m0 O^m0}_{i b} * l^{m0 m}_{b a}) (which is a vector)
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 * @param[in] aVec
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutIntermedW
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   ,  const midasvector_t& aVec
   )
{
   if (mIntermedW[aM][aM0][aOperMode_M0].Size() != I_0)
   {
      MIDASERROR("Intermed W already exists !");
   }
   mIntermedW[aM][aM0][aOperMode_M0].SetNewSize(aVec.Size(), false);
   for (In i = I_0; i < aVec.Size(); ++i)
   {
      mIntermedW[aM][aM0][aOperMode_M0][i] = aVec[i];
   }
}

/**
 * @brief
 *    Retrieves intermediates of type (tilde{h}^{m0 O^m0}_{i b} * l^{m0 m}_{b a}) (which is a vector)
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedW
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   )  const
{
   return mIntermedW[aM][aM0][aOperMode_M0];
}


/**
 * @brief
 *    Stores intermediates of type sum_{m1} (^m0O^m0 X^m1 * l^{m1 m0})
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 * @param[in] aVec
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutIntermedXL
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   ,  const midasvector_t& aVec
   )
{
   if (mIntermedXL[aM][aM0][aOperMode_M0].Size() != I_0)
   {
      MIDASERROR("IntermedXL already exists !");
   }
   mIntermedXL[aM][aM0][aOperMode_M0].SetNewSize(aVec.Size(), false);
   for (In i = I_0; i < aVec.Size(); ++i)
   {
      mIntermedXL[aM][aM0][aOperMode_M0][i] = aVec[i];
   }
}

/**
 * @brief
 *    Retrieves intermediates of type sum_{m1} (^m0O^m0 X^m1 * l^{m1 m0})
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedXL
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   )  const
{
   return mIntermedXL[aM][aM0][aOperMode_M0];
}

/**
 * @brief
 *    Stores intermediates of type sum_{m1} (^m0O^m0 W^m1 * s^{m1 m0})
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 * @param[in] aVec
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutIntermedWS
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   ,  const midasvector_t& aVec
   )
{
   if (mIntermedWS[aM][aM0][aOperMode_M0].Size() != I_0)
   {
      MIDASERROR("IntermedWS already exists !");
   }
   mIntermedWS[aM][aM0][aOperMode_M0].SetNewSize(aVec.Size(), false);
   for (In i = I_0; i < aVec.Size(); ++i)
   {
      mIntermedWS[aM][aM0][aOperMode_M0][i] = aVec[i];
   }
}

/**
 * @brief
 *    Retrieves intermediates of type sum_{m1} (^m0O^m0 W^m1 * s^{m1 m0})
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedWS
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   )  const
{
   return mIntermedWS[aM][aM0][aOperMode_M0];
}

/**
 * @brief
 *    Stores intermediates of type sum_{m1 != m, m0} (s^{m m1} * ^mO^m W^m1)
 *    For FullActiveBasis.
 * @param[in] aM0
 * @param[in] aM
 * @param[in] aOperMode_M
 * @param[in] aVec
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutIntermedWS_FAB
   (  const LocalModeNr aM0
   ,  const LocalModeNr aM
   ,  const LocalOperNr aOperMode_M
   ,  const midasvector_t& aVec
   )
{
   if (mIntermedWS_FAB[aM0][aM][aOperMode_M].Size() != I_0)
   {
      MIDASERROR("IntermedWS_FAB already exists !");
   }
   mIntermedWS_FAB[aM0][aM][aOperMode_M].SetNewSize(aVec.Size(), false);
   for (In i = I_0; i < aVec.Size(); ++i)
   {
      mIntermedWS_FAB[aM0][aM][aOperMode_M][i] = aVec[i];
   }
}

/**
 * @brief
 *    Retrieves intermediates of type sum_{m1 != m, m0} (s^{m m1} * ^mO^m W^m1)
 *    For FullActiveBasis.
 * @param[in] aM0
 * @param[in] aM
 * @param[in] aOperMode_M
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedWS_FAB
   (  const LocalModeNr aM0
   ,  const LocalModeNr aM
   ,  const LocalOperNr aOperMode_M
   )  const
{
   return mIntermedWS_FAB[aM0][aM][aOperMode_M];
}

/**
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 * @param[in] aVal
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutTraceVIntermed
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   ,  const value_t aVal
   )
{
   if (mTraceV[aM][aM0][aOperMode_M0] != value_t(0.0))
   {
      MIDASERROR("IntermedTraceV already exists !");
   }
   mTraceV[aM][aM0][aOperMode_M0] = aVal;
}

/**
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 **/
template<typename T, typename OPDEF_T>
typename TwoModeIntermeds<T, OPDEF_T>::value_t TwoModeIntermeds<T, OPDEF_T>::GetTraceVIntermed
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   )  const
{
   return mTraceV[aM][aM0][aOperMode_M0];
}


/**
 * @param[in] aClusterAmps
 * @param[in] aLambdaCoefs
 * @param[in] aNModes
 * @param[in] aMcr
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::CalcIntermedSumLS
   (  const GeneralDataCont<value_t>& aClusterAmps
   ,  const GeneralDataCont<value_t>& aLambdaCoefs
   ,  const In aNModes
   ,  const ModeCombiOpRange& aMcr
   )
{
   mIntermedSumLS.SetNewSize(aNModes, aNModes, false, false);
   for (Uin m = I_0; m < aNModes; ++m)
   {
      for (Uin m1 = I_0; m1 < aNModes; ++m1)
      {
         const In nvirm = this->Nmodals(m) - 1;
         const In nvirm1 = this->Nmodals(m1) - 1;
         mat_t ls_m2_sum_mat(nvirm, nvirm1, value_t(0.0));
         value_t ls_inner_prod_sum(0.0);
         for (Uin m2 = I_0; m2 < aNModes; ++m2)
         {
            // Sum of inner products of L and S intermeds
            if (m2 > m)
            {
               ls_inner_prod_sum -= this->GetAmplProdintermed(m, m2);
            }
            else if (m2 < m)
            {
               ls_inner_prod_sum -= this->GetAmplProdintermed(m2, m);
            }
            // Now if statements also check m1 != aM to avoid subtracting MC: (aM, aM0) twice.
            if (m2 > m1 && m2 != m)
            {
               ls_inner_prod_sum -= this->GetAmplProdintermed(m1, m2);
            }
            else if (m2 < m1 && m2 != m)
            {
               ls_inner_prod_sum -= this->GetAmplProdintermed(m2, m1);
            }

            // X*L*S type intermed
            if (m2 == m || m2 == m1)
            {
               continue;
            }
            const In nvirm2 = this->Nmodals(m2) - 1;
            mat_t s_m_m2, l_m1_m2;
            GetAmplMat(s_m_m2, nvirm, nvirm2, aClusterAmps, m, m2, aMcr);
            GetAmplMat(l_m1_m2, nvirm1, nvirm2, aLambdaCoefs, m1, m2, aMcr);
            mat_t temp_mat;
            if (m < m2 && m1 < m2)
            {
               temp_mat = mat_t(s_m_m2 * Transpose(l_m1_m2));
            }
            else if (m < m2 && m1 > m2)
            {
               temp_mat = mat_t(s_m_m2 * l_m1_m2);
            }
            else if (m > m2 && m1 < m2)
            {
               temp_mat = mat_t(Transpose(s_m_m2) * Transpose(l_m1_m2));
            }
            else if (m > m2 && m1 > m2)
            {
               temp_mat = mat_t(Transpose(s_m_m2) * l_m1_m2);
            }
            else
            {
               MIDASERROR("This should not happen!");
            }
            ls_m2_sum_mat += temp_mat;
         }
         mIntermedSumLS[m][m1] = std::move(ls_m2_sum_mat);
         this->PutAmplProdIntermedSum(m, m1, ls_inner_prod_sum);
      }
   }
}

/**
 * @param[in] aM
 * @param[in] aM0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::mat_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedSumLS
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   )  const
{
   return mIntermedSumLS[aM][aM0];
}

/**
 * @param[in] aNModes
 * @param[in] aLambdaCoefs
 * @param[in] aClusterAmps
 * @param[in] aMcr
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::CalcIntermedVirFMatSum
   (  const In& aNModes
   ,  const GeneralDataCont<value_t>& aLambdaCoefs
   ,  const GeneralDataCont<value_t>& aClusterAmps
   ,  const ModeCombiOpRange& aMcr
   )
{
   for (In m = I_0; m < aNModes; ++m)
   {
      const In nvirm = this->Nmodals(m) - 1;
      for (In m0 = I_0; m0 < aNModes; ++m0)
      {
         mat_t s_l_sum(nvirm, nvirm, value_t(0.0));

         for (In m1 = I_0; m1 < aNModes; ++m1)
         {
            const auto nvirm1 = this->Nmodals(m1) - 1;
            if (m1 == m || m1 == m0)
            {
               continue;
            }
            mat_t l_m_m1, s_m_m1;
            GetAmplMat(l_m_m1, nvirm, nvirm1, aLambdaCoefs, m, m1, aMcr);
            GetAmplMat(s_m_m1, nvirm, nvirm1, aClusterAmps, m, m1, aMcr);
            if (m < m1)
            {
               s_l_sum += s_m_m1 * Transpose(l_m_m1);
            }
            else
            {
               s_l_sum += Transpose(s_m_m1) * l_m_m1;
            }
         }
         s_l_sum.Transpose();
         mIntermedVirFMatSum[m][m0] = std::move(s_l_sum);
      }
   }
}

/**
 * @param[in] aM
 * @param[in] aM0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::mat_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermedVirFMatSum
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   )  const
{
   return mIntermedVirFMatSum[aM][aM0];
}

/**
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 * @param[in] aVec
 **/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::PutIntermed3bch
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   ,  const midasvector_t& aVec
   )
{
   if (mIntermed3bch[aM][aM0][aOperMode_M0].Size() != I_0)
   {
      MIDASERROR("Intermed3bch already exists !");
   }
   mIntermed3bch[aM][aM0][aOperMode_M0].SetNewSize(aVec.Size(), false);
   for (In i = I_0; i < aVec.Size(); ++i)
   {
      mIntermed3bch[aM][aM0][aOperMode_M0][i] = aVec[i];
   }
}

/**
 * @param[in] aM
 * @param[in] aM0
 * @param[in] aOperMode_M0
 **/
template<typename T, typename OPDEF_T>
const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& TwoModeIntermeds<T, OPDEF_T>::GetIntermed3bch
   (  const LocalModeNr aM
   ,  const LocalModeNr aM0
   ,  const LocalOperNr aOperMode_M0
   )  const
{
   return mIntermed3bch[aM][aM0][aOperMode_M0];
}

/************************************************************************//**
 * @brief
 *    Retrives lambdacoefs/clusteramps as matrix 
 * @param[in] arMat
 *    Matrix to copy stuff into.
 * @param[in] arNvirm1
 *    size tells the number of virtual amplitudes for mode 1 there is
 * @param[in] arNvirm2
 *    size tells the number of virtual amplitudes for mode 2 there is
 * @param[in] Ampl
 *    Ampl is the container of data we want a part of 
 *    (aLambdaCoefs or aClusterAmps in above methods)
 * @param[in] m1
 *    First mode. Used to calculate address for ampl.DataIo
 * @param[in] m2
 *    Second mode. Used to calculate address for ampl.DataIo
 * @param[in] aMcr
 *    ModeCombiOpRange for calcualtion.
 ***************************************************************************/
template<typename T, typename OPDEF_T>
void TwoModeIntermeds<T, OPDEF_T>::GetAmplMat
   (  mat_t& arMat
   ,  const Uin arNvirm1
   ,  const Uin arNvirm2
   ,  const GeneralDataCont<value_t>& Ampl
   ,  const In m1
   ,  const In m2
   ,  const ModeCombiOpRange& aMcr
   )  const
{         
   std::vector<In> mc = (m1 < m2) ? std::vector<In>{m1, m2} : std::vector<In>{m2, m1};
   Uin Nrows, Ncols;
   if (m1 < m2)
   {
      Nrows = arNvirm1;
      Ncols = arNvirm2;
   }
   else
   {
      Nrows = arNvirm2;
      Ncols = arNvirm1;
   }
   using const_iterator = std::vector<ModeCombi>::const_iterator;
   const_iterator it_mc;
   aMcr.Find(mc, it_mc);
   const auto addr = it_mc->Address(); 
   arMat.SetNewSize(Nrows, Ncols, false, false);
   Ampl.DataIo(IO_GET, arMat, Nrows*Ncols, Nrows, Ncols, addr);
}


template<typename T, typename OPDEF_T>
void TMI_DirProd(const TwoModeIntermeds<T, OPDEF_T>& aIntermed0, const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOperM1,
      const TwoModeIntermeds<T, OPDEF_T>& aIntermed2, const LocalModeNr aM2, const LocalModeNr aM3, const LocalOperNr aOperM3,
      typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aRes)
{
   In addr0 = aIntermed0.mXaddr[aM0][aM1];
   In n_mod0 = aIntermed0.Nmodals(aM0);
   addr0 += aOperM1 * n_mod0;

   In addr2 = aIntermed2.mXaddr[aM2][aM3];
   In n_mod2 = aIntermed2.Nmodals(aM2);
   addr2 += aOperM3 * n_mod2;

   for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
      for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
         aRes[idx0*(n_mod2-I_1)+idx2] = aIntermed0.mX[addr0+idx0] * aIntermed2.mX[addr2+idx2];
}  


template<typename T, typename OPDEF_T>
void TMI_DirProd(const TwoModeIntermeds<T, OPDEF_T>& aX, const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOp1,
                 const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aC, const LocalModeNr aM2, typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aRes)
{
   In addr = aX.mXaddr[aM0][aM1];
   In n_mod0 = aX.Nmodals(aM0);
   addr += aOp1*n_mod0;

   In n_mod2 = aX.Nmodals(aM2);

   if (aM0 < aM2)
      for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
         for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
            aRes[idx0*(n_mod2-I_1)+idx2] = aX.mX[addr+idx0] * aC[idx2];
   else
      for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
         for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
            aRes[idx2*(n_mod0-I_1)+idx0] = aC[idx2] * aX.mX[addr+idx0];
}


template<typename T, typename OPDEF_T>
std::ostream& operator<<(std::ostream& aOut, const TwoModeIntermeds<T, OPDEF_T>& aArg)
{
   Mout << "Y(m0,m1):" << endl;
   Mout << "Printing not implemented yet." << endl;

   Mout << endl << "(m1,oper)X(m0,a):" << endl;
   Mout << "mX.Size() = " << aArg.mX.Size() << endl;
  
   In i_m0 = I_0;
   for (auto m0=aArg.mXaddr.begin(); m0!=aArg.mXaddr.end(); ++m0)
   {
      aOut << "m0 = " << i_m0 << endl;
      In i_m1 = I_0;
      for (auto m1=m0->begin(); m1!=m0->end(); ++m1)
      {
         aOut << "   m1 = " << i_m1 << "      addr: " << *m1 << endl;
         i_m1++;
      }
      i_m0++;
   }
   
   Mout << endl << "(m0,oper)Z:" << endl;
   i_m0 = I_0;
   for (auto m0 = aArg.mZ.begin(); m0 != aArg.mZ.end(); ++m0)
   {
      aOut << "m0 = " << i_m0 << "  ";
      for (auto oper = m0->begin(); oper != m0->end(); ++oper)
         aOut << "  " << *oper;
      aOut << endl;
      i_m0++;
   }
   
   return aOut;
}


///**
// * @brief Clone intermediates
// * 
// * @return
// *    Copy of this
// */
//TwoModeIntermeds<T, OPDEF_T> TwoModeIntermeds<T, OPDEF_T>::Clone() const
//{
//   TwoModeIntermeds<T, OPDEF_T> intermeds;
//
//   // Most members are easy to copy
//   intermeds.mNmodals = this->mNmodals;
//   intermeds.mXaddr = this->mXaddr;
//   intermeds.mX = this->mX;
//   intermeds.mY = this->mY;
//   intermeds.mZ = this->mZ;
//
//   // Take care with mXC
//   std::vector<std::vector<std::list<std::pair<In,std::unique_ptr<midasvector_t>> > > > mXC;
//   const In size1 = this->mXC.size();
//   intermeds.mXC.resize(size1);
//   for(In i=I_0; i<size1; ++i)
//   {
//      const In size2 = this->mXC[i].size();
//      intermeds.mXC[i].resize(size2);
//      for(In j=I_0; j<size2; ++j)
//      {
//         for(const auto& l : this->mXC[i][j])
//         {
//            auto p = std::make_pair(l.first, std::make_unique<midasvector_t>(*l.second.get()));
//            intermeds.mXC[i][j].emplace_back(std::move(p));
//         }
//      }
//   }
//
//   return intermeds;
//}
