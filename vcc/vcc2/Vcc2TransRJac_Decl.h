/**
 *******************************************************************************
 * 
 * @file    Vcc2TransRJac_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSRJAC_DECL_H_INCLUDED
#define VCC2TRANSRJAC_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransFixedAmps.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

// Forward declarations.
namespace midas::vcc::vcc2
{        
   template<typename,typename> class Vcc2ContribsRJac;
} /* namespace midas::vcc::vcc2 */

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    For calculating _right_-hand transformations with the Jacobian of the
    *    VCC error vector.
    *
    * Calculates
    * \f[
    *    \rho_\mu
    *    = \sum_\nu A_{\mu\nu} r_\nu
    *    = \sum_\nu \langle \mu \vert \exp(-T) [H, \tau_\nu] \exp(T) \vert \Phi \rangle r_\nu
    *    = \langle \mu \vert \exp(-T) [H, R] \exp(T) \vert \Phi \rangle
    * \f]
    * with \f$A_{\mu,\nu}\f$ and \f$r_\nu\f$ being elements of the VCC error
    * vector Jacobian and the right-hand vector to be transformed,
    * respectively.
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * @note
    *    MBH, Mar 2019: For the purpose of debugging, I've had to take a deeper
    *    look at the original implementation, especially since the papers
    *    listed above do not go into much detail about the specific
    *    impementation of the right-hand Jacobian transformer, and the original
    *    code was largely undocumented.
    *    I therefor write below, what I am 99% certain is the
    *    derivation/implementation used, although I haven't looked all the way
    *    through the code.
    *    (For what it's worth, the issue was that whereas I had _thought_ --
    *    while refactoring the original implementation -- that setting the VCC
    *    energy was a mere _feature_ for transforming with the diagonal-shifted
    *    Jacobian \f$ (\mathbf{A} - E_{\text{VCC}} \mathbf{I}) \f$, it was
    *    _actually_ crucial for the correct calculation of the transformed
    *    result. In light of this I modified the class to be responsible itself
    *    for calculating the VCC energy (technically not the energy for general T)
    *    \f$E_{\text{VCC}}(T)
    *    = \langle\Phi\vert\exp(-T_2)\widetilde{H}\exp(T_2)\vert\Phi\rangle\f$
    *    corresponding to the given T-amplitudes.)
    *
    * Derivation of individual contributions
    * (where \f$\widetilde{H} = \exp(-T_1)H\exp(T_1)\f$ is the
    * \f$T_1\f$-transformed Hamiltonian, \f$\bar{H} = [\widetilde{H},R_1]\f$
    * the \f$R_1\f$-transformed one):
    * :
    * \f{align}{
    *    \rho_\mu
    *    &=
    *    \langle \mu \vert \exp(-T) [H, R] \exp(T) \vert \Phi \rangle
    *    \\&=
    *    \langle \mu \vert \exp(-T_2) [\widetilde{H}, R_1 + R_2] \exp(T_2) \vert \Phi \rangle
    *    \\&=
    *    \langle \mu \vert \exp(-T_2) \bar{H} \exp(T_2) \vert \Phi \rangle
    *    + \langle \mu \vert \exp(-T_2) \widetilde{H} \exp(T_2) R_2 \vert \Phi \rangle
    *    - \langle \mu \vert R_2 \exp(-T_2) \widetilde{H} \exp(T_2) \vert \Phi \rangle
    *    \\&=
    *    \langle \mu \vert \exp(-T_2) \bar{H} \exp(T_2) \vert \Phi \rangle
    *    +  \langle \mu \vert 
    *       (\widetilde{H} + [\widetilde{H},T_2] + \underbrace{\dots{}}_{=0^\text{(a)}})R_2
    *       \vert \Phi \rangle
    *    -  \langle \mu \vert R_2
    *       \Big(
    *          \vert\Phi\rangle\langle\Phi\vert
    *          + \sum_\nu \underbrace{\vert\nu\rangle}_{=0^\text{(b)}}\langle\nu\vert
    *       \Big)
    *       \exp(-T_2) \widetilde{H} \exp(T_2) \vert \Phi \rangle
    *    \\&=
    *    \langle\mu\vert \exp(-T_2) \bar{H} \exp(T_2) \vert\Phi\rangle
    *    +  \langle\mu\vert \widetilde{H}R_2 \vert\Phi\rangle
    *    +  \langle\mu\vert \widetilde{H}T_2 R_2 \vert\Phi\rangle
    *    -  \langle\mu\vert T_2
    *       \Big(
    *          \vert\Phi\rangle\langle\Phi\vert
    *          + \sum_\nu \underbrace{\vert\nu\rangle}_{=0^\text{(b)}}\langle\nu\vert
    *       \Big)
    *       \widetilde{H} R_2
    *       \vert\Phi\rangle
    *    -  r_{\mu_2} E_{\text{VCC}}(T)
    *    \\&=
    *    \langle\mu\vert \exp(-T_2) \bar{H} \exp(T_2) \vert\Phi\rangle
    *    +  \langle\mu\vert \widetilde{H}R_2 \vert\Phi\rangle
    *    +  \langle\mu_2\vert \widetilde{H}_2 T_2 R_2 \vert\Phi\rangle
    *    -  t_{\mu_2} \langle\Phi\vert \widetilde{H} R_2 \vert\Phi\rangle
    *    -  r_{\mu_2} E_{\text{VCC}}(T)
    * \f}
    * where some terms are zero because
    * -  (a) Terms \f$ \tfrac{1}{2!} [[\widetilde{H},T_2],T_2]R_2 \f$, etc.,
    *    would have excitation level 4 or higher, thus giving zero
    *    contributions with \f$ \langle\mu\vert \f$ of at most excitation level 2.
    * -  (b) \f$ X_2\vert\nu\rangle \f$ (with \f$ X_2 = T_2, R_2 \f$ and
    *    \f$ \vert\nu\rangle \f$ having excitation level at least 1) would have
    *    excitation level at least 3, thus giving zero contribution with
    *    \f$ \langle\mu\vert \f$ of at most excitation level 2.
    *
    * The first term is reminiscent of the VCC error vector, but with
    * \f$\bar{H}\f$ instead of \f$\widetilde{H}\f$, and some terms
    * automatically zero, because \f$\bar{H}\f$ cannot down-contract.
    * The \f$R_2\f$ terms have the same form as the VCC error vector
    * contributions, but with \f$R_2\f$ instead of \f$T_2\f$ in some places.
    ***************************************************************************/
   template
      <  typename T
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2TransRJac final
      :  public Vcc2TransFixedAmps<T, false, OPDEF_T>
   {
      public:
         using typename Vcc2TransBase<T, OPDEF_T>::value_t;
         using typename Vcc2TransBase<T, OPDEF_T>::absval_t;
         using typename Vcc2TransBase<T, OPDEF_T>::energy_t;
         using typename Vcc2TransBase<T, OPDEF_T>::opdef_t;
         using typename Vcc2TransBase<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2TransBase<T, OPDEF_T>::midasvector_t;
         using typename Vcc2TransBase<T, OPDEF_T>::datacont_t;

         Vcc2TransRJac(const Vcc2TransRJac&) = delete;
         Vcc2TransRJac& operator=(const Vcc2TransRJac&) = delete;
         Vcc2TransRJac(Vcc2TransRJac&&) = default;
         Vcc2TransRJac& operator=(Vcc2TransRJac&&) = default;
         ~Vcc2TransRJac() override = default;

         Vcc2TransRJac
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const bool aOnlyOneMode = false
            ,  const Uin aIoLevel = 0
            ,  const bool aTimeIt = false
            );

      private:
         using vcc2contribsrjac_t = midas::vcc::vcc2::Vcc2ContribsRJac<value_t,opdef_t>;
         using typename Vcc2TransBase<T, OPDEF_T>::intermeds_t;
         using typename Vcc2TransBase<T, OPDEF_T>::fclock_t;
         using typename Vcc2TransBase<T, OPDEF_T>::fscr_t;

         const std::pair<intermeds_t,energy_t> mPairT2IntermedsVccEnergy;

         const intermeds_t& T2Intermeds() const {return mPairT2IntermedsVccEnergy.first;}
         energy_t EVccForTAmps() const {return mPairT2IntermedsVccEnergy.second;}

         std::string PrettyClassName() const override;
         std::string PrettyTransformerType() const override;

         std::pair<intermeds_t,energy_t> ConstructT2Intermeds(const datacont_t& arParams) const;
         intermeds_t ConstructR2Intermeds(const datacont_t& arParams) const;

         In AddrOffsetDcOut() const override {return -1;}

         //!
         void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const override;

         void TwoModeTransformerImpl
            (  const vcc2contribsrjac_t& arVcc2Contribs
            ,  intermeds_t& arR2Intermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
         void OneModeTransformerImpl
            (  const vcc2contribsrjac_t& arVcc2Contribs
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSRJAC_DECL_H_INCLUDED*/
