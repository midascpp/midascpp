/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsBase_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSBASE_IMPL_H_INCLUDED
#define VCC2CONTRIBSBASE_IMPL_H_INCLUDED

// Standard headers.
#include <sstream>

// Midas headers.
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Vcc2ContribsBase<T, OPDEF_T>::Vcc2ContribsBase
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   )
   :  mpOpDef(apOpDef)
   ,  mrModInts(arModInts)
   ,  mNModals(VecNModals(arModInts))
{
   SanityCheck();
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsBase<T, OPDEF_T>::CheckTransType
   (  const modalintegrals_t& arModInts
   ,  const modalintegrals_trans_t& arExpType
   ,  const std::string& arExpString
   )
{
   if (arModInts.GetType() != arExpType)
   {
      MIDASERROR("Expected "+arExpString+" integrals, but these are "+arModInts.GetTypeString()+".");
   }
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
std::vector<Uin> Vcc2ContribsBase<T, OPDEF_T>::VecNModals
   (  const modalintegrals_t& arModInts
   )
{
   std::vector<Uin> v;
   v.reserve(arModInts.NModes());
   for(LocalModeNr m = 0; m < arModInts.NModes(); ++m)
   {
      v.emplace_back(arModInts.NModals(m));
   }
   return v;
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsBase<T, OPDEF_T>::SanityCheck
   (
   )  const
{
   std::stringstream ss;

   // Num. modes.
   if (  pOpDef()->NmodesInOp() != ModInts().NModes()
      || pOpDef()->NmodesInOp() != VecNModals().size()
      )
   {
      ss << "Num. modes mismatch; OpDef (= " << pOpDef()->NmodesInOp()
         << "), ModalIntegrals (= " << ModInts().NModes()
         << "), VecNModals() size (= " << VecNModals().size()
         << ")."
         ;
      MIDASERROR(ss.str());
   }

   // Num. modals.
   for(const auto& n: VecNModals())
   {
      if (n < I_1)
      {
         MIDASERROR("Found a num.modals < 1; this is invalid.");
      }
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSBASE_IMPL_H_INCLUDED*/
