/**
 *******************************************************************************
 * 
 * @file    Vcc2TransFixedAmps_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSFIXEDAMPS_DECL_H_INCLUDED
#define VCC2TRANSFIXEDAMPS_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransBase.h"
#include "vcc/ModalIntegrals.h"

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      ,  bool T2ONLY = false
      ,  typename OPDEF_T = OpDef
      >
   class Vcc2TransFixedAmps
      :  public Vcc2TransBase<T, OPDEF_T>
   {
      public:
         using typename Vcc2TransBase<T, OPDEF_T>::value_t;
         using typename Vcc2TransBase<T, OPDEF_T>::absval_t;
         using typename Vcc2TransBase<T, OPDEF_T>::energy_t;
         using typename Vcc2TransBase<T, OPDEF_T>::opdef_t;
         using typename Vcc2TransBase<T, OPDEF_T>::modalintegrals_t;
         using typename Vcc2TransBase<T, OPDEF_T>::midasvector_t;
         using typename Vcc2TransBase<T, OPDEF_T>::datacont_t;
         using typename Vcc2TransBase<T, OPDEF_T>::fclock_t;

         Vcc2TransFixedAmps(const Vcc2TransFixedAmps&) = delete;
         Vcc2TransFixedAmps& operator=(const Vcc2TransFixedAmps&) = delete;
         Vcc2TransFixedAmps(Vcc2TransFixedAmps&&) = default;
         Vcc2TransFixedAmps& operator=(Vcc2TransFixedAmps&&) = default;
         virtual ~Vcc2TransFixedAmps() = default;

      protected:
         Vcc2TransFixedAmps
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const bool aOnlyOneMode = false
            ,  const Uin aIoLevel = 0
            ,  const bool aTimeIt = false
            );

         const datacont_t& TAmps() const {return mrTAmps;}
         const modalintegrals_t& T1ModInts() const {return mT1ModInts;}

      private:
         const datacont_t& mrTAmps;
         const modalintegrals_t mT1ModInts;

         std::string PrettyClassName() const override;

         modalintegrals_t ConstructT1ModInts(modalintegrals_t, const datacont_t&) const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSFIXEDAMPS_DECL_H_INCLUDED*/
