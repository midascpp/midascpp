/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsBaseAmps_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSBASEAMPS_IMPL_H_INCLUDED
#define VCC2CONTRIBSBASEAMPS_IMPL_H_INCLUDED

// Standard headers.
#include <sstream>

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Vcc2ContribsBaseAmps<T, OPDEF_T>::Vcc2ContribsBaseAmps
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   )
   :  Vcc2ContribsBase<T, OPDEF_T>
      (  apOpDef
      ,  arModInts
      )
   ,  mrMcr(arMcr)
   ,  mMcAddresses(VecMcAddresses(arMcr, this->VecNModals()))
   ,  mrTAmps(arTAmps)
   ,  mTAmpsNorms2(this->CalcNorms2(this->TAmps(), this->McAddresses()))
{
   SanityCheck();
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Uin Vcc2ContribsBaseAmps<T, OPDEF_T>::NexciForModeCombi
   (  const Uin aMc
   )  const
{
   return mMcAddresses[aMc+I_1] - mMcAddresses[aMc];
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
std::vector<Uin> Vcc2ContribsBaseAmps<T, OPDEF_T>::VecMcAddresses
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arNModals
   )
{
   std::vector<Uin> v;
   v.reserve(arMcr.Size() + I_1);
   Uin a = 0;
   for(const auto& mc: arMcr)
   {
      v.emplace_back(a);
      a += NumParams(mc, arNModals);
   }
   v.emplace_back(a);
   return v;
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
std::vector<typename Vcc2ContribsBaseAmps<T, OPDEF_T>::absval_t> Vcc2ContribsBaseAmps<T, OPDEF_T>::CalcNorms2
   (  const datacont_t& arParams
   ,  const std::vector<Uin>& arMcAddresses
   )
{
   MidasAssert(!arMcAddresses.empty(), "arMcAddresses must be non-empty.");
   MidasAssert(arParams.Size() == arMcAddresses.back(), "Size mismatch, arParams and arMcAddresses.");

   std::vector<absval_t> v_norms2;
   v_norms2.reserve(arMcAddresses.size() - 1);
   for(Uin i = 0; i + 1 < arMcAddresses.size(); ++i)
   {
      Uin addr = arMcAddresses[i];
      Uin n_exci = arMcAddresses[i+1] - arMcAddresses[i];
      v_norms2.emplace_back(std::real(arParams.Norm2(addr, n_exci)));
   }
   return v_norms2;
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsBaseAmps<T, OPDEF_T>::SanityCheck
   (
   )  const
{
   std::stringstream ss;

   // ModeCombiOpRange should contain the empty ModeCombi as well as all
   // 1-ModeCombis, _and_ the modes should be 0,...,M-1; the implementations
   // rely on these assumptions in some/many cases.
   const auto& mcr = Mcr();
   if (mcr.NumEmptyMCs() == 0)
   {
      MIDASERROR("ModeCombiOpRange must contain the empty ModeCombi, but does not.");
   }
   Uin mc1_m = 0;
   std::vector<In> mc1_expect(1);
   for(auto it_mc1 = mcr.Begin(1), end = mcr.End(1); it_mc1 != end; ++it_mc1, ++mc1_m)
   {
      mc1_expect.at(0) = mc1_m;
      if (mc1_expect != it_mc1->MCVec())
      {
         ss << "Unexpected 1-ModeCombi; expected " << mc1_expect
            << ", got " << it_mc1->MCVec() << "."
            ;
         MIDASERROR(ss.str());
      }
   }
   if (mc1_m != this->ModInts().NModes())
   {
      ss << "Unexpected number of 1-ModeCombis; expected " << this->ModInts().NModes()
         << ", counted " << mc1_m
         ;
      MIDASERROR(ss.str());
   }

   // ModeCombi addresses.
   if (mMcAddresses.size() != mcr.Size()+I_1)
   {
      ss << "Num ModeCombi addresses mismatch; mMcAddresses.size() = " << mMcAddresses.size()
         << ", mcr.Size()+I_1 = " << mcr.Size()+I_1
         ;
      MIDASERROR(ss.str());
   }
   else
   {
      for(Uin i = 0; i < mcr.Size(); ++i)
      {
         if (mMcAddresses.at(i) != mcr.GetModeCombi(i).Address())
         {
            ss << "ModeCombi address mismatch; mMcAddresses.at("<<i<<") = " << mMcAddresses.at(i)
               << ", mcr.GetModeCombi("<<i<<").Address() = " << mcr.GetModeCombi(i).Address()
               ;
            MIDASERROR(ss.str());
         }
      }
      if (mMcAddresses.back() != TAmps().Size())
      {
         ss << "Last ModeCombi address (= " << mMcAddresses.back()
            << ") != parameter container size (= " << TAmps().Size()
            << ")."
            ;
         MIDASERROR(ss.str());
      }
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSBASEAMPS_IMPL_H_INCLUDED*/
