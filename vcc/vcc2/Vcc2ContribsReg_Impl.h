/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsReg_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSREG_IMPL_H_INCLUDED
#define VCC2CONTRIBSREG_IMPL_H_INCLUDED

// Midas headers.
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/vcc2/TwoModeIntermeds.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
Vcc2ContribsReg<T, OPDEF_T>::Vcc2ContribsReg
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const absval_t aScreenNorm2
   ,  const bool aDoingVccNotVci
   )
   :  Vcc2ContribsBaseAmps<T, OPDEF_T>
         (  apOpDef
         ,  arModInts
         ,  arMcr
         ,  arTAmps
         )
   ,  mScreenNorm2(aScreenNorm2)
   ,  mDoingVccNotVci(aDoingVccNotVci)
{
   SanityCheck();
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
const typename Vcc2ContribsReg<T, OPDEF_T>::modalintegrals_t& Vcc2ContribsReg<T, OPDEF_T>::R1Ints
   (
   )  const
{
   MIDASERROR("Cannot return R1-ints. for Vcc2ContribsReg<T, OPDEF_T> object.");
   return this->ModInts(); // To quench compiler warning.
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
const typename Vcc2ContribsReg<T, OPDEF_T>::datacont_t& Vcc2ContribsReg<T, OPDEF_T>::RCoefs
   (
   )  const
{
   MIDASERROR("Cannot return R-coefs. for Vcc2ContribsReg<T, OPDEF_T> object.");
   return this->TAmps(); // To quench compiler warning.
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
const std::vector<typename Vcc2ContribsReg<T, OPDEF_T>::absval_t>& Vcc2ContribsReg<T, OPDEF_T>::RCoefsNorms2
   (
   )  const
{
   MIDASERROR("Cannot return R-coefs. norm^2 for Vcc2ContribsReg<T, OPDEF_T> object.");
   return this->TAmpsNorms2(); // To quench compiler warning.
}

/***************************************************************************//**
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeRefRef<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeRefRef(value_t& aRes) const
{
   for (In i_term=I_0; i_term<this->pOpDef()->Nterms(); ++i_term)
   {
      // Outer loop is for rsp., i_r1_fac is the factor for which to use R1 integral.
      // If we are not doing response, we just break at the end of first iteration.
      for (In i_r1_fac=I_0; i_r1_fac<this->pOpDef()->NfactorsInTerm(i_term); ++i_r1_fac)
      {
         value_t val_term = this->pOpDef()->Coef(i_term);
         for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(i_term); ++i_fac)
         {
            LocalModeNr mode = this->pOpDef()->ModeForFactor(i_term, i_fac);
            LocalOperNr oper = this->pOpDef()->OperForFactor(i_term, i_fac);
            if (aRsp && i_fac==i_r1_fac)
               val_term *= this->R1Ints().GetOccElement(mode, oper);
            else
               val_term *= this->ModInts().GetOccElement(mode, oper);
         }
         aRes += val_term;

         if constexpr(!aRsp)
            break;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeRefT1(value_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   for (In i_term=I_0; i_term<this->pOpDef()->Nterms(); ++i_term)
   {
      value_t res_term = C_0;
      value_t coef = this->pOpDef()->Coef(i_term);

      LocalModeNr m0 = this->pOpDef()->ModeForFactor(i_term, I_0);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(i_term, I_0);

      if (this->pOpDef()->NfactorsInTerm(i_term) == I_1)
         res_term += coef * aIntermeds.GetZ(m0,oper0);
      else // We have two terms.
      {
         LocalModeNr m1 = this->pOpDef()->ModeForFactor(i_term, I_1);
         LocalOperNr oper1 = this->pOpDef()->OperForFactor(i_term, I_1);
         value_t pas0 = this->ModInts().GetOccElement(m0, oper0);
         value_t pas1 = this->ModInts().GetOccElement(m1, oper1);;
         res_term += coef * pas0 * aIntermeds.GetZ(m1,oper1);
         res_term += coef * pas1 * aIntermeds.GetZ(m0,oper0);
      }
      aRes += res_term;
   } 
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeRefT2(value_t& aRes, twomodeintermeds_t& aIntermeds) const
{
   // Calculates <ref| contribution and Y intermediates. 
   midasvector_t x_intermed;
   midasvector_t res(I_1, C_0);
   ModeCombi mc_empty;
   for (In i_term=I_0; i_term<this->pOpDef()->Nterms(); ++i_term)
   {
      if (this->pOpDef()->NfactorsInTerm(i_term) != I_2)
         continue;

      LocalModeNr m0 = this->pOpDef()->ModeForFactor(i_term, I_0);
      LocalModeNr m1 = this->pOpDef()->ModeForFactor(i_term, I_1);
      ModeCombi mc_m0(I_1);
      mc_m0.InsertMode(m0);

      // If operator contains ModeCombi%s not in the exci.param.
      // ModeCombiOpRange, then there might not be an X-intermediate for the
      // current operator term. This is (should be) signaled by GetXintermed.
      // If so, we'll check to make sure, the ModeCombi {m0,m1} really isn't
      // in the exci.param. MCR, as expected.
      if (!aIntermeds.GetXintermed(m0, m1, this->pOpDef()->OperForFactor(i_term, I_1), x_intermed))
      {
         const ModeCombi mc(std::set<In>{m0,m1},-1);
         if (this->Mcr().Contains(mc))
         {
            // Trouble; X-intermed. not found, but the ModeCombi _was_ in the exci. range.
            std::stringstream ss;
            ss << "X-intermediate not found for (m0,m1) = " << mc.MCVec()
               << ", but ModeCombi is contained in exci. range."
               << " (operator i_term = " << i_term << ")"
               ;
            MIDASERROR(ss.str());
         }
      }
      else
      {
         res[I_0] = C_0;
         this->ModInts().Contract(mc_empty, mc_m0, m0, this->pOpDef()->OperForFactor(i_term, I_0), res, x_intermed, -I_1);
         value_t contrib = this->pOpDef()->Coef(i_term) * res[I_0];
         aRes += contrib;
         aIntermeds.AddToY(m0,m1,contrib);
         aIntermeds.AddToY(m1,m0,contrib);
      }
   }
} 

/***************************************************************************//**
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeM0ref<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0ref(const LocalModeNr aM0, midasvector_t& aRes) const
{
   In n_act = this->pOpDef()->NactiveTerms(aM0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(aM0, i);

      for (In i_r1_fac=I_0; i_r1_fac<this->pOpDef()->NfactorsInTerm(term); ++i_r1_fac)
      {
         value_t fact = this->pOpDef()->Coef(term);
         midasvector_t res_up(aRes.Size(), C_0);
         for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(term); ++i_fac)
         {
            if (this->pOpDef()->ModeForFactor(term, i_fac) == aM0)
            {
               ModeCombi mc_up(I_1);
               mc_up.InsertMode(aM0);
               ModeCombi mc_empty;
               LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
               midasvector_t ref_vec(I_1, C_1);
               if (aRsp && i_fac == i_r1_fac)
                  this->R1Ints().Contract(mc_up, mc_empty, aM0, oper, res_up, ref_vec, I_1);
               else
                  this->ModInts().Contract(mc_up, mc_empty, aM0, oper,
                                                   res_up, ref_vec, I_1);
            }
            else
            {
               LocalModeNr mode = this->pOpDef()->ModeForFactor(term, i_fac);
               LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
               if (aRsp && i_fac == i_r1_fac)
                  fact *= this->R1Ints().GetOccElement(mode, oper);
               else
                  fact *= this->ModInts().GetOccElement(mode, oper);
            }
         }
         aRes += fact * res_up;

         if constexpr(!aRsp)
            break;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0T1(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   LocalModeNr m0 = aMc.Mode(I_0);
   In n_excis = this->NModals(m0)-I_1;
   midasvector_t excis(n_excis);
   this->TAmps().DataIo(IO_GET, excis, n_excis, aMc.Address());

   for (In i_term=I_0; i_term<this->pOpDef()->Nterms(); ++i_term)
   {
      midasvector_t forw_res(excis);
      value_t coef = this->pOpDef()->Coef(i_term);
      for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(i_term); ++i_fac)
      {
         LocalModeNr op_mode = this->pOpDef()->ModeForFactor(i_term, i_fac);
         LocalOperNr oper = this->pOpDef()->OperForFactor(i_term, i_fac);
         if (m0 == op_mode)
         {
            forw_res.Zero();
            this->ModInts().Contract(aMc, aMc, op_mode, oper, forw_res, excis, I_0);
         }
         else
         {
            coef *= this->ModInts().GetOccElement(op_mode, oper);
         }
      }
      aRes += coef * forw_res;
   }
   
   In n_act = this->pOpDef()->NactiveTerms(m0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);

      if (this->pOpDef()->NfactorsInTerm(term) == I_1)
         continue;

      value_t coef = this->pOpDef()->Coef(term);
      
      LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
      LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
      LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);

      midasvector_t res_up(n_excis, C_0);
      midasvector_t inp_up(I_1);
      ModeCombi mc_empty;
      if (m0 == op_m0)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m1,oper1) * coef;
         this->ModInts().Contract(aMc, mc_empty, op_m0, oper0, res_up, inp_up, I_1);
         aRes += res_up;
      }
      if (m0 == op_m1)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m0,oper0) * coef;
         this->ModInts().Contract(aMc, mc_empty, op_m1, oper1, res_up, inp_up, I_1);
         aRes += res_up;
      }
   }
}

/***************************************************************************//**
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeM0T2<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0T2
   (  const In aM0
   ,  midasvector_t& aRes
   ,  const twomodeintermeds_t& aIntermeds
   )  const
{
   const In m0 = aM0;

   // Taking care of terms in Hamiltonian with just one factor.
   for (In i_term=I_0; i_term<this->pOpDef()->Nterms(); ++i_term)
      if (this->pOpDef()->NfactorsInTerm(i_term) == I_1)
      {
         if constexpr(aRsp)
            continue;       // This factor will be R1 transformed and a down contraction gives 0.
         
         value_t coef = this->pOpDef()->Coef(i_term);
         LocalModeNr op_mode = this->pOpDef()->ModeForFactor(i_term, I_0);
         if (op_mode != m0)
         {
            LocalOperNr oper = this->pOpDef()->OperForFactor(i_term, I_0);
            midasvector_t intermed;
            if (aIntermeds.GetXintermed(aM0, op_mode, oper, intermed))
               aRes += coef * intermed;
         }
      } 

   // Taking care of terms in Hamiltonian with two factors.
   midasvector_t forw_res(aRes.Size());   
   ModeCombi mc_forw(I_1);
   mc_forw.InsertMode(aM0);
   
   const auto& xc_intermeds = aIntermeds.GetXCintermeds(m0,m0);
   for(auto it=xc_intermeds.begin(), end=xc_intermeds.end(); it!=end; ++it)
   {
      In op0 = it->first;
      forw_res.Zero();
      // Note: it->second has wrong dimension. May be a problem if Integrals::Contract()
      // implementation changes.
      if constexpr(aRsp)
         this->R1Ints().Contract(mc_forw, mc_forw, m0, op0, forw_res, *(it->second), I_0);
      else
         this->ModInts().Contract(mc_forw, mc_forw, m0, op0, forw_res, *(it->second), I_0);
      aRes += forw_res;
   }

   midasvector_t xc(aRes.Size());
   for (In m1=I_0; m1<this->pOpDef()->NmodesInOp(); ++m1)
   {
      if (m1 == m0)
         continue;

      const auto& xc_intermeds = aIntermeds.GetXCintermeds(m0,m1);
      for (auto it=xc_intermeds.begin(); it!=xc_intermeds.end(); ++it)
      {
         In op1 = it->first;
         for (In i=I_0; i<aRes.Size(); ++i)
            xc[i] = (*(it->second))[i];
         value_t pas1;
         if constexpr(aRsp)
            pas1 = this->R1Ints().GetOccElement(m1, op1);
         else
            pas1 = this->ModInts().GetOccElement(m1, op1);
         aRes += pas1 * xc;
      }
   }
}

/***************************************************************************//**
 * Do <m0,m1| H |ref>.
 *
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeM0M1ref<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0M1ref(const ModeCombi& aMc, midasvector_t& aRes) const
{
   LocalModeNr m0 = aMc.Mode(I_0);
   LocalModeNr m1 = aMc.Mode(I_1);

   // Make lists of terms with MC (m0,m1). Each list is identified
   // by the operator belonging to m0.
   In n_opers0 = this->pOpDef()->NrOneModeOpers(m0);
   std::vector<std::list<In>> act_terms(n_opers0);
   for (In i_term=I_0; i_term<this->pOpDef()->NactiveTerms(m0); ++i_term)
   {
      In term = this->pOpDef()->TermActive(m0, i_term);

      if ((this->pOpDef()->NfactorsInTerm(term) == I_2) &&
          (this->pOpDef()->ModeForFactor(term, I_1) == m1))
      {
         LocalOperNr op0 = this->pOpDef()->OperForFactor(term, I_0);
         act_terms[op0].push_back(term);
      }
   }
  
   // Loop over lists of terms. For each (m0,op0) pair do relevant up contractions
   // on m1 operator and sum results. Finally, do up with (m0,op0) operator. 
   ModeCombi mc_empty;
   ModeCombi up_mc1(I_1);
   up_mc1.InsertMode(m1);
   midasvector_t ref_vec(I_1);
   In exci0 = this->NModals(m0) - I_1;
   In exci1 = this->NModals(m1) - I_1;
   midasvector_t up_res1(exci1);
   midasvector_t up_res1_tot(exci1);
   midasvector_t up_res1_tot_rsp(exci1);
   midasvector_t up_res0(exci0*exci1);
   for (In op0=I_0; op0<n_opers0; ++op0)
   {
      up_res1_tot.Zero();
      if constexpr(aRsp)
         up_res1_tot_rsp.Zero();

      for (std::list<In>::const_iterator it_term=act_terms[op0].begin();
           it_term!=act_terms[op0].end(); ++it_term)
      {
         In term = *it_term;
         LocalOperNr op1 = this->pOpDef()->OperForFactor(term, I_1);
         value_t coef = this->pOpDef()->Coef(term);
         ref_vec[I_0] = coef;
         up_res1.Zero();
         this->ModInts().Contract(up_mc1, mc_empty, m1, op1, up_res1, ref_vec, I_1);
         up_res1_tot += up_res1;
         if constexpr(aRsp)
         {
            up_res1.Zero();
            this->R1Ints().Contract(up_mc1, mc_empty, m1, op1, up_res1, ref_vec, I_1);
            up_res1_tot_rsp += up_res1;
         }
            
      }
      up_res0.Zero();
      if constexpr(aRsp)
         this->R1Ints().Contract(aMc, up_mc1, m0, op0, up_res0, up_res1_tot, I_1);
      else
         this->ModInts().Contract(aMc, up_mc1, m0, op0, up_res0, up_res1_tot, I_1);
      aRes += up_res0;
      if constexpr(aRsp)
      {
         up_res0.Zero();
         this->ModInts().Contract(aMc, up_mc1, m0, op0, up_res0, up_res1_tot_rsp, I_1);
         aRes += up_res0;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0M1T1(const ModeCombi& aMc, midasvector_t& aRes) const
{
   for (In i_m = I_0; i_m<I_2; ++i_m)
   {
      // Get excitation coefficients for either m0 or m1.
      // I hope this is not too slow - we gotta get the coefficients somehow.
      LocalModeNr m_t = aMc.Mode(i_m);
      ModeCombi mc_t(I_1);
      mc_t.InsertMode(m_t);
      ModeCombiOpRange::const_iterator it_mc_t;
      this->Mcr().Find(mc_t, it_mc_t);
      In n_excis_t = this->NModals(m_t) - I_1;
      midasvector_t excis(n_excis_t);
      this->TAmps().DataIo(IO_GET, excis, n_excis_t, it_mc_t->Address());
      
      // The "other mode", i.e. the one which is not m_t.
      LocalModeNr m_other = aMc.Mode(I_1-i_m);

      // Generate up intermediates.
      In n_opers = this->pOpDef()->NrOneModeOpers(m_other);
      std::vector<midasvector_t> ups(n_opers);
      for (In i_op=I_0; i_op<n_opers; ++i_op)
      {
         ups[i_op].SetNewSize(aRes.Size());
         ups[i_op].Zero();
         this->ModInts().Contract(aMc, mc_t, m_other, i_op, ups[i_op], excis, I_1);
      }
      
      // Loop over terms containing "the other mode".
      midasvector_t res_forw(n_excis_t);
      midasvector_t res_up(aRes.Size());
      In n_act = this->pOpDef()->NactiveTerms(m_other);
      for (In i=I_0; i<n_act; ++i)
      {
         In term = this->pOpDef()->TermActive(m_other, i);
         value_t coef = this->pOpDef()->Coef(term);

         // Check if we have only one factor and take special care.
         if (this->pOpDef()->NfactorsInTerm(term) == I_1)
         {
            aRes += coef * ups[this->pOpDef()->OperForFactor(term, I_0)];
            continue;
         }
         
         LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
         LocalModeNr m_x = I_0;
         LocalOperNr op_x = I_0;
         LocalOperNr op_other = I_0;
         if (op_m0 == m_other)
         {
            m_x = op_m1;
            op_x = this->pOpDef()->OperForFactor(term, I_1);
            op_other = this->pOpDef()->OperForFactor(term, I_0);
         }
         else
         {
            m_x = op_m0;
            op_x = this->pOpDef()->OperForFactor(term, I_0);
            op_other = this->pOpDef()->OperForFactor(term, I_1);
         }

         if (m_x == m_t)
         {
            // Do forward + up.
            res_forw.Zero();
            this->ModInts().Contract(mc_t, mc_t, m_x, op_x, res_forw, excis, I_0);
            res_up.Zero();
            this->ModInts().Contract(aMc, mc_t, m_other, op_other, res_up, res_forw, I_1);
            aRes += coef * res_up;
         }
         else
         {
            // Use up intermediate and multiply passive term.
            value_t occ = this->ModInts().GetOccElement(m_x, op_x);
            aRes += coef * occ * ups[op_other];
         }
      }
   }
}

/***************************************************************************//**
 * Do <m0,m1| H T2 |ref> part.
 * aRsp: Use R1 Hamiltonian.
 * If you want the purely passive contributions to the forward part, pass the value
 * of TwoModeRefRef() as aRefRef. Passing zero (default) will cause the passive terms not
 * canelled by exp(-T) in VCC to be included.
 *
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeM0M1T2<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0M1T2
   (  const ModeCombi& aMc
   ,  midasvector_t& aRes
   ,  const twomodeintermeds_t& aIntermeds
   ,  const value_t aRefRef
   ,  clock_t* const aTmr0
   ,  clock_t* const aTmr1
   ,  In* const aTotTerms0
   ,  In* const aScreened0
   ,  In* const aTotTerms1
   ,  In* const aScreened1
   )  const
{
   if constexpr(aRsp)
   {
      if (!this->IsRJac())
      {
         MIDASERROR("Only RJac may call this with aRsp true.");
      }
   }

   // Get the T-amp/R-coef container depending on arguments to method, as well
   // as associated norms^2.
   const bool use_r_coefs = this->IsRJac() && !aRsp;
   const datacont_t& aExcis = use_r_coefs? this->RCoefs(): this->TAmps();
   const std::vector<absval_t>& aExciNorms2 = use_r_coefs? this->RCoefsNorms2(): this->TAmpsNorms2();

   In m0 = aMc.Mode(I_0);
   In m1 = aMc.Mode(I_1);
   In exci0 = this->NModals(m0)-I_1;
   In exci1 = this->NModals(m1)-I_1;
   midasvector_t amps(exci0*exci1, C_0);
   aExcis.DataIo(IO_GET, amps, amps.Size(), aMc.Address());

   // <m0,m1| H T(m0,m1) |ref> part.
   clock_t t0 = clock();
   const absval_t screen_norm2 = this->ScreenNorm2();
   absval_t max_norm2 = C_0;
   midasvector_t forw_res(amps.Size());
   value_t missing_passives = C_0;
   for (In i_mode=I_0; i_mode<I_2; ++i_mode)
   {
      LocalModeNr m_act = aMc.Mode(i_mode);
      LocalModeNr m_other = aMc.Mode(I_1-i_mode);
  
      // Prepare for h(m0,op) T(m0,m1) forward intermediates.
      In n_mact_ops = this->pOpDef()->NrOneModeOpers(m_act);
      vector<bool> forw_done(n_mact_ops);
      for (vector<bool>::iterator it=forw_done.begin(); it!=forw_done.end(); ++it)
         *it=false;
      std::vector<midasvector_t> forwards(n_mact_ops);

      // Prepare for response forward intermediates.
      vector<bool> forw_done_rsp;
      std::vector<midasvector_t> forwards_rsp;
      if constexpr(aRsp)
      {
         forw_done_rsp.resize(n_mact_ops);
         for (vector<bool>::iterator it=forw_done_rsp.begin(); it!=forw_done_rsp.end(); ++it)
            *it=false;
         forwards_rsp.resize(n_mact_ops);
      }
      
      In n_act = this->pOpDef()->NactiveTerms(m_act);
      for (In i_term=I_0; i_term<n_act; ++i_term)
      {
         In term = this->pOpDef()->TermActive(m_act, i_term);
         value_t coef = this->pOpDef()->Coef(term);
     
         In n_fac = this->pOpDef()->NfactorsInTerm(term);
         if (I_1 == n_fac)
         {
            (*aTotTerms0)++;
            LocalOperNr oper_act = this->pOpDef()->OperForFactor(term, I_0);
            if constexpr(aRsp)
               missing_passives += coef * this->R1Ints().GetOccElement(m_act, oper_act);
            else
            {
               missing_passives += coef * this->ModInts().GetOccElement(m_act, oper_act);
            }

            // Screening.
            if (screen_norm2 >= C_0)
            {
               max_norm2 = midas::util::AbsVal2(coef) * aExciNorms2[aMc.ModeCombiRefNr()];
               if constexpr(aRsp)       
                  max_norm2 *= this->R1Ints().GetNorm2(m_act, oper_act, modalintegrals_cont_t::FORWARD);  
               else
                  max_norm2 *= this->ModInts().GetNorm2(m_act, oper_act, modalintegrals_cont_t::FORWARD);  
            } 
            
            if (max_norm2 > screen_norm2)
            {
               if (false == forw_done[oper_act])
               {
                  forwards[oper_act].SetNewSize(amps.Size());
                  forwards[oper_act].Zero();
                  if constexpr(aRsp)
                     this->R1Ints().Contract(aMc, aMc, m_act, oper_act, forwards[oper_act], amps, I_0);
                  else
                     this->ModInts().Contract(aMc, aMc, m_act, oper_act,
                                                      forwards[oper_act], amps, I_0);
                  forw_done[oper_act] = true;
               }
               aRes += coef * forwards[oper_act];               
            }
            else
               (*aScreened0)++;

            // Check screening.
            /*
            midasvector_t check(coef*forwards[oper_act]);
            absval_t actual_norm2 = check.Norm2();
            if (actual_norm2 - actual_norm2/C_10_9 > max_norm2)
            {
               Mout << "Peter is stupid. Check 1." << endl
                    << "aMc:          " << aMc << endl
                    << "term:         " << term << endl
                    << "max_norm2:    " << max_norm2 << endl
                    << "actual_norm2: " << actual_norm2 << endl;
            }
            */
         }
         else // n_fac == 2
         {
            // Find out what factor corresponds to active mode.
            LocalOperNr oper_act=I_0;
            LocalOperNr oper_h=I_0;
            LocalModeNr mode_h;
            if (this->pOpDef()->ModeForFactor(term, I_0) == m_act)
            {
               oper_act = this->pOpDef()->OperForFactor(term, I_0);
               oper_h = this->pOpDef()->OperForFactor(term, I_1);      
               mode_h = this->pOpDef()->ModeForFactor(term, I_1);    // May be m_other or something else. 
            }
            else
            {
               oper_act = this->pOpDef()->OperForFactor(term, I_1);
               oper_h = this->pOpDef()->OperForFactor(term, I_0);
               mode_h = this->pOpDef()->ModeForFactor(term, I_0);    // May be m_other or something else.
            }
          
            if (m_other == mode_h && !aRsp)
               if (m_act != aMc.Mode(I_0))
                  continue;
            (*aTotTerms0)++;
           
            value_t pas_h = this->ModInts().GetOccElement(mode_h, oper_h);
            
            // Screening.
            if (screen_norm2 >= C_0)
            {
               max_norm2 = midas::util::AbsVal2(coef) * aExciNorms2[aMc.ModeCombiRefNr()];
               if constexpr(aRsp)
                  max_norm2 *= this->R1Ints().GetNorm2(m_act, oper_act, modalintegrals_cont_t::FORWARD);
               else
                  max_norm2 *= this->ModInts().GetNorm2(m_act, oper_act, modalintegrals_cont_t::FORWARD);

               if (mode_h == m_other)   // Screen 2 x forward.
                  max_norm2 *= this->ModInts().GetNorm2(mode_h, oper_h, modalintegrals_cont_t::FORWARD);
               else                     // 1 x forward + passive.
                  max_norm2 *= midas::util::AbsVal2(pas_h);
            }
           
            //midasvector_t check(aRes.Size()); 
            if (max_norm2 > screen_norm2)
            {
               // Forward the active factor.
               if (false == forw_done[oper_act])
               {
                  forwards[oper_act].SetNewSize(amps.Size());
                  forwards[oper_act].Zero();
                  if constexpr(aRsp)
                     this->R1Ints().Contract(aMc, aMc, m_act, oper_act, forwards[oper_act], amps, I_0);
                  else
                     this->ModInts().Contract(aMc, aMc, m_act, oper_act,
                                                      forwards[oper_act], amps, I_0);

                  forw_done[oper_act] = true;
               }

               if constexpr(aRsp)
                  missing_passives += coef * pas_h * this->R1Ints().GetOccElement(m_act, oper_act);
               else
                  missing_passives += coef * pas_h
                                          * this->ModInts().GetOccElement(m_act, oper_act);

               // Take care of the other factor.
               if (mode_h == m_other)
               {
                  forw_res.Zero();
                  this->ModInts().Contract(aMc, aMc, mode_h, oper_h, forw_res,
                                                   forwards[oper_act], I_0);
                  aRes += coef * forw_res;

                  // For checking screening.
                  //check = coef*forw_res;
               }
               else // Passive case.
               {
                  aRes += coef * pas_h * forwards[oper_act];
                  //check = coef * pas_h * forwards[oper_act];
               }
            }
            else
               (*aScreened0)++;

            // Check screening.
            /*
            absval_t actual_norm2 = check.Norm2();
            if (actual_norm2 - actual_norm2/C_10_9 > max_norm2)
            {
               Mout << "Peter is stupid. Check 2." << endl
                    << "aMc:          " << aMc << endl
                    << "term:         " << term << endl
                    << "max_norm2:    " << max_norm2 << endl
                    << "actual_norm2: " << actual_norm2 << endl;
            }
            */

            // If this term is passive we will never get here agian. Therefore, take care of
            // the second combination "h(m_act) [h(mode_h),R1] T(m_act, m_other)" for
            // response calculations.
            if (aRsp && (mode_h != m_other))
            {
               (*aTotTerms0)++;
               value_t pas_h_r1 = this->R1Ints().GetOccElement(mode_h, oper_h);;
               
               // Screening.
               if (screen_norm2 >= C_0)
               {
                  max_norm2 = midas::util::AbsVal2(coef) * aExciNorms2[aMc.ModeCombiRefNr()];
                  max_norm2 *= this->ModInts().GetNorm2(m_act, oper_act, modalintegrals_cont_t::FORWARD);
                  max_norm2 *= midas::util::AbsVal2(pas_h_r1);
               }
              
               if (max_norm2 > screen_norm2)
               { 
                  if (false == forw_done_rsp[oper_act])
                  {
                     forwards_rsp[oper_act].SetNewSize(amps.Size());
                     forwards_rsp[oper_act].Zero();
                     this->ModInts().Contract(aMc, aMc, m_act, oper_act,
                                                      forwards_rsp[oper_act], amps, I_0);
                     forw_done_rsp[oper_act] = true;
                  }
                  aRes += coef * pas_h_r1 * forwards_rsp[oper_act];
                  //check = coef * pas_h_r1 * forwards_rsp[oper_act];
                  value_t pas_act = this->ModInts().GetOccElement(m_act, oper_act);;
                  missing_passives += coef*pas_act*pas_h_r1;
               }
               else
                  (*aScreened0)++;
               
               // Check screening.
               /*
               absval_t actual_norm2 = check.Norm2();
               if (actual_norm2 - actual_norm2/C_10_9 > max_norm2)
               {
                  Mout << "Peter is stupid. Check 3." << endl
                       << "aMc:          " << aMc << endl
                       << "term:         " << term << endl
                       << "max_norm2:    " << max_norm2 << endl
                       << "actual_norm2: " << actual_norm2 << endl;
               }
               */
            }
         }
      }
   }
   missing_passives = aRefRef - missing_passives;
   aRes += missing_passives * amps;
   if (aTmr0 != nullptr) *aTmr0 += clock() - t0;
   
   // <m0,m1| H T(m0,m2) |ref> part.
   clock_t t1 = clock();
   midasvector_t up_res(exci0*exci1);
   const auto& xc_intermeds1 = aIntermeds.GetXCintermeds(m1, m0);
   for (auto it=xc_intermeds1.begin(); it!=xc_intermeds1.end(); ++it)
   {
      (*aTotTerms1)++;
      In op0 = it->first;
      midasvector_t* xc = it->second.get();
      absval_t norm2 = std::real((*xc)[this->NModals(m1)-I_1]);
      //^ Super scary stuff here; the XC intermediate vectors contain blocks of
      //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
      //as the Nmodals-1'th element. The intermeds. are of value_t while the
      //norm^2 is principally of absval_t but must be stored as value_t (with
      //zero imag. part if complex), since they're put in the same container as
      //the intermeds. themselves. Also we really have to rely on the ordering
      //of the XC vectors not changing _ever_... -MBH, Jan 2019.
      if constexpr(aRsp)
      {
         norm2 *= this->R1Ints().GetNorm2(m0, op0, modalintegrals_cont_t::UP);
      }
      else
      {
         norm2 *= this->ModInts().GetNorm2(m0, op0, modalintegrals_cont_t::UP);
      }
     
      if (norm2 > screen_norm2)
      { 
         up_res.Zero();
         ModeCombi xc_mc(I_1);
         xc_mc.InsertMode(m1);
         // NOTE: *xc has wrong dimension (norm at end). May cause problems if
         // Integrals::Contract() implementaion is changed.
         if constexpr(aRsp)
            this->R1Ints().Contract(aMc, xc_mc, m0, op0, up_res, *xc, I_1);
         else
            this->ModInts().Contract(aMc, xc_mc, m0, op0, up_res, *xc, I_1);
         aRes += up_res;
      }
      else
         (*aScreened1)++;

      // Test if sceening is ok.
      //if (up_res.Norm2()-norm2 > up_res.Norm2()/C_10_9/C_10_5)
      //   Mout << "Peter is stupid.  act. norm^2: " << up_res.Norm2()
      //        << "   pred. norm^2: " << norm2  << endl;
   }

   const auto& xc_intermeds0 = aIntermeds.GetXCintermeds(m0, m1);
   for (auto it=xc_intermeds0.begin(); it!=xc_intermeds0.end(); ++it)
   {
      (*aTotTerms1)++;
      In op1 = it->first;
      midasvector_t* xc = it->second.get();
      absval_t norm2 = std::real((*xc)[this->NModals(m0)-I_1]);
      //^ Super scary stuff here; the XC intermediate vectors contain blocks of
      //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
      //as the Nmodals-1'th element. The intermeds. are of value_t while the
      //norm^2 is principally of absval_t but must be stored as value_t (with
      //zero imag. part if complex), since they're put in the same container as
      //the intermeds. themselves. Also we really have to rely on the ordering
      //of the XC vectors not changing _ever_... -MBH, Jan 2019.
      if constexpr(aRsp)
      {
         norm2 *= this->R1Ints().GetNorm2(m1, op1, modalintegrals_cont_t::UP);
      }
      else
      {
         norm2 *= this->ModInts().GetNorm2(m1, op1, modalintegrals_cont_t::UP);
      }
     
      if (norm2 > screen_norm2)
      {
         up_res.Zero();
         ModeCombi xc_mc(I_1);
         xc_mc.InsertMode(m0);
         // NOTE: *xc has wrong dimension (norm at end). May cause problems if 
         // Integrals::Contract() implementaion is changed.
         if constexpr(aRsp)
            this->R1Ints().Contract(aMc, xc_mc, m1, op1, up_res, *(it->second), I_1);
         else
            this->ModInts().Contract(aMc, xc_mc, m1, op1, up_res, *(it->second), I_1);
         aRes += up_res;
      }
      else
         (*aScreened1)++;
      
      // Test if sceening is ok.
      //if (up_res.Norm2()-norm2 > up_res.Norm2()/C_10_9/C_10_5)
      //   Mout << "Peter is stupid.  act. norm^2: " << up_res.Norm2()
      //        << "   pred. norm^2: " << norm2  << endl;
   }
   if (aTmr1 != nullptr)
      *aTmr1 += clock() - t1;
}

/***************************************************************************//**
 * @note
 *    You _may_ in some cases need to call this method as 
 *    `obj.template TwoModeM0M1T2T2<...>(...)` in order to let the compiler know
 *    the function is templated.
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
template
   <  bool aRsp
   >
void Vcc2ContribsReg<T, OPDEF_T>::TwoModeM0M1T2T2
   (  const ModeCombi& aMc
   ,  midasvector_t& aRes
   ,  const twomodeintermeds_t& aIntermeds
   ,  const twomodeintermeds_t* const aR2Intermeds
   ,  clock_t* const aTmr0
   ,  clock_t* const aTmr1
   ,  In* const aTotTerms
   ,  In* const aScreened
   )  const
{
   if (aRsp && !this->IsRJac())
   {
      MIDASERROR("Only RJac may call this with aRsp true.");
   }

   LocalModeNr m0 = aMc.Mode(I_0);
   LocalModeNr m1 = aMc.Mode(I_1);
   In exci_0 = this->NModals(m0) - I_1;
   In exci_1 = this->NModals(m1) - I_1;

   // Doing <m0,m1| H T(m0,m1) T(m2,m3) |REF>
   clock_t t0 = clock();
   midasvector_t amps((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
   this->TAmps().DataIo(IO_GET, amps, amps.Size(), aMc.Address());
   
   value_t y_sum = C_0;
  
   if constexpr(false == aRsp)
   {
      for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); ++m2)
      {
         if (m2 == m0) // If m2 == m1, we get the Y[m0][m1] contribution which is ok.
            continue;
         y_sum -= aIntermeds.GetY(m0,m2);
         y_sum -= aIntermeds.GetY(m1,m2);
      }
      aRes += y_sum * amps;
   }
   else
   {
      value_t y_c2_sum = C_0;
      for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); ++m2)
         for (In m3=m2+I_1; m3<this->pOpDef()->NmodesInOp(); ++m3)
            if (m2!=m0 && m2!=m1 && m3!=m0 && m3!=m1)
            {
               y_sum += aIntermeds.GetY(m2,m3);
               y_c2_sum += aR2Intermeds->GetY(m2,m3);
            }
      
      midasvector_t c_coefs((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);
      this->RCoefs().DataIo(IO_GET, c_coefs, c_coefs.Size(), aMc.Address());
      aRes += y_sum * c_coefs;
      aRes += y_c2_sum * amps;
   }
            
   if (aTmr0 != nullptr)
      *aTmr0 += clock() - t0;

   // Doing <m0,m1| H T(m0,m2) T(m1,m3) |REF>
   clock_t t1 = clock();
   
   // Take care to use the right intermeds in case of response.
   const twomodeintermeds_t* intermeds = &aIntermeds;
   if constexpr(aRsp)
      intermeds = aR2Intermeds;

   const absval_t screen_norm2 = this->ScreenNorm2();
   midasvector_t dir_prod(exci_0*exci_1);
   midasvector_t xc;
   for (LocalModeNr m2=I_0; m2<this->pOpDef()->NmodesInOp(); ++m2)
   {
      if (m2==m0 || m2==m1)
         continue;

      xc.SetNewSize(exci_0, false); 
      const auto& xc_list = aIntermeds.GetXCintermeds(m0, m2);
      for (auto it=xc_list.begin(); it!=xc_list.end(); ++it)
      {
         In op2 = it->first;
         absval_t norm2 = C_0;
         if (intermeds->GetXnorm2(m1, m2, op2, norm2))
         {
            (*aTotTerms)++;
            norm2 *= std::real((*(it->second))[exci_0]);
            //^ Super scary stuff here; the XC intermediate vectors contain blocks of
            //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
            //as the Nmodals-1'th element. The intermeds. are of value_t while the
            //norm^2 is principally of absval_t but must be stored as value_t (with
            //zero imag. part if complex), since they're put in the same container as
            //the intermeds. themselves. Also we really have to rely on the ordering
            //of the XC vectors not changing _ever_... -MBH, Jan 2019.
            if (norm2 > screen_norm2)
            {
               for (In i=I_0; i<exci_0; ++i)
                  xc[i] = (*(it->second))[i];

               TMI_DirProd(*intermeds, m1, m2, op2, xc, m0, dir_prod);
               aRes += dir_prod;
            }
            else
               (*aScreened)++;

            // Check screening
            //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
            //   Mout << "Peter is stupid (a).  act. norm^2: " << tmp_res.Norm2()
            //        << "   pred. norm^2: " << norm2  << endl;
            
         }
      }

      // For response do the same stuff again but swap m0 and m1.
      if constexpr(aRsp)
      {
         xc.SetNewSize(exci_1, false); 
         const auto& xc_list = aIntermeds.GetXCintermeds(m1, m2);
         for (auto it=xc_list.begin(); it!=xc_list.end(); ++it)
         {
            In op2 = it->first;
            absval_t norm2 = C_0;
            if (aR2Intermeds->GetXnorm2(m0, m2, op2, norm2))
            {
               (*aTotTerms)++;
               norm2 *= std::real((*(it->second))[exci_1]);
               //^ Super scary stuff here; the XC intermediate vectors contain blocks of
               //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
               //as the Nmodals-1'th element. The intermeds. are of value_t while the
               //norm^2 is principally of absval_t but must be stored as value_t (with
               //zero imag. part if complex), since they're put in the same container as
               //the intermeds. themselves. Also we really have to rely on the ordering
               //of the XC vectors not changing _ever_... -MBH, Jan 2019.
               if (norm2 > screen_norm2)
               {
                  for (In i=I_0; i<exci_1; ++i)
                     xc[i] = (*(it->second))[i];

                  TMI_DirProd(*aR2Intermeds, m0, m2, op2, xc, m1, dir_prod);
                  aRes += dir_prod;
               }
               else
                  (*aScreened)++;
               
               // Check screening
               //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
               //   Mout << "Peter is stupid (b).  act. norm^2: " << tmp_res.Norm2()
               //      << "   pred. norm^2: " << norm2  << endl;
            }
         }
      }
   }

   xc.SetNewSize(exci_1, false);
   const auto& xc_list = intermeds->GetXCintermeds(m1,m1);
   for (auto it=xc_list.begin(); it!=xc_list.end(); ++it)
   {
      In op1 = it->first;
      absval_t norm2 = C_0;
      if (aIntermeds.GetXnorm2(m0, m1, op1, norm2))
      {
         (*aTotTerms)++;
         norm2 *= std::real((*(it->second))[exci_1]);
         //^ Super scary stuff here; the XC intermediate vectors contain blocks of
         //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
         //as the Nmodals-1'th element. The intermeds. are of value_t while the
         //norm^2 is principally of absval_t but must be stored as value_t (with
         //zero imag. part if complex), since they're put in the same container as
         //the intermeds. themselves. Also we really have to rely on the ordering
         //of the XC vectors not changing _ever_... -MBH, Jan 2019.
         if (norm2 > screen_norm2)
         {
            for (In i=I_0; i<exci_1; ++i)
               xc[i] = (*(it->second))[i];

            TMI_DirProd(aIntermeds, m0, m1, op1, xc, m1, dir_prod);
            aRes-= dir_prod;
         }
         else
            (*aScreened)++;

         // Check screening
         //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
         //   Mout << "Peter is stupid (c).  act. norm^2: " << tmp_res.Norm2()
         //      << "   pred. norm^2: " << norm2  << endl;
      }
   }

   // For response do the same again with m0 and m1 swapped.
   if constexpr(aRsp)
   {
      xc.SetNewSize(exci_0, false);
      const auto& xc_list = aR2Intermeds->GetXCintermeds(m0,m0);
      for (auto it=xc_list.begin(); it!=xc_list.end(); ++it)
      {
         In op0 = it->first;
         absval_t norm2 = C_0;
         if (aIntermeds.GetXnorm2(m1, m0, op0, norm2))
         {
            (*aTotTerms)++;
            norm2 *= std::real((*(it->second))[exci_0]);
            //^ Super scary stuff here; the XC intermediate vectors contain blocks of
            //XC intermediates (length Nmodals(m1)-1), followed by the block's norm^2
            //as the Nmodals-1'th element. The intermeds. are of value_t while the
            //norm^2 is principally of absval_t but must be stored as value_t (with
            //zero imag. part if complex), since they're put in the same container as
            //the intermeds. themselves. Also we really have to rely on the ordering
            //of the XC vectors not changing _ever_... -MBH, Jan 2019.
            if (norm2 > screen_norm2)
            {
               for (In i=I_0; i<exci_0; ++i)
                  xc[i] = (*(it->second))[i];

               TMI_DirProd(aIntermeds, m1, m0, op0, xc, m0, dir_prod);
               aRes-= dir_prod;
            }
            else
               (*aScreened)++;
         
            // Check screening
            //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
            //   Mout << "Peter is stupid (d).  act. norm^2: " << tmp_res.Norm2()
            //      << "   pred. norm^2: " << norm2  << endl;
         }
      }
   }
   
   for (In i_term=I_0; i_term<this->pOpDef()->NactiveTerms(m0); ++i_term)
   {
      In term = this->pOpDef()->TermActive(m0, i_term);
      if ((this->pOpDef()->IsModeActiveInTerm(m1, term) == false) ||
          (this->pOpDef()->NfactorsInTerm(term) != I_2))
         continue;
      
      // We have a (m0,m1) term. Do the direct product.
      (*aTotTerms)++;
      LocalOperNr op0 = this->pOpDef()->OperForFactor(term, I_0);
      LocalOperNr op1 = this->pOpDef()->OperForFactor(term, I_1);

      // Screening.
      value_t coef = this->pOpDef()->Coef(term);
      absval_t norm2 = midas::util::AbsVal2(coef);
      absval_t val = C_0;
      intermeds->GetXnorm2(m0, m1, op1, val);
      norm2 *= val;
      aIntermeds.GetXnorm2(m1, m0, op0, val);
      norm2 *= val;
      if (norm2 > screen_norm2)
      {
         TMI_DirProd(*intermeds, m0, m1, op1, aIntermeds, m1, m0, op0, dir_prod);
         aRes += coef * dir_prod;
      }
      else
         (*aScreened)++;
            
      // Check screening
      //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
      //   Mout << "Peter is stupid (e).  act. norm^2: " << tmp_res.Norm2()
      //      << "   pred. norm^2: " << norm2  << endl;

      if constexpr(aRsp)
      {
         (*aTotTerms)++;
         norm2 = midas::util::AbsVal2(coef);
         aIntermeds.GetXnorm2(m0, m1, op1, val);
         norm2 *= val;
         intermeds->GetXnorm2(m1, m0, op0, val);
         norm2 *= val;
         if (norm2 > screen_norm2)
         {
            TMI_DirProd(aIntermeds, m0, m1, op1, *intermeds, m1, m0, op0, dir_prod);
            aRes += coef * dir_prod;
         }
         else
            (*aScreened)++;
         
         // Check screening
         //if (tmp_res.Norm2()-norm2 > tmp_res.Norm2()/C_10_9/C_10_5)
         //   Mout << "Peter is stupid (f).  act. norm^2: " << tmp_res.Norm2()
         //      << "   pred. norm^2: " << norm2  << endl;
      }
   } 
   if (aTmr1 != nullptr)
      *aTmr1 += clock() - t1;
}


/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  typename OPDEF_T
   >
void Vcc2ContribsReg<T, OPDEF_T>::SanityCheck
   (
   )  const
{
   // Check modal integrals type.
   if (this->DoingVccNotVci())
   {
      this->CheckTransType(this->ModInts(), modalintegrals_t::TransT::T1, "T1 (for VCC)");
   }
   else
   {
      this->CheckTransType(this->ModInts(), modalintegrals_t::TransT::RAW, "RAW (for VCI)");
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSREG_IMPL_H_INCLUDED*/
