/**
 *******************************************************************************
 * 
 * @file    Vcc2TransLJac_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSLJAC_IMPL_H_INCLUDED
#define VCC2TRANSLJAC_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/TypeName.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/vcc2/TwoModeIntermeds.h"
#include "vcc/vcc2/Vcc2ContribsReg.h"
#include "vcc/vcc2/Vcc2ContribsLJac.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::Vcc2TransLJac
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const bool aOnlyOneMode
   ,  const Uin aIoLevel
   ,  const bool aTimeIt
   )
   :  Vcc2TransFixedAmps<T,T2ONLY,OPDEF_T>
         (  apOpDef
         ,  arRawModInts
         ,  arMcr
         ,  arTAmps
         ,  aOnlyOneMode
         ,  aIoLevel
         ,  aTimeIt
         )
   ,  mLocalT2Intermeds(ConstructT2Intermeds(arTAmps))
{
}

/***************************************************************************//**
 * @brief
 *    C-tor that takes pre-calculated intermeds.
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::Vcc2TransLJac
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  intermeds_t* apGlobalIntermeds
   ,  const bool aOnlyOneMode
   ,  const Uin aIoLevel
   ,  const bool aTimeIt
   )
   :  Vcc2TransFixedAmps<T,T2ONLY,OPDEF_T>
         (  apOpDef
         ,  arRawModInts
         ,  arMcr
         ,  arTAmps
         ,  aOnlyOneMode
         ,  aIoLevel
         ,  aTimeIt
         )
   ,  mpGlobalT2Intermeds(apGlobalIntermeds)
{
   if (  !apGlobalIntermeds
      )
   {
      MIDASERROR("nullptr passed as GlobalIntermeds to Vcc2TransLJac c-tor!");
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
std::string Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::PrettyClassName
   (
   )  const
{
   return std::string("Vcc2TransLJac<")+midas::type_traits::TypeName<T>()+">";
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
std::string Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::PrettyTransformerType
   (
   )  const
{
   return "<ref|Lexp(-T)[X,tau_nu]exp(T)|ref> (left-hand VCC Jacobian if X = H)";
}

/***************************************************************************//**
 * @param[in] arParams
 *    Intended to be the (fixed) T-amps. that define the L-Jacobian
 *    transformer.
 * @return
 *    Newly constructed T2 intermeds_t object.
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >  
typename Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::intermeds_t 
Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::ConstructT2Intermeds
   (  const datacont_t& arParams
   )  const
{
   if (gDebug || this->IoLevel() > I_10 || this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; computing T-based intermediates" << std::endl;
   }
   clock_t t0 = clock();

   intermeds_t intermeds(this->NModalsIn(), this->pOpDef(), this->Mcr(), true);
   this->InitializeXIntermeds(intermeds, this->T1ModInts(), this->TAmps());
   intermeds.InitializeXCintermeds(this->pOpDef());
   if (!this->OnlyOneMode())
   {
      // Supposedly this TwoModeRefT2 call is just to calculate the intermediates.
      midas::vcc::vcc2::Vcc2ContribsReg<value_t,opdef_t> vcc2contribsreg
         (  this->pOpDef()
         ,  this->T1ModInts()
         ,  this->Mcr()
         ,  this->TAmps()
         );
      value_t dummy;
      vcc2contribsreg.TwoModeRefT2(dummy,intermeds);
   }

   if (this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; T-based intermediates: " << fclock_t(clock() - t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
   return intermeds;
}  
   
/***************************************************************************//**
 * @note
 *    Declared `const` even though we modify Vcc2TransLJac<T,T2ONLY,OPDEF_T>::mT2Intermeds,
 *    which is however mutable. See note at member variable.
 *
 * @param[in] arLCoefs
 *    L-coefficients of current iteration. Needed for calculating Z
 *    intermediates.
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >  
void Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::SetupZIntermeds
   (  const datacont_t& arLCoefs
   )  const
{
   // Initialize correct size of Z intermediates if not done already (e.g. if X, Y, and XC are reused from Vcc2TransReg)
   if (  this->mpGlobalT2Intermeds
      && !this->mpGlobalT2Intermeds->ZInitialized()      // NKM: It shouldn't be as far as I can tell
      )
   {
      this->mpGlobalT2Intermeds->InitializeZ(this->pOpDef());
   }

   if (gDebug || this->IoLevel() > I_10 || this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; computing L-based intermediates" << std::endl;
   }
   clock_t t0 = clock();

   auto& intermeds = mpGlobalT2Intermeds ? *mpGlobalT2Intermeds : mLocalT2Intermeds;
   this->InitializeZIntermeds(intermeds, this->T1ModInts(), arLCoefs, true);

   if (this->TimeIt())
   {
      Mout  << this->PrettyClassName() << "; L-based intermediates: " << fclock_t(clock() - t0)/CLOCKS_PER_SEC << " s" << std::endl;
   }
}  
   
/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
void Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::TransformImpl
   (  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   clock_t t0 = clock();

   this->SetupZIntermeds(arIn);

   vcc2contribsljac_t vcc2contribsljac
      (  this->pOpDef()
      ,  this->T1ModInts()
      ,  this->Mcr()
      ,  this->TAmps()
      ,  arIn
      );

   if (this->TimeIt())
      Mout << "   Initialization:     " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;

   auto& intermeds = mpGlobalT2Intermeds ? *mpGlobalT2Intermeds : mLocalT2Intermeds;
   TwoModeTransformerImpl(vcc2contribsljac, intermeds, arIn, arOut);

   if constexpr   (  TDMVCC2
                  )
   {
      this->mSavedIntermediates = std::make_unique<intermeds_t>(std::move(intermeds));
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool T2ONLY
   ,  typename OPDEF_T
   ,  bool TDMVCC2
   >
void Vcc2TransLJac<T,T2ONLY,OPDEF_T,TDMVCC2>::TwoModeTransformerImpl
   (  const vcc2contribsljac_t& arVcc2Contribs
   ,  intermeds_t& arIntermeds
   ,  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   const auto& vcc2contribsljac = arVcc2Contribs;
   const auto& l_intermeds = arIntermeds;
   const auto& mcr = this->Mcr();
   clock_t t_tot = clock();

   // Do gamma_1 part
   clock_t t_tmp;
   clock_t t0 = clock(); 

   clock_t t1 = 0;
   clock_t t2 = 0;
   clock_t t3 = 0;
   clock_t t4 = 0;
   clock_t t5 = 0;
   clock_t t6 = 0;
   clock_t t7 = 0;
   clock_t t8 = 0;
   clock_t t9 = 0;

   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      midasvector_t res(this->NModals(m0)-I_1, C_0);

      clock_t t_tmp = clock();
      vcc2contribsljac.TwoModeLM0HfTau(mc,res);
      t1 += clock() - t_tmp;
      t_tmp = clock(); 
      vcc2contribsljac.TwoModeLM0TauHp(mc,res);
      t2 += clock() - t_tmp;
      t_tmp = clock(); 
      vcc2contribsljac.TwoModeLHTau(mc,res, l_intermeds);
      t3 +=clock() -t_tmp;
      t_tmp = clock(); 

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL2Vcc1pt2(mc, res);
      t4 +=clock() -t_tmp;
      t_tmp = clock(); 

      if (!this->OnlyOneMode())
      {
         t_tmp = clock(); 
         vcc2contribsljac.TwoModeLHddTTau(mc,res, l_intermeds);
         t5 +=clock() -t_tmp;

         t_tmp = clock(); 
         vcc2contribsljac.TwoModeLTauHddT(mc,res, l_intermeds);
         t6 +=clock() -t_tmp;

         t_tmp = clock(); 
         vcc2contribsljac.TwoModeL2H1XTau1(mc, res, l_intermeds);
         t7 +=clock() -t_tmp;

         t_tmp = clock(); 
         vcc2contribsljac.TwoModeLm0m1THTau1(mc,res, arIntermeds); 
         t8 +=clock() -t_tmp;

         t_tmp = clock(); 
         vcc2contribsljac.TwoModeLm1m2THTau1(mc,res);
         t9 +=clock() -t_tmp;
      }

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   }
   if (this->TimeIt())
   {
      Mout << "    gamma_1          " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl
         << "       LM0HfTau     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl
         << "       LM0TauHp     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl
         << "       LHTau     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl
         << "       L2Vcc1pt2     " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl
         << "       LHddTTau     " << fclock_t(t5) / CLOCKS_PER_SEC << " s" << endl
         << "       LTauHddT     " << fclock_t(t6) / CLOCKS_PER_SEC << " s" << endl
         << "       L2H1XTau1     " << fclock_t(t7) / CLOCKS_PER_SEC << " s" << endl
         << "       Lm0m1THTau1     " << fclock_t(t8) / CLOCKS_PER_SEC << " s" << endl
         << "       Lm1m2THTau1     " << fclock_t(t9) / CLOCKS_PER_SEC << " s" << endl;
   }

   // Do gamma_2 part
   t0 = clock(); 
   t_tmp = clock();

   t1 = 0;
   t2 = 0;
   t3 = 0;
   t4 = 0;
   t5 = 0;
   t6 = 0;

   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      In m0 = mc.Mode(I_0);
      In m1 = mc.Mode(I_1);

      midasvector_t res((this->NModals(m1)-I_1)*(this->NModals(m0)-I_1), C_0);


      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL1Tau2(mc,res);
      t1 +=clock() -t_tmp;

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL2HTau2(mc,res);
      t2 +=clock() -t_tmp;

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeLm1m2HTau2(mc,res);
      t3 +=clock() -t_tmp;

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL2THTau2(mc,res);
      t4 +=clock() -t_tmp;

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL2Tau2HT(mc,res,l_intermeds);
      t5 +=clock() -t_tmp;

      t_tmp = clock(); 
      vcc2contribsljac.TwoModeL2HTTau2(mc,res,l_intermeds);
      t6 +=clock() -t_tmp;

      arOut.DataIo(IO_PUT, res, res.Size(), mc.Address());
   } 
   if (this->TimeIt())
   {
      Mout << "    gamma_2          " << fclock_t(clock() - t0) / CLOCKS_PER_SEC << " s" << endl
         << "       L1Tau2     " << fclock_t(t1) / CLOCKS_PER_SEC << " s" << endl
         << "       L2HTau2     " << fclock_t(t2) / CLOCKS_PER_SEC << " s" << endl
         << "       Lm1m2HTau2     " << fclock_t(t3) / CLOCKS_PER_SEC << " s" << endl
         << "       L2THTau2     " << fclock_t(t4) / CLOCKS_PER_SEC << " s" << endl
         << "       L2Tau2HT     " << fclock_t(t5) / CLOCKS_PER_SEC << " s" << endl
         << "       L2HTTau2     " << fclock_t(t6) / CLOCKS_PER_SEC << " s" << endl;
      Mout << "   Total:               " << fclock_t(clock() - t_tot) / CLOCKS_PER_SEC << " s" << endl;
   }
}


} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSLJAC_IMPL_H_INCLUDED*/
