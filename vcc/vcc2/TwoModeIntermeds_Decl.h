/**
************************************************************************
* 
* @file                TwoModeIntermeds_Decl.h 
*
* Created:             51-5-2007
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
* Author:              Andreas Buchgraitz Jensen (buchgraitz@chem.au.dk) 
*
* Short Description:   Organizing intermediates for specific VCC[2]
*                      calculation.
* 
* Last modified: 16 Nov 2020
* 
* Detailed  Description: 
*     Houses intermediates and their methods for all VCC[2] related 
*     computations i.e. VCC[2], TDVCC[2], TDVCI[2], TDMVCC[2]
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TWOMODEINTERMEDS_DECL_H_INCLUDED
#define TWOMODEINTERMEDS_DECL_H_INCLUDED

// std headers
#include <list>
#include <vector>
#include <utility>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"

class ModeCombiOpRange;
class OpDef;

// Forward declaration of class and friends.
template<typename T, typename OPDEF_T=OpDef> class TwoModeIntermeds;

template<typename T, typename OPDEF_T>
void TMI_DirProd
   (  const TwoModeIntermeds<T, OPDEF_T>& aIntermed0
   ,  const LocalModeNr aM0
   ,  const LocalModeNr aM1
   ,  const LocalOperNr aOperM1
   ,  const TwoModeIntermeds<T, OPDEF_T>& aIntermed2
   ,  const LocalModeNr aM2
   ,  const LocalModeNr aM3
   ,  const LocalOperNr aOperM3
   ,  typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aRes
   );

template<typename T, typename OPDEF_T>
void TMI_DirProd
   (  const TwoModeIntermeds<T, OPDEF_T>& aX
   ,  const LocalModeNr aM0
   ,  const LocalModeNr aM1
   ,  const LocalOperNr aOp1
   ,  const typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aC
   ,  const LocalModeNr aM2
   ,  typename TwoModeIntermeds<T, OPDEF_T>::midasvector_t& aRes
   );

template<typename T, typename OPDEF_T>
std::ostream& operator<<(std::ostream& aOut, const TwoModeIntermeds<T, OPDEF_T>& aArg);


/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T, typename OPDEF_T>
class TwoModeIntermeds
{
public:
   using value_t = T;
   using absval_t = midas::type_traits::RealTypeT<value_t>;
   using midasvector_t = GeneralMidasVector<value_t>;
   using mat_t = GeneralMidasMatrix<value_t>;
   using opdef_t = OPDEF_T;

private:
   using invec1d = std::vector<In>;
   using invec2d = std::vector<std::vector<In>>;
   using vec1d = std::vector<value_t>;
   using vec2d = std::vector<std::vector<value_t>>;

   std::vector<In> mNmodals;

   // Start of intermediates. Value is -1 if no intermediate exists.
   invec2d mXaddr;

   //! Vec of X intermeds. and norms; for each (m0,m1,oper) a block of X intermeds. and their norm^2.
   midasvector_t mX;

   // For addressing XC intermediates. These are not available for all operators, so addressing
   // is [m0][m1] -> std::list (oper, midasvector_t ptr.) std::pairs.
   std::vector<std::vector<std::list<std::pair<In,std::unique_ptr<midasvector_t>> > > > mXC;

   In Nmodals(LocalModeNr) const;

   //! h(m0) h(m1) * T(m0,m1) contractions (all terms in H).
   std::vector<std::vector<value_t> > mY;

   //! h(m0, oper) * T(m0) contractions, (m0,oper)Z. Index [m0][oper]
   vec2d mZ;

   //! Vector of inner products of amplitudes. (Only values in one triangle of matrix because of m1 < m2 restricted summation)
   mat_t mAmplProd;
   mat_t mAmplProdSum;

   //! Matrix (row: m0, col: m) of vectors (index: oper_m0) with trace of V matrices.
   GeneralMidasMatrix<GeneralMidasVector<value_t>> mTraceV;

   //! Matrix (row: m0, col: m) of vectors (index: oper_m0) of W intermeds
   GeneralMidasMatrix<GeneralMidasVector<GeneralMidasVector<value_t>>> mIntermedW;
   //! Matrix (row: m0, col: m) of vectors (index: oper_m0) of X*L intermeds
   GeneralMidasMatrix<GeneralMidasVector<GeneralMidasVector<value_t>>> mIntermedXL;
   //! Matrix (row: m0, col: m) of vectors (index: oper_m0) of W*S intermeds
   GeneralMidasMatrix<GeneralMidasVector<GeneralMidasVector<value_t>>> mIntermedWS;
   //! Matrix (row: m0, col: m) of vectors (index: oper_m) of W*S intermeds specifically used in FullActiveBasis Tdmvcc
   GeneralMidasMatrix<GeneralMidasVector<GeneralMidasVector<value_t>>> mIntermedWS_FAB;

   //! Matrix of matrices intermediates
   GeneralMidasMatrix<mat_t> mIntermedVirFMatSum;

   //! Matrix of matrices for sums of l*s products
   GeneralMidasMatrix<mat_t> mIntermedSumLS;

   //! 3rd order BCH x intermed summations.
   GeneralMidasMatrix<GeneralMidasVector<GeneralMidasVector<value_t>>> mIntermed3bch;
   
public:
   TwoModeIntermeds();
   TwoModeIntermeds(std::vector<In> aNmodals, const opdef_t* aOpDef,
                    const ModeCombiOpRange&, bool aZ=false);
   ~TwoModeIntermeds() = default;

   //@{
   TwoModeIntermeds(const TwoModeIntermeds&) = delete;
   TwoModeIntermeds(TwoModeIntermeds&&) = default;
   TwoModeIntermeds& operator=(const TwoModeIntermeds&) = delete;
   TwoModeIntermeds& operator=(TwoModeIntermeds&&) = default;
   //@}

   void Initialize
     (   std::vector<In> aNmodals
     ,   const opdef_t* aOpDef
     ,   const ModeCombiOpRange&
     ,   bool aZ=false
     );

   //! Initialize intermeds specific for the tdmvcc2 implementation.
   void InitializeTdmvcc2Intermeds(const opdef_t& aOpDef, const ModeCombiOpRange& aMcr);
   void InitializeTdmvcc2IntermedsBeforeLJac(const opdef_t* aOpDef);

   void InitializeZ(const opdef_t* aOpDef);
   bool ZInitialized() const { return !mZ.empty(); }

   void InitializeXCintermeds(const opdef_t* aOpDef);
   
   // For X intermediate the indexing is (aM1, aOper)X(aM0,a)
   void PutXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOper, const midasvector_t& aVec);
   bool GetXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOper, midasvector_t& aVec) const;

   bool GetXnorm2(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, absval_t& aNorm2) const;
 
   // Get a std::list of pointers to the XC intermediates. 
   const auto& GetXCintermeds(const LocalModeNr aM0, const LocalModeNr aM1) const { return mXC[aM0][aM1]; }
   bool GetXCintermedsVector(const LocalModeNr aM0, const LocalModeNr aM1, std::vector<midasvector_t>& aXC) const;

   //! Get the (m0,m1) Y intermediate.
   value_t GetY(const LocalModeNr m0, const LocalModeNr m1) const {return mY[m0][m1];}

   //! Add a contribution to the (m0,m1) Y intermediate.
   void AddToY(const LocalModeNr m0, const LocalModeNr m1, const value_t aVal) {mY[m0][m1] += aVal;}

   //! Get the (m0,oper) Z intermediate.
   value_t GetZ(const LocalModeNr m0, const LocalOperNr oper) const {return mZ[m0][oper];}

   //! Assign value to the (m0,oper) Z intermediate.
   void AssignToZ(const LocalModeNr m0, const LocalOperNr oper, const value_t aVal) {mZ[m0][oper] = aVal;}

   //! For AmplProd intermeds. (sum_{a^m, b^m0}l_{a^mb^m0}s_{a^mb^m0}).
   void PutAmplProdintermed(const LocalModeNr aM, const LocalModeNr aM0, const value_t aVal);
   value_t GetAmplProdintermed(const LocalModeNr aM, const LocalModeNr aM0) const {return mAmplProd[aM][aM0]; }
   //! For summations of AmplProd intermeds. (sum_{a^m, b^m0}l_{a^mb^m0}s_{a^mb^m0}).
   void PutAmplProdIntermedSum(const LocalModeNr aM, const LocalModeNr aM0, const value_t aVal);
   value_t GetAmplProdIntermedSum(const LocalModeNr aM, const LocalModeNr aM0) const {return mAmplProdSum[aM][aM0]; }
   
   //! Put/Get/initialize trace of V matrices.
   void PutTraceVIntermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOperMode_M0, const value_t aVal);
   value_t GetTraceVIntermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOperMode_M0) const;

   //! Put/Get W intermeds. Which is vectors of type sum_{d^m0} tilde{h}_{d^m0 i^m0}*l^{m0 m}
   void PutIntermedW(const LocalModeNr aM, const LocalModeNr aM0, const LocalOperNr aOperMode_M0, const midasvector_t& aVec);
   const midasvector_t& GetIntermedW(const LocalModeNr aM, const LocalModeNr aM0, const LocalOperNr aOperMode_M0) const;

   //! Put/Get XL intermeds. Which is vectors of type sum_{m1} (^m0O^m0 X^m1 * l^{m1 m0})
   void PutIntermedXL(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M0, const midasvector_t& aVec);
   const midasvector_t& GetIntermedXL(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M0) const;

   //! Put/Get WS intermeds. Which is vectors of type sum_{m1} (^m0O^m0 W^m1 * s^{m1 m0})
   void PutIntermedWS(const LocalModeNr aM, const LocalModeNr aM0, const LocalOperNr aOperMode_M0, const midasvector_t& aVec);
   const midasvector_t& GetIntermedWS(const LocalModeNr aM, const LocalModeNr aM0, const LocalOperNr aOperMode_M0) const;
   //! Put/Get WS intermeds for FullActiveBasis. Which is vectors of type sum_{m1 != m, m0} (^mO^m W^m1 * s^{m1 m})
   void PutIntermedWS_FAB(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M, const midasvector_t& aVec);
   const midasvector_t& GetIntermedWS_FAB(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M) const;

   //! Intermed for Virtual F (sum of matrix products)
   void CalcIntermedVirFMatSum(const In& aNModes, const GeneralDataCont<value_t>& aLambdaCoefs, const GeneralDataCont<value_t>& aClusterAmps, const ModeCombiOpRange& aMcr);
   const mat_t& GetIntermedVirFMatSum(const LocalModeNr aM, const LocalModeNr aM0) const;

   //! Put/Get for ls matrices for M^4 -> M^3
   void CalcIntermedSumLS(const GeneralDataCont<value_t>& aClusterAmps, const GeneralDataCont<value_t>& aLambdaCoefs, const In aNModes, const ModeCombiOpRange& aMcr);
   const mat_t& GetIntermedSumLS(const LocalModeNr aM, const LocalModeNr aM0) const;

   //! Put/Get 3rd order bch terms. 
   void PutIntermed3bch(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M0, const midasvector_t& aVec);
   const midasvector_t& GetIntermed3bch(const LocalModeNr aM0, const LocalModeNr aM, const LocalOperNr aOperMode_M0) const;

   //! Helper function
   //mat_t GetAmplMat(const Uin Nrows, const Uin Ncols, const GeneralDataCont<value_t>& Ampl, const In m1, const In m2, const ModeCombiOpRange& aMcr) const; 
   void GetAmplMat(mat_t& arMat, const Uin Nrows, const Uin Ncols, const GeneralDataCont<value_t>& Ampl, const In m1, const In m2, const ModeCombiOpRange& aMcr) const; 


   friend void TMI_DirProd<>
      (  const TwoModeIntermeds<T,OPDEF_T>& aIntermed0
      ,  const LocalModeNr aM0
      ,  const LocalModeNr aM1
      ,  const LocalOperNr aOperM1
      ,  const TwoModeIntermeds<T,OPDEF_T>& aIntermed2
      ,  const LocalModeNr aM2
      ,  const LocalModeNr aM3
      ,  const LocalOperNr aOperM3
      ,  typename TwoModeIntermeds<T,OPDEF_T>::midasvector_t& aRes
      );

   //! Dir.prod. of (aM1, aOp1)X(aM0) for aX and the aC corresponding to excitations in mode aM2.
   friend void TMI_DirProd<>
      (  const TwoModeIntermeds<T,OPDEF_T>& aX
      ,  const LocalModeNr aM0
      ,  const LocalModeNr aM1
      ,  const LocalOperNr aOp1
      ,  const typename TwoModeIntermeds<T,OPDEF_T>::midasvector_t& aC
      ,  const LocalModeNr aM2
      ,  typename TwoModeIntermeds<T,OPDEF_T>::midasvector_t& aRes
      );
   
   friend std::ostream& operator<< <>(std::ostream& aOut, const TwoModeIntermeds<T,OPDEF_T>& aArg);

//   //! Clone the whole intermed container (NKM: basically a copy c-tor, but you need to call it explicitly)
//   TwoModeIntermeds<T, OPDEF_T> Clone() const;

};
   
#endif // TWOMODEINTERMEDS_DECL_H_INCLUDED
