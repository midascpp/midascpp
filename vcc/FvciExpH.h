/**
************************************************************************
* 
* @file                FvciExpH.h 
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   FvciExpH class and function declarations.
* 
* Last modified: Mon Oct 30, 2006  09:25AM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FVCIEXPH_H
#define FVCIEXPH_H

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasMatrix.h"
#include "ni/OneModeInt.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/MultiIndex.h"
#include "vcc/Vcc.h"
#include "ni/OneModeInt.h"

class FvciExpH
{
   private:
      //Logical iteration info
      In mHamDim;                                       ///< Dimension of Hamiltonian matrix. 
      string mDiagMeth;
      MidasMatrix mH;                                   ///< The Hamiltonian matrix.
      MultiIndex* mpMi;                                 ///< Pointer to the multi index.
      VccCalcDef* mpVccCalcDef;                         ///< Pointer 
      //Vcc*   mpVcc;                                   ///< Pointer 
      OpDef* mpOpDef;                                   ///< Pointer 
      BasDef* mpBasDef;                                 ///< Pointer 
      OneModeInt* mpOneModeInt;                         ///< Pointer 
   public:
      FvciExpH(In aDim,MultiIndex* apMultiIndex, OpDef* apOpDef, BasDef* apBasDef, 
            OneModeInt* apOneModeInt,VccCalcDef* apVccCalcDef);       ///< constructor 
      void Diagonalize();                               ///< Diagonalize H.
      void Construct();                                 ///< Solve scf equations for one mode 
};

#endif

