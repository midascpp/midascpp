/**
************************************************************************
* 
* @file                LanczosChain.h
*
* Created:             11-08-2009
*
* Author:              Peter Seidler    (seidler@chem.au.dk)
*
* Short Description:   Lanczos chain (tridiagonal matrix) definition.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSCHAIN_H
#define LANCZOSCHAIN_H

// Standard Headers
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "mmv/DataCont.h"
#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"
#include "vcc/LanczosRspFunc.h"


class LanczosChainDef : public LanczosChainDefBase
{
   protected:
      string mOper;                 ///< Initial q(0) vector is set to normalized eta^(mOper).
      //In mLength;                   ///< Length of chain (dimension of matrix).
      //In mOrtho;                    ///< Orthogonalization scheme.

      vector<string> mXopers;             
      ///< List of operators for which to calcluate x(j) = eta^oper * q(j).
      ///< Note that eta is normalized in this case.
   
   public:
      LanczosChainDef(const string& aOper="", In aLength=I_0,
                      In aOrtho=LANCZOS_ORTHO_NONE, vector<string>* const aXopers=NULL);
    
      void  SetOper(const string& aOper)      {mOper = aOper;}
      const string& GetOper() const           {return mOper;}
      void  SetLength(In aLength)             {mLength = aLength;}
      In    GetLength() const                 {return mLength;}
      void  SetOrtho(In aOrtho)               {mOrtho = aOrtho;}
      In    GetOrtho() const                  {return mOrtho;}
      void  AddXoper(const string& aXoper)    {mXopers.push_back(aXoper);}
      const string& GetXoper(In aIdx)         {return mXopers[aIdx];}
      const vector<string>& GetXopers() const {return mXopers;}
      In    NXopers() const                   {return mXopers.size();} 
      
      bool Combine(const LanczosChainDef& aOther);
      ///< Combine this and aOther into a single definition. Returns true if possible and stores
      ///< new definition in this. Otherwise return false.

      bool Compatible(const LanczosChainDef& aOther) const;
      ///< Returns true if aOther is a subset of or identical to this definition.
      
      friend std::ostream& operator<<(std::ostream& aOut, const LanczosChainDef& aDef);
};

class LanczosChain
{
   protected:
      LanczosChainDef     mDef;            ///< Definitiion of the chain.
    
      Nb                  mEtaNorm;        ///< Norm of eta vector used for q(0). 
      //MidasVector         mTalpha;         ///< Diagonal of T.
      //MidasVector         mTbeta;          ///< "Below" diagonal of T.
      vector<MidasVector> mX;              ///< Stores (u^T,q_j) dot products.      
      vector<DataCont>    mXetas;          ///< Normalized eta vectors to be dotted with q(j).
      vector<Nb>          mXetaNorms;      ///< Norms of eta vectors to be dotted with q(j).
      bool                mCalcOrtho;      ///< Calculate orthogonality of q vectors.
      string              mFilePrefix;     ///< Base name for generated files.

      Vcc*                mpVcc;
      ///< Pointer to VCC class responsible for this calculation.
      
      VccTransformer*     mpTrf;
      ///< Pointer to VCC transformer class to be used.

      Nb CalcEta(const string& aOper, DataCont& aDc);
      ///< Calculates eta vector. Stores normalized vector in aDc and returns norm.
      
      void CalcXetas();
      
      bool Restart(In& aLength, DataCont& aR, DataCont& aQjm1);
      ///< Attempts restart. Returns true if succesful.

      void WriteToFile(In aIdx, std::ios_base::openmode mode=ios_base::app);
      ///< Output utility used for writing the Lanczos chain to disc.
      
      void SaveQvec(DataCont& aQj, In aJ);
      ///< Save aQj on disc as Lanczos vector q(aJ).

      void Orthogonalize(DataCont& aVec, In aJ);
      ///< Orthogobalize aVec to q(0) ... q(aJ).

      void CalcOrtho();
      ///< Calculate dot prodcuts of q(j) vectors.
     
      In GetXoperIdx(const string& aOper) const;
      
      bool RestoreXfromQ(In aIdx, In aLength);
      ///< Attempt to restore mX[aIdx] from q vectors on disc.
      
   public:
      MidasVector         mTalpha; // Move this to protected later
      MidasVector         mTbeta;//Move this to protected later
      LanczosChain(const LanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf);
      void Evaluate();

      bool Compatible(const LanczosChainDef& aDef) const {return mDef.Compatible(aDef);}
      ///< Returns true if the chain specified in aDef can be extracted from this one.
     
      bool GetAlphaRange(MidasVector& aVals, In aBegin, In aLen) const;
      bool GetBetaRange(MidasVector& aVals, In aBegin, In aLen) const;
      bool GetXRange(MidasVector& aVals, In aBegin, In aLen, const string& aOper) const;
      Nb GetXetaNorm(const string& aOper) const;
      
      Nb GetBetaVal(In aIdx) const {return mTbeta[aIdx];}
      Nb GetAlphaVal(In aIdx) const {return mTalpha[aIdx];}
      Nb GetEtaNorm() const {return mEtaNorm;}
      
      MidasVector GetSpecificQVec(In);
      string GetFilePrefix() {return mFilePrefix;}

      MidasVector TransVecWithChain(MidasVector&);
      ///< Transforms inputvector with the chain vectors

      void Diag(In aDim, MidasVector* aEigVals, MidasMatrix* aEigVecs) const;
      ///< Diagonalize T matrix.

      void TransformStoX(const MidasVector& aS, DataCont& aX) const;
      ///< Transform S (eigenvector of T) to X (approximate eigenvector of full matrix).
      
      friend std::ostream& operator<<(std::ostream& aOut, const LanczosChain& aChain);
};

#endif

