/**
************************************************************************
* 
* @file                Xvec.cc
*
* Created:             11-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement Xvec class members, the general 
*                      excitation vector
* 
* Last modified:       02-06-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string> 
#include <vector> 
#include <typeindex>
#include <typeinfo>

// midas headers
#include "vcc/TensorDataCont.h"
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h" 
#include "input/Input.h" 
#include "util/Io.h" 
#include "util/Isums.h" 
#include "util/MultiIndex.h" 
#include "input/ModeCombi.h" 
#include "input/ModeCombiOpRange.h" 
#include "vcc/VccStateSpace.h"
#include "vcc/Xvec.h" 
#include "mmv/DataCont.h" 
#include "mmv/MMVT.h"
#include "tensor/Scalar.h"
#include "tensor/SimpleTensor.h"

// using declarations
using std::string;
using std::vector;
using std::make_pair;

Xvec::load_in_t    Xvec::load_in;
Xvec::load_out_t   Xvec::load_out;
Xvec::add_ref_t    Xvec::add_ref;
Xvec::no_add_ref_t Xvec::no_add_ref;

/**
 * Constructor, default
 **/
Xvec::Xvec() 
   : mExciRange()
   , mXvecData(DataCont())
   , mModeCombiAddress()
   , mpVccCalcDef(nullptr)
{
   mXvecData.get<DataCont>().Zero(); // zero elements so we avoid using uninitialized elements
   mNmodes = I_0;
}

/**
 * Convert mXvecData from TensorDataCont to DataCont format
 **/
void Xvec::ConvertToDataCont()
{
   // if already DataCont, we just return
   if (  mXvecData.type() == std::type_index(typeid(DataCont)) )
   {
      return; 
   }
   else
   {
      mXvecData = DataContFromTensorDataCont(mXvecData.get<TensorDataCont>());
   }
}

/**
 * Convert mXvecData from DataCont to TensorDataCont format
 **/
void Xvec::ConvertToTensorDataCont()
{
   // if already TensorDataCont, we just return
   if (  mXvecData.type() == std::type_index(typeid(TensorDataCont)) )
   {
      return;
   }
   else
   {
      mXvecData = TensorDataCont(mXvecData.get<DataCont>(), mExciRange, mpVccCalcDef, true);
   }
}

/**
 * Overload of Assign for loading IN data with and added reference set to 1.0
 *    - param `load_in_t` Load data into of Xvec
 *    - param `add_ref_t` Add ref compared to aData
 *
 * @param aData     Data to load in
 **/
void Xvec::Assign
   ( const TensorDataCont& aData
   , load_in_t
   , add_ref_t
   )
{
   auto& data = mXvecData.get<TensorDataCont>();
   
   assert(aData.Size() == (data.Size() - 1));
   
   data.GetModeCombiData(0) = NiceTensor<Nb>(new Scalar<Nb>(1.0));
   for(int i = 1; i < data.Size(); ++i)
   {
      data.GetModeCombiData(i) = aData.GetModeCombiData(i - 1);
   }
}

/**
 * Overload of Assign for loading OUT data removing reference
 *    - param `load_out_t` Load data out of Xvec
 *    - param `add_ref_t ` Add ref compared to aData (so in this case we
 *      actually "remove" ref before loading it out)
 *
 * @param aData      Container to load data out into
 **/
void Xvec::Assign
   ( TensorDataCont& aData
   , load_out_t
   , add_ref_t
   ) const
{
   auto& data = mXvecData.get<TensorDataCont>();
   
   assert(aData.Size() == (data.Size() - 1));
   
   for(int i = 1; i < data.Size(); ++i)
   {
      aData.GetModeCombiData(i - 1) = data.GetModeCombiData(i);
   }
}

/**
 * Overload of Assign for loading OUT data removing reference
 *    - param `load_in_t   ` Load data into Xvec
 *    - param `no_add_ref_t` Do not add ref compared to aData
 * 
 * @param aData      Container to load data out into
 **/
void Xvec::Assign
   ( const TensorDataCont& aData
   , load_in_t
   , no_add_ref_t
   )
{
   auto& data = mXvecData.get<TensorDataCont>();
   
   assert(aData.Size() == (data.Size()));
   
   for(int i = 0; i < data.Size(); ++i)
   {
      data.GetModeCombiData(i) = aData.GetModeCombiData(i);
   }
}

/**
 * Overload of Assign for loading OUT data removing reference
 * @param aData      Container to load data out into
 *    - param `load_out_t  ` Load data out of Xvec
 *    - param `no_add_ref_t` Do not add ref compared to aData
 **/
void Xvec::Assign
   ( TensorDataCont& aData
   , load_out_t
   , no_add_ref_t
   ) const
{
   auto& data = mXvecData.get<TensorDataCont>();
   
   assert(aData.Size() == (data.Size()));
   
   for(int i = 0; i < data.Size(); ++i)
   {
      aData.GetModeCombiData(i) = data.GetModeCombiData(i);
   }
}

/**
 * Copy Xvec meta-information, but no data.
 * @param arXvec           Xvec to copy meta-information from.
 **/
void Xvec::CopyNoData
   ( const Xvec& arXvec
   )
{
   mExciRange = arXvec.mExciRange;

   // If the input Xvec holds a TensorDataCont
   if (  arXvec.mXvecData.type() == std::type_index(typeid(TensorDataCont)) )
   {
      // Determine appropriate tensor type
      auto type   =  arXvec.mpVccCalcDef->GetDecompInfoSet().empty()
                  ?  BaseTensor<Nb>::typeID::SIMPLE
                  :  BaseTensor<Nb>::typeID::CANONICAL;

      // Construct state space
      VccStateSpace vss( arXvec.mExciRange, arXvec.mpVccCalcDef->GetNModals() );

      // Set mXvecData
      this->mXvecData = TensorDataCont( vss, type );
   }
   // If the input Xvec holds a DataCont
   else
   {
      mXvecData.get<DataCont>().SetNewSize(arXvec.mXvecData.get<DataCont>().Size(), false);
      // Initialize all elements to zero, so we avoid using elements that are not initialized!
      // this might not be performance-wise optimal though (but not performance critical either...)
      mXvecData.get<DataCont>().Zero();
      mXvecData.get<DataCont>().ChangeStorageLabelOnly(arXvec.mXvecData.get<DataCont>().Storage()); 
   }

   mpVccCalcDef      = arXvec.mpVccCalcDef;
   mModeCombiAddress = arXvec.mModeCombiAddress;
   mNmodes           = arXvec.mNmodes;
   mExciLevelAddress = arXvec.mExciLevelAddress;
}

/**
 * Get total size of excitation vector.
 * @return          Returns size.
 **/
In Xvec::Size
   (
   ) const 
{
   In size = I_0;
   
   if(mXvecData.type() == libmda::util::any_type::type_index(typeid(DataCont)))
   {
      size = mXvecData.get<DataCont>().Size();
   }
   else if(mXvecData.type() == libmda::util::any_type::type_index(typeid(TensorDataCont)))
   {
      size = mXvecData.get<TensorDataCont>().TotalSize();
   }
   else
   {
      size = -1;
      MIDASERROR("TYPE NOT KNOWN");
   }

   return size;
}

/**
 * Reinitialize from definitions of calculation 
 *
 * aLeftOpInit: An option whereby the
 * exci range is set according to <Left| Oper range,
 * and where at the same time, some addresses 
 * that are stored in apXvecLeft,
 * are initialized.
 *
 **/
void Xvec::ReInit
   ( VccCalcDef* apVccCalcDef
   , const OpDef* const apOpDef
   , const In aIsOrder
   , bool aLeftOpInit
   , Xvec* apXvecLeft
   )
{
   mpVccCalcDef  = apVccCalcDef;
   mNmodes = apOpDef->NmodesInOp(); // apOpDef defaulted to nullptr, but the second thing we do is try to access it, WTF??? :S
   // Check for special ModeCombiOpRange specifications, otherwise make a
   // "regular" one (i.e. from max. exc. level and max. ex.pr.mode).
   // 
   bool iso = mpVccCalcDef->Iso()||aIsOrder>I_0;
   bool flexcoupcalcdef = mpVccCalcDef->GetmVccFlexCoupCalcDef().GetmSetGroupCouplings()
            || mpVccCalcDef->GetmVccFlexCoupCalcDef().GetmSetSystemCoup()
            || mpVccCalcDef->GetmVccFlexCoupCalcDef().GetmMcScreen();
   // Check if we're doing initializing for left operator range.
   if (aLeftOpInit)
   {
      if (gVibIoLevel > 5) 
         Mout << " Construct a right ModeCombi space given by left*oper." << endl;
      ModeCombiOpRange tmp;
      for(  auto i = apXvecLeft->mExciRange.begin(), end = apXvecLeft->mExciRange.end()
         ;  i != end
         ;  ++i
         )
      {
         bool removed_dummy=false;
         std::vector<ModeCombi> v_mcs(mExciRange.begin(), mExciRange.end());
         SetToLeftOperRange
            (  v_mcs
            ,  removed_dummy
            ,  *i
            ,  *i
            ,  apOpDef->GetModeCombiOpRange()
            ,  tmp
            ,  true
            ,  true
            );
         mExciRange.Insert(std::move(v_mcs));
      }
   }
   else if (iso)
   {
      In is_order = mpVccCalcDef->IsOrder();
      if (aIsOrder!=I_0) is_order = aIsOrder; 
      // Use from mpVcc unless else is given as input to routine.

      if (gVibIoLevel > 5) 
         Mout << " Construct a " << is_order << " order ModeCombi space." << endl;
      if (is_order> I_0)
      {
         ModeCombiOpRange tmp;
         if (mpVccCalcDef->MaxExPrMode().size() > I_0|| mpVccCalcDef->MaxExciLevel() > -I_1)
         {
            if (gVibIoLevel > 5) 
               Mout << " restricted by max exci level and max pr mode." << endl;
            tmp = ConstructModeCombiOpRange
               (  mpVccCalcDef->MaxExciLevel()
               ,  mpVccCalcDef->MaxExPrMode()
               ,  mpVccCalcDef->GetNmodesInOcc()
               );
         }
         ModeCombi ref; // The reference - simple a zero ModeCombi;
         if (apOpDef == nullptr) 
            MIDASERROR("Xvec::ReInit error, apOpDef must not be nullptr for iso true");
         bool removed_dummy=false;
         std::vector<ModeCombi> v_mcs(mExciRange.begin(), mExciRange.end());
         SetToLeftOperRange
            (  v_mcs
            ,  removed_dummy
            ,  ref
            ,  ref
            ,  apOpDef->GetModeCombiOpRange()
            ,  tmp
            ,   true
            ,  false
            ,  true
            ,  is_order
            );
         mExciRange.Insert(std::move(v_mcs));
      }
   }
   // Else check if we're making ModeCombiOpRange from a FlexCoupCalcDef.
   else if(flexcoupcalcdef)
   {
      if (!mpVccCalcDef->MaxExPrMode().empty())
      {
         MIDASERROR("Can't handle both FlexCoupCalcDef and MaxExPrMode.");
      }
      else
      {
         if (gVibIoLevel > 5) 
         {
            Mout << " Construct a right ModeCombi space from FlexCoupCalcDef " << std::endl;
         }
         mExciRange = ConstructModeCombiOpRange
            (  mpVccCalcDef->GetmVccFlexCoupCalcDef()
            ,  mpVccCalcDef->MaxExciLevel()
            ,  mpVccCalcDef->GetNmodesInOcc()
            );
      }
   }
   // Else check if we're making ModeCombiOpRange from extended range modes.
   else if (mpVccCalcDef->UsingExtRangeModes())
   {
      const auto& modes = mpVccCalcDef->GetExtRangeModes();
      if (gVibIoLevel > 5) 
      {
         Mout  << " Construct a right ModeCombi space using extended range modes;\n"
               << "    effective max. exci. level = " << mpVccCalcDef->MaxExciLevel() << '\n'
               << "    number of modes            = " << mpVccCalcDef->GetNmodesInOcc() << '\n'
               << "    extended range modes       = " << std::vector<In>(modes.begin(),modes.end()) << '\n'
               << std::flush;
      }
      mExciRange = ConstructModeCombiOpRangeFromExtendedRangeModes
         (  mpVccCalcDef->GetExtRangeMaxEffExciLevel()
         ,  mpVccCalcDef->GetNmodesInOcc()
         ,  modes
         );
   }
   // If getting to here, make ModeCombiOpRange from max.exc.level and max.ex.pr.mode.
   else
   {
      if (gVibIoLevel > 5) 
      {
         Mout << " Construct a right ModeCombi space given by exci level and ex.pr.mode " << std::endl;
      }
      mExciRange = ConstructModeCombiOpRange
         (  mpVccCalcDef->MaxExciLevel()
         ,  mpVccCalcDef->MaxExPrMode()
         ,  mpVccCalcDef->GetNmodesInOcc()
         );
   }

   // Set addresses, and fill in addresses in mModeCombiAddress.
   const auto xvec_tot_size = SetAddresses(mExciRange, mpVccCalcDef->GetNModals());
   mModeCombiAddress.clear();
   mModeCombiAddress.reserve(mExciRange.Size()+I_1);
   for(const auto& mc: mExciRange)
   {
      mModeCombiAddress.emplace_back(mc.Address());
   }
   mModeCombiAddress.emplace_back(xvec_tot_size);
   
   // Set ModeCombi level addresses in mExciLevelAddress and some assertions.
   mExciLevelAddress.clear();
   mExciLevelAddress.reserve(mExciRange.GetMaxExciLevel()+I_2);
   for(Uin n_excited = 0; n_excited <= mExciRange.GetMaxExciLevel(); ++n_excited)
   {
      if (mExciRange.Begin(n_excited) != mExciRange.end())
      {
         mExciLevelAddress.emplace_back(mExciRange.Begin(n_excited)->Address());
      }
      else
      {
         MIDASERROR("mExciRange.Begin("+std::to_string(n_excited)+") == end(), unexpectedly.");
      }
   }
   mExciLevelAddress.emplace_back(xvec_tot_size);
   

   if (gVibIoLevel > 5)
      Mout << " Xvec initialized mExciRange with adresses:" << endl << mExciRange << endl
           << " Addresses of excitation levels in order 0, 1, 2, ..., [end of vector+1]:" << endl
           << "    " << mExciLevelAddress << endl;
   
   if (gVibIoLevel > 5) 
      Mout << " The size of the Xvec with the present MaxExciLevel "
         << " and MaxExPrMode: " << xvec_tot_size << endl;
   string storage = "InMem";
   if (mpVccCalcDef->VecStorage()>I_2) storage = "OnDisc";  

   // Avoid creating full DataCont if using tensors
   if (  this->mpVccCalcDef->UseTensorNlSolver()
      && this->mpVccCalcDef->Vcc()
      )
   {
      // Determine appropriate tensor type
      auto type   =  this->mpVccCalcDef->GetDecompInfoSet().empty()
                  ?  BaseTensor<Nb>::typeID::SIMPLE
                  :  BaseTensor<Nb>::typeID::CANONICAL;

      // Construct VccStateSpace
      VccStateSpace vss( this->mExciRange, this->mpVccCalcDef->GetNModals() );

      // Set mXvecData
      mXvecData = TensorDataCont( vss, type );
   }
   else
   {
      // In Mem unless VecStorage is set appropriately high in input
      mXvecData.get<DataCont>().ChangeStorageTo(storage);              
      mXvecData.get<DataCont>().SetNewSize(xvec_tot_size);
   }
}

/**
 * Reinitialize of a ``smaller'' Xvec for improved preconditioning 
 **/
void Xvec::ReInit2
   ( VccCalcDef* apVccCalcDef
   , In aPrecondExciLevel
   , Xvec* apXvecOld
   , const OpDef* const apOpDef
   )
{
   mpVccCalcDef  = apVccCalcDef;
   mNmodes = apXvecOld->mNmodes;

   //In n_mode_combi = mExciRange.Size();

   mExciLevelAddress.resize(I_0);
   mModeCombiAddress.resize(I_0);

   size_t count = I_0;
   for (In i=0;i<aPrecondExciLevel+I_1;i++) 
   {
      count += apXvecOld->mExciRange.NumModeCombisWithExcLevel(i);
      mExciLevelAddress.push_back(apXvecOld->mExciLevelAddress[i]);
   }
   mExciLevelAddress.push_back(apXvecOld->mExciLevelAddress[aPrecondExciLevel+I_2]);

   for (size_t i=0;i<count+I_1;i++)
      mModeCombiAddress.push_back(apXvecOld->mModeCombiAddress[i]);

   mExciRange.Insert(std::vector<ModeCombi>(apXvecOld->mExciRange.begin(), apXvecOld->mExciRange.begin()+count));

   for (size_t i_mode_combi=0; i_mode_combi<mExciRange.Size(); ++i_mode_combi)
   {
      const ModeCombi& modes_excited = mExciRange.GetModeCombi(i_mode_combi);
      mExciRange.AssignAddress(i_mode_combi, mModeCombiAddress[i_mode_combi]);
      if (gVibIoLevel > 5) 
      {
         Mout << " The modes excited are: " << modes_excited<<endl;
      }
      if (gVibIoLevel > 5) Mout 
         << " The ModeCombiAddress for i_mode_combi " << i_mode_combi 
         << " is : " << mModeCombiAddress[i_mode_combi] << endl;
   }
   string storage = "InMem";
   if (mpVccCalcDef->VecStorage()>I_2) storage = "OnDisc";  

   // Avoid creating full DataCont if using tensors
   if (  this->mpVccCalcDef->UseTensorNlSolver()   )
   {
      // Determine appropriate tensor type
      auto type   =  this->mpVccCalcDef->GetDecompInfoSet().empty()
                  ?  BaseTensor<Nb>::typeID::SIMPLE
                  :  BaseTensor<Nb>::typeID::CANONICAL;

      // Construct VccStateSpace
      VccStateSpace vss( this->mExciRange, this->mpVccCalcDef->GetNModals() );

      // Set mXvecData
      mXvecData = TensorDataCont( vss, type );
   }
   else
   {
      // In Mem unless VecStorage is set appropriately high in input
      mXvecData.get<DataCont>().ChangeStorageTo(storage);              
      mXvecData.get<DataCont>().SetNewSize(apXvecOld->AddressForExciLevel(aPrecondExciLevel+I_1));
   }
}

/**
 * N of individual excitations for a particular modecombination number 
 * Using the vector<ModeCombi> information.
 *
 * @param arModeCombiNr                 The ModeCombi number to get information for.
 * @return                              Return the number of excitations for the ModeCombi.
 **/
In Xvec::NexciForModeCombi
   ( const In& arModeCombiNr
   ) const
{
   if (mExciRange.Size()==0) return 0; 
   if (mExciRange.Size()==1) return mExciRange.Size();
   if (arModeCombiNr <= mExciRange.Size()-2)
   {
      return mExciRange.GetModeCombi(arModeCombiNr+1).Address()
      -mExciRange.GetModeCombi(arModeCombiNr).Address();
   }
   //return mXvecData.get<DataCont>().Size()-mExciRange.GetModeCombi(arModeCombiNr).Address();
   return this->Size() - mExciRange.GetModeCombi(arModeCombiNr).Address();
} 

/**
* Find a certain mode number including its iterator and ModeCombiNr.
* This function assumes the MC reference no. has been set!
* */
bool Xvec::Find
   (  const ModeCombi& arModeCombi
   ,  ModeCombiOpRange::const_iterator& arIterator
   ,  In& arModeCombiNr
   )  const
{
   return mExciRange.Find(arModeCombi, arIterator, arModeCombiNr);
}
/**
* Find a certain mode number for a certain integer value of the adress
* */
In Xvec::FindModeCombiForInt
   ( const In& arInt
   ) const
{
   In n_mode_combi = NexciTypes();
   for (In i_mode_combi = I_0; i_mode_combi<n_mode_combi-I_1;i_mode_combi++)
   {
      In ilow = mModeCombiAddress[i_mode_combi];
      In ihig = mModeCombiAddress[i_mode_combi+I_1];
      if ((arInt>=ilow)&&(arInt<ihig)) return i_mode_combi;
   }
   if (n_mode_combi>I_0)
   {
      In ilow = mModeCombiAddress[n_mode_combi-I_1];
      In ihig = mXvecData.get<DataCont>().Size();
      if ((arInt>=ilow)&&(arInt<ihig)) return n_mode_combi-I_1;
   }
   MIDASERROR( " arInt is out of range for ModeCombiOpRange in Xvec " );
   return I_0;
}

/**
 * Find a certain mode number including its iterator and ModeCombiNr.
 **/
In Xvec::SizeOut
   (
   ) const 
{
   Mout << " Output Size for Xvec:          " << endl;
   Mout << " A pointer to VccCalcDef        " << sizeof(mpVccCalcDef) << endl; 
   Mout << " Nr of In in mModeCombiAddress  " << mModeCombiAddress.size() << endl;
   Mout << " Size of mModeCombiAddress      " << sizeof(mModeCombiAddress) << endl;
   Mout << " Size of In                     " << sizeof(In) << endl;
   Mout << " ModeCombiOpRange size:         " << endl;
   In siz_tot = sizeof(mpVccCalcDef) + sizeof(In) + sizeof(mModeCombiAddress) + sizeof(In)*mModeCombiAddress.size();
   siz_tot   += mExciRange.SizeOut(Mout);
   siz_tot   += mXvecData.get<DataCont>().SizeOut();
   Mout << " Total estimated Xvec Size " << siz_tot << " byte " << endl;
   return siz_tot;
}

/**
 *
 **/
void Xvec::UpdateNorms2()
{
   if (gDebug)
      Mout << "Xvec::UpdateNorms2()..." << endl;
   Mout << "Xvec::UpdateNorms2()..." << endl;
   mNorms2.resize(NexciTypes());
   for (In i_mc=I_0; i_mc<NexciTypes(); ++i_mc)
   {
      In addr = ModeCombiAddress(i_mc);
      In n_exci = NexciForModeCombi(i_mc);
      mNorms2[i_mc] = mXvecData.get<DataCont>().Norm2(addr, n_exci);
   }
}

/**
* Find the address for a particular vector of modal occups. 
* */
In Xvec::AddressForOccVec
   ( const std::vector<In>& arIvec
   ) const
{
   In i_add = I_0;
   //Mout << " get address for occ vec = " << arIvec << endl;
   vector<In> modes_exci;
   vector<In> modes_exci_for_modes;
   bool outside=false;
   for (In i=I_0;i<mNmodes;i++)
   {
      In occ = I_0; // NOT!!: pVscfCalcDef()->GetOccMode(i);
      In rel_to_occ = arIvec[i]-occ;
      if (arIvec[i]>=mpVccCalcDef->Nmodals(i)) 
      {
         outside=true;
         i_add = -I_1;
         break;
      }
      if (rel_to_occ != I_0) 
      {
         modes_exci.push_back(i); 
         modes_exci_for_modes.push_back(rel_to_occ); 
      }
   }
   In exci_level = modes_exci.size();
   //Mout << " exci level " << exci_level << endl;
   if (exci_level >I_0 && !outside)
   {
      // Get the addree of the ModeCombi 
      //Mout << " modes_exci " << modes_exci << endl;
      //Mout << " modes_exci_for_modes " << modes_exci_for_modes << endl;
      ModeCombi mc(modes_exci,-I_1);
      bool found = mExciRange.IfContainedAssignAddressAndRefNum(mc);
      //Mout << " mc = " << mc << endl;
      if (found)
      {
         i_add = mc.Address();
         // Now the relative address inside the modecombi
         In nmodes = exci_level;
         vector<In> occ(nmodes);
         vector<In> occmax(nmodes);
         for (In i=I_0;i<nmodes;i++) 
         {
            occ[i]       = I_0;
            In i_op_mode = modes_exci[i];
            In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
            occmax[i]    = nmod-I_1;
         }
         string lowhig = "LOWHIG";
         bool exci_only=true; 
         MultiIndex mi(occ,occmax,lowhig,exci_only);
         In redadd=-I_1;
         mi.InForIvec(modes_exci_for_modes,redadd);
         //Mout << " reduced adress " << redadd << endl;
         i_add+= redadd;
      }
      else
      {
         i_add=-I_1; // Not found -> outside excitation space 
      }
   }
   //Mout << " i_add found = " << i_add << endl;
   return i_add;
}

/**
 * 
 **/
void Xvec::PrintWeightsInMCs
   ( const vector<vector<In> >& arMCs
   , DataCont& aEigVec
   , In aOffset
   ) const
{
   map<pair<In, vector<In> >, Nb> data;
   MidasVector eigvals(aEigVec.Size());
   aEigVec.DataIo(IO_GET, eigvals, aEigVec.Size());
   Mout << "Eigvec norm2 : " << aEigVec.Norm2() << endl;
   for(In i = I_0; i < eigvals.Size(); ++i)
   {
      vector<In> in_mcs;
      In mcnr = FindModeCombiForInt(i+aOffset);
      const ModeCombiOpRange& mcr = GetModeCombiOpRange();
      const ModeCombi& modes = mcr.GetModeCombi(mcnr);
      for(In j = I_0; j < arMCs.size(); ++j)
         for(In k = I_0; k < modes.Size(); ++k)
            if(std::find(arMCs[j].begin(), arMCs[j].end(), modes.Mode(k)) != arMCs[j].end())
               in_mcs.push_back(j);
      pair<map<pair<In, vector<In> >, Nb>::iterator, bool> is_in = data.insert(make_pair(make_pair(modes.Size(), in_mcs), eigvals[i]*eigvals[i]));
      if(!is_in.second)
         is_in.first->second += eigvals[i]*eigvals[i];
   }
   map<pair<In, In>, Nb> data_2;
   for(map<pair<In, vector<In> >, Nb>::iterator it = data.begin(); it != data.end(); ++it)
   {
      Mout << it->first.first << " " << it->first.second << " : " << sqrt(it->second) << endl;
      set<In> temp(it->first.second.begin(), it->first.second.end());
      pair<map<pair<In,In>, Nb>::iterator, bool> is_in = data_2.insert(make_pair(make_pair(it->first.first,temp.size()), it->second));
      if(!is_in.second)
         is_in.first->second += it->second;
   }
   Mout << "For Intra Molecular Mode Couplings : " << endl;
   for(map<pair<In, In>, Nb>::iterator it = data_2.begin(); it != data_2.end(); ++it)
   {
      Mout << it->first.first << " " << it->first.second << " : " << sqrt(it->second) << endl;
   }
}

/**
 * Get data for mode combination as a MidasVector.
 * @param aVector
 *    Vector to save data in. If aHasRef is true vector must be \#elements + 1
 *    long, as 0th index in Vector is not overwritten.
 * @param aMC                   Mode combination to get data for.
 * @param aHasRef               Does the vector have a reference.
 **/
void Xvec::GetModeCombiData
   (  MidasVector& aVector
   ,  const ModeCombi& aMC
   ,  bool aHasRef
   )  const
{
   // get information on mode combination
   ModeCombiOpRange::const_iterator it_mc;
   In i_mc;
   if(!this->Find(aMC, it_mc, i_mc))
   {
      MIDASERROR("Did not find modecombination");
   }

   // get the data according which type is stored in Xvec
   if(mXvecData.type() == libmda::util::any_type::type_index(typeid(DataCont)))
   {
      In n_exci  = this->NexciForModeCombi(i_mc);
      In address = this->ModeCombiAddress(i_mc);
      if(aHasRef)
      {
         mXvecData.get<DataCont>().DataIo(IO_GET, aVector, n_exci, address, I_1, I_1);
      }
      else
      {
         mXvecData.get<DataCont>().DataIo(IO_GET, aVector, n_exci, address, I_1, I_0);
      }
   }
   else if(mXvecData.type() == libmda::util::any_type::type_index(typeid(TensorDataCont)))
   {
      if(aHasRef)
      {
         MidasVector mc_data;
         ReassignMidasVectorFromNiceTensor(mc_data, mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc));
         for(int i = 0; i < mc_data.Size(); ++i)
         {
            aVector[i + 1] = mc_data[i];
         }
      }
      else
      {
         ReassignMidasVectorFromNiceTensor(aVector, mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc));
      }
   }
   else
   {
      MIDASERROR("Type not known.");
   }
}

/**
 * Set data for mode combination as a MidasVector.
 * @param aVector 
 *    Vector to save data in. If aHasRef is true vector must be \#elements + 1
 *    long, as 0th index in Vector is not overwritten.
 * @param aMC                   Mode combination to get data for.
 * @param aHasRef               Does the vector have a reference.
 **/
void Xvec::SetModeCombiData
   ( const MidasVector& aVector
   , const ModeCombi& aMC
   , bool aHasRef
   )
{
   // get information on mode combination
   ModeCombiOpRange::const_iterator it_mc;
   In i_mc;
   if(!this->Find(aMC, it_mc, i_mc))
   {
      MIDASERROR("Did not find modecombination");
   }
   In n_exci  = this->NexciForModeCombi(i_mc);

   // get the data according which type is stored in Xvec
   if(mXvecData.type() == libmda::util::any_type::type_index(typeid(DataCont)))
   {
      In address = this->ModeCombiAddress(i_mc);
      if(aHasRef)
      {
         mXvecData.get<DataCont>().DataIo(IO_PUT, aVector, n_exci, address, I_1, I_1);
      }
      else
      {
         mXvecData.get<DataCont>().DataIo(IO_PUT, aVector, n_exci, address, I_1, I_0);
      }
   }
   else if(mXvecData.type() == libmda::util::any_type::type_index(typeid(TensorDataCont)))
   {
      std::vector<unsigned> res_modals(aMC.Size());
      for (In i=I_0; i<aMC.Size(); ++i)
      {
         res_modals[i] = static_cast<unsigned>(mpVccCalcDef->Nmodals(aMC.Mode(i))-I_1);
      }

      if(aHasRef)
      {
         MidasVector mc_data(n_exci);
         for(int i = 0; i < mc_data.Size(); ++i)
         {
           mc_data[i] =  aVector[i + 1];
         }

         if(aMC.Size() == 0)
         {
            mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc) = NiceTensor<Nb>(new Scalar<Nb>(mc_data[0]));
         }
         else
         {
            mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc) = NiceTensor<Nb>(new SimpleTensor<Nb>(res_modals, mc_data.data()));
            mc_data.SetPointerToNull();
         }
      }
      else
      {
         MidasVector mc_data(n_exci);
         for(int i = 0; i < mc_data.Size(); ++i)
         {
           mc_data[i] =  aVector[i];
         }

         if(aMC.Size() == 0)
         {
            mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc) = NiceTensor<Nb>(new Scalar<Nb>(mc_data[0]));
         }
         else
         {
            mXvecData.get<TensorDataCont>().GetModeCombiData(i_mc) = NiceTensor<Nb>(new SimpleTensor<Nb>(res_modals, mc_data.data()));
            mc_data.SetPointerToNull();
         }
      }
   }
   else
   {
      MIDASERROR("Type not known.");
   }
}

/**
 * Set new label for DataCont.
 * @param aNewLabel        New label as std::string.
 * @param aMoveDataAlong   Move new data along.
 **/
void Xvec::NewLabel
   ( const std::string& aNewLabel
   , bool aMoveDataAlong
   )
{
   if(mXvecData.type() == libmda::util::any_type::type_index(typeid(DataCont)))
   {
      mXvecData.get<DataCont>().NewLabel(aNewLabel, aMoveDataAlong);
   }
   else
   {
      MIDASERROR("not implemted for type.");
   }
}

/**
 * Get the reference.
 * @return         Returns reference as Nb.
 **/
Nb Xvec::Ref
   (
   )  const
{
   Nb reference = C_0;
   if(mXvecData.type() == libmda::util::any_type::type_index(typeid(DataCont)))
   {
      mXvecData.get<DataCont>().DataIo(IO_GET, I_0, reference);
   }
   else if(mXvecData.type() == libmda::util::any_type::type_index(typeid(TensorDataCont)))
   {
      reference = mXvecData.get<TensorDataCont>().GetModeCombiData(0).GetScalar();
   }
   else
   {
      MIDASERROR("TYPE NOT KNOWN");
   }
   return reference;
}

/**
 *
 **/
const TensorDataCont& Xvec::GetTensorXvecData() const
{
   return mXvecData.get<TensorDataCont>();
}

/**
 *
 **/
TensorDataCont& Xvec::GetTensorXvecData()
{
   return mXvecData.get<TensorDataCont>();
}
