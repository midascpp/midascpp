/**
************************************************************************
* 
* @file                BandLanczosChain.h
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen (mrgodtliebsen@hotmail.com)
*
* Short Description:   Band Lanczos chain (banded matrix) definitions.
* 
* Last modified: Thu Dec 09, 2010  04:24PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BANDLANCZOSCHAIN_H
#define BANDLANCZOSCHAIN_H

// Standard Headers
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "mmv/DataCont.h"
#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"
#include "vcc/LanczosRspFunc.h"

///> Known from LanczosRspFunc.h
///> LANCZOS_ERROR              = I_0
///> LANCZOS_ORTHO_NONE         = I_1
///> LANCZOS_ORTHO_FULL         = I_2
///> LANCZOS_ORTHO_PERIODIC     = I_3
///> LANCZOS_ORTHO_PARTIAL      = I_4

class BandLanczosChainDef : public LanczosChainDefBase
{
   protected:
      In             mBlock;        // Number of block starting vectors
      bool           mSave;         // Save the Band Lanczos Chain or not.
      bool           mTest;
      Nb             mDtol;         // Sets deflation tolerance.
      Nb             mConv;         // Sets convergence criterion.
      Nb             mConvVal;      // Sets the value at which we convergence below.
      string         mDiagMethod;   // Sets the method used i diagonalizations
      vector<string> mOpers;
      bool           mDeflate;

   public:
      ///> Constructor
      BandLanczosChainDef(In aLength=I_0, In aBlock=I_0, In aOrtho = I_2, 
                          bool aSave=false, bool aTest=false, Nb aDtol=C_0, Nb aConv=C_0,
                          Nb aConvVal=C_0, bool aDeflate=true);

      ///> Get Values in BandChainDef
      const In& GetLength() const             {return mLength;}
      const In& GetBlock() const              {return mBlock;}
      const Nb& GetDtol() const               {return mDtol;}
      const Nb& GetConv() const               {return mConv;}
      const In& GetOrtho() const              {return mOrtho;}
      const bool& GetSave() const             {return mSave;}
      const bool& GetTest() const             {return mTest;}
      const vector<string>& GetOpers() const  {return mOpers;}
      const string& GetOper(const In i) const {return mOpers[i];}
      const string& GetDiagMethod() const     {return mDiagMethod;}
      const bool& GetDeflate() const          {return mDeflate;}
      const Nb& GetConvVal() const            {return mConvVal;}
      ///> Other Functions
      void AddOper(const string& aOper)               {mOpers.push_back(aOper);}
      void SetDiagMethod(const string& aDiagMethod)   {mDiagMethod = aDiagMethod;}
      void SetLength(In aLength)                      {mLength = aLength;}
      void SetBlock(In aBlock)                        {mBlock = aBlock;}

      //bool Combine(LanczosChainDefBase* aOther);
      bool Combine(const BandLanczosChainDef& aOther);
      //bool Compatible(LanczosChainDefBase* aOther) const;
      bool Compatible(const BandLanczosChainDef& aOther) const;
      ///< Returns true if aOther is a subset of or identical to this definition.

      ///> Output operator
      friend ostream& operator<<(ostream& aOut, const BandLanczosChainDef& aDef);
};

class BandLanczosChain
{
   protected:
      BandLanczosChainDef     mDef;          ///< Definition of the Chain.
      MidasMatrix             mT;            ///< Holds T matrix after Evaluation.
      MidasMatrix             mW;            ///< Holds W matrix when doing periodic or partial ortho.
      vector<DataCont>        mQ;            ///< Holds Q matrix after Evaluation.
      vector<DataCont>        mEta;          ///< Holds Eta vectors for rsp calc.
      MidasVector             mEigenval;     ///< Holds Eigenvalues of T after diagonalization.
      Vcc*                    mpVcc;         ///< Vcc class
      VccTransformer*         mpTrf;         ///< Tranformer
      string                  mFilePrefix;
      In                      mOrthoCount;   ///< Counter used in periodic and partial ortho.
      MidasMatrix             mOrthoIndex;   ///< Matrix used in partial ortho.
      
   public:
      ///> Constructor
      BandLanczosChain(const BandLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf);

      ///> Evaluate the BandLanczosChain.
      void Evaluate();

      ///> Functions used by evaluate
      void MakeStartVectors(const In nc, const vector<string>& aOpers);
      void TransformBand(DataCont& aqj, const In anc, const In aJ, Nb& aTime);
      bool Restart(In& aJ, In& aPc, bool& aFullyDeflated); 
      void SaveChain();

      const In& GetBlock() const           {return mDef.GetBlock();}
      const In& GetLength() const          {return mDef.GetLength();}
      const string& GetDiagMethod() const  {return mDef.GetDiagMethod();}
      const string& GetName() const        {return mFilePrefix;}
      In GetNumEta() const                 {return mEta.size();}
      
      void GetStartVector(const In aVec, DataCont& aQ) const;
      void GetEtaVector(const In aVec, DataCont& aEta) const;
      void GetT(MidasMatrix& aMat) const;
      
      ///> Different ortho schemes
      void Full_Ortho(DataCont& aVec, const In aJ, const In pc, const In repeat=I_3);
      void Periodic_Ortho(DataCont& aVec, MidasMatrix& aW, const In aJ, const In pc, 
                      const Nb aqjNorm, In& aOrthoCount);
      void Partial_Ortho(DataCont& aVec, MidasMatrix& aW, const In aJ, const In pc, 
                      const Nb aqjNorm, In& aOrthoCount, MidasMatrix& aOrthoIndex);
      
      ///> Used by Rsp func class
      bool Compatible(const BandLanczosChainDef& aDef) const {return mDef.Compatible(aDef);}
      void BandDiag(In aDim, MidasVector& aEigVals, MidasMatrix& aEigVecs) const;
      void TransformStoX(const MidasVector& aEigVec, DataCont& aX) const;

      ///> Testing and debugging functions
      bool TestConvergence(const In ItNum, const In pc, const vector<In>& DefIndex);
      void TestResidual(const In ItNum, const In anc);
      void CalcOrtho();
      void Test_T(const In ItNum, const In anc);
};

#endif
