/**
************************************************************************
* 
* @file                NHermLanczosChain.h
*
* Created:             25-04-2010
*
* Author:              Bo Thomsen    (bo@thomsen.tdcadsl.dk)
*
* Short Description:   Lanczos chain (tridiagonal matrix) definition, for the nonhermitian case
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NHERMLANCZOSCHAIN_H
#define NHERMLANCZOSCHAIN_H

const In I_LEFT = I_1;
const In I_RIGHT = I_2;

// Standard Headers
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "mmv/DataCont.h"
#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"
#include "vcc/LanczosRspFunc.h"

class NHermLanczosChainDef : public LanczosChainDefBase
{
   protected:
      string mOper;                 ///< Initial q(0) vector is set to normalized eta^(mOper).
      //In mLength;                   ///< Length of chain (dimension of matrix).
      //In mOrtho;                    ///< Orthogonalization scheme.
      In mRealLength;

      vector<string> mXopers;             
      ///< List of operators for which to calcluate x(j) = eta^oper * q(j).
      ///< Note that eta is normalized in this case.
   
   public:
      NHermLanczosChainDef(const string& aOper="", In aLength=I_0,
      In aOrtho=LANCZOS_ORTHO_NONE, vector<string>* const aXopers=NULL);


      void  SetRealLength(In aLength)         {mRealLength = aLength;}
      In  GetRealLength()                     {return mRealLength;}

      void  SetOper(const string& aOper)      {mOper = aOper;}
      const string& GetOper() const           {return mOper;}
      void  SetLength(In aLength)             {mLength = aLength;}
      In    GetLength() const                 {return mLength;}
      void  SetOrtho(In aOrtho)               {mOrtho = aOrtho;}
      In    GetOrtho() const                  {return mOrtho;}
      void  AddXoper(const string& aXoper)    {mXopers.push_back(aXoper);}
      const string& GetXoper(In aIdx)         {return mXopers[aIdx];}
      const vector<string>& GetXopers() const {return mXopers;}
      In    NXopers() const                   {return mXopers.size();} 
      bool  Combine(const NHermLanczosChainDef&);
      ///< Combine this and aOther into a single definition. Returns true if possible and stores
      ///< new definition in this. Otherwise return false.

      bool Compatible(const NHermLanczosChainDef& aOther) const;
      ///< Returns true if aOther is a subset of or identical to this definition.
      
      friend std::ostream& operator<<(std::ostream& aOut, const NHermLanczosChainDef& aDef);
};


class NHermLanczosChain
{
   protected:
      NHermLanczosChainDef     mDef;
      In             mRestartLen;
      Nb             mEtaNorm;
      Nb             mXiNorm;
      Nb             mDotProd;
      MidasVector      EvalsRes;
      //MidasMatrix      BandMatrix;
      MidasMatrix      Evectors;      
      //MidasVector      mTgamma;
      //MidasVector      mTalpha;
      //MidasVector      mTbeta;
      MidasVector      Evals;
      vector<DataCont>   normWvec;
      vector<DataCont>   normQvec;
      string         mFilePrefix;

      
      Vcc*                mpVcc;
      ///< Pointer to VCC class responsible for this calculation.
      
      VccTransformer*     mpTrf;
      ///< Pointer to VCC transformer class to be used.

      Nb CalcEta(const string& aOper, DataCont& aDc);
          
   public:
      In       GetRestartLen() const {return mRestartLen;}
      MidasVector             mTbeta;
      MidasVector             mTalpha; //Move to protected later
      MidasVector             mTgamma;
      NHermLanczosChain(const NHermLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf);
      Nb       GetEtaNorm() const {return mEtaNorm;}
      Nb       GetXiNorm() const {return mXiNorm;}
      Nb       GetDotProd() const {return mDotProd;}
      string   GetFilePrefix() const {return mFilePrefix;}
      MidasMatrix    BandMatrix;
      In       GetLenght() const {return mDef.GetLength();}
      
      void     WriteInitialValues();
      bool     Compatible(const NHermLanczosChainDef& aDef) const {return mDef.Compatible(aDef);}
      void     Diago(MidasVector&,MidasMatrix&, MidasMatrix&) const;//This really needs to be changed eventually, Diag for Rsp
      void      ExpCalcBandMat(In aJ);
      void      Output(); //Quick function to output eigenvalues
      void      Evaluate(); //Should do all the work
      void      SaveVec(DataCont & , In, string);
      void      Transfor(DataCont & , DataCont &, In, In);
      void      Diagonalize(); //Diagonalises T
      void      MakeMatrix(MidasVector&, MidasVector&, MidasVector&); //Makes a real matrix, should be removed when testing is done
      void      CalcRes();
      void      CalcEvecs();
      void      Orthogonalize(DataCont&, DataCont&, In); //Orthogonalises the q,p vectors to previous vectors
      Nb         CreateP0(DataCont&, const string&, DataCont&);
      bool     Restart(In& aLength, DataCont& aQj, DataCont& aPj, DataCont& aR, DataCont& aS);
      void     WriteValuesToFile(In aIdx, ios_base::openmode mode);
      
      void     TransformStoX(const MidasVector&, DataCont&, bool qTrans) const;

};
#endif
