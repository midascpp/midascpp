/**
 *******************************************************************************
 * 
 * @file    ModalIntegralsFuncs_Impl.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Utility functions related to the ModalIntegrals class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODALINTEGRALSFUNCS_IMPL_H_INCLUDED
#define MODALINTEGRALSFUNCS_IMPL_H_INCLUDED

#include "vscf/Vscf.h"
#include "vcc/Vcc.h"
#include "td/PrimitiveIntegrals.h"

namespace midas::vcc::modalintegrals
{
   /************************************************************************//**
    * This function simply copies the integral matrices from argument object to
    * newly constructed ModalIntegrals object. No transformations or anything -
    * clients own responsibility. (But see other ConstructModalIntegrals()
    * function.)
    *
    * @note
    *    If performance/memory issue: Introduce version taking a
    *    PrimitiveIntegrals<...>&& and a corresponding
    *    PrimitiveIntegrals<...>::TransformModalBasis(...) && version to be
    *    used for _moving_ the integral matrices instead of copying.
    *
    * @note
    *    The VEC_T, MAT_T template arguments needed for proper compilation with
    *    clang, but should probably always be GeneralMidasVector,
    *    GeneralMidasMatrix to work.
    *
    * @param[in] arPrimInts
    *    The PrimitiveIntegrals container.
    * @return
    *    Newly constructed ModalIntegrals<T> object containing the matrices of
    *    arPrimInts.
    ***************************************************************************/
   template<typename T, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  const midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&
         arPrimInts
      )
   {
      const Uin n_modes = arPrimInts.NModes();
      std::vector<std::vector<typename ModalIntegrals<T>::mat_t>> v;
      v.reserve(n_modes);
      for(Uin m = 0; m < n_modes; ++m)
      {
         const Uin n_opers = arPrimInts.NOper(m);
         v.emplace_back();
         v.back().reserve(n_opers);
         for(Uin o = 0; o < n_opers; ++o)
         {
            v.back().emplace_back(arPrimInts.GetTimeIndependentIntegrals(m,o));
         }
      }
      return ModalIntegrals<T>(std::move(v));
   }

   template<typename T, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&&
         arPrimInts
      )
   {
      const Uin n_modes = arPrimInts.NModes();
      std::vector<std::vector<typename ModalIntegrals<T>::mat_t>> v;
      v.reserve(n_modes);
      for(Uin m = 0; m < n_modes; ++m)
      {
         const Uin n_opers = arPrimInts.NOper(m);
         v.emplace_back();
         v.back().reserve(n_opers);
         for(Uin o = 0; o < n_opers; ++o)
         {
            v.back().emplace_back(std::move(arPrimInts).GetTimeIndependentIntegrals(m,o));
         }
      }
      return ModalIntegrals<T>(std::move(v));
   }

   /************************************************************************//**
    * @note
    *    Probably causes hard error if PrimitiveIntegrals have already been
    *    transformed to modal basis -- see PrimitiveIntegrals documentaion for
    *    details.
    *
    * @note
    *    The VEC_T, MAT_T template arguments needed for proper compilation with
    *    clang, but should probably always be GeneralMidasVector,
    *    GeneralMidasMatrix to work.
    *
    * @param[in] arPrimInts
    *    The PrimitiveIntegrals container, holding integrals in the primitive
    *    basis.
    *    Note: These will be modified, i.e. transformed from primitive to modal
    *    basis!
    * @param[in] arModals
    *    Coefficients describing basis transformation from primitive -> modal.
    *    Vectorized storage; blocks for each mode; each mode block contains
    *    sequences of `n_bas` coefficients, one for each modal. Up to `n_bas`
    *    of these sequences. Must be at least `n_limit` sequences (i.e. modals)
    *    for a given mode, where `n_limit` is taken from arModalBasisLimits.
    * @param[in] arModalOffsets
    *    Index into arModals, indicating where each mode block begins.
    * @param[in] arModalBasisLimits
    *    How many modals do we want modal integrals for for each mode?
    * @return
    *    Newly constructed ModalIntegrals<T> object containing the matrices of
    *    arPrimInts, after having transformed the primitive ones to modal
    *    integrals.
    ***************************************************************************/
   template<typename T, typename U, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&
         arPrimInts
      ,  const GeneralDataCont<U>& arModals
      ,  const std::vector<In>& arModalOffsets
      ,  const std::vector<In>& arModalBasisLimits
      )
   {
      arPrimInts.TransformModalBasis(arModals, arModalOffsets, arModalBasisLimits);
      return ConstructModalIntegrals(arPrimInts);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   ModalIntegrals<T> ConstructModalIntegralsElemOper
      (  Uin aElemOperMode
      ,  Uin aElemOperP
      ,  Uin aElemOperQ
      ,  const std::vector<Uin>& arNModals
      ,  const std::vector<Uin>& arNOpers
      )
   {
      if (arNModals.size() != arNOpers.size())
      {
         MIDASERROR
            (  "Size mismatch; arNModals.size() = "+std::to_string(arNModals.size())
            +  ", arNOpers.size() = "+std::to_string(arNOpers.size())
            );
      }
      if (aElemOperMode > arNModals.size())
      {
         MIDASERROR
            (  "Modes mismatch; aElemOperMode = "+std::to_string(aElemOperMode)
            +  " > arNModals.size() = "+std::to_string(arNModals.size())
            );
      }
      std::vector<std::vector<typename ModalIntegrals<T>::mat_t>> v_mats;
      for(Uin m = 0; m < arNModals.size(); ++m)
      {
         const auto& n_modal = arNModals.at(m);
         typename ModalIntegrals<T>::mat_t mat(n_modal, static_cast<T>(0));
         if (m == aElemOperMode)
         {
            if (aElemOperP >= n_modal || aElemOperQ > n_modal)
            {
               MIDASERROR
                  (  "P or Q >= n_modal; P = "+std::to_string(aElemOperP)
                  +  ", Q = "+std::to_string(aElemOperQ)
                  +  ", n_modal = "+std::to_string(n_modal)
                  +  ", mode = "+std::to_string(m)
                  );
            }
            mat[aElemOperP][aElemOperQ] = static_cast<T>(1);
         }
         v_mats.emplace_back(arNOpers.at(m), mat);
      }
      return ModalIntegrals<T>(std::move(v_mats));
   }

   /************************************************************************//**
    * Based on operator, basis and modal information in the Vscf object,
    * constructs primitive basis integrals, then transforms them to the modal
    * basis, puts them in new ModalIntegrals object which is then returned.
    *
    * @param[in,out] arVscf
    *    Vscf object containing operator and (primitive) basis definitions, as
    *    well as coefficients for primitive->modal basis transformation.
    * @param[in] arName
    *    Dummy name to give to the ModalIntegrals<T> object.
    * @return
    *    Newly constructed ModalIntegrals<T> object containing the modal
    *    integral matrices.
    ***************************************************************************/
   template<typename T>
   ModalIntegrals<T> InitFromVscf
      (  const Vscf& arVscf
      ,  const std::string& arName
      )
   {
      ModalIntegrals<T> mi;
      if (arVscf.pOpDef()->GetIsElementaryOperator())
      {
         const auto& opdef = *arVscf.pOpDef();
         const auto& calcdef = *arVscf.pVscfCalcDef();
         Uin m = opdef.GetElemMode();
         Uin p = opdef.GetElemP();
         Uin q = opdef.GetElemQ();
         std::vector<Uin> n_modals;
         std::vector<Uin> n_opers;
         for(LocalModeNr m = 0; m < calcdef.NmodesInModals(); ++m)
         {
            n_opers.push_back(opdef.NrOneModeOpers(m));
            n_modals.push_back(calcdef.Nmodals(m));
         }
         mi = ConstructModalIntegralsElemOper<T>(m, p, q, n_modals, n_opers);
      }
      else
      {
         mi = detail::InitFromVscfImpl<T>(arVscf);
      }
      mi.SetName(arName);
      return mi;
   }

   /************************************************************************//**
    * Calculates the modal overlap matrices; for each mode,
    * \f[
    *    L^\dagger O R
    * \f]
    *
    * @param[in] arLeftModals
    *    Left modals for each mode, modal coefficients along the columns.
    * @param[in] arRightModals
    *    Right modals for each mode, modal coefficients along the columns.
    * @param[in] arPrimOverlaps
    *    The (square) overlap matrices for the primitive basis functions for
    *    each mode.
    * @return
    *    The overlap matrix for the modals.
    ***************************************************************************/
   template<typename T>
   ModalIntegrals<T> InitOverlaps
      (  const std::vector<GeneralMidasMatrix<T>>& arLeftModals
      ,  const std::vector<GeneralMidasMatrix<T>>& arRightModals
      ,  const std::vector<GeneralMidasMatrix<T>>& arPrimOverlaps
      )
   {
      if (  arLeftModals.size() != arRightModals.size()
         || arLeftModals.size() != arPrimOverlaps.size()
         )
      {
         MIDASERROR
            (  "Mode mismatch; arLeftModals.size() = " + std::to_string(arLeftModals.size())
            +  ", arRightModals.size() = " + std::to_string(arRightModals.size())
            +  ", arPrimOverlaps.size() = " + std::to_string(arPrimOverlaps.size())
            );
      }

      const auto& n_modes = arLeftModals.size();
      std::vector<std::vector<typename ModalIntegrals<T>::mat_t>> v;
      v.reserve(n_modes);
      for(Uin m = 0; m < n_modes; ++m)
      {
         if (!arPrimOverlaps.at(m).IsSquare())
         {
            MIDASERROR("Non-square arPrimOverlaps.at("+std::to_string(m)+").");
         }
         const auto& n_modal_l = arLeftModals.at(m).Ncols();
         const auto& n_modal_r = arRightModals.at(m).Ncols();
         const auto& n_bas_l = arLeftModals.at(m).Nrows();
         const auto& n_bas_r = arRightModals.at(m).Nrows();
         const auto& n_bas_prim = arPrimOverlaps.at(m).Nrows();
         if (  n_bas_l != n_bas_prim
            || n_bas_r != n_bas_prim
            )
         {
            MIDASERROR
               (  "Dimension mismatch; m = " + std::to_string(m)
               +  ", arLeftModals.at(m) is "+std::to_string(n_bas_l)+" x "+std::to_string(n_modal_l)
               +  ", arPrimOverlaps.at(m) is "+std::to_string(n_bas_prim)+" x "+std::to_string(n_bas_prim)
               +  ", arRightModals.at(m) is "+std::to_string(n_bas_r)+" x "+std::to_string(n_modal_r)
               );
         }

         typename ModalIntegrals<T>::mat_t mat = arLeftModals.at(m).Conjugate() * arPrimOverlaps.at(m) * arRightModals.at(m);
         v.push_back({});
         v.back().push_back(std::move(mat));
      }
      return ModalIntegrals<T>(std::move(v));
   }


   namespace detail
   {
      /************************************************************************//**
       * Based on operator, basis and modal information in the Vscf object,
       * constructs primitive basis integrals, then transforms them to the modal
       * basis, puts them in new ModalIntegrals object which is then returned.
       *
       * @param[in,out] arVscf
       *    Vscf object containing operator and (primitive) basis definitions, as
       *    well as coefficients for primitive->modal basis transformation.
       * @return
       *    Newly constructed ModalIntegrals<T> object containing the modal
       *    integral matrices.
       ***************************************************************************/
      template<typename T>
      ModalIntegrals<T> InitFromVscfImpl
         (  const Vscf& arVscf
         )
      {
         std::vector<In> offsets(arVscf.pOpDef()->NmodesInOp());
         std::vector<In> nmodals(arVscf.pVscfCalcDef()->NmodesInModals());
         if (offsets.size() != nmodals.size())
         {
            MIDASERROR
               (  "Num. modes mismatch; arVscf.pOpDef()->NmodesInOp() = " + std::to_string(offsets.size())
               +  ", arVscf.pVscfCalcDef()->NmodesInModals() = " + std::to_string(nmodals.size())
               );
         }
         for(Uin m = 0; m < nmodals.size(); ++m)
         {
            offsets.at(m) = arVscf.ModalOffSet(m);
            nmodals.at(m) = arVscf.pVscfCalcDef()->Nmodals(m);
         } 

         midas::td::PrimitiveIntegrals
            <  T
            ,  ModalIntegrals<T>::template vec_templ_t
            ,  ModalIntegrals<T>::template mat_templ_t
            >
         prim_ints(*arVscf.pOneModeInt(), *arVscf.pModals(), offsets, nmodals);
         return ConstructModalIntegrals(std::move(prim_ints));
      }

      /************************************************************************//**
       * 
       ***************************************************************************/
      template<typename T>
      ModalIntegrals<T> ConvertFromReal<T>::Convert
         (  const ModalIntegrals<T>& arModInts
         )
      {
         return arModInts;
      }

      /************************************************************************//**
       * 
       ***************************************************************************/
      template<typename T>
      ModalIntegrals<std::complex<T>> ConvertFromReal<std::complex<T>>::Convert
         (  const ModalIntegrals<T>& arModInts
         )
      {
         using mat_t = typename ModalIntegrals<std::complex<T>>::mat_t;
         std::vector<std::vector<mat_t>> v;
         v.reserve(arModInts.NModes());
         for(LocalModeNr m = 0; m < arModInts.NModes(); ++m)
         {
            v.emplace_back();
            v.back().reserve(arModInts.NOper(m));
            for(LocalOperNr o = 0; o < arModInts.NOper(m); ++o)
            {
               v.back().emplace_back(arModInts.GetIntegrals(m,o));
            }
         }
         return v;
      }

   }  /* namespace detail */
} /* namespace midas::vcc::modalintegrals */


#endif/*MODALINTEGRALSFUNCS_IMPL_H_INCLUDED*/

