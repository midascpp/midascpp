/**
 *******************************************************************************
 * 
 * @file    ModalIntegralsFuncs.cc
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Utility functions related to the ModalIntegrals class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "vcc/ModalIntegralsFuncs.h"

namespace midas::vcc::modalintegrals
{
   /************************************************************************//**
    * Construct modal overlap matrices. Takes some stuff from the Vcc object,
    * then wraps a call to InitOverlaps().
    *
    * @note
    *    For backwards compatibility with Integrals class functionality.
    *
    * @param[in] arVcc
    *    Takes the right-hand modals and primitive overlap integrals from this
    *    one.
    * @param[in] arLeftModals
    *    Left modals for each mode, modal coefficients along the columns.
    * @param[in] arName
    *    Dummy name to give to the ModalIntegrals<T> object.
    * @return
    *    Newly constructed ModalIntegrals<T> object containing the modal
    *    overlap matrices.
    ***************************************************************************/
   ModalIntegrals<Nb> InitOverlapsFromVcc
      (  const Vcc& arVcc
      ,  const DataCont& arLeftModals
      ,  const std::string& arName
      )
   {
      const VccCalcDef& calcdef = *arVcc.pVccCalcDef();
      const auto& opdef = *arVcc.pOpDef();
      const auto& basdef = *arVcc.pBasDef();
      In n_modes = calcdef.NmodesInModals();

      // Gather the matrices.
      std::vector<MidasMatrix> v_l;
      std::vector<MidasMatrix> v_r;
      std::vector<MidasMatrix> v_overlap;
      v_l.reserve(n_modes);
      v_r.reserve(n_modes);
      v_overlap.reserve(n_modes);
      for (LocalModeNr m=I_0; m<n_modes; ++m)
      {
         GlobalModeNr g_mode = opdef.GetGlobalModeNr(m);
         LocalModeNr bas_mode = basdef.GetLocalModeNr(g_mode);
         In n_bas = basdef.Nbas(bas_mode);
         In n_mod = calcdef.Nmodals(m);
         In modal_addr = arVcc.ModalOffSet(m);

         //Get the primitive overlap integrals
         LocalOperNr i_op = opdef.GetUnitOpForMode(m);
         MidasMatrix prim_overlaps(n_bas);
         arVcc.pOneModeInt()->GetInt(m, i_op, prim_overlaps);
         
         MidasMatrix left_modals(n_mod, n_bas);
         arLeftModals.DataIo(IO_GET, left_modals, n_mod*n_bas, n_mod, n_bas, modal_addr);

         MidasMatrix right_modals(n_mod, n_bas);
         arVcc.pModals()->DataIo(IO_GET, right_modals, n_mod*n_bas, n_mod, n_bas, modal_addr);

         v_overlap.push_back(std::move(prim_overlaps));
         v_l.push_back(Transpose(left_modals));
         v_r.push_back(Transpose(right_modals));
      }
      ModalIntegrals<Nb> mi = InitOverlaps<Nb>(v_l, v_r, v_overlap);
      mi.SetName(arName);
      return mi;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   ModalIntegrals<Nb> InitFockFromVcc
      (  Vcc& arVcc
      ,  const std::string& arName
      )
   {
      using U = Nb;
      static_assert
         (  !midas::type_traits::IsComplexV<U>
         ,  "Not tested for complex; check correct transpose/conjugate usage before templating."
         );

      if (gDebug) Mout << " In InitFockFromVcc" << endl; 
      VccCalcDef calcdef = *arVcc.pVccCalcDef();

      const auto* pModals = arVcc.pModals(); // Because we need to transform form primitive -> modal basis.
      const auto* pOpDef = arVcc.pOpDef();   // Because we need the coupling level in the Hamiltonian.
      const auto* pBasDef = arVcc.pBasDef(); // Because we need the size of the primitive basis.

      In n_modes = calcdef.NmodesInModals();
      In hlevel = pOpDef->McLevel();

      // Put integrals in container.
      std::vector<std::vector<GeneralMidasMatrix<U>>> v_mats;
      for (LocalModeNr mode=I_0; mode<n_modes; ++mode)
      {
         GlobalModeNr gmode = pOpDef->GetGlobalModeNr(mode);
         LocalModeNr basmode = pBasDef->GetLocalModeNr(gmode);
         In nmod = calcdef.Nmodals(mode);
         In nbas = pBasDef->Nbas(basmode);
         GeneralMidasMatrix<U> lfock(nbas);   // Large dimension (primitive basis size).
         GeneralMidasMatrix<U> sfock(nmod);   // Small dimension (modal basis size).
        
         // Get modal coefficients. 
         GeneralMidasMatrix<U> modals(nmod, nbas);
         In addr = arVcc.ModalOffSet(mode);
         pModals->DataIo(IO_GET, modals, nbas*nmod, nmod, nbas, addr);
         GeneralMidasMatrix<U> modals_trans = Transpose(modals);
         
         v_mats.emplace_back();
         for (In mcl=I_0; mcl<=hlevel; ++mcl)
         {
            arVcc.ConstructOneModeFock(mode, lfock, true, mcl);
            GeneralMidasMatrix<U> half_trans(nmod,nbas);
            half_trans = modals*lfock;
            sfock = half_trans*modals_trans;

            if (gDebug)
            {
               Mout << "For mode = " << mode << " mcl = " << mcl << ":" << std::endl;
               Mout << "lfock: " << std::endl << lfock << std::endl;
               Mout << "sfock: " << std::endl << sfock << std::endl;
            }
            v_mats.back().push_back(sfock);
         }
      }
      return ModalIntegrals<Nb>(std::move(v_mats));
   }

} /* namespace midas::vcc::modalintegrals */
