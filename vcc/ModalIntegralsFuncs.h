/**
 *******************************************************************************
 * 
 * @file    ModalIntegralsFuncs.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Utility functions related to the ModalIntegrals class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODALINTEGRALSFUNCS_H_INCLUDED
#define MODALINTEGRALSFUNCS_H_INCLUDED

#include "vcc/ModalIntegralsFuncs_Decl.h"
#include "vcc/ModalIntegralsFuncs_Impl.h"

#endif/*MODALINTEGRALSFUNCS_H_INCLUDED*/
