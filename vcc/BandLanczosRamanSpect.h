/**
************************************************************************
* 
* @file                BandLanczosChain.cc
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen (mrgodtliebsen@hotmail.com)
*
* Short Description:   Definitions for Band Lanczos Raman Spectrum module.
* 
* Last modified: Thu Dec 09, 2010  04:24PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BANDLANCZOSRAMANSPECT_H
#define BANDLANCZOSRAMANSPECT_H

#include<string>
#include<fstream>
#include<vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosRspFunc.h"

class VccCalcDef;
class Vcc;

class BandLanczosRamanSpect
{
   protected:
      string                  mName;
      string                  mBlrsp;
      bool                    mAnalyze;
      vector<Nb>              mAnalysisBlocks;
      Nb                      mFreque;

      vector<Nb> mPeaks;
      vector<Nb> mPeaks_inty;

      void AnalyzeSpect(VccCalcDef* aVccCalcDef, Vcc* aVcc,
                        const MidasVector& aInty,
                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep,
                        vector<In>& aOperIndex,
                        vector<string>* aOpers);

      void InitAnalysisBlocksFromMinima(const MidasVector& aInty,
                                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);
      void GenerateGnuPlot();
      void IdentifyPeaks(const MidasVector& aInty, Nb FrqStart, Nb FrqEnd, Nb FrqStep);
      void FindOpers(vector<In>& aOperIndex, vector<string>* aOpers);

   public:
      BandLanczosRamanSpect(const string& aName, const string& aBlrsp, 
                            const string& aAnalyze, Nb aFreque);
      
      void Create(VccCalcDef* aVccCalcDef, Vcc* aVcc);
};

#endif //BANDLANCZOSIRSPECT_H
