/**
************************************************************************
* 
* @file                FvciExpH.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing FvciExpH class methods.
* 
* Last modified: man mar 21, 2005  11:20
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/MultiIndex.h"
#include "vcc/Vcc.h"
#include "vcc/FvciExpH.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/Diag.h"
#include "ni/OneModeInt.h"

/**
* Default "zero" Constructor 
* */
FvciExpH::FvciExpH(In aHamDim,MultiIndex* apMultiIndex, 
      OpDef* apOpDef, BasDef* apBasDef,OneModeInt* apOneModeInt, 
      VccCalcDef* apVccCalcDef) : mH(aHamDim)
{
   // Save info 
   mHamDim  = aHamDim; 
   mpMi     = apMultiIndex;
   mpVccCalcDef = apVccCalcDef;
   //mpVcc    = apVcc;
   mpOpDef  = apOpDef;
   mpBasDef = apBasDef;
   mpOneModeInt = apOneModeInt;
}

/**
* Diagonalize H 
* */
void FvciExpH::Diagonalize()
{
   if (mpVccCalcDef->IoLevel() > 5 || gDebug) Mout << " In FvciExpH::Diagonalize" << endl;
   MidasMatrix eig_vec(mHamDim);
   MidasVector eig_val(mHamDim);
   string diag_method;
   if (mpVccCalcDef->DiagMeth()=="LAPACK" ) 
      diag_method="DSYEVD";
   else if (mpVccCalcDef->DiagMeth()=="MIDAS")
      diag_method="MIDAS_JACOBI";
   else if (mpVccCalcDef->DiagMeth()=="DSYEVD" || 
         mpVccCalcDef->DiagMeth()=="MIDAS_JACOBI") 
      diag_method=mpVccCalcDef->DiagMeth();
   else
   {
      MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override! ");
      diag_method="DSYEVD";
   }
   Diag(mH,eig_vec,eig_val,diag_method,true);
   Mout << " Eigen values " << endl;
   for (In i=0;i<mHamDim;i++) Mout << " FVCI (Prim Basis) Eigval nr. " << setw(5) << i << " " 
      << setw(30) << eig_val[i] << " " << setw(30) << eig_val[i]*C_AUTKAYS << endl;
   if (gDebug && mpVccCalcDef->IoLevel() > 20) 
   {
      Mout << " Eigen vector matrix" << endl;
      Mout << eig_vec << endl;
   }
}
/**
* Construct H matrix
* */
void FvciExpH::Construct() 
{
   if (mpVccCalcDef->IoLevel() > 5 || gDebug) Mout << " In FvciExpH::Construct " << endl;
   In ndim = mpMi->Size();
   vector<In> ivec1(ndim);
   vector<In> ivec2(ndim);
   if (gDebug) Mout << " construct hamiltonian of dim: " << mHamDim << endl;
   for (In idx1=0; idx1<mHamDim;idx1++)
   {
      // get index vector from index.
      mpMi->IvecForIn(ivec1,idx1);
      //if (gDebug && gIoLevel > 10) Mout << "ivec1 idx1 = " << idx1 << endl;
      //if (gDebug && gIoLevel > 10) for (In i=0;i<ndim;i++) Mout << ivec1[i] << endl; 
      for (In idx2=0; idx2<mHamDim;idx2++)
      {
         // get index vector from index.
         mpMi->IvecForIn(ivec2,idx2);
         //if (gDebug && gIoLevel > 10) Mout << "ivec2 idx2 = " << idx2 << endl;
         //if (gDebug && gIoLevel > 10) for (In i=0;i<ndim;i++) Mout << ivec2[i] << endl; 
         // Get occupied integrals and construct matrix element.
         mH[idx1][idx2] = C_0;
         for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
         {
            //if (gDebug && gIoLevel > 10) Mout << " i_term " << i_term << endl; 
            Nb cont = mpOpDef->Coef(i_term);
            for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
            {
               //if (gDebug && gIoLevel > 10) Mout << " i_op_mode " << i_op_mode << endl; 
               if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term))
               {
                  //if (gDebug && gIoLevel > 10) Mout << " is not active " << endl; 
                  if (ivec1[i_op_mode] != ivec2[i_op_mode])
                  {
                     cont = C_0;
                     break;
                  }
               }
               else 
               {
                   //if (gDebug && gIoLevel > 10) Mout << " is ACTIVE " << endl; 
                   LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                   Nb aIntVal;
                   mpOneModeInt->GetInt(i_op_mode,i_oper,
                     ivec1[i_op_mode],ivec2[i_op_mode],aIntVal);
                   cont *= aIntVal;
                   //if (gDebug && gIoLevel > 10) Mout << " i_op_mode " << i_op_mode << " i_oper " << i_oper << endl; 
               }
            }
            //if (gDebug && gIoLevel > 10) Mout << " cont " << cont << endl; 
            mH[idx1][idx2] += cont;
         }
      }
   }
   if (gDebug && gIoLevel > 20) Mout << " hamiltonian : " << endl << mH << endl;
}
