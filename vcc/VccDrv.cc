/**
************************************************************************
* 
* @file                VccDrv.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for MidasCPP Vcc calculations
* 
* Last modified: Wed Mar 24, 2010  02:31PM
* 
* Detailed  Description: 
*
* Driving the MidasCPP Vcc calculation by use of the Vcc class.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/VccCalcDef.h"
#include "util/Io.h"
#include "util/MultiIndex.h"
#include "util/MakeTables.h"
#include "vcc/Vcc.h"
#include "vcc/FvciExpH.h"
#include "vcc/ExpHvci.h"
#include "ni/OneModeInt.h"
#include "util/Plot.h"
#include "util/MidasStream.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mmv/VectorAngle.h"
#include "mpi/FileStream.h"

// using declarations
using std::vector;
using std::string;
using std::map;
using std::vector;
using std::sort;


/**
* Local Declarations
* */
typedef map<string,In>::iterator Msii;
typedef vector<string>::iterator Vsi;

void PrepareSequence(In aI1,In aI2)
{
   string s1 = gVccCalcDef[aI1].GetName();
   string s2 = gVccCalcDef[aI2].GetName();
   Mout << " Copy " << s1 << " data to " << s2 << " if needed " << endl;
   CopyAllFiles(s1,s2);
}

/**
* Run vcc studies 
* */
void VccDrv()
{

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin correlated vibrational structure calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, Mout);
   Mout.setf(ios::showpoint);
  
   // Make a map over oper/basis combinations used in the whole set of vcc calculations.
   map<string,In> oper_bas_map;
   for (In i_calc = gVccCalcDef.size() - I_1; i_calc >= I_0; i_calc--)
   {
      string oper_name = gVccCalcDef[i_calc].Oper();
      string basis_name = gVccCalcDef[i_calc].Basis();
      string oper_bas = oper_name + "_" + basis_name;
      oper_bas_map[oper_bas] = i_calc;
   
      if (gVccCalcDef[i_calc].IoLevel() > I_8) 
      {
         Mout << " Nr. | oper_basis " << std::endl;
         for (Msii po = oper_bas_map.begin(); po != oper_bas_map.end(); po++) 
         {
            Mout << "  " << po->second << "  | ";
            Mout << po->first << std::endl;
         }
      }

      // Check whether analysis directory exists, and if it doesn't create it.
      if(!midas::filesystem::Exists(gVccCalcDef[i_calc].GetmVscfAnalysisDir()))
      {
         if(midas::filesystem::Mkdir(gVccCalcDef[i_calc].GetmVscfAnalysisDir(), false) != 0)
         {
            midas::filesystem::ErrorCheck("Could not create '" + gVccCalcDef[i_calc].GetmVscfAnalysisDir() + "'.");
         }
      }
   }
   In vci_only = I_0;
   In vcc_only = I_0;

   std::vector<MidasVector> v_fvci_vecs_ket;
   std::vector<MidasVector> v_fvci_vecs_bra;
   std::vector<std::string> v_fvci_vec_compare_names;

   for (size_t i_calc = I_0; i_calc < gVccCalcDef.size(); i_calc++)
   {

      string oper_name = gVccCalcDef[i_calc].Oper();
      string basis_name = gVccCalcDef[i_calc].Basis();
      In i_oper=gOperatorDefs.GetOperatorNr(oper_name);
      In i_basis = I_0;
      for (size_t i = I_0; i < gBasis.size(); i++)
      {
         if (gBasis[i].GetmBasName() == basis_name)
         {
            i_basis = i;
         }
      }
      if (i_basis == -I_1 || i_oper == -I_1)
      {
         MIDASERROR("Basis or Operator not found in VccDrv");
      }
      gBasis[i_basis].InitBasis(gOperatorDefs[i_oper]);

      // Check that basis limits have been set! This should have been done already for HO basis.
      if (  gVccCalcDef[i_calc].GetNModals().empty()
         )
      {
         std::vector<In> tmp(gVccCalcDef[i_calc].GetNmodesInOcc()); 
         const auto& opdef = gOperatorDefs[i_oper];
         for(LocalModeNr i_op_mode = I_0; i_op_mode < gVccCalcDef[i_calc].GetNmodesInOcc(); ++i_op_mode) 
         {
            LocalModeNr i_bas_mode = i_op_mode;
            if (  gBasis[i_basis].HasModeMap()
               )
            {
               GlobalModeNr i_g_mode   = opdef.GetGlobalModeNr(i_op_mode);
               i_bas_mode = gBasis[i_basis].GetLocalModeNr(i_g_mode);
            }
            else
            {
               MIDASERROR("gBasis: '" + gBasis[i_basis].GetmBasName() + "' should have mBasModeMap initialized at this point!");
            }

            In nbas = gBasis[i_basis].Nbas(i_bas_mode);
            tmp[i_op_mode] = nbas;
         }
         Mout << " Automatic modal limits = max = " << tmp << endl;
         gVccCalcDef[i_calc].SetModalLimits(tmp);
         gVccCalcDef[i_calc].SetModalLimitsInput(true);
      }
      if (gVccCalcDef[i_calc].IoLevel() > I_10)
      {
         Mout << "\n\n\n Do Vcc:  " << gVccCalcDef[i_calc].GetName() << " calculation \n" << std::endl;
      }
      string oper_bas = oper_name + "_" + basis_name;
      In i_first_calc = oper_bas_map[oper_bas];
      if (gDebug)
      {
         Mout << " i_calc: " << i_calc << " i_first_calc " << i_first_calc << std::endl;
      }
      
      //mbh: if not defined, define here. similar to vscf,
      // have seen to cause problems in conjunction with
      // adga, when left un-initialized.
      if (gVccCalcDef[i_calc].GetNmodesInOcc() == I_0)
      {
         gVccCalcDef[i_calc].ZeroOcc(gOperatorDefs[i_oper].NmodesInOp());
      }

      if (gOperatorDefs[i_oper].NmodesInOp() !=gVccCalcDef[i_calc].GetNmodesInOcc())
      {
         Mout << " Mismatch between number of modes in occupation vector: " << gOperatorDefs[i_oper].NmodesInOp()<< endl;
         Mout << " and nr of modes in operator:  "<< gVccCalcDef[i_calc].GetNmodesInOcc() << endl;
         MIDASERROR(" Mismatch between number of modes in occupation vector and nr of modes in operator" );
      }

      bool outside_basis_space = false;
      for (LocalModeNr i_op_mode=0;i_op_mode< gVccCalcDef[i_calc].GetNmodesInOcc();i_op_mode++)
      {
         In occ = gVccCalcDef[i_calc].GetOccMode(i_op_mode);
         GlobalModeNr i_g_mode  = gOperatorDefs[i_oper].GetGlobalModeNr(i_op_mode);
         LocalModeNr i_bas_mode  = gBasis[i_basis].GetLocalModeNr(i_g_mode);
         In nbas = gBasis[i_basis].Nbas(i_bas_mode);
         if (occ >= nbas)
         {
            outside_basis_space=true;
            continue;
         }
      }
      if (outside_basis_space)
      {
         string s=gVccCalcDef[i_calc].GetName() + " not possible as occupation outside basis ";
         Mout <<  " " << s << endl;
         MidasWarning(s);
         continue;
      }

      // Check that the operator contains both potential and kinetic energy operators, otherwise issue a warning
      const auto& has_keo_terms = gOperatorDefs[i_oper].GetOperWithKeoTerms();
      if (!has_keo_terms)
      {
         std::stringstream keo_terms_warning;
         keo_terms_warning << "No kinetic energy operator terms detected in operator "
                           << oper_name
                           << " for use in VCI/VCC, please investigate";
         MIDASERROR(keo_terms_warning.str());
      }

      //
      bool save = true;
      string storage = "InMem";
      if (gVccCalcDef[i_calc].IntStorage() != I_0)
      {
         storage = "OnDisc";
      }
      OneModeInt one_mode_int(&gOperatorDefs[i_oper], &gBasis[i_basis], storage, save);

      // Output on the operator and other stuff
      if (gVccCalcDef[i_calc].IoLevel() > I_4)
      {
         Mout << "\n\n";
         Mout << " Operator:                            " 
              << one_mode_int.OperName() << std::endl;
         Mout << " Operator-Type:                       " 
              << gOperatorDefs[i_oper].Type() << std::endl; 
         Mout << " Number of terms in operator:         " 
              << gOperatorDefs[i_oper].Nterms() << std::endl; 
         Mout << " Basis:                               " 
              << one_mode_int.BasisName() << std::endl; 
         Mout << " Basis-Type:                          " 
              << gBasis[i_basis].GetmBasSetType() << std::endl; 
         Mout << " Number of modes in vcc:              " 
              << gOperatorDefs[i_oper].NmodesInOp() << std::endl; 
         if (gOperatorDefs[i_oper].NmodesInOp() < I_100)
         {
            Mout << " Occupation vector:                   " 
                 << gVccCalcDef[i_calc].OccupVec() << std::endl; 
         }
         Mout << " One Mode Integral:                   " 
              << one_mode_int.GetmOneModeIntName() << std::endl;
         Mout << " Kinetic energy operator provided:    " 
              << has_keo_terms << std::endl;
         Mout << " Total number of one-mode operators:  " 
              << one_mode_int.GetmTotNoOneModeOpers() << std::endl;
         Mout << " Number of one mode integral:         " 
              << one_mode_int.GetmTotNoOneModeIntegrals() << std::endl;
         Mout << " IntStorage mode for vcc:             " 
              << gVccCalcDef[i_calc].IntStorage() << std::endl;
         Mout << " VecStorage mode for vcc:             " 
              << gVccCalcDef[i_calc].VecStorage() << std::endl << std::endl;
      }

      //
      bool restart_ok = false;
      if (i_calc > i_first_calc)
      {
         restart_ok = one_mode_int.RestartInt();
      }
      if (!restart_ok)
      {
         one_mode_int.CalcInt();
      }
    
      // Check that the analysis directory is set correctly
      if (gVccCalcDef[i_calc].GetmVscfAnalysisDir().empty()) //Emil, (19/02/2019): This is a dirty hack but I see no other way around when we need to do an "implicit" VSCf calculation for every VCI/VCC calculation.
      {
         std::string vscf_analysis_dir;
         for (auto& vscf_calc_def : gVscfCalcDef)
         {
            vscf_analysis_dir = vscf_calc_def.GetmVscfAnalysisDir();
            if ( !gVccCalcDef[i_calc].GetmVscfAnalysisDir().empty() 
               && gVccCalcDef[i_calc].GetmVscfAnalysisDir() != vscf_analysis_dir
               )
            {
               MIDASERROR("VSCF calculations are using different analysis directories, cannot figure which is to be used for VCC calculation");
            }
            else if (gVccCalcDef[i_calc].GetmVscfAnalysisDir() == vscf_analysis_dir)
            {
               continue;
            }
            else
            {
               gVccCalcDef[i_calc].SetmVscfAnalysisDir(vscf_analysis_dir);
            }
         }

         if (vscf_analysis_dir != gAnalysisDir)
         {
            Mout << " Setting the analysis directory for correlated vibrational structure calculation to: " << std::endl << "  " << vscf_analysis_dir << std::endl; 
         }
      }

      //gOperatorDefs[i_oper].InitOpRange(); // ian: i have commented this out as it makes no sense
      Vcc vcc(&gVccCalcDef[i_calc], &gOperatorDefs[i_oper], &gBasis[i_basis], &one_mode_int);
      vcc.StartGuess();
      vcc.Solve(); // solve VSCF equations first
      if (vcc.Converged())
      {
         Mout << " Vcc " << left << setw(25) << vcc.Name() << " converged!  E = " << vcc.GetEtot() << " in N_it = " << vcc.GetIter() << std::endl;
      }
      else
      {
         MIDASERROR(" VSCF not converged for vib correlation calculation "); 
      } 
 
      // Loop for iterative determination of natural modals.
      // In other cases the loop is defaultet to one round... 
      In n_it_mod = I_1;
      if (gVccCalcDef[i_calc].DoItNaMo()) 
      { 
         n_it_mod = gVccCalcDef[i_calc].GetItNaMoMaxIter(); 
         vcc.SetItNaMoConv(false); 
      }

      bool conv_or_max = false; 
      //for (In i_it_mod=I_1;(i_it_mod <=n_it_mod && !vcc.ItNaMoConv()) ;i_it_mod++) // loop until converged or max it. 
      for (In i_it_mod = I_1; !conv_or_max; i_it_mod++) // loop until converged or max it. 
      {
         if (gVccCalcDef[i_calc].DoItNaMo())
         {
            Out72Char(Mout,'$','$','$');
            Mout << "\n Iterative natural modals determination: Iteration " << i_it_mod << " of at most " << n_it_mod << std::endl; 
            //if (i_it_mod>I_1) 
            Mout << " I recalculate reference energy\n" << std::endl; 
            Out72Char(Mout,'$','$','$');
         }

         if (  gVccCalcDef[i_calc].Save() 
            || gVccCalcDef[i_calc].DoItNaMo() 
            || gVccCalcDef[i_calc].IoLevel() > I_2
            )
         {
            vcc.Finalize(i_it_mod <= I_1);
         }
         vcc.UpdateIntegrals();
         gVccCalcDef[i_calc].SetDone(vcc.GetEtot());
         vcc.UpdateEvmp0();

         // Prepare Target spaces etc. from input info
         if (  (gVccCalcDef[i_calc].AllFundamentals())
            || (gVccCalcDef[i_calc].AllFirstOvertones())
            || (gVccCalcDef[i_calc].Fundamentals())
            || (gVccCalcDef[i_calc].FirstOvertones())
            || (gVccCalcDef[i_calc].Reference())
            )
         {
            vcc.AutoTarget();
         }
         // Vmp 
         if (gVccCalcDef[i_calc].Vmp())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin VMP calc. ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
            vcc.DoVmp(); 
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VMP calculation done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
         // Vapt
         if (gVccCalcDef[i_calc].Vapt())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin VAPT calc. ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
            vcc.DoVapt(); 
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VAPT calculation done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
         // Direct Vci without construction of Hamiltonian 
         if (gVccCalcDef[i_calc].Vci())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin VCI calc. ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
   
            vcc.SetMethod(VCC_METHOD_VCI);
   
            if (gVccCalcDef[i_calc].CmpV3OrigVcc())
            {
               vcc.CompareTransformers(false);
               MIDASERROR(" Comparison of VCI transformers completed.");
            }
   
            vcc.DoVci();
            if (gVccCalcDef[i_calc].DoRsp()) 
            {
               vcc.PrepareRspTransformer();
               vcc.CheckVciRefForRsp(); 
               vcc.SetRspRefE(gVccCalcDef[i_calc].GetEfinal()); 
               if (gVccCalcDef[i_calc].RspDiagMeth()=="LAPACK" )
                  gVccCalcDef[i_calc].SetRspDiagMeth("DSYEVD");
               else if (gVccCalcDef[i_calc].RspDiagMeth()=="MIDAS" )
                  gVccCalcDef[i_calc].SetRspDiagMeth("MIDAS_JACOBI");
               else if (gVccCalcDef[i_calc].RspDiagMeth()=="DSYEVD" ||
                     gVccCalcDef[i_calc].RspDiagMeth()=="MIDAS_JACOBI")
                  gVccCalcDef[i_calc].SetRspDiagMeth(gVccCalcDef[i_calc].RspDiagMeth());
               else
               {
                  MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth for Vci Rsp. Override! ");
                  gVccCalcDef[i_calc].SetRspDiagMeth("DSYEVD");
               }
               if (gVccCalcDef[i_calc].GetRspLevel2Solver()=="MIDAS")
                  gVccCalcDef[i_calc].SetRspLevel2Solver("MIDAS_CG");
               else if (gVccCalcDef[i_calc].GetRspLevel2Solver()=="LAPACK") 
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DSYSV");
               else if (gVccCalcDef[i_calc].GetRspLevel2Solver()=="DSYSV" || 
               gVccCalcDef[i_calc].GetRspLevel2Solver()=="MIDAS_CG" || 
               gVccCalcDef[i_calc].GetRspLevel2Solver()=="SVD") 
                  gVccCalcDef[i_calc].SetRspLevel2Solver(gVccCalcDef[i_calc].GetRspLevel2Solver());
               else
               {
                  MidasWarning("Only DSYSV (LAPACK), MIDAS_CG (MIDAS), or SVD are allowed Level2Solver. Override! ");
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DSYSV");
               }
               if (gVccCalcDef[i_calc].GetRspHarmonicRR())
               {
                  gVccCalcDef[i_calc].SetRspDiagMeth("GGEV");
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DSYSV");
               }
               vcc.RspDrv();
               vcc.DeleteRspTransformer();
            }
   
            if (gVccCalcDef[i_calc].FcVciCalc()) vcc.FcCalc();
            if (gVccCalcDef[i_calc].FranckCondon()) vcc.FranckCondon();
   
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VCI calculation done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
         // Direct VciH2
         if (gVccCalcDef[i_calc].VciH2()||gVccCalcDef[i_calc].H2Start())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin VCIH2 calc. ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
   
            vcc.DoVciH2();
   
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VCIH2 calculation done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
         // Direct Vcc 
         if (gVccCalcDef[i_calc].Vcc())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin VCC calc. ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
              
            vcc.SetMethod(VCC_METHOD_VCC);
            
            if (gVccCalcDef[i_calc].CmpV3OrigVcc())
            {
               vcc.CompareTransformers(true);
               MIDASERROR(" Comparison of VCC transformers completed.");
            }
   
            if (gVccCalcDef[i_calc].CheckV3Left())
            {
               vcc.CheckV3Left();
               MIDASERROR(" Check of V3 left-hand transformer completed.");
            }
           
            vcc.DoVcc();
            if (gVccCalcDef[i_calc].DoRsp()) 
            {
               gVccCalcDef[i_calc].InRspPart() = true; // << HACK !!
               if (C_0 != gVccCalcDef[i_calc].GetNumVccJacobian())
               {
                  vcc.CalcNumVccJacobian();
               }
               
               vcc.SetRspRefE(gVccCalcDef[i_calc].GetEfinal()); 
               vcc.SetUseVccRspRefE(gVccCalcDef[i_calc].GetRspUseVccRefEnergy());
               vcc.PrepareRspTransformer();
               vcc.MakeRspFuncsNonVar(); 

               // If we are using the tensor solver, we need to set a different file name for the converged amplitudes!
               // This is needed when the transformer SetType() function is called later.
               if (  gVccCalcDef[i_calc].UseTensorNlSolver()
                  )
               {
                  MidasAssert(vcc.pVccCalcDef()->V3trans(), "If we are using TensorNlSolver, we must have a V3 transformer...");
                  vcc.SetRspTransformerAmplitudeFile(gVccCalcDef[i_calc].GetName() + "_Vcc_vec_tensor_0_0");
               }
               
               if (gVccCalcDef[i_calc].RspDiagMeth()=="LAPACK"  ||
                   gVccCalcDef[i_calc].RspDiagMeth()=="MIDAS")
                  gVccCalcDef[i_calc].SetRspDiagMeth("DGEEVX");
               else if (gVccCalcDef[i_calc].RspDiagMeth()=="DGEEVX" ||
                        gVccCalcDef[i_calc].RspDiagMeth()=="DGEEV")
                  gVccCalcDef[i_calc].SetRspDiagMeth(gVccCalcDef[i_calc].RspDiagMeth());
               else
               {
                  MidasWarning("Only DGEEV or DGEEVX (or LAPACK) allowed."
                                      "DiagMeth for Vcc Response. Override!");
                  gVccCalcDef[i_calc].SetRspDiagMeth("DGEEVX");
               }
               if (gVccCalcDef[i_calc].GetRspLevel2Solver()=="MIDAS" ||
                   gVccCalcDef[i_calc].GetRspLevel2Solver()=="LAPACK")
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DGESV");
               else if (gVccCalcDef[i_calc].GetRspLevel2Solver()=="DGESV" || 
                        gVccCalcDef[i_calc].GetRspLevel2Solver()=="SVD") 
                  gVccCalcDef[i_calc].SetRspLevel2Solver(gVccCalcDef[i_calc].GetRspLevel2Solver());
               else
               {
                  MidasWarning("Only DGESV (LAPACK or MIDAS), or SVD are allowed Level2Solver. "
                                       "Override!");
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DGESV");
               }
               if (gVccCalcDef[i_calc].GetRspHarmonicRR())
               {
                  gVccCalcDef[i_calc].SetRspDiagMeth("DGGEV");
                  gVccCalcDef[i_calc].SetRspLevel2Solver("DGESV");
               }
               vcc.RspDrv();
               vcc.DeleteRspTransformer();
               gVccCalcDef[i_calc].InRspPart() = false; // >> HACK !!
            }
            
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VCC calculation done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
         //
         // Vci with explicit construction of Hamiltonian up to a excitation level 
         if (gVccCalcDef[i_calc].ExpHvci())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VCI calculation with explicit H ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
            vcc.DoExpHvci(); 
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," VCI calculation with explicit H is done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }
   
         if (gVccCalcDef[i_calc].FvciExpH())
         {
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin FVCI calc. with explicit H/primitive basis ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
            vector<In> ref;
            vector<In> inc;
            In hdim = I_1;
            for (LocalModeNr i_op_mode=0;i_op_mode<gVccCalcDef[i_calc].GetNmodesInOcc();i_op_mode++)
            {
               GlobalModeNr i_g_mode   = gOperatorDefs[i_oper].GetGlobalModeNr(i_op_mode);
               LocalModeNr i_bas_mode = gBasis[i_basis].GetLocalModeNr(i_g_mode);
               In nbas       = gBasis[i_basis].Nbas(i_bas_mode);
               In low        = gBasis[i_basis].Lower(i_bas_mode);
               In high       = gBasis[i_basis].Higher(i_bas_mode);
               ref.push_back(low);
               inc.push_back(high);
               hdim *= nbas;
            }
            MultiIndex cispace(ref,inc);
            FvciExpH fvci(hdim,&cispace,&gOperatorDefs[i_oper],&gBasis[i_basis],
                  &one_mode_int,&gVccCalcDef[i_calc]);
            fvci.Construct();
            fvci.Diagonalize();
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," FVCI calc. with explicit H/primitive basis done ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }

         if (gVccCalcDef[i_calc].UsingSubspaceSolver())
         {
            auto cd = gVccCalcDef[i_calc];
            std::string s_info = midas::tdvcc::StringFromEnum(cd.SubspaceSolverCorrType())+" (MatRep) calculation";
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin "+s_info,'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
            vcc.DoSubspaceSolverCalc();
            Mout << endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," "+s_info+" done",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << endl;
         }

         //gOperatorDefs[i_oper].DeleteOpRange(); // ian: i have commented this out as it makes no sense
         if (gVccCalcDef[i_calc].Sequence() && i_calc < gVccCalcDef.size() - 1)
         {
            PrepareSequence(i_calc,i_calc+I_1);
         }
         if (gVccCalcDef[i_calc].Vci() && !gVccCalcDef[i_calc].Vcc())  vci_only++;
         if (!gVccCalcDef[i_calc].Vci() && gVccCalcDef[i_calc].Vcc())  vcc_only++;
   
         if (i_it_mod >=n_it_mod) conv_or_max = true; 
         if (vcc.ItNaMoConv()) conv_or_max = true; 
      }

      if (  vcc.SavedFvciVecKet().Size() != 0
         && vcc.SavedFvciVecBra().Size() != 0
         )
      {
         v_fvci_vec_compare_names.emplace_back(gVccCalcDef[i_calc].GetName());
         v_fvci_vecs_ket.emplace_back(vcc.SavedFvciVecKet());
         v_fvci_vecs_bra.emplace_back(vcc.SavedFvciVecBra());
      }
   }


   // Sort the gVccCalcDef calculations according to oper/basis/name/increasing energy.
   sort(gVccCalcDef.begin(),gVccCalcDef.end(),VscfCalcDefLess);

   // Output the vcc summary 
   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," Summary of Vcc state optimizations ",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   Mout << "\n\n";
   Mout << " Calculations ordered after operator/basis/increasing energy: " << endl;
   Mout << " +----------------------------------------------------------+ " << endl;


   In n_units = 3;

   for (In i_units=0;i_units<n_units;i_units++)
   {
      Nb conv = C_0;
      if (i_units == 0) 
      {
         Mout << "\n\n Units is direct accordance with input " << endl;
         conv = C_1;
      }
      if (i_units == 1) 
      {
         conv = C_AUTKAYS;
         Mout << "\n\n Units is converted with " << conv << " corresponding to au to cm-1 " << endl;
      }
      if (i_units == 2) 
      {
         conv = C_AUTEV;  
         Mout << "\n\n Units is converted with " << conv << " corresponding to au to eV " << endl;
      }

      Mout << "\n\n";
      Mout << " Calculations ordered after operator/basis/name/increasing energy: " << endl;
      Mout << " +-----------------------------------------------------------------+ " << endl;
      In i_first_cc_calc = -I_1;
      for (size_t i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
      {
         if (gVccCalcDef[i_calc].Done())
// && (gVccCalcDef[i_calc].Vcc()||gVccCalcDef[i_calc].VciH2()))
         {
            if (i_first_cc_calc <I_0) i_first_cc_calc = i_calc;
            if (gVccCalcDef[i_calc].Vcc())        Mout << " ss-VCC   ";
            else if (gVccCalcDef[i_calc].VciH2()) Mout << " ss-VCIH2 ";
            else if (gVccCalcDef[i_calc].Vci()) Mout << " ss-VCI ";
            else if (gVccCalcDef[i_calc].Vmp()) Mout << " ss-VMP ";
            else if (gVccCalcDef[i_calc].Vapt()) Mout << " ss-VAPT ";
            else if (gVccCalcDef[i_calc].UsingSubspaceSolver())
            {
               Mout << " ss-" << midas::tdvcc::StringFromEnum(gVccCalcDef[i_calc].SubspaceSolverCorrType()) << "(MR) ";
            }
            Mout << left << setw(24) << gVccCalcDef[i_calc].GetName();
            Mout << " E = " << right << setw(23) << gVccCalcDef[i_calc].GetEfinal()*conv;
            Mout << " Occ: [";
            for (In i_op_mode=0;i_op_mode< gVccCalcDef[i_calc].GetNmodesInOcc()-1;i_op_mode++)
               Mout << gVccCalcDef[i_calc].GetOccMode(i_op_mode) << ",";
            Mout << gVccCalcDef[i_calc].GetOccMode(gVccCalcDef[i_calc].GetNmodesInOcc()-1) << "]" << endl;
         }
      }
      Mout << "\n\n";
      Mout << " Excitation energies relative to the lowest state: " << endl;
      Mout << " +-----------------------------------------------+ " << endl;
      for (size_t i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
      {
         if (gVccCalcDef[i_calc].Done() && 
               (gVccCalcDef[i_calc].Vcc()||gVccCalcDef[i_calc].VciH2()||gVccCalcDef[i_calc].UsingSubspaceSolver()))
         {
            if (gVccCalcDef[i_calc].Vcc()) Mout << " ss-VCC   ";
            else if (gVccCalcDef[i_calc].VciH2()) Mout << " ss-VCIH2 ";
            else if (gVccCalcDef[i_calc].Vci()) Mout << " ss-VCI ";
            else if (gVccCalcDef[i_calc].Vmp()) Mout << " ss-VMP ";
            else if (gVccCalcDef[i_calc].Vapt()) Mout << " ss-VAPT ";
            else if (gVccCalcDef[i_calc].UsingSubspaceSolver())
            {
               Mout << " ss-" << midas::tdvcc::StringFromEnum(gVccCalcDef[i_calc].SubspaceSolverCorrType()) << "(MR) ";
            }
            //else Mout << "     ";
            Mout << left << setw(24) << gVccCalcDef[i_calc].GetName();
            Mout << " E = " << right << setw(23) << 
               (gVccCalcDef[i_calc].GetEfinal()-gVccCalcDef[i_first_cc_calc].GetEfinal())*conv;
            Mout  << " Occ: [";
            for (In i_op_mode=0;i_op_mode< gVccCalcDef[i_calc].GetNmodesInOcc()-1;i_op_mode++)
               Mout << gVccCalcDef[i_calc].GetOccMode(i_op_mode) << ",";
            Mout << gVccCalcDef[i_calc].GetOccMode(gVccCalcDef[i_calc].GetNmodesInOcc()-1) << "]" << endl;
         }
      }
   }

   // If asked for FVCI analysis (matrep and VCI), cross-compare FVCI vecs from
   // different calcs. now.
   std::map<std::string, decltype(v_fvci_vecs_ket)*> map_v_fvci_vecs =
      {  {  "ket", &v_fvci_vecs_ket}
      ,  {  "bra", &v_fvci_vecs_bra}
      };

   for(const auto& [s, pvec]: map_v_fvci_vecs)
   {
      if (pvec->size() > 2)
      {
         Mout << '\n';
         Mout << "Cross-comparing FVCI vector " << s << "s:\n"
              << "ref: " << v_fvci_vec_compare_names.at(0) << '\n'
              << std::flush;
         const auto& ref = pvec->at(0);
         for(Uin i = 1; i < pvec->size(); ++i)
         {
            const auto& v = pvec->at(i);
            Mout
               << "   FVCI-" << s << ", angle(ref,v) = " << VectorAngle(ref,v)
               << " rad    (name = " << v_fvci_vec_compare_names.at(i) << ")"
               << std::endl;
         }
      }
   }

   // 
   //    // Construct info for calculation of temperature effects and populations.
   //     
   {
      string meth_flag="unknown";
      if (vci_only) {meth_flag="Vci_ss";}
      else if (vcc_only) {meth_flag="Vcc_ss";}
      vector<VscfCalcDef*> help(gVccCalcDef.size());
      for (In i=I_0;i<help.size();i++) help[i]=&gVccCalcDef[i];
      ExtractPropsAndSaveOnFiles(help,meth_flag);
   }

   // Summarize vmp calculations if there is any

   // Eduard: As it is right now is a bit weird for Vapt, but should do the work
   // else copy-paste any_mp to any_vapt or make any_mp general enough
 
   bool any_mp = false;
   for (size_t i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
      if (gVccCalcDef[i_calc].Vmp() || gVccCalcDef[i_calc].Vapt()) any_mp = true;

   if (any_mp)
   {
      Mout << "\n Summary of vmp calculations is being made and stored in " << endl;
      Mout << " in the Vmp_res.tex file and in the gnu plot files " << endl;

      MidasVector* mpes   = new MidasVector [gVccCalcDef.size()]; // At most so many vmp calculations.
      MidasVector* mpac   = new MidasVector [gVccCalcDef.size()]; // At most so many vmp calculations.
      vector<In>*  p_ivec = new vector<In> [gVccCalcDef.size()];
      string*      mpname = new string [gVccCalcDef.size()]; // At most so many vmp calculations.
      MidasVector eref(gVccCalcDef.size(),C_0);
   
      // readin vmp results and count number of vmp calcs.
      In nvmp = I_0;
      //string basis;
      //string oper;
      //vector<In> occ_vec;
      for (size_t i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
      {
         // mbh: is there supposed to be a || Vapt() here?
         if (gVccCalcDef[i_calc].Vmp() || gVccCalcDef[i_calc].Vapt())
         {
            In n_order = gVccCalcDef[i_calc].VmpMaxOrder();
            In n_e = I_2*n_order+I_2;
            mpes[nvmp].SetNewSize(n_e,false);
            mpac[nvmp].SetNewSize(n_e,false);
            mpes[nvmp].Zero();
            mpac[nvmp].Zero();
            string name = gVccCalcDef[i_calc].GetName() + "_vmp_res";
            mpname[nvmp] = gVccCalcDef[i_calc].GetName() + " [";
            for (In i_op_mode=0;i_op_mode< gVccCalcDef[i_calc].GetNmodesInOcc()-1;i_op_mode++)
               mpname[nvmp] = mpname[nvmp] + std::to_string(gVccCalcDef[i_calc].GetOccMode(i_op_mode)) + ",";
            mpname[nvmp] = mpname[nvmp] + std::to_string(gVccCalcDef[i_calc].GetOccMode(gVccCalcDef[i_calc].GetNmodesInOcc()-I_1)) + "]";
            ifstream mpin(name.c_str());
            string s;
            while (getline(mpin,s))
            {
               istringstream s_in(s);
               In j;
               s_in >> j;
               s_in >> mpes[nvmp][j];
               s_in >> mpac[nvmp][j];
            }
            // incr. counter
            p_ivec[nvmp] = gVccCalcDef[i_calc].OccupVec();
   
            //basis = gVccCalcDef[i_calc].Basis();
            //oper  = gVccCalcDef[i_calc].Oper();
            //occ_vec = gVccCalcDef[i_calc].OccupVec();
   
            /*
            for (In i_calc2 =0;i_calc2<gVccCalcDef.size();i_calc2++)
            {
               if (gVccCalcDef[i_calc2].Vcc() 
                     && gVccCalcDef[i_calc2].MaxExciLevel() == gVccCalcDef[i_calc2].NmodesInModals()
                     && gVccCalcDef[i_calc2].Basis() == basis
                     && gVccCalcDef[i_calc2].Oper()  == oper)
               {
                  vector<In> occ_vec2 = gVccCalcDef[i_calc2].OccupVec();
                  ModeCombi m1(occ_vec,-I_1);
                  ModeCombi m2(occ_vec,-I_1);
                  if (m1==m2) 
                  {
                     eref[nvmp] = gVccCalcDef[i_calc2].GetEfinal();
                  }
               }
            }
            */
            if (gVccCalcDef[i_calc].Vcc()||gVccCalcDef[i_calc].VciH2())
            {
               eref[nvmp] = gVccCalcDef[i_calc].GetEfinal();
            }
            else
            {
               eref[nvmp] = C_10_10;
            }
   
            nvmp++;
         }
      }
   
      string name = gAnalysisDir+"/Vmp_res.tex";
      midas::mpi::OFileStream mpout(name);

      TexTableBeginDocument(mpout);
      mpout.unsetf(ios::uppercase);
      mpout.unsetf(ios::scientific);
      mpout.setf(ios_base::fixed);
      mpout.setf(ios::showpoint);
      In i_rep3 = -I_1;
      In n_real = 10;
      In n_rep = (4*n_real);
      In i_rep32 = max(n_rep/I_2,I_1);
      while (nvmp > I_0 && ++i_rep3 < n_rep)
      {
         In i_rep2 = i_rep3%(i_rep32);
         bool sci = (i_rep3 > i_rep32);
         In i_rep = i_rep2/I_2;
         bool cm = (i_rep2%I_2 == I_1);
         Nb conv = C_1;
         In ndigits = i_rep + 4;
         //Mout << " ndigits " << ndigits << " sci = " << sci << " i_rep32 " << i_rep32 << " i_rep3 " << i_rep3 << endl;
         if (cm) ndigits -= 4; 
         mpout.precision(ndigits);
         if (cm) conv = C_AUTKAYS;
         if (sci)
         {
            mpout.unsetf(ios_base::fixed);
            mpout.setf(ios::uppercase);
            mpout.setf(ios::scientific);
         }
      
   
         In n_2np1_max = -I_1;
         for (In ivmp=I_0;ivmp<nvmp;ivmp++)
         {
            n_2np1_max = std::max(n_2np1_max, static_cast<In>(mpes[ivmp].Size()));
         }
      
         string vmpst = " VMP energies in different orders for various vibrational states ";
         vmpst = vmpst + " with " + std::to_string(ndigits) + " digits ";
         if (cm) vmpst = vmpst + " with au to cm-1 conversion ";
         TexTableBegin(mpout,vmpst,nvmp+I_2);
         mpout << " Order &";
         for (In ivmp=I_0;ivmp<nvmp;ivmp++) 
         {
            mpout << "& [";
            In n = p_ivec[ivmp].size();
            for (In j=0;j<n-I_1;j++) mpout << p_ivec[ivmp][j] << ",";
            if (n>I_0) mpout << p_ivec[ivmp][n-I_1] << "]";
         }
         mpout << " \\\\ \\hline " << endl;
   
         mpout << "\\multicolumn{" << nvmp+I_2 << "}{l} {";
         mpout << " Energy contributions in different orders} \\\\" << endl;
         for (In i=I_0;i<n_2np1_max;i++)
         {
            mpout << "VMP" << left << i << " &";
            //mpout.setf(ios_base::showpos);
            for (In ivmp=I_0;ivmp<nvmp;ivmp++)
            {
               mpout << "& " << setw(12) << right;
               if (i<mpes[ivmp].Size()) 
               {
                  mpout << mpes[ivmp][i]*conv;
               }
               else
               {
                  mpout << " ";
               }
   
            }
            //mpout.unsetf(ios_base::showpos);
            mpout << " \\\\ " << endl;
         }
      
         mpout << "\\multicolumn{" << nvmp+I_2 << "}{l} {";
         mpout << " Accumulated energies to different orders} \\\\" << endl;
         for (In i=I_0;i<n_2np1_max;i++)
         {
            mpout << "VMP" << left << i << " &";
            for (In ivmp=I_0;ivmp<nvmp;ivmp++)
            {
               mpout << "& " << setw(12) << right;
               if (i<mpac[ivmp].Size()) 
               {
                  mpout << mpac[ivmp][i]*conv;
               }
               else
               {
                  mpout << " ";
               }
            }
            mpout << " \\\\ " << endl;
         }
         mpout << "\\multicolumn{" << nvmp+I_2 << "}{l} {";
         mpout << " Difference compared to VCC energy in different orders} \\\\" << endl;
         // find true erer 
         for (In i=I_0;i<n_2np1_max;i++)
         {
            mpout << "VMP" << left << i << " &";
            for (In ivmp=I_0;ivmp<nvmp;ivmp++)
            {
               mpout << "& " << setw(12) << right;
               if (i<mpac[ivmp].Size()) 
               {
                  mpout << (mpac[ivmp][i]-eref[ivmp])*conv;
               }
               else
               {
                  mpout << " ";
               }
            }
            mpout << " \\\\ " << endl;
         }
         mpout << "\\multicolumn{" << nvmp+I_2 << "}{l} {";
         mpout << " VCC energy ? is FVCI } \\\\" << endl;
         mpout << setw(5)  << left << "VCC  &";
         for (In ivmp=I_0;ivmp<nvmp;ivmp++)
         {
            mpout << "& " << setw(12) << right << eref[ivmp]*conv;
         }
         mpout << " \\\\ " << endl;
         vmpst = " ";
         TexTableEnd(mpout,vmpst);
   
         mpout << "\n\n\n";
   
         // Make figures
         // MakePsFig doesn't work anymore.
         /*
         for (In ivmp=I_0;ivmp<nvmp;ivmp++)
         {
            string psfig = mpname[ivmp] + "_vmp_err";
            MidasVector err(mpac[ivmp]);
            for (In i=I_0;i<err.Size();i++) err[i]-=eref[ivmp];
            MakePsFig(psfig,err,err.Size());
         }
         for (In ivmp=I_0;ivmp<nvmp;ivmp++)
         {
            string psfig = mpname[ivmp] + "_vmp_cont";
            MakePsFig(psfig,mpes[ivmp],mpes[ivmp].Size());
         }
         */
      }
      In i_rep = -I_1;
      while (i_rep++ < I_1)
      {
         In* nd = new In [nvmp];
         for (In ivmp=I_0;ivmp<nvmp;ivmp++) nd[ivmp]=mpes[ivmp].Size();
         MidasVector* merrv = new MidasVector [nvmp];
         for (In ivmp=I_0;ivmp<nvmp;ivmp++)
         {
            merrv[ivmp].SetNewSize(nd[ivmp]);
            for (In i=I_0;i<merrv[ivmp].Size();i++) 
               merrv[ivmp][i]=(mpac[ivmp][i]-eref[ivmp]);
            for (In i=I_0;i<merrv[ivmp].Size();i++) 
               if (fabs(merrv[ivmp][i]) <= C_NB_MIN) merrv[ivmp][i]=C_NB_MIN;
         }
   
         Nb conv = C_1;
         if (i_rep==I_1) conv = C_AUTKAYS;
         if (i_rep == I_0|| i_rep==I_1)
         {
            string psfig = gAnalysisDir+"/VmpTotEs";
            if (i_rep==I_1) psfig = psfig + "_cm";
            Mout << " multiplot " << psfig << endl;
            MakeMultiPlotPsAutoX(psfig, "", "Energy", mpname, I_0, mpac, nvmp,
                                 false, false, C_1, conv);
            psfig = gAnalysisDir+"/VmpContEs";
            if (i_rep==I_1) psfig = psfig + "_cm";
            Mout << " multiplot " << psfig << endl;
            MakeMultiPlotPsAutoX(psfig, "", "Energy", mpname, I_0, mpes, nvmp,
                                 false, false, C_1, conv);
            psfig = gAnalysisDir+"/VmpLogContEs";
            if (i_rep==I_1) psfig = psfig + "_cm";
            Mout << " multiplot " << psfig << endl;
            MakeMultiPlotPsAutoX(psfig, "", "Energy", mpname, I_0, mpes, nvmp,
                                 false, true, C_1, conv);
            psfig = gAnalysisDir+"/VmpErrEs";
            if (i_rep==I_1) psfig = psfig + "_cm";
            Mout << " multiplot " << psfig << endl;
            MakeMultiPlotPsAutoX(psfig, "", "Energy", mpname, I_0, merrv,nvmp,
                                 false, false, C_1, conv);
            psfig = gAnalysisDir+"/VmpLogErrEs";
            if (i_rep==I_1) psfig = psfig + "_cm";
            Mout << " multiplot " << psfig << endl;
            MakeMultiPlotPsAutoX(psfig, "", "Energy", mpname, I_0, merrv,nvmp,
                                 false, true, C_1, conv);
       
            /*
            for (In ivmp=I_0;ivmp<nvmp;ivmp++) nd[ivmp]=mpes[ivmp].Size()-I_1;
            psfig = "VmpTotEs_f1";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,mpac,nvmp,nd,I_1,I_1,false,false,C_1,conv);
            psfig = "VmpContEs_f1";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,mpes,nvmp,nd,I_1,I_1,false,false,C_1,conv);
            psfig = "VmpErrEs_f1";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,merrv,nvmp,nd,I_1,I_1,false,false,C_1,conv);
            psfig = "VmpLogErrEs_f1";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,merrv,nvmp,nd,I_1,I_1,false,true,C_1,conv);
      
            for (In ivmp=I_0;ivmp<nvmp;ivmp++) nd[ivmp]=mpes[ivmp].Size()-I_2;
            psfig = "VmpTotEs_f2";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,mpac,nvmp,nd,I_2,I_2,false,false,C_1,conv);
            psfig = "VmpContEs_f2";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,mpes,nvmp,nd,I_2,I_2,false,false,C_1,conv);
            psfig = "VmpErrEs_f2";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,merrv,nvmp,nd,I_2,I_2,false,false,C_1,conv);
            psfig = "VmpLogErrEs_f2";
            if (i_rep==I_1) psfig = psfig + "_cm";
            MakePsFig(psfig,mpname,merrv,nvmp,nd,I_2,I_2,false,true,C_1,conv);
            */
   
            delete[] nd; 
            delete[] merrv; 
         }
      }
      TexTableEndDocument(mpout);
      delete[] mpes;
      delete[] mpac;
      delete[] p_ivec;
      delete[] mpname;
   }
   
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Correlated calculations done ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
}

namespace midas
{
namespace vcc
{

/**
 * Namespace wrapper for vcc driver. Will call the old one in global namespace.
 **/
void VccDrv()
{
   ::VccDrv();
}

} /* namespace vcc */
} /* namespace midas */
