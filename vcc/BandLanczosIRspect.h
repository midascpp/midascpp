/**
************************************************************************
* 
* @file                BandLanczosIRspect.h
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen (mrgodtliebsen@hotmail.com)
*
* Short Description:   Definitions for Band Lanczos IR spectrum module.
* 
* Last modified: Thu Dec 09, 2010  04:24PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BANDLANCZOSIRSPECT_H
#define BANDLANCZOSIRSPECT_H

// std headers
#include<string>
#include<fstream>
#include<vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosRspFunc.h"

class VccCalcDef;
class Vcc;

class BandLanczosIRspect
{
   protected:
      string                  mName;
      string                  mBlrsp;
      bool                    mAnalyze;
      vector<Nb>              mAnalysisBlocks;
      
      vector<Nb> mPeaks;        // Store frequencies of recognized peaks
      vector<Nb> mPeaks_inty;

      bool       mPrintIRTable; ///< Should a latex table be made?
      Nb         mWeightLimit;  ///< For storing the limit for weights entering the latex table
      Nb         mPeakLimit;    ///< For storing the limit for peaks entering the latex table


      void GenerateGnuPlot();
      void IdentifyPeaks(const MidasVector& aInty, Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);
      
      void FindOpers(vector<In>& aOperIndex, vector<string>* aOpers);

      void AnalyzeSpect(VccCalcDef* aVccCalcDef, Vcc* aVcc,
                        const MidasVector& aInty,
                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep,
                        vector<In>& aOperIndex,
                        vector<string>* aOpers);

      void InitAnalysisBlocksFromMinima(const MidasVector& aInty,
                                        Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep);

   public:
      BandLanczosIRspect(const string& aName, const string& aBlrsp,
                         const bool aPrintIR, const Nb aWeightLim, 
                         const Nb aPeakLim, const string& aAnalyze);
      
      void Create(VccCalcDef* aVccCalcDef, Vcc* aVcc);
};

#endif //BANDLANCZOSIRSPECT_H
