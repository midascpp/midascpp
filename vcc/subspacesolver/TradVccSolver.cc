/**
 *******************************************************************************
 * 
 * @file    TradVccSolver.cc
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/subspacesolver/TradVccSolver.h"
#include "vcc/subspacesolver/TradVccSolver_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

// Define instantiation macro.
#define INSTANTIATE_TRADVCCSOLVER(T) \
   namespace midas::vcc::subspacesolver \
   { \
      template class TradVccSolver<T>; \
   } /* namespace midas::vcc::subspacesolver */ \


// Instantiations.
INSTANTIATE_TRADVCCSOLVER(Nb);
INSTANTIATE_TRADVCCSOLVER(std::complex<Nb>);

#undef INSTANTIATE_TRADVCCSOLVER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
