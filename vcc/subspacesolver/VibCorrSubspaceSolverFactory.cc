/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolverFactory.cc
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"

#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory.h"
#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_VIBCORRSUBSPACESOLVERFACTORY(PARAM_T) \
   namespace midas::vcc::subspacesolver \
   { \
      template std::unique_ptr<VibCorrSubspaceSolver<PARAM_T>> VibCorrSubspaceSolverFactory \
         (  midas::tdvcc::CorrType \
         ,  midas::tdvcc::TrfType \
         ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t \
         ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t \
         ,  const typename VibCorrSubspaceSolver<PARAM_T>::opdef_t& \
         ,  const typename VibCorrSubspaceSolver<PARAM_T>::modalintegrals_t& \
         ,  const ModeCombiOpRange& \
         ); \
   } /* namespace midas::vcc::subspacesolver */ \


// Instantiations.
INSTANTIATE_VIBCORRSUBSPACESOLVERFACTORY(Nb);
INSTANTIATE_VIBCORRSUBSPACESOLVERFACTORY(std::complex<Nb>);

#undef INSTANTIATE_VIBCORRSUBSPACESOLVERFACTORY

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
