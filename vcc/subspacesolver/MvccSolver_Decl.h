/**
 *******************************************************************************
 * 
 * @file    MvccSolver_Decl.h
 * @date    27-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MVCCSOLVER_DECL_H_INCLUDED
#define MVCCSOLVER_DECL_H_INCLUDED

#include <tuple>

// Midas headers.
#include "vcc/subspacesolver/VibCorrSubspaceSolver.h"

namespace midas::vcc::subspacesolver
{

   /************************************************************************//**
    * @brief
    *    Equation solver for MVCC equations. Currently employs a CROP/DIIS method
    *    to the entire vector of residuals (including modal-coefficient changes).
    *    Later improvements could involve alternating schemes where the modal equations
    *    are solved after each step in the amplitude equations.
    * 
    * @warning
    *    The active-space modal constraint (0 = F-tilde - F'-tilde) is enforced 
    *    by simply eliminating one of the matrices from the equations.
    *    It should be checked if the constraint is satisfied at the end.
    *    Perhaps the algorithm should alternate between which matrix is eliminated 
    *    to make it more balanced?
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   class MvccSolver
      :  public VibCorrSubspaceSolver<PARAM_T>
   {
      public:
         using typename VibCorrSubspaceSolver<PARAM_T>::param_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::absval_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::opdef_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::modalintegrals_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t;

         //! Transformer type
         using trf_t = TRF_TMPL<param_t>;
         template<typename U> using cont_tmpl = typename trf_t::template cont_tmpl<U>;
         using cont_t = typename trf_t::cont_t;
         using mat_t = GeneralMidasMatrix<param_t>;
         using modal_cont_t = typename trf_t::modal_cont_t;

         //! Wrap s,l,U,W in a tuple
         using sol_vec_t = std::tuple<cont_t, cont_t, modal_cont_t>;

         //! Forward all constructor calls to base class.
         template<class... Args>
         MvccSolver(const n_modals_t& aNActiveModals, Args&&... args)
            :  VibCorrSubspaceSolver<PARAM_T>(std::forward<Args>(args)...)
            ,  mNActiveModals(aNActiveModals)
         {
            this->Mcr() = VibCorrSubspaceSolver<PARAM_T>::SetupMcr(std::move(this->Mcr()), aNActiveModals);
         }

         std::string CorrType() const override;
         static constexpr In RemoveRefFromFile() {return false;}

         void WriteSolVecsToFile(std::ostream& arOs, const std::string& arBaseName, const Uin aIndent) const override;
      
      protected:
         //! Return number of active modals
         const auto& NActiveModals() const { return this->mNActiveModals; }

         virtual void PrintSolVecAnalysis(std::ostream&, In) const override;

         virtual Uin SizeParamCont() const override;

         //! Converge modals and amplitudes a bit tighter than full residual (Niels: This is hard-coded just for testing - make it an input argument!)
         absval_t ModalsResNormThr() const { return std::max(1.e-2*this->ResNormThr(), std::numeric_limits<absval_t>::epsilon()); }
         absval_t AmplsResNormThr() const { return std::max(1.e-2*this->ResNormThr(), std::numeric_limits<absval_t>::epsilon());; }

      private:
         //! Number of active modals
         const n_modals_t mNActiveModals;

         //! Use alternating algorithm
         bool mAlternating = false;

         bool SolveImpl() override;
         bool SolveAlternatingImpl();
         bool SolveStackedImpl();
         GeneralMidasVector<param_t> FvciVecImpl(bool aBraNotKet) const override;
         std::vector<std::string> PrivGetFileDescriptors() const override;
         In PrivRemoveRefFromFile() const override {return RemoveRefFromFile();}

         sol_vec_t StartGuess() const;

         constexpr absval_t PreconInvRegEpsilon() const { return 1.e-2; }

         bool Alternating() const { return this->mAlternating; }
   };

   namespace mvcc
   {
      //! Get file descriptors (without any dependence on TRF_TMPL)
      std::vector<std::string> GetFileDescriptors();
   }; /* namespace mvcc */

}; /* namespace midas::vcc::subspacesolver */


#endif /* MVCCSOLVER_DECL_H_INCLUDED */