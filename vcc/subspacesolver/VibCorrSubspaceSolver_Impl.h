/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolver_Impl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VIBCORRSUBSPACESOLVER_IMPL_H_INCLUDED
#define VIBCORRSUBSPACESOLVER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/Trim.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "input/VccCalcDef.h"
#include "vcc/ModalIntegrals.h"
#include "tensor/NiceTensor.h"
#include "tensor/EnergyDifferencesTensor.h"
#include "mmv/MidasVector.h"
#include "mmv/VectorAngle.h"


namespace midas::vcc::subspacesolver
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   VibCorrSubspaceSolver<PARAM_T>::VibCorrSubspaceSolver
      (  n_modals_t aNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  ModeCombiOpRange aMcr
      )
      :  mNModals(std::move(aNModals))
      ,  mrOpDef(arOpDef)
      ,  mrModInts(arModInts)
      ,  mMcr(SetupMcr(std::move(aMcr),mNModals))
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::EnableInvDiagJac0Precon
      (  const DataCont& arEigVals
      ,  const std::vector<In>& arOffSets
      )
   {
      mpEigVals = &arEigVals;
      mpOffSets = &arOffSets;
      mInvDiagJac0Precon = true;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::DisableInvDiagJac0Precon
      (
      )
   {
      mpEigVals = nullptr;
      mpOffSets = nullptr;
      mInvDiagJac0Precon = false;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::PrintSettings
      (  std::ostream& arOs
      ,  const Uin aIndent
      )  const
   {
      // Store old format flags.
      const auto old_flags = arOs.flags();

      // Width, precisions and such.
      const std::string tab(aIndent, ' ');
      const std::string tab2(aIndent + 3, ' ');
      const std::string eq = " = ";
      const Uin w = 33;
      const Uin w2 = 33 - 3;
      arOs << std::left;
      arOs << std::boolalpha;

      arOs << tab << "General:\n";
      arOs << tab2 << std::setw(w2) << "Name" << eq << this->Name() << '\n';
      arOs << tab2 << std::setw(w2) << "CorrType" << eq << this->CorrType() << '\n';
      arOs << tab2 << std::setw(w2) << "IoLevel" << eq << this->IoLevel() << '\n';
      arOs << tab2 << std::setw(w2) << "TimeIt" << eq << this->TimeIt() << '\n';

      arOs << tab << "System:\n";
      arOs << tab2 << std::setw(w2) << "Oper" << eq << this->Oper().Name() << '\n';
      arOs << tab2 << std::setw(w2) << "ModInts" << eq << this->ModInts().Name() << '\n';
      arOs << tab2 << std::setw(w2) << "MaxExci" << eq << this->Mcr().GetMaxExciLevel() << '\n';
      arOs << tab2 << std::setw(w2) << "NumModes" << eq << this->Mcr().NumberOfModes() << '\n';
      arOs << tab2 << std::setw(w2) << "NumModals" << eq << this->NModals() << '\n';
      arOs << tab2 << std::setw(w2) << "Size, single param. cont." << eq << this->SizeParamCont() << '\n';

      arOs << tab << "SubspaceSolver:\n";
      arOs << tab2 << std::setw(w2) << "Algorithm" << eq << this->Algorithm() << '\n';
      arOs << tab2 << std::setw(w2) << "SubspaceDim" << eq << this->SubspaceDim() << '\n';
      arOs << tab2 << std::setw(w2) << "MaxIter" << eq << this->MaxIter() << '\n';
      arOs << tab2 << std::setw(w2) << "ResNormThr" << eq << this->ResNormThr() << '\n';
      arOs << tab2 << std::setw(w2) << "InvDiagJac0Precon" << eq << this->InvDiagJac0Precon() << '\n';

      arOs << std::endl;
      arOs.flags(old_flags);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::PrintConvInfo
      (  std::ostream& arOs
      ,  const Uin aIndent
      )  const
   {
      // Store old format flags.
      const auto old_flags = arOs.flags();

      // Width, precisions and such.
      const std::string tab(aIndent, ' ');
      const std::string tab2(aIndent + 3, ' ');
      const std::string eq = " = ";
      const Uin w = 33;
      const Uin w2 = w - 3;
      arOs << std::left;
      arOs << std::boolalpha;

      arOs << tab << "General:\n";
      arOs << tab2 << std::setw(w2) << "Name" << eq << this->Name() << '\n';
      arOs << tab2 << std::setw(w2) << "Converged" << eq << this->Conv() << '\n';
      arOs << tab2 << std::setw(w2) << "Num. iterations" << eq << this->NIter() << '\n';
      arOs << tab2 << std::setw(w2) << "Final energy [au]" << eq << this->ExpVal() << '\n';
      arOs << tab2 << std::setw(w2) << "Final energy [cm^-1]" << eq << this->ExpVal()*C_AUTKAYS << '\n';
      arOs << tab2 << std::setw(w2) << "Final energy [eV]" << eq << this->ExpVal()*C_AUTEV << '\n';

      arOs << tab << "Solution vectors:\n";
      arOs << tab2 << std::setw(w2) << "Last residual norm" << eq << this->LastResNorm() << '\n';
      this->PrintSolVecAnalysis(arOs, aIndent);

      if (this->Conv() && this->FvciAnalysis())
      {
         const absval_t n2_ket = this->FvciNorm2(false);
         const absval_t n2_bra = this->FvciNorm2(true);
         const absval_t n2_prod = n2_ket * n2_bra;
         const absval_t angle = this->HilbertSpaceAngleKetBra(false);
         const absval_t angle_max_exci = this->HilbertSpaceAngleKetBra(true);

         arOs << tab << "FVCI vector analysis:\n";
         arOs << tab << "(b = |bra>, k = |ket>, as FVCI vectors)\n";
         arOs << tab2 << std::setw(w2) << "FVCI-norm, ket, sqrt(<k|k>)" << eq << sqrt(n2_ket) << '\n';
         arOs << tab2 << std::setw(w2) << "FVCI-norm, bra, sqrt(<b|b>)" << eq << sqrt(n2_bra) << '\n';
         arOs << tab2 << std::setw(w2) << "FVCI-norm, product" << eq << sqrt(n2_prod) << '\n';
         arOs << tab2 << std::setw(w2) << "angle(b,k) [rad]" << eq << angle << '\n';
         arOs << tab2 << std::setw(w2) << "angle(b,k) [rad] (<= max.exci)" << eq << angle_max_exci << '\n';
      }

      arOs << std::endl;
      arOs.flags(old_flags);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool VibCorrSubspaceSolver<PARAM_T>::Solve
      (
      )
   {
      const bool conv = SolveImpl();
      if (conv && this->FvciAnalysis())
      {
         this->mFvciVecs.reserve(2);
         for(const auto& bra_not_ket: {false,true})
         {
            this->mFvciVecs.emplace_back(this->FvciVec(bra_not_ket, false));
         }
      }
      return conv;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   Uin VibCorrSubspaceSolver<PARAM_T>::SizeParamCont
      (
      )  const
   {
      Uin n = 0;
      for(const auto& mc: this->Mcr())
      {
         n += NumParams(mc, this->NModals());
      }
      return n;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> VibCorrSubspaceSolver<PARAM_T>::DiagJac0
      (  const n_modals_t& aNModals
      )  const
   {
      GeneralMidasVector<param_t> v;
      if (this->mpEigVals && this->mpOffSets)
      {
         const std::vector<In> n_modals_in(aNModals.begin(), aNModals.end());
         for(const auto& mc: this->Mcr())
         {
            ExactEnergyDifferencesTensor<Nb> e_diffs(mc.MCVec(), n_modals_in, *mpEigVals, *mpOffSets, 0.0);
            v.Insert(v.Size(), e_diffs.GetData(), e_diffs.GetData() + e_diffs.TotalSize());
         }
      }
      else
      {
         MIDASERROR("mpEigVals or mpOffSets is nullptr.");
      }
      return v;
   }

   /************************************************************************//**
    * @note
    *    If ModeCombiOpRange contains the empty ModeCombi, we expect the
    *    corresponding DiagJac0 element to be zero. This is no good when taking
    *    the inverse. But generally we don't want to solve for ref. elements if
    *    present, so just set them to zero in the InvDiagJac0.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> VibCorrSubspaceSolver<PARAM_T>::InvDiagJac0
      (  const n_modals_t& aNModals
      )  const
   {
      GeneralMidasVector<param_t> v(this->DiagJac0(aNModals));
      for(Uin i = 0; i < this->Mcr().NumEmptyMCs() && i < v.Size(); ++i)
      {
         v[i] = param_t(0);
      }
      for(Uin i = this->Mcr().NumEmptyMCs(); i < v.Size(); ++i)
      {
         auto& val = v[i];
         if (val == param_t(0))
         {
            MIDASERROR("DiagJac0["+std::to_string(i)+"] = 0; degenerate reference state?");
         }
         else
         {
            val = param_t(1)/val;
         }
      }
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> VibCorrSubspaceSolver<PARAM_T>::FvciVec
      (  bool aBraNotKet
      ,  bool aNormalized
      )  const
   {
      MidasAssert(this->Conv(), "Solver not converged.");
      GeneralMidasVector<param_t> v = this->FvciVecImpl(aBraNotKet);
      if (aNormalized)
      {
         NormalizeRealPositive(v);
      }
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   ModeCombiOpRange VibCorrSubspaceSolver<PARAM_T>::SetupMcr
      (  ModeCombiOpRange&& aMcr
      ,  const n_modals_t& arNModals
      )
   {
      if (aMcr.NumEmptyMCs() == 0)
      {
         aMcr.Insert(std::vector<ModeCombi>{ModeCombi(std::vector<In>{},-1)});
      }
      SetAddresses(aMcr, arNModals);
      return std::move(aMcr);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::NormalizeRealPositive
      (  GeneralMidasVector<param_t>& arVec
      )
   {
      if(arVec.Size() > 0)
      {
         MidasAssert(arVec[0] != param_t(0), "arVec[0] == 0, don't know what to do.");
         arVec.Scale((midas::util::AbsVal(arVec[0])/arVec[0])*1./arVec.Norm());
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::WriteSolVecsToFile
      (  std::ostream& arOs
      ,  const std::string& arBaseName
      ,  const Uin aIndent
      )  const
   {
      if (this->SolVecs<>().size() > 0)
      {
         const auto old_flags = arOs.flags();
         arOs << std::left;
         const std::string base = midas::input::DeleteBlanks(arBaseName);

         const std::string tab(aIndent, ' ');
         arOs << tab << "Writing solution vectors to file:\n";

         const std::vector<std::string> v_descriptors = this->PrivGetFileDescriptors();
         for(Uin i = 0; i < this->SolVecs<>().size(); ++i)
         {
            const std::string file = base + "_" + v_descriptors.at(i);
            if(this->WriteSolVecToFile(this->SolVecs<>().at(i), file))
            {
               arOs
                  << tab
                  << " - " << std::setw(20) << v_descriptors.at(i)
                  << "(file: " << file << "_0)"
                  << '\n'
                  ;
            }
         }

         arOs << std::flush;
         arOs.flags(old_flags); 
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool VibCorrSubspaceSolver<PARAM_T>::WriteSolVecToFile
      (  const GeneralMidasVector<param_t>& arVec
      ,  const std::string& arName
      )  const
   {
      const GeneralMidasVector<param_t>* p_vec = &arVec;
      GeneralMidasVector<param_t> v_erase;
      if (this->PrivRemoveRefFromFile())
      {
         v_erase.SetNewSize(arVec.Size());
         v_erase.Erase(0,1);
         p_vec = &v_erase;
      }
      GeneralDataCont<param_t> dc(*p_vec, "InMem", arName, true);
      return dc.ChangeStorageTo("OnDisc");
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool VibCorrSubspaceSolver<PARAM_T>::WriteSolVecToFile
      (  const GeneralDataCont<param_t>& arDc
      ,  const std::string& arName
      )  const
   {
      GeneralMidasVector<param_t> mv;
      auto off = this->PrivRemoveRefFromFile() ? I_1 : I_0;
      auto nelem = arDc.Size() - off;
      arDc.DataIo(IO_GET, mv, nelem, off);

      GeneralDataCont<param_t> dc(mv, "InMem", arName, true);
      return dc.ChangeStorageTo("OnDisc");
   }

   /************************************************************************//**
    * @param[in] aModals      Vector of {U,W} matrices
    * @param[in] aName        Base name of file to write
    * @param[in] aU           Write U modals
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool VibCorrSubspaceSolver<PARAM_T>::WriteSolVecToFile
      (  const std::vector<std::pair<GeneralMidasMatrix<param_t>,GeneralMidasMatrix<param_t>>>& aModals
      ,  const std::string& aName
      ,  bool aU
      )  const
   {
      // Figure out size of modal coefs
      In size = I_0;
      std::vector<In> offsets;
      offsets.reserve(aModals.size());
      for(const auto& modals_m : aModals)
      {
         const auto& u = modals_m.first;
         const auto& w = modals_m.second;
         MidasAssert(u.Ncols() == w.Nrows(), "U.Ncols() != W.Nrows()");
         MidasAssert(w.Ncols() == u.Nrows(), "W.Ncols() != U.Nrows()");
         offsets.emplace_back(size);
         size += u.Ncols() * u.Nrows();
      }
      
      GeneralDataCont<param_t> dc(size, param_t(0.0), "InMem", aName, true);

      size_t m_count = 0;
      for(const auto& modals_m : aModals)
      {
         const auto& mmat = aU ? modals_m.first : modals_m.second;
         dc.DataIo(IO_PUT, mmat, mmat.size(), mmat.Nrows(), mmat.Ncols(), offsets[m_count++]);
      }

      return dc.ChangeStorageTo("OnDisc");
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename VibCorrSubspaceSolver<PARAM_T>::absval_t VibCorrSubspaceSolver<PARAM_T>::FvciNorm2
      (  bool aBraNotKet
      )  const
   {
      absval_t val = 0;
      try
      {
         val = Norm2(this->mFvciVecs.at(aBraNotKet? 1: 0));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Called FvciNorm2, but solver not converged, or FvciAnalysis() disabled.");
      }
      return val;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename VibCorrSubspaceSolver<PARAM_T>::absval_t VibCorrSubspaceSolver<PARAM_T>::FvciNorm2Product
      (
      )  const
   {
      return this->FvciNorm2(false) * this->FvciNorm2(true);
   }

   /************************************************************************//**
    * @note
    *    If ModeCombiOpRange is standard MCR[k] type, then `aOnlyWithinMaxExci
    *    = true` is the same as "in space". If not, then you'll also get a
    *    contribution from ModeCombi%s whose order is <=
    *    Mcr().GetMaxExciLevel().
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename VibCorrSubspaceSolver<PARAM_T>::absval_t VibCorrSubspaceSolver<PARAM_T>::HilbertSpaceAngleKetBra
      (  bool aOnlyWithinMaxExci
      )  const
   {
      absval_t val = 0;
      try
      {
         const auto& ket = this->mFvciVecs.at(0);
         const auto& bra = this->mFvciVecs.at(1);
         const Uin beg = 0;
         Uin end = Size(ket);
         if (aOnlyWithinMaxExci)
         {
            ModeCombiOpRange tmp_mcr(this->Mcr().GetMaxExciLevel(), this->NModals().size());
            end = SetAddresses(tmp_mcr, this->NModals());
         }
         val = VectorAngle<true>(bra, ket, beg, end);
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Called HilbertSpaceAngleKetBra, but solver not converged, or FvciAnalysis() disabled.");
      }
      return val;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void SetGeneralSettings
      (  VibCorrSubspaceSolver<PARAM_T>& arSolver
      ,  const VccCalcDef& arCalcDef
      ,  const DataCont* const apEigVals
      ,  const std::vector<In>* const apOffSets
      )
   {
      // General settings.
      arSolver.Name() = arCalcDef.GetName();
      arSolver.SubspaceDim() = arCalcDef.GetNSubspaceVecs();
      arSolver.MaxIter() = arCalcDef.GetItEqMaxIter();
      arSolver.ResNormThr() = arCalcDef.GetItEqResidThr();
      arSolver.TimeIt() = arCalcDef.TimeIt();
      arSolver.IoLevel() = arCalcDef.IoLevel();
      arSolver.FvciAnalysis() = arCalcDef.MatRepFvciAnalysis();

      // Algorithm.
      if (arCalcDef.GetCrop())
      {
         arSolver.EnableCrop();
      }
      else
      {
         arSolver.EnableDiis();
      }

      // Preconditioner.
      if (arCalcDef.UsePrecon())
      {
         MidasAssert(apEigVals, "arCalcDef.UsePrecon() == true, but apEigVals == nullptr.");
         MidasAssert(apOffSets, "arCalcDef.UsePrecon() == true, but apOffSets == nullptr.");
         arSolver.EnableInvDiagJac0Precon(*apEigVals, *apOffSets);
      }
      else
      {
         arSolver.DisableInvDiagJac0Precon();
      }
   }

   
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void VibCorrSubspaceSolver<PARAM_T>::PrintSolVecAnalysis
      (  std::ostream& arOs
      ,  In aIndent
      )  const
   {
      const std::string tab(aIndent, ' ');
      const std::string tab2(aIndent + 3, ' ');
      const std::string eq = " = ";
      const Uin w = 33;
      const Uin w2 = 33 - 3;
      arOs << tab2 << std::setw(w2) << "Num. sol. vecs." << eq << this->SolVecs<>().size() << '\n';
      const std::vector<std::string> v_descr = this->PrivGetFileDescriptors();
      for(Uin i = 0; i < v_descr.size() && i < this->SolVecs<>().size(); ++i)
      {
         arOs  << tab2 << std::setw(w2) << std::string("Norm(" + v_descr.at(i) + ")")
               << eq
               << Norm(this->SolVecs<>().at(i)) 
               << '\n';
      }
   }


} /* namespace midas::vcc::subspacesolver */

#endif/*VIBCORRSUBSPACESOLVER_IMPL_H_INCLUDED*/
