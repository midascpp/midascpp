/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolver.cc
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/subspacesolver/VibCorrSubspaceSolver.h"
#include "vcc/subspacesolver/VibCorrSubspaceSolver_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

// Define instantiation macro.
#define INSTANTIATE_VIBCORRSUBSPACESOLVER(PARAM_T) \
   namespace midas::vcc::subspacesolver \
   { \
      template class VibCorrSubspaceSolver<PARAM_T>; \
      template void SetGeneralSettings \
         (  VibCorrSubspaceSolver<PARAM_T>& \
         ,  const VccCalcDef& \
         ,  const DataCont* const \
         ,  const std::vector<In>* const \
         ); \
   } /* namespace midas::vcc::subspacesolver */ \


// Instantiations.
INSTANTIATE_VIBCORRSUBSPACESOLVER(Nb);
INSTANTIATE_VIBCORRSUBSPACESOLVER(std::complex<Nb>);

#undef INSTANTIATE_VIBCORRSUBSPACESOLVER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
