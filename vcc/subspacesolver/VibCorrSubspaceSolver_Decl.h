/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolver_Decl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VIBCORRSUBSPACESOLVER_DECL_H_INCLUDED
#define VIBCORRSUBSPACESOLVER_DECL_H_INCLUDED

// Standard headers.
#include <limits>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombiOpRange.h"
#include "libmda/util/any_type.h"

// Forward declarations.
class OpDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;
using DataCont = GeneralDataCont<Nb>;
class VccCalcDef;

namespace midas::vcc::subspacesolver
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class VibCorrSubspaceSolver
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         using opdef_t = OpDef;
         using modalintegrals_t = ModalIntegrals<param_t>;
         using n_modals_t = std::vector<Uin>;

         VibCorrSubspaceSolver
            (  n_modals_t aNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  ModeCombiOpRange arMcr
            );

         virtual ~VibCorrSubspaceSolver() = default;

         const std::string& Name() const {return mName;}
         std::string& Name() {return mName;} 
         Uin IoLevel() const {return mIoLevel;}
         Uin& IoLevel() {return mIoLevel;} 
         bool TimeIt() const {return mTimeIt;}
         bool& TimeIt() {return mTimeIt;}
         bool UsingDiis() const {return !mCropNotDiis;}
         bool UsingCrop() const {return mCropNotDiis;}
         std::string Algorithm() const {return this->UsingCrop()? "CROP": "DIIS";}
         void EnableDiis() {mCropNotDiis = false;}
         void EnableCrop() {mCropNotDiis = true;}
         Uin SubspaceDim() const {return mSubspaceDim;}
         Uin& SubspaceDim() {return mSubspaceDim;}
         Uin MaxIter() const {return mMaxIter;}
         Uin& MaxIter() {return mMaxIter;}
         absval_t ResNormThr() const {return mResNormThr;}
         absval_t& ResNormThr() {return mResNormThr;}
         bool FvciAnalysis() const {return mFvciAnalysis;}
         bool& FvciAnalysis() {return mFvciAnalysis;}
         bool InvDiagJac0Precon() const {return mInvDiagJac0Precon && mpEigVals && mpOffSets;}
         void EnableInvDiagJac0Precon(const DataCont& arEigVals, const std::vector<In>& arOffSets);
         void DisableInvDiagJac0Precon();
         virtual std::string CorrType() const = 0;

         bool Solve();
         void WriteSolVecsToDisk() const;

         bool Conv() const {return mConv;}
         Uin NIter() const {return mNIter;}
         param_t ExpVal() const {return mExpVal;}

         //! Get solution vectors (you need to know the type for the specific solver)
         template<typename U = std::vector<GeneralMidasVector<param_t>>>
         const U& SolVecs() const {return mSolVecs.template get<U>();}

         absval_t LastResNorm() const {return mLastResNorm;}

         GeneralMidasVector<param_t> FvciVec(bool aBraNotKet = false, bool aNormalized = true) const;

         void PrintSettings(std::ostream& arOs, const Uin aIndent) const;
         void PrintConvInfo(std::ostream& arOs, const Uin aIndent) const;
         virtual void WriteSolVecsToFile(std::ostream& arOs, const std::string& arBaseName, const Uin aIndent) const;

         /**
          * @name FVCI vector analysis
          **/
         //!@{
         //! Norm^2 of |ket> or <bra| expanded in FVCI space. I.e. <ket|ket> or <bra|bra>.
         absval_t FvciNorm2(bool aBraNotKet = false) const;

         //! Product of bra/ket FvciNorm2, i.e. <ket|ket>*<bra|bra>. Should be 1 for most/all methods!
         absval_t FvciNorm2Product() const;

         //! Vector angle between |ket> and |bra> as FVCI expansions. (`acos |<b*|k>|/(|b*||k|)`)
         absval_t HilbertSpaceAngleKetBra(bool aOnlyWithinMaxExci = false) const;

         //!@}

      protected:
         const n_modals_t& NModals() const {return mNModals;}
         auto NModes() const { return this->NModals().size(); }
         const opdef_t& Oper() const {return mrOpDef;}
         const modalintegrals_t& ModInts() const {return mrModInts;}
         const ModeCombiOpRange& Mcr() const {return mMcr;}
         ModeCombiOpRange& Mcr() {return mMcr;}
         virtual Uin SizeParamCont() const;

         GeneralMidasVector<param_t> DiagJac0(const n_modals_t&) const;
         GeneralMidasVector<param_t> InvDiagJac0(const n_modals_t&) const;

         void SetConv(bool a) {mConv = a;}
         void SetNIter(Uin a) {mNIter = a;}
         void SetExpVal(param_t a) {mExpVal = a;}
         void SetSolVecs(libmda::util::any_type&& a) {mSolVecs = std::move(a);}
         void SetLastResNorm(absval_t a) {mLastResNorm = a;}

         static ModeCombiOpRange SetupMcr(ModeCombiOpRange&& aMcr, const n_modals_t& arNModals);

         virtual void PrintSolVecAnalysis(std::ostream&, In) const;

         bool WriteSolVecToFile(const GeneralMidasVector<param_t>&, const std::string&) const;
         bool WriteSolVecToFile(const GeneralDataCont<param_t>&, const std::string&) const;
         bool WriteSolVecToFile
            (  const std::vector<std::pair<GeneralMidasMatrix<param_t>,GeneralMidasMatrix<param_t>>>&
            ,  const std::string&
            ,  bool
            )  const;

      private:
         const n_modals_t mNModals;
         const opdef_t& mrOpDef;
         const modalintegrals_t& mrModInts;
         ModeCombiOpRange mMcr;

         std::string mName = "vibcorrsubspacesolver";
         Uin mIoLevel = 0;
         bool mTimeIt = 0;
         bool mCropNotDiis = false;
         Uin mSubspaceDim = 3;
         Uin mMaxIter = 50;
         absval_t mResNormThr = 10*std::numeric_limits<absval_t>::epsilon();
         bool mFvciAnalysis = false;

         bool mInvDiagJac0Precon = false;
         const DataCont* mpEigVals = nullptr;
         const std::vector<In>* mpOffSets = nullptr;

         bool mConv = false;
         Uin mNIter = 0;
         param_t mExpVal = 0;
         libmda::util::any_type mSolVecs;
         absval_t mLastResNorm = -1;
         std::vector<GeneralMidasVector<param_t>> mFvciVecs;

         virtual bool SolveImpl() = 0;
         virtual GeneralMidasVector<param_t> FvciVecImpl(bool aBraNotKet) const = 0;
         static void NormalizeRealPositive(GeneralMidasVector<param_t>& arVec);

         virtual std::vector<std::string> PrivGetFileDescriptors() const = 0;
         virtual In PrivRemoveRefFromFile() const = 0;
   };

   //!
   template<typename PARAM_T>
   void SetGeneralSettings
      (  VibCorrSubspaceSolver<PARAM_T>& arSolver
      ,  const VccCalcDef& arCalcDef
      ,  const DataCont* const apEigVals = nullptr
      ,  const std::vector<In>* const apOffSets = nullptr
      );

} /* namespace midas::vcc::subspacesolver */

#endif/*VIBCORRSUBSPACESOLVER_DECL_H_INCLUDED*/
