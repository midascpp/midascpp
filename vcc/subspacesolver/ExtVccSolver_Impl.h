/**
 *******************************************************************************
 * 
 * @file    ExtVccSolver_Impl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXTVCCSOLVER_IMPL_H_INCLUDED
#define EXTVCCSOLVER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "mmv/MidasVector.h"
#include "td/tdvcc/trf/TrfTdextvccMatRep.h"
#include "td/tdvcc/params/ParamsTdextvcc.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/TdvccEnums.h"
#include "it_solver/nl_solver/SubspaceSolver.h"

namespace midas::vcc::subspacesolver
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool ExtVccSolver<PARAM_T>::SolveImpl
      (
      )
   {
      // The ModeCombiOpRange used _must_ include empty ModeCombi, but this is
      // handled in VibCorrSubspaceSolver::SetupMcr().
      const ModeCombiOpRange& mcr = this->Mcr();

      // The lambda doing the transformations.
      midas::tdvcc::TrfTdextvccMatRep<param_t> trf(this->NModals(), this->Oper(), this->ModInts(), mcr);
      trf.IoLevel() = this->IoLevel();
      trf.TimeIt() = this->TimeIt();
      auto f = [&trf](const sol_vec_t& v) -> sol_vec_t
      {
         vec_t dH_ds = trf.HamDerExtAmp(0, v.at(0), v.at(1));
         vec_t dH_dt = trf.HamDerClustAmp(0, v.at(0), v.at(1));
         if(Size(dH_ds) > 0 && Size(dH_dt) > 0)
         {
            dH_ds[0] = param_t(0);
            dH_dt[0] = param_t(0);
         }
         return {dH_ds, dH_dt};
      };

      // Setup preconditioner if necessary (otherwise leave as dummy).
      sol_vec_t precon = {vec_t(), vec_t()};
      if (this->InvDiagJac0Precon())
      {
         vec_t inv_diag = this->InvDiagJac0(this->NModals());
         precon = {inv_diag, inv_diag};
      }

      // Setup solver and solve.
      auto p_solver = midas::nlsolver::SubspaceSolverFactory<decltype(f)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  this->ResNormThr()
         ,  std::move(precon)
         );
      sol_vec_t v = {vec_t(this->SizeParamCont(), param_t(0)), vec_t(this->SizeParamCont(), param_t(0))};
      bool conv = p_solver->Solve(v);
      this->SetConv(conv);
      this->SetNIter(p_solver->NumIter());
      this->SetExpVal(midas::tdvcc::ExpVal
         (  absval_t(0)
         ,  midas::tdvcc::ParamsTdextvcc<param_t,GeneralMidasVector>(v.at(0), v.at(1))
         ,  trf
         ));
      this->SetLastResNorm(p_solver->LastResNorm());
      this->SetSolVecs(std::vector<vec_t>{std::move(v.at(0)), std::move(v.at(1))});

      return this->Conv();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<typename ExtVccSolver<PARAM_T>::param_t> ExtVccSolver<PARAM_T>::FvciVecImpl
      (  bool aBraNotKet
      )  const
   {
      midas::tdvcc::ParamsTdextvcc<param_t,GeneralMidasVector> params(this->template SolVecs<>().at(0), this->template SolVecs<>().at(1));
      if (aBraNotKet)
      {
         return midas::tdvcc::ConvertBraToFvci(params, this->NModals(), this->Mcr());
      }
      else
      {
         return midas::tdvcc::ConvertKetToFvci(params, this->NModals(), this->Mcr());
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   std::string ExtVccSolver<PARAM_T>::CorrType
      (
      )  const
   {
      return midas::tdvcc::StringFromEnum(midas::tdvcc::CorrType::EXTVCC);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   std::vector<std::string> ExtVccSolver<PARAM_T>::PrivGetFileDescriptors
      (
      )  const
   {
      return GetFileDescriptors();
   }

} /* namespace midas::vcc::subspacesolver */

#endif/*EXTVCCSOLVER_IMPL_H_INCLUDED*/
