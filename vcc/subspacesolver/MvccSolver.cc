/**
 *******************************************************************************
 * 
 * @file    MvccSolver.cc
 * @date    28-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/subspacesolver/MvccSolver.h"
#include "vcc/subspacesolver/MvccSolver_Impl.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

namespace midas::vcc::subspacesolver::mvcc
{
   //! Get file descriptors for MvccSolver (without any dependence on TRF_TMPL)
   std::vector<std::string> GetFileDescriptors() {return {"t_amps", "l_amps", "u_modals", "w_modals"};}
}; /* namespace midas::vcc::subspacesolver::mvcc */

// Define instantiation macro.
#define INSTANTIATE_MVCCSOLVER(T) \
   namespace midas::vcc::subspacesolver \
   { \
      template class MvccSolver<T, midas::tdvcc::TrfTdmvcc2>; \
      template class MvccSolver<T, midas::tdvcc::TrfTdmvccMatRep>; \
   } /* namespace midas::vcc::subspacesolver */ \


// Instantiations.
INSTANTIATE_MVCCSOLVER(Nb);
INSTANTIATE_MVCCSOLVER(std::complex<Nb>);

#undef INSTANTIATE_MVCCSOLVER

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

