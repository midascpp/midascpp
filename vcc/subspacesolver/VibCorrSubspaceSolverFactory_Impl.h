/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolverFactory_Impl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VIBCORRSUBSPACESOLVERFACTORY_IMPL_H_INCLUDED
#define VIBCORRSUBSPACESOLVERFACTORY_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory.h"
#include "vcc/subspacesolver/ExtVccSolver.h"
#include "vcc/subspacesolver/TradVccSolver.h"
#include "vcc/subspacesolver/MvccSolver.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"


namespace midas::vcc::subspacesolver
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   std::unique_ptr<VibCorrSubspaceSolver<PARAM_T>> VibCorrSubspaceSolverFactory
      (  midas::tdvcc::CorrType aCorr
      ,  midas::tdvcc::TrfType aTrf
      ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t aNActiveModals
      ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t aNPrimModals
      ,  const typename VibCorrSubspaceSolver<PARAM_T>::opdef_t& arOpDef
      ,  const typename VibCorrSubspaceSolver<PARAM_T>::modalintegrals_t& arModInts
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::tdvcc::CorrType;
      using midas::tdvcc::TrfType;

      // Small util. lambda for error info.
      auto err_info = [](CorrType c, TrfType t) -> std::string
      {
         std::stringstream ss;
         ss << "VibCorrSubspaceSolverFactory: Not implemented for "
            << "TrfType = '" << midas::tdvcc::StringFromEnum(t) << "', "
            << "CorrType = '" << midas::tdvcc::StringFromEnum(c) << "'."
            ;
         return ss.str();
      };

      std::unique_ptr<VibCorrSubspaceSolver<PARAM_T>> p = nullptr;
      try
      {
         if (aTrf == TrfType::MATREP)
         {
            if (aCorr == CorrType::EXTVCC)
            {
               p = std::make_unique<ExtVccSolver<PARAM_T>>(std::move(aNPrimModals), arOpDef, arModInts, arMcr);
            }
            else if  (aCorr == CorrType::VCC)
            {
               p = std::make_unique<TradVccSolver<PARAM_T>>(std::move(aNPrimModals), arOpDef, arModInts, arMcr);
            }
            else if  (aCorr == CorrType::TDMVCC)
            {
               p = std::make_unique<MvccSolver<PARAM_T, midas::tdvcc::TrfTdmvccMatRep>>(std::move(aNActiveModals), std::move(aNPrimModals), arOpDef, arModInts, arMcr);
            }
            else
            {
               throw(int(-2));
            }
         }
         else if (aTrf == TrfType::VCC2H2)
         {
            if (aCorr == CorrType::TDMVCC)
            {
               p = std::make_unique<MvccSolver<PARAM_T, midas::tdvcc::TrfTdmvcc2>>(std::move(aNActiveModals), std::move(aNPrimModals), arOpDef, arModInts, arMcr);
            }
            else
            {
               throw(int(-3));
            }
         }
         else
         {
            throw(int(-1));
         }
      }
      catch(int)
      {
         MIDASERROR(err_info(aCorr, aTrf));
      }

      return p;
   }

} /* namespace midas::vcc::subspacesolver */

#endif/*VIBCORRSUBSPACESOLVERFACTORY_IMPL_H_INCLUDED*/
