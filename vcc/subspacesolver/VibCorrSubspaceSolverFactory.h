/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolverFactory.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VIBCORRSUBSPACESOLVERFACTORY_H_INCLUDED
#define VIBCORRSUBSPACESOLVERFACTORY_H_INCLUDED

#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*VIBCORRSUBSPACESOLVERFACTORY_H_INCLUDED*/
