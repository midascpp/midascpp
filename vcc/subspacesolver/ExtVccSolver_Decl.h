/**
 *******************************************************************************
 * 
 * @file    ExtVccSolver_Decl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXTVCCSOLVER_DECL_H_INCLUDED
#define EXTVCCSOLVER_DECL_H_INCLUDED

// Standard headers.
#include <array>

// Midas headers.
#include "vcc/subspacesolver/VibCorrSubspaceSolver.h"

// Forward declarations.

namespace midas::vcc::subspacesolver
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class ExtVccSolver
      :  public VibCorrSubspaceSolver<PARAM_T>
   {
      public:
         using typename VibCorrSubspaceSolver<PARAM_T>::param_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::absval_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::opdef_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::modalintegrals_t;
         using typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t;

         using vec_t = GeneralMidasVector<param_t>;
         using sol_vec_t = std::array<vec_t,2>;

         //! Forward all constructor calls to base class.
         template<class... Args>
         ExtVccSolver(Args&&... args)
            :  VibCorrSubspaceSolver<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         std::string CorrType() const override;
         static std::vector<std::string> GetFileDescriptors() {return {"t_amps", "s_amps"};}
         static In RemoveRefFromFile() {return false;}

      private:
         bool SolveImpl() override;
         GeneralMidasVector<param_t> FvciVecImpl(bool aBraNotKet) const override;
         std::vector<std::string> PrivGetFileDescriptors() const override;
         In PrivRemoveRefFromFile() const override {return RemoveRefFromFile();}
   };

} /* namespace midas::vcc::subspacesolver */

#endif/*EXTVCCSOLVER_DECL_H_INCLUDED*/
