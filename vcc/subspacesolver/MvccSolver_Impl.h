/**
 *******************************************************************************
 * 
 * @file    MvccSolver_Impl.h
 * @date    27-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MVCCSOLVER_IMPL_H_INCLUDED
#define MVCCSOLVER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "input/Trim.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/TdvccEnums.h"
#include "it_solver/nl_solver/SubspaceSolver.h"
#include "util/RegularizedInverse.h"

namespace midas::vcc::subspacesolver
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   bool MvccSolver<PARAM_T, TRF_TMPL>::SolveImpl
      (
      )
   {
      if (  this->Alternating()
         )
      {
         return this->SolveAlternatingImpl();
      }
      else
      {
         return this->SolveStackedImpl();
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   bool MvccSolver<PARAM_T, TRF_TMPL>::SolveStackedImpl
      (
      )
   {
      // The ModeCombiOpRange used _must_ include empty ModeCombi, but this is
      // handled in VibCorrSubspaceSolver::SetupMcr().
      const ModeCombiOpRange& mcr = this->Mcr();

      trf_t trf(this->NActiveModals(), this->NModals(), this->Oper(), this->ModInts(), mcr);
      trf.IoLevel() = this->IoLevel();
      trf.TimeIt() = this->TimeIt();
      param_t energy = 0;

      // Setup guess
      sol_vec_t guess = this->StartGuess();

      // The lambda doing the transformations for T
      auto f = [&trf,&mcr,&energy](const sol_vec_t& v) -> sol_vec_t
      {
         const auto& s_trial = std::get<0>(v);
         const auto& l_trial = std::get<1>(v);
         const auto& modals_trial = std::get<2>(v);

         // Update integrals
         trf.UpdateIntegrals(modals_trial);

         // Compute error vector; then extract energy, set ref. elem to zero
         auto s_residual = trf.ErrVec(0, s_trial);
         energy = midas::tdvcc::ZeroFirstElemAndReturnIt(s_residual);

         // Zero all one-mode amplitudes
         auto s1norms = midas::tdvcc::ZeroOneModeAmplitudes(s_residual, mcr);

         // Compute l_residual = eta + lA.
         auto l_residual = trf.EtaVec(0, s_trial);
         Axpy(l_residual, trf.LJac(0, s_trial, l_trial), param_t(+1));
         midas::tdvcc::ZeroFirstElemAndReturnIt(l_residual);

         // Zero all one-mode amplitudes
         auto l1norms = midas::tdvcc::ZeroOneModeAmplitudes(l_residual, mcr);

         // Output
         if (trf.IoLevel() > I_10)
         {
            Mout  << " max ||l1^m|| = " << *std::max_element(l1norms.begin(), l1norms.end()) << std::endl;
         }

         const auto& nmodes = modals_trial.size();

         // Compute density matrices and mean fields
         const auto mean_fields = trf.MeanFieldMatrices(0, s_trial, l_trial); // Returns {H1, FR, H1', FR'}
         const auto densmats = trf.DensityMatrices(s_trial, l_trial);

         // Compute residuals
         modal_cont_t modals_residual(nmodes);
         for(In m=I_0; m<nmodes; ++m)
         {
            const auto& rho = densmats[m];
            const auto& u = modals_trial[m].first;
            const auto& w = modals_trial[m].second;

            // Q
            mat_t q = u*w;
            midas::mmv::WrapScale(q, param_t(-1));
            for(Uin alpha=I_0; alpha<q.Ncols(); ++alpha) q[alpha][alpha] += param_t(+1);

            mat_t f_check = mean_fields[m][0]*rho + mean_fields[m][1];
            mat_t fp_check = rho*mean_fields[m][2] + mean_fields[m][3];
            mat_t f_tilde = w*f_check;

            modals_residual[m] = std::make_pair(mat_t(q*f_check), mat_t(fp_check - f_tilde*w));  // In second residual we use the F-tilde = F'-tilde condition

            if (trf.IoLevel() > I_10)
            {
               mat_t fp_tilde = fp_check*u;
               Mout  << " mode " << m << "||F-tilde - F'-tilde|| = " << std::sqrt(DiffNorm2(f_tilde, fp_tilde)) << '\n'
                     << " u:\n" << u << '\n'
                     << " res_u:\n" << modals_residual[m].first << '\n'
                     << " w:\n" << w << '\n'
                     << " res_w:\n" << modals_residual[m].second << '\n'
                     << " F-check:\n" << f_check << '\n'
                     << " F'-check:\n" << fp_check << '\n'
                     << " Q:\n" << q << '\n'
                     << std::flush;
            }
         }

         // Return amplitude residuals
         return {s_residual, l_residual, modals_residual};
      };

      // Setup preconditioner for amplitude equations if necessary (otherwise leave as dummy).
      sol_vec_t precon;
      if (this->InvDiagJac0Precon())
      {
         cont_t inv_diag(this->InvDiagJac0(this->NActiveModals()));

         modal_cont_t ones(this->NModes());
         for(In imode=I_0; imode<this->NModes(); ++imode)
         {
            const auto& nprim = this->NModals()[imode];
            const auto& nact = this->NActiveModals()[imode];
            ones[imode] = std::make_pair(mat_t(nprim, nact, param_t(1.0)), mat_t(nact, nprim, param_t(1.0)));
         }

         precon = {inv_diag, inv_diag, ones};
      }

      // Setup solver
      auto p_solver = midas::nlsolver::SubspaceSolverFactory<decltype(f)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  this->ResNormThr()
         ,  precon
         );
      bool conv = p_solver->Solve(guess);

      // Set results
      this->SetConv(conv);
      this->SetNIter(p_solver->NumIter());
      this->SetExpVal(energy);
      this->SetLastResNorm(p_solver->LastResNorm());
      this->SetSolVecs(guess);

      return this->Conv();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   bool MvccSolver<PARAM_T, TRF_TMPL>::SolveAlternatingImpl
      (
      )
   {
      // The ModeCombiOpRange used _must_ include empty ModeCombi, but this is
      // handled in VibCorrSubspaceSolver::SetupMcr().
      const ModeCombiOpRange& mcr = this->Mcr();

      trf_t trf(this->NActiveModals(), this->NModals(), this->Oper(), this->ModInts(), mcr);
      trf.IoLevel() = this->IoLevel();
      trf.TimeIt() = this->TimeIt();
      param_t energy = 0;

      // Setup guess
      cont_t s;
      cont_t l;
      modal_cont_t modals;
      {
         sol_vec_t guess = this->StartGuess();
         s = std::move(std::get<0>(guess));
         l = std::move(std::get<1>(guess));
         modals = std::move(std::get<2>(guess));
      }
      // NB: Integrals will be updated during first residual calculation in main solver loop

      // The lambda doing the transformations for T
      auto f_s = [&trf,&mcr,&energy](const cont_t& s_trial) -> cont_t
      {
         // Compute error vector; then extract energy, set ref. elem to zero
         auto s_residual = trf.ErrVec(0, s_trial);
         energy = midas::tdvcc::ZeroFirstElemAndReturnIt(s_residual);

         // Zero all one-mode amplitudes
         auto s1norms = midas::tdvcc::ZeroOneModeAmplitudes(s_residual, mcr);

         // Return amplitude residuals
         return s_residual;
      };

      // The lambda doing the transformations for L (uses the s reference)
      auto f_l = [&trf,&mcr,&s](const cont_t& l_trial) -> cont_t
      {
         // Compute l_residual = eta + lA.
         auto l_residual = trf.EtaVec(0, s);
         Axpy(l_residual, trf.LJac(0, s, l_trial), param_t(+1));
         midas::tdvcc::ZeroFirstElemAndReturnIt(l_residual);

         // Zero all one-mode amplitudes
         auto l1norms = midas::tdvcc::ZeroOneModeAmplitudes(l_residual, mcr);

         // Output
         if (trf.IoLevel() > I_10)
         {
            Mout  << " max ||l1^m|| = " << *std::max_element(l1norms.begin(), l1norms.end()) << std::endl;
         }

         // Return amplitude residuals
         return l_residual;
      };

      // Modals residual for fixed amplitudes
      std::vector<mat_t> f_tilde_vec(this->NModes());
      auto f_modals = [&trf,&s,&l,&f_tilde_vec](const modal_cont_t& modals_trial) -> modal_cont_t
      {
         const auto& nmodes = modals_trial.size();

         // Update integrals for optimized modals
         trf.UpdateIntegrals(modals_trial);

         // Compute density matrices and mean fields
         auto mean_fields = trf.MeanFieldMatrices(0, s, l); // Returns {H1, FR, H1', FR'}
         auto densmats = trf.DensityMatrices(s, l);

         // Compute residuals
         modal_cont_t modals_residual(nmodes);
         for(In m=I_0; m<nmodes; ++m)
         {
            const auto& rho = densmats[m];
            const auto& u = modals_trial[m].first;
            const auto& w = modals_trial[m].second;

            // Q
            mat_t q = u*w;
            midas::mmv::WrapScale(q, param_t(-1));
            for(Uin alpha=I_0; alpha<q.Ncols(); ++alpha) q[alpha][alpha] += param_t(+1);

            mat_t f_check = mean_fields[m][0]*rho + mean_fields[m][1];
            mat_t fp_check = rho*mean_fields[m][2] + mean_fields[m][3];
            f_tilde_vec[m] = std::move(mat_t(w*f_check));
            const auto& f_tilde = f_tilde_vec[m];

            modals_residual[m] = std::make_pair(mat_t(q*f_check), mat_t(fp_check - f_tilde*w));  // In second residual we use the F-tilde = F'-tilde condition

            if (trf.IoLevel() > I_10)
            {
               mat_t fp_tilde = fp_check*u;
               Mout  << " mode " << m << "||F-tilde - F'-tilde|| = " << std::sqrt(DiffNorm2(f_tilde, fp_tilde)) << '\n'
                     << " u:\n" << u << '\n'
                     << " res_u:\n" << modals_residual[m].first << '\n'
                     << " w:\n" << w << '\n'
                     << " res_w:\n" << modals_residual[m].second << '\n'
                     << " F-check:\n" << f_check << '\n'
                     << " F'-check:\n" << fp_check << '\n'
                     << " Q:\n" << q << '\n'
                     << std::flush;
            }

         }

         return modals_residual;
      };

      // Setup preconditioner for amplitude equations if necessary (otherwise leave as dummy).
      cont_t precon;
      if (this->InvDiagJac0Precon())
      {
         cont_t inv_diag(this->InvDiagJac0(this->NActiveModals()));
         precon = std::move(inv_diag);
      }

      // Setup amplitude solvers
      auto p_s_solver = midas::nlsolver::SubspaceSolverFactory<decltype(f_s)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f_s
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  this->AmplsResNormThr()
         ,  precon
         );
      auto p_l_solver = midas::nlsolver::SubspaceSolverFactory<decltype(f_l)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f_l
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  this->AmplsResNormThr()
         ,  precon
         );

      // Modal solver (DIIS with no preconditioner performs best at the moment)
      auto p_modal_solver = midas::nlsolver::MakeDiisSolver
         (  f_modals
         ,  100         // Many subspace vectors
         ,  2000        // Many iterations
         ,  this->ModalsResNormThr()
         );

      // Preconditioner for modal equations
      //const auto precon_eps = this->PreconInvRegEpsilon();
      //auto modals_precon = [&f_tilde_vec,&precon_eps](modal_cont_t& uw)
      //{
      //   auto nmodes = f_tilde_vec.size();
      //   if (  uw.size() != nmodes
      //      )
      //   {
      //      MIDASERROR("Size mismatch in MVCC modal-equations preconditioner.");
      //   }

      //   for(In imode=I_0; imode<nmodes; ++imode)
      //   {
      //      auto& u = uw[imode].first;
      //      auto& w = uw[imode].second;

      //      mat_t f_tilde_inv = midas::math::RegularizedInverse(midas::math::inverse::xsvd, f_tilde_vec[imode], precon_eps);

      //      mat_t u_precon = u*f_tilde_inv;
      //      u = std::move(u_precon);
      //      mat_t w_precon = f_tilde_inv*w;
      //      w = std::move(w_precon);
      //   }
      //};

      //// Modal solver
      //auto p_modal_solver = midas::nlsolver::MakeNonHermitianPreconDiisSolver
      //   (  f_modals
      //   ,  this->SubspaceDim()
      //   ,  this->MaxIter()
      //   ,  this->ModalsResNormThr()
      //   ,  modals_precon
      //   ,  false                      // Do not apply preconditioner in reduced system
      //   );


      /********************************************
       * Main solver loop begin
       *******************************************/
      if(this->IoLevel() > I_1) Mout << " ========= Starting MVCC solver =========" << std::endl;
      Uin it_macro = 0;
      bool conv = false;
      absval_t resid_norm = 1;
      while(true)
      {
         // Check convergence for all
         auto resid_norm2 = midas::mmv::WrapNorm2(f_modals(modals));
         resid_norm2 += midas::mmv::WrapNorm2(f_s(s));
         resid_norm2 += midas::mmv::WrapNorm2(f_l(l));
         resid_norm = std::sqrt(resid_norm2);
         conv = resid_norm < this->ResNormThr();
         if(this->IoLevel() > I_5)
         {
            Mout  << "    === Iteration " << it_macro << '\n'
                  << "       |res|:          " << resid_norm << std::endl;
         }

         if (  conv
            || it_macro >= this->MaxIter()
            )
         {
            break;
         }

         // Solve T equations
         bool s_conv = p_s_solver->Solve(s);
         if(this->IoLevel() > I_5 || !s_conv)
         {
            Mout  << "       energy:         " << energy << '\n'
                  << "       s_conv:         " << std::boolalpha << s_conv << std::noboolalpha << std::endl;
         }

         // Solve L equations
         bool l_conv = p_l_solver->Solve(l);
         if(this->IoLevel() > I_5 || !l_conv)
         {
            Mout  << "       l_conv:         " << std::boolalpha << l_conv << std::noboolalpha << std::endl;
         }

         // Solve modal equations
         bool modals_conv = p_modal_solver->Solve(modals);
         if(this->IoLevel() > I_5 || !modals_conv)
         {
            Mout  << "       modals_conv:    " << std::boolalpha << modals_conv << std::noboolalpha << std::endl;
         }

         // incr iter
         ++it_macro;
      }
      /********************************************
       * Main solver loop end
       *******************************************/
      if(this->IoLevel() > I_1)
      {
         Mout  << " ========= MVCC solver done =========" << '\n'
               << "    E(MVCC):           " << energy << '\n'
               << "    |res|:             " << resid_norm << '\n'
               << "    N_iter:            " << it_macro << '\n'
               << " ====================================" << std::endl;
      }

      // Set results
      this->SetConv(conv);
      this->SetNIter(it_macro);
      this->SetExpVal(energy);
      this->SetLastResNorm(resid_norm);
      sol_vec_t solution = { std::move(s), std::move(l), std::move(modals) };
      this->SetSolVecs(solution);

      return this->Conv();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   typename MvccSolver<PARAM_T, TRF_TMPL>::sol_vec_t
   MvccSolver<PARAM_T, TRF_TMPL>::StartGuess
      (
      )  const
   {
      auto cont_size = this->SizeParamCont();

      cont_t t(cont_size, param_t(0));
      cont_t l(cont_size, param_t(0));

      modal_cont_t modals(this->NModes());
      for(Uin m=0; m<this->NModes(); ++m)
      {
         const auto& nprim = this->NModals()[m];
         const auto& nact = this->NActiveModals()[m];
         mat_t u(nprim, nact, param_t(0));
         u.Unit();
         mat_t w = Transpose(u);
         modals[m] = std::make_pair(std::move(u), std::move(w));
      }

      return std::make_tuple(std::move(t), std::move(l), std::move(modals));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   GeneralMidasVector<typename MvccSolver<PARAM_T, TRF_TMPL>::param_t>
   MvccSolver<PARAM_T, TRF_TMPL>::FvciVecImpl
      (  bool aBraNotKet
      )  const
   {
      MIDASERROR("Not implemented!");
      return GeneralMidasVector<param_t>();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   std::string MvccSolver<PARAM_T, TRF_TMPL>::CorrType
      (
      )  const
   {
      return midas::tdvcc::StringFromEnum(midas::tdvcc::CorrType::TDMVCC);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   std::vector<std::string> MvccSolver<PARAM_T, TRF_TMPL>::PrivGetFileDescriptors
      (
      )  const
   {
      return mvcc::GetFileDescriptors();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   void MvccSolver<PARAM_T, TRF_TMPL>::PrintSolVecAnalysis
      (  std::ostream& arOs
      ,  In aIndent
      )  const
   {
      const std::string tab(aIndent, ' ');
      const std::string tab2(aIndent + 3, ' ');
      const std::string eq = " = ";
      const Uin w = 33;
      const Uin w2 = 33 - 3;

      arOs << tab2 << std::setw(w2) << "Num. sol. vecs." << eq << std::tuple_size_v<sol_vec_t> << std::endl;

      const auto& sol = this->template SolVecs<sol_vec_t>();

      arOs  << tab2 << std::setw(w2) << std::string("Norm(T)")
            << eq
            << Norm(std::get<0>(sol)) 
            << '\n'
            << tab2 << std::setw(w2) << std::string("Norm(L)")
            << eq
            << Norm(std::get<1>(sol)) 
            << std::endl;
      const auto& modals = std::get<2>(sol);
      absval_t max_nonortho_err = 0;
      for(const auto& modals_m : modals)
      {
         mat_t wu = modals_m.second * modals_m.first;
         MidasAssert(wu.IsSquare(), "W*U must be square!");
         auto len = wu.Ncols();
         for(In i=I_0; i<len; ++i)
         {
            wu[i][i] -= param_t(1.0);
         }
         max_nonortho_err = std::max(max_nonortho_err, wu.Norm());
      }
      arOs  << tab2  << std::setw(w2) << std::string("Max non-biorthonormality error")
            << eq
            << max_nonortho_err
            << std::endl;;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   void MvccSolver<PARAM_T, TRF_TMPL>::WriteSolVecsToFile
      (  std::ostream& arOs
      ,  const std::string& arBaseName
      ,  const Uin aIndent
      )  const
   {
      const auto& sol = this->template SolVecs<sol_vec_t>();
      constexpr size_t nfiles = 4;

      const auto old_flags = arOs.flags();
      arOs << std::left;
      const std::string base = midas::input::DeleteBlanks(arBaseName);

      const std::string tab(aIndent, ' ');
      arOs << tab << "Writing solution vectors to file:\n";

      const std::vector<std::string> v_descriptors = this->PrivGetFileDescriptors();

      const std::string file_s = base + "_" + v_descriptors.at(0);
      if(this->WriteSolVecToFile(std::get<0>(sol), file_s))
      {
         arOs
            << tab
            << " - " << std::setw(20) << v_descriptors.at(0)
            << "(file: " << file_s << "_0)"
            << '\n'
            ;
      }

      const std::string file_l = base + "_" + v_descriptors.at(1);
      if(this->WriteSolVecToFile(std::get<1>(sol), file_l))
      {
         arOs
            << tab
            << " - " << std::setw(20) << v_descriptors.at(1)
            << "(file: " << file_l << "_0)"
            << '\n'
            ;
      }

      for(const auto& u : {true, false})
      {
         auto descr_idx = u ? 2 : 3;
         const std::string file = base + "_" + v_descriptors.at(descr_idx);
         if(this->WriteSolVecToFile(std::get<2>(sol), file, u))
         {
            arOs
               << tab
               << " - " << std::setw(20) << v_descriptors.at(descr_idx)
               << "(file: " << file << "_0)"
               << '\n'
               ;
         }
      }

      arOs << std::flush;
      arOs.flags(old_flags); 
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   Uin MvccSolver<PARAM_T, TRF_TMPL>::SizeParamCont
      (
      )  const
   {
      Uin n = 0;
      for(const auto& mc: this->Mcr())
      {
         n += NumParams(mc, this->NActiveModals());
      }
      return n;
   }

} /* namespace midas::vcc::subspacesolver */

#endif /* MVCCSOLVER_IMPL_H_INCLUDED */

