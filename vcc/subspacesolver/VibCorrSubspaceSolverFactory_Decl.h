/**
 *******************************************************************************
 * 
 * @file    VibCorrSubspaceSolverFactory_Decl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VIBCORRSUBSPACESOLVERFACTORY_DECL_H_INCLUDED
#define VIBCORRSUBSPACESOLVERFACTORY_DECL_H_INCLUDED

// Standard headers.
#include <memory>
#include <vector>

// Midas headers.
#include "vcc/subspacesolver/VibCorrSubspaceSolver.h"
#include "td/tdvcc/TdvccEnums.h"

// Forward declarations.


namespace midas::vcc::subspacesolver
{
   // Forward declarations.
   template<typename> class VibCorrSubspaceSolver;

   //!
   template
      <  typename PARAM_T
      >
   std::unique_ptr<VibCorrSubspaceSolver<PARAM_T>> VibCorrSubspaceSolverFactory
      (  midas::tdvcc::CorrType
      ,  midas::tdvcc::TrfType
      ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t
      ,  typename VibCorrSubspaceSolver<PARAM_T>::n_modals_t
      ,  const typename VibCorrSubspaceSolver<PARAM_T>::opdef_t&
      ,  const typename VibCorrSubspaceSolver<PARAM_T>::modalintegrals_t&
      ,  const ModeCombiOpRange&
      );

} /* namespace midas::vcc::subspacesolver */

#endif/*VIBCORRSUBSPACESOLVERFACTORY_DECL_H_INCLUDED*/
