#ifndef NHBANDLANCZOSCHAIN_H
#define NHBANDLANCZOSCHAIN_H

// Standard Headers
#include <time.h>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "mmv/DataCont.h"
#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"

class NHBandLanczosChainDef : public LanczosChainDefBase
{
   protected:
      In             mLength;
      In             mRBlock;
      In             mLBlock;
      In             mOrtho;
      bool           mSave;
      int            mSaveInterval;
      Nb             mDtol;
      Nb             mConv;
      string         mDiagMethod;
      vector<string> mOpers;
      bool           mDeflate;
      bool           mTestChain;

   public:
      ///> Constructor
      NHBandLanczosChainDef(In aLength, In aRBlock, In aLBlock, In aOrtho, bool aSave, int aSaveInterval, Nb aDtol,
                            Nb aConv=I_0, bool aDeflate=true);

      ///> Function for returning values in NHBandLanczosChainDef
      const In& GetLength() const             {return mLength;}
      const In& GetRBlock() const             {return mRBlock;}
      const In& GetLBlock() const             {return mLBlock;}
      const Nb& GetDtol() const               {return mDtol;}
      const Nb& GetConv() const               {return mConv;}
      const In& GetOrtho() const              {return mOrtho;}
      const bool& GetSave() const             {return mSave;}
      int GetSaveInterval() const             {return mSaveInterval;}
      const vector<string>& GetOpers() const  {return mOpers;}
      const string& GetOper(const In i) const {return mOpers[i];}
      const string& GetDiagMethod() const     {return mDiagMethod;}
      const bool& GetDeflate() const          {return mDeflate;}
      const bool& GetTest() const             {return mTestChain;}

      ///> Other Functions
      void AddOper(const string& aOper)               {mOpers.push_back(aOper);}
      void SetDiagMethod(const string& aDiagMethod)   {mDiagMethod = aDiagMethod;}
      void SetLength(In aLength)                      {mLength = aLength;}
      void SetRBlock(In aRBlock)                      {mRBlock = aRBlock;}
      void SetLBlock(In aLBlock)                      {mLBlock = aLBlock;}

      bool Combine(const NHBandLanczosChainDef& aOther) {return false;}
      bool Compatible(const NHBandLanczosChainDef& aOther) const;
      ///< Returns true if aOther is a subset of or identical to this definition.

      ///> Output operator
      friend ostream& operator<<(ostream& aOut, const NHBandLanczosChainDef& aDef);      
};

class NHBandLanczosChain
{
   protected:
      NHBandLanczosChainDef      mDef;
      MidasMatrix                mTmatRight;
      MidasMatrix                mTmatLeft;
      vector<DataCont>           mV;
      vector<DataCont>           mW;
      vector<DataCont>           mEta;
      vector<DataCont>           mXi;
      Vcc*                       mpVcc;
      VccTransformer*            mpTrf;
      string                     mFilePrefix;
      vector<Nb>                 mDelta;
      In                         mRandSeed;

      Nb   RandomNb();
      void CalcOrtho();
      void OutputRT(const vector<Nb>&);
      void TestResidual(const In ItNum, const In anc);
      void TestRT(const In ItNum, const In anc, const vector<Nb>& aDelta);
      void TestFullSpace(const In ItNum); 
      
      void PrintTmatrices(In) const;
      void PrintLanczosVectors(In);
      void PrintDelta(In) const;
      
   public:
      ///> Constructor
      NHBandLanczosChain(const NHBandLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf);
      
      In GetRBlock() const          {return mDef.GetRBlock();}
      In GetLBlock() const          {return mDef.GetLBlock();}
      In GetLength() const          {return mDef.GetLength();}
      string GetDiagMethod() const  {return mDef.GetDiagMethod();}
      string GetName() const        {return mFilePrefix;}
      In GetNumEta() const          {return mEta.size();}
      
      void MakeStartVectors(const In nc, const vector<string>& aOpers);

      void NHBandDiag(In aChainLen, MidasMatrix& aRightEigVals, MidasVector& aReEigVals, MidasVector& aImEigVals, MidasMatrix& aLeftEigVecs) const;
      void CheckEigVecs(MidasMatrix& aReRightEigVecs, MidasMatrix& aImRightEigVecs,
            MidasMatrix& aReLeftEigVecs, MidasMatrix& aImLeftEigVecs, MidasVector& aReEigVals,
            MidasVector& aImEigVals) const;
      
      void GetRightStartVector(const In aVec, DataCont& aV) const;
      void GetLeftStartVector(const In aVec, DataCont& aW) const;
      void GetXiVector(const In aVec, DataCont& aXi) const;
      void GetEtaVector(const In aVec, DataCont& aEta) const;
      const vector<Nb>* GetDelta() const;
      MidasMatrix GetV() const;
      
      void FullOrtho(DataCont& aVvec, DataCont& aWvec, const In aJ, const In pc, const In mc, 
                     const In repeat, bool& aBreakdown, const vector<Nb>& aDelta);
              
      bool Compatible(const NHBandLanczosChainDef& aDef) const {return mDef.Compatible(aDef);}
      bool Restart(In& aJ, In& aPc, In& aMc, bool& aFullyDeflated);
      void CleanOldSave() const;
      void SaveChain(const std::vector<In>&, const std::vector<In>&);

      void TransformRight(DataCont& avj, const In anc, const In aJ, const In aType);
      void TransformLeft(DataCont& awj, const In anc, const In aJ, const In aType);
      void TransformStoX(const MidasVector& aEigVec, DataCont& aX) const;
      void Evaluate();

      void Diagonalize(MidasMatrix& aMat, MidasMatrix& aRightEigVecs, MidasMatrix& aLeftEigVecs,
                       MidasVector& aReEigVals, MidasVector& aImEigVals);

};

#endif ///< NHBANDLANCZOSCHAIN_H
