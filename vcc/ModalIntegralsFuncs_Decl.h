/**
 *******************************************************************************
 * 
 * @file    ModalIntegralsFuncs_Decl.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Utility functions related to the ModalIntegrals class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODALINTEGRALSFUNCS_DECL_H_INCLUDED
#define MODALINTEGRALSFUNCS_DECL_H_INCLUDED

#include "vcc/ModalIntegrals.h"

// Forward declarations.
class Vscf;
class Vcc;
template<typename> class GeneralDataCont;
using DataCont = GeneralDataCont<Nb>;
namespace midas::td
{
   template
      <  typename
      ,  template<typename> typename
      ,  template<typename> typename
      >
   class PrimitiveIntegrals;
}  /* namespace midas::td */


//! Utility functions related to the ModalIntegrals class.
namespace midas::vcc::modalintegrals
{
   //@{
   //! Copies/moves integrals from a PrimitiveIntegrals object.
   template<typename T, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  const midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&
      );
   template<typename T, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&&
      );
   //@}

   //! Transform primitive->modal integrals using argument modal coefficients.
   template<typename T, typename U, template<typename> class VEC_T, template<typename> class MAT_T>
   ModalIntegrals<T> ConstructModalIntegrals
      (  midas::td::PrimitiveIntegrals
            <  T
            ,  VEC_T
            ,  MAT_T
            >&
      ,  const GeneralDataCont<U>& arModals
      ,  const std::vector<In>& arModalOffsets
      ,  const std::vector<In>& arModalBasisLimits
      );

   //! Construct modal integrals for elementary operator.
   template<typename T>
   ModalIntegrals<T> ConstructModalIntegralsElemOper
      (  Uin aElemOperMode
      ,  Uin aElemOperP
      ,  Uin aElemOperQ
      ,  const std::vector<Uin>& arNModals
      ,  const std::vector<Uin>& arNOpers
      );

   //! Construct modal integrals from contents of Vscf object.
   template<typename T>
   ModalIntegrals<T> InitFromVscf
      (  const Vscf&
      ,  const std::string& arName = ""
      );

   //! Construct modal overlap matrices.
   template<typename T>
   ModalIntegrals<T> InitOverlaps
      (  const std::vector<GeneralMidasMatrix<T>>& arLeftModals
      ,  const std::vector<GeneralMidasMatrix<T>>& arRightModals
      ,  const std::vector<GeneralMidasMatrix<T>>& arPrimOverlaps
      );

   //! Construct modal overlap matrices from contents of Vcc object.
   ModalIntegrals<Nb> InitOverlapsFromVcc
      (  const Vcc& arVcc
      ,  const DataCont& arLeftModals
      ,  const std::string& arName = ""
      );

   //! Initialize to one-mode Fock integrals over different coupling levels.
   ModalIntegrals<Nb> InitFockFromVcc
      (  Vcc& arVcc
      ,  const std::string& arName = ""
      );

   //! For implementation details.
   namespace detail
   {
      //! Construct modal integrals from contents of Vscf object.
      template<typename T>
      ModalIntegrals<T> InitFromVscfImpl
         (  const Vscf&
         );

      //! Uniform interface for real->real/complex conversion.
      template<typename T>
      struct ConvertFromReal
      {
         static ModalIntegrals<T> Convert(const ModalIntegrals<T>&);
      };
      template<typename T>
      struct ConvertFromReal<std::complex<T>>
      {
         static ModalIntegrals<std::complex<T>> Convert(const ModalIntegrals<T>&);
      };

   } /* namespace detail */
} /* namespace midas::vcc::modalintegrals::detail */



#endif/*MODALINTEGRALSFUNCS_DECL_H_INCLUDED*/
