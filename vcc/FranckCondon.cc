/**
************************************************************************
* 
* @file                FranckCondon.cc
*
* Created:             15-04-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Calculation of Franck-Condon factors
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "vcc/Vcc.h"
#include "vcc/VccTransformer.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "input/Input.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"

In Vcc::FCGetOtherCalcDef()
{
   const string& other_calc = mpVccCalcDef->FCOtherCalc();
   Mout << " Other calculation: " << other_calc << endl
        << " Searching for other calculation... ";

   In i_calc;
   for (i_calc=I_0; i_calc<gVccCalcDef.size(); ++i_calc)
   {
      string name = gVccCalcDef[i_calc].GetName();
      std::transform(name.begin(), name.end(), name.begin(), static_cast<int (*)(int)>(toupper));
      string::size_type idx = name.find(other_calc);
      if (idx != string::npos)
      {
         Mout << "Found " << name << endl;
         break;
      }
   }
   if (i_calc == gVccCalcDef.size())
   {
      Mout << "Not found." << endl;
      MIDASERROR(" FranckCondon: Other calculation not found.");
   }
   
   // Check if calculation is done, if same basis is used etc.
   bool ok = true;
   Mout << "    Checking if calculation is done:    ";
   if (gVccCalcDef[i_calc].Done())
      Mout << "ok" << endl;
   else
   {
      ok = false;
      Mout << "No" << endl;
   }
   Mout << "    Same primitive basis used:          ";
   if (gVccCalcDef[i_calc].Basis() == mpVccCalcDef->Basis())
      Mout << "yes: " << mpVccCalcDef->Basis() << endl;
   else
   {
      ok = false;
      Mout << "No" << endl;
   }
   Mout << "    Checking sizes of modal basis sets: ";
   for (In m=I_0; m<pOneModeInt()->GetmNoModesInInt(); ++m)
      if (mpVccCalcDef->Nmodals(m) != gVccCalcDef[i_calc].Nmodals(m))
      {
         ok = false;
         Mout << "Mismatch at mode: " << m << endl;
         break;
      }
   if (ok)
      Mout << "All modes match" << endl;

   if (!ok)
   {
      Mout << endl;
      MIDASERROR("Franck-Condon: Error.");
   }

   return i_calc;
}

In Vcc::FCGetOperIdx()
{
   const string& oper_name = mpVccCalcDef->FCOperator();
   In oper;
   if (oper_name.empty())
   {
      Mout << " No operator specified. Calculate simple overlaps." << endl;
      oper = -I_1;
   }
   else
   {
      Mout << " Searching for operator " << oper_name << ": ";
      for (oper=I_0; oper<gOperatorDefs.GetNrOfOpers(); ++oper)
      {
         string s = gOperatorDefs[oper].Name();
         std::transform(s.begin(), s.end(), s.begin(), static_cast<int (*)(int)>(toupper));
         if (s == oper_name)
            break;
      }
      
      if (oper == gOperatorDefs.GetNrOfOpers())
      {
         Mout << "Not found" << endl;
         MIDASERROR("Franck-Condon: Operator not found");
      }
      else
         Mout << "Found." << endl;
   }
   return oper;
} 

void Vcc::FranckCondon()
{
   Mout << " Calculation of Franck-Condon factors." << endl;
   Out72Char(Mout,'+','-','+');
   Mout << endl;

   In other_calc = FCGetOtherCalcDef();
   In oper = FCGetOperIdx();

   // Check that the "OtherCalc" operator does not contain kinetic energy operator terms
   const auto& other_calc_oper_name = gOperatorDefs[other_calc].Name();
   if (  gOperatorDefs[other_calc].GetOperWithKeoTerms()
      && ! OpDef::msOpInfo[other_calc_oper_name].GetType().Is(oper::energy)
      )
   {
      MIDASERROR("Kinetic energy operator terms detected in non-energy operator " + other_calc_oper_name + ", please investigate");
   }
  
   // Check that the operator does not contain kinetic energy operator terms
   if (oper != -I_1)
   {
      const auto& oper_name = gOperatorDefs[oper].Name();
      if (  gOperatorDefs[oper].GetOperWithKeoTerms()
         && ! OpDef::msOpInfo[oper_name].GetType().Is(oper::energy)
         )
      {
         MIDASERROR("Kinetic energy operator terms detected in non-energy operator " + oper_name + ", please investigate");
      }
   }

   VccTransformer trf(this, mpVccCalcDef, pOpDef(), mOneModeModalBasisInt.get(), mEvmp0);
   In n_coefs = trf.NexciXvec();          // The small space.

   // Calculate sigma, i.e. the transformation of each eigenvector with the operator.
   In n_roots = mpVccCalcDef->VciRoots();
   DataCont* sigma = new DataCont[n_roots];
   if (-I_1 == oper)
   {
      // Identity operator. Sigma equals coefficient vectors.
      for (In i=I_0; i<n_roots; ++i)
      {
         string s = mpVccCalcDef->GetName() + "_Vci_vec_" + std::to_string(i);
         sigma[i].GetFromExistingOnDisc(n_coefs, s);
         sigma[i].SaveUponDecon();
      }
   }
   else
   {
      // Get basis used for this calc.
      string s_basis = mpVccCalcDef->Basis();
      In i_basis = I_0;
      for (In i=I_0; i<gBasis.size(); ++i)
         if (gBasis[i].GetmBasName() == s_basis)
         {
            i_basis = i;
            break;
         }

      // Update primitive integrals.
      string storage = "InMem";
      if (I_0 != mpVccCalcDef->IntStorage())
         storage = "OnDisc";
      OneModeInt one_mode_int(&gOperatorDefs[oper], &gBasis[i_basis], storage, true);
      one_mode_int.CalcInt();

      trf.PutOperatorToWorkingOperatorAndUpdateModalIntegrals(this, &gOperatorDefs[oper], &one_mode_int);

      // The result of the transformation with the operator has to go out of space
      // since the Hartree products "out there" may have nonzero overlap with products
      // inside the space.
      trf.InitOOSTransXvec();
      In n_sigma = trf.NexciTransXvec();
      
      for (In i=I_0; i<n_roots; ++i)
      {
         // Get coefficients.
         DataCont coefs;
         string s = mpVccCalcDef->GetName() + "_Vci_vec_" + std::to_string(i);
         coefs.GetFromExistingOnDisc(n_coefs, s);
         coefs.SaveUponDecon();
        
         sigma[i].SetNewSize(n_sigma);
         Nb zero = C_0;
         trf.TransH(coefs, sigma[i], I_1, I_0, zero);
      }

      trf.RestoreOrigOperAndUpdateModalIntegrals(this);
   }

   // Init overlap integrals.
   DataCont dc_left_modals;
   dc_left_modals.GetFromExistingOnDisc(pModals()->Size(),
                                        gVccCalcDef[other_calc].GetName() + "_Modals");
   dc_left_modals.SaveUponDecon();
   const ModalIntegrals<Nb> olp_igrl = midas::vcc::modalintegrals::InitOverlapsFromVcc(*this, dc_left_modals, "overlaps");

   // Calculate gamma, i.e. change to modal basis of other calculation.
   DataCont* gamma = new DataCont[n_roots];
   for (In i=I_0; i<n_roots; ++i)
      gamma[i].SetNewSize(n_coefs);
   In n_modes = pOpDef()->NmodesInOp();
   vector<In> all_modes(n_modes);
   for (In m=I_0; m<n_modes; ++m)
      all_modes[m] = m;
   ModeCombi mc_all(all_modes, -I_1);

   const Xvec& xvec = trf.GetXvec();             // Small space for gamme.
   const Xvec& transxvec = trf.GetTransXvec();   // Large space for sigma.
   for (In i_lmc=I_0; i_lmc<xvec.NexciTypes(); ++i_lmc)
   {
      const ModeCombi& lmc = xvec.GetModeCombi(i_lmc);
      MidasVector* res = new MidasVector[n_roots];
      for (In i=I_0; i<n_roots; ++i)
      {
         res[i].SetNewSize(xvec.NexciForModeCombi(i_lmc));
         res[i].Zero();
      }
      
      for (In i_rmc=I_0; i_rmc<transxvec.NexciTypes(); ++i_rmc)
      {
         const ModeCombi& rmc = transxvec.GetModeCombi(i_rmc);

         ModeCombi landr;
         ModeCombi notlr;
         ModeCombi lnotr;
         ModeCombi lcmnr;
         ModeCombi rnotl;
         landr.Union(lmc, rmc);
         notlr.InFirstOnly(mc_all, landr);
         lnotr.InFirstOnly(lmc, rmc);
         lcmnr.Intersection(lmc, rmc);
         rnotl.InFirstOnly(rmc, lmc);
         // Calculate ii overlaps.
         Nb ii_olp = C_1;
         for (In i=I_0; i<notlr.Size(); ++i)
         {
            ii_olp *= olp_igrl.GetElement(notlr.Mode(i), I_0, I_0, I_0);
         }
         
         // Calculate ia overlaps.
         In addr = rmc.Address();
         In n_exci = transxvec.NexciForModeCombi(i_rmc);
         MidasVector* vec_in = new MidasVector[n_roots];
         MidasVector* vec_out = new MidasVector[n_roots];
         ModeCombi mc_in(rmc);
         ModeCombi mc_out(rmc);
         for (In i=I_0; i<n_roots; ++i)
         {
            vec_in[i].SetNewSize(n_exci);
            vec_out[i].SetNewSize(n_exci);
            sigma[i].DataIo(IO_GET, vec_in[i], n_exci, addr);
         }
         
         for (In i=I_0; i<rnotl.Size(); ++i)
         {
            In mode = rnotl.Mode(i);
            mc_out.RemoveMode(mode);
            In nmod = mpVccCalcDef->Nmodals(mode);
            for (In j=I_0; j<n_roots; ++j)
            {
               vec_out[j].SetNewSize(vec_in[j].Size() / (nmod-I_1));
               vec_out[j].Zero();
               olp_igrl.Contract(mc_out, mc_in, mode, I_0, vec_out[j], vec_in[j], -I_1);
               vec_in[j].SetNewSize(vec_out[j].Size());
               vec_in[j] = vec_out[j];
            }
            mc_in.RemoveMode(mode);
         }

         // Calculate ab overlaps.
         for (In i=I_0; i<lcmnr.Size(); ++i)
         {
            In mode = lcmnr.Mode(i);
            for (In j=I_0; j<n_roots; ++j)
            {
               vec_out[j].Zero();
               olp_igrl.Contract(mc_out, mc_in, mode, I_0, vec_out[j], vec_in[j], I_0);
               vec_in[j] = vec_out[j];
            }
         } 

         // Calculate ai overlaps.
         for (In i=I_0; i<lnotr.Size(); ++i)
         {
            In mode = lnotr.Mode(i);
            mc_out.InsertMode(mode);
            In nmod = mpVccCalcDef->Nmodals(mode);
            for (In j=I_0; j<n_roots; ++j)
            {
               vec_out[j].SetNewSize(vec_in[j].Size() * (nmod-I_1));
               vec_out[j].Zero();
               olp_igrl.Contract(mc_out, mc_in, mode, I_0, vec_out[j], vec_in[j], I_1);
               vec_in[j].SetNewSize(vec_out[j].Size());
               vec_in[j] = vec_out[j];
            }
            mc_in.InsertMode(mode);
         }

         for (In i=I_0; i<n_roots; ++i)
            res[i] += ii_olp * vec_in[i];
         delete[] vec_in;
         delete[] vec_out;
      }
      
      // Save to gamma vectors.
      In gamma_addr = lmc.Address();
      In gamma_exci = xvec.NexciForModeCombi(i_lmc);
      for (In i=I_0; i<n_roots; ++i)
         gamma[i].DataIo(IO_PUT, res[i], gamma_exci, gamma_addr);
      delete[] res;
   }
   delete[] sigma;
  
   // Calculate overlaps.
   Mout << endl
        << " Left =  " << gVccCalcDef[other_calc].GetName() << endl
        << " Right = " << mpVccCalcDef->GetName() << endl
        << " Left:     Right:    Overlap:                   Overlap^2:" << endl;
   string left_base = gVccCalcDef[other_calc].GetName() + "_Vci_vec_";
   //Newpart start
   ofstream file_stream("FranckCondonData.dat");
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   //Newpart end
   Nb lefteigval0 = gVccCalcDef[other_calc].GetVciEigVal(I_0);
   Nb righteigval0 = mpVccCalcDef->GetVciEigVal(I_0);
   file << "f(x)=";
   for (In left=I_0; left<gVccCalcDef[other_calc].VciRoots(); ++left)
   {
      Nb lefteigval = gVccCalcDef[other_calc].GetVciEigVal(left);
      string left_name = left_base + std::to_string(left);
      DataCont left_coefs;
      left_coefs.GetFromExistingOnDisc(n_coefs, left_name);
      left_coefs.SaveUponDecon();
      for (In right=I_0; right<n_roots; ++right)
      {
         Nb righteigval = mpVccCalcDef->GetVciEigVal(right);
         Nb fc = Dot(left_coefs, gamma[right]);
         Mout << setw(5) << left << setw(11) << right
              << setw(27) << fc
              << setw(27) << fc*fc << endl;
         file << fc*fc << "*exp((-(x-" << fabs((righteigval-righteigval0-(lefteigval-lefteigval0))*C_AUTKAYS) << ")**2)/400)+";
      }
   }
   file << endl;
   file_stream.close();
   delete[] gamma;
}
