/**
************************************************************************
* 
* @file                BandLanczosIRspect.cc
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen (mrgodtliebsen@hotmail.com)
*
* Short Description:   Implementation of BandLanczosIRspect class.
* 
* Last modified: Thu Dec 09, 2010  04:24PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "util/Io.h"
#include "input/Input.h"
#include "LanczosSpectBlock.h"
#include "BandLanczosIRspect.h"
#include "LanczosRspFunc.h"
#include "BandLanczosLinRspFunc.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"
#include "mmv/MidasMatrix.h"
#include "util/MidasSystemCaller.h"
#include "mpi/Impi.h"

///> Known from input/OpInfo.h
///>const In OPTYPE_X_DIPOLE        = 101;
///>const In OPTYPE_Y_DIPOLE        = 102;
///>const In OPTYPE_Z_DIPOLE        = 103;

BandLanczosIRspect::BandLanczosIRspect(const string& aName, const string& aBlrsp,
                                       const bool aPrintIR, const Nb aWeightLim, 
                                       const Nb aPeakLim, const string& aAnalyze="true"):
mName(aName), mBlrsp(aBlrsp), mPrintIRTable(aPrintIR), mWeightLimit(aWeightLim), mPeakLimit(aPeakLim)
{
   mName = gAnalysisDir+"/"+mName;
   
   string analyze = aAnalyze;
   transform(analyze.begin(), analyze.end(), analyze.begin(), (int (*)(int))std::toupper);
   if(analyze == "")
      mAnalyze = false;
   else if (analyze == "MINIMA" || analyze == "TRUE" || analyze == "USER")
      mAnalyze = true;
   else
      MIDASERROR(" #4 BandLanczosIR: ANALYZE parameter unknown.");
}
              

void BandLanczosIRspect::Create(VccCalcDef* aVccCalcDef, Vcc* aVcc)
{
   Mout << " Creating Band Lanczos IR spectrum with name: '" << mName << "'" << endl;
   
   BandLanczosLinRspFunc *p_blrf=dynamic_cast<BandLanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mBlrsp));
   Nb Gamma = p_blrf->GetGamma();

   vector<MidasMatrix>* ImLinRsp;
   ImLinRsp = p_blrf->GetImLinRsp();
   vector<Nb>* FrqRange;
   FrqRange = p_blrf->GetFrqRange();
   vector<string>* Opers;
   Opers = p_blrf->GetOpers();
   

   Nb FrqStart=p_blrf->GetFrqStart();
   Nb FrqEnd=p_blrf->GetFrqEnd();
   Nb FrqStep=p_blrf->GetFrqStep();

   vector<In> OperIndex;

   MidasVector ints(FrqRange->size());

   //Setup output file
   string filename = mName + ".dat";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << "# IR absorption spectrum generated using Band Lanczos chain " << mBlrsp << "."  << endl
        << "# Gamma (damping factor): " << Gamma*C_AUTKAYS << " cm-1" << endl
        << "# Columns: freq. (cm-1)   Absorption (arb. units)   "
        << "Abs (xdip)   Abs (ydip)   Abs (zdip)" << endl;

   file.precision(16);
   
   FindOpers(OperIndex, Opers);

   if(OperIndex.size() == I_3)
   {
      for(In i=0; i<FrqRange->size(); i++)
      {
         for(In j=0; j<OperIndex.size(); j++)
         {
            Nb operint = (*FrqRange)[i]*(*ImLinRsp)[i][OperIndex[j]][OperIndex[j]];
            ints[i] += operint;
         }
         file << (*FrqRange)[i]*C_AUTKAYS << "   " << ints[i] << endl;
      }


      GenerateGnuPlot();
      IdentifyPeaks(ints, FrqStart, FrqEnd, FrqStep);

      if(mAnalyze)
         AnalyzeSpect(aVccCalcDef, aVcc, ints, FrqStart, FrqEnd, FrqStep, OperIndex, Opers);   
   
   }
   else
   {
      MIDASERROR("You need X, Y, and Z Dipole operators to calculate a Band Lanczos IR spectrum.");
   }
}

void BandLanczosIRspect::FindOpers(vector<In>& aOperIndex, vector<string>* aOpers)
{

   for(In i=I_0; i<(*aOpers).size(); i++)
   {
      for(In j=I_0; j<gOperatorDefs.GetNrOfOpers(); j++)
      {
         if(gOperatorDefs[j].Name() == (*aOpers)[i])
         {
            if(OpDef::msOpInfo[gOperatorDefs[j].Name()].GetType().Is(oper::dipole))
            {
               aOperIndex.push_back(i);
            }
            else
            {
               Mout  << "Skipping operator with name: " << (*aOpers)[i]
                     << " in the calculation of the Band Lanczos IR Spectrum." << endl;
            }
         }
      }
   }
}

void BandLanczosIRspect::GenerateGnuPlot()
{
    string filename = mName + ".plot";
    ofstream file_stream(filename.c_str(), ios_base::trunc);
    MidasStreamBuf file_buf(file_stream);
    MidasStream file(file_buf);
    
    file << "set term postscript enhanced" << endl
         << "set output \"" << mName << ".eps\"" << endl << endl
         << "set xlabel \"Frequency (cm^{-1})\"" << endl
         << "set ylabel \"Intensity (arb. units)\"" << endl << endl
         << "plot \"" << mName << ".dat\" u 1:2 w lines" << endl;
    file_stream.close();
         
   if(midas::mpi::IsMaster())
   {
      system_command_t cmd = {"gnuplot", mName + ".plot"};
      MIDASSYSTEM(cmd);
   }
}

void BandLanczosIRspect::IdentifyPeaks(const MidasVector& aInty,
                                   Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep)
{
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   for (In i=I_3; i<nfrq-I_3; ++i)
   {
      // Seidler: This should be optimized to avoid recalculation.
      Nb der0   = (aInty[i+I_1]-aInty[i-I_1])/aFrqStep/C_2;
      Nb derp1  = (aInty[i+I_2]-aInty[i])/aFrqStep/C_2;
      Nb derm1  = (aInty[i]-aInty[i-I_2])/aFrqStep/C_2;
      Nb derp2  = (aInty[i+I_3]-aInty[i+I_1])/aFrqStep/C_2;
      Nb derm2  = (aInty[i-I_1]-aInty[i-I_3])/aFrqStep/C_2;
      Nb curv0  = (derp1-derm1)/aFrqStep/C_2;
      Nb curvp1 = (derp2-der0)/aFrqStep/C_2;
      Nb curvm1 = (der0-derm2)/aFrqStep/C_2;
      if (curv0<C_0 && curv0<curvm1 && curv0<curvp1)
      {
         mPeaks.push_back(aFrqStart+i*aFrqStep);
         mPeaks_inty.push_back(aInty[i]);
      }
   }

   string filename = mName + "_stick.dat";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << "# Band Lanczos IR stick spectrum generated from full spectrum." << endl
        << "# Columns: freq. (cm-1)     Absorption" << endl;
   file.precision(16); 
   Nb max=C_0;
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      file << mPeaks[i]*C_AUTKAYS << "    0.0" << endl
           << mPeaks[i]*C_AUTKAYS << "    " << mPeaks_inty[i] << endl
           << mPeaks[i]*C_AUTKAYS << "    0.0" << endl;
      if (mPeaks_inty[i]>max) max=mPeaks_inty[i]; 
   }
   Mout << " Peak positions and Intensities - abs and relative to max: " << endl; 
   Mout << " --------------------------------------------------------- " << endl; 
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      Mout << " " << mPeaks[i]*C_AUTKAYS << "    " << mPeaks_inty[i] << "  " << mPeaks_inty[i]/max << endl; 
   }
   Mout << endl;
   file_stream.close();
}

void BandLanczosIRspect::AnalyzeSpect(VccCalcDef* aVccCalcDef, Vcc* aVcc,
                                  const MidasVector& aInty,
                                  Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep,
                                  vector<In>& aOperIndex,
                                  vector<string>* aOpers)
{
   Mout << " Analyzing Band Lanczos IR spectrum with name: '" << mName << "'" << endl;
   
   BandLanczosLinRspFunc* blc;
   blc = dynamic_cast<BandLanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mBlrsp));

   if (mAnalysisBlocks.empty())
      InitAnalysisBlocksFromMinima(aInty, aFrqStart, aFrqEnd, aFrqStep);
   mAnalysisBlocks.push_back(aFrqEnd);   // Necessary for the generation of block objects below.
   
   // Setup blocks to be analyzed
   vector<LanczosSpectBlock> blocks;
   LanczosSpectBlock curblock;
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   Nb maxint = C_0; //For getting the intensity of the most intense peak
   for (In i=I_1; i<nfrq-I_1; ++i)
   {
      curblock.mIntegrInty += aInty[i]*aFrqStep;
      if (aFrqStart + i*aFrqStep >= mAnalysisBlocks[blocks.size()])
      {
         curblock.mEnd = aFrqStart+i*aFrqStep;
         blocks.push_back(curblock);
         maxint = max(curblock.mIntegrInty,maxint);
         curblock.mBegin = aFrqStart+i*aFrqStep;
         curblock.mIntegrInty = C_0;
      }
   }
   curblock.mEnd = aFrqEnd;
   blocks.push_back(curblock);
   Mout << "    Number of blocks: " << blocks.size() << endl;

   for (In i=I_0; i<blocks.size(); ++i)
      blocks[i].mWeights.ChangeStorageTo("InMem");
   
   Mout << "    Analysis starting (one dot per block): ";
   MidasVector* eigvals = nullptr;
   MidasVector* imeigvals = nullptr;
   MidasMatrix* eigvecs = nullptr;
   MidasMatrix* imeigvecs = nullptr;
   MidasMatrix* lefteigvecs = nullptr;
   MidasMatrix* imlefteigvecs = nullptr;
   MidasMatrix* zmat = nullptr;
   MidasMatrix* leftzmat = nullptr;
   eigvals=blc->GetReEigVals();
   eigvecs=blc->GetReRightEigVecs();
   zmat=blc->GetRightZMat();

   if(blc->GetNHerm() == true)
   {
      imeigvals = blc->GetImEigVals();
      imeigvecs = blc->GetImRightEigVecs();
      lefteigvecs = blc->GetReLeftEigVecs();
      imlefteigvecs = blc->GetImLeftEigVecs();
      leftzmat = blc->GetLeftZMat();
   }

   MidasMatrix w_q((*aOpers).size());
   MidasMatrix w((*aOpers).size());

   In cur_eigval = I_0;
   for (In b=I_0; b<blocks.size(); ++b)
   {
      for (In e=cur_eigval; e<(*eigvals).Size(); ++e)
      {
         if ((*eigvals)[e] < blocks[b].mEnd)
         {
            DataCont x;                            // Approximate eigenvector of A matrix.
            DataCont imx;
            
            if(blc->GetNHerm() == true)
            {
               if(blc->HasImEigVal() && blc->IncludeImEigVal())
               {
                  MidasVector eigv((*eigvecs).Nrows());  // Eigenvector of tridiagonal matrix.
                  MidasVector imeigv((*imeigvecs).Nrows());  // Eigenvector of tridiagonal matrix.
                  (*eigvecs).GetCol(eigv, e);
                  (*imeigvecs).GetCol(imeigv, e);
                  blc->TransformStoX(eigv, x);
                  blc->TransformStoX(imeigv, imx);
                  x.Pow(C_2);
                  imx.Pow(C_2);

                  for(In i=I_0; i<w_q.Nrows(); ++i)
                     for(In j= I_0; j<w_q.Ncols(); ++j)
                     {
                        w_q[i][j]=(*eigvals)[e]*((*lefteigvecs)[i][e]*(*eigvecs)[j][e] - (*imlefteigvecs)[i][e]*(*imeigvecs)[j][e]) 
                                - (*imeigvals)[e]*((*lefteigvecs)[i][e]*(*imeigvecs)[j][e] + (*imlefteigvecs)[i][e]*(*eigvecs)[j][e]);
                     }
               }
               else
               {
                  // Don't include Im values
                  MidasVector eigv((*eigvecs).Nrows());  // Eigenvector of tridiagonal matrix.
                  (*eigvecs).GetCol(eigv, e);
                  blc->TransformStoX(eigv, x);
                  x.Pow(C_2);

                  for(In i=I_0; i<w_q.Nrows(); ++i)
                     for(In j= I_0; j<w_q.Ncols(); ++j)
                        w_q[i][j]=(*eigvals)[e]*(*lefteigvecs)[i][e]*(*eigvecs)[j][e];
               }

               for(In i=I_0; i<w.Nrows(); ++i)
                  for(In j=I_0; j<w.Ncols(); ++j)
                  {
                     w[i][j] = C_0;
                     for(In k=I_0; k<w_q.Nrows(); ++k)
                        for(In l=I_0; l<w_q.Ncols(); ++l)
                           w[i][j] += w_q[k][l]*(*zmat)[k][i]*(*leftzmat)[l][j];
                  }
            }
            else // not non-hermitian
            {
               MidasVector eigv((*eigvecs).Nrows());  // Eigenvector of tridiagonal matrix.
               (*eigvecs).GetCol(eigv, e);
               blc->TransformStoX(eigv, x);
               x.Pow(C_2);

               for(In i=I_0; i<w_q.Nrows(); ++i)
                  for(In j= I_0; j<w_q.Ncols(); ++j)
                     w_q[i][j]=(*eigvals)[e]*(*eigvecs)[i][e]*(*eigvecs)[j][e];
               
               for(In i=I_0; i<w.Nrows(); ++i)
                  for(In j=I_0; j<w.Ncols(); ++j)
                  {
                     w[i][j] = C_0;
                     for(In k=I_0; k<w_q.Nrows(); ++k)
                        for(In l=I_0; l<w_q.Ncols(); ++l)
                           w[i][j] += w_q[k][l]*(*zmat)[k][i]*(*zmat)[l][j];
                  }
            }
            
            if (blocks[b].mWeights.Size() == I_0)
            {
               blocks[b].mWeights.SetNewSize(x.Size());
               blocks[b].mWeights.Zero();
            }

            if(blc->HasImEigVal() && blc->IncludeImEigVal())
            {
               for(In i=I_0; i<aOperIndex.size(); ++i)
               {
                  blocks[b].mWeights.Axpy(x, w[aOperIndex[i]][aOperIndex[i]]);
                  blocks[b].mWeights.Axpy(imx, w[aOperIndex[i]][aOperIndex[i]]);
               }
            }
            else
               for(In i=I_0; i<aOperIndex.size(); ++i)
                  blocks[b].mWeights.Axpy(x, w[aOperIndex[i]][aOperIndex[i]]);

            blocks[b].mNstates++;
         }
         else
         {
            cur_eigval = e;
            break;
         }
       }
       blocks[b].mWeights.Scale(C_1/blocks[b].mWeights.SumEle());
       blocks[b].mMaxInt = maxint;
       Mout << "." << std::flush;
   }
  
   Mout << endl
        << " Band Lanczos IR spectrum analysis results:" << endl;
   for (In b=I_0; b<blocks.size(); ++b)
   {
      blocks[b].mVcc = aVcc;
      Mout << " Block #" << b+1 << ":" << endl
           << blocks[b] << endl;

      if (fabs(blocks[b].mWeights.SumEle() - C_1) > 0.00000001)
      {
         Mout << " Warning:" << endl
              << " Sum of weights differ significantly from 1: " << blocks[b].mWeights.SumEle()
              << endl;
         string warning;
         warning=" Sum of weights differ significantly from 1 in one of the analyzed Band Lanczos IR blocks";
         MidasWarning(warning);
      }
   }
}

void BandLanczosIRspect::InitAnalysisBlocksFromMinima(const MidasVector& aInty,
                                                  Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep)
{
   mAnalysisBlocks.clear();
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   for (In i=I_1; i<nfrq-I_1; ++i)
      if (aInty[i]<aInty[i-I_1] && aInty[i]<aInty[i+I_1])
         mAnalysisBlocks.push_back(aFrqStart+i*aFrqStep);
}
