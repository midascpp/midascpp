/**
************************************************************************
* 
* @file    TensorGeneralDownContraction_Impl.h
*
* @date    13-09-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of TensorGeneralDownContraction
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TENSORGENERALDOWNCONTRACTION_IMPL_H_INCLUDED
#define TENSORGENERALDOWNCONTRACTION_IMPL_H_INCLUDED

#include <numeric>

#include "vcc/TensorGeneralDownContraction.h"

#include "tensor/NiceTensor.h"
#include "tensor/TensorDecomposer.h"
#include "vcc/v3/VccEvalData.h"
#include "vcc/TransformerV3.h"
#include "input/ModeCombi.h"
#include "util/CallStatisticsHandler.h"
#include "v3/EvalDataTraits.h"

namespace midas::vcc
{

/**
 * Constructor
 *
 * @param aEvalData     Evaluation data
 * @param aLMc          ModeCombi for <L| intermed
 * @param aCtrMc        Modes that will be down contracted
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
TensorGeneralDownContraction<T, DATA>::TensorGeneralDownContraction
   (  const DATA<T>& aEvalData
   ,  const ModeCombi& aLMc
   ,  const ModeCombi& aCtrMc
   )
   :  mContractionIndices     // Initialize to same size as aCtrMc
         (  aCtrMc.Size()
         )
   ,  mAlg
         (  aCtrMc.Size() == 0
         ?  algID::SCALE               // If no contracted modes, we just scale
         :  aLMc == aCtrMc
            ?  algID::ALL              // If MCs are equal, all modes are contracted
            :  aCtrMc.Size() == 1
               ?  algID::ONEINDEX      // If only one index is contracted, we use special algorithm
               :  algID::GENERAL       // Else, we use the general case
         )
{
   if constexpr   (  v3::DataAllowsDecompositionV<evaldata_t>
                  )
   {
      mRecompressContractionResult = aEvalData.GetGenDownCtrRecompressResult();
      mCanonicalScalar = aEvalData.DecomposedTensors();
      mDecompInfo = &aEvalData.GetDecompInfo();
   }

   // Locate positions of contraction indices
   for(size_t i=0; i<this->mContractionIndices.size(); ++i)
   {
      auto imode = aCtrMc.Mode(i);
      this->mContractionIndices[i] = aLMc.IdxNrForMode(imode);
   }
}

/**
 * Contraction interface
 *
 * @param aL         The <L| intermediate
 * @param aCtrProd   The [ctr. prod.] (all indices are contracted away)
 *
 * @return
 *    The result of the contraction.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
NiceTensor<T> TensorGeneralDownContraction<T, DATA>::Contract
   (  const NiceTensor<T>& aL
   ,  const NiceTensor<T>& aCtrProd
   )  const
{
   LOGCALL("calls");
   // Sanity check
   bool l_null = aL.IsNullPtr();
   bool cp_null = aCtrProd.IsNullPtr();
   if (  l_null
      || cp_null
      )
   {
      if (  l_null
         && !cp_null
         )
      {
         Mout  << " aCtrProd type: " << aCtrProd.ShowType() << std::endl;
         MIDASERROR("aL is not initialized!");
      }
      else if  (  !l_null
               && cp_null
               )
      {
         Mout  << " aL type: " << aL.ShowType() << std::endl;
         MIDASERROR("aCtrProd is not initialized!");
      }
      else
      {
         MIDASERROR("Both aL and aCtrProd are uninitialized!");
      }
   }

   NiceTensor<T> result;

   switch(  this->mAlg
         )
   {
      case algID::GENERAL:
      {
         LOGCALL("general");
         result = this->ContractGeneralImpl(aL, aCtrProd);
         break;
      }
      case algID::ONEINDEX:
      {
         LOGCALL("one index");
         result = this->ContractOneIndexImpl(aL, aCtrProd);
         break;
      }
      case algID::ALL:
      {
         LOGCALL("all");
         result = this->ContractAllIndicesImpl(aL, aCtrProd);
         break;
      }
      case algID::SCALE:
      {
         LOGCALL("scale");
         assert(aCtrProd.Type() == BaseTensor<T>::typeID::SCALAR);
         result = aL;
         result.Scale(aCtrProd.GetScalar());
         break;
      }
      default:
      {
         MIDASERROR("TensorGeneralDownContraction<T>::Contract(): Algorithm not found!");
      }
   }

   // Do recompression
   if constexpr   (  v3::DataAllowsDecompositionV<evaldata_t>
                  )
   {
      if (  this->mRecompressContractionResult
         && this->mAlg == algID::GENERAL  // Only GENERAL increases the rank of the tensor
         && result.NDim() > 0
         )
      {
         midas::tensor::TensorDecomposer decomposer(*this->mDecompInfo);

         if (  decomposer.CheckDecomposition(result)
            )
         {
            LOGCALL("recomp");
            result = decomposer.Decompose(result);
         }
      }
   }

   return result;
}

/**
 * Perform general contraction
 * 
 * @param aL         The <L| intermediate
 * @param aCtrProd   The [ctr. prod.] (all indices are contracted away)
 *
 * @return
 *    The result of the contraction.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
NiceTensor<T> TensorGeneralDownContraction<T, DATA>::ContractGeneralImpl
   (  const NiceTensor<T>& aL
   ,  const NiceTensor<T>& aCtrProd
   )  const
{
   // Construct contraction indices for aL
   std::vector<unsigned> indices_l(aL.NDim());
   std::iota(indices_l.begin(), indices_l.end(), 0);  // Fill with numbers [0:aL.NDim()-1]

   return contract(aL, indices_l, aCtrProd, this->mContractionIndices); // NB: No complex conj on aL
}


/**
 * Perform one-index contraction
 * 
 * @param aL         The <L| intermediate
 * @param aCtrProd   The [ctr. prod.] (all indices are contracted away)
 *
 * @return
 *    The result of the contraction.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
NiceTensor<T> TensorGeneralDownContraction<T, DATA>::ContractOneIndexImpl
   (  const NiceTensor<T>& aL
   ,  const NiceTensor<T>& aCtrProd
   )  const
{
   if (  this->mContractionIndices.size() != 1
      || aCtrProd.NDim() != 1
      )
   {
      MIDASERROR("Should not get here...");
   }

   return aL.ContractDown(this->mContractionIndices.front(), aCtrProd.ToSimpleTensor());
}


/**
 * Perform all-index contraction (dot product)
 *
 * @note
 *    This should be performed _without_ complex conjugation!!!
 * 
 * @param aL         The <L| intermediate
 * @param aCtrProd   The [ctr. prod.] (all indices are contracted away)
 *
 * @return
 *    The result of the contraction.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
NiceTensor<T> TensorGeneralDownContraction<T, DATA>::ContractAllIndicesImpl
   (  const NiceTensor<T>& aL
   ,  const NiceTensor<T>& aCtrProd
   )  const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      return NiceScalar<T>(sum_prod_elem(aL, aCtrProd), mCanonicalScalar);
   }
   else
   {
      // Return dot_product for real numbers, since it is implemented more generally!
      return NiceScalar<T>(dot_product(aL, aCtrProd), mCanonicalScalar);
   }
}

} /* namespace midas::vcc */

#endif /* TENSORGENERALDOWNCONTRACTION_IMPL_H_INCLUDED */
