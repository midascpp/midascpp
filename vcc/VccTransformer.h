/**
 ************************************************************************
 * 
 * @file                VccTransformer.h
 *
 * Created:             03-01-2008
 *                      Adapted from Vcc.h
 *
 * Author:              Ove Christiansen (ove@chem.au.dk)
 *                      Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Class for doing VCI / VMP / VCC transformations.
 * 
 * Last modified: Mon Mar 22, 2010  11:33AM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef VCC_TRANSFORMER_H
#define VCC_TRANSFORMER_H

#include <vector>
#include <string>
#include <time.h>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/Transformer.h"
#include "input/OpDef.h"
#include "input/VccCalcDef.h"
#include "util/MultiIndex.h"
#include "vcc/Xvec.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/vcc2/TwoModeIntermeds.h"
#include "vcc/TensorDataCont.h"
#include "vcc/vcc2/Vcc2TransFixedAmps.h"

namespace midas::vcc::vcc2
{
   template<typename,typename> class Vcc2TransBase;
} /* namespace midas::vcc::vcc2 */

class Vscf;
class OneModeInt;

enum TRANSFORMER { UNKNOWN=0, VCI=1, VCIH2=2, VCC=4, VMP=8, VCIRSP=16, VCCJAC=32, VCCJACDISC=64, LVCCJAC=128, SPECIAL=256 };

struct TRANSFORMER_MAP
{
   static std::map<TRANSFORMER,std::string> create_map()
   {
      std::map<TRANSFORMER,std::string> m;
      m[TRANSFORMER::UNKNOWN]    ="UNKNOWN";
      m[TRANSFORMER::VCI]        ="VCI";
      m[TRANSFORMER::VCIH2]      ="VCIH2";
      m[TRANSFORMER::VCC]        ="VCC";
      m[TRANSFORMER::VMP]        ="VMP";
      m[TRANSFORMER::VCIRSP]     ="VCIRSP";
      m[TRANSFORMER::VCCJAC]     ="VCCJAC";
      m[TRANSFORMER::VCCJACDISC] ="VCCJACDISC";
      m[TRANSFORMER::LVCCJAC]    ="LVCCJAC";
      m[TRANSFORMER::SPECIAL]    ="SPECIAL";
      return m;
   }
   static const std::map<TRANSFORMER,std::string> MAP;
};

/***
 * VccTransformer class
 ***/
class VccTransformer
   : public Transformer
{
   public:
      enum class VCC2RSP_T {RJAC, LJAC};

   protected:
      TRANSFORMER       mType;                       ///< Has to be one of the TRANSFORMER:: consts.
   
      Vcc* const        mpVcc;
      VccCalcDef* const mpVccCalcDef;
      OpDef*            mpOpDef;
      const ModalIntegrals<Nb>* const mpOneModeModalBasisIntOrig;      ///< One-mode operators integrals.
      ModalIntegrals<Nb> mOneModeModalBasisInt;
      ModalIntegrals<Nb> mR1Ints;
                      
      Xvec              mXvec;                       ///< Input vector.
      Xvec              mTransXvec;                  ///< Transformed vector.
   
      const Nb          mEvmp0;
      bool              mVciVcc;                     ///< Do VCC using VCI algorithms.
      bool              mT1TransH;                   ///< Use T1 transformed integrals (for VCC).      
      bool              mLambdaCalc;                 ///< A lambda calc.
      Nb                mLambda;                     ///< The current lambda
   
      Xvec*             mpExpTXvec;
      ///< For use in VCC Jacobian transformations. In certain cases, e.g. VCC response with
      ///< preconditioning, the T space may be different from the excitation spaces in mXvec and
      ///< mTransXvec.

      using mod_int_cont_t = decltype(mOneModeModalBasisInt)::ContT;

      //@{
      //! Get the 1-mode modal integrals to be used for calculations, be it orig. or T1.
      ModalIntegrals<Nb>* pOneModeModalBasisInt();
      const ModalIntegrals<Nb>* pOneModeModalBasisInt() const;
      //@}

      //! Reset modal integrals to orig (aT1 = false) or calc. T1 ints from amps.
      void UpdateModalIntegrals(bool aT1 = false, const Xvec* const = nullptr);

      //! Construct the R1-transformed integrals from T1-integrals and R1-coefs.
      void UpdateR1Integrals(const ModalIntegrals<Nb>&, const DataCont&, const Xvec&);
   
   private:
      /*********************************************************************
       * Stuff for two mode calculations.
       *********************************************************************/
      //@{
      //! Type aliases, primarily/solely for the VCC[2]/H2 transformations.
      using value_t = Nb;
      using datacont_t = GeneralDataCont<value_t>;
      using vcc2transbase_t = midas::vcc::vcc2::Vcc2TransBase<value_t,OpDef>;
      using vcc2transrsp_t = midas::vcc::vcc2::Vcc2TransFixedAmps<value_t,false,OpDef>;
      //@}

      /*********************************************************************//**
       * @brief
       *    Small wrapper class for Vcc2TransRJac and Vcc2TransLJac, for
       *    defining what should happen for the unique_ptr%s when
       *    VccTransformer object is copied. (For move/destruct it just uses
       *    unique_ptr defaults.)
       *
       * The current implementation (-MBH, Mar 2019) doesn't allow copying a
       * Vcc2TransBase and derived objects. It generally shouldn't be necessary
       * either. Therefore we assume it's fine to just set the unique_ptr to
       * nullptr in the copy, meaning VccTransformer reconstructs the
       * transformers anew when/if needed, when calling FactoryVcc2Trans().
       ************************************************************************/
      class Vcc2TransRspWrapper
         :  public std::unique_ptr<vcc2transrsp_t>
      {
         public:
            Vcc2TransRspWrapper() = default;
            Vcc2TransRspWrapper(Vcc2TransRspWrapper&&) = default;
            Vcc2TransRspWrapper& operator=(Vcc2TransRspWrapper&&) = default;

            //! Take ownership of pointed transformer object.
            Vcc2TransRspWrapper(vcc2transrsp_t* p);

            //@{
            //! "Copying" will actually just reset to nullptr and disable Reuse().
            Vcc2TransRspWrapper(const Vcc2TransRspWrapper&);
            Vcc2TransRspWrapper& operator=(const Vcc2TransRspWrapper&);
            //@}

            //@{
            //! Get/set whether stored trf. (if any) should be reused.
            bool Reuse() const {return mReuse;}
            bool& Reuse() {return mReuse;}
            //@}

         private:
            //! Whether stored trf. (if any) should be reused.
            bool mReuse = false;
      };

      Vcc2TransRspWrapper mVcc2TransRJac;
      Vcc2TransRspWrapper mVcc2TransLJac;

      bool UseVcc2Trans() const {return mpVccCalcDef->TwoModeTrans() || mpVccCalcDef->OneModeTrans();}
      const datacont_t& Vcc2GetCCoefs() const {return mXvec.GetXvecData();}
      const datacont_t& Vcc2GetTAmps() const {return mXvec.GetXvecData();}
      const datacont_t& Vcc2GetRCoefs() const {return mTransXvec.GetXvecData();}
      const datacont_t& Vcc2GetLCoefs() const {return mTransXvec.GetXvecData();}

      enum class VCC2_T {VCC, VCI, ETA, RJAC, LJAC};

      //@{
      //! Return Vcc2Transformer appropriately set up for specified enum type.
      std::unique_ptr<vcc2transbase_t> FactoryVcc2Trans(VCC2_T);
      const vcc2transrsp_t* FactoryVcc2Trans(VCC2RSP_T);
      //@}

      // *******************************************************************
   
   public:
      /*********************************************************************
       * The interface.
       *********************************************************************/
      void Transform(DataCont& aIn      , DataCont& aOut      , In aI, In aJ, Nb& aNb);
      void Transform(TensorDataCont& aIn, TensorDataCont& aOut, In aI, In aJ, Nb& aNb);
      
      virtual void PreparePreDiag();
      virtual void PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb);
      
      void RestorePreDiag();
      void ConstructExplicit(MidasMatrix&);
      In VectorDimension() const;
      const std::vector<In>& GetNModals() const;
   
      virtual ~VccTransformer();
     
      /*********************************************************************
       * Extended interface for VCC transformers.
       *********************************************************************/
      virtual void SetType(TRANSFORMER aType) {mType = aType;}
     
      virtual void CalcVccRspEta0(DataCont& aRes);
      virtual void CalcVccRspEta0(TensorDataCont& aRes)
      {
         MIDASERROR("TensorDataCont can only be handled by TransformerV3!");
      }
      // Calculate VCC eta0 vector.
      
      virtual Nb CalcVccRspFRS(DataCont& arDcInMult, DataCont& arDcInR, DataCont& arDcInS)
      { MIDASERROR("VccTransformer::VccTransRspF() not implemented."); return C_0;}
      
      virtual void CalcVccRspFR(DataCont& arDcInMults, DataCont& arDcInR, DataCont& aRes)
      { MIDASERROR("VccTransformer::CalculateVccFR not implemented.");}
      
      virtual std::ostream& Print(std::ostream& ostream) const 
      { ostream << "no printout for VccTransformer \n"; return ostream; }
      
      /*********************************************************************
       * Misc...
       *********************************************************************/
      
      VccTransformer(Vcc* apVcc, VccCalcDef* apVccCalcDef, OpDef* apOpDef, const ModalIntegrals<Nb>* const apIntegralsOrig,
                  Nb aEvmp0, In aISO=I_0);
      VccTransformer(VccTransformer& arTr, In aPrecondExciLevel, bool aAllowOutOfSpaceXvec=true);
      void SetOpDef(OpDef* aOpDef) {mpOpDef=aOpDef;}
      OpDef* GetOpDef() {return mpOpDef;}
      VccCalcDef* pVccCalcDef() {return mpVccCalcDef;}
      Vcc* pVcc() {return mpVcc;}
      bool T1TransH() {return mT1TransH;}
      void SetT1TransH(bool aT1) {mT1TransH=aT1;}
      TRANSFORMER GetType() {return mType;}
      
      void SetLambdaCalc(bool aLambdaCalc) {mLambdaCalc = aLambdaCalc;}
      void SetLambda(Nb aLambda) {mLambda = aLambda;}
      Nb GetLambda() {return mLambda;}
   
      void SetVciVcc(bool aB) {mVciVcc = aB;}
      bool GetOneModeTrans(){return mpVccCalcDef->OneModeTrans();}
      void SetSetOneModeTrans(bool aB){mpVccCalcDef->SetOneModeTrans(aB);}
      bool GetTwoModeTrans() { if(!mpVccCalcDef) std::cerr << " mpVccCalcDef = nullptr"; return mpVccCalcDef->TwoModeTrans(); }
      void SetSetTwoModeTrans(bool aB){mpVccCalcDef->SetTwoModeTrans(aB);}
      
      void OneBodyDensity(Xvec arXvec, DataCont& arDC);  

      //! Calls PutOperator... from Vscf, _and_ updates this transformer's ModalIntegrals accordingly.
      void PutOperatorToWorkingOperatorAndUpdateModalIntegrals(Vscf*, OpDef*, OneModeInt*);

      //! Calls Restore... from Vscf, _and_ updates this transformer's ModalIntegrals accordingly.
      void RestoreOrigOperAndUpdateModalIntegrals(Vscf*);

      /*********************************************************************
       * Xvec methods.
       *********************************************************************/
      void InitOOSTransXvec();                                   ///< Make mTransXvec out-of-space.
      In NexciTypesXvec() const {return mXvec.NexciTypes();}           ///< No. of MCs.
      In NexciXvec() const {return mXvec.Size();}                     ///< No. of parameters.
      In NexciTypesTransXvec() const {return mTransXvec.NexciTypes();} ///< No. of MCs.
      In NexciTransXvec() const {return mTransXvec.Size();}           ///< No. of parameters.
      void AssignXvec(MidasVector& arVec) {mXvec.AssignXvec(arVec);}
      void AssignXvec(DataCont& aDc) {mXvec.GetXvecData() = aDc;}
      //! Assign Xvec from TensorDataCont without reference
      void AssignXvec(const TensorDataCont& arTdc) { mXvec.Assign(arTdc, Xvec::load_in, Xvec::add_ref); }
      void GetTransXvec(MidasVector& arVec){mTransXvec.GetXvec(arVec);}
      void GetTransXvec(DataCont& aDc) {aDc = mTransXvec.GetXvecData();}
      const Xvec& GetXvec() {return mXvec;}
      const Xvec& GetTransXvec() {return mTransXvec;}
      string GetXvecStorage() {return mXvec.GetXvecData().Storage();}
      const ModeCombiOpRange& GetXvecModeCombiOpRange() const { return mXvec.GetModeCombiOpRange(); }
      const ModeCombiOpRange& GetTransXvecModeCombiOpRange() const { return mTransXvec.GetModeCombiOpRange(); }
      void ConvertTransXvecToDataCont()
      {
         this->mTransXvec.ConvertToDataCont();
      }
      // *******************************************************************
   
      /*********************************************************************
       * Transformer methods.
       *********************************************************************/     
      void VmpTransformer();    ///< Transforming mXvec by (H-H0=U) to obtain mTransXvec;
      Nb TransCorrE(DataCont& arDcIn,In aI, In aJ=I_0, Nb aNb=C_0); 
      
      void GenTransformer(bool aPreDIag = false);   ///< General VCI / VCC transformer.
      void TransformerH0(const In& arInv,const Nb& arE0=C_0);    
      ///< Transforming by H0(arInv>0, or P(H0-arE0)^-1P for arInv<-1) to obtain mTransXvec;
      ///< arInv +- 1 assign, arInv +-2 Add.
      void TransformerTrueHDiag(const In& arInv,const Nb& arE0=C_0);    
      ///< Transforming by the exact H diagonal Hdiag(arInv>0, or P(Hdiag-arE0)^-1P for arInv<-1)
      ///< to obtain mTransXvec; arInv +- 1 assign, arInv +-2 Add.
      void VccContractor(MidasVector& arSigma, const ModeCombi& arMp, const ModeCombi& arMq, 
            const ModeCombi& arMpq, const ModeCombi& arMpNotQ, const ModeCombi& arMqNotP,
            const ModeCombi& arMpqcommon, bool=false);
      void VciCoefsFromVcc(const ModeCombi& arM,const In& arAdd, 
            MidasVector& arVci, DataCont& arVcc,const Xvec& arXvec, 
            bool aLowerOnly=false,bool aMinusT=false);     
      ///< Represent <mu^ arM | exp(T) | R > - a particular exci from CC to Vci
      void VccFromVci(DataCont& arVci, DataCont& arVcc);     
      ///< Represent arVci in terms of exp(arVcc)
      void ExpT(Xvec& arXvecIn, DataCont& arDcIn, Xvec& arXvecOut, DataCont& arDcOut,
                Xvec& arXvecT, DataCont& arDcT, bool aMinusT);
      ///< Transform arDcIn by exp(+/- T) to arDcOut.
      void operator()(In aI,In aJ=I_1) {if (aI==0) TransformerH0(aJ); if (aI==1) GenTransformer();}   
      ///< For use as function object, transform by H or H0.
      void TransH(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ=I_0, Nb aNb = C_0);  
      void TransPreDiag(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ=I_0, Nb aNb = C_0);  
      void PreparePreDiagTrans();
      void RestoreAfterPreDiagTrans();
      void VccTransH(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb);  
      void VccTransPreDiag(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb);
      void TransH2(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb);  
      void SmallToLargeTrans(DataCont& arDcSmall, DataCont& arDcLarge, ModeCombiOpRange& arMcS,
                             ModeCombiOpRange& arMcL);
      ///< Transform between small and large storage of a vector. 
      void SumNpurify(DataCont& arDc);
      void Emaxpurify(DataCont& arDc);
      void Zeropurify(DataCont& arDc);
      void CheckPurifies(DataCont& arDc);
      Nb VccEnerForMpLocal(const ModeCombi& arMp,const Nb aEtot);
      ///< Get the vcc energy but excluding certain terms.
      // *******************************************************************
   
      
      /*********************************************************************
       * Response transformer methods.
       *********************************************************************/
      void TwoModeVccRspInit();
      void VciRspTransH(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb);  
      void VciRspTransF(DataCont& arDcIn,DataCont& arDcOut,Nb& aNb);  
      void PrimOrtoTrans(DataCont& arDcOc,DataCont& arDcPrim,Nb& arRefComp, In aI);
      ///< Vci transformation between primitive and orthogonal basis 
      void VccRspTransJacobian(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, Nb& aNb, bool aRigh=true);
      ///< VCC direct response transformer.
      void VccRspTransJacobianOnDisc(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, Nb& aNb);
      ///< VCC response transformer using Jacobian on Disc.
      ///< VCC left-transformation of a vector 
      void CustomAmp(DataCont& aAmpDC);
      void VciRspTransTrueAdiag(const In& aJ,const Nb& aNb=C_0);
   
      bool CmpDCs(DataCont& aDc1, DataCont& aDc2, const bool aVcc, const bool aVerbose=true);
   
      virtual Transformer* Clone() const;
      virtual Transformer* CloneImprovedPrecon(In aPreconLevel) const;
   
      virtual std::string Type() const { return TRANSFORMER_MAP::MAP.at(mType); }
      virtual std::string VccType() const { return "VccTransformer"; }

      //! VCC2: Set transformer to be reused at next FactoryVcc2Trans(VCC2RSP_T) call.
      void ReuseVcc2Trans(VCC2RSP_T);

      //! VCC2: Clear (i.e. deconstruct) the stored VCC2RSP_T transformer and disables Reuse().
      void ClearVcc2Trans(VCC2RSP_T);

};
  
void DirProd(MidasVector* apT, MultiIndex* apI, MidasVector& arResultVec,
             const In aN,const MultiIndex& arResMi, In aLevel, vector<In>& arResInt,
             Nb arResNb,vector<In>* apConnect, const bool aMinusT=false);
///< Direct product of coefficient vectors.

#endif  // VCC_TRANSFORMER_H
