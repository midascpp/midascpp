/**
************************************************************************
* 
* @file                 VccStateSpace.cc
*
* Created:              26-04-2016
*
* Author:               Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:    Defines the structure of the coefficient/amplitude
*                       vectors in a VCC (Vibrational Correlation Calculation).
* 
* Last modified:        26-04-2016 (Mads Boettger Hansen)
* 
* Detailed Description: Holds a ModeCombiOpRange which defines the range of
*                       mode combinations considered for the vibrational
*                       correlation calculation (VCC), as well as a vector
*                       containing the number of modals for each mode.
*                       Together these two objects define the "structure" of
*                       the coefficient/amplitude vectors in the space of
*                       vibrational states, i.e. they define the shape of all
*                       multi-dimensional arrays/tensors considered.  Initially
*                       intended for use with TensorDataCont and related
*                       classes.
*                       Since the state space structure is fixed during any
*                       given calculation, the VccStateSpace class is designed
*                       not to be altered after construction, and all relevant
*                       data should be set when constructing the object.
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>
#include <iomanip>
#include <string>
#include "vcc/VccStateSpace.h"
#include "vcc/VccStateModeCombi.h"

// Type aliases.
using const_iterator = VccStateSpace::const_iterator; // Just easier to type.

// -----------------------------------------------------------------------------
//    Private member functions
// -----------------------------------------------------------------------------
std::vector<Uin> VccStateSpace::AddressesFromDims(const ModeCombiOpRange& arMCR, const std::vector<Uin>& arDims) const
{
   // Check sanity. 
   // (Should actually never be invoked due to MIDASERROR in SanityCheck.)
   MidasAssert(mSanity, "VccStateSpace has not passed sanity check.");

   // Make vec. of size 0, then reserve according to input.
   // There must be an entry for each MC in the MCR, plus one for the end, so
   // that the mAddresses can be used to extract sizes as well.
   std::vector<Uin> vec(I_0);
   vec.reserve(arMCR.Size() + I_1);

   // First entry is 0.
   Uin address = I_0;
   vec.push_back(address);

   // Loop through MCs, calculate size and accumulate in the result vector.
   for(const auto& mc: arMCR)
   {
      Uin size = I_1;
      // Multiply factors to get size. -1 accounts for the reference state.
      for(const auto& mode: mc.MCVec())
      {
         // This really relies on the MCR and NModals having been validated
         // against each other.
         size *= arDims[mode];
      }
      address += size;
      vec.push_back(address);
   }
   
   return vec;
}

ModeCombiOpRange VccStateSpace::EraseRefModeCombi(ModeCombiOpRange aMCR, bool aEraseRef) const
{
   if(aEraseRef)
   {
      aMCR.Erase(std::vector<ModeCombi>{ModeCombi(std::vector<In>{}, -I_1)});
   }
   return aMCR;
}

// -----------------------------------------------------------------------------
//    Constructors, etc.
// -----------------------------------------------------------------------------
// Regulars.
VccStateSpace::~VccStateSpace() = default;

// -----------------------------------------------------------------------------
//    Member access
// -----------------------------------------------------------------------------
const ModeCombiOpRange& VccStateSpace::MCR() const
{
   return mMCR;
}

const std::vector<Uin>& VccStateSpace::Dims() const
{
   return mDims;
}

const std::vector<Uin>& VccStateSpace::Addresses() const
{
   return mAddresses;
}

Uin VccStateSpace::NumEmptyMCs() const
{
   return mMCR.NumEmptyMCs();
}

Uin VccStateSpace::NumMCs() const
{
   return mMCR.Size();
}

Uin VccStateSpace::LocateAddress(Uin aAddress) const
{
   // Binary search for the interval [begin, end) containing the given address.
   // Beginning and end of current search interval.
   // Setting end = size of address vector incorporates the case that the
   // address is outside the range of the MCR.
   // Note, from construction mAddresses always has >= 1 element.
   Uin beg = I_0;
   Uin end = Addresses().size();
   Uin mid = I_0;

   // Then search recursively in increasingly smaller subintervals.
   while(end - beg > I_1)
   {
      // Set midpoint of interval.
      Uin mid = beg + (end - beg)/I_2;

      // Compare with address.
      if(aAddress < Addresses()[mid])
      {
         end = mid;
      }
      else
      {
         beg = mid;
      }
   }

   // By now, 'beg' is such that aAddress is in [Addr[beg]; Addr[beg+1]) or in
   // [Addr[beg]; infinity) if Addr[beg] is the last element.
   return beg;
}

// -----------------------------------------------------------------------------
//    Iterator; Constructors, etc.
// -----------------------------------------------------------------------------

//! @cond Doxygen_Suppress
// Copy constructor/assignment needs special care due to the unique_ptr.
// Specifically, ensure that mpVSMC doesn't hold a nullptr.
const_iterator::const_iterator(const const_iterator& arIter)
   :  mpVSMC(arIter.mpVSMC ? new VccStateModeCombi(*arIter.mpVSMC) : nullptr)
{
}

// Constructor from a VccStateModeCombi.
// (Moving a VccStateModeCombi rvalue will probably be the only
// relevant case.)
const_iterator::const_iterator(VccStateModeCombi&& arVSMC)
   :  mpVSMC(new VccStateModeCombi(std::move(arVSMC)))
{
}
//! @endcond

// -----------------------------------------------------------------------------
//    Iterator; Iterator logic
// -----------------------------------------------------------------------------
// Comparison operators.
bool const_iterator::operator==(const const_iterator& arIter) const
{
   return (*mpVSMC == *arIter.mpVSMC);
}

bool const_iterator::operator!=(const const_iterator& arIter) const
{
   return !(*this == arIter);
}


// Increment/decrement operators.
const_iterator& const_iterator::operator++()
{
   ++(*mpVSMC);
   return *this;
}

const_iterator  const_iterator::operator++(int)
{
   const_iterator temp(*this);
   operator++();
   return *this;
}

const_iterator& const_iterator::operator--()
{
   --(*mpVSMC);
   return *this;
}

const_iterator  const_iterator::operator--(int)
{
   const_iterator temp(*this);
   operator--();
   return *this;
}


// Dereference operators.
const_iterator::reference const_iterator::operator*()  const
{
   return *mpVSMC;
}

const_iterator::pointer   const_iterator::operator->() const
{
   return mpVSMC.get();
}


// -----------------------------------------------------------------------------
//    Iterator related functions
// -----------------------------------------------------------------------------
const_iterator VccStateSpace::begin() const
{
   return const_iterator(VccStateModeCombi::ConstructBegin(*this));
}

const_iterator VccStateSpace::end()   const
{
   return const_iterator(VccStateModeCombi::ConstructEnd(*this));
}

// -----------------------------------------------------------------------------
//    Non-member functions related to class
// -----------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& arOS, const VccStateSpace& arVSS)
{
   // Save current flags of the ostream for resetting later.
   auto init_flags = arOS.flags();

   // Widths for formatting.
   int w_ref  = 5;
   int w_mc   = 20;
   int w_dims = 20;
   int w_addr = 7;
   int w_size = 6;
   int w_pad  = 2;
   int w_tot  = w_ref + w_mc + 2*w_dims + (2*w_addr+w_pad) + w_size + 5*w_pad;
   std::string line_sep(w_tot, '-');

   // Header line.
   arOS << line_sep << std::endl;
   arOS << std::right;
   arOS << std::setw(w_ref)          << "Ref#";
   arOS << std::setw(w_pad)          << "";
   arOS << std::setw(w_mc)           << "Mode combination";
   arOS << std::setw(w_pad)          << "";
   arOS << std::setw(w_dims)         << "Dimensions";
   arOS << std::setw(w_pad)          << "";
   arOS << std::setw(w_dims)         << "N_modals per mode";
   arOS << std::setw(w_pad)          << "";
   arOS << std::setw(2*w_addr+w_pad) << "Addr. [beg;end)";
   arOS << std::setw(w_pad)          << "";
   arOS << std::setw(w_size)         << "Size";
   arOS << std::endl;
   arOS << line_sep << std::endl;

   for(auto&& vssmc: arVSS)
   {
      arOS << std::setw(w_ref)  << vssmc.RefNumber();
      arOS << std::setw(w_pad)  << "";
      arOS << std::setw(w_mc)   << StringOfModes(vssmc);
      arOS << std::setw(w_pad)  << "";
      arOS << std::setw(w_dims) << StringOfDims(vssmc);
      arOS << std::setw(w_pad)  << "";
      arOS << std::setw(w_dims) << StringOfNModals(vssmc);
      arOS << std::setw(w_pad)  << "";
      arOS << "[" << std::setw(w_addr - 1) << vssmc.AddressBegin();
      arOS << std::left;
      arOS << std::setw(w_pad) << ";";
      arOS << std::right;
      arOS << std::setw(w_addr - 1) << vssmc.AddressEnd() << ")";
      arOS << std::setw(w_pad)  << "";
      arOS << std::setw(w_size) << vssmc.TensorSize();
      arOS << std::endl;
   }
   arOS << line_sep << std::endl;

   // Reset initial flags.
   arOS.flags(init_flags);
   return arOS;
}

