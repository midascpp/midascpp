/**
************************************************************************
* 
* @file                TensorDataCont.cc
*
* Created:             16-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Instantiations
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "inc_gen/TypeDefs.h"
#include "vcc/TensorDataCont_Decl.h"
#include "vcc/TensorDataCont_Impl.h"

#define INSTANTIATE_TENSORDATACONT(T) \
template class GeneralTensorDataCont<T>; \
template T Dot(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template T SumProdElems(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template T Angle(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template T Angle(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&, typename GeneralTensorDataCont<T>::real_t, typename GeneralTensorDataCont<T>::real_t); \
template GeneralTensorDataCont<T> Abs(const GeneralTensorDataCont<T>&); \
template GeneralDataCont<T> DataContFromTensorDataCont(const GeneralTensorDataCont<T>&); \
template NiceTensor<T> NiceTensorFromTensorDataCont(const GeneralTensorDataCont<T>&, const std::set<In>&); \
template typename GeneralTensorDataCont<T>::real_t Norm(const GeneralTensorDataCont<T>& arT); \
template typename GeneralTensorDataCont<T>::real_t Norm2(const GeneralTensorDataCont<T>& arT); \
template typename GeneralTensorDataCont<T>::real_t DiffNorm2(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template void Normalize(GeneralTensorDataCont<T>& arT); \
template void Orthogonalize(GeneralTensorDataCont<T>& arT, const GeneralTensorDataCont<T>& arU); \
template void Zero(GeneralTensorDataCont<T>& arT);  \
template std::ostream& operator<<(std::ostream&, const GeneralTensorDataCont<T>&); \
template void SetShape(GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template void assert_same_shape(const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template size_t Size(const GeneralTensorDataCont<T>&);

#define INSTANTIATE_TENSORDATACONT_2(T, U) \
template void Axpy(GeneralTensorDataCont<T>& arY, const GeneralTensorDataCont<T>& arX, U aA); \
template void Scale(GeneralTensorDataCont<T>& arT, U aNb); \
template void DataToPointer(const GeneralTensorDataCont<T>&, U*); \
template void DataFromPointer(GeneralTensorDataCont<T>&, const U*); \
\
template GeneralTensorDataCont<T>::GeneralTensorDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const VccCalcDef*, bool); \
template void GeneralTensorDataCont<T>::Scale(U); \
template void GeneralTensorDataCont<T>::Axpy(const GeneralTensorDataCont&, U);  \

#define INSTANTIATE_TENSORDATACONT_2INT(T, I) \
template GeneralTensorDataCont<T>::GeneralTensorDataCont(const ModeCombiOpRange&, const std::vector<I>&, bool); \
template void GeneralTensorDataCont<T>::ConstructZeroTensorDataCont(const ModeCombiOpRange&, const std::vector<I>&, bool); \
template GeneralTensorDataCont<T> RandomTensorDataCont(const ModeCombiOpRange&, const std::vector<I>&, bool);

#define INSTANTIATE_TENSORDATACONT_3INT(T, U, I) \
template void GeneralTensorDataCont<T>::ConstructXvecDataFromDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const std::vector<I>&, bool); \
template GeneralTensorDataCont<T>::GeneralTensorDataCont(const GeneralDataCont<U>&, const ModeCombiOpRange&, const std::vector<I>&, bool);

#define INSTANTIATE_TENSORDATACONT_3(T, U, V) \
template typename GeneralTensorDataCont<T>::real_t OdeNorm2(const GeneralTensorDataCont<T>&, U, V, const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template typename GeneralTensorDataCont<T>::real_t OdeMeanNorm2(const GeneralTensorDataCont<T>&, U, V, const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&); \
template typename GeneralTensorDataCont<T>::real_t OdeMaxNorm2(const GeneralTensorDataCont<T>&, U, V, const GeneralTensorDataCont<T>&, const GeneralTensorDataCont<T>&);


#define INSTANTIATE_TENSORDATACONT_4(T, U, CONTAINER_T, CONTAINER_U) \
template GeneralTensorDataCont<T> LinComb(const CONTAINER_T<GeneralTensorDataCont<T>>&, const CONTAINER_U<U>&);

// Pre-compile specific instances of both the wrapper and implementation for decompositions.
#define INSTANTIATE_TENSORDATACONT_DISABLE(T) \
template void GeneralTensorDataCont<T>::DecomposeImpl(const TensorDecompInfo::Set&, midas::type_traits::DisableIfComplexT<T>*); \
template void GeneralTensorDataCont<T>::DecomposeImpl(const TensorDecompInfo::Set&, const std::vector<real_t>&, midas::type_traits::DisableIfComplexT<T>*); \
template void GeneralTensorDataCont<T>::DecomposeImpl(const TensorDecompInfo::Set&, real_t, midas::type_traits::DisableIfComplexT<T>*); \
template void GeneralTensorDataCont<T>::DecomposeImpl(const TensorDecompInfo::Set&, const GeneralTensorDataCont<T>&, midas::type_traits::DisableIfComplexT<T>*); \
template void GeneralTensorDataCont<T>::DecomposeImpl(const TensorDecompInfo::Set&, const GeneralTensorDataCont<T>&, const std::vector<real_t>&, midas::type_traits::DisableIfComplexT<T>*); \
template void GeneralTensorDataCont<T>::Decompose(const TensorDecompInfo::Set&); \
template void GeneralTensorDataCont<T>::Decompose(const TensorDecompInfo::Set&, const std::vector<typename GeneralTensorDataCont<T>::real_t>&); \
template void GeneralTensorDataCont<T>::Decompose(const TensorDecompInfo::Set&, const typename GeneralTensorDataCont<T>::real_t&); \
template void GeneralTensorDataCont<T>::Decompose(const TensorDecompInfo::Set&, const GeneralTensorDataCont<T>&); \
template void GeneralTensorDataCont<T>::Decompose(const TensorDecompInfo::Set&, const GeneralTensorDataCont<T>&, const std::vector<typename GeneralTensorDataCont<T>::real_t>&);



INSTANTIATE_TENSORDATACONT(float);
INSTANTIATE_TENSORDATACONT(double);
INSTANTIATE_TENSORDATACONT(std::complex<float>);
INSTANTIATE_TENSORDATACONT(std::complex<double>);

INSTANTIATE_TENSORDATACONT_2(float, float);
INSTANTIATE_TENSORDATACONT_2(double, double);
INSTANTIATE_TENSORDATACONT_2(std::complex<float>, std::complex<float>);
INSTANTIATE_TENSORDATACONT_2(std::complex<double>, std::complex<double>);
INSTANTIATE_TENSORDATACONT_2(std::complex<float>, float);
INSTANTIATE_TENSORDATACONT_2(std::complex<double>, double);

INSTANTIATE_TENSORDATACONT_2INT(float, unsigned int);
INSTANTIATE_TENSORDATACONT_2INT(float, unsigned long);
INSTANTIATE_TENSORDATACONT_2INT(float, int);
INSTANTIATE_TENSORDATACONT_2INT(float, long);
INSTANTIATE_TENSORDATACONT_2INT(double, unsigned int);
INSTANTIATE_TENSORDATACONT_2INT(double, unsigned long);
INSTANTIATE_TENSORDATACONT_2INT(double, int);
INSTANTIATE_TENSORDATACONT_2INT(double, long);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<float>, unsigned int);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<float>, unsigned long);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<float>, int);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<float>, long);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<double>, unsigned int);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<double>, unsigned long);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<double>, int);
INSTANTIATE_TENSORDATACONT_2INT(std::complex<double>, long);

INSTANTIATE_TENSORDATACONT_3INT(float, float, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(float, float, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(float, float, int);
INSTANTIATE_TENSORDATACONT_3INT(float, float, long);
INSTANTIATE_TENSORDATACONT_3INT(double, double, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(double, double, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(double, double, int);
INSTANTIATE_TENSORDATACONT_3INT(double, double, long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, float, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, float, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, float, int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, float, long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, double, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, double, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, double, int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, double, long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, std::complex<float>, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, std::complex<float>, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, std::complex<float>, int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<float>, std::complex<float>, long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, std::complex<double>, unsigned int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, std::complex<double>, unsigned long);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, std::complex<double>, int);
INSTANTIATE_TENSORDATACONT_3INT(std::complex<double>, std::complex<double>, long);

INSTANTIATE_TENSORDATACONT_3(float, float, float);
INSTANTIATE_TENSORDATACONT_3(double, double, double);
INSTANTIATE_TENSORDATACONT_3(std::complex<float>, float, float);
INSTANTIATE_TENSORDATACONT_3(std::complex<double>, double, double);

INSTANTIATE_TENSORDATACONT_4(float, float, std::list, std::vector);
INSTANTIATE_TENSORDATACONT_4(double, double, std::list, std::vector);
INSTANTIATE_TENSORDATACONT_4(std::complex<float>, float, std::list, std::vector);
INSTANTIATE_TENSORDATACONT_4(std::complex<double>, double, std::list, std::vector);
INSTANTIATE_TENSORDATACONT_4(std::complex<float>, std::complex<float>, std::list, std::vector);
INSTANTIATE_TENSORDATACONT_4(std::complex<double>, std::complex<double>, std::list, std::vector);

INSTANTIATE_TENSORDATACONT_DISABLE(Nb);

#undef INSTANTIATE_TENSORDATACONT
#undef INSTANTIATE_TENSORDATACONT_2
#undef INSTANTIATE_TENSORDATACONT_4
#undef INSTANTIATE_TENSORDATACONT_DISABLE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
