/**
 ************************************************************************
 * 
 * @file                VccTransformer.cc
 *
 * Created:             4-01-2008
 *                      (Originally part of Vcc.cc)
 *
 * Author:              Ove Christiansen (ove@chem.au.dk)
 *                      Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementing some functions of class
 *                      Transformer.
 * 
 * Last modified: Thu Mar 25, 2010  03:54PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */


// Standard headers:
#include<vector>
using std::vector;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "util/MidasStream.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "operator/OperProd.h"
#include "input/ModeCombi.h"
#include "vcc/Vcc.h"
#include "vcc/Xvec.h"
#include "vcc/VccStateSpace.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "mmv/Diag.h"
#include "mpi/FileStream.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/VccTransformer.h"
#include "vcc/vcc2/Vcc2TransBase.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransRJac.h"
#include "vcc/vcc2/Vcc2TransLJac.h"

/**
 *
 **/
const std::map<TRANSFORMER,std::string> TRANSFORMER_MAP::MAP = 
   TRANSFORMER_MAP::create_map();

/**
 *
 **/
void VccTransformer::Transform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb)
{
   if (mTrfUsingPreDiag)
   {
      PreDiagTransform(aIn, aOut, aI, aJ, aNb);
      return;
   }
   switch(mType)
   {
      case TRANSFORMER::VCI:
         TransH(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCIH2:
         TransH2(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCC:
         VccTransH(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCIRSP:
         VciRspTransH(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCCJAC:
         VccRspTransJacobian(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCCJACDISC:
         VccRspTransJacobianOnDisc(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::LVCCJAC:
         VccRspTransJacobian(aIn, aOut, aI, aJ, aNb,false);
         break;
      default:
         MIDASERROR(" VccTransformer::Transform(): Unknown mType.");
   }
}

/**
 *
 **/
void VccTransformer::PreparePreDiag()
{
}

/**
 *
 **/
void VccTransformer::PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb)
{
   switch(mType)
   {
      case TRANSFORMER::VCI:
         TransPreDiag(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCC:
         VccTransPreDiag(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCIH2:
         TransH2(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCIRSP:
         VciRspTransH(aIn, aOut, aI, aJ, aNb);
         break;
      case TRANSFORMER::VCCJAC:
         VccRspTransJacobian(aIn, aOut, aI, aJ, aNb);
         break;
      default:
         MIDASERROR(" VccTransformer::PreDiagTransform(): Unknown mType.");
   }
}

/**
 *
 **/
void VccTransformer::RestorePreDiag()
{
}

/**
 *
 **/
void VccTransformer::ConstructExplicit(MidasMatrix& aMat)
{
   In size = VectorDimension();
   MidasWarningIf(size>2000,"Explicitly constructing transformer of size: " + std::to_string(size));
   
   aMat.SetNewSize(size,size,false);
   
   DataCont unit_vec(size, C_0, "InMem", "unit_vec");
   DataCont res(size, C_0, "InMem", "res_vec");

   Nb one = C_1, zero = C_0, dummy = C_0;
   for(In i=0; i<size; ++i)
   {
      unit_vec.DataIo(IO_PUT, i, one); // put in 1
      Transform(unit_vec, res, I_1, I_0, dummy);

      aMat.AssignCol(*(res.GetVector()),i);

      unit_vec.DataIo(IO_PUT, i, zero); // overwrite the 1 with 0, so we are ready for next column
   }
}


/**
 * constructor
 **/
VccTransformer::VccTransformer
   ( Vcc* apVcc
   , VccCalcDef* apVccCalcDef
   , OpDef* apOpDef
   , const ModalIntegrals<Nb>* const apIntegralsOrig
   , Nb aEvmp0
   , In aISO
   )
   : Transformer()
   , mType(TRANSFORMER::UNKNOWN)
   , mpVcc(apVcc)
   , mpVccCalcDef(apVccCalcDef)
   , mpOpDef(apOpDef)
   , mpOneModeModalBasisIntOrig(apIntegralsOrig)
   , mOneModeModalBasisInt(*mpOneModeModalBasisIntOrig)
   , mEvmp0(aEvmp0)
   , mVciVcc(false)
   , mLambdaCalc(false)
   , mLambda(C_0)
   , mpExpTXvec(NULL)
{
   mT1TransH = apVccCalcDef->T1TransH();
   
   // Set Xvec names.
   std::string name = mpVccCalcDef->GetName() + std::string("_trialvec");
   mXvec.NewLabel(name,false);
   name += std::string("_trans");
   mTransXvec.NewLabel(name,false);

   // Initialize Xvecs.
   mTransXvec.ReInit(mpVccCalcDef, mpOpDef, aISO);
   if (mpVccCalcDef->SizeIt()) 
   {
      Mout << " Size of mTransXvec: " << sizeof(mTransXvec) << endl
           << "    Total size:      " << mTransXvec.SizeOut() << endl;
   }

   if (mpVccCalcDef->Vcc() && mpVccCalcDef->VccOutOfSpace())
   {
      mXvec.ReInit(mpVccCalcDef, mpOpDef, I_0, true, &mTransXvec);
   }
   else
   {
      mXvec.CopyNoData(mTransXvec);
   }
   
   // print sizes if requested
   if (mpVccCalcDef->SizeIt()) 
   {
      Mout << " Size of mXvec:      " << sizeof(mXvec) << endl
           << "    Total size:      " << mXvec.SizeOut() << endl;
   }

   // Some assertions on correspondence between OpDef, ModalIntegrals and
   // excitation range.
   std::stringstream ss_err;
   if (this->GetOpDef()->NmodesInOp() == this->mpOneModeModalBasisIntOrig->NModes())
   {
      const Uin n_modes = this->GetOpDef()->NmodesInOp();
      // For now, we're assuming that the ModeCombiOpRange of mXvec/mTransXvec
      // must contain (M being number of modes in operator)
      // -  same number of modes as operator
      // -  the empty mode combination
      // -  "canonical" 1-mode combinations, i.e. {0},...,{M-1}.
      // -  no modes < 0 or >= M.
      // If at some point you find that these requirements are too conservative,
      // go ahead and modify accordingly.
      if (!ValidateExciMcr(this->mXvec.ExciRange(), n_modes, ss_err, Mout))
      {
         // If ValidateExciMcr returned false, detailed info has been output to
         // Mout and ss_err contains more concise error info.
         MIDASERROR
            (  std::string("For VccTransformer::mXvec (num. modes expected from OpDef: ")
            +  std::to_string(n_modes)
            +  "):\n"
            +  ss_err.str()
            );
      }
      if (!ValidateExciMcr(this->mTransXvec.ExciRange(), this->GetOpDef()->NmodesInOp(), ss_err, Mout))
      {
         // If ValidateExciMcr returned false, detailed info has been output to
         // Mout and ss_err contains more concise error info.
         MIDASERROR
            (  std::string("For VccTransformer::mTransXvec (num. modes expected from OpDef: ")
            +  std::to_string(n_modes)
            +  "):\n"
            +  ss_err.str()
            );
      }
   }
   else
   {
      ss_err
         << "Operator num. modes (which is " << this->GetOpDef()->NmodesInOp()
         << ") != ModalIntegrals num. modes (which is " << this->mpOneModeModalBasisIntOrig->NModes()
         << ")."
         ;
      MIDASERROR(ss_err.str());
   }
}

/**
 * New constructor for Improved Preconditioning
 **/
VccTransformer::VccTransformer
   (  VccTransformer& arTr
   ,  In aPrecondExciLevel
   ,  bool aAllowOutOfSpaceXvec
   )
   : Transformer()
   , mType(arTr.mType)
   , mpVcc(arTr.mpVcc)
   , mpVccCalcDef(arTr.mpVccCalcDef)
   , mpOpDef(arTr.mpOpDef)
   , mpOneModeModalBasisIntOrig(arTr.mpOneModeModalBasisIntOrig)
   , mOneModeModalBasisInt(arTr.mOneModeModalBasisInt)
   , mEvmp0(arTr.mEvmp0)
   , mVciVcc(false)
   , mLambdaCalc(false)
   , mLambda(C_0)
   , mpExpTXvec(NULL)
{
   // Set Xvec names.
   string name = mpVccCalcDef->GetName() + "2_trialvec";
   mXvec.NewLabel(name,false);
   name = name + "_trans";
   mTransXvec.NewLabel(name,false);

   In orig_exci_level = arTr.mTransXvec.NexciLevels()-I_1; // Exci level in the original Trf
   // Exci level in prec is aPrecondExciLevel

   bool orig_trf_fast=false;
   if (arTr.UseVcc2Trans())
      orig_trf_fast = true; // Original Trf: orig_trf_fast=true -> FAST; orig_trf_fast=false -> GENERAL
   bool der_trf_fast=false; // derived Trf

   // Currently, only (FAST,FAST) and (GEN,GEN) cases are supported so
   der_trf_fast=orig_trf_fast;

   // mT1TransH should not need to be changed
   mT1TransH = mpVccCalcDef->T1TransH();

   bool orig_VccOutOfSpace= mpVccCalcDef->VccOutOfSpace();
   if (  der_trf_fast   )
   {
      mpVccCalcDef->SetVccOutOfSpace(false);
   }

   bool original_twomode  = arTr.mpVccCalcDef->TwoModeTrans();

   // case (FAST2,FAST1) Rsp or CI
   if (orig_trf_fast && orig_exci_level==I_2 && aPrecondExciLevel==I_1)
   {
      mpVccCalcDef->SetTwoModeTrans(false);
      mpVccCalcDef->SetOneModeTrans(true);
   }

   // case (GEN,GEN) we may allow out-of-space excitations in mXvec
   if (  mType == TRANSFORMER::VCCJAC 
      && !orig_trf_fast
      && aAllowOutOfSpaceXvec
      )
   {
      if (!mpVccCalcDef->VccOutOfSpace()) mpVccCalcDef->SetVccOutOfSpace(true); // maybe unnecessary since it is false anyway
      Mout << " In case (GEN,GEN) Rsp mpVccCalcDef->VccOutOfSpace() " << StringForBool(mpVccCalcDef->VccOutOfSpace()) << endl;

      // Initialization specific for VCC response which is implemented using the out-of-space method.
      mpExpTXvec = new Xvec;
      mpExpTXvec->NewLabel("vcc_amps", false);
//      arTr.mTransXvec.ConvertToDataCont();
      mpExpTXvec->CopyNoData( arTr.mTransXvec );    // Set to size of VCC space.

      // mTransXvec corresponds to respons vector coefficients. Reduce size.
      mTransXvec.ReInit2( mpVccCalcDef, aPrecondExciLevel, &arTr.mTransXvec, mpOpDef );

      // mXvec is intermediate "out-of"-space.
      mXvec.ReInit( mpVccCalcDef, mpOpDef, I_0, true, &mTransXvec );
   }
   else
   {
//      arTr.mTransXvec.ConvertToDataCont();
//      arTr.mXvec.ConvertToDataCont();

      // if ImprovedPrecond use ReInit2 to initialze an Xvec
      mTransXvec.ReInit2( mpVccCalcDef, aPrecondExciLevel, &arTr.mXvec, mpOpDef );

      // Simply copying  the vector
      mXvec.CopyNoData( mTransXvec );
   }

   // Initialize in the case of (FAST2,FASTX) Rsp
   if (this->UseVcc2Trans())
   {
      Mout << "Vcc::IMPROVED PREC: Initializing (one)two-mode jacobian transformer stuff... " << endl;
      this->TwoModeVccRspInit();
   }

   if (mpVccCalcDef->SizeIt()) 
   {
      Mout << " Size of mXvec:      " << sizeof(mXvec) << endl
         << "    Total size:      " << mXvec.SizeOut() << endl;
      if (NULL != mpExpTXvec)
         Mout << " Size of mExpTXvec:  " << sizeof(*mpExpTXvec) << endl
            << "    Total size:      " << mpExpTXvec->SizeOut() << endl;

      // Peter extra
      Mout << " mTransXvec:" << endl << mTransXvec.GetModeCombiOpRange() << endl
           << " mXvec:     " << endl << mXvec.GetModeCombiOpRange() << endl;
      if (NULL != mpExpTXvec)
         Mout << " mpExpTXvec:" << endl << mpExpTXvec->GetModeCombiOpRange() << endl;
   }

   // Set variables to the original
   // These variables are changed back and forth in InverseMatTimesVec
   if (original_twomode)
   {
      mpVccCalcDef->SetTwoModeTrans(true);
      mpVccCalcDef->SetOneModeTrans(false);
   }
   mpVccCalcDef->SetVccOutOfSpace(orig_VccOutOfSpace);
}

/**
 *
 **/
VccTransformer::~VccTransformer
   (
   )
{
   if (NULL != mpExpTXvec)
      delete mpExpTXvec;
}

/**
* Initialize mTransXvec to be out-of-space.
* This requires mXvec to be in-space, i.e. the VccOutOfSpace flag must not be set i VccCalcDef.
*/
void VccTransformer::InitOOSTransXvec()
{
   mTransXvec.ReInit(mpVccCalcDef, mpOpDef, I_0, true, &mXvec);
}

/**
* Transforming arDcIn by H/H0 to arDcOut 
* aI = I_0 zeroth order ham
* aI = I_1 usual VCI
* aI = I_2 usual VCI, but some technicalities is designed for PreDiag VCI in a VCC contexts.
* */
void VccTransformer::TransH(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb aNb)
{
   if (aI==I_0 || aI==I_1) 
   {
      mXvec.GetXvecData().NewLabel(arDcIn.Label(),false); 
      mTransXvec.GetXvecData().NewLabel(arDcOut.Label(),false);
      mXvec.GetXvecData() = arDcIn; 
   }
   //TEST else if (aI == I_2) arDcIn.Reassign(mXvec.GetXvecData(),I_0,-I_1);
   else if (aI == I_2) 
   {
      Nb zero = C_0;
      mXvec.GetXvecData().DataIo(IO_PUT,I_0,zero);
      arDcIn.Reassign(mXvec.GetXvecData(),I_1,-I_1);
   }
   else
   {
      MIDASERROR("aI set wrong");
   }
   // if both on disc, this should have no effect, else it is needed.
   // Mout << " TransH arDcIn          " << arDcIn << endl;
   // Mout << " TransH mXvec.mXvecData " << mXvec.mXvecData << endl;
   if (gDebug || mpVccCalcDef->IoLevel() > I_13)
   {
      Mout << "VccTransformer::TransH():" << endl;
      Mout << "   mLabdaCalc =                           " << StringForBool(mLambdaCalc) << endl;
      Mout << "   aI, aJ =                               " << aI << ", " << aJ << endl;
      Mout << "   TransH arDcin               Label      " << arDcIn.Label() << endl;
      Mout << "   TransH arDcOut              Label      " << arDcOut.Label() << endl;
      Mout << "   TransH mXvec                Label      " << mXvec.GetXvecData().Label() << endl;
      Mout << "   TransH mTransXvec           Label      " << mTransXvec.GetXvecData().Label() << endl;
      Mout << "   TransH Current operator     Label      " << mpOpDef->Name() << endl;
      Mout << "   TransH arDcin               size       " << arDcIn.Size() << endl;
      Mout << "   TransH arDcOut              size       " << arDcOut.Size() << endl;
      Mout << "   TransH GetXvecData()            size begin " << mXvec.GetXvecData().Size() << endl;
      Mout << "   TransH mTransXvecData       size begin " << mTransXvec.GetXvecData().Size() << endl;
      Mout << "   TransH arDcin               storage    " << arDcIn.Storage() << endl;
      Mout << "   TransH arDcOut              storage    " << arDcOut.Storage() << endl;
      Mout << "   TransH mXvec.GetXvecData()      storage    " << mXvec.GetXvecData().Storage() << endl;
      Mout << "   TransH mTransXvec.GetXvecData() storage    " << mTransXvec.GetXvecData().Storage()
                                                           << endl;
      Mout << "   Norm Xvec : " << mXvec.GetXvecData().Norm() << endl;
      Mout << "   Norm arDcin: " << arDcIn.Norm() << endl;
   }
   if (!mLambdaCalc)
   {
      if (mpVccCalcDef->SumNprim()&&aI==1) SumNpurify(arDcIn);
      if (mpVccCalcDef->Emaxprim()&&aI==1) Emaxpurify(arDcIn);
      if (mpVccCalcDef->Zeroprim()&&aI==1) Zeropurify(arDcIn);
      if (aI==I_0)
      {
         if (mpVccCalcDef->GetTrueHDiag()) TransformerTrueHDiag(aJ,aNb);
         else TransformerH0(aJ,aNb); 
      }
      if (aI==I_1)
      {
         if (this->UseVcc2Trans())
         {
            const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::VCI);
            p_vcc2trans->Transform(Vcc2GetTAmps(), mTransXvec.GetXvecData());
         }
         else
         {
            // Mout << "General transformer..." << endl;
            // clock_t t0 = clock();
            GenTransformer(false);
            // Mout << "Time: " << Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
         }
      }
      if (aI==I_2)
         GenTransformer(true);   // Two-mode transformer should be used if relevant.
      if (aI==I_0 || aI==I_1) 
         arDcOut = mTransXvec.GetXvecData(); 
      // TEST
      else if (aI == I_2) 
         arDcOut.Reassign(mTransXvec.GetXvecData(),I_1,I_1);
      // If both on disc, this should have no effect, else it is needed.
      if (mpVccCalcDef->SumNprim() && aI==1) SumNpurify(arDcOut);
      if (mpVccCalcDef->Emaxprim() && aI==1) Emaxpurify(arDcOut);
      if (mpVccCalcDef->Zeroprim() && aI==1) Zeropurify(arDcOut);
   }
   else
   {
      if (aI==I_0) 
      {
         TransformerH0(aJ,aNb);  // H0 as usual
         arDcOut = mTransXvec.GetXvecData(); 
         // if both on disc, this should have no effect, else it is needed.
      }
      if (aI==I_1) 
      {
         if (gDebug) Mout << " TransH mLambda =  " << mLambda << endl;
         mTransXvec.GetXvecData().NewLabel(arDcOut.Label()+"_lambda",false);
         GenTransformer(false);
         if (gDebug) Mout  << " Norm of trans vec " 
            << mTransXvec.GetXvecData().Norm() << endl;
         arDcOut = mTransXvec.GetXvecData(); 
         // if both on disc, this should have no effect, else it is needed.
         if (gDebug) Mout  << " Norm of arDcout 1 " 
            << arDcOut.Norm() << endl;
         arDcOut.Scale(mLambda);
         if (gDebug) Mout  << " Norm of arDcout 2 " 
            << arDcOut.Norm() << endl;
         TransformerH0(I_1,C_0); // NB Transformer H0 transforms with -H0!!!
         Nb x = -C_1+mLambda;
         if (gDebug) Mout << " TransH C_1-mLambda =  " << x << endl;
         arDcOut.Axpy(mTransXvec.GetXvecData(),x);
      }
      else
         MIDASERROR( " No implemented lambda with aI diff from 0,1 in TransH" );
   }
   if (gDebug || mpVccCalcDef->IoLevel() > I_11)
   {
      Mout << "Norm TransXvec: " << mTransXvec.GetXvecData().Norm() << std::endl;
      Mout << "Norm arDcOut:   " << arDcOut.Norm() << std::endl;
   }

   // Set to dummy labels so no clashes happen due to both input DataConts and mXvec/mTransXvec
   // points to same files. 
   string dummy_v = "dummy_xvec";
   string dummy_vt = "dummy_xvectrans";
   mXvec.GetXvecData().NewLabel(dummy_v,false); 
   mTransXvec.GetXvecData().NewLabel(dummy_vt,false);
}

/**
* Prepare for PreDiagonalization transformations 
* */
void VccTransformer::PreparePreDiagTrans()
{
   Mout << " VccTransformer::PreparePreDiagTrans - nothing done so far " << endl;
}

/**
* PreDiagonalization transformer - pass simple control to TransH
* */
void VccTransformer::TransPreDiag(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb aNb)
{
   TransH(arDcIn,arDcOut,aI,aJ,aNb);
}

/**
* Restore after PreDiagonalization transformations 
* */
void VccTransformer::RestoreAfterPreDiagTrans()
{
   Mout << " VccTransformer::RestoreAfterPreDiagTrans - nothing done so far " << endl;
}

/**
* Purify based on sum of quantum numbers 
* */
void VccTransformer::SumNpurify(DataCont& arDc) 
{
   //Mout << " purify container " << arDc.Label() << endl;
   //Mout << " sumn max is      " << mpVccCalcDef->SumNThr() << endl;
   string lowhig = "LOWHIG";
   bool exci_only=true; 

   In i_mode_combi_p = -1;
   Nb zero = C_0;
   for(  auto ip = mTransXvec.ExciRange().begin(), end = mTransXvec.ExciRange().end()
      ;  ip != end
      ;  ++ip
      )
   {
      i_mode_combi_p++;
      const ModeCombi& mp = *ip; // mTransXvec.GetModeCombi(i_mode_combi_p);
      In n_exci_mp = mTransXvec.NexciForModeCombi(i_mode_combi_p);
      In ndim0 = mp.Size();
      vector<In> occ0(ndim0);
      vector<In> occmax0(ndim0);
      for (In i=0;i<ndim0;i++) 
      {
         occ0[i] = I_0;
         In i_op_mode =  mp.Mode(i);
         In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
         occmax0[i]   = nmod-I_1;
      }
      MultiIndex mi0(occ0,occmax0,lowhig,exci_only);
      In n0 = mi0.Size();
      if (n0!=n_exci_mp) MIDASERROR(" n0 is not n_exci_mp");
      vector<In> tmp(occ0);
      for (In i_exci=I_0;i_exci<n_exci_mp;i_exci++)
      {
         mi0.IvecForIn(tmp,i_exci);
         In sumn = I_0;
         for (In i=0;i<ndim0;i++) sumn+= tmp[i];
         if (sumn > mpVccCalcDef->SumNThr())
         {
            //Mout << " Sum N prim: Skip " << tmp << endl;
            //Mout << " address " 
            //<< mTransXvec.ModeCombiAddress(i_mode_combi_p)+i_exci << endl;
            arDc.DataIo(IO_PUT, mTransXvec.ModeCombiAddress(i_mode_combi_p)+i_exci,zero);
         }
      }
   }
}

/**
* Purify based on sum of quantum numbers 
* */
void VccTransformer::Emaxpurify(DataCont& arDc) 
{
   //Mout << " purify container " << arDc.Label() << endl;
   Mout << " Emax is      " << mpVccCalcDef->EmaxThr() << endl;
   Mout << " TO BE DONE " << endl; 
}

/**
* Purify based on sum of quantum numbers 
* */
void VccTransformer::Zeropurify(DataCont& arDc) 
{
   Mout << " purify container for close to zero but not complete zero: " << arDc.Label() << endl;
   In n_zeros=I_0; 
   In n_new_zeros=I_0; 
   In n_dc = arDc.Size(); 
   Nb min_size_not_zero =   mpVccCalcDef->ZeroThr(); 
   //if (arDc.InMem())  // ONly efficient in mem, but....
   Nb current_value; 
   for (In i=I_0;i<n_dc;i++)
   {
      arDc.DataIo(IO_GET,i,current_value); 
      if (fabs(current_value)==C_0) 
      {  
         n_zeros++; 
      }  
      else if (fabs(current_value)<min_size_not_zero) 
      {  
         current_value=C_0; 
         arDc.DataIo(IO_PUT,i,current_value); 
         n_new_zeros++; 
      } 
   } 
   Mout << " ZeroThr " << min_size_not_zero << endl; 
   Mout << " Already zero " << n_zeros << " Close to and now zero " << n_new_zeros 
        << " Not zero " << n_dc-n_zeros-n_new_zeros << " total; " << n_dc << " percentage zero now = " << C_10_2*(n_zeros+n_new_zeros)/n_dc << endl; 
}

/**
* Check if any purifies (used in eqs solvs) 
* */
void VccTransformer::CheckPurifies(DataCont& arDc) 
{
   //if (mpVccCalcDef->SumNprim()) SumNpurify(arDc);  // Not needed because never in that space 
   //if (mpVccCalcDef->Emaxprim()) Emaxpurify(arDc); 
   //Mout << " Check Purifies " << endl; 
   if (mpVccCalcDef->Zeroprim()) Zeropurify(arDc); 
}

/**
* Do transformation by H0*mXvec for positive arInv, by (H0-E) for negative arInv.
* arInv = +-1; Absolute transformation, - -> P(H-E)-1P, assignment to result
* arInv = +-2; Addto -2 means P(H-E)-1P, addition to result
* arInv < -2; (H0-E)^-1, assignment to result
* */
void VccTransformer::TransformerTrueHDiag(const In& arInv, const Nb& arE)
{
   Mout << "In VccTransformer::TransformerTrueHDiag" << endl;
   clock_t t0 = clock();

   if (gDebug || mpVccCalcDef->IoLevel() > I_5) 
   {
      Mout << " In VccTransformer::TransformerTrueHDiag  "<<endl; 
      if (arInv > I_0 ) Mout << " transform by -Hdiag " << endl;
      if (arInv < I_0 && -I_2 < arInv) Mout << " transform by -P(Hdiag-E^0)-1P " << endl;
      if (arInv == I_1|| arInv == -I_1) Mout << " Result is assigned to mTransXvec " << endl;
      if (arInv == I_2|| arInv == -I_2) Mout << " Result is subtracted from mTransXvec " << endl;
      if (arInv < -I_2 ) Mout << " transform by -(Hdiag-E)^-1, E = " << arE << endl;
   }

   Nb e_ref = mpVcc->GetEtot() - arE;

   In n_mode_combi_p = mTransXvec.ExciRange().Size();
   for (In i_mode_combi_p=0;i_mode_combi_p<n_mode_combi_p;i_mode_combi_p++)
   {
      const ModeCombi& mp = mTransXvec.ExciRange().GetModeCombi(i_mode_combi_p);
      In n_exci_mp = mTransXvec.NexciForModeCombi(i_mode_combi_p);
      //if (n_exci_mp < I_1) continue; 

      //MidasVector sigma_mp(n_exci_mp,C_0); 
      MidasVector sigma_mp(n_exci_mp); 
      // if an AddTo calculation, then read in the old transformed calculation.
      if (arInv==I_2||arInv == -I_2)
      {
         //mTransXvec.GetXvecData().DataIo(IO_GET,sigma_mp,n_exci_mp,mp.Address());
         mTransXvec.GetModeCombiData(sigma_mp, mp);
      }

      // Get the vector to transform
      MidasVector xvec_mp(n_exci_mp); 
      //mXvec.GetXvecData().DataIo(IO_GET,xvec_mp,n_exci_mp,mp.Address());
      mXvec.GetModeCombiData(xvec_mp, mp);

      if (i_mode_combi_p == 0) 
      {
         if (arInv > 0) sigma_mp -= e_ref*xvec_mp;
         if (arInv < -I_2) 
         {
            Nb einv = C_1/e_ref;
            sigma_mp -= einv*xvec_mp;
         }
      }
      else
      {
         MidasVector corr(n_exci_mp,C_0);
        
         for (In i_term=I_0; i_term<mpOpDef->Nterms(); ++i_term)
         {
            MidasVector dir_prod(I_1, C_1);
            //Mout << "Doing term: " << i_term << " for mp=" << mp << endl;
            if (!mpOpDef->TermActiveForModeCombi(i_term,mp))
               continue;

            ModeCombi mt(mpOpDef->Nterms());
            Nb coef = mpOpDef->Coef(i_term);
            Nb prod_of_factors_not_in_mp  = C_1;
            Nb to_substract  = C_1;

            for (In i_fac=0;i_fac<mpOpDef->NfactorsInTerm(i_term);i_fac++)
            {
               //Mout << "   i_fac=" << i_fac << endl;
               LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
               LocalOperNr i_oper = mpOpDef->OperForFactor(i_term,i_fac);
               mt.InsertMode(i_op_mode);
               Nb occ = pOneModeModalBasisInt()->GetOccElement(i_op_mode, i_oper);
               to_substract *= occ;
               if (!mp.IncludeMode(i_op_mode))
                  prod_of_factors_not_in_mp *= occ;
            }

            for (In i_mode=0;i_mode<mp.Size();++i_mode)
            {
               //Mout << "   i_mode=" << i_mode << endl;
               LocalModeNr mode = mp.Mode(i_mode);
               In excit = mpVccCalcDef->Nmodals(i_mode)-I_1;

               MidasVector integral(excit,C_1);
               if (mt.IncludeMode(mode))
               {
                 // Mout << "      in mt" << endl;
                  LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,mode);
                  for (In i_modal=0;i_modal<excit;i_modal++)
                  {
                     integral[i_modal] = pOneModeModalBasisInt()->GetElement(mode,i_oper,i_modal+I_1,i_modal+I_1);

                  }
                  //Mout << "      integrals: " << integral;
               }
               MidasVector tmp_dir_prod(dir_prod);
               In size_of_dir_prod = dir_prod.Size()*excit;
               dir_prod.SetNewSize(size_of_dir_prod);
               In size_integral = integral.Size();

               for (In idx1=I_0; idx1<tmp_dir_prod.Size(); ++idx1)
                  for (In idx2=I_0; idx2<size_integral; ++idx2)
                     dir_prod[idx1*(size_integral)+idx2] = tmp_dir_prod[idx1] * integral[idx2];
               //Mout << "      dir_prod: " << dir_prod;
            }
            //Mout << "   final dir_prod: " << dir_prod;
            //Mout << "   coef = " << coef << endl;
            //Mout << "   prod_of_factors_not_in_mp = " << prod_of_factors_not_in_mp << endl;
            //Mout << "   to_subtract: " << to_substract << endl;
            for(In i=0;i<n_exci_mp; ++i)
               corr[i] += -coef * to_substract + coef * prod_of_factors_not_in_mp * dir_prod[i];
         } 
         //Mout << "   corr = " << corr;
         for(In i=0;i<n_exci_mp; ++i)
         {
            Nb diag = e_ref + corr[i];
            if (arInv > 0) sigma_mp[i] -= diag*xvec_mp[i];
            if (arInv < -I_2) 
            {
               Nb einv = C_1/diag;
               sigma_mp[i] -= einv*xvec_mp[i];
            }
         }
      }            
      //mTransXvec.GetXvecData().DataIo(IO_PUT,sigma_mp,n_exci_mp,mp.Address());
      mTransXvec.SetModeCombiData(sigma_mp, mp);
      //Mout << "mp = " << mp << endl;
   }
   Mout << "TransformerTrueHDiag time: " << Nb(clock()-t0) / CLOCKS_PER_SEC << " sec" << endl;
}

/**
* Do transformation by H0*mXvec for positive arInv, by (H0-E) for negative arInv.
* arInv = +-1; Absolute transformation, - -> P(H-E)-1P, assignment to result
* arInv = +-2; Addto -2 means P(H-E)-1P, subtract from result
* arInv < -2; (H0-E)^-1, assignment to result
**/
void VccTransformer::TransformerH0
   (  const In& arInv
   ,  const Nb& arE
   )
{
   if (gDebug || mpVccCalcDef->IoLevel() > I_14) 
   {
      Mout << " In VccTransformer::TransformerH0()  " << std::endl
           << "    arE = " << arE << std::endl;
      if (arInv > I_0 )
      {
         Mout << "    transform by -H0 " << std::endl;
      }
      if (arInv < I_0 && -I_2 < arInv)
      {
         Mout << "    transform by -P(H0-E^0)-1P " << std::endl;
      }
      if (arInv == I_1 || arInv == -I_1)
      {
         Mout << "    Result is assigned to mTransXvec " << std::endl;
      }
      if (arInv == I_2 || arInv == -I_2)
      {
         Mout << "    Result is subtracted from mTransXvec " << std::endl;
      }
      if (arInv < -I_2 )
      {
         Mout << "    transform by -(H0-E)^-1" << std::endl;
      }
   }
   
   Nb e_diag = mEvmp0 - arE;

//   Mout  << " DEBUG: TransformerH0 with mEvmp0 = " << mEvmp0 << "  and  arE = " << arE << std::endl;
   
   In n_mode_combi_p = mTransXvec.ExciRange().Size();
   for (In i_mode_combi_p=0; i_mode_combi_p<n_mode_combi_p; ++i_mode_combi_p)
   {
      const ModeCombi& mp = mTransXvec.ExciRange().GetModeCombi(i_mode_combi_p);
      
      In n_exci_mp = mTransXvec.NexciForModeCombi(i_mode_combi_p);
      
      if (gDebug || mpVccCalcDef->IoLevel() > I_14)  // debug output
      {
         Mout << " The modes excited in mTransXvec i_mode_combi_p " 
              << i_mode_combi_p << " are: " << mp << std::endl;
         Mout << " The number of excitation for this mode combi: " 
              << n_exci_mp << std::endl;
      }

      if (n_exci_mp < I_1) continue; // if there are no amps for current modecombi we continue!

      MidasVector sigma_mp(n_exci_mp,C_0);

      // if an AddTo calculation, then read in the old transformed calculation.
      if (gDebug || mpVccCalcDef->IoLevel() > I_11)  // debug output
      {
         Mout << " mode combination " << mp 
              << " trans vect. norm  before read " 
              << sigma_mp.Norm() << endl;
      }

      if (arInv==I_2||arInv == -I_2) 
      {
         //mTransXvec.GetXvecData().DataIo(IO_GET,sigma_mp,n_exci_mp,mp.Address());
         mTransXvec.GetModeCombiData(sigma_mp, mp);
      }
      
      if (gDebug || mpVccCalcDef->IoLevel() > I_11)  // debug output
      {
         Mout << " mode combination " << mp 
              << " trans vect. norm  after  read " 
               << sigma_mp.Norm() << endl;
      }

      // Get the vector to transform
      MidasVector xvec_mp(n_exci_mp);
      if(mpVccCalcDef->Iso() && this->VccType() == "VccTransformer") // if ISO and old transformer we use a hardcoded DataCont interface
      {
         mXvec.GetXvecData().DataIo(IO_GET,xvec_mp,n_exci_mp,mp.Address()); // Iso way :C (mp is mode combi of TransXvec,
                                                                            // but for ISO excirange is different for Xvec and TransXvec.)
      }
      else // else we do it in the 'correct' data-independent way
      {
         mXvec.GetModeCombiData(xvec_mp, mp); // correct way
      }
      
      if (gDebug || mpVccCalcDef->IoLevel() > I_11)
      {
         Mout << " mode combination " << mp 
              << " trial vect. norm  after  read " 
              << xvec_mp.Norm() << endl;
      }

      if (i_mode_combi_p == 0)
      {
         if (arInv > 0) 
         {
            sigma_mp -= e_diag*xvec_mp;
         }

         if (arInv < -I_2) 
         {
            Nb einv = C_1/e_diag;
            sigma_mp -= einv*xvec_mp;
         }
      }
      else
      {
         string lowhig = "LOWHIG";
         bool exci_only = true; 

         In ndim0 = mp.Size();
         vector<In> occ0(ndim0);
         vector<In> occmax0(ndim0);

         for (In i=0; i<ndim0; ++i) 
         {
            occ0[i]      = I_0;
            In i_op_mode = mp.Mode(i);
            In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
            occmax0[i]   = nmod-I_1;
         }
         MultiIndex mi0(occ0,occmax0,lowhig,exci_only);
         In n0 = mi0.Size();

         vector<In> i0vec(ndim0);
         for (In i0=I_0; i0<n0; ++i0)
         {
            Nb fact = e_diag;
            mi0.IvecForIn(i0vec,i0);
            for (In im=I_0; im<ndim0; ++im)
            {
               fact += mpVcc->GetEigVal(mp.Mode(im),i0vec[im]);
            }
            if (arInv < 0 ) fact = C_1/fact;
            sigma_mp[i0] -= fact*xvec_mp[i0];
         }
      } 

      // store the result 
      if (gDebug || mpVccCalcDef->IoLevel() > I_11) 
         Mout << " mode combination " << mp << " trans vect. norm " << sigma_mp.Norm() << std::endl;

      mTransXvec.SetModeCombiData(sigma_mp, mp);
   }// for end
}

/**
* Transforming Xvec - result vector is put in mXtransVec. 
* aPreDiagTrans is only to use transformer as a usual VCI transformed within a VCC context.
* In the VCC contexts Xved and TransXvec ranges would be different.
* */
void VccTransformer::GenTransformer(bool aPreDiagTrans) 
{
   if (gDebug || mpVccCalcDef->IoLevel() > I_10)
   {
      Mout << " Vcc: Transformer Man - you run the show." << std::endl;
   }
   
   In n_mode_combi_p = NexciTypesTransXvec();
   In n_mode_combi_q = NexciTypesXvec();
   bool use_trans_op_range_for_xvec_op_range = aPreDiagTrans;
   if (mpVccCalcDef->IoLevel() > I_14 || gDebug) 
   {
      Mout << "In VccTransformer::GenTransformer():" << endl
           << "      mVciVcc             is  " << StringForBool(mVciVcc)  << endl
           << "      use_trans_op_range_for_xvec_op_range "
           << StringForBool(use_trans_op_range_for_xvec_op_range) << endl
           << "      n_mode_combi vector " << n_mode_combi_q << endl
           //<< "      mXvec MCR" << endl << mXvec.ExciRange() << endl
           << "      n_mode_combi result " << n_mode_combi_p << endl
           //<< "      mTransXvec MCR" << endl << mTransXvec.ExciRange() << endl
           << "      mXvec:      - label  " << mXvec.GetXvecData().Label() << endl
           << "      mTransXvec: - label  " << mTransXvec.GetXvecData().Label() << endl
           << "      mXvec:      - size   " << mXvec.GetXvecData().Size() << endl
           << "      mTransXvec: - size   " << mTransXvec.GetXvecData().Size() << endl
           << "      mXvec: " << mXvec.GetXvecData() << endl;
   }

   // In VCC this transformer is also called to do transformations in the MICRO iterations
   // in which case we might not want to use the T1-transformed integrals.
   //if (mT1TransH && !mVciVcc)
   //{
   //   // Update integrals in reqular fashion.
   //   UpdateModalIntegrals(false);
   //}

   //Mout << " Check mXvec status Transformer" << endl;
   //mXvec.ExciRange().CheckStatus();
   //Mout << " Check mTransXvec status Transformer" << endl;
   //mXvec.ExciRange().CheckStatus();
   ModeCombi mact;                       // The container for the active mode combi
                                         // - the common ones between mp and mq
   ModeCombi m_common;
   ModeCombi mp_not_in_mq;
   ModeCombi mq_not_in_mp;
   std::vector<ModeCombi> mq_for_mp;

   In i_vcc_contractor = I_0;
   In i_up             = I_0;
   In i_down           = I_0;
   In i_forward        = I_0;

   // Loop over mode combinations in the result  (mTransXvec)

   //In i_mode_combi_p = -1;
   Nb e_tot=C_0;
   for (In i_mode_combi_p=I_0; i_mode_combi_p<n_mode_combi_p; i_mode_combi_p++)
   //for (ModeCombiOpRange::const_iterator ip=mTransXvec.ExciRange().Begin();
   //ip!=mTransXvec.ExciRange().End();ip++)
   //for (ModeCombiOpRange::const_iterator 
         //ip=mTransXvec.ExciRange().mModeCombis.begin();
         //ip!=mTransXvec.ExciRange().mModeCombis.end();ip++)
   {
      //i_mode_combi_p++;
      //const ModeCombi& mp = *ip; // mTransXvec.GetModeCombi(i_mode_combi_p);
      const ModeCombi& mp = mTransXvec.ExciRange().GetModeCombi(i_mode_combi_p);
      //In n_excited = mp.Size();
      In n_exci_mp = mTransXvec.NexciForModeCombi(i_mode_combi_p);
      if (mpVccCalcDef->IoLevel() > I_14) 
      {
         Mout << " The modes excited in mTransXvec i_mode_combi_p = " << i_mode_combi_p
              << " are:" << mp << std::endl
              << " The number of excitation for this mode combi: " << n_exci_mp << std::endl;
      }
      if (n_exci_mp < I_1) continue;
      if (i_mode_combi_p == I_0)
      {
         if (mp.Size() != I_0)
           MIDASERROR("I Thought that the mode combi nr. 0 was {}?");
         if (n_exci_mp != I_1)
           MIDASERROR("I Thought that there was only E for combi nr. 0.");
      }

      MidasVector sigma_mp(n_exci_mp,C_0);

      // Diagonal contribution:
      // sigma^mp <- F^pas_mp * C^mp_p    
      // F^pas_mp = sum_(t-pas) C^t prod_m(m does not belong to mp) h_pmpmp,t

      // Calculate F factor 
      Nb f_factor = C_0;
      for (In i_term = I_0; i_term < mpOpDef->Nterms(); i_term++)
      {
         if (!mpOpDef->TermActiveForModeCombi(i_term, mp))
         {
            Nb fact = mpOpDef->Coef(i_term);
            if (fabs(fact) < mpVccCalcDef->ScreenZero())
            {
               continue;
            }
      
            for (In i_fac = I_0; i_fac < mpOpDef->NfactorsInTerm(i_term); i_fac++)
            {
               LocalModeNr i_op_mode = mpOpDef->ModeForFactor(i_term, i_fac);
               //if (gDebug) Mout << " term " << i_term << " factor " << i_fac 
                  //<< " mode " << i_op_mode << endl;
               // Ove revision 070703: As I see it this test can be delete, since
               // the above TermActiveForModeCombi test should have taken us away if
               // that was the case.
               //if (!mp.IncludeMode(i_op_mode))
               //{
               //if (gDebug) Mout << " i_op_mode " << i_op_mode << endl;

               LocalOperNr i_oper = mpOpDef->OperForFactor(i_term, i_fac);
               fact *= pOneModeModalBasisInt()->GetOccElement(i_op_mode, i_oper);
            }
            if (gDebug)
            {
               Mout << " For term " << i_term << " we have fact = " << fact << std::endl;
            }
            f_factor += fact;
         }
      }

      // For the given result Mode combination (mp) and 
      // the knowledge about the operator (OpDef pointer)
      // construct a range of ModeCombinations of the vector 
      // which contributes to the transformation.
      //
      // The contributions allowed are furthermore restricted by the
      // know ModeCombiOpRange of mXvec, if Xvec is allowed to go out
      // of original mXvec space, else it unrestricted (empty restrict range).

      //  Note that mVciVcc denotes a Vcc calc. done using to a large extent Vci
      //  algorithms so first block is only entered for Vcc with No out of space exci
      bool uncon_term_removed = false;
      if (!mpVccCalcDef->VccOutOfSpace() && mVciVcc && !use_trans_op_range_for_xvec_op_range)
      {
         bool remove_sel_unlinked = mpVccCalcDef->VccRemoveSelUnlinked();
         SetToLeftOperRange(mq_for_mp,uncon_term_removed,
            mp,mp, mpOpDef->GetModeCombiOpRange(),
            mTransXvec.GetModeCombiOpRange(),false,false,false,I_0,
            remove_sel_unlinked);
      }
      else if (use_trans_op_range_for_xvec_op_range)
      {
         SetToLeftOperRange(mq_for_mp,uncon_term_removed,
            mp,mp, mpOpDef->GetModeCombiOpRange(),
            mTransXvec.GetModeCombiOpRange());
      }
      else 
      {
         SetToLeftOperRange(mq_for_mp,uncon_term_removed,
            mp,mp, mpOpDef->GetModeCombiOpRange(),
            mXvec.GetModeCombiOpRange());
      }

      In n_mode_combi_q = mq_for_mp.size(); 
      if (mpVccCalcDef->IoLevel() > I_14)
      {
         Mout << " mp " << mp << std::endl;
         Mout << " n_mode_combi for mp_for_mq " << n_mode_combi_q << std::endl;
         //Mout << " The  <Left| H operator range:\n " << mq_for_mp << std::endl;
         Mout << " The  <Left| H operator range:\n ";
         for(const auto& mc: mq_for_mp)
         {
            Mout << mc << '\n';
         }
         Mout << std::flush;
      }

      In i_mode_combi_q=-1;
      ModeCombiOpRange::const_iterator iq_mq;
      //for (In i_mode_combi_q=0;i_mode_combi_q<n_mode_combi_q;
      //i_mode_combi_q++)

      //iq!=mq_for_mp.End();iq++)
      for(  auto iq = mq_for_mp.begin(), end = mq_for_mp.end()
         ;  iq != end
         ;  ++iq
         )
      {
         // If T1 transformed H is used, one-mode mq's will never contribute.
         // These mq's should really never be in mq_for_mp
         // --> Change ModeCombiOpRange::SetToLeftOperRange().
         if (mVciVcc && mT1TransH && iq->Size() == I_1)
            continue;

         // Get the mq, and mact = mq U mp 

         // Find mq in the mXvec list: the iterator and the number 
         In n_exci_mq=I_0;
         bool out_of_exci = false;
         if (!mpVccCalcDef->VccOutOfSpace() && mVciVcc 
              && !use_trans_op_range_for_xvec_op_range)
         {
            i_mode_combi_q = iq->ModeCombiRefNr();
            if (i_mode_combi_q == -I_1) out_of_exci = true;
            else n_exci_mq = mXvec.NexciForModeCombi(i_mode_combi_q);
            //if (mXvec.Find(*iq,iq_mq,i_mode_combi_q))
            //{ 
               //n_exci_mq = mXvec.NexciForModeCombi(i_mode_combi_q);
            //}
            //else
            //{
               //Mout << " The following is out of mode combination range " << *iq << endl;
               //out_of_exci = true;
            //}
            //Mout << " iq   " << *iq;
            //Mout << " orig " << i_mode_combi_q; 
            //Mout << " new  " << (*iq).ModeCombiRefNr() << endl;
         }
         else if (use_trans_op_range_for_xvec_op_range)
         {
            i_mode_combi_q = iq->ModeCombiRefNr();
            //mTransXvec.Find(*iq,iq_mq,i_mode_combi_q); 
            n_exci_mq = mTransXvec.NexciForModeCombi(i_mode_combi_q);
         }
         else // if (!use_trans_op_range_for_xvec_op_range)
         {
            // OTHER SOLUTION: Store not the mode combis but the iterators?
            i_mode_combi_q = iq->ModeCombiRefNr();
            if (-I_1 != i_mode_combi_q)
               n_exci_mq = mXvec.NexciForModeCombi(i_mode_combi_q);
            else
               out_of_exci = true;
         }

         if (n_exci_mq <I_1 && !out_of_exci) continue;
         /// Unclear what the effect of this would be for VCC algo 
         //which goes beyond excitation space??
         ModeCombi mq;
         if (!out_of_exci)
         {
            //const ModeCombi& mqx = mXvec.ExciRange().GetModeCombi(iq_mq);
            const ModeCombi& mqx = 
              mXvec.ExciRange().GetModeCombi(i_mode_combi_q);
            mq = mqx;
            if (use_trans_op_range_for_xvec_op_range) 
            {
               //const ModeCombi& mqy = mTransXvec.ExciRange().GetModeCombi(iq_mq);
               const ModeCombi& mqy = 
                  mTransXvec.ExciRange().GetModeCombi(i_mode_combi_q);
               mq = mqy;
            }
         }
         else
         {
            mq = *iq;
         }

         mact.Union(mp,mq);
         m_common.Intersection(mp,mq);
         mp_not_in_mq.InFirstOnly(mp,mq);
         mq_not_in_mp.InFirstOnly(mq,mp);
         In n_common = m_common.Size();
         In n_mp_not_in_mq = mp_not_in_mq.Size();
         In n_mq_not_in_mp = mq_not_in_mp.Size();

         if (mpVccCalcDef->IoLevel() > I_14) 
         {
            Mout << " mp:               " << mp << std::endl;
            Mout << " mq                " << mq << std::endl; 
            Mout << " *iq               " << *iq << std::endl;
            Mout << " mact :            " << mact << std::endl;
            Mout << " The mp common mq  " << m_common << std::endl;
            Mout << " mp_not_in_mq      " << mp_not_in_mq << std::endl;
            Mout << " mq_not_in_mp      " << mq_not_in_mp << std::endl;
            if (out_of_exci)
            {
               Mout << " out_of_exci space " << std::endl;
            }
         }

         if (!out_of_exci)
         {
            // Get the part of the vector to be transformed. Readin C^mq.
            MidasVector xvec_mq(n_exci_mq);
            if (mVciVcc)
            {
               VciCoefsFromVcc(mq,mq.Address(),xvec_mq,
                     mXvec.GetXvecData(),mXvec,false);
               if (gDebug)
                  Mout << " mq " << mq << " Norm  Cmq = exp(T)mq: " 
                       << xvec_mq.Norm() << endl;
            }
            else
               mXvec.GetXvecData().DataIo(IO_GET, xvec_mq, n_exci_mq, mq.Address());
            
            if (gDebug)
            {
               Mout << " transforming vector for ModeCombi " << mq
                    << " with excitation space size " << n_exci_mq << endl;
               Mout << " xvec_mq " << xvec_mq << endl;
            }
            // Add this first contribution to the part of sigma vector.
            // IF NORM XVEC_MQ = 0 THEN CONTINUE; WAIT FOR DEBUGGING PURPOSES
            // Add this first contribution to the part of sigma vector.
            // Alter this screening, to be f_factor*max of vec.
            if (mp == mq && fabs(f_factor) > mpVccCalcDef->ScreenZero()) 
            {
               if (gDebug) Mout << " The accumulated F factor is = " << 
                  f_factor << " for mp = " << mp << endl;
               sigma_mp += f_factor*xvec_mq; //sne note
               if (gDebug) Mout << " sigma_mp after diagonal contribution : " 
                  << sigma_mp << endl;
            }
            else if (gDebug && mp == mq) // For debugging only 
            {
               Mout << " The accumulated F factor is = " << f_factor 
                  << " for mp = " << mp << endl;
            }
            if (mVciVcc && mp == mq 
             && mpVccCalcDef->VccRemoveSelUnlinked() && i_mode_combi_p>I_0) 
            {
               Nb e_tot2 = e_tot;
               if (uncon_term_removed) e_tot2 = VccEnerForMpLocal(mp,e_tot);
               //Mout << " The used energy for E*C_mup is = " << e_tot2 << endl;
               sigma_mp -= e_tot2*xvec_mq;
            }
   
            // Find maximum size of some vectors 
            // to be used in the contraction 
            In n_xvec_red1 =xvec_mq.Size();
            if (n_mq_not_in_mp >0) 
            {
               In i_op_mode = mq_not_in_mp.Mode(n_mq_not_in_mp-1); 
               // First one to be contracted.
               if (gDebug) Mout << " orig size " << n_xvec_red1 ;
               In nmod = mpVccCalcDef->Nmodals(i_op_mode);
               if (nmod>1) n_xvec_red1 /= (nmod-1);
               if (gDebug) Mout << " new  size " << n_xvec_red1 << endl;
            }
              
            // Loop over operator 
            Nb zact = C_0;
            for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
            {
               // For active terms only (passive terms added just above)
               // One idea here would be to build a list of active terms 
               // and sum over those
               // only. Check with performance analysis. 
               if (mpOpDef->TermActiveForModeCombi(i_term,mact))
               {
                  //if (mpVccCalcDef->IoLevel() > 15) 
                  //{
                     //Mout << " Term : " << i_term << " is active for mact  " 
                        //<< mact << endl;
                  //}
                  // Not only the whole set must be active, 
                  // but in the product over
                  // prod_m h_pmqm^t we must check if one of p_m=i or q_m=i that
                  // the term is active for this operator. Else no contribution
                  // (and the implementation goes wrong).
                  bool skipterm = false;
                  for (In i_mq_not_in_mp=n_mq_not_in_mp-1;i_mq_not_in_mp>=0;
                        i_mq_not_in_mp--)
                  {
                     LocalModeNr i_op_mode = mq_not_in_mp.Mode(i_mq_not_in_mp);
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        skipterm = true;
                        continue;
                     }
                  }
                  if (skipterm) 
                  {
                     if (gDebug) Mout << " check1 : skip term " << i_term << endl;
                     continue;
                  }
                  for (In i_mp_not_in_mq=n_mp_not_in_mq-1;i_mp_not_in_mq>=0;
                        i_mp_not_in_mq--)
                  {
                     LocalModeNr i_op_mode = mp_not_in_mq.Mode(i_mp_not_in_mq);
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        skipterm = true;
                        continue;
                     }
                  }
                  if (skipterm) 
                  {
                     if (gDebug) Mout << " check2 : skip term " << i_term << endl;
                     continue;
                  }
                  // Calculate the Z^tact_mact factor  = 
                  // prod(m not in mact) h_m^tact
                  //
                  zact = mpOpDef->Coef(i_term);
                  if (fabs(zact) < mpVccCalcDef->ScreenZero()) continue; // Screen 
                  for (In i_fac=0;i_fac<mpOpDef->NfactorsInTerm(i_term);i_fac++)
                  {
                     LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
                     if (!mact.IncludeMode(i_op_mode))
                     {
                        LocalOperNr i_oper = mpOpDef->OperForFactor(i_term,i_fac);
                        zact *= pOneModeModalBasisInt()->GetOccElement(i_op_mode, i_oper);;
                     }
                  }
                  //if (gDebug) Mout << " zact factor " << zact << endl;
                  // Screen again 
                  if (fabs(zact) < mpVccCalcDef->ScreenZero()) continue; 
   
                  // Now it is time for the sum_qvec 
                  // Prod (m in mact) h^m,t_p^mq^m C_qvec 
                  // The real hard stuff.
   
                  MidasVector y0(I_0);
                  MidasVector y1(n_xvec_red1);
   
                  In nel1 = n_xvec_red1;
                  ModeCombi m0(mq);
                  ModeCombi m1(m0);
                  bool skip_rest = false;
                  for (In i_mq_not_in_mp=n_mq_not_in_mp-1;i_mq_not_in_mp>=0;
                        i_mq_not_in_mp--)
                  {
                     LocalModeNr i_op_mode = mq_not_in_mp.Mode(i_mq_not_in_mp);
                     m1.RemoveMode(i_op_mode); 
                     // A mode is removed from the result compared to the input
                     //if (gDebug)
                     //{
                        //Mout << " downward contraction " << i_mq_not_in_mp << 
                           //" of " << n_mq_not_in_mp << endl;
                        //Mout << " Mode to be contracted: " << i_op_mode << 
                           //" size of result " << nel1 << endl; 
                     //}
                     // Peter: Didn't we check already?
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        Mout << " Down C: Mode not active in operator term " << i_term
                             << " mode " << i_op_mode << endl;
                        y0.Zero();
                        skip_rest = true;
                        break;
                     }
                     LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                     if (i_mq_not_in_mp==n_mq_not_in_mp-1) 
                        pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,xvec_mq,-I_1);
                     else
                        pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,y0,-I_1);
                     
                     if (mpVccCalcDef->TimeIt()) i_down++;
                     y0.Reassign(y1,nel1);
                     if (i_mq_not_in_mp>0)
                     {
                        // New size is even smaller
                        In nmod = mpVccCalcDef->Nmodals(
                              mq_not_in_mp.Mode(i_mq_not_in_mp-1));
                        if (nmod>1)
                           nel1 /= (nmod-1);
                        y1.SetNewSize(nel1,false);
                     }
                     y1.Zero();
                     m0 = m1; // the result is the input of a coming contraction.
                     //if (gDebug) Mout 
                        //<< " Vector after down contraction " << i_mq_not_in_mp 
                        //<< " is: \n" <<y0 << endl;
                  }
                  if (skip_rest) break;
   
                  // If no down contraction, remember to copy data to y0. 
                  // nel1 would be size of xvec_mq unchanged.
                  if (n_mq_not_in_mp == 0) y0.Reassign(xvec_mq,nel1); 
   
                  for (In i_common=n_common-1;i_common>=0;i_common--)
                  {
                     LocalModeNr i_op_mode = m_common.Mode(i_common);
                     //if (gDebug)
                     //{
                        //Mout << " forward contraction " 
                           //<< i_common << " of " << n_common << endl;
                        //Mout << " Mode to be contracted: " 
                           //<< i_op_mode << endl; 
                     //}
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        if (gDebug || gVibIoLevel >I_7) 
                           Mout << " Forward C: Mode not active in operator term " << i_term
                                << " mode " << i_op_mode << endl;
                        continue;
                     }
                     LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                     pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,y0,I_0);
                     if (mpVccCalcDef->TimeIt()) i_forward++;
                     y0 = y1;
                     y1.Zero();
                     //if (gDebug) Mout << " Vector after forward contraction " 
                        //<< i_common
                        //<< " is: \n" <<y0 << endl;
                  }
   
                  for (In i_mp_not_in_mq=n_mp_not_in_mq-1;
                        i_mp_not_in_mq>=0;i_mp_not_in_mq--)
                  {
                     LocalModeNr i_op_mode = mp_not_in_mq.Mode(i_mp_not_in_mq);
                     //if (gDebug)
                     //{ 
                        //Mout << " Upward  contraction " << i_mp_not_in_mq << 
                           //" of " << n_mp_not_in_mq << endl;
                        //Mout << " Mode to be contracted: " << i_op_mode << endl; 
                     //}
                     m1.InsertMode(i_op_mode);
                     // Peter: Didn't we check already?
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        Mout << " Up C: Mode not active in operator term " << i_term
                             << " mode " << i_op_mode << endl;
                        y0.Zero();
                        skip_rest = true;
                        break;
                     }
                     In nmod = mpVccCalcDef->Nmodals(i_op_mode);
                     // New size is even smaller
                     nel1 *= (nmod-1);
                     y1.SetNewSize(nel1,false);
                     y1.Zero();
                     LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                     pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,y0,I_1);
                     if (mpVccCalcDef->TimeIt()) i_up++;
                     y0.Reassign(y1,nel1);
                     m0 = m1; // the result is the input of a coming contraction.
                     //if (gDebug) Mout << " Vector after Up contraction " 
                        //<< i_mp_not_in_mq 
                        //<< " is: \n" <<y0 << endl;
                  }
                  if (skip_rest) break;
                  //if (gDebug)
                  //{
                     //Mout << " after all this size of y0 is: " << y0.Size() 
                        //<< " should be: " << sigma_mp.Size()<<endl;
                     //Mout << " zact " << zact << endl;
                     //Mout << " sigma_mp before " << sigma_mp << endl;
                     //Mout << " term " << i_term << " y0 cont " << y0 << endl;
                     //MidasVector tmp(y0.Size());
                     //tmp = zact*y0;
                     //Mout << " term " << i_term << " zacty0 cont " << tmp << endl;
                  //}
                  sigma_mp += zact*y0;
                  //if (gDebug) Mout << " sigma_mp after " << sigma_mp << endl;
               }
            }
         }
         else
         {
            VccContractor(sigma_mp,mp,mq,mact,mp_not_in_mq,mq_not_in_mp,m_common);
            if (mpVccCalcDef->TimeIt()) i_vcc_contractor++;
            //if (gDebug) Mout << " sigma_mp after VccContractor for mq = " << mq << " is : \n " 
               //<< sigma_mp << endl;
         }
      }
      if (gDebug)
         Mout << " mp " << " Norm sigma_mp " << sigma_mp.Norm() << endl;
      // if (gDebug) Mout << " Transformed Vector before storage " 
         // << sigma_mp << endl;
      // Store this part of the sigma vector after adding all contributions 
      mTransXvec.GetXvecData().DataIo(IO_PUT,sigma_mp,n_exci_mp,
            mTransXvec.ModeCombiAddress(i_mode_combi_p));
      if (i_mode_combi_p == I_0) e_tot = sigma_mp[I_0];
   }
   if (gDebug) Mout << " VccTransformer::Transformer end, Norm of transformed vector " 
      << mTransXvec.GetXvecData().Norm() << endl;
   //Mout << " end of transform e_tot = " << e_tot << endl;
   if (mpVccCalcDef->TimeIt())
   {
      Mout << " The number of VccContractor calls  : " << i_vcc_contractor << endl;
      Mout << " The number of down contracts  :      " << i_down << endl;
      Mout << " The number of up contracts ;         " << i_up   << endl;
      Mout << " The number of forward contracts ;    " << i_forward  << endl;
   }
}

/**
*  Get the Vcc energy.
*  <R| H^(only mp active terms) exp(T)|R>
* */
Nb VccTransformer::VccEnerForMpLocal(const ModeCombi& arMp,const Nb aEtot)
{
   std::vector<ModeCombi> mq_for_mp;
   ModeCombi mc_ref;
   bool uncon_removed=false;
   SetToLeftOperRange(mq_for_mp,uncon_removed,
             mc_ref,arMp,mpOpDef->GetModeCombiOpRange(),
             mTransXvec.GetModeCombiOpRange(),false,false,false,I_0,
             true);
   if (!uncon_removed)
      return aEtot;

   MidasVector sigma_mp(I_1,C_0);
   for(  auto iq = mq_for_mp.begin(), end = mq_for_mp.end()
      ;  iq != end
      ;  ++iq
      )
   {
      if (!(*iq).Empty())
      {
         MidasVector sigma_mp2(I_1,C_0);
         VccContractor(sigma_mp2,mc_ref,*iq,*iq,mc_ref,*iq,arMp,true);
         sigma_mp[I_0] += sigma_mp2[I_0];
      }
   }
   return sigma_mp[I_0] + mpVcc->GetEtot();
}

/**
* Vcc Contracter:
* For given mp and mq add contribution
* NB, for energy cont arMpqcommon stores the mp to check upon.
* */
void VccTransformer::VccContractor(MidasVector& arSigma, const ModeCombi& arMp, 
   const ModeCombi& arMq, const ModeCombi& arMpq, 
   const ModeCombi& arMpNotQ, const ModeCombi& arMqNotP,
   const ModeCombi& arMpqcommon,bool aActEnerCont)
{
   bool minut_t = false;
   bool mp_zero = (arMp.Size()==I_0);
   In n_mp_not_in_mq = arMpNotQ.Size();
   In n_mq_not_in_mp = arMqNotP.Size();
   // In n_common       = arMpqcommon.Size(); // Seidler warning: This is unused.
   In n_p            = arMp.Size();
   ModeCombi m_tq;
   ModeCombi m_tp;
   ModeCombi m_tq_not_in_mp;
   ModeCombi m_tp_not_in_mq;
   ModeCombi m_tq_and_in_mp;
   ModeCombi mc_not_in_mp;
   ModeCombi mc_and_in_mp;
   ModeCombi mc_and_mp_common;
   // Get the different ModeCombi vectors of subsets that add up 
   // to the ModeCombi of formal intermediate level, namely Mq
   //
   // Thus build up info for the 
   // sum_{m^Q} sum_{MCR[s] in SMCR[m_Q]} 
   // <mu^mP| H  product_{m_k in MCR[s]} T^mk | Ref >
   //
   // oprangevec contains the SMCR[m_Q]
   //
   std::vector<std::vector<ModeCombi>> oprangevec(0);
   if (mT1TransH)
      SubSetSets(arMq, oprangevec, mXvec.ExciRange(), I_2);   // Exclude T1 (One mode excis).
   else
      SubSetSets(arMq, oprangevec, mXvec.ExciRange());

   for(auto&& v_mc: oprangevec)
   {
      std::sort(v_mc.begin(), v_mc.end());
   }

   if (gDebug) 
   {
      Mout << " In VccTransformer::VccContractor, For arMpNotQ = " << arMpNotQ << endl;
      Mout << " In VccTransformer::VccContractor, For arMqNotP = " << arMqNotP << endl;
      Mout << " In VccTransformer::VccContractor, For arMp     = " << arMp << endl;
      Mout << " In VccTransformer::VccContractor, For arMq     = " << arMq << endl;
      Mout << " there are  " << oprangevec.size() << " combinations " << endl;
      for (In i=I_0;i<oprangevec.size();i++)
      {
         Mout << "mode combi range " << i << endl << oprangevec[i] << endl;
      }
   }

   //
   // A double loop over MCR[s] and terms are required.
   //
   // Loop over operator 
   for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
   {
      // For active terms only (passive terms added just above)
      // One idea here would be to build a list of active terms 
      // and sum over those only. Check with performance analysis. 
      if (mpOpDef->TermActiveForModeCombi(i_term,arMpq))
      {
         //Mout << " term " << i_term << " active for arMpq " << arMpq << endl;
         // This test could be used to eliminitate further
         // non-con. terms, but turn out to be rather inefficient.
         // We therefore let it rest in peace......
         // Letting it rest means that the RemoveSelUnlinked is only
         // used in the context of defining the Q space and the 
         // corresponding eliminations for the Energy cont. 
         if (false && mpVccCalcDef->VccRemoveSelUnlinked() && 
             (n_p > I_0 || aActEnerCont))// && n_common == I_0)
         {
            vector<In> mt_ivec;
            for (In i_fac=0;i_fac<mpOpDef->NfactorsInTerm(i_term);i_fac++)
            {
               LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
               mt_ivec.push_back(i_op_mode);
            }
            ModeCombi mt(mt_ivec,-I_1);
            bool connect;
            if (mp_zero) connect = DoesMcrConnectMcs(mTransXvec.ExciRange(),
                                   arMpqcommon,mt);
            else connect = DoesMcrConnectMcs(mTransXvec.ExciRange(),arMp,mt);
            //if (connect ) Mout << " test connect true " << endl;
            if (!connect ) 
            {
               if (n_p==0) Mout << " ener cont ";
               Mout << " test connect false - skip term " << endl;
               continue;
            }
         }
         //if (mpVccCalcDef->IoLevel() > 15) 
         //{
            //Mout << " Term : " << i_term << " is active for arMpq  " 
               //<< arMpq << endl;
         //}
         // Not only the whole set must be active, 
         // but in the product over
         // prod_m h_pmqm^t we must check if one of p_m=i or q_m=i that
         // the term is active for this operator. Else no contribution
         // (and the implementation may potentially go wrong??).
         bool skipterm = false;
         for (In i_mq_not_in_mp=n_mq_not_in_mp-1;i_mq_not_in_mp>=0;
               i_mq_not_in_mp--)
         {
            LocalModeNr i_op_mode = arMqNotP.Mode(i_mq_not_in_mp);
            if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
            {
               skipterm = true;
               continue;
            }
         }
         if (skipterm) 
         {
            //if (gDebug) 
               // Mout << " check1 : skip term " << i_term << endl;
            continue;
         }
         for (In i_mp_not_in_mq=n_mp_not_in_mq-1;i_mp_not_in_mq>=0;
               i_mp_not_in_mq--)
         {
            LocalModeNr i_op_mode = arMpNotQ.Mode(i_mp_not_in_mq);
            if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
            {
               skipterm = true;
               continue;
            }
         }
         if (skipterm) 
         {
            //if (gDebug) 
               // Mout << " check2 : skip term " << i_term << endl;
            continue;
         }

         // Mout << " term " << i_term << " survived checks " << endl;

         // Calculate the Z^tact_mact factor  = 
         // prod(m not in mact) h_m^tact
         //
         Nb zact = mpOpDef->Coef(i_term);
         if (fabs(zact) < mpVccCalcDef->ScreenZero()) continue; // Screen 
         //if (gDebug) Mout << " factors " 
            //<< mpOpDef->NfactorsInTerm(i_term) 
               //<< " in term " << i_term << endl; 
         vector<In> modes; // The modes in the operator product that
                           // are both in m^t and in (m^q u m^p)
         for (In i_fac=0;i_fac<mpOpDef->NfactorsInTerm(i_term);i_fac++)
         {
            LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
            if (!arMpq.IncludeMode(i_op_mode))
            {
               LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac);
               zact *= pOneModeModalBasisInt()->GetOccElement(i_op_mode, i_oper);;
            }
            else
            {
               modes.push_back(i_op_mode);
            }
         }
         //if (gDebug) 
             // Mout << " zact factor " << zact << endl;
         // Screen again 
         if (fabs(zact) < mpVccCalcDef->ScreenZero()) continue; 

         //ModeCombi mt = ModeCombi(mpOpDef->GetOperProd(i_term).GetModes(),-I_1);
         ModeCombi m_tpq(modes,-I_1);
         m_tq.Intersection(m_tpq,arMq);
         m_tp.Intersection(m_tpq,arMp);
         m_tq_not_in_mp.InFirstOnly(m_tq,arMp);
         m_tp_not_in_mq.InFirstOnly(m_tp,arMq);
         m_tq_and_in_mp.Intersection(m_tq,arMp);
         In n_mtp_not_in_mq = m_tp_not_in_mq.Size();
         if (gDebug)
         {
            Mout << " m_tpq           = " << m_tpq << endl;
            Mout << " m_tq            = " << m_tq << endl;
            Mout << " m_tp            = " << m_tp << endl;
            Mout << " m_tq_not_in_mp  = " << m_tq_not_in_mp << endl;
            Mout << " m_tp_not_in_mq  = " << m_tp_not_in_mq << endl;
            Mout << " m_tq_and_in_mp  = " << m_tq_and_in_mp << endl;
         }
         //
         //
         // Now calculate the product
         // ymtponly = prod_{m in m tponly} h_{ai}^m
         //
         //
         bool skip_rest = false;
         MidasVector ymtponly0(I_1,C_1);
         MidasVector ymtponly1(ymtponly0);
         //ymtponly0.SetNewSize(I_0,false);
         //ymtponly1.SetNewSize(I_0,false);
         ModeCombi ymtponly_m0;
         ModeCombi ymtponly_m1;
         In nel1 = I_1;
         for (In i_mtp_not_in_mq=n_mtp_not_in_mq-1;
               i_mtp_not_in_mq>=0;i_mtp_not_in_mq--)
         {
            LocalModeNr i_op_mode = m_tp_not_in_mq.Mode(i_mtp_not_in_mq);
            //if (gDebug)
            //{ 
               //Mout << " Upward  contraction " << i_mtp_not_in_mq << 
                  //" of " << n_mtp_not_in_mq << endl;
               //Mout << " Mode to be contracted: " << i_op_mode << endl; 
            //}
            ymtponly_m1.InsertMode(i_op_mode);
            if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
            {
               // Mout << " Up C: Mode not active in operator " << 
                  // " term " << i_term << " mode " << i_op_mode << endl;
               ymtponly0.Zero();
               skip_rest = true;
               break;
            }
            In nmod = mpVccCalcDef->Nmodals(i_op_mode);
            nel1 *= (nmod-1);
            ymtponly1.SetNewSize(nel1,false);
            ymtponly1.Zero();
            LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
            pOneModeModalBasisInt()->Contract(ymtponly_m1,ymtponly_m0,i_op_mode,i_oper,
                                             ymtponly1,ymtponly0,I_1);
            ymtponly0.Reassign(ymtponly1,nel1);
            ymtponly_m0 = ymtponly_m1; 
            // the result is the input of a coming contraction.
            //if (gDebug) Mout << " Vector after Up contraction " 
               //<< i_mtp_not_in_mq 
               //<< " is: \n" <<ymtponly0<< endl;
         }
         if (skip_rest) 
         {
            Mout <<  " Up Contract -  Break and skip_rest " << endl;
            break;
         }
         Nb norm = ymtponly0.Norm();
         // Screen on product of zact and ymtponly0 norm.
         // Screen again 
         //Mout << " Norm of Y^mtponly " << norm << endl;
         if (fabs(norm*zact) < mpVccCalcDef->ScreenZero()) continue; 
         ymtponly1.SetNewSize(I_1,false);
   
         // Loop over combination mode combination ranges MCR[s], 
         // get contribution.

         for (In i=I_0;i<oprangevec.size();i++)
         {
            In nmodcomb = oprangevec[i].size();
            //Mout << " Mode combi range:  " << i << " has " << nmodcomb 
               //<< " mode combinations " << endl;

            //< space for each t-vector in the mode combination range
            //< The extra 1 is for the (h*h..) product calculated above
            MidasVector* p_vecs  = new MidasVector [nmodcomb+I_1];
            MultiIndex*  p_mi    = new MultiIndex  [nmodcomb+I_1];
            vector<In>*  p_ivec  = new vector<In>  [nmodcomb+I_1];
            ModeCombi*   p_mc    = new ModeCombi   [nmodcomb+I_1];
            //
            // loop over the mode combis 
            //
            for (In imc=I_0;imc<nmodcomb;imc++)
            {
               const ModeCombi& mc = oprangevec[i][imc];
      
               // Prepare the data, there are ndata elements of type mc.
               //ModeCombiOpRange::const_iterator iq_mq;
               //In i_mode_combi;
               //mXvec.Find(mc,iq_mq,i_mode_combi); 
               In i_mode_combi = mc.ModeCombiRefNr();
               //Mout << " i1 = " << i_mode_combi << " i2 = " << i_mode_combi2 << endl;
               In ndata = mXvec.NexciForModeCombi(i_mode_combi);
               p_vecs[imc].SetNewSize(ndata);
               mXvec.GetXvecData().DataIo(IO_GET,p_vecs[imc],ndata,mc.Address());
               //Mout << "VccContractor, mc:    " << mc << endl;
               //Mout << " imc = " << imc << " vector " << p_vecs[imc] << endl;
      
               if (mpOpDef->TermActiveForModeCombi(i_term,mc))
               {
                  // Find modes only in mc and not in mp and "down" contract those. 
                  // Size is reduced 

                  mc_not_in_mp.InFirstOnly(mc,arMp);
                  mc_and_in_mp.Intersection(mc,arMp);
                  mc_and_mp_common.Intersection(mc,arMp);
                  In n_mc_not_in_mp = mc_not_in_mp.Size();
                  In n_common       = mc_and_mp_common.Size();
                  In n_modes_in_tm = mc.Size();
   
                  if (gDebug)
                  {
                     Mout << " mc_and_mp_common = " << mc_and_mp_common << endl;
                     Mout << " mc_not_in_mp     = " << mc_not_in_mp << endl;
                     Mout << " mc_and_in_mp     = " << mc_and_in_mp << endl;
                     Mout << " Contracting t^ " << mc << " with ndata = " << ndata 
                        << " and n_modes " << n_modes_in_tm << endl;
                     for (In i_modes=n_modes_in_tm-I_1;i_modes>=0;i_modes--)
                     {
                        LocalModeNr i_op_mode = mc.Mode(i_modes);
                        LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                        Mout << " i_op_mode " << i_op_mode << " i_oper " 
                             << i_oper << endl;
                     }
                  }
   
                  In n_xvec_red1 = ndata;
                  if (n_mc_not_in_mp >0) 
                  {
                     In i_op_mode = mc_not_in_mp.Mode(n_mc_not_in_mp-1); 
                     // First one to be contracted.
                     //if (gDebug) Mout << " orig size " << n_xvec_red1 ;
                     In nmod = mpVccCalcDef->Nmodals(i_op_mode);
                     if (nmod>1) n_xvec_red1 /= (nmod-1);
                     //if (gDebug) Mout << " new  size " << n_xvec_red1 << endl;
                  }
                  MidasVector y0(I_0);
                  MidasVector y1(n_xvec_red1);
     
                  In nel1=n_xvec_red1;
                  ModeCombi m0(mc);
                  ModeCombi m1(m0);
                  bool skip_rest = false;
                  for (In i_mc_not_in_mp=n_mc_not_in_mp-1;i_mc_not_in_mp>=0;
                        i_mc_not_in_mp--)
                  {
                     LocalModeNr i_op_mode = mc_not_in_mp.Mode(i_mc_not_in_mp);
                     m1.RemoveMode(i_op_mode); 
                     // A mode is removed from the result compared to the input
                     //if (gDebug)
                     //{
                        //Mout << " downward contraction " << i_mc_not_in_mp << 
                           //" of " << n_mc_not_in_mp << endl;
                        //Mout << " Mode to be contracted: " << i_op_mode << 
                           //" size of result " << nel1 << endl; 
                     //}
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        // Mout << " Down C: Mode not active in operator-term " 
                           // << i_term << " mode " << i_op_mode << endl;
                        y0.Zero();
                        skip_rest = true;
                        break;
                     }
                     LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                     if (i_mc_not_in_mp==n_mc_not_in_mp-1) 
                        pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,
                                                         y1,p_vecs[imc],-I_1);
                     else
                        pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,y0,-I_1);
                     y0.Reassign(y1,nel1);
                     if (i_mc_not_in_mp>0)
                     {
                        // New size is even smaller
                        In nmod = mpVccCalcDef->Nmodals(
                              mc_not_in_mp.Mode(i_mc_not_in_mp-1));
                        if (nmod>1) nel1 /= (nmod-1);
                        y1.SetNewSize(nel1,false);
                     }
                     y1.Zero();
                     m0 = m1; // the result is the input of a coming contraction.
                     //if (gDebug) 
                        // Mout << " Vector after down contraction " 
                        //      << i_mc_not_in_mp 
                        //      << " is: \n" <<y0 << endl;
                  }
                  if (skip_rest) 
                  {
                     Mout <<  " Down Contract -  Break and skip_rest " << endl;
                     break;
                  }
      
                  // Find modes in mc and also in mp and "forward" contract those. 
                  // Size is unchanged 
     
                  // If no down cont. remember to copy data to y0. 
                  // nel1 would be size of xvec_mq unchanged
                  if (n_mc_not_in_mp == 0) y0.Reassign(p_vecs[imc],nel1); 
                  //Mout << " after down contraction y0 (if any! ) " << y0 << endl;
   
                  for (In i_common=n_common-1;i_common>=0;i_common--)
                  {
                     LocalModeNr i_op_mode = mc_and_mp_common.Mode(i_common);
                     //if (gDebug)
                     //{
                        //Mout << " forward contraction " 
                           //<< i_common << " of " << n_common << endl;
                        //Mout << " Mode to be contracted: " 
                           //<< i_op_mode << endl; 
                     //}
                     if (!mpOpDef->IsModeActiveInTerm(i_op_mode,i_term)) 
                     {
                        //if (gDebug || gVibIoLevel >I_7) 
                           // Mout << " Forward C: Mode not active: " << 
                           // " term " << i_term << " mode " << i_op_mode << endl;
                        continue;
                     }
                     LocalOperNr i_oper = mpOpDef->OperForOperMode(i_term,i_op_mode);
                     pOneModeModalBasisInt()->Contract(m1,m0,i_op_mode,i_oper,y1,y0,I_0);
                     y0 = y1;
                     y1.Zero();
                     //if (gDebug) 
                        // Mout << " Vector after forward contraction " 
                          // << i_common << " is: \n" <<y0 << endl;
                  }
                  // Store the results of contraction - both mode combi 
                  // and numerical vector
                  p_mc[imc] = m1;
                  p_vecs[imc].Reassign(y0,nel1);
                  //Mout << " after forward contraction p_vecs " 
                  //     << p_vecs[imc] << endl;
                  //Mout << " after forward contraction p_mc   " 
                  //     << p_mc[imc] << endl;
                  //Mout << " Before # data = " << ndata << " now " 
                  //     << p_vecs[imc].Size() << endl;

               }
               else
               {
                  p_mc[imc] = mc;
                  //Mout << " term " << i_term << " not active for mode combi " 
                  //     << p_mc[imc] << endl;
               }
   

               // Prepare the indexer....
               p_mi[imc].ReInit(p_mc[imc],mpVccCalcDef); // m1 = p_mc[imc]
               // Connect the modes in the subsets with the modes in the full 
               // arMp result vector
               // p_ivec stores for each mode in the subset the corresponding
               // address in the full set.
               p_ivec[imc].resize(p_mc[imc].Size());
               for (In k=I_0;k<p_mc[imc].Size();k++)
                  for (In j=I_0;j<arMp.Size();j++)
                     if (p_mc[imc].Mode(k)==arMp.Mode(j)) p_ivec[imc][k] = j;
               //Mout << " imc = " << imc << " p_ivec ";
               // if (p_ivec[imc].size() > 0) Mout << p_ivec[imc];
               // Mout << endl;
            } 

            // Now make a direct product of the transformed T^mc and u
            // the Z and Y factors 
            // to give the collected matrix.
            // Stor the ymtponly in the same format.
            p_vecs[nmodcomb].Reassign(ymtponly0,-I_1);
            p_mc[nmodcomb] = ymtponly_m1;
            p_mi[nmodcomb].ReInit(ymtponly_m1,mpVccCalcDef); 
            // Allowed ? Consideration; this m_1 must be a subset of 
            // m^P which should be allowed for
            // all calculations where the excitation space is 
            // closed under deexcitation.
            // So it should both be allowed and not larger than the resultant sigma.
            p_ivec[nmodcomb].resize(ymtponly_m1.Size());
            for (In k=I_0;k<ymtponly_m1.Size();k++)
               for (In j=I_0;j<arMp.Size();j++)
                  if (ymtponly_m1.Mode(k)==arMp.Mode(j)) p_ivec[nmodcomb][k] = j;
            // prepare some infomation
            MultiIndex mcmi(arMp,mpVccCalcDef);
            In nc    = mcmi.Size();
            In ncvec = mcmi.VecSize();
            In level = I_0;
            vector<In> res_ivec(ncvec,-I_1);
            Nb res=C_1;
            MidasVector tmp_res_vec(nc);
            if (gDebug)
            {
               Mout << " m^P     : " << arMp << endl;
               Mout << " m^tponly; " << ymtponly_m1 << endl;
               Mout << " mode combi range " << i << endl << oprangevec[i] << endl;
               for (In imc=I_0;imc<=nmodcomb;imc++)
               {
                  Mout << " transformed mct, imc = " << imc << " " 
                       << p_mc[imc] << endl;
                  Mout << " transformed vector     " << p_vecs[imc] << endl;
               }
            }
            if (arSigma.Size() != tmp_res_vec.Size()) 
               MIDASERROR("Dim error in VccContractor ");
            DirProd(p_vecs,p_mi,tmp_res_vec,nmodcomb+I_1,mcmi,level,
                    res_ivec,res,p_ivec,minut_t);
   
            //Mout << " sigma_mp dim " << arSigma.Size() << " tmp_res_vec " 
            //     << tmp_res_vec.Size() << endl;
            //Mout << " Norm of cont: " << zact*tmp_res_vec.Norm() << endl;
            arSigma += zact*tmp_res_vec;

            delete[] p_mc;
            delete[] p_ivec;
            delete[] p_mi;
            delete[] p_vecs;
         }
      }
      //else
      //{
         //Mout << " term " << i_term << " is NOT active for arMpq " << arMpq << endl;
      //} 
   }
}

/**
* Get the C_m  = < mu^ m | exp(+-T) | R >,
* aMinusT, if exp(-T) else exp(T)
* aLowerOnly, if T_mu is included or not.
* */
void VccTransformer::VciCoefsFromVcc(const ModeCombi& arM,const In& arAdd,
      MidasVector& arVci, DataCont& arVcc, const Xvec& arXvec,
      bool aLowerOnly,bool aMinusT)
{
   // Get the different ModeCombi vectors of subsets that add up 
   // to the ModeCombi
   std::vector<std::vector<ModeCombi>> oprangevec(0);
   if (mT1TransH)
      SubSetSets(arM,oprangevec,arXvec.ExciRange(), I_2);       // Exclude T1 (one-mode excis).
   else
      SubSetSets(arM,oprangevec,arXvec.ExciRange());

   for(auto&& v_mc: oprangevec)
   {
      std::sort(v_mc.begin(), v_mc.end());
   }
   
   if (gDebug) 
   {
      // const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
      Mout << " In VccTransformer::VciCoefsFromVcc " << endl
           << "       arVci size:     " << arVci.Size() << endl
           << "       arVcc label:    " << arVcc.Label() << endl
           << "       arVcc size:     " << arVcc.Size() << endl
      //     << "       arXvec:" << endl << mcr << endl
           << "       arM =           " << arM << endl
           << "             there are " 
           << oprangevec.size() << " combinations " << endl;
      for (In i=I_0;i<oprangevec.size();i++)
      {
         Mout << "             Mode combi range " << i << ":" << endl << oprangevec[i] << endl;
      }
   }

   // Construct the multiindex corresponding to the modecombi
   string lowhig = "LOWHIG";
   MultiIndex mcmi(arM,mpVccCalcDef);
   // prepare some infomation
   In nc    = mcmi.Size();
   In ncvec = mcmi.VecSize();
   if (gDebug)
      Mout << " ncvec = " << ncvec << " nc = " 
           << nc << " arVci size " << arVci.Size() << endl;
   if (nc <= I_0) return;

   // loop over the different subset combinations
   for (In i=I_0;i<oprangevec.size();i++)
   {
      In nmodcomb = oprangevec[i].size();
      //Mout << " nmodcomb " << nmodcomb << " for i = " << i << " " << oprangevec[i] << endl;

      // If there is only one MC in the MCR then it is the same one and it is therefore put 
      // directly into the VCI vector.
      if (aLowerOnly && nmodcomb<=I_1)
      {
         if (gDebug) Mout << " no contribution added for " 
            << oprangevec[i] << endl;
         continue;
      } 
      else if (nmodcomb <= I_1) 
      {
         // If only one mode combi then it must be in the Xvec for T.
         // Find it because we need its address to get the right data.
         // Peter: I'm not sure if this search is necessary. I guess the address
         // is put into "the one mode combi" by SubSetSets().
         ModeCombiOpRange::const_iterator iq_m;
         In i_mode_combi_m;
         arXvec.Find(arM,iq_m,i_mode_combi_m);
         arVcc.DataIo(IO_GET,arVci,arVci.Size(),arXvec.ModeCombiAddress(i_mode_combi_m),
               I_1,I_0,I_1,false,C_0,true,C_1);
         if (aMinusT && nmodcomb == I_1) arVci.ChangeSign(); // Odd order T^aN
         //Mout << " add direct contribution after vector is " << arVci << endl;
         continue;
      }

      MultiIndex* p_mi = new MultiIndex [nmodcomb];
      MidasVector* p_vecs = new MidasVector [nmodcomb];
      vector<In>* p_ivec = new vector<In> [nmodcomb];
      // loop over the mode combis = loop over the subsets
      for (In imc=I_0;imc<nmodcomb;imc++)
      {
         const ModeCombi& mc = oprangevec[i][imc];

         // 1. Prepare the indexer....
         p_mi[imc].ReInit(mc,mpVccCalcDef);

         // 2. Prepare the data , there are ndata elements of type mc.
         //ModeCombiOpRange::const_iterator iq_mq;
         //In i_mode_combi;
         //arXvec.Find(mc,iq_mq,i_mode_combi); 
         In i_mode_combi = mc.ModeCombiRefNr();
         In ndata = arXvec.NexciForModeCombi(i_mode_combi);
         p_vecs[imc].SetNewSize(ndata);
         arVcc.DataIo(IO_GET,p_vecs[imc],ndata,mc.Address());
         //Mout << "VciCoefsFromVcc, mc:    " << mc << endl;
         //Mout << " imc = " << imc << " vector " << p_vecs[imc] << endl;

         // 3. Connect the modes in the subsets with the modes in the full arM.
         // p_ivec stores for each mode in the subset the corresponding
         // address in the full set.
         p_ivec[imc].resize(mc.Size());
         for (In k=I_0;k<mc.Size();k++)
            for (In j=I_0;j<arM.Size();j++)
               if (mc.Mode(k)==arM.Mode(j)) p_ivec[imc][k] = j;
         //Mout << " imc = " << imc << " p_ivec " << p_ivec[imc] << endl;
      }
      In level = I_0;
      vector<In> res_ivec(ncvec,-I_1);
      Nb res=C_1;
      DirProd(p_vecs,p_mi,arVci,nmodcomb,mcmi,level,res_ivec,res,p_ivec,aMinusT);
      delete[] p_mi;
      delete[] p_vecs;
      delete[] p_ivec;
   }
   if (gDebug) Mout << " final Vcc as Vci vector " << arVci << endl;
}

/**
* C to exp(T) transformation
* */
void VccTransformer::VccFromVci(DataCont& arVci, DataCont& arVcc)
{
   mXvec.GetXvecData().NewLabel(arVci.Label(),false); 
   mTransXvec.GetXvecData().NewLabel(arVcc.Label(),false);
   mTransXvec.GetXvecData() = arVcc;
   mXvec.GetXvecData()      = arVci;
   In i_mode_combi_p = -1;
   // Loop through the result vector from the beginning and onwards
   // to higher excitations.
   for(  auto ip = mTransXvec.ExciRange().begin(), end = mTransXvec.ExciRange().end()
      ;  ip != end
      ;  ++ip
      )
   {
      i_mode_combi_p++;
      const ModeCombi& mp = *ip; // mTransXvec.GetModeCombi(i_mode_combi_p);
      //In n_excited = mp.Size();
      In n_exci_mp = mTransXvec.NexciForModeCombi(i_mode_combi_p);
      if (mpVccCalcDef->IoLevel() > 15) 
      {
         Mout << " The modes excited in mTransXvec i_mode_combi_p " 
            << i_mode_combi_p << endl;
         Mout << " are: " << mp << endl;
         Mout << " The number of excitation for this mode combi: " 
            << n_exci_mp<<endl;
      }
      if (n_exci_mp < I_1) continue;

      MidasVector sigma_mp(n_exci_mp,C_0);
      mXvec.GetXvecData().DataIo(IO_GET,sigma_mp,n_exci_mp,mp.Address());

      MidasVector sigma_mp2(n_exci_mp,C_0);
      //  sigma_mp2 = C^mp,  = [exp(Tm)]^mp m<mp
      bool t1_save = mT1TransH;
      mT1TransH = false;    // We need the T1 amplitudes in this case.
      VciCoefsFromVcc(mp,mp.Address(),sigma_mp2,
            mTransXvec.GetXvecData(),mTransXvec,true);
      mT1TransH = t1_save;

      //Mout << " before mp cont with mp = " << mp << endl;
      //Mout << sigma_mp << endl;
      sigma_mp -= sigma_mp2;
      //Mout << " after mp cont with mp = " << mp << endl;
      //Mout << sigma_mp << endl;
      //Mout << " the contribution is = " << endl;
      //Mout << sigma_mp2 << endl;
      mTransXvec.GetXvecData().DataIo(IO_PUT,sigma_mp,n_exci_mp,mp.Address());
   }
   arVcc = mTransXvec.GetXvecData();
}

/**
* Transforming arDcIn by H/H0 to arDcOut 
* */
void VccTransformer::VccTransH
   (  DataCont& arDcIn
   ,  DataCont& arDcOut
   ,  In aI
   ,  In aJ
   ,  Nb& aNb
   )
{
   mXvec.GetXvecData().NewLabel(arDcIn.Label()+"_xvec",false); 
   mTransXvec.GetXvecData().NewLabel(arDcOut.Label()+"_transxvec",false);
   
   if (aI==I_0) arDcIn.Reassign(mXvec.GetXvecData(), I_1, -I_1);
   if (aI==I_1) SmallToLargeTrans(arDcIn, mXvec.GetXvecData(), mTransXvec.ExciRange(), mXvec.ExciRange());
   Nb one = C_1;
   mXvec.GetXvecData().DataIo(IO_PUT,I_0,one);

   if (mpVccCalcDef->SumNprim() && aI == I_1) SumNpurify(mXvec.GetXvecData()); 
   if (mpVccCalcDef->Emaxprim() && aI == I_1) Emaxpurify(mXvec.GetXvecData()); 
   if (mpVccCalcDef->Zeroprim() && aI == I_1) Zeropurify(mXvec.GetXvecData()); 

   if (gDebug || mpVccCalcDef->IoLevel() > I_14)
   {
      Mout << "In VccTransformer::VccTransH():" << endl
           << "      aI, aJ, aNb          values     "
           << aI << ", " << aJ << ", " << aNb << endl
           << "      arDcin               Label      " << arDcIn.Label() << endl
           << "      arDcOut              Label      " << arDcOut.Label() << endl
           << "      mXvec                Label      " << mXvec.GetXvecData().Label() << endl
           << "      mTransXvec           Label      " << mTransXvec.GetXvecData().Label() << endl
           << "      arDcin               size       " << arDcIn.Size() << endl
           << "      arDcOut              size       " << arDcOut.Size() << endl
           << "      GetXvecData()            size begin " << mXvec.GetXvecData().Size() << endl
           << "      mTransXvecData       size begin " << mTransXvec.GetXvecData().Size() << endl
           << "      arDcin               storage    " << arDcIn.Storage() << endl
           << "      arDcOut              storage    " << arDcOut.Storage() << endl
           << "      mXvec.GetXvecData()      storage    " << mXvec.GetXvecData().Storage() << endl
           << "      mTransXvec.GetXvecData() storage    "
           << mTransXvec.GetXvecData().Storage() << endl
           << "      Norm Xvec : " << mXvec.GetXvecData().Norm() << endl
           << "      Norm arDcin: " << arDcIn.Norm() << endl;
   }
   
   // If needed, perform T1 transformation of integrals.
   // (The VCC[2] transformer will take care of T1-trans on its own, and will
   // always take the raw modal integrals for construction.)
   if (mT1TransH && !this->UseVcc2Trans())
   {
      if (gDebug || mpVccCalcDef->IoLevel() > I_10)
      {
         Mout << " In VccTransH(): T1 transforming the integrals." << std::endl;
      }
      // Update integrals, use T1 transformation.
      UpdateModalIntegrals(true, &mXvec);
   }

   bool bs = mVciVcc;
   mVciVcc = true;
   if (aI==I_0)
   {
      if (mpVccCalcDef->GetTrueHDiag()) TransformerTrueHDiag(aJ,aNb);
      else TransformerH0(aJ,aNb); 
   }
         
   if (aI==I_1)
   {
      if (this->UseVcc2Trans())
      {
         const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::VCC);
         p_vcc2trans->Transform(Vcc2GetTAmps(), mTransXvec.GetXvecData());
      }
      else
      {
         //Mout << "VCC: Using general transformer." << endl;
         // clock_t t0 = clock();
         GenTransformer();
         // Mout << "Time: " << Nb(clock() - t0) / CLOCKS_PER_SEC << " s" << endl;
      }
   }
   mVciVcc = bs;
   
   mTransXvec.GetXvecData().DataIo(IO_GET,I_0,aNb);
   
   // For aI = I_1 we need an additional layer exp(-T) transformation! 
   if (aI==I_1 && aJ==I_0 && !mpVccCalcDef->VccRemoveSelUnlinked())
   {
      if (gDebug || mpVccCalcDef->IoLevel() > I_5)
         Mout << " Transform with exp(-T) in VccTransH " << endl;
      // Put input into mXvec again, not that this is with small storage!
      // Peter: I guess what is meant by the above is that the small amount
      // of data in arDcIn is put into the large vector GetXvecData().
      // However, the address structure of the data follows mTransXvec.
      // If this is true, there is an ambiguity in ExpMinusT since the two calls
      // to VciCoefsFromVcc() uses different Xvecs.
      // Btw. why do we need the reassignment?
      arDcIn.Reassign(mXvec.GetXvecData(),I_1,-I_1);
      
      if (!(this->UseVcc2Trans()))
      {
         DataCont tmp(mTransXvec.GetXvecData());
         ExpT(mTransXvec, tmp, mTransXvec, mTransXvec.GetXvecData(), mTransXvec, mXvec.GetXvecData(), true);
      }
   }
   else
      if (gDebug || mpVccCalcDef->IoLevel() > I_5)
      {
         Mout << " No transformation with exp(-T) in VccTransH  " << endl;
         if (mpVccCalcDef->TwoModeTrans())
            Mout << " NOTE: exp(-T) is always performed by the explicit two-mode transformer."
                 << endl;
         if (mpVccCalcDef->OneModeTrans())
            Mout << " NOTE: exp(-T) is always performed by the explicit one-mode transformer."
                 << endl;
      }
   
   if (mpVccCalcDef->SumNprim() && aI==1) SumNpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Emaxprim() && aI==1) Emaxpurify(mTransXvec.GetXvecData());
   if (mpVccCalcDef->Zeroprim() && aI==1) Zeropurify(mTransXvec.GetXvecData());

   if (aI==I_1) arDcOut.Reassign(mTransXvec.GetXvecData(),I_1,I_1);
   if (aI==I_0) arDcOut.Reassign(mTransXvec.GetXvecData(),I_1,I_1,-aNb); 

   if (gDebug || mpVccCalcDef->IoLevel() > I_14)
   {
      Mout << "VccTransH: Norm TransXvec: " << mTransXvec.GetXvecData().Norm() << std::endl;
      Mout << "VccTransH: Norm arDcOut: " << arDcOut.Norm() << std::endl;
   }
}

/**
* Transforming arDcIn by H/H0 to arDcOut 
* Nb is actually a dummy arg and is replace by Etot
* */
void VccTransformer::VccTransPreDiag(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb)
{
   //Mout << " VccTransPreDiag in " << arDcIn.Norm() << endl; 
   Nb x = C_0;
   UpdateModalIntegrals(false);
   bool bs = mVciVcc;
   mVciVcc = false;
   //Mout << " In VccTransPreDiag - prepared to do real transformation with aI = " 
        //<< aI << " aJ " << aJ << endl;
   //Mout << " In TransPreDiag - before TransH " << endl; 
   //TransH(arDcIn,arDcOut,aI,aJ,aNb);
   //Mout << " VccTransPreDiag  - after TransH -norm in  " << arDcIn.Norm() << endl; 
   //Mout << " VccTransPreDiag  - after TransH -norm in  " << arDcOut.Norm() << endl; 
   TransH(arDcIn,arDcOut,aI,aJ,x);
   //Mout << " VccTransPreDiag arDcIn  Norm " << arDcIn.Norm() << endl; 
   //Mout << " VccTransPreDiag arDcOut Norm " << arDcOut.Norm() << endl; 
   mVciVcc = bs;
   //Mout << " Reference Hartree-product energy " << mpVcc->GetEtot() << endl;
   arDcOut.Axpy(arDcIn,-mpVcc->GetEtot());  // Subtract reference energy times vec.
   //Mout << " VccTransPreDiag out " << arDcOut.Norm() << endl;
}

/**
* Transform arDcIn with exp(+/- T). arDcT contains amplitudes.
* The arXvec* arguments are only used to obtain addresses of mode combis.
* The actual data are a the corresponding arDc* arguments.
* **/
void VccTransformer::ExpT(Xvec& arXvecIn, DataCont& arDcIn,
               Xvec& arXvecOut, DataCont& arDcOut,
               Xvec& arXvecT, DataCont& arDcT,
               bool aMinusT)
{
   if (gDebug || mpVccCalcDef->IoLevel() > 15) 
   {
      Mout << "In VccTransformer::ExpT():" << endl
           << "      Mode combis in input:  " << arXvecIn.NexciTypes() << endl
           << "      Dim. of input vector:  " << arDcIn.Size() << endl
           << "      Mode combis in output: " << arXvecOut.NexciTypes() << endl
           << "      Dim. of output vector: " << arDcOut.Size() << endl
           << "      Mode combis in T:      " << arXvecT.NexciTypes() << endl
           << "      Dim. of amp. vector:   " << arDcT.Size() << endl;
      if (gDebug)
      {
         Mout << "      mxvec: " << mXvec.GetXvecData().Label()       << endl;
         Mout << "      mxvec: " << mTransXvec.GetXvecData().Label()  << endl;
      }
   }
   
   ModeCombi mt; 
   // Loop over mode combinations in the result.
   In i_mode_combi_p = -1;
   for(  auto ip = arXvecOut.ExciRange().begin(), end = arXvecOut.ExciRange().end()
      ;  ip != end
      ;  ++ip
      )
   {
      i_mode_combi_p++;
      const ModeCombi& mp = *ip;   // arXvecOut.GetModeCombi(i_mode_combi_p);
      In nmp       = mp.Size();
      In n_exci_mp = arXvecOut.NexciForModeCombi(i_mode_combi_p);
      
      if (mpVccCalcDef->IoLevel() > 15) 
      {
         Mout << "VccTransformer::ExpT() cont'd.:" << endl
              << "      The modes excited in arXvecOut, i_mode_combi_p " 
              << i_mode_combi_p << endl
              << "      are: " << mp << endl
              << "      The number of excitation for this mode combi: " 
              << n_exci_mp << endl;
      }
      if (n_exci_mp < I_1) continue;

      // Result for current mode combi.
      MidasVector sigma_mp(n_exci_mp);

      // If current output mode combi, mp, is in input then add
      // contribution corresponding to '1' in exp(T) expansion
      // in sum_p <mu| exp(T) |p> c_p, i.e. c_mu.
      ModeCombiOpRange::const_iterator mp_inp;     // Never used.
      In i_mp_inp;
      if (arXvecIn.Find(mp, mp_inp, i_mp_inp))
         arDcIn.DataIo(IO_GET, sigma_mp, n_exci_mp, arXvecIn.ModeCombiAddress(i_mp_inp));
      
      // Add the contrib. from q=ref in sum_q <mu| Exp(T) |q> c_q,
      // i.e. t_mu*c_ref.
      // If mp is empty, i.e. the reference, this contrib has just been
      // added above.
      // If T1 transformed H is used, we get nothing if mp is one-mode exci.
      if (nmp > I_0 && !(mT1TransH && I_1 == nmp) )
      {
         MidasVector cc_as_ci_mump(n_exci_mp);
         VciCoefsFromVcc(mp, i_mp_inp, cc_as_ci_mump, arDcT, arXvecT,
               false, aMinusT);
         
         if (gDebug)
            Mout << " mp " << mp << " Norm  Cmp = exp(T)mp: " 
                 << cc_as_ci_mump.Norm() << endl;

         Nb c_0;
         arDcIn.DataIo(IO_GET, I_0, c_0);
         sigma_mp += c_0*cc_as_ci_mump;
      }
      
      // Loop over all mode combinations mq in input vector. Test each mode combi
      // mq and skip if not appropriate.
      // BETTER IDEA: Make a list and loop over necessary mq's.
      ModeCombiOpRange::const_iterator iq_mt;
      In i_mode_combi_t;
      In i_mode_combi_q=-I_1;
      for(  auto iq = arXvecIn.ExciRange().begin(), end = arXvecIn.ExciRange().end()
         ;  iq != end
         ;  ++iq
         )
      {
         i_mode_combi_q++;
         const ModeCombi& mq = *iq;       // arXvecIn.GetModeCombi(i_mode_combi_p);

         In nmq = mq.Size();
         if (nmq < I_1)                // We have taken care of the Reference cont right above.
            continue;

         In n_exci_mq = arXvecIn.NexciForModeCombi(i_mode_combi_q);
         if (n_exci_mq < I_1)
            continue;

         // Modes only in output must be connected to input through ext(T).
         mt.InFirstOnly(mp,mq);
         In nmt   = mt.Size();
         if (nmt < I_1)
            continue;
         if (mT1TransH && I_1 == nmt)     // No one-mode excitations if T1 transformed H is used.
            continue;
         
         bool mqinmp = true;
         for (In imq=I_0;imq<nmq;imq++) 
            mqinmp = mqinmp && mp.IncludeMode(mq.Mode(imq));
         if (!mqinmp) 
            continue;       // If mode in input is not in output we don't have a chance.
         
         arXvecOut.Find(mt,iq_mt,i_mode_combi_t); 
         In n_exci_mt = arXvecOut.NexciForModeCombi(i_mode_combi_t);
         if (n_exci_mt < I_1)
            continue;

         if (mpVccCalcDef->IoLevel() > 15) 
         {
            Mout << "VccTransformer::ExpT() cont'd.:" << endl
                 << "      mp:            " << mp << endl
                 << "      mq             " << mq << endl 
                 << "      *iq            " << *iq << endl
                 << "      mt             " << mt << endl
                 << "      i_mode_combi_t " << i_mode_combi_t << endl
                 << "      n_exci_mt      " << n_exci_mt << endl;
         }

         // Get the part of the vector to be transformed.
         MidasVector cc_as_ci(n_exci_mt);

         // As far as I can see, mt.Address() argument isn't used for anything in
         // VciCoefsFromVcc().
         VciCoefsFromVcc(mt, mt.Address(), cc_as_ci, arDcT, arXvecT, false, aMinusT); 
         if (gDebug)
            Mout << " mt " << mt << " Norm Cmt = exp(T)mt: " 
                 << cc_as_ci.Norm() << endl;
         
         // Get the components of the input vector.
         MidasVector inp_vec(n_exci_mq);
         arDcIn.DataIo(IO_GET, inp_vec, n_exci_mq,
                       arXvecIn.ModeCombiAddress(i_mode_combi_q));

         //Mout << " for " << mt;
         //Mout << " exp(-T)/ci-rep: " << cc_as_ci << endl;

         MultiIndex mi_mp(mp,mpVccCalcDef);
         MultiIndex mi_mt(mt,mpVccCalcDef);
         MultiIndex mi_mq(mq,mpVccCalcDef);
         In np    = mi_mp.Size();

         if (gDebug)
            Mout << " Transforming vector for ModeCombi " << mq
                 << " with excitation space size " << n_exci_mt << endl
                 << " cc_as_ci " << cc_as_ci << endl;
         
         In it;
         In iq_nr;
         vector<In> ipvec(nmp,I_0);
         vector<In> itvec(nmt,I_0);
         vector<In> iqvec(nmq,I_0);

         vector<In> connect_p_t(nmt); // address of mt in mp
         vector<In> connect_p_q(nmq); // address of mq in mp

         for (In k=I_0; k<mp.Size(); k++)
         {
            for (In j=I_0; j<mt.Size(); j++)
               if (mt.Mode(j)==mp.Mode(k)) connect_p_t[j] = k;
            for (In j=I_0; j<mq.Size(); j++)
               if (mq.Mode(j)==mp.Mode(k)) connect_p_q[j] = k;
         }
         if (mpVccCalcDef->IoLevel() > 15) 
         {
            Mout << "VccTransformer::ExpT() connect_p_t       " << connect_p_t << endl;
            Mout << "VccTransformer::ExpT() connect_p_q       " << connect_p_q << endl;
         }

         for (In ip2=I_0; ip2<np; ip2++)
         {
            mi_mp.IvecForIn(ipvec,ip2);
            for (In j=I_0; j<mt.Size(); j++) itvec[j] = ipvec[connect_p_t[j]];
            for (In j=I_0; j<mq.Size(); j++) iqvec[j] = ipvec[connect_p_q[j]];
            mi_mt.InForIvec(itvec,it);
            mi_mq.InForIvec(iqvec,iq_nr);
            sigma_mp[ip2] += cc_as_ci[it]*inp_vec[iq_nr];
         }
         // Mout << " Norm of sigma_mp after cont 3 " << sigma_mp.Norm() 
            // << " for " << mq << " and " << mt << endl;
         //Mout << " cont cont " << sigma_mp2 << endl;
         //sigma_mp += sigma_mp2;
      }
      if (gDebug) 
      {
         //Mout << " Transformed Vector before storage " << sigma_mp << endl;
         Mout << "VccTransformer::ExpT() mp " << mp << " Norm  sigma_mp: " 
         << sigma_mp.Norm() << endl;
      }
      // Store this part of the sigma vector after adding all contributions 
      arDcOut.DataIo(IO_PUT,sigma_mp,n_exci_mp,
            arXvecOut.ModeCombiAddress(i_mode_combi_p));
   }
}

/**
* Copy arDcIn with A exci only and B in a small ModeCombiOpRange 
* to arDcOut with A 1.0 VSCF component and B a large ModeCombiOpRange
* */
void VccTransformer::SmallToLargeTrans(DataCont& arDcSmall,DataCont& arDcLarge, 
      ModeCombiOpRange& arMcS, ModeCombiOpRange& arMcL) 
   //, vector<Int>& arAddOfSinL)
{
   arDcLarge.Zero();
   In i_mode_combi = -I_1;
   for(  auto i = arMcS.begin(), end = arMcS.end()
      ;  i != end
      ;  ++i
      )
   {
      i_mode_combi++;
      In n_exci_s = mTransXvec.NexciForModeCombi(i_mode_combi);
      MidasVector tmp(n_exci_s);

      if (i_mode_combi >I_0) arDcSmall.DataIo(IO_GET,tmp,n_exci_s,i->Address()-I_1); 
      // The subt. of one is for the VSCF reference.
      else tmp[I_0] = C_1;

      ModeCombiOpRange::const_iterator j;
      if (arMcL.Find(*i, j))
      {
         // Mout << "mode combi " << *i << " of small is is " 
            // << *j << " in large " << endl;
         arDcLarge.DataIo(IO_PUT,tmp,n_exci_s,j->Address());
      }
   }
   Nb sn = arDcSmall.Norm();
   if (gDebug || mpVccCalcDef->IoLevel() > I_5)
   {
      Mout << " Norm of arDcSmall " << sn << endl;
      Mout << " Norm of arDcLarge " << arDcLarge.Norm() 
         << " (should be the same as for small " << endl;
      Mout << " Norm of (1+arDcSmall) " << sqrt(C_1+sn*sn) << endl;
   }
}

/**
* Do Vmp calculation
* */
void VccTransformer::VmpTransformer()
{
   if (gDebug)
   {
      Mout << " In VccTransformer::VmpTransformer()" << endl;
      Mout << " Norm of start vector: " << mXvec.Norm() << endl;
   }
   if (this->UseVcc2Trans())
   {
      const auto p_vcc2trans = FactoryVcc2Trans(VCC2_T::VCI);
      p_vcc2trans->Transform(Vcc2GetTAmps(), mTransXvec.GetXvecData());
   }
   else
      GenTransformer();
   if (gDebug)
      Mout << " Norm of H*vector: " << mTransXvec.Norm() << endl;
   TransformerH0(I_2);
   if (gDebug)
      Mout << " Norm of (H-H0)*vector: " << mTransXvec.Norm() << endl;
   if (gDebug)
      Mout << " VmpTransformer() Done." << endl;
}

/**
* Calculate <R| H | wf > where wf is either MP or Vcc,
* Restores mTransXvec which may actually be unnceccesary in some case (not all).
* */
Nb VccTransformer::TransCorrE(DataCont& arDcIn,In aI, In aJ, Nb aNb)
{
   mXvec.GetXvecData().NewLabel(arDcIn.Label(),false); 
   mXvec.GetXvecData() = arDcIn; 
   // if both on disc, this should have no effect, else it is needed.
   mTransXvec.GetXvecData().NewLabel("tmp_trans_cor_e",false);

   Nb e_vmp;

   /*
   // This version carries out the complete transformation! 
   // which is unnecessary!
   if (aI==0) TransformerH0(aJ,aNb); 
   if (aI==1) Transformer();
   mTransXvec.GetXvecData().DataIo(IO_GET,I_0,e_vmp);
   Mout << " try one " << e_vmp << endl;
   */
   
   // Store mTransXvec info
   ModeCombiOpRange oprtmp = mTransXvec.ExciRange();
   In nold = mTransXvec.GetXvecData().Size();

   // We do not use T1 integrals in this case.
   bool t1_save = mT1TransH;
   mT1TransH = false;
  
   // We have to go out-of-space since the mTransXvec is smaller than the actual
   // excitation space.
   bool oos = mpVccCalcDef->VccOutOfSpace();
   mpVccCalcDef->SetVccOutOfSpace(true);
   
   // Second version with only one (the ref) in the target space
   mTransXvec.ExciRange() = ModeCombiOpRange(I_0,I_0);
   mTransXvec.ExciRange().AssignAddress(I_0, I_0);
   mTransXvec.GetXvecData().NewLabel("tmp_trans_cor_e",false);
   mTransXvec.GetXvecData().SetNewSize(I_1);
   if (aI==0) TransformerH0(aJ,aNb); 
   if (aI==1) GenTransformer();
   mTransXvec.GetXvecData().DataIo(IO_GET,I_0,e_vmp);
   //Mout << " try two " << e_vmp << endl;

   // Reset mTransXvec info
   mTransXvec.ExciRange() = oprtmp;
   mTransXvec.GetXvecData().SetNewSize(nold);
   mT1TransH = t1_save;
   mpVccCalcDef->SetVccOutOfSpace(oos);

   Mout << "e_vmp = " << e_vmp << endl;
   return e_vmp;
}

/**
* Transforming arDcIn by H/H0 to arDcOut 
* */
void VccTransformer::TransH2(DataCont& arDcIn,DataCont& arDcOut,In aI, In aJ, Nb& aNb)
{
   //Mout << " transH2 in " << arDcIn << endl;
   //Mout << " transH2 out " << arDcIn << endl;
   if (aI==I_1)
   {
      TransH(arDcIn,arDcOut,aI,aJ,aNb);
      Nb x = -mLambda;
      DataCont tmp(arDcOut);
      tmp.Axpy(arDcIn,x);  
      TransH(tmp,arDcOut,aI,aJ,aNb);
      arDcOut.Axpy(tmp,x);  
   }
   else if (aI==I_0)
   {
      Nb x = aNb + mLambda;
      TransH(arDcIn,arDcOut,aI,aJ,x);
      DataCont tmp(arDcOut);
      TransH(tmp,arDcOut,aI,aJ,x);
      arDcOut.Scale(-C_1);
   }
}

/**
 *
 **/
bool VccTransformer::CmpDCs(DataCont& aDc1, DataCont& aDc2, const bool aVcc, const bool aVerbose)
{
   // For comparing floating point number.
   Nb thr = pow(C_10,-(C_10+C_3));
   if (sizeof(Nb) == sizeof(long double))
      thr = pow(C_10,-(C_10+C_3));
   
   // Do comparison.
   MidasVector v1;
   MidasVector v2;
   const ModeCombiOpRange& mcr = mTransXvec.GetModeCombiOpRange();
   bool failure = false;
   Nb max_rel = C_0;
   Nb max_dif = C_0;
   In first_mc = aVcc? I_1:I_0;
   for (In i_mc=first_mc; i_mc<mcr.Size(); ++i_mc)
   {
      const ModeCombi& mc = mcr.GetModeCombi(i_mc);
      if (aVerbose)
         Mout << "MC: " << mc << endl;

      In addr = mc.Address();
      if (aVcc)
         addr--;
      In n_exci = mTransXvec.NexciForModeCombi(i_mc);
      v1.SetNewSize(n_exci);
      v2.SetNewSize(n_exci);
      aDc1.DataIo(IO_GET, v1, n_exci, addr);
      aDc2.DataIo(IO_GET, v2, n_exci, addr);
      bool print = false;
      for (In i=I_0; i<v1.Size(); ++i)
      {
         Nb dif = fabs(v1[i]-v2[i]);
         if (dif > max_dif)
            max_dif = dif;
         Nb rel = fabs((v1[i]-v2[i])/v1[i]);
         if (rel > max_rel)
            max_rel = rel;
         if ((v1[i] == C_0 && v2[i]>1.0e-5) ||
             (dif > 1.0e-18 && v1[i]!=C_0 && rel>thr))
         {
            print = true;
            failure = true;
         }
      }
      if (print && aVerbose)
      {
         Mout.setf(ios::showpos);
         Mout << scientific;
         for (In i=I_0; i<v1.Size(); ++i)
         {
            Mout << "i=" << setw(3) << "   1: " << setw(14) << v1[i]
                 << "   2: " << setw(14) << v2[i]
                 << "     1-2: " << setw(14) << v1[i]-v2[i]
                 << "    rel: " << setw(14) << fabs((v1[i]-v2[i])/v1[i]) << endl;
         }
         Mout.unsetf(ios::showpos);
      }
   }
   if (aVerbose)
   {
      Mout << "Vector comparison: Max dif.:      " << max_dif << endl
           << "Vector comparison: Max rel. dev.: " << max_rel << endl;
      if (failure)
         Mout << "VECTOR COMPARE: SOMETHING WRONG!" << endl;
      else
         Mout << "VECTOR COMPARE: EVERYTHING SEEMS OK." << endl;
   }

   return !failure;
}

/**
* Make direct product of aN t vectors described
* by the vectors apT and indices apI.
* */
void DirProd(MidasVector* apT,MultiIndex* apI, MidasVector& arResultVec,
      const In aN,const MultiIndex& arResMi, In aLevel, vector<In>& arResInt,
      Nb arResNb,vector<In>* apConnect,const bool aMinusT)
{ 
   if (aLevel<aN)  // "Forward" the loop.
   {
      In n  = apI[aLevel].Size();
      In nv = apI[aLevel].VecSize();
      for (In i=I_0;i<n;i++)
      {
         vector<In> ivec(nv);
         apI[aLevel].IvecForIn(ivec,i);
         for (In j=I_0;j<nv;j++)
            arResInt[apConnect[aLevel][j]] = ivec[j]; // NOT Int[j]
         Nb new_val = arResNb*apT[aLevel][i];
         DirProd(apT,apI,arResultVec,aN,arResMi,aLevel+I_1,arResInt,new_val,apConnect,aMinusT);
      }
   } 
   else            // Finally add.
   {
      In add;
      arResMi.InForIvec(arResInt,add);
      if (aMinusT && (aN%I_2==I_1)) 
        arResultVec[add] -= arResNb;
      else 
        arResultVec[add] += arResNb;
   }
}
/**
* Calculates the one-body density matrix of a VCI wavefunction
* */
void VccTransformer::OneBodyDensity(Xvec arXvec, DataCont& arDC)
{

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin VCI one-body density calculation              ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   clock_t t0 = clock();

   arXvec.GetXvecData().NewLabel(arDC.Label(),false);
   In ndim =  mpVccCalcDef->GetNmodesInOcc();
   Mout << " ndim: " << ndim << endl;
   vector<In> occ_vec(ndim);      // The reference full occupation vector = 0
   for (In i=I_0;i<ndim;i++) occ_vec[i] =I_0;
   vector<In> occmax_vec(ndim);      // The maximum full occupation vector.
   vector<In> mode_index_low(ndim);      // The maximum full occupation vector.
   In dim_rho=I_0;

   for (In i_op_mode=I_0;i_op_mode<ndim;i_op_mode++) 
   {
      mode_index_low[i_op_mode]=dim_rho;
      occmax_vec[i_op_mode]   = mpVccCalcDef->Nmodals(i_op_mode) - I_1;
      dim_rho +=mpVccCalcDef->Nmodals(i_op_mode); 
   }
   Mout << " dim_rho: " << dim_rho << endl;

   MidasMatrix rho(dim_rho,C_0);
   string lowhig = "LOWHIG";
   bool exci_only=true; 
   Mout << "arDC: " << arDC << endl;
   Mout << "norm arDC: " << arDC.Norm() << endl;

   const ModeCombiOpRange& mcr = arXvec.GetModeCombiOpRange();
   In n_mode_combi_p = mcr.Size();
   Mout << " Total number of modecombi's: n_mode_combi_p= " << n_mode_combi_p << endl ;

   for (In i_mode_combi_0=I_0; i_mode_combi_0<mcr.Size(); ++i_mode_combi_0)
   {
      Mout << "------------------------" << endl;
      const ModeCombi& m0 = mcr.GetModeCombi(i_mode_combi_0);
      Mout << " i_mode_combi_0: " << i_mode_combi_0 
         << " m0: " << m0.MCVec() << endl ;
      In n_exci_m0 = arXvec.NexciForModeCombi(i_mode_combi_0);
      //Mout << " n_exci_m0: " << n_exci_m0 << endl ;
      In ndim0 = m0.Size();
      //Mout << " Size of m0 -- ndim0: " << ndim0 << endl ;

      vector<In> occ0(ndim0);
      vector<In> occmax0(ndim0);
      for (In i=0;i<ndim0;i++) 
      {
         occ0[i] = I_0;
         In i_op_mode =  m0.Mode(i);
         In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
         occmax0[i]   = nmod-I_1;
      }
      MultiIndex mi0(occ0,occmax0,lowhig,exci_only);
      //In n0 = mi0.Size();
      //Mout << " Size of the MultiIndex: " << n0 << endl ;

      // Get the vector to use
      MidasVector xvec_m0(n_exci_m0,C_0);
      //arDC.DataIo(IO_GET,xvec_m0,n_exci_m0,m0.Address());
      arDC.DataIo(IO_GET,xvec_m0,n_exci_m0,arXvec.ModeCombiAddress(i_mode_combi_0));
         //Mout << " xvec_m0 " << xvec_m0 << endl;

      // Outer loop through all the mode combinations
      for (In i_exci=I_0;i_exci<n_exci_m0;i_exci++)
      {
         // Get the i_exci-th occupation vector tmpi
         vector<In> tmpi(occ0);
         mi0.IvecForIn(tmpi,i_exci);
         //Mout << " tmpi: " << tmpi << endl; 

         // Loop through all the excitations j_exci >= i_exci+I_1
         for (In j_exci=i_exci;j_exci<n_exci_m0;j_exci++)
         {
            Mout << "------------------------" << endl;
            vector<In> tmpj(occ0);
            mi0.IvecForIn(tmpj,j_exci);
            //Mout << xvec_m0[i_exci]  << m0.MCVec() << tmpi << tmpj  << m0.MCVec() <<  xvec_m0[j_exci] << endl ;
            In n_diff=I_0;

            for (In k=I_0;k<ndim0;k++)
            {
               if (tmpi[k]!=tmpj[k]) n_diff++;
            }
            Mout << " n_diff: " << n_diff << endl;

            if (n_diff==I_0)
            {
               Mout << xvec_m0[i_exci] << " {"  << m0.MCVec()<< "} " << tmpi << " - "  
                  << "{"<< m0.MCVec()<< "} "  << tmpj<< " " <<  xvec_m0[j_exci] << endl ;
               In mode_idx=I_0;
               for (In k=I_0;k<ndim;k++)
               {
                  In idx;
                  if (m0.IncludeMode(k))
                  {
                     idx = mode_index_low[k]+tmpi[mode_idx];
                     mode_idx++;
                  }
                  else
                  {
                     idx=mode_index_low[k];
                  }
                  rho[idx][idx]+=xvec_m0[i_exci]*xvec_m0[i_exci];
                  Mout << "rho[" << idx << "][" << idx << "] + = "<< xvec_m0[i_exci]*xvec_m0[i_exci] << endl;
                  //Mout << "rho[" << idx << "][" << idx << "] = "<< rho[idx][idx] << endl;
               }
            }
            else if (n_diff==I_1)
            {
               Mout << xvec_m0[i_exci] << " {"  << m0.MCVec()<< "} " << tmpi << " - "  
                  << "{"<< m0.MCVec()<< "} "  << tmpj<< " " <<  xvec_m0[j_exci] << endl ;
               In mode_idx=I_0;
               for (In k=I_0;k<ndim;k++)
               {
                  In idxi,idxj;
                  if (m0.IncludeMode(k))
                  {
                     idxi = mode_index_low[k]+tmpi[mode_idx];
                     idxj = mode_index_low[k]+tmpj[mode_idx];
                     mode_idx++;
                  }
                  else
                  {
                     idxi=mode_index_low[k];
                     idxj=idxi;
                  }
                  rho[idxi][idxj]+=xvec_m0[i_exci]*xvec_m0[j_exci];
                  rho[idxj][idxi]+=xvec_m0[i_exci]*xvec_m0[j_exci];
                  Mout << "rho[" << idxi << "][" << idxj << "] + = "<< xvec_m0[i_exci]*xvec_m0[j_exci] << endl;
                  Mout << "rho[" << idxj << "][" << idxi << "] + = "<< xvec_m0[i_exci]*xvec_m0[j_exci] << endl;
                  //Mout << "rho[" << idxi << "][" << idxj << "] = "<< rho[idxi][idxj] << endl;
                  //Mout << "rho[" << idxj << "][" << idxi << "] = "<< rho[idxj][idxi] << endl;
               }
            }
         }

         // systematically delete one mode from mp and found contributions to rho
         for (In i_del=I_0;i_del<ndim0;i_del++)
         {
            //Mout << "------------------------" << endl;
            ModeCombi m1(m0);
            m1.RemoveMode(m1.Mode(i_del));   
            In ndim1 = m1.Size();
            //Mout << " ndim1: " << ndim1 << endl ;
            vector<In> occ1(ndim1);
            vector<In> occmax1(ndim1);
            for (In i=I_0;i<ndim1;i++) 
            {
               occ1[i] = I_0;
               In i_op_mode =  m1.Mode(i);
               In nmod      = mpVccCalcDef->Nmodals(i_op_mode);
               occmax1[i]   = nmod-I_1;
            }
            MultiIndex mi1(occ1,occmax1,lowhig,exci_only);
            //In n1 = mi1.Size();

            ModeCombiOpRange::const_iterator iq_m;
            In i_mode_combi_1;
            arXvec.Find(m1,iq_m,i_mode_combi_1);
            //Mout << " i_mode_combi_1: " << i_mode_combi_1 
            //   << " m1: " << m1.MCVec() << endl ;
            In n_exci_m1 = arXvec.NexciForModeCombi(i_mode_combi_1);
            //Mout << " n_exci_m1: " << n_exci_m1 << endl ;

            // Get the vector to use
            MidasVector xvec_m1(n_exci_m1,C_0);
            arDC.DataIo(IO_GET,xvec_m1,n_exci_m1,arXvec.ModeCombiAddress(i_mode_combi_1));
            //arDC.DataIo(IO_GET,xvec_m1,n_exci_m1,m1.Address());
            //Mout << " xvec_m1 " << xvec_m1 << endl;

            //for (In i_exci=I_0;i_exci<n_exci_m0;i_exci++)
            //{
            //   // Get the i_exci-th occupation vector tmpi
            //   vector<In> tmpi(occ0);
            //   mi0.IvecForIn(tmpi,i_exci);


            //j_exci=I_0;
            // Loop through all the excitations j_exci >= i_exci+I_1
            for (In j_exci=I_0;j_exci<n_exci_m1;j_exci++)
            {
               vector<In> tmpj(occ1);
               mi1.IvecForIn(tmpj,j_exci);

               In mode_idx_i=I_0;
               In mode_idx_j=I_0;
               bool the_same=true;

               while (the_same==true && mode_idx_i<ndim0)
               {
                  if (mode_idx_i==i_del)
                  {
                     mode_idx_i++;
                     continue;
                  }
                  if (tmpj[mode_idx_j]!=tmpi[mode_idx_i]) the_same=false;
                  mode_idx_i++;
                  mode_idx_j++;
               }
               //Mout << " the same: " << StringForBool(the_same) << endl;

               if (the_same==true)
               {
                  Mout << "------------------------" << endl;
                  Mout << xvec_m0[i_exci] << " {"  << m0.MCVec()<< "} " << tmpi << " - "   
                     << "{"<< m1.MCVec()<< "} " << tmpj << " " <<  xvec_m1[j_exci] << endl ;
                  In mode_idx_i=I_0;
                  In mode_idx_j=I_0;
                  for (In k=I_0;k<ndim;k++)
                  {
                     In idxi,idxj;
                     if (m0.IncludeMode(k))
                     {
                        idxi = mode_index_low[k]+tmpi[mode_idx_i];
                        mode_idx_i++;
                     }
                     else
                     {
                        idxi=mode_index_low[k];
                     }
                     if (m1.IncludeMode(k))
                     {
                        idxj = mode_index_low[k]+tmpj[mode_idx_j];
                        mode_idx_j++;
                     }
                     else
                     {
                        idxj=mode_index_low[k];
                     }
                     rho[idxi][idxj]+=xvec_m0[i_exci]*xvec_m1[j_exci];
                     rho[idxj][idxi]+=xvec_m0[i_exci]*xvec_m1[j_exci];
                     Mout << "rho[" << idxi << "][" << idxj << "] + = "<< xvec_m0[i_exci]*xvec_m1[j_exci] << endl;
                     Mout << "rho[" << idxj << "][" << idxi << "] + = "<< xvec_m0[i_exci]*xvec_m1[j_exci] << endl;
                     //Mout << "rho[" << idxi << "][" << idxj << "] = "<< rho[idxi][idxj] << endl;
                     //Mout << "rho[" << idxj << "][" << idxi << "] = "<< rho[idxj][idxi] << endl;
                  }
               }
               //if (k_exci==n_exci_m1-I_1) j_exci=I_0; 
               //else j_exci++;
            }
            //}
         }
      }

   }
   Mout << "------------------------" << endl;
   Nb sum_occ_num=C_0;
   for (In i_mode=I_0;i_mode<ndim;i_mode++)
   {
      Mout << " Mode #" << i_mode << endl;
      In idx_low=(occmax_vec[i_mode]+I_1)*i_mode;
      In idx_high=(occmax_vec[i_mode]+I_1)*(i_mode+I_1);
      MidasMatrix rho_part(dim_rho/ndim,C_0);
      In count_j=I_0;
      In count_k=I_0;
      for (In j=idx_low;j<idx_high;j++)
      {
         for (In k=idx_low;k<idx_high;k++)
         {
            rho_part[count_j][count_k]=rho[j][k];
            //Mout << "["<< count_j << "][" << count_k << "] <- "<< "["<< j << "][" << k << "]" << endl;
            ++count_k;
         }
         ++count_j;
         count_k=I_0;
      }
      Mout << rho_part << endl;
      MidasVector eig_val(dim_rho/ndim,C_0);
      MidasMatrix eig_vec(dim_rho/ndim,C_0);
      string diag_meth;
      if (gNumLinAlg=="MIDAS") diag_meth="MIDAS_JACOBI";
      else diag_meth="DSYEVD";
      Diag(rho_part,eig_vec,eig_val, diag_meth);

      Mout << " Natural modal occupation numbers: " << eig_val << endl;
      Nb tot_occnum=C_0;
      for (In i=I_0;i<eig_val.Size();i++) tot_occnum+=eig_val[i];
      Mout << " Total occupation: " << tot_occnum << endl;
      sum_occ_num+=tot_occnum;
      for (In j=I_0;j<dim_rho/ndim;j++)
      {
         for (In k=I_0;k<dim_rho/ndim;k++)
         {
            Mout << "eig_vec["<< k << "][" << j << "] = " << eig_vec[k][j]<< endl;
         }
      }
   }
   Mout << "------------------------" << endl;
   Mout << "\n Total sum of  occupations: " << sum_occ_num << endl;
   Mout << "------------------------" << endl;



   MidasVector eig_val(dim_rho,C_0);
   MidasMatrix eig_vec(dim_rho,C_0);
   string diag_meth;
   if (gNumLinAlg=="MIDAS") diag_meth="MIDAS_JACOBI";
   else diag_meth="DSYEVD";
   Diag(rho,eig_vec,eig_val, diag_meth);
   Mout << " Natural modal occupation numbers: " << eig_val << endl;
   Nb tot_occnum=C_0;
   for (In i=I_0;i<eig_val.Size();i++) tot_occnum+=eig_val[i];
   Mout << " Total occupation: " << tot_occnum << endl;


   bool output_rho = true;
   if (output_rho)
   {
      Mout << "\n One-Body density matrix:\n ------------------------" << endl;
      for (In i1=0;i1<dim_rho;i1++)
      {
         for (In i2=0;i2<dim_rho;i2++)
         {
            Mout << setw(24) << right << rho[i1][i2];
         }
         Mout << endl;
      }
      midas::mpi::OFileStream rho_out(gAnalysisDir + "/RHO.mat");

      rho_out.setf(ios_base::scientific, ios_base::floatfield);
      midas::stream::ScopedPrecision(6, rho_out);
      for (In i1=0;i1<dim_rho;i1++)
      {
         for (In i2=0;i2<dim_rho;i2++)
         {
            rho_out << setw(14) << right << rho[i1][i2];
         }
         rho_out << endl;
      }
   }

   Mout << "OneBodyDensity time: " << Nb(clock()-t0) / CLOCKS_PER_SEC << " sec" << endl;
   Mout << endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," VCI one-body density calculation done  ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << endl;

}

/**
 *
 **/
void VccTransformer::PutOperatorToWorkingOperatorAndUpdateModalIntegrals
   (  Vscf* apVscf
   ,  OpDef* apOpDef
   ,  OneModeInt* apOneModeInt
   )
{
   apVscf->PutOperatorToWorkingOperator(apOpDef, apOneModeInt);
   this->SetOpDef(apOpDef);
   this->UpdateModalIntegrals();
}

/**
 *
 **/
void VccTransformer::RestoreOrigOperAndUpdateModalIntegrals
   (  Vscf* apVscf
   )
{
   apVscf->RestoreOrigOper();
   this->SetOpDef(apVscf->pOpDef());
   this->UpdateModalIntegrals();
}

/**
 *
 **/
In VccTransformer::VectorDimension() const
{ 
   switch(mType)
   {
      case TRANSFORMER::VCC:
         return NexciTransXvec() - I_1; 
      case TRANSFORMER::VCI:
         return NexciTransXvec();
      case TRANSFORMER::VCIRSP:
         return NexciTransXvec() - I_1;
      case TRANSFORMER::VCCJAC:
         return NexciTransXvec() - I_1;
      case TRANSFORMER::LVCCJAC:
         return NexciTransXvec() - I_1;
      default:
         MIDASERROR("VectorDimension(): NOT IMPLEMENTED FOR CHOOSEN TYPE: " + TRANSFORMER_MAP::MAP.at(mType));
   }

   return 0; // should/can not reach here
}

/**
 *
 **/
ModalIntegrals<Nb>* VccTransformer::pOneModeModalBasisInt()
{
   return &mOneModeModalBasisInt;
}

/**
 *
 **/
const ModalIntegrals<Nb>* VccTransformer::pOneModeModalBasisInt() const
{
   return &mOneModeModalBasisInt;
}

/***************************************************************************//**
 * Updates VccTransformer::mOneModeModalBasisInt to either be equal to the
 * original modal integrals or the T1-transformed ones, depending on arguments.
 *
 * @param[in] aT1
 *    If false, the resulting integrals will be the original ones.
 *    If true, will copy the original integrals, then T1-transform with the
 *    T1-amplitudes in the Xvec.
 * @param[in] aXvec
 *    If aT1 is false, this isn't used and can just be nullptr.
 *    If aT1 is true, the T1-amplitudes shall be taken from aXvec.
 ******************************************************************************/
void VccTransformer::UpdateModalIntegrals
   (  bool aT1
   ,  const Xvec* const aXvec
   )
{
   // Always reset to the original ones.
   *pOneModeModalBasisInt() = *mpOneModeModalBasisIntOrig;

   // Only do extra work if T1-transforming.
   if (aT1)
   {
      // Set up for extracting T1 amps to temp. container.
      const auto& n_modes = pOneModeModalBasisInt()->NModes();
      std::vector<MidasVector> v_t1_amps;
      v_t1_amps.reserve(n_modes);

      // Then fetch the T1 amps for each mode.
      for(In m = 0; m < n_modes; ++m)
      {
         ModeCombi mc(std::vector<In>{m}, -1);
         ModeCombiOpRange::const_iterator it_mc;
         In i_mc;
         aXvec->Find(mc, it_mc, i_mc);
         MidasVector amps(aXvec->NexciForModeCombi(i_mc)+I_1); // First element will be zero.
         aXvec->GetModeCombiData(amps, mc, true); // true as amps has a reference
         v_t1_amps.push_back(std::move(amps));
      }

      // Lastly, transform.
      pOneModeModalBasisInt()->T1Transform(v_t1_amps);
   }
}

/***************************************************************************//**
 * Copies the T1-integrals, then R1-transforms them using argument R1-coefs.
 * The Xvec is _only_ used for some address lookups for use with the DataCont.
 * 
 * @param[in] arT1Ints
 *    Modal integrals, assumed to be T1-transformed already.
 * @param[in] arR1Coefs
 *    Contains the R1-coefs. (possibly along with the rest of the R vector).
 * @param[in] arXvec
 *    Used for indexing/addressing logic in arR1Coefs.
 ******************************************************************************/
void VccTransformer::UpdateR1Integrals
   (  const ModalIntegrals<Nb>& arT1Ints
   ,  const DataCont& arR1Coefs
   ,  const Xvec& arXvec
   )
{
   // Copy T1-integrals.
   mR1Ints = arT1Ints;
   mR1Ints.SetName(mpVcc->Name() + "_R1_integrals");

   // Set up for extracting T1 amps to temp. container.
   const auto& n_modes = mR1Ints.NModes();
   std::vector<MidasVector> v_r1_coefs;
   v_r1_coefs.reserve(n_modes);

   // Then fetch the R1 amps for each mode.
   for(In m = 0; m < n_modes; ++m)
   {
      // Get one-mode response coefficients for this mode.
      ModeCombi mc(std::vector<In>{m}, -1);
      ModeCombiOpRange::const_iterator it_mc;
      In i_mc;
      arXvec.Find(mc, it_mc, i_mc);
      In n_exci = arXvec.NexciForModeCombi(i_mc);
      In address = arXvec.ModeCombiAddress(i_mc);
      MidasVector coefs(n_exci+I_1);      // First element will be zero
                                          // (For more readable code below).
      arR1Coefs.DataIo(IO_GET, coefs, n_exci, address, I_1, I_1);
      v_r1_coefs.push_back(std::move(coefs));
   }
   mR1Ints.R1Transform(v_r1_coefs);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
std::unique_ptr<VccTransformer::vcc2transbase_t> VccTransformer::FactoryVcc2Trans
   (  VCC2_T aType
   )
{
   using vcc2transreg_t = midas::vcc::vcc2::Vcc2TransReg<value_t>;
   using vcc2transeta_t = midas::vcc::vcc2::Vcc2TransEta<value_t>;
   using vcc2transrjac_t = midas::vcc::vcc2::Vcc2TransRJac<value_t>;
   using vcc2transljac_t = midas::vcc::vcc2::Vcc2TransLJac<value_t>;

   if (!mpVccCalcDef->TwoModeTrans() && !mpVccCalcDef->OneModeTrans())
   {
      MIDASERROR("FactoryVcc2Trans called in error; TwoModeTrans() and OneModeTrans() both false.");
   }

   std::unique_ptr<vcc2transbase_t> p(nullptr);
   const auto* const p_opdef = mpOpDef;
   const auto& raw_mod_ints = *mpOneModeModalBasisIntOrig;
   const auto& mcr = mXvec.GetModeCombiOpRange();

   switch(aType)
   {
      case VCC2_T::VCC:
      {
         p.reset(new vcc2transreg_t(p_opdef, raw_mod_ints, mcr, !mpVccCalcDef->TwoModeTrans()));
         break;
      }
      case VCC2_T::VCI:
      {
         auto* p_tmp = new vcc2transreg_t(p_opdef, raw_mod_ints, mcr, !mpVccCalcDef->TwoModeTrans());
         p_tmp->DoingVcc() = false;
         p.reset(p_tmp);
         break;
      }
      case VCC2_T::ETA:
      {
         p.reset(new vcc2transeta_t(p_opdef, raw_mod_ints, mcr, !mpVccCalcDef->TwoModeTrans()));
         break;
      }
      case VCC2_T::RJAC:
      {
         p.reset(new vcc2transrjac_t(p_opdef, raw_mod_ints, mcr, Vcc2GetTAmps(), !mpVccCalcDef->TwoModeTrans(), mpVccCalcDef->IoLevel(), mpVccCalcDef->TimeIt()));
         break;
      }
      case VCC2_T::LJAC:
      {
         p.reset(new vcc2transljac_t(p_opdef, raw_mod_ints, mcr, Vcc2GetTAmps(), !mpVccCalcDef->TwoModeTrans(), mpVccCalcDef->IoLevel(), mpVccCalcDef->TimeIt()));
         break;
      }
      default:
      {
         MIDASERROR("Unknown VCC2_T.");
         break;
      }
   }

   p->IoLevel() = mpVccCalcDef->IoLevel();
   p->TimeIt() = mpVccCalcDef->TimeIt();

   // Only has effect for VCI, VCC, RJAC, but we'll just set it generally.
   p->ScreenNorm2Thr() = mpVccCalcDef->TwoModeTransScreen();

   return p;
}

/***************************************************************************//**
 * If the requested transformer object is not constructed yet _or_ if it's
 * Reuse() is not enabled, this function call will construct it according to
 * the currently stored T-amplitudes, OpDef, etc.
 *
 * The transformer is then kept (in any case) until calling ClearVcc2Trans(),
 * meaning that T1-integrals and intermediates need not be recalculated at the
 * next call to this function (if Reuse() is enabled at that time, that is).
 *
 * Reuse() is enabled/disabled by calling ReuseVcc2Trans()/ClearVcc2Trans()
 * respectively.
 *
 * @param[in] aType
 *    Whether to get RJAC or LJAC transformer.
 * @return
 *    Pointer to response transformer of request type, stored by this
 *    VccTransformer object.
 ******************************************************************************/
const VccTransformer::vcc2transrsp_t* VccTransformer::FactoryVcc2Trans
   (  VCC2RSP_T aType
   )
{
   using vcc2transrjac_t = midas::vcc::vcc2::Vcc2TransRJac<value_t>;
   using vcc2transljac_t = midas::vcc::vcc2::Vcc2TransLJac<value_t>;

   if (!mpVccCalcDef->TwoModeTrans() && !mpVccCalcDef->OneModeTrans())
   {
      MIDASERROR("FactoryVcc2Trans called in error; TwoModeTrans() and OneModeTrans() both false.");
   }

   vcc2transrsp_t* p_rsp_trans = nullptr;
   const auto* const p_opdef = mpOpDef;
   const auto& raw_mod_ints = *mpOneModeModalBasisIntOrig;
   const auto& mcr = mXvec.GetModeCombiOpRange();

   switch(aType)
   {
      case VCC2RSP_T::RJAC:
      {
         if (!mVcc2TransRJac || !mVcc2TransRJac.Reuse())
         {
            if (gDebug || mpVccCalcDef->IoLevel() > I_10)
            {
               Mout << "Constructing transformer for VCC2RSP_T::RJAC." << std::endl;
            }
            mVcc2TransRJac.reset(new vcc2transrjac_t(p_opdef, raw_mod_ints, mcr, Vcc2GetTAmps(), !mpVccCalcDef->TwoModeTrans(), mpVccCalcDef->IoLevel(), mpVccCalcDef->TimeIt()));
         }
         p_rsp_trans = mVcc2TransRJac.get();
         break;
      }
      case VCC2RSP_T::LJAC:
      {
         if (!mVcc2TransLJac || !mVcc2TransLJac.Reuse())
         {
            if (gDebug || mpVccCalcDef->IoLevel() > I_10)
            {
               Mout << "Constructing transformer for VCC2RSP_T::LJAC." << std::endl;
            }
            mVcc2TransLJac.reset(new vcc2transljac_t(p_opdef, raw_mod_ints, mcr, Vcc2GetTAmps(), !mpVccCalcDef->TwoModeTrans(), mpVccCalcDef->IoLevel(), mpVccCalcDef->TimeIt()));
         }
         p_rsp_trans = mVcc2TransLJac.get();
         break;
      }
      default:
      {
         MIDASERROR("Unknown VCC2RSP_T.");
         break;
      }
   }

   p_rsp_trans->IoLevel() = mpVccCalcDef->IoLevel();
   p_rsp_trans->TimeIt() = mpVccCalcDef->TimeIt();

   // Only has effect for VCI, VCC, RJAC, but we'll just set it generally.
   p_rsp_trans->ScreenNorm2Thr() = mpVccCalcDef->TwoModeTransScreen();

   return p_rsp_trans;
}

/***************************************************************************//**
 * Enables Vcc2TransRspWrapper::Reuse() so that once the transformer is
 * constructed at first FactoryVcc2Trans(VCC2RSP_T) call it will be reused at
 * subsequent calls.
 *
 * If a transformer object is currently stored this will be deleted, as a
 * safety precaution.
 *
 * Only has effect if this object is set up for VCC[2]/H2 transformations
 * (UseVcc2Trans()).
 *
 * @param[in] aType
 *    Whether to apply to RJAC or LJAC transformer.
 ******************************************************************************/
void VccTransformer::ReuseVcc2Trans
   (  VCC2RSP_T aType
   )
{
   if (this->UseVcc2Trans())
   {
      Vcc2TransRspWrapper* p_wrapper = nullptr;
      std::string s;
      switch(aType)
      {
         case VCC2RSP_T::RJAC:
            p_wrapper = &mVcc2TransRJac;
            s = "RJAC";
            break;
         case VCC2RSP_T::LJAC:
            p_wrapper = &mVcc2TransLJac;
            s = "LJAC";
            break;
         default:
            MIDASERROR("Unknown VCC2RSP_T.");
            break;
      }

      if (p_wrapper != nullptr)
      {
         if (p_wrapper->Reuse())
         {
            MIDASERROR("Reuse() already true; mismatched ReuseVcc2Trans()/ClearVcc2Trans() pair?");
         }
         if (gDebug || mpVccCalcDef->IoLevel() > I_10)
         {
            Mout << "Enabling reuse of VCC2RSP_T::" << s << " transformer." << std::endl;
         }
         p_wrapper->reset();
         p_wrapper->Reuse() = true;
      }
      else
      {
         MIDASERROR("p_wrapper unexpectedly nullptr");
      }
   }
}

/***************************************************************************//**
 * Deconstructs specified transformer. Call this function after you're done
 * with all your response calculation iterations, so that the next client won't
 * get erroneous results from accidentally reusing your stored
 * T-integrals/intermediates.
 *
 * Also disables Reuse().
 *
 * Only has effect if this object is set up for VCC[2]/H2 transformations
 * (UseVcc2Trans()).
 *
 * @param[in] aType
 *    Whether to clear (deconstruct) the RJAC or LJAC transformer.
 ******************************************************************************/
void VccTransformer::ClearVcc2Trans
   (  VCC2RSP_T aType
   )
{
   if (this->UseVcc2Trans())
   {
      Vcc2TransRspWrapper* p_wrapper = nullptr;
      std::string s;
      switch(aType)
      {
         case VCC2RSP_T::RJAC:
            p_wrapper = &mVcc2TransRJac;
            s = "RJAC";
            break;
         case VCC2RSP_T::LJAC:
            p_wrapper = &mVcc2TransLJac;
            s = "LJAC";
            break;
         default:
            MIDASERROR("Unknown VCC2RSP_T.");
            break;
      }

      if (p_wrapper != nullptr)
      {
         if (!p_wrapper->Reuse())
         {
            MIDASERROR("Reuse() already false; mismatched ReuseVcc2Trans()/ClearVcc2Trans() pair?");
         }
         if (gDebug || mpVccCalcDef->IoLevel() > I_10)
         {
            Mout << "Disabling reuse of VCC2RSP_T::" << s << " transformer." << std::endl;
         }
         p_wrapper->reset();
         p_wrapper->Reuse() = false;
      }
      else
      {
         MIDASERROR("p_wrapper unexpectedly nullptr");
      }
   }
}

/**
 *
 **/
const std::vector<In>& VccTransformer::GetNModals() const
{
   return mpVccCalcDef->GetNModals();
}

/**
 * clone
 **/
Transformer* VccTransformer::Clone() const
{
   VccTransformer* ptr = new VccTransformer(*this);
   return ptr;
}

/**
 * clone
 **/
Transformer* VccTransformer::CloneImprovedPrecon(In aLevel) const
{
   Mout << " VccTransformer clone " << std::endl;
   VccTransformer* ptr = new VccTransformer(const_cast<VccTransformer&>(*this),aLevel);
   Mout << " Cloned and return " << std::endl;
   return ptr;
}

void VccTransformer::CustomAmp(DataCont& aAmpDC)
{
   In vecsize = aAmpDC.Size();
   In sin_size = mXvec.SinSize(); 
   MidasVector amp_vec(vecsize, C_0);
   Nb sin_amp = C_0;
   Nb doub_amp = C_1;
   for (In i = I_0; i < sin_size ; i++)
   {
      amp_vec[i] = sin_amp; 
   } 
   for (In i = sin_size; i < vecsize ; i++)
   {
      doub_amp += C_2;
      amp_vec[i] = doub_amp; 
   } 
   aAmpDC.DataIo(IO_PUT,amp_vec,vecsize); 
}

void VccTransformer::TwoModeVccRspInit()
{
   // Load the T amplitudes into mXvec for use in response transformer.
   string s = mpVccCalcDef->GetName() + "_Vcc_vec_0";
   In n_amp = mTransXvec.Size() - 1;
   datacont_t tmp_amp_dc;
   tmp_amp_dc.GetFromExistingOnDisc(n_amp, s);
   tmp_amp_dc.SaveUponDecon();

   // We need to put the reference at index zero to get correspondence with Xvec addresses.
   tmp_amp_dc.Reassign(mXvec.GetXvecData(), I_1, -I_1);
}

/**
 *
 **/
VccTransformer::Vcc2TransRspWrapper::Vcc2TransRspWrapper
   (  vcc2transrsp_t* p
   )
   :  std::unique_ptr<vcc2transrsp_t>(p)
{
}

/**
 *
 **/
VccTransformer::Vcc2TransRspWrapper::Vcc2TransRspWrapper
   (  const Vcc2TransRspWrapper&
   )
   :  std::unique_ptr<vcc2transrsp_t>(nullptr)
{
}

/**
 *
 **/
VccTransformer::Vcc2TransRspWrapper& VccTransformer::Vcc2TransRspWrapper::operator=
   (  const Vcc2TransRspWrapper&
   )
{
   reset();
   mReuse = false;
   return *this;
}
