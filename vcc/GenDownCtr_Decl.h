/**
 ************************************************************************
 * 
 * @file                GenDownCtr_Decl.h
 *
 * Created:             16-12-2009
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
 *
 * Short Description:   Functions for doing generalized down contractions.
 * 
 * Last modified:
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef GENDOWNCTR_DECL_H_INCLUDED
#define GENDOWNCTR_DECL_H_INCLUDED

#include <vector>

#include "input/ModeCombi.h"
#include "mmv/MidasVector.h"
#include "v3/VccEvalData.h"

namespace midas::vcc
{

// Identifiers for specific algorithm to use.
const In GEN_DOWN_CTR_UNKNOWN = I_0;
const In GEN_DOWN_CTR_GEN     = I_1;      ///< General down contraction.
const In GEN_DOWN_CTR_SIMPLE  = I_2;      ///< Used if MCs are identical.
const In GEN_DOWN_CTR_1D      = I_4;      ///< Used if down MC has dimension one.

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class GenDownCtr
{
   public:
      using evaldata_t = DATA<T>;

   protected:
      In         mAlgo;          ///< The specific algorithm to be used.

      // Variables and functions for GEN_DOWN_CTR_GEN algorithm.
      ModeCombi  mGenInMc;       ///< MC for input vector.
      std::vector<In> mGenNmodals;    ///< mNmodals[i] is Number of excited modals in mode mInMc.Mode(i).
      std::vector<In> mGenDownIdx;    ///< Vector of indices into aInMc. The set of modes
                                 ///< aInMc.Mode(mDownIdx[i]) specifies the MC of the down vector.
      std::vector<In> mGenResIdx;     ///< Vector of indices into aInMc. The set of modes
                                 ///< aInMc.Mode(mResIdx[i]) specifies the MC of the result vector.
      
      void InitGeneral(const evaldata_t& aEvalData, const ModeCombi& aInMc, const ModeCombi& aDownMc);
      void CtrGeneral(const GeneralMidasVector<T>& aInVec, const GeneralMidasVector<T>& aDownVec, GeneralMidasVector<T>& aResVec) const;


      // Variables and functions for GEN_DOWN_CTR_1D algorithm.
      In m1DNbef;
      In m1DNafter;
      
      void Init1D(const evaldata_t& aEvalData, const ModeCombi& aInMc, const ModeCombi& aDownMc);
      void Ctr1D(const GeneralMidasVector<T>& aInVec, const GeneralMidasVector<T>& aDownVec, GeneralMidasVector<T>& aResVec) const;
      
      // Function for GEN_DOWN_CTR_SIMPLE algorithm.      
      void CtrSimple(const GeneralMidasVector<T>& aInVec, const GeneralMidasVector<T>& aDownVec, GeneralMidasVector<T>& aResVec) const;
      

      // Variables for statistics / profiling. 
      static In mNtot;
      static In mNgen;
      static In mNsimple;
      static In mN1D;

   public:
      GenDownCtr();
      GenDownCtr(const evaldata_t& aEvalData, const ModeCombi& aInMc, const ModeCombi& aDownMc);
      
      void Init (const evaldata_t& aEvalData, const ModeCombi& aInMc, const ModeCombi& aDownMc);
      ///< Initialize contraction. Algorithm is automatically selected.
      
      void Contract(const GeneralMidasVector<T>& aInVec, const GeneralMidasVector<T>& aDownVec, GeneralMidasVector<T>& aResVec) const;
      ///< Perform contraction of aInVec with aDownVec and add result to aRes.

      static void ClearStatistics(); // Reset counters.
      static void PrintStatistics(); // Print algorithm usage.
};

} /* namespace midas::vcc */

#endif  /* GENDOWNCTR_DECL_H_INCLUDED */
