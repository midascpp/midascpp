#ifndef DIRPRODSPECIAL_H_INCLUDED
#define DIRPRODSPECIAL_H_INCLUDED

#include <vector>

#include "inc_gen/TypeDefs.h"
#include "input/ModeCombi.h"
#include "tensor/NiceTensor.h"

namespace midas::vcc::dirprod
{

// special tensor direct products
template
   <  typename T
   >
bool DirProdTensorSimpleSpecial
   (  NiceTensor<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   );

template
   <  typename T
   >
bool DirProdTensorSimpleSpecialMixed
   (  NiceTensor<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   );

} /* namespace midas::vcc::dirprod */

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "DirProdSpecial_Impl.h"
#include "DirProdSpecialMixed_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* DIRPRODSPECIAL_H_INCLUDED */
