#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "inc_gen/TypeDefs.h"
#include "TensorSumAccumulator.h"
#include "TensorSumAccumulator_Impl.h"

namespace midas::vcc
{
// define 
#define INSTANTIATE_TENSORSUMACCUMULATOR(T) \
template class TensorSumAccumulator<T>; \

// concrete instantiations
INSTANTIATE_TENSORSUMACCUMULATOR(Nb)
INSTANTIATE_TENSORSUMACCUMULATOR(std::complex<Nb>)

#undef INSTANTIATE_TENSORSUMACCUMULATOR

#define INSTANTIATE_TENSORSUMACCUMULATOR_DISABLE(T) \
template const midas::tensor::TensorDecomposer& TensorSumAccumulator<T>::GetDecomposer(midas::type_traits::DisableIfComplexT<T>*) const; \
template void TensorSumAccumulator<T>::SetDecompThreshold(typename TensorSumAccumulator<T>::real_t, midas::type_traits::DisableIfComplexT<T>*);

INSTANTIATE_TENSORSUMACCUMULATOR_DISABLE(Nb);

#undef INSTANTIATE_TENSORSUMACCUMULATOR_DISABLE

} /* namespace midas::vcc */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

