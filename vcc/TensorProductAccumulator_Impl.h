#ifndef TENSORPRODUCTACCUMULATOR_IMPL_H_INCLUDED
#define TENSORPRODUCTACCUMULATOR_IMPL_H_INCLUDED

#include <numeric>

#include "vcc/TensorProductAccumulator_Decl.h"

#include "vcc/DirProd.h"
#include "tensor/DirProdHelpers.h"
#include "tensor/TensorSum.h"
#include "tensor/TensorDirectProduct.h"
#include "tensor/TensorFits.h"
#include "tensor/Scalar.h"
#include "util/CallStatisticsHandler.h"

#include "libmda/util/scope_guard.h"

namespace midas::vcc
{

/**
 * Reserve elements in vectors.
 * @param aSize       Size to be reserved.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::Reserve
   (  int aSize
   )
{
   mTensors.reserve(aSize);
   mMcs.reserve(aSize);
}

/**
 * Make connection index pairs for given result mode combi.
 * @param aResMc           Mc to make connection for.
 **/
template
   <  typename T
   >
typename TensorProductAccumulator<T>::connection_t TensorProductAccumulator<T>::Connection
   (  const ModeCombi& aResMc
   )  const
{
   connection_t connection;
   connection.reserve(aResMc.Size());
   bool assigned;

   for(int i_res_mode = 0; i_res_mode < aResMc.Size(); ++i_res_mode)
   {
      assigned = false;
      for(int i_mc = 0; i_mc < mMcs.size(); ++i_mc)
      {
         for(int i_mode = 0; i_mode < mMcs[i_mc].Size(); ++i_mode)
         {
            if(mMcs[i_mc].Mode(i_mode) == aResMc.Mode(i_res_mode))
            {
               connection.emplace_back(i_mc, i_mode);
               assigned = true;
               break;
            }
         }
         if(assigned) break;
      }
   }
   return connection;
}

/**
 * Find dimensions of direct product.
 * @param aResMc
 * @return                 Returns the dimensions as a std::vector<unsigned>.
 **/
template
   <  typename T
   >
std::vector<unsigned> TensorProductAccumulator<T>::Dimensions
   (  const ModeCombi& aResMc
   )  const
{
   auto connection = this->Connection(aResMc);
   std::vector<unsigned> dimensions(aResMc.Size());
   for(int i = 0; i < dimensions.size(); ++i)
   {
      dimensions.at(i) = mTensors[connection[i].first].GetDims()[connection[i].second];
   }
   return dimensions;
}


/**
 * Do direct product of all SimpleTensors.
 * @param aResTensor                     Result of direct product.
 * @param aResMc                         Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensorSimpleImpl
   (  NiceTensor<T>& aResTensor
   ,  const ModeCombi& aResMc 
   )
{
   dirprod::DirProdTensorSimple(aResTensor, aResMc, mTensors, mMcs, mCoef);
}

/**
 * Do direct product of all CanonicalTensors.
 * @param aResTensor                     Result of direct product.
 * @param aResMc                         Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensorCanonicalImpl
   (  NiceTensor<T>& aResTensor
   ,  const ModeCombi& aResMc
   ) 
{
   MidasWarning("Calling DirProdTensorCanonicalImpl for NiceTensor. This function is not optimal!");

   //::DirProdTensorCanonical(aResTensor, aResMc, mTensors, mMcs, mCoef);
   // get tensors ready to be moved
   std::vector<std::unique_ptr<BaseTensor<T> > > tensors;
   tensors.reserve(mTensors.size());
   for(int i = 0; i < mTensors.size(); ++i)
   {
      tensors.emplace_back(std::unique_ptr<BaseTensor<T> >(mTensors[i].Release()));
   }
   
   auto dirprod = NiceTensorDirectProduct<T> (  aResTensor.GetDims()
                                             ,  mCoef
                                             ,  std::move(tensors)
                                             ,  this->Connection(aResMc)
                                             );
   
   // check for screening of term
   if(!mProdScreen || (dirprod.Norm() > mProdScreen))
   {
      aResTensor += dirprod;
   }
}

/**
 * Do direct product of all CanonicalTensors.
 * @param aResTensor                     Result of direct product.
 * @param aResMc                         Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensorCanonicalImpl
   (  TensorSumAccumulator<T>& aResTensor
   ,  const ModeCombi& aResMc
   ) 
{
   LOGCALL("begin");

   // get tensors ready to be moved
   std::vector<std::unique_ptr<BaseTensor<T> > > tensors;
   auto size = this->mTensors.size();
   tensors.reserve(size);

   // Get ready to calculate rank and norms
   unsigned rank = 1;
   std::vector<real_t> norm2s(size);

   // Loop through tensors
   for(int i = 0; i < size; ++i)
   {
      rank *= this->mTensors[i].template StaticCast<CanonicalTensor<T> >().GetRank();
      norm2s[i] = this->mTensors[i].Norm2(); // The only norm calculation performed!

      // tensors take ownership of mTensors[i]
      tensors.emplace_back(std::unique_ptr<BaseTensor<T> >(this->mTensors[i].Release()));
   }
   
   // Calculate norm2 of dirprod from norm2s
   const auto norm2 = std::accumulate(norm2s.begin(), norm2s.end(), std::pow(std::abs(mCoef), 2), std::multiplies<real_t>());

   // if rank > 0 and the term is not screened, we add to the sum
   if (  rank > 0
      && (  !this->mProdScreen
         || std::sqrt(norm2) > this->mProdScreen
         )
      )
   {
      // Construct direct product and calculate connections, etc.
      auto dirprod = NiceTensorDirectProduct<T> (  aResTensor.Tensor().GetDims()
                                                ,  mCoef
                                                ,  std::move(tensors)
                                                ,  this->Connection(aResMc)
                                                );


      // Normalize tensors using norm2s
      if (  this->mNormalizeTensors
         )
      {
         dirprod.template StaticCast<TensorDirectProduct<T> >().NormalizeTensors(norm2s);
      }
      // Recompress tensors
      if (  this->mRecompressTensors
         ) 
      {
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            MidasWarning("TensorProductAccumulator: Cannot recompress complex tensors.");
         }
         else
         {
            if (  rank <= this->mLowRankLimit
               )
            {
               LOGCALL("construct mode matrices");
               dirprod = NiceTensor<T>(dirprod.template StaticCast<TensorDirectProduct<T>>().ConstructModeMatrices(this->mBalanceDirprodToCp));
            }
            else
            {
               LOGCALL("recomp");
               dirprod = aResTensor.GetDecomposer().Decompose( dirprod, norm2, -C_1 );
            }
         }
      }

      // Add dirprod to TensorSumAccumulator
      aResTensor += dirprod;
   }
}

/**
 * Do generalized direct product of all CanonicalTensors.
 * @param aResTensor                     Result of direct product.
 * @param aResMc                         Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::GeneralizedDirProdTensorCanonicalImpl
   (  TensorSumAccumulator<T>& aResTensor
   ,  const ModeCombi& aResMc
   )
{
   LOGCALL("calls");

   // Check that we are only performing product of two tensors
   if (  this->mTensors.size() != 2
      )
   {
      MIDASERROR("CP generalized direct products are only implemented for 2 tensors!");
   }

   // Check that all vectors are rank-1
   for(const auto& tens : this->mTensors)
   {
      if (  tens.NDim() == 1
         && tens.template StaticCast<CanonicalTensor<T>>().GetRank() > 1
         )
      {
         MIDASERROR("Vectors should have rank <= 1 before generalized dirprod!");
      }
   }

   // If the MCs are the same, perform Hadamard product (element-wise)
   if (  this->mMcs[0] == this->mMcs[1]
      )
   {
      LOGCALL("same MCs");

      // Niels: If the MCs are not equal to aResMc, we need to implement reordering
      if (  this->mMcs[0] != aResMc
         )
      {
         MIDASERROR("CP generalized direct product for identical MCs: MC != aResMc.");
      }

      auto prod = this->mTensors[0];

      prod *= this->mTensors[1];

      if (  this->mRecompressTensors
         )
      {
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            MidasWarning("TensorProductAccumulator: Cannot recompress complex tensors.");
         }
         else
         {
            LOGCALL("gen dirprod recomp");
            prod = aResTensor.GetDecomposer().Decompose( prod );
         }
      }

      aResTensor += prod;

      return;
   }
   // If one of the tensors is a vector, we can handle more general cases
   else if  (  this->mMcs[0].Size() == I_1
            || this->mMcs[1].Size() == I_1
            )
   {
      LOGCALL("one vector");
      // If first is vector
      if (  this->mMcs[0].Size() == I_1
         )
      {
         auto vec_mode = this->mMcs[0].Mode(0);
         auto idx = this->mMcs[1].IdxNrForMode(vec_mode);

         this->mTensors[1].template StaticCast<CanonicalTensor<T>>().PartialHadamardProduct(idx, this->mTensors[0].template StaticCast<CanonicalTensor<T>>());

         aResTensor += this->mTensors[1];
         return;
      }
      // If second is vector
      else
      {
         auto vec_mode = this->mMcs[1].Mode(0);
         auto idx = this->mMcs[0].IdxNrForMode(vec_mode);

         this->mTensors[0].template StaticCast<CanonicalTensor<T>>().PartialHadamardProduct(idx, this->mTensors[1].template StaticCast<CanonicalTensor<T>>());

         aResTensor += this->mTensors[0];
         return;
      }
   }
   else
   {
      Mout  << " GeneralizedDirProdTensorCanonicalImpl: MCs:\n " 
            << this->mMcs[0] << "\n " 
            << this->mMcs[1] << std::endl;
      MIDASERROR("CP generalized direct product: We need a more general implementation!");
   }
}

/**
 * Do direct product of mixed Simple- and CanonicalTensors.
 * @param aResTensor                 Result of direct product (will in this case be simple).
 * @param aResMc                     Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensorMixedImpl
   (  NiceTensor<T>& aResTensor
   ,  const ModeCombi& aResMc
   ) 
{
   // NB!!! This converts all tensors to SimpleTensor
   dirprod::DirProdTensorCanonical(aResTensor, aResMc, mTensors, mMcs, mCoef); // for now we call Canonical version
}

/**
 * Do direct product of NiceTensors.
 * @param aResTensor        Result of direct product.
 * @param aResMc            Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensor
   (  NiceTensor<T>& aResTensor
   ,  const ModeCombi& aResMc
   ) 
{
   if(AllSimpleTensors(mTensors) && aResTensor.Type() == BaseTensor<T>::typeID::SIMPLE)
   {
      this->DirProdTensorSimpleImpl(aResTensor, aResMc); // call specialized class method
   }
   else if(AllCanonicalTensors(mTensors) && aResTensor.Type() == BaseTensor<T>::typeID::SUM)
   {
      if (  this->mGeneralizedDirProd
         )
      {
         MIDASERROR("Generalized CP dirprod not implemented for NiceTensor!");
      }
      else
      {
         this->DirProdTensorCanonicalImpl(aResTensor, aResMc); // call the class method
      }
   }
   else if(aResTensor.Type() == BaseTensor<T>::typeID::SIMPLE) // not all simple or canonical, for now we convert to all simple and do all simple dirprod
   {
      this->DirProdTensorMixedImpl(aResTensor, aResMc); // for now we call Canonical version
   }
   else
   {
      MIDASERROR("DIRPROD NOT IMPLEMENTED");
   }
}

/**
 * Do direct product of NiceTensors.
 * @param aResTensor        Result of direct product.
 * @param aResMc            Modecombination of result.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProdTensor
   (  TensorSumAccumulator<T>& aResTensor
   ,  const ModeCombi& aResMc
   ) 
{
   if (  AllSimpleTensors(mTensors)
      && aResTensor.Tensor().Type() == BaseTensor<T>::typeID::SIMPLE
      )
   {
      this->DirProdTensorSimpleImpl(aResTensor.Tensor(), aResMc); // call specialized class method
   }
   else if  (  AllCanonicalTensors(mTensors)
            && aResTensor.Tensor().Type() == BaseTensor<T>::typeID::SUM
            )
   {
      if (  this->mGeneralizedDirProd
         )
      {
         this->GeneralizedDirProdTensorCanonicalImpl(aResTensor, aResMc);
      }
      else
      {
         this->DirProdTensorCanonicalImpl(aResTensor, aResMc); // call the class method
      }
   }
   else
   {
      Mout  << "AllCanonicalTensors is " << std::boolalpha << AllCanonicalTensors(mTensors) 
            << " for tensor of type " << aResTensor.Tensor().ShowType()
            << std::endl;
      Mout  << " Types and dims in mTensors:" << std::endl;
      for(const auto& tens : mTensors)
      {
         Mout  << tens.ShowType() << "  " << tens.ShowDims() << std::endl;
      }
      MIDASERROR("DIRPROD NOT IMPLEMENTED");
   }
}

/**
 * Constructor from integer argument, givin max size of direct product.
 * @param aCoef
 * @param aSize                                Size to be reserved.
 * @param aProdScreen
 * @param aScreen
 * @param aLowRankLimit
 * @param aGeneralizedDirProd
 **/
template
   <  typename T
   >
TensorProductAccumulator<T>::TensorProductAccumulator
   (  T aCoef
   ,  int aSize
   ,  real_t aProdScreen
   ,  real_t aScreen
   ,  In aLowRankLimit
   ,  bool aGeneralizedDirProd
   )
   :  mCoef(aCoef)
   ,  mProdScreen(aProdScreen)
   ,  mScreen(aScreen)
   ,  mLowRankLimit(aLowRankLimit)
   ,  mGeneralizedDirProd(aGeneralizedDirProd)
{
   this->Reserve(aSize);
}

/**
 * Adds a tensor to the direct product.
 * @param aTensor       Tensor to add.
 * @param aMc           Mode combination of tensor.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::AddTensor
   (  NiceTensor<T>&& aTensor
   ,  ModeCombi&& aMc
   )
{
   if (aTensor.IsScalar()) // if dim(intermed) == 0 (scalar) put into coef
   {
      mCoef *= aTensor.GetScalar();
   }
   else if  (  aTensor.TotalSize() == 1 
            && aMc.Size() == 0
            ) // if size of intermed is 1 and MC has size 0, we also treat it as a scalar
   {
      T scalar;
      aTensor.GetTensor()->DumpInto(&scalar);
      mCoef *= scalar;
   }
   else // else let rest in vecs and collect later in direct product
   {
      mTensors.emplace_back(std::move(aTensor));
      mMcs.emplace_back(std::move(aMc));
   }
}

/**
 * Do the direct product saving result in tensor.
 * @param aResTensor    Result tensor. Result of direct product is added.
 * @param aResMc        Result modecombination.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProd
   (  NiceTensor<T>& aResTensor
   ,  const ModeCombi& aResMc
   )
{
   // assert that direct product has not been evaluated yet
   MidasAssert(!mDone, "DirProd can only be evaulated once!");
   scope_on_exit{ mDone = true; }; // when scope exits we set mDone to true

   // If mTensors contains a ZeroTensor, nothing will be added to aResTensor
   if  (  ContainsZeroTensor(mTensors)
       )
   {
      return;
   }

   // Test for very simple cases.
   if (  mTensors.size() == 0
      ) // #tensors == 0, we just add the coefficient
   {
      aResTensor.ElementwiseScalarAddition(mCoef);
      return;
   }
   else if  (  mTensors.size() == I_1
            )  // #tensors == 1, multiply the single tensor with coefficient and add.
   {
      mTensors[I_0].Scale(mCoef);
      aResTensor += mTensors[I_0];
      return;
   }

   if (  !aResMc.Size()
      )
   {
      MidasWarning(" DOING STANDARD DIRPROD WITH SIZE OF MP = " + std::to_string(aResMc.Size()));
   }

   // Do direct product...
   this->DirProdTensor(aResTensor, aResMc);
}

/**
 * Do the direct product saving result in tensor.
 * @param aResTensor    Result tensor. Result of direct product is added.
 * @param aResMc        Result modecombination.
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::DirProd
   (  TensorSumAccumulator<T>& aResTensor
   ,  const ModeCombi& aResMc
   )
{
//   LOGCALL("begin");
   // assert that direct product has not been evaluated yet
   MidasAssert(!mDone, "DirProd can only be evaulated once!");
   scope_on_exit{ mDone = true; }; // when scope exits we set mDone to true

   // If mTensors contains a ZeroTensor, nothing will be added to aResTensor
   if  (  ContainsZeroTensor(mTensors)
       )
   {
      return;
   }

   // Test for very simple cases.
   if (mTensors.size() == 0) // #tensors == 0, we just add the coefficient
   { // hmm this will only happen for reference modecombi right (i.e. size 0)?
      aResTensor += NiceScalar<T>(mCoef, aResTensor.Tensor().Type() != BaseTensor<T>::typeID::SIMPLE);
      return;
   }
   else if  (  mTensors.size() == I_1
            )  // #tensors == 1, multiply the single tensor with coefficient and add.
   {
      bool screen_tensor = false;
      if (  mScreen  )
      {
         screen_tensor = mTensors[I_0].Norm()*std::abs(mCoef) < mScreen;
      }

      // if the term is not screened, we add to the sum
      if (  !mScreen
         || !screen_tensor
         )
      {
         mTensors[I_0].Scale(mCoef);
         aResTensor += mTensors[I_0];
      }

      return;
   }

   // Do direct product...
   this->DirProdTensor(aResTensor, aResMc);
}

/**
 * Print dimensions of all tensors and the corresponding MCs
 **/
template
   <  typename T
   >
void TensorProductAccumulator<T>::PrintDimensions
   (
   )  const
{
   auto size = mTensors.size();
   for(In i=I_0; i<size; ++i)
   {
      Mout  << " Tensor dims: " << mTensors[i].ShowDims() << " for MC: " << mMcs[i] << std::endl;
   }
}

} /* namespace midas::vcc */

#endif /* TENSORPRODUCTACCUMULATOR_IMPL_H_INCLUDED */
