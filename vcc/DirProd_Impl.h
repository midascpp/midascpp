/**
 ************************************************************************
 * 
 * @file                DirProd_Impl.h
 *
 * Created:             31-03-2009
 *
 * Author:              Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Implementing general and specialized
 *                      direct product functions.
 * 
 * Last modified: Mon May 11, 2009  04:56PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef DIRPROD_IMPL_H_INCLUDED
#define DIRPROD_IMPL_H_INCLUDED

// std headers
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "DirProd.h"
#include "mmv/MidasVector.h"
#include "vcc/DirProdSpecial.h"
#include "input/ModeCombi.h"
#include "tensor/DirProdHelpers.h"
#include "tensor/SimpleTensor.h"

namespace midas::vcc::dirprod
{

/**
 * Make connection between resulting mode combination and individual
 * modecombinations of direct product.
 * 
 * @param aResMc Resulting modecombination.
 * @param aInMcs Mode combinations of tensors used in Direct product
 * @result       Matrix that for each modecombination gives the index in the resulting mode combination.
 **/
template
   <  class I
   >
std::vector<std::vector<I> >& MakeConnect
   (  const ModeCombi& aResMc
   ,  const std::vector<ModeCombi>& aInMcs
   );

// specialization for type 'In' (used for implementation using GeneralMidasVector<T>)
std::vector<std::vector<In> > global_In_connect;
template<>
std::vector<std::vector<In> >& MakeConnect
   (  const ModeCombi& aResMc
   ,  const std::vector<ModeCombi>& aInMcs
   )
{
   using I = In;
   auto nvecs = aInMcs.size();
   global_In_connect.resize(aInMcs.size());

   for (I i = I_0; i < nvecs; ++i)
   {
      global_In_connect[i].resize(aInMcs[i].Size());
      for (I j = I_0; j < aInMcs[i].Size(); ++j)
      {
         for (I k = I_0; k < aResMc.Size(); ++k)
         {
            if (aInMcs[i].Mode(j) == aResMc.Mode(static_cast<In>(k)))
            {
               global_In_connect[i][j] = k;
               break;
            }
         }
      }
   }

   return global_In_connect;
}

// specialization for type 'unsigned int' (used for implementation using NiceTensor)
std::vector<std::vector<unsigned int> > global_unsigned_connect;
template<>
std::vector<std::vector<unsigned int> >& MakeConnect
   (  const ModeCombi& aResMc
   ,  const std::vector<ModeCombi>& aInMcs
   )
{
   using I = unsigned int;
   auto nvecs = aInMcs.size();
   global_unsigned_connect.resize(aInMcs.size());

   for (I i = I_0; i < nvecs; ++i)
   {
      global_unsigned_connect[i].resize(aInMcs[i].Size());
      for (I j = I_0; j < aInMcs[i].Size(); ++j)
      {
         for (I k = I_0; k < aResMc.Size(); ++k)
         {
            if (aInMcs[i].Mode(j) == aResMc.Mode(static_cast<In>(k)))
            {
               global_unsigned_connect[i][j] = k;
            }
         }
      }
   }

   return global_unsigned_connect;
}

/**
 * Generate direct product of input vector and add result to aRes.
 * If possible, use specialized and efficient routines.
 *
 * @param  aRes:       Result is added to this vector.
 * @param  aResMc:     MC of result vector.
 * @param  aNmodals:   Number of _excited_ modals for each mode in result.
 * @param  aInVecs:    Vector of input MidasVectors.
 * @param  aInMcs:     Vector of MCs for input vectors.
 * @param  aInNmodals: Vector of vectors containing number of modals for each input MC.
 * @param  aCoef:      Coefficient to be multiplied on direct product before addition to aRes.
 **/
template
   <  typename T
   >
void DirProd2
   (  GeneralMidasVector<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<In>& aNmodals
   ,  const std::vector<GeneralMidasVector<T>>& aInVecs
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const std::vector<vector<In> >& aInNmodals
   ,  const T aCoef
   )
{
   if (aInVecs.size() == 2 &&
       DirProd_2vecs(aRes, aResMc, aInVecs[0], aInVecs[1], aInMcs[0], aInMcs[1],
                     aInNmodals[0], aInNmodals[1], aCoef))
   {
      return;
   }
  
   if (aInVecs.size() == 3 &&
       DirProd_3vecs(aRes, aResMc, aInVecs[0], aInVecs[1], aInVecs[2],
                     aInMcs[0], aInMcs[1], aInMcs[2],
                     aInNmodals[0], aInNmodals[1], aInNmodals[2], aCoef))
   {
      return;
   }
   
   // Setup vectors connecting each index in input MCs to index in result MC. 
   auto& connect = MakeConnect<In>(aResMc, aInMcs);
   // then do direct product for the general case
   DirProdGen(aRes, aNmodals, aInVecs, connect, aCoef);
}

/**
 * General function for generating direct product of some input vectors.
 * This is inefficient and last resort.
 *
 * @param aRes           Result is added to aRes.
 * @param aNmodals       Number of modals for each mode in result vector.
 * @param aInVecs        The input vectors.
 * @param aConnect       Connect index in each input MC to index in output MC:
 *                       aConnect[i][j] = Index in result MC for mode j in aInVec[i].
 * @param aCoef          Number multiplied onto result before addition to aRes.
 **/
std::vector<In> global_ivec;
template
   <  typename T
   >
void DirProdGen
   (  GeneralMidasVector<T>& aRes
   ,  const vector<In>& aNmodals
   ,  const vector<GeneralMidasVector<T>>& aInVecs
   ,  const vector<vector<In> >& aConnect
   ,  const T aCoef
   )
{
   const In res_size = aRes.Size();
   global_ivec.resize(aNmodals.size());
   for(int i = 0; i < global_ivec.size(); ++i)
   {
      global_ivec[i] = static_cast<In>(0);
   }

   In i_res = I_0;
   while(true)
   {
      T res = C_1;
      for (In i = I_0; i < aInVecs.size(); ++i)
      {
         In addr = global_ivec[aConnect[i][I_0]];
         for (In k=I_1; k<aConnect[i].size(); ++k)
         {
            addr = addr*aNmodals[aConnect[i][k]] + global_ivec[aConnect[i][k]];
         }
         res *= aInVecs[i][addr];
      }
      aRes[i_res] += aCoef * res;

      if (++i_res < res_size)
      {
         IncIvec(global_ivec, aNmodals);
      }
      else
      {
         break;
      }
   }
}

/**
 * 
 **/
template
   <  typename T
   >
bool DirProd_2vecs
   (  GeneralMidasVector<T>& aRes
   ,  const ModeCombi& aMcRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const ModeCombi& aMc1
   ,  const ModeCombi& aMc2
   ,  const vector<In>& aNmodals1
   ,  const vector<In>& aNmodals2
   ,  const T aCoef
   )
{
   In nm1 = aMc1.Size();
   In nm2 = aMc2.Size();

   if (nm1==I_1 && nm2==I_1)                                 //1-dim x 1-dim
   {
      if (aMc1.Mode(I_0) < aMc2.Mode(I_0))                   //     (m0) x (m1)
         DirProd_vec(aRes, aInVec1, aInVec2, aCoef);
      else                                                   //     (m1) x (m0)
         DirProd_vec(aRes, aInVec2, aInVec1, aCoef);
   }
   else if (nm1==I_1 && nm2==I_2)                            // 1-dim x 2-dim
      if (aMc1.Mode(I_0) < aMc2.Mode(I_0))                   //    (m0) x (m1,m2)
         DirProd_vec(aRes, aInVec1, aInVec2, aCoef);
      else if (aMc1.Mode(I_0) < aMc2.Mode(I_1))              //    (m1) x (m0,m2)
         DirProd_1m_2m_aba(aRes, aInVec1, aInVec2, aNmodals1, aNmodals2, aCoef);
      else                                                   //    (m2) x (m0,m1)
         DirProd_vec(aRes, aInVec2, aInVec1, aCoef);
   else if (nm1==I_2 && nm2==I_1)                            // 2-dim x 1-dim
      if (aMc2.Mode(I_0) < aMc1.Mode(I_0))                   //    (m1,m2) x (m0)
         DirProd_vec(aRes, aInVec2, aInVec1, aCoef);
      else if (aMc2.Mode(I_0) < aMc1.Mode(I_1))              //    (m0,m2) x (m1)
         DirProd_1m_2m_aba(aRes, aInVec2, aInVec1, aNmodals2, aNmodals1, aCoef);
      else                                                   //    (m0,m1) x (m2)
         DirProd_vec(aRes, aInVec1, aInVec2, aCoef);
   else
      return false;
   
   return true;
}

/**
 * 
 **/
template
   <  typename T
   >
bool DirProd_3vecs
   (  GeneralMidasVector<T>& aRes
   ,  const ModeCombi& aMcRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const GeneralMidasVector<T>& aInVec3
   ,  const ModeCombi& aMc1
   ,  const ModeCombi& aMc2
   ,  const ModeCombi& aMc3
   ,  const vector<In>& aNmodals1
   ,  const vector<In>& aNmodals2
   ,  const vector<In>& aNmodals3
   ,  const T aCoef
   )
{
   In nm1 = aMc1.Size();
   In nm2 = aMc2.Size();
   In nm3 = aMc3.Size();

   if (nm1==I_1 && nm2==I_1 && nm3==I_1)
   {
      In m1=aMc1.Mode(I_0);
      In m2=aMc2.Mode(I_0);
      In m3=aMc3.Mode(I_0);
      if (m1<m2 && m2<m3)
         DirProd_vec(aRes, aInVec1, aInVec2, aInVec3, aCoef);
      else if (m1<m3 && m3<m2)
         DirProd_vec(aRes, aInVec1, aInVec3, aInVec2, aCoef);
      else if (m2<m1 && m1<m3)
         DirProd_vec(aRes, aInVec2, aInVec1, aInVec3, aCoef);
      else if (m2<m3 && m3<m1)
         DirProd_vec(aRes, aInVec2, aInVec3, aInVec1, aCoef);
      else if (m3<m1 && m1<m2)
         DirProd_vec(aRes, aInVec3, aInVec1, aInVec2, aCoef);
      else if (m3<m2 && m2<m1)
         DirProd_vec(aRes, aInVec3, aInVec2, aInVec1, aCoef);
   }
   else
      return false;

   return true;
}

/**
 * 
 **/
template
   <  typename T
   >
void DirProd_vec
   (  GeneralMidasVector<T>& aRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const T aCoef
   )
{
   In sz1 = aInVec1.Size();
   In sz2 = aInVec2.Size();
   for (In i=I_0; i<sz1; ++i)
   {
      T vec1coef = aInVec1[i];
      for (In k=I_0; k<sz2; ++k)
      {
         aRes[i*sz2+k] += aCoef * vec1coef * aInVec2[k];
      }
   }
}

/**
 * 
 **/
template
   <  typename T
   >
void DirProd_vec
   (  GeneralMidasVector<T>& aRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const GeneralMidasVector<T>& aInVec3
   ,  const T aCoef
   )
{
   In sz1 = aInVec1.Size();
   In sz2 = aInVec2.Size();
   In sz3 = aInVec3.Size();

   In mult23 = sz2*sz3;
   
   for (In i=I_0; i<sz1; ++i)
      for (In k=I_0; k<sz2; ++k)
         for (In m=I_0; m<sz3; ++m)
            aRes[i*mult23 + k*sz3 + m] += aCoef * aInVec1[i] * aInVec2[k] * aInVec3[m];
}

/**
 * 
 **/
template
   <  typename T
   >
void DirProd_1m_2m_aba
   (  GeneralMidasVector<T>& aRes
   ,  const GeneralMidasVector<T>& aInVec1
   ,  const GeneralMidasVector<T>& aInVec2
   ,  const vector<In>& aNmodals1
   ,  const vector<In>& aNmodals2
   ,  const T aCoef
   )
{
   In mult1 = aNmodals2[1];
   In mult2 = aNmodals1[0] * aNmodals2[1];

   for (In i=I_0; i<aNmodals2[0]; ++i)
      for (In k=I_0; k<aNmodals1[0]; ++k)
      {
         T vec1coef = aInVec1[k];
         for (In m=I_0; m<aNmodals2[1]; ++m)
            aRes[i*mult2 + k*mult1 + m] += aCoef * vec1coef * aInVec2[i*mult1 + m];
      }
}

/**
 * General implementation of direct product for SimpleTensor%s.
 *
 * @param aRes                        Result of direct product.
 * @param aResMc                      Modecombination of result.
 * @param aInTensor                   Vector of tensors to be direct product'ed together.
 * @param aInMcs                      Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef                       Coefficient to be multiplied to each element of result.
 **/
namespace detail
{
template
   <  typename T
   >
std::vector<T*>& GetGlobalOtherPtrs
   (
   )
{
   static std::vector<T*> global_other_ptrs;

   return global_other_ptrs;
}
std::vector<unsigned> global_res_index;
} /* namespace detail */
template
   <  typename T
   >
void DirProdTensorSimpleGen
   (  NiceTensor<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   )
{
   // make connection map
   auto& connect = MakeConnect<unsigned int>(aResMc, aInMcs);

   // make vector of ptrs (to directly access SimpleTensor data)
   detail::GetGlobalOtherPtrs<T>().resize(aInTensor.size());
   for(int i = 0; i < aInTensor.size(); ++i)
   {
      detail::GetGlobalOtherPtrs<T>()[i] = static_cast<SimpleTensor<T>* >(aInTensor[i].GetTensor())->GetData();
   }
   auto ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   
   // set index size and init to zero
   detail::global_res_index.resize(aRes.NDim());
   for(int i = 0; i < detail::global_res_index.size(); ++i)
   {
      detail::global_res_index[i] = 0;
   }

   // do direct product
   int size = aRes.TotalSize();
   for(int i = 0; i < size; ++i)
   {
      // calculate result for current index
      auto res = static_cast<T>(1.0);
      for(int other_idx = 0; other_idx < aInTensor.size(); ++other_idx)
      {
         auto addr = detail::global_res_index[connect[other_idx][0]];
         auto addr_size = connect[other_idx].size();
         for(int addr_idx = 1; addr_idx < addr_size; ++addr_idx)
         {
            auto connection = connect[other_idx][addr_idx];
            addr *= aRes.GetDims()[connection];
            addr += detail::global_res_index[connection];
         }
         res *= detail::GetGlobalOtherPtrs<T>()[other_idx][addr];
      }
      *(ptr++) += aCoef * res; // then add result
      IncIvec(detail::global_res_index, aRes.GetDims());
   }
}

/**
 * Do direct product of all SimpleTensor%s.
 *
 * @param arRes                    Result of direct product.
 * @param aResMc                   Modecombination of result.
 * @param aInTensor                Vector of tensors to be direct product'ed together.
 * @param aInMcs                   Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef                    Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorSimple
   (  NiceTensor<T>& arRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   )
{
   // special cases
   if (  DirProdTensorSimpleSpecial(arRes, aResMc, aInTensor, aInMcs, aCoef)
      )
   {
      return; // if we hit a special case we return now
   }
   if (  DirProdTensorSimpleSpecialMixed(arRes, aResMc, aInTensor, aInMcs, aCoef)
      )
   {
      return; // if we hit a special case we return now
   }

   // if not any of the special cases we do General case
   DirProdTensorSimpleGen(arRes, aResMc, aInTensor, aInMcs, aCoef);
}


/**
 * Do direct product of all CanonicalTensor%s.
 *
 * @param arRes     Result of direct product.
 * @param aResMc    Modecombination of result.
 * @param aInTensor Vector of tensors to be direct product'ed together.
 * @param aInMcs    Vector of modecombinations corresponding to tensors in aInTensors.
 * @param aCoef     Coefficient to be multiplied to each element of result.
 **/
template
   <  typename T
   >
void DirProdTensorCanonical
   (  NiceTensor<T>& arRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   )
{
   std::vector<NiceTensor<T> > simple(aInTensor.size());
   for(int i = 0; i < simple.size(); ++i)
   {
      simple[i] = aInTensor[i].ToSimpleTensor();
   }
   DirProdTensorSimple(arRes, aResMc, simple, aInMcs, aCoef);
}

} /* namespace midas::vcc::dirprod */

#endif /* DIRPROD_IMPL_H_INCLUDED */
