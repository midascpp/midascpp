/**
************************************************************************
* 
* @file                LanczosLinRspFunc.cc
*
* Created:             10-08-2009
*
* Author:              Peter Seidler  (seidler@chem.au.dk)
*                      Werner Gyorffy (werner@chem.au.dk)
*
* Short Description:   Implementation of LanczosLinRspFunc class.
* 
* Last modified: Thu Nov 12, 2009  03:55PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <map>
#include <complex>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "input/Input.h"
#include "vcc/LanczosChain.h"
#include "vcc/NHermLanczosChain.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosLinRspFunc.h"
#include "mmv/MidasMatrix.h"
#include "mmv/Diag.h"

using namespace std;

LanczosLinRspFunc::LanczosLinRspFunc(const std::map<string,string>& aParams,
                                     const bool aBand, const bool aNHerm):
   LanczosRspFunc(aParams,"LanczosLinRspFunc", aBand, aNHerm), mImInclude(true), mTHasImEigVal(false)
{
   if(gIoLevel > 5)
   {
      Mout << " LanczosLinRspFunc::LanczosLinRspFunc()" << endl
           << "    Parameters:" << endl;

      for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
         Mout << "    " << it->first << " -> " << it->second << endl;
   }

   InitOperNames(aParams);
   InitFrqRange(aParams);
   InitGamma(aParams);
   InitChainLen(aParams);
   InitOrtho(aParams);
   InitSolver(aParams);
   InitImStatus(aParams);
}

void LanczosLinRspFunc::InitImStatus(const std::map<string,string>& aParams)
{
   // Search for ImStatus parameter.
   map<string,string>::const_iterator it = aParams.find("INCLUDEIM");
   if (it != aParams.end())
   {
      // Not 100% fool proof. You may specify a string for any constant
      if(it->second == "TRUE")
      {
         mImInclude = true;
      }
      else if(it->second == "FALSE")
      {
         mImInclude = false;
      }
      else
      {
         MIDASERROR("Unknown setting for INCLUDEIM!");
      }
   }
}

void LanczosLinRspFunc::ConstructionError
   (  const std::string aMsg
   ,  const std::map<string,string>& aParams
   )
{
   Mout << " LanczosLinRspFunc construction error:" << endl
        << aMsg << endl << endl
        << " Constructor parameters:" << endl;
   
   for (map<string,string>::const_iterator it=aParams.begin(); it!=aParams.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;

   MIDASERROR("");
}

void LanczosLinRspFunc::InitOperNames(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("OPERS");
   if (it == aParams.end())
      ConstructionError(" No operators specified.", aParams);
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Operators must be enclosed in (...).", aParams);
   string opstr = s.substr(1,s.size()-2);
   vector<string> ops = SplitString(opstr, ",");
   if (ops.size() != 2)
      ConstructionError(" Exactly two operators must be specified.", aParams);
   mOper1 = ops[0];
   mOper2 = ops[1];
}

void LanczosLinRspFunc::InitFrqRange(const map<string,string>& aParams)
{
   // If FRQ is specified just initialize a single frequency value.
   map<string,string>::const_iterator it = aParams.find("FRQ");
   if (it != aParams.end())
   {
      istringstream is(it->second);
      is >> mFrqStart;
      mFrqStart /= C_AUTKAYS;
      mFrqEnd = mFrqStart;
      mFrqStep = C_0;
      return;
   }

   // If FRQ_RANGE is specified initialize a range.
   it = aParams.find("FRQ_RANGE");
   if(it == aParams.end())
   {
      if(gIoLevel > 5)
      {
         Mout  << " No FRQ or FRQ_RANGE keyword set, so I'm using default values: " << endl
               << "     FrqStart = 0 cm^{-1}" << endl
               << "     FrqEnd   = 4500 cm^{-1} " << endl
               << "     FrqStep  = 1 cm^{-1}" << endl;
      }

      mFrqStart = C_0/C_AUTKAYS;
      mFrqEnd = 4500/C_AUTKAYS;
      mFrqStep = C_1/C_AUTKAYS;
      return;
   }
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Frequency range must be enclosed in (...).", aParams);
   string frqstr = s.substr(1,s.size()-2);
   vector<string> frqs = SplitString(frqstr, ":");
   if (frqs.size() != 3)
      ConstructionError(" Frequency range must be secified as (start:end:step).", aParams);
   istringstream is(frqs[0]);
   is >> mFrqStart;
   is.clear();
   is.str(frqs[1]);
   is >> mFrqEnd;
   is.clear();
   is.str(frqs[2]);
   is >> mFrqStep;

   mFrqStart /= C_AUTKAYS;
   mFrqEnd /= C_AUTKAYS;
   mFrqStep /= C_AUTKAYS;
}

void LanczosLinRspFunc::InitGamma(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("GAMMA");
   if (it == aParams.end())
   {
      if(gIoLevel > 5)
      {
         Mout  << " No GAMMA keyword set, so I'm using default values: " << endl
               << "     Gamma = 10 cm^{-1}" << endl;
      }

      mGamma = C_10/C_AUTKAYS;
      return;
   }
   istringstream is(it->second);
   is >> mGamma;
   mGamma /= C_AUTKAYS;
}

void LanczosLinRspFunc::InitChainLen(const map<string,string>& aParams)
{
   map<string,string>::const_iterator it = aParams.find("CHAINLEN");
   if (it == aParams.end())           
      ConstructionError(" CHAINLEN not specified", aParams);   
   istringstream is(it->second);
   is >> mChainLen;
}

void LanczosLinRspFunc::InitOrtho(const map<string,string>& aParams)
{
   ///> Search for orthogonalization parameter.
   map<string,string>::const_iterator it = aParams.find("ORTHO");
   if(it == aParams.end())
   {
      if(gIoLevel > 5)
      {
         Mout << " No ORTHO keyword set, so I'm using default values: " << endl
              << "      Ortho = \"FULL\"     [1]" << endl;
      }

      mOrthoChain = 1;
      return;
   }
   if (it != aParams.end())
   {
      string s = "ORTHO_" + it->second;
      
      if (ConstForString(s) == LANCZOS_ERROR)
         ConstructionError(" Unknown ORTHO parameter.", aParams);
      
      mOrthoChain = LanczosRspFunc::ConstForString(s);
   }
}

void LanczosLinRspFunc::InitSolver(const map<string,string>& aParams)
{
   ///> Search for solver parameter.
   map<string,string>::const_iterator it = aParams.find("SOLVER");
   if(it == aParams.end())
   {
      if(gIoLevel > 5)
      {
         Mout << " No SOLVER keyword set, so I'm using default values: " << endl
              << "      Solver = \"LINEAR\"     [16]" << endl;
      }

      mSolver = 16;
      return;
   }

   if (it != aParams.end())
   {
      string s = "SOLVER_" + it->second;
      
      if (ConstForString(s) == LANCZOS_ERROR)
         ConstructionError(" Unknown SOLVER parameter.", aParams);
      
      mSolver = LanczosRspFunc::ConstForString(s);
   }
}

vector<LanczosChainDefBase*> LanczosLinRspFunc::GetChains(string& aString) const
{
   if(mNHerm)
      aString = "lan_nh";
   else
      aString = "lan";

   vector<LanczosChainDefBase*> aDefs;
   aDefs.push_back(LanczosChainDefBase::Factory(mNHerm, mOper2, mChainLen, mOrthoChain));
   aDefs.back()->AddXoper(mOper1);
   
   return aDefs;
}

void LanczosLinRspFunc::Evaluate()
{
   if(!mNHerm)
   {
      Mout << " Evaluating: " << endl << *this;
      // Get chain to use.
      LanczosChainDef chaindef(mOper2, mChainLen, mOrthoChain);
      chaindef.AddXoper(mOper1);
      const LanczosChain& chain = *FindChain(chaindef);
      Mout << " Using:" << endl << chain;
      
      switch (mSolver)
      {
         case LANCZOS_SOLVER_LIN:
            VciRspFctLin(chain);
            break;
         case LANCZOS_SOLVER_EIG:
            VciRspFctEig(chain);
            break;
         case LANCZOS_SOLVER_CF:
            VciRspFctCf(chain);
            break;
         default:
            MIDASERROR("LanczosLinRspFunc::Evaluate(): Unknown mSolver.");
      }

      WriteDataToFile();
      Mout << endl;
   }
   if(mNHerm)
   {
      Mout << " Evaluating: " << endl << *this;
      // Get chain to use.
      NHermLanczosChainDef chaindef(mOper2, mChainLen, mOrthoChain);
      chaindef.AddXoper(mOper1);
      const NHermLanczosChain& chain = *FindChainNH(chaindef);
      
      /*switch (mSolver)
      {
         case LANCZOS_SOLVER_LIN:
            VciRspFctLin(chain);
            break;
         case LANCZOS_SOLVER_EIG:*/
            VccRspFctEig(chain);
         /*   break;
         case LANCZOS_SOLVER_CF:
            VciRspFctCf(chain);
            break;
         default:
            MIDASERROR("LanczosLinRspFunc::Evaluate(): Unknown mSolver.");
      }*/
      WriteDataToFile();
      Mout << endl; 
   }
}

void LanczosLinRspFunc::VciRspFctLin(const LanczosChain& aChain)
{
   // Vectors used to store chain.
   MidasVector alpha(mChainLen, C_0);
   MidasVector beta(mChainLen-I_1, C_0);
   MidasVector qx(mChainLen, C_0);                    // q vectors dotted on appropriate eta.
   Nb eta_norm = aChain.GetEtaNorm();                 // Norm of eta used as q(0).
   Nb xeta_norm = aChain.GetXetaNorm(mOper1);         // Norm of eta dotted on succesive q(j)'s.
   aChain.GetAlphaRange(alpha, I_0, mChainLen);
   aChain.GetBetaRange(beta, I_0, mChainLen-I_1);
   aChain.GetXRange(qx, I_0, mChainLen, mOper1);
   // Vectors used for solving linear equations.
   MidasVector e(mChainLen, C_0);          // Unit vector.
   MidasVector yre(mChainLen);             // Solution vector for (T-wI)*y = e.
   MidasVector yim(mChainLen);             // Solution vector for (T-wI)*y = e.
   e[I_0] = C_1;

   // Calculate response function in specified frequency interval.
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;
   mReVals.SetNewSize(n_frq);
   mImVals.SetNewSize(n_frq);
   for (In i=I_0; i<n_frq; ++i)
   {
      Nb frq = mFrqStart + i*mFrqStep;
      TriDiagLinEqSol(alpha, beta, beta, e, yre, yim, frq, mGamma);
      mReVals[i] = xeta_norm*eta_norm * Dot(qx,yre);
      mImVals[i] = xeta_norm*eta_norm * Dot(qx,yim);
      TriDiagLinEqSol(alpha, beta, beta, e, yre, yim, -frq, -mGamma);
      mReVals[i] += xeta_norm*eta_norm * Dot(qx,yre);
      mImVals[i] += xeta_norm*eta_norm * Dot(qx,yim);
   }

   std::ofstream response_file("lan_rsp",std::ofstream::app);
   PrintResponseToStream(response_file);
}

void LanczosLinRspFunc::WriteMatrix(const NHermLanczosChain& aChain)
{
   DataCont ToWriteMatrix;
   ToWriteMatrix.SetNewSize(mChainLen*mChainLen);
   ostringstream namematrix;
   namematrix << aChain.GetFilePrefix() << "_FQQmat";
   ToWriteMatrix.NewLabel(namematrix.str());
   ToWriteMatrix.SaveUponDecon();
   ToWriteMatrix.DataIo(IO_PUT, Fqq, mChainLen*mChainLen, mChainLen, mChainLen);
}

void LanczosLinRspFunc::MakeTransformsForVcc(MidasMatrix& aReFQSTransMatrix, MidasMatrix& aImFQSTransMatrix, 
                                             MidasMatrix& aSetOfRightEVecs, MidasVector& aSetOfImEigVals, 
                                             const NHermLanczosChain& aChain)
{
   Fqq.SetNewSize(mChainLen,mChainLen);
   ostringstream namemult;
   namemult << mpVccCalcDef->GetName() << "_mul0_vec_0";
   DataCont Multi;
   Multi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, namemult.str());
   if (! Multi.ChangeStorageTo("InMem",true, false, true))
         MIDASERROR("Error reading Multi in transformer creation step. ");
   bool FoundVector = true, FirstPass = true;
   In StartingToCalcFTrans = I_0;
   time_t t0 = time(NULL);
   In FirstItInFQQCalc = I_0;
   /*In FirstItInFQQCalc = aChain.GetRestartLen();
   if(aChain.GetRestartLen() != I_0)
   {
      DataCont GetFQQ;
      ostringstream temp;
      temp << aChain.GetFilePrefix() << "_FQQmat";
      GetFQQ.GetFromExistingOnDisc(aChain.GetRestartLen()*aChain.GetRestartLen(), temp.str());
      if (! GetFQQ.ChangeStorageTo("InMem",true))
      {
         FirstItInFQQCalc = I_0;
         Mout << "Error in restarting from previous FQQ matrix starting calculation from scratch!!" << endl;
      }
      else
      {
         GetFQQ.DataIo(IO_GET, Fqq);
      }
   }*/
   for(In j = I_0;j < mChainLen;j++)
   {
      ostringstream temp;
      temp << aChain.GetFilePrefix() << "_FQQmat" << j;
      DataCont GetFQQ;
      GetFQQ.GetFromExistingOnDisc(j+I_1, temp.str());
      if(! GetFQQ.ChangeStorageTo("InMem",true, false, true))
      {
         Mout << "Starting calculation of FQQ matrix from itt. " << j << endl;
         FirstItInFQQCalc = j;
         break;
      }
      else
      {
         for(In i = I_0; i <= j; i++)
         {
            Fqq[j][i] = (*GetFQQ.GetVector())[i];
            Fqq[i][j] = (*GetFQQ.GetVector())[i];
         }
      }
      if(j == mChainLen - I_1)
      {
         FirstItInFQQCalc = mChainLen;
      }
   }
   for(In j = FirstItInFQQCalc;j < mChainLen;j++)
   {
      MidasVector StorageForRestart;
      StorageForRestart.SetNewSize(j+I_1);
      ostringstream temp;
      temp << aChain.GetFilePrefix() << "_q" << j;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, temp.str());
      if (! q.ChangeStorageTo("InMem",true, false, true))
            MIDASERROR("Error reading q(j) in insertion step. ");
      for(In i = I_0; i <= j; i++)
      {
         ostringstream temp2;
         temp2 << aChain.GetFilePrefix() << "_q" << i << "_Fq";
         DataCont Fq;
         if(FirstPass)
            t0 = time(NULL);
         if(i == j)
         {
            Fq.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, temp2.str());
            if(!(Fq.ChangeStorageTo("InMem",true, false, true)))
            {
               Mout << "calculating FQ vector" << endl;
               Fq.SetNewSize(mpTrf->NexciXvec()-I_1);
               Fq.Zero();
               mpTrf->CalcVccRspFR(Multi,q,Fq);
               Fq.NewLabel(temp2.str());
               Fq.ChangeStorageTo("OnDisc");
               Fq.SaveUponDecon(true);
            }
         }
         else
         {  
            Fq.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, temp2.str());
            if(!(Fq.ChangeStorageTo("InMem",true, false, true)))
               MIDASERROR("Error in reading calculated Fq vectors ");
         }
         StorageForRestart[i] = Dot(Fq,q);
         Fqq[j][i] = Dot(Fq,q);
         Fqq[i][j] = Dot(Fq,q);
      }
      ostringstream temp3;
      temp3 << aChain.GetFilePrefix() << "_FQQmat" << j;
      DataCont SaveToDisk(StorageForRestart, "InMem", temp3.str(),true);
      if(FirstPass)
      {
         FirstPass = false;
         StartingToCalcFTrans = j;
         Mout << " Required iterations: " << mChainLen-j << endl
              << " First iteration: " << time(NULL)-t0 << " s"
              << endl << " Progress (one . is 10 iterations):" << endl << " ";
      }
      else if((j-StartingToCalcFTrans)%10 == I_0)
      {
         Mout << "." << std::flush;
      }
   }
   
   WriteMatrix(aChain);

   mEigVecs.SetNewSize(mChainLen,mChainLen,false,false);
   if(!mImInclude || !mTHasImEigVal)
   {
      aReFQSTransMatrix.SetNewSize(mChainLen,mChainLen,false,false);
      aReFQSTransMatrix = Transpose(aSetOfRightEVecs)*Fqq*aSetOfRightEVecs;
      mEigVecs = aSetOfRightEVecs;
   }
   else
   {
      Mout << "INCLUDING IM CONTRIBS! Now making transforms" << endl;
      MidasMatrix Imrighteigvecs(mChainLen);
      Imrighteigvecs.Zero();
      for(In i = I_0;i < mChainLen;i++)
      {
         if(aSetOfImEigVals[i] != C_0)
         {
            MidasVector tempstorage(mChainLen),tempstorage2(mChainLen);
            aSetOfRightEVecs.GetCol(tempstorage,i+I_1);
            Imrighteigvecs.AssignCol(tempstorage,i);
            tempstorage = tempstorage*-C_1;
            Imrighteigvecs.AssignCol(tempstorage,i+I_1);
            aSetOfRightEVecs.GetCol(tempstorage2,i);
            aSetOfRightEVecs.AssignCol(tempstorage2,i+I_1);
            i++;
         }
      }
      mEigVecs = aSetOfRightEVecs;
      aReFQSTransMatrix.SetNewSize(mChainLen,mChainLen,false,false);
      aImFQSTransMatrix.SetNewSize(mChainLen,mChainLen,false,false);
      aReFQSTransMatrix = Transpose(aSetOfRightEVecs)*Fqq*aSetOfRightEVecs + Transpose(Imrighteigvecs)*Fqq*Imrighteigvecs;
      aImFQSTransMatrix = Transpose(Imrighteigvecs)*Fqq*aSetOfRightEVecs + Transpose(aSetOfRightEVecs)*Fqq*Imrighteigvecs;
   }
}

/*
Biorthogonalize eigenvectors, if the vector is a set of complex vectors only orthogonalize the first(Since this takes care of the second as well).
Note that the left eigenvectors are already complex conjugated, hence the signs on the orthogonalization step.
*/
void LanczosLinRspFunc::BiorthoEigenVecs(MidasMatrix& aSetOfLeftEigenVecs, MidasMatrix& aSetOfRightEigenVecs,MidasVector& ImEigs)
{
   MidasVector LeftVector(aSetOfLeftEigenVecs.Nrows(),C_0), RightVector(aSetOfLeftEigenVecs.Nrows(),C_0);
   MidasVector ImLeftVector(aSetOfLeftEigenVecs.Nrows(),C_0), ImRightVector(aSetOfLeftEigenVecs.Nrows(),C_0);
   for(In i = I_0; i < aSetOfLeftEigenVecs.Nrows();i++)
   {
      if(ImEigs[i] == C_0)
      {
         LeftVector.Zero();
         RightVector.Zero();
         aSetOfLeftEigenVecs.GetCol(LeftVector,i);
         aSetOfRightEigenVecs.GetCol(RightVector,i);
         aSetOfLeftEigenVecs.ScaleCol(C_1/Dot(LeftVector,RightVector),i);
         LeftVector.Zero();
         aSetOfLeftEigenVecs.GetCol(LeftVector,i);
         //Mout << "Dot product between new vectors : " << Dot(LeftVector,RightVector) << endl;
      }
      else
      {
         if(mImInclude)
         {
            LeftVector.Zero();
            RightVector.Zero();
            ImLeftVector.Zero();
            ImRightVector.Zero();
            aSetOfLeftEigenVecs.GetCol(LeftVector,i);
            aSetOfRightEigenVecs.GetCol(RightVector,i);
            aSetOfLeftEigenVecs.GetCol(ImLeftVector,i+I_1);
            aSetOfRightEigenVecs.GetCol(ImRightVector,i+I_1);
            Nb Cr =Dot(LeftVector,RightVector)-Dot(ImLeftVector,ImRightVector);
            Nb Ci =Dot(RightVector,ImLeftVector)+Dot(LeftVector,ImRightVector);
            Nb Cnormsq = Cr*Cr+Ci*Ci;
            Mout << "Eigvecnr " << i << ": Cr = " << Cr << " Ci = " << Ci << " Cnormsq = " << Cnormsq << endl;
            MidasVector NewReVec = Cr*LeftVector+Ci*ImLeftVector;
            NewReVec.Scale(C_1/Cnormsq);
            MidasVector NewImVec = Cr*ImLeftVector-Ci*LeftVector;
            NewImVec.Scale(C_1/Cnormsq);
            aSetOfLeftEigenVecs.AssignCol(NewReVec,i);
            aSetOfLeftEigenVecs.AssignCol(NewImVec,i+I_1);
            Mout << "Dot product between new vectors : " << Dot(NewReVec,RightVector)-Dot(NewImVec,ImRightVector) << " + " << Dot(NewImVec,RightVector)+Dot(ImRightVector,NewReVec) << "*i" << endl;
         }
         else
         {
            LeftVector.Zero();
            RightVector.Zero();
            aSetOfLeftEigenVecs.GetCol(LeftVector,i);
            aSetOfRightEigenVecs.GetCol(RightVector,i);
            aSetOfLeftEigenVecs.ScaleCol(C_1/Dot(LeftVector,RightVector),i);
            LeftVector.Zero();
            aSetOfLeftEigenVecs.GetCol(LeftVector,i);
            aSetOfLeftEigenVecs.AssignCol(LeftVector,i+I_1);
            aSetOfRightEigenVecs.AssignCol(RightVector,i+I_1);
         }
         i++;
      }
   }
}

void LanczosLinRspFunc::TestVccEigvectors(MidasMatrix& Tmatrix, MidasMatrix& LeftEigVecs, MidasMatrix& RightEigVecs, 
                                          MidasVector& ReEigvals, MidasVector ImEigvals)
{
   MidasVector TempEigVecSto(LeftEigVecs.Nrows(),C_0), TempTransEigVecSto(LeftEigVecs.Nrows(),C_0);
   for(In i = I_0; i < LeftEigVecs.Nrows();i++)
   {
      if(ImEigvals[i] == C_0)
      {
         RightEigVecs.GetCol(TempEigVecSto, i);
         RightEigVecs.GetCol(TempTransEigVecSto, i);
         TempTransEigVecSto.MultiplyWithMatrix(Tmatrix);
         TempTransEigVecSto-=ReEigvals[i]*TempEigVecSto;
         Mout << "Residual from " << i << "th right eigenvector : " << TempTransEigVecSto.Norm() << endl;
         LeftEigVecs.GetCol(TempEigVecSto, i);
         LeftEigVecs.GetCol(TempTransEigVecSto, i);
         TempTransEigVecSto.LeftMultiplyWithMatrix(Tmatrix);
         TempTransEigVecSto-=ReEigvals[i]*TempEigVecSto;
         Mout << "Residual from " << i << "th left eigenvector : " << TempTransEigVecSto.Norm() << endl; 
      }  
      else
      {
         MidasVector ImTempEigVecSto(LeftEigVecs.Nrows(),C_0), ImTempTransEigVecSto(LeftEigVecs.Nrows(),C_0);
         RightEigVecs.GetCol(TempEigVecSto, i);
         RightEigVecs.GetCol(TempTransEigVecSto, i);
         RightEigVecs.GetCol(ImTempEigVecSto, i+I_1);
         RightEigVecs.GetCol(ImTempTransEigVecSto, i+I_1);
         TempTransEigVecSto.MultiplyWithMatrix(Tmatrix);
         ImTempTransEigVecSto.MultiplyWithMatrix(Tmatrix);
         TempTransEigVecSto-=ReEigvals[i]*TempEigVecSto;
         TempTransEigVecSto+=ImEigvals[i]*ImTempEigVecSto;
         ImTempTransEigVecSto-=ImEigvals[i]*TempEigVecSto;
         ImTempTransEigVecSto-=ReEigvals[i]*ImTempEigVecSto;
         Mout << "Residual from " << i << "th and " << i+I_1 << "th  right eigenvector : " << 
                  TempTransEigVecSto.Norm()+ImTempTransEigVecSto.Norm() << endl;
         LeftEigVecs.GetCol(TempEigVecSto, i);
         LeftEigVecs.GetCol(TempTransEigVecSto, i);
         LeftEigVecs.GetCol(ImTempEigVecSto, i+I_1);
         LeftEigVecs.GetCol(ImTempTransEigVecSto, i+I_1);
         TempTransEigVecSto.LeftMultiplyWithMatrix(Tmatrix);
         ImTempTransEigVecSto.LeftMultiplyWithMatrix(Tmatrix);
         TempTransEigVecSto-=ReEigvals[i]*TempEigVecSto;
         TempTransEigVecSto+=ImEigvals[i]*ImTempEigVecSto;
         ImTempTransEigVecSto-=ImEigvals[i]*TempEigVecSto;
         ImTempTransEigVecSto-=ReEigvals[i]*ImTempEigVecSto;
         Mout << "Residual from " << i << "th and " << i+I_1 << "th left eigenvector : " << 
               TempTransEigVecSto.Norm()+ImTempTransEigVecSto.Norm() << endl;
         i++;
      }
   }
}

void LanczosLinRspFunc::VccRspFctEig(const NHermLanczosChain& aChain)
{
// This method is only possible for functions of the type <<X;X>>.
   if (mOper1 != mOper2)
      MIDASERROR(" LanczosLinRspFunc::VccRspFctEig(): Eigensolution based calculation\n"
            " of response function is only possible for the type <<X;X>>, i.e.\n"
            " both operators must be identical.");
   Nb eta_norm = aChain.GetEtaNorm();                 // Norm of eta used as q(0).
   Nb xi_norm = aChain.GetXiNorm();
   Nb dotprod = aChain.GetDotProd();
   // Get eigenvalues and -vectors of T matrix.
   MidasVector eigvals(mChainLen, C_0);
   MidasVector imeigvals(mChainLen, C_0);
   MidasMatrix lefteigvecs(mChainLen);
   MidasMatrix righteigvecs(mChainLen);
   vector<Nb> helper;
   MidasMatrix hey;
   hey.SetNewSize(mChainLen,mChainLen,false, false);
   MidasMatrix bandy(mChainLen,C_0);
   bandy.Zero();
   bandy[I_0][I_0] = aChain.mTalpha[I_0];
   for(In i = I_1; i < mChainLen; i++)
   {
      bandy[i-I_1][i] = aChain.mTgamma[i];
      bandy[i][i] = aChain.mTalpha[i];
      bandy[i][i-I_1] = aChain.mTbeta[i];
   }
   Diag(bandy, righteigvecs, eigvals, imeigvals,lefteigvecs, "DGEEV",helper, hey,true,true);
   for(In i=I_0;i<mChainLen;i++)
   {
      if(fabs(imeigvals[i] - mGamma) < 0.5*mGamma)
      {
         Mout << "Warning gamma factor is getting close to imaginary part of eigevalue " << i << " : " << eigvals[i] << " + "
              << imeigvals[i] << "i" << endl;
         Mout << "Eigval in cm-1 : " << C_AUTKAYS*eigvals[i] << " + " << imeigvals[i]*C_AUTKAYS << endl;
         Mout << "imeigvals - mGamma : " << imeigvals[i] - mGamma << " mGamma : " << mGamma << endl;
      }
   }
   //Mout << "Test before biortho!!" << endl;
   //TestVccEigvectors(bandy, lefteigvecs, righteigvecs, eigvals, imeigvals);
   BiorthoEigenVecs(lefteigvecs,righteigvecs,imeigvals);
   mLeigVecs.SetNewSize(mChainLen,mChainLen,false,false);
   mLeigVecs = lefteigvecs;
   //Mout << "Test after biortho!!" << endl;
   //TestVccEigvectors(bandy, lefteigvecs, righteigvecs, eigvals, imeigvals);
   // Calculate response functions
   MidasVector re_eta_s(mChainLen);
   re_eta_s.Zero();
   MidasVector im_eta_s(mChainLen);
   im_eta_s.Zero();
   //Calc eta_s using orthonormality
   for (In i = I_0; i < mChainLen;i++)
   {
      if(imeigvals[i] == C_0 || !mImInclude)
      {
         re_eta_s[i] = righteigvecs[I_0][i];
      }
      else
      {
         mTHasImEigVal = true;
         re_eta_s[i] = righteigvecs[I_0][i];
         re_eta_s[i+I_1] = righteigvecs[I_0][i];
         im_eta_s[i] = righteigvecs[I_0][i+I_1];
         im_eta_s[i+I_1] = -righteigvecs[I_0][i+I_1];
         i++;
      }
   }
   mWeights.SetNewSize(mChainLen);
   mWeights.Zero();
   for (In i = I_0; i < mChainLen;i++)
   {
      if(imeigvals[i] == C_0 || !mImInclude)
      {
         mWeights[i] = eigvals[i]*righteigvecs[I_0][i]*lefteigvecs[I_0][i];
      }
      else
      {
         mWeights[i] = eigvals[i]*(righteigvecs[I_0][i]*lefteigvecs[I_0][i] 
                                    - righteigvecs[I_0][i+I_1]*lefteigvecs[I_0][i+I_1]);
         mWeights[i] -= imeigvals[i]*(righteigvecs[I_0][i]*lefteigvecs[I_0][i+I_1] 
                                    + righteigvecs[I_0][i+I_1]*lefteigvecs[I_0][i]);
         mWeights[i+I_1] = mWeights[i];
         i++;
      }
   }
   mImEigVals.SetNewSize(mChainLen);
   mEigVals.SetNewSize(mChainLen);
   mEigVals = eigvals;
   mImEigVals = imeigvals;
   MidasMatrix ReFQSTransform, ImFQSTransform;
   MakeTransformsForVcc(ReFQSTransform, ImFQSTransform,righteigvecs,imeigvals,aChain);
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;
   mReVals.SetNewSize(n_frq);
   mImVals.SetNewSize(n_frq);
   mFContribReVals.SetNewSize(n_frq);
   mFContribImVals.SetNewSize(n_frq);
   Mout << endl << "Calculating rsp functions : ";
   for (In i=I_0; i<n_frq; i++)
   {
      if(i % 250 == 0)
         Mout << "." << std::flush;
      Nb frq = mFrqStart + i*mFrqStep;
      //Mout << "frq " << frq << endl;
      MidasVector re_t_s_omegai(mChainLen);
      MidasVector im_t_s_omegai(mChainLen);
      re_t_s_omegai.Zero();
      im_t_s_omegai.Zero();
      MidasVector re_t_s_momegai(mChainLen);
      MidasVector im_t_s_momegai(mChainLen);
      re_t_s_momegai.Zero();
      im_t_s_momegai.Zero();
      for (In j = I_0; j<mChainLen;j++)
      {
         if(imeigvals[j] == C_0 || !mImInclude)
         {
            re_t_s_omegai[j] = (lefteigvecs[I_0][j]*(frq-eigvals[j])/((frq-eigvals[j])*(frq-eigvals[j])+mGamma*mGamma));
            im_t_s_omegai[j] = (((mGamma)/((frq-eigvals[j])*(frq-eigvals[j])+mGamma*mGamma))*lefteigvecs[I_0][j]);
            re_t_s_momegai[j] = (lefteigvecs[I_0][j]*(-frq-eigvals[j])/((-frq-eigvals[j])*(-frq-eigvals[j])+mGamma*mGamma));
            im_t_s_momegai[j] = (((-mGamma)/((-frq-eigvals[j])*(-frq-eigvals[j])+mGamma*mGamma))*lefteigvecs[I_0][j]);
         }
         else
         {
            Nb fact = C_1/((-eigvals[j]+frq)*(-eigvals[j]+frq) + (imeigvals[j]-mGamma)*(imeigvals[j]-mGamma));
            re_t_s_omegai[j] = fact*(frq-eigvals[j])*lefteigvecs[I_0][j];
            re_t_s_omegai[j] += fact*lefteigvecs[I_0][j+I_1]*(-imeigvals[j]+mGamma);         

            im_t_s_omegai[j] = fact*(mGamma-imeigvals[j])*lefteigvecs[I_0][j];            
            im_t_s_omegai[j] += fact*lefteigvecs[I_0][j+I_1]*(-eigvals[j]+frq);

            fact = C_1/((-eigvals[j]-frq)*(-eigvals[j]-frq) + (-imeigvals[j]-mGamma)*(-imeigvals[j]-mGamma));
            re_t_s_momegai[j] = fact*(-frq-eigvals[j])*lefteigvecs[I_0][j];
            re_t_s_omegai[j] += fact*lefteigvecs[I_0][j+I_1]*(-imeigvals[j]-mGamma);            

            im_t_s_momegai[j] = fact*(-mGamma-imeigvals[j])*lefteigvecs[I_0][j];
            im_t_s_omegai[j] += fact*lefteigvecs[I_0][j+I_1]*(-eigvals[j]-frq);

            //j+1 part

            fact = C_1/((-eigvals[j]+frq)*(-eigvals[j]+frq) + (-imeigvals[j]-mGamma)*(-imeigvals[j]-mGamma));
            re_t_s_omegai[j+I_1] = fact*(frq-eigvals[j])*lefteigvecs[I_0][j];
            re_t_s_omegai[j+I_1] -= fact*lefteigvecs[I_0][j+I_1]*(imeigvals[j]+mGamma);

            im_t_s_omegai[j+I_1] = fact*(mGamma+imeigvals[j])*lefteigvecs[I_0][j];
            im_t_s_omegai[j+I_1] -= fact*lefteigvecs[I_0][j+I_1]*(-eigvals[j]+frq);

            fact = C_1/((-eigvals[j]-frq)*(-eigvals[j]-frq) + (imeigvals[j]-mGamma)*(imeigvals[j]-mGamma));
            re_t_s_momegai[j+I_1] = fact*(-frq-eigvals[j])*lefteigvecs[I_0][j];
            re_t_s_omegai[j+I_1] -= fact*lefteigvecs[I_0][j+I_1]*(imeigvals[j]-mGamma);

            im_t_s_momegai[j+I_1] = fact*(-mGamma+imeigvals[j])*lefteigvecs[I_0][j];
            im_t_s_omegai[j+I_1] -= fact*lefteigvecs[I_0][j+I_1]*(-eigvals[j]-frq);
            j++;
         }
         //Mout << "Re part " << j << " : " << re_t_s_omegai[j] << " Im part : " << im_t_s_omegai[j] << endl;
         //Mout << "Re part " << j << " : " << re_t_s_momegai[j] << " Im part : " << im_t_s_momegai[j] << endl;
      }
      CalculateVccResponse(ReFQSTransform, ImFQSTransform, xi_norm*dotprod*eta_norm, xi_norm*xi_norm*dotprod,
                           re_eta_s, im_eta_s, re_t_s_momegai, im_t_s_momegai, re_t_s_omegai, im_t_s_omegai,
                           mReVals[i], mImVals[i],mFContribReVals[i],mFContribImVals[i]);
   }
   
   std::ofstream response_file("nhlan_rsp",std::ofstream::app);
   PrintResponseToStream(response_file);
   Mout << endl;
}

void LanczosLinRspFunc::CalculateVccResponse(MidasMatrix& aReFQSTransMatrix, MidasMatrix& aImFQSTransMatrix,
                                             Nb aFactor, Nb anotherFactor,MidasVector& aRealEtaVec, 
                                             MidasVector& aImEtaVec,MidasVector& aRealTmVec, MidasVector& aImTmVec,
                                             MidasVector& aRealTVec, MidasVector& aImTVec, Nb& aRealPart, 
                                             Nb& aImPart,Nb& aFRePart, Nb& aFImPart)
{
   if(!mTHasImEigVal || !mImInclude)
   {
      aRealPart = (Dot(aRealEtaVec, aRealTVec)+Dot(aRealEtaVec, aRealTmVec))*aFactor;
      aImPart = (Dot(aRealEtaVec,aImTVec)+Dot(aRealEtaVec,aImTmVec))*aFactor;

      aRealTmVec.LeftMultiplyWithMatrix(aReFQSTransMatrix);
      aImTmVec.LeftMultiplyWithMatrix(aReFQSTransMatrix);
      
      aFRePart = Dot(aRealTmVec,aRealTVec)*anotherFactor;
      aFImPart = Dot(aImTmVec,aImTVec)*anotherFactor;

   }
   else
   {
      aRealPart = (Dot(aRealEtaVec, aRealTVec)+Dot(aRealEtaVec, aRealTmVec))*aFactor;
      aImPart = (Dot(aRealEtaVec,aImTVec)+Dot(aRealEtaVec,aImTmVec))*aFactor;
      
      aRealPart -= (Dot(aImEtaVec,aImTVec)+Dot(aImEtaVec,aImTmVec))*aFactor;
      aImPart += (Dot(aImEtaVec,aRealTVec)+Dot(aImEtaVec,aRealTmVec))*aFactor;

      MidasVector aRealTmVecImTrans = aRealTmVec;
      MidasVector aImTmVecImTrans = aImTmVec;
      
      aRealTmVecImTrans.LeftMultiplyWithMatrix(aImFQSTransMatrix);
      aImTmVecImTrans.LeftMultiplyWithMatrix(aImFQSTransMatrix);

      aRealTmVec.LeftMultiplyWithMatrix(aReFQSTransMatrix);
      aImTmVec.LeftMultiplyWithMatrix(aReFQSTransMatrix);

      aFRePart = Dot(aRealTmVec,aRealTVec)*anotherFactor;
      aFRePart -= Dot(aImTmVec,aImTVec)*anotherFactor;
      aFRePart -= Dot(aImTmVecImTrans,aRealTVec)*anotherFactor;
      aFRePart -= Dot(aRealTmVecImTrans,aImTVec)*anotherFactor;
      
      aFImPart = Dot(aRealTmVecImTrans,aRealTVec)*anotherFactor;
      aFImPart += Dot(aImTmVec,aRealTVec)*anotherFactor;
      aFImPart += Dot(aRealTmVec,aImTVec)*anotherFactor;
      aFImPart -= Dot(aImTmVecImTrans,aImTVec)*anotherFactor;
      
   }
   aRealPart += aFRePart;
   aImPart   += aFImPart;
}

void LanczosLinRspFunc::VciRspFctEig(const LanczosChain& aChain)
{
   // This method is only possible for functions of the type <<X;X>>.
   if (mOper1 != mOper2)
      MIDASERROR(" LanczosLinRspFunc::VciRspFctEig(): Eigensolution based calculation\n"
            " of response function is only possible for the type <<X;X>>, i.e.\n"
            " both operators must be identical.");
   
   Nb eta_norm = aChain.GetEtaNorm();                 // Norm of eta used as q(0).
   
   // Get eigenvalues and -vectors of T matrix.
   MidasVector eigvals(mChainLen, C_0);
   MidasMatrix eigvecs(mChainLen);
   aChain.Diag(mChainLen, &eigvals, &eigvecs);

   // Calculate response functions
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;
   mReVals.SetNewSize(n_frq);
   mImVals.SetNewSize(n_frq);
   for (In i=I_0; i<n_frq; ++i)
   {
      Nb frq = mFrqStart + i*mFrqStep;
      
      Nb accr = C_0;     // Accumulated real part.
      Nb acci = C_0;     // Accumulated imaginary part.
      for (In j=I_0;j<mChainLen;j++) 
      {
         Nb fact = (eta_norm*eta_norm)/((eigvals[j]-frq)*(eigvals[j]-frq)
                   + mGamma*mGamma);
         accr += fact*eigvecs[I_0][j]*eigvecs[I_0][j]*(eigvals[j]-frq); 
         acci += fact*eigvecs[I_0][j]*eigvecs[I_0][j]*mGamma;

         fact = (eta_norm*eta_norm)/((eigvals[j]+frq)*(eigvals[j]+frq)
                + mGamma*mGamma);
         accr += fact*eigvecs[I_0][j]*eigvecs[I_0][j]*(eigvals[j]+frq); 
         acci -= fact*eigvecs[I_0][j]*eigvecs[I_0][j]*mGamma;
      }
      mReVals[i] = accr;
      mImVals[i] = acci;
   }
}

void LanczosLinRspFunc::VciRspFctCf(const LanczosChain& aChain)
{
   // This method is only possible for functions of the type <<X;X>>.
   if (mOper1 != mOper2)
      MIDASERROR(" LanczosLinRspFunc::VciRspFctCf(): Continued fraction based calculation\n"
            " of response function is only possible for the type <<X;X>>, i.e.\n"
            " both operators must be identical.");
   
   Nb eta_norm = aChain.GetEtaNorm();                 // Norm of eta used as q(0).

   // Calculate response functions
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;
   mReVals.SetNewSize(n_frq);
   mImVals.SetNewSize(n_frq);
   for (In i=I_0; i<n_frq; ++i)
   {
      complex<double> cfminus(C_0,C_0);
      complex<double> cfplus(C_0,C_0);
      Nb frq = mFrqStart + i*mFrqStep;
      complex<double> shift (frq,mGamma);
      for (In j=I_1;j<mChainLen+I_1;j++) 
      {
         complex<double> cpx_alpha(aChain.GetAlphaVal(mChainLen-j),C_0);
         complex<double> cpx_beta(aChain.GetBetaVal(mChainLen-j),C_0);
         complex<double> one(C_1,C_0);
         cfminus=one/(shift-cpx_alpha-cpx_beta*cpx_beta*cfminus);
         cfplus =one/(-shift-cpx_alpha-cpx_beta*cpx_beta*cfplus);
      }
      mReVals[i] = -(eta_norm*eta_norm)*(cfminus.real()+cfplus.real());
      mImVals[i] = -(eta_norm*eta_norm)*(cfminus.imag()+cfplus.imag());
   }
}

void LanczosLinRspFunc::WriteDataToFile()
{
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;

   string filename = gAnalysisDir+"/"+mName + ".dat";
   Mout << " Generating file: '" << filename << "'" << endl;
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   midas::stream::ScopedPrecision(15, file);
   file << scientific
        << "# Linear response function <<" << mOper1 << "," << mOper2 << ">>"
        << " generated using Lanczos chain." << endl
        << "# Chain length: " << mChainLen << endl
        << "# gamma (damping factor): " << mGamma*C_AUTKAYS << " cm-1" << endl
        << "# Columns: freq. (cm-1)     Real part (au)     Imag. part (au)" << endl;


   for (In i=I_0; i<n_frq; ++i)
      file << (mFrqStart+i*mFrqStep)*C_AUTKAYS
           << "   " << mReVals[i] << "   " << mImVals[i] << endl;

   file_stream.close();
   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      string filename2 = gAnalysisDir+"/"+mName + "_extrainfo.dat";
      Mout << " Generating file: '" << filename2 << "'" << endl;
      ofstream file_stream2(filename2.c_str(), ios_base::trunc);
      MidasStreamBuf file2_buf(file_stream2);
      MidasStream file2(file2_buf);
      midas::stream::ScopedPrecision(15, file2);
      file2 << scientific
         << "# Linear response function <<" << mOper1 << "," << mOper2 << ">>"
         << " generated using Lanczos chain." << endl
         << "# Chain length: " << mChainLen << endl
         << "# gamma (damping factor): " << mGamma*C_AUTKAYS << " cm-1" << endl
         << "# Columns: freq. (cm-1)     Real part (au)     Imag. part (au)     FReal part (au)     FImag. part (au)" << endl;
        
      for (In i=I_0; i<n_frq; ++i)
      {
         //Mout << (mFrqStart+i*mFrqStep)*C_AUTKAYS
         //      << "   " << mFContribImVals[i] << endl;
         file2 << (mFrqStart+i*mFrqStep)*C_AUTKAYS
               << "   " << mReVals[i] << "   " << mImVals[i] << "   " << mFContribReVals[i] 
               << "   " << mFContribImVals[i] << endl;
      }
   }
}

void LanczosLinRspFunc::GetEigSolutions(MidasVector& aEigVals, MidasMatrix& aEigVecs) const
{
   aEigVals.SetNewSize(mChainLen);
   aEigVecs.SetNewSize(mChainLen, mChainLen);
   aEigVals.Zero();
   aEigVecs.Zero();
   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      aEigVals = mEigVals;
      return;
   }
   LanczosChainDef chaindef(mOper2, mChainLen, mOrthoChain);
   chaindef.AddXoper(mOper1);
   const LanczosChain* chain = FindChain(chaindef);
   chain->Diag(mChainLen, &aEigVals, &aEigVecs);
}

void LanczosLinRspFunc::GetEigSolutions(MidasVector& aWeights, MidasVector& aEigVals, MidasVector& aImEigVals,MidasMatrix& aEigVecs, MidasMatrix& aLEigVecs) const
{
   aEigVals.SetNewSize(mChainLen);
   aImEigVals.SetNewSize(mChainLen);
   aEigVecs.SetNewSize(mChainLen, mChainLen);
   aLEigVecs.SetNewSize(mChainLen, mChainLen);
   aImEigVals.Zero();
   aEigVals.Zero();
   aEigVecs.Zero();
   aWeights.SetNewSize(mChainLen);
   aWeights = mWeights;
   aEigVals = mEigVals;
   aImEigVals = mImEigVals;
   aEigVecs = mEigVecs;
   aLEigVecs = mLeigVecs;
}

void LanczosLinRspFunc::TransformStoX(const MidasVector& aS, DataCont& aX) const
{
   LanczosChainDef chaindef(mOper2, mChainLen, mOrthoChain);
   chaindef.AddXoper(mOper1);
   const LanczosChain* chain = FindChain(chaindef);

   chain->TransformStoX(aS, aX);
}

void LanczosLinRspFunc::TransformStoX(const MidasVector& aR, const MidasVector& aL, DataCont& aRT, DataCont& aLT) const
{
   NHermLanczosChainDef chaindef(mOper2, mChainLen, mOrthoChain);
   chaindef.AddXoper(mOper1);
   const NHermLanczosChain* chain = FindChainNH(chaindef);
   chain->TransformStoX(aR, aRT, true);
   chain->TransformStoX(aL, aLT, false);
}

ostream& LanczosLinRspFunc::Print(ostream& aOut) const
{
   aOut << " <<" << mOper1 << ";" << mOper2 << ">> (Lanczos)" << endl
        << "    Name (file prefix): '" << mName << "'" << endl;

   if (mFrqStart == mFrqEnd)
      aOut << "    Frequency:          " << mFrqStart*C_AUTKAYS << "cm-1" << endl;
   else
      aOut << "    Frequency range:    (" << mFrqStart*C_AUTKAYS << ":" << mFrqEnd*C_AUTKAYS
           << ":" << mFrqStep*C_AUTKAYS << ") cm-1" << endl;

   aOut << "    Gamma:              " << mGamma*C_AUTKAYS << " cm-1" << endl
        << "    Solver method:      " << LanczosRspFunc::StringForConst(mSolver) << endl
        << "    Chain length:       " << mChainLen << endl
        << "    Orthogonalization:  " << LanczosRspFunc::StringForConst(mOrthoChain) << endl;

   return aOut;
}

void LanczosLinRspFunc::PrintResponseToStream(std::ostream& os) const
{
   midas::stream::ScopedPrecision(17, os);
   os << scientific;
   Nb frq = mFrqStart;
   for(int i=0; i<mReVals.size(); ++i)
   {
      os << "Linear response for frq=" << frq << " + i*" << mGamma<< " (au)    " << frq*C_AUTKAYS << " + i*" << mGamma*C_AUTKAYS << "(cm^-1)\n";
      os << mReVals[i] << "\n";
      os << mImVals[i] << "\n";
      frq+=mFrqStep;
   }
}

vector<RspFunc> LanczosLinRspFunc::ConvertToRspFunc() const
{
   vector<RspFunc> result;
   RspFunc helper(I_2);
   helper.SetOp(mOper1,I_0);
   helper.SetOp(mOper2,I_1);
   In n_frq = In((mFrqEnd-mFrqStart)/mFrqStep) + I_1;
   for (In i=I_0; i<n_frq; ++i) {
      Nb frq=mFrqStart+i*mFrqStep;
      helper.SetFrq(frq,I_1);
      helper.SetValue(mReVals[i]);
      helper.SetHasBeenEval(true);
      result.push_back(helper);
   }
   Mout << "I will return Convert to RspFunc..." << endl;
   return result;
}
