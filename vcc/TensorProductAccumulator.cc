#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "TensorProductAccumulator.h"
#include "TensorProductAccumulator_Impl.h"

namespace midas::vcc
{

// define 
#define INSTANTIATE_TENSORPRODUCTACCUMULATOR(T) \
template class TensorProductAccumulator<T>; \

// concrete instantiations
INSTANTIATE_TENSORPRODUCTACCUMULATOR(Nb)
INSTANTIATE_TENSORPRODUCTACCUMULATOR(std::complex<Nb>)

#undef INSTANTIATE_TENSORPRODUCTACCUMULATOR

} /* namespace midas::vcc */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

