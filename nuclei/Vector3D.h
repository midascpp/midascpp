/**
************************************************************************
* 
* @file                Vector3D.h
*
* Created:             04-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Interface 3-dimensional vector class.
* 
* Last modified: man mar 21, 2005  11:35
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VECTOR3D_H
#define VECTOR3D_H

#include<iostream>
using std::istream;
using std::ostream;

#include"inc_gen/math_link.h"
#include "mmv/MidasMatrix.h"

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"

class Nuclei;  
/**
* Class Vector3D: Vector in three dimensions constructed from two points.
* Provides length, angles, dot and cross products with other vectors. 
* */
class Vector3D
{
   public:
      Vector3D();                               ///< Create uninitialized.
      Vector3D(Nb aX, Nb aY, Nb aZ);            ///< Create from coordinates.
      Vector3D(Nuclei aNuc1, Nuclei aNuc2);     ///< Create from two points.
      Nb Length() const;                        ///< Length of vector.
      Nb Dot(Vector3D aV) const;                ///< Dot with v2.
      Vector3D Cross(Vector3D aV) const;        ///< Vector product with v2.
      Vector3D TransformMidasMatrix(MidasMatrix) const;
      Nb Angle(Vector3D aV) const;              ///< Angle relative to v in degrees.
      Nb RadAngle(Vector3D aV) const;           ///< Angle relative to v in radians.
      Nb Dist(Vector3D aV) const;               ///< Distance relative to v
      Nb X() const;                             ///< Provide x
      Nb Y() const;                             ///< Provide y
      Nb Z() const;                             ///< Provide z
      void AddX(Nb arX);
      void AddY(Nb arY);
      void AddZ(Nb arZ);
      void SetX(Nb arNb) {mX = arNb;}          ///< Set x
      void SetY(Nb arNb) {mY = arNb;}          ///< Set y
      void SetZ(Nb arNb) {mZ = arNb;}          ///< Set z
      void Normalize();                        ///< Normalize vector
      void Orthonormalize(const Vector3D& aV); ///< Orthonormalize agains aV 
      void Orthogonalize(const Vector3D& aV);  ///< Orthogonalize agains aV 
      Vector3D& operator+=(const Vector3D& aV); ///< Define addition, unary 
      Vector3D& operator-=(const Vector3D& aV); ///< Define subtraction, unary 
      friend Vector3D operator-(const Vector3D&); ///< unary -
      friend Vector3D operator*(Nb aAlpha, const Vector3D& aV); 
                                                ///< Define scalar multiply
      friend Vector3D operator*(const Vector3D& aV,Nb aAlpha);  
                                                ///< Define scalar multiply
      friend Vector3D operator+(const Vector3D& aV1,const Vector3D& aV2); 
                                                ///< Define addition, binary
      friend Vector3D operator-(const Vector3D& aV1,const Vector3D& aV2); 
                                                ///< Define subtraction, binary
      friend ostream& operator<<(ostream& aOs,const Vector3D& v); 
                                                ///< Define output
      void Zero() {mX=C_0;mY=C_0;mZ=C_0;}        ///< Provide z
      Nb operator[](In i) const {if (i==0) return mX; else if (i==1) return mY; else if (i==2) return mZ; return C_NB_MAX;} 
                                             // output but no ref. 
   private:
      Nb mX;                                    ///< Actual x coordinate
      Nb mY;                                    ///< Actual y coordinate
      Nb mZ;                                    ///< Actual z coordinate
}; 

#endif
