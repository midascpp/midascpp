/**
************************************************************************
* 
* @file                AtomicData.cc
*
* Created:             07-01-2006
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Member functions for AtomicData class.
* 
* Last modified: Sun Jan 08, 2006  12:22AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <map>
using std::map;
#include <algorithm>
using std::transform;
using std::max;
#include<iostream>
#include<fstream>
#include<sstream>
using std::ifstream;
#include<utility>


// Link to standard headers:
#include "inc_gen/math_link.h"
#include "inc_gen/stdlib_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"  /// Has to be the first.
#include "inc_gen/Const.h"    /// provides pi. 
#include "input/ProgramSettings.h"
#include "util/Io.h"  /// i-o stuff
#include "util/Path.h"

#include "nuclei/AtomicData.h"  

using namespace midas;

/**
* Constructor 
* */
AtomicData::AtomicData()
{
   mHasBeenInit=false; 
} 
/**
* Constructor 
* */
void AtomicData::Init()
{
   InitStrings();
   InitMasses();
   mHasBeenInit=true; 
} 
/**
* FillUp
* */
void AtomicData::InitMasses()
{
   std::string filename = "atomic_masses";
   std::string datafile = path::Join(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR"), filename);
   std::ifstream nuc_mass_file(datafile, std::ios::in);
   
   // Check that we actually opened the file
   if(nuc_mass_file.fail())
   {
      MIDASERROR("AtomicData::InitMasses() : Could not open '" + datafile + "' for reading.");
   }

   In z,a;
   Nb m,w;
   std::string str;
   
   while(std::getline(nuc_mass_file, str))
   {
      std::stringstream sstr(str);
      sstr >> z >> a >> m >> w;
      mAtomicMasses[std::make_pair(z,a)] = std::make_pair(m,w);
   }

   set<In> znucs;
   //znucs.reserve(110);
   //Mout << " List of atomic data " << endl;
   for (map<pair<In,In>,pair<Nb,Nb> >::const_iterator ci=mAtomicMasses.begin();
        ci != mAtomicMasses.end();ci++)
   {
       //Mout << setw(10) << ci->first.first << " " << ci->first.second << " " << 
          //ci->second.first << " " << ci->second.second << endl;
       znucs.insert(ci->first.first);
   }

   //Mout << " Most Common Isotopes " << endl;
   for (set<In>::const_iterator si=znucs.begin();si != znucs.end();si++)
   {
      In z = *si; 
      In a_common = 0;
      Nb w_common = 0;
      for (map<pair<In,In>,pair<Nb,Nb> >::const_iterator ci=mAtomicMasses.begin();
           ci != mAtomicMasses.end();ci++)
      {
         if (ci->first.first==z && ci->second.second > w_common)
         {
            a_common = ci->first.second;
            w_common = ci->second.second;
         }
      }
      mMostCommonIsotope[z]=a_common;
      //Mout << " z = " << z << " a_most_common = " << mMostCommonIsotope[z] << endl;
   }
}
/**
* FillUp
* */
void AtomicData::InitStrings()
{
   string datadir  = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR");
   string filename = "nucmap.data";
   string datafile=datadir+"/"+filename;
   ifstream nuc_map_file(datafile.c_str(),ios::in);

   string line;
   while (getline(nuc_map_file,line))
   {
      string nuc = line.substr(0,2);
      while(nuc.find(" ")!= nuc.npos) nuc.erase(nuc.find(" "),I_1); // delete blanks
      string st_z_nuc = line.substr(2);
      In z_nuc = atoi(st_z_nuc.c_str());
      mNucMapLabToZnuc[nuc] = z_nuc;
      mNucMapZnucToLab[z_nuc] = nuc;
   }
   nuc_map_file.close();
}
  
/**
* GetMass
* */
Nb AtomicData::GetMass(In aZ, In aA) 
{
   pair<In,In> p1(aZ,aA);
   pair<Nb,Nb> p2 = mAtomicMasses[p1];
   return p2.first;
}
/**
* GetIsotopicWeight 
* */
Nb AtomicData::GetIsotopicWeight(In aZ,In aA) 
{
   pair<In,In> p1(aZ,aA);
   pair<Nb,Nb> p2 = mAtomicMasses[p1];
   return p2.second;
}
/**
* GetString from Z  
* */
string AtomicData::GetStringFromZ(In aZ) 
{
   string s = mNucMapZnucToLab[aZ];
   return s; 
}
/**
* Get Z from label 
* */
In AtomicData::GetZnucFromString(const std::string& aS) 
{
   In z = mNucMapLabToZnuc[aS];
   return z; 
}

