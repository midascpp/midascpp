/**
************************************************************************
* 
* @file                Vector3D.cc
*
* Created:             03-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Member functions for vector3D class.
* 
* Last modified: Thu Oct 15, 2009  01:41PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include<iostream>
using std::ostream;
using std::cout;
using std::endl;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h" // skal vaere foran alle hvori Nb kan taenkes brugt.
#include "inc_gen/Const.h" 

#include "nuclei/Nuclei.h"  
#include "nuclei/Vector3D.h"  
#include "input/Input.h"  
#include "mmv/MidasMatrix.h"  

/**
* Construct vector uninitialized.
* */
Vector3D::Vector3D()
{
   mX = C_0;
   mY = C_0;
   mZ = C_0;
}

/**
* Construct vector from given coordinates.
* */
Vector3D::Vector3D(Nb aX, Nb aY, Nb aZ)
{
   mX = aX;
   mY = aY;
   mZ = aZ;
}

/**
* Construct vector from between two nuclei. 
* */
Vector3D::Vector3D(Nuclei aNuc1,Nuclei aNuc2)
{
   mX = aNuc2.X() - aNuc1.X();
   mY = aNuc2.Y() - aNuc1.Y();
   mZ = aNuc2.Z() - aNuc1.Z();
}

/**
* Provide interface for coordinates of vector
* */
Nb Vector3D::X() const {return mX;}
Nb Vector3D::Y() const {return mY;}
Nb Vector3D::Z() const {return mZ;}

/**
* Dot product between current vector and vector v
* NB! Implement as operator!!!
* */
Nb Vector3D::Dot(Vector3D aV) const
{
   return mX*aV.mX+mY*aV.mY+mZ*aV.mZ;
} 
/**
* Cross product between current vector and vector v
* */
Vector3D Vector3D::Cross(Vector3D aV) const
{
   Vector3D tmp;
   tmp.mX = mY*aV.mZ - mZ*aV.mY;
   tmp.mY = mZ*aV.mX - mX*aV.mZ;
   tmp.mZ = mX*aV.mY - mY*aV.mX;
   return tmp;
} 

Vector3D Vector3D::TransformMidasMatrix(MidasMatrix aM) const
{
   //Mout << "Midasmatrix: " << endl << aM << endl;
   if(aM.Ncols()!=I_3 || aM.Nrows()!=I_3) {
      Mout << "Cols = " << aM.Ncols() << " Rows = " << aM.Nrows() << endl;
      MIDASERROR("You can only transform a Vector3D by 3x3 matrices!");
   }
   Vector3D tmp=*this;
   tmp.mX=C_0;
   tmp.mY=C_0;
   tmp.mZ=C_0;
   for(In i=I_0;i<I_3;i++)
      for(In j=I_0;j<I_3;j++)
         if(i==0)
            tmp.mX+=(*this)[j]*aM[i][j];
         else if(i==1)
            tmp.mY+=(*this)[j]*aM[i][j];
         else 
            tmp.mZ+=(*this)[j]*aM[i][j];
   return tmp;
}
/**
* Length of vector
* */
Nb Vector3D::Length() const
{
   return sqrt(Dot(*this));
}
/**
* Normalize vector
* */
void Vector3D::Normalize()
{
   Nb a = C_1/sqrt(Dot(*this));
   mX = a*mX;
   mY = a*mY;
   mZ = a*mZ;
}
/**
* Orthonormalize vector
* */
void Vector3D::Orthonormalize(const Vector3D& aV)
{
   Nb a = Dot(aV);
   mX -= a*aV.X();
   mY -= a*aV.Y();
   mZ -= a*aV.Z();
   Normalize();
}

/**
* Orthogonalize vector
* */
void Vector3D::Orthogonalize(const Vector3D& aV)
{
   Nb a = Dot(aV);
   mX -= a*aV.X();
   mY -= a*aV.Y();
   mZ -= a*aV.Z();
}

/**
* Provide Angle between two vectors in radians
**/
Nb Vector3D::RadAngle(Vector3D aV) const
{
   Nb l1 = Length();
   Nb l2 = aV.Length();
   Nb d12= Dot(aV);
   Nb cosa = d12/(l1*l2);

   return acos(cosa);
}
/**
* Provide Angle between two vectors in degrees
* */
Nb Vector3D::Angle(Vector3D aV) const
{
   Nb l1 = Length();
   Nb l2 = aV.Length();
   Nb d12= Dot(aV);  
   Nb cosa = d12/(l1*l2);
   return acos(cosa)*C_180/C_PI;
}

/**
* Provide Distance between two vectors.
* */
Nb Vector3D::Dist(Vector3D aV) const
{
   Nb dist=(mX-aV.X())*(mX-aV.X());
   dist += (mY-aV.Y())*(mY-aV.Y());
   dist += (mZ-aV.Z())*(mZ-aV.Z());
   return sqrt(dist);
}

/**
* Add delta to x
**/
void Vector3D::AddX(Nb arX)
{
  mX+=arX;
}

/**
* Add delta to y
**/
void Vector3D::AddY(Nb arY)
{
  mY+=arY;
}

/**
* Add delta to z
**/
void Vector3D::AddZ(Nb arZ)
{
  mZ+=arZ;
}

/**
* Output operator for Vector3D
* */
ostream& operator<<(ostream& os,const Vector3D& aV)
{
   return os << "(" << aV.X()<< "," << aV.Y() << "," << aV.Z() << ")";
}

/**
* Operator for scalar multiplication of vector
* */
Vector3D operator*(Nb s, const Vector3D& aV)
{
   Nb resmx = s*aV.X();
   Nb resmy = s*aV.Y(); 
   Nb resmz = s*aV.Z();
   Vector3D res(resmx,resmy,resmz);
   return res;
}

/**
* Operator for scalar multiplication of vector
* */
Vector3D operator*(const Vector3D& aV, Nb s)
{
   return s*aV;
}

/**
* Operator for addition of two vectors -  binary addition
* */
Vector3D operator+(const Vector3D& aV1, const Vector3D& aV2)
{
   Vector3D sum = aV1;
   sum += aV2;
   return sum;
}
/**
* Operator for subtraction of two vectors -  binary addition
* */
Vector3D operator-(const Vector3D& aV1, const Vector3D& aV2)
{
   Vector3D sum = aV1;
   sum -= aV2;
   return sum;
}
/**
* Operator for unary - 
* */
Vector3D operator-(const Vector3D& aV1)
{
   return Vector3D(C_0,C_0,C_0)-aV1;
}
/**
* Operator for unary addition to a Vector. 
* */
Vector3D& Vector3D::operator+=(const Vector3D& aV1)
{
   mX += aV1.X();
   mY += aV1.Y();
   mZ += aV1.Z();
   return *this;
}
/**
* Operator for unary addition to a Vector. 
* */
Vector3D& Vector3D::operator-=(const Vector3D& aV1)
{
   mX -= aV1.X();
   mY -= aV1.Y();
   mZ -= aV1.Z();
   return *this;
}

