/**
************************************************************************
* 
* @file                Nuclei.cc
*
* Created:             03-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Member functions for Nuclei class.
* 
* Last modified: March, 26th 2015 (carolin)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <map>
#include <math.h>
using std::map;
#include <algorithm>
using std::transform;
using std::max;

// Link to standard headers:
#include "inc_gen/math_link.h"
#include "inc_gen/stdlib_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"  /// Has to be the first.
#include "inc_gen/Const.h"    /// provides pi. 
#include "input/Input.h"  /// Has to be the first.
#include "util/Io.h"  /// i-o stuff
#include "nuclei/AtomicData.h"  /// i-o stuff
#include "nuclei/Nuclei.h"  
#include "nuclei/Vector3D.h"  
#include "mmv/MidasMatrix.h"  

#include "libmda/numeric/float_eq.h"

/**
* Constructor for nuclei with coordinates
* */
Nuclei::Nuclei( Nb aX
              , Nb aY
              , Nb aZ
              , Nb aQ
              , const std::string& aAtomLabel
              , const std::string& aBasisLabel
              , In aSubSysMem
              , In aSubSys
              , const NucTreatType& arNucTreatType
              )
   : mX(aX)
   , mY(aY)
   , mZ(aZ)
   , mQ(aQ)
   , mSubSys(aSubSys)
   , mSubSysMem(aSubSysMem)
   , mAtomLabel(aAtomLabel)
   , mBasisLabel(aBasisLabel)
   //, mZnuc(GetZFromLabel())
   , mForceNonStandard(false)
   , mNucTreatType(arNucTreatType)
{
   mZnuc       = GetZFromLabel();
   SetDigitModest(I_6);
   SetShiftLenNeg(C_I_10_13);
   SetToMostCommonIsotope();
}

/**
* Constructor without initialization.
* */
Nuclei::Nuclei()
{ 
   mNucTreatType=NucTreatType::ACTIVE;
   mForceNonStandard = false;
   SetDigitModest(I_6);
   SetShiftLenNeg(C_I_10_13);
}

/**
* Distance to another nuclei
* */
Nb Nuclei::Distance(const Nuclei& arNuclei) const
{
   Nb xdif = mX - arNuclei.X();
   Nb ydif = mY - arNuclei.Y();
   Nb zdif = mZ - arNuclei.Z();
   return sqrt(xdif*xdif+ydif*ydif+zdif*zdif);
}
/**
* Distance to a point defined by a 3D vector
* */
Nb Nuclei::Distance(const Vector3D& arP3d) const
{
   Nb xdif = mX - arP3d.X();
   Nb ydif = mY - arP3d.Y();
   Nb zdif = mZ - arP3d.Z();
   return sqrt(xdif*xdif+ydif*ydif+zdif*zdif);
}
/**
* Distance to origo
* */
Nb Nuclei::DistanceToOrigo() const
{
   return sqrt(mX*mX+mY*mY+mZ*mZ);
}

/**
* Shift by a 3D vector
* */
void Nuclei::Shift(const Vector3D& arP3d) 
{
   mX += arP3d.X();
   mY += arP3d.Y();
   mZ += arP3d.Z();
}

/**
* Shift by a 3D vector
* */
void Nuclei::Shift(const MidasVector& arP3d) 
{
   mX += arP3d[0];
   mY += arP3d[1];
   mZ += arP3d[2];
}

/**
* Repulsion to another nuclei,
* Constant is put to one (atomic units, if structures are in atomic units).
* */
Nb Nuclei::Repuls(const Nuclei& arNuclei) const
{
   Nb d = Distance(arNuclei);
   return mQ*arNuclei.Q()/d;
}

/**
* Move existing Nuclei to new position by giving the new position
* */
void Nuclei::MoveTo(Nb aX, Nb aY, Nb aZ) 
{
   mX = aX;
   mY = aY;
   mZ = aZ;
}
/**
* Output with human number of digits for xsf format 
* */
string Nuclei::OutXyzFormat(bool arConverToAangstrom)
{
   std::ostringstream oss;
   midas::stream::ScopedPrecision(8, oss);
   oss.setf(ios_base::fixed);
   oss.unsetf(ios_base::scientific);
   oss<< AtomLabel()<< " ";
   oss.width(15);
   if (arConverToAangstrom) oss<< right << X()*C_TANG << " " << right << Y()*C_TANG << " " << right << Z()*C_TANG << " ";
   else oss << right << X() << " " << right << Y() << " " << right << Z() << " ";
   return oss.str();
}

#ifndef SYMMETRY_DEBUG_VERBOSE
#define SYMMETRY_DEBUG_VERBOSE false
#endif /* SYMMETRY_DEBUG_VERBOSE */

/**
 *
 **/
bool Nuclei::IsNumEqual
   (  const Nuclei& aOther
   ,  In aUlps
   )  const
{
   auto checkcoord_equal = [](Nb aX1, Nb aX2, In aUlps)
   {
      if constexpr(SYMMETRY_DEBUG_VERBOSE)
      {
         std::cout << " check 1 " << libmda::numeric::float_eq(aX1, aX2, aUlps) << std::endl;
         std::cout << " check 2 " << libmda::numeric::float_numeq_zero(aX1, C_1, aUlps) << std::endl;
         std::cout << " check 3 " << libmda::numeric::float_numeq_zero(aX2, C_1, aUlps) << std::endl;
      }

      return   (  libmda::numeric::float_eq(aX1, aX2, aUlps) 
               || (  libmda::numeric::float_numeq_zero(aX1, C_1, aUlps)
                  && libmda::numeric::float_numeq_zero(aX2, C_1, aUlps)
                  )
               );
   };

   if constexpr(SYMMETRY_DEBUG_VERBOSE)
   {
      std::cout << " check x " << mX << "  " << aOther.mX << std::endl << checkcoord_equal(mX, aOther.mX, aUlps) << std::endl;
      std::cout << " check y " << mY << "  " << aOther.mY << std::endl << checkcoord_equal(mY, aOther.mY, aUlps) << std::endl;
      std::cout << " check z " << mZ << "  " << aOther.mZ << std::endl << checkcoord_equal(mZ, aOther.mZ, aUlps) << std::endl;   
      std::cout << std::endl;
   }

   if (  this->GetCleanAtomLabel() == aOther.GetCleanAtomLabel()
      && checkcoord_equal(mX, aOther.mX, aUlps)
      && checkcoord_equal(mY, aOther.mY, aUlps)
      && checkcoord_equal(mZ, aOther.mZ, aUlps)
      )
   {
      return true;
   }
   return false;
}

/**
* Output operator for Nuclei.
* */
ostream& operator<<(ostream& aOs,const Nuclei& arNuclei)
{
   In n_digit = I_20;
   if (arNuclei.mForceNonStandard) n_digit = arNuclei.DigitModest(); //arNuclei.mDigitModest;
   
   midas::stream::ScopedPrecision(n_digit, aOs);
   aOs.setf(ios_base::showpoint|ios_base::showpos|ios_base::scientific);
   aOs << left << setw(5) << arNuclei.AtomLabel() << " ";
   aOs << right;
   aOs << arNuclei.X()<< " " << arNuclei.Y()<< " " << arNuclei.Z()<< " ";
   
   midas::stream::ScopedPrecision(n_digit/2, aOs);
   aOs << arNuclei.Q()<< " ";
   aOs.setf(ios_base::fixed);
   aOs.unsetf(ios_base::showpos);
   
   midas::stream::ScopedPrecision(3, aOs);
   aOs << setw(3) << arNuclei.SubSys() << " ";
   aOs << setw(3) << arNuclei.SubSysMem() << " ";
   aOs << arNuclei.BasisLabel();
   aOs.unsetf(ios_base::showpos|ios_base::fixed);
   
   return aOs;
}

/**
* Input operator for Nuclei.
* */
istream& operator>>(istream& aOs,Nuclei& arNuclei)
{
   Nb x=C_0;
   Nb y=C_0;
   Nb z=C_0;
   Nb q=C_0;
   In sub_sys_mem    = I_1;
   In sub_sys        = I_1;
   string atom_label = "X";
   string basis      = " ";
   aOs >> atom_label >> x >> y >> z;
   aOs >> q >> sub_sys >> sub_sys_mem >> basis; 
   Nuclei tmp(x,y,z,q,atom_label,basis,sub_sys_mem,sub_sys);
   arNuclei = tmp;
   return aOs;
}

/**
* Provide angle between three nuclei with *this in the middle. (Degrees)
* */
Nb Nuclei::Angle(const Nuclei& arNuc1, const Nuclei& arNuc3) const
{
   Vector3D n2n1(*this,arNuc1);
   Vector3D n2n3(*this,arNuc3);
   return n2n1.Angle(n2n3);
}

/**
* Provide angle between three nuclei with *this in the middle. (Radians)
* */
Nb Nuclei::RadAngle(const Nuclei& arNuc1, const Nuclei& arNuc3) const
{
   Vector3D n2n1(*this,arNuc1);
   Vector3D n2n3(*this,arNuc3);
   return n2n1.RadAngle(n2n3);
}

/**
* Provide dihedral angle between four nuclei with *this at the end
* */
Nb Nuclei::Dihedral(const Nuclei& arNuc1, const Nuclei& arNuc2, const Nuclei& arNuc3)
{
   Vector3D n2n1(arNuc3, arNuc2);
   Vector3D n3n2(arNuc2, arNuc1);
   Vector3D n4n3(arNuc1,*this);

   Vector3D dir1 = n2n1.Cross(n3n2);
   Vector3D dir2 = n3n2.Cross(n4n3);

   return atan2(n3n2.Length()*n2n1.Dot(dir2),dir1.Dot(dir2));
}

/**
* Get Z from the label and put it into Q and Znuc
* */
void Nuclei::SetQzFromLabel() 
{
   mZnuc = GetZFromLabel();
   mQ = mZnuc;
}

/**
 * Get clean atom label (only atom type).
 **/
std::string Nuclei::GetCleanAtomLabel
   (
   ) const
{
   string label = mAtomLabel.substr(0,2);
   transform(label.begin(),label.end(),label.begin(),(In(*) (In))toupper);
   while(label.find(" ")!= label.npos) label.erase(label.find(" "),I_1); // delete blanks
   transform(label.begin(),label.end(),label.begin(),(In(*) (In))toupper);
   In len = label.length();
   for (In i=0;i<len;i++) 
   {
      char c1 = label[i];
      int i1 = isalpha(c1); // check if character is alphabetic
      if (i1 == 0) label.erase(i);
   }
   return label;
}

/**
* Get Z from the label.
* */
In Nuclei::GetZFromLabel() 
{
   string label = mAtomLabel.substr(0,2);
   transform(label.begin(),label.end(),label.begin(),(In(*) (In))toupper);
   while(label.find(" ")!= label.npos) label.erase(label.find(" "),I_1); // delete blanks
   transform(label.begin(),label.end(),label.begin(),(In(*) (In))toupper);
   In len = label.length();
   for (In i=0;i<len;i++) 
   {
      char c1 = label[i];
      int i1 = isalpha(c1); // check if character is alphabetic
      if (i1 == 0) label.erase(i);
   }

   In z2 = -1;
   z2 = gAtomicData.GetZnucFromString(label);
   
   if (z2 <=0)
   {
      Mout << "label:>" << label << "<" << endl;
      Mout << " GetZFromLabel: z found: " << z2 << " appears unusual to me!" << endl;
   }
   
   return z2;
}

/**
* Get Label from Z label.
* */
string GetLabelFromZ(In aZ) 
{
   return gAtomicData.GetStringFromZ(aZ); 
}
/**
* Scale the coordinates 
* */
void Nuclei::ScaleCoord(const Nb& arScaleFac) 
{
   mX *= arScaleFac;
   mY *= arScaleFac;
   mZ *= arScaleFac;
}
/**
* Sort nuclei after
* 0 first
*   sort after (i) highest charge first; (ii) basis, (iii) Subsys 
*   (iv) SubSysMem
* */
bool NucleiLess(const Nuclei& arN1, const Nuclei& arN2)
{
   if (arN1.SubSys() == I_0 && arN2.SubSys() == I_0)
   {
      if (arN1.Znuc() != arN2.Znuc()) 
         return arN1.Znuc() > arN2.Znuc();
      else if (arN1.BasisLabel() != arN2.BasisLabel())
         return arN1.BasisLabel() < arN2.BasisLabel();
      else 
         return arN1.SubSysMem() < arN2.SubSysMem(); 
   }
   if (arN1.SubSys() == I_0) return true;
   if (arN2.SubSys() == I_0) return false;

   if (arN1.BasisLabel() != arN2.BasisLabel())
      return arN1.BasisLabel() < arN2.BasisLabel();
   else if (arN1.Znuc() != arN2.Znuc()) 
      return arN1.Znuc() > arN2.Znuc();
   else if (arN1.SubSys() != arN2.SubSys())
      return arN1.SubSys() < arN2.SubSys();
   else 
      return arN1.SubSysMem() < arN2.SubSysMem(); 
   //if (arN1.Znuc() != arN2.Znuc()) 
      //return arN1.Znuc() > arN2.Znuc();
   //else if (arN1.BasisLabel() != arN2.BasisLabel())
      //return arN1.BasisLabel() < arN2.BasisLabel();
   //else if (arN1.SubSys() != arN2.SubSys())
      //return arN1.SubSys() < arN2.SubSys();
   //else 
      //return arN1.SubSysMem() < arN2.SubSysMem(); 
}
/**
* Sort nuclei after
*   sort after (i) SubSys (ii) charge; (iii) basis (iv) SubSysMem
* */
bool NucleiLess2(const Nuclei& arN1, const Nuclei& arN2)
{
   if (arN1.SubSys() != arN2.SubSys()) 
      return arN1.SubSys() < arN2.SubSys();
   else if (arN1.Znuc() != arN2.Znuc()) 
      return arN1.Znuc() > arN2.Znuc();
   else if (arN1.BasisLabel() != arN2.BasisLabel())
      return arN1.BasisLabel() < arN2.BasisLabel();
   else 
      return arN1.SubSysMem() < arN2.SubSysMem(); 
}
/**
* Shift center of charge vector 
* */
void FindCenterOfNuclearCharge(vector<Nuclei>& arNucVec, 
      Vector3D& arConcVec,bool aTest,In aIhit)
{
   In i=I_0; 
   In n_nuc = arNucVec.size();
   bool conti = true;
   Nb conc_x = C_0;
   Nb conc_y = C_0;
   Nb conc_z = C_0;
   Nb q_tot=C_0;
   In i_hit = aIhit;
   if (aTest && n_nuc >0) if (arNucVec[i].SubSys() != i_hit) conti = false;
   //while (i<n_nuc && conti) 
   while (i<n_nuc ) 
   {
      if (conti)
      {
         Nb q = Nb(arNucVec[i].Znuc()); //NB Use Z instead of Q!!!
         q_tot += q;
         //Mout << " nuc conti " << i << " " << arNucVec[i] << endl;
         conc_x += q*arNucVec[i].X();
         conc_y += q*arNucVec[i].Y();
         conc_z += q*arNucVec[i].Z();
      }
      i++;
      if (aTest && n_nuc >i) 
      {
         if (arNucVec[i].SubSys() != i_hit) conti = false;
         else conti = true;
      }
   }
   //Mout << i << " nuclei included in determination of center of nuc charge " 
       //<< q_tot << endl;
   arConcVec.SetX(conc_x/q_tot);
   arConcVec.SetY(conc_y/q_tot);
   arConcVec.SetZ(conc_z/q_tot);
}
/**
* Find center of mass vector
* */
void FindCenterOfMass(vector<Nuclei>& arNucVec, 
      Vector3D& arConcVec)
{
   In i=0; 
   In n_nuc = arNucVec.size();
   Nb conc_x = C_0;
   Nb conc_y = C_0;
   Nb conc_z = C_0;
   Nb m_tot=C_0;
   while (i<n_nuc ) 
   {
      Nb m = arNucVec[i].GetMass();
      m_tot += m;
      conc_x += m*arNucVec[i].X();
      conc_y += m*arNucVec[i].Y();
      conc_z += m*arNucVec[i].Z();
      i++;
   }
   //Mout << i << " nuclei included in determination of center of nuc charge " 
       //<< q_tot << endl;
   arConcVec.SetX(conc_x/m_tot);
   arConcVec.SetY(conc_y/m_tot);
   arConcVec.SetZ(conc_z/m_tot);
}
/**
* Find the heavy atom coordinates for a particular subsystem
* */
void FindHeavyAtom(vector<Nuclei>& arNucVec,Vector3D& ar3, In aSubSys)
{
   Nb q_max = C_0;
   In i_max = I_0;
   for (In i=I_0;i<arNucVec.size();i++) 
   {
      if (arNucVec[i].SubSys() == aSubSys)
      {
         if (arNucVec[i].Znuc() > q_max ) 
         {
            q_max = arNucVec[i].Znuc();
            i_max = i;
         }
      }
   }
   ar3.SetX(arNucVec[i_max].X());
   ar3.SetY(arNucVec[i_max].Y());
   ar3.SetZ(arNucVec[i_max].Z());
   //Mout << " in heavy atom " << ar3 << endl;
}
/**
* Shift all nuclei with the same vector
* */
void ShiftNuclei(vector<Nuclei>& arNucVec, Vector3D& arShiftVec)
{
   In n_nuc = arNucVec.size();
   if (n_nuc <= I_0) return;
   Nb shift_len = arShiftVec.Length();
   if (shift_len < arNucVec[I_0].ShiftLenNeg()) return;
   //Nb e_repuls = RepulsEnergy(arNucVec);
   //if (gPesIoLevel > I_3 ) Mout << " Nuclear Repulsion energy before shift " << e_repuls << endl;
   for (In i=I_0;i<n_nuc;i++) arNucVec[i].Shift(arShiftVec);
   //e_repuls = RepulsEnergy(arNucVec);
   //if (gPesIoLevel > I_3 ) Mout << " Nuclear Repulsion energy after  shift " << e_repuls << endl;
}
/**
* Repulsion between nuclei of a vector of neclei 
* Constant is put to one (atomic units, if structures are in atomic units).
* */
Nb RepulsEnergy(vector<Nuclei>& arNucVec)
{
   In n_nuc = arNucVec.size();
   Nb e_repuls = C_0;
   for (In i=I_0;i<n_nuc;i++) 
      for (In j=I_0;j<i;j++) e_repuls += arNucVec[i].Repuls(arNucVec[j]);
   return e_repuls;
}

/*
* Set Q from label of the form Atom#Number
*/
void Nuclei::SetQfromGeneralLabel(string label)
{
   string::size_type pos = label.find("#",0);
   if (pos != string::npos)
   {
//      label.erase(pos,label.length());
      label.erase(pos);
   }
   SetAtomLabel(label);
   SetQzFromLabel();
}

/**
 * Rotate nuclei in 3D-space.
 *
 * @param aM The rotation matrix.
 **/
void Nuclei::Rotate(const MidasMatrix& aM) 
{
   Nuclei n;
   n.SetX(aM[0][0]*mX+aM[0][1]*mY+aM[0][2]*mZ);
   n.SetY(aM[1][0]*mX+aM[1][1]*mY+aM[1][2]*mZ);
   n.SetZ(aM[2][0]*mX+aM[2][1]*mY+aM[2][2]*mZ);
   mX=n.X();
   mY=n.Y();
   mZ=n.Z();
}

Nb Nuclei::GetMass() const
{
   if(mAnuc == I_0) MIDASERROR("Using nuclei without mass");
   return gAtomicData.GetMass(mZnuc,mAnuc);
}

Nb Nuclei::GetIsotopicWeight() const 
{
   return gAtomicData.GetMass(mZnuc,mAnuc);
}
  
void Nuclei::SetToMostCommonIsotope() 
{
   mAnuc = gAtomicData.MostCommonIsotope(mZnuc);
}
/** *********************************************************************
 *  operator == only checks for some of the properties, i.e. whether it is 
 *  at the same position and has the same atoms
 ********************************************************************** */
bool Nuclei::operator==(const Nuclei& nuc2) const
{
   bool same=true;
   same = (same && (mX-nuc2.mX)<C_I_10_12);
   same = (same && (mY-nuc2.mY)<C_I_10_12);
   same = (same && (mZ-nuc2.mZ)<C_I_10_12);
   same = (same && (mQ-nuc2.mQ)<C_I_10_12);
   same = (same && (mZnuc==nuc2.mZnuc));
   same = (same && (mAnuc==nuc2.mAnuc));
   same = (same && (mAtomLabel==nuc2.mAtomLabel));
   //same = (same && (mI==nuc2.mI));
   /*
   same = (same && (mSubSys==nuc2.mSubSys));
   same = (same && (mSubSysMem==nuc2.mSubSysMem));
   same = (same && (mBasisLabel==nuc2.mBasisLabel));
   same = (same && (mForceNonStandard==nuc2.mForceNonStandard));
   same = (same && (mDigitModest==nuc2.mDigitModest));
   same = (same && (mShiftLenNeg-nuc2.mShiftLenNeg)<C_I_10_12);
   same = (same && (mNucTreatType==nuc2.mNucTreatType));
   */

   return same;
}
/** *********************************************************************
 ********************************************************************** */
ostream& operator<<(ostream& os,const NucTreatType& arNucTreatType)
{
   switch (arNucTreatType)
   {
      case NucTreatType::FROZEN:
      {
         os<< "FROZEN" ;
         break;
      }
      case NucTreatType::ACTIVE:
      {
         os<< "ACTIVE" ;
         break;
      }
      case NucTreatType::INACTIVE:
      {
         os<< "INACTIVE" ;
         break;
      }
      case NucTreatType::CAP:
      {
         os<< "CAP" ;
         break;
      }
      default:
      {
         MIDASERROR("Unknown nuctreattype type");
         break;
      }
   }
   return os;
}

/** *********************************************************************
 ********************************************************************** */
NucTreatType NucTreatTypeFromString(const std::string& arString)
{
   const map<string,NucTreatType> stringtoparam
   {
      {"ACTIVE",NucTreatType::ACTIVE}
      , {"INACTIVE",NucTreatType::INACTIVE}
      , {"FROZEN",NucTreatType::FROZEN}
      , {"CAP",NucTreatType::CAP}
   };

   return midas::input::FindKeyword(stringtoparam, arString);
}
