/**
************************************************************************
* 
* @file                AtomicData.h
*
* Created:             07-01-2006
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for treating atomic nuclei
* 
* Last modified: Sun Jan 08, 2006  12:22AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ATOMICMASSES_H
#define ATOMICMASSES_H

#include<iostream>
using std::ostream;
using std::istream;
#include<string>
using std::string;
#include<map>
using std::map;
#include<vector>
using std::vector;
#include <utility>
using std::pair;
//using std::make_pair;

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"
#include"nuclei/Vector3D.h"


/**
* Class AtomicMasses
* */
class AtomicData
{
   public:
      //! Constructor
      AtomicData(); 

      //! Initialize AtomicData object
      void Init();

      //! GetMass of Z,A
      Nb GetMass(In aZ, In aA);
      
      //! Get isotopic weight Z,A
      Nb GetIsotopicWeight(In aZ, In aA);
      
      //!
      string GetStringFromZ(In aZ); 
      
      //!
      In GetZnucFromString(const std::string& aS);

      //! Get most common isotope
      In MostCommonIsotope(In aZ)  
      {  
         return mMostCommonIsotope[aZ];
      }

   private:
      //! Private member functions
      void InitMasses();   ///< Read In Masses  
      void InitStrings();  ///< Read In String/Int maps 
      
      //! Member data
      std::map<std::pair<In,In>, std::pair<Nb,Nb> > mAtomicMasses;
      std::map<std::string,In>                      mNucMapLabToZnuc;    ///< From Znuc to Label
      std::map<In,std::string>                      mNucMapZnucToLab;    ///< From Znuc to Label
      std::map<In,In>                               mMostCommonIsotope;
      bool                                          mHasBeenInit;
};

#endif 
