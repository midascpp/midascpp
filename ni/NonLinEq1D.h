/**
************************************************************************
* 
* @file                NonLinEq1D.h
*
* Created:             25-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Interface headerfile for namespace NonLinEq1D
* 
* Last modified: man mar 21, 2005  11:50
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NONLINEQ1D_H
#define NONLINEQ1D_H

#include"inc_gen/TypeDefs.h"

/**
* Namespace NonLinEq1D  
* */
namespace NonLinEq1D
{
   bool Bisect(Nb a,Nb b, Nb& c, PointerToFunc1 f, Nb delta, Nb epsn, In MaxIt, In& iter,bool both);
   ///< Simple Bisector method    
   bool Newton(Nb& xp,PointerToFunc1 f, PointerToFunc1 fd, Nb delta, Nb epsn, In MaxIt, In& iter,bool both);
   ///< Newton method    
}

#endif

