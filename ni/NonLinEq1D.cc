/**
************************************************************************
* 
* @file                NonLinEq1D.cc
*
* Created:             25-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Solution of non-lin. eq. of one-variable
* 
* Last modified: man mar 21, 2005  11:50
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Link to Standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ni/NonLinEq1D.h" 
#include "util/Io.h"

/**
* Bisection algorithm for root finding:
* a,b    are endpoints of interval whithin a root lies (thus f(a)*f(b) < * 0). 
* c      middle point stored in c and this is the result on return.
* delta: tolerance with respect to accurate of root 
* epsn:  tolerance with respect to accurate of function(root)
* MaxIt: Max number of iterations
* iter:  returns the actual number of iterations used.
* both:  Controls if both (true) x and f(x) must be within tolerance or only
*        one of them(false).
* */
bool NonLinEq1D::Bisect(Nb a,Nb b, Nb& c, PointerToFunc1 f, Nb delta, Nb epsn, In MaxIt,In& iter,bool both)
{
   Nb u = f(a);                                         ///< Value at left point
   Nb h = b - a;                                        ///< interval

   for (In i=I_1;i<=MaxIt;i++)          
   {
      h*=C_I_2;                                           ///< Reduced interval by factor half.
      c = a+h;                                          ///< New middle point
      Nb w = f(c);                                      ///< Value at Middle point
      // Mout << " a, b, c, u, w, h: " << a << " " << b << " " << c << " " << u << " " << w << " " << h << endl;
      if (both)
      {
         if (fabs(h) < delta && fabs(w) < epsn) 
         {
            iter = i;
            return true;                                      ///< If Interval to small AND value to small return current point.  
         }
      } else
      {
         if (fabs(h) < delta || fabs(w) < epsn) 
         {
            iter = i;
            return true;                                      ///< If Interval to small OR value to small return current point.  
         }
      }
      ((u>C_0 && w<C_0) || (u<C_0 && w>C_0)) ? (b=c): (a=c,u=w);///< Else make new choice. (if f(a)*f(b)<0->b=c, else a=c).
   }
   iter = MaxIt;                                        ///< Give up - return what we have
   return false;
}

/**
* Newtons algorithm for root-finding
* xp:  guess point on input. on output it is the 
* f:  pointer to function
* fd:  pointer to function derivative
* delta: tolerance with respect to accurate of root 
* epsn:  tolerance with respect to accurate of function(root)
* MaxIt: Max number of iterations
* iter:  returns the actual number of iterations used.
* both:  Controls if both x and f(x) must be within tolerance or only
*        one of them
* */
bool NonLinEq1D::Newton(Nb& xp,PointerToFunc1 f, PointerToFunc1 fd, Nb delta, Nb epsn, In MaxIt,In& iter, bool both)
{
   Nb v = f(xp);                                        ///< Start function value.
   Nb xnew = C_0;                                       ///< Storage of running x-estimate
   for (In i=I_1;i<=MaxIt;i++)          
   {
      Nb derv = fd(xp);
      if (!derv)                                        ///< Check for zero derivative/division
      {
         Mout << " Division by zero in Newton! " << endl;
         exit(I_1);
      }

      xnew = xp - v/derv;                               ///< New point
      v = f(xnew);                                      ///< f-value at new point
      if (both)
      {
         if (fabs(xnew-xp) < delta && fabs(v) < epsn) 
         {
            iter = i;
            xp = xnew;
            return true;
         }
      } else
      {
         if (fabs(xnew-xp) < delta || fabs(v) < epsn) 
         {
            iter = i;
            xp = xnew;
            return true;
         }
      }
      xp = xnew;                                        ///< Not good enough? Try once again.
   }
   iter = MaxIt;
   xp = xnew;
   return false;                                         ///< Give up - return what we have
}

