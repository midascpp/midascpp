/**
************************************************************************
* 
* @file                NumInt1D.cc
*
* Created:             23-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing 1D-numerical integration
* 
* Last modified: man mar 21, 2005  11:50
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was smupplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Link to Standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ni/NumInt1D.h"
#include "util/Io.h"

/**
* NumInt1D, constructor
* */
NumInt1D::NumInt1D(Nb a, Nb b, In n, PointerToFunc1 f)
{
   mLower = a;
   mUpper = b;
   mTpz_valid = false; 
   mSmp_valid = false; 
   mIntegrand = f;
   mNorder = n;
}

/**
* Changer boundaries of 1d-numint 
* */
inline void NumInt1D::changebd(Nb a,Nb b)
{
   mLower = a;
   mUpper = b;
   mTpz_valid = false; 
   mSmp_valid = false; 
}

/**
* Changer order of numerical integration.
* */
inline void NumInt1D::changeorder(In n)
{
   mNorder= n;
   mTpz_valid = false; 
   mSmp_valid = false; 
}


/**
 * NumInt1D - the actual integrater.
 */
//void NumInt1D::NumInt(bool simp=true) const // THIS true is not allowed by icc compiler (double default)
void NumInt1D::NumInt(bool simp) const
{
   Nb   h = (mUpper-mLower)/mNorder;
   Nb sum = mIntegrand(mLower)/C_2;
   for (In i=I_1; i<mNorder; i++) sum += mIntegrand(mLower+i*h);
   sum += mIntegrand(mUpper)/C_2;

   mResTpz = sum*h;                                     // mResult of Trapez integration.
   mTpz_valid = true;

   if (simp)
   {
      Nb sum2 = C_0;
      for (In i=I_1; i<=mNorder; i++) sum2 += mIntegrand(mLower+(i-C_I_2)*h);

      mResSmp = (sum+C_2*sum2)*h/C_3;                        // mResult of Simpson integration.
      mSmp_valid = true;
   }
}

Nb NumInt1D::Trapez() const
{
   if (!mTpz_valid) 
   {
      bool b = false; // does not make Simpson.
      this->NumInt(b);
      // Mout << " Now I calculate Trapez " << endl; // Test only - ugly output
   }
   return mResTpz;
}

Nb NumInt1D::Simpson() const
{
   if (!mSmp_valid) 
   { // p is a pointer to an object. p-> gives acces to members.
      bool b = true; // make both Trapez and Simpson.
      this->NumInt(b);
      // Mout << " Now I calculate Simpson" << endl; // Test only - ugly output
   }
   return mResSmp;
}

void NumInt1D::TestPointer(NumInt1D* p,NumInt1D q)
{
   pmemberf simp = &NumInt1D::Simpson;  // Adress of operator working on a qualified class member name. -> pointer to member
   pmemberf trap = &NumInt1D::Trapez; 
   Mout << " Pointer 1 call to simpson" << (q.*simp)() << endl; // q is object. *simp is the function pointed to by simp.
   Mout << " Pointer 1 call to trapez " << (q.*trap)() << endl;
   Mout << " Pointer 2 call to simpson" << (p->*simp)() << endl; // p is a pointer to an object. p-> gives acces to members.
   Mout << " Pointer 2 call to trapez " << (p->*trap)() << endl;
}

