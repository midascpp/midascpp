/**
************************************************************************
* 
* @file                NumInt1D.h 
*
* Created:             23-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   header for NumInt1D class
* 
* Last modified: man mar 21, 2005  11:50
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was smupplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NUMINT1D_H 
#define NUMINT1D_H

#include "inc_gen/TypeDefs.h"

class NumInt1D
{
   private:
      Nb mLower;
      Nb mUpper;
      In mNorder;
      PointerToFunc1 mIntegrand;
      mutable Nb mResTpz;
      mutable Nb mResSmp;
      mutable bool mTpz_valid;
      mutable bool mSmp_valid;
   public:
      NumInt1D(Nb a, Nb b, In n, PointerToFunc1 f);
      In nord() const {return mNorder;}
      Nb lowbd() const {return mLower;}
      Nb upbd() const {return mUpper;}
      Nb Trapez() const; 
      Nb Simpson() const;
      void changebd(Nb,Nb);
      void changeorder(In n);
      void NumInt(bool simp=true) const;
      void TestPointer(NumInt1D* p,NumInt1D q);
};
typedef Nb (NumInt1D::*pmemberf)() const;

#endif 

