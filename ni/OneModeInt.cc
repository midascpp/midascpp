/**
************************************************************************
*
* @file                OneModeInt.cc
*
* Created:             23-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Members for OneMode integral Class calculations etc.
*
* Last modified: Tue Nov 24, 2009  02:38PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <string>
#include <vector>
#include <utility>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "ni/OneModeInt.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/Diag.h"
#include "util/Isums.h"
#include "util/Io.h"
#include "util/Math.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "operator/OneModeOper.h"
#include "operator/StateTransferOneModeOper.h"
#include "operator/DeltaFunctionOneModeOper.h"

/**
 * Constructor from operator and basis 
 *
 * @param apOpDef
 * @param apBasDef
 * @param aStorage
 * @param aSaveUponDecon
**/
OneModeInt::OneModeInt
   (  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   ,  std::string aStorage
   ,  bool aSaveUponDecon
   )
   :  mStorage(aStorage)
   ,  mTheOneModeInt()
   ,  mpOpDef(apOpDef)
   ,  mpBasDef(apBasDef)
   ,  mUseMassScaling(false)
{
   // Sanity check and set accuracy of B-spline integrals
   if (  !apBasDef
      )
   {
      MidasAssert(apOpDef->StateTransferForAll(), "OneModeInt can only work with no basis if state-transfer operators are used for all modes.");
   }
   else
   {
      this->mBsplIntRelAccuracy = apBasDef->GetGaussLegendreRelAccuracy();
   }

   mTheOneModeInt.SaveUponDecon(aSaveUponDecon);
   mOneModeIntName = apOpDef->Name();
   if (  apBasDef
      )
   {
      mOneModeIntName += "_" + apBasDef->GetmBasName();
   }
   else
   {
      mOneModeIntName += "_NoBasis";
   }
   mTheOneModeInt.NewLabel(mOneModeIntName + "_OneModeInt");
   mTheOneModeInt.ChangeStorageTo(aStorage);

   // Check there is not a mode mismatch between operator and basis.
   if (  apBasDef
      && apOpDef->NmodesInOp() != apBasDef->GetmNoModesInBas()
      )
   {
      Mout << " Number of modes defined in operator and basis does not match!" << std::endl;
      Mout << " Number of modes defined in operator: " << apOpDef->NmodesInOp() << std::endl;
      Mout << " Number of modes defined in basis:    " << apBasDef->GetmNoModesInBas() << std::endl;
      MIDASERROR(" Number of modes defined in operator and basis mismatch!");
   }
   mNoModesInInt = apOpDef->NmodesInOp();

   mHoQiOnly = false;
   if (  apBasDef
      && apBasDef->GetmBasSetType() == "PROD-ALLHO"
      && apOpDef->Type() == "SOP-Q^I-ONLY"
      )
   {
      if (  gOperIoLevel > I_9
         )
      {
         Mout << "Calculate HO integrals recursively " << std::endl;
      }
      mHoQiOnly = true;
   }
   //reset to according the storage or not of the combined operators
   //under development. then maybe pass as an argument
   //bool aStoreComOpInt=true;

   mMaxNhigh    = I_0;
   mMaxQi       = I_0;
   mMaxddQi     = I_0;
   mMaxddQQi    = I_0; //for (DDQ) Q^i integrals
   mMaxQiddQ    = I_0; //for Q^i (DDQ) integrals
   mMaxddQQiddQ = I_0; //for (DDQ) Q^i (DDQ) integrals
   // loop modes in op
   In i_mode_add = 0;
   In i_int_add  = 0;
   In n_occ_mode_off = 0; // mbh
   for (LocalModeNr i_op_mode = I_0; i_op_mode < mNoModesInInt; i_op_mode++)
   {
      //Mode-checking
      GlobalModeNr i_g_mode   = apOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = apBasDef ? In(apBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
      In nbas = I_0;

      if (  this->mpOpDef->StateTransferOnly(i_op_mode)
         )
      {
         nbas = this->mpOpDef->StateTransferIJMax(i_op_mode) + I_1;
      }
      else
      {
         if (i_g_mode < I_0 || i_bas_mode < I_0)
         {
            MIDASERROR(" Basis/Operator mode mismatch!");
         }

         nbas = apBasDef->Nbas(i_bas_mode);
      }
      In n_int_pr_oper = nbas*nbas;
      mOneModeIntOccOff.push_back(n_occ_mode_off); // mbh
      n_occ_mode_off += mpOpDef->NrOneModeOpers(i_op_mode); // mbh
      i_int_add += n_int_pr_oper*apOpDef->NrOneModeOpers(i_op_mode);
      i_mode_add += apOpDef->NrOneModeOpers(i_op_mode);

      // find mMaxNhigh, valid only for ho. TODO: check
      if (  apBasDef
         && mHoQiOnly
         )
      {
         if (apBasDef->Higher(i_bas_mode) > mMaxNhigh)
         {
            mMaxNhigh = apBasDef->Higher(i_bas_mode);
         }
      }

      // Find mMaxQi, mMaxddQi, mMaxddQQi, mMaxQiddQ, mMaxddQQiddQ
      In n_max = mpOpDef->GetHeighestQpowForMode(i_op_mode);
      if (n_max > mMaxQi)
      {
         mMaxQi = n_max;
      }
      n_max = mpOpDef->GetHeighestDDpowForMode(i_op_mode);
      if (n_max > mMaxddQi)
      {
         mMaxddQi = n_max;
      }
      n_max = mpOpDef->GetHeighestQpowDDForMode(i_op_mode);
      if (n_max > mMaxQiddQ)
      {
         mMaxQiddQ = n_max;
      }
      n_max = mpOpDef->GetHeighestDDQpowForMode(i_op_mode);
      if (n_max > mMaxddQQi)
      {
         mMaxddQQi = n_max;
      }
      n_max = mpOpDef->GetHeighestDDQpowDDForMode(i_op_mode);
      if (n_max > mMaxddQQiddQ)
      {
         mMaxddQQiddQ = n_max;
      }

      if (gDebug)
      {
         Mout << " i_g_mode " << i_g_mode << " nbas = " << nbas << " nr of int up-to and including this_mode " << i_int_add << std::endl;
      }
   }

   //
   mTotNoOneModeOpers = i_mode_add;
   mTotNoOneModeIntegrals = i_int_add;
   if (mHoQiOnly)
   {
      mTotNoOneModeIntegrals = (1+mMaxQi)*(1+mMaxNhigh)*(1+mMaxNhigh);
      mTotNoOneModeIntegrals += mMaxddQi*(1+mMaxNhigh)*(1+mMaxNhigh);
      mTotNoOneModeIntegrals += mMaxddQQi*(1+mMaxNhigh)*(1+mMaxNhigh);
      mTotNoOneModeIntegrals += mMaxQiddQ*(1+mMaxNhigh)*(1+mMaxNhigh);
      mTotNoOneModeIntegrals += mMaxddQQiddQ*(1+mMaxNhigh)*(1+mMaxNhigh);
   }

   //
   GenIntOffSets();

   if (gDebug)
   {
      Mout << std::endl << " One-mode Integrals per mode and integral offsets: " << std::endl;
      for (LocalModeNr i_op_mode = I_0; i_op_mode < mNoModesInInt; i_op_mode++)
      {
         In n_oper = apOpDef->NrOneModeOpers(i_op_mode);

         for (LocalOperNr i_oper = I_0; i_oper < n_oper; i_oper++)
         {
            GlobalOperNr g_op_nr = apOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper);
            Mout << "  Mode Q"         << i_op_mode
                 << ", operator "      << i_oper
                 << ": "               << gOperatorDefs.GetOneModeOperDescription(g_op_nr)
                 << ", (i_int_add: "   << mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second << ")"
                 << std::endl;
         }
      }
   }

   // Only valid for Ho basis
   if (  apBasDef
      && apBasDef->GetmBasSetType() == "PROD-ALLHO"
      )
   {
      if (  mHoQiOnly
         )
      {
         mUseMassScaling = true;
      }

      if (  mpOpDef->ScaledPot()
         && apBasDef->GetmUsingScalingFreqs()
         )
      {
         bool test = true;
         // Check all masses are unity
         for (In i_bas_mode = I_0; i_bas_mode < mNoModesInInt; i_bas_mode++)
         {
            Nb mass = apBasDef->GetMass(i_bas_mode);
            test = test && (std::fabs(mass - C_1) < C_NB_EPSILON * C_10);
            if (!test)
            {
               break;
            }
         }

         mUseMassScaling = test;
      }
   }
}

/**
 * Generate address of integral of an operator defined my (local_mode,global_op)
 * Note ddq comes before q integrals! (alphabetic sorting).
**/
void OneModeInt::GenIntOffSets()
{
   In offset = I_0;
   if (mHoQiOnly)
   {
      for (LocalModeNr i = I_0; i < mNoModesInInt; i++)
      {
         for (LocalOperNr i_oper=0;i_oper<mpOpDef->NrOneModeOpers(i);i_oper++)
         {
            GlobalOperNr op_nr = mpOpDef->GetOneModeOperGlobalNr(i,i_oper);
            bool added_new_int = true;
            for(In j = I_0; j < mNoModesInInt; j++)
            {
               pair<map<pair<LocalModeNr, GlobalOperNr>, In>::iterator, bool> is_new = mOffSetMap.insert(std::make_pair(std::make_pair(j,op_nr), offset));
               if(!is_new.second)
               {
                  added_new_int = false;
                  break;
               }
            }
            if(added_new_int)
               offset += (1+mMaxNhigh)*(1+mMaxNhigh);
         }
      }
   }
   else
   {
      for(LocalModeNr i = I_0; i < mNoModesInInt; i++)
      {
         for (LocalOperNr i_oper=0;i_oper<mpOpDef->NrOneModeOpers(i);i_oper++)
         {
            GlobalOperNr op_nr = mpOpDef->GetOneModeOperGlobalNr(i,i_oper);
            //pair<map<pair<LocalModeNr, GlobalOperNr>, In>::iterator, bool> is_new = mOffSetMap.insert(std::make_pair(std::make_pair(i,op_nr), offset));
            mOffSetMap.insert(std::make_pair(std::make_pair(i,op_nr), offset));
            GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i);
            LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
            In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(i) + I_1;
            offset += nbas*nbas;
         }
      }
   }
}

/**
 * Get the integrals from data from a previous calculation
**/
bool OneModeInt::RestartInt()
{
   string tmp_storage = "OnDisc";
   mTheOneModeInt.SetNewSize(I_0, false);        // Resize, do NOT do anything.
   mTheOneModeInt.ChangeStorageTo(tmp_storage);  // Change storage to on disc.
   bool ok = true;
   // Check info on DataContainer.
   if (ok)
   {
      mTheOneModeInt.SetNewSize(mTotNoOneModeIntegrals, false);  // Resize - means assume the data are on disc.
      mTheOneModeInt.ChangeStorageTo(mStorage);       // Change storage to standard (nothing done if on disc).
   }
   return ok;
}

/**
 * Calculate the integrals. On request also the Coriolis integrals are calculated
**/
void OneModeInt::CalcInt()
{
   // Resize, do NOT keep old integrals.
   mTheOneModeInt.SetNewSize(mTotNoOneModeIntegrals, false);
   ZeroInt();

   // If elementary operator, no primitive integrals.
   if (mpOpDef->GetIsElementaryOperator())
   {
      if (gOperIoLevel > I_13)
      {
         Mout << " Elementary operator: Integral calculation skipped " << std::endl;
      }
      return;
   }

   In modes_in_loop = mNoModesInInt;
   // mHoQiOnly means that for all modes all integrals are not there - only a list of Q^i integrals and on requesting the integrals, they are multiplied by the appropriate factors.
   if (mHoQiOnly)
   {
      modes_in_loop = I_1;
   }

   if (gOperIoLevel > I_5)
   {
      Mout << std::endl << " **** Calculate one-mode integrals for " << modes_in_loop << " modes **** " << std::endl;
   }

   bool calc_cor_int = false;
   bool pure_polynomial_oper_set = true;
   // Loop over one-mode operators for this mode.
   for (LocalModeNr i_op_mode = I_0; i_op_mode < modes_in_loop; i_op_mode++)
   {
      if (  this->mpOpDef->StateTransferOnly(i_op_mode)
         )
      {
         this->SetStateTransferIntegrals(i_op_mode);
         continue;
      }
      else if  (  !mpBasDef
               )
      {
         MIDASERROR("OneModeInt needs a BasDef if not using state transfer operators!");
      }

      // Mode-checking
      GlobalModeNr i_g_mode = I_0;
      LocalModeNr i_bas_mode = I_0;
      if (gDebug || gOperIoLevel > I_6)
      {
         Mout << " All operators are of type Q^i: " << mHoQiOnly << std::endl;
      }
      if (!mHoQiOnly)
      {
         i_g_mode = mpOpDef->GetGlobalModeNr(i_op_mode);
         i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      }
      // Size info
      In nbas = mMaxNhigh + I_1;
      if (!mHoQiOnly)
      {
         nbas = mpBasDef->Nbas(i_bas_mode);
      }
      bool ho_bas = false;
      bool gaus_bas = false;
      bool bspl_bas = false;

      if (mpBasDef->OneModeType(i_bas_mode) == "HO") //
      {
         if (gDebug || gOperIoLevel > I_13)
         {
            Mout << " Harmonic oscillator integrals calculated for mode "
                 << gOperatorDefs.GetModeLabel(i_g_mode) << std::endl;
         }
         ho_bas = true;
      }
      else if (mpBasDef->OneModeType(i_bas_mode) == "Gauss") // do the integrals over gaussians
      {
         if (gDebug || gOperIoLevel > I_13)
         {
            Mout << " Integrals over localized gaussian basis calculated for mode "
                 << gOperatorDefs.GetModeLabel(i_g_mode) << std::endl;
         }
         gaus_bas = true;
      }
      else if (mpBasDef->OneModeType(i_bas_mode) == "Bsplines") // do the integrals over B-splines
      {
         if (gDebug || gOperIoLevel > I_13)
         {
            Mout << " Integrals over B-splines calculated for mode "
                 << gOperatorDefs.GetModeLabel(i_g_mode) << std::endl;
         }
         bspl_bas = true;
      }
      else
      {
         Mout << " Integrals for basis type " << mpBasDef->OneModeType(i_bas_mode)
              << " for mode " << gOperatorDefs.GetModeLabel(i_g_mode)
              << " is not supported yet " << std::endl;

         MIDASERROR(" Unrecognized basis set in OneModeInt::CalcInt ");
      }

      bool calc_cor_int = (mMaxddQQi + mMaxQiddQ + mMaxddQQiddQ > I_0);

      if (calc_cor_int && gOperIoLevel > I_4)
      {
         Mout << " Coriolis integrals are calculated and added to the container " << endl;
      }

      bool pure_polynomial_oper_set = true;
      // Check if the entire operator set for this mode is of type polynomial:
      for (LocalOperNr j_oper = I_0; j_oper < mpOpDef->NrOneModeOpers(i_op_mode); ++j_oper)
      {
         if (  !gOperatorDefs.GetOneModeOper(mpOpDef->GetOneModeOperGlobalNr(i_op_mode, j_oper))->IsPoly()
            || gOperatorDefs.GetmBsplineGenIntEval()
            )
         {
            pure_polynomial_oper_set = false;
            break;
         }
      }

      if (gOperIoLevel > I_5 && pure_polynomial_oper_set)
      {
         Mout << " One-mode operator integrals for mode Q" << i_op_mode << " will be calculated the standard way, as all one-mode operators are of type polynomial" << std::endl;
      }
      else if (gOperIoLevel > I_5 && !pure_polynomial_oper_set)
      {
         Mout << " One-mode operator integrals for mode Q" << i_op_mode << " will be calculated the general way, as not all one-mode operators are of type polynomial" << std::endl;
      }

      // If the entire operator set for the current mode is of type polynomial we can chose the standard (easy) way of doing integrals, otherwise we have to apply the more expensive general evaluation process
      if (pure_polynomial_oper_set && !gOperatorDefs.GetmBsplineGenIntEval())
      {
         bool qi_method = true;
         // Check to see if our operator can be calculated the easy or hard way...
         for (LocalOperNr i_oper = I_0; i_oper < mpOpDef->NrOneModeOpers(i_op_mode); ++i_oper)
         {
            if (!gOperatorDefs.GetOneModeOper(mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper))->IsPoly())
            {
               Mout << "GetGenInit true for " << gOperatorDefs.GetOneModeOperDescription(mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper)) << std::endl;
               qi_method = false;
               break;
            }
         }

         if (!qi_method)
         {
            MIDASERROR("Something wrong with oper set");
         }

         // Do Q^i integrals all at one, first find Q^n_max and then calculate all.
         In n_max = mMaxQi;
         if (!mHoQiOnly)
         {
            n_max = mpOpDef->GetHeighestQpowForMode(i_op_mode);
         }
         if (ho_bas)
         {
            HoqiInt(i_op_mode, nbas, n_max, i_bas_mode);
         }
         else if (gaus_bas)
         {
            GaussqiInt(i_op_mode, nbas, n_max, i_bas_mode);
         }
         else if (bspl_bas)
         {
            BsplqiInt(i_op_mode, nbas, n_max, i_bas_mode);
         }

         // Do (DDQ)^i integrals all at one, first find (DDQ)^n_max and then calculate all.
         n_max = mMaxddQi;
         if (!mHoQiOnly)
         {
            n_max = mpOpDef->GetHeighestDDpowForMode(i_op_mode);
         }
         if (ho_bas)
         {
            HoqiInt(i_op_mode, nbas, n_max, i_bas_mode, true);
         }
         else if (gaus_bas)
         {
            GaussqiInt(i_op_mode, nbas, n_max, i_bas_mode, true);
         }
         else if (bspl_bas)
         {
            BsplqiInt(i_op_mode, nbas, n_max, i_bas_mode, true);
         }

         if(calc_cor_int)
         {
            // Do Q^i (DDQ) integrals all at one, first find n_max and then calculate all.
            n_max = mMaxQiddQ;
            if (!mHoQiOnly)
            {
               n_max = mpOpDef->GetHeighestQpowDDForMode(i_op_mode);
            }
            if (ho_bas)
               HocorInt(i_op_mode,nbas,n_max,i_bas_mode,true);
            else if (gaus_bas)
               GausscorInt(i_op_mode,nbas,n_max,i_bas_mode,true);
            else if (bspl_bas)
               BsplcorInt(i_op_mode,nbas,n_max,i_bas_mode,true);

            // Do (DDQ) Q^i integrals all at one, first find n_max and then calculate all.
            n_max = mMaxddQQi;
            if (!mHoQiOnly)
            {
               n_max = mpOpDef->GetHeighestDDQpowForMode(i_op_mode);
            }
            if (ho_bas)
               HocorInt(i_op_mode,nbas,n_max,i_bas_mode,false,true);
            else if (gaus_bas)
               GausscorInt(i_op_mode,nbas,n_max,i_bas_mode,false,true);
            else if (bspl_bas)
               BsplcorInt(i_op_mode,nbas,n_max,i_bas_mode,false,true);

            // Do (DDQ) Q^i DDQ integrals all at one, first find n_max and then calculate all.
            n_max = mMaxddQQiddQ;
            if (!mHoQiOnly)
            {
               n_max = mpOpDef->GetHeighestDDQpowDDForMode(i_op_mode);
            }
            if (ho_bas)
               HocorInt(i_op_mode,nbas,n_max,i_bas_mode,false,false,true);
            else if (gaus_bas)
               GausscorInt(i_op_mode,nbas,n_max,i_bas_mode,false,false,true);
            else if (bspl_bas)
               BsplcorInt(i_op_mode,nbas,n_max,i_bas_mode,false,false,true);
         }
      }
      else
      {
         if (ho_bas || gaus_bas)
         {
            MIDASERROR("Integrals for one-mode operators cannot be calculated with gaussian or harmonic basis");
         }

         for (LocalOperNr i_oper = I_0; i_oper < mpOpDef->NrOneModeOpers(i_op_mode); ++i_oper)
         {
            GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper);
            const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(g_op_nr);

            BsplGenInt(i_op_mode, nbas, i_bas_mode, this_oper, g_op_nr);
         }
      }
   }

   // Do a numeric check of the integrals:
   if(gDebug && pure_polynomial_oper_set)
   {
      EvalSos();
   }

   // Print all integral information:
   if (gDebug)
   {
      PrintInt();
   }

   // Print collected sums of relevant integral information:
   if (gOperIoLevel > I_6 || gDebug)
   {
      Mout << std::endl;
      Mout << " Norm of all one-mode integrals is:                       " << mTheOneModeInt.Norm() << std::endl;
      Mout << " Sum-over-elements of all one-mode integrals is:          " << mTheOneModeInt.SumEle() << std::endl;
      Mout << " Absolute sum-over-elements of all one-mode integrals is: " << mTheOneModeInt.SumAbsEle() << std::endl;
   }
}

/**
 * Get the integrals for a specific operator mode and an operator number
**/
void OneModeInt::GetInt
   (  const LocalModeNr arOperMode
   ,  const LocalOperNr arOper
   ,  MidasMatrix& aM
   ,  bool aNoScale
   )  const
{
   //check if the integral is a combined one..
   GlobalModeNr i_g_mode = mpOpDef->GetGlobalModeNr(arOperMode);
   LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
   In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(arOperMode) + I_1;
   In nbas2 = nbas*nbas;
   if (nbas != aM.Ncols())
   {
      MIDASERROR( " Size mismatch in OneModeInt::GetInt");
   }

   //
   GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(arOperMode, arOper);

   if (  mHoQiOnly
      )
   {
      MidasAssert(mpBasDef, "mpBasDef must be set for mHoQiOnly");
      In ipow=0;
      bool deriv = false;
      bool cor_two_deriv = false;
      bool cor_right_deriv = false;
      bool cor_left_deriv = false;
      bool is_cor_int_req;
      GetHoInfo(g_op_nr,ipow,deriv,cor_two_deriv,cor_left_deriv,cor_right_deriv,is_cor_int_req);
      In add = mOffSetMap.find(std::make_pair(arOperMode,g_op_nr))->second;
      //Mout << " for oper: " << oper_name << " address is: " << add << endl << endl;
      mTheOneModeInt.DataIo(IO_GET,aM,nbas2,nbas,nbas,add,I_1,false,C_0,1+mMaxNhigh);
      //Mout << " integral before scaling: " << endl;
      //Mout << aM << endl;
      if (aNoScale) return;
      Nb mass = mpBasDef->GetMass(i_bas_mode);
      Nb scal = mpBasDef->GetOmeg(i_bas_mode);
      Nb omeg_sav=scal;
      if (!deriv && mpOpDef->ScaledPot()) scal/=pow(mpOpDef->ScaleFactorForMode(arOperMode),C_2);
      Nb scale_fact1 = sqrt(mass*scal);
      if (!deriv) scale_fact1 = C_1/scale_fact1;
      Nb scale_fact = C_1;
      for (In i = I_0; i < ipow; i++)
      {
         scale_fact *= scale_fact1;
      }
      if ((deriv || mUseMassScaling) && !is_cor_int_req)
      {
         aM.Scale(scale_fact);
      }
      else if (is_cor_int_req)
      {
         if (cor_two_deriv)
         {
            scale_fact*=(mass*omeg_sav);
            aM.Scale(scale_fact);
         }
         else if (cor_left_deriv || cor_right_deriv)
         {
            scale_fact *= sqrt(mass*omeg_sav);
            aM.Scale(scale_fact);
         }
      }
   }
   else
   {
      In add = mOffSetMap.find(std::make_pair(arOperMode, g_op_nr))->second;
      mTheOneModeInt.DataIo(IO_GET, aM, nbas2, nbas, nbas, add);
   }

   if (gDebug)
   {
      Mout << " Integrals retrieved in GetInt: " << std::endl;
      Mout << aM << std::endl;
   }
}

/**
 * Get the integrals for a specific operator mode,operator number,
 * and left and right indeces.
**/
void OneModeInt::GetInt
   (  const LocalModeNr arOperMode
   ,  const LocalOperNr arOper
   ,  const In& arLeftIdx
   ,  const In& arRightIdx
   ,  Nb& aIntegral
   ,  bool aNoScale
   )  const
{
   //check if the integral is a combined one
   GlobalModeNr i_g_mode = mpOpDef->GetGlobalModeNr(arOperMode);
   LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
   In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(arOperMode) + I_1;
   if (nbas < arRightIdx)
   {
      MIDASERROR( " Size mismatch in OneModeInt::GetInt a Nb");
   }
   if (nbas < arLeftIdx)
   {
      MIDASERROR( " Size mismatch in OneModeInt::GetInt a Nb");
   }

   //determines if the required integral has to be calculated "on the fly"
   GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(arOperMode, arOper);
   if (  mHoQiOnly
      )
   {
      MidasAssert(mpBasDef, "mpBasDef must be set for mHoQiOnly");

      const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(g_op_nr);
      In ipow = I_0;
      bool deriv = false;
      bool cor_two_deriv = false;
      bool cor_left_deriv = false;
      bool cor_right_deriv = false;
      bool is_cor_int_req = false;
      GetHoInfo(g_op_nr,ipow,deriv,cor_two_deriv,cor_left_deriv,cor_right_deriv,is_cor_int_req);
      // Now found adress of (mode,oper) integral block.
      In add = mOffSetMap.find(std::make_pair(arOperMode,g_op_nr))->second;
      add += (mMaxNhigh + 1)*arLeftIdx + arRightIdx;

      mTheOneModeInt.DataIo(IO_GET,add,aIntegral);
      if (aNoScale) return;
      //are the coriolis integrals required?
      is_cor_int_req = (cor_two_deriv || cor_left_deriv || cor_right_deriv);
      Nb mass = mpBasDef->GetMass(i_bas_mode);
      Nb scal = mpBasDef->GetOmeg(i_bas_mode);
      Nb omeg_sav=scal;
      if (!deriv && mpOpDef->ScaledPot()) scal/=pow(mpOpDef->ScaleFactorForMode(arOperMode),C_2);
      Nb scale_fact1 = sqrt(mass*scal);
      if (!deriv) scale_fact1 = C_1/scale_fact1;
      Nb scale_fact = C_1;
      for (In i=0;i<ipow;i++) scale_fact*=scale_fact1;
      if ((deriv || mUseMassScaling) && !is_cor_int_req)
      {
         aIntegral*=scale_fact;
      }
      else if (is_cor_int_req)
      {
         if (cor_two_deriv)
         {
            scale_fact*=(mass*omeg_sav);
            aIntegral*=scale_fact;
         }
         else if (cor_left_deriv || cor_right_deriv)
         {
            scale_fact*=sqrt(mass*omeg_sav);
            aIntegral*=scale_fact;
         }
      }
   }
   else
   {
      In add = mOffSetMap.find(std::make_pair(arOperMode, g_op_nr))->second;
      add += nbas*arLeftIdx + arRightIdx;
      mTheOneModeInt.DataIo(IO_GET, add, aIntegral);
   }
   //Mout << " integrals retrieved in GetInt: " << endl;
   //Mout << aIntegral << endl;
}
/**
* Calculate the HO Q^i integrals for a particular aOperMode
* Includes both Q^i and (DDQ)^i - the later requires aDeriv = true, default is false.
* Coriolis integrals q*ddq and ddq*q  are calculated by aDeriv,aCoriolis = (f,t) and (t,t) respectively.
* */
void OneModeInt::HoqiInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool aDeriv
   ,  bool aCoriolis
   )
{
   MidasAssert(mpBasDef, "BasDef needs to be set for OneModeInt::HoqiInt");

   LocalModeNr i_op_mode=aOperMode;
   if (gDebug || gOperIoLevel > I_13)
   {
      Mout << " Harmonic oscillator integrals";
      if (!aCoriolis && !aDeriv) Mout << " Q^i";
      if (!aCoriolis && aDeriv) Mout << " (DDQ)^i";
      if (aCoriolis && aDeriv ) Mout << " coriolis integrals (DDQ) Q";
      if (aCoriolis && !aDeriv ) Mout << " coriolis integrals Q (DDQ)";
      Mout << " calculated for oper mode nr. " << aOperMode << std::endl;
   }
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In n_max = aNmax;
   // Scaling info. UPDATE ACCORDING TO INPUT
   Nb mass = mpBasDef->GetMass(aBasMode);
   Nb scal = mpBasDef->GetOmeg(aBasMode);
   if (!aDeriv && mpOpDef->ScaledPot()) scal/=pow(mpOpDef->ScaleFactorForMode(i_op_mode),C_2);
   Nb scale_fact1 = C_1;
   if (!mHoQiOnly && !aCoriolis)
   {
      scale_fact1 = sqrt(mass*scal);
      if (!aDeriv) scale_fact1 = C_1/scale_fact1;
   }
   if (gDebug)
   {
      Mout << " mass:       " << mass        << std::endl;
      Mout << " scal:       " << scal        << std::endl;
      Mout << " scale_fact: " << scale_fact1 << std::endl;
   }
   if (gDebug)
   {
      Mout << " Harmonic oscillator integrals calculated for upto n_max = " << n_max << " and nbas= " << nbas << std::endl;
   }

   MidasMatrix intim2(nbas);  // integrals for Q^(n-2)/(DDQ)^(n-2), init som unit matrix
   intim2.Zero();
   In der_pow = I_0;
   In pow = I_0;
   for (In i1 = I_0; i1 < nbas; i1++)
   {
      intim2[i1][i1] = C_1;
   }
   if (gDebug)
   {
      Mout << "HoqiInt: " << setw(10) << gOperatorDefs.AddOneModeOper(pow,der_pow) << " mode " << i_op_mode << " norm: " << intim2.Norm() << " sumele: " << intim2.SumEle() << std::endl;

      Mout << gOperatorDefs.AddOneModeOper(pow, der_pow) << std::endl << intim2 << std::endl;
   }

   In i_add;
   GlobalOperNr g_op_nr;
   if (!aDeriv && !aCoriolis)
   {
      if (gOperIoLevel > I_5)
      {
         Mout << " The current set of one-mode operators for mode Q" << i_op_mode << ": " << std::endl;
         for (In i = I_0; i < gOperatorDefs.GetNrOfOneModeOpers(); ++i)
         {
            Mout << "  Operator " << i << " = " << gOperatorDefs.GetOneModeOperDescription(i) << std::endl;
         }
      }

      g_op_nr = gOperatorDefs.AddOneModeOper(I_0);
      i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

      if (gDebug)
      {
         Mout << " g_op_nr " << g_op_nr << " operator "<< gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
      }
      mTheOneModeInt.DataIo(IO_PUT, intim2, nbas2, nbas, nbas, i_add);
   }

   //
   if (n_max < I_1)
   {
      return;
   }

   // integrals for Q^(n-1)/(DDQ)^(-1), init som q matrix/ddq matrix
   MidasMatrix intim1(nbas);
   intim1.Zero();
   if (!aDeriv)
   {
      for (In i1 = I_1; i1 < nbas; i1++)
      {
         intim1[i1][i1 - I_1] = std::sqrt(i1/C_2);
      }
   }
   if (aDeriv)
   {
      for (In i1 = I_1; i1 < nbas; i1++)
      {
         intim1[i1][i1 - I_1] = -std::sqrt(i1/C_2);
      }
   }
   for (In i1 = I_1; i1 < nbas; i1++)
   {
      intim1[i1 - I_1][i1] = std::sqrt(i1/C_2);
   }

   if (!mHoQiOnly)
   {
      intim1.Scale(scale_fact1);
   }
   if (aDeriv)
   {
      der_pow = I_1;
   }
   if (!aDeriv)
   {
      pow = I_1;
   }

   //
   g_op_nr = gOperatorDefs.AddOneModeOper(pow, der_pow);
   i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

   if (gDebug)
   {
      Mout << " g_oper_nr " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
   }

   if (!aCoriolis)
   {
      mTheOneModeInt.DataIo(IO_PUT, intim1, nbas2, nbas, nbas, i_add);
   }

   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "HoqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intim1.Norm() << " sumele: " << intim1.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intim1 << std::endl;
   }

   if (n_max<2) return;
   MidasMatrix inti(nbas);    // integrals for Q^n/(DDQ)^n,

   // Fill into n=0 in intim2 and store on container.
   // Fill into n=1 in intim1 and store on container.
   // Use recursive formulaes to generate the rest of the integrals.

   bool l_der = false;
   if (aCoriolis&&aDeriv)  der_pow = I_1;//oper = "ddqq";
   if (aCoriolis&&!aDeriv) l_der=true;
   for (In i_q = 2;i_q <= n_max;i_q++)
   {
      if (gDebug)
      {
         Mout << " calculate integrals for i_q = " << i_q << " of " << n_max << std::endl;
      }

      if ((!aCoriolis&&!aDeriv) || (aCoriolis&&aDeriv))  // Q^i + in recurs
      {
         for (In i1 = 1;i1<nbas;i1++)
            for (In i2 = 1;i2<nbas;i2++)
            {
               inti[i1][i2] = sqrt(i2/C_2)*intim1[i1][i2-1];
               inti[i1][i2] +=  sqrt(i1/C_2)*intim1[i1-1][i2];
               inti[i1][i2] +=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i2];
            }
         In i10=0;
         for (In i2 = 1;i2<nbas;i2++)
         {
            inti[i10][i2] = sqrt(i2/C_2)*intim1[i10][i2-1];
            inti[i10][i2] +=  scale_fact1*((i_q-1)/C_2)*intim2[i10][i2];
         }
         In i20=0;
         for (In i1 = 1;i1<nbas;i1++)
         {
            inti[i1][i20] =  sqrt(i1/C_2)*intim1[i1-1][i20];
            inti[i1][i20] +=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i20];
         }
         inti[i10][i20] =  scale_fact1*((i_q-1)/C_2)*intim2[i10][i20];
      }
      else if ((!aCoriolis && aDeriv) || (aCoriolis&&!aDeriv)) // (DDQ)^i - in recurs
      {
         for (In i1 = 1;i1<nbas;i1++)
            for (In i2 = 1;i2<nbas;i2++)
            {
               inti[i1][i2] = sqrt(i2/C_2)*intim1[i1][i2-1];
               inti[i1][i2] -=  sqrt(i1/C_2)*intim1[i1-1][i2];
               inti[i1][i2] -=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i2];
            }
         In i10=0;
         for (In i2 = 1;i2<nbas;i2++)
         {
            inti[i10][i2] = sqrt(i2/C_2)*intim1[i10][i2-1];
            inti[i10][i2] -=  scale_fact1*((i_q-1)/C_2)*intim2[i10][i2];
         }
         In i20=0;
         for (In i1 = 1;i1<nbas;i1++)
         {
            inti[i1][i20] =  -sqrt(i1/C_2)*intim1[i1-1][i20];
            inti[i1][i20] -=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i20];
         }
         inti[i10][i20] =  -scale_fact1*((i_q-1)/C_2)*intim2[i10][i20];
      }
      else
      {
         MIDASERROR( " OneModeInt::HoqiInt should have found something by now  .... ");
      }

      if (!mHoQiOnly)
      {
         inti.Scale(scale_fact1);
      }

      // Store integrals on datacontainer.
      if (!aDeriv) pow=i_q;
      if (aDeriv) der_pow=i_q;
      g_op_nr = gOperatorDefs.AddOneModeOper(pow,der_pow,l_der);
      i_add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
      mTheOneModeInt.DataIo(IO_PUT,inti,nbas2,nbas,nbas,i_add);

      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << " g_oper_nr " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "HoqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << inti.Norm() << " sumele: " << inti.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << inti << std::endl;
      }

      // prepare for next round.
      if (i_q < n_max)
      {
         intim2 = intim1;
         intim1 = inti;
      }
   }
  return;
}

/**
* Calculate the HO general Coriolis integrals for a particular aOperMode
* Includes the following integrals:
* Q^i (DDQ): with is_qddq=true
* Q^i (DDQ)^2 with is_qddqsq=true
* (DDQ) Q^i with is_ddqq=true
**/
void OneModeInt::HocorInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool is_qddq
   ,  bool is_ddqq
   ,  bool is_ddqqddq
   )
{
   MidasAssert(mpBasDef, "BasDef needs to be set for OneModeInt::HocorInt");

   LocalModeNr i_op_mode=aOperMode;
   Mout << " Harmonic oscillator Coriolis integrals";
   if (is_qddq)    Mout << " Q^i (DDQ) ";
   if (is_ddqq)  Mout << " (DDQ) Q^i ";
   if (is_ddqqddq)    Mout << " (DDQ) Q^i (DDQ) ";
   Mout << " calculated for oper mode nr. " << aOperMode <<endl;
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In n_max = aNmax;
   //check n_max is at least=1. Otherwise no new integras are calculated
   if (n_max<=0)
   {
      Mout << " no new integrals are calculated in HocorInt on return " << endl;
      return;
   }
   // Scaling info. UPDATE ACCORDING TO INPUT
   Nb mass = mpBasDef->GetMass(aBasMode);
   Nb scal = mpBasDef->GetOmeg(aBasMode);
   Nb omeg_sav = scal;
   if (mpOpDef->ScaledPot()) scal/=pow(mpOpDef->ScaleFactorForMode(i_op_mode),C_2);
   Nb scale_fact1 = C_1;
   Nb scale_fact2 = C_1;
   if (!mHoQiOnly)
   {
      scale_fact1 = C_1/sqrt(mass*scal);
      scale_fact2 = sqrt(mass*omeg_sav);
      //if(is_ddqqddq) scale_fact2*=scale_fact2;
   }
   if (gDebug) Mout << " mass:      " << mass        << endl;
   if (gDebug) Mout << " scal:      " << scal        << endl;
   if (gDebug) Mout << " scale_fact1 " << scale_fact1 << endl;
   if (gDebug) Mout << " scale_fact2 " << scale_fact2 << endl;

   if (gDebug) Mout << " Harmonic oscillator Coriolis integrals calculated for upto n_max = "
       << n_max << " and nbas= " << nbas << endl;

   MidasMatrix intim2(nbas);  //integrals for Q^(n-2)/(DDQ)^(n-2), init som unit matrix
   MidasMatrix intim2_sav(nbas);  //integrals for Q^(n-2)/(DDQ)^(n-2), init som unit matrix
   intim2.Zero();
   intim2_sav.Zero();
   GlobalOperNr g_op_nr = gOperatorDefs.AddOneModeOper(I_0);
   for (In i1 = 0;i1<nbas;i1++) intim2[i1][i1]=C_1;
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "HocorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intim2.Norm() << " sumele: " << intim2.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intim2 << std::endl;
   }

   //if (n_max<1) return;
   MidasMatrix intim1(nbas);  // integrals for Q^(n-1)/(DDQ)^(-1), init som q matrix/ddq matrix
   intim1.Zero();
   for (In i1 = 1;i1<nbas;i1++) intim1[i1][i1-1]=-sqrt(i1/C_2);
   for (In i1 = 1;i1<nbas;i1++) intim1[i1-1][i1]=sqrt(i1/C_2);

   if (!mHoQiOnly) intim1.Scale(scale_fact2);
   g_op_nr = gOperatorDefs.AddOneModeOper(I_0,I_1);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << " g_oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl;

      Mout << "HocorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intim1.Norm() << " sumele: " << intim1.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intim1 << std::endl;
   }

   MidasMatrix inti(nbas);    // integrals for Q^n/(DDQ)^n,
   inti.Zero();
   // Fill into n=0 in intim2 and store on container.
   // Fill into n=1 in intim1 and store on container.
   if(!is_ddqqddq)
   {
      intim2_sav=intim2;
      intim2=intim1;
   }
   else if (is_ddqqddq)
   //then strore the second derivative in intim2
   {
      if (gDebug) Mout << " calculate intermediate (DDQ)^2 integrals " << endl;
      for (In i1 = 1;i1<nbas;i1++)
         for (In i2 = 1;i2<nbas;i2++)
         {
            inti[i1][i2] = sqrt(i2/C_2)*intim1[i1][i2-1];
            inti[i1][i2] -=  sqrt(i1/C_2)*intim1[i1-1][i2];
            inti[i1][i2] -=  scale_fact2*C_I_2*intim2[i1][i2];
         }
      In i10=0;
      for (In i2 = 1;i2<nbas;i2++)
      {
         inti[i10][i2] = sqrt(i2/C_2)*intim1[i10][i2-1];
         inti[i10][i2] -=  scale_fact2*C_I_2*intim2[i10][i2];
      }
      In i20=0;
      for (In i1 = 1;i1<nbas;i1++)
      {
         inti[i1][i20] =  -sqrt(i1/C_2)*intim1[i1-1][i20];
         inti[i1][i20] -=  scale_fact2*C_I_2*intim2[i1][i20];
      }
      inti[i10][i20] =  -scale_fact2*C_I_2*intim2[i10][i20];
      if (!mHoQiOnly)
         inti.Scale(scale_fact2);
      intim2_sav=intim2;
      intim2=inti;
   }

   if (is_ddqqddq) scale_fact2*=scale_fact2;
   Nb fact=C_I_2;
   if(is_ddqq) fact=-C_I_2;
   else if (is_ddqqddq) fact=C_0;
   intim1.Zero();
   //Fill into n=1 in intim1 and store on container.
   for (In i1 = 1;i1<nbas;i1++)
   {
      for (In i2 = 1;i2<nbas;i2++)
      {
         intim1[i1][i2]  =  sqrt(i2/C_2)*intim2[i1][i2-1];
         intim1[i1][i2] +=  sqrt(i1/C_2)*intim2[i1-1][i2];
         intim1[i1][i2] -=  fact*scale_fact2*intim2_sav[i1][i2];
      }
   }
   In i10=0;
   for (In i2 = 1;i2<nbas;i2++)
   {
      intim1[i10][i2]  =  sqrt(i2/C_2)*intim2[i10][i2-1];
      intim1[i10][i2] -=  fact*scale_fact2*intim2_sav[i10][i2];
   }
   In i20=0;
   for (In i1 = 1;i1<nbas;i1++)
   {
      intim1[i1][i20]  =  sqrt(i1/C_2)*intim2[i1-1][i20];
      intim1[i1][i20] -=  fact*scale_fact2*intim2_sav[i1][i20];
   }
   intim1[i10][i20]    =  -fact*scale_fact2*intim2_sav[i10][i20];

   if(!mHoQiOnly) intim1.Scale(scale_fact1);

   In pow = I_1;
   In pow_other = I_1;
   In r_der = I_1;
   bool l_der = false;

   if (is_ddqq)
   {
      r_der = I_0;
      l_der = true;
   }
   else if (is_ddqqddq)
   {
      pow_other = I_0;
      l_der = true;
   }
   g_op_nr = gOperatorDefs.AddOneModeOper(pow,r_der,l_der);
   In i_add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << " g_oper_nr " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
   }
   mTheOneModeInt.DataIo(IO_PUT,intim1,nbas2,nbas,nbas,i_add);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "HocorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intim1.Norm() << " sumele: " << intim1.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intim1 << std::endl;
   }
   if (n_max<2) return;

   bool no_scale=false;
   if (mHoQiOnly) no_scale=true;
   intim2_sav.Zero();
   //Get integrals for Q^0/Q^1 needed for the calculation of this integral
   LocalOperNr loc_oper = mpOpDef->GetOneModeOperLocalNr(i_op_mode,gOperatorDefs.AddOneModeOper(pow_other));
   GetInt(i_op_mode, loc_oper, intim2_sav, no_scale);
   //Use recursive formulae to generate the rest of the integrals.
   for (In i_q=2;i_q <= n_max;i_q++)
   {
      Nb fact=C_I_2;
      if (  is_ddqq  )
      {
         fact=-C_I_2;
      }
      if (  is_ddqqddq  )
      {
         fact=((i_q-1)/C_2)*scale_fact1;
      }
      if (  gDebug   )
      {
         Mout << " calculate integrals for i_q = " << i_q << " of " << n_max << endl;
      }

      for (In i1 = 1;i1<nbas;i1++)
      {
         for (In i2 = 1;i2<nbas;i2++)
         {
            inti[i1][i2] = sqrt(i2/C_2)*intim1[i1][i2-1];
            inti[i1][i2] +=  sqrt(i1/C_2)*intim1[i1-1][i2];
            inti[i1][i2] +=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i2];
            inti[i1][i2] -=  scale_fact2*fact*intim2_sav[i1][i2];
         }
         In i10=0;
         for (In i2 = 1;i2<nbas;i2++)
         {
            inti[i10][i2] = sqrt(i2/C_2)*intim1[i10][i2-1];
            inti[i10][i2] +=  scale_fact1*((i_q-1)/C_2)*intim2[i10][i2];
            inti[i10][i2] -=  scale_fact2*fact*intim2_sav[i10][i2];
         }
         In i20=0;
         for (In i1 = 1;i1<nbas;i1++)
         {
            inti[i1][i20] =  sqrt(i1/C_2)*intim1[i1-1][i20];
            inti[i1][i20] +=  scale_fact1*((i_q-1)/C_2)*intim2[i1][i20];
            inti[i1][i20] -=  scale_fact2*fact*intim2_sav[i1][i20];
         }
         inti[i10][i20] =  scale_fact1*((i_q-1)/C_2)*intim2[i10][i20];
         inti[i10][i20] -=  scale_fact2*fact*intim2_sav[i10][i20];
      }

      if (  !mHoQiOnly  )
      {
         inti.Scale(scale_fact1);
      }
      //Store integrals on datacontainer.
      ++pow;
      g_op_nr = gOperatorDefs.AddOneModeOper(pow,r_der,l_der);
      i_add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
      mTheOneModeInt.DataIo(IO_PUT,inti,nbas2,nbas,nbas,i_add);

      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "HocorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << inti.Norm() << " sumele: " << inti.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << inti << std::endl;
      }
      //prepare for next round: retrieve new integrals.
      if (i_q < n_max)
      {
         intim2 = intim1;
         intim1 = inti;
         ++pow_other;
         //Here we get the Q^i integrals needed for the calculation of the next set of integrals
         loc_oper = mpOpDef->GetOneModeOperLocalNr(i_op_mode,gOperatorDefs.AddOneModeOper(pow_other));
         intim2_sav.Zero();
         GetInt(i_op_mode,loc_oper,intim2_sav,no_scale);
      }
  }
  return;
}

/**
* Calculate the Q^n integrals over gaussians for a particular aOperMode
* Includes both Q^n and (DDQ)^n - the later requires aDeriv = true, default is false.
* Coriolis integrals q*ddq and ddq*q  are calculated by aDeriv,aCoriolis = (f,t) and (t,t) respectively.
**/
void OneModeInt::GaussqiInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool aDeriv
   ,  bool aCoriolis
   )
{
   MidasAssert(mpBasDef, "BasDef needs to be set for GaussqiInt");

   if (gDebug || gOperIoLevel > I_13)
   {
      Mout << " Integrals ";
      if (!aCoriolis && !aDeriv) Mout << " Q^i";
      if (!aCoriolis && aDeriv) Mout << " (DDQ)^i";
      if (aCoriolis && aDeriv ) Mout << " (DDQ) Q";
      if (aCoriolis && !aDeriv ) Mout << " Q (DDQ)";
      Mout << " over gaussian functions are calculated for oper mode nr. " << aOperMode << std::endl;
   }
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In n_max = aNmax;
   //if fitting with scaling coordinates then scale_fact=sqrt(omeg)
   //Nb omeg = mpBasDef->GetOmeg(aBasMode);
   Nb scale_fact1 = C_1;
   if (mpOpDef->ScaledPot()) scale_fact1*=mpOpDef->ScaleFactorForMode(aOperMode);
   if (gDebug) Mout << " scale_fact: " << scale_fact1 << endl;
   if (gDebug) Mout << " Integrals over gaussian basis calculated for upto n_max = "
       << n_max << " and nbas= " << nbas << endl;
   //working space for holding zeta_ij and Zeta_ij elements
   MidasVector bas_exp(nbas);
   bas_exp.Zero();
   mpBasDef->GetVecOfExps(aBasMode, bas_exp);
   if (gDebug)
   {
      Mout << " Vector of exponents for basis no. " << aBasMode << endl;
      Mout << bas_exp << endl;
   }
   MidasMatrix p_exp(nbas);
   p_exp.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      p_exp[i1][i1]=C_I_2*bas_exp[i1];
      for (In i2=0; i2<i1; i2++)
      {
         Nb val=(bas_exp[i1]*bas_exp[i2])/(bas_exp[i1]+bas_exp[i2]);
         p_exp[i1][i2]=val;
         p_exp[i2][i1]=val;
      }
   }
   MidasVector g_pt(nbas);
   g_pt.Zero();
   mpBasDef->GetVecOfPts(aBasMode,g_pt);
   if (gDebug)
   {
      Mout << " location of gaussians for basis no. " << aBasMode << endl;
      Mout << g_pt << endl;
   }
   MidasMatrix p_cen(nbas);
   p_cen.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
     p_cen[i1][i1]=g_pt[i1];
      for(In i2=0; i2<i1; i2++)
      {
         Nb val=(g_pt[i1]*bas_exp[i1]+g_pt[i2]*bas_exp[i2])/(bas_exp[i1]+bas_exp[i2]);
         p_cen[i1][i2]=val;
         p_cen[i2][i1]=val;
      }
   }
   //compute overlap matrix
   MidasMatrix ovlp(nbas);
   ovlp.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      ovlp[i1][i1]=C_1;
      for (In i2=0; i2<i1; i2++)
      {
         Nb ov_exp=-p_exp[i1][i2]*pow((g_pt[i1]-g_pt[i2]),C_2);
         Nb ov_prcst=(C_4*p_exp[i1][i2])/(bas_exp[i1]+bas_exp[i2]);
         ovlp[i1][i2]=pow(ov_prcst,C_I_4)*exp(ov_exp);
         ovlp[i2][i1]=pow(ov_prcst,C_I_4)*exp(ov_exp);
      }
   }
   In power = I_0;
   In power_der = I_0;
   GlobalOperNr g_op_nr = gOperatorDefs.AddOneModeOper(power,power_der);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "GaussqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << aOperMode << " norm: " << ovlp.Norm() << " sumele: " << ovlp.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << ovlp << std::endl;
   }
   In i_add;
   if (!aDeriv&&!aCoriolis)  //if Q^0, then save it
   {
      i_add = mOffSetMap.find(std::make_pair(aOperMode,g_op_nr))->second;
      if (gDebug)
      {
         Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
      }
      mTheOneModeInt.DataIo(IO_PUT,ovlp,nbas2,nbas,nbas,i_add);
   }

   if (n_max<1) return;
   MidasMatrix intqn(nbas);  // integrals for Q^1/(DDQ)^1
   intqn.Zero();
   //Q^1 integrals
   if (!aDeriv && !aCoriolis)
   {
      for (In i1=0; i1<nbas; i1++)
      {
         intqn[i1][i1]=g_pt[i1];
         for (In i2=0; i2<i1; i2++)
         {
            intqn[i1][i2]=p_cen[i1][i2]*ovlp[i1][i2];
            intqn[i2][i1]=p_cen[i1][i2]*ovlp[i1][i2];
         }
      }
   }
   //d/dq integral
   else if (!aCoriolis && aDeriv)
   {
      for (In i1=0; i1<nbas; i1++)
      {
         intqn[i1][i1]=C_0;
         for (In i2=0; i2<i1; i2++)
         {
            intqn[i1][i2]=-C_2*p_exp[i1][i2]*(g_pt[i1]-g_pt[i2])*ovlp[i1][i2];
            intqn[i2][i1]=-intqn[i1][i2];
         }
      }
   }
   //then apply the scaling factor if it is not a pure derivative
   if (!aDeriv && !aCoriolis) intqn.Scale(scale_fact1);
   //and save into the container
   if (!aDeriv) ++power;
   if (aDeriv) ++power_der;
   //if (aCoriolis&&aDeriv)  oper = "ddqq";
   //if (aCoriolis&&!aDeriv) oper = "qddq";
   g_op_nr = gOperatorDefs.AddOneModeOper(power,power_der);
   i_add = mOffSetMap.find(std::make_pair(aOperMode,g_op_nr))->second;
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
   }
   if (!aCoriolis)
   {
      mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
   }
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "GaussqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << aOperMode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;
      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
   }

   if (n_max<2) return;
   intqn.Zero();
   //do Q^2 or (DDQ)^2
   if (!aDeriv && !aCoriolis) //this is Q^2
   {
      for (In i1=0; i1<nbas; i1++)
      {
         intqn[i1][i1]=(C_1/(C_4*bas_exp[i1]))+pow(g_pt[i1],C_2);
         for (In i2=0; i2<i1; i2++)
         {
            intqn[i1][i2]=(pow(p_cen[i1][i2],C_2)+(C_1/(C_2*(bas_exp[i1]+bas_exp[i2]))))*ovlp[i1][i2];
            intqn[i2][i1]=(pow(p_cen[i2][i1],C_2)+(C_1/(C_2*(bas_exp[i1]+bas_exp[i2]))))*ovlp[i1][i2];
         }
      }
   }
   else if (!aCoriolis && aDeriv) //this is (DDQ)^2
   {
      for (In i1=0; i1<nbas; i1++)
      {
         intqn[i1][i1]=-bas_exp[i1];
         for (In i2=0; i2<i1; i2++)
         {
            //intqn[i1][i2]=-C_2*alph[i2]*ovlp[i1][i2]*(C_1-C_2*alph[i2]*((C_1/(C_2*(alph[i1]+alph[i2])))+pow((r_mat[i1][i2]-r_i[i2]),C_2)));
            //intqn[i2][i1]=-C_2*alph[i1]*ovlp[i2][i1]*(C_1-C_2*alph[i1]*((C_1/(C_2*(alph[i1]+alph[i2])))+pow((r_mat[i2][i1]-r_i[i1]),C_2)));
            //simpler
            intqn[i1][i2]=-C_2*ovlp[i1][i2]*p_exp[i1][i2]*(C_1-C_2*p_exp[i1][i2]*pow((g_pt[i1]-g_pt[i2]),C_2));
            intqn[i2][i1]=intqn[i1][i2];
         }
      }
   }
   //then apply the scaling factor if it is not a pure derivative
   if (!aDeriv && !aCoriolis) intqn.Scale(pow(scale_fact1,C_2));
   //and save into the container
   if(!aDeriv) ++power;
   if (!aCoriolis && aDeriv) ++power_der;
   g_op_nr = gOperatorDefs.AddOneModeOper(power, power_der);
   i_add = mOffSetMap.find(std::make_pair(aOperMode,g_op_nr))->second;
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
   }
   mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "GaussqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << aOperMode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
   }

   if (n_max<3) return;

   //then calculate Q^n integrals up to assigned n_max
   if (!(!aCoriolis && !aDeriv))
   {
      MIDASERROR("  Inconsistent value of nmax in calling GaussqiInt ");
   }
   for (In i_q=3; i_q<=n_max; i_q++)
   {
      Nb scale_fact=pow(scale_fact1,Nb(i_q));
      //Mout << " scale_fact= " << scale_fact << endl;
      if (gDebug)
      {
         Mout << " calculate integrals for i_q = " << i_q << " of " << n_max << std::endl;
      }
      intqn.Zero();

      In k_max=In(floor(C_I_2*i_q)); //check conversion
      //Mout << " for n= " << i_q << " is k_max= " << k_max << endl;
      for (In i1=0; i1<nbas; i1++)
      {
         for (In i2=0; i2<=i1; i2++)
         {
            Nb val=C_0;
            //do k=0 first
            val+=pow(p_cen[i1][i2],Nb(i_q));
            for (In k=1; k<=k_max; k++)
            {
               Nb zeta_exp=Nb(i_q-2*k);
               val+=NbBinCo(i_q,2*k)*(NbEmFact(2*k-1)/(pow(C_2,Nb(k))*pow((bas_exp[i1]+bas_exp[i2]),Nb(k))))*pow(p_cen[i1][i2],zeta_exp);
            }
            intqn[i1][i2]=val*ovlp[i1][i2];
         }
      }
      //then compute the upper half
      for (In i1=0; i1<nbas; i1++)
         for (In i2=0; i2<i1; i2++)
            intqn[i2][i1]=intqn[i1][i2];

      //then scale the resulting matrix
      intqn.Scale(scale_fact);

      //Store integrals on datacontainer.
      if (!aDeriv && !aCoriolis) ++power;
      g_op_nr = gOperatorDefs.AddOneModeOper(power);
      i_add = mOffSetMap.find(std::make_pair(aOperMode,g_op_nr))->second;
      if (!aDeriv && !aCoriolis) mTheOneModeInt.DataIo(IO_PUT,intqn,nbas2,nbas,nbas,i_add);

      //debug stuff
      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "GaussqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << aOperMode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
      }
      // prepare for next round.
   }
   return;
}
/**
* Calculate Coriolis integrals over gaussian functions for a particular aOperMode
* Includes the following integrals:
* Q^i (DDQ): with is_qddq=true
* (DDQ) Q^i with is_ddqq=true
* (DDQ) Q^i (DDQ) with is_ddqqddq=true
**/
void OneModeInt::GausscorInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool is_qddq
   ,  bool is_ddqq
   ,  bool is_ddqqddq
   )
{
   MidasAssert(mpBasDef, "BasDef needs to be set for OneModeInt::GuasscorInt");

   LocalModeNr i_op_mode=aOperMode;
   Mout << " Coriolis integrals over gaussian functions";
   In power = I_0;
   In r_der_pow = I_0;
   bool l_der = false;
   if (is_qddq)
   {
      r_der_pow = I_1;
      Mout << " Q^i (DDQ) ";
   }
   if (is_ddqq)
   {
      l_der = true;
      Mout << " (DDQ) Q^i ";
   }
   if (is_ddqqddq)
   {
      r_der_pow = I_1;
      l_der = true;
      Mout << " (DDQ) Q^i (DDQ) ";
   }
   Mout << " calculated for oper mode nr. " << aOperMode <<endl;
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In n_max = aNmax;
   //check n_max is at least=1. Otherwise no new integras are calculated
   if (n_max<=0)
   {
      Mout << " No new integrals are calculated in LgbcorInt on return " << endl;
      return;
   }
   //n_max+=1;
   //if (is_ddqqddq) n_max+=1;
   // Scaling info. UPDATE ACCORDING TO INPUT
   //if fitting with scaling coordinates then scale_fact=sqrt(omeg)
   Nb scale_fact1 = C_1;
   if (mpOpDef->ScaledPot()) scale_fact1*=mpOpDef->ScaleFactorForMode(i_op_mode);
   if (gDebug) Mout << " Scale_fact " << scale_fact1 << endl;

   if (gDebug) Mout << " Coriolis integrals over localized gaussian functions calculated for upto n_max = "
       << n_max << " and nbas= " << nbas << endl;
   //working space for holding zeta_ij and Zeta_ij elements
   MidasVector bas_exp(nbas);
   bas_exp.Zero();
   mpBasDef->GetVecOfExps(aBasMode, bas_exp);

   MidasMatrix p_exp(nbas);
   p_exp.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      p_exp[i1][i1]=C_I_2*bas_exp[i1];
      for (In i2=0; i2<i1; i2++)
      {
         Nb val=(bas_exp[i1]*bas_exp[i2])/(bas_exp[i1]+bas_exp[i2]);
         p_exp[i1][i2]=val;
         p_exp[i2][i1]=val;
      }
   }
   MidasVector g_pt(nbas);
   g_pt.Zero();
   mpBasDef->GetVecOfPts(aBasMode,g_pt);

   MidasMatrix p_cen(nbas);
   p_cen.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      p_cen[i1][i1]=g_pt[i1];
      for(In i2=0; i2<i1; i2++)
      {
         Nb val=(g_pt[i1]*bas_exp[i1]+g_pt[i2]*bas_exp[i2])/(bas_exp[i1]+bas_exp[i2]);
         p_cen[i1][i2]=val;
         p_cen[i2][i1]=val;
      }
   }
   //compute overlap matrix, stored into intm2
   MidasMatrix intm2(nbas);
   intm2.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      intm2[i1][i1]=C_1;
      for (In i2=0; i2<i1; i2++)
      {
         Nb ov_exp=-p_exp[i1][i2]*pow((g_pt[i1]-g_pt[i2]),C_2);
         Nb ov_prcst=(C_4*p_exp[i1][i2])/(bas_exp[i1]+bas_exp[i2]);
         intm2[i1][i2]=pow(ov_prcst,C_I_4)*exp(ov_exp);
         intm2[i2][i1]=pow(ov_prcst,C_I_4)*exp(ov_exp);
      }
   }
   MidasMatrix ovlp(nbas);
   ovlp.Zero();
   ovlp=intm2;
   GlobalOperNr g_op_nr = gOperatorDefs.AddOneModeOper(I_0);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "GausscorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intm2.Norm() << " sumele: " << intm2.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intm2 << std::endl;
   }

   MidasMatrix intm1(nbas);  //integrals for Q^1
   intm1.Zero();
   for (In i1=0; i1<nbas; i1++)
   {
      intm1[i1][i1]=g_pt[i1];
      for (In i2=0; i2<i1; i2++)
      {
         intm1[i1][i2]=p_cen[i1][i2]*intm2[i1][i2];
         intm1[i2][i1]=p_cen[i1][i2]*intm2[i1][i2];
      }
   }
   g_op_nr = gOperatorDefs.AddOneModeOper(I_1);
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "GausscorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intm1.Norm() << " sumele: " << intm1.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intm1 << std::endl;
   }

   MidasMatrix intm0(nbas);
   intm0.Zero();
   //do Q^2 integrals
   for (In i1=0; i1<nbas; i1++)
   {
      intm0[i1][i1]=(C_1/(C_4*bas_exp[i1]))+pow(g_pt[i1],C_2);
      for (In i2=0; i2<i1; i2++)
      {
         intm0[i1][i2]=(pow(p_cen[i1][i2],C_2)+(C_1/(C_2*(bas_exp[i1]+bas_exp[i2]))))*intm2[i1][i2];
         intm0[i2][i1]=(pow(p_cen[i2][i1],C_2)+(C_1/(C_2*(bas_exp[i1]+bas_exp[i2]))))*intm2[i1][i2];
      }
   }
   MidasMatrix intsav(nbas);
   intsav.Zero();
   //calculate q*ddq and ddq*q integrals. store in intm0
   if (!is_ddqqddq)
   {
      for (In i1=0; i1<nbas; i1++)
         for (In i2=0; i2<nbas; i2++)
         {
            intsav[i1][i2]=-C_2*bas_exp[i2]*intm0[i1][i2];
            intsav[i1][i2]+=C_2*bas_exp[i2]*g_pt[i2]*intm1[i1][i2];
         }
      if (is_ddqq)
         intsav+=intm2;
      //apply scaling factor and put into the container
      intsav.Scale(scale_fact1);
      ++power;
      g_op_nr = gOperatorDefs.AddOneModeOper(power,r_der_pow,l_der);
      In i_add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
      }
      mTheOneModeInt.DataIo(IO_PUT, intsav, nbas2, nbas, nbas, i_add);
      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << "GausscorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intsav.Norm() << " sumele: " << intsav.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intsav << std::endl;
      }
      if (n_max<2) return;
   }
   // then shift and enter the loop
   MidasMatrix intm3(0);
   if (is_ddqqddq)
   {
      intm3.SetNewSize(nbas);
      intm3=intm2;
   }
   intm2=intm1;
   intm1=intm0;
   In i_q_max=n_max+1;
   if (is_ddqqddq) i_q_max=n_max+2;
   for (In i_q=3; i_q<=i_q_max; i_q++)
   {
      Nb scale_fact=pow(scale_fact1,Nb(i_q-1));
      if (is_ddqqddq) scale_fact=pow(scale_fact1,Nb(i_q-2));
      //Mout << " scale_fact= " << scale_fact << endl;
      if (gDebug) Mout << " loop index i_q = " << i_q << " of " << i_q_max << endl;
      intm0.Zero();
      In k_max=In(floor(C_I_2*i_q)); //check conversion
      Mout << " for n= " << i_q << " is k_max= " << k_max << endl;
      for (In i1=0; i1<nbas; i1++)
         for (In i2=0; i2<=i1; i2++)
         {
            Nb val=C_0;
            //do k=0 first
            val+=pow(p_cen[i1][i2],Nb(i_q));
            for (In k=1; k<=k_max; k++)
            {
               Nb zeta_exp=Nb(i_q-2*k);
               val+=NbBinCo(i_q,2*k)*(NbEmFact(2*k-1)/(pow(C_2,Nb(k))*pow((bas_exp[i1]+bas_exp[i2]),Nb(k))))*pow(p_cen[i1][i2],zeta_exp);
            }
            intm0[i1][i2]=val*ovlp[i1][i2];
         }
      //then compute the upper half
      for (In i1=0; i1<nbas; i1++)
         for (In i2=0; i2<i1; i2++)
            intm0[i2][i1]=intm0[i1][i2];
      //then compute the various matrices
      intsav.Zero();
      if (!is_ddqqddq)
      {
         for (In i1=0; i1<nbas; i1++)
            for (In i2=0; i2<nbas; i2++)
            {
               intsav[i1][i2]=-C_2*bas_exp[i2]*intm0[i1][i2];
               intsav[i1][i2]+=C_2*bas_exp[i2]*g_pt[i2]*intm1[i1][i2];
            }
         if (is_ddqq)
         {
            intm2.Scale(Nb(i_q-1));
            intsav+=intm2;
         }
      }
      else if (is_ddqqddq)
      {
         for (In i1=0; i1<nbas; i1++)
            for (In i2=0; i2<nbas; i2++)
            {
               intsav[i1][i2]=C_4*pow(bas_exp[i2],C_2)*intm0[i1][i2];
               intsav[i1][i2]-=C_8*pow(bas_exp[i2],C_2)*g_pt[i2]*intm1[i1][i2];
               intsav[i1][i2]+=(C_4*pow(bas_exp[i2],C_2)*pow(g_pt[i2],C_2)-C_2*bas_exp[i2]*Nb(i_q-1))*intm2[i1][i2];
               intsav[i1][i2]+=C_2*Nb(i_q-2)*bas_exp[i2]*g_pt[i2]*intm3[i1][i2];
            }
      }
      //then scale the resulting matrix
      intsav.Scale(scale_fact);
      //Store integrals on datacontainer.
      ++power;
      g_op_nr = gOperatorDefs.AddOneModeOper(power,r_der_pow,l_der);
      In i_add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
      mTheOneModeInt.DataIo(IO_PUT,intsav,nbas2,nbas,nbas,i_add);

      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << " oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "GausscorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intsav.Norm() << " sumele: " << intsav.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intsav << std::endl;
      }
      //prepare for next round: retrieve new integrals.
      if (i_q<i_q_max)
      {
         if (is_ddqqddq) intm3=intm2;
         intm2 = intm1;
         intm1 = intm0;
      }
  }
  return;
}

/**
 * <vec|int|vec>
**/
void OneModeInt::IntBracket
   (  MidasVector& aMidasVector
   ,  const LocalModeNr arOperMode
   ,  const LocalOperNr arOperNr
   ,  Nb& aResult
   )
{
   //ask the difference with InMem
   GlobalModeNr i_g_mode = mpOpDef->GetGlobalModeNr(arOperMode);
   LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
   In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(arOperMode) + I_1;
   In nbas2 = nbas*nbas;

   if (StringNoCaseCompare(mStorage, "InMem"))
   {
      GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(arOperMode, arOperNr);
      if (  mHoQiOnly
         )
      {
         MidasAssert(mpBasDef, "mpBasDef must be set for mHoQiOnly");

         In ipow = I_0;
         bool deriv = false;
         bool cor_two_deriv = false;
         bool cor_left_deriv = false;
         bool cor_right_deriv = false;
         bool is_cor_int_req = false;
         GetHoInfo(g_op_nr, ipow, deriv, cor_two_deriv, cor_left_deriv, cor_right_deriv, is_cor_int_req);
         // Now found adress of (mode,oper) integral block.
         In add = mOffSetMap.find(std::make_pair(arOperMode, g_op_nr))->second;
         MidasMatrix h_m_p(nbas);
         mTheOneModeInt.DataIo(IO_GET, h_m_p, nbas2, nbas, nbas, add, I_1, false, C_0, I_1 + mMaxNhigh);
         MidasVector half_trans = h_m_p*aMidasVector;
         aResult = Dot(half_trans, aMidasVector); // Assumes real vector

         Nb mass = mpBasDef->GetMass(i_bas_mode);
         Nb scal = mpBasDef->GetOmeg(i_bas_mode);
         Nb omeg_sav = scal;
         if (!deriv && mpOpDef->ScaledPot())
         {
            scal /= pow(mpOpDef->ScaleFactorForMode(arOperMode), C_2);
         }
         Nb scale_fact1 = sqrt(mass*scal);
         if (!deriv)
         {
            scale_fact1 = C_1/scale_fact1;
         }
         Nb scale_fact = C_1;
         for (In i = I_0; i < ipow; i++)
         {
            scale_fact *= scale_fact1;
         }
         is_cor_int_req = (cor_two_deriv || cor_left_deriv || cor_right_deriv);
         if ((deriv || mUseMassScaling) && !is_cor_int_req)
         {
            aResult *= scale_fact;
         }
         else if (is_cor_int_req)
         {
            if (cor_two_deriv)
            {
               scale_fact *= (mass*omeg_sav);
               aResult *= scale_fact;
            }
            else if (cor_left_deriv || cor_right_deriv)
            {
               scale_fact *= sqrt(mass*omeg_sav);
               aResult *= scale_fact;
            }
         }
      }
      else
      {
         In add = mOffSetMap.find(std::make_pair(arOperMode, g_op_nr))->second;
         MidasMatrix h_m_p(nbas);
         mTheOneModeInt.DataIo(IO_GET, h_m_p, nbas2, nbas, nbas, add);
         MidasVector half_trans = h_m_p*aMidasVector;
         if (gDebug)
         {
            Mout << "vers2 half_trans norm, sumele " << half_trans.Norm() << " " << half_trans.SumEle() << std::endl;
         }
         aResult = Dot(half_trans, aMidasVector); // Assumes real vector
      }
   }
   else
   {
      //Standard manner
      MidasMatrix h_m_p(nbas);
      GetInt(arOperMode, arOperNr, h_m_p);
      MidasVector half_trans = h_m_p*aMidasVector;
      if (gDebug)
      {
         Mout << "vers3 half_trans norm, sumele " << half_trans.Norm() << " " << half_trans.SumEle() << std::endl;
      }
      aResult = Dot(half_trans, aMidasVector); // Assumes real vector
   }
   if (gOperIoLevel > I_14)
   {
      Mout << " IntBracket (<vec|int|vec>) for mode " << arOperMode << " operator " << arOperNr << ": " << std::endl << " result = " << aResult << std::endl << std::endl;
   }
}

/**
 * Fills a matrix for a specific mode with entries that holds the combination a operator integral values multiplied with the corresponding linear coefficients. This matrix can normally be identified as some sort of Hamiltonian.
 *
 * @param arMidasMatrix The matrix to be filled and ultimately "returned"
 * @param arOperMode    The mode for which to build the matrix
 * @param arOperNr      The operator whose associated values are to be inserted into the matrix
 * @param arScaleFact   The linear coefficient for the operator
**/
void OneModeInt::AddIntTo
   (  MidasMatrix& arMidasMatrix
   ,  const LocalModeNr& arOperMode
   ,  const LocalOperNr& arOperNr
   ,  const Nb& arScaleFact
   )
{
   GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(arOperMode);
   LocalModeNr i_bas_mode  = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
   In nbas                 = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(arOperMode) + I_1;
   In nbas2                = nbas*nbas;

   // Check for combined operators
   GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(arOperMode, arOperNr);

   if (StringNoCaseCompare(mStorage, "InMem"))
   {
      if (  mHoQiOnly
         )
      {
         MidasAssert(mpBasDef, "mpBasDef must be set for mHoQiOnly");

         In ipow = I_0;
         bool deriv = false;
         bool cor_two_deriv = false;
         bool cor_left_deriv = false;
         bool cor_right_deriv = false;
         bool is_cor_int_req = false;

         //
         GetHoInfo
            (  g_op_nr
            ,  ipow
            ,  deriv
            ,  cor_two_deriv
            ,  cor_left_deriv
            ,  cor_right_deriv
            ,  is_cor_int_req
            );

         // Now find adress of (mode,oper) integral block.
         In add = mOffSetMap.find(std::make_pair(arOperMode,g_op_nr))->second;

         Nb mass = mpBasDef->GetMass(i_bas_mode);
         Nb scal = mpBasDef->GetOmeg(i_bas_mode);
         Nb omeg_sav = scal;
         if (!deriv && mpOpDef->ScaledPot())
         {
            scal /= std::pow(mpOpDef->ScaleFactorForMode(arOperMode), C_2);
         }
         Nb scale_fact1 = std::sqrt(mass*scal);
         if (!deriv)
         {
            scale_fact1 = C_1 / scale_fact1;
         }
         Nb scale_fact = C_1;
         for (In i = I_0; i < ipow; i++)
         {
            scale_fact *= scale_fact1;
         }

         is_cor_int_req = (cor_two_deriv || cor_left_deriv || cor_right_deriv);
         if ((deriv || mUseMassScaling) && !is_cor_int_req)
         {
            scale_fact *= arScaleFact;
         }
         else if (is_cor_int_req)
         {
            if (cor_two_deriv)
            {
               scale_fact *= (mass * omeg_sav);
               scale_fact *= arScaleFact;
            }
            else if (cor_left_deriv || cor_right_deriv)
            {
               scale_fact *= std::sqrt(mass * omeg_sav);
               scale_fact *= arScaleFact;
            }
         }
         else
         {
            scale_fact = arScaleFact;
         }

         // Collect evaluated primitive integral values and add these to the Hamiltonian together with their associated linear coefficients
         mTheOneModeInt.DataIo
            (  IO_GET
            ,  arMidasMatrix
            ,  nbas2
            ,  nbas
            ,  nbas
            ,  add
            ,  I_1
            ,  false
            ,  C_0
            ,  I_1 + mMaxNhigh
            ,  true
            ,  scale_fact
            );
      }
      else
      {
         // Collect evaluated primitive integral values and add these to the Hamiltonian together with their associated linear coefficients
         const auto& add = mOffSetMap.find(std::make_pair(arOperMode, g_op_nr))->second;
         mTheOneModeInt.DataIo
            (  IO_GET
            ,  arMidasMatrix
            ,  nbas2
            ,  nbas
            ,  nbas
            ,  add
            ,  I_1
            ,  false
            ,  C_0
            ,  -I_1
            ,  true
            ,  arScaleFact
            );
      }
   }
   else
   {
      // Collect evaluated primitive integral values
      MidasMatrix h_m_p(nbas);
      GetInt(arOperMode, arOperNr, h_m_p);

      // Add these to the Hamiltonian together with their associated linear coefficients
      arMidasMatrix += arScaleFact * h_m_p;
   }
}

/**
 * Check if the operator will be considered in the SoS evaluation.
 * We ignore the following operators Q^0, Q^1 and (DDQ)^1
 * Returns true if the operator is not to be considered
**/
bool OneModeInt::CheckSoSOper
   (  GlobalOperNr aGOpNr
   )
{
   const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(aGOpNr);
   if(this_oper->GetPow() == I_0)
   {
      if(this_oper->GetRDer() < I_2)
      {
         return true;
      }
   }
   else if(this_oper->GetPow() == I_1)
   {
      if(!this_oper->GetLDer() || this_oper->GetRDer() < I_0)
      {
         return true;
      }
   }
   return false;
}

/**
* Get the operator components used to check in the EvalSos function.
* Assumes that we are not going to enter with aGOpNr -> Q^0, Q^1 or (DDQ)^1.
* Which the function above should have stopped.
**/
void OneModeInt::GetSoSCompOpers
   (  const GlobalOperNr aGOpNr
   ,  const LocalModeNr aLModeNr
   ,  LocalOperNr& arFirstOper
   ,  LocalOperNr& arSecondOper
   )
{
   const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(aGOpNr);
   GlobalOperNr first, second;
   if(this_oper->GetPow() == I_0)
   {
      //THe operator is (DDQ)^i
      first = gOperatorDefs.AddOneModeOper(I_0, this_oper->GetRDer() - I_1);
      second = gOperatorDefs.AddOneModeOper(I_0, I_1);
   }
   else
   {
      if(this_oper->GetLDer())
      {
         if(this_oper->GetRDer() == I_1)
         {
            //The operator is (DDQ) Q^i (DDQ)
            first = gOperatorDefs.AddOneModeOper(I_0,I_1);
            second = gOperatorDefs.AddOneModeOper(this_oper->GetPow(),I_1);
         }
         else
         {
            //The operator is (DDQ) Q^i
            first = gOperatorDefs.AddOneModeOper(I_0, I_1);
            second = gOperatorDefs.AddOneModeOper(this_oper->GetPow());
         }
      }
      else if(this_oper->GetRDer() == I_1)
      {
         //The operator is Q^i (DDQ)
         first = gOperatorDefs.AddOneModeOper(this_oper->GetPow());
         second = gOperatorDefs.AddOneModeOper(I_0, I_1);
      }
      else
      {
         //The operator is Q^i
         first = gOperatorDefs.AddOneModeOper(this_oper->GetPow() - I_1);
         second = gOperatorDefs.AddOneModeOper(I_1);
      }
   }
   //first and second will be asigned by now since they will get to the else( else()) statement..
   arFirstOper = mpOpDef->GetOneModeOperLocalNr(aLModeNr, first);
   arSecondOper = mpOpDef->GetOneModeOperLocalNr(aLModeNr, second);
}

/**
* Evaluate Sum over States for check consistency of one mode coriolis integrals
* as well as integrals over localised gaussian functions
**/
void OneModeInt::EvalSos()
{
   MidasAssert(mpBasDef, "mpBasDef not set!");

   MidasMatrix op_right(0);
   MidasMatrix op_left(0);
   MidasMatrix op_res(0);
   MidasMatrix op_mat(0);
   In modes_in_loop = mNoModesInInt;

   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   string s = " Evaluate Sum over States integrals for " + std::to_string(modes_in_loop) + " modes ";
   OneArgOut72(Mout,s,'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   Mout << endl;

   for (LocalModeNr i_op_mode=0; i_op_mode<modes_in_loop; i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas = mpBasDef->Nbas(i_bas_mode);

      if (mpBasDef->OneModeType(i_bas_mode)=="HO") //
      {
         Mout << " Sos matrix representations for mode "
          << gOperatorDefs.GetModeLabel(i_g_mode)<<endl;
         //loop over one-mode operators for this mode
         for (LocalOperNr i_op=0; i_op<mpOpDef->NrOneModeOpers(i_op_mode); i_op++)
         {
            GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode,i_op);
            //check just new integrals
            //Do not consider Q^0, Q^1 and (DDQ)^1
            if (CheckSoSOper(g_op_nr)) continue;
            Mout << " Oper no. " << i_op << " with label " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << endl;
            Mout << endl;
            string s_op1;
            string s_op2;
            op_mat.SetNewSize(nbas);
            op_mat.Zero();
            GetInt(i_op_mode,i_op,op_mat);
            //Mout << orig_int;
            //retrieve addresses of sub operators and integrals
            LocalOperNr i_op_first, i_op_second;
            op_left.SetNewSize(nbas);
            op_left.Zero();
            op_right.SetNewSize(nbas);
            op_right.Zero();
            GetSoSCompOpers(g_op_nr,i_op_mode,i_op_first,i_op_second);
            GetInt(i_op_mode,i_op_first, op_left);
            GetInt(i_op_mode,i_op_second, op_right);
            //Mout << " matrix representation for oper no. " << i_op_n << " oper = " << oper << endl;
            //Mout << b_fact;
            //Mout << endl;
            //now evaluate sos
            //Mout << " sos matrix representation for oper. no. " << i_oper << " oper = " << oper << endl;
            op_res.SetNewSize(nbas);
            op_res.Zero();
            op_res=op_left*op_right;
            //Mout << result;
            //Mout << endl;
            //Mout << " Norm of the one mode original integrals: " << orig_int.Norm();
            //Mout << " SumAbsEle of all original one-mode integrals: " << orig_int.SumAbsEle();
            //Mout << endl;
            //Mout << " Summary: " << endl << endl;
            Mout << " ia    "  << " ib    " << "       explicit value      " << "      sos value       " << "     error     " << endl;
            for (In ia=0; ia<nbas; ia++)
               for (In ib=0; ib<nbas; ib++)
               {
                  if (fabs(op_mat[ia][ib]-op_res[ia][ib])<C_NB_EPSILON) continue;
                  Mout << ia  << "    " << ib << "    " << op_mat[ia][ib] << "    " << op_res[ia][ib] << "    " << op_mat[ia][ib]-op_res[ia][ib] << endl;
               }
            Mout << endl;
         }
      }
      else if (mpBasDef->OneModeType(i_bas_mode)=="Gauss" || mpBasDef->OneModeType(i_bas_mode)=="Bsplines")
      {
         Mout << " Sos matrix representations for mode "
              << gOperatorDefs.GetModeLabel(i_g_mode)<<endl;
         //debug
         //Mout << " Basis set dimension: " << nbas << endl;
         //find overlap matrix and invert it
         MidasMatrix ovlp(nbas);
         MidasMatrix s_inv(nbas);
         LocalOperNr i_op_n = mpOpDef->GetUnitOpForMode(i_op_mode);
         ovlp.Zero();
         GetInt(i_op_mode,i_op_n,ovlp);
         //debug
         //Mout << " Overlap matrix in EvalSos: " << endl;
         //Mout << ovlp << endl;
         //and diagonalize it
         MidasMatrix eig_vec_s(nbas);
         MidasVector eig_val_s(nbas);
         Diag(ovlp,eig_vec_s,eig_val_s,"MIDAS_JACOBI",true);
         //
         MidasVector sigm_vec(nbas);
         for (In k=0; k<nbas; k++)
            sigm_vec[k]=C_1/eig_val_s[k];
         //build S^-1 matrix
         MidasMatrix sigm_m1(nbas,sigm_vec);
         MidasMatrix tmp(nbas);
         tmp.Zero();
         tmp=eig_vec_s*sigm_m1;
         eig_vec_s.Transpose();
         s_inv.Zero();
         s_inv=tmp*eig_vec_s;
         //check correctness
         //MidasMatrix unit(nbas);
         //unit=s_inv*ovlp;
         //Mout << " checking inversion of S matrix in OneModeInt::EvalSos() " << endl;
         //Mout << unit << endl;

         //loop over one-mode operators for this mode
         for (LocalOperNr i_op=0; i_op<mpOpDef->NrOneModeOpers(i_op_mode); i_op++)
         {

            GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode,i_op);
            if (CheckSoSOper(g_op_nr)) continue;
            Mout << " Sos matrix evaluation for oper no. " << i_op << " with label " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << endl;
            Mout << endl;
            string s_op1;
            string s_op2;
            op_mat.SetNewSize(nbas);
            op_mat.Zero();
            GetInt(i_op_mode,i_op,op_mat);
            //Mout << orig_int;
            //retrieve now sub operators and integrals
            LocalOperNr i_op_first, i_op_second;
            op_left.SetNewSize(nbas);
            op_left.Zero();
            op_right.SetNewSize(nbas);
            op_right.Zero();
            GetSoSCompOpers(g_op_nr,i_op_mode,i_op_first,i_op_second);
            GetInt(i_op_mode,i_op_first, op_left);
            GetInt(i_op_mode,i_op_second, op_right);
            //Mout << " matrix representation for oper no. " << i_op_n << " oper = " << mpOpDef->GetOneModeOperDescription(i_op_mode,i_op_n) << endl;
            //Mout << op_right;
            //Mout << endl;
            //now evaluate sos
            //Mout << " sos matrix representation for oper. no. " << i_oper << " oper = " << oper << endl;
            MidasMatrix tmp(nbas);
            tmp.Zero();
            op_res.SetNewSize(nbas);
            op_res.Zero();
            tmp=op_left*s_inv;
            op_res=tmp*op_right;
            //Mout << result;
            //Mout << endl;
            //Mout << " Norm of the one mode original integrals: " << orig_int.Norm();
            //Mout << " SumAbsEle of all original one-mode integrals: " << orig_int.SumAbsEle();
            //Mout << endl;
            //Mout << " Summary: " << endl << endl;
            Mout << " ia    "  << " ib    " << "       explicit value      " << "      sos value       " << "     error     " << endl;
            for (In ia=0; ia<nbas; ia++)
               for (In ib=0; ib<nbas; ib++)
               {
                  if (fabs(op_mat[ia][ib]-op_res[ia][ib])<C_NB_EPSILON) continue;
                  Mout << ia  << "    " << ib << "    " << op_mat[ia][ib] << "    " << op_res[ia][ib] << "    " << op_mat[ia][ib]-op_res[ia][ib] << endl;
               }
            Mout << endl;
            //}
         }
      }
      else
      {
         Mout << "Integrals for basis type " << mpBasDef->OneModeType(i_bas_mode)
                 << " for mode " << gOperatorDefs.GetModeLabel(i_g_mode)
                 << " is not supported yet " << endl;
      }
   }
}
/**
* print infos on calculated integrals
**/
void OneModeInt::PrintInt
   (
   )  const
{
   MidasMatrix aM(I_0);
   In modes_in_loop = mNoModesInInt;
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   string s = " Print integrals for no. " + std::to_string(modes_in_loop) + " modes ";
   OneArgOut72(Mout,s,'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   Mout << endl;

   for (LocalModeNr i_op_mode=0; i_op_mode<modes_in_loop; i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
      In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(i_op_mode) + I_1;
      In nbas2 = nbas*nbas;
      aM.SetNewSize(nbas);

      if (  mpOpDef->StateTransferOnly(i_op_mode)
         )
      {
         Mout << " Integrals over state-transfer operators calculated for mode: "
              << gOperatorDefs.GetModeLabel(i_g_mode) << std::endl;
         Mout << std::endl;

         //loop over one-mode operators for this mode
         for (In i_oper=0; i_oper<mpOpDef->NrOneModeOpers(i_op_mode); ++i_oper)
         {
            GlobalOperNr op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode,i_oper);
            const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(op_nr);
            //print all integrals
            Mout << " matrix representation for oper no. " << i_oper << " with label: " << this_oper->GetOrigOperString() << std::endl;
            Mout << std::endl;
            In add = mOffSetMap.find(std::make_pair(i_op_mode,op_nr))->second;
            mTheOneModeInt.DataIo(IO_GET,aM,nbas2,nbas,nbas,add);
            Mout << aM << std::endl;
         }
      }
      else
      {
         if (mpBasDef->OneModeType(i_bas_mode) == "HO") //
         {
            if (gDebug || gOperIoLevel > I_13)
            {
               Mout << " Harmonic oscillator integrals calculated for mode: " << gOperatorDefs.GetModeLabel(i_g_mode) << std::endl << std::endl;
            }
            //loop over one-mode operators for this mode
            for (In i_oper=0; i_oper<mpOpDef->NrOneModeOpers(i_op_mode); i_oper++)
            {
               GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode,i_oper);
               //print all integrals
               Mout << " matrix representation for oper no. " << i_oper << " with label: "
                    << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << endl;
               Mout << endl;
               if (mHoQiOnly)
               {
                  In ipow=0;
                  bool deriv = false;
                  bool cor_two_deriv = false;
                  bool cor_left_deriv = false;
                  bool cor_right_deriv = false;
                  bool is_cor_int_req;
                  bool is_valid = true;
                  GetHoInfo(g_op_nr,ipow,deriv,cor_two_deriv,cor_left_deriv,cor_right_deriv,is_cor_int_req);
                  In add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
                  Mout << " for oper: " << gOperatorDefs.GetOperLabel(g_op_nr) << " address is: " << add << endl << endl;
                  mTheOneModeInt.DataIo(IO_GET,aM,nbas2,nbas,nbas,add,I_1,false,C_0,1+mMaxNhigh);
                  //Mout << " integral before scaling: " << endl;
                  //Mout << aM << endl;
                  Nb mass = mpBasDef->GetMass(i_bas_mode);
                  Nb scal = mpBasDef->GetOmeg(i_bas_mode);
                  Nb omeg_sav=scal;
                  if (!deriv && mpOpDef->ScaledPot()) scal/=pow(mpOpDef->ScaleFactorForMode(i_op_mode),C_2);
                  Nb scale_fact1 = sqrt(mass*scal);
                  if (!deriv) scale_fact1 = C_1/scale_fact1;
                  Nb scale_fact = C_1;
                  for (In i=0;i<ipow;i++) scale_fact *= scale_fact1;
                  if ((deriv || mUseMassScaling) && !is_cor_int_req)
                  {
                     aM.Scale(scale_fact);
                  }
                  else if (is_cor_int_req)
                  {
                     if (cor_two_deriv)
                     {
                        scale_fact*=(mass*omeg_sav);
                        aM.Scale(scale_fact);
                     }
                     else if (cor_left_deriv || cor_right_deriv)
                     {
                        scale_fact*=sqrt(mass*omeg_sav);
                        aM.Scale(scale_fact);
                     }
                  }
               }
               else
               {
                  In add = mOffSetMap.find(std::make_pair(i_op_mode,g_op_nr))->second;
                  add += i_oper*nbas*nbas;
                  mTheOneModeInt.DataIo(IO_GET,aM,nbas2,nbas,nbas,add);
               }
               //debug
               //Mout << " integral value:  " << endl;
               Mout << aM << endl;
            }
         }
         else if (mpBasDef->OneModeType(i_bas_mode)=="Gauss" || mpBasDef->OneModeType(i_bas_mode)=="Bsplines")
         {
            Mout << " Integrals over non orthogonal basis (Gaussians/Bsplines)  calculated for mode: "
                 << gOperatorDefs.GetModeLabel(i_g_mode)<<endl;
            Mout << endl;
            //loop over one-mode operators for this mode
            for (In i_oper=0; i_oper<mpOpDef->NrOneModeOpers(i_op_mode); i_oper++)
            {
               GlobalOperNr op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode,i_oper);
               const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(op_nr);
               //print all integrals
               Mout << " matrix representation for oper no. " << i_oper << " with label: " << this_oper->GetOrigOperString() << endl;
               Mout << endl;
               In add = mOffSetMap.find(std::make_pair(i_op_mode,op_nr))->second;
               mTheOneModeInt.DataIo(IO_GET,aM,nbas2,nbas,nbas,add);
               Mout << aM << endl;
            }
         }
         else
         {
            Mout << "Integrals for basis type " << mpBasDef->OneModeType(i_bas_mode)
                 << " for mode " << gOperatorDefs.GetModeLabel(i_g_mode)
                 << " is not supported yet " << endl;
         }
      }
   }
}

/**
* Calculate the Q^i integrals over the B-splines for a particular aOperMode
* Includes both Q^i and (DDQ)^i - the later requires aDeriv = true, default is false.
* Coriolis integrals Q (DDQ) and (DDQ) Q  are calculated by aDeriv,aCoriolis = (f,t) and (t,t) respectively.
**/
void OneModeInt::BsplqiInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool aDeriv
   ,  bool aCoriolis
   )
{
   LocalModeNr i_op_mode = aOperMode;
   if (gDebug || gOperIoLevel > I_9)
   {
      Mout << " Integrals over B-splines";
      if (!aCoriolis && !aDeriv)
      {
         Mout << " Q^i ";
      }
      if (!aCoriolis && aDeriv)
      {
         Mout << " (DDQ)^i ";
      }
      Mout << "calculated for oper mode nr. " << aOperMode << std::endl;
   }
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In n_max = aNmax;
   In iord = mpBasDef->Nord(aBasMode);
   In n_int = mpBasDef->Nint(aBasMode);
   In nderiv = I_1;
   if (!aCoriolis && aDeriv)
   {
      nderiv = I_2;
   }
   //if fitting with scaling coordinates then scale_fact=sqrt(omeg)
   Nb scale_fact1 = C_1;
   if (mpOpDef->ScaledPot())
   {
      scale_fact1 *= mpOpDef->ScaleFactorForMode(i_op_mode);
   }
   if (gDebug)
   {
      Mout << " scale_fact " << scale_fact1 << std::endl;
   }
   if (gDebug)
   {
      Mout << " Integrals over the B-splines basis calculated for upto n_max = " << n_max << " and nbas= " << nbas << std::endl;
   }

   // Set Gaussian points and weights for Gauss-Legendre quadrature
   In n_gauss = mpBasDef->GetmBasGLQuadPnts();
   MidasVector x_g(n_gauss,C_0);
   MidasVector w_g(n_gauss,C_0);
   midas::math::GauLeg(x_g, w_g, -C_1, C_1, this->mBsplIntRelAccuracy);
   //get the grid of knots
   MidasVector t_grid;
   //Mout << " call getgridof points: " << endl;
   mpBasDef->GetVecOfPts(aBasMode,t_grid);
   //Mout << " exit getgridof points: " << endl;
   MidasMatrix ovlp(nbas);
   ovlp.Zero();
   //run over the intervals and generate the gaussian pnts and wgts
   for (In i_int = I_0; i_int < n_int; i_int++)
   {
      In itl = mpBasDef->GetLastInd(aBasMode, i_int);
      In ibl = I_0;
      if (mpBasDef->GetmKeepUnboundedBsplines())
      {
         ibl = itl - iord;
      }
      else
      {
         ibl = itl - iord - I_1;
      }

      In klow = I_1;
      if (i_int == I_0 && !mpBasDef->GetmKeepUnboundedBsplines())
      {
         klow = I_2;
      }
      Nb x1  = mpBasDef->GetKnot(aBasMode,itl-1);
      Nb x2  = mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode,i_int));

      Nb bma=(x2-x1)/C_2; // sne :BMinusAverage??
      Nb bpa=(x2+x1)/C_2;

      for (In i_g=0; i_g<x_g.Size(); i_g++)
      {
         Nb x=bpa+bma*x_g[i_g];
         Nb w=w_g[i_g]*bma;
         //Mout << " i_g: " << i_g << " x: " << x << endl;
         MidasMatrix da(iord,C_0);
         MidasMatrix dbi(iord,nderiv,C_0);
         //Mout << " iord: " << iord << " nderiv: " << nderiv << endl;
         //Mout << " enter Bsplvd " << endl;
         Bsplvd(t_grid,iord,x,itl,da,dbi,nderiv);
         //Mout << " dbi: " << endl;
         //Mout << dbi << endl;
         //Mout << " exit Bsplvd " << endl;
         In n_dim=iord;
         if (i_int == n_int - I_1 && !mpBasDef->GetmKeepUnboundedBsplines())
         {
            n_dim = iord - I_1;
         }
         for (In k=klow; k<=n_dim; k++)
         {
            In k1 = ibl + k;
            for (In l=klow; l<=n_dim; l++)
            {
               In l1 = ibl + l;
               Nb wbb=w*dbi[k-1][0]*dbi[l-1][0];
               ovlp[k1-1][l1-1]+=wbb;
            }
         }
      }
   }
   //Mout << " building overlap matrix: " << endl;
   //for (In i_r=0; i_r<ovlp.Nrows(); i_r++)
   //{
      //for (In i_c=0; i_c<ovlp.Ncols(); i_c++) Mout << ovlp[i_r][i_c] << " ";
      //Mout << endl;
   //}
   In power = I_0;
   In power_der = I_0;
   GlobalOperNr g_op_nr = gOperatorDefs.AddOneModeOper(power, power_der);

   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "BsplqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << ovlp.Norm() << " sumele: " << ovlp.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << ovlp << std::endl;
   }
   In i_add;
   if (!aDeriv && !aCoriolis)  //if Q^0, then save it
   {
      i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

      if (gOperIoLevel > I_9)
      {
         Mout << "  For mode " << i_op_mode << ", operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " (BsplqiInt 0): " << std::endl;

         Mout << "  The intqn norm is = " << ovlp.Norm() << " and the sum-over-elements is = " << ovlp.SumEle() << std::endl;
      }

      if (gDebug)
      {
         Mout << "  oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;
      }

      mTheOneModeInt.DataIo(IO_PUT, ovlp, nbas2, nbas, nbas, i_add);
   }
// sne note to print call

   if (n_max<1) return;

   MidasMatrix intqn(nbas);  // integrals for Q^1/(DDQ)^1
   intqn.Zero();
   for (In i_int=0; i_int<n_int; i_int++)
   {
      In itl=mpBasDef->GetLastInd(aBasMode,i_int);
      In ibl = I_0;
      if (mpBasDef->GetmKeepUnboundedBsplines())
      {
         ibl = itl - iord;
      }
      else
      {
         ibl = itl - iord - I_1;
      }

      In klow = I_1;
      if (i_int == I_0 && !mpBasDef->GetmKeepUnboundedBsplines())
      {
         klow = I_2;
      }
      Nb x1  = mpBasDef->GetKnot(aBasMode,itl-1);
      Nb x2  = mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode,i_int));
      //Mout << " for i_int: " << i_int << " x1: " << x1 << " x2: " << x2 << endl;
      Nb bma=(x2-x1)/C_2;
      Nb bpa=(x2+x1)/C_2;
      for (In i_g=0; i_g<x_g.Size(); i_g++)
      {
         Nb x=bpa+bma*x_g[i_g];
         Nb w=w_g[i_g]*bma;
         MidasMatrix da(iord,C_0);
         MidasMatrix dbi(iord,nderiv);
         Bsplvd(t_grid,iord,x,itl,da,dbi,nderiv);
         In n_dim=iord;
         if (i_int == n_int - I_1 && !mpBasDef->GetmKeepUnboundedBsplines())
         {
            n_dim = iord - I_1;
         }
         for (In k=klow; k<=n_dim; k++)
         {
            In k1=ibl+k;
            for (In l=klow; l<=n_dim; l++)
            {
               In l1=ibl+l;
               Nb wbb=C_0;
               if (!aDeriv && !aCoriolis) wbb=w*dbi[k-1][0]*dbi[l-1][0]*x;  //Q^1 integrals
               else if (!aCoriolis && aDeriv) wbb=w*dbi[k-1][0]*dbi[l-1][1]; //d/dq integral
               else if (aCoriolis && aDeriv) wbb=w*dbi[k-1][0]*(dbi[l-1][0]+x*dbi[l-1][1]); //ddqq integral
               else if (aCoriolis && !aDeriv) wbb=w*dbi[k-1][0]*x*dbi[l-1][1]; //qddq integral
               intqn[k1-1][l1-1]+=wbb;
            }
         }
      }
   }
   //Mout << " intqn: " << endl;
   //Mout << intqn << endl;
   //then apply the scaling factor if it is not a pure derivative
   if (!aDeriv && !aCoriolis) intqn.Scale(scale_fact1);
   //and save into the container
   if (!aDeriv) ++power;
   if (aDeriv) ++power_der;
   g_op_nr = gOperatorDefs.AddOneModeOper(power, power_der);
   i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

   if (gOperIoLevel > I_9)
   {
      Mout << "  For mode " << i_op_mode << ", operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " (BsplqiInt 1): " << std::endl;

      Mout << "  The intqn norm is = " << intqn.Norm() << " and the sum-over-elements is = " << intqn.SumEle() << std::endl;
   }

   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "  oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

      Mout << "  BsplqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << endl << intqn << std::endl;
   }

   if (!aCoriolis)
   {
      mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
   }
   if (n_max < I_2)
   {
      return;
   }

   intqn.Zero();
   //do Q^2 or (DDQ)^2
   for (In i_int=0; i_int<n_int; i_int++)
   {
      In itl = mpBasDef->GetLastInd(aBasMode,i_int);
      In ibl = I_0;
      if (mpBasDef->GetmKeepUnboundedBsplines())
      {
         ibl = itl - iord;
      }
      else
      {
         ibl = itl - iord - I_1;
      }

      In klow = I_1;
      if (i_int == I_0 && !mpBasDef->GetmKeepUnboundedBsplines())
      {
         klow = I_2;
      }
      Nb x1  = mpBasDef->GetKnot(aBasMode,itl-1);
      Nb x2  = mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode,i_int));
      Nb bma=(x2-x1)/C_2;
      Nb bpa=(x2+x1)/C_2;
      for (In i_g=0; i_g<x_g.Size(); i_g++)
      {
         Nb x=bpa+bma*x_g[i_g];
         Nb w=w_g[i_g]*bma;
         MidasMatrix da(iord,C_0);
         MidasMatrix dbi(iord,nderiv,C_0);
         Bsplvd(t_grid,iord,x,itl,da,dbi,nderiv);
         In n_dim=iord;
         if (i_int == n_int - I_1 && !mpBasDef->GetmKeepUnboundedBsplines())
         {
            n_dim = iord - I_1;
         }
         for (In k=klow; k<=n_dim; k++)
         {
            In k1=ibl+k;
            for (In l=klow; l<=n_dim; l++)
            {
               In l1=ibl+l;
               Nb wbb=C_0;
               if (!aDeriv && !aCoriolis) wbb=w*dbi[k-1][0]*dbi[l-1][0]*pow(x,C_2);   //Q^2 integrals
               else if (!aCoriolis && aDeriv) wbb=-w*dbi[k-1][1]*dbi[l-1][1];         //(DDQ)^2 integral
               intqn[k1-1][l1-1]+=wbb;
            }
         }
      }
   }
   //then apply the scaling factor if it is not a pure derivative
   if (!aDeriv && !aCoriolis)
   {
      intqn.Scale(pow(scale_fact1, C_2));
   }
   //and save into the container
   if (!aDeriv)
   {
      ++power;
   }
   if (!aCoriolis && aDeriv)
   {
      ++power_der;
   }
   g_op_nr = gOperatorDefs.AddOneModeOper(power, power_der);
   i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

   if (gOperIoLevel > I_9)
   {
      Mout << "  For mode " << i_op_mode << ", operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " (BsplqiInt 2): " << std::endl;

      Mout << "  The intqn norm is = " << intqn.Norm() << " and the sum-over-elements is = " << intqn.SumEle() << std::endl;
   }
   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "  oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

      Mout << "  BsplqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

      Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
   }

   mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
   if (n_max < I_3)
   {
      return;
   }

   //then calculate Q^n integrals up to assigned n_max
   if (!(!aCoriolis && !aDeriv)) MIDASERROR(" check value of nmax in calling BsplqiInt ");
   for (In i_q=3; i_q<=n_max; i_q++)
   {
      Nb scale_fact=pow(scale_fact1,Nb(i_q));
      //Mout << " scale_fact= " << scale_fact << endl;
      if (gDebug) Mout << " calculate integrals for i_q = " << i_q << " of " << n_max << endl;
      intqn.Zero();
      for (In i_int=0; i_int<n_int; i_int++)
      {
         In itl = mpBasDef->GetLastInd(aBasMode,i_int);
         In ibl = I_0;
         if (mpBasDef->GetmKeepUnboundedBsplines())
         {
            ibl = itl - iord;
         }
         else
         {
            ibl = itl - iord - I_1;
         }

         In klow = I_1;
         if (i_int == I_0 && !mpBasDef->GetmKeepUnboundedBsplines())
         {
            klow = I_2;
         }
         Nb x1=mpBasDef->GetKnot(aBasMode,itl-1);
         Nb x2=mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode,i_int));
         //Mout << " for i_int: " << i_int << " x1: " << x1 << " x2: " << x2 << endl;
         Nb bma=(x2-x1)/C_2;
         Nb bpa=(x2+x1)/C_2;
         for (In i_g=0; i_g<x_g.Size(); i_g++)
         {
            Nb x=bpa+bma*x_g[i_g];
            Nb w=w_g[i_g]*bma;
            MidasMatrix da(iord,C_0);
            MidasMatrix dbi(iord,nderiv,C_0);
            Bsplvd(t_grid,iord,x,itl,da,dbi,nderiv);
            In n_dim=iord;
            if (i_int == n_int - I_1 && !mpBasDef->GetmKeepUnboundedBsplines())
            {
               n_dim = iord - I_1;
            }
            for (In k=klow; k<=n_dim; k++)
            {
               In k1=ibl+k;
               for (In l=klow; l<=n_dim; l++)
               {
                  In l1=ibl+l;
                  Nb wbb=w*dbi[k-1][0]*dbi[l-1][0]*pow(x,i_q);
                  intqn[k1-1][l1-1]+=wbb;
               }
            }
         }
      }
      //then scale the resulting matrix
      intqn.Scale(scale_fact);

      //Store integrals on datacontainer.
      ++power;
      g_op_nr = gOperatorDefs.AddOneModeOper(power);
      i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

      if (gOperIoLevel > I_9)
      {
         Mout << "  For mode " << i_op_mode << ", operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " (BsplqiInt 3): " << std::endl;

         Mout << "  The intqn norm is = " << intqn.Norm() << " and the sum-over-elements is = " << intqn.SumEle() << std::endl;
      }
      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << "  oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "  BsplqiInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
      }

      if (!aDeriv && !aCoriolis)
      {
         mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
      }
      // prepare for next round.
   }
   return;
}
/**
* Calculate Coriolis integrals over B-spline functions for a particular aOperMode
* Includes the following integrals:
* Q^i (DDQ): with is_qddq=true
* (DDQ) Q^i with is_ddqq=true
* (DDQ) Q^i (DDQ) with is_ddqqddq=true
**/
void OneModeInt::BsplcorInt
   (  LocalModeNr aOperMode
   ,  In aNbas
   ,  In aNmax
   ,  In aBasMode
   ,  bool is_qddq
   ,  bool is_ddqq
   ,  bool is_ddqqddq
   )
{
   LocalModeNr i_op_mode = aOperMode;
   Mout << " Coriolis integrals over B-splines functions";
   In power = I_0;
   In r_der_pow = I_0;
   bool l_der = false;
   if (is_qddq)
   {
      Mout << " Q^i (DDQ) ";
      r_der_pow = I_1;
   }
   if (is_ddqq)
   {
      Mout << " (DDQ) Q^i ";
      l_der = true;
   }
   if (is_ddqqddq)
   {
      Mout << " (DDQ) Q^i (DDQ) ";
      l_der = true;
      r_der_pow = I_1;
   }
   Mout << "calculated for oper mode nr. " << aOperMode << std::endl;
   //check n_max is at least=1. Otherwise no new integras are calculated
   In n_max = aNmax;
   if (n_max <= I_0)
   {
      Mout << " No new integrals are calculated in BsplcorInt on return " << std::endl;
      return;
   }
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In iord = mpBasDef->Nord(aBasMode);
   In n_int = mpBasDef->Nint(aBasMode);
   In nderiv = I_2;
   // Scaling info. UPDATE ACCORDING TO INPUT
   //if fitting with scaling coordinates then scale_fact=sqrt(omeg)
   Nb scale_fact1 = C_1;
   if (mpOpDef->ScaledPot())
   {
      scale_fact1 *= mpOpDef->ScaleFactorForMode(i_op_mode);
   }
   if (gDebug)
   {
      Mout << " scale_fact " << scale_fact1 << std::endl;
   }
   if (gDebug)
   {
      Mout << " Coriolis integrals over B-splines functions calculated for upto n_max = " << n_max << " and nbas= " << nbas << std::endl;
   }

   // Set Gaussian points and weights for Gauss-Legendre quadrature
   In n_gauss = mpBasDef->GetmBasGLQuadPnts();
   MidasVector x_g(n_gauss, C_0);
   MidasVector w_g(n_gauss, C_0);
   midas::math::GauLeg(x_g, w_g, -C_1, C_1, this->mBsplIntRelAccuracy);
   //get the grid of knots
   MidasVector t_grid;
   //Mout << " call getgridof points: " << endl;
   mpBasDef->GetVecOfPts(aBasMode,t_grid);
   //Mout << " exit getgridof points: " << endl;
   MidasMatrix intqn(nbas);
   //intqn.Zero();
   //MidasMatrix tmp(nbas,iord);
   //tmp.Zero();
   //enter the loop over n.
   for (In i_q=1; i_q<=n_max; i_q++)
   {
      Nb scale_fact=pow(scale_fact1,Nb(i_q));
      //Mout << " scale_fact= " << scale_fact << endl;
      if (gDebug) Mout << " loop index i_q = " << i_q << " of " << n_max << endl;
      if (gDebug) Mout << " calculate integrals for i_q = " << i_q << " of " << n_max << endl;
      intqn.Zero();
      for (In i_int=0; i_int<n_int; i_int++)
      {
         In itl = mpBasDef->GetLastInd(aBasMode,i_int);
         In ibl = I_0;
         if (mpBasDef->GetmKeepUnboundedBsplines())
         {
            ibl = itl - iord;
         }
         else
         {
            ibl = itl - iord - I_1;
         }

         In klow = I_1;
         if (i_int == I_0 && !mpBasDef->GetmKeepUnboundedBsplines())
         {
            klow = I_2;
         }
         Nb x1=mpBasDef->GetKnot(aBasMode,itl-1);
         Nb x2=mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode,i_int));
         //Mout << " for i_int: " << i_int << " x1: " << x1 << " x2: " << x2 << endl;
         Nb bma=(x2-x1)/C_2;
         Nb bpa=(x2+x1)/C_2;
         for (In i_g=0; i_g<x_g.Size(); i_g++)
         {
            Nb x=bpa+bma*x_g[i_g];
            Nb w=w_g[i_g]*bma;
            MidasMatrix da(iord,C_0);
            MidasMatrix dbi(iord,nderiv,C_0);
            Bsplvd(t_grid,iord,x,itl,da,dbi,nderiv);
            In n_dim=iord;
            if (i_int == n_int - I_1 && !mpBasDef->GetmKeepUnboundedBsplines())
            {
               n_dim = iord - I_1;
            }
            for (In k=klow; k<=n_dim; k++)
            {
               In k1=ibl+k;
               for (In l=klow; l<=n_dim; l++)
               {
                  In l1=ibl+l;
                  //Mout << " k: " << k << " l:" << l << "k1: " << k1 << "l1: " << l1 << endl;
                  if (is_qddq)
                     intqn[k1-1][l1-1]+=w*dbi[k-1][0]*dbi[l-1][1]*pow(x,i_q);
                  else if (is_ddqq)
                     intqn[k1-1][l1-1]+=-w*dbi[k-1][1]*dbi[l-1][0]*pow(x,i_q);
                  else if (is_ddqqddq)
                     intqn[k1-1][l1-1]+=-w*dbi[k-1][1]*dbi[l-1][1]*pow(x,i_q);
               }
            }
         }
      }
      //then scale the resulting matrix
      intqn.Scale(scale_fact);
      //Store integrals on datacontainer.
      ++power;
      GlobalOperNr g_op_nr = gOperatorDefs.AddOneModeOper(power, r_der_pow, l_der);
      In i_add = mOffSetMap.find(std::make_pair(i_op_mode, g_op_nr))->second;

      if (gOperIoLevel > I_9)
      {
         Mout << "  For mode " << i_op_mode << ", operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " (BsplcorInt): " << std::endl;

         Mout << "  The intqn norm is = " << intqn.Norm() << " and the sum-over-elements is = " << intqn.SumEle() << std::endl;
      }
      if (gDebug || gOperIoLevel > I_14)
      {
         Mout << "  oper " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " integral address " << i_add << std::endl;

         Mout << "  BsplcorInt: "<< setw(10) << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " mode " << i_op_mode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

         Mout << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << std::endl << intqn << std::endl;
      }

      mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);
  }
  return;
}

/**
 *
**/
MidasVector OneModeInt::GetAllInt() const
{
   MidasVector the_integrals;
   mTheOneModeInt.DataIo(IO_GET, the_integrals);
   return the_integrals;
}

/**
 * Calculate generic integrals involving B-spline functions by means of Gauss-Legendre quadrature. This can be used to solve any general form of integrals.
**/
void OneModeInt::BsplGenInt
   (  const LocalModeNr& aOperMode
   ,  const In& aNbas
   ,  const LocalModeNr& aBasMode
   ,  const std::unique_ptr<OneModeOperBase<Nb>>& apOper
   ,  const GlobalOperNr& aGOpNr
   )
{
   // B-spline related variables
   LocalModeNr i_op_mode = aOperMode;
   In nbas = aNbas;
   In nbas2 = nbas*nbas;
   In iord = mpBasDef->Nord(aBasMode);
   In n_int = mpBasDef->Nint(aBasMode);

   // Are we integrating a delta function?
   bool delta_oper = apOper->Type() == OneModeOperBase<Nb>::OperType::DELTAFUNCTION;

   // Position of delta function. NB: delta_pos is given in non-frequency-scaled coordinates (like the grid bounds) and will not be scaled with scale_fact later.
   auto delta_pos = delta_oper ? static_cast<const DeltaFunctionOneModeOper<Nb>*>(apOper.get())->GetX() : C_0;

   // Initialize integral matrix
   MidasMatrix intqn(nbas);
   intqn.Zero();

   // Indicates that values of the B-spline itself and appropriate derivatives should be calculated up to but _not_ including the nderiv'th order derivative
   In nderiv = I_1;
   In deri = I_0;
   In derj = I_0;

   // Detailed output for each individual one-mode operator
   if (gDebug || gOperIoLevel > I_12)
   {
      Mout << std::endl << " Traits for one-mode operator " << apOper->OperString() << std::endl;
      Mout << "  Unit type operator:           " << apOper->GetmIsUnitOperator() << std::endl;
      Mout << "  Polynomial type operator:     " << apOper->IsPoly() << std::endl;
      Mout << "  Polynomial order:             " << apOper->GetPow() << std::endl;
      Mout << "  Right derivative order:       " << apOper->GetRDer() << std::endl;
      Mout << "  Left derivative:              " << apOper->GetLDer() << std::endl;
      Mout << "  Simple kinetic energy term:   " << apOper->GetmIsSimpleKeTerm() << std::endl;
      Mout << "  Coriolis coupling term:       " << apOper->GetmIsCoriolisTerm() << std::endl;
      Mout << "  PsCs kinetic energy operator: " << apOper->GetmIsPscKeTerm() << std::endl;
   }

   // The operator is a simple kinetic energy operator
   if (apOper->GetmIsSimpleKeTerm())
   {
      // First order derivatives on the B-splines are needed for these one-mode operators
      nderiv = I_2;

      // Set bra and ket state derivative orders
      if (apOper->GetRDer() == I_1)
      {
         derj = I_1;
      }
      else if (apOper->GetRDer() == I_2)
      {
         deri = I_1;
         derj = I_1;
      }
   }
   // The operator is part of the Watson (Coriolis coupling) operator
   else if (apOper->GetmIsCoriolisTerm())
   {
      // Set derivative order for B-splines
      nderiv = apOper->GetRDer() + I_1;

      // Set bra-state derivative order
      if (apOper->GetLDer())
      {
         deri = I_1;

         // At least first order derivatives on the B-splines will be needed to evaluate integrals with these types of one-mode operators
         if (nderiv == I_1)
         {
            nderiv = I_2;
         }
      }

      // Set ket-state derivative order
      derj = apOper->GetRDer();
   }

   // If fitting with scaled coordinates then scale_fact = sqrt(omeg), (see #1 SCALEFACTORS in .mop file)
   Nb scale_fact = C_1;
   if (mpOpDef->ScaledPot() && !apOper->GetmIsPscKeTerm())
   {
      scale_fact *= mpOpDef->ScaleFactorForMode(i_op_mode);
   }

   if (gDebug)
   {
      Mout << " Scale factor used in integral evaluation: " << scale_fact << std::endl;
   }

   // Get the knot sequence
   MidasVector t_grid;
   mpBasDef->GetVecOfPts(aBasMode, t_grid);

   // If we have a delta function, just evaluate without using Gauss-Legendre quadrature
   if (  delta_oper
      )
   {
      // Locate interval index containing the delta
      In i_int = I_0;
      In itl = I_0;
      for(; i_int < n_int; ++i_int)
      {
         itl = mpBasDef->GetLastInd(aBasMode, i_int);
         Nb x1 = mpBasDef->GetKnot(aBasMode, itl - I_1);
         Nb x2 = mpBasDef->GetKnot(aBasMode, itl);

         // Break if delta_pos is in the interval
         if (  libmda::numeric::float_geq(delta_pos, x1)
            && libmda::numeric::float_leq(delta_pos, x2)
            )
         {
            Mout  << " Delta-function interval found:\n"
                  << "    x = " << delta_pos << "\n"
                  << "    interval: " << "[" << x1 << ", " << x2 << "]\n"
                  << std::flush;
            break;
         }
      }
      // Containers for all non-derivative and derivative B-spline functions
      MidasMatrix da(iord, C_0);
      // Will contain B-splines of all possible orders and associated derivatives at a specific position
      MidasMatrix dbi(iord, nderiv, C_0);
      // Calculate B-splines at the specific position x of order iord, iord - 1,..., iord + 1 - nderiv, i.e. both the B-splines themselves and their derivatives are obtained
      Bsplvd(t_grid, iord, delta_pos, itl, da, dbi, nderiv);
      // Get the B-splines to loop over
      In klow = ((i_int == I_0) && !mpBasDef->GetmKeepUnboundedBsplines()) ? I_2 : I_1;
      In ibl = mpBasDef->GetmKeepUnboundedBsplines() ? itl - iord : itl - iord - I_1;
      In n_dim = ((i_int == n_int - I_1) && !mpBasDef->GetmKeepUnboundedBsplines()) ? iord - I_1 : iord;
      // Get sign of integral (if left derivative, we get a minus from integration by parts)
      Nb sign = apOper->GetLDer() ? -C_1 : C_1;
      // Loop over B-splines
      for(In k=klow; k<=n_dim; ++k)
      {
         In k1 = ibl + k;
         for(In l=klow; l<=n_dim; ++l)
         {
            In l1 = ibl + l;
            intqn[l1-I_1][k1-I_1] += sign * dbi[l-I_1][deri] * dbi[k-I_1][derj];
         }
      }
   }
   else
   {
      // Set Gaussian points and weights for Gauss-Legendre quadrature
      In n_gauss = mpBasDef->GetmBasGLQuadPnts();
      MidasVector x_g(n_gauss, C_0);
      MidasVector w_g(n_gauss, C_0);

      // Generate the Gaussian points and weight
      midas::math::GauLeg(x_g, w_g, -C_1, C_1, this->mBsplIntRelAccuracy);

      // Run over the intervals defined by the breakpoint sequence and generate the Gaussian points and weights
      for (In i_int = I_0; i_int < n_int; i_int++)
      {
         // Get index of the last B-spline that is not zero in the interval i_int
         In itl = mpBasDef->GetLastInd(aBasMode, i_int);

         // Get index of the first B-spline that is not zero in the interval i_int
         In ibl = I_0;
         if (mpBasDef->GetmKeepUnboundedBsplines())
         {
            ibl = itl - iord;
         }
         else
         {
            ibl = itl - iord - I_1;
         }

         // The lowest order of the B-spline functions is 1
         In klow = I_1;

         // The first interval (specified by the breakpoint sequence) is special in the sense that we want to exclude the first B-spline function as this is unbounded for clamped knot sequences
         if (  i_int == I_0
            && !mpBasDef->GetmKeepUnboundedBsplines()
            )
         {
            klow = I_2;
         }

         // Get first and last knot position for the i_int interval, i.e. the integration limits
         Nb x1 = mpBasDef->GetKnot(aBasMode, itl - I_1);
         Nb x2 = mpBasDef->GetKnot(aBasMode, mpBasDef->GetLastInd(aBasMode, i_int));

         // Gauss-Legendre trick to change general integration interval from [a,b] to [-1,1]
         Nb bma = (x2 - x1)/C_2;
         Nb bpa = (x2 + x1)/C_2;

         // Run over the Gauss-Legendre quadrature points
         for (In i_g = I_0; i_g < x_g.Size(); i_g++)
         {
            // Due to the Gauss-Legendre trick, Gaussian points and weights are modified
            Nb x = bpa + bma*x_g[i_g];
            Nb w = w_g[i_g]*bma;

            // Containers for all non-derivative and derivative B-spline functions
            MidasMatrix da(iord, C_0);

            // Will contain B-splines of all possible orders and associated derivatives at a specific position
            MidasMatrix dbi(iord, nderiv, C_0);

            // Calculate B-splines at the specific position x of order iord, iord - 1,..., iord + 1 - nderiv, i.e. both the B-splines themselves and their derivatives are obtained
            Bsplvd(t_grid, iord, x, itl, da, dbi, nderiv);

            // The order of the B-spline functions indicates how many B-spline functions will be non-zero for a given interval and thereby how many functions will contribute to the integral evaluation
            In n_dim = iord;

            // The last interval (specified by the breakpoint sequence) is special in the sense that we want to exclude the last B-spline function as this is unbounded for clamped knot sequences
            if (  i_int == n_int - I_1
               && !mpBasDef->GetmKeepUnboundedBsplines()
               )
            {
               // Taking the derivative of a B-spline reduces the order by 1
               n_dim = iord - I_1;
            }

            // Locate the correct B-spline coefficient value and extract this in order to evaluate the expectation value of an one-mode operator function sandwiched between a bra and a ket state represented by B-spline functions
            for (In k = klow; k <= n_dim; k++)
            {
               In k1 = ibl + k;
               for (In l = klow; l <= n_dim; l++)
               {
                  In l1 = ibl + l;

                  intqn[l1 - I_1][k1 - I_1] += EvaluateExpectationValue(apOper, dbi, deri, derj, w, x, k, l, scale_fact);
               }
            }
         }
      }
   }

   // Get address for storing evaluated integrals in data container
   In i_add = mOffSetMap.find(std::make_pair(i_op_mode, aGOpNr))->second;

   if (gOperIoLevel > I_9)
   {
      Mout << " For mode " << i_op_mode << ", operator " << apOper->GetOrigOperString() << ": " << std::endl;

      Mout << "  The intqn norm is = " << intqn.Norm() << " and the sum-over-elements is = " << intqn.SumEle() << std::endl;
   }

   if (gDebug || gOperIoLevel > I_14)
   {
      Mout << "  oper " << apOper->OperString() << " integral address " << i_add << std::endl;

      Mout << "  BsplGenInt: "<< setw(I_10) << apOper->OperString() << " mode " << i_op_mode << " norm: " << intqn.Norm() << " sumele: " << intqn.SumEle() << std::endl;

      Mout << apOper->OperString() << std::endl << intqn << std::endl;
   }

   // Store evaluated integrals in data container
   mTheOneModeInt.DataIo(IO_PUT, intqn, nbas2, nbas, nbas, i_add);

   return;
}

/**
 * Evaluates and return the expectation value of an one-mode operator function sandwiched between a bra and a ket state represented by B-spline functions. 
 * Note that this function takes care of handling integrals for any rectilinear coordinates
 *
 * @param apOperMode
 *    A one-mode operator.
 * @param aDerBsplInt
 *    Derivative B-spline integral at function value.
 * @param aLeftDerOrder
 *    In case of a derivative situated to the left of the function, indicates the derivative order.
 * @param aRightDerOrder
 *    In case of a derivative situated to the right of the function, indicates the derivative order.
 * @param aIntWeight
 *    Weight of the associated term, (in practice Gauss-Legendre quadrature weigts for general integration intervals).
 * @param aIntPoint
 *    Point of integration for which a function value is to be obtained.
 * @param aFirstBsplIndx
 *    Matrix index for retrieving correct derivative B-spline function.
 * @param aSecondBsplIndx
 *    Matrix index for retrieving correct derivative B-spline function.
 * @param aScaleFact
 *    In case of scaled coordinates, use scaling factor.
 * @return
 *    The integral evaluated over the given one-mode operator.
**/
Nb OneModeInt::EvaluateExpectationValue
   (  const std::unique_ptr<OneModeOperBase<Nb>>& apOperMode
   ,  const MidasMatrix& aDerBsplInt
   ,  const In& aLeftDerOrder
   ,  const In& aRightDerOrder
   ,  const Nb& aIntWeight
   ,  const Nb& aIntPoint
   ,  const In& aFirstBsplIndx
   ,  const In& aSecondBsplIndx
   ,  const Nb& aScaleFact
   )  const
{
   // Do (DDQ)^1 integrals:
   if (apOperMode->GetRDer() == I_1 && !apOperMode->GetLDer() && apOperMode->GetPow() == I_0)
   {
      return aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder];
   }
   // Do (DDQ)^2 integrals, (with integration by parts):
   else if (apOperMode->GetRDer() == I_2 && !apOperMode->GetLDer() && apOperMode->GetPow() == I_0)
   {
      return -aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder];
   }

   // If the function has not returned yet, we need a function evaluation
   Nb f_value = apOperMode->EvaluateFunc(aScaleFact * aIntPoint);

   // Do f(Q) (DDQ)^j integrals:
   if (apOperMode->GetRDer() != I_0 && !apOperMode->GetLDer() && apOperMode->GetPow() != I_0)
   {
      return aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder] * f_value;
   }
   // Do (DDQ)^i f(Q) integrals, (with integration by parts):
   else if (apOperMode->GetRDer() == I_0 && apOperMode->GetLDer() && apOperMode->GetPow() != I_0)
   {
      return -aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder] * f_value;
   }
   // Do (DDQ)^i f(Q) (DDQ)^j integrals, (with integration by parts):
   else if (apOperMode->GetRDer() != I_0 && apOperMode->GetLDer() && apOperMode->GetPow() != I_0)
   {
      return -aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder] * f_value;
   }
   // Do f(Q) integrals, (ordinary one-mode operators):
   else
   {
      return aIntWeight * aDerBsplInt[aFirstBsplIndx - I_1][aRightDerOrder] * aDerBsplInt[aSecondBsplIndx - I_1][aLeftDerOrder] * f_value;
   }
}

void OneModeInt::GetHoInfo
   (  const GlobalOperNr aOper
   ,  In& arPow
   ,  bool& arDeriv
   ,  bool& arCorTwoDer
   ,  bool& arCorLDer
   ,  bool& arCorRDer
   ,  bool& arCorReq
   )  const
{
   bool is_valid = true;
   const std::unique_ptr<OneModeOperBase<Nb>>& this_oper = gOperatorDefs.GetOneModeOper(aOper);
   if (this_oper->GetLDer())
   {
      if (this_oper->GetRDer() == I_1)
      {
         arCorTwoDer = true;
      }
      else if (this_oper->GetRDer() == I_0)
      {
         arCorLDer = true;
      }
      else
      {
         is_valid = false;
      }
   }
   else if (this_oper->GetRDer() > I_0)
   {
      if (this_oper->GetPow() > I_0 && this_oper->GetRDer() == I_1)
      {
         arCorRDer = true;
      }
      else if (this_oper->GetPow() == I_0)
      {
         arPow = this_oper->GetRDer();
         arDeriv = true;
      }
      else
      {
         is_valid = false;
      }
   }
   if (arPow == I_0)
   {
      arPow = this_oper->GetPow();
   }
   if (!is_valid)
   {
      MIDASERROR("mHoQiOnly block of OneModeInt::GetInt oper_name not q or ddq or belonging to the Coriolis set ");
   }
   arCorReq = (arCorTwoDer || arCorRDer || arCorLDer);
}

/**
 * Set the integrals of state-transfer operators
 *
 * @param aMode
 **/
void OneModeInt::SetStateTransferIntegrals
   (  LocalModeNr aMode
   )
{
   auto nop = this->mpOpDef->NrOneModeOpers(aMode);
   for (LocalOperNr iop=I_0; iop<nop; ++iop)
   {
      GlobalOperNr g_op_nr = this->mpOpDef->GetOneModeOperGlobalNr(aMode, iop);
      const auto& this_oper = gOperatorDefs.GetOneModeOper(g_op_nr);

      bool unit_op = false;
      if (  this_oper->Type() != OneModeOperBase<Nb>::OperType::STATETRANSFER
         )
      {
         if (  this_oper->GetmIsUnitOperator()
            )
         {
            unit_op = true;
         }
         else
         {
            MIDASERROR("SetStateTransferIntegrals can only handle |I><J| operators or the unit operator!");
         }
      }

      // Set the values.
      In nbas = this->mpOpDef->StateTransferIJMax(aMode) + I_1;
      In nbas2 = nbas*nbas;
      MidasMatrix integrals(nbas);

      if (  unit_op
         )
      {
         integrals.Unit();
      }
      else
      {
         integrals.Zero();
         const auto& st_oper_cast = static_cast<const StateTransferOneModeOper<Nb>&>(*this_oper);
         auto i = st_oper_cast.GetI();
         auto j = st_oper_cast.GetJ();

         integrals[i][j] = C_1;
      }

      // Get address of integral block
      In i_add = mOffSetMap.find(std::make_pair(aMode, g_op_nr))->second;
      mTheOneModeInt.DataIo(IO_PUT, integrals, nbas2, nbas, nbas, i_add);
   }
}
