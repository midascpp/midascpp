/**
************************************************************************
*
* @file                OneModeInt.h
*
* Created:             23-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for integral calculations etc.
*
* Last modified: Mon Dec 11, 2006  08:42PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ONEMODEINT_H
#define ONEMODEINT_H

// std headers
#include <string>
#include <vector>
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "mmv/DataCont.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

class OneModeInt
{
   private:
      //!  Storage mode (inmem, ondisc)
      std::string             mStorage;

      //!  The one mode integrals
      DataCont                mTheOneModeInt;

      //!  Pointer to OpDef object
      const OpDef* const      mpOpDef;

      //!  Pointer to BasDef object
      const BasDef* const     mpBasDef;

      //!  OneModeInt name
      std::string             mOneModeIntName;

      //!  Number of modes in integral
      In                      mNoModesInInt;

      //!  Total number of one-mode operators
      In                      mTotNoOneModeOpers;

      //!  Total number of integrals
      In                      mTotNoOneModeIntegrals;

      //!  The largest highest max among the modes
      In                      mMaxNhigh;

      //!  The largest nmax for i, Q^i among all modes
      In                      mMaxQi;

      //!  The largest nmax for i, (DDQ)^i among all modes
      In                      mMaxddQi;

      //!  The largest nmax for i, (DDQ) Q^i integrals
      In                      mMaxddQQi;

      //!  The largest nmax for i, Q^i (DDQ) integrals
      In                      mMaxQiddQ;

      //!  The largest nmax for i, (DDQ) Q^i (DDQ) integrals
      In                      mMaxddQQiddQ;

      //!  Address for occupied modal.
      std::vector<In>         mOneModeIntOccOff;

      //!  Numbering of modes global numbering
      std::vector<In>         mGlobalModeNrs;

      //!  Only <HO|Q^i|HO> integrals required
      bool                    mHoQiOnly;

      //! Use mass scaling of the integrals, (only applicable when mHoQiOnly = true)
      bool                    mUseMassScaling;

      //!
      std::map<std::pair<LocalModeNr, GlobalOperNr>, In>       mOffSetMap;

      //! Accuracy of Gauss-Legendre points relative to numeric epsilon
      unsigned mBsplIntRelAccuracy = 10;

      //! Get information on which kind of integrals needs to be solved for a harmonic oscillator primitive basis
      void GetHoInfo(const GlobalOperNr, In&, bool&, bool&, bool&, bool&, bool&) const;

      //! Generate address of an integral associated with a particular operator
      void GenIntOffSets();

      //! Set all integrals to zero
      void ZeroInt()
      {
         mTheOneModeInt.Zero();
      }

      //! Check whether or not an operator should be part of the sum-over-states evaluation
      bool CheckSoSOper(GlobalOperNr);

      //! Get the correct elements for used in the sum-over-states evaluation
      void GetSoSCompOpers(const GlobalOperNr, const LocalModeNr, LocalOperNr&, LocalOperNr&);

      //! Evaluate the sum-over-states for coriolis integrals
      void EvalSos();

      //@{
      //! Calculate one-mode integrals for a wave function represented by different primitive bases
      void HoqiInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasisMode, bool aDeriv = false, bool aCoriolis = false);
      void HocorInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasMode, bool is_qddq = false, bool is_ddqq = false, bool is_ddqqddq = false);
      void GaussqiInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasMode, bool aDeriv = false, bool aCoriolis = false);
      void GausscorInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasMode, bool is_qddq = false, bool is_ddqq = false, bool is_ddqqddq = false);
      void BsplqiInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasMode, bool aDeriv = false, bool aCoriolis = false);
      void BsplcorInt(LocalModeNr aOperMode, In aNbas, In aNmax, In aBasMode, bool is_qddq = false, bool is_ddqq = false, bool is_ddqqddq = false);
      void BsplGenInt(const LocalModeNr&, const In&, const LocalModeNr&, const std::unique_ptr<OneModeOperBase<Nb>>&, const GlobalOperNr&);
      void SetStateTransferIntegrals(LocalModeNr);
      //@}

      //! Evaluate the expectation value for wave functions expressed in a B-spline primitive basis with general one-mode operators
      Nb EvaluateExpectationValue
         (  const std::unique_ptr<OneModeOperBase<Nb>>& apOperMode
         ,  const MidasMatrix& aDerBsplInt
         ,  const In& aLeftDerOrder
         ,  const In& aRightDerOrder
         ,  const Nb& aIntWeight
         ,  const Nb& aIntPoint
         ,  const In& aFirstBsplIndx
         ,  const In& aSecondBsplIndx
         ,  const Nb& aScaleFact
         )  const;

   public:
      //! Constructor from operator and basis pointers
      OneModeInt
         (  const OpDef* const
         ,  const BasDef* const
         ,  std::string aStorage = "InMem"
         ,  bool aSaveUponDecon = true
         );

      //@{
      //! The defaults do what they should; the data in DataCont takes care of itself.
      OneModeInt(OneModeInt&&) = default;
      ~OneModeInt() = default;
      //@}

      //@{
      //! The copy'ers are not necessary, so deleted for now. Enable if need be.
      OneModeInt(const OneModeInt&) = delete;
      OneModeInt& operator=(const OneModeInt&) = delete;
      //@}

      //! Calculate all one-mode integrals
      void CalcInt();

      //! Get integrals from previous calculation. restart ok ? is returned
      bool RestartInt();

      //! Multiply the integrals by a scale factor (?)
      void AddIntTo(MidasMatrix& aMidasMatrix, const LocalModeNr& arOperMode, const LocalOperNr& arOperNr, const Nb& aScaleFact = C_1);

      //! Get the integrals and return the bracket "<vec|int|vec>"
      void IntBracket(MidasVector& aMidasVector, const LocalModeNr arOperMode, const LocalOperNr arOperNr, Nb& aResult);

      //! Get the integrals for a specific operator
      void GetInt(const LocalModeNr aOperMode, const LocalOperNr aOperNr, MidasMatrix& aM, bool aNoScale = false) const;

      //! Get the integrals and indices for a specific operator
      void GetInt(const LocalModeNr aOperMode, const LocalOperNr aOperNr, const In& aLeftIdx, const In& aRightIdx, Nb& aIntegral, bool aNoScale = false) const;

      //! Print all the integrals
      void PrintInt() const;

      //!
      MidasVector GetAllInt() const;

      //! Use mass scaling
      const bool GetmUseMassScaling() const {return mUseMassScaling;}

      //@{
      //! OneModeInt class getters
      const OpDef* const GetmpOpDef() const                 {return mpOpDef;}
      const BasDef* const GetmpBasDef() const               {return mpBasDef;}
      const std::string OperName() const                    {return mpOpDef->Name();}
      const std::string& BasisName() const                  {return mpBasDef->GetmBasName();}
      const std::string& GetmOneModeIntName() const         {return mOneModeIntName;}
      const In& GetmNoModesInInt() const                    {return mNoModesInInt;}
      const In& GetmTotNoOneModeOpers() const               {return mTotNoOneModeOpers;}
      const In& GetmTotNoOneModeIntegrals() const           {return mTotNoOneModeIntegrals;}
      const std::vector<In>& GetOneModeIntOccOff() const    {return mOneModeIntOccOff;}
      //@}
};

#endif
