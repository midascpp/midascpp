/**
************************************************************************
* 
* @file                Ode1D.cc
*
* Created:             24-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Class for ordinary differential eq. in 1D 
* 
* Last modified: man mar 21, 2005  11:51
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ni/Ode1D.h"
#include "ni/NonLinEq1D.h"
using NonLinEq1D::Bisect;
#include "util/Io.h"


Ode1D::Ode1D(Nb t0, Nb x0, Nb t, Nb (*f)(Nb, Nb))
{
   mTimeInit = t0;
   mSolInit  = x0;
   mTimeEnd  = t;
   msfn      = f;
}

/**
* Note thats this function returns the full set of points!
* */
// ian: This function returns a pointer.... remember to delete after use !!!
Nb* Ode1D::Euler(In n) const
{
   Nb* x = new Nb [n+I_1];
   Nb h = (mTimeEnd - mTimeInit)/n;
   x[I_0] = mSolInit;
   for (In i = I_0; i < n; i++)
   {
      x[i+I_1] = x[i] + h*msfn(mTimeInit + i*h,x[i]);
   }
   return x;
}

// ian: This function returns a pointer.... remember to delete after use !!!
Nb* Ode1D::EulerImp(In n) const 
{
   Nb* x = new Nb [n+I_1];
   Nb h = (mTimeEnd - mTimeInit)/n;
   x[I_0] = mSolInit;
   Nb delta = C_I_10_5;
   Nb eps   = C_I_10_6;
   for (In i = I_0; i < n; i++)
   {
      Nb tnew = mTimeInit + i*h;
      Nb dx = h*msfn(tnew,x[i]);
      Nb xtry = x[i] + dx;
      // Solve 0 = -x_i+1 + x_i + h_k * f(t_i+1,x_i+1), Bisection method
      In MaxIt = I_100;
      Nb a = xtry-dx/C_2;
      Nb b = xtry+dx/C_2;
      Nb u = msfn(tnew,a)*h+x[i]-a;
      Nb he = b-a;    
      Nb c = C_0;
      for (In j=I_1;j<=MaxIt;j++)
      {
         he *= C_I_2; 
         c = a+he;
         Nb w = msfn(tnew,c)*h+x[i]-c;
         //Mout << " a, b, c, u, w, he,h: " << a << " " << b << " " << c << " " << u << " " << w << " " << he << " " << h<< endl;
         if (fabs(he)<delta && fabs(w) < eps) 
         {
            break;
         } else
         {
            ((u>C_0 && w<C_0) || (u<C_0 && w>C_0)) ? (b=c): (a=c,u=w);
            if (j==MaxIt) Mout << " Non-convergence in Ode1D::EulerImp" << endl;
         }
      }
      x[i+I_1] = c;
      //Mout << " step : " << c << endl;
   }
   return x;
}

// ian: This function returns a pointer.... remember to delete after use !!!
Nb* Ode1D::EulerPc(In n) const
{
   Nb* x = new Nb [n+I_1];
   Nb h = (mTimeEnd - mTimeInit)/n;
   x[I_0] = mSolInit;
   for (In i = I_0; i < n; i++)
   {
      x[i+I_1] = x[i] + h*msfn(mTimeInit + i*h,x[i]);
      x[i+I_1] = x[i] + h*msfn(mTimeInit + (i+I_1)*h,x[i+I_1]);
   }
   return x;
}

// ian: This function returns a pointer.... remember to delete after use !!!
Nb* Ode1D::Rk2(In n) const
{
   Nb* x = new Nb [n+I_1];
   Nb h = (mTimeEnd - mTimeInit)/n;
   x[I_0] = mSolInit;
   for (In i = I_0; i < n; i++)
   {
      Nb t = mTimeInit + i*h;
      Nb F1 = h*msfn(t,x[i]);
      Nb F2 = h*msfn(t+h,x[i]+F1);
      x[i+I_1] = x[i] + C_I_2*(F1+F2);
   }
   return x;
}

// ian: This function returns a pointer.... remember to delete after use !!!
Nb* Ode1D::Rk4(In n) const
{
   Nb* x = new Nb [n+I_1];
   Nb h = (mTimeEnd - mTimeInit)/n;
   x[I_0] = mSolInit;
   for (In i = I_0; i < n; i++)
   {
      Nb t = mTimeInit + i*h;
      Nb F1 = h*msfn(t,x[i]);
      Nb F2 = h*msfn(t+h*C_I_2,x[i]+F1*C_I_2);
      Nb F3 = h*msfn(t+h*C_I_2,x[i]+F2*C_I_2);
      Nb F4 = h*msfn(t+h,x[i]+F3);
      x[i+I_1] = x[i] + (F1+C_2*F2+C_2*F2+F4)/C_6;
   }
   return x;
}
