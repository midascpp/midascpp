/**
************************************************************************
* 
* @file                Tdecomp.h 
*
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   tensor decomposition user interface class 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TDECOMP_H
#define TDECOMP_H

void TdecompDrv();

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "input/TdecompCalcDef.h"
#include "ecor/TurboInfo.h"
#include "ecor/RIDecomposer.h"

class Tdecomp
{
  private:
//CalcDefs

     TdecompCalcDef*   mpTdecompCalcDef;   ///< Pointer to input/setup info 


  public:
    Tdecomp(TdecompCalcDef* apCalcDef);      ///< Constructor 

    void Decompose();          // do the requested decomposition

//Getters
    const TdecompCalcDef* GetTdecompCalcDef() {return mpTdecompCalcDef;}
};


#endif
