/**
************************************************************************
* 
* @file                TdecompDrv.cc
*
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Driver for direct use of tensor decompsitions 
*                      in MidasCPP 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// My headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "input/Input.h"
#include "input/TdecompCalcDef.h"
#include "tdecomp/Tdecomp.h"

/**
* Run scf calc 
* */
void TdecompDrv()
{

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Start interface for tensor decomposition ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
  Mout << "\n\n";


  for (In i_calc = 0; i_calc<gTdecompCalcDef.size(); i_calc++) 
  {  

/*
***********   Create Tdecomp object  **************************
*/
    Tdecomp tdecomp(&gTdecompCalcDef[i_calc]);

/*
***********   Start tensor decomposition  *********************
*/

    Timer time_it;
    tdecomp.Decompose();
    string s_time = " CPU time used in tensor decomposition interface :";
    time_it.CpuOut(Mout,s_time);

  }

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Tensor decomposition done ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
}

