/**
************************************************************************
* 
* @file                Tdecomp.cc
*
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Implementation of the user interface for
*                      tensor decompositions
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>
#include<list>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "tdecomp/Tdecomp.h"
#include "input/TdecompCalcDef.h"
#include "tensor/NiceTensor_vector.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "it_solver/nl_solver/RankStatistics.h"
#include "vcc/TensorDataCont.h"
#include "tensor/TensorDecomposer.h"

#include "tensor/hooi.h"
#include "lapack_interface/packed_matrix.h"

namespace X=contraction_indices;

/**
* Default "zero" Constructor 
* */
Tdecomp::Tdecomp(TdecompCalcDef* apCalcDef):
   mpTdecompCalcDef(apCalcDef)
{
}

/**
* "Decompose"
* */
void Tdecomp::Decompose()
{

   if (  mpTdecompCalcDef->GetTensorFileType() == "turbomole"  )
   {
      Mout << " I have a turbomole RI integral file (Q,mu nu)" << std::endl;
      Mout << std::endl;

      std::string filename = mpTdecompCalcDef->GetTensorFileName();

      // init Turbomole reader
      TurboInfo turboinfo;

      // read info file
      std::string filinfo = filename + ".aux"; 
      turboinfo.ReadInfo(filinfo);

      // read (Q,mu nu)
      NiceTensor<Nb> bqint = turboinfo.Read3RI(filename);

      ///////////////////////////////////////////////////////////////
      // decompose (Q,mu nu)
      ///////////////////////////////////////////////////////////////

      // init decomposer
      RIDecomposer ridecomposer;
      ridecomposer.SetDecompInfoSet(mpTdecompCalcDef->GetDecompInfoSet());

      In naux = turboinfo.NAux();  
      In nsbf = turboinfo.NBas();  

      // do decomposition
      ridecomposer.Decompose(bqint);

   }
   else if  (  mpTdecompCalcDef->GetTensorFileType() == "tensordatacont"   )
   {
      // Init timer
      Timer decomp_timer;

      // Get file name and read tensor
      std::string filename = mpTdecompCalcDef->GetTensorFileName();
      Mout << "Reading TensorDataCont from file: " << filename << std::endl;

      TensorDataCont tdc;
      tdc.ReadFromDisc( filename );
      auto ref = tdc;
      RankStatistics rstat( "RankStatistics before and after decomposition" );

      // Decompose
      rstat.EmplaceRankInfo(tdc);
      decomp_timer.Reset();
      tdc.Decompose( mpTdecompCalcDef->GetDecompInfoSet() );
      decomp_timer.CpuOut(Mout, "CPU time spent on decomposing tensor:  ");
      decomp_timer.WallOut(Mout, "Wall time spent on decomposing tensor: ");
      rstat.EmplaceRankInfo(tdc);

      // Write decomposed TensorDataCont to disk
      std::string new_filename( this->mpTdecompCalcDef->GetName()+"_decomposed_tensordatacont" );
      tdc.WriteToDisc(new_filename);
      Mout << "Decomposed tensor written to file: " << new_filename << std::endl;
      Mout << std::endl;

      // Calculate error norm (the safe way)
      auto diff_norm2 = C_0;
      for(size_t i=0; i<tdc.Size(); ++i)
      {
         diff_norm2 += safe_diff_norm2<Nb>(tdc.GetModeCombiData(i).GetTensor(), ref.GetModeCombiData(i).GetTensor());
      }
      auto diff_norm = std::sqrt(diff_norm2);
      auto ref_norm = ref.Norm();

      Mout  << "Final error norm:\n"
            << "   Absolute       = " << diff_norm << "\n"
            << "   Norm relative  = " << diff_norm / ref_norm << "\n"
            << std::endl;

      Mout << std::setprecision(3) << rstat << std::endl;
   }
   else if  (  mpTdecompCalcDef->GetTensorFileType() == "nicetensor"   )
   {
      // Init timer
      Timer decomp_timer;

      // Get file name
      std::string filename = mpTdecompCalcDef->GetTensorFileName();
      Mout << "Reading NiceTensor from file: " << filename << std::endl;

      // Get input stream and read tensor
      std::ifstream input(filename, std::ios::in | std::ios::binary );
      NiceTensor<Nb> tensor_in(input);
      input.close();

      // Get values for output
      auto ref_norm = tensor_in.Norm();
      auto input_rank = -I_1;
      switch(tensor_in.Type())
      {
         case BaseTensor<Nb>::typeID::SIMPLE:
         {
            input_rank = -I_1;
            break;
         }
         case BaseTensor<Nb>::typeID::CANONICAL:
         {
            input_rank = tensor_in.StaticCast<CanonicalTensor<Nb>>().GetRank();
            break;
         }
         case BaseTensor<Nb>::typeID::SUM:
         {
            input_rank = tensor_in.StaticCast<TensorSum<Nb>>().Rank();
            break;
         }
         case BaseTensor<Nb>::typeID::DIRPROD:
         {
            input_rank = tensor_in.StaticCast<TensorDirectProduct<Nb>>().Rank();
            break;
         }
         default:
         {
            MIDASERROR("Tdecomp: Unrecognized tensor format!");
         }
      }

      // Decompose tensor
      auto ndim = tensor_in.NDim();
      Mout << "Decomposing " << ndim << ".-order " << tensor_in.ShowType() << " :" << std::endl;
      Mout << "Dimensions of tensor: " << tensor_in.ShowDims() << std::endl;
      midas::tensor::TensorDecomposer decomposer(mpTdecompCalcDef->GetDecompInfoSet());
      decomp_timer.Reset();
      auto tensor_out = decomposer.Decompose(tensor_in);
      auto cpu_time = decomp_timer.CpuTime();
      auto wall_time = decomp_timer.WallTime();

      // Write result to disk
      std::string out_name( this->mpTdecompCalcDef->GetName()+"_decomposed_tensor" );
      std::ofstream output( out_name, std::ios::out | std::ios::binary );
      tensor_out.Write(output);
      output.close();

      // Get output values
      auto out_rank = tensor_out.StaticCast<CanonicalTensor<Nb>>().GetRank();
      auto diff_norm = std::sqrt( safe_diff_norm2<Nb>(tensor_in.GetTensor(), tensor_out.GetTensor()) );

      // Write output
      Mout  << "Results of tensor decomposition:" << "\n"
            << "\t||T_in - T_out||              = " << diff_norm << "\n"
            << "\t||T_in - T_out||/||T_in||     = " << diff_norm / ref_norm << "\n"
            << "\tInput rank                    = " << input_rank << "\n"
            << "\tOutput rank                   = " << out_rank << "\n"
            << "\tCPU time                      = " << cpu_time << "\n"
            << "\tWall time                     = " << wall_time << std::endl;
   }
   else
   {
      Mout << " Unknown type for tensorfile " << std::endl;
      MIDASERROR(" Check your input please "); 
   }
}






