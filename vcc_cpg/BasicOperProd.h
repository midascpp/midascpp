/**
************************************************************************
* 
* @file                BasicOperProd.h
*
* Created:             13-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   BasicOperProd class: E.g. T2 * H3 * R2 * T2.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASICOPERPROD_H
#define BASICOPERPROD_H

#include <iostream>
#include <vector>
#include <map>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/CtrProd.h"

class BasicOperProd
{
   protected:
      Nb                     mCoef;
      std::vector<BasicOper> mLT;          ///< Excitation operators to the left of H.
      In                     mHlevel;      ///< Excitation level of H.
      std::vector<BasicOper> mRT;          ///< Excitation operators to the right of H.
      bool                   mCmtOrigin;   ///< Product originates from commutator expansion.
      
      void InitCtrProd(CtrProd& aCP) const;
      ///< Initialize a "bare" contraction product based on this operator product.

      // These functions are used by GetCtrProds().
      void AddUps(const CtrProd& aCP, std::vector<CtrProd>& aRes) const;
      void AddLt(const CtrProd& aCP, std::vector<CtrProd>& aRes) const;
      void AddOcc(const CtrProd& aCP, std::vector<CtrProd>& aRes) const;
      void AddRtForwDown(const CtrProd& aCP, std::vector<CtrProd>& aRes) const;
      
   public:     
      BasicOperProd(Nb aCoef, std::vector<BasicOper>& aProd,
                    bool aCmtOrigin);
      ///< Contruct from a vector of basic operators.

      BasicOperProd(Nb aCoef, const string& aOpers, bool aCmtOrigin);
      ///< Construct product form string, e.g. "H3T2T2".
      
      void SetCoef(Nb aCoef) {mCoef = aCoef;}
      Nb Coef() {return mCoef;}
      void ChangeSign() {mCoef = -mCoef;}        ///< Change sign of coefficient.

      void AddOperRight(const BasicOper& aOper) {mRT.push_back(aOper);} 
      ///< Add operator to the right of H.
      
      void AddOperLeft(const BasicOper& aOper) {mLT.push_back(aOper);} 
      ///< Add operator to the left of H.
      
      In LeftExciLevel();                        ///< Total left-hand excitation level.
      In RightExciLevel();                       ///< Total right-hand excitation level.
     
      void Sort() {std::sort(mLT.begin(),mLT.end()); std::sort(mRT.begin(), mRT.end());}
      ///< Sort basic operators to the left and right of H.
     
      void GetCtrProds(In aMinExci, In aMaxExci, std::vector<CtrProd>& aRes,
                       bool aRemoveSumRestr=true) const;
      ///< Generate the list of possible contraction products based on this operator product.

      In Nfactors() {return I_1 + mLT.size() + mRT.size();}
      ///< Return the total number of factors in product.
      
      friend bool operator<(const BasicOperProd& aLeft, const BasicOperProd& aRight);
      ///< Compare without respect to coefficient.
      
      friend bool operator==(const BasicOperProd& aLeft, const BasicOperProd& aRight);
      ///< Compare without respect to coefficient.

      void Latex(std::ostream& aOut) const;      ///< Print LaTeX code to aOut.
      friend std::ostream& operator<<(std::ostream& aOut, const BasicOperProd& aProd);

      static void GenerateVci(In aHdim, In aExciLevel, std::vector<BasicOperProd>& aRes);
      ///< Generate a set of basic operator products corresponding to a VCI transformation.
     
      static void CollectDuplicates(std::vector<BasicOperProd>& aIn,
                                    std::vector<BasicOperProd>& aOut, In aMaxExci);
      ///< Collect identical products and add coefficients.
};

#endif // BASICOPERPROD_H
