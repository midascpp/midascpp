/**
************************************************************************
* 
* @file                BasicOperProd.cc
*
* Created:             13-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of BasicOperProd class.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/CtrProd.h"
#include "util/Fractions.h"

BasicOperProd::BasicOperProd(Nb aCoef, std::vector<BasicOper>& aProd, bool aCmtOrigin):
   mCoef(aCoef), mCmtOrigin(aCmtOrigin)
{
   In len = aProd.size();
   In idx;
   for (idx = I_0; idx<len; ++idx)
      if (aProd[idx].Type() == OP::H)
         break;
      else
         mLT.push_back(aProd[idx]);

   mHlevel = aProd[idx].Level();

   for (idx=idx+I_1; idx<len; ++idx)
      mRT.push_back(aProd[idx]);
}

BasicOperProd::BasicOperProd(Nb aCoef, const string& aOpers, bool aCmtOrigin):
   mCoef(aCoef), mCmtOrigin(aCmtOrigin)
{
   bool found_h = false;
   for (In i=I_0; i<aOpers.size(); i+=I_2)
   {
      OP type = BasicOper::TypeForChar(aOpers[i]);
      In lvl = In(aOpers[i+I_1]) - In('0');
      if (type == OP::H)
      {
         mHlevel = lvl;
         found_h = true;
      }
      else if (found_h)
         mRT.push_back(BasicOper(type, lvl));
      else
         mLT.push_back(BasicOper(type, lvl));
   }
}

In BasicOperProd::LeftExciLevel()
{
   In level = I_0;
   for (In i=I_0; i<mLT.size(); ++i)
      level += mLT[i].Level();
   return level;
}

In BasicOperProd::RightExciLevel()
{
   In level = I_0;
   for (In i=I_0; i<mRT.size(); ++i)
      level += mRT[i].Level();
   return level;
}

void BasicOperProd::GetCtrProds(In aMinExci, In aMaxExci, vector<CtrProd>& aRes,
                                bool aRemoveSumRestr) const
{
   vector<CtrProd> cp_vec;
   CtrProd cp;
   InitCtrProd(cp);

   // First step: Add up contractions.
   vector<CtrProd> tmp;
   AddUps(cp, tmp);

   // Remove products where excitation level is out of limits.
   for (In i=I_0; i<tmp.size(); ++i)
   {
      In exci = tmp[i].ExciLevel();
      if (aMinExci<=exci && exci<=aMaxExci)
         cp_vec.push_back(tmp[i]);
   }

   // At this point a lot of equivalent products has been generated.
   CtrProd::RemoveDuplicates(cp_vec);

   // At this point we have a unique list.
   // Initialize each contraction product and remove sum restrictions.
   // The removal of sum restrictions increases the number of products.
   vector<CtrProd> cp_vec2;
   for (In i=I_0; i<cp_vec.size(); ++i)
   {
      cp_vec[i].InitModeSums();
      cp_vec[i].InitModeGuides();
      if (aRemoveSumRestr)
         cp_vec[i].RemoveSumRestrictions(cp_vec2);
      else
         cp_vec2.push_back(cp_vec[i]);
   }
   copy(cp_vec2.begin(), cp_vec2.end(), back_inserter(aRes));
}

void BasicOperProd::InitCtrProd(CtrProd& aCP) const
{
   aCP.SetCoef(mCoef);
   aCP.SetHlevel(mHlevel);
   for (In i=I_0; i<mLT.size(); ++i)
   {
      CtrT ct(mLT[i]);
      aCP.AddLt(ct);
   }
   for (In i=I_0; i<mRT.size(); ++i)
   {
      CtrT ct(mRT[i]);
      aCP.AddRt(ct);
   }
}

void BasicOperProd::AddUps(const CtrProd& aCP, vector<CtrProd>& aRes) const
{
   AddLt(aCP, aRes);

   // Try to add an extra up contraction.
   CtrProd new_cp(aCP);
   if (new_cp.AddUp())
      AddUps(new_cp, aRes);
}

void BasicOperProd::AddLt(const CtrProd& aCP, vector<CtrProd>& aRes) const
{
   vector<CtrProd> cps;
   if (aCP.AddLExci(cps))
      for (In i=I_0; i<cps.size(); ++i)
         AddLt(cps[i], aRes);
   else
      AddOcc(aCP, aRes);
}

void BasicOperProd::AddOcc(const CtrProd& aCP, vector<CtrProd>& aRes) const
{
   // Make sure there are enough unused modes on right-Ts that we can get
   // rid of the remaining free H factors.
   if (aCP.FreeH() <= aCP.UnusedRtModes())
      AddRtForwDown(aCP, aRes);
   
   CtrProd new_cp(aCP);
   if (new_cp.AddOcc())
      AddOcc(new_cp, aRes);
}

void BasicOperProd::AddRtForwDown(const CtrProd& aCP, vector<CtrProd>& aRes) const
{
   vector<CtrProd> cps;
   if (aCP.AddRForwDown(cps))
      for (In i=I_0; i<cps.size(); ++i)
         AddRtForwDown(cps[i], aRes);
   else
   {
      CtrProd cp(aCP);
      if (cp.Finalize(mCmtOrigin))
         aRes.push_back(cp);
   }
}

bool operator<(const BasicOperProd& aLeft, const BasicOperProd& aRight)
{
   if (aLeft.mLT.size() != aRight.mLT.size())
      return aLeft.mLT.size() < aRight.mLT.size();
   else
   {
      // Number of left-hand T's equal, compare individual operators.
      for (In i=I_0; i<aLeft.mLT.size(); ++i)
         if (aLeft.mLT[i] != aRight.mLT[i])
            return aLeft.mLT[i] < aRight.mLT[i];
   }

   if (aLeft.mHlevel != aRight.mHlevel)
      return aLeft.mHlevel < aRight.mHlevel;
   
   if (aLeft.mRT.size() != aRight.mRT.size())
      return aLeft.mRT.size() < aRight.mRT.size();
   else
   {
      // Number of right-hand T's equal, compare individual operators.
      for (In i=I_0; i<aLeft.mRT.size(); ++i)
         if (aLeft.mRT[i] != aRight.mRT[i])
            return aLeft.mRT[i] < aRight.mRT[i];
   }
   return false;
}

bool operator==(const BasicOperProd& aLeft, const BasicOperProd& aRight)
{
   if (aLeft.mLT.size() == aRight.mLT.size())
   {
      // Number of left-hand T's equal, compare individual operators.
      for (In i=I_0; i<aLeft.mLT.size(); ++i)
         if (aLeft.mLT[i] != aRight.mLT[i])
            return false;
   }
   else
      return false;

   if (aLeft.mHlevel != aRight.mHlevel)
      return false;
   
   if (aLeft.mRT.size() == aRight.mRT.size())
   {
      // Number of right-hand T's equal, compare individual operators.
      for (In i=I_0; i<aLeft.mRT.size(); ++i)
         if (aLeft.mRT[i] != aRight.mRT[i])
            return false;
   }
   else
      return false;

   return true;
}

void BasicOperProd::Latex(ostream& aOut) const
{
   string s;
   FracFloatLatex(mCoef, s, true);
   aOut << s << " ";
   for (vector<BasicOper>::const_iterator it=mLT.begin(); it!=mLT.end(); ++it)
      it->Latex(aOut);
   aOut << "H" << "_{" << mHlevel << "} ";
   for (vector<BasicOper>::const_iterator it=mRT.begin(); it!=mRT.end(); ++it)
      it->Latex(aOut);
}

ostream& operator<<(ostream& aOut, const BasicOperProd& aProd)
{
   string sc;
   FracFloat(aProd.mCoef, sc);
   aOut << setw(6) << sc << " ";
   for (vector<BasicOper>::const_iterator it=aProd.mLT.begin(); it!=aProd.mLT.end(); ++it)
      aOut << *it << " ";
   aOut << "H" << aProd.mHlevel;
   aOut << " ";
   for (vector<BasicOper>::const_iterator it=aProd.mRT.begin(); it!=aProd.mRT.end(); ++it)
      aOut << *it << " ";
   return aOut;
}

/**
 * Generate vector of basic operator products corresponding to a VCI calculation.
 * aHdim:      Maximum mode coupling in Hamiltonian.
 * aExciLevel: Excitation level, i.e. VCI[aExciLevel].
 **/
void BasicOperProd::GenerateVci(In aHdim, In aExciLevel, vector<BasicOperProd>& aRes)
{
   aRes.clear();
   for (In h=I_1; h<=aHdim; ++h)
   {
      vector<BasicOper> ops;
      // Note: Using t=0 gives 'Hh T0' where T0 corresponds to the reference coefficient.
      for (In t=I_0; t<=aExciLevel; ++t)
      {
         ops.clear();
         ops.push_back(BasicOper(OP::H, h));
         ops.push_back(BasicOper(OP::T, t));
         aRes.push_back(BasicOperProd(C_1, ops, false));
      }
   }
}

/**
 * Collect duplicate operator products in aIn and store result in aOut. For instance two
 * entries, 1/2 T2 H2 and 1/4 T2 H2, in aIn will become 3/4 T2 H2 in aOut.
 * Also, product with excitation level larger than aMaxExci will be removed.
 **/
void BasicOperProd::CollectDuplicates(vector<BasicOperProd>& aIn, vector<BasicOperProd>& aOut,
                                     In aMaxExci)
{
   sort(aIn.begin(), aIn.end());
   BasicOperProd cur_opr(aIn[I_0]);
   for (In i=I_1; i<aIn.size(); ++i)
      if (aIn[i] == cur_opr)
         cur_opr.SetCoef(cur_opr.Coef() + aIn[i].Coef());
      else
      {
         if ((fabs(cur_opr.Coef()) > C_NB_EPSILON) && (cur_opr.LeftExciLevel() <= aMaxExci))
            aOut.push_back(cur_opr);
         cur_opr = aIn[i];
      }
   if ((fabs(cur_opr.Coef()) > C_NB_EPSILON) && (cur_opr.LeftExciLevel() <= aMaxExci))
      aOut.push_back(cur_opr);
}
