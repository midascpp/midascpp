/**
************************************************************************
* 
* @file                CtrProd.h
*
* Created:             20-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class representing product of i, u, and t
*                      contractions.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef CTRPROD_H
#define CTRPROD_H

#include <ostream>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/v3/ModeSum.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/CtrT.h"

void VectorSwapModes(vector<In>& aVec, In aM1, In aM2);

class CtrProd
{
   protected:
      Nb                  mCoef;  ///< Coefficient for this product.
      std::vector<midas::vcc::v3::ModeSum>     mSums;  ///< Sums over non-excited modes.
      std::vector<In>     mOcc;   ///< Mode associated with passive (occupied) integrals.
      std::vector<In>     mUps;   ///< Up contracted modes.
      std::vector<CtrT>   mLt;    ///< Contracted T operators to the left of H.
      std::vector<CtrT>   mRt;    ///< Contracted T operators to the right of H.
    
      std::vector<std::vector<In> > mModeGuides; ///< Distribution of concrete modes from bra state.
      
      // These variables are used for storing the current state in the process of adding
      // contractions. 
      In                  mFreeH;         ///< Number of non-used factors i H.
      In                  mCurMode;       ///< Next free general mode number.
      In                  mCurLt;         ///< Current left-T. Used for adding left-t excitations.

      bool EquivalenceSwap(CtrProd& aOther, const std::vector<In>& aModes, In aIdx1) const;
      ///< Check if all intermediates in aOther can be made equivalent to those in
      ///< this object by swapping the modes a aModes. Set aIdx1=0 on first call.
      ///< Used by public wrapper Equivalence().
   
      bool EquivalenceCheck(const CtrProd& aOther) const;
      ///< Check if this CtrProd is identical to aOther.
      ///< Used by public wrapper Equivalence().

      void SwapModes(In aM1, In aM2);
      ///< Swap all occurences of modes aM1 and aM2,

      bool ReplaceMode(In aM1, In aM2);
      ///< Replace all occurences of mode aM1 with aM2. Returns false if the resulting product
      ///< does not make sense.
      
      void RepairCoef();
      ///< Fix coefficient to take care of possible permutations.

      void GetAllExciModes(std::vector<In>& aModes) const;
      ///< Add all excited modes to aModes.
     
      void GetAllNonExciModes(std::vector<In>& aModes) const;
      ///< Add all modes not excited to aModes.
     
      void GetAllModes(std::vector<In>& aModes) const;
      ///< Add all modes in contraction product to aModes.
    
      void GetTauModes(std::vector<In>& aModes) const;
      ///< Get modes in OP::E excitation operator.
      
      void RemoveSumRestrictionsRec(std::vector<CtrProd>& aProds) const;
      ///< Recursive function for removing sum restrictions. Used by wrapper member function
      ///< RemoveSumRestrictions() which is doing some post-processing on the products.
    
      const CtrT* GetTau() const;
      ///< Returns a pointer to the OP::E operator if it exists. Otherwise return NULL.
      
      void InitModeSumsR();
      ///< Init the sums over non-excited modes for right-hand trans.
      
      void InitModeSumsL(bool aAdjustCoef=true, bool aRestrict=true);
      ///< Init the sums for left-hand transformation.

      void InitTModeSumsL(std::vector<In>& aExciModes, std::vector<In>& aPureExciModes,
                          std::vector<In>& aDOModes, std::vector<In>& aTauModes,
                          std::vector<In>& aTauPureDownModes, bool aRestrict);
      ///< Utility function for adding summations corresponding to modes on Ts for left-hand
      ///< transformations.
      
      void AdjustCoefL();
      ///< Adjusts coefficient after mode sums for left-hand contraction product have been added.
   
      void AdjustCoefLUtil(std::vector<In>& aFixedModes, const std::vector<In>& aTauModes, bool aLeft);
      
      bool TEquivalence(In aIdx1, In aIdx2, const std::vector<In>& aFixedModes, bool aLeft);
      ///< Returns true if T-contractions with indices aIdx1 and aIdx2 are equivalent.
      ///< Modes in aFixedModes (assumed sorted) are not permuted to obtain equivalence.
      ///< If aLeft==true, indices refer to left-hand Ts, otherwise to right-hand Ts.
      
      void InitModeGuidesR();      ///< Init the mode guides for right-hand transformation.
      void InitModeGuidesL();      ///< Init the mode guides for left-hand transformation.
    
      bool MultiExci() const;      ///< Return true if some mode index is excited more than once.
      bool ValidateTCtr() const;   ///< Return false if some T contraction is not valid.

   public:
      static bool msUseRforL;      ///< If set to true, use algorithms for right-hand
                                   ///< transformations even if an OP::E operator exists.
                                   ///< Useful for debugging.
      
      CtrProd(): mCoef(C_0), mFreeH(I_0), mCurMode(I_0), mCurLt(I_0) {}
      
      void SetCoef(Nb aC)     {mCoef = aC;}
      Nb   Coef() const       {return mCoef;}
      void SetHlevel(In aLvl) {mFreeH = aLvl;}
      void AddLt(CtrT& aLt)   {mLt.push_back(aLt);}
      void AddRt(CtrT& aRt)   {mRt.push_back(aRt);}
      In   NLt() const        {return mLt.size();}
      In   NRt() const        {return mRt.size();}
      In   FreeH() const      {return mFreeH;}

      In UnusedRtModes() const;    ///< Total number of modes not used in right-hand Ts.
      In ExciLevel() const;        ///< Total number of modes excited by this product.
      
      bool AddUp();                ///< Add up contraction. Return true if succesfull.
      bool AddOcc();               ///< Add passive contraction. Return true if succesfull.

      bool AddLExci(std::vector<CtrProd>& aRes) const;
      ///< Add one left-T excitation. Excitation may be connected to passive or right-T down
      ///< contraction. All possibilites are stored in aRes.
     
      bool AddRForwDown(std::vector<CtrProd>& aRes) const;
      ///< Ass a down or forward contraction on right-hand Ts.
      
      bool Finalize(bool aCmtOrigin);
      ///< Finalize product. Returns true if final contraction product is legal.

      void InitModeSums();         ///< Init mode sums.
      void InitModeGuides();       ///< Init mode guides.

      void GetCoefModes(std::vector<In>& aModes) const;
      ///< Append all modes with and associated operator to mModes.

      void ContractModeNumbers();
      ///< Contract the general mode numbers so they become 0,1,2,3,...
      ///< and excited modes are lower than non-excited modes.
      
      bool Equivalence(const CtrProd& aOther, const std::vector<In>* aModes=nullptr) const;
      ///< Returns true if aOther can be made identical to this CtrProd by swapping
      ///< mode numbers given by aModes. If aModes == NULL all modes in contraction
      ///< product will be used.

      void RemoveSumRestrictions(std::vector<CtrProd>& aCtrProds) const;
      ///< Remove restrictions in summations over modes by adding correction products.
      ///< All resulting products are added to aCtrProds.
      ///< Mode guides and coeffcients are updated as well.
      
      bool ContainsEtype() const;
      ///< Returns true if a contraction of type OP::E is present signaling a left-hand
      ///< transformation.

      void WriteV3ContribSpec(std::ostream& aOut) const;
      ///< Write a specification for use by V3Contrib::Factory().

      void Latex(std::ostream& aOut) const;      
      friend std::ostream& operator<<(std::ostream& aOut, const CtrProd& aCP);

      static void RemoveDuplicates(std::vector<CtrProd>& aCtrProds, bool aAccumCoef=false);
      ///< Remove equivalent copies of contractions products.
      ///< If aAccumCoef==true coefficients are added.
};

#endif // CTRPROD_H
