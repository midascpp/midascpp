/**
************************************************************************
* 
* @file                BasicOper.h
*
* Created:             13-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   BasicOper class: Describing a basic operator,
*                                       e.g. H1, H2, T2, R3, ...
* 
* Last modified:
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASICOPER_H
#define BASICOPER_H

#include <iostream>
#include <map>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

// Different types of operators.
/*const In OP_U =  0;   ///< Unknown type.
const In OP_H =  1;   ///< Hamiltonian.
const In OP_T =  2;   ///< Cluster operator.
const In OP_P =  4;   ///< General excitation operator used in response calculations.
const In OP_Q =  8;   ///< Another general excitation operator for response.
const In OP_R = 16;   ///< Excitation operator corresponding to right-hand eigenvector of
                      ///< VCC Jacobian.
const In OP_S = 32;   ///< ... and yet another general excitation vector for response.
const In OP_E = 64;   ///< An excitation operator with no associated coefficients.
                      ///< Used e.g. for left-hand VCC Jacobian transformation.*/

enum OP { U=0, H=1, T=2, P=4, Q=8, R=16, S=32, E=64, L=128 };

struct OP_MAP
{
   static std::map<OP,char> create_map()
   {
      std::map<OP,char> m;
      m[OP::U] = 'U';
      m[OP::H] = 'H';
      m[OP::T] = 'T';
      m[OP::P] = 'P';
      m[OP::Q] = 'Q';
      m[OP::R] = 'R';
      m[OP::S] = 'S';
      m[OP::E] = 'E';
      m[OP::L] = 'L';
      return m;
   }
   static std::map<char,OP> create_inv_map()
   {
      std::map<char,OP> m;
      m['U'] = OP::U;
      m['H'] = OP::H;
      m['T'] = OP::T;
      m['P'] = OP::P;
      m['Q'] = OP::Q;
      m['R'] = OP::R;
      m['S'] = OP::S;
      m['E'] = OP::E;
      m['L'] = OP::L;
      return m;
   }
   static const std::map<OP,char> MAP;
   static const std::map<char,OP> INV_MAP;
};

class BasicOper
{
   protected:
      OP mType;             ///< Type, e.g. OP::T.
      In mLevel;            ///< Excitation level.

   public:     
      BasicOper(OP aType, In aLevel): mType(aType), mLevel(aLevel) {}

      OP Type() const {return mType;}
      In Level() const {return mLevel;}
      
      friend bool operator<(const BasicOper& aLeft, const BasicOper& aRight);
      friend bool operator!=(const BasicOper& aLeft, const BasicOper& aRight);
      friend bool operator==(const BasicOper& aLeft, const BasicOper& aRight);

      void Latex(std::ostream& aOut) const;      ///< Print LaTeX math code for operator on aOut.
      friend std::ostream& operator<<(std::ostream& aOut, const BasicOper& aOper);

      /* Static members and functions for translating between integer and character
       * representation of basic operators. Useful for e.g. output. */
      static std::map<In, char> msTypeCharMap; 
      static std::map<char, In> msCharTypeMap;
      static void InitOperTypeMaps();              
      static char CharForType(OP aType, bool aLowerCase=false);
      static OP TypeForChar(char aC);
 
};

#endif // BASICOPER_H
