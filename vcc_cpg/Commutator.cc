/**
************************************************************************
* 
* @file                Commutator.cc
*
* Created:             17-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class Commutator.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "vcc_cpg/Commutator.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"
#include "util/Fractions.h"
#include "util/Isums.h"

Commutator::Commutator(Nb aCoef, const string& aOpers)
{
   ReInit(aCoef, aOpers);
}

void Commutator::ReInit(Nb aCoef, const string& aOpers)
{
   mCoef = aCoef;
   mOpers.clear();
   for (In i=I_0; i<aOpers.size(); i+=I_2)
   {
      OP type = BasicOper::TypeForChar(aOpers[i]);
      In lvl = In(aOpers[i+I_1]) - In('0');;
      AddOper(type,lvl);
   }
}

/**
 * Note this function may return negative value if H dimension
 * is sufficiently large. That is ok.
 **/
In Commutator::MinExciLevel() const
{
   In min_level = I_0;
   for (In i=I_0; i<mOpers.size(); ++i)
   {
      In level = mOpers[i].Level();
      if (OP::H == mOpers[i].Type())
         min_level -= level;
      else
         min_level += level;
   }
   return min_level;
}

bool Commutator::IncludesExciOperLevel(In aLevel) const
{
   for (In i=I_0; i<mOpers.size(); ++i)
      if (mOpers[i].Type()!=OP::H && mOpers[i].Level()==aLevel)
         return true;
   return false;
}

In Commutator::MaxExciOperLevel() const
{
   In max_level = I_0;
   for (In i=I_0; i<mOpers.size(); ++i)
      if (OP::H!=mOpers[i].Type() && max_level<mOpers[i].Level())
         max_level = mOpers[i].Level();
   return max_level;
}

/**
 * Add new nesting level in BCH expansion.
 * aCmt:        Current commutator
 * aLevel:      Current level of nesting
 * aMinTExci:   Lowest level of T added.
 * aMaxTExci:   Highset level of T added.
 * aMaxResExci: Highest excitation level of result.
 * aRes:        New commutators are added to this vector.
 **/
void Commutator::BCHAddLevel(In aMinTExci, In aMaxTExci, In aMaxResExci,
                             In aLevel, const Commutator& aCmt, vector<Commutator>& aRes) const
{
   In min_exci = aCmt.MinExciLevel();
   for (In exci=aMinTExci; exci<=aMaxTExci; ++exci)
      if (min_exci+exci <= aMaxResExci)
      {
         Commutator new_cmt(aCmt);
         new_cmt.AddOper(OP::T, exci);
         Nb coef = new_cmt.Coef();
         new_cmt.SetCoef(coef / Nb(aLevel));
         aRes.push_back(new_cmt);
         (aRes.back()).UpdCoefPermute(aRes[0].mOpers.size());
         BCHAddLevel(exci, aMaxTExci, aMaxResExci, aLevel+I_1, new_cmt, aRes);
      }
      else
         return;
}

/**
 * Update coefficient according to the number of possible permutations of the excitation
 * operators added during the BCH expansion.
 * We assume operators are sorted so all operators of each type are together in mOpers vector.
 * aStart is the first index where operators were added.
 **/
void Commutator::UpdCoefPermute(In aStart)
{
   // The number of permutations is n! / (n1! * n2! * ...) where n1, n2, ... are numbers of
   // identical operators.
   Nb perm_corr = NbFact(mOpers.size()-aStart);
   BasicOper cur_op(mOpers[aStart]);
   In n_op = I_1;
   for (In i=aStart+I_1; i<mOpers.size(); ++i)
   {
      if (mOpers[i] == cur_op)
         n_op++;
      else
      {
         perm_corr /= NbFact(n_op);
         cur_op = mOpers[i];
         n_op = I_1;
      }
   }
   perm_corr /= NbFact(n_op);
   mCoef *= perm_corr;
}

void Commutator::BCH(In aMinTExci, In aMaxTExci, In aMaxResExci, vector<Commutator>& aRes) const
{
   aRes.clear();
   aRes.push_back(*this);

   BCHAddLevel(aMinTExci, aMaxTExci, aMaxResExci, I_1, *this, aRes);
}

void Commutator::Expand(In aMaxExci, vector<BasicOperProd>& aRes) const
{
   vector<BasicOper> init;
   init.push_back(mOpers[I_0]);
   vector<BasicOperProd> old;
   old.push_back(BasicOperProd(mCoef, init, true));
   
   for (In oper=I_1; oper<mOpers.size(); ++oper)
   {
      vector<BasicOperProd> cur;
      for (In n=I_0; n<old.size(); ++n)
      {
         BasicOperProd op = old[n];
         op.AddOperRight(mOpers[oper]);
         cur.push_back(op);
         op = old[n];
         op.AddOperLeft(mOpers[oper]);
         op.ChangeSign();
         cur.push_back(op);
      }
      old = cur;
   }

   vector<BasicOperProd> final;
   BasicOperProd::CollectDuplicates(old, final, aMaxExci);
   aRes.insert(aRes.end(), final.begin(), final.end());
}

bool Commutator::ContainsEtype() const
{
   for (In i=I_0; i<mOpers.size(); ++i)
      if (mOpers[i].Type() == OP::E)
         return true;

   return false;
}

void Commutator::GetCtrProds(In aMinExci, In aMaxExci, vector<CtrProd>& aCtrProds,
                             bool aRemoveSumRestr) const
{
   vector<BasicOperProd> ops;
   Expand(aMaxExci, ops);
   vector<CtrProd> cps;
   for (In i=I_0; i<ops.size(); ++i)
      ops[i].GetCtrProds(aMinExci, aMaxExci, cps, aRemoveSumRestr);
 
   // The removal of sum restrictions may have generated some identical contraction products
   // originating from different basic operator products. Collect these.
   if (aRemoveSumRestr)
      CtrProd::RemoveDuplicates(cps, true);
   
   copy(cps.begin(), cps.end(), back_inserter(aCtrProds));
}

void Commutator::Latex(ostream& aOut) const
{
   string s;
   FracFloatLatex(mCoef, s, true);
   aOut << s << " ";
   
   if (mOpers.empty())
   {
      aOut << "(empty cmt)";
      return;
   }
   if (mOpers.size() == I_1)
   {
      mOpers[I_0].Latex(aOut);
      return;
   }
   
   for (In i=I_0; i<mOpers.size()-I_1; ++i)
      aOut << "[";
   mOpers[I_0].Latex(aOut);
   aOut << ",";
   for (In i=I_1; i<mOpers.size()-I_1; ++i)
   {
      mOpers[i].Latex(aOut);
      aOut << "],";
   }
   mOpers.back().Latex(aOut);
   aOut << "]";
}

ostream& operator<<(ostream& aOut, const Commutator& aCmt)
{
   string sc;
   FracFloat(aCmt.mCoef, sc);
   aOut << setw(5) << sc << " ";

   if (aCmt.mOpers.empty())
   {
      aOut << "(empty cmt)";
      return aOut;
   }
   if (aCmt.mOpers.size() == I_1)
   {
      aOut << aCmt.mOpers[I_0];
      return aOut;
   }
   
   for (In i=I_0; i<aCmt.mOpers.size()-I_1; ++i)
      aOut << "[";
   aOut << aCmt.mOpers[I_0] << ",";
   for (In i=I_1; i<aCmt.mOpers.size()-I_1; ++i)
      aOut << aCmt.mOpers[i] << "],";
   aOut << aCmt.mOpers.back() << "]";
   return aOut;
}

bool CompareCommutator(const Commutator aComm1,const Commutator aComm2)
{
   Nb coef1 = aComm1.mCoef;
   Nb coef2 = aComm2.mCoef;
   vector<BasicOper> opers1 = aComm1.mOpers;
   vector<BasicOper> opers2 = aComm2.mOpers;

   if (coef1 != coef2) return false;
   else if (opers1.size() != opers2.size()) return false;
   else for (In i=I_0 ; i < opers1.size(); i++)
   {
      for (In i=I_0 ; i < opers2.size(); i++)
      {
         if (opers1[i] != opers2[i] ) return false;
      }
   }
   return true;
}

bool Commutator::ContainsOper(OP aType, In aLevel) const
{
   for (In i=I_0; i<mOpers.size(); ++i)
      if ((mOpers[i].Type() == aType) && (mOpers[i].Level() == aLevel))
         return true;

   return false;
}

In Commutator::MaxPtOrder()
{
   In pt_ord=I_0;
   for (In i=I_0; i<mOpers.size(); ++i)
   {
      pt_ord += mOpers[i].Level() - I_1;
//Mout << "mOpers["<<i<<"].Level()= " << mOpers[i].Level() << endl
//<< "pt_ord= " << pt_ord << endl; 
   }
   return pt_ord;
}
