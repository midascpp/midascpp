/**
************************************************************************
* 
* @file                CtrProd.cc
*
* Created:             20-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class CtrProd.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <iterator>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "vcc_cpg/BasicOperProd.h"
#include "vcc_cpg/CtrProd.h"
#include "vcc_cpg/CtrT.h"
#include "util/Fractions.h"
#include "vcc/v3/ModeSum.h"

bool CtrProd::msUseRforL = false;

void VectorSwapModes(vector<In>& aVec, In aM1, In aM2)
{  
   for (In i=I_0; i<aVec.size(); ++i)
      if (aVec[i] == aM1)
         aVec[i] = aM2;
      else if (aVec[i] == aM2)
         aVec[i] = aM1;
}     

In CtrProd::UnusedRtModes() const
{
   In n = I_0;
   for (In i=I_0; i<mRt.size(); ++i)
      n += mRt[i].TLevel() - mRt[i].UsedModes();
   return n;
}

In CtrProd::ExciLevel() const
{
   In n = mUps.size();
   for (In i=I_0; i<mLt.size(); ++i)
      n += mLt[i].ExciLevel();
   for (In i=I_0; i<mRt.size(); ++i)
      n += mRt[i].ExciLevel();
   return n;
}

bool CtrProd::AddUp()
{
   if (mFreeH != I_0)
   {
      mUps.push_back(mCurMode++);
      mFreeH--;
      return true;
   }
   return false;
}

bool CtrProd::AddOcc()
{
   if (mFreeH != I_0)
   {
      mOcc.push_back(mCurMode++);
      mFreeH--;
      return true;
   }
   return false;
}

bool CtrProd::AddLExci(vector<CtrProd>& aRes) const
{
   aRes.clear();
   if (mCurLt > In(mLt.size())-I_1)
      return false;

   CtrProd cp(*this);
   In cur_mode = cp.mCurMode;
   cp.mLt[mCurLt].AddExci(cp.mCurMode++);
   if (cp.mLt[mCurLt].UsedModes() >= cp.mLt[mCurLt].TLevel())
      cp.mCurLt++;

   // Pure excitation.
   aRes.push_back(cp);

   if (cp.mFreeH > I_0)
   {
      // Connection to occupied modes.
      CtrProd occ_cp(cp);
      occ_cp.mOcc.push_back(cur_mode);
      occ_cp.mFreeH--;
      aRes.push_back(occ_cp);

      // Connection to right-T down contracted modes.
      for (In i=I_0; i<mRt.size(); ++i)
      {
         CtrProd rd_cp(cp);
         if (rd_cp.mRt[i].AddDown(cur_mode))
         {
            rd_cp.mFreeH--;
            aRes.push_back(rd_cp);
         }
      }
   }
   return true;
}

bool CtrProd::AddRForwDown(vector<CtrProd>& aRes) const
{
   aRes.clear();
   if (mFreeH<=I_0)
      return false;

   for (In i=I_0; i<mRt.size(); ++i)
   {
      CtrProd cp(*this);
      if (cp.mRt[i].AddForw(mCurMode))
      {
         cp.mCurMode++;
         cp.mFreeH--;
         aRes.push_back(cp);
      }
      cp = *this;
      if (cp.mRt[i].AddDown(mCurMode))
      {
         cp.mCurMode++;
         cp.mFreeH--;
         aRes.push_back(cp);
      }
   }
   return true;
}

/**
 * Finalize contraction product. Then test if it is valid and return true if so.
 * aCmtOrigin: If true the CtrProd is assumed to originate from a commutator expansion.
 *             In this case every excitation operator must have at least one mode in
 *             common with H.
 **/
bool CtrProd::Finalize(bool aCmtOrigin)
{
   // Add pure excitations to right-hand Ts.
   vector<In> d_modes;                    // Down contracted modes.
   for (In i=I_0; i<mRt.size(); ++i)
   {
      if (aCmtOrigin && mRt[i].UsedModes() == I_0)
         return false;
      else
         mRt[i].GetDownModes(d_modes);

      In t_level = mRt[i].TLevel();
      In used_modes = mRt[i].UsedModes();
      for (In j=I_0; j<t_level-used_modes; ++j)
         mRt[i].AddExci(mCurMode++);
   }
   sort(d_modes.begin(), d_modes.end());
   vector<In> do_modes(d_modes);                             // Down and occupied modes.
   copy(mOcc.begin(), mOcc.end(), back_inserter(do_modes));
   sort(do_modes.begin(), do_modes.end());

   // Check if every left-hand T has a mode in common with H.
   for (In i=I_0; i<mLt.size(); ++i)
   {
      vector<In> l_exci;
      mLt[i].GetExciModes(l_exci);
      vector<In> cmn;
      set_intersection(do_modes.begin(), do_modes.end(), l_exci.begin(), l_exci.end(),
                       back_inserter(cmn));
      if (cmn.size() == 0)
         return false;
   }

   RepairCoef();
   
   return true;
}

/**
 * Seidler: UGLY FUNCTION. Make it nicer later.
 **/
void CtrProd::RepairCoef()
{
   // Fix left-hand Ts.
   In t_level = -I_1;
   OP type = OP::U;
   Nb fac = C_1;
   In count = I_1;
   for (In i=I_0; i<mLt.size(); ++i)
   {
      if (mLt[i].TLevel()>t_level || mLt[i].Type()!=type)
      {
         mCoef *= fac;
         t_level = mLt[i].TLevel();
         type = mLt[i].Type();
         fac = C_1;
         count = I_1;
      }
      else
      {
         count++;
         fac *= Nb(count);
      }
   }
   mCoef *= fac;

   // Fix right-hand Ts.
   t_level = -I_1;
   type = OP::U;
   fac = C_1;
   for (In i=I_0; i<mRt.size(); ++i)
   {
      if (mRt[i].TLevel()>t_level || mRt[i].Type()!=type)
      {
         mCoef *= fac;
         t_level = mRt[i].TLevel();
         type = mRt[i].Type();
         fac = C_1;
         count = I_1;
      }
      else
      {
         count++;
         fac *= Nb(count);
      }
   }
   mCoef *= fac;

}

/**
 * Return true if an OP::E excitation operator is present.
 * Quite inefficient. Could be changed to a member variable.
 **/
bool CtrProd::ContainsEtype() const
{
   for (In i=I_0; i<mLt.size(); ++i)
      if (mLt[i].Type() == OP::E)
         return true;
   
   for (In i=I_0; i<mRt.size(); ++i)
      if (mRt[i].Type() == OP::E)
         return true;

   return false;
}

const CtrT* CtrProd::GetTau() const
{
   for (In i=I_0; i<mLt.size(); ++i)
      if (mLt[i].Type() == OP::E)
         return &mLt[i];
   
   for (In i=I_0; i<mRt.size(); ++i)
      if (mRt[i].Type() == OP::E)
         return &mRt[i];

   return NULL;
}

void CtrProd::GetTauModes(vector<In>& aModes) const
{
   for (In i=I_0; i<mLt.size(); ++i)
      if (mLt[i].Type() == OP::E)
      {
         mLt[i].GetAllModes(aModes);
         return;
      }
   
   for (In i=I_0; i<mRt.size(); ++i)
      if (mRt[i].Type() == OP::E)
      {
         mRt[i].GetAllModes(aModes);
         return;
      }
}

void CtrProd::InitModeSums()
{
   if (ContainsEtype() && !msUseRforL)
      InitModeSumsL();
   else
      InitModeSumsR();
}

void CtrProd::InitModeSumsR()
{
   vector<In> exci_modes;                   // Modes excited in T contractions.
   vector<In> l_exci_modes;                 // Modes excited in left-hand T contractions.
   vector<In> do_modes = mOcc;              // Down contracted and occupied modes.
   for (In i=I_0; i<mLt.size(); ++i)
   {
      mLt[i].GetExciModes(l_exci_modes);
      mLt[i].GetExciModes(exci_modes);
   }
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].GetExciModes(exci_modes);
      mRt[i].GetDownModes(do_modes);
   }
   sort(exci_modes.begin(), exci_modes.end());
   sort(l_exci_modes.begin(), l_exci_modes.end());
   sort(do_modes.begin(), do_modes.end());
  
   vector<In> pure_exci;                 // Modes that are purely excited, i.e. have no operator
                                         // associated at all (e.g. no left-occ. connection).
   set_difference(exci_modes.begin(), exci_modes.end(), do_modes.begin(), do_modes.end(),
                  back_inserter(pure_exci));

   // Add sum for occupied integrals.
   midas::vcc::v3::ModeSum ms;
   set_difference(mOcc.begin(), mOcc.end(), exci_modes.begin(), exci_modes.end(),
                  back_inserter(ms.mModes));
   if (! ms.mModes.empty())
   {
      ms.mRestrict = pure_exci;
      mSums.push_back(ms);
   }

   // Add sums for down contractions on right-hand Ts.
   // Modify coefficient for Ts that are solely down contracted to avoid overcounting.
   vector<In> used;                       // Indices in mRt that has been processed.
   for (In i=I_0; i<mRt.size(); ++i)
   {
      // Figure out number of down contractions which are excited in left-hand Ts.
      vector<In> downs;
      mRt[i].GetDownModes(downs);
      sort(downs.begin(), downs.end());
      In n_ldowns = mRt[i].NDownCmn(l_exci_modes);
      
      if (mRt[i].Ndown() - n_ldowns == I_0 ||
          used.end() != find(used.begin(), used.end(), i))
         continue;

      ms.Clear();
      set_difference(downs.begin(), downs.end(), exci_modes.begin(), exci_modes.end(),
                     back_inserter(ms.mModes));

      vector<In> excis;
      mRt[i].GetExciModes(excis);
      set_difference(pure_exci.begin(), pure_exci.end(), excis.begin(), excis.end(),
                     back_inserter(ms.mRestrict));
      mSums.push_back(ms);
      
      // If there are no forwards, excitations, or left-connected down contractions, check
      // for other contractions of this kind and add summations for these.
      // Modify coefficient as well.
      In n_equiv = I_1;    // Number of equivalent contractions.
      if (n_ldowns==I_0 && mRt[i].Nexci()==I_0 && mRt[i].Nforw()==I_0)
      {
         for (In j=i+I_1; j<mRt.size(); ++j)
            if (mRt[i].CmpKind(mRt[j]) && mRt[j].NDownCmn(l_exci_modes)==I_0)
            {
               used.push_back(j);
               mCoef /= Nb(++n_equiv);
               ms.Clear();
               mRt[j].GetDownModes(ms.mModes);
               ms.mRestrict = pure_exci;
               mSums.push_back(ms);
            }
      }
   }
}

void CtrProd::InitModeSumsL(bool aAdjustCoef, bool aRestrict)
{
   mSums.clear();
   
   // Set up various vectors of modes with certain characteristics.
   vector<In> exci_modes;                   // Modes excited in T contractions.
   vector<In> pure_exci_modes;              // Purely excited modes (no oper. associated)
   vector<In> do_modes = mOcc;              // Down contracted and occupied modes.
   for (In i=I_0; i<mLt.size(); ++i)
      mLt[i].GetExciModes(exci_modes);
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].GetExciModes(exci_modes);
      mRt[i].GetDownModes(do_modes);
   }
   sort(exci_modes.begin(), exci_modes.end());
   sort(do_modes.begin(), do_modes.end());
   set_difference(exci_modes.begin(), exci_modes.end(), do_modes.begin(), do_modes.end(),
                  back_inserter(pure_exci_modes));

   // Get modes specific for tau operator.
   const CtrT* tau = GetTau();
   vector<In> tau_modes;
   vector<In> tau_down_modes;
   vector<In> tau_pure_down_modes;
   tau->GetAllModes(tau_modes);
   sort(tau_modes.begin(), tau_modes.end());
   tau->GetDownModes(tau_down_modes);
   set_difference(tau_down_modes.begin(), tau_down_modes.end(),
                  exci_modes.begin(), exci_modes.end(),
                  back_inserter(tau_pure_down_modes));
 
   InitTModeSumsL(exci_modes, pure_exci_modes, do_modes,
                  tau_modes, tau_pure_down_modes, aRestrict);

   if (aAdjustCoef)
      AdjustCoefL();

   // Add sum for up contractions.
   if (! mUps.empty())
   {
      sort(mUps.begin(), mUps.end());
      mSums.push_back(midas::vcc::v3::ModeSum(mUps));
   }

   // Passive contractions with no connection to Ts.
   // Sum, restrict from pure excitations. This sum is added as the last one since
   // concrete restrictions may be set by other sums.
   midas::vcc::v3::ModeSum ms;
   set_difference(mOcc.begin(), mOcc.end(), exci_modes.begin(), exci_modes.end(),
                  back_inserter(ms.mModes));
   if (! ms.mModes.empty())
   {
      if (aRestrict)
         ms.mRestrict = pure_exci_modes;
      mSums.push_back(ms);
   }
}

void CtrProd::InitTModeSumsL(vector<In>& aExciModes, vector<In>& aPureExciModes,
                             vector<In>& aDOModes, vector<In>& aTauModes,
                             vector<In>& aTauPureDownModes, bool aRestrict)
{
   std::vector<CtrT> ctrs(mLt);
   copy(mRt.begin(), mRt.end(), back_inserter(ctrs));
   
   vector<midas::vcc::v3::ModeSum> pure_down_sums;        // Sums for pure down contractions. Must be added as
                                          // the last sums due to possible restrictions.
   
   for (In i_ctr=I_0; i_ctr<ctrs.size(); ++i_ctr)
   {
      if (ctrs[i_ctr].Type() == OP::E)
         continue;

      // Get excited modes not already assigned by mode guides.
      vector<In> exci;
      vector<In> tmp;
      ctrs[i_ctr].GetExciModes(tmp);
      sort(tmp.begin(), tmp.end());
      set_difference(tmp.begin(), tmp.end(), aTauModes.begin(), aTauModes.end(),
                     back_inserter(exci));

      // Sum purely excited modes.
      midas::vcc::v3::ModeSum sum;
      set_difference(exci.begin(), exci.end(), aDOModes.begin(), aDOModes.end(),
                     back_inserter(sum.mModes));
      if (! sum.mModes.empty())
      {
         if (aRestrict)
            sum.mRestrict = aTauPureDownModes;
         mSums.push_back(sum);
      }

      // Sum excited modes connected to passive contractions.
      sum.Clear();
      set_intersection(exci.begin(), exci.end(), mOcc.begin(), mOcc.end(),
                       back_inserter(sum.mModes));
      if (! sum.mModes.empty())
         mSums.push_back(sum);

      // Sum excited modes connected to down contractions.
      for (In k=I_0; k<ctrs.size(); ++k)
      {
         sum.Clear();
         vector<In> other_down;
         ctrs[k].GetDownModes(other_down);
         sort(other_down.begin(), other_down.end());
         set_intersection(exci.begin(), exci.end(), other_down.begin(), other_down.end(),
                          back_inserter(sum.mModes));
         if (! sum.mModes.empty())
         {
            mSums.push_back(sum);
         }
      }

      // Sum forward contracted modes.
      sum.Clear();
      ctrs[i_ctr].GetForwModes(sum.mModes);
      if (! sum.mModes.empty())
         mSums.push_back(sum);

      // Sum purely down contracted modes.
      sum.Clear();
      tmp.clear();
      vector<In> down;
      ctrs[i_ctr].GetDownModes(down);
      set_difference(down.begin(), down.end(), aExciModes.begin(), aExciModes.end(),
                     back_inserter(tmp));
      set_difference(tmp.begin(), tmp.end(), aTauModes.begin(), aTauModes.end(),
                     back_inserter(sum.mModes));
      if (! sum.mModes.empty())
      {
         if (aRestrict)
            sum.mRestrict = aPureExciModes;
         pure_down_sums.push_back(sum);
      }
   }
   copy(pure_down_sums.begin(), pure_down_sums.end(), back_inserter(mSums));
}

void CtrProd::AdjustCoefL()
{
   In used_sums = I_0;
   In n_modes   = I_0;
   vector<In> fixed_modes;
   GetTau()->GetAllModes(fixed_modes);
   while (used_sums != mSums.size())
   {
      n_modes++;
      vector<vector<In> > sum_modes;
      for (In i=I_0; i<mSums.size(); ++i)
      {
         if (mSums[i].mModes.size() == n_modes)
         {
            used_sums++;
            sum_modes.push_back(mSums[i].mModes);
            copy(mSums[i].mModes.begin(), mSums[i].mModes.end(), back_inserter(fixed_modes));
         }
      }
      sort(sum_modes.begin(), sum_modes.end());
      sort(fixed_modes.begin(), fixed_modes.end());

      vector<In> all_modes;
      vector<In> swap_modes;
      GetAllModes(all_modes);
      sort(all_modes.begin(), all_modes.end());
      set_difference(all_modes.begin(), all_modes.end(),
                     fixed_modes.begin(), fixed_modes.end(),
                     back_inserter(swap_modes));
      
      vector<In> dummy_modes(sum_modes.size()*n_modes);
      for (In i=I_0; i<dummy_modes.size(); ++i)
         dummy_modes[i] = mCurMode + i;

      vector<CtrProd> prods;   // Contraction products corresponding to various permutations.
     
      // Try all different permutations.
      In n_perm = I_0;
      do
      {
         n_perm++;

         CtrProd prod(*this);

         In dummy_idx = I_0;
         for (In i=I_0; i<sum_modes.size(); ++i)
            for (In j=I_0; j<sum_modes[i].size(); ++j)
               prod.ReplaceMode(sum_modes[i][j], dummy_modes[dummy_idx++]);

         In i = I_0;
         for (i=I_0; i<prods.size(); ++i)
            if (prod.Equivalence(prods[i], &swap_modes))
               break;
         if (i == prods.size())
            prods.push_back(prod);
         
      } while (next_permutation(sum_modes.begin(), sum_modes.end()));
      
      mCoef *= Nb(prods.size()) / Nb(n_perm);
   }
}

bool CtrProd::TEquivalence(In aIdx1, In aIdx2, const vector<In>& aFixedModes, bool aLeft)
{
   vector<CtrT>* ctrs = NULL;
   if (aLeft)
      ctrs = &mLt;
   else
      ctrs = &mRt;

   // Basic check.
   if (! (*ctrs)[aIdx1].CmpKind((*ctrs)[aIdx2]))
      return false;
  
   // Get all modes; 
   vector<In> modes1;
   vector<In> modes2;
   (*ctrs)[aIdx1].GetExciModes(modes1);
   (*ctrs)[aIdx1].GetForwModes(modes1);
   (*ctrs)[aIdx1].GetDownModes(modes1);
   (*ctrs)[aIdx2].GetExciModes(modes2);
   (*ctrs)[aIdx2].GetForwModes(modes2);
   (*ctrs)[aIdx2].GetDownModes(modes2);
  
   // If any modes on the Ts are fixed, they can not be equivalent.
   vector<In> tmp(modes1);
   vector<In> cmn;
   copy(modes2.begin(), modes2.end(), back_inserter(tmp));
   sort(tmp.begin(), tmp.end());
   set_intersection(aFixedModes.begin(), aFixedModes.end(), tmp.begin(), tmp.end(),
                     back_inserter(cmn));
   if (! cmn.empty())
      return false;
   
   // Make new contraction products for comparison. Swap modes on one product.
   CtrProd prod1(*this);
   CtrProd prod2(*this);
   for (In i=I_0; i<modes1.size(); ++i)
      prod2.SwapModes(modes1[i], modes2[i]);
   
   // Set up the modes to be swapped in the equivalence test, i.e. all modes except those
   // on the contraction and those chosen as fixed by the aFixedModes argument.
   vector<In> all_modes;
   GetAllModes(all_modes);
   sort(all_modes.begin(), all_modes.end());
   copy(aFixedModes.begin(), aFixedModes.end(), back_inserter(modes1));
   sort(modes1.begin(), modes1.end());
   vector<In> swap_modes;
   set_difference(all_modes.begin(), all_modes.end(), modes1.begin(), modes1.end(),
                  back_inserter(swap_modes));

   return prod1.Equivalence(prod2, &swap_modes);
}

void CtrProd::InitModeGuides()
{
   if (ContainsEtype() && !msUseRforL)
      InitModeGuidesL();
   else
      InitModeGuidesR();
}

/**
 * Automatically generate mode guides.
 * The idea is to generate all possible guides and then discard
 * those giving rise to equivalent products.
 **/
void CtrProd::InitModeGuidesR()
{
   mModeGuides.clear();

   // Get all excited modes.
   vector<In> exci_modes;
   GetAllExciModes(exci_modes);
   if (exci_modes.empty())
   {
      mModeGuides.push_back(vector<In>());       // Empty mode guide.
      return;
   }
   sort(exci_modes.begin(), exci_modes.end());

   // Get all modes not excited.
   vector<In> non_exci_modes;
   GetAllNonExciModes(non_exci_modes);

   // Set up dummy mode numbers to be distributed by mode guides.
   vector<In> dummy_modes(exci_modes.size());
   for (In i=I_0; i<dummy_modes.size(); ++i)
      dummy_modes[i] = mCurMode + i;

   vector<CtrProd> prods;  // Contraction products corresponding to different mode guides.

   // Try all possible permutations of the excited modes as mode guides.
   do
   {
      CtrProd prod(*this);

      // Apply mode guide to product using dummy modes.
      for (In m=I_0; m<exci_modes.size(); ++m)
         prod.ReplaceMode(exci_modes[m], dummy_modes[m]);

      // Check if this product is equivalent to anything we have so far.
      // Equivalence is only based on swapping of modes not excited.
      In i = I_0;
      for (i=I_0; i<prods.size(); ++i)
         if (prod.Equivalence(prods[i], &non_exci_modes))
            break;
      if (i == prods.size())
      {
         mModeGuides.push_back(exci_modes);
         prods.push_back(prod);
      }
   } while (next_permutation(exci_modes.begin(), exci_modes.end()));
}

/**
 * Generate mode guides. This function is equivalent to InitModeGuidesR()
 * but is used in case of left-hand transformations where the OP::E (tau)
 * excitation operator specifies the mode indices for the result.
 **/
void CtrProd::InitModeGuidesL()
{
   mModeGuides.clear();

   vector<In> tau_modes;
   GetTauModes(tau_modes);
   sort(tau_modes.begin(), tau_modes.end());
   if (tau_modes.empty())
      MIDASERROR("CtrProd::InitModeGuidesL(): tau_modes empty.");

   vector<In> all_modes;
   GetAllModes(all_modes);
   sort(all_modes.begin(), all_modes.end());

   vector<In> sum_modes;           // Modes not in tau which must therefore be summed.
   set_difference(all_modes.begin(), all_modes.end(), tau_modes.begin(), tau_modes.end(),
                  back_inserter(sum_modes));
  
   // Set up dummy mode numbers to be distributed by mode guides.
   vector<In> dummy_modes(tau_modes.size());
   for (In i=I_0; i<dummy_modes.size(); ++i)
      dummy_modes[i] = mCurMode + i;
  
   vector<CtrProd> prods;   // Contraction products corresponding to different mode guides.
  
   // Try all possible permutations of tau modes as mode guides.
   do
   {
      CtrProd prod(*this);

      // Apply mode guide using dummy modes.
      for (In m=I_0; m<tau_modes.size(); ++m)
         prod.ReplaceMode(tau_modes[m], dummy_modes[m]);

      // Check if this is equivalent to anything we have so far by swapping the "sum modes".
      In i = I_0;
      for (i=I_0; i<prods.size(); ++i)
         if (prod.Equivalence(prods[i], &sum_modes))
            break;
      if (i == prods.size())
      {
         mModeGuides.push_back(tau_modes);
         prods.push_back(prod);
      }
   } while (next_permutation(tau_modes.begin(), tau_modes.end()));
}

void CtrProd::GetAllExciModes(vector<In>& aModes) const
{
   copy(mUps.begin(), mUps.end(), back_inserter(aModes));
   for (In i=I_0; i<mLt.size(); ++i)
      mLt[i].GetExciModes(aModes);
   
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].GetExciModes(aModes);
      mRt[i].GetForwModes(aModes);
   }
}

void CtrProd::GetAllNonExciModes(vector<In>& aModes) const
{
   copy(mOcc.begin(), mOcc.end(), back_inserter(aModes));
   for (In i=I_0; i<mLt.size(); ++i)
      mLt[i].GetDownModes(aModes);
   for (In i=I_0; i<mRt.size(); ++i)
      mRt[i].GetDownModes(aModes);
}

void CtrProd::GetAllModes(vector<In>& aModes) const
{
   GetAllExciModes(aModes);
   GetAllNonExciModes(aModes);
   sort(aModes.begin(), aModes.end());
   aModes.erase(unique(aModes.begin(), aModes.end()), aModes.end());
}

void CtrProd::GetCoefModes(vector<In>& aModes) const
{
   aModes = mOcc;
   copy(mUps.begin(), mUps.end(), back_inserter(aModes));
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].GetForwModes(aModes);
      mRt[i].GetDownModes(aModes);
   }
}

bool CtrProd::Equivalence(const CtrProd& aOther, const vector<In>* aModes) const
{
   // Basic check on size of vectors and number of modes.
   if (mOcc.size() != aOther.mOcc.size() ||
       mUps.size() != aOther.mUps.size() ||
       mLt.size()  != aOther.mLt.size()  ||
       mRt.size()  != aOther.mRt.size()  ||
       mCurMode    != aOther.mCurMode)
      return false;

   // More involved test on the kinds of contractions.
   vector<In> check(mLt.size(), I_0);
   vector<In> used(mLt.size(), I_0);
   for (In i=I_0; i<mLt.size(); ++i)
      for (In j=I_0; j<aOther.mLt.size(); ++j)
         if (used[j] != I_1)
            if (mLt[i].CmpKind(aOther.mLt[j]))
            {
               check[i] = I_1;
               used[j] = I_1;
               break;
            }
   
   In checksum = I_0;
   for (In i=I_0; i<check.size(); ++i)
      checksum += check[i];

   if (checksum != check.size())
      return false;
   
   check.clear();
   used.clear();
   check.resize(mRt.size(), I_0);
   used.resize(mRt.size(), I_0);
   for (In i=I_0; i<mRt.size(); ++i)
      for (In j=I_0; j<aOther.mRt.size(); ++j)
         if (used[j] != I_1)
            if (mRt[i].CmpKind(aOther.mRt[j]))
            {
               check[i] = I_1;
               used[j] = I_1;
               break;
            }
   
   checksum = I_0;
   for (In i=I_0; i<check.size(); ++i)
      checksum += check[i];

   if (checksum != check.size())
      return false;
   
   // At this point at least both products contain the same kinds of contractions.
   // Start swapping to see if they are really equivalent.
   vector<In> swap_modes;
   if (NULL == aModes)
      GetAllModes(swap_modes);
   else
      swap_modes = *aModes;
   CtrProd other(aOther);
   return EquivalenceSwap(other, swap_modes, I_0);
}

bool CtrProd::EquivalenceSwap(CtrProd& aOther, const vector<In>& aModes, In aIdx1) const
{
   if (EquivalenceCheck(aOther))
      return true;

   // No swapping of index corresponding to this mode.
   if (aIdx1<In(aModes.size())-I_1 && EquivalenceSwap(aOther, aModes, aIdx1+I_1))
      return true;

   // Swap with all higher mode numbers.
   for (In idx2=aIdx1+I_1; idx2<aModes.size(); ++idx2)
   {
      CtrProd mod(aOther);
      mod.SwapModes(aModes[aIdx1], aModes[idx2]);
      if (EquivalenceSwap(mod, aModes, aIdx1+I_1))
         return true;
   }
   return false;
} 

bool CtrProd::EquivalenceCheck(const CtrProd& aOther) const
{
   if (mOcc != aOther.mOcc ||
       mUps != aOther.mUps)
      return false;

   vector<In> check(mLt.size(), I_0);
   for (In i=I_0; i<mLt.size(); ++i)
      for (In j=I_0; j<aOther.mLt.size(); ++j)
         if (mLt[i].CmpWithModes(aOther.mLt[j]))
            check[i] = I_1;
   
   In checksum = I_0;
   for (In i=I_0; i<check.size(); ++i)
      checksum += check[i];

   if (checksum != check.size())
      return false;
   
   check.clear();
   check.resize(mRt.size(), I_0);
   for (In i=I_0; i<mRt.size(); ++i)
      for (In j=I_0; j<aOther.mRt.size(); ++j)
         if (mRt[i].CmpWithModes(aOther.mRt[j]))
            check[i] = I_1;
   
   checksum = I_0;
   for (In i=I_0; i<check.size(); ++i)
      checksum += check[i];

   if (checksum != check.size())
      return false;

   return true;      // Passed all tests.
}

void CtrProd::SwapModes(In aM1, In aM2)
{
   // Swap in sums.
   for (In i=I_0; i<mSums.size(); ++i)
   {
      VectorSwapModes(mSums[i].mModes, aM1, aM2);
      VectorSwapModes(mSums[i].mRestrict, aM1, aM2);
   }

   // Swap in contractions.
   VectorSwapModes(mOcc, aM1, aM2);
   sort(mOcc.begin(), mOcc.end());
   VectorSwapModes(mUps, aM1, aM2);
   sort(mUps.begin(), mUps.end());
   for (In i=I_0; i<mLt.size(); ++i)
      mLt[i].SwapModes(aM1, aM2);
   for (In i=I_0; i<mRt.size(); ++i)
      mRt[i].SwapModes(aM1, aM2);

   // Swap in mode guides.
   for (In i=I_0; i<mModeGuides.size(); ++i)
      VectorSwapModes(mModeGuides[i], aM1, aM2);
}

bool CtrProd::ReplaceMode(In aM1, In aM2)
{
   replace(mOcc.begin(), mOcc.end(), aM1, aM2);
   sort(mOcc.begin(), mOcc.end());
   if (unique(mOcc.begin(), mOcc.end()) != mOcc.end())
      return false;
   
   replace(mUps.begin(), mUps.end(), aM1, aM2);
   sort(mUps.begin(), mUps.end());
   if (unique(mUps.begin(), mUps.end()) != mUps.end())
      return false;

   for (In i=I_0; i<mLt.size(); ++i)
      if (! mLt[i].ReplaceMode(aM1, aM2))
         return false;
   
   for (In i=I_0; i<mRt.size(); ++i)
      if (! mRt[i].ReplaceMode(aM1, aM2))
         return false;

   // Check that no modes associated with an operator are identical.
   vector<In> op_modes = mOcc;
   copy(mUps.begin(), mUps.end(), back_inserter(op_modes));
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].GetForwModes(op_modes);
      mRt[i].GetDownModes(op_modes);
   }
   sort(op_modes.begin(), op_modes.end());
   if (unique(op_modes.begin(), op_modes.end()) != op_modes.end())
      return false;
   
   for (In i=I_0; i<mSums.size(); ++i)
   {
      replace(mSums[i].mModes.begin(), mSums[i].mModes.end(), aM1, aM2);
      replace(mSums[i].mRestrict.begin(), mSums[i].mRestrict.end(), aM1, aM2);
   }

   for (In i=I_0; i<mModeGuides.size(); ++i)
      replace(mModeGuides[i].begin(), mModeGuides[i].end(), aM1, aM2);
  
   return true;
}

bool CtrProd::MultiExci() const
{
   vector<In> exci;
   GetAllExciModes(exci);
   sort(exci.begin(), exci.end());
   In nexci = exci.size();
   exci.erase(unique(exci.begin(), exci.end()), exci.end());
   if (exci.size() != nexci)
      return true;
   return false;
}

bool CtrProd::ValidateTCtr() const
{
   for (In i=I_0; i<mLt.size(); ++i)
      if (!mLt[i].Valid())
         return false;
   
   for (In i=I_0; i<mRt.size(); ++i)
      if (!mRt[i].Valid())
         return false;

   return true;
}


void CtrProd::RemoveSumRestrictions(vector<CtrProd>& aCtrProds) const
{
   vector<CtrProd> tmp;
   RemoveSumRestrictionsRec(tmp);
   for (In i=I_0; i<tmp.size(); ++i)
      tmp[i].ContractModeNumbers();
 
   // If left-hand transformation, mode summations must be reinitialized since modes corresponding
   // to different contractions may at this point be part of the same sum. The coefficient
   // must be adjusted accordingly to avoid overcounting. 
   if (ContainsEtype() && !msUseRforL)
   {
      for (In i=I_0; i<tmp.size(); ++i)
      {
         for (In s=I_0; s<tmp[i].mSums.size(); ++s)
            for (In m=I_1; m<=tmp[i].mSums[s].mModes.size(); ++m)
               tmp[i].mCoef /= Nb(m);

         tmp[i].InitModeSumsL(false, false);
         
         for (In s=I_0; s<tmp[i].mSums.size(); ++s)
            for (In m=I_1; m<=tmp[i].mSums[s].mModes.size(); ++m)
               tmp[i].mCoef *= Nb(m);
      }
   }
   
   // Some compensating terms may turn out identical.
   // Collect these and accumulate coefficients. The correction from the mode guides below
   // is still ok since all identical products will give rise to the same mode guides.
   RemoveDuplicates(tmp, true);
   
   // Correct coefficients according to the number of mode guides that are possible
   // in new products.
   In n_orig_mg = mModeGuides.size();
   for (In i=I_0; i<tmp.size(); ++i)
   {
      tmp[i].InitModeGuides();
      Nb coef = tmp[i].Coef();
      In n_mg = tmp[i].mModeGuides.size();
      tmp[i].SetCoef(coef * Nb(n_orig_mg) / Nb(n_mg));
   }

   copy(tmp.begin(), tmp.end(), back_inserter(aCtrProds));
}

void CtrProd::RemoveSumRestrictionsRec(vector<CtrProd>& aCtrProds) const
{
   // Search for first sum with restrictions.
   In i_sum = I_0;
   for (i_sum=I_0; i_sum<mSums.size(); ++i_sum)
      if (! mSums[i_sum].mRestrict.empty())
         break;
      
   CtrProd prod(*this);

   // If no restrictions left:
   // Make sure no modes appear twice as excitations. 
   // If not, add copy of this to final list of products.
   if (i_sum == mSums.size())
   {
      if (!prod.MultiExci() && prod.ValidateTCtr())
         aCtrProds.push_back(prod);
      return;
   }

   midas::vcc::v3::ModeSum& sum = prod.mSums[i_sum];

   // Remove first restriction in this sum.
   In rm = sum.mRestrict.front();                // Restriction to be removed.
   sum.mRestrict.erase(sum.mRestrict.begin());

   // Compensate for this restriction.
   CtrProd comp(prod);                           // Compensating product.
   comp.SetCoef(-comp.Coef());
   midas::vcc::v3::ModeSum& comp_sum = comp.mSums[i_sum];
   In sm = comp_sum.mModes.back();               // Summation mode to be replaced by removed mode.
   comp_sum.mModes.pop_back();
   if (comp_sum.mModes.empty())
      comp.mSums.erase(comp.mSums.begin() + i_sum);
   if (comp.ReplaceMode(sm, rm))
   {
      comp.RemoveSumRestrictionsRec(aCtrProds);  // Remove further restrictions in
                                                 // compensating product.
   }

   // Remove further sum restrictions in current product.
   prod.RemoveSumRestrictionsRec(aCtrProds);
}

void CtrProd::ContractModeNumbers()
{
   // Order excited modes as 0,1,2,3,...
   In m=I_0;         // First mode
   sort(mUps.begin(), mUps.end());
   for (m=I_0; m<mUps.size(); ++m)
   {
      if (m != mUps[m])
         SwapModes(mUps[m], m);
   }
   
   for (In i=I_0; i<mLt.size(); ++i)
   {
      vector<In> exci;
      mLt[i].GetExciModes(exci);
      sort(exci.begin(), exci.end());
      for (In k=I_0; k<exci.size(); ++k)
      {
         if (m != exci[k])
            SwapModes(exci[k], m);
         m++;
      }
   }

   for (In i=I_0; i<mRt.size(); ++i)
   {
      vector<In> forw;
      mRt[i].GetForwModes(forw);
      sort(forw.begin(), forw.end());
      for (In k=I_0; k<forw.size(); ++k)
      {
         if (m != forw[k])
            SwapModes(forw[k], m);
         m++;
      }

      vector<In> exci;
      mRt[i].GetExciModes(exci);
      sort(exci.begin(), exci.end());
      for (In k=I_0; k<exci.size(); ++k)
      {
         if (m != exci[k])
            SwapModes(exci[k], m);
         m++;
      }
   }

   // Order modes for passive contractions.
   vector<In> exci_modes;
   GetAllExciModes(exci_modes);
   sort(exci_modes.begin(), exci_modes.end());
   sort(mOcc.begin(), mOcc.end());
   for (In i=I_0; i<mOcc.size(); ++i)
   {
      In mode = mOcc[i];
      if (find(exci_modes.begin(), exci_modes.end(), mode) != exci_modes.end())
         continue;
      if (m != mode)
         SwapModes(mOcc[i], m);
      m++;
   }

   // Order down modes.
   for (In i=I_0; i<mRt.size(); ++i)
   {
      vector<In> down;
      mRt[i].GetDownModes(down);
      sort(down.begin(), down.end());
      for (In k=I_0; k<down.size(); ++k)
      {
         In mode = down[k];
         if (find(exci_modes.begin(), exci_modes.end(), mode) != exci_modes.end())
            continue;
         if (m != mode)
            SwapModes(down[k], m);
         m++;
      }
   }
   mCurMode = m;      // mCurMode should equal highest mode number plus one.

   // Sort modes in summations.
   for (In i=I_0; i<mSums.size(); ++i)
   {
      sort(mSums[i].mModes.begin(), mSums[i].mModes.end());
      sort(mSums[i].mRestrict.begin(), mSums[i].mRestrict.end());
   }
}

void CtrProd::WriteV3ContribSpec(ostream& aOut) const
{
   if (ContainsEtype() && !msUseRforL)
      aOut << "IPL '";       // Intermediate product for left-hand transformations.
   else
      aOut << "IP '";        // Intermediate product for right-hand transformations.
   
   midas::stream::ScopedPrecision(16, aOut);
   aOut << scientific << showpos << mCoef << noshowpos << "' '";
   
   ostringstream os;
   for (In i=I_0; i<mSums.size(); ++i)
   {
      mSums[i].WriteSpec(os);
      os << " ";
   }
   aOut << setw(30) << left << os.str();
   aOut << "' '";
   os.str("");
   if (ContainsEtype() && !msUseRforL)
   {
      vector<In> exci_modes;
      GetAllExciModes(exci_modes);
      sort(exci_modes.begin(), exci_modes.end());
      os << "l" << exci_modes << " ";
   }
   vector<In> coef_modes;
   GetCoefModes(coef_modes);
   os << "c" << coef_modes << " ";
   if (! mUps.empty())
      os << "u" << mUps << " ";
   for (In i=I_0; i<mLt.size(); ++i)
   {
      mLt[i].WriteIntermediateSpec(os);
      os << " ";
   }
   if (! mOcc.empty())
      os << "i" << mOcc << " ";
   for (In i=I_0; i<mRt.size(); ++i)
   {
      mRt[i].WriteIntermediateSpec(os);
      os << " ";
   }
   aOut << setw(60) << os.str() << "' '";
   for (In i=I_0; i<mModeGuides.size(); ++i)
      aOut << mModeGuides[i];
   aOut << "'";
}

void CtrProd::Latex(ostream& aOut) const
{
      string s;
      FracFloatLatex(mCoef, s, true);
      ostringstream os;
      os << setw(10) << left << s;
      aOut << "$" << os.str() << "$ & ";

      os.str("");
      for (In i=I_0; i<mSums.size(); ++i)
      {
         mSums[i].Latex(os);
         os << " ";
      }
      aOut << setw(40) << left << "$" << os.str() << "$ & $";

      os.str("");
      vector<In> coef_modes;
      GetCoefModes(coef_modes);
      os << "c_{";
      for (In i=I_0; i<coef_modes.size(); ++i)
         os << "m_{" << coef_modes[i] << "}";
      os << "}";

      if (! mUps.empty())
      {
         os << " \\; \\cdot \\; u^{";
         for (In i=I_0; i<mUps.size(); ++i)
            os << "m_{" << mUps[i] << "}";
         os << "}";
      }

      for (In i=I_0; i<mLt.size(); ++i)
      {
         os << " \\; \\cdot \\; ";
         mLt[i].Latex(os);
      }

      if (! mOcc.empty())
      {
         os << " \\; \\cdot \\; i_{";
         for (In i=I_0; i<mOcc.size(); ++i)
            os << "m_{" << mOcc[i] << "}";
         os << "}";
      }
      
      for (In i=I_0; i<mRt.size(); ++i)
      {
         os << " \\; \\cdot \\; ";
         mRt[i].Latex(os);
      }

      aOut << setw(120) << left << os.str() << "$ \\\\";
}

ostream& operator<<(ostream& aOut, const CtrProd& aCP)
{
   string sc;
   FracFloat(aCP.mCoef, sc);
   aOut << setw(5) << sc;

   ostringstream os;
   os << "  ";
   copy(aCP.mSums.begin(), aCP.mSums.end(), ostream_iterator<midas::vcc::v3::ModeSum>(os, "   "));
   aOut << left << setw(15) << os.str();

   os.str("");
   if (! aCP.mUps.empty())
   {
      os << "u(";
      copy(aCP.mUps.begin(), aCP.mUps.end(), ostream_iterator<In>(os, ""));
      os << ")";
   }
   aOut << setw(10) << os.str();

   os.str("");
   copy(aCP.mLt.begin(), aCP.mLt.end(), ostream_iterator<CtrT>(os, "   "));
   aOut << "l: " << setw(20) << os.str();

   os.str("");
   if (! aCP.mOcc.empty())
   {
      os << "i(";
      copy(aCP.mOcc.begin(), aCP.mOcc.end(), ostream_iterator<In>(os, ""));
      os << ")";
   }
   aOut << setw(10) << os.str();

   os.str("");
   copy(aCP.mRt.begin(), aCP.mRt.end(), ostream_iterator<CtrT>(os, "   "));
   aOut << "r: " << setw(20) << os.str();

   if (aCP.mFreeH != I_0)
      aOut << "free_h: " << aCP.mFreeH;

   return aOut;
}

void CtrProd::RemoveDuplicates(vector<CtrProd>& aCtrProds, bool aAccumCoef)
{
   vector<bool> del(aCtrProds.size(), false);
   vector<CtrProd> tmp;

   for (In i=I_0; i<aCtrProds.size(); ++i)
   {
      // Save this product if it has not been marked as deleted.
      if (! del[i])
      {
         tmp.push_back(aCtrProds[i]);
         del[i] = true;
      }
      else
         continue;

      // Check if any other products are equivalent to this one. If so, delete them.
      for (In j=i+I_1; j<aCtrProds.size(); ++j)
         if (tmp.back().Equivalence(aCtrProds[j]))
         {
            if (aAccumCoef)
            {
               Nb coef = tmp.back().Coef();
               coef += aCtrProds[j].Coef();
               tmp.back().SetCoef(coef);
            }
            del[j] = true;
         }
   }
   aCtrProds = tmp;
}
