/**
************************************************************************
* 
* @file                CtrT.h
*
* Created:             20-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class representing contracted T operator.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef CTRT_H
#define CTRT_H

#include <ostream>
#include <vector>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc_cpg/BasicOper.h"

void VectorSwapModes(vector<In>& aVec, In aM1, In aM2);

class CtrT
{
   protected:
      In              mTLevel;     ///< Excitation level of T operator.
      std::vector<In> mExci;       ///< Purely excited modes.
      std::vector<In> mForw;       ///< Forward contracted modes.
      std::vector<In> mDown;       ///< Down contracted modes.
      OP              mType;       ///< Actual type of excitation oper. (T, R, P, ...).

      In mUsedModes;               ///< Actual number of modes on T.
      
   public:
      CtrT(In aTLevel, OP aType = OP::T):
         mTLevel(aTLevel), mType(aType), mUsedModes(I_0) {}
      
      CtrT(const BasicOper& aOper);

      In TLevel() const {return mTLevel;}
      ///< Return excitation level of T.
     
      In Nexci() const     {return mExci.size();}
      In Nforw() const     {return mForw.size();}
      In Ndown() const     {return mDown.size();}
      In ExciLevel() const {return mExci.size() + mForw.size();}

      In UsedModes() const {return mUsedModes;}
      ///< Return number of modes actually present on T.

      In NModesCmn(std::vector<In> aModes) const;
      ///< return number of modes in common with a aModes.
      ///< aModes is supposed to be sorted.
      
      In NDownCmn(std::vector<In> aModes) const;
      ///< Return the number of down-contracted modes in common with aModes.
      ///< aModes is supposed to be sorted.
      
      bool AddExci(In aMode);
      bool AddForw(In aMode);
      bool AddDown(In aMode);
      
      void SwapModes(In aM1, In aM2);
      ///< Swap aM1 with aM2.

      bool ReplaceMode(In aM1, In aM2);
      ///< Replace mode aM1 with aM2. Returns false if resulting contraction does not make sense
      ///< due to two identical modes.
      
      bool CmpWithModes(const CtrT& aOther) const;
      ///< Compare. All mode numbers must be identical for succes.

      bool CmpKind(const CtrT& aOther) const;
      ///< Compare. Only the number of excitations, forwards, and downs must be identical.
      
      OP Type() const {return mType;}
      
      void GetExciModes(std::vector<In>& aModes) const
      {std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));}
    
      void GetForwModes(std::vector<In>& aModes) const
      {std::copy(mForw.begin(), mForw.end(), std::back_inserter(aModes));}

      void GetDownModes(std::vector<In>& aModes) const
      {std::copy(mDown.begin(), mDown.end(), std::back_inserter(aModes));}

      void GetAllModes(std::vector<In>& aModes) const;

      bool Valid() const;
      ///< Returns true if all modes are different.
      
      void WriteIntermediateSpec(std::ostream& aOut) const;
      ///< Write a specification for use by Intermediates::Factory().
      
      void Latex(std::ostream& aOut) const;
      friend std::ostream& operator<<(std::ostream& aOut, const CtrT& aCtrT);
};

#endif // CTRT_H
