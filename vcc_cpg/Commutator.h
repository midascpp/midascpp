/**
************************************************************************
* 
* @file                Commutator.h
*
* Created:             17-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class describing a commutator of basic operators.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef COMMUTATOR_H
#define COMMUTATOR_H

#include <iostream>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc_cpg/BasicOper.h"
#include "vcc_cpg/BasicOperProd.h"

class Commutator
{
   protected:
      Nb                     mCoef;
      std::vector<BasicOper> mOpers;    ///< Vector defining the commutator.

      void BCHAddLevel(In aMinExci, In aMaxExci, In aMaxResLevel, In aLevel,
                       const Commutator& aCmt, std::vector<Commutator>& aRes) const;
      ///< Used for adding new nesting levels during BCH expansion.
      
      void UpdCoefPermute(In aStart);
      ///< Update coefficient according to possible T-permutations. Used during BCH expansion.

   public:
      Commutator(): mCoef(C_0) {}
      Commutator(Nb aCoef, const string& aOpers);

      void ReInit(Nb aCoef, const string& aOpers);
      ///< Reinitialize commutator.
      
      void SetCoef(Nb aCoef) {mCoef = aCoef;}
      Nb Coef() {return mCoef;}
     
      void AddOper(OP aType, In aLevel) {mOpers.push_back(BasicOper(aType, aLevel));}
      ///< Add operator X such that [...] -> [[...],X].

      void BCH(In aMinTExci, In aMaxTExci, In aMaxResExci, std::vector<Commutator>& aRes) const;
      ///< Generate BCH expansion of this commutator using T levels from aMinTExci to
      ///< aMaxTExci. Resulting commutators must have maximum excitation level aMaxResLevel.
      ///< Add results to aRes.

      void Expand(In aMaxExci, std::vector<BasicOperProd>& aRes) const;
      ///< Expand into list of basic operator products. Add products to end of aRes.
      
      In MinExciLevel() const;
      ///< Return the lowest excitation level this commutator can generate when
      ///< acting on the reference state.

      bool IncludesExciOperLevel(In aLevel) const;
      ///< Returns true if commutator includes an excitation operator with level aLevel.
     
      In MaxExciOperLevel() const;
      /// Return the highest excitation level of any excitation operator.

      bool ContainsEtype() const;
      /// Returns true if an operator of type OP_E is present in the commutator.
      
      void GetCtrProds(In aMinExci, In aMaxExci, vector<CtrProd>& aCtrProds,
                       bool aRemoveSumRestr=true) const;
      ///< Append all contraction products originating from this commutator to aCtrProds.

      bool ContainsOper(OP aType, In aLevel) const;
      ///< Returns true if an operator of type aType and level aLevel is present in the commutator

      In MaxPtOrder();
      ///< Returns the maximum order in the fluctuation operator for the commutator

      void Latex(std::ostream& aOut) const;
      friend std::ostream& operator<<(std::ostream& aOut, const Commutator& aProd);
      friend bool CompareCommutator(const Commutator aComm1, const Commutator aComm2);
};

#endif // COMMUTATOR_H
