/**
************************************************************************
* 
* @file                CtrT.cc
*
* Created:             20-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class CtrT.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "vcc_cpg/CtrT.h"
#include "vcc_cpg/BasicOper.h"

CtrT::CtrT(const BasicOper& aOper): mUsedModes(I_0)
{
   mType = aOper.Type();

   if (mType!=OP::T && mType!=OP::R && mType!=OP::P && mType!=OP::Q && mType!=OP::S && mType!=OP::E) 
   {
      Mout << "CtrT::CtrT(const BasicOper& aOper): aOper.Type() = " << aOper.Type()
           << " is not an excitation operator." << endl;
      MIDASERROR("");
   }

   mTLevel = aOper.Level();
} 

In CtrT::NModesCmn(vector<In> aModes) const
{
   vector<In> all_modes;
   vector<In> cmn;
   GetAllModes(all_modes);
   sort(all_modes.begin(), all_modes.end());
   set_intersection(aModes.begin(), aModes.end(), all_modes.begin(), all_modes.end(),
         back_inserter(cmn));
   return cmn.size();
}

In CtrT::NDownCmn(vector<In> aModes) const
{
   vector<In> down(mDown);              // To allow sorting though this function is const.
   sort(down.begin(), down.end());
   vector<In> cmn;
   set_intersection(aModes.begin(), aModes.end(), mDown.begin(), mDown.end(),
                    back_inserter(cmn));
   return cmn.size();
}

bool CtrT::AddExci(In aMode)
{
   if (mUsedModes < mTLevel)
   {
      mExci.push_back(aMode);
      mUsedModes++;
      return true;
   }
   return false;
}

bool CtrT::AddForw(In aMode)
{
   if (mUsedModes < mTLevel)
   {
      mForw.push_back(aMode);
      mUsedModes++;
      return true;
   }
   return false;
}

bool CtrT::AddDown(In aMode)
{
   if (mUsedModes < mTLevel)
   {
      mDown.push_back(aMode);
      mUsedModes++;
      return true;
   }
   return false;
}

void CtrT::SwapModes(In aM1, In aM2)
{
   VectorSwapModes(mExci, aM1, aM2);
   VectorSwapModes(mForw, aM1, aM2);
   VectorSwapModes(mDown, aM1, aM2);
   sort(mExci.begin(), mExci.end());
   sort(mForw.begin(), mForw.end());
   sort(mDown.begin(), mDown.end());
}

bool CtrT::ReplaceMode(In aM1, In aM2)
{
   replace(mExci.begin(), mExci.end(), aM1, aM2);
   sort(mExci.begin(), mExci.end());
   replace(mForw.begin(), mForw.end(), aM1, aM2);
   sort(mForw.begin(), mForw.end());
   replace(mDown.begin(), mDown.end(), aM1, aM2);
   sort(mDown.begin(), mDown.end());

   vector<In> all_modes(mExci);
   copy(mForw.begin(), mForw.end(), back_inserter(all_modes));
   copy(mDown.begin(), mDown.end(), back_inserter(all_modes));
   if (unique(all_modes.begin(), all_modes.end()) != all_modes.end())
      return false;
   else
      return true;
}

void CtrT::GetAllModes(vector<In>& aModes) const
{
   copy(mExci.begin(), mExci.end(), back_inserter(aModes));
   copy(mForw.begin(), mForw.end(), back_inserter(aModes));
   copy(mDown.begin(), mDown.end(), back_inserter(aModes));
}

bool CtrT::CmpWithModes(const CtrT& aOther) const
{
   if (mExci == aOther.mExci &&
       mForw == aOther.mForw &&
       mDown == aOther.mDown &&
       mType == aOther.mType)
      return true;

   return false;
}

bool CtrT::CmpKind(const CtrT& aOther) const
{
   if (mExci.size() == aOther.mExci.size() &&
       mForw.size() == aOther.mForw.size() &&
       mDown.size() == aOther.mDown.size() &&
       mType        == aOther.mType)
      return true;

   return false;
}

bool CtrT::Valid() const
{
   vector<In> modes(mExci);
   copy(mForw.begin(), mForw.end(), back_inserter(modes));
   copy(mDown.begin(), mDown.end(), back_inserter(modes));
   sort(modes.begin(), modes.end());
   In n = modes.size();
   modes.erase(unique(modes.begin(), modes.end()), modes.end());
   if (modes.size() != n)
      return false;
   return true;
}

void CtrT::WriteIntermediateSpec(ostream& aOut) const
{
   aOut << BasicOper::CharForType(mType, true);
   aOut << mExci << mForw << mDown;
}

void CtrT::Latex(ostream& aOut) const
{
   if (mTLevel != mUsedModes)
      aOut << mTLevel;

   if (! mForw.empty())
   {
      aOut << "{}^{";
      for (In i=I_0; i<mForw.size(); ++i)
         aOut << "m_{" << mForw[i] << "}";
      aOut << "}";
   }
   
   aOut << BasicOper::CharForType(mType, true);
   
   if (! mExci.empty())
   {
      aOut << "^{";
      for (In i=I_0; i<mExci.size(); ++i)
         aOut << "m_{" << mExci[i] << "}";
      aOut << "}";
   }
  
   if (! mDown.empty())
   {
      aOut << "_{";
      for (In i=I_0; i<mDown.size(); ++i)
         aOut << "m_{" << mDown[i] << "}";
      aOut << "}";
   }
}

ostream& operator<<(ostream& aOut, const CtrT& aCtrT)
{
   aOut << BasicOper::CharForType(aCtrT.mType, true);

   if (aCtrT.TLevel() != aCtrT.UsedModes())
      aOut << aCtrT.TLevel();

   if (! aCtrT.mExci.empty())
   {
      aOut << "(e";
      copy(aCtrT.mExci.begin(), aCtrT.mExci.end(), ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   if (! aCtrT.mForw.empty())
   {
      aOut << "(f";
      copy(aCtrT.mForw.begin(), aCtrT.mForw.end(), ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   if (! aCtrT.mDown.empty())
   {
      aOut << "(d";
      copy(aCtrT.mDown.begin(), aCtrT.mDown.end(), ostream_iterator<In>(aOut, ""));
      aOut << ")";
   }
   return aOut;
}
