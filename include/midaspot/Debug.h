/*=============================================================*
 *
 * midaspot/Debug.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_DEBUG_H_INCLUDED
#define MIDASPOT_DEBUG_H_INCLUDED

/*************************************************************** 
 * 
 * Introspection functions
 * 
 ***************************************************************/
/**
 * Print version of MidasPot
 **/
void print_version()
{
   std::cout << description.version << std::endl;
   exit(0);
}

/**
 * Print description of MidasPot and operators
 **/
void print_description()
{
   std::cout   << description.description << "\n"
               << std::left
               << formatting_settings.tab << std::setw(formatting_settings.large_width) << "Operator" << " " << std::setw(formatting_settings.large_width) << "Description" << "\n"
               << formatting_settings.delimeter << "\n";
   
   for(int i = 0; i < description.noperators; ++i)
   {
      std::cout << formatting_settings.tab << std::setw(formatting_settings.large_width) << description.operator_descriptions[i].name << " " 
                                           << std::setw(formatting_settings.large_width) << description.operator_descriptions[i].description << "\n";
   }

   std::cout << "\n" << std::flush;
}

/**
 * Print coordinates with atom symbol labels
 **/
void print_coords_array(std::string p_atoms[], const dynamic_matrix& coords)
{
   for(int i = 0; i < coords.nrows; ++i)
   {
      std::cout << std::setw(formatting_settings.atom_symbol_width) << p_atoms[i];
      for(int xyz = 0; xyz < coords.ncols; ++xyz)
      {
         std::cout << ' ' << coords[i][xyz];
      }
      std::cout << '\n';
   }
   std::cout << std::flush;
}

/**
 * Print geometry
 **/
void print_geometry()
{
   dynamic_matrix geometry(molecule.natoms, 3);
   for(int i = 0; i < molecule.natoms; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         geometry[i][j] = molecule.geometry[i][j];
      }
   }
   print_coords_array(molecule.atoms, geometry);
}

/**
 * Helper function for printing displacement coordinates
 **/
void print_disp_coords_impl(std::string p_atoms[], const dynamic_matrix& all_coords)
{
   const int ncoords = molecule.natoms * 3;
   dynamic_matrix coords(molecule.natoms, 3);

   for(int i = 0; i < molecule.ndegrees_of_freedom; ++i)
   {
      for(int j = 0; j < ncoords; ++j)
      {
         int atom = j/3;
         int xyz  = j%3;
         coords[atom][xyz] = all_coords[i][j];
      }
      print_coords_array(p_atoms, coords);
      std::cout << '\n';
   }
}

/**
 * Print displacement coordinates
 **/
void print_disp_coords(bool mass_weighted = true)
{
   const int ncoords = molecule.natoms * 3;
   dynamic_matrix nc(molecule.ndegrees_of_freedom, ncoords);
   for(int i = 0; i < 6; ++i)
   {
      for(int j = 0; j < ncoords; ++j)
      {
         int atom = j/3;
         if(mass_weighted)
         {
            nc[i][j] = molecule.normal_coordinates[i][j];
         }
         else
         {
            nc[i][j] = molecule.normal_coordinates[i][j]/sqrt(molecule.masses[atom]);
         }
      }
   }
   print_disp_coords_impl(molecule.atoms, nc);
}

/**
 * Print masses
 **/
void print_masses()
{
   for(unsigned int i = 0; i < molecule.natoms; ++i)
   {
      std::cout << std::setw(formatting_settings.atom_symbol_width) << molecule.atoms[i]
                << ' ' << molecule.masses[i] << '\n';
   }
   std::cout << std::flush;
}

/*************************************************************** 
 * 
 * Debug output code 
 * 
 ***************************************************************/
/** 
 * Check that the internal normal coordinates are othonormal
 **/
void check_normal_coordinate_orthogonality()
{
   int ndegrees_of_freedom = molecule.ndegrees_of_freedom;
   int natoms              = molecule.natoms;
   int ncoords             = natoms * 3;

   std::cerr << "**********************************" << std::endl;
   std::cerr << "* Check orthonomality of normal coordinates " << std::endl;
   std::cerr << "**********************************" << std::endl;
   for(int i = 0; i < ndegrees_of_freedom; ++i)
   {
      for(int j = 0; j < ndegrees_of_freedom; ++j)
      {
         floating_point_type dot = 0.0;
         for(int k = 0; k < ncoords; ++k)
         {
            dot += molecule.normal_coordinates[i][k] * molecule.normal_coordinates[j][k];
         }
         std::cerr << i << " " << j << " dot = " << dot;
         if(i == j)
         {
            if(!float_almost_equal(1.0, dot, 1e-12))
            {
               std::cerr << " !";
            }
         }
         else
         {
            if(!float_almost_equal(1.0, 1.0 + dot, 1e-12))
            {
               std::cerr << " !";
            }
         }
         std::cerr << std::endl;
      }
   }
   std::cerr << std::endl;
   std::cerr << "Translational : " << std::endl;
   for(int i = 0; i < ndegrees_of_freedom; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         floating_point_type dot = 0.0;
         for(int k = 0; k < ncoords; ++k)
         {
            dot += molecule.normal_coordinates[i][k] * molecule.translational_coordinates[j][k];
         }
         std::cerr << i << " " << (j == 0 ? 'x' : j == 1 ? 'y' : 'z') << " dot = " << dot;
         if(!float_almost_equal(1.0, 1.0 + dot, 1e-12))
         {
            std::cerr << " !";
         }
         std::cerr << std::endl;
      }
      std::cerr << std::endl;
   }
}

/**
 * Debug print-out of Q-vector
 **/
void debug_print_q_vector(const molecule_external_data_type& molecule_input)
{
   std::cerr << "**********************************" << std::endl;
   std::cerr << "* Q-vector " << std::endl;
   std::cerr << "**********************************" << std::endl;
   for(int i = 0; i < molecule_input.ndegrees_of_freedom; ++i)
   {
      std::cerr << molecule_input.q[i] << std::endl;
   }
   std::cerr << std::endl;
}

/**
 * Debug print-out of rotation matrix
 **/
void debug_print_rotation_matrix(const molecule_external_data_type& molecule_input)
{
   std::cerr << "**********************************" << std::endl;
   std::cerr << "* Rotation matrix " << std::endl;
   std::cerr << "**********************************" << std::endl;
   std::cerr << molecule_input.rot_matrix[0][0] << " " << molecule_input.rot_matrix[0][1] << " " << molecule_input.rot_matrix[0][2] << std::endl;
   std::cerr << molecule_input.rot_matrix[1][0] << " " << molecule_input.rot_matrix[1][1] << " " << molecule_input.rot_matrix[1][2] << std::endl;
   std::cerr << molecule_input.rot_matrix[2][0] << " " << molecule_input.rot_matrix[2][1] << " " << molecule_input.rot_matrix[2][2] << std::endl;
   std::cerr << std::endl;
}

#endif /* MIDASPOT_DEBUG_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/Debug.h header end
 *
 *=============================================================*/
