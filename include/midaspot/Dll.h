/*=============================================================*
 *
 * midaspot/Dll.h header begin
 *
 *=============================================================*/
#ifndef MIDAS_DLL_H_INCLUDED
#define MIDAS_DLL_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif 

/**
 * Define some types.
 **/
typedef int PROPERTY_TYPE;
typedef int PROVIDES_TYPE;
typedef int FLOAT_TYPE;
typedef int MATRIX_TYPE;
typedef int EXIT_STATUS;
typedef int UNIT_TYPE;

/**
 * Define some constants.
 **/
#define PROVIDES_NOTHING   0x0000
#define PROVIDES_WHATEVER  0x0000
#define PROVIDES_VALUE     0x0001
#define PROVIDES_PROPERTY  0x0001
#define PROVIDES_GRADIENT  0x0010
#define PROVIDES_HESSIAN   0x0100

#define FLOAT32     0x0001
#define FLOAT64     0x0010
#define TYPE_FLOAT  0x0001
#define TYPE_DOUBLE 0x0010

#define MATRIX_TYPE_ROW_MAJOR 0x0001
#define MATRIX_TYPE_COL_MAJOR 0x0010
#define MATRIX_ROW_MAJOR 0x0001
#define MATRIX_COL_MAJOR 0x0010

#define PROPERTY_ENERGY   0x0001
#define PROPERTY_DIPOLE_X 0x0002
#define PROPERTY_DIPOLE_Y 0x0003
#define PROPERTY_DIPOLE_Z 0x0004

#define BOHR      0x0001
#define AANGSTROM 0x0002

#ifndef __cplusplus
#define nullptr 0
#endif 

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif 
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#ifdef DEBUG
#define DEBUG_DYNAMIC_INTERFACE
#endif

#ifndef __cplusplus
#define true 1
#endif

#ifndef __cplusplus
#define false 0
#endif 

/**
 * Structure for passing which evaluate functions are present.
 **/
typedef struct property_interface
{
   char const*    name;
   char const*    function_str;
   PROPERTY_TYPE  property;
   PROVIDES_TYPE  provides;
   char const*    description;
} property_interface_t;

/**
 * Structure for holding cartesian coordinates.
 **/
typedef struct cartesian
{
   int   natoms;
   void* coord;
} cartesian_t;

/**
 * Structure for holding return of single point evaluation.
 **/
typedef struct singlepoint
{
   void* value;
   void* gradient;
   void* hessian;
} singlepoint_t;

/**
 * Define interface function signatures.
 **/
typedef int (*float_type_fptr)();
typedef int (*matrix_type_fptr)();
typedef int (*geometry_units_fptr)();
typedef int (*natoms_fptr)();

typedef EXIT_STATUS(*evaluate_fptr)(struct cartesian* c, struct singlepoint* sp, int iopt);

/**
 * Struct to hold functions defining the surface.
 **/
typedef struct surface
{
   /* Handle to dynamic surface library */
   void* handle;
   
   /* Some types that can be defined by dynamic surface library */
   FLOAT_TYPE    float_type;
   MATRIX_TYPE   matrix_type;
   PROVIDES_TYPE provides;
   UNIT_TYPE     geometry_units;
   
   /* Number of atoms for the surface */
   int natoms;

   /* Set of evaluation functions */
   int                   nfunctions;
   property_interface_t* interface;
   evaluate_fptr*        functions;
} surface_t;

/**
 * Surface struct helpers.
 **/
//! Initialize surface by loading dynamic library.
int initialize_surface(struct surface* s, const char* dll);
//! Finalize surface by unloading dynamic library. After this calling functions in surface struct is undefined behaviour.
int finalize_surface  (struct surface* s);

/**
 * Some helper functions.
 **/
//! Allocate cartesian struct.
int allocate_cartesian  (const struct surface* s, struct cartesian* c   );
//! Allocate singlepoint struct.
int allocate_singlepoint(const struct surface* s, struct singlepoint* sp, int provides);

//! De-allocate cartesian struct.
int deallocate_cartesian  (struct cartesian*   c );
//! De-allocate singlepoint struct.
int deallocate_singlepoint(struct singlepoint* sp);

/**
 * Some evaluation helpers.
 **/
EXIT_STATUS allocate_property(const struct surface* s, struct cartesian* c, struct singlepoint* sp, PROVIDES_TYPE iopt);
EXIT_STATUS evaluate_property(const struct surface* s, struct cartesian* c, struct singlepoint* sp, const char* name, PROVIDES_TYPE iopt);
EXIT_STATUS deallocate_property(struct cartesian* c, struct singlepoint* sp);

/**
 *
 **/
property_interface_t* property_introspection     (const struct surface* s, int prop_num);
const char*           property_introspection_name(const struct surface* s, int prop_num);

/**
 * Some Macros for creating interface.
 **/
//! Begin defining evaluate.
#define DYNLIB_EVALUATE_BEGIN \
   property_interface_t iface_functions[] = {

//! Edn defining evaluate.
#define DYNLIB_EVALUATE_END  \
   };                        \
                             \
   int iface_nfunctions = sizeof(iface_functions)/sizeof(property_interface_t); 

//! Define float type.
#define DYNLIB_FLOAT_TYPE(a)  \
   FLOAT_TYPE float_type()    \
   {                          \
      return a;               \
   }             

//! Define natoms.
#define DYNLIB_NATOMS(a)   \
   int natoms()            \
   {                       \
      return a;            \
   }

//! Define matrix type.
#define DYNLIB_MATRIX_TYPE(a)  \
   MATRIX_TYPE matrix_type()   \
   {                           \
      return a;                \
   }                           

//! Define unit type
#define DYNLIB_GEOMETRY_UNITS(a)  \
   UNIT_TYPE geometry_units()     \
   {                              \
      return a;                   \
   }

#ifdef __cplusplus
}
#endif 

#endif /* MIDAS_DLL_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/Dll.h header end
 *
 *=============================================================*/
