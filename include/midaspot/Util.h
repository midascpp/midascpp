/*=============================================================*
 *
 * midaspot/Util.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_UTIL_H_INCLUDED
#define MIDASPOT_UTIL_H_INCLUDED

/*************************************************************** 
 * 
 * Includes and some type definitions
 * 
 ***************************************************************/
/**
 * Include std headers
 **/
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <iomanip>
#include <set>
#include <functional>

/**
 * Set floating point precision
 **/
#if defined(FLOAT32)
using floating_point_type = float;
#define USING_FLOAT32
#else
using floating_point_type = double;
#define USING_FLOAT64
#endif /* FLOAT32 */

/*************************************************************** 
 * 
 * General helper functions 
 * 
 ***************************************************************/

/**
 * Check float almost equal.
 * 
 * Blatantly stolen from: https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 **/
bool float_almost_equal(floating_point_type f1, floating_point_type f2, floating_point_type max_rel_dist)
{
   auto dist = std::fabs(f1 - f2);
   f1        = std::fabs(f1);
   f2        = std::fabs(f2);
   auto largest = f1 > f2 ? f1 : f2;
   if(dist <= largest * max_rel_dist)
   {
      return true;
   }
   return false;
}

/**
 * Basic dynamic vector with automatic/scope-bound resource handling
 **/
struct dynamic_vector
{
   int size                      = 0;
   floating_point_type* data     = nullptr;
   floating_point_type* accessor = nullptr;

   dynamic_vector(int s = 0)
      :  size(s)
   {
      data     = (floating_point_type* )malloc(size  * sizeof(floating_point_type));
      accessor = data;
   }

   dynamic_vector(dynamic_vector&& other)
      :  size(other.size)
   {
      std::swap(data,     other.data);
      std::swap(accessor, other.accessor);
   }

   ~dynamic_vector()
   {
      if(data)
      {
         free(data);
      }
   }

   floating_point_type& operator[](int i)
   {
      return accessor[i];
   }
   
   const floating_point_type& operator[](int i) const
   {
      return accessor[i];
   }
};

/**
 * Basic dynamic matrix with automatic/scope-bound resource handling
 **/
struct dynamic_matrix
{
   int nrows = 0;
   int ncols = 0;
   int size  = 0;
   floating_point_type*  data     = nullptr;
   floating_point_type** accessor = nullptr;

   dynamic_matrix(int nr = 0, int nc = 0)
      :  nrows(nr)
      ,  ncols(nc)
      ,  size(nrows * ncols)
   {
      data     = (floating_point_type* )malloc(size  * sizeof(floating_point_type));
      accessor = (floating_point_type**)malloc(nrows * sizeof(floating_point_type*));
      for(int i = 0 ; i < nrows; ++i)
      {
         accessor[i] = data + i * ncols;
      }
   }

   dynamic_matrix(dynamic_matrix&& other)
      :  nrows(other.nrows)
      ,  ncols(other.ncols)
      ,  size(other.size)
   {
      std::swap(data,     other.data);
      std::swap(accessor, other.accessor);
   }

   ~dynamic_matrix()
   {
      if(data)
      {
         free(data);
      }
      if(accessor)
      {
         free(accessor);
      }
   }

   floating_point_type* operator[](int i)
   {
      return accessor[i];
   }
   
   const floating_point_type* operator[](int i) const
   {
      return accessor[i];
   }
};

/*************************************************************** 
 * 
 * Some helper structs
 * 
 ***************************************************************/
/**
 * Global struct for holding commandline input data
 **/
struct cl_data_type
{
   std::string filename = "water.xyz";
   bool kabsch = false;
   std::string units = "au";
   std::set<std::string> operators;
   bool addreference = true;
   bool check_bounds = true;
   bool check_bounds_throw = true;
   floating_point_type bounds_thresh = floating_point_type(1.0e-12);
} cl_data;

/**
 * Struct for storing operator descriptions
 **/
struct operator_description_type
{
   std::string name;
   std::string description;
};

/**
 * Data struct holding some formatting constants
 **/
const struct formatting_settings_type
{
   int atom_symbol_width = 4;
   int large_width       = 20;
   std::string tab       = "   ";
   std::string delimeter = std::string(2*large_width + 1, '-');
} formatting_settings;


/*************************************************************** 
 * 
 * Molecule handling code
 * 
 ***************************************************************/
/**
 * Struct for holding external molecular data
 **/
struct molecule_external_data_type
{
   int natoms;
   int ndegrees_of_freedom;

   std::vector<std::string> atom_labels;

   dynamic_matrix geometry;
   dynamic_matrix displacement;
   dynamic_matrix mass_scaled_displacement;
   
   dynamic_vector q;

   floating_point_type   rot_matrix[3][3] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

   molecule_external_data_type(int n, int ndeg)
      :  natoms(n)
      ,  ndegrees_of_freedom(ndeg)
      ,  geometry(natoms, 3)
      ,  displacement(natoms, 3)
      ,  mass_scaled_displacement(natoms, 3)
      ,  q(ndegrees_of_freedom)
   {
   }
};

/**
 * Get conversion factor for geometry (AA to AU), geometry is saved in AU
 **/
floating_point_type geometry_conversion_factor()
{
   if(cl_data.units == "au") 
   {
      return floating_point_type(1.0);
   }
   else if(cl_data.units == "aa") 
   {
      return floating_point_type(1.88972612499358972e+00);
   }
   else 
   {
      throw std::runtime_error("Unknown conversion units: " + cl_data.units);
   }
}

#endif /* MIDASPOT_UTIL_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/Util.h header end
 *
 *=============================================================*/
