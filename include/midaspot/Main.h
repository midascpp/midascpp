/*=============================================================*
 *
 * midaspot/Main.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_MAIN_H_INCLUDED
#define MIDASPOT_MAIN_H_INCLUDED

/*************************************************************** 
 * 
 * Command line options
 * 
 ***************************************************************/
/**
 * Create commandline input structure
 **/
cl_type create_cl_input()
{
   cl_type cl;
   cl 
      .option("--version", "", "Display version information.")
      .option("--debug",   "", "Debug flag. Prints extra debug information.")
      .option("--description", "", "Prints description of the MidasPot.")
      .option("--geometry", "", "Prints reference geometry for the MidasPot.")
      .option("--masses", "", "Prints nuclear masses (atomic mass units).")
      .option("--dispcoords", "", "Prints underlying displacement coordinates.")
      .option("--dispcoordsmw", "", "Prints underlying displacement coordinates (mass-weighted).")
      .option("--kabsch", "", "Use Kabsch-algorithm to rotate input structure to reference frame.")
      .option("--operator -o", "string", "Get operator value (more than one operator can be evaluated).", "multi")
      .option("--units -u",    "string", "Specify which units the input is given in (Default: 'au').")
      .option("--ref", "", "If set, the reference value will not be added to value of potential.")
      .option("--disable-bounds-check", "", "If set, operator bounds will not be checked.")
      .option("--do-not-throw-bounds-check", "", "If set, operator bounds will be checked but only give stdout notification.")
   #if defined(FLOAT32)
      .option("--bounds-check-thresh", "float", "Set the threshold of the bounds check (default: 1e-12).")
   #else
      .option("--bounds-check-thresh", "double", "Set the threshold of the bounds check (default: 1e-12).")
   #endif /* FLOAT32 */
      .option("--precision", "int", "Set output precision (default: 17).")
      .option("--fixed", "", "Output in fixed format.")
      .argument("moleculefile", "string");
      ;

   return cl;
}

/**
 * Setup cl_data struct based on input
 **/
void load_cl_args_to_cl_data(const cl_args_type& cl_args)
{
   cl_data.kabsch = cl_args.get<bool>("kabsch") ? true : false;
   
   if(auto operator_option = cl_args.get<string_vector>("operator"))
   {
      auto operator_vector = operator_option.value();
      for(const auto& oper : operator_vector)
      {
         cl_data.operators.insert(oper);
      }
   }
   
   if(auto option_units = cl_args.get<std::string>("units"))
   {
      cl_data.units = option_units.value();
   }

   cl_data.addreference = cl_args.get<bool>("ref") ? false : true;
   cl_data.check_bounds = cl_args.get<bool>("check_bounds") ? true : false;
   cl_data.check_bounds_throw = cl_args.get<bool>("check_bounds_throw") ? true : false;
   if(auto option_bounds_thresh = cl_args.get<floating_point_type>("bounds_check_thresh"))
   {
      cl_data.bounds_thresh = option_bounds_thresh.value();
   }

   if(auto moleculefile = cl_args.get<std::string>("moleculefile"))
   {
      cl_data.filename = moleculefile.value();
   }
   else
   {
      throw std::runtime_error("No filename argument.");
   }
}


/*************************************************************** 
 * 
 * Read molecular input
 * 
 ***************************************************************/
/**
 * Read molecular XYZ file
 **/
void read_molecule_xyz_file(const std::string& filename, molecule_external_data_type& molecule_external_data)
{
   std::ifstream input_file(filename);
   if(input_file.fail())
   {
      throw std::runtime_error("Failed when trying to open file: "+filename);
   }
   
   std::string str;
   int counter = 0;
   while(std::getline(input_file, str))
   {
      if(str.empty())
      {
         continue;
      }

      if(counter >= molecule_external_data.natoms)
      {
         throw std::runtime_error("Read to many lines in molecule input.");
      }
      auto split = split_string(str);
      if(split.size() != 4)
      {
         throw std::runtime_error("Number of entries on line " + std::to_string(counter + 1) + " is wrong. It is " + std::to_string(split.size()) + " but should be 4.");
      }
      molecule_external_data.atom_labels.emplace_back(split[0]);
      #if defined(FLOAT32)
      molecule_external_data.geometry[counter][0] = float_from_string(split[1]) * geometry_conversion_factor();
      molecule_external_data.geometry[counter][1] = float_from_string(split[2]) * geometry_conversion_factor();
      molecule_external_data.geometry[counter][2] = float_from_string(split[3]) * geometry_conversion_factor();
      #else
      molecule_external_data.geometry[counter][0] = double_from_string(split[1]) * geometry_conversion_factor();
      molecule_external_data.geometry[counter][1] = double_from_string(split[2]) * geometry_conversion_factor();
      molecule_external_data.geometry[counter][2] = double_from_string(split[3]) * geometry_conversion_factor();
      #endif /* FLOAT32 */
      
      ++counter;
   }
   if(counter != molecule_external_data.natoms)
   {
      throw std::runtime_error("Read too few lines in molecule input.");
   }
}


/*************************************************************** 
 * 
 * Main function
 * 
 ***************************************************************/
int main(int argc, const char* argv[])
{
   try
   {
      // Parse command line
      cl_type cl   = create_cl_input();
      auto cl_args = parse_cl_args(argc, argv, cl);

      load_cl_args_to_cl_data(cl_args);
      
      // Setup ostreams
      int precision = 17;
      if(auto option_precision = cl_args.get<int>("precision"))
      {
         precision = option_precision.value();
      }
      std::cout << std::showpos << std::setprecision(precision);
      std::cerr << std::showpos << std::setprecision(precision);
      
      if(cl_args.get<bool>("fixed"))
      {
         std::cout << std::fixed;
         std::cerr << std::fixed;
      }
      else
      {
         std::cout << std::scientific;
         std::cerr << std::scientific;
      }
      
      bool exit_after_print = false;
      if(cl_args.get<bool>("version"))
      {
         print_version();
         exit_after_print = true;
      }
      
      if(cl_args.get<bool>("description"))
      {
         print_description();
         exit_after_print = true;
      }
      
      if(cl_args.get<bool>("geometry"))
      {
         print_geometry();
         exit_after_print = true;
      }
      
      if(cl_args.get<bool>("masses"))
      {
         print_masses();
         exit_after_print = true;
      }
      
      if(cl_args.get<bool>("dispcoords"))
      {
         print_disp_coords(false);
         exit_after_print = true;
      }
      
      if(cl_args.get<bool>("dispcoordsmw"))
      {
         print_disp_coords(true);
         exit_after_print = true;
      }

      if(exit_after_print)
      {
         std::exit(0);
      }
      
      // Calculate properties
      molecule_external_data_type molecule_input(molecule.natoms, molecule.ndegrees_of_freedom);
      read_molecule_xyz_file(cl_data.filename, molecule_input);

      for(int i = 0; i < molecule.natoms; ++i)
      {
         if(molecule_input.atom_labels[i] != molecule.atoms[i])
         {
            std::string str_i = std::to_string(i);
            throw std::runtime_error
               (  "Atom " +  str_i + " does not match reference structure.\n"
                  "    Internal label " + str_i + " : '" + molecule.atoms[i] + "'\n"
                  "    External label " + str_i + " : '" + molecule_input.atom_labels[i] + "'\n"
               );
         }
      }

      if(cl_data.kabsch)
      {
         #if defined(LAPACKLINK)
         kabsch_rotate(molecule_input);
         #else
         std::cerr << "Error not linked with LAPACK, so no Kabsch rotation available." << std::endl;
         std::exit(-1);
         #endif /* LAPACKLINK */
      }
      
      generate_displacement_and_q(molecule, molecule_input);
      
      if(cl_args.get<bool>("debug"))
      {
         check_normal_coordinate_orthogonality();
         debug_print_q_vector(molecule_input);
         debug_print_rotation_matrix(molecule_input);
      }
      
      auto evaluated = evaluate_operators(molecule_input);
      for(const auto& oper : evaluated)
      {
         for(int i = 0; i < pow(3,oper.order); ++i)
         {
            std::cout << oper.name << tensor_names[oper.order][i] << " = " << oper.values[i] << "\n";
         }
      }
      
   }
   catch(const std::exception& e)
   {
      std::cerr << e.what() << "\n" << std::endl;
   }
   
   return 0;
}

#endif /* MIDASPOTMAIN_H_INCLUDED */
/*=============================================================*
 *
 * MidasPotMain.h header end
 *
 *=============================================================*/
