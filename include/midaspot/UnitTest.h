/*=============================================================*
 *
 * midaspot/UnitTest.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_UNITTEST_H_INCLUDED
#define MIDASPOT_UNITTEST_H_INCLUDED

#include <cutee/cutee.hpp>

/*************************************************************** 
 * 
 * Setup
 * 
 ***************************************************************/

/*************************************************************** 
 * 
 * Tests
 * 
 ***************************************************************/
void geometry_conversion_test()
{
   UNIT_ASSERT_FEQUAL(geometry_conversion_factor(), floating_point_type(1.0)                  , "Conversion factor for unit AU wrong.");
   cl_data.units = "aa";
   UNIT_ASSERT_FEQUAL(geometry_conversion_factor(), floating_point_type(1.889726124993590e+00), "Conversion factor for unit AA wrong.");
   cl_data.units = "au";
}

/*************************************************************** 
 * 
 * Main function
 * 
 ***************************************************************/
int main(int argc, const char* argv[])
{
   // Create cutee test suite
   cutee::suite suite("MidasPot unit tests.");

   // Add tests
   suite.add_function("Test geometry conversion factors.", geometry_conversion_test);

   // Run test suite
   #if defined(CUTEE_VERSION) && ((CUTEE_VERSION_MAJOR >= 2) || (CUTEE_VERSION_MAJOR == 2 && CUTEE_VERSION_PATCH >= 5))
      if(!suite.run())
      {
         return 1;
      }
   #else
      suite.run();
   #endif /* CUTEE_VERSION */
   
   // Return
   return 0;
}

#endif /* MIDASPOT_UNITTEST_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/UnitTest.h header end
 *
 *=============================================================*/
