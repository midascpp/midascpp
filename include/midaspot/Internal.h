/*=============================================================*
 *
 * midaspot/Internal.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_INTERNAL_H_INCLUDED
#define MIDASPOT_INTERNAL_H_INCLUDED

/*************************************************************** 
 * 
 * Kabsch algorithm for rotating to reference frame 
 * 
 ***************************************************************/
#ifdef LAPACKLINK
extern "C"
{
   void sgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , float* a,  int* lda, float* s
              , float* u,  int* ldu
              , float* vt, int* ldvt
              , float* work, int* lwork, int* info
              );

   void dgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , double* a,  int* lda, double* s
              , double* u,  int* ldu
              , double* vt, int* ldvt
              , double* work, int* lwork, int* info
              );
}

void load_matrix(floating_point_type mat_array[][3], floating_point_type* mat, int m, int n)
{
   int counter = 0;
   for(int j = 0; j < n; ++j)
   {
      for(int i = 0; i < m; ++i)
      {
         mat[counter] = mat_array[i][j];
         ++counter;
      }
   }
}

struct svd_type
{
   floating_point_type* u;
   floating_point_type* vt;
   floating_point_type* s;
   
   ~svd_type()
   {
      delete[] u;
      delete[] vt;
      delete[] s;
   }
};

svd_type lapack_gesvd(floating_point_type mat_array[][3], int m, int n)
{
   char jobu  = 'S';
   char jobvt = 'S';
   int  lda   = m;
   int  ldu   = m;
   int  ldvt  = std::min(m, n);
   int  ucol  = std::min(m, n);
   int  ssize = std::min(m, n);
   int  lwork = std::max(std::max(10000, 3 * std::min(m, n) + std::max(m, n)), 5 * std::min(m, n));
   floating_point_type* work = new floating_point_type[lwork];
   floating_point_type* mat  = new floating_point_type[m * n];
   load_matrix(mat_array, mat, m, n);
   svd_type    svd;
   svd.u  = new floating_point_type[ldu  * ucol];
   svd.vt = new floating_point_type[ldvt * n];
   svd.s  = new floating_point_type[ssize];
   int info;
   #if defined(FLOAT32)
   sgesvd_(&jobu, &jobvt, &m, &n, mat, &lda, svd.s, svd.u, &ldu, svd.vt, &ldvt, work, &lwork, &info);
   #else
   dgesvd_(&jobu, &jobvt, &m, &n, mat, &lda, svd.s, svd.u, &ldu, svd.vt, &ldvt, work, &lwork, &info);
   #endif /* FLOAT32 */
   if(info != 0) { std::cout << "Info != 0, exiting..." << std::endl; std::exit(-1); }
   delete[] mat;
   delete[] work;
   return svd;
}

void find_center_of_mass(floating_point_type** geometry, floating_point_type* masses, floating_point_type* center_of_mass, int natoms)
{
   center_of_mass[0] = floating_point_type(0.0);
   center_of_mass[1] = floating_point_type(0.0);
   center_of_mass[2] = floating_point_type(0.0);
   
   floating_point_type total_mass = floating_point_type(0.0);
   
   for(int i = 0; i < natoms; ++i)
   {
      total_mass += masses[i];
      center_of_mass[0] += masses[i] * geometry[i][0];
      center_of_mass[1] += masses[i] * geometry[i][1];
      center_of_mass[2] += masses[i] * geometry[i][2];
   }
   
   auto inv_total_mass = floating_point_type(1.0 / total_mass);
   center_of_mass[0] *= inv_total_mass;
   center_of_mass[1] *= inv_total_mass;
   center_of_mass[2] *= inv_total_mass;
}

void shift_geometry(floating_point_type** geometry, floating_point_type* shift, int natoms)
{
   for(int i = 0; i < natoms; ++i)
   {
      geometry[i][0] -= shift[0];
      geometry[i][1] -= shift[1];
      geometry[i][2] -= shift[2];
   }
}

void kabsch_rotate(molecule_external_data_type& molecule_input)
{
   /* Calculate Center of Mass's (CoM) and translationally align input structure and reference
    * Both structures will be moved to have CoM in origo
    */
   int natoms = molecule.natoms;
   floating_point_type com_reference[3];
   floating_point_type com_input[3];

   dynamic_matrix geometry(natoms, 3);
   for(int i = 0; i < natoms; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         geometry[i][j] = molecule.geometry[i][j];
      }
   }
   
   find_center_of_mass(geometry.accessor               , molecule.masses, com_reference, natoms);
   find_center_of_mass(molecule_input.geometry.accessor, molecule.masses, com_input    , natoms);
   
   shift_geometry(geometry.accessor               , com_reference, natoms);
   shift_geometry(molecule_input.geometry.accessor, com_input    , natoms);
   
   for(int i = 0; i < natoms; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         molecule.geometry[i][j] = geometry[i][j];
      }
   }
   
   /* Calculate H and do SVD */
   floating_point_type h_mat[3][3];
   for(int i = 0; i < 3; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         h_mat[i][j] = floating_point_type(0.0);
         for(int k = 0; k < natoms; ++k)
         {
            h_mat[i][j] += molecule_input.geometry[k][i] * molecule.masses[k] * molecule.geometry[k][j];
         }
      }
   }
   
   auto svd = lapack_gesvd(h_mat, 3, 3);
   floating_point_type vut[3][3];
   for(int i = 0; i < 3; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         vut[i][j] = floating_point_type(0.0);
         for(int k = 0; k < 3; ++k)
         {
            vut[i][j] += svd.vt[i * 3 + k] * svd.u[j + k * 3];
         }
      }
   }
   
   floating_point_type det   =  vut[0][0] * vut[1][1] * vut[2][2]
                    +  vut[0][1] * vut[1][2] * vut[2][0]
                    +  vut[0][2] * vut[1][0] * vut[2][1]
                    -  vut[0][2] * vut[1][1] * vut[2][0]
                    -  vut[0][1] * vut[1][0] * vut[2][2]
                    -  vut[0][0] * vut[1][2] * vut[2][1]
                    ;
   for(int i = 0; i < 3; ++i)
   {
      for(int j = 0; j < 3; ++j)
      {
         molecule_input.rot_matrix[i][j] = floating_point_type(0.0);
         for(int k = 0; k < 3; ++k)
         {
            molecule_input.rot_matrix[i][j] += svd.vt[i * 3 + k] * svd.u[j + k * 3] * (k == 2 ? det : 1.0);
         }
      }
   }
   
   /* Rotate the input coordinates to the reference frame */
   floating_point_type temp[3];
   for(int i = 0; i < natoms; ++i)
   {
      temp[0] = molecule_input.geometry[i][0];
      temp[1] = molecule_input.geometry[i][1];
      temp[2] = molecule_input.geometry[i][2];
      molecule_input.geometry[i][0] = molecule_input.rot_matrix[0][0] * temp[0] + molecule_input.rot_matrix[0][1] * temp[1] + molecule_input.rot_matrix[0][2] * temp[2];
      molecule_input.geometry[i][1] = molecule_input.rot_matrix[1][0] * temp[0] + molecule_input.rot_matrix[1][1] * temp[1] + molecule_input.rot_matrix[1][2] * temp[2];
      molecule_input.geometry[i][2] = molecule_input.rot_matrix[2][0] * temp[0] + molecule_input.rot_matrix[2][1] * temp[1] + molecule_input.rot_matrix[2][2] * temp[2];
   }
   
}

#endif /* LAPACKLINK*/


/*************************************************************** 
 * 
 * Operator evaluation
 * 
 ***************************************************************/
/**
 * Generate displacement and Q-vector
 **/
void generate_displacement_and_q(const molecule_internal_data_type& molecule_internal_data, molecule_external_data_type& molecule_external_data)
{
   for(int i = 0; i < molecule_external_data.natoms; ++i)
   {
      molecule_external_data.displacement[i][0] = molecule_external_data.geometry[i][0] - molecule.geometry[i][0];
      molecule_external_data.displacement[i][1] = molecule_external_data.geometry[i][1] - molecule.geometry[i][1];
      molecule_external_data.displacement[i][2] = molecule_external_data.geometry[i][2] - molecule.geometry[i][2];
      molecule_external_data.mass_scaled_displacement[i][0] = molecule_external_data.displacement[i][0] * std::sqrt(molecule.masses[i] * 1.82288848000000007e+03);
      molecule_external_data.mass_scaled_displacement[i][1] = molecule_external_data.displacement[i][1] * std::sqrt(molecule.masses[i] * 1.82288848000000007e+03);
      molecule_external_data.mass_scaled_displacement[i][2] = molecule_external_data.displacement[i][2] * std::sqrt(molecule.masses[i] * 1.82288848000000007e+03);
   }
   
   int ncart = molecule_external_data.natoms * 3;
   for(int i = 0; i < molecule_external_data.ndegrees_of_freedom; ++i)
   {
      molecule_external_data.q[i] = 0.0;
      for(int j = 0; j < ncart; ++j)
      {
         int atom  = std::floor(j/3);
         int coord = j%3;
         molecule_external_data.q[i] += molecule_internal_data.normal_coordinates[i][j] * molecule_external_data.mass_scaled_displacement[atom][coord];
      }
   }
}

/*************************************************************** 
 * 
 * Operator evaluation
 * 
 ***************************************************************/
/**
 * Struct for storing operator values.
 **/
struct oper_value
{
   int            order;
   std::string    name;
   dynamic_vector values;
   
   oper_value(int o, const std::string& n) 
      :  order(o)
      ,  name(n)
      ,  values(std::pow(3, order))
   {
      for(int i = 0; i < values.size; ++i)
      {
         values[i] = floating_point_type(0);
      }
   }
};

/**
 * Rotate tensor operators.
 * Does the following rotation of a given tensor operator:
 * 
 *    \f[
 *       T_{i_1i_2...i_n}^R = \sum_{r_1} \sum_{r_2} ... \sum_{r_n} R_{i_1r_1} R_{i_2r_2} ... R_{i_nr_n} T_{r_1r_2...r_n}
 *    \f]
 * 
 **/
void rotate_tensor_operator(oper_value& oper, const floating_point_type rotmat[][3])
{
   const int dim  = 3;
   const int size = std::pow(dim, oper.order);
   dynamic_vector tensor_temp(size);
   
   for(int iorder = 0; iorder < oper.order; ++iorder)
   {
      const int nblocks    = std::pow(dim, oper.order - iorder - 1);
      const int incr       = std::pow(dim, iorder);
      const int block_size = incr;
      
      for(int block = 0; block < nblocks; ++block)
      {
         for(int block_elem = 0; block_elem < block_size; ++block_elem)
         {
            int elem = block * block_size * dim + block_elem;
            tensor_temp[elem           ] = rotmat[0][0] * oper.values[elem] + rotmat[1][0] * oper.values[elem + incr] + rotmat[2][0] * oper.values[elem + 2 *incr];
            tensor_temp[elem +     incr] = rotmat[0][1] * oper.values[elem] + rotmat[1][1] * oper.values[elem + incr] + rotmat[2][1] * oper.values[elem + 2 *incr];
            tensor_temp[elem + 2 * incr] = rotmat[0][2] * oper.values[elem] + rotmat[1][2] * oper.values[elem + incr] + rotmat[2][2] * oper.values[elem + 2 *incr];
         }
      }
      for(int isize = 0; isize < size; ++isize)
      {
         oper.values[isize] = tensor_temp[isize];
      }
   }
}

#endif /* MIDASPOT_INTERNAL_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/Internal.h header end
 *
 *=============================================================*/
