# ==============================================================================
#  STANDARD CONFIGURATION
# ==============================================================================
# These are your standard settings (as set by the 'configure' script) and will
# be used for the regular build/installation.
# See the 'build types' section below on how to define a new build type.

# ------------------------------------------------------------------------------
#  COMPILERS, FLAGS, ETC.
# ------------------------------------------------------------------------------
# # Build architecture.
ARCH           = @build_type@
EXT_DYNLIB     = @ext_dynlib@
DYNLIB_NAME    = @DYNLIB_NAME@

# Compilers.
FC             = @FC@
CC             = @CC@
CXX            = @CXX@
CCACHE         = @CCACHE@
COMPILE_FC     = $(strip $(FC))
COMPILE_CC     = $(strip $(CCACHE) $(CC))
COMPILE_CXX    = $(strip $(CCACHE) $(CXX))

# Compiler flags.
WARNINGFLAGS   = -Wall -Wno-sign-compare -Wno-strict-overflow -Wno-unused-variable -Wno-unused-function -Wno-unknown-pragmas
FFLAGS         = @FCFLAGS@
CFLAGS         = @CCFLAGS@
CXXFLAGS       = $(WARNINGFLAGS) @CXXFLAGS@
CPPFLAGS       = @CPPFLAGS@ @BOOST_CPPFLAGS@ -pthread
LDFLAGS        = @LDFLAGS@
LIB_LIST       = @LIBS@ @BOOST_LDFLAGS@ libmda/lib/libmda.a -pthread -ldl
INCLUDE_DIR    = @INCLUDE_DIR@
INSTALLDIR     = @INSTALL_BINDIR@
GCCTOOLCHAIN   = @GCCTOOLCHAIN@

# Misc. commands.
RM             = rm -f
MKDIR          = mkdir
MKDIR_P        = mkdir -p
FIND           = find
PRINTF         = printf
AR             = @AR@
ARFLAGS        = @ARFLAGS@
CMAKE          = cmake
GIT	         = git
TAR            = tar
STAT           = @STAT@
STAT_MODE      = @STAT_MODE@
STRIP          = @STRIP@

# ------------------------------------------------------------------------------
#  LIBRARIES, PROGRAMS
# ------------------------------------------------------------------------------
# EXTERNAL LIBRARIES
EXTERNAL    = @EXTERNAL@
DOWNLOADCMD = @DOWNLOADCMD@ 
lapack_URL  = @with_lapack_url@
gsl_URL   	= @with_gsl_url@
fftw_URL   	= @with_fftw_url@
boost_URL   = @with_boost_url@
cutee_URL   = @with_cutee_url@
stackd_URL  = @with_stackd_url@

# 'Tinker' options.
MAKE_TINKER    = @enable_tinker@
PATH_TO_TINKER = @tinker_path@

# ------------------------------------------------------------------------------
#  INSTALLATION
# ------------------------------------------------------------------------------
# Build directory (where output files from compilation will be put).
BUILDDIR       = @enable_build_dir@
SOURCEDIR      = @SOURCE_DIR@

# Install directory and subdirectories (where installation will be done).
INSTALLPREFIX      = @INSTALL_PREFIX@
INSTALLBINDIR      = @INSTALL_BINDIR@
INSTALLEXECDIR     = @INSTALL_EXECDIR@
INSTALLLIBDIR      = @INSTALL_LIBDIR@
INSTALLINCLUDEDIR  = @INSTALL_INCLUDEDIR@
INSTALLSHAREDIR    = @INSTALL_SHAREDIR@

# "Source" directories, from which the installation is processed.
ORIGININCLUDEDIR  = @SOURCE_DIR@/include
ORIGINSHAREDIR    = @SOURCE_DIR@/share

# Installation programs.
mkinstalldirs  = @SOURCE_DIR@/config/mkinstalldirs
install        = @INSTALL@

# ------------------------------------------------------------------------------
#  MISCELLANEOUS
# ------------------------------------------------------------------------------
# MAKE DIRECTORY
# 'make' puts some files in this directory in order to provide the other
# functionalities in this section. (By default it is a hidden top-level
# directory.)
dir_MAKE = @enable_make_dir@

# SCRATCH DIRECTORY
# The scratch directory used in connection with the test suite. Not used by
# 'make' as such, only for excluding it from file searches which is relevant if
# the user has chosen the scratch directory to be a subdirectory of the Midas
# main directory. Absolute path assumed.
dir_SCRATCH = @enable_scratch_dir@

# FILE SEARCH
# If reuse_file_search = true, 'make' will read in list of directories and
# object files from  and .make/lst_OBJ.makefile (or whatever is defined as
# $(file_VPATH) and $(file_lst_OBJ), but don't worry about this.
# These files are updated every time 'make' is executed normally. If no
# directories or source files have been added/removed since last execution, it
# is then possible to avoid the complete file search (using shell command
# 'find'), which can sometimes take a while if the file system is slow.
# The ?= means: assign value only if not already defined. This means that the
# value can e.g. be defined in the command line; 
#    make reuse_file_search=true <args>
# which will then override the assignment here.
# Any other value than 'true', including no assignment, disables the feature.
reuse_file_search ?= @enable_reuse_file_search@

# DISPLAY PROGRESS
# Turn on/off an output like
#    [ 43% (of 417 files)]
# at the beginning of each compilation command line.
# Also toggle colors on/off (bold green) if your terminal supports it (checked
# by the configure script).
# Any other value than 'true', including no assignment, disables the feature.
progress_display ?= @enable_progress_display@
color_support    ?= @enable_color_support@
# If color_support = true the two variables below will control color
# formatting; otherwise they have no effect. 'setaf' or 'setf' controls
# foreground color, and which of the two it is depends on the terminal. The
# right one _should_ have been chosen by the 'configure' script. If none are
# set, it means none of them worked.
# There are no actual standards, but the color scheme _could_ be something like
# this (and probably your terminal supports 256 colors or more; these are just
# the basic ones). Try for yourself if you want to.
#    tput sgr0      _should_ return to normal text.
#    tput setaf 0   black
#    tput setaf 1   red
#    tput setaf 2   green (default)
#    tput setaf 3   yellow
#    tput setaf 4   blue
#    tput setaf 5   magenta
#    tput setaf 6   cyan
#    tput setaf 7   grey
color_format_beg ?= @color_format_beg@
color_format_end ?= @color_format_end@

# VERBOSITY
# This will control whether or not to print the commands used for compiling,
# linking, and making subdirectories. Can be useful for debugging the building
# process, but it can be a massive wall of text.
# Any other value than 'true', including no assignment, gives non-verbose
# output.
verbose_make ?= @enable_verbose_make@

# ==============================================================================
#  TARGETS AND BUILD TYPES
# ==============================================================================
# Default target:
.PHONY : default
default: all

# The name of the main/primary build (just a name, no significance for the
# final program).
MAIN_build     =  release
lst_BUILDS     := $(MAIN_build)

# To create a new build type, simply append the build type name by adding a
#    lst_BUILDS += <build_type_name>
# line below. The making of specific build types are toggled off/on by
# commenting/uncommenting the corresponding lines.
# The specifications for the new build type are set in the 'build type
# configurations' section below by setting the appropriate variables.
@debug_build_comment@lst_BUILDS     += debug

# ==============================================================================
#  BUILD TYPE CONFIGURATIONS
# ==============================================================================
# Set the variables of each build type by inserting lines like
#    <variable_name>_<build_type_name> := <value>
# If standard values are desired, just put $(<variable_name>) as the value. See
# the main and debug build configurations for examples.
# NB! You may want to set these in 'Makefile.config.in' instead of
# 'Makefile.config' since the latter will be overwritten when running
# 'configure'.

# Main build.
# (Should always just be a duplicate of the standard configurations above. If
# you find that anything needs to be changed here, you should probably change
# the way 'configure' sets the variables.)
ARCH_$(MAIN_build)         := $(ARCH)
FC_$(MAIN_build)           := $(COMPILE_FC)
CC_$(MAIN_build)           := $(COMPILE_CC)
CXX_$(MAIN_build)          := $(COMPILE_CXX)
FFLAGS_$(MAIN_build)       := $(FFLAGS)
CFLAGS_$(MAIN_build)       := $(CFLAGS)
CXXFLAGS_$(MAIN_build)     := $(CXXFLAGS)
CPPFLAGS_$(MAIN_build)     := $(CPPFLAGS)
LDFLAGS_$(MAIN_build)      := $(LDFLAGS)
LIB_LIST_$(MAIN_build)     := $(LIB_LIST)
INCLUDE_DIR_$(MAIN_build)  := $(INCLUDE_DIR)
STRIP_$(MAIN_build)        := $(STRIP)

# Debug build.
ARCH_debug           := $(ARCH)
FC_debug             := $(COMPILE_FC)
CC_debug             := $(COMPILE_CC)
CXX_debug            := $(COMPILE_CXX)
FFLAGS_debug         := @FCFLAGS_DEBUG@
CFLAGS_debug         := @CCFLAGS_DEBUG@
CXXFLAGS_debug       := $(WARNINGFLAGS) @CXXFLAGS_DEBUG@
CPPFLAGS_debug       := $(CPPFLAGS)
LDFLAGS_debug        := $(LDFLAGS)
LIB_LIST_debug       := $(LIB_LIST)
INCLUDE_DIR_debug    := $(INCLUDE_DIR)
STRIP_debug          :=


