#include <iostream>

#include "test/ParalTest.h"

namespace midas::test
{

void ParalTest()
{
   cutee::suite suite("Paral test");
   
   //
   suite.add_test<RunAsyncTest>("PARALTEST: RUNASYNCTEST ");
   suite.add_test<RunAsyncFailTest>("PARALTEST: RUNASYNCFAILTEST ");
   
   //
   RunSuite(suite);
}

} /* namespace midas::test */
