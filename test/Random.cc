/**
 *******************************************************************************
 * 
 * @file    Random.cc
 * @date    14-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Random data for testing.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "test/Random.h"
#include "test/Random_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_RANDOMNUMBER_INT(T) \
   namespace midas::test::random \
   { \
      template T RandomNumber<T>(bool); \
   } /* namespace midas::test::random */ \

#define INSTANTIATE_RANDOM(T) \
   namespace midas::test::random \
   { \
      template T RandomNumber<T>(bool); \
      template std::vector<T> RandomStdVector<T>(const std::size_t, bool); \
      template GeneralMidasVector<T> RandomMidasVector<T>(const std::size_t, bool); \
      template GeneralMidasMatrix<T> RandomMidasMatrix<T>(const std::size_t, const std::size_t, bool); \
      template GeneralMidasMatrix<T> RandomHermitianMidasMatrix(const std::size_t, bool); \
      template GeneralMidasMatrix<T> RandomAntiHermitianMidasMatrix(const std::size_t, bool); \
      template GeneralDataCont<T> RandomDataCont<T>(const std::size_t, bool); \
      template void LoadRandom(T*, const std::size_t, bool); \
      template struct RandomVectorContainer<T, std::vector>; \
      template struct RandomVectorContainer<T, GeneralMidasVector>; \
      template struct RandomVectorContainer<T, GeneralDataCont>; \
   } /* namespace midas::test::random */ \

// Instantiations.
INSTANTIATE_RANDOMNUMBER_INT(bool);
INSTANTIATE_RANDOMNUMBER_INT(short);
INSTANTIATE_RANDOMNUMBER_INT(int);
INSTANTIATE_RANDOMNUMBER_INT(long);
INSTANTIATE_RANDOMNUMBER_INT(unsigned short);
INSTANTIATE_RANDOMNUMBER_INT(unsigned int);
INSTANTIATE_RANDOMNUMBER_INT(unsigned long);
INSTANTIATE_RANDOM(float);
INSTANTIATE_RANDOM(double);
INSTANTIATE_RANDOM(std::complex<float>);
INSTANTIATE_RANDOM(std::complex<double>);
// NB! No 'long double' versions, because std::complex<long double> does not
// have an MPI datatype trait and long double does not have cutee support.

#undef INSTANTIATE_RANDOM
#undef INSTANTIATE_RANDOMNUMBER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
