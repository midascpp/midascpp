#include "test/SplineNDTestCase.h"

#include "util/MidasStream.h"

extern MidasStream Mout;

namespace midas::test
{

void SplineNDTest()
{
   cutee::suite suite("SplineND test");
   
   suite.add_test<SplineNDTestCase>("SplineND test");
   
   RunSuite(suite);
}

} /* namespace midas::test */
