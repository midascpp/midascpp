/**
************************************************************************
* 
* @file                McTdHVectorContainerTest.h
*
* Created:             20-12-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Unit tests for McTdHVectorContainer
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MCTDHVECTORCONTAINERTEST_H_INCLUDED
#define MCTDHVECTORCONTAINERTEST_H_INCLUDED

#include <complex>
#include <numeric>
#include "td/mctdh/container/McTdHVectorContainer.h"
#include "test/CuteeInterface.h"

namespace detail
{

/**
 * Create CasTdHVectorContainer%s for testing
 **/
template
   <  typename T
   >
CasTdHVectorContainer<T> make_test_castdhvector
   (  char aType = 'a'
   )
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
   using real_t = midas::type_traits::RealTypeT<T>;

   // N = 6
   In nbas = I_6;

   // M = 3, n = 3
   std::vector<unsigned> act_space(3, 3);
   auto nmodes = act_space.size();

   // Number of modal coefs = \sum_m N^m * n^m = 3*6*3 = 54
   size_t n_modal_coefs = nbas*std::accumulate(act_space.begin(), act_space.end(), 0);

   // Construct zero container
   CasTdHVectorContainer<T> result(act_space, n_modal_coefs);


   switch   (  aType
            )
   {
      case 'a':
      {
         // Set modal coefs. Just nact first unit vectors in each mode.
         // If complex, rotate them!
         size_t off=0;
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            for(In iact=I_0; iact<act_space[imode]; ++iact)
            {
               if constexpr   (  is_complex
                              )
               {
                  result.Modals()[off+iact] = std::polar(real_t(1.), real_t(0.1)*iact);
               }
               else
               {
                  result.Modals()[off+iact] = T(1.);
               }
               off += nbas;
            }
         }

         // Set configuration-space coefficients. Just a (rotated) 1 in the first element.
         if constexpr   (  is_complex
                        )
         {
            static_cast<SimpleTensor<T>*>(result.Coefs().GetTensor())->GetData()[0] = std::polar(real_t(1.), real_t(0.5));
         }
         else
         {
            static_cast<SimpleTensor<T>*>(result.Coefs().GetTensor())->GetData()[0] = T(1.);
         }

         break;
      }
      case 'b':
      {
         // Set modal coefs. Linear combinations of two subsequent primitive-basis functions with equal weights.
         size_t off=0;
         for(In imode=I_0; imode<nmodes; ++imode)
         {
            MidasAssert(nbas >= 2*act_space[imode], "CasTdHVectorContainer 'b' requires nbas >= 2*nact for all modes");

            for(In iact=I_0; iact<act_space[imode]; ++iact)
            {
               real_t val = real_t(1.)/std::sqrt(2.);

               if constexpr   (  is_complex
                              )
               {
                  result.Modals()[off+I_2*iact] = std::polar(val, real_t(0.1*iact));
                  result.Modals()[off+I_2*iact+I_1] = std::polar(val, real_t(0.12*iact));
               }
               else
               {
                  result.Modals()[off+I_2*iact] = val;
                  result.Modals()[off+I_2*iact+I_1] = val;
               }

               off += nbas;
            }
         }

         // Set configuration-space coefficients. Just a 1 in the first element.
         auto* data_ptr = static_cast<SimpleTensor<T>*>(result.Coefs().GetTensor())->GetData();
         auto totalsize = result.Coefs().TotalSize();
         real_t one_sqrt_n = real_t(1.)/std::sqrt(totalsize);
         if constexpr   (  is_complex
                        )
         {
            for(In icoef=I_0; icoef<totalsize; ++icoef)
            {
               data_ptr[icoef] = real_t(std::pow(-1., icoef))*std::polar(one_sqrt_n, real_t(icoef*0.1));
            }
         }
         else
         {
            for(In icoef=I_0; icoef<totalsize; ++icoef)
            {
               data_ptr[icoef] = real_t(std::pow(-1., icoef))*one_sqrt_n; // alternating sign
            }
         }

         break;
      }
      default:
      {
         std::string err = "No match for aType = ";
         err.push_back(aType);
         err += ".";
         MIDASERROR(err);
      }
   }

   // Return result
   return result;
}

} /* namespace detail */

/**
 * Test for constructing a CasTdHVectorContainer and calculating norm
 **/
template
   <  typename T
   >
class CasTdHVectorContainerTest
   :  public virtual cutee::test
{
   private:
      static constexpr bool mIsComplex = midas::type_traits::IsComplexV<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

      void actual_test
         (
         )
      {
         auto casvec_a = detail::make_test_castdhvector<T>('a');
         auto casvec_b = detail::make_test_castdhvector<T>('b');

         auto coef_dims_a = casvec_a.Coefs().GetDims();
         auto coef_dims_b = casvec_b.Coefs().GetDims();

         real_t refnorm_a = std::sqrt(std::accumulate(coef_dims_a.begin(), coef_dims_a.end(), 0)+1.);
         real_t refnorm_b = std::sqrt(std::accumulate(coef_dims_b.begin(), coef_dims_b.end(), 0)+1.);

         real_t norm_a = casvec_a.Norm();
         real_t norm_b = casvec_b.Norm();

         UNIT_ASSERT_FEQUAL(norm_a, refnorm_a, "CasTdHVectorContainer::Norm() failed!");
         UNIT_ASSERT_FEQUAL(norm_b, refnorm_b, "CasTdHVectorContainer::Norm() failed!");
      }

   public:
      void run
         (
         )  override
      {
         this->actual_test();
      }
};

/**
 * Test for Axpys
 **/
template
   <  typename T
   >
class CasTdHVectorContainerAxpyTest
   :  public virtual cutee::test
{
   private:
      static constexpr bool mIsComplex = midas::type_traits::IsComplexV<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

      void actual_test
         (
         )
      {
         const auto casvec_a = detail::make_test_castdhvector<T>('a');
         const auto casvec_b = detail::make_test_castdhvector<T>('b');

         {
            auto diff_a = casvec_a;
            diff_a.Axpy(casvec_a, real_t(-1.));
            auto diff_a_norm = diff_a.Norm();
            bool zero_a = libmda::numeric::float_numeq_zero(diff_a_norm, casvec_a.Norm());
            UNIT_ASSERT(zero_a, "CasTdHVectorContainer Axpy failed for real_t scalar!");
         }

         {
            auto diff_a_complex = casvec_a;
            diff_a_complex.Axpy(casvec_a, T(-1.));
            auto diff_a_norm_complex = diff_a_complex.Norm();
            bool zero_a_complex = libmda::numeric::float_numeq_zero(diff_a_norm_complex, casvec_a.Norm());
            UNIT_ASSERT(zero_a_complex, "CasTdHVectorContainer Axpy failed for T scalar!");
         }

         {
            auto diff_b = casvec_b;
            diff_b.Axpy(casvec_b, real_t(-1.));
            auto diff_b_norm = diff_b.Norm();
            bool zero_b = libmda::numeric::float_numeq_zero(diff_b_norm, casvec_b.Norm());
            UNIT_ASSERT(zero_b, "CasTdHVectorContainer Axpy failed for real_t scalar!");
         }

         {
            auto diff_b_complex = casvec_b;
            diff_b_complex.Axpy(casvec_b, T(-1.));
            auto diff_b_norm_complex = diff_b_complex.Norm();
            bool zero_b_complex = libmda::numeric::float_numeq_zero(diff_b_norm_complex, casvec_b.Norm());
            UNIT_ASSERT(zero_b_complex, "CasTdHVectorContainer Axpy failed for T scalar!");
         }

         {
            auto axpy_a = casvec_a;
            axpy_a.Axpy(casvec_a, real_t(2.));
            auto scaled_a = casvec_a;
            scaled_a.Scale(real_t(3.));
            scaled_a.Axpy(axpy_a, real_t(-1.));
            bool zero_axpy_a = libmda::numeric::float_numeq_zero(scaled_a.Norm(), axpy_a.Norm());
            UNIT_ASSERT(zero_axpy_a, "CasTdHVectorContainer Axpy and Scale not equivalent with real_t scalar!");
         }

         {
            auto axpy_a_complex = casvec_a;
            axpy_a_complex.Axpy(casvec_a, T(2.));
            auto scaled_a_complex = casvec_a;
            scaled_a_complex.Scale(T(3.));
            scaled_a_complex.Axpy(axpy_a_complex, T(-1.));
            bool zero_axpy_a_complex = libmda::numeric::float_numeq_zero(scaled_a_complex.Norm(), axpy_a_complex.Norm());
            UNIT_ASSERT(zero_axpy_a_complex, "CasTdHVectorContainer Axpy and Scale not equivalent with T scalar!");
         }

         {
            auto axpy_b = casvec_b;
            axpy_b.Axpy(casvec_b, real_t(2.));
            auto scaled_b = casvec_b;
            scaled_b.Scale(real_t(3.));
            scaled_b.Axpy(axpy_b, real_t(-1.));
            bool zero_axpy_b = libmda::numeric::float_numeq_zero(scaled_b.Norm(), axpy_b.Norm());
            UNIT_ASSERT(zero_axpy_b, "CasTdHVectorContainer Axpy and Scale not equivalent with real_t scalar!");
         }

         {
            auto axpy_b_complex = casvec_b;
            axpy_b_complex.Axpy(casvec_b, T(2.));
            auto scaled_b_complex = casvec_b;
            scaled_b_complex.Scale(T(3.));
            scaled_b_complex.Axpy(axpy_b_complex, T(-1.));
            bool zero_axpy_b_complex = libmda::numeric::float_numeq_zero(scaled_b_complex.Norm(), axpy_b_complex.Norm());
            UNIT_ASSERT(zero_axpy_b_complex, "CasTdHVectorContainer Axpy and Scale not equivalent with T scalar!");
         }
      }

   public:
      void run
         (
         )  override
      {
         this->actual_test();
      }
};

/**
 * Test for constructing a CasTdHVectorContainer and calculating norm
 **/
template
   <  typename T
   >
class CasTdHVectorContainerZeroTest
   :  public virtual cutee::test
{
   private:
      static constexpr bool mIsComplex = midas::type_traits::IsComplexV<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

      void actual_test
         (
         )
      {
         auto casvec_a = detail::make_test_castdhvector<T>('a');
         auto casvec_b = detail::make_test_castdhvector<T>('b');

         auto norm_a = casvec_a.Norm();
         auto norm_b = casvec_b.Norm();

         casvec_a.Zero();
         casvec_b.Zero();

         bool a_zero = libmda::numeric::float_numeq_zero(casvec_a.Norm(), norm_a);
         bool b_zero = libmda::numeric::float_numeq_zero(casvec_b.Norm(), norm_b);

         UNIT_ASSERT(a_zero, "CasTdHVectorContainer::Zero() failed!");
         UNIT_ASSERT(b_zero, "CasTdHVectorContainer::Zero() failed!");
      }

   public:
      void run
         (
         )  override
      {
         this->actual_test();
      }
};

#endif /* MCTDHVECTORCONTAINERTEST_H_INCLUDED */
