/**
 *******************************************************************************
 * 
 * @file    GlobalDataTest.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef GLOBALDATATEST_H_INCLUDED
#define GLOBALDATATEST_H_INCLUDED

#include "test/CuteeInterface.h"

namespace midas{
namespace test{
/***************************************************************************//**
 * @brief Namespace for testing stuff under midas::global
 ******************************************************************************/
namespace globaldata{

//@{
//! Tests.
struct TestServiceLocatorConstructor : public cutee::test {void run() override;};
struct TestServiceLocatorAddService  : public cutee::test {void run() override;};
struct TestServiceLocatorGetService  : public cutee::test {void run() override;};
struct TestServiceLocatorGlobalService  
   : public cutee::test 
{  
   void setup()    override;
   void run()      override;
   void teardown() override;
};
//@}

} /* namespace globaldata */
} /* namespace test */
} /* namespace midas */

#endif /* GLOBALDATATEST_H_INCLUDED */
