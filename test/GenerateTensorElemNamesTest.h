/**
 *******************************************************************************
 * 
 * @file    GenerateTensorElemNamesTest.h
 * @date    23-10-2019
 * @author  Bo Thomsen (drbothomsen@gmail.com)
 *
 * @brief
 *    A test for the GenerateTensorElemName utility function
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef GENERATETENSORELEMNAMETEST_H_INCLUDED
#define GENERATETENSORELEMNAMETEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/CuteeInterface.h"

#include "util/GenerateTensorElemName.h"

#include <string>
#include <vector>

namespace midas::test::generate_tensor_element_names 
{

   /************************************************************************//**
    * @brief
    *    Tests should be simple, so brief descriptions should suffice.
    *
    * Otherwise put some more details here. //CHANGE! comments
    ***************************************************************************/
   struct TestGeneratedTensorElementNames
      :  public cutee::test
   {
      void run() override
      {
         // Use these assertions for the tests. All of them will automatically
         // print expected and actual values.

         std::vector<std::string> test_vals = {"X","Y","Z","XX","XY","XZ","YX","YY","YZ","ZX","ZY","ZZ","XXX","XXY"};
         midas::util::GenerateTensorElemName ThreeDimCart({"X","Y","Z"});
         for(const auto& name : test_vals)
         {
            UNIT_ASSERT_EQUAL(name, ThreeDimCart.GetAndAdvance(), "Error failed running generator test");
         }

         test_vals = {"XX","XY","XZ","YX","YY","YZ","ZX","ZY","ZZ"};
         ThreeDimCart.Reset(2);
         for(const auto& name : test_vals)
         {
            UNIT_ASSERT_EQUAL(name, ThreeDimCart.GetAndAdvance(), "Error failed reset and run test");
         }
         
         midas::util::GenerateTensorElemNameNthOrder periodic({"_DOG", "_CAT", "_BIRD"},1);
         test_vals = {"_DOG", "_CAT", "_BIRD","_DOG", "_CAT", "_BIRD"};
         for(const auto& name : test_vals)
         {
            UNIT_ASSERT_EQUAL(name, periodic.GetAndAdvance(), "Error failed periodic name gen test");
         }
      }
   };

} /* namespace midas::test::generate_tensor_element_names */

#endif/*GENERATETENSORELEMNAMETEST_H_INCLUDED*/
