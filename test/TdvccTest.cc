/**
 *******************************************************************************
 * 
 * @file    TdvccTest.cc
 * @date    15-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/TdvccTest.h"
#include "util/type_traits/TypeName.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

namespace midas::test
{
   namespace tdvcc::detail
   {
      template
         <  typename T
         ,  template<typename> class CONT_T
         >
      void AddParamsTdvccImpl
         (  cutee::suite& arSuite
         )
      {
         using midas::type_traits::TypeName;
         std::string s = "ParamsTdvcc<"+TypeName<T>()+","+TypeName<CONT_T>()+">; ";

         using namespace ::midas::test::tdvcc::params;
         arSuite.add_test<ParamsTdvccConstructorEmpty<T,CONT_T>>(s+"Constructor, empty");
         arSuite.add_test<ParamsTdvccConstructorZero<T,CONT_T>>(s+"Constructor, zero");
         arSuite.add_test<ParamsTdvccConstructorCustom<T,CONT_T>>(s+"Constructor, custom");
         arSuite.add_test<ParamsTdvcc_Scale<T,CONT_T>>(s+"Scale");
         arSuite.add_test<ParamsTdvcc_Zero<T,CONT_T>>(s+"Zero");
         arSuite.add_test<ParamsTdvcc_Axpy<T,CONT_T>>(s+"Axpy");
         arSuite.add_test<ParamsTdvcc_SetShape<T,CONT_T>>(s+"SetShape");
         arSuite.add_test<ParamsTdvcc_Norm<T,CONT_T>>(s+"Norm");
         arSuite.add_test<ParamsTdvcc_Size<T,CONT_T>>(s+"Size");
         arSuite.add_test<ParamsTdvcc_OdeMeanNorm2<T,CONT_T>>(s+"OdeMeanNorm2");
         arSuite.add_test<ParamsTdvcc_OdeMaxNorm2<T,CONT_T>>(s+"OdeMaxNorm2");
         arSuite.add_test<ParamsTdvcc_DataToPointer<T,CONT_T>>(s+"DataToPointer");
         arSuite.add_test<ParamsTdvcc_DataFromPointer<T,CONT_T>>(s+"DataFromPointer");
         arSuite.add_test<ParamsTdvcc_DiffNorm2<T,CONT_T>>(s+"DiffNorm2");
      }

      template
         <  typename PARAM_T
         ,  template<typename> class CONT_TMPL
         ,  template<typename, template<typename> class> class PARAM_CONT_TMPL
         ,  template<typename> class TRF_TMPL
         ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
         ,  bool IMAG_TIME = false
         >
      void AddDerivativeTestsImpl
         (  cutee::suite& arSuite
         ,  const std::string& arStrBase
         )
      {
         //using param_t = PARAM_T;
         //using trf_t = TRF_TMPL<PARAM_T>;
         //using deriv_t = DERIV_TMPL<PARAM_T, trf_t::template cont_tmpl, TRF_TMPL, IMAG_TIME>;

         using midas::type_traits::TypeName;
         const std::string s = " ("+TypeName<PARAM_T>()+", "+(IMAG_TIME? "imag t": "real t")+")";

         using namespace ::midas::test::tdvcc::params;
         using namespace ::midas::test::tdvcc::derivative;
         // Add for
         //    -  ParamsTdvci<T,GeneralMidasVector>, DerivTdvciMatRep
         //    -  ParamsTdvci<T,GeneralDataCont>,    DerivTdvci2
         //    -  ParamsTdvcc<T,GeneralMidasVector>, DerivTdvccMatRep
         //    -  ParamsTdvcc<T,GeneralDataCont>,    DerivTdvcc2

         std::vector<Uin> n_modals = {3,2,3};
         std::vector<Uin> n_opers  = {4,2,3};
         std::set<Uin> max_exci_set = {2};
         std::set<Uin> max_coup_set = {2};

         if constexpr   (  std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvcc<PARAM_T>>
                        || std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvci<PARAM_T>>
                        )
         {
            max_exci_set.emplace(1);
            max_coup_set.emplace(1);
            max_exci_set.emplace(3);
            max_coup_set.emplace(3);
         }

         auto info = [](const std::vector<Uin>& nm, const std::vector<Uin>& no, Uin maxexci, Uin maxcoup) -> std::string
            {
               std::stringstream ss;
               ss << ", Nm = " << nm
                  << ", Om = " << no
                  << ", VCC[" << maxexci << "]/H" << maxcoup
                  ;
               return ss.str();
            };

         for(const auto& max_exci : max_exci_set)
         {
            for(const auto& max_coup : max_coup_set)
            {
               std::string s_info = info(n_modals, n_opers, max_exci, max_coup);
               arSuite.add_test
                  <  DerivativeTest
                     <  PARAM_T
                     ,  CONT_TMPL
                     ,  PARAM_CONT_TMPL
                     ,  TRF_TMPL
                     ,  DERIV_TMPL
                     ,  IMAG_TIME
                     >
                  >("DerivativeTest; "+arStrBase+s+s_info, n_modals, n_opers, max_exci, max_coup);
            }
         }
      }

      template
         <  typename PARAM_T
         ,  bool IMAG_TIME
         ,  template
               <  typename
               ,  template<typename> class
               ,  template<typename> class
               ,  template<typename, template<typename> class, template<typename> class, bool> class
               ,  bool
               >
               class TEST
         ,  class... Args
         >
      void AddTdvccTestImpl
         (  cutee::suite& arSuite
         ,  const std::string& arStr
         ,  Args&&... args
         )
      {
         arSuite.add_test
            <  TEST<PARAM_T, GeneralMidasVector, TrfTdvccMatRep, DerivTdvcc, IMAG_TIME>
            >(arStr+", VCC MatRep", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralDataCont, TrfTdvcc2, DerivTdvcc, IMAG_TIME>
            >(arStr+", VCC[2]/H2", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralTensorDataCont, TrfGeneralTimTdvcc, DerivTdvcc, IMAG_TIME>
            >(arStr+", General VCC", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralMidasVector, TrfTdvciMatRep, DerivTdvci, IMAG_TIME>
            >(arStr+", VCI MatRep", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralDataCont, TrfTdvci2, DerivTdvci, IMAG_TIME>
            >(arStr+", VCI[2]/H2", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralTensorDataCont, TrfGeneralTimTdvci, DerivTdvci, IMAG_TIME>
            >(arStr+", General VCI", std::forward<Args>(args)...);
      }

      template
         <  typename PARAM_T
         ,  bool IMAG_TIME
         >
      void AddTdvccTestsImpl
         (  cutee::suite& arSuite
         )
      {
         //using namespace tdvcc::params;
         //using namespace tdvcc::derivative;
         // Add for
         //    -  ParamsTdvci<T,GeneralMidasVector>, DerivTdvciMatRep
         //    -  ParamsTdvci<T,GeneralDataCont>,    DerivTdvci2
         //    -  ParamsTdvcc<T,GeneralMidasVector>, DerivTdvccMatRep
         //    -  ParamsTdvcc<T,GeneralDataCont>,    DerivTdvcc2

         using midas::type_traits::TypeName;
         const std::string s = "Tdvcc ("+TypeName<PARAM_T>()+", "+(IMAG_TIME? "imag t": "real t")+"); ";

         std::vector<Uin> n_modals = {3,2,3};
         std::vector<Uin> n_opers  = {4,2,3};
         Uin max_exci = 2;
         Uin max_coup = 2;

         AddTdvccTestImpl<PARAM_T,IMAG_TIME,InitialTest>(arSuite, s+"InitialTest", n_modals, n_opers, max_exci, max_coup);
         AddTdvccTestImpl<PARAM_T,IMAG_TIME,TdOperTest>(arSuite, s+"TdOperTest", n_modals, n_opers, max_exci, max_coup);
         AddTdvccTestImpl<PARAM_T,IMAG_TIME,ExpValTest>(arSuite, s+"ExpValTest", n_modals, n_opers, max_exci, max_coup);
         AddTdvccTestImpl<PARAM_T,IMAG_TIME,WaterPotHExpValTest>(arSuite, s+"WaterPotHExpValTest");
      }

   } /* namespace tdvcc::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void TdvccTest()
   {
      // Create test_suite object.
      cutee::suite suite("Tdvcc test");

      // Add all the tests to the suite.
      using namespace tdvcc;
      tdvcc::detail::AddParamsTdvccImpl<Nb,GeneralMidasVector>(suite);
      tdvcc::detail::AddParamsTdvccImpl<Nb,GeneralDataCont>(suite);
      tdvcc::detail::AddParamsTdvccImpl<Nb,GeneralTensorDataCont>(suite);
      tdvcc::detail::AddParamsTdvccImpl<std::complex<Nb>,GeneralMidasVector>(suite);
      tdvcc::detail::AddParamsTdvccImpl<std::complex<Nb>,GeneralDataCont>(suite);
      tdvcc::detail::AddParamsTdvccImpl<std::complex<Nb>,GeneralTensorDataCont>(suite);

      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTdvcc,TrfTdvccMatRep,DerivTdvcc,false>(suite, "Tdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTdvcc,TrfTdvccMatRep,DerivTdvcc,true>(suite, "Tdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralMidasVector,ParamsTdvcc,TrfTdvccMatRep,DerivTdvcc,true>(suite, "Tdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTdvcc,TrfTdvcc2,DerivTdvcc,false>(suite, "Tdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTdvcc,TrfTdvcc2,DerivTdvcc,true>(suite, "Tdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralDataCont,ParamsTdvcc,TrfTdvcc2,DerivTdvcc,true>(suite, "Tdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralTensorDataCont,ParamsTdvcc,TrfGeneralTimTdvcc,DerivTdvcc,false>(suite, "TimTdvcc, VCC");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralTensorDataCont,ParamsTdvcc,TrfGeneralTimTdvcc,DerivTdvcc,true>(suite, "TimTdvcc, VCC");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralTensorDataCont,ParamsTdvcc,TrfGeneralTimTdvcc,DerivTdvcc,true>(suite, "TimTdvcc, VCC");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTdvci,TrfTdvciMatRep,DerivTdvci,false>(suite, "Tdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTdvci,TrfTdvciMatRep,DerivTdvci,true>(suite, "Tdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralMidasVector,ParamsTdvci,TrfTdvciMatRep,DerivTdvci,true>(suite, "Tdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTdvci,TrfTdvci2,DerivTdvci,false>(suite, "Tdvci, VCI[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTdvci,TrfTdvci2,DerivTdvci,true>(suite, "Tdvci, VCI[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralDataCont,ParamsTdvci,TrfTdvci2,DerivTdvci,true>(suite, "Tdvci, VCI[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralTensorDataCont,ParamsTdvci,TrfGeneralTimTdvci,DerivTdvci,false>(suite, "TimTdvci, VCI");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralTensorDataCont,ParamsTdvci,TrfGeneralTimTdvci,DerivTdvci,true>(suite, "TimTdvci, VCI");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralTensorDataCont,ParamsTdvci,TrfGeneralTimTdvci,DerivTdvci,true>(suite, "TimTdvci, VCI");

      tdvcc::detail::AddTdvccTestsImpl<std::complex<Nb>,false>(suite);
      tdvcc::detail::AddTdvccTestsImpl<std::complex<Nb>,true>(suite);
      tdvcc::detail::AddTdvccTestsImpl<Nb,true>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
