/**
 *******************************************************************************
 * 
 * @file    GenerateTensorElemNamesTest.cc
 * @date    23-10-2019
 * @author  Bo Thomsen (drbothomsen@gmail.com) //CHANGE!
 *
 * @brief
 *    A test for the GenerateTensorElemName utility function
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/GenerateTensorElemNamesTest.h"

namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    The driver for the Generate Tensor Element Name unit tests.
    ***************************************************************************/
   void GenerateTensorNamesTest() 
   {
      // Create test_suite object.
      cutee::suite suite("GenerateTensorNamesTest test");

      // Add all the tests to the suite.
      using namespace generate_tensor_element_names;
      suite.add_test<TestGeneratedTensorElementNames>                          ("Brief description, for output file.");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
