#ifndef CALCCODETEST_H_INCLUDED
#define CALCCODETEST_H_INCLUDED

//We are using a set here to test the ordering of CalcCodes
//In production code this is done in a map, but they should be eqvivalent.
//If not we are facing a more fundamental problem not related to the CalcCode class
#include <set>
#include <sstream>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "pes/CalcCode.h"
#include "pes/SimpleRational.h"
#include "test/CuteeInterface.h"

namespace midas::test
{

using std::set;
using std::stringstream;
using std::string;

/**
* We first test if we are able to convert the calc code to the correct string
**/
struct TestCalcCodeStringConv 
   : public cutee::test
{
   void run() 
   {
      vector<In> new_modes {0,2,3,4};
      vector<SimpleRational> gridpoints { {1,4}, {3,16}, {4,16}, {5,32} };
      CalcCode calc4(new_modes, gridpoints);
      stringstream gen_calc_code;
      gen_calc_code << calc4;
      string real_calc_code = "*#1_1/4#3_3/16#4_1/4#5_5/32*"; // mode numbers are increased by one in calc-codes... this will hopefully be fixed in the future to not be confusing :O
      UNIT_ASSERT(gen_calc_code.str()==real_calc_code,"Calc code converted to string : "+gen_calc_code.str()+" should be "+real_calc_code);   
   }
};

/**
* Second test is to ensure that the CalculationLists will have the right ordering of single point calculations
**/
struct TestCalcCodeSetOrdering 
   : public cutee::test
{
   void run() 
   {
      set<CalcCode> aCalcCodeSet; //< This should have same order as we will force on the vector
      vector<CalcCode> aCalcCodeVect; //< Will contain a sorted list of the CalcCodes
      vector<In> new_modes;
      new_modes.push_back(0);
      new_modes.push_back(2);
      new_modes.push_back(3);
      new_modes.push_back(4);
      vector<SimpleRational> gridpoints;
      gridpoints.emplace_back(1,4);
      gridpoints.emplace_back(3,16);
      gridpoints.emplace_back(4,16);
      gridpoints.emplace_back(5,32);
      CalcCode calc4(new_modes, gridpoints);
      aCalcCodeSet.insert(calc4);
      new_modes.clear();
      gridpoints.clear();
      new_modes.push_back(1);
      new_modes.push_back(2);
      new_modes.push_back(3);
      new_modes.push_back(4);
      gridpoints.emplace_back(1,4);
      gridpoints.emplace_back(3,16);
      gridpoints.emplace_back(4,16);
      gridpoints.emplace_back(5,32);
      CalcCode calc5(new_modes, gridpoints);
      aCalcCodeSet.insert(calc5);
      new_modes.clear();
      gridpoints.clear();
      new_modes.push_back(1);
      new_modes.push_back(3);
      new_modes.push_back(4);
      gridpoints.emplace_back(3,16);
      gridpoints.emplace_back(4,16);
      gridpoints.emplace_back(5,32);
      CalcCode calc3(new_modes, gridpoints);
      aCalcCodeSet.insert(calc3);
      new_modes.clear();
      gridpoints.clear();
      new_modes.push_back(1);
      gridpoints.emplace_back(5,32);
      CalcCode calc2(new_modes, gridpoints);
      aCalcCodeSet.insert(calc2);
      new_modes.clear();
      gridpoints.clear();
      new_modes.push_back(1);
      gridpoints.emplace_back(-5,32);
      CalcCode calc1(new_modes, gridpoints);
      aCalcCodeSet.insert(calc1);
      new_modes.clear();
      gridpoints.clear();
      CalcCode calc0(new_modes, gridpoints);
      aCalcCodeSet.insert(calc0);
      
      UNIT_ASSERT(aCalcCodeSet.size() == 6, "Number of CalcCodes in set is wrong");
      
      aCalcCodeVect.push_back(calc0);
      aCalcCodeVect.push_back(calc1);
      aCalcCodeVect.push_back(calc2);
      aCalcCodeVect.push_back(calc3);
      aCalcCodeVect.push_back(calc4);
      aCalcCodeVect.push_back(calc5);
      set<CalcCode>::iterator it = aCalcCodeSet.begin();
      for(In i = I_0; i < aCalcCodeVect.size(); ++i)
      {
         UNIT_ASSERT(aCalcCodeVect[i] == *it, "CalcCodes ordered wrong");
         ++it;
      }
   }
};

} /* namespace midas::test */

#endif /* CALCCODETEST_H_INCLUDED */
