/**
 *******************************************************************************
 * 
 * @file    MvccSolverTest.cc
 * @date    28-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/MvccSolverTest.h"
#include "util/type_traits/TypeName.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"

namespace midas::test
{
   namespace mvccsolver::detail
   {
      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests using the template parameters that this function is
       *    called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddTests
         (  cutee::suite& arSuite
         )
      {
         //std::string s = "("+midas::type_traits::TypeName<T>()+") ";
         auto info = [](const std::string& s, bool aCrop, bool aDiagPrecon) -> std::string
         {
            std::stringstream ss;
            ss << std::boolalpha
               << s << "("+midas::type_traits::TypeName<T>()+")"
               << ", algorithm = " << (aCrop? "CROP": "DIIS")
               << ", diag.precon. = " << aDiagPrecon
               ;
            return ss.str();
         };

         // NKM, Nov 2020: only test with DIIS for now
         for(const auto& crop: {false})
         {
            // MBH, Jan 2020: converges horribly slowly without precon!
            for(const auto& precon: {true})
            {
               arSuite.add_test<MvccSolverTest<T,midas::tdvcc::TrfTdmvcc2>>(info("MVCC[2]/H2",crop,precon), crop, precon);
               //arSuite.add_test<MvccSolverTest<T,midas::tdvcc::TrfTdmvccMatRep>>(info("MVCC[2] MATREP",crop,precon), crop, precon);
            }
         }
      }

   } /* namespace mvccsolver::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void MvccSolverTest()
   {
      // Create test_suite object.
      cutee::suite suite("MvccSolver test");

      // Add all the tests to the suite.
      using mvccsolver::detail::AddTests;
      AddTests<Nb>(suite);
      //AddTests<std::complex<Nb>>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */

