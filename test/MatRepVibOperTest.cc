/**
 *******************************************************************************
 * 
 * @file    MatRepVibOperTest.cc
 * @date    19-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "util/type_traits/Complex.h"
#include "test/MatRepVibOperTest.h"
#include "util/type_traits/TypeName.h"

namespace midas::test
{
   namespace matrep::detail
   {
      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepUtils.h` to this test suite.
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      void AddMatRepUtilsOrganizeMcrSpaceVecTestImpl
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::utils;
         using midas::type_traits::TypeName;
         std::string s = " ("+TypeName<T>()+","+TypeName<CONT_T>()+")";
         arSuite.add_test<OrganizeMcrSpaceVecTest<T,CONT_T>>("OrganizeMcrSpaceVecTest"+s);
      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepUtils.h` to this test suite.
       ************************************************************************/
      void AddMatRepUtilsTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::utils;
         std::string s = "MatRepUtils; ";
         arSuite.add_test<SetFromVecTest>(s+"SetFromVec");
         arSuite.add_test<CumulativeProductTest>(s+"CumulativeProduct");
         arSuite.add_test<MultiIndexTest>(s+"MultiIndex");
         arSuite.add_test<AbsIndexTest>(s+"AbsIndex");
         arSuite.add_test<IncrMultiIndexTest>(s+"IncrMultiIndex");
         arSuite.add_test<IncrMultiIndexWithAbsIncrTest>(s+"IncrMultiIndexWithAbsIncr");
         arSuite.add_test<ProductTest>(s+"Product");
         arSuite.add_test<ComplementaryModesTest>(s+"ComplementaryModes");
         arSuite.add_test<SubsetDimsTest>(s+"SubsetDims");
         arSuite.add_test<ShiftValsTest>(s+"ShiftVals");
         arSuite.add_test<ModeCombiExciIndicesTest>(s+"ModeCombiExciIndices");
         arSuite.add_test<McrExciIndicesTest>(s+"McrExciIndices");
         arSuite.add_test<ExtendToSecondarySpaceTestA<Nb>>(s+"ExtendToSecondarySpace A, Nb");
         arSuite.add_test<ExtendToSecondarySpaceTestA<std::complex<Nb>>>(s+"ExtendToSecondarySpace A, std::complex<Nb>");
         arSuite.add_test<ExtendToSecondarySpaceTestB<Nb>>(s+"ExtendToSecondarySpace B, Nb");
         arSuite.add_test<ExtendToSecondarySpaceTestB<std::complex<Nb>>>(s+"ExtendToSecondarySpace B, std::complex<Nb>");
         AddMatRepUtilsOrganizeMcrSpaceVecTestImpl<Nb,GeneralMidasVector>(arSuite);
         AddMatRepUtilsOrganizeMcrSpaceVecTestImpl<Nb,GeneralDataCont>(arSuite);
         AddMatRepUtilsOrganizeMcrSpaceVecTestImpl<std::complex<Nb>,GeneralMidasVector>(arSuite);
         AddMatRepUtilsOrganizeMcrSpaceVecTestImpl<std::complex<Nb>,GeneralDataCont>(arSuite);

         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {}, {}, {}", std::vector<Uin>{}, std::set<Uin>{}, std::vector<std::set<Uin>>{}, std::vector<std::vector<Uin>>{});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {2}, {0}, {{0}}", std::vector<Uin>{2}, std::set<Uin>{0}, std::vector<std::set<Uin>>{{0}}, std::vector<std::vector<Uin>>{{2}});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {4,3}, {2,5}, {{2,5}}", std::vector<Uin>{4,3}, std::set<Uin>{2,5}, std::vector<std::set<Uin>>{{2,5}}, std::vector<std::vector<Uin>>{{4,3}});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {4,3}, {2,5}, {{2},{5}}", std::vector<Uin>{4,3}, std::set<Uin>{2,5}, std::vector<std::set<Uin>>{{2},{5}}, std::vector<std::vector<Uin>>{{4},{3}});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {4,3}, {2,5}, {{5},{2}}", std::vector<Uin>{4,3}, std::set<Uin>{2,5}, std::vector<std::set<Uin>>{{5},{2}}, std::vector<std::vector<Uin>>{{3},{4}});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {5,2,3}, {1,4,7}, {{4},{1,7}}", std::vector<Uin>{5,2,3}, std::set<Uin>{1,4,7}, std::vector<std::set<Uin>>{{4},{1,7}}, std::vector<std::vector<Uin>>{{2},{5,3}});
         arSuite.add_test<SplitMultiIndexTest>(s+"SplitMultiIndexTest; {1,2,3,5,8,13}, {2,3,5,7,11,13}, {{3,5},{2,11,13},{7}}", std::vector<Uin>{1,2,3,5,8,13}, std::set<Uin>{2,3,5,7,11,13}, std::vector<std::set<Uin>>{{3,5},{2,11,13},{7}}, std::vector<std::vector<Uin>>{{2,3},{1,8,13},{5}});
      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepVibOper.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddMatRepVibOperTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::viboper;
         std::string s = "("+midas::type_traits::TypeName<T>()+") MatRepVibOper; ";
         // Tests for _both_ real and complex numbers.
         arSuite.add_test<Constructor<T>>(s+"Constructor");
         arSuite.add_test<DirProdOneModeOpersAllModes<T>>(s+"DirProdOneModeOpersAllModes");
         arSuite.add_test<DirProdOneModeOpersSomeModes<T>>(s+"DirProdOneModeOpersSomeModes");
         arSuite.add_test<FullOperatorFromOpDefA<T>>(s+"FullOperatorFromOpDefA (std)");
         arSuite.add_test<FullOperatorFromOpDefB<T>>(s+"FullOperatorFromOpDefB (non-std)");
         arSuite.add_test<FullOperatorFromOpDefBScrambled<T>>(s+"FullOperatorFromOpDefB (non-std - scrambled)");
         arSuite.add_test<FullOperatorBlock<T>>(s+"FullOperatorBlock");
         arSuite.add_test<FullActiveTermsOperatorFromOpDefA<T>>(s+"FullActiveTermsOperatorFromOpDefA (std)");
         arSuite.add_test<ShiftOper<T>>(s+"ShiftOper");
         arSuite.add_test<ClusterOper0<T>>(s+"ClusterOper0");
         arSuite.add_test<ClusterOper1<T>>(s+"ClusterOper1");
         arSuite.add_test<ClusterOper2<T>>(s+"ClusterOper2");
         arSuite.add_test<ClusterOper3<T>>(s+"ClusterOper3");
         arSuite.add_test<ClusterOperMcr<T>>(s+"ClusterOperMcr");
         arSuite.add_test<KappaOper1<T>>(s+"KappaOper1");
         arSuite.add_test<KappaOper2<T>>(s+"KappaOper2");
         arSuite.add_test<KappaOperAllModes<T>>(s+"KappaOperAllModes");
         arSuite.add_test<ExpClusterOper<T>>(s+"ExpClusterOper");
         arSuite.add_test<ExtractExciParams<T>>(s+"ExtractExciParams");
         arSuite.add_test<ExtractToMcrSpace<T>>(s+"ExtractToMcrSpace");
         arSuite.add_test<McrExciIndicesAndExtractToMcrSpace<T>>(s+"McrExciIndicesAndExtractToMcrSpace");

         // Tests for complex/real numbers _only_.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
         }
         else
         {
         }
      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepVibOper.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddSparseClusterOperTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::sparseclusteroper;
         std::string s = "SparseClusterOper; ";
         std::string sT = "("+midas::type_traits::TypeName<T>()+") " + s;

         // Tests for complex/real numbers _only_.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
         }
         else
         {
            arSuite.add_test<Constructor>(s+"Constructor");
            arSuite.add_test<IndicesExc>(s+"IndicesExc");
            arSuite.add_test<IndicesDeExc>(s+"IndicesDeExc");
         }

         // Tests for _both_ real and complex numbers.
         arSuite.add_test<Transform<T,false,false>>(sT+"Transform, !DEEXC, !CONJ");
         arSuite.add_test<Transform<T,false,true>>(sT+"Transform, !DEEXC, CONJ");
         arSuite.add_test<Transform<T,true,false>>(sT+"Transform, DEEXC, !CONJ");
         arSuite.add_test<Transform<T,true,true>>(sT+"Transform, DEEXC, CONJ");
         arSuite.add_test<RefToFullSpace<T,false>>(sT+"RefToFullSpace, !CONJ");
         arSuite.add_test<RefToFullSpace<T,true>>(sT+"RefToFullSpace, CONJ");

      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepVibOper.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddOperMatTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::opermat;

         std::string s = "("+midas::type_traits::TypeName<T>()+") OperMat; ";
         arSuite.add_test<MpiSyncTestOpDef<T>>(s+"MpiSyncTestOpDef");
         arSuite.add_test<MpiSyncTestModalIntegrals<T>>(s+"MpiSyncTestModalIntegrals");

         for(const auto& symtype: std::vector<char>{'h', 'a', 'g'})
         {
            std::string s = "("+midas::type_traits::TypeName<T>()+", '"+symtype+"') OperMat; ";

            // Tests for complex/real numbers _only_.
            if constexpr(midas::type_traits::IsComplexV<T>)
            {
            }
            else
            {
            }

            // Tests for _both_ real and complex numbers.
            arSuite.add_test<Constructor<T>>(s+"Constructor", symtype);
            arSuite.add_test<MatVecMultTest<T,false>>(s+"MatVecMult, TRANSPOSE=false", symtype);
            arSuite.add_test<MatVecMultTest<T,true>>(s+"MatVecMult, TRANSPOSE=true", symtype);
            arSuite.add_test<OperatorAsterisk<T>>(s+"operator*", symtype);
            for(const auto& s_add: std::vector<char>{'h', 'a', 'g'})
            {
               arSuite.add_test<AddSumOneModeOpersTest<T>>(s+"AddSumOneModeOpers '"+s_add+"'",s_add,symtype);
            }
            arSuite.add_test<GetRowTest<T>>(s+"GetRow", symtype);
            arSuite.add_test<DumpAndReadFromDisk<T>>(s+"DumpAndReadFromDisk", symtype);
         }
      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `MatRepTransformers.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddMatRepTransformersTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::matrep::trf;
         std::string s = "("+midas::type_traits::TypeName<T>()+") MatRepTransformers; ";
         // Tests for _both_ real and complex numbers.

         // Tests that don't use operator matrix.
         arSuite.add_test<ShiftOperBraket<T,false>>(s+"ShiftOperBraket (!CONJ_BRA)");
         arSuite.add_test<ShiftOperBraket<T,true>>(s+"ShiftOperBraket (CONJ_BRA)");
         arSuite.add_test<TrfExpClusterOper<T,false,false>>(s+"TrfExpClusterOper (!DEEXC, !CONJ)");
         arSuite.add_test<TrfExpClusterOper<T,false,true>>(s+"TrfExpClusterOper (!DEEXC, CONJ)");
         arSuite.add_test<TrfExpClusterOper<T,true,false>>(s+"TrfExpClusterOper (DEEXC, !CONJ)");
         arSuite.add_test<TrfExpClusterOper<T,true,true>>(s+"TrfExpClusterOper (DEEXC, CONJ)");
         arSuite.add_test<TensorDirectProductTest<T>>(s+"TensorDirectProduct");
         arSuite.add_test<CRefTest<T>>(s+"CRefTest");
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3} 0->0", std::vector<Uin>{2,3}, 0, 0);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3} 0->1", std::vector<Uin>{2,3}, 0, 1);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3} 1->1", std::vector<Uin>{2,3}, 1, 1);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3} 1->2", std::vector<Uin>{2,3}, 1, 2);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {4,2,3} 2->2", std::vector<Uin>{4,2,3}, 2, 2);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {4,2,3} 3->3", std::vector<Uin>{4,2,3}, 3, 3);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {4,2,3} 2->3", std::vector<Uin>{4,2,3}, 2, 3);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3,2,3} 2->3", std::vector<Uin>{2,3,2,3}, 2, 3);
         arSuite.add_test<ExpTRefTest<T>>(s+"ExpTRefTest, {2,3,2,3} 2->4", std::vector<Uin>{2,3,2,3}, 2, 4);
         arSuite.add_test<Ref1pLExpmTTest<T>>(s+"Ref1pLExpmTTest");
         arSuite.add_test<RefExpSExpmTTest<T>>(s+"RefExpSExpmTTest");
         arSuite.add_test<TrfExtVccLeftExpmSTest<T>>(s+"TrfExtVccLeftExpmSTest");
         arSuite.add_test<TrfExtVccRightExpmSTest<T>>(s+"TrfExtVccRightExpmSTest");
         arSuite.add_test<ExplVccJacSolveEtaPlusLATest<T>>(s+"ExplVccJacSolveEtaPlusLATest");
         for(const auto& bal: std::vector<bool>{false,true})
         {
            for(const auto& incl: std::vector<bool>{false,true})
            {
               const std::string s_info = std::string(", ")+(bal? "": "!")+"balance, "+(incl? "": "!")+"incl zeroes";
               arSuite.add_test<GetRandomModalTransMatsTest<T>>(s+"GetRandomModalTransMatsTest"+s_info, bal, incl);
            }
         }
         arSuite.add_test<VccOneModeDensityMatricesTest<T>>(s+"VccOneModeDensityMatrices");

         {
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = empty", ModeCombiOpRange());
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = (0,3)", ModeCombiOpRange(0,3));
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = (1,3)", ModeCombiOpRange(1,3));
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = (2,3)", ModeCombiOpRange(2,3));
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = (3,3)", ModeCombiOpRange(3,3));
            ModeCombiOpRange mcr(3,3);
            mcr.Erase(std::vector<ModeCombi>
                  {  ModeCombi(std::set<In>{},-1)
                  ,  ModeCombi(std::set<In>{1},-1)
                  ,  ModeCombi(std::set<In>{0,1},-1)
                  });
            arSuite.add_test<InvExpTRefTest<T>>(s+"InvExpTRefTest, MCR = {0},{2},{0,2},{1,2},{0,1,2}", mcr);
         }

         // For transformers using operator matrix we test the cases Hermitian,
         // anti-Hermitian and general matrix.
         for(const auto& symtype: std::vector<char>{'h', 'a', 'g'})
         {
            s = "("+midas::type_traits::TypeName<T>()+", '"+symtype+"') MatRepTransformers; ";
            arSuite.add_test<TrfVccErrVecTest<T>>(s+"TrfVccErrVecTest", symtype);
            arSuite.add_test<TrfVciTest<T>>(s+"TrfVciTest", symtype);
            arSuite.add_test<TrfVccEtaVecTest<T>>(s+"TrfVccEtaVecTest", symtype);
            arSuite.add_test<TrfVccRJacTest<T>>(s+"TrfVccRJacTest", symtype);
            arSuite.add_test<TrfVccLJacTest<T>>(s+"TrfVccLJacTest", symtype);
            arSuite.add_test<ExplVccJacTest<T>>(s+"ExplVccJacTest", symtype);
            arSuite.add_test<TrfExtVccHamDerExtAmpTest<T>>(s+"TrfExtVccHamDerExtAmpTest", symtype);
            arSuite.add_test<TrfExtVccHamDerClustAmpTest<T>>(s+"TrfExtVccHamDerClustAmpTest", symtype);
            for(const auto& balance: std::vector<bool>{false,true})
            {
               const std::string s_bal = balance? "balance U/W": "!balance U/W";
               arSuite.add_test<TdmvccHalfTransMeanFieldMatricesTest<T>>(s+"TdmvccHalfTransMeanFieldMatricesTest, "+s_bal, balance, symtype);
               arSuite.add_test<TdmvccUDotWDotBaseExpressionTest<T>>(s+"TdmvccUDotWDotBaseExpressionTest, "+s_bal, balance, symtype);
            }
         }

         // Tests for complex/real numbers _only_.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
         }
         else
         {
         }
      }

   } /* namespace matrep::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void MatRepVibOperTest()
   {
      // Create test_suite object.
      cutee::suite suite("MatRepVibOper test");

      // Add all the tests to the suite.
      using matrep::detail::AddMatRepUtilsTests;
      AddMatRepUtilsTests(suite);

      using matrep::detail::AddMatRepVibOperTests;
      AddMatRepVibOperTests<Nb>(suite);
      AddMatRepVibOperTests<std::complex<Nb>>(suite);

      using matrep::detail::AddSparseClusterOperTests;
      AddSparseClusterOperTests<Nb>(suite);
      AddSparseClusterOperTests<std::complex<Nb>>(suite);

      using matrep::detail::AddOperMatTests;
      AddOperMatTests<Nb>(suite);
      AddOperMatTests<std::complex<Nb>>(suite);

      using matrep::detail::AddMatRepTransformersTests;
      AddMatRepTransformersTests<Nb>(suite);
      AddMatRepTransformersTests<std::complex<Nb>>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
