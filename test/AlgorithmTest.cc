#include "AlgorithmTest.h"

namespace midas::test
{

/**
 * Test functions from algo folder
 **/
void AlgorithmTest()
{
   cutee::suite suite("Algorithm test");

   // Utility test
   suite.add_test<MoveToCentroidTest >("Move to centroid test");

   // Algorithm Test
   suite.add_test<KabschTest >("Kabsch test");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
