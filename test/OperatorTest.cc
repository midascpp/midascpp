/**
 *******************************************************************************
 * 
 * @file    OperatorTest.cc
 * @date    26/03-2020
 * @author  Ian Heide Godtliebsen (ian@chem.au.dk)
 *
 * @brief
 *    Testing of MidasCpp operators
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/OperatorTest.h" 

namespace midas::test
{
   namespace operator_test::detail 
   {
   } /* namespace operator_test::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void OperatorTest() 
   {
      // Create test_suite object.
      cutee::suite suite("OperatorTest");

      // Add all the tests to the suite.
      using namespace operator_test; 
      suite.add_test<PropertyTypeTest> ("Property Type Test");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
