#include "MoleculeTest.h"

namespace midas::test
{

void MoleculeTest()
{
   cutee::suite suite("Molecule test");

   // Symmetry test
   suite.add_test<WaterSymC2VTest >("Water C2V test");
   suite.add_test<C2H2F2SymC2HTest>("C2H2F2 C2H test");

   // Molecular input tests
   suite.add_test<MoleculeFieldConstructionTest>("Test construction of MoleculeFields struct.");
   suite.add_test<MoleculeFieldInsertionTest   >("Test insertion into MoleculeFields struct.");
   
   // Utility tests
   suite.add_test<DiagonalRotateToInertiaFrameTest>("Test calculation of inertia tensor.");
   suite.add_test<RotateToInertiaFrameTest>        ("Test calculation of inertia tensor.");
   suite.add_test<ShiftToCenterOfMassTest>         ("Test center of mass shifting.");

   suite.add_test<NormalizeNormalCoordinatesTest>     ("Test normalizing of normal coordinates.");
   suite.add_test<OrthoNormalizeNormalCoordinatesTest>("Test ortho-normalizing of normal coordinates.");
   suite.add_test<CreateTranslationalCoordinatesTest> ("Test creation of translational coordinates.");
   //suite.add_test<CreateRotationalCoordinatesTest>    ("Test creation of rotational coordinates.");
   
   suite.add_test<CreateRotationXTest>     ("Test creation of rotation matrix around x-axis.");
   suite.add_test<CreateRotationYTest>     ("Test creation of rotation matrix around y-axis.");
   suite.add_test<CreateRotationZTest>     ("Test creation of rotation matrix around z-axis.");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
