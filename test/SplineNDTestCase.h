/**
************************************************************************
* 
* @file                SplineNDTest.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli
*
* Short Description:   Test SplineND class 
* 
* Last modified: Thu Dec 03, 2009  02:01PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SPLINENDTESTCASE_H
#define SPLINENDTESTCASE_H
// Standard Headers.
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// My Headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Test.h"
#include "test/CuteeInterface.h"
#include "input/Input.h"
#include "pes/splines/SplineND.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"

namespace midas::test
{

/**
* Controls the testing of stuff 
* */
struct SplineNDTestCase : public cutee::test{
   void fnd (const MidasVector&  arXog1, const MidasVector& arXog2, const MidasVector& arXog3, const MidasVector& arXog4, const MidasVector& arXog5, MidasVector& arYog) 
   {
      In nog1 = arXog1.Size();
      In nog2 = arXog2.Size();
      In nog3 = arXog3.Size();
      In nog4 = arXog4.Size();
      In nog5 = arXog4.Size();
      MidasVector xog1_tmp = arXog1;
      MidasVector xog2_tmp = arXog2;
      MidasVector xog3_tmp = arXog3;
      MidasVector xog4_tmp = arXog4;
      MidasVector xog5_tmp = arXog5;
      xog1_tmp.Pow(I_3);
      xog2_tmp.Pow(I_3);
      xog3_tmp.Pow(I_3);
      xog4_tmp.Pow(I_3);
      xog5_tmp.Pow(I_3);
      In nog1234 = nog1 * nog2 * nog3 * nog4 * nog5;
      if ( nog1234 != arYog.Size())
      {
        arYog.SetNewSize(nog1234,false);
      }
      In iog1234 = I_0;
      for (In iog1 =I_0; iog1 < nog1; iog1++)
      {
         for (In iog2 =I_0; iog2 < nog2; iog2++)
         { 
            for (In iog3 =I_0; iog3 < nog3; iog3++)
            {
               for (In iog4 =I_0; iog4 < nog4; iog4++)
               {
                  for (In iog5 =I_0; iog5 < nog5; iog5++)
                  {
                     arYog[iog1234] = xog1_tmp[iog1] + xog2_tmp[iog2] + xog3_tmp[iog3] + xog4_tmp[iog4] + xog5_tmp[iog5];
                     iog1234+=I_1;
                  }
               }
            }
         }
      }
   }
   
   void run() 
   {
      //Mout << "****** Performing test of SplineND class " << endl << endl;
   
      // *** then defines the function to be interpolated (a normal cubic in three dimensions)
   
      Nb* Xig1_tmp;
      Nb* Xig2_tmp;
      Nb* Xig3_tmp;
      Nb* Xig4_tmp;
      Nb* Xig5_tmp;
      Nb low_boundX1_ig   = -C_1;
      Nb upper_boundX1_ig = C_1;
      Nb low_boundX2_ig   = -C_2;
      Nb upper_boundX2_ig = C_2;
      Nb low_boundX3_ig   = -C_3;
      Nb upper_boundX3_ig = C_3;
      Nb low_boundX4_ig   = -C_4;
      Nb upper_boundX4_ig = C_4;
      Nb low_boundX5_ig   = -C_5;
      Nb upper_boundX5_ig = C_5;
      In num_of_stepX1_ig = I_5;
      In num_of_stepX2_ig = I_5;
      In num_of_stepX3_ig = I_5;
      In num_of_stepX4_ig = I_5;
      In num_of_stepX5_ig = I_5;
      Nb hX1_ig;
      Nb hX2_ig;
      Nb hX3_ig;
      Nb hX4_ig;
      Nb hX5_ig;
   
   // *** input lower and upper bounds and the # of steps
    
   //   Minp << Nb low_bound << Nb upper_bound << In num_of_steps << endl;
      //Mout << "Lower bound in X1 direction in SplineND interpolation : " << low_boundX1_ig << endl << endl;
      //Mout << "Upper bound in X1 direction in SplineND interpolation : " << upper_boundX1_ig << endl << endl;
      //Mout << "# of steps in X1  direction in SplineND interpolation : " << num_of_stepX1_ig << endl << endl;
      //Mout << "Lower bound in X2 direction in SplineND interpolation : " << low_boundX2_ig << endl << endl;
      //Mout << "Upper bound in X2 direction in SplineND interpolation : " << upper_boundX2_ig << endl << endl;
      //Mout << "# of steps in  X2 direction in SplineND interpolation : " << num_of_stepX2_ig << endl << endl;
      //Mout << "Lower bound in X3 direction in SplineND interpolation : " << low_boundX3_ig << endl << endl;
      //Mout << "Upper bound in X3 direction in SplineND interpolation : " << upper_boundX3_ig << endl << endl;
      //Mout << "# of steps in  X3 direction in SplineND interpolation : " << num_of_stepX3_ig << endl << endl;
      //Mout << "Lower bound in X4 direction in SplineND interpolation : " << low_boundX4_ig << endl << endl;
      //Mout << "Upper bound in X4 direction in SplineND interpolation : " << upper_boundX4_ig << endl << endl;
      //Mout << "# of steps in  X4 direction in SplineND interpolation : " << num_of_stepX4_ig << endl << endl;
      //Mout << "Lower bound in X5 direction in SplineND interpolation : " << low_boundX5_ig << endl << endl;
      //Mout << "Upper bound in X5 direction in SplineND interpolation : " << upper_boundX5_ig << endl << endl;
      //Mout << "# of steps in  X5 direction in SplineND interpolation : " << num_of_stepX5_ig << endl << endl;
               
   
      hX1_ig = (upper_boundX1_ig - low_boundX1_ig)/num_of_stepX1_ig;
      hX2_ig = (upper_boundX2_ig - low_boundX2_ig)/num_of_stepX2_ig;
      hX3_ig = (upper_boundX3_ig - low_boundX3_ig)/num_of_stepX3_ig;
      hX4_ig = (upper_boundX4_ig - low_boundX4_ig)/num_of_stepX4_ig;
      hX5_ig = (upper_boundX5_ig - low_boundX5_ig)/num_of_stepX5_ig;
   
      In nig1 = num_of_stepX1_ig + I_1;
      In nig2 = num_of_stepX2_ig + I_1;
      In nig3 = num_of_stepX3_ig + I_1;
      In nig4 = num_of_stepX4_ig + I_1;
      In nig5 = num_of_stepX5_ig + I_1;
   
      Xig1_tmp = new Nb [nig1];
      Xig2_tmp = new Nb [nig2];
      Xig3_tmp = new Nb [nig3];
      Xig4_tmp = new Nb [nig4];
      Xig5_tmp = new Nb [nig5];
   
      for (In i = I_0; i < nig1; i++)
      { 
         Xig1_tmp[i] = low_boundX1_ig + Nb(i)*hX1_ig;
      }
      for (In i = I_0; i < nig2; i++)
      {
         Xig2_tmp[i] = low_boundX2_ig + Nb(i)*hX2_ig;
      }
      for (In i = I_0; i < nig3; i++)
      {
         Xig3_tmp[i] = low_boundX3_ig + Nb(i)*hX3_ig;
      }
      for (In i = I_0; i < nig4; i++)
      {
         Xig4_tmp[i] = low_boundX4_ig + Nb(i)*hX4_ig;
      }
      for (In i = I_0; i < nig5; i++)
      {
         Xig5_tmp[i] = low_boundX5_ig + Nb(i)*hX5_ig;
      }
      
      vector<MidasVector> Xig(I_5);
      vector<MidasVector> Xog(I_5);
      MidasVector Xig1(nig1, Xig1_tmp);
      MidasVector Xig2(nig2, Xig2_tmp);
      MidasVector Xig3(nig3, Xig3_tmp);
      MidasVector Xig4(nig4, Xig4_tmp);
      MidasVector Xig5(nig5, Xig5_tmp);
   
      delete[] Xig1_tmp;
      delete[] Xig2_tmp;
      delete[] Xig3_tmp;
      delete[] Xig4_tmp;
      delete[] Xig5_tmp;
   
      Xig[I_0].SetNewSize(nig1,false);
      Xig[I_1].SetNewSize(nig2,false);
      Xig[I_2].SetNewSize(nig3,false);
      Xig[I_3].SetNewSize(nig4,false);
      Xig[I_4].SetNewSize(nig5,false);
      
      Xig[I_0] = Xig1;
      Xig[I_1] = Xig2;
      Xig[I_2] = Xig3;
      Xig[I_3] = Xig4;
      Xig[I_4] = Xig5;
   
      In nig1234 = nig1 * nig2 * nig3 * nig4 * nig5;
      MidasVector Yig(nig1234, C_0);
      MidasVector YigDerivs(nig1234, C_0);
   
      fnd(Xig[I_0], Xig[I_1], Xig[I_2], Xig[I_3], Xig[I_4], Yig);
   
   // *** then construct the 3D cubic natural spline interpolant
   // *** construct the array with the first derivatives
      MidasVector FirstDerivLeft(I_5);
      FirstDerivLeft[I_0]=C_3;
      FirstDerivLeft[I_1]=12.;
      FirstDerivLeft[I_2]=27.;
      FirstDerivLeft[I_3]=48.;
      FirstDerivLeft[I_4]=75.;
      MidasVector FirstDerivRight(I_5);
      FirstDerivRight[I_0]=C_3;
      FirstDerivRight[I_1]=12.;
      FirstDerivRight[I_2]=27.;
      FirstDerivRight[I_3]=48.;
      FirstDerivRight[I_4]=75.;
   
   
      SplineND CubicApprox(SPLINEINTTYPE::GENERAL, Xig, Yig, I_1, FirstDerivLeft[I_4], FirstDerivRight[I_4]);
      CubicApprox.GetYigDerivs(YigDerivs);
   
   // and evaluate at few selected points
   
      Nb* x1_int;
      Nb* x2_int;
      Nb* x3_int;
      Nb* x4_int;
      Nb* x5_int;
      Nb low_boundX1_og   = -C_1;
      Nb upper_boundX1_og = C_1;
      Nb low_boundX2_og   = -C_2;
      Nb upper_boundX2_og = C_2;
      Nb low_boundX3_og   = -C_3;
      Nb upper_boundX3_og = C_3;
      Nb low_boundX4_og   = -C_4;
      Nb upper_boundX4_og = C_4;
      Nb low_boundX5_og   = -C_5;
      Nb upper_boundX5_og = C_5;
      In num_of_stepX1_og = I_20;
      In num_of_stepX2_og = I_20;
      In num_of_stepX3_og = I_20;
      In num_of_stepX4_og = I_20;
      In num_of_stepX5_og = I_20;
   
      //Mout << "Lower bound in X1 direction in finer grid for SplineND interpolation : " << low_boundX1_og << endl << endl;
      //Mout << "Upper bound in X1 direction in finer grid for SplineND interpolation : " << upper_boundX1_og << endl << endl;
      //Mout << "# of steps in X1  direction in finer grid for SplineND interpolation : " << num_of_stepX1_og << endl << endl;
      //Mout << "Lower bound in X2 direction in finer grid for SplineND interpolation : " << low_boundX2_og << endl << endl;
      //Mout << "Upper bound in X2 direction in finer grid for SplineND interpolation : " << upper_boundX2_og << endl << endl;
      //Mout << "# of steps in  X2 direction in finer grid for SplineND interpolation : " << num_of_stepX2_og << endl << endl;
      //Mout << "Lower bound in X3 direction in finer grid for SplineND interpolation : " << low_boundX3_og << endl << endl;
      //Mout << "Upper bound in X3 direction in finer grid for SplineND interpolation : " << upper_boundX3_og << endl << endl;
      //Mout << "# of steps in  X3 direction in finer grid for SplineND interpolation : " << num_of_stepX3_og << endl << endl;
      //Mout << "Lower bound in X4 direction in finer grid for SplineND interpolation : " << low_boundX4_og << endl << endl;
      //Mout << "Upper bound in X4 direction in finer grid for SplineND interpolation : " << upper_boundX4_og << endl << endl;
      //Mout << "# of steps in  X4 direction in finer grid for SplineND interpolation : " << num_of_stepX4_og << endl << endl;
      //Mout << "Lower bound in X5 direction in finer grid for SplineND interpolation : " << low_boundX5_og << endl << endl;
      //Mout << "Upper bound in X5 direction in finer grid for SplineND interpolation : " << upper_boundX5_og << endl << endl;
      //Mout << "# of steps in  X5 direction in finer grid for SplineND interpolation : " << num_of_stepX5_og << endl << endl;
   
      Nb hX1_og;
      Nb hX2_og;
      Nb hX3_og;
      Nb hX4_og;
      Nb hX5_og;
   
      hX1_og = (upper_boundX1_og - low_boundX1_og)/num_of_stepX1_og;
      hX2_og = (upper_boundX2_og - low_boundX2_og)/num_of_stepX2_og;
      hX3_og = (upper_boundX3_og - low_boundX3_og)/num_of_stepX3_og;
      hX4_og = (upper_boundX4_og - low_boundX4_og)/num_of_stepX4_og;
      hX5_og = (upper_boundX5_og - low_boundX5_og)/num_of_stepX5_og;
   
      In nog1 = num_of_stepX1_og + I_1;
      In nog2 = num_of_stepX2_og + I_1;
      In nog3 = num_of_stepX3_og + I_1;
      In nog4 = num_of_stepX4_og + I_1;
      In nog5 = num_of_stepX5_og + I_1;
      In nog1234 = nog1 * nog2 *nog3 *nog4 * nog5;
      x1_int = new Nb [nog1];
      x2_int = new Nb [nog2];
      x3_int = new Nb [nog3];
      x4_int = new Nb [nog4];
      x5_int = new Nb [nog5];
   
      for (In i = I_0; i < nog1; i++)
      {
         x1_int[i] = low_boundX1_og + Nb(i)*hX1_og;
      }
      for (In i = I_0; i < nog2; i++)
      {     
         x2_int[i] = low_boundX2_og + Nb(i)*hX2_og;
      }
      for (In i = I_0; i < nog3; i++)
      {     
         x3_int[i] = low_boundX3_og + Nb(i)*hX3_og;
      }
      for (In i = I_0; i < nog4; i++)
      {     
         x4_int[i] = low_boundX4_og + Nb(i)*hX4_og;
      }
      for (In i = I_0; i < nog5; i++)
      {     
         x5_int[i] = low_boundX5_og + Nb(i)*hX5_og;
      }
      MidasVector Xog1(nog1, x1_int);
      MidasVector Xog2(nog2, x2_int);
      MidasVector Xog3(nog3, x3_int);
      MidasVector Xog4(nog4, x4_int);
      MidasVector Xog5(nog5, x5_int);
      delete[] x1_int;
      delete[] x2_int;
      delete[] x3_int;
      delete[] x4_int;
      delete[] x5_int;
   
      Xog[I_0].SetNewSize(nog1, false);
      Xog[I_1].SetNewSize(nog2, false);
      Xog[I_2].SetNewSize(nog3, false);
      Xog[I_3].SetNewSize(nog4, false);
      Xog[I_4].SetNewSize(nog5, false);
      Xog[I_0] = Xog1;
      Xog[I_1] = Xog2;
      Xog[I_2] = Xog3;
      Xog[I_3] = Xog4;
      Xog[I_4] = Xog5;
   
      //Mout << " allocating GetYog ... " << endl << endl;
      MidasVector Yog(nog1234, C_0);
      MidasVector yog_anal(nog1234, C_0);
      GetYog(Yog, Xog, Xig, Yig, YigDerivs, I_1, SPLINEINTTYPE::GENERAL, FirstDerivLeft, FirstDerivRight);
   
      fnd(Xog[I_0], Xog[I_1], Xog[I_2], Xog[I_3], Xog[I_4], yog_anal);
      
      //Mout.setf(ios_base::scientific, ios_base::floatfield);
      //Mout.setf(ios_base::uppercase);
      //Mout.precision(12);
      //Mout.width(20);
   
   
      //Mout << "**** results from SplineND interpolation **** " << endl << endl;
   
   // *** Then find out some statistics
      Nb Rms;
      RmsDevPerElement(Rms, Yog, yog_anal, nog1234);
      //Mout << "root mean squared deviation is: " << Rms << endl << endl;
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(Rms,1.0),"RMS not close to zero");
   
   
      /*Mout << " Xog1            " <<  "                 "   <<  "Xog2             " << "                    "  <<  "Xog3             " << "               " 
          <<  "Xog4             " <<  "                 " << " Xog5             " <<  "                 "  <<    "Yog              " << " yog_anal           "  << endl << endl;*/
   
      In iog1234=I_0;
      for (In k = I_0; k<nog1; k++)
      {
         for (In j = I_0; j<nog2; j++)
         {
            for (In l = I_0; l<nog3; l++)
            {
               for (In m = I_0; m<nog4; m++)
               {
                  for (In p = I_0; p<nog5; p++)
                  {
                      /*Mout << Xog[I_0][k] << "                "  << Xog[I_1][j] <<   "                     "  << Xog[I_2][l] << "                " 
                           << Xog[I_3][m] << "                "  << Xog[I_4][p] << "                "  <<  Yog[iog1234] <<  "                     " <<  yog_anal[iog1234] << endl;*/
                      UNIT_ASSERT(CheckTest(Yog[iog1234],yog_anal[iog1234]), "SplineND test failed at "+std::to_string(iog1234));
                      ++iog1234;
                  }
               }
            }        
         }
      }
      //Mout  << endl << endl;
   
      //Mout << "********** end of SplineNDTest *****************" << endl;
   }
   bool CheckTest(Nb aSpline, Nb aAnalytic){
      return (aSpline-aAnalytic < 1000*C_NB_EPSILON);
   }
};

} /* namespace midas::test */

#endif //SPLINENDTESTCASE_H
