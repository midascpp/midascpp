#ifndef MEMTEST_H_INCLUDED
#define MEMTEST_H_INCLUDED

#include <memory>

#include "test/CuteeInterface.h"

#include "util/midas_mem_check.h"

namespace midas::test
{

#define SIZE 10

/**
 * @class MemTestBase
 *    Base class for all MemTest unit tests.
 *    Will call test if MIDAS_MEM_DEBUG is defined,
 *    else it will output a string saying that the MemTest is skipped.
 */
template<class A>
struct MemTestBase: public A
{
   void do_test() 
   {
      #ifdef MIDAS_MEM_DEBUG
      A::do_test();
      #else /* MIDAS_MEM_DEBUG */
      // if not mem debug build we make an output to alert the client
      std::cout << " MIDAS_MEM_DEBUG NOT SET, SO I'LL SKIP MEMTEST'S " << std::endl;
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class NoCurruption
 *    Test that we can allocate and deallocate a pointer without mem_stat reporting corrupted memory.
 *    This will test for both front_guard and tail_guard corruption at the same time.
 */
template<class T>
struct NoCorruption: public cutee::test
{
   void run() 
   {
      #ifdef MIDAS_MEM_DEBUG
      UNIT_ASSERT(!mem::get_mem_stat().corrupted(),"MEMORY CORRUPTED BEFORE TEST!");
      std::unique_ptr<T[]> ptr(new T[SIZE]);
      delete[] ptr.release();
      UNIT_ASSERT(!mem::get_mem_stat().corrupted(),"MEMORY CORRUPTED AFTER TEST!");
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class FrontCorruption
 *    Test that mem_stat correctly identifies front_guard corruption.
 *    Will reset corruption flag after test, so we do not interfere with other tests.
 */
template<class T>
struct FrontCorruption: public cutee::test
{
   void run() 
   {
      #ifdef MIDAS_MEM_DEBUG
      UNIT_ASSERT(!mem::get_mem_stat().corrupted(),"MEMORY CORRUPTED BEFORE TEST!");
      std::unique_ptr<T[]> ptr(new T[SIZE]);
      auto ptrd = mem::front_guard(ptr);
      ptrd->magic = 0; // corrupt front guard
      delete[] ptr.release();
      UNIT_ASSERT(mem::get_mem_stat().corrupted(),"MEMORY NOT CORRUPTED AFTER TEST!");
      mem::get_mem_stat().reset_corruption(); // reset corruption to not interfere with other tests
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class BackCorruption
 *    Test that mem_stat correctly identifies tail_guard corruption.  
 *    Will reset corruption flag after test, so we do not interfere with other tests.
 */
template<class T>
struct BackCorruption: public cutee::test
{
   void run() 
   {
      #ifdef MIDAS_MEM_DEBUG
      UNIT_ASSERT(!mem::get_mem_stat().corrupted(),"MEMORY CORRUPTED BEFORE TEST!");
      std::unique_ptr<T[]> ptr(new T[SIZE]);
      auto ptrt = mem::tail_guard(ptr);
      ptrt->magic = 0; // corrupt tail guard
      delete[] ptr.release();
      UNIT_ASSERT(mem::get_mem_stat().corrupted(),"MEMORY NOT CORRUPTED AFTER TEST!");
      mem::get_mem_stat().reset_corruption(); // reset corruption to not interfere with other tests
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class PointerSize
 *    Test that pointer size is set correctly in front_guard.
 */
template<class T>
struct PointerSize: public cutee::test
{
   void run() 
   {
      #ifdef MIDAS_MEM_DEBUG
      std::unique_ptr<T[]> ptr(new T[SIZE]);
      auto ptrd = mem::front_guard(ptr);
      UNIT_ASSERT(ptrd->size/sizeof(T)==SIZE,"SIZE NOT CORRECT");
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class PointerAddress
 *    Test that pointer address is set correctly in front_guard.
 */
template<class T>
struct PointerAddress: public cutee::test
{
   void run() 
   {
      #ifdef MIDAS_MEM_DEBUG
      std::unique_ptr<T[]> ptr(new T[SIZE]);
      auto ptrd = mem::front_guard(ptr);
      UNIT_ASSERT(ptrd->addr==ptr.get(),"ADDRESS NOT CORRECT");
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class CurrentUse
 *    Test that mem_stat's current_use is updated correctly on allocation and on de-allocation.
 */
template<class T>
struct CurrentUse: public cutee::test
{
   void run()
   {
      #ifdef MIDAS_MEM_DEBUG
      auto current_use = mem::get_mem_stat().current_use();
      {
         std::unique_ptr<T[]> ptr(new T[SIZE]);
         mem::log_allocation_guard guard; // do not log allocations in UNIT_ASSERT_EQUAL
         UNIT_ASSERT_EQUAL(mem::get_mem_stat().current_use(), current_use + static_cast<decltype(current_use)>(sizeof(T)*SIZE), "CURRENT USE NOT CORRECT AFTER ALLOCATION.");
      }
      mem::log_allocation_guard guard; // do not log allocations in UNIT_ASSERT_EQUAL
      UNIT_ASSERT_EQUAL(mem::get_mem_stat().current_use(), current_use, "CURRENT USE NOT THE SAME AFTER ALLOCATION AND DEALLOCATION.");
      #endif /* MIDAS_MEM_DEBUG */
   }
};

/**
 * @class TimesNewDelete
 *    Test that mem_stat correctly logs number of new calls and delete calls.
 */
template<class T>
struct TimesNewDelete: public cutee::test
{
   void run()
   {
      #ifdef MIDAS_MEM_DEBUG
      auto times_new = mem::get_mem_stat().times_new();
      auto times_delete = mem::get_mem_stat().times_delete();
      {   
         std::unique_ptr<T[]> ptr(new T[SIZE]);
         mem::log_allocation_guard guard; // do not log allocations in UNIT_ASSERT_EQUAL
         UNIT_ASSERT_EQUAL(mem::get_mem_stat().times_new()   , times_new + static_cast<decltype(times_new)>(1), "TIMES NEW NOT CORRECT AFTER ALLOCATION."   );
         UNIT_ASSERT_EQUAL(mem::get_mem_stat().times_delete(), times_delete                                   , "TIMES DELETE NOT CORRECT AFTER ALLOCATION.");
      }   
      mem::log_allocation_guard guard; // do not log allocations in UNIT_ASSERT_EQUAL
      UNIT_ASSERT_EQUAL(mem::get_mem_stat().times_new()   , times_new    + static_cast<decltype(times_new)>(1), "TIMES NEW NOT CORRECT AFTER DEALLOCATION."   );
      UNIT_ASSERT_EQUAL(mem::get_mem_stat().times_delete(), times_delete + static_cast<decltype(times_new)>(1), "TIMES DELETE NOT CORRECT AFTER DEALLOCATION.");
      #endif /* MIDAS_MEM_DEBUG */
   }
};

#undef SIZE

//
// Wrap MemTest's in MemBaseBase
//
template<class T>
using MemTestNoCorruption = MemTestBase<NoCorruption<T> >;
template<class T>
using MemTestFrontCorruption = MemTestBase<FrontCorruption<T> >;
template<class T>
using MemTestBackCorruption = MemTestBase<BackCorruption<T> >;
template<class T>
using MemTestPointerSize = MemTestBase<PointerSize<T> >;
template<class T>
using MemTestPointerAddress = MemTestBase<PointerAddress<T> >;
template<class T>
using MemTestCurrentUse = MemTestBase<CurrentUse<T> >;
template<class T>
using MemTestTimesNewDelete = MemTestBase<TimesNewDelete<T> >;

} /* namespace midas::test */

#endif /*MEMTEST_H_INCLUDED */
