/**
 *******************************************************************************
 * 
 * @file    TestDrivers.cc
 * @date    21-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For handling unit test drivers.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <iomanip>
#include <utility>

#include "input/Trim.h"
#include "test/TestDrivers.h"
#include "test/Test.h"

namespace midas::test
{
   /************************************************************************//**
    * @name Individual unit test drivers. 
    ***************************************************************************/
   //!@{
   void NiTest();
   void NucleiTest();
   void Spline1DTest();
   void Spline2DTest();
   void Spline3DTest();
   void SplineNDTest();
   void BsplinePlotter();
   void MatVecTest();
   void GenericFunctionTests();
   void TaylorTest();
   void GlobalDataTest();
   void DataContTest();
   void TypeTraitsTest();
   void FromStringTest();
   void FunctionTraitsTest();
   void CalcCodeTest();
   void ItSolverTest();
   void MemTest();
   void ParalTest();
   void ILapackTest();
   void RangeTest();
   void BaseTensorTest();
   void MathTest();
   void PropertyFileStorageTest();
   void SinglePointTest();
   void PesTest();
   void ModelPotTest();
   void MoleculeTest();
   void TensorDataContTest();
   void McTdHVectorContainerTest();
   void VccStateSpaceTest();
   void PesIntegralsTest();
   void QuadratureWrappersTest();
   void OdeTest();
   void OrderedCombinationTest();
   void ModeCombiOpRangeTest();
   void TestDriversTest();
   void ExtLibsTest();
   void ModalIntegralsTest();
   void PathTest();
   void MatRepVibOperTest();
   void Vcc2TransTest();
   void ErrorTest();
   void VibCalcTestUtilsTest();
   void TdOperTest();
   void TdvccTest();
   void AlgorithmTest();
   void GenerateTensorNamesTest();
   void ContainerMathWrappersTest();
   void SubspaceSolverTest();
   void MatRepSolversTest();
   void MvccSolverTest();
   void TdmvccTest();
   void RandomTest();
   void OperatorTest();
   void VectorAngleTest();
   void GeoOptTest();
   void MlearnTest();
   void OneModeDensitiesTest();
   void PolyFitSvdTest();


   //CHANGE! Copy UnitTestTemplate line and append above for your own unit test.
   void UnitTestTemplateTest();
   void PlaygroundRun();
   //!@}

   /************************************************************************//**
    * Update this with your own unit test! The string is the keyword that must
    * be given in the input file in order to run your test. (The string is
    * space and case insensitive.)
    *
    * @return
    *    Map of identifiers -> unit test drivers.
    ***************************************************************************/
   std::map<TestDrivers::identifier_t, TestDrivers::driver_t> TestDrivers::AvailableDrivers
      (
      )
   {
      return
         {  {"Ni", NiTest}
         ,  {"Nuclei", NucleiTest}
         ,  {"Spline1D", Spline1DTest}
         ,  {"Spline2D", Spline2DTest}
         ,  {"Spline3D", Spline3DTest}
         ,  {"SplineND", SplineNDTest}
         ,  {"BsplinePlotter", BsplinePlotter}
         ,  {"MatVec", MatVecTest}
         ,  {"GenericFunction", GenericFunctionTests}
         ,  {"Taylor", TaylorTest}
         ,  {"GlobalData", GlobalDataTest}
         ,  {"DataCont", DataContTest}
         ,  {"TypeTraits", TypeTraitsTest}
         ,  {"FromString", FromStringTest}
         ,  {"FunctionTraits", FunctionTraitsTest}
         ,  {"CalcCode", CalcCodeTest}
         ,  {"ItSolver", ItSolverTest}
         ,  {"Mem", MemTest}
         ,  {"Paral", ParalTest}
         ,  {"ILapack", ILapackTest}
         ,  {"Range", RangeTest}
         ,  {"BaseTensor", BaseTensorTest}
         ,  {"Math", MathTest}
         ,  {"PropertyFileStorage", PropertyFileStorageTest}
         ,  {"SinglePoint", SinglePointTest}
         ,  {"Pes", PesTest}
         ,  {"ModelPot", ModelPotTest}
         ,  {"Molecule", MoleculeTest}
         ,  {"Algorithm", AlgorithmTest}
         ,  {"TensorDataCont", TensorDataContTest}
         ,  {"McTdHVectorContainer", McTdHVectorContainerTest}
         ,  {"VccStateSpace", VccStateSpaceTest}
         ,  {"PesIntegrals", PesIntegralsTest}
         ,  {"QuadratureWrappers", QuadratureWrappersTest}
         ,  {"Ode", OdeTest}
         ,  {"OrderedCombination", OrderedCombinationTest}
         ,  {"ModeCombiOpRange", ModeCombiOpRangeTest}
         ,  {"TestDrivers", TestDriversTest}
         ,  {"ExtLibs", ExtLibsTest}
         ,  {"ModalIntegrals", ModalIntegralsTest}
         ,  {"Path", PathTest}
         ,  {"MatRepVibOper", MatRepVibOperTest}
         ,  {"Vcc2Trans", Vcc2TransTest}
         ,  {"Error", ErrorTest}
         ,  {"VibCalcTestUtils", VibCalcTestUtilsTest}
         ,  {"SubspaceSolver", SubspaceSolverTest}
         ,  {"ContainerMathWrappers", ContainerMathWrappersTest}
         ,  {"TdOper", TdOperTest}
         ,  {"Tdvcc", TdvccTest}
         ,  {"GenerateTensorNamesTest", GenerateTensorNamesTest}
         ,  {"MatRepSolvers", MatRepSolversTest}
//         ,  {"MvccSolver", MvccSolverTest}    // Niels: does not work at the moment
         ,  {"Tdmvcc", TdmvccTest}
         ,  {"Random", RandomTest}
         ,  {"Operator", OperatorTest}
         ,  {"VectorAngle", VectorAngleTest}
         ,  {"GeoOpt", GeoOptTest}
         ,  {"Mlearn", MlearnTest}
         ,  {"OneModeDensities", OneModeDensitiesTest}
         ,  {"PolyFitSvd", PolyFitSvdTest}

         //CHANGE! Copy UnitTestTemplate line and append above for your own unit test.
         ,  {"UnitTestTemplate", UnitTestTemplateTest}
         ,  {"PlaygroundRun", PlaygroundRun}
         };
   }

   //! Initialize static member variable mTestDrivers.
   const std::map<TestDrivers::identifier_t, TestDrivers::driver_t> TestDrivers::mTestDrivers = TestDrivers::Parse(TestDrivers::AvailableDrivers());

   /************************************************************************//**
    * The argument identifier must match (after parsing) one of the keys in
    * mTestDrivers. This function handles the parsing, thus aIdent is
    * space/case insensitive.
    *
    * @param[in] aIdent
    *    Identifier for the driver to be appended.
    * @return
    *    Whether driver was successfully appended.
    ***************************************************************************/
   bool TestDrivers::AppendDriver
      (  identifier_t aIdent
      )
   {
      auto parsed = Parse(std::move(aIdent));
      if (IsValidDriver(parsed))
      {
         mSpecificDrivers.push_back(std::move(parsed));
         return true;
      }
      else
      {
         return false;
      }
   }

   /************************************************************************//**
    * @return
    *    Whether to run all drivers. If any drivers have been specifically
    *    added, only those will be run.
    ***************************************************************************/
   bool TestDrivers::TestAll
      (
      )  const
   {
      return mSpecificDrivers.empty();
   }

   /************************************************************************//**
    * @return
    *    The drivers to run, be it all or only some specific ones.
    ***************************************************************************/
   const std::vector<TestDrivers::driver_t> TestDrivers::Drivers
      (
      )  const
   {
      std::vector<driver_t> result;
      if (TestAll())
      {
         result.reserve(mTestDrivers.size());
         for(const auto& driver: mTestDrivers)
         {
            result.push_back(driver.second);
         }
      }
      else
      {
         result.reserve(mSpecificDrivers.size());
         for(const auto& ident: mSpecificDrivers)
         {
            // mSpecificDrivers contains only valid identifiers, so no try-catch.
            result.push_back(mTestDrivers.at(ident));
         }
      }
      return result;
   }

   /************************************************************************//**
    * @return
    *    A map of identifiers to bools, that signify whether or not that
    *    specific driver is currently enabled for testing in this object.
    ***************************************************************************/
   std::map<TestDrivers::identifier_t, bool> TestDrivers::Summary
      (
      )  const
   {
      // Local, sorted copy for binary_search lookup.
      std::vector<identifier_t> v_sorted(mSpecificDrivers);
      std::sort(v_sorted.begin(), v_sorted.end());
      bool all_enabled = TestAll();
      auto enabled = [&v_sorted, &all_enabled](const identifier_t i) -> bool
         {
            return all_enabled || std::binary_search(v_sorted.begin(), v_sorted.end(), i);
         };
      // Build identifier-bool map.
      std::map<identifier_t, bool> result;
      for(const auto& kv: mTestDrivers)
      {
         result.emplace_hint(result.end(), kv.first, enabled(kv.first));
      }
      return result;
   }

   /************************************************************************//**
    * @param[in,out] arOs
    *    The ostream to which this function prints a list of available drivers.
    ***************************************************************************/
   void TestDrivers::Help
      (  std::ostream& arOs
      )
   {
      arOs << "The valid test driver identifiers are (space and case insensitive):\n";
      for(const auto& kv: mTestDrivers)
      {
         arOs << std::setw(3) << "" << kv.first << '\n';
      }
   }


   /************************************************************************//**
    * @param[in] arIdent
    *    Identifier to validate.
    * @return
    *    Whether argument identifies an actual driver in mTestDrivers.
    ***************************************************************************/
   bool TestDrivers::IsValidDriver
      (  const identifier_t& arIdent
      )
   {
      return (mTestDrivers.find(arIdent) != mTestDrivers.end());
   }

   /************************************************************************//**
    * @param[in] aIdent
    *    Identifier to parse.
    * @return
    *    Identifier after parsing. (No blanks, all lowercase.)
    ***************************************************************************/
   TestDrivers::identifier_t TestDrivers::Parse
      (  identifier_t aIdent
      )
   {
      return midas::input::ToLowerCase(midas::input::DeleteBlanks(std::move(aIdent)));
   }

   /************************************************************************//**
    * @param[in] aMap
    *    Map to parse.
    * @return
    *    Map after parsing.
    ***************************************************************************/
   std::map<TestDrivers::identifier_t, TestDrivers::driver_t> TestDrivers::Parse
      (  std::map<identifier_t, driver_t> aMap
      )
   {
      std::map<identifier_t, driver_t> result;
      for(auto&& kv: aMap)
      {
         result.emplace_hint(result.end(), Parse(std::move(kv.first)), std::move(kv.second));
      }
      return result;
   }

} /* namespace midas::test */
