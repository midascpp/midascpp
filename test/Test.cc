/**
************************************************************************
* 
* @file                Test.cc
*
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Test driver for the midas program.
* 
* Last modified: Thu Oct 26, 2006  06:00PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:

// My headers:
//#include "inc_gen/TypeDefs.h"
#include "test/Test.h"
#include "test/TestDrivers.h"
//#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "mpi/Impi.h"

extern MidasStream Mout;

/***************************************************************************//**
 * Run test of units/features, pass control to local drivers.
 ******************************************************************************/
void midas::test::Test(const TestDrivers& arDrivers)
{
   midas::stream::ScopedPrecision(16, Mout);
   Mout << std::scientific;
   Mout << " Midas Tests starting on rank : " << midas::mpi::GlobalRank() << std::endl;

   for(const auto& driver: arDrivers.Drivers())
   {
      driver();
   }

   Mout << " Midas Tests ended " << endl;
}


