#include "test/CalcCodeTest.h"

namespace midas::test
{

void CalcCodeTest()
{
   cutee::suite suite("CalcCode test");

   //
   suite.add_test<TestCalcCodeStringConv>("CalcCode to string conversion test");
   suite.add_test<TestCalcCodeSetOrdering>("CalcCode sorting test");
   
   //
   RunSuite(suite);
}

} /* namespace midas::test */
