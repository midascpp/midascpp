/*
************************************************************************
* 
* @file                TaylorTest.h
*
* Created:             11-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   
* 
* Last modified: Thu Dec 03, 2009  02:05PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef TAYLORTEST_H_INCLUDED
#define TAYLORTEST_H_INCLUDED

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "test/CuteeInterface.h"
#include "mathlib/Taylor/taylor.hpp"

namespace midas::test
{

/**
 * Note the workings of the taylor template here, first we have the variable
 * type used for creating the taylor expansion second we have the number of
 * variables and lastly the order of the taylor expansion...
 **/
struct SimpleTaylorTestCase : public cutee::test
{
   Nb fx(Nb aX)
   {
      return C_2*aX*aX+C_5*aX+C_4;
   }

   Nb dfx(Nb aX)
   {
      return C_4*aX+C_5;
   }

   taylor<Nb, 1, 2> taylorfx(const taylor<Nb, 1, 2>& aX)
   {
      return C_2*aX*aX+C_5*aX+C_4;
   }

   void run() 
   {
      taylor<Nb, 1, 2> newEpsilon(0,0);
      taylor<Nb, 1, 2> xtaylor;
      taylor<Nb, 1, 2> ytaylor;
      Nb x, y, dydx;
      for(In i = I_0; i < 50; ++i)
      {
         x = i/15.5;
         xtaylor = x + newEpsilon;
         ytaylor = taylorfx(xtaylor);
         y = fx(x);
         dydx = dfx(x);
         UNIT_ASSERT(CheckTest(ytaylor[0], y), "Function evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ytaylor[1], dydx), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ytaylor[2]*C_2, C_4), "Derivative evaluation failed for "+std::to_string(i));
      }
   }
   
   bool CheckTest(Nb aSpline, Nb aAnalytic)
   {
      return (std::fabs((aSpline-aAnalytic)/aAnalytic) < C_NB_EPSILON);
   }
};

struct TaylorTestCase2D : public cutee::test
{
   Nb f(Nb aX, Nb aY)
   {
      return C_2*aX*aX+C_3*aX*aY+C_7*aY*aY+C_5*aX+C_5*aY+C_4;
   }

   Nb fdx(Nb aX, Nb aY)
   {
      return C_4*aX+C_3*aY+C_5;
   }

   Nb fdy(Nb aX, Nb aY)
   {
      return C_3*aX+C_7*C_2*aY+C_5;
   }

   taylor<Nb, 2, 2> taylorf(const taylor<Nb, 2, 2>& aX, const taylor<Nb, 2, 2>& aY)
   {
      return C_2*aX*aX+C_3*aX*aY+C_7*aY*aY+C_5*aX+C_5*aY+C_4;
   }

   void run() 
   {
      taylor<Nb, 2, 2> newEpsilon1(0,0);
      taylor<Nb, 2, 2> newEpsilon2(0,1);
      taylor<Nb, 2, 2> xtaylor;
      taylor<Nb, 2, 2> ytaylor;
      taylor<Nb, 2, 2> ftaylor;
      Nb x, y, fval, dy, dx;
      for(In i = I_0; i < 50; ++i)
      {
         x = i/15.5;
         y = i/23.1;
         xtaylor = x + newEpsilon1;
         ytaylor = y + newEpsilon2;
         ftaylor = taylorf(xtaylor, ytaylor);
         fval = f(x,y);
         dx = fdx(x,y);
         dy = fdy(x,y);
         UNIT_ASSERT(CheckTest(ftaylor[0], fval), "Function evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ftaylor[1], dx), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ftaylor[2], dy), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ftaylor[3]*C_2, C_4), "Function evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ftaylor[4], C_3), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ftaylor[5]*C_2, C_7*C_2), "Derivative evaluation failed for "+std::to_string(i));
      }
   }
   
   bool CheckTest(Nb aSpline, Nb aAnalytic)
   {
      return (std::fabs((aSpline-aAnalytic)/aAnalytic) < C_NB_EPSILON);
   }
};

struct CrazyTaylorTestCase : public cutee::test
{
   Nb fx(Nb aX)
   {
      return cos(aX)*exp(C_2*aX*aX+C_3*aX+C_5);
   }

   Nb dfdx(Nb aX)
   {
      return -sin(aX)*exp(C_2*aX*aX+C_3*aX+C_5)+cos(aX)*(C_4*aX+C_3)*exp(C_2*aX*aX+C_3*aX+C_5);
   }

   Nb ddfdxdx(Nb aX)
   {
      return -cos(aX)*exp(C_2*aX*aX+C_3*aX+C_5)-sin(aX)*(C_4*aX+C_3)*exp(C_2*aX*aX+C_3*aX+C_5)
            -sin(aX)*(C_4*aX+C_3)*exp(C_2*aX*aX+C_3*aX+C_5)+cos(aX)*C_4*exp(C_2*aX*aX+C_3*aX+C_5)
            +cos(aX)*(C_4*aX+C_3)*(C_4*aX+C_3)*exp(C_2*aX*aX+C_3*aX+C_5);
   }

   taylor<Nb, 1, 2> taylorfx(const taylor<Nb, 1, 2>& aX)
   {
      return cos(aX)*exp(C_2*aX*aX+C_3*aX+C_5);
   }

   void run() 
   {
      taylor<Nb, 1, 2> newEpsilon(0,0);
      taylor<Nb, 1, 2> xtaylor;
      taylor<Nb, 1, 2> ytaylor;
      Nb x, y, dydx, ddydxdx;
      for(In i = I_0; i < 50; ++i)
      {
         x = i/15.5;
         xtaylor = x + newEpsilon;
         ytaylor = taylorfx(xtaylor);
         y = fx(x);
         dydx = dfdx(x);
         ddydxdx = ddfdxdx(x);
         UNIT_ASSERT(CheckTest(ytaylor[0], y), "Function evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ytaylor[1], dydx), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(ytaylor[2]*C_2, ddydxdx), "DDerivative evaluation failed for "+std::to_string(i));
      }
   }
   
   bool CheckTest(Nb aSpline, Nb aAnalytic)
   {
      return (std::fabs((aSpline-aAnalytic)/aAnalytic) < 100*C_NB_EPSILON);
   }
};

struct TaylorGenericFuncInterfaceTest : public cutee::test
{
   taylor<Nb, 1, 2> taylorfx(const taylor<Nb, 1, 2>& aX)
   {
      return C_2*aX*aX+C_5*aX+C_4;
   }

   void run() 
   {
      FunctionContainer<taylor<Nb, 1, 2> > func_cont;
      GenericFunctionWrapper<taylor<Nb, 1, 2> > gen_fx("2*Q*Q+5*Q+4",func_cont);
      taylor<Nb, 1, 2> newEpsilon(0,0);
      taylor<Nb, 1, 2> xtaylor;
      taylor<Nb, 1, 2> ytaylor;
      taylor<Nb, 1, 2> genytaylor;
      Nb x;
      for(In i = I_0; i < 50; ++i)
      {
         x = i/15.5;
         xtaylor = x + newEpsilon;
         ytaylor = taylorfx(xtaylor);
         gen_fx.SetVariableI(xtaylor, I_0);
         genytaylor = gen_fx.EvaluateFunc();
         UNIT_ASSERT(CheckTest(genytaylor[0],ytaylor[0]), "Function evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(genytaylor[1],ytaylor[1]), "Derivative evaluation failed for "+std::to_string(i));
         UNIT_ASSERT(CheckTest(genytaylor[2],ytaylor[2]), "Derivative evaluation failed for "+std::to_string(i));
      }
   }
   
   bool CheckTest(Nb aSpline, Nb aAnalytic)
   {
      return ((aSpline-aAnalytic)/aAnalytic < C_NB_EPSILON);
   }
};

} /* namespace midas::test */

#endif //TAYLORTEST_H_INCLUDED
