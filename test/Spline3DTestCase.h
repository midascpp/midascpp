/**
************************************************************************
* 
* @file                Spline3DTest.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli
*
* Short Description:   Test Spline3D class 
* 
* Last modified: Thu Dec 03, 2009  02:01PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SPLINE3DTESTCASE_H
#define SPLINE3DTESTCASE_H
// Standard Headers.
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// My Headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Test.h"
#include "test/CuteeInterface.h"
#include "input/Input.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/Spline3D.h"
//#include "pes/SplineND.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"

namespace midas::test
{

/**
* Controls the testing of stuff 
* */

struct Spline3DTestCase : public cutee::test
{
   void fxyz (const MidasVector&  arXog1, const MidasVector& arXog2, const MidasVector& arXog3, MidasVector& arYog) 
   {
      In nog1 = arXog1.Size();
      In nog2 = arXog2.Size();
      In nog3 = arXog3.Size();
      MidasVector xog1_tmp = arXog1;
      MidasVector xog2_tmp = arXog2;
      MidasVector xog3_tmp = arXog3;
      xog1_tmp.Pow(I_3);
      xog2_tmp.Pow(I_3);
      xog3_tmp.Pow(I_3);
      In nog123 = nog1 * nog2 * nog3;
      if ( nog123 != arYog.Size())
      {
        arYog.SetNewSize(nog123,false);
      }
      In iog123 = I_0;
      for (In iog1 =I_0; iog1 < nog1; iog1++)
      {
         for (In iog2 =I_0; iog2 < nog2; iog2++)
         { 
            for (In iog3 =I_0; iog3 < nog3; iog3++)
            {
               arYog[iog123] = xog1_tmp[iog1] + xog2_tmp[iog2] + xog3_tmp[iog3];
               iog123+=I_1;
            }
         }
      }
   }
   
   // uncomment //! for using SplineND class
   /**
   * performing test for Spline3D(ND) class
   **/
   
   void run() 
   {
      //Mout << "****** Performing test of Spline3D class " << endl << endl;
   
   // *** then defines the function to be interpolated (a normal cubic in three dimensions)
   
      Nb* Xig1_tmp;
      Nb* Xig2_tmp;
      Nb* Xig3_tmp;
      Nb low_boundX1_ig   = -C_1;
      Nb upper_boundX1_ig = C_1;
      Nb low_boundX2_ig   = -C_1;
      Nb upper_boundX2_ig = C_1;
      Nb low_boundX3_ig   = -C_1;
      Nb upper_boundX3_ig = C_1;
      In num_of_stepX1_ig = I_5;
      In num_of_stepX2_ig = I_5;
      In num_of_stepX3_ig = I_5;
      Nb hX1_ig;
      Nb hX2_ig;
      Nb hX3_ig;
   
   // *** input lower and upper bounds and the # of steps
    
   //   Minp << Nb low_bound << Nb upper_bound << In num_of_steps << endl;
      //Mout << "Lower bound in X1 direction for Spline3D interpolation : " << low_boundX1_ig   << endl << endl;
      //Mout << "Upper bound in X1 direction for Spline3D interpolation : " << upper_boundX1_ig << endl << endl;
      //Mout << "# of steps  in X1 direction for Spline3D interpolation : " << num_of_stepX1_ig << endl << endl;
      //Mout << "Lower bound in X2 direction for Spline3D interpolation : " << low_boundX2_ig   << endl << endl;
      //Mout << "Upper bound in X2 direction for Spline3D interpolation : " << upper_boundX2_ig << endl << endl;
      //Mout << "# of steps  in X2 direction for Spline3D interpolation : " << num_of_stepX2_ig << endl << endl;
      //Mout << "Lower bound in X3 direction for Spline3D interpolation : " << low_boundX3_ig   << endl << endl;
      //Mout << "Upper bound in X3 direction for Spline3D interpolation : " << upper_boundX3_ig << endl << endl;
      //Mout << "# of steps  in X3 direction for Spline3D interpolation : " << num_of_stepX3_ig << endl << endl;
               
   
      hX1_ig = (upper_boundX1_ig-low_boundX1_ig)/num_of_stepX1_ig;
      hX2_ig = (upper_boundX2_ig-low_boundX2_ig)/num_of_stepX2_ig;
      hX3_ig = (upper_boundX3_ig-low_boundX3_ig)/num_of_stepX3_ig;
   
      In nig1 = num_of_stepX1_ig+I_1;
      In nig2 = num_of_stepX2_ig+I_1;
      In nig3 = num_of_stepX3_ig+I_1;
   
      Xig1_tmp = new Nb [nig1];
      Xig2_tmp = new Nb [nig2];
      Xig3_tmp = new Nb [nig3];
   
      for (In i = I_0; i < nig1; i++)
      { 
         Xig1_tmp[i] = low_boundX1_ig + Nb(i)*hX1_ig;
      }
      for (In i = I_0; i < nig2; i++)
      {
         Xig2_tmp[i] = low_boundX2_ig + Nb(i)*hX2_ig;
      }
      for (In i = I_0; i < nig3; i++)
      {
         Xig3_tmp[i] = low_boundX3_ig + Nb(i)*hX3_ig;
      }
      MidasVector Xig1(nig1, Xig1_tmp);
      MidasVector Xig2(nig2, Xig2_tmp);
      MidasVector Xig3(nig3, Xig3_tmp);
   
      delete[] Xig1_tmp;
      delete[] Xig2_tmp;
      delete[] Xig3_tmp;
   
   //!   vector<MidasVector> Xig(I_3);
   //!   vector<MidasVector> Xog(I_3);
   //!   Xig[I_0].SetNewSize(nig1, false);
   //!   Xig[I_1].SetNewSize(nig2, false);
   //!   Xig[I_2].SetNewSize(nig3, false);
   //!   Xig[I_0]=Xig1;
   //!   Xig[I_1]=Xig2;
   //!   Xig[I_2]=Xig3;
      
      In nig123 = nig1 * nig2 * nig3;
      MidasVector Yig(nig123, C_0);
      MidasVector YigDerivs(nig123, C_0);
   
      fxyz(Xig1, Xig2, Xig3, Yig);
   
   // *** then construct the 3D cubic natural spline interpolant
   // *** construct the first derivatives arrays
      MidasVector FirstDerivLeft(I_3, C_3);
      MidasVector FirstDerivRight(I_3, C_3);
      
      Spline3D CubicApprox(SPLINEINTTYPE::GENERAL, Xig1, Xig2, Xig3, Yig, FirstDerivLeft[I_2], FirstDerivRight[I_2]);
      CubicApprox.GetYigDerivs(YigDerivs);
   
   // and evaluate at few selected points
   
      Nb* x1_int;
      Nb* x2_int;
      Nb* x3_int;
      Nb low_boundX1_og   = -C_1;
      Nb upper_boundX1_og = C_1;
      Nb low_boundX2_og   = -C_1;
      Nb upper_boundX2_og = C_1;
      Nb low_boundX3_og   = -C_1;
      Nb upper_boundX3_og = C_1;
      In num_of_stepX1_og = I_100;
      In num_of_stepX2_og = I_100;
      In num_of_stepX3_og = I_100;
   
      //Mout << "Lower bound in X1 direction for finer grid in Spline3D interpolation : " << low_boundX1_og   << endl << endl;
      //Mout << "Upper bound in X1 direction for finer grid in Spline3D interpolation : " << upper_boundX1_og << endl << endl;
      //Mout << "# of steps  in X1 direction for finer grid in Spline3D interpolation : " << num_of_stepX1_og << endl << endl;
      //Mout << "Lower bound in X2 direction for finer grid in Spline3D interpolation : " << low_boundX2_og   << endl << endl;
      //Mout << "Upper bound in X2 direction for finer grid in Spline3D interpolation : " << upper_boundX2_og << endl << endl;
      //Mout << "# of steps  in X2 direction for finer grid in Spline3D interpolation : " << num_of_stepX2_og << endl << endl;
      //Mout << "Lower bound in X3 direction for finer grid in Spline3D interpolation : " << low_boundX3_og   << endl << endl;
      //Mout << "Upper bound in X3 direction for finer grid in Spline3D interpolation : " << upper_boundX3_og << endl << endl;
      //Mout << "# of steps  in X3 direction for finer grid in Spline3D interpolation : " << num_of_stepX3_og << endl << endl;
   
      Nb hX1_og;
      Nb hX2_og;
      Nb hX3_og;
   
      hX1_og = (upper_boundX1_og-low_boundX1_og)/num_of_stepX1_og;
      hX2_og = (upper_boundX2_og-low_boundX2_og)/num_of_stepX2_og;
      hX3_og = (upper_boundX3_og-low_boundX3_og)/num_of_stepX3_og;
   
      In nog1 = num_of_stepX1_og + I_1;
      In nog2 = num_of_stepX2_og + I_1;
      In nog3 = num_of_stepX3_og + I_1;
      In nog123 = nog1 * nog2 *nog3;
      x1_int = new Nb [nog1];
      x2_int = new Nb [nog2];
      x3_int = new Nb [nog3];
   
      for (In i = I_0; i < nog1; i++)
      {
         x1_int[i] = low_boundX1_og + Nb(i)*hX1_og;
      }
      for (In i = I_0; i < nog2; i++)
      {     
         x2_int[i] = low_boundX2_og + Nb(i)*hX2_og;
      }
      for (In i = I_0; i < nog3; i++)
      {     
         x3_int[i] = low_boundX3_og + Nb(i)*hX3_og;
      }
      MidasVector Xog1(nog1, x1_int);
      MidasVector Xog2(nog2, x2_int);
      MidasVector Xog3(nog3, x3_int);
   
      delete[] x1_int;
      delete[] x2_int;
      delete[] x3_int;
   
      MidasVector Yog(nog123, C_0);
      MidasVector yog_analysis(nog123, C_0);
      GetYog(Yog, Xog1, Xog2, Xog3, Xig1, Xig2, Xig3, Yig, YigDerivs, SPLINEINTTYPE::GENERAL, FirstDerivLeft, FirstDerivRight);
      fxyz(Xog1, Xog2, Xog3, yog_analysis);
      
   
   // *** Then find out some statistics
    
      Nb Rms;
      RmsDevPerElement(Rms, Yog, yog_analysis, nog123);
      //Mout << "root mean squared deviation is: " << Rms << endl << endl;
      
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(Rms,1.0),"RMS not close to zero");
   
      //Mout << "Xog1     " <<  "        "   <<  "Xog2      " << "          " << "Xog3      " << "          "  <<  "Yog      " << "           "  <<  "yog_analysis       " << endl;
      In iog123=I_0;
      for (In k = I_0; k<nog1; k++)
      {
         for (In j = I_0; j<nog2; j++)
         {
            for (In l = I_0; l<nog3; l++)
            {
               //Mout << Xog1[k] << "      "  << Xog2[j] << "      "  << Xog3[l] << "      " << Yog[iog123]  << "     " << yog_analysis[iog123] << endl;
               UNIT_ASSERT(CheckTest(Yog[iog123],yog_analysis[iog123]),"Spline3D test failed at "+std::to_string(iog123));
               iog123+=I_1;
            }
         }        
      }
      //Mout << endl << endl;
   //Mout << "********** end of Spline3DTest *****************" << endl;
   }

   bool CheckTest(Nb aSpline, Nb aAnalytic){
      return (aSpline-aAnalytic < 10*C_NB_EPSILON);
   }
};

} /* namespace midas::test */

#endif //SPLINE3DTESTCASE_H
