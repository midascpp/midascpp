/**
 *******************************************************************************
 * 
 * @file    TdOperTest.cc
 * @date    07-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/TdOperTest.h"
#include "input/ModeCombiOpRange.h"
#include "test/util/TransTestUtils.h"

namespace midas::test
{
   namespace tdoper::detail
   {
      /*********************************************************************//**
       *
       ************************************************************************/
      OpDef GetEmptyOpDef()
      {
         ModeCombiOpRange mcr;
         std::vector<Uin> n_opers;
         std::vector<std::vector<Nb>> coefs;
         return util::GetOpDefFromCoefs<Nb>(mcr, n_opers, coefs);
      }

   } /* namespace tdoper::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void TdOperTest()
   {
      // Create test_suite object.
      cutee::suite suite("TdOper test");

      // Add all the tests to the suite.
      using namespace tdoper;
      suite.add_test<TdOperTermTimeIndep<Nb>>("TdOperTerm, c = 1", Nb(1));
      suite.add_test<TdOperTermTimeIndep<std::complex<Nb>>>("TdOperTerm, c = 1+0i", std::complex<Nb>(1));
      suite.add_test<TdOperTermTimeIndep<std::complex<Nb>>>("TdOperTerm, c = i", std::complex<Nb>(0,1));
      suite.add_test<TdOperTermTimeDepCustomDeriv<Nb>>("TdOperTerm, TD, custom der, real");
      suite.add_test<TdOperTermTimeDepCustomDeriv<std::complex<Nb>>>("TdOperTerm, TD, custom der, comp");
      //suite.add_test<TdOperTermTimeDepImplDeriv<Nb>>("TdOperTerm, TD, impl der, real");
      //suite.add_test<TdOperTermTimeDepImplDeriv<std::complex<Nb>>>("TdOperTerm, TD, impl der, comp");
      suite.add_test<TdOperTermTimeDepFallbackDeriv<Nb>>("TdOperTerm, TD, fallback der, real");
      suite.add_test<TdOperTermTimeDepFallbackDeriv<std::complex<Nb>>>("TdOperTerm, TD, fallback der, comp");

      suite.add_test<LinCombOperTest<Nb>>("LinCombOperTest, Nb");
      suite.add_test<LinCombOperTest<std::complex<Nb>>>("LinCombOperTest, std::complex<Nb>");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
