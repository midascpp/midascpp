/**
 *******************************************************************************
 * 
 * @file    PesIntegralsTest.cc
 * @date    20-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for pes/integrals classes.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <cmath>

#include "test/PesIntegralsTest.h"
#include "pes/integrals/OneModeIntegrals.h"
#include "pes/integrals/SumOverProductIndexer.h"
#include "pes/integrals/ModeCombiIntegrals.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"

#include "util/Error.h"

namespace midas::test::pes_integrals
{

namespace onemodeintegrals{

/***************************************************************************//**
 * @brief
 *    Construct and check number of functions, intervals are set correctly. No
 *    scaling factor.
 ******************************************************************************/
void Constructor::run()
{
   using func_t = std::function<Nb(Nb)>;
   FunctionGenerator<func_t> gen;
   auto func = gen.Functions();
   auto intervals = gen.intervals;

   OneModeIntegrals<func_t> omi(12, func, intervals);

   decltype(omi.NumFunctions()) ctrl_num_f = func.size();
   decltype(omi.NumIntervals()) ctrl_num_i = intervals.size();
   UNIT_ASSERT_EQUAL(omi.NumFunctions(), ctrl_num_f, "Wrong number of functions.");
   UNIT_ASSERT_EQUAL(omi.NumIntervals(), ctrl_num_i, "Wrong number of intervals.");
}

/***************************************************************************//**
 * @brief
 *    Check interval begin/end points.
 ******************************************************************************/
void IntervalBeginEndPoints::run()
{
   using func_t = std::function<Nb(Nb)>;
   FunctionGenerator<func_t> gen;
   auto func = gen.Functions();
   auto intervals = gen.intervals;

   OneModeIntegrals<func_t> omi(12, func, intervals);

   decltype(omi.NumIntervals()) ctrl_num_i = intervals.size();
   UNIT_ASSERT_EQUAL(omi.NumIntervals(), ctrl_num_i, "Wrong number of intervals.");
   for(Uin i = I_0; i < omi.NumIntervals(); ++i)
   {
      decltype(omi.IntervalBegin(0)) ctrl_beg = intervals[i].first;
      decltype(omi.IntervalEnd(0))   ctrl_end = intervals[i].second;
      UNIT_ASSERT_EQUAL(omi.IntervalBegin(i), ctrl_beg, "Wrong IntervalBegin("+std::to_string(i)+").");
      UNIT_ASSERT_EQUAL(omi.IntervalEnd(i), ctrl_end, "Wrong IntervalEnd("+std::to_string(i)+").");
   }
}

} /* onemodeintegrals */




namespace sumoverproductindexer{
using midas::pes::integrals::SumOverProductIndexer;

/***************************************************************************//**
 * @brief
 *    Very basic constructor check; no terms, no functions.
 ******************************************************************************/
void ConstructBasic::run()
{
   using func_t = std::function<Nb(Nb)>;
   std::vector<Nb> c;
   std::vector<std::vector<func_t>> f;

   SumOverProductIndexer<func_t> sop(c, f, [](const func_t&, const func_t&)->bool{return true;});

   UNIT_ASSERT_EQUAL(sop.NumModes(), Uin(0), "Wrong number of modes.");
   UNIT_ASSERT_EQUAL(sop.NumTerms(), Uin(0), "Wrong number of terms.");
}

} /* namespace sumoverproductindexer */

namespace modecombiintegrals{
using midas::pes::integrals::SumOverProductIndexer;
using midas::pes::integrals::ModeCombiIntegrals;

/***************************************************************************//**
 * @brief
 *    Very basic constructor check; no terms, no functions.
 ******************************************************************************/
void ConstructBasic::run()
{
   using func_t = std::function<Nb(Nb)>;
   Uin num_quad_points = I_12;
   std::vector<Nb> scaling_factor;
   std::vector<Nb> c;
   std::vector<std::vector<func_t>> f;
   SumOverProductIndexer<func_t> sop(c, f, [](const func_t&, const func_t&)->bool{return true;});
   std::vector<std::vector<std::pair<Nb,Nb>>> intervals;

   ModeCombiIntegrals<func_t> mci(std::move(sop), intervals, num_quad_points, scaling_factor);

   decltype(mci.NumModes()) ctrl_n_modes = I_0;
   UNIT_ASSERT_EQUAL(mci.NumModes(), ctrl_n_modes, "Wrong num. modes.");
}

} /* namespace modecombiintegrals */

} /* namespace midas::test::pes_integrals */

namespace midas::test
{
void PesIntegralsTest()
{
   cutee::suite suite("PesIntegrals test");
   
   /**************************************************************************************
    * 
    **************************************************************************************/
   using namespace pes_integrals;
   // OneModeIntegrals
   suite.add_test<onemodeintegrals::Constructor>("onemodeintegrals: Constructor");
   suite.add_test<onemodeintegrals::IntervalBeginEndPoints>("onemodeintegrals: IntervalBeginEndPoints");
   suite.add_test<onemodeintegrals::Integrals<std::function<Nb(Nb)>>>("onemodeintegrals: Integrals, std::function");
   suite.add_test<onemodeintegrals::Integrals<GenericFunctionWrapper<Nb>>>("onemodeintegrals: Integrals, GenericFunctionWrapper");
   suite.add_test<onemodeintegrals::IntegralsScaling<std::function<Nb(Nb)>>>("onemodeintegrals: Integrals with scale fact., std::function");
   suite.add_test<onemodeintegrals::IntegralsScaling<GenericFunctionWrapper<Nb>>>("onemodeintegrals: Integrals with scale fact., GenericFunctionWrapper");

   // SumOverProductIndexer
   suite.add_test<sumoverproductindexer::ConstructBasic>("sumoverproductindexer: Constructor, basics");
   suite.add_test<sumoverproductindexer::ConstructAndCheck<std::function<Nb(Nb)>>>("sumoverproductindexer: Constructor, std::function");
   suite.add_test<sumoverproductindexer::ConstructAndCheck<GenericFunctionWrapper<Nb>>>("sumoverproductindexer: Constructor, GenericFunctionWrapper");
   
   // ModeCombiIntegrals
   suite.add_test<modecombiintegrals::ConstructBasic>("modecombiintegrals: Constructor");
   suite.add_test<modecombiintegrals::ConstructAndCheck<std::function<Nb(Nb)>>>("modecombiintegrals: Constructor, std::function");
   suite.add_test<modecombiintegrals::ConstructAndCheck<GenericFunctionWrapper<Nb>>>("modecombiintegrals: Constructor, std::function");

   // run tests
   RunSuite(suite);
}
} /* namespace midas::test */

