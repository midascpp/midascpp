/**
 *******************************************************************************
 * 
 * @file    Random.h
 * @date    14-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Random data for testing.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TEST_RANDOM_H_INCLUDED
#define MIDAS_TEST_RANDOM_H_INCLUDED

#include "test/Random_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "test/Random_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*MIDAS_TEST_RANDOM_H_INCLUDED*/
