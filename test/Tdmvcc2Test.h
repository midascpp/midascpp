/**
 *******************************************************************************
 * 
 * @file    Tdmvcc2Test.h
 * @date    14-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDMVCC2TEST_H_INCLUDED
#define TDMVCC2TEST_H_INCLUDED

#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/trf/TrfVariationalConstraintTdmvcc2.h"
#include "td/tdvcc/trf/TrfVariationalConstraintMatRep.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"
#include "td/tdvcc/deriv/DerivTdmvcc.h"
#include "td/tdvcc/params/ParamsTdmvcc.h"
#include "td/tdvcc/anal/AutoCorr.h"
#include "td/tdvcc/Tdmvcc.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/TdvccIfc.h"
#include "util/StaticLoop.h"
#include "util/RegularizedInverse.h"
#include "util/Timer.h"
#include "mmv/ContainerMathWrappers.h"

namespace midas::test::tdmvcc
{
   namespace detail
   {
      /*********************************************************************//**
       * Base class for testing TDMVCC transformers
       ************************************************************************/
      template
         <  typename T
         >
      class Tdmvcc2TrfTestBase
      {            
         public:
            using param_t = T;
            using absval_t = midas::type_traits::RealTypeT<param_t>;

            //! c-tor
            Tdmvcc2TrfTestBase
               (  const ModeCombiOpRange& arMcr
               ,  const std::vector<Uin>& arNTdModals
               ,  const std::vector<Uin>& arNPrimModals
               ,  const std::vector<Uin>& arNOpers
               )
               :  mNTdModals(arNTdModals)
               ,  mNPrimModals(arNPrimModals)
               ,  mNOpers(arNOpers)
               ,  mMcr(arMcr)
            {
               mSizeTdParams = SetAddresses(mMcr, arNTdModals);   // Modifies mMcr
            }

            //! Get random OpDef
            OpDef RandomOpDef(const ModeCombiOpRange& arOperMcr) const
            {
               const auto coefs = util::GetRandomParams<absval_t>(arOperMcr, NOpers(), false, false, 0);
               return util::GetOpDefFromCoefs(arOperMcr, this->NOpers(), coefs);
            }

            //! Get random ModalIntegrals in primitive basis
            ModalIntegrals<param_t> RandomPrimModInts(const OpDef& arOpDef) const
            {
               return util::GetRandomModalIntegrals<param_t>(arOpDef, this->NPrimModals());
            }

            //! Get random amplitude parameters
            std::vector<std::vector<param_t>> RandomParams(const ModeCombiOpRange& arParamsMcr) const
            {
               return util::GetRandomParams<param_t>(arParamsMcr, this->NTdModals(), true);
            }

         protected:
            //! Get number of modes
            auto NModes() const { return this->mMcr.NumberOfModes(); }

            //! Get number of TD modals
            const auto& NTdModals() const { return this->mNTdModals; }

            //! Get number of prim. modals
            const auto& NPrimModals() const { return this->mNPrimModals; }

            //! Get number of operators
            const auto& NOpers() const { return this->mNOpers; }

            //! Get MCR
            const auto& Mcr() const { return this->mMcr; }

            //! Get number of parameters
            auto SizeTdParams() const { return this->mSizeTdParams; }

         private:
            //! Number of time-dependent modasl
            std::vector<Uin> mNTdModals;

            //! Number of prim. modals
            std::vector<Uin> mNPrimModals;

            //! Number of operators
            std::vector<Uin> mNOpers;

            //! MCR
            ModeCombiOpRange mMcr;

            //! Number of parameters in TD amplitude vector
            Uin mSizeTdParams = 0;

      };

      /*********************************************************************//**
       * Random set of biorthonormal modals
       ************************************************************************/
      template
         <  typename T
         >
      auto RandomModals
         (  const std::vector<Uin>& aNTdModals
         ,  const std::vector<Uin>& aNPrimModals
         )
      {
         using modal_mat_t = GeneralMidasMatrix<T>;
         auto nmodes = aNTdModals.size();
         if (  aNPrimModals.size() != nmodes
            )
         {
            Mout  << " RandomModals called with:\n"
                  << "    n: " << aNTdModals << "\n"
                  << "    N: " << aNPrimModals << "\n"
                  << std::flush;
            MIDASERROR("Vector sizes mismatch!");
         }
         std::vector<std::pair<modal_mat_t,modal_mat_t>> result(nmodes);

         for(In m=I_0; m<nmodes; ++m)
         {               
            const auto& nprim = aNPrimModals[m];
            const auto& ntd = aNTdModals[m];

            // Generate random modals in full basis
            modal_mat_t ufull = random::RandomMidasMatrix<T>(nprim, nprim);
            ufull.NormalizeColumns();
            const modal_mat_t wfull = midas::math::RegularizedInverse(midas::math::inverse::xsvd, ufull, 1.e-20); // Calculate W as inverse of U

            // Extract sub blocks
            modal_mat_t uact(nprim, ntd, T(0));
            modal_mat_t wact(ntd, nprim, T(0));
            for(In i=I_0; i<nprim; ++i)
            {
               for(In j=I_0; j<ntd; ++j)
               {
                  uact[i][j] = ufull[i][j];
                  wact[j][i] = wfull[j][i];
               }
            }

            result[m] = std::make_pair(std::move(uact), std::move(wact));
         }

         return result;
      }

      //! Calculate difference norm of MidasVector and DataCont
      template
         <  typename T
         >
      midas::type_traits::RealTypeT<T> CalcDiffNorm
         (  const GeneralDataCont<T>& aDc
         ,  const GeneralMidasVector<T>& aMv
         )
      {            
         auto size = aDc.Size();
         GeneralMidasVector<T> dc_vec(size);
         aDc.DataIo(IO_GET, dc_vec, size);

         return std::sqrt(DiffNorm2(dc_vec, aMv));
      }
   }; /* namespace detail */

   /*********************************************************************//**
    * Test the test utilities
    ************************************************************************/
   template
      <  typename T
      >
   class TdmvccTestUtilitiesTest
      :  public cutee::test
   {
      public:
         using mat_t = GeneralMidasMatrix<T>;

         //! c-tor
         TdmvccTestUtilitiesTest
            (  const std::vector<Uin>& aNTdModals
            ,  const std::vector<Uin>& aNPrimModals
            )
            :  mNTdModals(aNTdModals)
            ,  mNPrimModals(aNPrimModals)
         {
         }
      
      //! Run test
      void run() override
      {
         auto rand_modals = detail::RandomModals<T>(mNTdModals, mNPrimModals);

         for(const auto& uw : rand_modals)
         {
            auto prod = mat_t(uw.second * uw.first);  // W*U (should be I)
            auto unorm = uw.first.Norm();
            auto wnorm = uw.second.Norm();
            auto size = prod.Nrows();
            for(In i=I_0; i<size; ++i)
            {
               prod[i][i] -= T(1.0);
            }
            auto err = prod.Norm();

            UNIT_ASSERT_FZERO_PREC(err, unorm*wnorm, 10, "Random modals are not biorthonormal.");
         }
      }

      private:
         std::vector<Uin> mNTdModals;
         std::vector<Uin> mNPrimModals;
   };

   /*********************************************************************//**
    * Test H transformer for TDMVCC[2]:
    *    - ErrVec
    *    - EtaVec
    *    - LJac
    *    - MeanFieldMatrices
    *    - DensityMatrices
    ************************************************************************/
   template
      <  typename T
      >
   struct Tdmvcc2HTransformerTest
      :  public detail::Tdmvcc2TrfTestBase<T>
      ,  public cutee::test
   {
      //! Aliases
      using absval_t = midas::type_traits::RealTypeT<T>;
      using trf_t = midas::tdvcc::TrfTdmvcc2<T>;
      using ref_trf_t = midas::tdvcc::TrfTdmvccMatRep<T>;
      using cont_t = GeneralDataCont<T>;
      using ref_cont_t = GeneralMidasVector<T>;

      //! c-tor
      Tdmvcc2HTransformerTest
         (  const ModeCombiOpRange& aMcr
         ,  const std::vector<Uin>& aNTdModals
         ,  const std::vector<Uin>& aNPrimModals
         ,  const std::vector<Uin>& aNOpers
         )
         :  detail::Tdmvcc2TrfTestBase<T>
               (  aMcr
               ,  aNTdModals
               ,  aNPrimModals
               ,  aNOpers
               )
      {
      }

      //! Implementation of run function
      void run
         (
         )  override
      {
         const auto& mcr = this->Mcr();

         // Initialize random parameters
         const auto op = this->RandomOpDef(mcr);
         const auto modints = this->RandomPrimModInts(op);
         const auto t = std::abs(random::RandomNumber<absval_t>());
         auto s_vec = this->RandomParams(mcr);
         auto l_vec = this->RandomParams(mcr);
         const auto modals = detail::RandomModals<T>(this->NTdModals(), this->NPrimModals());

         // Zero one-mode amplitudes
         for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
         {
            Uin i = std::distance(beg, it);
            midas::mmv::WrapZero(s_vec.at(i));
            midas::mmv::WrapZero(l_vec.at(i));
         }

         // Copy random params to appropriate containers
         cont_t s_dc, l_dc;
         util::ConstructParamsVecCont(s_dc, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_dc, mcr, this->SizeTdParams(), l_vec);
         ref_cont_t s_mv, l_mv;
         util::ConstructParamsVecCont(s_mv, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_mv, mcr, this->SizeTdParams(), l_vec);

         // Construct hard-coded TDMVCC[2] transformer and reference (matrep) transformer
         trf_t trf(this->NTdModals(), this->NPrimModals(), op, modints, mcr);
         ref_trf_t ref_trf(this->NTdModals(), this->NPrimModals(), op, modints, mcr);

         // Update time-dependent integrals
         trf.UpdateIntegrals(modals);
         ref_trf.UpdateIntegrals(modals);

         // Check ErrVec
         {               
            auto test = trf.ErrVec(t, s_dc);
            auto ref = ref_trf.ErrVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "ErrVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check EtaVec
         {               
            auto test = trf.EtaVec(t, s_dc);
            auto ref = ref_trf.EtaVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "EtaVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check LJac
         {               
            auto test = trf.LJac(t, s_dc, l_dc);
            auto ref = ref_trf.LJac(t, s_mv, l_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "LJac differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check MeanFieldMatrices
         {
            auto test = trf.MeanFieldMatrices(t, s_dc, l_dc);
            auto ref = ref_trf.MeanFieldMatrices(t, s_mv, l_mv);
            UNIT_ASSERT(test.size() == ref.size(), "NModes mismatch in mean-field matrices.");
            for(In m=I_0; m<test.size(); ++m)
            {
               midas::util::StaticFor<0,4>::Loop
                  (  [&m,&test,&ref](auto I)
                     {
                        const auto& test_i = std::get<I>(test[m]);
                        const auto& ref_i = std::get<I>(ref[m]);
                        auto norm = ref_i.Norm();
                        auto dnorm = std::sqrt(DiffNorm2(test_i, ref_i));
                        UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "Mean-field matrices differ in Tdmvcc2 and TdmvccMatRep (mode " + std::to_string(m) + ", nr. " + std::to_string(I) + ")");
                     }
                  );
            }
         }
         // Check DensityMatrices
         {
            auto test = trf.DensityMatrices(s_dc, l_dc);
            auto ref = ref_trf.DensityMatrices(s_mv, l_mv);
            UNIT_ASSERT(test.size() == ref.size(), "NModes mismatch in density matrices.");
            for(In m=I_0; m<test.size(); ++m)
            {
               auto norm = ref[m].Norm();
               auto dnorm = std::sqrt(DiffNorm2(test[m], ref[m]));
               UNIT_ASSERT_FZERO_PREC(dnorm, norm, 30, "Density matrices differ in Tdmvcc2 and TdmvccMatRep (mode " + std::to_string(m) + ")");
            }
         }
      }
   };

   /*********************************************************************//**
    * Test MeanField transformer for TDMVCC[2] in FullActiveBasis limit
    ************************************************************************/
   template
      <  typename T
      >
   struct Tdmvcc2HMeanFieldFullActiveBasis
      :  public detail::Tdmvcc2TrfTestBase<T>
      ,  public cutee::test
   {
      //! Aliases
      using absval_t = midas::type_traits::RealTypeT<T>;
      using trf_t = midas::tdvcc::TrfTdmvcc2<T>;
      using cont_t = GeneralDataCont<T>;
      using ref_cont_t = GeneralMidasVector<T>;
      using mat_t = GeneralMidasMatrix<T>;

      //! c-tor
      Tdmvcc2HMeanFieldFullActiveBasis
         (  const ModeCombiOpRange& aMcr
         ,  const std::vector<Uin>& aNTdModals
         ,  const std::vector<Uin>& aNPrimModals
         ,  const std::vector<Uin>& aNOpers
         )
         :  detail::Tdmvcc2TrfTestBase<T>
               (  aMcr
               ,  aNTdModals
               ,  aNPrimModals
               ,  aNOpers
               )
      {
         MidasAssert(aNTdModals.size() == aNPrimModals.size(), "Unequal number of modes in td- and prim modals");
         for (In i = I_0; i < aNTdModals.size(); ++i)
         {
            MidasAssert(aNTdModals[i] == aNPrimModals[i], "Number of td modals is not equal to number of prim modals in FullActiveBasis!");
         }
      }

      absval_t DiffNormVirVec(const mat_t& aTestMat, const mat_t& aRefMat)
      {
         ref_cont_t test_vir_vec(aTestMat.Nrows() - 1, T(0));
         ref_cont_t ref_vir_vec(aRefMat.Nrows() - 1, T(0));
         MidasAssert(test_vir_vec.Size() == ref_vir_vec.Size(), "Wrong dimensions for virtual parts of mean field matrices");
         aTestMat.GetOffsetCol(test_vir_vec, 0, I_1);
         aRefMat.GetOffsetCol(ref_vir_vec, 0, I_1);
         return std::sqrt(test_vir_vec.DiffNorm2(ref_vir_vec));
      }

      //! Implementation of run function
      void run
         (
         )  override
      {
         const auto& mcr = this->Mcr();

         // Initialize random parameters
         const auto op = this->RandomOpDef(mcr);
         const auto modints = this->RandomPrimModInts(op);
         const auto t = std::abs(random::RandomNumber<absval_t>());
         auto s_vec = this->RandomParams(mcr);
         auto l_vec = this->RandomParams(mcr);
         const auto modals = detail::RandomModals<T>(this->NTdModals(), this->NPrimModals());

         // Zero one-mode amplitudes
         for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
         {
            Uin i = std::distance(beg, it);
            midas::mmv::WrapZero(s_vec.at(i));
            midas::mmv::WrapZero(l_vec.at(i));
         }

         // Copy random params to appropriate containers
         cont_t s_dc, l_dc;
         util::ConstructParamsVecCont(s_dc, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_dc, mcr, this->SizeTdParams(), l_vec);
         ref_cont_t s_mv, l_mv;
         util::ConstructParamsVecCont(s_mv, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_mv, mcr, this->SizeTdParams(), l_vec);

         // Construct TDMVCC[2] transformer for both test and reference
         trf_t trf(this->NTdModals(), this->NPrimModals(), op, modints, mcr);
         trf_t ref_trf(this->NTdModals(), this->NPrimModals(), op, modints, mcr);

         // Update time-dependent integrals
         trf.UpdateIntegrals(modals);
         ref_trf.UpdateIntegrals(modals);
         
         // It is strictly not necessary to test ErrVec and LJac again, but the intermediate containers are initialized by doing these
         // So we get a segmentation fault if either not calling ErrVec and LJac or manually initializing the containers here.
         // So easiest way is just to call ErrVec and LJac again.
         // Check ErrVec
         {               
            auto test = trf.ErrVec(t, s_dc);
            auto ref = ref_trf.ErrVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = std::sqrt(DiffNorm2(test, ref));
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "ErrVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check EtaVec
         {               
            auto test = trf.EtaVec(t, s_dc);
            auto ref = ref_trf.EtaVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = std::sqrt(DiffNorm2(test, ref));
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "EtaVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check LJac
         {               
            auto test = trf.LJac(t, s_dc, l_dc);
            auto ref = ref_trf.LJac(t, s_mv, l_mv);
            auto norm = Norm(ref);
            auto dnorm = std::sqrt(DiffNorm2(test, ref));
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "LJac differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check MeanFieldMatrices for the FullActiveBasis option
         {
            auto test = trf.MeanFieldMatricesFullActiveBasis(t, s_dc, l_dc);
            auto ref = ref_trf.MeanFieldMatrices(t, s_mv, l_mv);
            UNIT_ASSERT(test.size() == ref.size(), "NModes mismatch in mean-field matrices.");
            for(In m=I_0; m<test.size(); ++m)
            {
               const auto& u = modals[m].first;
               const auto& w = modals[m].second;

               auto f_norm = test[m][1].Norm();
               auto f_dnorm = DiffNormVirVec(test[m][1], mat_t(w * ref[m][1]));
               UNIT_ASSERT_FZERO_PREC(f_dnorm, f_norm, 100, "Mean-field matrices F_tilde (FullActiveBasis) differ in Tdmvcc2 and TdmvccMatRep (mode " + std::to_string(m) + ")");

               auto fp_norm = test[m][3].Norm();
               auto fp_dnorm = DiffNormVirVec(test[m][3], mat_t(ref[m][3] * u));
               UNIT_ASSERT_FZERO_PREC(fp_dnorm, fp_norm, 100, "Mean-field matrices Fp_tilde (FullActiveBasis) differ in Tdmvcc2 and TdmvccMatRep (mode " + std::to_string(m) + ")");
            }
         }
      }
   };

   /*********************************************************************//**
    * Test g transformer for TDMVCC[2]:
    *    - ErrVec             (with random g matrices)
    *    - EtaVec             (with random g matrices)
    *    - LJac               (with random g matrices)
    *    - UVector            (NB. needs correct H trans. first - different intermeds for the two transformers)
    *    - ConstraintMatrices (NB. needs correct H trans. first - different intermeds for the two transformers) 
    ************************************************************************/
   template
      <  typename T
      >
   struct Tdmvcc2GTransformerTest
      :  public detail::Tdmvcc2TrfTestBase<T>
      ,  public cutee::test
   {
      //! Aliases
      using absval_t = midas::type_traits::RealTypeT<T>;
      using trf_t = midas::tdvcc::TrfVariationalConstraintTdmvcc2<T>;
      using ref_trf_t = midas::tdvcc::TrfVariationalConstraintMatRep<T>;
      using cont_t = GeneralDataCont<T>;
      using ref_cont_t = GeneralMidasVector<T>;

      //! c-tor
      Tdmvcc2GTransformerTest
         (  const ModeCombiOpRange& aMcr
         ,  const std::vector<Uin>& aNTdModals
         ,  const std::vector<Uin>& aNPrimModals
         ,  const std::vector<Uin>& aNOpers
         )
         :  detail::Tdmvcc2TrfTestBase<T>
               (  aMcr
               ,  aNTdModals
               ,  aNPrimModals
               ,  aNOpers
               )
      {
      }

      //! Implementation of run function
      void run
         (
         )  override
      {
         const auto& mcr = this->Mcr();
         const auto nmodes = mcr.NumberOfModes();
         const auto t = std::abs(random::RandomNumber<absval_t>());
         auto s_vec = this->RandomParams(mcr);
         auto l_vec = this->RandomParams(mcr);

         // Zero one-mode amplitudes
         for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
         {
            Uin i = std::distance(beg, it);
            midas::mmv::WrapZero(s_vec.at(i));
            midas::mmv::WrapZero(l_vec.at(i));
         }

         typename trf_t::gmats_t rand_gmats;
         for(In m=I_0; m<nmodes; ++m)
         {
            const auto& n = this->NTdModals()[m];
            rand_gmats.emplace_back(random::RandomMidasMatrix<T>(n,n));
         }

         // Copy random params to appropriate containers
         cont_t s_dc, l_dc;
         util::ConstructParamsVecCont(s_dc, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_dc, mcr, this->SizeTdParams(), l_vec);
         ref_cont_t s_mv, l_mv;
         util::ConstructParamsVecCont(s_mv, mcr, this->SizeTdParams(), s_vec);
         util::ConstructParamsVecCont(l_mv, mcr, this->SizeTdParams(), l_vec);

         // Construct hard-coded TDMVCC[2] transformer and reference (matrep) transformer
         trf_t trf(this->NTdModals(), mcr);
         ref_trf_t ref_trf(this->NTdModals(), mcr);

         // Update g integrals using random matrices
         trf.UpdateIntegrals(rand_gmats);
         ref_trf.UpdateIntegrals(rand_gmats);

         // Check ErrVec
         {               
            auto test = trf.ErrVec(t, s_dc);
            auto ref = ref_trf.ErrVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "g-operator ErrVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check EtaVec
         {               
            auto test = trf.EtaVec(t, s_dc);
            auto ref = ref_trf.EtaVec(t, s_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "g-operator EtaVec differs for Tdmvcc2 and TdmvccMatRep");
         }
         // Check LJac
         {               
            auto test = trf.LJac(t, s_dc, l_dc);
            auto ref = ref_trf.LJac(t, s_mv, l_mv);
            auto norm = Norm(ref);
            auto dnorm = detail::CalcDiffNorm(test, ref);
            UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "g-operator LJac differs for Tdmvcc2 and TdmvccMatRep");
         }
      }
   };

   /**
    * @brief   Test calculation of full TDMVCC[2] time derivative using random parameters.
    *          This also involves correct construction of g matrices.
    */
   template
      <  typename T
      >
   class Tdmvcc2DerivativeTest
      :  public detail::Tdmvcc2TrfTestBase<T>
      ,  public cutee::test
   {
      public:
         //! Alias
         using typename detail::Tdmvcc2TrfTestBase<T>::param_t;
         using typename detail::Tdmvcc2TrfTestBase<T>::absval_t;
         inline constexpr static bool real_params = !midas::type_traits::IsComplexV<param_t>;

         using cont_t = GeneralDataCont<param_t>;
         using param_cont_t = midas::tdvcc::ParamsTdmvcc<param_t, GeneralDataCont>;
         using deriv_t = midas::tdvcc::DerivTdmvcc<param_t, GeneralDataCont, midas::tdvcc::TrfTdmvcc2, midas::tdvcc::TrfVariationalConstraintTdmvcc2, real_params>;
         using eom_t = midas::tdvcc::Tdmvcc<param_cont_t, midas::tdvcc::TrfTdmvcc2<param_t>, midas::tdvcc::TrfVariationalConstraintTdmvcc2<param_t>, deriv_t>;

         using ref_cont_t = GeneralMidasVector<param_t>;
         using ref_param_cont_t = midas::tdvcc::ParamsTdmvcc<param_t, GeneralMidasVector>;
         using ref_deriv_t = midas::tdvcc::DerivTdmvcc<param_t, GeneralMidasVector, midas::tdvcc::TrfTdmvccMatRep, midas::tdvcc::TrfVariationalConstraintMatRep, real_params>;
         using ref_eom_t = midas::tdvcc::Tdmvcc<ref_param_cont_t, midas::tdvcc::TrfTdmvccMatRep<param_t>, midas::tdvcc::TrfVariationalConstraintMatRep<param_t>, ref_deriv_t>;

         //! c-tor
         template<typename... Args>
         Tdmvcc2DerivativeTest(Args&&... args) : detail::Tdmvcc2TrfTestBase<T>(std::forward<Args>(args)...) {}

         //! Run test
         void run() override
         {
            const auto& mcr = this->Mcr();

            // Initialize random parameters
            const auto op = this->RandomOpDef(mcr);
            const auto modints = this->RandomPrimModInts(op);
            const auto t = std::abs(random::RandomNumber<absval_t>());
            auto s_vec = this->RandomParams(mcr);
            auto l_vec = this->RandomParams(mcr);
            const auto modals = detail::RandomModals<T>(this->NTdModals(), this->NPrimModals());
            const auto phase = random::RandomNumber<T>();

            // Zero one-mode amplitudes
            for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
            {
               Uin i = std::distance(beg, it);
               midas::mmv::WrapZero(s_vec.at(i));
               midas::mmv::WrapZero(l_vec.at(i));
            }

            // Copy random params to appropriate containers
            cont_t s_dc, l_dc;
            util::ConstructParamsVecCont(s_dc, mcr, this->SizeTdParams(), s_vec);
            util::ConstructParamsVecCont(l_dc, mcr, this->SizeTdParams(), l_vec);
            ref_cont_t s_mv, l_mv;
            util::ConstructParamsVecCont(s_mv, mcr, this->SizeTdParams(), s_vec);
            util::ConstructParamsVecCont(l_mv, mcr, this->SizeTdParams(), l_vec);

            // Make param containers
            const param_cont_t params(modals, s_dc, l_dc, phase);
            const ref_param_cont_t ref_params(modals, s_mv, l_mv, phase);

            // Set up EOMs
            eom_t eom(this->NPrimModals(), this->NTdModals(), op, modints, mcr);
            eom.InitializeTransformers();
            eom.InitializeSizeParams(this->NTdModals());
            ref_eom_t ref_eom(this->NPrimModals(), this->NTdModals(), op, modints, mcr);
            ref_eom.InitializeTransformers();
            ref_eom.InitializeSizeParams(this->NTdModals());

            // Calculate derivatives
            const auto deriv = eom.Derivative(t, params);
            const auto ref_deriv = ref_eom.Derivative(t, ref_params);

            // Check modal derivative
            {
               const auto& test = deriv.ModalCont();
               const auto& ref = ref_deriv.ModalCont();
               UNIT_ASSERT(test.size() == ref.size(), "Modal derivatives have different sizes.");
               for(In m=I_0; m<this->NModes(); ++m)
               {
                  const auto& du = test[m].first;
                  const auto& dw = test[m].second;
                  const auto& ref_du = ref[m].first;
                  const auto& ref_dw = ref[m].second;
                  const auto unorm = ref_du.Norm();
                  const auto wnorm = ref_dw.Norm();
                  const auto uerr = std::sqrt(DiffNorm2(du, ref_du));
                  const auto werr = std::sqrt(DiffNorm2(dw, ref_dw));
                  UNIT_ASSERT_FZERO_PREC(uerr, unorm, 2000, "dU/dt differs.");
                  UNIT_ASSERT_FZERO_PREC(werr, wnorm, 2000, "dW/dt differs.");
               }
            }
            // Check s derivative
            {
               const auto& test = deriv.ClusterAmps();
               const auto& ref = ref_deriv.ClusterAmps();
               auto norm = Norm(ref);
               auto dnorm = detail::CalcDiffNorm(test, ref);
               UNIT_ASSERT_FZERO_PREC(dnorm, norm, 10, "ds/dt differs.");
            }
            // Check l derivative
            {
               const auto& test = deriv.LambdaCoefs();
               const auto& ref = ref_deriv.LambdaCoefs();
               auto norm = Norm(ref);
               auto dnorm = detail::CalcDiffNorm(test, ref);
               UNIT_ASSERT_FZERO_PREC(dnorm, norm, 20, "dl/dt differs.");
            }
            // Check phase derivative
            {
               const auto test = deriv.Phase();
               const auto ref = ref_deriv.Phase();
               UNIT_ASSERT_FEQUAL_PREC(std::real(test), std::real(ref), 400, "d eps/dt (real part) differs.");
               UNIT_ASSERT_FEQUAL_PREC(std::imag(test), std::imag(ref), 400, "d eps/dt (imag part) differs.");
            }
         }
   };

   /**
    * @brief   Test calculation of TDMVCC[2] autocorrelation function
    */
   template
      <  typename T
      >
   class Tdmvcc2AutoCorrTest
      :  public detail::Tdmvcc2TrfTestBase<T>
      ,  public cutee::test
   {
      public:
         //! Alias
         using typename detail::Tdmvcc2TrfTestBase<T>::param_t;
         using typename detail::Tdmvcc2TrfTestBase<T>::absval_t;
         using settings_t = typename midas::tdvcc::TdvccIfc::ifc_autocorr_t;
         inline constexpr static bool real_params = !midas::type_traits::IsComplexV<param_t>;

         using cont_t = GeneralDataCont<param_t>;
         using param_cont_t = midas::tdvcc::ParamsTdmvcc<param_t, GeneralDataCont>;

         using ref_cont_t = GeneralMidasVector<param_t>;
         using ref_param_cont_t = midas::tdvcc::ParamsTdmvcc<param_t, GeneralMidasVector>;
         
         //! c-tor
         template<typename... Args>
         Tdmvcc2AutoCorrTest(Args&&... args) : detail::Tdmvcc2TrfTestBase<T>(std::forward<Args>(args)...) {}

         //! Run test
         void run() override
         {
            const auto& mcr = this->Mcr();
            
            // Initial state type
            midas::tdvcc::InitType init_type = midas::tdvcc::InitType::VCCGS;
            settings_t settings = midas::tdvcc::AutoCorrSettings();
            settings.mCalcTypeA = true;
            settings.mCalcTypeB = true;

            // Initialize random parameters (for two different states a and b)
            auto s_vec_a = this->RandomParams(mcr);
            auto s_vec_b = this->RandomParams(mcr);
            auto l_vec_a = this->RandomParams(mcr);
            auto l_vec_b = this->RandomParams(mcr);
            const auto modals_a = detail::RandomModals<T>(this->NTdModals(), this->NPrimModals());
            const auto modals_b = detail::RandomModals<T>(this->NTdModals(), this->NPrimModals());
            const auto phase_a = random::RandomNumber<T>();
            const auto phase_b = random::RandomNumber<T>();

            // Zero one-mode amplitudes
            for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
            {
               Uin i = std::distance(beg, it);
               midas::mmv::WrapZero(s_vec_a.at(i));
               midas::mmv::WrapZero(s_vec_b.at(i));
               midas::mmv::WrapZero(l_vec_a.at(i));
               midas::mmv::WrapZero(l_vec_b.at(i));
            }

            // Copy random params to appropriate containers
            cont_t s_dc_a, s_dc_b, l_dc_a, l_dc_b;
            util::ConstructParamsVecCont(s_dc_a, mcr, this->SizeTdParams(), s_vec_a);
            util::ConstructParamsVecCont(s_dc_b, mcr, this->SizeTdParams(), s_vec_b);
            util::ConstructParamsVecCont(l_dc_a, mcr, this->SizeTdParams(), l_vec_a);
            util::ConstructParamsVecCont(l_dc_b, mcr, this->SizeTdParams(), l_vec_b);
            ref_cont_t s_mv_a, s_mv_b, l_mv_a, l_mv_b;
            util::ConstructParamsVecCont(s_mv_a, mcr, this->SizeTdParams(), s_vec_a);
            util::ConstructParamsVecCont(s_mv_b, mcr, this->SizeTdParams(), s_vec_b);
            util::ConstructParamsVecCont(l_mv_a, mcr, this->SizeTdParams(), l_vec_a);
            util::ConstructParamsVecCont(l_mv_b, mcr, this->SizeTdParams(), l_vec_b);

            // Make param containers
            const param_cont_t params_a(modals_a, s_dc_a, l_dc_a, phase_a);
            const param_cont_t params_b(modals_b, s_dc_b, l_dc_b, phase_b);
            const ref_param_cont_t ref_params_a(modals_a, s_mv_a, l_mv_a, phase_a);
            const ref_param_cont_t ref_params_b(modals_b, s_mv_b, l_mv_b, phase_b);

            // Calculate overlaps
            const Uin iolevel = 0;
            const auto [test_corrA, test_corrB] = AutoCorrTdmvcc2(params_a, params_b, this->NTdModals(), this->Mcr(), init_type, settings, iolevel);
            const auto [ref_corrA, ref_corrB]   = AutoCorr(ref_params_a, ref_params_b, this->NTdModals(), this->Mcr());
   

            // Check type A autocorrelation function
            {
               const auto& test = test_corrA;
               const auto& ref = ref_corrA;
               UNIT_ASSERT_FEQUAL_PREC(std::real(test), std::real(ref), 450, "Real parts of type A autocorrelation functions differ!");
               UNIT_ASSERT_FEQUAL_PREC(std::imag(test), std::imag(ref), 450, "Imaginary parts of type A autocorrelation functions differ!");
            }

            // Check type B autocorrelation function
            {
               const auto& test = test_corrB;
               const auto& ref = ref_corrB;
               UNIT_ASSERT_FEQUAL_PREC(std::real(test), std::real(ref), 450, "Real parts of type B autocorrelation functions differ!");
               UNIT_ASSERT_FEQUAL_PREC(std::imag(test), std::imag(ref), 450, "Imaginary parts of type B autocorrelation functions differ!");
            }

         } /* end of run() */

   }; /* end of Tdmvcc2AutoCorrTest */


}; /* namespace midas::test::tdmvcc */

#endif /* TDMVCC2TEST_H_INCLUDED */
