/**
 *******************************************************************************
 * 
 * @file    SubspaceSolverTest.h
 * @date    12-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef SUBSPACESOLVERTEST_H_INCLUDED
#define SUBSPACESOLVERTEST_H_INCLUDED

#include "test/CuteeInterface.h"
#include "util/type_traits/Complex.h"
#include "mmv/ContainerMathWrappers.h"
#include "test/Random.h"

#include "mmv/MidasVector.h"

#include <vector>
#include <array>

#define MAXIT 50
#define THRESH 1.e-14

namespace midas::test
{
namespace detail
{

//! Non-linear function returning std::vector
#define A 1.e4
std::vector<Nb> TestFunction1(const std::vector<Nb>& aVec)
{
   assert(aVec.size() == 2);
   std::vector<Nb> result(2);

   result[0] = A*aVec[0]*aVec[1] - 1.0;
   result[1] = std::exp(-aVec[0]) + std::exp(-aVec[1]) - 1.0 - 1./A;

   return result;
}
std::vector<Nb> GetPrecon1()
{
   std::vector<Nb> inv_jac =
   {  Nb(1.0/(9.0*A))
   ,  Nb(-1.0)
   };

   return inv_jac;
}
#undef A

//! Non-linear function returning std::array<Nb,2>
std::array<Nb,2> TestFunction2(const std::array<Nb,2>& aVec)
{
   std::array<Nb,2> result;
   result[0] = -2.0*(1.0 - aVec[0]) - 4.e2*aVec[0]*(aVec[1] - aVec[0]*aVec[0]);
   result[1] = 2.e2*(aVec[1] - aVec[0]*aVec[0]);
   return result;
}
std::array<Nb,2> GetPrecon2()
{
   std::array<Nb,2> inv_jac;
   inv_jac[0] = -1./398.;
   inv_jac[1] = 1./200.;
   return inv_jac;
}

//! Functor as test function
struct TestFunctor
{
   using vec_t = std::array<Nb, 2>;

   vec_t operator()(const vec_t& aVec) const
   {
      const auto& x = aVec[0];
      const auto& y = aVec[1];

      vec_t result;
      result[0] = (x*x + y - 11.0)*4.0*x + 2.0*(x + y*y - 7.0);
      result[1] = 2.0*(x*x + y - 11.0) + 4.0*y*(x + y*y - 7.0);
      return result;
   }

   static constexpr vec_t GetPrecon()
   {
      return vec_t{1./74., 1./34.};
   }
};

//! Get a lambda function
#define K 0.1
auto GetTestLambda()
{
   auto l = [](const MidasVector& v) -> MidasVector
   {
      assert(v.Size() == 2);
      const auto& x = v[0];
      const auto& y = v[1];
      MidasVector result(2);
      result[0] = x + 2*K*x*y;
      result[1] = y + K*x*x - K*y*y;
      return result;
   };

   return l;
}
MidasVector GetPrecon4()
{
   MidasVector inv_jac(2);
   inv_jac[0] = 1.0;
   inv_jac[1] = 1.0;
   return inv_jac;
}
#undef K

//! Linear equations (Hermitian)
std::array<Nb, 4> LinearHermitianFunction
   (  const std::array<Nb, 4>& aVec
   )
{
   std::array<Nb, 4> result;
   result[0] = aVec[0]*1.00 + aVec[1]*0.50 + aVec[2]*0.25 + aVec[3]*0.15 - 0.500;
   result[1] = aVec[0]*0.50 + aVec[1]*2.00 + aVec[2]*0.75 + aVec[3]*0.55 - 0.750;
   result[2] = aVec[0]*0.25 + aVec[1]*0.75 + aVec[2]*3.00 + aVec[3]*0.60 + 0.500;
   result[3] = aVec[0]*0.15 + aVec[1]*0.55 + aVec[2]*0.60 + aVec[3]*4.00 + 0.250;
   return result;
}
std::array<Nb, 4> LinearHermitianPrecon
   (
   )
{
   std::array<Nb, 4> result;
   result[0] = 1.00;
   result[1] = 1./2.00;
   result[2] = 1./3.00;
   result[3] = 1./4.00;
   return result;
}

//! Functor as test function
struct TestFunctor2
{
   using vec_t = std::tuple<Nb,Nb,std::array<Nb, 2>>;

   vec_t operator()(const vec_t& aVec) const
   {
      const auto& x0 = std::get<0>(aVec);
      const auto& x1 = std::get<1>(aVec);
      const auto& x2 = std::get<2>(aVec)[0];
      const auto& x3 = std::get<2>(aVec)[1];

      std::array<Nb,2> arr;
      Nb r0    = 1.00*x0 + 0.50*x1 + 0.25*x2 + 0.15*x3 - 0.500 - 1.e-5*x1*x2;
      Nb r1    = 0.50*x0 + 2.00*x1 + 0.75*x2 + 0.55*x3 - 0.750 + 3.e-4*x2*x3;
      arr[0]   = 0.25*x0 + 0.75*x1 + 3.00*x2 + 0.60*x3 + 0.500 + 2.e-4*x3*x0;
      arr[1]   = 0.15*x0 + 0.55*x1 + 0.60*x2 + 4.00*x3 + 0.250 + 5.e-4*x0*x1;
      return std::make_tuple(r0, r1, arr);
   }

   static constexpr vec_t GetPrecon()
   {
      return std::make_tuple(1.00, 1./2.00, std::array<Nb,2>{1./3.00, 1./4.00});
   }
};
} /* namespace detail */

/**
 * Test for non-preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct NoPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      solver_tmpl<decltype(&detail::TestFunction1)> sol1(detail::TestFunction1, DIM, MAXIT, THRESH);
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::vector<Nb>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1(2);
      v1[1] = 8.0;
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for TestFunction1");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::TestFunction1(v1)), THRESH), "Solution is not accurate enough for TestFunction1");

      // Test using TestFunction2
      solver_tmpl<decltype(&detail::TestFunction2)> sol2(detail::TestFunction2, DIM, MAXIT, THRESH);
      using vec_t2 = typename decltype(sol2)::vec_t;
      constexpr bool correct_vect2 = std::is_same_v<vec_t2, std::array<Nb,2>>;
      static_assert(correct_vect2, "Wrong deduction of vec_t");
      vec_t2 v2;
      v2[0] = 0.9;
      v2[1] = 0.8;
      bool conv2 = sol2.Solve(v2);
      UNIT_ASSERT(conv2, "Solver not converged for TestFunction2");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::TestFunction2(v2)), THRESH), "Solution is not accurate enough for TestFunction2");

      // Test using TestFunctor
      detail::TestFunctor tft3;
      solver_tmpl<detail::TestFunctor> sol3(tft3, DIM, MAXIT, 2.5*THRESH);
      using vec_t3 = typename decltype(sol3)::vec_t;
      constexpr bool correct_vect3 = std::is_same_v<vec_t3, std::array<Nb,2>>;
      static_assert(correct_vect3, "Wrong deduction of vec_t");
      vec_t3 v3;
      v3[0] = 0.0;
      v3[1] = 0.0;
      bool conv3 = sol3.Solve(v3);
      UNIT_ASSERT(conv3, "Solver not converged for TestFunctor");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(tft3(v3)), 2.5*THRESH), "Solution is not accurate enough for TestFunctor");

      // Test using lambda function
      auto l4 = detail::GetTestLambda();
      solver_tmpl<decltype(detail::GetTestLambda())> sol4(l4, DIM, MAXIT, THRESH);
      using vec_t4 = typename decltype(sol4)::vec_t;
      constexpr bool correct_vect4 = std::is_same_v<vec_t4, MidasVector>;
      static_assert(correct_vect4, "Wrong deduction of vec_t");
      vec_t4 v4(2, 0.1);
      bool conv4 = sol4.Solve(v4);
      UNIT_ASSERT(conv4, "Solver not converged for TestLambda");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(l4(v4)), THRESH), "Solution is not accurate enough for TestLambda");
   }
};

/**
 * Test for diagonally preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct DiagPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      solver_tmpl<decltype(&detail::TestFunction1)> sol1(detail::TestFunction1, DIM, MAXIT, THRESH, detail::GetPrecon1());
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::vector<Nb>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1(2);
      v1[1] = 8.0;
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for TestFunction1");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::TestFunction1(v1)), THRESH), "Solution is not accurate enough for TestFunction1");

      // Test using TestFunction2
      solver_tmpl<decltype(&detail::TestFunction2)> sol2(detail::TestFunction2, DIM, MAXIT, THRESH, detail::GetPrecon2());
      using vec_t2 = typename decltype(sol2)::vec_t;
      constexpr bool correct_vect2 = std::is_same_v<vec_t2, std::array<Nb,2>>;
      static_assert(correct_vect2, "Wrong deduction of vec_t");
      vec_t2 v2;
      v2[0] = 0.9;
      v2[1] = 0.8;
      bool conv2 = sol2.Solve(v2);
      UNIT_ASSERT(conv2, "Solver not converged for TestFunction2");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::TestFunction2(v2)), THRESH), "Solution is not accurate enough for TestFunction2");

      // Test using TestFunctor
      detail::TestFunctor tft3;
      solver_tmpl<detail::TestFunctor> sol3(tft3, DIM, MAXIT, THRESH, detail::TestFunctor::GetPrecon());
      using vec_t3 = typename decltype(sol3)::vec_t;
      constexpr bool correct_vect3 = std::is_same_v<vec_t3, std::array<Nb,2>>;
      static_assert(correct_vect3, "Wrong deduction of vec_t");
      vec_t3 v3;
      v3[0] = 0.0;
      v3[1] = 0.0;
      bool conv3 = sol3.Solve(v3);
      UNIT_ASSERT(conv3, "Solver not converged for TestFunctor");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(tft3(v3)), THRESH), "Solution is not accurate enough for TestFunctor");

      // Test using lambda function
      auto l4 = detail::GetTestLambda();
      solver_tmpl<decltype(detail::GetTestLambda())> sol4(l4, DIM, MAXIT, THRESH, detail::GetPrecon4());
      using vec_t4 = typename decltype(sol4)::vec_t;
      constexpr bool correct_vect4 = std::is_same_v<vec_t4, MidasVector>;
      static_assert(correct_vect4, "Wrong deduction of vec_t");
      vec_t4 v4(2, 0.1);
      bool conv4 = sol4.Solve(v4);
      UNIT_ASSERT(conv4, "Solver not converged for TestLambda");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(l4(v4)), THRESH), "Solution is not accurate enough for TestLambda");
   }
};

/**
 * Test for non-preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct LinearNoPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      solver_tmpl<decltype(&detail::LinearHermitianFunction)> sol1(detail::LinearHermitianFunction, DIM, MAXIT, THRESH);
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::array<Nb, 4>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1;
      v1[0] = 0.0;
      v1[1] = 0.0;
      v1[2] = 0.0;
      v1[3] = 0.0;
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for LinearHermitianFunction");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::LinearHermitianFunction(v1)), THRESH), "Solution is not accurate enough for LinearHermitianFunction");
   }
};

/**
 * Test for non-preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct LinearDiagPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      solver_tmpl<decltype(&detail::LinearHermitianFunction)> sol1(detail::LinearHermitianFunction, DIM, MAXIT, THRESH, detail::LinearHermitianPrecon());
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::array<Nb, 4>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1;
      v1[0] = 0.0;
      v1[1] = 0.0;
      v1[2] = 0.0;
      v1[3] = 0.0;
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for LinearHermitianFunction");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(detail::LinearHermitianFunction(v1)), THRESH), "Solution is not accurate enough for LinearHermitianFunction");
   }
};

/**
 * Test for non-preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct QuasiLinearNoPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      detail::TestFunctor2 tft2;
      solver_tmpl<detail::TestFunctor2> sol1(tft2, DIM, MAXIT, THRESH);
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::tuple<Nb, Nb, std::array<Nb, 2>>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1 = std::make_tuple(C_0, C_0, std::array<Nb, 2>{C_0, C_0});
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for TestFunctor2");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(tft2(v1)), THRESH), "Solution is not accurate enough for TestFunctor2");
   }
};

/**
 * Test for non-preconditioned subspace solvers
 **/
template
   <  template<typename> typename SOLVER_TMPL
   ,  In DIM
   >
struct QuasiLinearDiagPreconSubspaceSolverTest
   :  public cutee::test
{
   template<typename F> using solver_tmpl = SOLVER_TMPL<F>;

   void run
      (
      )  override
   {
      // Test using TestFunction1
      detail::TestFunctor2 tft2;
      solver_tmpl<detail::TestFunctor2> sol1(tft2, DIM, MAXIT, THRESH, detail::TestFunctor2::GetPrecon());
      using vec_t1 = typename decltype(sol1)::vec_t;
      constexpr bool correct_vect1 = std::is_same_v<vec_t1, std::tuple<Nb, Nb, std::array<Nb, 2>>>;
      static_assert(correct_vect1, "Wrong deduction of vec_t");
      vec_t1 v1 = std::make_tuple(C_0, C_0, std::array<Nb, 2>{C_0, C_0});
      bool conv1 = sol1.Solve(v1);
      UNIT_ASSERT(conv1, "Solver not converged for TestFunctor2");
      UNIT_ASSERT(libmda::numeric::float_lt(midas::mmv::WrapNorm(tft2(v1)), THRESH), "Solution is not accurate enough for TestFunctor2");
   }
};

} /* namespace midas::test */

#undef MAXIT
#undef THRESH

#endif /* SUBSPACESOLVERTEST_H_INCLUDED */
