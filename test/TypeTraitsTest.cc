/**
 *******************************************************************************
 * 
 * @file    TypeTraitsTest.cc
 * @date    13-07-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for midas::type_traits.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/TypeTraitsTest.h"

// Declarations.
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;
template<typename> class GeneralDataCont;

namespace midas::test
{

void TypeTraitsTest()
{
   cutee::suite suite("TypeTraits test");
   
   /**************************************************************************************
    * 
    **************************************************************************************/
   using namespace type_traits;
   suite.add_test<Complex>("Complex");

   // TypeName<A>() tests, A being a class template.
   suite.add_test<TypeNameTmplTest<std::complex>>("TypeName<std::complex>", "std::complex");
   suite.add_test<TypeNameTmplTest<std::vector>>("TypeName<std::vector>", "std::vector");
   suite.add_test<TypeNameTmplTest<GeneralMidasVector>>("TypeName<GeneralMidasVector>", "GeneralMidasVector");
   suite.add_test<TypeNameTmplTest<GeneralMidasMatrix>>("TypeName<GeneralMidasMatrix>", "GeneralMidasMatrix");
   suite.add_test<TypeNameTmplTest<GeneralDataCont>>("TypeName<GeneralDataCont>", "GeneralDataCont");

   // TypeName<T>(), T being a type.
   suite.add_test<TypeNameTest<In>>("TypeName<In>", "In");
   suite.add_test<TypeNameTest<Uin>>("TypeName<Uin>", "Uin");
   suite.add_test<TypeNameTest<Nb>>("TypeName<Nb>", "Nb");
   suite.add_test<TypeNameTest<std::complex<Nb>>>("TypeName<std::complex<Nb>>", "std::complex<Nb>");
   suite.add_test<TypeNameTest<GeneralMidasVector<Nb>>>("TypeName<GeneralMidasVector<Nb>>", "GeneralMidasVector<Nb>");
   suite.add_test<TypeNameTest<GeneralMidasMatrix<Nb>>>("TypeName<GeneralMidasMatrix<Nb>>", "GeneralMidasMatrix<Nb>");
   suite.add_test<TypeNameTest<GeneralDataCont<Nb>>>("TypeName<GeneralDataCont<Nb>>", "GeneralDataCont<Nb>");
   suite.add_test<TypeNameTest<GeneralMidasVector<std::complex<Nb>>>>("TypeName<GeneralMidasVector<std::complex<Nb>>>", "GeneralMidasVector<std::complex<Nb>>");
   suite.add_test<TypeNameTest<std::vector<Nb>>>("TypeName<std::vector<Nb>>", "std::vector<Nb>");

   // TypeName(const T&), T being a type.
   suite.add_test<TypeNameVarTest<In>>("TypeName(const In&)", "In");
   suite.add_test<TypeNameVarTest<Uin>>("TypeName(const Uin&)", "Uin");
   suite.add_test<TypeNameVarTest<Nb>>("TypeName(const Nb&)", "Nb");
   suite.add_test<TypeNameVarTest<std::complex<Nb>>>("TypeName(const std::complex<Nb>&)", "std::complex<Nb>");
   suite.add_test<TypeNameVarTest<std::vector<Nb>>>("TypeName(const std::vector<Nb>&)", "std::vector<Nb>");

   // run tests
   RunSuite(suite);
}
} /* namespace midas::test */
