/**
************************************************************************
* 
* @file                MidasExpressionTest.h
*
* Created:             30-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Test case for turning midas expressions into vectors
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASEXPRESSIONTEST_H
#define MIDASEXPRESSIONTEST_H

#include <string>
#include <vector>

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"

struct ExpandToVectorTest : public cutee::test
{
   void run() 
   {
      std::vector<std::string> real_result_1;
      std::string test_string_1("EXPAND($i,$i,1 2 3 4)");
      vector<In> test1_real_val;
      for(In i = 1; i < 5; ++i)
         test1_real_val.push_back(i);
      vector<In> test1_conv_val = midas::util::VectorFromString<In>(test_string_1);
      UNIT_ASSERT(test1_conv_val.size() == test1_real_val.size(), 
                                             "Test failed to convert \""+test_string_1+"\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test1_conv_val.size())+
                                             " should be : "+std::to_string(test1_real_val.size()) );
      if(test1_conv_val.size() == test1_real_val.size())
         for(In i = 0; i < test1_real_val.size(); ++i)
         {
            UNIT_ASSERT(test1_conv_val[i] == test1_real_val[i], "Test failed to convert \""+test_string_1+"\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test1_conv_val[i])+
                                                                "Should be : "+std::to_string(test1_real_val[i]));
         }
      
      vector<string> test2_real_val;
      test2_real_val.emplace_back("1+1");
      test2_real_val.emplace_back("2+1");
      test2_real_val.emplace_back("3+1");
      test2_real_val.emplace_back("4+1");
      test2_real_val.emplace_back("1+2");
      test2_real_val.emplace_back("2+2");
      test2_real_val.emplace_back("3+2");
      test2_real_val.emplace_back("4+2");
      test2_real_val.emplace_back("1+3");
      test2_real_val.emplace_back("2+3");
      test2_real_val.emplace_back("3+3");
      test2_real_val.emplace_back("4+3");
      test2_real_val.emplace_back("1+4");
      test2_real_val.emplace_back("2+4");
      test2_real_val.emplace_back("3+4");
      test2_real_val.emplace_back("4+4");
      
      string test2_string("EXPAND($i+$j,$i $j,[1..4;1])");
      vector<string> test2_conv_val = midas::util::StringVectorFromString(test2_string);
      UNIT_ASSERT(test2_conv_val.size() == test2_real_val.size(), "Test failed to convert \""+test2_string+
                                                                 "\" to vector<string>. Size of resulting vector is : "+
                                                                 std::to_string(test2_conv_val.size())+
                                                                 " should be : "+std::to_string(test2_real_val.size())
                                                                 );
      for(Uin i = 0; i < test2_conv_val.size(); ++i)
      {
         UNIT_ASSERT(test2_conv_val[i] == test2_real_val[i], "Test failed to convert \""+test2_string+
                                                             "\" to vector<string>, error in element "+std::to_string(i)+
                                                             " Resulting vector element is : "+test2_conv_val[i]+
                                                             " should be : "+test2_real_val[i]
                                                             );
      }
      
      vector<string> test3_real_val;
      test3_real_val.emplace_back("1+2");
      test3_real_val.emplace_back("2+2");
      test3_real_val.emplace_back("3+2");
      test3_real_val.emplace_back("4+2");
      test3_real_val.emplace_back("1+4");
      test3_real_val.emplace_back("2+4");
      test3_real_val.emplace_back("3+4");
      test3_real_val.emplace_back("4+4");
      test3_real_val.emplace_back("1+6");
      test3_real_val.emplace_back("2+6");
      test3_real_val.emplace_back("3+6");
      test3_real_val.emplace_back("4+6");
      test3_real_val.emplace_back("1+8");
      test3_real_val.emplace_back("2+8");
      test3_real_val.emplace_back("3+8");
      test3_real_val.emplace_back("4+8");
      
      string test3_string("EXPAND($i+$j,$i $j,[1..4;1],[2..8;2])");
      vector<string> test3_conv_val = midas::util::StringVectorFromString(test3_string);
      UNIT_ASSERT(test3_conv_val.size() == test3_real_val.size(), "Test failed to convert \""+test3_string+
                                                                 "\" to vector<string>. Size of resulting vector is : "+
                                                                 std::to_string(test3_conv_val.size())+
                                                                 " should be : "+std::to_string(test3_real_val.size())
                                                                 );
      for(Uin i = 0; i < test3_conv_val.size(); ++i)
      {
         UNIT_ASSERT(test3_conv_val[i] == test3_real_val[i], "Test failed to convert \""+test3_string+
                                                             "\" to vector<string>, error in element "+std::to_string(i)+
                                                             " Resulting vector element is : "+test3_conv_val[i]+
                                                             " should be : "+test3_real_val[i]
                                                             );
      }
   }
};
#endif //FROMSTRINGTEST_H
