#ifndef PLAYGROUNDTEST_H_INCLUDED
#define PLAYGROUNDTEST_H_INCLUDED

#include "test/CuteeInterface.h"
#include "util/MidasStream.h"
extern MidasStream Mout;

namespace midas
{
namespace test
{

struct PlaygroundRunImpl : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};
} /* namespace test */
} /* namespace midas */

/*** Input file to be used ***
#0 MIDAS Input

#1 Test
   PlaygroundRun

#0 Midas Input End
******************************/

/* Put your included files etc here */
//#include"tensor/NiceTensor.h"
//#include"ecor/ReferenceState.h"

//! Put your shtoopid test code here!
void midas::test::PlaygroundRunImpl::actual_test()
{

}

#endif /* PLAYGROUNDTEST_H_INCLUDED */
