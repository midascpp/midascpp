/**
************************************************************************
* 
* @file                VccStateSpaceTest.h
*
* Created:             26-04-2016
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Unit tests for VccStateSpace.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCCSTATESPACETEST_H_INCLUDED
#define VCCSTATESPACETEST_H_INCLUDED

#include <iostream>
#include <iterator>

#include "input/ModeCombiOpRange.h"
#include "vcc/VccStateSpace.h"
#include "vcc/VccStateModeCombi.h"
#include "test/CuteeInterface.h"

namespace midas::test
{
namespace vcc_state_space
{
namespace detail
{
   // Utility functions used in the testing:

   // Don't change this, or tests will fail. (Some of it is hardcoded.)
   // (The n_modals[0] = 1, leads to a dimension of 0, meaning tensors of size
   // 0, and is as such a rather pedantic example, but including here for the
   // purpose of proper testing.)
   // These are for setting up a ModeCombiOpRange.
   const std::vector<In> n_modals{I_1, I_9, I_6, I_5, I_13};
   const Uin n_exc   = I_5;
   const Uin n_modes = n_modals.size();
   // These are for removing some ModeCombis of an MCR, thus making it less
   // trivial.
   const Uin skip_stride = I_3;
   const Uin skip_begin  = I_0;

   const std::vector<char> mcr_type{'a', 'b'};
   const std::vector<bool> erase_ref{true, false};

   // Hardcoded stuff for running the tests. aSwitch = 'a' is a full MCR, 'b'
   // is a "funky" one with a lot of MCs (including the reference state)
   // omitted.
   ModeCombiOpRange MakeMCR(char aSwitch = 'a');
   std::vector<Uin> TestAddresses(char aSwitch = 'a');
   std::vector<Uin> TestRefNumbers(bool aInclRef, char aSwitch = 'a');
   std::vector<std::vector<Uin> > TestIndices(bool aInclRef, char aSwitch = 'a');
   std::vector<std::vector<Uin> > TestVecOfDims(char aSwitch = 'a', bool aEraseRef = false);

   // Just some functions used internally in the above.
   void PolishMCR(ModeCombiOpRange& arMCR, const std::vector<In>&  arNModals);

   // Remove every aN'th element of an MCR or a vector (of dims.), starting at
   // (and including) element aBegin.
   void RemoveElements(ModeCombiOpRange& arMCR, const std::vector<In>& arAllNModals, Uin aN, Uin aBegin = I_0);
   void RemoveElements(std::vector<std::vector<Uin> >& arDims, Uin aN, Uin aBegin = I_0);
   
   // Get sizes and addresses.
   std::vector<Uin> SizesFromDims(const std::vector<std::vector<Uin>>& arDims);
   std::vector<Uin> AddressesFromSizes(const std::vector<Uin>& arSizes);

   // Info string about MCR type and incl. ref.
   std::string Info(char aType, bool aInclRef = true);
}  /* namespace detail */

/**
 * Constructs a VccStateSpace from an MCR and a vector of num. modals per mode.
 * Checks that ModeCombiOpRange, vector of dimensions, vector of addresses, and
 * number of ModeCombis (in total and empty ones) are right.
 **/
struct Constructor: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t, r);
            const auto& ref_n_modals = detail::n_modals;
            const auto& ref_dims = detail::TestVecOfDims(t, r);
            const auto  ref_addresses = detail::AddressesFromSizes(detail::SizesFromDims(ref_dims));
            auto ref_mcr = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            // Check that data members have been assigned correctly.
            // The MCR.
            const auto& vss_mcr = vss.MCR();
            Uin n_mcs_val = vss_mcr.Size();
            Uin n_mcs_exp = ref_mcr.Size() - (r? ref_mcr.NumEmptyMCs(): I_0);
            UNIT_ASSERT_EQUAL(n_mcs_val, n_mcs_exp, "Wrong #MCs in VccStateSpace. "+si);
            // Compare individual MCs.
            {
               auto&& it = vss_mcr.begin(), end = vss_mcr.end(), ref_it = ref_mcr.begin();
               // Account for the reference/empty MC possibly having been
               // erased in the VSS.
               if(r)
               {
                  std::advance(ref_it, ref_mcr.NumEmptyMCs());
               }
               for(; it != end; ++it, ++ref_it)
               {
                  UNIT_ASSERT(*it == *ref_it, "MCs not equal. "+si);
               }
               UNIT_ASSERT(ref_it == ref_mcr.end(), "Reference MCR iterator didn't reach end.");
            }

            // The dimensions (from the #modals per mode).
            const auto& dims = vss.Dims();
            Uin dims_val = dims.size();
            Uin dims_exp = ref_n_modals.size();
            UNIT_ASSERT_EQUAL(dims_val, dims_exp,  "Wrong size of vector of dimensions. "+si);
            for(Uin i = I_0; i < dims.size(); ++i)
            {
               UNIT_ASSERT_EQUAL(static_cast<In>(dims[i]), ref_n_modals[i] - I_1, "Wrong dimension (i = "+std::to_string(i)+"). "+si);
            }
            // The addresses (account for the possible removal of empty MC).
            const auto& addr = vss.Addresses();
            Uin addr_val = addr.size();
            Uin addr_exp = ref_addresses.size();
            UNIT_ASSERT_EQUAL(addr_val, addr_exp, "Wrong size of vec. of addresses. "+si);
            for(Uin i = I_0; i < addr.size(); ++i)
            {
               UNIT_ASSERT_EQUAL(addr[i], ref_addresses[i], "Wrong address (i = "+std::to_string(i)+"). "+si);
            }

            // The number of empty MCs and MCs in total.
            Uin n_emp_val = vss.NumEmptyMCs();
            Uin n_emp_exp = (ref_mcr.NumEmptyMCs()==I_0 || r)? I_0: ref_mcr.NumEmptyMCs();
            UNIT_ASSERT_EQUAL(n_emp_val, n_emp_exp, "Wrong number of empty MCs. "+si);
            Uin n_vss_mcs_val = vss.NumMCs();
            Uin n_vss_mcs_exp = ref_mcr.Size() - (r? ref_mcr.NumEmptyMCs(): I_0);
            UNIT_ASSERT_EQUAL(n_vss_mcs_val, n_vss_mcs_exp, "Wrong number of MCs. "+si);
         }
      }
   }
};

/**
 * Uses VccStateSpace::const_iterator to access ModeCombis.
 **/
struct IteratorModeCombis: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t, r);
            const auto& ref_n_modals = detail::n_modals;
            const auto ref_mcr = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            auto&& mcr_iter = ref_mcr.begin();
            auto&& mcr_end  = ref_mcr.end();
            // Account for the reference/empty MC possibly having been
            // erased in the VSS.
            if(r)
            {
               std::advance(mcr_iter, ref_mcr.NumEmptyMCs());
            }
            // Compare ModeCombis.
            for(const auto& vssmc: vss)
            {
               UNIT_ASSERT(mcr_iter   != mcr_end,   "Too many mode combinations in VccStateSpace. "+si);
               UNIT_ASSERT(vssmc.MC() == *mcr_iter, "Mode combinations not equal. "+si);
               ++mcr_iter;
            }
            UNIT_ASSERT(mcr_iter == mcr_end, "Too few mode combinations in VccStateSpace. "+si);
         }
      }
   }
};

/**
 * Uses VccStateSpace::const_iterator to extract dimensions and number of
 * modals per mode.
 **/
struct IteratorDimsAndNModals: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t, r);
            const auto& ref_dims     = detail::TestVecOfDims(t, r);
            const auto& ref_n_modals = detail::n_modals;
            const auto  ref_mcr      = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            auto&& it_dims  = ref_dims.begin();
            auto&& end_dims = ref_dims.end();
            for(auto&& it_vss = vss.begin(), end_vss = vss.end(); it_vss != end_vss; ++it_vss)
            {
               UNIT_ASSERT(it_dims != end_dims, "Too many mode combinations in VccStateSpace. "+si);
               const auto& vss_dims    = it_vss->GetDims();
               const auto& vss_nmodals = it_vss->GetNModals();
               UNIT_ASSERT_EQUAL(vss_dims.size(),    it_dims->size(), "Size mismatch for dimensions. "+si);
               UNIT_ASSERT_EQUAL(vss_nmodals.size(), it_dims->size(), "Size mismatch for n_modals. "+si);
               for(Uin i = 0; i < it_dims->size(); ++i)
               {
                  UNIT_ASSERT_EQUAL(vss_dims[i], (*it_dims)[i], "Wrong dimensions. "+si);
                  UNIT_ASSERT_EQUAL(vss_nmodals[i], static_cast<In>((*it_dims)[i]+I_1), "Wrong n_modals. "+si);
               }
               ++it_dims;
            }
            UNIT_ASSERT(it_dims == end_dims, "Too few mode combinations in VccStateSpace. "+si);
         }
      }
   }
};

/**
 * Uses VccStateSpace::const_iterator to extract addresses, tensor sizes and
 * ModeCombi reference numbers.
 **/
struct IteratorAddressesAndSizes: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t, r);
            const auto& ref_dims     = detail::TestVecOfDims(t, r);
            const auto& ref_n_modals = detail::n_modals;
            const auto  ref_mcr      = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            const auto  ref_sizes = detail::SizesFromDims(ref_dims);
            const auto  ref_addr  = detail::AddressesFromSizes(ref_sizes);
            Uin         n         = I_0;

            for(const auto& vssmc: vss)
            {
               UNIT_ASSERT_EQUAL(vssmc.RefNumber(),    n,               "Wrong ModeCombi ref. num. "+si);
               UNIT_ASSERT_EQUAL(vssmc.AddressBegin(), ref_addr[n],     "Wrong begin address. "+si);
               UNIT_ASSERT_EQUAL(vssmc.AddressEnd(),   ref_addr[n+I_1], "Wrong end address. "+si);
               UNIT_ASSERT_EQUAL(vssmc.TensorSize(),   ref_sizes[n],    "Wrong tensor size. "+si);
               ++n;
            }
            UNIT_ASSERT_EQUAL(n, vss.NumMCs(), "Unexpected number of loop iterations. "+si);
         }
      }
   }
};

/**
 * Uses VccStateSpace::const_iterator to check whether the mode combinations
 * contain a given address.
 **/
struct IteratorContainsAddress: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t,r);
            const auto& ref_n_modals = detail::n_modals;
            const auto  ref_mcr      = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            // Some addresses, and the MC in which it is located, using the
            // (hardcoded) dimensions {0,8,5,4,12}.
            std::vector<Uin> v_addr    = detail::TestAddresses(t);
            std::vector<Uin> v_refnum  = detail::TestRefNumbers(r, t);
            for(Uin i = I_0; i < v_addr.size(); ++i)
            {
               // Assert that LocateAddress works.
               Uin refnum = vss.LocateAddress(v_addr[i]);
               UNIT_ASSERT_EQUAL(refnum, v_refnum[i], "Wrong MC number for address. "+si);

               // Assert that ContainsAddress works.
               std::vector<bool> ref_contained(vss.NumMCs() + I_1, false);
               ref_contained[v_refnum[i]] = true;

               auto&& iter_ref = ref_contained.begin();
               auto&& end_ref  = ref_contained.end();
               for(auto&& iter_mc = vss.begin(), end_mc =vss.end(); iter_mc != end_mc; ++iter_mc, ++iter_ref)
               {
                  UNIT_ASSERT(iter_mc->ContainsAddress(v_addr[i]) ==  *iter_ref, "ContainsAddress wrong. "+si);
               }
            }
         }
      }
   }
};

/**
 * Uses VccStateSpace::const_iterator to get a multi-index vector from an
 * address.
 **/
struct IteratorIndexVecFromAddress: public cutee::test
{
   void run() 
   {
      for(const auto& t: detail::mcr_type)
      {
         for(const auto& r: detail::erase_ref)
         {
            std::string si = detail::Info(t,r);
            const auto& ref_n_modals = detail::n_modals;
            const auto  ref_mcr      = detail::MakeMCR(t);
            VccStateSpace vss(ref_mcr, ref_n_modals, r);

            // Some addresses, and the MC in which it is located, using the
            // (hardcoded) dimensions {0,8,5,4,12}.
            std::vector<Uin> v_addr    = detail::TestAddresses(t);
            std::vector<Uin> v_refnum  = detail::TestRefNumbers(r, t);
            std::vector<std::vector<Uin> > v_ref_index = detail::TestIndices(r, t);

            for(Uin i = I_0; i < v_addr.size(); ++i)
            {
               // Advance an iterator to the right MC, otherwise
               // GetIndexVecFromAddress gives a MIDASERROR.
               auto&& iter = vss.begin();
               std::advance(iter, vss.LocateAddress(v_addr[i]));

               // Compare index vectors with references.
               if(iter != vss.end())
               {
                  UNIT_ASSERT(iter->ContainsAddress(v_addr[i]), "Iterator not at right MC. "+si);
                  const auto& index = iter->GetIndexVecFromAddress(v_addr[i]);
                  UNIT_ASSERT_EQUAL(index.size(), v_ref_index[i].size(),"Index vector, wrong size. "+si);
                  for(Uin j = I_0; j < index.size(); ++j)
                  {
                     UNIT_ASSERT_EQUAL(index[j], v_ref_index[i][j], "Wrong index. "+si);
                  }
               }
            }
         }
      }
   }
};

/**
 * Uses the operator<<(std::ostream&, const VccStateSpace&) overload. Not
 * directly a test, since there are no assertions that can fail, but can be
 * used to visually inspect that the contents of the object are as expected.
 **/
struct OstreamOperator: public cutee::test
{
   void run() 
   {
      char t = 'b';
      bool r = true;
      std::string si = detail::Info(t,r);

      const auto& ref_n_modals = detail::n_modals;
      auto ref_mcr = detail::MakeMCR(t);
      VccStateSpace vss(ref_mcr, ref_n_modals, r);

      std::string control = std::string()
         + "-------------------------------------------------------------------------------------------------\n"
         + " Ref#      Mode combination            Dimensions     N_modals per mode   Addr. [beg;end)    Size\n"
         + "-------------------------------------------------------------------------------------------------\n"
         + "    0                   (0)                   {0}                   {1}  [     0;      0)       0\n"
         + "    1                   (1)                   {8}                   {9}  [     0;      8)       8\n"
         + "    2                   (3)                   {4}                   {5}  [     8;     12)       4\n"
         + "    3                   (4)                  {12}                  {13}  [    12;     24)      12\n"
         + "    4                 (0,2)                 {0,5}                 {1,6}  [    24;     24)       0\n"
         + "    5                 (0,3)                 {0,4}                 {1,5}  [    24;     24)       0\n"
         + "    6                 (1,2)                 {8,5}                 {9,6}  [    24;     64)      40\n"
         + "    7                 (1,3)                 {8,4}                 {9,5}  [    64;     96)      32\n"
         + "    8                 (2,3)                 {5,4}                 {6,5}  [    96;    116)      20\n"
         + "    9                 (2,4)                {5,12}                {6,13}  [   116;    176)      60\n"
         + "   10               (0,1,2)               {0,8,5}               {1,9,6}  [   176;    176)       0\n"
         + "   11               (0,1,3)               {0,8,4}               {1,9,5}  [   176;    176)       0\n"
         + "   12               (0,2,3)               {0,5,4}               {1,6,5}  [   176;    176)       0\n"
         + "   13               (0,2,4)              {0,5,12}              {1,6,13}  [   176;    176)       0\n"
         + "   14               (1,2,3)               {8,5,4}               {9,6,5}  [   176;    336)     160\n"
         + "   15               (1,2,4)              {8,5,12}              {9,6,13}  [   336;    816)     480\n"
         + "   16               (2,3,4)              {5,4,12}              {6,5,13}  [   816;   1056)     240\n"
         + "   17             (0,1,2,3)             {0,8,5,4}             {1,9,6,5}  [  1056;   1056)       0\n"
         + "   18             (0,1,3,4)            {0,8,4,12}            {1,9,5,13}  [  1056;   1056)       0\n"
         + "   19             (0,2,3,4)            {0,5,4,12}            {1,6,5,13}  [  1056;   1056)       0\n"
         + "   20           (0,1,2,3,4)          {0,8,5,4,12}          {1,9,6,5,13}  [  1056;   1056)       0\n"
         + "-------------------------------------------------------------------------------------------------\n"
         + "\n"
         ;

      std::stringstream ss;
      ss << vss << std::endl;
      UNIT_ASSERT_EQUAL("\n"+ss.str(), "\n"+control, "Wrong operator<< output for "+si);
   }
};


} /* namespace vcc_state_space */
} /* namespace midas::test */

#endif /* VCCSTATESPACETEST_H_INCLUDED */
