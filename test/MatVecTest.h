#ifndef MATVECTEST_H
#define MATVECTEST_H

#include "test/CuteeInterface.h"

#include "libmda/tests/char_expression/matrix_collection.h"
#include "libmda/tests/expression/matrix_collection.h"
#include "libmda/tests/general/general_test.h"

#include "test/check/MatricesAreEqual.h"
#include "test/Random.h"

#include "mmv/test_midas_mat_mult.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "mmv/InterpolativeDecomposition.h"

namespace midas::test
{

using libmda::tests::matrix_vector_collection;
using namespace libmda::tests::general;
using midas::test::mmv::mat_vec_mult_collection;

using midas::test::check::MatricesAreEqual;

/**
 *
 **/
struct VectorNormTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = 1.0; vec(1) = 2.0; vec(2) = 1.0;
      UNIT_ASSERT_FEQUAL(vec.Norm(), sqrt(6), " vector norm not correct ");
   }
};

/**
 *
 **/
struct MatrixNormTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_2,I_2);
      mat(0,0) =  1.0; mat(0,1) = 2.0;
      mat(1,0) = -2.0; mat(1,1) = 3.0;
      UNIT_ASSERT_FEQUAL(mat.Norm(), sqrt(18), " matrix norm not correct ");
   }
};

/**
 *
 **/
struct VectorSumEleTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = 1.0; vec(1) = 2.0; vec(2) = 5.0;
      UNIT_ASSERT_FEQUAL(vec.SumEle(), 8.0, " MidasVector SumEle() failed ");
   }
};

/**
 *
 **/
struct VectorProductEleTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = -1.0; vec(1) = -2.0; vec(2) = 5.0;
      UNIT_ASSERT_FEQUAL(vec.ProductEle(), 10.0, " MidasVector ProductEle() failed ");
   }
};

/**
 *
 **/
struct VectorSumAbsEleTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = -1.0; vec(1) = -2.0; vec(2) = 5.0;
      UNIT_ASSERT_FEQUAL(vec.SumAbsEle(), 8.0, " MidasVector SumAbsEle() failed ");
   }
};

/**
 *
 **/
struct VectorFindMaxValueTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = -1.0; vec(1) = -2.0; vec(2) = -5.0;
      UNIT_ASSERT_FEQUAL(vec.FindMaxValue(), -1.0, " MidasVector FindMaxValue() failed ");
   }
};

/**
 *
 **/
struct VectorFindMinValueTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = -1.0; vec(1) = -2.0; vec(2) = 5.0;
      UNIT_ASSERT_FEQUAL(vec.FindMinValue(), -2.0, " MidasVector FindMinValue() failed ");
   }
};


/**
 *
 **/
struct VectorFindMaxAbsValueTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = -1.0; vec(1) = -2.0; vec(2) = -5.0;
      UNIT_ASSERT_FEQUAL(vec.FindMaxAbsValue(), 5.0, " MidasVector FindMaxAbsValue() failed ");
   }
};

/**
 *
 **/
struct VectorProductAbsEleTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3);
      vec(0) = 1.0; vec(1) = -2.0; vec(2) = 5.0;
      UNIT_ASSERT_FEQUAL(vec.ProductAbsEle(), 10.0, " MidasVector ProductAbsEle() failed ");
   }
};

/**
 *
 **/
struct MatrixSumEleTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_2,I_2);
      mat(0,0) = 1.0; mat(0,1) = 2.0; 
      mat(1,0) = 5.0; mat(1,1) = 2.0;
      UNIT_ASSERT_FEQUAL(mat.SumEle(), 10.0, " MidasMatrix SumEle() failed ");
   }
};

/**
 *
 **/
struct MatrixSumAbsEleTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_2,I_2);
      mat(0,0) = -1.0; mat(0,1) = 2.0; 
      mat(1,0) = 5.0; mat(1,1) = -2.0;
      UNIT_ASSERT_FEQUAL(mat.SumAbsEle(), 10.0, " MidasMatrix SumAbsEle() failed ");
   }
};

/**
 *
 **/
struct MatrixTraceTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) = 1.0; mat(0,1) = 2.0; mat(0,2) = 1.0; 
      mat(1,0) = 5.0; mat(1,1) = 2.0; mat(1,2) = 3.0;
      mat(2,0) = 2.0; mat(2,1) = 3.0; mat(2,2) = 4.0;
      UNIT_ASSERT_FEQUAL(mat.Trace(), 7.0, " MidasMatrix Trace() failed ");
   }
};

/**
 *
 **/
struct MatrixIsSymmetricTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) = -1.0; 
      mat(1,0) =  5.0; mat(1,1) = -2.0; mat(1,2) =  3.0;
      mat(2,0) = -1.0; mat(2,1) =  3.0; mat(2,2) =  4.0;
      UNIT_ASSERT(mat.IsSymmetric(),"MidasMatrix::IsSymmetric() false for symmetric matrix.");
      mat(1,0) = -5.0;
      UNIT_ASSERT(!mat.IsSymmetric(),"MidasMatrix::IsSymmetric() true for non-symmetric matrix.");
   }
};

/**
 *
 **/
struct MatrixInvertSymmetricTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) = -1.0; 
      mat(1,0) =  5.0; mat(1,1) = -2.0; mat(1,2) =  3.0;
      mat(2,0) = -1.0; mat(2,1) =  3.0; mat(2,2) =  4.0;
      MidasMatrix mat_invert(mat);
      auto determinant = InvertSymmetric(mat_invert);
      MidasMatrix mat_identity_rhs(mat_invert*mat);
      MidasMatrix mat_identity_lhs(mat*mat_invert);
      MidasMatrix identity(I_3, I_3);
      identity.Unit();
      MidasMatrix diff_rhs(identity - mat_identity_rhs);
      MidasMatrix diff_lhs(identity - mat_identity_lhs);
      UNIT_ASSERT_FZERO_PREC(diff_rhs.Norm(), identity.Norm(), 5, "InvertSymmetric() failed for symmetric real matrix.");
      UNIT_ASSERT_FZERO_PREC(diff_lhs.Norm(), identity.Norm(), 5, "InvertSymmetric() failed for symmetric real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_rhs.Trace(), Nb(mat.Nrows()), 4, "InvertSymmetric() failed for symmetric real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_lhs.Trace(), Nb(mat.Nrows()), 4, "InvertSymmetric() failed for symmetric real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(determinant, -145.0, 7, "InvertSymmetric() determinant wrong for symmetric real matrix.");
   }
};

/**
 *
 **/
struct MatrixInvertGeneralSymmetricTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) = -1.0; 
      mat(1,0) =  5.0; mat(1,1) = -2.0; mat(1,2) =  3.0;
      mat(2,0) = -1.0; mat(2,1) =  3.0; mat(2,2) =  4.0;
      MidasMatrix mat_invert(mat);
      auto determinant = InvertGeneral(mat_invert);
      MidasMatrix mat_identity_rhs(mat_invert*mat);
      MidasMatrix mat_identity_lhs(mat*mat_invert);
      MidasMatrix identity(I_3, I_3);
      identity.Unit();
      MidasMatrix diff_rhs(identity - mat_identity_rhs);
      MidasMatrix diff_lhs(identity - mat_identity_lhs);
      UNIT_ASSERT_FZERO_PREC(diff_rhs.Norm(), identity.Norm(), 4, "InvertGeneral() failed for symmetric real matrix.");
      UNIT_ASSERT_FZERO_PREC(diff_lhs.Norm(), identity.Norm(), 4, "InvertGeneral() failed for symmetric real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_rhs.Trace(), Nb(mat.Nrows()), 4, "InvertGeneral() failed for symmetric real matrix."); // MKL needs a looser precision (3) to pass the test
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_lhs.Trace(), Nb(mat.Nrows()), 4, "InvertGeneral() failed for symmetric real matrix."); // MKL needs a looser precision (3) to pass the test
      UNIT_ASSERT_FEQUAL(determinant, -145.0, "InvertGeneral() determinant wrong for symmetric real matrix.");
   }
};

/**
 *
 **/
struct MatrixInvertGeneralRealTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) = -1.0; 
      mat(1,0) = -2.0; mat(1,1) = -2.0; mat(1,2) =  3.0;
      mat(2,0) =  3.0; mat(2,1) =  7.0; mat(2,2) =  4.0;
      MidasMatrix mat_invert(mat);
      auto determinant = InvertGeneral(mat_invert);
      MidasMatrix mat_identity_rhs(mat_invert*mat);
      MidasMatrix mat_identity_lhs(mat*mat_invert);
      MidasMatrix identity(I_3, I_3);
      identity.Unit();
      MidasMatrix diff_rhs(identity - mat_identity_rhs);
      MidasMatrix diff_lhs(identity - mat_identity_lhs);
      UNIT_ASSERT_FZERO_PREC(diff_rhs.Norm(), identity.Norm(), 10, "InvertGeneral() failed for general real matrix.");
      UNIT_ASSERT_FZERO_PREC(diff_lhs.Norm(), identity.Norm(), 10, "InvertGeneral() failed for general real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_rhs.Trace(), Nb(mat.Nrows()), 4, "InvertGeneral() failed for general real matrix.");
      UNIT_ASSERT_FEQUAL_PREC(mat_identity_lhs.Trace(), Nb(mat.Nrows()), 4, "InvertGeneral() failed for general real matrix.");
      UNIT_ASSERT_FEQUAL(determinant, 64.0, "InvertGeneral() determinant wrong for general real matrix.");
   }
};

/**
 *
 **/
struct MatrixInvertGeneralComplexTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mat(I_3,I_3);
      mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) = -1.0; 
      mat(1,0) = -2.0; mat(1,1) = -2.0; mat(1,2) =  3.0;
      mat(2,0) =  3.0; mat(2,1) =  7.0; mat(2,2) =  4.0;
      
      using compl_t = std::complex<Nb>;
      using mat_t = GeneralMidasMatrix<compl_t>;
      mat_t compl_mat(mat);
      Axpy(compl_mat, mat, compl_t(0, 1));
      mat_t compl_mat_inv(compl_mat);
      [[maybe_unused]] auto compl_determinant = InvertGeneral(compl_mat_inv);
      mat_t compl_mat_indentity_rhs(compl_mat_inv*compl_mat);
      mat_t compl_mat_indentity_lhs(compl_mat*compl_mat_inv);
      mat_t compl_identity(I_3, I_3);
      compl_identity.Unit();
      mat_t compl_diff_rhs(compl_identity - compl_mat_indentity_rhs);
      mat_t compl_diff_lhs(compl_identity - compl_mat_indentity_lhs);
      UNIT_ASSERT_FZERO_PREC(compl_diff_rhs.Norm(), compl_identity.Norm(), 10, "InvertGeneral() failed for general complex matrix.");
      UNIT_ASSERT_FZERO_PREC(compl_diff_lhs.Norm(), compl_identity.Norm(), 10, "InvertGeneral() failed for general complex matrix.");
      UNIT_ASSERT_FEQUAL_PREC(Nb(std::abs(compl_mat_indentity_rhs.Trace())), Nb(std::abs(compl_mat.Nrows())), 4, "InvertGeneral() failed for general complex matrix.");
      UNIT_ASSERT_FEQUAL_PREC(Nb(std::abs(compl_mat_indentity_lhs.Trace())), Nb(std::abs(compl_mat.Nrows())), 4, "InvertGeneral() failed for general complex matrix.");
   }
};


/**
 *
 **/
template
   <  typename T
   >
class MatrixInvertGeneralRandom
   :  public cutee::test
{
   public:
      // Type names
      using mat_t = GeneralMidasMatrix<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

      // Creator
      MatrixInvertGeneralRandom
         (  const Uin aSize
         )
         :  mSize(aSize)
      {
      }

      // Run tests
      void run() override
      {
         mat_t mat(random::RandomMidasMatrix<T>(mSize, mSize, false));
         mat_t mat_inv(mat);
         [[maybe_unused]] auto determinant = InvertGeneral(mat_inv);
         mat_t check_rhs(mat_inv*mat);
         mat_t check_lhs(mat*mat_inv);
         mat_t identity(mSize, mSize, T(0));
         identity.Unit();
         real_t diffnorm_rhs = std::sqrt(DiffNorm2(check_rhs, identity));
         real_t diffnorm_lhs = std::sqrt(DiffNorm2(check_rhs, identity));
         real_t refnorm = real_t(identity.Norm());
         UNIT_ASSERT_FZERO_PREC(diffnorm_rhs, refnorm, 100*mSize, "InvertGeneral() failed for general random matrix."); // MGH: Choosing a good threshold is hard!
         UNIT_ASSERT_FZERO_PREC(diffnorm_lhs, refnorm, 100*mSize, "InvertGeneral() failed for general random matrix."); // 
      }

   private:
      const Uin mSize;

};

/**
 *
 **/
struct MatrixInterpolativeDecompositionTest
   :  public cutee::test
{
   void run()
   {
      // Test 1
      {
         MidasMatrix mat(I_3,I_3);
         // Matrix with linear dependency (col3 = col1 + col2)
         mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) =  7.0; 
         mat(1,0) = -2.0; mat(1,1) = -2.0; mat(1,2) = -6.0;
         mat(2,0) =  3.0; mat(2,1) =  7.0; mat(2,2) = 13.0;

         // Add small perturbation
         mat(0,2) += 1.e-14;
         mat(1,2) += 2.e-14;
         mat(2,2) += 4.e-13;

         
         auto id = midas::InterpolativeDecomposition(mat, 1.e-12);

         MidasMatrix x(id.mRank, I_3);
         for(In i=0; i<id.mRank; ++i)
         {
            for(In j=0; j<I_3; ++j)
            {
               x(i,j) = id.XElem(i, j);
            }
         }

         MidasMatrix ac(I_3, id.mRank);
         Nb* ptr = id.mAc.get();
         for(In j=0; j<id.mRank; ++j)
         {
            for(In i=0; i<I_3; ++i)
            {
               ac(i,j) = *(ptr++);
            }
         }

         MidasMatrix a_reconstruct = ac * x;

         MidasMatrix err = mat - a_reconstruct;

         auto errnorm = err.Norm();
         UNIT_ASSERT( errnorm < 1.e-10, "ID decomp failed!");
      }

      // Test 2
      {
         MidasMatrix mat(I_3,I_4);
         // Rectangular matrix
         mat(0,0) =  1.0; mat(0,1) =  5.0; mat(0,2) =  5.0; mat(0,3) = 1.0;
         mat(1,0) = -2.0; mat(1,1) = -2.0; mat(1,2) = -2.0; mat(1,3) = 2.0;
         mat(2,0) =  3.0; mat(2,1) =  7.0; mat(2,2) =  1.0; mat(2,3) = 4.0;

         auto id = midas::InterpolativeDecomposition(mat, 1.e-12);

         MidasMatrix x(id.mRank, I_4);
         for(In i=0; i<id.mRank; ++i)
         {
            for(In j=0; j<I_4; ++j)
            {
               x(i,j) = id.XElem(i, j);
            }
         }

         MidasMatrix ac(I_3, id.mRank);
         Nb* ptr = id.mAc.get();
         for(In j=0; j<id.mRank; ++j)
         {
            for(In i=0; i<I_3; ++i)
            {
               ac(i,j) = *(ptr++);
            }
         }

         MidasMatrix a_reconstruct = ac * x;

         MidasMatrix err = mat - a_reconstruct;

         auto errnorm = err.Norm();
         UNIT_ASSERT( errnorm < 1.e-10, "ID decomp failed!");
      }
   }
};

/**
 *
 **/
template<typename T>
struct MatrixDiffNorm2Test: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasMatrix<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasMatrix<T>(size, size);
      const vec_t v = random::RandomMidasMatrix<T>(size, size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diffnorm2_a = DiffNorm2(v, u);
      const absval_t diffnorm2_b = DiffNorm2(u, v);
      const absval_t diffnorm2_c = diff_a.Norm2();
      const absval_t diffnorm2_d = diff_b.Norm2();
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_b, 0, "Wrong DiffNorm2.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_c, 0, "Wrong DiffNorm2.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_d, 0, "Wrong DiffNorm2.");
   }
};

/**
 *
 **/
template<typename T>
struct MatrixDiffNormTest: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasMatrix<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasMatrix<T>(size, size);
      const vec_t v = random::RandomMidasMatrix<T>(size, size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diffnorm_a = DiffNorm(v, u);
      const absval_t diffnorm_b = DiffNorm(u, v);
      const absval_t diffnorm_c = diff_a.Norm();
      const absval_t diffnorm_d = diff_b.Norm();
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_b, 0, "Wrong DiffNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_c, 0, "Wrong DiffNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_d, 0, "Wrong DiffNorm.");
   }
};

/**
 *
 **/
template<typename T>
struct MatrixDiffOneNormTest: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasMatrix<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasMatrix<T>(size, size);
      const vec_t v = random::RandomMidasMatrix<T>(size, size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diff1norm_a = DiffOneNorm(v, u);
      const absval_t diff1norm_b = DiffOneNorm(u, v);
      const absval_t diff1norm_c = diff_a.SumAbsEle();
      const absval_t diff1norm_d = diff_b.SumAbsEle();
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_b, 0, "Wrong DiffOneNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_c, 0, "Wrong DiffOneNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_d, 0, "Wrong DiffOneNorm.");
   }
};



/**
 *
 **/
template<class T = Nb>
struct VectorComplexNormTest: public cutee::test
{
   static_assert(std::is_floating_point<T>::value,"VectorComplexNormTest only works for floating-point");

   void run() 
   {
      using comp_type = std::complex<T>;
      GeneralMidasVector<comp_type> vec(I_3);
      vec(0)=comp_type(1.0,3.0); vec(1)=comp_type(2.0,1.0); vec(2)=comp_type(2.0,2.0);
      UNIT_ASSERT_FEQUAL(vec.Norm(), T(std::sqrt(23.0)), " Vector Norm() failed for complex type");
   }
};

/**
 *
 **/
struct VectorDotTest: public cutee::test
{
   void run() 
   {
      MidasVector vec0(I_3);
      MidasVector vec1(I_3);
      vec0(0)=3.0; vec0(1)=1.0; vec0(2)=2.0;
      vec1(0)=1.0; vec1(1)=3.0; vec1(2)=2.0;
      Nb dot = Dot(vec0,vec1);
      UNIT_ASSERT_FEQUAL(dot, 10.0, " Vector Dot failed ");
   }
};

/**
 *
 **/
template<class T = Nb>
struct VectorComplexDotTest: public cutee::test
{
   static_assert(std::is_floating_point<T>::value,"VectorComplexDotTest only works for floating-point");

   void run() 
   {
      using comp_type = std::complex<T>;
      GeneralMidasVector<comp_type> vec0(I_3);
      GeneralMidasVector<comp_type> vec1(I_3);
      vec0(0)=comp_type(1.0,-3.0); vec0(1)=comp_type(2.0,-1.0); vec0(2)=comp_type(-2.0,4.0);
      vec1(0)=comp_type(2.0, 2.0); vec1(1)=comp_type(1.0, 2.0); vec1(2)=comp_type( 3.0,2.0);
      comp_type dot = Dot(vec0,vec1);
      UNIT_ASSERT_FEQUAL(dot.real(), T(-2.0), " Vector Dot failed for complex type");
      UNIT_ASSERT_FEQUAL(dot.imag(), T(-3.0), " Vector Dot failed for complex type");
   }
};

/**
 *
 **/
struct VectorSumProdElemsTest: public cutee::test
{
   void run() 
   {
      MidasVector vec0(std::vector<Nb>{3.0, 1.0, 2.0});
      MidasVector vec1(std::vector<Nb>{1.0, 3.0, 2.0});
      Nb result = SumProdElems(vec0,vec1);
      UNIT_ASSERT_EQUAL(result, 10.0, " Vector SumProdElems failed ");
   }
};

/**
 *
 **/
struct VectorInsertTest: public cutee::test
{
   void run() override
   {
      MidasVector v;
      std::vector<Nb> init = {1,2,3,4};
      v.Insert(0, init.begin(), init.end());
      std::vector<Nb> a = {-1, -2, -3, -4};
      v.Insert(1, a.begin(), a.end()-1);
      v.Insert(v.Size()-1, a.at(3));

      // Insertions at end.
      v.Insert(v.Size(), a.begin()+1, a.begin()+2);
      v.Insert(v.Size(), a.at(2));

      // Empty range insertion.
      v.Insert(0, a.begin(), a.begin());

      MidasVector ctrl(std::vector<Nb>{1, -1, -2, -3, 2, 3, -4, 4, -2, -3});
      UNIT_ASSERT_EQUAL(Uin(v.Size()), Uin(ctrl.Size()), "Wrong Size().");
      UNIT_ASSERT_EQUAL(MidasVector(v-ctrl).Norm(), Nb(0), "Wrong diffnorm.");
   }
};

/**
 *
 **/
struct VectorEraseTest: public cutee::test
{
   void run() override
   {
      MidasVector v(std::vector<Nb>{1,2,3,4,5,6});
      v.Erase(v.Size()-1, v.Size()+2); // Now {1,2,3,4,5}
      v.Erase(0,1);                    // Now {2,3,4,5}
      v.Erase(1,3);                    // Now {2,5}
      MidasVector ctrl(std::vector<Nb>{2,5});
      UNIT_ASSERT_EQUAL(Uin(v.Size()), Uin(ctrl.Size()), "Wrong Size().");
      UNIT_ASSERT_EQUAL(MidasVector(v-ctrl).Norm(), Nb(0), "Wrong diffnorm.");
   }
};

/**
 *
 **/
template<typename T>
struct VectorReleaseTest: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasVector<T>;
      const Uin size = 5;
      vec_t v_copy(size);
      vec_t u;

      {
         std::unique_ptr<T[]> p(nullptr);
         {
            vec_t v = random::RandomMidasVector<T>(size);
            v_copy = v;
            UNIT_ASSERT_EQUAL(Uin(v.Size()), size, "Wrong Size(), before Release().");
            UNIT_ASSERT(Uin(v.Capacity()) >= size, "Capacity() < size, before Release().");

            p = v.Release();

            UNIT_ASSERT_NOT_EQUAL(p.get(), nullptr, "After release, p holds nullptr.");
            UNIT_ASSERT_EQUAL(Uin(v.Size()), Uin(0), "Wrong Size(), after Release().");
            UNIT_ASSERT_EQUAL(Uin(v.Capacity()), Uin(0), "Wrong Capacity(), after Release().");

            // Now v goes out of scope, it's destructor is called, so p's data
            // will be deleted prematurely if v didn't release ownership as
            // intended.
            // Hopefully, this will lead to a rather easily detectable error.
         }

         // Copy p's data into u for later equality check against v_copy.
         u = vec_t(size, p.get());

         // Now p goes out of scope, and it destroys the data.
         // If v didn't release ownership as intended, this causes a segfault,
         // at least in a test I did during development. -MBH, Mar 2020.
         // A copy of the data is in u.
      }

      // Check that it's the right data v released.
      UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(u,v_copy), Nb(0), 0, "u != v_copy");
   }
};

/**
 *
 **/
template<typename T>
struct VectorDiffNorm2Scaled: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasVector<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 5;

      const vec_t u = random::RandomMidasVector<T>(size);
      const vec_t v = random::RandomMidasVector<T>(size);
      const T u_scale = random::RandomNumber<T>();
      const T v_scale = random::RandomNumber<T>();

      const absval_t ctrl_def = DiffNorm2(u, v);
      const absval_t ctrl_a   = Norm2(vec_t(u_scale*u - v_scale*v));
      const absval_t ctrl_b   = Norm2(vec_t(u_scale*u.Conjugate() - v_scale*v));

      const absval_t val_def = DiffNorm2Scaled(u, v);
      const absval_t val_a   = DiffNorm2Scaled<false>(u, v, u_scale, v_scale);
      const absval_t val_b   = DiffNorm2Scaled<true>(u, v, u_scale, v_scale);

      UNIT_ASSERT_FEQUAL_PREC(val_def, ctrl_def, 0, "Wrong DiffNorm2Scaled, default.");
      UNIT_ASSERT_FEQUAL_PREC(val_a, ctrl_a, 0, "Wrong DiffNorm2Scaled, CONJ = false.");
      UNIT_ASSERT_FEQUAL_PREC(val_b, ctrl_b, 0, "Wrong DiffNorm2Scaled, CONJ = true.");

      const Uin subsize = size - 2;
      const Uin offset = 1;
      vec_t u_slice(subsize);
      vec_t v_slice(subsize);
      u.PieceIo(IO_GET, u_slice, u_slice.Size(), offset);
      v.PieceIo(IO_GET, v_slice, u_slice.Size(), offset);

      const absval_t ctrl_sa  = Norm2(vec_t(u_scale*u_slice - v_scale*v_slice));
      const absval_t ctrl_sb  = Norm2(vec_t(u_scale*u_slice.Conjugate() - v_scale*v_slice));

      const absval_t val_sa   = DiffNorm2Scaled<false>(u, v, u_scale, v_scale, offset, subsize);
      const absval_t val_sb   = DiffNorm2Scaled<true>(u, v, u_scale, v_scale, offset, subsize);

      UNIT_ASSERT_FEQUAL_PREC(val_sa, ctrl_sa, 0, "Wrong DiffNorm2Scaled, slices, CONJ = false.");
      UNIT_ASSERT_FEQUAL_PREC(val_sb, ctrl_sb, 0, "Wrong DiffNorm2Scaled, slices, CONJ = true.");
   }
};

/**
 *
 **/
template<typename T>
struct VectorDiffNorm2Test: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasVector<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasVector<T>(size);
      const vec_t v = random::RandomMidasVector<T>(size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diffnorm2_a = u.DiffNorm2(v);
      const absval_t diffnorm2_b = v.DiffNorm2(u);
      const absval_t diffnorm2_c = diff_a.Norm2();
      const absval_t diffnorm2_d = diff_b.Norm2();
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_b, 0, "Wrong DiffNorm2.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_c, 0, "Wrong DiffNorm2.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm2_a, diffnorm2_d, 0, "Wrong DiffNorm2.");
   }
};


/**
 *
 **/
template<typename T>
struct VectorDiffNormTest: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasVector<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasVector<T>(size);
      const vec_t v = random::RandomMidasVector<T>(size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diffnorm_a = u.DiffNorm(v);
      const absval_t diffnorm_b = v.DiffNorm(u);
      const absval_t diffnorm_c = diff_a.Norm();
      const absval_t diffnorm_d = diff_b.Norm();
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_b, 0, "Wrong DiffNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_c, 0, "Wrong DiffNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diffnorm_a, diffnorm_d, 0, "Wrong DiffNorm.");
   }
};

/**
 *
 **/
template<typename T>
struct VectorDiffOneNormTest: public cutee::test
{
   void run() override
   {
      using vec_t = GeneralMidasVector<T>;
      using absval_t = midas::type_traits::RealTypeT<T>;
      const Uin size = 10;

      const vec_t u = random::RandomMidasVector<T>(size);
      const vec_t v = random::RandomMidasVector<T>(size);
      const vec_t diff_a = u - v;
      const vec_t diff_b = v - u;

      const absval_t diff1norm_a = u.DiffOneNorm(v);
      const absval_t diff1norm_b = v.DiffOneNorm(u);
      const absval_t diff1norm_c = diff_a.SumAbsEle();
      const absval_t diff1norm_d = diff_b.SumAbsEle();
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_b, 0, "Wrong DiffOneNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_c, 0, "Wrong DiffOneNorm.");
      UNIT_ASSERT_FEQUAL_PREC(diff1norm_a, diff1norm_d, 0, "Wrong DiffOneNorm.");
   }
};

/**
 *
 **/
template<class T = Nb>
struct VectorComplexSumProdElemsTest: public cutee::test
{
   static_assert(std::is_floating_point<T>::value,"VectorComplexDotTest only works for floating-point");

   void run() 
   {
      using ct = std::complex<T>;
      GeneralMidasVector<ct> vec0(std::vector<ct>{ct(1,-3), ct(2,-1), ct(-2,4), ct(2,-1)});
      GeneralMidasVector<ct> vec1(std::vector<ct>{ct(2,2),  ct(1,2),  ct(3,2),  ct(-1,3)});
      ct result = SumProdElems(vec0,vec1,1,3);
      UNIT_ASSERT_EQUAL(result, ct(-10,11), "Vector SumProdElems failed for complex type.");
   }
};

/**
 *
 **/
template<class T = Nb>
struct VectorComplexSelfDotTest: public cutee::test
{
   static_assert(std::is_floating_point<T>::value,"VectorComplexSelfDotTest only works for floating-point");
   
   void run() 
   {
      using comp_type = std::complex<Nb>;
      GeneralMidasVector<comp_type> vec0(I_3);
      vec0(0)=comp_type(1.0,3.0); vec0(1)=comp_type(2.0,1.0); vec0(2)=comp_type(2.0,2.0);
      comp_type dot = Dot(vec0,vec0);
      UNIT_ASSERT_FEQUAL(dot.real(), T(23.0), " Vector Self Dot failed for complex type");
      UNIT_ASSERT_FEQUAL(dot.imag(), T( 0.0), " Vector Self Dot failed for complex type");
   }
};

/**
 *
 **/
struct VectorIterTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_3); vec(0)=1.0; vec(1)=2.0; vec(2)=3.0;
      MidasVector::iterator iter = vec.begin();
      UNIT_ASSERT_FEQUAL((*iter),1.0,"vec(0) incorrect");
      ++iter;
      UNIT_ASSERT_FEQUAL((*iter),2.0,"vec(1) incorrect");
      ++iter;
      UNIT_ASSERT_FEQUAL((*iter),3.0,"vec(2) incorrect");
      ++iter;
      UNIT_ASSERT(iter==vec.end(),"iter is not at end :(");
   }
};

/**
 *
 **/
struct VectorStrideIterTest: public cutee::test
{
   void run() 
   {
      MidasVector vec(I_4); vec(0)=1.0; vec(1)=2.0; vec(2)=3.0; vec(3)=4.0;
      MidasVector::stride_iterator iter = vec.begin(2);
      UNIT_ASSERT_FEQUAL((*iter),1.0,"vec(0) incorrect");
      ++iter;
      UNIT_ASSERT_FEQUAL((*iter),3.0,"vec(2) incorrect");
      ++iter;
      UNIT_ASSERT(iter==vec.end(2),"iter is not at end :(");
   }
};

} /* namespace midas::test */

#endif /* MATVECTEST_H */
