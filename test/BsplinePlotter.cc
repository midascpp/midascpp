/**
************************************************************************
* 
* @file                BsplinePlotter.cc
*
* Created:             04-06-2008
*
* Author:              Daniele Toffoli
*
* Short Description:   Plot a family of B-splines for a fixed grid of knots 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers.
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// Indirect link to Standard Headers.
//#include "math_link.h"

// My Headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/OneModeBsplDef.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "util/Plot.h"
#include "test/CuteeInterface.h"

namespace midas::test
{

namespace bsplineplotter
{
/**
* Test function for plotting 
**/
struct BsplinePlotterImpl
   :  public cutee::test
{
   void run() override
   {
      In mode=I_0;
      string s_type="Bspline";
      Nb l_b=-5.0e0;
      Nb r_b=5.e0;
      In iord=5;
      In n_bas=10;
      //Construct the B-spline basis
      OneModeBsplDef b_spline(mode,s_type,l_b,r_b,iord,n_bas,false);
      In n_int=b_spline.Nint();
      MidasVector knots(n_int+1,C_0);
      Nb k_step=fabs(r_b-l_b)/n_int;
      for (In i_k=I_0; i_k<n_int+1; i_k++)
         knots[i_k]=l_b+Nb(i_k)*k_step;
      knots[knots.Size()-1]=r_b;
      //Mout << " Grid of knots: " << endl;
      //Mout << knots << endl;
      //set the plotting grid
      In n_pts=1000;
      Nb step=fabs(r_b-l_b)/n_pts;
      MidasVector b_grid(n_pts,C_0);
      for (In i_p=I_0; i_p<n_pts; i_p++)
         b_grid[i_p]=l_b+Nb(i_p)*step;
      b_grid[b_grid.Size()-I_1]=r_b;
      //Mout << " Grid of evaluation points: " << endl;
      //Mout << b_grid << endl;
      //plot then each Bspline
      MidasVector* b_grid_set=new MidasVector [n_bas+1];
      MidasVector* b_val_set=new MidasVector [n_bas+1];
      for (In i_bas=I_0; i_bas<n_bas+1; i_bas++)
      {
         if (i_bas!=n_bas) 
         {
            b_grid_set[i_bas].SetNewSize(n_pts);
            b_grid_set[i_bas]=b_grid;
            b_val_set[i_bas].SetNewSize(n_pts);
            for (In i_g=I_0; i_g<n_pts; i_g++)
               b_val_set[i_bas][i_g]=b_spline.EvalSplineBasis(i_bas,b_grid[i_g],false);
         }
         else 
         {
            b_grid_set[i_bas].SetNewSize(n_int+1);
            b_grid_set[i_bas]=knots;
            b_val_set[i_bas].SetNewSize(n_int+1);
            b_val_set[i_bas].Zero();
         }
      }
      //prepare the plot
      string* titles = new string [n_bas+1];
      for (In i_s=I_0; i_s<n_bas+1; i_s++) titles[i_s]="";
      MakeMultiPlotPs("Bspline_basis","q (a.u.)", " ",titles, b_grid_set,b_val_set,n_bas+1);
      delete[] b_grid_set;
      delete[] b_val_set;
      delete[] titles;
   }
};
} /* namespace bsplineplotter */

void BsplinePlotter()
{
   // Create test_suite object.
   cutee::suite suite("BsplinePlotter");

   // Add all the tests to the suite.
   using namespace bsplineplotter;
   suite.add_test<BsplinePlotterImpl> ("BsplinePlotter");

   // Run the tests.
   RunSuite(suite);
}

} /* namespace midas::test */
