#include <iostream>

#include "BaseTensorTest.h"

namespace midas::test
{

void BaseTensorTest()
{
   cutee::suite suite("BaseTensor test");
   
   /**************************************************************************************
    * SimpleTensor tests
    **************************************************************************************/
   // basic
   suite.add_test<SimpleTensorTest<float> >("SimpleTensor test float");
   suite.add_test<SimpleTensorTest<double> >("SimpleTensor test double");
   suite.add_test<SimpleTensorAdditionAssignmentTest<float> >("SimpleTensor AdditionAssignment test float");
   suite.add_test<SimpleTensorAdditionAssignmentTest<double> >("SimpleTensor AdditionAssignemt test double");
   suite.add_test<SimpleTensorAdditionTest<float> >("SimpleTensor Addition test float");
   suite.add_test<SimpleTensorAdditionTest<double> >("SimpleTensor Addition test double");
   suite.add_test<SimpleTensorSubtractionAssignmentTest<float> >("SimpleTensor SubtractionAssignment test float");
   suite.add_test<SimpleTensorSubtractionAssignmentTest<double> >("SimpleTensor SubtractionAssignemt test double");
   suite.add_test<SimpleTensorSubtractionTest<float> >("SimpleTensor Subtraction test float");
   suite.add_test<SimpleTensorSubtractionTest<double> >("SimpleTensor Subtraction test double");
   // contract down
   suite.add_test<SimpleTensorSimpleContractDownTest<float> >("SimpleTensor Simple ContractDown test float");
   suite.add_test<SimpleTensorSimpleContractDownTest<double> >("SimpleTensor Simple ContractDown test double");
   suite.add_test<SimpleTensorContractDownTest<float> >("SimpleTensor ContractDown test float");
   suite.add_test<SimpleTensorContractDownTest<double> >("SimpleTensor ContractDown test double");
   // contract forward
   suite.add_test<SimpleTensorVerySimpleMatrixContractForwardTest<float> >("SimpleTensor Very Simple Matrix ContractForward test float");
   suite.add_test<SimpleTensorVerySimpleMatrixContractForwardTest<double> >("SimpleTensor Very Simple Matrix ContractForward test double");
   suite.add_test<SimpleTensorSimpleMatrixContractForwardTest<float> >("SimpleTensor Simple Matrix ContractForward test float");
   suite.add_test<SimpleTensorSimpleMatrixContractForwardTest<double> >("SimpleTensor Simple Matrix ContractForward test double");
   suite.add_test<SimpleTensorSimpleContractForwardTest<float> >("SimpleTensor Simple ContractForward test float");
   suite.add_test<SimpleTensorSimpleContractForwardTest<double> >("SimpleTensor Simple ContractForward test double");
   suite.add_test<SimpleTensorContractForwardTest<float> >("SimpleTensor ContractForward test float");
   suite.add_test<SimpleTensorContractForwardTest<double> >("SimpleTensor ContractForward test double");
   // contract up
   suite.add_test<SimpleTensorContractUpTest<float> >("SimpleTensor ContractUp test float");
   suite.add_test<SimpleTensorContractUpTest<double> >("SimpleTensor ContractUp test double");
   // general contract down
   suite.add_test<SimpleTensorGeneralContractDownTest<float> >("SimpleTensor General ContractDown test float");
   suite.add_test<SimpleTensorGeneralContractDownTest<double> >("SimpleTensor General ContractDown test double");
   // general contract forward
   suite.add_test<SimpleTensorGeneralContractForwardTest<float> >("SimpleTensor General ContractForward test float");
   suite.add_test<SimpleTensorGeneralContractForwardTest<double> >("SimpleTensor General ContractForward test double");
   // general contract up
   suite.add_test<SimpleTensorGeneralContractUpTest<float> >("SimpleTensor General ContractUp test float");
   suite.add_test<SimpleTensorGeneralContractUpTest<double> >("SimpleTensor General ContractUp test double");
   // Special contractions
   suite.add_test<SimpleTensorSpecialContractionsTest<float> >("SimpleTensor special contractions (float)");
   suite.add_test<SimpleTensorSpecialContractionsTest<double> >("SimpleTensor special contractions (double)");
   // IO
   suite.add_test<SimpleTensorIoTest<float> >("SimpleTensor file IO, float");
   suite.add_test<SimpleTensorIoTest<double> >("SimpleTensor file IO, double");
   suite.add_test<SimpleTensorIoTest<std::complex<float> > >("SimpleTensor file IO, std::complex<float>");
   suite.add_test<SimpleTensorIoTest<std::complex<double> > >("SimpleTensor file IO, std::complex<double>");

   /**************************************************************************************
    * CanonicalTensor tests
    **************************************************************************************/
   // basic
   suite.add_test<CanonicalTensorTest<float> >("CanonicalTensor test float");
   suite.add_test<CanonicalTensorTest<double> >("CanonicalTensor test double");
   suite.add_test<CanonicalTensorRankZeroTest<float> >("CanonicalTensor rank zero test float");
   suite.add_test<CanonicalTensorRankZeroTest<double> >("CanonicalTensor rank zero test double");
   // contract up
   suite.add_test<CanonicalTensorContractUpTest<float> >("CanonicalTensor ContractUp test float");
   suite.add_test<CanonicalTensorContractUpTest<double> >("CanonicalTensor ContractUp test double");
   // contract down
   suite.add_test<CanonicalTensorContractDownTest<float> >("CanonicalTensor ContractDown test float");
   suite.add_test<CanonicalTensorContractDownTest<double> >("CanonicalTensor ContractDown test double");
   suite.add_test<CanonicalTensorContractDown2DTest<float> >("CanonicalTensor ContractDown2D test float");
   suite.add_test<CanonicalTensorContractDown2DTest<double> >("CanonicalTensor ContractDown2D test double");
   // contract forward
   suite.add_test<CanonicalTensorContractForwardTest<float> >("CanonicalTensor ContractForward test float");
   suite.add_test<CanonicalTensorContractForwardTest<double> >("CanonicalTensor ContractForward test double");
   
   /**************************************************************************************
    * ZeroTensor tests
    **************************************************************************************/
   // basic
   suite.add_test<ZeroTensorConstructionTest<float> >("ZeroTensor construction test float");
   suite.add_test<ZeroTensorConstructionTest<double> >("ZeroTensor construction test double");
   // contract down
   suite.add_test<ZeroTensorContractDownTest<float> >("ZeroTensor ContractDown test float");
   suite.add_test<ZeroTensorContractDownTest<double> >("ZeroTensor ContractDown test double");
   // contract forward
   suite.add_test<ZeroTensorContractForwardTest<float> >("ZeroTensor ContractForward test float");
   suite.add_test<ZeroTensorContractForwardTest<double> >("ZeroTensor ContractForward test double");
   // contract forward
   suite.add_test<ZeroTensorContractUpTest<float> >("ZeroTensor ContractUp test float");
   suite.add_test<ZeroTensorContractUpTest<double> >("ZeroTensor ContractUp test double");
   
   /**************************************************************************************
    * TensorDirectProduct tests
    **************************************************************************************/
   suite.add_test<TensorDirectProductTest<float> >("TensorDirectProduct test float");
   suite.add_test<TensorDirectProductTest<double> >("TensorDirectProduct test double");
   suite.add_test<TensorDirectProductDotTest<float> >("TensorDirectProduct dot test float");
   suite.add_test<TensorDirectProductDotTest<double> >("TensorDirectProduct dot test double");
   //suite.add_test<TensorDirectProductCanonicalRecompressTest<float> >("TensorDirectProductCanonicalRecompression test float");
   suite.add_test<TensorDirectProductCanonicalRecompressTest<double> >("TensorDirectProductCanonicalRecompression test double");
   
   /**************************************************************************************
    * TensorSum tests
    **************************************************************************************/
   suite.add_test<TensorSumAdditionAssignmentTest<float> >("TensorSum addition assignment test float");
   suite.add_test<TensorSumAdditionAssignmentTest<double> >("TensorSum addition assignment test double");
   suite.add_test<TensorSumSubtractionAssignmentTest<float> >("TensorSum subtraction assignment test float");
   suite.add_test<TensorSumSubtractionAssignmentTest<double> >("TensorSum subtraction assignment test double");
   suite.add_test<TensorSumDotTest<float> >("TensorSum dot test float");
   suite.add_test<TensorSumDotTest<double> >("TensorSum dot test double");
   suite.add_test<TensorSumDotCoefTest<float> >("TensorSum dot test with mCoef != 1 float");
   suite.add_test<TensorSumDotCoefTest<double> >("TensorSum dot test with mCoef != 1 double");
   
   /**************************************************************************************
    * Simple VS CanonicalTensor tests (cross check)
    **************************************************************************************/
   // hardcoded
   suite.add_test<SimpleVsCanonicalTensorContractDownTest<float> >("Simple Vs CanonicalTensor ContractDown float");
   suite.add_test<SimpleVsCanonicalTensorContractDownTest<double> >("Simple Vs CanonicalTensor ContractDown double");
   suite.add_test<SimpleVsCanonicalTensorContractUpTest<float> >("Simple Vs CanonicalTensor ContractUp float");
   suite.add_test<SimpleVsCanonicalTensorContractUpTest<double> >("Simple Vs CanonicalTensor ContractUp double");
   suite.add_test<SimpleVsCanonicalTensorContractForwardTest<float> >("Simple Vs CanonicalTensor ContractForward float");
   suite.add_test<SimpleVsCanonicalTensorContractForwardTest<double> >("Simple Vs CanonicalTensor ContractForward double");
   // general
   suite.add_test<SimpleVsCanonicalTensorGeneralContractDownTest<float> >("Simple Vs CanonicalTensor General ContractDown float");
   suite.add_test<SimpleVsCanonicalTensorGeneralContractDownTest<double> >("Simple Vs CanonicalTensor General ContractDown double");
   /* NB NB NB !! the general up contract tests do not run, as the general contractor for Canonical cannot do up contraction */
   //suite.add_test<SimpleVsCanonicalTensorGeneralContractUpTest<float> >("Simple Vs CanonicalTensor General ContractDown float");
   //suite.add_test<SimpleVsCanonicalTensorGeneralContractUpTest<double> >("Simple Vs CanonicalTensor General ContractDown double");
   suite.add_test<SimpleVsCanonicalTensorGeneralContractForwardTest<float> >("Simple Vs CanonicalTensor General ContractForward float");
   suite.add_test<SimpleVsCanonicalTensorGeneralContractForwardTest<double> >("Simple Vs CanonicalTensor General ContractForward double");

   /**************************************************************************************
    * Decomposition tests
    **************************************************************************************/
   suite.add_test<DecompositionTest<float> >("CanonicalTensor fittings test float");
   suite.add_test<DecompositionTest<double> >("CanonicalTensor fittings test double");
   suite.add_test<CanonicalDecomposition2DTest<float> >("CanonicalTensor 2D fittings test float");
   suite.add_test<CanonicalDecomposition2DTest<double> >("CanonicalTensor 2D fittings test double");

   /**************************************************************************************
    * Utility tests
    **************************************************************************************/
   // simple
   suite.add_test<SimpleTensorScaleTest<float> > ("SimpleTensor Scale test float");
   suite.add_test<SimpleTensorScaleTest<double> >("SimpleTensor Scale test double");
   suite.add_test<SimpleTensorZeroTest<float> > ("SimpleTensor Zero test float");
   suite.add_test<SimpleTensorZeroTest<double> >("SimpleTensor Zero test double");
   suite.add_test<SimpleTensorDotProductTest<float> >("SimpleTensor Dot Product test, float");
   suite.add_test<SimpleTensorDotProductTest<double> >("SimpleTensor Dot Product test, double");
   suite.add_test<ComplexSimpleTensorDotProductTest<float> >("Complex SimpleTensor Dot Product test, float");
   suite.add_test<ComplexSimpleTensorDotProductTest<double> >("Complex SimpleTensor Dot Product test, double");
   suite.add_test<SimpleTensorAxpyTest<float> >("SimpleTensor Axpy test, float");
   suite.add_test<SimpleTensorAxpyTest<double> >("SimpleTensor Axpy test, double");
   suite.add_test<SimpleVsCanonicalTensorAxpyTest<float> >("Simple/CanonicalTensor Axpy test, float");
   suite.add_test<SimpleVsCanonicalTensorAxpyTest<double> >("Simple/CanonicalTensor Axpy test, double");
   suite.add_test<SimpleVsZeroTensorAxpyTest<float> >("Simple/ZeroTensor Axpy test, float");
   suite.add_test<SimpleVsZeroTensorAxpyTest<double> >("Simple/ZeroTensor Axpy test, double");
   // canonical
   suite.add_test<CanonicalTensorAddVectorsTest<float> >("CanonicalTensor add vectors test, float");
   suite.add_test<CanonicalTensorAddVectorsTest<double> >("CanonicalTensor add vectors test, double");
   suite.add_test<CanonicalTensorScaleTest<float> > ("CanonicalTensor Scale test float");
   suite.add_test<CanonicalTensorScaleTest<double> >("CanonicalTensor Scale test double");
   suite.add_test<CanonicalTensorBalanceTest<float> >("CanonicalTensor Balance test float");
   suite.add_test<CanonicalTensorBalanceTest<double> >("CanonicalTensor Balance test double");
   suite.add_test<CanonicalTensorToSimpleTensorTest<float> > ("CanonicalTensor ToSimpleTensor test float");
   suite.add_test<CanonicalTensorToSimpleTensorTest<double> >("CanonicalTensor ToSimpleTensor test double");
   suite.add_test<CanonicalTensorDotProductTest<float> >("CanonicalTensor Dot Product test, float");
   suite.add_test<CanonicalTensorDotProductTest<double> >("CanonicalTensor Dot Product test, double");
   suite.add_test<ComplexCanonicalTensorDotProductTest<float> >("Complex CanonicalTensor Dot Product test, float");
   suite.add_test<ComplexCanonicalTensorDotProductTest<double> >("Complex CanonicalTensor Dot Product test, double");
   suite.add_test<CanonicalTensorDumpIntoTest<float> >("CanonicalTensor DumpInto test, float");
   suite.add_test<CanonicalTensorDumpIntoTest<double> >("CanonicalTensor DumpInto test, double");
   suite.add_test<CanonicalVsSimpleTensorDotProductTest<float> >("Canonical/SimpleTensor Dot Product test, float");
   suite.add_test<CanonicalVsSimpleTensorDotProductTest<double> >("Canonical/SimpleTensor Dot Product test, double");
   suite.add_test<CanonicalTensorAxpyTest<float> >("CanonicalTensor Axpy test, float");
   suite.add_test<CanonicalTensorAxpyTest<double> >("CanonicalTensor Axpy test, double");
   suite.add_test<CanonicalVsZeroTensorAxpyTest<float> >("Canonical/ZeroTensor Axpy test, float");
   suite.add_test<CanonicalVsZeroTensorAxpyTest<double> >("Canonical/ZeroTensor Axpy test, double");
   suite.add_test<GammaMatrixTest<float> > ("Gamma matrix test float");
   suite.add_test<GammaMatrixTest<double> >("Gamma matrix test double");
   suite.add_test<GeneralGammaMatrixTest<float> > ("General Gamma matrix test float");
   suite.add_test<GeneralGammaMatrixTest<double> >("General Gamma matrix test double");
   // zero
   suite.add_test<ZeroTensorDotProductTest<float> >("ZeroTensor Dot Product test, float");
   suite.add_test<ZeroTensorDotProductTest<double> >("ZeroTensor Dot Product test, double");
   suite.add_test<ZeroVsSimpleTensorDotProductTest<float> >("Zero/SimpleTensor Dot Product test, float");
   suite.add_test<ZeroVsSimpleTensorDotProductTest<double> >("Zero/SimpleTensor Dot Product test, double");
   suite.add_test<ZeroVsCanonicalTensorDotProductTest<float> >("Zero/CanonicalTensor Dot Product test, float");
   suite.add_test<ZeroVsCanonicalTensorDotProductTest<double> >("Zero/CanonicalTensor Dot Product test, double");
   suite.add_test<ZeroTensorAxpyTest<float> >("ZeroTensor Axpy test, float");
   suite.add_test<ZeroTensorAxpyTest<double> >("ZeroTensor Axpy test, double");
   // tensor direct product
   suite.add_test<TensorDirectProductVsCanonicalTensorDotTest<float> >("TensorDirectProduct/CanonicalTensor Dot Product test, float");
   suite.add_test<TensorDirectProductVsCanonicalTensorDotTest<double> >("TensorDirectProduct/CanonicalTensor Dot Product test, double");
   // base
   suite.add_test<DiffNormTest<float> > ("Diff Norm test, float");
   suite.add_test<DiffNormTest<double> >("Diff Norm test, double");

   /**************************************************************************************
    * Dot utility tests
    **************************************************************************************/
   suite.add_test<DotIndexCreatorTest<float> > ("IndexCreatorTest.");
   suite.add_test<DotIndexCreatorTest<double> >("IndexCreatorTest.");

   /**************************************************************************************
    * NiceTensor tests
    **************************************************************************************/
   // basics
   suite.add_test<NiceTensorFromTypeID<float> >("Construct NiceTensor from TypeID, float");
   suite.add_test<NiceTensorFromTypeID<double> >("Construct NiceTensor from TypeID, double");
   suite.add_test<UnitNiceTensorFromTypeID<float> >("Construct unit NiceTensor from TypeID, float");
   suite.add_test<UnitNiceTensorFromTypeID<double> >("Construct unit NiceTensor from TypeID, double");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
