#ifndef NUCLEITEST_H
#define NUCLEITEST_H

#include "test/CuteeInterface.h"

#include "nuclei/AtomicData.h"
#include "nuclei/Nuclei.h"

namespace midas::test
{

struct AtomicDataTest
{
   void run()
   {
      // Create and initialize atomic data
      AtomicData atomic_data;
      atomic_data.Init();
      
      // Test some values
      auto m_1_1    = atomic_data.GetMass(1, 1);
      auto m_42_100 = atomic_data.GetMass(42, 100);

      UNIT_ASSERT_FEQUAL(m_1_1,    1.0078250321000000000286334E+00, "");
      UNIT_ASSERT_FEQUAL(m_42_100, 9.9907476999999999998924860E+01, "");
   }
};

struct NucleiInternalCoordTest : public cutee::test
{
   void run() 
   {
      Nuclei n1(C_0,C_0,C_0,C_8,"O","STO-3G",I_1,I_1);
      Nuclei n2(C_1,C_0,C_1,C_8,"H","STO-3G",I_2,I_1);
      Nuclei n3(-C_1,C_0,C_1,C_8,"H","STO-3G",I_3,I_1);
      Nuclei n4(-C_10_3,C_10_3,C_10_3,C_100+C_10,"Nr110");
      Nuclei n5(C_0, -C_1, C_1, C_1,"H");
      
      UNIT_ASSERT_FEQUAL(n2.Distance(n1),sqrt(C_2),"n2.Distance(n1) failed");
      UNIT_ASSERT_FEQUAL(n1.Angle(n2,n3),90.0,"n1.Angle(n2,n3) failed");
      UNIT_ASSERT_FEQUAL(n1.Angle(n2,n4),90.0,"n1.Angle(n2,n4) failed");
      UNIT_ASSERT_FEQUAL(n1.RadAngle(n2,n4),0.5*C_PI,"n1.RadAngle(n2,n4) failed");
      UNIT_ASSERT_FEQUAL(n5.Dihedral(n3,n2,n1),0.5*C_PI,"n5.Dihedral(n3,n2,n1) failed");
   }
};

struct NucleiMassTest : public cutee::test
{
   void run() 
   {
      Nuclei n1(C_0,C_0,C_0,C_8,"O","STO-3G",I_1,I_1);
      Nuclei n2(C_1,C_0,C_1,C_8,"H","STO-3G",I_2,I_1);
      UNIT_ASSERT_FEQUAL(n1.GetMass(),1.5994914622099999999826903E+01,"Mass for oxygen not read properly from file");
      UNIT_ASSERT_FEQUAL(n2.GetMass(),1.0078250321000000000286334E+00,"Mass for hydrogen not read properly from file");
      n2.SetAnuc(2);
      UNIT_ASSERT_FEQUAL(n2.GetMass(),2.0141017779999999999258203E+00,"Mass for deuterium not read properly from file");
   }
};

struct NucleiRotationTest : public cutee::test
{
   void run() 
   {
      Nuclei n1(C_1,C_0,C_1,C_8,"H","STO-3G",I_2,I_1);
      MidasMatrix rotation(3,3,C_0);
      rotation[1][0] = -1;
      rotation[0][1] = 1;
      rotation[2][2] = 1;
      n1.Rotate(rotation);
      UNIT_ASSERT_FEQUAL(n1.X(), 0.0, "Rotation failed");
      UNIT_ASSERT_FEQUAL(n1.Y(), -1.0, "Rotation failed");
      UNIT_ASSERT_FEQUAL(n1.Z(), 1.0, "Rotation failed");
   }
};

} /* namespace midas::test */


#endif //NUCLEITEST_H
