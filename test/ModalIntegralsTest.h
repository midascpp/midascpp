/**
 *******************************************************************************
 * 
 * @file    ModalIntegralsTest.h
 * @date    28-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Testing the ModalIntegrals class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MODALINTEGRALSTEST_H_INCLUDED
#define MODALINTEGRALSTEST_H_INCLUDED

#include <vector>

#include "test/CuteeInterface.h"
#include "test/check/MatricesAreEqual.h"
#include "test/Random.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "input/ModeCombi.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/VscfCalcDef.h"
#include "ni/OneModeInt.h"
#include "vscf/Vscf.h"
#include "td/PrimitiveIntegrals.h"

#include "vcc/Vcc.h"
#include "input/VccCalcDef.h"

#include "vcc/ModalIntegrals.h"
#include "vcc/ModalIntegralsFuncs.h"

namespace midas::test::modalintegrals
{
   template<typename T> using param_t = typename ModalIntegrals<T>::param_t;
   template<typename T> using absval_t = typename ModalIntegrals<T>::absval_t;
   template<typename T> using vec_t = typename ModalIntegrals<T>::vec_t;
   template<typename T> using mat_t = typename ModalIntegrals<T>::mat_t;
   template<typename T> using cont_t = typename ModalIntegrals<T>::ContT;
   template<typename T> using trans_t = typename ModalIntegrals<T>::TransT;
   template<typename T> using prim_int_t = midas::td::PrimitiveIntegrals
      <  T
      ,  ModalIntegrals<T>::template vec_templ_t
      ,  ModalIntegrals<T>::template mat_templ_t
      >;

   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
      //! Convert std::vector to MidasVector.
      template<typename T>
      vec_t<T> ConvertToVector(const std::vector<T>&);

      //! Convert vector of vectors to (square) matrix.
      template<typename T>
      mat_t<T> ConvertToMatrix(const std::vector<std::vector<T>>&);

      //! Hardcoded, (pseudo)randomly generated integrals, real/complex.
      template<typename T>
      std::vector<std::vector<mat_t<T>>> RandomIntegrals();

      //! Hardcoded, (pseudo)randomly generated amps/coefs, real/complex.
      template<typename T>
      std::vector<vec_t<T>> RandomParams();

      //! Check integral matrices against control.
      template<typename T>
      std::pair<bool, std::string> CheckIntegrals
         (  const ModalIntegrals<T>& arMi
         ,  const std::vector<std::vector<mat_t<T>>>& arCtrl
         ,  Uin aUlpsTest = 0
         ,  Uin aUlpsZeroCutoff = 0
         );

      //! Check norms squared against control.
      template<typename T>
      std::pair<bool, std::string> CheckNormsSquared
         (  const ModalIntegrals<T>& arMi
         ,  const std::vector<std::vector<std::vector<absval_t<T>>>> arCtrl
         ,  Uin aUlpsRelToOne = 0
         );

      //! Reference data from form_vcc_twomode; integrals and norms^2 pre/post T1, T1 amps.
      template<typename T>
      std::tuple
         <  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         ,  std::vector<vec_t<T>>
         ,  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         >
      RefFormVccTwoModeT1();

      //! Reference data from vcc2_left_transf_2m; integrals and norms^2 pre/post T1, T1 amps.
      template<typename T>
      std::tuple
         <  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         ,  std::vector<vec_t<T>>
         ,  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         >
      RefVcc2LeftTransf2mT1();

      //! Reference data from form_vcc_twomode; integrals and norms^2 pre/post R1, R1 coefs.
      template<typename T>
      std::tuple
         <  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<vec_t<T>>
         ,  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         >
      RefFormVccTwoModeR1();

      //! Reference data from vcc2_left_transf_2m; integrals and norms^2 pre/post R1, R1 coefs.
      template<typename T>
      std::tuple
         <  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<vec_t<T>>
         ,  std::vector<std::vector<mat_t<T>>>
         ,  std::vector<std::vector<std::vector<absval_t<T>>>>
         >
      RefVcc2LeftTransf2mR1();

      //! Get real/imaginary parts of complex container.
      template<typename T>
      std::pair<vec_t<T>, vec_t<T>> GetRealImagParts(const vec_t<std::complex<T>>&);
      template<typename T>
      std::pair<mat_t<T>, mat_t<T>> GetRealImagParts(const mat_t<std::complex<T>>&);
      ////! Combine real/imaginary parts to complex container.
      //template<typename T>
      //vec_t<std::complex<T>> CombineToComplex(const vec_t<T>& arReal, const vec_t<T>& arImag);
   }

   /************************************************************************//**
    * @brief
    *    Constructs empty object.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ConstructorEmpty
      :  public cutee::test
   {
      void run() override
      {
         ModalIntegrals<T> mi;

         UNIT_ASSERT_EQUAL
            (  mi.NModes()
            ,  static_cast<decltype(mi.NModes())>(0)
            ,  "Wrong number of modes."
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Constructs object with modes but no operator terms.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ConstructorEmptyOperators
      :  public cutee::test
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         Uin n_modes = 3;
         std::vector<std::vector<mat_t>> integrals(n_modes);
         ModalIntegrals<T> mi(std::move(integrals));

         UNIT_ASSERT_EQUAL
            (  mi.NModes()
            ,  static_cast<decltype(mi.NModes())>(n_modes)
            ,  "Wrong number of modes."
            );
         for(LocalModeNr m = 0; m < mi.NModes(); ++m)
         {
            UNIT_ASSERT_EQUAL
               (  mi.NOper(m)
               ,  static_cast<decltype(mi.NOper(m))>(0)
               ,  "Wrong number of operators, m = "+std::to_string(m)+"."
               );
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Constructs object with modes and operators but empty integral
    *    matrices.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ConstructorEmptyIntegrals
      :  public cutee::test
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<Uin> n_opers = {2, 13, 5, 0};
         Uin n_modes = n_opers.size();
         std::vector<std::vector<mat_t>> integrals;
         for(const auto& n: n_opers)
         {
            integrals.push_back(std::vector<mat_t>(n));
         }
         ModalIntegrals<T> mi(std::move(integrals));

         UNIT_ASSERT_EQUAL
            (  mi.NModes()
            ,  static_cast<decltype(mi.NModes())>(n_modes)
            ,  "Wrong number of modes."
            );
         for(LocalModeNr m = 0; m < mi.NModes(); ++m)
         {
            UNIT_ASSERT_EQUAL
               (  mi.NOper(m)
               ,  static_cast<decltype(mi.NOper(m))>(n_opers.at(m))
               ,  "Wrong number of operators, m = "+std::to_string(m)+"."
               );
            UNIT_ASSERT_EQUAL
               (  mi.NModals(m)
               ,  static_cast<decltype(mi.NModals(m))>(0)
               ,  "Wrong dimension of integrals matrix, m = "+std::to_string(m)+"."
               );
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Constructs object with modes, operators and nonempty (but zero-valued)
    *    integral matrices.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ConstructorZeroValuedIntegrals
      :  public cutee::test
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<std::pair<Uin,Uin>> n_opers_ints = {{2,2}, {13,0}, {5,3}, {0,6}};
         Uin n_modes = n_opers_ints.size();
         std::vector<std::vector<mat_t>> integrals;
         for(const auto& n: n_opers_ints)
         {
            integrals.push_back(std::vector<mat_t>(n.first, mat_t(n.second)));
         }
         ModalIntegrals<T> mi(std::move(integrals));

         UNIT_ASSERT_EQUAL
            (  mi.NModes()
            ,  static_cast<decltype(mi.NModes())>(n_modes)
            ,  "Wrong number of modes."
            );
         for(LocalModeNr m = 0; m < mi.NModes(); ++m)
         {
            UNIT_ASSERT_EQUAL
               (  mi.NOper(m)
               ,  static_cast<decltype(mi.NOper(m))>(n_opers_ints.at(m).first)
               ,  "Wrong number of operators, m = "+std::to_string(m)+"."
               );
            decltype(mi.NModals(m)) d = mi.NOper(m) == 0? 0: n_opers_ints.at(m).second;
            UNIT_ASSERT_EQUAL
               (  mi.NModals(m)
               ,  d
               ,  "Wrong dimension of integrals matrix, m = "+std::to_string(m)+"."
               );
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Constructs object with non-zero integral matrices.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ConstructorNonZeroIntegrals
      :  public cutee::test
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         mat_t a(1);
         mat_t b(1);
         mat_t c(2);
         mat_t d(2);
         a[0][0] =  1.;
         b[0][0] = -2.;
         c[0][0] =  2.; c[0][1] = -3.;
         c[1][0] =  0.; c[1][1] =  4.;
         d[0][0] =  1.; d[0][1] =  0.;
         d[1][0] =  7.; d[1][1] = -1.;
         std::vector<std::vector<mat_t>> ints =
            {  {a, b, a}
            ,  {c, d}
            };
         auto tmp(ints);
         ModalIntegrals<T> mi(std::move(tmp));
         UNIT_ASSERT_EQUAL(mi.NModes(), static_cast<decltype(mi.NModes())>(ints.size()), "Wrong NModes.");
         std::vector<std::vector<std::vector<typename decltype(mi)::absval_t>>> norms2 =
            {  {  {1.0, 0.0, 0.0, 0.0}
               ,  {4.0, 0.0, 0.0, 0.0}
               ,  {1.0, 0.0, 0.0, 0.0}
               }
            ,  {  {4.0, 9.0, 0.0, 16.0}
               ,  {1.0, 0.0, 49.0, 1.0}
               }
            };
         for(Uin m = 0; m < ints.size(); ++m)
         {
            UNIT_ASSERT_EQUAL(mi.NOper(m), static_cast<decltype(mi.NOper(m))>(ints.at(m).size()), "Wrong NOper("+std::to_string(m)+").");
            for(Uin o = 0; o < ints.at(m).size(); ++o)
            {
               UNIT_ASSERT(check::MatricesAreEqual(mi.GetIntegrals(m,o), ints.at(m).at(o), 0), "Wrong integrals.");
               using cont_t = typename decltype(mi)::ContT;
               for(const auto& t: std::vector<cont_t>{cont_t::PASSIVE, cont_t::DOWN, cont_t::UP, cont_t::FORWARD})
               {
                  UNIT_ASSERT_EQUAL
                     (  mi.GetNorm2(m, o, t)
                     ,  norms2.at(m).at(o).at(t)
                     ,  "Wrong norm2, m = "+std::to_string(m)+", o = "+std::to_string(o)+", t = "+std::to_string(t)
                     );
               }
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Check that the norms squared of the integrals are correct.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct NormsSquared
      :  public cutee::test
   {
      void run() override
      {
         const auto& tup = detail::RefFormVccTwoModeT1<T>();
         const auto& ints = std::get<0>(tup);
         const auto& norms2 = std::get<1>(tup);

         auto tmp = ints;
         ModalIntegrals<T> mi(std::move(tmp));
         for(Uin m = 0; m < mi.NModes(); ++m)
         {
            for(Uin o = 0; o < mi.NOper(m); ++o)
            {
               using cont_t = typename decltype(mi)::ContT;
               for (const auto& t: std::vector<cont_t>{cont_t::PASSIVE, cont_t::DOWN, cont_t::UP, cont_t::FORWARD})
               {
                  UNIT_ASSERT_FEQUAL_PREC
                     (  mi.GetNorm2(m, o, t)
                     ,  norms2.at(m).at(o).at(t)
                     ,  3
                     ,  "Wrong norm2, m = "+std::to_string(m)+", o = "+std::to_string(o)+", t = "+std::to_string(t));
               }
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    T1-transform some integrals.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct T1TransformReal
      :  public cutee::test
   {
      void run() override
      {
         // Very simple, 1 mode, 1 operator.
         std::vector<std::vector<mat_t<T>>> integrals =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { 1, -2,  3}
                     ,  {-4,  5, -1}
                     ,  { 0,  2,  3}
                     }
                  )
               }
            };
         ModalIntegrals<T> mi(std::move(integrals));

         // The pre-T1 norms-squared.
         // p: 1^2                      = 1
         // d: (-2)^2 + 3^2             = 13
         // u: (-4)^2 + 0^2             = 16
         // f: 5^2 + (-1)^2 + 2^2 + 3^2 = 39
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 1), "Pre-T1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>(13), "Pre-T1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(16), "Pre-T1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>(39), "Pre-T1: forward.");

         // Now T1-transform with some amplitudes.
         // (Fill in something weird for the dummy to verify it has no effect.)
         std::vector<vec_t<T>> amps =
            {  detail::ConvertToVector(std::vector<T>{3.14, 3, -7})
            };
         mi.T1Transform(amps);

         // Manually calculating the T1-transformed integrals.
         // h_00 = 1 + (-2)(3) + (3)(-7) = 1 -6 -21 = -26
         // h_10 =-4 + (5)(3) + (-1)(-7) -(-26)(3)  = -4 +15 +7 +78 = 96
         // h_20 = 0 + (2)(3) + (3)(-7)  -(-26)(-7)  = 6 -21 -182    = -197
         // h_11 = 5 - (-2)(3)  = 5 + 6 = 11
         // h_21 = 2 - (-2)(-7) = 2 -14 =-12
         // h_12 =-1 - (3)(3)   =-1 -9  =-10
         // h_22 = 3 - (3)(-7)  = 3 +21 = 24
         std::vector<std::vector<mat_t<T>>> control =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { -26, -2,  3}
                     ,  {  96, 11,-10}
                     ,  {-197,-12, 24}
                     }
                  )
               }
            };
         auto assert = detail::CheckIntegrals(mi, control);
         UNIT_ASSERT(assert.first, "Post-T1, integrals: "+assert.second);

         // The post-T1 norms-squared.
         // p: 26^2                      =   676
         // d: 2^2  + 3^2                =    13
         // u: 96^2 + 197^2              = 48025
         // f: 11^2 + 10^2 + 12^2 + 24^2 =   941
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>(  676), "Post-T1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>(   13), "Post-T1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(48025), "Post-T1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>(  941), "Post-T1: forward.");
      }
   };

   /************************************************************************//**
    * @brief
    *    T1-transform some integrals. Complex integrals and amplitudes.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct T1TransformComplex
      :  public cutee::test
   {
      void run() override
      {
         static_assert(midas::type_traits::IsComplexV<T>, "Expected complex type.");

         // Very simple, 1 mode, 1 operator.
         std::vector<std::vector<mat_t<T>>> integrals =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { { 1, 2}, {-1, 1}}
                     ,  { {-3,-1}, { 0,-3}}
                     }
                  )
               }
            };
         ModalIntegrals<T> mi(std::move(integrals));

         // The pre-T1 norms-squared.
         // p: 1^2 + 2^2                =  5
         // d: 1^2 + 1^2                =  2
         // u: 3^2 + 1^2                = 10
         // f: 0^2 + 3^2                =  9
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 5), "Pre-T1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>( 2), "Pre-T1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(10), "Pre-T1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>( 9), "Pre-T1: forward.");

         // Now T1-transform with some amplitudes.
         // (Fill in something weird for the dummy to verify it has no effect.)
         std::vector<vec_t<T>> amps =
            {  detail::ConvertToVector(std::vector<T>{{42.,-3.14}, {-2,1}})
            };
         mi.T1Transform(amps);

         // Manually calculating the T1-transformed integrals.
         // h_00  =  1+2i + (-1+1i)(-2+1i) 
         //       = (1 +(-1)(-2) +i^2(1)(1)) + (2 +(-1)(1) +(1)(-2))i
         //       = 2 - 1i
         // h_10  = -3-1i + ( 0-3i)(-2+1i) - (2-1i)(-2+1i)
         //       = (-3 +i^2(-3)(1) -(2)(-2) -i^2(-1)(1)) + (-1 +(-3)(-2) -(2)(1) -(-1)(-2))i
         //       = 3 + 1i
         // h_11  = 0-3i - (-1+1i)(-2+1i) 
         //       = (0 -(-1)(-2) -i^2(1)(1)) + (-3 -(-1)(1) -(1)(-2))i
         //       = -1 + 0i
         std::vector<std::vector<mat_t<T>>> control =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { { 2,-1}, {-1, 1}}
                     ,  { { 3, 1}, {-1, 0}}
                     }
                  )
               }
            };
         auto assert = detail::CheckIntegrals(mi, control);
         UNIT_ASSERT(assert.first, "Post-T1, integrals: "+assert.second);

         // The post-T1 norms-squared.
         // p: 2^2 + 1^2 =  5
         // d: 1^2 + 1^2 =  2
         // u: 3^2 + 1^2 = 10
         // f: 1^2 + 0^2 =  1
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 5), "Post-T1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>( 2), "Post-T1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(10), "Post-T1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>( 1), "Post-T1: forward.");
      }
   };

   /************************************************************************//**
    * @brief
    *    T1-transform some integrals, randomly generated.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct T1TransformRandom
      :  public cutee::test
   {
      void run() override
      {
         // Get integrals and params.
         auto amps = detail::RandomParams<T>();
         ModalIntegrals<T> mi(std::move(detail::RandomIntegrals<T>()));

         // The result of the T1 transform on each (mode, operator) integral
         // matrix can be calculated in matrix form as
         //    (I - T1m) M (I + T1m)
         // where
         //    m:   the mode
         //    I:   identity of dim(m)
         //    T1m: matrix with T1-amps down the [a;0] column, rest 0.
         //    M:   the untransformed integrals
         // (Ineffecient but correct.)
         std::vector<std::vector<mat_t<T>>> control;
         for(Uin m = 0; m < mi.NModes(); ++m)
         {
            // Setup identity and T1m for this mode.
            mat_t<T> I(mi.NModals(m), mi.NModals(m), 0);
            I.SetDiagToNb(1);

            mat_t<T> T1m(mi.NModals(m), mi.NModals(m), 0);
            for(Uin a = 1; a < T1m.Nrows(); ++a)
            {
               T1m[a][0] =  amps.at(m)[a];
            }

            // Then calculate transformed matrices for each operator.
            control.push_back({});
            for(Uin o = 0; o < mi.NOper(m); ++o)
            {
               const auto& M = mi.GetIntegrals(m,o);
               control.back().push_back(mat_t<T>((I - T1m)*M*(I + T1m)));
            }
         }

         // Do the T1-trans. and compare with control.
         mi.T1Transform(amps);
         auto assert = detail::CheckIntegrals(mi, control, 4);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    T1-transform some integrals.
    *
    * Uses reference data from the form_vcc_twomode test, using the former
    * Integrals class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct T1TransformTestSuiteA
      :  public cutee::test
   {
      void run() override
      {
         const auto& tup = detail::RefFormVccTwoModeT1<T>();
         const auto& pre_ints = std::get<0>(tup);
         const auto& pre_norms2 = std::get<1>(tup);
         const auto& t1_amps = std::get<2>(tup);
         const auto& post_ints = std::get<3>(tup);
         const auto& post_norms2 = std::get<4>(tup);

         // Construct and check norms squared.
         auto tmp = pre_ints;
         ModalIntegrals<T> mi(std::move(tmp));
         auto assert = detail::CheckNormsSquared(mi, pre_norms2, 1);
         UNIT_ASSERT(assert.first, "Pre-T1 norms^2: "+assert.second);

         // Do T1-transformation, check integral matrices are right, and check
         // norms squared.
         mi.T1Transform(t1_amps);
         assert = detail::CheckIntegrals(mi, post_ints, 5);
         UNIT_ASSERT(assert.first, "Post-T1 integrals: "+assert.second);

         assert = detail::CheckNormsSquared(mi, post_norms2, 10);
         UNIT_ASSERT(assert.first, "Post-T1 norms^2: "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    T1-transform some integrals.
    *
    * Uses reference data from the vcc2_left_transf_2m test, using the former
    * Integrals class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct T1TransformTestSuiteB
      :  public cutee::test
   {
      void run() override
      {
         const auto& tup = detail::RefVcc2LeftTransf2mT1<T>();
         const auto& pre_ints = std::get<0>(tup);
         const auto& pre_norms2 = std::get<1>(tup);
         const auto& t1_amps = std::get<2>(tup);
         const auto& post_ints = std::get<3>(tup);
         const auto& post_norms2 = std::get<4>(tup);

         // Construct and check norms squared.
         auto tmp = pre_ints;
         ModalIntegrals<T> mi(std::move(tmp));
         auto assert = detail::CheckNormsSquared(mi, pre_norms2, 1);
         UNIT_ASSERT(assert.first, "Pre-T1 norms^2: "+assert.second);

         // Do T1-transformation, check integral matrices are right, and check
         // norms squared.
         mi.T1Transform(t1_amps);
         assert = detail::CheckIntegrals(mi, post_ints, 7);
         UNIT_ASSERT(assert.first, "Post-T1 integrals: "+assert.second);

         assert = detail::CheckNormsSquared(mi, post_norms2, 250);
         UNIT_ASSERT(assert.first, "Post-T1 norms^2: "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    R1-transform some integrals.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct R1TransformReal
      :  public cutee::test
   {
      void run() override
      {
         // Very simple, 1 mode, 1 operator.
         std::vector<std::vector<mat_t<T>>> integrals =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { 1, -2,  3}
                     ,  {-4,  5, -1}
                     ,  { 0,  2,  3}
                     }
                  )
               }
            };
         ModalIntegrals<T> mi(std::move(integrals), trans_t<T>::T1);

         // The pre-R1 norms-squared.
         // p: 1^2                      = 1
         // d: (-2)^2 + 3^2             = 13
         // u: (-4)^2 + 0^2             = 16
         // f: 5^2 + (-1)^2 + 2^2 + 3^2 = 39
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 1), "Pre-R1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>(13), "Pre-R1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(16), "Pre-R1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>(39), "Pre-R1: forward.");

         // Now R1-transform with some coefficients.
         // (Fill in something weird for the dummy to verify it has no effect.)
         std::vector<vec_t<T>> amps =
            {  detail::ConvertToVector(std::vector<T>{3.14, 3, -7})
            };
         mi.R1Transform(amps);

         // Manually calculating the R1-transformed integrals.
         // h_00 = (-2)(3) + (3)(-7)            = -27
         // h_10 = (5)(3) + (-1)(-7) - (1)(3)   =  19
         // h_20 = (2)(3) + (3)(-7) - (1)(-7)   =  -8
         // h_11 = -(-2)(3)                      =  6
         // h_21 = -(-2)(-7)                     =-14
         // h_12 = -(3)(3)                       = -9
         // h_22 = -(3)(-7)                      = 21
         // Zero the rest.
         std::vector<std::vector<mat_t<T>>> control =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { -27,  0,  0}
                     ,  {  19,  6, -9}
                     ,  {  -8,-14, 21}
                     }
                  )
               }
            };
         auto assert = detail::CheckIntegrals(mi, control);
         UNIT_ASSERT(assert.first, "Post-R1, integrals: "+assert.second);

         // The post-R1 norms-squared.
         // p: 27^2                    = 729
         // d: 0                       =   0
         // u: 19^2 + 8^2              = 425
         // f: 6^2 + 9^2 + 14^2 + 21^2 = 754
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>(729), "Post-R1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>(  0), "Post-R1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(425), "Post-R1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>(754), "Post-R1: forward.");
      }
   };

   /************************************************************************//**
    * @brief
    *    R1-transform some integrals. Complex integrals and coefficients.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct R1TransformComplex
      :  public cutee::test
   {
      void run() override
      {
         static_assert(midas::type_traits::IsComplexV<T>, "Expected complex type.");

         // Very simple, 1 mode, 1 operator.
         std::vector<std::vector<mat_t<T>>> integrals =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { { 1, 2}, {-1, 1}}
                     ,  { {-3,-1}, { 0,-3}}
                     }
                  )
               }
            };
         ModalIntegrals<T> mi(std::move(integrals), trans_t<T>::T1);

         // The pre-R1 norms-squared.
         // p: 1^2 + 2^2                =  5
         // d: 1^2 + 1^2                =  2
         // u: 3^2 + 1^2                = 10
         // f: 0^2 + 3^2                =  9
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 5), "Pre-R1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>( 2), "Pre-R1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(10), "Pre-R1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>( 9), "Pre-R1: forward.");

         // Now R1-transform with some coefficients.
         // (Fill in something weird for the dummy to verify it has no effect.)
         std::vector<vec_t<T>> amps =
            {  detail::ConvertToVector(std::vector<T>{{42.,-3.14}, {-2,1}})
            };
         mi.R1Transform(amps);

         // Manually calculating the R1-transformed integrals.
         // h_00  = (-1+1i)(-2+1i)
         //       = (-1)(-2) -(1)(1) +((-1)(1) +(1)(-2))i
         //       = 1 -3i
         // h_10  = (0-3i)(-2+1i) - (1+2i)(-2+1i)
         //       = (-1-5i)(-2+1i)
         //       = (-1)(-2) -(-5)(1) + ((-1)(1) +(-5)(-2))i
         //       = 7 +9i
         // h_11  = -(-1+1i)(-2+1i)
         //       = -1 +3i
         // h_01  = 0
         std::vector<std::vector<mat_t<T>>> control =
            {  {  detail::ConvertToMatrix(std::vector<std::vector<T>>
                     {  { { 1,-3}, { 0, 0}}
                     ,  { { 7, 9}, {-1, 3}}
                     }
                  )
               }
            };
         auto assert = detail::CheckIntegrals(mi, control);
         UNIT_ASSERT(assert.first, "Post-R1, integrals: "+assert.second);

         // The post-R1 norms-squared.
         // p: 1^2 + 3^2   = 10
         // d:             =  0
         // u: 7^2 + 9^2   =130
         // f: 1^2 + 3^2   = 10
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::PASSIVE), absval_t<T>( 10), "Post-R1: passive.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::DOWN   ), absval_t<T>(  0), "Post-R1: down.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::UP     ), absval_t<T>(130), "Post-R1: up.");
         UNIT_ASSERT_EQUAL(mi.GetNorm2(0,0,cont_t<T>::FORWARD), absval_t<T>( 10), "Post-R1: forward.");
      }
   };

   /************************************************************************//**
    * @brief
    *    R1-transform some integrals, randomly generated.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct R1TransformRandom
      :  public cutee::test
   {
      void run() override
      {
         // Get integrals and params.
         auto amps = detail::RandomParams<T>();
         ModalIntegrals<T> mi(std::move(detail::RandomIntegrals<T>()), trans_t<T>::T1);

         // The result of the R1 transform on each (mode, operator) integral
         // matrix can be calculated in matrix form as
         //    [M, R1m] = M R1m - R1m M
         // where
         //    m:   the mode
         //    I:   identity of dim(m)
         //    R1m: matrix with R1-coefs. down the [a;0] column, rest 0.
         //    M:   the untransformed integrals
         // (Ineffecient but correct.)
         std::vector<std::vector<mat_t<T>>> control;
         for(Uin m = 0; m < mi.NModes(); ++m)
         {
            // Setup identity and R1m for this mode.
            mat_t<T> I(mi.NModals(m), mi.NModals(m), 0);
            I.SetDiagToNb(1);

            mat_t<T> R1m(mi.NModals(m), mi.NModals(m), 0);
            for(Uin a = 1; a < R1m.Nrows(); ++a)
            {
               R1m[a][0] =  amps.at(m)[a];
            }

            // Then calculate transformed matrices for each operator.
            control.push_back({});
            for(Uin o = 0; o < mi.NOper(m); ++o)
            {
               const auto& M = mi.GetIntegrals(m,o);
               control.back().push_back(mat_t<T>(M*R1m - R1m*M));
            }
         }

         // Do the R1-trans. and compare with control.
         mi.R1Transform(amps);
         auto assert = detail::CheckIntegrals(mi, control, 1);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    R1-transform some integrals.
    *
    * Uses reference data from the form_vcc_twomode test, using the former
    * Integrals class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct R1TransformTestSuiteA
      :  public cutee::test
   {
      void run() override
      {
         const auto& tup = detail::RefFormVccTwoModeR1<T>();
         const auto& pre_ints = std::get<0>(tup);
         const auto& r1_coefs = std::get<1>(tup);
         const auto& post_ints = std::get<2>(tup);
         const auto& post_norms2 = std::get<3>(tup);

         // Construct and check norms squared.
         auto tmp = pre_ints;
         ModalIntegrals<T> mi(std::move(tmp), trans_t<T>::T1);

         // Do R1-transformation, check integral matrices are right, and check
         // norms squared.
         mi.R1Transform(r1_coefs);
         auto assert = detail::CheckIntegrals(mi, post_ints, 1);
         UNIT_ASSERT(assert.first, "Post-R1 integrals: "+assert.second);
         
         assert = detail::CheckNormsSquared(mi, post_norms2, 3);
         UNIT_ASSERT(assert.first, "Post-R1 norms^2: "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    R1-transform some integrals.
    *
    * Uses reference data from the vcc2_left_transf_2m test, using the former
    * Integrals class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct R1TransformTestSuiteB
      :  public cutee::test
   {
      void run() override
      {
         const auto& tup = detail::RefVcc2LeftTransf2mR1<T>();
         const auto& pre_ints = std::get<0>(tup);
         const auto& r1_coefs = std::get<1>(tup);
         const auto& post_ints = std::get<2>(tup);
         const auto& post_norms2 = std::get<3>(tup);

         // Construct and check norms squared.
         auto tmp = pre_ints;
         ModalIntegrals<T> mi(std::move(tmp), trans_t<T>::T1);

         // Do R1-transformation, check integral matrices are right, and check
         // norms squared.
         mi.R1Transform(r1_coefs);
         auto assert = detail::CheckIntegrals(mi, post_ints, 1, 1);
         UNIT_ASSERT(assert.first, "Post-R1 integrals: "+assert.second);

         assert = detail::CheckNormsSquared(mi, post_norms2, 3);
         UNIT_ASSERT(assert.first, "Post-R1 norms^2: "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Check that the transformation types are set correctly.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct TransformationTypes
      :  public cutee::test
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         using type = typename ModalIntegrals<T>::TransT;
         std::vector<std::vector<mat_t>> ints {{mat_t(1)}};
         std::vector<vec_t> amps {vec_t(1)};
         ModalIntegrals<T> mi(std::move(ints));
         UNIT_ASSERT(mi.GetType() == type::RAW, "Before T1-trans, expected type::RAW.");
         UNIT_ASSERT_EQUAL(mi.GetTypeString(), std::string("RAW"), "Before T1-trans.");
         mi.T1Transform(amps);
         UNIT_ASSERT(mi.GetType() == type::T1, "After T1-trans, expected type::T1.");
         UNIT_ASSERT_EQUAL(mi.GetTypeString(), std::string("T1"), "After T1-trans.");
         mi.R1Transform(amps);
         UNIT_ASSERT(mi.GetType() == type::R1, "After R1-trans, expected type::R1.");
         UNIT_ASSERT_EQUAL(mi.GetTypeString(), std::string("R1"), "After R1-trans.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the Contract, with _down_ contraction settings.
    *
    * Data taken from the v3_h3_vcc3 test, using the old Integrals class
    * implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractDown
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 2;
         const In o = 1;
         const In which = -1; // -1, 0, 1: down, forward, up
         const intop_t notransp = intop_t::STANDARD;
         const ModeCombi mc_in (std::vector<In>{2,4,5}, -1);
         const ModeCombi mc_out(std::vector<In>{4,5}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {  -9.4837337228817906E-06
               ,   8.9968697860621615E-04
               ,   1.4265870435457395E-05
               ,   5.2563245458224701E-05
               ,   5.3741123309632480E-06
               ,   3.2445254625158611E-06
               ,   3.5599224193928233E-07
               ,   1.9487237085126323E-07
               }
            );
         vec_t<T> v_out(4, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {  -8.0811527008111395E-05
               ,   7.6844045293543821E-03
               ,   1.2185837335443992E-04
               ,   4.4895326915742403E-04
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  {  5.2049519599951266E-01,  8.5410705908806026E+00,  3.5301119971728204E-02 }
               ,  {  8.5410705909081130E+00,  3.7076810827201800E-01,  1.2082358240542776E+01 }
               ,  {  3.5301119970359944E-02,  1.2082358240523337E+01,  2.2034037861613262E-01 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 1
            ,  {  mat_t<T>(3, 3, 0.0)     // m = 2
               ,  mat_m_o                 // <- working on m = 2, o = 1 in this test
               }
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 3
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 4
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 5
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, notransp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_EQUAL(v_out[i], v_ctrl[i], "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the Contract, with _forward_ contraction settings.
    *
    * Data taken from the form_vcc_twomode test, using the old Integrals class
    * implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractForward
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 1;
         const In o = 2;
         const In which = 0; // -1, 0, 1: down, forward, up
         const intop_t notransp = intop_t::STANDARD;
         const ModeCombi mc_in (std::vector<In>{0,1}, -1);
         const ModeCombi mc_out(std::vector<In>{0,1}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {  -7.8486705913538934E-05
               ,   8.3674229536262185E-06
               ,  -4.7362334495028028E-05
               ,  -4.8617228735535020E-06
               ,  -3.5023211799653807E-02
               ,   1.6625892228179803E-07
               }
            );
         vec_t<T> v_out(6, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {  -3.0735695607280920E-02
               ,   3.6295548867718262E-03
               ,  -4.5287846638011622E-02
               ,  -1.6380165693471401E-03
               ,  -1.5199048119175368E+01
               ,  -1.5102922939927656E-03
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  {  8.8128567019706679E+01,  6.4542357013135003E-03,  1.2432824055585411E+02, -7.5721830827815164E-04 }
               ,  {  6.4542357013135021E-03,  2.6268995505756686E+02,  1.1318467093536762E-02,  2.1363222848950664E+02 }
               ,  {  1.2432824055585412E+02,  1.1318467093536764E-02,  4.3397070930578394E+02,  1.6326016969448260E-02 }
               ,  { -7.5721830827815099E-04,  2.1363222848950664E+02,  1.6326016969448263E-02,  6.0218090298283164E+02 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(4, 4, 0.0)     // m = 1
               ,  mat_t<T>(4, 4, 0.0)
               ,  mat_m_o                 // <- working on m = 1, o = 2 in this test
               }
            ,  {  mat_t<T>(5, 5, 0.0) }   // m = 2
            ,  {  mat_t<T>(6, 6, 0.0) }   // m = 3
            ,  {  mat_t<T>(7, 7, 0.0) }   // m = 4
            ,  {  mat_t<T>(8, 8, 0.0) }   // m = 5
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, notransp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_FEQUAL_PREC(v_out[i], v_ctrl[i], 1, "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the Contract, with _up_ contraction settings.
    *
    * Data taken from the v3_h3_vcc3 test, using the old Integrals class
    * implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractUp
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 5;
         const In o = 1;
         const In which = 1; // -1, 0, 1: down, forward, up
         const intop_t notransp = intop_t::STANDARD;
         const ModeCombi mc_in (std::vector<In>{1,2}, -1);
         const ModeCombi mc_out(std::vector<In>{1,2,5}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {   7.5456590884924211E+01
               ,   9.6880250155681995E+02
               ,  -1.9105688062925698E-04
               ,  -2.4530181091211752E-03
               }
            );
         vec_t<T> v_out(8, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {   4.6181835502922564E+02
               ,   2.0060224345649388E-03
               ,   5.9293796919542447E+03
               ,   2.5755729618761544E-02
               ,  -1.1693289253390739E-03
               ,  -5.0792698732544917E-09
               ,  -1.5013251655364398E-02
               ,  -6.5213777903055140E-08
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  { -4.8239527901773632E-04,  6.1203183129956944E+00,  2.6585118821816993E-05 }
               ,  {  6.1203183129956944E+00, -5.9686899066976635E-04,  8.5363646453319557E+00 }
               ,  {  2.6585118821816936E-05,  8.5363646453319557E+00, -6.8741367891344922E-04 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 1
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 2
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 3
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 4
            ,  {  mat_t<T>(3, 3, 0.0)     // m = 5
               ,  mat_m_o                 // <- working on m = 5, o = 1 in this test
               }
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, notransp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_EQUAL(v_out[i], v_ctrl[i], "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the down Contract, using _transposed_ integrals settings.
    *
    * Data taken from the vcc2_left_transf_2m test, using the old Integrals
    * class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractDownTranspose
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 2;
         const In o = 1;
         const In which = -1; // -1, 0, 1: down, forward, up
         const intop_t transp = intop_t::TRANSP;
         const ModeCombi mc_in (std::vector<In>{1,2}, -1);
         const ModeCombi mc_out(std::vector<In>{1}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {  -2.5982508712749731E-08
               ,  -5.4857758424062032E-08
               ,  -5.0370651271827803E-09
               ,   2.5055277222497821E-08
               }
            );
         vec_t<T> v_out(2, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {   2.0450147384528116E-07
               ,   4.8511095030496869E-08
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  {  9.9562178053205913E-02, -8.3779961053944021E+00,  2.5213196898971102E-01 }
               ,  { -8.3952056974091551E+00, -9.6638164784547065E-01, -1.1905334183106140E+01 }
               ,  {  2.4840663790231032E-01, -1.1893246072064411E+01, -2.1106125995221610E+00 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 1
            ,  {  mat_t<T>(3, 3, 0.0)     // m = 2
               ,  mat_m_o                 // <- working on m = 2, o = 1 in this test
               }
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 3
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 4
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 5
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, transp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_EQUAL(v_out[i], v_ctrl[i], "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the forward Contract, using _transposed_ integrals settings.
    *
    * Data taken from the vcc2_left_transf_2m test, using the old Integrals
    * class implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractForwardTranspose
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 1;
         const In o = 1;
         const In which = 0; // -1, 0, 1: down, forward, up
         const intop_t transp = intop_t::TRANSP;
         const ModeCombi mc_in (std::vector<In>{0,1}, -1);
         const ModeCombi mc_out(std::vector<In>{0,1}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {  -2.3733143329211236E-05
               ,   9.7587300528542247E-06
               ,  -9.0339154683284628E-01
               ,   2.1627306803330454E-01
               }
            );
         vec_t<T> v_out(4, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {   1.9255048186427590E-06
               ,   1.3570392564259576E-04
               ,   1.2585518336412649E+00
               ,   5.8950353826859310E+00
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  { -1.5788349685560528E+00, -5.4088071918748870E+00,  3.8043823671412630E-01 }
               ,  { -5.4270468903372899E+00, -3.2215758475182175E+00, -7.6508494342846660E+00 }
               ,  {  3.7678800716369965E-01, -7.6375323543900091E+00, -4.7008965636957569E+00 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(3, 3, 0.0)     // m = 1
               ,  mat_m_o                 // <- working on m = 1, o = 1 in this test
               }
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 2
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 3
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 4
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 5
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, transp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_FEQUAL_PREC(v_out[i], v_ctrl[i], 3, "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test the up Contract, using _transposed_ integrals settings.
    *
    * Data taken from the v3_h3_vcc3_rsp test, using the old Integrals class
    * implementation.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractUpTranspose
      :  public cutee::test
   {
      void run() override
      {
         using intop_t = midas::vcc::IntegralOpT;

         // Parameters etc. for function call.
         const In m = 3;
         const In o = 1;
         const In which = 1; // -1, 0, 1: down, forward, up
         const intop_t transp = intop_t::TRANSP;
         const ModeCombi mc_in (std::vector<In>{0,2}, -1);
         const ModeCombi mc_out(std::vector<In>{0,2,3}, -1);
         const vec_t<T> v_in = detail::ConvertToVector
            (  std::vector<T>
               {  -4.1854907611011042E-06
               ,  -1.7299062208411777E-08
               ,  -6.1937898711568470E-11
               ,  -2.5599568223341445E-13
               }
            );
         vec_t<T> v_out(8, 0.0);
         const vec_t<T> v_ctrl = detail::ConvertToVector
            (  std::vector<T>
               {  -3.3024793300422559E-05
               ,   1.7078523921165241E-06
               ,  -1.3649485480494889E-07
               ,   7.0587289425138328E-09
               ,  -4.8870883228850758E-10
               ,   2.5273210362885797E-11
               ,  -2.0198836824896170E-12
               ,   1.0445676820914083E-13
               }
            );

         // The matrix elements relevant for this test.
         mat_t<T> mat_m_o = detail::ConvertToMatrix
            (  std::vector<std::vector<T>>
               {  {  9.3930842524647806E-01,  7.8903037147630712E+00, -4.0804113295121175E-01 }
               ,  {  7.8976418480802151E+00,  2.6797253608828444E+00,  1.1175776898437130E+01 }
               ,  { -4.0506903025021090E-01,  1.1170651013563386E+01,  4.3809072425764848E+00 }
               }
            );
         // And then set up all mode/operator matrices, the rest just being
         // zero, but of the right size.
         std::vector<std::vector<mat_t<T>>> ints =
            {  {  mat_t<T>(3, 3, 0.0) }   // m = 0
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 1
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 2
            ,  {  mat_t<T>(3, 3, 0.0)     // m = 3
               ,  mat_m_o                 // <- working on m = 1, o = 1 in this test
               }
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 4
            ,  {  mat_t<T>(3, 3, 0.0) }   // m = 5
            };
         ModalIntegrals<T> mi(std::move(ints));

         mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, transp);

         UNIT_ASSERT_EQUAL(v_out.Size(), v_ctrl.Size(), "v_out size mismatch.");
         for(Uin i = 0; i < v_out.Size(); ++i)
         {
            UNIT_ASSERT_EQUAL(v_out[i], v_ctrl[i], "v_out value, i = "+std::to_string(i));
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Test complex versions of Contract.
    *
    * The real versions of Contract are assumed to function. Performs
    * real/imag. contractions separately and combines results to get the
    * reference values.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ContractComplex
      :  public cutee::test
   {
      void run() override
      {
         static_assert(midas::type_traits::IsComplexV<T>, "Expected complex type.");
         using real_t = midas::type_traits::RealTypeT<T>;
         using intop_t = midas::vcc::IntegralOpT;

         // Constant for every type of contraction.
         const Uin n_modals = 3;
         const Uin n_virt = n_modals - 1;
         const In m = 0;
         const In o = 0;

         // Info string for error output.
         auto info = [](In which, intop_t intop, Uin index)->std::string
            {
               static std::map<intop_t, std::string> intop_map = 
               {  {  intop_t::STANDARD, "STANDARD" }
               ,  {  intop_t::TRANSP, "TRANSP" }
               ,  {  intop_t::CONJ, "CONJ" }
               };
               std::stringstream ss;
               ss << "which = " << which
                  << ", intop = " << intop_map.at(intop)
                  << ", i = " << index
                  ;
               return ss.str();
            };

         // Input vector to be transformed.
         const vec_t<T> v_in = random::RandomMidasVector<T>(n_virt*n_virt);
         const auto& p_v_in = detail::GetRealImagParts<real_t>(v_in);
         const auto& v_in_r = p_v_in.first;
         const auto& v_in_i = p_v_in.second;

         // Set up ModalIntegrals; complex and separate real/imag. parts.
         const mat_t<T> mat = random::RandomMidasMatrix<T>(n_modals, n_modals);
         const mat_t<real_t> mat_zr(n_modals, n_modals, 0);
         const mat_t<T> mat_zc(n_modals, n_modals, 0);
         const auto& p_mat = detail::GetRealImagParts<real_t>(mat);
         const auto& mat_r = p_mat.first;
         const auto& mat_i = p_mat.second;
         ModalIntegrals<T> mi(std::vector<std::vector<mat_t<T>>>{{mat}, {mat_zc}, {mat_zc}});
         ModalIntegrals<real_t> mi_r(std::vector<std::vector<mat_t<real_t>>>{{mat_r},{mat_zr},{mat_zr}});
         ModalIntegrals<real_t> mi_i(std::vector<std::vector<mat_t<real_t>>>{{mat_i},{mat_zr},{mat_zr}});

         // We'll loop over these types; down/forward/up, transposed and not.
         const std::set<In> s_which = {-1, 0, 1};
         const std::set<intop_t> s_intop = {intop_t::STANDARD, intop_t::TRANSP, intop_t::CONJ};

         //! Settings depending on contraction type.
         const std::map<In, Uin> m_which_to_size_out =
            {  { -1,   n_virt }
            ,  {  0,   n_virt*n_virt }
            ,  {  1,   n_virt*n_virt*n_virt }
            };
         const std::map<In, ModeCombi> m_which_to_mc_in =
            {  { -1,   ModeCombi(std::vector<In>{0,1}, -1) }
            ,  {  0,   ModeCombi(std::vector<In>{0,1}, -1) }
            ,  {  1,   ModeCombi(std::vector<In>{1,2}, -1) }
            };
         const std::map<In, ModeCombi> m_which_to_mc_out =
            {  { -1,   ModeCombi(std::vector<In>{1}, -1) }
            ,  {  0,   ModeCombi(std::vector<In>{0,1}, -1) }
            ,  {  1,   ModeCombi(std::vector<In>{0,1,2}, -1) }
            };

         // Then loop through it all and do contractions.
         for(const auto& intop: s_intop)
         {
            for(const auto& which: s_which)
            {
               const auto& mc_in = m_which_to_mc_in.at(which);
               const auto& mc_out = m_which_to_mc_out.at(which);
               const auto& size_out = m_which_to_size_out.at(which);

               // Complex calculation.
               vec_t<T> v_out(size_out, T(0));
               mi.Contract(mc_out, mc_in, m, o, v_out, v_in, which, intop);

               // Separate real/imag. calculations, then combine.
               vec_t<real_t> v_ctrl_rr_contrib(size_out, real_t(0));
               vec_t<real_t> v_ctrl_ri_contrib(size_out, real_t(0));
               vec_t<real_t> v_ctrl_ir_contrib(size_out, real_t(0));
               vec_t<real_t> v_ctrl_ii_contrib(size_out, real_t(0));
               mi_r.Contract(mc_out, mc_in, m, o, v_ctrl_rr_contrib, v_in_r, which, intop);
               mi_r.Contract(mc_out, mc_in, m, o, v_ctrl_ri_contrib, v_in_i, which, intop);
               mi_i.Contract(mc_out, mc_in, m, o, v_ctrl_ir_contrib, v_in_r, which, intop);
               mi_i.Contract(mc_out, mc_in, m, o, v_ctrl_ii_contrib, v_in_i, which, intop);
               vec_t<real_t> v_ctrl_r = (intop == intop_t::CONJ) ? vec_t<real_t>(v_ctrl_rr_contrib + v_ctrl_ii_contrib) : vec_t<real_t>(v_ctrl_rr_contrib - v_ctrl_ii_contrib);
               vec_t<real_t> v_ctrl_i = (intop == intop_t::CONJ) ? vec_t<real_t>(v_ctrl_ri_contrib - v_ctrl_ir_contrib) : vec_t<real_t>(v_ctrl_ri_contrib + v_ctrl_ir_contrib);

               // Assertions.
               for(Uin i = 0; i < v_out.Size(); ++i)
               {
                  real_t one(1);
                  real_t diff_r = std::real(v_out[i]) - v_ctrl_r[i];
                  real_t diff_i = std::imag(v_out[i]) - v_ctrl_i[i];
                  UNIT_ASSERT_FEQUAL_PREC(one+diff_r, one, 4, "Re part, "+info(which, intop, i));
                  UNIT_ASSERT_FEQUAL_PREC(one+diff_i, one, 4, "Im part, "+info(which, intop, i));
               }
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Base class with some hardcoded objects for testing that historic
    *    behaviour remains unbroken.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitTestsBase
      :  public cutee::test
   {
      //! Set up operator for this test.
      OpDef GetOpDef() const;

      //! Set up basis for this test.
      BasDef GetBasDef(const OpDef& arOpDef) const;

      //! Set up the primitive one-mode integrals.
      OneModeInt GetOneModeInt(const OpDef& arOpDef, const BasDef& arBasDef) const;

      //! Set up the modals for the primitive->modal integrals transformation.
      template<typename U = Nb>
      GeneralDataCont<U> GetModals() const;

      //! The offsets of each mode-block in the DataCont from GetModals().
      std::vector<In> GetModalOffsets() const;

      //! The modal basis limits used in these tests.
      std::vector<In> GetModalBasisLimits() const;

      //! The reference data for controlling, initial primitive integrals.
      std::vector<std::vector<mat_t<T>>> GetControlPrim() const;

      //! The reference data for controlling, after trans. prim.->modal integrals.
      std::vector<std::vector<mat_t<T>>> GetControl() const;

      void setup() override
      {
         gOperatorDefs.ReSet();
      }

      void teardown() override
      {
         gOperatorDefs.ReSet();
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from PrimitiveIntegrals object.
    *
    * This is actually just a test that the hardcoded primitive integrals are
    * correct -- if this one fails the other tests that inherit from
    * InitTestsBase are not to be trusted.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitFromPrimitiveIntegralsNoModalTrans
      :  public InitTestsBase<T>
   {
      void run() override
      {
         const auto opdef = this->GetOpDef();
         const auto basdef = this->GetBasDef(opdef);
         prim_int_t<T> pi(&opdef, &basdef);
         ModalIntegrals<T> mi = midas::vcc::modalintegrals::ConstructModalIntegrals(pi);
         auto assert = detail::CheckIntegrals(mi, this->GetControlPrim(), 7);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from PrimitiveIntegrals object and
    *    prim.->modal transformation coefficients.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitFromPrimitiveIntegralsWithModalTrans
      :  public InitTestsBase<T>
   {
      void run() override
      {
         const auto opdef = this->GetOpDef();
         const auto basdef = this->GetBasDef(opdef);
         prim_int_t<T> pi(&opdef, &basdef);
         ModalIntegrals<T> mi = midas::vcc::modalintegrals::ConstructModalIntegrals
            (  pi
            ,  this->template GetModals<T>()
            ,  this->GetModalOffsets()
            ,  this->GetModalBasisLimits()
            );
         auto assert = detail::CheckIntegrals(mi, this->GetControl(), 8);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from PrimitiveIntegrals object and
    *    prim.->modal transformation coefficients **multiplied by unit complex
    *    number** to ensure complex conjugation is handled correctly!
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitFromPrimitiveIntegralsWithModalTransComplex
      :  public InitTestsBase<T>
   {
      void run() override
      {
         static_assert(midas::type_traits::IsComplexV<T>, "Expected complex type.");

         const auto opdef = this->GetOpDef();
         const auto basdef = this->GetBasDef(opdef);
         const T scale(cos(C_PI/3.0), sin(C_PI/3.0));
         GeneralDataCont<T> dc_modals = this->template GetModals<T>();
         dc_modals.Scale(scale);
         prim_int_t<T> pi(&opdef, &basdef);
         ModalIntegrals<T> mi = midas::vcc::modalintegrals::ConstructModalIntegrals
            (  pi
            ,  dc_modals
            ,  this->GetModalOffsets()
            ,  this->GetModalBasisLimits()
            );
         auto assert = detail::CheckIntegrals(mi, this->GetControl(), 8);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from Vscf object.
    *
    * Sets up basis, operator, primitive integrals, and ultimately a Vscf
    * object. Fills in some random modal coefficients. Then constructs
    * ModalIntegrals based on Vscf data.
    * The reference values for assertions were computed using the former
    * Integrals class.
    *
    * The setup is _very_ cumbersome, but is more or the less the only way to
    * do it at the time of writing (Jan 2019).
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitFromVscf
      :  public InitTestsBase<T>
   {
      void run() override
      {
         auto opdef = this->GetOpDef();
         auto basdef = this->GetBasDef(opdef);
         auto omi = this->GetOneModeInt(opdef, basdef);
         VscfCalcDef vscfcalcdef;
         std::vector<In> occvec = {0,0};
         vscfcalcdef.SetOccVec(occvec);
         vscfcalcdef.SetModalLimits(this->GetModalBasisLimits());
         Vscf vscf(&vscfcalcdef, &opdef, &basdef, &omi);
         *vscf.pModals() = this->GetModals();
         ModalIntegrals<T> mi = midas::vcc::modalintegrals::InitFromVscf<T>(vscf, "name");
         auto assert = detail::CheckIntegrals(mi, this->GetControl(), 8);
         UNIT_ASSERT(assert.first, assert.second);
         UNIT_ASSERT_EQUAL(mi.Name(), std::string("name"), "Wrong name.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from Vscf object.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitOverlaps
      :  public cutee::test
   {
      void run() override
      {
         mat_t<T> L(3,2);
         L[0][0] = 1;  L[0][1] = 1;
         L[1][0] =-1;  L[1][1] = 0;
         L[2][0] = 0;  L[2][1] = 1;
         mat_t<T> R(3,2);
         R[0][0] = 0;  R[0][1] = 1;
         R[1][0] = 2;  R[1][1] = 0;
         R[2][0] = 0;  R[2][1] = 0;
         mat_t<T> O(3,3);
         O[0][0] = 1;  O[0][1] = 0;  O[0][2] =-1;
         O[1][0] = 0;  O[1][1] = 1;  O[1][2] = 0;
         O[2][0] =-1;  O[2][1] = 0;  O[2][2] = 1;
         // Control:
         mat_t<T> C(2,2);
         C[0][0] = -2;  C[0][1] =  1;
         C[1][0] =  0;  C[1][1] =  0;
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            L[2][0] += T(0, 1);
            R[1][1] += T(0, 1);
            O[1][0] += T(0, 1);
            C[0][1] += T(0,-1);
         }
         UNIT_ASSERT(check::MatricesAreEqual(C, mat_t<T>(L.Conjugate()*O*R), 0), "Control unexpected value.");

         std::vector<std::vector<mat_t<T>>> ctrl = {{C}};
         std::vector<mat_t<T>> v_l = {L};
         std::vector<mat_t<T>> v_r = {R};
         std::vector<mat_t<T>> v_o = {O};
         ModalIntegrals<T> mi = midas::vcc::modalintegrals::InitOverlaps(v_l, v_r, v_o);
         auto assert = detail::CheckIntegrals(mi, ctrl);
         UNIT_ASSERT(assert.first, assert.second);

         //// Just some dummies for checking these compile: 
         //// - InitOverlapsFromVcc
         //// - InitFockFromVcc
         //// Too complicated to set up a numerical test, that I'll bother now. MBH, Jan 2019.
         //// InitOverlapsFromVcc is checked through test_suite/FranckCondon, though. MBH, May 2019.
         //if constexpr(!midas::type_traits::IsComplexV<T>)
         //{
         //   VccCalcDef vcccalcdef;
         //   OpDef opdef;
         //   BasDef basdef;
         //   OneModeInt omi(&opdef, &basdef);
         //   Vcc vcc(&vcccalcdef, &opdef, &basdef, &omi);
         //   DataCont dc_l;
         //   midas::vcc::modalintegrals::InitOverlapsFromVcc(vcc, dc_l, "name");
         //   midas::vcc::modalintegrals::InitFockFromVcc(vcc, "name");
         //}
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from Vscf object.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitElementary
      :  public cutee::test
   {
      void run() override
      {
         const Uin elem_m = 1;
         const Uin elem_p = 0;
         const Uin elem_q = 2;
         std::vector<Uin> n_modals = {3, 3};
         std::vector<Uin> n_opers = {5, 9};

         // Control: zero-matrices except for the mode with the elementary
         // operator. The OpDef can have more than one operator for each mode,
         // but they'll all be identical, don't know why... historical
         // behaviour. MBH, Jan 2019.
         std::vector<std::vector<mat_t<T>>> ctrl;
         for(Uin m = 0; m < n_modals.size(); ++m)
         {
            mat_t<T> mat(n_modals.at(m), T(0));
            if (m == elem_m)
            {
               mat[elem_p][elem_q] = 1;
            }
            ctrl.emplace_back(n_opers.at(m), mat);
         }

         ModalIntegrals<T> mi = midas::vcc::modalintegrals::ConstructModalIntegralsElemOper<T>
            (  elem_m
            ,  elem_p
            ,  elem_q
            ,  n_modals
            ,  n_opers
            );
         UNIT_ASSERT_EQUAL(mi.NModes(), Uin(n_modals.size()), "Wrong num. modes.");
         for(Uin m = 0; m < mi.NModes(); ++m)
         {
            UNIT_ASSERT_EQUAL(mi.NOper(m), Uin(n_opers.at(m)), "Wrong num. opers.");
            UNIT_ASSERT_EQUAL(mi.NModals(m), Uin(n_modals.at(m)), "Wrong num. modals.");
            for(Uin o = 0; o < mi.NOper(m); ++o)
            {
               UNIT_ASSERT(check::MatricesAreEqual(mi.GetIntegrals(m,o), ctrl.at(m).at(o), 0), "Wrong matrix, m = "+std::to_string(m)+", o = "+std::to_string(o)+".");
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Initialize modal integrals from Vscf object.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct InitElementaryFromVscf
      :  public InitTestsBase<T>
   {
      void run() override
      {
         const Uin elem_m = 1;
         const Uin elem_p = 0;
         const Uin elem_q = 2;
         std::vector<Uin> n_modals = {3, 3};
         std::vector<Uin> n_opers = {5, 9};

         // Set up Vscf. A lot of the stuff in there will not actually be used,
         // but is necessary for a sane Vscf object.
         auto opdef = this->GetOpDef();
         opdef.SetIsElementaryOperator(true);
         opdef.SetElementaryOperatorTo(elem_m, elem_p, elem_q);
         auto basdef = this->GetBasDef(opdef);
         auto omi = this->GetOneModeInt(opdef, basdef);
         VscfCalcDef vscfcalcdef;
         std::vector<In> occvec = {0,0};
         vscfcalcdef.SetOccVec(occvec);
         vscfcalcdef.SetModalLimits(this->GetModalBasisLimits());
         Vscf vscf(&vscfcalcdef, &opdef, &basdef, &omi);
         *vscf.pModals() = this->GetModals();

         // Control: zero-matrices except for the mode with the elementary
         // operator. The OpDef can have more than one operator for each mode,
         // but they'll all be identical, don't know why that's necessary...
         // historical behaviour. MBH, Jan 2019.
         std::vector<std::vector<mat_t<T>>> ctrl;
         for(Uin m = 0; m < n_modals.size(); ++m)
         {
            mat_t<T> mat(n_modals.at(m), T(0));
            if (m == elem_m)
            {
               mat[elem_p][elem_q] = 1;
            }
            ctrl.emplace_back(n_opers.at(m), mat);
         }

         ModalIntegrals<T> mi = midas::vcc::modalintegrals::InitFromVscf<T>
            (  vscf
            ,  "dummy_name"
            );

         UNIT_ASSERT_EQUAL(mi.NModes(), Uin(n_modals.size()), "Wrong num. modes.");
         for(Uin m = 0; m < mi.NModes(); ++m)
         {
            UNIT_ASSERT_EQUAL(mi.NOper(m), Uin(n_opers.at(m)), "Wrong num. opers.");
            UNIT_ASSERT_EQUAL(mi.NModals(m), Uin(n_modals.at(m)), "Wrong num. modals.");
            for(Uin o = 0; o < mi.NOper(m); ++o)
            {
               UNIT_ASSERT(check::MatricesAreEqual(mi.GetIntegrals(m,o), ctrl.at(m).at(o), 0), "Wrong matrix, m = "+std::to_string(m)+", o = "+std::to_string(o)+".");
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Get a single integral matrix element.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct GetElement
      :  public cutee::test
   {
      void run() override
      {
         Uin d = 3;
         Uin i = 1;
         Uin j = 2;
         T zero = 0;
         T val = 42;
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            val += T(0, 60);
         }
         mat_t<T> z(d, d, zero);
         mat_t<T> v(d, d, zero);
         v[i][j] = val;
         std::vector<std::vector<mat_t<T>>> v_mats
            {  {  z, z, z}
            ,  {  z, z, v, z}    // special value is at m = 1, o = 2.
            ,  {  z, z}
            };
         ModalIntegrals<T> mi(std::move(v_mats));
         UNIT_ASSERT_EQUAL(mi.GetElement(0, 0, 0, 0), zero, "Expected zero value.");
         UNIT_ASSERT_EQUAL(mi.GetElement(1, 2, i, j), val, "Expected non-zero value.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Get a (occ.,occ.) integral matrix element.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct GetOccElement
      :  public cutee::test
   {
      void run() override
      {
         Uin occ = 0;
         Uin d = 3;
         T zero = 0;
         T val = 42;
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            val += T(0, 60);
         }
         mat_t<T> z(d, d, zero);
         mat_t<T> v(d, d, zero);
         v[occ][occ] = val;
         std::vector<std::vector<mat_t<T>>> v_mats
            {  {  z, z, z}
            ,  {  z, z, v, z}    // special value is at m = 1, o = 2.
            ,  {  z, z}
            };
         ModalIntegrals<T> mi(std::move(v_mats));
         UNIT_ASSERT_EQUAL(mi.GetOccElement(0, 0), zero, "Expected zero value.");
         UNIT_ASSERT_EQUAL(mi.GetOccElement(1, 2), val, "Expected non-zero value.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Error assertion base class. Does the setup/teardown for ErrorControl,
    *    common to all tests that check for MIDASERRORs.
    ***************************************************************************/
   struct ErrorAssertionBase
      :  public cutee::test
   {
      void setup()
      {
         ErrorControl(ErrorCtrl::SETNUMBER);
         ErrorControl(ErrorCtrl::MUTESTDOUT);
         ErrorControl(ErrorCtrl::MUTEMOUT);
      }
      void teardown()
      {
         ErrorControl(ErrorCtrl::RESETNUMBER);
         ErrorControl(ErrorCtrl::UNMUTESTDOUT);
         ErrorControl(ErrorCtrl::UNMUTEMOUT);
         ErrorControl(ErrorCtrl::SETKILL);
      }
   };


   /************************************************************************//**
    * @brief
    *    Constructs an object where integral matrices of same mode differs in
    *    size, and checks it causes a MIDASERROR.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorConstructorInconsistentSize
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<std::vector<mat_t>> integrals = {{mat_t(1), mat_t(2)}};
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         ModalIntegrals<T> mi(std::move(integrals));
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Constructs an object with a non-square integral matrix and checks it
    *    causes a MIDASERROR.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorConstructorNonSquareMatrix
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<std::vector<mat_t>> integrals = {{mat_t(2,3)}};
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         ModalIntegrals<T> mi(std::move(integrals));
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks accessing integral matrix with mode or operator number
    *    out-of-range causes error.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorGetIntegralsOutOfRange
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<std::vector<mat_t>> integrals {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         ModalIntegrals<T> mi(std::move(integrals));
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.GetIntegrals(1, 2);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks accessing integral matrix element with left/right index
    *    out-of-range causes error.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorGetElementOutOfRange
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         std::vector<std::vector<mat_t>> integrals {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         ModalIntegrals<T> mi(std::move(integrals));
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.GetElement(0, 0, 0, 2);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if passing wrong number of amplitude vectors to
    *    T1Transform().
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorT1TransformNModesMismatch
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> amps {vec_t(2)};
         ModalIntegrals<T> mi(std::move(ints));
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.T1Transform(amps);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if amplitude vectors have wrong dimensions in
    *    T1Transform().
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorT1TransformDimsMismatch
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> amps {vec_t(2), vec_t(4)};
         ModalIntegrals<T> mi(std::move(ints));
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.T1Transform(amps);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if T1-transforming integrals that are anything
    *    but RAW.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorCanOnlyT1TransformRaw
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         using type = typename ModalIntegrals<T>::TransT;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> params {vec_t(2), vec_t(3)};
         ModalIntegrals<T> mi(std::move(ints));
         mi.T1Transform(params);

         // Try T1-transforming T1-integrals.
         {
            ModalIntegrals<T> mi_t1(mi);
            UNIT_ASSERT(mi_t1.GetType() == type::T1, "TransT is not T1.");
            UNIT_ASSERT(ErrorNumber() == 0, "Before T1 on T1-transformed: ErrorNumber not 0.");
            mi_t1.T1Transform(params);
            UNIT_ASSERT(ErrorNumber() != 0, "After T1 on T1-transformed: ErrorNumber still 0.");
            ErrorControl(ErrorCtrl::RESETNUMBER);
         }

         // Try T1-transforming R1-integrals.
         {
            ModalIntegrals<T> mi_r1(mi);
            mi_r1.R1Transform(params);
            UNIT_ASSERT(mi_r1.GetType() == type::R1, "TransT is not R1.");
            UNIT_ASSERT(ErrorNumber() == 0, "Before T1 on R1-transformed: ErrorNumber not 0.");
            mi_r1.T1Transform(params);
            UNIT_ASSERT(ErrorNumber() != 0, "After T1 on R1-transformed: ErrorNumber still 0.");
            ErrorControl(ErrorCtrl::RESETNUMBER);
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if passing wrong number of coefficient vectors to
    *    R1Transform().
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorR1TransformNModesMismatch
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> coefs {vec_t(2)};
         ModalIntegrals<T> mi(std::move(ints), trans_t<T>::T1);
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.R1Transform(coefs);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if coefficient vectors have wrong dimensions in
    *    R1Transform().
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorR1TransformDimsMismatch
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> coefs {vec_t(2), vec_t(4)};
         ModalIntegrals<T> mi(std::move(ints), trans_t<T>::T1);
         UNIT_ASSERT(ErrorNumber() == 0, "ErrorNumber not initially 0.");
         mi.R1Transform(coefs);
         UNIT_ASSERT(ErrorNumber() != 0, "Expected MIDASERROR, but ErrorNumber is still 0.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Checks for hard error if R1-transforming integrals that are anything
    *    but T1-transformed.
    ***************************************************************************/
   template 
      <  typename T
      >
   struct ErrorCanOnlyR1TransformT1
      :  public ErrorAssertionBase
   {
      void run() override
      {
         using mat_t = mat_t<T>;
         using vec_t = vec_t<T>;
         using type = typename ModalIntegrals<T>::TransT;
         std::vector<std::vector<mat_t>> ints {{mat_t(2)}, {mat_t(3), mat_t(3)}};
         std::vector<vec_t> params {vec_t(2), vec_t(3)};
         ModalIntegrals<T> mi(std::move(ints));

         // Try R1-transforming RAW integrals.
         {
            ModalIntegrals<T> mi_raw(mi);
            UNIT_ASSERT(mi_raw.GetType() == type::RAW, "TransT is not RAW.");
            UNIT_ASSERT(ErrorNumber() == 0, "Before R1 on RAW: ErrorNumber not 0.");
            mi_raw.R1Transform(params);
            UNIT_ASSERT(ErrorNumber() != 0, "After R1 on RAW: ErrorNumber still 0.");
            ErrorControl(ErrorCtrl::RESETNUMBER);
         }

         // Try R1-transforming R1-integrals.
         {
            ModalIntegrals<T> mi_r1(mi);
            mi_r1.T1Transform(params);
            mi_r1.R1Transform(params);
            UNIT_ASSERT(ErrorNumber() == 0, "Before R1 on R1-transformed: ErrorNumber not 0.");
            mi_r1.R1Transform(params);
            UNIT_ASSERT(ErrorNumber() != 0, "After R1 on R1-transformed: ErrorNumber still 0.");
            ErrorControl(ErrorCtrl::RESETNUMBER);
         }
      }
   };


} /* namespace midas::test::modalintegrals */

#endif/*MODALINTEGRALSTEST_H_INCLUDED*/
