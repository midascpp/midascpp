#include "test/MatVecTest.h"

#include <complex>

namespace midas::test
{

void MatVecTest()
{
   cutee::suite suite("MatVec test");
   
   // ============================= libmda mat/vec tests ================================
   suite.add_test<libmda::tests::expression::matrix_collection_basic<MidasMatrix> >("Mat expression test");
   suite.add_test<libmda::tests::expression::vector_collection_basic<MidasVector> >("Vec expression test");
   suite.add_test<matrix_vector_collection<MidasMatrix, MidasVector> >("Mat Vec char expression test");
   suite.add_test<mat_vec_mult_collection<MidasMatrix, MidasVector> >("Mat Vec oper mult expression test");
   
   suite.add_test<vector_construction_test<MidasVector> >("vector construction",2);
   suite.add_test<vector_copy_construction_test<MidasVector> >("vector copy construction");
   suite.add_test<vector_implicit_copy_construction_test<MidasVector> >("vector implicit copy construction");
   suite.add_test<vector_copy_assignment_test<MidasVector> >("vector copy assignment");

   suite.add_test<matrix_construction_test<MidasMatrix> >("matrix constrution",2,3);
   suite.add_test<matrix_copy_construction_test<MidasMatrix> >("matrix copy construction");
   suite.add_test<matrix_implicit_copy_construction_test<MidasMatrix> >("matrix implicit copy construction");
   suite.add_test<matrix_copy_assignment_test<MidasMatrix> >("matrix copy assignment");
   
   suite.add_test<mmv::mat_complex_vec_mul_test<MidasMatrix, GeneralMidasVector<std::complex<double> > > >("real matrix times complex vector");
   
   // ============================= Midas mat/vec tests =================================
   suite.add_test<VectorDotTest>("Vector Dot Test");
   suite.add_test<VectorNormTest>("Vector Norm Test");
   suite.add_test<VectorSumEleTest>("Vector SumEle Test");
   suite.add_test<VectorSumAbsEleTest>("Vector SumAbsEle Test");
   suite.add_test<VectorProductAbsEleTest>("Vector ProductAbsEle Test");
   suite.add_test<VectorFindMaxAbsValueTest>("Vector FindMaxAbsValue Test");
   suite.add_test<VectorFindMaxValueTest>("Vector FindMaxValue Test");
   suite.add_test<VectorFindMinValueTest>("Vector FindMinValue Test");
   suite.add_test<VectorSumProdElemsTest>("Vector SumProdElems Test");
   suite.add_test<VectorInsertTest>("Vector Insert Test");
   suite.add_test<VectorEraseTest>("Vector Erase Test");
   suite.add_test<VectorReleaseTest<Nb>>("Vector Release Test, Nb");
   suite.add_test<VectorReleaseTest<std::complex<Nb>>>("Vector Release Test, std::complex<Nb>");
   suite.add_test<VectorDiffNorm2Scaled<Nb>>("Vector DiffNorm2Scaled, Nb");
   suite.add_test<VectorDiffNorm2Scaled<std::complex<Nb>>>("Vector DiffNorm2Scaled, std::complex<Nb>");

   suite.add_test<VectorDiffNorm2Test<Nb>>("Vector DiffNorm2, Nb");
   suite.add_test<VectorDiffNorm2Test<std::complex<Nb>>>("Vector DiffNorm2, std::complex<Nb>");
   suite.add_test<VectorDiffNormTest<Nb>>("Vector DiffNorm, Nb");
   suite.add_test<VectorDiffNormTest<std::complex<Nb>>>("Vector DiffNorm, std::complex<Nb>");
   suite.add_test<VectorDiffOneNormTest<Nb>>("Vector DiffOneNorm, Nb");
   suite.add_test<VectorDiffOneNormTest<std::complex<Nb>>>("Vector DiffOneNorm, std::complex<Nb>");
   
   // complex vector specific
   suite.add_test<VectorComplexDotTest<> >("Vector Complex Dot Test");
   suite.add_test<VectorComplexSelfDotTest<> >("Vector Complex Dot Test");
   suite.add_test<VectorComplexNormTest<> >("Vector Complex Norm Test");
   suite.add_test<VectorComplexSumProdElemsTest<> >("Vector Complex SumProdElems Test");
   
   // matrix 
   suite.add_test<MatrixNormTest>("Matrix Norm Test");
   suite.add_test<MatrixSumEleTest>("Matrix SumEle Test");
   suite.add_test<MatrixSumAbsEleTest>("Matrix SumAbsEle Test");
   suite.add_test<MatrixTraceTest>("Matrix Trace Test");
   suite.add_test<MatrixIsSymmetricTest>("Matrix IsSymmetric Test");
   suite.add_test<MatrixInvertSymmetricTest>("Matrix InvertSymmetric Test");
   suite.add_test<MatrixInvertGeneralSymmetricTest>("Matrix InvertGeneral symmetric Test");
   suite.add_test<MatrixInvertGeneralRealTest>("Matrix InvertGeneral general real Test");
   suite.add_test<MatrixInvertGeneralComplexTest>("Matrix InvertGeneral general complex Test");
   for (const auto& n : {5, 10, 17, 31})
   {
      suite.add_test<MatrixInvertGeneralRandom<Nb>>("MidasMatrix InvertGeneral on random general real matrix, size " + std::to_string(n), n);
      suite.add_test<MatrixInvertGeneralRandom<std::complex<Nb>>>("MidasMatrix InvertGeneral on random general comp matrix, size " + std::to_string(n), n);
   }
   suite.add_test<MatrixInterpolativeDecompositionTest>("Matrix InterpolativeDecomposition test");
   
   suite.add_test<MatrixDiffNorm2Test<Nb>>("Matrix DiffNorm2, Nb");
   suite.add_test<MatrixDiffNorm2Test<std::complex<Nb>>>("Matrix DiffNorm2, std::complex<Nb>");
   suite.add_test<MatrixDiffNormTest<Nb>>("Matrix DiffNorm2, Nb");
   suite.add_test<MatrixDiffNormTest<std::complex<Nb>>>("Matrix DiffNorm2, std::complex<Nb>");
   suite.add_test<MatrixDiffOneNormTest<Nb>>("Matrix DiffNorm2, Nb");
   suite.add_test<MatrixDiffOneNormTest<std::complex<Nb>>>("Matrix DiffNorm2, std::complex<Nb>");
   
   // ============================= Midas iter tests ====================================
   suite.add_test<VectorIterTest>("Vector iter test ");
   suite.add_test<VectorStrideIterTest>("Vector stride iter test ");

   RunSuite(suite);
}

} /* namespace midas::test */
