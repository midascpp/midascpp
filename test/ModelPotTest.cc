#include <iostream>

#include "test/ModelPotTest.h"

namespace midas::test
{

void ModelPotTest()
{
   cutee::suite suite("ModelPot tests");
   
   //
   suite.add_test<MorsePotTest>("MorsePot");
   suite.add_test<DoubleWellPotTest>("DoubleWellPot");
   suite.add_test<PotentialHandlerTest>("PotentialHandler");
   
   //
   RunSuite(suite);
}

} /* namespace midas::test */
