/**
************************************************************************
* 
* @file                Spline1DTest.cc
*
* Created:             04-10-2006
*
* Author:              Daniele Toffoli
*
* Short Description:   Test Spline1D class 
* 
* Last modified: Thu Dec 03, 2009  02:01PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SPLINE1DTESTCASE_H_INCLUDED
#define SPLINE1DTESTCASE_H_INCLUDED

// std headers
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Test.h"
#include "test/CuteeInterface.h"
#include "input/Input.h"
#include "pes/splines/Spline1D.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"

namespace midas::test
{

/**
* Controls the testing of stuff 
* */
struct Spline1DTestCase 
   : public cutee::test
{
   void fx (const MidasVector&  arXog1, MidasVector& arYog)
   {
      In nog1 = arXog1.Size();
      if ( nog1 != arYog.Size())
      {
         arYog.SetNewSize(nog1,false);
      }
      arYog = arXog1;
      arYog.Pow(I_3);
   }
    
   // Run
   void run() 
   {
      // *** then defines the function to be interpolated (a normal cubic from 1 to 5)
      Nb* x_tmp;
      Nb low_bound_ig    = -C_1;
      Nb upper_bound_ig  =  C_1;
      In num_of_steps_ig =  I_10;
      Nb h_ig;
   
      // *** input lower and upper bounds and the # of steps
      h_ig = (upper_bound_ig-low_bound_ig)/Nb(num_of_steps_ig);
   
      In n_ig = num_of_steps_ig+I_1;
      x_tmp = new Nb [n_ig];
      for (In i=I_0; i<n_ig; i++)
      {
         x_tmp[i] = low_bound_ig + i*h_ig;
      }
   
      MidasVector Xig(n_ig, x_tmp);
      delete[] x_tmp;
      MidasVector Yig(n_ig, C_0);
      fx(Xig, Yig);
      MidasVector YigDerivs(n_ig);
   
      // *** then construct the cubic natural spline interpolant
      Spline1D CubicApprox(SPLINEINTTYPE::GENERAL, Xig, Yig, C_3, C_3);
      CubicApprox.GetYigDerivs(YigDerivs);
   
      // and evaluate at few selected points
   
      Nb* x_int;
      Nb low_bound_og    = -C_1;
      Nb upper_bound_og  =  C_1;
      In num_of_steps_og =  I_100;
      Nb h_og;
      h_og = (upper_bound_og-low_bound_og)/Nb(num_of_steps_og);
   
      In n_og = num_of_steps_og+I_1;
      x_int = new Nb [n_og];
      for (In i=I_0; i<n_og; i++)
      {
         x_int[i] = low_bound_og + Nb(i)*h_og;
      }
   
      MidasVector Xog(n_og, x_int);
      delete[] x_int;
      MidasVector Yog(n_og, C_0);
      MidasVector yog_analysis(n_og);
      GetYog(Yog, Xog, Xig, Yig, YigDerivs);
      fx(Xog, yog_analysis);
      
      // *** Then find out some statistics
      Nb Rms;
      RmsDevPerElement(Rms, Yog, yog_analysis, n_og);
      
      // ASSERT
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(Rms,1.0),"RMS not close to zero");
      
      for (In k = I_0; k<n_og; k++)
      {                    
         UNIT_ASSERT(CheckTest(Yog[k],yog_analysis[k]),"Test failed for "+std::to_string(k));
      }
   }

   bool CheckTest(Nb aSpline, Nb aAnalytic)
   {
      return (aSpline-aAnalytic < C_NB_EPSILON);
   }
};

} /* namespace midas::test */

#endif /* SPLINE1DTESTCASE_H_INCLUDED */

