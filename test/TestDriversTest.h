/**
 *******************************************************************************
 * 
 * @file    TestDriversTest.h
 * @date    28-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Tests the TestDrivers class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TESTDRIVERSTEST_H_INCLUDED
#define TESTDRIVERSTEST_H_INCLUDED

#include <algorithm>
#include <sstream>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/TestDrivers.h"
#include "test/CuteeInterface.h"

namespace midas::test::testdrivers
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Test that the default constructor enables running all tests.
    *
    * Since we don't know what constitues _all_ tests in the future, we'll only
    * check that (i) it says it'll run all, (ii) container from Drivers() isn't
    * empty.
    *
    * Would be nice to check for some drivers in Drivers(), but it's not so
    * trivial checking a function pointer.
    ***************************************************************************/
   struct AllDriversByDefault
      :  public cutee::test
   {
      void run() override
      {
         midas::test::TestDrivers td;
         auto drivers = td.Drivers();

         UNIT_ASSERT(td.TestAll(), "Default TestDrivers should test all.");
         UNIT_ASSERT(!drivers.empty(), "Drivers() container shouldn't be empty.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Test that by appending drivers, only the ones appended are returned
    *    from Drivers().
    * 
    * We'll append the three drivers that should always be there: this
    * TestDriversTest, the UnitTestTemplateTest and the PlaygroundRun.
    * Try appending some that are not there, and verify it changes nothing.
    * Try appending same test twice, and Drivers() size is increased
    * accordingly.
    * 
    * Also try some funky upper/lowercase and whitespace formatting to ensure
    * that driver identifiers are correctly parsed.
    ***************************************************************************/
   struct AppendDriver
      :  public cutee::test
   {
      void run() override
      {
         midas::test::TestDrivers td;
         
         // Try appending bad driver.
         bool appended = td.AppendDriver("this driver really shouldn't exist");
         auto drivers = td.Drivers();

         UNIT_ASSERT(!appended, "That driver shouldn't have been appended.");
         UNIT_ASSERT(td.TestAll(), "Default TestDrivers should test all.");
         UNIT_ASSERT(!drivers.empty(), "Drivers() container shouldn't be empty.");

         // Append one.
         appended = td.AppendDriver("testdrivers");
         drivers = td.Drivers();
         UNIT_ASSERT(appended, "Didn't return true for appending 'testdrivers'.");
         UNIT_ASSERT(!td.TestAll(), "Shouldn't still be testing all.");
         UNIT_ASSERT_EQUAL
            (  drivers.size()
            ,  static_cast<decltype(drivers.size())>(1)
            ,  "Drivers() container should contain 1 test."
            );
         // Append the same again.
         appended = td.AppendDriver("testdrivers");
         drivers = td.Drivers();
         UNIT_ASSERT(appended, "Didn't return true for appending 'testdrivers'.");
         UNIT_ASSERT_EQUAL
            (  drivers.size()
            ,  static_cast<decltype(drivers.size())>(2)
            ,  "Drivers() container should contain 2 test."
            );

         // Append another one.
         appended = td.AppendDriver("UNITtestTEMPLAT    e");
         drivers = td.Drivers();
         UNIT_ASSERT(appended, "Didn't return true for appending 'UNITtestTEMPLAT    e'.");
         UNIT_ASSERT(!td.TestAll(), "Shouldn't still be testing all.");
         UNIT_ASSERT_EQUAL
            (  drivers.size()
            ,  static_cast<decltype(drivers.size())>(3)
            ,  "Drivers() container should contain 3 tests."
            );

         // And another one.
         appended = td.AppendDriver("   p L  a   y groundRun  ");
         drivers = td.Drivers();
         UNIT_ASSERT(appended, "Didn't return true for appending '   p L  a   y groundRun  '.");
         UNIT_ASSERT(!td.TestAll(), "Shouldn't still be testing all.");
         UNIT_ASSERT_EQUAL
            (  drivers.size()
            ,  static_cast<decltype(drivers.size())>(4)
            ,  "Drivers() container should contain 4 tests."
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests the Summary() for a default constructed object and one with
    *    appended drivers.
    *
    * For the default object, all drivers should be enabled (true); with
    * drivers appended, only those specified should be true.
    ***************************************************************************/
   struct Summary
      :  public cutee::test
   {
      void run() override
      {
         midas::test::TestDrivers td;

         // All should be true at this point.
         for(const auto& kv: td.Summary())
         {
            UNIT_ASSERT(kv.second, "Did not get 'true' for driver '"+kv.first+"'.");
         }

         // Append some; then only those should be true.
         std::vector<midas::test::TestDrivers::identifier_t> append =
            {  "testdrivers"
            ,  "unittesttemplate"
            ,  "playgroundrun"
            };
         for(const auto& ident: append)
         {
            td.AppendDriver(ident);
         }
         for(const auto& kv: td.Summary())
         {
            bool expected = (std::find(append.begin(), append.end(), kv.first) != append.end());
            UNIT_ASSERT_EQUAL(kv.second, expected, "Unexpected value for driver '"+kv.first+"'.");
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests the Help() output.
    *
    * We'll check that 'testdrivers', 'unittesttemplate' and 'playgroundrun'
    * are listed in the help, and that number of lines is as expected, i.e. all
    * drivers are listed. But can't be more specific since more unit test
    * drivers will be added in the future.
    ***************************************************************************/
   struct Help
      :  public cutee::test
   {
      void run() override
      {
         std::stringstream ss;
         midas::test::TestDrivers::Help(ss);
         std::string line;

         // Read the lines into a vector.
         std::vector<std::string> v;
         while (std::getline(ss, line, '\n'))
         {
            v.push_back(std::move(line));
         }

         // First line should be:
         UNIT_ASSERT_EQUAL
            (  v.front()
            ,  std::string("The valid test driver identifiers are (space and case insensitive):")
            ,  "Wrong initial help line."
            );

         // Check for some drivers that we know should be there. (There should
         // be some whitespace in front.)
         std::vector<midas::test::TestDrivers::identifier_t> idents =
            {  "   testdrivers"
            ,  "   unittesttemplate"
            ,  "   playgroundrun"
            };
         for(const auto& s: idents)
         {
            bool found = (std::find(v.begin(), v.end(), s) != v.end());
            UNIT_ASSERT(found, "Didn't find '"+s+"' line in Help().");
         }

         // Check there are as many lines (+1 for the first line) as there are
         // drivers in a default constructed object.
         midas::test::TestDrivers td;
         auto drivers = td.Drivers();
         UNIT_ASSERT_EQUAL
            (  v.size()
            ,  static_cast<decltype(v.size())>(drivers.size() + 1)
            ,  "Unexpected number of lines in Help() output."
            );
      }
   };


} /* namespace midas::test::testdrivers */

#endif/*TESTDRIVERSTEST_H_INCLUDED*/
