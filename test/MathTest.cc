/**
 *******************************************************************************
 * 
 * @file    MathTest.cc
 * @date    07-09-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for util/Math.h.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#include <iostream>

#include "MathTest.h"

namespace midas::test
{

void MathTest()
{
   cutee::suite suite("Math test");
   
   /**************************************************************************************
    *
    **************************************************************************************/
   // basic
   suite.add_test<GreatestCommonDivisorTest<int>>("GCD test, signed");
   suite.add_test<GreatestCommonDivisorTest<unsigned int>>("GCD test, unsigned");

   // sinc
   suite.add_test<SincTest<float>>("Test of sinc function, float");
   suite.add_test<SincTest<double>>("Test of sinc function, double");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
