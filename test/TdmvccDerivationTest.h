/**
 *******************************************************************************
 * 
 * @file    TdmvccDerivationTest.h
 * @date    21-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDMVCCDERIVATIONTEST_H_INCLUDED
#define TDMVCCDERIVATIONTEST_H_INCLUDED

#include <numeric>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/OperMat.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "test/util/MpiSyncedAssertions.h"
#include "test/util/TransTestUtils.h"
#include "test/Random.h"
#include "lapack_interface/LapackInterface.h"
#include "util/type_traits/Complex.h"
#include "util/Timer.h"

#include "td/tdvcc/trf/TrfTdvccUtils.h"

#include "test/CuteeInterface.h"

namespace midas::test::tdmvcc
{
   using n_modals_t = std::vector<Uin>;
   template<typename T> using mat_t = GeneralMidasMatrix<T>;
   template<typename T> using vec_t = GeneralMidasVector<T>;
   template<typename T> using absval_t = typename mat_t<T>::real_t;

   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   } /* namespace detail */
} /* namespace midas::test::tdmvcc */

namespace midas::test::tdmvcc
{
   using midas::util::matrep::SparseClusterOper;
   using midas::util::matrep::McrOrganizedToStackedVec;

   namespace detail
   {
      template<typename T>
      std::pair<OpDef,ModalIntegrals<T>> RandomOperatorModInts
         (  const n_modals_t& arNModals
         )
      {
         const Uin n_modes = arNModals.size();
         const std::vector<Uin> opers(n_modes, 2);
         const ModeCombiOpRange mcr(n_modes, n_modes);
         const auto coefs = util::GetRandomParams<absval_t<T>>(mcr, opers, false, false, 0);
         auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
         auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, arNModals, 'g');
         return std::make_pair(std::move(opdef), std::move(mod_ints));
      }

      template<typename T>
      std::vector<mat_t<T>> RandomGmMats
         (  const n_modals_t& arNModals
         )
      {
         std::vector<mat_t<T>> v;
         v.reserve(arNModals.size());
         for(const auto& n: arNModals)
         {
            v.emplace_back(random::RandomMidasMatrix<T>(n,n));
         }
         return v;
      }

      template<typename T>
      void SetBlocksRandom
         (  std::vector<mat_t<T>>& arMats
         ,  bool aRowVirtNotOcc
         ,  bool aColVirtNotOcc
         )
      {
         for(auto& mat: arMats)
         {
            MidasAssert(mat.Nrows() > 0, "Nrows() == 0");
            MidasAssert(mat.Ncols() > 0, "Ncols() == 0");
            const Uin r_beg = aRowVirtNotOcc?           1: 0;
            const Uin r_end = aRowVirtNotOcc? mat.Nrows(): 1;
            const Uin c_beg = aColVirtNotOcc?           1: 0;
            const Uin c_end = aColVirtNotOcc? mat.Ncols(): 1;
            for(Uin i = r_beg; i < r_end; ++i)
            {
               for(Uin j = c_beg; j < c_end; ++j)
               {
                  mat[i][j] = random::RandomNumber<T>();
               }
            }
         }
      }

      template<typename T>
      mat_t<T> HMat
         (  const n_modals_t& arNModals
         ,  const OpDef& arOpDef
         ,  const ModalIntegrals<T>& arModInts
         )
      {
         midas::util::matrep::MatRepVibOper<T> mr(arNModals);
         mr.FullOperator(arOpDef, arModInts);
         return std::move(mr).GetMatRep();
      }

      template<typename T>
      mat_t<T> GMat
         (  const n_modals_t& arNModals
         ,  const std::vector<mat_t<T>>& arGmMats
         )
      {
         MidasAssert(arNModals.size() == arGmMats.size(), "Size mismatch.");
         for(Uin m = 0; m < arNModals.size(); ++m)
         {
            MidasAssert(arGmMats.at(m).IsSquare(), "Not square, m = "+std::to_string(m));
            MidasAssert(arNModals.at(m) == arGmMats.at(m).Nrows(), "Dim. mismatch, m = "+std::to_string(m));
         }

         const Uin dim = midas::util::matrep::Product(arNModals);
         mat_t<T> g_mat(dim, dim, T(0));
         for(Uin m = 0; m < arGmMats.size(); ++m)
         {
            midas::util::matrep::MatRepVibOper<T>::DirProdOneModeOpers
               (  arNModals
               ,  std::set<Uin>{m}
               ,  std::vector<mat_t<T>>{arGmMats.at(m)}
               ,  g_mat
               ,  T(1)
               ,  true
               );
         }
         return g_mat;
      }

      template<typename T>
      std::vector<mat_t<T>> HminusGCommutatorPlusIRhoDot
         (  const mat_t<T>& arHMat
         ,  const mat_t<T>& arGMat
         ,  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arLCoefs
         ,  bool aExcludeSingles = false
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::McrExciIndices;
         // |ref> = (1,0,0,...)
         vec_t<T> ref(arSco.FullDim(), T(0));
         ref[0] = 1;
         // |ket> = exp(T)|ref>
         vec_t<T> ket(arSco.FullDim(), T(0));
         ket[0] = 1;
         ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, std::move(ket), T(+1));
         // <bra| = <ref|(1+L)exp(-T)
         vec_t<T> bra = arSco.RefToFullSpace<false>(arLCoefs);
         bra[0] += 1;
         bra = TrfExpClusterOper<true,false>(arSco, arTAmpls, std::move(bra), T(-1));

         const mat_t<T> Hmg = mat_t<T>(arHMat - arGMat);
         const vec_t<T> Hmg_ket = vec_t<T>(Hmg * ket);
         const vec_t<T> bra_Hmg = vec_t<T>(bra * Hmg);
         const vec_t<T> expmT_Hmg_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, Hmg_ket, T(-1));

         std::vector<mat_t<T>> v_res;
         v_res.reserve(arSco.NumModes());
         const n_modals_t& n_modals = arSco.Dims();
         for(const auto& nm: n_modals)
         {
            const Uin mode = v_res.size();
            std::set<Uin> sm = {mode};
            std::vector<Uin> cr(1);
            std::vector<Uin> an(1);
            mat_t<T> m(nm, nm, T(0));
            for(Uin w = 0; w < m.Nrows(); ++w)
            {
               cr.at(0) = w;
               for(Uin v = 0; v < m.Ncols(); ++v)
               {
                  an.at(0) = v;
                  // <bra|[H-g,E^m_wv]|ket>
                  const T comm_val = ShiftOperBraket<false>(bra_Hmg, ket, n_modals, sm, cr, an)
                                   - ShiftOperBraket<false>(bra, Hmg_ket, n_modals, sm, cr, an);
                  // i_rho_dot^m_wv = sum_mu
                  //    ( <bra|[E^m_wv, tau_mu]|ket> <mu'|exp(-T)(H - g)|ket>
                  //    - <bra|[H - g,  tau_mu]|ket> <mu'|exp(-T)E^m_wv |ket>
                  //    )
                  T i_rho_dot = 0;
                  MatRepVibOper<T> mr(n_modals);
                  mr.ShiftOper(sm, cr, an);
                  const mat_t<T> E_m_wv = std::move(mr).GetMatRep();
                  const vec_t<T> bra_E_m_wv = vec_t<T>(bra * E_m_wv);
                  const vec_t<T> E_m_wv_ket = vec_t<T>(E_m_wv * ket);
                  const vec_t<T> expmT_E_m_wv_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, E_m_wv_ket, T(-1));
                  Uin msize_limit = aExcludeSingles ? 2 : 1;
                  for(const auto& [modes, excis]: McrExciIndices(arSco.Mcr(), arSco.Dims()))
                  {
                     if (modes.size() < msize_limit)
                     {
                        continue;
                     }
                     const std::vector<Uin> iref(modes.size(), 0);
                     for(const auto& exci: excis)
                     {
                        T E_mu   = ShiftOperBraket<false>(bra_E_m_wv, ket, n_modals, modes, exci, iref)
                                 - ShiftOperBraket<false>(bra, E_m_wv_ket, n_modals, modes, exci, iref);
                        T Hmg_mu = ShiftOperBraket<false>(bra_Hmg, ket, n_modals, modes, exci, iref)
                                 - ShiftOperBraket<false>(bra, Hmg_ket, n_modals, modes, exci, iref);
                        T e_Hmg  = ShiftOperBraket<false>(ref, expmT_Hmg_ket, n_modals, modes, iref, exci);
                        T e_E    = ShiftOperBraket<false>(ref, expmT_E_m_wv_ket, n_modals, modes, iref, exci);
                        i_rho_dot += E_mu * e_Hmg - Hmg_mu * e_E;
                     }
                  }

                  m[w][v] = comm_val + i_rho_dot;
               }
            }
            v_res.emplace_back(std::move(m));
         }

         return v_res;
      }

      /**
       * v_down vector (part of the RHS for calculating g_up)
       **/
      template<typename T>
      std::vector<vec_t<T>> VDown
         (  const mat_t<T>& arHMat
         ,  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arLCoefs
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::McrExciIndices;

         // |ref> = (1,0,0,...)
         vec_t<T> ref(arSco.FullDim(), T(0));
         ref[0] = 1;
         // |ket> = exp(T)|ref>
         vec_t<T> ket(arSco.FullDim(), T(0));
         ket[0] = 1;
         ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, std::move(ket), T(+1));
         // <bra| = <ref|(1+L)exp(-T)
         vec_t<T> bra = arSco.RefToFullSpace<false>(arLCoefs);
         bra[0] += 1;
         bra = TrfExpClusterOper<true,false>(arSco, arTAmpls, std::move(bra), T(-1));

         const vec_t<T> H_ket = vec_t<T>(arHMat * ket);
         const vec_t<T> bra_H = vec_t<T>(bra * arHMat);
         const vec_t<T> expmT_H_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, H_ket, T(-1));

         std::vector<vec_t<T>> v_res;
         v_res.reserve(arSco.NumModes());
         const n_modals_t& n_modals = arSco.Dims();
         for(const auto& nm: n_modals)
         {
            const Uin mode = v_res.size();
            std::set<Uin> sm = {mode};
            std::vector<Uin> cr{0};
            std::vector<Uin> an(1);
            vec_t<T> v(nm-1, T(0));
            for(Uin a = 0; a < v.Size(); ++a)
            {
               an.at(0) = a+1;
               // <bra|[H,E^m_ia]|ket>
               const T comm_val = ShiftOperBraket<false>(bra_H, ket, n_modals, sm, cr, an)
                                - ShiftOperBraket<false>(bra, H_ket, n_modals, sm, cr, an);
               //  sum_mu
               //    ( <bra|[E^m_ia, tau_mu]|ket> <mu'|exp(-T)H|ket>
               //    - <bra|[H, tau_mu]|ket> <mu'|exp(-T)E^m_ia |ket>
               //    )
               T add = 0;
               MatRepVibOper<T> mr(n_modals);
               mr.ShiftOper(sm, cr, an);
               const mat_t<T> E_m_ia = std::move(mr).GetMatRep();
               const vec_t<T> bra_E_m_ia = vec_t<T>(bra * E_m_ia);
               const vec_t<T> E_m_ia_ket = vec_t<T>(E_m_ia * ket);
               const vec_t<T> expmT_E_m_ia_ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, E_m_ia_ket, T(-1));
               for(const auto& [modes, excis]: McrExciIndices(arSco.Mcr(), arSco.Dims()))
               {
                  if (modes.size() < 2)
                  {
                     continue;
                  }
                  const std::vector<Uin> iref(modes.size(), 0);
                  for(const auto& exci: excis)
                  {
                     T E_mu   = ShiftOperBraket<false>(bra_E_m_ia, ket, n_modals, modes, exci, iref)
                              - ShiftOperBraket<false>(bra, E_m_ia_ket, n_modals, modes, exci, iref);
                     T H_mu   = ShiftOperBraket<false>(bra_H, ket, n_modals, modes, exci, iref)
                              - ShiftOperBraket<false>(bra, H_ket, n_modals, modes, exci, iref);
                     T e_H    = ShiftOperBraket<false>(ref, expmT_H_ket, n_modals, modes, iref, exci);
                     T e_E    = ShiftOperBraket<false>(ref, expmT_E_m_ia_ket, n_modals, modes, iref, exci);
                     add += E_mu * e_H - H_mu * e_E;
                  }
               }

               v[a] = comm_val + add;
            }
            v_res.emplace_back(std::move(v));
         }

         return v_res;
      }

      /**
       * v_down vector (part of the RHS for calculating g_up)
       **/
      template<typename T>
      mat_t<T> MDownDown
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arLCoefs
         ,  In aMode1
         ,  In aMode2
         ,  In aNOneMode
         ,  In aOneModeOff
         )
      {
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::TrfVccErrVec;
         using midas::util::matrep::TrfVccEtaVec;
         using midas::util::matrep::TrfVccLJac;
         using midas::util::matrep::OperMat;
         using midas::util::matrep::ShiftOperBraketLooper;

         const auto& n_modals = arSco.Dims();
         const auto& mcr = arSco.Mcr();
         auto n_exci = arSco.ContainsEmptyMC() ? arSco.NumAmpls() - aNOneMode - 1 : arSco.NumAmpls() - aNOneMode;
         //Mout  << " Number of excitations = " << n_exci << std::endl;
         auto off = aOneModeOff + aNOneMode;
         //Mout  << " Offset = " << off << std::endl;
         mat_t<T> eta_shift_ia_m1(n_modals[aMode1]-1, n_exci, T(0));
         mat_t<T> err_shift_ib_m2(n_exci, n_modals[aMode2]-1, T(0));

         mat_t<T> err_shift_ia_m1(n_modals[aMode1]-1, n_exci, T(0)); // NB: transpose definition
         mat_t<T> eta_shift_ib_m2(n_exci, n_modals[aMode2]-1, T(0)); // NB: transpose definition

         MatRepVibOper<T> shift(n_modals);
         ShiftOperBraketLooper looper(n_modals, mcr);

         // Calculate terms for mode 1
         for(In a_m1=I_1; a_m1<n_modals[aMode1]; ++a_m1)
         {
            shift.ShiftOper(std::set<Uin>{Uin(aMode1)}, std::vector<Uin>{0}, std::vector<Uin>{Uin(a_m1)});
            //Mout  << " MatRep of E_^" << aMode1 << "_{0," << a_m1 << "}:\n" << shift.GetMatRep() << std::endl;
            OperMat<T> E_ia(n_modals, shift.GetMatRep());

            auto err = TrfVccErrVec(E_ia, arSco, arTAmpls);
            //Mout  << " m=" << aMode1 << ", <mu'|exp(-T)E_{0," << a_m1 << "}|Psi> = " << err << std::endl;
            err_shift_ia_m1.AssignRow(err, a_m1-1, off);

            auto eta = TrfVccEtaVec(E_ia, looper, arSco, arTAmpls);
            Axpy(eta, TrfVccLJac(E_ia, looper, arSco, arTAmpls, arLCoefs), T(+1.));
            //Mout  << " m=" << aMode1 << ", <Psi'|[E_{0," << a_m1 << "},tau_mu]|Psi> = " << eta << std::endl;
            eta_shift_ia_m1.AssignRow(eta, a_m1-1, off);
         }
         //Mout  << " err_shift_ia_m1:\n" << err_shift_ia_m1 << std::endl;
         //Mout  << " eta_shift_ia_m1:\n" << eta_shift_ia_m1 << std::endl;
         // Calculate terms for mode 2
         for(In b_m2=I_1; b_m2<n_modals[aMode2]; ++b_m2)
         {
            shift.ShiftOper(std::set<Uin>{Uin(aMode2)}, std::vector<Uin>{0}, std::vector<Uin>{Uin(b_m2)});
            //Mout  << " MatRep of E_^" << aMode2 << "_{0," << b_m2 << "}:\n" << shift.GetMatRep() << std::endl;
            OperMat<T> E_ib(n_modals, shift.GetMatRep());

            auto err = TrfVccErrVec(E_ib, arSco, arTAmpls);
            //Mout  << " m'=" << aMode2 << ", <mu'|exp(-T)E_{0," << b_m2 << "}|Psi> = " << err << std::endl;
            err_shift_ib_m2.AssignCol(err, b_m2-1, off);

            auto eta = TrfVccEtaVec(E_ib, looper, arSco, arTAmpls);
            Axpy(eta, TrfVccLJac(E_ib, looper, arSco, arTAmpls, arLCoefs), T(+1.));
            //Mout  << " m'=" << aMode2 << ", <Psi'|[E_{0," << b_m2 << "},tau_mu]|Psi> = " << eta << std::endl;
            eta_shift_ib_m2.AssignCol(eta, b_m2-1, off);
         }
         //Mout  << " err_shift_ib_m2:\n" << err_shift_ib_m2 << std::endl;
         //Mout  << " eta_shift_ib_m2:\n" << eta_shift_ib_m2 << std::endl;

         auto mdd = mat_t<T>(eta_shift_ia_m1 * err_shift_ib_m2);
         Axpy(mdd, mat_t<T>(err_shift_ia_m1 * eta_shift_ib_m2), T(-1));
         return mdd;
      }

      /**
       * MCR(compl) = MCR(full) \ MCR(exci)
       **/
      ModeCombiOpRange McrCompl
         (  Uin aNModes
         ,  const ModeCombiOpRange& arMcr
         )
      {
         ModeCombiOpRange mcr_compl(aNModes, aNModes);
         {
            std::vector<ModeCombi> v_erase;
            v_erase.reserve(arMcr.Size());
            for(const auto& mc: arMcr)
            {
               v_erase.emplace_back(mc);
            }
            mcr_compl.Erase(std::move(v_erase));
         }
         return mcr_compl;
      }

      /**
       * exp(T)|ref>
       **/
      template<typename T>
      vec_t<T> ExpPTRef
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         vec_t<T> ket(arSco.FullDim(), T(0));
         ket[0] = 1;
         ket = TrfExpClusterOper<false,false>(arSco, arTAmpls, std::move(ket), T(+1));
         return ket;
      }

      /**
       * <ref|(1+L)exp(-T)
       **/
      template<typename T>
      vec_t<T> Ref1pLExpMT
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arLCoefs
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         vec_t<T> bra = arSco.RefToFullSpace<false>(arLCoefs);
         bra[0] += 1;
         bra = TrfExpClusterOper<true,false>(arSco, arTAmpls, std::move(bra), T(-1));
         return bra;
      }

      /**
       * <chi|exp(-T) M exp(T)|Phi>
       **/
      template<typename T>
      vec_t<T> Chi_ExpMT_Mat_ExpPT_Ref
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arPsi
         ,  const mat_t<T>& arM
         ,  const ModeCombiOpRange& arMcrCompl
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::MatRepVibOper;
         vec_t<T> v = vec_t<T>(arM * arPsi);
         v = TrfExpClusterOper<false,false>(arSco, arTAmpls, std::move(v), T(-1));
         return MatRepVibOper<T>::ExtractToMcrSpace(v, arMcrCompl, arSco.Dims());
      }

      /**
       * <chi|exp(-T)E^m_{i,bm}exp(T)|Phi>
       **/
      template<typename T>
      mat_t<T> Chi_ExpMT_Shift_ExpPT_Ref
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::McrExciIndices;
         using midas::util::matrep::Product;

         const auto& n_modals = arSco.Dims();
         const Uin n_modes = n_modals.size();
         const Uin n_cols = std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size();

         ModeCombiOpRange mcr_compl = McrCompl(n_modes, arSco.Mcr());
         const Uin n_rows = SetAddresses(mcr_compl, n_modals);

         // |ket> = exp(T)|ref>
         const auto ket = ExpPTRef(arSco, arTAmpls);
         const auto& psi = ket;

         mat_t<T> res(n_rows, n_cols, T(0));
         MatRepVibOper<T> mr(n_modals);
         Uin j = 0;
         for(Uin m = 0; m < n_modes; ++m)
         {
            for(Uin b = 1; b < n_modals.at(m); ++b, ++j)
            {
               mr.ShiftOper(std::set<Uin>{m}, std::vector<Uin>{0}, std::vector<Uin>{b});
               const vec_t<T> col = Chi_ExpMT_Mat_ExpPT_Ref(arSco, arTAmpls, psi, mr.GetMatRep(), mcr_compl);
               MidasAssert(col.Size() == res.Nrows(), "Nrows mismatch");
               MidasAssert(j < res.Ncols(), "Ncols mismatch");
               res.AssignCol(col, j);
            }
         }
         MidasAssert(j == res.Ncols(), "Ncols mismatch, final");
         return res;
      }

      /**
       * <Lambda|[M,tau_chi]|Psi>
       **/
      template<typename T>
      vec_t<T> Lambda_Comm_Mat_TauChi_Psi
         (  const vec_t<T>& arPsi
         ,  const vec_t<T>& arLambda
         ,  const mat_t<T>& arM
         ,  const ModeCombiOpRange& arMcrCompl
         ,  const n_modals_t& arNModals
         )
      {
         using midas::util::matrep::McrExciIndices;
         using midas::util::matrep::ShiftOperBraket;
         const auto& mcr_compl = arMcrCompl;
         const auto& n_modals = arNModals;
         const auto& bra = arLambda;
         const auto& ket = arPsi;
         const vec_t<T> bra_M = vec_t<T>(bra * arM);
         const vec_t<T> M_ket = vec_t<T>(arM * ket);
         std::vector<T> v;
         for(const auto& [modes, excis]: McrExciIndices(mcr_compl, n_modals))
         {
            const std::vector<Uin> iref(modes.size(), 0);
            for(const auto& exci: excis)
            {
               v.emplace_back( ShiftOperBraket<false>(bra_M, ket, n_modals, modes, exci, iref)
                             - ShiftOperBraket<false>(bra, M_ket, n_modals, modes, exci, iref)
                     );
            }
         }
         return vec_t<T>(std::move(v));
      }

      /**
       * <Lambda|[E^m_{i,bm},tau_chi]|Psi>
       **/
      template<typename T>
      mat_t<T> Lambda_Comm_Shift_TauChi_Psi
         (  const SparseClusterOper& arSco
         ,  const vec_t<T>& arTAmpls
         ,  const vec_t<T>& arLCoefs
         )
      {
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::McrExciIndices;
         using midas::util::matrep::Product;

         const auto& n_modals = arSco.Dims();
         const Uin n_modes = n_modals.size();
         const Uin n_rows = std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size();

         ModeCombiOpRange mcr_compl = McrCompl(n_modes, arSco.Mcr());
         const Uin n_cols = SetAddresses(mcr_compl, n_modals);

         // |ket> = exp(T)|ref>
         const auto ket = ExpPTRef(arSco, arTAmpls);
         // <bra| = <ref|(1+L)exp(-T)
         const auto bra = Ref1pLExpMT(arSco, arTAmpls, arLCoefs);

         mat_t<T> res(n_rows, n_cols, T(0));
         MatRepVibOper<T> mr(n_modals);
         Uin i = 0;
         for(Uin m = 0; m < n_modes; ++m)
         {
            for(Uin b = 1; b < n_modals.at(m); ++b, ++i)
            {
               mr.ShiftOper(std::set<Uin>{m}, std::vector<Uin>{0}, std::vector<Uin>{b});
               const mat_t<T> E_m_ib = mr.GetMatRep();
               vec_t<T> row = Lambda_Comm_Mat_TauChi_Psi(ket, bra, E_m_ib, mcr_compl, n_modals);
               res.AssignRow(row, i);
            }
         }
         return res;
      }

      template<typename T>
      std::string print_mat(const mat_t<T>& mat)
      {
         constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
         const absval_t<T> cutoff = 1e-13;
         const Uin prec = 6;
         const Uin w = 12;
         std::stringstream ss;
         ss << std::fixed;
         ss << std::setprecision(prec);
         ss << std::right;
         for(Uin i = 0; i < mat.Nrows(); ++i)
         {
            for(Uin j = 0; j < mat.Ncols(); ++j)
            {
               ss << std::setw(w);
               if (midas::util::AbsVal(mat[i][j]) > cutoff)
               {
                  ss << std::real(mat[i][j]);
                  if constexpr(is_complex)
                  {
                     ss << std::showpos << std::setw(w) << std::imag(mat[i][j]) << "*i";
                     ss << std::noshowpos;
                  }
               }
               else
               {
                  ss << "0";
                  if constexpr(is_complex)
                  {
                     ss << std::setw(w+2) << "";
                  }
               }
               ss << ' ';
            }
            ss << '\n';
         }
         return ss.str();
      };


   } /* namespace detail */

   /************************************************************************//**
    * @brief
    *    Asserts that some blocks of g-operator are redundant when determining
    *    it variationally.
    *
    * The variational constraint on g is
    * ~~~
    *    0 = <Lambda|[H-g,E^m_{wv}]|Psi> + i dot{rho}^m_{wv},  for all m,w,v
    * ~~~
    * The derivation shows that
    * -  for MCR closed under deexcitation:
    *    -  equations for `(w,v) != (i,b)` trivially satisfied
    *    -  all but the occ-virt block of g are redundant.
    ***************************************************************************/
   template<typename T>
   struct VariationalGRedundancies
      :  public cutee::test
   {
      VariationalGRedundancies(Uin aNModes, ModeCombiOpRange aMcr)
         :  mNModes(aNModes)
         ,  mMcr(std::move(aMcr))
      {}

      void run() override
      {
         Mout.Mute();
         Mout << "VariationalGRedundancies, name = " << this->name() << std::endl;
         const n_modals_t n_modals(this->mNModes, 3);
         const ModeCombiOpRange& mcr = this->mMcr;
         Mout << "MCR = \n" << mcr << std::endl;
         const SparseClusterOper sco(n_modals, mcr);
         const vec_t<T> t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(util::GetRandomParams<T>(mcr, n_modals));
         const vec_t<T> l_coefs = McrOrganizedToStackedVec<T,GeneralMidasVector>(util::GetRandomParams<T>(mcr, n_modals));

         const auto [opdef, modints] = detail::RandomOperatorModInts<T>(n_modals);
         const std::vector<mat_t<T>> v_gm = detail::RandomGmMats<T>(n_modals);
         const mat_t<T> H_mat = detail::HMat(n_modals, opdef, modints);
         const mat_t<T> g_mat = detail::GMat(n_modals, v_gm);

         const std::vector<mat_t<T>> v_res = detail::HminusGCommutatorPlusIRhoDot(H_mat, g_mat, sco, t_ampls, l_coefs);
         //Mout << "MBH-debug: initial results!" << std::endl;
         for(const auto& mat: v_res)
         {
            //Mout << "MBH-debug: mat = \n" << detail::print_mat(mat) << std::endl;
         }

         for(const bool r_virt: {false,true})
         {
            for(const bool c_virt: {false,true})
            {
               auto new_v_gm = v_gm;
               detail::SetBlocksRandom(new_v_gm, r_virt, c_virt);
               const mat_t<T> new_g_mat = detail::GMat(n_modals, new_v_gm);
               const std::vector<mat_t<T>> new_v_res = detail::HminusGCommutatorPlusIRhoDot(H_mat, new_g_mat, sco, t_ampls, l_coefs);
               // Mout << "MBH-debug: compare with new random g^m for ("
               //   << (r_virt? "virt": "occ") << "-"
               //   << (c_virt? "virt": "occ") << ")-block"
               //   << std::endl;
               for(  auto it0 = v_res.begin(), it = new_v_res.begin()
                  ;  it0 != v_res.end() && it != new_v_res.end()
                  ;  ++it0, ++it
                  )
               {
                  const mat_t<T> diff = mat_t<T>(*it - *it0);
                  //Mout << "MBH-debug: diff = \n" << detail::print_mat(diff) << std::endl;
               }
            }
         }

         UNIT_ASSERT(true, "VariationalGRedundancies test here!");
      }

      private:
         const Uin mNModes = 0;
         const ModeCombiOpRange mMcr;
   };

   /************************************************************************//**
    * @brief
    *    Checks singularities of coef. matrix when determining G variationally.
    *
    * For 1-mode ampls. included, the variational constraint on g,
    * ~~~
    *    0 = <Lambda|[H-g,E^m_{wv}]|Psi> + i dot{rho}^m_{wv},  for all m,w,v
    * ~~~
    * reduces to solving equations for the occ-virt block;
    * ~~~
    *    Ag = h
    * where
    *    h_{m,b^m} := sum_chi
    *       ( <Lambda|[H,tau_chi]         |Psi><chi|exp(-T)E^m_{ib^m}exp(T)|Phi>
    *       - <Lambda|[E^m_{ib^m},tau_chi]|Psi><chi|exp(-T)H         exp(T)|Phi>)
    *
    *    A_{m bm, m' am'} := sum_chi
    *       ( <Lambda|[E^m'_{i,am'},tau_chi]|Psi><chi|exp(-T)E^m_{i,bm}  exp(T)|Phi>
    *       - <Lambda|[E^m_{i,bm}  ,tau_chi]|Psi><chi|exp(-T)E^m'_{i,am'}exp(T)|Phi>
    *       
    *    g_{m' am'} := g^{m'}_{i,am'}
    * ~~~
    *
    * A is (complex) anti-symmetric; it's eigenvalues should therefore come in
    * +/- pairs, and if its size is odd, there should be an eigenvalue of zero,
    * making it singular.
    *    - verify this
    *    - if singular; is h in column space anyway?
    ***************************************************************************/
   template<typename T>
   struct VariationalGCoefMatrixSingularities
      :  public cutee::test
   {
      VariationalGCoefMatrixSingularities
         (  n_modals_t aNModals
         ,  ModeCombiOpRange aMcr
         ,  bool aZeroT1
         ,  bool aZeroL1
         )
         :  mNModals(std::move(aNModals))
         ,  mMcr(std::move(aMcr))
         ,  mZeroT1(aZeroT1)
         ,  mZeroL1(aZeroL1)
      {}

      void run() override
      {
         Mout.Mute();
         Mout << "VariationalGCoefMatrixSingularities, name = \n"
            << this->name() << std::endl;
         Mout << std::boolalpha;
         Mout << "   mZeroT1 = " << this->mZeroT1 << std::endl;
         Mout << "   mZeroL1 = " << this->mZeroL1 << std::endl;
         n_modals_t n_modals = this->mNModals;
         ModeCombiOpRange mcr = this->mMcr;

         const n_modals_t n_modals_init = n_modals;

         //Adding dummy mode with 2 modals (A still singular!)...
         //if ((std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size()) % 2 != 0)
         //{
         //   n_modals.emplace_back(2);
         //   mcr.Insert(std::vector<ModeCombi>{ModeCombi(std::set<In>{mcr.LargestMode()+1},-1)});
         //}

         ////Adding 2 dummy modes with 3,2 modals and 2-mode combi. (Doesn't work.)
         //if ((std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size()) % 2 != 0)
         //{
         //   n_modals.emplace_back(3);
         //   n_modals.emplace_back(2);
         //   const In m0 = mcr.LargestMode() + 1;
         //   const In m1 = m0 + 1;
         //   mcr.Insert(std::vector<ModeCombi>
         //      {  ModeCombi(std::set<In>{m0},-1)
         //      ,  ModeCombi(std::set<In>{m1},-1)
         //      ,  ModeCombi(std::set<In>{m0,m1},-1)
         //      });
         //}

         //////Adding 3 dummy modes with 2,2,2 modals and 2-mode combis. (Doesn't work.)
         //std::tuple<bool,std::vector<Uin>,std::vector<ModeCombi>> zero_mod_ints = {false, {}, {}};
         //if ((std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size()) % 2 != 0)
         //{
         //   n_modals.emplace_back(2);
         //   n_modals.emplace_back(2);
         //   n_modals.emplace_back(2);
         //   const In m0 = mcr.LargestMode() + 1;
         //   const In m1 = m0 + 1;
         //   const In m2 = m0 + 2;
         //   zero_mod_ints = {true, {Uin(m0),Uin(m1),Uin(m2)},
         //      {  ModeCombi(std::set<In>{m0},-1)
         //      ,  ModeCombi(std::set<In>{m1},-1)
         //      ,  ModeCombi(std::set<In>{m2},-1)
         //      ,  ModeCombi(std::set<In>{m0,m1},-1)
         //      ,  ModeCombi(std::set<In>{m0,m2},-1)
         //      ,  ModeCombi(std::set<In>{m1,m2},-1)
         //      }};
         //   mcr.Insert(std::get<2>(zero_mod_ints));
         //}

         //// Adding extra modal to a mode to avoid singularity. (Works.)
         //if ((std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size()) % 2 != 0)
         //{
         //   n_modals.front() += 1;
         //}

         // Let top modals have zero cluster parameters.
         for(auto& n: n_modals)
         {
            ++n;
         }
         if ((std::accumulate(n_modals.begin(), n_modals.end(), Uin(0)) - n_modals.size()) % 2 != 0)
         {
            n_modals.front() += 1;
         }


         const Uin n_modes = n_modals.size();
         const ModeCombiOpRange mcr_compl = detail::McrCompl(n_modes, mcr);
         //Mout << "n_modals_init = " << n_modals_init << std::endl;
         //Mout << "n_modals      = " << n_modals << std::endl;
         //Mout << "MCR = \n" << mcr << std::endl;
         //Mout << "MCR compl = \n" << mcr_compl << std::endl;
         const SparseClusterOper sco(n_modals, mcr);
         auto vv_t_ampls = util::GetRandomParams<T>(mcr, n_modals_init);
         auto vv_l_coefs = util::GetRandomParams<T>(mcr, n_modals_init);

         vv_t_ampls = midas::util::matrep::ExtendToSecondarySpace(mcr,n_modals_init,n_modals,vv_t_ampls);
         vv_l_coefs = midas::util::matrep::ExtendToSecondarySpace(mcr,n_modals_init,n_modals,vv_l_coefs);

         for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
         {
            Uin i = std::distance(beg, it);
            if (this->mZeroT1)
            {
               auto& t1m = vv_t_ampls.at(i);
               t1m = std::vector<T>(t1m.size(), T(0));
            }
            if (this->mZeroL1)
            {
               auto& l1m = vv_l_coefs.at(i);
               l1m = std::vector<T>(l1m.size(), T(0));
            }
         }
         //if (std::get<0>(zero_mod_ints))
         //{
         //   for(const auto& mc: std::get<2>(zero_mod_ints))
         //   {
         //      ModeCombiOpRange::const_iterator it;
         //      if (mcr.Find(mc, it))
         //      {
         //         const Uin i = std::distance(mcr.begin(), it);
         //         auto& t_mc = vv_t_ampls.at(i);
         //         t_mc = std::vector<T>(t_mc.size(), T(0));
         //         //auto& l_mc = vv_l_coefs.at(i);
         //         //l_mc = std::vector<T>(l_mc.size(), T(0));
         //      }
         //   }
         //}

         vec_t<T> t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(vv_t_ampls);
         vec_t<T> l_coefs = McrOrganizedToStackedVec<T,GeneralMidasVector>(vv_l_coefs);

         //// Zeroing all cluster params... (Eqs. trivially satisfied.)
         //t_ampls.Zero();
         //l_coefs.Zero();

         //Mout << "MBH-debug: t_ampls = " << t_ampls;
         //Mout << "MBH-debug: l_coefs = " << l_coefs;
         Mout << std::endl;

         auto [opdef, modints] = detail::RandomOperatorModInts<T>(n_modals);
         const std::vector<mat_t<T>> v_gm = detail::RandomGmMats<T>(n_modals);
         const mat_t<T> H_mat = detail::HMat(n_modals, opdef, modints);

         //if (std::get<0>(zero_mod_ints))
         //{
         //   std::vector<std::vector<mat_t<T>>> vv_mi;
         //   for(LocalModeNr m = 0; m < modints.NModes(); ++m)
         //   {
         //      vv_mi.emplace_back();
         //      for(LocalOperNr o = 0; o < modints.NOper(m); ++o)
         //      {
         //         vv_mi.back().emplace_back(modints.GetIntegrals(m,o));
         //      }
         //   }
         //   for(const auto& m: std::get<1>(zero_mod_ints))
         //   {
         //      for(auto& mi: vv_mi.at(m))
         //      {
         //         mi.Zero();
         //      }
         //   }
         //   modints = ModalIntegrals<T>(std::move(vv_mi));
         //}

         // A matrix
         const mat_t<T> chi_expmt_shift_exppt_ref = detail::Chi_ExpMT_Shift_ExpPT_Ref(sco, t_ampls);
         const mat_t<T> lambda_comm_shift_tauchi_psi = detail::Lambda_Comm_Shift_TauChi_Psi(sco, t_ampls, l_coefs);

         //Mout << "MBH-debug: <chi|exp(-T)Eexp(T)|ref> = \n" << detail::print_mat(chi_expmt_shift_exppt_ref) << std::endl;
         //Mout << "MBH-debug: <Lambda|[E,tau_chi]|Psi> = \n" << detail::print_mat(lambda_comm_shift_tauchi_psi) << std::endl;
         const mat_t<T> Aterm = mat_t<T>(lambda_comm_shift_tauchi_psi * chi_expmt_shift_exppt_ref);
         mat_t<T> A = Aterm - Transpose(Aterm);
         //Mout << "MBH-debug: Aterm = \n" << detail::print_mat(Aterm) << std::endl;
         //Mout << "MBH-debug: A = \n" << detail::print_mat(A) << std::endl;

         // Right hand side
         const auto ket = detail::ExpPTRef(sco, t_ampls);
         const auto bra = detail::Ref1pLExpMT(sco, t_ampls, l_coefs);
         const vec_t<T> chi_expmt_H_exppt_ref = detail::Chi_ExpMT_Mat_ExpPT_Ref(sco, t_ampls, ket, H_mat, mcr_compl);
         const vec_t<T> lambda_comm_H_tauchi_psi = detail::Lambda_Comm_Mat_TauChi_Psi(ket, bra, H_mat, mcr_compl, n_modals);
         //Mout << "MBH-debug: <chi|exp(-T)Hexp(T)|ref> = " << chi_expmt_H_exppt_ref;
         //Mout << "MBH-debug: <Lambda|[H,tau_chi]|Psi> = " << lambda_comm_H_tauchi_psi;
         const vec_t<T> rhs 
            = T(-1)*vec_t<T>(lambda_comm_H_tauchi_psi * chi_expmt_shift_exppt_ref)
            - T(-1)*vec_t<T>(lambda_comm_shift_tauchi_psi * chi_expmt_H_exppt_ref)
            ;
         //Mout << "MBH-debug: rhs = " << rhs;

         // SVD
         const auto svd_sol = GESVD(A, 'A');
         mat_t<T> U, Vt;
         vec_t<absval_t<T>> S;
         LoadU(svd_sol, U);
         LoadVt(svd_sol, Vt);
         LoadS(svd_sol, S);
         //Mout << "MBH-debug: S = " << S;
         //Mout << "U = \n" << U << std::endl;
         //Mout << "Vt = \n" << Vt << std::endl;

         const vec_t<T> U_dag_rhs = vec_t<T>(U.Conjugate() * rhs);
         const vec_t<T> rhs_V = vec_t<T>(rhs * Vt.Conjugate());
         //Mout << "MBH-debug: singular values and U^dag rhs = \n";
         MidasAssert(U_dag_rhs.Size() == S.Size(), "Size...");
         MidasAssert(rhs_V.Size() == S.Size(), "Size...");
         for(Uin i = 0; i < U_dag_rhs.Size(); ++i)
         {
            Mout << S[i] 
               << "   " << std::setw(23) << U_dag_rhs[i] 
               << "   " << std::setw(23) << rhs_V[i] 
               << '\n';
         }
         Mout << std::endl;

         if (Rank(svd_sol) == A.Nrows() && RankAbs(svd_sol) == A.Nrows())
         {
            mat_t<T> A_inv;
            PseudoInverse(svd_sol, A_inv);
            const vec_t<T> g = A_inv * rhs;
            //Mout << "MBH-debug: solution g vec = " << g;
            //Mout << "MBH-debug: Ag - h = " << vec_t<T>(A*g - rhs) << std::endl;

            std::vector<mat_t<T>> v_gm = detail::RandomGmMats<T>(n_modals);
            //std::vector<mat_t<T>> v_gm(n_modes);
            Uin i = 0;
            for(Uin m = 0; m < v_gm.size(); ++m)
            {
               //v_gm.at(m).SetNewSize(n_modals.at(m), false, true);
               for(Uin b = 1; b < n_modals.at(m); ++b, ++i)
               {
                  v_gm.at(m)[0][b] = g[i];
               }
            }
            //Mout << "MBH-debug: solution g mats = \n";
            //for(const auto& gm: v_gm)
            //{
            //   Mout << "MBH-debug: g^m = \n" << detail::print_mat(gm) << std::endl;
            //}

            const mat_t<T> g_mat = detail::GMat(n_modals, v_gm);
            const std::vector<mat_t<T>> v_res = detail::HminusGCommutatorPlusIRhoDot(H_mat, g_mat, sco, t_ampls, l_coefs);
            //for(const auto& mat: v_res)
            //{
            //   Mout << "MBH-debug: check mat = \n" << detail::print_mat(mat) << std::endl;
            //}
         }

         UNIT_ASSERT(true, "VariationalGCoefMatrixSingularities test here!");
      }

      private:
         const n_modals_t mNModals;
         const ModeCombiOpRange mMcr;
         const bool mZeroT1 = false;
         const bool mZeroL1 = false;
   };


   /************************************************************************//**
    * @brief
    *    Checks that the variational condition is satisfied for excluded T1/L1 
    *    when solving Z^m g_{down}^m = eta^{H,m} and setting 
    *    g_{up}^m = omega^{H-g_{down}^m}
    ***************************************************************************/
   template<typename T>
   struct VariationalConditionCheckT1L1Excl
      :  public cutee::test
   {
      VariationalConditionCheckT1L1Excl(n_modals_t aNModals, ModeCombiOpRange aMcr)
         :  mNModals(std::move(aNModals))
         ,  mMcr(std::move(aMcr))
      {
         SetAddresses(this->mMcr, this->mNModals);
      }

      void run() override
      {
         using namespace midas::test::mpi_sync;
         Mout.Mute();
         Mout << "VariationalConditionCheckT1L1Excl, name = \n"
            << this->name() << std::endl;
         const n_modals_t& n_modals = this->mNModals;
         const ModeCombiOpRange& mcr = this->mMcr;
         const Uin n_modes = n_modals.size();
         Mout << "n_modals      = " << n_modals << std::endl;
         Mout << "MCR = \n" << mcr << std::endl;
         auto mcr_copy = mcr;
         auto nparam = SetAddresses(mcr_copy, n_modals);
         const SparseClusterOper sco(n_modals, std::move(mcr_copy));
         auto vv_t_ampls = util::GetRandomParams<T>(mcr, n_modals);
         auto vv_l_coefs = util::GetRandomParams<T>(mcr, n_modals);

         // Zero one-mode amplitudes
         for(auto it = mcr.Begin(1), beg = mcr.begin(), end = mcr.End(1); it != end; ++it)
         {
            Uin i = std::distance(beg, it);
            auto& t1m = vv_t_ampls.at(i);
            t1m = std::vector<T>(t1m.size(), T(0));
            auto& l1m = vv_l_coefs.at(i);
            l1m = std::vector<T>(l1m.size(), T(0));
         }

         //Mout  << " T (T1=0):\n" << vv_t_ampls << "\n"
         //      << " L (L1=0):\n" << vv_l_coefs << "\n"
         //      << std::flush;

         // Get stacked amplitudes
         const vec_t<T> t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(vv_t_ampls);
         const vec_t<T> l_coefs = McrOrganizedToStackedVec<T,GeneralMidasVector>(vv_l_coefs);

         // Get H operator
         auto [opdef, modints] = detail::RandomOperatorModInts<T>(n_modals);

         // Calculate density matrices and eta vector
         auto densmats = midas::util::matrep::VccOneModeDensityMatrices(sco, t_ampls, l_coefs);

         auto omega_H = midas::util::matrep::TrfVccErrVec(n_modals, opdef, modints, mcr, vv_t_ampls);
         auto eta_H = midas::util::matrep::TrfVccEtaVec(n_modals, opdef, modints, mcr, vv_t_ampls);
         Axpy(eta_H, midas::util::matrep::TrfVccLJac(n_modals, opdef, modints, mcr, vv_t_ampls, vv_l_coefs), T(+1));

         // Extract one-mode eta and omega (stored in gu_vec)
         auto [n_onemode, onemode_off, eta_H_1m] = this->ExtractOneModeAmplitudes(eta_H);
         auto [n_onemode_2, onemode_off_2, omega_H_1m] = this->ExtractOneModeAmplitudes(omega_H);
         MPISYNC_UNIT_ASSERT(n_onemode == n_onemode_2, "Number of one-mode amplitudes in eta and omega are not the same");
         MPISYNC_UNIT_ASSERT(onemode_off == onemode_off_2, "One-mode offsets in eta and omega are not the same");

         // Solve g-down equations
         const auto [gd_vec, z_vec] = midas::tdvcc::constraint::SolveVariationalGDownEquations(densmats, eta_H_1m, this->mRegType, this->mEpsilon, false); 

         // Make direct g_up
         const auto gu_direct = this->DirectGUp(gd_vec, omega_H_1m, t_ampls);

         // Construct g matrices
         const auto g_mats_direct_up = this->ConstructGMatrices(gu_direct, gd_vec);
         const auto singles_dot_norms_direct = this->CheckSinglesDerivative(g_mats_direct_up, t_ampls, l_coefs, omega_H, eta_H, sco, onemode_off, n_onemode);
         MPISYNC_UNIT_ASSERT_FZERO_PREC(singles_dot_norms_direct[0], absval_t<T>(1.) + singles_dot_norms_direct[1], 2000, "Derivative of T_1 amplitudes is not zero with direct g_up!");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(singles_dot_norms_direct[2], absval_t<T>(1.) + singles_dot_norms_direct[3], 2000, "Derivative of L_1 amplitudes is not zero with direct g_up!");

#ifndef VAR_MPI   // The modal integrals and OpDef are not synced, and the alt implementation used MPI in the matrix-vector product => error!
         // construct full-space H
         const auto H_mat = detail::HMat(n_modals, opdef, modints);

         // Construct g_up in the variational way (using two different implementations)
         const auto gu_lineq = LinEqGUp(H_mat, z_vec, gd_vec, sco, t_ampls, l_coefs, n_onemode, onemode_off, false);
         const auto gu_lineq_alt = LinEqGUp(H_mat, z_vec, gd_vec, sco, t_ampls, l_coefs, n_onemode, onemode_off, true);
         for(In imode=I_0; imode<n_modes; ++imode)
         {
            //auto diff_var_direct = std::sqrt(gu_direct[imode].DiffNorm2(gu_lineq[imode]));
            //Mout  << " Mode " << imode << ": ||gu_direct - gu_lineq|| = " << diff_var_direct << std::endl;
            auto diff_var_var = std::sqrt(gu_lineq[imode].DiffNorm2(gu_lineq_alt[imode]));
            MPISYNC_UNIT_ASSERT_FZERO_PREC(diff_var_var, gu_lineq[imode].Norm(), 30000, "The two implementations of fully variational g 'up' differ!");
         }
         // Construct g matrices and check singles derivative
         const auto g_mats_lineq_up = this->ConstructGMatrices(gu_lineq, gd_vec);
         const auto singles_dot_norms_lineq = this->CheckSinglesDerivative(g_mats_lineq_up, t_ampls, l_coefs, omega_H, eta_H, sco, onemode_off, n_onemode);
         MPISYNC_UNIT_ASSERT_FZERO_PREC(singles_dot_norms_lineq[2], absval_t<T>(1.) + singles_dot_norms_lineq[3], 2000, "Derivative of L_1 amplitudes is not zero with lineq g_up!");

         const auto g_mat_lineq_g_up = detail::GMat(n_modals, g_mats_lineq_up);

         // Check variational condition
         auto var_eq_lineq = detail::HminusGCommutatorPlusIRhoDot(H_mat, g_mat_lineq_g_up, sco, t_ampls, l_coefs, true);
         std::vector<absval_t<T>> var_errs_lineq(n_modes);
         for(In imode=I_0; imode<n_modes; ++imode)
         {
            var_errs_lineq[imode] = var_eq_lineq[imode].Norm();
            //Mout  << " LinEq g_up, Mode " << imode << " <Psi'|[H-g,E_vw]|Psi> + i*rho_dot_wv:\n" << detail::print_mat(var_eq_lineq[imode]) << std::endl;
         }
         for(In imode=I_0; imode<n_modes; ++imode)
         {
            MPISYNC_UNIT_ASSERT_FZERO_PREC(var_errs_lineq[imode], g_mat_lineq_g_up.Norm(), 100, "Variational condition not satisfied for lineq g_up!");
         }
#endif
      }

      private:
         const n_modals_t mNModals;
         ModeCombiOpRange mMcr;
         midas::tdvcc::RegInverseType mRegType = midas::tdvcc::RegInverseType::XSVD;
         absval_t<T> mEpsilon = absval_t<T>(1.e-20);

         //! Calculate g "up" by requiring T1_dot = 0
         std::vector<vec_t<T>> DirectGUp
            (  const std::vector<vec_t<T>>& aGDownVec
            ,  const std::vector<vec_t<T>>& aOmegaHVec
            ,  const vec_t<T>& aTAmpls
            )  const
         {
            auto n_modes = this->mNModals.size();
            auto result = aOmegaHVec;
            for(In imode=I_0; imode<n_modes; ++imode)
            {
               result[imode] -= midas::tdvcc::constraint::OmegaGDown(aGDownVec, aTAmpls, this->mMcr, this->mNModals, imode);
            }

            return result;
         }

         //! Calculate g "up" by solving linear equations
         std::vector<vec_t<T>> LinEqGUp
            (  const mat_t<T>& aHMat
            ,  const std::vector<mat_t<T>>& aZMats
            ,  const std::vector<vec_t<T>>& aGDownVec
            ,  const SparseClusterOper& arSco
            ,  const vec_t<T>& aTAmpls
            ,  const vec_t<T>& aLCoefs
            ,  In aNOneMode
            ,  In aOneModeOff
            ,  bool aDirectRhs
            )  const
         {
            Timer timer;
            using midas::util::matrep::OperMat;
            auto n_modes = this->mNModals.size();

            std::vector<vec_t<T>> rhs;
            if (  aDirectRhs
               )
            {
               OperMat<T> H(this->mNModals, aHMat);
               rhs = std::move(midas::tdvcc::constraint::UVector(H, this->ConstructGMatrices(std::vector<vec_t<T>>{}, aGDownVec), arSco, aTAmpls, aLCoefs));
            }
            else
            {
               rhs = std::move(detail::VDown(aHMat, arSco, aTAmpls, aLCoefs));
            }

            std::vector<vec_t<T>> gu_vec(n_modes);
            for(In imode=I_0; imode<n_modes; ++imode)
            {
               auto zt = Transpose(aZMats[imode]);
               zt.Scale(T(-1));

               auto u = rhs[imode];

               if (  !aDirectRhs
                  )
               {
                  for(In jmode=I_0; jmode<n_modes; ++jmode)
                  {
                     auto mdd = detail::MDownDown(arSco, aTAmpls, aLCoefs, imode, jmode, aNOneMode, aOneModeOff);
                     u -= vec_t<T>(mdd * aGDownVec[jmode]);
                  }
               }

               gu_vec[imode] = std::move(midas::tdvcc::constraint::SolveLinearEquations(zt, u, this->mRegType, this->mEpsilon, false));
            }
            
            std::ostringstream oss;
            oss << " time spent on variational g 'up' with aDirectRhs = " << std::boolalpha << aDirectRhs << ": ";
            timer.CpuOut(Mout, " CPU" + oss.str());
            //timer.WallOut(Mout, " Wall" + oss.str());

            return gu_vec;
         }

         //! Extract one-mode amplitudes from full vector
         std::tuple<In, In, std::vector<vec_t<T>>> ExtractOneModeAmplitudes
            (  const vec_t<T>& aVec
            )  const
         {
            auto n_modes = this->mNModals.size();
            std::vector<vec_t<T>> onemode(n_modes);
            In n_onemode = 0;
            In onemode_off = 0;
            for(In imode=0; imode<n_modes; ++imode)
            {
               auto len = this->mNModals[imode] - I_1;
               n_onemode += len;
               onemode[imode].SetNewSize(len);
               ModeCombiOpRange::const_iterator it;
               this->mMcr.Find(std::vector<In>{imode}, it);
               auto addr = it->Address();
               if (  imode == 0
                  )
               {
                  onemode_off = addr;
               }
               for(In i=0; i<len; ++i)
               {
                  onemode[imode][i] = aVec[addr+i];
               }
            }

            return std::make_tuple(std::move(n_onemode), std::move(onemode_off), std::move(onemode));
         }

         //! Construct vector of g matrices
         std::vector<mat_t<T>> ConstructGMatrices
            (  const std::vector<vec_t<T>>& aGUp
            ,  const std::vector<vec_t<T>>& aGDown
            )  const
         {
            auto n_modes = this->mNModals.size();
            std::vector<mat_t<T>> g_mats(n_modes);
            for(In imode=I_0; imode<n_modes; ++imode)
            {
               auto& gm = g_mats[imode];
               const auto& n = this->mNModals[imode];
               gm.SetNewSize(n, n, false, true);
               for(In a=I_1; a<n; ++a)
               {
                  if (  !aGDown.empty()
                     )
                  {
                     gm[0][a] = aGDown[imode][a-1];
                  }
                  if (  !aGUp.empty()
                     )
                  {
                     gm[a][0] = aGUp[imode][a-1];
                  }
               }
            }

            return g_mats;
         }

         //! Check singles derivative is zero
         std::array<absval_t<T>,4> CheckSinglesDerivative
            (  const std::vector<mat_t<T>>& aGMats
            ,  const vec_t<T>& aTAmpls
            ,  const vec_t<T>& aLCoefs
            ,  const vec_t<T>& aOmegaH
            ,  const vec_t<T>& aEtaH
            ,  const SparseClusterOper& arSco
            ,  In aOneModeOff
            ,  In aNOneMode
            )  const
         {
            const auto& mcr = this->mMcr;
            const auto& n_modals = this->mNModals;
            auto n_modes = n_modals.size();

            midas::util::matrep::OperMat<T> g_oper_mat(n_modals, false);
            g_oper_mat.Zero();
            g_oper_mat.AddSumOneModeOpers(aGMats, T(1.0));

            auto omega = aOmegaH;
            Axpy(omega, midas::util::matrep::TrfVccErrVec(g_oper_mat, arSco, aTAmpls), T(-1.0));

            auto eta = aEtaH;
            midas::util::matrep::ShiftOperBraketLooper looper(n_modals, mcr);
            Axpy(eta, midas::util::matrep::TrfVccEtaVec(g_oper_mat, looper, arSco, aTAmpls), T(-1.0));
            Axpy(eta, midas::util::matrep::TrfVccLJac(g_oper_mat, looper, arSco, aTAmpls, aLCoefs), T(-1.0));

            //Mout  << " i*s_dot:\n" << omega << "\n"
            //      << "-i*l_dot:\n" << eta << "\n"
            //      << std::endl;

            auto s1_dot_norm = std::sqrt(omega.Norm2(aOneModeOff, aNOneMode));
            auto s_dot_norm = omega.Norm();
            auto l1_dot_norm = std::sqrt(eta.Norm2(aOneModeOff, aNOneMode));
            auto l_dot_norm = eta.Norm();

            return std::array<absval_t<T>, 4>{ std::move(s1_dot_norm), std::move(s_dot_norm), std::move(l1_dot_norm), std::move(l_dot_norm) };
         }
   };

} /* namespace midas::test::tdmvcc */

#endif   /*TDMVCCDERIVATIONTEST_H_INCLUDED*/
