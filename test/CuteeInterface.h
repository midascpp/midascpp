#ifndef CUTEEINTERFACE_HPP_INCLUDED
#define CUTEEINTERFACE_HPP_INCLUDED

#define CUTEE_OSTREAM_UTILITY
#define CUTEE_OSTREAM_UTILITY_TYPE
#include <cutee/cutee.hpp>

namespace midas
{
namespace test
{

//! Run cutee test suite
void RunSuite(cutee::suite& suite);

} /* namespace test */ 
} /* namespace midas */

#endif /* CUTEEINTERFACE_HPP_INCLUDED */
