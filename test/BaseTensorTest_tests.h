#include <iostream>
#include <iomanip>
#include <cmath>
#include <memory>
#include <string>
#include <utility>
#include <random>
#include <type_traits>

#include "input/ProgramSettings.h"
#include "tensor/NiceTensor.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/Scalar.h"
#include "tensor/ZeroTensor.h"
#include "tensor/SimpleTensor.h"
#include "tensor/DotEvaluator_Helpers.h"
#include "tensor/TensorFits.h"
#include "tensor/FitASD.h"
#include "tensor/FitID.h"
#include"libmda/numeric/float_eq.h"
#include "td/mctdh/eom/mctdh_tensor_utils.h"
#include "util/Os.h"

#define NDIM 3 // NB: these cannot be changed without breaking tests...
#define  DIM 3
#define RANK 2


/****************** GENERATE INPUTS *************************/
namespace midas::test::detail
{

template<class T, class... U>
NiceTensor<T> make_simple_simpletensor(T&& offset, U&&... u)
{
   std::vector<unsigned int> dims{static_cast<unsigned>(std::forward<U>(u))...};
   SimpleTensor<T>* result(new SimpleTensor<T>(dims));
   T* ptr = result->GetData();
   for(int i = 0; i < result->TotalSize(); ++i)
   {
      ptr[i] = static_cast<T>(i) + offset;
   }
   return NiceTensor<T>(result);
}

template <class T>
NiceTensor<T> make_test_simpletensor(char aSwitch = 'a')
{
   std::vector<unsigned int> dims{};
   for(int i=0;i<NDIM;i++)
   {
      dims.push_back(DIM);
   }
   SimpleTensor<T>* result(new SimpleTensor<T>(dims));

   T* ptr=result->GetData();

   // Norm_frobenius(a) = 2.60026916323381418082
   // Norm_frobenius(b) = 4.21195421320671556935
   // dot_product(a,b)  = 2.22758702955160696947
   switch (aSwitch)
   {
      case 'a':
      {
         ptr[DIM*(DIM*0+0)+0]=static_cast<T>(-0.46924057824563703001);
         ptr[DIM*(DIM*0+0)+1]=static_cast<T>( 0.26019992910741440717);
         ptr[DIM*(DIM*0+0)+2]=static_cast<T>( 0.39165591544598554741);
         ptr[DIM*(DIM*0+1)+0]=static_cast<T>(-0.84433139045606631079);
         ptr[DIM*(DIM*0+1)+1]=static_cast<T>( 0.61075179806354240597);
         ptr[DIM*(DIM*0+1)+2]=static_cast<T>(-0.20305688316651471226);
         ptr[DIM*(DIM*0+2)+0]=static_cast<T>(-0.99988845475880339109);
         ptr[DIM*(DIM*0+2)+1]=static_cast<T>( 0.68453722349156453930);
         ptr[DIM*(DIM*0+2)+2]=static_cast<T>(-0.59952549809889421617);
         ptr[DIM*(DIM*1+0)+0]=static_cast<T>( 0.73355561251796497224);
         ptr[DIM*(DIM*1+0)+1]=static_cast<T>( 0.38246727361611543294);
         ptr[DIM*(DIM*1+0)+2]=static_cast<T>( 0.22078191586011408631);
         ptr[DIM*(DIM*1+1)+0]=static_cast<T>( 0.28607430820172297103);
         ptr[DIM*(DIM*1+1)+1]=static_cast<T>(-0.03789665937563246878);
         ptr[DIM*(DIM*1+1)+2]=static_cast<T>( 0.23773851153672143077);
         ptr[DIM*(DIM*1+2)+0]=static_cast<T>( 0.49948294340908838684);
         ptr[DIM*(DIM*1+2)+1]=static_cast<T>( 0.87550347425113783650);
         ptr[DIM*(DIM*1+2)+2]=static_cast<T>(-0.40749595891612755416);
         ptr[DIM*(DIM*2+0)+0]=static_cast<T>(-0.11483901604845536681);
         ptr[DIM*(DIM*2+0)+1]=static_cast<T>(-0.17659638863732385339);
         ptr[DIM*(DIM*2+0)+2]=static_cast<T>( 0.72627249324543985409);
         ptr[DIM*(DIM*2+1)+0]=static_cast<T>(-0.42612909296000944792);
         ptr[DIM*(DIM*2+1)+1]=static_cast<T>( 0.60575080769195421837);
         ptr[DIM*(DIM*2+1)+2]=static_cast<T>(-0.31407053823191377617);
         ptr[DIM*(DIM*2+2)+0]=static_cast<T>(-0.20757267965209091898);
         ptr[DIM*(DIM*2+2)+1]=static_cast<T>(-0.09454885957247394224);
         ptr[DIM*(DIM*2+2)+2]=static_cast<T>( 0.19791813677791303405);
         break;
      }
      case 'b':
      {
         ptr[DIM*(DIM*0+0)+0]=static_cast<T>( 7.80657831840039784232e-1);
         ptr[DIM*(DIM*0+0)+1]=static_cast<T>( 1.65200091468683807339e-1);
         ptr[DIM*(DIM*0+0)+2]=static_cast<T>( 6.20101152382640408156e-1);
         ptr[DIM*(DIM*0+1)+0]=static_cast<T>( 3.28976804436915792884e-1);
         ptr[DIM*(DIM*0+1)+1]=static_cast<T>(-3.31566961813355501332e-2);
         ptr[DIM*(DIM*0+1)+2]=static_cast<T>(-5.49067091480621716215e-1);
         ptr[DIM*(DIM*0+2)+0]=static_cast<T>(-1.37929868234360930579e+0);
         ptr[DIM*(DIM*0+2)+1]=static_cast<T>( 1.55872760251013486865e+0);
         ptr[DIM*(DIM*0+2)+2]=static_cast<T>(-3.94797509942364455160e-1);
         ptr[DIM*(DIM*1+0)+0]=static_cast<T>(-1.87494312999762630101e-1);
         ptr[DIM*(DIM*1+0)+1]=static_cast<T>(-5.23746298689742428856e-1);
         ptr[DIM*(DIM*1+0)+2]=static_cast<T>(-1.02258973310082024177e+0);
         ptr[DIM*(DIM*1+1)+0]=static_cast<T>(-1.13845145288954485707e+0);
         ptr[DIM*(DIM*1+1)+1]=static_cast<T>(-5.75336826930973987970e-1);
         ptr[DIM*(DIM*1+1)+2]=static_cast<T>(-1.12166733976699739550e+0);
         ptr[DIM*(DIM*1+2)+0]=static_cast<T>( 2.98882896746668236254e-1);
         ptr[DIM*(DIM*1+2)+1]=static_cast<T>( 1.02227488148687184477e+0);
         ptr[DIM*(DIM*1+2)+2]=static_cast<T>(-5.95392612114416727720e-2);
         ptr[DIM*(DIM*2+0)+0]=static_cast<T>( 9.19735439269228405301e-1);
         ptr[DIM*(DIM*2+0)+1]=static_cast<T>( 3.88778728032143638949e-1);
         ptr[DIM*(DIM*2+0)+2]=static_cast<T>(-8.12252836076070683369e-1);
         ptr[DIM*(DIM*2+1)+0]=static_cast<T>( 1.20937224767564410755e-1);
         ptr[DIM*(DIM*2+1)+1]=static_cast<T>(-1.92667486215412547823e-1);
         ptr[DIM*(DIM*2+1)+2]=static_cast<T>(-1.91429263996605114606e+0);
         ptr[DIM*(DIM*2+2)+0]=static_cast<T>(-8.02771584562882112301e-1);
         ptr[DIM*(DIM*2+2)+1]=static_cast<T>( 4.79805395136556123159e-1);
         ptr[DIM*(DIM*2+2)+2]=static_cast<T>( 4.41045146939060939140e-1);
         break;
      }
      default:
      {
         MIDASERROR("Invalid switch value.");
      }
   }

   return NiceTensor<T>(result);
}

template <class T>
NiceTensor<std::complex<T>> make_complex_test_simpletensor
   (  char aSwitch = 'a'
   )
{
   // Norm_frobenius(a) = 3.7464122772216797
   // Norm_frobenius(b) = 
   // dot_product(a,b)  = 3.3460678974561775 + i*1.8751718877413732
   switch (aSwitch)
   {
      case 'a':
      {
         unsigned dim = 3;
         auto result = NiceSimpleTensor<std::complex<T> >(std::vector<unsigned>{dim, dim, dim});
         auto* ptr = static_cast<SimpleTensor<std::complex<T>>*>(result.GetTensor())->GetData();

         ptr[dim*(dim*0+0)+0]=std::complex<T>(-0.46924057824563703001,  0.00125487566695164329);
         ptr[dim*(dim*0+0)+1]=std::complex<T>( 0.26019992910741440717, -0.46124077824564703001);
         ptr[dim*(dim*0+0)+2]=std::complex<T>( 0.39165591544598554741,  0.19125591544998554741);
         ptr[dim*(dim*0+1)+0]=std::complex<T>(-0.84433139045606631079, -0.47454698513457951324);
         ptr[dim*(dim*0+1)+1]=std::complex<T>( 0.61075179806354240597, -0.12589754632159753466);
         ptr[dim*(dim*0+1)+2]=std::complex<T>(-0.20305688316651471226,  0.00001254835488799753);
         ptr[dim*(dim*0+2)+0]=std::complex<T>(-0.99988845475880339109,  0.11134578955564343443);
         ptr[dim*(dim*0+2)+1]=std::complex<T>( 0.68453722349156453930,  0.13134178956568340443);
         ptr[dim*(dim*0+2)+2]=std::complex<T>(-0.59952549809889421617,  0.25967422585820100000);
         ptr[dim*(dim*1+0)+0]=std::complex<T>( 0.73355561251796497224,  0.96689297983138390000);
         ptr[dim*(dim*1+0)+1]=std::complex<T>( 0.38246727361611543294, -0.98708119526287260000);
         ptr[dim*(dim*1+0)+2]=std::complex<T>( 0.22078191586011408631,  0.94396129050073170000);
         ptr[dim*(dim*1+1)+0]=std::complex<T>( 0.28607430820172297103,  0.01559998363445891400);
         ptr[dim*(dim*1+1)+1]=std::complex<T>(-0.03789665937563246878, -0.74682691474109400000);
         ptr[dim*(dim*1+1)+2]=std::complex<T>( 0.23773851153672143077, -0.19308676570523464000);
         ptr[dim*(dim*1+2)+0]=std::complex<T>( 0.49948294340908838684,  0.22619616867615222000);
         ptr[dim*(dim*1+2)+1]=std::complex<T>( 0.87550347425113783650,  0.44190583694658336000);
         ptr[dim*(dim*1+2)+2]=std::complex<T>(-0.40749595891612755416,  0.55709210822858600000);
         ptr[dim*(dim*2+0)+0]=std::complex<T>(-0.11483901604845536681, -0.24757688902350283000);
         ptr[dim*(dim*2+0)+1]=std::complex<T>(-0.17659638863732385339,  0.35188174989122280000);
         ptr[dim*(dim*2+0)+2]=std::complex<T>( 0.72627249324543985409,  0.97277657237358950000);
         ptr[dim*(dim*2+1)+0]=std::complex<T>(-0.42612909296000944792, -0.54872274217203330000);
         ptr[dim*(dim*2+1)+1]=std::complex<T>( 0.60575080769195421837, -0.96810610877222050000);
         ptr[dim*(dim*2+1)+2]=std::complex<T>(-0.31407053823191377617,  0.22874496425655488000);
         ptr[dim*(dim*2+2)+0]=std::complex<T>(-0.20757267965209091898, -0.12859894214108514000);
         ptr[dim*(dim*2+2)+1]=std::complex<T>(-0.09454885957247394224, -0.27138918642207965000);
         ptr[dim*(dim*2+2)+2]=std::complex<T>( 0.19791813677791303405, -0.47133918641207995000);

         return result;

         break;
      }
      case 'b':
      {
         unsigned dim = 3;
         auto result = NiceSimpleTensor<std::complex<T> >(std::vector<unsigned>{dim, dim, dim});
         auto* ptr = static_cast<SimpleTensor<std::complex<T>>* >(result.GetTensor())->GetData();

         ptr[dim*(dim*0+0)+0]=std::complex<T>( 7.80657831840039784232e-1,  0.82036675285800000);
         ptr[dim*(dim*0+0)+1]=std::complex<T>( 1.65200091468683807339e-1, -0.70354840192600000);
         ptr[dim*(dim*0+0)+2]=std::complex<T>( 6.20101152382640408156e-1,  0.45794613603600000);
         ptr[dim*(dim*0+1)+0]=std::complex<T>( 3.28976804436915792884e-1,  0.13041009318800000);
         ptr[dim*(dim*0+1)+1]=std::complex<T>(-3.31566961813355501332e-2,  0.41928809204900000);
         ptr[dim*(dim*0+1)+2]=std::complex<T>(-5.49067091480621716215e-1,  0.98178289564800000);
         ptr[dim*(dim*0+2)+0]=std::complex<T>(-1.37929868234360930579e+0,  0.57343982420700000);
         ptr[dim*(dim*0+2)+1]=std::complex<T>( 1.55872760251013486865e+0, -0.40202022779100000);
         ptr[dim*(dim*0+2)+2]=std::complex<T>(-3.94797509942364455160e-1, -0.19733561747600000);
         ptr[dim*(dim*1+0)+0]=std::complex<T>(-1.87494312999762630101e-1, -0.02488751718690000);
         ptr[dim*(dim*1+0)+1]=std::complex<T>(-5.23746298689742428856e-1,  0.94377179288600000);
         ptr[dim*(dim*1+0)+2]=std::complex<T>(-1.02258973310082024177e+0,  0.32300693290200000);
         ptr[dim*(dim*1+1)+0]=std::complex<T>(-1.13845145288954485707e+0,  0.78444530726900000);
         ptr[dim*(dim*1+1)+1]=std::complex<T>(-5.75336826930973987970e-1, -0.71765946076000000);
         ptr[dim*(dim*1+1)+2]=std::complex<T>(-1.12166733976699739550e+0,  0.07729040267400000);
         ptr[dim*(dim*1+2)+0]=std::complex<T>( 2.98882896746668236254e-1,  0.11604838454900000);
         ptr[dim*(dim*1+2)+1]=std::complex<T>( 1.02227488148687184477e+0,  0.74235186674600000);
         ptr[dim*(dim*1+2)+2]=std::complex<T>(-5.95392612114416727720e-2,  0.19702055221200000);
         ptr[dim*(dim*2+0)+0]=std::complex<T>( 9.19735439269228405301e-1, -0.53008079093000000);
         ptr[dim*(dim*2+0)+1]=std::complex<T>( 3.88778728032143638949e-1,  0.15747807270500000);
         ptr[dim*(dim*2+0)+2]=std::complex<T>(-8.12252836076070683369e-1,  0.57580134473300000);
         ptr[dim*(dim*2+1)+0]=std::complex<T>( 1.20937224767564410755e-1, -0.18686716621200000);
         ptr[dim*(dim*2+1)+1]=std::complex<T>(-1.92667486215412547823e-1,  0.24590830411700000);
         ptr[dim*(dim*2+1)+2]=std::complex<T>(-1.91429263996605114606e+0,  0.82745318224500000);
         ptr[dim*(dim*2+2)+0]=std::complex<T>(-8.02771584562882112301e-1,  0.18519970207900000);
         ptr[dim*(dim*2+2)+1]=std::complex<T>( 4.79805395136556123159e-1, -0.64938074796600000);
         ptr[dim*(dim*2+2)+2]=std::complex<T>( 4.41045146939060939140e-1,  0.93208379191000000);

         return result;

         break;
      }
      case 'c':
      {
         unsigned dim = 3;
         auto result = NiceSimpleTensor<std::complex<T> >(std::vector<unsigned>{dim, dim});
         auto* ptr = static_cast<SimpleTensor<std::complex<T>>* >(result.GetTensor())->GetData();

         ptr[dim*0+0]=std::complex<T>( 7.80657831840039784232e-1,  0.82036675285800000);
         ptr[dim*0+1]=std::complex<T>( 3.28976804436915792884e-1,  0.13041009318800000);
         ptr[dim*0+2]=std::complex<T>(-1.37929868234360930579e+0,  0.57343982420700000);
         ptr[dim*1+0]=std::complex<T>(-1.87494312999762630101e-1, -0.02488751718690000);
         ptr[dim*1+1]=std::complex<T>(-1.13845145288954485707e+0,  0.78444530726900000);
         ptr[dim*1+2]=std::complex<T>( 2.98882896746668236254e-1,  0.11604838454900000);
         ptr[dim*2+0]=std::complex<T>( 9.19735439269228405301e-1, -0.53008079093000000);
         ptr[dim*2+1]=std::complex<T>( 1.20937224767564410755e-1, -0.18686716621200000);
         ptr[dim*2+2]=std::complex<T>(-8.02771584562882112301e-1,  0.18519970207900000);

         return result;

         break;
      }
      case 'd':
      {
         unsigned dim = 3;
         auto result = NiceSimpleTensor<std::complex<T> >(std::vector<unsigned>{dim, dim});
         auto* ptr = static_cast<SimpleTensor<std::complex<T>>* >(result.GetTensor())->GetData();

         ptr[dim*0+0]=std::complex<T>(-0.46924057824563703001,  0.00125487566695164329);
         ptr[dim*0+1]=std::complex<T>( 0.26019992910741440717, -0.46124077824564703001);
         ptr[dim*0+2]=std::complex<T>( 0.39165591544598554741,  0.19125591544998554741);
         ptr[dim*1+0]=std::complex<T>(-0.84433139045606631079, -0.47454698513457951324);
         ptr[dim*1+1]=std::complex<T>( 0.61075179806354240597, -0.12589754632159753466);
         ptr[dim*1+2]=std::complex<T>(-0.20305688316651471226,  0.00001254835488799753);
         ptr[dim*2+0]=std::complex<T>(-0.99988845475880339109,  0.11134578955564343443);
         ptr[dim*2+1]=std::complex<T>( 0.68453722349156453930,  0.13134178956568340443);
         ptr[dim*2+2]=std::complex<T>(-0.59952549809889421617,  0.25967422585820100000);

         return result;

         break;
      }
      default:
      {
         MIDASERROR("Invalid switch value.");

         return NiceTensor<std::complex<T> >();
      }
   }
}

template <class T>
NiceTensor<T> make_test_canonicaltensor(char aSwitch = 'a')
{
   std::vector<unsigned int> dims{};
   for(int i=0;i<NDIM;i++)
   {
      dims.push_back(DIM);
   }
   CanonicalTensor<T>* result(new CanonicalTensor<T>(dims, RANK));

   T** ptr=result->GetModeMatrices();

   // Haven't calculated norms, etc. for these - simply convert them to
   // SimpleTensors within the test and compare results (assuming that
   // corresponding funcitons work for SimpleTensor).
   switch (aSwitch)
   {
      case 'a':
         ptr[0][DIM*0+0]=static_cast<T>(-0.30316644928721925289);
         ptr[0][DIM*0+1]=static_cast<T>(-0.66551935395687622510);
         ptr[0][DIM*0+2]=static_cast<T>( 0.87482059099410980352);
         ptr[0][DIM*1+0]=static_cast<T>( 0.75681384885549207731);
         ptr[0][DIM*1+1]=static_cast<T>(-0.98050719575263522998);
         ptr[0][DIM*1+2]=static_cast<T>(-0.05727340579392437014);

         ptr[1][DIM*0+0]=static_cast<T>( 0.92338434012564252207);
         ptr[1][DIM*0+1]=static_cast<T>( 0.30681975644951386961);
         ptr[1][DIM*0+2]=static_cast<T>(-0.89076871257751366606);
         ptr[1][DIM*1+0]=static_cast<T>(-0.81390782735601940878);
         ptr[1][DIM*1+1]=static_cast<T>(-0.77119974522307277454);
         ptr[1][DIM*1+2]=static_cast<T>( 0.82796691359587426540);

         ptr[2][DIM*0+0]=static_cast<T>( 0.35784925478030515933);
         ptr[2][DIM*0+1]=static_cast<T>( 0.24966648716567552135);
         ptr[2][DIM*0+2]=static_cast<T>( 0.39056057058631044043);
         ptr[2][DIM*1+0]=static_cast<T>( 0.41817242779004870634);
         ptr[2][DIM*1+1]=static_cast<T>( 0.33825709068906806287);
         ptr[2][DIM*1+2]=static_cast<T>(-0.34278663581003732119);
         break;
      case 'b':
         ptr[0][DIM*0+0]=static_cast<T>( 1.07281432293872036610e+0);
         ptr[0][DIM*0+1]=static_cast<T>( 3.17736198546000603304e-1);
         ptr[0][DIM*0+2]=static_cast<T>( 2.49891105423980464106e-1);
         ptr[0][DIM*1+0]=static_cast<T>(-4.95419602913967360713e-1);
         ptr[0][DIM*1+1]=static_cast<T>(-4.16288578149171861753e-1);
         ptr[0][DIM*1+2]=static_cast<T>(-3.18379116571013420778e-1);

         ptr[1][DIM*0+0]=static_cast<T>(-7.83794187398087149887e-1);
         ptr[1][DIM*0+1]=static_cast<T>( 1.22939942966188087148e+0);
         ptr[1][DIM*0+2]=static_cast<T>(-1.60116761971947219045e+0);
         ptr[1][DIM*1+0]=static_cast<T>( 1.62043490465219069963e-1);
         ptr[1][DIM*1+1]=static_cast<T>(-2.25739407025942906415e+0);
         ptr[1][DIM*1+2]=static_cast<T>( 1.04380414116366915245e+0);

         ptr[2][DIM*0+0]=static_cast<T>(-6.66593124605966980134e-1);
         ptr[2][DIM*0+1]=static_cast<T>(-1.12491368280543454355e-2);
         ptr[2][DIM*0+2]=static_cast<T>(-6.32802088311924082475e-1);
         ptr[2][DIM*1+0]=static_cast<T>(-1.56308518662175965908e+0);
         ptr[2][DIM*1+1]=static_cast<T>(-5.54090224103914952281e-1);
         ptr[2][DIM*1+2]=static_cast<T>( 6.76506354172687340842e-1);
         break;
      default:
         MIDASERROR("Invalid switch value.");
   }

   return NiceTensor<T>( result );
}

template <class T>
NiceTensor<std::complex<T>> make_complex_test_canonicaltensor(char aSwitch = 'a')
{
   using value_t = std::complex<T>;

   std::vector<unsigned int> dims{};
   for(int i=0;i<NDIM;i++)
   {
      dims.push_back(DIM);
   }
   CanonicalTensor<value_t>* result(new CanonicalTensor<value_t>(dims, RANK));

   value_t** ptr=result->GetModeMatrices();

   // Haven't calculated norms, etc. for these - simply convert them to
   // SimpleTensors within the test and compare results (assuming that
   // corresponding funcitons work for SimpleTensor).
   switch (aSwitch)
   {
      case 'a':
         ptr[0][DIM*0+0]=value_t(-0.30316644928721925289,  -0.503589996262 );
         ptr[0][DIM*0+1]=value_t(-0.66551935395687622510,  -0.173315854779 );
         ptr[0][DIM*0+2]=value_t( 0.87482059099410980352,  -0.503524942372 );
         ptr[0][DIM*1+0]=value_t( 0.75681384885549207731,  -0.0159481381817);
         ptr[0][DIM*1+1]=value_t(-0.98050719575263522998,  -0.41232020095  );
         ptr[0][DIM*1+2]=value_t(-0.05727340579392437014,   0.790586377129 );
 
         ptr[1][DIM*0+0]=value_t( 0.92338434012564252207,   0.555369858775 );
         ptr[1][DIM*0+1]=value_t( 0.30681975644951386961,  -0.366012387923 );
         ptr[1][DIM*0+2]=value_t(-0.89076871257751366606,  -0.0384846316691);
         ptr[1][DIM*1+0]=value_t(-0.81390782735601940878,   0.163528183165 );
         ptr[1][DIM*1+1]=value_t(-0.77119974522307277454,   0.000408230659146);
         ptr[1][DIM*1+2]=value_t( 0.82796691359587426540,   0.829057784643 );
 
         ptr[2][DIM*0+0]=value_t( 0.35784925478030515933,  -0.140618456973 );
         ptr[2][DIM*0+1]=value_t( 0.24966648716567552135,   0.930728456143 );
         ptr[2][DIM*0+2]=value_t( 0.39056057058631044043,   0.673627856594 );
         ptr[2][DIM*1+0]=value_t( 0.41817242779004870634,  -0.700444610526 );
         ptr[2][DIM*1+1]=value_t( 0.33825709068906806287,   0.724439862823 );
         ptr[2][DIM*1+2]=value_t(-0.34278663581003732119,  -0.954335825297 );
         break;
      case 'b':
         ptr[0][DIM*0+0]=value_t( 1.07281432293872036610e+0,  -0.697386882426);
         ptr[0][DIM*0+1]=value_t( 3.17736198546000603304e-1,   0.811270606789 );
         ptr[0][DIM*0+2]=value_t( 2.49891105423980464106e-1,   0.0865940179948);
         ptr[0][DIM*1+0]=value_t(-4.95419602913967360713e-1,   0.427952548259 );
         ptr[0][DIM*1+1]=value_t(-4.16288578149171861753e-1,   0.825926574124 );
         ptr[0][DIM*1+2]=value_t(-3.18379116571013420778e-1,   0.642860726166 );

         ptr[1][DIM*0+0]=value_t(-7.83794187398087149887e-1,   0.748813863926 );
         ptr[1][DIM*0+1]=value_t( 1.22939942966188087148e+0,   0.399269900912 );
         ptr[1][DIM*0+2]=value_t(-1.60116761971947219045e+0,   0.312798588377 );
         ptr[1][DIM*1+0]=value_t( 1.62043490465219069963e-1,   0.349673999675 );
         ptr[1][DIM*1+1]=value_t(-2.25739407025942906415e+0,   0.127661663452 );
         ptr[1][DIM*1+2]=value_t( 1.04380414116366915245e+0,  -0.561682369297);
 
         ptr[2][DIM*0+0]=value_t(-6.66593124605966980134e-1,   0.548769653381 );
         ptr[2][DIM*0+1]=value_t(-1.12491368280543454355e-2,   0.926214753945 );
         ptr[2][DIM*0+2]=value_t(-6.32802088311924082475e-1,  -0.605947407957);
         ptr[2][DIM*1+0]=value_t(-1.56308518662175965908e+0,  -0.135354406067);
         ptr[2][DIM*1+1]=value_t(-5.54090224103914952281e-1,  -0.31266600141 );
         ptr[2][DIM*1+2]=value_t( 6.76506354172687340842e-1,  -0.93611197247 );
         break;
      default:
         MIDASERROR("Invalid switch value.");
   }

   return NiceTensor<value_t>( result );
}

template <class T>
NiceTensor<T> make_test_canonicaltensor2d(char aSwitch = 'a')
{
   std::vector<unsigned int> dims{3, 4};
   CanonicalTensor<T>* result(new CanonicalTensor<T>(dims, RANK));

   T** ptr=result->GetModeMatrices();

   // Haven't calculated norms, etc. for these - simply convert them to
   // SimpleTensors within the test and compare results (assuming that
   // corresponding funcitons work for SimpleTensor).
   switch (aSwitch)
   {
      case 'a':
         ptr[0][3*0+0]=static_cast<T>(+0.16672655129030489185);
         ptr[0][3*0+1]=static_cast<T>(+0.91357019495069291537);
         ptr[0][3*0+2]=static_cast<T>(-0.06793548610711830360);
         ptr[0][3*1+0]=static_cast<T>(-0.78331198258868228734);
         ptr[0][3*1+1]=static_cast<T>(+0.91963206788516416168);
         ptr[0][3*1+2]=static_cast<T>(+0.44627082241845106303);

         ptr[1][4*0+0]=static_cast<T>(+0.25774179242033090631);
         ptr[1][4*0+1]=static_cast<T>(-0.54638722761217206614);
         ptr[1][4*0+2]=static_cast<T>(-0.58154101846309869117);
         ptr[1][4*0+3]=static_cast<T>(+0.30552971440030485262);
         ptr[1][4*1+0]=static_cast<T>(-0.83851581482182158123);
         ptr[1][4*1+1]=static_cast<T>(-0.89142796068718377356);
         ptr[1][4*1+2]=static_cast<T>(+0.14299328744946815384);
         ptr[1][4*1+3]=static_cast<T>(-0.83137231176518189280);
         break;
      case 'b':
         ptr[0][3*0+0]=static_cast<T>(-0.75873477425396784746);
         ptr[0][3*0+1]=static_cast<T>(+0.19294385034426664305);
         ptr[0][3*0+2]=static_cast<T>(+0.34467400315792162679);
         ptr[0][3*1+0]=static_cast<T>(-0.58080349247455675954);
         ptr[0][3*1+1]=static_cast<T>(+0.52032222168527586525);
         ptr[0][3*1+2]=static_cast<T>(-0.72196169656747066767);

         ptr[1][4*0+0]=static_cast<T>(-0.76423363056132420645);
         ptr[1][4*0+1]=static_cast<T>(+0.62465767926179283087);
         ptr[1][4*0+2]=static_cast<T>(-0.23445934039411586713);
         ptr[1][4*0+3]=static_cast<T>(-0.36728295529430798894);
         ptr[1][4*1+0]=static_cast<T>(-0.58053613211506416292);
         ptr[1][4*1+1]=static_cast<T>(-0.32123776043794061152);
         ptr[1][4*1+2]=static_cast<T>(+0.20079704122636399255);
         ptr[1][4*1+3]=static_cast<T>(+0.56832539932064829458);
         break;
      default:
         MIDASERROR("Invalid switch value.");
   }

   return NiceTensor<T>( result );
}

template <class T>
NiceTensor<T> make_test_generalcanonicaltensor
   ( unsigned aRank = 2
   , char aSwitch   = 'a'
   )
{
   std::vector<unsigned int> dims{2,3,4};
   CanonicalTensor<T>* result(new CanonicalTensor<T>(dims, aRank));
   T** ptr=result->GetModeMatrices();

   // Haven't calculated norms, etc. for these - simply convert them to
   // SimpleTensors within the test and compare results (assuming that
   // corresponding funcitons work for SimpleTensor).
   switch (aRank)
   {
      case 0:
      {
         break;
      }
      case 1:
      {
         switch (aSwitch)
         {
            case 'a':
               ptr[0][2*0+0]=static_cast<T>(-0.02307482087590611094);
               ptr[0][2*0+1]=static_cast<T>(-0.58521407693280234864);

               ptr[1][3*0+0]=static_cast<T>(-0.62260314953108391483);
               ptr[1][3*0+1]=static_cast<T>(-0.53782936593932783372);
               ptr[1][3*0+2]=static_cast<T>(+0.69177444132319056461);

               ptr[2][4*0+0]=static_cast<T>(+0.15796130782877125498);
               ptr[2][4*0+1]=static_cast<T>(-0.96222371094911911314);
               ptr[2][4*0+2]=static_cast<T>(+0.92815487317930787643);
               ptr[2][4*0+3]=static_cast<T>(+0.89884917109712240545);
               break;
            default:
               MIDASERROR("Invalid switch value.");
         }
         break;
      }
      case 2:
      {
         switch (aSwitch)
         {
            case 'a':
               ptr[0][2*0+0]=static_cast<T>(-0.30316644928721925289);
               ptr[0][2*0+1]=static_cast<T>(-0.66551935395687622510);
               ptr[0][2*1+0]=static_cast<T>( 0.75681384885549207731);
               ptr[0][2*1+1]=static_cast<T>(-0.98050719575263522998);

               ptr[1][3*0+0]=static_cast<T>( 0.92338434012564252207);
               ptr[1][3*0+1]=static_cast<T>( 0.30681975644951386961);
               ptr[1][3*0+2]=static_cast<T>(-0.89076871257751366606);
               ptr[1][3*1+0]=static_cast<T>(-0.81390782735601940878);
               ptr[1][3*1+1]=static_cast<T>(-0.77119974522307277454);
               ptr[1][3*1+2]=static_cast<T>( 0.82796691359587426540);

               ptr[2][4*0+0]=static_cast<T>( 0.35784925478030515933);
               ptr[2][4*0+1]=static_cast<T>( 0.24966648716567552135);
               ptr[2][4*0+2]=static_cast<T>( 0.39056057058631044043);
               ptr[2][4*0+3]=static_cast<T>( 0.87482059099410980352);
               ptr[2][4*1+0]=static_cast<T>( 0.41817242779004870634);
               ptr[2][4*1+1]=static_cast<T>( 0.33825709068906806287);
               ptr[2][4*1+2]=static_cast<T>(-0.34278663581003732119);
               ptr[2][4*1+3]=static_cast<T>(-0.05727340579392437014);
               break;
            case 'b':
               ptr[0][2*0+0]=static_cast<T>( 1.07281432293872036610e+0);
               ptr[0][2*0+1]=static_cast<T>( 3.17736198546000603304e-1);
               ptr[0][2*1+0]=static_cast<T>(-4.95419602913967360713e-1);
               ptr[0][2*1+1]=static_cast<T>(-4.16288578149171861753e-1);

               ptr[1][3*0+0]=static_cast<T>(-7.83794187398087149887e-1);
               ptr[1][3*0+1]=static_cast<T>( 1.22939942966188087148e+0);
               ptr[1][3*0+2]=static_cast<T>(-1.60116761971947219045e+0);
               ptr[1][3*1+0]=static_cast<T>( 1.62043490465219069963e-1);
               ptr[1][3*1+1]=static_cast<T>(-2.25739407025942906415e+0);
               ptr[1][3*1+2]=static_cast<T>( 1.04380414116366915245e+0);

               ptr[2][4*0+0]=static_cast<T>(-6.66593124605966980134e-1);
               ptr[2][4*0+1]=static_cast<T>(-1.12491368280543454355e-2);
               ptr[2][4*0+2]=static_cast<T>(-6.32802088311924082475e-1);
               ptr[2][4*0+3]=static_cast<T>( 2.49891105423980464106e-1);
               ptr[2][4*1+0]=static_cast<T>(-1.56308518662175965908e+0);
               ptr[2][4*1+1]=static_cast<T>(-5.54090224103914952281e-1);
               ptr[2][4*1+2]=static_cast<T>( 6.76506354172687340842e-1);
               ptr[2][4*1+3]=static_cast<T>(-3.18379116571013420778e-1);
               break;
            default:
               MIDASERROR("Invalid switch value.");
         }
         break;
      }
      case 3:
      {
         switch (aSwitch)
         {
            default:
               MIDASERROR("Invalid switch value.");
         }
         break;
      }
      default:
         MIDASERROR("Invalid rank value");
   }

   return NiceTensor<T>( result );
}

/**
 * Make CP tensor with random elements and decaying terms
 **/
template
   <  class T
   >
NiceTensor<T> make_test_random_canonicaltensor
   (  const std::vector<unsigned>& aDims
   ,  unsigned aRank
   )
{
   auto result = NiceCanonicalTensor<T>(aDims, aRank);
   auto& cp_result = *static_cast<CanonicalTensor<T>*>(result.GetTensor());
   T** mms = cp_result.GetModeMatrices();
   auto ndim = aDims.size();

   // Create normal distribution with zero mean and unit variance
   std::normal_distribution norm_dist(0., 1.);

   // Set random mode-matrix elements
   for(In idim=0; idim<ndim; ++idim)
   {
      T* mm = mms[idim];
      for(In irank=0; irank<aRank; ++irank)
      {
         T tmp = static_cast<T>(0.);
         for(In i=0; i<aDims[idim]; ++i)
         {
            *(mm++) = norm_dist(midas::util::detail::get_mersenne());
         }
      }
   }

   // Get norms of rank-1 terms
   auto lambdas = cp_result.GetRankOneNorms();

   // Scale mode-matrix elements
   for(In idim=0; idim<ndim; ++idim)
   {
      T* mm = mms[idim];
      for(In irank=0; irank<aRank; ++irank)
      {
         T tmp = static_cast<T>(0.);
         for(In i=0; i<aDims[idim]; ++i)
         {
            // Scale by decaying weight and norm of random rank-1 term
            *(mm++) *= (std::exp(-static_cast<T>(irank+1) / (2*ndim)) / std::pow(lambdas[irank], static_cast<T>(1.)/ndim));
         }
      }
   }

   return result;
}

template<class T>
NiceTensor<T> make_test_tensordirectproduct
   ( char aSwitch = 'a'
   )
{
   std::vector<unsigned int> dims{2,3,4};
   std::vector<std::unique_ptr<BaseTensor<T> > > tensors;
   TensorDirectProduct<T>* result = nullptr;

   switch(aSwitch)
   {
      case 'a':
      {
         tensors.emplace_back(new CanonicalTensor<T>({2 ,4}, 2));
         T** ptr1 = dynamic_cast<CanonicalTensor<T>* >(tensors.back().get())->GetModeMatrices();
         ptr1[0][2*0+0]=static_cast<T>(-0.30316644928721925289);
         ptr1[0][2*0+1]=static_cast<T>(-0.66551935395687622510);
         ptr1[0][2*1+0]=static_cast<T>( 0.75681384885549207731);
         ptr1[0][2*1+1]=static_cast<T>(-0.98050719575263522998);
         
         ptr1[1][4*0+0]=static_cast<T>( 0.35784925478030515933);
         ptr1[1][4*0+1]=static_cast<T>( 0.24966648716567552135);
         ptr1[1][4*0+2]=static_cast<T>( 0.39056057058631044043);
         ptr1[1][4*0+3]=static_cast<T>( 0.87482059099410980352);
         ptr1[1][4*1+0]=static_cast<T>( 0.41817242779004870634);
         ptr1[1][4*1+1]=static_cast<T>( 0.33825709068906806287);
         ptr1[1][4*1+2]=static_cast<T>(-0.34278663581003732119);
         ptr1[1][4*1+3]=static_cast<T>(-0.05727340579392437014);
         
         tensors.emplace_back(new CanonicalTensor<T>({3}, 3));
         T** ptr2 = dynamic_cast<CanonicalTensor<T>*>(tensors.back().get())->GetModeMatrices();
         ptr2[0][3*0+0]=static_cast<T>( 0.92338434012564252207);
         ptr2[0][3*0+1]=static_cast<T>( 0.30681975644951386961);
         ptr2[0][3*0+2]=static_cast<T>(-0.89076871257751366606);
         ptr2[0][3*1+0]=static_cast<T>(-0.81390782735601940878);
         ptr2[0][3*1+1]=static_cast<T>(-0.77119974522307277454);
         ptr2[0][3*1+2]=static_cast<T>( 0.82796691359587426540);
         ptr2[0][3*2+0]=static_cast<T>( 0.62043490465219069963);
         ptr2[0][3*2+1]=static_cast<T>(-0.25739407025942906415);
         ptr2[0][3*2+2]=static_cast<T>( 0.04380414116366915245);
         
         result = new TensorDirectProduct<T>({2,3,4}, 2.0, std::move(tensors), { {0,0}, {1,0}, {0,1} });
         break;
      }
      case 'b':
      {
         tensors.emplace_back(new CanonicalTensor<T>({2}, 1));
         T** ptr1 = dynamic_cast<CanonicalTensor<T>* >(tensors.back().get())->GetModeMatrices();
         ptr1[0][2*0+0]=static_cast<T>(-0.31879863825598697691);
         ptr1[0][2*0+1]=static_cast<T>(+0.82360970131671273897);
         
         tensors.emplace_back(new CanonicalTensor<T>({3}, 1));
         T** ptr2 = dynamic_cast<CanonicalTensor<T>* >(tensors.back().get())->GetModeMatrices();
         ptr2[0][3*0+0]=static_cast<T>(+0.52142710367250888481);
         ptr2[0][3*0+1]=static_cast<T>(-0.60525206280994514607);
         ptr2[0][3*0+2]=static_cast<T>(-0.20248483754752311103);
         
         tensors.emplace_back(new CanonicalTensor<T>({4}, 1));
         T** ptr3 = dynamic_cast<CanonicalTensor<T>* >(tensors.back().get())->GetModeMatrices();
         ptr3[0][4*0+0]=static_cast<T>(+0.24283986521019595983);
         ptr3[0][4*0+1]=static_cast<T>(+0.86512089657250812458);
         ptr3[0][4*0+2]=static_cast<T>(+0.28925771389372134657);
         ptr3[0][4*0+3]=static_cast<T>(+0.20402601389308139268);
         
         result = new TensorDirectProduct<T>({2,3,4}, 2.0, std::move(tensors), { {0,0}, {1,0}, {2,0} });
         break;
      }
      default:
         MIDASERROR("Invalid switch value.");
   }
   
   return NiceTensor<T>( result );
}

template<class T>
NiceTensor<T> make_test_tensorsum(char aSwitch = 'a')
{
   std::vector<unsigned> dims{3,3,3};
   NiceTensor<T> result(new TensorSum<T>(dims));
   switch(aSwitch)
   {
      case 'a':
      {
         result += detail::make_test_simpletensor<T>('a');
         result += detail::make_test_simpletensor<T>('b');
         break;
      }
      case 'b':
      {
         result += detail::make_test_simpletensor<T>('a');
         result += detail::make_test_canonicaltensor<T>('a');
         break;
      }
      case 'c':
      {
         result += detail::make_test_canonicaltensor<T>('a');
         result += detail::make_test_canonicaltensor<T>('b');
         break;
      }
      case 'd':
      {
         result += detail::make_test_simpletensor<T>('b');
         result += detail::make_test_canonicaltensor<T>('b');
         break;
      }         
      case 'e':
      {
         result -= detail::make_test_simpletensor<T>('a');
         result -= detail::make_test_simpletensor<T>('b');
         break;
      }
      case 'f':
      {
         result -= detail::make_test_simpletensor<T>('a');
         result -= detail::make_test_canonicaltensor<T>('a');
         break;
      }
      default:
      {
         MIDASERROR("Invalid switch value.");
      }
   }
   return result;
}

template <class T>
NiceTensor<T> make_test_matrix()
{
   std::vector<unsigned int> dims{};
   for(int i=0;i<2;i++)
   {
      dims.push_back(DIM);
   }
   SimpleTensor<T>* result(new SimpleTensor<T>(dims));

   T* ptr=result->GetData();

   ptr[DIM*0+0]=static_cast<T>(-0.86455936510985931243);
   ptr[DIM*0+1]=static_cast<T>(-0.81341747142447662355);
   ptr[DIM*0+2]=static_cast<T>( 0.38328829002142472682);
   ptr[DIM*1+0]=static_cast<T>( 0.26274563848617193962);
   ptr[DIM*1+1]=static_cast<T>( 0.05017513771425718971);
   ptr[DIM*1+2]=static_cast<T>(-0.04216193030958770294);
   ptr[DIM*2+0]=static_cast<T>( 0.48071725775855722596);
   ptr[DIM*2+1]=static_cast<T>(-0.99855637353109760745);
   ptr[DIM*2+2]=static_cast<T>(-0.59988716058623636762);
 
   return NiceTensor<T>(result);
}
 
template <class T>
NiceTensor<T> make_test_vector()
{
   std::vector<unsigned int> dims{};
   dims.push_back(DIM);
   SimpleTensor<T>* result(new SimpleTensor<T>(dims));

   T* ptr=result->GetData();

   ptr[0]=static_cast<T>(+0.58336057859334911058);
   ptr[1]=static_cast<T>(-0.29855310284790337327);
   ptr[2]=static_cast<T>(+0.06102759677053248843);

   return NiceTensor<T>(result);
}

template <class T>
NiceTensor<T> make_test_vector4()
{
   std::vector<unsigned int> dims{4};
   SimpleTensor<T>* result(new SimpleTensor<T>(dims));

   T* ptr=result->GetData();

   ptr[0]=static_cast<T>(+0.46383392518941168881);
   ptr[1]=static_cast<T>(+0.89240851204199911351);
   ptr[2]=static_cast<T>(-0.47849597257526965688);
   ptr[3]=static_cast<T>(-0.57684934641626883778);

   return NiceTensor<T>(result);
}

template<class T>
NiceTensor<T> make_test_zerotensor
   ( const std::vector<unsigned>& dims
   )
{
   return NiceTensor<T>(new ZeroTensor<T>(dims));
}

template<class T>
std::unique_ptr<T[]> GammaMatrix
   ( const NiceTensor<T>& tens1
   , const NiceTensor<T>& tens2
   , unsigned idx
   )
{
   auto canonical_ptr_1 = dynamic_cast<const CanonicalTensor<T>*>(tens1.GetTensor());
   auto canonical_ptr_2 = dynamic_cast<const CanonicalTensor<T>*>(tens2.GetTensor());
   std::unique_ptr<T[]> gamma = std::unique_ptr<T[]>(new T[canonical_ptr_1->GetRank() * canonical_ptr_2->GetRank()]);
   GammaMatrix (  canonical_ptr_1
               ,  canonical_ptr_2
               ,  idx 
               ,  gamma
               );
   return gamma;
}

template<class T>
T gamma_norm
   ( const std::unique_ptr<T[]>& gamma
   , unsigned size
   )
{
   auto result = static_cast<T>(0.0);
   for(unsigned i = 0; i < size; ++i)
   {
      result += gamma[i]*gamma[i];
   }
   return std::sqrt(result);
}

} /* namespace midas::test::detail */

/********* TESTS PROPER ***********/
/**
 *
 **/
template <class T>
void midas::test::SimpleTensorTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   // Real
   {
   NiceTensor<T> dummy = detail::make_test_simpletensor<T>();

   T norm=dummy.Norm();
   T norm_ref=static_cast<T>(2.6002691632338139627);
   UNIT_ASSERT_FEQUAL( norm, norm_ref, "SimpleTensor.Norm() failed" );

   NiceTensor<T> copy=dummy;
   T factor=static_cast<T>(5.0);
   copy*=factor;
   UNIT_ASSERT_FEQUAL( copy.Norm(), norm_ref*factor, "SimpleTensor *= constant failed" );

   copy+=dummy;
   T factor2=static_cast<T>(6.0);
   UNIT_ASSERT_FEQUAL( copy.Norm(), norm_ref*factor2, "SimpleTensor += SimpleTensor failed" );
   }

   // Complex
   {
   auto dummy = detail::make_complex_test_simpletensor<T>();

   auto norm = dummy.Norm();
   auto norm_ref = static_cast<T>(3.7464123087769514);
   UNIT_ASSERT_FEQUAL( norm, norm_ref, "SimpleTensor.Norm() for complex numbers failed" );

   auto copy=dummy;
   auto factor=static_cast<T>(5.0);
   copy*=factor;
   UNIT_ASSERT_FEQUAL( copy.Norm(), norm_ref*factor, "Complex SimpleTensor *= constant failed" );

   copy+=dummy;
   T factor2=static_cast<T>(6.0);
   UNIT_ASSERT_FEQUAL( copy.Norm(), norm_ref*factor2, "Complex SimpleTensor += SimpleTensor failed" );
   }
}

/**
 *
 **/
template <class T>
void midas::test::SimpleTensorAdditionAssignmentTest<T>::actual_test()
{
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');

   simplea += simpleb;
   
   UNIT_ASSERT_FEQUAL( simplea.Norm(), static_cast<T>(5.38118314820427734446), "SimpleTensor += failed" );
}

/**
 *
 **/
template <class T>
void midas::test::SimpleTensorAdditionTest<T>::actual_test()
{
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');

   auto simpleresult = simplea + simpleb;
   
   UNIT_ASSERT_FEQUAL( simpleresult.Norm(), static_cast<T>(5.38118314820427734446), "SimpleTensor + failed" );
}

/**
 *
 **/
template <class T>
void midas::test::SimpleTensorSubtractionAssignmentTest<T>::actual_test()
{
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');

   simplea -= simpleb;
   
   UNIT_ASSERT_FEQUAL( simplea.Norm(), static_cast<T>(4.47736350504527003125), "SimpleTensor -= failed" );
}

/**
 *
 **/
template <class T>
void midas::test::SimpleTensorSubtractionTest<T>::actual_test()
{
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');

   auto simpleresult = simplea - simpleb;
   
   UNIT_ASSERT_FEQUAL( simpleresult.Norm(), static_cast<T>(4.47736350504527003125), "SimpleTensor - failed" );
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   auto dummy = detail::make_test_simpletensor<T>();
   auto vector = detail::make_test_vector<T>();
   /*********** ContractDown ***********/
   {
      auto foo = dummy.ContractDown(0, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.2184062891495091030), "SimpleTensor::ContractDown 0 failed" );
   }
   {
      auto foo = dummy.ContractDown(1, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.8126202155618208467), "SimpleTensor::ContractDown 1 failed" );
   }
   {
      auto foo = dummy.ContractDown(2, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.2696166151365619879), "SimpleTensor::ContractDown 2 failed" );
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   auto dummy = detail::make_test_simpletensor<T>();
   auto matrix = detail::make_test_matrix<T>();
   /*********** ContractForward ***********/
   {
      auto foo = dummy.ContractForward(0, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.42998047197865663094), "SimpleTensor::ContractForward 0 failed" );
   }
   {
      auto foo = dummy.ContractForward(1, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.45922557499771654577), "SimpleTensor::ContractForward 1 failed"  );
   }
   {
      auto foo = dummy.ContractForward(2, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.42264490628473350142), "SimpleTensor::ContractForward 2 failed" );
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   auto dummy = detail::make_test_simpletensor<T>();
   auto vector = detail::make_test_vector<T>();
   /*********** ContractUp ***********/
   {
      auto foo = dummy.ContractUp(0, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.7113799208322542089), "SimpleTensor::ContractUp 0 failed");
   }
   {
      auto foo = dummy.ContractUp(1, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.7113799208322542089), "SimpleTensor::ContractUp 1 failed" );
   }
   {
      auto foo = dummy.ContractUp(2, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.7113799208322542089), "SimpleTensor::ContractUp 2 failed" );
   }
   {
      auto foo = dummy.ContractUp(3, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.7113799208322542089), "SimpleTensor::ContractUp 3 failed" );
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorSimpleContractDownTest<T>::actual_test()
{
   auto simple = detail::make_simple_simpletensor<T>(0.0, 2, 3, 4);

   // contract index 0
   {
      auto vec   = detail::make_simple_simpletensor<T>(2.0, 2);
      auto result = simple.ContractDown(0, vec);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(51962.0)), "Down contract index 0 failed!");
   }  
   // contract index 1
   {
      auto vec   = detail::make_simple_simpletensor<T>(3.0, 3);
      auto result = simple.ContractDown(1, vec);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(213440.0)), "Down contract index 1 failed!");
   }
   // contract index 2
   {
      auto vec   = detail::make_simple_simpletensor<T>(4.0, 4);
      auto result = simple.ContractDown(2, vec);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(534904.0)), "Down contract index 2 failed!");
   }
}

/**
 * test forward contraction for simple square matrix
 **/
template<class T>
void midas::test::SimpleTensorVerySimpleMatrixContractForwardTest<T>::actual_test()
{
   auto simple = detail::make_simple_simpletensor<T>(0.0, 2, 2);

   // contract index 0
   {
      auto mat = detail::make_simple_simpletensor<T>(0.0, 3, 2);
      auto result = simple.ContractForward(0, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(631.0)), "Forward contract index 0 failed!");
   }  
   // contract index 1
   {
      auto mat = detail::make_simple_simpletensor<T>(0.0, 3, 2);
      auto result = simple.ContractForward(1, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(742.0)), "Forward contract index 1 failed!");
   }  
}

/**
 * test forward contraction for non-square matrix
 **/
template<class T>
void midas::test::SimpleTensorSimpleMatrixContractForwardTest<T>::actual_test()
{

   // contract index 0
   {
      auto simple = detail::make_simple_simpletensor<T>(0.0, 2, 4);
      auto mat = detail::make_simple_simpletensor<T>(0.0, 3, 2);
      auto result = simple.ContractForward(0, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(6666.0)), "Forward contract index 0 failed!");
   }
   // contract index 1
   {
      auto simple = detail::make_simple_simpletensor<T>(0.0, 4, 2);
      auto mat = detail::make_simple_simpletensor<T>(0.0, 3, 2);
      auto result = simple.ContractForward(1, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(7596.0)), "Forward contract index 1 failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorSimpleContractForwardTest<T>::actual_test()
{
   auto simple = detail::make_simple_simpletensor<T>(0.0, 2, 3, 4);

   // contract index 0
   {
      auto mat = detail::make_simple_simpletensor<T>(2.0, 3, 2);
      auto result = simple.ContractForward(0, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(521758.0)), "Forward contract index 0 failed!");
   }  
   // contract index 1
   {
      auto mat = detail::make_simple_simpletensor<T>(3.0, 2, 3);
      auto result = simple.ContractForward(1, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(842860.0)), "Forward contract index 1 failed!");
   }
   // contract index 2
   {
      auto mat = detail::make_simple_simpletensor<T>(4.0, 2, 4);
      auto result = simple.ContractForward(2, mat);
      UNIT_ASSERT_FEQUAL(result.Norm(), std::sqrt(static_cast<T>(2111408.0)), "Forward contract index 2 failed!");
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorGeneralContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;

   auto dummy = detail::make_test_simpletensor<T>();
   auto vector = detail::make_test_vector<T>();
   /*********** ContractDown ***********/
   // contract index 0
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], vector[i]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.2184062891495091030), "general SimpleTensor::ContractDown 0 failed" );
   }
   // contract index 1
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], vector[j]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.8126202155618208467), "general SimpleTensor::ContractDown 1 failed" );
   }
   // contract index 2
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], vector[k]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.2696166151365619879), "general SimpleTensor::ContractDown 2 failed" );
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorGeneralContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   
   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;
   using contraction_indices::l;

   auto dummy = detail::make_test_simpletensor<T>();
   auto matrix = detail::make_test_matrix<T>();
   /*********** ContractForward ***********/
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], matrix[l,i]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.42998047197865663094), "general SimpleTensor::ContractForward 0 failed" );
   }
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], matrix[l,j]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.45922557499771654577), "general SimpleTensor::ContractForward 1 failed"  );
   }
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], matrix[l,k]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(2.42264490628473350142), "general SimpleTensor::ContractForward 2 failed" );
   }
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorGeneralContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   
   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;
   using contraction_indices::l;

   auto dummy = detail::make_test_simpletensor<T>();
   auto vector = detail::make_test_vector<T>();
   /*********** ContractUp ***********/
   {
      auto foo = static_cast<NiceTensor<T> >(contract(dummy[i,j,k], vector[l]));
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.7113799208322542089), "General SimpleTensor::ContractUp failed");
   }
}

/**
 * Test special contractions such as contracting all but one index away for two tensors (used in MCTDH).
 **/
template
   <  typename T
   >
void midas::test::SimpleTensorSpecialContractionsTest<T>::actual_test()
{
   static_assert(std::is_floating_point_v<T>, "only works with floating point or complex");

   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;
   using contraction_indices::l;

   // Contract all but one index of two tensors
   {
      // Two different tensors
      auto left = detail::make_complex_test_simpletensor<T>('a');
      auto right = detail::make_complex_test_simpletensor<T>('b');

      auto result_lr_0 = midas::mctdh::detail::ContractAllButOne(left, right, I_0);
      auto result_lr_1 = midas::mctdh::detail::ContractAllButOne(left, right, I_1);
      auto result_lr_2 = midas::mctdh::detail::ContractAllButOne(left, right, I_2);

      // Contract all indices but the ones corresponding to aMode
      decltype(left) ref_lr_0 = contract(left[i, k, l], right[j, k, l], true, false);
      decltype(left) ref_lr_1 = contract(left[k, i, l], right[k, j, l], true, false);
      decltype(left) ref_lr_2 = contract(left[k, l, i], right[k, l, j], true, false);

      // norms
      auto ref_lr_0_norm = ref_lr_0.Norm();
      auto ref_lr_1_norm = ref_lr_1.Norm();
      auto ref_lr_2_norm = ref_lr_2.Norm();

      // diff norms
      auto diff_lr_0 = std::sqrt(safe_diff_norm2(result_lr_0.GetTensor(), ref_lr_0.GetTensor()));
      auto diff_lr_1 = std::sqrt(safe_diff_norm2(result_lr_1.GetTensor(), ref_lr_1.GetTensor()));
      auto diff_lr_2 = std::sqrt(safe_diff_norm2(result_lr_2.GetTensor(), ref_lr_2.GetTensor()));

      UNIT_ASSERT_FZERO_PREC(diff_lr_0, ref_lr_0_norm, 2, "ContractAllButOne failed for two different tensors (mode 0).");
      UNIT_ASSERT_FZERO_PREC(diff_lr_1, ref_lr_1_norm, 2, "ContractAllButOne failed for two different tensors (mode 1).");
      UNIT_ASSERT_FZERO_PREC(diff_lr_2, ref_lr_2_norm, 2, "ContractAllButOne failed for two different tensors (mode 2).");
   }

   // Contract all indices of right tensor and keep one in left tensor
   {
      // Two different tensors
      auto left = detail::make_complex_test_simpletensor<T>('a');
      auto right = detail::make_complex_test_simpletensor<T>('c');   // lower dimensionality!

      auto result_lr_0 = midas::mctdh::detail::ContractAllInRight(left, right, I_0);
      auto result_lr_1 = midas::mctdh::detail::ContractAllInRight(left, right, I_1);
      auto result_lr_2 = midas::mctdh::detail::ContractAllInRight(left, right, I_2);

      // Contract all indices but the ones corresponding to aMode
      decltype(left) ref_lr_0 = contract(left[i, k, l], right[k, l], true, false);
      decltype(left) ref_lr_1 = contract(left[k, i, l], right[k, l], true, false);
      decltype(left) ref_lr_2 = contract(left[k, l, i], right[k, l], true, false);

      // norms
      auto ref_lr_0_norm = ref_lr_0.Norm();
      auto ref_lr_1_norm = ref_lr_1.Norm();
      auto ref_lr_2_norm = ref_lr_2.Norm();

      // diff norms
      auto diff_lr_0 = std::sqrt(safe_diff_norm2(result_lr_0.GetTensor(), ref_lr_0.GetTensor()));
      auto diff_lr_1 = std::sqrt(safe_diff_norm2(result_lr_1.GetTensor(), ref_lr_1.GetTensor()));
      auto diff_lr_2 = std::sqrt(safe_diff_norm2(result_lr_2.GetTensor(), ref_lr_2.GetTensor()));

      UNIT_ASSERT_FZERO_PREC(diff_lr_0, ref_lr_0_norm, 2, "ContractAllInRight failed for two different tensors (mode 0).");
      UNIT_ASSERT_FZERO_PREC(diff_lr_1, ref_lr_1_norm, 2, "ContractAllInRight failed for two different tensors (mode 1).");
      UNIT_ASSERT_FZERO_PREC(diff_lr_2, ref_lr_2_norm, 2, "ContractAllInRight failed for two different tensors (mode 2).");
   }

   // Contract all but one index (two different indeces). Special case for matrices.
   {
      // Two different tensors
      auto left = detail::make_complex_test_simpletensor<T>('c');
      auto right = detail::make_complex_test_simpletensor<T>('d');

      auto result_01 = midas::mctdh::detail::ContractAllButOne(left, right, I_0, I_1, false);
      auto result_01_ref = midas::mctdh::detail::ContractAllButOne(left, right, I_0, I_1, true);

      auto result_10 = midas::mctdh::detail::ContractAllButOne(left, right, I_1, I_0, false);
      auto result_10_ref = midas::mctdh::detail::ContractAllButOne(left, right, I_1, I_0, true);

      auto result_00 = midas::mctdh::detail::ContractAllButOne(left, right, I_0, I_0, false);
      auto result_00_ref = midas::mctdh::detail::ContractAllButOne(left, right, I_0, I_0, true);

      auto result_11 = midas::mctdh::detail::ContractAllButOne(left, right, I_1, I_1, false);
      auto result_11_ref = midas::mctdh::detail::ContractAllButOne(left, right, I_1, I_1, true);

      // Norms
      auto ref_01_norm = result_01_ref.Norm();
      auto ref_10_norm = result_10_ref.Norm();
      auto ref_00_norm = result_00_ref.Norm();
      auto ref_11_norm = result_11_ref.Norm();

      // Diff norms
      auto diff_01 = std::sqrt(safe_diff_norm2(result_01.GetTensor(), result_01_ref.GetTensor()));
      auto diff_10 = std::sqrt(safe_diff_norm2(result_10.GetTensor(), result_10_ref.GetTensor()));
      auto diff_00 = std::sqrt(safe_diff_norm2(result_00.GetTensor(), result_00_ref.GetTensor()));
      auto diff_11 = std::sqrt(safe_diff_norm2(result_11.GetTensor(), result_11_ref.GetTensor()));

      UNIT_ASSERT_FZERO_PREC(diff_01, ref_01_norm, 2, "ContractAllButOne (two diff indices) failed with GEMM for matrices (0,1).");
      UNIT_ASSERT_FZERO_PREC(diff_10, ref_10_norm, 2, "ContractAllButOne (two diff indices) failed with GEMM for matrices (1,0).");
      UNIT_ASSERT_FZERO_PREC(diff_00, ref_00_norm, 2, "ContractAllButOne (two diff indices) failed with GEMM for matrices (0,0).");
      UNIT_ASSERT_FZERO_PREC(diff_11, ref_11_norm, 2, "ContractAllButOne (two diff indices) failed with GEMM for matrices (1,1).");

      // Tests for different dimensions
      auto tens_34 = make_nice_random_simple<T>(std::vector<unsigned>{3, 4});
      auto tens_35 = make_nice_random_simple<T>(std::vector<unsigned>{3, 5});
      auto tens_43 = make_nice_random_simple<T>(std::vector<unsigned>{4, 3});
      auto tens_53 = make_nice_random_simple<T>(std::vector<unsigned>{5, 3});

      auto result_34_35 = midas::mctdh::detail::ContractAllButOne(tens_34, tens_35, I_1, I_1, false);
      auto ref_34_35    = midas::mctdh::detail::ContractAllButOne(tens_34, tens_35, I_1, I_1, true);
      auto result_43_35 = midas::mctdh::detail::ContractAllButOne(tens_43, tens_35, I_0, I_1, false);
      auto ref_43_35    = midas::mctdh::detail::ContractAllButOne(tens_43, tens_35, I_0, I_1, true);
      auto result_43_53 = midas::mctdh::detail::ContractAllButOne(tens_43, tens_53, I_0, I_0, false);
      auto ref_43_53    = midas::mctdh::detail::ContractAllButOne(tens_43, tens_53, I_0, I_0, true);
      auto result_34_53 = midas::mctdh::detail::ContractAllButOne(tens_34, tens_53, I_1, I_0, false);
      auto ref_34_53    = midas::mctdh::detail::ContractAllButOne(tens_34, tens_53, I_1, I_0, true);

      auto ref_34_35_norm = ref_34_35.Norm();
      auto ref_43_35_norm = ref_43_35.Norm();
      auto ref_43_53_norm = ref_43_53.Norm();
      auto ref_34_53_norm = ref_34_53.Norm();

      auto diff_34_35 = std::sqrt(safe_diff_norm2(result_34_35.GetTensor(), ref_34_35.GetTensor()));
      auto diff_43_35 = std::sqrt(safe_diff_norm2(result_43_35.GetTensor(), ref_43_35.GetTensor()));
      auto diff_43_53 = std::sqrt(safe_diff_norm2(result_43_53.GetTensor(), ref_43_53.GetTensor()));
      auto diff_34_53 = std::sqrt(safe_diff_norm2(result_34_53.GetTensor(), ref_34_53.GetTensor()));

      UNIT_ASSERT_FZERO_PREC(diff_34_35, ref_34_35_norm, 2, "ContractAllButOne (two diff indices + diff dims) failed with GEMM for matrices (1,1).");
      UNIT_ASSERT_FZERO_PREC(diff_43_35, ref_43_35_norm, 2, "ContractAllButOne (two diff indices + diff dims) failed with GEMM for matrices (0,1).");
      UNIT_ASSERT_FZERO_PREC(diff_43_53, ref_43_53_norm, 2, "ContractAllButOne (two diff indices + diff dims) failed with GEMM for matrices (0,0).");
      UNIT_ASSERT_FZERO_PREC(diff_34_53, ref_34_53_norm, 2, "ContractAllButOne (two diff indices + diff dims) failed with GEMM for matrices (1,0).");
   }
}


/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto vec = detail::make_test_vector<T>();

   // contract index 0
   {
      auto result_simple    = simple.ContractDown(0, vec);
      auto result_canonical = canonical.ContractDown(0, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Down contract index 0 failed!");
   }  
   // contract index 1
   {
      auto result_simple    = simple.ContractDown(1, vec);
      auto result_canonical = canonical.ContractDown(1, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Down contract index 1 failed!");
   }  
   // contract index 2
   {
      auto result_simple    = simple.ContractDown(2, vec);
      auto result_canonical = canonical.ContractDown(2, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Down contract index 2 failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorGeneralContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto vec = detail::make_test_vector<T>();
   
   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;

   // contract index 0
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , vec[i]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], vec[i]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS general Down contract index 0 failed!");
   }  
   // contract index 1
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , vec[j]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], vec[j]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS general Down contract index 1 failed!");
   }  
   // contract index 2
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , vec[k]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], vec[k]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS general Down contract index 2 failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto vec = detail::make_test_vector<T>();

   // contract index 0
   {
      auto result_simple    = simple.ContractUp(0, vec);
      auto result_canonical = canonical.ContractUp(0, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Up contract index 0 failed!");
   }  
   // contract index 1
   {
      auto result_simple    = simple.ContractUp(1, vec);
      auto result_canonical = canonical.ContractUp(1, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Up contract index 1 failed!");
   }  
   // contract index 2
   {
      auto result_simple    = simple.ContractUp(2, vec);
      auto result_canonical = canonical.ContractUp(2, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Up contract index 2 failed!");
   }  
   // contract index 3
   {
      auto result_simple    = simple.ContractUp(3, vec);
      auto result_canonical = canonical.ContractUp(3, vec);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Up contract index 3 failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorGeneralContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto vec = detail::make_test_vector<T>();
   
   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;
   using contraction_indices::l;

   // contract index 0
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k], vec[l]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], vec[l]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS General Up contract failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto mat = detail::make_test_matrix<T>();

   // contract index 0
   {
      auto result_simple    = simple.ContractForward(0, mat);
      auto result_canonical = canonical.ContractForward(0, mat);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Forward contract index 0 failed!");
   }  
   // contract index 1
   {
      auto result_simple    = simple.ContractForward(1, mat);
      auto result_canonical = canonical.ContractForward(1, mat);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Forward contract index 1 failed!");
   }  
   // contract index 2
   {
      auto result_simple    = simple.ContractForward(2, mat);
      auto result_canonical = canonical.ContractForward(2, mat);
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS Forward contract index 2 failed!");
   }  
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorGeneralContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   auto mat = detail::make_test_matrix<T>();
   
   using contraction_indices::i;
   using contraction_indices::j;
   using contraction_indices::k;
   using contraction_indices::l;

   // contract index 0
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , mat[l,i]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], mat[l,i]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS General Forward contract index 0 failed!");
   }  
   // contract index 1
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , mat[l,j]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], mat[l,j]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS General Forward contract index 1 failed!");
   }  
   // contract index 2
   {
      auto result_simple    = static_cast<NiceTensor<T> >(contract(simple[i,j,k]   , mat[l,k]));
      auto result_canonical = static_cast<NiceTensor<T> >(contract(canonical[i,j,k], mat[l,k]));
      UNIT_ASSERT_FEQUAL(result_simple.Norm(), result_canonical.Norm(), "VS General Forward contract index 2 failed!");
   }  
}

/**
 *
 **/
template
   <  class T
   >
void midas::test::SimpleTensorIoTest<T>::actual_test
   (
   )
{
   // Only run on master rank. Otherwise, only master writes and slaves cannot find the file they need to read.
   if (  midas::mpi::IsMaster()
      )
   {
      using real_t = midas::type_traits::RealTypeT<T>;
      static_assert(std::is_floating_point_v<real_t>, "only works with floating point");

      auto real_tens = detail::make_test_simpletensor<T>();
      std::string real_filename = "SimpleTensorIoTest_real_tens";
      real_tens.WriteToDisc(real_filename);

      NiceTensor<T> real_tens_read;
      real_tens_read.ReadFromDisc(real_filename);

      real_t err = diff_norm_new(*real_tens.GetTensor(), *real_tens_read.GetTensor());

      UNIT_ASSERT_FZERO(err, real_tens.Norm(), "SimpleTensor file IO does not work!");

      // cleanup
      std::remove(real_filename.c_str());

      // also with non-zero imaginary part for complex!
      if constexpr   (  midas::type_traits::IsComplexV<T>
                     )
      {
         auto complex_tens = detail::make_complex_test_simpletensor<real_t>();
         std::string complex_filename = "SimpleTensorIoTest_complex_tens";
         complex_tens.WriteToDisc(complex_filename);

         NiceTensor<T> complex_tens_read;
         complex_tens_read.ReadFromDisc(complex_filename);

         real_t err_complex = diff_norm_new(*complex_tens.GetTensor(), *complex_tens_read.GetTensor());

         UNIT_ASSERT_FZERO(err_complex, complex_tens.Norm(), "SimpleTensor file IO with non-zero imaginary parts does not work!");

         // cleanup
         std::remove(complex_filename.c_str());
      }
   }
}


/**
 *
 **/
template <class T>
void midas::test::CanonicalTensorTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");

   // square 3x3x3
   NiceTensor<T> dummy=detail::make_test_canonicaltensor<T>();

   auto norm_ref = static_cast<T>(1.364508870102732585);
   UNIT_ASSERT_FEQUAL( dummy.Norm(), norm_ref, "CanonicalTensor.Norm() failed" );

   auto copy = dummy;
   auto factor = static_cast<T>(5.0);
   copy *= factor;
   UNIT_ASSERT_FEQUAL( copy.Norm(), norm_ref*factor,  "CanonicalTensor *= constant failed" );
   
   // general 2x3x4
   NiceTensor<T> general_dummy = detail::make_test_generalcanonicaltensor<T>(2,'a');
   auto general_norm_ref = static_cast<T>(1.47664564114447723607);
   UNIT_ASSERT_FEQUAL( general_dummy.Norm(), general_norm_ref, "CanonicalTensor.Norm() failed for general" );
}

/**
 *
 **/
template <class T>
void midas::test::CanonicalTensorRankZeroTest<T>::actual_test()
{   
   auto canonicalrankzero = detail::make_test_generalcanonicaltensor<T>(0);
   UNIT_ASSERT_EQUAL ( canonicalrankzero.template StaticCast<CanonicalTensor<T> >().GetRank(), static_cast<unsigned>(0), "CanonicalTensor.GetRank() failed for general rank 0.");
   UNIT_ASSERT_FEQUAL( canonicalrankzero.Norm(), static_cast<T>(0.0), "CanonicalTensor.Norm() failed for general rank 0." );

   canonicalrankzero.Scale(static_cast<T>(3.14159));
   UNIT_ASSERT_FEQUAL( canonicalrankzero.Norm(), static_cast<T>(0.0), "CanonicalTensor.Norm() failed after Scale for general rank 0." );
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dummy  = detail::make_test_canonicaltensor<T>();
   auto vector = detail::make_test_vector<T>();

   /*********** ContractDown ***********/
   {
      auto foo = dummy.ContractDown(0, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.63745597197758276228), "CanonicalTensor::ContractDown 0 failed" );
   }
   {
      auto foo = dummy.ContractDown(1, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.29428868956372551072), "CanonicalTensor::ContractDown 1 failed" );
   }
   {
      auto foo = dummy.ContractDown(2, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.27599218873262792138), "CanonicalTensor::ContractDown 2 failed" );
   }
};

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorContractDown2DTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonical  = detail::make_test_canonicaltensor2d<T>();
   auto vector3 = detail::make_test_vector<T>();
   auto vector4 = detail::make_test_vector4<T>();

   /*********** ContractDown ***********/
   {
      auto result = canonical.ContractDown(0, vector3);
      UNIT_ASSERT_EQUAL ( result.template DynamicCast<CanonicalTensor<T> >().GetRank()
                        , static_cast<unsigned>(1)
                        , "CanonicalTensor::ContractDown 0 Rank failed for 2D" );
      UNIT_ASSERT_EQUAL ( result.GetDims()
                        , std::vector<unsigned>{4} 
                        , "CanonicalTensor::ContractDown 0 Dims failed for 2D" );
      UNIT_ASSERT_FEQUAL( result.Norm()
                        , static_cast<T>(+1.05110116571161382813)
                        , "CanonicalTensor::ContractDown 0 Norm failed for 2D" );
   }
   {
      auto result = canonical.ContractDown(1, vector4);
      UNIT_ASSERT_EQUAL ( result.template DynamicCast<CanonicalTensor<T> >().GetRank()
                        , static_cast<unsigned>(1)
                        , "CanonicalTensor::ContractDown 1 Rank failed for 2D" );
      UNIT_ASSERT_EQUAL ( result.GetDims()
                        , std::vector<unsigned>{3} 
                        , "CanonicalTensor::ContractDown 1 Dims failed for 2D" );
      UNIT_ASSERT_FEQUAL( result.Norm()
                        , static_cast<T>(+1.15436510061453645665)
                        , "CanonicalTensor::ContractDown 1 Norm failed for 2D" );
   }
};

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dummy  = detail::make_test_canonicaltensor<T>();
   auto vector = detail::make_test_vector<T>();

   /*********** ContractUp ***********/
   {
      auto foo = dummy.ContractUp(0, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.89805821455313106227), "CanonicalTensor::ContractUp 0 failed" );
   }
   {
      auto foo = dummy.ContractUp(1, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.89805821455313106227), "CanonicalTensor::ContractUp 1 failed" );
   }
   {
      auto foo = dummy.ContractUp(2, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.89805821455313106227), "CanonicalTensor::ContractUp 2 failed" );
   }
   {
      auto foo = dummy.ContractUp(3, vector);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.89805821455313106227), "CanonicalTensor::ContractUp 3 failed" );
   }
};

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dummy  = detail::make_test_canonicaltensor<T>();
   auto matrix = detail::make_test_matrix<T>();

   /*********** ContractForward ***********/
   {
      auto foo = dummy.ContractForward(0, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.50970826729051044168), "CanonicalTensor::ContractForward 0 failed" );
   }
   {
      auto foo = dummy.ContractForward(1, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.63613048611609301908), "CanonicalTensor::ContractForward 1 failed" );
   }
   {
      auto foo = dummy.ContractForward(2, matrix);
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(1.42271240812458743541), "CanonicalTensor::ContractForward 2 failed" );
   }
};

/**
 *
 **/
template <class T>
void midas::test::DecompositionTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   NiceTensor<T> dummy = detail::make_test_canonicaltensor<T>();
   NiceTensor<T> full = dummy.ToSimpleTensor();

   const T ref_norm = static_cast<T>(1.364508870102732585);
   UNIT_ASSERT_FEQUAL( dummy.Norm(), ref_norm, "CanonicalTensor.Norm() failed" );
   UNIT_ASSERT_FEQUAL(  full.Norm(), ref_norm, "SimpleTensor.Norm() failed" );
   
   // svd guess
   unsigned int rank=dynamic_cast<CanonicalTensor<T>*>( dummy.GetTensor() )->GetRank();
   auto newfit_svd = NiceTensor<T>(new CanonicalTensor<T>(dynamic_cast<SimpleTensor<T>*>( full .GetTensor() )->CanonicalSVDGuess(rank)));

   const T ref_svd = static_cast<T>(1.4142135623730951455);
   UNIT_ASSERT_FEQUAL_PREC( newfit_svd.Norm(), ref_svd, 3, "SimpleTensor::SVDGuess failed" ); // MKL needs looser precision (3) to pass the test
   
   // cp-als
   auto newfit_als = newfit_svd;
   auto fit_report_als = dynamic_cast<CanonicalTensor<T>*>( newfit_als.GetTensor() )->FitALS(*dynamic_cast<SimpleTensor<T>*>( full .GetTensor() ), 1000, 1.e-7);

   UNIT_ASSERT( fabs(newfit_als.Norm() - ref_norm) < 1.e-5, "Alternating-least squares failed" );
   UNIT_ASSERT(fit_report_als.error < 1.e-3, "CP-ALS failed");

   // non-linear conjugated gradient
   auto newfit_ncg = newfit_svd;
   CpNCGInput<T> ncg_input;
   ncg_input.SetMaxiter(1000);
   ncg_input.SetMaxerr(1.e-5);
   ncg_input.SetUseGeneralSolver(true);
   auto fit_report_ncg = FitNCG(full.GetTensor(), ref_norm*ref_norm, static_cast<CanonicalTensor<T>*>(newfit_ncg.GetTensor()), ncg_input);

   UNIT_ASSERT( fabs(newfit_ncg.Norm() - ref_norm) < 1.e-5, "Non-linear conjugate gradient optimization failed" );
   UNIT_ASSERT(fit_report_ncg.error < 1.e-3, "CP-NCG (general solver) failed");

   // non-linear conjugated gradient specialised
   auto newfit_ncg_2 = newfit_svd;
   CpNCGInput<T> ncg_input_2;
   ncg_input_2.SetMaxiter(1000);
   ncg_input_2.SetMaxerr(1.e-5);
   ncg_input_2.SetLineSearch(CpNCGInput<T>::lsType::EXACT);
   ncg_input_2.SetNcgType(CpNCGInput<T>::ncgType::HZ);
   auto fit_report_ncg_2 = FitNCG(full.GetTensor(), ref_norm*ref_norm, static_cast<CanonicalTensor<T>*>(newfit_ncg_2.GetTensor()), ncg_input_2);

   UNIT_ASSERT( fabs(newfit_ncg_2.Norm() - ref_norm) < 1.e-5, "Non-linear conjugate gradient optimization failed" );
   UNIT_ASSERT(fit_report_ncg_2.error < 1.e-3, "CP-NCG (specialized solver) failed");


   // Pivotized alternating steepest descent
   auto newfit_pasd = newfit_svd;
   CpAsdInput<T> pasd_input;
   pasd_input.SetMaxerr(1.e-7);
   pasd_input.SetMaxiter(500);
   pasd_input.SetBalanceModeVectors(true);
   pasd_input.SetDiagonalPreconditioner(true);
   auto fit_report_pasd = FitPASD(full.GetTensor(), ref_norm*ref_norm, static_cast<CanonicalTensor<T>*>(newfit_pasd.GetTensor()), pasd_input);
   
   UNIT_ASSERT( fabs(newfit_pasd.Norm() - ref_norm) < 1.e-5, "PASD failed!" );
   UNIT_ASSERT(fit_report_pasd.error < 1.e-3, "CP-PASD failed");

   // Pivotized alternating least squares
   auto newfit_pals = newfit_svd;
   CpAlsInput<T> pals_input;
   pals_input.SetMaxerr(1.e-7);
   pals_input.SetMaxiter(300);
   auto fit_report_pals = FitPALS(full.GetTensor(), ref_norm*ref_norm, static_cast<CanonicalTensor<T>*>(newfit_pals.GetTensor()), pals_input);

   UNIT_ASSERT( fabs(newfit_pals.Norm() - ref_norm) < 1.e-5, "PALS failed!" );
   UNIT_ASSERT(fit_report_pals.error < 1.e-3, "CP-PALS failed");

   // Pivotized alternating least squares with Newton
   auto newfit_pals_newton = newfit_svd;
   CpAlsInput<T> pals_newton_input;
   pals_newton_input.SetMaxerr(1.e-7);
   pals_newton_input.SetMaxiter(300);
   pals_newton_input.SetCpAlsNewton(true);
   auto fit_report_pals_newton = FitPALS(full.GetTensor(), ref_norm*ref_norm, static_cast<CanonicalTensor<T>*>(newfit_pals_newton.GetTensor()), pals_newton_input);

   UNIT_ASSERT( fabs(newfit_pals_newton.Norm() - ref_norm) < 1.e-5, "PALS Newton failed!" );
   UNIT_ASSERT(fit_report_pals_newton.error < 1.e-3, "CP-PALS (Newton) failed");


   // CP-ID algorithm
   std::vector<unsigned> id_dims(4, 8);
   unsigned id_target_rank = 1000;
   auto id_target = detail::make_test_random_canonicaltensor<T>( id_dims, id_target_rank ); // Random with exponential decay in weights
   NiceTensor<T> id_result;
   CpIdInput<T> id_input;
   id_input.mDoSvd = true;
   id_input.mMaxRank = 100;
   id_input.mAddRank = 20;
   id_input.mRcond = 1.e-8;
   id_input.mIoLevel = I_10;
   FitCPID(id_target, id_result, id_input);
   auto rank_out = static_cast<const CanonicalTensor<T>* const>(id_result.GetTensor())->GetRank();
   auto id_err = diff_norm_new(*id_result.GetTensor(), *id_target.GetTensor());
   auto id_result_norm = id_result.Norm();
   auto id_target_norm = id_target.Norm();
   auto norm_err = fabs(id_result_norm - id_target_norm) / id_target_norm;

   auto expt_err = static_cast<T>(0.);
   for(In irank=rank_out; irank<id_target_rank; ++irank)
   {
      expt_err += std::exp(-static_cast<T>(irank+1));
   }
   expt_err = std::sqrt(expt_err);

   UNIT_ASSERT( norm_err < 1.e-5, "CP-ID (random tensor, decaying weights) failed!");

   // CP-ID with real TensorSum<Nb>
   if constexpr   (  std::is_same_v<T, double>
                  )
   {
      std::ifstream input( std::string(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR")) + std::string("/tensorsum.dat"), std::ios::in | std::ios::binary );
      input.exceptions(std::ios::badbit | std::ios::failbit);
      NiceTensor<T> errvec_in(input);
      input.close();

      // Do pure CP-ID
      NiceTensor<T> errvec_out;
      CpIdInput<T> id_input_errvec;
      id_input_errvec.mMaxRank = 49;
      id_input_errvec.mDoSvd = true;
      id_input_errvec.mAddRank = 100;
      id_input_errvec.mRcond = 1.e-4;
      id_input_errvec.mIoLevel = I_10;
      FitCPID(errvec_in, errvec_out, id_input_errvec);
      auto rank_out_errvec = static_cast<const CanonicalTensor<T>* const>(errvec_out.GetTensor())->GetRank();
      auto id_err_errvec = diff_norm_new(*errvec_out.GetTensor(), *errvec_in.GetTensor());
      auto id_result_norm_errvec = errvec_out.Norm();
      auto id_target_norm_errvec = errvec_in.Norm();
      auto norm_err_errvec = fabs(id_result_norm_errvec - id_target_norm_errvec) / id_target_norm_errvec;

      // Do CP-ALS on top
      CpAlsInput<T> idals_input;
      idals_input.mMaxiter = I_5;
      idals_input.mMaxerr = 1.e-9;
      idals_input.mDistanceConvCheck = false;
      idals_input.mBalanceResult = true;
      idals_input.mCpAlsGelsd = I_20;
      idals_input.mIoLevel = I_10;
      FitCPALS(errvec_in, -C_1, errvec_out, idals_input);  // Pass norm = -1 to see how it goes...
      id_err_errvec = diff_norm_new(*errvec_out.GetTensor(), *errvec_in.GetTensor());
      id_result_norm_errvec = errvec_out.Norm();
      norm_err_errvec = fabs(id_result_norm_errvec - id_target_norm_errvec) / id_target_norm_errvec;

      // Check norm of final fit
      UNIT_ASSERT( norm_err_errvec < 1.e-5, "CP-ID (errvec) failed!");
      UNIT_ASSERT( id_err_errvec   < 1.e-3, "CP-ID (errvec) failed!");
   }
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalDecomposition2DTest<T>::actual_test()
{
   auto simple = detail::make_simple_simpletensor<T>(static_cast<T>(0.0), 2, 3);
   auto canonical = NiceTensor<T>(new CanonicalTensor<T>(dynamic_cast<SimpleTensor<T>*>(simple.GetTensor())->CanonicalSVDGuess(2)));
   dynamic_cast<CanonicalTensor<T>* >(canonical.GetTensor())->FitALS(* dynamic_cast<SimpleTensor<T>*>(simple.GetTensor()), 1000, 1e-6);
   UNIT_ASSERT_FEQUAL(canonical.Norm(), simple.Norm(), "ALS not working for 2D SimpleTensor.");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorScaleTest<T>::actual_test()
{
   auto simple = detail::make_test_simpletensor<T>();
   simple.Scale(static_cast<T>(2.0));
   UNIT_ASSERT_FEQUAL(simple.Norm(), std::sqrt(static_cast<T>(4.0))*detail::make_test_simpletensor<T>().Norm(),"Norm not correct after Scale.");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorZeroTest<T>::actual_test()
{
   auto simple = detail::make_test_simpletensor<T>();
   simple.Zero();
   UNIT_ASSERT_FEQUAL(simple.Norm(), static_cast<T>(0.0), "Norm not correct after Zero.");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   auto simple_a = detail::make_test_simpletensor<T>('a');
   auto simple_b = detail::make_test_simpletensor<T>('b');
   T dot = dot_product(simple_a, simple_b);
   UNIT_ASSERT_FEQUAL(dot, static_cast<T>(2.22758702955160696947), "Wrong result for dot product.");
}

template<class T>
void midas::test::ComplexSimpleTensorDotProductTest<T>::actual_test()
{
   using value_t = std::complex<T>;

   static_assert(std::is_floating_point<T>::value, "Only works with floating point");

   // Test for real numbers
   auto real_simple_a = detail::make_test_simpletensor<value_t>('a');
   auto real_simple_b = detail::make_test_simpletensor<value_t>('b');
   auto real_dot = dot_product(real_simple_a, real_simple_b);
   UNIT_ASSERT_FEQUAL(std::real(real_dot), static_cast<T>(2.22758702955160696947), "Wrong result for real dot product (real part).");
   UNIT_ASSERT_FEQUAL(std::imag(real_dot), static_cast<T>(0.), "Wrong result for real dot product (imag part).");

   auto complex_simple_a = detail::make_complex_test_simpletensor<T>('a');
   auto complex_simple_b = detail::make_complex_test_simpletensor<T>('b');
   auto complex_dot = dot_product(complex_simple_a, complex_simple_b);

   // Calculate extra reference check
   value_t complex_dot_ref(0.);
   auto* a_data = static_cast<const SimpleTensor<value_t>* const>(complex_simple_a.GetTensor())->GetData();
   auto* b_data = static_cast<const SimpleTensor<value_t>* const>(complex_simple_b.GetTensor())->GetData();
   for(In i=I_0; i<complex_simple_a.TotalSize(); ++i)
   {
      complex_dot_ref += midas::math::Conj(a_data[i]) * b_data[i];
   }


   UNIT_ASSERT_FEQUAL_PREC(std::real(complex_dot), static_cast<T>(3.3460678974561775), 3, "Wrong result for complex dot product (real part).");
   UNIT_ASSERT_FEQUAL_PREC(std::imag(complex_dot), static_cast<T>(1.8751718877413732), 2, "Wrong result for complex dot product (imag part).");
   UNIT_ASSERT_FEQUAL_PREC(std::real(complex_dot), std::real(complex_dot_ref), 2, "Wrong result for complex dot product (real part). REF VALUES NOT CONSISTENT!");
   UNIT_ASSERT_FEQUAL_PREC(std::imag(complex_dot), std::imag(complex_dot_ref), 2, "Wrong result for complex dot product (imag part). REF VALUES NOT CONSISTENT!");

   auto complex_dot_cc = dot_product(complex_simple_b, complex_simple_a);
   UNIT_ASSERT_FEQUAL(std::real(complex_dot), std::real(complex_dot_cc), "Complex dot product lacks correct symmetry (real part)!");
   UNIT_ASSERT_FEQUAL(std::imag(complex_dot), -std::imag(complex_dot_cc), "Complex dot product lacks correct symmetry (imag part)!");

   auto complex_dot_aa = dot_product(complex_simple_a, complex_simple_a);
   auto complex_dot_bb = dot_product(complex_simple_b, complex_simple_b);
   auto norm2_a = complex_simple_a.Norm2();
   auto norm2_b = complex_simple_b.Norm2();
   UNIT_ASSERT_FEQUAL(std::real(complex_dot_aa), norm2_a, "Complex dot product: <A|A> != ||A||^2.");
   UNIT_ASSERT_FEQUAL(std::real(complex_dot_bb), norm2_b, "Complex dot product: <B|B> != ||B||^2.!");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   NiceTensor<T> y1_simple = detail::make_test_simpletensor<T>('a');
   NiceTensor<T> y2_simple = y1_simple;
   NiceTensor<T> x_simple  = detail::make_test_simpletensor<T>('b');
   T a1_scalar = static_cast<T>(-3.91550727473222215380e+0);
   T a2_scalar = static_cast<T>( 2.36849013424809529127e+0);

   // Using scalars
   //    a1 = -3.91550727473222215380e+0
   //    a2 =  2.36849013424809529127e+0
   // and SimpleTensors in from make_test_simpletensor, we get for
   //    y1 += a1*x
   //    y2 += a2*x
   // that
   //    |y1|     =   16.164811513167000772855930997281
   //    |y2|     =   10.808952440874846663017872958083
   //    <y1|x>   =  -67.235698029003005759632392512724
   //    <y2|x>   =   44.245924325298632838594356057486
   //    <y1|y2>  = -161.20782094902150352657199167003
   // and the x norm (which should remain unchanged) is
   //    |x|      =    4.2119542132067155693555830374264
   y1_simple.Axpy(x_simple, a1_scalar);
   y2_simple.Axpy(x_simple, a2_scalar);
   T norm_x  = x_simple.Norm();
   T norm_y1 = y1_simple.Norm();
   T norm_y2 = y2_simple.Norm();
   T dot_y1x = dot_product(y1_simple, x_simple);
   T dot_y2x = dot_product(y2_simple, x_simple);
   T dot_12  = dot_product(y1_simple, y2_simple);

   // Some assertions for the result.
   UNIT_ASSERT_FEQUAL(norm_y1, static_cast<T>(  16.164811513167000772855930997281), "Wrong norm of y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(norm_y2, static_cast<T>(  10.808952440874846663017872958083), "Wrong norm of y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(dot_y1x, static_cast<T>( -67.235698029003005759632392512724), "Wrong dot product <y|x>. y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(dot_y2x, static_cast<T>(  44.245924325298632838594356057486), "Wrong dot product <y|x>. y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(dot_12,  static_cast<T>(-161.20782094902150352657199167003 ), "Wrong dot product <y1|y2>.");

   // Also assert that the added tensor is unchanged.
   UNIT_ASSERT_FEQUAL(norm_x, static_cast<T>(4.2119542132067155693555830374264), "Norm of x tensor has changed.");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsCanonicalTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   NiceTensor<T> y1_canonical = detail::make_test_canonicaltensor<T>('a');
   NiceTensor<T> y1_simple    = y1_canonical.ToSimpleTensor();
   NiceTensor<T> y2_simple    = y1_simple;
   NiceTensor<T> x_canonical  = detail::make_test_canonicaltensor<T>('b');
   NiceTensor<T> x_simple     = x_canonical.ToSimpleTensor();
   T a1_scalar = static_cast<T>(-1.62986539738366742469e+0);
   T a2_scalar = static_cast<T>( 3.15469832354693080489e+0);

   T ref_norm_x            = x_canonical.Norm();
   unsigned int ref_rank_x = (dynamic_cast<CanonicalTensor<T>*>(x_canonical.GetTensor()))->GetRank();

   NiceTensor<T> y1_simple_ref = y1_simple;
   NiceTensor<T> y2_simple_ref = y2_simple;
   y1_simple_ref.Axpy(x_simple, a1_scalar);
   y2_simple_ref.Axpy(x_simple, a2_scalar);
   T ref_norm_y1 = y1_simple_ref.Norm();
   T ref_norm_y2 = y2_simple_ref.Norm();
   T ref_dot_y1x = dot_product(y1_simple_ref, x_simple);
   T ref_dot_y2x = dot_product(y2_simple_ref, x_simple);

   y1_simple.Axpy(x_canonical, a1_scalar);
   y2_simple.Axpy(x_canonical, a2_scalar);
   T norm_y1           = y1_simple.Norm();
   T norm_y2           = y2_simple.Norm();
   T dot_y1x           = dot_product(y1_simple, x_simple);
   T dot_y2x           = dot_product(y2_simple, x_simple);
   T norm_x            = x_canonical.Norm();
   unsigned int rank_x = (dynamic_cast<CanonicalTensor<T>*>(x_canonical.GetTensor()))->GetRank();

   UNIT_ASSERT_FEQUAL(norm_y1, ref_norm_y1, "Wrong norm of y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(norm_y2, ref_norm_y2, "Wrong norm of y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(dot_y1x, ref_dot_y1x, "Wrong dot product <y|x>. y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(dot_y2x, ref_dot_y2x, "Wrong dot product <y|x>. y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(norm_x,  ref_norm_x,  "Norm of x tensor has changed.");
   UNIT_ASSERT_EQUAL (rank_x,  ref_rank_x,  "Rank of x tensor has changed.");
}

/**
 *
 **/
template<class T>
void midas::test::SimpleVsZeroTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   NiceTensor<T> y1_simple = detail::make_test_simpletensor<T>('a');
   NiceTensor<T> y2_simple = y1_simple;
   std::vector<unsigned> x_zt_dims(NDIM, DIM);
   NiceTensor<T> x_zero  = detail::make_test_zerotensor<T>(x_zt_dims);
   T a1_scalar = static_cast<T>(-2.76505248363338340667e+0);
   T a2_scalar = static_cast<T>( 2.13604167642099840307e+0);

   T ref_norm = y1_simple.Norm();
   y1_simple.Axpy(x_zero, a1_scalar);
   y2_simple.Axpy(x_zero, a2_scalar);
   T norm_y1 = y1_simple.Norm();
   T norm_y2 = y2_simple.Norm();
   T norm_x  = x_zero.Norm();

   UNIT_ASSERT_FEQUAL(norm_y1, ref_norm, "Wrong norm of y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(norm_y2, ref_norm, "Wrong norm of y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(norm_x,  static_cast<T>(C_0), "Norm of x tensor has changed.");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorScaleTest<T>::actual_test()
{
   auto canonical = detail::make_test_canonicaltensor<T>();
   canonical.Scale(static_cast<T>(2.2));
   UNIT_ASSERT_FEQUAL(canonical.Norm(), static_cast<T>(2.2)*detail::make_test_canonicaltensor<T>().Norm(), "Norm not correct after Scale of CanonicalTensor with positive number.");

   auto canonical2 = detail::make_test_canonicaltensor<T>();
   canonical2.Scale(static_cast<T>(-2.2));
   UNIT_ASSERT_FEQUAL(canonical2.Norm(), static_cast<T>(2.2)*detail::make_test_canonicaltensor<T>().Norm(), "Norm not correct after Scale of CanonicalTensor with negative number.");
}

/**
 *
 **/
template <class T>
void midas::test::CanonicalTensorBalanceTest<T>::actual_test()
{
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto ref = canonical;

   static_cast<CanonicalTensor<T>*>(canonical.GetTensor())->BalanceModeVectors();

   auto err = diff_norm_new(*canonical.GetTensor(), *ref.GetTensor());

   UNIT_ASSERT_FEQUAL(ref.Norm(), canonical.Norm(), "Norm not correct after BalanceModeVectors()");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorToSimpleTensorTest<T>::actual_test()
{
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto simple = canonical.ToSimpleTensor();
   UNIT_ASSERT_FEQUAL(canonical.Norm(), simple.Norm(), "Norm not correct after ToSimpleTensor of CanonicalTensor.");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   auto canonical_a = detail::make_test_canonicaltensor<T>('a');
   auto canonical_b = detail::make_test_canonicaltensor<T>('b');
   auto ref_simple_a = canonical_a.ToSimpleTensor();
   auto ref_simple_b = canonical_b.ToSimpleTensor();
   T ref_dot = dot_product(ref_simple_a, ref_simple_b);
   T dot = dot_product(canonical_a, canonical_b);
   // Precision: 12*std::numerical_limits<T>::epsilon() necessary due to
   // round-off errors when computing dot product for CanonicalTensor.
   UNIT_ASSERT_FEQUAL_PREC(dot, static_cast<T>(ref_dot), 15, "Wrong result for dot product.");
}

/**
 *
 **/
template<class T>
void midas::test::ComplexCanonicalTensorDotProductTest<T>::actual_test()
{
   using value_t = std::complex<T>;
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   auto real_canonical_a = detail::make_test_canonicaltensor<value_t>('a');
   auto real_canonical_b = detail::make_test_canonicaltensor<value_t>('b');
   auto ref_real_simple_a = real_canonical_a.ToSimpleTensor();
   auto ref_real_simple_b = real_canonical_b.ToSimpleTensor();
   value_t ref_real_dot = dot_product(ref_real_simple_a, ref_real_simple_b);
   value_t real_dot = dot_product(real_canonical_a, real_canonical_b);
   // Precision: 12*std::numerical_limits<T>::epsilon() necessary due to
   // round-off errors when computing dot product for CanonicalTensor.
   UNIT_ASSERT_FEQUAL_PREC(std::real(real_dot), std::real(ref_real_dot), 12, "Wrong result for complex dot product of real-valued CanonicalTensor (real part).");
   UNIT_ASSERT_FEQUAL_PREC(std::imag(real_dot), std::imag(ref_real_dot), 12, "Wrong result for complex dot product of real-valued CanonicalTensor (imag part).");

   auto canonical_a = detail::make_complex_test_canonicaltensor<T>('a');
   auto canonical_b = detail::make_complex_test_canonicaltensor<T>('b');
   auto ref_simple_a = canonical_a.ToSimpleTensor();
   auto ref_simple_b = canonical_b.ToSimpleTensor();
   value_t ref_dot = dot_product(ref_simple_a, ref_simple_b);
   value_t dot = dot_product(canonical_a, canonical_b);
   // Precision: 18*std::numerical_limits<T>::epsilon() necessary due to
   // round-off errors when computing dot product for CanonicalTensor.
   UNIT_ASSERT_FEQUAL_PREC(std::real(dot), std::real(ref_dot), 18, "Wrong result for complex dot product of complex-valued CanonicalTensor (real part).");
   UNIT_ASSERT_FEQUAL_PREC(std::imag(dot), std::imag(ref_dot), 42, "Wrong result for complex dot product of complex-valued CanonicalTensor (imag part).");

   auto dot_cc = dot_product(canonical_b, canonical_a);
   UNIT_ASSERT_FEQUAL_PREC(std::real(dot),  std::real(dot_cc), 10, "Complex CanonicalTensor dot product lacks symmetry (real part).");
   UNIT_ASSERT_FEQUAL_PREC(std::imag(dot), -std::imag(dot_cc), 10, "Complex CanonicalTensor dot product lacks symmetry (imag part).");

   auto dot_aa = dot_product(canonical_a, canonical_a);
   auto dot_bb = dot_product(canonical_b, canonical_b);
   auto norm2_a = canonical_a.Norm2();
   auto norm2_b = canonical_b.Norm2();
   UNIT_ASSERT_FEQUAL(std::real(dot_aa), norm2_a, "Complex dot product: <A|A> != ||A||^2.");
   UNIT_ASSERT_FEQUAL(std::real(dot_bb), norm2_b, "Complex dot product: <B|B> != ||B||^2.!");
   UNIT_ASSERT_FZERO_PREC(std::imag(dot_aa), norm2_a, 2, "<A|A> has non-zero imaginary part!");
   UNIT_ASSERT_FZERO_PREC(std::imag(dot_bb), norm2_b, 2, "<B|B> has non-zero imaginary part!");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorDumpIntoTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");

   // Real
   auto canonical = detail::make_test_canonicaltensor<T>('a');
   auto ptr = std::unique_ptr<T[]>(new T[canonical.TotalSize()]);
   canonical.DumpInto(ptr.get());
   auto ref_simple = NiceTensor<T>(new SimpleTensor<T>(canonical.GetDims(), ptr.release()));
   
   UNIT_ASSERT_FEQUAL(canonical.Norm(), ref_simple.Norm(), "Norm not correct after dump into.");


   // Complex
   auto c_canonical = detail::make_complex_test_canonicaltensor<T>();
   auto c_ptr = std::make_unique<std::complex<T>[]>(c_canonical.TotalSize());
   c_canonical.DumpInto(c_ptr.get());
   auto c_ref_simple = NiceSimpleTensor<std::complex<T>>(c_canonical.GetDims(), c_ptr.release());

   UNIT_ASSERT_FEQUAL(c_canonical.Norm(), c_ref_simple.Norm(), "Norm not correct after complex DumpInto");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorAddVectorsTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   std::vector<unsigned> dims = {3};
   auto ndim = dims.size();
   
   auto cp_1 = NiceCanonicalTensor<T>(dims, 1);
   {
      auto& cp_1_cast = *static_cast<CanonicalTensor<T>*>(cp_1.GetTensor());
      T** ptr_1 = cp_1_cast.GetModeMatrices();
      ptr_1[0][0] = T(1.7);
      ptr_1[0][1] = T(2.4);
      ptr_1[0][2] = T(3.1);
   }
   auto cp_1_ref = cp_1;

   auto cp_2 = NiceCanonicalTensor<T>(dims, 2);
   {
      auto& cp_2_cast = *static_cast<CanonicalTensor<T>*>(cp_2.GetTensor());
      T** ptr_2 = cp_2_cast.GetModeMatrices();
      ptr_2[0][0] = T(-2.0);
      ptr_2[0][1] = T(-2.8);
      ptr_2[0][2] = T(-3.0);
      ptr_2[0][3] = T(0.3);
      ptr_2[0][4] = T(0.4);
      ptr_2[0][5] = T(-0.1);
   }

   // Add tensors (should give zero)
   auto norm_ref = cp_1.Norm();
   cp_1 += cp_2;

   UNIT_ASSERT_FZERO(cp_1.Norm(), norm_ref, "Error in CanonicalTensor::operator+= for rank-1 vector! Norm wrong!");
   UNIT_ASSERT_EQUAL(static_cast<CanonicalTensor<T>*>(cp_1.GetTensor())->GetRank(), unsigned(1), "Error in CanonicalTensor::operator+= for rank-1 vector! Rank wrong!");

   // Reset
   cp_1 = cp_1_ref;

   // Do Axpy
   cp_1.Scale(T(2.));
   norm_ref = cp_1.Norm();

   cp_1.Axpy(cp_2, T(2.));

   UNIT_ASSERT_FZERO(cp_1.Norm(), norm_ref, "Error in CanonicalTensor::Axpy() for rank-1 vector! Norm wrong!");
   UNIT_ASSERT_EQUAL(static_cast<CanonicalTensor<T>*>(cp_1.GetTensor())->GetRank(), unsigned(1), "Error in CanonicalTensor::Axpy() for rank-1 vector! Rank wrong!");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalVsSimpleTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");

   // Real
   {
   auto canonical  = detail::make_test_canonicaltensor<T>('a');
   auto simple     = detail::make_test_simpletensor<T>('a');
   auto ref_simple = canonical.ToSimpleTensor();
   T ref_dot = dot_product(simple, ref_simple);
   T dot_a   = dot_product(canonical, simple);
   T dot_b   = dot_product(simple, canonical);
   // Precision: 6*std::numerical_limits<T>::epsilon() necessary due to
   // round-off errors when computing dot product for CanonicalTensor.
   UNIT_ASSERT_FEQUAL_PREC(dot_a, ref_dot, 6, "Wrong result for dot_product(canonical, simple).");
   UNIT_ASSERT_FEQUAL_PREC(dot_b, ref_dot, 6, "Wrong result for dot_product(simple, canonical).");
   }

   // Complex
   {
   auto c_canonical  = detail::make_complex_test_canonicaltensor<T>('a');
   auto c_simple     = detail::make_complex_test_simpletensor<T>('a');
   auto c_ref_simple = c_canonical.ToSimpleTensor();
   auto c_ref_dot = dot_product(c_simple, c_ref_simple);
   auto c_dot_a   = dot_product(c_canonical, c_simple);
   auto c_dot_b   = dot_product(c_simple, c_canonical);
   // Precision: 6*std::numerical_limits<T>::epsilon() necessary due to
   // round-off errors when computing dot product for CanonicalTensor.
   UNIT_ASSERT_FEQUAL_PREC(c_dot_a, midas::math::Conj(c_ref_dot), 6, "Wrong result for dot_product(c_canonical, c_simple).");
   UNIT_ASSERT_FEQUAL_PREC(c_dot_b, c_ref_dot, 6, "Wrong result for dot_product(c_simple, c_canonical).");
   }
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   NiceTensor<T> y1_canonical = detail::make_test_canonicaltensor<T>('a');
   NiceTensor<T> y2_canonical = y1_canonical;
   NiceTensor<T> x_canonical  = detail::make_test_canonicaltensor<T>('b');
   T a1_scalar = static_cast<T>(-6.68531886566040456366e-1);
   T a2_scalar = static_cast<T>( 6.33637309186703467923e-1);

   // References.
   T            ref_norm_x  = x_canonical.Norm();
   unsigned int ref_rank_x  = (dynamic_cast<CanonicalTensor<T>*>(x_canonical.GetTensor()))->GetRank();
   unsigned int ref_rank_y1 = (dynamic_cast<CanonicalTensor<T>*>(y1_canonical.GetTensor()))->GetRank();
   unsigned int ref_rank_y2 = (dynamic_cast<CanonicalTensor<T>*>(y2_canonical.GetTensor()))->GetRank();
   NiceTensor<T> y1_simple_ref = y1_canonical.ToSimpleTensor();
   NiceTensor<T> y2_simple_ref = y1_simple_ref;
   NiceTensor<T> x_simple_ref  = x_canonical.ToSimpleTensor();
   y1_simple_ref.Axpy(x_simple_ref, a1_scalar);
   y2_simple_ref.Axpy(x_simple_ref, a2_scalar);
   T ref_norm_y1 = y1_simple_ref.Norm();
   T ref_norm_y2 = y2_simple_ref.Norm();
   T ref_dot_12  = dot_product(y1_simple_ref, y2_simple_ref);

   // Tests.
   y1_canonical.Axpy(x_canonical, a1_scalar);
   y2_canonical.Axpy(x_canonical, a2_scalar);
   T norm_y1 = y1_canonical.Norm();
   T norm_y2 = y2_canonical.Norm();
   T dot_12  = dot_product(y1_canonical, y2_canonical);
   T            norm_x  = x_canonical.Norm();
   unsigned int rank_x  = (dynamic_cast<CanonicalTensor<T>*>(x_canonical.GetTensor()))->GetRank();
   unsigned int rank_y1 = (dynamic_cast<CanonicalTensor<T>*>(y1_canonical.GetTensor()))->GetRank();
   unsigned int rank_y2 = (dynamic_cast<CanonicalTensor<T>*>(y2_canonical.GetTensor()))->GetRank();

   UNIT_ASSERT_FEQUAL(norm_y1, ref_norm_y1, "Wrong norm of y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(norm_y2, ref_norm_y2, "Wrong norm of y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL_PREC(dot_12,  ref_dot_12, 4, "Wrong dot product <y1|y2>.");
   UNIT_ASSERT_FEQUAL(norm_x,  ref_norm_x,  "Norm of x tensor has changed.");
   UNIT_ASSERT_EQUAL(rank_x,  ref_rank_x, "Rank of x tensor has changed.");
   UNIT_ASSERT_EQUAL(rank_y1, (ref_rank_y1 + ref_rank_x), "rank(y1_final) != rank(y1_init) + rank(x_init).");
   UNIT_ASSERT_EQUAL(rank_y2, (ref_rank_y2 + ref_rank_x), "rank(y2_final) != rank(y2_init) + rank(x_init).");
}

/**
 *
 **/
template<class T>
void midas::test::CanonicalVsZeroTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   NiceTensor<T> y1_canonical = detail::make_test_canonicaltensor<T>('a');
   NiceTensor<T> y2_canonical = y1_canonical;
   std::vector<unsigned> x_zt_dims(NDIM, DIM);
   NiceTensor<T> x_zero  = detail::make_test_zerotensor<T>(x_zt_dims);
   T a1_scalar = static_cast<T>(-1.02029341357061453799e+0);
   T a2_scalar = static_cast<T>( 2.07679457406854472978e+0);

   T ref_norm = y1_canonical.Norm();
   y1_canonical.Axpy(x_zero, a1_scalar);
   y2_canonical.Axpy(x_zero, a2_scalar);
   T norm_y1 = y1_canonical.Norm();
   T norm_y2 = y2_canonical.Norm();
   T norm_x  = x_zero.Norm();

   UNIT_ASSERT_FEQUAL(norm_y1, ref_norm, "Wrong norm of y = a*x + y, a < 0.");
   UNIT_ASSERT_FEQUAL(norm_y2, ref_norm, "Wrong norm of y = a*x + y, a > 0.");
   UNIT_ASSERT_FEQUAL(norm_x,  static_cast<T>(C_0), "Norm of x tensor has changed.");
}

/**
 *
 **/
template<class T>
void midas::test::ZeroTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   std::vector<unsigned> dims{I_2, I_3, I_4};
   auto zt = detail::make_test_zerotensor<T>(dims);
   T dot = dot_product(zt, zt);
   UNIT_ASSERT_FEQUAL(dot, static_cast<T>(C_0), "Wrong result for dot product.");
}

/**
 *
 **/
template<class T>
void midas::test::ZeroVsSimpleTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   std::vector<unsigned> zt_dims(NDIM, DIM);
   auto zt = detail::make_test_zerotensor<T>(zt_dims);
   auto st = detail::make_test_simpletensor<T>('a');
   UNIT_ASSERT_FEQUAL(zt.Norm(), static_cast<T>(C_0), "norm(ZeroTensor) != 0.");
   UNIT_ASSERT_FEQUAL(st.Norm(), static_cast<T>(2.60026916323381418082), "norm(SimpleTensor) wrong.");
   T dot_zs = dot_product(zt, st);
   T dot_sz = dot_product(st, zt);
   UNIT_ASSERT_FEQUAL(dot_zs, static_cast<T>(C_0), "dot_product(ZeroTensor,SimpleTensor) != 0.");
   UNIT_ASSERT_FEQUAL(dot_sz, static_cast<T>(C_0), "dot_product(SimpleTensor,ZeroTensor) != 0.");
}

/**
 *
 **/
template<class T>
void midas::test::ZeroVsCanonicalTensorDotProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   std::vector<unsigned> zt_dims(NDIM, DIM);
   auto zt = detail::make_test_zerotensor<T>(zt_dims);
   auto ct = detail::make_test_canonicaltensor<T>('a');
   UNIT_ASSERT_FEQUAL(zt.Norm(), static_cast<T>(C_0), "norm(ZeroTensor) != 0.");
   UNIT_ASSERT_FEQUAL(ct.Norm(), static_cast<T>(1.364508870102732585), "norm(CanonicalTensor) wrong.");
   T dot_zc = dot_product(zt, ct);
   T dot_cz = dot_product(ct, zt);
   UNIT_ASSERT_FEQUAL(dot_zc, static_cast<T>(C_0), "dot_product(ZeroTensor,CanonicalTensor) != 0.");
   UNIT_ASSERT_FEQUAL(dot_cz, static_cast<T>(C_0), "dot_product(CanonicalTensor,ZeroTensor) != 0.");
}

/**
 *
 **/
template<class T>
void midas::test::ZeroTensorAxpyTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point");
   std::vector<unsigned> dims{I_2, I_3, I_4};
   NiceTensor<T> zt = detail::make_test_zerotensor<T>(dims);
   zt.Axpy(zt, static_cast<T>(2.0));
   T norm = zt.Norm();
   UNIT_ASSERT_FEQUAL(norm, static_cast<T>(C_0), "Norm of ZT after Axpy with ZT is not 0.");
}

/**
 * 
 **/
template<class T>
void midas::test::ZeroTensorConstructionTest<T>::actual_test()
{
   auto dims = std::vector<unsigned>{2, 3, 4};
   auto zero = NiceTensor<T>(new ZeroTensor<T>(dims));
   UNIT_ASSERT_FEQUAL(zero.Norm(), static_cast<T>(0.0), " ZeroTensor construction. Norm not correct.");
   UNIT_ASSERT_EQUAL (zero.GetDims(), dims, "ZeroTensor construction: Dims not correct.");
}

/**
 *
 **/
template<class T>
void midas::test::ZeroTensorContractDownTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dims = std::vector<unsigned>{3, 3, 3};
   auto dummy  = detail::make_test_zerotensor<T>(dims);
   auto vector = detail::make_test_vector<T>();

   /*********** ContractDown ***********/
   {
      auto foo = dummy.ContractDown(0, vector);
      auto dims_contracted = std::vector<unsigned>{3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractDown type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractDown 0 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractDown Dims not correct.");
   }
   {
      auto foo = dummy.ContractDown(1, vector);
      auto dims_contracted = std::vector<unsigned>{3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractDown type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractDown 1 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractDown Dims not correct.");
   }
   {
      auto foo = dummy.ContractDown(2, vector);
      auto dims_contracted = std::vector<unsigned>{3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractDown type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractDown 2 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractDown Dims not correct.");
   }
};

/**
 *
 **/
template<class T>
void midas::test::ZeroTensorContractForwardTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dims = std::vector<unsigned>{3, 3, 3};
   auto dummy  = detail::make_test_zerotensor<T>(dims);
   auto matrix = detail::make_test_matrix<T>();

   /*********** ContractForward ***********/
   {
      auto foo = dummy.ContractForward(0, matrix);
      auto dims_contracted = std::vector<unsigned>{3, 3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractForward type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractForward 0 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractForward Dims not correct.");
   }
   {
      auto foo = dummy.ContractForward(1, matrix);
      auto dims_contracted = std::vector<unsigned>{3, 3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractForward type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractForward 1 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractForward Dims not correct.");
   }
   {
      auto foo = dummy.ContractForward(2, matrix);
      auto dims_contracted = std::vector<unsigned>{3, 3, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractForward type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractForward 2 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractForward Dims not correct.");
   }
};

/**
 *
 **/
template<class T>
void midas::test::ZeroTensorContractUpTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto dims = std::vector<unsigned>{2, 4, 5};
   auto dummy  = detail::make_test_zerotensor<T>(dims);
   auto vector = detail::make_test_vector<T>();

   /*********** ContractUp ***********/
   {
      auto foo = dummy.ContractUp(0, vector);
      auto dims_contracted = std::vector<unsigned>{3, 2, 4, 5};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractUp type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractUp 0 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractUp Dims not correct.");
   }
   {
      auto foo = dummy.ContractUp(1, vector);
      auto dims_contracted = std::vector<unsigned>{2, 3, 4, 5};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractUp type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractUp 1 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractUp Dims not correct.");
   }
   {
      auto foo = dummy.ContractUp(2, vector);
      auto dims_contracted = std::vector<unsigned>{2, 4, 3, 5};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractUp type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractUp 2 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractUp Dims not correct.");
   }
   {
      auto foo = dummy.ContractUp(3, vector);
      auto dims_contracted = std::vector<unsigned>{2, 4, 5, 3};
      UNIT_ASSERT_EQUAL ( static_cast<int>(foo.Type()), static_cast<int>(BaseTensor<T>::typeID::ZERO), "ZeroTensor::ContractUp type not correct.");
      UNIT_ASSERT_FEQUAL( foo.Norm(), static_cast<T>(0.0), "ZeroTensor::ContractUp 2 failed" );
      UNIT_ASSERT_EQUAL ( foo.GetDims(), dims_contracted, "ZeroTensor::ContractUp Dims not correct.");
   }
};

/**
 *
 **/
template<class T>
void midas::test::DiffNormTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensora = detail::make_test_simpletensor<T>('a');
   auto tensorb = detail::make_test_simpletensor<T>('b');
   
   auto diff1 = diff_norm(*(tensora.GetTensor()), *(tensorb.GetTensor()));
   auto diff1new = diff_norm_new(*(tensora.GetTensor()), *(tensorb.GetTensor()));
   UNIT_ASSERT_FEQUAL(diff1, static_cast<T>(4.47736350504527003125), "diff_norm not correct 1");
   UNIT_ASSERT_FEQUAL(diff1new, static_cast<T>(4.47736350504527003125), "diff_norm_new not correct 1");
   
   auto diff2 = diff_norm(*(tensora.GetTensor()), *(tensora.GetTensor()));
   auto diff2new = diff_norm_new(*(tensora.GetTensor()), *(tensora.GetTensor()));
   UNIT_ASSERT_FEQUAL(diff2, static_cast<T>(0.0), "diff_norm not correct 2");
   UNIT_ASSERT_FEQUAL(diff2new, static_cast<T>(0.0), "diff_norm_new not correct 2");
   
   auto canonical = detail::make_test_canonicaltensor<T>();
   auto diff3 = diff_norm(*(tensora.GetTensor()), *(canonical.GetTensor()));
   auto diff3new = diff_norm_new(*(tensora.GetTensor()), *(canonical.GetTensor()));
   UNIT_ASSERT_FEQUAL(diff3, static_cast<T>(2.98563931316080122258), "diff_norm not correct 3");
   UNIT_ASSERT_FEQUAL(diff3new, static_cast<T>(2.98563931316080122258), "diff_norm_new not correct 3");
}

/**
 *
 **/
template<class T>
void midas::test::GammaMatrixTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonicaltensora = detail::make_test_canonicaltensor<T>('a');
   
   auto ranka = dynamic_cast<CanonicalTensor<T>* >(canonicaltensora.GetTensor())->GetRank();
   auto gamma_size_aa = ranka*ranka;
   auto gamma_aa0 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 0);
   auto gamma_aa1 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 1);
   auto gamma_aa2 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 2);
   
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa0, gamma_size_aa), static_cast<T>(1.02004906924761185039), "Gamma matrix 0 not correct.");
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa1, gamma_size_aa), static_cast<T>(0.76988396389407165454), "Gamma matrix 1 not correct.");
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa2, gamma_size_aa), static_cast<T>(3.85602951557976725283), "Gamma matrix 2 not correct.");
}

/**
 *
 **/
template<class T>
void midas::test::GeneralGammaMatrixTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto canonicaltensora = detail::make_test_generalcanonicaltensor<T>(2, 'a');
   
   auto ranka = dynamic_cast<CanonicalTensor<T>* >(canonicaltensora.GetTensor())->GetRank();
   auto gamma_size_aa = ranka*ranka;
   auto gamma_aa0 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 0);
   auto gamma_aa1 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 1);
   auto gamma_aa2 = detail::GammaMatrix(canonicaltensora, canonicaltensora, 2);
   
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa0, gamma_size_aa), static_cast<T>(2.09024715049346143658), "General Gamma matrix 0 not correct.");
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa1, gamma_size_aa), static_cast<T>(0.86486497796606709443), "General Gamma matrix 1 not correct.");
   UNIT_ASSERT_FEQUAL(detail::gamma_norm(gamma_aa2, gamma_size_aa), static_cast<T>(3.28870785344308602660), "General Gamma matrix 2 not correct.");
}

/**
 *
 **/
template<class T>
void midas::test::TensorDirectProductTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensordirectproduct_a = detail::make_test_tensordirectproduct<T>('a');
   auto simple_ref_a = tensordirectproduct_a.ToSimpleTensor();

   UNIT_ASSERT_FEQUAL(tensordirectproduct_a.Norm(), simple_ref_a.Norm(), "Norm not correct for TensorDirectProduct a.");
   
   auto tensordirectproduct_b = detail::make_test_tensordirectproduct<T>('b');
   auto simple_ref_b = tensordirectproduct_b.ToSimpleTensor();

   UNIT_ASSERT_FEQUAL(tensordirectproduct_b.Norm(), simple_ref_b.Norm(), "Norm not correct for TensorDirectProduct b.");

   NiceTensor<T> cp_tensor_a( static_cast<TensorDirectProduct<T>*>(tensordirectproduct_a.GetTensor())->ConstructModeMatrices() );
   NiceTensor<T> cp_tensor_b( static_cast<TensorDirectProduct<T>*>(tensordirectproduct_b.GetTensor())->ConstructModeMatrices() );

   auto err_a = std::sqrt(safe_diff_norm2(cp_tensor_a.GetTensor(), tensordirectproduct_a.GetTensor()));
   auto err_b = std::sqrt(safe_diff_norm2(cp_tensor_b.GetTensor(), tensordirectproduct_b.GetTensor()));

   UNIT_ASSERT_FEQUAL(tensordirectproduct_a.Norm(), cp_tensor_a.Norm(), "Norm a not correct after TensorDirectProduct::ConstructModeMatrices()");
   UNIT_ASSERT_FEQUAL(tensordirectproduct_b.Norm(), cp_tensor_b.Norm(), "Norm b not correct after TensorDirectProduct::ConstructModeMatrices()");
}


/**
 *
 **/
template<class T>
void midas::test::TensorDirectProductDotTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensordirectproduct_a = detail::make_test_tensordirectproduct<T>('a');
   auto tensordirectproduct_b = detail::make_test_tensordirectproduct<T>('b');
   auto simple_ref_a = tensordirectproduct_a.ToSimpleTensor();
   auto simple_ref_b = tensordirectproduct_b.ToSimpleTensor();

   UNIT_ASSERT_FEQUAL( dot_product(tensordirectproduct_a, tensordirectproduct_b)
                     , dot_product(simple_ref_a, simple_ref_b)
                     , "TensorDirectProduct dot not correct.");
}

/**
 *
 **/
template<class T>
void midas::test::TensorDirectProductVsCanonicalTensorDotTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensordirectproduct = detail::make_test_tensordirectproduct<T>('a');
   auto canonicaltensor = detail::make_test_generalcanonicaltensor<T>(2, 'a');
   
   auto simple_ref_directproduct = tensordirectproduct.ToSimpleTensor();
   auto simple_ref_canonical = canonicaltensor.ToSimpleTensor();
   
   UNIT_ASSERT_FEQUAL_PREC( dot_product(tensordirectproduct, canonicaltensor)
                          , dot_product(simple_ref_directproduct, simple_ref_canonical)
                          , 4, "TensorDirectProduct vs CanonicalTensor dot not correct.");
}

/**
 * Test that we can recompress a TensorDirectProduct of ALL CanonicalTensors
 **/
template<class T>
void midas::test::TensorDirectProductCanonicalRecompressTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensordirectproduct = detail::make_test_tensordirectproduct<T>('b');
   auto canonicaltensor = detail::make_test_generalcanonicaltensor<T>(1, 'a');
   
   auto fit_report = FitCPALS(tensordirectproduct, tensordirectproduct.Norm2(), canonicaltensor, CpAlsInput<T>());
   
   UNIT_ASSERT      (fit_report.converged,                         "Not converged");
   UNIT_ASSERT_EQUAL(fit_report.numiter, static_cast<unsigned>(1), "Number of iterations not 1");
   UNIT_ASSERT      (fit_report.error < 1e-7,                      "Error not below 1e-7");
}

/**
 *
 **/
template<class T>
void midas::test::TensorSumAdditionAssignmentTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensorsum = NiceTensor<T>(new TensorSum<T>(std::vector<unsigned>{3,3,3}));
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');
   auto canonicala = detail::make_test_canonicaltensor<T>('a');
   
   tensorsum += simplea;
   UNIT_ASSERT_FEQUAL(tensorsum.Norm(), static_cast<T>(2.60026916323381396268), "TensorSum norm not correct after += (1)");
   
   tensorsum += simpleb;
   UNIT_ASSERT_FEQUAL(tensorsum.Norm(), static_cast<T>(5.38118314820427725165), "TensorSum norm not correct after += (2)");
   
   tensorsum += canonicala;
   UNIT_ASSERT_FEQUAL(tensorsum.Norm(), static_cast<T>(5.47827214840072588978), "TensorSum norm not correct after += (3)");
}

/**
 *
 **/
template<class T>
void midas::test::TensorSumSubtractionAssignmentTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensorsum = NiceTensor<T>(new TensorSum<T>(std::vector<unsigned>{3,3,3}));
   auto simplea = detail::make_test_simpletensor<T>('a');
   auto simpleb = detail::make_test_simpletensor<T>('b');
   auto canonicala = detail::make_test_canonicaltensor<T>('a');
   
   tensorsum += simplea;
   tensorsum -= simpleb;
   UNIT_ASSERT_FEQUAL(tensorsum.Norm(), static_cast<T>(4.47736350504527034827), "TensorSum norm not correct after -= (1)");
   
   tensorsum -= canonicala;
   UNIT_ASSERT_FEQUAL(tensorsum.Norm(), static_cast<T>(4.65646147569287253282), "TensorSum norm not correct after -= (2)");
}

/**
 *
 **/
template<class T>
void midas::test::TensorSumDotTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensorsuma = detail::make_test_tensorsum<T>('a');
   auto tensorsumb = detail::make_test_tensorsum<T>('b');
   auto simpleb = detail::make_test_simpletensor<T>('b');
   auto canonicalb = detail::make_test_canonicaltensor<T>('b');
   
   UNIT_ASSERT_FEQUAL( dot_product(tensorsuma, simpleb)
                     , static_cast<T>(+19.96814532370140936497)
                     , "TensorSum vs Simple dot not correct.");
   UNIT_ASSERT_FEQUAL( dot_product(simpleb, tensorsuma) // arguments swapped
                     , static_cast<T>(+19.96814532370140936497)
                     , "Simple vs TensorSum dot not correct.");
   UNIT_ASSERT_FEQUAL_PREC( dot_product(tensorsuma, canonicalb)
                          , static_cast<T>(-1.10048758133229866501)
                          , 4   // needs a little looser threshold
                          , "TensorSum vs Canonical dot not correct.");
   UNIT_ASSERT_FEQUAL( dot_product(tensorsuma, tensorsumb)
                     , static_cast<T>(+8.58521135123447319548)
                     , "TensorSum vs TensorSum dot not correct.");
}


/**
 *
 **/
template<class T>
void midas::test::TensorSumDotCoefTest<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "only works with floating point");
   auto tensorsuma = detail::make_test_tensorsum<T>('e');
   auto tensorsumb = detail::make_test_tensorsum<T>('f');
   auto simpleb = detail::make_test_simpletensor<T>('b');
   auto canonicalb = detail::make_test_canonicaltensor<T>('b');
   
   UNIT_ASSERT_FEQUAL( dot_product(tensorsuma, simpleb)
                     , static_cast<T>(-19.96814532370140936497)
                     , "TensorSum vs Simple dot not correct.");
   UNIT_ASSERT_FEQUAL( dot_product(simpleb, tensorsuma) // arguments swapped
                     , static_cast<T>(-19.96814532370140936497)
                     , "Simple vs TensorSum dot not correct.");
   UNIT_ASSERT_FEQUAL_PREC( dot_product(tensorsuma, canonicalb)
                          , static_cast<T>(+1.10048758133229866501)
                          , 4   // needs a little looser threshold
                          , "TensorSum vs Canonical dot not correct.");
   UNIT_ASSERT_FEQUAL( dot_product(tensorsuma, tensorsumb)
                     , static_cast<T>(+8.58521135123447319548)
                     , "TensorSum vs TensorSum dot not correct.");
}

/**
 *
 **/
template<class T>
void midas::test::DotIndexCreatorTest<T>::actual_test()
{
   IndexCreator index_creator;

   UNIT_ASSERT_EQUAL(index_creator.GetLeftIndex(0) , 0, "IndexCreator GetLeftIndex failed 1.");
   UNIT_ASSERT_EQUAL(index_creator.GetLeftIndex(1) , 1, "IndexCreator GetLeftIndex failed 2.");
   UNIT_ASSERT_EQUAL(index_creator.GetRightIndex(0), 2, "IndexCreator GetRightIndex failed 1.");
   UNIT_ASSERT_EQUAL(index_creator.GetRightIndex(2), 3, "IndexCreator GetRightIndex failed 2.");
   UNIT_ASSERT_EQUAL(index_creator.GetLeftIndex(0) , 0, "IndexCreator GetLeftIndex failed 3.");
   UNIT_ASSERT_EQUAL(index_creator.GetRightIndex(2), 3, "IndexCreator GetRightIndex failed 3.");
}

/**
 *
 **/
template<class T>
void midas::test::NiceTensorFromTypeID<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point.");

   // Dimensions, common for all the tensors in this test.
   std::vector<unsigned int> dims_tensor{I_2, I_4, I_3};
   std::vector<unsigned int> dims_scalar;

   // Vector of pairs (typeID, type string), for looping.
   std::vector<std::pair<typename BaseTensor<T>::typeID, std::string> > type_id{
      {BaseTensor<T>::typeID::SIMPLE,    "SimpleTensor"},
      {BaseTensor<T>::typeID::CANONICAL, "CanonicalTensor"},
      {BaseTensor<T>::typeID::SCALAR,    "Scalar"},
      {BaseTensor<T>::typeID::ZERO,      "ZeroTensor"}
   };

   // We test that
   //    -  mTensor of the NiceTensor is not a nullptr.
   //    -  type is correct.
   //    -  norm is zero.
   //    -  in the case of CanonicalTensor, that rank is 0.
   for(auto it = type_id.begin(); it != type_id.end(); ++it)
   {
      // Set dims. according to Scalar/non-Scalar.
      std::vector<unsigned int> dims = (it->first == BaseTensor<T>::typeID::SCALAR)? dims_scalar : dims_tensor;

      // The constructor should throw a MIDASERROR if type doesn't match. 
      NiceTensor<T> nt(dims, it->first);

      // Checks.
      UNIT_ASSERT(!nt.IsNullPtr(), "NiceTensor("+it->second+") contains nullptr.");
      UNIT_ASSERT(nt.Type() == it->first, "NiceTensor("+it->second+"); wrong type (enum is "+std::to_string(static_cast<In>(nt.Type()))+", should have been "+std::to_string(static_cast<In>(it->first))+").");
      UNIT_ASSERT_FEQUAL(nt.Norm(), static_cast<T>(C_0), "NiceTensor("+it->second+"); non-zero norm.");

      if(it->first == BaseTensor<T>::typeID::CANONICAL)
      {
         CanonicalTensor<T>* ct_ptr = dynamic_cast<CanonicalTensor<T>*>(nt.GetTensor());
         UNIT_ASSERT(ct_ptr != nullptr, "Couldn't dyn.cast. to NiceTensor("+it->second+").GetTensor() to CanonicalTensor*.");
         UNIT_ASSERT_EQUAL(static_cast<unsigned int>(ct_ptr->GetRank()), static_cast<unsigned int>(I_0), "NiceTensor("+it->second+"); non-zero rank.");
      }
   }
};

/**
 *
 **/
template<class T>
void midas::test::UnitNiceTensorFromTypeID<T>::actual_test()
{
   static_assert(std::is_floating_point<T>::value, "Only works with floating point.");

   // Dimensions, common for all the tensors in this test.
   const std::vector<unsigned int> dims_tensor{I_3, I_6, I_4};
   const std::vector<unsigned int> dims_scalar;
   unsigned int size_tensor = I_1;
   unsigned int size_scalar = I_1;
   for(In i = I_0; i < dims_tensor.size(); ++i)
   {
      size_tensor *= dims_tensor[i];
   }

   // Index number to be set equal to 1 in the test, (1, 4, 2).
   // Corresponds to the position (for dimensions (3, 6, 4)):
   //    1*(6*4) + 4*(4) + 2  =  42.
   const std::vector<unsigned int> index_tensor{I_1, I_4, I_2};
   const std::vector<unsigned int> index_scalar;
   const In pos_tensor = 42;

   // Make a reference SimpleTensor from an array pointed to by unique_ptr.
   std::unique_ptr<T[]> ptr(new T[size_tensor]);
   for(In i = I_0; i < size_tensor; ++i)
   {
      ptr[i] = static_cast<T>(C_0);
   }
   ptr[pos_tensor] = static_cast<T>(C_1);
   SimpleTensor<T>* const st_ref = new SimpleTensor<T>(dims_tensor, ptr.release());

   // ... and a NiceTensor to wrap it.
   const NiceTensor<T> nt_ref_original_tensor(st_ref);
   const NiceTensor<T> nt_ref_original_scalar(new Scalar<T>(static_cast<T>(C_1)));

   // Vector of pairs (typeID, type string), for looping.
   const std::vector<std::pair<typename BaseTensor<T>::typeID, std::string> > type_id{
      {BaseTensor<T>::typeID::SIMPLE,    "SimpleTensor"},
      {BaseTensor<T>::typeID::CANONICAL, "CanonicalTensor"},
      {BaseTensor<T>::typeID::SCALAR,    "Scalar"},
   };

   // We test that
   //    -  mTensor of the NiceTensor is not a nullptr.
   //    -  type is correct.
   //    -  norm is one.
   //    -  diff. norm with the SimpleTensor reference is zero.
   //    -  in the case of CanonicalTensor, that rank is 1.
   for(auto it = type_id.begin(); it != type_id.end(); ++it)
   {
      // Set arguments depending on whether it's a Scalar/non-Scalar.
      std::vector<unsigned int> dims;
      std::vector<unsigned int> index;
      NiceTensor<T> nt_ref_original;
      if(it->first == BaseTensor<T>::typeID::SCALAR)
      {
         dims  = dims_scalar;
         index = index_scalar;
         nt_ref_original = nt_ref_original_scalar;
      }
      else
      {
         dims  = dims_tensor;
         index = index_tensor;
         nt_ref_original = nt_ref_original_tensor;
      }

      // The constructor should throw a MIDASERROR if type doesn't match. 
      const NiceTensor<T> nt(dims, index, it->first);

      // Difference tensor with the SimpleTensor reference.
      NiceTensor<T> nt_ref(nt_ref_original);
      nt_ref.Axpy(nt, static_cast<T>(-C_1));

      // Checks.
      UNIT_ASSERT(!nt.IsNullPtr(), "NiceTensor("+it->second+") contains nullptr.");
      UNIT_ASSERT(nt.Type() == it->first, "NiceTensor("+it->second+"); wrong type (enum is "+std::to_string(static_cast<In>(nt.Type()))+", should have been "+std::to_string(static_cast<In>(it->first))+").");
      UNIT_ASSERT_FEQUAL(nt.Norm(), static_cast<T>(C_1), "NiceTensor("+it->second+"); norm isn't 1.");
      UNIT_ASSERT_FEQUAL(nt_ref.Norm(), static_cast<T>(C_0), "NiceTensor("+it->second+"); non-zero diff. norm with reference.");

      if(it->first == BaseTensor<T>::typeID::CANONICAL)
      {
         CanonicalTensor<T>* ct_ptr = dynamic_cast<CanonicalTensor<T>*>(nt.GetTensor());
         UNIT_ASSERT(ct_ptr != nullptr, "Couldn't dyn.cast. to NiceTensor("+it->second+").GetTensor() to CanonicalTensor*.");
         UNIT_ASSERT_EQUAL(static_cast<unsigned int>(ct_ptr->GetRank()), static_cast<unsigned int>(I_1), "NiceTensor("+it->second+"); rank not 1.");
      }
   }
};
