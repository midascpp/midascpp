/**
 *******************************************************************************
 * 
 * @file    ModeCombiOpRangeTest.cc
 * @date    14-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for the ModeCombiOpRange class and related functions.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/ModeCombiOpRangeTest.h"
#include "util/FileSystem.h"

namespace midas::test
{
   namespace modecombioprange::detail
   {
      /*********************************************************************//**
       * @param[in] arMc
       *    ModeCombi, whose mode numbers are checked.
       * @param[in] arCtrl
       *    Control vector containing the expected mode numbers.
       * @return
       *    `.first`: whether mode numbers are the same.
       *    `.second`: error message if unequal.
       ************************************************************************/
      std::pair<bool, std::string> CheckModeCombi
         (  const ModeCombi& arMc
         ,  const std::vector<In>& arCtrl
         )
      {
         bool equal = true;
         if (arMc.Size() != arCtrl.size())
         {
            equal = false;
         }
         else
         {
            for(Uin i = I_0; i < arCtrl.size(); ++i)
            {
               if (arMc.Mode(i) != arCtrl[i])
               {
                  equal = false;
                  break;
               }
            }
         }

         std::stringstream err;
         if (!equal)
         {
            err << "{ ";
            for(Uin i = I_0; i < arMc.Size(); ++i)
            {
               err << arMc.Mode(i) << ' ';
            }
            err << "} (MC) != { ";
            for(const auto& m: arCtrl)
            {
               err << m << ' ';
            }
            err << "} (ctrl)";
         }
         return std::make_pair(equal, err.str());
      }

      /*********************************************************************//**
       * Only checks the mode numbers of the ModeCombi%s, that
       * ModeCombiOpRange::Size() is correct, and that iterators work
       * correctly.
       *
       * @param[in] arMcr
       *    The ModeCombiOpRange to check.
       * @param[in] arCtrl
       *    The control, whose `std::vector<In>`s is supposed to be equivalent
       *    (i.e. have same mode numbers) to the ModeCombi%s of arMcr.
       * @return
       *    `.first`: true if all ModeCombi%s are equivalent to the control
       *    `.second`: error message in case of failure, otherwise empty string
       ************************************************************************/
      std::pair<bool, std::string> CheckModeCombis
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<std::vector<In>>& arCtrl
         )
      {
         auto assert_get = CheckModeCombisGet(arMcr, arCtrl);
         auto assert_iter = CheckModeCombisIterators(arMcr, arCtrl);
         if (!assert_get.first || !assert_iter.first)
         {
            std::stringstream err;
            if (!assert_get.first)  err << "\n" << assert_get.second  << ", using GetModeCombi.";
            if (!assert_iter.first) err << "\n" << assert_iter.second << ", using iterators.";
            return std::make_pair(false, err.str());
         }
         else
         {
            return std::make_pair(true, "");
         }
      }

      /*********************************************************************//**
       * Arguments same as for CheckModeCombis().
       * Uses ModeCombiOpRange::Size() and
       * ModeCombiOpRange::GetModeCombi() for checking.
       ************************************************************************/
      std::pair<bool, std::string> CheckModeCombisGet
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<std::vector<In>>& arCtrl
         )
      {
         if (arMcr.Size() != arCtrl.size())
         {
            std::stringstream err;
            err << "MCR size (" << arMcr.Size() << ") != ctrl size (" << arCtrl.size() << ")";
            return std::make_pair(false, err.str());
         }
         else
         {
            for(Uin i = I_0; i < arMcr.Size(); ++i)
            {
               auto assert = CheckModeCombi(arMcr.GetModeCombi(i), arCtrl[i]);
               if (!assert.first)
               {
                  return std::make_pair(false, assert.second+" (i = "+std::to_string(i)+")");
               }
            }
            return std::make_pair(true, "");
         }
      }

      /*********************************************************************//**
       * @brief
       *    Helper implementation for CheckModeCombisIterators().
       ************************************************************************/
      template<class McrIter, class VecIter>
      std::pair<bool, std::string> CheckModeCombisIteratorsImpl
         (  McrIter&& arMcrBeg
         ,  const McrIter& arMcrEnd
         ,  VecIter&& arCtrlBeg
         ,  const VecIter& arCtrlEnd
         )
      {
         Uin i = I_0;
         for(
            ;  arMcrBeg != arMcrEnd && arCtrlBeg != arCtrlEnd
            ;  ++arMcrBeg, ++arCtrlBeg, ++i
            )
         {
            auto assert = CheckModeCombi(*arMcrBeg, *arCtrlBeg);
            if (!assert.first)
            {
               return std::make_pair(false, assert.second+" (i = "+std::to_string(i)+")");
            }
         }
         if (arMcrBeg != arMcrEnd)
         {
            return std::make_pair(false, "MCR size > ctrl size (which is "+std::to_string(i)+")");
         }
         else if (arCtrlBeg != arCtrlEnd)
         {
            return std::make_pair(false, "ctrl size > MCR size (which is "+std::to_string(i)+")");
         }
         else
         {
            return std::make_pair(true, "");
         }
      }

      /*********************************************************************//**
       * Arguments same as for CheckModeCombis().
       * Uses all four types of iterator calls; begin(), cbegin(), rbegin() and
       * crbegin() (and similar for end()).
       ************************************************************************/
      std::pair<bool, std::string> CheckModeCombisIterators
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<std::vector<In>>& arCtrl
         )
      {
         // begin()/end()
         auto assert = CheckModeCombisIteratorsImpl(arMcr.begin(), arMcr.end(), arCtrl.begin(), arCtrl.end());
         if (!assert.first)
         {
            return std::make_pair(false, assert.second+" (begin/end)");
         }
         // cbegin()/cend()
         assert = CheckModeCombisIteratorsImpl(arMcr.cbegin(), arMcr.cend(), arCtrl.cbegin(), arCtrl.cend());
         if (!assert.first)
         {
            return std::make_pair(false, assert.second+" (cbegin/cend)");
         }
         // rbegin()/rend()
         assert = CheckModeCombisIteratorsImpl(arMcr.rbegin(), arMcr.rend(), arCtrl.rbegin(), arCtrl.rend());
         if (!assert.first)
         {
            return std::make_pair(false, assert.second+" (rbegin/rend)");
         }
         // crbegin()/crend()
         assert = CheckModeCombisIteratorsImpl(arMcr.crbegin(), arMcr.crend(), arCtrl.crbegin(), arCtrl.crend());
         if (!assert.first)
         {
            return std::make_pair(false, assert.second+" (crbegin/crend)");
         }
         
         // Success if getting to here.
         return std::make_pair(true, "");
      }

      /*********************************************************************//**
       * 
       ************************************************************************/
      std::pair<bool, std::string> CheckNumWithExcLevel
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<Uin>& arCtrl
         )
      {
         for(Uin i = I_0; i < arCtrl.size(); ++i)
         {
            if (arMcr.NumModeCombisWithExcLevel(i) != arCtrl.at(i))
            {
               std::stringstream err;
               err   << "NumModeCombisWithExcLevel(" << i 
                     << ") (" << arMcr.NumModeCombisWithExcLevel(i)
                     << ") != ctrl (" << arCtrl.at(i)
                     << ")."
                     ;
               return std::make_pair(false, err.str());
            }
         }
         return std::make_pair(true, "");
      }

      std::pair<bool, std::string> CheckReducedSizes
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<Uin>& arCtrl
         )
      {
         // Lambda to do the summations of arCtrl.
         auto redsize = [&arCtrl](Uin aMax, Uin aMin) -> Uin
            {
               Uin s = I_0;
               for(Uin i = aMin; i <= aMax; ++i)
               {
                  s += arCtrl.at(i);
               }
               return s;
            };
         // Loop over all possible reduced sizes (yes, some of them should be
         // zero due to max < min; feature).
         for(Uin max = I_0; max < arCtrl.size(); ++max)
         {
            for(Uin min = I_0; min < arCtrl.size(); ++min)
            {
               if (arMcr.ReducedSize(max, min) != redsize(max, min))
               {
                  std::stringstream err;
                  err   << "ReducedSize(" << max << ", " << min
                        << ") (" << arMcr.ReducedSize(max, min)
                        << ") != ctrl (" << redsize(max, min)
                        << ")."
                        ;
                  return std::make_pair(false, err.str());
               }
            }
         }
         return std::make_pair(true, "");
      }

      /*********************************************************************//**
       * 
       ************************************************************************/
      std::pair<bool, std::string> CheckRefNumbers
         (  const ModeCombiOpRange& arMcr
         )
      {
         Uin n = I_0;
         for(auto it = arMcr.begin(), end = arMcr.end(); it != end; ++it, ++n)
         {
            if (it->ModeCombiRefNr() != n)
            {
               std::stringstream err;
               err   << "Wrong ModeCombiRefNr(); got " << it->ModeCombiRefNr()
                     << ", excpected " << n
                     ;
               return std::make_pair(false, err.str());
            }
         }
         return std::make_pair(true, "");
      }

      /*********************************************************************//**
       * 
       ************************************************************************/
      std::pair<bool, std::string> CheckAddresses
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<In>& arAddresses
         )
      {
         if (arMcr.Size() != arAddresses.size())
         {
            std::stringstream err;
            err   << "arMcr.Size() (which is " << arMcr.Size()
                  << ") != arAddresses.size() (which is " << arAddresses.size()
                  << ")"
                  ;
            return std::make_pair(false, err.str());
         }
         else
         {
            for(Uin i = I_0; i < arMcr.Size(); ++i)
            {
               const auto& a_mcr  = arMcr.GetModeCombi(i).Address();
               const auto& a_ctrl = arAddresses.at(i);
               if (a_mcr != a_ctrl)
               {
                  std::stringstream err;
                  err   << "GetModeCombi("<<i<<").Address() (which is " << a_mcr
                        << ") != ctrl address (which is " << a_ctrl
                        << ")"
                        ;
                  return std::make_pair(false, err.str());
               }
            }
            return std::make_pair(true, "");
         }
      }

      /*********************************************************************//**
       * 
       ************************************************************************/
      std::pair<bool, std::string> CheckQueries
         (  const ModeCombiOpRange& arMcr
         ,  const std::vector<std::vector<In>>& arCtrl
         )
      {
         // Deduce the expected query values from arCtrl.
         Uin max_exci_level = I_0;
         std::vector<Uin> num_exci_level;
         std::set<In> modes;
         for(const auto& mc: arCtrl)
         {
            max_exci_level = std::max(max_exci_level, static_cast<Uin>(mc.size()));
            if (mc.size() >= num_exci_level.size())
            {
               num_exci_level.resize(mc.size()+1, 0);
            }
            ++num_exci_level.at(mc.size());
            for(const auto& m: mc)
            {
               modes.insert(m);
            }
         }
         Uin num_empty_mcs = num_exci_level.empty()? 0: num_exci_level.at(0);
         std::vector<In> v_modes(modes.begin(), modes.end());

         std::vector<std::pair<bool, std::string>> v_errors;
         v_errors.push_back(CheckModeCombis(arMcr, arCtrl));
         if (arMcr.GetMaxExciLevel() != max_exci_level)
         {
            std::stringstream err;
            err << "GetMaxExciLevel() ("<<arMcr.GetMaxExciLevel()<<") != ctrl ("<<max_exci_level<<")";
            v_errors.emplace_back(false, err.str());
         }
         if (arMcr.NumEmptyMCs() != num_empty_mcs)
         {
            std::stringstream err;
            err << "NumEmptyMCs() ("<<arMcr.NumEmptyMCs()<<") != ctrl ("<<num_empty_mcs<<")";
            v_errors.emplace_back(false, err.str());
         }
         if (arMcr.NumberOfModes() != v_modes.size())
         {
            std::stringstream err;
            err << "NumberOfModes() ("<<arMcr.NumberOfModes()<<") != ctrl ("<<v_modes.size()<<")";
            v_errors.emplace_back(false, err.str());
         }
         if (arMcr.ModesContained() != v_modes)
         {
            std::stringstream err;
            err << "ModesContained() ("<<arMcr.ModesContained()<<") != ctrl ("<<v_modes<<")";
            v_errors.emplace_back(false, err.str());
         }
         if (!v_modes.empty())
         {
            In smallest_mode = v_modes.front();
            In largest_mode  = v_modes.back();
            if (arMcr.SmallestMode() != smallest_mode)
            {
               std::stringstream err;
               err << "SmallestMode() ("<<arMcr.SmallestMode()<<") != ctrl ("<<smallest_mode<<")";
               v_errors.emplace_back(false, err.str());
            }
            if (arMcr.LargestMode() != largest_mode)
            {
               std::stringstream err;
               err << "LargestMode() ("<<arMcr.LargestMode()<<") != ctrl ("<<largest_mode<<")";
               v_errors.emplace_back(false, err.str());
            }
         }

         v_errors.push_back(CheckNumWithExcLevel(arMcr, num_exci_level));
         v_errors.push_back(CheckReducedSizes(arMcr, num_exci_level));
         v_errors.push_back(CheckRefNumbers(arMcr));

         bool success = true;
         std::stringstream err;
         for(const auto& e: v_errors)
         {
            if (!e.first)
            {
               success = false;
               err << "\n" << e.second;
            }
         }
         return std::make_pair(success, err.str());
      }

      /*********************************************************************//**
       *
       ************************************************************************/
      std::pair<bool, std::string> CheckPartitionings
         (  const std::vector<std::vector<ModeCombi>>& arVecVecMcs
         ,  const std::vector<std::vector<std::vector<In>>>& arCtrlMcs
         ,  const std::vector<std::vector<In>>& arCtrlAddresses
         ,  const std::vector<std::vector<In>>& arCtrlRefNums
         )
      {
         if (  arCtrlAddresses.size() != arCtrlMcs.size()
            || arCtrlRefNums.size() != arCtrlMcs.size()
            )
         {
            return std::make_pair(false, "Control MCs, addresses, refnums, size mismatch.");
         }
         else
         {
            if (arVecVecMcs.size() != arCtrlMcs.size())
            {
               std::stringstream err;
               err   << "arVecVecMcs.size() (which is " << arVecVecMcs.size()
                     << ") != arCtrlMcs.size() (which is " << arCtrlMcs.size() << ")"
                     ;
               return std::make_pair(false, err.str());
            }
            else
            {
               std::vector<std::pair<bool, std::string>> v_errors;
               for(Uin i = I_0; i < arVecVecMcs.size(); ++i)
               {
                  std::string err_part = "Partitioning i = "+std::to_string(i);
                  ModeCombiOpRange tmp_mcr;
                  const auto& vec_mcs = arVecVecMcs.at(i);
                  tmp_mcr.Insert(vec_mcs);
                  // Check the ModeCombis, i.e. the mode numbers.
                  auto assert = detail::CheckQueries(tmp_mcr, arCtrlMcs.at(i));
                  v_errors.emplace_back(assert.first, err_part+": "+assert.second);

                  // Check the addresses.
                  const auto& vec_addresses = arCtrlAddresses.at(i);
                  if (vec_mcs.size() != vec_addresses.size())
                  {
                     std::stringstream err;
                     err   << err_part << ": Num MCs (which is " << vec_mcs.size()
                           << ") != num ctrl addresses (which is " << vec_addresses.size()
                           << ")"
                           ;
                     v_errors.emplace_back(false, err.str());
                  }
                  else
                  {
                     for(Uin p = I_0; p < vec_mcs.size(); ++p)
                     {
                        if (vec_mcs.at(p).Address() != vec_addresses.at(p))
                        {
                           std::stringstream err;
                           err   << err_part << ": Address of ModeCombi p = " << p
                                 << ", got " << vec_mcs.at(p).Address()
                                 << ", expected " << vec_addresses.at(p)
                                 ;
                           v_errors.emplace_back(false, err.str());
                        }
                     }
                  }

                  // Check the ref nums.
                  const auto& vec_refnums = arCtrlRefNums.at(i);
                  if (vec_mcs.size() != vec_refnums.size())
                  {
                     std::stringstream err;
                     err   << err_part << ": Num MCs (which is " << vec_mcs.size()
                           << ") != num ctrl refnums (which is " << vec_refnums.size()
                           << ")"
                           ;
                     v_errors.emplace_back(false, err.str());
                  }
                  else
                  {
                     for(Uin p = I_0; p < vec_mcs.size(); ++p)
                     {
                        if (vec_mcs.at(p).ModeCombiRefNr() != vec_refnums.at(p))
                        {
                           std::stringstream err;
                           err   << err_part << ": ModeCombiRefNr of ModeCombi p = " << p
                                 << ", got " << vec_mcs.at(p).ModeCombiRefNr()
                                 << ", expected " << vec_refnums.at(p)
                                 ;
                           v_errors.emplace_back(false, err.str());
                        }
                     }
                  }
               }

               // Final accumulation.
               bool success = true;
               std::stringstream err;
               for(const auto& e: v_errors)
               {
                  if (!e.first)
                  {
                     success = false;
                     err << "\n" << e.second;
                  }
               }
               return std::make_pair(success, err.str());
            }
         }
      }

      /************************************************************************//**
       *
       ***************************************************************************/
      void RemoveFile
         (  const std::string& arFile
         )
      {
         if (midas::filesystem::IsFile(arFile))
         {
            auto ret_val = midas::filesystem::Remove(arFile);
            if (ret_val != 0)
            {
               MIDASERROR
                  (  "Failure when removing "+arFile
                  +  " (return value: "+std::to_string(ret_val)+")"
                  );
            }
         }
      }

      /************************************************************************//**
       *
       ***************************************************************************/
      midas::mpi::OFileStream& ConstructorFlexCouplingsBase::WriteModeCombi
         (  midas::mpi::OFileStream& arOs
         ,  const std::vector<In>& arMc
         )
      {
         for(auto it = arMc.begin(), end = arMc.end(); it != end; )
         {
            arOs << *it;
            if (++it != end)
            {
               arOs << ',';
            }
         }
         return arOs;
      }

      /************************************************************************//**
       *
       ***************************************************************************/
      void ConstructorFlexCouplingsBase::WriteFile
         (  const std::string& arFile
         ,  const contents_t& arConts
         )
      {
         // The ModeCombiOpRange implementation only reads in from master, so
         // only let master write (StreamType::MPI_MASTER).
         midas::mpi::OFileStream ofs(arFile, midas::mpi::OFileStream::StreamType::MPI_MASTER);
         if (ofs)
         {
            ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            ofs << std::scientific;
            ofs.precision(16);
            try
            {
               for(const auto& line: arConts)
               {
                  WriteModeCombi(ofs, std::get<0>(line)) << ' ' << std::get<1>(line) << '\n';
               }
               ofs.close();
            }
            catch(const std::ofstream::failure& e)
            {
               MIDASERROR("Failure when writing to/closing " + arFile + ": " + e.what());
            }
         }
         else
         {
            MIDASERROR("Couldn't open for writing: " + arFile);
         }
      }

   } /* namespace modecombioprange::detail */

   /************************************************************************//**
    *
    ***************************************************************************/
   void modecombioprange::TestUpdateAdga::WriteVecOfStrings
      (  const std::string& arFile
      ,  const std::vector<std::string>& arVs
      )
   {
      // The ModeCombiOpRange implementation only reads in from master, so
      // only let master write (StreamType::MPI_MASTER).
      midas::mpi::OFileStream ofs(arFile, midas::mpi::OFileStream::StreamType::MPI_MASTER);
      if (ofs)
      {
         ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);
         try
         {
            for(const auto& line: arVs)
            {
               ofs << line << '\n';
            }
            ofs.close();
         }
         catch(const std::ofstream::failure& e)
         {
            MIDASERROR("Failure when writing to/closing " + arFile + ": " +e.what());
         }
      }
      else
      {
         MIDASERROR("Couldn't open for writing: "+arFile);
      }
   }




   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void ModeCombiOpRangeTest()
   {
      // Create test_suite object.
      cutee::suite suite("ModeCombiOpRange test");

      // Add all the tests to the suite.
      using namespace modecombioprange;
      suite.add_test<ConstructorDefault>("Constructor, default (empty range)");
      suite.add_test<ConstructorEmptyModeCombi>("Constructor, the empty ModeCombi");
      suite.add_test<ConstructorMaxExciLevel>("Constructor, max. exci. level");
      suite.add_test<ConstructorMaxExciLevelLTNumModes>("Constructor, max. exci. level > num modes");
      suite.add_test<ConstructorMaxExPerMode>("Constructor, max. exc. per mode");
      suite.add_test<ConstructorGroupCouplings>("Constructor, GroupCouplings");
      suite.add_test<ConstructorFlexCouplingsA>("Constructor, FlexCouplings A");
      suite.add_test<ConstructorFlexCouplingsB>("Constructor, FlexCouplings B");
      suite.add_test<ConstructorEffectiveExciCount>("Constructor, effective exci. count");
      suite.add_test<ConstructorWrapperExtendedRangeModes>("Constructor wrapper, extended range modes");
      suite.add_test<InsertSortedIntoEmptyRange>("Insert, sorted into empty range");
      suite.add_test<InsertUnsortedIntoNonEmptyRange>("Insert, unsorted into nonempty range");
      suite.add_test<InsertIdenticalModeCombis>("Insert, several identical ModeCombis");
      suite.add_test<InsertCheckExistingRemains>("Insert, check existing ModeCombi remains in range");
      suite.add_test<EraseSortedModeCombis>("Erase, sorted ModeCombis to erase");
      suite.add_test<EraseUnsortedModeCombis>("Erase, unsorted ModeCombis to erase");
      suite.add_test<Clear>("Clear");
      suite.add_test<Find>("Find");
      suite.add_test<Contains>("Contains");
      suite.add_test<AssignAddress>("AssignAddress");
      suite.add_test<SetAddressesTest>("SetAddresses");
      suite.add_test<SpecificLevelIterators>("Begin(Uin)/End(Uin) iterators");
      suite.add_test<IfContainedAssignAddressAndRefNum>("IfContainedAssignAddressAndRefNum");
      suite.add_test<TestModeCombisFixedNonFixedModes>("ModeCombisFixedNonFixedModes");
      suite.add_test<TestDoesMcrConnectMcs>("DoesMcrConnectMcs");
      suite.add_test<TestBuildCommonMcr>("BuildCommonMcr");
      suite.add_test<TestSetToLeftOperRange>("SetToLeftOperRange");
      suite.add_test<TestSubSetSets>("SubSetSets");
      suite.add_test<TestUpdateAdga>("UpdateAdga");
      suite.add_test<TestSizeOut>("SizeOut");
      suite.add_test<OperatorOstream>("operator<<");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
