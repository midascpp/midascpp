/**
************************************************************************
* 
* @file                SinglePointTest.h
*
* Created:             02-08-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   
                       
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SINGLEPOINTTEST_H_INCLUDED
#define SINGLEPOINTTEST_H_INCLUDED

// std headers
#include <memory>

// midas headers
#include "test/CuteeInterface.h"
#include "util/MidasSystemCaller.h"
#include "util/Os.h"
#include "pes/PesInfo.h"
#include "pes/PropertyInfo.h"
#include "pes/CalcCode.h"
#include "pes/singlepoint/SinglePoint.h"
#include "pes/singlepoint/GenericSinglePoint.h"
#include "pes/singlepoint/DaltonSinglePoint.h"

namespace midas::test
{
#define _ARG3(_0,_1,_2,_3,...) _3
// macro that will counter number of arguments up to three
#define NARG3(...) _ARG3(__VA_ARGS__,3,2,1,0)

// macro for expanding statements
#define EXPAND_INFO_0()
#define EXPAND_INFO_1(_1) _1;
#define EXPAND_INFO_2(_1, _2) _1; _2;
#define EXPAND_INFO_3(_1, _2, _3) _1; _2; _3;
#define EXPAND_INFO__(N,...) EXPAND_INFO_ ## N (__VA_ARGS__)
#define EXPAND_INFO_(N,...) EXPAND_INFO__(N,__VA_ARGS__)
#define EXPAND_INFO(...) EXPAND_INFO_(NARG3(__VA_ARGS__),__VA_ARGS__)

////
// macro for creating tests
////
#define CREATE_SINGLEPOINT_CONSTRUCTION_TEST(NAME,STRING,...) \
/** 
 * SinglePointConstructionTest - tests whether we can construct a singlepoint of the given type
 **/ \
struct NAME##SinglePointConstructionTest : public cutee::test \
{ \
   std::string m_savedir_save = ""; \
   \
   void setup() \
   { \
      m_savedir_save = gSaveDir; /* save global savedir, so we can reset after test has been run */ \
      gSaveDir = os::Getcwd(); /* set global save dir to current working directory */  \
      system_command_t cmd = {"touch","DUMMY"}; \
      MIDASSYSTEM(cmd); /* create file 'DUMMY' */\
   } \
   \
   void run()  \
   { \
      SinglePointInfo sp_info; \
      sp_info["PROGRAM"] = "SP_" #STRING; \
      EXPAND_INFO(__VA_ARGS__)\
      pes::PropertyInfo property_info;\
      std::unique_ptr<SinglePointImpl> sp = SinglePointImpl::Factory(sp_info, property_info); /* try to create singlepoint */ \
      \
      UNIT_ASSERT(bool(dynamic_cast<NAME##SinglePoint*>(sp.get())),"Could not construct "  #NAME); /* check that we created the correct singleploint */ \
   } \
   \
   void teardown() \
   { \
      gSaveDir = m_savedir_save; /* reset savedir to what is was before test */ \
      system_command_t cmd = {"rm","-rf","DUMMY"}; \
      MIDASSYSTEM(cmd); /* remove file DUMMY */ \
   } \
};

CREATE_SINGLEPOINT_CONSTRUCTION_TEST(Generic
                                   , GENERIC
                                   , sp_info["INPUTCREATORSCRIPT"] = "DUMMY"
                                   , sp_info["RUNSCRIPT"] = "DUMMY"
                                   )

CREATE_SINGLEPOINT_CONSTRUCTION_TEST(Dalton
                                   , DALTON
                                   )

#undef CREATE_SINGLEPOINT_CONSTRUCTION_TEST // undefine macro so it cannot be (mis-)used elsewhere

} /* namespace midas::test */

#endif /* SINGLEPOINTTEST_H_INCLUDED */
