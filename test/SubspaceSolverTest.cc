/**
 *******************************************************************************
 * 
 * @file    SubspaceSolverTest.cc
 * @date    12-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "SubspaceSolverTest.h"
#include "it_solver/nl_solver/SubspaceSolver.h"

namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void SubspaceSolverTest()
   {
      // Create test_suite object.
      cutee::suite suite("SubspaceSolver test");

      // Solver tests.
      suite.add_test<LinearNoPreconSubspaceSolverTest<midas::nlsolver::CropSolver, 3>>("CROP(3) (lin. eqs. no preconditioner)");
      suite.add_test<QuasiLinearNoPreconSubspaceSolverTest<midas::nlsolver::CropSolver, 3>>("CROP(3) (quasi-linear equations, no preconditioner)");
      suite.add_test<NoPreconSubspaceSolverTest<midas::nlsolver::DiisSolver, 3>>("DIIS(3) (no preconditioner)");
      suite.add_test<LinearNoPreconSubspaceSolverTest<midas::nlsolver::DiisSolver, 3>>("DIIS(3) (lin. eqs. no preconditioner)");
      suite.add_test<QuasiLinearNoPreconSubspaceSolverTest<midas::nlsolver::DiisSolver, 3>>("DIIS(3) (quasi-linear equations, no preconditioner)");

      suite.add_test<LinearDiagPreconSubspaceSolverTest<midas::nlsolver::DiagPreconCropSolver, 3>>("CROP(3) (lin. eqs. diagonal preconditioner)");
      suite.add_test<DiagPreconSubspaceSolverTest<midas::nlsolver::DiagPreconDiisSolver, 3>>("DIIS(3) (diagonal preconditioner)");
      suite.add_test<LinearDiagPreconSubspaceSolverTest<midas::nlsolver::DiagPreconDiisSolver, 3>>("DIIS(3) (lin. eqs. diagonal preconditioner)");
      suite.add_test<QuasiLinearDiagPreconSubspaceSolverTest<midas::nlsolver::DiagPreconDiisSolver, 3>>("DIIS(3) (quasi-linear equations, diagonal preconditioner)");

      // Run the tests.
      RunSuite(suite);
   }

} /* namespace midas::test */

