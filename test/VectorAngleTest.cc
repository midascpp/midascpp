/**
 *******************************************************************************
 * 
 * @file    VectorAngleTest.cc
 * @date    18-03-2020 
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/VectorAngleTest.h"
#include "util/type_traits/Complex.h"
#include "util/type_traits/TypeName.h"
#include "mmv/DataCont.h"

namespace midas::test
{
   namespace vectorangle::detail
   {
      /*********************************************************************//**
       * 
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      void AddVectorAngleTests
         (  cutee::suite& arSuite
         )
      {
         using midas::type_traits::TypeName;
         std::string s = "("+TypeName<T>()+","+TypeName<CONT_T>()+") ";
         arSuite.add_test<VectorAngleTestEasy<T,CONT_T>>(s+"VectorAngleTestEasy");
         arSuite.add_test<VectorAngleTestOrthg<T,CONT_T>>(s+"VectorAngleTestOrthg");
         arSuite.add_test<VectorAngleTestParallel<T,CONT_T>>(s+"VectorAngleTestParallel");
         arSuite.add_test<VectorAngleTestZeroVec<T,CONT_T>>(s+"VectorAngleTestZeroVec");
         arSuite.add_test<VectorAngleTestNearParallelA<T,CONT_T>>(s+"VectorAngleTestNearParallelA");
         arSuite.add_test<VectorAngleTestNearParallelB<T,CONT_T>>(s+"VectorAngleTestNearParallelB");
         arSuite.add_test<VectorAngleTestRange<T,CONT_T>>(s+"VectorAngleTestRange");
         arSuite.add_test<VectorAngleTestConj<T,CONT_T>>(s+"VectorAngleTestConj");
         arSuite.add_test<VectorAngleTestSlice<T,CONT_T>>(s+"VectorAngleTestSlice");
      }

   } /* namespace vectorangle::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void VectorAngleTest()
   {
      // Create test_suite object.
      cutee::suite suite("VectorAngle test");

      // Add all the tests to the suite.
      using vectorangle::detail::AddVectorAngleTests;
      AddVectorAngleTests<float,GeneralMidasVector>(suite);
      AddVectorAngleTests<float,GeneralDataCont>(suite);
      AddVectorAngleTests<double,GeneralMidasVector>(suite);
      AddVectorAngleTests<double,GeneralDataCont>(suite);
      AddVectorAngleTests<std::complex<float>,GeneralMidasVector>(suite);
      AddVectorAngleTests<std::complex<float>,GeneralDataCont>(suite);
      AddVectorAngleTests<std::complex<double>,GeneralMidasVector>(suite);
      AddVectorAngleTests<std::complex<double>,GeneralDataCont>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
