/**
 *******************************************************************************
 * 
 * @file    ContainerMathWrappersTest.h
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef CONTAINERMATHWRAPPERSTEST_H_INCLUDED
#define CONTAINERMATHWRAPPERSTEST_H_INCLUDED

#include "test/CuteeInterface.h"
#include "util/type_traits/Complex.h"
#include "mmv/ContainerMathWrappers.h"
#include "test/Random.h"

#include "mmv/MidasVector.h"

#include <vector>
#include <array>
namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    Tests WrapNorm and WrapNorm2 for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapNormArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a;
         absval_t ctrl_norm2 = 0;
         for(auto& v: a)
         {
            v = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            ctrl_norm2 += Norm2(v);
         }
         absval_t ctrl_norm = sqrt(ctrl_norm2);

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm2(a), ctrl_norm2, "Bad result, WrapNorm2.");
         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), ctrl_norm, "Bad result, WrapNorm.");
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapNorm and WrapNorm2 for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapNormContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a;
         absval_t ctrl_norm2 = 0;
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            ctrl_norm2 += Norm2(a.back());
         }
         absval_t ctrl_norm = sqrt(ctrl_norm2);

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm2(a), ctrl_norm2, "Bad result, WrapNorm2.");
         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), ctrl_norm, "Bad result, WrapNorm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests WrapZero for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapZeroArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a;
         absval_t ctrl_norm2 = 0;
         for(auto& v: a)
         {
            v = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            midas::mmv::WrapZero(v);
            ctrl_norm2 += Norm2(v);
         }

         UNIT_ASSERT_FZERO(ctrl_norm2, absval_t(1.0), "Bad result, WrapZero.");
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapZero for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapZeroContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a;
         absval_t ctrl_norm2 = 0;
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            midas::mmv::WrapZero(a.back());
            ctrl_norm2 += Norm2(a.back());
         }

         UNIT_ASSERT_FZERO(ctrl_norm2, absval_t(1.0), "Bad result, WrapZero.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests WrapScale for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapScaleArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a;
         absval_t ctrl_norm2 = 0;
         const auto scalar = random::RandomNumber<absval_t>();
         for(auto& v: a)
         {
            v = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            ctrl_norm2 += Norm2(v);
            midas::mmv::WrapScale(v, scalar);
         }

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), std::sqrt(ctrl_norm2)*std::abs(scalar), "Bad result, WrapScale.");
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapScale for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapScaleContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a;
         absval_t ctrl_norm2 = 0;
         const auto scalar = random::RandomNumber<absval_t>();
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            ctrl_norm2 += Norm2(a.back());
            midas::mmv::WrapScale(a.back(), scalar);
         }

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), std::sqrt(ctrl_norm2)*std::abs(scalar), "Bad result, WrapScale.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests WrapDotProduct for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapDotProductArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a, b;
         T ctrl_dot = 0;
         for(In i=I_0; i<D; ++i)
         {
            a[i] = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            b[i] = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            ctrl_dot += Dot(a[i], b[i]);
         }

         auto d = midas::mmv::WrapDotProduct(a,b);

         UNIT_ASSERT_FEQUAL(std::real(d), std::real(ctrl_dot), "Bad result, WrapDotProduct (real part).");
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            UNIT_ASSERT_FEQUAL(std::imag(d), std::imag(ctrl_dot), "Bad result, WrapDotProduct (imag part).");
         }
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapDotProduct for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapDotProductContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a, b;
         T ctrl_dot = 0;
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            b.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            ctrl_dot += Dot(a.back(), b.back());
         }

         auto d = midas::mmv::WrapDotProduct(a,b);

         UNIT_ASSERT_FEQUAL(std::real(d), std::real(ctrl_dot), "Bad result, WrapDotProduct (real part).");
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            UNIT_ASSERT_FEQUAL(std::imag(d), std::imag(ctrl_dot), "Bad result, WrapDotProduct (imag part).");
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests WrapAxpy for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapAxpyArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a;
         for(auto& v : a)
         {
            v = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            midas::mmv::WrapAxpy(v, v, absval_t(-1.0));
         }

         UNIT_ASSERT_FZERO(midas::mmv::WrapNorm(a), absval_t(1.0), "Bad result, WrapAxpy.");
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapAxpy for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapAxpyContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a;
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            midas::mmv::WrapAxpy(a.back(), a.back(), absval_t(-1.0));
         }

         UNIT_ASSERT_FZERO(midas::mmv::WrapNorm(a), absval_t(1.0), "Bad result, WrapAxpy.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests WrapHadamardProduct for std::array.
    ***************************************************************************/
   template
      <  typename T
      ,  Uin D
      >
   struct WrapHadamardProductArray
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using arr_t = std::array<GeneralMidasVector<param_t>,D>;

      void run() override
      {
         arr_t a, b;
         absval_t ctrl_norm2 = 0;
         for(In i=I_0; i<D; ++i)
         {
            a[i] = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            b[i] = random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5);
            auto c = a[i];
            midas::mmv::WrapHadamardProduct(c, b[i]);
            ctrl_norm2 += midas::mmv::WrapNorm2(c);
         }

         midas::mmv::WrapHadamardProduct(a, b);

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), std::sqrt(ctrl_norm2), "Bad result, WrapHadamardProduct.");
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Tests WrapHadamardProduct for container
    ***************************************************************************/
   template
      <  typename T
      ,  template<typename...> typename CONT_TMPL
      ,  Uin D
      >
   struct WrapHadamardProductContainer
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<param_t>;
      using cont_t = CONT_TMPL<GeneralMidasVector<T>>;

      void run() override
      {
         cont_t a, b;
         absval_t ctrl_norm2 = 0;
         for(In i=I_0; i<D; ++i)
         {
            a.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            b.emplace_back(random::RandomVectorContainer<param_t,GeneralMidasVector>{}(5));
            auto c = a.back();
            midas::mmv::WrapHadamardProduct(c, b.back());
            ctrl_norm2 += midas::mmv::WrapNorm2(c);
         }

         midas::mmv::WrapHadamardProduct(a, b);

         UNIT_ASSERT_FEQUAL(midas::mmv::WrapNorm(a), std::sqrt(ctrl_norm2), "Bad result, WrapHadamardProduct.");
      }
   };

}; /* namespace midas::test */

#endif /* CONTAINERMATHWRAPPERSTEST_H_INCLUDED */