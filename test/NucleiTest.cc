#include "test/NucleiTest.h"

namespace midas::test
{

void NucleiTest()
{
   cutee::suite suite("Nuclei test");

   // Atomic data tests
   suite.add_test<AtomicDataTest>("Test reading of atomic_data test");
   
   // Nuclei tests
   suite.add_test<NucleiInternalCoordTest>("Nuclei coordinates test");
   suite.add_test<NucleiMassTest>("Nuclei mass test");
   suite.add_test<NucleiRotationTest>("Nuclei rotation test");
   
   RunSuite(suite);
}

} /* namespace midas::test */
