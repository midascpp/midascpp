/**
 *******************************************************************************
 * 
 * @file    ExtLibsTest.cc
 * @date    19-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Compile time checks that libraries are included/linked.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/ExtLibsTest.h"

namespace midas::test
{
   namespace extlibs::detail
   {
   } /* namespace extlibs::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void ExtLibsTest()
   {
      // Create test_suite object.
      cutee::suite suite("ExtLibs test");

      // Add all the tests to the suite.
      using namespace extlibs;

#ifdef ENABLE_BOOST
      suite.add_test<Boost>("boost");
#endif /* ENABLE_BOOST */

      // Always enabled.
      suite.add_test<Cutee>("cutee");

#ifdef ENABLE_FFTW
      suite.add_test<Fftw>("fftw");
#endif /* ENABLE_FFTW */

#ifdef ENABLE_GSL
      suite.add_test<Gsl>("gsl");
#endif /* ENABLE_GSL */

      // Always enabled.
      suite.add_test<Lapack>("lapack");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
