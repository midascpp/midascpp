/**
 *******************************************************************************
 *
 * @file    ModeCombiOpRangeTest.h
 * @date    14-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for the ModeCombiOpRange class and related functions.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 *
 *******************************************************************************
 **/

#ifndef MODECOMBIOPRANGETEST_H_INCLUDED
#define MODECOMBIOPRANGETEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "input/ModeCombiOpRange.h"
#include "input/FlexCoupCalcDef.h"
#include "input/GroupCouplings.h"
#include "input/FlexCouplings.h"

namespace midas::test::modecombioprange
{
   namespace detail
   {
      //! Check that ModeCombi is equivalent (same mode numbers) as vector.
      std::pair<bool, std::string> CheckModeCombi(const ModeCombi&, const std::vector<In>&);

      //! Check ModeCombi%s through CheckModeCombisGet() and CheckModeCombisIterators().
      std::pair<bool, std::string> CheckModeCombis
         (  const ModeCombiOpRange&
         ,  const std::vector<std::vector<In>>&
         );

      //! Check all ModeCombi%s are equivalent to those in vectors using Size() and GetModeCombi().
      std::pair<bool, std::string> CheckModeCombisGet
         (  const ModeCombiOpRange&
         ,  const std::vector<std::vector<In>>&
         );

      //! Check all ModeCombi%s are equivalent to those in vectors using iterators.
      std::pair<bool, std::string> CheckModeCombisIterators
         (  const ModeCombiOpRange&
         ,  const std::vector<std::vector<In>>&
         );

      //@{
      //! Check reduced sizes/num. per exc. level  based on the control num. per exc. level.
      std::pair<bool, std::string> CheckNumWithExcLevel
         (  const ModeCombiOpRange&
         ,  const std::vector<Uin>&
         );
      std::pair<bool, std::string> CheckReducedSizes
         (  const ModeCombiOpRange&
         ,  const std::vector<Uin>&
         );
      //@}

      //! Check than the ModeCombi ref. numbers are 0,...,Size()-1.
      std::pair<bool, std::string> CheckRefNumbers(const ModeCombiOpRange&);

      //! Check the ModeCombi addresses.
      std::pair<bool, std::string> CheckAddresses
         (  const ModeCombiOpRange&
         ,  const std::vector<In>&
         );

      //! Check the ModeCombi%s of the range (and their order), except for their addresses.
      std::pair<bool, std::string> CheckQueries
         (  const ModeCombiOpRange&
         ,  const std::vector<std::vector<In>>&
         );

      //! Check partitionings.
      std::pair<bool, std::string> CheckPartitionings
         (  const std::vector<std::vector<ModeCombi>>& arVecVecMcs
         ,  const std::vector<std::vector<std::vector<In>>>& arCtrlMcs
         ,  const std::vector<std::vector<In>>& arCtrlAddresses
         ,  const std::vector<std::vector<In>>& arCtrlRefNums
         );

      //! Remove a file, plus some error output in case of failure.
      void RemoveFile(const std::string&);
   }

   /************************************************************************//**
    * @brief
    *    Tests default constructor; should create an empty ModeCombiOpRange.
    ***************************************************************************/
   struct ConstructorDefault
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         // Queries.
         UNIT_ASSERT_EQUAL(mcr.Size(), static_cast<Uin>(0), "Size() failed.");
         UNIT_ASSERT_EQUAL(mcr.GetMaxExciLevel(), static_cast<Uin>(0), "GetMaxExciLevel() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumModeCombisWithExcLevel(I_0), static_cast<Uin>(0), "NumModeCombisWithExcLevel(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.ModesContained(), std::vector<In>{}, "ModesContained() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumberOfModes(), static_cast<Uin>(0), "NumberOfModes() failed.");
         UNIT_ASSERT_EQUAL(mcr.ReducedSize(I_0), static_cast<Uin>(0), "ReducedSize(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.NumEmptyMCs(), static_cast<Uin>(0), "NumEmptyMCs() failed.");

         // Iterators.
         UNIT_ASSERT(mcr.begin() == mcr.end(), "begin() should equal end() for empty range.");
         UNIT_ASSERT(mcr.cbegin() == mcr.cend(), "cbegin() should equal cend() for empty range.");
         UNIT_ASSERT(mcr.rbegin() == mcr.rend(), "rbegin() should equal rend() for empty range.");
         UNIT_ASSERT(mcr.crbegin() == mcr.crend(), "crbegin() should equal crend() for empty range.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests basic constructor with only the empty ModeCombi in range.
    ***************************************************************************/
   struct ConstructorEmptyModeCombi
      :  public cutee::test
   {
      void run() override
      {
         Uin max_exci = I_0;
         Uin n_modes = I_0;
         ModeCombiOpRange mcr(max_exci, n_modes);
         UNIT_ASSERT_EQUAL(mcr.Size(), static_cast<Uin>(1), "Size() failed.");
         UNIT_ASSERT_EQUAL(mcr.GetMaxExciLevel(), static_cast<Uin>(0), "GetMaxExciLevel() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumModeCombisWithExcLevel(I_1), static_cast<Uin>(0), "NumModeCombisWithExcLevel(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.NumModeCombisWithExcLevel(I_0), static_cast<Uin>(1), "NumModeCombisWithExcLevel(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.ModesContained(), std::vector<In>{}, "ModesContained() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumberOfModes(), static_cast<Uin>(0), "NumberOfModes() failed.");
         UNIT_ASSERT_EQUAL(mcr.ReducedSize(I_0), static_cast<Uin>(1), "ReducedSize(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.NumEmptyMCs(), static_cast<Uin>(1), "NumEmptyMCs() failed.");

         // Since Size() is asserted to be 1, we can inspect the ModeCombi.
         UNIT_ASSERT_EQUAL(mcr.GetModeCombi(I_0), ModeCombi(std::vector<In>{}, -I_1), "Contained ModeCombi not the empty one, {}.");

         // Iterators.
         // const_iterator
         {
            auto beg = mcr.cbegin();
            auto end = mcr.cend();
            UNIT_ASSERT(beg != end, "cbegin() shouldn't equal cend() for non-empty range.");
            UNIT_ASSERT_EQUAL(*beg, mcr.GetModeCombi(I_0), "*cbegin() should equal GetModeCombi(0).");
            UNIT_ASSERT(++beg == end, "++cbegin() should equal cend() for range of Size() == 1.");
         }
         // iterator
         {
            auto beg = mcr.begin();
            auto end = mcr.end();
            UNIT_ASSERT(beg != end, "begin() shouldn't equal end() for non-empty range.");
            UNIT_ASSERT_EQUAL(*beg, mcr.GetModeCombi(I_0), "*begin() should equal GetModeCombi(0).");
            UNIT_ASSERT(++beg == end, "++begin() should equal end() for range of Size() == 1.");
         }
         // const_reverse_iterator
         {
            auto beg = mcr.crbegin();
            auto end = mcr.crend();
            UNIT_ASSERT(beg != end, "crbegin() shouldn't equal crend() for non-empty range.");
            UNIT_ASSERT_EQUAL(*beg, mcr.GetModeCombi(I_0), "*crbegin() should equal GetModeCombi(0).");
            UNIT_ASSERT(++beg == end, "++crbegin() should equal crend() for range of Size() == 1.");
         }
         // reverse_iterator
         {
            auto beg = mcr.rbegin();
            auto end = mcr.rend();
            UNIT_ASSERT(beg != end, "rbegin() shouldn't equal rend() for non-empty range.");
            UNIT_ASSERT_EQUAL(*beg, mcr.GetModeCombi(I_0), "*rbegin() should equal GetModeCombi(0).");
            UNIT_ASSERT(++beg == end, "++rbegin() should equal rend() for range of Size() == 1.");
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests basic constructor from max. exci. level.
    ***************************************************************************/
   struct ConstructorMaxExciLevel
      :  public cutee::test
   {
      void run() override
      {
         // 5 modes, up to 3-mode combinations.
         ModeCombiOpRange mcr(I_3, I_5);
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {3}
            ,  {4}
            ,  {0,1}
            ,  {0,2}
            ,  {0,3}
            ,  {0,4}
            ,  {1,2}
            ,  {1,3}
            ,  {1,4}
            ,  {2,3}
            ,  {2,4}
            ,  {3,4}
            ,  {0,1,2}
            ,  {0,1,3}
            ,  {0,1,4}
            ,  {0,2,3}
            ,  {0,2,4}
            ,  {0,3,4}
            ,  {1,2,3}
            ,  {1,2,4}
            ,  {1,3,4}
            ,  {2,3,4}
            };

         // The ModeCombi%s. Also checks Size(), GetModeCombi(), iterators.
         auto assert = detail::CheckModeCombis(mcr, control);
         UNIT_ASSERT(assert.first, "Inequality in range: " + assert.second);

         // Queries.
         UNIT_ASSERT_EQUAL(mcr.GetMaxExciLevel(), static_cast<Uin>(3), "GetMaxExciLevel() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumEmptyMCs(), static_cast<Uin>(1), "NumEmptyMCs() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumberOfModes(), static_cast<Uin>(5), "NumberOfModes() failed.");
         UNIT_ASSERT_EQUAL(mcr.ModesContained(), std::vector<In>({0,1,2,3,4}), "ModesContained() failed.");
         std::vector<Uin> num_exci_level = {1, 5, 10, 10, 0, 0};
         assert = detail::CheckNumWithExcLevel(mcr, num_exci_level);
         UNIT_ASSERT(assert.first, assert.second);
         assert = detail::CheckReducedSizes(mcr, num_exci_level);
         UNIT_ASSERT(assert.first, assert.second);
         assert = detail::CheckRefNumbers(mcr);
         UNIT_ASSERT(assert.first, assert.second);

         // CheckQueries tests the same things as above, so this is sort of a
         // way to ensure that CheckQueries work as intended.
         auto queries = detail::CheckQueries(mcr, control);
         UNIT_ASSERT
            (  queries.first
            ,  "CheckQueries failed, but shouldn't have (if getting to here); got: "+queries.second
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests basic constructor from max. exci. level. that is larger than
    *    number of modes, which should fallback to having set it equal to the
    *    number of modes.
    ***************************************************************************/
   struct ConstructorMaxExciLevelLTNumModes
      :  public cutee::test
   {
      void run() override
      {
         // 3 modes, up to 4-mode combinations; shall be equivalent to up to
         // 3-mode combinations.
         ModeCombiOpRange mcr(I_4, I_3);
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {0,1}
            ,  {0,2}
            ,  {1,2}
            ,  {0,1,2}
            };

         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests constructor using max. excitation level and max. excitations per
    *    mode.
    ***************************************************************************/
   struct ConstructorMaxExPerMode
      :  public cutee::test
   {
      void run() override
      {
         // 6 modes, up to 5-mode combinations, modes 2, 4, 5 restricted.
         // Effectively, due to restrictions, there will only be up to 4-mode
         // couplings, and mode 2 will not participate at all, so only 5 modes.
         ModeCombiOpRange mcr(I_5, {5,5,0,5,4,2});
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {3}
            ,  {4}
            ,  {5}
            ,  {0,1}
            ,  {0,3}
            ,  {0,4}
            ,  {0,5}
            ,  {1,3}
            ,  {1,4}
            ,  {1,5}
            ,  {3,4}
            ,  {3,5}
            ,  {4,5}
            ,  {0,1,3}
            ,  {0,1,4}
            ,  {0,3,4}
            ,  {1,3,4}
            ,  {0,1,3,4}
            };

         // The ModeCombi%s. Also checks Size(), GetModeCombi(), iterators.
         auto assert = detail::CheckModeCombis(mcr, control);
         UNIT_ASSERT(assert.first, "Inequality in range: " + assert.second);

         // Queries.
         UNIT_ASSERT_EQUAL(mcr.GetMaxExciLevel(), static_cast<Uin>(4), "GetMaxExciLevel() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumEmptyMCs(), static_cast<Uin>(1), "NumEmptyMCs() failed.");
         UNIT_ASSERT_EQUAL(mcr.NumberOfModes(), static_cast<Uin>(5), "NumberOfModes() failed.");
         UNIT_ASSERT_EQUAL(mcr.ModesContained(), std::vector<In>({0,1,3,4,5}), "ModesContained() failed.");
         std::vector<Uin> num_exci_level = {1, 5, 10, 4, 1, 0, 0};
         assert = detail::CheckNumWithExcLevel(mcr, num_exci_level);
         UNIT_ASSERT(assert.first, assert.second);
         assert = detail::CheckReducedSizes(mcr, num_exci_level);
         UNIT_ASSERT(assert.first, assert.second);
         assert = detail::CheckRefNumbers(mcr);
         UNIT_ASSERT(assert.first, assert.second);

         // CheckQueries tests the same things as above, so this is sort of a
         // way to ensure that CheckQueries work as intended.
         auto queries = detail::CheckQueries(mcr, control);
         UNIT_ASSERT
            (  queries.first
            ,  "CheckQueries failed, but shouldn't have (if getting to here); got: "+queries.second
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests constructor using a FlexCoupCalcDef with GroupCouplings.
    *
    * First sets up vectors of strings corresponding to mode groups and group
    * combis respectively. (These correspond to what would be given on input
    * when running the program for real.) Do a check that these are as
    * expected.
    *
    * Then set up a FlexCoupCalcDef, and then a ModeCombiOpRange using this
    * FlexCoupCalcDef. (The ModeCombiOpRange constructor internally extracts a
    * GroupCouplings object from the vectors of strings in the
    * FlexCoupCalcDef.)
    *
    * Lastly verify that the ModeCombiOpRange is as expected.
    *
    * @note
    *    Also does a check of the constructed GroupCouplings, which actually
    *    ought to be done in a separate unit test... But I'm not setting that
    *    up now. -MBH, Mar 2017.
    ***************************************************************************/
   struct ConstructorGroupCouplings
      :  public cutee::test
   {
      void run() override
      {
         // Set up GroupCouplings stuff. It's based on strings (in practice from
         // input file).
         // Do tests that these are as expected (should really be done in a
         // GroupCouplings unit test...).

         // Each string of this vector is a mode group. The mode groups must be
         // disjoint.
         const std::vector<std::string> vec_str_mode_groups =
            {  "0 2"
            ,  "1 3"
            ,  "4"
            };

         // Each string of this vector is a group combi.
         // The format is "max_level group_1 group_2 ...";
         // "3 1 2" --> {1,2}, {1,1,2}, {1,2,2}
         // (Each group contained at least once, maximum max_level groups in each
         // combi.)
         const std::vector<std::string> vec_str_group_combis =
            {  "2 0"       // GroupCombis: {0}, {0,0}
            ,  "1 2"       // GroupCombis: {2}
            ,  "2 1 2"     // GroupCombis: {1,2}
            ,  "4 0 1 2"   // GroupCombis: {0,1,2}, {0,0,1,2}, {0,1,1,2}, {0,1,2,2}
            };

         // Check that the generated GroupCouplings are as expected.
         {
            GroupCouplings gcs(vec_str_mode_groups, vec_str_group_combis);

            // Controls:
            const std::vector<std::vector<In>> ctrl_mode_groups =
               {  {0,2}
               ,  {1,3}
               ,  {4}
               };
            UNIT_ASSERT(gcs.NrGroups() == ctrl_mode_groups.size(), "Wrong num. groups.");
            for(Uin i = I_0; i < gcs.NrGroups(); ++i)
            {
               bool assert = (gcs.ModeGroup(i) == ctrl_mode_groups[i]);
               UNIT_ASSERT(assert, "ModeGroup("+std::to_string(i)+") is wrong.");
            }

            const std::vector<std::vector<In>> ctrl_group_combis =
               {  {0}
               ,  {0,0}
               ,  {2}
               ,  {1,2}
               ,  {0,1,2}
               ,  {0,0,1,2}
               ,  {0,1,1,2}
               ,  {0,1,2,2}
               };
            UNIT_ASSERT(gcs.NrGroupCombis() == ctrl_group_combis.size(), "Wrong num. GroupCombis.");
            for(Uin i = I_0; i < gcs.NrGroupCombis(); ++i)
            {
               bool assert = (gcs.GroupCombi(i) == ctrl_group_combis[i]);
               UNIT_ASSERT(assert, "GroupCombi("+std::to_string(i)+") is wrong.");
            }
         }

         ModeCombiOpRange mcr(GroupCouplings(vec_str_mode_groups, vec_str_group_combis));

         // Control for ModeCombiOpRange.
         std::vector<std::vector<In>> control =
            {  {}          // Empty ModeCombi always inserted.
            ,  {0}         // GC = {0},   with MG[0] = {0,2}
            ,  {2}         // --
            ,  {4}         // GC = {2},   with MG[2] = {4}
            ,  {0,2}       // GC = {0,0}, with MG[0] = {0,2}
            ,  {1,4}       // GC = {1,2}, with MG[1] = {1,3}, MG[2] = {4}
            ,  {3,4}       // --
            ,  {0,1,4}     // GC = {0,1,2}, with MG[0] = {0,2}, MG[1] = {1,3}, MG[2] = {4}
            ,  {0,3,4}     // --
            ,  {1,2,4}     // --
            ,  {2,3,4}     // --
            ,  {0,1,2,4}   // GC = {0,0,1,2}
            ,  {0,1,3,4}   // GC = {0,1,1,2}
            ,  {0,2,3,4}   // GC = {0,0,1,2}
            ,  {1,2,3,4}   // GC = {0,1,1,2}
            };
         // ... and GC = {0,1,2,2} reduces to GC = {0,1,2} since there's only one
         // mode in MG[2] = {4}.

         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   namespace detail
   {
      /************************************************************************//**
       * @brief
       *    A base class with setup/teardown implementations for FlexCouplings
       *    constructor tests.
       ***************************************************************************/
      struct ConstructorFlexCouplingsBase
         :  public cutee::test
      {
         using contents_line_t = std::tuple<std::vector<In>, Nb>;
         using contents_t = std::vector<contents_line_t>;

         //! The file name; to be implemented by derived test.
         virtual std::string FileName() const = 0;

         //! The actual file contents, specific to the derived test.
         virtual contents_t FileContents() const = 0;

         //! Write the ModeCombi like e.g. `0,2,3`.
         static midas::mpi::OFileStream& WriteModeCombi(midas::mpi::OFileStream& arOs, const std::vector<In>&);

         //! Write the file, lines like e.g. `0,2,3 5.8285326503977944500646E-06`.
         static void WriteFile(const std::string&, const contents_t&);

         void setup() override {WriteFile(FileName(), FileContents());}
         void teardown() override {RemoveFile(FileName());}
      };
   } /* namespace detail */

   /***************************************************************************//**
    * @brief
    *    Tests constructor using a FlexCoupCalcDef with FlexCouplings.
    *
    * This one taken from the ModeCombiOpRange constructed in the
    * `mc_screen_out` test.
    ******************************************************************************/
   struct ConstructorFlexCouplingsA
      :  public detail::ConstructorFlexCouplingsBase
   {
      std::string FileName() const override
      {
         return "ConstructorFlexCouplingsA_mc_screen_estimates";
      }

      contents_t FileContents() const override
      {
         return
            {  {  {0}, 1.1733048207976302240718E-02}
            ,  {  {1}, 1.1969137296341995566062E-02}
            ,  {  {2}, 3.3456076104102998314271E-02}
            ,  {  {3}, 3.3373472312935074934881E-02}
            ,  {  {4}, 3.3632691021881738757671E-02}
            ,  {  {5}, 3.3373472311932377010191E-02}
            ,  {  {0,1}, 2.2437033869070944521037E-05}
            ,  {  {0,2}, 6.7885928706942381483450E-05}
            ,  {  {0,3}, 2.2097367934857288507278E-03}
            ,  {  {0,4}, 6.4201135916321030129767E-06}
            ,  {  {0,5}, 2.2097367934600484380703E-03}
            ,  {  {1,2}, 2.3954364085393216494968E-03}
            ,  {  {1,3}, 2.8759432879907151781994E-05}
            ,  {  {1,4}, 2.0001746110537292795695E-03}
            ,  {  {1,5}, 2.8759434973734816337046E-05}
            ,  {  {2,3}, 9.9527797770628337065829E-05}
            ,  {  {2,4}, 5.2088783759961687809537E-04}
            ,  {  {2,5}, 9.9527795715618461148098E-05}
            ,  {  {3,4}, 5.0245776999087120092834E-06}
            ,  {  {3,5}, 3.7266945239045051871049E-04}
            ,  {  {4,5}, 5.0245782574976999778574E-06}
            ,  {  {0,1,2}, 9.8422171486394001058660E-06}
            ,  {  {0,1,3}, 3.6300611505572486601844E-06}
            ,  {  {0,1,4}, 3.1564577391490171166507E-06}
            ,  {  {0,1,5}, 3.6300632111054190103512E-06}
            ,  {  {0,2,3}, 5.8285326503977944500646E-06}
            ,  {  {0,2,4}, 8.0623655261449226525999E-07}
            ,  {  {0,2,5}, 5.8285326587076171083335E-06}
            ,  {  {0,3,4}, 3.3070157083986460747486E-07}
            ,  {  {0,3,5}, 2.9607152650007971788487E-04}
            ,  {  {0,4,5}, 3.3070158817945828302575E-07}
            ,  {  {1,2,3}, 8.3080912704392645212140E-06}
            ,  {  {1,2,4}, 2.9466573634305656854970E-04}
            ,  {  {1,2,5}, 8.3080917140836550190261E-06}
            ,  {  {1,3,4}, 1.6221162769052093538324E-06}
            ,  {  {1,3,5}, 1.7635468143625069363604E-06}
            ,  {  {1,4,5}, 1.6221164321708152020348E-06}
            ,  {  {2,3,4}, 1.1247495640173942614066E-06}
            ,  {  {2,3,5}, 3.9155348303250924767486E-06}
            ,  {  {2,4,5}, 1.1247496864639060045236E-06}
            ,  {  {3,4,5}, 1.1511221640071826428171E-06}
            ,  {  {0,1,2,3}, 1.5969883238029357899962E-07}
            ,  {  {0,1,2,4}, 1.5112977790562522292556E-07}
            ,  {  {0,1,2,5}, 1.5969883114604650792983E-07}
            ,  {  {0,1,3,4}, 3.3940233306664886783586E-08}
            ,  {  {0,1,3,5}, 1.8548360178990759308975E-07}
            ,  {  {0,1,4,5}, 3.3940233252751418361863E-08}
            ,  {  {0,2,3,4}, 4.6965264257979443806767E-08}
            ,  {  {0,2,3,5}, 3.5712460461710244853073E-07}
            ,  {  {0,2,4,5}, 4.6965264071075469771349E-08}
            ,  {  {0,3,4,5}, 1.4791588288825577227121E-07}
            ,  {  {1,2,3,4}, 9.8754304558904869654602E-09}
            ,  {  {1,2,3,5}, 1.3194306892737743773588E-08}
            ,  {  {1,2,4,5}, 9.8754093420292062653208E-09}
            ,  {  {1,3,4,5}, 2.3219393279211441402444E-08}
            ,  {  {2,3,4,5}, 7.3129206167731686410619E-08}
            };
      }

      void run() override
      {
         FlexCoupCalcDef calcdef;
         calcdef.SetmMcScreenThr(1.0e-04);
         calcdef.SetmSetFlexCoup(true);
         calcdef.SetmMcScreenFile(FileName());
         FlexCouplings fc(calcdef);
         ModeCombiOpRange mcr(fc, 3, 6);
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {3}
            ,  {4}
            ,  {5}
            ,  {0,3}
            ,  {0,5}
            ,  {1,2}
            ,  {1,4}
            ,  {2,4}
            ,  {3,5}
            ,  {0,3,5}
            ,  {1,2,4}
            };

         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests constructor using a FlexCoupCalcDef with FlexCouplings.
    *
    * This one is with a made up set of screening estimates, that tests special
    * cases more thoroughly.
    *
    * In particular tests that:
    * -  ModeCombi%s are only included if their scr.est. is higher than or
    *    equal to threshold.
    * -  That only up to the given max. level is included.
    * -  That sub-ModeCombi%s are included (even though their scr.est. is too
    *    low) if contained in an accepted higher-level ModeCombi, _if_ that
    *    ModeCombi's level is <= the max. level.
    * -  That the max level can be set higher than what's in the file, and that
    *    that will just be equivalent to setting the max level equal to the
    *    highest level in the file.
    ***************************************************************************/
   struct ConstructorFlexCouplingsB
      :  public detail::ConstructorFlexCouplingsBase
   {
      std::string FileName() const override
      {
         return "ConstructorFlexCouplingsB_mc_screen_estimates";
      }

      contents_t FileContents() const override
      {
         return
            {  {  {0},     0.75}
            ,  {  {1},     0.75}
            ,  {  {2},     0.25}
            ,  {  {3},     0.75}
            ,  {  {0,1},   0.75}
            ,  {  {0,2},   0.25}
            ,  {  {0,3},   0.75}
            ,  {  {1,2},   0.25}
            ,  {  {1,3},   0.75}
            ,  {  {2,3},   0.25}
            ,  {  {0,1,2}, 0.75}
            ,  {  {0,1,3}, 0.25}
            ,  {  {0,2,3}, 0.25}
            ,  {  {1,2,3}, 0.25}
            };
      }

      struct Settings
      {
         Nb mThr;
         Uin mMaxLevel;
         std::vector<std::vector<In>> mControl;

         Settings(Nb aThr, Uin aMaxLevel, std::vector<std::vector<In>> aControl)
            :  mThr(aThr)
            ,  mMaxLevel(aMaxLevel)
            ,  mControl(aControl)
         {}
      };

      void run() override
      {
         std::vector<Settings> v_settings =
            {  Settings(0.75, 1, {{}, {0}, {1}, {3}})
            ,  Settings(0.75+C_NB_EPSILON, 1, {{}})
            ,  Settings(0.1, 2, {{}, {0}, {1}, {2}, {3}, {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}})
            ,  Settings(0.5, 2, {{}, {0}, {1}, {3}, {0,1}, {0,3}, {1,3}})
            ,  Settings(0.5, 3, {{}, {0}, {1}, {2}, {3}, {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {0,1,2}})
            ,  Settings(0.5, 4, {{}, {0}, {1}, {2}, {3}, {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {0,1,2}})
            };

         Uin index = 0;
         for(const auto& s: v_settings)
         {
            FlexCoupCalcDef calcdef;
            calcdef.SetmMcScreenThr(s.mThr);
            calcdef.SetmSetFlexCoup(true);
            calcdef.SetmMcScreenFile(FileName());
            ModeCombiOpRange mcr(calcdef, s.mMaxLevel, 4);

            auto assert = detail::CheckQueries(mcr, s.mControl);
            UNIT_ASSERT(assert.first, "i = " + std::to_string(index) + assert.second);
            ++index;
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests constructor from an effective excitation count vector.
    ***************************************************************************/
   struct ConstructorEffectiveExciCount
      :  public cutee::test
   {
      void run() override
      {
         // Set up for multiple tests.
         std::vector
            <  std::tuple
               <  std::vector<Uin>              // effective exci. count
               ,  Uin                           // max level
               ,  std::vector<std::vector<In>>  // control
               >
            > v_settings;

         // Test 0:
         //    Empty exc. count vector; no modes, only empty ModeCombi.
         v_settings.emplace_back
            (  std::vector<Uin>{}
            ,  2
            ,  std::vector<std::vector<In>>
               {  {}
               }
            );
         // Test 1:
         //    Exc. count vector with all ones.
         //    This will be equivalent to normal max. exci. level constructor.
         v_settings.emplace_back
            (  std::vector<Uin>{1,1,1}
            ,  2
            ,  std::vector<std::vector<In>>
               {  {}
               ,  {0}
               ,  {1}
               ,  {2}
               ,  {0,1}
               ,  {0,2}
               ,  {1,2}
               }
            );
         // Test 2:
         //    Exc. count vector with all zeroes.
         //    Since no modes contribute to the effective excitation level, all
         //    ModeCombis from given modes will be generated.
         v_settings.emplace_back
            (  std::vector<Uin>{0,0,0}
            ,  0
            ,  std::vector<std::vector<In>>
               {  {}
               ,  {0}
               ,  {1}
               ,  {2}
               ,  {0,1}
               ,  {0,2}
               ,  {1,2}
               ,  {0,1,2}
               }
            );
         // Test 3:
         //    Now with a non-trivial effective exci level count.
         //    ModeCombis are generated only if their count is <= the given level.
         v_settings.emplace_back
            (  std::vector<Uin>{1,0,0,2,1,0}
            ,  2
            ,  std::vector<std::vector<In>>
               {  {}
               ,  {0}
               ,  {1}
               ,  {2}
               ,  {3}
               ,  {4}
               ,  {5}
               ,  {0,1}
               ,  {0,2}
               // {0,3} missing, eff. count is 1 + 2 = 3
               ,  {0,4}
               ,  {0,5}
               ,  {1,2}
               ,  {1,3}
               ,  {1,4}
               ,  {1,5}
               ,  {2,3}
               ,  {2,4}
               ,  {2,5}
               // {3,4} missing, eff. count is 2 + 1 = 3
               ,  {3,5}
               ,  {4,5}
               ,  {0,1,2}
               // {0,1,3} missing, eff. count is 1 + 0 + 2 = 3
               ,  {0,1,4}
               ,  {0,1,5}
               // {0,2,3} missing, eff. count is 1 + 0 + 2 = 3
               ,  {0,2,4}
               ,  {0,2,5}
               // {0,3,4} missing, eff. count is 1 + 2 + 1 = 4
               // {0,3,5} missing, eff. count is 1 + 2 + 0 = 3
               ,  {0,4,5}
               ,  {1,2,3}
               ,  {1,2,4}
               ,  {1,2,5}
               // {1,3,4} missing, eff. count is 0 + 2 + 1 = 3
               ,  {1,3,5}
               ,  {1,4,5}
               // {2,3,4} missing, eff. count is 0 + 2 + 1 = 3
               ,  {2,3,5}
               ,  {2,4,5}
               // {3,4,5} missing, eff. count is 2 + 1 + 0 = 3
               // {0,1,2,3} missing, eff. count is 1 + 0 + 0 + 2 = 3
               ,  {0,1,2,4}
               ,  {0,1,2,5}
               // {0,1,3,4} missing, eff. count is 1 + 0 + 2 + 1 = 4
               // {0,1,3,5} missing, eff. count is 1 + 0 + 2 + 0 = 3
               ,  {0,1,4,5}
               // {0,2,3,4} missing, eff. count is 1 + 0 + 2 + 1 = 4
               // {0,2,3,5} missing, eff. count is 1 + 0 + 2 + 0 = 3
               ,  {0,2,4,5}
               // {0,3,4,5} missing, eff. count is 1 + 2 + 1 + 0 = 4
               // {1,2,3,4} missing, eff. count is 0 + 0 + 2 + 1 = 3
               ,  {1,2,3,5}
               ,  {1,2,4,5}
               // {1,3,4,5} missing, eff. count is 0 + 2 + 1 + 0 = 3
               // {2,3,4,5} missing, eff. count is 0 + 2 + 1 + 0 = 3
               // {0,1,2,3,4} missing, eff. count is 1 + 0 + 0 + 2 + 1 = 4
               // {0,1,2,3,5} missing, eff. count is 1 + 0 + 0 + 2 + 0 = 3
               ,  {0,1,2,4,5}
               // {0,1,3,4,5} missing, eff. count is 1 + 0 + 2 + 1 + 0 = 4
               // {0,2,3,4,5} missing, eff. count is 1 + 0 + 2 + 1 + 0 = 4
               // {1,2,3,4,5} missing, eff. count is 0 + 0 + 2 + 1 + 0 = 3
               // {0,1,2,3,4,5} missing, eff. count is 1 + 0 + 0 + 2 + 1 + 0  = 4
               }
            );
         // Test 4:
         //    Non-trivial effective exci level count.
         //    Max level is larger than number of modes.
         //    ModeCombis are generated only if their count is <= the given level.
         v_settings.emplace_back
            (  std::vector<Uin>{0,2,3,5}
            ,  5
            ,  std::vector<std::vector<In>>
               {  {}
               ,  {0}
               ,  {1}
               ,  {2}
               ,  {3}
               ,  {0,1}
               ,  {0,2}
               ,  {0,3}
               ,  {1,2}
               ,  {0,1,2}
               }
            );

         // Then run the subtests.
         Uin i = I_0;
         for(const auto& sett: v_settings)
         {
            const auto& eff_exc_count = std::get<0>(sett);
            const auto& max_level = std::get<1>(sett);
            const auto& control = std::get<2>(sett);
            auto mcr = ModeCombiOpRange::ConstructFromEffectiveExciCount(max_level, eff_exc_count);

            auto assert = detail::CheckQueries(mcr, control);
            UNIT_ASSERT(assert.first, "i = "+std::to_string(i)+assert.second);
            ++i;
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests constructor wrapper using extended range modes.
    *
    * This just wraps a call to the
    * ModeCombiOpRange::ConstructFromEffectiveExciCount() where the given modes
    * counts 0 towards the effective exc. level, and the rest count 1 (as
    * usual).
    ***************************************************************************/
   struct ConstructorWrapperExtendedRangeModes
      :  public cutee::test
   {
      void run() override
      {
         // Effective max. level 2, 6 modes, modes {1,3} count zero towards the max.
         auto mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(2, 6, {1,3});
         auto mcr_control = ModeCombiOpRange::ConstructFromEffectiveExciCount(2, {1,0,1,0,1,1});

         // Convert control range to vector, just to reuse existing assertion interface.
         std::vector<std::vector<In>> v_control;
         for(const auto& mc: mcr_control)
         {
            v_control.push_back(mc.MCVec());
         }

         auto assert = detail::CheckQueries(mcr, v_control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Test insertion of already sorted ModeCombi%s, into empty
    *    ModeCombiOpRange.
    ***************************************************************************/
   struct InsertSortedIntoEmptyRange
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         std::vector<std::vector<In>> control;
         control.insert(control.end(), {{}, {0}});

         // Calls Insert(std::vector<ModeCombi>&&) if available.
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(control.at(0),-1)
               ,  ModeCombi(control.at(1),-1)
               }
            );
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "Insert(std::vector<ModeCombi> rvalue) failed:"+assert.second);

         // Calls Insert(const std::vector<ModeCombi>&).
         control.insert(control.end(), {{1,2}, {2,4}, {1,2,4,6}});
         std::vector<ModeCombi> vec_mc =
            {  ModeCombi(control.at(2), -1)
            ,  ModeCombi(control.at(3), -1)
            ,  ModeCombi(control.at(4), -1)
            };
         mcr.Insert(vec_mc);
         assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "Insert(std::vector<ModeCombi> lvalue) failed:"+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Test insertion of unsorted ModeCombi%s, into nonempty
    *    ModeCombiOpRange.
    ***************************************************************************/
   struct InsertUnsortedIntoNonEmptyRange
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         std::vector<std::vector<In>> control =
            {  {2}
            ,  {4}
            ,  {0,4}
            ,  {5,12}
            ,  {2,4,6}
            ,  {0,4,12,13}
            };

         // Set up ModeCombiOpRange with _some_ of the ModeCombi%s in control.
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(control.at(2), -1)
               ,  ModeCombi(control.at(4), -1)
               }
            );
         // Then insert rest afterwards (in unsorted order), to ensure that
         // they are correctly sorted after insertion.
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(control.at(1), -1)
               ,  ModeCombi(control.at(0), -1)
               ,  ModeCombi(control.at(5), -1)
               ,  ModeCombi(control.at(3), -1)
               }
            );
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Test insertion of ModeCombi%s already in the range; test that they're
    *    not duplicated.
    ***************************************************************************/
   struct InsertIdenticalModeCombis
      :  public cutee::test
   {
      void run() override
      {
         // 3 modes, only modes 1, 2 can be part of 2-mode combinations;
         // {}, {0}, {1}, {2}, {1,2} so far...
         // Then we insert some that are not already there, and some that are
         // (which then shouldn't be duplicated in the range).
         ModeCombiOpRange mcr(I_2, {1,2,2});
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{2}, -1)
               ,  ModeCombi(std::vector<In>{}, -1)
               ,  ModeCombi(std::vector<In>{1}, -1)
               ,  ModeCombi(std::vector<In>{1}, -1)
               ,  ModeCombi(std::vector<In>{0,1}, -1)    // <- should insert this
               ,  ModeCombi(std::vector<In>{0,1,2}, -1)  // <- should insert this
               ,  ModeCombi(std::vector<In>{0,1}, -1)
               ,  ModeCombi(std::vector<In>{1}, -1)
               }
            );
         // Now the range should contain:
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {0,1}
            ,  {1,2}
            ,  {0,1,2}
            };
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Test insertion of ModeCombi%s already in the range; test the existing
    *    ones are the ones to remain in the range, i.e. that they're not
    *    overwritten.
    ***************************************************************************/
   struct InsertCheckExistingRemains
      :  public cutee::test
   {
      void run() override
      {
         // 3 modes, up to 2-mode combinations. Make a control for later use.
         ModeCombiOpRange mcr;
         In magic_address = 1337;
         Uin magic_index = 3;
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{}, -1)
               ,  ModeCombi(std::vector<In>{0}, -1)
               ,  ModeCombi(std::vector<In>{1}, -1)
               ,  ModeCombi(std::vector<In>{2}, magic_address)
               ,  ModeCombi(std::vector<In>{0,1}, -1)
               ,  ModeCombi(std::vector<In>{0,2}, -1)
               ,  ModeCombi(std::vector<In>{1,2}, -1)
               ,  ModeCombi(std::vector<In>{0,1,2}, -1)
               }
            );
         std::vector<std::vector<In>> control;
         for(const auto& mc: mcr)
         {
            control.emplace_back(mc.MCVec());
         }
         const auto magic_mc_vec = mcr.GetModeCombi(magic_index).MCVec();

         // Then construct a lot of identical ModeCombi, with different
         // addresses (so all are distinguishable), and insert in one bunch.
         std::vector<ModeCombi> vec;
         for(Uin address = I_0; address < magic_address; ++address)
         {
            vec.emplace_back(magic_mc_vec, address);
         }

         // Then we insert, and an unstable sorting algorithm will probably
         // shuffle up the inserted ModeCombi%s so that one of the inserted
         // ones will replace the existing one.
         mcr.Insert(vec);

         // Range should be unaffected.
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);

         // Does the right ModeCombi remain in the range?
         UNIT_ASSERT_EQUAL
            (  mcr.GetModeCombi(magic_index).Address()
            ,  magic_address
            ,  "Wrong Address(); the existing ModeCombi must have been overwritten."
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Erases a sorted vector of ModeCombis from the range.
    ***************************************************************************/
   struct EraseSortedModeCombis
      :  public cutee::test
   {
      void run() override
      {
         // 4 modes, up to 2-mode combinations.
         // {}, {0}, {1}, {2}, {3}, {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}
         ModeCombiOpRange mcr(I_2, I_4);

         // Then erase some, calling Erase(std::vector<ModeCombi>&&) if available.
         // Put some non-standard addresses, to ensure only the mode numbers
         // are used for comparison checking.
         // Also put some that are not in the range.
         mcr.Erase
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{1}, 137)
               ,  ModeCombi(std::vector<In>{0,1}, 232)
               ,  ModeCombi(std::vector<In>{2,3}, 748)
               ,  ModeCombi(std::vector<In>{0,2,3}, 828) // <- not in range
               ,  ModeCombi(std::vector<In>{1,2,3}, 913) // <- not in range
               }
            );
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {2}
            ,  {3}
            ,  {0,2}
            ,  {0,3}
            ,  {1,2}
            ,  {1,3}
            };
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "Erase(std::vector<ModeCombi> rvalue) failed:"+assert.second);

         // Calls Erase(const std::vector<ModeCombi>&).
         std::vector<ModeCombi> vec_mc =
            {  ModeCombi(std::vector<In>{}, 22)
            ,  ModeCombi(std::vector<In>{2}, 83)
            ,  ModeCombi(std::vector<In>{0,2}, 112)
            ,  ModeCombi(std::vector<In>{1,2}, 736)
            ,  ModeCombi(std::vector<In>{0,1,2,3}, 1239) // <- not in range
            };
         mcr.Erase(vec_mc);
         control = {{0}, {3}, {0,3}, {1,3}};
         assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "Erase(std::vector<ModeCombi> lvalue) failed:"+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Erases an unsorted vector of ModeCombis from the range.
    ***************************************************************************/
   struct EraseUnsortedModeCombis
      :  public cutee::test
   {
      void run() override
      {
         // 4 modes, up to 2-mode combinations.
         // {}, {0}, {1}, {2}, {3}, {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}
         ModeCombiOpRange mcr(I_2, I_4);

         // Erase some that are not in order. Also with some not in the range.
         mcr.Erase
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{1}, 137)
               ,  ModeCombi(std::vector<In>{0,1}, 232)
               ,  ModeCombi(std::vector<In>{2,3,5}, 83)  // <- not in the range
               ,  ModeCombi(std::vector<In>{2,3}, 748)
               ,  ModeCombi(std::vector<In>{0,2}, 112)
               ,  ModeCombi(std::vector<In>{3,4}, 83)    // <- not in the range
               ,  ModeCombi(std::vector<In>{1,2}, 736)
               ,  ModeCombi(std::vector<In>{}, 22)
               ,  ModeCombi(std::vector<In>{2}, 83)
               ,  ModeCombi(std::vector<In>{6}, 83)      // <- not in the range
               }
            );
         std::vector<std::vector<In>> control = {{0}, {3}, {0,3}, {1,3}};

         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Clear the contents of the range.
    ***************************************************************************/
   struct Clear
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(I_2, I_2);
         std::vector<std::vector<In>> control = {{}, {0}, {1}, {0,1}};
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "Before clear: "+assert.second);

         mcr.Clear();
         control = {};
         assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After clear: "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Find ModeCombi%s in the range.
    ***************************************************************************/
   struct Find
      :  public cutee::test
   {
      void run() override
      {
         // Empty range.

         // Construct {{}, {0}, {1}, {2}, {1,2}}.
         ModeCombiOpRange mcr(I_2, {1,2,2});
         std::vector<std::vector<In>> control = {{}, {0}, {1}, {2}, {1,2}};
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);

         // Check non-existing one.
         ModeCombi mc_a(std::vector<In>{0,1}, -I_1);
         decltype(mcr.begin()) mc_iter;
         bool found = mcr.Find(mc_a, mc_iter);
         UNIT_ASSERT_EQUAL(found, false, "Find(ModeCombi) returns true, but shouldn't.");
         UNIT_ASSERT(mc_iter == mcr.end(), "Find(ModeCombi) iterator should've been end() on failure.");

         // Check existing one.
         ModeCombi mc_b(std::vector<In>{1}, -I_1);
         found = mcr.Find(mc_b, mc_iter);
         UNIT_ASSERT_EQUAL(found, true, "Find(ModeCombi) returns false, but shouldn't.");
         UNIT_ASSERT(mc_iter != mcr.end(), "Find(ModeCombi) returns end() iterator but shouldn't.");
         UNIT_ASSERT_EQUAL(*mc_iter, mc_b, "Find(ModeCombi) returns wrong iterator.");

         // Now remove the existing, insert the non-existing, and check again.
         mcr.Erase(std::vector<ModeCombi>{mc_b});
         mcr.Insert(std::vector<ModeCombi>{mc_a});
         control = {{}, {0}, {2}, {0,1}, {1,2}};
         assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After insert/erase: "+assert.second);

         // Checks again.
         found = mcr.Find(mc_b, mc_iter);
         UNIT_ASSERT_EQUAL(found, false, "Find(ModeCombi) returns true, but shouldn't.");
         UNIT_ASSERT(mc_iter == mcr.end(), "Find(ModeCombi) iterator should've been end() on failure.");

         found = mcr.Find(mc_a, mc_iter);
         UNIT_ASSERT_EQUAL(found, true, "Find(ModeCombi) returns false, but shouldn't.");
         UNIT_ASSERT(mc_iter != mcr.end(), "Find(ModeCombi) returns end() iterator but shouldn't.");
         UNIT_ASSERT_EQUAL(*mc_iter, mc_a, "Find(ModeCombi) returns wrong iterator.");

         // Aaaaand also test the std::vector<In> version. (Although probably
         // already tested, since it's called by ModeCombi version.
         std::vector<std::pair<bool, std::vector<In>>> v_pairs =
            {  {false, {0,2}}
            ,  {true, {}}
            ,  {true, {1,2}}
            };
         for(const auto& p: v_pairs)
         {
            found = mcr.Find(p.second, mc_iter);
            UNIT_ASSERT_EQUAL(found, p.first, "Find(vec) returns wrong bool.");
            if (found)
            {
               UNIT_ASSERT(mc_iter != mcr.end(), "Find(vec) returns end but shouldn't.");
               UNIT_ASSERT_EQUAL(mc_iter->MCVec(), p.second, "Find(vec) returns wrong iterator.");
            }
            else
            {
               UNIT_ASSERT(mc_iter == mcr.end(), "Find(vec) iterator should've been end() on failure.");
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Assign address to a ModeCombi in the range.
    *
    * Check various implementations, and also check that we can go out-of-range
    * without failure, and without modification of the range (except for the
    * addresses).
    ***************************************************************************/
   struct AssignAddress
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(2, 3);
         std::vector<std::vector<In>> ctrl_mcr =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {0,1}
            ,  {0,2}
            ,  {1,2}
            };
         std::vector<In> ctrl_add(7, -I_1);
         auto assert = detail::CheckQueries(mcr, ctrl_mcr);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);
         assert = detail::CheckAddresses(mcr, ctrl_add);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);

         // Using AssignAddress(Uin, In).
         mcr.AssignAddress(2, 42);
         mcr.AssignAddress(27, 42);
         ctrl_add.at(2) = 42;
         assert = detail::CheckQueries(mcr, ctrl_mcr);
         UNIT_ASSERT(assert.first, "After AssignAddress(Uin,...): "+assert.second);
         assert = detail::CheckAddresses(mcr, ctrl_add);
         UNIT_ASSERT(assert.first, "After AssignAddress(Uin,...): "+assert.second);

         // Using AssignAddress(const_iterator, In).
         mcr.AssignAddress(mcr.begin()+5, 60);
         mcr.AssignAddress(mcr.begin()+1000, 60);
         ctrl_add.at(5) = 60;
         assert = detail::CheckQueries(mcr, ctrl_mcr);
         UNIT_ASSERT(assert.first, "After AssignAddress(const_iterator,...): "+assert.second);
         assert = detail::CheckAddresses(mcr, ctrl_add);
         UNIT_ASSERT(assert.first, "After AssignAddress(const_iterator,...): "+assert.second);

         // Using AssignAddress(const std::vector<In>&, In).
         mcr.AssignAddress(std::vector<In>{0,1}, 144);
         mcr.AssignAddress(std::vector<In>{0,1,2}, 144);
         ctrl_add.at(4) = 144;
         assert = detail::CheckQueries(mcr, ctrl_mcr);
         UNIT_ASSERT(assert.first, "After AssignAddress(const std::vector<In>&,...): "+assert.second);
         assert = detail::CheckAddresses(mcr, ctrl_add);
         UNIT_ASSERT(assert.first, "After AssignAddress(const std::vector<In>&,...): "+assert.second);
      }
   };

   /************************************************************************//**
    * @brief
    *    Set canonical addresses of ModeCombi%s in the range.
    ***************************************************************************/
   struct SetAddressesTest
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
               {  ModeCombi(std::set<In>{}, -1)
               ,  ModeCombi(std::set<In>{0}, -1)
               ,  ModeCombi(std::set<In>{1,2}, -1)
               ,  ModeCombi(std::set<In>{0,1,2}, -1)
               });
         std::vector<Uin> n_modals = {3, 5, 4};
         Uin tot = SetAddresses(mcr, n_modals);
         std::vector<Uin> ctrl = {0, 1, 3, 15, 39};
         for(Uin i = 0; i < mcr.Size(); ++i)
         {
            UNIT_ASSERT_EQUAL(Uin(mcr.GetModeCombi(i).Address()), ctrl.at(i), "Wrong address, i = "+std::to_string(i));
         }
         UNIT_ASSERT_EQUAL(tot, ctrl.back(), "Wrong return value.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check the custom iterators that allows iteration through specific
    *    ModeCombi levels only.
    ***************************************************************************/
   struct SpecificLevelIterators
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(I_5, {5,5,0,5,4,2});
         mcr.Erase
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{0}, -1)
               ,  ModeCombi(std::vector<In>{1}, -1)
               ,  ModeCombi(std::vector<In>{2}, -1)
               ,  ModeCombi(std::vector<In>{3}, -1)
               ,  ModeCombi(std::vector<In>{4}, -1)
               ,  ModeCombi(std::vector<In>{5}, -1)
               }
            );
         std::vector<std::vector<In>> control =
            {  {}          //  0
            ,  {0,1}       //  1
            ,  {0,3}       //  2
            ,  {0,4}       //  3
            ,  {0,5}       //  4
            ,  {1,3}       //  5
            ,  {1,4}       //  6
            ,  {1,5}       //  7
            ,  {3,4}       //  8
            ,  {3,5}       //  9
            ,  {4,5}       // 10
            ,  {0,1,3}     // 11
            ,  {0,1,4}     // 12
            ,  {0,3,4}     // 13
            ,  {1,3,4}     // 14
            ,  {0,1,3,4}   // 15
            };
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);

         // In general Begin(0) and End(GetMaxExciLevel()) shall be equivalent
         // to begin() and end().
         UNIT_ASSERT(mcr.Begin(0) == mcr.begin(), "mcr.Begin(0) != mcr.begin()");
         UNIT_ASSERT(mcr.End(mcr.GetMaxExciLevel()) == mcr.end(), "mcr.End(maxlevel) != mcr.end()");

         // In general End(n) shall equal Begin(n+1), also for ModeCombi levels
         // not in the range.
         for(Uin i = I_0; i < mcr.GetMaxExciLevel()+2; ++i)
         {
            UNIT_ASSERT
               (  mcr.Begin(i+1) == mcr.End(i)
               ,  "Begin("+std::to_string(i+1)+") != End("+std::to_string(i)+")"
               );
         }

         // Then specific tests for this ModeCombiOpRange.
         UNIT_ASSERT_EQUAL(mcr.Begin(0)->MCVec(), control.at(0), "Begin(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.Begin(1)->MCVec(), control.at(1), "Begin(1) failed.");
         UNIT_ASSERT_EQUAL(mcr.Begin(2)->MCVec(), control.at(1), "Begin(2) failed.");
         UNIT_ASSERT_EQUAL(mcr.Begin(3)->MCVec(), control.at(11), "Begin(3) failed.");
         UNIT_ASSERT_EQUAL(mcr.Begin(4)->MCVec(), control.at(15), "Begin(4) failed.");

         UNIT_ASSERT_EQUAL(mcr.End(0)->MCVec(), control.at(1), "End(0) failed.");
         UNIT_ASSERT_EQUAL(mcr.End(1)->MCVec(), control.at(1), "End(1) failed.");
         UNIT_ASSERT_EQUAL(mcr.End(2)->MCVec(), control.at(11), "End(2) failed.");
         UNIT_ASSERT_EQUAL(mcr.End(3)->MCVec(), control.at(15), "End(3) failed.");

         // Loop constructs.
         // Compares distance between pointers with number of ModeCombis of
         // expected size, and number of ModeCombis of that size; all of these
         // should be equal.
         // And should confirm that we really iterate through the right
         // subrange of the ModeCombiOpRange.
         for(Uin n = 0; n <= mcr.GetMaxExciLevel()+2; ++n)
         {
            Uin count = 0;
            Uin bad_count = 0;
            Uin dist = std::distance(mcr.Begin(n), mcr.End(n));
            for(auto it = mcr.Begin(n), end = mcr.End(n); it != end; ++it)
            {
               if (it->Size() == n)
               {
                  ++count;
               }
               else
               {
                  ++bad_count;
               }
            }
            UNIT_ASSERT_EQUAL
               (  count
               ,  dist
               ,  "Wrong count of "+std::to_string(n)+"-ModeCombis."
               );
            UNIT_ASSERT_EQUAL
               (  bad_count
               ,  Uin(0)
               ,  "Wrong count of non-"+std::to_string(n)+"-ModeCombis."
               );
            UNIT_ASSERT_EQUAL
               (  count
               ,  mcr.NumModeCombisWithExcLevel(n)
               ,  "count != NumModeCombisWithExcLevel("+std::to_string(n)+")"
               );
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Are ModeCombi%s contained in range?
    ***************************************************************************/
   struct Contains
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(I_2, I_3);
         std::vector<std::vector<In>> control = {{}, {0}, {1}, {2}, {0,1}, {0,2}, {1,2}};
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);

         UNIT_ASSERT(mcr.Contains(std::vector<In>{0,1}), "Vec; Should contain {0,1} but doesn't.");
         UNIT_ASSERT(!mcr.Contains(std::vector<In>{0,1,2}), "Vec; Shouldn't contain {0,1,2} but does.");

         UNIT_ASSERT(mcr.Contains(ModeCombi(std::vector<In>{0,1},-1)), "MC; Should contain {0,1} but doesn't.");
         UNIT_ASSERT(!mcr.Contains(ModeCombi(std::vector<In>{0,1,2},-1)), "MC; Shouldn't contain {0,1,2} but does.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Are ModeCombi%s contained in range? If so, assign the ref.num. and
    *    address from range.
    ***************************************************************************/
   struct IfContainedAssignAddressAndRefNum
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         mcr.Insert
            (  std::vector<ModeCombi>
               {  ModeCombi(std::vector<In>{},    0) // ref num 0
               ,  ModeCombi(std::vector<In>{1},   6) // ref num 1
               ,  ModeCombi(std::vector<In>{0,1}, 9) // ref num 2
               }
            );
         ModeCombi mc_not_contained(std::vector<In>{0}, -1);
         UNIT_ASSERT_EQUAL(mc_not_contained.Address(), -1, "Initially: mc_not_contained bad Address.");
         UNIT_ASSERT_EQUAL(mc_not_contained.ModeCombiRefNr(), -1, "Initially: mc_not_contained bad ref num.");

         bool contained = mcr.IfContainedAssignAddressAndRefNum(mc_not_contained);
         UNIT_ASSERT(!contained, "Shouldn't contain {0} but does.");
         UNIT_ASSERT_EQUAL(mc_not_contained.Address(), -1, "After: mc_not_contained bad Address.");
         UNIT_ASSERT_EQUAL(mc_not_contained.ModeCombiRefNr(), -1, "After: mc_not_contained bad ref num.");

         ModeCombi mc_contained(std::vector<In>{1}, -1);
         UNIT_ASSERT_EQUAL(mc_contained.Address(), -1, "Initially: mc_contained bad Address.");
         UNIT_ASSERT_EQUAL(mc_contained.ModeCombiRefNr(), -1, "Initially: mc_contained bad ref num.");

         contained = mcr.IfContainedAssignAddressAndRefNum(mc_contained);
         UNIT_ASSERT(contained, "Should contain {1} but doesn't.");
         UNIT_ASSERT_EQUAL(mc_contained.Address(), 6, "After: mc_contained bad Address.");
         UNIT_ASSERT_EQUAL(mc_contained.ModeCombiRefNr(), 1, "After: mc_contained bad ref num.");
      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests ModeCombisFixedNonFixedModes with and without a
    *    restriction range.
    ******************************************************************************/
   struct TestModeCombisFixedNonFixedModes
      :  public cutee::test
   {
      void run() override
      {
         // Fixed modes: {0, 1}
         // Source of modes: {5, 6, 7}
         // Restriction ranges:
         //    -  empty, here we'll use no restriction.
         //    -  the nonempty has no 4,5-mode combinations with mode 7, so these
         //       will be excluded.
         std::vector<ModeCombiOpRange> restrict_range =
            {  ModeCombiOpRange()
            ,  ModeCombiOpRange(4, {4,4,0,0,0,4,4,3})
            };
         std::vector<std::vector<std::vector<In>>> control =
            {  {  {0,1}
               ,  {0,1,5}
               ,  {0,1,6}
               ,  {0,1,7}
               ,  {0,1,5,6}
               ,  {0,1,5,7}
               ,  {0,1,6,7}
               ,  {0,1,5,6,7}
               }
            ,  {  {0,1}
               ,  {0,1,5}
               ,  {0,1,6}
               ,  {0,1,7}
               ,  {0,1,5,6}
               }
            };
         std::vector<In> fixed_modes = {0,1};
         std::vector<In> source_modes = {5, 6, 7};

         for(Uin i = I_0; i < restrict_range.size(); ++i)
         {
            // Assign some non-trivial addresses to restrict_range.at(i).
            auto& rest = restrict_range.at(i);
            for(Uin i = I_0; i < rest.Size(); ++i)
            {
               rest.AssignAddress(i, 100+i);
            }
            ModeCombiOpRange mcr;
            const auto v_mcs = ModeCombisFixedNonFixedModesRestricted
               (  I_0
               ,  source_modes.size()
               ,  source_modes
               ,  fixed_modes
               ,  restrict_range.at(i)
               );
            mcr.Insert(v_mcs);

            // Check the range.
            auto assert = detail::CheckQueries(mcr, control.at(i));
            UNIT_ASSERT(assert.first, "Bad MCR, i = "+std::to_string(i)+": "+assert.second);

            // Check the addresses and ref numbers of v_mcs match the ones in
            // restriction range.
            for(const auto& mc: v_mcs)
            {
               ModeCombiOpRange::const_iterator rest_it;
               if (rest.Find(mc, rest_it))
               {
                  UNIT_ASSERT_EQUAL(mc.Address(), rest_it->Address(), "Wrong address.");
                  UNIT_ASSERT_EQUAL(mc.ModeCombiRefNr(), rest_it->ModeCombiRefNr(), "Wrong ref. num.");
               }
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests the DoesMcrConnectMcs function.
    ***************************************************************************/
   struct TestDoesMcrConnectMcs
      :  public cutee::test
   {
      void run() override
      {
         // Make a range...
         ModeCombiOpRange mcr(I_2, {2,2,1,1});
         std::vector<std::vector<In>> control = {{}, {0}, {1}, {2}, {3}, {0,1}};
         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, "After construction: "+assert.second);

         // Some ModeCombi%s that are _not_ connected by range.
         UNIT_ASSERT
            (  !DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{4,5},-1)
               ,  ModeCombi(std::vector<In>{6,7},-1)
               )
            ,  "{4,5} and {6,7} shouldn't be connected by range."
            );
         UNIT_ASSERT
            (  !DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{2,3,4,5},-1)
               ,  ModeCombi(std::vector<In>{0,1,6},-1)
               )
            ,  "{2,3,4,5} and {0,1,6} shouldn't be connected by range."
            );
         UNIT_ASSERT
            (  !DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{2,4},-1)
               ,  ModeCombi(std::vector<In>{3,4},-1)
               )
            ,  "{2,4} and {3,4} shouldn't be connected by range."
            );
         UNIT_ASSERT
            (  !DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{},-1)
               ,  ModeCombi(std::vector<In>{},-1)
               )
            ,  "{} and {0} shouldn't be connected by range."
            );

         // Some ModeCombi%s that _are_ connected by range.
         UNIT_ASSERT
            (  DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{0},-1)
               ,  ModeCombi(std::vector<In>{0},-1)
               )
            ,  "{0} and {0} should be connected by range."
            );
         UNIT_ASSERT
            (  DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{0,2},-1)
               ,  ModeCombi(std::vector<In>{1,3},-1)
               )
            ,  "{0,2} and {1,3} should be connected by range."
            );
         UNIT_ASSERT
            (  DoesMcrConnectMcs
               (  mcr
               ,  ModeCombi(std::vector<In>{0,4,5},-1)
               ,  ModeCombi(std::vector<In>{1,6,7,8},-1)
               )
            ,  "{0,4,5} and {1,6,7,8} should be connected by range."
            );
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests the BuildCommonMcr function.
    ***************************************************************************/
   struct TestBuildCommonMcr
      :  public cutee::test
   {
      void run() override
      {
         // Make a reference ModeCombiOpRange like:
         //    {{}, {0}, {1}, {2}, {0,1}, {0,2}, {1,2}}
         ModeCombiOpRange ref_mcr(I_2, I_3);

         // ModeCombi has nothing in common with MCR.
         // The resulting MCR should be empty.
         std::vector<std::vector<In>> control;
         auto common_mcr = BuildCommonMcr(ref_mcr, ModeCombi(std::vector<In>{3,4},-1));
         auto assert = detail::CheckQueries(common_mcr, control);
         UNIT_ASSERT(assert.first, "Wrong common MCR (MC: {3,4}): "+assert.second);

         // ModeCombi {0,2,3,5} has some modes in common with MCR.
         // The resulting MCR should be {{0}, {2}, {0,1}, {0,2}, {1,2}}.
         control = {{0}, {2}, {0,1}, {0,2}, {1,2}};
         common_mcr = BuildCommonMcr(ref_mcr, ModeCombi(std::vector<In>{0,2,3,5},-1));
         assert = detail::CheckQueries(common_mcr, control);
         UNIT_ASSERT(assert.first, "Wrong common MCR (MC: {0,2,3,5}): "+assert.second);
      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests the SetToLeftOperRange function.
    *
    * Several subtests:
    *    1. "Left" and "check" ModeCombi are the same. Empty "restriction" range
    *       (so no restriction); add to existing range; no Iso (interaction space
    *       order).
    *    2. "Left" and "check" ModeCombi are the same. "Restriction" range, only
    *       up to 3-mode combinations; don't add to existing, i.e. clear it first;
    *       no Iso (interaction space order).
    *    3. "Left" and "check" are different. "Left" is {}, "check" is a 2-mode
    *       combi. Don't add to existing, no Iso, _do_ check while; one of the
    *       operator range ModeCombi%s are skipped through this check.
    *    4. "Left" and "check" are both {}. Empty "restriction" range (so no
    *       restriction); don't add to existing; use Iso (interaction space
    *       order).
    ******************************************************************************/
   struct TestSetToLeftOperRange
      :  public cutee::test
   {
      void run() override
      {
         // Test 1.
         // mcr_base = {{}, {0}, ..., {5}, {0,1}, ..., {4,5}}
         // left mc  = {2,4}
         // check mc = (same as left)
         // op.range = {{2,4}, {3,4}, {3,5}}
         // restrict = none (empty)
         // rest.bool= true (no effect if range is empty)
         //
         // Result: will add
         //    Due to {2,4}: none
         //    Due to {3,4}: {2,3,4}
         //    Due to {3,5}: ({2,3,4},) {2,4,5}, {2,3,4,5}
         {
            ModeCombiOpRange mcr_base(2, 6);
            std::vector<ModeCombi> vec_mcr_base(mcr_base.begin(), mcr_base.end());
            ModeCombi mc_left(std::vector<In>({2,4}), -1);
            ModeCombiOpRange op_range;
            op_range.Insert
               (  std::vector<ModeCombi>
                  {  ModeCombi(std::vector<In>({2,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,5}), -1)
                  }
               );
            ModeCombiOpRange restr_range;
            bool restr = false;
            bool add   = true;
            std::vector<std::vector<In>> control =
               {  {}
               ,  {0},  {1},  {2},  {3},  {4},  {5}
               ,  {0,1},{0,2},{0,3},{0,4},{0,5}
               ,  {1,2},{1,3},{1,4},{1,5}
               ,  {2,3},{2,4},{2,5}
               ,  {3,4},{3,5}
               ,  {4,5}
               ,  {2,3,4}
               ,  {2,4,5}
               ,  {2,3,4,5}
               };

            bool removed_some = false;
            SetToLeftOperRange
               (  vec_mcr_base
               ,  removed_some
               ,  mc_left
               ,  mc_left
               ,  op_range
               ,  restr_range
               ,  restr
               ,  add
               );

            ModeCombiOpRange test_mcr_base;
            test_mcr_base.Insert(vec_mcr_base);
            auto assert = detail::CheckQueries(test_mcr_base, control);
            UNIT_ASSERT(assert.first, "Test 1; CheckQueries: "+assert.second);

            UNIT_ASSERT(removed_some == false, "Test 1; Removed some but shouldn't have.");

            // Check the addresses and ref numbers match the ones in
            // restriction range.
            for(const auto& mc: vec_mcr_base)
            {
               ModeCombiOpRange::const_iterator rest_it;
               if (restr_range.Find(mc, rest_it))
               {
                  UNIT_ASSERT_EQUAL(mc.Address(), rest_it->Address(), "Test 1; Wrong address.");
                  UNIT_ASSERT_EQUAL(mc.ModeCombiRefNr(), rest_it->ModeCombiRefNr(), "Test 1; Wrong ref. num.");
               }
            }
         }

         // Test 2.
         // mcr_base = {{}, {0}, ..., {5}, {0,1}, ..., {4,5}} (but is cleared)
         // left mc  = {2,4}
         // check mc = (same as left)
         // op.range = {{2,4}, {3,4}, {3,5}}
         // restrict = only up to three-mode couplings
         // rest.bool= true (this is default)
         // add to   = false (this is default)
         //
         // Result: will clear everything, then add
         //    Due to {2,4}: {}, {2}, {4}, {2,4}
         //    Due to {3,4}: {2}, {2,3}, {2,4}, {2,3,4}
         //    Due to {3,5}: {2,4}, {2,3,4}, {2,4,5}, {2,3,4,5}
         // except {2,3,4,5} is not inserted due to restriction.
         {
            ModeCombiOpRange mcr_base(2, 6);
            std::vector<ModeCombi> vec_mcr_base(mcr_base.begin(), mcr_base.end());
            ModeCombi        mc_left(std::vector<In>({2,4}), -1);
            ModeCombiOpRange op_range;
            op_range.Insert
               (  std::vector<ModeCombi>
                  {  ModeCombi(std::vector<In>({2,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,5}), -1)
                  }
               );
            ModeCombiOpRange restr_range(3, 6);
            std::vector<std::vector<In>> control =
               {  {}
               ,  {2}
               ,  {4}
               ,  {2,3}
               ,  {2,4}
               ,  {2,3,4}
               ,  {2,4,5}
               };
            // Assign some non-trivial addresses to restrict_range.at(i).
            for(Uin i = I_0; i < restr_range.Size(); ++i)
            {
               restr_range.AssignAddress(i, 100+i);
            }

            bool removed_some = false;
            SetToLeftOperRange
               (  vec_mcr_base
               ,  removed_some
               ,  mc_left
               ,  mc_left
               ,  op_range
               ,  restr_range
               );

            ModeCombiOpRange test_mcr_base;
            test_mcr_base.Insert(vec_mcr_base);
            auto assert = detail::CheckQueries(test_mcr_base, control);
            UNIT_ASSERT(assert.first, "Test 2; CheckQueries: "+assert.second);
            UNIT_ASSERT(removed_some == false, "Test 2; Removed some but shouldn't have.");

            // Check the addresses and ref numbers match the ones in
            // restriction range.
            for(const auto& mc: vec_mcr_base)
            {
               ModeCombiOpRange::const_iterator rest_it;
               if (restr_range.Find(mc, rest_it))
               {
                  UNIT_ASSERT_EQUAL(mc.Address(), rest_it->Address(), "Test 2; Wrong address.");
                  UNIT_ASSERT_EQUAL(mc.ModeCombiRefNr(), rest_it->ModeCombiRefNr(), "Test 2; Wrong ref. num.");
               }
            }
         }

         // Test 3.
         // mcr_base = {{}, {0}, ..., {5}, {0,1}, ..., {4,5}} (but is cleared)
         // left mc  = {} (reference state)
         // check mc = {2,5}
         // op.range = {{1,4}, {3,4}, {3,5}}
         // restrict = {{}, {0}, {1}, {2}, {0,1}, {0,2}, {1,2}}
         // rest.bool= false
         // add to   = false
         // iso      = false
         // iso num  = 0 (no effect when iso is false)
         // check    = true
         //
         // Result: will clear everything in mcr.
         // Then form a common mcr between check and "restrict" (ModeCombi%s from
         // range containing either 2, 5 or both):
         //    {2}, {0,2}, {1,2}
         // Then for each operator ModeCombi, is is connected to any of the above
         // or to {2,5}?  If so add (based on the left ModeCombi, {}), otherwise
         // skip (and set "removed" bool = true):
         //    {1,4}: {}, {1}, {4}, {1,4} (connected to {2,5} through {1,2})
         //    {3,4}: not connected to any, thus skipped
         //    {3,5}: {}, {3}, {5}, {3,5} (connected to {2,5} directly)
         {
            ModeCombiOpRange mcr_base(2, 6);
            std::vector<ModeCombi> vec_mcr_base(mcr_base.begin(), mcr_base.end());
            ModeCombi        mc_left(std::vector<In>({}), -1);
            ModeCombi        mc_check(std::vector<In>({2,5}), -1);
            ModeCombiOpRange op_range;
            op_range.Insert
               (  std::vector<ModeCombi>
                  {  ModeCombi(std::vector<In>({1,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,4}), -1)
                  ,  ModeCombi(std::vector<In>({3,5}), -1)
                  }
               );
            ModeCombiOpRange restr_range(2, {2,2,2,0,0,0});
            // Assign some non-trivial addresses to restrict_range.at(i).
            for(Uin i = I_0; i < restr_range.Size(); ++i)
            {
               restr_range.AssignAddress(i, 100+i);
            }
            bool restr = false;
            bool add   = false;
            bool iso   = false;
            In iso_ord = I_0;
            bool check_while = true;
            std::vector<std::vector<In>> control =
               {  {}
               ,  {1}
               ,  {3}
               ,  {4}
               ,  {5}
               ,  {1,4}
               ,  {3,5}
               };

            bool removed_some = false;
            SetToLeftOperRange
               (  vec_mcr_base
               ,  removed_some
               ,  mc_left
               ,  mc_check
               ,  op_range
               ,  restr_range
               ,  restr
               ,  add
               ,  iso
               ,  iso_ord
               ,  check_while
               );

            ModeCombiOpRange test_mcr_base;
            test_mcr_base.Insert(vec_mcr_base);
            auto assert = detail::CheckQueries(test_mcr_base, control);
            UNIT_ASSERT(assert.first, "Test 3; CheckQueries: "+assert.second);
            UNIT_ASSERT(removed_some == true, "Test 3; Says I didn't remove any but I did.");

            // Check the addresses and ref numbers match the ones in
            // restriction range.
            for(const auto& mc: vec_mcr_base)
            {
               ModeCombiOpRange::const_iterator rest_it;
               if (restr_range.Find(mc, rest_it))
               {
                  UNIT_ASSERT_EQUAL(mc.Address(), rest_it->Address(), "Test 3; Wrong address.");
                  UNIT_ASSERT_EQUAL(mc.ModeCombiRefNr(), rest_it->ModeCombiRefNr(), "Test 3; Wrong ref. num.");
               }
            }
         }

         // Test 4.
         // mcr_base = {{}, {0}, ..., {5}, {0,1}, ..., {4,5}} (but is cleared)
         // left mc  = {} (reference state)
         // check mc = {} (no effect)
         // op.range = {{0,1}, {2}, {3}}
         // restrict = {} (empty: no effect)
         // rest.bool= true (no effect when empty range)
         // add to   = false
         // iso      = true
         // iso num  = 2
         // check    = false (default)
         //
         // Result: will clear everything in mcr.
         // Then build up unrestricted range (from the "left" ModeCombi {}):
         //    Due to {0,1}: {}, {0}, {1}, {0,1}
         //    Due to {2}:   {}, {2}
         //    Due to {3}:   {}, {3}
         // So Iso = 1 result would be: {}, {0}, {1}, {2}, {3}, {0,1}.
         // Then repeat _once_ more since interaction space order is 2.
         // Loop through each MC in Iso = 1 range and build up using oper. range.
         //    {}: Same as already there from Iso = 1.
         //    {0}:     {0,1}:   {0}, {0,1}
         //             {2}:     {0}, {0,2}
         //             {3}:     {0}, {0,3}
         //    {1}:     {0,1}:   {1}, {0,1}
         //             {2}:     {1}, {1,2}
         //             {3}:     {1}, {1,3}
         //    {2}:     {0,1}:   {2}, {0,2}, {1,2}, {0,1,2}
         //             {2}:     {2}
         //             {3}:     {2}, {2,3}
         //    {3}:     {0,1}:   {3}, {0,3}, {1,3}, {0,1,3}
         //             {2}:     {3}, {2,3}
         //             {3}:     {3}
         //    {0,1}:   {0,1}:   {0,1}
         //             {2}:     {0,1}, {0,1,2}
         //             {3}:     {0,1}, {0,1,3}
         // Final result:
         //    {}, {0}, {1}, {2}, {3},
         //    {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}
         //    {0,1,2}, {0,1,3},
         // But {0,2,3}, {1,2,3}, {0,1,2,3} not included, they would be Iso = 3.
         {
            ModeCombiOpRange mcr_base(2, 6);
            std::vector<ModeCombi> vec_mcr_base(mcr_base.begin(), mcr_base.end());
            ModeCombi mc_left(std::vector<In>({}), -1);
            ModeCombiOpRange op_range;
            op_range.Insert
               (  std::vector<ModeCombi>
                  {  ModeCombi(std::vector<In>({0,1}), -1)
                  ,  ModeCombi(std::vector<In>({2}), -1)
                  ,  ModeCombi(std::vector<In>({3}), -1)
                  }
               );
            ModeCombiOpRange restr_range;
            bool restr = true;
            bool add   = false;
            bool iso   = true;
            In iso_ord = I_2;
            std::vector<std::vector<In>> control =
               {  {}
               ,  {0}
               ,  {1}
               ,  {2}
               ,  {3}
               ,  {0,1}
               ,  {0,2}
               ,  {0,3}
               ,  {1,2}
               ,  {1,3}
               ,  {2,3}
               ,  {0,1,2}
               ,  {0,1,3}
               };

            bool removed_some = false;
            SetToLeftOperRange
               (  vec_mcr_base
               ,  removed_some
               ,  mc_left
               ,  mc_left
               ,  op_range
               ,  restr_range
               ,  restr
               ,  add
               ,  iso
               ,  iso_ord
               );

            ModeCombiOpRange test_mcr_base;
            test_mcr_base.Insert(vec_mcr_base);
            auto assert = detail::CheckQueries(test_mcr_base, control);
            UNIT_ASSERT(assert.first, "Test 4; CheckQueries: "+assert.second);
            UNIT_ASSERT(removed_some == false, "Test 4; Removed some but shouldn't have.");

            // Check the addresses and ref numbers match the ones in
            // restriction range.
            for(const auto& mc: vec_mcr_base)
            {
               ModeCombiOpRange::const_iterator rest_it;
               if (restr_range.Find(mc, rest_it))
               {
                  UNIT_ASSERT_EQUAL(mc.Address(), rest_it->Address(), "Test 4; Wrong address.");
                  UNIT_ASSERT_EQUAL(mc.ModeCombiRefNr(), rest_it->ModeCombiRefNr(), "Test 4; Wrong ref. num.");
               }
            }
         }
      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests SubSetSets.
    ******************************************************************************/
   struct TestSubSetSets
      :  public cutee::test
   {
      void run() override
      {
         // 2-mode couplings and addresses, but {2,3} is missing, so {{0,1},{2,3}}
         // should not be included in the end.
         ModeCombiOpRange mcr_superset;
         mcr_superset.Insert
            (  std::vector<ModeCombi>
                  (  {  ModeCombi(std::vector<In>({}),     0)
                     ,  ModeCombi(std::vector<In>({0}),    1)
                     ,  ModeCombi(std::vector<In>({1}),    4)
                     ,  ModeCombi(std::vector<In>({2}),    7)
                     ,  ModeCombi(std::vector<In>({3}),   10)
                     ,  ModeCombi(std::vector<In>({0,1}), 13)
                     ,  ModeCombi(std::vector<In>({0,2}), 22)
                     ,  ModeCombi(std::vector<In>({0,3}), 31)
                     ,  ModeCombi(std::vector<In>({1,2}), 40)
                     ,  ModeCombi(std::vector<In>({1,3}), 49)
                     }
                  )
            );

         ModeCombi mc_to_partition(std::vector<In>{0,1,2,3}, -I_1);

         // With min_level = 2.
         {
            std::vector<std::vector<ModeCombi>> vec_vec_mcs;
            Uin min_level = I_2;
            SubSetSets(mc_to_partition, vec_vec_mcs, mcr_superset, min_level);
            std::vector<std::vector<std::vector<In>>> ctrl_mc =
               {  {  {0,2}
                  ,  {1,3}
                  }
               ,  {  {0,3}
                  ,  {1,2}
                  }
               };
            std::vector<std::vector<In>> ctrl_address =
               {  {  22
                  ,  49
                  }
               ,  {  31
                  ,  40
                  }
               };
            std::vector<std::vector<In>> ctrl_refnums =
               {  {  6
                  ,  9
                  }
               ,  {  7
                  ,  8
                  }
               };
            auto assert = detail::CheckPartitionings(vec_vec_mcs, ctrl_mc, ctrl_address, ctrl_refnums);
            UNIT_ASSERT(assert.first, "Test 1: "+assert.second);
         }

         // With min_level = 2 (as above) but a more restricted superset.
         {
            ModeCombiOpRange local_superset;
            local_superset.Insert
               (  std::vector<ModeCombi>
                     (  {  ModeCombi(std::vector<In>({}),     0)
                        ,  ModeCombi(std::vector<In>({0,1}), 13)
                        ,  ModeCombi(std::vector<In>({0,2}), 22)
                        ,  ModeCombi(std::vector<In>({0,3}), 31)
                        ,  ModeCombi(std::vector<In>({1,2}), 40)
                        ,  ModeCombi(std::vector<In>({1,3}), 49)
                        }
                     )
               );
            std::vector<std::vector<ModeCombi>> vec_vec_mcs;
            Uin min_level = I_2;
            SubSetSets(mc_to_partition, vec_vec_mcs, local_superset, min_level);
            std::vector<std::vector<std::vector<In>>> ctrl_mc =
               {  {  {0,2}
                  ,  {1,3}
                  }
               ,  {  {0,3}
                  ,  {1,2}
                  }
               };
            std::vector<std::vector<In>> ctrl_address =
               {  {  22
                  ,  49
                  }
               ,  {  31
                  ,  40
                  }
               };
            std::vector<std::vector<In>> ctrl_refnums =
               {  {  2
                  ,  5
                  }
               ,  {  3
                  ,  4
                  }
               };
            auto assert = detail::CheckPartitionings(vec_vec_mcs, ctrl_mc, ctrl_address, ctrl_refnums);
            UNIT_ASSERT(assert.first, "Test 1a: "+assert.second);
         }


         // With min_level = 1.
         {
            std::vector<std::vector<ModeCombi>> vec_vec_mcs;
            Uin min_level = I_1;
            SubSetSets(mc_to_partition, vec_vec_mcs, mcr_superset, min_level);
            std::vector<std::vector<std::vector<In>>> ctrl_mc =
               {  {  {2},     {3},     {0,1}          }
               ,  {  {0,2},   {1,3}                   }
               ,  {  {1},     {3},     {0,2}          }
               ,  {  {0,3},   {1,2}                   }
               ,  {  {0},     {3},     {1,2}          }
               ,  {  {1},     {2},     {0,3}          }
               ,  {  {0},     {2},     {1,3}          }
               ,  {  {0},     {1},     {2},     {3}   }
               };
            std::vector<std::vector<In>> ctrl_address =
               {  {  7,    10,   13       }
               ,  {  22,   49             }
               ,  {  4,    10,   22       }
               ,  {  31,   40             }
               ,  {  1,    10,   40       }
               ,  {  4,    7,    31       }
               ,  {  1,    7,    49       }
               ,  {  1,    4,    7,    10 }
               };
            std::vector<std::vector<In>> ctrl_refnums =
               {  {  3, 4, 5     }
               ,  {  6, 9        }
               ,  {  2, 4, 6     }
               ,  {  7, 8        }
               ,  {  1, 4, 8     }
               ,  {  2, 3, 7     }
               ,  {  1, 3, 9     }
               ,  {  1, 2, 3, 4  }
               };
            auto assert = detail::CheckPartitionings(vec_vec_mcs, ctrl_mc, ctrl_address, ctrl_refnums);
            UNIT_ASSERT(assert.first, "Test 2: "+assert.second);
         }

      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests UpdateAdga.
    *
    * The file contents and expected output taken from the pes_Adga_Update
    * test, hence the verbosity...
    *
    * The test starts with a 6-mode, up-to-3-mode-combination
    * ModeCombiOpRange, then uses the UpdateAdga() function to erase some of
    * the 2-mode combinations (while leaving the 3-mode ones as they are).
    ******************************************************************************/
   struct TestUpdateAdga
      :  public cutee::test
   {
      std::string mIntFile = "MaxInts_all";
      std::string mPropFile = "prop_no_1.mop";
      std::vector<std::string> mIntFileContents =
         {  "6"
         ,  "0 11"
         ,  "0 1.0000000000000000000000E+00"
         ,  "1 9.8667302949637969433638E-01"
         ,  "2 2.4200889141784487357256E+00"
         ,  "3 2.8799158034513934190102E+00"
         ,  "4 9.1371005621939058727321E+00"
         ,  "5 1.0471137719759404660635E+01"
         ,  "6 4.2408896162706874122250E+01"
         ,  "7 4.7238139795424636702137E+01"
         ,  "8 2.3437962017189056496136E+02"
         ,  "9 1.3966426237342316474255E-02"
         ,  "10 7.4530133130732506563376E-02"
         ,  "1 11"
         ,  "0 9.9999999999999977795540E-01"
         ,  "1 9.9341034057526578227026E-01"
         ,  "2 2.4584279469580292420972E+00"
         ,  "3 2.9396450385668173588272E+00"
         ,  "4 9.4277349732652950820011E+00"
         ,  "5 1.0850156721859097075367E+01"
         ,  "6 4.4489244181330157346110E+01"
         ,  "7 4.9742569363779715274632E+01"
         ,  "8 2.5021509172205537652189E+02"
         ,  "9 1.4729672225443631722319E-02"
         ,  "10 7.6621633929842600241322E-02"
         ,  "2 11"
         ,  "0 1.0000000000000002220446E+00"
         ,  "1 1.0006087547291271366845E+00"
         ,  "2 2.5044397480226443519769E+00"
         ,  "3 3.0084040658541382207147E+00"
         ,  "4 9.7918916051196323024897E+00"
         ,  "5 1.1312405494728785981806E+01"
         ,  "6 4.7220038726048159105630E+01"
         ,  "7 5.2953129683283123085857E+01"
         ,  "8 2.7197584046917580735681E+02"
         ,  "9 1.7482800520393516202633E-02"
         ,  "10 8.3622659755471909215352E-02"
         ,  "3 13"
         ,  "0 1.0000000000000011102230E+00"
         ,  "1 1.0037037120730216699371E+00"
         ,  "2 2.6792120555084522059985E+00"
         ,  "3 3.2466202969297359715029E+00"
         ,  "4 1.1705680002265843597797E+01"
         ,  "5 1.6712192050128319209534E+01"
         ,  "6 6.5210498613404155321405E+01"
         ,  "7 1.2284046015883002667124E+02"
         ,  "8 4.4393498212234487709793E+02"
         ,  "9 1.0175949148271953390577E+03"
         ,  "10 3.5857612641787713982922E+03"
         ,  "11 1.9801453570498478429229E-02"
         ,  "12 8.8711019358484896613071E-02"
         ,  "4 13"
         ,  "0 1.0000000000000006661338E+00"
         ,  "1 1.0094753782597851721192E+00"
         ,  "2 2.9290259540930478898701E+00"
         ,  "3 4.0505377559133908249578E+00"
         ,  "4 1.4600671888063230241528E+01"
         ,  "5 2.9086847482811744924902E+01"
         ,  "6 9.4558709224106095803108E+01"
         ,  "7 2.3330137036590548405002E+02"
         ,  "8 7.5142185536501642673102E+02"
         ,  "9 2.1376492867837118865282E+03"
         ,  "10 7.0843192032184597337618E+03"
         ,  "11 3.1588914020511241198541E-02"
         ,  "12 1.1168756318674283312120E-01"
         ,  "5 11"
         ,  "0 1.0000000000000006661338E+00"
         ,  "1 9.7657059280972102754959E-01"
         ,  "2 2.3534187104205717133709E+00"
         ,  "3 2.7875116175089638126394E+00"
         ,  "4 8.6328321678041941567017E+00"
         ,  "5 9.8674194499930454327341E+00"
         ,  "6 3.8767266258340150386630E+01"
         ,  "7 4.3151066698616013184164E+01"
         ,  "8 2.0638820707821099631474E+02"
         ,  "9 3.6299670511010115325767E-02"
         ,  "10 1.1969714901750964919369E-01"
         };
      std::vector<std::string> mPropFileContents =
         {  "#0MIDASOPERATOR"
         ,  "#1MODENAMES"
         ,  "Q0 Q1 Q2 Q3 Q4 Q5"
         ,  "#1SCALEFACTORS"
         ,  "7.3539356486540793933493E-02 7.6117675885324281859212E-02 8.3676815619175587412926E-02 9.0019664124989171027913E-02 1.1551942940125486947167E-01 1.1691100692824352158539E-01"
         ,  "#1OPERATORTERMS"
         ,  " 2.0807933127622665848969E-07 Q^1(Q0)"
         ,  " 2.7118900861612957667313E-03 Q^2(Q0)"
         ,  "-8.4300377558152332350852E-10 Q^3(Q0)"
         ,  " 2.8767374851475872843919E-05 Q^4(Q0)"
         ,  "-2.3686616312703231763918E-11 Q^5(Q0)"
         ,  "-9.1718725200157294700912E-07 Q^6(Q0)"
         ,  " 6.5406610079666260503951E-13 Q^7(Q0)"
         ,  " 1.1915090976469813874149E-08 Q^8(Q0)"
         ,  " 1.3187114011424060716832E-06 Q^1(Q1)"
         ,  " 2.8888959469175350992798E-03 Q^2(Q1)"
         ,  "-5.2740844462312354911184E-09 Q^3(Q1)"
         ,  " 1.6512034022628207499597E-05 Q^4(Q1)"
         ,  "-2.1431713126540019614301E-11 Q^5(Q1)"
         ,  "-3.9136271011313677673418E-07 Q^6(Q1)"
         ,  " 1.0850254990112934902977E-12 Q^7(Q1)"
         ,  " 3.5649528917147686017772E-09 Q^8(Q1)"
         ,  " 9.7364320056440129772662E-05 Q^1(Q2)"
         ,  " 3.4890324931235276728625E-03 Q^2(Q2)"
         ,  " 2.3897009700733094414698E-05 Q^3(Q2)"
         ,  " 1.4024412418429980052830E-06 Q^4(Q2)"
         ,  "-3.1135831118227827853193E-07 Q^5(Q2)"
         ,  "-1.0546023015851796095468E-07 Q^6(Q2)"
         ,  " 4.1024521550077616612604E-09 Q^7(Q2)"
         ,  " 6.2851332589201198805330E-10 Q^8(Q2)"
         ,  "-1.1324317354025314466201E-04 Q^1(Q3)"
         ,  " 4.0372904741490757071776E-03 Q^2(Q3)"
         ,  " 4.2267794696945304010888E-04 Q^3(Q3)"
         ,  " 2.9980866015598406359999E-05 Q^4(Q3)"
         ,  " 1.6905315182244996623229E-06 Q^5(Q3)"
         ,  " 8.4988319233033704849789E-08 Q^6(Q3)"
         ,  " 4.1541023920124047538044E-09 Q^7(Q3)"
         ,  " 2.0145549861134330951740E-10 Q^8(Q3)"
         ,  " 9.9158887436760133294797E-12 Q^9(Q3)"
         ,  " 1.8377250784073795612447E-13 Q^10(Q3)"
         ,  "-9.1737117459694266668503E-05 Q^1(Q4)"
         ,  " 6.6718391491816103622270E-03 Q^2(Q4)"
         ,  " 1.0231612373568467130869E-03 Q^3(Q4)"
         ,  " 9.9132479170555704017787E-05 Q^4(Q4)"
         ,  " 8.2352929798735347022287E-06 Q^5(Q4)"
         ,  " 6.1762251795782069363483E-07 Q^6(Q4)"
         ,  " 4.5324483960971790469374E-08 Q^7(Q4)"
         ,  " 3.2369322000907377703035E-09 Q^8(Q4)"
         ,  " 2.2920891569225515342623E-10 Q^9(Q4)"
         ,  " 1.1371840691908107010694E-11 Q^10(Q4)"
         ,  " 1.3095598677959610185381E-06 Q^1(Q5)"
         ,  " 6.8358243620712782345761E-03 Q^2(Q5)"
         ,  " 3.3423825110552173885516E-06 Q^3(Q5)"
         ,  " 1.1449907826347828813333E-04 Q^4(Q5)"
         ,  " 4.7372259630194467010161E-08 Q^5(Q5)"
         ,  " 7.0420064430826628508068E-07 Q^6(Q5)"
         ,  " 4.4450075342529732071669E-10 Q^7(Q5)"
         ,  " 4.2798200759359302912171E-09 Q^8(Q5)"
         ,  "#0MIDASOPERATOREND"
         };

      //! Do the actual writing to file.
      static void WriteVecOfStrings(const std::string&, const std::vector<std::string>&);

      //! Write the MaxInts_all and prop_no_1.mop files (taken from the
      //pes_Adga_Update test).
      void setup() override
      {
         midas::filesystem::Mkdir("analysis");
         WriteVecOfStrings(mIntFile, mIntFileContents);
         WriteVecOfStrings(mPropFile, mPropFileContents);
      }

      //! Delete files again.
      void teardown() override
      {
         detail::RemoveFile(mIntFile);
         detail::RemoveFile(mPropFile);
      }

      void run() override
      {
         std::vector<Nb> thresholds =
            {  8.0e-03
            ,  8.0e-03
            ,  0.0e+00
            };
         ModeCombiOpRange mcr(3, 6);
         UpdateAdga(mcr, 2, thresholds, ".", "./analysis/", ".");
         std::vector<std::vector<In>> control =
            {  {}
            ,  {0}
            ,  {1}
            ,  {2}
            ,  {3}
            ,  {4}
            ,  {5}
            ,  {2,3}
            ,  {2,4}
            ,  {2,5}
            ,  {3,4}
            ,  {3,5}
            ,  {4,5}
            ,  {0,1,2}
            ,  {0,1,3}
            ,  {0,1,4}
            ,  {0,1,5}
            ,  {0,2,3}
            ,  {0,2,4}
            ,  {0,2,5}
            ,  {0,3,4}
            ,  {0,3,5}
            ,  {0,4,5}
            ,  {1,2,3}
            ,  {1,2,4}
            ,  {1,2,5}
            ,  {1,3,4}
            ,  {1,3,5}
            ,  {1,4,5}
            ,  {2,3,4}
            ,  {2,3,5}
            ,  {2,4,5}
            ,  {3,4,5}
            };

         auto assert = detail::CheckQueries(mcr, control);
         UNIT_ASSERT(assert.first, assert.second);
      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests SizeOut.
    *
    * It's hard to really test this one, since sizes could probably be
    * implementation specific. We'll just test it outputs _something_.
    ******************************************************************************/
   struct TestSizeOut
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(3, 6);
         std::stringstream ss;
         Uin size = mcr.SizeOut(ss);

         //Mout << ss.str() << std::endl;
         UNIT_ASSERT(size > I_0, "Expected non-zero byte size.");
         UNIT_ASSERT(!ss.str().empty(), "Expected non-empty size summary.");
      }
   };

   /***************************************************************************//**
    * @brief
    *    Tests operator<< for ModeCombiOpRange.
    ******************************************************************************/
   struct OperatorOstream
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr(2, 3);
         std::stringstream ss;
         ss << '\n' << mcr;
         std::string s_control = std::string("\n")
            + "      0   () Address: -1 RefNr:   0\n"
            + "      1   (0) Address: -1 RefNr:   1\n"
            + "      2   (1) Address: -1 RefNr:   2\n"
            + "      3   (2) Address: -1 RefNr:   3\n"
            + "      4   (0,1) Address: -1 RefNr:   4\n"
            + "      5   (0,2) Address: -1 RefNr:   5\n"
            + "      6   (1,2) Address: -1 RefNr:   6\n"
            ;
         UNIT_ASSERT_EQUAL(ss.str(), s_control, "Wrong operator<< output.");
      }
   };

} /* namespace midas::test::modecombioprange */

#endif/*MODECOMBIOPRANGETEST_H_INCLUDED*/
