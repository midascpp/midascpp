/**
 *******************************************************************************
 * 
 * @file    VibCalcTestUtilsTest.h
 * @date    13-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef VIBCALCTESTUTILSTEST_H_INCLUDED
#define VIBCALCTESTUTILSTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "ni/OneModeInt.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "vscf/Vscf.h"
#include "vcc/Vcc.h"
#include "test/util/OpDefsForTesting.h"
#include "test/CuteeInterface.h"
#include "vcc/ModalIntegrals.h"

namespace midas::test::vibcalctestutils
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Test base for running VSCF and VCC and checks energies.
    ***************************************************************************/
   struct VscfVccTestBase
      :  public cutee::test
   {
      public:
         void setup() override
         {
            gOperatorDefs.ReSet();
         }

         void teardown() override
         {
            gOperatorDefs.ReSet();

            // The tensor solver outputs some '.' for iterations. Flush them.
            if (NlTensorSolver())
            {
               Mout << std::endl;
            }
         }

         void run() override
         {
            // Setup.
            OpDef opdef = GetOpDef();
            BasDef basdef = GetBasDef(opdef);
            OneModeInt omi = midas::test::util::SetupOneModeInts(opdef, basdef);
            VccCalcDef calcdef = SetupVccCalcDef();

            // VSCF.
            Vcc calc(&calcdef, &opdef, &basdef, &omi);
            midas::test::util::RunVscf(calc);
            UNIT_ASSERT(calc.Converged(), "VSCF not converged.");
            UNIT_ASSERT_FEQUAL_PREC(calc.GetEtot(), EVscf(), 10, "Wrong VSCF energy.");

            // VCC.
            midas::test::util::RunVcc(calc);
            UNIT_ASSERT(calc.GsCalcConverged(), "VCC not converged.");
            UNIT_ASSERT_FEQUAL_PREC(calc.GsFinalEnergy(), EVcc(), 10, "Wrong VCC energy.");
         }

      protected:
         VccCalcDef SetupVccCalcDef() const
         {
            return midas::test::util::SetupVccCalcDef(GetNModals(), GetMaxExci(), NlTensorSolver());
         }

         virtual OpDef GetOpDef() const = 0;
         virtual BasDef GetBasDef(const OpDef&) const = 0;
         virtual Nb EVscf() const = 0;
         virtual Nb EVcc() const = 0;
         virtual Uin GetMaxExci() const = 0;
         virtual bool NlTensorSolver() const = 0;
         virtual const std::vector<In>& GetOccVec() const = 0;
         virtual const std::vector<In>& GetNModals() const = 0;
   };


   /************************************************************************//**
    * @brief
    *    Test base for running VSCF and VCC and checks energies.
    ***************************************************************************/
   struct WaterHoBas_ir_h2o_vci
      :  public VscfVccTestBase
   {
      public:
         WaterHoBas_ir_h2o_vci
            (  Uin aCoupling
            ,  Uin aNumPrimBas
            ,  Uin aMaxExci
            ,  Uin aNModals
            ,  bool aNlTensorSolver
            ,  Nb aEVscfExpect
            ,  Nb aEVccExpect
            )
            :  mCoupling(aCoupling)
            ,  mNumPrimBas(aNumPrimBas)
            ,  mMaxExci(aMaxExci)
            ,  mNlTensorSolver(aNlTensorSolver)
            ,  mEVscfExpect(aEVscfExpect)
            ,  mEVccExpect(aEVccExpect)
            ,  mNModals(mOccVec.size(), aNModals)
         {
         }

      protected:
         Uin mCoupling;
         Uin mNumPrimBas;
         Uin mMaxExci;
         bool mNlTensorSolver;
         Nb mEVscfExpect;
         Nb mEVccExpect;
         std::vector<In> mOccVec = {0,0,0};
         std::vector<In> mNModals;

         OpDef GetOpDef() const override
         {
            return midas::test::util::WaterH0_ir_h2o_vci(mCoupling, true, false);
         }

         BasDef GetBasDef(const OpDef& arOpDef) const override
         {
            return midas::test::util::HoBasis(arOpDef, mNumPrimBas, false);
         }

         Nb EVscf() const override {return mEVscfExpect;}
         Nb EVcc() const override {return mEVccExpect;}
         Uin GetMaxExci() const override {return mMaxExci;}
         bool NlTensorSolver() const override {return mNlTensorSolver;}
         const std::vector<In>& GetOccVec() const override {return mOccVec;}
         const std::vector<In>& GetNModals() const override {return mNModals;}

   };

} /* namespace midas::test::vibcalctestutils */

#endif/*VIBCALCTESTUTILSTEST_H_INCLUDED*/
