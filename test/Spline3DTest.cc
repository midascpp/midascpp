#include "test/Spline3DTestCase.h"

namespace midas::test
{

void Spline3DTest()
{
   cutee::suite suite("Spline3D test");
   suite.add_test<Spline3DTestCase>("Spline3D test");
   RunSuite(suite);
}

} /* namespace midas::test */
