/**
 *******************************************************************************
 * 
 * @file    Vcc2TransTest.h
 * @date    23-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef VCC2TRANSTEST_H_INCLUDED
#define VCC2TRANSTEST_H_INCLUDED

#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Random.h"
#include "test/util/TransTestUtils.h"
#include "util/type_traits/Complex.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepTransformers.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransRJac.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"

#include "util/CallStatisticsHandler.h"

#include "test/CuteeInterface.h"

namespace midas::test::vcc2trans
{
   using midas::util::matrep::MatRepVibOper;
   template<typename T> using param_t = T;
   template<typename T> using coef_t = midas::type_traits::RealTypeT<T>;
   template<typename T> using absval_t = midas::type_traits::RealTypeT<T>;
   template<typename T> using mat_t = GeneralMidasMatrix<T>;
   template<typename T> using vec_t = GeneralMidasVector<T>;
   template<typename T> using datacont_t = GeneralDataCont<T>;
   constexpr Uin gen_rel_diff_norm_max_ulps = 20;

   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Check that the util::GetOpDefFromCoefs gives the expected
    *    OpDef, otherwise anything can happen in the other tests.
    ***************************************************************************/
   template
      <  typename T
      >
   struct DetailGetOpDefTest
      :  public cutee::test
   {
      private:
         using v_coef_t = std::vector<std::vector<coef_t<T>>>;
         using ctrl_t = std::vector
               <  std::tuple
                  <  coef_t<T>
                  ,  std::vector<GlobalModeNr>
                  ,  std::vector<LocalModeNr>
                  ,  std::vector<LocalOperNr>
                  >
               >;

         void TestImpl
            (  const std::string& arDescriptor
            ,  const OpDef& arOpDef
            ,  const std::vector<Uin>& arNumOpersGlobModes
            ,  const ctrl_t& arCtrl
            ,  const std::vector<GlobalModeNr>& arLoc2Glob
            )
         {
            const std::string s = "("+arDescriptor+") ";

            const Uin ctrl_n_terms = arCtrl.size();
            const Uin ctrl_n_modes = arNumOpersGlobModes.size();
            std::vector<Uin> ctrl_act_terms(ctrl_n_modes, 0);
            for(const auto& term: arCtrl)
            {
               for(const auto& m: std::get<2>(term))
               {
                  try
                  {
                     ++ctrl_act_terms.at(m);
                  }
                  catch(const std::out_of_range& oor)
                  {
                     MIDASERROR(s+"arCtrl mode out of range: "+std::string(oor.what()));
                  }
               }
            }
            std::vector<Uin> ctrl_eff_num_opers;
            const auto& modes_contained = arOpDef.GetModeCombiOpRange().ModesContained();
            for(In m = 0; m < arNumOpersGlobModes.size(); ++m)
            {
               if (std::find(modes_contained.begin(), modes_contained.end(), m) != modes_contained.end())
               {
                  // arNumOpersGlobModes + 1, because it implicitly inserts the 1-mode
                  // identity operator (Q^0) for each mode.
                  ctrl_eff_num_opers.emplace_back(arNumOpersGlobModes.at(m) + 1);
               }
               else
               {
                  // Just 1 (the implicit Q^0), but nothing else because the
                  // mode is not in the OpDef range.
                  ctrl_eff_num_opers.emplace_back(1);
               }
            }

            // Sum-over-product structure.
            UNIT_ASSERT_EQUAL(Uin(arOpDef.Nterms()), ctrl_n_terms, s+"Wrong num. terms.");
            UNIT_ASSERT_EQUAL(Uin(arOpDef.NmodesInOp()), ctrl_n_modes, s+"Wrong num. modes.");
            for(LocalModeNr m = 0; m < arOpDef.NmodesInOp(); ++m)
            {
               UNIT_ASSERT_EQUAL
                  (  Uin(arOpDef.NrOneModeOpers(m))
                  ,  Uin(ctrl_eff_num_opers.at(arOpDef.GetGlobalModeNr(m)))
                  ,  s+"Wrong num. opers., m = "+std::to_string(m)
                  );
            }

            // Global and local mode maps.
            for(LocalModeNr m = 0; m < arOpDef.NmodesInOp(); ++m)
            {
               GlobalModeNr glob_m = arOpDef.GetGlobalModeNr(m);
               LocalModeNr loc_m = arOpDef.GetLocalModeNr(glob_m);
               UNIT_ASSERT_EQUAL(glob_m, arLoc2Glob.at(m), s+"GetGlobalModeNr, m = "+std::to_string(m));
               UNIT_ASSERT_EQUAL(loc_m, m, s+"GetLocalModeNr, m = "+std::to_string(m));
            }

            // Terms.
            for(Uin t = 0; t < arOpDef.Nterms(); ++t)
            {
               // Check coefs. Only real coefficients (at the moment at least).
               UNIT_ASSERT_EQUAL(std::imag(arOpDef.Coef(t)), absval_t<T>(0), s+"Non-zero imag. part of coef.");
               UNIT_ASSERT_EQUAL(arOpDef.Coef(t), std::get<0>(arCtrl.at(t)), s+"Wrong coef, t = "+std::to_string(t));

               // Check coefs, modes and operators of each term.
               const OperProd& op = arOpDef.GetOperProd(t);
               const std::vector<LocalModeNr> v_loc = op.GetModes();
               std::vector<GlobalModeNr> v_glob(v_loc.size());
               std::transform(v_loc.begin(), v_loc.end(), v_glob.begin(), [&arLoc2Glob](LocalModeNr m)->GlobalModeNr {return arLoc2Glob.at(m);});

               UNIT_ASSERT_EQUAL
                  (  v_glob
                  ,  std::get<1>(arCtrl.at(t))
                  ,  s+"Wrong global modes, term = "+std::to_string(t)
                  );
               UNIT_ASSERT_EQUAL
                  (  v_loc
                  ,  std::get<2>(arCtrl.at(t))
                  ,  s+"Wrong local modes, term = "+std::to_string(t)
                  );
               UNIT_ASSERT_EQUAL
                  (  op.GetOpers()
                  ,  std::get<3>(arCtrl.at(t))
                  ,  s+"Wrong opers, term = "+std::to_string(t)
                  );
            }
            
            // Active terms.
            UNIT_ASSERT(arOpDef.UseActiveTermsAlgo(), s+"Expected UseActiveTermsAlgo() set true.");
            for(LocalModeNr m = 0; m < arOpDef.NmodesInOp(); ++m)
            {
               UNIT_ASSERT_EQUAL(Uin(arOpDef.NactiveTerms(m)), ctrl_act_terms.at(m), s+"Num.act., m = "+std::to_string(m));
            }
         }

      public:
         void run() override
         {
            {
               // ====================================================================
               //  TEST A (RATHER STANDARD OPDEF)
               // ====================================================================
               // Operator MCR = {}, {0}, {1}, {2}, {0,2}, {1,2}
               ModeCombiOpRange mcr(2,3);
               mcr.Erase(std::vector<ModeCombi>{ModeCombi(std::vector<In>{0,1},-1)});
               std::vector<Uin> n_opers = {3,1,2};
               v_coef_t v_coefs =
                  {  {0}                     // MC = {}; 1 param (not used)
                  ,  {1, 2, -3}              // MC = {0}; 3 params
                  ,  {4}                     // MC = {1}; 1 params
                  ,  {5, -6}                 // MC = {2}; 2 params
                  ,  {7, 8, -9, 10, 11, -12} // MC = {0,2}; 6 params
                  ,  {13, 14}                // MC = {1,2}; 2 params
                  };
               // Expected OpDef structure:
               const ctrl_t v_coefs_modes_opers_for_term =
                  {  {  coef_t<T>(  1),  {0},     {0},     {1}    }
                  ,  {  coef_t<T>(  2),  {0},     {0},     {2}    }
                  ,  {  coef_t<T>( -3),  {0},     {0},     {3}    }
                  ,  {  coef_t<T>(  4),  {1},     {1},     {1}    }
                  ,  {  coef_t<T>(  5),  {2},     {2},     {1}    }
                  ,  {  coef_t<T>( -6),  {2},     {2},     {2}    }
                  ,  {  coef_t<T>(  7),  {0, 2},  {0, 2},  {1, 1} }
                  ,  {  coef_t<T>(  8),  {0, 2},  {0, 2},  {1, 2} }
                  ,  {  coef_t<T>( -9),  {0, 2},  {0, 2},  {2, 1} }
                  ,  {  coef_t<T>( 10),  {0, 2},  {0, 2},  {2, 2} }
                  ,  {  coef_t<T>( 11),  {0, 2},  {0, 2},  {3, 1} }
                  ,  {  coef_t<T>(-12),  {0, 2},  {0, 2},  {3, 2} }
                  ,  {  coef_t<T>( 13),  {1, 2},  {1, 2},  {1, 1} }
                  ,  {  coef_t<T>( 14),  {1, 2},  {1, 2},  {1, 2} }
                  };
               const std::vector<GlobalModeNr> map_loc_glob = {0,1,2};
               const OpDef opdef = util::GetOpDefFromCoefs(mcr, n_opers, v_coefs);
               TestImpl("A", opdef, n_opers, v_coefs_modes_opers_for_term, map_loc_glob);
            }
            {
               // ====================================================================
               //  TEST B (NON-STANDARD OPDEF)
               // ====================================================================
               // Operator MCR = { {0,2} } in 4-mode system.
               ModeCombiOpRange mcr;
               mcr.Insert(std::vector<ModeCombi>{ModeCombi(std::vector<In>{0,2},-1)});
               std::vector<Uin> n_opers = {2,4,1,3};
               v_coef_t v_coefs =
                  {  {1, 2}
                  };
               // Expected OpDef structure:
               const ctrl_t v_coefs_modes_opers_for_term =
                  {  {  coef_t<T>(  1),  {0,2},  {0,2},   {1,1} }
                  ,  {  coef_t<T>(  2),  {0,2},  {0,2},   {2,1} }
                  };
               const std::vector<GlobalModeNr> map_loc_glob = {0,1,2,3};
               const OpDef opdef = util::GetOpDefFromCoefs(mcr, n_opers, v_coefs);
               TestImpl("B", opdef, n_opers, v_coefs_modes_opers_for_term, map_loc_glob);
            }
            {
               // ====================================================================
               //  TEST C (NON-STANDARD OPDEF)
               // ====================================================================
               // Operator MCR = { {2}, {1,3} } in 6-mode system.
               ModeCombiOpRange mcr;
               mcr.Insert(std::vector<ModeCombi>
                  {  ModeCombi(std::vector<In>{2},-1)
                  ,  ModeCombi(std::vector<In>{1,3},-1)
                  });
               std::vector<Uin> n_opers = {5,2,4,1,3,5};
               v_coef_t v_coefs =
                  {  {-1, -2, -3, -4}
                  ,  {1, 2}
                  };
               // Expected OpDef structure:
               const ctrl_t v_coefs_modes_opers_for_term =
                  {  {  coef_t<T>( -1),  {2},    {2},     {1} }
                  ,  {  coef_t<T>( -2),  {2},    {2},     {2} }
                  ,  {  coef_t<T>( -3),  {2},    {2},     {3} }
                  ,  {  coef_t<T>( -4),  {2},    {2},     {4} }
                  ,  {  coef_t<T>(  1),  {1,3},  {1,3},   {1,1} }
                  ,  {  coef_t<T>(  2),  {1,3},  {1,3},   {2,1} }
                  };
               const std::vector<GlobalModeNr> map_loc_glob = {0,1,2,3,4,5};
               const OpDef opdef = util::GetOpDefFromCoefs(mcr, n_opers, v_coefs);
               TestImpl("C", opdef, n_opers, v_coefs_modes_opers_for_term, map_loc_glob);
            }
         }

   };

   /************************************************************************//**
    * @brief
    *    Explicit computation of Vcc Jacobian matrix by three methods;
    *    MatRepVibOper, Vcc2TransRJac, Vcc2TransLJac.
    ***************************************************************************/
   template
      <  typename T
      >
   struct ExplicitJacobian
      :  public cutee::test
   {
      public:
         ExplicitJacobian
            (  std::vector<Uin> aNModals
            ,  std::vector<Uin> aNOpers
            ,  Uin aMaxExciAmps = 2
            ,  Uin aMaxCoupOper = 2
            )
            :  mNModals(std::move(aNModals))
            ,  mNOpers(std::move(aNOpers))
            ,  mMaxExciAmps(aMaxExciAmps)
            ,  mMaxCoupOper(aMaxCoupOper)
         {
         }

      private:
         using transljac_t = midas::vcc::vcc2::Vcc2TransLJac<T>;
         using transrjac_t = midas::vcc::vcc2::Vcc2TransRJac<T>;
         const std::vector<Uin> mNModals;
         const std::vector<Uin> mNOpers;
         const Uin mMaxExciAmps;
         const Uin mMaxCoupOper;
         
         const std::vector<Uin>& NModals() const {return mNModals;}
         const std::vector<Uin>& NOpers() const {return mNOpers;}
         Uin MaxExciAmps() const {return mMaxExciAmps;}
         Uin MaxCoupOper() const {return mMaxCoupOper;}

         std::pair<ModeCombiOpRange,Uin> OperMcr() const
         {
            return util::GetMcrRandomMcsErased(NOpers(), MaxCoupOper());
         }
         std::pair<ModeCombiOpRange,Uin> ParamsMcr() const
         {
            return util::GetMcrRandomMcsErased(NModals(), MaxExciAmps());
         }

         //! Setup R/L Jacobian transformer.
         static std::unique_ptr<midas::vcc::vcc2::Vcc2TransFixedAmps<T>> SetupJacTransformer
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t<T>& arTAmps
            ,  char aJacType
            )
         {
            std::unique_ptr<midas::vcc::vcc2::Vcc2TransFixedAmps<T>> p(nullptr);
            switch(aJacType)
            {
               case 'l':
                  p.reset(new transljac_t(&arOpDef, arModInts, arMcr, arTAmps));
                  break;
               case 'r':
                  p.reset(new transrjac_t(&arOpDef, arModInts, arMcr, arTAmps));
                  break;
               default:
                  MIDASERROR(std::string("Case '")+aJacType+"' not valid option.");
                  break;
            }
            return p;
         }

         static vec_t<T> TransformStandardUnitVector
            (  const midas::vcc::vcc2::Vcc2TransFixedAmps<T>& arTransformer
            ,  const Uin aTotSizeParams
            ,  const Uin aUnitIndex
            )
         {
            const bool l_not_r 
               =  (dynamic_cast<const transljac_t*>(&arTransformer) != nullptr)
                  ?  true
                  :  false
                  ;
            datacont_t<T> dc_tau_nu(aTotSizeParams);
            dc_tau_nu.DataIo(IO_PUT, aUnitIndex, param_t<T>(1));
            datacont_t<T> dc_col_nu(aTotSizeParams - (l_not_r? 0: 1));
            arTransformer.Transform(dc_tau_nu, dc_col_nu);
            vec_t<T> v_col_nu(aTotSizeParams - 1);
            dc_col_nu.DataIo(IO_GET, v_col_nu, v_col_nu.Size(), l_not_r? 1: 0);
            return v_col_nu;
         }

         //!
         static mat_t<T> CalcJacobianMatrix
            (  const midas::vcc::vcc2::Vcc2TransFixedAmps<T>& arTransformer
            ,  const Uin aTotSizeParams
            )
         {
            const bool l_not_r 
               =  (dynamic_cast<const transljac_t*>(&arTransformer) != nullptr)
                  ?  true
                  :  false
                  ;
            const Uin A_size = aTotSizeParams - 1;
            mat_t<T> A(A_size, A_size, param_t<T>(0));
            for(Uin nu = 0; nu < A_size; ++nu)
            {
               const Uin unit_index = nu + 1;
               const auto res_nu = TransformStandardUnitVector(arTransformer, aTotSizeParams, unit_index);
               if (l_not_r)
               {
                  if (res_nu.size() != A.Ncols()) MIDASERROR("Wrong row size.");
                  A.AssignRow(res_nu, nu);
               }
               else
               {
                  if (res_nu.size() != A.Nrows()) MIDASERROR("Wrong column size.");
                  A.AssignCol(res_nu, nu);
               }
            }
            return A;
         }

         static mat_t<T> MatRepJacobian
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<param_t<T>>>& arTAmps
            )
         {
            return midas::util::matrep::ExplVccJac
               (  arNModals
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  arTAmps
               );
         }


      public:
         void run() override
         {
            // Setup (Hamiltonian) operator.
            const auto mcr_oper = (this->OperMcr()).first;
            const auto oper_coefs = util::GetRandomParams<coef_t<T>>(mcr_oper, NOpers(), false, false, 0);
            const auto opdef = util::GetOpDefFromCoefs(mcr_oper, NOpers(), oper_coefs);
            const auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, NModals());

            // Setup T amplitudes.
            const auto p_mcr_params = this->ParamsMcr();
            const auto& mcr_params = p_mcr_params.first;
            const auto& tot_size_params = p_mcr_params.second;
            const auto t_amps = util::GetRandomParams<T>(mcr_params, NModals());
            datacont_t<T> dc_t;
            util::ConstructParamsVecCont(dc_t, mcr_params, tot_size_params, t_amps);

            const mat_t<T> A_matrep = MatRepJacobian(NModals(), opdef, mod_ints, mcr_params, tot_size_params, t_amps);

            std::map<char,mat_t<T>> A_transf;
            for(const auto& type: std::vector<char>{'l', 'r'})
            {
               const auto p_transf = SetupJacTransformer
                  (  opdef
                  ,  mod_ints
                  ,  mcr_params
                  ,  dc_t
                  ,  type
                  );
               if (p_transf)
               {
                  A_transf[type] = CalcJacobianMatrix(*p_transf, tot_size_params);
               }
               else
               {
                  MIDASERROR("p_transf = nullptr, unexpectedly...");
               }
            }

            // The indices of the 1-, 2- mode parts and end of the Jacobian.
            // We utilize that the addresses have been set according to exci.
            // space defined by the number of modals. We subtract one, because
            // mcr_params contains the empty ModeCombi, but the final Jacobian
            // matrix doesn't have indices for this.
            UNIT_ASSERT_EQUAL(Uin(mcr_params.GetMaxExciLevel()), Uin(2), "Unexpected max.exci. level.");
            const Uin beg1 = mcr_params.Begin(1)->Address() - 1;
            const Uin beg2 = mcr_params.Begin(2)->Address() - 1;
            const Uin end2 = tot_size_params - 1;

            // Lambdas for comparing matrices.
            auto l_assert_equal = [beg1, beg2, end2]
               (  const mat_t<T>& a
               ,  const mat_t<T>& b
               )  -> std::pair<bool,std::string>
            {
               std::stringstream ss;
               if (!a.IsSquare())return std::make_pair(false, "a is non-square");
               if (!b.IsSquare())return std::make_pair(false, "b is non-square");
               if (a.Nrows() != b.Nrows())
               {
                  ss << "a.Nrows() = " << a.Nrows() << " != b.Nrows() = " << b.Nrows();
                  return std::make_pair(false, ss.str());
               }
               if (a.Ncols() != b.Ncols())
               {
                  ss << "a.Ncols() = " << a.Ncols() << " != b.Ncols() = " << b.Ncols();
                  return std::make_pair(false, ss.str());
               }

               mat_t<T> diff = a - b;
               const absval_t<T> one = 1;
               const absval_t<T> reldiffnorm = diff.Norm()/b.Norm();
               const Uin dev_ulps = libmda::numeric::float_ulps(one+reldiffnorm, one);
               if (dev_ulps > gen_rel_diff_norm_max_ulps)
               {
                  const auto diff_norm_11 = sqrt(diff.Norm2Block(beg1, beg2, beg1, beg2));
                  const auto diff_norm_12 = sqrt(diff.Norm2Block(beg1, beg2, beg2, end2));
                  const auto diff_norm_21 = sqrt(diff.Norm2Block(beg2, end2, beg1, beg2));
                  const auto diff_norm_22 = sqrt(diff.Norm2Block(beg2, end2, beg2, end2));
                  ss << "1+|a-b|/|b| != 1 (dist = " << dev_ulps << " ulps); diff-norms per block:\n"
                     << "   [1m,1m]: " << diff_norm_11 << '\n'
                     << "   [1m,2m]: " << diff_norm_12 << '\n'
                     << "   [2m,1m]: " << diff_norm_21 << '\n'
                     << "   [2m,2m]: " << diff_norm_22 << '\n'
                     ;
                  return std::make_pair(false, ss.str());
               }
               return std::make_pair(true, "");
            };

            // Comparisons.
            auto assert_l_mr = l_assert_equal(A_transf.at('l'), A_matrep);
            auto assert_r_mr = l_assert_equal(A_transf.at('r'), A_matrep);
            auto assert_r_l  = l_assert_equal(A_transf.at('r'), A_transf.at('l'));
            UNIT_ASSERT(assert_l_mr.first, "LJac vs. matrep; "+assert_l_mr.second);
            UNIT_ASSERT(assert_r_mr.first, "RJac vs. matrep; "+assert_r_mr.second);
            UNIT_ASSERT(assert_r_l.first, "RJac vs. LJac; "+assert_r_l.second);
         }
   };


   /************************************************************************//**
    * @brief
    *    Base struct for some methods shared by Vcc2 transformer tests.
    *    Derived classes implement method specific computations.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vcc2TransTestBase
      :  public cutee::test
   {
      public:
         /******************************************************************//**
          * @brief
          *    Enum used to construct different types of ModeCombiOpRange.
          *
          * Either the standard random ones
          * (util::GetMcrRandomMcsErased()), or ones specifically
          * tailored to fail due to certain bugs in the code, and thus test
          * them in the future. (With the bugs hopefully gone, when you look at
          * this, the comments won't make much sense; just know that they were
          * useful at some point.)
          *********************************************************************/
         enum class MCR_T
         {  Standard          // Standard (max.exci,num.modes).
         ,  DebugLJacA        // See commit: "Crucial bug fixes for the Vcc2Contribs[...] classes"
         ,  NonDeexcClsOperA  // Oper. MCR not de-exc.closed; MCR = {0}, {1,2} (glob. modes)
         ,  NonDeexcClsOperB  // Oper. MCR not de-exc.closed; MCR = {1}, {0,2} (glob. modes)
         ,  NonDeexcClsOperC  // Oper. MCR not de-exc.closed; MCR = {0,2} (glob. modes)
         ,  NonDeexcClsAmpMcrA// Ampl. MCR not de-exc.closed; MCR = {0},{1},{0,1} ({} missing)
         ,  NonDeexcClsAmpMcrB// Ampl. MCR not de-exc.closed; MCR = {},{1},{0,1} ({0} missing)
         ,  NonDeexcClsAmpMcrC// Ampl. MCR not de-exc.closed; MCR = {},{0},{0,1} ({1} missing)
         };

      private:
         MCR_T mMcrT = MCR_T::Standard;
         std::vector<Uin> mNModals = {};
         std::vector<Uin> mNOpers = {};
         Uin mMaxExciAmps = 0;
         Uin mMaxCoupOper = 0;

      protected:
         Vcc2TransTestBase
            (  std::vector<Uin> aNModals
            ,  std::vector<Uin> aNOpers
            ,  Uin aMaxExciAmps
            ,  Uin aMaxCoupOper
            )
            :  mMcrT(MCR_T::Standard)
            ,  mNModals(std::move(aNModals))
            ,  mNOpers(std::move(aNOpers))
            ,  mMaxExciAmps(aMaxExciAmps)
            ,  mMaxCoupOper(aMaxCoupOper)
         {
         }

         Vcc2TransTestBase
            (  MCR_T aMcrT
            )
            :  mMcrT(aMcrT)
            ,  mNModals(SetNModals(aMcrT))
            ,  mNOpers(SetNOpers(aMcrT))
            ,  mMaxExciAmps(2)
            ,  mMaxCoupOper(2)
         {
         }

         const std::vector<Uin>& NModals() const {return mNModals;}
         const std::vector<Uin>& NOpers() const {return mNOpers;}
         Uin MaxExciAmps() const {return mMaxExciAmps;}
         Uin MaxCoupOper() const {return mMaxCoupOper;}

         virtual bool DoingVcc() const {return true;}
         virtual Uin NumParamVectors() const = 0;
         MCR_T McrT() const {return mMcrT;}

         static std::vector<Uin> SetNModals(MCR_T aMcrT)
         {
            switch(aMcrT)
            {
               case MCR_T::DebugLJacA:
                  return std::vector<Uin>{2,2,2,2};
                  break;
               case MCR_T::NonDeexcClsOperA:
                  return std::vector<Uin>{4,2,3};
                  break;
               case MCR_T::NonDeexcClsOperB:
                  return std::vector<Uin>{2,2,2};
                  break;
               case MCR_T::NonDeexcClsOperC:
                  return std::vector<Uin>{2,3,2};
                  break;
               case MCR_T::NonDeexcClsAmpMcrA:
                  return std::vector<Uin>{3,3};
                  break;
               case MCR_T::NonDeexcClsAmpMcrB:
                  return std::vector<Uin>{3,3};
                  break;
               case MCR_T::NonDeexcClsAmpMcrC:
                  return std::vector<Uin>{3,3};
                  break;
               default:
                  MIDASERROR("Can't handle default.");
                  return std::vector<Uin>{};
                  break;
            }
         }

         static std::vector<Uin> SetNOpers(MCR_T aMcrT)
         {
            switch(aMcrT)
            {
               case MCR_T::DebugLJacA:
                  return std::vector<Uin>{2,2,2,2};
                  break;
               case MCR_T::NonDeexcClsOperA:
                  return std::vector<Uin>{1,1,1};
                  break;
               case MCR_T::NonDeexcClsOperB:
                  return std::vector<Uin>{1,1,1};
                  break;
               case MCR_T::NonDeexcClsOperC:
                  return std::vector<Uin>{1,0,1};
                  break;
               case MCR_T::NonDeexcClsAmpMcrA:
                  return std::vector<Uin>{2,2};
                  break;
               case MCR_T::NonDeexcClsAmpMcrB:
                  return std::vector<Uin>{2,2};
                  break;
               case MCR_T::NonDeexcClsAmpMcrC:
                  return std::vector<Uin>{2,2};
                  break;
               default:
                  MIDASERROR("Can't handle default.");
                  return std::vector<Uin>{};
                  break;
            }
         }

         //! Constructing the ModeCombiOpRange for the operator.
         std::pair<ModeCombiOpRange,Uin> OperMcr() const
         {
            switch(this->McrT())
            {
               case MCR_T::Standard:
               {
                  return util::GetMcrRandomMcsErased(NOpers(), MaxCoupOper());
                  break;
               }
               case MCR_T::DebugLJacA:
               {
                  ModeCombiOpRange mcr(this->MaxCoupOper(), this->NOpers().size());
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsOperA:
               {
                  ModeCombiOpRange mcr;
                  mcr.Insert(std::vector<ModeCombi>
                     {  ModeCombi(std::set<In>{0},-1)
                     ,  ModeCombi(std::set<In>{1,2},-1)
                     });
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsOperB:
               {
                  ModeCombiOpRange mcr;
                  mcr.Insert(std::vector<ModeCombi>
                     {  ModeCombi(std::set<In>{1},-1)
                     ,  ModeCombi(std::set<In>{0,2},-1)
                     });
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsOperC:
               {
                  ModeCombiOpRange mcr;
                  mcr.Insert(std::vector<ModeCombi>{ModeCombi(std::set<In>{0,2},-1)});
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrA:
               {
                  ModeCombiOpRange mcr(this->MaxCoupOper(), this->NOpers().size());
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrB:
               {
                  ModeCombiOpRange mcr(this->MaxCoupOper(), this->NOpers().size());
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrC:
               {
                  ModeCombiOpRange mcr(this->MaxCoupOper(), this->NOpers().size());
                  Uin size = util::SetAddresses(mcr, this->NOpers());
                  return std::make_pair(std::move(mcr), size);
                  break;
               }
               default:
               {
                  MIDASERROR("Can't handle default.");
                  return std::make_pair(ModeCombiOpRange(),0);
                  break;
               }
            }
         }

         //! Constructing the ModeCombiOpRange for the parameters.
         std::pair<ModeCombiOpRange,Uin> ParamsMcr() const
         {
            switch(this->McrT())
            {
               case MCR_T::Standard:
               {
                  return util::GetMcrRandomMcsErased(NModals(), MaxExciAmps());
                  break;
               }
               case MCR_T::DebugLJacA:
               {
                  // MCR = {{},{0},...,{3},{0,1},..., NOT({1,2}),...,{2,3}}
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  mcr.Erase(std::vector<ModeCombi>{ModeCombi(std::set<In>{1,2},-1)});
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsOperA:
               {
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsOperB:
               {
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsOperC:
               {
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrA:
               {
                  // MCR = {{0},{1},{0,1}}
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  mcr.Erase(std::vector<ModeCombi>{ModeCombi(std::set<In>{},-1)});
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrB:
               {
                  // MCR = {{},{1},{0,1}}
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  mcr.Erase(std::vector<ModeCombi>{ModeCombi(std::set<In>{0},-1)});
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               case MCR_T::NonDeexcClsAmpMcrC:
               {
                  // MCR = {{},{0},{0,1}}
                  ModeCombiOpRange mcr(this->MaxExciAmps(), this->NModals().size());
                  mcr.Erase(std::vector<ModeCombi>{ModeCombi(std::set<In>{1},-1)});
                  Uin size = util::SetAddresses(mcr, this->NModals());
                  return std::make_pair(std::move(mcr),size);
                  break;
               }
               default:
               {
                  MIDASERROR("Can't handle default.");
                  return std::make_pair(ModeCombiOpRange(),0);
                  break;
               }
            }
         }

         //! The parameters necessary; T (VCC err, eta), C (VCI), T,R (R-Jac.), T,L (L-Jac.).
         std::vector<std::vector<std::vector<param_t<T>>>> ConstructParams
            (  const ModeCombiOpRange& arMcr
            ,  const std::vector<Uin>& arNumModals
            )  const
         {
            std::vector<std::vector<std::vector<param_t<T>>>> v;
            v.reserve(this->NumParamVectors());
            for(Uin i = 0; i < this->NumParamVectors(); ++i)
            {
               v.emplace_back(util::GetRandomParams<T>(arMcr, arNumModals, DoingVcc()));
            }
            return v;
         }

         //! Return the MCR space vector result from the matrix rep. control calc.
         virtual vec_t<T> CtrlMatRepImpl
            (  const OpDef&
            ,  const ModalIntegrals<T>&
            ,  const ModeCombiOpRange&
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>&
            )  const = 0;
         //! Return the MCR space vector result from the actual transformer impl.
         virtual vec_t<T> ResultTransImpl
            (  const OpDef&
            ,  const ModalIntegrals<T>&
            ,  const ModeCombiOpRange&
            ,  const Uin
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>&
            )  const = 0;

      public:
         void run() override
         {
            // Setup (Hamiltonian) operator.
            const auto mcr_oper = (this->OperMcr()).first;
            const auto oper_coefs = util::GetRandomParams<coef_t<T>>(mcr_oper, NOpers(), false, false, 0);
            const auto opdef = util::GetOpDefFromCoefs(mcr_oper, NOpers(), oper_coefs);
            const auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, NModals());

            // Setup input params ({T}, {C}, {T,R}, {T,L} depending on method).
            const auto p_mcr_params = this->ParamsMcr();
            const auto& mcr_params = p_mcr_params.first;
            const auto& tot_size_params = p_mcr_params.second;
            const auto v_params = ConstructParams(mcr_params, NModals());

            // Full space matrix representation.
            const vec_t<T> ctrl = CtrlMatRepImpl(opdef, mod_ints, mcr_params, v_params);

            // Actual transformer.
            const vec_t<T> result = ResultTransImpl(opdef, mod_ints, mcr_params, tot_size_params, v_params);
            // Assertions.
            UNIT_ASSERT_EQUAL(result.Size(), ctrl.Size(), "Res/ctrl sizes differ.");
            const absval_t<T> one(1);
            const absval_t<T> diffnorm = vec_t<T>(result - ctrl).Norm();
            const absval_t<T> reldiffnorm = diffnorm/ctrl.Norm();
            const absval_t<T> min = diffnorm < reldiffnorm? diffnorm: reldiffnorm;
            UNIT_ASSERT_FEQUAL_PREC
               (  one + min
               ,  one
               ,  gen_rel_diff_norm_max_ulps
               ,  "1 + min(|res-ctrl|,|res-ctrl|/|ctrl|) != 1."
               );
         }
   };

   /************************************************************************//**
    * @brief
    *    Compare Vcc2TransReg (VCC error vector) against full space
    *    matrix representation computation.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vcc2ErrVec
      :  public Vcc2TransTestBase<T>
   {
      public:
         using typename Vcc2TransTestBase<T>::MCR_T;

         template<class... Args>
         Vcc2ErrVec(Args&&... args)
            :  Vcc2TransTestBase<T>(std::forward<Args>(args)...)
         {
         }

      private:
         //! For VCC we just need one set of params, the T amplitudes.
         Uin NumParamVectors() const override {return 1;}

         /******************************************************************//**
          * @brief
          *    The exp(-T)Hexp(T)|ref> computation in full space matrix
          *    representation.
          *********************************************************************/
         vec_t<T> CtrlMatRepImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            return midas::util::matrep::TrfVccErrVec
               (  this->NModals()
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  t_amps
               );
         }

         /******************************************************************//**
          * @brief
          *    The computation by Vcc2TransReg<T>::Transform().
          *********************************************************************/
         vec_t<T> ResultTransImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            midas::vcc::vcc2::Vcc2TransReg<T> trf(&arOpDef, arModInts, arMcr);
            datacont_t<T> dc_in;
            util::ConstructParamsVecCont(dc_in, arMcr, aTotSizeParams, t_amps);
            datacont_t<T> dc_out(aTotSizeParams);
            trf.Transform(dc_in, dc_out);
            vec_t<T> result(dc_out.Size());
            dc_out.DataIo(IO_GET, result, result.Size());
            return result;
         }
   };

   /************************************************************************//**
    * @brief
    *    Compare Vcc2TransReg (VCC error vector) against full space
    *    matrix representation computation.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vci2Trans
      :  public Vcc2TransTestBase<T>
   {
      public:
         using typename Vcc2TransTestBase<T>::MCR_T;

         template<class... Args>
         Vci2Trans(Args&&... args)
            :  Vcc2TransTestBase<T>(std::forward<Args>(args)...)
         {
         }

      private:
         bool DoingVcc() const override {return false;}

         //! For VCI we just need one set of params, the C coefficients.
         Uin NumParamVectors() const override {return 1;}

         /******************************************************************//**
          * @brief
          *    The HC|ref> transform in full space matrix representation.
          *********************************************************************/
         vec_t<T> CtrlMatRepImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& c_coefs = arParams.at(0);
            return midas::util::matrep::TrfVci
               (  this->NModals()
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  c_coefs
               );
         }

         /******************************************************************//**
          * @brief
          *    The computation by Vcc2TransReg<T>::Transform().
          *********************************************************************/
         vec_t<T> ResultTransImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& c_coefs = arParams.at(0);
            midas::vcc::vcc2::Vcc2TransReg<T> trf(&arOpDef, arModInts, arMcr);
            trf.DoingVcc() = false;
            datacont_t<T> dc_in;
            util::ConstructParamsVecCont(dc_in, arMcr, aTotSizeParams, c_coefs);
            datacont_t<T> dc_out(aTotSizeParams);
            trf.Transform(dc_in, dc_out);
            vec_t<T> result(dc_out.Size());
            dc_out.DataIo(IO_GET, result, result.Size());
            return result;
         }
   };

   /************************************************************************//**
    * @brief
    *    Compare Vcc2TransRJac (VCC R-hand Jacobian) against full space
    *    matrix representation computation.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vcc2RJac
      :  public Vcc2TransTestBase<T>
   {
      public:
         using typename Vcc2TransTestBase<T>::MCR_T;

         template<class... Args>
         Vcc2RJac(Args&&... args)
            :  Vcc2TransTestBase<T>(std::forward<Args>(args)...)
         {
         }

      private:
         //! For R-Jacobian we need two set of params, the T amps. and R coefs.
         Uin NumParamVectors() const override {return 2;}

         /******************************************************************//**
          * @brief
          *    The exp(-T)[H,R]exp(T)|ref> computation in full space matrix
          *    representation.
          * @note
          *    The Vcc2TransRJac::Transform() won't compute the ref. element,
          *    so there it will be zero (it doesn't play a role in VCC
          *    response); however, it _is_ calculated in the full space, so
          *    we'll explicitly set it to zero to get consistent results.
          *********************************************************************/
         vec_t<T> CtrlMatRepImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            const auto& r_coefs = arParams.at(1);
            return midas::util::matrep::TrfVccRJac
               (  this->NModals()
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  t_amps
               ,  r_coefs
               );
         }

         /******************************************************************//**
          * @brief
          *    The computation by Vcc2TransReg<T>::Transform().
          *
          * @note
          *    For the RJac transformer, the "out" container doesn't contain an
          *    element for the reference, and thus has size -1 compared to
          *    other containers.
          *********************************************************************/
         vec_t<T> ResultTransImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            const auto& r_coefs = arParams.at(1);
            datacont_t<T> dc_t;
            util::ConstructParamsVecCont(dc_t, arMcr, aTotSizeParams, t_amps);
            datacont_t<T> dc_r;
            util::ConstructParamsVecCont(dc_r, arMcr, aTotSizeParams, r_coefs);
            midas::vcc::vcc2::Vcc2TransRJac<T> trf(&arOpDef, arModInts, arMcr, dc_t);
            datacont_t<T> dc_out(aTotSizeParams - 1);
            trf.Transform(dc_r, dc_out);
            vec_t<T> result(dc_out.Size() + 1, param_t<T>(0));
            dc_out.DataIo(IO_GET, result, dc_out.Size(), 0, 1, 1);
            return result;
         }
   };

   /************************************************************************//**
    * @brief
    *    Compare Vcc2TransEta (VCC response eta vector) against full space
    *    matrix representation computation.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vcc2Eta
      :  public Vcc2TransTestBase<T>
   {
      public:
         using typename Vcc2TransTestBase<T>::MCR_T;

         template<class... Args>
         Vcc2Eta(Args&&... args)
            :  Vcc2TransTestBase<T>(std::forward<Args>(args)...)
         {
         }

      private:
         //! For eta we just need one set of params, the T amps.
         Uin NumParamVectors() const override {return 1;}

         /******************************************************************//**
          * @brief
          *    The <ref|exp(-T)[H,tau_nu]exp(T)|ref> computation in full space
          *    matrix representation.
          * @note
          *    The Vcc2TransEta::Transform() won't compute the ref. element,
          *    so there it will be zero (it doesn't play a role in VCC
          *    response); however, it _is_ calculated in the full space, so
          *    we'll explicitly set it to zero to get consistent results.
          *********************************************************************/
         vec_t<T> CtrlMatRepImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            return midas::util::matrep::TrfVccEtaVec
               (  this->NModals()
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  t_amps
               );
         }

         /******************************************************************//**
          * @brief
          *    The computation by Vcc2TransEta<T>::Transform().
          * @note
          *    For the Eta transformer, the "out" container doesn't contain an
          *    element for the reference, and thus has size -1 compared to
          *    other containers.
          *********************************************************************/
         vec_t<T> ResultTransImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            midas::vcc::vcc2::Vcc2TransEta<T> trf(&arOpDef, arModInts, arMcr);
            datacont_t<T> dc_in;
            util::ConstructParamsVecCont(dc_in, arMcr, aTotSizeParams, t_amps);
            datacont_t<T> dc_out(aTotSizeParams - 1);
            trf.Transform(dc_in, dc_out);
            vec_t<T> result(dc_out.Size() + 1, param_t<T>(0));
            dc_out.DataIo(IO_GET, result, dc_out.Size(), 0, 1, 1);
            return result;
         }
   };

   /************************************************************************//**
    * @brief
    *    Compare Vcc2TransEta (VCC response eta vector) against full space
    *    matrix representation computation.
    ***************************************************************************/
   template
      <  typename T
      >
   struct Vcc2LJac
      :  public Vcc2TransTestBase<T>
   {
      public:
         using typename Vcc2TransTestBase<T>::MCR_T;

         template<class... Args>
         Vcc2LJac(Args&&... args)
            :  Vcc2TransTestBase<T>(std::forward<Args>(args)...)
         {
         }

      protected:
         //! For L-Jacobian we need two set of params, the T amps. and L coefs.
         Uin NumParamVectors() const override {return 2;}

         /******************************************************************//**
          * @brief
          *    The <ref|Lexp(-T)[H,tau_nu]exp(T)|ref> computation in full space
          *    matrix representation.
          *********************************************************************/
         vec_t<T> CtrlMatRepImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            const auto& l_coefs = arParams.at(1);
            return midas::util::matrep::TrfVccLJac
               (  this->NModals()
               ,  arOpDef
               ,  arModInts
               ,  arMcr
               ,  t_amps
               ,  l_coefs
               );
         }

         /******************************************************************//**
          * @brief
          *    The computation by Vcc2TransLJac<T>::Transform().
          *********************************************************************/
         vec_t<T> ResultTransImpl
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<T>& arModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const Uin aTotSizeParams
            ,  const std::vector<std::vector<std::vector<param_t<T>>>>& arParams
            )  const override
         {
            const auto& t_amps = arParams.at(0);
            const auto& l_coefs = arParams.at(1);
            datacont_t<T> dc_t;
            util::ConstructParamsVecCont(dc_t, arMcr, aTotSizeParams, t_amps);
            datacont_t<T> dc_l;
            util::ConstructParamsVecCont(dc_l, arMcr, aTotSizeParams, l_coefs);
            midas::vcc::vcc2::Vcc2TransLJac<T> trf(&arOpDef, arModInts, arMcr, dc_t);
            datacont_t<T> dc_out(aTotSizeParams);
            trf.Transform(dc_l, dc_out);
            vec_t<T> result(dc_out.Size(), param_t<T>(0));
            dc_out.DataIo(IO_GET, result, dc_out.Size());
            return result;
         }
   };

} /* namespace midas::test::vcc2trans */

#endif/*VCC2TRANSTEST_H_INCLUDED*/
