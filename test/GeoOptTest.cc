/**
 *******************************************************************************
 * 
 * @file    GeoOptTest.cc
 * @date    03-09-2020 
 * @author  Denis G. Artiukhin (artiukhin@chem.au.dk) 
 *
 * @brief
 *    Unit tests for routines in geoopt folder. 
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/GeoOptTest.h" 

namespace midas::test
{
   namespace geoopt_test::detail 
   {
      
      void RemoveFile(const std::string& arFile)
      {
         if (midas::filesystem::IsFile(arFile))
         {
            auto ret_val = midas::filesystem::Remove(arFile);
            if (ret_val != 0)
            {
               MIDASERROR
                  (  "RemoveFile(): Failure when removing " + arFile
                  +  " (return value: "+std::to_string(ret_val)+")"
                  );
            }
         }
      }
   
   } /* geoopt_test::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void GeoOptTest() 
   {
      // Create test_suite object.
      cutee::suite suite("GeoOptTest test");

      // geoopt/geoutil.h  
      suite.add_test<geoopt_test::TestCenterMolecule>               ("Test CenterMolecule().");
      suite.add_test<geoopt_test::TestBuildProjector>               ("Test BuildProjector().");
      suite.add_test<geoopt_test::TestCleanHessian>                 ("Test CleanHessian().");
      suite.add_test<geoopt_test::TestReadInternalCoordinates>      ("Test ReadInternalCoordinates().");
      suite.add_test<geoopt_test::TestWriteInternalCoordinates>     ("Test WriteInternalCoordinates().");
      suite.add_test<geoopt_test::TestReadWriteInternalCoordinates> ("Test ReadInternalCoordinates followed by WriteInternalCoordinates().");
      suite.add_test<geoopt_test::TestGetRefCoords>                 ("Test GetRefCoords().");

      // geoopt/GenerFuncs.h 
      suite.add_test<geoopt_test::TestPseudoInvert>     ("Test PseudoInvert().");

      // geoopt/BmatBuilder.h
      suite.add_test<geoopt_test::TestGenerateRow>      ("Test GenerateRow().");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
