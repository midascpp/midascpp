/**
 *******************************************************************************
 * 
 * @file    TdOperTest.h
 * @date    07-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDOPERTEST_H_INCLUDED
#define TDOPERTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "input/OpDef.h"
#include "td/oper/TdOperTerm.h"
#include "td/oper/LinCombOper.h"
#include "test/CuteeInterface.h"
#include "mmv/MidasMatrix.h"

namespace midas::test::tdoper
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
      //! Construct and get an empty OpDef.
      OpDef GetEmptyOpDef();

      template<typename T>
      struct Func
      {
         inline static constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
         using res_t = T;
         using step_t = midas::type_traits::RealTypeT<T>;

         res_t operator()(step_t t) const
         {
            res_t val(cos(t));
            if constexpr(is_complex)
            {
               val += res_t(0,sin(t));
            }
            return val;
         };

         res_t Deriv(step_t t) const
         {
            res_t val(-sin(t));
            if constexpr(is_complex)
            {
               val += res_t(0,cos(t));
            }
            return val;
         };
      };

   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename LIN_COEF_T
      >
   struct TdOperTermTimeIndep
      :  public cutee::test
   {
      private:
         LIN_COEF_T mCoef;

      public:
         TdOperTermTimeIndep
            (  LIN_COEF_T aCoef
            )
            :  mCoef(aCoef)
         {}

         void run() override
         {
            using lin_coef_t = LIN_COEF_T;
            using step_t = midas::type_traits::RealTypeT<lin_coef_t>;
            using term_t = midas::td::TdOperTerm<step_t,lin_coef_t,OpDef>;
            const OpDef oper = detail::GetEmptyOpDef();
            term_t term(oper, mCoef);
            
            // Access operator (to see we don't get MIDASERROR).
            const OpDef& oper_ref = term.Oper();
            UNIT_ASSERT_EQUAL(Uin(oper_ref.Nterms()), Uin(0), "Wrong number of oper. terms.");

            // Evaluate coefficient and derivative at different times; constant
            // so should always give same result.
            for(const auto& t: std::vector<step_t>{-1,0,1})
            {
               UNIT_ASSERT_EQUAL(term.Coef(t), mCoef, "Bad Coef(t) at t = "+std::to_string(t));
               UNIT_ASSERT_EQUAL(term.Deriv(t), lin_coef_t(0), "Bad Deriv(t) at t = "+std::to_string(t));
            }
         }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename LIN_COEF_T
      >
   struct TdOperTermTimeDepCustomDeriv
      :  public cutee::test
   {
      inline static constexpr bool is_complex = midas::type_traits::IsComplexV<LIN_COEF_T>;

      void run() override
      {
         using lin_coef_t = LIN_COEF_T;
         using step_t = midas::type_traits::RealTypeT<lin_coef_t>;
         using term_t = midas::td::TdOperTerm<step_t,lin_coef_t,OpDef>;
         const OpDef oper = detail::GetEmptyOpDef();

         // coef(t) = 
         //    exp(it) = cos(t) + i sin(t) for complex case
         //    cos(t)                      for real case
         // deriv(t) =
         //    i exp(it) = -sin(t) + i cos(t) for complex case
         //    -sin(t)                        for real case
         auto coef = [](step_t t)->lin_coef_t
            {
               lin_coef_t val(cos(t));
               if constexpr(is_complex)
               {
                  val += lin_coef_t(0,sin(t));
               }
               return val;
            };
         auto deriv = [](step_t t)->lin_coef_t
            {
               lin_coef_t val(-sin(t));
               if constexpr(is_complex)
               {
                  val += lin_coef_t(0,cos(t));
               }
               return val;
            };

         term_t term(oper, coef, deriv);
         
         // Access operator (to see we don't get MIDASERROR).
         const OpDef& oper_ref = term.Oper();
         UNIT_ASSERT_EQUAL(Uin(oper_ref.Nterms()), Uin(0), "Wrong number of oper. terms.");

         // Evaluate coefficient and derivative at different times.
         for(const auto& t: std::vector<step_t>{-1,1})
         {
            UNIT_ASSERT_FEQUAL(term.Coef(t), coef(t), "Bad Coef(t) at t = "+std::to_string(t));
            UNIT_ASSERT_FEQUAL(term.Deriv(t), deriv(t), "Bad Deriv(t) at t = "+std::to_string(t));
         }
      }
   };

   ///************************************************************************//**
   // * 
   // ***************************************************************************/
   //template
   //   <  typename LIN_COEF_T
   //   >
   //struct TdOperTermTimeDepImplDeriv
   //   :  public cutee::test
   //{
   //   using lin_coef_t = LIN_COEF_T;
   //   using step_t = midas::type_traits::RealTypeT<lin_coef_t>;
   //   using term_t = midas::td::TdOperTerm<step_t,lin_coef_t,OpDef>;
   //   inline static constexpr bool is_complex = midas::type_traits::IsComplexV<lin_coef_t>;

   //   void run() override
   //   {
   //      //const OpDef oper = detail::GetEmptyOpDef();

   //      //Func f;
   //      //term_t term(oper, f);
   //      //
   //      //// Access operator (to see we don't get MIDASERROR).
   //      //const OpDef& oper_ref = term.Oper();
   //      //UNIT_ASSERT_EQUAL(Uin(oper_ref.Nterms()), Uin(0), "Wrong number of oper. terms.");

   //      //// Evaluate coefficient and derivative at different times.
   //      //for(const auto& t: std::vector<step_t>{-1,1})
   //      //{
   //      //   LIN_COEF_T one(1);
   //      //   UNIT_ASSERT_FEQUAL(term.Coef(t), f(t), "Bad Coef(t) at t = "+std::to_string(t));
   //      //   UNIT_ASSERT_FEQUAL(term.Deriv(t), f.Deriv(t), "Bad Deriv(t) at t = "+std::to_string(t));
   //      //}
   //   }
   //};

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename LIN_COEF_T
      >
   struct TdOperTermTimeDepFallbackDeriv
      :  public cutee::test
   {
      inline static constexpr bool is_complex = midas::type_traits::IsComplexV<LIN_COEF_T>;

      void run() override
      {
         using lin_coef_t = LIN_COEF_T;
         using step_t = midas::type_traits::RealTypeT<lin_coef_t>;
         using term_t = midas::td::TdOperTerm<step_t,lin_coef_t,OpDef>;
         const OpDef oper = detail::GetEmptyOpDef();

         // coef(t) = 
         //    exp(it) = cos(t) + i sin(t) for complex case
         //    cos(t)                      for real case
         // deriv(t) =
         //    i exp(it) = -sin(t) + i cos(t) for complex case
         //    -sin(t)                        for real case
         auto coef = [](step_t t)->lin_coef_t
            {
               lin_coef_t val(cos(t));
               if constexpr(is_complex)
               {
                  val += lin_coef_t(0,sin(t));
               }
               return val;
            };
         auto deriv = [](step_t t)->lin_coef_t
            {
               lin_coef_t val(-sin(t));
               if constexpr(is_complex)
               {
                  val += lin_coef_t(0,cos(t));
               }
               return val;
            };

         term_t term(oper, coef);
         
         // Access operator (to see we don't get MIDASERROR).
         const OpDef& oper_ref = term.Oper();
         UNIT_ASSERT_EQUAL(Uin(oper_ref.Nterms()), Uin(0), "Wrong number of oper. terms.");

         // Evaluate coefficient and derivative at different times.
         // NB! Derivative is from approximate formula, hence _not_ very accurate.
         for(const auto& t: std::vector<step_t>{-1,1})
         {
            UNIT_ASSERT_FEQUAL(term.Coef(t), coef(t), "Bad Coef(t) at t = "+std::to_string(t));
            UNIT_ASSERT_FEQUAL_PREC(term.Deriv(t), deriv(t), 18000000, "Bad Deriv(t) at t = "+std::to_string(t));
         }
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename LIN_COEF_T
      >
   struct LinCombOperTest
      :  public cutee::test
   {
      using lin_coef_t = LIN_COEF_T;
      using step_t = midas::type_traits::RealTypeT<lin_coef_t>;
      using oper_t = GeneralMidasMatrix<lin_coef_t>;
      using lincomboper_t = midas::td::LinCombOper<step_t,lin_coef_t,oper_t>;

      void run() override
      {
         auto f = [](step_t t)->lin_coef_t {return t*t;};
         auto d = [](step_t t)->lin_coef_t {return 2*t;};
         const lin_coef_t coef(2);
         detail::Func<lin_coef_t> func;

         oper_t oper;

         lincomboper_t lco;
         lco.emplace_back(oper, f, d);
         lco.emplace_back(oper, coef);
         lco.emplace_back(oper, f);
         lco.emplace_back(oper, func, [&func](step_t t)->auto {return func.Deriv(t);});

         //step_t ctrl_t = 2;
         step_t ctrl_t = sqrt(2);
         std::vector<lin_coef_t> ctrl_coef = {f(ctrl_t), coef, f(ctrl_t), func(ctrl_t)};
         std::vector<lin_coef_t> ctrl_der  = {d(ctrl_t),    0, d(ctrl_t), func.Deriv(ctrl_t)};
         std::vector<Uin> ctrl_ulps = {0, 0, 1500000, 0};

         for(Uin i = 0; i < lco.size(); ++i)
         {
            std::string si = ", i = " + std::to_string(i) + ".";
            UNIT_ASSERT_EQUAL(Uin(lco.at(i).Oper().Nrows()), Uin(0), "Oper().Nrows()"+si);
            UNIT_ASSERT_EQUAL(lco.at(i).Coef(ctrl_t), ctrl_coef.at(i), "Wrong Coef(t)"+si);
            UNIT_ASSERT_FEQUAL_PREC(lco.at(i).Deriv(ctrl_t), ctrl_der.at(i), ctrl_ulps.at(i), "Wrong Deriv(t)"+si);
         }
      }
   };

} /* namespace midas::test::tdoper */

#endif/*TDOPERTEST_H_INCLUDED*/
