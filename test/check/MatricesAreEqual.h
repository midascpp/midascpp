#ifndef MATRICIESAREEQUAL_H_INCLUDED
#define MATRICIESAREEQUAL_H_INCLUDED

#include "mmv/MidasMatrix.h"

namespace midas
{
namespace test
{
namespace check
{

/**
 *
 **/
template<class T>
bool DataContAreEqual(const GeneralDataCont<T>& aData1, const GeneralDataCont<T>& aData2, unsigned aUlps = 4)
{
   const auto& cont1 = aData1;
   const auto& cont2 = aData2;

   assert(cont1.Size() == cont2.Size());

   for(int i=0; i<cont1.Size(); ++i)
   {
      // get elements
      T elem1, elem2;
      cont1.DataIo(IO_GET, i, elem1);
      cont2.DataIo(IO_GET, i, elem2);

      // assert element equal
      if(!libmda::numeric::float_eq(elem1 ,elem2 ,aUlps))
      {
         Mout << elem1 << " " << elem2 << " " << libmda::numeric::float_ulps(elem1,elem2) << std::endl;
         return false;
      }
   }

   return true;
}
                                    

/**
 *
 **/
template<class T>
bool MatricesAreEqual(const GeneralMidasMatrix<T>& aMat1, const GeneralMidasMatrix<T>& aMat2, unsigned aUlps = 4)
{
   MidasAssert(aMat1.Nrows() == aMat2.Nrows(), "Wrong number of rows in MidasMatrices: " + std::to_string(aMat1.Nrows()) + " vs. " + std::to_string(aMat2.Nrows()));
   MidasAssert(aMat1.Ncols() == aMat2.Ncols(), "Wrong number of cols in MidasMatrices: " + std::to_string(aMat1.Ncols()) + " vs. " + std::to_string(aMat2.Ncols()));

   for(int i=0; i<aMat1.Nrows(); ++i)
   {
      for(int j=0; j<aMat1.Ncols(); ++j)
      {
         if(!libmda::numeric::float_eq(aMat1[i][j], aMat2[i][j], aUlps))
         {
            std::stringstream ss;
            ss << std::scientific << std::setprecision(16);
            ss << "aMat1["<<i<<"]["<<j<<"] = " << aMat1[i][j] << '\n'
               << "aMat2["<<i<<"]["<<j<<"] = " << aMat2[i][j] << '\n'
               << "float_ulps = " << libmda::numeric::float_ulps(aMat1[i][j],aMat2[i][j]) << '\n'
               << " (type : " << libmda::util::type_of(T()) << ")" << '\n'
               << "aMat1 = \n" << aMat1
               << "aMat2 = \n" << aMat2
               ;
            bool was_muted = Mout.Muted();
            Mout.Unmute();
            Mout << ss.str() << std::flush;
            if (was_muted) Mout.Mute();
            return false;
         }
      }
   }
   return true;
}

/**
 *
 **/
template<class T>
bool VectorsAreEqual(const GeneralMidasVector<T>& aVec1, const GeneralMidasVector<T>& aVec2, unsigned aUlps = 4)
{
   MidasAssert(aVec1.Size() == aVec2.Size(), "Wrong size of MidasVectors: " + std::to_string(aVec1.Size()) + " vs. " + std::to_string(aVec2.Size()));

   for(int i=0; i<aVec1.Size(); ++i)
   {
      if(!libmda::numeric::float_eq(aVec1[i],aVec2[i],aUlps))
      {
         std::stringstream ss;
         ss << std::scientific << std::setprecision(16);
         ss << "aVec1["<<i<<"] = " << aVec1[i] << '\n'
            << "aVec2["<<i<<"] = " << aVec2[i] << '\n'
            << "float_ulps = " << libmda::numeric::float_ulps(aVec1[i],aVec2[i]) << '\n'
            << " (type : " << libmda::util::type_of(T()) << ")" << '\n'
            << "aVec1 = " << aVec1
            << "aVec2 = " << aVec2
            ;
         bool was_muted = Mout.Muted();
         Mout.Unmute();
         Mout << ss.str() << std::flush;
         if (was_muted) Mout.Mute();
         return false;
      }
   }
   return true;
}

} /* namespace check */
} /* namespace test */
} /* namespace midas */

#endif /* MATRICIESAREEQUAL_H_INCLUDED */
