#include "MatricesAreEqual.h"

#include "libmda/numeric/float_eq.h"

#include "mmv/MidasMatrix.h"

#include "util/MidasStream.h"
extern MidasStream Mout;

namespace midas
{
namespace test
{
namespace check
{

//bool MatricesAreEqual(const MidasMatrix& aMat1, const MidasMatrix& aMat2, unsigned aUlps)
//{
//   assert(aMat1.Nrows() == aMat2.Nrows());
//   assert(aMat1.Ncols() == aMat2.Ncols());
//
//   for(int i=0; i<aMat1.Nrows(); ++i)
//   {
//      for(int j=0; j<aMat1.Ncols(); ++j)
//      {
//         //Mout << aMat1[i][j] << " " << aMat2[i][j] << " " << libmda::numeric::float_ulps(aMat1[i][j],aMat2[i][j]) << std::endl;
//         if(!libmda::numeric::float_eq(aMat1[i][j],aMat2[i][j],aUlps))
//         {
//            return false;
//         }
//      }
//   }
//   return true;
//}


//bool VectorsAreEqual(const MidasVector& aVec1, const MidasVector& aVec2, unsigned aUlps)
//{
//   assert(aVec1.Size() == aVec2.Size());
//
//   for(int i=0; i<aVec1.Size(); ++i)
//   {
//      if(!libmda::numeric::float_eq(aVec1[i],aVec2[i],aUlps))
//      {
//         return false;
//      }
//   }
//   return true;
//}

} /* namespace check */
} /* namespace test */
} /* namespace midas */
