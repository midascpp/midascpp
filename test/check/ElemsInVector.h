#ifndef ELEMSINVECTOR_H_INCLUDED
#define ELEMSINVECTOR_H_INCLUDED

#include "Compare.h"

namespace midas
{
namespace test
{
namespace check
{
namespace detail
{


template<size_t N, size_t M>
struct ElemsInVectorImpl
{
   template<class T, class Tuple, class Comp>
   static bool Apply(const T& vec, const Tuple& t, const Comp& comp)
   {
      if(comp(vec.at(N),std::get<N>(t)))
      {
         return ElemsInVectorImpl<N+1,M>::Apply(vec,t,comp);
      }
      else
      {
         return false;
      }
   }
};

template<size_t N>
struct ElemsInVectorImpl<N,N>
{
   template<class T, class Tuple, class Comp>
   static bool Apply(const T& vec, const Tuple& t, const Comp& comp)
   {
      return true;
   }
};

} /* namespace detail */

template<class T, class Tuple, class Comp = DefaultCompare>
bool ElemsInVector(const T& vec, Tuple&& t, Comp comp = DefaultCompare() )
{
   if(vec.size() != std::tuple_size<Tuple>::value)
   {
      return false;
   }
   else
   {
      return detail::ElemsInVectorImpl<0
                                     , std::tuple_size<Tuple>::value
                                     >::Apply(vec,t,comp);  
   }
}

} /* namespace midas */
} /* namespace test */
} /* namespace check */

#endif /* ELEMSINVECTOR_H_INCLUDED */
