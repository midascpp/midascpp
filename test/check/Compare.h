#ifndef COMPARE_H_INCLUDED
#define COMPARE_H_INCLUDED

#include "libmda/numeric/float_eq.h"

namespace midas
{
namespace test
{
namespace check
{

struct DefaultCompare
{
   template<class T, class U>
   bool operator()(T&& t, U&& u) const
   {
      return (t==u);
   }
};

struct FloatCompare
{
   unsigned max_ulps = 2;
   FloatCompare() {};
   FloatCompare(unsigned u): max_ulps(u) {};

   template<class T, class U>
   bool operator()(T&& t, U&& u) const
   {
      return libmda::numeric::float_eq(t,u,max_ulps);
   }
};

} /* namespace check */
} /* namespace test */
} /* namespace midas */

#endif /* COMPARE_H_INCLUDED */
