#include "SinglePointTest.h"

namespace midas::test
{

void SinglePointTest()
{
   cutee::suite suite("SinglePoint test");
   
   //suite.add_test<GenericSinglePointConstructionTest>("Generic Singlepoint construction test");
   //suite.add_test<DaltonSinglePointConstructionTest>("DALTON Singlepoint construction test");
   
   RunSuite(suite);
}

} /* namespace midas::test */
