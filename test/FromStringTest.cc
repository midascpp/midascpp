#include "test/FromStringTest.h"

#include "test/MidasExpressionTest.h"

namespace midas::test
{

void FromStringTest()
{
   cutee::suite suite("FromString test");

   // add tests
   suite.add_test<StringToNumberTest>("Test of string to In and Nb");
   suite.add_test<StringToVectorTest>("Test of string to vector<In> and vector<Nb>");
   suite.add_test<StringToVectorTestAdv>("Test of string to vector<In>, using expansions");
   suite.add_test<ExpandToVectorTest>("Test of midas general expressions");
   suite.add_test<TupleFromStringTest>("Test TupleFromString.");
   
   // run tests
   RunSuite(suite);
}
} /* namespace midas::test */
