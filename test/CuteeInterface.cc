#include "test/CuteeInterface.h"

#include <regex>
#include <sstream>
#include <string>

// Include midas stream, and define Mout
#include "util/MidasStream.h"

extern MidasStream Mout;

namespace midas
{
namespace test
{

/*!
 * Wrapper for running cutee test suite.
 * Will check version of cutee library and run the suite accordingly,
 * as more options are available for new 2.x.x version of cutee.
 * If suite fails will print suite output to standard out.
 *
 * @param suite   The test suite to run.
 */
void RunSuite(cutee::suite& suite)
{
   std::stringstream sstr;
   std::stringstream fancy_sstr;
   
   // Mute Mout (we don't want output from the code, only the test suite).
   Mout.Mute();

#if defined(CUTEE_VERSION)
   // if CUTEE_VERSION is defined we are at least version 2.0.0
   cutee::writer_collection wc
      {  std::forward_as_tuple(sstr      , cutee::format::raw)
      ,  std::forward_as_tuple(fancy_sstr, cutee::format::fancy)
      };

   suite.run(wc);
#else
   // cutee version 1.x.x
   suite.run(sstr);
#endif /* CUTEE_VERSION */
   
   auto str       = sstr.str();
   auto fancy_str = fancy_sstr.str();
   
   // Output to Mout
   Mout.Unmute();
   Mout << str << std::flush;
   
   // If we hit a failed test output to std::cout
   std::regex e("FAILED");
   std::smatch sm;
   if(std::regex_search(str, sm, e))
   {
      if(!fancy_str.empty())
      {
         std::cout << fancy_str << std::flush;
      }
      else
      {
         std::cout << str << std::flush;
      }
   }
}

} /* namespace test */
} /* namespace midas */
