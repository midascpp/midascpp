/**
 *******************************************************************************
 * 
 * @file    PesIntegralsTest_Impl.h
 * @date    20-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for pes/integrals classes.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PESINTEGRALSTEST_IMPL_H_INCLUDED
#define PESINTEGRALSTEST_IMPL_H_INCLUDED

#include <functional>

#include "pes/integrals/SumOverProductIndexer.h"
#include "pes/integrals/ModeCombiIntegrals.h"
#include "util/Error.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"

namespace midas{
namespace test{
namespace pes_integrals{

namespace onemodeintegrals{

template<typename T>
std::vector<T> FunctionGenerator<T>::Functions() const
{
   MIDASERROR("Only works for template specialized versions.");
   return std::vector<T>();
}

template<typename T>
Nb FunctionGenerator<T>::IntegralFromAntiDerivative
   (  Uin aFunc
   ,  Nb aIntBegin
   ,  Nb aIntEnd
   ,  Nb aScale
   )
{
   const auto& anti = anti_derivatives.at(aFunc);
   return (anti(aScale*aIntEnd) - anti(aScale*aIntBegin))/aScale;
}

/***************************************************************************//**
 * @brief
 *    Specialization for std::function<Nb(Nb)>.
 ******************************************************************************/
template<>
std::vector<std::function<Nb(Nb)>>
FunctionGenerator<std::function<Nb(Nb)>>::Functions() const
{
   std::vector<function_t> func;
   func.emplace_back([](Nb x){return 1 + x;});
   func.emplace_back([](Nb x){return pow(x,2);});
   func.emplace_back([](Nb x){return 1 + sin(x);});
   func.emplace_back([](Nb x){return 1 + cos(x);});
   func.emplace_back([](Nb x){return exp(x);});
   return func;
}

/***************************************************************************//**
 * @brief
 *    Specialization for GenericFunctionWrapper<Nb>.
 ******************************************************************************/
template<>
std::vector<GenericFunctionWrapper<Nb>>
FunctionGenerator<GenericFunctionWrapper<Nb>>::Functions() const
{
   std::vector<function_t> func;
   FunctionContainer<Nb> func_cont;
   std::vector<std::string> v_var = {"x"};
   func.emplace_back(std::string("1+x"), func_cont, v_var);
   func.emplace_back(std::string("x^2"), func_cont, v_var);
   func.emplace_back(std::string("1+SIN(x)"), func_cont, v_var);
   func.emplace_back(std::string("1+COS(x)"), func_cont, v_var);
   func.emplace_back(std::string("EXP(x)"), func_cont, v_var);
   return func;
}

/***************************************************************************//**
 * @brief
 *    Check integral values. Templated for various functor types.
 ******************************************************************************/
template<typename F>
void Integrals<F>::run()
{
   using func_t = F;
   FunctionGenerator<func_t> gen;
   auto func = gen.Functions();
   auto intervals = gen.intervals;

   OneModeIntegrals<func_t> omi(12, func, intervals);

   for(Uin i = I_0; i < omi.NumIntervals(); ++i)
   {
      const auto& i_beg = intervals.at(i).first;
      const auto& i_end = intervals.at(i).second;
      for(Uin f = I_0; f < omi.NumFunctions(); ++f)
      {
         Nb val = omi.Integral(i, f);
         Nb ctrl = gen.IntegralFromAntiDerivative(f, i_beg, i_end);
         UNIT_ASSERT_FEQUAL_PREC(val, ctrl, 10,
            "Bad integral; function "+std::to_string(f)+
            " on interval "+std::to_string(i)+
            " ["+std::to_string(i_beg)+";"+std::to_string(i_end)+"]."
            );
      }
   }
}

/***************************************************************************//**
 * @brief
 *    Check integral values, non-unit scaling factor. Templated for various
 *    functor types.
 ******************************************************************************/
template<typename F>
void IntegralsScaling<F>::run()
{
   Nb scaling = .42;
   using func_t = F;
   FunctionGenerator<func_t> gen;
   auto func = gen.Functions();
   auto intervals = gen.intervals;

   OneModeIntegrals<func_t> omi(12, func, intervals, scaling);

   for(Uin i = I_0; i < omi.NumIntervals(); ++i)
   {
      const auto& i_beg = intervals.at(i).first;
      const auto& i_end = intervals.at(i).second;
      for(Uin f = I_0; f < omi.NumFunctions(); ++f)
      {
         Nb val = omi.Integral(i, f);
         Nb ctrl = gen.IntegralFromAntiDerivative(f, i_beg, i_end, scaling);
         UNIT_ASSERT_FEQUAL_PREC(val, ctrl, 10,
            "Bad integral; function "+std::to_string(f)+
            " on interval "+std::to_string(i)+
            " ["+std::to_string(i_beg)+";"+std::to_string(i_end)+"]."
            );
      }
   }
}


} /* namespace onemodeintegrals */

/***************************************************************************//**
 * @brief Namespace for testing pes::integrals::SumOverProductIndexer.
 ******************************************************************************/
namespace sumoverproductindexer{

template<typename T>
std::vector<std::vector<T>> SumOverProductGenerator<T>::Functions() const
{
   MIDASERROR("Only works for template specialized versions.");
   return std::vector<std::vector<T>>();
}

template<typename T>
bool SumOverProductGenerator<T>::Equality(const T&, const T&)
{
   MIDASERROR("Only works for template specialized versions.");
   return false;
}

/***************************************************************************//**
 * @brief
 *    Specialization for std::function<Nb(Nb)>.
 ******************************************************************************/
template<>
std::vector<std::vector<std::function<Nb(Nb)>>>
SumOverProductGenerator<std::function<Nb(Nb)>>::Functions() const
{
   return std::vector<std::vector<std::function<Nb(Nb)>>>
      {  {  [](Nb x) -> Nb {return x;}
         ,  [](Nb x) -> Nb {return x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      ,  {  [](Nb x) -> Nb {return x*x;}
         ,  [](Nb x) -> Nb {return x*x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      ,  {  [](Nb x) -> Nb {return x;}
         ,  [](Nb x) -> Nb {return x*x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      ,  {  [](Nb x) -> Nb {return x*x;}
         ,  [](Nb x) -> Nb {return x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      ,  {  [](Nb x) -> Nb {return x*x*x;}
         ,  [](Nb x) -> Nb {return x*x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      ,  {  [](Nb x) -> Nb {return x*x*x;}
         ,  [](Nb x) -> Nb {return x;}
         ,  [](Nb x) -> Nb {return x;}
         }
      };
}

/***************************************************************************//**
 * @brief
 *    Specialization for std::function<Nb(Nb)>.
 *
 * Equal if the function values are equal at x = 2.0.
 * This is _not_ a proper way to generally check equality of functions, but
 * it'll work in this case.
 ******************************************************************************/
template<>
bool SumOverProductGenerator<std::function<Nb(Nb)>>::Equality
   (  const std::function<Nb(Nb)>& arF
   ,  const std::function<Nb(Nb)>& arG
   )
{
   return arF(2.) == arG(2.);
}


/***************************************************************************//**
 * @brief
 *    Specialization for GenericFunctionWrapper<Nb>
 ******************************************************************************/
template<>
std::vector<std::vector<GenericFunctionWrapper<Nb>>>
SumOverProductGenerator<GenericFunctionWrapper<Nb>>::Functions() const
{
   FunctionContainer<Nb> func_cont;
   std::vector<std::string> v_var = {"x"};
   GenericFunctionWrapper<Nb> f(std::string("x^1"), func_cont, v_var);
   GenericFunctionWrapper<Nb> g(std::string("x^2"), func_cont, v_var);
   GenericFunctionWrapper<Nb> h(std::string("x^3"), func_cont, v_var);
   return std::vector<std::vector<GenericFunctionWrapper<Nb>>>
      {  {  f, f, f  }
      ,  {  g, g, f  }
      ,  {  f, g, f  }
      ,  {  g, f, f  }
      ,  {  h, g, f  }
      ,  {  h, f, f  }
      };
}

/***************************************************************************//**
 * @brief
 *    Specialization for GenericFunctionWrapper<Nb>
 *
 * Equal if comparing equal according to
 * GenericFunctionWrapper<T>::Compare(const GenericFunctionWrapper<T>&).
 ******************************************************************************/
template<>
bool SumOverProductGenerator<GenericFunctionWrapper<Nb>>::Equality
   (  const GenericFunctionWrapper<Nb>& arF
   ,  const GenericFunctionWrapper<Nb>& arG
   )
{
   return arF.Compare(arG);
}



/***************************************************************************//**
 * @brief
 *    Templated test that constructs and checks for some various functor types.
 ******************************************************************************/
template<typename T>
void ConstructAndCheck<T>::run()
{
   using pes::integrals::SumOverProductIndexer;

   SumOverProductGenerator<T> gen;
   std::vector<Nb> c = gen.coefficients;
   std::vector<std::vector<T>> f = gen.Functions();
   std::function<bool(const T&, const T&)> eq = gen.Equality;

   SumOverProductIndexer<T> sop(c, f, eq);

   UNIT_ASSERT_EQUAL(sop.NumModes(), gen.ctrl_num_modes, "Wrong num. modes.");
   UNIT_ASSERT_EQUAL(sop.NumTerms(), gen.ctrl_num_terms, "Wrong num. terms.");
   for(Uin m = I_0; m < sop.NumModes(); ++m)
   {
      UNIT_ASSERT_EQUAL(sop.NumFunctions(m), gen.ctrl_num_funcs.at(m), "Wrong num. funcs, mode "+std::to_string(m));
   }
   for(Uin t = I_0; t < sop.NumTerms(); ++t)
   {
      UNIT_ASSERT_FEQUAL(sop.Coefficient(t), gen.coefficients.at(t), "Wrong coefficient, term "+std::to_string(t));
      auto ctrl_ind = gen.ctrl_indices.at(t);
      auto sop_ind = sop.FunctionIndices(t);
      UNIT_ASSERT_EQUAL(sop_ind.size(), ctrl_ind.size(), "Num. indices, term "+std::to_string(t));
      for(Uin m = I_0; m < sop_ind.size(); ++m)
      {
         UNIT_ASSERT_EQUAL(sop_ind[m], ctrl_ind[m], "Term "+std::to_string(t)+", function index "+std::to_string(m));
      }
   }
}

} /* namespace sumoverproductindexer */

namespace modecombiintegrals{
template<typename T>
std::vector<std::vector<T>> SumOverProductGenerator<T>::Functions() const
{
   MIDASERROR("Only works for template specialized versions.");
   return std::vector<std::vector<T>>();
}

template<typename T>
bool SumOverProductGenerator<T>::Equality(const T&, const T&)
{
   MIDASERROR("Only works for template specialized versions.");
   return false;
}

/***************************************************************************//**
 * @brief
 *    Specialization for std::function<Nb(Nb)>.
 ******************************************************************************/
template<>
std::vector<std::vector<std::function<Nb(Nb)>>> 
SumOverProductGenerator<std::function<Nb(Nb)>>::Functions() const
{
   auto x  = [](Nb x) -> Nb {return x;};
   auto x2 = [](Nb x) -> Nb {return x*x;};
   return std::vector<std::vector<std::function<Nb(Nb)>>>
      {  {  x, x  }
      ,  {  x2,x  }
      ,  {  x, x2 }
      ,  {  x2,x2 }
      };
}

/***************************************************************************//**
 * @brief
 *    Specialization for std::function<Nb(Nb)>.
 ******************************************************************************/
template<>
bool SumOverProductGenerator<std::function<Nb(Nb)>>::Equality
   (  const std::function<Nb(Nb)>& arF
   ,  const std::function<Nb(Nb)>& arG
   )
{
   return arF(2.) == arG(2.);
}

/***************************************************************************//**
 * @brief
 *    Specialization for GenericFunctionWrapper<Nb>.
 ******************************************************************************/
template<>
std::vector<std::vector<GenericFunctionWrapper<Nb>>> 
SumOverProductGenerator<GenericFunctionWrapper<Nb>>::Functions() const
{
   FunctionContainer<Nb> func_cont;
   std::vector<std::string> v_var = {"x"};
   GenericFunctionWrapper<Nb>  x(std::string("x^1"), func_cont, v_var);
   GenericFunctionWrapper<Nb> x2(std::string("x^2"), func_cont, v_var);
   return std::vector<std::vector<GenericFunctionWrapper<Nb>>>
      {  {  x, x  }
      ,  {  x2,x  }
      ,  {  x, x2 }
      ,  {  x2,x2 }
      };
}

/***************************************************************************//**
 * @brief
 *    Specialization for GenericFunctionWrapper<Nb>.
 ******************************************************************************/
template<>
bool SumOverProductGenerator<GenericFunctionWrapper<Nb>>::Equality
   (  const GenericFunctionWrapper<Nb>& arF
   ,  const GenericFunctionWrapper<Nb>& arG
   )
{
   return arF.Compare(arG);
}

/***************************************************************************//**
 * @brief
 *    Constructs and checks calculated integrals for template functor type.
 ******************************************************************************/
template<typename T>
void ConstructAndCheck<T>::run()
{
   using pes::integrals::SumOverProductIndexer;
   using pes::integrals::ModeCombiIntegrals;

   Uin num_quad_points = I_12;
   SumOverProductGenerator<T> gen;
   const auto& intervals = gen.intervals;
   std::vector<Nb> scaling_factor(intervals.size(), C_1);
   SumOverProductIndexer<T> sop(gen.coefficients, gen.Functions(), gen.Equality);

   ModeCombiIntegrals<T> mci(std::move(sop), intervals, num_quad_points, scaling_factor);

   decltype(mci.NumModes()) ctrl_n_modes = intervals.size();
   UNIT_ASSERT_EQUAL(mci.NumModes(), ctrl_n_modes, "Wrong num. modes.");
   for(Uin k = I_0; k < intervals.size(); ++k)
   {
      decltype(mci.NumIntervals(k)) ctrl_n_ints = intervals[k].size();
      UNIT_ASSERT_EQUAL(mci.NumIntervals(k), ctrl_n_ints, "Wrong num. intervals, mode "+std::to_string(k));
   }

   for(Uin i = I_0; i < mci.NumIntervals(I_0); ++i)
   {
      for(Uin j = I_0; j < mci.NumIntervals(I_1); ++j)
      {
         std::vector<Uin> v_ind = {i, j};
         Nb val = mci.Integral(v_ind);
         Nb ctrl = gen.ctrl[i][j];
         std::stringstream err;
         err   << "Bad integral. Interval index: (" << i << "," << j << ").";
         UNIT_ASSERT_FEQUAL_PREC(val, ctrl, 60, err.str());
      }
   }
}

} /* namespace modecombiintegrals */

} /* namespace pes_integrals */
} /* namespace test */
} /* namespace midas */

#endif/*PESINTEGRALSTEST_IMPL_H_INCLUDED*/
