/**
************************************************************************
* 
* @file                VccStateSpaceTest.cc
*
* Created:             24-11-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Unit tests for VccStateSpace.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>
#include <sstream>

#include "test/VccStateSpaceTest.h"
#include "util/Error.h"

//Some utility functions used in the testing:
namespace midas::test
{
namespace vcc_state_space
{

//Utility functions used in the testing:
namespace detail
{
   ModeCombiOpRange MakeMCR(char aSwitch)
   {
      MidasAssert(n_modes <= n_modals.size(), "More modes than size of n_modals vector.");
      ModeCombiOpRange mcr(n_exc, n_modes);

      switch(aSwitch)
      {
         case 'a':
         {
            break;
         }
         case 'b':
         {
            RemoveElements(mcr, n_modals, skip_stride, skip_begin);
            break;
         }
         default:
         {
            MIDASERROR("Bad switch.");
            break;
         }
      }

      PolishMCR(mcr, n_modals);
      return mcr;
   }

   std::vector<Uin> TestAddresses(char aSwitch)
   {
      std::vector<Uin> v;
      switch(aSwitch)
      {
         case 'a':
         {
            v = {1337, 29, 3510};
            break;
         }
         case 'b':
         {
            v = {42,0,666};
            break;
         }
         default:
         {
            MIDASERROR("Bad switch.");
            break;
         }
      }

      return v;
   }

   std::vector<Uin> TestRefNumbers(bool aEraseRef, char aSwitch)
   {
      std::vector<Uin> v;
      if(aSwitch == 'a')
      {
         if(!aEraseRef)
         {
            v = {24,5,32};
         }
         else
         {
            v = {23,9,31};
         }
      }
      else if(aSwitch == 'b')
      {
         if(!aEraseRef)
         {
            v = {6, 1, 15};
         }
         else
         {
            v = {6, 1, 15};
         }
      }
      else
      {
         MIDASERROR("Bad switch.");
      }
      return v;
   }

   std::vector<std::vector<Uin> > TestIndices(bool aEraseRef, char aSwitch)
   {
      std::vector<std::vector<Uin> > v;
      if(aSwitch == 'a')
      {
         if(!aEraseRef)
         {
            v = {{7,2,11}, {11}, {}};
         }
         else
         {
            v = {{7,3,0}, {0,0}, {}};
         }
      }
      else if(aSwitch == 'b')
      {
         if(!aEraseRef)
         {
            v = {{3,3}, {0}, {5,2,6}};
         }
         else
         {
            v = {{3,3}, {0}, {5,2,6}};
         }
      }
      else
      {
         MIDASERROR("Bad switch.");
      }
      return v;
   }

   std::vector<std::vector<Uin> > TestVecOfDims(char aSwitch, bool aEraseRef)
   {
      std::vector<std::vector<Uin> > dims =
      {
         // Empty MC. (1)
         {},
         // 1-mode MCs. (5)
         {0},
         {8},
         {5},
         {4},
         {12}, 
         // 2-mode MCs. (10)
         {0,8},      {0,5},    {0,4},  {0,12},
         {8,5},      {8,4},    {8,12},
         {5,4},      {5,12},
         {4,12},
         // 3-mode MCs. (10)
         {0,8,5},    {0,8,4},  {0,8,12}, {0,5,4}, {0,5,12}, {0,4,12},
         {8,5,4},    {8,5,12}, {8,4,12}, 
         {5,4,12},
         // 4-mode MCs. (5)
         {0,8,5,4},
         {0,8,5,12},
         {0,8,4,12},
         {0,5,4,12},
         {8,5,4,12},
         // Full MC. (1)
         {0,8,5,4,12}
      };

      switch(aSwitch)
      {
         case 'a':
         {
            break;
         }
         case 'b':
         {
            RemoveElements(dims, skip_stride, skip_begin);
            break;
         }
         default:
         {
            MIDASERROR("Bad switch.");
            break;
         }
      }

      if(aEraseRef)
      {
         // Iterate backwards since erase() invalidates iterators from the
         // point of erasure an onwards.
         auto&& it = dims.end(), beg = dims.begin();
         while(it != beg)
         {
            --it;
            if(it->size() == I_0)
            {
               dims.erase(it);
            }
         }
      }

      return dims;
   }

   void PolishMCR(ModeCombiOpRange& arMCR, const std::vector<In>&  arNModals)
   {
      SetAddresses(arMCR, arNModals);
   }

   void RemoveElements(ModeCombiOpRange& arMCR, const std::vector<In>& arAllNModals, Uin aN, Uin aBegin)
   {
      std::vector<ModeCombi> v_mcs_erase;
      Uin n = I_0;
      for(auto it = arMCR.begin()+aBegin, end = arMCR.end(); it < end; ++it, ++n)
      {
         if(n % aN == I_0)
         {
            v_mcs_erase.push_back(*it);
         }
      }
      arMCR.Erase(v_mcs_erase);
      PolishMCR(arMCR, arAllNModals);
   }

   void RemoveElements(std::vector<std::vector<Uin> >& arDims, Uin aN, Uin aBegin)
   {
      if(aBegin >= arDims.size())
      {
         return;
      }
      else
      {
         // Set up iterators.
         auto&& it  = arDims.begin();
         std::advance(it, aBegin);
         Uin n = I_0;
         while(it != arDims.end())
         {
            if(n % aN == I_0)
            {
               it = arDims.erase(it);
            }
            else
            {
               ++it;
            }
            ++n;
         }
      }
   }

   std::vector<Uin> SizesFromDims(const std::vector<std::vector<Uin>>& arDims)
   {
      std::vector<Uin> vec_sizes;
      vec_sizes.reserve(arDims.size());
      for(const auto& dims: arDims)
      {
         Uin size = I_1;
         for(const auto& d: dims)
         {
            size *= d;
         }
         vec_sizes.push_back(size);
      }
      return vec_sizes;
   }

   std::vector<Uin> AddressesFromSizes(const std::vector<Uin>& arSizes)
   {
      // Construct result vector. Reserve space corresponding to size of
      // argument. +1 for the first-beyond-last address.
      std::vector<Uin> vec_addr;
      vec_addr.reserve(arSizes.size() + I_1);
      Uin addr = I_0;
      vec_addr.push_back(addr);
      for(const auto& s: arSizes)
      {
         addr += s;
         vec_addr.push_back(addr);
      }
      return vec_addr;
   }

   std::string Info(char aType, bool aEraseRef)
   {
      std::stringstream info;
      info << "(MCR type: " << aType << "; erase ref.: " << std::boolalpha << aEraseRef << ").";
      return info.str();
   }
} /* namespace detail */
} /* namespace vcc_state_space */

//The tests:
void VccStateSpaceTest()
{
   cutee::suite suite("VccStateSpace test");
   
   /**************************************************************************************
    * VccStateSpace tests
    **************************************************************************************/
   using namespace vcc_state_space;
   suite.add_test<Constructor>("Constructor");
   suite.add_test<IteratorModeCombis>("Iterator; mode combinations");
   suite.add_test<IteratorDimsAndNModals>("Iterator; dimensions and N_modals");
   suite.add_test<IteratorAddressesAndSizes>("Iterator; addresses, tensor sizes");
   suite.add_test<IteratorContainsAddress>("Iterator; ContainsAddress");
   suite.add_test<IteratorIndexVecFromAddress>("Iterator; GetIndexVecFromAddress");
   suite.add_test<OstreamOperator>("operator<<(std::ostream&, ...)");
   
   //Run tests.
   RunSuite(suite);
}

} /* namespace midas::test */
