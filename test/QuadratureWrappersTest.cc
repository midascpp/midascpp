/**
 *******************************************************************************
 * 
 * @file    QuadratureWrappersTest.cc
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for quadrature wrappers
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <cmath>

#ifdef ENABLE_GSL
#include "quadrature_wrappers/function/GslFunction.h"
#endif /* ENABLE_GSL */

#include "test/QuadratureWrappersTest.h"

#include "util/Error.h"

namespace midas::test::quadrature_wrappers
{

#ifdef ENABLE_GSL
namespace function
{

//! Just a little functor for use in tests.
class NamespaceTestFunctorClass
{
   public:
      double mParam;
      NamespaceTestFunctorClass(double aParam): mParam(aParam) {}
      double operator()(double x) const {return x - mParam;}
};

/***************************************************************************//**
 * @brief
 *    Tests the GslFunctionFromFunctor function for functors of various types;
 *    lambdas, std::function, class with operator() overload.
 ******************************************************************************/
void TestGslFunctionFromFunctor::run()
{
   using midas::quadrature_wrappers::gsl::GslFunctionFromFunctor;
   std::vector<std::tuple<gsl_function, double, double, std::string>> v;

   // Lambda, no capture.
   auto lambda_no_capture = [](double x)->double {return x*x;};
   v.emplace_back(GslFunctionFromFunctor(lambda_no_capture), 2., 4., "Lambda no capture");

   // Lambda with capture.
   double param = 2.;
   auto lambda_with_capture = [&param](double x)->double {return param*x*x;};
   v.emplace_back(GslFunctionFromFunctor(lambda_with_capture), 2., 8., "Lambda with capture");

   // std::function
   std::function<double(double)> std_function = [&param](double x) {return -param*x;};
   v.emplace_back(GslFunctionFromFunctor(std_function), -1., 2., "std::function");

   // Functor class.
   NamespaceTestFunctorClass functor(2.);
   v.emplace_back(GslFunctionFromFunctor(functor), -1., -3., "Functor class");

   for(const auto& t: v)
   {
      auto f = std::get<0>(t);
      double x = std::get<1>(t);
      double val = f.function(x, f.params);
      UNIT_ASSERT_EQUAL(val, std::get<2>(t), std::get<3>(t));
   }
}

} /* namespace function */
#endif /* ENABLE_GSL */


namespace integration
{

/***************************************************************************//**
 *
 ******************************************************************************/
std::pair<std::vector<function_t>, std::vector<function_t>> TestFunctionsAndAntiDerivatives()
{
   std::vector<function_t> func;
   std::vector<function_t> anti;
   // f(x) = 1 + x. F(x) = x + 1/2 x^2.
   func.emplace_back([](Nb x){return 1 + x;});
   anti.emplace_back([](Nb x){return x + pow(x,2)/2.;});
   // f(x) = x^2. F(x) = 1/3 x^3.
   func.emplace_back([](Nb x){return pow(x,2);});
   anti.emplace_back([](Nb x){return pow(x,3)/3.;});
   // f(x) = 1 + sin(x). F(x) = x - cos(x).
   func.emplace_back([](Nb x){return 1 + sin(x);});
   anti.emplace_back([](Nb x){return x - cos(x);});
   // f(x) = 1 + cos(x). F(x) = x + sin(x).
   func.emplace_back([](Nb x){return 1 + cos(x);});
   anti.emplace_back([](Nb x){return x + sin(x);});
   // f(x) = exp(x). F(x) = exp(x).
   func.emplace_back([](Nb x){return exp(x);});
   anti.emplace_back([](Nb x){return exp(x);});

   return std::pair<std::vector<function_t>,std::vector<function_t>>{func, anti};
}

/***************************************************************************//**
 *
 ******************************************************************************/
std::vector<std::pair<Nb,Nb>> TestIntervals()
{
   return std::vector<std::pair<Nb,Nb>>
      {  { -1.0, 1.0}
      ,  {  0.0, 1.0}
      ,  {-C_PI, C_PI}
      ,  {  0.0, C_PI}
      };
}

/***************************************************************************//**
 *
 ******************************************************************************/
Nb IntegralFromAntiDerivative
   (  const function_t& arAntiDer
   ,  Nb aIntervalBegin
   ,  Nb aIntervalEnd
   )
{
   return arAntiDer(aIntervalEnd) - arAntiDer(aIntervalBegin);
}

/***************************************************************************//**
 * Tests GaussLegendre quadrature wrapper for some test functions with
 * analytical antiderivative.
 ******************************************************************************/
void TestGaussLegendre::run()
{
   auto pair_func_anti = TestFunctionsAndAntiDerivatives();
   auto intervals = TestIntervals();

   std::vector<Uin> orders{12, 24, 96};
   for(const auto& o: orders)
   {
      midas::quadrature_wrappers::GaussLegendre gl(o);
      for(Uin i = 0; i < pair_func_anti.first.size(); ++i)
      {
         auto& f = pair_func_anti.first[i];
         auto& a = pair_func_anti.second[i];
         for(const auto& interval: intervals)
         {
            auto val = gl.Evaluate(f, interval.first, interval.second);
            auto compare = IntegralFromAntiDerivative(a, interval.first, interval.second);
            std::stringstream err;
            err   << "Function " << i
                  << ", interval [" << interval.first
                  << "; " << interval.second
                  << "], order " << o
                  ;
            UNIT_ASSERT_FEQUAL_PREC(val, compare, 10, err.str());
         }
      }
   }
}

} /* integration */

} /* namespace midas::test::quadrature_wrappers */

namespace midas::test
{
void QuadratureWrappersTest()
{
   cutee::suite suite("QuadratureWrappers test");
   
   using namespace quadrature_wrappers;
#ifdef ENABLE_GSL
   suite.add_test<function::TestGslFunctionFromFunctor>("func: GslFunction");
#endif /* ENABLE_GSL */
   suite.add_test<integration::TestGaussLegendre>("num int: GaussLegendre");
   
   // run tests
   RunSuite(suite);
}
} /* namespace midas::test */
