/**
 *******************************************************************************
 * 
 * @file    OrderedCombinationTest.cc
 * @date    27-03-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for OrderedCombination.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <iostream>

#include "test/OrderedCombinationTest.h"


//Some utility functions used in the testing:
namespace midas::test
{

//Utility functions used in the testing:
namespace detail
{
   /************************************************************************//**
    * @param[in] aOc
    *    The OrderedCombination to check for equality against arControl.
    * @param[in] arControl
    *    Vector of std::vector<In> whose indices shall match those of aOc.
    * @param[out] arErrStr
    *    String which will be cleared and assigned information about offending
    *    combination in case of inequality.
    * @return
    *    Whether they were equal.
    ***************************************************************************/
   bool Equality
      (  OrderedCombination aOc
      ,  const std::vector<std::vector<In>>& arControl
      ,  std::string& arErrStr
      )
   {
      std::stringstream err_str;
      bool equality = true;
      Uin count = I_0;
      for(auto iter = arControl.begin(), end = arControl.end(); iter != end; ++iter, ++aOc, ++count)
      {
         // Assert not at end of OrderedCombination yet.
         if (aOc.AtEnd())
         {
            err_str  << "OrderedCombination has size " << count
                     << ", which is less than the control size "
                     << arControl.size()
                     << ".\n";

            equality = false;
            break;
         }

         // Assert equality of vectors.
         if (aOc.GetCombination() != *iter)
         {
            err_str  << "Inequality at combination " << count << ";\n"
                     << "   OrderedCombination: { ";
            for(const auto& i: aOc.GetCombination())
            {
               err_str  << i << ' ';
            }
            err_str  << "}\n"
                     << "   Control:            { ";
            for(const auto& i: *iter)
            {
               err_str  << i << ' ';
            }
            err_str  << "}\n";

            equality = false;
         }
      }

      // Assert we're at end of OrderedCombination now.
      if (!aOc.AtEnd())
      {
         err_str  << "OrderedCombination has size larger than control size "
                  << "(which is " << arControl.size() << ").\n";
         equality = false;
      }

      arErrStr = err_str.str();
      return equality;
   }
} /* namespace detail */

//The tests:
void OrderedCombinationTest()
{
   cutee::suite suite("OrderedCombination test");
   
   /**************************************************************************************
    * OrderedCombination tests
    **************************************************************************************/
   suite.add_test<ConstrDefaultNonFixed>("Constructor, default non-fixed elements");
   suite.add_test<ConstrSuppliedNonFixed>("Constructor, supplied non-fixed elements");
   suite.add_test<ConstrFixedDefaultNonFixed>("Constructor, fixed, default non-fixed");
   suite.add_test<ConstrFixedSuppliedNonFixed>("Constructor, fixed, supplied non-fixed");
   suite.add_test<UtilitySizeBegin>("Utility function, SizeBegin");
   suite.add_test<UtilitySizeEnd>("Utility function, SizeEnd");
   suite.add_test<UtilityNumElementsNonFixed>("Utility function, NumElementsNonFixed");
   suite.add_test<UtilityNumElementsTotal>("Utility function, NumElementsTotal");
   suite.add_test<ModeCombiOperatorLessThan>("Corresponds to ModeCombi operator< order");
   
   //Run tests.
   RunSuite(suite);
}
} /* namespace midas::test */
