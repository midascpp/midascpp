/**
************************************************************************
* 
* @file                FromStringTest.h
*
* Created:             30-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Test case for turning strings into numbers and vectors
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ITSOLVERTEST_H
#define ITSOLVERTEST_H

#include "test/CuteeInterface.h"

#include "it_solver/reduced/ReducedGeneralEigenvalueEquation.h"
#include "it_solver/transformer/MidasDataContTransformer.h"

#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "mmv/Transformer.h"

namespace midas::test
{

std::vector<DataCont> ConstructTrials(In num, In size);
//MidasDataContTransformer<> ConstructMidasDataContTransformer(Transformer& t);
MidasDataContTransformer ConstructMidasDataContTransformer(Transformer& t);

//template<template<class> class container_t, class vector_t>
template<class container_t>
void AddTrialVector(container_t& vec, In num, In size)
{
   for(In i=0; i<num; ++i)
   {
      vec.push_back(DataCont());
      vec.back().SetNewSize(size);
      vec.back().Zero();
      vec.back().DataIo(IO_PUT,vec.size()-1,1.0);
   }
}

struct ReducedGeneralEigenvalueEquationTest: public cutee::test
{
   void run() 
   {
      In num = 2;
      In size = 6;
      
      ReducedGeneralEigenvalueEquation<MidasMatrix,MidasMatrix> rgee;
      auto trials = midas::test::ConstructTrials(num,size);

      rgee.AddToReducedMatrix(trials,trials,matrix_tag::MatA);
      rgee.AddToReducedMatrix(trials,trials,matrix_tag::MatB);
      
      midas::test::AddTrialVector(trials,num,size);

      rgee.AddToReducedMatrix(trials,trials,matrix_tag::MatA);
      rgee.AddToReducedMatrix(trials,trials,matrix_tag::MatB);
      
      MidasVector eigval;
      MidasMatrix eigvec;

      rgee.Evaluate(eigvec,eigval);
      
      UNIT_ASSERT(eigval.size()==2*num,"not correct amount of eigvals");
      UNIT_ASSERT(eigvec.extent<0>()==2*num,"not correct amount of eigvec");
      UNIT_ASSERT(eigvec.extent<1>()==2*num,"not correct amount of eigvec");
   }
};

} /* namespace midas::test */

#endif /* ITSOLVERTEST_H */
