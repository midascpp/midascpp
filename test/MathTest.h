/**
 *******************************************************************************
 * 
 * @file    MathTest.h
 * @date    07-09-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for util/Math.h.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATHTEST_H_INCLUDED
#define MATHTEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "util/Math.h"
#include "inc_gen/math_link.h"

namespace midas::test
{

/**
 *
 **/
template<class T>
struct GreatestCommonDivisorTest : public cutee::test
{
   void run() 
   {
      using midas::util::GreatestCommonDivisor;
      // Tests with zeros.
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(0,0), 0, "gcd(0,0) fail");
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(7,0), 7, "gcd(7,0) fail");
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(0,4), 4, "gcd(0,4) fail");
      // Big numbers.
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(31790,254163), 1, "gcd(31790,254163) fail");
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(10674846,1335180), 42, "gcd(10674846,1335180) fail");
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(1572864,4194304), 524288, "gcd(1572864,4194304) fail");
      UNIT_ASSERT_EQUAL(GreatestCommonDivisor(17381,6435), 13, "gcd(17381,6435) fail");
      // Negative numbers if applicable.
      if (std::numeric_limits<T>::is_signed)
      {
         UNIT_ASSERT_EQUAL(GreatestCommonDivisor(-7,0), 7, "gcd(-7,0) fail");
         UNIT_ASSERT_EQUAL(GreatestCommonDivisor(0,-4), 4, "gcd(0,-4) fail");
         UNIT_ASSERT_EQUAL(GreatestCommonDivisor(-31790,254163), 1, "gcd(-31790,254163) fail");
         UNIT_ASSERT_EQUAL(GreatestCommonDivisor(10674846,-1335180), 42, "gcd(10674846,-1335180) fail");
         UNIT_ASSERT_EQUAL(GreatestCommonDivisor(-17381,-6435), 13, "gcd(-17381,-6435) fail");
      }
   }
};

/**
 * Sinc function
 **/
template<class T>
struct SincTest : public cutee::test
{
   void run()
   {
      UNIT_ASSERT_FEQUAL(midas::math::sinc<T>(0.), T(1.), "sinc(0) != 1");
      UNIT_ASSERT_FEQUAL(midas::math::sinc<T>(1.), std::real(std::sin<T>(1.)), "sinc(1) != sin(1)");
   }
};

} /* namespace midas::test */

#endif/*MATHTEST_H_INCLUDED*/
