/**
************************************************************************
* 
* @file                TensorDataContTest.cc
*
* Created:             24-11-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Unit tests for TensorDataCont.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>

#include "TensorDataContTest.h"
#include "tensor/ZeroTensor.h"

//Some utility functions used in the testing:
namespace midas::test
{

//Utility functions used in the testing:
namespace detail
{
   std::vector<unsigned int> TestDimensionVector()
   {
      std::vector<unsigned int> dim_vec{3, 2, 2, 2};
         ///^ If changing these, also change hardcoded TestNiceTensorXD() funcs.
      return dim_vec;
   }

   NiceTensor<Nb> TestNiceTensor1D(char aSwitch)
   {
      std::vector<unsigned int> dims(midas::test::detail::TestDimensionVector());
      dims.resize(1, 0);
      SimpleTensor<Nb>* st(new SimpleTensor<Nb>(dims));
      Nb* ptr = st->GetData();

      //Fill in data:
      switch (aSwitch)
      {
         case 'a':
         {
            //(Frobenius norm: 3.076758195344850)
            ptr[0] = -2.9599697720e+0;
            ptr[1] =  7.1252228750e-2;
            ptr[2] =  8.3662599850e-1;
            break;
         }
         case 'b':
         {
            //(Frobenius norm:     1.098947522909552)
            //(Dot product with a: 3.246859767068249)
            ptr[0] = -1.0271285950e+0;
            ptr[1] = -2.8159808960e-1;
            ptr[2] =  2.7091515900e-1;
            break;
         }
         default:
         {
            MIDASERROR("Invalid switch-case argument.");
         }
      }
      
      return NiceTensor<Nb>(st);
   }

   NiceTensor<Nb> TestNiceTensor2D(char aSwitch)
   {
      std::vector<unsigned int> dims(midas::test::detail::TestDimensionVector());
      dims.resize(2, 0);
      SimpleTensor<Nb>* st(new SimpleTensor<Nb>(dims));
      Nb* ptr = st->GetData();

      //Fill in data:
      switch (aSwitch)
      {
         case 'a':
         {
            //(Frobenius norm: 4.950744060632982)
            ptr[0*dims[0] + 0] =  6.8414573370e-1;
            ptr[0*dims[0] + 1] = -3.3018351860e-1;
            ptr[0*dims[0] + 2] =  1.6818046290e+0;
            ptr[1*dims[0] + 0] =  3.7351352240e+0;
            ptr[1*dims[0] + 1] =  2.3364559190e+0;
            ptr[1*dims[0] + 2] =  1.3015613700e+0;
            break;
         }
         case 'b':
         {
            //(Frobenius norm:     4.491023010065378)
            //(Dot product with a:-7.090563355058138)
            ptr[0*dims[0] + 0] =  1.2736487520e+0;
            ptr[0*dims[0] + 1] =  2.8968537090e+0;
            ptr[0*dims[0] + 2] = -2.0194934780e+0;
            ptr[1*dims[0] + 0] = -1.2523229950e+0;
            ptr[1*dims[0] + 1] = -6.6585174540e-1;
            ptr[1*dims[0] + 2] =  2.0162638180e+0;
            break;
         }
         default:
         {
            MIDASERROR("Invalid switch-case argument.");
         }
      }

      return NiceTensor<Nb>(st);
   }

   NiceTensor<Nb> TestNiceTensor3D(char aSwitch)
   {
      std::vector<unsigned int> dims(midas::test::detail::TestDimensionVector());
      dims.resize(3, 0);
      SimpleTensor<Nb>* st(new SimpleTensor<Nb>(dims));
      Nb* ptr = st->GetData();

      //Fill in data:
      switch (aSwitch)
      {
         case 'a':
         {
            //(Frobenius norm: 6.836575003299601)
            ptr[(0*dims[1] + 0)*dims[0] + 0] = -5.7664274950e-1;
            ptr[(0*dims[1] + 0)*dims[0] + 1] = -5.9353170480e-1;
            ptr[(0*dims[1] + 0)*dims[0] + 2] = -1.1687495030e+0;
            ptr[(0*dims[1] + 1)*dims[0] + 0] = -8.1572867150e-1;
            ptr[(0*dims[1] + 1)*dims[0] + 1] = -9.1594784470e-1;
            ptr[(0*dims[1] + 1)*dims[0] + 2] =  3.7102943730e+0;
            ptr[(1*dims[1] + 0)*dims[0] + 0] =  1.6464263790e+0;
            ptr[(1*dims[1] + 0)*dims[0] + 1] =  1.2776363480e+0;
            ptr[(1*dims[1] + 0)*dims[0] + 2] = -6.9489396420e-1;
            ptr[(1*dims[1] + 1)*dims[0] + 0] = -6.1725939320e-1;
            ptr[(1*dims[1] + 1)*dims[0] + 1] =  3.0774049720e+0;
            ptr[(1*dims[1] + 1)*dims[0] + 2] =  3.8392635980e+0;
            break;
         }
         case 'b':
         {
            //(Frobenius norm:     9.251558331194406)
            //(Dot product with b:-7.032288283309311)
            ptr[(0*dims[1] + 0)*dims[0] + 0] = -1.7119977530e+0;
            ptr[(0*dims[1] + 0)*dims[0] + 1] =  4.2411480850e+0;
            ptr[(0*dims[1] + 0)*dims[0] + 2] =  3.7236433350e-1;
            ptr[(0*dims[1] + 1)*dims[0] + 0] = -1.0181243370e-1;
            ptr[(0*dims[1] + 1)*dims[0] + 1] = -1.9739478460e+0;
            ptr[(0*dims[1] + 1)*dims[0] + 2] = -6.3636554200e-1;
            ptr[(1*dims[1] + 0)*dims[0] + 0] = -5.2404287490e+0;
            ptr[(1*dims[1] + 0)*dims[0] + 1] =  5.2942903520e+0;
            ptr[(1*dims[1] + 0)*dims[0] + 2] = -8.4823237010e-1;
            ptr[(1*dims[1] + 1)*dims[0] + 0] =  1.8779791600e+0;
            ptr[(1*dims[1] + 1)*dims[0] + 1] =  1.4618372440e-1;
            ptr[(1*dims[1] + 1)*dims[0] + 2] = -6.8068247740e-1;
            break;
         }
         default:
         {
            MIDASERROR("Invalid switch-case argument.");
         }
      }

      return NiceTensor<Nb>(st);
   }
   
   NiceTensor<Nb> TestNiceTensorZero(unsigned int aNdims)
   {
      std::vector<unsigned int> dims(midas::test::detail::TestDimensionVector());
      dims.resize(aNdims, I_0);
      return NiceTensor<Nb>(new ZeroTensor<Nb>(dims));
   }

   /**
    *
    **/
   ModeCombiOpRange MakeMCR(In exci, In modes, const std::vector<In>& nmodals)
   {
      ModeCombiOpRange mcr(exci, modes); 
      SetAddresses(mcr, nmodals);
      return mcr;
   }

   /**
    *
    **/
   ModeCombiOpRange MakeMCRNoRef(const ModeCombiOpRange& arMCR)
   {
      // Make a new one from the ref-to-const by only adding non-empty mode
      // combinations.
      ModeCombiOpRange mcr_new(arMCR);
      Uin empty_mcs = mcr_new.NumEmptyMCs();
      if (empty_mcs > I_0)
      {
         mcr_new.Erase(std::vector<ModeCombi>{ModeCombi(std::vector<In>{}, -I_1)});
         // For good measure, correct the addresses.
         for(auto it = mcr_new.begin(), end = mcr_new.end(); it != end; ++it)
         {
            mcr_new.AssignAddress(it, it->Address() - empty_mcs);
         }
      }
      return mcr_new;
   }

}  /* namespace detail */

//The tests:
void TensorDataContTest()
{
   cutee::suite suite("TensorDataCont test");
   
   /**************************************************************************************
    * TensorDataCont tests
    **************************************************************************************/
   //Test category, e.g. basic, arithmetics, etc.
   suite.add_test<ConstructFromInt>("Constr. from int. (size), empty tensors");
   suite.add_test<ConstructFromIntAndAssignTensors>("Constr. from int. (size), assign tensors");
   suite.add_test<ConstructToShapeWithZeros>("Constr. from MCR and modals, fill all tensors with zeros");
   suite.add_test<ConstructZeroFromVSSAndTypeID>("Constr. zero from VccStateSpace, typeID");
   suite.add_test<ConstructUnitFromVSSAndTypeID>("Constr. unit from VccStateSpace, typeID");
   suite.add_test<ElementwiseAdditionTest>("Testing += operator overload");
   suite.add_test<ElementwiseSubtractionTest>("Testing -= operator overload");
   suite.add_test<ElementwiseProductTest>("Testing *= operator overload");
   suite.add_test<ElementwiseDivisionTest>("Testing /= operator overload");
   suite.add_test<ElementwiseScalarAdditionTest>("Testing elementwise scalar addition");
   suite.add_test<ZeroFunctionTest>("Set all elements to zero, confirm the result");
   suite.add_test<TensorDataContScaleTest>("TensorDataCont Scale");
   suite.add_test<TensorDataContNormalizeTest>("TensorDataCont Normalize");
   suite.add_test<GetMinRankTest>("Search all CanonicalTensors to get minimum rank");
   suite.add_test<GetMaxRankTest>("Search all CanonicalTensors to get maximum rank");
   suite.add_test<AverageRankTest>("Average rank of all CanonicalTensors");
   suite.add_test<AverageRankDifferenceTest>("Average distance to theoretical max. rank of all CanonicalTensors");
   suite.add_test<DataCompressionTest>("Data compression of TensorDataCont. CP/SIMPLE.");
   suite.add_test<MaxNormTest>("Largest tensor norm in TensorDataCont.");
   suite.add_test<DotProductTest>("Dot product, <a|b>");
   suite.add_test<AxpyTest>("Axpy, Y = a*X + Y");
   // Conversion from/to DataCont
   suite.add_test<DataContToTensorDataContTest>("DataCont to TensorDataCont test");
   suite.add_test<DataContToTensorDataContTest2>("DataCont to TensorDataCont back to DataCont test");
   suite.add_test<DataContToTensorDataContRefTest>("DataCont to TensorDataCont test including reference");
   suite.add_test<DataContToTensorDataContRefTest2>("DataCont to TensorDataCont back to DataCont test including reference");
   // Conversion to NiceTensor
   suite.add_test<NiceTensorFromTensorDataContTest>("TensorDataCont to NiceTensor (i.e. truncated CI to FCI representation)");
   // File IO
   suite.add_test<FileIoTest>("Read/Write TensorDataCont from/to file");
   
   //Run tests.
   RunSuite(suite);
}

} /* namespace midas::test */
