/**
************************************************************************
* 
* @file                PolyFitSvdTest.cc
*
* Created:             14-01-2021
*
* Author:              Nicolai Machholdt Høyer
*
* Short Description:   Contains utility functions to test PolyFit class
* 
* Last modified:       10-02-2021
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/FitBasisCalcDef.h"
#include "pes/DoFitting.h"
#include "pes/splines/SplineND.h"
#include "pes/fitting/PolyFit.h"
#include "pes/fitting/FitFunctions.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "test/CuteeInterface.h"
#include "inc_gen/Arch.h"


namespace midas::test
{

namespace polyfitsvd
{
/**
* Fill in function values of polynimial in Value vector arY from given variables and parameters (arX and arPar).
**/
void Polynomial(const std::vector<MidasVector>& arX, const std::vector<MidasVector>& arPar ,  MidasVector& arY)
{
   if (arX.size() != arPar.size())
   {
      MIDASERROR("The size of arX and arPar do not match!");
   }
   // Number of variables
   In dim = arX.size();
   // Determine number of terms in the polynomial
   In Nterms = I_1;
   for (In i = I_0; i < dim; ++i)
   {
      Nterms *= arPar[i].Size();
   }

   // Loop over and fill in output array (arY)
   for (In iY = I_0; iY < arY.Size(); ++iY)
   {
      arY[iY] = C_0;
      // Loop over each term in polynomial
      for (In t = I_0; t < Nterms; ++t)
      {
         Nb term = C_1;
         In cnt_Y = iY;
         In cnt_t = t;
         // Evaluate each term
         for (In idim = dim-I_1; idim >= I_0; --idim)
         {
            // Determine idices
            In ix = cnt_Y % arX[idim].Size();
            In it = cnt_t % arPar[idim].Size();
            cnt_Y /= arX[idim].Size();
            cnt_t /= arPar[idim].Size();

            term *= arPar[idim][it] * pow(arX[idim][ix], it+1);
         }
         arY[iY] += term;
      }
   }
}

/**
* Recursive addition to vector of arPowvectors
**/
void AddPowvecsToVec
   (  std::vector<InVector>& arPowvecVec
   ,  InVector& aInVec
   ,  const In& arNmc
   ,  const In& aCoordNr
   ,  const InVector& arMxPows
   )
{
   for (In i_t = I_0; i_t < arMxPows[aCoordNr]; ++i_t)
   {
      aInVec[aCoordNr] = i_t;
      if (aCoordNr == (arNmc - I_1) ) // we have reached end, add to list
      {
         arPowvecVec.push_back(aInVec);
      }
      else
      {
         AddPowvecsToVec(arPowvecVec, aInVec, arNmc, aCoordNr + I_1, arMxPows);
      }
   }
}

/**
* Setup fitting basis to be used by the PolyFit class
**/
void SetupFitBasis
   (  const FitFunctions& aFitFunctions
   ,  const In& fit_dim
   ,  const InVector& arMxPows
   ,  std::vector<InVector>& BasisFuncOrders
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& in_fit_functions
   )
{
   // Fill in orders of the basis functions to be used in to BasisFuncOrders
   InVector i_cur_vec(fit_dim); // working vector in recursive AddPowvecsToVec function
   for (In i = I_0; i < fit_dim; i++)
   {
      i_cur_vec[i] = I_0;
   }
   AddPowvecsToVec(BasisFuncOrders, i_cur_vec, fit_dim, I_0, arMxPows);

   // Vector to hold the size of all functions
   InVector func_list_sizes(fit_dim, I_0);
   for (In i = I_0; i < BasisFuncOrders.size(); ++i)
   {
      for (In j = I_0; j < BasisFuncOrders[i].size(); ++j)
      {
         if (func_list_sizes[j] < BasisFuncOrders[i][j] + I_1)
         {
            func_list_sizes[j] = BasisFuncOrders[i][j] + I_1;
         }
      }
   }
   
   // Fill in fit functions into in_fit_functions
   std::string aPropType = "ALL";
   for (In i_mt = I_0; i_mt < fit_dim; ++i_mt)
   {
      //In mode_nr = i_mt;
      aFitFunctions.GetFunctions(i_mt, I_0, func_list_sizes[i_mt], aPropType, in_fit_functions);
   }
}

/**
* Test 1D SVD fit to polynomial using PolyFit
**/
struct PolyFitSvd1DTestImpl
   :  public cutee::test
{
   void run() override
   {
      // Set up vectors holding the polynomial coefficients to obtain
      MidasVector func1(I_4);
      func1[I_0] = C_3;
      func1[I_1] = C_6;
      func1[I_2] = C_9;
      func1[I_3] = C_12;

      // Collect the parameter vectors in suitable vector for later use
      std::vector<MidasVector> paramsvec;
      paramsvec.push_back(func1);
      
      // Define the input data
      const Nb low_bound_Xin = -C_5;
      const Nb upp_bound_Xin =  C_5;
      In num_steps_Xin = I_20;
      const Nb step_size_Xin = (upp_bound_Xin - low_bound_Xin)/num_steps_Xin;
      // Increase number of steps by one to hit bounds exactly
      num_steps_Xin++;

      // Create input vector
      MidasVector Xin(num_steps_Xin);
      for (In i = I_0; i < Xin.Size(); i++)
      { 
         Xin[i] = low_bound_Xin + i*step_size_Xin;
      }
      
      // Create vectors to hold the input vectors
      std::vector<MidasVector> Xin_vec;
      Xin_vec.push_back(Xin);

      // Set up basis
      // Use default polynomial basis as fit functions. Orders from 1 up to 12
      FitBasisCalcDef aFitBasisCalcDef;
      FitFunctions aFitFunctions(aFitBasisCalcDef);

      // Setup the correct fit basis
      In fit_dim = paramsvec.size();
      std::vector<std::vector<MidasFunctionWrapper<Nb>* > > fit_functions(fit_dim, std::vector<MidasFunctionWrapper<Nb>* >());
      In MxPow = func1.Size();
      InVector arMxPows(fit_dim, MxPow);
      std::vector<InVector> BasisFuncOrders;
      SetupFitBasis(aFitFunctions, fit_dim, arMxPows, BasisFuncOrders, fit_functions);
      In Nparams = BasisFuncOrders.size();
      
      // Create Values from polynomial functions
      MidasVector Values(Xin.Size());
      Polynomial(Xin_vec, paramsvec, Values);
      // Set variances to 1 for all values
      MidasVector Variance(Values.Size(), 1.0);

      // Set up relevant options to do a SVD fit
      std::string FitMethod = "SVD";
      bool CutSingular = false;
      Nb SvdCutThr = C_I_10_13;
      bool Tikhonov = false;
      Nb TikhonovParam = C_I_10_13;

      // Do the fit
      PolyFit LeastSqrFit(Nparams);
      LeastSqrFit.MakeFit(FitMethod, Values, Xin_vec, Variance, BasisFuncOrders, fit_functions, CutSingular, SvdCutThr, Tikhonov, TikhonovParam);

      // Retrieve least square parameters
      MidasVector least_sqr_params;
      least_sqr_params.SetNewSize(Nparams);
      LeastSqrFit.GetLeastSqrParms(least_sqr_params);

      // Vector containing the coefficients to be compared with
      MidasVector target_params = RefCoefss();

      // Assert that the correct parameters have been determined
      for (In i = I_0; i < least_sqr_params.Size(); ++i)
      {
         UNIT_ASSERT_FEQUAL_PREC(least_sqr_params[i], target_params[i], 2, "SVD fit did not find correct coefficient in 1D fit");
      }
   }

   /**
   * Vector holding the coefficients to be testet against
   **/
   MidasVector RefCoefss()
   {
      MidasVector ref_vals(I_4);

      // 1d fit func1
      #if defined(ARCH_ARM)
         MidasWarning("I have detected ARM architecture and used ARM specific reference data in PolyFitSvdTest: PolyFitSvd1DTestImpl. Is this correct?");
         ref_vals[0] = 3.0000000000136624e+00;
         ref_vals[1] = 6.0000000000000249e+00;
         ref_vals[2] = 8.9999999999991704e+00;
         ref_vals[3] = 1.1999999999999996e+01;
      #else
         ref_vals[0] = 2.9999999999993561e+00;
         ref_vals[1] = 5.9999999999998259e+00;
         ref_vals[2] = 9.0000000000000338e+00;
         ref_vals[3] = 1.2000000000000005e+01;
      #endif /* defined(ARCH_ARM) */
      
      
      return ref_vals;
   }
};

/**
* Test 2D SVD fit to polynomial using PolyFit
**/
struct PolyFitSvd2DTestImpl
   :  public cutee::test
{
   void run() override
   {
      // Set up vectors holding the polynomial coefficients to obtain
      MidasVector func1(I_4);
      func1[I_0] = C_3;
      func1[I_1] = C_6;
      func1[I_2] = C_9;
      func1[I_3] = C_12;
      MidasVector func2(I_4);
      func2[I_0] = C_2;
      func2[I_1] = C_4;
      func2[I_2] = C_8;
      func2[I_3] = C_16;

      // Collect the parameter vectors in suitable vector for later use
      std::vector<MidasVector> paramsvec;
      paramsvec.push_back(func1);
      paramsvec.push_back(func2);
      
      // Define the input data
      const Nb low_bound_Xin = -C_5;
      const Nb upp_bound_Xin =  C_5;
      In num_steps_Xin = I_20;
      const Nb step_size_Xin = (upp_bound_Xin - low_bound_Xin)/num_steps_Xin;
      // Increase number of steps by one to hit bounds exactly
      num_steps_Xin++;

      // Create input vector
      MidasVector Xin(num_steps_Xin);
      for (In i = I_0; i < Xin.Size(); i++)
      { 
         Xin[i] = low_bound_Xin + i*step_size_Xin;
      }
      
      // Create vectors to hold the input vectors (we use the same Xin for all dimensions)
      std::vector<MidasVector> Xin_vec;
      Xin_vec.push_back(Xin);
      Xin_vec.push_back(Xin);

      // Set up basis
      // Use default polynomial basis as fit functions. Orders from 1 up to 12
      FitBasisCalcDef aFitBasisCalcDef;
      FitFunctions aFitFunctions(aFitBasisCalcDef);

      // Setup the correct fit basis
      In fit_dim = paramsvec.size();
      std::vector<std::vector<MidasFunctionWrapper<Nb>* > > fit_functions(fit_dim, std::vector<MidasFunctionWrapper<Nb>* >());
      In MxPow = func1.Size() < func2.Size() ? func2.Size() : func1.Size();
      InVector arMxPows(fit_dim, MxPow);
      std::vector<InVector> BasisFuncOrders;
      SetupFitBasis(aFitFunctions, fit_dim, arMxPows, BasisFuncOrders, fit_functions);
      In Nparams = BasisFuncOrders.size();
      
      // Create Values from polynomial functions
      MidasVector Values(pow(Xin.Size(), fit_dim));
      Polynomial(Xin_vec, paramsvec, Values);
      // Set variances to 1 for all values
      MidasVector Variance(Values.Size(), 1.0);

      // Set up relevant options to do a SVD fit
      std::string FitMethod = "SVD";
      bool CutSingular = false;
      Nb SvdCutThr = C_I_10_13;
      bool Tikhonov = false;
      Nb TikhonovParam = C_I_10_13;

      // Do the fit
      PolyFit LeastSqrFit(Nparams);
      LeastSqrFit.MakeFit(FitMethod, Values, Xin_vec, Variance, BasisFuncOrders, fit_functions, CutSingular, SvdCutThr, Tikhonov, TikhonovParam);

      // Retrieve least square parameters
      MidasVector least_sqr_params;
      least_sqr_params.SetNewSize(Nparams);
      LeastSqrFit.GetLeastSqrParms(least_sqr_params);

      // Vector containing the coefficients to be compared with
      MidasVector target_params = RefCoefss();

      // Assert that the correct parameters have been determined
      In i = I_0;
      for (In x1 = I_0; x1 < paramsvec[0].Size(); ++x1)
      {
         for (In x2 = I_0; x2 < paramsvec[1].Size(); ++x2)
         {
            UNIT_ASSERT_FEQUAL_PREC(least_sqr_params[i], target_params[i], 5, "SVD fit did not find correct coefficient in 2D fit");
            ++i;
         }
      }
   }

   /**
   * Vector holding the coefficients to be testet against
   **/
   MidasVector RefCoefss()
   {
      MidasVector ref_vals(I_4*I_4);

      // 2d fit func1*func2
      ref_vals.SetNewSize(I_4*I_4);
      #if defined(ARCH_ARM)
         MidasWarning("I have detected ARM architecture and used ARM specific reference data in PolyFitSvdTest: PolyFitSvd2DTestImpl. Is this correct?");
         ref_vals[0]  = 5.9999999995002211e+00;
         ref_vals[1]  = 1.2000000000664011e+01;
         ref_vals[2]  = 2.4000000000035520e+01;
         ref_vals[3]  = 4.7999999999971223e+01;
         ref_vals[4]  = 1.2000000000095168e+01;
         ref_vals[5]  = 2.4000000000064350e+01;
         ref_vals[6]  = 4.7999999999988169e+01;
         ref_vals[7]  = 9.5999999999999233e+01;
         ref_vals[8]  = 1.8000000000008590e+01;
         ref_vals[9]  = 3.5999999999988319e+01;
         ref_vals[10] = 7.1999999999997925e+01;
         ref_vals[11] = 1.4400000000000060e+02;
         ref_vals[12] = 2.3999999999991172e+01;
         ref_vals[13] = 4.7999999999998749e+01;
         ref_vals[14] = 9.6000000000000824e+01;
         ref_vals[15] = 1.9200000000000009e+02;
      #else
         ref_vals[0]  = 5.9999999997651976e+00;
         ref_vals[1]  = 1.2000000000028525e+01;
         ref_vals[2]  = 2.4000000000022791e+01;
         ref_vals[3]  = 4.7999999999997463e+01;
         ref_vals[4]  = 1.1999999999691626e+01;
         ref_vals[5]  = 2.4000000000031957e+01;
         ref_vals[6]  = 4.8000000000009045e+01;
         ref_vals[7]  = 9.6000000000001108e+01;
         ref_vals[8]  = 1.7999999999990155e+01;
         ref_vals[9]  = 3.5999999999996291e+01;
         ref_vals[10] = 7.1999999999999346e+01;
         ref_vals[11] = 1.4400000000000057e+02;
         ref_vals[12] = 2.4000000000008750e+01;
         ref_vals[13] = 4.8000000000001265e+01;
         ref_vals[14] = 9.5999999999999943e+01;
         ref_vals[15] = 1.9199999999999972e+02;
      #endif /* defined(ARCH_ARM) */
      
      return ref_vals;
   }
};

/**
* Test 3D SVD fit to polynomial using PolyFit
**/
struct PolyFitSvd3DTestImpl
   :  public cutee::test
{
   void run() override
   {
      // Set up vectors holding the polynomial coefficients to obtain
      MidasVector func1(I_4);
      func1[I_0] = C_3;
      func1[I_1] = C_6;
      func1[I_2] = C_9;
      func1[I_3] = C_12;
      MidasVector func2(I_4);
      func2[I_0] = C_2;
      func2[I_1] = C_4;
      func2[I_2] = C_8;
      func2[I_3] = C_16;
      MidasVector func3(I_4);
      func3[I_0] = C_1;
      func3[I_1] = C_2;
      func3[I_2] = C_3;
      func3[I_3] = C_4;

      // Collect the parameter vectors in suitable vector for later use
      std::vector<MidasVector> paramsvec;
      paramsvec.push_back(func1);
      paramsvec.push_back(func2);
      paramsvec.push_back(func3);
      
      // Define the input data
      const Nb low_bound_Xin = -C_5;
      const Nb upp_bound_Xin =  C_5;
      In num_steps_Xin = I_20;
      const Nb step_size_Xin = (upp_bound_Xin - low_bound_Xin)/num_steps_Xin;
      // Increase number of steps by one to hit bounds exactly
      num_steps_Xin++;

      // Create input vector
      MidasVector Xin(num_steps_Xin);
      for (In i = I_0; i < Xin.Size(); i++)
      { 
         Xin[i] = low_bound_Xin + i*step_size_Xin;
      }
      
      // Create vectors to hold the input vectors (we use the same Xin for all dimensions)
      std::vector<MidasVector> Xin_vec;
      Xin_vec.push_back(Xin);
      Xin_vec.push_back(Xin);
      Xin_vec.push_back(Xin);

      // Set up basis
      // Use default polynomial basis as fit functions. Orders from 1 up to 12
      FitBasisCalcDef aFitBasisCalcDef;
      FitFunctions aFitFunctions(aFitBasisCalcDef);

      // Setup the correct fit basis
      In fit_dim = paramsvec.size();
      std::vector<std::vector<MidasFunctionWrapper<Nb>* > > fit_functions(fit_dim, std::vector<MidasFunctionWrapper<Nb>* >());
      In MxPow = func1.Size() < func2.Size() ? func2.Size() : func1.Size();
      InVector arMxPows(fit_dim, MxPow);
      std::vector<InVector> BasisFuncOrders;
      SetupFitBasis(aFitFunctions, fit_dim, arMxPows, BasisFuncOrders, fit_functions);
      In Nparams = BasisFuncOrders.size();
      
      // Create Values from polynomial functions
      MidasVector Values(pow(Xin.Size(), fit_dim));
      Polynomial(Xin_vec, paramsvec, Values);
      // Set variances to 1 for all values
      MidasVector Variance(Values.Size(), 1.0);

      // Set up relevant options to do a SVD fit
      std::string FitMethod = "SVD";
      bool CutSingular = false;
      Nb SvdCutThr = C_I_10_13;
      bool Tikhonov = false;
      Nb TikhonovParam = C_I_10_13;

      // Do the fit
      PolyFit LeastSqrFit(Nparams);
      LeastSqrFit.MakeFit(FitMethod, Values, Xin_vec, Variance, BasisFuncOrders, fit_functions, CutSingular, SvdCutThr, Tikhonov, TikhonovParam);

      // Retrieve least square parameters
      MidasVector least_sqr_params;
      least_sqr_params.SetNewSize(Nparams);
      LeastSqrFit.GetLeastSqrParms(least_sqr_params);

      // Vector containing the coefficients to be compared with
      MidasVector target_params = RefCoefss();

      // Assert that the correct parameters have been determined
      In i = I_0;
      for (In x1 = I_0; x1 < paramsvec[0].Size(); ++x1)
      {
         for (In x2 = I_0; x2 < paramsvec[1].Size(); ++x2)
         {
            for (In x3 = I_0; x3 < paramsvec[2].Size(); ++x3)
            {
               UNIT_ASSERT_FEQUAL_PREC(least_sqr_params[i], target_params[i], 5, "SVD fit did not find correct coefficient in 3D fit");
               ++i;
            }
         }
      }
   }

   /**
   * Vector holding the coefficients to be testet against
   **/
   MidasVector RefCoefss()
   {
      MidasVector ref_vals(I_4*I_4*I_4);

      // 3d fit func1*func2*func3
      #if defined(ARCH_ARM)
         MidasWarning("I have detected ARM architecture and used ARM specific reference data in PolyFitSvdTest: PolyFitSvd3DTestImpl. Is this correct?");
         ref_vals[0]  = 6.0000000410893781e+00;
         ref_vals[1]  = 1.2000000408758398e+01;
         ref_vals[2]  = 1.8000000012322069e+01;
         ref_vals[3]  = 2.3999999990271579e+01;
         ref_vals[4]  = 1.2000000419940053e+01;
         ref_vals[5]  = 2.3999999915885084e+01;
         ref_vals[6]  = 3.5999999970141985e+01;
         ref_vals[7]  = 4.8000000002304930e+01;
         ref_vals[8]  = 2.4000000048682310e+01;
         ref_vals[9]  = 4.7999999971951993e+01;
         ref_vals[10] = 7.1999999996188663e+01;
         ref_vals[11] = 9.6000000000973372e+01;
         ref_vals[12] = 4.7999999967894354e+01;
         ref_vals[13] = 9.6000000001475570e+01;
         ref_vals[14] = 1.4400000000229849e+02;
         ref_vals[15] = 1.9199999999990368e+02;
         ref_vals[16] = 1.2000000095837352e+01;
         ref_vals[17] = 2.4000000093253867e+01;
         ref_vals[18] = 3.5999999985041512e+01;
         ref_vals[19] = 4.7999999997389942e+01;
         ref_vals[20] = 2.3999999988082099e+01;
         ref_vals[21] = 4.8000000028006887e+01;
         ref_vals[22] = 7.2000000003498968e+01;
         ref_vals[23] = 9.5999999996957214e+01;
         ref_vals[24] = 4.7999999983562645e+01;
         ref_vals[25] = 9.6000000003485908e+01;
         ref_vals[26] = 1.4400000000127491e+02;
         ref_vals[27] = 1.9199999999970390e+02;
         ref_vals[28] = 9.5999999999680966e+01;
         ref_vals[29] = 1.9199999999820241e+02;
         ref_vals[30] = 2.8799999999985442e+02;
         ref_vals[31] = 3.8400000000011136e+02;
         ref_vals[32] = 1.8000000034347959e+01;
         ref_vals[33] = 3.5999999994503249e+01;
         ref_vals[34] = 5.3999999997165958e+01;
         ref_vals[35] = 7.1999999999783682e+01;
         ref_vals[36] = 3.5999999991914251e+01;
         ref_vals[37] = 7.2000000006585552e+01;
         ref_vals[38] = 1.0800000000054777e+02;
         ref_vals[39] = 1.4399999999980471e+02;
         ref_vals[40] = 7.1999999995824950e+01;
         ref_vals[41] = 1.4400000000164920e+02;
         ref_vals[42] = 2.1600000000030124e+02;
         ref_vals[43] = 2.8799999999994532e+02;
         ref_vals[44] = 1.4400000000102816e+02;
         ref_vals[45] = 2.8799999999971385e+02;
         ref_vals[46] = 4.3199999999992491e+02;
         ref_vals[47] = 5.7600000000000387e+02;
         ref_vals[48] = 2.3999999993223245e+01;
         ref_vals[49] = 4.8000000006354590e+01;
         ref_vals[50] = 7.2000000000813245e+01;
         ref_vals[51] = 9.5999999999667224e+01;
         ref_vals[52] = 4.8000000000357943e+01;
         ref_vals[53] = 9.5999999999318263e+01;
         ref_vals[54] = 1.4399999999987969e+02;
         ref_vals[55] = 1.9200000000005920e+02;
         ref_vals[56] = 9.6000000001209798e+01;
         ref_vals[57] = 1.9199999999945405e+02;
         ref_vals[58] = 2.8799999999992303e+02;
         ref_vals[59] = 3.8400000000002802e+02;
         ref_vals[60] = 1.9200000000001108e+02;
         ref_vals[61] = 3.8400000000005201e+02;
         ref_vals[62] = 5.7600000000000807e+02;
         ref_vals[63] = 7.6799999999999716e+02;
      #else
         ref_vals[0]  = 6.0000000682686832e+00;
         ref_vals[1]  = 1.2000000372012028e+01;
         ref_vals[2]  = 1.8000000017083913e+01;
         ref_vals[3]  = 2.3999999995419397e+01;
         ref_vals[4]  = 1.2000000147260979e+01;
         ref_vals[5]  = 2.4000000012275969e+01;
         ref_vals[6]  = 3.6000000001986749e+01;
         ref_vals[7]  = 4.7999999994941234e+01;
         ref_vals[8]  = 2.4000000024242098e+01;
         ref_vals[9]  = 4.7999999953504670e+01;
         ref_vals[10] = 7.1999999997152571e+01;
         ref_vals[11] = 9.6000000001856989e+01;
         ref_vals[12] = 4.7999999983479334e+01;
         ref_vals[13] = 9.5999999996782435e+01;
         ref_vals[14] = 1.4400000000050926e+02;
         ref_vals[15] = 1.9200000000028649e+02;
         ref_vals[16] = 1.2000000211016381e+01;
         ref_vals[17] = 2.3999999987930238e+01;
         ref_vals[18] = 3.5999999982620814e+01;
         ref_vals[19] = 4.7999999998542037e+01;
         ref_vals[20] = 2.4000000028596261e+01;
         ref_vals[21] = 4.8000000001580723e+01;
         ref_vals[22] = 7.2000000000021132e+01;
         ref_vals[23] = 9.5999999999750372e+01;
         ref_vals[24] = 4.7999999967967149e+01;
         ref_vals[25] = 9.6000000008962886e+01;
         ref_vals[26] = 1.4400000000240391e+02;
         ref_vals[27] = 1.9199999999965462e+02;
         ref_vals[28] = 9.5999999998830276e+01;
         ref_vals[29] = 1.9199999999990800e+02;
         ref_vals[30] = 2.8799999999995276e+02;
         ref_vals[31] = 3.8399999999998300e+02;
         ref_vals[32] = 1.8000000034709146e+01;
         ref_vals[33] = 3.5999999976723302e+01;
         ref_vals[34] = 5.3999999997630702e+01;
         ref_vals[35] = 7.2000000000553413e+01;
         ref_vals[36] = 3.5999999981856469e+01;
         ref_vals[37] = 7.2000000006573742e+01;
         ref_vals[38] = 1.0800000000059788e+02;
         ref_vals[39] = 1.4399999999996453e+02;
         ref_vals[40] = 7.1999999996717179e+01;
         ref_vals[41] = 1.4400000000297069e+02;
         ref_vals[42] = 2.1600000000022200e+02;
         ref_vals[43] = 2.8799999999988211e+02;
         ref_vals[44] = 1.4400000000158124e+02;
         ref_vals[45] = 2.8799999999971726e+02;
         ref_vals[46] = 4.3199999999992724e+02;
         ref_vals[47] = 5.7600000000000239e+02;
         ref_vals[48] = 2.3999999992263923e+01;
         ref_vals[49] = 4.8000000007819317e+01;
         ref_vals[50] = 7.2000000000786798e+01;
         ref_vals[51] = 9.5999999999811450e+01;
         ref_vals[52] = 4.8000000000608040e+01;
         ref_vals[53] = 9.5999999999919638e+01;
         ref_vals[54] = 1.4399999999991462e+02;
         ref_vals[55] = 1.9199999999998121e+02;
         ref_vals[56] = 9.6000000001750280e+01;
         ref_vals[57] = 1.9199999999933286e+02;
         ref_vals[58] = 2.8799999999987011e+02;
         ref_vals[59] = 3.8400000000002109e+02;
         ref_vals[60] = 1.9199999999993835e+02;
         ref_vals[61] = 3.8399999999998067e+02;
         ref_vals[62] = 5.7600000000000773e+02;
         ref_vals[63] = 7.6800000000000114e+02;
      #endif /* defined(ARCH_ARM) */
      
      return ref_vals;
   }
};

} /* namespace polyfitsvd */


void PolyFitSvdTest()
{
   // Create test_suite object.
   cutee::suite suite("PolyFitSvd test");

   using namespace polyfitsvd;
   suite.add_test<PolyFitSvd1DTestImpl> ("PolyFitSvd1D test");
   suite.add_test<PolyFitSvd2DTestImpl> ("PolyFitSvd2D test");
   suite.add_test<PolyFitSvd3DTestImpl> ("PolyFitSvd3D test");

   // Run the tests.
   RunSuite(suite);
}

} /* namespace midas::test */
