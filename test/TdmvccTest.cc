/**
 *******************************************************************************
 * 
 * @file    TdmvccTest.cc
 * @date    21-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "mpi/Impi.h"
#include "util/type_traits/Complex.h"
#include "test/TdmvccDerivationTest.h"
#include "test/Tdmvcc2Test.h"
#include "util/type_traits/TypeName.h"
#include "util/RandomNumberGenerator.h"

namespace midas::test
{
   namespace tdmvcc::detail
   {
      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `Tdmvcc.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddTdmvccDerivationTests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::tdmvcc;
         std::string s = "("+midas::type_traits::TypeName<T>()+") TdmvccDerivation; ";

         // Tests for _both_ real and complex numbers.
         {
            Uin n_modes = 4;
            // VariationalGRedundancies
            auto info = [&s](const std::string& str, Uin M, Uin k, bool rm_1m) -> std::string
            {
               std::stringstream ss;
               ss << s << str
                  << ", num.modes = " << M
                  << ", MCR max.exci = " << k
                  << ", 1-mode MCs " << (rm_1m? "excluded": "included")
                  ;
               return ss.str();
            };
            for(const bool rm_1m: {false,true})
            {
               for(Uin n_modals = 1; n_modals <= n_modes; ++n_modals)
               {
                  ModeCombiOpRange mcr(n_modals, n_modes);
                  if (rm_1m)
                  {
                     std::vector<ModeCombi> v_mc;
                     for(auto it_mc = mcr.Begin(1); it_mc != mcr.End(1); ++it_mc)
                     {
                        v_mc.emplace_back(*it_mc);
                     }
                     mcr.Erase(std::move(v_mc));
                  }
                  arSuite.add_test<VariationalGRedundancies<T>>(info("VariationalGRedundancies", n_modes, n_modals, rm_1m),n_modes,mcr);
               }
            }
            // VariationalGCoefMatrixSingularities
            auto info2 = [&s](const std::string& str, Uin M, Uin k, const std::vector<Uin>& Nm) -> std::string
            {
               std::stringstream ss;
               ss << s << str
                  << ", num.modes = " << M
                  << ", MCR max.exci = " << k
                  << ", Nm = " << Nm
                  ;
               return ss.str();
            };
            for(const Uin n_modes: {2, 3})
            {
               for(const Uin n_modals: {3, 4})
               {
                  std::vector<Uin> v_n_modals(n_modes, n_modals);
                  for(const bool zero_t1: {false,true})
                  {
                     for(const bool zero_l1: {false,true})
                     {
                        ModeCombiOpRange mcr(n_modals, n_modes);
                        arSuite.add_test<VariationalGCoefMatrixSingularities<T>>(info2("VariationalGCoefMatrixSingularities", n_modes, n_modals, v_n_modals), v_n_modals, mcr, zero_t1, zero_l1);
                        arSuite.add_test<VariationalConditionCheckT1L1Excl<T>>(info2("VariationalConditionCheckT1L1Excl  ", n_modes, n_modals, v_n_modals), v_n_modals, mcr);
                     }
                  }
               }
            }
         }

         // Tests for complex/real numbers _only_.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
         }
         else
         {
         }
      }

      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests from `Tdmvcc2.h` to this test suite, using the
       *    template parameters that this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddTdmvcc2Tests
         (  cutee::suite& arSuite
         )
      {
         using namespace midas::test::tdmvcc;
         std::string s = "("+midas::type_traits::TypeName<T>()+") Tdmvcc2; ";

         constexpr Uin max_exci = 2;

         // Tests for _both_ real and complex numbers.
         {
            // ErrVec test
            [[maybe_unused]] auto info = [&s]
              (   const std::string& str
              ,   const std::vector<Uin>& n
              ,   const std::vector<Uin>& N
              ,   const std::vector<Uin>& O
              )   -> std::string
            {
               if (  N.size() != O.size()
                  || N.size() != n.size()
                  )
               {
                  MIDASERROR("n, N, and O vectors must have same size!");
               }
               
               std::stringstream ss;
               ss << s << str
                  << ", num.modes = " << N.size()
                  << ", num.tdmodals = " << n
                  << ", num.primmodals = " << N
                  << ", num.opers = " << O
                  ;
               return ss.str();
            };
            for(const auto& n_modes : {3, 4})
            {
               std::vector<Uin> n_td_modals(n_modes);
               std::iota(n_td_modals.begin(), n_td_modals.end(), 2); // Fill with {2,3,4,..}
               std::shuffle(n_td_modals.begin(),n_td_modals.end(), midas::util::detail::get_mersenne()); // Shuffle elements
               std::vector<Uin> n_prim_modals(n_td_modals);
               std::for_each(n_prim_modals.begin(), n_prim_modals.end(), [](auto& n){ n+=10; }); // N = n+10 for all modes
               std::vector<Uin> n_opers(n_modes);
               std::iota(n_opers.begin(), n_opers.end(), 2); // Fill with {2,3,4,..}
               std::shuffle(n_opers.begin(),n_opers.end(), midas::util::detail::get_mersenne()); // Shuffle elements
               std::vector<Uin> n_g_opers(n_modes, 1);

               // MPI sync vectors which may have been shuffled differently
               #ifdef VAR_MPI
                  void* ptr_tdmodals = const_cast<void*>(static_cast<const void*>(n_td_modals.data()));
                  midas::mpi::detail::WRAP_Bcast(ptr_tdmodals, n_modes, midas::mpi::DataTypeTrait<Uin>::Get(), midas::mpi::MasterRank(), MPI_COMM_WORLD);
                  void* ptr_primmodals = const_cast<void*>(static_cast<const void*>(n_prim_modals.data()));
                  midas::mpi::detail::WRAP_Bcast(ptr_primmodals, n_modes, midas::mpi::DataTypeTrait<Uin>::Get(), midas::mpi::MasterRank(), MPI_COMM_WORLD);
                  void* ptr_opers = const_cast<void*>(static_cast<const void*>(n_opers.data()));
                  midas::mpi::detail::WRAP_Bcast(ptr_opers, n_modes, midas::mpi::DataTypeTrait<Uin>::Get(), midas::mpi::MasterRank(), MPI_COMM_WORLD);
               #endif

               ModeCombiOpRange mcr(max_exci, n_modes);

               // Util tests
               arSuite.add_test<TdmvccTestUtilitiesTest<T>>(info("TdmvccTestUtilities", n_td_modals, n_prim_modals, n_opers), n_td_modals, n_prim_modals);

               // H transformer tests
               arSuite.add_test<Tdmvcc2HTransformerTest<T>>(info("Tdmvcc2HTransformer", n_td_modals, n_prim_modals, n_opers), mcr, n_td_modals, n_prim_modals, n_opers);

               // MeanFields for FullActiveBasis method
               arSuite.add_test<Tdmvcc2HMeanFieldFullActiveBasis<T>>(info("Tdmvcc2HMeanFieldsFullActiveBasis", n_prim_modals, n_prim_modals, n_opers), mcr, n_prim_modals, n_prim_modals, n_opers);

               // g transformer tests
               arSuite.add_test<Tdmvcc2GTransformerTest<T>>(info("Tdmvcc2GTransformer ", n_td_modals, n_prim_modals, n_g_opers), mcr, n_td_modals, n_prim_modals, n_g_opers);

               // Full-derivative test (including g construction)
               arSuite.add_test<Tdmvcc2DerivativeTest<T>>(info("Tdmvcc2Derivative  ", n_td_modals, n_prim_modals, n_opers), mcr, n_td_modals, n_prim_modals, n_opers);

               // Autocorrelation function test
               arSuite.add_test<Tdmvcc2AutoCorrTest<T>>(info("Tdmvcc2AutoCorr    ", n_td_modals, n_prim_modals, n_opers), mcr, n_td_modals, n_prim_modals, n_opers);
            }     

         }

      }
   } /* namespace detail */

   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void TdmvccTest()
   {
      // Create test_suite object.
      cutee::suite suite("Tdmvcc test");

      // Add all the tests to the suite.
      using tdmvcc::detail::AddTdmvccDerivationTests;
      AddTdmvccDerivationTests<Nb>(suite);
      AddTdmvccDerivationTests<std::complex<Nb>>(suite);
      using tdmvcc::detail::AddTdmvcc2Tests;
      AddTdmvcc2Tests<Nb>(suite);
      AddTdmvcc2Tests<std::complex<Nb>>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
