/**
************************************************************************
* 
* @file                FromStringTest.h
*
* Created:             30-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Test case for turning strings into numbers and vectors
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FROMSTRINGTEST_H_INCLUDED
#define FROMSTRINGTEST_H_INCLUDED

#include <string>
#include <vector>

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"

namespace midas::test
{

/**
 *
 **/
struct StringToNumberTest 
   : public cutee::test
{
   void run() 
   {
      In test1_real_val = 214;
      In test1_conv_val = midas::util::FromString<In>("214");
      Nb test2_real_val = 3.12e-1;
      Nb test2_conv_val = midas::util::FromString<Nb>("3.12e-1");
      Nb test3_real_val = 312;
      Nb test3_conv_val = midas::util::FromString<Nb>("312");
      UNIT_ASSERT(test1_real_val == test1_conv_val, "Test failed to convert 214 to In. Conversion : "+std::to_string(test1_conv_val)+
                                                    " should be : "+std::to_string(test1_real_val));
      UNIT_ASSERT(test2_real_val == test2_conv_val, "Test failed to convert 3.12e-1 to Nb. Conversion : "+std::to_string(test2_conv_val)+
                                                                " should be : "+std::to_string(test2_real_val));
      UNIT_ASSERT(test3_real_val == test3_conv_val, "Test failed to convert 3.12e-1 to Nb. Conversion : "+std::to_string(test3_conv_val)+
                                                                            " should be : "+std::to_string(test3_real_val));
   }
};

/**
 *
 **/
struct StringToVectorTest 
   : public cutee::test
{
   void run() 
   {
      vector<In> test1_real_val;
      for(In i = 1; i < 5; ++i)
         test1_real_val.push_back(i);
      vector<In> test1_conv_val = midas::util::VectorFromString<In>("1 2 3 4");
      vector<In> test1_comma_conv_val = midas::util::VectorFromString<In>("1,2,3,4",',');
      UNIT_ASSERT(test1_conv_val.size() == test1_real_val.size(), 
                                             "Test failed to convert \"1 2 3 4\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test1_conv_val.size())+
                                             " should be : "+std::to_string(test1_real_val.size()) );
      UNIT_ASSERT(test1_comma_conv_val.size() == test1_real_val.size(), 
                                             "Test failed to convert \"1,2,3,4\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test1_comma_conv_val.size())+
                                             " should be : "+std::to_string(test1_real_val.size()) );
      if(test1_comma_conv_val.size() == test1_real_val.size() && test1_conv_val.size() == test1_real_val.size())
         for(In i = 0; i < test1_real_val.size(); ++i)
         {
            UNIT_ASSERT(test1_conv_val[i] == test1_real_val[i], "Test failed to convert \"1 2 3 4\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test1_conv_val[i])+
                                                                "Should be : "+std::to_string(test1_real_val[i]));
            UNIT_ASSERT(test1_comma_conv_val[i] == test1_real_val[i], "Test failed to convert \"1,2,3,4\" to vector<In>, error in element "
                                                                      +std::to_string(i)+
                                                                      "Resulting vector element is : "+std::to_string(test1_comma_conv_val[i])+
                                                                      "Should be : "+std::to_string(test1_real_val[i]));
         }
      vector<Nb> test2_real_val;
      test2_real_val.push_back(1.23e-3);
      test2_real_val.push_back(5.0312e+10);
      test2_real_val.push_back(5);
      vector<Nb> test2_conv_val = midas::util::VectorFromString<Nb>("1.23e-3 5.0312e+10 5");
      UNIT_ASSERT(test2_conv_val.size() == test2_real_val.size(), 
                                             "Test failed to convert \"1.23e-3 5.0312e+10 5\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test1_conv_val.size())+
                                             " should be : "+std::to_string(test1_real_val.size()) );
      if(test2_conv_val.size() == test2_real_val.size())
         for(In i = 0; i < test2_real_val.size(); ++i)
         {
            UNIT_ASSERT(test2_conv_val[i] == test2_real_val[i], "Test failed to convert \"1.23e-3 5.0312e+10 5\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test2_conv_val[i])+
                                                                "Should be : "+std::to_string(test2_real_val[i]));
         }

   }
};

/**
 *
 **/
struct StringToVectorTestAdv 
   : public cutee::test
{
   void run() 
   {
      vector<In> test1_real_val;
      for(In i = 1; i < 5; ++i)
         test1_real_val.push_back(i);
      vector<In> test1_conv_val = midas::util::VectorFromString<In>("[1..4]");
      UNIT_ASSERT(test1_conv_val.size() == test1_real_val.size(), 
                                             "Test failed to convert \"[1..4]\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test1_conv_val.size())+
                                             " should be : "+std::to_string(test1_real_val.size()) );
      if(test1_conv_val.size() == test1_real_val.size())
         for(In i = 0; i < test1_real_val.size(); ++i)
         {
            UNIT_ASSERT(test1_conv_val[i] == test1_real_val[i], "Test failed to convert \"[1..4]\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                " Resulting vector element is : "+std::to_string(test1_conv_val[i])+
                                                                "Should be : "+std::to_string(test1_real_val[i]));
         }
      

      vector<In> test2_real_val;
      for(In i = 0; i < 4; ++i)
         test2_real_val.push_back(3);
      vector<In> test2_conv_val = midas::util::VectorFromString<In>("[4*3]");
      UNIT_ASSERT(test2_conv_val.size() == test2_real_val.size(), 
                                             "Test failed to convert \"[4*3]\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test2_conv_val.size())+
                                             " should be : "+std::to_string(test2_real_val.size()) );
      if(test2_conv_val.size() == test2_real_val.size())
         for(In i = 0; i < test2_real_val.size(); ++i)
         {
            UNIT_ASSERT(test2_conv_val[i] == test2_real_val[i], "Test failed to convert \"[4*3]\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test2_conv_val[i])+
                                                                "Should be : "+std::to_string(test2_real_val[i]));
         }

      vector<In> test3_real_val;
      for(In i = 1; i < 5; ++i)
         test3_real_val.push_back(i);

      test3_real_val.push_back(6);
      test3_real_val.push_back(9);
      for(In i = 0; i < 4; ++i)
         test3_real_val.push_back(10);
      vector<In> test3_conv_val = midas::util::VectorFromString<In>("[1..4] 6 9 [4*10]");
      UNIT_ASSERT(test3_conv_val.size() == test3_real_val.size(), 
                                             "Test failed to convert \"[1..4] 6 9 4*10\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test3_conv_val.size())+
                                             " should be : "+std::to_string(test3_real_val.size()) );
      if(test3_conv_val.size() == test3_real_val.size())
         for(In i = 0; i < test3_real_val.size(); ++i)
         {
            UNIT_ASSERT(test3_conv_val[i] == test3_real_val[i], "Test failed to convert \"[1..4] 6 9 [4*10]\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test3_conv_val[i])+
                                                                "Should be : "+std::to_string(test3_real_val[i]));
         }

      vector<In> test4_real_val;
      for(In i = 1; i < 5; i = i + 2)
         test4_real_val.push_back(i);
      vector<In> test4_conv_val = midas::util::VectorFromString<In>("[1..4;2]");
      UNIT_ASSERT(test4_conv_val.size() == test4_real_val.size(), 
                                             "Test failed to convert \"[1..4;2]\" to vector<In>. Size of resulting vector is : "
                                             +std::to_string(test4_conv_val.size())+
                                             " should be : "+std::to_string(test4_real_val.size()) );
      if(test4_conv_val.size() == test4_real_val.size())
         for(In i = 0; i < test4_real_val.size(); ++i)
         {
            UNIT_ASSERT(test4_conv_val[i] == test4_real_val[i], "Test failed to convert \"[1..4;2]\" to vector<In>, error in element "
                                                                +std::to_string(i)+
                                                                "Resulting vector element is : "+std::to_string(test4_conv_val[i])+
                                                                "Should be : "+std::to_string(test4_real_val[i]));
         }
   }
};

/**
 * Test that we can create a std::tuple from string with a specific separator.
 *
 *    E.g. the string "2 3.2 hey", should be converted to a std::tuple<int, double, std::string>,
 *    containing (2, 3.2, "hey").
 **/
struct TupleFromStringTest 
   : public cutee::test
{
   void run()
   {
      // Create string.
      std::string str = "2 3.2 hey";

      // Convert string to tuple.
      std::tuple<int, double, std::string> t = midas::util::TupleFromString<int, double, std::string>(str);

      // Assert output
      UNIT_ASSERT_EQUAL (std::get<0>(t), 2    , "Did not read int correctly.");
      UNIT_ASSERT_FEQUAL(std::get<1>(t), 3.2  , "Did not read double correctly.");
      UNIT_ASSERT_EQUAL (std::get<2>(t), std::string("hey"), "Did not read string correctly.");
   }
};
} /* namespace midas::test */

#endif /* FROMSTRINGTEST_H_INCLUDED */
