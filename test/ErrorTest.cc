/**
 *******************************************************************************
 * 
 * @file    TypeTraitsTest.cc
 * @date    13-07-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for midas::type_traits.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/ErrorTest.h"

#include "test/Test.h"
#include "util/Error.h"

namespace midas::test
{

void ErrorTest()
{
   cutee::suite suite("Error test");
   
   /**************************************************************************************
    * 
    **************************************************************************************/
   using namespace midas::test::error;
   suite.add_test<ErrorNumberInitTest> ("Test that error number initially is set correctly.");
   suite.add_test<ErrorNumberResetTest>("Test that error number is correct after reset.");
   suite.add_test<ErrorNumberTest>     ("Test that we can use error number instead of kill for MIDASERROR(...).");

   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */

