/**
 *******************************************************************************
 * 
 * @file    Random_Decl.h
 * @date    14-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Random data for testing.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TEST_RANDOM_DECL_H_INCLUDED
#define MIDAS_TEST_RANDOM_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"

// Forward declarations.
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;
template<typename> class GeneralDataCont;

namespace midas::test::random
{
   //! Absolute value used for numerical thresholds in this namespace.
   template<typename T>
   using absval_t = midas::type_traits::RealTypeT<T>;

   //! Random bool, (unsigned) int, or real/complex float in [-1;1].
   template<typename T>
   T RandomNumber(bool aMpiSync = true);

   //! Random bool. (Convenience wrapper for RandomNumber<bool>().)
   inline bool RandomBool(bool aMpiSync = true) {return RandomNumber<bool>(aMpiSync);}

   /************************************************************************//**
    * @name Random containers
    * 
    * Containers with random::Numbers() (real/complex floating point numbers).
    * For the real case, (Anti)Hermitian is equivalent to (Anti)Symmetrix.
    ***************************************************************************/
   //!@{
   template<typename T>
   std::vector<T> RandomStdVector(const std::size_t, bool aMpiSync = true);

   template<typename T>
   GeneralMidasVector<T> RandomMidasVector(const std::size_t, bool aMpiSync = true);

   template<typename T>
   GeneralMidasMatrix<T> RandomMidasMatrix(const std::size_t, const std::size_t, bool aMpiSync = true);

   template<typename T>
   GeneralMidasMatrix<T> RandomHermitianMidasMatrix(const std::size_t, bool aMpiSync = true);

   template<typename T>
   GeneralMidasMatrix<T> RandomAntiHermitianMidasMatrix(const std::size_t, bool aMpiSync = true);

   template<typename T>
   GeneralDataCont<T> RandomDataCont(const std::size_t, bool aMpiSync = true);

   template<typename T>
   void LoadRandom(T*, const std::size_t, bool aMpiSync = true);

   //@{
   //! Random vector container; specialize for e.g. GeneralMidasVector<T>, etc.
   template<typename T, template<typename...> class VEC>
   struct RandomVectorContainer
   {
      VEC<T> operator()(const std::size_t, bool) const = delete;
   };

   template<typename T>
   struct RandomVectorContainer<T,std::vector>
   {
      std::vector<T> operator()(const std::size_t, bool aMpiSync = true) const;
   };

   template<typename T>
   struct RandomVectorContainer<T,GeneralMidasVector>
   {
      GeneralMidasVector<T> operator()(const std::size_t, bool aMpiSync = true) const;
   };

   template<typename T>
   struct RandomVectorContainer<T,GeneralDataCont>
   {
      GeneralDataCont<T> operator()(const std::size_t, bool aMpiSync = true) const;
   };

   //@}

   //!@}
} /* namespace midas::test::random */

#endif/*MIDAS_TEST_RANDOM_DECL_H_INCLUDED*/
