/**
************************************************************************
* 
* @file                McTdHVectorContainerTest.cc
*
* Created:             20-12-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Unit tests for McTdHVectorContainer
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "test/CuteeInterface.h"
#include "test/McTdHVectorContainerTest.h"


extern MidasStream Mout;

namespace midas::test
{

/**
 *
 **/
void McTdHVectorContainerTest
   (
   )
{
   cutee::suite suite("McTdHVectorContainer test");

   /**************************************************************************************
    * McTdHVectorContainer tests
    **************************************************************************************/
   // Constructor and norm test
   suite.add_test<CasTdHVectorContainerTest<float>>("Construct CasTdHVectorContainer<float>.");
   suite.add_test<CasTdHVectorContainerTest<double>>("Construct CasTdHVectorContainer<double>.");
   suite.add_test<CasTdHVectorContainerTest<std::complex<float>>>("Construct CasTdHVectorContainer<complex<float>>.");
   suite.add_test<CasTdHVectorContainerTest<std::complex<double>>>("Construct CasTdHVectorContainer<complex<double>>.");

   // Axpy tests
   suite.add_test<CasTdHVectorContainerAxpyTest<float>>("CasTdHVectorContainer<float> Axpy test.");
   suite.add_test<CasTdHVectorContainerAxpyTest<double>>("CasTdHVectorContainer<double> Axpy test.");
   suite.add_test<CasTdHVectorContainerAxpyTest<std::complex<float>>>("CasTdHVectorContainer<complex<float>> Axpy test.");
   suite.add_test<CasTdHVectorContainerAxpyTest<std::complex<double>>>("CasTdHVectorContainer<complex<double>> Axpy test.");

   // Zero tests
   suite.add_test<CasTdHVectorContainerZeroTest<float>>("Zero CasTdHVectorContainer<float>.");
   suite.add_test<CasTdHVectorContainerZeroTest<double>>("Zero CasTdHVectorContainer<double>.");
   suite.add_test<CasTdHVectorContainerZeroTest<std::complex<float>>>("Zero CasTdHVectorContainer<complex<float>>.");
   suite.add_test<CasTdHVectorContainerZeroTest<std::complex<double>>>("Zero CasTdHVectorContainer<complex<double>>.");
   
   //Run tests.
   RunSuite(suite);
}

} /* namespace midas::test */
