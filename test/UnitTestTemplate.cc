/**
 *******************************************************************************
 * 
 * @file    UnitTestTemplate.cc //CHANGE!
 * @date    19-09-2018 //CHANGE!
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk) //CHANGE!
 *
 * @brief
 *    A template file for unit tests. Copy and modify this for your own test. //CHANGE!
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/UnitTestTemplate.h" //CHANGE!

namespace midas::test
{
   namespace your_test_namespace::detail //CHANGE! your_test_namespace
   {
   } /* namespace your_test_namespace::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void UnitTestTemplateTest() //CHANGE! and copy this to TestDrivers.cc declarations.
   {
      // Create test_suite object.
      cutee::suite suite("UnitTestTemplate test"); //CHANGE!

      // Add all the tests to the suite.
      using namespace your_test_namespace; //CHANGE! your_test_namespace and tests below
      suite.add_test<TestOne>                          ("Brief description, for output file.");
      suite.add_test<TestWithTemplateParameter<float>> ("Parameter test, float");
      suite.add_test<TestWithTemplateParameter<double>>("Parameter test, double");
      suite.add_test<TestWithSetupAndTeardown>         ("Another test");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
