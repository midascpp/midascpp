/**
 *******************************************************************************
 * 
 * @file    ContainerMathWrappersTest.cc
 * @date    12-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <array>
#include <vector>
#include <list>

#include "ContainerMathWrappersTest.h"
#include "mmv/ContainerMathWrappers.h"
#include "mmv/MidasVector.h"

namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void ContainerMathWrappersTest()
   {
      // Create test_suite object.
      cutee::suite suite("ContainerMathWrappers test");

      // Norm tests
      suite.add_test<WrapNormArray<Nb,1>>("WrapNormArray<Nb,1>");
      suite.add_test<WrapNormArray<std::complex<Nb>,1>>("WrapNormArray<std::complex<Nb>,1>");
      suite.add_test<WrapNormArray<Nb,2>>("WrapNormArray<Nb,2>");
      suite.add_test<WrapNormArray<std::complex<Nb>,2>>("WrapNormArray<std::complex<Nb>,2>");
      suite.add_test<WrapNormContainer<Nb,std::vector,1>>("WrapNormContainer<Nb,std::vector,1>");
      suite.add_test<WrapNormContainer<std::complex<Nb>,std::vector,1>>("WrapNormContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapNormContainer<Nb,std::vector,4>>("WrapNormContainer<Nb,std::vector,4>");
      suite.add_test<WrapNormContainer<std::complex<Nb>,std::vector,4>>("WrapNormContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapNormContainer<Nb,std::list,1>>("WrapNormContainer<Nb,std::list,1>");
      suite.add_test<WrapNormContainer<std::complex<Nb>,std::list,1>>("WrapNormContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapNormContainer<Nb,std::list,4>>("WrapNormContainer<Nb,std::list,4>");
      suite.add_test<WrapNormContainer<std::complex<Nb>,std::list,4>>("WrapNormContainer<std::complex<Nb>,std::list,4>");

      // Zero tests
      suite.add_test<WrapZeroArray<Nb,1>>("WrapZeroArray<Nb,1>");
      suite.add_test<WrapZeroArray<std::complex<Nb>,1>>("WrapZeroArray<std::complex<Nb>,1>");
      suite.add_test<WrapZeroArray<Nb,2>>("WrapZeroArray<Nb,2>");
      suite.add_test<WrapZeroArray<std::complex<Nb>,2>>("WrapZeroArray<std::complex<Nb>,2>");
      suite.add_test<WrapZeroContainer<Nb,std::vector,1>>("WrapZeroContainer<Nb,std::vector,1>");
      suite.add_test<WrapZeroContainer<std::complex<Nb>,std::vector,1>>("WrapZeroContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapZeroContainer<Nb,std::vector,4>>("WrapZeroContainer<Nb,std::vector,4>");
      suite.add_test<WrapZeroContainer<std::complex<Nb>,std::vector,4>>("WrapZeroContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapZeroContainer<Nb,std::list,1>>("WrapZeroContainer<Nb,std::list,1>");
      suite.add_test<WrapZeroContainer<std::complex<Nb>,std::list,1>>("WrapZeroContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapZeroContainer<Nb,std::list,4>>("WrapZeroContainer<Nb,std::list,4>");
      suite.add_test<WrapZeroContainer<std::complex<Nb>,std::list,4>>("WrapZeroContainer<std::complex<Nb>,std::list,4>");

      // Scale tests
      suite.add_test<WrapScaleArray<Nb,1>>("WrapScaleArray<Nb,1>");
      suite.add_test<WrapScaleArray<std::complex<Nb>,1>>("WrapScaleArray<std::complex<Nb>,1>");
      suite.add_test<WrapScaleArray<Nb,2>>("WrapScaleArray<Nb,2>");
      suite.add_test<WrapScaleArray<std::complex<Nb>,2>>("WrapScaleArray<std::complex<Nb>,2>");
      suite.add_test<WrapScaleContainer<Nb,std::vector,1>>("WrapScaleContainer<Nb,std::vector,1>");
      suite.add_test<WrapScaleContainer<std::complex<Nb>,std::vector,1>>("WrapScaleContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapScaleContainer<Nb,std::vector,4>>("WrapScaleContainer<Nb,std::vector,4>");
      suite.add_test<WrapScaleContainer<std::complex<Nb>,std::vector,4>>("WrapScaleContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapScaleContainer<Nb,std::list,1>>("WrapScaleContainer<Nb,std::list,1>");
      suite.add_test<WrapScaleContainer<std::complex<Nb>,std::list,1>>("WrapScaleContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapScaleContainer<Nb,std::list,4>>("WrapScaleContainer<Nb,std::list,4>");
      suite.add_test<WrapScaleContainer<std::complex<Nb>,std::list,4>>("WrapScaleContainer<std::complex<Nb>,std::list,4>");

      // DotProduct tests
      suite.add_test<WrapDotProductArray<Nb,1>>("WrapDotProductArray<Nb,1>");
      suite.add_test<WrapDotProductArray<std::complex<Nb>,1>>("WrapDotProductArray<std::complex<Nb>,1>");
      suite.add_test<WrapDotProductArray<Nb,2>>("WrapDotProductArray<Nb,2>");
      suite.add_test<WrapDotProductArray<std::complex<Nb>,2>>("WrapDotProductArray<std::complex<Nb>,2>");
      suite.add_test<WrapDotProductContainer<Nb,std::vector,1>>("WrapDotProductContainer<Nb,std::vector,1>");
      suite.add_test<WrapDotProductContainer<std::complex<Nb>,std::vector,1>>("WrapDotProductContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapDotProductContainer<Nb,std::vector,4>>("WrapDotProductContainer<Nb,std::vector,4>");
      suite.add_test<WrapDotProductContainer<std::complex<Nb>,std::vector,4>>("WrapDotProductContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapDotProductContainer<Nb,std::list,1>>("WrapDotProductContainer<Nb,std::list,1>");
      suite.add_test<WrapDotProductContainer<std::complex<Nb>,std::list,1>>("WrapDotProductContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapDotProductContainer<Nb,std::list,4>>("WrapDotProductContainer<Nb,std::list,4>");
      suite.add_test<WrapDotProductContainer<std::complex<Nb>,std::list,4>>("WrapDotProductContainer<std::complex<Nb>,std::list,4>");

      // Axpy tests
      suite.add_test<WrapAxpyArray<Nb,1>>("WrapAxpyArray<Nb,1>");
      suite.add_test<WrapAxpyArray<std::complex<Nb>,1>>("WrapAxpyArray<std::complex<Nb>,1>");
      suite.add_test<WrapAxpyArray<Nb,2>>("WrapAxpyArray<Nb,2>");
      suite.add_test<WrapAxpyArray<std::complex<Nb>,2>>("WrapAxpyArray<std::complex<Nb>,2>");
      suite.add_test<WrapAxpyContainer<Nb,std::vector,1>>("WrapAxpyContainer<Nb,std::vector,1>");
      suite.add_test<WrapAxpyContainer<std::complex<Nb>,std::vector,1>>("WrapAxpyContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapAxpyContainer<Nb,std::vector,4>>("WrapAxpyContainer<Nb,std::vector,4>");
      suite.add_test<WrapAxpyContainer<std::complex<Nb>,std::vector,4>>("WrapAxpyContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapAxpyContainer<Nb,std::list,1>>("WrapAxpyContainer<Nb,std::list,1>");
      suite.add_test<WrapAxpyContainer<std::complex<Nb>,std::list,1>>("WrapAxpyContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapAxpyContainer<Nb,std::list,4>>("WrapAxpyContainer<Nb,std::list,4>");
      suite.add_test<WrapAxpyContainer<std::complex<Nb>,std::list,4>>("WrapAxpyContainer<std::complex<Nb>,std::list,4>");

      // HadamardProduct tests
      suite.add_test<WrapHadamardProductArray<Nb,1>>("WrapHadamardProductArray<Nb,1>");
      suite.add_test<WrapHadamardProductArray<std::complex<Nb>,1>>("WrapHadamardProductArray<std::complex<Nb>,1>");
      suite.add_test<WrapHadamardProductArray<Nb,2>>("WrapHadamardProductArray<Nb,2>");
      suite.add_test<WrapHadamardProductArray<std::complex<Nb>,2>>("WrapHadamardProductArray<std::complex<Nb>,2>");
      suite.add_test<WrapHadamardProductContainer<Nb,std::vector,1>>("WrapHadamardProductContainer<Nb,std::vector,1>");
      suite.add_test<WrapHadamardProductContainer<std::complex<Nb>,std::vector,1>>("WrapHadamardProductContainer<std::complex<Nb>,std::vector,1>");
      suite.add_test<WrapHadamardProductContainer<Nb,std::vector,4>>("WrapHadamardProductContainer<Nb,std::vector,4>");
      suite.add_test<WrapHadamardProductContainer<std::complex<Nb>,std::vector,4>>("WrapHadamardProductContainer<std::complex<Nb>,std::vector,4>");
      suite.add_test<WrapHadamardProductContainer<Nb,std::list,1>>("WrapHadamardProductContainer<Nb,std::list,1>");
      suite.add_test<WrapHadamardProductContainer<std::complex<Nb>,std::list,1>>("WrapHadamardProductContainer<std::complex<Nb>,std::list,1>");
      suite.add_test<WrapHadamardProductContainer<Nb,std::list,4>>("WrapHadamardProductContainer<Nb,std::list,4>");
      suite.add_test<WrapHadamardProductContainer<std::complex<Nb>,std::list,4>>("WrapHadamardProductContainer<std::complex<Nb>,std::list,4>");

      // Run the tests.
      RunSuite(suite);
   }

} /* namespace midas::test */


