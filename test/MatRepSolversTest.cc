/**
 *******************************************************************************
 * 
 * @file    MatRepSolversTest.cc
 * @date    07-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/MatRepSolversTest.h"
#include "util/type_traits/TypeName.h"

namespace midas::test
{
   namespace matrepsolver::detail
   {
      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests using the template parameters that this function is
       *    called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddTests
         (  cutee::suite& arSuite
         )
      {
         //std::string s = "("+midas::type_traits::TypeName<T>()+") ";
         auto info = [](const std::string& s, Uin aMaxExci, bool aCrop, bool aDiagPrecon) -> std::string
         {
            std::stringstream ss;
            ss << std::boolalpha
               << s << "("+midas::type_traits::TypeName<T>()+")"
               << "; max.exci. = " << aMaxExci
               << ", algorithm = " << (aCrop? "CROP": "DIIS")
               << ", diag.precon. = " << aDiagPrecon
               ;
            return ss.str();
         };

         for(const auto& max_exci: {3,2})
         {
            for(const auto& crop: {false, true})
            {
               // MBH, Jan 2020: converges horribly slowly without precon!
               for(const auto& precon: {true})
               {
                  arSuite.add_test<SolverExtVccDiagPreconWater<T>>(info("SolverExtVccDiagPreconWater",max_exci,crop,precon), max_exci, crop, precon);
                  arSuite.add_test<SolverTradVccDiagPreconWater<T>>(info("SolverTradVccDiagPreconWater",max_exci,crop,precon), max_exci, crop, precon);
               }
            }
         }
      }

   } /* namespace matrepsolver::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void MatRepSolversTest()
   {
      // Create test_suite object.
      cutee::suite suite("MatRepSolvers test");

      // Add all the tests to the suite.
      using matrepsolver::detail::AddTests;
      AddTests<Nb>(suite);
      AddTests<std::complex<Nb>>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
