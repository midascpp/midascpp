/**
 *******************************************************************************
 *
 * @file    MatRepVibOperTest.h
 * @date    19-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 *
 *******************************************************************************
 **/

#ifndef MATREPVIBOPERTEST_H_INCLUDED
#define MATREPVIBOPERTEST_H_INCLUDED

#include <numeric>
#include <functional>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Arch.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/OperMat.h"
#include "util/Math.h"
#include "util/type_traits/Complex.h"
#include "test/Random.h"
#include "test/util/TransTestUtils.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "util/CallStatisticsHandler.h"
#include "util/InterfaceOpenMP.h"
#include "test/util/MpiSyncedAssertions.h"
#include "test/CuteeInterface.h"

namespace midas::test::matrep
{
   using midas::util::matrep::MatRepVibOper;
   using midas::util::matrep::SparseClusterOper;
   template<typename T> using mat_t = typename MatRepVibOper<T>::mat_t;
   template<typename T> using vec_t = typename MatRepVibOper<T>::vec_t;
   template<typename T> using absval_t = typename mat_t<T>::real_t;

   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }
} /* namespace midas::test::matrep */

namespace midas::test::matrep::utils
{
   /************************************************************************//**
    * @brief
    *    Check utility function SetFromVec.
    ***************************************************************************/
   struct SetFromVecTest
      :  public cutee::test
   {
      void run() override
      {
         std::set<Uin> s = midas::util::matrep::SetFromVec(std::vector<In>{2,1,3});
         UNIT_ASSERT_EQUAL(Uin(s.size()), Uin(3), "Wrong size.");
         auto it = s.begin();
         UNIT_ASSERT_EQUAL(*it++, Uin(1), "Bad 0'th value.");
         UNIT_ASSERT_EQUAL(*it++, Uin(2), "Bad 1'st value.");
         UNIT_ASSERT_EQUAL(*it++, Uin(3), "Bad 2'nd value.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function CumulativeProduct.
    ***************************************************************************/
   struct CumulativeProductTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::CumulativeProduct;
         const std::vector<Uin> d    = { 2, 4, 3, 2};
         const std::vector<Uin> ctrl = {24, 6, 2, 1};
         const std::vector<Uin> res  = CumulativeProduct(d);
         UNIT_ASSERT_EQUAL(res, ctrl, "Wrong CumulativeProduct.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function MultiIndex.
    ***************************************************************************/
   struct MultiIndexTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::MultiIndex;
         using ret_t = std::vector<Uin>;
         const std::vector<Uin> dims = {2,4,3};
         UNIT_ASSERT_EQUAL(MultiIndex( 0, dims), ret_t({0,0,0}), "MultiIndex(0) failed.");
         UNIT_ASSERT_EQUAL(MultiIndex( 1, dims), ret_t({0,0,1}), "MultiIndex(1) failed.");
         UNIT_ASSERT_EQUAL(MultiIndex( 5, dims), ret_t({0,1,2}), "MultiIndex(5) failed.");
         UNIT_ASSERT_EQUAL(MultiIndex(14, dims), ret_t({1,0,2}), "MultiIndex(14) failed.");
         UNIT_ASSERT_EQUAL(MultiIndex(23, dims), ret_t({1,3,2}), "MultiIndex(23) failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function SplitMultiIndex.
    ***************************************************************************/
   struct SplitMultiIndexTest
      :  public cutee::test
   {
      public:
         using modes_t = std::set<Uin>;
         using multiindex_t = std::vector<Uin>;

         SplitMultiIndexTest
            (  multiindex_t aMI
            ,  modes_t aFullMc
            ,  std::vector<modes_t> aSubMcs
            ,  std::vector<multiindex_t> aCtrl
            )
            :  mMI(std::move(aMI))
            ,  mFullMc(std::move(aFullMc))
            ,  mSubMcs(std::move(aSubMcs))
            ,  mCtrl(std::move(aCtrl))
         {
         }


         void run() override
         {
            using midas::util::matrep::SplitMultiIndex;
            UNIT_ASSERT_EQUAL(SplitMultiIndex(mMI, mFullMc, mSubMcs), mCtrl, "Wrong result.");
         }

      private:
         multiindex_t mMI;
         modes_t mFullMc;
         std::vector<modes_t> mSubMcs;
         std::vector<multiindex_t> mCtrl;

   };

   /************************************************************************//**
    * @brief
    *    Check utility function AbsIndex.
    ***************************************************************************/
   struct AbsIndexTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::AbsIndex;
         using inp_t = std::vector<Uin>;
         using ret_t = Uin;
         const std::vector<Uin> dims = {2,4,3};
         UNIT_ASSERT_EQUAL(AbsIndex(inp_t({0,0,0}), dims), ret_t( 0), "AbsIndex({0,0,0}) failed.");
         UNIT_ASSERT_EQUAL(AbsIndex(inp_t({0,0,1}), dims), ret_t( 1), "AbsIndex({0,0,1}) failed.");
         UNIT_ASSERT_EQUAL(AbsIndex(inp_t({0,1,2}), dims), ret_t( 5), "AbsIndex({0,1,2}) failed.");
         UNIT_ASSERT_EQUAL(AbsIndex(inp_t({1,0,2}), dims), ret_t(14), "AbsIndex({1,0,2}) failed.");
         UNIT_ASSERT_EQUAL(AbsIndex(inp_t({1,3,2}), dims), ret_t(23), "AbsIndex({1,3,2}) failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function IncrMultiIndex.
    ***************************************************************************/
   struct IncrMultiIndexTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::MultiIndex;
         using midas::util::matrep::IncrMultiIndex;
         using midas::util::matrep::Product;
         using mi_t = std::vector<Uin>;
         const mi_t dims = {2,4,3};
         mi_t mi = {0,0,0};
         for(Uin i = 0; i < Product(dims); ++i, IncrMultiIndex(mi,dims))
         {
            const mi_t ctrl = MultiIndex(i, dims);
            UNIT_ASSERT_EQUAL(mi, ctrl, "Wrong multi-index, i = "+std::to_string(i));
         }
         const mi_t ctrl = {0,0,0};
         UNIT_ASSERT_EQUAL(mi, ctrl, "Wrong multi-index, end.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function IncrMultiIndex with abs index increment
    *    returned.
    ***************************************************************************/
   struct IncrMultiIndexWithAbsIncrTest
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::util::matrep;
         using mi_t = std::vector<Uin>;
         using mc_t = std::set<Uin>;
         const std::vector<std::tuple<mc_t,mi_t>> v_tuples =
            {  {{}, {}}
            ,  {{1,4}, {2,1}}
            ,  {{1,4}, {0,0}}
            ,  {{0,1,2,3,4,5}, {1,2,0,0,2,1}}
            };

         for(const auto& t: v_tuples)
         {
            // For controls.
            const mi_t d       = {2,4,1,2,3,3};
            const mc_t m_fix   = std::get<0>(t); // indices in range [0;d.size())
            const mi_t i_fix   = std::get<1>(t);
            const mc_t m_free  = ComplementaryModes(d.size(), m_fix);
            const mi_t d_free  = SubsetDims(m_free, d);

            // For results.
            const mi_t p_cum = CumulativeProduct(d);
            const mi_t p_cum_free = SubsetDims(m_free, p_cum);
            const mi_t init_i_free(m_free.size(), 0); // start at {0,...}, then loop
            mi_t i_free = init_i_free;
            const Uin init_abs_i = AbsIndex(MatRepVibOper<Nb>::CombineIndices(m_fix, i_fix, i_free), d);
            Uin abs_i = init_abs_i;

            std::stringstream t_info;
            t_info << ", m_fix = " << m_fix << ", i_fix = " << i_fix;
            const std::string s_t_info = t_info.str();
            for(Uin i = 0; i < Product(d_free); ++i)
            {
               const std::string s_info = "i = " + std::to_string(i) + s_t_info;

               // Controls.
               const mi_t ctrl_i_free = MultiIndex(i, d_free);
               const mi_t i_tot_a = MatRepVibOper<Nb>::CombineIndices(m_fix, i_fix, ctrl_i_free);
               const Uin ctrl_abs_i = AbsIndex(i_tot_a, d);

               // Results.
               UNIT_ASSERT_EQUAL(i_free, ctrl_i_free, "Wrong i_free, "+s_info);
               UNIT_ASSERT_EQUAL(abs_i, ctrl_abs_i, "Wrong abs_i, "+s_info);

               // Update step.
               In incr = IncrMultiIndex(i_free, d_free, p_cum_free);
               abs_i += incr;
            }

            UNIT_ASSERT_EQUAL(i_free, init_i_free, "Wrong i_free, end" + s_t_info);
            UNIT_ASSERT_EQUAL(abs_i, init_abs_i, "Wrong abs_i, end" + s_t_info);
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function Product.
    ***************************************************************************/
   struct ProductTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::Product;
         using inp_t = std::vector<Uin>;
         using ret_t = Uin;
         UNIT_ASSERT_EQUAL(Product(inp_t({})), ret_t(1), "Product({}) failed.");
         UNIT_ASSERT_EQUAL(Product(inp_t({0})), ret_t(0), "Product({0}) failed.");
         UNIT_ASSERT_EQUAL(Product(inp_t({1})), ret_t(1), "Product({1}) failed.");
         UNIT_ASSERT_EQUAL(Product(inp_t({1,0})), ret_t(0), "Product({1,0}) failed.");
         UNIT_ASSERT_EQUAL(Product(inp_t({3,2})), ret_t(6), "Product({3,2}) failed.");
         UNIT_ASSERT_EQUAL(Product(inp_t({4,2,3})), ret_t(24), "Product({4,2,3}) failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function ComplementaryModes.
    ***************************************************************************/
   struct ComplementaryModesTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::ComplementaryModes;
         using inp_t = std::set<Uin>;
         using ctrl_t = std::vector<Uin>;
         auto s2v = [](const inp_t& s)->ctrl_t {return ctrl_t(s.begin(),s.end());};
         //^ Function returns set, but convert to vector for using available operator<< overload.

         UNIT_ASSERT_EQUAL(s2v(ComplementaryModes(0, inp_t({}))), ctrl_t({}), "Fail: (0,{})");
         UNIT_ASSERT_EQUAL(s2v(ComplementaryModes(3, inp_t({}))), ctrl_t({0,1,2}), "Fail: (3,{})");
         UNIT_ASSERT_EQUAL(s2v(ComplementaryModes(3, inp_t({0,1,2}))), ctrl_t({}), "Fail: (3,{0,1,2})");
         UNIT_ASSERT_EQUAL(s2v(ComplementaryModes(5, inp_t({0,2}))), ctrl_t({1,3,4}), "Fail: (3,{0,2})");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function SubsetDims.
    ***************************************************************************/
   struct SubsetDimsTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::SubsetDims;
         using dims_t = std::vector<Uin>;
         using modes_t = std::set<Uin>;

         UNIT_ASSERT_EQUAL(SubsetDims(modes_t({}), dims_t({})), dims_t({}), "Fail: ({},())");
         UNIT_ASSERT_EQUAL(SubsetDims(modes_t({}), dims_t({2,5,3})), dims_t({}), "Fail: ({},(2,5,3))");
         UNIT_ASSERT_EQUAL(SubsetDims(modes_t({1}), dims_t({2,5,3})), dims_t({5}), "Fail: ({1},(2,5,3))");
         UNIT_ASSERT_EQUAL(SubsetDims(modes_t({0,2}), dims_t({2,5,3})), dims_t({2,3}), "Fail: ({0,2},(2,5,3))");
         UNIT_ASSERT_EQUAL(SubsetDims(modes_t({0,1,2}), dims_t({2,5,3})), dims_t({2,5,3}), "Fail: ({0,1,2},(2,5,3))");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function ShiftVals.
    ***************************************************************************/
   struct ShiftValsTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::ShiftVals;
         using inp_t = std::vector<Uin>;
         using ret_t = std::vector<Uin>;

         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({}), 0), ret_t({}), "Fail: ({}, 0)");
         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({}),-1), ret_t({}), "Fail: ({},-1)");
         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({}),+1), ret_t({}), "Fail: ({},+1)");
         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({3,5,2}), 0), ret_t({3,5,2}), "Fail: ({3,5,2}, 0)");
         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({3,5,2}),-1), ret_t({2,4,1}), "Fail: ({3,5,2},-1)");
         UNIT_ASSERT_EQUAL(ShiftVals(inp_t({3,5,2}),+1), ret_t({4,6,3}), "Fail: ({3,5,2},+1)");
      }
   };

   /************************************************************************//**
    * @brief
    *    Extract vector of excitation indices for a mode combination.
    ***************************************************************************/
   struct ModeCombiExciIndicesTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::ModeCombiExciIndices;
         const std::vector<Uin> dims = {3,4,5};
         std::vector<std::vector<Uin>> ctrl_empty =
            {  {}
            };
         const auto v_ind_empty = ModeCombiExciIndices(std::set<Uin>{}, dims);
         UNIT_ASSERT_EQUAL(v_ind_empty, ctrl_empty, "MC {} failed.");

         std::vector<std::vector<Uin>> ctrl_nonempty =
            {  {1,1}
            ,  {1,2}
            ,  {1,3}
            ,  {1,4}
            ,  {2,1}
            ,  {2,2}
            ,  {2,3}
            ,  {2,4}
            };
         const auto v_ind_nonempty = ModeCombiExciIndices(std::set<Uin>{0,2}, dims);
         UNIT_ASSERT_EQUAL(v_ind_nonempty, ctrl_nonempty, "MC {0,2} failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Extract vector(pair(mode combination, excitation indices))
    *    corresponding to a ModeCombiOpRange.
    ***************************************************************************/
   struct McrExciIndicesTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::McrExciIndices;
         using return_t = std::vector<std::pair<std::set<Uin>, std::vector<std::vector<Uin>>>>;
         const std::vector<Uin> dims = {3,4,5};

         auto lambda_assert = [](const return_t& a, const return_t& b) -> std::pair<bool, std::string>
            {
               std::stringstream err;
               if (a.size() != b.size())
               {
                  err << "a.size() = " << a.size() << " != b.size() = " << b.size();
                  return std::make_pair(false, err.str());
               }
               for(Uin i = 0; i < a.size(); ++i)
               {
                  if (a.at(i).first != b.at(i).first)
                  {
                     err<< "a.at("<<i<<") set = " << a.at(i).first << " != "
                        << "b.at("<<i<<") set = " << b.at(i).first;
                     return std::make_pair(false, err.str());
                  }
                  if (a.at(i).second != b.at(i).second)
                  {
                     err   << "a.at("<<i<<") exci.indices = " << a.at(i).second
                           << " != "
                           << "b.at("<<i<<") exci.indices = " << b.at(i).second
                           ;
                     return std::make_pair(false, err.str());
                  }
               }
               return std::make_pair(true, "");
            };

         ModeCombiOpRange mcr_empty;
         const return_t ctrl_empty =
            {
            };
         const auto v_empty = McrExciIndices(mcr_empty, dims);
         UNIT_ASSERT_EQUAL(Uin(v_empty.size()), Uin(mcr_empty.Size()), "MCR {} wrong size.");
         auto a = lambda_assert(v_empty, ctrl_empty);
         UNIT_ASSERT(a.first, a.second);

         ModeCombiOpRange mcr_empty_mc(0,0);
         const return_t ctrl_empty_mc =
            {  {{}, {{}}}
            };
         const auto v_empty_mc = McrExciIndices(mcr_empty_mc, dims);
         UNIT_ASSERT_EQUAL(Uin(v_empty_mc.size()), Uin(mcr_empty_mc.Size()), "MCR {{}} wrong size.");
         a = lambda_assert(v_empty_mc, ctrl_empty_mc);
         UNIT_ASSERT(a.first, a.second);

         ModeCombiOpRange mcr_nonempty;
         mcr_nonempty.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{},-1)
            ,  ModeCombi(std::set<In>{0},-1)
            ,  ModeCombi(std::set<In>{2},-1)
            ,  ModeCombi(std::set<In>{0,1},-1)
            });
         const return_t ctrl_nonempty =
            {  {{},        {{}}}
            ,  {{0},       {{1}, {2}}}
            ,  {{2},       {{1}, {2}, {3}, {4}}}
            ,  {{0,1},     {{1,1}, {1,2}, {1,3}, {2,1}, {2,2}, {2,3}}}
            };
         const auto v_nonempty = McrExciIndices(mcr_nonempty, dims);
         UNIT_ASSERT_EQUAL(Uin(v_nonempty.size()), Uin(mcr_nonempty.Size()), "MCR {{},{0},{2},{0,1}} wrong size.");
         a = lambda_assert(v_nonempty, ctrl_nonempty);
         UNIT_ASSERT(a.first, a.second);
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct ExtendToSecondarySpaceTestA
      :  public cutee::test
   {
      void run() override
      {
         const ModeCombiOpRange mcr(2,2);
         const std::vector<Uin> n_modals_red = {3,2};
         const std::vector<Uin> n_modals_ext = {4,3};
         const T t_ref     = random::RandomNumber<T>();
         const T t0_1      = random::RandomNumber<T>();
         const T t0_2      = random::RandomNumber<T>();
         const T t0_3      = 0;
         const T t1_1      = random::RandomNumber<T>();
         const T t1_2      = 0;
         const T t01_11    = random::RandomNumber<T>();
         const T t01_12    = 0;
         const T t01_21    = random::RandomNumber<T>();
         const T t01_22    = 0;
         const T t01_31    = 0;
         const T t01_32    = 0;
         const std::vector<std::vector<T>> params_red = 
            {  {  t_ref   // MC: {}
               }
            ,  {  t0_1    // MC: {0}
               ,  t0_2
               }
            ,  {  t1_1    // MC: {1}
               }
            ,  {  t01_11  // MC: {0,1}
               ,  t01_21 
               }
            };
         const std::vector<std::vector<T>> params_ext_ctrl = 
            {  {  t_ref   // MC: {}
               }
            ,  {  t0_1    // MC: {0}
               ,  t0_2
               ,  t0_3
               }
            ,  {  t1_1    // MC: {1}
               ,  t1_2
               }
            ,  {  t01_11  // MC: {0,1}
               ,  t01_12
               ,  t01_21 
               ,  t01_22 
               ,  t01_31 
               ,  t01_32 
               }
            };
         const std::vector<std::vector<T>> params_ext = midas::util::matrep::ExtendToSecondarySpace
            (  mcr
            ,  n_modals_red
            ,  n_modals_ext
            ,  params_red
            );
         UNIT_ASSERT_EQUAL(params_ext.size(), params_ext_ctrl.size(), "Wrong size().");
         for(Uin i = 0; i < params_ext_ctrl.size(); ++i)
         {
            UNIT_ASSERT_EQUAL(params_ext.at(i).size(), params_ext_ctrl.at(i).size(), "Wrong size(), i = "+std::to_string(i)+".");
            for(Uin j = 0; j < params_ext_ctrl.at(i).size(); ++j)
            {
               const auto& val  = params_ext.at(i).at(j);
               const auto& ctrl = params_ext_ctrl.at(i).at(j);
               UNIT_ASSERT_FEQUAL_PREC(val, ctrl, 0, "Inequality, i = "+std::to_string(i)+", j = "+std::to_string(j)+".");
            }
         }
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct ExtendToSecondarySpaceTestB
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::util::matrep;
         const ModeCombiOpRange mcr(3,3);
         const std::vector<Uin> n_modals_red = {2,3,2};
         const std::vector<Uin> n_modals_ext = {4,4,3};
         const std::vector<std::vector<T>> params_red = util::GetRandomParams<T>(mcr, n_modals_red);
         const std::vector<std::vector<T>> params_ext = midas::util::matrep::ExtendToSecondarySpace
            (  mcr
            ,  n_modals_red
            ,  n_modals_ext
            ,  params_red
            );
         UNIT_ASSERT_EQUAL(Uin(params_ext.size()), Uin(mcr.Size()), "Wrong size().");
         for(Uin i = 0; i < mcr.Size(); ++i)
         {
            const auto& mc = mcr.GetModeCombi(i);
            const Uin ctrl_size = NumParams(mc, n_modals_ext);
            UNIT_ASSERT_EQUAL(params_ext.at(i).size(), ctrl_size, "Wrong size(), i = "+std::to_string(i)+".");
            const auto sub_dims_red = ShiftVals(SubsetDims(SetFromVec(mc.MCVec()), n_modals_red), -1);
            const auto sub_dims_ext = ShiftVals(SubsetDims(SetFromVec(mc.MCVec()), n_modals_ext), -1);
            for(Uin j = 0; j < ctrl_size; ++j)
            {
               const auto mi_ext = MultiIndex(j, sub_dims_ext);
               bool is_sec_space = false;
               for(Uin m = 0; m < mi_ext.size(); ++m)
               {
                  is_sec_space = (mi_ext.at(m) >= sub_dims_red.at(m))? true: is_sec_space;
               }
               const T val  = params_ext.at(i).at(j);
               T ctrl = 0;
               if (!is_sec_space)
               {
                  const Uin abs_j_red = AbsIndex(mi_ext, sub_dims_red);
                  ctrl = params_red.at(i).at(abs_j_red);
               }
               std::stringstream ss;
               ss << "Inequality, i = " << i << ", j = " << j << ", mi_ext = " << mi_ext << ".";
               UNIT_ASSERT_FEQUAL_PREC(val, ctrl, 0, ss.str());
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Check conversion from vector following ModeCombiOpRange ordering to an
    *    ordered std::vector<std::vector<T>>.
    *    And back!
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct OrganizeMcrSpaceVecTest
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::OrganizeMcrSpaceVec;
         using midas::util::matrep::McrOrganizedToStackedVec;
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{},-1)
            ,  ModeCombi(std::set<In>{0},-1)
            ,  ModeCombi(std::set<In>{1},-1)
            ,  ModeCombi(std::set<In>{0,1},-1)
            ,  ModeCombi(std::set<In>{1,2},-1)
            });
         const std::vector<Uin> dims = {2,2,3};
         const T e_000 = random::RandomNumber<T>();
         const T e_010 = random::RandomNumber<T>();
         const T e_011 = random::RandomNumber<T>();
         const T e_012 = random::RandomNumber<T>();
         const T e_100 = random::RandomNumber<T>();
         const T e_110 = random::RandomNumber<T>();

         const CONT_T<T> v_test(GeneralMidasVector<T>(std::vector<T>
            {  e_000    // MC: {}
            ,  e_100    // MC: {0}
            ,  e_010    // MC: {1}
            ,  e_110    // MC: {0,1}
            ,  e_011    // MC: {1,2}
            ,  e_012    // (same)
            }));
         const std::vector<std::vector<T>> v_ctrl =
            {  {e_000}       // MC: {}
            ,  {e_100}       // MC: {0}
            ,  {e_010}       // MC: {1}
            ,  {e_110}       // MC: {0,1}
            ,  {e_011,e_012} // MC: {1,2}
            };

         std::vector<std::vector<T>> v_res = OrganizeMcrSpaceVec(dims, mcr, v_test);
         UNIT_ASSERT_EQUAL(v_res, v_ctrl, "Bad OrganizeMcrSpaceVec.");

         CONT_T<T> v_res_inv = McrOrganizedToStackedVec<T,CONT_T>(v_res);
         UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(v_res_inv, v_test)), absval_t<T>(1), 0, "Bad McrOrganizedToStackedVec.");
      }
   };

} /* namespace midas::test::matrep::utils */

namespace midas::test::matrep::viboper
{
   /************************************************************************//**
    * @brief
    *    Construct initial zero matrix of correct dimensions.
    ***************************************************************************/
   template<typename T>
   struct Constructor
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 3, 4};
         const Uin full_dim = std::accumulate(dims.begin(), dims.end(), Uin(1), std::multiplies<>());

         MatRepVibOper<T> matrep(dims);
         UNIT_ASSERT_EQUAL(matrep.FullDim(), full_dim, "Wrong full dimension.");
         UNIT_ASSERT_EQUAL(matrep.NModes(), Uin(3), "Wrong number of modes.");
         for(Uin i = 0; i < matrep.NModes(); ++i)
         {
            UNIT_ASSERT_EQUAL(matrep.Dim(i), dims.at(i), "Wrong Dim("+std::to_string(i)+").");
         }
         UNIT_ASSERT_EQUAL(matrep.Dims(), dims, "Wrong Dims().");

         mat_t<T> mat = matrep.GetMatRep();
         UNIT_ASSERT_EQUAL(Uin(mat.Nrows()), full_dim, "Wrong num. matrix rows.");
         UNIT_ASSERT_EQUAL(Uin(mat.Ncols()), full_dim, "Wrong num. matrix cols.");
         mat_t<T> mat_expect(full_dim, full_dim, T(0));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a direct product of 1-mode operators for all modes.
    ***************************************************************************/
   template<typename T>
   struct DirProdOneModeOpersAllModes
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 3};
         MatRepVibOper<T> matrep(dims);
         mat_t<T> oper_0(2);
         mat_t<T> oper_1(3);
         oper_0.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1
            , -1, 2
            }));
         oper_1.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0, 2
            ,  0, 0, 3
            ,  1,-2, 1
            }));
         mat_t<T> mat_expect(6);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0,-1, 0, 2
            ,  0, 0, 0, 0, 0, 3
            ,  0, 0, 0, 1,-2, 1
            ,  1, 0,-2,-2, 0, 4
            ,  0, 0,-3, 0, 0, 6
            , -1, 2,-1, 2,-4, 2
            }));
         matrep.DirProdOneModeOpers(std::vector<mat_t<T>>{oper_0, oper_1});
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a direct product of 1-mode operators for some modes (identity
    *    operator for remaining modes).
    ***************************************************************************/
   template<typename T>
   struct DirProdOneModeOpersSomeModes
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 2, 3};
         MatRepVibOper<T> matrep(dims);
         mat_t<T> oper_0(2);
         mat_t<T> oper_2(3);
         oper_0.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1
            , -1, 2
            }));
         oper_2.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0, 2
            ,  0, 0, 3
            ,  1,-2, 1
            }));
         mat_t<T> mat_expect(12);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0, 0, 0, 0,-1, 0, 2, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0, 1,-2, 1, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 0, 2
            ,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3
            ,  0, 0, 0, 0, 0, 0, 0, 0, 0, 1,-2, 1
            ,  1, 0,-2, 0, 0, 0,-2, 0, 4, 0, 0, 0
            ,  0, 0,-3, 0, 0, 0, 0, 0, 6, 0, 0, 0
            , -1, 2,-1, 0, 0, 0, 2,-4, 2, 0, 0, 0
            ,  0, 0, 0, 1, 0,-2, 0, 0, 0,-2, 0, 4
            ,  0, 0, 0, 0, 0,-3, 0, 0, 0, 0, 0, 6
            ,  0, 0, 0,-1, 2,-1, 0, 0, 0, 2,-4, 2
            }));
         matrep.DirProdOneModeOpers(std::set<Uin>{0, 2}, std::vector<mat_t<T>>{oper_0, oper_2});
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a "full" operator from an entire OpDef and associated
    *    ModalIntegrals. "Standard" ModeCombiOpRange for OpDef.
    ***************************************************************************/
   template<typename T>
   struct FullOperatorFromOpDefA
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 2};
         MatRepVibOper<T> matrep(dims);

         // Set up a simple OpDef. It's an awkward way to do it, but the only
         // way I'm certain the object will be internally constistent.
         // This OpDef is pretty "standard";
         // - global mode MCR (as written in file): {0}, {1}, {0,1}
         // - local  mode MCR (as stored in OpDef): {0}, {1}, {0,1} (same!)
         // I.e. the OpDef's internal local->global mode map should be
         //    0 -> 0
         //    1 -> 1
         const absval_t<T> c0 = 1, c1 = -2, c2 = 2, c3 = -1, c4 = 5;
         std::stringstream ss;
         ss << "#0MIDASOPERATOR\n"
            << "#1MODENAMES\n"
            << "Q0 Q1\n"
            << "#1SCALEFACTORS\n"
            << "1.0E+00 1.0E+00\n"
            << "#1OPERATORTERMS\n"
            << c0 << " Q^1(Q0)\n"
            << c1 << " Q^2(Q0)\n"
            << c2 << " Q^1(Q1)\n"
            << c3 << " Q^2(Q1)\n"
            << c4 << " Q^2(Q0) Q^2(Q1)\n"
            << "#0MIDASOPERATOREND\n";
         OpDef opdef;
         midas::input::ReadGenericMidasInput(ss, opdef, "", -1, 0);
         opdef.InitOpRange();
         opdef.Reorganize(true);
         opdef.MCRReorder();

         const std::vector<GlobalModeNr> ctrl_loc2glob = {0, 1};
         UNIT_ASSERT_EQUAL(Uin(opdef.NmodesInOp()), Uin(ctrl_loc2glob.size()), "Wrong num. modes in OpDef.");
         for(LocalModeNr m = 0; m < opdef.NmodesInOp(); ++m)
         {
            UNIT_ASSERT_EQUAL(opdef.GetGlobalModeNr(m), ctrl_loc2glob.at(m), "Wrong global mode, local m = "+std::to_string(m));
         }

         // The modal integrals. NB! OpDef implicitly adds the Q^0 operator for
         // all modes. We'll just leave that at zero here.
         mat_t<T> m_0_0(dims.at(0));
         mat_t<T> m_0_1(dims.at(0));
         mat_t<T> m_0_2(dims.at(0));
         mat_t<T> m_1_0(dims.at(1));
         mat_t<T> m_1_1(dims.at(1));
         mat_t<T> m_1_2(dims.at(1));
         m_0_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1,-1
            ,  0, 0
            }));
         m_0_2.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0
            ,  2, 0
            }));
         m_1_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0
            ,  0, 3
            }));
         m_1_2.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1
            ,  0,-1
            }));
         std::vector<std::vector<mat_t<T>>> v_ints =
            {  {  m_0_0, m_0_1, m_0_2 }
            ,  {  m_1_0, m_1_1, m_1_2 }
            };
         ModalIntegrals<T> mi(std::move(v_ints));

         // The control.
         mat_t<T> m_c0(matrep.FullDim());
         m_c0.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1, 0,-1, 0
            ,  0, 1, 0,-1
            ,  0, 0, 0, 0
            ,  0, 0, 0, 0
            }));
         mat_t<T> m_c1(matrep.FullDim());
         m_c1.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0, 0, 0
            ,  0,-1, 0, 0
            ,  2, 0, 0, 0
            ,  0, 2, 0, 0
            }));
         mat_t<T> m_c2(matrep.FullDim());
         m_c2.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0, 0
            ,  0, 3, 0, 0
            ,  0, 0, 0, 0
            ,  0, 0, 0, 3
            }));
         mat_t<T> m_c3(matrep.FullDim());
         m_c3.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1, 0, 0
            ,  0,-1, 0, 0
            ,  0, 0, 0, 1
            ,  0, 0, 0,-1
            }));
         mat_t<T> m_c4(matrep.FullDim());
         m_c4.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0,-1, 0, 0
            ,  0, 1, 0, 0
            ,  0, 2, 0, 0
            ,  0,-2, 0, 0
            }));
         const mat_t<T> m_ctrl = c0*m_c0 + c1*m_c1 + c2*m_c2 + c3*m_c3 + c4*m_c4;

         // The MatRepVibOper method, and assertion.
         matrep.FullOperator(opdef, mi);
         mat_t<T> diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case a.");

         // ... and expecting same result if providing the proper local->global
         // mode map.
         std::unordered_map<LocalModeNr,Uin> mode_map;
         for(LocalModeNr m = 0; m < ctrl_loc2glob.size(); ++m)
         {
            mode_map[m] = Uin(ctrl_loc2glob.at(m));
         }
         matrep.FullOperator(opdef, mi, mode_map);
         diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case b.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a "full" operator from an entire OpDef and associated
    *    ModalIntegrals. Non-"standard" ModeCombiOpRange for OpDef.
    ***************************************************************************/
   template<typename T>
   struct FullOperatorFromOpDefB
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> glob_dims = {2, 3, 2, 1};
         MatRepVibOper<T> matrep(glob_dims);

         // Set up a simple OpDef. It's an awkward way to do it, but the only
         // way I'm certain the object will be internally constistent.
         // This OpDef is tricky;
         // - global mode MCR (as written in file): {2}, {1,3}
         // - local  mode MCR (as stored in OpDef): {0}, {1,2}
         // I.e. the OpDef's internal local->global mode map should be
         //    0 -> 2
         //    1 -> 1
         //    2 -> 3
         const absval_t<T> c0 = 1, c1 = -2;
         std::stringstream ss;
         ss << "#0MIDASOPERATOR\n"
            << "#1MODENAMES\n"
            << "Q2 Q1 Q3\n"
            << "#1SCALEFACTORS\n"
            << "1.0E+00 1.0E+00 1.0E+00 1.0E+00 1.0E+00\n"
            << "#1OPERATORTERMS\n"
            << c0 << " Q^1(Q2)\n"
            << c1 << " Q^1(Q1) Q^1(Q3)\n"
            << "#0MIDASOPERATOREND\n";
         OpDef opdef;
         midas::input::ReadGenericMidasInput(ss, opdef, "", -1, 0);
         opdef.InitOpRange();
         opdef.Reorganize(true);
         opdef.MCRReorder();

         const std::vector<GlobalModeNr> ctrl_loc2glob = {2, 1, 3};
         UNIT_ASSERT_EQUAL(Uin(opdef.NmodesInOp()), Uin(ctrl_loc2glob.size()), "Wrong num. modes in OpDef.");
         for(LocalModeNr m = 0; m < opdef.NmodesInOp(); ++m)
         {
            UNIT_ASSERT_EQUAL(opdef.GetGlobalModeNr(m), ctrl_loc2glob.at(m), "Wrong global mode, local m = "+std::to_string(m));
         }

         // The modal integrals. NB! OpDef implicitly adds the Q^0 operator for
         // all modes. We'll just leave that at zero here.
         mat_t<T> m_loc0_glob2_0(glob_dims.at(2));
         mat_t<T> m_loc0_glob2_1(glob_dims.at(2));
         mat_t<T> m_loc1_glob1_0(glob_dims.at(1));
         mat_t<T> m_loc1_glob1_1(glob_dims.at(1));
         mat_t<T> m_loc2_glob3_0(glob_dims.at(3));
         mat_t<T> m_loc2_glob3_1(glob_dims.at(3));

         m_loc0_glob2_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1,-1
            ,  0, 0
            }));
         m_loc1_glob1_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 2
            , -1,-1, 0
            ,  3, 0, 1
            }));
         m_loc2_glob3_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  2
            }));

         std::vector<std::vector<mat_t<T>>> v_ints =
            {  {  m_loc0_glob2_0, m_loc0_glob2_1 }
            ,  {  m_loc1_glob1_0, m_loc1_glob1_1 }
            ,  {  m_loc2_glob3_0, m_loc2_glob3_1 }
            };
         ModalIntegrals<T> mi(std::move(v_ints));

         // The control.
         mat_t<T> m_ctrl(matrep.FullDim(), matrep.FullDim(), T(0));
         matrep.DirProdOneModeOpers
            (  std::set<Uin>{2}
            ,  std::vector<mat_t<T>>{m_loc0_glob2_1}
            );
         m_ctrl += c0 * matrep.GetMatRep();
         matrep.DirProdOneModeOpers
            (  std::set<Uin>{1,3}
            ,  std::vector<mat_t<T>>{m_loc1_glob1_1, m_loc2_glob3_1}
            );
         m_ctrl += c1 * matrep.GetMatRep();

         // The MatRepVibOper method, and assertion.
         matrep.FullOperator(opdef, mi);
         mat_t<T> diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case a.");

         // ... and expecting same result if providing the proper local->global
         // mode map.
         std::unordered_map<LocalModeNr,Uin> mode_map;
         for(LocalModeNr m = 0; m < ctrl_loc2glob.size(); ++m)
         {
            mode_map[m] = Uin(ctrl_loc2glob.at(m));
         }
         matrep.FullOperator(opdef, mi, mode_map);
         diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case b.");
      }
   };


   /************************************************************************//**
    * @brief
    *    Make a "full" operator from an entire OpDef and associated
    *    ModalIntegrals. Non-"standard" ModeCombiOpRange for OpDef.
    ***************************************************************************/
   template<typename T>
   struct FullOperatorFromOpDefBScrambled
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> glob_dims = {2, 3, 2, 1};
         MatRepVibOper<T> matrep(glob_dims);

         // Set up a simple OpDef. It's an awkward way to do it, but the only
         // way I'm certain the object will be internally constistent.
         // This OpDef is tricky;
         // - global mode MCR (as written in file): {2}, {1,3}
         // - local  mode MCR (as stored in OpDef): {0}, {1,2}
         // I.e. the OpDef's internal local->global mode map should be
         //    0 -> 2
         //    1 -> 1
         //    2 -> 3
         const absval_t<T> c0 = 1, c1 = -2;
         std::stringstream ss;
         ss << "#0MIDASOPERATOR\n"
            << "#1MODENAMES\n"
            << "Q2 Q1 Q3\n"
            << "#1SCALEFACTORS\n"
            << "1.0E+00 1.0E+00 1.0E+00 1.0E+00 1.0E+00\n"
            << "#1OPERATORTERMS\n"
            << c1 << " Q^1(Q1) Q^1(Q3)\n"
            << c0 << " Q^1(Q2)\n"
            << "#0MIDASOPERATOREND\n";
         OpDef opdef;
         midas::input::ReadGenericMidasInput(ss, opdef, "", -1, 0);
         opdef.InitOpRange();
         opdef.Reorganize(true);
         opdef.MCRReorder();

         const std::vector<GlobalModeNr> ctrl_loc2glob = {2, 1, 3};
         UNIT_ASSERT_EQUAL(Uin(opdef.NmodesInOp()), Uin(ctrl_loc2glob.size()), "Wrong num. modes in OpDef.");
         for(LocalModeNr m = 0; m < opdef.NmodesInOp(); ++m)
         {
            UNIT_ASSERT_EQUAL(opdef.GetGlobalModeNr(m), ctrl_loc2glob.at(m), "Wrong global mode, local m = "+std::to_string(m));
         }

         // The modal integrals. NB! OpDef implicitly adds the Q^0 operator for
         // all modes. We'll just leave that at zero here.
         mat_t<T> m_loc0_glob2_0(glob_dims.at(2));
         mat_t<T> m_loc0_glob2_1(glob_dims.at(2));
         mat_t<T> m_loc1_glob1_0(glob_dims.at(1));
         mat_t<T> m_loc1_glob1_1(glob_dims.at(1));
         mat_t<T> m_loc2_glob3_0(glob_dims.at(3));
         mat_t<T> m_loc2_glob3_1(glob_dims.at(3));

         m_loc0_glob2_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1,-1
            ,  0, 0
            }));
         m_loc1_glob1_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 2
            , -1,-1, 0
            ,  3, 0, 1
            }));
         m_loc2_glob3_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  2
            }));

         std::vector<std::vector<mat_t<T>>> v_ints =
            {  {  m_loc0_glob2_0, m_loc0_glob2_1 }
            ,  {  m_loc1_glob1_0, m_loc1_glob1_1 }
            ,  {  m_loc2_glob3_0, m_loc2_glob3_1 }
            };
         ModalIntegrals<T> mi(std::move(v_ints));

         // The control.
         mat_t<T> m_ctrl(matrep.FullDim(), matrep.FullDim(), T(0));
         matrep.DirProdOneModeOpers
            (  std::set<Uin>{2}
            ,  std::vector<mat_t<T>>{m_loc0_glob2_1}
            );
         m_ctrl += c0 * matrep.GetMatRep();
         matrep.DirProdOneModeOpers
            (  std::set<Uin>{1,3}
            ,  std::vector<mat_t<T>>{m_loc1_glob1_1, m_loc2_glob3_1}
            );
         m_ctrl += c1 * matrep.GetMatRep();

         // The MatRepVibOper method, and assertion.
         matrep.FullOperator(opdef, mi);
         mat_t<T> diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case a.");

         // ... and expecting same result if providing the proper local->global
         // mode map.
         std::unordered_map<LocalModeNr,Uin> mode_map;
         for(LocalModeNr m = 0; m < ctrl_loc2glob.size(); ++m)
         {
            mode_map[m] = Uin(ctrl_loc2glob.at(m));
         }
         matrep.FullOperator(opdef, mi, mode_map);
         diffmat = matrep.GetMatRep() - m_ctrl;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm, case b.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Test making a block of an operator.
    ***************************************************************************/
   template<typename T>
   struct FullOperatorBlock
      :  public cutee::test
   {
      void run() override
      {
         using midas::test::random::RandomNumber;
         std::vector<Uin> n_modals = {2,3,2};
         std::vector<Uin> opers = {2,2,2};
         ModeCombiOpRange mcr(3, 3);
         const auto coefs = util::GetRandomParams<midas::type_traits::RealTypeT<T>>(mcr,opers,false,false,0);
         auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
         auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, n_modals, 'g');

         // For control.
         mat_t<T> mr_mat;
         {
            MatRepVibOper<T> mr(n_modals);
            mr.FullOperator(opdef, mod_ints);
            mr_mat = std::move(mr).GetMatRep();
         }
         const Uin full_dim = mr_mat.Nrows();

         const Uin rnd_r_beg = RandomNumber<Uin>() % full_dim;
         const Uin rnd_r_end = rnd_r_beg + RandomNumber<Uin>() % (full_dim + 1 - rnd_r_beg);
         const Uin rnd_c_beg = RandomNumber<Uin>() % full_dim;
         const Uin rnd_c_end = rnd_c_beg + RandomNumber<Uin>() % (full_dim + 1 - rnd_c_beg);
         for(const auto& indices: std::vector<std::array<Uin,4>>
               {  {0,full_dim,0,full_dim}
               ,  {0,0,0,0}
               ,  {2,9,3,12}
               ,  {rnd_r_beg, rnd_r_end, rnd_c_beg, rnd_c_end}
               }
            )
         {
            const Uin r_beg = indices.at(0);
            const Uin r_end = indices.at(1);
            const Uin c_beg = indices.at(2);
            const Uin c_end = indices.at(3);

            // Control. (Manually slice out the block of the matrix.)
            mat_t<T> ctrl(r_end - r_beg, c_end - c_beg, T(0));
            for(Uin i = r_beg; i < r_end; ++i)
            {
               for(Uin j = c_beg; j < c_end; ++j)
               {
                  ctrl[i - r_beg][j - c_beg] = mr_mat[i][j];
               }
            }

            // Result.
            mat_t<T> res;
            MatRepVibOper<T>::FullOperator(n_modals, opdef, mod_ints, res, r_beg, r_end, c_beg, c_end);

            // Assertion.
            std::stringstream ss;
            ss << " for slice "
               << "[" << r_beg << ";" << r_end << ") x "
               << "[" << c_beg << ";" << c_end << ")."
               ;
            const std::string info = ss.str();
            UNIT_ASSERT_EQUAL(res.Nrows(), ctrl.Nrows(), "Wrong Nrows()" + info);
            UNIT_ASSERT_EQUAL(res.Ncols(), ctrl.Ncols(), "Wrong Ncols()" + info);
            if (res.Nrows() > 0 && res.Ncols() > 0)
            {
               UNIT_ASSERT_FZERO_PREC(ctrl.DifferenceNorm(res), absval_t<T>(1), 0, "Wrong FullOperator() block" + info);
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a "full" operator from an entire OpDef and associated
    *    ModalIntegrals, using only active terms for given mode. "Standard"
    *    ModeCombiOpRange for OpDef.
    ***************************************************************************/
   template<typename T>
   struct FullActiveTermsOperatorFromOpDefA
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 2};
         MatRepVibOper<T> matrep(dims);

         // Set up a simple OpDef. It's an awkward way to do it, but the only
         // way I'm certain the object will be internally constistent.
         // This OpDef is pretty "standard";
         // - global mode MCR (as written in file): {0}, {1}, {0,1}
         // - local  mode MCR (as stored in OpDef): {0}, {1}, {0,1} (same!)
         // I.e. the OpDef's internal local->global mode map should be
         //    0 -> 0
         //    1 -> 1
         const absval_t<T> c0 = 1, c1 = -2, c2 = 2, c3 = -1, c4 = 5;
         std::stringstream ss;
         ss << "#0MIDASMOPINPUT\n"
            << "#1MODENAMES\n"
            << "Q0 Q1\n"
            << "#1SCALEFACTORS\n"
            << "1.0E+00 1.0E+00\n"
            << "#1OPERATORTERMS\n"
            << c0 << " Q^1(Q0)\n"
            << c1 << " Q^2(Q0)\n"
            << c2 << " Q^1(Q1)\n"
            << c3 << " Q^2(Q1)\n"
            << c4 << " Q^2(Q0) Q^2(Q1)\n"
            << "#0MIDASMOPINPUTEND\n";
         OpDef opdef;
         midas::input::ReadGenericMidasInput(ss, opdef, "", -1, 0);
         opdef.InitOpRange();
         opdef.Reorganize(true);
         opdef.MCRReorder();

         const std::vector<GlobalModeNr> ctrl_loc2glob = {0, 1};
         UNIT_ASSERT_EQUAL(Uin(opdef.NmodesInOp()), Uin(ctrl_loc2glob.size()), "Wrong num. modes in OpDef.");
         for(LocalModeNr m = 0; m < opdef.NmodesInOp(); ++m)
         {
            UNIT_ASSERT_EQUAL(opdef.GetGlobalModeNr(m), ctrl_loc2glob.at(m), "Wrong global mode, local m = "+std::to_string(m));
         }

         // The modal integrals. NB! OpDef implicitly adds the Q^0 operator for
         // all modes. We'll just leave that at zero here.
         mat_t<T> m_0_0(dims.at(0));
         mat_t<T> m_0_1(dims.at(0));
         mat_t<T> m_0_2(dims.at(0));
         mat_t<T> m_1_0(dims.at(1));
         mat_t<T> m_1_1(dims.at(1));
         mat_t<T> m_1_2(dims.at(1));
         m_0_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1,-1
            ,  0, 0
            }));
         m_0_2.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0
            ,  2, 0
            }));
         m_1_1.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0
            ,  0, 3
            }));
         m_1_2.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1
            ,  0,-1
            }));
         std::vector<std::vector<mat_t<T>>> v_ints =
            {  {  m_0_0, m_0_1, m_0_2 }
            ,  {  m_1_0, m_1_1, m_1_2 }
            };
         ModalIntegrals<T> mi(std::move(v_ints));

         // The control.
         mat_t<T> m_c0(matrep.FullDim());
         m_c0.MatrixFromVector(vec_t<T>(std::vector<T>
            {  1, 0,-1, 0
            ,  0, 1, 0,-1
            ,  0, 0, 0, 0
            ,  0, 0, 0, 0
            }));
         mat_t<T> m_c1(matrep.FullDim());
         m_c1.MatrixFromVector(vec_t<T>(std::vector<T>
            { -1, 0, 0, 0
            ,  0,-1, 0, 0
            ,  2, 0, 0, 0
            ,  0, 2, 0, 0
            }));
         mat_t<T> m_c2(matrep.FullDim());
         m_c2.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0, 0
            ,  0, 3, 0, 0
            ,  0, 0, 0, 0
            ,  0, 0, 0, 3
            }));
         mat_t<T> m_c3(matrep.FullDim());
         m_c3.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 1, 0, 0
            ,  0,-1, 0, 0
            ,  0, 0, 0, 1
            ,  0, 0, 0,-1
            }));
         mat_t<T> m_c4(matrep.FullDim());
         m_c4.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0,-1, 0, 0
            ,  0, 1, 0, 0
            ,  0, 2, 0, 0
            ,  0,-2, 0, 0
            }));

         std::map<Uin,std::map<bool,mat_t<T>>> ctrl_map;
         // Mode 0, ctrl.
         // Active terms (true):    0, 1, 4
         // Inactive terms (false): 2, 3
         ctrl_map[0][true]   = mat_t<T>(c0*m_c0 + c1*m_c1 + c4*m_c4);
         ctrl_map[0][false]  = mat_t<T>(c2*m_c2 + c3*m_c3);

         // Mode 1, ctrl.
         // Active terms (true):    2, 3, 4
         // Inactive terms (false): 0, 1
         ctrl_map[1][true]  = mat_t<T>(c2*m_c2 + c3*m_c3 + c4*m_c4);
         ctrl_map[1][false] = mat_t<T>(c0*m_c0 + c1*m_c1);

         // Full operator matrix.
         matrep.FullOperator(opdef, mi);
         const mat_t<T> ctrl_full = matrep.GetMatRep();

         // The MatRepVibOper method, and assertion.
         for(const auto& [mode, map_act]: ctrl_map)
         {
            std::stringstream info;
            info << ", mode = " << mode << ".";

            matrep.FullOperator(opdef, mi, std::make_pair(true, mode));
            const mat_t<T> mat_act = matrep.GetMatRep();
            UNIT_ASSERT_EQUAL(mat_act.Nrows(), map_act.at(true).Nrows(), "Wrong Nrows()"+info.str());
            UNIT_ASSERT_EQUAL(mat_act.Ncols(), map_act.at(true).Ncols(), "Wrong Ncols()"+info.str());
            UNIT_ASSERT_FEQUAL_PREC(mat_t<T>(mat_act - map_act.at(true)).Norm(), absval_t<T>(0), 0, "Non-zero diff.norm (act.)"+info.str());

            matrep.FullOperator(opdef, mi, std::make_pair(false, mode));
            const mat_t<T> mat_full = matrep.GetMatRep();
            UNIT_ASSERT_FEQUAL_PREC(mat_t<T>(mat_full - ctrl_full).Norm(), absval_t<T>(0), 0, "Non-zero diff.norm (full)"+info.str());

            const mat_t<T> mat_inact = mat_t<T>(mat_full - mat_act);
            UNIT_ASSERT_FEQUAL_PREC(mat_t<T>(mat_inact - map_act.at(false)).Norm(), absval_t<T>(0), 0, "Non-zero diff.norm (inact.)"+info.str());
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a direct product of 1-mode operators for some modes (identity
    *    operator for remaining modes).
    ***************************************************************************/
   template<typename T>
   struct ShiftOper
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {3, 2};
         MatRepVibOper<T> matrep(dims);

         // 1-mode shift operator.
         // E^{0}_{1 <-- 2}
         // =
         //    0 0 0     1 0
         //    0 0 1  x  0 1
         //    0 0 0
         mat_t<T> mat_expect(6);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 1, 0
            ,  0, 0, 0, 0, 0, 1
            ,  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            }));
         matrep.ShiftOper(std::set<Uin>{0}, std::vector<Uin>{1}, std::vector<Uin>{2});
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm., 1-mode shift.");

         // 2-mode shift operator.
         // E^{0,1}_{2,0 <-- 0,1}
         // =
         //    0 0 0     0 1
         //    0 0 0  x  0 0
         //    1 0 0
         // multiplied by scalar.
         T v = -1.337;
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            ,  0, v, 0, 0, 0, 0
            ,  0, 0, 0, 0, 0, 0
            }));
         matrep.ShiftOper(std::set<Uin>{0,1}, std::vector<Uin>{2,0}, std::vector<Uin>{0,1}, v);
         diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm., 2-mode shift.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 0-mode cluster operator, i.e. with a scaler on the diagonal.
    ***************************************************************************/
   template<typename T>
   struct ClusterOper0
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 2};
         MatRepVibOper<T> matrep(dims);
         T t = random::RandomNumber<T>();
         const std::vector<T> v_amps = {t};
         matrep.ClusterOper(std::set<Uin>{}, v_amps);

         // Needs to shift all states to themselves multiplied by scalar.
         mat_t<T> mat_expect(4, T(0));
         mat_expect.SetDiagToNb(t);
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 1-mode \f$ T_1 \f$ cluster operator.
    ***************************************************************************/
   template<typename T>
   struct ClusterOper1
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 3, 2};
         MatRepVibOper<T> matrep(dims);
         T t1 = random::RandomNumber<T>();
         T t2 = random::RandomNumber<T>();
         const std::vector<T> v_amps = {t1, t2};
         matrep.ClusterOper(std::set<Uin>{1}, v_amps);

         // Needs to shift
         //    (0,0,0) --> t1 (0,1,0), t2 (0,2,0)
         //    (0,0,1) --> t1 (0,1,1), t2 (0,2,1)
         //    (1,0,0) --> t1 (1,1,0), t2 (1,2,0)
         //    (1,0,1) --> t1 (1,1,1), t2 (1,2,1)
         mat_t<T> mat_expect(12);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0
            ,  0, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0

            , t1, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0
            ,  0,t1,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0

            , t2, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0
            ,  0,t2,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0


            ,  0, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0
            ,  0, 0,  0, 0,  0, 0,    0, 0,  0, 0,  0, 0

            ,  0, 0,  0, 0,  0, 0,   t1, 0,  0, 0,  0, 0
            ,  0, 0,  0, 0,  0, 0,    0,t1,  0, 0,  0, 0

            ,  0, 0,  0, 0,  0, 0,   t2, 0,  0, 0,  0, 0
            ,  0, 0,  0, 0,  0, 0,    0,t2,  0, 0,  0, 0
            }));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 2-mode \f$ T_2 \f$ cluster operator.
    ***************************************************************************/
   template<typename T>
   struct ClusterOper2
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 3, 3};
         MatRepVibOper<T> matrep(dims);
         T t11 = random::RandomNumber<T>();
         T t12 = random::RandomNumber<T>();
         T t21 = random::RandomNumber<T>();
         T t22 = random::RandomNumber<T>();
         const std::vector<T> v_amps = {t11, t12, t21, t22};
         matrep.ClusterOper(std::set<Uin>{1,2}, v_amps);

         // Needs to shift
         //    (0,0,0) --> t11 (0,1,1), t12 (0,1,2), t21 (0,2,1), t22 (0,2,2)
         //    (1,0,0) --> t11 (1,1,1), t12 (1,1,2), t21 (1,2,1), t22 (1,2,2)
         mat_t<T> mat_expect(18);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0

            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,t11, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,t12, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0

            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,t21, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,t22, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0


            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0

            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,  t11, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,  t12, 0, 0,  0, 0, 0,  0, 0, 0

            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,    0, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,  t21, 0, 0,  0, 0, 0,  0, 0, 0
            ,  0, 0, 0,  0, 0, 0,  0, 0, 0,  t22, 0, 0,  0, 0, 0,  0, 0, 0
            }));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 3-mode \f$ T_3 \f$ cluster operator.
    ***************************************************************************/
   template<typename T>
   struct ClusterOper3
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {3, 3, 3};
         MatRepVibOper<T> matrep(dims);
         T t111 = random::RandomNumber<T>();
         T t112 = random::RandomNumber<T>();
         T t121 = random::RandomNumber<T>();
         T t122 = random::RandomNumber<T>();
         T t211 = random::RandomNumber<T>();
         T t212 = random::RandomNumber<T>();
         T t221 = random::RandomNumber<T>();
         T t222 = random::RandomNumber<T>();
         const std::vector<T> v_amps = {t111, t112, t121, t122, t211, t212, t221, t222};
         matrep.ClusterOper(std::set<Uin>{0,1,2}, v_amps);

         // Needs to shift
         //    (0,0,0) --> t111 (1,1,1)
         //                t112 (1,1,2)
         //                t121 (1,2,1)
         //                t122 (1,2,2)
         //                t211 (2,1,1)
         //                t212 (2,1,2)
         //                t221 (2,2,1)
         //                t222 (2,2,2)
         mat_t<T> mat_expect(27);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0


            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t111,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t112,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t121,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t122,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0


            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t211,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t212,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0

            ,   0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t221,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            ,t222,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0,  0,0,0, 0,0,0, 0,0,0
            }));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a cluster operator for an entire ModeCombiOpRange.
    ***************************************************************************/
   template<typename T>
   struct ClusterOperMcr
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 2, 2};
         MatRepVibOper<T> matrep(dims);

         // t^{m0,...}_{a^{m0},...} but I'll skip the excitation index here,
         // since there's only _one_ excited level for each mode in this case.
         // So the numbers here just refer to the mode combinations.
         // We'll exclude the {1} and {0,2} mode combination to make it less trivial.
         T ref = random::RandomNumber<T>();
         T t0  = random::RandomNumber<T>();
         T t2  = random::RandomNumber<T>();
         T t01 = random::RandomNumber<T>();
         T t12 = random::RandomNumber<T>();
         const std::vector<std::vector<T>> v_v_amps = {{ref}, {t0}, {t2}, {t01}, {t12}};
         ModeCombiOpRange mcr(2, 3);
         mcr.Erase(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{1},-1)
            ,  ModeCombi(std::set<In>{0,2},-1)
            });
         matrep.ClusterOper(mcr, v_v_amps);

         // Needs to shift
         //    (0,0,0) --> t0  (1,0,0)
         //    (0,0,1) --> t0  (1,0,1)
         //    (0,1,0) --> t0  (1,1,0)
         //    (0,1,1) --> t0  (1,1,1)
         //    (0,0,0) --> t2  (0,0,1)
         //    (0,1,0) --> t2  (0,1,1)
         //    (1,0,0) --> t2  (1,0,1)
         //    (1,1,0) --> t2  (1,1,1)
         //    (0,0,0) --> t01 (1,1,0)
         //    (0,0,1) --> t01 (1,1,1)
         //    (0,0,0) --> t12 (0,1,1)
         //    (1,0,0) --> t12 (1,1,1)
         mat_t<T> mat_expect(8);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            { ref,  0,    0,  0,      0,  0,    0,  0
            ,  t2,ref,    0,  0,      0,  0,    0,  0

            ,   0,  0,  ref,  0,      0,  0,    0,  0
            , t12,  0,   t2,ref,      0,  0,    0,  0


            ,  t0,  0,    0,  0,    ref,  0,    0,  0
            ,   0, t0,    0,  0,     t2,ref,    0,  0

            , t01,  0,   t0,  0,      0,  0,  ref,  0
            ,   0,t01,    0, t0,    t12,  0,   t2,ref
            }));

         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 1-mode (anti-Hermitian) \f$ \kappa \f$ operator.
    ***************************************************************************/
   template<typename T>
   struct KappaOper1
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {2, 3};
         MatRepVibOper<T> matrep(dims);
         const T k1 = random::RandomNumber<T>(); // kappa^{mode 1}_{1}
         const T k2 = random::RandomNumber<T>(); // kappa^{mode 1}_{2}
         const std::vector<T> v_kappa = {k1, k2};
         matrep.KappaOper(1, v_kappa);
         // The minus conjugated kappas.
         using midas::math::Conj;
         const T c1 = -Conj(k1);
         const T c2 = -Conj(k2);

         // Needs to shift
         //    (0,0) --> k1 (0,1), k2 (0,2)
         //    (0,1) --> c1 (0,0)
         //    (0,2) --> c2 (0,0)
         //
         //    (1,0) --> k1 (1,1), k2 (1,2)
         //    (1,1) --> c1 (1,0)
         //    (1,2) --> c2 (1,0)
         mat_t<T> mat_expect(6);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0,c1,c2,   0, 0, 0
            , k1, 0, 0,   0, 0, 0
            , k2, 0, 0,   0, 0, 0

            ,  0, 0, 0,   0,c1,c2
            ,  0, 0, 0,  k1, 0, 0
            ,  0, 0, 0,  k2, 0, 0
            }));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a 2-mode (anti-Hermitian) \f$ \kappa \f$ operator.
    ***************************************************************************/
   template<typename T>
   struct KappaOper2
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {3, 3};
         MatRepVibOper<T> matrep(dims);
         const T k01 = random::RandomNumber<T>(); // kappa^{mode 0}_{1}
         const T k02 = random::RandomNumber<T>(); // kappa^{mode 0}_{2}
         const T k11 = random::RandomNumber<T>(); // kappa^{mode 1}_{1}
         const T k12 = random::RandomNumber<T>(); // kappa^{mode 1}_{2}
         const std::vector<std::vector<T>> v_kappa_m = {{k01, k02}, {k11, k12}};
         matrep.KappaOper(std::set<Uin>{0,1}, v_kappa_m);
         // The minus conjugated kappas.
         using midas::math::Conj;
         const T c01 = -Conj(k01);
         const T c02 = -Conj(k02);
         const T c11 = -Conj(k11);
         const T c12 = -Conj(k12);

         // Needs to shift
         //    (0,0) --> k01 (1,0)
         //              k02 (2,0)
         //    (0,1) --> k01 (1,1)
         //              k02 (2,1)
         //    (0,2) --> k01 (1,2)
         //              k02 (2,2)
         //    (0,0) --> k11 (0,1)
         //              k12 (0,2)
         //    (1,0) --> k11 (1,1)
         //              k12 (1,2)
         //    (2,0) --> k11 (2,1)
         //              k12 (2,2)
         // and the other way for conjugated coefficients.
         mat_t<T> mat_expect(9);
         mat_expect.MatrixFromVector(vec_t<T>(std::vector<T>
            {  0,c11,c12,  c01,  0,  0,  c02,  0,  0
            ,k11,  0,  0,    0,c01,  0,    0,c02,  0
            ,k12,  0,  0,    0,  0,c01,    0,  0,c02

            ,k01,  0,  0,    0,c11,c12,    0,  0,  0
            ,  0,k01,  0,  k11,  0,  0,    0,  0,  0
            ,  0,  0,k01,  k12,  0,  0,    0,  0,  0

            ,k02,  0,  0,    0,  0,  0,    0,c11,c12
            ,  0,k02,  0,    0,  0,  0,  k11,  0,  0
            ,  0,  0,k02,    0,  0,  0,  k12,  0,  0
            }));
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Make a (anti-Hermitian) \f$ \kappa \f$ operator for all modes.
    ***************************************************************************/
   template<typename T>
   struct KappaOperAllModes
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {3, 4, 2};
         Uin full_dim = 3*4*2;
         std::vector<std::vector<T>> v_kappa_m;
         for(const auto& d: dims)
         {
            v_kappa_m.emplace_back(random::RandomStdVector<T>(d-1));
         }

         // Expected matrix as sum of individual kappa^m.
         mat_t<T> mat_expect(full_dim, T(0));
         MatRepVibOper<T> tmp_matrep(dims);
         Uin m = 0;
         for(const auto& kappa_m: v_kappa_m)
         {
            tmp_matrep.KappaOper(m, kappa_m);
            mat_expect += tmp_matrep.GetMatRep();
            ++m;
         }

         // Then calculate at once.
         MatRepVibOper<T> matrep(dims);
         matrep.KappaOper(v_kappa_m);

         // Assert.
         mat_t<T> diffmat = matrep.GetMatRep() - mat_expect;
         UNIT_ASSERT_EQUAL(diffmat.Norm(), absval_t<T>(0), "Non-zero diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check implementation of matrix exponential implementation for cluster
    *    operator.
    ***************************************************************************/
   template<typename T>
   struct ExpClusterOper
      :  public cutee::test
   {
      void run() override
      {
         // Setup random cluster operator.
         const std::vector<Uin> dims = {2, 3, 2, 4};
         MatRepVibOper<T> matrep(dims);

         // Lambda for calculating number of MC amplitudes.
         auto n_exci = [&dims](const ModeCombi& mc) -> Uin
            {
               Uin n = 1;
               for(const auto& m: mc.MCVec())
               {
                  n *= dims.at(m)-1;
               }
               return n;
            };

         // ModeCombiOpRange with empty MC removed. Container for storing
         // amplitudes for later use in MatRepVibOper<T>::ClusterOper(...).
         ModeCombiOpRange mcr(3, 4);
         mcr.Erase(std::vector<ModeCombi>{ModeCombi(0)});
         std::vector<std::vector<T>> v_amps;
         v_amps.reserve(mcr.Size());

         // Individual mode combi cluster operators are nil-potent at order 2,
         // so there exponential are exp(+-T_{mc}) = I +- T_{mc}. Make exp(T)
         // and exp(-T) as a product (cluster operators commute) of (I +
         // T_{mc}) over all mode combinations.
         const mat_t<T> I(matrep.FullDim(), vec_t<T>(matrep.FullDim(), T(1)));
         mat_t<T> ctrl_exp_pT(I);
         mat_t<T> ctrl_exp_mT(I);
         for(const auto& mc: mcr)
         {
            auto amps = random::RandomStdVector<T>(n_exci(mc));
            const auto& const_amps = amps;
            MatRepVibOper<T> tmp_matrep(dims);
            tmp_matrep.ClusterOper(midas::util::matrep::SetFromVec(mc.MCVec()), const_amps);
            const auto T_mc = tmp_matrep.GetMatRep();
            ctrl_exp_pT = ctrl_exp_pT*(I + T_mc);
            ctrl_exp_mT = ctrl_exp_mT*(I - T_mc);
            v_amps.emplace_back(std::move(amps));

            // A check that T_mc actually is nilpotent as expected.
            UNIT_ASSERT_EQUAL(mat_t<T>(T_mc * T_mc).Norm(), absval_t<T>(0), "T_mc not nilpotent.");
         }

         // Calculate exp(+-T) using MatRepVibOper method, uses entire T at once.
         matrep.ClusterOper(mcr, v_amps);
         const auto exp_pT = matrep.ExpClusterOper();
         const auto exp_mT = matrep.ExpClusterOper(-1);

         // Assertions.
         mat_t<T> diff_exp_pT = exp_pT - ctrl_exp_pT;
         mat_t<T> diff_exp_mT = exp_mT - ctrl_exp_mT;
         const absval_t<T> one(1);
         UNIT_ASSERT_FEQUAL_PREC(one+diff_exp_pT.Norm(), one, 20, "Non-zero diff.norm, exp(+T).");
         UNIT_ASSERT_FEQUAL_PREC(one+diff_exp_mT.Norm(), one, 20, "Non-zero diff.norm, exp(-T).");
      }
   };

   /************************************************************************//**
    * @brief
    *    Extract, from a full space vector, the parameters corresponding to
    *    excitations within some modes.
    ***************************************************************************/
   template<typename T>
   struct ExtractExciParams
      :  public cutee::test
   {
      void run() override
      {
         const std::vector<Uin> dims = {3,2,2};
         MatRepVibOper<T> matrep(dims);

         const T e_000 = random::RandomNumber<T>();
         const T e_001 = random::RandomNumber<T>();
         const T e_010 = random::RandomNumber<T>();
         const T e_011 = random::RandomNumber<T>();
         const T e_100 = random::RandomNumber<T>();
         const T e_101 = random::RandomNumber<T>();
         const T e_110 = random::RandomNumber<T>();
         const T e_111 = random::RandomNumber<T>();
         const T e_200 = random::RandomNumber<T>();
         const T e_201 = random::RandomNumber<T>();
         const T e_210 = random::RandomNumber<T>();
         const T e_211 = random::RandomNumber<T>();
         const vec_t<T> v_full(std::vector<T>
            {  e_000
            ,  e_001
            ,  e_010
            ,  e_011
            ,  e_100
            ,  e_101
            ,  e_110
            ,  e_111
            ,  e_200
            ,  e_201
            ,  e_210
            ,  e_211
            });

         const std::vector<T> v_mc_empty = matrep.ExtractExciParams(v_full, std::set<Uin>{});
         const std::vector<T> ctrl_mc_empty = {e_000};
         UNIT_ASSERT_EQUAL(v_mc_empty, ctrl_mc_empty, "MC {} vector mismatch.");

         const std::vector<T> v_mc_0 = matrep.ExtractExciParams(v_full, std::set<Uin>{0});
         const std::vector<T> ctrl_mc_0 = {e_100, e_200};
         UNIT_ASSERT_EQUAL(v_mc_0, ctrl_mc_0, "MC {0} vector mismatch.");

         const std::vector<T> v_mc_1_2 = matrep.ExtractExciParams(v_full, std::set<Uin>{1,2});
         const std::vector<T> ctrl_mc_1_2 = {e_011};
         UNIT_ASSERT_EQUAL(v_mc_1_2, ctrl_mc_1_2, "MC {1,2} vector mismatch.");

         const std::vector<T> v_mc_0_1_2 = matrep.ExtractExciParams(v_full, std::set<Uin>{0,1,2});
         const std::vector<T> ctrl_mc_0_1_2 = {e_111, e_211};
         UNIT_ASSERT_EQUAL(v_mc_0_1_2, ctrl_mc_0_1_2, "MC {0,1,2} vector mismatch.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check conversion of vector of full space parameters to one following a
    *    ModeCombiOpRange ordering.
    ***************************************************************************/
   template<typename T>
   struct ExtractToMcrSpace
      :  public cutee::test
   {
      void run() override
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{},-1)
            ,  ModeCombi(std::set<In>{0},-1)
            ,  ModeCombi(std::set<In>{1},-1)
            ,  ModeCombi(std::set<In>{0,1},-1)
            ,  ModeCombi(std::set<In>{1,2},-1)
            });
         const std::vector<Uin> dims = {2,2,3};
         const MatRepVibOper<T> matrep(dims);
         const T e_000 = random::RandomNumber<T>();
         const T e_001 = random::RandomNumber<T>();
         const T e_002 = random::RandomNumber<T>();
         const T e_010 = random::RandomNumber<T>();
         const T e_011 = random::RandomNumber<T>();
         const T e_012 = random::RandomNumber<T>();
         const T e_100 = random::RandomNumber<T>();
         const T e_101 = random::RandomNumber<T>();
         const T e_102 = random::RandomNumber<T>();
         const T e_110 = random::RandomNumber<T>();
         const T e_111 = random::RandomNumber<T>();
         const T e_112 = random::RandomNumber<T>();
         const vec_t<T> v_full(std::vector<T>
            {  e_000
            ,  e_001
            ,  e_002
            ,  e_010
            ,  e_011
            ,  e_012
            ,  e_100
            ,  e_101
            ,  e_102
            ,  e_110
            ,  e_111
            ,  e_112
            });

         const vec_t<T> v_ctrl(std::vector<T>
            {  e_000    // MC: {}
            ,  e_100    // MC: {0}
            ,  e_010    // MC: {1}
            ,  e_110    // MC: {0,1}
            ,  e_011    // MC: {1,2}
            ,  e_012    // (same)
            });

         vec_t<T> v_res = matrep.ExtractToMcrSpace(v_full, mcr);

         UNIT_ASSERT_EQUAL(v_res.Size(), v_ctrl.Size(), "Vector sizes unequal.");
         vec_t<T> diff = v_res - v_ctrl;
         UNIT_ASSERT_EQUAL(absval_t<T>(diff.Norm()), absval_t<T>(0), "Non-zero diff. norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check McrExciIndices and ExtractToMcrSpace are in correspondence, i.e.
    *    can be used to retrieve the same elements of a vector.
    *
    * For the McrExciIndices we do this the inefficient and awkward way of
    * creating a de-excitation operator (ShiftOper()) that de-excites
    * `(...) --> (0,...,0)`; then applies that to the full-space vector and
    * takes it 0'th element, i.e. the `(0,...,0)` element, which will be the
    * one that was at the desired excitation index before de-excitation.
    ***************************************************************************/
   template<typename T>
   struct McrExciIndicesAndExtractToMcrSpace
      :  public cutee::test
   {
      void run() override
      {
         using midas::util::matrep::McrExciIndices;
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{},-1)
            ,  ModeCombi(std::set<In>{0},-1)
            ,  ModeCombi(std::set<In>{1},-1)
            ,  ModeCombi(std::set<In>{0,1},-1)
            ,  ModeCombi(std::set<In>{1,2},-1)
            });
         const std::vector<Uin> dims = {2,2,3};
         MatRepVibOper<T> matrep(dims);
         const T e_000 = random::RandomNumber<T>();
         const T e_001 = random::RandomNumber<T>();
         const T e_002 = random::RandomNumber<T>();
         const T e_010 = random::RandomNumber<T>();
         const T e_011 = random::RandomNumber<T>();
         const T e_012 = random::RandomNumber<T>();
         const T e_100 = random::RandomNumber<T>();
         const T e_101 = random::RandomNumber<T>();
         const T e_102 = random::RandomNumber<T>();
         const T e_110 = random::RandomNumber<T>();
         const T e_111 = random::RandomNumber<T>();
         const T e_112 = random::RandomNumber<T>();
         const vec_t<T> v_full(std::vector<T>
            {  e_000
            ,  e_001
            ,  e_002
            ,  e_010
            ,  e_011
            ,  e_012
            ,  e_100
            ,  e_101
            ,  e_102
            ,  e_110
            ,  e_111
            ,  e_112
            });

         const vec_t<T> v_ctrl(std::vector<T>
            {  e_000    // MC: {}
            ,  e_100    // MC: {0}
            ,  e_010    // MC: {1}
            ,  e_110    // MC: {0,1}
            ,  e_011    // MC: {1,2}
            ,  e_012    // (same)
            });

         vec_t<T> v_res_a = matrep.ExtractToMcrSpace(v_full, mcr);

         const auto mcr_exci_indices = McrExciIndices(mcr, dims);
         vec_t<T> v_res_b;
         Uin i = 0;
         for(const auto& p_mc_exci_indices: mcr_exci_indices)
         {
            const auto& modes = p_mc_exci_indices.first;
            const auto& exci_indices = p_mc_exci_indices.second;
            v_res_b.SetNewSize(v_res_b.Size() + exci_indices.size());
            const std::vector<Uin> index_crea(modes.size(), 0);
            for(const auto& index_anni: exci_indices)
            {
               matrep.ShiftOper(modes, index_crea, index_anni);
               const mat_t<T> de_exc = matrep.GetMatRep();
               const vec_t<T> v_tmp = de_exc * v_full;
               v_res_b[i] = v_tmp[0];
               ++i;
            }
         }

         UNIT_ASSERT_EQUAL(v_res_a.Size(), v_ctrl.Size(), "Vector sizes unequal.");
         vec_t<T> diff_a = v_res_a - v_ctrl;
         UNIT_ASSERT_EQUAL(absval_t<T>(diff_a.Norm()), absval_t<T>(0), "Non-zero diff., a vs. ctrl.");

         UNIT_ASSERT_EQUAL(v_res_b.Size(), v_res_a.Size(), "Vector sizes unequal.");
         vec_t<T> diff_b = v_res_b - v_res_a;
         UNIT_ASSERT_EQUAL(absval_t<T>(diff_b.Norm()), absval_t<T>(0), "Non-zero diff., b vs. a.");
      }
   };

} /* namespace midas::test::matrep::viboper */

namespace midas::test::matrep::sparseclusteroper
{
   /************************************************************************//**
    * @brief
    *    Just some hardcoded stuff shared between the tests.
    ***************************************************************************/
   struct SparseClusterOperTestBase
      :  public cutee::test
   {
      std::vector<Uin> SimpleDims() const
      {
         return std::vector<Uin>{2,2,2};
      }

      ModeCombiOpRange SimpleMcr() const
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{}, -1)
            ,  ModeCombi(std::set<In>{0}, -1)
            ,  ModeCombi(std::set<In>{1}, -1)
            ,  ModeCombi(std::set<In>{0,1}, -1)
            ,  ModeCombi(std::set<In>{1,2}, -1)
            });
         return mcr;
      }

      std::vector<Uin> Dims() const
      {
         return std::vector<Uin>{2,4,3};
      }

      std::vector<ModeCombiOpRange> VecMcr() const
      {
         // Pretty standard.
         std::vector<ModeCombiOpRange> v;
         v.emplace_back(3, 3);

         // Standard but no {} MC.
         ModeCombiOpRange mcr_b(2, 3);
         mcr_b.Erase(std::vector<ModeCombi>{ModeCombi(0)});
         v.emplace_back(std::move(mcr_b));

         // Funky MCR.
         ModeCombiOpRange mcr_c;
         mcr_c.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{}, -1)
            ,  ModeCombi(std::set<In>{2}, -1)
            ,  ModeCombi(std::set<In>{0,1}, -1)
            ,  ModeCombi(std::set<In>{0,1,2}, -1)
            });
         v.emplace_back(std::move(mcr_c));

         return v;
      }
   };

   /************************************************************************//**
    * @brief
    *    Construct and check basic queries.
    ***************************************************************************/
   struct Constructor
      :  public SparseClusterOperTestBase
   {
      void run() override
      {
         const auto dims = this->Dims();
         auto v_mcr = this->VecMcr();
         const Uin d_full = midas::util::matrep::Product(dims);

         for(Uin i = 0; i < v_mcr.size(); ++i)
         {
            const std::string info = " (MCR i = "+std::to_string(i)+")";
            const Uin d_params = SetAddresses(v_mcr.at(i), dims);
            const auto& mcr = v_mcr.at(i);
            SparseClusterOper oper(dims, mcr);

            UNIT_ASSERT_EQUAL(oper.ContainsEmptyMC(), bool(mcr.NumEmptyMCs() > 0), "Wrong ContainsEmptyMC()."+info);
            UNIT_ASSERT_EQUAL(oper.NumModes(), dims.size(), "Wrong NumModes()."+info);
            UNIT_ASSERT_EQUAL(oper.FullDim(), d_full, "Wrong FullDim()."+info);
            UNIT_ASSERT_EQUAL(oper.NumAmpls(), d_params, "Wrong NumAmpls()."+info);

            std::vector<Uin> v_addr;
            for(const auto& mc: mcr)
            {
               v_addr.emplace_back(mc.Address());
            }
            v_addr.emplace_back(d_params);
            UNIT_ASSERT_EQUAL(oper.AmplMcAddresses(), v_addr, "Wrong AmplMcAddresses()."+info);

         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Construct and check indices for excitation transformation.
    ***************************************************************************/
   struct IndicesExc
      :  public SparseClusterOperTestBase
   {
      void run() override
      {
         using midas::util::matrep::Product;
         using index_t = SparseClusterOper::index_t;
         const auto dims = this->SimpleDims();
         const auto mcr = this->SimpleMcr();
         const SparseClusterOper oper(dims, mcr);

         const index_t ctrl =
            {  {{0,0}}
            ,  {{1,0}}
            ,  {{0,2}, {2,0}}
            ,  {{0,4}, {1,2}, {3,0}}
            ,  {{0,1}, {4,0}}
            ,  {{1,1}, {5,0}}
            ,  {{0,3}, {2,1}, {4,2}, {6,0}}
            ,  {{1,3}, {3,1}, {4,4}, {5,2}, {7,0}}
            };
         const Uin n_dense = 20;

         UNIT_ASSERT_EQUAL(oper.IndicesExc(), ctrl, "Wrong IndicesExc().");
         UNIT_ASSERT_EQUAL(oper.NumNonZeroElems(), n_dense, "Wrong NumNonZeroElems().");
         Uin d_full = Product(dims);
         Nb density = Nb(n_dense)/(d_full * d_full);
         Nb sparsity = 1. - density;
         UNIT_ASSERT_FEQUAL_PREC(oper.Density(), density, 0, "Wrong Density().");
         UNIT_ASSERT_FEQUAL_PREC(oper.Sparsity(), sparsity, 0, "Wrong Sparsity().");

         const std::map<Uin,std::vector<Uin>> ctrl_mpi_omp_ranges =
            {  {1, {0,8}}           // 1 rank : 20 dense elems.
            ,  {2, {0,6,8}}         // 2 ranks: 11, 9 dense elems.
            ,  {3, {0,4,7,8}}       // 3 ranks: 7, 8, 5 dense elems.
            ,  {4, {0,3,5,7,8}}     // 4 ranks: 4, 5, 6, 5 dense elems.
            ,  {5, {0,3,5,6,7,8}}   // 5 ranks: 4, 5, 2, 4, 5 dense elems.
            ,  {6, {0,3,4,6,7,8,8}} // 6 ranks: 4, 3, 4, 4, 5, 0 dense elems.
            };
         for(const auto& [n, ctrl_mpi_omp_range]: ctrl_mpi_omp_ranges)
         {
            const std::vector<Uin> mpi_omp_range = oper.ExampleMpiOmpRangesExc(n);
            UNIT_ASSERT_EQUAL(mpi_omp_range, ctrl_mpi_omp_range, "MPI/OMP range, n = "+std::to_string(n));
         }

         const Uin n_mpi_omp = midas::mpi::GlobalSize() * omp_get_max_threads();
         std::vector<Uin> ctrl_mpi_omp_range;
         try
         {
            ctrl_mpi_omp_range = ctrl_mpi_omp_ranges.at(n_mpi_omp);
         }
         catch(const std::out_of_range&)
         {
            ctrl_mpi_omp_range = oper.ExampleMpiOmpRangesExc(n_mpi_omp);
         }
         UNIT_ASSERT_EQUAL(oper.MpiOmpRangesExc(), ctrl_mpi_omp_range, "MPI/OMP range, max_threads * GlobalSize().");

      }
   };

   /************************************************************************//**
    * @brief
    *    Construct and check indices for deexcitation transformation.
    ***************************************************************************/
   struct IndicesDeExc
      :  public SparseClusterOperTestBase
   {
      void run() override
      {
         using midas::util::matrep::Product;
         using index_t = SparseClusterOper::index_t;
         const auto dims = this->SimpleDims();
         const auto mcr = this->SimpleMcr();
         const SparseClusterOper oper(dims, mcr);

         const index_t ctrl =
            {  {{0,0}, {2,2}, {3,4}, {4,1}, {6,3}}
            ,  {{1,0}, {3,2}, {5,1}, {7,3}}
            ,  {{2,0}, {6,1}}
            ,  {{3,0}, {7,1}}
            ,  {{4,0}, {6,2}, {7,4}}
            ,  {{5,0}, {7,2}}
            ,  {{6,0}}
            ,  {{7,0}}
            };
         const Uin n_dense = 20;

         UNIT_ASSERT_EQUAL(oper.IndicesDeExc(), ctrl, "Wrong IndicesDeExc().");
         UNIT_ASSERT_EQUAL(oper.NumNonZeroElems(), n_dense, "Wrong NumNonZeroElems().");
         Uin d_full = Product(dims);
         Nb density = Nb(n_dense)/(d_full * d_full);
         Nb sparsity = 1. - density;
         UNIT_ASSERT_FEQUAL_PREC(oper.Density(), density, 0, "Wrong Density().");
         UNIT_ASSERT_FEQUAL_PREC(oper.Sparsity(), sparsity, 0, "Wrong Sparsity().");

         const std::map<Uin,std::vector<Uin>> ctrl_mpi_omp_ranges =
            {  {1, {0,8}}           // 1 rank : 20 dense elems.
            ,  {2, {0,3,8}}         // 2 ranks: 11, 9 dense elems.
            ,  {3, {0,1,4,8}}       // 3 ranks: 5, 8, 7 dense elems.
            ,  {4, {0,1,3,5,8}}     // 4 ranks: 5, 6, 5, 4 dense elems.
            ,  {5, {0,1,2,4,5,8}}   // 5 ranks: 5, 4, 4, 3, 4 dense elems.
            ,  {6, {0,1,2,3,4,5,8}} // 6 ranks: 5, 4, 2, 2, 3, 4 dense elems.
            };
         for(const auto& [n, ctrl_mpi_omp_range]: ctrl_mpi_omp_ranges)
         {
            const std::vector<Uin> mpi_omp_range = oper.ExampleMpiOmpRangesDeExc(n);
            UNIT_ASSERT_EQUAL(mpi_omp_range, ctrl_mpi_omp_range, "MPI/OMP range, n = "+std::to_string(n));
         }

         const Uin n_mpi_omp = midas::mpi::GlobalSize() * omp_get_max_threads();
         std::vector<Uin> ctrl_mpi_omp_range;
         try
         {
            ctrl_mpi_omp_range = ctrl_mpi_omp_ranges.at(n_mpi_omp);
         }
         catch(const std::out_of_range&)
         {
            ctrl_mpi_omp_range = oper.ExampleMpiOmpRangesDeExc(n_mpi_omp);
         }
         UNIT_ASSERT_EQUAL(oper.MpiOmpRangesDeExc(), ctrl_mpi_omp_range, "MPI/OMP range, max_threads * GlobalSize().");

      }
   };

   /************************************************************************//**
    * @brief
    *    Transformations.
    ***************************************************************************/
   template
      <  typename T
      ,  bool DEEXC
      ,  bool CONJ
      >
   struct Transform
      :  public SparseClusterOperTestBase
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using vec_t = SparseClusterOper::vec_tmpl<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         const auto dims = this->Dims();
         auto v_mcr = this->VecMcr();
         const Uin d_full = midas::util::matrep::Product(dims);

         for(Uin i = 0; i < v_mcr.size(); ++i)
         {
            const std::string info = " (MCR i = "+std::to_string(i)+")";
            const Uin d_params = SetAddresses(v_mcr.at(i), dims);
            const auto& mcr = v_mcr.at(i);
            SparseClusterOper oper(dims, mcr);

            const vec_t v = random::RandomMidasVector<T>(oper.FullDim());
            const vec_t ampls = random::RandomMidasVector<T>(oper.NumAmpls());
            const auto v_v_ampls = midas::util::matrep::OrganizeMcrSpaceVec(dims, mcr, ampls);

            // Control.
            MatRepVibOper<T> mr(dims);
            mr.ClusterOper(mcr, v_v_ampls);
            mat_t mat_T = mr.GetMatRep();
            if constexpr(DEEXC)
            {
               mat_T.Transpose();
            }
            if constexpr(CONJ)
            {
               mat_T.ConjugateElems();
            }
            const vec_t ctrl = mat_T * v;

            // Assertions.
            const vec_t res = oper.Transform<DEEXC,CONJ>(ampls, v);
            MPISYNC_UNIT_ASSERT_EQUAL(res.Size(), ctrl.Size(), "Wrong Size()."+info);
            MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res,ctrl))/Norm(ctrl), absval_t(1), 1, "Non-zero rel.diff.norm."+info);
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    RefToFullSpace
    ***************************************************************************/
   template
      <  typename T
      ,  bool CONJ
      >
   struct RefToFullSpace
      :  public SparseClusterOperTestBase
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using vec_t = SparseClusterOper::vec_tmpl<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         const auto dims = this->Dims();
         auto v_mcr = this->VecMcr();
         const Uin d_full = midas::util::matrep::Product(dims);

         for(Uin i = 0; i < v_mcr.size(); ++i)
         {
            const std::string info = " (MCR i = "+std::to_string(i)+")";
            const Uin d_params = SetAddresses(v_mcr.at(i), dims);
            const auto& mcr = v_mcr.at(i);
            SparseClusterOper oper(dims, mcr);

            const vec_t ampls = random::RandomMidasVector<T>(oper.NumAmpls());
            const auto v_v_ampls = midas::util::matrep::OrganizeMcrSpaceVec(dims, mcr, ampls);

            // Control.
            MatRepVibOper<T> mr(dims);
            vec_t ref(mr.FullDim());
            ref[0] = 1;
            mr.ClusterOper(mcr, v_v_ampls);
            mat_t mat_T = mr.GetMatRep();
            if constexpr(CONJ)
            {
               mat_T.ConjugateElems();
            }
            const vec_t ctrl = mat_T * ref;

            // Assertions.
            const vec_t res = oper.RefToFullSpace<CONJ>(ampls);
            MPISYNC_UNIT_ASSERT_EQUAL(res.Size(), ctrl.Size(), "Wrong Size()."+info);
            MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res,ctrl))/Norm(ctrl), absval_t(1), 0, "Non-zero rel.diff.norm."+info);
         }
      }
   };

} /* namespace midas::test::matrep::sparseclusteroper */

namespace midas::test::matrep::opermat
{
   /************************************************************************//**
    * @brief
    *    Just some hardcoded stuff shared between the tests.
    ***************************************************************************/
   template<typename T>
   struct OperMatTestBase
      :  public cutee::test
   {
      using sym_t = typename midas::util::matrep::OperMat<T>::SymType;

      std::vector<Uin> NModals() const
      {
         return std::vector<Uin>{2,3,2};
      }

      std::pair<OpDef, ModalIntegrals<T>> Operator() const
      {
         std::vector<Uin> opers = {2,2,2};
         ModeCombiOpRange mcr(3, 3);
         const auto coefs = util::GetRandomParams<midas::type_traits::RealTypeT<T>>(mcr,opers,false,false,0);
         auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
         auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, this->NModals(), this->SymType());
         return std::make_pair(std::move(opdef), std::move(mod_ints));
      }

      ModeCombiOpRange Mcr() const
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{}, -1)
            ,  ModeCombi(std::set<In>{2}, -1)
            ,  ModeCombi(std::set<In>{0,1}, -1)
            ,  ModeCombi(std::set<In>{0,1,2}, -1)
            });
         return mcr;
      }

      OperMatTestBase(char aSymType = 'h')
         :  mSymType(aSymType)
      {}

      char SymType() const {return mSymType;}

      sym_t CtrlSymType()
      {
         switch(this->SymType())
         {
            default:  MIDASERROR(std::string("Unexpected SymType() = '")+this->SymType()+"'.");
            case 'h': return sym_t::HERMITIAN;
            case 'a': return sym_t::ANTIHERMITIAN;
            case 'g': return sym_t::GENERAL;
         }
      }

      Uin MatVecUlps(bool aTranspose) const
      {
         // Precision: In general the OperMat multiplication should give exact
         // same results as regular mat-vec. mult. However, for MPI the
         // TRANSPOSE = true _can_ give slight descrepancies because of the
         // MPI_Allreduce(MPI_SUM) changes the order of summations compared to
         // serial version.
         // Same can happen when using OMP or MKL.
         return 2;
      }

      void setup() override
      {
         midas::mpi::Barrier();
      }
      void teardown() override
      {
         midas::mpi::Barrier();
      }

      private:
         char mSymType = 'h';
   };

   /************************************************************************//**
    * @brief
    *    Check operator coefficients are equal across ranks. (Even though
    *    randomly generated.)
    ***************************************************************************/
   template<typename T>
   struct MpiSyncTestOpDef
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      MpiSyncTestOpDef(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
#ifdef VAR_MPI // Only do testing if running MPI.
         using namespace midas::test::mpi_sync;
         using v_coefs_t = GeneralMidasVector<Nb>;

         const auto oper = this->Operator();
         const OpDef& opdef = oper.first;

         // First extract coeficients.
         v_coefs_t v_own(opdef.Nterms());
         for(Uin i = 0; i < v_own.Size(); ++i)
         {
            v_own[i] = opdef.Coef(i);
         }

         // Broadcast master -> slaves and compare.
         v_coefs_t v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());
         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Wrong Size().");
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(v_own,v_master), absval_t<T>(0), 0, "Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * @brief
    *    Check modal integrals are equal across ranks. (Even though
    *    randomly generated.)
    ***************************************************************************/
   template<typename T>
   struct MpiSyncTestModalIntegrals
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      MpiSyncTestModalIntegrals(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
#ifdef VAR_MPI // Only do testing if running MPI.
         using namespace midas::test::mpi_sync;
         using vec_t = GeneralMidasVector<T>;

         const auto oper = this->Operator();
         const ModalIntegrals<T>& mi = oper.second;

         // Loop through all matrices.
         for(LocalModeNr m = 0; m < mi.NModes(); ++m)
         {
            vec_t v_own(mi.NModals(m)*mi.NModals(m));
            for(LocalOperNr o = 0; o < mi.NOper(m); ++o)
            {
               // Extract to vector.
               v_own.MatrixRowByRow(mi.GetIntegrals(m,o));

               // Broadcast master -> slaves and compare.
               vec_t v_master = v_own;
               v_master.MpiBcast(midas::mpi::MasterRank());

               std::stringstream ss;
               ss << ", (mode,oper) = (" << m << "," << o << ").";
               MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Wrong Size()"+ss.str());
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(v_own,v_master), absval_t<T>(0), 0, "Inequality"+ss.str());
            }
         }
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * @brief
    *    Construct and check basic queries.
    ***************************************************************************/
   template<typename T>
   struct Constructor
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      Constructor(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using midas::util::matrep::OperMat;
         const auto n_modals = this->NModals();
         const auto oper = this->Operator();

         const OperMat<T> mat(n_modals, oper.first, oper.second);

         const Uin full_dim = midas::util::matrep::Product(n_modals);
         UNIT_ASSERT_EQUAL(mat.GetSymType(), this->CtrlSymType(), "Wrong GetSymType().");
         UNIT_ASSERT_EQUAL(mat.FullDim(), full_dim, "Wrong FullDim().");
         UNIT_ASSERT_EQUAL(mat.Nrows(), full_dim, "Wrong Nrows().");
         UNIT_ASSERT_EQUAL(mat.Ncols(), full_dim, "Wrong Ncols().");
         UNIT_ASSERT(mat.IsSquare(), "Wrong IsSquare().");
      }
   };

   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T, bool TRANSPOSE>
   struct MatVecMultTest
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      MatVecMultTest(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::OperMat;
         using vec_t = GeneralMidasVector<T>;
         const auto n_modals = this->NModals();
         const auto oper = this->Operator();

         // Control.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const auto ctrl_mat = mr.GetMatRep();
         const vec_t v = random::RandomMidasVector<T>(mr.FullDim());
         vec_t ctrl;
         if constexpr(TRANSPOSE)
         {
            ctrl = vec_t(v * ctrl_mat);
         }
         else
         {
            ctrl = vec_t(ctrl_mat * v);
         }

         // Result.
         const OperMat<T> mat(n_modals, oper.first, oper.second);
         const vec_t res = mat.template MatVecMult<TRANSPOSE>(v);

         std::stringstream ss;
         ss << std::boolalpha << "Wrong MatVecMult<TRANSPOSE=" << TRANSPOSE << ">().";
         MPISYNC_UNIT_ASSERT_EQUAL(res.Size(), ctrl.Size(), "Size mismatch. "+ss.str());
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res,ctrl))/Norm(ctrl), absval_t<T>(1), this->MatVecUlps(TRANSPOSE), ss.str());

#ifdef VAR_MPI
         // Compare results across ranks.
         const std::vector<std::pair<std::string,vec_t>> v_vecs =
            {  {"v", v}
            ,  {"ctrl", ctrl}
            ,  {"res", res}
            };

         for(const auto& kv: v_vecs)
         {
            const vec_t& v_own = kv.second;
            vec_t v_master = v_own;
            v_master.MpiBcast(midas::mpi::MasterRank());
            MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Wrong Size(), vec = '"+kv.first+"'.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(v_own,v_master), absval_t<T>(0), 0, "Inequality, vec = '"+kv.first+"'.");
         }
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   struct OperatorAsterisk
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      OperatorAsterisk(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::OperMat;
         using vec_t = GeneralMidasVector<T>;
         const auto n_modals = this->NModals();
         const auto oper = this->Operator();

         // Setup.
         const OperMat<T> mat(n_modals, oper.first, oper.second);
         const vec_t v = random::RandomMidasVector<T>(mat.FullDim());

         // Controls.
         const vec_t ctrl_a = mat.template MatVecMult<false>(v);
         const vec_t ctrl_b = mat.template MatVecMult<true>(v);

         // Results.
         const vec_t res_a = mat * v;
         const vec_t res_b = v * mat;

         MPISYNC_UNIT_ASSERT_EQUAL(res_a.Size(), ctrl_a.Size(), "Size mismatch (res/ctrl a).");
         MPISYNC_UNIT_ASSERT_EQUAL(res_b.Size(), ctrl_b.Size(), "Size mismatch (res/ctrl b).");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res_a,ctrl_a))/Norm(ctrl_a), absval_t<T>(1), 0, "Wrong mat * v");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res_b,ctrl_b))/Norm(ctrl_b), absval_t<T>(1), 0, "Wrong v * mat");

#ifdef VAR_MPI
         // Compare results across ranks.
         const std::vector<std::pair<std::string,vec_t>> v_vecs =
            {  {"v", v}
            ,  {"ctrl_a", ctrl_a}
            ,  {"ctrl_b", ctrl_b}
            ,  {"res_a", res_a}
            ,  {"res_b", res_b}
            };

         for(const auto& kv: v_vecs)
         {
            const vec_t& v_own = kv.second;
            vec_t v_master = v_own;
            v_master.MpiBcast(midas::mpi::MasterRank());
            MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Wrong Size(), vec = '"+kv.first+"'.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(v_own,v_master), absval_t<T>(0), 0, "Inequality, vec = '"+kv.first+"'.");
         }
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   struct GetRowTest
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      GetRowTest(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::OperMat;
         using vec_t = GeneralMidasVector<T>;
         const auto n_modals = this->NModals();
         const auto oper = this->Operator();

         // For controls.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const auto ctrl_mat = mr.GetMatRep();

         // For results.
         const OperMat<T> mat(n_modals, oper.first, oper.second);

         MPISYNC_UNIT_ASSERT_EQUAL(mat.Nrows(), ctrl_mat.Nrows(), "Wrong Nrows().");
         MPISYNC_UNIT_ASSERT_EQUAL(mat.Ncols(), ctrl_mat.Ncols(), "Wrong Ncols().");

         vec_t ctrl_row(ctrl_mat.Ncols());
         vec_t row;
         for(Uin i = 0; i < mat.Nrows(); ++i)
         {
            std::stringstream ss;
            ss << ". i = " << i << ".";

            // Control.
            ctrl_mat.GetRow(ctrl_row, i);

            // Result.
            row = mat.GetRow(i);

            // Assert.
            MPISYNC_UNIT_ASSERT_EQUAL(row.Size(), ctrl_row.Size(), "Wrong row.Size()"+ss.str());
            MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(row,ctrl_row), absval_t<T>(0), 0, "Inequality"+ss.str());

#ifdef VAR_MPI
            // Compare results across ranks.
            const std::vector<std::pair<std::string,vec_t>> v_vecs =
               {  {"ctrl_row", ctrl_row}
               ,  {"row", row}
               };

            for(const auto& kv: v_vecs)
            {
               std::stringstream ss_mpi;
               ss_mpi << ss.str() << ", vec = '" << kv.first << "'. (MPI-sync.)";
               const vec_t& v_own = kv.second;
               vec_t v_master = v_own;
               v_master.MpiBcast(midas::mpi::MasterRank());
               MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Wrong Size()"+ss_mpi.str());
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(DiffNorm2(v_own,v_master), absval_t<T>(0), 0, "Inequality"+ss_mpi.str());
            }
#endif /* VAR_MPI */
         }
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct AddSumOneModeOpersTest
      :  public OperMatTestBase<T>
   {
      using typename OperMatTestBase<T>::sym_t;

      template<class... Args>
      AddSumOneModeOpersTest(char aSymOfAdded, Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
         ,  mSymOfAdded(aSymOfAdded)
      {}

      sym_t CtrlSymTypeAfterAdd()
      {
         // Convention for now is just that it converts to GENERAL.
         return sym_t::GENERAL;
      }

      void run() override
      {
         using midas::util::matrep::OperMat;
         using vec_t = GeneralMidasVector<T>;
         using mat_t = GeneralMidasMatrix<T>;

         const auto n_modals = this->NModals();
         const Uin full_dim = midas::util::matrep::Product(n_modals);
         const auto [opdef, modints] = this->Operator();
         const vec_t v_in = random::RandomMidasVector<T>(full_dim);
         const T coef = random::RandomNumber<T>();

         MatRepVibOper<T> mr(n_modals);
         std::vector<mat_t> v_add;
         mat_t m_add(full_dim, full_dim, T(0));
         for(Uin m = 0; m < n_modals.size(); ++m)
         {
            const auto& n = n_modals.at(m);
            switch(this->mSymOfAdded)
            {
               case 'h':
                  v_add.emplace_back(random::RandomHermitianMidasMatrix<T>(n));
                  break;
               case 'a':
                  v_add.emplace_back(random::RandomAntiHermitianMidasMatrix<T>(n));
                  break;
               case 'g':
                  v_add.emplace_back(random::RandomMidasMatrix<T>(n,n));
                  break;
               default:
                  MIDASERROR(std::string("Unexpected mSymOfAdded = '")+this->mSymOfAdded+"'.");
                  break;
            }

            mr.DirProdOneModeOpers(std::set<Uin>{m}, std::vector<mat_t>{v_add.back()});
            m_add += mr.GetMatRep();
         }

         // Control.
         mr.FullOperator(opdef, modints);
         mat_t ctrl_m = mr.GetMatRep();
         const vec_t ctrl_bef = vec_t(ctrl_m * v_in);
         const vec_t ctrl_bef_t = vec_t(v_in * ctrl_m);

         ctrl_m += mat_t(coef * m_add);
         const vec_t ctrl_aft = vec_t(ctrl_m * v_in);
         const vec_t ctrl_aft_t = vec_t(v_in * ctrl_m);

         // Result.
         OperMat<T> om(n_modals, opdef, modints);
         const vec_t res_bef = om * v_in;
         const vec_t res_bef_t = v_in * om;
         om.AddSumOneModeOpers(v_add, coef);
         const vec_t res_aft = om * v_in;
         const vec_t res_aft_t = v_in * om;

         auto rdn = [](const vec_t& v, const vec_t& r) -> absval_t<T>
         {
            return sqrt(DiffNorm2(v,r))/Norm(r);
         };
         UNIT_ASSERT_FZERO_PREC(rdn(res_bef,ctrl_bef), absval_t<T>(1), 1, "Inequality, bef.");
         UNIT_ASSERT_FZERO_PREC(rdn(res_bef_t,ctrl_bef_t), absval_t<T>(1), 1, "Inequality, bef_t.");
         UNIT_ASSERT_FZERO_PREC(rdn(res_aft,ctrl_aft), absval_t<T>(1), 1, "Inequality, aft.");
         UNIT_ASSERT_FZERO_PREC(rdn(res_aft_t,ctrl_aft_t), absval_t<T>(1), 1, "Inequality, aft_t.");
         UNIT_ASSERT_EQUAL(om.GetSymType(), this->CtrlSymTypeAfterAdd(), "Wrong GetSymType() after.");
      }

      private:
         char mSymOfAdded = 'h';
   };

   /************************************************************************//**
    * @brief
    *    Construct one, let it dump to disk, check that other one can read it
    *    in again. Handled behind the curtains, so we'll check as much as we
    *    can.
    ***************************************************************************/
   template<typename T>
   struct DumpAndReadFromDisk
      :  public OperMatTestBase<T>
   {
      template<class... Args>
      DumpAndReadFromDisk(Args&&... args)
         :  OperMatTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::OperMat;
         using midas::util::matrep::Product;
         using midas::filesystem::IsFile;
         using vec_t = GeneralMidasVector<T>;
         auto n_modals = this->NModals();
         const auto [init_opdef,init_modints] = this->Operator();
         auto opdef = init_opdef;
         auto modints = init_modints;
         opdef.SetName("opdef0");
         modints.SetName("modints0");
         const vec_t v = random::RandomMidasVector<T>(Product(n_modals));
         vec_t res_0;
         vec_t res_a;
         vec_t res_b;
         vec_t res_b_ctrl;
         vec_t res_c;
         vec_t res_c_ctrl;
         std::vector<std::string> v_no_dump_file_names;
         std::string last_dump_file_name;

         {
            OperMat<T> mat(n_modals, opdef, modints, true);
            last_dump_file_name = mat.DumpFileName();
            this->mTearDownFiles.emplace_back(mat.DumpFileName());
            res_0 = mat * v;
            // Destructor should dump to disk.
         }
         MPISYNC_UNIT_ASSERT(IsFile(last_dump_file_name+"_0"), last_dump_file_name +" wasn't dumped but should.");

         {
            // This should cause a read-in, 'cause we're calling with same arguments.
            OperMat<T> mat(n_modals, opdef, modints, true);
            last_dump_file_name = mat.DumpFileName();
            this->mTearDownFiles.emplace_back(mat.DumpFileName());
            res_a = mat * v;
         }
         MPISYNC_UNIT_ASSERT(IsFile(last_dump_file_name+"_0"), last_dump_file_name +" wasn't dumped but should.");

         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res_0,res_a)), res_0.Norm(), 0, "res_0, res_a not equal.");

         // Now delete some terms from operator.
         for(Uin i = 0; i < opdef.Nterms(); ++i)
         {
            opdef.DeleteOpTerm(i);
         }

         {
            // Still constructing with same objects, but opdef has changed, so
            // should construct anew.
            OperMat<T> mat(n_modals, opdef, modints, true);
            last_dump_file_name = mat.DumpFileName();
            this->mTearDownFiles.emplace_back(mat.DumpFileName());
            res_b = mat * v;

            // Identical opdef but changed name, so nothing to read in, thus
            // should definitely construct from scratch.
            auto opdef_b_ctrl = opdef;
            opdef_b_ctrl.SetName("opdef_b_ctrl");
            OperMat<T> mat_b_ctrl(n_modals, opdef_b_ctrl, modints);
            this->mTearDownFiles.emplace_back(mat_b_ctrl.DumpFileName());
            res_b_ctrl = mat_b_ctrl * v;
            v_no_dump_file_names.emplace_back(mat_b_ctrl.DumpFileName());
         }
         MPISYNC_UNIT_ASSERT(IsFile(last_dump_file_name+"_0"), last_dump_file_name +" wasn't dumped but should.");

         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res_b,res_b_ctrl)), res_b_ctrl.Norm(), 0, "res_b not equal to res_b_ctrl.");

         const absval_t<T> sqrt_eps = sqrt(std::numeric_limits<absval_t<T>>::epsilon());
         auto info = [sqrt_eps](const std::string& s, absval_t<T> dn2) -> std::string
         {
            std::stringstream ss;
            ss << std::scientific;
            ss << s << " (Must have read-in but shouldn't.) "
               << "rel.diff.norm = " << dn2
               << "> sqrt(eps) = " << sqrt_eps;
            return ss.str();
         };

         const absval_t<T> dn2_b = sqrt(DiffNorm2(res_0, res_b))/res_0.Norm();
         MPISYNC_UNIT_ASSERT(dn2_b > sqrt_eps, info("res_0, res_b too equal.", dn2_b));

         // Now modify modints. (T1 transform.)
         std::vector<vec_t> v_t1;
         for(LocalModeNr m = 0; m < modints.NModes(); ++m)
         {
            v_t1.emplace_back(random::RandomMidasVector<T>(modints.NModals(m)));
         }
         modints.T1Transform(v_t1);

         {
            // Still constructing with same objects, but modints has changed, so
            // should construct anew.
            OperMat<T> mat(n_modals, opdef, modints, true);
            last_dump_file_name = mat.DumpFileName();
            this->mTearDownFiles.emplace_back(mat.DumpFileName());
            res_c = mat * v;

            // Identical modints but changed name, so nothing to read in, thus
            // should definitely construct from scratch.
            auto modints_c_ctrl = modints;
            modints_c_ctrl.SetName("modints_c_ctrl");
            OperMat<T> mat_c_ctrl(n_modals, opdef, modints_c_ctrl);
            this->mTearDownFiles.emplace_back(mat_c_ctrl.DumpFileName());
            res_c_ctrl = mat_c_ctrl * v;
            v_no_dump_file_names.emplace_back(mat_c_ctrl.DumpFileName());
         }
         MPISYNC_UNIT_ASSERT(IsFile(last_dump_file_name+"_0"), last_dump_file_name +" wasn't dumped but should.");

         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res_c,res_c_ctrl)), res_c_ctrl.Norm(), 0, "res_c not equal to res_c_ctrl.");

         const absval_t<T> dn2_c = sqrt(DiffNorm2(res_b, res_c))/res_0.Norm();
         MPISYNC_UNIT_ASSERT(dn2_b > sqrt_eps, info("res_b, res_c too equal.", dn2_b));

         // Check the SetDumpToDisk(false) OperMat%s haven't dumped.
         for(const auto& f: v_no_dump_file_names)
         {
            MPISYNC_UNIT_ASSERT(!midas::filesystem::IsFile(f + "_0"), f+"_0 was dumped to disk, but shouldn't.");
         }
      }

      void teardown() override
      {
         midas::mpi::Barrier();
         std::string s;
         for(const auto& f: this->mTearDownFiles)
         {
            s = f + "_0";
            if (midas::filesystem::IsFile(s))
            {
               midas::filesystem::Remove(s);
            }
         }
         midas::mpi::Barrier();
      }

      private:
         //! For cleaning up dumped files at teardown.
         std::vector<std::string> mTearDownFiles;
   };

} /* namespace midas::test::matrep::opermat */

namespace midas::test::matrep::trf
{
   /************************************************************************//**
    * @brief
    *    Just some hardcoded stuff shared between the tests.
    ***************************************************************************/
   template<typename T>
   struct MatRepTransformersTestBase
      :  public cutee::test
   {
      static constexpr Uin max_ulps = 20;

      static std::vector<Uin> NModals()
      {
         return std::vector<Uin>{2,3,2};
      }

      static std::vector<Uin> NModalsExtended()
      {
         return std::vector<Uin>{5,4,4};
      }

      std::pair<OpDef, ModalIntegrals<T>> Operator
         (  const std::vector<Uin>& arNModals = NModals()
         )  const
      {
         std::vector<Uin> opers = {2,2,2};
         ModeCombiOpRange mcr(3, 3);
         const auto coefs = util::GetRandomParams<midas::type_traits::RealTypeT<T>>(mcr,opers,false,false,0);
         auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
         auto mod_ints = util::GetRandomModalIntegrals<T>(opdef, arNModals, this->mSymType);
         return std::make_pair(std::move(opdef), std::move(mod_ints));
      }

      ModeCombiOpRange Mcr() const
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{}, -1)
            ,  ModeCombi(std::set<In>{2}, -1)
            ,  ModeCombi(std::set<In>{0,1}, -1)
            ,  ModeCombi(std::set<In>{0,1,2}, -1)
            });
         return mcr;
      }

      std::vector<std::pair<std::set<Uin>,std::vector<Uin>>> ExciSpace() const
      {
         return std::vector<std::pair<std::set<Uin>,std::vector<Uin>>>
            {  {  {},      {}       }
            ,  {  {2},     {1}      }
            ,  {  {0,1},   {1,1}    }
            ,  {  {0,1},   {1,2}    }
            ,  {  {0,1,2}, {1,1,1}  }
            ,  {  {0,1,2}, {1,2,1}  }
            };
      }

      MatRepTransformersTestBase(char aSymType = 'h')
         :  mSymType(aSymType)
      {}

      private:
         char mSymType = 'h';
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::ShiftOperBraket.
    ***************************************************************************/
   template<typename T, bool CONJ_BRA>
   struct ShiftOperBraket
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using namespace midas::util::matrep;
         using midas::util::matrep::ShiftOperBraket;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using mc_t = std::set<Uin>;
         using modals_t = std::vector<Uin>;

         auto info = [](const mc_t& mc, const modals_t& a, const modals_t& b) -> std::string
         {
            std::stringstream ss;
            ss << "MC = " << mc << "; " << a << " -> " << b
               << " (CONJ_BRA = " << std::boolalpha << CONJ_BRA << ")";
            return ss.str();
         };

         const modals_t n_modals = {2,3,2};
         const ModeCombiOpRange mcr(n_modals.size(), n_modals.size());
         const Uin full_dim = Product(n_modals);

         const vec_t bra = random::RandomMidasVector<T>(full_dim);
         const vec_t ket = random::RandomMidasVector<T>(full_dim);

         MatRepVibOper<T> mr(n_modals);

         mc_t mc_set;
         modals_t sub_dims;
         std::vector<Uin> anni;
         std::vector<Uin> crea;
         T ctrl = 0;
         T res = 0;
         Uin real_ulps;
         [[maybe_unused]] Uin imag_ulps;
         #if defined(ARCH_ARM)
            MidasWarning("I have detected ARM architecture and used ARM specific reference data in MatRepVibOperTest: ShiftOperBraket. Is this correct?");
            real_ulps = 2;
            imag_ulps = 78;
         #else
            real_ulps = 2;
            imag_ulps = 2;
         #endif /* defined(ARCH_ARM) */

         for(const auto& mc: mcr)
         {
            mc_set = SetFromVec(mc.MCVec());
            sub_dims = SubsetDims(mc_set, n_modals);
            const Uin mc_dim = Product(sub_dims);
            anni.resize(mc_dim);
            crea.resize(mc_dim);
            for(Uin i = 0; i < mc_dim; ++i)
            {
               anni = MultiIndex(i, sub_dims);
               for(Uin j = 0; j < mc_dim; ++j)
               {
                  crea = MultiIndex(j, sub_dims);

                  // Control.
                  mr.ShiftOper(mc_set, crea, anni);
                  if constexpr(CONJ_BRA)
                  {
                     ctrl = Dot(bra, vec_t(mr.GetMatRep() * ket));
                  }
                  else
                  {
                     ctrl = SumProdElems(bra, vec_t(mr.GetMatRep() * ket));
                  }

                  // Result.
                  res = ShiftOperBraket<CONJ_BRA>(bra, ket, n_modals, mc_set, crea, anni);

                  // Assert.
                  if constexpr (midas::type_traits::IsComplexV<T>)
                  {
                     MPISYNC_UNIT_ASSERT_FEQUAL_PREC(res.real(), ctrl.real(), real_ulps, info(mc_set, anni, crea));
                     MPISYNC_UNIT_ASSERT_FEQUAL_PREC(res.imag(), ctrl.imag(), imag_ulps, info(mc_set, anni, crea));
                  }
                  else
                  {
                     MPISYNC_UNIT_ASSERT_FEQUAL_PREC(res, ctrl, real_ulps, info(mc_set, anni, crea));
                  }
               }
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfExpClusterOper.
    ***************************************************************************/
   template<typename T, bool DEEXC, bool CONJ>
   struct TrfExpClusterOper
      :  public MatRepTransformersTestBase<T>
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfExpClusterOper;

         const auto n_modals = this->NModals();
         const Uin n_modes = n_modals.size();
         const std::vector<std::pair<std::string,ModeCombiOpRange>> v_mcr =
            {  {"non-std. MCR", this->Mcr()}
            ,  {"MCR(3,"+std::to_string(n_modes)+")", ModeCombiOpRange(3,n_modes)}
            };
         std::vector<T> coefs = {T(1.), T(-1.), T(-.5)};
         // Add an imaginary part too the last one if complex T.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            coefs.back() += T(0., 2.);
         }

         for(const auto& p_mcr: v_mcr)
         {
            const auto& mcr = p_mcr.second;
            SparseClusterOper sparse_op(n_modals, mcr);
            vec_t v_t_amps = random::RandomMidasVector<T>(sparse_op.NumAmpls());
            if (mcr.NumEmptyMCs() > 0 && v_t_amps.Size() > 0)
            {
               v_t_amps[0] = 0;
            }
            const auto t_amps = midas::util::matrep::OrganizeMcrSpaceVec(n_modals, mcr, v_t_amps);

            MatRepVibOper<T> mr(n_modals);
            mr.ClusterOper(mcr, t_amps);
            const vec_t v = random::RandomMidasVector<T>(mr.FullDim());

            for(const auto& a: coefs)
            {
               // Ctrl.
               mat_t expT = mr.ExpClusterOper(a);
               if constexpr(DEEXC)
               {
                  expT.Transpose();
               }
               if constexpr(CONJ)
               {
                  expT.ConjugateElems();
               }
               const vec_t ctrl = expT * v;

               // Result.
               const vec_t res = TrfExpClusterOper<DEEXC,CONJ>(sparse_op, v_t_amps, v, a);

               // Assertions.
               std::stringstream ss;
               ss << " (" << p_mcr.first << ", a = " << a << ", DEEXC = " << std::boolalpha << DEEXC << ", CONJ = " << CONJ << ")";
               const std::string info = ss.str();
               MPISYNC_UNIT_ASSERT_EQUAL(ctrl.Size(), res.Size(), "Size mismatch."+info);
               MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res,ctrl))/Norm(ctrl), absval_t(1), 4, "Non-zero rel.diff.norm."+info);
            }
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfVccErrVec.
    ***************************************************************************/
   template<typename T>
   struct TrfVccErrVecTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfVccErrVecTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfVccErrVec;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = exp_mT * H * exp_pT * ref;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         const vec_t trf_res = TrfVccErrVec(n_modals, oper.first, oper.second, mcr, t_amps);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfVci.
    ***************************************************************************/
   template<typename T>
   struct TrfVciTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfVciTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfVci;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto c_coefs = util::GetRandomParams<T>(mcr, n_modals, false);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         mr.ClusterOper(mcr, c_coefs);
         const mat_t C = mr.GetMatRep();
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = H * C * ref;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         const vec_t trf_res = TrfVci(n_modals, oper.first, oper.second, mcr, c_coefs);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfVccEtaVec.
    ***************************************************************************/
   template<typename T>
   struct TrfVccEtaVecTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfVccEtaVecTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfVccEtaVec;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;
         const auto exci_space = this->ExciSpace();
         vec_t mr_res(exci_space.size(), T(0));
         Uin i = 0;
         for(const auto& exci: exci_space)
         {
            mr.ShiftOper(exci.first, exci.second, std::vector<Uin>(exci.second.size(),0));
            const mat_t tau_mu = mr.GetMatRep();
            mr_res[i] = Dot(ref, vec_t(exp_mT*(H*tau_mu - tau_mu*H)*exp_pT*ref));
            ++i;
         };

         // Transformer implementation.
         const vec_t trf_res = TrfVccEtaVec(n_modals, oper.first, oper.second, mcr, t_amps);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfVccRJac.
    ***************************************************************************/
   template<typename T>
   struct TrfVccRJacTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfVccRJacTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfVccRJac;
         using midas::util::matrep::TrfVccEtaVec;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);
         const auto r_coefs = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         mr.ClusterOper(mcr, r_coefs);
         const mat_t R = mr.GetMatRep();
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = exp_mT * (H*R - R*H) * exp_pT * ref;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         // The 'true' result zeroes the reference element explicitly.
         // The 'false' one has <ref|exp(-T)[H,R]exp(T)|ref> at index 0, which
         // is eta^T * R.
         vec_t trf_res_0 = TrfVccRJac(n_modals, oper.first, oper.second, mcr, t_amps, r_coefs, true);
         const vec_t trf_res   = TrfVccRJac(n_modals, oper.first, oper.second, mcr, t_amps, r_coefs, false);
         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");

         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res_0.Size(), "Size mismatch, trf_res_0.");
         MPISYNC_UNIT_ASSERT_EQUAL(trf_res_0[0], T(0), "Non-zero reference element in trf_res_0.");
         const vec_t eta = TrfVccEtaVec(n_modals, oper.first, oper.second, mcr, t_amps);
         T eta_R = 0;
         Uin i = 0;
         for(const auto& r_mc: r_coefs)
         {
            for(auto it =r_mc.begin(), end = r_mc.end(); it != end && i < eta.Size(); ++it, ++i)
            {
               eta_R += *it * eta[i];
            }
         }
         trf_res_0[0] = eta_R;
         const absval_t reldiffnorm_0 = vec_t(trf_res_0 - trf_res).Norm()/trf_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm_0, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1. (trf_res_0)");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfVccLJac.
    ***************************************************************************/
   template<typename T>
   struct TrfVccLJacTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfVccLJacTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfVccLJac;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);
         const auto l_coefs = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         mr.ClusterOper(mcr, l_coefs);
         const mat_t L = Transpose(mr.GetMatRep());
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;
         const auto exci_space = this->ExciSpace();
         vec_t mr_res(exci_space.size(), T(0));
         Uin i = 0;
         for(const auto& exci: exci_space)
         {
            mr.ShiftOper(exci.first, exci.second, std::vector<Uin>(exci.second.size(),0));
            const mat_t tau_mu = mr.GetMatRep();
            mr_res[i] = Dot(ref, vec_t(L*exp_mT*(H*tau_mu - tau_mu*H)*exp_pT*ref));
            ++i;
         };

         // Transformer implementation.
         const vec_t trf_res = TrfVccLJac(n_modals, oper.first, oper.second, mcr, t_amps, l_coefs);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::ExplVccJac.
    *
    * Tests two things overall:
    * i) That Jacobian calculating by not disregarding the empty ModeCombi has the
    * structure
    * ```
    *    0  |  eta
    *    ---+-----
    *    0  |  A
    * ```
    * where `A` is the usual Jacobian, with no indices for the empty ModeCombi.
    *
    * ii) That explicit matrix-vector multiplications with the Jacobian matrix
    * are consistent with the transformations by TrfVccRJac and TrfVccLJac.
    * Also, that TrfVccLJac is equivalent to both `l*A` and `A^T * l` (i.e. it
    * is _not_ `A^\dagger * l`.
    ***************************************************************************/
   template<typename T>
   struct ExplVccJacTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      ExplVccJacTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::ExplVccJac;
         using midas::util::matrep::TrfVccEtaVec;
         using midas::util::matrep::TrfVccRJac;
         using midas::util::matrep::TrfVccLJac;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Set up Jacobian with/without index for the empty ModeCombi.
         ModeCombiOpRange mcr_no_empty(mcr);
         mcr_no_empty.Erase(std::vector<ModeCombi>{ModeCombi()});
         const std::vector<std::vector<T>> t_amps_no_empty(t_amps.begin()+1, t_amps.end());

         const mat_t A_large = ExplVccJac(n_modals, oper.first, oper.second, mcr, t_amps, false);
         const mat_t A_small = ExplVccJac(n_modals, oper.first, oper.second, mcr, t_amps, true);
         const mat_t A_no_empty = ExplVccJac(n_modals, oper.first, oper.second, mcr_no_empty, t_amps_no_empty, true);

         // Assertions on structure of Jacobian matrix.
         MPISYNC_UNIT_ASSERT(A_large.IsSquare(), "Non-square A(large).");
         MPISYNC_UNIT_ASSERT(A_small.IsSquare(), "Non-square A(small).");
         MPISYNC_UNIT_ASSERT(A_no_empty.IsSquare(), "Non-square A(no empty MC).");
         MPISYNC_UNIT_ASSERT_EQUAL(A_small.Nrows(), A_no_empty.Nrows(), "Dimension mismatch; A(small), A(no empty).");
         MPISYNC_UNIT_ASSERT_EQUAL(A_large.Nrows(), A_no_empty.Nrows()+1, "Dimension mismatch; A(large), A(no empty).");

         const absval_t one = 1;
         absval_t reldiffnorm = mat_t(A_small - A_no_empty).Norm() / A_no_empty.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "rel.diff.norm. (A_small - A_no_empty)");

         mat_t A_large_block;
         A_large_block.AssignToSubMatrix(A_large, 1, A_small.Nrows(), 1, A_small.Ncols());
         reldiffnorm = mat_t(A_large_block - A_small).Norm() / A_small.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "rel.diff.norm. (A_large_block - A_small)");

         vec_t A_large_col_0(A_large.Nrows());
         A_large.GetCol(A_large_col_0, 0);
         MPISYNC_UNIT_ASSERT_FZERO_PREC(absval_t(A_large_col_0.Norm()), absval_t(1), 0, "Non-zero 0'th col.");

         vec_t A_large_row_0(A_large.Ncols());
         A_large.GetRow(A_large_row_0, 0);
         const vec_t eta = TrfVccEtaVec(n_modals, oper.first, oper.second, mcr, t_amps);
         reldiffnorm = vec_t(A_large_row_0 - eta).Norm() / eta.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, 2, "0'th row not eta.");


         // Assertions on results of TrfVccRJac/LJac.
         const auto rl_coefs = util::GetRandomParams<T>(mcr_no_empty, n_modals);
         std::vector<T> tmp_rl;
         for(const auto& rl_mc: rl_coefs)
         {
            for(const auto& rl_coef: rl_mc)
            {
               tmp_rl.emplace_back(rl_coef);
            }
         }
         const vec_t V(tmp_rl);

         const vec_t trf_r = TrfVccRJac(n_modals, oper.first, oper.second, mcr_no_empty, t_amps_no_empty, rl_coefs);
         const vec_t trf_l = TrfVccLJac(n_modals, oper.first, oper.second, mcr_no_empty, t_amps_no_empty, rl_coefs);
         const auto& A = A_no_empty;

         const vec_t AR = A*V;
         reldiffnorm = vec_t(AR - trf_r).Norm()/trf_r.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "|AR - trf_r|/|trf_r|");

         const vec_t LA = V*A;
         const vec_t ATL = Transpose(A)*V;
         reldiffnorm = vec_t(LA - trf_l).Norm()/trf_l.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "|LA - trf_l|/|trf_l|");
         reldiffnorm = vec_t(ATL - trf_l).Norm()/trf_l.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "|A^T L - trf_l|/|trf_l|");
      }
   };

   /************************************************************************//**
    * @brief
    *    Given some T-amplitudes, solves eta + l*A = 0, to find corresponding l
    *    coefficients.
    *
    * Since a randomly generated Jacobian may be ill-conditioned, this test
    * uses a hardcoded randomly generated Jacobian that has been verified to be
    * well-conditioned.
    ***************************************************************************/
   template<typename T>
   struct ExplVccJacSolveEtaPlusLATestBase
      :  public cutee::test
   {
      using param_t = T;
      using absval_t = midas::type_traits::RealTypeT<T>;
      using vec_t = typename MatRepVibOper<T>::vec_t;

      std::vector<Uin> NModals() const
      {
         return std::vector<Uin>{2,3,2};
      }
      ModeCombiOpRange Mcr() const
      {
         ModeCombiOpRange mcr;
         mcr.Insert(std::vector<ModeCombi>
            {  ModeCombi(std::set<In>{}, -1)
            ,  ModeCombi(std::set<In>{2}, -1)
            ,  ModeCombi(std::set<In>{0,1}, -1)
            ,  ModeCombi(std::set<In>{0,1,2}, -1)
            });
         return mcr;
      }

      virtual std::pair<OpDef,ModalIntegrals<param_t>> OperImpl() const = 0;
      virtual std::vector<std::vector<param_t>> TAmpsImpl() const = 0;


      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::ExplVccJacSolveEtaPlusLA;
         using midas::util::matrep::TrfVccEtaVec;
         using midas::util::matrep::TrfVccLJac;
         using midas::util::matrep::OrganizeMcrSpaceVec;

         const auto n_modals = NModals();
         const auto p_oper = this->OperImpl();
         const auto& oper = p_oper.first;
         auto mcr = Mcr();
         const auto& modints = p_oper.second;
         auto t_amps = TAmpsImpl();

         for(const auto& keep_empty_mc: std::vector<bool>{true, false})
         {
            auto s = std::string(" (")+(keep_empty_mc?"with {}":"without {}")+")";
            if (keep_empty_mc)
            {
               MPISYNC_UNIT_ASSERT_EQUAL(Uin(mcr.NumEmptyMCs()), Uin(1), "Mcr must contain MC {}."+s);
            }
            else
            {
               mcr.Erase(std::vector<ModeCombi>{ModeCombi()});
               t_amps.erase(t_amps.begin());
            }
            const auto p_l_success = ExplVccJacSolveEtaPlusLA(n_modals, oper, modints, mcr, t_amps);
            MPISYNC_UNIT_ASSERT(p_l_success.second, "Equation solver failed. Matrix should be well-conditioned!"+s);

            const auto l_coefs = OrganizeMcrSpaceVec(n_modals, mcr, p_l_success.first);
            const auto ctrl_eta = TrfVccEtaVec(n_modals, oper, modints, mcr, t_amps);
            const auto ctrl_lA = TrfVccLJac(n_modals, oper, modints, mcr, t_amps, l_coefs);
            MPISYNC_UNIT_ASSERT_EQUAL(ctrl_eta.Size(), ctrl_lA.Size(), "ctrl_eta, ctrl_lA, size mismatch."+s);
            MPISYNC_UNIT_ASSERT_EQUAL(ctrl_eta.Size(), p_l_success.first.Size(), "ctrl_eta, sol. l, size mismatch."+s);
            const absval_t diffnorm = vec_t(ctrl_eta + ctrl_lA).Norm()/(std::max(1.0,ctrl_lA.Norm()));

            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(absval_t(1)+diffnorm, absval_t(1), 4, "|eta+lA|/max(1,|lA|) != 0"+s);
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Real data for ExplVccJacSolveEtaPlusLATest.
    ***************************************************************************/
   template<typename T>
   struct ExplVccJacSolveEtaPlusLATest
      :  public ExplVccJacSolveEtaPlusLATestBase<T>
   {
      private:
         using param_t = T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         using mat_t = GeneralMidasMatrix<param_t>;
         using vec_t = GeneralMidasVector<param_t>;

         std::pair<OpDef,ModalIntegrals<param_t>> OperImpl() const override
         {
            ModeCombiOpRange mcr(3,3);
            std::vector<Uin> opers = {1,1,1};
            std::vector<std::vector<absval_t>> coefs =
               {  { 0.0000000000000000e+00}
               ,  { 9.9762030644286903e-01}
               ,  { 7.5103835802316632e-01}
               ,  { 4.5785048405124118e-01}
               ,  { 9.8665003757613023e-01}
               ,  { 5.8670199000898338e-01}
               ,  { 1.3946654022193194e-01}
               ,  {-5.8460179846375293e-01}
               };
            auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
            mat_t oper_m0_1(2,2);
            mat_t oper_m1_1(3,3);
            mat_t oper_m2_1(2,2);
            oper_m0_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  -7.4279179650889526E-01, -5.4480126862060307E-01
               ,  -5.4480126862060307E-01, -4.2201916580601107E-01
               }));
            oper_m1_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  -3.9523046216816415E-01, -1.6235378132256539E-01,  4.7904420399084779E-01
               ,  -1.6235378132256539E-01,  8.1084792777217185E-01,  1.6009625749664691E-02
               ,   4.7904420399084779E-01,  1.6009625749664691E-02,  6.7327865158982547E-01
               }));
            oper_m2_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  -9.0307989760839380E-01,  2.7247286151919359E-01
               ,   2.7247286151919359E-01, -7.3975292354350741E-01
               }));
            std::vector<std::vector<mat_t>> v_modints = 
               {  {mat_t(2,2,param_t(0.0)), oper_m0_1}
               ,  {mat_t(3,3,param_t(0.0)), oper_m1_1}
               ,  {mat_t(2,2,param_t(0.0)), oper_m2_1}
               };
            return std::make_pair(std::move(opdef), ModalIntegrals<param_t>(std::move(v_modints)));
         }

         std::vector<std::vector<param_t>> TAmpsImpl() const override
         {
            return std::vector<std::vector<param_t>>
               {  { 0.0000000000000000E+00}
               ,  { 8.3579703044048448E-01}
               ,  { 3.8228704765957944E-01,-7.1159349538190142E-02}
               ,  {-4.5525178160873514E-01,-9.6125154568842885E-01}
               };
         }
   };

   /************************************************************************//**
    * @brief
    *    Complex data for ExplVccJacSolveEtaPlusLATest.
    ***************************************************************************/
   template<typename T>
   struct ExplVccJacSolveEtaPlusLATest<std::complex<T>>
      :  public ExplVccJacSolveEtaPlusLATestBase<std::complex<T>>
   {
      private:
         using param_t = std::complex<T>;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         using mat_t = GeneralMidasMatrix<param_t>;
         using vec_t = GeneralMidasVector<param_t>;

         std::pair<OpDef,ModalIntegrals<param_t>> OperImpl() const override
         {
            ModeCombiOpRange mcr(3,3);
            std::vector<Uin> opers = {1,1,1};
            std::vector<std::vector<absval_t>> coefs =
               {  { 0.0000000000000000e+00}
               ,  { 9.4343838653141598E-01}
               ,  {-1.6156340277790171E-01}
               ,  { 2.5392338709298601E-01}
               ,  {-6.4510016251474456E-03}
               ,  { 9.5875278393708929E-01}
               ,  {-8.6200513873761642E-01}
               ,  { 6.4349744274362397E-01}
               };
            auto opdef = util::GetOpDefFromCoefs(mcr, opers, coefs);
            mat_t oper_m0_1(2,2);
            mat_t oper_m1_1(3,3);
            mat_t oper_m2_1(2,2);
            oper_m0_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  param_t(4.2347261109935874E-01,0.0000000000000000E+00), param_t(2.6927198219794590E-01,-7.0966562621986151E-01)
               ,  param_t(2.6927198219794590E-01,7.0966562621986151E-01), param_t(2.0969160488101002E-01,0.0000000000000000E+00)
               }));
            oper_m1_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  param_t(-2.1473204326227591E-01,0.0000000000000000E+00), param_t(-7.3241128859399152E-01,6.9143313860786915E-01), param_t(3.1789677481118050E-01,-8.4895676231220285E-01)
               ,  param_t(-7.3241128859399152E-01,-6.9143313860786915E-01), param_t(-9.4715248466170221E-01,0.0000000000000000E+00), param_t(-3.4067832523507025E-01,-4.1654019160580757E-03)
               ,  param_t(3.1789677481118050E-01,8.4895676231220285E-01), param_t(-3.4067832523507025E-01,4.1654019160580757E-03), param_t(3.5473343249287770E-01,0.0000000000000000E+00)
               }));
            oper_m2_1.MatrixFromVector(vec_t(std::vector<param_t>
               {  param_t(-5.9338779691452803E-01,0.0000000000000000E+00), param_t(-2.7242526022540992E-01,5.3817232873713894E-01)
               ,  param_t(-2.7242526022540992E-01,-5.3817232873713894E-01), param_t(-5.1818656211676695E-01,0.0000000000000000E+00)
               }));
            
            std::vector<std::vector<mat_t>> v_modints = 
               {  {mat_t(2,2,param_t(0.0)), oper_m0_1}
               ,  {mat_t(3,3,param_t(0.0)), oper_m1_1}
               ,  {mat_t(2,2,param_t(0.0)), oper_m2_1}
               };
            return std::make_pair(std::move(opdef), ModalIntegrals<param_t>(std::move(v_modints)));
         }

         std::vector<std::vector<param_t>> TAmpsImpl() const override
         {
            return std::vector<std::vector<param_t>>
               {  {  param_t(0.0000000000000000E+00,0.0000000000000000E+00)}
               ,  {  param_t(7.1937345148049614E-01,5.4347373953635669E-01)}
               ,  {  param_t(5.5641808443619367E-01,-8.1441745995879578E-01), param_t(-7.4549369182099912E-01,-7.7490709483970588E-01)}
               ,  {  param_t(5.4528579477809513E-01,-2.0511163380116026E-01), param_t(-9.1441679045427982E-01,2.3785639163988104E-01)}
               };
         }
   };

   /************************************************************************//**
    * @brief
    *    Check utility function TensorDirectProduct.
    ***************************************************************************/
   template<typename T>
   struct TensorDirectProductTest
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::Product;
         using midas::util::matrep::SubsetDims;
         using midas::util::matrep::TensorDirectProduct;

         using dims_t = std::vector<Uin>;
         using vec_t = GeneralMidasVector<T>;
         using mc_t = std::set<Uin>;

         const mc_t mc = {2,3,5};
         const dims_t dims = {4,4,3,2,4,2,4};
         const std::vector<mc_t> sub_mcs = {mc_t{3}, mc_t{2,5}};
         const auto res_size = Product(SubsetDims(mc, dims));
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(res_size), Uin(12), "Wrong result size.");

         const auto t3  = random::RandomMidasVector<T>(Product(SubsetDims(sub_mcs.at(0), dims)));
         const auto t25 = random::RandomMidasVector<T>(Product(SubsetDims(sub_mcs.at(1), dims)));
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(t3.Size()),  Uin(2), "t3, wrong size.");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(t25.Size()), Uin(6), "t25, wrong size.");

         const vec_t ctrl(std::vector<T>
            {  t3[0] * t25[0*2 + 0] // 000 --> 0, 00
            ,  t3[0] * t25[0*2 + 1] // 001 --> 0, 01
            ,  t3[1] * t25[0*2 + 0] // 010 --> 1, 00
            ,  t3[1] * t25[0*2 + 1] // 011 --> 1, 01
            ,  t3[0] * t25[1*2 + 0] // 100 --> 0, 10
            ,  t3[0] * t25[1*2 + 1] // 101 --> 0, 11
            ,  t3[1] * t25[1*2 + 0] // 110 --> 1, 10
            ,  t3[1] * t25[1*2 + 1] // 111 --> 1, 11
            ,  t3[0] * t25[2*2 + 0] // 200 --> 0, 20
            ,  t3[0] * t25[2*2 + 1] // 201 --> 0, 21
            ,  t3[1] * t25[2*2 + 0] // 210 --> 1, 20
            ,  t3[1] * t25[2*2 + 1] // 211 --> 1, 21
            });

         const auto p_res_mc = TensorDirectProduct
            (  dims
            ,  sub_mcs
            ,  std::vector<vec_t>{t3, t25}
            );

         MPISYNC_UNIT_ASSERT(p_res_mc.second == mc, "Wrong result mode combination.");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(p_res_mc.first.Size()), Uin(res_size), "Wrong result size.");
         const auto dn = vec_t(p_res_mc.first - ctrl).Norm();
         MPISYNC_UNIT_ASSERT_FZERO_PREC(dn, decltype(dn)(1.), 1, "Wrong diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check C|ref>.
    ***************************************************************************/
   template<typename T>
   struct CRefTest
      :  public MatRepTransformersTestBase<T>
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::CRefFullSpace;

         const auto n_modals = this->NModals();
         const auto mcr = this->Mcr();
         const auto c_coefs = util::GetRandomParams<T>(mcr, n_modals);

         MatRepVibOper<T> mr(n_modals);
         mr.ClusterOper(mcr, c_coefs);
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;
         const vec_t ctrl = mr.GetMatRep() * ref;

         const vec_t result = CRefFullSpace(n_modals, mcr, c_coefs);

         MPISYNC_UNIT_ASSERT_EQUAL(result.Size(), ctrl.Size(), "Wrong size.");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(result,ctrl)), absval_t(1), 0, "Wrong diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check Exp(T), i.e. VCC -> VCI.
    ***************************************************************************/
   template<typename T>
   struct ExpTRefTest
      :  public cutee::test
   {
      ExpTRefTest
         (  std::vector<Uin> aNModals
         ,  Uin aMaxExciInp
         ,  Uin aMaxExciOut
         )
         :  mNModals(std::move(aNModals))
         ,  mMaxExciInp(aMaxExciInp)
         ,  mMaxExciOut(aMaxExciOut)
      {
      }

      const std::vector<Uin> mNModals;
      const Uin mMaxExciInp;
      const Uin mMaxExciOut;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using midas::util::matrep::ExpTRef;

         const auto& n_modals = this->mNModals;
         const auto& max_inp = this->mMaxExciInp;
         const auto& max_out = this->mMaxExciOut;
         ModeCombiOpRange mcr_input(max_inp,n_modals.size());
         SetAddresses(mcr_input, n_modals);
         const auto std_t_amps = util::GetRandomParams<T>(mcr_input, n_modals);
         GeneralMidasVector<T> mv_t_amps;
         for(const auto& t_mc: std_t_amps)
         {
            mv_t_amps.Insert(mv_t_amps.Size(), t_mc.begin(), t_mc.end());
         }

         MatRepVibOper<T> mr(n_modals);
         mr.ClusterOper(mcr_input, std_t_amps);
         const auto mr_expT = mr.ExpClusterOper(+1);
         GeneralMidasVector<T> ref(mr.FullDim());
         ref[0] = T(1);
         const GeneralMidasVector<T> ctrl_full_space = mr_expT * ref;

         ModeCombiOpRange mcr_result(max_out, n_modals.size());
         SetAddresses(mcr_result, n_modals);
         const GeneralMidasVector<T> ctrl = mr.ExtractToMcrSpace(ctrl_full_space, mcr_result);

         const GeneralMidasVector<T> result = ExpTRef(n_modals, mcr_input, mv_t_amps, max_out);
         MPISYNC_UNIT_ASSERT_EQUAL(result.Size(), ctrl.Size(), "Wrong sizes.");

         const absval_t<T> diffnorm = sqrt(DiffNorm2(result, ctrl));
         MPISYNC_UNIT_ASSERT_FZERO_PREC(diffnorm, absval_t<T>(1), 12, "Wrong diff.norm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Check inverse `exp(T)|ref>`, i.e. given `(1 + C)|ref>` find `T` so that
    *    `exp(T)|ref> = (1 + C)|ref>` within the ModeCombiOpRange-space.
    ***************************************************************************/
   template<typename T>
   struct InvExpTRefTest
      :  public cutee::test
   {
      InvExpTRefTest
         (  ModeCombiOpRange aMcr
         )
         :  mMcr(std::move(aMcr))
      {
         SetAddresses(mMcr, mNModals);
      }

      std::vector<Uin> mNModals = {3,5,4};
      ModeCombiOpRange mMcr;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using vec_t = GeneralMidasVector<T>;
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::InvExpTRef;
         using midas::util::matrep::SparseClusterOper;
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::McrOrganizedToStackedVec;

         const auto& n_modals = this->mNModals;
         const auto& mcr = this->mMcr;
         SparseClusterOper clust_oper(n_modals, mcr);

         const vec_t ctrl_t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(util::GetRandomParams<T>(mcr, n_modals));
         vec_t v(clust_oper.FullDim(), T(0));
         v[0] = T(1);
         v = TrfExpClusterOper<false,false>(clust_oper, ctrl_t_ampls, std::move(v), T(1));
         const vec_t c_coefs = MatRepVibOper<T>::ExtractToMcrSpace(v, mcr, n_modals);

         const vec_t t_ampls = InvExpTRef(n_modals, mcr, c_coefs);

         MPISYNC_UNIT_ASSERT_EQUAL(t_ampls.Size(), ctrl_t_ampls.Size(), "Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(t_ampls,ctrl_t_ampls)), ctrl_t_ampls.Norm(), 4, "Inequality.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::Ref1pLExpmT.
    ***************************************************************************/
   template<typename T>
   struct Ref1pLExpmTTest
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using midas::util::matrep::Ref1pLExpmT;

         const std::vector<Uin> n_modals = {2,2};
         const ModeCombiOpRange mcr(2,2);
         const auto t_ampls = util::GetRandomParams<T>(mcr, n_modals);
         const auto l_coefs = util::GetRandomParams<T>(mcr, n_modals);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(t_ampls.size()), Uin(4), "Wrong t_ampls size.");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(l_coefs.size()), Uin(4), "Wrong l_coefs size.");
         for(Uin i = 0; i < t_ampls.size(); ++i)
         {
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(t_ampls.at(i).size()), Uin(1), "Wrong t_ampls subsize.");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(l_coefs.at(i).size()), Uin(1), "Wrong l_coefs subsize.");
         }
         MPISYNC_UNIT_ASSERT_EQUAL(T(t_ampls.at(0).at(0)), T(0), "Wrong t_ampls.at(0).at(0).");
         MPISYNC_UNIT_ASSERT_EQUAL(T(l_coefs.at(0).at(0)), T(0), "Wrong l_coefs.at(0).at(0).");
         const auto t0  = t_ampls.at(1).at(0);
         const auto t1  = t_ampls.at(2).at(0);
         const auto t01 = t_ampls.at(3).at(0);
         const auto l0  = l_coefs.at(1).at(0);
         const auto l1  = l_coefs.at(2).at(0);
         const auto l01 = l_coefs.at(3).at(0);

         const GeneralMidasVector<T> ctrl(std::vector<T>
            {  T(1) + (t0*t1 - t01)*l01 - t0*l0 - t1*l1
            ,  l0 - t1*l01
            ,  l1 - t0*l01
            ,  l01
            });

         const GeneralMidasVector<T> mv_t(std::vector<T>{0,t0,t1,t01});
         const GeneralMidasVector<T> mv_l(std::vector<T>{0,l0,l1,l01});
         const GeneralMidasVector<T> result = Ref1pLExpmT(n_modals, mcr, mv_t, mv_l, 2);

         MPISYNC_UNIT_ASSERT_EQUAL(result.Size(), ctrl.Size(), "Size mismatch.");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(result,ctrl)), absval_t(1), 5, "Wrong diffnorm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::RefExpSExpmT.
    ***************************************************************************/
   template<typename T>
   struct RefExpSExpmTTest
      :  public cutee::test
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using midas::util::matrep::RefExpSExpmT;

         const std::vector<Uin> n_modals = {2,2};
         const ModeCombiOpRange mcr(2,2);
         const auto t_ampls = util::GetRandomParams<T>(mcr, n_modals);
         const auto s_ampls = util::GetRandomParams<T>(mcr, n_modals);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(t_ampls.size()), Uin(4), "Wrong t_ampls size.");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(s_ampls.size()), Uin(4), "Wrong s_ampls size.");
         for(Uin i = 0; i < t_ampls.size(); ++i)
         {
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(t_ampls.at(i).size()), Uin(1), "Wrong t_ampls subsize.");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(s_ampls.at(i).size()), Uin(1), "Wrong s_ampls subsize.");
         }
         MPISYNC_UNIT_ASSERT_EQUAL(T(t_ampls.at(0).at(0)), T(0), "Wrong t_ampls.at(0).at(0).");
         MPISYNC_UNIT_ASSERT_EQUAL(T(s_ampls.at(0).at(0)), T(0), "Wrong s_ampls.at(0).at(0).");
         const auto t0  = t_ampls.at(1).at(0);
         const auto t1  = t_ampls.at(2).at(0);
         const auto t01 = t_ampls.at(3).at(0);
         const auto s0  = s_ampls.at(1).at(0);
         const auto s1  = s_ampls.at(2).at(0);
         const auto s01 = s_ampls.at(3).at(0);

         const GeneralMidasVector<T> ctrl(std::vector<T>
            {  T(1) + (t0*t1 - t01)*(s0*s1 + s01) - t0*s0 - t1*s1
            ,  s0 - t1*(s0*s1 + s01)
            ,  s1 - t0*(s0*s1 + s01)
            ,  s0*s1 + s01
            });

         const GeneralMidasVector<T> mv_t(std::vector<T>{0,t0,t1,t01});
         const GeneralMidasVector<T> mv_l(std::vector<T>{0,s0,s1,s01});
         const GeneralMidasVector<T> result = RefExpSExpmT(n_modals, mcr, mv_t, mv_l, 2);

         MPISYNC_UNIT_ASSERT_EQUAL(result.Size(), ctrl.Size(), "Size mismatch.");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(result,ctrl)), absval_t(1), 5, "Wrong diffnorm.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfExtVccHamDerExtAmp.
    ***************************************************************************/
   template<typename T>
   struct TrfExtVccHamDerExtAmpTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfExtVccHamDerExtAmpTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfExtVccHamDerExtAmp;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);
         const auto s_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         // Hamiltonian.
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         // T and exp(+-T)
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         // S and exp(S)
         // S = sum_mu s_mu tau_mu^dagger
         // Call S' = ClusterOper(s_mu) = sum_mu s_mu tau_mu
         // Then S = S'^T and exp(S) = exp(S'^T) = (exp(S'))^T
         mr.ClusterOper(mcr, s_amps);
         const mat_t exp_pS = Transpose(mr.ExpClusterOper(+1));
         // Reference ket.
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = exp_pS * exp_mT * H * exp_pT * ref;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         const vec_t trf_res = TrfExtVccHamDerExtAmp(n_modals, oper.first, oper.second, mcr, t_amps, s_amps);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfExtVccHamDerClustAmp.
    ***************************************************************************/
   template<typename T>
   struct TrfExtVccHamDerClustAmpTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TrfExtVccHamDerClustAmpTest(Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
      {}

      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfExtVccHamDerClustAmp;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         const auto mcr = this->Mcr();
         const auto t_amps = util::GetRandomParams<T>(mcr, n_modals);
         const auto s_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         MatRepVibOper<T> mr(n_modals);
         // Hamiltonian.
         mr.FullOperator(oper.first, oper.second);
         const mat_t H = mr.GetMatRep();
         // T and exp(+-T)
         mr.ClusterOper(mcr, t_amps);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         // S and exp(S)
         // S = sum_mu s_mu tau_mu^dagger
         // Call S' = ClusterOper(s_mu) = sum_mu s_mu tau_mu
         // Then S = S'^T and exp(S) = exp(S'^T) = (exp(S'))^T
         mr.ClusterOper(mcr, s_amps);
         const mat_t exp_pS = Transpose(mr.ExpClusterOper(+1));
         // Reference ket.
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const auto exci_space = this->ExciSpace();
         vec_t mr_res(exci_space.size(), T(0));
         Uin i = 0;
         for(const auto& exci: exci_space)
         {
            mr.ShiftOper(exci.first, exci.second, std::vector<Uin>(exci.second.size(),0));
            const mat_t tau_mu = mr.GetMatRep();
            mr_res[i] = Dot(ref, vec_t(exp_pS*exp_mT*(H*tau_mu - tau_mu*H)*exp_pT*ref));
            ++i;
         };

         // Transformer implementation.
         const vec_t trf_res = TrfExtVccHamDerClustAmp(n_modals, oper.first, oper.second, mcr, t_amps, s_amps);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfExtVccLeftExpmS.
    ***************************************************************************/
   template<typename T>
   struct TrfExtVccLeftExpmSTest
      :  public MatRepTransformersTestBase<T>
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfExtVccLeftExpmS;
         using midas::util::matrep::McrOrganizedToStackedVec;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         auto mcr = this->Mcr();
         const auto tot_size = SetAddresses(mcr, n_modals);
         const auto params = util::GetRandomParams<T>(mcr, n_modals);
         const auto s_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         // It amounts to exp(-S) P |ref>, where P is a cluster operator based
         // on the parameters.
         MatRepVibOper<T> mr(n_modals);
         // S and exp(S)
         // S = sum_mu s_mu tau_mu^dagger
         // Call S' = ClusterOper(s_mu) = sum_mu s_mu tau_mu
         // Then S = S'^T and exp(S) = exp(S'^T) = (exp(S'))^T
         mr.ClusterOper(mcr, s_amps);
         const mat_t exp_mS = Transpose(mr.ExpClusterOper(-1));
         // P
         mr.ClusterOper(mcr, params);
         const mat_t P = mr.GetMatRep();
         // Reference ket.
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = exp_mS * P * ref;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         vec_t vec_cont;
         util::ConstructParamsVecCont(vec_cont, mcr, tot_size, params);
         SparseClusterOper clust_oper(n_modals, mcr);
         const vec_t trf_res = TrfExtVccLeftExpmS(clust_oper, McrOrganizedToStackedVec<T,GeneralMidasVector>(s_amps), vec_cont);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TrfExtVccRightExpmS.
    ***************************************************************************/
   template<typename T>
   struct TrfExtVccRightExpmSTest
      :  public MatRepTransformersTestBase<T>
   {
      void run() override
      {
         using namespace midas::test::mpi_sync;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = typename MatRepVibOper<T>::mat_t;
         using vec_t = typename MatRepVibOper<T>::vec_t;
         using midas::util::matrep::TrfExtVccRightExpmS;
         using midas::util::matrep::McrOrganizedToStackedVec;

         const auto n_modals = this->NModals();
         const auto oper = this->Operator();
         auto mcr = this->Mcr();
         const auto tot_size = SetAddresses(mcr, n_modals);
         const auto params = util::GetRandomParams<T>(mcr, n_modals);
         const auto s_amps = util::GetRandomParams<T>(mcr, n_modals);

         // Explicit MatRepVibOper calculation.
         // It amounts to <ref| P exp(-S), where P is a cluster deexc-operator based
         // on the parameters. P = p_mu tau_mu^dagger
         MatRepVibOper<T> mr(n_modals);
         // S and exp(S)
         // S = sum_mu s_mu tau_mu^dagger
         // Call S' = ClusterOper(s_mu) = sum_mu s_mu tau_mu
         // Then S = S'^T and exp(S) = exp(S'^T) = (exp(S'))^T
         mr.ClusterOper(mcr, s_amps);
         const mat_t exp_mS = Transpose(mr.ExpClusterOper(-1));
         // P
         mr.ClusterOper(mcr, params);
         const mat_t P = Transpose(mr.GetMatRep());
         // Reference ket.
         vec_t ref(mr.FullDim(), T(0));
         ref[0] = 1;

         const vec_t mr_res_full = ref * P * exp_mS;
         const vec_t mr_res = mr.ExtractToMcrSpace(mr_res_full, mcr);

         // Transformer implementation.
         vec_t vec_cont;
         util::ConstructParamsVecCont(vec_cont, mcr, tot_size, params);
         SparseClusterOper clust_oper(n_modals, mcr);
         const vec_t trf_res = TrfExtVccRightExpmS(clust_oper, McrOrganizedToStackedVec<T,GeneralMidasVector>(s_amps), vec_cont);

         // Assertion.
         MPISYNC_UNIT_ASSERT_EQUAL(mr_res.Size(), trf_res.Size(), "Size mismatch.");
         const absval_t one = 1;
         const absval_t reldiffnorm = vec_t(trf_res - mr_res).Norm()/mr_res.Norm();
         MPISYNC_UNIT_ASSERT_FEQUAL_PREC(one + reldiffnorm, one, this->max_ulps, "1 + |res-ctrl|/|ctrl| != 1.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::test::util::GetRandomModalTransMats.
    ***************************************************************************/
   template<typename T>
   struct GetRandomModalTransMatsTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      GetRandomModalTransMatsTest(bool aBalance, bool aIncludeZeroes, Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
         ,  mBalance(aBalance)
         ,  mIncludeZeroes(aIncludeZeroes)
      {}

      void run() override
      {
         //using namespace midas::util::matrep;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = GeneralMidasMatrix<T>;
         using vec_t = GeneralMidasVector<T>;

         // Dimensions of system.
         const auto n_modals_td = this->NModals();
         const auto n_modals_prim = this->NModalsExtended();

         // For modal transformations;
         // for each mode m a pair {U^m,W^m}, where
         // U^m transforms primitive creation operators -> TD. ones
         // W^m transforms primitive annihilation operators -> TD. ones
         // In theory dimensions are
         //    U^m: N(ext)_m x N(td)_m    (tall)
         //    W^m: N(td)_m  x N(ext)_m   (wide)
         // but to avoid changing the space they can be requested to be 
         // N(ext) x N(ext) but with the extra columns/rows, of U^m/W^m
         // respectively, identically zero.
         const auto v_modal_trans_mats = util::GetRandomModalTransMats<T>(n_modals_prim, n_modals_td, this->mBalance, this->mIncludeZeroes);

         {
            Uin m = 0;
            for(const auto& [u, w]: v_modal_trans_mats)
            {
               const auto& size_prim = n_modals_prim.at(m);
               const auto& size_td = n_modals_td.at(m);
               const Uin u_n_cols = this->mIncludeZeroes? size_prim: size_td;
               const Uin w_n_rows = this->mIncludeZeroes? size_prim: size_td;

               // Assert dimensions of matrices.
               UNIT_ASSERT_EQUAL(u.Nrows(), size_prim, "Wrong u.Nrows(), m = "+std::to_string(m)+".");
               UNIT_ASSERT_EQUAL(u.Ncols(), u_n_cols, "Wrong u.Ncols(), m = "+std::to_string(m)+".");
               UNIT_ASSERT_EQUAL(w.Nrows(), w_n_rows, "Wrong w.Nrows(), m = "+std::to_string(m)+".");
               UNIT_ASSERT_EQUAL(w.Ncols(), size_prim, "Wrong w.Ncols(), m = "+std::to_string(m)+".");

               // Assert bi-orthonormality of non-zero parts.
               // v_diag = {1,...,1} or
               // v_diag = {1,...,1,0,...} (if mIncludeZeroes)
               vec_t v_diag(size_td, T(1));
               if (this->mIncludeZeroes)
               {
                  v_diag.Append(vec_t(size_prim - size_td, T(0)));
               }
               const mat_t prod = w * u;
               UNIT_ASSERT_FZERO_PREC(prod.NormOutOfDiagonal()/(size_td*(size_td-1)), absval_t(1), 28, "W*U, non-zero off-diagonal, m = "+std::to_string(m)+".");
               UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(prod.GetDiag(), v_diag))/Norm(v_diag), absval_t(1), 28, "W*U, diagonal not {1,...,1,0,...}, m = "+std::to_string(m)+".");

               // Assert
               // - balanced column/row norms of U/W respectively, or
               // - normalized columns of U
               for(Uin i = 0; i < size_td; ++i)
               {
                  if (this->mBalance)
                  {
                     UNIT_ASSERT_FEQUAL_PREC(w.NormRow(i), u.NormCol(i), 4, "Unbalanced column/row norms, m = "+std::to_string(m)+".");
                  }
                  else
                  {
                     UNIT_ASSERT_FEQUAL_PREC(u.NormCol(i), absval_t(1), 4, "U, un-normalized column, i = "+std::to_string(i)+".");
                  }
               }

               // Assert zeroes on last columns/rows.
               if (this->mIncludeZeroes)
               {
                  for(Uin i = size_td; i < w.Nrows(); ++i)
                  {
                     UNIT_ASSERT_FEQUAL_PREC(w.NormRow(i), absval_t(0), 0, "W, non-zero row, i = "+std::to_string(i)+".");
                     UNIT_ASSERT_FEQUAL_PREC(u.NormCol(i), absval_t(0), 0, "U, non-zero column, i = "+std::to_string(i)+".");
                  }
               }

               ++m;
            }
         }
      }

      private:
         bool mBalance = true;
         bool mIncludeZeroes = true;
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::VccOneModeDensityMatrices().
    ***************************************************************************/
   template<typename T>
   struct VccOneModeDensityMatricesTest
      :  public MatRepTransformersTestBase<T>
   {
      void run() override
      {
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = GeneralMidasMatrix<T>;
         using vec_t = GeneralMidasVector<T>;

         const auto n_modals = this->NModals();
         const Uin n_modes = n_modals.size();
         const auto mcr = this->Mcr();
         const auto t_ampls = util::GetRandomParams<T>(mcr, n_modals);
         const auto l_coefs = util::GetRandomParams<T>(mcr, n_modals);

         // CONTROL.
         MatRepVibOper<T> mr(n_modals);

         // |ket> = exp(T)|ref>
         vec_t ket(mr.FullDim(), T(0));
         ket[0] = 1;
         mr.ClusterOper(mcr, t_ampls);
         const mat_t exp_pT = mr.ExpClusterOper(+1);
         const mat_t exp_mT = mr.ExpClusterOper(-1);
         ket = vec_t(exp_pT * ket);

         // <bra| = <ref|(1+L)exp(-T)
         vec_t bra(mr.FullDim(), T(0));
         bra[0] = 1;
         mr.ClusterOper(mcr, l_coefs);
         bra = vec_t(bra * Transpose(mr.GetMatRep()));
         bra[0] += 1;
         bra = vec_t(bra * exp_mT);

         std::vector<mat_t> v_ctrl;
         for(Uin m = 0; m < n_modes; ++m)
         {
            const Uin n_m = n_modals.at(m);
            mat_t rho_m(n_m, n_m, T(0));
            for(Uin anni = 0; anni < rho_m.Nrows(); ++anni)
            {
               for(Uin crea = 0; crea < rho_m.Ncols(); ++crea)
               {
                  mr.ShiftOper(std::set<Uin>{m}, std::vector<Uin>{crea}, std::vector<Uin>{anni});
                  rho_m[anni][crea] = SumProdElems(bra, vec_t(mr.GetMatRep() * ket));
               }
            }
            v_ctrl.emplace_back(std::move(rho_m));
         }

         // METHOD.
         const std::vector<mat_t> v_result = midas::util::matrep::VccOneModeDensityMatrices
            (  n_modals
            ,  mcr
            ,  t_ampls
            ,  l_coefs
            );

         // Assertions.
         UNIT_ASSERT_EQUAL(v_result.size(), v_ctrl.size(), "Wrong size().");
         for(Uin m = 0; m < v_ctrl.size(); ++m)
         {
            const std::string s = ", m = " + std::to_string(m) + ".";
            const auto& res = v_result.at(m);
            const auto& ctrl = v_ctrl.at(m);
            UNIT_ASSERT_EQUAL(res.Nrows(), ctrl.Nrows(), "Wrong Nrows()"+s);
            UNIT_ASSERT_EQUAL(res.Ncols(), ctrl.Ncols(), "Wrong Ncols()"+s);
            UNIT_ASSERT_FZERO_PREC(mat_t(res - ctrl).Norm()/ctrl.Norm(), absval_t(1), 4, "Inequality"+s);
         }
      }
   };


   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::MeanFieldMatricesTdmvcc.
    *
    * Tests against naive implementations in the full direct-product space of
    * the primitive basis, that is _almost_ identical to the definitions, which
    * are
    * \f{align}{
    *    \check{F}^m_{\alpha^m v^m}
    *    &= \langle \Psi' \vert 
    *       \widetilde{a}^{m\dagger}_{v^m} [a^m_{\alpha^m}, H] 
    *       \vert \Psi \rangle
    *    \\
    *    \check{F}'^m_{w^m \beta^m}
    *    &= \langle \Psi' \vert 
    *       [H, a^{m\dagger}_{\beta^m}] \widetilde{b}^m_{w^m}
    *       \vert \Psi \rangle
    * \f}
    * The primitive mean-field matrices have elements
    * \f{align}{
    *    F^m_{\alpha^m \beta^m}
    *    &= \langle \Psi' \vert 
    *       a^{m\dagger}_{\beta^m} [a^m_{\alpha^m}, H] 
    *       \vert \Psi \rangle
    *    = \langle \Psi' \vert 
    *       E^m_{\beta^m,\alpha^m} H^m_\text{act}
    *       \vert \Psi \rangle
    *    \\
    *    F'^m_{\alpha^m \beta^m}
    *    &= \langle \Psi' \vert 
    *       [H, a^{m\dagger}_{\beta^m}] a^m_{\alpha^m}
    *       \vert \Psi \rangle
    *    = \langle \Psi' \vert 
    *       H^m_\text{act} E^m_{\beta^m,\alpha^m}
    *       \vert \Psi \rangle
    * \f}
    * where the commutators vanish are zero for inactive terms, and the minus
    * terms vanish for active terms due to double annihilation/creation in the
    * same mode.
    * The latter expressions can be evaluated in the primitive basis using
    * tools from util::matrep, because all operators are number conserving.
    *
    * The half-transformed mean-field matrices are then obtained from the
    * primitive ones through
    * \f{align}{
    *    \check{\mathbf{F}}^m &= \mathbf{F}^m \widetilde{\mathbf{U}}^m
    *    \\
    *    \check{\mathbf{F}}'^m &= \widetilde{\mathbf{W}}^m \mathbf{F}'^m
    * \f}
    * through the expressions
    * \f{align}{
    *    \widetilde{a}^{m\dagger}_{v^m}
    *    &= \sum_{\beta^m} a^{m\dagger}_{\beta^m} \widetilde{U}^m_{\beta^m,v^m}
    *    \\
    *    \widetilde{b}^{m}_{w^m}
    *    &= \sum_{\alpha^m} \widetilde{W}^m_{w^m,\alpha^m} a^{m}_{\alpha^m}
    * \f}
    ***************************************************************************/
   template<typename T>
   struct TdmvccHalfTransMeanFieldMatricesTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TdmvccHalfTransMeanFieldMatricesTest(bool aBalance, Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
         ,  mBalance(aBalance)
      {}

      void run() override
      {
         using midas::util::matrep::MatRepVibOper;
         using midas::util::matrep::SparseClusterOper;
         using midas::util::matrep::ExtendToSecondarySpace;
         using midas::util::matrep::McrOrganizedToStackedVec;
         using midas::util::matrep::IncrMultiIndex;
         using midas::util::matrep::TrfExpClusterOper;
         using midas::util::matrep::ShiftOperBraket;
         using midas::util::matrep::TdmvccHalfTransMeanFieldMatrices;
         using absval_t = midas::type_traits::RealTypeT<T>;
         using mat_t = GeneralMidasMatrix<T>;
         using vec_t = GeneralMidasVector<T>;

         // Setup system.
         const auto n_modals_td = this->NModals();
         const auto n_modals_prim = this->NModalsExtended();
         const Uin n_modes = n_modals_prim.size();
         const auto [opdef, modints] = this->Operator(n_modals_prim);
         const auto mcr = this->Mcr();

         // For cluster operators.
         const auto t_ampls_td = util::GetRandomParams<T>(mcr, n_modals_td);
         const auto l_coefs_td = util::GetRandomParams<T>(mcr, n_modals_td);

         // For modal transformations.
         const auto v_modal_trans_mats = util::GetRandomModalTransMats<T>(n_modals_prim, n_modals_td, this->mBalance);

         // Expressions to calculate for each mode:
         //    F^_{alpha,v}  = <bra| a_v^dagger [a_alpha, H] |ket>
         //    F^'_{v,alpha} = <bra| [H, a_alpha^dagger] b_v |ket>
         // Procedure:
         //    -  We work in a full space given by sizes of primitive/time-independent basis.
         //    -  We switch between time-dependent modals (including secondary
         //       space) and primitive.
         //    -  We recognize that for active terms, i.e. terms in H which
         //       include the mode m in question, the terms
         //          H_act a_alpha |ket> = 0
         //          <bra| a_alpha^dagger H_act = 0
         //       vanish because of the double annihilations.
         //    -  On the other hand, for in-active terms, the a_alpha[^dagger]
         //       commutes with H_inact, so the commutator vanishes.
         //    -  This allows us to rewrite:
         //          F^_{alpha,v}  = <bra| a_v^dagger a_alpha H_act |ket>
         //          F^'_{w,beta}  = <bra| H_act a_beta^dagger b_v |ket>
         //    -  Expressing the time-dep. crea./anni. operators like
         //          a_v^dagger = sum_beta  a_beta^dagger U_{beta,v}
         //          b_w        = sum_alpha W_{w,alpha} a_alpha
         //       we get the expressions
         //          F^_{alpha,v}  = sum_beta F_{alpha,beta} U_{beta,v}
         //          F^'_{w,beta}  = sum_alpha W_{w,alpha} F'_{alpha,beta}
         //       where
         //          |ket_prim>  = prod_m U^m exp(T_td)|ref>
         //          <bra_prim|  = <ref|(1+L_td)exp(-T_td) prod_m W^m
         //       and
         //          F_{alpha,beta}  = <bra_prim| a_beta^dagger a_alpha H |ket_prim>
         //          F'_{alpha,beta} = <bra_prim| H a_beta^dagger a_alpha |ket_prim>


         // CONTROL
         // We'll work with matrix representations of a size corresponding to
         // the primitive basis.
         MatRepVibOper<T> mr(n_modals_prim);
         SparseClusterOper clust_oper(n_modals_prim, mcr);

         // Convert the t-amps./l-coefs from active-only-space to
         // extended space (including secondary space). Still it's in the
         // time-dep. basis.
         const auto t_ampls_ext = ExtendToSecondarySpace(mcr, n_modals_td, n_modals_prim, t_ampls_td);
         const auto l_coefs_ext = ExtendToSecondarySpace(mcr, n_modals_td, n_modals_prim, l_coefs_td);
         const vec_t vec_t_ampls_ext = McrOrganizedToStackedVec<T,GeneralMidasVector>(t_ampls_ext);
         const vec_t vec_l_coefs_ext = McrOrganizedToStackedVec<T,GeneralMidasVector>(l_coefs_ext);

         // The bra and ket states, constant for all modes:
         // |ket> = exp(+T)|ref>
         vec_t ket(clust_oper.FullDim(), T(0));
         ket[0] = 1;
         ket = TrfExpClusterOper<false,false>(clust_oper, vec_t_ampls_ext, std::move(ket), T(+1));
         vec_t bra = clust_oper.RefToFullSpace<false>(vec_l_coefs_ext);
         bra[0] += 1;
         bra = TrfExpClusterOper<true,false>(clust_oper, vec_t_ampls_ext, std::move(bra), T(-1));

         // Set up direct product operators of the 1-mode U_m and W_m matrices.
         // And vectors with truncated versions for later use.
         std::vector<mat_t> v_u_mats;
         std::vector<mat_t> v_w_mats;
         std::vector<std::pair<mat_t,mat_t>> v_trunc_mats;
         {
            Uin m = 0;
            for(const auto& [U_m, W_m]: v_modal_trans_mats)
            {
               v_u_mats.emplace_back(U_m);
               v_w_mats.emplace_back(W_m);

               mat_t U_trunc = U_m;
               U_trunc.SetNewSize(In(n_modals_prim.at(m)), In(n_modals_td.at(m)));
               mat_t W_trunc = W_m;
               W_trunc.SetNewSize(In(n_modals_td.at(m)), In(n_modals_prim.at(m)));
               v_trunc_mats.emplace_back(std::move(U_trunc), std::move(W_trunc));

               ++m;
            }
         }
         mr.DirProdOneModeOpers(v_u_mats);
         const mat_t U_tot = mr.GetMatRep();
         mr.DirProdOneModeOpers(v_w_mats);
         const mat_t W_tot = mr.GetMatRep();

         // Transform from extended time-dep. basis to primitive basis.
         ket = vec_t(U_tot * ket);
         bra = vec_t(bra * W_tot);

         // Loop through modes.
         std::vector<std::pair<mat_t,mat_t>> ctrl_v_f_fp;
         for(Uin m = 0; m < n_modes; ++m)
         {
            // Construct H_act for mode m and transform ket/bra with it.
            mr.FullOperator(opdef, modints, std::make_pair(true, m));
            const mat_t H_act_m = mr.GetMatRep();
            const vec_t H_ket = vec_t(H_act_m * ket);
            const vec_t bra_H = vec_t(bra * H_act_m);

            // Construct F and F' in primitive basis.
            const Uin N_m = n_modals_prim.at(m);
            const Uin n_m = n_modals_td.at(m);
            mat_t F(N_m, N_m, T(0));
            mat_t Fp(N_m, N_m, T(0));
            std::set<Uin> set_m = {m};
            std::vector<Uin> dim_m = {N_m};
            std::vector<Uin> alpha(set_m.size(), 0);
            std::vector<Uin> beta(set_m.size(), 0);
            for(Uin i_a = 0; i_a < N_m; ++i_a, IncrMultiIndex(alpha, dim_m))
            {
               for(Uin i_b = 0; i_b < N_m; ++i_b, IncrMultiIndex(beta, dim_m))
               {
                  F[i_a][i_b]  = ShiftOperBraket<false>(bra, H_ket, n_modals_prim, set_m, beta, alpha);
                  Fp[i_a][i_b] = ShiftOperBraket<false>(bra_H, ket, n_modals_prim, set_m, beta, alpha);
               }
            }

            // Convert them to F^ and F^' in half-transformed basis.
            const auto& [U_m, W_m] = v_trunc_mats.at(m);
            F  = mat_t(F * U_m);
            Fp = mat_t(W_m * Fp);
            UNIT_ASSERT_EQUAL(Uin(F.Nrows()), N_m, "Wrong F.Nrows(), m = "+std::to_string(m));
            UNIT_ASSERT_EQUAL(Uin(F.Ncols()), n_m, "Wrong F.Ncols(), m = "+std::to_string(m));
            UNIT_ASSERT_EQUAL(Uin(Fp.Nrows()), n_m, "Wrong Fp.Nrows(), m = "+std::to_string(m));
            UNIT_ASSERT_EQUAL(Uin(Fp.Ncols()), N_m, "Wrong Fp.Ncols(), m = "+std::to_string(m));

            ctrl_v_f_fp.emplace_back(std::move(F), std::move(Fp));
         }


         // METHOD
         const std::vector<std::pair<mat_t,mat_t>> v_f_fp = TdmvccHalfTransMeanFieldMatrices
            (  n_modals_td
            ,  opdef
            ,  modints
            ,  v_trunc_mats
            ,  mcr
            ,  t_ampls_td
            ,  l_coefs_td
            ,  false
            );

         // ASSERTIONS.
         UNIT_ASSERT_EQUAL(v_f_fp.size(), ctrl_v_f_fp.size(), "Wrong v_f_fp.size().");
         {
            Uin m = 0;
            for(  auto it = v_f_fp.cbegin(), it_ctrl = ctrl_v_f_fp.cbegin()
               ;  it != v_f_fp.cend() && it_ctrl != ctrl_v_f_fp.cend()
               ;  ++it, ++it_ctrl, ++m
               )
            {
               const auto& [F, Fp] = *it;
               const auto& [ctrl_F, ctrl_Fp] = *it_ctrl;

               const std::string s = ", m = "+std::to_string(m)+".";
               UNIT_ASSERT_EQUAL(F.Nrows(), ctrl_F.Nrows(), "Wrong F.Nrows()"+s);
               UNIT_ASSERT_EQUAL(F.Ncols(), ctrl_F.Ncols(), "Wrong F.Ncols()"+s);
               UNIT_ASSERT_EQUAL(Fp.Nrows(), ctrl_Fp.Nrows(), "Wrong Fp.Nrows()"+s);
               UNIT_ASSERT_EQUAL(Fp.Ncols(), ctrl_Fp.Ncols(), "Wrong Fp.Ncols()"+s);
               UNIT_ASSERT_FZERO_PREC(mat_t(F-ctrl_F).Norm()/ctrl_F.Norm(), absval_t(1), 60, "F inequality"+s);
               UNIT_ASSERT_FZERO_PREC(mat_t(Fp-ctrl_Fp).Norm()/ctrl_Fp.Norm(), absval_t(1), 60, "Fp inequality"+s);
            }
         }
      }

      private:
         bool mBalance = true;
   };

   /************************************************************************//**
    * @brief
    *    Tests midas::util::matrep::TdmvccUDotBaseExpression() and
    *    midas::util::matrep::TdmvccWDotBaseExpression().
    ***************************************************************************/
   template<typename T>
   struct TdmvccUDotWDotBaseExpressionTest
      :  public MatRepTransformersTestBase<T>
   {
      template<class... Args>
      TdmvccUDotWDotBaseExpressionTest(bool aBalance, Args&&... args)
         :  MatRepTransformersTestBase<T>(std::forward<Args>(args)...)
         ,  mBalance(aBalance)
      {}

      void run() override
      {
         using mat_t = GeneralMidasMatrix<T>;
         using vec_t = GeneralMidasVector<T>;
         using midas::util::matrep::TdmvccUDotBaseExpression;
         using midas::util::matrep::TdmvccWDotBaseExpression;

         // Setup system.
         const auto n_modals_td = this->NModals();
         const auto n_modals_prim = this->NModalsExtended();
         const Uin n_modes = n_modals_prim.size();

         const auto vec_U_W = util::GetRandomModalTransMats<T>(n_modals_prim, n_modals_td, this->mBalance, false);

         for(Uin m = 0; m < n_modes; ++m)
         {
            const std::string s = ", m = "+std::to_string(m)+".";
            const auto& n_td = n_modals_td.at(m);
            const auto& n_prim = n_modals_prim.at(m);
            const auto& [U,W] = vec_U_W.at(m);
            const mat_t G = random::RandomMidasMatrix<T>(n_prim, n_prim);
            const mat_t I(n_prim, vec_t(n_prim, T(1)));
            const mat_t P = mat_t(U*W);
            const mat_t F_chk = random::RandomMidasMatrix<T>(n_prim, n_td);
            const mat_t F_chk_pr = random::RandomMidasMatrix<T>(n_td, n_prim);
            const mat_t rho_inv = random::RandomMidasMatrix<T>(n_td, n_td);

            const mat_t ctrl_pls_iU_dot = mat_t(G*U + (I-P)*(F_chk*rho_inv - G*U));
            const mat_t ctrl_min_iW_dot = mat_t(W*G + (rho_inv*F_chk_pr - W*G)*(I-P));
            UNIT_ASSERT_EQUAL(ctrl_pls_iU_dot.Nrows(), U.Nrows(), "Wrong ctrl_pls_iU_dot.Nrows()"+s);
            UNIT_ASSERT_EQUAL(ctrl_pls_iU_dot.Ncols(), U.Ncols(), "Wrong ctrl_pls_iU_dot.Ncols()"+s);
            UNIT_ASSERT_EQUAL(ctrl_min_iW_dot.Nrows(), W.Nrows(), "Wrong ctrl_min_iW_dot.Nrows()"+s);
            UNIT_ASSERT_EQUAL(ctrl_min_iW_dot.Ncols(), W.Ncols(), "Wrong ctrl_min_iW_dot.Ncols()"+s);

            const mat_t g = mat_t(W * G * U);
            const mat_t pls_iU_dot = TdmvccUDotBaseExpression(U, F_chk, P, rho_inv, g);
            const mat_t min_iW_dot = TdmvccWDotBaseExpression(W, F_chk_pr, P, rho_inv, g);

            UNIT_ASSERT_EQUAL(pls_iU_dot.Nrows(), ctrl_pls_iU_dot.Nrows(), "Wrong pls_iU_dot.Nrows()"+s);
            UNIT_ASSERT_EQUAL(pls_iU_dot.Ncols(), ctrl_pls_iU_dot.Ncols(), "Wrong pls_iU_dot.Ncols()"+s);
            UNIT_ASSERT_EQUAL(min_iW_dot.Nrows(), ctrl_min_iW_dot.Nrows(), "Wrong min_iW_dot.Nrows()"+s);
            UNIT_ASSERT_EQUAL(min_iW_dot.Ncols(), ctrl_min_iW_dot.Ncols(), "Wrong min_iW_dot.Ncols()"+s);
            UNIT_ASSERT_FZERO_PREC(mat_t(pls_iU_dot - ctrl_pls_iU_dot).Norm()/ctrl_pls_iU_dot.Norm(), absval_t<T>(1), 20, "Ineq. U"+s);
            UNIT_ASSERT_FZERO_PREC(mat_t(min_iW_dot - ctrl_min_iW_dot).Norm()/ctrl_min_iW_dot.Norm(), absval_t<T>(1), 20, "Ineq. W"+s);
         }
      }

      private:
         bool mBalance = true;
   };


} /* namespace midas::test::matrep::trf */

#endif/*MATREPVIBOPERTEST_H_INCLUDED*/
