#include "test/DataContTest.h"
#include "util/type_traits/TypeName.h"

namespace midas::test
{
   namespace datacont::detail
   {
      template
         <  typename T
         >
      void AddLoopOverElementsTestsImpl
         (  cutee::suite& arSuite
         )
      {
         using midas::type_traits::TypeName;
         std::string s = " ("+TypeName<T>()+")";
         std::string s_dc = " ("+TypeName<T>()+","+TypeName<GeneralDataCont>()+")";
         std::string s_mv = " ("+TypeName<T>()+","+TypeName<GeneralMidasVector>()+")";

         // LoopOverElementsAxpyTest
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralMidasVector,true>>("LoopOverElementsAxpyTest InMem"+s_mv);
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralMidasVector,false>>("LoopOverElementsAxpyTest OnDisc"+s_mv);
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralDataCont,true,true>>("LoopOverElementsAxpyTest InMem/InMem"+s_dc);
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralDataCont,true,false>>("LoopOverElementsAxpyTest InMem/OnDisc"+s_dc);
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralDataCont,false,true>>("LoopOverElementsAxpyTest OnDisc/InMem"+s_dc);
         arSuite.add_test<LoopOverElementsAxpyTest<T,GeneralDataCont,false,false>>("LoopOverElementsAxpyTest OnDisc/OnDisc"+s_dc);

         // LoopOverElementsScaleTest
         arSuite.add_test<LoopOverElementsScaleTest<T,true>>("LoopOverElementsScaleTest InMem"+s);
         arSuite.add_test<LoopOverElementsScaleTest<T,false>>("LoopOverElementsScaleTest OnDisc"+s);

         // LoopOverElementsDotTest
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralMidasVector,true>>("LoopOverElementsDotTest InMem"+s_mv);
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralMidasVector,false>>("LoopOverElementsDotTest OnDisc"+s_mv);
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralDataCont,true,true>>("LoopOverElementsDotTest InMem/InMem"+s_dc);
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralDataCont,true,false>>("LoopOverElementsDotTest InMem/OnDisc"+s_dc);
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralDataCont,false,true>>("LoopOverElementsDotTest OnDisc/InMem"+s_dc);
         arSuite.add_test<LoopOverElementsDotTest<T,GeneralDataCont,false,false>>("LoopOverElementsDotTest OnDisc/OnDisc"+s_dc);

         // LoopOverElementsNorm2Test
         arSuite.add_test<LoopOverElementsNorm2Test<T,true>>("LoopOverElementsNorm2Test InMem (full range)"+s);
         arSuite.add_test<LoopOverElementsNorm2Test<T,false>>("LoopOverElementsNorm2Test OnDisc (full range)"+s);
         arSuite.add_test<LoopOverElementsNorm2Test<T,true>>("LoopOverElementsNorm2Test InMem [2;13)"+s, 2, 13);
         arSuite.add_test<LoopOverElementsNorm2Test<T,false>>("LoopOverElementsNorm2Test OnDisc [2;13)"+s, 2, 13);

         // LoopOverElementsDoubleDiffNorm2Test
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralMidasVector,true>>("LoopOverElementsDoubleDiffNorm2Test OnDisc"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralMidasVector,false>>("LoopOverElementsDoubleDiffNorm2Test InMem"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,true,true,true>>("LoopOverElementsDoubleDiffNorm2Test InMem/InMem/InMem"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,true,true,false>>("LoopOverElementsDoubleDiffNorm2Test InMem/InMem/OnDisc"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,true,false,true>>("LoopOverElementsDoubleDiffNorm2Test InMem/OnDisc/InMem"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,true,false,false>>("LoopOverElementsDoubleDiffNorm2Test InMem/OnDisc/OnDisc"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,false,true,true>>("LoopOverElementsDoubleDiffNorm2Test OnDisc/InMem/InMem"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,false,true,false>>("LoopOverElementsDoubleDiffNorm2Test OnDisc/InMem/OnDisc"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,false,false,true>>("LoopOverElementsDoubleDiffNorm2Test OnDisc/OnDisc/InMem"+s);
         arSuite.add_test<LoopOverElementsDoubleDiffNorm2Test<T,GeneralDataCont,false,false,false>>("LoopOverElementsDoubleDiffNorm2Test OnDisc/OnDisc/OnDisc"+s);
      }

      template
         <  typename T
         ,  bool ThisInMem
         >
      void AddCumulativeFunctionTestsImpl
         (  cutee::suite& arSuite
         )
      {
         using midas::type_traits::TypeName;
         std::string s = " ("+TypeName<T>()+","+(ThisInMem? "InMem": "OnDisc")+")";

         arSuite.add_test<NormTest<T,ThisInMem>>("Norm (full range)"+s);
         arSuite.add_test<Norm2Test<T,ThisInMem>>("Norm2 (full range)"+s);
         arSuite.add_test<SumEleTest<T,ThisInMem>>("SumEle (full range)"+s);
         arSuite.add_test<SumAbsEleTest<T,ThisInMem>>("SumAbsEle (full range)"+s);
         arSuite.add_test<DotTest<T,ThisInMem>>("Dot (full range)"+s);
         arSuite.add_test<SumProdElemsTest<T,ThisInMem>>("SumProdElems (full range)"+s);
         arSuite.add_test<DiffNorm2Test<T,ThisInMem>>("DiffNorm2 (full range)"+s);
         arSuite.add_test<DiffNorm2ScaledTest<T,ThisInMem,false>>("DiffNorm2Scaled !CONJ (full range)"+s);
         arSuite.add_test<DiffNorm2ScaledTest<T,ThisInMem,true>>("DiffNorm2Scaled CONJ (full range)"+s);
         arSuite.add_test<NormTest<T,ThisInMem>>("Norm [2;2+11)"+s, 2, 11);
         arSuite.add_test<Norm2Test<T,ThisInMem>>("Norm2 [2;2+11)"+s, 2, 11);
         arSuite.add_test<SumEleTest<T,ThisInMem>>("SumEle [2;2+11)"+s, 2, 11);
         arSuite.add_test<SumAbsEleTest<T,ThisInMem>>("SumAbsEle [2;2+11)"+s, 2, 11);
         arSuite.add_test<DotTest<T,ThisInMem>>("Dot [2;2+11)"+s, 2, 11);
         arSuite.add_test<SumProdElemsTest<T,ThisInMem>>("SumProdElems [2;2+11)"+s, 2, 11);
         arSuite.add_test<DiffNorm2Test<T,ThisInMem>>("DiffNorm2 [2;2+11)"+s, 2, 11);
         arSuite.add_test<DiffNorm2ScaledTest<T,ThisInMem,false>>("DiffNorm2Scaled !CONJ [2;2+11)"+s, 2, 11);
         arSuite.add_test<DiffNorm2ScaledTest<T,ThisInMem,true>>("DiffNorm2Scaled CONJ [2;2+11)"+s, 2, 11);
      }

   } /* namespace datacont::detail */

void DataContTest()
{
   using namespace datacont;
   cutee::suite suite("DataCont test");
   
   suite.add_test<ConstructorTest>("DataCont constructor test");
   suite.add_test<ConstructorTest2>("DataCont constructor test 2");
   suite.add_test<ConstructorTest3>("DataCont constructor test 3");
   suite.add_test<SaveOnDiscTest>("DataCont save on disc test");
   suite.add_test<SaveOnDiscTest2>("DataCont save on disc test 2");
   suite.add_test<DataIoReturnStatusTest<GeneralMidasVector, Nb>>("DataIo ret.stat., GeneralMidasVector<Nb>");
   suite.add_test<DataIoReturnStatusTest<GeneralMidasMatrix, Nb>>("DataIo ret.stat., GeneralMidasMatrix<Nb>");
   suite.add_test<DataIoReturnStatusTest<GeneralMidasVector, std::complex<Nb>>>("DataIo ret.stat., GeneralMidasVector<std::complex<Nb>>");
   suite.add_test<DataIoReturnStatusTest<GeneralMidasMatrix, std::complex<Nb>>>("DataIo ret.stat., GeneralMidasMatrix<std::complex<Nb>>");
   suite.add_test<DataIoNbReturnStatusTest<Nb>>("DataIo ret.stat., Nb");
   suite.add_test<DataIoNbReturnStatusTest<std::complex<Nb>>>("DataIo ret.stat., std::complex<Nb>");
   suite.add_test<HadamardProductTest>("DataCont Hadamard product test");
   suite.add_test<HadamardInverseProductTest>("DataCont Hadamard inverse product test");
   suite.add_test<HadamardProductSelfTest>("DataCont Hadamard product self test");
   suite.add_test<HadamardInverseProductSelfTest>("DataCont Hadamard inverse product self test");
   suite.add_test<HadamardProductVectorTest>("DataCont Hadamard product vector test");
   suite.add_test<DataContInsertTest<Nb,true>>("DataCont Insert test (real,InMem)");
   suite.add_test<DataContInsertTest<Nb,false>>("DataCont Insert test (comp,InMem)");
   suite.add_test<DataContInsertTest<std::complex<Nb>,true>>("DataCont Insert test (real,OnDisc)");
   suite.add_test<DataContInsertTest<std::complex<Nb>,false>>("DataCont Insert test (comp,OnDisc)");
   suite.add_test<DataContEraseTest<true>>("DataCont Erase test (InMem)");
   suite.add_test<DataContEraseTest<false>>("DataCont Erase test (OnDisc)");
   suite.add_test<DataContConjugateTest<Nb>>("DataCont Conjugate (Nb)");
   suite.add_test<DataContConjugateTest<std::complex<Nb>>>("DataCont Conjugate (std::complex<Nb>)");


   datacont::detail::AddLoopOverElementsTestsImpl<Nb>(suite);
   datacont::detail::AddLoopOverElementsTestsImpl<std::complex<Nb>>(suite);
   datacont::detail::AddCumulativeFunctionTestsImpl<Nb,true>(suite);
   datacont::detail::AddCumulativeFunctionTestsImpl<std::complex<Nb>,true>(suite);
   datacont::detail::AddCumulativeFunctionTestsImpl<Nb,false>(suite);
   datacont::detail::AddCumulativeFunctionTestsImpl<std::complex<Nb>,false>(suite);
   
   
   RunSuite(suite);
}

} /* namespace midas::test */
