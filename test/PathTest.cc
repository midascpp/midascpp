/**
 *******************************************************************************
 * 
 * @file    PathTest.cc 
 * @date    NOW
 * @author  Ian Heide Godtliebsen (ian@chem.au.dk)
 *
 * @brief
 *    Tests functionality of util/Path.h/.cc
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/PathTest.h" 

namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void PathTest() 
   {
      // Create test_suite object.
      cutee::suite suite("PathTest test");

      // Add all the tests to the suite.
      suite.add_test<path_test::TestHasDirEnd  >("Test midas::path::HasDirEnd()."  );
      suite.add_test<path_test::TestHasDirBegin>("Test midas::path::HasDirBegin().");

      suite.add_test<path_test::TestFileName>("Test midas::path::FileName().");
      suite.add_test<path_test::TestBaseName>("Test midas::path::BaseName().");
      suite.add_test<path_test::TestDirName >("Test midas::path::DirName()." );
      
      suite.add_test<path_test::TestRelPath> ("Test midas::path::IsRelPath().");
      suite.add_test<path_test::TestAbsPath> ("Test midas::path::IsAbsPath().");
      suite.add_test<path_test::TestJoin   > ("Test midas::path::Join()."     );

      // Run the tests.
      RunSuite(suite);
   }

} /* namespace midas::test */
