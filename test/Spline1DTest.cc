#include "test/Spline1DTestCase.h"

namespace midas::test
{

void Spline1DTest()
{
   cutee::suite suite("Spline1D test");
   suite.add_test<Spline1DTestCase>("Spline1D test");
   RunSuite(suite);
} 

} /* namespace midas::test */
