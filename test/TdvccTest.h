/**
 *******************************************************************************
 * 
 * @file    TdvccTest.h
 * @date    15-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCTEST_H_INCLUDED
#define TDVCCTEST_H_INCLUDED

#include "input/ProgramSettings.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Random.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "vcc/ModalIntegrals.h"
#include "test/util/TransTestUtils.h"
#include "test/util/OpDefsForTesting.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "ni/OneModeInt.h"
#include "input/VscfCalcDef.h"
#include "vscf/Vscf.h"
#include "vcc/ModalIntegralsFuncs.h"

#include "td/tdvcc/params/ParamsTdvcc.h"
#include "td/tdvcc/params/ParamsTdvci.h"
#include "td/tdvcc/deriv/DerivTdvcc.h"
#include "td/tdvcc/deriv/DerivTdvci.h"
#include "td/tdvcc/trf/TrfTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTdvcc2.h"
#include "td/tdvcc/trf/TrfTdvci2.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvcc.h"
#include "td/tdvcc/trf/TrfGeneralTimTdvci.h"
#include "td/tdvcc/anal/ExpValTdvcc.h"
#include "td/tdvcc/Tdvcc.h"
#include "td/oper/LinCombOper.h"
#include "ode/OdeInfo.h"
#include "input/OdeInput.h"
#include "ode/OdeIntegrator.h"

#include "test/util/MpiSyncedAssertions.h"
#include "test/CuteeInterface.h"

template<typename> class GeneralMidasVector;

namespace midas::test::tdvcc
{
   using namespace midas::tdvcc;

   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
      /*********************************************************************//**
       * @name Some VCC/VCI type traits
       ************************************************************************/
      //!@{
      template<template<typename, template<typename> class> class PARAM_CONT_T>
      struct DoingVcc: std::false_type {};

      template<>
      //struct DoingVcc<ParamsTdvcc> {static constexpr bool value = true;};
      struct DoingVcc<ParamsTdvcc>: std::true_type {};

      template<template<typename, template<typename> class> class PARAM_CONT_T>
      inline constexpr bool DoingVccV = DoingVcc<PARAM_CONT_T>::value;

      template<class>
      struct DoingVccDeriv: std::false_type {};

      template<typename T, template<typename> class C, template<typename> class TRF, bool I>
      struct DoingVccDeriv<DerivTdvcc<T,C,TRF,I>>: std::true_type {};

      template<class DERIV_T>
      inline constexpr bool DoingVccDerivV = DoingVccDeriv<DERIV_T>::value;

      //!@}

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename PARAM_T, bool DOING_VCC = true>
      struct TestSystemSetup
      {
         protected:
            using param_t = PARAM_T;
            using absval_t = midas::type_traits::RealTypeT<param_t>;

            TestSystemSetup
               (  std::vector<Uin> aNModals
               ,  std::vector<Uin> aNOpers
               ,  Uin aMaxExciAmps
               ,  Uin aMaxCoupOper
               )
               :  mNModals(std::move(aNModals))
               ,  mNOpers(std::move(aNOpers))
               ,  mMaxExciAmps(aMaxExciAmps)
               ,  mMaxCoupOper(aMaxCoupOper)
            {
            }

            const std::vector<Uin>& NModals() const {return mNModals;}
            const std::vector<Uin>& NOpers() const {return mNOpers;}
            Uin MaxExciAmps() const {return mMaxExciAmps;}
            Uin MaxCoupOper() const {return mMaxCoupOper;}

            std::pair<ModeCombiOpRange,Uin> OperMcr() const
            {
               return util::GetMcrRandomMcsErased(NOpers(), MaxCoupOper());
            }
            std::pair<ModeCombiOpRange,Uin> ParamsMcr() const
            {
               return util::GetMcrRandomMcsErased(NModals(), MaxExciAmps());
            }
            OpDef RandomOpDef(const ModeCombiOpRange& arOperMcr) const
            {
               const auto coefs = util::GetRandomParams<absval_t>(arOperMcr, NOpers(), false, false, 0);
               return util::GetOpDefFromCoefs(arOperMcr, NOpers(), coefs);
            }
            ModalIntegrals<param_t> RandomModInts(const OpDef& arOpDef) const
            {
               return util::GetRandomModalIntegrals<param_t>(arOpDef, NModals());
            }
            std::vector<std::vector<param_t>> RandomParams(const ModeCombiOpRange& arParamsMcr, bool aZeroSingles = false) const
            {
               return util::GetRandomParams<param_t>(arParamsMcr, NModals(), DOING_VCC, aZeroSingles);
            }
            
         private:
            std::vector<Uin> mNModals = {};
            std::vector<Uin> mNOpers = {};
            Uin mMaxExciAmps = 0;
            Uin mMaxCoupOper = 0;
      };
   }

   namespace params
   {
      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvccBase
         :  public cutee::test
      {
         private:
            const Uin mSize = 4;

            const ModeCombiOpRange mMcr = ModeCombiOpRange(2, 2);
            const std::vector<Uin> mNModals = { 2, 2 };

         protected:
            using absval_t = midas::type_traits::RealTypeT<T>;
            using param_cont_t = ParamsTdvcc<T,CONT_T>;

            Uin ParamsSize() const {return mSize;}
            const auto& Mcr() const {return mMcr;}
            const auto& NModals() const {return mNModals;}

            CONT_T<T> GetParams() const
            {
               if constexpr   (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T>>
                              )
               {
                  return RandomTensorDataCont<T>(this->Mcr(), this->NModals(), true);
               }
               else
               {
                  return random::RandomVectorContainer<T,CONT_T>{}(ParamsSize());
               }
            }
            CONT_T<T> GetZeroParams() const
            {
               if constexpr   (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T>>
                              )
               {
                  return GeneralTensorDataCont<T>(this->Mcr(), this->NModals(), true);
               }
               else
               {
                  return CONT_T<T>(ParamsSize(), T(0));
               }
            }

            //@{
            //! Diff.norm(^2), using Axpy and Norm(2) impl. for CONT_T.
            static absval_t DiffNorm2(CONT_T<T> a, const CONT_T<T>& b)
            {
               Axpy(a, b, T(-1));
               return a.Norm2();
            }

            static absval_t DiffNorm(CONT_T<T> a, const CONT_T<T>& b)
            {
               return sqrt(DiffNorm2(a, b));
            }

            static absval_t DiffNorm2(const param_cont_t& a, const param_cont_t& b)
            {
               absval_t norm2 = 0;
               norm2 += midas::util::AbsVal2(a.Phase() - b.Phase());
               norm2 += DiffNorm2(a.ClusterAmps(), b.ClusterAmps());
               norm2 += DiffNorm2(a.LambdaCoefs(), b.LambdaCoefs());
               return norm2;
            }

            static absval_t DiffNorm(const param_cont_t& a, const param_cont_t& b)
            {
               return sqrt(DiffNorm2(a, b));
            }

            static std::vector<T> CompoundContainer(const param_cont_t& a)
            {
               std::vector<T> v;
               v.reserve(a.Size());
               GeneralMidasVector<T> mv_params(a.SizeParams());
               
               // Fill in phase.
               v.emplace_back(a.Phase());

               // Fill in T-amps.
               if constexpr(std::is_same_v<CONT_T<T>, GeneralDataCont<T>>)
               {
                  a.ClusterAmps().DataIo(IO_GET, mv_params, mv_params.Size());
               }
               else if constexpr (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T> >
                                 )
               {
                  auto dc = DataContFromTensorDataCont(a.ClusterAmps());
                  dc.DataIo(IO_GET, mv_params, mv_params.Size());
               }
               else
               {
                  mv_params = a.ClusterAmps();
               }
               for(Uin i = 0; i < mv_params.Size(); ++i)
               {
                  v.emplace_back(mv_params[i]);
               }

               // Fill in L-coefs.
               if constexpr(std::is_same_v<CONT_T<T>, GeneralDataCont<T>>)
               {
                  a.LambdaCoefs().DataIo(IO_GET, mv_params, mv_params.Size());
               }
               else if constexpr (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T> >
                                 )
               {
                  auto dc = DataContFromTensorDataCont(a.LambdaCoefs());
                  dc.DataIo(IO_GET, mv_params, mv_params.Size());
               }
               else
               {
                  mv_params = a.LambdaCoefs();
               }
               for(Uin i = 0; i < mv_params.Size(); ++i)
               {
                  v.emplace_back(mv_params[i]);
               }

               return v;
            }

            std::vector<absval_t> SeparateRealImagParts(const std::vector<T>& a)
            {
               if constexpr(midas::type_traits::IsComplexV<T>)
               {
                  std::vector<absval_t> v;
                  v.reserve(2*a.size());
                  for(auto&& elem: a)
                  {
                     v.emplace_back(std::real(elem));
                     v.emplace_back(std::imag(elem));
                  }
                  return v;
               }
               else
               {
                  return a;
               }
            }

            static absval_t CtrlOdeMeanNorm2
               (  const param_cont_t& aDeltaY
               ,  absval_t aAbsTol
               ,  absval_t aRelTol
               ,  const param_cont_t& aYOld
               ,  const param_cont_t& aYNew
               )
            {
               GeneralMidasVector<T> dely(CompoundContainer(aDeltaY));
               GeneralMidasVector<T> yold(CompoundContainer(aYOld));
               GeneralMidasVector<T> ynew(CompoundContainer(aYNew));
               return OdeMeanNorm2(dely, aAbsTol, aRelTol, yold, ynew);
            }

            static absval_t CtrlOdeMaxNorm2
               (  const param_cont_t& aDeltaY
               ,  absval_t aAbsTol
               ,  absval_t aRelTol
               ,  const param_cont_t& aYOld
               ,  const param_cont_t& aYNew
               )
            {
               GeneralMidasVector<T> dely(CompoundContainer(aDeltaY));
               GeneralMidasVector<T> yold(CompoundContainer(aYOld));
               GeneralMidasVector<T> ynew(CompoundContainer(aYNew));
               return OdeMaxNorm2(dely, aAbsTol, aRelTol, yold, ynew);
            }

            //@}
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvccConstructorEmpty
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            param_cont_t params;
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params.Size()), Uin(1), "Wrong Size().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params.SizeParams()), Uin(0), "Wrong SizeParams().");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(params.Phase(), T(0), 0, "Wrong Phase().");
            const auto& amps = params.ClusterAmps();
            const auto& coefs= params.LambdaCoefs();
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(amps)), Uin(0), "Wrong Size(amps).");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(coefs)),Uin(0), "Wrong Size(coefs).");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvccConstructorZero
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            std::unique_ptr<param_cont_t> params = nullptr;
            if constexpr   (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T>>
                           )
            {
               params = std::make_unique<param_cont_t>(this->Mcr(), this->NModals());
            }
            else
            {
               params = std::make_unique<param_cont_t>(this->ParamsSize());
            }
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params->Size()), Uin(1+2*this->ParamsSize()), "Wrong Size().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params->SizeParams()), Uin(this->ParamsSize()), "Wrong SizeParams().");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(params->Phase(), T(0), 0, "Wrong Phase().");
            const auto& amps = params->ClusterAmps();
            const auto& coefs= params->LambdaCoefs();
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(amps)), Uin(this->ParamsSize()), "Wrong Size(amps).");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(coefs)),Uin(this->ParamsSize()), "Wrong Size(coefs).");
            const auto ctrl_zero = this->GetZeroParams();
            const auto dn_amps = this->DiffNorm(amps, ctrl_zero);
            const auto dn_coefs= this->DiffNorm(coefs,ctrl_zero);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(dn_amps, absval_t(0), 0, "Wrong diff.norm, amps.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(dn_coefs,absval_t(0), 0, "Wrong diff.norm, coefs.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvccConstructorCustom
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const auto ctrl_amps = this->GetParams();
            const auto ctrl_coefs = this->GetParams();
            const auto ctrl_phase = random::RandomNumber<T>();
            param_cont_t params(ctrl_amps, ctrl_coefs, ctrl_phase);
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params.Size()), Uin(1+2*this->ParamsSize()), "Wrong Size().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(params.SizeParams()), Uin(this->ParamsSize()), "Wrong SizeParams().");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(params.Phase(), ctrl_phase, 0, "Wrong Phase().");
            const auto& amps = params.ClusterAmps();
            const auto& coefs= params.LambdaCoefs();
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(amps)), Uin(this->ParamsSize()), "Wrong Size(amps).");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(coefs)),Uin(this->ParamsSize()), "Wrong Size(coefs).");
            const auto dn_amps = this->DiffNorm(amps, ctrl_amps);
            const auto dn_coefs= this->DiffNorm(coefs,ctrl_coefs);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(dn_amps, absval_t(0), 0, "Wrong diff.norm, amps.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(dn_coefs,absval_t(0), 0, "Wrong diff.norm, coefs.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_Scale
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            T ctrl_phase = random::RandomNumber<T>();
            auto ctrl_amps = this->GetParams();
            auto ctrl_coefs = this->GetParams();
            param_cont_t params(ctrl_amps, ctrl_coefs, ctrl_phase);

            const T factor = random::RandomNumber<T>();
            ctrl_phase *= factor;
            Scale(ctrl_amps, factor);
            Scale(ctrl_coefs, factor);
            param_cont_t ctrl_params(ctrl_amps, ctrl_coefs, ctrl_phase);

            Scale(params, factor);
            const auto diffnorm = this->DiffNorm(params, ctrl_params);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(diffnorm, absval_t(0), 0, "Wrong diff.norm.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_Zero
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            T ctrl_phase = random::RandomNumber<T>();
            auto ctrl_amps = this->GetParams();
            auto ctrl_coefs = this->GetParams();
            param_cont_t params(ctrl_amps, ctrl_coefs, ctrl_phase);
            std::unique_ptr<param_cont_t> ctrl_zero = nullptr;
            if constexpr   (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T>>
                           )
            {
               ctrl_zero = std::make_unique<param_cont_t>(this->Mcr(), this->NModals());
            }
            else
            {
               ctrl_zero = std::make_unique<param_cont_t>(this->ParamsSize());
            }
            const auto diffnorm_nonzero = this->DiffNorm(params, *ctrl_zero);
            MPISYNC_UNIT_ASSERT(cutee::numeric::float_neq(diffnorm_nonzero, absval_t(0), 100), "Zero initially.");

            Zero(params);
            const auto diffnorm = this->DiffNorm(params, *ctrl_zero);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(diffnorm, absval_t(0), 0, "Non-zero diff.norm after Zero().");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_Axpy
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            param_cont_t y(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t x(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const T a = random::RandomNumber<T>();

            auto ctrl_phase = a*x.Phase() + y.Phase();
            auto ctrl_amps  = y.ClusterAmps();
            auto ctrl_coefs = y.LambdaCoefs();
            Axpy(ctrl_amps,  x.ClusterAmps(), a);
            Axpy(ctrl_coefs, x.LambdaCoefs(), a);
            const param_cont_t ctrl(ctrl_amps, ctrl_coefs, ctrl_phase);

            Axpy(y, x, a);
            const auto diffnorm = this->DiffNorm(y, ctrl);
            const auto refnorm  = Norm(ctrl);
            MPISYNC_UNIT_ASSERT_FZERO_PREC(diffnorm, refnorm, 1, "Non-zero diff.norm.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_SetShape
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            param_cont_t a;
            param_cont_t b;
            const Uin size_a = 2;
            const Uin size_b = 3;
            if constexpr   (  std::is_same_v<param_cont_t, ParamsTdvcc<T, GeneralTensorDataCont>>
                           || std::is_same_v<param_cont_t, ParamsTdvci<T, GeneralTensorDataCont>>
                           )
            {
               ModeCombiOpRange mcr_a(1, 2);
               mcr_a.Erase(std::vector<ModeCombi>{mcr_a.GetModeCombi(2)});
               ModeCombiOpRange mcr_b(1, 2);
               std::vector<Uin> nmodals = {2, 2};
               a=param_cont_t(mcr_a, nmodals);
               b=param_cont_t(mcr_b, nmodals);
            }
            else
            {
               a=param_cont_t(size_a);
               b=param_cont_t(size_b);
            }
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(a.SizeParams()), Uin(size_a), "Bef. SetShape(); a.SizeParams().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(a.Size()), Uin(1+2*size_a), "Bef. SetShape(); a.Size().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(b.SizeParams()), Uin(size_b), "Bef. SetShape(); b.SizeParams().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(b.Size()), Uin(1+2*size_b), "Bef. SetShape(); b.Size().");

            SetShape(a, b);
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(a.SizeParams()), Uin(size_b), "Aft. SetShape(); a.SizeParams().");
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(a.Size()), Uin(1+2*size_b), "Aft. SetShape(); a.Size().");
            MPISYNC_UNIT_ASSERT_EQUAL(a.SizeParams(), b.SizeParams(), "Aft. SetShape(); a,b SizeParams().");
            MPISYNC_UNIT_ASSERT_EQUAL(a.Size(), b.Size(), "Aft. SetShape(); a,b Size().");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_Norm
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const param_cont_t a(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const absval_t norm2 = Norm2(a);
            const absval_t norm = Norm(a);

            param_cont_t z = a;
            z.Zero();
            const absval_t ctrl_norm2 = this->DiffNorm2(a,z);
            const absval_t ctrl_norm = sqrt(ctrl_norm2);

            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(norm2, ctrl_norm2, 0, "Wrong norm2.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(norm, ctrl_norm, 0, "Wrong norm.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_Size
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const param_cont_t a(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(Size(a)), Uin(a.Size()), "Wrong Size(a).");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_OdeMeanNorm2
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const absval_t tol_abs = midas::util::AbsVal(random::RandomNumber<T>());
            const absval_t tol_rel = midas::util::AbsVal(random::RandomNumber<T>());

            const param_cont_t y_dlt(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t y_old(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t y_new(this->GetParams(), this->GetParams(), random::RandomNumber<T>());

            const absval_t ctrl = this->CtrlOdeMeanNorm2(y_dlt, tol_abs, tol_rel, y_old, y_new);
            const absval_t odemeannorm2 = OdeMeanNorm2(y_dlt, tol_abs, tol_rel, y_old, y_new);

            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(odemeannorm2, ctrl, 8, "Wrong OdeMeanNorm2.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_OdeMaxNorm2
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const absval_t tol_abs = midas::util::AbsVal(random::RandomNumber<T>());
            const absval_t tol_rel = midas::util::AbsVal(random::RandomNumber<T>());

            const param_cont_t y_dlt(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t y_old(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t y_new(this->GetParams(), this->GetParams(), random::RandomNumber<T>());

            const absval_t ctrl = this->CtrlOdeMaxNorm2(y_dlt, tol_abs, tol_rel, y_old, y_new);
            const absval_t odemaxnorm2 = OdeMaxNorm2(y_dlt, tol_abs, tol_rel, y_old, y_new);

            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(odemaxnorm2, ctrl, 2, "Wrong OdeMaxNorm2 for container type: '" + libmda::util::type_of<param_cont_t>() + "'.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_DataToPointer
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const param_cont_t params(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            GeneralMidasVector<absval_t> ctrl(this->SeparateRealImagParts(this->CompoundContainer(params)));
            const Uin size = (midas::type_traits::IsComplexV<T>? 2: 1) * params.Size();
            auto p = std::make_unique<absval_t[]>(size);
            DataToPointer(params, p.get());

            GeneralMidasVector<absval_t> result(size, p.get());

            absval_t diffnorm = GeneralMidasVector<absval_t>(result - ctrl).Norm();
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(diffnorm, absval_t(0), 0, "DataToPointer failed.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_DataFromPointer
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const param_cont_t a(this->GetParams(), this->GetParams(), random::RandomNumber<T>());

            const Uin size = (midas::type_traits::IsComplexV<T>? 2: 1) * a.Size();
            auto p = std::make_unique<absval_t[]>(size);
            DataToPointer(a, p.get());

            param_cont_t b;
            SetShape(b, a);
            DataFromPointer(b, p.get());

            const absval_t diffnorm = this->DiffNorm(a, b);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(diffnorm, absval_t(0), 0, "DataFromPointer failed.");
         }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template<typename T, template<typename> class CONT_T>
      struct ParamsTdvcc_DiffNorm2
         :  public ParamsTdvccBase<T,CONT_T>
      {
         using typename ParamsTdvccBase<T,CONT_T>::param_cont_t;
         using typename ParamsTdvccBase<T,CONT_T>::absval_t;
         void run() override
         {
            using namespace midas::test::mpi_sync;
            const param_cont_t a(this->GetParams(), this->GetParams(), random::RandomNumber<T>());
            const param_cont_t b(this->GetParams(), this->GetParams(), random::RandomNumber<T>());

            const absval_t diffnorm2 = ::DiffNorm2(a,b);
            const absval_t ctrl = this->DiffNorm2(a,b);

            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(diffnorm2, ctrl, 2, "Wrong diffnorm2.");
         }
      };


   } /* namespace params */

   namespace derivative
   {
      /*********************************************************************//**
       *
       ************************************************************************/
      template
         <  typename PARAM_T
         ,  template<typename> class CONT_T
         ,  bool IMAG_TIME
         >
      ParamsTdvcc<PARAM_T,CONT_T> CtrlDerivative
         (  const ParamsTdvcc<PARAM_T,CONT_T>& arParams
         ,  const std::vector<Uin>& arNModals
         ,  const OpDef& arOpDef
         ,  const ModalIntegrals<PARAM_T>& arModInts
         ,  const ModeCombiOpRange& arMcr
         )
      {
         using namespace midas::util::matrep;

         const auto ampls = OrganizeMcrSpaceVec(arNModals, arMcr, arParams.ClusterAmps());
         const auto coefs = OrganizeMcrSpaceVec(arNModals, arMcr, arParams.LambdaCoefs());

         // dot(phase) = err_0 = "E_VCC"
         // dot(s) = -i err-vec
         auto s_dot = TrfVccErrVec(arNModals, arOpDef, arModInts, arMcr, ampls);
         PARAM_T phase_dot = s_dot[0];
         s_dot[0] = 0;

         // dot(l) =  i (eta + lA)
         auto l_dot = TrfVccEtaVec(arNModals, arOpDef, arModInts, arMcr, ampls);
         l_dot += TrfVccLJac(arNModals, arOpDef, arModInts, arMcr, ampls, coefs);
         l_dot[0] = 0;

         if constexpr(!IMAG_TIME)
         {
            Scale(s_dot, PARAM_T(0,-1));
            Scale(l_dot, PARAM_T(0,+1));
         }
         else
         {
            phase_dot = 0;
            Scale(s_dot, PARAM_T(-1));
            Scale(l_dot, PARAM_T(-1));
         }

         if constexpr   (  std::is_same_v<CONT_T<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                        )
         {
            GeneralDataCont<PARAM_T> l_dot_dc(l_dot);
            CONT_T<PARAM_T> l_dot_tdc(l_dot_dc, arMcr, arNModals, true);
            GeneralDataCont<PARAM_T> s_dot_dc(s_dot);
            CONT_T<PARAM_T> s_dot_tdc(s_dot_dc, arMcr, arNModals, true);
            return ParamsTdvcc<PARAM_T,CONT_T>(s_dot_tdc, l_dot_tdc, phase_dot);
         }
         else
         {
            return ParamsTdvcc<PARAM_T,CONT_T>(s_dot, l_dot, phase_dot);
         }
      }

      /*********************************************************************//**
       *
       ************************************************************************/
      template
         <  typename PARAM_T
         ,  template<typename> class CONT_T
         ,  bool IMAG_TIME
         >
      ParamsTdvci<PARAM_T,CONT_T> CtrlDerivative
         (  const ParamsTdvci<PARAM_T,CONT_T>& arParams
         ,  const std::vector<Uin>& arNModals
         ,  const OpDef& arOpDef
         ,  const ModalIntegrals<PARAM_T>& arModInts
         ,  const ModeCombiOpRange& arMcr
         )
      {
         using namespace midas::util::matrep;

         const auto coefs = OrganizeMcrSpaceVec(arNModals, arMcr, arParams.Coefs());
         auto c_dot = TrfVci(arNModals, arOpDef, arModInts, arMcr, coefs);

         if constexpr(!IMAG_TIME)
         {
            Scale(c_dot, PARAM_T(0,-1));
         }
         else
         {
            Scale(c_dot, PARAM_T(-1));
         }

         if constexpr   (  std::is_same_v<CONT_T<PARAM_T>, GeneralTensorDataCont<PARAM_T>>
                        )
         {
            GeneralDataCont<PARAM_T> dc(c_dot);
            CONT_T<PARAM_T> c_dot_tdc(dc, arMcr, arNModals, true);
            return ParamsTdvci<PARAM_T,CONT_T>(std::move(c_dot_tdc));
         }
         else
         {
            return ParamsTdvci<PARAM_T,CONT_T>(std::move(c_dot));
         }
      }

      /*********************************************************************//**
       *
       ************************************************************************/
      template
         <  typename PARAM_T
         ,  template<typename> class CONT_T
         ,  template<typename, template<typename> class> class PARAM_CONT_T
         ,  bool IMAG_TIME
         >
      struct DerivativeTestBase
         :  public detail::TestSystemSetup<PARAM_T, detail::DoingVccV<PARAM_CONT_T>>
         ,  public cutee::test
      {
         public:
            using absval_t = midas::type_traits::RealTypeT<PARAM_T>;

            static constexpr bool doing_vcc = detail::DoingVccV<PARAM_CONT_T>;

            void run() override
            {
               using namespace midas::test::mpi_sync;
               // Random params
               {
                  const PARAM_CONT_T<PARAM_T,CONT_T> ctrl = CtrlDerivative<PARAM_T, CONT_T, IMAG_TIME>
                     (  this->ParamCont()
                     ,  this->NModals()
                     ,  this->Oper()
                     ,  this->ModInts()
                     ,  this->Mcr()
                     );

                  PARAM_CONT_T<PARAM_T,CONT_T> result = ComputeDerivative
                     (  this->ParamCont()
                     ,  this->NModals()
                     ,  this->Oper()
                     ,  this->ModInts()
                     ,  this->Mcr()
                     );

                  MPISYNC_UNIT_ASSERT_EQUAL(ctrl.Size(), this->ParamCont().Size(), "Wrong ctrl.Size().");
                  MPISYNC_UNIT_ASSERT_EQUAL(result.Size(), this->ParamCont().Size(), "Wrong result.Size().");
                  auto diff = result;
                  Axpy(diff, ctrl, -PARAM_T(1));
                  const auto diffnorm = Norm(diff);
                  const auto reldiffnorm = diffnorm/Norm(ctrl);
                  const auto min = std::min(diffnorm, reldiffnorm);
                  bool check = cutee::numeric::float_eq(min+1, decltype(min)(1), 20);
                  MPISYNC_UNIT_ASSERT_FZERO_PREC(min, Norm(ctrl), 10, "Wrong derivative with random amplitudes.");
               }

               // Random params with zero singles excitations
               {
                  const PARAM_CONT_T<PARAM_T,CONT_T> ctrl_zero_t1 = CtrlDerivative<PARAM_T, CONT_T, IMAG_TIME>
                     (  this->ParamContZeroT1()
                     ,  this->NModals()
                     ,  this->Oper()
                     ,  this->ModInts()
                     ,  this->Mcr()
                     );

                  PARAM_CONT_T<PARAM_T,CONT_T> result_zero_t1 = ComputeDerivative
                     (  this->ParamContZeroT1()
                     ,  this->NModals()
                     ,  this->Oper()
                     ,  this->ModInts()
                     ,  this->Mcr()
                     );

                  MPISYNC_UNIT_ASSERT_EQUAL(ctrl_zero_t1.Size(), this->ParamCont().Size(), "Wrong ctrl.Size().");
                  MPISYNC_UNIT_ASSERT_EQUAL(result_zero_t1.Size(), this->ParamCont().Size(), "Wrong result.Size().");

                  auto diff_zero_t1 = result_zero_t1;
                  Axpy(diff_zero_t1, ctrl_zero_t1, -PARAM_T(1));
                  const auto diffnorm_zero_t1 = Norm(diff_zero_t1);
                  const auto reldiffnorm_zero_t1 = diffnorm_zero_t1/Norm(ctrl_zero_t1);
                  const auto min_zero_t1 = std::min(diffnorm_zero_t1, reldiffnorm_zero_t1);
                  bool check_zero_t1 = cutee::numeric::float_eq(min_zero_t1+1, decltype(min_zero_t1)(1), 20);
                  MPISYNC_UNIT_ASSERT_FZERO_PREC(min_zero_t1, Norm(ctrl_zero_t1), 10, "Wrong derivative with random amplitudes (t1=0)");
               }
            }

            void setup() override
            {
               gOperatorDefs.ReSet();
            }
            void teardown() override
            {
               gOperatorDefs.ReSet();
            }


         protected:
            template<class... Args>
            DerivativeTestBase(Args&&... args)
               :  detail::TestSystemSetup<PARAM_T, doing_vcc>(std::forward<Args>(args)...)
               ,  mOperMcr(this->OperMcr().first)
               ,  mParamsMcr(this->ParamsMcr())
               ,  mOpDef(this->RandomOpDef(mOperMcr))
               ,  mModInts(this->RandomModInts(mOpDef))
               ,  mPhase(random::RandomNumber<PARAM_T>())
               ,  mClusterAmps(this->RandomParams(mParamsMcr.first))
               ,  mClusterAmpsZeroT1(this->RandomParams(mParamsMcr.first, true))
               ,  mLambdaCoefs(this->RandomParams(mParamsMcr.first))
            {
               util::ConstructParamsVecCont
                  (  mClusterAmpsVecT
                  ,  mParamsMcr.first
                  ,  mParamsMcr.second
                  ,  mClusterAmps
                  ,  this->NModals()
                  );
               util::ConstructParamsVecCont
                  (  mClusterAmpsZeroT1VecT
                  ,  mParamsMcr.first
                  ,  mParamsMcr.second
                  ,  mClusterAmpsZeroT1
                  ,  this->NModals()
                  );
               util::ConstructParamsVecCont
                  (  mLambdaCoefsVecT
                  ,  mParamsMcr.first
                  ,  mParamsMcr.second
                  ,  mLambdaCoefs
                  ,  this->NModals()
                  );
               mParamCont = this->SetupParamCont();
               mParamContZeroT1 = this->SetupParamContZeroT1();
            }

            static constexpr bool DoingVcc() {return doing_vcc;}
            const ModeCombiOpRange& Mcr() const {return mParamsMcr.first;}
            Uin SizeParams() const {return mParamsMcr.second;}
            const OpDef& Oper() const {return mOpDef;}
            const ModalIntegrals<PARAM_T>& ModInts() const {return mModInts;}
            const PARAM_T& Phase() const {return mPhase;}
            const CONT_T<PARAM_T>& ClusterAmps() const {return mClusterAmpsVecT;}
            const CONT_T<PARAM_T>& LambdaCoefs() const {return mLambdaCoefsVecT;}
            const PARAM_CONT_T<PARAM_T,CONT_T>& ParamCont() const {return mParamCont;}
            const PARAM_CONT_T<PARAM_T,CONT_T>& ParamContZeroT1() const {return mParamContZeroT1;}

            virtual PARAM_CONT_T<PARAM_T,CONT_T> ComputeDerivative
               (  const PARAM_CONT_T<PARAM_T,CONT_T>& arParams
               ,  const std::vector<Uin>& arNModals
               ,  const OpDef& arOpDef
               ,  const ModalIntegrals<PARAM_T>& arModInts
               ,  const ModeCombiOpRange& arMcr
               )  const = 0;

         private:
            ModeCombiOpRange mOperMcr;
            std::pair<ModeCombiOpRange,Uin> mParamsMcr;
            OpDef mOpDef;
            ModalIntegrals<PARAM_T> mModInts;
            PARAM_T mPhase;
            std::vector<std::vector<PARAM_T>> mClusterAmps;
            std::vector<std::vector<PARAM_T>> mClusterAmpsZeroT1;
            std::vector<std::vector<PARAM_T>> mLambdaCoefs;
            CONT_T<PARAM_T> mClusterAmpsVecT;
            CONT_T<PARAM_T> mClusterAmpsZeroT1VecT;
            CONT_T<PARAM_T> mLambdaCoefsVecT;
            PARAM_CONT_T<PARAM_T,CONT_T> mParamCont;
            PARAM_CONT_T<PARAM_T,CONT_T> mParamContZeroT1;

            PARAM_CONT_T<PARAM_T,CONT_T> SetupParamCont() const
            {
               if constexpr(std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,ParamsTdvcc<PARAM_T,CONT_T>>)
               {
                  return ParamsTdvcc<PARAM_T,CONT_T>(mClusterAmpsVecT, mLambdaCoefsVecT, mPhase);
               }
               else if constexpr(std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,ParamsTdvci<PARAM_T,CONT_T>>)
               {
                  return ParamsTdvci<PARAM_T,CONT_T>(mClusterAmpsVecT);
               }
               else
               {
                  static_assert(!std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,PARAM_CONT_T<PARAM_T,CONT_T>>, "Unexpected param_cont_t.");
               }
            }
            PARAM_CONT_T<PARAM_T,CONT_T> SetupParamContZeroT1() const
            {
               if constexpr(std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,ParamsTdvcc<PARAM_T,CONT_T>>)
               {
                  return ParamsTdvcc<PARAM_T,CONT_T>(mClusterAmpsZeroT1VecT, mLambdaCoefsVecT, mPhase);
               }
               else if constexpr(std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,ParamsTdvci<PARAM_T,CONT_T>>)
               {
                  return ParamsTdvci<PARAM_T,CONT_T>(mClusterAmpsZeroT1VecT);
               }
               else
               {
                  static_assert(!std::is_same_v<PARAM_CONT_T<PARAM_T,CONT_T>,PARAM_CONT_T<PARAM_T,CONT_T>>, "Unexpected param_cont_t.");
               }
            }
      };

      /*********************************************************************//**
       *
       ************************************************************************/
      template
         <  typename PARAM_T
         ,  template<typename> class CONT_T
         ,  template<typename, template<typename> class> class PARAM_CONT_T
         ,  template<typename> class TRF_T
         ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_T
         ,  bool IMAG_TIME
         >
      struct DerivativeTest
         :  public DerivativeTestBase<PARAM_T, CONT_T, PARAM_CONT_T, IMAG_TIME>
      {
         public:
            static constexpr bool doing_vcc = DerivativeTestBase<PARAM_T,CONT_T,PARAM_CONT_T,IMAG_TIME>::doing_vcc;

            template<class... Args>
            DerivativeTest(Args&&... args)
               :  DerivativeTestBase<PARAM_T, CONT_T, PARAM_CONT_T, IMAG_TIME>(std::forward<Args>(args)...)
            {
            }

            PARAM_CONT_T<PARAM_T,CONT_T> ComputeDerivative
               (  const PARAM_CONT_T<PARAM_T,CONT_T>& arParams
               ,  const std::vector<Uin>& arNModals
               ,  const OpDef& arOpDef
               ,  const ModalIntegrals<PARAM_T>& arModInts
               ,  const ModeCombiOpRange& arMcr
               )  const override
            {
               TRF_T<PARAM_T> trf(arNModals, arOpDef, arModInts, arMcr);
               if constexpr   (  std::is_same_v<TRF_T<PARAM_T>, TrfGeneralTimTdvci<PARAM_T>>
                              || std::is_same_v<TRF_T<PARAM_T>, TrfGeneralTimTdvcc<PARAM_T>>
                              )
               {
                  if constexpr(MPI_DEBUG)
                  {
                     midas::mpi::WriteToLog("Initialize V3 contribs on rank " + std::to_string(midas::mpi::GlobalRank()));
                  }
                  trf.V3cFilePrefix() = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
                  trf.InitializeContribs();
                  if constexpr(MPI_DEBUG)
                  {
                     midas::mpi::WriteToLog("Finished initializing V3 contribs on rank " + std::to_string(midas::mpi::GlobalRank()));
                  }
               }

               if constexpr   (  MPI_DEBUG
                              )
               {
                  std::ostringstream oss;
                  oss   << " DerivativeTest::ComputeDerivative(). Call deriv with params:\n"
                        << " Rank:        " << midas::mpi::GlobalRank() << "\n"
                        << " arParams:    " << arParams << "\n"
                        << " arMcr:       " << arMcr << "\n"
                        << " arNModals:   " << arNModals << "\n"
                        << std::flush;
                  midas::mpi::WriteToLog(oss.str());
               }

               DERIV_T<PARAM_T, CONT_T, TRF_T, IMAG_TIME> deriv;
               return deriv(C_0, arParams, trf);
            }
      };

   } /* namespace derivative */

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   PARAM_T CtrlExpVal
      (  const CONT_TMPL<PARAM_T>& arBra
      ,  const CONT_TMPL<PARAM_T>& arOperTrfKet
      ,  const bool aVcc
      )
   {
      GeneralMidasVector<PARAM_T> mv_bra(Size(arBra));
      GeneralMidasVector<PARAM_T> mv_ket(Size(arOperTrfKet));

      if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>, GeneralDataCont<PARAM_T>>)
      {
         arBra.DataIo(IO_GET, mv_bra, mv_bra.Size());
         arOperTrfKet.DataIo(IO_GET, mv_ket, mv_ket.Size());
      }
      else if constexpr (std::is_same_v<CONT_TMPL<PARAM_T>, GeneralTensorDataCont<PARAM_T>>)
      {
         auto dc_bra = DataContFromTensorDataCont(arBra);
         auto dc_ket = DataContFromTensorDataCont(arOperTrfKet);
         dc_bra.DataIo(IO_GET, mv_bra, mv_bra.Size());
         dc_ket.DataIo(IO_GET, mv_ket, mv_ket.Size());
      }
      else
      {
         mv_bra = arBra;
         mv_ket = arOperTrfKet;
      }

      MidasAssert(Size(mv_bra) == Size(mv_ket), "arBra/arOperTrfKet must have same size.");
      MidasAssert(Size(mv_bra) > 0, "Size(arBra) <= 0");

      PARAM_T val = 0;
      if (aVcc)
      {
         for(Uin i = 1; i < Size(mv_ket); ++i)
         {
            val += mv_bra[i] * mv_ket[i];
         }
         val += mv_ket[0];
      }
      else
      {
         for (Uin i = 0; i < Size(mv_ket); ++i)
         {
            val += midas::math::Conj(mv_bra[i])*mv_ket[i];
         }
         val /= Norm2(mv_bra);
      }
      return val;
   }

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  class TRF_T
      >
   PARAM_T CtrlExpVal
      (  midas::type_traits::RealTypeT<PARAM_T> aTime
      ,  const ParamsTdvcc<PARAM_T,CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      )
   {
      return CtrlExpVal(arState.LambdaCoefs(), arTrf.ExpValTrf(aTime, arState.ClusterAmps()), true);
   }

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  class TRF_T
      >
   PARAM_T CtrlExpVal
      (  midas::type_traits::RealTypeT<PARAM_T> aTime
      ,  const ParamsTdvci<PARAM_T,CONT_TMPL>& arState
      ,  const TRF_T& arTrf
      )
   {
      return CtrlExpVal(arState.Coefs(), arTrf.ExpValTrf(aTime, arState.Coefs()), false);
   }

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct TdvccTestBase
      :  public detail::TestSystemSetup
            <  PARAM_T
            ,  detail::DoingVccDerivV<DERIV_TMPL<PARAM_T,CONT_TMPL,TRF_TMPL,IMAG_TIME>>
            >
      ,  public cutee::test
   {
      public:
         void setup() override
         {
            gOperatorDefs.ReSet();
         }
         void teardown() override
         {
            gOperatorDefs.ReSet();
         }

      protected:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using cont_t = CONT_TMPL<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;
         static constexpr bool doing_vcc = detail::DoingVccDerivV<deriv_t>;
         
         template<class... Args>
         TdvccTestBase(Args&&... args)
            :  detail::TestSystemSetup<param_t, doing_vcc>(std::forward<Args>(args)...)
            ,  mOperMcr(this->OperMcr().first)
            ,  mParamsMcr(this->ParamsMcr())
            ,  mOpDef(this->RandomOpDef(mOperMcr))
            ,  mModInts(this->RandomModInts(mOpDef))
            ,  mPhase(random::RandomNumber<param_t>())
            ,  mClusterAmps(this->RandomParams(mParamsMcr.first))
            ,  mLambdaCoefs(this->RandomParams(mParamsMcr.first))
         {
            util::ConstructParamsVecCont
               (  mClusterAmpsVecT
               ,  mParamsMcr.first
               ,  mParamsMcr.second
               ,  mClusterAmps
               ,  this->NModals()
               );
            util::ConstructParamsVecCont
               (  mLambdaCoefsVecT
               ,  mParamsMcr.first
               ,  mParamsMcr.second
               ,  mLambdaCoefs
               ,  this->NModals()
               );
            mParamCont = SetupParamCont();
         }

         static constexpr bool DoingVcc() {return doing_vcc;}
         const ModeCombiOpRange& Mcr() const {return mParamsMcr.first;}
         Uin SizeParams() const {return mParamsMcr.second;}
         const OpDef& Oper() const {return mOpDef;}
         const ModalIntegrals<param_t>& ModInts() const {return mModInts;}
         const param_t& Phase() const {return mPhase;}
         const cont_t& ClusterAmps() const {return mClusterAmpsVecT;}
         const cont_t& LambdaCoefs() const {return mLambdaCoefsVecT;}
         const param_cont_t& ParamCont() const {return mParamCont;}

         virtual midas::ode::OdeInfo GetOdeInfo() const
         {
            midas::ode::OdeInfo ode_info;
            ode_info["DRIVER"] = std::string("MIDAS");
            ode_info["STEPPER"] = std::string("DOPR853");
            ode_info["IOLEVEL"] = 5;
            ode_info["TIMEINTERVAL"] = std::pair<Nb,Nb>(0, 1e-2);
            ::detail::ValidateOdeInput(ode_info);
            return ode_info;
         };

      private:
         ModeCombiOpRange mOperMcr;
         std::pair<ModeCombiOpRange,Uin> mParamsMcr;
         OpDef mOpDef;
         ModalIntegrals<param_t> mModInts;
         param_t mPhase;
         std::vector<std::vector<param_t>> mClusterAmps;
         std::vector<std::vector<param_t>> mLambdaCoefs;
         cont_t mClusterAmpsVecT;
         cont_t mLambdaCoefsVecT;
         param_cont_t mParamCont;

         param_cont_t SetupParamCont() const
         {
            if constexpr(DoingVcc())
            {
               return param_cont_t(mClusterAmpsVecT, mLambdaCoefsVecT, mPhase);
            }
            else
            {
               return param_cont_t(mClusterAmpsVecT);
            }
         }
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct InitialTest
      :  public TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>
   {
      public:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;

         template<class... Args>
         InitialTest(Args&&... args)
            :  TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>(std::forward<Args>(args)...)
         {
         }

         void run() override
         {
            using namespace midas::test::mpi_sync;
            tdvcc_t tdvcc(this->NModals(), this->Oper(), this->ModInts(), this->Mcr());
            tdvcc.InitializeTransformers();
            tdvcc.InitializeSizeParams(this->NModals());
            auto init_state = this->ParamCont();
            auto ode_info = this->GetOdeInfo();
            OdeIntegrator<tdvcc_t> integrator(&tdvcc, ode_info);
            integrator.Integrate(init_state);

            MPISYNC_UNIT_ASSERT(true, "This is just an initial test.");
         }
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct TdOperTest
      :  public TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>
   {
      public:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;
         using oper_t = typename tdvcc_t::oper_t;

         template<class... Args>
         TdOperTest(Args&&... args)
            :  TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>(std::forward<Args>(args)...)
         {
         }

         void run() override
         {
            using namespace midas::test::mpi_sync;
            const auto opdef = this->Oper();
            oper_t lincomboper;
            lincomboper.emplace_back(opdef, 1);
            lincomboper.emplace_back(opdef, [](step_t t)->param_t {return t;});
            std::vector<ModalIntegrals<param_t>> v_modints(lincomboper.size(), this->ModInts());
            tdvcc_t tdvcc(this->NModals(), lincomboper, std::move(v_modints), this->Mcr());
            tdvcc.InitializeTransformers();
            tdvcc.InitializeSizeParams(this->NModals());
            const auto params = this->ParamCont();
            const auto d0  = tdvcc.Derivative(step_t(0), params);
            auto dp1 = tdvcc.Derivative(step_t(+1), params);
            auto dm2 = tdvcc.Derivative(step_t(-2), params);

            Axpy(dp1, d0, -2);
            Axpy(dm2, d0, +1);
            const auto dn_dp1 = Norm(dp1);
            const auto dn_dm2 = Norm(dm2);
            MPISYNC_UNIT_ASSERT_EQUAL(dn_dp1+1, decltype(dn_dp1)(1), "Wrong diffnorm dn_dp1");
            MPISYNC_UNIT_ASSERT_EQUAL(dn_dm2+1, decltype(dn_dm2)(1), "Wrong diffnorm dn_dm2");
         }
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct ExpValTest
      :  public TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>
   {
      public:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;
         using oper_t = typename tdvcc_t::oper_t;

         template<class... Args>
         ExpValTest(Args&&... args)
            :  TdvccTestBase<PARAM_T, CONT_TMPL, TRF_TMPL, DERIV_TMPL, IMAG_TIME>(std::forward<Args>(args)...)
         {
         }

         void run() override
         {
            using namespace midas::test::mpi_sync;
            trf_t trf(this->NModals(), this->Oper(), this->ModInts(), this->Mcr());
            if constexpr   (  std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvci<PARAM_T>>
                           || std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvcc<PARAM_T>>
                           )
            {
               trf.V3cFilePrefix() = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
               trf.InitializeContribs();
            }
            const auto state = this->ParamCont();
            const step_t time = 0;

            // Control.
            const auto ctrl = CtrlExpVal(time, state, trf);
            
            // Result.
            const auto result = ExpVal(time, state, trf);

            // If imag parts are vanishing compared to real parts, only compare real parts
            if (  cutee::numeric::float_numeq_zero(std::imag(result), std::real(result), 300)
               && cutee::numeric::float_numeq_zero(std::imag(ctrl), std::real(ctrl), 300)
               )
            {
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(std::real(result), std::real(ctrl), 12, "Bad exp. val (real)");
            }
            // Else, if real parts are vanishing compared to imag parts, only compare imag parts
            else if  (  cutee::numeric::float_numeq_zero(std::real(result), std::imag(result), 100)
                     && cutee::numeric::float_numeq_zero(std::real(ctrl), std::imag(ctrl), 100)
                  )
            {
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(std::imag(result), std::imag(ctrl), 10, "Bad exp. val (imag)");
            }
            // Else, compare full complex number
            else
            {
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(result, ctrl, 12, "Bad exp. val.");
            }
         }
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   struct WaterPotBase
   {
      public:
         void RunVcc()
         {
            // Setup.
            mpHam = std::make_unique<OpDef>(midas::test::util::WaterH0_ir_h2o_vci(mCoup,true,false));
            mpBas = std::make_unique<BasDef>(midas::test::util::HoBasis(*mpHam, mNPrims, false));
            mpOmi = std::make_unique<OneModeInt>(midas::test::util::SetupOneModeInts(*mpHam, *mpBas));
            mpCalcDef = std::make_unique<VccCalcDef>(midas::test::util::SetupVccCalcDef(std::vector<In>(mpHam->NmodesInOp(),mNModals), mMaxExci, true));
            mVecNModals = std::vector<Uin>(mpCalcDef->GetNModals().begin(), mpCalcDef->GetNModals().end());

            // VSCF.
            mpVcc = std::make_unique<Vcc>(mpCalcDef.get(), mpHam.get(), mpBas.get(), mpOmi.get());
            midas::test::util::RunVscf(*mpVcc);

            if (mpVcc->Converged())
            {
               midas::test::util::RunVcc(*mpVcc);
               Mout << std::endl;
               mMcr = mpVcc->GsCalcMcr();
            }
         }

      protected:
         const OpDef& GetHam() const {if(!mpHam){MIDASERROR("mpHam = nullptr");} return *mpHam;}
         const BasDef& GetBas() const {if(!mpBas){MIDASERROR("mpBas = nullptr");} return *mpBas;}
         const OneModeInt& GetOmi() const {if(!mpOmi){MIDASERROR("mpOmi = nullptr");} return *mpOmi;}
         const VccCalcDef& GetCalcDef() const {if(!mpCalcDef){MIDASERROR("mpCalcDef = nullptr");} return *mpCalcDef;}
         const Vcc& GetVcc() const {if(!mpVcc){MIDASERROR("mpVcc = nullptr");} return *mpVcc;}
         const std::vector<Uin>& NModals() const {return mVecNModals;}
         const ModeCombiOpRange& Mcr() const {return mMcr;}

      private:
         Uin mCoup = 2;
         Uin mMaxExci = 2;
         Uin mNPrims = 10;
         Uin mNModals = 3;

         std::unique_ptr<OpDef> mpHam;
         std::unique_ptr<BasDef> mpBas;
         std::unique_ptr<OneModeInt> mpOmi;
         std::unique_ptr<VccCalcDef> mpCalcDef;
         std::unique_ptr<Vcc> mpVcc;
         std::vector<Uin> mVecNModals;
         ModeCombiOpRange mMcr;
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct WaterPotVccTestBase
      :  public WaterPotBase
      ,  public cutee::test
   {
      public:
         using param_t = PARAM_T;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;
         using oper_t = typename tdvcc_t::oper_t;

         void setup() override
         {
            gOperatorDefs.ReSet();
            this->RunVcc();
            if(!GetVcc().Converged()){MIDASERROR("VSCF not converged.");}
            if(!GetVcc().GsCalcConverged()){MIDASERROR("VCC not converged.");}
            mMi = midas::vcc::modalintegrals::detail::ConvertFromReal<param_t>::Convert(*GetVcc().pIntegrals());
         }
         void teardown() override
         {
            gOperatorDefs.ReSet();
         }

      protected:
         const ModalIntegrals<param_t>& GetMi() const {return mMi;}

         virtual midas::ode::OdeInfo GetOdeInfo() const
         {
            midas::ode::OdeInfo ode_info;
            ode_info["DRIVER"] = std::string("MIDAS");
            ode_info["STEPPER"] = std::string("DOPR853");
            ode_info["IOLEVEL"] = 5;
            ode_info["TIMEINTERVAL"] = std::pair<Nb,Nb>(0, 1.0e+02);
            ::detail::ValidateOdeInput(ode_info);
            return ode_info;
         };

      private:
         ModalIntegrals<param_t> mMi;
   };

   /*********************************************************************//**
    *
    ************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
      ,  bool IMAG_TIME
      >
   struct WaterPotHExpValTest
      :  public WaterPotVccTestBase<PARAM_T,CONT_TMPL,TRF_TMPL,DERIV_TMPL,IMAG_TIME>
   {
      public:
         using param_t = PARAM_T;
         using real_t = midas::type_traits::RealTypeT<param_t>;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using trf_t = TRF_TMPL<param_t>;
         using deriv_t = DERIV_TMPL<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>;
         using param_cont_t = typename deriv_t::param_cont_t;
         using tdvcc_t = Tdvcc<param_cont_t,trf_t,deriv_t>;
         using oper_t = typename tdvcc_t::oper_t;

         void run() override
         {
            using namespace midas::test::mpi_sync;
            MPISYNC_UNIT_ASSERT(this->GetVcc().Converged(), "VSCF not converged.");
            MPISYNC_UNIT_ASSERT(this->GetVcc().GsCalcConverged(), "VCC not converged.");
            const real_t E_vscf = this->GetVcc().GetEtot();
            [[maybe_unused]] const real_t E_vcc = this->GetVcc().GsFinalEnergy();

            trf_t trf(this->NModals(), this->GetHam(), this->GetMi(), this->Mcr());
            if constexpr   (  std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvci<PARAM_T>>
                           || std::is_same_v<TRF_TMPL<PARAM_T>, TrfGeneralTimTdvcc<PARAM_T>>
                           )
            {
               trf.V3cFilePrefix() = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/";
               trf.InitializeContribs();
            }
            tdvcc_t tdvcc(this->NModals(), this->GetHam(), this->GetMi(), this->Mcr());
            tdvcc.InitializeTransformers();
            tdvcc.InitializeSizeParams(this->NModals());

            // Setup initial state.
            // For VCC: all zeroes
            // For VCI: [1,0,0,...] (1 for the ref. state)
            auto init_state = tdvcc.ShapedZeroVector();
            if constexpr
               (  std::is_same_v
                  <  deriv_t
                  ,  DerivTdvci<param_t, CONT_TMPL, TRF_TMPL, IMAG_TIME>
                  >
               )
            {
               if constexpr   (  std::is_same_v<CONT_TMPL<param_t>, GeneralTensorDataCont<param_t>>
                              )
               {
                  auto newcoefs = init_state.Coefs();
                  newcoefs.GetModeCombiData(I_0) = NiceScalar<param_t>(1.0);
                  init_state = decltype(init_state)(newcoefs);
               }
               else
               {
                  GeneralMidasVector<param_t> vci(init_state.Size(), param_t(0));
                  if (vci.Size() > 0)
                  {
                     vci[0] = param_t(1);
                  }
                  init_state = decltype(init_state)(CONT_TMPL<param_t>(vci));
               }
            }

            auto ode_info = this->GetOdeInfo();
            auto t_int = ode_info.template get<std::pair<Nb,Nb>>("TIMEINTERVAL");
            auto t_beg = t_int.first;
            auto t_end = t_int.second;

            const param_t exp_val_beg = ExpVal(t_int.first, init_state, trf);
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(std::real(exp_val_beg), E_vscf, 2, "Wrong Re[E(t_beg)].");
            MPISYNC_UNIT_ASSERT_FZERO_PREC(std::imag(exp_val_beg), E_vscf, 1, "Wrong Im[E(t_beg)].");

            OdeIntegrator<tdvcc_t> integrator(&tdvcc, ode_info);
            integrator.Integrate(init_state);
            const param_t exp_val_end = ExpVal(t_int.second, init_state, trf);

            if constexpr(IMAG_TIME)
            {
               // Takes too long for IMAG_TIME to converge to VCC ground
               // state, and it's already tested in some of the test_suite
               // tests, so just check here that it is lower than the
               // starting energy.
               // Also for VCI, it makes no sense to compare with VCC energy.
               MPISYNC_UNIT_ASSERT(std::real(exp_val_end) < E_vscf, "Re[E(t_end)] is not < E_vscf.");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(std::imag(exp_val_end), real_t(0), 0, "Wrong Im[E(t_end)].");
            }
            else
            {
               // Check conservation of energy.
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(std::real(exp_val_end), E_vscf, 10, "Wrong Re[E(t_end)].");
               MPISYNC_UNIT_ASSERT_FZERO_PREC(std::imag(exp_val_end), E_vscf, 10, "Wrong Im[E(t_end)].");
            }
         }

      private:
   };

} /* namespace midas::test::tdvcc */

#endif/*TDVCCTEST_H_INCLUDED*/
