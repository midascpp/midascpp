
/**
************************************************************************
* 
* @file                BaseTensorTest.h
*
* Created:             16-12-2014
*
* Author:              Sergio A. Losilla (sergio@chem.au.dk)
*
* Short Description:   Unit tests for BaseTensor and derived classes.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASETENSORTEST_H_INCLUDED
#define BASETENSORTEST_H_INCLUDED

#include<complex>

#include "test/CuteeInterface.h"

#include "tensor/NiceTensor.h"

namespace midas
{
namespace test
{

/**
 *
 **/
template<class T>
struct SimpleTensorTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorAdditionAssignmentTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorAdditionTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorSubtractionAssignmentTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorSubtractionTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};


/**
 *
 **/
template<class T>
struct SimpleTensorSimpleContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorVerySimpleMatrixContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorSimpleMatrixContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorSimpleContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};


/**
 *
 **/
template<class T>
struct SimpleTensorGeneralContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorGeneralContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleTensorGeneralContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template
   <  typename T
   >
struct SimpleTensorSpecialContractionsTest
   :  public virtual cutee::test
{
   void actual_test
      (
      );

   void run
      (
      )  override
   {
      this->actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorGeneralContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorGeneralContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct SimpleVsCanonicalTensorGeneralContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template
   <  class T
   >
struct SimpleTensorIoTest
   :  public virtual cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorRankZeroTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorContractDown2DTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct DecompositionTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Check that a 2D SimpleTensor can be decomposed to CanonicalTensor form.
 **/
template<class T>
struct CanonicalDecomposition2DTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that scaling a SimpleTensor with a scalar works. 
 **/
template<class T>
struct SimpleTensorScaleTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that zeroing a SimpleTensor works. 
 **/
template<class T>
struct SimpleTensorZeroTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot product works for two SimpleTensors.
 **/
template<class T>
struct SimpleTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot product works for two complex SimpleTensors.
 **/
template<class T>
struct ComplexSimpleTensorDotProductTest : public virtual cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot product works for two complex CanonicalTensors.
 **/
template<class T>
struct ComplexCanonicalTensorDotProductTest : public virtual cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for two SimpleTensors.
 **/
template<class T>
struct SimpleTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for a CanonicalTensor being added to a SimpleTensor.
 **/
template<class T>
struct SimpleVsCanonicalTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for a ZeroTensor being added to a SimpleTensor.
 **/
template<class T>
struct SimpleVsZeroTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that scaling a CanonicalTensor with a scalar works. 
 **/
template<class T>
struct CanonicalTensorScaleTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that balancing a CanonicalTensor works. 
 **/
template<class T>
struct CanonicalTensorBalanceTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorToSimpleTensorTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot product works for two CanonicalTensors.
 **/
template<class T>
struct CanonicalTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct CanonicalTensorDumpIntoTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T>
struct CanonicalTensorAddVectorsTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot_product(CanonicalTensor,SimpleTensor) works.
 **/
template<class T>
struct CanonicalVsSimpleTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for two CanonicalTensors.
 **/
template<class T>
struct CanonicalTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for a ZeroTensor being added to a CanonicalTensor.
 **/
template<class T>
struct CanonicalVsZeroTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot product works for two ZeroTensors.
 **/
template<class T>
struct ZeroTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot_product(ZeroTensor,SimpleTensor) works.
 **/
template<class T>
struct ZeroVsSimpleTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that dot_product(ZeroTensor,CanonicalTensor) works.
 **/
template<class T>
struct ZeroVsCanonicalTensorDotProductTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test that axpy works for two ZeroTensors.
 **/
template<class T>
struct ZeroTensorAxpyTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test construction of ZeroTensor, also implicitly tests Norm() function.
 **/
template<class T>
struct ZeroTensorConstructionTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test ContractDown for ZeroTensor.
 **/
template<class T>
struct ZeroTensorContractDownTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test ContractForward for ZeroTensor.
 **/
template<class T>
struct ZeroTensorContractForwardTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test ContractForward for ZeroTensor.
 **/
template<class T>
struct ZeroTensorContractUpTest : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test diff_norm
 **/
template<class T>
struct DiffNormTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};


/**
 * Test construction of gamma matrix for "square" tensor
 **/
template<class T>
struct GammaMatrixTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};


/**
 * Test construction of gamma matrix for general dimension tensor
 **/
template<class T>
struct GeneralGammaMatrixTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * Test construction/norm of TensorDirectProduct of canonical tensors
 **/
template<class T>
struct TensorDirectProductTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * Test Dot product of two TensorDirectProducts
 **/
template<class T>
struct TensorDirectProductDotTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * Check dot between TensorDirectProduct and CanonicalTensor
 **/
template<class T>
struct TensorDirectProductVsCanonicalTensorDotTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct TensorSumAdditionAssignmentTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct TensorSumSubtractionAssignmentTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct TensorSumDotTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct TensorSumDotCoefTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 *
 **/
template<class T> 
struct TensorDirectProductCanonicalRecompressTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * 
 **/
template<class T>
struct DotIndexCreatorTest : public cutee::test
{
   void actual_test();

   void run()
   {
      actual_test();
   }
};

/**
 * Test construction of NiceTensor from a BaseTensor<T>::typeID giving the type
 * of the BaseTensor<T> contained within the NiceTensor.
 **/
template<class T>
struct NiceTensorFromTypeID : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

/**
 * Test construction of a unit NiceTensor (one element 1, rest is 0) from a
 * BaseTensor<T>::typeID giving the type of the BaseTensor<T> contained within
 * the NiceTensor.
 **/
template<class T>
struct UnitNiceTensorFromTypeID : public cutee::test
{
   void actual_test();

   void run() 
   {
      actual_test();
   }
};

} /* namespace test */
} /* namespace midas */

#include "BaseTensorTest_tests.h"

#endif /* BASETENSORTEST_H_INCLUDED */
