/**
************************************************************************
* 
* @file                TensorDataContTest.h
*
* Created:             24-11-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Unit tests for TensorDataCont.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORDATACONTTEST_H_INCLUDED
#define TENSORDATACONTTEST_H_INCLUDED

#include<complex>

#include "inc_gen/Const.h"
#include "vcc/TensorDataCont.h"
#include "vcc/VccStateSpace.h"
#include "tensor/SimpleTensor.h"
#include "test/Random.h"
#include "test/CuteeInterface.h"


namespace midas::test
{

//Utility functions used in the testing:
namespace detail
{
   std::vector<unsigned int> TestDimensionVector();   ///< Holds tensor dims. used in testing.
   NiceTensor<Nb> TestNiceTensor1D(char aSwitch = 'a');     ///< 1-dim. NiceTensor for test cases.
   NiceTensor<Nb> TestNiceTensor2D(char aSwitch = 'a');     ///< 2-dim. NiceTensor for test cases.
   NiceTensor<Nb> TestNiceTensor3D(char aSwitch = 'a');     ///< 3-dim. NiceTensor for test cases.
   NiceTensor<Nb> TestNiceTensorZero(unsigned int aNdims);  ///< N-dim. NiceTensor with ZeroTensor.
   ModeCombiOpRange MakeMCR(In exci, In modes, const std::vector<In>& nmodals);
   ModeCombiOpRange MakeMCRNoRef(const ModeCombiOpRange&);
}  /* namespace detail */

/**
 * Checks that a TensorDataCont can be constructed from an integer, giving the
 * size. The NiceTensor%s of the resulting TensorDataCont should only hold
 * nullptr%s.
 **/
struct ConstructFromInt: public cutee::test
{
   void run() 
   {
      TensorDataCont tdc(I_2);
      UNIT_ASSERT_EQUAL(tdc.Size(), I_2, "Wrong size of TensorDataCont.");
      UNIT_ASSERT(tdc.GetModeCombiData(I_0).IsNullPtr(), "NiceTensor of TensorDataCont should contain nullptr but doesn't.");
      UNIT_ASSERT(tdc.GetModeCombiData(I_1).IsNullPtr(), "NiceTensor of TensorDataCont should contain nullptr but doesn't.");
   }
};

/**
 * Checks that a TensorDataCont can be constructed from an integer, giving the
 * size, and that the NiceTensor%s of the TensorDataCont can then be
 * copy-assigned.
 **/
struct ConstructFromIntAndAssignTensors: public cutee::test
{
   void run() 
   {
      //Set size of TDC and assign tensors:
      TensorDataCont tdc(I_2);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D();
      tdc.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D();

      //Check that the NiceTensors are not nullptrs:
      UNIT_ASSERT(!tdc.GetModeCombiData(I_0).IsNullPtr(), "NiceTensor contains nullptr.");
      UNIT_ASSERT(!tdc.GetModeCombiData(I_1).IsNullPtr(), "NiceTensor contains nullptr.");

      //Check some of the elements and the norms:
      //(Can only access elements if it's a SimpleTensor.)
      SimpleTensor<Nb>* st_ptr0 = dynamic_cast<SimpleTensor<Nb>*>(tdc.GetModeCombiData(I_0).GetTensor());
      SimpleTensor<Nb>* st_ptr1 = dynamic_cast<SimpleTensor<Nb>*>(tdc.GetModeCombiData(I_1).GetTensor());
      UNIT_ASSERT(static_cast<bool>(st_ptr0),"Failed dynamic_cast<SimpleTensor<Nb>*>(BaseTensor<Nb>*).");
      UNIT_ASSERT(static_cast<bool>(st_ptr1),"Failed dynamic_cast<SimpleTensor<Nb>*>(BaseTensor<Nb>*).");

      UNIT_ASSERT_FEQUAL(st_ptr0->GetData()[1], 7.1252228750e-2, "Wrong element value in tensor 0.");
      UNIT_ASSERT_FEQUAL(st_ptr1->GetData()[4], 2.3364559190e+0, "Wrong element value in tensor 1.");
      UNIT_ASSERT_FEQUAL(tdc.GetModeCombiData(I_0).Norm(), 3.076758195344850, "Wrong norm in tensor 0.");
      UNIT_ASSERT_FEQUAL(tdc.GetModeCombiData(I_1).Norm(), 4.950744060632982, "Wrong norm in tensor 1.");
   }
};

/**
 * Initialize TensorDataCont of specific shape filled with zeros
 */
struct ConstructToShapeWithZeros : public cutee::test
{
   void run() 
   {
      // Initialize dummy MCR and set modal limits
      std::vector<In> nmodals = { 5, 5, 5 };
      ModeCombiOpRange mcr = detail::MakeMCR( 3, 3, nmodals); 

      // Construct TensorDataCont (without reference state)
      TensorDataCont tdc(mcr, nmodals);

      // Check all elements zero
      UNIT_ASSERT_FEQUAL(tdc.Norm(), C_0, "Elements different from zero")
   }
};

/**
 * Construct a TensorDataCont from a VccStateSpace and a BaseTensor<T>::typeID
 * with all NiceTensor%s being a zero tensor of the required type.
 */
struct ConstructZeroFromVSSAndTypeID : public cutee::test
{
   void run() 
   {
      // Initialize dummy MCR and set number of modals per mode (incl.
      // reference state).
      std::vector<In> n_modals = { 4, 5, 7 };
      In max_exc_level = 3;
      In n_modes = n_modals.size();

      ModeCombiOpRange mcr_a = detail::MakeMCR( 3, 3, n_modals); 
      ModeCombiOpRange mcr_b = detail::MakeMCRNoRef(mcr_a);

      // Vectors of pairs for looping in the test.
      // - of typeID and corresponding string.
      std::vector<std::pair<typename BaseTensor<Nb>::typeID, std::string> > v_type{
         {BaseTensor<Nb>::typeID::SIMPLE,    "SimpleTensor"},
         {BaseTensor<Nb>::typeID::CANONICAL, "CanonicalTensor"},
         {BaseTensor<Nb>::typeID::ZERO,      "ZeroTensor"}
      };
      // - of true/false (incl./excl. ref. state).
      std::vector<std::pair<bool, std::string> > v_exclref{
         {false,"EraseRef = false"},
         {true, "EraseRef = true"}
      };
      // - of MCRs containing/not containing a reference state.
      std::vector<std::pair<const ModeCombiOpRange&, std::string> > v_mcr{
         {mcr_a, "MCR with ref. state."},
         {mcr_b, "MCR without ref. state."}
      };

      // We test that
      //    -  types are correct.
      //    -  size is right.
      //    -  norm of TDC is zero.
      for(const auto& type: v_type)
      {
         for(const auto& exclref: v_exclref)
         {
            for(const auto& mcr: v_mcr)
            {
               // An error information string.
               std::string info = " ("+type.second+", "+exclref.second+", "+mcr.second+")";

               // Construct VccStateSpace, then TensorDataCont.
               VccStateSpace  vss(mcr.first, n_modals, exclref.first);
               TensorDataCont tdc(vss, type.first);

               // Check type for every NiceTensor.
               for(In i = I_0; i < tdc.Size(); ++i)
               {
                  const auto& nt = tdc.GetModeCombiData(i);
                  Uin val = static_cast<Uin>(nt.Type());
                  Uin exp = static_cast<Uin>(nt.NDim()==I_0? BaseTensor<Nb>::typeID::SCALAR: type.first);
                  UNIT_ASSERT_EQUAL(val, exp, "Wrong type for i = "+std::to_string(i)+"."+info);
               }

               // Check that size (number of NiceTensors) is right.
               Uin exp_size = vss.NumMCs();
               UNIT_ASSERT_EQUAL(static_cast<Uin>(tdc.Size()), exp_size, "Wrong size."+info);

               // Check that TDC norm is zero.
               UNIT_ASSERT_FEQUAL(tdc.Norm(), C_0, "Non-zero norm."+info);
            }
         }
      }
   }
};

/**
 * Construct a TensorDataCont from a VccStateSpace and a BaseTensor<T>::typeID
 * with all NiceTensor%s being a zero tensor of the required type, except for
 * one which is a unit NiceTensor.
 */
struct ConstructUnitFromVSSAndTypeID : public cutee::test
{
   void run() 
   {
      // Initialize dummy MCR and set number of modals per mode (incl.
      // reference state).
      std::vector<In> n_modals = { 4, 5, 7 };
      In max_exc_level = 3;
      In n_modes = n_modals.size();

      ModeCombiOpRange mcr_a = detail::MakeMCR( 3, 3, n_modals); 
      ModeCombiOpRange mcr_b = detail::MakeMCRNoRef(mcr_a);

      // In vector/DataCont form, the MCR has the following amount of elements:
      // (Remember: the actual dimensions of the tensors are (3, 4, 6), not 
      // (4, 5, 7), because 1 is subtracted for the reference state.
      //
      //    Reference (scalar)      1  Addresses:  0
      //    1-mode part             3              1
      //                            4              4
      //                            6              8
      //    2-mode part           3*4             14
      //                          3*6             26
      //                          4*6             44
      //    3-mode part         3*4*6             68
      //    -------------------------
      //                          140
      //    -1 without ref.       139
      // Let index define the 1-position of the DataCont (rest is 0).
      // index = 42 is then the (2,4) element in the [0,2] 2-mode combination.
      // DataCont ref. Or the (2,5) element if ref. state is not included.
      const In ref_dc_size  = 140;
      const In ref_dc_index = 42;
      const std::vector<unsigned> ref_vec_dims{3,6};
      const std::vector<unsigned> ref_vec_index_incl{2,4};
      const std::vector<unsigned> ref_vec_index_excl{2,5};

      // Vectors of pairs for looping in the test.
      // - of typeID and corresponding string.
      std::vector<std::pair<typename BaseTensor<Nb>::typeID, std::string> > v_type{
         {BaseTensor<Nb>::typeID::SIMPLE,    "SimpleTensor"},
         {BaseTensor<Nb>::typeID::CANONICAL, "CanonicalTensor"},
      };
      // - of true/false (incl./excl. ref. state).
      std::vector<std::pair<bool, std::string> > v_exclref{
         {false,"EraseRef = false"},
         {true, "EraseRef = true"}
      };
      // - of MCRs containing/not containing a reference state.
      std::vector<std::pair<const ModeCombiOpRange&, std::string> > v_mcr{
         {mcr_a, "MCR with ref. state."},
         {mcr_b, "MCR without ref. state."}
      };

      // We test that
      //    -  types are correct.
      //    -  size is right.
      //    -  norm of TDC is unit.
      //    -  difference norm with reference from DataCont is zero.
      //    -  difference norm of unit NiceTensor with reference is zero.
      for(const auto& type: v_type)
      {
         for(const auto& exclref: v_exclref)
         {
            for(const auto& mcr: v_mcr)
            {
               // An error information string.
               std::string info = " ("+type.second+", "+exclref.second+", "+mcr.second+")";

               // Construct VccStateSpace, then TensorDataCont.
               VccStateSpace  vss(mcr.first, n_modals, exclref.first);
               TensorDataCont tdc(vss, ref_dc_index, type.first);

               // Check type for every NiceTensor.
               for(In i = I_0; i < tdc.Size(); ++i)
               {
                  const auto& nt = tdc.GetModeCombiData(i);
                  Uin val = static_cast<Uin>(nt.Type());
                  Uin exp = static_cast<Uin>(nt.NDim()==I_0? BaseTensor<Nb>::typeID::SCALAR: type.first);
                  UNIT_ASSERT_EQUAL(val, exp, "Wrong type for i = "+std::to_string(i)+"."+info);
               }

               // Check that size (number of NiceTensors) is right.
               Uin exp_size = vss.NumMCs();
               UNIT_ASSERT_EQUAL(static_cast<Uin>(tdc.Size()), exp_size, "Wrong size."+info);

               // Check that TDC norm is one.
               UNIT_ASSERT_FEQUAL(tdc.Norm(), C_1, "Non-unit norm."+info);

               // Determine whether the reference state has been excluded
               // (either in VSS construction or in the MCR). Make a reference
               // unit DataCont and NiceTensor.
               bool excluded_ref = (vss.NumEmptyMCs() == I_0);
               DataCont ref_dc(ref_dc_size - static_cast<Uin>(excluded_ref));
               ref_dc.SetToUnitVec(ref_dc_index);
               const std::vector<unsigned int>& index =  excluded_ref 
                                                      ?  ref_vec_index_excl
                                                      :  ref_vec_index_incl;
               NiceTensor<Nb> ref_nt(ref_vec_dims, index, BaseTensor<Nb>::typeID::SIMPLE);

               // Compare full TDC.
               // (The following 'if construct' is some ugly shit to account
               // for the TensorDataCont(DataCont&,...) constructor being
               // flawed, in that it does not handle the case "incl. ref. state
               // = false, but MCR doesn't contain ref. state" properly. We
               // have to modify the incl. ref. state bool so that it's true if
               // the MCR doesn't contain an empty MC. Otherwise the TDC
               // constructor tries to exclude an MC that's not there.)
               bool actual_exclref = (exclref.first && mcr.first.NumEmptyMCs() != I_0);
               TensorDataCont ref_tdc(ref_dc, mcr.first, n_modals, !actual_exclref);
               ref_tdc.Axpy(tdc, -C_1);
               UNIT_ASSERT_FEQUAL(ref_tdc.Norm(), C_0, "Difference norm is non-zero."+info);

               // Compare non-zero NiceTensor.
               Uin unit_nt_refnum = vss.LocateAddress(ref_dc_index);
               unit_nt_refnum -= (!exclref.first ? I_0 : vss.NumEmptyMCs());
               const auto& nt = tdc.GetModeCombiData(unit_nt_refnum);
               ref_nt.Axpy(nt, -C_1);
               UNIT_ASSERT_FEQUAL(ref_nt.Norm(), C_0, "Non-zero diff. norm with NiceTensor."+info);
            }
         }
      }
   }
};

/**
 * Tests that the TensorDataCont::operator+=(const TensorDataCont&) operator
 * overload works correctly.
 **/
struct ElementwiseAdditionTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataConts and set elements
      TensorDataCont tdc1(I_2);
      tdc1.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc1.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      
      TensorDataCont tdc2(I_2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('b');
      tdc2.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('b');

      // Do tdc1 += tdc2 and check result
      tdc1+=tdc2;
      UNIT_ASSERT_FEQUAL(tdc1.Norm(), 6.904047646531736, "Bug in += operator overload for TensorDataCont" );
   }
};

/**
 * Tests that the TensorDataCont::operator-=(const TensorDataCont&) overload
 * works correctly.
 **/
struct ElementwiseSubtractionTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataConts and set elements
      TensorDataCont tdc1(I_2);
      tdc1.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc1.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      
      TensorDataCont tdc2(I_2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('b');
      tdc2.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('b');

      // Do tdc1 -= tdc2 and check result
      tdc1-=tdc2;
      UNIT_ASSERT_FEQUAL(tdc1.Norm(), 7.939816638785807, "Bug in -= operator overload for TensorDataCont" );
   }
};

/**
 * Tests that the *= operator overload for TensorDataCont works correctly
 **/
struct ElementwiseProductTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataConts and set elements
      TensorDataCont tdc1(I_2);
      tdc1.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc1.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      
      TensorDataCont tdc2(I_2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('b');
      tdc2.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('b');

      // Do tdc1 *= tdc2 and check result
      tdc1*=tdc2;
      UNIT_ASSERT_FEQUAL(tdc1.Norm(), 7.327466442655832, "Bug in *= operator overload for TensorDataCont" );
   }
};

/**
 * Tests that the /= operator overload for TensorDataCont works correctly
 **/
struct ElementwiseDivisionTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataConts and set elements
      TensorDataCont tdc1(I_2);
      tdc1.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc1.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      
      TensorDataCont tdc2(I_2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('b');
      tdc2.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('b');

      // Do tdc1 /= tdc2 and check result
      tdc1/=tdc2;
      UNIT_ASSERT_FEQUAL(tdc1.Norm(), 6.365984176164938, "Bug in /= operator overload for TensorDataCont" );
   }
};

/**
 * Tests the ElementwiseScalarAddition function for TensorDataCont
 **/
struct ElementwiseScalarAdditionTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataCont and set elements
      TensorDataCont tdc(I_2);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');

      // Add a scalar to each element and check the result
      Nb number = 2.00559874523456;
      tdc.ElementwiseScalarAddition(number);
      UNIT_ASSERT_FEQUAL(tdc.Norm(), 9.984379497355064, "Bug in TensorDataCont::ElementwiseScalarAddition()" );
   }
};

/**
 * Tests that the TensorDataCont::Zero() method works correctly
 **/
struct ZeroFunctionTest: public cutee::test
{
   void run() 
   {
      // Declare TensorDataConts and set elements
      TensorDataCont tdc(I_3);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor3D('a');

      // Set all elements to zero and check the result
      tdc.Zero();
      UNIT_ASSERT_FEQUAL(tdc.Norm(), C_0, "TensorDataCont::Zero() gives norm different from C_0")
   }
};


/**
 * Test that TensorDataCont::Scale() function works as intended.  Dependent on
 * NiceTensor::Scale() and SimpleTensor::Scale().
 **/
struct TensorDataContScaleTest : public cutee::test
{
   void run() 
   {
      //Set size of TDCs and assign tensors (leave some of them as ZeroTensor):
      In size = I_3;
      TensorDataCont tdc_a(size);
      tdc_a.GetModeCombiData(I_0) = detail::TestNiceTensor1D('a');
      tdc_a.GetModeCombiData(I_1) = detail::TestNiceTensor1D('a');
      tdc_a.GetModeCombiData(I_2) = detail::TestNiceTensor3D('a');

      // Scale TensorDataCont and check resulting norm against old norm:
      auto norm_before = tdc_a.Norm();
      tdc_a.Scale(2.0);
      UNIT_ASSERT_FEQUAL(tdc_a.Norm(), 2.0*norm_before, "TensorDataCont::Scale() not working.");
   }
};

/**
 * Test that TensorDataCont::Normalize() function works as intended.  Implicitly
 * tests TensorDataCont::Scale() as well.
 **/
struct TensorDataContNormalizeTest : public cutee::test
{
   void run() 
   {
      //Set size of TDCs and assign tensors:
      In size = I_3;
      TensorDataCont tdc_a(size);
      tdc_a.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc_a.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      tdc_a.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor3D('a');

      // Normalize TensorDataCont and check norm of result:
      tdc_a.Normalize();
      UNIT_ASSERT_FEQUAL(tdc_a.Norm(), 1.0, "TensorDataCont::Normalize() not working.");
   }
};

/**
 * Searches the ranks of all CanonicalTensor%s to find the minimum rank.
 **/
struct GetMinRankTest : public cutee::test
{
   void run()
   {
      In size = I_8;
      TensorDataCont tdc(size);

      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 3));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 4));
      tdc.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,4}, 6));
      tdc.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,4}, 9));

      auto vec = tdc.GetMinRankVec();

      // Test
      UNIT_ASSERT_EQUAL(tdc.GetMinRank(), I_2, "TensorDataCont::GetMinRank() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_0), -I_1,"TensorDataCont::GetMinRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_1), I_2, "TensorDataCont::GetMinRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_2), I_3, "TensorDataCont::GetMinRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_3), I_6, "TensorDataCont::GetMinRankVec() Not working!");
   }
};

/**
 * Searches the ranks of all CanonicalTensor%s to find the maximum rank.
 **/
struct GetMaxRankTest : public cutee::test
{
   void run()
   {
      In size = I_8;
      TensorDataCont tdc(size);

      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 1));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 4));
      tdc.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,4}, 6));
      tdc.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,4}, 4));

      auto vec = tdc.GetMaxRankVec();

      // Test
      UNIT_ASSERT_EQUAL(tdc.GetMaxRank(), I_7, "TensorDataCont::GetMaxRank() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_0), -I_1, "TensorDataCont::GetMaxRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_1), I_2, "TensorDataCont::GetMaxRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_2), I_7, "TensorDataCont::GetMaxRankVec() Not working!");
      UNIT_ASSERT_EQUAL(vec.at(I_3), I_6, "TensorDataCont::GetMaxRankVec() Not working!");
   }
};

/**
 * Returns average rank of all CanonicalTensor%s.
 **/
struct AverageRankTest : public cutee::test
{
   void run()
   {
      In size = I_6;
      TensorDataCont tdc(size);

      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3}, 6));
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 4));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2,4}, 2));


      // Test
      UNIT_ASSERT_FEQUAL(tdc.AverageRank(), 4.400, "TensorDataCont::AverageRank() Not working!");

      // Test vec
      In size2 = I_8;
      TensorDataCont tdc2(size2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc2.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc2.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 0));
      tdc2.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc2.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc2.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 2));
      tdc2.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 6));
      tdc2.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 4));

      auto vec = tdc2.AverageRankVec();

      UNIT_ASSERT_FEQUAL(vec.at(I_0),-C_1, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_1), C_1, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_2), C_4, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_3), C_5, "TensorDataCont::AverageRankVec() Not working!");
   }
};

/**
 * Returns average distance to theoretical max. rank of all CanonicalTensor%s.
 **/
struct AverageRankDifferenceTest : public cutee::test
{
   void run()
   {
      In size = I_6;
      TensorDataCont tdc(size);

      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3}, 6));
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 4));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2,4}, 2));

      // Test
      UNIT_ASSERT_FEQUAL(tdc.AverageRankDifference(), 2.0, "TensorDataCont::AverageRankDifference() Not working!");

      // Test vec
      In size2 = I_8;
      TensorDataCont tdc2(size2);
      tdc2.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc2.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc2.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 0));
      tdc2.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc2.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc2.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 2));
      tdc2.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 6));
      tdc2.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 4));

      auto vec = tdc2.AverageRankDifferenceVec();

      UNIT_ASSERT_FEQUAL(vec.at(I_0),-C_1, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_1), C_1, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_2), C_2, "TensorDataCont::AverageRankVec() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_3), C_13, "TensorDataCont::AverageRankVec() Not working!");
   }
};

/**
 * Calculates DataCompressionFactors of CP tensors (CanonicalTensor%s).
 **/
struct DataCompressionTest : public cutee::test
{
   void run()
   {
      In size = I_8;
      TensorDataCont tdc(size);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 0));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 2));
      tdc.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 6));
      tdc.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 4));
      
      auto vec = tdc.DataCompressionFactors();

      UNIT_ASSERT_FEQUAL(vec.at(I_0), -C_1, "TensorDataCont::DataCompressionFactors() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_1), 0.76923076923076927,"TensorDataCont::DataCompressionFactors() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_2), 1.5555555555555556,"TensorDataCont::DataCompressionFactors() Not working!");
      UNIT_ASSERT_FEQUAL(vec.at(I_3), 0.72222222222222222,"TensorDataCont::DataCompressionFactors() Not working!");
   }
};

/**
 * Calculates the largest tensor norm in the TensorDataCont
 */
struct MaxNormTest : public cutee::test
{
   void run()
   {
      In size = I_6;
      TensorDataCont tdc(size);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor1D('a');
      tdc.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor1D('b');
      tdc.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_3) = midas::test::detail::TestNiceTensor2D('b');
      tdc.GetModeCombiData(I_4) = midas::test::detail::TestNiceTensor3D('a');
      tdc.GetModeCombiData(I_5) = midas::test::detail::TestNiceTensor3D('b');

      auto maxnorm = tdc.MaxNorm();

      UNIT_ASSERT_FEQUAL(maxnorm, 9.251558331194406, "TensorDataCont::MaxNorm() Not working!");
   }
};

/**
 * Calculates dot product of two TensorDataCont%s containing NiceTensor%s.
 * Only tests with ZeroTensor%s and SimpleTensor%s but other cases are tested
 * on the tensor level in BaseTensorTest.
 **/
struct DotProductTest : public cutee::test
{
   void run() 
   {
      In size = I_6;
      TensorDataCont tdc_a(size);
      TensorDataCont tdc_b(size);

      // Dot contribution: 0
      tdc_a.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensorZero(I_4);
      tdc_b.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensorZero(I_4);

      // Dot contribution: 0
      tdc_a.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensorZero(I_3);
      tdc_b.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor3D('b');

      // Dot contribution: 0
      tdc_a.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor2D('a');
      tdc_b.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensorZero(I_2);

      // Dot contribution: 3.246859767068249
      tdc_a.GetModeCombiData(I_3) = midas::test::detail::TestNiceTensor1D('a');
      tdc_b.GetModeCombiData(I_3) = midas::test::detail::TestNiceTensor1D('b');

      // Dot contribution: -7.090563355058138
      tdc_a.GetModeCombiData(I_4) = midas::test::detail::TestNiceTensor2D('a');
      tdc_b.GetModeCombiData(I_4) = midas::test::detail::TestNiceTensor2D('b');

      // Dot contribution: -7.032288283309311
      tdc_a.GetModeCombiData(I_5) = midas::test::detail::TestNiceTensor3D('a');
      tdc_b.GetModeCombiData(I_5) = midas::test::detail::TestNiceTensor3D('b');

      Nb dot = Dot(tdc_a, tdc_b);

      UNIT_ASSERT_FEQUAL(dot, -10.875991871299201, "Wrong dot product.");
   }
};

/**
 * Calculates Y = aX + Y, (Y: the calling TensorDataCont, X: the other TDC, a:
 * scalar). Only tests with ZeroTensor<T>%s and SimpleTensor<T>%s but other cases are
 * tested on the tensor level in BaseTensorTest.
 **/
struct AxpyTest : public cutee::test
{
   void run()
   {
      In size = I_5;
      TensorDataCont tdc_y(size);
      TensorDataCont tdc_x(size);

      tdc_y.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensorZero(I_4);
      tdc_x.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensorZero(I_4);

      tdc_y.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('a');
      tdc_x.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensorZero(I_2);

      tdc_y.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor1D('a');
      tdc_x.GetModeCombiData(I_2) = midas::test::detail::TestNiceTensor1D('b');

      tdc_y.GetModeCombiData(I_3) = midas::test::detail::TestNiceTensor2D('a');
      tdc_x.GetModeCombiData(I_3) = midas::test::detail::TestNiceTensor2D('b');

      tdc_y.GetModeCombiData(I_4) = midas::test::detail::TestNiceTensor3D('a');
      tdc_x.GetModeCombiData(I_4) = midas::test::detail::TestNiceTensor3D('b');

      Nb a = -1.9839851676003331e+0;

      // Some reference values.
      Nb ref_norm_y = 23.862709019107289;
      Nb ref_norm_x = tdc_x.Norm();

      std::vector<Nb> ref_vals(I_5, C_0);
      ref_vals[I_1] = (dynamic_cast<SimpleTensor<Nb>*>(tdc_y.GetModeCombiData(I_1).GetTensor()))->GetData()[I_2];
      for(In i=I_2; i<ref_vals.size(); ++i)
      {
         ref_vals[i] = 
            (dynamic_cast<SimpleTensor<Nb>*>(tdc_y.GetModeCombiData(i).GetTensor()))->GetData()[I_2]
            + a*(dynamic_cast<SimpleTensor<Nb>*>(tdc_x.GetModeCombiData(i).GetTensor()))->GetData()[I_2];
      }

      // Do the axpy.
      tdc_y.Axpy(tdc_x, a);

      // Values after axpy.
      std::vector<Nb> vals(I_5, C_0);
      for(In i=I_1; i<vals.size(); ++i)
      {
         vals[i] = 
            (dynamic_cast<SimpleTensor<Nb>*>(tdc_y.GetModeCombiData(i).GetTensor()))->GetData()[I_2];
      }

      Nb norm_y = tdc_y.Norm();
      Nb norm_x = tdc_x.Norm();


      // Checks.
      UNIT_ASSERT_FEQUAL(norm_y, ref_norm_y, "Wrong norm of y TDC after axpy.");
      UNIT_ASSERT_FEQUAL(norm_x, ref_norm_x, "Norm of x TDC has changed during axpy.");
      // Element wise checks.
      UNIT_ASSERT_FEQUAL(vals[I_0], ref_vals[I_0], "Wrong elem. val. in MC 0 of y tens. after axpy.");
      UNIT_ASSERT_FEQUAL(vals[I_1], ref_vals[I_1], "Wrong elem. val. in MC 1 of y tens. after axpy.");
      UNIT_ASSERT_FEQUAL(vals[I_2], ref_vals[I_2], "Wrong elem. val. in MC 2 of y tens. after axpy.");
      UNIT_ASSERT_FEQUAL(vals[I_3], ref_vals[I_3], "Wrong elem. val. in MC 3 of y tens. after axpy.");
      UNIT_ASSERT_FEQUAL(vals[I_4], ref_vals[I_4], "Wrong elem. val. in MC 4 of y tens. after axpy.");
   }
};

/**
 * Checks that a TensorDataCont can be created from a DataCont.  Uses random
 * numbers, but should not be a problem as they are consistently checked
 * against themselves.
 **/
struct DataContToTensorDataContTest: public cutee::test
{

   void run() 
   {
      auto datacont = random::RandomDataCont<Nb>(static_cast<size_t>(23));
      auto tensor_datacont = TensorDataCont(datacont, detail::MakeMCR(3, 3, {2,3,4}), std::vector<int>{2,3,4});

      UNIT_ASSERT_FEQUAL(tensor_datacont.Norm(), datacont.Norm(), "TensorDataCont could not be created from DataCont.");
   }
};

/**
 * Test that a DataCont can be created correctly from a TensorDataCont.
 * Indirectly also checks that a TensorDataCont can be created from a DataCont.
 * Uses random numbers, but should not be a problem as they are consistently
 * checked against themselves.
 **/
struct DataContToTensorDataContTest2: public cutee::test
{
   void run() 
   {
      auto datacont = random::RandomDataCont<Nb>(static_cast<size_t>(23));
      auto tensor_datacont = TensorDataCont(datacont, detail::MakeMCR(3, 3, {2,3,4}), std::vector<int>{2,3,4});
      auto datacont_recreated = DataContFromTensorDataCont(tensor_datacont);
      
      UNIT_ASSERT_FEQUAL(datacont_recreated.Norm(), datacont.Norm(), "DataCont could not be recreated from TensorDataCont.");
   }
};

/**
 * Checks that a TensorDataCont can be created from a DataCont, including a
 * reference.  Uses random numbers, but should not be a problem as they are
 * consistently checked against themselves.
 **/
struct DataContToTensorDataContRefTest: public cutee::test
{

   void run() 
   {
      auto datacont = random::RandomDataCont<Nb>(static_cast<size_t>(24));
      auto tensor_datacont = TensorDataCont(datacont, detail::MakeMCR(3, 3, {2,3,4}), std::vector<int>{2,3,4}, true);

      UNIT_ASSERT_FEQUAL(tensor_datacont.Norm(), datacont.Norm(), "TensorDataCont could not be created from DataCont.");
   }
};

/**
 * Test that a DataCont can be created correctly from a TensorDataCont,
 * including a reference.  Indirectly also checks that a TensorDataCont can be
 * created from a DataCont.  Uses random numbers, but should not be a problem
 * as they are consistently checked against themselves.
 **/
struct DataContToTensorDataContRefTest2: public cutee::test
{
   void run() 
   {
      auto datacont = random::RandomDataCont<Nb>(static_cast<size_t>(24));
      auto tensor_datacont = TensorDataCont(datacont, detail::MakeMCR(3, 3, {2,3,4}), std::vector<int>{2,3,4}, true);
      auto datacont_recreated = DataContFromTensorDataCont(tensor_datacont);
      
      UNIT_ASSERT_FEQUAL(datacont_recreated.Norm(), datacont.Norm(), "DataCont could not be recreated from TensorDataCont.");
   }
};

/**
 * Test that a TensorDataCont can be converted to a NiceTensor, i.e. truncated CI expansion to FCI form.
 **/
struct NiceTensorFromTensorDataContTest
   :  virtual public cutee::test
{
   void run()
   {
      auto data = random::RandomDataCont<Nb>(static_cast<size_t>(24));
      auto tdc = TensorDataCont(data, detail::MakeMCR(3, 3, {2,3,4}), std::vector<unsigned>{2,3,4}, true);
      auto full_tensor = NiceTensorFromTensorDataCont(tdc);

      // Check dims
      UNIT_ASSERT_EQUAL(full_tensor.NDim(), 3, "Dimensionality of NiceTensor wrong");
      UNIT_ASSERT_EQUAL(full_tensor.Extent(0), 2, "First dimension wrong in NiceTensor");
      UNIT_ASSERT_EQUAL(full_tensor.Extent(1), 3, "Second dimension wrong in NiceTensor");
      UNIT_ASSERT_EQUAL(full_tensor.Extent(2), 4, "Third dimension wrong in NiceTensor");

      // Check norm
      UNIT_ASSERT_FEQUAL(full_tensor.Norm(), tdc.Norm(), "Norm of NiceTensor is wrong");

      // Check individual elements
      auto* full_data = full_tensor.StaticCast<SimpleTensor<Nb>>().GetData();
      UNIT_ASSERT_FEQUAL(full_data[0], tdc.GetModeCombiData(0).GetScalar(), "Reference coef wrong in NiceTensor");
      In start_mc02 = I_13;
      auto* tdc_mc02_data = tdc.GetModeCombiData(I_5).StaticCast<SimpleTensor<Nb>>().GetData();
      for(In i=I_0; i<I_3; ++i)
      {
         UNIT_ASSERT_FEQUAL(full_data[start_mc02+i], tdc_mc02_data[i], "Wrong coef in MC {0,2} in NiceTensor");
      }
   }
};

/**
 * Tests that the fileIO for TensorDataCont works as expected
 **/
struct FileIoTest: public cutee::test
{
   void teardown() override
   {
      if (midas::filesystem::IsFile(filename))
      {
         auto ret_val = midas::filesystem::Remove(filename);
         if (ret_val != 0)
         {
            MIDASERROR
               (  "Failure when removing "+filename
               +  " (return value: "+std::to_string(ret_val)+")"
               );
         }
      }
   }

   void run() override
   {
      const In size = I_9;
      TensorDataCont tdc(size);
      tdc.GetModeCombiData(I_0) = midas::test::detail::TestNiceTensor2D('a');
      tdc.GetModeCombiData(I_1) = midas::test::detail::TestNiceTensor2D('b');
      tdc.GetModeCombiData(I_2) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 2));
      tdc.GetModeCombiData(I_3) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{5,2}, 0));
      tdc.GetModeCombiData(I_4) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,2,4}, 3));
      tdc.GetModeCombiData(I_5) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,4,4}, 7));
      tdc.GetModeCombiData(I_6) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,4}, 2));
      tdc.GetModeCombiData(I_7) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 6));
      tdc.GetModeCombiData(I_8) = NiceTensor<Nb>(random_canonical_tensor<Nb>(std::vector<unsigned int>{2,3,3,5}, 4));

      tdc.WriteToDisc(filename, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);

      TensorDataCont tdc2(size);
      tdc2.ReadFromDisc(filename);

      for(In i=I_0; i<size; ++i)
      {
         Nb diff_norm = diff_norm_new(*tdc.GetModeCombiData(i).GetTensor(), *tdc2.GetModeCombiData(i).GetTensor());
         UNIT_ASSERT_FEQUAL(diff_norm, 0., "TensorDataCont FileIO does not work!");
      }
   }

   private:
      const std::string filename = "tensor_data_cont_fileIO_test.dat";

};

} /* namespace midas::test */

#endif /* TENSORDATACONTTEST_H_INCLUDED */
