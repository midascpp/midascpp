/**
 *******************************************************************************
 * 
 * @file    ExtLibsTest.h
 * @date    02-03-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Compile time checks that libraries are included/linked.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXTLIBSTEST_H_INCLUDED
#define EXTLIBSTEST_H_INCLUDED

#include <complex>

#include "test/CuteeInterface.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#ifdef ENABLE_BOOST
#include <boost/math/special_functions/gamma.hpp>
namespace midas::test::extlibs
{
   /************************************************************************//**
    * @brief
    *    Checks we can use something from boost.
    ***************************************************************************/
   struct Boost
      :  public cutee::test
   {
      void run() override
      {
         const double x = 0.5;
         const double val = boost::math::tgamma(x);
         const double expect = sqrt(C_PI);
         UNIT_ASSERT_FEQUAL(val, expect, "Error for boost. (boost::math::gamma)");
      }
   };
} /* namespace midas::test::extlibs */
#endif /* ENABLE_BOOST */

// Cutee is always enabled.
#include <cutee/float_eq.hpp>
namespace midas::test::extlibs
{
   /************************************************************************//**
    * @brief
    *    Checks we can use something from cutee.
    ***************************************************************************/
   struct Cutee
      :  public cutee::test
   {
      void run() override
      {
         const double one = 1.0;
         const double one_plus_eps = one + std::numeric_limits<double>::epsilon();
         const Uin dist = cutee::numeric::float_ulps(one, one_plus_eps);
         const Uin expect = 1;
         UNIT_ASSERT_EQUAL(dist, expect, "Error for cutee. (cutee::numeric::float_ulps)");
      }
   };
} /* namespace midas::test::extlibs */

#ifdef ENABLE_FFTW
#include <fftw3.h>
namespace midas::test::extlibs
{
   /************************************************************************//**
    * @brief
    *    Checks we can use something from fftw.
    ***************************************************************************/
   struct Fftw
      :  public cutee::test
   {
      void run() override
      {
         // Allocate.
         const Uin N = 2;
         fftw_complex* in  = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * N);
         fftw_complex* out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * N);
         fftw_plan p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

         // Load data to arrays.
         for(Uin i = 0; i < N; ++i)
         {
            in[i][0] = 1;
            in[i][1] = 1;
            out[i][0] = 0;
            out[i][1] = 0;
         }

         // Execute.
         fftw_execute(p);

         // Load data from arrays.
         std::complex<double> out0 = {out[0][0], out[0][1]};
         std::complex<double> out1 = {out[1][0], out[1][1]};

         // Deallocate.
         fftw_destroy_plan(p);
         fftw_free(in);
         fftw_free(out);

         // Assert.
         UNIT_ASSERT_EQUAL(out0, std::complex<double>(2,2), "Error for fftw. (out0)");
         UNIT_ASSERT_EQUAL(out1, std::complex<double>(0,0), "Error for fftw. (out1)");
      }
   };
} /* namespace midas::test::extlibs */
#endif /* ENABLE_FFTW */

#ifdef ENABLE_GSL
#include <gsl/gsl_sf_gamma.h>
namespace midas::test::extlibs
{
   /************************************************************************//**
    * @brief
    *    Checks we can use something from gsl.
    ***************************************************************************/
   struct Gsl
      :  public cutee::test
   {
      void run() override
      {
         const double x = 0.5;
         const double val = gsl_sf_gamma(x);
         const double expect = sqrt(C_PI);
         UNIT_ASSERT_FEQUAL(val, expect, "Error for gsl. (gsl_sf_gamma)");
      }
   };
} /* namespace midas::test::extlibs */
#endif /* ENABLE_GSL */

// LAPACK is always enabled.
#include "lapack_interface/math_wrappers.h"
namespace midas::test::extlibs
{
   /************************************************************************//**
    * @brief
    *    Checks we can use something from lapack.
    ***************************************************************************/
   struct Lapack
      :  public cutee::test
   {
      void run() override
      {
         double alpha = 1.0;
         double beta = 1.0;
         char tc = 'N';
         int n = 1;
         double a[1] = {2};
         double b[1] = {3};
         double c[1] = {-1};

         midas::lapack_interface::gemm
            (  &tc
            ,  &tc
            ,  &n
            ,  &n
            ,  &n
            ,  &alpha
            ,  a
            ,  &n
            ,  b
            ,  &n
            ,  &beta
            ,  c
            ,  &n
            );
         double expect = 5;
         UNIT_ASSERT_EQUAL(c[0], expect, "Error for lapack. (midas::lapack_interface::gemm)");
      }
   };
} /* namespace midas::test::extlibs */


#endif/*EXTLIBSTEST_H_INCLUDED*/
