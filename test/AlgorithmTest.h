/**
************************************************************************
* 
* @file                MoleculeTest.h
*
* Created:             16-12-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Unit tests MoleculeInfo.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_TEST_MOLECULETEST_H_INCLUDED
#define MIDAS_TEST_MOLECULETEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "algo/Kabsch.h"
#include "util/Rotation.h"
#include "test/Random.h"
#include "mmv/MidasMatrix.h"

namespace midas::test
{

namespace detail
{

/**
 * Create a set of points.
 **/
MidasMatrix CreatePoints
   (
   )
{
   return midas::test::random::RandomMidasMatrix<Nb>(4,3);
}

/**
 * Create a rotation R(X)R(Y)R(Z).
 **/
MidasMatrix CreateRotation
   (
   )
{
   auto rot_matrix_x = util::CreateRotationMatrixX(22 , util::Unit::Degrees);
   auto rot_matrix_y = util::CreateRotationMatrixY(48 , util::Unit::Degrees);
   auto rot_matrix_z = util::CreateRotationMatrixZ(137, util::Unit::Degrees);

   MidasMatrix total_rot_matrix{I_3, I_3};

   total_rot_matrix = rot_matrix_x * rot_matrix_y * rot_matrix_z;

   return total_rot_matrix;
}

/**
 * Create a set of points that is a rotation of another set of points.
 **/
MidasMatrix CreateRotatedPoints
   (  const MidasMatrix& aP
   )
{
   assert(aP.Ncols() == I_3);

   auto rot_matrix = CreateRotation();

   MidasMatrix q{aP.Nrows(), aP.Ncols(), C_0};

   for(int i = 0; i < q.Nrows(); ++i)
   {
      for(int j = 0; j < q.Ncols(); ++j)
      {
         for(int k = 0; k < q.Ncols(); ++k)
         {
            q[i][j] += rot_matrix[j][k] * aP[i][k];
         }
      }
   }

   return q;
}

/**
 * Move a set of points P to the centroid.
 **/
void MoveToCentroid
   (  MidasMatrix& aP
   )
{
   auto centroid_p = algo::FindCentroid(aP);

   for(int i = 0; i < aP.Nrows(); ++i)
   {
      for(int j = 0; j < aP.Ncols(); ++j)
      {
         aP[i][j] -= centroid_p[j];
      }
   }
}

} /* namespace detail */

/**
 * Test shifting to centroid for at set of points P.
 **/
struct MoveToCentroidTest
{
   void run()
   {
      auto p = detail::CreatePoints();

      detail::MoveToCentroid(p);

      auto centroid_origo = algo::FindCentroid(p);

      UNIT_ASSERT_EQUAL(centroid_origo.Size(), 3, "Size not 3.");

      UNIT_ASSERT_FZERO(centroid_origo[0], 1.0, "Element 0 not == 0.0.");
      UNIT_ASSERT_FZERO(centroid_origo[1], 1.0, "Element 1 not == 0.0.");
      UNIT_ASSERT_FZERO(centroid_origo[2], 1.0, "Element 2 not == 0.0.");
   }
};

/**
 * Test Kabsch algorithm for finding the optimal rotation between two sets of points P and Q
 **/
struct KabschTest
{
   void run()
   {  
      auto p = detail::CreatePoints();
      
      detail::MoveToCentroid(p);

      MidasMatrix q = detail::CreateRotatedPoints(p);

      auto rot_matrix_kabsch  = algo  ::Kabsch(p, q);
      auto rot_matrix_check   = detail::CreateRotation();
      
      UNIT_ASSERT_EQUAL(rot_matrix_kabsch.Nrows(), 3, "Number of rows incorrect.");
      UNIT_ASSERT_EQUAL(rot_matrix_kabsch.Ncols(), 3, "Number of cols incorrect.");

      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[0][0], rot_matrix_check[0][0], 250, "Element 0,0 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[0][1], rot_matrix_check[0][1], 250, "Element 0,1 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[0][2], rot_matrix_check[0][2], 250, "Element 0,2 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[1][0], rot_matrix_check[1][0], 250, "Element 1,0 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[1][1], rot_matrix_check[1][1], 250, "Element 1,1 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[1][2], rot_matrix_check[1][2], 250, "Element 1,2 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[2][0], rot_matrix_check[2][0], 250, "Element 2,0 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[2][1], rot_matrix_check[2][1], 250, "Element 2,1 not correct.");
      UNIT_ASSERT_FEQUAL_PREC(rot_matrix_kabsch[2][2], rot_matrix_check[2][2], 250, "Element 2,2 not correct.");
   }
};

} /* namespace midas::test */

#endif /* MIDAS_TEST_MOLECULETEST_H_INCLUDED */
