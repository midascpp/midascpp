#include "PlaygroundRun.h"


namespace midas::test
{

void PlaygroundRun()
{
   cutee::suite suite("PlaygroundRun");
   
   // add tests
   suite.add_test<PlaygroundRunImpl>("PlaygroundRun");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
