/**
 *******************************************************************************
 * 
 * @file    PropertyFileStorageTest.cc
 * @date    09-10-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for PropertyFileStorage class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PROPERTYFILESTORAGETEST_H_INCLUDED
#define PROPERTYFILESTORAGETEST_H_INCLUDED

#include <string>
#include <tuple>
#include <vector>

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "pes/PropertyFileStorage.h"
#include "util/UnderlyingType.h"

namespace midas::test
{
namespace property_file_storage
{

using value_t = PropertyFileStorage::value_t;
using calc_num_t = PropertyFileStorage::calc_num_t;
using calc_code_t = PropertyFileStorage::calc_code_t;
using property_t = PropertyFileStorage::property_t;
using contents_line_t = std::tuple<calc_num_t, calc_code_t, value_t>;
using contents_t = std::vector<contents_line_t>;

/***************************************************************************//**
 * Base struct use for these tests. Inherits test-suite functionality from
 * unit_test. Overrides setup() and teardown() functions that are then used for
 * pre-/post-processing at each test; in this case writing/removing test
 * property file.
 *
 * Also holds some functions used to compare resulting values with reference
 * values uniformly accross different tests. (Must be member functions, since
 * they need to inherit from cutee::test as well.)
 ******************************************************************************/
struct BasePropertyFileStorageTest: public cutee::test
{
   //! Property file contents used for writing and comparing results.
   const contents_t mContents =
      {  std::make_tuple(1,    "*#1_-1/4*",             1.729050E-02)
      ,  std::make_tuple(2,    "*#0*",                  0.000000E+00)
      ,  std::make_tuple(3,    "*#1_1/4*",              7.767704E-03)
      ,  std::make_tuple(13,   "*#6_1/4*",              3.621017E-02)
      ,  std::make_tuple(1337, "*#1_-21/256#2_5/64*",   1.513957E-03)
      };
   //! Contents to be appended for some tests.
   const contents_t mToAppend = 
      {  std::make_tuple(42,   "*#3_1/4*",              1.011121E-02)
      ,  std::make_tuple(60,   "*#4_1/8*",              4.311069E-04)
      ,  std::make_tuple(88,   "*#8_-1/8*",             2.894309E-01)
      };
   //! Precision (num. decimals in sci. not.) when writing file.
   const Uin mPrecision = 6;
   //! File name used in these tests.
   const std::string mFileName = "PropertyFileStorageTest_file";

   //! Write argument data to a property file.
   void WritePropertyFile();

   //! Delete the written property file (if it exists).
   void RemovePropertyFile();

   //! Check line contents against expected values.
   void CheckPropertyLine(const property_t&, const contents_line_t&);

   //! Check line contents against expected values.
   void CheckPropertyLine(const value_t&, const calc_num_t&, const contents_line_t&);

   //! Reads a file and checks property lines match argument, in order.
   void CheckPropertyFile(const std::string&, const contents_t&);

   //! Setup before doing tests. Writes a test property file.
   void setup() override
   {
      WritePropertyFile();
   }

   //! Teardown after doing tests. Removes the property file.
   void teardown() override
   {
      RemovePropertyFile();
   }
};

/**
 * @brief
 *    Tests construction from file name and that storage mode is _on disc_.
 **/
struct Constructor : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("on disc"), "Wrong storage mode.");
   }
};

/**
 * @brief
 *    Tests reading into/clearing memory and that storage modes are rightly
 *    designated.
 **/
struct ReadIntoAndClearFromMemory : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("on disc"), "Wrong storage mode.");
      pfs.ReadIntoMemory();
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("in memory"), "Wrong storage mode.");
      pfs.ClearFromMemory();
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("on disc"), "Wrong storage mode.");
   }
};

/**
 * @brief
 *    Searches for calculations codes without having read file into memory.
 **/
struct SearchOnDisc : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("on disc"), "Wrong storage mode.");
      for(const auto& t: mContents)
      {
         auto pair_prop_bool = pfs.Search(std::get<1>(t));
         UNIT_ASSERT(pair_prop_bool.second, "Didn't find line for calc code " + std::get<1>(t));
         CheckPropertyLine(pair_prop_bool.first, t);
      }
      auto pair_prop_bool = pfs.Search("rubbish");
      UNIT_ASSERT(!pair_prop_bool.second, "Shouldn't have found this 'calc code'.");
   }
};

/**
 * @brief
 *    Searches for calculations codes after having read file into memory.
 **/
struct SearchInMemory : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("on disc"), "Wrong storage mode.");
      pfs.ReadIntoMemory();
      UNIT_ASSERT_EQUAL(pfs.StorageMode(), std::string("in memory"), "Wrong storage mode.");
      // Remove file to ensure that we really are reading from memory.
      RemovePropertyFile();
      for(const auto& t: mContents)
      {
         auto pair_prop_bool = pfs.Search(std::get<1>(t));
         UNIT_ASSERT(pair_prop_bool.second, "Didn't find line for calc code " + std::get<1>(t));
         CheckPropertyLine(pair_prop_bool.first, t);
      }
      auto pair_prop_bool = pfs.Search("rubbish");
      UNIT_ASSERT(!pair_prop_bool.second, "Shouldn't have found this 'calc code'.");
   }
};

/**
 * @brief
 *    Searches for calculations codes using utility function
 *    ExtractPropertyLine().
 **/
struct TestExtractPropertyLine : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      pfs.ReadIntoMemory();
      value_t value;
      calc_num_t calc_num;
      for(const auto& t: mContents)
      {
         bool found = ExtractPropertyLine(pfs, std::get<1>(t), value, calc_num);
         UNIT_ASSERT(found, "Didn't find line for calc code " + std::get<1>(t));
         CheckPropertyLine(value, calc_num, t);
      }
      bool found = ExtractPropertyLine(pfs, "rubbish", value, calc_num);
      UNIT_ASSERT(!found, "Shouldn't have found this 'calc code'.");
   }
};

/**
 * @brief
 *    Appends extra property lines while the storage mode is _on disc_.
 *    Shall write continously.
 **/
struct AppendWhileOnDisc : public BasePropertyFileStorageTest
{
   void run() override
   {
      // Append stuff within scope, so we can also check stuff after destructor
      // was called.
      auto exp_file_contents = mContents;
      {
         PropertyFileStorage pfs(mFileName);
         // Append entries step-wise, and check whether it's on file.
         for(auto it = mToAppend.begin(), end = mToAppend.end(); it != end; ++it)
         {
            // Append.
            const auto& a = *it;
            PropertyFileStorage::property_t p = std::make_tuple
               (  std::get<2>(a)
               ,  std::get<0>(a)
               );
            PropertyFileStorage::calc_code_t c = std::get<1>(a);
            pfs.Append(c, p);

            // Update expected contents, then check it's on file.
            exp_file_contents.push_back(*it);
            CheckPropertyFile(mFileName, exp_file_contents);
         }
      }

      // Final check of file now that the PropertyFileStorage destructor has
      // been called.
      CheckPropertyFile(mFileName, exp_file_contents);
   }
};

/**
 * @brief
 *    Appends extra property lines while the storage mode is _in memory_.
 *    Shall not write before destruction of object.
 **/
struct AppendWhileInMemory : public BasePropertyFileStorageTest
{
   void run() override
   {
      // Append stuff within scope, so we can also check stuff after destructor
      // was called.
      auto exp_file_contents = mContents;
      {
         PropertyFileStorage pfs(mFileName);
         pfs.ReadIntoMemory();
         // Append entries step-wise, and check whether it's on file.
         for(auto it = mToAppend.begin(), end = mToAppend.end(); it != end; ++it)
         {
            // Append.
            const auto& a = *it;
            PropertyFileStorage::property_t p = std::make_tuple
               (  std::get<2>(a)
               ,  std::get<0>(a)
               );
            PropertyFileStorage::calc_code_t c = std::get<1>(a);
            pfs.Append(c, p);

            // Update expected _final_ contents, but check that file _only_
            // contains initial stuff. Check that appended property can be
            // found in memory.
            exp_file_contents.push_back(*it);
            CheckPropertyFile(mFileName, mContents);

            auto p_prop_bool = pfs.Search(c);
            UNIT_ASSERT(p_prop_bool.second, "Search for app. line failed, calc code: " + c);
            CheckPropertyLine(p_prop_bool.first, a);
         }
      }

      // Final check of file now that the PropertyFileStorage destructor has
      // been called.
      CheckPropertyFile(mFileName, exp_file_contents);
   }
};

/**
 * @brief
 *    Check that appending while storage is _in memory_, then clearing from
 *    memory triggers the writing to file.
 **/
struct AppendAndClearFromMemory : public BasePropertyFileStorageTest
{
   void run() override
   {
      // Open, read into memory, append.
      PropertyFileStorage pfs(mFileName);
      pfs.ReadIntoMemory();
      for(const auto& a: mToAppend)
      {
         pfs.Append(std::get<1>(a), std::make_tuple(std::get<2>(a), std::get<0>(a)));
      }

      // Check file contents haven't been changed yet.
      auto exp_file_contents = mContents;
      CheckPropertyFile(mFileName, exp_file_contents);

      // Clear memory, then check file has been updated.
      pfs.ClearFromMemory();
      exp_file_contents.insert(exp_file_contents.end(), mToAppend.begin(), mToAppend.end());
      CheckPropertyFile(mFileName, exp_file_contents);
   }
};

/**
 * @brief
 *    Appends calculations codes and properties using utility function
 *    AppendPropertyLine().
 **/
struct TestAppendPropertyLine : public BasePropertyFileStorageTest
{
   void run() override
   {
      PropertyFileStorage pfs(mFileName);
      pfs.ReadIntoMemory();
      for(const auto& a: mToAppend)
      {
         bool appended = AppendPropertyLine
            (  pfs
            ,  std::get<1>(a) // calc code
            ,  std::get<2>(a) // value
            ,  std::get<0>(a) // calc num
            );
         UNIT_ASSERT(appended, "Failure appending prop. line, calc code: " + std::get<1>(a));
         auto p_prop_bool = pfs.Search(std::get<1>(a));
         UNIT_ASSERT(p_prop_bool.second, "Failure searching appended calc code: " + std::get<1>(a));
         CheckPropertyLine(p_prop_bool.first, a);
      }
   }
};

} /* namespace property_file_storage */
} /* namespace midas::test */

#endif/*PROPERTYFILESTORAGETEST_H_INCLUDED*/
