#ifndef MODELPOTTEST_H_INCLUDED
#define MODELPOTTEST_H_INCLUDED

#include<memory>

#include "test/CuteeInterface.h"

#include "potentials/ModelPot.h"
#include "potentials/MorsePot.h"
#include "potentials/DoubleWellPot.h"
#include "potentials/PotentialHandler.h"

namespace midas::test
{

/**
 *
 **/
struct MorsePotTest: public cutee::test
{
   Nb MorsePotEval(Nb x) const
   {
      return 0.1026 * (1.0 - std::exp(-0.732*(x-1.9972)))*(1.0 - std::exp(-0.732*(x-1.9972)));
   }

   void run() 
   {
      ModelPotInfo mp_info = { {"MODELPOT","MORSEPOT"} };
      std::unique_ptr<ModelPot> pot = ModelPot::Factory(mp_info);
      UNIT_ASSERT(bool(dynamic_cast<MorsePot*>(pot.get())), "Could not construct MorsePot.");
      
      for(int i = 0; i < 100; ++i)
      {
         Nb x = i*0.1;
         UNIT_ASSERT_FEQUAL(dynamic_cast<MorsePot*>(pot.get())->EvalPot(x,0),MorsePotEval(x), "MorsePot evaluation failed.");
      }

      Nb x = 1.9972;
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(dynamic_cast<MorsePot*>(pot.get())->EvalPot(x,0),1.0),"MorsePot evaluation failed at equilibrium.");
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(dynamic_cast<MorsePot*>(pot.get())->EvalDer(x,0)[0],1.0),"MorsePot derivative failed at equilibrium.");
      //UNIT_ASSERT(cutee::numeric::float_numeq_zero(dynamic_cast<MorsePot*>(pot.get())->EvalHess(x)[0][0],1.0),"MorsePot hessian failed at equilibrium.");
   }
};

/**
 *
 **/
struct DoubleWellPotTest: public cutee::test
{
   //Nb DoubleWellPotEval(Nb x) const
   //{
   //   return 0; // FIXME!
   //}

   void run() 
   {
      ModelPotInfo mp_info = { {"MODELPOT","DOUBLEWELLPOT"} };
      std::unique_ptr<ModelPot> pot = ModelPot::Factory(mp_info);
      UNIT_ASSERT(bool(dynamic_cast<DoubleWellPot*>(pot.get())), "Could not construct DoubleWellPot.");

      //for(int i = -1000; i < 1000; ++i)
      //{
      //   Nb x = i*0.1;
      //   UNIT_ASSERT_FEQUAL(dynamic_cast<DoubleWellPot*>(pot.get())->EvalPot(x),DoubleWellPotEval(x),"DoubleWellPot evaluation failed.");
      //}
   }
};

/**
 * PotentialHandler Test
 **/
struct PotentialHandlerTest
   : public cutee::test
{
   void run()
   {
      // Create handler
      using namespace midas;
      potentials::PotentialHandler handler;
      
      // Create modelpot info
      ModelPotInfo mp_info_morse      = { {"MODELPOT","MORSEPOT"} };
      ModelPotInfo mp_info_doublewell = { {"MODELPOT","DOUBLEWELLPOT"} };
   
      // Load potentials into handler
      auto morse_key = handler.LoadPotential(mp_info_morse);
      auto dw_key    = handler.LoadPotential(mp_info_doublewell);
   
      // Try to get out potentials again
      auto& morse_pot      = handler.GetPotential(morse_key);
      auto& doublewell_pot = handler.GetPotential(dw_key);

      // Do some assertions.
      UNIT_ASSERT_EQUAL(morse_pot->Type()     , std::string("MORSEPOT")     , "Type not correct for loaded morse potential."     );
      UNIT_ASSERT_EQUAL(doublewell_pot->Type(), std::string("DOUBLEWELLPOT"), "Type not correct for loaded doublewell potential.");
   }
};

} /* namespace midas::test */

#endif /* MODELPOTTEST_H_INCLUDED */
