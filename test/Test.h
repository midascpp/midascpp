/**
************************************************************************
* 
* @file                Test.h
*
* Created:             28-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store test declarations. 
* 
* Last modified: Thu Oct 26, 2006  06:00PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TEST_H
#define TEST_H

namespace midas::test
{
   // Forward declaration.
   class TestDrivers;

   //! Master unit test driver.
   void Test(const TestDrivers&);

} /* namespace midas::test */

#endif /* TEST_H */
