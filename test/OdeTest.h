/**
 *******************************************************************************
 * 
 * @file    OdeTest.h
 * @date    13-07-2018
 * @author  Niels Kristian Kjærgård Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Unit tests for ode/
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef ODETEST_H_INCLUDED
#define ODETEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "ode/OdeIntegrator.h"
#include "input/OdeInput.h"
#include "mmv/MidasVector.h"

#include "inc_gen/TypeDefs.h"
#include "util/SanityCheck.h"

namespace midas::test
{

namespace detail
{
template
   <  class T
   >   
struct decaying_exponential_ode
{
   //! TypeDefs
   using step_t = T;
   using param_t = T;
   using vec_t = GeneralMidasVector<T>;

   //! Derivative
   vec_t Derivative
      (  step_t aStep
      ,  const vec_t& aVec
      )  const
   {
      assert(aVec.size() == 1);

      param_t val = -aVec[0];

      return vec_t(1, val);
   }

   //! Shaped zero vector
   vec_t ShapedZeroVector
      (
      )  const
   {
      return GeneralMidasVector<T>(1, T(0.));
   }
};

} /* namespace detail */

/**
 *
 **/
template<class T>
struct DecayingExponentialTest : public cutee::test
{
   //! run test
   void run()
   {
      GeneralMidasVector<T> vec_zero(1,T(0.));
      GeneralMidasVector<T> vec_one(1,T(1.));

      detail::decaying_exponential_ode<T> test_ode;

      // Test DOPR853
      Mout  << " Testing Dopr853" << std::endl;
      midas::ode::OdeInfo info_853;
      info_853["DRIVER"] = std::string("MIDAS");
      info_853["STEPPER"] = std::string("DOPR853");
      info_853["TIMEINTERVAL"] = std::make_pair(C_0, C_1);
      info_853["DATABASESAVEVECTORS"] = true;
      info_853["ODESTEPPERABSTOL"] = C_I_10_12;
      info_853["ODESTEPPERRELTOL"] = C_0;
      info_853["OUTPUTPOINTS"] = -I_1;
      ::detail::ValidateOdeInput(info_853);

      Mout  << "    Starting from 0" << std::endl;
      OdeIntegrator integ_zero_853(&test_ode, info_853);
      integ_zero_853.Integrate(vec_zero);
      const auto& ts_zero_853 = integ_zero_853.GetDatabaseT();
      const auto& ys_zero_853 = integ_zero_853.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_zero_853.size(), ys_zero_853.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_zero_853.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL(ys_zero_853[i][0], T(0.), "Wrong saved point for dy/dt = -y with y(0) = 0 using Dopr853!");
      }

      Mout  << "    Starting from 1" << std::endl;
      OdeIntegrator integ_one_853(&test_ode, info_853);
      integ_one_853.Integrate(vec_one);
      const auto& ts_one_853 = integ_one_853.GetDatabaseT();
      const auto& ys_one_853 = integ_one_853.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_one_853.size(), ys_one_853.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_one_853.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL_PREC(ys_one_853[i][0], std::exp(-ts_one_853[i]), 1000, "Wrong saved point for dy/dt = -y with y(0) = 1 using Dopr853!");
      }

      // Test DOPR5
      vec_zero[0] = T(0.);
      vec_one[0] = T(1.);
      Mout  << " Testing Dopr5" << std::endl;
      midas::ode::OdeInfo info_5;
      info_5["DRIVER"] = std::string("MIDAS");
      info_5["STEPPER"] = std::string("DOPR5");
      info_5["TIMEINTERVAL"] = std::make_pair(C_0, C_1);
      info_5["DATABASESAVEVECTORS"] = true;
      info_5["ODESTEPPERABSTOL"] = C_I_10_12;
      info_5["ODESTEPPERRELTOL"] = C_0;
      info_5["OUTPUTPOINTS"] = -I_1;
      ::detail::ValidateOdeInput(info_5);

      Mout  << "    Starting from 0" << std::endl;
      OdeIntegrator integ_zero_5(&test_ode, info_5);
      integ_zero_5.Integrate(vec_zero);
      const auto& ts_zero_5 = integ_zero_5.GetDatabaseT();
      const auto& ys_zero_5 = integ_zero_5.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_zero_5.size(), ys_zero_5.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_zero_5.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL(ys_zero_5[i][0], T(0.), "Wrong saved point for dy/dt = -y with y(0) = 0 using Dopr5!");
      }

      Mout  << "    Starting from 1" << std::endl;
      OdeIntegrator integ_one_5(&test_ode, info_5);
      integ_one_5.Integrate(vec_one);
      const auto& ts_one_5 = integ_one_5.GetDatabaseT();
      const auto& ys_one_5 = integ_one_5.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_one_5.size(), ys_one_5.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_one_5.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL_PREC(ys_one_5[i][0], std::exp(-ts_one_5[i]), 5000, "Wrong saved point for dy/dt = -y with y(0) = 1 using Dopr5!");
      }

      // Test TSIT5
      vec_zero[0] = T(0.);
      vec_one[0] = T(1.);
      Mout  << " Testing Tsit5" << std::endl;
      midas::ode::OdeInfo info_t5;
      info_t5["DRIVER"] = std::string("MIDAS");
      info_t5["STEPPER"] = std::string("TSIT5");
      info_t5["TIMEINTERVAL"] = std::make_pair(C_0, C_1);
      info_t5["DATABASESAVEVECTORS"] = true;
      info_t5["ODESTEPPERABSTOL"] = C_I_10_12;
      info_t5["ODESTEPPERRELTOL"] = C_0;
      info_t5["OUTPUTPOINTS"] = -I_1;
      ::detail::ValidateOdeInput(info_t5);

      Mout  << "    Starting from 0" << std::endl;
      OdeIntegrator integ_zero_t5(&test_ode, info_t5);
      integ_zero_t5.Integrate(vec_zero);
      const auto& ts_zero_t5 = integ_zero_t5.GetDatabaseT();
      const auto& ys_zero_t5 = integ_zero_t5.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_zero_t5.size(), ys_zero_t5.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_zero_t5.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL(ys_zero_t5[i][0], T(0.), "Wrong saved point for dy/dt = -y with y(0) = 0 using Tsit5!");
      }

      Mout  << "    Starting from 1" << std::endl;
      OdeIntegrator integ_one_t5(&test_ode, info_t5);
      integ_one_5.Integrate(vec_one);
      const auto& ts_one_t5 = integ_one_t5.GetDatabaseT();
      const auto& ys_one_t5 = integ_one_t5.GetDatabaseY();
      UNIT_ASSERT_EQUAL(ts_one_t5.size(), ys_one_t5.size(), "Database saves wrong stuff!");
      for(size_t i=0; i<ts_one_t5.size(); ++i)
      {
         UNIT_ASSERT_FEQUAL_PREC(ys_one_t5[i][0], std::exp(-ts_one_t5[i]), 5000, "Wrong saved point for dy/dt = -y with y(0) = 1 using Tsit5!");
      }
   }
};

} /* namespace midas::test */


#endif /* ODETEST_H_INCLUDED */
