/**
 *******************************************************************************
 * 
 * @file    ErrorTest.h
 * @date    
 * @author  
 *
 * @brief
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef ERRORTEST_H_INCLUDED
#define ERRORTEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "util/Error.h"

namespace midas::test::error
{
   struct ErrorNumberBaseTest: public cutee::test
   {
      void setup()
      {
         ErrorControl(ErrorCtrl::SETNUMBER);
         ErrorControl(ErrorCtrl::MUTESTDOUT);
         ErrorControl(ErrorCtrl::MUTEMOUT);
      }

      void teardown()
      {
         ErrorControl(ErrorCtrl::RESETNUMBER);
         ErrorControl(ErrorCtrl::UNMUTESTDOUT);
         ErrorControl(ErrorCtrl::UNMUTEMOUT);
         ErrorControl(ErrorCtrl::SETKILL);
      }
   };
      
   /************************************************************************//**
    ***************************************************************************/
   struct ErrorNumberInitTest: public ErrorNumberBaseTest
   {
      void run() override
      {
         int errnum = ErrorNumber();
         UNIT_ASSERT_EQUAL(errnum, 0, "ErrorNumber not set correctly initially.");
      }
   };

   /************************************************************************//**
    ***************************************************************************/
   struct ErrorNumberResetTest: public ErrorNumberBaseTest
   {
      void run() override
      {
         MIDASERROR("Unit test error.");
         ErrorControl(ErrorCtrl::RESETNUMBER);
         int errnum = ErrorNumber();
         UNIT_ASSERT_EQUAL(errnum, 0, "ErrorNumber not set correctly after reset.");
      }
   };

   /************************************************************************//**
    ***************************************************************************/
   struct ErrorNumberTest: public ErrorNumberBaseTest
   {
      void run() override
      {
         MIDASERROR("Unit test error.");
         int errnum = ErrorNumber();
         UNIT_ASSERT_EQUAL(errnum, -1, "ErrorNumber not set correctly after error.");
      }
   };
} /* namespace midas::test::type_traits */


#endif/*ERRORTEST_H_INCLUDED*/
