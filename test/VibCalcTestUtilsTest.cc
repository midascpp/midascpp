/**
 *******************************************************************************
 * 
 * @file    VibCalcTestUtilsTest.cc
 * @date    13-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/VibCalcTestUtilsTest.h"

namespace midas::test
{
   namespace vibcalctestutils::detail
   {
   } /* namespace vibcalctestutils::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void VibCalcTestUtilsTest()
   {
      // Create test_suite object.
      cutee::suite suite("VibCalcTestUtils test");

      // Add all the tests to the suite.
      using namespace vibcalctestutils;

      auto info = [](const std::string& s, Uin coup, Uin prim, Uin exci, Uin modals, bool solver)->std::string
         {
            std::stringstream ss;
            ss << s 
               << "; VCC[" << exci << "]/H" << coup
               << ", N_prim = " << std::setw(2) << prim 
               << ", N_modals = " << std::setw(2) << modals
               << ", " << (solver?"tensor solver":"old solver")
               ;
            return ss.str();
         };
      for(const auto& tens_solver: std::vector<bool>{false,true})
      {
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 1,  5, 2, 5, tens_solver), 1, 5,  2, 5, tens_solver, 2.1284947081795506E-02, 2.1284947081795503E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 1, 10, 2, 5, tens_solver), 1, 10, 2, 5, tens_solver, 2.1284864184549043E-02, 2.1284864184549043E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 1, 15, 2, 5, tens_solver), 1, 15, 2, 5, tens_solver, 2.1284864176857241E-02, 2.1284864176857238E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 2,  5, 2, 5, tens_solver), 2, 5,  2, 5, tens_solver, 2.1058922318580697E-02, 2.0985731080964343E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 2, 10, 2, 5, tens_solver), 2, 10, 2, 5, tens_solver, 2.1058848987659657E-02, 2.0985648077119362E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 2, 15, 2, 5, tens_solver), 2, 15, 2, 5, tens_solver, 2.1058848985035353E-02, 2.0985648065741311E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 3,  5, 3, 3, tens_solver), 3, 5,  3, 3, tens_solver, 2.1059106155037473E-02, 2.0987984921662602E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 3, 10, 3, 3, tens_solver), 3, 10, 3, 3, tens_solver, 2.1059032748837990E-02, 2.0987894600799688E-02);
         suite.add_test<WaterHoBas_ir_h2o_vci>(info("ir_h2o_vci", 3, 15, 3, 3, tens_solver), 3, 15, 3, 3, tens_solver, 2.1059032746196405E-02, 2.0987894595305995E-02);
      }

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
