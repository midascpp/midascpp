/**
 *******************************************************************************
 * 
 * @file    MlearnTest.cc
 * @date    14-09-2020 
 * @author  Denis G. Artiukhin (artiukhin@chem.au.dk) 
 *
 * @brief
 *    Unit tests for routines in mlearn folder. 
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/MlearnTest.h" 

namespace midas::test
{
   namespace mlearn_test::detail 
   {
   
   } /* mlearn_test::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void MlearnTest() 
   {
      // Create test_suite object.
      cutee::suite suite("MlearnTest test");

      // mlearn/mlutil.h  
      suite.add_test<mlearn_test::TestGetNumberOfHyperParameters>("Test GetNumberOfHyperParameters().");

      // mlearn/MLDescriptor.h
      suite.add_test<mlearn_test::TestMLDescriptorGetNatoms>           ("Test MLDescriptorGetNatoms().");
      suite.add_test<mlearn_test::TestMLDescriptorGetLmax>             ("Test MLDescriptorGetLmax().");
      suite.add_test<mlearn_test::TestMLDescriptorGetDerivativeOrder>  ("Test MLDescriptorGetDerivativeOrder().");
      suite.add_test<mlearn_test::TestMLDescriptorGetDerivCoord1>      ("Test MLDescriptorGetDerivCoord1().");
      suite.add_test<mlearn_test::TestMLDescriptorGetDerivCoord2>      ("Test MLDescriptorGetDerivCoord2().");
      suite.add_test<mlearn_test::TestMLDescriptorGetMaxNeighbours>    ("Test MLDescriptorGetMaxNeighbours().");
      suite.add_test<mlearn_test::TestMLDescriptorGetAlpha>            ("Test MLDescriptorGetAlpha().");

      // Run the tests.
      RunSuite(suite);
   }

} /* namespace midas::test */
