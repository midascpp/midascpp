#ifndef DATACONTTEST_H_INCLUDED
#define DATACONTTEST_H_INCLUDED

#include "test/CuteeInterface.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/type_traits/Complex.h"
#include "vcc/TensorDataCont.h"
#include "util/type_traits/TypeName.h"

#include "test/Random.h"

namespace midas::test
{

namespace datacont
{
/**
 *
 **/
struct ConstructorTest: public cutee::test
{
   /**
    * run
    **/
   void run() 
   {
      DataCont cont(3,2.0);
      double data;
      cont.DataIo(IO_GET, 0, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 1, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 2, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
   }
};

/**
 *
 **/
struct ConstructorTest2: public cutee::test
{
   /**
    * run
    **/
   void run() 
   {
      MidasVector data_vec(3,2.0);
      DataCont cont(data_vec);
      double data;
      cont.DataIo(IO_GET, 0, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 1, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 2, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
   }
};

/**
 *
 **/
struct ConstructorTest3: public cutee::test
{
   /**
    * run
    **/
   void run() 
   {
      MidasMatrix data_mat(2,2,2.0);
      DataCont cont(data_mat);
      double data;
      cont.DataIo(IO_GET, 0, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 1, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 2, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 3, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
   }
};

/**
 *
 **/
struct SaveOnDiscTest: public cutee::test
{
   /**
    * run
    **/
   void run() 
   {
      DataCont cont(3,2.0);
      cont.ChangeStorageTo("OnDisc");

      double data;
      cont.DataIo(IO_GET, 0, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 1, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
      cont.DataIo(IO_GET, 2, data);
      UNIT_ASSERT_FEQUAL(data,2.0,"element not correct");
   }
};

/**
 *
 **/
struct SaveOnDiscTest2: public cutee::test
{
   /**
    * run
    **/
   void run() 
   {
      DataCont cont(3,2.0);
      cont.ChangeStorageTo("OnDisc");

      double data = 3.0;
      cont.DataIo(IO_PUT, 0, data);
      data = 4.0;
      cont.DataIo(IO_PUT, 1, data);
      data = 5.0;
      cont.DataIo(IO_PUT, 2, data);

      cont.DataIo(IO_GET, 0, data);
      UNIT_ASSERT_FEQUAL(data,3.0,"element not correct");
      cont.DataIo(IO_GET, 1, data);
      UNIT_ASSERT_FEQUAL(data,4.0,"element not correct");
      cont.DataIo(IO_GET, 2, data);
      UNIT_ASSERT_FEQUAL(data,5.0,"element not correct");
   }
};

/***************************************************************************//**
 * @brief
 *    Checks DataIo and its return status.
 *
 * Does simple DataIo (put/get +robust) in mem/on disk for cases expected to
 * succeed (also checks here that values are put/gotten as expected) and fail
 * (reading nonexisting file from disc). Checks return status accordingly.
 *
 * Not the prettiest of tests... anyway, it gets the job done.
 ******************************************************************************/
template<template<class T> class M, class U>
struct DataIoReturnStatusTest: public cutee::test
{
   static_assert  (  std::is_same_v<M<U>, GeneralMidasVector<U>>
                  || std::is_same_v<M<U>, GeneralMidasMatrix<U>>
                  ,  "Template class M must be either GeneralMidasVector or GeneralMidasMatrix."
                  );
   using mmv_t = M<U>;
   using datacont_t = GeneralDataCont<U>;
   using midasvector_t = GeneralMidasVector<U>;
   using io_t = typename datacont_t::Io;

   std::string ToString(const IoType<int(IO_GET)>&) const {return "IO_GET";}
   std::string ToString(const IoType<int(IO_PUT)>&) const {return "IO_PUT";}
   std::string ToString(const IoType<int(IO_GET | IO_ROBUST)>&) const {return "IO_GET | IO_ROBUST";}

   template<int I>
   void run_impl(const IoType<I>& io)
   {
      // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
      // defined only in terms of the template parameter N).
      constexpr IoType<I> constexpr_io_type;

      constexpr bool is_complex = midas::type_traits::IsComplexV<U>;
      constexpr bool is_vec = std::is_same_v<mmv_t, GeneralMidasVector<U>>;
      std::string str_io = ToString(io);

      const std::vector<std::string> v_storage{"InMem", "OnDisc"};
      auto err_str = [&str_io](const std::string& s) -> std::string
      {
         std::stringstream ss;
         ss << "Info: Storage = " << s << ", I/O operation = " << str_io << ".";
         return ss.str();
      };
      for(const auto& storage: v_storage)
      {
         // Normal operation, this should succeed, whether put/get and mem/disk.

         // Values; if complex add non-zero imaginary part as well.
         U dc_val(1.);
         U mmv_val(2.);
         if constexpr(is_complex)
         {
            dc_val.imag(3.);
            mmv_val.imag(4.);
         }
         datacont_t dc(1, dc_val, storage);
         // Do the DataIo operation. Interface varies a little for
         // MidasVector/Matrix and especially for Nb.
         // Then extract the one element in the MidasMatrix/Vector for later
         // comparison.
         io_t status;
         U extr_mmv(0);
         if constexpr(is_vec) // GeneralMidasVector<U>
         {
            mmv_t mmv(dc.Size(), mmv_val);
            status = dc.DataIo(io, mmv, dc.Size());
            extr_mmv = mmv[I_0];
         }
         else // Else it's GeneralMidasMatrix<U>
         {
            mmv_t mmv(dc.Size(), mmv_val);
            status = dc.DataIo(io, mmv, dc.Size(), mmv.Nrows(), mmv.Ncols());
            extr_mmv = mmv[I_0][I_0];
         }

         // Check data was actually put/gotten correctly. Before checking
         // return status.
         // Yes, it also uses the DataIo that we're checking, but it's not
         // trivial doing it in another way; if DataIo failed earlier, we'll
         // probably get a MIDASERROR at this point.
         midasvector_t mv_check(I_1, C_0);
         dc.DataIo(IO_GET, mv_check, I_1);
         // If getting, both should be equal to DataCont value, otherwise
         // both equal to MidasMatrix/Vector value.
         const U* p_exp_val = nullptr;
         if constexpr(bool(constexpr_io_type & IO_GET))
         {
            p_exp_val = &dc_val;
         }
         else if constexpr(bool(constexpr_io_type & IO_PUT))
         {
            p_exp_val = &mmv_val;
         }
         else
         {
            static_assert(true, "Neither IO_GET nor IO_PUT was set.");
         }
         UNIT_ASSERT_EQUAL(mv_check[I_0], *p_exp_val, "DataIo failed (DC). "+err_str(storage));
         UNIT_ASSERT_EQUAL(extr_mmv, *p_exp_val, "DataIo failed (MMV). "+err_str(storage));

         // If getting to here, DataIo (probably) did the right thing, and
         // it's meaningful to check status.
         UNIT_ASSERT(status == io_t::SUCCESS, "Wrong status, expected SUCCESS. "+err_str(storage));
      }

      // In case we're doing IO_GET | IO_ROBUST, we can do something that's
      // supposed to fail, then check that we got the IO_ERROR return value.
      // InMem should always succeed (DataIo doesn't check for that at
      // least), but if OnDisc we can catch failure with IO_ROBUST.
      if constexpr(bool(constexpr_io_type & IO_ROBUST) && bool(constexpr_io_type & IO_GET))
      {
         // Default DataCont, with bogus label and on disk, then trying
         // to read from it; should fail.
         datacont_t dc;
         dc.NewLabel(std::string("no_way_this_file_is_on_disk_trollololz_io_")+std::to_string(int(constexpr_io_type)));
         dc.ChangeStorageTo("OnDisc");
         dc.SetNewSize(I_1);
         mmv_t mmv(I_1);
         io_t status;
         if constexpr(is_vec) // GeneralMidasVector<U>
         {
            status = dc.DataIo(io, mmv, dc.Size());
         }
         else // Else it's GeneralMidasMatrix<U>
         {
            status = dc.DataIo(io, mmv, dc.Size(), mmv.Nrows(), mmv.Ncols());
         }
         UNIT_ASSERT(status == io_t::ERROR,"Not IO_ERROR (with IO_ROBUST). "+err_str("OnDisc"));
      }
   }

   void run()
   {
      run_impl(IO_GET);
      run_impl(IO_PUT);
      run_impl(IO_GET | IO_ROBUST);
   }

};

/***************************************************************************//**
 * @brief
 *    Checks DataIo and its return status.
 *
 * Does simple DataIo (put/get +robust) in mem/on disk for cases expected to
 * succeed (also checks here that values are put/gotten as expected) and fail
 * (reading nonexisting file from disk). Checks return status accordingly.
 *
 * Not the prettiest of tests... anyway, it gets the job done.
 ******************************************************************************/
template<class U>
struct DataIoNbReturnStatusTest: public cutee::test
{
   using datacont_t = GeneralDataCont<U>;
   using midasvector_t = GeneralMidasVector<U>;
   using io_t = typename datacont_t::Io;

   std::string ToString(const IoType<int(IO_GET)>&) const {return "IO_GET";}
   std::string ToString(const IoType<int(IO_PUT)>&) const {return "IO_GET";}
   std::string ToString(const IoType<int(IO_GET | IO_ROBUST)>&) const {return "IO_GET | IO_ROBUST";}

   template<int I>
   void run_impl(const IoType<I>& io)
   {
      // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
      // defined only in terms of the template parameter N).
      constexpr IoType<I> constexpr_io_type;

      constexpr bool is_complex = midas::type_traits::IsComplexV<U>;

      const std::vector<std::string> v_storage{"InMem", "OnDisc"};
      std::string str_io = ToString(io);

      auto err_str = [&str_io](const std::string& s) -> std::string
      {
         std::stringstream ss;
         ss << "Info: Storage = " << s << ", I/O operation = " << str_io << ".";
         return ss.str();
      };
      for(const auto& storage: v_storage)
      {
         // Normal operation, this should succeed, whether put/get and mem/disk.

         // Values; if complex add non-zero imaginary part as well.
         U dc_val(1.);
         U init_nb_val(2.);
         if constexpr(is_complex)
         {
            dc_val.imag(3.);
            init_nb_val.imag(4.);
         }
         U nb_val(init_nb_val);
         datacont_t dc(1, dc_val, storage);
         // Do the DataIo operation for Nb.
         auto status = dc.DataIo(io, 0, nb_val);

         // Check data was actually put/gotten correctly. Before checking
         // return status.
         // Yes, it also uses the DataIo that we're checking, but it's not
         // trivial doing it in another way; if DataIo failed earlier, we'll
         // probably get a MIDASERROR at this point.
         midasvector_t mv_check(I_1, C_0);
         dc.DataIo(IO_GET, mv_check, I_1);
         // If getting, both should be equal to DataCont value, otherwise
         // both equal to MidasMatrix/Vector value.
         const U* p_exp_val = nullptr;
         if constexpr(bool(constexpr_io_type & IO_GET))
         {
            p_exp_val = &dc_val;
         }
         else if constexpr (bool(constexpr_io_type & IO_PUT))
         {
            p_exp_val = &init_nb_val;
         }
         else
         {
            static_assert(true, "Neither IO_GET nor IO_PUT was set.");
         }
         UNIT_ASSERT_EQUAL(mv_check[I_0], *p_exp_val, "DataIo failed (DC). "+err_str(storage));
         UNIT_ASSERT_EQUAL(nb_val, *p_exp_val, "DataIo failed (MMV). "+err_str(storage));

         // If getting to here, DataIo (probably) did the right thing, and
         // it's meaningful to check status.
         UNIT_ASSERT(status == io_t::SUCCESS, "Wrong status, expected SUCCESS. "+err_str(storage));
      }

      // In case we're doing IO_GET | IO_ROBUST, we can do something that's
      // supposed to fail, then check that we got the IO_ERROR return value.
      // InMem should always succeed (DataIo doesn't check for that at
      // least), but if OnDisc we can catch failure with IO_ROBUST.
      if constexpr(bool(constexpr_io_type & IO_ROBUST) && bool(constexpr_io_type & IO_GET))
      {
         // Default DataCont, with bogus label and on disk, then trying
         // to read from it; should fail.
         datacont_t dc;
         dc.NewLabel(std::string("no_way_this_file_is_on_disk_trollololz_io_")+std::to_string(int(constexpr_io_type)));
         dc.ChangeStorageTo("OnDisc");
         dc.SetNewSize(I_1);
         U nb_val(0);
         auto status = dc.DataIo(io, 0, nb_val);
         UNIT_ASSERT(status == io_t::ERROR,"Not IO_ERROR (with IO_ROBUST). "+err_str("OnDisc"));
      }
   }

   void run()
   {
      run_impl(IO_GET);
      run_impl(IO_PUT);
      run_impl(IO_GET | IO_ROBUST);
   }

};

/**
 *
 **/
struct HadamardProductTest: public cutee::test
{
   bool assert_product(DataCont& cont, DataCont& cont1, DataCont& cont2, Nb scal)
   {
      for(int i=0; i<cont.Size(); ++i)
      {
         // get elements
         Nb elem1, elem2;
         cont1.DataIo(IO_GET, i, elem1);
         cont2.DataIo(IO_GET, i, elem2);
         
         // do product
         Nb test_prod = scal*elem1*elem2;
         
         Nb prod;
         cont.DataIo(IO_GET, i, prod);
         // assert product
         if(!cutee::numeric::float_eq(prod,test_prod,2))
         {
            return false;
         }
      }

      return true;
   }

   /**
    * run
    **/
   void run() 
   {
      size_t size = 3;
      Nb scal = 2.0;
      DataCont cont = random::RandomDataCont<Nb>(size);
      DataCont cont1 = cont;
      DataCont cont2 = random::RandomDataCont<Nb>(size);

      cont.HadamardProduct(cont2,scal,DataCont::Product::Normal);

      UNIT_ASSERT(assert_product(cont,cont1,cont2,scal),"hadamard product not correct");
   }
};

/**
 * Hadamard inverse product test
 **/
struct HadamardInverseProductTest: public cutee::test
{
   bool assert_product(DataCont& cont, DataCont& cont1, DataCont& cont2, Nb scal)
   {
      for(int i=0; i<cont.Size(); ++i)
      {
         // get elements
         Nb elem1, elem2;
         cont1.DataIo(IO_GET, i, elem1);
         cont2.DataIo(IO_GET, i, elem2);
         
         // do product
         Nb test_prod = scal*elem1/elem2;
         
         Nb prod;
         cont.DataIo(IO_GET, i, prod);
         // assert product
         if(!cutee::numeric::float_eq(prod,test_prod,2))
         {
            return false;
         }
      }

      return true;
   }
   
   /**
    * run
    **/
   void run() 
   {
      size_t size = 3;
      Nb scal = 2.0;
      DataCont cont = random::RandomDataCont<Nb>(size);
      DataCont cont1 = cont;
      DataCont cont2 = random::RandomDataCont<Nb>(size);

      cont.HadamardProduct(cont2,scal,DataCont::Product::Inverse);

      UNIT_ASSERT(assert_product(cont,cont1,cont2,scal),"hadamard inverse product not correct");
   }
};

/**
 *
 **/
struct HadamardProductSelfTest: public cutee::test
{
   bool assert_product(DataCont& cont, DataCont& cont1, DataCont& cont2, Nb scal)
   {
      for(int i=0; i<cont.Size(); ++i)
      {
         // get elements
         Nb elem1, elem2;
         cont1.DataIo(IO_GET, i, elem1);
         cont2.DataIo(IO_GET, i, elem2);
         
         // do product
         Nb test_prod = scal*elem1*elem2;
         
         Nb prod;
         cont.DataIo(IO_GET, i, prod);
         // assert product
         if(!cutee::numeric::float_eq(prod,test_prod,2))
         {
            return false;
         }
      }

      return true;
   }

   /**
    * run
    **/
   void run() 
   {
      size_t size = 3;
      Nb scal = 2.0;
      DataCont cont = random::RandomDataCont<Nb>(size);
      DataCont cont1 = cont;

      cont.HadamardProduct(cont,scal,DataCont::Product::Normal);

      UNIT_ASSERT(assert_product(cont,cont1,cont1,scal),"hadamard product self not correct");
   }
};

/**
 *
 **/
struct HadamardInverseProductSelfTest: public cutee::test
{
   bool assert_product(DataCont& cont, DataCont& cont1, DataCont& cont2, Nb scal)
   {
      for(int i=0; i<cont.Size(); ++i)
      {
         // get elements
         Nb elem1, elem2;
         cont1.DataIo(IO_GET, i, elem1);
         cont2.DataIo(IO_GET, i, elem2);
         
         // do product
         Nb test_prod = scal*elem1/elem2;
         
         Nb prod;
         cont.DataIo(IO_GET, i, prod);
         // assert product
         if(!cutee::numeric::float_eq(prod,test_prod,2))
         {
            return false;
         }
      }

      return true;
   }

   /**
    * run
    **/
   void run() 
   {
      size_t size = 3;
      Nb scal = 2.0;
      DataCont cont = random::RandomDataCont<Nb>(size);
      DataCont cont1 = cont;

      cont.HadamardProduct(cont,scal,DataCont::Product::Inverse);

      UNIT_ASSERT(assert_product(cont,cont1,cont1,scal),"hadamard inverse product self not correct");
   }
};

/**
 *
 **/
struct HadamardProductVectorTest: public cutee::test
{
   bool assert_product(DataCont& cont, DataCont& cont1, const MidasVector& cont2, Nb scal)
   {
      for(int i=0; i<cont.Size(); ++i)
      {
         // get elements
         Nb elem1, elem2;
         cont1.DataIo(IO_GET, i, elem1);
         elem2 = cont2[i];
         
         // do product
         Nb test_prod = scal*elem1*elem2;
         
         Nb prod;
         cont.DataIo(IO_GET, i, prod);
         // assert product
         if(!cutee::numeric::float_eq(prod,test_prod,2))
         {
            return false;
         }
      }

      return true;
   }

   /**
    * run
    **/
   void run() 
   {
      size_t size = 3;
      Nb scal = 2.0;
      DataCont cont = random::RandomDataCont<Nb>(size);
      DataCont cont1 = cont;
      MidasVector cont2 = random::RandomMidasVector<Nb>(size);

      cont.HadamardProduct(cont2,scal,DataCont::Product::Normal);

      UNIT_ASSERT(assert_product(cont,cont1,cont2,scal),"hadamard product not correct");
   }
};

/***************************************************************************//**
 *
 ******************************************************************************/
struct MaxBufSizeTestBase
   :  public cutee::test
{
   public:
      void setup() override
      {
         mStoredgMaxBufSize = gMaxBufSize;
         gMaxBufSize = mTestgMaxBufSize;
      }

      void teardown() override
      {
         gMaxBufSize = mStoredgMaxBufSize;
      }

   protected:
      const Uin mSize = 23;

   private:
      Uin mStoredgMaxBufSize = 0;
      const Uin mTestgMaxBufSize = 3;
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  template<typename> class CONT_T
   ,  bool ThisInMem
   ,  bool OtherInMem = true
   >
struct LoopOverElementsAxpyTest
   :  public MaxBufSizeTestBase
{
   void run() override
   {
      using absval_t = midas::util::AbsVal_t<T>;
      const std::string filename_base = std::string("LoopOverElementsAxpyTest")
         +  "_" + midas::type_traits::TypeName<T>()
         +  "_" + midas::type_traits::TypeName<CONT_T>()
         +  "_" + (ThisInMem? "InMem": "OnDisc")
         +  "_" + (OtherInMem?"InMem": "OnDisc")
         ;
      const std::string filename_this = filename_base + "_this";
      const std::string filename_other = filename_base + "_other";

      // Setup.
      const auto alpha = random::RandomNumber<T>();
      GeneralDataCont<T> y = random::RandomVectorContainer<T,GeneralDataCont>{}(mSize);
      CONT_T<T> x = random::RandomVectorContainer<T,CONT_T>{}(mSize);

      UNIT_ASSERT(y.InMem() && !y.OnDisc(), "'this' not in memory initially.");
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         UNIT_ASSERT(x.InMem() && !x.OnDisc(), "'other' not in memory initially.");
      }

      // Control does the same operation using GeneralMidasVector.
      GeneralMidasVector<T> ctrl_y(y.Size());
      GeneralMidasVector<T> ctrl_x(x.Size());
      y.DataIo(IO_GET, ctrl_y, ctrl_y.Size());
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         x.DataIo(IO_GET, ctrl_x, ctrl_x.Size());
      }
      else
      {
         ctrl_x = x;
      }
      Axpy(ctrl_y, ctrl_x, alpha);

      // Put things on disk depending on template arguments.
      if constexpr(!ThisInMem)
      {
         y.NewLabel(filename_this);
         y.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(y.OnDisc() && !y.InMem(), "'this' should be on disk now.");
      }
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>> && !OtherInMem)
      {
         y.NewLabel(filename_other);
         x.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(x.OnDisc() && !x.InMem(), "'other' should be on disk now.");
      }

      // Then call LoopOverElements.
      const auto& cr_x = x;
      auto elem_axpy = [alpha](T& this_elem, const T& other_elem) -> void
         {
            this_elem += alpha*other_elem;
         };
      y.LoopOverElements(elem_axpy, cr_x);

      // Compare results.
      GeneralMidasVector<T> result(y.Size());
      y.DataIo(IO_GET, result, result.Size());
      const absval_t diffnorm = GeneralMidasVector<T>(result - ctrl_y).Norm2();
      const absval_t refnorm  = GeneralMidasVector<T>(ctrl_y).Norm2();
      UNIT_ASSERT_FZERO_PREC(diffnorm, refnorm, 2, "Non-zero diff.norm.");
   }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct LoopOverElementsScaleTest
   :  public MaxBufSizeTestBase
{
   void run() override
   {
      using absval_t = midas::util::AbsVal_t<T>;
      const std::string filename_base = std::string("LoopOverElementsScaleTest")
         +  "_" + midas::type_traits::TypeName<T>()
         +  "_" + (ThisInMem? "InMem": "OnDisc")
         ;
      const std::string filename_this = filename_base + "_this";
      const std::string filename_other = filename_base + "_other";

      // Setup.
      const auto alpha = random::RandomNumber<T>();
      GeneralDataCont<T> y = random::RandomVectorContainer<T,GeneralDataCont>{}(mSize);

      UNIT_ASSERT(y.InMem() && !y.OnDisc(), "'this' not in memory initially.");

      // Control does the same operation using GeneralMidasVector.
      GeneralMidasVector<T> ctrl_y(y.Size());
      y.DataIo(IO_GET, ctrl_y, ctrl_y.Size());
      Scale(ctrl_y, alpha);

      // Put things on disk depending on template arguments.
      if constexpr(!ThisInMem)
      {
         y.NewLabel(filename_this);
         y.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(y.OnDisc() && !y.InMem(), "'this' should be on disk now.");
      }

      // Then call LoopOverElements.
      auto elem_scale = [alpha](T& this_elem) -> void
         {
            this_elem *= alpha;
         };
      y.LoopOverElements(elem_scale);

      // Compare results.
      GeneralMidasVector<T> result(y.Size());
      y.DataIo(IO_GET, result, result.Size());
      const absval_t diffnorm = GeneralMidasVector<T>(result - ctrl_y).Norm2();
      UNIT_ASSERT_EQUAL(diffnorm, absval_t(0), "Non-zero diff.norm.");
   }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  template<typename> class CONT_T
   ,  bool ThisInMem
   ,  bool OtherInMem = true
   >
struct LoopOverElementsDotTest
   :  public MaxBufSizeTestBase
{
   void run() override
   {
      const std::string filename_base = std::string("LoopOverElementsDotTest")
         +  "_" + midas::type_traits::TypeName<T>()
         +  "_" + midas::type_traits::TypeName<CONT_T>()
         +  "_" + (ThisInMem? "InMem": "OnDisc")
         +  "_" + (OtherInMem?"InMem": "OnDisc")
         ;
      const std::string filename_this = filename_base + "_this";
      const std::string filename_other = filename_base + "_other";

      // Setup.
      GeneralDataCont<T> y = random::RandomVectorContainer<T,GeneralDataCont>{}(mSize);
      CONT_T<T> x = random::RandomVectorContainer<T,CONT_T>{}(mSize);

      UNIT_ASSERT(y.InMem() && !y.OnDisc(), "'this' not in memory initially.");
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         UNIT_ASSERT(x.InMem() && !x.OnDisc(), "'other' not in memory initially.");
      }

      // Control does the same operation using GeneralMidasVector.
      GeneralMidasVector<T> ctrl_y(y.Size());
      GeneralMidasVector<T> ctrl_x(x.Size());
      y.DataIo(IO_GET, ctrl_y, ctrl_y.Size());
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         x.DataIo(IO_GET, ctrl_x, ctrl_x.Size());
      }
      else
      {
         ctrl_x = x;
      }
      const auto ctrl_dot = Dot(ctrl_y, ctrl_x);

      // Put things on disk depending on template arguments.
      if constexpr(!ThisInMem)
      {
         y.NewLabel(filename_this);
         y.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(y.OnDisc() && !y.InMem(), "'this' should be on disk now.");
      }
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>> && !OtherInMem)
      {
         y.NewLabel(filename_other);
         x.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(x.OnDisc() && !x.InMem(), "'other' should be on disk now.");
      }

      // Then call LoopOverElements.
      const auto& cr_y = y;
      const auto& cr_x = x;
      T dot = 0;
      auto add_to_dot = [&dot](const T& this_elem, const T& other_elem) -> void
         {
            dot += midas::math::Conj(this_elem)*other_elem;
         };
      cr_y.LoopOverElements(add_to_dot, cr_x);

      // Compare results.
      UNIT_ASSERT_FEQUAL_PREC(dot, ctrl_dot, 16, "Wrong dot result.");
   }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct LoopOverElementsNorm2Test
   :  public MaxBufSizeTestBase
{
   private:
      const Uin mBeg = 0;
      const Uin mEnd = this->mSize;

   public:
      LoopOverElementsNorm2Test() {}
      LoopOverElementsNorm2Test(Uin aBeg, Uin aEnd): mBeg(aBeg), mEnd(aEnd) {}

      void run() override
      {
         using absval_t = midas::util::AbsVal_t<T>;
         const std::string filename_base = std::string("LoopOverElementsNorm2Test")
            +  "_" + midas::type_traits::TypeName<T>()
            +  "_" + (ThisInMem? "InMem": "OnDisc")
            +  std::to_string(mBeg) + "_" + std::to_string(mEnd)
            ;
         const std::string filename_this = filename_base + "_this";
         const std::string filename_other = filename_base + "_other";

         // Setup.
         GeneralDataCont<T> y = random::RandomVectorContainer<T,GeneralDataCont>{}(mSize);

         UNIT_ASSERT(y.InMem() && !y.OnDisc(), "'this' not in memory initially.");

         // Control does the same operation using GeneralMidasVector.
         GeneralMidasVector<T> ctrl_y(y.Size());
         y.DataIo(IO_GET, ctrl_y, ctrl_y.Size());
         const auto ctrl_norm2 = ctrl_y.Norm2(mBeg, mEnd-mBeg);

         // Put things on disk depending on template arguments.
         if constexpr(!ThisInMem)
         {
            y.NewLabel(filename_this);
            y.ChangeStorageTo("OnDisc");
            UNIT_ASSERT(y.OnDisc() && !y.InMem(), "'this' should be on disk now.");
         }

         // Then call LoopOverElements.
         const auto& cr_y = y;
         absval_t norm2 = 0;
         auto add_to_norm2 = [&norm2](const T& this_elem) -> void
            {
               norm2 += midas::util::AbsVal2(this_elem);
            };
         cr_y.LoopOverElements(add_to_norm2, mBeg, mEnd);

         // Compare results.
         UNIT_ASSERT_EQUAL(norm2, ctrl_norm2, "Wrong norm2 result.");
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  template<typename> class CONT_T
   ,  bool ThisInMem
   ,  bool OtherAInMem = true
   ,  bool OtherBInMem = true
   >
struct LoopOverElementsDoubleDiffNorm2Test
   :  public MaxBufSizeTestBase
{
   void run() override
   {
      using absval_t = midas::util::AbsVal_t<T>;
      const std::string filename_base = std::string("LoopOverElementsDoubleDiffNorm2Test")
         +  "_" + midas::type_traits::TypeName<T>()
         +  "_" + midas::type_traits::TypeName<CONT_T>()
         +  "_" + (ThisInMem? "InMem": "OnDisc")
         +  "_" + (OtherAInMem?"InMem": "OnDisc")
         +  "_" + (OtherBInMem?"InMem": "OnDisc")
         ;
      const std::string filename_this = filename_base + "_this";
      const std::string filename_other_a = filename_base + "_other_a";
      const std::string filename_other_b = filename_base + "_other_b";

      // Setup.
      GeneralDataCont<T> y = random::RandomVectorContainer<T,GeneralDataCont>{}(mSize);
      CONT_T<T> a = random::RandomVectorContainer<T,CONT_T>{}(mSize);
      CONT_T<T> b = random::RandomVectorContainer<T,CONT_T>{}(mSize);

      UNIT_ASSERT(y.InMem() && !y.OnDisc(), "'this' not in memory initially.");
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         UNIT_ASSERT(a.InMem() && !a.OnDisc(), "'other a' not in memory initially.");
         UNIT_ASSERT(b.InMem() && !b.OnDisc(), "'other b' not in memory initially.");
      }

      // Control does the same operation using GeneralMidasVector.
      GeneralMidasVector<T> ctrl_y(y.Size());
      GeneralMidasVector<T> ctrl_a(a.Size());
      GeneralMidasVector<T> ctrl_b(b.Size());
      y.DataIo(IO_GET, ctrl_y, ctrl_y.Size());
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         a.DataIo(IO_GET, ctrl_a, ctrl_a.Size());
         b.DataIo(IO_GET, ctrl_b, ctrl_b.Size());
      }
      else
      {
         ctrl_a = a;
         ctrl_b = b;
      }
      const auto ctrl_diffnorm2_a = GeneralMidasVector<T>(ctrl_y - ctrl_a).Norm2();
      const auto ctrl_diffnorm2_b = GeneralMidasVector<T>(ctrl_y - ctrl_b).Norm2();

      // Put things on disk depending on template arguments.
      if constexpr(!ThisInMem)
      {
         y.NewLabel(filename_this);
         y.ChangeStorageTo("OnDisc");
         UNIT_ASSERT(y.OnDisc() && !y.InMem(), "'this' should be on disk now.");
      }
      if constexpr(std::is_same_v<CONT_T<T>,GeneralDataCont<T>>)
      {
         if constexpr(!OtherAInMem)
         {
            a.NewLabel(filename_other_a);
            a.ChangeStorageTo("OnDisc");
            UNIT_ASSERT(a.OnDisc() && !a.InMem(), "'other a' should be on disk now.");
         }
         if constexpr(!OtherBInMem)
         {
            b.NewLabel(filename_other_b);
            b.ChangeStorageTo("OnDisc");
            UNIT_ASSERT(b.OnDisc() && !b.InMem(), "'other b' should be on disk now.");
         }
      }

      // Then call LoopOverElements.
      const auto& cr_y = y;
      const auto& cr_a = a;
      const auto& cr_b = b;
      absval_t diffnorm2_a = 0;
      absval_t diffnorm2_b = 0;
      auto add_to_diffnorm2s = 
         [&diffnorm2_a, &diffnorm2_b]
         (const T& this_elem, const T& a_elem, const T& b_elem)
         -> void
         {
            diffnorm2_a += midas::util::AbsVal2(a_elem - this_elem);
            diffnorm2_b += midas::util::AbsVal2(b_elem - this_elem);
         };
      cr_y.LoopOverElements(add_to_diffnorm2s, cr_a, cr_b);

      // Compare results.
      UNIT_ASSERT_EQUAL(diffnorm2_a, ctrl_diffnorm2_a, "Wrong diffnorm2_a.");
      UNIT_ASSERT_EQUAL(diffnorm2_b, ctrl_diffnorm2_b, "Wrong diffnorm2_b.");
   }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  typename RESULT_T
   ,  bool ThisInMem
   ,  Uin N_CONTAINERS
   >
struct CumulativeFunctionTestBase
   :  public MaxBufSizeTestBase
{
   protected:
      CumulativeFunctionTestBase() {}
      CumulativeFunctionTestBase(In aBeg, In aN): mBeg(aBeg), mN(aN) {}

      const In mBeg = 0;
      const In mN = -1;

   private:
      //! Generate a datacont base file name by trimming away non-alnum characters from test name.
      std::string FileName() const
      {
         std::string s = this->name();
         s.erase
            (  std::remove_if
                  (  s.begin()
                  ,  s.end()
                  ,  [](const auto& c)->bool {return ! ::isalnum(c);}
                  )
            ,  s.end()
            );
         return s;
      }

      //! Compute control using a GeneralMidasVector<T>.
      virtual RESULT_T GetCtrl(const std::vector<GeneralMidasVector<T>>&) const = 0;

      //! Compute result using a GeneralDataCont<T>.
      virtual RESULT_T GetResult(const std::vector<GeneralDataCont<T>>&) const = 0;

   public:
      void run() override
      {
         using result_t = RESULT_T;
         const std::string filename_base = FileName();
         const std::string filename_this = filename_base + "_this";

         // Setup.
         std::vector<GeneralDataCont<T>> v_gdc;
         std::vector<GeneralMidasVector<T>> v_gmv;
         for(Uin i = 0; i < N_CONTAINERS; ++i)
         {
            // Make random GeneralDataCont.
            v_gdc.emplace_back(random::RandomVectorContainer<T,GeneralDataCont>{}(mSize));
            UNIT_ASSERT(v_gdc.back().InMem() && !v_gdc.back().OnDisc(), "GDC not in mem. init.");

            // Make GeneralMidasVector and copy GeneralDataCont contents into it.
            GeneralMidasVector<T> tmp_mv(v_gdc.back().Size());
            v_gdc.back().DataIo(IO_GET, tmp_mv, tmp_mv.Size());
            v_gmv.emplace_back(std::move(tmp_mv));
         }

         // Control does the same operation using GeneralMidasVector.
         const result_t ctrl = GetCtrl(v_gmv);

         // Put things on disk depending on template arguments.
         // (For now we're putting either all or none on disk, but never just some.)
         if constexpr(!ThisInMem)
         {
            Uin index = 0;
            for(auto& gdc: v_gdc)
            {
               gdc.NewLabel(filename_this + "_" + std::to_string(index));
               gdc.ChangeStorageTo("OnDisc");
               UNIT_ASSERT(gdc.OnDisc() && !gdc.InMem(), "GDC should be on disk now.");
               ++index;
            }
         }

         // Then call function and compare.
         const result_t result = GetResult(v_gdc);

         UNIT_ASSERT_FEQUAL_PREC(result, ctrl, 13, "Bad result.");
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct NormTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  typename GeneralDataCont<T>::real_t
         ,  ThisInMem
         ,  1
         >
{
   public:
      using result_t = typename GeneralDataCont<T>::real_t;

      template<class... Args>
      NormTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 1>(std::forward<Args>(args)...)
      {}


   private:

      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return sqrt(a.at(0).Norm2(this->mBeg, this->mN));
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return a.at(0).Norm();
         }
         else
         {
            return a.at(0).Norm(this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct Norm2Test
   :  public CumulativeFunctionTestBase
         <  T
         ,  typename GeneralDataCont<T>::real_t
         ,  ThisInMem
         ,  1
         >
{
   public:
      using result_t = typename GeneralDataCont<T>::real_t;

      template<class... Args>
      Norm2Test(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 1>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return a.at(0).Norm2(this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return a.at(0).Norm2();
         }
         else
         {
            return a.at(0).Norm2(this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct SumEleTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  T
         ,  ThisInMem
         ,  1
         >
{
   public:
      using result_t = T;

      template<class... Args>
      SumEleTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 1>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return a.at(0).SumEle(this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return a.at(0).SumEle();
         }
         else
         {
            return a.at(0).SumEle(this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct SumAbsEleTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  typename GeneralDataCont<T>::real_t
         ,  ThisInMem
         ,  1
         >
{
   public:
      using result_t = typename GeneralDataCont<T>::real_t;

      template<class... Args>
      SumAbsEleTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 1>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         GeneralMidasVector<result_t> abs_copy(a.at(0).Size());
         for(Uin i = 0; i < abs_copy.Size(); ++i)
         {
            abs_copy[i] = midas::util::AbsVal(a.at(0)[i]);
         }
         return abs_copy.SumEle(this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return a.at(0).SumAbsEle();
         }
         else
         {
            return a.at(0).SumAbsEle(this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct DotTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  T
         ,  ThisInMem
         ,  2
         >
{
   public:
      using result_t = T;

      template<class... Args>
      DotTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 2>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return Dot(a.at(0), a.at(1), this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return Dot(a.at(0), a.at(1));
         }
         else
         {
            return Dot(a.at(0), a.at(1), this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct SumProdElemsTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  T
         ,  ThisInMem
         ,  2
         >
{
   public:
      using result_t = T;

      template<class... Args>
      SumProdElemsTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 2>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return SumProdElems(a.at(0), a.at(1), this->mBeg, this->mBeg + this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return SumProdElems(a.at(0), a.at(1));
         }
         else
         {
            return SumProdElems(a.at(0), a.at(1), this->mBeg, this->mBeg + this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   >
struct DiffNorm2Test
   :  public CumulativeFunctionTestBase
         <  T
         ,  typename GeneralDataCont<T>::real_t
         ,  ThisInMem
         ,  2
         >
{
   public:
      using result_t = typename GeneralDataCont<T>::real_t;

      template<class... Args>
      DiffNorm2Test(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 2>(std::forward<Args>(args)...)
      {}

   private:
      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return a.at(0).DiffNorm2(a.at(1), this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return DiffNorm2(a.at(0), a.at(1));
         }
         else
         {
            return a.at(0).DiffNorm2(a.at(1), this->mBeg, this->mN);
         }
      }
};

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   ,  bool ThisInMem
   ,  bool CONJ
   >
struct DiffNorm2ScaledTest
   :  public CumulativeFunctionTestBase
         <  T
         ,  typename GeneralDataCont<T>::real_t
         ,  ThisInMem
         ,  2
         >
{
   public:
      using result_t = typename GeneralDataCont<T>::real_t;

      template<class... Args>
      DiffNorm2ScaledTest(Args&&... args)
         :  CumulativeFunctionTestBase<T, result_t, ThisInMem, 2>(std::forward<Args>(args)...)
         ,  mScaleA(random::RandomNumber<T>())
         ,  mScaleB(random::RandomNumber<T>())
      {}

   private:
      const T mScaleA = 0;
      const T mScaleB = 0;

      //! Compute control using a GeneralMidasVector<T>.
      result_t GetCtrl(const std::vector<GeneralMidasVector<T>>& a) const override
      {
         return DiffNorm2Scaled<CONJ>(a.at(0), a.at(1), this->mScaleA, this->mScaleB, this->mBeg, this->mN);
      }

      //! Compute result using a GeneralDataCont<T>.
      result_t GetResult(const std::vector<GeneralDataCont<T>>& a) const override
      {
         if (this->mBeg == 0 && this->mN < 0)
         {
            return DiffNorm2Scaled<CONJ>(a.at(0), a.at(1), this->mScaleA, this->mScaleB);
         }
         else
         {
            return DiffNorm2Scaled<CONJ>(a.at(0), a.at(1), this->mScaleA, this->mScaleB, this->mBeg, this->mN);
         }
      }
};


/**
 *
 **/
template<typename T, bool ThisInMem>
struct DataContInsertTest: public cutee::test
{
   void run() override
   {
      GeneralDataCont<T> v;
      GeneralMidasVector<T> init(std::vector<T>{T(1),T(2),T(3),T(4)});
      v.Insert(0, init.begin(), init.end());
      if (!ThisInMem)
      {
         v.ChangeStorageTo("OnDisc");
      }
      std::vector<T> a = {-T(1), -T(2), -T(3), -T(4)};
      v.Insert(1, a.begin(), a.end()-1);
      v.Insert(v.Size()-1, a.at(3));

      // Insertions at end.
      v.Insert(v.Size(), a.begin()+1, a.begin()+2);
      v.Insert(v.Size(), a.at(2));

      // Empty range insertion.
      v.Insert(0, a.begin(), a.begin());

      GeneralDataCont<T> ctrl(GeneralMidasVector<T>(std::vector<T>{T(1), -T(1), -T(2), -T(3), T(2), T(3), -T(4), T(4), -T(2), -T(3)}));
      UNIT_ASSERT_EQUAL(Uin(v.Size()), Uin(ctrl.Size()), "Wrong Size().");
      Axpy(v,ctrl,-T(1));
      UNIT_ASSERT_EQUAL(v.Norm(), Nb(0), "Wrong diffnorm.");
   }
};

/**
 *
 **/
template< bool ThisInMem>
struct DataContEraseTest: public cutee::test
{
   void run() override
   {
      DataCont v(MidasVector(std::vector<Nb>{1,2,3,4,5,6}), ThisInMem? "InMem": "OnDisc");
      v.Erase(v.Size()-1, v.Size()+2); // Now {1,2,3,4,5}
      v.Erase(0,1);                    // Now {2,3,4,5}
      v.Erase(1,3);                    // Now {2,5}
      DataCont ctrl(MidasVector(std::vector<Nb>{2,5}));
      UNIT_ASSERT_EQUAL(Uin(v.Size()), Uin(ctrl.Size()), "Wrong Size().");
      Axpy(v,ctrl,-1.0);
      UNIT_ASSERT_EQUAL(v.Norm(), Nb(0), "Wrong diffnorm.");
   }
};

template<typename T>
struct DataContConjugateTest: public cutee::test
{
   void run() override
   {
      using dc_t = GeneralDataCont<T>;
      using vec_t = GeneralMidasVector<T>;
      const Uin size = 10;

      // Make control MidasVector.
      vec_t mv = random::RandomMidasVector<T>(size);
      // Load into DataCont.
      dc_t dc = dc_t(mv);
      // Ctrl = conjugate MidasVector.
      dc_t ctrl = dc_t(mv.Conjugate());
      // Result = conjugate DataCont.
      dc_t res = dc.Conjugate();
      // Compare ctrl and result.
      UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(res,ctrl)), Nb(1), 0, "Wrong DiffNorm.");
   }
};
     


} /* namespace datacont */

} /* namespace midas::test */

#endif /* DATACONTTEST_H_INCLUDED */
