/**
 *******************************************************************************
 * 
 * @file    PropertyFileStorageTest.cc
 * @date    09-10-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for PropertyFileStorage class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>

#include "PropertyFileStorageTest.h"

#include "mpi/FileStream.h"
#include "util/Error.h"
#include "util/FileSystem.h"

namespace midas::test
{
namespace property_file_storage
{

void BasePropertyFileStorageTest::WritePropertyFile
   (
   )
{
   // The current PropertyFileStorage impl. lets all ranks read in (possibly
   // flawed), so make sure all ranks write. (StreamType::MPI_ALL_DISTRIBUTED.)
   midas::mpi::OFileStream ofs(mFileName, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
   if (ofs)
   {
      ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);
      ofs << std::scientific;
      ofs.precision(mPrecision);
      try
      {
         for(const auto& line: mContents)
         {
            ofs   << std::get<0>(line) << "   "
                  << std::get<1>(line) << "   "
                  << std::get<2>(line) << "   "
                  << '\n';
         }
      }
      catch(const std::ofstream::failure& e)
      {
         MIDASERROR("Failure when writing to " + mFileName + ": " + e.what());
      }

      try
      {
         ofs.close();
      }
      catch(const std::ofstream::failure& e)
      {
         MIDASERROR("Failure when closing " + mFileName + ": " + e.what());
      }
   }
   else
   {
      MIDASERROR("Couldn't open for writing: " + mFileName);
   }
}

void BasePropertyFileStorageTest::RemovePropertyFile
   (
   )
{
   if (midas::filesystem::IsFile(mFileName))
   {
      auto ret_val = midas::filesystem::Remove(mFileName);
      if (ret_val != 0)
      {
         MIDASERROR
            (  "Failure when removing " 
            +  mFileName 
            +  " (return value: " 
            +  std::to_string(ret_val) 
            +  ")"
            );
      }
   }
}

void BasePropertyFileStorageTest::CheckPropertyLine
   (  const property_t& arVal
   ,  const contents_line_t& arExpect
   )
{
   using midas::util::ToUType;
   using property_fields = PropertyFileStorage::property_fields;
   CheckPropertyLine
      (  std::get<ToUType(property_fields::value)>(arVal)
      ,  std::get<ToUType(property_fields::calc_num)>(arVal)
      ,  arExpect
      );
}

void BasePropertyFileStorageTest::CheckPropertyLine
   (  const value_t& arVal
   ,  const calc_num_t& arLineNum
   ,  const contents_line_t& arExpect
   )
{
   UNIT_ASSERT_EQUAL(arVal, std::get<2>(arExpect), "Wrong value.");
   UNIT_ASSERT_EQUAL(arLineNum, std::get<0>(arExpect), "Wrong calc. number.");
}

void BasePropertyFileStorageTest::CheckPropertyFile
   (  const std::string& arFileName
   ,  const contents_t& arExpectedContents
   )
{
   std::ifstream ifs(mFileName);
   std::string s;
   if (ifs)
   {
      calc_num_t calc_num;
      calc_code_t calc_code;
      value_t value;

      // Simultaneously loop over lines of file and expected contents.
      auto it_cont = arExpectedContents.begin(), it_end = arExpectedContents.end();
      for( ; std::getline(ifs, s); ++it_cont)
      {
         // Start off by checking we didn't read more lines than expected.
         UNIT_ASSERT(it_cont != it_end, "Read unexpected line: " + s);

         // Then load contents of line and check values are correct.
         std::istringstream iss(s);
         iss.exceptions(std::istringstream::failbit | std::istringstream::badbit);
         try
         {
            // Read the (expected) three fields of the line.
            iss   >> calc_num
                  >> calc_code
                  >> value
                  ;
            // Compare with expected values.
            CheckPropertyLine(value, calc_num, *it_cont);
            UNIT_ASSERT_EQUAL(calc_code, std::get<1>(*it_cont), "Wrong calc. code.");
         }
         catch(const std::ifstream::failure& e)
         {
            MIDASERROR("Failure when reading from " + mFileName + ": " + e.what());
         }
      }

      // Now the iter. should be at end, otherwise there was too few lines.
      UNIT_ASSERT(it_cont == it_end, "Too few lines in file.");

      try
      {
         ifs.close();
      }
      catch(const std::ifstream::failure& e)
      {
         MIDASERROR("Failure when closing " + mFileName + ": " + e.what());
      }
   }
   else
   {
      MIDASERROR("Couldn't open for reading: " + mFileName);
   }
}

} /* namespace property_file_storage */

void PropertyFileStorageTest()
{
   cutee::suite suite("PropertyFileStorage test");
   
   /**************************************************************************************
    * 
    **************************************************************************************/
   using namespace property_file_storage;
   suite.add_test<Constructor>("Constructor");
   suite.add_test<ReadIntoAndClearFromMemory>("ReadIntoMemory, ClearFromMemory");
   suite.add_test<SearchOnDisc>("Search (SearchOnDisc)");
   suite.add_test<SearchInMemory>("Search (SearchInMemory)");
   suite.add_test<TestExtractPropertyLine>("Search (ExtractPropertyLine)");
   suite.add_test<AppendWhileOnDisc>("Append (AppendOnDisc)");
   suite.add_test<AppendWhileInMemory>("Append (AppendInMemory)");
   suite.add_test<AppendAndClearFromMemory>("Append and ClearFromMemory");
   suite.add_test<TestAppendPropertyLine>("Append (AppendPropertyLine)");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
