/**
 *******************************************************************************
 * 
 * @file    OdeTest.cc
 * @date    13-07-2018
 * @author  Niels Kristian Kjærgård Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Unit tests for ode/
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <iostream>

#include "OdeTest.h"

namespace midas::test
{

void OdeTest()
{
   cutee::suite suite("ODE test");
   
   /**************************************************************************************
    *
    **************************************************************************************/
   // sinc
   suite.add_test<DecayingExponentialTest<float>>("Integrate dy/dt = -y, float");
   suite.add_test<DecayingExponentialTest<double>>("Integrate dy/dt = -y, double");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
