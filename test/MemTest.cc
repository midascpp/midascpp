#include <iostream>

#include "test/MemTest.h"

namespace midas::test
{

void MemTest()
{
   cutee::suite suite("Mem test");
   
   ////////////////////////////////////////////////////////////////////////////////////
   // double tests
   ////////////////////////////////////////////////////////////////////////////////////
   suite.add_test<MemTestNoCorruption   <double> >("MEMORY TEST: NO CORRUPTION DOUBLE");
   suite.add_test<MemTestFrontCorruption<double> >("MEMORY TEST: FRONT CORRUPTION DOUBLE");
   suite.add_test<MemTestBackCorruption <double> >("MEMORY TEST: BACK CORRUPTION DOUBLE");
   suite.add_test<MemTestPointerSize    <double> >("MEMORY TEST: POINTER SIZE DOUBLE");
   suite.add_test<MemTestPointerAddress <double> >("MEMORY TEST: POINTER ADDRESS DOUBLE");
   suite.add_test<MemTestCurrentUse     <double> >("MEMORY TEST: CURRENT USE DOUBLE");
   suite.add_test<MemTestTimesNewDelete <double> >("MEMORY TEST: TIMES NEW DELETE DOUBLE");
   
   ////////////////////////////////////////////////////////////////////////////////////
   // integer tests
   ////////////////////////////////////////////////////////////////////////////////////
   suite.add_test<MemTestPointerSize   <int> >("MEMORY TEST: POINTER SIZE INTEGER");
   suite.add_test<MemTestPointerAddress<int> >("MEMORY TEST: POINTER ADDRESS INTEGER");
   suite.add_test<MemTestCurrentUse    <int> >("MEMORY TEST: CURRENT USE INTEGER");
   suite.add_test<MemTestTimesNewDelete<int> >("MEMORY TEST: TIMES NEW DELETE INTEGER");
   
   RunSuite(suite);
}

} /* namespace midas::test */
