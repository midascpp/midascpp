/**
************************************************************************
* 
* @file                OneModeDensitiesTest.cc
*
* Created:             28-02-2021
*
* Author:              Nicolai Machholdt Høyer
*
* Short Description:   Test the OneModeDensities class
* 
* Last modified:       28-02-2021
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>

// midas headers
#include "test/CuteeInterface.h"
#include "td/OneModeDensities.h"

namespace midas::test
{

namespace onemodedensitiestest
{

using real_t      = Nb;
using vec_real_t  = GeneralMidasVector<real_t>;
using mat_param_t = GeneralMidasMatrix<real_t>;

/**
 * Create some test densities
 */
std::vector<vec_real_t> GetTestDensities(In time_step = I_1)
{
    std::vector<vec_real_t> test_densities;
    
    // Set number of modes and grid steps
    In n_modes = I_2;
    In n_steps = I_10;

    for (In i_mode = I_0; i_mode < n_modes; ++i_mode)
    {
        vec_real_t density_for_mode(n_steps);
        for (In q = I_0; q < n_steps; ++q)
        {
            density_for_mode[q] = (i_mode+1) * q * C_1/C_2 * time_step;
        }
        test_densities.push_back(density_for_mode);
    }

    return test_densities;
}

/**
 * Fill in vectors holding a grid (not used) and test vscf modals.
 */
void FillTestVscfModalsAndGrid
    (   std::vector<vec_real_t>& aGridPoints
    ,   std::vector<std::vector<vec_real_t>>& aVscfModals)
{
    // Container for VSCF modals
    std::vector<std::vector<vec_real_t>> vscf_modals;

    // Set number of modes and time steps
    In n_steps = I_10;
    In n_prim_modals = I_5;

    std::vector<vec_real_t> grid_point;
    for (In q = I_0; q < n_steps; ++q)
    {
        vec_real_t modals(n_prim_modals);
        for (In i_modal = I_0; i_modal < n_prim_modals; ++i_modal)
        {
            modals[i_modal] = q * C_1/C_2 * (i_modal+1);
        }
        grid_point.push_back(modals);
    }
    vscf_modals.push_back(grid_point);

    //
    aGridPoints = grid_point;
    aVscfModals = vscf_modals;
}

/**
 * Create a vector holding a density matrix for each mode
 */
std::vector<mat_param_t> GetTestDens(In mat_size)
{
    std::vector<mat_param_t> dens_mats;
    mat_param_t density(mat_size, mat_size);

    for (In i = I_0; i < density.Nrows(); ++i)
    {
        for (In j = I_0; j < density.Ncols(); ++j)
        {
            density[i][j] = (i+C_1)*(j+C_1);
        }
    }

    dens_mats.push_back(density);
    return dens_mats;
}

/**
 * Create MCTDH modals
 * 2 active modals and 5 prim modals
 */
vec_real_t GetMctdhModals()
{
    vec_real_t modals(I_2*I_5);
    for (In i = I_0; i < modals.Size(); ++i)
    {
        modals[i] = (i+1) * C_1/C_7;
    }
    return modals;
}

/**
 * Create a vector with offsets corresponding to the MCTDH modals
 */
std::vector<std::vector<In>> GetMctdhOffsets()
{
    std::vector<std::vector<In>> offsets;
    std::vector<In> ivec;
    for (In i = I_0; i < I_2; ++i)
    {
        ivec.push_back(I_5);
    }
    offsets.push_back(ivec);
    return offsets;
}

/**
 * Create TDMVCC modal mats
 */
std::vector<std::pair<mat_param_t,mat_param_t>> GetTdmvccModalMats()
{
    std::vector<std::pair<mat_param_t,mat_param_t>> modal_mats;

    mat_param_t U(I_5, I_2);
    mat_param_t W(I_2, I_5);
    for (In i = I_0; i < U.Nrows(); ++i)
    {
        for (In j = I_0; j < U.Ncols(); ++j)
        {
            U[i][j] = (i+1)*C_1/C_2 + (j+1)*C_1/C_3;
            W[j][i] = (j+1)*C_1/C_2 + (i+1)*C_1/C_3;
        }
    }

    std::pair<mat_param_t,mat_param_t> mat_pair(U, W);
    modal_mats.push_back(mat_pair);

    return modal_mats;
}



/**
 * Test that space and time smoothening can be turned off
 */
struct OneModeDensitiesNoSpaceTimeSmooth
    : public cutee::test
{
    void run() override
    {
        // Initialize test object with no smoothening
        midas::td::OneModeDensities<Nb> one_mode_densities("one_mode_densities_test_object_no_smoothening");
        one_mode_densities.SetSpaceSmoothening(I_0);
        one_mode_densities.SetTimeSmoothening(I_0);        

        // Loop over two iterations
        for (In i = I_0; i < I_2; ++i)
        {
            // Create som densities to feed into one_mode_densities
            std::vector<vec_real_t> test_densities = GetTestDensities();

            // Supply precalculated densites 
            one_mode_densities.SupplyOneModeDensities(test_densities);
            const auto& dens = one_mode_densities.GetOneModeDensities();

            // Perform space-smoothening
            one_mode_densities.CalcSsOneModeDensities();
            const auto& ss_dens = one_mode_densities.GetSsOneModeDensities();

            // perfotm space-time-smoothening
            one_mode_densities.CalcStsOneModeDensities();
            const auto& sts_dens = one_mode_densities.GetStsOneModeDensities();

            // Check the norm of calculated densities
            for (In i_dens = I_0; i_dens < dens.size(); ++i_dens)
            {
                UNIT_ASSERT_FEQUAL_PREC(dens[i_dens].Norm()   , ss_dens[i_dens].Norm() , 0, "one-mode density and ss-one-mode density has differnt norms");
                UNIT_ASSERT_FEQUAL_PREC(ss_dens[i_dens].Norm(), sts_dens[i_dens].Norm(), 0, "ss-one-mode density and sts-one-mode density has differnt norms");
            }
        }
    }
};

/**
 * Test space-time-smoothening
 */
struct OneModeDensitiesSpaceTimeSmooth
    : public cutee::test
{
    void run() override
    {
        // Initialize object from string
        midas::td::OneModeDensities<Nb> one_mode_densities("one_mode_densities_test_object");
        // Set user defined smoothening parameters
        one_mode_densities.SetSpaceSmoothening(I_5);
        one_mode_densities.SetTimeSmoothening(I_2);

        // Vector holding reference values
        vec_real_t ref_vals = RefNorms();

        // Loop over three iterations
        for (In i = I_0; i < I_3; ++i)
        {
            // Create som densities to feed into one_mode_densities
            std::vector<vec_real_t> test_densities = GetTestDensities(i+1);

            // Supply precalculated densites 
            one_mode_densities.SupplyOneModeDensities(test_densities);
            const auto& dens = one_mode_densities.GetOneModeDensities();

            // Perform space-smoothening
            one_mode_densities.CalcSsOneModeDensities();
            const auto& ss_dens = one_mode_densities.GetSsOneModeDensities();

            // perfotm space-time-smoothening
            one_mode_densities.CalcStsOneModeDensities();
            const auto& sts_dens = one_mode_densities.GetStsOneModeDensities();

            // Check the norm of calculated densities
            for (In i_dens = I_0; i_dens < dens.size(); ++i_dens)
            {
                UNIT_ASSERT_FEQUAL_PREC(dens[i_dens].Norm()    , ref_vals[i*6 + i_dens*3]    , 0, "Wrong norm of one-mode density");
                UNIT_ASSERT_FEQUAL_PREC(ss_dens[i_dens].Norm() , ref_vals[i*6 + i_dens*3 + 1], 0, "Wrong norm of ss-one-mode density");
                UNIT_ASSERT_FEQUAL_PREC(sts_dens[i_dens].Norm(), ref_vals[i*6 + i_dens*3 + 2], 0, "Wrong norm of sts-one-mode density");
            }
        }
    }

    /**
     * Vector holding the norms to be testet against
     **/
    vec_real_t RefNorms()
    {
        vec_real_t ref_vals(I_3*I_3*I_2);

        ref_vals[0] = 8.4409715080670669e+00;
        ref_vals[1] = 7.3739406018763125e+00;
        ref_vals[2] = 7.3739406018763125e+00;
        ref_vals[3] = 1.6881943016134134e+01;
        ref_vals[4] = 1.4747881203752625e+01;
        ref_vals[5] = 1.4747881203752625e+01;
        ref_vals[6] = 1.6881943016134134e+01;
        ref_vals[7] = 1.4747881203752625e+01;
        ref_vals[8] = 1.1060910902814470e+01;
        ref_vals[9] = 3.3763886032268267e+01;
        ref_vals[10] = 2.9495762407505250e+01;
        ref_vals[11] = 2.2121821805628940e+01;
        ref_vals[12] = 2.5322914524201199e+01;
        ref_vals[13] = 2.2121821805628940e+01;
        ref_vals[14] = 1.4747881203752625e+01;
        ref_vals[15] = 5.0645829048402398e+01;
        ref_vals[16] = 4.4243643611257880e+01;
        ref_vals[17] = 2.9495762407505250e+01;
        
        return ref_vals;
    }
};

/**
 * Test Calculation of one-mode densities 
 */
struct OneModeDensitiesCalcDensities
    : public cutee::test
{
    void run() override
    {
        // Setup neccesary data
        // Create grid and VSCF modals
        std::vector<vec_real_t> grid_points;
        std::vector<std::vector<vec_real_t>> vscf_modals;
        FillTestVscfModalsAndGrid(grid_points, vscf_modals);

        // Initialize object from string and VSCF modals
        midas::td::OneModeDensities<Nb> one_mode_densities("one_mode_densities_test_object", grid_points, vscf_modals);
        
        // Create density matrix in primitive basis
        std::vector<mat_param_t> prim_dens_mat = GetTestDens(vscf_modals[0][0].Size());

        // Create density matrix in active basis
        std::vector<mat_param_t> act_dens_mat = GetTestDens(I_2);
        
        // Create MCTDH modal matrices and vector of offsets
        vec_real_t mctdh_modals = GetMctdhModals();
        std::vector<std::vector<In>> mctdh_offsets = GetMctdhOffsets();

        // Create TDMVCC modal matrices
        std::vector<std::pair<mat_param_t,mat_param_t>> tdmvcc_modal_mats = GetTdmvccModalMats();
        
        // Calculate densities
        // MCTDH
        one_mode_densities.CalcMctdhOneModeDensities(act_dens_mat, mctdh_modals, mctdh_offsets);
        const auto& mctdh_dens = one_mode_densities.GetOneModeDensities();
        const Nb refval_mctdh  = 9.6091884484030263e+04;
        UNIT_ASSERT_FEQUAL_PREC(mctdh_dens[0].Norm(), refval_mctdh, 1, "Error in norm of MCTDH one-mode density")

        // TDVCC
        one_mode_densities.CalcTdvccOneModeDensities(prim_dens_mat);
        const auto& tdvcc_dens = one_mode_densities.GetOneModeDensities();
        const Nb refval_tdvcc  = 9.3643784205426578e+04;
        UNIT_ASSERT_FEQUAL_PREC(tdvcc_dens[0].Norm(), refval_tdvcc, 1, "Error in norm of TDVCC one-mode density")

        // TDMVCC
        one_mode_densities.CalcTdmvccOneModeDensities(act_dens_mat, tdmvcc_modal_mats);
        const auto& tdmvcc_dens = one_mode_densities.GetOneModeDensities();
        const Nb refval_tdmvcc  = 3.0782491874139191e+05;
        UNIT_ASSERT_FEQUAL_PREC(tdmvcc_dens[0].Norm(), refval_tdmvcc, 1, "Error in norm of TDMVCC one-mode density")
    }
};

} /* namespace onemodedensitiestest */


void OneModeDensitiesTest()
{
    // Create test_suite object.
   cutee::suite suite("OneModeDensities test");
   
   using namespace onemodedensitiestest;
   suite.add_test<OneModeDensitiesNoSpaceTimeSmooth> ("OneModeDensitiesNoSpaceTimeSmooth test");
   suite.add_test<OneModeDensitiesSpaceTimeSmooth> ("OneModeDensitiesSpaceTimeSmooth test");
   suite.add_test<OneModeDensitiesCalcDensities> ("OneModeDensitiesCalcDensities test");
   // Run the tests.
   RunSuite(suite);
}

} /* namespace midas::test */
