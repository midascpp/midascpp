/**
 *******************************************************************************
 * 
 * @file    TestDrivers.h
 * @date    21-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For handling unit test drivers.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TESTDRIVERS_H_INCLUDED
#define TESTDRIVERS_H_INCLUDED

#include <functional>
#include <map>
#include <string>
#include <vector>

namespace midas::test
{
   /***************************************************************************//**
    * @brief
    *    Handles which unit test drivers to run.
    *
    * By default all drivers in AvailableDrivers() will be run (by being
    * returned by Drivers()). If specifically adding any drivers (using
    * AppendDriver()), _only_ those specified ones will be run.
    ******************************************************************************/
   class TestDrivers
   {
      public:
         using identifier_t = std::string;
         using driver_t = std::function<void()>;

         //! Default constructor; all of AvailableDrivers() active.
         explicit TestDrivers() = default;

         //! Append a specific driver to run (will then _only_ run specified ones).
         bool AppendDriver(identifier_t);

         //! Whether to run all test drivers.
         bool TestAll() const;

         //! The drivers to run, be it all or only some specific ones.
         const std::vector<driver_t> Drivers() const;

         //! Map of identifiers and bools (whether enabled).
         std::map<identifier_t, bool> Summary() const;

         //! Print list of available drivers.
         static void Help(std::ostream&);

      private:
         //! All possible drivers; initialized to AvailableDrivers() after parsing.
         static const std::map<identifier_t, driver_t> mTestDrivers;

         //! Identifieres for specific drivers, if any have been specified.
         std::vector<identifier_t> mSpecificDrivers;

         //! Whether argument identifies an actual driver in mTestDrivers.
         static bool IsValidDriver(const identifier_t&);

         //! Get map of all available drivers, for init. of mTestDrivers.
         static std::map<identifier_t, driver_t> AvailableDrivers();

         //! Parses an identifier, for uniformity.
         static identifier_t Parse(identifier_t);

         //! Parses identifier/driver map, for uniformity.
         static std::map<identifier_t, driver_t> Parse(std::map<identifier_t, driver_t>);
   };

} /* namespace midas::test */

#endif/*TESTDRIVERS_H_INCLUDED*/
