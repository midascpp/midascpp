/**
 *******************************************************************************
 * 
 * @file    MvccSolverTest.h
 * @date    28-10-2020
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MVCCSOLVERSTEST_H_INCLUDED
#define MVCCSOLVERSTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "ni/OneModeInt.h"
#include "input/VccCalcDef.h"
#include "vcc/Vcc.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "test/util/OpDefsForTesting.h"
#include "tensor/EnergyDifferencesTensor.h"
#include "it_solver/nl_solver/SubspaceSolver.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "lapack_interface/SYEVD.h"
#include "vcc/subspacesolver/MvccSolver.h"
#include "vcc/subspacesolver/VibCorrSubspaceSolverFactory.h"
#include "mmv/VectorAngle.h"
#include "test/util/MpiSyncedAssertions.h"
#include "test/CuteeInterface.h"
#include "td/tdvcc/trf/TrfTdmvcc2.h"
#include "td/tdvcc/trf/TrfTdmvccMatRep.h"
#include "td/tdvcc/params/ParamsTdvccUtils.h"
#include "td/tdvcc/trf/TrfTdvccUtils.h"

namespace midas::test::mvccsolver
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> typename TRF_TMPL
      >
   class MvccSolverTest
      :  public cutee::test
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         using real_t = midas::type_traits::RealTypeT<param_t>;

         using trf_t = TRF_TMPL<param_t>;
         using cont_t = typename trf_t::cont_t;
         using mat_t = GeneralMidasMatrix<param_t>;
         using sol_vec_t = typename midas::vcc::subspacesolver::MvccSolver<PARAM_T, TRF_TMPL>::sol_vec_t;
         using modal_cont_t = typename midas::vcc::subspacesolver::MvccSolver<PARAM_T, TRF_TMPL>::modal_cont_t;

      private:
         //@{
         //! Fixed settings for the molecular system.
         const Uin n_modes = 3;
         const Uin n_prim_modals_per_mode = 10;
         const Uin n_active_modals_per_mode = 3;
         const Uin oper_coup = 2;
         const Uin n_prim_bas = 15;
         const Uin max_exci = 2;
         const std::vector<In> n_prim_modals_in = std::vector<In>(n_modes, n_prim_modals_per_mode);
         const std::vector<Uin> n_prim_modals = std::vector<Uin>(n_modes, n_prim_modals_per_mode);
         const std::vector<In> n_active_modals_in = std::vector<In>(n_modes, n_active_modals_per_mode);
         const std::vector<Uin> n_active_modals = std::vector<Uin>(n_modes, n_active_modals_per_mode);
         //@}

         //@{
         //! Subspace solver settings.
         const Uin sub_dim = 500;
         const Uin max_it = 1000;
         const absval_t threshold = 1.0e-6;
         const bool crop_not_diis = false;
         const bool diag_precon = true;
         //@}

         //@{
         //! Misc settings.
         const bool unmute = true;
         bool mout_was_muted = false;
         //@}

         //@{
         //! Controls. (Energies are for oper_coup = 2, n_prim_bas = 15, max_exci = 2, n_prim_modals = 10, n_active_modals = 3.)
         const Nb ctrl_e_vscf = 2.10588489850353465e-02;
         const Nb ctrl_e_vcc = 2.0987894595305995E-02;
         //@}

         std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>>
         GetSolver
            (  const Vcc& arVccCalc
            ,  const std::vector<Uin>& aNActive
            ,  const std::vector<Uin>& aNPrim 
            ,  const OpDef& aOpDef
            ,  const ModalIntegrals<param_t>& aModInts
            ,  const ModeCombiOpRange& aMcr
            )  const
         {
            std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>> p = nullptr;
            if constexpr ( std::is_same_v<trf_t, midas::tdvcc::TrfTdmvcc2<param_t>> )
            {
               p = midas::vcc::subspacesolver::VibCorrSubspaceSolverFactory<param_t>
                  (  midas::tdvcc::CorrType::TDMVCC
                  ,  midas::tdvcc::TrfType::VCC2H2
                  ,  aNActive
                  ,  aNPrim
                  ,  aOpDef
                  ,  aModInts
                  ,  aMcr
                  );
            }
            else if constexpr ( std::is_same_v<trf_t, midas::tdvcc::TrfTdmvccMatRep<param_t>> )
            {
               p = midas::vcc::subspacesolver::VibCorrSubspaceSolverFactory<param_t>
                  (  midas::tdvcc::CorrType::TDMVCC
                  ,  midas::tdvcc::TrfType::MATREP
                  ,  aNActive
                  ,  aNPrim
                  ,  aOpDef
                  ,  aModInts
                  ,  aMcr
                  );
            }
            else
            {
               MIDASERROR("MvccSolverTest only implemented for VCC2H2 and MATREP");
            }
            
            p->SubspaceDim() = sub_dim;
            p->MaxIter() = max_it;
            p->ResNormThr() = threshold;
            p->IoLevel() = I_6;
            if (crop_not_diis) p->EnableCrop();
            if (diag_precon) p->EnableInvDiagJac0Precon(arVccCalc.GetEigVal(), arVccCalc.GetOccModalOffSet());
            return p;
         }

         std::array<absval_t, 5> ResidualNorm
            (  const std::tuple<cont_t, cont_t, modal_cont_t>& arSolVecs
            ,  const std::vector<Uin>& arNActiveModals
            ,  const std::vector<Uin>& arNPrimModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const
         {
            trf_t trf(arNActiveModals, arNPrimModals, arOpDef, arModInts, arMcr);
            const auto& s = std::get<0>(arSolVecs);
            const auto& l = std::get<1>(arSolVecs);
            const auto& modals = std::get<2>(arSolVecs);
            trf.UpdateIntegrals(modals);
            auto nmodes = arNActiveModals.size();

            // 1) Compute error vector; then extract dot(phase), set ref. elem to zero, multiply by -i.
            auto s_residual = trf.ErrVec(0, s);
            midas::tdvcc::ZeroFirstElemAndReturnIt(s_residual);

            // 2) Compute l_residual = eta + lA.
            auto l_residual = trf.EtaVec(0, s);
            Axpy(l_residual, trf.LJac(0, s, l), param_t(+1));
            midas::tdvcc::ZeroFirstElemAndReturnIt(l_residual);

            // 3) Zero all one-mode amplitudes
            auto s1norms = midas::tdvcc::ZeroOneModeAmplitudes(s_residual, arMcr);
            auto l1norms = midas::tdvcc::ZeroOneModeAmplitudes(l_residual, arMcr);

            // 4) Compute modal derivatives
            auto mean_fields = trf.MeanFieldMatrices(0, s, l); // Returns {H1, FR, H1', FR'}
            auto densmats = trf.DensityMatrices(s, l);
            modal_cont_t modals_residual(nmodes);
            absval_t max_ftilde_err = 0;
            for(In m=I_0; m<nmodes; ++m)
            {
               const auto& rho = densmats[m];
               const auto& u = modals[m].first;
               const auto& w = modals[m].second;

               // Q
               mat_t q = u*w;
               midas::mmv::WrapScale(q, param_t(-1));
               for(Uin alpha=I_0; alpha<q.Ncols(); ++alpha) q[alpha][alpha] += param_t(+1);

               mat_t f_check = mean_fields[m][0]*rho + mean_fields[m][1];
               mat_t fp_check = rho*mean_fields[m][2] + mean_fields[m][3];

               // NB: Here we do not use F-tilde = F'-tilde in order to check that all equations are converged
               modals_residual[m] = std::make_pair(mat_t(q*f_check), mat_t(fp_check*q));

               mat_t f_tilde = w*f_check;
               mat_t fp_tilde = fp_check*u;
               auto ftilde_err = std::sqrt(DiffNorm2(f_tilde, fp_tilde));
               max_ftilde_err = std::max(max_ftilde_err, ftilde_err);
            }

            return {midas::mmv::WrapNorm(s_residual), midas::mmv::WrapNorm(l_residual), midas::mmv::WrapNorm(modals_residual), max_ftilde_err, *std::max_element(l1norms.begin(), l1norms.end())};
         }

         Uin CtrlNIter
            (  bool aCropNotDiis
            ,  bool aDiagPrecon
            )
         {
            if (  !aCropNotDiis
               )
            {
               return 4;
            }
            else
            {
               MidasWarning("Correct number of iterations for CROP not known.");
               return 4;
            }
         }

         constexpr absval_t SvdInverseThreshold() const { return std::sqrt(std::numeric_limits<absval_t>::epsilon()); }

      public:
         MvccSolverTest
            (  bool aCrop = false
            ,  bool aDiagPrecon = true
            )
            :  crop_not_diis(aCrop)
            ,  diag_precon(aDiagPrecon)
         {
         }

         void setup() override
         {
            gOperatorDefs.ReSet();
            if (unmute)
            {
               mout_was_muted = Mout.Muted();
               Mout.Unmute();
            }
         }
         void teardown() override
         {
            if(unmute && mout_was_muted)
            {
               Mout.Mute();
            }
            gOperatorDefs.ReSet();
         }
         void run() override
         {
            using namespace midas::test::mpi_sync;
            if (unmute)
            {
               Mout << "test name = " << this->name() << std::endl;
            }

            // Setup.
            OpDef opdef = midas::test::util::WaterH0_ir_h2o_vci(oper_coup, true, false);
            BasDef basdef = midas::test::util::HoBasis(opdef, n_prim_bas, false);
            OneModeInt omi = midas::test::util::SetupOneModeInts(opdef, basdef);
            VccCalcDef calcdef = midas::test::util::SetupVccCalcDef(n_prim_modals_in, n_active_modals, max_exci, false);

            // VSCF.
            Vcc calc(&calcdef, &opdef, &basdef, &omi);
            midas::test::util::RunVscf(calc, !unmute);
            MPISYNC_UNIT_ASSERT(calc.Converged(), "VSCF not converged.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(calc.GetEtot(), ctrl_e_vscf, 10, "Wrong VSCF energy.");

            // Setup for vib.corr. calc.
            ModalIntegrals<real_t> modints_re = *calc.pIntegrals();;
            ModalIntegrals<param_t> modints = midas::vcc::modalintegrals::detail::ConvertFromReal<param_t>::Convert(*calc.pIntegrals());
            ModeCombiOpRange mcr(max_exci, n_modes);
            const Uin tot_size = SetAddresses(mcr, n_active_modals);

            // Run the iterative solver.
            auto p_solver = GetSolver(calc, n_active_modals, n_prim_modals, opdef, modints, mcr);
            bool ret_conv = p_solver->Solve();
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(ret_conv), Uin(p_solver->Conv()), "solver: ret_conv != Conv()");
            MPISYNC_UNIT_ASSERT(p_solver->Conv(), "solver: not converged.");
            //MPISYNC_UNIT_ASSERT_EQUAL(p_solver->NIter(), CtrlNIter(crop_not_diis, diag_precon), "solver: wrong num. iters.");

            // Check residual vector.
            const auto [s_err, l_err, modals_err, fterr, l1err] = ResidualNorm(p_solver->template SolVecs<sol_vec_t>(), n_active_modals, n_prim_modals, opdef, modints, mcr);
            {
               std::stringstream ss;
               ss << std::scientific << "(norm = " << s_err << ", thr = " << threshold << ")";
               MPISYNC_UNIT_ASSERT(s_err < threshold, "T error-vector norm >= thr "+ss.str());
            }
            {
               std::stringstream ss;
               ss << std::scientific << "(norm = " << l_err << ", thr = " << threshold << ")";
               MPISYNC_UNIT_ASSERT(l_err < threshold, "L error-vector norm >= thr "+ss.str());
            }
            {
               std::stringstream ss;
               ss << std::scientific << "(norm = " << modals_err << ", thr = " << threshold << ")";
               MPISYNC_UNIT_ASSERT(modals_err < threshold, "Modals error-vector norm >= thr "+ss.str());
            }
            MPISYNC_UNIT_ASSERT_FZERO_PREC(fterr, absval_t(1), 10, "0=F-tilde - F'-tilde not satisfied");
            MPISYNC_UNIT_ASSERT_FZERO_PREC(l1err, absval_t(1), 10, "L1 != 0 in converged MVCC solution");
         }
   };

} /* namespace midas::test::matrepsolver */

#endif/*MATREPSOLVERSTEST_H_INCLUDED*/

