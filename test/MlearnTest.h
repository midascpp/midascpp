/**
 *******************************************************************************
 * 
 * @file    MlearnTest.h 
 * @date    14-09-2020 
 * @author  Denis G. Artiukhin (artiukhin@chem.au.dk)
 *
 * @brief
 *    Unit tests for routines in mlearn folder.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MLEARNTEST_H_INCLUDED 
#define MLEARNTEST_H_INCLUDED 

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/CuteeInterface.h"
#include "mlearn/mlutil.h"
#include "mlearn/MLDescriptor.h"

namespace midas::test::mlearn_test 
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Test that GetNumberOfHyperParameters() function returns correctly
    ***************************************************************************/
   struct TestGetNumberOfHyperParameters 
      :  public cutee::test
   {
      void run() override
      {
          
         std::vector<std::string> kernel_types {   "OUEXP", "MATERN32", "MATERN52", "SQEXP", 
                                                   "SC32", "SC52", "SCEXP", 
                                                   "POLYNOM", 
                                                   "PERIODIC", "ANOVA", "RATQUAD", 
                                                   "SOAP",  
                                                   "RBF"   };
       
         std::vector<In> num_hparam_correct = {   2, 2, 2, 2,
                                                  7, 7, 7,
                                                  4, 
                                                  3, 3, 3,  
                                                  7,  
                                                  6   };

          // 6 internal coordinates
          In nintern = 6;
          In num_layer;
          
          // use as many layers as many kernel types you have
          num_layer = kernel_types.size();
          std::vector<In> res = mlearn::GetNumberOfHyperParameters<Nb>(kernel_types, nintern, num_layer);
          
          for (int i=0; i<num_layer; ++i)
          {
             UNIT_ASSERT_EQUAL(res[i], num_hparam_correct[i], "The number of hyperparametes is not right for kernel" + kernel_types[i] + "."); 
          }

      }
   };


   /************************************************************************//**
    * @brief
    *    Constructing MLDescriptor object for further testing
    *    of the corresponding member functions
    ***************************************************************************/
   struct MLDescriptorConstructed
      :  public cutee::test
   {
       
      MLDescriptorConstructed()
      {
         third.DefineAsGradElem(I_1); 
      };

      // First
      std::vector<Nb> data1 = {   2.2966185592572628e+00,  2.1079973291664178e+00, 
                                  2.1366660779608653e+00,  2.1079973291876204e+00,
                                  2.1366660777709336e+00, -1.0513256931687920e-12   };  
       
      MLDescriptor<Nb> first{data1, MLDescriptor<Nb>::SCALAR};
   
      // Second
      std::vector<Nb> data2 = {  -2.6360621197030580e-02,  0.0000000000000000e+00,  7.3981992423321485e-01, 
                                 -1.2218974491030747e+00,  0.0000000000000000e+00, -2.0770046547136758e-01,
                                  1.6402596791494333e+00,  0.0000000000000000e+00, -5.3211946821047795e-01   };

      std::vector<std::string> elemlist2 = {   "o", "h", "h"   };
      int lmax2 = 8;
      
      MLDescriptor<Nb> second{data2, MLDescriptor<Nb>::ATOMISTIC, lmax2, elemlist2};
      
      // Third
      std::vector<Nb> data3 = {  -2.6360621197030580e-02,  0.0000000000000000e+00,  7.3981992423321485e-01, 
                                 -1.2218974491030747e+00,  0.0000000000000000e+00, -2.0770046547136758e-01,
                                  1.6402596791494333e+00,  0.0000000000000000e+00, -5.3211946821047795e-01   };

      MLDescriptor<Nb> third{data3, MLDescriptor<Nb>::SCALAR, I_10};
      // DGA: lmax will be overwrittem by 8, not sure it is intended, but ok...
   
   };


   
   /************************************************************************//**
    * @brief
    *    Test that GetNatoms() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetNatoms 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
         
         UNIT_ASSERT_EQUAL(first.GetNatoms(),  0, "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetNatoms(), 3, "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetNatoms(),  0, "Third object: Strict integer equality failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Test that GetLmax() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetLmax 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         UNIT_ASSERT_EQUAL(first.GetLmax(),  8,  "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetLmax(), 8,  "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetLmax(),  8, "Third object: Strict integer equality failed.");
      }
   };


   /************************************************************************//**
    * @brief
    *    Test that GetDerivativeOrder() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetDerivativeOrder 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         UNIT_ASSERT_EQUAL(first.GetDerivativeOrder(),  0, "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetDerivativeOrder(), 0, "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetDerivativeOrder(),  1, "Third object: Strict integer equality failed.");
      }
   };

   
   /************************************************************************//**
    * @brief
    *    Test that GetDerivCoord1() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetDerivCoord1 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         UNIT_ASSERT_EQUAL(first.GetDerivCoord1(),  -1, "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetDerivCoord1(), -1, "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetDerivCoord1(),   1, "Third object: Strict integer equality failed.");
      }
   };


   /************************************************************************//**
    * @brief
    *    Test that GetDerivCoord1() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetDerivCoord2 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         UNIT_ASSERT_EQUAL(first.GetDerivCoord2(),  -1, "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetDerivCoord2(), -1, "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetDerivCoord2(),  -1, "Third object: Strict integer equality failed.");
      }
   };


   /************************************************************************//**
    * @brief
    *    Test that GetDerivCoord1() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetMaxNeighbours 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         UNIT_ASSERT_EQUAL(first.GetMaxNeighbours(),  0, "First object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(second.GetMaxNeighbours(), 2, "Second object: Strict integer equality failed.");
         UNIT_ASSERT_EQUAL(third.GetMaxNeighbours(),  0, "Third object: Strict integer equality failed.");
      }
   };


   /************************************************************************//**
    * @brief
    *    Test that GetAlpha() function from MLDescriptor returns correctly
    ***************************************************************************/
   struct TestMLDescriptorGetAlpha 
      :  public MLDescriptorConstructed
   {
      void run() override
      {
          
         // UNIT_ASSERT_EQUAL(first.GetAlpha(),  1, "First object: Strict integer equality failed.");   // should give a seg fault
         UNIT_ASSERT_EQUAL(second.GetAlpha(), 1, "Second object: Strict integer equality failed.");
         // UNIT_ASSERT_EQUAL(third.GetAlpha(),  1, "Third object: Strict integer equality failed.");   // should give a seg fault

      }
   };

} /* namespace midas::test::mlearn_test */ 

#endif/*MLEARNTEST_H_INCLUDED*/ 
