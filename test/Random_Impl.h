/**
 *******************************************************************************
 * 
 * @file    Random_Impl.h
 * @date    14-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Random data for testing.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TEST_RANDOM_IMPL_H_INCLUDED
#define MIDAS_TEST_RANDOM_IMPL_H_INCLUDED


#include "util/RandomNumberGenerator.h"
#include "util/Math.h"
#include "util/type_traits/Complex.h"
#include "util/AbsVal.h"
#include "mpi/Impi.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"

namespace midas::test::random
{

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
T RandomNumber
   (  bool aMpiSync
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   constexpr bool is_bool = std::is_same_v<T,bool>;
   constexpr bool is_int = std::is_integral_v<T> && !is_bool;
   constexpr bool is_float = std::is_floating_point_v<real_t>;
   static_assert(is_bool || is_int || is_float, "Expected bool, int or float.");
   if constexpr(is_bool)
   {
      return RandomNumber<int>(aMpiSync) % 2 == 1;
   }
   else
   {
      T val;
      if constexpr(is_int)
      {
         val = midas::util::RandomInt<T>();
      }
      else
      {
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            val = T(midas::util::RandomSignedFloat<real_t>(), midas::util::RandomSignedFloat<real_t>());
         }
         else
         {
            val = midas::util::RandomSignedFloat<real_t>();
         }
      }
      if (aMpiSync)
      {
#ifdef VAR_MPI
         midas::mpi::detail::WRAP_Bcast
            (  &val
            ,  1
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  midas::mpi::MasterRank()
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
#endif /* VAR_MPI */
      }
      return val;
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
std::vector<T> RandomStdVector
   (  const std::size_t aSize
   ,  bool aMpiSync
   )
{
   std::vector<T> v;
   v.reserve(aSize);
   for(std::size_t i = 0; i < aSize; ++i)
   {
      v.emplace_back(RandomNumber<T>(false));
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      midas::mpi::detail::WRAP_Bcast
         (  v.data()
         ,  v.size()
         ,  midas::mpi::DataTypeTrait<T>::Get()
         ,  midas::mpi::MasterRank()
         ,  midas::mpi::CommunicatorWorld().GetMpiComm()
         );
#endif /* VAR_MPI */
   }
   return v;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> RandomMidasVector
   (  const std::size_t aSize
   ,  bool aMpiSync
   )
{
   GeneralMidasVector<T> v(aSize);
   for(std::size_t i = 0; i < v.Size(); ++i)
   {
      v[i] = RandomNumber<T>(false);
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      v.MpiBcast(midas::mpi::MasterRank());
#endif /* VAR_MPI */
   }
   return v;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasMatrix<T> RandomMidasMatrix
   (  const std::size_t aNrows
   ,  const std::size_t aNcols
   ,  bool aMpiSync
   )
{
   GeneralMidasMatrix<T> m(aNrows, aNcols, T(0));
   for(std::size_t i = 0; i < m.Nrows(); ++i)
   {
      for(std::size_t j = 0; j < m.Ncols(); ++j)
      {
         m[i][j] = RandomNumber<T>(false);
      }
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      GeneralMidasVector<T> v(m.Nrows() * m.Ncols());
      v.MatrixRowByRow(m);
      v.MpiBcast(midas::mpi::MasterRank());
      m.MatrixFromVector(v);
#endif /* VAR_MPI */
   }
   return m;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasMatrix<T> RandomHermitianMidasMatrix
   (  const std::size_t aDim
   ,  bool aMpiSync
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;

   GeneralMidasMatrix<T> m(aDim, aDim, T(0));
   for(std::size_t i = 0; i < m.Nrows(); ++i)
   {
      m[i][i] = RandomNumber<real_t>(false);
      for(std::size_t j = i+1; j < m.Ncols(); ++j)
      {
         const auto val = RandomNumber<T>(false);
         m[i][j] = val;
         m[j][i] = midas::math::Conj(val);
      }
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      GeneralMidasVector<T> v(m.Nrows() * m.Ncols());
      v.MatrixRowByRow(m);
      v.MpiBcast(midas::mpi::MasterRank());
      m.MatrixFromVector(v);
#endif /* VAR_MPI */
   }
   return m;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasMatrix<T> RandomAntiHermitianMidasMatrix
   (  const std::size_t aDim
   ,  bool aMpiSync
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;

   GeneralMidasMatrix<T> m(aDim, aDim, T(0));
   for(std::size_t i = 0; i < m.Nrows(); ++i)
   {
      if constexpr(midas::type_traits::IsComplexV<T>)
      {
         m[i][i] = T(real_t(0), RandomNumber<real_t>(false));
      }
      for(std::size_t j = i+1; j < m.Ncols(); ++j)
      {
         const auto val = RandomNumber<T>(false);
         m[i][j] = val;
         m[j][i] = -midas::math::Conj(val);
      }
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      GeneralMidasVector<T> v(m.Nrows() * m.Ncols());
      v.MatrixRowByRow(m);
      v.MpiBcast(midas::mpi::MasterRank());
      m.MatrixFromVector(v);
#endif /* VAR_MPI */
   }
   return m;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralDataCont<T> RandomDataCont
   (  const std::size_t aSize
   ,  bool aMpiSync
   )
{
   GeneralDataCont<T> dc(aSize);
   dc.DataIo(IO_PUT, RandomMidasVector<T>(aSize, aMpiSync), aSize);
   return dc;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void LoadRandom
   (  T* t
   ,  const std::size_t s
   ,  bool aMpiSync
   )
{
   for(std::size_t i = 0; i < s; ++i)
   {
      t[i] = RandomNumber<T>(false);
   }
   if (aMpiSync)
   {
#ifdef VAR_MPI
      midas::mpi::detail::WRAP_Bcast
         (  t
         ,  s
         ,  midas::mpi::DataTypeTrait<T>::Get()
         ,  midas::mpi::MasterRank()
         ,  midas::mpi::CommunicatorWorld().GetMpiComm()
         );
#endif /* VAR_MPI */
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<typename T>
std::vector<T> RandomVectorContainer<T,std::vector>::operator()
   (  const std::size_t aSize
   ,  bool aMpiSync
   )  const
{
   return RandomStdVector<T>(aSize, aMpiSync);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> RandomVectorContainer<T,GeneralMidasVector>::operator()
   (  const std::size_t aSize
   ,  bool aMpiSync
   )  const
{
   return RandomMidasVector<T>(aSize, aMpiSync);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<typename T>
GeneralDataCont<T> RandomVectorContainer<T,GeneralDataCont>::operator()
   (  const std::size_t aSize
   ,  bool aMpiSync
   )  const
{
   return RandomDataCont<T>(aSize, aMpiSync);
}

} /* namespace midas::test::random */

#endif/*MIDAS_TEST_RANDOM_IMPL_H_INCLUDED*/
