/**
 *******************************************************************************
 * 
 * @file    Vcc2TransTest.cc
 * @date    23-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "util/type_traits/Complex.h"
#include "util/type_traits/TypeName.h"
#include "util/Error.h"
#include "test/Random.h"
#include "test/Vcc2TransTest.h"

namespace midas::test
{
   namespace vcc2trans::detail
   {
      /*********************************************************************//**
       * @param[in,out] arSuite
       *    Adds tests to this test suite, using the template parameters that
       *    this function is called with.
       ************************************************************************/
      template
         <  typename T
         >
      void AddTestsImpl
         (  cutee::suite& arSuite
         )
      {
         std::string s = "("+midas::type_traits::TypeName<T>()+") ";

         // Basic tests.
         arSuite.add_test<DetailGetOpDefTest<T>>(s+"DetailGetOpDefTest");

         // Special debug tests, that is/were used to show-case/fail for/test
         // certain specific bugs in the code.
         using MCR_T = typename Vcc2TransTestBase<T>::MCR_T;
         std::map<MCR_T, std::string> m_debug_tests;
         m_debug_tests[MCR_T::DebugLJacA] = "DebugLJacA";
         m_debug_tests[MCR_T::NonDeexcClsOperA] = "NonDeexcClsOperA";
         m_debug_tests[MCR_T::NonDeexcClsOperB] = "NonDeexcClsOperB";
         m_debug_tests[MCR_T::NonDeexcClsOperC] = "NonDeexcClsOperC";
         //m_debug_tests[MCR_T::NonDeexcClsAmpMcrA] = "NonDeexcClsAmpMcrA"; // Fail; MC {} missing
         //m_debug_tests[MCR_T::NonDeexcClsAmpMcrB] = "NonDeexcClsAmpMcrB"; // Fail; 1-MC {0} missing
         //m_debug_tests[MCR_T::NonDeexcClsAmpMcrC] = "NonDeexcClsAmpMcrC"; // Fail; 1-MC {1} missing
         for(const auto& mcr_t: m_debug_tests)
         {
            arSuite.add_test<Vcc2ErrVec<T>>(s+mcr_t.second+", Vcc2TransReg (VCC)", mcr_t.first);
            arSuite.add_test<Vci2Trans<T>> (s+mcr_t.second+", Vcc2TransReg (VCI) ", mcr_t.first);
            arSuite.add_test<Vcc2RJac<T>>  (s+mcr_t.second+", Vcc2TransRJac", mcr_t.first);
            arSuite.add_test<Vcc2Eta<T>>   (s+mcr_t.second+", Vcc2TransEta", mcr_t.first);
            arSuite.add_test<Vcc2LJac<T>>  (s+mcr_t.second+", Vcc2TransLJac", mcr_t.first);
         }

         // Various settings to use for transformer tests. Loop through these.
         using settings_t =
            std::tuple
               <  std::vector<Uin>  // num. modals
               ,  std::vector<Uin>  // num. operators
               ,  Uin               // max. exci. level for amplitudes/coefficients
               ,  Uin               // max. coupling level in operator
               >;
         std::vector<settings_t>  v_settings =
         {  {  {2,2}, {1,1}, 1, 1}
         ,  {  {2,2}, {1,1}, 1, 2}
         ,  {  {2,2}, {1,1}, 2, 1}
         ,  {  {2,2}, {1,1}, 2, 2}
         ,  {  {2,2,2}, {1,1,1}, 2, 2}
         ,  {  {3,3,3}, {2,2,2}, 2, 2}
         ,  {  {2,3,2}, {2,3,2}, 2, 2}
         ,  {  {3,2,3,2}, {2,4,2,3}, 2, 2}
         };

         // For pretty-printing info about test arguments/settings.
         auto info = []
            (  const std::string& m
            ,  const settings_t& t
            )  ->std::string
            {
               std::stringstream ss;
               ss << "NModals = " << std::get<0>(t)
                  << ", NOpers = " << std::get<1>(t)
                  << ", " << m << "[" << std::get<2>(t) << "]/H" << std::get<3>(t)
                  ;
               return ss.str();
            };

         // Loop through settings, calling different tests with each.
         for(const auto& t: v_settings)
         {
            const auto& n_modals = std::get<0>(t);
            const auto& n_opers  = std::get<1>(t);
            const auto& max_exci = std::get<2>(t);
            const auto& max_coup = std::get<3>(t);

            arSuite.add_test<Vcc2ErrVec<T>>
               (  s+"Vcc2TransReg;  "+info("VCC",t)
               ,  n_modals
               ,  n_opers
               ,  max_exci
               ,  max_coup
               );
            arSuite.add_test<Vci2Trans<T>>
               (  s+"Vcc2TransReg;  "+info("VCI",t)
               ,  n_modals
               ,  n_opers
               ,  max_exci
               ,  max_coup
               );
            arSuite.add_test<Vcc2RJac<T>>
               (  s+"Vcc2TransRJac; "+info("RJac",t)
               ,  n_modals
               ,  n_opers
               ,  max_exci
               ,  max_coup
               );
            arSuite.add_test<Vcc2Eta<T>>
               (  s+"Vcc2TransEta;  "+info("Eta",t)
               ,  n_modals
               ,  n_opers
               ,  max_exci
               ,  max_coup
               );
            arSuite.add_test<Vcc2LJac<T>>
               (  s+"Vcc2TransLJac; "+info("LJac",t)
               ,  n_modals
               ,  n_opers
               ,  max_exci
               ,  max_coup
               );
            if (max_exci == 2)
            {
               arSuite.add_test<ExplicitJacobian<T>>
                  (  s+"Expl. Jacobian "+info("R/L-Jacobian",t)
                  ,  n_modals
                  ,  n_opers
                  ,  max_exci
                  ,  max_coup
                  );
            }
         }

         // Tests for complex/real numbers _only_.
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
         }
         else
         {
         }
		}

   } /* namespace vcc2trans::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void Vcc2TransTest()
   {
      // Create cutee::suite object.
      cutee::suite suite("Vcc2Trans test");

      // Add all the tests to the suite.
      using vcc2trans::detail::AddTestsImpl;
      AddTestsImpl<Nb>(suite);
      AddTestsImpl<std::complex<Nb>>(suite);


      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
