/**
 *******************************************************************************
 * 
 * @file    VectorAngleTest.h
 * @date    18-03-2020 
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef VECTORANGLETEST_H_INCLUDED
#define VECTORANGLETEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/VectorAngle.h"
#include "test/Random.h"

#include "test/CuteeInterface.h"

namespace midas::test::vectorangle
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
      //! Convert MidasVector to CONT_T; specialize for e.g. GeneralTensorDataCont at some point.
      template<template<typename> class CONT_T, typename T>
      CONT_T<T> ConvertToContType(const GeneralMidasVector<T>& arV)
      {
         return CONT_T<T>(arV);
      }
   }

   /************************************************************************//**
    * @brief
    *    Tests equality with acos(<u|v>/(N(u)N(v))) for "easy" cases (u and v
    *    quite different), but possibly of different norm and complex phase.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestEasy
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;
         const Uin ulps = 4;

         // Slightly random vectors, with angle about 1 radian.
         // All positive numbers, makes it rather numerically stable for the
         // testing.
         const absval_t rnd_fact = .01;
         mv_t u_mv(std::vector<T>{T(.2), T(.2), T(.8)});
         mv_t v_mv(std::vector<T>{T(.8), T(.2), T(.2)});
         u_mv += rnd_fact * random::RandomMidasVector<T>(u_mv.Size());
         v_mv += rnd_fact * random::RandomMidasVector<T>(v_mv.Size());
         u_mv.Normalize();
         v_mv.Normalize();

         const absval_t cs = sqrt(midas::util::AbsVal2(Dot(u_mv,v_mv))/(Norm2(u_mv)*Norm2(v_mv)));
         const absval_t ctrl = acos(cs);

         // Convert to CONT_T, compute value and control.
         // Should be invariant under scaling by (complex) factor.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "Init.; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "Init.; wrong VectorAngle(v,u).");

         Scale(u,random::RandomNumber<T>());
         Scale(v,random::RandomNumber<T>());

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "After Scale; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "After Scale; wrong VectorAngle(v,u).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with theta = pi/2 for orthogonal case.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestOrthg
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;
         const Uin ulps = 4;

         mv_t u_mv(std::vector<T>{ .6, .8});
         mv_t v_mv(std::vector<T>{-.8, .6});

         const absval_t ctrl = C_PI/2.;

         // Convert to CONT_T, compute value and control.
         // Should be invariant under scaling by (complex) factor.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "Init.; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "Init.; wrong VectorAngle(v,u).");

         Scale(u,random::RandomNumber<T>());
         Scale(v,random::RandomNumber<T>());

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "After Scale; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "After Scale; wrong VectorAngle(v,u).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with theta = 0 for parallel case.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestParallel
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin size = 2;
         mv_t u_mv = random::RandomMidasVector<T>(size);
         u_mv.Normalize();
         mv_t v_mv = u_mv;

         // Convert to CONT_T, compute value and control.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

         absval_t ref = std::max(u.Norm(), v.Norm());

         UNIT_ASSERT_FZERO_PREC(VectorAngle(u,v), ref, 1, "Init.; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FZERO_PREC(VectorAngle(v,u), ref, 1, "Init.; wrong VectorAngle(v,u).");

         // The angle should be invariant under scaling by a (complex) factor.
         Scale(u,random::RandomNumber<T>());
         Scale(v,random::RandomNumber<T>());

         ref = std::max(u.Norm(), v.Norm());

         UNIT_ASSERT_FZERO_PREC(VectorAngle(u,v), ref, 4, "After Scale; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FZERO_PREC(VectorAngle(v,u), ref, 4, "After Scale; wrong VectorAngle(v,u).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with theta = pi/2 for zero vector case.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestZeroVec
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin size = 2;
         mv_t u_mv = random::RandomMidasVector<T>(size);
         mv_t z_mv(size, T(0));
         mv_t e_mv(0);

         // Value of pi/2, signifying that the zero vec. is orthg. to any other vec. (incl. itself).
         const absval_t ctrl = C_PI/2.;

         // Convert to CONT_T, compute value and control.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> z = detail::ConvertToContType<CONT_T>(z_mv);
         CONT_T<T> e = detail::ConvertToContType<CONT_T>(e_mv);

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,z), ctrl, 0, "Init.; wrong VectorAngle(u,z).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(z,u), ctrl, 0, "Init.; wrong VectorAngle(z,u).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(z,z), ctrl, 0, "Init.; wrong VectorAngle(z,z).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(e,e), ctrl, 0, "Init.; wrong VectorAngle(e,e).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with acos(<u|v>/(N(u)N(v))) -> ||u'-v'|| for
    *    near-parallel case. (u', v' normalized)
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestNearParallelA
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin ulps = 16;
         const absval_t eps = std::numeric_limits<absval_t>::epsilon();
         const absval_t e = .5*sqrt(eps);
         mv_t u_mv(std::vector<T>{T(1), T(0)});
         mv_t v_mv(std::vector<T>{T(1), T(e)});
         // Exact math:
         // CS(u,v) = |<u|v>|/(N(u)N(v))
         // = 1/(1*sqrt(1 + e^2))
         // = (1 + e^2)^(-1/2)
         // ~ 1 - 1/2 e^2 + O(e^4)
         // theta = acos(CS(u,v)) ~ acos(1 - 1/2 e^2)
         // = e(1 + 1/24 e^2 + ...)

         // Numerically (with e = 1/2 eps):
         // CS(u,v) = (1 + e^2)^(-1/2) = (1 + 1/4 eps)^(-1/2) = (1)^(-1/2) = 1 (and theta = 0, wrong!)
         // theta = e(1 + 1/24 e^2 + ...) = e(1 + 1/96 eps + ...) = e

         const absval_t ctrl = e;

         // Convert to CONT_T, compute value and control.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "Init.; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "Init.; wrong VectorAngle(v,u).");

         Scale(u,random::RandomNumber<T>());
         Scale(v,random::RandomNumber<T>());

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl, ulps, "After Scale; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl, ulps, "After Scale; wrong VectorAngle(v,u).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with acos(<u|v>/(N(u)N(v))) -> ||u'-v'|| for
    *    near-parallel case. (u', v' normalized)
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestNearParallelB
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin ulps = 16;
         const absval_t eps = std::numeric_limits<absval_t>::epsilon();
         const mv_t u_mv(std::vector<T>{T(1), T(4)*eps*random::RandomNumber<T>()});
         const mv_t v_mv(std::vector<T>{u_mv[0]*(T(1)+T(4)*eps*random::RandomNumber<T>()), T(4)*eps*random::RandomNumber<T>()});
         const absval_t dnorm = sqrt(DiffNorm2(u_mv,v_mv));

         const absval_t ctrl = dnorm;

         // Convert to CONT_T, compute value and control.
         CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

         UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(u,v) - ctrl), absval_t(1), ulps, "Init.; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(v,u) - ctrl), absval_t(1), ulps, "Init.; wrong VectorAngle(v,u).");

         Scale(u,random::RandomNumber<T>());
         Scale(v,random::RandomNumber<T>());

         UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(u,v) - ctrl), absval_t(1), ulps, "After Scale; wrong VectorAngle(u,v).");
         UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(v,u) - ctrl), absval_t(1), ulps, "After Scale; wrong VectorAngle(v,u).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests equality with given angle for a broad range of angles.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestRange
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin ulps = 8;
         const absval_t eps = std::numeric_limits<absval_t>::epsilon();
         const mv_t u_mv(std::vector<T>{1, 0});

         for(absval_t theta = 1.; theta >= eps; theta /= 2)
         {
            mv_t v_mv(std::vector<T>{cos(theta), sin(theta)});
            CONT_T<T> u = detail::ConvertToContType<CONT_T>(u_mv);
            CONT_T<T> v = detail::ConvertToContType<CONT_T>(v_mv);

            UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(u,v) - theta), absval_t(1), ulps, "Init.; wrong VectorAngle(u,v), theta = "+std::to_string(theta)+".");
            UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(v,u) - theta), absval_t(1), ulps, "Init.; wrong VectorAngle(v,u), theta = "+std::to_string(theta)+".");

            Scale(u,random::RandomNumber<T>());
            Scale(v,random::RandomNumber<T>());

            UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(u,v) - theta), absval_t(1), ulps, "After Scale; wrong VectorAngle(u,v), theta = "+std::to_string(theta)+".");
            UNIT_ASSERT_FZERO_PREC(std::abs(VectorAngle(v,u) - theta), absval_t(1), ulps, "After Scale; wrong VectorAngle(v,u), theta = "+std::to_string(theta)+".");
         }
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests with/without conjugation of elements of first argument.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestConj
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;
         constexpr bool is_complex = midas::type_traits::IsComplexV<T>;

         const Uin ulps = 2;
         mv_t u_mv(std::vector<T>{1, 1});
         mv_t v_mv(std::vector<T>{1, 1});
         if constexpr(is_complex)
         {
            u_mv[1] += T(0,-1);
            v_mv[1] += T(0,+1);
         }
         mv_t uc_mv = u_mv.Conjugate();

         // For T real they're parallel for both CONJ = true,false.
         // For T complex,
         //    u  = (1, 1-i)
         //    u* = (1, 1+i)
         //    v  = (1, 1+i)
         // CS(u,v)  = |<u|v>|/(|u|*|v|)
         // <u|v>    = 1 + (1-i)^* (1+i) = 1 + (1 - 1) + 2i = 1 + 2i
         // |<u|v>|  = sqrt(5)
         // |u|=|v|  = sqrt(3)
         // CS(u,v)  = sqrt(5)/3
         // theta    = acos(sqrt(5)/3)
         //
         // <u*|v>   = 1 + (1+i)^* (1+i) = 3
         // |u*|=|v| = sqrt(3)
         // CS(u,v)  = 1
         // theta    = acos(1) = 0

         absval_t ctrl_reg  = 0;
         absval_t ctrl_conj = 0;
         if constexpr(is_complex)
         {
            ctrl_reg = acos(sqrt(5)/3);
         }

         CONT_T<T> u  = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> uc = detail::ConvertToContType<CONT_T>(uc_mv);
         CONT_T<T> v  = detail::ConvertToContType<CONT_T>(v_mv);

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(u,v), ctrl_reg, ulps, "!CONJ, wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,u), ctrl_reg, ulps, "!CONJ, wrong VectorAngle(v,u).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(uc,v), ctrl_conj, ulps, "!CONJ, wrong VectorAngle(uc,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle(v,uc), ctrl_conj, ulps, "!CONJ, wrong VectorAngle(v,uc).");

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle<true>(u,v), ctrl_conj, ulps, "CONJ, wrong VectorAngle(u,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle<true>(v,u), ctrl_conj, ulps, "CONJ, wrong VectorAngle(v,u).");

         UNIT_ASSERT_FEQUAL_PREC(VectorAngle<true>(uc,v), ctrl_reg, ulps, "CONJ, wrong VectorAngle(uc,v).");
         UNIT_ASSERT_FEQUAL_PREC(VectorAngle<true>(v,uc), ctrl_reg, ulps, "CONJ, wrong VectorAngle(v,uc).");

      }
   };

   /************************************************************************//**
    * @brief
    *    Tests for slices of input vectors.
    ***************************************************************************/
   template<typename T, template<typename> class CONT_T>
   struct VectorAngleTestSlice
      :  public cutee::test
   {
      void run() override
      {
         using mv_t = GeneralMidasVector<T>;
         using absval_t = midas::type_traits::RealTypeT<T>;

         const Uin size = 5;
         const Uin beg = 1;
         const Uin end = 4;
         const Uin subsize = end - beg;
         const mv_t u_mv = random::RandomMidasVector<T>(size);
         const mv_t v_mv = random::RandomMidasVector<T>(size);
         mv_t u_slice(subsize);
         mv_t v_slice(subsize);
         u_mv.PieceIo(IO_GET, u_slice, u_slice.Size(), beg);
         v_mv.PieceIo(IO_GET, v_slice, v_slice.Size(), beg);

         const absval_t ctrl_a = VectorAngle<false>(u_slice, v_slice);
         const absval_t ctrl_b = VectorAngle<true>(u_slice, v_slice);
         const absval_t ctrl_z = C_PI/2;

         CONT_T<T> u  = detail::ConvertToContType<CONT_T>(u_mv);
         CONT_T<T> v  = detail::ConvertToContType<CONT_T>(v_mv);
         const absval_t val_a = VectorAngle<false>(u, v, beg, end);
         const absval_t val_b = VectorAngle<true>(u, v, beg, end);
         const absval_t val_za = VectorAngle<false>(u, v, beg, beg);
         const absval_t val_zb = VectorAngle<true>(u, v, beg, beg);

         UNIT_ASSERT_FEQUAL_PREC(val_za, ctrl_z, 0, "Wrong VectorAngle, empty slice, !CONJ");
         UNIT_ASSERT_FEQUAL_PREC(val_zb, ctrl_z, 0, "Wrong VectorAngle, empty slice, CONJ");
         UNIT_ASSERT_FEQUAL_PREC(val_a , ctrl_a, 2, "Wrong VectorAngle, slice, !CONJ");
         UNIT_ASSERT_FEQUAL_PREC(val_b , ctrl_b, 0, "Wrong VectorAngle, slice, CONJ");
      }
   };

} /* namespace midas::test::vectorangle */

#endif/*VECTORANGLETEST_H_INCLUDED*/ //CHANGE! header guard
