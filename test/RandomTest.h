/**
 *******************************************************************************
 * 
 * @file    RandomTest.h
 * @date    06-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef RANDOMTEST_H_INCLUDED
#define RANDOMTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "test/Random.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mpi/Impi.h"
#include "test/util/MpiSyncedAssertions.h"
#include "test/CuteeInterface.h"

namespace midas::test::random_test
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
      template<typename U>
      struct mpisafe_value
      {
         using type = U;
      };
      template<>
      struct mpisafe_value<bool>
      {
         using type = int;
      };
      template<typename U>
      using mpisafe_value_t = typename mpisafe_value<U>::type;

      template<typename T>
      midas::type_traits::RealTypeT<T> DiffNorm2(const std::vector<T>& a, const std::vector<T>& b)
      {
         midas::type_traits::RealTypeT<T> s = 0;
         for(Uin i = 0; i < a.size(); ++i)
         {
            s += midas::util::AbsVal2(a.at(i) - b.at(i));
         }
         return s;
      }

   }

   /************************************************************************//**
    * @brief
    *    Base class with setup/teardown MPI barriers, for use with MPI tests.
    ***************************************************************************/
   struct RandomTestMpiSyncBase
      :  public cutee::test
   {
      void setup() override
      {
         midas::mpi::Barrier();
      }
      void teardown() override
      {
         midas::mpi::Barrier();
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomNumberTest
      :  public RandomTestMpiSyncBase
   {
      void run() override
      {
#ifdef VAR_MPI
         using namespace midas::test::mpi_sync;
         constexpr bool is_bool = std::is_same_v<T,bool>;
         constexpr bool is_int = std::is_integral_v<T> && !is_bool;
         using value_t = detail::mpisafe_value_t<T>;
         const value_t v_own = value_t(random::RandomNumber<T>());
         value_t v_master = v_own;
         midas::mpi::detail::WRAP_Bcast
            (  &v_master
            ,  1
            ,  midas::mpi::DataTypeTrait<value_t>::Get()
            ,  midas::mpi::MasterRank()
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
         if constexpr(is_bool || is_int)
         {
            MPISYNC_UNIT_ASSERT_EQUAL(v_own, v_master, "Not MPI-synced.");
         }
         else
         {
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(v_own, v_master, 0, "Not MPI-synced.");
         }
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomStdVectorTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         const std::vector<T> v_own = random::RandomStdVector<T>(size);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(v_own.size()), size, "Wrong size().");

#ifdef VAR_MPI
         std::vector<T> v_master = v_own;
         midas::mpi::detail::WRAP_Bcast
            (  v_master.data()
            ,  v_master.size()
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  midas::mpi::MasterRank()
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.size(), v_master.size(), "Not MPI-synced: Wrong size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(detail::DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomMidasVectorTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         const GeneralMidasVector<T> v_own = random::RandomMidasVector<T>(size);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(v_own.Size()), size, "Wrong Size().");

#ifdef VAR_MPI
         GeneralMidasVector<T> v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());
         
         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Not MPI-synced: Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomMidasMatrixTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin rows = 3;
         const Uin cols = 4;
         const GeneralMidasMatrix<T> m = random::RandomMidasMatrix<T>(rows,cols);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Nrows()), rows, "Wrong Nrows().");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Ncols()), cols, "Wrong Ncols().");

#ifdef VAR_MPI
         GeneralMidasVector<T> v_own(m.Nrows() * m.Ncols());
         v_own.MatrixRowByRow(m);
         GeneralMidasVector<T> v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Not MPI-synced: Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomHermitianMidasMatrixTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         const GeneralMidasMatrix<T> m = random::RandomHermitianMidasMatrix<T>(size);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Nrows()), size, "Wrong Nrows().");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Ncols()), size, "Wrong Ncols().");
         MPISYNC_UNIT_ASSERT(m.IsHermitian(0), "Not Hermitian.");
         if constexpr(!midas::type_traits::IsComplexV<T>)
         {
            MPISYNC_UNIT_ASSERT(m.IsSymmetric(0), "Not symmetric.");
         }

#ifdef VAR_MPI
         GeneralMidasVector<T> v_own(m.Nrows() * m.Ncols());
         v_own.MatrixRowByRow(m);
         GeneralMidasVector<T> v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Not MPI-synced: Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomAntiHermitianMidasMatrixTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         const GeneralMidasMatrix<T> m = random::RandomAntiHermitianMidasMatrix<T>(size);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Nrows()), size, "Wrong Nrows().");
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(m.Ncols()), size, "Wrong Ncols().");
         MPISYNC_UNIT_ASSERT(m.IsAntiHermitian(0), "Not anti-Hermitian.");
         if constexpr(!midas::type_traits::IsComplexV<T>)
         {
            UNIT_ASSERT(m.IsAntiSymmetric(0), "Not anti-symmetric.");
         }

#ifdef VAR_MPI
         GeneralMidasVector<T> v_own(m.Nrows() * m.Ncols());
         v_own.MatrixRowByRow(m);
         GeneralMidasVector<T> v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Not MPI-synced: Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct RandomDataContTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         const GeneralDataCont<T> dc = random::RandomDataCont<T>(size);
         MPISYNC_UNIT_ASSERT_EQUAL(Uin(dc.Size()), size, "Wrong Size().");
         MPISYNC_UNIT_ASSERT(dc.InMem(), "Data not in memory.");
         MPISYNC_UNIT_ASSERT(!dc.OnDisc(), "Data on disk.");

#ifdef VAR_MPI
         GeneralMidasVector<T> v_own(dc.Size());
         dc.DataIo(IO_GET, v_own, v_own.Size());
         GeneralMidasVector<T> v_master = v_own;
         v_master.MpiBcast(midas::mpi::MasterRank());

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.Size(), v_master.Size(), "Not MPI-synced: Wrong Size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   struct LoadRandomTest
      :  public RandomTestMpiSyncBase
   {
      using absval_t = midas::type_traits::RealTypeT<T>;

      void run() override
      {
#ifdef VAR_MPI
         using namespace midas::test::mpi_sync;
         const Uin size = 3;
         std::vector<T> v_own(size);
         random::LoadRandom(v_own.data(), v_own.size());

         using namespace midas::test::mpi_sync;
         std::vector<T> v_master = v_own;
         midas::mpi::detail::WRAP_Bcast
            (  v_master.data()
            ,  v_master.size()
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  midas::mpi::MasterRank()
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );

         MPISYNC_UNIT_ASSERT_EQUAL(v_own.size(), v_master.size(), "Not MPI-synced: Wrong size().");
         MPISYNC_UNIT_ASSERT_FZERO_PREC(detail::DiffNorm2(v_own,v_master), absval_t(0), 0, "Not MPI-synced: Inequality.");
#endif /* VAR_MPI */
      }
   };

} /* namespace midas::test::random_test */

#endif/*RANDOMTEST_H_INCLUDED*/
