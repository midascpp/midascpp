#ifndef GENERICFUNCTIONTESTS_H
#define GENERICFUNCTIONTESTS_H

#include "test/CuteeInterface.h"

#include "libmda/util/type_info.h"
#include "libmda/util/to_string.h"

#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/generalfunctions/FunctionContainer.h"

namespace midas::test
{

/**
 *
 */
template<int M> struct pow_internal {
   template<class U> static U apply(U u) {
      return u * pow_internal<M-1>::apply(u);
   }
};

template<> struct pow_internal<0> {
   template<class U> static U apply(U u) {
      return U(1);
   }
};

template<int N, class T>
struct RaiseTest : public cutee::test
{
   T m_arg;
   RaiseTest(T a_arg = 3.14159265359) : cutee::test(), m_arg(a_arg) { }
   void run() 
   {
      FunctionContainer<T> func_cont;
      GenericFunctionWrapper<T> gen_func("Q^"+std::to_string(N),func_cont);
      gen_func.SetVariableI(m_arg,0);

      UNIT_ASSERT_FEQUAL(gen_func.EvaluateFunc(),std::pow(m_arg,N)
                       , "Generic function failed against std::pow for N = "+std::to_string(N) + " and T = " + libmda::util::type_of<T>());
      UNIT_ASSERT_FEQUAL(gen_func.EvaluateFunc(),pow_internal<N>::apply(m_arg)
                       , "Generic function failed internal power function for N = " + std::to_string(N) + " and T = " + libmda::util::type_of<T>());
   }
};

/**
 *
 **/
#define MAKE_GENERIC_UNARY_FUNCTION_TEST(NAME,GENFUNC,STDFUNC) \
template<class T> \
struct NAME##Test : public cutee::test { \
   T m_arg; \
   NAME##Test(T a_arg = 3.14159265359): cutee::test(), m_arg(a_arg) { } \
   void run()  { \
      FunctionContainer<T> func_cont; \
      GenericFunctionWrapper<T> gen_func(#GENFUNC "(Q)",func_cont); \
      gen_func.SetVariableI(m_arg,0); \
      \
      UNIT_ASSERT_FEQUAL(gen_func.EvaluateFunc(),std::STDFUNC(m_arg) \
                       , "Generic function failed for unary function " #NAME \
                         ", with argument " + libmda::util::to_string_with_precision(m_arg)); \
   } \
};

// trigonometri
MAKE_GENERIC_UNARY_FUNCTION_TEST(Cos,COS,cos)
MAKE_GENERIC_UNARY_FUNCTION_TEST(Sin,SIN,sin)
MAKE_GENERIC_UNARY_FUNCTION_TEST(Tan,TAN,tan)

// other stuff
MAKE_GENERIC_UNARY_FUNCTION_TEST(Exp,EXP,exp)
MAKE_GENERIC_UNARY_FUNCTION_TEST(Sqrt,SQRT,sqrt)

#undef MAKE_GENERIC_UNARY_FUNCTION_TEST

/**
 *
 **/
#define MAKE_GENERIC_BINARY_OPERATOR_TEST(NAME,OPER) \
template<class T> \
struct NAME##Test : public cutee::test { \
   T m_lhs, m_rhs; \
   NAME##Test(T a_lhs = 3.14159265359, T a_rhs = 1.61803398875): cutee::test(), m_lhs(a_lhs), m_rhs(a_rhs) { } \
   void run()  { \
      FunctionContainer<T> func_cont; \
      GenericFunctionWrapper<T> gen_func("Q" #OPER + libmda::util::to_string_with_precision(m_rhs),func_cont); \
      gen_func.SetVariableI(m_lhs,0); \
      \
      UNIT_ASSERT_FEQUAL(gen_func.EvaluateFunc(),(m_lhs OPER m_rhs) \
                       , "Generic function failed for binary operator" #OPER \
                         ", with arguments " + libmda::util::to_string_with_precision(m_lhs) + \
                         " and " + libmda::util::to_string_with_precision(m_rhs) \
                       ); \
   } \
};

MAKE_GENERIC_BINARY_OPERATOR_TEST(Plus,+)
MAKE_GENERIC_BINARY_OPERATOR_TEST(Minus,-)
MAKE_GENERIC_BINARY_OPERATOR_TEST(Prod,*)
MAKE_GENERIC_BINARY_OPERATOR_TEST(Divide,/)

#undef MAKE_GENERIC_BINARY_OPERATOR_TEST

/**
 *
 **/
#define MAKE_GENERIC_UNARY_OPERATOR_TEST(NAME,OPER) \
template<class T> \
struct NAME##Test : public cutee::test { \
   T m_arg; /* argument member */\
   NAME##Test(T a_arg = 3.14159265359): cutee::test(), m_arg(a_arg) { } /* ctor */\
   void run()  { \
      FunctionContainer<T> func_cont; \
      GenericFunctionWrapper<T> gen_func(#OPER "Q",func_cont); \
      gen_func.SetVariableI(m_arg,0); \
      \
      UNIT_ASSERT_FEQUAL(gen_func.EvaluateFunc(),(OPER m_arg) \
                       , "Generic function failed for unary operator" #OPER \
                         ", with argument " + libmda::util::to_string_with_precision(m_arg)); \
   } \
};

//MAKE_GENERIC_UNARY_OPERATOR_TEST(UnaryPlus,+) // unary plus not implemented
MAKE_GENERIC_UNARY_OPERATOR_TEST(UnaryMinus,-)

#undef MAKE_GENERIC_BINARY_OPERATOR_TEST


/**
 *
 */
struct TestGenericFunctions : public cutee::test
{
   Nb fx(Nb aQ)
   {
      return cos(aQ)*exp(aQ+5.1)/(aQ+13.6*(aQ+1.1));
   }
   void run() 
   {
      FunctionContainer<Nb> func_cont;
      GenericFunctionWrapper<Nb> gen_fx("COS(Q)*EXP(Q+5.1)/(Q+13.6*(Q+1.1))",func_cont);
      for(In i = I_0; i < 50; ++i)
      {
         gen_fx.SetVariableI(i/15.5, I_0);
         UNIT_ASSERT_FEQUAL_PREC(gen_fx.EvaluateFunc(), fx(i/15.5), 2, "Function evaluation failed for i = " + std::to_string(i));
      }
   }
};

struct TestGenericFunctionsStrongPrec : public cutee::test{
   Nb fx(Nb aQ){
      return aQ*aQ;
   }
   void run() 
   {
      FunctionContainer<Nb> func_cont;
      GenericFunctionWrapper<Nb> gen_fx("Q*Q",func_cont);
      for(In i = I_0; i < 50; ++i)
      {
         Nb x = i/15.5;
         gen_fx.SetVariableI(x, I_0);
         UNIT_ASSERT_FEQUAL_PREC(gen_fx.EvaluateFunc(), fx(x), 2, "Failed for i = " + std::to_string(i));
      }
   }
};

struct TestMidasFunctions : public cutee::test{
   Nb mZero;
   TestMidasFunctions() : cutee::test(), mZero(std::cos(Nb(0.0))*std::exp(Nb(5.1))/(Nb(13.6)*(Nb(1.1)))) {}
   void run() 
   {
      FunctionContainer<Nb> func_cont;
      MidasFunctionWrapper<Nb> mgen_fx("COS(Q)*EXP(Q+5.1)/(Q+13.6*(Q+1.1))", func_cont, false);
      for(In i = I_0; i < 50; ++i)
      {
         mgen_fx.SetVariableI(i/15.5, I_0);
         UNIT_ASSERT_FEQUAL_PREC(mgen_fx.EvaluateFunc(), fx0(Nb(i)/15.5), 256, "Function evaluation failed for i = " + std::to_string(i));
      }
   }

   Nb fx0(const Nb& aQ) const
   {
      return (std::cos(Nb(aQ))*std::exp(Nb(Nb(aQ)+Nb(5.1)))/Nb(Nb(aQ)+Nb(13.6)*(Nb(aQ)+Nb(1.1))) - mZero);
   }
};

struct ShuntingYardTest : public cutee::test
{
   void run()
   {
      // Setup
      FunctionContainer<Nb> func_cont;
      std::vector<std::string> var(I_1,"Q");
      In arit_count;
      ShuntingYard<Nb> TestFactory(var);
      
      // run tests
      auto polish1 = TestFactory.GenerateExpression("Q-5",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, 5, -, ", "Polish 1 failed.");
      
      auto polish2 = TestFactory.GenerateExpression("(Q-5)*4/(Q*5+4)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, 5, -, 4, *, Q, 5, *, 4, +, /, ", "Polish 2 failed.");
      
      auto polish3 = TestFactory.GenerateExpression("-(Q-5*(Q+6/(Q*7)))",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, 5, Q, 6, Q, 7, *, /, +, *, -, U-, ", "Polish 3 failed.");
      
      auto polish4 = TestFactory.GenerateExpression("COS(Q-5)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, 5, -, COS, ", "Polish 4 failed.");
      
      auto polish5 = TestFactory.GenerateExpression("EXP(COS(Q-5))/SIN(Q*7)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, 5, -, COS, EXP, Q, 7, *, SIN, /, ", "Polish 5 failed.");
   }
};

struct FunctionContainerShuntingYardTest : public cutee::test
{
   void run()
   {
      // Initialize
      FunctionContainer<Nb> func_cont;
      func_cont.InsertFunction("F1(x,y,z)", "x^2+y*z");
      func_cont.InsertFunction("F2(x,y)", "x-2*y");
      func_cont.InsertFunction("F3(x)", "x^3");
      func_cont.InsertConstant("alpha", 1.5e-5);
      func_cont.InsertConstant("beta", 2.5e1);
      func_cont.InsertConstant("gamma", 2.5e1);
      std::vector<std::string> var(I_1,"Q");
      
      In arit_count;
      ShuntingYard<Nb> TestFactory(var);
      
      // Do testing
      auto polish1 = TestFactory.GenerateExpression("F3(Q)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, F3, ", "Polish 1 failed.");
      
      auto polish2 = TestFactory.GenerateExpression("F2(Q,alpha)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, alpha, F2, ", "Polish 2 failed.");
      
      auto polish3 = TestFactory.GenerateExpression("F2(Q,Q^2)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, Q, 2, ^, F2, ", "Polish 3 failed.");
      
      auto polish4 = TestFactory.GenerateExpression("F1(F2(Q,Q^2),Q,F3(Q))",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "Q, Q, 2, ^, F2, Q, Q, F3, F1, ", "Polish 4 failed.");
      
      auto polish5 = TestFactory.GenerateExpression("F1(F2(F3(alpha),F3(beta)*F3(gamma)),F3(Q),Q^1-5/Q+3)",func_cont,arit_count);
      UNIT_ASSERT_EQUAL(TestFactory.GetRevPolish(), "alpha, F3, beta, F3, gamma, F3, *, F2, Q, F3, Q, 1, ^, 5, Q, /, -, 3, +, F1, ", "Polish 5 failed.");
   }
};

} /* namespace midas::test */

#endif //GENERICFUNCTIONTESTS_H
