/**
 *******************************************************************************
 * 
 * @file    MpiSyncedAssertions.h
 * @date    07-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Some semi-robust utilities for calling (some of) cutee's UNIT_ASSERT[*]s
 *    in MPI environment.
 *
 * Use when results are expected to differ across different ranks; especially,
 * they're meant to ensure that all ranks break the current test if any of them
 * get a failed assertion, so that not only some of them continue, which can
 * cause MPI errors due to the ranks being out of sync.
 *
 * @note
 *    Certainly, there's room for improvements and additions, but for now (Feb
 *    2020), it's safer than just using plain cutee calls. Ideally, it should
 *    be handled within the cutee library itself.
 *    Also, only works for plain-old-data types (for which there is an
 *    MPI_Datatype).
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MPISYNCEDASSERTIONS_H_INCLUDED
#define MPISYNCEDASSERTIONS_H_INCLUDED

#include <cutee/cutee.hpp>
#include "util/Error.h"
#include "util/type_traits/Complex.h"
#include "mpi/Impi.h"

namespace midas::test::mpi_sync
{
   namespace detail
   {
      template
         <  class T
         ,  class U
         ,  class F_COMPARE
         ,  class F_CUTEE_ASSERTER
         >
      void AssertImpl
         (  const T& t
         ,  const U& u
         ,  cutee::info&& i
         ,  F_COMPARE&& compare
         ,  F_CUTEE_ASSERTER&& cutee_asserter
         )
      {
         if constexpr(MPI_DEBUG)
         {
            std::stringstream ss_mpi_dbg;
            ss_mpi_dbg << std::scientific << std::setprecision(16);
            ss_mpi_dbg << __FILE__ << ":" << __LINE__ << "; AssertImpl begin, "
               << " t = " << t
               << " u = " << u
               ;
            midas::mpi::WriteToLog(ss_mpi_dbg.str());
         }
#ifdef VAR_MPI
         const auto t_type = midas::mpi::DataTypeTrait<std::decay_t<T>>::Get();
         const auto u_type = midas::mpi::DataTypeTrait<std::decay_t<U>>::Get();
         const auto m_rank = midas::mpi::MasterRank();
         const auto mpi_comm = midas::mpi::CommunicatorWorld().GetMpiComm();

         // For modifying info message.
         const std::string s_tab(16, ' ');
         std::stringstream ss;
         std::stringstream ss_got;
         const Uin w_rank = std::to_string(midas::mpi::GlobalSize()).size();
         ss_got << std::right;
         if constexpr(  (  midas::type_traits::IsComplexV<std::decay_t<T>>
                        && std::is_floating_point_v<midas::type_traits::RealTypeT<std::decay_t<T>>>
                        )
                     || std::is_floating_point_v<std::decay_t<T>>
                     )
         {
            ss_got << std::scientific << std::setprecision(16);
         }
         ss << '\n' << s_tab << "Failed on ";

         // Number of fails.
         Uin n_fails = 0;
         const auto n_fails_type = midas::mpi::DataTypeTrait<decltype(n_fails)>::Get();

         // Results and number of fails.
         // tuple: {T val, U val, rank}
         std::vector<std::tuple<std::decay_t<T>,std::decay_t<U>,Uin>> v_results;

         if (!midas::mpi::IsMaster())
         {
            // Slaves send their t and u to master.
            if (int ret = midas::mpi::detail::WRAP_Send(&t, 1, t_type, m_rank, 0, mpi_comm) != 0)
            {
               MIDASERROR("Non-zero return status = "+std::to_string(ret));
            }
            if (int ret = midas::mpi::detail::WRAP_Send(&u, 1, u_type, m_rank, 0, mpi_comm) != 0)
            {
               MIDASERROR("Non-zero return status = "+std::to_string(ret));
            }
         }
         else
         {
            ss << "ranks ";

            // Master receives from slaves, checks and stores all results.
            std::decay_t<T> t_rank;
            std::decay_t<U> u_rank;
            for(Uin r = 0; r < midas::mpi::GlobalSize(); ++r)
            {
               if (r == midas::mpi::MasterRank())
               {
                  t_rank = t;
                  u_rank = u;
               }
               else
               {
                  MPI_Status status;
                  if (int ret = midas::mpi::detail::WRAP_Recv(&t_rank, 1, t_type, r, 0, mpi_comm, &status) != 0)
                  {
                     MIDASERROR("Non-zero return status = "+std::to_string(ret));
                  }
                  if (int ret = midas::mpi::detail::WRAP_Recv(&u_rank, 1, u_type, r, 0, mpi_comm, &status) != 0)
                  {
                     MIDASERROR("Non-zero return status = "+std::to_string(ret));
                  }
               }
               if (!(compare(t_rank,u_rank)))
               {
                  ++n_fails;
                  ss << r << ", ";
                  ss_got << s_tab << "rank " << std::setw(w_rank) << r << " got: " << t_rank << '\n';
               }
               v_results.emplace_back(t_rank,u_rank,r);
            }
            ss.seekp(-2, std::ios_base::end) << "; ";
         }

         // Then broadcasts how many tests failed.
         if (int ret = midas::mpi::detail::WRAP_Bcast(&n_fails, 1, n_fails_type, m_rank, mpi_comm) != 0)
         {
            MIDASERROR("Non-zero return status = "+std::to_string(ret));
         }
         ss << n_fails << " rank(s) in total."
            << " This is rank " << midas::mpi::GlobalRank()
            ;
         if (midas::mpi::IsMaster())
         {
            ss << " (master).";
         }
         else
         {
            ss << ". Check output from master (rank " << midas::mpi::MasterRank() << ").";
         }
         ss << '\n';
         i._message += ss.str();
         i._message += ss_got.str();

         if (n_fails > 0)
         {
            if constexpr(MPI_DEBUG)
            {
               std::stringstream ss_mpi_dbg;
               ss_mpi_dbg << std::scientific << std::setprecision(16);
               ss_mpi_dbg << __FILE__ << ":" << __LINE__ << "; AssertImpl, n_fails = " << n_fails;
               midas::mpi::WriteToLog(ss_mpi_dbg.str());
            }
            // Loop through v_results and do UNIT_ASSERT*s; in case of failure we'll
            // get a decent-looking error message from this.
            // Only master can have a non-empty v_results, so slaves just skip
            // through this. Master may stop here, if assertion fails.
            for(const auto& [t_test, u_test, rank]: v_results)
            {
               cutee_asserter(t_test, u_test, cutee::info(i));
            }

            // Then we assert on the number of failed tests, which will make sure
            // slaves also fail and stop.
            cutee::asserter::assert_equal(n_fails, 0, cutee::info(i));

            // And then an extra precautious safe-guard to make sure we stop if
            // n_fails > 0, if above cutee assertions doesn't throw as intended.
            MIDASERROR("n_fails > 0 (but cutee should have thrown before getting to here).");
         }
#else /* !VAR_MPI */
         cutee_asserter(t, u, std::move(i));
#endif /* VAR_MPI */
         if constexpr(MPI_DEBUG)
         {
            std::stringstream ss_mpi_dbg;
            ss_mpi_dbg << std::scientific << std::setprecision(16);
            ss_mpi_dbg << __FILE__ << ":" << __LINE__ << "; AssertImpl end";
            midas::mpi::WriteToLog(ss_mpi_dbg.str());
         }
      }

   } /* namespace detail */

   template<class T, class U>
   void AssertEqual(const T& t, const U& u, cutee::info&& i)
   {
      detail::AssertImpl
         (  t
         ,  u
         ,  std::move(i)
         ,  [](const T& tval, const U& uval) -> bool 
            {
               return tval == uval;
            }
         ,  [](const T& tval, const U& uval, cutee::info&& inf) -> void
            {
               cutee::asserter::assert_equal(tval, uval, std::move(inf));
            }
         );
   }

   template<class T>
   void AssertTrue(const T& t, cutee::info&& i)
   {
      AssertEqual(int(bool(t)), int(true), std::move(i));
   }

   template<class T, class U, class I>
   void AssertFEqualPrec(const T& t, const U& u, I&& ulps, cutee::info&& i)
   {
      detail::AssertImpl
         (  t
         ,  u
         ,  std::move(i)
         ,  [ulps](const T& tval, const U& uval) -> bool 
            {
               return cutee::numeric::float_eq(tval, uval, ulps);
            }
         ,  [ulps](const T& tval, const U& uval, cutee::info&& inf) -> void
            {
               cutee::asserter::assert_float_equal_prec(tval, uval, ulps, std::move(inf));
            }
         );
   }

   template<class T, class U, class I>
   void AssertFZeroPrec(const T& t, const U& u, I&& ulps, cutee::info&& i)
   {
      detail::AssertImpl
         (  t
         ,  u
         ,  std::move(i)
         ,  [ulps](const T& tval, const U& uval) -> bool 
            {
               return cutee::numeric::float_numeq_zero(tval, uval, ulps);
            }
         ,  [ulps](const T& tval, const U& uval, cutee::info&& inf) -> void
            {
               cutee::asserter::assert_float_numeq_zero_prec(tval, uval, ulps, std::move(inf));
            }
         );
   }
} /* namespace midas::test::mpi_sync */

#define MPISYNC_UNIT_ASSERT(a, b) \
   midas::test::mpi_sync::AssertTrue(a, cutee::info{b, __FILE__, __LINE__});

#define MPISYNC_UNIT_ASSERT_EQUAL(a, b, c) \
   midas::test::mpi_sync::AssertEqual(a, b, cutee::info{c, __FILE__, __LINE__});

#define MPISYNC_UNIT_ASSERT_FEQUAL_PREC(a,b,c,d) \
   midas::test::mpi_sync::AssertFEqualPrec(a, b, c, cutee::info{d, __FILE__, __LINE__});

#define MPISYNC_UNIT_ASSERT_FZERO_PREC(a,b,c,d) \
   midas::test::mpi_sync::AssertFZeroPrec(a, b, c, cutee::info{d, __FILE__, __LINE__});

#endif/*MPISYNCEDASSERTIONS_H_INCLUDED*/
