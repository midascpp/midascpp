/**
 *******************************************************************************
 * 
 * @file    OpDefsForTesting.h
 * @date    10-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef OPDEFSFORTESTING_H_INCLUDED
#define OPDEFSFORTESTING_H_INCLUDED

// Standard headers.
#include <map>
#include <string>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"

// Forward declarations.
class GlobalOperatorDefinitions;
class OpDef;
class BasDef;
class OneModeInt;
class VccCalcDef;
class Vscf;
class Vcc;

namespace midas::test::util
{
   //! Setup global mode labels.
   void SetupGlobalModeLabels
      (  GlobalOperatorDefinitions& arGlobOpDefs
      ,  const std::vector<std::string>& arModeLabels
      ,  const bool aResetBefore = true
      );

   //! Setup OpDef from an input stream.
   OpDef SetupOpDef
      (  std::istream& arOperInput
      ,  const std::vector<std::string>& arModeLabels
      ,  const std::string& arName = "unit_test_operator"
      ,  const bool aAddSimpleKE = false
      ,  const bool aResetBefore = true
      ,  const bool aResetAfter = true
      );

   enum class OperType
      {  H        // Hamiltonian
      ,  DipX     // Dipole components
      ,  DipY
      ,  DipZ
      };

   //@{
   //! Up-to-3-mode H, dipole {X,Y,Z} for H2O, from ir_h2o_vci test.
   OpDef WaterH0_ir_h2o_vci(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipX_ir_h2o_vci(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipY_ir_h2o_vci(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipZ_ir_h2o_vci(Uin, bool aResetBefore = true, bool aResetAfter = true);
   std::map<OperType,OpDef> WaterOpers_ir_h2o_vci(Uin, bool aResetBefore = true, bool aResetAfter = true);
   //@}

   //@{
   //! Up-to-2-mode H, dipole {X,Y,Z} for H2O, from pes_MultilevelAdga_sym test.
   OpDef WaterH0_pes_MultilevelAdga_sym(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipX_pes_MultilevelAdga_sym(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipY_pes_MultilevelAdga_sym(Uin, bool aResetBefore = true, bool aResetAfter = true);
   OpDef WaterDipZ_pes_MultilevelAdga_sym(Uin, bool aResetBefore = true, bool aResetAfter = true);
   std::map<OperType,OpDef> WaterOpers_pes_MultilevelAdga_sym(Uin, bool aResetBefore = true, bool aResetAfter = true);
   //@}


   //@{
   //! BasDef%s with reasonable defaults, based on given OpDef.
   BasDef HoBasis(const OpDef&, Uin aHoQuantNum = 20, bool aUseScalingFreqs = true);
   BasDef BsplineBasis(const OpDef&, Uin aOrders = 10, Nb aDens = 0.8, Uin aTurningPoint = 10, bool aUseScalingFreqs = true);
   BasDef GaussBasis(const OpDef&, Nb aExponent = 1.0, Nb aDens = 0.8, Uin aTurningPoint = 10, bool aUseScalingFreqs = true);
   //@}


   //! Calculates and returns OneModeInt%s based on OpDef/BasDef.
   OneModeInt SetupOneModeInts(const OpDef&, const BasDef&);

   //! Returns a rather minimalistic but working VccCalcDef for a ground state calc.
   VccCalcDef SetupVccCalcDef
      (  const std::vector<In>& arNModals
      ,  Uin aMaxExci
      ,  bool aUseNlTensorSolver = true
      );
   //! Returns a rather minimalistic but working VccCalcDef for a ground state calc.
   VccCalcDef SetupVccCalcDef
      (  const std::vector<In>& arNModals
      ,  const std::vector<Uin>& arNActiveModals
      ,  Uin aMaxExci
      ,  bool aUseNlTensorSolver = true
      );

   //! Perform a rather standard VSCF ground state calc. and finalize for VCC if converged.
   void RunVscf(Vscf& arVscf, bool aMuteMout = true);

   //! Perform a rather standard VCC ground state calc. VSCF _must_ be converged already.
   void RunVcc(Vcc& arVcc, bool aMuteMout = true);

} /* namespace midas::test::util */

#endif/*OPDEFSFORTESTING_H_INCLUDED*/
