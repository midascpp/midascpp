/**
 *******************************************************************************
 * 
 * @file    TransTestUtils_Decl.h
 * @date    18-03-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRANSTESTUTILS_DECL_H_INCLUDED
#define TRANSTESTUTILS_DECL_H_INCLUDED

#include <set>
#include <utility>
#include <vector>
#include "inc_gen/TypeDefs.h"

template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;
template<typename> class GeneralDataCont;
template<typename> class GeneralTensorDataCont;
class ModeCombiOpRange;
class OpDef;

namespace midas::test::util
{
   //! ModeCombiOpRange with num. modes, max. exci. and some random ModeCombi%s erased.
   std::pair<ModeCombiOpRange,Uin> GetMcrRandomMcsErased
      (  const std::vector<Uin>& arNumModals
      ,  Uin aMaxExci
      ,  Uin aNumErase = 0
      ,  Uin aMinLevelErase = 1
      );

   //! Set ModeCombi addresses of ModeCombiOpRange corresponding to number of modals per mode.
   Uin SetAddresses
      (  ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumModals
      );

   //! Set up random exci.params. corresponding to ModeCombiOpRange and num. modals.
   template<typename T>
   std::vector<std::vector<T>> GetRandomParams
      (  const ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumModals
      ,  const bool aZeroRefParam = true
      ,  const bool aZeroSingles = false
      ,  const In aAddend = -1
      );

   //! Random OpDef... based on some ModeCombiOpRange. Using random (real) coefs.
   template<typename T>
   OpDef GetOpDefFromCoefs
      (  const ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumOpers
      ,  const std::vector<std::vector<T>> arCoefs
      );

   //! Random ModalIntegrals corresponding to OpDef. aSymType = 'h', 'a' or 'g'
   template<typename T>
   ModalIntegrals<T> GetRandomModalIntegrals
      (  const OpDef& arOpDef
      ,  const std::vector<Uin>& arNumModals
      ,  char aSymType = 'h'
      );

   //! 1-mode modal transformation matrices for each mode; pair = {U, W}. See documentation!
   template<typename T>
   std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> GetRandomModalTransMats
      (  const std::vector<Uin>& arNumModalsExt
      ,  const std::vector<Uin>& arNumModalsRed
      ,  bool aBalanceColsRows = true
      ,  bool aIncludeZeroColsRows = true
      );

   //@{
   //! Construct amplitude container.
   template<typename T>
   void ConstructParamsVecCont
      (  GeneralMidasVector<T>& arVecCont
      ,  const ModeCombiOpRange& arMcr
      ,  const Uin aTotSize
      ,  const std::vector<std::vector<T>>& arParams
      ,  const std::vector<Uin>& arNModals = {}    // Not needed
      );
   template<typename T>
   void ConstructParamsVecCont
      (  GeneralDataCont<T>& arVecCont
      ,  const ModeCombiOpRange& arMcr
      ,  const Uin aTotSize
      ,  const std::vector<std::vector<T>>& arParams
      ,  const std::vector<Uin>& arNModals = {}    // Not needed
      );
   template<typename T>
   void ConstructParamsVecCont
      (  GeneralTensorDataCont<T>& arVecCont
      ,  const ModeCombiOpRange& arMcr
      ,  const Uin aTotSize
      ,  const std::vector<std::vector<T>>& arParams
      ,  const std::vector<Uin>& arNModals
      );
   //@}
   
} /* namespace midas::test::util */

#endif/*TRANSTESTUTILS_DECL_H_INCLUDED*/
