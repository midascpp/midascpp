/**
 *******************************************************************************
 * 
 * @file    UnitTestTemplate.h //CHANGE!
 * @date    19-09-2018 //CHANGE!
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk) //CHANGE!
 *
 * @brief
 *    A template file for unit tests. Copy and modify this for your own test. //CHANGE!
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef UNITTESTTEMPLATE_H_INCLUDED //CHANGE! header guard
#define UNITTESTTEMPLATE_H_INCLUDED //CHANGE! header guard

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/CuteeInterface.h"

namespace midas::test::your_test_namespace //CHANGE! your_test_namespace
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Tests should be simple, so brief descriptions should suffice.
    *
    * Otherwise put some more details here. //CHANGE! comments
    ***************************************************************************/
   struct TestOne //CHANGE! struct names, here and below
      :  public cutee::test
   {
      void run() override
      {
         // Use these assertions for the tests. All of them will automatically
         // print expected and actual values.

         // Boolean assertion.
         UNIT_ASSERT(true, "Error message.");

         // Assertion for strict equality.
         UNIT_ASSERT_EQUAL(I_1, I_1, "Strict integer equality failed.");

         // Assertions for floating-point equality. Default tolerance is 2;
         // unit is ULPs (Unit in the Last Place or Unit of Least Precision).
         Nb nearly_one  = C_1 + 1*C_NB_EPSILON;
         Nb roughly_one = C_1 + 8*C_NB_EPSILON;
         UNIT_ASSERT_FEQUAL(nearly_one, C_1, "Numerical equality failed, default ULPs.");
         UNIT_ASSERT_FEQUAL_PREC(nearly_one,  C_1,  2, "Numerical equality failed, 2 ULPs.");
         UNIT_ASSERT_FEQUAL_PREC(roughly_one, C_1, 10, "Numerical equality failed, 10 ULPs.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Templated test for testing templated class/function. Implementation
    *    must be in a header file; this one or an _Impl.h.
    ***************************************************************************/
   template<class T>
   struct TestWithTemplateParameter
      :  public cutee::test
   {
      void run() override
      {
         // Test and assertions with template parameters.
         T one = static_cast<T>(1);
         T nearly_one = static_cast<T>(1) + std::numeric_limits<T>::epsilon();
         UNIT_ASSERT_FEQUAL(nearly_one, one, "Numerical equality failed.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Base class for setup/teardown that is common for several tests.
    *
    * If several tests have some setup/teardown in common, e.g. writing and
    * deleting a specific file, put it in an intermediate base struct and
    * inherit from that. Otherwise delete this if unnecessary.
    ***************************************************************************/
   struct BaseStructForCommonSetupAndTeardown
      :  public cutee::test
   {
      void setup() override
      {
         // E.g. writing a specific file before each test.
      }
      void teardown() override
      {
         // E.g. removing the file again after each test.
      }
   };

   /************************************************************************//**
    * @brief
    *    Inherits setup()/teardown() which are called before/after run().
    ***************************************************************************/
   struct TestWithSetupAndTeardown
      :  public BaseStructForCommonSetupAndTeardown
   {
      void run() override
      {
         // Do test that requires certain things to have been set up, e.g. a
         // specific file to be on disk.
         bool successfully_modified_file = true;
         UNIT_ASSERT(successfully_modified_file, "File modification failed.");
      }
   };

} /* namespace midas::test::your_test_namespace */ //CHANGE! your_test_namespace

#endif/*UNITTESTTEMPLATE_H_INCLUDED*/ //CHANGE! header guard
