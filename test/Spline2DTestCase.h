/**
************************************************************************
* 
* @file                Spline2DTest.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli
*
* Short Description:   Test Spline2D class 
* 
* Last modified: Thu Dec 03, 2009  02:01PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SPLINE2DTESTCASE_H
#define SPLINE2DTESTCASE_H

// Standard Headers.
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// Indirect link to Standard Headers.
//#include "math_link.h"

// My Headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/Test.h"
#include "test/CuteeInterface.h"
#include "input/Input.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
//#include "pes/SplineND.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"

namespace midas::test
{

/**
* Controls the testing of stuff 
* */
struct Spline2DTestCase : public cutee::test{
   void fxy (const MidasVector&  arXog1, const MidasVector& arXog2, MidasVector& arYog) 
   {
      In nog1 = arXog1.Size();
      In nog2 = arXog2.Size();
      MidasVector xog1_tmp = arXog1;
      MidasVector xog2_tmp = arXog2;
      xog1_tmp.Pow(I_3);
      xog2_tmp.Pow(I_3);
      In nog12 = nog1 * nog2;
      if ( nog12 != arYog.Size())
      {
        arYog.SetNewSize(nog12,false);
      }
      In iog12 = I_0;
      for (In iog1 =I_0; iog1 < nog1; iog1++)
      {
         for (In iog2 =I_0; iog2 < nog2; iog2++)
         { 
            arYog[iog12] = xog1_tmp[iog1] + xog2_tmp[iog2];
            iog12+=I_1;
         }
      }
   }
   
   /**
   * performing test for class Spline2D(ND) 
   **/
   // *** uncomment //! for testing the SplineND class
   void run() 
   {
      //Mout << "****** Performing test of Spline2D class " << endl << endl;
   
   // *** then defines the function to be interpolated (a normal cubic in two dimensions)
   
      Nb* Xig1_tmp;
      Nb* Xig2_tmp;
      Nb low_boundX1_ig = -C_1;
      Nb upper_boundX1_ig = C_1;
      Nb low_boundX2_ig = -C_1;
      Nb upper_boundX2_ig = C_1;
      In num_of_stepX1_ig = I_5;
      In num_of_stepX2_ig = I_5;
      Nb hX1_ig;
      Nb hX2_ig;
   
   // *** input lower and upper bounds and the # of steps
    
      //Mout << "Lower bound in X1 direction for spline2D interpolation : " << low_boundX1_ig  << endl << endl;
      //Mout << "Upper bound in X1 direction for spline2D interpolation: " << upper_boundX1_ig << endl << endl;
      //Mout << "# of steps  in X1 direction for spline2D interpolation: " << num_of_stepX1_ig << endl << endl;
      //Mout << "Lower bound in X2 direction for spline2D interpolation: " << low_boundX2_ig   << endl << endl;
      //Mout << "Upper bound in X2 direction for spline2D interpolation: " << upper_boundX2_ig << endl << endl;
      //Mout << "# of steps  in X2 direction for spline2D interpolation: " << num_of_stepX2_ig << endl << endl;
               
   
      hX1_ig = (upper_boundX1_ig-low_boundX1_ig)/num_of_stepX1_ig;
      hX2_ig = (upper_boundX2_ig-low_boundX2_ig)/num_of_stepX2_ig;
   
      In nig1 = num_of_stepX1_ig+I_1;
      In nig2 = num_of_stepX2_ig+I_1;
   
      Xig1_tmp = new Nb [nig1];
      Xig2_tmp = new Nb [nig2];
   
      for (In i = I_0; i < nig1; i++)
      { 
         Xig1_tmp[i] = low_boundX1_ig + i*hX1_ig;
      }
      for (In i = I_0; i < nig2; i++)
      {
         Xig2_tmp[i] = low_boundX2_ig + i*hX2_ig;
      }
   
   
      MidasVector Xig1(nig1, Xig1_tmp);
      MidasVector Xig2(nig2, Xig2_tmp);
      delete[] Xig1_tmp;
      delete[] Xig2_tmp;
      vector<MidasVector> Xig(I_2);
      vector<MidasVector> Xog(I_2);
      Xig[I_0].SetNewSize(nig1, false);
      Xig[I_1].SetNewSize(nig2, false);
      Xig[I_0]=Xig1;
      Xig[I_1]=Xig2;
      MidasVector YigDerivs(nig2*nig1);
      
      In nig12 = nig1 * nig2;
      MidasVector Yig(nig12, C_0);
   
      fxy(Xig1, Xig2, Yig);
   
   // *** then construct the 2D cubic natural spline interpolant
   
      Spline2D CubicApprox(SPLINEINTTYPE::GENERAL, Xig1, Xig2, Yig, C_3, C_3);
   
      CubicApprox.GetYigDerivs(YigDerivs);
   
   // and evaluate at few selected points
   
      Nb* x1_int;
      Nb* x2_int;
      //Nb* f_int;
      Nb low_boundX1_og = -C_1;
      Nb upper_boundX1_og = C_1;
      Nb low_boundX2_og = -C_1;
      Nb upper_boundX2_og = C_1;
      In num_of_stepX1_og = I_100;
      In num_of_stepX2_og = I_100;
      Nb hX1_og;
      Nb hX2_og;
   
      //Mout << "Lower bound in X1 direction for finer grid in spline2D interpolation : " << low_boundX1_og  << endl << endl;
      //Mout << "Upper bound in X1 direction for finer grid in spline2D interpolation: " << upper_boundX1_og << endl << endl;
      //Mout << "# of steps  in X1 direction for finer grid in spline2D interpolation: " << num_of_stepX1_og << endl << endl;
      //Mout << "Lower bound in X2 direction for finer grid in spline2D interpolation: " << low_boundX2_og   << endl << endl;
      //Mout << "Upper bound in X2 direction for finer grid in spline2D interpolation: " << upper_boundX2_og << endl << endl;
      //Mout << "# of steps  in X2 direction for finer grid in spline2D interpolation: " << num_of_stepX2_og << endl << endl;
   
      hX1_og = (upper_boundX1_og-low_boundX1_og)/num_of_stepX1_og;
      hX2_og = (upper_boundX2_og-low_boundX2_og)/num_of_stepX2_og;
   
      In nog1 = num_of_stepX1_og + I_1;
      In nog2 = num_of_stepX2_og + I_1;
      In nog12 = nog1 * nog2;
      x1_int = new Nb [nog1];
      x2_int = new Nb [nog2];
   
      for (In i = I_0; i < nog1; i++)
      {
         x1_int[i] = low_boundX1_og + i*hX1_og;
      }
      for (In i = I_0; i < nog2; i++)
      {     
         x2_int[i] = low_boundX2_og + i*hX2_og;
      }
   
      MidasVector Xog1(nog1, x1_int);
      MidasVector Xog2(nog2, x2_int);
      delete[] x1_int;
      delete[] x2_int;
      Xog[I_0].SetNewSize(nog1, false);
      Xog[I_1].SetNewSize(nog2, false);
      Xog[I_0]=Xog1;
      Xog[I_1]=Xog2;
   
      MidasVector Yog(nog12, C_0);
      MidasVector yog_analysis(nog12, C_0);
      GetYog(Yog, Xog1, Xog2, Xig1, Xig2, Yig, YigDerivs, SPLINEINTTYPE::GENERAL, C_3, C_3);
      fxy(Xog1, Xog2, yog_analysis);
      
   
     // Mout << "**** results from Spline2D interpolation **** " << endl << endl;
   
   // *** Then find out some statistics
      Nb Rms;
      RmsDevPerElement(Rms, Yog, yog_analysis, nog12);
      
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(Rms,1.0),"RMS not close to zero");
      //Mout << "   Xog1     " <<  "            "   <<  "   Xog2      " << "               "  <<  "   Yog      " << "           "   <<  "  Analitic result  " <<  endl << endl;
      In iog12=I_0;
      for (In k = I_0; k<nog1; k++)
      {
         for (In j = I_0; j<nog2; j++)
         {
            //Mout << Xog1[k] << "        "  << Xog2[j] << "        "  <<  Yog[iog12]  << "       "   << yog_analysis[iog12] << endl;
            UNIT_ASSERT(CheckTest(Yog[iog12],yog_analysis[iog12]),"2D spline test failed for "+std::to_string(iog12));
            iog12+=I_1;
         }
      }
      //Mout << endl << endl;
   
      //Mout << "********** end of Spline2DTest *****************" << endl;
   }

   bool CheckTest(Nb aSpline, Nb aAnalytic){
      return (aSpline-aAnalytic < 10*C_NB_EPSILON);
   }
};

} /* namespace midas::test */

#endif //SPLINE2DTESTCASE_H
