/**
 *******************************************************************************
 * 
 * @file    GlobalDataTest.cc
 * @date    09-10-2017
 * @author  Ian Heide Godtliebsen (ian@chem.au.dk)
 *
 * @brief
 *    Unit tests for global data classes.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/GlobalDataTest.h"

#include "inc_gen/TypeDefs.h"
#include "input/GlobalData.h"

namespace midas{
namespace test{
namespace globaldata{

using namespace midas::global;

/***************************************************************************//**
 * @brief
 *    Test that we can construct an empty service locator.
 ******************************************************************************/
void TestServiceLocatorConstructor::run()
{
   // Construct service locator.
   ServiceLocator service_locator;
   
   // Assert that we cannot find what-ever service
   UNIT_ASSERT(!service_locator.HasService<Nb>(), "Has service Nb.");
   UNIT_ASSERT(!service_locator.HasService<In>(), "Has service In.");
}

/***************************************************************************//**
 * @brief
 *    Test that we can add services to the service locator.
 ******************************************************************************/
void TestServiceLocatorAddService::run()
{
   // Construct service locator.
   ServiceLocator service_locator;

   // Add some services
   UNIT_ASSERT(service_locator.AddService(Nb{}), "Service not added.");
   UNIT_ASSERT(service_locator.AddService(In{}), "Service not added.");
   
   // Assert that we cannot find what-ever service
   UNIT_ASSERT(service_locator.HasService<Nb>(), "Doesn't have service Nb.");
   UNIT_ASSERT(service_locator.HasService<In>(), "Doesn't have service In.");

   // Clear service locator so we do not pollute the other tests.
   service_locator.Clear();
}

/***************************************************************************//**
 * @brief
 *    Test that we can get services from the service locator.
 ******************************************************************************/
void TestServiceLocatorGetService::run()
{
   Nb magic_number = 0xDEADBEEF;

   // Construct service locator.
   ServiceLocator service_locator;
   ServiceLocator::service_container_type service_container;

   // Add some services (assert that the service is actually added)
   UNIT_ASSERT(service_locator.AddService(Nb{magic_number}, service_container), "Service not added");
   
   // Assert that we cannot find the service
   UNIT_ASSERT(service_locator.HasService<Nb>(service_container), "Doesn't have service Nb.");

   // Assert that the service is correct.
   UNIT_ASSERT_EQUAL(service_locator.GetService<const Nb>(service_container), magic_number, "Service not correct.");

   // Change the Nb service
   service_locator.GetService<Nb>(service_container) = 1.0;
   
   // Assert that the service changed.
   UNIT_ASSERT_EQUAL(service_locator.GetService<const Nb>(service_container), 1.0, "Service not correct after change.");
   
   // Clear service container
   service_locator.Clear(service_container);

   // Assert service container is cleared.
   UNIT_ASSERT_EQUAL(service_container.size(), std::size_t{0}, "Service container size is not zero after clear.");
}

/***************************************************************************//**
 * @brief
 *    Test that we can save a global service and retrieve somewhere else.
 ******************************************************************************/
void TestServiceLocatorGlobalService::setup()
{
   // Add a service
   ServiceLocator service_locator;
   service_locator.AddService<In>(In{1337});
}

void TestServiceLocatorGlobalService::run()
{
   // Add a service
   ServiceLocator service_locator;
      
   // Do some assertions
   UNIT_ASSERT      (!service_locator.AddService<In>(In{1338}), "Service was added even though is should be present :C.");
   UNIT_ASSERT_EQUAL(service_locator.GetService<In>(), In{1337}   , "Service was not correct.");

}

void TestServiceLocatorGlobalService::teardown()
{
   // Clear the global service locator.
   ServiceLocator service_locator;
   service_locator.Clear();
}

} /* namespace globaldata */

/**
 * Function for running all tests on global data.
 **/
void GlobalDataTest
   (
   )
{
   // Create test suite
   cutee::suite suite("GlobalData test");
   
   
   // Add tests
   using namespace midas::test::globaldata;
   
   suite.add_test<TestServiceLocatorConstructor>("Service locator constructor.");
   suite.add_test<TestServiceLocatorAddService> ("Service locator add service.");
   suite.add_test<TestServiceLocatorGetService> ("Service locator get service.");
   suite.add_test<TestServiceLocatorGlobalService> ("Service locator global service.");
   
   // run tests
   RunSuite(suite);
}

} /* namespace test */
} /* namespace midas */
