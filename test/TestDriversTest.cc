/**
 *******************************************************************************
 * 
 * @file    TestDriversTest.cc
 * @date    28-09-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Tests the TestDrivers class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/TestDriversTest.h"

#include "util/MidasStream.h"
extern MidasStream Mout;

namespace midas::test
{
   namespace testdrivers::detail
   {
   } /* namespace testdrivers::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void TestDriversTest()
   {
      // Create test_suite object.
      cutee::suite suite("TestDrivers test");

      // Add all the tests to the suite.
      using namespace testdrivers;
      suite.add_test<AllDriversByDefault>("Default constructor; all drivers");
      suite.add_test<AppendDriver>("AppendDriver");
      suite.add_test<Summary>("Summary");
      suite.add_test<Help>("Help");

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
