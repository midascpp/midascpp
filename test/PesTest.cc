#include "PesTest.h"

namespace midas
{
namespace test
{

void PesTest()
{
   // Create test suite.
   cutee::suite suite("Pes module test");
   
   // Add tests.
   suite.add_test<pes_test::PropertyInfoTest>("PropertyInfo test");
   
   // Run tests.
   RunSuite(suite);
}

} /* namespace test */
} /* namespace midas */
