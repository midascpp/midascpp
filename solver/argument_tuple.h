#ifndef ARGUMENT_TUPLE_H_INCLUDED
#define ARGUMENT_TUPLE_H_INCLUDED

#include<tuple>
#include"../libmda/meta/index_list.h"

//////////
// Argument tuple class to hold a tuple of function arguments
//
//
//////////
template<class... Ts>
using argument_tuple = std::tuple<Ts...>;

template<class... Ts>
inline argument_tuple<Ts...> make_argument_tuple(Ts&&... ts)
{
   return argument_tuple<Ts...>(std::forward<Ts>(ts)...);
}

namespace detail
{

/////
// implementation of pass argument tuple
// passes an argument tuple to a class
/////
struct pass_argument_tuple_impl
{
   template<class F, class M, class T, unsigned... Is>
   inline auto operator()(F&& f, M&& m, T&& t, libmda::meta::index_list<Is...>) const
      -> decltype(std::declval<M>()(std::forward<F>(f)
                                  , std::get<Is>(t)...
                                  ) )
   {
      return m(std::forward<F>(f),std::get<Is>(t)...);
   }
   
};

} // namespace detail

///////
// interface function for passing an argument tuple and a function (f) to another function (m)
// 
///////
template<class F, class M, class T>
inline auto pass_argument_tuple(F&& f, M&& m, T&& t)
   ->  decltype(::detail::pass_argument_tuple_impl()(std::forward<F>(f)
                                                   , std::forward<M>(m)
                                                   , std::forward<T>(t)
                                                   , libmda::meta::index_range<0, std::tuple_size<typename std::decay<T>::type>::value>()
                                                   ) )
{
   return ::detail::pass_argument_tuple_impl()(std::forward<F>(f)
                                             , std::forward<M>(m)
                                             , std::forward<T>(t)
                                             , libmda::meta::index_range<0, std::tuple_size<typename std::decay<T>::type>::value>()
                                             );
}

//////
// invoker structs to invoke methods
//
/////
struct invoke_evaluate
{
   template<class F, class... Ts>
   inline auto operator()(F&& f, Ts&&... ts) const
      -> decltype(f(std::forward<Ts>(ts)...))
   {
      return f(std::forward<Ts>(ts)...);
   }
};

struct invoke_deriv
{
   template<class F, class... Ts>
   inline auto operator()(F&& f, Ts&&... ts) const
      -> decltype(f.first_deriv(std::forward<Ts>(ts)...))
   {
      return f.first_deriv(std::forward<Ts>(ts)...);
   }
};


#endif /* ARGUMENT_TUPLE_H_INCLUDED */
