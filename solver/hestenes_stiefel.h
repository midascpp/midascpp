#ifndef HESTENES_STIEFEL_H_INCLUDED
#define HESTENES_STIEFEL_H_INCLUDED

#include <iostream>
#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"

namespace detail
{

template<class Signature>
class hestenes_stiefel_impl: public Ncg<Signature,hestenes_stiefel_impl<Signature> >
{
   public:
      
      Resul_t<Signature> beta() const 
      {
         Resul_t<Signature> beta = dot(this->m_df_new,Argum2_t<Signature>(this->m_df_new-this->m_df))/dot(Argum2_t<Signature>(this->m_df_new-this->m_df),this->m_p);
         return beta;
      }
   public:
      template<class... Ts>
      hestenes_stiefel_impl(Ts&&... ts): 
         Ncg<Signature,hestenes_stiefel_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} // namespace detail

template<class F, class Arg>
auto hestenes_stiefel(F&& f
                 , Arg&& arg
                 //, Ncg_args<F(Arg)>  ncg_args  = {}
                 //, Line_args<F(Arg)> line_args = {}
                 , Resul_t<F(Arg)> acc=1e-10
                 , bool summary=false
                 ) 
   -> decltype(wrap_the_function< ::detail::hestenes_stiefel_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{}))
{
   return wrap_the_function< ::detail::hestenes_stiefel_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{});
}

#endif /* HESTENES_STIEFEL_H_INCLUDED */
