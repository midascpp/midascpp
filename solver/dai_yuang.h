#ifndef DAI_YUANG_H_INCLUDED
#define DAI_YUANG_H_INCLUDED

#include <iostream>
#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"

namespace detail
{

template<class Signature>
class dai_yuang_impl: public Ncg<Signature,dai_yuang_impl<Signature> >
{
   public:
      
      Resul_t<Signature> beta() const 
      {
         Resul_t<Signature> beta = dot(this->m_df_new,this->m_df_new)/dot(Argum2_t<Signature>(this->m_df_new-this->m_df),this->m_p);
         beta = std::max(beta,Resul_t<Signature>(0.0));
         return beta;
      }
   public:
      template<class... Ts>
      dai_yuang_impl(Ts&&... ts): 
         Ncg<Signature,dai_yuang_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} // namespace detail

template<class F, class Arg>
auto dai_yuang(F&& f
                 , Arg&& arg
                 //, Ncg_args<F(Arg)>  ncg_args  = {}
                 //, Line_args<F(Arg)> line_args = {}
                 , Resul_t<F(Arg)> acc=1e-10
                 , bool summary=false
                 ) 
   -> decltype(wrap_the_function< ::detail::dai_yuang_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{}))
{
   return wrap_the_function< ::detail::dai_yuang_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{});
}

#endif /* DAI_YUANG_H_INCLUDED */
