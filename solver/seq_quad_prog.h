#ifndef SEQ_QUAD_PROG_H_INCLUDED
#define SEQ_QUAD_PROG_H_INCLUDED

#include"util.h"
#include"constraints.h"
#include"lagrangian.h"
#include"size.h"
#include"ilapack/dgesv.h"
//#include"pod_functions.h"

#include"../libmda/functions.h"

template<class A, class B>
void sqp_solve_lin_eq(A& a, B& b)
{
   // DGESV
   DGESV(a,b,ilapack::col_major());
}

class seq_quad_prog_impl
{
   public:
      template<class F, class... Ts, class Arg>
      Argum2_t<F(Arg)> apply(F&& f, constraints_pack<Ts...> c, Arg&& arg)
      {
         Argum2_t<F(Arg)> x(arg);
         decltype(x)      x_new = x;
         
         auto arg_size = size(x); 
         auto con_size = c.size(); // constraints size
         //std::cout << " CON _SIZE : " << con_size << std::endl;
         auto tot_size = arg_size + con_size; // row/col size
         
         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         // fix types for these ! !!!
         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         SDArray2D<double> kkt_matrix(tot_size,tot_size); // kkt_matrix in column major form
         SDArray1D<double> rhs(tot_size);                 // right hand side
         
         // init lambdas (FIX TYPE !)
         SDArray1D<double> lambdas(con_size);
         for(size_t i = 0; i<con_size; ++i)
            lambdas(i) = 1.0; // init to 1 or 0?

         auto lagrange = make_lagrangian(f,c);

         std::cout << " arg size: " << arg_size << std::endl;
         std::cout << " con size: " << con_size << std::endl;

         bool converged = false;
         unsigned int k = 0;
         decltype(x) deriv_f_k = x; 
         
         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         // FIX TYPE FOR THESE !
         //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         SDArray2D<double> second_deriv_f_k(arg_size,arg_size);
         SDArray1D<double> c_k(con_size);
         SDArray2D<double> A_k(c.size(),arg_size);

         int iii;
         unsigned int k_max = 10;
         // main loop
         while(!converged && k<k_max)
         {
            ///////////
            // evaluate f_k, \nabla f_k,  \nabla^2 L_k, c_k, and A_k
            ///////////
            auto f_k           = f(x);
            invoke_first_deriv(f,x,deriv_f_k);
            call_second_deriv (lagrange,x,lambdas,second_deriv_f_k);
            c(x,c_k);          // evaluate constraints
            c.jacobian(x,A_k); // evaluate jacobian
            
            /////////
            // load values into kkt matrix and rhs
            /////////
            // load 2nd deriv into kkt matrix
            load_second_deriv(kkt_matrix,second_deriv_f_k,arg_size);
            
            // load jacobian into kkt matrix
            for(size_t j = 0; j<con_size; ++j)
               for(size_t i = 0; i<arg_size; ++i)
               {
                  kkt_matrix(i,j+arg_size) = A_k(j,i);
                  kkt_matrix(j+arg_size,i) = -A_k(j,i);
               }
            
            // zero rest of kkt matrix
            for(size_t i = arg_size; i<tot_size; ++i)
               for(size_t j = arg_size; j<tot_size; ++j)
                  kkt_matrix(i,j) = 0;

            // load 1st deriv into right hand side
            load_first_deriv(rhs,deriv_f_k,arg_size);
            
            // load constraint into right hand side
            for(size_t i = 0; i<con_size; ++i)
               rhs(arg_size+i) = -c_k(i);
            

            // some debug output
            std::cout << "x   = " << x   << std::endl;
            std::cout << "f_k = " << f_k << std::endl;
            std::cout << "c_k = " << c_k << std::endl;
            std::cout << "A_k = " << A_k;
            std::cout << "f'' = \n" << second_deriv_f_k;
            std::cout << "deriv_f_k = " << deriv_f_k << std::endl; 
            
            std::cout << " KKT = \n" << kkt_matrix << std::endl;
            std::cout << " RHS = " << rhs << std::endl;
            
            //////////
            // solve set of linear equations
            //////////
            sqp_solve_lin_eq(kkt_matrix,rhs);
            
            std::cout << " after solve RHS = " << rhs << std::endl;
            
            //////////
            // set x_{k+1} <- x_k + p_k and \lambda_{k+1} <- l_k
            //////////
            update_solution(x,rhs,x_new,arg_size);
            update_lambdas(rhs,arg_size,con_size,lambdas);
            
            std::cout << " x_new = " << x_new << std::endl;

            //std::cin >> iii;
            
            //////////
            // check for convergence
            /////////
            if(check_convergence(x,x_new,1e-5))
               converged = true;

            x = x_new;

            ++k; // increase iteration counter
         }
         return x;
      }

   private:
      /////
      // function to check for convergence
      //
      /////
      template<class T, class U, class V>
      bool check_convergence(T&& t, U&& u, V&& v)
      {
         return libmda::mda_norm(t-u)<v;
      }

      /////////
      // load second derivative into kkt matrix using tag dispatch
      //
      ////////
      template<class T, class U>
      void load_second_deriv_impl(T&& kkt, U&& second_deriv, size_t s, float_tag)
      {
         kkt(0,0) = second_deriv;
      }
      template<class T, class U>
      void load_second_deriv_impl(T&& kkt, U&& second_deriv, size_t s, mda_tag)
      {
         for(size_t i = 0; i<s; ++i)
            for(size_t j = 0; j<s; ++j)
               kkt(i,j) = second_deriv(j,i);
      }
      template<class T, class U>
      void load_second_deriv(T&& kkt, U&& second_deriv, size_t s)
      {
         load_second_deriv_impl(std::forward<T>(kkt)
                              , std::forward<U>(second_deriv)
                              , s
                              , Get_tag_type<typename std::decay<U>::type>()
                              );
      }

      //////
      // load first derivative into rhs matrix using tag dispatch
      //
      /////
      template<class T, class U>
      void load_first_deriv_impl(T&& rhs, U&& first_deriv, size_t s, float_tag)
      {
         rhs(0) = -first_deriv;
      }
      template<class T, class U>
      void load_first_deriv_impl(T&& rhs, U&& first_deriv, size_t s, mda_tag)
      {
         for(size_t i = 0; i < s; ++i)
            rhs(i) = -first_deriv(i);
      }
      template<class T, class U>
      void load_first_deriv(T&& rhs, U&& first_deriv, size_t s)
      {
         load_first_deriv_impl(std::forward<T>(rhs)
                             , std::forward<U>(first_deriv)
                             , s
                             , Get_tag_type<typename std::decay<U>::type>()
                             );
      }

      /////
      // call second derivative
      //
      ////
      template<class F, class Arg, class L, class D>
      void call_second_deriv_impl(F&& f, Arg&& arg, L&& l, D&& d, float_tag)
      {
         f.second_deriv(std::forward<Arg>(arg)
                      , std::forward<L>(l)
                      , d(0,0)
                      );
      }
      template<class F, class Arg, class L, class D>
      void call_second_deriv_impl(F&& f, Arg&& arg, L&& l, D&& d, mda_tag)
      {
         f.second_deriv(std::forward<Arg>(arg)
                      , std::forward<L>(l)
                      , std::forward<D>(d)
                      );
      }
      template<class F, class Arg, class L, class D>
      void call_second_deriv(F&& f, Arg&& arg, L&& l, D&& d)
      {
         call_second_deriv_impl(std::forward<F>(f)
                              , std::forward<Arg>(arg)
                              , std::forward<L>(l)
                              , std::forward<D>(d)
                              , Get_tag_type<typename std::decay<Arg>::type>()
                              );
      }

      //////
      // update x
      //
      //////
      template<class T, class U, class V>
      void update_solution_impl(T&& x, U&& rsh, V&& x_new, size_t s, float_tag)
      {
         x_new = x + rsh(0);
      }
      template<class T, class U, class V>
      void update_solution_impl(T&& x, U&& rsh, V&& x_new, size_t s, mda_tag)
      {
         for(size_t i=0; i<s; ++i)
         {
            x_new(i) = x(i) + rsh(i);
         }
      }
      template<class T, class U, class V>
      void update_solution(T&& x, U&& rsh, V&& x_new, size_t s)
      {
         update_solution_impl(std::forward<T>(x)
                            , std::forward<U>(rsh)
                            , std::forward<V>(x_new)
                            , s
                            , Get_tag_type<typename std::decay<T>::type>()
                            );
      }

      ////
      // update lambda
      //
      /////
      template<class T, class L>
      void update_lambdas(T&& rhs, size_t as, size_t cs, L&& l)
      {
         for(size_t i=0; i<cs; ++i)
            l(i) = rhs(as+i);
      }
};

template<class F, class... Ts, class Arg>
auto seq_quad_prog(F&& f, constraints_pack<Ts...> c, Arg&& arg)
   -> decltype(seq_quad_prog_impl().apply(std::forward<F>(f)
                               , c
                               , std::forward<Arg>(arg) 
                               ) )
{  
   return seq_quad_prog_impl().apply(std::forward<F>(f)
                                   , c
                                   , std::forward<Arg>(arg) 
                                   );
}

#endif /* SEQ_QUAD_PROG_H_INCLUDED */
