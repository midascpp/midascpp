#ifndef FUNC_1D_H_INCLUDED
#define FUNC_1D_H_INCLUDED

#include"function_matrix.h"

struct func_1d
{
   private:
      func_1d(const func_1d&) = delete;
   
   public:
   func_1d() = default;

   // function x^2 + x
   double operator()(double arg)  const { return arg*arg + arg; }

   //double first_deriv(double arg) const 
   //{ 
   //   std::cout << " using func_1d deriv " << std::endl;
   //   return 2.0*arg + 1; 
   //}
   
   void first_deriv(double arg, double& der) const 
   { 
      //std::cout << " using func_1d deriv reference version " << std::endl;
      der = 2.0*arg + 1; 
   }
   
   void second_deriv(double arg, double& der) const
   {
      der = 2.0;
   }
};

struct func_1d_c1
{
   // x = 1
   double operator()(double arg) const
   {
      return arg - 1;
   }

   void first_deriv(double arg, double& der) const
   {
      der = 1;
   }

   void sub_second_deriv(double arg, double l, double& der) const
   {
      // DO NOTHING! (there is no second derivative of constraint)
      
      //auto f = make_function_matrix([&arg](int i, int j)->double { 0;});
      //std::cout << f.at(1,2) << std::endl;
      //der = f.at(0,0);
      //der -= 0.0;
   }
};

#endif  /* FUNC_1D_H_INCLUDED */
