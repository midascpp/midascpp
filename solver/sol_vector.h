#ifndef SOL_VECTOR_H_INCLUDED
#define SOL_VECTOR_H_INCLUDED

#include <utility>
#include "../libmda/arrays/SDArray1D.h"

template<class tensor_t, class vector_t = midas::mmv::SDArray1D<double> >
class sol_vector: public vector_t
{
   private:
      /////
      // reference to original tensor
      // (needed for dimension information)
      ////
      const tensor_t& _tens;
      
   public:
      using vector_t::operator();
      using vector_t::operator=;

      template<typename... Ts>
      sol_vector(const tensor_t& t, Ts&&... ts): vector_t(std::forward<Ts>(ts)...) 
                                               , _tens(t)
      { 
      }

      /////
      // calculate different offsets
      //
      /////
      size_t off1() const 
      { 
         return 0; 
      }

      size_t off2() const 
      { 
         return off1()+_tens.template extent<0>(); 
      }

      size_t off3() const 
      { 
         return off2()+_tens.template extent<1>(); 
      }
      
      size_t off12() const 
      { 
         return off3()+_tens.template extent<2>(); 
      }

      size_t off13() const 
      { 
         return off12()+_tens.template extent<0>()*_tens.template extent<1>(); 
      }

      size_t off23() const 
      { 
         return off13()+_tens.template extent<0>()*_tens.template extent<2>(); 
      }
      
      size_t offc() const  
      { 
         return off23()+_tens.template extent<1>()*_tens.template extent<2>(); 
      } // NB HARDCODED FOR 3D
      
      /////
      // get different sub arrays
      //
      /////
      // get T_1
      auto t1() 
         -> decltype(std::declval<vector_t>().template sub_array<1>(std::declval<sol_vector>().off1(),_tens.template extent<0>(),1))
      { 
         return vector_t::template sub_array<1>(off1(),_tens.template extent<0>(),1); 
      }
      
      // get T_2
      auto t2() 
         -> decltype(std::declval<vector_t>().template sub_array<1>(std::declval<sol_vector>().off2(),_tens.template extent<1>(),1))
      { 
         return vector_t::template sub_array<1>(off2(),_tens.template extent<1>(),1); 
      }
      
      // get T_3
      auto t3() 
         -> decltype(std::declval<vector_t>().template sub_array<1>(std::declval<sol_vector>().off3(),_tens.template extent<2>(),1))
      { 
         return vector_t::template sub_array<1>(off3(),_tens.template extent<2>(),1); 
      }

      // get T_12
      auto t12() 
         -> decltype(std::declval<vector_t>().template sub_array<2>(std::declval<sol_vector>().off12(),_tens.template extent<0>(),_tens.template extent<1>(),1,1))
      { 
         return vector_t::template sub_array<2>(off12(),_tens.template extent<0>(),_tens.template extent<1>(),1,1); 
      }
      
      // get T_13
      auto t13() 
         -> decltype(std::declval<vector_t>().template sub_array<2>(std::declval<sol_vector>().off13(),_tens.template extent<0>(),_tens.template extent<2>(),1,1))
      { 
         return vector_t::template sub_array<2>(off13(),_tens.template extent<0>(),_tens.template extent<2>(),1,1); 
      }
      
      // get T_23
      auto t23() 
         -> decltype(std::declval<vector_t>().template sub_array<2>(std::declval<sol_vector>().off23(),_tens.template extent<1>(),_tens.template extent<2>(),1,1))
      { 
         return vector_t::template sub_array<2>(off23(),_tens.template extent<1>(),_tens.template extent<2>(),1,1); 
      }
      
      // get c
      auto c() 
         -> decltype(std::declval<vector_t>().template sub_array<1>(std::declval<sol_vector>().offc(),4,1))
      { 
         return vector_t::template sub_array<1>(offc(),4,1); 
      }
};

#endif /* SOL_VECTOR_H_INCLUDED */
