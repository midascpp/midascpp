#ifndef BEALES_FUNC_H_INCLUDED
#define BEALES_FUNC_H_INCLUDED

#include <cmath>

#include "typedefs.h"
#include "../libmda/util/require.h"

struct beales_func
{
   /*! Evaluate function
    *
    */
   template<class T
          , libmda::Require_order<T,1> = 0
          >
   double operator()(T&& arg) const
   {
      assert(arg.size() == 2);
      return (1.5 - arg.at(0) + arg.at(0)*arg.at(1))*(1.5 - arg.at(0) + arg.at(0)*arg.at(1))
           + (2.25 - arg.at(0) + arg.at(0)*arg.at(1)*arg.at(1))*(2.25 - arg.at(0) + arg.at(0)*arg.at(1)*arg.at(1))
           + (2.625 - arg.at(0) + arg.at(0)*arg.at(1)*arg.at(1)*arg.at(1))*(2.625 - arg.at(0) + arg.at(0)*arg.at(1)*arg.at(1)*arg.at(1));
   }
};

#endif /* BEALES_FUNC_H_INCLUDED */
