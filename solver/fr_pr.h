#ifndef FR_PR_H_INCLUDED
#define FR_PR_H_INCLUDED

#include <iostream>
#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"
#include "libmda/numeric/float_eq.h"
//#include "util/Error.h"

namespace detail
{

template<class Signature>
class fr_pr_impl: public Ncg<Signature,fr_pr_impl<Signature> >
{
   public:
      
      Resul_t<Signature> beta() const 
      {
         Resul_t<Signature> beta_fr = dot(this->m_df_new,this->m_df_new)/dot(this->m_df,this->m_df);
         Resul_t<Signature> beta_pr = dot(this->m_df_new,Argum2_t<Signature>(this->m_df_new-this->m_df))/dot(this->m_df,this->m_df);

         if(libmda::numeric::float_lt(beta_pr, -beta_fr)) return -beta_fr;
         else if(libmda::numeric::float_leq(std::fabs(beta_pr), beta_fr)) return beta_pr;
         else if(libmda::numeric::float_gt(beta_pr, beta_fr)) return beta_fr;
         //else MIDASERROR("Unknown condition");
         else assert(false);
         
         // will never get here!
         return static_cast<Resul_t<Signature> >(0.0);
      }
   public:
      template<class... Ts>
      fr_pr_impl(Ts&&... ts): 
         Ncg<Signature,fr_pr_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} // namespace detail

template<class F, class Arg>
auto fr_pr( F&& f
          , Arg&& arg
          //, Ncg_args<F(Arg)>  ncg_args  = {}
          //, Line_args<F(Arg)> line_args = {}
          , Resul_t<F(Arg)> acc=1e-10 
          , bool summary=false
          ) 
   -> decltype(wrap_the_function< ::detail::fr_pr_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{}))
{
   return wrap_the_function< ::detail::fr_pr_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{});
}

#endif /* FR_PR_H_INCLUDED */
