/**
************************************************************************
* 
* @file    downhill_simplex.h
*
* @date    02-11-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Generalizing Gunnar's implementation of the downhill simplex method 
*     originally written for LaplaceQuadrature optimization.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef DOWNHILL_SIMPLEX_H_INCLUDED
#define DOWNHILL_SIMPLEX_H_INCLUDED

#include "util/RandomNumberGenerator.h"

namespace solver
{
namespace detail
{
/**
 * Generate points for downhill-simplex optimization
 *
 * @param simplex          First vector is start guess. The rest of the simplex is generated from these values.
 * @param volume_factor    Parameter that determines the volume of the generated simplex. 
 **/
template
   <  class T
   >
void initialize_simplex
   (  std::vector<std::vector<T>>& simplex
   ,  T volume_factor = static_cast<T>(1.e-3)
   )
{
   assert(simplex.size() == simplex.at(0).size() + 1);

   // Size of simplex
   auto dim = simplex.size() - 1;

   T one_minus_vol = static_cast<T>(1.) - volume_factor;

   for(size_t ipoint = 0; ipoint < dim; ++ipoint)
   {
      for(size_t ival = 0; ival < dim; ++ival)
      {
         // Generate random variation
         auto rand = one_minus_vol + volume_factor*midas::util::RandomSignedFloat<T>();

         simplex[ipoint+1][ival] = std::exp(rand * std::log(simplex[0][ival]));
      }
   }
}

} /* namespace detail */


/**
 * Downhill simplex algorithm
 *
 * Purpose: search minimum of a multidimensional function using downhill simplex algorithm
 * 
 * Literature: Numerical Recipes; Press, Teukolsky, Vetterling, Flannery;
 *             Third Edition, 2007
 *
 * @param err_func      The error function (must have operator() implemented and a typename val_t).
 * @param point         The starting simplex.
 * @param yval          Values of the error function for different points in the simplex.
 * @param thrrel
 * @param thrabs
 * @param maxiter
 * @param iter
 * @param failed
 * @param converged
 **/
template
   <  class ERRFUNC
   >
void downhill_simplex
   (  ERRFUNC& err_func
   ,  std::vector< std::vector<typename ERRFUNC::val_t> >& point
   ,  std::vector<typename ERRFUNC::val_t>& yval
   ,  typename ERRFUNC::val_t thrrel
   ,  typename ERRFUNC::val_t thrabs
   ,  int maxiter
   ,  int& iter
   ,  bool& failed
   ,  bool& converged
   )
{
   using val_t = typename ERRFUNC::val_t;

   const bool locdbg = false;

   size_t ndim = point.size() - 1;

   static const val_t eps = std::numeric_limits<val_t>::epsilon();  

   val_t one = 1.0;
   val_t two = 2.0;
   val_t half = 0.5;
   val_t zero = 0.0; 

   val_t ytry  = 0.0;
   val_t ysave = 0.0;

   int ihigh, ilow, inexthi;

   val_t reldiff = 0.0;
   val_t absdiff = 0.0;
   val_t swap;

   val_t thrshld = thrabs;

   failed = false;

   iter = 0;

   std::vector<val_t> psum(ndim);
   std::vector<val_t> ptry(ndim);

   //---------------------------------------
   // determine the midpoint of the simplex:
   //---------------------------------------
   psum = point[0];
//   for (int ipoint = 0; ipoint < ndim + 1; ++ipoint)   // Niels: This was a bug, right?
   for (int ipoint = 1; ipoint < ndim + 1; ++ipoint)
   {
      for (int idx = 0; idx < ndim; ++idx)
      {
         psum[idx] += point[ipoint][idx];
      } 
   }

   converged = false;

   while (iter < maxiter && !converged)
   {
      Mout  << " =-=- Downhill Simplex iteration " << iter << " -=-=" << std::endl;
      //----------------------------------------------------------
      // determine highest, next highest and lowest result values:
      //----------------------------------------------------------
      if (yval[0] > yval[1]) 
      {
         ihigh = 0;
         inexthi = 1;
      }
      else
      {
         ihigh = 1;
         inexthi = 0;
      }
      ilow = 0;

      for (int ipoint = 0; ipoint < ndim + 1; ++ipoint)
      {
         if (yval[ipoint] <= yval[ilow]) ilow = ipoint;
         if (yval[ipoint] > yval[ihigh]) 
         {
            inexthi = ihigh;
            ihigh   = ipoint;
         }
         else if (yval[ipoint] > yval[inexthi] && ipoint != ihigh ) 
         {
            inexthi = ipoint;
         }
      } 
      //----------------------------------------------------------
      // calculate the relative difference between the highest and
      // lowest result values and check for convergence:
      //----------------------------------------------------------
      absdiff = std::abs(yval[ihigh] - yval[ilow]);
      reldiff = absdiff / ( half * (std::abs(yval[ihigh]) + std::abs(yval[ilow]) + eps )); // add eps so that we never divide by zero

      if (locdbg) Mout << "absdiff " << absdiff << " reldiff " << std::endl;

      if (reldiff < thrrel ||  absdiff < thrabs)
      {
         converged = true;

         //--------------------------------------
         // swap minimal point to first position:
         //--------------------------------------
         swap       = yval[0];
         yval[0]    = yval[ilow];
         yval[ilow] = swap;
         point[0].swap(point[ilow]);

         Mout  << "    Converged!\n"
               << "       Err:   " << yval[0] << "\n"
               << "       Point: " << point[0] << "\n"
               << std::flush;

         break;
      }

      // set threshhold for function
      thrshld = std::max(static_cast<val_t>(reldiff * (std::abs(yval[ihigh]) + std::abs(yval[ilow])) * 0.01), thrabs);

      //--------------------------------------------------------------
      // reflected highest point at the midpoint of remaining simplex:
      //--------------------------------------------------------------
      val_t fac1 = (one - (-one))/ndim;
      val_t fac2 = fac1 - (-one);
      for (int idx = 0; idx < ndim; idx++)
      {
         ptry[idx] = fac1 * psum[idx] - fac2 * point[ihigh][idx];
      }
      ytry = err_func(ptry, thrshld, failed);
      iter = iter + 1;
      if (failed) break;

      if (ytry < yval[ihigh])
      { 
         // accept new point...
         yval[ihigh] = ytry;
         for (int idx = 0; idx < ndim; idx++)
         {
            psum[idx] += ptry[idx] - point[ihigh][idx] ; 
            point[ihigh][idx] = ptry[idx]; 
         }
      }

      //-----------------------------------------------
      // simplex expansion or contraction:
      //-----------------------------------------------
      if (ytry <= yval[ilow])
      {
         // new point better than all other points
         //  --> expand simplex in the direction of the new point
         //      by increasing the step
         val_t fac1 = (one-(two))/ndim;
         val_t fac2 = fac1-(two);
         for (int idx = 0; idx < ndim; idx++)
         {
            ptry[idx] += fac1 * psum[idx] - fac2 * point[ihigh][idx];
         }
         ytry = err_func(ptry, thrshld, failed);
         iter = iter + 1;
         if (failed) break;

         if (ytry < yval[ihigh])
         {
            // accept new point...
            yval[ihigh] = ytry;
            for (int idx = 0; idx < ndim; idx++)
            {
               psum[idx] += ptry[idx] - point[ihigh][idx];
               point[ihigh][idx] = ptry[idx];
            }
         }
      }
      else if (ytry >= yval[inexthi])
      {
         // not a significant improvement
         //  --> contract simplex in the present search direction
         //      by decreasing the step
         ysave = yval[ihigh];
         val_t fac1 = (one - (half)) / ndim;
         val_t fac2 = fac1 - (half);
         for (int idx = 0; idx < ndim; idx++)
         {
            ptry[idx] += fac1 * psum[idx] - fac2 * point[ihigh][idx];
         }
         ytry = err_func(ptry, thrshld, failed);
         iter = iter + 1;
         if (failed) break;

         if (ytry < yval[ihigh])
         {
            // accept new point...
            yval[ihigh] = ytry;
            for (int idx = 0; idx < ndim; idx++)
            {
               psum[idx] += ptry[idx] - point[ihigh][idx];
               point[ihigh][idx] = ptry[idx];
            }
         }

         // if the result got worse, contract the whole simplex
         // around the present optimal point:
         if (ytry >= ysave) 
         {
            for (int ipoint = 0; ipoint < ndim + 1; ipoint++)
            {
               if (ipoint != ilow) 
               {
                  for (int idx = 0; idx < ndim; idx++)
                  {
                     point[ipoint][idx] = psum[idx] = half * (point[ipoint][idx] +  point[ilow][idx] );
                  }
                  yval[ipoint] = err_func(psum, thrshld, failed);
                  if (failed) break;
               }
            }
            iter = iter + ndim;

            // determine midpoint of contracted simplex:
            for (int idx = 0; idx < ndim; idx++)
            {
               val_t sum = 0.0;
               for (int ipoint = 0; ipoint < ndim + 1; ipoint++)
               {
                  sum += point[ipoint][idx];
               } 
               psum[idx] = sum;
            }
         }
      }
   }
}

} /* namespace solver */

#endif /* DOWNHILL_SIMPLEX_H_INCLUDED */
