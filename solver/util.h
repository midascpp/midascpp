#ifndef SOLVER_UTIL_H_INCLUDED
#define SOLVER_UTIL_H_INCLUDED

namespace detail
{
   //////
   // type sink
   //
   /////
   template<typename... Ts>
   struct type_sink: std::true_type 
   {
   };

} // namespace detail

#endif /* SOLVER_UTIL_H_INCLUDED */
