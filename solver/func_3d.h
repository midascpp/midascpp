#ifndef FUNC_3D_H_INCLUDED
#define FUNC_3D_H_INCLUDED

#include<utility>
#include"../libmda/util/require.h"
#include"../libmda/util/enforce.h"

struct func_3d
{
   // X^2 + Y^2 + Z^2
   
   template<class T, libmda::Enforce_order<T,1> = 0> 
   auto operator()(T&& arg) const 
      -> typename std::remove_reference<T>::type::value_type
   {
      assert(arg.size() == 3);
      return arg.at(0)*arg.at(0) + arg.at(1)*arg.at(1) + arg.at(2)*arg.at(2);
   }

   template<class T, libmda::Enforce_order<T,1> = 0> 
   void first_deriv(T&& arg, T&& der) const
   {
      assert(arg.size() == 3 && der.size() == 3);
      der(0) = 2.0*arg.at(0);
      der(1) = 2.0*arg.at(1);
      der(2) = 2.0*arg.at(2);
   }
   
   template<class T
          , class U
          , libmda::Enforce_order<T,1> = 0
          , libmda::Enforce_order<U,2> = 0
          > 
   void second_deriv(T&& arg, U&& der) const
   {
      assert(arg.size() == 3 && der.size() == 9);
      der(0,0) = 2.0; der(0,1) = 0.0; der(0,2) = 0.0;
      der(1,0) = 0.0; der(1,1) = 2.0; der(1,2) = 0.0;
      der(2,0) = 0.0; der(2,1) = 0.0; der(2,2) = 2.0;
   }
};

struct func_3d_c1
{
   // x + y = 2.0
   template<class T, libmda::Enforce_order<T,1> = 0>
   double operator()(T&& arg) const
   {
      return arg.at(0) + arg.at(1) - 2.0;
   }

   template<class T
          , class U
          , libmda::Enforce_order<T,1> = 0
          , libmda::Enforce_order<U,1> = 0
          >
   void first_deriv(T&& arg, U&& d) const
   {
      assert(arg.size() == 3 && d.size() == 3);
      d(0) = 1.0; d(1) = 1.0; d(2) = 0.0;
   }
   
   template<class T
          , class L
          , class D
          , libmda::Enforce_order<T,1> = 0
          , libmda::Enforce_order<D,2> = 0
          >
   void sub_second_deriv(T&& arg, L&& l, D&& d) const
   {
      assert(arg.size() == 3 && d.size() == 9);
      // do nothing (no second derivative)
      //d(0) = 1.0; d(1) = 1.0; d(2) = 0.0;
   }
};

struct func_3d_c2
{
   // x + y = 2.0
   template<class T, libmda::Enforce_order<T,1> = 0>
   double operator()(T&& arg) const
   {
      return arg.at(0) + arg.at(2) - 2.0;
   }

   template<class T
          , class U
          , libmda::Enforce_order<T,1> = 0
          , libmda::Enforce_order<U,1> = 0
          >
   void first_deriv(T&& arg, U&& d) const
   {
      assert(arg.size() == 3 && d.size() == 3);
      d(0) = 1.0; d(1) = 0.0; d(2) = 1.0;
   }
   
   template<class T
          , class L
          , class D
          , libmda::Enforce_order<T,1> = 0
          , libmda::Enforce_order<D,2> = 0
          >
   void sub_second_deriv(T&& arg, L&& l, D&& d) const
   {
      assert(arg.size() == 3 && d.size() == 9);
      // do nothing (no second derivative)
      //d(0) = 1.0; d(1) = 1.0; d(2) = 0.0;
   }
};

#endif /* FUNC_3D_H_INCLUDED */
