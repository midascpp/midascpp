#ifndef TAG_H_INCLUDED
#define TAG_H_INCLUDED

/////
// Tagging mechanism to witch between array and floating point
//
////
struct float_tag {};
struct mda_tag   {};

template<class T>
struct get_tag_type         { using type = mda_tag; };
template<>
struct get_tag_type<float>  { using type = float_tag; };
template<>
struct get_tag_type<double> { using type = float_tag; };

template<class T>
using Get_tag_type = typename get_tag_type<T>::type;

#endif /* TAG_H_INCLUDED */
