#ifndef NCG_H_INCLUDED
#define NCG_H_INCLUDED

//std headers
#include<cmath>

//Midas headers
#include"typedefs.h"
#include"phi_func.h"
#include"convergence.h"
#include"function_wrapper.h"
#include"result_of_aug.h"
#include"invoke_derivatives.h"
#include"libmda/numeric/float_eq.h"
 
#include "../mmv/StdVectorOperators.h"

#include "libmda/numeric/optim/wolfe_line_search.h"

template<class Sig>
struct Ncg_args;
template<class F, class... Args>
struct Ncg_args<F(Args...)>
{
   Ncg_args(const Resul_t<F(Args...)> accuracy = 1e-10
          , const bool                summary  = false): 
      _accuracy(accuracy)
    , _summary(summary)
   {
   }

   const Resul_t<F(Args...)> _accuracy;
   const bool                _summary;
};

template<class Sig>
struct Line_args;
template<class F, class... Args>
struct Line_args<F(Args...)>
{
   Line_args(const Resul_t<F(Args...)> alpha_max = 10
           , const Resul_t<F(Args...)> c_1 = 1e-4
           , const Resul_t<F(Args...)> c_2 = 0.1):
      _alpha_max(alpha_max)
    , _c_1(c_1)
    , _c_2(c_2)
                               
   {
   }

   const Resul_t<F(Args...)> _alpha_max;
   const Resul_t<F(Args...)> _c_1;
   const Resul_t<F(Args...)> _c_2;
};

template<class A>
struct mixin_self
{
   A& self()
   { return static_cast<A&>(*this); }

   A const& self() const
   { return static_cast<A const&>(*this); }
};

template<class F, class A>
class Ncg;

template<class F, class T>
void plot_stuff(const F&, const T&, const T&, const T&, int k)
{
   std::cout << " PLOT NOT IMPLEMENTED " << std::endl;
   //assert(false);
}

template<class F, class Arg, class A>
class Ncg<F(Arg),A>: public mixin_self<A>
{
   private:
      const F& m_f;
   
   protected:
      // data needed by different methods to calculate beta
      Argum2_t<F(Arg)> m_x;
      Argum2_t<F(Arg)> m_x_new;
      Resul_t<F(Arg)>  m_f_0;
      Argum2_t<F(Arg)> m_df;
      Argum2_t<F(Arg)> m_df_new;
      Argum2_t<F(Arg)> m_p;
      Argum2_t<F(Arg)> m_p_new;
   
   private:
      const Resul_t<F(Arg)> m_acc;
      const bool            m_summary;

      const Resul_t<F(Arg)> m_alpha_max;
      const Resul_t<F(Arg)> m_c_1;
      const Resul_t<F(Arg)> m_c_2;
      
      template<class FF, class Argg, class D>
      D&& invoke_and_return_first_deriv(FF&& f, Argg&& arg, D&& d)
      {
         invoke_first_deriv(std::forward<FF>(f),std::forward<Argg>(arg),std::forward<D>(d));
         return std::forward<D>(d);   
      }
   
   protected:
      //ncg(F&& f, Arg&& x_0, Resul_t<F(Arg)> acc, bool summary):
      Ncg(F&& f, Arg&& x_0, Ncg_args<F(Arg)> ncg_args, Line_args<F(Arg)> line_args):
           m_f(std::forward<F>(f))
         , m_x(std::forward<Arg>(x_0))
         , m_x_new(m_x)
         , m_f_0(m_f(m_x))
         , m_df(m_x)
         , m_df_new(m_df)
         , m_p(-invoke_and_return_first_deriv(m_f,m_x,m_df)) // not pretty, but it works
         , m_p_new(-invoke_and_return_first_deriv(m_f,m_x,m_df)) // not pretty, but it works
         , m_acc(ncg_args._accuracy)
         , m_summary(ncg_args._summary)
         , m_alpha_max(line_args._alpha_max)
         , m_c_1(line_args._c_1)
         , m_c_2(line_args._c_2)
      {
      }
   
   public:
      auto solve() -> decltype(m_x)
      {
         int k = 0;
         Resul_t<F(Arg)> alpha_init = 0.0;
         bool restart = false;
         Resul_t<F(Arg)> alpha = 1.0;
         Resul_t<F(Arg)> beta;
         Resul_t<F(Arg)> f_eval = 1.0;
         while( !converged(m_df,m_acc) && (k<2000) && (alpha != 0.0)) 
         {
            phi_func<decltype(m_f)(Arg)>  phi(m_f,m_x,m_p);

//          Resul_t<F(Arg)> alpha = wolfe_line_search(phi,m_alpha_max,m_c_1,m_c_2);
            //auto alpha_max = 10.0/norm(m_p);
            auto alpha_max = 1000000.0;
            alpha = libmda::numeric::optim::wolfe_line_search(phi, alpha_init, alpha_max, 0.05, 1e-4, 0.1);
            //Resul_t<F(Arg)> alpha = wolfe_line_search(phi, 10.0, 1e-4, 0.1);
            
            //std::cout << " m_x            : " << m_x << std::endl;
            //std::cout << " m_p            : " << m_p << std::endl;
            //std::cout << " alpha          : " << alpha << std::endl;
            //std::cout << " m_x_new before : " << m_x_new << std::endl;
            m_x_new  = m_x + alpha*m_p;
            //std::cout << " m_x_new after  : " << m_x_new << std::endl;
            invoke_first_deriv(m_f, m_x_new, m_df_new);
            
            //Resul_t<F(Arg)> beta = this->self().beta();
            //m_p_new = -m_df_new + beta*m_p;

            if(!restart)
            {
               beta = this->self().beta();
               m_p_new = -m_df_new + beta*m_p;
            }
            else
            {
               beta = 0.0;
               m_p_new = -m_df_new;
            }
//            std::cout << " beta           : " << beta << std::endl;
            
            //plot_stuff(m_f, m_x_new, m_df_new, m_p_new, k);
            // print out
            if(m_summary)
            {
               std::cout<< " Summary iteration " << k << "\n"
                        << " f(x_new)          = " << (f_eval = m_f(m_x_new)) << "\n"
                        << " |f'(x)|           = " << norm(m_df_new) << "\n" 
                        << " step              = " << alpha << "\n"
                        << " step_max          = " << alpha_max << "\n"
                        << " beta              = " << beta << "\n"
                        << " |x_new|           = " << norm(m_x_new) << "\n"
                        << " |p|               = " << norm(m_p_new) << "\n"
                        //<< " |df|              = " << norm(m_df_new) << "\n"
                        //<< " |x_new-x|/|x_new| = " << norm(m_x-m_x_new)/norm(m_x_new) << "\n"
                        << std::endl;
            }

            restart = libmda::numeric::float_geq(std::fabs(dot(m_df_new, m_df))/dot(m_df_new, m_df_new), Resul_t<F(Arg)>(0.1));

            m_x  = m_x_new;
            m_df = m_df_new;
            m_p = m_p_new;

            k+=1;
//            if(k>1) exit(1);
         }
         return m_x;
      }
};

template<template <class> class T, class F, class Arg>
inline auto wrap_the_function(F&& f
                     , Arg&& arg
                     , Ncg_args<F(Arg)>  ncg_args
                     , Line_args<F(Arg)> line_args
                     )
   -> decltype(T<F(Arg)>(std::forward<F>(f),std::forward<Arg>(arg),ncg_args,line_args).solve())
{
   return T<F(Arg)>(std::forward<F>(f),std::forward<Arg>(arg),ncg_args,line_args).solve();
}


#endif /* NCG_H_INCLUDED */
