#ifndef TENSOR_CREATION_H_INCLUDED
#define TENSOR_CREATION_H_INCLUDED

#include <stdlib.h>
#include "../libmda/arrays/MultiDimArray.h"
#include "../libmda/arrays/SDArray1D.h"
#include "sol_vector.h"
#include "../libmda/functions.h"
#include "../libmda/expr/op_tensorprod.h"
using libmda::tensorprod;
using libmda::mda_norm;
using tens3d = libmda::MDA<double,3>;

tens3d dir_prod_3d(int i, int j, int k)
{
   tens3d tens(i,j,k);
   for(int ii=0; ii<i; ++ii)
   {
      double fi = 1.0/(ii+1.0);
      for(int jj=0; jj<j; ++jj)
      {
         double fj = 1.0/(jj+1.0);
         for(int kk=0; kk<k; ++kk)
         {
            double fk = 1.0/(kk+1.0);
            //std::cout << "(i,j,k) = (" << ii << "," << jj << "," << kk << ")" << std::endl;
            tens(ii,jj,kk) = fi*fj*fk;
         }
      }
   }
   
   return tens;
}


tens3d f_1_23_3d(int i, int j, int k)
{
   tens3d tens(i,j,k);
   for(int ii=0; ii<i; ++ii)
   {
      double fi = 1.0/(ii+1.0);
      for(int jj=0; jj<j; ++jj)
      {
         for(int kk=0; kk<k; ++kk)
         {
            double fjk = 1.0/(jj+kk+1.0);
            //std::cout << "(i,j,k) = (" << ii << "," << jj << "," << kk << ")" << std::endl;
            tens(ii,jj,kk) = fi*fjk;
         }
      }
   }
   
   return tens;
}

tens3d random_3d(int i, int j, int k)
{
   tens3d tens(i,j,k);
   for(int ii=0; ii<i; ++ii)
   {
      for(int jj=0; jj<j; ++jj)
      {
         for(int kk=0; kk<k; ++kk)
         {
            //std::cout << "(i,j,k) = (" << ii << "," << jj << "," << kk << ")" << std::endl;
            tens(ii,jj,kk) = double(rand())/RAND_MAX;
         }
      }
   }
   
   return tens;
}

//tens3d dir_prod_noise_3d(int i, int j, int k, double noise=1e-4)
//{
//   return dir_prod_3d(i,j,k) + noise*random_3d(i,j,k);
//}

//midas::mmv::SDArray1D<double> start_guess_random(const tens3d& tens)
sol_vector<tens3d> start_guess_random(const tens3d& tens)
{
   size_t size = tens.extent<0>()+tens.extent<1>()+tens.extent<2>()
               + tens.extent<0>()*tens.extent<1>()
               + tens.extent<0>()*tens.extent<2>()
               + tens.extent<1>()*tens.extent<2>();
   //midas::mmv::SDArray1D<double> vec(size);
   sol_vector<tens3d> vec(tens,size);
   
   //std::cout << " size = " << size << std::endl;

   //for(size_t i = 0; i<size; ++i)
   //   vec(i) = 0.0;
   //vec(0) = 1.0;
   double norm = 0;
   for(size_t i = 0; i<size; ++i)
   {
      vec(i) = double(rand())/RAND_MAX;
      norm += vec(i)*vec(i);
   }

   vec = vec/sqrt(norm);
   
   return vec;
   //return vec;
}

//midas::mmv::SDArray1D<double> start_guess(const tens3d& tens, int idx = 
sol_vector<tens3d> start_guess(const tens3d& tens, int idx = 0)
{
   size_t size = tens.extent<0>()+tens.extent<1>()+tens.extent<2>()
               + tens.extent<0>()*tens.extent<1>()
               + tens.extent<0>()*tens.extent<2>()
               + tens.extent<1>()*tens.extent<2>();
   //midas::mmv::SDArray1D<double> vec(size);
   sol_vector<tens3d> vec(tens,size);
   
   //std::cout << " size = " << size << std::endl;

   for(size_t i = 0; i<size; ++i)
      vec(i) = 0.0;
   vec(idx) = 1.0;
   //double norm = 0;
   //for(size_t i = 0; i<size; ++i)
   //{
   //   vec(i) = double(rand())/RAND_MAX;
   //   norm += vec(i)*vec(i);
   //}

   //vec = vec/sqrt(norm);
   
   return vec;
   //return vec;
}

//midas::mmv::SDArray1D<double> start_guess_norm(const tens3d& tens)
sol_vector<tens3d> start_guess_norm(const tens3d& tens)
{
   size_t size = tens.extent<0>()+tens.extent<1>()+tens.extent<2>()
               + tens.extent<0>()*tens.extent<1>()
               + tens.extent<0>()*tens.extent<2>()
               + tens.extent<1>()*tens.extent<2>() + 4 + 9;
   //midas::mmv::SDArray1D<double> vec(size);
   sol_vector<tens3d> vec(tens,size);
   
   //sol_vector<> vec(tens);
   //size_t size = vec.size();
   //std::cout << " size = " << size << std::endl;

   //for(size_t i = 0; i<size; ++i)
   //   vec(i) = 1.0;
   //vec(0) = 1.0;
   //double norm = 0;
   for(size_t i = 0; i<size; ++i)
   {
      vec(i) = double(rand())/RAND_MAX;
      //norm += vec(i)*vec(i);
   }
    
   auto t1  = vec.t1();
   auto t2  = vec.t2();
   auto t3  = vec.t3();
   auto t12 = vec.t12();
   auto t13 = vec.t13();
   auto t23 = vec.t23();
   auto c = vec.c();
   //auto lambda = vec.lambda();
   
   for(int i =0; i<t1.size(); ++i)
      t1(i) = 1.0/(i+1.0);

   for(int j=0; j<t23.extent<0>(); ++j)
      for(int k=0; k<t23.extent<1>(); ++k)
         t23(j,k) = 1.0/(j+k+1.0);
   
   c(0) = 0.0;
   c(1) = 0.0;
   c(2) = 0.0;
   c(3) = mda_norm(t1)*mda_norm(t23);

   t1 /= mda_norm(t1);
   
   t2 /= mda_norm(t2);
   t3 /= mda_norm(t3);

   t12 -= tensorprod(t1,t2)*mda_dot(t12,tensorprod(t1,t2));
   t13 -= tensorprod(t1,t3)*mda_dot(t13,tensorprod(t1,t3));
   //t23 -= tensorprod(t2,t3)*mda_dot(t23,tensorprod(t2,t3));
   //
   t12 /= mda_norm(t12);
   t13 /= mda_norm(t13);
   t23 /= mda_norm(t23);

   //lambda = 0.0;
   //lambda(0) = 1.0;
   //lambda(1) = 1.0;
   //lambda(2) = 1.0;
   //lambda(3) = 1.0;
   //lambda(4) = 1.0;
   //lambda(5) = 1.0;
   //lambda(6) = 1.0;
   //lambda(7) = 1.0;
   //lambda(8) = 1.0;
   ////vec = vec/sqrt(norm);
   
   return vec;
   //return vec;
}

#endif /* TENSOR_CREATION_H_INCLUDED */
