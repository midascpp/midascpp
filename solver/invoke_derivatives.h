#ifndef INVOKE_DERIVATIVES_H_INCLUDED
#define INVOKE_DERIVATIVES_H_INCLUDED

#include<iostream>
#include"util.h"

namespace detail
{

///////
// pass deriv type as last argument of deriv function
// 
//////
struct pass_deriv
{
   template<class F, class Arg, class D>
   inline void operator()(F&& f, Arg&& arg, D&& d) const
   {
      //std::cout << "We are at this point using passed derivative method!" << std::endl;
      f.first_deriv(std::forward<Arg>(arg),std::forward<D>(d));
   }
};

//////
// return deriv type from first derivative function
//
/////
struct return_deriv
{
   template<class F, class Arg, class D>
   inline void operator()(F&& f, Arg&& arg, D&& d) const
   {
      d = f.first_deriv(std::forward<Arg>(arg));
   }
};

///////
// helper class to redirect to return_deriv
//
//////
template<class,class=void>
struct first_deriv_help : pass_deriv
{
};

///////
// helper class to redirect to pass_deriv
//
//////
template<class F, class Arg, class D>
struct first_deriv_help<F(Arg,D)
                      , typename std::enable_if<type_sink<decltype(std::declval<F>().first_deriv(std::declval<Arg>()))>::value>::type
                      > : return_deriv
{
};

} // namespace detail

/////
// invoke first derivate wrapper function
//
/////
template<class F, class Arg, class D>
inline void invoke_first_deriv(F&& f, Arg&& arg, D&& d)
{
   ::detail::first_deriv_help<F(Arg,D)>()(std::forward<F>(f)
                                        , std::forward<Arg>(arg)
                                        , std::forward<D>(d));
}

//////
// invoke second derivative wrapper function
//
//////
template<class F, class... Args, class D>
inline void invoke_second_deriv(F&& f, D&& d, Args&&... args)
{
   f.second_deriv(std::forward<Args>(args)...,std::forward<D>(d));
}

#endif /* INVOKE_DERIVATIVES_H_INCLUDED */
