#ifndef LAGRANGIAN_H_INCLUDED
#define LAGRANGIAN_H_INCLUDED

template<class F, class C>
class lagrangian
{
   public:
      /////
      // constructor, takes both lvalue and rvalue reference
      //
      ////
      lagrangian(F& f, C& c) : _f(f), _c(c)
      {
      }

      /////
      // evaluate lagrangian
      //
      /////
      template<class Arg, class L, class R>
      void evaluate(Arg&& arg, L&& l, R&& r)
      {
         typename std::decay<L>::type con(l.size());
         _c(arg,con);              // evaluate constraints
         r = _f(arg) - dot(l,con); // calculate lagrangian
      }

      /////
      // calculate derivative of lagrangian
      //
      /////
      //template<class Arg, class L, class R>
      //void first_deriv(Arg&& arg, L&& l, R&& r)
      //{
      //   
      //}

      /////
      // calculate second derivative of lagrangian
      //
      /////
      template<class Arg, class L, class D>
      void second_deriv(Arg&& arg, L&& l, D&& d)
      {
         _f.second_deriv(arg,d);              // calc second deriv and put into result
         _c.subtract_second_derivs(arg,l,d);  // subtract second deriv of lambda times constraint
      }

   private:
      const F& _f; // function
      const C& _c; // constraints
};

template<class F, class C>
lagrangian<F,C> make_lagrangian(F& f, C& c)
{
   return lagrangian<F,C>(f,c);
}


#endif /* LAGRANGIAN_H_INCLUDED */
