#ifndef TYPEDEFS_H_INCLUDED
#define TYPEDEFS_H_INCLUDED

#include "../libmda/meta/std_wrappers.h"

// some usings for interface
template<class A>
using Argum_t = typename libmda::Remove_const<libmda::Remove_reference<A> >::argum_t;
template<class A>
using Value_t = typename libmda::Remove_const<libmda::Remove_reference<A> >::value_t;
template<class A>
using Step_t  = typename libmda::Remove_const<libmda::Remove_reference<A> >::step_t;

#endif /* TYPEDEFS_H_INCLUDED */
