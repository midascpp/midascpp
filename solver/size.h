#ifndef GET_SIZE_H_INCLUDED
#define GET_SIZE_H_INCLUDED

#include<typeinfo>
#include"../libmda/meta/std_wrappers.h"
#include"tag.h"
//using libmda::iEnable_if;
//using libmda::Is_pod;
//using libmda::Remove_reference;

namespace detail
{

template<class T>
//inline constexpr size_t get_size_impl(T&& t, float_tag)
inline constexpr size_t size_impl(T&& t, float_tag)
{
   return 1;
}

template<class T>
//inline size_t get_size_impl(T&& t, mda_tag)
inline size_t size_impl(T&& t, mda_tag)
{
   return t.size();
}

} // namespace detail

template<class T>
//inline size_t get_size(T&& t)
inline size_t size(T&& t)
{
   return ::detail::size_impl(std::forward<T>(t), Get_tag_type<libmda::meta::Decay<T> >());
}

#endif /* GET_SIZE_H_INCLUDED */
