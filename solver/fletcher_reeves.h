#ifndef FLETCHER_REEVES_H_INCLUDED
#define FLETCHER_REEVES_H_INCLUDED

#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"

namespace detail
{

template<class Signature>
class fletcher_reeves_impl: public Ncg<Signature,fletcher_reeves_impl<Signature> >
{
   public:
      Resul_t<Signature> beta() const 
      {
         Resul_t<Signature> beta = dot(this->m_df_new,this->m_df_new)/dot(this->m_df,this->m_df);
         return beta;
      }
   public:
      template<class... Ts>
      fletcher_reeves_impl(Ts&&... ts): 
         Ncg<Signature,fletcher_reeves_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} // namespace detail

template<class F, class Arg>
auto fletcher_reeves(F&& f
                   , Arg&& arg
                   , Resul_t<F(Arg)> acc=1e-10
                   , bool summary=false
                   ) 
   -> decltype(wrap_the_function< ::detail::fletcher_reeves_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg),{acc,summary},{}))
{
   return wrap_the_function< ::detail::fletcher_reeves_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg),{acc,summary},{});
}

#endif /* FLETCHER_REEVES_H_INCLUDED */
