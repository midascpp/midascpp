#ifndef FUNCTION_MATRIX_H_INCLUDED
#define FUNCTION_MATRIX_H_INCLUDED

template<class A>
class function_matrix_base
{

};

template<class F>
struct function_matrix : function_matrix_base<function_matrix<F> >
{
   template<class FF>
   function_matrix(FF&& f): _f(std::forward<FF>(f))
   {
   }

   template<class... Is>
   auto at(Is&&... is)
      -> decltype(std::declval<F>()(std::forward<Is>(is)...))
   {
      return _f(std::forward<Is>(is)...);
   }

   private:
      const F& _f;
};

template<class F>
function_matrix<F> make_function_matrix(F&& f)
{
   return function_matrix<F>(std::forward<F>(f));  
}

#endif /* FUNCTION_MATRIX_H_INCLUDED */
