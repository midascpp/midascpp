#ifndef CONSTRAINTS_H_INCLUDED
#define CONSTRAINTS_H_INCLUDED

#include <typeinfo>
#include <tuple>
#include "../libmda/meta/std_wrappers.h"
#include "../libmda/arrays/SDArray1D.h"
#include "../libmda/arrays/SDArray2D.h"
#include "invoke_derivatives.h"
#include "tag.h"
using midas::mmv::SDArray1D;
using midas::mmv::SDArray2D;
//using libmda::meta::Decay;


// forward declaration
template<class... Ts>
class constraints_pack;

namespace detail
{
//////////////
//
// recursively iterate through function calls dependent on size_t 
//
//
/////////////
//////
// general recursive implementation
//////
template<template<size_t> class A, size_t N, size_t M>
struct function_recursion
{
   template<class... Ts>
   void apply(Ts&&... ts)
   {
      A<N>().apply(ts...);
      function_recursion<A,N+1,M>().apply(std::forward<Ts>(ts)...);
   }
};

//////
// termination condition
/////
template<template<size_t> class A, size_t N>
struct function_recursion<A,N,N>
{
   template<class... Ts>
   void apply(Ts&&... ts)
   {
      // do nothing...
   }
};

//////////////
// functions/helpers for evaluating each constraint and return result
// 
////////////
/////
// evaluate constraint number N
///
template<size_t N>
struct evaluate_constraint
{
   template<class P, class C, class... Args>
   void apply(P&& p, C&& c, Args&&... args) 
   {
      c(N) = std::get<N>(p)(args...);
   }
};

//////
// interface to evaluting constraints
/////
template<class P, class C, class... Args>
void evaluate_constraints(P&& p, C&& c, Args&&... args)
{
   function_recursion<evaluate_constraint,0,libmda::meta::Decay<P>::size()>()
      .apply(p
           , std::forward<C>(c)
           , std::forward<Args>(args)...
           );
}

////////////////////////////
//
// functions/ helpers for evaluating constraints jacobian
//
//
////////////////////////////
template<size_t N>
struct evaluate_constraint_jacobian
{
   template<class C, class J, class... Args>
   void dispatch_first_deriv(C& c, J& j, float_tag, Args&&... args)
   {
      invoke_first_deriv(c,args...,j(N,0));
   }
   template<class C, class J, class... Args>
   void dispatch_first_deriv(C& c, J& j, mda_tag, Args&&... args)
   {
      SDArray1D<double> deriv(args...);
      invoke_first_deriv(c,args...,deriv);
      for(int i=0; i<deriv.size(); ++i)
         j(N,i) = deriv(i);
   }
   
   template<class P, class J, class... Args>
   void apply(P&& p, J&& j, Args&&... args)
   {
      dispatch_first_deriv(std::get<N>(p)
                         , std::forward<J>(j)
                         , Get_tag_type<libmda::meta::Decay<Args> >()...
                         , std::forward<Args>(args)...
                         ); 
   }
};

////
// inteface
//
// takes: P = pack, J = jacobian, Args = function arguments
////
template<class P, class J, class... Args>
void evaluate_constraints_jacobian(P&& p, J&& j, Args&&... args)
{
   function_recursion<evaluate_constraint_jacobian,0,libmda::meta::Decay<P>::size()>()
      .apply(p
           , std::forward<J>(j)
           , std::forward<Args>(args)...
           );
}

/////
// evaluate second derivative of N-th constraint
// and multuply it with a lambda and add to D
////
template<size_t N>
struct evaluate_sub_second_deriv
{
   template<class P, class L, class D, class... Args>
   void apply(P&& p, L&& l, D&& d, Args&&... args)
   {
      std::get<N>(p).sub_second_deriv(std::forward<Args>(args)...
                                    , l(N)
                                    , std::forward<D>(d)
                                    );
   }
};

template<class P, class L, class D, class... Args>
void evaluate_sub_second_derivs(P&& p, L&& l, D&& d, Args&&... args)
{
   function_recursion<evaluate_sub_second_deriv,0,libmda::meta::Decay<P>::size()>()
      .apply(p
           , std::forward<L>(l)
           , std::forward<D>(d)
           , std::forward<Args>(args)...
           );
}

} // namespace detail

///////////////
//
// struct for holding constraint functions
//
///////////////
template<class... Ts>
class constraints_pack: public std::tuple<Ts...>
{
   public:
      //////
      // constructor
      //
      /////
      template<class... Us>
      constraints_pack(Us&&... us): 
         std::tuple<Ts...>(std::forward<Us>(us)...)
      {
      }
      
      //////
      // evaluate constraints
      //
      /////
      template<class Arg, class C>
      void operator()(Arg&& arg, C&& c) const
      {
         ::detail::evaluate_constraints(*this,std::forward<C>(c),std::forward<Arg>(arg));
      }
      
      ///////
      // evaluate constraints jacobian
      //
      ///////
      template<class Arg, class J>
      void jacobian(Arg&& arg, J&& j) const
      {
         ::detail::evaluate_constraints_jacobian(*this,std::forward<J>(j),std::forward<Arg>(arg));
      }

      /////
      // evaluate and subtrac second derivs
      //
      /////
      template<class Arg, class L, class D>
      void subtract_second_derivs(Arg&& arg, L&& l, D&& d) const
      {
         ::detail::evaluate_sub_second_derivs(*this,std::forward<L>(l), std::forward<D>(d), std::forward<Arg>(arg));
      }
      
      ///////
      // get number of constraints
      //
      //////
      static constexpr size_t size() { return sizeof...(Ts); }
};

//////
// return pack of constraints
//
/////
template<class... Ts>
constraints_pack<Ts...> constraints(Ts&&... ts)
{
   return constraints_pack<Ts...>(std::forward<Ts>(ts)...);
}

#endif /* CONSTRAINTS_H_INCLUDED */
