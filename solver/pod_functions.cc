#include "pod_functions.h"
#include<cmath>

double dot(double arg1, double arg2)
{
   return arg1*arg2;
}

double norm(double arg)
{
   return sqrt(arg*arg);
};
