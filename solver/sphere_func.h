#ifndef SPHERE_FUNC_H_INCLUDED
#define SPHERE_FUNC_H_INCLUDED

#include "typedefs.h"
#include "../libmda/util/require.h"

struct sphere_func
{
   // Sphere func: f(X) = \sum_i x_i^2
   //         min: f(0,...,0) = 0   
   //
   
   /*! Evaluation of function
    * 
    */
   template< class T
           , libmda::Require_order<T,1> = 0
           >
   double operator()(T&& arg) const
   {
      double result = 0.0;
      for(int i = 0; i < arg.size(); ++i)
      {
         result += arg.at(i)*arg.at(i);
      }
      return result;
   }
   
   /*! First derivative
    *
    */
   template< class T
           , libmda::Enforce_order<T,1> = 0
           > 
   void first_deriv(T&& arg, T&& der) const
   {
      assert(arg.size() == der.size());
      for(int i = 0; i < arg.size(); ++i)
      {
         der.at(i) = 2.0*arg.at(i);
      }
   }
};

#endif /* SPHERE_FUNC_H_INCLUDED */
