#ifndef CONVERGENCE_H_INCLUDED
#define CONVERGENCE_H_INCLUDED

#include<vector>

#include "../libmda/numeric/float_eq.h"
#include "../libmda/util/require.h"
#include "../libmda/basic_using.h"

bool converged(const double arg, const double acc);

template<class T
       , libmda::Require_order<T,1> = 0 
       >
bool converged(T&& arg, const libmda::Value_type<libmda::Remove_reference<T> > acc)
{
   for(int i=0; i<arg.size(); ++i)
      if( libmda::numeric::float_geq(fabs(arg.at(i)),acc) )
         return false;
   return true;
}

template<class T>
bool converged(const std::vector<T>& arg, const T acc)
{
   for(int i=0; i<arg.size(); ++i)
      if( libmda::numeric::float_geq(fabs(arg[i]),acc) )
         return false;
   return true;
}

#endif /* CONVERGENCE_H_INCLUDED */
