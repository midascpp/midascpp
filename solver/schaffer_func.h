#ifndef SCHAFFER_FUNC_H_INCLUDED
#define SCHAFFER_FUNC_H_INCLUDED

#include <cmath>

#include "typedefs.h"
#include "../libmda/util/require.h"

struct schaffer_func
{
   // Schaffer function
   //
   //
   
   /*! Evaluate function
    *
    */
   template<class T
          , libmda::Require_order<T,1> = 0
          >
   double operator()(T&& arg) const
   {
      assert(arg.size() == 2);
      return 0.5 + (sin(arg.at(0)*arg.at(0) - arg.at(1)*arg.at(1))*sin(arg.at(0)*arg.at(0) - arg.at(1)*arg.at(1)) - 0.5)
                 / ((1.0 + 0.001*(arg.at(0)*arg.at(0) + arg.at(1)*arg.at(1)))*(1.0 + 0.001*(arg.at(0)*arg.at(0) + arg.at(1)*arg.at(1))));
   }
};

#endif /* SCHAFFER_FUNC_H_INCLUDED */
