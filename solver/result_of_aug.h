#ifndef RESULT_OF_AUG_H_INCLUDED
#define RESULT_OF_AUG_H_INCLUDED

template<size_t N, class... Args>
struct get_arg;

template<class Arg>
struct get_arg<1,Arg> { using type = Arg; };

template<class F>
struct result_of_aug;

template<class F, class... Args>
struct result_of_aug<F(Args...)>
{
   using resul_t = typename std::result_of<F(Args...)>::type;
   using argum_t = typename get_arg<sizeof...(Args), Args...>::type;
   using funct_t = F;
};

template<class Sig>
using Resul_t = typename result_of_aug<Sig>::resul_t;
template<class Sig>
using Argum2_t = typename std::remove_reference<typename result_of_aug<Sig>::argum_t>::type;
template<class Sig>
using Funct_t = typename result_of_aug<Sig>::argum_t;


#endif /* RESULT_OF_AUG_H_INCLUDED */
