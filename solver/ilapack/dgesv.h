#ifndef DGESV_H_INCLUDED
#define DGESV_H_INCLUDED

#include<iostream>
#include<memory>
#include"iface.h"
#include"lapack_tag.h"

template<class A, class B>
int DGESV(A& mata, B& matb, ilapack::col_major)
{
   int n    = mata.template extent<1>(); 
   int nrsh = 1; // this should be matb.extent<1>() in case of matrix
   double* a = mata.get();
   int lda  = std::max(1,n);
   std::unique_ptr<int> ipiv_smp(new int(n));
   int* ipiv = ipiv_smp.get(); 
   double* b = matb.get();
   int ldb  = std::max(1,n);
   int info; // output integer
   
   //               n nrsh  a       lda ipiv          b       ldb info
   //ilapack::dgesv_(&n,&nrsh,mata.get(),&lda,ipiv_smp.get(),matb.get(),&ldb,&info);
   ilapack::dgesv(n,nrsh,mata.get(),lda,ipiv_smp.get(),matb.get(),ldb,info);

   if(info)
      std::cout << " dgesv: warning info != 0 " << std::endl;
   
   return info;
}

template<class A, class B>
int DGESV(A& mata, B& matb, ilapack::row_major)
{
   // not implementet
   std::cout << " row major DGESV not implemented " << std::endl;
   assert(false);
}

#endif /* DGESV_H_INCLUDED */
