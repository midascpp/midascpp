#ifndef LAPACK_TAG_H_INCLUDED
#define LAPACK_TAG_H_INCLUDED

namespace ilapack
{

//////
// tags for tag dispatching col/row major matrices to lapack interface routines
// 
//////
struct row_major { };
struct col_major { };

} // namespace ilapack

#endif /* LAPACK_TAG_H_INCLUDED */
