#ifndef IFACE_LAPACK_H_INCLUDED
#define IFACE_LAPACK_H_INCLUDED

namespace ilapack
{

extern "C"
{
////////
// dgesv:
//    solves general system of linear equations AX=B
//
////
void dgesv_(int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb
          , int* info);
}
// C++ style wrapper 
inline void dgesv(int& n, int& nrhs, double* a, int& lda, int* ipiv, double* b, int& ldb
                , int& info)
{
   dgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info);
}

extern "C"
{
////////
// dsgesv:
//    solves general system of linear equations AX=B
//    using iterativement refinement
////
void dsgesv_(int* n, int* nrhs, double* a, int* lda, int* ipiv, double* b, int* ldb
          , double* x, int* ldx, double* work, float* swork, int* iter, int* info);
}

} // namespace ilapack

#endif /* IFACE_LAPACK_H_INCLUDED */
