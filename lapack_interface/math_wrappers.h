#ifndef MATH_WRAPPERS_H_INCLUDED
#define MATH_WRAPPERS_H_INCLUDED

#include<complex>

namespace midas
{
namespace lapack_interface
{

   /***************************************************************************/
   // GEMV
   /***************************************************************************/
   void gemv(char* trans 
           , int* m, int* n 
           , float* alpha
           , float* a, int* lda
           , float* x, int* incx 
           , float* beta
           , float* y, int* incy
           );

   void gemv(char* trans 
           , int* m, int* n 
           , double* alpha
           , double* a, int* lda
           , double* x, int* incx
           , double* beta
           , double* y, int* incy
           );

   void gemv(char* trans 
           , int* m, int* n 
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* x, int* incx
           , std::complex<float>* beta
           , std::complex<float>* y, int* incy
           );

   void gemv(char* trans 
           , int* m, int* n 
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* x, int* incx
           , std::complex<double>* beta
           , std::complex<double>* y, int* incy
           );

   /***************************************************************************/
   // GEMV
   /***************************************************************************/
   void gemv(char* transa
           , int* m, int* n
           , float* alpha
           , float* a, int* lda
           , float* x, int* incx
           , float* beta
           , float* y, int* incy
           );

   void gemv(char* transa
           , int* m, int* n
           , double* alpha
           , double* a, int* lda
           , double* x, int* incx
           , double* beta
           , double* y, int* incy
           );

   void gemv(char* transa
           , int* m, int* n
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* x, int* incx
           , std::complex<float>* beta
           , std::complex<float>* y, int* incy
           );

   void gemv(char* transa
           , int* m, int* n
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* x, int* incx
           , std::complex<double>* beta
           , std::complex<double>* y, int* incy
           );
   /***************************************************************************/
   // GEMM
   /***************************************************************************/
   void gemm(char* transa, char* transb
           , int* m, int* n, int* k
           , float* alpha
           , float* a, int* lda
           , float* b, int* ldb
           , float* beta
           , float* c, int* ldc
           );

   void gemm(char* transa, char* transb
           , int* m, int* n, int* k
           , double* alpha
           , double* a, int* lda
           , double* b, int* ldb
           , double* beta
           , double* c, int* ldc
           );

   void gemm(char* transa, char* transb
           , int* m, int* n, int* k
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* b, int* ldb
           , std::complex<float>* beta
           , std::complex<float>* c, int* ldc
           );

   void gemm(char* transa, char* transb
           , int* m, int* n, int* k
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* b, int* ldb
           , std::complex<double>* beta
           , std::complex<double>* c, int* ldc
           );

   /***************************************************************************/
   // GELSS
   /***************************************************************************/
   void gelss(int* m, int* n, int* nrhs
            , float* a, int* lda
            , float* b, int* ldb, float* s, float* rcond, int* rank
            , float* work, int* lwork, int* info
            );

   void gelss(int* m, int* n, int* nrhs
            , double* a, int* lda
            , double* b, int* ldb, double* s, double* rcond, int* rank
            , double* work, int* lwork, int* info
            );

   // NB: Complex have different interface, but we initialize rwork in gelss in order to preserve the interface.
   void gelss(int* m, int* n, int* nrhs
            , std::complex<float>* a, int* lda
            , std::complex<float>* b, int* ldb, float* s, float* rcond, int* rank
            , std::complex<float>* work, int* lwork, int* info
            );

   void gelss(int* m, int* n, int* nrhs
            , std::complex<double>* a, int* lda
            , std::complex<double>* b, int* ldb, double* s, double* rcond, int* rank
            , std::complex<double>* work, int* lwork, int* info
            );


   /***************************************************************************/
   // GELSD
   /***************************************************************************/
   void gelsd(int* m, int* n, int* nrhs
            , float* a, int* lda
            , float* b, int* ldb, float* s, float* rcond, int* rank
            , float* work, int* lwork, int* iwork, int* info
            );

   void gelsd(int* m, int* n, int* nrhs
            , double* a, int* lda
            , double* b, int* ldb, double* s, double* rcond, int* rank
            , double* work, int* lwork, int* iwork, int* info
            );


   /***************************************************************************/
   // GESVD
   /***************************************************************************/
   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , float* a,  int* lda, float* s
            , float* u,  int* ldu
            , float* vt, int* ldvt
            , float* work, int* lwork, int* info
            );

   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , double* a,  int* lda, double* s
            , double* u,  int* ldu
            , double* vt, int* ldvt
            , double* work, int* lwork, int* info
            );
   
   ////
   // the two below functions are further wrappers as the complex lapack routine has a different interface
   // (it has an extra rwork argument given after lwork)
   ////
   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , std::complex<float>* a,  int* lda, float* s
            , std::complex<float>* u,  int* ldu
            , std::complex<float>* vt, int* ldvt
            , std::complex<float>* work, int* lwork, int* info
            );

   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , std::complex<double>* a,  int* lda , double* s
            , std::complex<double>* u,  int* ldu
            , std::complex<double>* vt, int* ldvt
            , std::complex<double>* work, int* lwork, int* info
            );
   
   /***************************************************************************/
   // GESDD
   /***************************************************************************/
   void gesdd(char* jobz
            , int* m, int* n
            , float* a,  int* lda, float* s
            , float* u,  int* ldu
            , float* vt, int* ldvt
            , float* work, int* lwork, int* iwork, int* info
            );

   void gesdd(char* jobz
            , int* m, int* n
            , double* a,  int* lda, double* s
            , double* u,  int* ldu
            , double* vt, int* ldvt
            , double* work, int* lwork, int* iwork, int* info
            );
   
   ////
   // the two below functions are further wrappers as the complex lapack routine has a different interface
   // (it has an extra rwork argument given after lwork)
   ////
   void gesdd(char* jobz
            , int* m, int* n
            , std::complex<float>* a,  int* lda, float* s
            , std::complex<float>* u,  int* ldu
            , std::complex<float>* vt, int* ldvt
            , std::complex<float>* work, int* lwork, int* iwork, int* info
            );
   
   void gesdd(char* jobz
            , int* m, int* n
            , std::complex<double>* a,  int* lda, double* s
            , std::complex<double>* u,  int* ldu
            , std::complex<double>* vt, int* ldvt
            , std::complex<double>* work, int* lwork, int* iwork, int* info
            );
   
   /***************************************************************************/
   // GEQRF
   /***************************************************************************/
   void geqrf_(int* m, int* n
             , float* a, int* lda
             , float* tau
             , float* work, int* lwork, int* info
             );
   
   void geqrf_(int* m, int* n
             , double* a, int* lda
             , double* tau
             , double* work, int* lwork, int* info
             );
   
   void geqrf_(int* m, int* n
             , std::complex<float>* a, int* lda
             , std::complex<float>* tau
             , std::complex<float>* work, int* lwork, int* info
             );
   
   void geqrf_(int* m, int* n
             , std::complex<double>* a, int* lda
             , std::complex<double>* tau
             , std::complex<double>* work, int* lwork, int* info
             );

   /***************************************************************************/
   // GEQP3
   /***************************************************************************/
   void geqp3( int* m, int* n
             , float* a, int* lda
             , int* jpvt, float* tau
             , float* work, int* lwork, int* info
             );

   void geqp3( int* m, int* n
             , double* a, int* lda
             , int* jpvt, double* tau
             , double* work, int* lwork, int* info
             );

   void geqp3( int* m, int* n
             , std::complex<float>* a, int* lda
             , int* jpvt, std::complex<float>* tau
             , std::complex<float>* work, int* lwork, int* info
             );

   void geqp3( int* m, int* n
             , std::complex<double>* a, int* lda
             , int* jpvt, std::complex<double>* tau
             , std::complex<double>* work, int* lwork, int* info
             );

   /***************************************************************************/
   // GETRF_
   /***************************************************************************/
   void getrf_(int* m, int* n
             , float* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf_(int* m, int* n
             , double* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf_(int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf_(int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv, int* info
             );
   
   /***************************************************************************/
   // GETRF
   /***************************************************************************/
   void getrf(int* m, int* n
             , float* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf(int* m, int* n
             , double* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf(int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv, int* info
             );
   
   void getrf(int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv, int* info
             );

   /***************************************************************************/
   // GETRS
   /***************************************************************************/
   void getrs(char* transa, int* m, int* n
             , float* a, int* lda
             , int* ipiv
             , float* b, int* ldb
             , int* info
             );
   
   void getrs(char* transa, int* m, int* n
             , double* a, int* lda
             , int* ipiv
             , double* b, int* ldb
             , int* info
             );
   
   void getrs(char* transa, int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv
             , std::complex<float>* b, int* ldb
             , int* info
             );
   
   void getrs(char* transa, int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv
             , std::complex<double>* b, int* ldb
             , int* info
             );

   /***************************************************************************/
   // GETRI
   /***************************************************************************/
   void getri( int* n
             , float* a, int* lda
             , int* ipiv
             , float* work, int* lwork
             , int* info
             );
   
   void getri( int* n
             , double* a, int* lda
             , int* ipiv
             , double* work, int* lwork
             , int* info
             );
   
   void getri( int* n
             , std::complex<float>* a, int* lda
             , int* ipiv
             , std::complex<float>* work, int* lwork
             , int* info
             );
   
   void getri( int* n
             , std::complex<double>* a, int* lda
             , int* ipiv
             , std::complex<double>* work, int* lwork
             , int* info
             );

   /***************************************************************************/
   // GESV
   /***************************************************************************/
   void gesv_(int* n, int* nrhs
            , float* a, int* lda
            , int* ipiv
            , float* b, int* ldb
            , int* info
            );

   void gesv_(int* n, int* nrhs
            , double* a, int* lda
            , int* ipiv
            , double* b, int* ldb
            , int* info
            );
   
   void gesv_(int* n, int* nrhs
            , std::complex<float>* a, int* lda
            , int* ipiv
            , std::complex<float>* b, int* ldb
            , int* info
            );
   
   void gesv_(int* n, int* nrhs
            , std::complex<double>* a, int* lda
            , int* ipiv
            , std::complex<double>* b, int* ldb
            , int* info
            );
   
   /***************************************************************************/
   // POSV
   /***************************************************************************/
   void posv_(char* uplo, int* n, int* nrhs
            , float* a, int* lda
            , float* b, int* ldb
            , int* info
            ); 
   
   void posv_(char* uplo, int* n, int* nrhs
            , double* a, int* lda
            , double* b, int* ldb
            , int* info
            ); 
   
   void posv_(char* uplo, int* n, int* nrhs
            , std::complex<float>* a, int* lda
            , std::complex<float>* b, int* ldb
            , int* info
            ); 
   
   void posv_(char* uplo, int* n, int* nrhs
            , std::complex<double>* a, int* lda
            , std::complex<double>* b, int* ldb
            , int* info
            ); 

   /***************************************************************************/
   // HESV
   /***************************************************************************/
   void hesv( char* uplo, int* n, int* nrhs
            , std::complex<float>* a, int* lda, int* ipiv
            , std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork
            , int* info
            ); 
   
   void hesv( char* uplo, int* n, int* nrhs
            , std::complex<double>* a, int* lda, int* ipiv
            , std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork
            , int* info
            ); 
   
   /***************************************************************************/
   // SYGV
   /***************************************************************************/
   void sygv_(int* itype, char* jobz
            , char* uplo, int* n
            , float* a, int* lda
            , float* b, int* ldb
            , float* w
            , float* work, int* lwork
            , int* info
            );

   void sygv_(int* itype, char* jobz
            , char* uplo, int* n
            , double* a, int* lda
            , double* b, int* ldb
            , double* w
            , double* work, int* lwork
            , int* info
            );
   //
   // NO COMPLEX VERSIONS FOR SYGV
   //
   
   /***************************************************************************/
   // SYGVD
   /***************************************************************************/
   void sygvd_(int* itype, char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* b, int* ldb
             , float* w
             , float* work, int* lwork, int* iwork
             , int* liwork, int* info
             );

   void sygvd_(int* itype, char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* b, int* ldb
             , double* w
             , double* work, int* lwork, int* iwork
             , int* liwork, int* info
             );
   //
   // NO COMPLEX VERSIONS FOR SYGVD
   //

   /***************************************************************************/
   // SYEVD
   /***************************************************************************/
   void syevd_(char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* w
             , float* work, int* lwork, int* iwork
             , int* liwork, int* info
             );

   void syevd_(char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* w
             , double* work, int* lwork, int* iwork
             , int* liwork, int* info
             );
   //
   // NO COMPLEX VERSIONS FOR SYEVD
   //

   /***************************************************************************/
   // SYEVX
   /***************************************************************************/
   void syevx
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  float* a
      ,  int* lda
      ,  float* vl
      ,  float* vu
      ,  int* il
      ,  int* iu
      ,  float* abstol
      ,  int* m
      ,  float* w
      ,  float* z
      ,  int* ldz
      ,  float* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      );

   void syevx
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  double* a
      ,  int* lda
      ,  double* vl
      ,  double* vu
      ,  int* il
      ,  int* iu
      ,  double* abstol
      ,  int* m
      ,  double* w
      ,  double* z
      ,  int* ldz
      ,  double* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      );
   //
   // NO COMPLEX VERSIONS FOR SYEVX

   /***************************************************************************/
   // HEEV
   /***************************************************************************/
   void heev
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* info
      );

   void heev
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* info
      );

   /***************************************************************************/
   // HEEVD
   /***************************************************************************/
   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );


   /***************************************************************************/
   // HEEVD
   /***************************************************************************/
   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );


   /***************************************************************************/
   // GEEV
   /***************************************************************************/
   void geev_(char* jobvl, char* jobvr, int* n
            , float* a, int* lda
            , float* wr
            , float* wi
            , float* vl, int* ldvl
            , float* vr, int* ldvr
            , float* work, int* lwork, int* info
            );

   void geev_(char* jobvl, char* jobvr, int* n
            , double* a, int* lda
            , double* wr
            , double* wi
            , double* vl, int* ldvl
            , double* vr, int* ldvr
            , double* work, int* lwork, int* info
            );
   
   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void geev_(char* jobvl, char* jobvr, int* n
            , std::complex<float>* a, int* lda
            , std::complex<float>* w
            , std::complex<float>* vl, int* ldvl
            , std::complex<float>* vr, int* ldvr
            , std::complex<float>* work, int* lwork
            , float* rwork, int* info
            );
   
   void geev_(char* jobvl, char* jobvr, int* n
            , std::complex<double>* a, int* lda
            , std::complex<double>* w
            , std::complex<double>* vl, int* ldvl
            , std::complex<double>* vr, int* ldvr
            , std::complex<double>* work, int* lwork
            , double* rwork, int* info
            );

   /***************************************************************************/
   // GGEV
   /***************************************************************************/
   void ggev_(char* jobvl, char* jobvr, int* n
            , float* a, int* lda
            , float* b, int* ldb
            , float* alphar
            , float* alphai
            , float* beta
            , float* vl, int* ldvl
            , float* vr, int* ldvr
            , float* work
            , int* lwork, int* info
            );

   void ggev_(char* jobvl, char* jobvr, int* n
            , double* a, int* lda
            , double* b, int* ldb
            , double* alphar
            , double* alphai
            , double* beta
            , double* vl, int* ldvl
            , double* vr, int* ldvr
            , double* work
            , int* lwork, int* info
            );
   
   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void ggev_(char* jobvl, char* jobvr, int* n
            , std::complex<float>* a, int* lda
            , std::complex<float>* b, int* ldb
            , std::complex<float>* alpha
            , std::complex<float>* beta
            , std::complex<float>* vl, int* ldvl
            , std::complex<float>* vr, int* ldvr
            , std::complex<float>* work
            , int* lwork
            , float* rwork, int* info
            );
   
   void ggev_(char* jobvl, char* jobvr, int* n
            , std::complex<double>* a, int* lda
            , std::complex<double>* b, int* ldb
            , std::complex<double>* alpha
            , std::complex<double>* beta
            , std::complex<double>* vl, int* ldvl
            , std::complex<double>* vr, int* ldvr
            , std::complex<double>* work
            , int* lwork
            , double* rwork, int* info
            );

   /***************************************************************************/
   // SPTRF + SPTRS
   /***************************************************************************/
   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               float* ap,
               float* b, int* ldb,
               int* info);

   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               double* ap,
               double* b, int* ldb,
               int* info);

   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<float>* ap,
               std::complex<float>* b, int* ldb,
               int* info);

   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<double>* ap,
               std::complex<double>* b, int* ldb,
               int* info);

   /***************************************************************************/
   // HPTRF + HPTRS
   /***************************************************************************/
   void hptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<float>* ap,
               std::complex<float>* b, int* ldb,
               int* info);

   void hptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<double>* ap,
               std::complex<double>* b, int* ldb,
               int* info);

   /***************************************************************************/
   // SYMM
   /***************************************************************************/
   void symm(char* side, char* uplo,
               int* m, int* n,
               float* alpha,
               float* a, int* lda,
               float* b, int* ldb,
               float* beta,
               float* c, int* ldc);

   void symm(char* side, char* uplo,
               int* m, int* n,
               double* alpha,
               double* a, int* lda,
               double* b, int* ldb,
               double* beta,
               double* c, int* ldc);

   void symm(char* side, char* uplo,
               int* m, int* n,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* b, int* ldb,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc);

   void symm(char* side, char* uplo,
               int* m, int* n,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* b, int* ldb,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc);

   /***************************************************************************/
   // SYRK
   /***************************************************************************/
   void syrk(char* uplo, char* trans,
               int* n, int* k,
               float* alpha,
               float* a, int* lda,
               float* beta,
               float* c, int* ldc);

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               double* alpha,
               double* a, int* lda,
               double* beta,
               double* c, int* ldc);

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc);

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc);

   /***************************************************************************/
   // SPMV
   /***************************************************************************/
   void spmv(char* uplo,
             int* n,
             float* alpha,
             float* ap,
             float* x, int* incx,
             float* beta,
             float* y, int* incy);

   void spmv(char* uplo,
             int* n, 
             double* alpha,
             double* ap, 
             double* x, int* incx,
             double* beta,
             double* y, int* incy);

   void spmv(char* uplo,
             int* n,
             std::complex<float>* alpha,
             std::complex<float>* ap,
             std::complex<float>* x, int* incx, 
             std::complex<float>* beta,
             std::complex<float>* y, int* incy);

   void spmv(char* uplo,
             int* n,
             std::complex<double>* alpha,
             std::complex<double>* a, 
             std::complex<double>* x, int* incx,
             std::complex<double>* beta,
             std::complex<double>* y, int* incy);

   /***************************************************************************/
   // SFRK
   /***************************************************************************/
   void sfrk(char* transr,
             char* uplo,
             char* trans,
             int* n,
             int* k,
             float* alpha,
             float* a, int* lda,
             float* beta,
             float* c);

   void sfrk(char* transr,
             char* uplo,
             char* trans,
             int* n, 
             int* k,
             double* alpha,
             double* a, int* lda, 
             double* beta,
             double* c);

   /***************************************************************************/
   // TFTTP
   /***************************************************************************/
   void tfttp(char* transr,
              char* uplo,
              int* n,
              float* arf,
              float* ap,
              int* info);

   void tfttp(char* transr,
              char* uplo,
              int* n, 
              double* arf, 
              double* ap,
              int* info);

   /***************************************************************************/
   // AXPY
   /***************************************************************************/
   void axpy(int* n,
             float* alpha,
             float* dx,
             int* incx,
             float* dy,
             int* incy);

   void axpy(int* n,
             double* alpha,
             double* dx,
             int* incx,
             double* dy,
             int* incy);

   void axpy(int* n,
             std::complex<float>* alpha,
             std::complex<float>* dx,
             int* incx,
             std::complex<float>* dy,
             int* incy);

   void axpy(int* n,
             std::complex<double>* alpha,
             std::complex<double>* dx,
             int* incx,
             std::complex<double>* dy,
             int* incy);

   /***************************************************************************/
   // POTRF
   /***************************************************************************/
   void potrf(char* uplo,
              int* n,
              float* a, int* lda,
              int* info);

   void potrf(char* uplo,
              int* n,
              double* a, int* lda,
              int* info);

   void potrf(char* uplo,
              int* n,
              std::complex<float>* a, int* lda,
              int* info);

   void potrf(char* uplo,
              int* n,
              std::complex<double>* a, int* lda,
              int* info);

   /***************************************************************************/
   // POTRF2
   /***************************************************************************/
   void potrf2(char* uplo,
               int* n,
               float* a, int* lda,
               int* info);

   void potrf2(char* uplo,
               int* n,
               double* a, int* lda,
               int* info);

   /***************************************************************************/
   // POTRS
   /***************************************************************************/
   void potrs(char* uplo,
              int* n,
              int* nrhs,
              float* a, int* lda,
              float* b, int* ldb,
              int* info);

   void potrs(char* uplo,
              int* n,
              int* nrhs,
              double* a, int* lda,
              double* b, int* ldb,
              int* info);

   /***************************************************************************/
   // POTRI
   /***************************************************************************/
   void potri(char* uplo,
              int* n,
              float* a, int* lda,
              int* info);

   void potri(char* uplo,
              int* n,
              double* a, int* lda,
              int* info);

   void potri(char* uplo,
              int* n,
              std::complex<float>* a, int* lda,
              int* info);

   void potri(char* uplo,
              int* n,
              std::complex<double>* a, int* lda,
              int* info);

   /***************************************************************************/
   // TRTRI
   /***************************************************************************/
   void trtri(char* uplo,
              char* diag,
              int* n,
              float* a, int* lda,
              int* info);

   void trtri(char* uplo,
              char* diag,
              int* n,
              double* a, int* lda,
              int* info);

   void trtri(char* uplo,
              char* diag,
              int* n,
              std::complex<float>* a, int* lda,
              int* info);

   void trtri(char* uplo,
              char* diag,
              int* n,
              std::complex<double>* a, int* lda,
              int* info);

   /***************************************************************************/
   // TRTRS
   /***************************************************************************/
   void trtrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              float* a, int* lda,
              float* b, int* ldb,
              int* info);

   void trtrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              double* a, int* lda,
              double* b, int* ldb,
              int* info);

   /***************************************************************************/
   // TPTRS
   /***************************************************************************/
   void tptrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              float* ap,
              float* b, int* ldb,
              int* info);

   void tptrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              double* ap,
              double* b, int* ldb,
              int* info);

   /***************************************************************************/
   // POTRI
   /***************************************************************************/
   void potri(char* uplo,
              int* n,
              float* a, int* lda,
              int* info);

   void potri(char* uplo,
              int* n,
              double* a, int* lda,
              int* info);

   /***************************************************************************/
   // SYEV
   /***************************************************************************/
   void syev(char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* w
             , float* work, int* lwork
             , int* info
             );

   void syev(char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* w
             , double* work, int* lwork
             , int* info
             );
   //
   // NO COMPLEX VERSIONS FOR SYEV
   //
   /***************************************************************************/
   // DOT
   /***************************************************************************/
   float dot(  int* n
            ,  float* dx
            ,  int* incx
            ,  float* dy
            ,  int* incy
            );

   double dot(  int* n
             ,  double* dx
             ,  int* incx
             ,  double* dy
             ,  int* incy
             );

   /***************************************************************************/
   // COPY
   /***************************************************************************/
   void copy(  int* n
            ,  float* dx
            ,  int* incx
            ,  float* dy
            ,  int* incy
            );

   void copy(  int* n
            ,  double* dx
            ,  int* incx
            ,  double* dy
            ,  int* incy
            );

   /***************************************************************************/
   // SYR
   /***************************************************************************/
   void syr(  char* uplo  
           ,  int* n
           ,  float* alpha
           ,  float* dx
           ,  int* incx
           ,  float* a
           ,  int* lda
           );

   void syr(  char* uplo  
           ,  int* n
           ,  double* alpha
           ,  double* dx
           ,  int* incx
           ,  double* a
           ,  int* lda
           );

   /***************************************************************************/
   // SCAL
   /***************************************************************************/
   void scal(  int* n
            ,  float* da
            ,  float* dx
            ,  int* incx
            );

   void scal(  int* n
            ,  double* da
            ,  double* dx
            ,  int* incx
            );

   /***************************************************************************/
   // SYMV
   /***************************************************************************/
   void symv(char* uplo
           , int* n
           , float* alpha
           , float* a, int* lda
           , float* x, int* incx
           , float* beta
           , float* y, int* incy
           );

   void symv(char* uplo
           , int* n
           , double* alpha
           , double* a, int* lda
           , double* x, int* incx
           , double* beta
           , double* y, int* incy
           );

   void symv(char* uplo
           , int* n
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* x, int* incx
           , std::complex<float>* beta
           , std::complex<float>* y, int* incy
           );

   void symv(char* uplo
           , int* n
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* x, int* incx
           , std::complex<double>* beta
           , std::complex<double>* y, int* incy
           );

   /***************************************************************************/
   // LAIC1
   /***************************************************************************/
   void laic1  (  int* job
               ,  int* j
               ,  float* x
               ,  float* sest
               ,  float* w
               ,  float* gamma
               ,  float* sestpr
               ,  float* s
               ,  float* c
               );

   void laic1  (  int* job
               ,  int* j
               ,  double* x
               ,  double* sest
               ,  double* w
               ,  double* gamma
               ,  double* sestpr
               ,  double* s
               ,  double* c
               );

   /***************************************************************************/
   // SYTRF
   /***************************************************************************/
   void sytrf(char* uplo,
              int* n,
              float* a, int* lda,
              int* ipiv, 
              float* work, int* lwork,
              int* info);

   void sytrf(char* uplo,
              int* n,
              double* a, int* lda,
              int* ipiv, 
              double* work, int* lwork,
              int* info);

   /***************************************************************************/
   // SYTRI
   /***************************************************************************/
   void sytri(char* uplo,
              int* n,
              float* a, int* lda,
              int* ipiv, float* work,
              int* info);

   void sytri(char* uplo,
              int* n,
              double* a, int* lda,
              int* ipiv, double* work,
              int* info);

   /***************************************************************************/
   // SYTRS
   /***************************************************************************/
   void sytrs(char* uplo,
              int* n,
              int* nrhs,
              float* a, int* lda,
              int* ipiv,
              float* b, int* ldb,
              int* info);

   void sytrs(char* uplo,
              int* n,
              int* nrhs,
              double* a, int* lda,
              int* ipiv,
              double* b, int* ldb,
              int* info);

   /***************************************************************************/
   // ORMQR
   /***************************************************************************/
   void ormqr(char* side, char* trans,
              int* m, int* n, int* k,
              float* a, int* lda, float* tau,
              float* c, int* ldc,
              float* work, int* lwork,
              int* info);

   void ormqr(char* side, char* trans,
              int* m, int* n, int* k,
              double* a, int* lda, double* tau,
              double* c, int* ldc,
              double* work, int* lwork,
              int* info);
   
} /* namespace lapack_interface */
} /* namespace midas */

#endif /* MATH_WRAPPERS_H_INCLUDED */
