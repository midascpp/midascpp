#ifndef ILAPACK_H_INCLUDED
#define ILAPACK_H_INCLUDED

#include "libmda/util/static_value.h"
#include "libmda/util/static_type.h"

namespace ilapack
{
using libmda::util::static_value;
using libmda::util::static_type;

/**
 * matrix switches
 **/
enum lapack_matrix_t { ROW_MAJOR, COL_MAJOR, MAT_SYM, MAT_ANTISYM, MAT_GEN };

typedef static_value<lapack_matrix_t,ROW_MAJOR> (*row_major_t) ();
typedef static_value<lapack_matrix_t,COL_MAJOR> (*col_major_t) ();
typedef static_value<lapack_matrix_t,MAT_SYM> (*mat_sym_t) ();
typedef static_value<lapack_matrix_t,MAT_ANTISYM> (*mat_antisym_t) ();
typedef static_value<lapack_matrix_t,MAT_GEN> (*mat_gen_t) ();

inline static_value<lapack_matrix_t,ROW_MAJOR> row_major() { return 0; }
inline static_value<lapack_matrix_t,COL_MAJOR> col_major() { return 0; }
inline static_value<lapack_matrix_t,MAT_SYM> mat_sym() { return 0; }
inline static_value<lapack_matrix_t,MAT_ANTISYM> mat_antisym() { return 0; }
inline static_value<lapack_matrix_t,MAT_GEN> mat_gen() { return 0; }

/**
 * eigenvec switches
 **/
enum class lapack_eigenvector_t { NORMALIZE, NORMALIZE_RHS, NORMALIZE_LHS
                                , BI_NORMALIZE, BI_NORMALIZE_RHS, BI_NORMALIZE_LHS 
                                };

typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::NORMALIZE> (*normalize_t) ();
typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::NORMALIZE_RHS> (*normalize_rhs_t) ();
typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::NORMALIZE_LHS> (*normalize_lhs_t) ();
typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::BI_NORMALIZE> (*binormalize_t) ();
typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::BI_NORMALIZE_RHS> (*binormalize_rhs_t) ();
typedef static_value<lapack_eigenvector_t,lapack_eigenvector_t::BI_NORMALIZE_LHS> (*binormalize_lhs_t) ();

inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::NORMALIZE> normalize() { return 0; }
inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::NORMALIZE_RHS> normalize_rhs() { return 0; }
inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::NORMALIZE_LHS> normalize_lhs() { return 0; }
inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::BI_NORMALIZE> binormalize() { return 0; }
inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::BI_NORMALIZE_RHS> binormalize_rhs() { return 0; }
inline static_value<lapack_eigenvector_t, lapack_eigenvector_t::BI_NORMALIZE_LHS> binormalize_lhs() { return 0; }

/**
 * lapack algorithm switches
 **/
enum lapack_algorithm_t { ALGO_DSYGVD, ALGO_DSYEVD, ALGO_DGEEV, ALGO_DGGEV };

typedef static_value<lapack_algorithm_t,ALGO_DSYGVD> (*iDSYGVD_t) ();
typedef static_value<lapack_algorithm_t,ALGO_DSYGVD> (*iDSYEVD_t) ();
typedef static_value<lapack_algorithm_t,ALGO_DGEEV> (*iDGEEV_t) ();
typedef static_value<lapack_algorithm_t,ALGO_DGGEV> (*iDGGEV_t) ();

inline static_value<lapack_algorithm_t,ALGO_DSYGVD> iDSYGVD() { return 0; }
inline static_value<lapack_algorithm_t,ALGO_DSYGVD> iDSYEVD() { return 0; }
inline static_value<lapack_algorithm_t,ALGO_DGEEV> iDGEEV() { return 0; }
inline static_value<lapack_algorithm_t,ALGO_DGGEV> iDGGEV() { return 0; }

} /* namespace ilapack */


#endif /* ILAPACK_H_INCLUDED */
