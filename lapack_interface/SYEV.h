#ifndef SYEV_H_INCLUDED
#define SYEV_H_INCLUDED

#include <memory> // for std::unique_ptr

#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"

/**
 * SYEV
 **/
// SYEV: Divide-and-conquer
// computes eigenvalues for:
//    A*x=lambda*x
// where A is an N*N real symmetric matrix saved in column major format!
// define either userfriendly or efficient (not both :O ).
#define USERFRIENDLY_SYEV
//#define EFFICIENT_SYEV
template<class T>
Eigenvalue_struct<T> SYEV
   (  T* arMatrix, int aN
   ,  char aJobz = 'V'
   ,  char aUplo = 'U'
   )
{
   char jobz = aJobz;  // a little extra work but adds some clarity...
   char uplo = aUplo;
   int  lda  = max(1,aN);
   
   std::unique_ptr<T[]> w(new T[aN]);
   int lwork = max(10000, 3 * aN - 1); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work(new T[lwork]);
   int info;
   
   //cout << " Doing SYEV " << endl;
   midas::lapack_interface::syev(&jobz, &uplo, &aN, arMatrix, &lda, w.get(), work.get(), &lwork, &info);
   if(info != 0)
   {
      MidasWarning("SYEV INFO=" + std::to_string(info));
   }
      
   // Create eigenvalue struct to return
   Eigenvalue_struct<T> eigen;
   eigen._info = info;
   eigen._n = aN;
   eigen._num_eigval = aN;
   eigen._eigenvalues = w.release();
   eigen._eigenvectors = new T*[aN];
#ifdef USERFRIENDLY_SYEV // userfriendly but less efficient
   T* p = new T[aN*aN];
   for(int i=0; i<aN*aN; ++i)
      p[i] = arMatrix[i];
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = p + i*aN;
#endif /* USERFRIENDLY_SYEV */
#ifdef EFFICIENT_SYEV // more efficient but less user friendly as arMatrix is stored in "eigen"
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = arMatrix + i*aN;
   //arMatrix = nullptr;
#endif /* EFFICIENT_SYEV */

   return eigen;
}

/**
 *
 **/
template<class T>
Eigenvalue_struct<T> SYEV(const GeneralMidasMatrix<T>& arMatrix, char aJobz = 'V', char aUplo = 'U')
{
   //assert(Is_symmetric(arMatrix))
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0);
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);

   // Do SYEV
   return SYEV(a.get(),arMatrix.Ncols(),aJobz,aUplo);
}

#endif /* SYEV_H_INCLUDED */
