
/**
 *************************************************************************
 * 
 * @file                packed_matrix.h
 *
 * Created:             27.10.2016
 *
 * Author:              Gunnar Schmitz (Gunnar.Schmitz@chem.au.dk)
 *
 * \brief               Contains low level functions to operate on
 *                      matrices in packed form
 * 
 * Detailed Description: These are mainly translated Fortran routines
 *                       to do for instance matrix multiplications
 *                       with symmetrical packed matrices. 
 *                       (Matrix saved as column major)
 *
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 *************************************************************************
 **/

#include "lapack_interface/packed_matrix.h"
#include <iomanip>

/**
 *  @brief Get a triop of columns out of the symmetric matrix 
 *
 *  Get a strip of columns is..ie out of the symmetric matrix a
 *  of dimension n, a is in (upper) packed form.
 *  This is put on the matrix as in the first (ie-is+1) columns
 *  where a has dimensions (n,nbmax) 
 *
 * 
 * @param as      on ouput columns of the symmetric packed matrix
 * @param a       symmetric packed matrix (n*(n+1)/2)
 * @param is      start column
 * @param ie      end column
 * @param n       dimension of the symmetric packed matrix 
 * @param nbmax   second dimension of as (currently not used...)
 *
 * */
void getstrp
(
   double* as
   , double* a
   , int is
   , int ie
   , int n
   , int nbmax
)
{
   int jj, ioff, j, i, ii;
   int iadr;

   if (is < 0 || ie < is || ie > n)
   {
      throw std::runtime_error("getstrp: is or ie");
   }

   jj = is*(is+1)/2;
   ioff = is;

   for (int j = is; j < ie; ++j)
   {
      for (int i = 0; i <= j; ++i)
      {
         iadr = (j-ioff)*n + i;
         //if (iadr >= n*nbmax) throw std::runtime_error("getstrp: Out of range");
         as[iadr] = a[jj+i];
      }
      jj += j + 1;
   }
   for (int j = is; j < ie; ++j)
   {
      for (int i = j+1; i < ie; ++i)
      {
         iadr = (j-ioff)*n + i;
         //if (iadr >= n*nbmax) throw std::runtime_error("getstrp: Out of range");
         as[iadr] = as[(i-ioff)*n + j];
      }
   }

   ii = ie*(ie+1)/2;
   for (int i = ie; i < n; ++i)
   {
      for (int j = is; j < ie; ++j)
      {
         iadr = (j-ioff)*n + i;
         //if (iadr >= n*nbmax) throw std::runtime_error("getstrp: Out of range");
         as[iadr] = a[ii+j];
      }
      ii += i + 1;
   }

   return;
}



/**
 *  @brief Symmetric packed matrix multiplication 
 *
 *  Performs a matrix matrix multiplication 
 *
 *     A = alpha S x B + beta A    (side = L)
 *  
 *  or
 *
 *     A = alpha B x S + beta A    (side = R)
 *  
 *  where S is a symmetric packed matrix of dimension n 
 *  and B and A are (n x nrow) or (nrow x n) matrices. 
 *
 *  Using BLAS routines which necessitates to get and put
 *  columns of (anti) symmetric matrices on a normal matrix
 * 
 * @param side    specifies if the symmetric matrix is multiplied form the left ('L') or from the right ('R')
 * @param a       Ouput (n x nrow) matrix
 * @param s       Symmetric matrix stored in packed format (n*(n+1)/2)
 * @param b       Input (n x nrow) matrix  
 * @param scr     Scratch array (n x md)
 * @param n       First dimension
 * @param alpha   Scalar
 * @param beta    Scalar
 * @param md      Size of scratch array
 * @param nrow    number of rows
 *
 * */
void smdgm
(
   char side
   , double* a    // (n,md)
   , double* s    // (n*(n+1)/2)
   , double* b    // (n,md)
   , double* scr  // (n,md)
   , int n
   , double* alpha
   , double* beta
   , int md
   , int nrow
)
{
   int iadr = 0;

   char nc = 'T';
   char nt = 'N';
 
   int is = 0;
   int ie = std::min(n,md);
   int nblk = (n+md-1)/md;
   int nrws;

   for (int iblk = 1; iblk <= nblk; ++iblk)
   {
      nrws = ie - is;
      getstrp(scr,s,is,ie,n,nrws);

      if (side == 'L')
      {
         iadr = is; // a(is,1) 
         midas::lapack_interface::gemm( &nc, &nt, &nrws, &nrow, &n, alpha,
                                        scr, &n, b, &n, beta, &a[iadr], &n);
      }
      else if (side == 'R')
      {
         iadr = is * n; // a(1,is) 
         midas::lapack_interface::gemm( &nc, &nt, &nrow, &nrws, &n, alpha,
                                        b, &n, scr, &n, beta, &a[iadr], &n);
      }

      is = ie;
      ie = std::min(ie + md,n);
   }

   return;
}

/**
 *  @brief Packed matrix multiplication 
 *
 *  Performs a matrix matrix multiplication 
 *
 *     x(n,m) = alpha * s(n*(n+1)/2) * b(n,m) + beta * x(n,m)  (side = L)
 * 
 *  or
 *
 *     x(m,n) = alpha * b(m,n) * s(n*(n+1)/2) + beta * x(m,n)  (side = R)
 *
 *  where S is a symmetric packed matrix of dimension n 
 *  and X and B are (n x m) matrices. 
 *  using BLAS routines, which necessitates scratch array scr1(n,md) 
 *  holding columns of symmetric matrices. blocking is fixed at md.
 *
 *
 * @param side    specifies if the symmetric matrix is multiplied form the left ('L') or from the right ('R')
 * @param n       number of rows n
 * @param m       number of columns m
 * @param x       on output n x m matrix X         
 * @param s       symmetric packed matrix (n*(n+1)/2)
 * @param b       matrix c(n,m)
 * @param alpha   scalar
 * @param beta    scalar
 * @param scr1    scratch array 
 * @param nmmax   first dimension of scratch array. For good batching select nmmax = n
 * @param md      second dimension of scratch array to define blocking (e.g. md = 250)
 *
 * */
void matmulpak
(
   char side
   , int n
   , int m
   , double* x
   , double* s
   , double* b
   , double* alpha
   , double* beta
   , double* scr1
   , int nmmax
   , int md
)
{

   int nrws, iadr;

   // init
   for (int i = 0; i < nmmax*md; ++i)
   {
      scr1[i] = 0.0;
   }

   int is = 0;
   int ie, nblk;
   bool transposed = false;

   ie = std::min(m,md);
   nblk = (m+md-1) / md;

   if ( side == 'L')
   {
      transposed = false;
   }
   else
   {
      transposed = true;
   }

   for (int iblk = 1; iblk <= nblk; ++iblk)
   {
      nrws = ie - is;

      if (transposed) 
      {
         iadr = is;
      }
      else
      {
         iadr = is * n;
      }

      smdgm(side,&x[iadr],s,&b[is*n],scr1,n,alpha,beta,md,nrws);

      is = ie;
      ie = std::min(ie+md,m);
   }

   return;
}

void matprt
(
   double* vec
   , int ndim1
   , int ndim2   
)
{
   int maxcol, mincol, i, j;
   maxcol = 0;

   while (maxcol < ndim2)
   {
      mincol = maxcol + 1;
      maxcol = std::min(maxcol+5,ndim2);
      //write(6,'(/7x,5(5x,i3,6x))') (j,j=mincol,maxcol)
      std::cout << std::endl << "    ";
      for (int j = mincol - 1; j < maxcol; ++j)
      {
         std::cout << "     " << j << "      ";
      }
      std::cout << std::endl;
   
      std::cout << std::endl;
      for (int i = 0; i < ndim1; ++i)
      {
         //write(6,'(i5,1x,5(1x,f13.8))') i,(vec(i,j),j=mincol,maxcol)
         std::cout << std::noshowpos << i << " ";
         for (int j = mincol - 1; j < maxcol; ++j)
         {
            std::cout << std::internal << std::showpos << std::setprecision(8)  << std::fixed << " " << vec[j*ndim1 + i];
         }
         std::cout << std::noshowpos << std::endl;
      }
   }

   return;
}

void symmatprt
(
   double* d
   , int n
)
{
   double dd[10];
   int max, imax, imin, j, k, i, ii, jj, ij;
   max = 5;
   imax = 0;
   
   do
   {
      imin = imax + 1;
      imax = std::min(imax+max,n);
      //write(istdout,'(/5x,5(6x,i3,6x),/)') (i,i=imin,imax)
      std::cout << std::endl << "    ";
      for (int i = imin - 1; i < imax; ++i)
      {
         std::cout << "     " << i << "      ";
      }
      std::cout << std::endl;
      std::cout << std::endl;
      for (int j = 0; j < n; ++j)
      {
         k = 0;
         for (int i = imin - 1; i < imax; ++i)
         {
            ii = std::max(i, j);
            jj = std::min(i, j);
            ij = (ii*(ii+1))/2 + jj;
            dd[k] = d[ij];
            k = k + 1;
         }
         //write(istdout,'(i3,1x,5(1x,f13.8))') j,(dd(i),i=1,k)
         std::cout << std::noshowpos << j << " ";
         for (int i = 0; i < k; ++i)
         {
            std::cout << std::internal << std::showpos << std::setprecision(8)  << std::fixed << " " << dd[i];
         }
         std::cout << std::endl;
      }
   }
   while (imax > n ); 
   std::cout << std::noshowpos << std::endl;

   return;
}

