#ifndef SVD_STRUCT_H_INCLUDED
#define SVD_STRUCT_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <algorithm>
#include <complex>

#include "mmv/MidasMatrix.h"
#include "util/type_traits/Complex.h"

/**
 * SINGULAR VALUES DECOMPOSITIONS
 **/
// Structure for holding singular value decompsitions
template<class T> struct SVD_struct
{
   using a_type = T;
   using s_type = midas::type_traits::RealTypeT<T>;
   using u_type = T;
   using vt_type = T;

   int info = -1;
   int m = 0, n = 0;
   std::unique_ptr<s_type []> s;
   std::unique_ptr<vt_type[]> vt; // right singular vectors
   std::unique_ptr<u_type []> u; // left singular vectors
   
   inline int Min() const
   { 
      return (min(m,n)); 
   }

   T** Pseudo()
   {
      T* p = new T[m*n];
      T** pseudo = new T*[m];
      for(int i=0; i<m; ++i)
         pseudo[i] = p + i*n;
      
      for(int i=0; i<m; ++i)
         for(int j=0; j<n; ++j)
         {   
            pseudo[i][j] = 0;
            for(int k=0; k<Min(); ++k)
               if(fabs(s[k]) >= 1e-15*s[0])
                  pseudo[i][j] += vt[j][k]*u[k][i]/T(s[k]);
         }

      return pseudo;
   }

   void Pseudo(T** aPseudo)
   {
      for(int i=0; i<m; ++i)
         for(int j=0; j<n; ++j)
         {   
            aPseudo[i][j] = 0;
            for(int k=0; k<Min(); ++k)
               if(fabs(s[k]) >= 1e-15*s[0])
                  aPseudo[i][j] += vt[j][k]*u[k][i]/T(s[k]);
         }
   }

   T** A()
   {
      T* p = new T[m*n];
      T** a = new T*[n];
      for(int i=0; i<n; ++i)
      {
         a[i] = p + i*m;
      }

      for(int i=0; i<n; ++i)
      {
         for(int j=0; j<m; ++j)
         {
            a[i][j] = 0;
            for(int k=0; k<Min(); ++k)
               a[i][j] += u[k][j]*T(s[k])*vt[i][k];
         }
      }
   
      return a;
   }
};

/**
 * Calculate the pseudo-inverse and save into MidasMatrix.
 * 
 * For matrix A, the pseudo inverse is calculated as:
 *    
 * \f[
 *    A^{+} = V \Sigma^{+} U^{*}
 * \f]
 *
 * where \f$ \left[ \Sigma^+ \right]_{ii} = \sigma_{ii}^{-1}\f$.
 *
 * @param aSvd          The SVD to calculate the pseudo inverse with.
 * @param aPseudo       Output matrix. On ouput will hold pseudo inverse.
 * @param aThreshold    Singular value relative threshold. Everything below will be cut.
 **/
template
   <  class T
   ,  class s_type = typename SVD_struct<T>::s_type
   >
void PseudoInverse
   (  const SVD_struct<T>& aSvd
   ,  GeneralMidasMatrix<typename SVD_struct<T>::u_type>& aPseudo
   ,  const s_type aThreshold = s_type(1e-12)
   )
{
   using midas::math::Conj;
   aPseudo.SetNewSize(In(aSvd.n), In(aSvd.m));

   const s_type thresh = aThreshold*aSvd.s[0]; // pre-calc threshold for cutting of singular values

   for(int col=0; col<aSvd.n; ++col)    // loop over N (cols of original matrix)
   {
      for(int row=0; row<aSvd.m; ++row) // loop over M (rows of original matrix)
      {
         size_t u_idx = row;
         size_t vt_idx = aSvd.n*col;
         aPseudo[col][row] = T(0.0);     // pseudo inverse has dimension N-by-M
         for(int contract_idx=0; contract_idx<aSvd.Min(); ++contract_idx)
         {
            if(aSvd.s[contract_idx] > thresh)
            {
               aPseudo[col][row] += Conj(aSvd.vt[vt_idx]*aSvd.u[u_idx])/aSvd.s[contract_idx];
               ++vt_idx;  // update vt index
               u_idx+=aSvd.m; // update u index
            }
         }
      }
   }
}

/**
 * Load U from SVD_struct ta MidasMatrix.
 *
 * @param aSvd     The SVD to load U from.
 * @param aU       Output matrix. On ouput will hold U.
 **/
template
   <  class T
   >
void LoadU
   (  const SVD_struct<T>& aSvd
   ,  GeneralMidasMatrix<typename SVD_struct<T>::u_type>& aU
   )
{
   aU.SetNewSize(In(aSvd.m), In(aSvd.m));
   size_t counter = 0;
   for(In j=I_0; j<aU.Ncols(); ++j)
   {
      for(In i=I_0; i<aU.Nrows(); ++i)
      {
         aU[i][j] = aSvd.u[counter++];
      }
   }
}

/**
 * Load Vt from SVD_struct to MidasMatrix.
 *
 * @param aSvd      The SVD to load Vt from.
 * @param aVt       Output matrix. On ouput will hold Vt.
 **/
template
   <  class T
   >
void LoadVt
   (  const SVD_struct<T>& aSvd
   ,  GeneralMidasMatrix<typename SVD_struct<T>::vt_type>& aVt
   )
{
   aVt.SetNewSize(In(aSvd.n), In(aSvd.n));
   size_t counter = 0;
   for(In j=I_0; j<aVt.Ncols(); ++j)
   {
      for(In i=I_0; i<aVt.Nrows(); ++i)
      {
         aVt[i][j] = aSvd.vt[counter++];
      }
   }
}

/**
 * Load S from SVD_struct to MidasMatrix.
 *
 * @param aSvd     The SVD to load S from.
 * @param aS       Output matrix. On ouput will hold S.
 **/
template
   <  class T
   >
void LoadS
   (  const SVD_struct<T>& aSvd
   ,  GeneralMidasVector<typename SVD_struct<T>::s_type>& aS
   )
{
   aS.SetNewSize(In(aSvd.Min()));
   for(In i=I_0; i<aS.Size(); ++i)
   {
      aS[i] = aSvd.s[i];
   }
}

/**
 * Load A from SVD_struct to MidasMatrix.
 *
 * @param aSvd     The SVD to load A from.
 * @param aA       Output matrix. On ouput will hold A.
 **/
template
   <  class T
   >
void LoadA
   (  const SVD_struct<T>& aSvd
   ,  GeneralMidasMatrix<typename SVD_struct<T>::a_type>& aA
   )
{
   aA.SetNewSize(In(aSvd.m), In(aSvd.n));
   for(In j=I_0; j<aA.Ncols(); ++j)
   {
      for(In i=I_0; i<aA.Nrows(); ++i)
      {
         size_t u_idx = i;
         size_t vt_idx = aSvd.n*j;
         aA[i][j] = 0;
         for(In k = I_0; k<aSvd.Min(); ++k)
         {
            aA[i][j] += aSvd.u[u_idx]*aSvd.s[k]*aSvd.vt[vt_idx];
            ++vt_idx;      // update vt index
            u_idx+=aSvd.m; // update u index
         }
      }
   }
}

/**
 * Find rank of svd.
 *
 * @param aSvd          The SVD to find rank from.
 * @param aThreshold    Singular value relative threshold. Everything below will be cut.
 * @return              Will return the rank of the SVD.
 **/
template
   <  class T
   >
size_t Rank
   (  const SVD_struct<T>& aSvd
   ,  const typename SVD_struct<T>::s_type aThreshold = 1e-12
   )
{
   using s_type = typename SVD_struct<T>::s_type;
   size_t nrank = 0; // init nrank
   const s_type thresh = aThreshold*aSvd.s[0];
   size_t min_mn = aSvd.Min();
   for(size_t s_idx = 0; s_idx < min_mn; ++s_idx) // loop over elements of s
   {
      if(aSvd.s[s_idx] > thresh)                      // if value is above threshold we add 1 to rank
         ++nrank;
   }
   return nrank;     // return rank
}

/**
 * Find rank necessary to obtain ||A-A*|| < thresh
 *
 * @param aSvd       The SVD
 * @param aThresh    The accuracy threshold
 * @return           The rank
 **/
template
   <  class T
   >
size_t RankAcc
   (  const SVD_struct<T>& aSvd
   ,  const typename SVD_struct<T>::s_type aThresh = 1.e-12
   )
{
   using s_type = typename SVD_struct<T>::s_type;
   assert(aThresh >= s_type(0.));

   const size_t min_mn = aSvd.Min();

   // Initial values
   int rank = min_mn;
   s_type err_sq = s_type(0.);

   // Error is the sum of squared left-out singular values
   for(int i=min_mn-1; i>=0; --i)
   {
      err_sq += aSvd.s[i]*aSvd.s[i];

      // Break, if error becomes too large
      if (  std::sqrt(err_sq) > aThresh
         )
      {
         break;
      }
      // Else, decrease rank
      else
      {
         --rank;
      }
   }

   return size_t(rank);
}

/**
 * Find nullity of svd.
 *
 * @param aSvd          The SVD to find nullity from.
 * @param aThreshold    Singular value relative threshold. Everything below will be cut.
 * @return              Will return the nullity of the SVD.
 **/
template
   <  class T
   >
size_t Nullity
   (  const SVD_struct<T>& aSvd
   ,  const typename SVD_struct<T>::s_type aThreshold = 1e-12
   )
{
   using s_type = typename SVD_struct<T>::s_type;
   size_t nnullity = 0;
   const s_type thresh = aThreshold*aSvd.s[0];
   size_t min_mn = aSvd.Min();
   for(size_t s_idx = 0; s_idx < min_mn; ++s_idx)
   {
      if(aSvd.s[s_idx] <= thresh)
         ++nnullity;
   }
   return nnullity;
}

/**
 * Find "absolute" rank of svd.
 *
 * @param aSvd          The SVD to find rank from.
 * @param aThreshold    Singular value absolute threshold. Everything below will be cut.
 * @return              Will return the rank of the SVD.
 **/
template
   <  class T
   >
size_t RankAbs
   (  const SVD_struct<T>& aSvd
   ,  const typename SVD_struct<T>::s_type aThreshold = 1e-12
   )
{
   using s_type = typename SVD_struct<T>::s_type;
   size_t nrank = 0; // init nrank
   const s_type thresh = aThreshold;
   size_t min_mn = aSvd.Min();
   for(size_t s_idx = 0; s_idx < min_mn; ++s_idx) // loop over elements of s
   {
      if(aSvd.s[s_idx] > thresh)                      // if value is above threshold we add 1 to rank
         ++nrank;
   }
   return nrank;     // return rank
}

#endif /* SVD_STRUCT_H_INCLUDED */
