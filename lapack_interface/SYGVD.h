#ifndef SYGVD_H_INCLUDED
#define SYGVD_H_INCLUDED

#include <memory> // for std::unique_ptr

#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/is_complex.h"

/**
 * SYGVD (divide-and-conquer)
 **/

// SYGVD: Divide-and-conquer
// computes eigenvalues for:
//    itype = 1: A*x=(lambda)*B*x
//          = 2: A*B*x=(lambda)*x
//          = 3: B*A*x=(lambda)*x
// where A and B are N*N real symmetric definite matrices saved in column major format!
// define either userfriendly or efficient (not both :O ).
// This is faster than SYGV for large matrices but uses more space !
#define USERFRIENDLY_DSYGVD
//#define EFFICIENT_DSYGVD
template<class T>
Eigenvalue_struct<T> SYGVD(T* arMatrix
                         , T* brMatrix
                         , int aN
                         , char aJobz = 'V'
                         , int aItype = 1
                         , char aUplo = 'U'
                         )
{
   // assert that T is not complex
   static_assert(!is_complex<T>::value,"SYGVD does not work for complex numbers");
   
   // 
   int itype = aItype; // a little extra work but adds some clarity... (probably not :P )
   char jobz = aJobz;  // a little extra work but adds some clarity...
   char uplo = aUplo;  // a little extra work but adds some clarity...
   int lda = max(1,aN);
   int ldb = max(1,aN);
   std::unique_ptr<T[]> w(new T[aN]);
   int lwork = max(10000,1+6*aN+2*aN*aN); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work(new T[lwork]);
   int liwork = max(10000,3+5*aN);
   std::unique_ptr<int[]> iwork(new int[liwork]);
   int info;
   
   //cout << " Doing DSYGVD " << endl;
   midas::lapack_interface::sygvd_(&itype, &jobz, &uplo, &aN, arMatrix, &lda, brMatrix, &ldb, w.get(), work.get(), &lwork, iwork.get(), &liwork, &info);
   if(info != 0)
   {
      MidasWarning("SYGVD INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   Eigenvalue_struct<T> eigen;
   eigen._info = info;
   eigen._n = aN;
   eigen._num_eigval = aN;
   eigen._eigenvalues = w.release();
   eigen._eigenvectors = new T*[aN];
#ifdef USERFRIENDLY_DSYGVD // userfriendly but less efficient
   T* p = new T[aN*aN];
   for(int i=0; i<aN*aN; ++i)
      p[i] = arMatrix[i];
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = p + i*aN;
#endif /* USERFRIENDLY_DSYGVD */
#ifdef EFFICIENT_DSYGVD // more efficient but less user friendly as arMatrix is stored in "eigen"
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = arMatrix + i*aN;
#endif /* EFFICIENT_DSYGVD */

   return eigen;
}

// overload of DSYGVD for MidasMatrix
// currently this will not destroy the matrix... but a future version might !
template<class T>
Eigenvalue_struct<T> SYGVD(const GeneralMidasMatrix<T>& arMatrix
                         , const GeneralMidasMatrix<T>& brMatrix
                         , char aJobz = 'V'
                         , int aItype = 1
                         , char aUplo = 'U'
                         )
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   // Do DSYGVD
   return SYGVD(a.get(), b.get(), arMatrix.Ncols(), aJobz, aItype, aUplo);
}

#endif /* SYGVD_H_INCLUDED */
