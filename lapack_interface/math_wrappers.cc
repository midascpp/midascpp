#include "lapack_interface/math_wrappers.h"
#include "util/Error.h"

#include <complex>
#include <memory>
#include <iostream>
#include <algorithm> // for std::min

extern "C"
{
   ///////////////////////////////////////////////////////////////////////////////7
   // gemv
   ///////////////////////////////////////////////////////////////////////////////7
   void sgemv_(char* transa
           , int* m, int* n
           , float* alpha
           , float* a, int* lda
           , float* x, int* incx
           , float* beta
           , float* y, int* incy
           );

   void dgemv_(char* transa
           , int* m, int* n
           , double* alpha
           , double* a, int* lda
           , double* x, int* incx
           , double* beta
           , double* y, int* incy
           );

   void cgemv_(char* transa
           , int* m, int* n
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* x, int* incx
           , std::complex<float>* beta
           , std::complex<float>* y, int* incy
           );

   void zgemv_(char* transa
           , int* m, int* n
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* x, int* incx
           , std::complex<double>* beta
           , std::complex<double>* y, int* incy
           );

   ///////////////////////////////////////////////////////////////////////////////7
   // gemm
   ///////////////////////////////////////////////////////////////////////////////7
   void sgemm_(char* transa, char* transb,
               int* m, int* n, int* k,
               float* alpha,
               float* a, int* lda,
               float* b, int* ldb,
               float* beta,
               float* c, int* ldc);
   void dgemm_(char* transa, char* transb,
               int* m, int* n, int* k,
               double* alpha,
               double* a, int* lda,
               double* b, int* ldb,
               double* beta,
               double* c, int* ldc);
   void cgemm_(char* transa, char* transb,
               int* m, int* n, int* k,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* b, int* ldb,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc);
   void zgemm_(char* transa, char* transb,
               int* m, int* n, int* k,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* b, int* ldb,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc);
   
   ///////////////////////////////////////////////////////////////////////////////7
   // gelss
   ///////////////////////////////////////////////////////////////////////////////7
   void sgelss_(int* m, int* n, int* nrhs,
                float* a, int* lda,
                float* b, int* ldb,
                float* s, float* rcond, int* rank,
                float* work, int* lwork, int* info);
   void dgelss_(int* m, int* n, int* nrhs,
                double* a, int* lda,
                double* b, int* ldb,
                double* s, double* rcond, int* rank,
                double* work, int* lwork, int* info);
   void cgelss_(int* m, int* n, int* nrhs,
                std::complex<float>* a, int* lda,
                std::complex<float>* b, int* ldb,
                float* s, float* rcond, int* rank,
                std::complex<float>* work, int* lwork, std::complex<float>* rwork, int* info);
   void zgelss_(int* m, int* n, int* nrhs,
                std::complex<double>* a, int* lda,
                std::complex<double>* b, int* ldb,
                double* s, double* rcond, int* rank,
                std::complex<double>* work, int* lwork, std::complex<double>* rwork, int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // gelsd
   ///////////////////////////////////////////////////////////////////////////////7
   void sgelsd_(int* m, int* n, int* nrhs,
                float* a, int* lda,
                float* b, int* ldb,
                float* s, float* rcond, int* rank,
                float* work, int* lwork, int* iwork, int* info);
   void dgelsd_(int* m, int* n, int* nrhs,
                double* a, int* lda,
                double* b, int* ldb,
                double* s, double* rcond, int* rank,
                double* work, int* lwork, int* iwork, int* info);
//   void cgelsd_(int* m, int* n, int* nrhs,
//                std::complex<float>* a, int* lda,
//                std::complex<float>* b, int* ldb,
//                std::complex<float>* s, std::complex<float>* rcond, int* rank,
//                std::complex<float>* work, int* lwork, std::complex<float>* rwork, int* iwork, int* info);
//   void zgelsd_(int* m, int* n, int* nrhs,
//                std::complex<double>* a, int* lda,
//                std::complex<double>* b, int* ldb,
//                std::complex<double>* s, std::complex<double>* rcond, int* rank,
//                std::complex<double>* work, int* lwork, std::complex<double>* rwork, int* iwork, int* info);
   
   ///////////////////////////////////////////////////////////////////////////////7
   // gesvd
   ///////////////////////////////////////////////////////////////////////////////7
   void sgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , float* a,  int* lda, float* s
              , float* u,  int* ldu
              , float* vt, int* ldvt
              , float* work, int* lwork, int* info
              );

   void dgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , double* a,  int* lda, double* s
              , double* u,  int* ldu
              , double* vt, int* ldvt
              , double* work, int* lwork, int* info
              );

   void cgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , std::complex<float>* a,  int* lda, float* s
              , std::complex<float>* u,  int* ldu
              , std::complex<float>* vt, int* ldvt
              , std::complex<float>* work, int* lwork
              , float* rwork, int* info
              );

   void zgesvd_(char* jobu, char* jobvt
              , int* m, int* n
              , std::complex<double>* a,  int* lda, double* s
              , std::complex<double>* u,  int* ldu
              , std::complex<double>* vt, int* ldvt
              , std::complex<double>* work, int* lwork
              , double* rwork, int* info
              );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // gesdd
   ///////////////////////////////////////////////////////////////////////////////7
   void sgesdd_(char* jobz
              , int* m, int* n
              , float* a,  int* lda, float* s
              , float* u,  int* ldu
              , float* vt, int* ldvt
              , float* work, int* lwork, int* iwork, int* info
              );

   void dgesdd_(char* jobz
              , int* m, int* n
              , double* a,  int* lda, double* s
              , double* u,  int* ldu
              , double* vt, int* ldvt
              , double* work, int* lwork, int* iwork, int* info
              );
   
   void cgesdd_(char* jobz
              , int* m, int* n
              , std::complex<float>* a,  int* lda, float* s
              , std::complex<float>* u,  int* ldu
              , std::complex<float>* vt, int* ldvt
              , std::complex<float>* work, int* lwork, float* rwork
              , int* iwork, int* info
              );
   
   void zgesdd_(char* jobz
              , int* m, int* n
              , std::complex<double>* a,  int* lda, double* s
              , std::complex<double>* u,  int* ldu
              , std::complex<double>* vt, int* ldvt
              , std::complex<double>* work, int* lwork, double* rwork
              , int* iwork, int* info
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // geqrf
   ///////////////////////////////////////////////////////////////////////////////7
   void sgeqrf_(int* m, int* n
              , float* a, int* lda
              , float* tau
              , float* work, int* lword, int* info
              );
   
   void dgeqrf_(int* m, int* n
              , double* a, int* lda
              , double* tau
              , double* work, int* lword, int* info
              );
   
   void cgeqrf_(int* m, int* n
              , std::complex<float>* a, int* lda
              , std::complex<float>* tau
              , std::complex<float>* work, int* lword, int* info
              );
   
   void zgeqrf_(int* m, int* n
              , std::complex<double>* a, int* lda
              , std::complex<double>* tau
              , std::complex<double>* work, int* lword, int* info
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // geqp3
   ///////////////////////////////////////////////////////////////////////////////7
   void sgeqp3_(int* m, int* n
              , float* a, int* lda, int* jpvt
              , float* tau
              , float* work, int* lwork, int* info
              );
   
   void dgeqp3_(int* m, int* n
              , double* a, int* lda, int* jpvt
              , double* tau
              , double* work, int* lwork, int* info
              );
   
   void cgeqp3_(int* m, int* n
              , std::complex<float>* a, int* lda, int* jpvt
              , std::complex<float>* tau
              , std::complex<float>* work, int* lwork, int* info
              );
   
   void zgeqp3_(int* m, int* n
              , std::complex<double>* a, int* lda, int* jpvt
              , std::complex<double>* tau
              , std::complex<double>* work, int* lwork, int* info
              );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // getrf
   ///////////////////////////////////////////////////////////////////////////////7
   void sgetrf_(int* m, int* n
              , float* a, int* lda
              , int* ipiv, int* info
              );
   
   void dgetrf_(int* m, int* n
              , double* a, int* lda
              , int* ipiv, int* info
              );
   
   void cgetrf_(int* m, int* n
              , std::complex<float>* a, int* lda
              , int* ipiv, int* info
              );
   
   void zgetrf_(int* m, int* n
              , std::complex<double>* a, int* lda
              , int* ipiv, int* info
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // getrs
   ///////////////////////////////////////////////////////////////////////////////7

   void sgetrs_(char* transa, int* m, int* n
              , float* a, int* lda
              , int* ipiv
              , float* b, int* ldb 
              , int* info
              );
   
   void dgetrs_(char* transa, int* m, int* n
              , double* a, int* lda
              , int* ipiv
              , double* b, int* ldb
              , int* info
              );
   
   void cgetrs_(char* transa, int* m, int* n
              , std::complex<float>* a, int* lda
              , int* ipiv
              , std::complex<float>* b, int* ldb
              , int* info
              );
   
   void zgetrs_(char* transa, int* m, int* n
              , std::complex<double>* a, int* lda
              , int* ipiv
              , std::complex<double>* b, int* ldb
              , int* info 
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // getri
   ///////////////////////////////////////////////////////////////////////////////7

   void sgetri_(int* n
              , float* a, int* lda
              , int* ipiv
              , float* b, int* ldb 
              , int* info
              );
   
   void dgetri_(int* n
              , double* a, int* lda
              , int* ipiv
              , double* b, int* ldb
              , int* info
              );
   
   void cgetri_(int* n
              , std::complex<float>* a, int* lda
              , int* ipiv
              , std::complex<float>* b, int* ldb
              , int* info
              );
   
   void zgetri_(int* n
              , std::complex<double>* a, int* lda
              , int* ipiv
              , std::complex<double>* b, int* ldb
              , int* info 
              );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // gesv
   ///////////////////////////////////////////////////////////////////////////////7
   void sgesv_(int* n, int* nrhs
             , float* a, int* lda
             , int* ipiv
             , float* b, int* ldb
             , int* info
             );
   
   void dgesv_(int* n, int* nrhs
             , double* a, int* lda
             , int* ipiv
             , double* b, int* ldb
             , int* info
             );
   
   void cgesv_(int* n, int* nrhs
             , std::complex<float>* a, int* lda
             , int* ipiv
             , std::complex<float>* b, int* ldb
             , int* info
             );
   
   void zgesv_(int* n, int* nrhs
             , std::complex<double>* a, int* lda
             , int* ipiv
             , std::complex<double>* b, int* ldb
             , int* info
             );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // posv
   ///////////////////////////////////////////////////////////////////////////////7
   void sposv_(char* uplo, int* n, int* nrhs
             , float* a, int* lda
             , float* b, int* ldb
             , int* info
             );

   void dposv_(char* uplo, int* n, int* nrhs
             , double* a, int* lda
             , double* b, int* ldb
             , int* info
             );

   void cposv_(char* uplo, int* n, int* nrhs
             , std::complex<float>* a, int* lda
             , std::complex<float>* b, int* ldb
             , int* info
             );
   
   void zposv_(char* uplo, int* n, int* nrhs
             , std::complex<double>* a, int* lda
             , std::complex<double>* b, int* ldb
             , int* info
             );

   ///////////////////////////////////////////////////////////////////////////////7
   // hesv
   ///////////////////////////////////////////////////////////////////////////////7
   void chesv_(char* uplo, int* n, int* nrhs
             , std::complex<float>* a, int* lda, int* ipiv
             , std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork
             , int* info
             );
   
   void zhesv_(char* uplo, int* n, int* nrhs
             , std::complex<double>* a, int* lda, int* ipiv
             , std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork
             , int* info
             );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // sygv
   ///////////////////////////////////////////////////////////////////////////////7
   void ssygv_(int* itype, char* jobz
             , char* uplo, int* n
             , float* a, int* lda
             , float* b, int* ldb
             , float* w
             , float* work, int* lwork
             , int* info
             );

   void dsygv_(int* itype, char* jobz
             , char* uplo, int* n
             , double* a, int* lda
             , double* b, int* ldb
             , double* w
             , double* work, int* lwork
             , int* info
             );

   ///////////////////////////////////////////////////////////////////////////////7
   // sygvd
   ///////////////////////////////////////////////////////////////////////////////7
   void ssygvd_(int* itype, char* jobz, char* uplo, int* n
              , float* a, int* lda
              , float* b, int* ldb
              , float* w
              , float* work, int* lwork, int* iwork
              , int* liwork, int* info
              );

   void dsygvd_(int* itype, char* jobz, char* uplo, int* n
              , double* a, int* lda
              , double* b, int* ldb
              , double* w
              , double* work, int* lwork, int* iwork
              , int* liwork, int* info
              );
   
   ///////////////////////////////////////////////////////////////////////////////7
   // syevd
   ///////////////////////////////////////////////////////////////////////////////7
   void ssyevd_(char* jobz, char* uplo, int* n
              , float* a, int* lda
              , float* w
              , float* work, int* lwork, int* iwork
              , int* liwork, int* info
              );

   void dsyevd_(char* jobz, char* uplo, int* n
              , double* a, int* lda
              , double* w
              , double* work, int* lwork, int* iwork
              , int* liwork, int* info
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // syevx
   ///////////////////////////////////////////////////////////////////////////////7
   void ssyevx_
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  float* a
      ,  int* lda
      ,  float* vl
      ,  float* vu
      ,  int* il
      ,  int* iu
      ,  float* abstol
      ,  int* m
      ,  float* w
      ,  float* z
      ,  int* ldz
      ,  float* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      );

   void dsyevx_
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  double* a
      ,  int* lda
      ,  double* vl
      ,  double* vu
      ,  int* il
      ,  int* iu
      ,  double* abstol
      ,  int* m
      ,  double* w
      ,  double* z
      ,  int* ldz
      ,  double* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      );
   ///////////////////////////////////////////////////////////////////////////////7
   // heev
   ///////////////////////////////////////////////////////////////////////////////7
   void cheev_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* info
      );

   void zheev_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* info
      );

   
   
   ///////////////////////////////////////////////////////////////////////////////7
   // heevd
   ///////////////////////////////////////////////////////////////////////////////7
   void cheevd_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   void zheevd_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   
   ///////////////////////////////////////////////////////////////////////////////7
   // heevd
   ///////////////////////////////////////////////////////////////////////////////7
   void cheevd_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   void zheevd_
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      );

   
   ///////////////////////////////////////////////////////////////////////////////7
   // geev
   ///////////////////////////////////////////////////////////////////////////////7
   void sgeev_(char* jobvl, char* jobvr, int* n
             , float* a, int* lda
             , float* wr
             , float* wi
             , float* vl, int* ldvl
             , float* vr, int* ldvr
             , float* work, int* lwork, int* info
             );

   void dgeev_(char* jobvl, char* jobvr, int* n
             , double* a, int* lda
             , double* wr
             , double* wi
             , double* vl, int* ldvl
             , double* vr, int* ldvr
             , double* work, int* lwork, int* info
             );
   
   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void cgeev_(char* jobvl, char* jobvr, int* n
             , std::complex<float>* a, int* lda
             , std::complex<float>* w
             , std::complex<float>* vl, int* ldvl
             , std::complex<float>* vr, int* ldvr
             , std::complex<float>* work, int* lwork
             , float* rwork, int* info
             );
   
   void zgeev_(char* jobvl, char* jobvr, int* n
             , std::complex<double>* a, int* lda
             , std::complex<double>* w
             , std::complex<double>* vl, int* ldvl
             , std::complex<double>* vr, int* ldvr
             , std::complex<double>* work, int* lwork
             , double* rwork, int* info
             );

   ///////////////////////////////////////////////////////////////////////////////7
   // ggev
   ///////////////////////////////////////////////////////////////////////////////7
   void sggev_(char* jobvl, char* jobvr, int* n
             , float* a, int* lda
             , float* b, int* ldb
             , float* alphar
             , float* alphai
             , float* beta
             , float* vl, int* ldvl
             , float* vr, int* ldvr
             , float* work
             , int* lwork, int* info
             );

   void dggev_(char* jobvl, char* jobvr, int* n
             , double* a, int* lda
             , double* b, int* ldb
             , double* alphar
             , double* alphai
             , double* beta
             , double* vl, int* ldvl
             , double* vr, int* ldvr
             , double* work
             , int* lwork, int* info
             );

   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void cggev_(char* jobvl, char* jobvr, int* n
             , std::complex<float>* a, int* lda
             , std::complex<float>* b, int* ldb
             , std::complex<float>* alpha
             , std::complex<float>* beta
             , std::complex<float>* vl, int* ldvl
             , std::complex<float>* vr, int* ldvr
             , std::complex<float>* work
             , int* lwork
             , float* rwork, int* info
             );

   void zggev_(char* jobvl, char* jobvr, int* n
             , std::complex<double>* a, int* lda
             , std::complex<double>* b, int* ldb
             , std::complex<double>* alpha
             , std::complex<double>* beta
             , std::complex<double>* vl, int* ldvl
             , std::complex<double>* vr, int* ldvr
             , std::complex<double>* work
             , int* lwork
             , double* rwork, int* info
             );

   ///////////////////////////////////////////////////////////////////////////////7
   // sptrf
   ///////////////////////////////////////////////////////////////////////////////7
   void ssptrf_(char* uplo,
                int* n,
                float* ap,
                int* ipiv,
                int* info);
   void dsptrf_(char* uplo,
                int* n,
                double* ap,
                int* ipiv,
                int* info);
   void csptrf_(char* uplo,
                int* n,
                std::complex<float>* ap,
                int* ipiv,
                int* info);
   void zsptrf_(char* uplo,
                int* n,
                std::complex<double>* ap,
                int* ipiv,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // hptrf
   ///////////////////////////////////////////////////////////////////////////////7
   void chptrf_(char* uplo,
                int* n,
                std::complex<float>* ap,
                int* ipiv,
                int* info);
   void zhptrf_(char* uplo,
                int* n,
                std::complex<double>* ap,
                int* ipiv,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // hptrs
   ///////////////////////////////////////////////////////////////////////////////7
   void chptrs_(char* uplo,
                int* n,
                int* nrhs,
                std::complex<float>* ap,
                int* ipiv,
                std::complex<float>* b, int* ldb,
                int* info);
   void zhptrs_(char* uplo,
                int* n,
                int* nrhs,
                std::complex<double>* ap,
                int* ipiv,
                std::complex<double>* b, int* ldb,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // sptrs
   ///////////////////////////////////////////////////////////////////////////////7
   void ssptrs_(char* uplo,
                int* n,
                int* nrhs,
                float* ap,
                int* ipiv,
                float* b, int* ldb,
                int* info);
   void dsptrs_(char* uplo,
                int* n,
                int* nrhs,
                double* ap,
                int* ipiv,
                double* b, int* ldb,
                int* info);
   void csptrs_(char* uplo,
                int* n,
                int* nrhs,
                std::complex<float>* ap,
                int* ipiv,
                std::complex<float>* b, int* ldb,
                int* info);
   void zsptrs_(char* uplo,
                int* n,
                int* nrhs,
                std::complex<double>* ap,
                int* ipiv,
                std::complex<double>* b, int* ldb,
                int* info);
   ///////////////////////////////////////////////////////////////////////////////7
   // symm 
   ///////////////////////////////////////////////////////////////////////////////7
   void ssymm_(char* side, char* uplo, 
               int* m, int* n, 
               float* alpha,
               float* a, int* lda,
               float* b, int* ldb,
               float* beta,
               float* c, int* ldc);
   void dsymm_(char* side, char* uplo,
               int* m, int* n,
               double* alpha,
               double* a, int* lda,
               double* b, int* ldb,
               double* beta,
               double* c, int* ldc);
   void csymm_(char* side, char* uplo,
               int* m, int* n,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* b, int* ldb,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc);
   void zsymm_(char* side, char* uplo,
               int* m, int* n,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* b, int* ldb,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc);
   ///////////////////////////////////////////////////////////////////////////////7
   // syrk 
   ///////////////////////////////////////////////////////////////////////////////7
   void ssyrk_(char* uplo, char* trans, 
               int* n, int* k, 
               float* alpha,
               float* a, int* lda,
               float* beta,
               float* c, int* ldc);
   void dsyrk_(char* uplo, char* trans,
               int* n, int* k,
               double* alpha,
               double* a, int* lda,
               double* beta,
               double* c, int* ldc);
   void csyrk_(char* uplo, char* trans,
               int* n, int* k,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc);
   void zsyrk_(char* uplo, char* trans,
               int* n, int* k,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc);

   ///////////////////////////////////////////////////////////////////////////////7
   // spmv
   ///////////////////////////////////////////////////////////////////////////////7
   void sspmv_(char* uplo, 
               int* n,  
               float* alpha,
               float* ap,
               float* x, int* incx,
               float* beta,
               float* y, int* incy);
   void dspmv_(char* uplo,
               int* n, 
               double* alpha,
               double* ap, 
               double* x, int* incx,
               double* beta,
               double* y, int* incy);
   void cspmv_(char* uplo,
               int* n, 
               std::complex<float>* alpha,
               std::complex<float>* ap,
               std::complex<float>* x, int* incx,
               std::complex<float>* beta,
               std::complex<float>* y, int* incy);
   void zspmv_(char* uplo, 
               int* n, 
               std::complex<double>* alpha,
               std::complex<double>* ap,
               std::complex<double>* x, int* incx,
               std::complex<double>* beta,
               std::complex<double>* y, int* incy);
   ///////////////////////////////////////////////////////////////////////////////7
   // sfrk
   ///////////////////////////////////////////////////////////////////////////////7
   void ssfrk_(char* transr,
               char* uplo,
               char* trans, 
               int* n,  
               int* k,
               float* alpha,
               float* a, int* lda,
               float* beta,
               float* c);
   void dsfrk_(char* transr,
               char* uplo,
               char* trans,
               int* n, 
               int* k,
               double* alpha,
               double* a, int* lda, 
               double* beta,
               double* c);
   ///////////////////////////////////////////////////////////////////////////////7
   // tfttp
   ///////////////////////////////////////////////////////////////////////////////7
   void stfttp_(char* transr,
                char* uplo,
                int* n,  
                float* arf,
                float* ap,
                int* info);
   void dtfttp_(char* transr,
                char* uplo,
                int* n, 
                double* arf, 
                double* ap,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // axpy 
   ///////////////////////////////////////////////////////////////////////////////7
   void saxpy_(int* n,
               float* alpha,
               float* dx,
               int* incx,
               float* dy,
               int* incy);
   void daxpy_(int* n,
               double* alpha,
               double* dx,
               int* incx,
               double* dy,
               int* incy);
   void caxpy_(int* n,
               std::complex<float>* alpha,
               std::complex<float>* dx,
               int* incx,
               std::complex<float>* dy,
               int* incy);
   void zaxpy_(int* n,
               std::complex<double>* alpha,
               std::complex<double>* dx,
               int* incx,
               std::complex<double>* dy,
               int* incy);

   ///////////////////////////////////////////////////////////////////////////////7
   // potrf 
   ///////////////////////////////////////////////////////////////////////////////7
   void spotrf_(char* uplo,
                int* n,
                float* a, int* lda,
                int* info);
   void dpotrf_(char* uplo,
                int* n,
                double* a, int* lda,
                int* info);
   void cpotrf_(char* uplo,
                int* n,
                std::complex<float>* a, int* lda,
                int* info);
   void zpotrf_(char* uplo,
                int* n,
                std::complex<double>* a, int* lda,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // potrf2 
   ///////////////////////////////////////////////////////////////////////////////7
   void spotrf2_(char* uplo,
                 int* n,
                 float* a, int* lda,
                 int* info);
   void dpotrf2_(char* uplo,
                 int* n,
                 double* a, int* lda,
                 int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // potrs
   ///////////////////////////////////////////////////////////////////////////////7
   void spotrs_(char* uplo,
                int* n,
                int* nrhs,
                float* a, int* lda,
                float* b, int* ldb,
                int* info);
   void dpotrs_(char* uplo,
                int* n,
                int* nrhs,
                double* a, int* lda,
                double* b, int* ldb,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // potri 
   ///////////////////////////////////////////////////////////////////////////////7
   void spotri_(char* uplo,
                int* n,
                float* a, int* lda,
                int* info);
   void dpotri_(char* uplo,
                int* n,
                double* a, int* lda,
                int* info);
   void cpotri_(char* uplo,
                int* n,
                std::complex<float>* a, int* lda,
                int* info);
   void zpotri_(char* uplo,
                int* n,
                std::complex<double>* a, int* lda,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // trtri 
   ///////////////////////////////////////////////////////////////////////////////7
   void strtri_(char* uplo,
                char* diag,
                int* n,
                float* a, int* lda,
                int* info);
   void dtrtri_(char* uplo,
                char* diag,
                int* n,
                double* a, int* lda,
                int* info);
   void ctrtri_(char* uplo,
                char* diag,
                int* n,
                std::complex<float>* a, int* lda,
                int* info);
   void ztrtri_(char* uplo,
                char* diag,
                int* n,
                std::complex<double>* a, int* lda,
                int* info);
   ///////////////////////////////////////////////////////////////////////////////7
   // trtrs
   ///////////////////////////////////////////////////////////////////////////////7
   void strtrs_(char* uplo,
                char* trans,
                char* diag,
                int* n,
                int* nrhs,
                float* a, int* lda,
                float* b, int* ldb,
                int* info);
   void dtrtrs_(char* uplo,
                char* trans,
                char* diag,
                int* n,
                int* nrhs,
                double* a, int* lda,
                double* b, int* ldb,
                int* info);
   ///////////////////////////////////////////////////////////////////////////////7
   // tptrs
   ///////////////////////////////////////////////////////////////////////////////7
   void stptrs_(char* uplo,
                char* trans,
                char* diag,
                int* n,
                int* nrhs,
                float* ap,
                float* b, int* ldb,
                int* info);
   void dtptrs_(char* uplo,
                char* trans,
                char* diag,
                int* n,
                int* nrhs,
                double* ap,
                double* b, int* ldb,
                int* info);
   ///////////////////////////////////////////////////////////////////////////////7
   // syev
   ///////////////////////////////////////////////////////////////////////////////7
   void ssyev_(char* jobz, char* uplo, int* n
              , float* a, int* lda
              , float* w
              , float* work, int* lwork 
              , int* info
              );

   void dsyev_(char* jobz, char* uplo, int* n
              , double* a, int* lda
              , double* w
              , double* work, int* lwork
              , int* info
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // dot
   ///////////////////////////////////////////////////////////////////////////////7
   float sdot_(  int* n
              ,  float* dx
              ,  int* incx
              ,  float* dy
              ,  int* incy
              );

   double ddot_(  int* n
               ,  double* dx
               ,  int* incx
               ,  double* dy
               ,  int* incy
               );

   ///////////////////////////////////////////////////////////////////////////////7
   // copy
   ///////////////////////////////////////////////////////////////////////////////7
   void scopy_(  int* n
              ,  float* dx
              ,  int* incx
              ,  float* dy
              ,  int* incy
              );

   void dcopy_(  int* n
              ,  double* dx
              ,  int* incx
              ,  double* dy
              ,  int* incy
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // syr
   ///////////////////////////////////////////////////////////////////////////////7
   void ssyr_(  char* uplo  
             ,  int* n
             ,  float* alpha
             ,  float* dx
             ,  int* incx
             ,  float* a
             ,  int* lda
             );

   void dsyr_(  char* uplo  
             ,  int* n
             ,  double* alpha
             ,  double* dx
             ,  int* incx
             ,  double* a
             ,  int* lda
             );

   ///////////////////////////////////////////////////////////////////////////////7
   // scal
   ///////////////////////////////////////////////////////////////////////////////7
   void sscal_(  int* n
              ,  float* da
              ,  float* dx
              ,  int* incx
              );


   void dscal_(  int* n
              ,  double* da
              ,  double* dx
              ,  int* incx
              );

   ///////////////////////////////////////////////////////////////////////////////7
   // symv
   ///////////////////////////////////////////////////////////////////////////////7
   void ssymv_(char* uplo, 
               int* n,
               float* alpha,
               float* a, int* lda,
               float* x, int* incx,
               float* beta,
               float* y, int* incy);
   void dsymv_(char* uplo,
               int* n, 
               double* alpha,
               double* a, int* lda,
               double* x, int* incx,
               double* beta,
               double* y, int* incy);
   void csymv_(char* uplo,
               int* n, 
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* x, int* incx,
               std::complex<float>* beta,
               std::complex<float>* y, int* incy);
   void zsymv_(char* uplo, 
               int* n, 
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* x, int* incx,
               std::complex<double>* beta,
               std::complex<double>* y, int* incy);

   ///////////////////////////////////////////////////////////////////////////////7
   // laic1
   ///////////////////////////////////////////////////////////////////////////////7
   void slaic1_(  int* job
               ,  int* j
               ,  float* x
               ,  float* sest
               ,  float* w
               ,  float* gamma
               ,  float* sestpr
               ,  float* s
               ,  float* c
               );

   void dlaic1_(  int* job
               ,  int* j
               ,  double* x
               ,  double* sest
               ,  double* w
               ,  double* gamma
               ,  double* sestpr
               ,  double* s
               ,  double* c
               );

   ///////////////////////////////////////////////////////////////////////////////7
   // sytrf 
   ///////////////////////////////////////////////////////////////////////////////7
   void ssytrf_(char* uplo,
                int* n,
                float* a, int* lda,
                int* ipiv, 
                float* work, int* lwork,
                int* info);
   void dsytrf_(char* uplo,
                int* n,
                double* a, int* lda,
                int* ipiv, 
                double* work, int* lwork,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // sytrs
   ///////////////////////////////////////////////////////////////////////////////7
   void ssytrs_(char* uplo,
                int* n,
                int* nrhs,
                float* a, int* lda,
                int* ipiv,
                float* b, int* ldb,
                int* info);
   void dsytrs_(char* uplo,
                int* n,
                int* nrhs,
                double* a, int* lda,
                int* ipiv,
                double* b, int* ldb,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // sytri 
   ///////////////////////////////////////////////////////////////////////////////7
   void ssytri_(char* uplo,
                int* n,
                float* a, int* lda,
                int* ipiv, float* work,
                int* info);
   void dsytri_(char* uplo,
                int* n,
                double* a, int* lda,
                int* ipiv, double* work,
                int* info);

   ///////////////////////////////////////////////////////////////////////////////7
   // ORMQR 
   ///////////////////////////////////////////////////////////////////////////////7
   void sormqr_(char* side, char* trans,
                int* m, int* n, int* k,
                float* a, int* lda, float* tau,
                float* c, int* ldc,
                float* work, int* lwork,
                int* info);

   void dormqr_(char* side, char* trans,
                int* m, int* n, int* k,
                double* a, int* lda, double* tau,
                double* c, int* ldc,
                double* work, int* lwork,
                int* info);

};

namespace midas
{
namespace lapack_interface
{
   /***************************************************************************/
   // GEMV
   /***************************************************************************/
   void gemv(char* transa
           , int* m, int* n
           , float* alpha
           , float* a, int* lda
           , float* x, int* incx
           , float* beta
           , float* y, int* incy
           )
   {
      sgemv_(transa, m, n, alpha, a, lda, x, incx, beta, y, incy);
   }

   void gemv(char* transa
           , int* m, int* n
           , double* alpha
           , double* a, int* lda
           , double* x, int* incx
           , double* beta
           , double* y, int* incy
           )
   {
      dgemv_(transa, m, n, alpha, a, lda, x, incx, beta, y, incy);
   }

   void gemv(char* transa
           , int* m, int* n
           , std::complex<float>* alpha
           , std::complex<float>* a, int* lda
           , std::complex<float>* x, int* incx
           , std::complex<float>* beta
           , std::complex<float>* y, int* incy
           )
   {
      cgemv_(transa, m, n, alpha, a, lda, x, incx, beta, y, incy);
   }

   void gemv(char* transa
           , int* m, int* n
           , std::complex<double>* alpha
           , std::complex<double>* a, int* lda
           , std::complex<double>* x, int* incx
           , std::complex<double>* beta
           , std::complex<double>* y, int* incy
           )
   {
      zgemv_(transa, m, n, alpha, a, lda, x, incx, beta, y, incy);
   }

   /***************************************************************************/
   // GEMM
   /***************************************************************************/
   void gemm(char* transa, char* transb,
               int* m, int* n, int* k,
               float* alpha,
               float* a, int* lda,
               float* b, int* ldb,
               float* beta,
               float* c, int* ldc)
   {
      sgemm_(transa, transb,
               m, n, k,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void gemm(char* transa, char* transb,
               int* m, int* n, int* k,
               double* alpha,
               double* a, int* lda,
               double* b, int* ldb,
               double* beta,
               double* c, int* ldc)
   {
      dgemm_(transa, transb,
               m, n, k,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void gemm(char* transa, char* transb,
               int* m, int* n, int* k,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* b, int* ldb,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc)
   {
      cgemm_(transa, transb,
               m, n, k,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void gemm(char* transa, char* transb,
               int* m, int* n, int* k,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* b, int* ldb,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc)
   {
      zgemm_(transa, transb,
               m, n, k,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   /***************************************************************************/
   // GELSS
   /***************************************************************************/
   void gelss(int* m, int* n, int* nrhs,
                     float* a, int* lda,
                     float* b, int* ldb,
                     float* s, float* rcond, int* rank,
                     float* work, int* lwork, int* info)
   {
      sgelss_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, info);
      return;
   }

   void gelss(int* m, int* n, int* nrhs,
                     double* a, int* lda,
                     double* b, int* ldb,
                     double* s, double* rcond, int* rank,
                     double* work, int* lwork, int* info)
   {
      dgelss_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, info);
      return;
   }

   void gelss(int* m, int* n, int* nrhs,
                     std::complex<float>* a, int* lda,
                     std::complex<float>* b, int* ldb,
                     float* s, float* rcond, int* rank,
                     std::complex<float>* work, int* lwork, int* info)
   {
      // rwork is initialized here in order to preserve same interface as real version
      auto rwork = std::make_unique<std::complex<float>[]>(5*std::min(*m, *n));

      cgelss_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, rwork.get(), info);
      return;
   }

   void gelss(int* m, int* n, int* nrhs,
                     std::complex<double>* a, int* lda,
                     std::complex<double>* b, int* ldb,
                     double* s, double* rcond, int* rank,
                     std::complex<double>* work, int* lwork, int* info)
   {
      // rwork is initialized here in order to preserve same interface as real version
      auto rwork = std::make_unique<std::complex<double>[]>(5*std::min(*m, *n));

      zgelss_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, rwork.get(), info);
      return;
   }

   /***************************************************************************/
   // GELSD
   /***************************************************************************/
   void gelsd(int* m, int* n, int* nrhs,
                     float* a, int* lda,
                     float* b, int* ldb,
                     float* s, float* rcond, int* rank,
                     float* work, int* lwork, int* iwork, int* info)
   {
      sgelsd_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, iwork, info);
      return;
   }

   void gelsd(int* m, int* n, int* nrhs,
                     double* a, int* lda,
                     double* b, int* ldb,
                     double* s, double* rcond, int* rank,
                     double* work, int* lwork, int* iwork, int* info)
   {
      dgelsd_(m, n, nrhs,
                     a, lda,
                     b, ldb,
                     s, rcond, rank,
                     work, lwork, iwork, info);
      return;
   }

//   void gelsd(int* m, int* n, int* nrhs,
//                     std::complex<float>* a, int* lda,
//                     std::complex<float>* b, int* ldb,
//                     std::complex<float>* s, std::complex<float>* rcond, int* rank,
//                     std::complex<float>* work, int* lwork, int* iwork, int* info)
//   {
//      cgelsd_(m, n, nrhs,
//                     a, lda,
//                     b, ldb,
//                     s, rcond, rank,
//                     work, lwork, iwork, info);
//      return;
//   }
//
//   void gelsd(int* m, int* n, int* nrhs,
//                     std::complex<double>* a, int* lda,
//                     std::complex<double>* b, int* ldb,
//                     std::complex<double>* s, std::complex<double>* rcond, int* rank,
//                     std::complex<double>* work, int* lwork, int* iwork, int* info)
//   {
//      zgelsd_(m, n, nrhs,
//                     a, lda,
//                     b, ldb,
//                     s, rcond, rank,
//                     work, lwork, iwork, info);
//      return;
//   }


   /***************************************************************************/
   // GESVD
   /***************************************************************************/
   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , float* a,  int* lda, float* s
            , float* u,  int* ldu
            , float* vt, int* ldvt
            , float* work, int* lwork, int* info
            )
   {
      sgesvd_(jobu, jobvt
             , m, n
             , a,  lda, s
             , u,  ldu
             , vt, ldvt
             , work, lwork, info
             );
   }

   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , double* a,  int* lda, double* s
            , double* u,  int* ldu
            , double* vt, int* ldvt
            , double* work, int* lwork, int* info
            )
   {
      dgesvd_(jobu, jobvt
            , m, n
            , a,  lda, s
            , u,  ldu
            , vt, ldvt
            , work, lwork, info
            );
   }

   void gesvd(char* jobu, char* jobvt
             , int* m, int* n
             , std::complex<float>* a,  int* lda, float* s
             , std::complex<float>* u,  int* ldu
             , std::complex<float>* vt, int* ldvt
             , std::complex<float>* work, int* lwork, int* info
             )
   {
      float* rwork = new float[5*std::min(*m,*n)];
      cgesvd_(jobu, jobvt
            , m, n
            , a,  lda, s
            , u,  ldu
            , vt, ldvt
            , work, lwork
            , rwork, info
            );
      delete[] rwork;
   }

   void gesvd(char* jobu, char* jobvt
            , int* m, int* n
            , std::complex<double>* a,  int* lda, double* s
            , std::complex<double>* u,  int* ldu
            , std::complex<double>* vt, int* ldvt
            , std::complex<double>* work, int* lwork, int* info
            )
   {
      double* rwork = new double[5*std::min(*m,*n)];
      zgesvd_(jobu, jobvt
            , m, n
            , a,  lda, s
            , u,  ldu
            , vt, ldvt
            , work, lwork
            , rwork, info
            );
      delete[] rwork;
   }
   
   /***************************************************************************/
   // GESDD
   /***************************************************************************/
   void gesdd(char* jobz
            , int* m, int* n
            , float* a,  int* lda, float* s
            , float* u,  int* ldu
            , float* vt, int* ldvt
            , float* work, int* lwork, int* iwork, int* info
            )
   {
      sgesdd_(jobz
            , m, n
            , a, lda, s
            , u, ldu
            , vt, ldvt
            , work, lwork, iwork, info
            );
   }

   void gesdd(char* jobz
            , int* m, int* n
            , double* a,  int* lda, double* s
            , double* u,  int* ldu
            , double* vt, int* ldvt
            , double* work, int* lwork, int* iwork, int* info
            )
   {
      dgesdd_(jobz
            , m, n
            , a, lda, s
            , u, ldu
            , vt, ldvt
            , work, lwork, iwork, info
            );
   }
   
   void gesdd(char* jobz
            , int* m, int* n
            , std::complex<float>* a,  int* lda, float* s
            , std::complex<float>* u,  int* ldu
            , std::complex<float>* vt, int* ldvt
            , std::complex<float>* work, int* lwork, int* iwork, int* info
            )
   {
      float* rwork = new float[std::max(1, *jobz == 'N' ? 5*std::min(*m,*n) : 5*std::min(*m,*n)*std::min(*m,*n) + 7*std::min(*m,*n))];
      cgesdd_(jobz
            , m, n
            , a, lda, s
            , u, ldu
            , vt, ldvt
            , work, lwork, rwork
            , iwork, info
            );
      delete[] rwork;
   }
   
   void gesdd(char* jobz
            , int* m, int* n
            , std::complex<double>* a,  int* lda, double* s
            , std::complex<double>* u,  int* ldu
            , std::complex<double>* vt, int* ldvt
            , std::complex<double>* work, int* lwork, int* iwork, int* info
            )
   {
      double* rwork = new double[std::max(1, *jobz == 'N' ? 5*std::min(*m,*n) : 5*std::min(*m,*n)*std::min(*m,*n) + 7*std::min(*m,*n))];
      zgesdd_(jobz
            , m, n
            , a, lda, s
            , u, ldu
            , vt, ldvt
            , work, lwork, rwork
            , iwork, info
            );
      delete[] rwork;
   }
   
   /***************************************************************************/
   // GEQRF
   /***************************************************************************/
   void geqrf_(int* m, int* n
             , float* a, int* lda
             , float* tau
             , float* work, int* lwork, int* info
             )
   {
      sgeqrf_(m, n
            , a, lda
            , tau
            , work, lwork, info
            );  
   }
   
   void geqrf_(int* m, int* n
             , double* a, int* lda
             , double* tau
             , double* work, int* lwork, int* info
             )
   {
      dgeqrf_(m, n
            , a, lda
            , tau
            , work, lwork, info
            );  
   }
   
   void geqrf_(int* m, int* n
             , std::complex<float>* a, int* lda
             , std::complex<float>* tau
             , std::complex<float>* work, int* lwork, int* info
             )
   {
      cgeqrf_(m, n
            , a, lda
            , tau
            , work, lwork, info
            );  
   }
   
   void geqrf_(int* m, int* n
             , std::complex<double>* a, int* lda
             , std::complex<double>* tau
             , std::complex<double>* work, int* lwork, int* info
             )
   {
      zgeqrf_(m, n
            , a, lda
            , tau
            , work, lwork, info
            );  
   }

   /***************************************************************************/
   // GEQP3
   /***************************************************************************/
   void geqp3( int* m, int* n
             , float* a, int* lda, int* jpvt
             , float* tau
             , float* work, int* lwork, int* info
             )
   {
      sgeqp3_(m, n
            , a, lda, jpvt
            , tau
            , work, lwork, info
            );  
   }
   
   void geqp3( int* m, int* n
             , double* a, int* lda, int* jpvt
             , double* tau
             , double* work, int* lwork, int* info
             )
   {
      dgeqp3_(m, n
            , a, lda, jpvt
            , tau
            , work, lwork, info
            );  
   }
   
   void geqp3( int* m, int* n
             , std::complex<float>* a, int* lda, int* jpvt
             , std::complex<float>* tau
             , std::complex<float>* work, int* lwork, int* info
             )
   {
      cgeqp3_(m, n
            , a, lda, jpvt
            , tau
            , work, lwork, info
            );  
   }
   
   void geqp3( int* m, int* n
             , std::complex<double>* a, int* lda, int* jpvt
             , std::complex<double>* tau
             , std::complex<double>* work, int* lwork, int* info
             )
   {
      zgeqp3_(m, n
            , a, lda, jpvt
            , tau
            , work, lwork, info
            );  
   }
   
   /***************************************************************************/
   // GETRF
   /***************************************************************************/
   void getrf_(int* m, int* n
             , float* a, int* lda
             , int* ipiv, int* info
             )
   {
      sgetrf_(m, n
            , a, lda
            , ipiv, info
            ); 
   }
   
   void getrf_(int* m, int* n
             , double* a, int* lda
             , int* ipiv, int* info
             )
   {
      dgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
   }
   
   void getrf_(int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv, int* info
             )
   {
      cgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
   }
   
   void getrf_(int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv, int* info
             )
   {
      zgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
   }

   /***************************************************************************/
   // GETRF
   /***************************************************************************/
   void getrf(int* m, int* n
             , float* a, int* lda
             , int* ipiv, int* info
             )
   {
      sgetrf_(m, n
            , a, lda
            , ipiv, info
            ); 
      return; 
   }
   
   void getrf(int* m, int* n
             , double* a, int* lda
             , int* ipiv, int* info
             )
   {
      dgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
      return; 
   }
   
   void getrf(int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv, int* info
             )
   {
      cgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
      return; 
   }
   
   void getrf(int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv, int* info
             )
   {
      zgetrf_(m, n
            , a, lda
            , ipiv, info
            );  
      return; 
   }

   /***************************************************************************/
   // GETRS
   /***************************************************************************/
   void getrs(char* transa, int* m, int* n
             , float* a, int* lda
             , int* ipiv
             , float* b, int* ldb
             , int* info
             )
   {
      sgetrs_(transa, m, n
            , a, lda
            , ipiv
            , b, ldb
            , info
            );  
      return; 
   }
   
   void getrs(char* transa, int* m, int* n
             , double* a, int* lda
             , int* ipiv
             , double* b, int* ldb
             , int* info
             )
   {
      dgetrs_(transa, m, n
            , a, lda
            , ipiv
            , b, ldb
            , info
            );  
      return; 
   }
   
   void getrs(char* transa, int* m, int* n
             , std::complex<float>* a, int* lda
             , int* ipiv
             , std::complex<float>* b, int* ldb
             , int* info
             )
   {
      cgetrs_(transa, m, n
            , a, lda
            , ipiv
            , b, ldb
            , info
            );  
      return; 
   }
   
   void getrs(char* transa, int* m, int* n
             , std::complex<double>* a, int* lda
             , int* ipiv
             , std::complex<double>* b, int* ldb
             , int* info
             )
   {
      zgetrs_(transa, m, n
            , a, lda
            , ipiv
            , b, ldb
            , info
            );  
      return; 
   }

   /***************************************************************************/
   // GETRI
   /***************************************************************************/
   void getri( int* n
             , float* a, int* lda
             , int* ipiv
             , float* work, int* lwork
             , int* info
             )
   {
      sgetri_(n
            , a, lda
            , ipiv
            , work, lwork
            , info
            );  
      return; 
   }
   
   void getri( int* n
             , double* a, int* lda
             , int* ipiv
             , double* work, int* lwork
             , int* info
             )
   {
      dgetri_(n
            , a, lda
            , ipiv
            , work, lwork
            , info
            );  
      return; 
   }
   
   void getri( int* n
             , std::complex<float>* a, int* lda
             , int* ipiv
             , std::complex<float>* work, int* lwork
             , int* info
             )
   {
      cgetri_(n
            , a, lda
            , ipiv
            , work, lwork
            , info
            );  
      return; 
   }
   
   void getri( int* n
             , std::complex<double>* a, int* lda
             , int* ipiv
             , std::complex<double>* work, int* lwork
             , int* info
             )
   {
      zgetri_(n
            , a, lda
            , ipiv
            , work, lwork
            , info
            );  
      return; 
   }

   /***************************************************************************/
   // GESV
   /***************************************************************************/
   void gesv_(int* n, int* nrhs
            , float* a, int* lda
            , int* ipiv
            , float* b, int* ldb
            , int* info
            )
   {
      sgesv_(n, nrhs
           , a, lda
           , ipiv
           , b, ldb
           , info
           );
   }
   
   void gesv_(int* n, int* nrhs
            , double* a, int* lda
            , int* ipiv
            , double* b, int* ldb
            , int* info
            )
   {
      dgesv_(n, nrhs
           , a, lda
           , ipiv
           , b, ldb
           , info
           );
   }
   
   void gesv_(int* n, int* nrhs
            , std::complex<float>* a, int* lda
            , int* ipiv
            , std::complex<float>* b, int* ldb
            , int* info
            )
   {
      cgesv_(n, nrhs
           , a, lda
           , ipiv
           , b, ldb
           , info
           );
   }
   
   void gesv_(int* n, int* nrhs
            , std::complex<double>* a, int* lda
            , int* ipiv
            , std::complex<double>* b, int* ldb
            , int* info
            )
   {
      zgesv_(n, nrhs
           , a, lda
           , ipiv
           , b, ldb
           , info
           );
   }

   /***************************************************************************/
   // POSV
   /***************************************************************************/
   void posv_(char* uplo, int* n, int* nrhs
            , float* a, int* lda
            , float* b, int* ldb
            , int* info
            )
   {
      sposv_(uplo, n, nrhs
           , a, lda
           , b, ldb
           , info
           );
   }

   void posv_(char* uplo, int* n, int* nrhs
            , double* a, int* lda
            , double* b, int* ldb
            , int* info
            )
   {
      dposv_(uplo, n, nrhs
           , a, lda
           , b, ldb
           , info
           );
   }

   void posv_(char* uplo, int* n, int* nrhs
            , std::complex<float>* a, int* lda
            , std::complex<float>* b, int* ldb
            , int* info
            )
   {
      cposv_(uplo, n, nrhs
           , a, lda
           , b, ldb
           , info
           );
   }

   void posv_(char* uplo, int* n, int* nrhs
            , std::complex<double>* a, int* lda
            , std::complex<double>* b, int* ldb
            , int* info
            )
   {
      zposv_(uplo, n, nrhs
           , a, lda
           , b, ldb
           , info
           );
   }

   /***************************************************************************/
   // HESV
   /***************************************************************************/
   void hesv( char* uplo, int* n, int* nrhs
            , std::complex<float>* a, int* lda, int* ipiv
            , std::complex<float>* b, int* ldb, std::complex<float>* work, int* lwork
            , int* info
            )
   {
      chesv_(uplo, n, nrhs
           , a, lda, ipiv
           , b, ldb, work, lwork
           , info
           );
   }


   void hesv( char* uplo, int* n, int* nrhs
            , std::complex<double>* a, int* lda, int* ipiv
            , std::complex<double>* b, int* ldb, std::complex<double>* work, int* lwork
            , int* info
            )
   {
      zhesv_(uplo, n, nrhs
           , a, lda, ipiv
           , b, ldb, work, lwork
           , info
           );
   }

   /***************************************************************************/
   // SYGV
   /***************************************************************************/
   void sygv_(int* itype, char* jobz
            , char* uplo, int* n
            , float* a, int* lda
            , float* b, int* ldb
            , float* w
            , float* work, int* lwork
            , int* info
            )
   {
      ssygv_(itype, jobz
           , uplo, n
           , a, lda
           , b, ldb
           , w
           , work, lwork
           , info
           );
   }

   void sygv_(int* itype, char* jobz
            , char* uplo, int* n
            , double* a, int* lda
            , double* b, int* ldb
            , double* w
            , double* work, int* lwork
            , int* info
            )
   {
      dsygv_(itype, jobz
           , uplo, n
           , a, lda
           , b, ldb
           , w
           , work, lwork
           , info
           );
   }
   //
   // NO COMPLEX VERSION OF SYGV
   //
   
   /***************************************************************************/
   // SYGVD
   /***************************************************************************/
   void sygvd_(int* itype, char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* b, int* ldb
             , float* w
             , float* work, int* lwork, int* iwork
             , int* liwork, int* info
             )
   {
      ssygvd_(itype, jobz, uplo, n
            , a, lda
            , b, ldb
            , w
            , work, lwork, iwork
            , liwork, info
            );
   }

   void sygvd_(int* itype, char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* b, int* ldb
             , double* w
             , double* work, int* lwork, int* iwork
             , int* liwork, int* info
             )
   {
      dsygvd_(itype, jobz, uplo, n
            , a, lda
            , b, ldb
            , w
            , work, lwork, iwork
            , liwork, info
            );
   }
   //
   // NO COMPLEX VERSION OF SYGVD
   //
   
   /***************************************************************************/
   // SYEVD
   /***************************************************************************/
   void syevd_(char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* w
             , float* work, int* lwork, int* iwork
             , int* liwork, int* info
             )
   {
      ssyevd_(jobz, uplo, n
            , a, lda
            , w
            , work, lwork, iwork
            , liwork, info
            );
   }
   
   void syevd_(char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* w
             , double* work, int* lwork, int* iwork
             , int* liwork, int* info
             )
   {
      dsyevd_(jobz, uplo, n
            , a, lda
            , w
            , work, lwork, iwork
            , liwork, info
            );
   }
   
   /***************************************************************************/
   // SYEVX
   /***************************************************************************/

   void syevx
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  float* a
      ,  int* lda
      ,  float* vl
      ,  float* vu
      ,  int* il
      ,  int* iu
      ,  float* abstol
      ,  int* m
      ,  float* w
      ,  float* z
      ,  int* ldz
      ,  float* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      )
   {
      ssyevx_(jobz, range, uplo, n, a, lda, vl, vu, il, iu, abstol, m, w, z, ldz, work, lwork, iwork, ifail, info);
   }

   void syevx
      (  char* jobz
      ,  char* range
      ,  char* uplo
      ,  int* n
      ,  double* a
      ,  int* lda
      ,  double* vl
      ,  double* vu
      ,  int* il
      ,  int* iu
      ,  double* abstol
      ,  int* m
      ,  double* w
      ,  double* z
      ,  int* ldz
      ,  double* work
      ,  int* lwork
      ,  int* iwork
      ,  int* ifail
      ,  int* info
      )
   {
      dsyevx_(jobz, range, uplo, n, a, lda, vl, vu, il, iu, abstol, m, w, z, ldz, work, lwork, iwork, ifail, info);
   }

   /***************************************************************************/
   // HEEV
   /***************************************************************************/
   void heev
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* info
      )
   {
      cheev_(jobz, uplo, n, a, lda, w, work, lwork, rwork, info);
   }

   void heev
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* info
      )
   {
      zheev_(jobz, uplo, n, a, lda, w, work, lwork, rwork, info);
   }


   /***************************************************************************/
   // HEEVD
   /***************************************************************************/
   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<float>* a
      ,  int* lda
      ,  float* w
      ,  std::complex<float>* work
      ,  int* lwork
      ,  float* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      )
   {
      cheevd_(jobz, uplo, n, a, lda, w, work, lwork, rwork, lrwork, iwork, liwork, info);
   }

   void heevd
      (  char* jobz
      ,  char* uplo
      ,  int* n
      ,  std::complex<double>* a
      ,  int* lda
      ,  double* w
      ,  std::complex<double>* work
      ,  int* lwork
      ,  double* rwork
      ,  int* lrwork
      ,  int* iwork
      ,  int* liwork
      ,  int* info
      )
   {
      zheevd_(jobz, uplo, n, a, lda, w, work, lwork, rwork, lrwork, iwork, liwork, info);
   }


   
   /***************************************************************************/
   // GEEV
   /***************************************************************************/
   void geev_(char* jobvl, char* jobvr, int* n
            , float* a, int* lda
            , float* wr
            , float* wi
            , float* vl, int* ldvl
            , float* vr, int* ldvr
            , float* work, int* lwork, int* info
            )
   {
      sgeev_(jobvl, jobvr, n
           , a, lda
           , wr
           , wi
           , vl, ldvl
           , vr, ldvr
           , work, lwork, info
           );
   }
   
   void geev_(char* jobvl, char* jobvr, int* n
            , double* a, int* lda
            , double* wr
            , double* wi
            , double* vl, int* ldvl
            , double* vr, int* ldvr
            , double* work, int* lwork, int* info
            )
   {
      dgeev_(jobvl, jobvr, n
           , a, lda
           , wr
           , wi
           , vl, ldvl
           , vr, ldvr
           , work, lwork, info
           );
   }
   
   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void geev_(char* jobvl, char* jobvr, int* n
            , std::complex<float>* a, int* lda
            , std::complex<float>* w
            , std::complex<float>* vl, int* ldvl
            , std::complex<float>* vr, int* ldvr
            , std::complex<float>* work, int* lwork
            , float* rwork, int* info
            )
   {
      cgeev_(jobvl, jobvr, n
           , a, lda
           , w
           , vl, ldvl
           , vr, ldvr
           , work, lwork
           , rwork, info
           );
   }
   
   void geev_(char* jobvl, char* jobvr, int* n
            , std::complex<double>* a, int* lda
            , std::complex<double>* w
            , std::complex<double>* vl, int* ldvl
            , std::complex<double>* vr, int* ldvr
            , std::complex<double>* work, int* lwork
            , double* rwork, int* info
            )
   {
      zgeev_(jobvl, jobvr, n
           , a, lda
           , w
           , vl, ldvl
           , vr, ldvr
           , work, lwork
           , rwork, info
           );
   }
   
   /***************************************************************************/
   // GGEV
   /***************************************************************************/
   void ggev_(char* jobvl, char* jobvr, int* n
            , float* a, int* lda
            , float* b, int* ldb
            , float* alphar
            , float* alphai
            , float* beta
            , float* vl, int* ldvl
            , float* vr, int* ldvr
            , float* work
            , int* lwork, int* info
            )
   {
      sggev_(jobvl, jobvr, n
           , a, lda
           , b, ldb
           , alphar
           , alphai
           , beta
           , vl, ldvl
           , vr, ldvr
           , work
           , lwork, info
           );
   }
   
   void ggev_(char* jobvl, char* jobvr, int* n
            , double* a, int* lda
            , double* b, int* ldb
            , double* alphar
            , double* alphai
            , double* beta
            , double* vl, int* ldvl
            , double* vr, int* ldvr
            , double* work
            , int* lwork, int* info
            )
   {
      dggev_(jobvl, jobvr, n
           , a, lda
           , b, ldb
           , alphar
           , alphai
           , beta
           , vl, ldvl
           , vr, ldvr
           , work
           , lwork, info
           );
   }
   
   // NB COMPLEX VERSIONS HAVE DIFFERENT SIGNATURE!
   void ggev_(char* jobvl, char* jobvr, int* n
            , std::complex<float>* a, int* lda
            , std::complex<float>* b, int* ldb
            , std::complex<float>* alpha
            , std::complex<float>* beta
            , std::complex<float>* vl, int* ldvl
            , std::complex<float>* vr, int* ldvr
            , std::complex<float>* work
            , int* lwork
            , float* rwork, int* info
            )
   {
      cggev_(jobvl, jobvr, n
           , a, lda
           , b, ldb
           , alpha
           , beta
           , vl, ldvl
           , vr, ldvr
           , work
           , lwork
           , rwork, info
           );
   }
   
   void ggev_(char* jobvl, char* jobvr, int* n
            , std::complex<double>* a, int* lda
            , std::complex<double>* b, int* ldb
            , std::complex<double>* alpha
            , std::complex<double>* beta
            , std::complex<double>* vl, int* ldvl
            , std::complex<double>* vr, int* ldvr
            , std::complex<double>* work
            , int* lwork
            , double* rwork, int* info
            )
   {
      zggev_(jobvl, jobvr, n
            , a, lda
            , b, ldb
            , alpha
            , beta
            , vl, ldvl
            , vr, ldvr
            , work
            , lwork
            , rwork, info
            );
   }

   /***************************************************************************/
   // SPTRF + SPTRS
   //
   // sptrf:
   //     INFO is INTEGER
   //     = 0: successful exit
   //     < 0: if INFO = -i, the i-th argument had an illegal value
   //     > 0: if INFO = i, D(i,i) is exactly zero.  The factorization
   //          has been completed, but the block diagonal matrix D is
   //          exactly singular, and division by zero will occur if it
   //          is used to solve a system of equations.
   //
   // sptrs:
   //     INFO is INTEGER
   //     = 0:  successful exit
   //     < 0: if INFO = -i, the i-th argument had an illegal value
   /***************************************************************************/
   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               float* ap,
               float* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      ssptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         ssptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }

   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               double* ap,
               double* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      dsptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         dsptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }
   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<float>* ap,
               std::complex<float>* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      csptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         csptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }

   void sptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<double>* ap,
               std::complex<double>* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      zsptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         zsptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }

   /***************************************************************************/
   // HPTRF + HPTRS
   //
   // hptrf:
   //     INFO is INTEGER
   //     = 0: successful exit
   //     < 0: if INFO = -i, the i-th argument had an illegal value
   //     > 0: if INFO = i, D(i,i) is exactly zero.  The factorization
   //          has been completed, but the block diagonal matrix D is
   //          exactly singular, and division by zero will occur if it
   //          is used to solve a system of equations.
   //
   // hptrs:
   //     INFO is INTEGER
   //     = 0:  successful exit
   //     < 0: if INFO = -i, the i-th argument had an illegal value
   /***************************************************************************/
   void hptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<float>* ap,
               std::complex<float>* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      chptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         chptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }

   void hptrfs(char* uplo,
               int* n,
               int* nrhs,
               std::complex<double>* ap,
               std::complex<double>* b, int* ldb,
               int* info)
   {
      int* ipiv=new int[*n];
      zhptrf_(uplo,
              n,
              ap,
              ipiv,
              info);
      if (*info == 0)
      {
         zhptrs_(uplo,
                 n,
                 nrhs,
                 ap,
                 ipiv,
                 b, ldb,
                 info);
      }
      delete[] ipiv;
      return;
   }

   /***************************************************************************/
   // SYMM
   /***************************************************************************/
   void symm(char* side, char* uplo,
               int* m, int* n,
               float* alpha,
               float* a, int* lda,
               float* b, int* ldb,
               float* beta,
               float* c, int* ldc)
   {
      ssymm_(side, uplo,
               m, n,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void symm(char* side, char* uplo,
               int* m, int* n,
               double* alpha,
               double* a, int* lda,
               double* b, int* ldb,
               double* beta,
               double* c, int* ldc)
   {
      dsymm_(side, uplo,
               m, n,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void symm(char* side, char* uplo,
               int* m, int* n,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* b, int* ldb,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc)
   {
      csymm_(side, uplo,
               m, n,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   void symm(char* side, char* uplo,
               int* m, int* n,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* b, int* ldb,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc)
   {
      zsymm_(side, uplo,
               m, n,
               alpha,
               a, lda,
               b, ldb,
               beta,
               c, ldc);
      return;
   }

   /***************************************************************************/
   // SYRK
   /***************************************************************************/
   void syrk(char* uplo, char* trans,
               int* n, int* k,
               float* alpha,
               float* a, int* lda,
               float* beta,
               float* c, int* ldc)
   {
      ssyrk_(uplo,trans,
               n, k,
               alpha,
               a, lda,
               beta,
               c, ldc);
      return;
   }

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               double* alpha,
               double* a, int* lda,
               double* beta,
               double* c, int* ldc)
   {
      dsyrk_(uplo, trans,
               n, k,
               alpha,
               a, lda,
               beta,
               c, ldc);
      return;
   }

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               std::complex<float>* alpha,
               std::complex<float>* a, int* lda,
               std::complex<float>* beta,
               std::complex<float>* c, int* ldc)
   {
      csyrk_(uplo, trans,
               n, k,
               alpha,
               a, lda,
               beta,
               c, ldc);
      return;
   }

   void syrk(char* uplo, char* trans,
               int* n, int* k,
               std::complex<double>* alpha,
               std::complex<double>* a, int* lda,
               std::complex<double>* beta,
               std::complex<double>* c, int* ldc)
   {
      zsyrk_(uplo, trans,
               n, k,
               alpha,
               a, lda,
               beta,
               c, ldc);
      return;
   }

   /***************************************************************************/
   // SPMV
   /***************************************************************************/
   void spmv(char* uplo,
             int* n,
             float* alpha,
             float* ap,
             float* x, int* incx,
             float* beta,
             float* y, int* incy)
   {
      sspmv_(uplo,
             n,
             alpha,
             ap,
             y, incx,
             beta,
             y, incy);
      return;
   }

   void spmv(char* uplo,
             int* n, 
             double* alpha,
             double* ap, 
             double* x, int* incx,
             double* beta,
             double* y, int* incy)
   {
      dspmv_(uplo,
             n, 
             alpha,
             ap, 
             x, incx,
             beta,
             y, incy);
      return;
   }

   void spmv(char* uplo,
             int* n,
             std::complex<float>* alpha,
             std::complex<float>* ap,
             std::complex<float>* x, int* incx, 
             std::complex<float>* beta,
             std::complex<float>* y, int* incy)
   {
      cspmv_(uplo,
             n,
             alpha,
             ap,
             x, incx,
             beta,
             y, incy);
      return;
   }

   void spmv(char* uplo,
             int* n,
             std::complex<double>* alpha,
             std::complex<double>* ap, 
             std::complex<double>* x, int* incx,
             std::complex<double>* beta,
             std::complex<double>* y, int* incy)
   {
      zspmv_(uplo,
             n,
             alpha,
             ap, 
             x, incx,
             beta,
             y, incy);
      return;
   }
   /***************************************************************************/
   // SFRK
   /***************************************************************************/
   void sfrk(char* transr,
             char* uplo,
             char* trans,
             int* n,
             int* k,
             float* alpha,
             float* a, int* lda,
             float* beta,
             float* c)
   {
      ssfrk_(transr,
             uplo,
             trans,
             n,
             k,
             alpha,
             a, lda, 
             beta,
             c);
      return;
   }

   void sfrk(char* transr,
             char* uplo,
             char* trans,
             int* n, 
             int* k,
             double* alpha,
             double* a, int* lda, 
             double* beta,
             double* c)
   {
      dsfrk_(transr,
             uplo,
             trans,
             n, 
             k,
             alpha,
             a, lda, 
             beta,
             c);
      return;
   }

   /***************************************************************************/
   // TFTTP
   /***************************************************************************/
   void tfttp(char* transr,
              char* uplo,
              int* n,
              float* arf,
              float* ap,
              int* info)
   {
      stfttp_(transr,
              uplo,
              n,
              arf,
              ap, 
              info);
      return;
   }

   void tfttp(char* transr,
              char* uplo,
              int* n, 
              double* arf, 
              double* ap,
              int* info)
   {
      dtfttp_(transr,
              uplo,
              n, 
              arf,  
              ap,
              info);
      return;
   }

   /***************************************************************************/
   // AXPY
   /***************************************************************************/
   void axpy(int* n,
             float* alpha,
             float* dx,
             int* incx,
             float* dy,
             int* incy)
   {
      saxpy_(n,alpha,dx,incx,dy,incy);
      return;
   }
   void axpy(int* n,
             double* alpha,
             double* dx,
             int* incx,
             double* dy,
             int* incy)
   {
      daxpy_(n,alpha,dx,incx,dy,incy);
      return;
   }
   void axpy(int* n,
             std::complex<float>* alpha,
             std::complex<float>* dx,
             int* incx,
             std::complex<float>* dy,
             int* incy)
   {
      caxpy_(n,alpha,dx,incx,dy,incy);
      return;
   }

   void axpy(int* n,
             std::complex<double>* alpha,
             std::complex<double>* dx,
             int* incx,
             std::complex<double>* dy,
             int* incy)
   {
      zaxpy_(n,alpha,dx,incx,dy,incy);
      return;
   }

   /***************************************************************************/
   // POTRF
   /***************************************************************************/
   void potrf(char* uplo,
              int* n,
              float* a, int* lda,
              int* info)
   {
      spotrf_(uplo,n,a,lda,info);
      return;
   }
   void potrf(char* uplo,
              int* n,
              double* a, int* lda,
              int* info)
   {
      dpotrf_(uplo,n,a,lda,info);
      return;
   }

   void potrf(char* uplo,
              int* n,
              std::complex<float>* a, int* lda,
              int* info)
   {
      cpotrf_(uplo,n,a,lda,info);
      return;
   }
   void potrf(char* uplo,
              int* n,
              std::complex<double>* a, int* lda,
              int* info)
   {
      zpotrf_(uplo,n,a,lda,info);
      return;
   }

   /***************************************************************************/
   // POTRF2
   /***************************************************************************/
   void potrf2(char* uplo,
               int* n,
               float* a, int* lda,
               int* info)
   {
      spotrf_(uplo,n,a,lda,info);
      return;
   }
   void potrf2(char* uplo,
               int* n,
               double* a, int* lda,
               int* info)
   {
      dpotrf_(uplo,n,a,lda,info);
      return;
   }

   /***************************************************************************/
   // POTRS
   /***************************************************************************/
   void potrs(char* uplo,
              int* n,
              int* nrhs,
              float* a, int* lda,
              float* b, int* ldb,
              int* info)
   {
      spotrs_(uplo,n,nrhs,a,lda,b,ldb,info);
      return;
   }
   void potrs(char* uplo,
              int* n,
              int* nrhs,
              double* a, int* lda,
              double* b, int* ldb,
              int* info)
   {
      dpotrs_(uplo,n,nrhs,a,lda,b,ldb,info);
      return;
   }

   /***************************************************************************/
   // POTRI
   /***************************************************************************/
   void potri(char* uplo,
              int* n,
              float* a, int* lda,
              int* info)
   {
      spotri_(uplo,n,a,lda,info);
      return;
   }
   void potri(char* uplo,
              int* n,
              double* a, int* lda,
              int* info)
   {
      dpotri_(uplo,n,a,lda,info);
      return;
   }

   void potri(char* uplo,
              int* n,
              std::complex<float>* a, int* lda,
              int* info)
   {
      cpotri_(uplo,n,a,lda,info);
      return;
   }
   void potri(char* uplo,
              int* n,
              std::complex<double>* a, int* lda,
              int* info)
   {
      zpotri_(uplo,n,a,lda,info);
      return;
   }

   /***************************************************************************/
   // TRTRI
   /***************************************************************************/
   void trtri(char* uplo,
              char* diag,
              int* n,
              float* a, int* lda,
              int* info)
   {
      strtri_(uplo,diag,n,a,lda,info);
      return;
   }

   void trtri(char* uplo,
              char* diag,
              int* n,
              double* a, int* lda,
              int* info)
   {
      dtrtri_(uplo,diag,n,a,lda,info);
      return;
   }

   void trtri(char* uplo,
              char* diag,
              int* n,
              std::complex<float>* a, int* lda,
              int* info)
   {
      ctrtri_(uplo,diag,n,a,lda,info);
      return;
   }

   void trtri(char* uplo,
              char* diag,
              int* n,
              std::complex<double>* a, int* lda,
              int* info)
   {
      ztrtri_(uplo,diag,n,a,lda,info);
      return;
   }

   /***************************************************************************/
   // TRTRS
   /***************************************************************************/
   void trtrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              float* a, int* lda,
              float* b, int* ldb,
              int* info)
   {
      strtrs_(uplo,trans,diag,n,nrhs,a,lda,b,ldb,info);
      return;
   }

   void trtrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              double* a, int* lda,
              double* b, int* ldb,
              int* info)
   {
      dtrtrs_(uplo,trans,diag,n,nrhs,a,lda,b,ldb,info);
      return;
   }

   /***************************************************************************/
   // TPTRS
   /***************************************************************************/
   void tptrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              float* ap,
              float* b, int* ldb,
              int* info)
   {
      stptrs_(uplo,trans,diag,n,nrhs,ap,b,ldb,info);
      return;
   }

   void tptrs(char* uplo,
              char* trans,
              char* diag,
              int* n,
              int* nrhs,
              double* ap,
              double* b, int* ldb,
              int* info)
   {
      dtptrs_(uplo,trans,diag,n,nrhs,ap,b,ldb,info);
      return;
   }

   /***************************************************************************/
   // SYEV
   /***************************************************************************/
   void syev(  char* jobz, char* uplo, int* n
             , float* a, int* lda
             , float* w
             , float* work, int* lwork
             , int* info
             )
   {
      ssyev_(jobz, uplo, n
            , a, lda
            , w
            , work, lwork
            , info
            );
   }
   
   void syev(  char* jobz, char* uplo, int* n
             , double* a, int* lda
             , double* w
             , double* work, int* lwork
             , int* info
             )
   {
      dsyev_(jobz, uplo, n
            , a, lda
            , w
            , work, lwork
            , info
            );
   }

   /***************************************************************************/
   // DOT
   /***************************************************************************/
   float dot(  int* n
            ,  float* dx
            ,  int* incx
            ,  float* dy
            ,  int* incy
            )
   {
      return sdot_(n,dx,incx,dy,incy);
   }

   double dot(  int* n
             ,  double* dx
             ,  int* incx
             ,  double* dy
             ,  int* incy
             )
   {
      return ddot_(n,dx,incx,dy,incy);
   }

   /***************************************************************************/
   // COPY
   /***************************************************************************/
   void copy(  int* n
            ,  float* dx
            ,  int* incx
            ,  float* dy
            ,  int* incy
            )
   {
      scopy_(n,dx,incx,dy,incy);
   }

   void copy(  int* n
            ,  double* dx
            ,  int* incx
            ,  double* dy
            ,  int* incy
            )
   {
      dcopy_(n,dx,incx,dy,incy);
   }

   /***************************************************************************/
   // SYR
   /***************************************************************************/
   void syr(  char* uplo  
           ,  int* n
           ,  float* alpha
           ,  float* dx
           ,  int* incx
           ,  float* a
           ,  int* lda
           )
   {
      ssyr_(uplo,n,alpha,dx,incx,a,lda);
   }

   void syr(  char* uplo  
           ,  int* n
           ,  double* alpha
           ,  double* dx
           ,  int* incx
           ,  double* a
           ,  int* lda
           )
   {
      dsyr_(uplo,n,alpha,dx,incx,a,lda);
   }

   /***************************************************************************/
   // SCAL
   /***************************************************************************/
   void scal(  int* n
            ,  float* da
            ,  float* dx
            ,  int* incx
            )
   {
      sscal_(n,da,dx,incx);
   }

   void scal(  int* n
            ,  double* da
            ,  double* dx
            ,  int* incx
            )
   {
      dscal_(n,da,dx,incx);
   }

   /***************************************************************************/
   // SYMV
   /***************************************************************************/
   void symv(char* uplo,
             int* n,
             float* alpha,
             float* a, int* lda,
             float* x, int* incx,
             float* beta,
             float* y, int* incy)
   {
      ssymv_(uplo,
             n, 
             alpha,
             a, lda,
             x, incx,
             beta,
             y, incy);
      return;
   }

   void symv(char* uplo,
             int* n, 
             double* alpha,
             double* a, int* lda,
             double* x, int* incx,
             double* beta,
             double* y, int* incy)
   {
      dsymv_(uplo,
             n,
             alpha,
             a, lda,
             x, incx,
             beta,
             y, incy);
      return;
   }

   void symv(char* uplo,
             int* n, 
             std::complex<float>* alpha,
             std::complex<float>* a, int* lda,
             std::complex<float>* x, int* incx,
             std::complex<float>* beta,
             std::complex<float>* y, int* incy)
   {
      csymv_(uplo,
             n,
             alpha,
             a, lda,
             x, incx,
             beta,
             y, incy);
      return;
   }

   void symv(char* uplo,
             int* n, 
             std::complex<double>* alpha,
             std::complex<double>* a, int* lda,
             std::complex<double>* x, int* incx,
             std::complex<double>* beta,
             std::complex<double>* y, int* incy)
   {
      zsymv_(uplo,
             n,
             alpha,
             a, lda,
             x, incx,
             beta,
             y, incy);
      return;
   }

   /***************************************************************************/
   // LAIC1
   /***************************************************************************/
   void laic1  (  int* job
               ,  int* j
               ,  float* x
               ,  float* sest
               ,  float* w
               ,  float* gamma
               ,  float* sestpr
               ,  float* s
               ,  float* c
               )
   {
      slaic1_(job, j, x, sest, w, gamma, sestpr, s, c);
      return;
   }

   void laic1  (  int* job
               ,  int* j
               ,  double* x
               ,  double* sest
               ,  double* w
               ,  double* gamma
               ,  double* sestpr
               ,  double* s
               ,  double* c
               )
   {
      dlaic1_(job, j, x, sest, w, gamma, sestpr, s, c);
      return;
   }

   /***************************************************************************/
   // SYTRF
   /***************************************************************************/
   void sytrf(char* uplo,
              int* n,
              float* a, int* lda,
              int* ipiv,
              float* work, int* lwork,
              int* info)
   {
      ssytrf_(uplo,n,a,lda,ipiv,work,lwork,info);
      return;
   }
   void sytrf(char* uplo,
              int* n,
              double* a, int* lda,
              int* ipiv,
              double* work, int* lwork,
              int* info)
   {
      dsytrf_(uplo,n,a,lda,ipiv,work,lwork,info);
      return;
   }

   /***************************************************************************/
   // SYTRS
   /***************************************************************************/
   void sytrs(char* uplo,
              int* n,
              int* nrhs,
              float* a, int* lda,
              int* ipiv,
              float* b, int* ldb,
              int* info)
   {
      ssytrs_(uplo,n,nrhs,a,lda,ipiv,b,ldb,info);
      return;
   }
   void sytrs(char* uplo,
              int* n,
              int* nrhs,
              double* a, int* lda,
              int* ipiv,
              double* b, int* ldb,
              int* info)
   {
      dsytrs_(uplo,n,nrhs,a,lda,ipiv,b,ldb,info);
      return;
   }

   /***************************************************************************/
   // SYTRI
   /***************************************************************************/
   void sytri(char* uplo,
              int* n,
              float* a, int* lda,
              int* ipiv, float* work,
              int* info)
   {
      ssytri_(uplo,n,a,lda,ipiv,work,info);
      return;
   }
   void sytri(char* uplo,
              int* n,
              double* a, int* lda,
              int* ipiv, double* work,
              int* info)
   {
      dsytri_(uplo,n,a,lda,ipiv,work,info);
      return;
   }

   /***************************************************************************/
   // ORMQR 
   /***************************************************************************/
   void ormqr(char* side, char* trans,
              int* m, int* n, int* k,
              float* a, int* lda, float* tau,
              float* c, int* ldc,
              float* work, int* lwork,
              int* info)
   {
      sormqr_(side,trans,m,n,k,a,lda,tau,c,ldc,work,lwork,info);
      return;
   }

   void ormqr(char* side, char* trans,
              int* m, int* n, int* k,
              double* a, int* lda, double* tau,
              double* c, int* ldc,
              double* work, int* lwork,
              int* info)
   {
      dormqr_(side,trans,m,n,k,a,lda,tau,c,ldc,work,lwork,info);
      return;
   }

} /* namespace lapack_interface */
} /* namespace midas */
