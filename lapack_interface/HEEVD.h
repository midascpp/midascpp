#ifndef HEEVD_H_INCLUDED
#define HEEVD_H_INCLUDED

#include <memory> // for std::unique_ptr

#include "util/type_traits/Complex.h"

#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/SYEVD.h"


/**
 * Perform divide-and-conquer eigenvalue decomposition of a Hermitian matrix.
 *
 * @param arMatrix         Hermitian matrix as column-major pointer
 * @param aN               Dimension of the matrix
 * @param aJobz            'N' for only eigenvalues, 'V' for eigenvalues and eigenvectors
 * @param aUplo            'U' for upper triangle is stored, 'L' for lower triangle
 **/
template
   <  typename T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_struct<T> HEEVD
   (  T* arMatrix
   ,  int aN
   ,  char aJobz = 'V'
   ,  char aUplo = 'U'
   )
{
   using param_t = T;
   using real_t = midas::type_traits::RealTypeT<T>;

   // Set arguments and initialize workspace
   char jobz = aJobz;
   char uplo = aUplo;
   int n = aN;
   int lda = std::max(1, n);
   int lwork = std::max(1, 2*n + n*n);
   auto work = std::make_unique<param_t[]>(lwork);
   int lrwork = 1 + 5*n + 2*n*n;
   auto rwork = std::make_unique<real_t[]>(lrwork);
   int liwork = 3 + 5*n;
   auto iwork = std::make_unique<int[]>(liwork);
   int info = -1;

   // initialize eigenvalues
   auto w = std::make_unique<real_t[]>(n);

   // Call lapack routine
   midas::lapack_interface::heevd(&jobz, &uplo, &n, arMatrix, &lda, w.get(), work.get(), &lwork, rwork.get(), &lrwork, iwork.get(), &liwork, &info);
   
   // Sanity check
   if (  info != 0
      )
   {
      MidasWarning("HEEVD info = " + std::to_string(info));
   }

   // Init result
   Eigenvalue_struct<T> eig_sol;
   eig_sol._info = info;
   eig_sol._n = aN;
   eig_sol._num_eigval = aN;
   eig_sol._eigenvalues = w.release();

   if (  aJobz == 'V'
      )
   {
      eig_sol._eigenvectors = new T*[n];
      auto size = n*n;
      T* p = new T[size];
      for(int i=0; i<size; ++i)
      {
         p[i] = arMatrix[i];
      }
      for(int i=0; i<n; ++i)
      {
         eig_sol._eigenvectors[i] = p + i*aN;
      }
   }
   else
   {
      eig_sol._eigenvectors = nullptr;
   }

   return eig_sol;
}

/**
 * Wrapper for MidasMatrix
 *
 * @note
 *    Call SYEVD for real matrices
 **/
template
   <  typename T
   >
Eigenvalue_struct<T> HEEVD
   (  const GeneralMidasMatrix<T>& arMatrix
   ,  char aJobz = 'V'
   ,  char aUplo = 'U'
   )
{
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0 && arMatrix.IsSquare());
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);

   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      // Do HEEVD
      return HEEVD(a.get(), arMatrix.Ncols(), aJobz, aUplo);
   }
   else
   {
      return SYEVD(a.get(), arMatrix.Ncols(), aJobz, aUplo);
   }
}

#endif /* HEEVD_H_INCLUDED */
