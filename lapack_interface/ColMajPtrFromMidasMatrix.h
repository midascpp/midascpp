#ifndef COLMAJPTRFROMMIDASMATRIX_H_INCLUDED
#define COLMAJPTRFROMMIDASMATRIX_H_INCLUDED

#include "mmv/MidasMatrix.h"

//template<class T>
//T** ColMajPtrFromMidasMatrix(const GeneralMidasMatrix<T>& arMatrix)
//{
//   T* a_p = new T[arMatrix.Ncols()*arMatrix.Nrows()];
//   T** a = new T*[arMatrix.Ncols()];
//   for(int i=0; i<arMatrix.Ncols(); ++i)
//   {   
//      a[i] = a_p + i*arMatrix.Nrows();
//      for(int j=0; j<arMatrix.Nrows(); ++j)
//         a[i][j] = arMatrix[j][i];
//   }
//   return a;
//}

template<class T>
std::unique_ptr<T[]> ColMajPtrFromMidasMatrix2(const GeneralMidasMatrix<T>& arMatrix)
{
   std::unique_ptr<T[]> ptr(new T[arMatrix.Ncols()*arMatrix.Nrows()]);
   int idx = 0;

   for(int i=0; i<arMatrix.Ncols(); ++i)
   {  
      for(int j=0; j<arMatrix.Nrows(); ++j)
      {
         ptr[idx] = arMatrix[j][i];
         ++idx;
      }
   }

   return ptr;
}

#endif /* COLMAJPTRFROMMIDASMATRIX_H_INCLUDED */
