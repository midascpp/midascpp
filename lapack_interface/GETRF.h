#ifndef GETRF_H_INCLUDED
#define GETRF_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <algorithm> // for std::min, std::max

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/LU_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

/**
 *
 **/
template<class T>
LU_struct<T> GETRF(T* aA, int aM, int aN)
{
   using a_type = typename detail::LU_struct_traits<T>::a_type;
   using ipiv_type = typename detail::LU_struct_traits<T>::ipiv_type;

   ///////////////////////
   // setup
   ///////////////////////
   int m = aM;
   int n = aN;
   a_type* a = aA;
   int lda = std::max(1,m);
   std::unique_ptr<ipiv_type[]> ipiv(new ipiv_type[std::min(m,n)]);
   int info;
   
   ///////////////////////
   // call geqrf_
   ///////////////////////
   midas::lapack_interface::getrf_(&m,&n,a,&lda,ipiv.get(),&info);

   if(info != 0)
   {
      MidasWarning("GETRF: info = " + std::to_string(info));     
   }

   ///////////////////////
   // save results
   ///////////////////////
   LU_struct<T> lu;
   lu.info = info;
   lu.m = m; lu.n = n;
   lu.a = std::unique_ptr<a_type[]>(std::move(a));
   lu.ipiv = std::move(ipiv);
   for(int i = 0; i < lu.Min(); ++i)
      lu.ipiv[i]-=1; // subtract 1 to go from fortran indexing to C indexing (we count from 0 instead of 1 in C)

   return lu;
}

/**
 * Midas Matrix interface
 **/
template<class T> 
LU_struct<T> GETRF(const GeneralMidasMatrix<T>& aMatrix)
{
   auto a = ColMajPtrFromMidasMatrix2(aMatrix);
   // we release both unique_ptr's as the lu struct will take ownership over these
   return GETRF(a.release(),aMatrix.Nrows(),aMatrix.Ncols());
}

#endif /* GETRF_H_INCLUDED */
