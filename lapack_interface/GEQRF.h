#ifndef GEQRF_H_INCLUDED
#define GEQRF_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <algorithm> // for std::min, std::max

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/QR_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

template<class T>
QR_struct<T> GEQRF(T* aA, int aM, int aN)
{
   using a_type = typename detail::QR_struct_traits<T>::a_type;
   using tau_type = typename detail::QR_struct_traits<T>::tau_type;

   ///////////////////////
   // setup
   ///////////////////////
   int m = aM;
   int n = aN;
   a_type* a = aA;
   int lda = std::max(1,m);
   std::unique_ptr<tau_type[]> tau(new tau_type[std::min(m,n)]);
   int lwork = std::max(std::max(1,n),n*1000); // fix 1000->optimal block size using ilaenv
   std::unique_ptr<a_type[]> work(new a_type[lwork]);
   int info;
   
   ///////////////////////
   // call geqrf_
   ///////////////////////
   midas::lapack_interface::geqrf_(&m,&n,a,&lda,tau.get(),work.get(),&lwork,&info);

   if(info != 0)
   {
      MidasWarning("GEQRF: info = " + std::to_string(info));
   }

   ///////////////////////
   // save results
   ///////////////////////
   QR_struct<T> qr;
   qr.info = info;
   qr.m = m; qr.n = n;
   qr.a = std::unique_ptr<a_type[]>(std::move(a));
   qr.tau = std::move(tau);

   return qr;
}

/**
 * Midas Matrix interface
 **/
template<class T> 
QR_struct<T> GEQRF(const GeneralMidasMatrix<T>& aMatrix)
{
   auto a = ColMajPtrFromMidasMatrix2(aMatrix);
   // we release both unique_ptr's as the qr struct will take ownership over these
   return GEQRF(a.release(),aMatrix.Nrows(),aMatrix.Ncols());
}

#endif /* GEQRF_H_INCLUDED */
