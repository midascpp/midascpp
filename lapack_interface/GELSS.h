#ifndef GELSS_H_INCLUDED
#define GELSS_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <algorithm> // for std::min, std::max

#include "lapack_interface/Fit_struct.h"
#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

/**
 *
 **/
template
   <  class T
   >
Fit_struct<T> GELSS
   (  T* aA
   ,  T* aB
   ,  int aM
   ,  int aN
   ,  int aNrhs
   ,  typename Fit_struct<T>::s_type aRcond
   )
{
   using a_type = typename Fit_struct<T>::a_type;
   using b_type = typename Fit_struct<T>::b_type;
   using s_type = typename Fit_struct<T>::s_type;
   
   ///////////////////////
   // setup all variables
   ///////////////////////
   int m = aM; // rows of A (input)
   int n = aN; // columns of A (input)
   int nrhs = aNrhs; // number of right-hand-sides (columns of B) (input)

   a_type* a = aA; // matrix A, on output right singular vectors of A (input/output)
   int lda = std::max(1,aM); // leading dimension of A (input)
   
   b_type* b = aB; // matrix/vector B, on output solution matrix X (input/output)
   int ldb = std::max(1,std::max(n,m)); // leading dimension of B (input)

   auto s = std::make_unique<s_type[]>(std::min(m,n)); // vector S (output)
   s_type rcond = aRcond; // condition number (input)
   int rank; // output rank (output)

   int lwork = std::max(1, std::max(3*n+std::max(std::max(2*n,nrhs),m), 3*m+std::max(std::max(2*m,nrhs),n)) );// dimension of workspace
   auto work = std::make_unique<T[]>(lwork); // workspace
   int info; // info (output)

   ///////////////////////
   // call lapack routine (through general wrapper)
   ///////////////////////
   midas::lapack_interface::gelss(&m,&n,&nrhs,a,&lda,b,&ldb,s.get(),&rcond,&rank,work.get(),&lwork,&info);
   if(info != 0)
   {
      MidasWarning("GELSS info=" + std::to_string(info));
   }
   
   ///////////////////////
   // load stuff to output struct
   ///////////////////////
   Fit_struct<T> fit;
   fit.info = info;
   fit.m_ = m; fit.n_ = n;
   fit.nrhs_ = nrhs; fit.rank_ = rank;
   fit.A_ = std::unique_ptr<a_type[]>(std::move(a));
   fit.B_ = std::unique_ptr<b_type[]>(std::move(b));
   fit.S_ = std::unique_ptr<s_type[]>(std::move(s));

   return fit;
}

/**
 *  interface for MidasMatrix, with multiple RHS
 **/
template<class T>
Fit_struct<T> GELSS
   (  const GeneralMidasMatrix<T>& aAmatrix
   ,  const GeneralMidasMatrix<T>& aBmatrix
   ,  typename Fit_struct<T>::s_type aRcond
   )
{
   auto a = ColMajPtrFromMidasMatrix2(aAmatrix);
   auto b = ColMajPtrFromMidasMatrix2(aBmatrix);
   // we release both unique_ptr's as the fit struct will take ownership over these
   return GELSS(a.release(),b.release(),aAmatrix.Nrows(),aAmatrix.Ncols(),aBmatrix.Ncols(),aRcond);
}

/**
 *  interface for MidasMatrix, with one rhs
 **/
template<class T>
Fit_struct<T> GELSS
   (  const GeneralMidasMatrix<T>& aAmatrix
   ,  const GeneralMidasVector<T>& aBmatrix
   ,  typename Fit_struct<T>::s_type aRcond
   )
{
   auto a = ColMajPtrFromMidasMatrix2(aAmatrix);
   auto b = PtrFromMidasVector(aBmatrix);
   // we release both unique_ptr's as the fit struct will take ownership over these
   return GELSS(a.release(),b.release(),aAmatrix.Nrows(),aAmatrix.Ncols(),1,aRcond);
}

#endif /* GELSS_H_INCLUDED */
