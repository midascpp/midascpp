#ifndef EIGENVALUEUTILS_H_INCLUDED
#define EIGENVALUEUTILS_H_INCLUDED

#include <utility>
#include "Eigenvalue_struct.h"

/**
 *
 **/
template<class T>
constexpr bool has_complex_eigenvalues(const Eigenvalue_struct<T>& eig_sol)
{
   return false;
}

/**
 *
 **/
template<class T>
inline bool has_complex_eigenvalues(const Eigenvalue_ext_struct<T>& eig_sol)
{
   for(int i=0; i<eig_sol.num_eigval_; ++i)
   {
      if(eig_sol.im_eigenvalues_[i] != T(0))
      {
         return true;
      }
   }
   return false;
}

//Sort eigenvalues/vectors according to eigenvalues
//in ascending/descending order.
template<class T>
void sort_eigenvalues(Eigenvalue_struct<T>& arEigSol, bool aAscending = true)
{
   if(aAscending)
   {
      for(In i=I_0; i<arEigSol._num_eigval; ++i)
      {
         //Locate minimal element.
         In min = i;
         for(In j=i+I_1; j<arEigSol._num_eigval; ++j)
         {
            if(arEigSol._eigenvalues[j] < arEigSol._eigenvalues[min])
            {
               min = j;
            }
         }
         
         //Swap.
         std::swap(arEigSol._eigenvalues[i], arEigSol._eigenvalues[min]);
         for(In k=I_0; k<arEigSol._n; ++k)
         {
            std::swap(arEigSol._eigenvectors[i][k], arEigSol._eigenvectors[min][k]);
         }
      }
   }
   else  //Descending.
   {
      for(In i=I_0; i<arEigSol._num_eigval; ++i)
      {
         //Locate maximal element.
         In max = i;
         for(In j=i+I_1; j<arEigSol._num_eigval; ++j)
         {
            if(arEigSol._eigenvalues[j] > arEigSol._eigenvalues[max])
            {
               max = j;
            }
         }
         
         //Swap.
         std::swap(arEigSol._eigenvalues[i], arEigSol._eigenvalues[max]);
         for(In k=I_0; k<arEigSol._n; ++k)
         {
            std::swap(arEigSol._eigenvectors[i][k], arEigSol._eigenvectors[max][k]);
         }
      }
   }
}

//Sort eigenvalues/vectors according to real part of eigenvalues
//in ascending/descending order.
//Note that by using the (strictly) greather/lesser than comparison operators
//we preserve the ordering of complex conjugate pairs.
//(In other words it's a stable sort.)
template<class T>
void sort_eigenvalues(Eigenvalue_ext_struct<T>& arEigSol, bool aAscending = true)
{
   if(aAscending)
   {
      for(In i=I_0; i<arEigSol.num_eigval_; ++i)
      {
         //Locate minimal element.
         In min = i;
         for(In j=i+I_1; j<arEigSol.num_eigval_; ++j)
         {
            if(arEigSol.re_eigenvalues_[j] < arEigSol.re_eigenvalues_[min])
            {
               min = j;
            }
         }
         
         //Swap.
         std::swap(arEigSol.re_eigenvalues_[i], arEigSol.re_eigenvalues_[min]);
         std::swap(arEigSol.im_eigenvalues_[i], arEigSol.im_eigenvalues_[min]);
         for(In k=I_0; k<arEigSol.n_; ++k)
         {
            std::swap(arEigSol.rhs_eigenvectors_[i][k], arEigSol.rhs_eigenvectors_[min][k]);
            std::swap(arEigSol.lhs_eigenvectors_[i][k], arEigSol.lhs_eigenvectors_[min][k]);
         }
      }
   }
   else  //Descending.
   {
      for(In i=I_0; i<arEigSol.num_eigval_; ++i)
      {
         //Locate maximal element.
         In max = i;
         for(In j=i+I_1; j<arEigSol.num_eigval_; ++j)
         {
            if(arEigSol.re_eigenvalues_[j] > arEigSol.re_eigenvalues_[max])
            {
               max = j;
            }
         }
         
         //Swap.
         std::swap(arEigSol.re_eigenvalues_[i], arEigSol.re_eigenvalues_[max]);
         std::swap(arEigSol.im_eigenvalues_[i], arEigSol.im_eigenvalues_[max]);
         for(In k=I_0; k<arEigSol.n_; ++k)
         {
            std::swap(arEigSol.rhs_eigenvectors_[i][k], arEigSol.rhs_eigenvectors_[max][k]);
            std::swap(arEigSol.lhs_eigenvectors_[i][k], arEigSol.lhs_eigenvectors_[max][k]);
         }
      }
   }
}

#endif /* EIGENVALUEUTILS_H_INCLUDED */
