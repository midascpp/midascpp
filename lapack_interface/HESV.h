#ifndef HESV_H_INCLUDED
#define HESV_H_INCLUDED

#include "lapack_interface/math_wrappers.h"
#include "LapackInterface.h"
#include "Linear_eq_struct.h"
#include "ColMajPtrFromMidasMatrix.h"
#include "util/type_traits/Complex.h"

/**
 * HESV solve A*x = B where A is Hermitian
 **/
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Linear_eq_struct<T> HESV
   (  T* arMatrix
   ,  T* brMatrix
   ,  int aN
   ,  int aNrhs
   ,  char aUplo
   )
{
   char uplo = aUplo; // this is maybe stupid :O (but consistent :P )
   int n=aN;          // this is maybe stupid :O
   int nrhs=aNrhs;    // this is maybe stupid :O
   int lda=std::max(1,aN);
   int ldb=std::max(1,aN);
   int info;
   auto ipiv = std::make_unique<int[]>(n);

   // Do workspace query for optimal lwork
   int lwork = -1;
   T work_opt;
   midas::lapack_interface::hesv(&uplo, &n, &nrhs, nullptr, &lda, nullptr, nullptr, &ldb, &work_opt, &lwork, &info);
   if (  info != 0
      )
   {
      MIDASERROR("HESV info = " + std::to_string(info) + " after workspace query!");
   }
   lwork = static_cast<int>(work_opt.real());
   auto work = std::make_unique<T[]>(std::max(1, lwork));

   // Solve the equations
   midas::lapack_interface::hesv(&uplo, &n, &nrhs, arMatrix, &lda, ipiv.get(), brMatrix, &ldb, work.get(), &lwork, &info);
   if(info != 0)
   {
      bool force = true;
      MidasWarning("HESV INFO = " + std::to_string(info), force);
   }
   
   // setup linear equation struct
   Linear_eq_struct<T> sol;
   sol.info = info;
   sol.n = aN;
   sol.n_rhs = aNrhs;
   sol.solution = std::make_unique<T[]>(aN*aNrhs);
   for(int i=0; i<aN*aNrhs; ++i)
   {
      sol.solution[i] = brMatrix[i];
   }
   
   return sol;
}

/**
 *
 **/
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Linear_eq_struct<T> HESV
   (  T** arMatrix
   ,  T** brMatrix
   ,  int aN
   ,  int aNrhs
   ,  char aUplo = 'U'
   )
{
   return HESV(arMatrix[0], brMatrix[0], aN, aNrhs, aUplo);
}

/**
 *
 **/
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Linear_eq_struct<T> HESV
   (  const GeneralMidasMatrix<T>& arMatrix
   ,  const GeneralMidasMatrix<T>& brMatrix
   ,  char aUplo = 'U'
   )
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   return HESV(a.get(), b.get(), arMatrix.Nrows(), brMatrix.Ncols(), aUplo);
}

/**
 * Overload for POSV for solving MidasMatrix*x = MidasVector
 **/
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Linear_eq_struct<T> HESV
   (  const GeneralMidasMatrix<T>& arMatrix
   ,  const GeneralMidasVector<T>& brVec
   ,  char aUplo = 'U'
   )
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = PtrFromMidasVector(brVec);

   return HESV(a.get(), b.get(), arMatrix.Nrows(), 1, aUplo);
}

#endif /* HESV_H_INCLUDED */
