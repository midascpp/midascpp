#ifndef SETMIDASMATRIXFROMCOLMAJPTR_H_INCLUDED
#define SETMIDASMATRIXFROMCOLMAJPTR_H_INCLUDED

#include "mmv/MidasMatrix.h"

template<class T>
void SetMidasMatrixFromColumnMajorPtr(GeneralMidasMatrix<T>& mat, T** const& ptr, size_t row, size_t col)
{
   using type = T;
   type** elements = new type*[row];
   for(size_t i=0; i<row; ++i)
   {
      elements[i] = new type[col];
      for(size_t j=0; j<col; ++j)
      {
         elements[i][j] = ptr[j][i];
      }
   }
   mat.SetData(elements,row,col);
}

template<class T>
void SetMidasMatrixFromColMajPtr2(GeneralMidasMatrix<T>& aMat, const std::unique_ptr<T[]>& aPtr, size_t row, size_t col)
{
   aMat.SetNewSize(In(row), In(col));
   unsigned counter = 0;
   for(In i=I_0; i<aMat.Ncols(); ++i)
   {
      for(In j=I_0; j<aMat.Nrows(); ++j)
      {
         aMat[j][i] = aPtr[counter];
         ++counter;
      }
   }
}

#endif /* SETMIDASMATRIXFROMCOLMAJPTR_H_INCLUDED */
