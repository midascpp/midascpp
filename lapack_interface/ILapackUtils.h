#ifndef ILAPACKUTILS_H_INCLUDED
#define ILAPACKUTILS_H_INCLUDED

#include <utility>
#include <memory>
#include "libmda/util/multiple_return.h"
#include "libmda/numeric/float_eq.h"
#include "libmda/numeric/signum.h"
#include "util/Error.h"
#include "util/UnderlyingType.h"
#include "util/type_traits/TypeName.h"


namespace ilapack
{
namespace util
{



/**
 * Enumerator utilities (copied from TdvccEnums.h)
 **/
//!@{
template<class ENUM>
const std::map<std::string,ENUM>& GetMapStringToEnum()
{
   static_assert(!std::is_same_v<ENUM,ENUM>, "Can only call specializations of this.");
   static std::map<std::string,ENUM> m;
   return m;
}

template<class ENUM>
ENUM EnumFromString(const std::string& s)
{
   try
   {
      return GetMapStringToEnum<ENUM>().at(s);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Key string '" + s + "' not found for "+midas::type_traits::TypeName<ENUM>()+" enum.");
      return ENUM();
   }
}

template<class ENUM>
std::string StringFromEnum(ENUM e)
{
   for(const auto& kv: GetMapStringToEnum<ENUM>())
   {
      if (kv.second == e)
      {
         return kv.first;
      }
   }
   MIDASERROR(midas::type_traits::TypeName<ENUM>() + " enum value '" + std::to_string(midas::util::ToUType(e)) + "' not found.");
   return "";
}
//!@} 

//@{
//! Lapack routine names (not exhaustive - add more as needed)
enum class RoutineName
{  GESDD
,  GESVD
,  GETRF
,  GETRI
,  TRTRI
};
template<> inline const std::map<std::string, RoutineName>& GetMapStringToEnum()
{
   static std::map<std::string, RoutineName> m;
   m["GESDD"] = RoutineName::GESDD;
   m["GESVD"] = RoutineName::GESVD;
   m["GETRF"] = RoutineName::GETRF;
   m["GETRI"] = RoutineName::GETRI;
   m["TRTRI"] = RoutineName::TRTRI;
   return m;
}
//@}


/*
 *
 */
template<class T>
inline auto squared(T t) -> decltype(t*t)
{
   return t*t;
}

/*
 *
 */
template<class T>
libmda::util::return_type<T,T> complex_conjugate(T re, T im)
{
   return libmda::util::ret(re,-im);
}

/*
 *
 */
template<class T> 
T complex_norm(T re, T im)
{
   return sqrt(ilapack::util::squared(re) + ilapack::util::squared(im));
}

/*
 *
 */
template<class T>
libmda::util::return_type<T,T> complex_sqrt(T re, T im)
{
   T norm = ilapack::util::complex_norm(re,im);
   
   T re_sqrt = sqrt((norm + re)/T(2));
   T im_sqrt = libmda::numeric::signum(im)*sqrt((norm-re)/T(2));

   return libmda::util::ret(re_sqrt,im_sqrt);
}

/*
 *
 */
template<class T>
libmda::util::return_type<T,T> complex_squared(T re, T im)
{
   T re_squared = ilapack::util::squared(re) - ilapack::util::squared(im);
   T im_squared = 2*re*im;

   return libmda::util::ret(re_squared,im_squared);
}

/*
 *
 */
template<class T, class I>
T column_norm_squared_offset(T* t, I size, I offset)
{
   T norm_sqr = 0;
   for(I i=0; i<size; ++i)
   {  
      norm_sqr+=ilapack::util::squared(t[offset]);
      ++offset;  
   }
   return norm_sqr;
}

/*
 *
 */
template<class T, class U, class I>
void column_scale_offset(T* t, const U& scal, I size, I offset)
{
   for(I i=0; i<size; ++i)
   {
      t[offset]*=scal;
      ++offset;
   }
}

/*
 *
 */
template<class T, class U, class I>
void column_inv_scale_offset(T* t, const U& scal, I size, I offset)
{
   U inv_scal = U(1)/scal;
   ilapack::util::column_scale_offset(t,inv_scal,size,offset);
}

/*
 *
 */
template<class T, class U, class I>
void column_inv_scale_complex_offset(T* t, const U& re_scal, const U& im_scal, I size, I offset)
{
   I offset2 = offset;
   I im_offset = offset + size;
   I im_offset2 = im_offset;

   U inv_norm_sqr = 1.0/(ilapack::util::squared(re_scal) + ilapack::util::squared(im_scal));
   
   std::unique_ptr<T[]> re_temp(new T[size]);
   std::unique_ptr<T[]> im_temp(new T[size]);
   
   for(I i=0; i<size; ++i)
   {
      re_temp[i] = (re_scal*t[offset] + im_scal*t[im_offset])*inv_norm_sqr;
      im_temp[i] = (-im_scal*t[offset] + re_scal*t[im_offset])*inv_norm_sqr;
      ++offset;
      ++im_offset;
   }

   for(I i=0;i<size; ++i)
   {
      t[offset2] = re_temp[i];
      t[im_offset2] = im_temp[i];
      ++offset2;
      ++im_offset2;
   }
}

/*
 *
 */
template<class L, class R, class I>
auto column_dot_offset(L* l, R* r, I size, I offset)
   -> decltype(std::declval<L>()*std::declval<R>())
{
   decltype(std::declval<L>()*std::declval<R>()) dot = 0.0;
   for(I i=0; i<size; ++i)
   {
      dot+=l[offset]*r[offset];
      ++offset;
   }
   return dot;
}

/*
 *
 */
template<class L, class R, class I>
auto column_dot_offset2(L* l, R* r, I size, I offset1, I offset2)
   -> decltype(std::declval<L>()*std::declval<R>())
{
   decltype(std::declval<L>()*std::declval<R>()) dot = 0.0;
   for(I i=0; i<size; ++i)
   {
      dot+=l[offset1]*r[offset2];
      ++offset1;
      ++offset2;
   }
   return dot;
}

/*
 *
 */
template<class T, class U, class I>
void columns_complex_conjugate(T* t, U* const u, I n, I m)
{
   for(I i=0; i<m; ++i)
   {
      if(u[i])
      {
         // complex pair
         I mat_idx_im = (i+1)*n;
         for(I j=0; j<n; ++j)
         {
            t[mat_idx_im] = -t[mat_idx_im];
            ++mat_idx_im;
         }
         ++i;
      }
   }
}


//
// normalize columns of colunm major nxm mat t
//
template<class T, class I>
void normalize_columns(T* t, I n, I m)
{
   I mat_idx = 0;
   for(I i=0; i<m; ++i)
   {
      T dot = column_dot_offset(t,t,n,mat_idx);

      if(libmda::numeric::float_neq(dot,0.0,8))
      {
         column_inv_scale_offset(t,sqrt(dot),n,mat_idx);
      }
      else
      {
         std::cout << " THIS IS ERROR " << std::endl;
         exit(40);
      }

      mat_idx+=n;
   }
}

//
// normalize columns of colunm major nxm mat t
// where u indicates complex pair
//
template<class T, class U, class I>
void normalize_columns_complex(T* t, U* u, I n, I m)
{
   T norm;
   I mat_idx = 0;

   for(I i=0; i<m; ++i)
   {
      if(!u[i])
      {
         // not complex
         auto norm = column_dot_offset(t,t,n,mat_idx);
         
         column_inv_scale_offset(t,sqrt(norm),n,mat_idx);
         
         mat_idx+=n;
      }
      else
      {
         // complex pair
         I mat_idx_im = mat_idx + n;

         auto re_dot = column_dot_offset(t,t,n,mat_idx) + column_dot_offset(t,t,n,mat_idx_im);
         
         norm = sqrt(re_dot);
         column_inv_scale_offset(t,norm,n,mat_idx);
         column_inv_scale_offset(t,norm,n,mat_idx_im);
         
         mat_idx+=2*n;
         ++i;
      }
   }
}

//
// will change l
//
template<class L, class R, class U, class I>
void binormalize_columns_unbalanced(L* l, R* r, U* const u, I n, I m)
{
   I mat_idx = 0;
   for(I i=0; i<m; ++i)
   {
      if(!u[i])
      {
         // not complex
         auto dot = column_dot_offset(l,r,n,mat_idx);
         column_inv_scale_offset(l,dot,n,mat_idx);

         mat_idx+=n;
      }
      else
      {
         // complex pair
         I mat_idx_im = mat_idx + n;

         auto re_dot = column_dot_offset(l,r,n,mat_idx) - column_dot_offset(l,r,n,mat_idx_im);
         auto im_dot = column_dot_offset2(l,r,n,mat_idx,mat_idx_im) + column_dot_offset2(l,r,n,mat_idx_im,mat_idx);
      
         column_inv_scale_complex_offset(l,re_dot,im_dot,n,mat_idx);

         mat_idx += 2*n;
         ++i;
      }
   }
}

/*
 *
 */
template<class L, class R, class U, class I>
void binormalize_columns_balanced(L* l, R* r, U* u, I n, I m)
{
   I mat_idx = 0;
   for(I i=0; i<m; ++i)
   {
      if(!u[i]) // check for complex pair
      {
         // not complex
         auto dot = column_dot_offset(l,r,n,mat_idx);
         
         if(libmda::numeric::float_pos(dot))
         {  // dot positive
            auto dot_sqrt = sqrt(dot);
            column_inv_scale_offset(l,dot_sqrt,n,mat_idx);
            column_inv_scale_offset(r,dot_sqrt,n,mat_idx);
         }
         else
         { // dot negative
            auto dot_sqrt = sqrt(fabs(dot));
            column_inv_scale_offset(l,-dot_sqrt,n,mat_idx);
            column_inv_scale_offset(r,dot_sqrt,n,mat_idx);
         }

         mat_idx+=n;
      }
      else
      {
         // complex pair
         I mat_idx_im = mat_idx + n;
         
         auto re_dot = column_dot_offset(l,r,n,mat_idx) - column_dot_offset(l,r,n,mat_idx_im);
         auto im_dot = column_dot_offset2(l,r,n,mat_idx,mat_idx_im) + column_dot_offset2(l,r,n,mat_idx_im,mat_idx);
      
         decltype(re_dot) re_sqrt;
         decltype(im_dot) im_sqrt;
         libmda::util::ret(re_sqrt,im_sqrt) = ilapack::util::complex_sqrt(re_dot,im_dot);
         
         column_inv_scale_complex_offset(l,re_sqrt,im_sqrt,n,mat_idx);
         column_inv_scale_complex_offset(r,re_sqrt,im_sqrt,n,mat_idx);

         mat_idx += 2*n;
         ++i;
      }
   }
}

} /* namespace util */
} /* namespace ilapack */

#endif /* ILAPACKUTILS_H_INCLUDED */
