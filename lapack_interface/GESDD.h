#ifndef GESDD_H_INCLUDED
#define GESDD_H_INCLUDED

#include <memory>
#include <algorithm> // for std::max, std::min

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/SVD_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"

// Forward declarations
template <class T>
struct SVD_struct;

namespace detail 
{

/**
 * Helper function to determine correct lwork size.
 **/
inline int lwork_switch_one(int m, int n, char jobz)
{
   switch(jobz) 
   {
      case 'N':
         return 7*n + n;
      case 'O':
         return 3*n*n + 4*n + 2*n*n + 3*n;
      case 'S':
      case 'A':
         return 3*n*n + 4*n + n*n + 3*n;
      default:
         return 0;
   }
}

/**
 * Helper function to determine correct lwork size.
 **/
inline int lwork_switch_two(int m, int n, char jobz)
{
   switch(jobz) 
   {
      case 'N':
         return 3*n + std::max(m,7*n);
      case 'O':
         return 3*n + std::max(m, n*n + (3*n*n + 4*n));
      case 'S':
      case 'A':
         return 3*n + std::max(m, 3*n*n + 4* n);
      default:
         return 0;
   }
}

/**
 * Function to determine correct lwork size.
 **/
inline int gesdd_get_lwork(int m, int n, char jobz)
{
   if(m >= std::floor(n*11/6))
   {
      return lwork_switch_one(m,n,jobz);
   }
   else if(m >= n)
   {
      return lwork_switch_two(m,n,jobz);
   }
   else if(n >= std::floor(m*11/6))
   {
      return lwork_switch_one(n,m,jobz);
   }
   else if(n >= m)
   {
      return lwork_switch_two(n,m,jobz);
   }
   else
   {  
      MIDASERROR("something worng");
      return -1; /* will never get here */
   }
}

} /* namespace detail */

/**
 * @brief GESDD computes SVD of matrix using a divide and conquer algorithm.
 *
 * REAL VERSIONS:
 * ~~~
 *    GESDD computes the singular value decomposition (SVD) of a real
 *    M-by-N matrix A, optionally computing the left and right singular
 *    vectors.  If singular vectors are desired, it uses a
 *    divide-and-conquer algorithm.
 *
 *    The SVD is written
 *
 *         A = U * SIGMA * transpose(V)
 *
 *    where SIGMA is an M-by-N matrix which is zero except for its
 *    min(m,n) diagonal elements, U is an M-by-M orthogonal matrix, and
 *    V is an N-by-N orthogonal matrix.  The diagonal elements of SIGMA
 *    are the singular values of A; they are real and non-negative, and
 *    are returned in descending order.  The first min(m,n) columns of
 *    U and V are the left and right singular vectors of A.
 *
 *    Note that the routine returns VT = V**T, not V.
 * ~~~
 *
 * COMPLEX VERSIONS:
 * ~~~
 *    ZGESVD computes the singular value decomposition (SVD) of a complex
 *    M-by-N matrix A, optionally computing the left and/or right singular
 *    vectors.
 *
 *    The SVD is written
 *
 *         A = U * SIGMA * conjugate-transpose(V)
 *
 *    where SIGMA is an M-by-N matrix which is zero except for its
 *    min(m,n) diagonal elements, U is an M-by-M unitary matrix, and
 *    V is an N-by-N unitary matrix.  The diagonal elements of SIGMA
 *    are the singular values of A; they are real and non-negative, and
 *    are returned in descending order.  The first min(m,n) columns of
 *    U and V are the left and right singular vectors of A.
 *
 *    Note that the routine returns V**H, not V.
 * ~~~
 *
 * @param arMatrix    Pointer to MxN matrix in column major format (we call fortran-type lapack routines!).
 *                    !!NB!! pointer will be destroyed!
 * @param aM          Number of rows.
 * @param aN          Number of cols.
 * @param aType       Specifies options for computing all or part of the matrix U:
 *                      'A':  all M columns of U and all N rows of V**T are
 *                            returned in the arrays U and VT;
 *            (default) 'S':  the first min(M,N) columns of U and the first
 *                            min(M,N) rows of V**T are returned in the arrays U
 *                            and VT;
 *                      'O':  If M >= N, the first N columns of U are overwritten
 *                            on the array A and all rows of V**T are returned in
 *                            the array VT;
 *                            otherwise, all columns of U are returned in the
 *                            array U and the first M rows of V**T are overwritten
 *                            in the array A;
 *                      'N':  no columns of U or rows of V**T are computed.
 *
 * @return           Return SVD wrapped in special SVD_struct.
 **/
template<class T>
SVD_struct<T> GESDD(T* arMatrix, int aM, int aN, char aType='S')
{
   using a_type = typename SVD_struct<T>::a_type;
   using s_type = typename SVD_struct<T>::s_type;
   using vt_type = typename SVD_struct<T>::vt_type;
   using u_type = typename SVD_struct<T>::u_type;

   ///////////////////////
   // setup
   ///////////////////////
   char jobz = aType;
   int lda = aM;
   std::unique_ptr<s_type[]> s(new s_type[min(aM,aN)]);
   int ldu = aM;
   int ucol;
   if(jobz == 'A')
   {
      ucol = aM;
   }
   else
   {
      ucol = min(aM,aN);
   }
   std::unique_ptr<u_type[]> u(new u_type[ldu*ucol]);
   int ldvt;
   if(jobz == 'A')
   {
      ldvt = aN;
   }
   else
   {
      ldvt = min(aN,aM);
   }
   std::unique_ptr<vt_type[]> vt(new vt_type[ldvt*aN]);
   int lwork = max(10000, detail::gesdd_get_lwork(aM,aN,jobz));
   std::unique_ptr<a_type[]> work(new a_type[lwork]);
   std::unique_ptr<int[]> iwork(new int[8*min(aM,aN)]);
   int info;

   ///////////////////////
   // call gesdd_
   ///////////////////////
   midas::lapack_interface::gesdd(&jobz,&aM,&aN,arMatrix,&lda,s.get(),u.get(),&ldu,vt.get(),&ldvt,work.get(),&lwork,iwork.get(),&info);
   
   if(info != 0)
   {
      MidasWarning("GESDD: info = " + std::to_string(info));
   }

   ///////////////////////
   // save results
   ///////////////////////
   SVD_struct<T> svd;
   svd.info = info;
   svd.m = aM; 
   svd.n = aN;
   svd.u  = std::move(u);
   svd.vt = std::move(vt);
   svd.s  = std::move(s);
   //svd.u = new u_type*[ucol];
   //for(int i=0; i<ucol; ++i)
   //   svd.u[i] = u .get()+ i*aM;
   //u.release(); // release u, so we do not deallocate it
   //svd.vt = new vt_type*[aN];
   //for(int i=0; i<aN; ++i)
   //   svd.vt[i] = vt.get() + i*ldvt;
   //vt.release(); // release vt, so we do not deallocate it 
   //svd.s = s.release(); // 
   
   return svd;
}

/**
 * Same as GESDD for T*, will just assume that whole pointer is stored consecutively after arMatrix[0].
 *
 * @param arMatrix   Pointer to MxN matrix in column major format (we call fortran-type lapack routines!).
 *                    !!NB!! pointer will be destroyed!
 * @param aM         Number of rows.
 * @param aN         Number of cols.
 * @param aType      Specifies options for computing all or part of the matrix U (see other GESDD for detailed description).
 *
 * @return           Return SVD wrapped in special SVD_struct.
 **/
template<class T>
SVD_struct<T> GESDD(T** arMatrix, int aM, int aN, char aType='S')
{
   return GESDD(arMatrix[0],aM,aN,aType);
}

/**
 * Midas matrix interface to GESDD. Will copy MidasMatrix to a pointer in column major format.
 *
 * @param arMatrix   The MidasMatrix to calculate the SVD for.
 * @param aType      Specifies options for computing all or part of the matrix U (see other GESDD for detailed description).
 * 
 * @return           Return SVD wrapped in special SVD_struct.
 **/
template<class T>
SVD_struct<T> GESDD(const GeneralMidasMatrix<T>& arMatrix, char aType='S')
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   // we get a, as svd struct does NOT take ownership, so its needs to be deallocated afterwards
   return GESDD(a.get(), arMatrix.Nrows(), arMatrix.Ncols(), aType);
}

#endif /* GESDD_H_INCLUDED */
