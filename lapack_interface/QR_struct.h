#ifndef QR_STRUCT_H_INCLUDED
#define QR_STRUCT_H_INCLUDED

#include <memory>
#include <algorithm>

#include "libmda/numeric/float_eq.h"

#include "util/MidasStream.h"
extern MidasStream Mout;

namespace detail
{

template<class T> struct QR_struct_traits
{
   using a_type = T;
   using tau_type = T;
};
 
} /* namespace detail */

template<class T> struct QR_struct 
{
   using a_type = typename detail::QR_struct_traits<T>::a_type;
   using tau_type = typename detail::QR_struct_traits<T>::tau_type;
  
   int info = -1; 
   int m, n;
   std::unique_ptr<a_type[]> a;
   std::unique_ptr<tau_type[]> tau;

   int Min() const { return std::min(m,n); }
   a_type Determinant() const;
};


template<class T>
typename QR_struct<T>::a_type QR_struct<T>::Determinant() const
{
   // do some checks
   MidasAssert(m == n, "Asking for determinant of non-square matrix.");
   MidasAssert(m > 0 && n > 0, "Asking for determinant of 0x0 matrix.");
   // calculate determinant det(A) = det(Q)*det(R) = sign()*det(R) = sign() \prod_i r_ii
   // where sign() depends on number of householder reflectors used when constructing Q = H(0)*...*H(k), k = min(m,n)
   // (tau can be zero!)
   a_type determinant = a_type(1.0);
   for(int i = 0; i < n; ++i)
   {
      determinant *= a[i + i*n];
   }
   
   // calculate sign (odd number of HouseHolder reflectors gives negative sign)
   int counter = 0;
   for(int i = 0; i < Min(); ++i)
   {
      if(tau[i] != a_type(0.0)) ++counter;
   }
   
   // return signed determinant
   return (counter%2 ? -a_type(1.0) : a_type(1.0))*determinant; 
}

template<class T> 
//typename QR_struct<T>::a_type LoadA(const QR_struct<T>& qr, GeneralMidasMatrix<T>& mat)
void LoadA(const QR_struct<T>& qr, GeneralMidasMatrix<T>& mat)
{
   mat.SetNewSize(In(qr.m), In(qr.n));
   int counter = 0;
   //for(int i = 0; i < qr.m*qr.n; ++i)
   //   Mout << " qr[" << i << "] = " << qr.a[i] << std::endl;
   //for(int i = 0; i < qr.n; ++i)
   //   Mout << " tau[" << i << "] = " << qr.tau[i] << std::endl;

   // construct R-matrix
   for(int j = 0; j <  mat.Ncols(); ++j)
   {
      for(int i = 0; i < mat.Nrows(); ++i)
      {
         i <= j ? mat[i][j] = qr.a[counter] : mat[i][j] = C_0;
         ++counter;
      }
   }

   //Mout << " MAT R: " << std::endl << mat << std::endl;

   // apply the n-householder reflections
   GeneralMidasMatrix<T> temp_mat(mat.Nrows(),mat.Ncols());
   GeneralMidasVector<T> temp_vec(mat.Ncols());
   for(int i = qr.n-1; i >= 0; --i) // loop over transformations
   {
      int col_idx = i*qr.m;
      for(int k = 0; k < mat.Ncols(); ++k)
      {
         temp_vec[k] = mat[i][k];
         for(int l = i + 1; l < mat.Nrows(); ++l) // constraction
         {
            temp_vec[k] += qr.a[col_idx + l]*mat[l][k];
         }
      }
      //Mout << " TEMP VEC: " << std::endl << temp_vec << std::endl;
      for(int k = 0; k < mat.Ncols(); ++k)
      {
         for(int j = 0; j < mat.Nrows(); ++j) // loop over matrix elements
         {
            temp_mat[j][k] = temp_vec[k];
            if(j == i) temp_mat[j][k] *= qr.tau[i];
            else if(j > i) temp_mat[j][k] *= qr.tau[i]*qr.a[col_idx + j]; // identity
            else temp_mat[j][k] = C_0;
         }
      }
      //Mout << " TEMP MAT: " << std::endl << temp_mat << std::endl;
      //for(int j = 0; j < mat.Nrows(); ++j) // loop over matrix elements
      //   for(int k = 0; k < mat.Ncols(); ++k)
      //      temp_mat[j][k] += mat[j][k]; // identity
      //std::swap(mat,temp_mat);
      //mat = std::move(temp_mat);
      //Mout << " MAT BEFORE: " << std::endl << mat << std::endl;
      mat -= temp_mat;
      //Mout << " MAT AFTER: " << std::endl << mat << std::endl;
   }
}

#endif /* QR_STRUCT_H_INCLUDED */
