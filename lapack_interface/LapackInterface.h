/**
************************************************************************
* 
* @file                LapackInterface.h
*
* Created:             07-11-2012
*
* Author:              Ian H. Godtleibsen (ian@chem.au.dk)
*
* Short Description:   
* 
* Last modified: Tue May 12, 2012  10:51AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LAPACKINTERFACE_H_INCLUDED
#define LAPACKINTERFACE_H_INCLUDED

// std headers
#include <limits>
#include <complex>

// midas headers
#include "mmv/MidasMatrix.h"
#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/EigenvalueUtils.h"
#include "lapack_interface/Linear_eq_struct.h"
#include "lapack_interface/ILapack.h"

////////////////////////////////////////////////////////////////////////////////////////////
// Singular Value Decomposition (SVD) 
////////////////////////////////////////////////////////////////////////////////////////////
/**
 * A = U*E*V^T
 **/
// general
#include "lapack_interface/GESVD.h"

// general divivde-and-conquer
#include "lapack_interface/GESDD.h"

////////////////////////////////////////////////////////////////////////////////////////////
// Eigenvalue equations
////////////////////////////////////////////////////////////////////////////////////////////
/**
 * 1: A*x = lambda*B*y (A and B symmetric) 
 * 2: A*B*x = lambda*y (A and B symmetric) 
 * 3: B*A*x = lambda*y (A and B symmetric) 
 **/
// standard
#include "lapack_interface/SYGV.h"
// divide-and-conquer
#include "lapack_interface/SYGVD.h"

/**
 * A*x = lambda*x (A Symmetric)
 **/
// divide-and-conquer
#include "lapack_interface/SYEVD.h"

/**
 * A*x = lambda*x (A General)
 **/
// general (only float/double for now, no complex versions yet)
#include "lapack_interface/GEEV.h"

/**
 * A*x = lambda*B*x (A and B General)
 * or equally mu*A*x = B*x (A and B General)
 **/
// general (only float/double for now, no complex versions yet)
#include "lapack_interface/GGEV.h"

////////////////////////////////////////////////////////////////////////////////////////////
// Linear equations
////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  solve A*X = B where A is symmetric positive definite N x N and B is general N x NRHS
 **/
#include "lapack_interface/POSV.h"

/**
 *  solve A*X = B where A is general N x N and B is general N x NRHS
 **/
#include "lapack_interface/GESV.h"

////////////////////////////////////////////////////////////////////////////////////////////
// Matrix decompositions
////////////////////////////////////////////////////////////////////////////////////////////
// QR decompositions
#include "lapack_interface/GEQRF.h" // QR decomposition of general matrix
// LU decompositions
#include "lapack_interface/GETRF.h" // LU decomposition of general matrix

////////////////////////////////////////////////////////////////////////////////////////////
// MISC.
////////////////////////////////////////////////////////////////////////////////////////////
#include "lapack_interface/GELSS.h" // solve linear least squares problems
#include "lapack_interface/GETRI.h" // invert matrix based on LU decomposition


#endif // LAPACKINTERFACE_H_INCLUDED
