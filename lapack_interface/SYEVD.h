#ifndef SYEVD_H_INCLUDED
#define SYEVD_H_INCLUDED

#include <memory> // for std::unique_ptr

#include "util/type_traits/Complex.h"

#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"

/**
 * SYEVD
 **/
// SYEVD: Divide-and-conquer
// computes eigenvalues for:
//    A*x=lambda*x
// where A is an N*N real symmetric matrix saved in column major format!
// define either userfriendly or efficient (not both :O ).
#define USERFRIENDLY_SYEVD
//#define EFFICIENT_SYEVD
template
   <  typename T
   ,  typename = std::enable_if_t<!midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_struct<T> SYEVD(T* arMatrix, int aN, char aJobz = 'V', char aUplo = 'U')
{
   char jobz = aJobz;  // a little extra work but adds some clarity...
   char uplo = aUplo;
   int lda = max(1,aN);
   int ldb = max(1,aN);
   std::unique_ptr<T[]> w(new T[aN]);
   int lwork = max(10000,1+6*aN+2*aN*aN); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work(new T[lwork]);
   int liwork = max(10000,3+5*aN);
   std::unique_ptr<int[]> iwork(new int[liwork]);
   int info;
   
   //cout << " Doing SYEVD " << endl;
   midas::lapack_interface::syevd_(&jobz, &uplo, &aN, arMatrix, &lda, w.get(), work.get(), &lwork, iwork.get(), &liwork, &info);
   if(info != 0)
   {
      MidasWarning("SYEVD INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   Eigenvalue_struct<T> eigen;
   eigen._info = info;
   eigen._n = aN;
   eigen._num_eigval = aN;
   eigen._eigenvalues = w.release();
   eigen._eigenvectors = new T*[aN];
#ifdef USERFRIENDLY_SYEVD // userfriendly but less efficient
   T* p = new T[aN*aN];
   for(int i=0; i<aN*aN; ++i)
      p[i] = arMatrix[i];
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = p + i*aN;
#endif /* USERFRIENDLY_SYEVD */
#ifdef EFFICIENT_SYEVD // more efficient but less user friendly as arMatrix is stored in "eigen"
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = arMatrix + i*aN;
   //arMatrix = nullptr;
#endif /* EFFICIENT_SYEVD */

   return eigen;
}

/**
 *
 **/
template<class T>
Eigenvalue_struct<T> SYEVD(const GeneralMidasMatrix<T>& arMatrix, char aJobz = 'V', char aUplo = 'U')
{
   //assert(Is_symmetric(arMatrix))
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0);
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);

   // Do SYEVD
   return SYEVD(a.get(),arMatrix.Ncols(),aJobz,aUplo);
}

#endif /* SYEVD_H_INCLUDED */
