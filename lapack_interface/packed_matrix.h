
/**
 *************************************************************************
 * 
 * @file                packed_matrix.h
 *
 * Created:             27.10.2016
 *
 * Author:              Gunnar Schmitz (Gunnar.Schmitz@chem.au.dk)
 *
 * \brief               Contains low level functions to operate on
 *                      matrices in packed form
 * 
 * Detailed Description: These are mainly translated Fortran routines
 *                       to do for instance matrix multiplications
 *                       with symmetrical packed matrices. 
 *                       (Matrix saved as column major)
 *
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 *************************************************************************
 **/

#ifndef PACKED_MATRIX_H_INCLUDED
#define PACKED_MATRIX_H_INCLUDED

#include<algorithm>
#include<stdio.h>
#include<iostream>
#include<stdexcept>

#include "lapack_interface/math_wrappers.h" 

void getstrp(double* as, double* a, int is, int ie, int n, int nbmax);
void smdgm(char side, double* a, double* s, double* b, double* scr, int n, double* alpha, double* beta, int md, int nrow);
void matmulpak(char side, int a, int m, double* x, double* b, double* c, double* alpha, double* beta, double* scr1, int nmmax, int md);
void matprt(double* vec, int ndim1, int ndim2);
void symmatprt(double* d, int n);

#endif 
