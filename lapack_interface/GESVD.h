#ifndef GESVD_H_INCLUDED
#define GESVD_H_INCLUDED

#include <memory>
#include <algorithm>

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/SVD_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"

// Forward declarations
template <class T>
struct SVD_struct;

/**
 * SVD (gesvd)
 **/
template<class T>
SVD_struct<T> GESVD(T* arMatrix, int aM, int aN, char aType='S')
{
   using a_type = typename SVD_struct<T>::a_type;
   using s_type = typename SVD_struct<T>::s_type;
   using vt_type = typename SVD_struct<T>::vt_type;
   using u_type = typename SVD_struct<T>::u_type;

   ///////////////////////
   // setup
   ///////////////////////
   char jobu = aType;
   char jobvt = aType;
   int lda = aM;
   std::unique_ptr<s_type[]> s(new s_type[std::min(aM,aN)]);
   int ldu = aM;
   int ucol;
   if(jobu == 'A')
      ucol = aM;
   else
      ucol = std::min(aM,aN);
   std::unique_ptr<u_type[]> u(new u_type[ldu*ucol]);
   int ldvt;
   if(jobvt == 'A')
      ldvt = aN;
   else
      ldvt = std::min(aM,aN);
   std::unique_ptr<vt_type[]> vt(new vt_type[ldvt*aN]);
   int lwork = std::max(std::max(10000,3*std::min(aM,aN)+max(aM,aN)),5*std::min(aM,aN));
   std::unique_ptr<a_type[]> work(new a_type[lwork]);
   int info;

   ///////////////////////
   // call gesvd
   ///////////////////////
   midas::lapack_interface::gesvd(&jobu,&jobvt,&aM,&aN,arMatrix,&lda,s.get(),u.get(),&ldu,vt.get(),&ldvt,work.get(),&lwork,&info);

   if(info != 0)
   {
      MidasWarning("GESVD: info = " + std::to_string(info));
   }

   ///////////////////////
   // save result
   ///////////////////////
   SVD_struct<T> svd;
   svd.info = info;
   svd.m = aM; 
   svd.n = aN;
   svd.u  = std::move(u);
   svd.vt = std::move(vt);
   svd.s  = std::move(s);
   //svd.u = new u_type*[ucol];
   //for(int i=0; i<ucol; ++i)
   //   svd.u[i] = u.get() + i*aM;
   //u.release(); // release u, so we do not deallocate it
   //svd.vt = new vt_type*[aN];
   //for(int i=0; i<aN; ++i)
   //   svd.vt[i] = vt.get() + i*ldvt;
   //vt.release(); // release vt, so we do not deallocate it
   //svd.s = s.release(); // release s, so we do not deallocate it

   return svd;
}

template<class T>
SVD_struct<T> GESVD(T** arMatrix, int aM, int aN, char aType='S')
{
   return GESVD(arMatrix[0], aM, aN, aType);
}

/**
 * Midas Matrix interface 
 **/
template<class T>
SVD_struct<T> GESVD(const GeneralMidasMatrix<T>& arMatrix, char aType='S')
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   return GESVD(a.get(), arMatrix.Nrows(), arMatrix.Ncols(), aType);
}

#endif /* GESVD_H_INCLUDED */
