#ifndef LU_STRUCT_H_INCLUDED
#define LU_STRUCT_H_INCLUDED

#include <memory>
#include <algorithm>

#include "util/Error.h"

namespace detail
{

template<class T> struct LU_struct_traits
{
   using a_type = T;
   using ipiv_type = int;
};
 
} /* namespace detail */

/**
 * a: LU 
 *
 * ipiv: pivot
 *
 **/

template<class T> struct LU_struct {
   using a_type = typename detail::LU_struct_traits<T>::a_type;
   using ipiv_type = typename detail::LU_struct_traits<T>::ipiv_type;
 
   int info = -1;  
   int m, n;
   std::unique_ptr<a_type[]> a;
   std::unique_ptr<ipiv_type[]> ipiv;

   int Min() const { return std::min(m,n); }
   a_type Determinant() const;
};

template<class T>
typename LU_struct<T>::a_type LU_struct<T>::Determinant() const
{
   // do some checks
   MidasAssert(m == n, "Asking for determinant of non-square matrix.");
   MidasAssert(m > 0 && n > 0, "Asking for determinant of 0x0 matrix.");
   // calculate determinant det(A) = sign()*det(L)*det(U) = sign()*det(U) = sign() \prod_i u_ii
   // where sign() depends on number of row swaps determined by ipiv
   a_type determinant = a_type(1.0);
   for(In i = I_0; i < n; ++i)
   {
      determinant *= a[i + i*n];
   }
   // calculate sign (odd number of swaps means negative sign)
   In counter = I_0;
   for(In i = I_0; i < Min(); ++i)
   {
      if(ipiv[i] == i) // if ipiv == i   =>   no swap is done
         break;
      ++counter;
   }
   return a_type((counter%2 ? -1.0 : 1.0))*determinant;
}

/**
 *
 **/
template<class T>
void LoadA(const LU_struct<T>& lu, GeneralMidasMatrix<typename LU_struct<T>::a_type>& mat)
{
   mat.SetNewSize(In(lu.m), In(lu.n), false);
   // calcualte A mat
   for(In i = I_0; i < lu.m; ++i)
   {
      for(In j = I_0; j < lu.n; ++j)
      {
         mat[i][j] = C_0;
         for(In k = I_0; k <= j && k < i; ++k)
         {
            mat[i][j] += lu.a[i + k*lu.m] * lu.a[j*lu.m + k];
         }
         if(j >= i) mat[i][j] += lu.a[j*lu.m + i];
      }
   }
   // swap rows according to ipiv
   for(In i = In(lu.Min()-I_1); i>=I_0; --i)
   {
      for(In j = I_0; j < lu.n; ++j)
      {
         std::swap(mat[i][j],mat[lu.ipiv[i]][j]);
      }
   }
}

#endif /* LU_STRUCT_H_INCLUDED */
