#ifndef GYSV_H_INCLUDED
#define GYSV_H_INCLUDED

#include "inc_gen/Warnings.h"

#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/is_complex.h"

/**
 * SYGV
 **/

// SYGV:
// computes eigenvalues for:
//    itype = 1: A*x=(lambda)*B*x
//          = 2: A*B*x=(lambda)*x
//          = 3: B*A*x=(lambda)*x
// where A and B are N*N real symmetric definite matrices saved in column major format!
// define either userfriendly or efficient (not both :O ).
#define USERFRIENDLY_SYGV
//#define EFFICIENT_SYGV
template<class T>
Eigenvalue_struct<T> SYGV(T* arMatrix
                        , T* brMatrix
                        , int aN
                        , char aJobz = 'V'
                        , int aItype = 1
                        , char aUplo = 'U'
                        )
{
   // assert that T is not complex
   static_assert(!is_complex<T>::value,"SYGV does not work for complex numbers");
   
   //
   int itype = aItype; // a little extra work but adds some clarity... (probably not :P )
   char jobz = aJobz;  // a little extra work but adds some clarity...
   char uplo = aUplo;  // a little extra work but adds some clarity...
   int lda = max(1,aN);
   int ldb = max(1,aN);
   T* w = new T[aN];
   int lwork = max(10000,max(1,3*aN-1)); // can perhaps be optimized by using ilaenv... 
   T* work = new T[lwork];
   int info;
   
   //cout << " Doing SYGV " << endl;
   midas::lapack_interface::sygv_(&itype, &jobz, &uplo, &aN, arMatrix, &lda, brMatrix, &ldb, w, work, &lwork, &info);
   if(info != 0)
   {
      MidasWarning("SYGV INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   Eigenvalue_struct<T> eigen;
   eigen._info = info;
   eigen._n = aN;
   eigen._num_eigval = aN;
   eigen._eigenvalues = w;
   eigen._eigenvectors = new T*[aN];
#ifdef USERFRIENDLY_SYGV // userfriendly but less efficient
   T* p = new T[aN*aN];
   for(int i=0; i<aN*aN; ++i)
      p[i] = arMatrix[i];
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = p + i*aN;
#endif /* USERFRIENDLY_SYGV */
#ifdef EFFICIENT_SYGV // more efficient but less user friendly as arMatrix is stored in "eigen"
   for(int i=0; i<aN; ++i)
      eigen._eigenvectors[i] = arMatrix + i*aN;
#endif /* EFFICIENT_SYGV */

   delete[] work;
   return eigen;
}

// overload of SYGV for MidasMatrix
// currently this will not destroy the matrix... but a future version might !
template<class T>
Eigenvalue_struct<T> SYGV(const GeneralMidasMatrix<T>& arMatrix
                        , const GeneralMidasMatrix<T>& brMatrix
                        , char aJobz = 'V'
                        , int aItype = 1
                        , char aUplo = 'U'
                        )
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);
   
   // Do SYGV
   return SYGV(a.get(),b.get(),arMatrix.Ncols(),aJobz,aItype,aUplo);
}

#endif /* GYSV_H_INCLUDED */
