#ifndef IS_COMPLEX_H_INCLUDED
#define IS_COMPLEX_H_INCLUDED

#include <type_traits> // for std::integral_constant

/**
 * trait for checking whether a type T is a std::complex
 **/
template<class T>
struct is_complex: std::integral_constant<bool, false>
{ // general: false
};

template<class T>
struct is_complex<std::complex<T> >: std::integral_constant<bool, true>
{ // specialization for std::complex: true
};

#endif /* IS_COMPLEX_H_INCLUDED */
