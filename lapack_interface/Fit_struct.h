#ifndef FIT_STRUCT_H_INCLUDED
#define FIT_STRUCT_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <complex>
#include <type_traits> // for std::is_floating_point

namespace detail
{

/**
 * traits 
 **/
template<class T> struct Fit_struct_traits
{
   static_assert(std::is_floating_point<T>::value, "T must be floating point");
   using a_type = T;
   using b_type = T;
   using s_type = T;
};
 
/**
 *
 **/
template<class T> struct Fit_struct_traits<std::complex<T> > 
{
   static_assert(std::is_floating_point<T>::value, "T must be floating point");
   using a_type = std::complex<T>;
   using b_type = std::complex<T>;
   using s_type = T;
};

} /* namespace detail */

/**
 *
 **/
template<class T>
struct Fit_struct 
{
   using a_type = typename detail::Fit_struct_traits<T>::a_type;
   using b_type = typename detail::Fit_struct_traits<T>::b_type;
   using s_type = typename detail::Fit_struct_traits<T>::s_type;

   int info = -1;
   int m_, n_;
   int nrhs_, rank_;
   std::unique_ptr<a_type[]> A_;
   std::unique_ptr<b_type[]> B_;
   std::unique_ptr<s_type[]> S_;
};

#endif /* FIT_STRUCT_H_INCLUDED */
