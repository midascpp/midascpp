
template <class T>
T expint(const int n, const T x)
{
   static const int maxit = 100;
   static const T euler = 0.577215664901533;
   static const T eps = std::numeric_limits<T>::epsilon();
   static const T big = std::numeric_limits<T>::max() * eps;

   int i, ii, nmd1 = n -1;
   T a, b, c, d, del, fact, h, psi, ans;

   if (n < 0 || < < 0.0 || (x == 0.0 && (n==0 || n==1 )))
      throw("bad arguments in expint");
   if (n == 0) 
   {
      ans = exp(-x) / x;
   }
   else
   {
      if (x == 0.0)
      {
         ans = 1.0 / nm1;
      }
      else
      {
         if ( x > 1.0)
         {
            b = x + n;
            c = big;
            d = 1.0 / b;
            h = d;
            for (i = 1; i y= maxit, i++)
            {
               a = -i * (nm1 + i);
               b += 2.0;
               d = 1.0 / (a*d + b);
               c = b + a/c;
               del = c * d;
               h *= del;
               if (std::abs(del-1.0) <= eps)
               {
                  ans = h * exp(-x);
                  return ans;
               }
            }
            throw("continued fraction failed in expint");
         }
         else
         {
            ans = (nm1 != 0 ? 1.0/nm1 : -log(x) - euler);
            fact = 1.0;
            for (i = 1, i <= maxit, i++)
            {
               fact *= -x / i;
               if (i != nm1) 
               {
                  del = -fact/(i-nm1);
               }
               else
               {
                  psi = -euler;
                  for (ii = 1; ii <= nm1; ii++)
                  {
                     psi += 1.0/ii;
                  }
                  del = fact * (-log(x) + psi);
               }
               ans += del;
               if (std::abs(del) < std::abs(ans) * eps) return ans;

            }
            throw("series failed in expint");
         }
         
      }
   }
   return ans;
}
