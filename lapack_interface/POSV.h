#ifndef POSV_H_INCLUDED
#define POSV_H_INCLUDED

#include "lapack_interface/math_wrappers.h"
#include "LapackInterface.h"
#include "Linear_eq_struct.h"
#include "ColMajPtrFromMidasMatrix.h"

/**
 * DPOSV solve A*x = B where A is symmetric positive definite
 **/
template<class T>
Linear_eq_struct<T> POSV(T* arMatrix, T* brMatrix, int aN, int aNrhs, char aUplo)
{
   char uplo = aUplo; // this is maybe stupid :O (but consistent :P )
   int n=aN;          // this is maybe stupid :O
   int nrhs=aNrhs;    // this is maybe stupid :O
   int lda=max(1,aN);
   int ldb=max(1,aN);
   int info;

   midas::lapack_interface::posv_(&uplo,&n,&nrhs,arMatrix,&lda,brMatrix,&ldb,&info);
   if(info != 0)
   {
      MidasWarning("POSV INFO = " + std::to_string(info));
   }
   
   // setup linear equation struct
   Linear_eq_struct<T> sol;
   sol.info = info;
   sol.n = aN;
   sol.n_rhs = aNrhs;
   sol.solution = std::unique_ptr<T[]> (new T[aN*aNrhs]);
   for(int i=0; i<aN*aNrhs; ++i)
   {
      sol.solution[i] = brMatrix[i];
   }
   
   return sol;
}

/**
 *
 **/
template<class T>
Linear_eq_struct<T> POSV(T** arMatrix, T** brMatrix, int aN, int aNrhs, char aUplo = 'U')
{
   return POSV(arMatrix[0],brMatrix[0],aN,aNrhs,aUplo);
}

/**
 *
 **/
template<class T>
Linear_eq_struct<T> POSV(const GeneralMidasMatrix<T>& arMatrix, const GeneralMidasMatrix<T>& brMatrix, char aUplo = 'U')
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   return POSV(a.get(),b.get(),arMatrix.Nrows(),brMatrix.Ncols(),aUplo);
}

/**
 * Overload for POSV for solving MidasMatrix*x = MidasVector
 **/
template<class T>
Linear_eq_struct<T> POSV(const GeneralMidasMatrix<T>& arMatrix, const GeneralMidasVector<T>& brVec, char aUplo = 'U')
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = PtrFromMidasVector(brVec);

   return POSV(a.get(),b.get(),arMatrix.Nrows(),1,aUplo);
}

#endif /* POSV_H_INCLUDED */
