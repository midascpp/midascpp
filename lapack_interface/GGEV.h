#ifndef GGEV_H_INCLUDED
#define GGEV_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <type_traits> // for std::is_floating_point

#include "inc_gen/Warnings.h"

#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"

// GGEV: 
// computes eigenvalues and eigenvectors for:
//    A*X=(lambda)*B*X
//    (mu)*A*Y = B*Y
// where A and B are N*N real general matricies saved in column major format!
// define either userfriendly or efficient (not both :O ).
#define USERFRIENDLY_GGEV
//#define EFFICIENT_GGEV // NOT IMPLEMENTED!!
template
   <  class T
   ,  typename = std::enable_if_t< !midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_ext_struct<T> GGEV(T* arMatrix, T* brMatrix, int aN, char aJobvl = 'V', char aJobvr = 'V')
{
   static_assert(std::is_floating_point<T>::value, "GEEV only implemented for floating point at the moment.");

   char jobvl = aJobvl;  // a little extra work but adds some clarity...
   char jobvr = aJobvr;  // a little extra work but adds some clarity...
   int lda = max(1,aN);
   int ldb = max(1,aN);
   std::unique_ptr<T[]> alphar ( new T[aN] );
   std::unique_ptr<T[]> alphai ( new T[aN] );
   std::unique_ptr<T[]> beta ( new T[aN] );
   int ldvl = max(1,aN);
   int ldvr = max(1,aN);
   std::unique_ptr<T[]> vl ( new T[ldvl*aN] );
   std::unique_ptr<T[]> vr ( new T[ldvr*aN] );
   int lwork = max(10000,8*aN); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work ( new T[lwork] );
   int info;
   
   //cout << " Doing GGEV " << endl;
   midas::lapack_interface::ggev_(&jobvl, &jobvr, &aN, arMatrix, &lda, brMatrix, &ldb, alphar.get(), alphai.get(), beta.get()
                                , vl.get(), &ldvl, vr.get(), &ldvr, work.get(), &lwork, &info);
   if(info != 0)
   {
      MidasWarning("GGEV INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   Eigenvalue_ext_struct<T> eigen;
   eigen.info_ = info;
   eigen.n_ = aN;
   eigen.num_eigval_ = aN;
   eigen.re_eigenvalues_ = alphar.release();
   eigen.im_eigenvalues_ = alphai.release();
   // load eigenvalues
   for(int i=0; i<eigen.num_eigval_;++i)
   {
      if(beta[i] != 0)
      {
         eigen.re_eigenvalues_[i] /= beta[i];
         eigen.im_eigenvalues_[i] /= beta[i];
      }
      else
         MIDASERROR("BETA IS ZERO");
   }
   // load eigenvectors
   eigen.lhs_eigenvectors_ = new T*[aN];
   eigen.rhs_eigenvectors_ = new T*[aN];
#ifdef USERFRIENDLY_GGEV // userfriendly but less efficient
   for(int i=0; i<aN; ++i)
   {
      eigen.lhs_eigenvectors_[i] = vl.get() + i*aN;
      eigen.rhs_eigenvectors_[i] = vr.get() + i*aN;
   }
   vl.release(); // release ownership
   vr.release(); // release ownership
#endif /* USERFRIENDLY_GGEV */
#ifdef EFFICIENT_GGEV 
   // not implemeted... (not needed)
   exit(42);
#endif /* EFFICIENT_GGEV */

   return eigen;
}

/**
 *
 **/
template
   <  class T
   ,  typename = std::enable_if_t< !midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_ext_struct<T> GGEV(const GeneralMidasMatrix<T>& arMatrix
                            , const GeneralMidasMatrix<T>& brMatrix
                            , char aJobvl = 'V'
                            , char aJobvr = 'V'
                            )
{
   // assert some stuff... (hmm ??)
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0);
   assert((brMatrix.Ncols()>0) && brMatrix.Nrows()>0);

   //
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   // Do GGEV
   return GGEV(a.get(),b.get(),arMatrix.Ncols(),aJobvl,aJobvr);
}


// GGEV: 
// computes eigenvalues and eigenvectors for:
//    A*X=(lambda)*B*X
//    (mu)*A*Y = B*Y
// where A and B are N*N complex general matricies saved in column major format!
// define either userfriendly or efficient (not both :O ).
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_complex_struct<T> GGEV(T* arMatrix, T* brMatrix, int aN, char aJobvl = 'V', char aJobvr = 'V')
{
   char jobvl = aJobvl;  // a little extra work but adds some clarity...
   char jobvr = aJobvr;  // a little extra work but adds some clarity...
   int lda = max(1,aN);
   int ldb = max(1,aN);
   std::unique_ptr<T[]> alpha ( new T[aN] );
   std::unique_ptr<T[]> beta ( new T[aN] );
   int ldvl = max(1,aN);
   int ldvr = max(1,aN);
   std::unique_ptr<T[]> vl ( new T[ldvl*aN] );
   std::unique_ptr<T[]> vr ( new T[ldvr*aN] );
   int lwork = max(10000,2*aN); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work ( new T[lwork] );
   std::unique_ptr<double[]> rwork ( new double[8*aN] );
   int info;
   
   //cout << " Doing GGEV " << endl;
   midas::lapack_interface::ggev_(&jobvl, &jobvr, &aN, arMatrix, &lda, brMatrix, &ldb, alpha.get(), beta.get()
                                , vl.get(), &ldvl, vr.get(), &ldvr, work.get(), &lwork, rwork.get(), &info);
   if(info != 0)
   {
      MidasWarning("GGEV INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   Eigenvalue_complex_struct<T> eigen;
   eigen.info_ = info;
   eigen.n_ = aN;
   eigen.num_eigval_ = aN;
   eigen.eigenvalues_ = alpha.release();
   // load eigenvalues
   for(int i=0; i<eigen.num_eigval_;++i)
   {
      if (  !libmda::numeric::float_numeq_zero(std::abs(beta[i]), aN, 5) )
      {
         eigen.eigenvalues_[i] /= beta[i];
      }
      else
      {
         MIDASERROR("BETA IS ZERO");
      }
   }
   // load eigenvectors
   eigen.lhs_eigenvectors_ = new T*[aN];
   eigen.rhs_eigenvectors_ = new T*[aN];
   for(int i=0; i<aN; ++i)
   {
      eigen.lhs_eigenvectors_[i] = vl.get() + i*aN;
      eigen.rhs_eigenvectors_[i] = vr.get() + i*aN;
   }
   vl.release(); // release ownership
   vr.release(); // release ownership
   return eigen;
}

/**
 *
 **/
template
   <  class T
   ,  typename = std::enable_if_t<midas::type_traits::IsComplexV<T> >
   >
Eigenvalue_complex_struct<T> GGEV(const GeneralMidasMatrix<T>& arMatrix
                            , const GeneralMidasMatrix<T>& brMatrix
                            , char aJobvl = 'V'
                            , char aJobvr = 'V'
                            )
{
   // assert some stuff... (hmm ??)
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0);
   assert((brMatrix.Ncols()>0) && brMatrix.Nrows()>0);

   //
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   // Do GGEV
   return GGEV(a.get(),b.get(),arMatrix.Ncols(),aJobvl,aJobvr);
}

#endif /* GGEV_H_INCLUDED */
