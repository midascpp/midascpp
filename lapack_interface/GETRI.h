#ifndef GETRI_H_INCLUDED
#define GETRI_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <algorithm> // for std::min, std::max

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/LU_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
//#include "lapack_interface/PtrFromMidasVector.h"

#include "inc_gen/Warnings.h"
#include "mmv/MidasMatrix.h"
//#include "mmv/MidasVector.h"

/***************************************************************************//**
 * @param[in] arLU   LU_struct (e.g. obtained by using GETRF). On exit arLU.a
 *                   is replaced by the inverse of the original matrix. Note
 *                   that arLU.ipiv is modified,
 ******************************************************************************/
template
    <   class T
    >
void GETRI
    (   LU_struct<T>& arLU
    )
{
   MIDASERROR("Implement me!");
   using a_type = typename detail::LU_struct_traits<T>::a_type;

   // Check that matrix is square
   MidasAssert(arLU.m == arLU.n, "Cannot invert non-square matrix!");

   // Change from C-style to Fortran-style indexing
   for (int i = 0; i < arLU.n; ++i)
   {
       arLU.ipiv[i] += 1; 
   }

   // Setup and workspace query
   int n = arLU.n;
   int lda = std::max(1, n);
   int info;
   int lwork = -1; 
   a_type optwork;
   midas::lapack_interface::getri(&n, arLU.a, &lda, arLU.ipiv, &optwork, &lwork, &info);
   if (info != 0) MIDASERROR("GETRI: Workspace query failed with info = " + std::to_string(info) + " !");
   lwork = static_cast<int>(std::real(optwork));
   auto work = std::make_unique<a_type[]>(std::max(1, lwork));
   
   // Call getri
   midas::lapack_interface::getri(&n, arLU.a, &lda, arLU.ipiv, work.get(), &lwork, &info);
   if (info != 0) MidasWarning("GETRI: Failed with info = " + std::to_string(info) + " !");     
}

// /**
//  * Midas Matrix interface
//  **/
// template<class T> 
// LU_struct<T> GETRF(const GeneralMidasMatrix<T>& aMatrix)
// {
//    auto a = ColMajPtrFromMidasMatrix2(aMatrix);
//    // we release both unique_ptr's as the lu struct will take ownership over these
//    return GETRF(a.release(),aMatrix.Nrows(),aMatrix.Ncols());
// }

#endif /* GETRI_H_INCLUDED */
