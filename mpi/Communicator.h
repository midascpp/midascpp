/**
************************************************************************
* 
* @file                Communicator.h
*
* Created:             10-10-2013
*
* Author:              Ian H. Godtliebsen  (ian@chem.au.dk)
*
* Short Description:   Class to hold information on an MPI communicator.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_COMMUNICATOR_H_INCLUDED
#define MIDAS_MPI_COMMUNICATOR_H_INCLUDED

#include "mpi/TypeDefs.h"

#include "mpi/CommunicatorHandle.h"

namespace midas
{
namespace mpi
{

/**
 * Class to hold information on MPI communicators.
 **/
class Communicator
{
   private:
      //! MPI communicator.
      mutable MPI_Comm mCommunicator;
      //! MPI group
      MPI_Group mGroup;
      
      //! Name of communicator.
      std::string mName;
      //! Rank in communicator
      int mRank;
      //! Nr of processes in communicator
      int mNrProc;
      //! Master of communicator
      int mMasterRank = 0;

   public:
      //! Constructor
      Communicator(const std::string& aName, MPI_Comm aCommunicator);

      //!@{
      //! Delete copy ctor/assignment
      Communicator(const Communicator&) = delete;
      Communicator& operator=(const Communicator&) = delete;
      
      //!@{
      //! Default move ctor/assignment
      Communicator(Communicator&&) = default;
      Communicator& operator=(Communicator&&) = default;
      //!@}

      //! Destructor
      ~Communicator();

      //!@{
      //! Getters for MPI structures.
      //MPI_Comm GetCommunicator()    const { return mCommunicator; }
      MPI_Comm  GetMpiComm()        const { return mCommunicator; }
      MPI_Group GetMpiGroup()       const { return mGroup; }
      //!@}
      
      //!@{
      //! Getters
      const std::string& GetName()  const { return mName; }
      int GetRank()                 const { return mRank; }
      int GetNrProc()               const { return mNrProc; }
      int GetMasterRank()           const { return mMasterRank; }
      //!@}
      
      //!@{
      //! Query
      bool IsMaster()             const { return mRank == mMasterRank; }
      //!@}
      
      ////!
      //int Barrier() const;
      
      //! Split communicator.
      CommunicatorHandle SplitCommunicator(const std::string& aName, int aColor, int aKey) const;
};

//!
inline bool operator==(const Communicator& aComm1, const Communicator& aComm2)
{
   return aComm1.GetName() == aComm2.GetName();
}

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_COMMUNICATOR_H_INCLUDED */
