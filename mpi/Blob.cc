/**
************************************************************************
* 
* @file                Blob.cc
*
* Created:             09-12-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of Blob.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "Blob.h"

#ifdef VAR_MPI

#include <cstring>

#include "util/Error.h"
#include "mpi/Wrapper.h"

namespace midas
{
namespace mpi
{

/**
 *
 **/
Blob::Blob
   (  int aCapacity
   )
   :  mBlob(new char[aCapacity])
   ,  mCapacity(aCapacity)
{
}

/**
 *
 **/
void Blob::Load
   (  const char* aPtr
   ,  std::size_t aSize
   )
{
   if((aSize + mSize) > mCapacity)
   {
      //MIDASERROR("On Load() : Capacity to small.");
      mCapacity = (mCapacity > 512 ? 2 * mCapacity : 512);
      std::unique_ptr<char[]> new_blob(new char[mCapacity]);
      memcpy(static_cast<void*>(new_blob.get()), static_cast<void*>(mBlob.get()), mSize);
      mBlob = std::move(new_blob);
   }
   
   // copy the data to the blob
   memcpy(static_cast<void*>(mBlob.get() + mSize), aPtr, aSize);
   mSize += aSize;
}

/**
 *
 **/
void Blob::Load
   (  const std::string& aStr
   )
{
   std::size_t size = aStr.size();
   this->Load(reinterpret_cast<char*>(&size), sizeof(size));
   this->Load(aStr.c_str(), size);
}

/**
 *
 **/
void Blob::Unload
   (  char* aPtr
   ,  std::size_t aSize
   )
{
   if((aSize + mPosition) > mCapacity)
   {
      MIDASERROR("On Unload() : Capacity to small.");
   }

   // copy the data from the blob
   memcpy(aPtr, mBlob.get() + mPosition, aSize);
   mPosition += aSize;
}

/**
 *
 **/
void Blob::Unload
   (  std::string& aStr
   )
{
   std::size_t size;
   this->Unload(reinterpret_cast<char*>(&size), sizeof(size));
      
   std::unique_ptr<char[]> ptr(new char[size + 1]);
      
   this->Unload(ptr.get(), size);
   ptr[size] = '\0';
      
   aStr = std::string(ptr.get(), size);
}

/**
 *
 **/
int Blob::Send
   (  const Communicator& aCommunicator
   ,  int aDestination
   ,  int aTag
   )  const
{
   int return_code = detail::WRAP_Send(mBlob.get(), mSize, MPI_BYTE, aDestination, aTag, aCommunicator.GetMpiComm());
   return return_code;
}

/**
 *
 **/
int Blob::Recv
   (  const Communicator& aCommunicator
   ,  int aSource
   ,  int aTag
   )
{
   MPI_Status status;
   int number_amount;
   MPI_Probe(aSource, aTag, aCommunicator.GetMpiComm(), &status);
   MPI_Get_count(&status, MPI_BYTE, &number_amount);
   if(number_amount > mCapacity)
   {
      mBlob = std::unique_ptr<char[]>(new char[number_amount]);
      mCapacity = number_amount;
   }
   mSize = number_amount;
   int return_code = detail::WRAP_Recv(mBlob.get(), mSize, MPI_BYTE, aSource, aTag, aCommunicator.GetMpiComm(), &status);
   
   return return_code;
}

/**
 *
 **/
void Blob::Isend
   (
   )
{
}

/**
 *
 **/
void Blob::Irecv
   (
   )
{
}

/**
 *
 **/
int Blob::Bcast
   (  const Communicator& aCommunicator
   ,  int aSource
   )
{
   // send size of buffer
   if(aCommunicator.GetRank() == aSource)
   {
      // sending rank
      int number_amount = mSize;
      detail::WRAP_Bcast(&number_amount, 1, MPI_INT, aSource, aCommunicator.GetMpiComm());
   }
   else
   {
      // recieving ranks
      int number_amount;
      detail::WRAP_Bcast(&number_amount, 1, MPI_INT, aSource, aCommunicator.GetMpiComm());
   
      if(number_amount > mCapacity)
      {
         mBlob = std::unique_ptr<char[]>(new char[number_amount]);
         mCapacity = number_amount;
      }
      
      mSize = number_amount;
   }

   return detail::WRAP_Bcast(mBlob.get(), mSize, MPI_BYTE, aSource, aCommunicator.GetMpiComm());
}

/**
 *
 **/
int Blob::Bcast
   (  const Communicator& aCommunicator
   )
{
   return this->Bcast(aCommunicator, aCommunicator.GetMasterRank());
}

} /* namespace mpi */
} /* namespace midas */

#endif /* VAR_MPI */
