#ifndef MPI_MPILOG_H_INCLUDED
#define MPI_MPILOG_H_INCLUDED

namespace midas
{
namespace mpi
{

//! Create header for MPI logging message
std::string CreateMessageHeader();

//! Open mpi log files
void OpenLogFile();

//! Close mpi log files
void CloseLogFile();

// Write to mpi log files
void WriteToLog(const std::string&);


} /* namespace mpi */
} /* namespace midas */

#endif /* MPI_MPILOG_H_INCLUDED */
