/**
************************************************************************
* 
* @file                CommunicatorManager.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class to control which MPI_Comm we are currently using
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_COMMUNICATOR_MANAGER_H_INCLUDED
#define MIDAS_MPI_COMMUNICATOR_MANAGER_H_INCLUDED

#ifdef VAR_MPI
#include <string>
#include <mpi.h>
#include <list>

#include "inc_gen/TypeDefs.h"
#include "mpi/Communicator.h"

namespace midas
{
namespace mpi
{

// Forward decleare CommunicatorHandle.
class CommunicatorHandle;

/**
 * struct to comtrol the current process's communicator
 **/
struct CommunicatorManager
{
   private:
      ////! Are we in an mpi region
      //bool mMpiRegion;
      ////! Comm self
      //Communicator mCommSelf;
      //! List of communicators that this process belongs to
      std::list<Communicator> mCommunicators;
      
      //! Constructer is private to prevent use. This should only be constructed through get_mpi_communicator!
      CommunicatorManager();
      
      //! delete copy ctor
      CommunicatorManager(const CommunicatorManager&) = delete;
      
      //! delete move ctor
      CommunicatorManager(CommunicatorManager&&) = delete;
      
      //! delete copy assignment
      CommunicatorManager& operator=(const CommunicatorManager&) = delete;
      
      //! delete move assignment
      CommunicatorManager& operator=(CommunicatorManager&&) = delete;
      
   public:
      //! Create communicator from name and MPI_Communicator
      CommunicatorHandle CreateCommunicator(const std::string& aName, MPI_Comm aCommunicator);
      
      //! Create communicator from Communicator
      CommunicatorHandle CreateCommunicator(const Communicator& aCommunicator);

      //! Get the current, i.e. last constructed communicator.
      const Communicator& GetCurrentCommunicator() const;

      //! Get communicator by name.
      const Communicator& GetCommunicator(const std::string& aName) const;

      //! Get a string of all communicators in list.
      std::string GetTreeString()        const;
      
      //! Erase communicator.
      void EraseCommunicator(const std::string& aName);
      
      //! Destructor.
      ~CommunicatorManager();

      //!
      void AddCommWorld()
      {
         mCommunicators.emplace_back("MPI_COMM_WORLD", MPI_COMM_WORLD);
      }
      
      //! Interface function to get mpi_finalizer object is declared friend for permission to use default contructor.
      friend CommunicatorManager& get_CommunicatorManager();
};

//! Interface function to force one time contruction of mpi finalizer
CommunicatorManager& get_CommunicatorManager();

} /* namespace mpi_util */
} /* namespace midas */

#endif /* VAR_MPI */

#endif /* MIDAS_MPI_COMMUNICATOR_MANAGER_H_INCLUDED */
