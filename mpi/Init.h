#ifndef MIDAS_MPI_INIT_H_INCLUDED
#define MIDAS_MPI_INIT_H_INCLUDED

#include "mpi/Guard.h"
#include "mpi/Communicator.h"

namespace midas
{
namespace mpi
{

//! Initialize master/slave mpi. Returns guard that will make sure slaves are stopped.
Guard Init(const Communicator& aCommunicator);

//!
void OutputMpiHeader(std::ostream& aOs);

} /* namespace mpi */
} /* namespace midas */


#endif /* MIDAS_MPI_INIT_H_INCLUDED */
