#ifndef MIDAS_MPI_IMPI_H_INCLUDED
#define MIDAS_MPI_IMPI_H_INCLUDED

/**
 * Include some typedefs (most are only defined if we are not using mpi).
 * This includes mpi.h if VAR_MPI is set.
 **/
#include "mpi/TypeDefs.h"

/**
 * Include headers that provide an interface even if we do not use mpi
 **/
#include "mpi/Info.h"
#include "mpi/Communicator.h"
#include "mpi/Guard.h"
#include "mpi/Signal.h"
#include "mpi/FileToString.h"
#include "mpi/Wrapper.h"
#include "mpi/MpiLog.h"
#include "mpi/FileStream.h"
#include "mpi/FileSystemMpi.h"

/**
 * Include headers that provide an interface ONLY if we use mpi
 **/
#ifdef VAR_MPI
#include "mpi/DataTypeTrait.h"
#endif /* VAR_MPI */

#endif /* MIDAS_MPI_IMPI_H_INCLUDED */
