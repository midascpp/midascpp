#include <iostream>
#include <string>
#include <fstream>
#include <mutex>

#include "mpi/Info.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/paral/threaded_system_call.h" // for date_now()
#include "concurrency/Info.h"


namespace midas
{
namespace mpi
{

/**
 * Struct for holding log file ofstream.
 **/
struct MpiLog
{
   // Log file
   std::string   filename; 
   std::ofstream out;
   
   // Mutexes to synchronize writing to different logs
   std::mutex    stdout_mutex;
   std::mutex    file_mutex;
} mpi_log;

/**
 * Create header for MPI logging message.
 *
 * Will add date, global rank, thread id, and display whether the code is currently in an mpi region.
 *
 * @return Returns message header for MPI logging messages.
 **/
std::string CreateMessageHeader()
{
   const std::string message_header 
      = "[" + date_now()                    + "] "
      + "(" + std::to_string(GlobalRank())  + ") "
      + "[" + concurrency::ThreadIDString() + "] "
      + "{" + (IsMpiRegion() ? "*" : " ")   + "} : "
      ;

   return message_header;
}

/**
 * Open log file for rank.
 **/
void OpenLogFile
   (
   )
{  
   mpi_log.filename = "mpi_rank_" + std::to_string(mpi::GlobalRank()) + ".log";
   if (DoLogging() & Info::mpi_log_type::log_file) 
   {
      mpi_log.out.open(mpi_log.filename, std::ofstream::out | std::ofstream::app);
      Out72Char  (mpi_log.out, '$', '$', '$');
      Out72Char  (mpi_log.out, '$', ' ', '$');
      OneArgOut72(mpi_log.out, " MIDAS CPP MPI RANK " + std::to_string(mpi::GlobalRank()) + " LOG FILE ", '$');
      Out72Char  (mpi_log.out, '$', ' ', '$');
      Out72Char  (mpi_log.out, '$', '$', '$');
      OutputDate (mpi_log.out, "\n The date is: ");
   }
}

/**
 * Close log file for rank.
 **/
void CloseLogFile
   (
   )
{
   if (mpi_log.out.is_open()) 
   {
      mpi_log.out.close();
   }
}

/**
 * Write something to log.
 **/
void WriteToLog
   (  const std::string& aMsg
   )
{
   // Create message
   const std::string message = CreateMessageHeader() + aMsg + "\n";
   
   // Print to stdout if requested
   if (DoLogging() & Info::mpi_log_type::log_stdout)
   {
      std::lock_guard<std::mutex> lock(mpi_log.stdout_mutex);
      std::cout << message << std::flush;
   }
   
   // Print to file if requested
   if (DoLogging() & Info::mpi_log_type::log_file)
   {
      std::lock_guard<std::mutex> lock(mpi_log.file_mutex);
      if (mpi_log.out.is_open()) 
      {
         mpi_log.out << message;
         mpi_log.out << std::flush;
      }
   }
}

} /* namespace mpi */
} /* namespace midas */
