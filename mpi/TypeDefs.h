#ifndef MIDAS_MPI_TYPEDEFS_H_INCLUDED
#define MIDAS_MPI_TYPEDEFS_H_INCLUDED

// If MPI_DEBUG is not defined, we define it according to DEBUG macro.
#ifndef MPI_DEBUG
   #ifdef DEBUG
      #define MPI_DEBUG true
   #else
      #define MPI_DEBUG false
   #endif /* DEBUG */
#endif /* MPI_DEBUG */

// If MPI_ASSERT_STATUS is not defined, we define it according to DEBUG macro.
#ifndef MPI_ASSERT_STATUS
   #ifdef DEBUG
      #define MPI_ASSERT_STATUS true
   #else
      #define MPI_ASSERT_STATUS false
   #endif /* DEBUG */
#endif /* MPI_ASSERT_STATUS */

#ifdef VAR_MPI
#include <mpi.h>
#else /* !VAR_MPI */

// If not VAR_MPI, we need to define some placeholder types...

/**
 * Some typedefs
 **/
using MPI_Comm     = void*;
using MPI_Group    = void*;
using MPI_Status   = void*;
using MPI_Datatype = void*;
using MPI_Request  = void*;
using MPI_Op       = void*;
using MPI_Info     = void*;

/**
 * Some constants
 **/
extern MPI_Comm MPI_COMM_WORLD;
extern MPI_Comm MPI_COMM_NULL;

constexpr int MPI_SUCCESS = 0;

#endif /* VAR_MPI */

#endif /* MIDAS_MPI_TYPEDEFS_H_INCLUDED */
