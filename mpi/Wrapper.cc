#include <iostream>
#include <mutex>
#include <thread>
#include <sstream>
#include <cstdlib>

#include "util/Error.h"

#include "mpi/TypeDefs.h"
#include "mpi/Info.h"
#include "mpi/Blob.h"
#include "mpi/Communicator.h"
#include "mpi/MpiLog.h"

#include "concurrency/Info.h"

namespace midas
{
namespace mpi
{
namespace detail
{

using mutex_type = std::mutex;
mutex_type mpi_library_mutex;

/**
 * Pointer to string.
 *
 * @param ptr   The pointer to print as string.
 *
 * @return Returns dummy string.
 **/
std::string ToString 
   (  const void* ptr
   )
{
   std::stringstream sstr;
   sstr << ptr;
   return sstr.str();
}

#ifdef VAR_MPI
/**
 * Convert MPI_Datatype to string for outputting.
 *
 * @param datatype   The datatype to convert.
 *
 * @return Returns datatype as string.
 **/
std::string DataTypeToString
   (  MPI_Datatype datatype
   )
{
   char name[MPI_MAX_OBJECT_NAME];
   int  namelen;
   int  i;
   int  errs = 0;

   MPI_Type_get_name(datatype, name, &namelen);
   
   return std::string(name);
}

/**
 * Convert MPI_Comm to string for outputting.
 *
 * @param  comm    The communicator to convert.
 *
 * @return Returns datatype as string.
 **/
std::string CommToString
   (  MPI_Comm comm
   )
{
   char name[MPI_MAX_OBJECT_NAME];
   int  namelen;
   int  i;
   int  errs = 0;

   MPI_Comm_get_name(comm, name, &namelen);
   
   return std::string(name);
}

#else /* ! VAR_MPI */

/**
 * DataTypeToString when we are not compiling for MPI.
 *
 * @return Returns dummy string.
 **/
std::string DataTypeToString 
   (  void* ptr
   )
{
   return ToString(ptr);
}

/**
 * CommToString when we are not compiling for MPI.
 *
 * @return Returns dummy string.
 **/
std::string CommToString 
   (  void* ptr
   )
{
   return ToString(ptr);
}
#endif /* VAR_MPI */

/**
 * Acquire a lock for call into mpi library.
 * If MPI_THREAD_SERIALIZED we need a lock, to make sure we do not call into mpi library from multiple threads at the same time.
 *
 * @return If MPI_THREAD_SERIALIZED will return a locked lock-guard, if MPI_THREAD_MULTIPLE will return an unlocked lock-guard.
 **/
auto AcquireMpiLock
   (
   )
{
   std::unique_lock<mutex_type> lock(mpi_library_mutex, std::defer_lock);
#ifdef VAR_MPI
   if(ThreadingProvided() == MPI_THREAD_SERIALIZED)
   {
      if constexpr(MPI_DEBUG)
      {
         WriteToLog("Thread " + concurrency::ThreadIDString() + " is acquiring MPI lock.");
      }
      lock.lock();
      if constexpr(MPI_DEBUG)
      {
         WriteToLog("Thread " + concurrency::ThreadIDString() + " aquired MPI lock.");
      }
   }
#endif /* VAR_MPI */
   return lock;
}

/*!
 * Midas MPI_Barrier wrapper function.
 *
 * All calls to MPI_Barrier should be done using this wrapper function.
 *
 * @param comm   The MPI communicator handle
 *
 * @return Returns status of call to MPI_Barrier.
 */
int WRAP_Barrier
   (  MPI_Comm comm 
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Barrier() [" + CommToString(comm) + "].");
   }

   // Enter barrier.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Barrier( comm );
#else 
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Barrier() [" + CommToString(comm) + "].");
   }
   
   return status;
}

/*!
 * Midas MPI_Send wrapper function.
 *
 * All calls to MPI_Send should be done using this wrapper function.
 *
 * @param buf       Buffer to send
 * @param count     Number of elements
 * @param datatype  The type of the buffer
 * @param dest      The destination rank
 * @param tag       Tag used to identify data
 * @param comm      The MPI communicator handle
 * 
 * @return Returns status of call to MPI_Send.
 */
int WRAP_Send
   (  const void *buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int dest
   ,  int tag
   ,  MPI_Comm comm
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Send() to Rank : " + std::to_string(dest) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }

   // Send the data.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Send(const_cast<void*>(buf), count, datatype, dest, tag, comm);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Send() to Rank : " + std::to_string(dest) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }

   return status;
}

/*!
 * Midas MPI_ISend wrapper function.
 *
 * All calls to MPI_Isend should be done using this wrapper function.
 *
 * @param buf       Buffer to send
 * @param count     Number of elements
 * @param datatype  The type of the buffer
 * @param dest      The destination rank
 * @param tag       Tag used to identify data
 * @param comm      The MPI communicator handle
 * @param request   The MPI comminication request
 * 
 * @return Returns status of call to MPI_Isend.
 */
int WRAP_Isend
   (  const void *buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int dest
   ,  int tag
   ,  MPI_Comm comm
   ,  MPI_Request* request
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Isend() to Rank : " + std::to_string(dest) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }

   // Send the data.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Isend(const_cast<void*>(buf), count, datatype, dest, tag, comm, request);
#else
   int status = 0;
#endif /* VAR_MPI */
   
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Isend() to Rank : " + std::to_string(dest) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }

   return status;
}

/*!
 * Midas MPI_Recv wrapper function.
 *
 * All calls to MPI_Recv should be done using this wrapper.
 *
 * @param buf        Buffer to receive in
 * @param count      Number of elements
 * @param datatype   The type of the buffer
 * @param source     The source rank
 * @param tag        Tag used to identify data
 * @param comm       The MPI communicator handle
 * @param mpi_status Status struct
 *
 * @return Returns status of call to MPI_Recv.
 */
int WRAP_Recv
   (  void* buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int source
   ,  int tag
   ,  MPI_Comm comm
   ,  MPI_Status* mpi_status
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Recv() from Rank : " + std::to_string(source) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }

   if constexpr (MPI_DEBUG || MPI_ASSERT_STATUS)
   {
      assert(mpi_status);
   }
   
   // Receive the data.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Recv(buf, count, datatype, source, tag, comm, mpi_status);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Recv() from Rank : " + std::to_string(source) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }
   
   return status;
}

/*!
 * Midas MPI_Irecv wrapper function.
 *
 * All calls to MPI_Irecv should be done using this wrapper.
 *
 * @param buf        Buffer to receive in
 * @param count      Number of elements
 * @param datatype   The type of the buffer
 * @param source     The source rank
 * @param tag        Tag used to identify data
 * @param comm       The MPI communicator handle
 * @param request    The MPI comminication request
 *
 * @return Returns status of call to MPI_Recv.
 */
int WRAP_Irecv
   (  void* buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int source
   ,  int tag
   ,  MPI_Comm comm
   ,  MPI_Request* request
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Irecv() from Rank : " + std::to_string(source) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }
   
   // Receive the data.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Irecv(buf, count, datatype, source, tag, comm, request);
#else
   int status = 0;
#endif /* VAR_MPI */
   
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Irecv() from Rank : " + std::to_string(source) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }
   
   return status;
}

/*!
 * Midas MPI_Bcast wrapper function.
 *
 * All calls to MPI_Bcast should be done using this wrapper.
 *
 * @param buf        Buffer to receive in
 * @param count      Number of elements
 * @param datatype   The type of the buffer
 * @param root       The source rank
 * @param comm       The MPI communicator handle
 *
 * @return Returns status of call to MPI_Bcast.
 */
int WRAP_Bcast
   (  void* buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int root
   ,  MPI_Comm comm
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Bcast() with root " + std::to_string(root) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }
   
   // Do the broadcast.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Bcast(buf, count, datatype, root, comm);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Bcast() with root " + std::to_string(root) + " [" + CommToString(comm) + "] (" + DataTypeToString(datatype) + ") {" + std::to_string(count) + "} <" + ToString(buf) + ">.");
   }
   
   return status;
}

/*!
 * Midas MPI_Ibcast wrapper function.
 *
 * All calls to MPI_Ibcast should be done using this wrapper.
 *
 * @param buf        Buffer to receive in
 * @param count      Number of elements
 * @param datatype   The type of the buffer
 * @param root       The source rank
 * @param comm       The MPI communicator handle
 * @param req        Pointer to request to wait for
 *
 * @return Returns status of call to MPI_Bcast.
 */
int WRAP_Ibcast
   (  void* buf
   ,  int count
   ,  MPI_Datatype datatype
   ,  int root
   ,  MPI_Comm comm
   ,  MPI_Request* req
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Ibcast()");
   }
   
   // Do the broadcast.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Ibcast(buf, count, datatype, root, comm, req);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Ibcast()");
   }
   
   return status;
}

/*!
 * Midas MPI_Wait wrapper function.
 *
 * All calls to MPI_Wait should be done using this wrapper.
 *
 * @param req        Pointer to request to wait for
 * @param mpi_status     Status
 *
 * @return Returns status of call to MPI_Bcast.
 */
int WRAP_Wait
   (  MPI_Request* req
   ,  MPI_Status* mpi_status
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Wait()");
   }

   if constexpr (MPI_DEBUG || MPI_ASSERT_STATUS)
   {
      assert(mpi_status);
   }
   
   // Do the wait.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Wait(req, mpi_status);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Wait()");
   }

   return status;
}

/*!
 * Midas MPI_Abort wrapper function.
 *
 * All calls to MPI_Abort should be done using this wrapper.
 *
 * @param comm        Communicator to abort.
 * @param errorcode   Error code to return.
 *
 * @return Status?
 */
int WRAP_Abort
   (  MPI_Comm comm
   ,  int errorcode
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      std::cout << "Rank : " << GlobalRank() << " is calling MPI_Abort." << std::endl;
   }
   
   // Do the abort.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Abort(comm, errorcode);
   return status;
#else
   abort();
   return MPI_SUCCESS;
#endif /* VAR_MPI */
}

/*!
 * Midas MPI_Reduce wrapper function.
 *
 * All calls to MPI_Reduce should be done using this wrapper.
 *
 * @param sendbuf    Buffer to send from
 * @param recvbuf    Buffer to recv in
 * @param count      Number of elements
 * @param datatype   The type of the buffer
 * @param op         Reduction operation
 * @param root       The source rank
 * @param comm       The MPI communicator handle
 *
 * @return Returns status of call to MPI_Reduce.
 */
int WRAP_Reduce
   (  const void* sendbuf
   ,  void* recvbuf
   ,  int count
   ,  MPI_Datatype datatype
   ,  MPI_Op op
   ,  int root
   ,  MPI_Comm comm
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Reduce()");
   }
   
   // Do the reduction.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Reduce()");
   }
   
   return status;
}

/*!
 * Midas MPI_Allreduce wrapper function.
 *
 * All calls to MPI_Allreduce should be done using this wrapper.
 *
 * @param sendbuf    
 *    Buffer to send from
 * @param recvbuf    
 *    Buffer to recv in
 * @param count      
 *    Number of elements
 * @param datatype   
 *    The type of the buffer
 * @param op         
 *    Reduction operation
 * @param comm       
 *    The MPI communicator handle
 *
 * @return 
 *    Returns status of call to MPI_Reduce.
 */
int WRAP_Allreduce
   (  const void* sendbuf
   ,  void* recvbuf
   ,  int count
   ,  MPI_Datatype datatype
   ,  MPI_Op op
   ,  MPI_Comm comm
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Allreduce()");
   }
   
   // Do the reduction.
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Allreduce()");
   }
   
   return status;
}

/**
 * Wrapper for MPI_Comm_spawn().
 *
 * @param command
 *    Name of program to be spawned (string, significant only at root). 
 * @param argv
 *    Arguments to command (array of strings, significant only at root). 
 * @param maxprocs
 *    Maximum number of processes to start (integer, significant only at root). 
 * @param info
 *    A set of key-value pairs telling the runtime system where and how to start the processes (handle, significant only at root). 
 * @param root
 *    Rank of process in which previous arguments are examined (integer). 
 * @param comm
 *    Intracommunicator containing group of spawning processes (handle). 
 * @param intercomm
 *    OUTPUT: Intercommunicator between original group and the newly spawned group (handle). 
 * @param array_of_errcodes
 *    OUTPUT: One code per process (array of integers). 
 *
 * @return
 *    Returns error status.
 **/
int WRAP_Comm_spawn
   (  const char *command
   ,  char *argv[]
   ,  int maxprocs
   ,  MPI_Info info
   ,  int root
   ,  MPI_Comm comm
   ,  MPI_Comm *intercomm
   ,  int array_of_errcodes[]
   )
{
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entering MPI_Comm_spawn()");
   }

#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Comm_spawn(command, argv, maxprocs, info, root, comm, intercomm, array_of_errcodes);
#else
   MIDASERROR("Called MPI_Comm_spawn in a non-MPI build.");
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   
   // Some debug output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Leaving MPI_Comm_spawn()");
   }

   return status;
}

/**
 * Wrapper for MPI_Info_create().
 *
 * @param info
 *    info object created (handle).
 * 
 * @return
 *    Returns error status.
 **/
int WRAP_Info_create
   (  MPI_Info* info
   )
{
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Info_create(info);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   return status;
}

/**
 * Wrapper for MPI_Info_free().
 *
 * @param info
 *    info object to be freed (handle). 
 * 
 * @return
 *    Returns error status. 
 **/
int WRAP_Info_free
   (  MPI_Info *info
   )
{
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Info_free(info);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   return status;
}

/**
 * Wrapper for MPI_Info_set().
 *
 * @param info
 *    info object (handle).
 * @param key
 *    key (string).
 * @param value
 *    value (string).
 *
 * @return
 *    Returns error status. 
 **/
int WRAP_Info_set
   (  MPI_Info info
   ,  const char *key
   ,  const char *value
   )
{
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Info_set(info, key, value);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   return status;
}

/**
 * Wrapper for MPI_Info_delete().
 * 
 * @param info
 *    info object (handle) 
 * @param key
 *    key (string) 
 *
 * @return
 *    Returns error status. 
 **/
int WRAP_Info_delete
   (  MPI_Info info
   ,  const char *key
   )
{
#ifdef VAR_MPI
   auto lock = AcquireMpiLock();
   int status = MPI_Info_delete(info, key);
#else
   int status = MPI_SUCCESS;
#endif /* VAR_MPI */
   return status;
}

} /* namespace detail */



/* ******************************************************************
 * MPI SMART INTERFACE BEGINS HERE 
 *
 * Use these functions to do MPI actions and your life will become 
 * easy (Hopefully) :)!
 *
 * The idea is to implement a wrapper corresponding to each 
 * MPI action (e.g. MPI_Send, MPI_Bcast, etc)
 * for each "datatype" (e.g. Blob, std::vector<T>, etc),
 * using the Communicator structure to handle MPI_Comms.
 *
 ********************************************************************/

/**
 * Broadcast Blob to communicator.
 *
 * @param aBlob           The blob to broadcast.
 * @param aCommunicator   The communicator to broadcast over.
 * @param aSource         The source of the broadcast.
 *
 * @return Returns status of call to MPI_Bcast.
 **/
int Bcast
   (  Blob& aBlob
   ,  const Communicator& aCommunicator
   ,  int aSource
   )
{
#ifdef VAR_MPI
   return aBlob.Bcast(aCommunicator, aSource);
#else
   return 0;
#endif /* VAR_MPI */
}

/**
 * Broadcast Blob to communicator using master rank as source.
 *
 * @param aBlob           The blob to broadcast.
 * @param aCommunicator   The communicator to broadcast over.
 *
 * @return Returns status of call to MPI_Bcast.
 **/
int Bcast
   (  Blob& aBlob
   ,  const Communicator& aCommunicator
   )
{
#ifdef VAR_MPI
   return Bcast(aBlob, aCommunicator, aCommunicator.GetMasterRank());
#else
   return 0;
#endif /* VAR_MPI */
}

/**
 * Barrier on communicator.
 *
 * @param aCommunicator   The communicator to put a barrier on.
 *
 * @return Return status of call to MPI_Barrier.
 **/
int Barrier
   (  const Communicator& aCommunicator
   )
{
   return detail::WRAP_Barrier(aCommunicator.GetMpiComm());
}


} /* namespace mpi */
} /* namespace midas */
