/**
************************************************************************
* 
* @file                Blob.h
*
* Created:             09-12-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Abstract Blob class to send data over MPI connection.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_MPI_BLOB_H_INCLUDED
#define MIDAS_MPI_BLOB_H_INCLUDED

#ifdef VAR_MPI
#include <memory>

#include "mpi/Communicator.h"

namespace midas
{
namespace mpi
{

/**
 * Class to abstract a "blob" of data to be sent via MPI,
 * to minimize the number of send/recv and as such minimize network traffic.
 * Facilitates interface functions to load and unload data to/from the data blob.
 **/
class Blob
{
   private:
      //! Internal blob.
      std::unique_ptr<char[]> mBlob;
      //! Size of blob.
      std::size_t mSize = 0;
      //! Capacity of blob.
      std::size_t mCapacity = 0;
      //! Position of unload pointer
      std::size_t mPosition = 0;

   public:
      //! Cosntructor
      Blob(int aCapacity = 0);

      //!@{
      //! Load data
      void Load(const char*, std::size_t);
      void Load(const std::string&);
      //!@}
      
      //!@{
      //! Unload data
      void Unload(char*, std::size_t);
      void Unload(std::string&);
      //!@}

      //! Query size of blob
      std::size_t Size() const { return mSize; }
      
      //! MPI send
      int Send(const Communicator& aCommunicator, int aDestination, int aTag) const;

      //! MPI receive
      int Recv(const Communicator& aCommunicator, int aSource, int aTag);

      //! MPI non-blocking send
      void Isend();

      //! MPI non-blocking receive
      void Irecv();

      //!@{
      //! MPI broadcast
      int Bcast(const Communicator& aCommunicator, int aSource);
      //! MPI broadcast from communicator master
      int Bcast(const Communicator& aCommunicator);
      //!@}
};

} /* namespace mpi */
} /* namespace midas */

#else /* ! VAR_MPI */

namespace midas
{
namespace mpi
{

/**
 * Temporary dummy class for when we do not use MPI.
 * Might be removed at some point (should not affect client code... hopefully).
 **/ 
class Blob
{
};

} /* namespace mpi */
} /* namespace midas */ 

#endif /* VAR_MPI */

#endif /* MIDAS_MPI_BLOB_H_INCLUDED */
