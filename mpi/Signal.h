#ifndef MIDAS_MPI_SIGNAL_H_INCLUDED
#define MIDAS_MPI_SIGNAL_H_INCLUDED

#include "mpi/Communicator.h"

namespace midas
{
namespace mpi
{

//! Possible mpi signals
enum signal { ERROR, STOP, INPUT, PES, VSCF, VCC, TDH, TDVCC, TEST, SYSTEM };

//! Send signal to slaves
void SendSignal(const Communicator& aCommunicator, int aSignal);

//! Receive signal from master (will block until signal is sent)
int ReceiveSignal(const Communicator& aCommunicator);

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_SIGNAL_H_INCLUDED */
