/**
************************************************************************
* 
* @file                Finalizer.cc
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of Class to control when to call MPI_Finalize.
*                      
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "Finalizer.h"

#include <iostream>

#include "mpi/TypeDefs.h"
#include "mpi/Info.h"
#include "mpi/MpiLog.h"

namespace midas
{
namespace mpi
{
/**
 * Default ctor
 **/
Finalizer::Finalizer() { }

/**
 * On destructon of Finalizer object 
 * we will call MPI_Finalize.
 **/
Finalizer::~Finalizer
   (
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("ENTERING MPI FINALIZE");
   }
   
   // Close log files (if open)
   CloseLogFile();

#ifdef VAR_MPI
   // Do a barrier, then Finalize MPI.
   MPI_Barrier(MPI_COMM_WORLD);
   MPI_Finalize();
#endif /* VAR_MPI */
}

/**
 * Function force construction of Finalizer object.
 * Everything contructed after a call to this function is
 * guarenteed to be destructed before MPI_Finalize is called.
 **/
Finalizer& get_Finalizer()
{
   static Finalizer finalizer;
   return finalizer;
}

} /* namespace mpi */
} /* namespace midas */
