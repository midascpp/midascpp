/**
************************************************************************
* 
* @file                CommunicatorManager.cc
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class to control which MPI_Comm we are currently using
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifdef VAR_MPI
#include <string>
#include <mpi.h>

#include "mpi/CommunicatorManager.h"
#include "mpi/CommunicatorHandle.h"
#include "mpi/Finalizer.h"

namespace midas
{
namespace mpi
{

/**
 * Constructor for mpi communicator manager.
 * Should never be called directly, and the CommunicatorManager should always be
 * retrieved using the get_CommunicatorManager() interface function.
 **/
CommunicatorManager::CommunicatorManager
   (
   ) 
   :  mCommunicators()
{  
   //mCommunicators.emplace_back("MPI_COMM_WORLD", MPI_COMM_WORLD);
}

/**
 * Destructor for mpi communicator manager.
 **/
CommunicatorManager::~CommunicatorManager
   (
   )
{
}

/**
 *
 **/
CommunicatorHandle CommunicatorManager::CreateCommunicator
   (  const std::string& aName
   ,  MPI_Comm aCommunicator
   )
{
   mCommunicators.emplace_back(aName, aCommunicator);
   return CommunicatorHandle(aName);
}

/**
 * Get the current communicator.
 * Will return MPI_COMM_WORLD if communicator stack is empty.
 *
 * @return   The currently active communicator.
 **/
const Communicator& CommunicatorManager::GetCommunicator
   (  const std::string& aName
   )  const
{
   for(const auto& comm : mCommunicators)
   {
      if(comm.GetName() == aName)
      {
         return comm;
      }
   }
   return mCommunicators.back();
}

/**
 * Get the current communicator.
 * Will return MPI_COMM_WORLD if communicator stack is empty.
 *
 * @return   The currently active communicator.
 **/
const Communicator& CommunicatorManager::GetCurrentCommunicator
   (  
   )  const
{
   return mCommunicators.back();
}


/**
 * Erase a communicator with given name.
 **/
void CommunicatorManager::EraseCommunicator
   (  const std::string& aName
   )
{
   auto& comm = GetCommunicator(aName);
   MPI_Comm mpi_comm = comm.GetMpiComm();
   MPI_Barrier(mpi_comm);
   mCommunicators.remove(comm);
}

/**
 *
 **/
std::string CommunicatorManager::GetTreeString
   (  
   ) const
{
   std::string ret;
   for(const auto& comm : mCommunicators)
   {
      ret += "[" + comm.GetName() + "_" + std::to_string(comm.GetRank()) + "]";
   }
   return ret;
}

/**
 * Interface to access the CommunicatorManager.
 * This function should always be used to access the CommunicatorManager,
 * as it will make sure that the CommunicatorManager is constructed on the first call,
 * and will thus also make sure that when the program ends, 
 * the destructors are called in the correct order.
 * 
 * This is required because of the concept of "global initialization failure", 
 * which is inherent to C/C++ programs.
 *
 * NB NB NB THIS WILL BE REDONE !
 **/
CommunicatorManager& get_CommunicatorManager()
{
   get_Finalizer(); // get mpi finalizer so mpi_finalizes destructor
                        // gets called after CommunicatorManagers destructor
   static CommunicatorManager communicator_manager;
   return communicator_manager;
}

} /* namespace mpi */
} /* namespace midas */


#endif /* VAR_MPI */
