#include "mpi/FileStream.h"

#include "mpi/MpiLog.h"

namespace midas
{
namespace mpi
{

/*!
 * Constructor
 */
OFileStream::OFileStream
   (  const std::string&      aFilename
   ,  const StreamType&       aStreamType
   ,  std::ios_base::openmode aOpenmode
   )
   :  mStreamType(aStreamType)
{
   WriteToLog("Constructing mpi::OFileStream.");

   if(mStreamType == StreamType::MPI_MASTER)
   {
      if(IsMaster())
      {
         dynamic_cast<std::ofstream&>(*this).open(aFilename, aOpenmode);
      }
   }
   else if(mStreamType == StreamType::MPI_ALL_DISTRIBUTED)
   {
      dynamic_cast<std::ofstream&>(*this).open(aFilename, aOpenmode);
   }
   else
   {
      MIDASERROR("Unknown 'StreamType'.");
   }
}

/*!
 * 
 */
void OFileStream::close
   (
   )
{
   WriteToLog("Calling mpi::OFileStream::close().");

   if(mStreamType == StreamType::MPI_MASTER)
   {
      if(IsMaster())
      {
         dynamic_cast<std::ofstream&>(*this).close();
      }
   }
   else if(mStreamType == StreamType::MPI_ALL_DISTRIBUTED)
   {
      dynamic_cast<std::ofstream&>(*this).close();
   }
   else
   {
      MIDASERROR("Unknown 'StreamType'.");
   }
}

} /* namespace mpi */ 
} /* namespace midas */
