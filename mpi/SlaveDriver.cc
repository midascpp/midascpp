/**
************************************************************************
* 
* @file                SlaveDriver.cc
*
* Created:             10-1-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   
*                      
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// SlaveDriver could be moved to mains and maybe renamed MainMpi or smt...
#include <iostream>


#include "mpi/SlaveDriver.h"

#include "mpi/Impi.h"
#include "pes/PesMpi.h"
#include "vcc/VccDrv.h"
#include "vscf/VscfDrv.h"
#include "td/tdh/TdHDrv.h"
#include "td/tdvcc/TdvccDrv.h"
#include "test/Test.h"
#include "system/System.h"
extern midas::test::TestDrivers gTestDrivers;

namespace midas
{
namespace mpi
{

/**
 * Will send all slaves to an infinite loop waiting for tasks sent from master process/rank.
 * After recieving a task, the corresponding slave function will be called from the task table.
 *
 * @param aCommunicator    The communicator handling communication between master and slaves.
 **/
void SlaveDriver
   (  const Communicator& aCommunicator
   )
{
#ifdef VAR_MPI
   // master cannot start slave loop
   if(aCommunicator.IsMaster())
   {
      if constexpr (MPI_DEBUG)
      {
         WriteToLog(" WARNING : SlaveDriver() cannot be called from Master.");
      }
      return;
   }
   
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Entered the SlaveDriver routine.");
   }

   // Tell slaves we are in an mpi region
   auto mpi_region_guard = StartMpiRegion();

   //MPI_Status status;
   bool running = true;
   while(running)
   {
      // receive command (blocking)
      int task = mpi::ReceiveSignal(aCommunicator);

      // Run the task
      switch(task)
      {
         case signal::PES:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering PES SlaveDriver.");
            }
            
            // Call PES slavedriver
            pes::mpi_impl::SlaveDriver(aCommunicator);
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving PES SlaveDriver.");
            }
            break;
         }
         case signal::TDH:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering TdHDrv().");
            }
            
            // Call TdH driver
            tdh::TdHDrv();
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving TdHDrv().");
            }
            break;
         }
         case signal::TDVCC:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering TdvccDrv().");
            }
            
            // Call TdH driver
            tdvcc::TdvccDrv();
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving TdvccDrv().");
            }
            break;
         }
         case signal::VSCF:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering VscfDrv().");
            }

            // Call vscf driver
            vscf::VscfDrv();
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving VscfDrv().");
            }
            break;
         }
         case signal::VCC:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering VccDrv().");
            }

            vcc::VccDrv();
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving VccDrv().");
            }
            break;
         }
         case signal::SYSTEM:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering SystemDrv().");
            }
            
            // Call System driver.
            SystemDrv();
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving SystemDrv().");
            }
            break;
         }
         case signal::TEST:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Entering midas::test::Test().");
            }
            
            // Call Test driver.
            midas::test::Test(gTestDrivers);
            
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Leaving midas::test::Test().");
            }
            break;
         }
         case signal::STOP:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() :  Stopping! ");
            }

            running = false;
            break;
         }
         default:
         {
            if constexpr (MPI_DEBUG)
            {
               WriteToLog(" SlaveDriver() : Unknown task : " + std::to_string(task));
            }
         }
      }
   }
#endif /* VAR_MPI */
}

} /* namespace mpi */
} /* namespace midas */
