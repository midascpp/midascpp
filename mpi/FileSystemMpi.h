/**
 *******************************************************************************
 * 
 * @file    FileSystemMpi.h
 * @date    12-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    MPI-relevant versions of midas::filesystem methods.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MIDAS_MPI_FILESYSTEMMPI_H_INCLUDED
#define MIDAS_MPI_FILESYSTEMMPI_H_INCLUDED

#include "util/FileSystem.h"
#include "mpi/Impi.h"

namespace midas::mpi::filesystem
{
   namespace detail
   {
      /**
       * @param[in] aQuery
       *    The query functor of signature `bool aQuery(Args...)`.
       * @param[in] aArgs
       *    The arguments to aQuery.
       * @return
       *    The return-value obtained on the master rank.
       **/
      template<typename F, typename... Args>
      bool QueryOnMaster
         (  F aQuery
         ,  Args&&... aArgs
         )
      {
         int val = 0;
         if (midas::mpi::IsMaster())
         {
            val = static_cast<int>(aQuery(std::forward<Args>(aArgs)...));
         }
#ifdef VAR_MPI
         int status = midas::mpi::detail::WRAP_Bcast
            (  &val
            ,  1
            ,  MPI_INT
            ,  midas::mpi::MasterRank()
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
         if (status != 0)
         {
            MIDASERROR("Non-zero return status from WRAP_Bcast, status = "+std::to_string(status));
         }
#endif /* VAR_MPI */
         return bool(val);
      }

      /**
       * @param[in] aQuery
       *    The query functor of signature `bool aQuery(Args...)`.
       * @param[in] aArgs
       *    The arguments to aQuery.
       * @return
       *    True if aQuery is true on all ranks, otherwise false.
       **/
      template<typename F, typename... Args>
      bool QueryOnAll
         (  F aQuery
         ,  Args&&... aArgs
         )
      {
         int val = static_cast<int>(aQuery(std::forward<Args>(aArgs)...));
#ifdef VAR_MPI
         int sum = 0;
         int status = ::midas::mpi::detail::WRAP_Allreduce
            (  &val
            ,  &sum
            ,  1
            ,  MPI_INT
            ,  MPI_SUM
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
         if (status != 0)
         {
            MIDASERROR("Non-zero return status from WRAP_Allreduce, status = "+std::to_string(status));
         }
         val = (sum == midas::mpi::GlobalSize())? 1: 0;
#endif /* VAR_MPI */
         return bool(val);
      }

   } /* namespace detail */

   /**
    * @name Query
    * 
    * All `*OnMaster` performs the query on master only, who broadcasts results
    * to all ranks.
    * `*OnAll` is hopefully self-explanatory.
    *
    * Function declarations are like:
    * ~~~
    *     inline bool IsFileOnMaster(const std::string& aPath);
    *     inline bool IsFileOnAll(const std::string& aPath);
    * ~~~
    *
    * See descriptions of underlying functions in util/FileSystem.h.
    *
    **/
   //!@{
   // Macro for wrapping the MPI_DEBUG WriteToLog's.
   #define WRAP_QueryOn(TYPE,FUNC) \
      inline bool FUNC##On##TYPE(const std::string& aPath) \
      { \
         if constexpr(MPI_DEBUG) \
         { \
            ::midas::mpi::WriteToLog("Entering mpi::filesystem::" #FUNC "On" #TYPE ", arg = '"+aPath+"'."); \
         } \
         bool val = ::midas::mpi::filesystem::detail::QueryOn##TYPE(midas::filesystem::FUNC, aPath); \
         if constexpr(MPI_DEBUG) \
         { \
            ::midas::mpi::WriteToLog("Leaving mpi::filesystem::" #FUNC "On" #TYPE ", arg = '"+aPath+"', return = "+(val? "true":"false")+"."); \
         } \
         return val; \
      }

   WRAP_QueryOn(Master,Exists);
   WRAP_QueryOn(Master,IsFile);
   WRAP_QueryOn(Master,IsDir);
   WRAP_QueryOn(Master,IsSymlink);
   WRAP_QueryOn(Master,IsExecutable);
   WRAP_QueryOn(All,Exists);
   WRAP_QueryOn(All,IsFile);
   WRAP_QueryOn(All,IsDir);
   WRAP_QueryOn(All,IsSymlink);
   WRAP_QueryOn(All,IsExecutable);
   //!@}

} /* namespace midas::mpi::filesystem */

#endif /* MIDAS_MPI_FILESYSTEMMPI_H_INCLUDED */
