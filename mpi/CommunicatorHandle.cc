/**
************************************************************************
* 
* @file                CommunicatorHandle.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class to control RAII/SBRM (Scope-Bound Resource Management) for CommunicatorManager splits.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "mpi/CommunicatorHandle.h"

#include "mpi/CommunicatorManager.h"

namespace midas
{
namespace mpi
{

/**
 *
 **/
CommunicatorHandle::~CommunicatorHandle()
{
#ifdef VAR_MPI
   if(mPop)
   {
      get_CommunicatorManager().EraseCommunicator(mName);
   }
#endif /* VAR_MPI */
}

} /* namespace mpi */

} /* namespace midas */
