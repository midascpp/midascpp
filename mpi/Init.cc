#include "mpi/Init.h"

#include "mpi/Interface.h"
#include "mpi/SlaveDriver.h"
#include "mpi/Signal.h"
#include "mpi/Info.h"
#include "mpi/MpiLog.h"
#include "util/Io.h"
#include "input/Input.h"

namespace midas
{
namespace mpi
{

/**
 * Check if this directory is a
 * Network filesystem system (NFS) / Distributed filesystem 
 **/
void CheckNfs()
{
#ifdef VAR_MPI
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Check NFS");
   }
   
   // Master touches a file on NFS.
   if(get_Interface().GetMpiGlobalRank() == 0)
   {
      TouchFile(gMainDir + "/TEST_OF_WHETHER_NFS_OR_NOT_FOR_MIDAS");
   }
   
   detail::WRAP_Barrier(MPI_COMM_WORLD);

   // Slaves check if they can see the file.
   if(get_Interface().GetMpiGlobalRank() != 0) 
   {
      std::ifstream ifs(string(gMainDir + "/TEST_OF_WHETHER_NFS_OR_NOT_FOR_MIDAS"));
      if(!ifs.is_open())
      {
         MIDASERROR("The directory specified MUST be an NFS one (or similar).");
      }
      ifs.close();
   }
   
   detail::WRAP_Barrier(MPI_COMM_WORLD);
   
   // Then master removes the file again.
   if(get_Interface().GetMpiGlobalRank() == 0)
   {
      RmFile(gMainDir + "/TEST_OF_WHETHER_NFS_OR_NOT_FOR_MIDAS");
   }

   detail::WRAP_Barrier(MPI_COMM_WORLD);
   
   // Some after process debug-output.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Done checking NFS");
   }
#endif
}

/**
 * Init mpi master/slave.
 *
 * Slaves will go to a loop waiting for work.
 **/
Guard Init
   (  const Communicator& aCommunicator
   )
{
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Initializing MPI");
   }

   // Check that we are on a Network-File-System.
   midas::mpi::CheckNfs();

   if(aCommunicator.IsMaster())
   {
      int stop_signal = signal::STOP;

      // Master
      return Guard
         (  [&aCommunicator, stop_signal]()
            {
               if constexpr (MPI_DEBUG)
               {
                  WriteToLog("Master rank sending STOP signal.");
               }
                  
               // Send stop signal
               SendSignal(aCommunicator, stop_signal);
            }
         );
   }
   else
   {
      // Slaves
      SlaveDriver(aCommunicator);
      return Guard([](){});
   }
}

/**
 *
 **/
void OutputMpiHeader
   (  std::ostream& aOs
   )
{
   if(IsMaster())
   {
      constexpr char box   = 'O';
      constexpr char space = ' ';
      Out72Char  (aOs, box, box  , box);
      Out72Char  (aOs, box, space, box);
   #ifdef VAR_MPI
      /* Get Mpi Version */
      int major_version;
      int minor_version;
      MPI_Get_version(&major_version, &minor_version);

      char string_version[MPI_MAX_LIBRARY_VERSION_STRING];
      int  length;
      MPI_Get_library_version(string_version, &length);
      string_version[length] = '\0';

      OneArgOut72(aOs, " This is an MPI build ", box);
      Out72Char  (aOs, box, space, box);
      OneArgOut72(aOs, " Nr. processes      : " + std::to_string(GlobalSize()), box);
      OneArgOut72(aOs, " Master Rank        : " + std::to_string(MasterRank()), box);
      OneArgOut72(aOs, " Threading provided : " + ThreadingProvidedString(), box);
      Out72Char  (aOs, box, space, box);
      OneArgOut72(aOs, " MPI Standard : " + std::to_string(major_version) + "." + std::to_string(minor_version), box);
      OneArgOut72(aOs, " MPI Library  : " + std::string(string_version), box);
   #else
      OneArgOut72(aOs, " !!! This is NOT an MPI build !!!", box);
   #endif /* VAR_MPI */
      Out72Char  (aOs, box, space, box);
      Out72Char  (aOs, box, box  , box);
   }
}

} /* namespace mpi */
} /* namespace midas */
