/**
************************************************************************
* 
* @file                Interface.cc
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "mpi/Interface.h"

#include <string>
#ifdef VAR_MPI
#include <mpi.h>
#include "mpi/CommunicatorManager.h"
#include "mpi/CommunicatorHandle.h"
#include "mpi/Finalizer.h"
#endif /* VAR_MPI */

#include "input/Input.h"
#include "inc_gen/TypeDefs.h"


namespace midas
{
namespace mpi
{

In Interface::GetMpiRank() const
{
#ifdef VAR_MPI
   return get_CommunicatorManager().GetCurrentCommunicator().GetRank();
#else
   return 0;
#endif //VAR_MPI
}

In Interface::GetMpiNrProc() const
{
#ifdef VAR_MPI
   return get_CommunicatorManager().GetCurrentCommunicator().GetNrProc();
#else
   return 1;
#endif //VAR_MPI
}
      
In Interface::GetMpiGlobalRank() const
{
#ifdef VAR_MPI
   return get_CommunicatorManager().GetCommunicator("MPI_COMM_WORLD").GetRank();
#else
   return 0;
#endif //VAR_MPI
}
      
In Interface::GetMpiGlobalNrProc() const
{
#ifdef VAR_MPI
   return get_CommunicatorManager().GetCommunicator("MPI_COMM_WORLD").GetNrProc();
#else
   return 1;
#endif //VAR_MPI
}

std::string Interface::GetMpiTreeString() const
{
#ifdef VAR_MPI
   return get_CommunicatorManager().GetTreeString();
#else
   return std::string();
#endif //VAR_MPI
}

Interface& get_Interface()
{
#ifdef VAR_MPI
   get_CommunicatorManager(); /* get mpi comm_manager so it exist when we need it */
#endif /* VAR_MPI */
   static Interface mpi_i;
   return mpi_i;
}


} /* namespace mpi */
} /* namespace midas */
