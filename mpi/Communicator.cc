#include "mpi/Communicator.h"

#include "mpi/CommunicatorManager.h"

#include "mpi/MpiLog.h"

#include <iostream>

namespace midas
{
namespace mpi
{

/**
 * Communicator contructor.
 * Will get rank and nr of processes on the communicator.
 *
 * @param aName          A name/label for the communicator for later reference.
 * @param aCommunicator  The actual MPI communicator.
 **/
Communicator::Communicator
   (  const std::string& aName
   ,  MPI_Comm aCommunicator
   )
   :  mCommunicator(aCommunicator)
   ,  mName(aName)
{
#ifdef VAR_MPI
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Creating Communicator : " + aName);
   }
   if(mCommunicator != MPI_COMM_NULL)
   {
      MPI_Comm_group(mCommunicator, &mGroup);
      MPI_Comm_rank (mCommunicator, &mRank);
      MPI_Comm_size (mCommunicator, &mNrProc);
   }
#else
   mRank   = 0;
   mNrProc = 0;
#endif /* VAR_MPI */
}

/**
 * Communicator destructor.
 * If it is not MPI_COMM_WORDL, destructor will free the held communicator.
 **/
Communicator::~Communicator
   (
   )
{
#ifdef VAR_MPI
   if(mCommunicator != MPI_COMM_WORLD)
   {
      MPI_Comm_free(&mCommunicator);
   }
#endif /* VAR_MPI */
}

/**
 * Split the current communicator, 
 * and return a handle for Scope Bound Resource Management (SBRM) of the communicators
 *
 * @param aName    Name of new communicator.
 * @param aColor   Mpi color.
 * @param aKey     Mpi key.
 *
 * @return    Returns a handle which will automatically pop the communicator when scope ends.
 **/
CommunicatorHandle Communicator::SplitCommunicator
   (  const std::string& aName
   ,  int aColor
   ,  int aKey
   )  const
{
#ifdef VAR_MPI
   MPI_Barrier(mCommunicator);
   MPI_Comm mpi_comm_split;
   MPI_Comm_split(mCommunicator, aColor, aKey, &mpi_comm_split);

   return get_CommunicatorManager().CreateCommunicator(aName, mpi_comm_split);
#else /* ! VAR_MPI */
   return CommunicatorHandle("DUMMY");
#endif /* VAR_MPI */
}   

} /* namespace mpi */
} /* namespace midas */
