#include "mpi/Info.h"

#include "mpi/TypeDefs.h"
#include "mpi/Finalizer.h"
#include "mpi/Communicator.h"
#include "mpi/CommunicatorManager.h" // 25/07/2017 : THIS WILL BE REMOVED
#include "mpi/MpiLog.h"

#include "inc_gen/Warnings.h"
#include "util/Error.h"
#include "util/FileSystem.h"
#include "util/Os.h"

namespace midas
{
namespace mpi
{

bool Info::mMpiRegion;
bool Info::mThreadedRegion;
int  Info::mMasterRank;
int  Info::mMpiThreadingProvided;

int  Info::mGlobalRank;
int  Info::mGlobalSize;
Communicator Info::mCommunicatorWorld = Communicator("MPI_COMM_NULL", MPI_COMM_NULL);
Info::mpi_log_type Info::mDoLogging;

/**
 * Return MPI threaded provided string for debug printout.
 *
 * @param aThreadingProvided
 *    The threading level.
 *
 * @return
 *    Returns threaded provided string.
 **/
std::string ThreadingProvidedToString
   (  int aThreadingProvided
   )
{
   switch(aThreadingProvided)
   {
#ifdef VAR_MPI
      case MPI_THREAD_SINGLE:
         return {"MPI_THREAD_SINGLE"};
      case MPI_THREAD_FUNNELED:
         return {"MPI_THREAD_FUNNELED"};
      case MPI_THREAD_SERIALIZED:
         return {"MPI_THREAD_SERIALIZED"};
      case MPI_THREAD_MULTIPLE:
         return {"MPI_THREAD_MULTIPLE"};
#endif /* VAR_MPI */
      default:
         return {"UNKNOWN"};
   }
}

/**
 * Initialize mpi info struct.
 **/
void Info::Initialize
   (
   )
{
   mMpiRegion = false;
   mThreadedRegion = false;
   mMasterRank = 0;
   mMpiThreadingProvided = 0;
   mGlobalRank = 0;
   mGlobalSize = 1;

   if constexpr(MPI_DEBUG) 
   {
      mDoLogging  = static_cast<mpi_log_type>(mpi_log_type::log_stdout | mpi_log_type::log_file);
   }
   else
   {
      mDoLogging  = mpi_log_type::log_none;
   }
}

/**
 * Initialize MPI. 
 **/
void Initialize
   (  int* argc
   ,  char*** argv
   )
{
   Info::Initialize();
#ifdef VAR_MPI
   int threading_provided;

   // Initialize MPI with requested threading level ( 13/11-2017 : FOR NOW ALSWAYS USES MPI_THREAD_SERIALIZED. IF MPI_THREAD_MULTIPLE IS NEEDED MAKE INPUT OPTION!)
   if (false) 
   {
      // Use MPI_THREAD_MULTIPLE
      MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &Info::mMpiThreadingProvided);
   }
   else
   {
      // Use MPI_THREAD_SERIALIZED (I think this is more stable than MPI_THREAD_MULTIPLE).
      MPI_Init_thread(argc, argv, MPI_THREAD_SERIALIZED, &Info::mMpiThreadingProvided);
   }
   
   // Get global rank and size
   MPI_Comm_rank(MPI_COMM_WORLD, &Info::mGlobalRank);
   MPI_Comm_size(MPI_COMM_WORLD, &Info::mGlobalSize);
#endif /* VAR_MPI */
   
   // Open log files
   OpenLogFile();
   
#ifdef VAR_MPI
   // Check that threading level is sufficient
   if (  Info::mMpiThreadingProvided != MPI_THREAD_MULTIPLE 
      && Info::mMpiThreadingProvided != MPI_THREAD_SERIALIZED
      ) 
   {
      MIDASERROR("MPI ERROR : Level of threading provided is not sufficient. LEVEL : " + std::to_string(Info::mMpiThreadingProvided));
   }
   else
   {
      if constexpr (MPI_DEBUG)
      {
         WriteToLog("Threading provided : " + ThreadingProvidedToString(Info::mMpiThreadingProvided) + " (" + std::to_string(Info::mMpiThreadingProvided) + ").");
      }
   }
   
   // Create WORLD communicator
   Info::mCommunicatorWorld = Communicator("MPI_COMM_WORLD", MPI_COMM_WORLD);
   
   // Acquire finalizer (Makes sure MPI_Finalize is called on program exit)
   get_Finalizer();
   get_CommunicatorManager().AddCommWorld(); // 25/07/2017 : THIS WILL BE REMOVED AT SOME POINT
#endif /* VAR_MPI */
   
}

/**
 * Are we in an mpi region
 **/
bool IsMpiRegion()
{
   return Info::mMpiRegion;
}

/**
 * Get master rank
 **/
int MasterRank()
{
   return Info::mMasterRank;
}

/**
 *
 **/
bool IsMaster()
{
   return Info::mGlobalRank == Info::mMasterRank;
}

/**
 *
 **/
int GlobalRank()
{
   return Info::mGlobalRank;
}

/**
 *
 **/
int GlobalSize()
{
   return Info::mGlobalSize;
}

/**
 *
 **/
int ThreadingProvided()
{
   return Info::mMpiThreadingProvided;
}

/**
 *
 **/
std::string ThreadingProvidedString()
{
#ifdef VAR_MPI
   switch(ThreadingProvided())
   {
      case MPI_THREAD_SINGLE:
         return "MPI_THREAD_SINGLE";
      case MPI_THREAD_FUNNELED:
         return "MPI_THREAD_FUNNELED";
      case MPI_THREAD_SERIALIZED:
         return "MPI_THREAD_SERIALIZED";
      case MPI_THREAD_MULTIPLE:
         return "MPI_THREAD_MULTIPLE";
   }
#endif /* VAR_MPI */
   return "UNKNOWN";
}

/**
 *
 **/
bool IsThreadMultiple()
{
#ifdef VAR_MPI
   return Info::mMpiThreadingProvided == MPI_THREAD_MULTIPLE;
#else 
   return true;
#endif /* VAR_MPI */
}

/**
 *
 **/
const Communicator& CommunicatorWorld()
{
   return Info::mCommunicatorWorld;
}

/**
 *
 **/
Info::mpi_log_type DoLogging()
{
   return Info::mDoLogging;
}

/**
 * Signal the start of an Mpi region to 
 * functions which can work both in MPI and in serial.
 **/
Guard StartMpiRegion
   (
   )
{
   if constexpr(MPI_DEBUG)
   {
      WriteToLog("Entering MpiRegion");
   }
   
   // Set MpiReriong to true
   Info::mMpiRegion = true;
   
   // Return guard which will end MpiRegion on destruction
   return Guard
      (  []()
         { 
            if constexpr(MPI_DEBUG)
            {
               WriteToLog("Leaving MpiRegion.");
            }
            Info::mMpiRegion = false; 
         } 
      );
}

} /* namespace mpi */
} /* namespace midas */
