/**
************************************************************************
* 
* @file                Finalizer.h
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Class to control when to call MPI::FINALIZE
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_FINALIZER_H_INCLUDED
#define MIDAS_MPI_FINALIZER_H_INCLUDED

namespace midas
{
namespace mpi
{

/**
 * Struct to control when mpi finalize should be called.
 *
 * Use get_Finalizer to construct the Finalizer, 
 * which will ensure that everything constructed after 
 * will be destructed before MPI_Finalize is called.
 **/
struct Finalizer
{
   private:
      //! constructer is private to prevent use, this should only be constructed through get_Finalizer!
      Finalizer();
      
      //!@{
      //! We do not allow copy/move construction/assignment, so we delete the defaults
      Finalizer(const Finalizer&) = delete;
      Finalizer(Finalizer&&) = delete;
      Finalizer& operator=(const Finalizer&) = delete;
      Finalizer& operator=(Finalizer&&) = delete;
      //!@}
   public:
      //! Destructor
      ~Finalizer();
      
      //! Interface function to get Finalizer object, it is declared friend for permission to use default contructor
      friend Finalizer& get_Finalizer();
};

//! interface function to force contruction of mpi finalizer
Finalizer& get_Finalizer();

} /* namespace mpi */
} /* namespace midas */


#endif /* MIDAS_MPI_FINALIZER_H_INCLUDED */
