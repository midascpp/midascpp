/**
************************************************************************
* 
* @file                ForLoop.cc
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Function for expanding a loop using mpi
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "mpi/ForLoop.h"

#ifdef VAR_MPI
#include <mpi.h>
#endif //VAR_MPI

// std headers
#include <vector>
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mpi/CommunicatorHandle.h"
#include "mpi/CommunicatorManager.h"
#include "mpi/UtilFunc.h"

namespace midas
{
namespace mpi
{
namespace detail
{

/**
 *
 **/
std::vector<std::pair<In, In> > ForLoopImpl::TaskDivision
   (  In task_number
   ,  In proc_first
   ,  In proc_last
   ) 
{
   std::vector<std::pair<In, In> > res;
   In i_hi;
   In i_lo;
   In proc;
   In proc_number;
   In proc_remain;
   In task_proc;
   In task_remain;

   proc_number = proc_last + 1 - proc_first;

   i_hi = 0;

   task_remain = task_number;
   proc_remain = proc_number;

   for ( proc = proc_first; proc <= proc_last; proc++ )
   {
      task_proc = i4_div_rounded(task_remain, proc_remain);

      proc_remain = proc_remain - 1;
      task_remain = task_remain - task_proc;

      i_lo = i_hi;
      i_hi = i_hi + task_proc;
      res.emplace_back(std::make_pair(i_lo,i_hi));
   }
   return res;
}

/**
 *
 **/
In ForLoopImpl::i4_div_rounded
   (  In a
   ,  In b
   )
{
   In a_abs;
   In b_abs;
   static In i4_huge = 2147483647;
   In value;

   if ( a == 0 && b == 0 )
   {
      value = i4_huge;
   }
   else if ( a == 0 )
   {
      value = 0;
   }
   else if ( b == 0 )
   {
      if ( a < 0 )
      {
         value = - i4_huge;
      }
      else
      {
         value = + i4_huge;
      }
   }
   else
   {
      a_abs = abs ( a );
      b_abs = abs ( b );

      value = a_abs / b_abs;
      //
      //  Round the value.
      //
      if ( ( 2 * value + 1 ) * b_abs < 2 * a_abs )
      {
         value = value + 1;
      }
      //
      //  Set the sign.
      //
      if ( ( a < 0 && 0 < b ) || ( 0 < a && b < 0 ) )
      {
           value = - value;
      }
   }
   return value;
}

}  /* namespace detail */
}  /* namespace mpi */
}  /* namespace midas */
