#ifndef MIDAS_MPI_INFO_H_INCLUDED
#define MIDAS_MPI_INFO_H_INCLUDED

#include <string>
#include "mpi/Guard.h"

namespace midas
{
namespace mpi
{

//
class Communicator;

/**
 * Hold information on some global mpi variables.
 **/
struct Info
{
   public:
      enum mpi_log_type { log_none = 0, log_stdout = 1, log_file = 2 };

   private:
      //! Is MIDAS compiled with MPI
   #ifdef VAR_MPI
      static constexpr bool mCompiledWithMpi = true;
   #else 
      static constexpr bool mCompiledWithMpi = false;
   #endif /* VAR_MPI */
      //! Are we in an mpi region.
      static bool mMpiRegion;
      //! Are we in a threaded region.
      static bool mThreadedRegion;
      //! Which node is the global master.
      static int  mMasterRank;
      //! The level of threading provided by MPI
      static int mMpiThreadingProvided;
      //!  Global rank of this process.
      static int mGlobalRank;
      //!  Global size of world
      static int mGlobalSize;
      //! Global Communicator
      static Communicator mCommunicatorWorld;
      //! Self Communicator
      //static Communicator mCommunicatorSelf;
      //! Do logging
      static mpi_log_type mDoLogging;
   
   public:
      //! Initialize mpi info
      static void Initialize();
      
      //!@{
      //! Friends <3
      friend void Initialize(int*, char***);
      friend bool IsMpiRegion();
      friend int MasterRank();     
      friend bool IsMaster();
      friend int GlobalRank();
      friend int GlobalSize();
      friend int ThreadingProvided();
      friend std::string ThreadingProvidedString();
      friend bool IsThreadMultiple();
      friend const Communicator& CommunicatorWorld();
      friend Guard StartMpiRegion();
      friend mpi_log_type DoLogging();
      //!@}
};

//inline Info::mpi_log_type operator&(const Info::mpi_log_type& l, const Info::mpi_log_type& r)
//{
//   return static_cast<Info::mpi_log_type>(static_cast<int>(l) & static_cast<int>(r));
//}
//
//inline Info::mpi_log_type operator|(const Info::mpi_log_type& l, const Info::mpi_log_type& r)
//{
//   return static_cast<Info::mpi_log_type>(static_cast<int>(l) | static_cast<int>(r));
//}

//! Initialize MPI
void Initialize(int* argc, char*** argv);

//! Are we in an mpi region
bool IsMpiRegion();

//! What is the master rank
int MasterRank();

//! Is this the master process
bool IsMaster();

//! Get global rank.
int GlobalRank();

//! Get global rank.
int GlobalSize();

//! Get provided threading level.
int ThreadingProvided();

//! Get provided threading level as string. 
std::string ThreadingProvidedString();

//! Get the world communicator
const Communicator& CommunicatorWorld();

//! Do logging?
Info::mpi_log_type DoLogging();

//! Start an mpi region
Guard StartMpiRegion();

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_INFO_H_INCLUDED */
