#ifndef MIDAS_MPI_SLAVE_GUARD_H_INCLUDED
#define MIDAS_MPI_SLAVE_GUARD_H_INCLUDED

#include <functional>

namespace midas
{
namespace mpi
{

/**
 *
 **/
class Guard
{
   private:
      //! Function to call on destructor
      std::function<void()> mCallback;

      //!
      bool mDoCallback = true;

   public:
      //! 
      Guard(Guard&& aOther)
      {
         std::swap(mCallback, aOther.mCallback);
         aOther.mDoCallback = false;
      }

      //!
      Guard(const Guard&) = delete;

      //! Constructor
      template<class F>
      Guard(F&& aCallback)
         :  mCallback(std::forward<F>(aCallback))
      {
      }
      
      //! Destructor will call the callback
      ~Guard()
      {
         if (mDoCallback) 
         {
            mCallback();
         }
      }
};

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_SLAVE_GUARD_H_INCLUDED */
