/**
************************************************************************
* 
* @file                SlaveDriver.h
*
* Created:             10-1-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   
*                      
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "mpi/Communicator.h"

namespace midas
{
namespace mpi
{

/**
 * Will send all slaves to an infinite loop waiting for tasks sent from master process/rank.
 * After recieving a task, the corresponding slave function will be called from the task table.
 *
 * @param aCommunicator    The communicator handling communication between master and slaves.
 **/
void SlaveDriver(const Communicator& aCommunicator);

} /* namespace mpi */
} /* namespace midas */
