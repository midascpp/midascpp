#include "mpi/Signal.h"

#include <iostream>

#include "util/Error.h"
#include "mpi/TypeDefs.h"
#include "mpi/Wrapper.h"
#include "mpi/MpiLog.h"

namespace midas
{
namespace mpi
{

/**
 * Convert MPI signal to string for debug print-out.
 * Currently can only handle top-level signals,
 * and signals from local MPI-drivers (e.g. inside the PES module)
 * will return "UNKNOWN" 
 * (signal integer value are still printed in the below functions).
 *
 * @param aSignal    
 *    The signal.
 *
 * @return
 *    Returns a string describing the signal.
 **/
std::string SignalToString
   (  int aSignal
   )
{
   switch(aSignal)
   {
      case ERROR:
         return {"ERROR"};
      case STOP:
         return {"STOP"};
      case INPUT:
         return {"INPUT"};
      case PES:
         return {"PES"};
      case VSCF:
         return {"VSCF"};
      case VCC:
         return {"VCC"};
      case TDH:
         return {"TDH"};
      case TDVCC:
         return {"TDVCC"};
      case TEST:
         return {"TEST"};
      case SYSTEM:
         return {"SYSTEM"};
      default:
         return {"UNKNOWN"};
   }
}

/**
 * Send signal to all slaves in communicator.
 *
 * @param aCommunicator   
 *    The communicator to send the signal to.
 * @param aSignal         
 *    The signal to send.
 **/
void SendSignal
   (  const Communicator& aCommunicator
   ,  int aSignal
   )
{
   if constexpr (MPI_DEBUG) 
   {
      WriteToLog("Entered SendSignal()");
   }

#ifdef VAR_MPI
   // only master can run a command
   if(!aCommunicator.IsMaster())
   {
      MIDASERROR(" ERROR : SendSignal() cannot be called from Slave ");
   }
   
   // Send the task nr to slaves.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Broadcasting signal : " + SignalToString(aSignal) + " (" + std::to_string(aSignal) + ").");
   }
   auto status = detail::WRAP_Bcast(&aSignal, 1, MPI_INT, aCommunicator.GetRank(), aCommunicator.GetMpiComm());
#endif /* VAR_MPI */
}

/**
 * Receive signal from master rank in communicator.
 *
 * @param aCommunicator  
 *    The communicator to receive on.
 *
 * @return 
 *    Returns the received signal.
 **/
int ReceiveSignal
   (  const Communicator& aCommunicator
   )
{
   if constexpr (MPI_DEBUG) 
   {
      WriteToLog("Entered ReceiveSignal()");
   }

#ifdef VAR_MPI
   if(aCommunicator.IsMaster())
   {
      MIDASERROR(" ERROR : ReceiveSignal() cannot be called from Master ");
   }
   
   // Receive task from master.
   int task;
   auto ret = detail::WRAP_Bcast(&task, 1, MPI_INT, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm());
   if(ret != MPI_SUCCESS)
   {
      MidasWarning(" WARNING : In ReceiveSignal MPI_Bcast failed.");
   }

   if constexpr (MPI_DEBUG)
   {
      WriteToLog("Received signal : " + SignalToString(task) + " (" + std::to_string(task) + ").");
   }
   
   // Return the received signal.   
   return task;
#else
   // If we are not doing MPI we return an error signal, as there should be no slaves to receive on
   return signal::ERROR;
#endif /* VAR_MPI */
}

} /* namespace mpi */
} /* namespace midas */
