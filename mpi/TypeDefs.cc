#include "TypeDefs.h"

#ifndef VAR_MPI
MPI_Comm MPI_COMM_WORLD = nullptr;
MPI_Comm MPI_COMM_NULL  = nullptr;
#endif /* VAR_MPI */
