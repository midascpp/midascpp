/**
************************************************************************
* 
* @file                mpi/Interface.h
*
* Created:             10-1-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Interface class for the entire program to use even when
*                      MPI is not in effect
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_INTERFACE_H_INCLUDED
#define MIDAS_MPI_INTERFACE_H_INCLUDED

#include <string>

#ifdef VAR_MPI
#include <mpi.h>
#include "mpi/Wrapper.h"
#endif /* VAR_MPI */

#include "inc_gen/TypeDefs.h"


namespace midas
{
namespace mpi
{

/**
 *
 **/
struct Interface
{
   private:
      //! Constructer is private to prevent use, this should only be constructed through get_mpi_communicator!
      Interface() = default;

      //!@{
      //! We do not allow copy/move construction/assignment, so we delete the defaults
      Interface(const Interface&) = delete;
      Interface(Interface&&) = delete;
      Interface& operator=(const Interface&) = delete;
      Interface& operator=(Interface&&) = delete;
      //!@}
   
   public:
      //!
      In          GetMpiRank()         const;
      //!
      In          GetMpiNrProc()       const;
      //!
      In          GetMpiGlobalRank()   const;
      //!
      In          GetMpiGlobalNrProc() const;
      //!
      std::string GetMpiTreeString()   const;
      
      //! Friend function to force construction of mpi::Interface.
      friend Interface& get_Interface();
};

//! Interface function to force contruction of mpi Interface
Interface& get_Interface();

//! Check that we have a Network-File-System (NFS)
void CheckNfs();

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_INTERFACE_H_INCLUDED */
