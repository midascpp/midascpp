#!/bin/bash

#file = the Midas.out file
#file2 = the irspec.dat file
#lim = which peaks are to be included based on their intensity relative to the most intense peak
#minweightinclude = what is to be included based on their contribution to the peak

file=$1
lim=$2
file2=$3
minweightinclude=$4
outputdir=$5
blocks=$(grep "Block #" $file | wc -l)


offset=`awk '{if(NR>6){if(height < $2){height = $2; print height*1.025-height}}}' intervall=$intervall intervalr=$intervalr $file2 | tail -1`

echo $offset

let arrayindex=0

declare -a ARRAY

for((i=1;i<=$blocks;i++))
do
   checkweight=`grep -m$i -A3 "Block #" $file | tail -1 | awk -F' ' '{if($4 > lim){printf lim ":" $4}}' lim=$lim`
   if ! test "$checkweight" = ""
   then
      ARRAY[arrayindex]=$i
      ((arrayindex++))
   fi
done

echo '\begin{center}
\begin{threeparttable}
\caption{}
\label{}
\begin{tabular}{ccccc}
\hline \hline
 & Interval $(cm^{-1})$ & peak poss. &\% of largest& Analysis \\
\hline' > $outputdir/ir_table

echo '#set term aqua
set term postscript enhanced
set output "weights_graph.eps"
' > $outputdir/spec.plot

j=1

for i in ${ARRAY[@]}
do
   echo "iteration "$i
	interval=`grep -m$i "states included" $file | tail -1 | sed 's/-1/\$\^\{-1\}\$/g' | cut -d'(' -f1`
   nreigvals=`grep -m$i "states included" $file | tail -1 | sed 's/-1/\$\^\{-1\}\$/g' | cut -d'(' -f2 | cut -d' ' -f1`
   analysis=`grep -m$i -A1 "Largest weights" $file | tail -1 | awk -F'+' '{for(k = 1; (k<=NF); k++){print $k}}' | awk -F' ' '{if($1 >= minweightinclude){printf $0 "+"}}' minweightinclude=$minweightinclude | sed 's/\(.*\)+/\1/'`
   relsize=`grep -m$i -A3 "Block #" $file | tail -1 | cut -d' ' -f 8`
   export intervall=`echo $interval | cut -d' ' -f 1`
   export intervalr=`echo $interval | cut -d' ' -f 3`
   peakposs=`awk '{if(NR>6){ if($1 > intervall && $1 < intervalr){if(height < $2){height = $2; print $1}}}}' intervall=$intervall intervalr=$intervalr offset=$offset $file2 | tail -1`
   echo $j' & '$interval' & '$peakposs' & '$relsize' & $'$analysis'$ \\ '>> $outputdir/ir_table
   
   numtoinc=`grep "In Block nr : $i " $file | wc -l`
   height=`awk '{if(NR>6){ if($1 > intervall && $1 < intervalr){if(height < $2){height = $2; print height+offset}}}}' intervall=$intervall intervalr=$intervalr offset=$offset $file2 | tail -1`
   heightnr=`awk '{if(NR>6){ if($1 > intervall && $1 < intervalr){if(height < $2){height = $2; print height+offset*2}}}}' intervall=$intervall intervalr=$intervalr offset=$offset $file2 | tail -1`
   echo 'set arrow '$i' from '$intervall','$height' to '$intervalr','$height' nohead' >> $outputdir/spec.plot
   echo 'set label '$i'"'$((j++))'" at '$intervall','$heightnr >> $outputdir/spec.plot
   echo '' >> $outputdir/spec.plot
done

echo 'plot "'$file2'" u 1:2 w lines lt 1 notitle' >> $outputdir/spec.plot
        
echo '\hline\hline
\end{tabular}
\end{threeparttable}
\end{center}' >> $outputdir/ir_table
