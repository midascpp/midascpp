#!/bin/bash
#****
# Script to setup midas generic pes interface using Ruby scripting
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: This script sets up the ruby scripts that can be used
#               to interface MidasCpp with ES programs in generic pes construction.
#               The script should be run in the savedir of pes that needs
#               to interface using hte ruby interface.
#               Afterwards user need to supply the interface input
#               in the file rb_scripts.input.
#
#****
PWD=$(pwd)

if [ ! -s $PWD/rb_script.input ]
then
cat > $PWD/rb_script.input << %EOF%
#**
# scripts input file
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#**

#**
# Here follows all COMPULSARY keywords
#**
%run_es_command=
%number_of_atoms=

%input_template_begin
#put input template here!
%input_template_end

%prop_general_template_begin
#put prop general template here!
%prop_general_template_end

#**
# here follows optional templates
#**
#%validate_template_begin
#if validate, then put validate template here!
#%validate_template_end

#%cartrot_xyz_template_begin
# optionally put cartrot template here!
#%cartrot_xyz_template_end
%EOF%
fi

cat > $PWD/include.rb << %EOF%
#*
#  ruby includes
#
#     author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#     Description: Common functions and classes used by 
#                  the three interfacing ruby scripts
#
#*

require "pathname"

class File
   def go_to_keyword(keyword)
      self.seek(0)
      self.each_line do |line|
         if line.match(/#{keyword}/)
            return true
         end
      end
      return false
   end
end

class String
   def remove_comments
      replace self.gsub(/#.*\n/,"")
   end
   def remove_leading_whitespace
      replace self.gsub(/\A\s*/,"")
   end
   def remove_trailing_whitespace
      replace self.gsub(/\s*\Z/,"")
   end
   def remove_apostrophe
      replace self.gsub(/'/,"")
   end
   def strip_line
      remove_comments
      remove_leading_whitespace
      remove_trailing_whitespace
      remove_apostrophe
   end
   def blank_line?
      return self.match(/\A\s*\Z/)
   end
end

class Keywordmap
   def vars
      @key = Array.new()
      @value = Array.new()
   end
   def initialize
      vars
   end
   def add_pairs_from_file(script_input_file)
      script_input_file.each_line do |line|
         line.strip_line
         if line.match(/ *%.+=.*/)
            line_split = line.split("=",2)
            @key.insert(0,line_split[0])
            @value.insert(0,line_split[1].gsub(/\n/,""))
         end
      end
   end
   def add_pair(keyword, value)
      @key.insert(0,keyword)
      @value.insert(0,value)
   end
   def remove_pair(keyword, value)
      @value.delete_at(@key.index(keyword))
      @key.delete(keyword)
   end
   def add(line)
      line_split=line.split("=",2)
      add_pair(line_split[0],line_split[1])
   end
   def find_keyword(keyword)
      return @key.index(keyword) ? @value[@key.index(keyword)] : nil
#return @key.index(keyword) ? @value[@key.index(keyword)] : ""
   end
   def find_all_keyword(sub_keyword)
      return @key.find_all{|keyword| keyword.match(/#{sub_keyword}/)}
   end
   def substitute_into_line(line)
#puts "line: "+line
#      while match=line.match(/%\w*%?/) do
#         puts "match: "+match.to_s
#         line.gsub!(match.to_s,find_keyword(match.to_s))
#      end
      @key.each do |keyword|
         line.gsub!(keyword,find_keyword(keyword))
      end
#puts line
      return line
   end
   def find_substituted_keyword(keyword)
      value=find_keyword(keyword)
      if value
         substitute_into_line(value)
      end
      return value
   end
end

class Coordmap < Keywordmap
   def create_coord_map_from_xyz(xyz_coord_file,coord_conversion_factor)
      xyz_coord_file.seek(0)
      xyz_coord_file.readline  
      xyz_coord_file.readline  
      
      atom_num=1
      xyz_coord_file.each_line do |line|
         line_split = line.split
         for i in 1..3
            line_split[i] = "%18.17f" % (line_split[i].to_f*coord_conversion_factor.to_f)
            line_split[i] = line_split[i].to_s
         end
         add_pair("%atom_"+atom_num.to_s,line_split[0])
         add_pair("%x_coord_"+atom_num.to_s,line_split[1])
         add_pair("%y_coord_"+atom_num.to_s,line_split[2])
         add_pair("%z_coord_"+atom_num.to_s,line_split[3])
         atom_num=atom_num+1
      end
   end
end

class Filetemplate
   def initialize
      @name=nil
      @lines=Array.new
   end
   def add_line(line)
      @lines.push(line)
   end
   def create_template(file,name,end_line)
      @name=name
      while line=file.gets do
         line.match(/#{end_line}/) ? break : @lines.push(line)
      end
   end
   def each_line(&block)
      @lines.each(&block)
   end
   def name?
      if @name
         return true
      else
         return false
      end
   end
   def get_name
      return @name
   end
end

def open_file(path_name, read_write)
   pn = Pathname.new(path_name)
   if pn.exist?
      return File.open(pn,read_write)
   else
      puts "Couldn't find file: "+path_name
      puts "I will now exit"
      exit(40)
   end
end

def find_value(es_output_file, line)
   es_output_file.seek(0)
   if line[%r[\*+]]
      range=(line=~%r[\*])..(line=~%r[\*])+(line[%r[\*+]].size)
   else
      range=2..1 #empty range
   end
   line = line.gsub("*",".")
   regular_expression=/#{line}/
   es_output_file.each_line do |file_line|
     if file_line.match(regular_expression)
         value = file_line[range]
         value = value.gsub(/\s+/, "")
         return value,true
     end
   end
   return "",false
end

def do_exports(keyword_map)
   keyword_map.find_all_keyword("%export").each do |keyword|
      export=keyword.split("_",2)[1]
      export.upcase!
      if ENV[export]
         ENV[export]+=File::PATH_SEPARATOR+keyword_map.find_substituted_keyword(keyword)
      else
         ENV[export]=keyword_map.find_substituted_keyword(keyword)
      end
   end
end

def run_command(command)
   process_status = IO.popen(command)
   Process.wait
end
%EOF%

cat > $PWD/create_input.rb << %EOF%
#!/usr/bin/env ruby
#**************
# input creator script for midascpp (using ruby)
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: creates input from rb_script.input file
#               provided by user
#
#*************

load '$PWD/include.rb'
   
def write_geometry(midas_input_xyz, input_file, coord_conversion_factor)
   midas_input_xyz.seek(0)
   midas_input_xyz.readline
   midas_input_xyz.readline
   
   midas_input_xyz.each_line do |line|
      line_split = line.split
      for i in 1..3
         line_split[i] = "%18.17f" % (line_split[i].to_f*coord_conversion_factor.to_f)
         line_split[i] = line_split[i].to_s
      end
      full_line = line_split[0]+" "+line_split[1]+" "+line_split[2]+" "+line_split[3]
      input_file.write(full_line+"\n")
   end
end

def create_input(keyword_map,input_file_templates)
   midas_input_xyz=open_file("midasifc.xyz_input","r")
   coord_conversion_factor=keyword_map.find_substituted_keyword("%input_xyz_conversion_factor")
   coord_map=Coordmap.new
   coord_map.create_coord_map_from_xyz(midas_input_xyz,coord_conversion_factor)
   input_file_name=keyword_map.find_substituted_keyword("%es_input_file_name")
   if input_file_name.nil?
      input_file_name="input.inp" # default
   end
   
   input_file_templates.each do |file_template|
      file_template.name? ? file_name=file_template.get_name : file_name=input_file_name
      input_file=File.open(file_name,"w")
      file_template.each_line do |line|
         line.strip_line
         if line.match(/%geometry_xyz/)
            write_geometry(midas_input_xyz,input_file,coord_conversion_factor)
         else
            keyword_map.substitute_into_line(line)
            coord_map.substitute_into_line(line)
            input_file.write(line+"\n")
         end
      end
      input_file.close
   end
end

def create_input_main()
   keyword_map=Keywordmap.new
   input_file_templates=Array.new
   script_input_file=File.open("$PWD/rb_script.input","r")
   script_input_file.each_line do |line|
      line.strip_line
      case line
         when /%input_template_begin/
            input_file_template=Filetemplate.new
            input_file_template.create_template(script_input_file,line.split("=",2)[1],"%input_template_end")
            input_file_templates.push(input_file_template)
         when /%.+=.*/
            keyword_map.add(line)
      end
   end
   script_input_file.close # close input 
   if not keyword_map.find_keyword("%number_of_atoms")
      input_xyz_file=File.open("midasifc.xyz_input","r") # this will need dir probably!
      line=input_xyz_file.gets
      line.strip_line
      keyword_map.add_pair("%number_of_atoms",line)
   end
   if not keyword_map.find_keyword("%input_xyz_conversion_factor")
      keyword_map.add_pair("%input_xyz_conversion_factor","1")
   end
   do_exports(keyword_map)
   create_input(keyword_map,input_file_templates)
   if value=keyword_map.find_substituted_keyword("%os_run_create_input")
      run_command(value)
   end
end

create_input_main
%EOF%

cat > $PWD/run.rb << %EOF%
#!/usr/bin/env ruby
#****
# run script
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: Runs ES point for midas and creates midasifc.prop_general
#               with the properties given in rb_script.input
#
#****

load '$PWD/include.rb'

def run_es_program(keyword_map)
   files_to_copy=keyword_map.find_substituted_keyword("%files_to_copy_to_scratch")
   directory=keyword_map.find_keyword("%dir")
   if (not files_to_copy.nil?) && (not files_to_copy.empty?)
      files_to_copy=files_to_copy.split(" ")
      files_to_copy.each do |file|
         if not file.empty? # can i remove this check ?...
            process_status = IO.popen("cp "+file+" "+directory)
            Process.wait
         end
      end
   end
   command=keyword_map.find_substituted_keyword("%run_es_command")
   if not command
      puts "no run es command given"
      exit(40)
   end
   process_status = IO.popen("cd "+directory+"; "+command)
   Process.wait
end

def get_properties(keyword_map, prop_general_template)
   es_output_file_name=keyword_map.find_substituted_keyword("%es_output_file_name")
   if es_output_file_name.empty?
      es_output_file_name="output.out"
   end
   directory=keyword_map.find_keyword("%dir")
   es_output_file=File.open(directory+"/"+es_output_file_name,"r")
   prop_info_file=File.open(directory+"/midasifc.prop_general","w")
   
   prop_general_template.each_line do |line|   
      line.strip_line
      if not line.blank_line?
         line_split = line.split("&")
         #TODO: MAKE VARIABLE FILE!!! not just es_output
         line_split[1],line_found = find_value(es_output_file,line_split[1])
         prop_info_file.write(line_split[0])
         prop_info_file.write(" ")
         if line_found
            line_split[1]=line_split[1].gsub(/\n/,"")
            prop_info_file.write(line_split[1])
         else
            prop_info_file.write("not_found")
         end
         prop_info_file.write("\n")
      end
   end
   prop_info_file.close
   es_output_file.close
end

def find_coord(coord_file,line_split,directory,conversion_factor)
#TODO: add variable conversion factor!
   if (not line_split[4].nil?) && (not line_split[4].empty?)
      coord_file=open_file(directory+"/"+line_split[4],"r")
   end
   x_coord, found_x = find_value(coord_file, line_split[1])
   y_coord, found_y = find_value(coord_file, line_split[2])
   z_coord, found_z = find_value(coord_file, line_split[3])
   x_coord = ("%18.17f" % (x_coord.to_f*conversion_factor.to_f)).to_s
   y_coord = ("%18.17f" % (y_coord.to_f*conversion_factor.to_f)).to_s
   z_coord = ("%18.17f" % (z_coord.to_f*conversion_factor.to_f)).to_s
   if (not line_split[4].nil?) && (not line_split[4].empty?)
      coord_file.close
   end
   return x_coord, y_coord, z_coord
end

def create_rotated(keyword_map, cartrot_template)
   directory=keyword_map.find_keyword("%dir")
   cartrot_xyz_file=File.open(directory+"/midasifc.cartrot_xyz","w")
   es_output_file_name=keyword_map.find_substituted_keyword("%es_output_file_name")
   if es_output_file_name.empty?
      es_output_file_name="output.out"
   end
   es_output_file=open_file(directory+"/"+es_output_file_name,"r")
   coord_conversion_factor=keyword_map.find_substituted_keyword("%cartrot_xyz_conversion_factor")

   cartrot_template.each_line do |line|
      line.strip_line
      keyword_map.substitute_into_line(line)
      line_split = line.split("&")
      if line_split.size >= 4
         line_split[1],line_split[2],line_split[3] = find_coord(es_output_file, line_split, directory, coord_conversion_factor)
         cartrot_xyz_file.write(line_split[0]+" "+line_split[1]+" "+line_split[2]+" "+line_split[3]+"\n")
      else
         cartrot_xyz_file.write(line+"\n")
      end
   end
   es_output_file.close
   cartrot_xyz_file.close
end

def run_main()
   keyword_map=Keywordmap.new
   keyword_map.add_pair("%dir",ARGV[0])
   prop_general_template=Filetemplate.new
   cartrot_xyz_template=Filetemplate.new
   script_input_file=File.open("$PWD/rb_script.input","r")
   script_input_file.each_line do |line|
      line.strip_line
      case line
         when /%prop_general_template_begin/
            prop_general_template.create_template(script_input_file,"","%prop_general_template_end")
         when /%cartrot_xyz_template_begin/
            cartrot_xyz_template.create_template(script_input_file,"","%cartrot_xyz_template_end")
         when /%.+=.*/
            keyword_map.add(line)
      end
   end
   script_input_file.close
   if not keyword_map.find_keyword("%number_of_atoms")
      input_xyz_file=File.open("midasifc.xyz_input","r") # this will need dir probably!
      line=input_xyz_file.gets
      line.strip_line
      keyword_map.add_pair("%number_of_atoms",line)
   end
   if not keyword_map.find_keyword("%cartrot_xyz_conversion_factor")
      keyword_map.add_pair("%cartrot_xyz_conversion_factor","1")
   end
   do_exports(keyword_map)
   run_es_program(keyword_map)
   get_properties(keyword_map, prop_general_template)
   if cartrot_xyz_template.name?
      create_rotated(keyword_map, cartrot_xyz_template)
   end
   ## below is obsolete 
   #process_status = IO.popen("touch "+ARGV[1])
   #Process.wait
end

run_main
%EOF%

cat > $PWD/validate.rb << %EOF%
#!/usr/bin/env ruby
#****
# Midas Validation script (using ruby)
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: validates calculation using rb_script.input
#               file provided by user
#               This is only used if Validation is 
#               specified in the main MidasCpp input file.
#
#**********

# OPTIONAL TODO: make it so that we always validate midasifc.prop_general??..

load '$PWD/include.rb'

def check_bound(found_line,value,line)
   line = line.gsub("x",value)
   if found_line && eval(line)
      return true
   else
      return false
   end
end

def check_error(es_output_file, line_split, num_error)
   if (not line_split[3].nil?) && (not line_split[3].empty?)
      es_output_file=open_file(line_split[3],"r")
   end
   value,found_line = find_value(es_output_file, line_split[1])
   if (not line_split[2].nil?) && (not line_split[2].empty?)
      in_bound = check_bound(found_line,value,line_split[2])
   else
      in_bound = true
   end
   if (not found_line) || (not in_bound)
      num_error=num_error+1
   end
   if (not line_split[3].nil?) && (not line_split[3].empty?)
      es_output_file.close
   end
   return num_error
end

def check(keyword_map, validate_template)
   num_error=0
   num_suspicious=0
   es_output_file_name=keyword_map.find_substituted_keyword("%es_output_file_name")
   if es_output_file_name.empty?
      es_output_file_name="output.out"
   end
   es_output_file=open_file(es_output_file_name,"r")
   
   validate_template.each_line do |line|
      line.strip_line
      if not line.empty?
         keyword_map.substitute_into_line(line)
         line_split = line.split("&")
         if line_split[0].match(/error/)
            num_error = check_error(es_output_file,line_split,num_error)
         elsif line_split[0].match(/suspicious/)
            num_suspicious = check_error(es_output_file,line_split,num_suspicious)
         else
            puts "Error!!"
         end
      end
   end
   
   puts [num_error,num_suspicious]

## BELOW IS NOW OBSOLETE!
## write file midasifc.generic_error
#   generic_error_file=File.open("midasifc.generic_error","w")
#   generic_error_file.write(num_error.to_s+"\n")
#   generic_error_file.write(num_suspicious.to_s+"\n")
#   if (num_error>0) && (files_to_copy_on_error=keyword_map.find_substituted_keyword("%files_to_copy_on_error"))
#      generic_error_file.write(files_to_copy_on_error+" ")
#   end
#   if (num_suspicious>0) && (files_to_copy_if_suspicious=keyword_map.find_substituted_keyword("%files_to_copy_if_suspicious"))
#      generic_error_file.write(files_to_copy_if_suspicious)
#   end
end

def validate_main()
   keyword_map = Keywordmap.new
   validate_template = Filetemplate.new
   script_input_file=open_file("$PWD/rb_script.input","r")
   script_input_file.each_line do |line|
      line.strip_line
      case line
         when /%validate_template_begin/
            validate_template.create_template(script_input_file,nil,"%validate_template_end")
         when /%.+=.*/
            keyword_map.add(line)
      end
   end
   script_input_file.close
   do_exports(keyword_map)
   check(keyword_map, validate_template)
end

validate_main
%EOF%

chmod +x $PWD/create_input.rb
chmod +x $PWD/run.rb
chmod +x $PWD/validate.rb
