#!/bin/bash

###########################################################
# Utility functions
###########################################################
usage () {
   echo "Usage: $0 [options] tool [tool-options]"
   echo ""
   echo "Options:"
   echo "   -h,--help                     Display this message."
   echo "   -v,--version                  Display version information."
   echo ""
   echo "Tools:"
   echo "   datacont_diff_norm            Calculate the difference norm between two DataCont."
   echo "   datacont_to_ascii             Convert DataCont to ascii."
   echo "   tensordatacont_diff_norm      Calculate the difference norm between two TensorDataCont."
   echo "   tensordatacont_to_datacont    Convert TensorDataCont to DataCont."
   echo "   comparevibs                   Compare two .mmol molecule files."
   echo "   autocorr_spectrum             Calculate spectrum from an auto-correlation function."
   echo "   mctdh_wf_analysis             Analyse MCTDH wavefunction."
   echo ""
   echo "For more information on tools see man page for midastools."
}

version () {
   echo "midasutil Version 1.0.0"
}

###########################################################
# Input processing
###########################################################
while test $# -gt 0
do
   case $1 in
      -h | --help)
         usage
         exit 0
         ;;
      -v | --version)
         version
         exit 0
         ;;
      -*)
         echo "Unknown option '$1'." 
         ;;
      *)
         # First argument is the tool, rest are arguments passed to the tool
         tool=$1
         shift
         while test $# -gt 0
         do
            [[ -z $args ]] && args=$1 || args=$args" "$1
            shift
         done
         ;;
   esac
   shift
done

###########################################################
# Carry out commands
###########################################################

# datacont_diff_norm
if [[ "$tool" == "datacont_diff_norm" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/datacont_diff_norm.x $args
# datacont_to_ascii
elif [[ "$tool" == "datacont_to_ascii" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/datacont_to_ascii.x $args
# tensordatacont_diff_norm
elif [[ "$tool" == "tensordatacont_diff_norm" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/tensordatacont_diff_norm.x $args
# tensordatacont_to_datacont
elif [[ "$tool" == "tensordatacont_to_datacont" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/tensordatacont_to_datacont.x $args
# comparevibs
elif [[ "$tool" == "comparevibs" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/comparevibs.x $args
# autocorr_spectrum
elif [[ "$tool" == "autocorr_spectrum" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/autocorr_spectrum.x $args
# mctdh_wf_analysis
elif [[ "$tool" == "mctdh_wf_analysis" ]]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/mctdh_wf_analysis.x $args
else
   echo "'" $tool "' is not recognized as a MidasCpp tool!" 
fi


# vim:syntax=sh
