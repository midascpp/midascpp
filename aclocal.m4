m4_include([config/acx_blas.m4])
m4_include([config/acx_lapack.m4])
m4_include([config/ax_lapack_version.m4])
m4_include([config/check_gnu_make.m4])
m4_include([config/acx_mpi.m4])
dnl m4_include([config/acx_getkw.m4])
m4_include([config/acx_f90_mpi.m4])
m4_include([config/acx_tinker.m4])
m4_include([config/acx_pthread.m4])
m4_include([config/ax_boost_base.m4])
m4_include([config/ax_compare_version.m4])

AC_DEFUN([ACX_BUILD_FLAGS],[. ./config/$1.conf])

AC_DEFUN([ACX_SUBST_BUILD_FLAGS],[
[. ./config/$1.conf]
dnl ian: this is a hack to include the new c++ standard to all build types
cxxflags=$cxxflags" -std=c++17"
cxxflags_debug=$cxxflags_debug" -std=c++17"

fcflags=$fcflags" "$FCFLAGS
ccflags=$ccflags" "$CFLAGS
cxxflags=$cxxflags" "$CXXFLAGS
fcflags_debug=$fcflags_debug" "$FCFLAGS
ccflags_debug=$ccflags_debug" "$CFLAGS
cxxflags_debug=$cxxflags_debug" "$CXXFLAGS

AC_SUBST(FCFLAGS, $fcflags)
AC_SUBST(CCFLAGS, $ccflags)
AC_SUBST(CXXFLAGS, $cxxflags)
AC_SUBST(AR, $ar)
AC_SUBST(ARFLAGS, $arflags)

dnl MBH: Dedicated flags for debug build.
AC_SUBST(FCFLAGS_DEBUG, $fcflags_debug)
AC_SUBST(CCFLAGS_DEBUG, $ccflags_debug)
AC_SUBST(CXXFLAGS_DEBUG, $cxxflags_debug)

]) #ACX_SUBST_BUILD_FLAGS
