MidasCpp Changelog
==============

Midas 2022.04.0 
===========

New Features
-----------
   -  MidasCpp release executables will be stripped using the `strip` commandline tool, if available.
      This behavior can be disabled using the `--disable-strip` configure option.
   -  Added TDMVCC[2] method (for 2-mode-coupled Hamiltonians) with option for autocorrelation function.
   -  Adds new paradigms for forking new processes and running external programs.
      These new options can be set with the `--spawn-paradigm` command-line option.
      The default is the old way, `fork_exec`, where MidasCpp just fork's and exec's directly.
      The two new ways are `fork_server` and `mpi_comm_spawn`, 
      using an external job server for `fork() + exec()`, or using `MPI_Comm_spawn` for spawning processes,
      respectively. This also adds a new exectuable distributed with MidasCpp, 
      `midascpp_fork_server` which is the external fork-server, 
      which should be running in the background on each node when
      using `--spawn-paradigm=fork_server`. 
      (NOTE: `mpi_comm_spawn` paradigm is currently disabled).
   -  Will now print some information on which MPI implementation and version is used
      in the .mout file, when using MPI.
   -  Added man-pages for `midascpp`, `midascpp_fork_server`, `midasutil`, `midasdynlib`, and `midastools` executables.
   -  Added `--help` and `--version` options to `midastools` and `midasutil`.
   -  Added `--enable-lto` option to `configure`, which will enable Link-Time optimizations
      when compiling MidasCpp.

Minor Bugfixes
-----------
   -  Fixed a bug which could cause integer overflow in ADGA-pes. Occured for large number of ADGA iterations and large systems.
   -  Fixed a bug in the MPI implementation of the coordinate rotation schemes in MidasCpp,
      which would make the calculation stall.

Changed defaults
-----------
   -  Moves `vim`, `autocomp`, `template`, and `data` folders to `share/midascpp`,
      as they contain static architecture independent data.

Midas 2021.10.0 (Quality-of-Life update)
===========

New Features
-----------
   -  Added option for restarting a Tdvcc/Tdmvcc calculation by reading necessary parameters from a file.
     (The writing of these files are also new.)
   -  MidasCpp will now print a nice ASCII logo at the top of the `.mout` output file.
   -  Added the option for calculating the normal coordinates through the #3 ModVibCoord keyword.
   -  MidasCpp can now be configured with the `--with-<extlib>-url` for all libraries and not just LibBoost,
      e.g. `--with-gsl-url=/path/to/gsl-1.16.tar.gz`.
   -  MidasCpp now compiles with Clang 10.x.x (and possibly newer).

Minor Bugfixes
-----------
   -  Fixed an issue where level 0 and 1 keywords could not be indented. 
      Most `.minp` keywords should now be properly indentable.
   -  Updated the default URL for LibBoost. This means MidasCpp should be able to download the library automatically again.
   -  Updated pointgroup symmetry for molecules so the code no longer assumes how the molecule is oriented i.e. how the symmetry operations are defined according to axes.

Changed defaults
-----------
   - Removed the reading of frequencies from the ORCAHESS reader. This is done because MidasCpp uses a different
     set of masses for the nuclei, and the frequencies/normalcoordinates from ORCA would therefore not be 
     correct internally in MidasCpp.

Source code structural changes
-----------
   - Added the file 'input/Macros.h' which defines a set of macros to use for defining variables and Set/Get functions in CalcDefs.
     This has been implemented to all the CalcDefs in the ModSys heirarchy and Td hierarchy.
   - Put the CalcDefs of the ModSys hierarchy inside the midas::input namespace.
   - Put the CalcDefs of the Td hierarchy inside the midas::input namespace.
   - Changed the name of the Name() member function in vscf, vcc and rsp CalcDefs to GetName() to align with the standard naming conventions for CalcDefs.

Midas 2021.04.1 (Hotfix)
===========

Bugfix release!

We identified a bug where if the modes were given unique identifiers/names,
the program would crash or give wrong results, if the names were not given in
alphabetical order. This problem has affected both the PES and the wavefunction code.
If no mode names have been given these problems will not have occured,
so this should only affected the specialist options where mode names are provided explicitly.

Major Bugfixes
-----------
   - Fixed that the mode order had to be in alphabetic order in the `.mmol` ADGA input.
     This means that if the modes were not alphabetically ordered in older versions of the code results would come out wrong!
     This has now been fixed, and mode order can now be arbitrary.
     This only applies to cases where mode names were given explicitly in the `.mmol` file.
   - Fixed an issue where the program would either crash or give wrong results,
     if `#2 ModeNames` were provided in `.mmop` if the terms under `#2 OperatorTerms` were not in the same order w.r.t. the mode names.
     This only applies to cases where `#2 ModeNames` has been used.
   - Fixed a bug with the checks in the `test_suite/TEST`-script, where for some tests the check would always evaluate to `true`
     if some of the test criteria had been removed or were empty. 
     This is no longer the case, and all checks should now be done correctly.

Minor Bugfixes
-----------
   - Fixed a minor bug/issue with `midacpp.source_me.template`, where if `CPLUS_INCLUDE_PATH` was empty,
     sourcing the script would mess up the dependency file generation used by the build system.
     This fix only has impact for developers or people who are changing and recompiling the code 
     and who are also actively using a `midascpp.source_me` file.

Changed defaults
-----------
   - The adaptive ADGA method now only splits if the sum of errors across the property surfaces being optimized are larger than the threshold.
     This only has an effect if running multistate adaptive ADGA.
   - The default URL for downloading GSL (Gnu Scientific Library) has been changed to use Gnu's own mirror.

Source code structural changes
-----------
   - Added `pes/adga` subdirectory, and moved all files relating directly to the iterative grids to here, renamed `ItGridBoxes` -> `ItGrid`.
   - ADGA now first checks for convergence for all boxes, then splits the unconverged boxes. 
     Instead of splitting the box immediately after finding out it is not converged.

Midas 2021.04.0
===========

Feature and bugfix release.

New Features
-----------
   - Added the calculation code to the second line of the midasifc.xyz_input files for PES generation using generic single points.
   - New wave-function methods
   - Added TDMVCC[k] method with time-dependent modals (using full-space matrix implementation)

Bugfixes
-----------
   - Fixes a problem with the capping procedure for non-covalently bonded fragments within Falcon, and adds an option to turn it off (`#4 NoCapping` under `#3 ModVibCoord`).
   - Fixes incrorrectly generated maximum powers for polynomial fit functions in DIF potential constructions.

Changed defaults
-----------
   - Changed the .fitbas files to start reading their keywords from level 1 instead of level 3.
   - Changed the fitbasis input heirachy, please consult the manual and the examples in the test_suite and example suite for a detailed explanation.
   - Changed the #0MidasMopInput keyword to #0MidasOperator and #0MidasMopInputEnd to #0MidasOperatorEnd, using the old keywords results in a warning.
   - Changed the #0MoleculeInput keyword to #0MidasMolecule and #0MoleculeInputEnd to #0MidasMoleculeEnd, using the old keywords results in a warning.

Midas 2020.10.0
===========

Feature and bugfix release.

New Features
-----------
   - Warnings can now be treated as hard-errors by setting enviroment variable `MIDAS_WARNING_AS_ERROR=1` before running MidasCpp.
   - Adds derivates to Morse potentials (gradient and Hessian), Partridge-Schwenke (Gradient) and FilePot (Gradient and Hessian) potentials
   - Revamped MidasPot handling to streamline the handling of the calculated potentials across the PES and WF module
   - Added MR-MCTDH[n] methods for performing truncated MCTDH calculations on multi-reference systems
   - Vectorized FilePot, Morse and Double-Well Potentials allowing for multiple properties from each of these types of SP_MODEL potentials.
   - Added pes_Adga_MultiState test to the pes test suite to test Multistate ADGA

Bugfixes
-----------
   - Adds `#include <string>` header to `mpi/Info.h`.
   - Adds `#include <ostream>` header to `util/Io_fwd.h`.
   - Correctly add keywords `#2 NORMALIZENORMALCOORDINATES` and `#2 ORTHONORMALIZENORMALCOORDINATES`, reintrocuces check and optional orthonomalizatrion of the Normal Coordinates.
   - Restored the pes_extrap1 test to a working state and added it to the PES test suite
   - FilePot now works again, i.e. one can do a PES calculation using a previously calculated PES with a supplied .mop + .mmol file.

Changed defaults
-----------
   - When constructing a PES, it is now a hard error if the normalcoordinates are not ortho-normal.
     The checking threshold is defaulted to `1e-7` and can be manipulated using `#2 NORMALCOORDINATETHRESHOLD` keyword.

Midas 2020.04.0
===========

Feature and bugfix release.

New Features
-----------
- General
   - Added new `midastools` tool which provides access to some data analysis tools.
   - Added new `midasdynlib` tool for PES dynamic library introspection.
   - All executables (`midascpp`, `midastools`, `midasutil`, and `midasdynlib`) now have bash autocompletion,
     which can either be installed locally with `midasutil` or loaded when sourcing a `midascpp.source_me` (preferred).
   - For reproduceability of results, MidasCpp now writes which compiler and what version was used to compile the program in the `.mout` header.
- Operators:
   - Kinetic energy terms are no longer added to operators, unless it has type `energy`, or set explicitly with `#3 KineticEnergy`.
   - Adds option `USER_DEFINED` for `#3 KineticEnergy`, 
     which should be used if kinetic terms are explicitly typed as operator terms in the operator file or definition.
   - Add keyword `#1 SetInfo` to `.mop` files. This works as `#3 SetInfo` in the `#2 Operator` block of `.minp` input.
   - Types can now be defined for properties in `midasifc.mpropinfo` when creating a PES/property-surface.
     Types defined here will be included in the corresponding `.mop` files.
   - All operator types that take frequency input under `#3 SetInfo`, will now have frequencies indexed, starting from `0`, e.g. `frq_0=0.0`.
- MidasPot^(tm)
   - Added optional `#2 REFERENCEVALUE` keyword to  operator input. This allows directly connecting an operator with a reference value and avoiding the .mrefval file, e.g. for MidasPot generation.
   - New keyword for creating MidasPot #2 TENSOROPERATOR. Will take a set/tensor of operators to be rotated together when rotations occur (e.g. kabsch rotation).
- New wave-function methods
   - Added TDVCC[2] method with time-independent modals (for 2-mode-coupled Hamiltonians)
   - Added TDEVCC[k] method with time-independent modals (using full-space matrix implementation)
   - Added EVCC[k] ground state solver (using full-space matrix implementation)
   - Added MCTDH and MCTDH[n] methods
- Machine-Learning options:
   - Added Sparse Gaussian Process Regression
   - Added kernel based Farthest point sampling
   - Added kernel normalization to increase numerical stability
   - Added Multilayer Gaussian Process Regression  
- ADGA PES construction
   - Double incremental expansion in Falcon coordinates (DIF)

Bugfixes
-----------
- Fixed some clang++ compilation warnings (tested with versions 5.0.1 and 6.0.0)
- Fixed a bug where operators where sorted alphabetically when merging multi-level surfaces. 
  This meant that one could end up calculating VSCF energies with a non-energy operator,
  if the energy operators name was not alphabetically first.
  Operators are no longer sorted, and the order will be taken from `midasifc.mpropinfo`.
- Fixed the `Makefile` system to better work on Mac. 
  Some commands did not work properly on Mac OS, but these have now been generalized.
- Fixed an issue where compilation with MPI would fail if `MPI_Comm` and `MPI_Datatype` had the same underlying type.
- Fixed bug when using `MPI_Bcast` with tensors of size 1

Changed defaults
-----------

Midas 2019.10.0
===========

Feature and bugfix release.

New Features
-----------
- MidasPot^(tm)
    - Now has the option to rotate input coordinates to reference, if linked with lapack.
    - Pes bounds will now be checked by default with a threshold of 1e-12. Check can be disabled.
 
- Interface to Midas Machine Learning algorithms
    - Stand-alone interface to Gaussian Process Regression (GPR)
        - Generation of internal coordinates based on XYZ file
        - Training based on internal coordinates, interatomic distances or xyz coordinates
        - Inference based on derivative information (squared exponential kernel)

- PES construction
    - Extrapolation of higher-order mode couplings using GPR
    - Using GPR predictor for Single Point Calculations 

- State-transfer operators
    - Operator files can now state transfer operators, |I><J|(Q), which shift occupation from |J> to |I>
      in mode Q. If using state transfer, no other types of operators can be used in a given mode.

Bugfixes
-----------
- MidasPot^(tm)
    - Operator bounds file is now actually optional.
- Falcon
    - Auxiliary coordinates are properly generated.


Changed defaults
-----------
- Integral accuracy
    - The default accuracy of the Gauss-Legendre quadrature for calculating integrals of B-splines has been increased. 
    - The old default was to calculate the nodes and weights to an accuracy of 1.e-14.
    - The new default is to use a threshold of 10 times machine epsilon. This can be changed on input.



Midas 2019.07.0
===========

Feature and bugfix release.

New Features
-----------

- Vibrational coordinates
    - Flexible adaptation of local coordinates of nuclei (FALCON) - distance-based coupling estimates
- Static grid PES construction
    - Double incremental expansion in Falcon coordinates (DIF)
    - Double incremental expansion in Falcon coordinates with auxiliary coordinate transformation (DIFACT)

Bugfixes
-----------
- Bugfix to ensure consistency of mass weighting when working with non default isotopes. This bug led to failures when constructing PESs using non-default isotopes in the previous version. 

Changed defaults
-----------

- ADGA
   - Default for `#2 AdgaScalFac` is now `1.0 1.0 15.0 30.0 30.0` instead of `1.0 2.0 15.0 30.0 30.0`, which means that the two-mode part is tighter converged than before.

Midas 2019.04.0
===========

Initial Release

New Features
-----------

- VSCF
    - ss. Energies
    - RSP Expectation Values
- VMPn
    - ss. Energies
- VAPTn
    - ss. Energies
- VCI[n]
    - Response - excitation energiesm and std. rsp. func
    - Damped response for IR and Raman
    - Lanczos response solver
- VCC[n]
    - VCC[2]/H2 Two-mode VCC, energy + lin. response
    - VCC[n] energy and lin. response
    - VCC[n] ground state tensor decomposition
    - VCC[n] damped response
    - Lanczos response solver
- ADGA PES construction
- Static grid PES construction
- Taylor Force Field
- Vibrational coordinates
    - Normal coordinates
    - Optimized, localized and Hybrid coordinates
