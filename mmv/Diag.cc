/**
************************************************************************
* 
* @file                Diag.cc
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing friends for diagonalization of a MidasMatrix.
* 
* Last modified: Wed Dec 16, 2009  11:26AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "mmv/Diag.h"

// std headers
#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>
#include<type_traits>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/Warnings.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/StorageForSort.h"
#include "util/Io.h"
#include "input/Input.h"

// using declarations
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

extern "C" void dgeev_(char *jobvl, char *jobvr, int *n, double *a,  int *lda, double *wr, double *wi, double *vl,
      int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);
// dgeev is slightly slower than eispack RG if N>200!!!
// dgeevx maybe faster than dgeev for larger problems. The strategy might be dgeev if N<200, dgeevx if N>=200.

extern "C" void dgeevx_(char *balanc, char *jobvl, char *jobvr, char *sense, int *n, double *a, int *lda, double *wr, 
       double *wi, double *vl, int *ldvl, double *vr, int *ldvr, int *ilo, int *ihi, double *scale, 
       double *abnrm, double *rconde, double *rcondv, double *work, int *lwork, int *iwork, int *info);

//extern "C" int rg_(int *nm, int *n, double *a, double *wr, double *wi, int *matz, double *z, int *iv1, double *fv1,
//      int *ierr);

extern "C" void dsyevd_(char *jobz, char *uplo, int *n, double *a, int *lda, double *w, double *work, int *lwork,
      int *iwork, int *liwork, int *info);
// dsyevd is slightly slower than dsyevr, but faster than dsyev or dsyevx for all N.
// (http://www.netlib.org/lapack/lug/node71.html)

extern "C" void dsygvd_(int *itype, char *jobz, char *uplo, int *n, double *a, int *lda, double *b, int *ldb, 
      double *w, double *work, int *lwork, int *iwork, int *liwork, int *info);

extern "C" void dggev_(char *jobvl, char *jobvr, int *n, double *a, int *lda, double *b, int *ldb, double *alphar, 
   double *alphai, double *beta, double *vl, int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);

extern "C" void dgesdd_(char *jobz, int *m, int *n, double *a, int *lda,double *s, double *u, int *ldu, double *vt,
      int *ldvt, double *work, int *lwork, int *iwork, int *info );

extern "C" void dsbev_(char *jobz, char *uplo, int *n, int *kd, double *ab, int *ldab, double *w,
                       double *z, int *ldz, double *work, int *info);

extern "C" void dsbevd_(char *jobz, char *uplo, int *n, int *kd, double *ab, int *ldab, double *w,
                       double *z, int *ldz, double *work, int *lwork, int *iwork,
                       int *liwork, int *info);
///< dsbevd is faster than dsbev but takes more memory, and might fail on old computers.
///< This should not be a problem, and should be used as default for banded matrices.


/**
* A friend for diagonalization of a matrix.
* */
void Diag(MidasMatrix& arM, MidasMatrix& arEigVec, MidasVector& arEigVal, 
      string aHow, bool aStoreEigVec,bool aOrderAbsVals, In aSuperDiag)
{
   vector<Nb> dummy_vec;
   MidasMatrix dummy_MM(I_0);
   Diag(arM,arEigVec,arEigVal,aHow,dummy_vec,dummy_MM,aStoreEigVec,aOrderAbsVals, aSuperDiag);
}

/**
* A friend for diagonalization of a matrix.
* */
void Diag(MidasMatrix& arM, MidasMatrix& arEigVec, MidasVector& arEigVal,
          string aHow, vector<Nb>& arEigValReIm, MidasMatrix& arMetric, bool aStoreEigVec,
          bool aOrderAbsVals, In aSuperDiag)
{
   MidasVector dummy_vec(arEigVal.Size());
   MidasMatrix dummy_mat(I_0);
   Diag(arM,arEigVec,arEigVal,dummy_vec,dummy_mat,aHow,arEigValReIm,arMetric,aStoreEigVec,aOrderAbsVals,aSuperDiag);
}
/**
* A friend for diagonalization of a matrix.
* */
void Diag
   (  MidasMatrix& arM
   ,  MidasMatrix& arEigVec
   ,  MidasVector& arEigVal
   ,  MidasVector& arImEigVal
   ,  MidasMatrix& arLEigVec
   ,  string aHow
   ,  vector<Nb>& arEigValReIm
   ,  MidasMatrix& arMetric
   ,  bool aStoreEigVec
   ,  bool aOrderAbsVals
   ,  In aSuperDiag
   )
{
   if (gDebug || gIoLevel > 10) Mout << " Matrix diagonalization using " << aHow << " algorithm" << endl;
   bool aftertreat=false; // ian: why do have this variable? every method uses aftertreatment anyways...
   arEigValReIm.clear();
   vector<MidasVector> eig_vec_re_im;

   // Mode 1 
   // NR routine X denoted by  aHow = "NR_X", X = ......
   if (aHow=="NR_JACOBI")
   {
      if (gDebug) Mout << " Using NR diagonalization routine " << aHow << endl;
      MidasMatrix m_copy(arM); // Ok make a copy of arM so we can test it later.
      In nrot;
      nr_jacobi(m_copy, arEigVal,arEigVec,nrot);
      arEigVec.NormalizeColumns();
      aftertreat = true;
   }
   // Mode 2 
   else if (aHow=="DGEEVX")
   {
      if (gDebug) Mout << " Using diagonalization routine " << aHow << endl;
      //Mout << " The method is DGEEVX. " << endl;
      char BALANC = 'B';
      char JOBVL = 'N';
      char JOBVR = 'V';
      char SENSE = 'N';
      int N = arM.Nrows();
      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = std::max(1,N);
      double *WR = new double[N];
      double *WI = new double[N];
      double *VL = new double[N*N];//If JOBVL = 'N', VL is not referenced. So what?
      int LDVL = N;
      double *VR = new double[N*N];
      int LDVR = N;
      int ILO = 0;//output
      int IHI = 0;//output
      double *SCALE = new double[N];//output
      double ABNRM = 0;//output
      double *RCONDE = new double[N];//output
      double *RCONDV = new double[N];//output
      //int LWORK = N*(N+6);
      int LWORK = max(10000000,N*(N+6));
      double *WORK = new double[LWORK];
      int *IWORK = new int[2*N-2];
      int INFO = 0;

      // Computes for an N-by-N real nonsymmetric matrix A, the
      // eigenvalues and the right eigenvectors.
      dgeevx_(&BALANC,&JOBVL,&JOBVR,&SENSE,&N,A,&LDA,WR,WI,VL,&LDVL,VR,&LDVR,&ILO,&IHI,SCALE,&ABNRM,RCONDE,RCONDV,WORK,&LWORK,IWORK,&INFO);
      if(INFO != I_0)
      {
         MIDASERROR(" Eigensolution failed ... ");
      }

      if (N!=arM.Nrows() || N!=LDA)
      {
         if (N!=arM.Nrows()) Mout << " N!=arM.Nrows(): " << N << "!=" << arM.Nrows() <<  endl;
         if (N!=LDA) Mout <<  " N!=LDA: " << N << "!=" << LDA <<  endl;
         MIDASERROR(" N!=arM.Nrows() || N!=LDA ");
      }
      for (In i=I_0;i<N;i++)
      {
         arEigVal[i]=WR[i];
         //Mout << " eigval[" << i << "] = " <<  arEigVal[i] << endl;
      }
      MidasVector cmp_eig_val(N);
      bool save_cmp_val=true;
      for (In i=I_0;i<N;i++)
      {
         cmp_eig_val[i]=WI[i];
         if (fabs(cmp_eig_val[i])>C_0)
         {
            Mout << " Nonzero imag. part was found: " <<  arEigVal[i] << "+i*("<<  cmp_eig_val[i]
               << ") for eigval #" << i << endl;
            //cmp_idx.push_back(i);
            if (save_cmp_val)
            {
               arEigValReIm.push_back(arEigVal[i]);
               arEigValReIm.push_back(cmp_eig_val[i]);
            }
            save_cmp_val=!save_cmp_val;
         }
      }
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
           arEigVec[i][j]=VR[counter];
            counter++;
         }
      }
      //Mout << " Eigen vector matrix \n" << arEigVec << endl;
      eig_vec_re_im.reserve(arEigValReIm.size());
      //Mout << " cmp_idx.size: " << cmp_idx.size() << " cmp_idx: " << cmp_idx << endl;

      In i=I_0;
      while (i<N)
      {
         if (fabs(cmp_eig_val[i])>C_0)
         {
            Nb norm_re=arEigVec.NormCol(i);
            Nb norm_im=arEigVec.NormCol(i+I_1);
            Nb norm=sqrt(norm_re*norm_re+norm_im*norm_im);
            //Mout << " Norm of eig.vecs #" <<  i << ": "<< norm_re << endl
            //   << " Norm of eig.vecs #" <<  i+I_1 << ": "<< norm_im << endl;
            arEigVec.ScaleCol(C_1/norm,i); 
            arEigVec.ScaleCol(C_1/norm,i+I_1); 
            MidasVector tmp_mv(N);
            arEigVec.GetCol(tmp_mv,i,I_0,I_1);
            eig_vec_re_im.push_back(tmp_mv);
            Mout << " Complex conjugate eig.vec #" <<  i << " is saved" << endl;
            arEigVec.GetCol(tmp_mv,i+I_1);
            eig_vec_re_im.push_back(tmp_mv);
            Mout << " Complex conjugate eig.vec #" <<  i+I_1 << " is saved" << endl;
            i+=I_1;
         }
         else
         {
            Nb norm=arEigVec.NormCol(i);
            arEigVec.ScaleCol(C_1/norm,i); 
         }
         i+=I_1;
      }

      aftertreat = true;

      delete[] A; 
      delete[] WR;
      delete[] WI;
      delete[] VL;
      delete[] VR;
      delete[] SCALE;
      delete[] RCONDE;
      delete[] RCONDV;
      delete[] WORK;
      delete[] IWORK;
   }
   else if (aHow=="DGEEV")
   {
      if (gDebug) Mout << " Using diagonalization routine " << aHow << endl;
      char JOBVL = 'V';
      char JOBVR = 'V';
      int N = arM.Nrows();

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = N;
      double *WR = new double[N];
      double *WI = new double[N];
      double *VL = new double[N*N];
      int LDVL = N;
      double *VR = new double[N*N];
      int LDVR = N;
      //int LWORK = 4*N;
      int LWORK = max(10000000,4*N);
      double *WORK = new double[LWORK];
      int INFO = 0;

      //Computes the eigenvalues and the right eigvectors of an N-by-N real nonsymmetric matrix A
      dgeev_(&JOBVL,&JOBVR,&N,A,&LDA,WR,WI,VL,&LDVL,VR,&LDVR,WORK,&LWORK,&INFO);
      if(INFO != I_0)
         MIDASERROR(" Eigensolution failed ... ");

      if (N!=arM.Nrows() || N!=LDA)
      {
         if (N!=arM.Nrows()) Mout << " N!=arM.Nrows(): " << N << "!=" << arM.Nrows() <<  endl;
         if (N!=LDA) Mout <<  " N!=LDA: " << N << "!=" << LDA <<  endl;
         MIDASERROR(" N!=arM.Nrows() || N!=LDA ");
      }
      for (In i=I_0;i<N;i++)
      {
         arEigVal[i]=WR[i];
         //Mout << " eigval[" << i << "] = " <<  arEigVal[i] << endl;
      }
      MidasVector cmp_eig_val(N);
      bool save_cmp_val=true;
      for (In i=I_0;i<N;i++)
      {
         arImEigVal[i]=WI[i];
         if (fabs(arImEigVal[i])>C_0)
         {
            Mout << " Nonzero imag. part was found: " <<  arEigVal[i] << "+i*("<<  arImEigVal[i]
               << ") for eigval #" << i << endl;
            //cmp_idx.push_back(i);
            if (save_cmp_val)
            {
               arEigValReIm.push_back(arEigVal[i]);
               arEigValReIm.push_back(arImEigVal[i]);
            }
            save_cmp_val=!save_cmp_val;
         }
      }
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
           arEigVec[i][j]=VR[counter];
            counter++;
         }
      }
      if(arLEigVec.Nrows() != I_0)
      {
         counter = I_0;
         for(In j=I_0;j<N; j++)
         {
            for(In i=I_0;i<N;i++)
            {
               arLEigVec[i][j]=VL[counter];
               counter++;
            }
         }
      }
      //Mout << " Eigen vector matrix \n" << arEigVec << endl;
      eig_vec_re_im.reserve(arEigValReIm.size());
      //Mout << " cmp_idx.size: " << cmp_idx.size() << " cmp_idx: " << cmp_idx << endl;

      In i=I_0;
      while (i<N)
      {
         // EigVecs are already normalized... This step shouldn't be necessary!
         if (fabs(arImEigVal[i])>C_0)
         {
            Nb norm_re=arEigVec.NormCol(i);
            Nb norm_im=arEigVec.NormCol(i+I_1);
            Nb norm=sqrt(norm_re*norm_re+norm_im*norm_im);
            //Mout << " Norm of eig.vecs #" <<  i << ": "<< norm_re << endl
            //   << " Norm of eig.vecs #" <<  i+I_1 << ": "<< norm_im << endl;
            arEigVec.ScaleCol(C_1/norm,i); 
            arEigVec.ScaleCol(C_1/norm,i+I_1); 
            MidasVector tmp_mv(N);
            arEigVec.GetCol(tmp_mv,i);
            eig_vec_re_im.push_back(tmp_mv);
            //Mout << " Complex conjugate eig.vec #" <<  i << " is saved" << endl;
            arEigVec.GetCol(tmp_mv,i+I_1);
            eig_vec_re_im.push_back(tmp_mv);
            //Mout << " Complex conjugate eig.vec #" <<  i+I_1 << " is saved" << endl;
            i+=I_1;
         }
         else
         {
            Nb norm=arEigVec.NormCol(i);
            arEigVec.ScaleCol(C_1/norm,i); 
         }
         i+=I_1;
      }

      aftertreat = true;

      delete[] A; 
      delete[] WR;
      delete[] WI;
      delete[] VL;
      delete[] VR;
      delete[] WORK;
   }
   else if ( aHow == "MIDAS_JACOBI" )
   {
      if (gDebug) Mout << " Using diagonalization routine " << aHow << endl;
      In nrot = I_100;
      MidasMatrix m_copy(arM); // Ok make a copy of arM so we can test it later.
      Jacobi(m_copy, arEigVal,arEigVec,nrot);
      arEigVec.NormalizeColumns();
      aftertreat = true;
   }
   else if (aHow == "DSYEVD")
   {
      if (gDebug) Mout << " Using diagonalization routine " << aHow << endl;
      //Mout << " The method is DSYEVD. " << endl;
      char JOBZ = 'V';
      char UPLO = 'U';
      int N = arM.Nrows();

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = N;
      double *W = new double[N];
      //int LWORK = 1+6*N+2*N*N;
      int LWORK = max(10000000,1+6*N+2*N*N);
      int LIWORK = 3 + 5*N;
      double *WORK = new double[LWORK];
      int *IWORK = new int[LIWORK];
      int INFO = 0;
      //Mout << "N: " << N << " LWORK: " << LWORK << " LIWORK: " << LIWORK << endl;

      //Computes the eigenvalues and the eigvectors of an N-by-N real symmetric matrix A
      dsyevd_(&JOBZ,&UPLO,&N,A,&LDA,W,WORK,&LWORK,IWORK,&LIWORK,&INFO);

      if (INFO != I_0)
      {
         MIDASERROR(" Eigensolution failed ... ");
      }

      for (In i=I_0;i<N;i++)
      {
         arEigVal[i]=W[i];
         //Mout << " eigval[" << i << "] = " <<  arEigVal[i] << endl;
      }
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            arEigVec[i][j]=A[counter];
            counter++;
         }
      }
      arEigVec.NormalizeColumns();
      aftertreat = true;

      delete[] A; 
      delete[] W;
      delete[] WORK;
      delete[] IWORK;
   }
   else if (aHow=="DGGEV")
   {
      if (gDebug) Mout << " Using diagonalization routine " << aHow << endl;
      //Mout << " The method is DGGEV. " << endl;
      char JOBVL = 'N';
      char JOBVR = 'V';
      int N = arM.Nrows();

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }
      int LDA = N;

      // matrix B in column-major format
      double *B = new double[N*N];
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            B[counter]=arMetric[i][j];
            counter++;
         }
      }
      int LDB = N;

      double *ALPHAR = new double[N];//output
      double *ALPHAI = new double[N];//output
      double *BETA = new double[N];//output
      int LDVL = N;
      double *VL = new double[LDVL*N];
      int LDVR = N;
      double *VR = new double[LDVR*N];
      int LWORK = max(10000000,8*N);
      double *WORK = new double[LWORK];
      int INFO = I_0;

      // Solves Ax=lambdaBx generalized eigenvalue problem of an N-by-N real nonsymmetric matrix A
      // and the metric matrix B
      //Computes the eigenvalues and the right eigvectors 

      dggev_(&JOBVL,&JOBVR,&N,A,&LDA,B,&LDB,ALPHAR,ALPHAI,BETA,VL,&LDVL,VR,&LDVR,WORK,&LWORK,&INFO);

      if(INFO != I_0)
         MIDASERROR(" Eigensolution failed ... ");

      for (In i=I_0;i<N;i++)
      {
         if (BETA[i]!=C_0) 
         {
            arEigVal[i]=ALPHAR[i]/BETA[i];
         }
         else
         {
            Mout << " For eigval[" << i << "] BETA is zero!" << endl;
            MIDASERROR(" BETA is zero in DGGEV. ");
         }
      }
      MidasVector cmp_eig_val(N);
      bool save_cmp_val=true;
      for (In i=I_0;i<N;i++)
      {
         cmp_eig_val[i]=ALPHAI[i]/BETA[i];
         if (fabs(cmp_eig_val[i])>C_0)
         {
            //cmp_idx.push_back(i);
            //Mout << " ALPHAR[" << i << "]=" << ALPHAR[i] << endl;
            //Mout << " ALPHAI[" << i << "]=" << ALPHAI[i] << endl;
            //Mout << " BETA[" << i << "]=" << BETA[i] << endl;
            if (save_cmp_val)
            {
               //Mout << " I force the complex conjugate eigenvalues numerically correct! " <<  endl;
               arEigValReIm.push_back(arEigVal[i]);
               arEigValReIm.push_back(cmp_eig_val[i]);
               arEigVal[i+I_1]=arEigVal[i];
               ALPHAI[i+I_1]=-ALPHAI[i];
               BETA[i+I_1]=BETA[i];
            }
            save_cmp_val=!save_cmp_val;
            Mout << " Nonzero imag. part was found: " <<  arEigVal[i] << "+i*("<<  cmp_eig_val[i]
               << ") for eigval #" << i << endl;
         }
      }
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
           arEigVec[i][j]=VR[counter];
            counter++;
         }
      }
      //Mout << " Eigen vector matrix \n" << arEigVec << endl;
      eig_vec_re_im.reserve(arEigValReIm.size());
      //Mout << " cmp_idx.size: " << cmp_idx.size() << " cmp_idx: " << cmp_idx << endl;

      In i=I_0;
      while (i<N)
      {
         if (fabs(cmp_eig_val[i])>C_0)
         {
            Nb norm_re=arEigVec.NormCol(i);
            Nb norm_im=arEigVec.NormCol(i+I_1);
            Nb norm=sqrt(norm_re*norm_re+norm_im*norm_im);
            Mout << " Norm of eig.vecs #" <<  i << ": "<< norm_re << endl;
            //   << " Norm of eig.vecs #" <<  i+I_1 << ": "<< norm_im << endl;
            arEigVec.ScaleCol(C_1/norm,i); 
            arEigVec.ScaleCol(C_1/norm,i+I_1); 
            MidasVector tmp_mv(N);
            arEigVec.GetCol(tmp_mv,i);
            eig_vec_re_im.push_back(tmp_mv);
            //Mout << " Complex conjugate eig.vec #" <<  i << " is saved" << endl;
            arEigVec.GetCol(tmp_mv,i+I_1);
            eig_vec_re_im.push_back(tmp_mv);
            //Mout << " Complex conjugate eig.vec #" <<  i+I_1 << " is saved" << endl;
            i+=I_1;
         }
         else
         {
            //Nb norm=arEigVec.NormCol(i);
            Nb norm=arEigVec.MetricNormCol(i,arMetric);
            //Mout << "Sne skips normalization of reduced space vector " << endl;
            arEigVec.ScaleCol(C_1/norm,i); 
         }
         i+=I_1;
      }

      aftertreat = true; //sne changed this to true - lets try

      delete[] A; 
      delete[] B; 
      delete[] ALPHAR;
      delete[] ALPHAI;
      delete[] BETA;
      delete[] VL;
      delete[] VR;
      delete[] WORK;

   }
   else if(aHow == "DSBEV")
   {
      char JOBZ='V';
      char UPLO='U';
      int N = arM.Ncols();
      int KD = aSuperDiag;
      int LDAB = KD+I_1;

      double *AB = new double[LDAB*N];
      In counter=I_0;
      for(In j=I_0; j<N; ++j)
      {
         In i_init=j-KD;
         for(In i=i_init; i<=j; ++i)
         {   
            if(i<I_0)
            {
               AB[counter]=I_0;
               counter++;
            }
            else
            {
               AB[counter]=arM[i][j];
               counter++;
            }
         }
      }

      double *W = new double[N];
      int LDZ = N;
      double *Z = new double[LDZ*N];
      int work_init = max(1,3*N-2);
      double *WORK = new double[work_init];
      int INFO = 0;

      ///> Compute the eigenvalues
      dsbev_(&JOBZ,&UPLO,&N,&KD,AB,&LDAB,W,Z,&LDZ,WORK,&INFO);
      if(INFO != I_0)
      {
         MIDASERROR("In file Diag.cc; Diag(): DSBEV failed to diagonalize.");
      }

      for(In i=I_0; i<N; ++i)
         arEigVal[i] = W[i];

      counter=I_0;
      for(In j=I_0; j<N; ++j)
         for(In i=I_0; i<N; ++i)
         {
            arEigVec[i][j] = Z[counter];
            counter++;
         }     
      for(In i=I_0; i<N; ++i)
      {
         Nb norm=arEigVec.NormCol(i);
         arEigVec.ScaleCol(C_1/norm,i);
      }
      aftertreat=true;
      delete[] AB;
      delete[] W;
      delete[] WORK;
      delete[] Z;
   }
   else if(aHow=="DSBEVD")
   {
      char JOBZ='V';
      char UPLO='U';
      int N = arM.Ncols();
      int KD = aSuperDiag;
      int LDAB = KD+I_1;

      double *AB = new double[LDAB*N];
      In counter=I_0;
      for(In j=I_0; j<N; ++j)
      {
         In i_init=j-KD;
         for(In i=i_init; i<=j; ++i)
         {   
            if(i<I_0)
            {
               AB[counter]=I_0;
               counter++;
            }
            else
            {
               AB[counter]=arM[i][j];
               counter++;
            }
         }
      }

      double *W = new double[N];
      int LDZ = N;
      double *Z = new double[LDZ*N];
      int LWORK = 2*(1+5*N+2*N*N);
      double *WORK = new double[LWORK];
      int LIWORK = 2*(3+5*N);
      int *IWORK = new int[LIWORK];
      int INFO = 0;

      ///> Compute eigenvalues and eigvectors.
      dsbevd_(&JOBZ,&UPLO,&N,&KD,AB,&LDAB,W,Z,&LDZ,WORK,&LWORK,IWORK,&LIWORK,&INFO);
      if(INFO != I_0)
      {
         MIDASERROR("In file Diag.cc; Diag(): DSBEVD failed to diagonalize.");
      }

      for(In i=I_0; i<N; ++i)
         arEigVal[i] = W[i];

      counter=I_0;
      for(In j=I_0; j<N; ++j)
         for(In i=I_0; i<N; ++i)
         {
            arEigVec[i][j] = Z[counter];
            counter++;
         }     
      for(In i=I_0; i<N; ++i)
      {
         Nb norm=arEigVec.NormCol(i);
         arEigVec.ScaleCol(C_1/norm,i);
      }
      aftertreat=true;
      delete[] AB;
      delete[] W;
      delete[] WORK;
      delete[] Z;
      delete[] IWORK;
   }
   else 
   {
      Mout << " Unknown diagonalization method " << aHow << endl;
      MIDASERROR(" Unknown diagonalization method in Diag");
   }
   if (aftertreat)
   {
      In n = arEigVal.Size();
      //arEigVec.NormalizeColumns();
      // Eigenvalues are not necessarily in order:

      // --- ian: isn't this code redundant??
      /*if (arEigValReIm.size()>I_0)
      {
         In i=I_0;
         while (i<arEigValReIm.size())
         {
            In j=I_0;
            while (arEigVal[j]!=arEigValReIm[i]) j++;
            arEigVec.AssignCol(eig_vec_re_im[i],j);
            arEigVec.AssignCol(eig_vec_re_im[i+I_1],j+I_1);
            i+=I_2;
         }
      }*/
      // --- ?? end
      
      //
      // Sort eigenvalue and eigenvector (lowest eigenvalue comes first)
      //
      //Mout << " IN DIAG: " << std::endl;
      //Mout << " Reduced Matrix: " << arM << std::endl;
      //Mout << " Eigvals: " << arEigVal << std::endl;
      //Mout << " Eigvecs: " << arEigVec << std::endl;
      
      Sort3(arEigVal,arEigVec,arImEigVal,arLEigVec,aOrderAbsVals);
         

      bool test=false;
      if (test)
      {
         MidasVector tst(n);
         MidasVector col(n);
         for (In i=0;i<n;i++)
         {
            arEigVec.GetCol(col,i);
            tst = arM*col;
            tst -= arEigVal[i]*col;
            Nb tstnorm = tst.Norm();
            Nb norm = col.Norm();
            Nb tol_norm = C_I_10_12; // For double precision.
            if (gDebug || tstnorm/norm > tol_norm) 
            {
               if (tstnorm > tol_norm)
                  Mout << " Is something fishy going on?? Residual is quite large" << endl;
               Mout << " Residual root i " << i << " has Norm " << tstnorm
                  << " and sumele " << tst.SumEle() << endl;
               Mout << " root " << i << " EigenValue" << arEigVal[i]
                  << " Norm of EigenVector " << col.Norm() << endl;
               if (gDebug && gIoLevel > 20) Mout << " Eigenvector: " << col << endl;
            }
         }
      }
   }

}

/**
* A friend for performing a single JacobiRot on the matrix (which is changed).
* */
inline void JacobiRot(MidasMatrix &arM, const Nb aS, const Nb aTau, const In aI,
                const In aJ, const In aK, const In aL)
{
                Nb g=arM[aI][aJ];
                Nb h=arM[aK][aL];
                arM[aI][aJ]=g-aS*(h+g*aTau);
                arM[aK][aL]=h+aS*(g-h*aTau);
}

/**
* A friend for Jacobi diagonalization of a Matrix.
* Following numerical recipies (using own tools).
* */
void Jacobi(MidasMatrix& arM, MidasVector& arEigVal, MidasMatrix& arEigVec, In& aNrot)
{
   In nrotin = aNrot;
   aNrot = 0;
   In n_first = 4;

   In n = arM.Ncols();
   MidasVector b(n,C_0);
   MidasVector z(n,C_0);
   b.PutToDiag(arM);
   arEigVal.PutToDiag(arM);
   arEigVec.Unit();
      //Mout << " arM      " << endl << arM      << endl;
      //Mout << " arEigVec " << endl << arEigVec << endl;
      //Mout << " arEigVal " << endl << arEigVal << endl;
      //Mout << " nrotin  " << endl << nrotin << endl;
      //Mout << " b       " << endl << b      << endl;
      //Mout << " z       " << endl << z      << endl;
   

   Nb thresh;

   for (In iter=1;iter<nrotin;iter++)
   {
      Nb sum = arM.SumAbsOutOfDiagonalUpper();
      if (sum == C_0) return;                           //< Outofdiag = 0, done.

      if (iter<n_first)
      {
         thresh = C_I_5*sum/(n*n);
      }
      else
      {
         thresh = C_0;
      }
      //Mout << " arM      " << endl << arM      << endl;
      //Mout << " sum =   " << sum  << " thresh = " << thresh << endl;

      for (In ip=0;ip<n-1;ip++)
      {
         for (In iq=ip+1;iq<n;iq++)
         {
            //Mout << " arM " << arM[ip][iq] << " ip " << ip << " iq " << iq << " iter = " << iter << endl;
            Nb g=C_100*fabs(arM[ip][iq]);
            if (iter > n_first 
                  && (fabs(arEigVal[ip])+g) == fabs(arEigVal[ip])
                  && (fabs(arEigVal[iq])+g) == fabs(arEigVal[iq])) 
            {
               arM[ip][iq] = C_0;
            }
            else if  (fabs(arM[ip][iq]) > thresh)
            {
               Nb h = arEigVal[iq]-arEigVal[ip];
               Nb t;
               if ((fabs(h)+g) == fabs(h))
                  t=(arM[ip][iq])/h;
               else 
               {
                  Nb theta=C_I_2*h/(arM[ip][iq]);
                  t=C_1/(fabs(theta)+sqrt(C_1+theta*theta));
                  if (theta < C_0) t = -t;
               }
               Nb c   = C_1/sqrt(C_1+t*t);
               Nb s   = t*c;
               Nb tau = s/(C_1+c);
               h      = t*arM[ip][iq];
               //Mout << " t= " << t << " tau = " << tau << " h = " << h << endl;
               z[ip] -= h;
               z[iq] += h;
               arEigVal[ip] -= h;
               arEigVal[iq] += h;
               arM[ip][iq]=C_0;
               for (In j=0;j<ip;j++)
                  JacobiRot(arM,s,tau,j,ip,j,iq);
               for (In j=ip+1;j<iq;j++)
                  JacobiRot(arM,s,tau,ip,j,j,iq);
               for (In j=iq+1;j<n;j++)
                  JacobiRot(arM,s,tau,ip,j,iq,j);
               for (In j=0;j<n;j++)
                  JacobiRot(arEigVec,s,tau,j,ip,j,iq);
               aNrot++;
            }
            else
            {
               //Mout << " No hit" << endl;
            }
         }
      }
      for (In ip=0;ip<n;ip++) 
      {
          b[ip]       +=  z[ip];
          arEigVal[ip]  =  b[ip];
          z[ip]        =  C_0;
      }
   }
   //Mout << " aNrot = " << aNrot << endl;
   MIDASERROR( " too many iterations in routine jacobi" );
}

void nr_jacobi(MidasMatrix &a, MidasVector &d, MidasMatrix &v, In &nrot)
{
        In i,j,ip,iq;
        Nb tresh,theta,tau,t,sm,s,h,g,c;

        In n=d.Size();
        MidasVector b(n),z(n);
        for (ip=0;ip<n;ip++) {
                for (iq=0;iq<n;iq++) v[ip][iq]=0.0;
                v[ip][ip]=1.0;
        }
        for (ip=0;ip<n;ip++) {
                b[ip]=d[ip]=a[ip][ip];
                z[ip]=0.0;
        }
        nrot=0;
        for (i=1;i<=50;i++) {
                sm=0.0;
                for (ip=0;ip<n-1;ip++) {
                        for (iq=ip+1;iq<n;iq++)
                                sm += fabs(a[ip][iq]);
                }
                if (sm == 0.0)
                        return;
                if (i < 4)
                        tresh=0.2*sm/(n*n);
                else
                        tresh=0.0;
                for (ip=0;ip<n-1;ip++) {
                        for (iq=ip+1;iq<n;iq++) {
                                g=100.0*fabs(a[ip][iq]);
                                if (i > 4 && (fabs(d[ip])+g) == fabs(d[ip])
                                        && (fabs(d[iq])+g) == fabs(d[iq]))
                                                a[ip][iq]=0.0;
                                else if (fabs(a[ip][iq]) > tresh) {
                                        h=d[iq]-d[ip];
                                        if ((fabs(h)+g) == fabs(h))
                                                t=(a[ip][iq])/h;
                                        else {
                                                theta=0.5*h/(a[ip][iq]);
                                                t=1.0/(fabs(theta)+sqrt(1.0+theta*theta));
                                                if (theta < 0.0) t = -t;
                                        }
                                        c=1.0/sqrt(1+t*t);
                                        s=t*c;
                                        tau=s/(1.0+c);
                                        h=t*a[ip][iq];
                                        z[ip] -= h;
                                        z[iq] += h;
                                        d[ip] -= h;
                                        d[iq] += h;
                                        a[ip][iq]=0.0;
                                        for (j=0;j<ip;j++)
                                                JacobiRot(a,s,tau,j,ip,j,iq);
                                        for (j=ip+1;j<iq;j++)
                                                JacobiRot(a,s,tau,ip,j,j,iq);
                                        for (j=iq+1;j<n;j++)
                                                JacobiRot(a,s,tau,ip,j,iq,j);
                                        for (j=0;j<n;j++)
                                                JacobiRot(v,s,tau,j,ip,j,iq);
                                        ++nrot;
                                }
                        }
                }
                for (ip=0;ip<n;ip++) {
                        b[ip] += z[ip];
                        d[ip]=b[ip];
                        z[ip]=0.0;
                }
        }
        MIDASERROR("Too many iterations in routine nr_jacobi");
}
/**
* A friend for performing a singular value decomposition (moved here from PesFit).
*/
void svdcmp(MidasMatrix &a, MidasVector &w, MidasMatrix &v)
{

   std::string how = "DGESDD";
   if (gNumLinAlg == "MIDAS")
   {
      how = "MIDAS_SVD";
   }
   //#ifdef USE_MATHL
   if (how == "DGESDD")
   {
      if (gDebug||gIoLevel > I_10)
      {
         Mout << " Using SVD routine " << how << std::endl;
      }

      char JOBZ = 'S';
      int M = a.Nrows();
      int N = a.Ncols();

      // matrix A in column-major format
      double *A = new double[M*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<M; i++)
         {
            A[counter]=a[i][j];
            counter++;
         }
      }

      int LDA = M;
      double *S = new double[min(M,N)];
      int LDU = M;
      int UCOL = min(M,N); // UCOL=M if JOBZ='A'
      double *U = new double[LDU*UCOL];
      int LDVT = min(M,N); // =N if JOBZ='A'
      double *VT = new double[LDVT*N];
      int LWORK = min(10000000,3*min(M,N) + max(max(M,N),4*min(M,N)*min(M,N)+4*min(M,N)));
      int *IWORK = new int[8*min(M,N)];
      int INFO = 0;

      // mbh: Note!!! This is required in order to make dgesdd
      //      work for liblapack v. 3.0.3 on CenTOS
      //      Extremely ugly - should be investigated!!!
      LWORK=LWORK/I_4+LWORK;
      double *WORK = new double[LWORK];

      //Computes the singular value decomposition of a real M-by-N matrix A
      dgesdd_(&JOBZ,&M,&N,A,&LDA,S,U,&LDU,VT,&LDVT,WORK,&LWORK,IWORK,&INFO);
      if(INFO != I_0)
      {
         /**
         * If this failed we need to try doing it the midas 
         * way before giving up on the SVD method completely
         **/
         MidasWarning("Warning LaPack SVD failed with INFO : "+std::to_string(INFO)+" Trying to do MidasSVD instead");
         MidasSVD(a, w, v);
         delete[] A; 
         delete[] S; 
         delete[] U; 
         delete[] VT; 
         delete[] WORK;
         delete[] IWORK;
         return;
      }
      for (In i=I_0;i<min(M,N);i++)
      {
         w[i]=S[i];
      }
      counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<M; i++)
         {
            a[i][j]=U[counter];
            counter++;
         }
      }
      //Note that VT is the transpose of V=v
      counter = I_0;
      for(In i=I_0; i<LDVT; i++)
      {
         for(In j=I_0; j<N; j++)
         {
            v[i][j]=VT[counter];
            counter++;
         }
      }

      delete[] A; 
      delete[] S; 
      delete[] U; 
      delete[] VT; 
      delete[] WORK;
      delete[] IWORK;
   }
   else if (how=="MIDAS_SVD")
   {
      MidasSVD(a, w, v);
   }
   else 
   {
      Mout << " Unknown SVD method " << how << endl;
      MIDASERROR("Unknown SVD method");
   }
   //#endif
}
/*
* svdcmp routine, taken "asis" from Numerical Recipes in c++, pag. 70: 
* perform the SVD of a matrix A (= u * w * vT). On output the matrix u
* replace matrix A. Note also that th matrix v (and NOT its transpose) is
* returned.
*/
void MidasSVD(MidasMatrix &a, MidasVector &w, MidasMatrix &v)
{
   bool flag;
   In i=0 ,its=0, j=0, jj=0, k=0, l=0, nm=0;
   Nb anorm=C_0, c=C_0, f=C_0, g=C_0, h=C_0, s=C_0, scale=C_0, x=C_0 ,y=C_0, z=C_0;

   In m=a.Nrows();
   In n=a.Ncols();
   MidasVector rv1(n);
   g=scale=anorm=0.0;
   for (i=0;i<n;i++) {
      l=i+2;
      rv1[i]=scale*g;
      g=s=scale=0.0;
      if (i < m) {
         for (k=i;k<m;k++) scale += fabs(a[k][i]);
         if (scale != 0.0) {
            for (k=i;k<m;k++) {
               a[k][i] /= scale;
               s += a[k][i]*a[k][i];
            }
            f=a[i][i];
            g = -SIGN((Nb)sqrt(s),f);
            h=f*g-s;
            a[i][i]=f-g;
            for (j=l-1;j<n;j++) {
               for (s=0.0,k=i;k<m;k++) s += a[k][i]*a[k][j];
               f=s/h;
               for (k=i;k<m;k++) a[k][j] += f*a[k][i];
            }
            for (k=i;k<m;k++) a[k][i] *= scale;
         }
      }
      w[i]=scale *g;
      g=s=scale=0.0;
      if (i+1 <= m && i != n) {
         for (k=l-1;k<n;k++) scale += fabs(a[i][k]);
         if (scale != 0.0) {
            for (k=l-1;k<n;k++) {
               a[i][k] /= scale;
               s += a[i][k]*a[i][k];
            }
            f=a[i][l-1];
            g = -SIGN((Nb)sqrt(s),f);
            h=f*g-s;
            a[i][l-1]=f-g;
            for (k=l-1;k<n;k++) rv1[k]=a[i][k]/h;
            for (j=l-1;j<m;j++) {
               for (s=0.0,k=l-1;k<n;k++) s += a[j][k]*a[i][k];
               for (k=l-1;k<n;k++) a[j][k] += s*rv1[k];
            }
            for (k=l-1;k<n;k++) a[i][k] *= scale;
         }
      }
      anorm=MAX(anorm,(fabs(w[i])+fabs(rv1[i])));
   }
   for (i=n-1;i>=0;i--) {
      if (i < n-1) {
         if (g != 0.0) {
            for (j=l;j<n;j++)
               v[j][i]=(a[i][j]/a[i][l])/g;
            for (j=l;j<n;j++) {
               for (s=0.0,k=l;k<n;k++) s += a[i][k]*v[k][j];
               for (k=l;k<n;k++) v[k][j] += s*v[k][i];
            }
         }
         for (j=l;j<n;j++) v[i][j]=v[j][i]=0.0;
      }
      v[i][i]=1.0;
      g=rv1[i];
      l=i;
   }
   for (i=MIN(m,n)-1;i>=0;i--) {
      l=i+1;
      g=w[i];
      for (j=l;j<n;j++) a[i][j]=0.0;
      if (g != 0.0) {
         g=1.0/g;
         for (j=l;j<n;j++) {
            for (s=0.0,k=l;k<m;k++) s += a[k][i]*a[k][j];
            f=(s/a[i][i])*g;
            for (k=i;k<m;k++) a[k][j] += f*a[k][i];
         }
         for (j=i;j<m;j++) a[j][i] *= g;
      } else for (j=i;j<m;j++) a[j][i]=0.0;
      ++a[i][i];
   }
   for (k=n-1;k>=0;k--) {
      for (its=0;its<30;its++) {
         flag=true;
         for (l=k;l>=0;l--) {
            nm=l-1;
            if (fabs(rv1[l])+anorm == anorm) {
               flag=false;
               break;
            }
            if (fabs(w[nm])+anorm == anorm) break;
         }
         if (flag) {
            c=0.0;
            s=1.0;
            for (i=l-1;i<k+1;i++) {
               f=s*rv1[i];
               rv1[i]=c*rv1[i];
               if (fabs(f)+anorm == anorm) break;
               g=w[i];
               h=pythag(f,g);
               w[i]=h;
               h=1.0/h;
               c=g*h;
               s = -f*h;
               for (j=0;j<m;j++) {
                  y=a[j][nm];
                  z=a[j][i];
                  a[j][nm]=y*c+z*s;
                  a[j][i]=z*c-y*s;
               }
            }
         }
         z=w[k];
         if (l == k) {
            if (z < 0.0) {
               w[k] = -z;
               for (j=0;j<n;j++) v[j][k] = -v[j][k];
            }
            break;
         }
         if (its == 29) 
         {
            MIDASERROR("no convergence in 30 svdcmp iterations");
         }
         x=w[l];
         nm=k-1;
         y=w[nm];
         g=rv1[nm];
         h=rv1[k];
         f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
         g=pythag(f,1.0);
                     f=((x-z)*(x+z)+h*((y/(f+(Nb)SIGN(g,f)))-h))/x;
         c=s=1.0;
         for (j=l;j<=nm;j++) {
            i=j+1;
            g=rv1[i];
            y=w[i];
            h=s*g;
            g=c*g;
            z=pythag(f,h);
            rv1[j]=z;
            c=f/z;
            s=h/z;
            f=x*c+g*s;
            g=g*c-x*s;
            h=y*s;
            y *= c;
            for (jj=0;jj<n;jj++) {
               x=v[jj][j];
               z=v[jj][i];
               v[jj][j]=x*c+z*s;
               v[jj][i]=z*c-x*s;
            }
            z=pythag(f,h);
            w[j]=z;
            if (z) {
               z=1.0/z;
               c=f*z;
               s=h*z;
            }
            f=c*g+s*y;
            x=c*y-s*g;
            for (jj=0;jj<m;jj++) {
               y=a[jj][j];
               z=a[jj][i];
               a[jj][j]=y*c+z*s;
               a[jj][i]=z*c-y*s;
            }
         }
         rv1[l]=0.0;
         rv1[k]=f;
         w[k]=x;
      }
   }
}
/**
* SvBksb  routine, spirit of Numerical Recipes in c++, pag. 67: 
* solve the linear system
* A x = b with backsubstitution given the SVD of a matrix A (= u * w * vT):
* x = V w^-1 Ut b 
* Addition of Tikhonov regularization with t=aTikhonovParam 
* (A^T A)x + t x = A^T b => min( ||Ax - b||^2 + t ||x||^2 ) 
* (A^T A + t) x  = A^T b => x =  V F (U^T b), with F= w/(w^2 + t) 
**/

void SvBksb(MidasMatrix& u, MidasVector& w, MidasMatrix& v, MidasVector& b, MidasVector& x, bool aTikhonov, const Nb aTikhonovParam)
{
   In jj,j,i;
   Nb s;

   In m=u.Nrows();
   In n=u.Ncols();
   MidasVector tmp(n);
   if (aTikhonov)
   {
   // tmp = w/(w^2+t) * U^tb 
      for (j = I_0; j < n; j++) 
      {
         s = C_0;
         if (w[j] != 0)
         {
            for (i = I_0; i < m; i++) s += u[i][j]*b[i];
            s *= w[j]/(w[j]*w[j]+aTikhonovParam); // Tikhonov regularization 
         }
         tmp[j] = s;
      }
   }
   else
   {
      // tmp = w-1 * U^tb
      for (j = I_0; j < n; j++) 
      {
         s = C_0;
         if (w[j] != C_0)    // Standard svd cutting  
         {
            for (i = I_0; i < m; i++) s += u[i][j]*b[i];
            s /= w[j];                 
         }
         tmp[j] = s;
      }
   }
   // Common V*tmp 
   for (j = I_0; j < n; j++) 
   {
      s = C_0;
      for (jj = I_0; jj < n; jj++) s += v[j][jj]*tmp[jj];
      x[j] = s;
   }
}
/**
* routine pythag: taken from Numerical Recipes in c++, pag. 73
**/
Nb pythag(const Nb a, const Nb b)
{
   Nb absa,absb;

   absa=fabs(a);
   absb=fabs(b);
   if (absa > absb) return absa*sqrt(C_1+(absb/absa)*(absb/absa));
   else return (absb == C_0 ? C_0 : absb*sqrt(C_1+(absa/absb)*(absa/absb)));
}
