/**
 ************************************************************************
 * 
 * @file                Transformer.h
 *
 * Created:             6-2-2008
 *
 *                      Peter Seidler    (seidler@chem.au.dk)
 *
 * Short Description:   Interface definition for transformer used in
 *                      ItEqSol (Iterative Equation Solver).
 * 
 * Last modified: 
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef TRANSFORMER_H_INCLUDED
#define TRANSFORMER_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasMatrix.h"

/**
 *
 **/
class Transformer
{
   protected:
      bool mTrfUsingPreDiag;   ///< Use PreDiagTransform() when Transform() is called.
   
   public:
      Transformer()
         : mTrfUsingPreDiag(false)
      {
      }
   
      void TrfUsingPreDiag(bool aB) 
      {
         mTrfUsingPreDiag = aB;
      }
   
      virtual void Transform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb) = 0;
      virtual void CheckPurifies(DataCont& aIn)  = 0;
   
      virtual void PreparePreDiag() = 0;
      virtual void PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb) = 0;
      virtual void RestorePreDiag() = 0;
      
      // construct the transformer explicitly and return as matrix
      virtual void ConstructExplicit(MidasMatrix&) = 0;
   
      virtual ~Transformer() = 0;
      
      virtual std::ostream& Print(std::ostream&) const = 0;
      virtual In VectorDimension() const = 0;

      // clone transformer
      virtual Transformer* Clone() const = 0;
      virtual Transformer* CloneImprovedPrecon(In aLevel) const = 0;
      
      // check if the transformer is a NullTransformer
      virtual bool Null() const { return false; }
      virtual std::string Type() const = 0;

   protected:
      /*******************************************************************/
      /* line delimeters for transformer outputs                         */
      /* delimeter 1 used for start/end of output,                       */
      /* delimeter 2 used to separate different parts of output          */
      /* made virtual so they can be specialized for specfic transformer */
      /*******************************************************************/
      virtual void LineDelimeter1(std::ostream& ostream) const
      { 
         Out72Char(ostream,'+','-','+'); 
      }
      virtual void LineDelimeter2(std::ostream& ostream) const
      { 
         Out72Char(ostream,'~','~','~'); 
      }
};

/**
 *
 **/
inline Transformer::~Transformer
   (
   )
{
}

/**
 *
 **/
class NullTransformer: public Transformer
{
   public:
      NullTransformer(): Transformer()
      {
      }
   
      virtual void Transform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb) { MIDASERROR("NullTransformer::Transform() called"); }
      virtual void CheckPurifies(DataCont& aIn) { MIDASERROR("NullTransformer::CheckPurifies() called"); }
   
      virtual void PreparePreDiag() { MIDASERROR("NullTransformer::PreparePreDiag() called"); }
      virtual void PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb) { MIDASERROR("NullTransformer::PreDiagTransform called"); }
      virtual void RestorePreDiag() { MIDASERROR("NullTransformer::RestorePreDiag() called"); }
      
      virtual void ConstructExplicit(MidasMatrix&) { MIDASERROR("NullTransformer::ConstructExplicit() called"); }
   
      virtual ~NullTransformer() 
      {
      }
      
      virtual std::ostream& Print(std::ostream& os) const 
      { 
         LineDelimeter1(os);
         os << " **************** NullTransformer ******************" << std::endl;
         LineDelimeter1(os);
         return os;
      };

      virtual In VectorDimension() const { return 0; }
      virtual Transformer* Clone() const { MIDASERROR("NullTransformer::Clone() called"); return nullptr; }
      virtual Transformer* CloneImprovedPrecon(In aLevel) const { MIDASERROR("NullTransformer::CloneImprovedPrecon() called"); return nullptr; }
      
      virtual bool Null() const { return true; }
      virtual std::string Type() const { return "NULL"; }
};

/**
 *
 **/
inline std::ostream& operator<<(std::ostream& ostream, Transformer* trf)
{ 
   return trf->Print(ostream); 
}

/**
 *
 **/
void TransformerDestroy(Transformer* apTrans);

#endif /* TRANSFORMER_H_INCLUDED */
