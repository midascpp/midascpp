/**
************************************************************************
* 
* @file                MidasVector.h
*
* Created:             11-03-2013
*
* Author:              Ove Christiansen   (ove@chem.au.dk)
*                      Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Midas Vector Class Interface
* 
* Last modified: Thu Aug 13, 2013  12:19PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASVECTOR_H
#define MIDASVECTOR_H

#include<complex>
#include "util/type_traits/Complex.h"
#include "util/Error.h"
#include "util/Math.h"
#include "mmv/VectorToPointer.h"

//
// include declaration of GeneralMidasVector
//
#include "MidasVector_Decl.h"

//
// typedef MidasVector to be backwards compatible
//
using MidasVector = GeneralMidasVector<Nb>;
using ComplexMidasVector = GeneralMidasVector<std::complex<Nb>>;

//
// include MidasMatrix
// MidasMatrix needs GeneralMidasVector declaration,
// and GeneralMidasVector implementation needs MidasMatrix
//
#include "MidasMatrix.h"

//
// typedef MidasVector to be backwards compatible
//
using MidasMatrix = GeneralMidasMatrix<Nb>;
using ComplexMidasMatrix = GeneralMidasMatrix<std::complex<Nb>>;

//
// include implementation of GeneralMidasVector
//
#include "MidasVector_Impl.h"

//
//
//
void TriDiagLinEqSol(const MidasVector& arDiag, const MidasVector& arSubDiag,
                     const MidasVector& arSuperDiag, const MidasVector& arRHS,
                     MidasVector& arSolVecRe, MidasVector& arSolVecIm,
                     Nb aOmega=C_0, Nb aGamma=C_0);
///<  Solving Tx=b linear equation system, where T is a tridiagonal complex matrix

//
//
//
void BandedLinEqSol(const MidasMatrix& arMat, const MidasVector& arRHS,
                     MidasVector& arSolVecRe,  MidasVector& arSolVecIm,
                     Nb aOmega=C_0, Nb aGamma=C_0);
///<  Solving Ax=b linear equation system, where A is a Banded complex matrix

//
//
//
void DiagT(const MidasVector& arDiag, const MidasVector& arSubDiag,
           const MidasVector& arSuperDiag, const In aNc);

//
//
//
void DiagSymTridiag(const MidasVector& arDiag, const MidasVector& arSubDiag,
           const MidasVector& arSuperDiag, MidasVector& arEigVal, MidasMatrix& arEigVec);

/**
 * interface for iterative solver and ODE
 **/
template
   <  class T
   ,  class U
   >
inline void Scale
   (  GeneralMidasVector<T>& aVec
   ,  U aScalar
   )
{
   aVec.Scale(aScalar);
}

/**
 * interface for it_solver and ode
 **/
template<class T>
inline typename GeneralMidasVector<T>::real_t Norm
   (  const GeneralMidasVector<T>& aVec
   )
{
   return aVec.Norm();
}

template<class T>
inline typename GeneralMidasVector<T>::real_t Norm2
   (  const GeneralMidasVector<T>& aVec
   )
{
   return aVec.Norm2();
}

template<class T>
inline typename GeneralMidasVector<T>::real_t DiffNorm2
   (  const GeneralMidasVector<T>& aVec
   ,  const GeneralMidasVector<T>& aOther
   )
{
   return aVec.DiffNorm2(aOther);
}

template<class T>
inline typename GeneralMidasVector<T>::real_t DiffNorm
   (  const GeneralMidasVector<T>& aVec
   ,  const GeneralMidasVector<T>& aOther
   )
{
   return aVec.DiffNorm(aOther);
}

template<class T>
inline typename GeneralMidasVector<T>::real_t DiffOneNorm
   (  const GeneralMidasVector<T>& aVec
   ,  const GeneralMidasVector<T>& aOther
   )
{
   return aVec.DiffOneNorm(aOther);
}

template<bool CONJ_FIRST = false, class T>
inline typename GeneralMidasVector<T>::real_t DiffNorm2Scaled
   (  const GeneralMidasVector<T>& arFirst
   ,  const GeneralMidasVector<T>& arSecond
   ,  const T aFirstScale = T(1)
   ,  const T aSecondScale = T(1)
   ,  In aOff = 0
   ,  In aN = -1
   )
{
   return arFirst.template DiffNorm2Scaled<CONJ_FIRST>(arSecond, aFirstScale, aSecondScale, aOff, aN);
}

/**
 *
 **/
template<class T>
inline void Zero
   (  GeneralMidasVector<T>& aVec
   )
{
   aVec.Zero();
}

/**
 *
 **/
template
   <  class T
   ,  class U
   >
inline void Axpy
   (  GeneralMidasVector<T>& aVec
   ,  const GeneralMidasVector<T>& aOther
   ,  U scal
   )
{
   StandardAxpy(aVec.Size(), scal, aOther, I_1, aVec, I_1);
}

/**
 *
 **/
template<class T>
inline void SetShape
   (  GeneralMidasVector<T>& aVec
   ,  const GeneralMidasVector<T>& aShape
   )
{
   aVec.SetNewSize(aShape.Size());
}

/**
 * @param aVec
 * @return
 *    Size of vector
 **/
template
   <  typename T
   >
inline size_t Size
   (  const GeneralMidasVector<T>& aVec
   )
{
   return aVec.Size();
}

/**
 * Mean Norm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations I - Nonstiff Problems, page 167-168
 *
 * ||e||^2 = (1/n) * sum_i (e[i] / sc[i])^2
 *
 * with sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol
 *
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
auto OdeMeanNorm2
   (  const GeneralMidasVector<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasVector<PARAM_T>& aYOld
   ,  const GeneralMidasVector<PARAM_T>& aYNew
   )
{
   return OdeNorm2(aDeltaY, aAbsTol, aRelTol, aYOld, aYNew) / aDeltaY.size();
}


/**
 * Norm2 for ODE.
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
ABSVAL_T OdeNorm2
   (  const GeneralMidasVector<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasVector<PARAM_T>& aYOld
   ,  const GeneralMidasVector<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   const auto size = aDeltaY.size();
   assert(size == aYOld.size());
   assert(size == aYNew.size());

   // AbsTol must be > 0
   assert(std::real(aAbsTol) > C_0);

   ABSVAL_T result = 0;
   ABSVAL_T sc_i = 0;

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   for(Uin i=I_0; i<size; ++i)
   {
      sc_i = aAbsTol + std::max(AbsVal(aYOld[i]), AbsVal(aYNew[i]))*aRelTol;
      result += AbsVal2(aDeltaY[i]/sc_i);
   }

   return result;
}

/**
 * MaxNorm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations I - Nonstiff Problems, page 167-168
 *
 * ||e||^2 = max_i (e[i] / sc[i])^2
 *
 * with sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol
 *
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
auto OdeMaxNorm2
   (  const GeneralMidasVector<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasVector<PARAM_T>& aYOld
   ,  const GeneralMidasVector<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   auto size = aDeltaY.size();
   assert(size == aYOld.size());
   assert(size == aYNew.size());

   // AbsTol must be > 0
   assert(std::real(aAbsTol) > C_0);

   ABSVAL_T result = 0;
   ABSVAL_T sc_i = 0;

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   for(Uin i=I_0; i<size; ++i)
   {
      sc_i = aAbsTol + std::max(AbsVal(aYOld[i]), AbsVal(aYNew[i]))*aRelTol;
      result = std::max(result, AbsVal2(aDeltaY[i]/sc_i));
   }

   return result;
}

/**
 * Sum the safe way.
 *
 * @param aVecs      Vector of GeneralMidasVector%s
 * @return
 *    Sum of input vectors
 **/
template
   <  class T
   >
GeneralMidasVector<T> NeumaierSum
   (  const std::vector<GeneralMidasVector<T> >& aVecs
   )
{
   auto nvecs = aVecs.size();
   assert(nvecs > 0);
   auto vsize = aVecs[0].size();
   for(const auto& v : aVecs)
   {
      assert(v.size() == vsize);
   }

   GeneralMidasVector<T> result(vsize, T(0.));

   for(In ielem=I_0; ielem<vsize; ++ielem)
   {
      T sum = aVecs[I_0][ielem];
      T c = T(0.);
      for(In ivec=I_1; ivec<nvecs; ++ivec)
      {
         const auto& v = aVecs[ivec];
         T t = sum + v[ielem];
         if (  std::abs(sum) >= std::abs(v[ielem])
            )
         {
            c += (sum - t) + v[ielem];
         }
         else
         {
            c += (v[ielem] - t) + sum;
         }
         sum = t;
      }
      result[ielem] = sum + c;
   }

   return result;
}

#endif /* MIDASVECTOR_H */
