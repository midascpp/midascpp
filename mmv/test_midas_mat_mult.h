#ifndef TEST_MIDAS_MAT_MULT_H
#define TEST_MIDAS_MAT_MULT_H

#include "test/CuteeInterface.h"

namespace midas
{
namespace test
{
namespace mmv
{

/**
 * Test matrix-matrix contraction or matrix product.
 **/
template<class matrix_type>
struct mat_mat_contraction
   : public cutee::test
{
   void run()
   {
      // Create matrices
      matrix_type mat0(I_2, I_2);
      matrix_type mat1(I_2, I_2);
      matrix_type mat2(I_2, I_2);
      
      mat1(0,0) = 2.0; mat1(0,1) = 1.0; 
      mat1(1,0) = 3.0; mat1(1,1) = 4.0;
      
      mat2(0,0) = 1.0; mat2(0,1) = 3.0; 
      mat2(1,0) = 3.0; mat2(1,1) = 1.0;
      
      // Do the contraction
      mat0 = mat1 * mat2;
      
      // Assert result
      UNIT_ASSERT_FEQUAL(mat0(0,0),5.0,"mat0(0,0) failed");
      UNIT_ASSERT_FEQUAL(mat0(0,1),7.0,"mat0(0,1) failed");
      UNIT_ASSERT_FEQUAL(mat0(1,0),15.0,"mat0(1,0) failed");
      UNIT_ASSERT_FEQUAL(mat0(1,1),13.0,"mat0(1,1) failed");
   }
};

/**
 * Test matrix-vector contraction.
 *
 *   /- - -\   /|\   /x\
 *   |     | * ||| = | |
 *   \     /   \|/   \ /
 *
 **/
template<class matrix_type, class vector_type>
struct mat_vec_contraction
   : public cutee::test
{
   void run()
   {
      // Create matrix and vector
      vector_type vec0(I_3);
      matrix_type mat1(I_3, I_2);
      vector_type vec2(I_2);
      
      mat1(0,0) = 2.0; mat1(0,1) = 1.0; 
      mat1(1,0) = 3.0; mat1(1,1) = 4.0;
      mat1(2,0) = 4.0; mat1(2,1) = 2.0;

      vec2(0) = 1.0; 
      vec2(1) = 3.0;

      // Do the contraction
      vec0 = mat1 * vec2;

      // Assert result
      UNIT_ASSERT_FEQUAL(vec0(0),5.0,"mat0(0,0) failed");
      UNIT_ASSERT_FEQUAL(vec0(1),15.0,"mat0(0,1) failed");
      UNIT_ASSERT_FEQUAL(vec0(2),10.0,"mat0(0,1) failed");
   }
};

/**
 * Test vector-matrix contraction.
 *
 *            /|    \
 *  (- - -) * ||    | = (x    ) 
 *            \|    /    
 *
 **/
template<class matrix_type, class vector_type>
struct vec_mat_contraction
   : public cutee::test
{
   void run()
   {
      // Create matrix and vector
      vector_type vec0(I_2);
      vector_type vec1(I_3);
      matrix_type mat2(I_3, I_2);
      
      vec1(0) = 1.0; vec1(1) = 3.0; vec1(2) = 1.0;
      
      mat2(0,0) = 2.0; mat2(0,1) = 1.0; 
      mat2(1,0) = 3.0; mat2(1,1) = 4.0;
      mat2(2,0) = 4.0; mat2(2,1) = 2.0;

      // Do the contraction
      vec0 = vec1 * mat2;

      // Assert result
      UNIT_ASSERT_FEQUAL(vec0(0),15.0,"mat0(0,0) failed");
      UNIT_ASSERT_FEQUAL(vec0(1),15.0,"mat0(0,1) failed");
   }
};

/**
 *
 **/
template<class matrix_type, class vector_type>
struct vec_mat_contraction_addition
   :  public cutee::test
{
   void run()
   {
      vector_type vec0(I_2);
      vector_type vec1(I_3);
      matrix_type mat2(I_3, I_2);
      vector_type vec3(I_2);
      
      vec1(0) = 1.0; vec1(1) = 3.0; vec1(2) = 1.0;
      
      mat2(0,0) = 2.0; mat2(0,1) = 1.0; 
      mat2(1,0) = 3.0; mat2(1,1) = 4.0;
      mat2(2,0) = 4.0; mat2(2,1) = 2.0;

      vec3(0) = 1.0;
      vec3(1) = 4.0;

      // Do the contraction
      vec0 = vec1 * mat2 + vec3;

      UNIT_ASSERT_FEQUAL(vec0(0),16.0,"mat0(0,0) failed");
      UNIT_ASSERT_FEQUAL(vec0(1),19.0,"mat0(0,1) failed");
   }
};

/**
 *
 **/
template<class vector_type>
struct vec_dot
   :  public cutee::test
{
   void run()
   {
      vector_type vec0(I_2);
      vector_type vec1(I_2);
      
      vec0(0) = 1.0; vec0(1) = 3.0;
      vec1(0) = 2.0; vec1(1) = 4.0;

      typename vector_type::value_type dot = (vec0*vec1).at();

      UNIT_ASSERT_FEQUAL(dot,14.0,"mat0(0,0) failed");
   }
};

/**
 *
 **/
template<class vector_type>
struct vec_vec_dot
   :  public cutee::test
{
   void run()
   {
      vector_type vec0(I_2);
      vector_type vec1(I_2);
      vector_type vec2(I_2);
      vector_type vec3(I_2);
      
      vec1(0) = 1.0; vec1(1) = 2.0;
      vec2(0) = 2.0; vec2(1) = -4.0;
      vec3(0) = 3.0; vec3(1) = 1.0;

      vec0 = vec1*(vec2*vec3);

      UNIT_ASSERT_FEQUAL(vec0(0),2.0,"vec0(0) failed");
      UNIT_ASSERT_FEQUAL(vec0(1),4.0,"vec1(0) failed");
   }
};

/**
 *
 **/
template<class vector_type>
struct vec_scal
   :  public cutee::test
{
   void run() 
   {
      vector_type vec0(I_2);
      vector_type vec1(I_2);
      
      vec0(0) = 1.0; vec0(1) = 3.0;
      vec1(0) = 2.0; vec1(1) = 4.0;

      vec0 *= 3.0;
      vec1 /= 2.0;

      UNIT_ASSERT_FEQUAL(vec0(0),3.0,"vec0(0) failed");
      UNIT_ASSERT_FEQUAL(vec0(1),9.0,"vec0(1) failed");
      UNIT_ASSERT_FEQUAL(vec1(0),1.0,"vec1(0) failed");
      UNIT_ASSERT_FEQUAL(vec1(1),2.0,"vec2(1) failed");
   }
};

/**
 *
 **/
template<class matrix_type, class complex_vector_type>
struct mat_complex_vec_mul_test
   :  public cutee::test
{
   void run()
   {
      complex_vector_type vec0(I_2);
      matrix_type mat1(I_2, I_3);
      complex_vector_type vec2(I_3);

      mat1(0,0) =  1.0; mat1(0,1) = 2.0; mat1(0,2) = -3.2;
      mat1(1,0) = -3.0; mat1(1,1) = 4.0; mat1(1,2) =  1.1;

      vec2(0) = {2.0, 3.0}; vec2(1) = {1.0, -1.0}; vec2(2) = {3.0,1.2};
   
      vec0 = mat1 * vec2;

      UNIT_ASSERT(libmda::numeric::float_eq(vec0(0),{-5.60,-2.84}),"vec0(0) failed");
      UNIT_ASSERT(libmda::numeric::float_eq(vec0(1),{1.30,-11.68}),"vec0(1) failed");
   }
};

/**
 *
 **/
template<class matrix_type, class vector_type>
struct mat_vec_mult_collection
   :  public cutee::collection
{
   mat_vec_mult_collection()
   {
      add_test<mat_mat_contraction<matrix_type> >();
      add_test<mat_vec_contraction<matrix_type, vector_type> >();
      add_test<vec_mat_contraction<matrix_type, vector_type> >();
      add_test<vec_mat_contraction_addition<matrix_type, vector_type> >();
      add_test<vec_dot<vector_type> >();
      add_test<vec_vec_dot<vector_type> >();
      add_test<vec_scal<vector_type> >();
   }
};


} // namespace mmv
} // namespace test
} // namespace midas

#endif /* TEST_MIDAS_MAT_MULT_H */
