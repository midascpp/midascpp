/**
************************************************************************
* 
* @file                VectorToPointer.h
*
* Created:             17-01-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Copy data between vectors and pointers
* 
* Last modified: Thu Aug 13, 2013  12:19PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VECTORTOPOINTER_H_INCLUDED
#define VECTORTOPOINTER_H_INCLUDED

#include <type_traits>
#include "util/IsDetected.h"

namespace detail
{
//! Check for size function
template
   <  typename T
   >
using has_size_function_t = decltype(std::declval<T>().size());

//! Check for operator[] with one argument
template
   <  typename T
   ,  typename A
   >
using has_square_brackets_t = decltype(std::declval<T>()[std::declval<A>()]);
} /* namespace detail */


/**
 * Copy data to pointer. Real version.
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  class T
   ,  class U
   ,  template<typename> typename VECTOR
   ,  typename std::enable_if_t<!midas::type_traits::IsComplexV<T> >* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_size_function_t, VECTOR<T>>>* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_square_brackets_t, VECTOR<T>, In>>* = nullptr
   >
void DataToPointer
   (  const VECTOR<T>& aVec
   ,  U* apPtr
   )
{
   static_assert(std::is_floating_point<U>::value && std::is_floating_point<T>::value, "This only works for floating-point types!");
   auto size = aVec.size();
   for(In i=I_0; i<size; ++i)
   {
      apPtr[i] = aVec[i];
   }
}

/**
 * Copy data to pointer. Complex version.
 * NB: Store real and imag parts after each other (Re[x1], Im[x1], Re[x2], Im[x2], ..., Re[xn], Im[xn]).
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  class T
   ,  class U
   ,  template<typename> typename VECTOR
   ,  typename std::enable_if_t<midas::type_traits::IsComplexV<T> >* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_size_function_t, VECTOR<T>>>* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_square_brackets_t, VECTOR<T>, In>>* = nullptr
   >
void DataToPointer
   (  const VECTOR<T>& aVec
   ,  U* apPtr
   )
{
   static_assert(std::is_floating_point<U>::value, "This only works for floating-point types!");
   auto size = aVec.size();
   In j=I_0;
   for(In i=I_0; i<size; ++i)
   {
      apPtr[j] = std::real(aVec[i]);
      apPtr[j+I_1] = std::imag(aVec[i]);
      j += I_2;
   }
}

/**
 * Set data from pointer. Real version.
 * NB: Vector must have correct size!
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  class T
   ,  class U
   ,  template<typename> typename VECTOR
   ,  typename std::enable_if_t<!midas::type_traits::IsComplexV<T> >* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_size_function_t, VECTOR<T>>>* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_square_brackets_t, VECTOR<T>, In>>* = nullptr
   >
void DataFromPointer
   (  VECTOR<T>& aVec
   ,  const U* apPtr
   )
{
   static_assert(std::is_floating_point<U>::value && std::is_floating_point<T>::value, "This only works for floating-point types!");
   auto size = aVec.size();
   for(In i=I_0; i<size; ++i)
   {
      aVec[i] = apPtr[i];
   }
}

/**
 * Set data from pointer. Complex version.
 * NB: Vector must have correct size!
 *
 * @param aVec    Input vector
 * @param apPtr   Pointer
 **/
template
   <  class T
   ,  class U
   ,  template<typename> typename VECTOR
   ,  typename std::enable_if_t<midas::type_traits::IsComplexV<T> >* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_size_function_t, VECTOR<T>>>* = nullptr
   ,  typename std::enable_if_t<midas::util::IsDetectedV<detail::has_square_brackets_t, VECTOR<T>, In>>* = nullptr
   >
void DataFromPointer
   (  VECTOR<T>& aVec
   ,  const U* apPtr
   )
{
   static_assert(std::is_floating_point<U>::value, "This only works for floating-point types!");
   auto size = aVec.size();
   In j = I_0;
   for(In i=I_0; i<size; ++i)
   {
      aVec[i] = T(apPtr[j], apPtr[j+I_1]);
      j += I_2;
   }
}

#endif /* VECTORTOPOINTER_H_INCLUDED */
