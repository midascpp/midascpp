/**
************************************************************************
* 
* @file                LinEqSol.cc
*
* Created:             11-08-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing friend for solving linear equations
* 
* Last modified: Fri Mar 27, 2009  12:06PM
h 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers
#include<iostream>
using std::endl;
#include<fstream>
using std::ofstream;
using std::ifstream;
#include<string>
using std::string;

// Link to standard headers
#include "inc_gen/math_link.h"

// My headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
//#include "util/CHARACTER.h"
#include "input/Input.h"
#include "mmv/Diag.h"

extern "C" void dgesv_(int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);

extern "C" void dposv_(char *uplo, int *n, int *nrhs, double *a, int *lda, double *b, int *ldb, int *info);

extern "C" void dsysv_(char *uplo, int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb,
      double *work, int *lwork, int *info);

extern "C" void dgesvd_(char *jobu, char *jobvt, int *h, int *n, double *a, int *lda, double *s, double *u,
      int *ldu, double *vt, int *ldvt, double *work, int *lwork, int *info);

extern "C" int ilaenv_wrapper_(int *, char*, int*, char *,int *, int *, int *, int *, int *);

/**
* A friend for solving linear equation systems 
* A X = B, B has aNrhs cols. A is general real matrix.
* NB for ConjGrad impl. A must be symmetric
* */
void LinEqSol(MidasMatrix& arM, MidasMatrix& arSol,
            MidasMatrix& arRhs, In aNrhs, string aHow, 
            MidasVector& arResid, In& aIter, const Nb aTol,
            const In aMaxIter)
{
   if (gDebug||gIoLevel>I_10) Mout << " Solving Linear Equations using " << aHow 
                    << " algorithm" << endl;
   //bool aftertreat=false;
   In n_eq = aNrhs;
   In n_dim = arM.Nrows();
   if (n_eq > arRhs.Ncols()) MIDASERROR("Not enough space for Rhs vectors in LinEqSol");
   if (n_eq > arSol.Ncols()) MIDASERROR("Not enough space for sol vectors in LinEqSol");
   if (n_eq > arResid.Size()) MIDASERROR("Not enough space for Residuals in LinEqSol");

   Nb norm = arM.Norm();
   bool matrix_close_to_zero=(norm < aTol); 
   if (norm < aTol) Mout << " Small norm of matrix - watch out " << endl;
   // Mode 1 
   if (aHow=="DGESV")
   {
   if (gDebug||gIoLevel>I_10) Mout << " Solving Linear Equations using " << aHow 
                    << " algorithm" << endl;
      //Mout << " The method is DGESV. " << endl;

      int N = arM.Nrows();
      int NRHS = arRhs.Ncols();//==n_eq==aNrhs

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = N;
      int *IPIV = new int[N*N];

      // matrix A in column-major format
      double *B = new double[N*NRHS];
      counter = I_0;
      for(In j=I_0; j<NRHS; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            B[counter]=arRhs[i][j];
            counter++;
         }
      }

      // Leading dimension of a matrix specifies the number of storage locations
      // between elements in the same row. The leading dimension of the array must be
      // at least as large as the number of rows in the matrix.
      int LDB = N;
      int INFO = 0;

      //Solves the system of linear AX=B of an N-by-N real nonsymmetric matrix A
      dgesv_(&N,&NRHS,A,&LDA,IPIV,B,&LDB,&INFO);
      if (INFO > I_0)
         Mout << " The factorization has been completed,\n" 
            << " but the factor U is exactly singular,\n"
            << " so the solution could not be computed." << endl;
      else if (INFO < I_0)
      {
         Mout << " The " << -INFO << "-th argument had an illegal value" << endl;
         MIDASERROR(" Solution failed ... ");
      }

      //if (m!=n_dim && n!=n_dim && n_eq2 != n_eq) 
      //{
      //   if (!matrix_close_to_zero ) MIDASERROR(" Solution failed ... ");
      //   else 
      //   {
      //      // If matrix is zero then rhs must also be zero. 
      //      Mout << " Matrix norm " <<  norm << endl;
      //      for (In j=0;j<n_eq;j++) 
      //      {
      //         Nb norm_rhs=C_0;
      //         for (In i=0;i<arRhs.Nrows();i++) norm_rhs = arRhs[i][j]*arRhs[i][i];
      //         Mout << " Rhs " << j << " norm = " << norm_rhs << endl;
      //         if (norm_rhs > aTol) MIDASERROR("Matrix zero norm , Rhs non-zero norm");
      //         else  // set to zero 
      //            for (In i=0;i<arSol.Nrows();i++) arSol[i][j]=C_0;
      //      }
      //   }
      //} 
      //else
      //{
      //if (gDebug) Mout << " Reading in solution vectors with m,n,n_eq: " 
      //         << m << " " << n << " " << n_eq << endl;
      counter = I_0;
      for(In j=I_0; j<NRHS; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            arSol[i][j]=B[counter];
            counter++;
         }
      }
      //}

      delete[] A; 
      delete[] IPIV;
      delete[] B;
   }
   else if (aHow=="DSYSV")  // Mode 2 
   {
   if (gDebug||gIoLevel>I_10) Mout << " Solving Linear Equations using " << aHow 
                    << " algorithm" << endl;
      //Mout << " The method is DSYSV instead of MIDAS_CG. " << endl;

      char UPLO = 'U';
      int N = arM.Nrows();
      int NRHS = arRhs.Ncols();//==n_eq==aNrhs

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = N;
      int *IPIV = new int[N];

      // matrix A in column-major format
      double *B = new double[N*NRHS];
      counter = I_0;
      for(In j=I_0; j<NRHS; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            B[counter]=arRhs[i][j];
            counter++;
         }
      }

      // Leading dimension of a matrix specifies the number of storage locations
      // between elements in the same row. The leading dimension of the array must be
      // at least as large as the number of rows in the matrix.
      int LDB = N;
      char NAME[7]={"DSYTRF"}; // needs to be 7 long to be null terminated.
      int NAME_LEN = 6;        // but the length of the string is only 6 for fortran purposes!
      //string NAME="DSYTRF";
      int ONE=I_1;
      int MINUSONE=-I_1;
      // mbh: PGI gives segmentation fault when using ilaenv...
      // ian: introduced wrapper for ilaenv to correctly initialize char array for fortran... 
      // ian: hopefully it will not give segfault anymore ;)
#ifdef VAR_PGF77
      // ian: perhaps this could also call ilaenv_wrapper_ instead now... 
      int NB=N;
#else
      int OSIZE = 1;
      int NB = max(ilaenv_wrapper_(&ONE,NAME,&NAME_LEN,&UPLO,&OSIZE,&N,&MINUSONE,&MINUSONE,&MINUSONE),N);
#endif
      //Mout << " werner NB = " << NB << endl;
      int LWORK = max(10000000,N*NB);
      double *WORK = new double[LWORK];
      int INFO = 0;
      //Mout << "UPLO : " << UPLO << endl;
      //Solves the system of linear AX=B of an N-by-N real nonsymmetric matrix A
      dsysv_(&UPLO,&N,&NRHS,A,&LDA,IPIV,B,&LDB,WORK,&LWORK,&INFO);
      
      if(INFO != I_0)
      {
         if (!matrix_close_to_zero ) MIDASERROR(" Solution failed ... ");
         else 
         {
            // If matrix is zero then rhs must also be zero. 
            Mout << " Matrix norm " <<  norm << endl;
            for (In j=0;j<n_eq;j++) 
            {
               Nb norm_rhs=C_0;
               for (In i=0;i<arRhs.Nrows();i++) norm_rhs = arRhs[i][j]*arRhs[i][i];
               Mout << " Rhs " << j << " norm = " << norm_rhs << endl;
               if (norm_rhs > aTol) MIDASERROR("Matrix zero norm , Rhs non-zero norm");
               else  // set to zero 
                  for (In i=0;i<arSol.Nrows();i++) arSol[i][j]=C_0;
            }
         }
      } 
      else
      {
         //if (gDebug) Mout << " Reading in solution vectors with m,n,n_eq: " 
         //         << m << " " << n << " " << n_eq << endl;
         counter = I_0;
         for(In j=I_0; j<NRHS; j++)
         {
            for(In i=I_0; i<N; i++)
            {
               arSol[i][j]=B[counter];
               counter++;
            }
         }
      }

      MidasVector rhs(n_dim);
      MidasVector sol(n_dim);
      for (In i=I_0;i<NRHS;i++)
      {
         arRhs.GetCol(rhs,i);
         arSol.GetCol(sol,i);
         Nb resid = C_NB_MAX;
         MidasVector tst = arM*sol;
         tst-=rhs;
         resid=tst.Norm();

         if (resid>aTol && resid > C_NB_EPSILON*C_10_2) 
         {
            Mout << " arM norm " << arM.Norm() << endl;
            Mout << " rhs norm " << rhs.Norm() << endl;
            Mout << " sol norm " << sol.Norm() << endl;
            Mout << " (arM*sol-rhs) norm " << resid << endl;
            Mout << " LinEqSol; Large residual - some numerical problems? " << endl;
            string swar="LinEqSol; Large residual - some numerical problems?";
            MidasWarning(swar);
         }
         arResid[i] = resid;
      }

      delete[] A; 
      delete[] IPIV;
      delete[] B;
      delete[] WORK;
   }
   else if (aHow=="DPOSV")
   {
      if (gDebug||gIoLevel>I_10) Mout << " Solving Linear Equations using " << aHow 
         << " algorithm" << endl;
      //Mout << " The method is DPOSV instead of MIDAS_CG. " << endl;

      char UPLO = 'U';
      int N = arM.Nrows();
      int NRHS = arRhs.Ncols();//==n_eq==aNrhs

      // matrix A in column-major format
      double *A = new double[N*N];
      In counter = I_0;
      for(In j=I_0; j<N; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            A[counter]=arM[i][j];
            counter++;
         }
      }

      int LDA = N;

      // matrix A in column-major format
      double *B = new double[N*NRHS];
      counter = I_0;
      for(In j=I_0; j<NRHS; j++)
      {
         for(In i=I_0; i<N; i++)
         {
            B[counter]=arRhs[i][j];
            counter++;
         }
      }

      // Leading dimension of a matrix specifies the number of storage locations
      // between elements in the same row. The leading dimension of the array must be
      // at least as large as the number of rows in the matrix.
      int LDB = N;
      int INFO = 0;

      //Solves the system of linear AX=B of an N-by-N real nonsymmetric matrix A
      dposv_(&UPLO,&N,&NRHS,A,&LDA,B,&LDB,&INFO);
      if(INFO != I_0)
      {
         if (!matrix_close_to_zero ) MIDASERROR(" Solution failed ... ");
         else 
         {
            // If matrix is zero then rhs must also be zero. 
            Mout << " Matrix norm " <<  norm << endl;
            for (In j=0;j<n_eq;j++) 
            {
               Nb norm_rhs=C_0;
               for (In i=0;i<arRhs.Nrows();i++) norm_rhs = arRhs[i][j]*arRhs[i][i];
               Mout << " Rhs " << j << " norm = " << norm_rhs << endl;
               if (norm_rhs > aTol) MIDASERROR("Matrix zero norm , Rhs non-zero norm");
               else  // set to zero 
                  for (In i=0;i<arSol.Nrows();i++) arSol[i][j]=C_0;
            }
         }
      } 
      else
      {
         //if (gDebug) Mout << " Reading in solution vectors with m,n,n_eq: " 
         //         << m << " " << n << " " << n_eq << endl;
         counter = I_0;
         for(In j=I_0; j<NRHS; j++)
         {
            for(In i=I_0; i<N; i++)
            {
               arSol[i][j]=B[counter];
               counter++;
            }
         }
      }

      MidasVector rhs(n_dim);
      MidasVector sol(n_dim);
      for (In i=I_0;i<NRHS;i++)
      {
         arRhs.GetCol(rhs,i);
         arSol.GetCol(sol,i);
         Nb resid = C_NB_MAX;
         MidasVector tst = arM*sol-rhs;
         resid=tst.Norm();

         if (resid>aTol && resid > C_NB_EPSILON*C_10_2) 
         {
            Mout << " arM norm " << arM.Norm() << endl;
            Mout << " rhs norm " << rhs.Norm() << endl;
            Mout << " sol norm " << sol.Norm() << endl;
            Mout << " (arM*sol-rhs) norm " << resid << endl;
            Mout << " LinEqSol; Large residual - some numerical problems? " << endl;
            string swar="LinEqSol; Large residual - some numerical problems?";
            MidasWarning(swar);
         }
         arResid[i] = resid;
      }

      delete[] A; 
      delete[] B;
   }
   else if (aHow=="MIDAS_CG")
   {
      if (gDebug||gIoLevel>I_10) Mout << " Solving Linear Equations using " << aHow 
         << " algorithm" << endl;
      MidasVector rhs(n_dim);
      MidasVector sol(n_dim);
      In iter=I_0;
      for (In i_c=I_0;i_c<n_eq;i_c++)
      {
         arRhs.GetCol(rhs,i_c);
         sol = rhs;
         Nb resid = C_NB_MAX;
         arM.ConjGrad(sol,rhs,resid,iter,aTol,aMaxIter);
         //Mout << " resid                  " << resid << endl;
         //Mout << " aTol                   " << aTol  << endl;
         //Mout << " C_NB_EPSILON*C_10_2    " << C_NB_EPSILON*C_10_2 << endl;
         if (resid>aTol && resid > C_NB_EPSILON*C_10_2) 
         {
            //Mout << " resid                  " << resid << endl;
            //Mout << " aTol                   " << aTol  << endl;
            //Mout << " C_NB_EPSILON*C_10_2    " << C_NB_EPSILON*C_10_2 << endl;
            //Mout << " arM = " << arM << endl;
            Mout << " arM norm " << arM.Norm() << endl;
            Mout << " rhs norm " << rhs.Norm() << endl;
            Mout << " sol norm " << sol.Norm() << endl;
            MidasVector tst = arM*sol-rhs;
            Mout << " tst norm " << tst.Norm() << endl;
            Mout << " arM*sol-rhs norm " << tst.Norm() << endl;
            Mout << " LinEqSol; Large residual - some numerical problems? " << endl;
            //               if (tst.Norm() > max(arM.Norm(),rhs.Norm())*C_NB_EPSILON*C_10_3)
            //               {
            //                  MIDASERROR("Large residual");
            //               }
            //               else 
            //               {
            string swar="LinEqSol; Large residual - some numerical problems?";
            MidasWarning(swar);
            //               }
         }
         arSol.AssignCol(sol,i_c);
         arResid[i_c] = resid;
         aIter = max(iter,aIter);
      }
      //#endif
   }
   else if (aHow == "SVD")
   {
      MidasMatrix u(arM);
      MidasVector w(n_dim);
      MidasMatrix v(n_dim);

      //Mout << " arM: " << u << endl;

      svdcmp(u,w,v);

      //Mout << " u: " << u << endl;
      //Mout << " w: " << w   << endl;
      //Mout << " v: " << v   << endl;

      Nb wmin=w.FindMinValue();
      Nb wmax=w.FindMaxValue();
      Nb cond_num=fabs(wmax)/fabs(wmin);
      Mout << "In SVD\n wmin: " << wmin << " wmax: " << wmax 
         << " condition number: " << cond_num << endl;

      In j;
      const Nb TOL=C_I_10_15;
      //Nb wmax,tmp,thresh,sum;
      Nb thresh;
      Nb ma=n_dim;

      //wmax=C_0;
      //for (j=I_0; j<ma; j++)
      //   if (w[j] > wmax) wmax=w[j];
      thresh=TOL*wmax;
      Mout << " thresh: " << thresh << endl;
      In no_of_del_val=I_0;
      for (j=I_0; j<ma; j++)
      {
         if (w[j] < thresh)
         {
            w[j]=C_0;
            no_of_del_val+=I_1;
         }
      }
      if (no_of_del_val>I_0)
         Mout << " Number of deleted diagonal elements w[i] after SVD: " << no_of_del_val << endl;

      if (gDebug) Mout << " Using Singular Value Decomposition "  << endl;
      MidasVector rhs(n_dim);
      MidasVector sol(n_dim);
      //In iter=I_0;
      for (In i_c = I_0; i_c < n_eq; i_c++)
      {
         arRhs.GetCol(rhs, i_c);
         arSol.GetCol(sol, i_c);
         //sol = rhs;
         Nb resid = C_NB_MAX;
         SvBksb(u, w, v, rhs, sol);
         //arM.ConjGrad(sol,rhs,resid,iter,aTol,aMaxIter);
         MidasVector residual = arM*sol-rhs;
         resid = residual.Norm();

         //Mout << " resid                  " << resid << endl;
         //Mout << " aTol                   " << aTol  << endl;
         //Mout << " C_NB_EPSILON*C_10_2    " << C_NB_EPSILON*C_10_2 << endl;
         //if (resid>aTol && resid > C_NB_EPSILON*C_10_2) 
         //Mout << " resid                  " << resid << endl;
         //Mout << " aTol                   " << aTol  << endl;
         //Mout << " C_NB_EPSILON*C_10_2    " << C_NB_EPSILON*C_10_2 << endl;
         //Mout << " arM = " << arM << endl;
         Mout << " arM norm " << arM.Norm() << endl;
         Mout << " rhs norm " << rhs.Norm() << endl;
         Mout << " sol norm " << sol.Norm() << endl;
         Mout << " resid norm " << resid << endl;
         //MidasVector tst = arM*sol-rhs;
         //Mout << " tst norm " << tst.Norm() << endl;
         //Mout << " arM*sol-rhs norm " << tst.Norm() << endl;
         //Mout << " LinEqSol; Large residual - some numerical problems? " << endl;
         if (resid > max(arM.Norm(),rhs.Norm())*C_NB_EPSILON*C_10_3)
         {
            //MIDASERROR("Large residual");
            //else 
            string swar="LinEqSol; Large residual - some numerical problems?";
            MidasWarning(swar);
         }
         arSol.AssignCol(sol,i_c);
         arResid[i_c] = resid;
         //aIter = max(iter,aIter);
      }
   }
   else 
   {
      Mout << " Unknown LinEqSol method " << aHow << endl;
      MIDASERROR("Unknown LinEqSol method");
   }
}
