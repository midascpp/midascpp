/**
************************************************************************
* 
* @file                MidasVector_Impl.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen   (ove@chem.au.dk)  Original implementation
*                      Mikkel B. Hansen   (mbh@chem.au.dk?) Template rework
*                      Ian H. Godtliebsen (ian@chem.au.dk)  Interface separation+interface to libmda
*
* Short Description:   Implementation for GeneralMidasVector
* 
* Last modified: Fri Dec 15, 2006  08:40PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASVECTOR_IMPL_H
#define MIDASVECTOR_IMPL_H

// Standard headers
#include<iostream>
#include<string>
#include<algorithm>
#include<complex>

   // Link to standard headers
#include "inc_gen/math_link.h"

   // My headers
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "util/Plot.h"
#include "util/Io.h"
#include "util/Math.h"
#include "util/AbsVal.h"
#include "util/CallStatisticsHandler.h"

#include "mpi/Impi.h"

extern bool gDebug;
extern In gAnalysisIoLevel;

template<class T>
GeneralMidasVector<T>::GeneralMidasVector(const In aN, const T* const apV):
   mCapacity(aN), mLength(aN), mElements(nullptr)
{
   Allocate();
   for(In i=I_0; i<aN; ++i)
   {
      *(mElements+i) = *(apV+i);
   }
}

template<class T>
GeneralMidasVector<T>::GeneralMidasVector
   (  const In aN
   ,  const T aX
   )
   :  mCapacity(aN)
   ,  mLength(aN)
   ,  mElements(nullptr)
{
   Allocate();
   // Loop over aN (and not mLength) to silence warning
   // mmv/MidasVector_Impl.h:80:22: warning: ‘void* __builtin_memset(void*, int, long unsigned int)’ offset [0, 7] is out of the bounds [0, 0] [-Warray-bounds]
   // 80 |  *(mElements+i) = aX;
   for(In i=I_0; i<aN; ++i)
   {
      *(mElements+i) = aX;
   }
}

/**
* Copy constructor.
* */
template<class T>
GeneralMidasVector<T>::GeneralMidasVector
   (  const GeneralMidasVector<T>& arV
   )
   :  mCapacity(arV.Size())
   ,  mLength(arV.Size())
   ,  mElements(nullptr)
{
   Allocate();
   In len = arV.Size();
   for(In i=I_0; i<len; ++i)
   {
      *(mElements+i) = *(arV.mElements+i);
   }
}

/**
* Copy constructor.
* */
template<class T>
GeneralMidasVector<T>::GeneralMidasVector(const vector<T>& arV):
   mCapacity(arV.size()), mLength(arV.size()), mElements(NULL)
{
   Allocate();
   for(In i=I_0; i<mLength; ++i) 
   {
      *(mElements+i) = arV[i];
   }
}

/**
* Construction from libmda expression
**/
template<class T>
template<class A, libmda::Require_order<A,1> >
GeneralMidasVector<T>::GeneralMidasVector(const libmda::expr::expression_base<A>& aOther):
   mCapacity(aOther.size()), mLength(aOther.size()), mElements(NULL)
{
   Allocate();
   for(In i=I_0;i<mLength; ++i) 
   {
      mElements[i] = aOther.at(i);
   }
}

/**
 * Destructor
 **/
template<class T>
GeneralMidasVector<T>::~GeneralMidasVector
   (
   ) 
{
   Deallocate();
}

/**
* Return size of vector
* */
template<class T>
typename GeneralMidasVector<T>::size_type GeneralMidasVector<T>::Size() const { return mLength; }

/**
 * Zero vector
**/
template<class T>
void GeneralMidasVector<T>::Zero() 
{
   for (In i=I_0; i<mLength; ++i)
   {
      mElements[i] = T();
   }
}
/**
* Unit vector
* */
template<class T>
void GeneralMidasVector<T>::UnitVector(In aI) 
{
   Zero(); 
   mElements[aI]=T(C_1); 
}
/**
 * change the size within current capacity
 **/
template<class T>
void GeneralMidasVector<T>::SetNewSizeWithinCapacity(In aNewSize)
{ 
   if (aNewSize > mCapacity) 
   {
      MIDASERROR("new size larger than capacity"); 
   }
   mLength = aNewSize; 
} 
/**
* Change sign 
* */
template<class T>
void GeneralMidasVector<T>::ChangeSign() 
{
   for (In i=I_0; i<mLength; ++i)
   {
      mElements[i] = -mElements[i];
   }
}
/**
* Scale by scalar 
* */
template
   <  class T
   >
template
   <  class U
   >
void GeneralMidasVector<T>::Scale
   (  const U aNb
   ,  In aBegin
   ,  In aN
   ) 
{
   if (aBegin==I_0&&aN==-I_1) 
   {
      for(In i=I_0; i<mLength; ++i)
      {
         mElements[i] *= aNb;
      }
   }
   else
   {
      if (  aBegin + aN > mLength   )
      {
         MIDASERROR(" Out of range in GeneralMidasVector Scale ");
      }
      for(In i=I_0; i<aN; ++i)
      {
         mElements[i+aBegin] *= aNb;
      }
   }
}

/**
* Change the capacity of the vector.
* Old data are always retained.
*/
template<class T>
void GeneralMidasVector<T>::Reserve
   ( const In aNewCapacity
   )
{
   if (  aNewCapacity == mCapacity
      )
   {
      return;
   }

   if (aNewCapacity < mLength)
   {
      MIDASERROR("GeneralMidasVector<T>::Reserve(): Requested capacity smaller than current length.");
   }

   if (mLength > I_0 && aNewCapacity > I_0)      // We have to save the old data.
   {
      std::unique_ptr<T[]> tmp(new T[mLength]);
      for(In i=I_0; i<mLength; ++i)
      {
         tmp[i] = mElements[i];
      }
      Deallocate();
      mCapacity = aNewCapacity;
      Allocate();
      for(In i=I_0; i<mLength; ++i)
      {
         mElements[i] = tmp[i];
      }
   }
   else
   {
      Deallocate();
      mCapacity = aNewCapacity;
      Allocate();
   }
}

/**
* Set new length of vector.
* If new length is within the capacity: Old elements are retained.
* If new length is larger than the capacity: Old elements are retained
* only if aSaveOldData==true. Capacity will be new length.
*/
template<class T>
void GeneralMidasVector<T>::SetNewSize
   ( const In& aNewLength
   , const bool aSaveOldData
   )
{
   if (  aNewLength == mLength
      )
   {
      return;
   }

   if (aNewLength <= mCapacity)
   {
      mLength = aNewLength;
   }
   else if (aSaveOldData)   // aNewLength > mCapacity.
   {
      Reserve(aNewLength);
      mLength = aNewLength;
   }
   else
   {
      Deallocate();
      mLength = aNewLength;
      mCapacity = aNewLength;
      Allocate();
   }
}
/**
  * Clear the vector to free up memory.
  **/
template<class T>
void GeneralMidasVector<T>::Clear()
{
   Deallocate();
   mLength = I_0;
   mCapacity = I_0;
   mElements = new T[mCapacity];
}

/**
 * set data to pointer (use with caution)
 **/
template<class T>
void GeneralMidasVector<T>::SetData(T*& aElements, In aCapacity, In aLength) 
{ 
   Deallocate(); // first destroys old pointer to avoid leaks
   mElements = aElements;
   aElements = nullptr; // <- MidasVector now takes ownership over pointer
   mCapacity=aCapacity; 
   mLength=aLength; 
}

/**
  * Steal the data from another vec 
  * (move the data from victimvec to this by stealing the pointer)
  **/
template<class T>
void GeneralMidasVector<T>::StealData(GeneralMidasVector<T>& aVictimVec)
{
   Deallocate();

   mElements = aVictimVec.mElements;
   mLength = aVictimVec.mLength;
   mCapacity = aVictimVec.mCapacity;

   aVictimVec.SetPointerToNull();
}

/**
 * This object releases ownership of the stored data, i.e. is no longer
 * responsible for its deallocation.
 * The returned unique_ptr takes responsibility of the data/deallocation.
 * At return, this object is left in a valid, empty state, i.e. no allocated
 * data and zero Size()/Capacity().
 *
 * @note
 *    Intended as a safer alternative to using methods data(),
 *    SetPointerToNull(), SetData(), StealData().
 *    Safer due to the use of std::unique_ptr<T[]> which will delete the data
 *    by default, even if used uncautiously.
 *
 * @return
 *    unique_ptr that now has the responsibility for the data.
 **/
template<class T>
std::unique_ptr<T[]> GeneralMidasVector<T>::Release
   (
   )
{
   std::unique_ptr<T[]> p(this->data());
   this->SetPointerToNull();
   return p;
}

/**
 * 
 **/
template<class T>
void GeneralMidasVector<T>::MatrixRowByRow
   (  const GeneralMidasMatrix<T>& arM
   ,  size_t aColOffSet
   ,  size_t aRowOffSet
   )
{
   assert(aRowOffSet <= arM.Nrows() && aColOffSet <= arM.Ncols());

   if ((arM.Nrows()-aRowOffSet)*(arM.Ncols()-aColOffSet) != mLength) 
   {
      Mout << " GeneralMidasVector<T>::MatrixRowByRow, size of vector and matrix does not fit" << endl;
      MIDASERROR(" GeneralMidasVector<T>::MatrixRowByRow, size of vector and matrix does not fit");
   }

   In i_v = 0;
   for(In i=aRowOffSet; i<arM.Nrows(); ++i)
   {
      for (In j=aColOffSet; j<arM.Ncols(); ++j)
      {
         mElements[i_v++] = static_cast<T>(arM(i,j));
      }
   }
}

/**
 * 
 **/
template<class T>
void GeneralMidasVector<T>::MatrixColByCol
   (  const GeneralMidasMatrix<T>& arM
   ,  size_t aColOffSet
   ,  size_t aRowOffSet
   )
{
   assert(aRowOffSet <= arM.Nrows() && aColOffSet <= arM.Ncols());

   if ((arM.Nrows()-aRowOffSet)*(arM.Ncols()-aColOffSet) != mLength) 
   {
      Mout << " GeneralMidasVector<T>::MatrixColByCol, size of vector and matrix does not fit" << endl;
      MIDASERROR(" GeneralMidasVector<T>::MatrixColByCol, size of vector and matrix does not fit");
   }
   In i_v = 0;
   for(In i=aColOffSet; i<arM.Ncols(); ++i)
   {
      for(In j=aRowOffSet; j<arM.Nrows(); ++j)
      {
         mElements[i_v++] = static_cast<T>(arM(j,i));
      }
   }
}

/**
* Put some elements equal to a constant.
* */
template<class T>
void GeneralMidasVector<T>::PutToNb(const T aNb, In aLength, In aStart, In aStride)
{
   In i_add = aStart;
   In i=I_0;
   if (aStart+(aLength-1)*aStride > mLength) 
   {
      Mout << " Something wrong with start/stride/length in GeneralMidasVector<T>::PutToNb " << endl;
      MIDASERROR(" Stop in GeneralMidasVector<T>::PutToNb due to index out of range ");
   }
   while (i<aLength)
   {
      mElements[i_add] = aNb;
      i_add += aStride; 
      ++i;
   } 
   if (gDebug) Mout << " Vector has been put to number : " << std::endl;
}
/**
* Get a Subvector between the elements aStart -- aEnd
* */
template<class T>
GeneralMidasVector<T> GeneralMidasVector<T>::GetSubVec(const In& aStart, const In& aEnd) const
{
   assert( aEnd >= aStart );

   In lengths=aEnd-aStart+I_1;
   GeneralMidasVector<T> sub_vec(lengths);
   for (In i=aStart; i<=aEnd ; ++i) 
   {
      sub_vec.mElements[i-aStart] = mElements[i];
   }
   return sub_vec;
}
/**
* Fill some elements with the entries of another vector.
* */
template
   <  class T
   >
void GeneralMidasVector<T>::FillPart
   (  const GeneralMidasVector<T>& arVec
   ,  const In& aStart
   )
{
   In i=I_0;
   if (aStart+ arVec.Size() > mLength) 
   {
      Mout << " Vector does not fit in the other one in GeneralMidasVector<T>::FillPart " << endl;
      MIDASERROR(" Stop in GeneralMidasVector<T>::FillPart due to index out of range ");
   }
   for (In i=aStart; i<arVec.Size()+aStart ; ++i) 
   {
      mElements[i] = arVec[i-aStart];
   }
   if (gDebug) Mout << " Vector has been (partly) filled with another : " << endl;
}
/**
* Append by another midas vector 
* */
template<class T>
void GeneralMidasVector<T>::Append(const GeneralMidasVector<T>& arVec)
{
   In old_length=mLength;
   Reserve(mCapacity+arVec.Capacity());
   SetNewSizeWithinCapacity(mLength+arVec.Size());
   for (In i=old_length; i<mLength; ++i) 
   {
      mElements[i] = arVec[i-old_length];
   }
}
/**
 * * Put elements to a power string 
 * * */
template<class T>
void GeneralMidasVector<T>::Pow(const vector<In> aPower)
{
// *** check the dimensions

   if (aPower.size() != this->Size())
   {
      Mout << " Wrong Dimesions in GeneralMidasVector<T>::Pow  " << endl;
      Mout << " size of aPower is :  " << aPower.size()  << endl;
      Mout << " size of MidasVector is :  " << this->Size()  << endl;
      MIDASERROR(" Stop in GeneralMidasVector<T>::Pow due to inconsistent shape with aPower vector ");
   }
   for (In i=I_0; i<mLength; ++i)
   {
      mElements[i] = std::pow(mElements[i], aPower[i]);
   }
}

/**
* Put elements to some power 
* */
template<class T>
void GeneralMidasVector<T>::Pow(const Nb aPower)
{
   for(In i=I_0; i<mLength; ++i)
   {
      mElements[i] = std::pow(mElements[i], aPower);
   }
}
/**
* Put elements to some power 
* */
template<class T>
void GeneralMidasVector<T>::Pow(const In aIpow)
{
   if (aIpow == I_1) return;
   if (aIpow == I_0) 
   {
      T one=T(C_1);
      PutToNb(one,mLength);
   }
   else
   {
      Nb power = aIpow;
      Pow(power);
   }
}

/**
* Shift a diagonal by aNb
* */
template<class T>
void GeneralMidasVector<T>::Shift(const T aNb)
{
   for(In i=I_0; i<mLength; ++i)
   {
      mElements[i] += aNb;
   }
}

/**
* Put to diagonal of matrix 
**/
template<class T>
void GeneralMidasVector<T>::PutToDiag(GeneralMidasMatrix<T>& aM)
{
   if (  aM.Nrows() < mLength
      || aM.Ncols() < mLength
      )
   {
      MIDASERROR( " Matrix to small for PutToDiag");
   }
   for(In i=I_0; i<mLength; ++i)
   {
      mElements[i] = (T)aM[i][i];
   }
}

/**
* Copy the first elements 
**/
template<class T>
void GeneralMidasVector<T>::Copy(const GeneralMidasVector<T>& aV, In aLength)
{
   if (  aLength == -1  )
   {
      aLength = aV.Size();
   }
   if (  aLength > Size()  )
   {
      MIDASERROR( " Vector::Copy size of result is too small ");
   }
   if (  aLength > aV.Size()  )
   {
      MIDASERROR( " Vector::Copy size of vector from is too small ");
   }
   for (In i=I_0; i<aLength; ++i)
   {
      mElements[i] = aV[i];
   }
}
/**
* Reassign vector to the aLength elements of another vector
* */
template<class T>
void GeneralMidasVector<T>::Reassign(const GeneralMidasVector<T>& aV, In aLength)
{
   if (aLength == -1) aLength = aV.Size();
   SetNewSize(aLength,false); // Set new size without saving old
   Copy(aV,aLength);// Copy the required number of elements from the other vector
}

/***************************************************************************//**
 * These are tag dispatch solutions that forward the call to PieceIoImpl in
 * such a way that this object and the argument vector are const correct, as
 * according to the use of IO_GET/IO_PUT respectively.
 *
 * @param arIo
 *    IoType, either IO_GET (from this obejct) or IO_PUT (to this object).
 *    (IO_GET/IO_PUT defined in inc_gen/Const.h).
 * @param arOther
 *    The vector to which data is assigned (IO_GET) or taken from (IO_PUT).
 * @param arArgs
 *    Arguments to be forwarded to PieceIoImpl().
 ******************************************************************************/
template<class T>
template<int N, class... Args, std::enable_if_t<bool(N & IO_GET), int>>
void GeneralMidasVector<T>::PieceIo
   (  const IoType<N>& arIo
   ,  GeneralMidasVector<T>& arOther
   ,  Args... arArgs
   )  const
{
   PieceIoImpl(*this, arIo, arOther, std::forward<Args>(arArgs)...);
}

template<class T>
template<int N, class... Args, std::enable_if_t<bool(N & IO_PUT), int>>
void GeneralMidasVector<T>::PieceIo
   (  const IoType<N>& arIo
   ,  const GeneralMidasVector<T>& arOther
   ,  Args... arArgs
   )
{
   PieceIoImpl(*this, arIo, arOther, std::forward<Args>(arArgs)...);
}

/***************************************************************************//**
 * Implementation - only to be called through PieceIo().
 *
 * @param arThis
 *    "This object", i.e. the one calling PieceIo(). THIS is const
 *    (IO_GET)/non-const (IO_PUT) type depending on which PieceIo() was called.
 * @param[in] aPutGet
 *    IoType defining whehter to IO_GET from/IO_PUT to arThis. OTHER is const
 *    (IO_PUT)/non-const (IO_GET) type depending on which PieceIo() was called.
 * @param aV
 *    The vector to which data is assigned (IO_GET) or taken from (IO_PUT).
 * @param[in] aLength
 *    The number of elements to transfer.
 * @param[in] aThisStart
 *    Index at which to start in arThis.
 * @param[in] aThisStride
 *    Stride in arThis.
 * @param[in] aPieceStart
 *    Index at which to start in aV.
 * @param[in] aPieceStride
 *    Stride in aV.
 * @param[in] aAddTo
 *    Whether to add the data to the target (in stead of assignment).
 * @param[in] aScaleFac
 *    If "adding to" scale data to be added by this factor. _Only_ has an
 *    effect if aAddTo is true.
 ******************************************************************************/
template<class T>
template<class THIS, int N, class OTHER>
void GeneralMidasVector<T>::PieceIoImpl
   (  THIS& arThis
   ,  const IoType<N>& aPutGet
   ,  OTHER& aV
   ,  In aLength
   ,  In aThisStart
   ,  In aThisStride
   ,  In aPieceStart
   ,  In aPieceStride
   ,  bool aAddTo
   ,  T aScaleFac
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   In i_add = aThisStart;
   In i_add_piece = aPieceStart;
   In i=I_0;
   if constexpr(bool(constexpr_io_type & IO_PUT))
   {
      if (aAddTo)
      {
         while (i<aLength)
         {
            arThis.mElements[i_add] += aScaleFac*aV[i_add_piece];
            i_add       += aThisStride;
            i_add_piece += aPieceStride;
            i++;
         }
      }
      else
      {
         while (i<aLength)
         {
            arThis.mElements[i_add] = aV[i_add_piece];
            i_add       += aThisStride;
            i_add_piece += aPieceStride;
            i++;
         }
      }
      if (gDebug) Mout << " GeneralMidasVector<T>::PieceIo put" << endl;
   }
   else if constexpr(bool(constexpr_io_type & IO_GET))
   {
      if (aAddTo)
      {
         while (i<aLength)
         {
            aV[i_add_piece] += aScaleFac*arThis.mElements[i_add];
            i_add       += aThisStride;
            i_add_piece += aPieceStride;
            i++;
         }
      }
      else
      {
         while (i<aLength)
         {
            aV[i_add_piece]= arThis.mElements[i_add];
            i_add       += aThisStride;
            i_add_piece += aPieceStride;
            i++;
         }
      }
      if (gDebug) Mout << " GeneralMidasVector<T>::PieceIo get" << endl;
   }
   else
   {
      static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                     ,  "One of IO_PUT and IO_GET must be set!"
                     );
   }
}

/**
* Sum of aN elements (with sign) starting at address aOff.
* */
template<class T>
typename GeneralMidasVector<T>::value_type GeneralMidasVector<T>::SumEle(In aOff, In aN) const
{
   if (  -I_1 == aN  )
   {
      aN = mLength - aOff;
   }
   value_type sum = value_type(C_0);
   for(In i=aOff; i<aOff+aN; ++i)
   {
      sum += mElements[i];
   }
   return sum; 
}

/**
 * * Product of elements (with sign)
 * * */
template<class T>
typename GeneralMidasVector<T>::value_type GeneralMidasVector<T>::ProductEle() const
{
   value_type product = value_type(C_1);
   for(In i=I_0; i<mLength; ++i)
   {
      product  *= mElements[i];
   }
   return product;
}


/**
* add absolute values of all elements (identical to onenorm).
* */
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::SumAbsEle() const
{
   real_t sum = real_t(C_0);
   for (In i=I_0; i<mLength; ++i)
   {
      sum += std::abs(*(mElements+i));
   }
   return sum; 
}
/**
* Product of elements (absolute values)
**/
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::ProductAbsEle() const
{
   real_t product = real_t(C_1);
   for(In i=I_0; i<mLength; ++i)
   {
      product *= std::abs(mElements[i]);
   }
   return product;
}
/**
* add absolute values of all to get onenorm (identical to sumabsele).
* */   
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::Norm1() const
{
   real_t norm = real_t(C_0);
   for(In i=I_0; i<mLength; ++i)
   {
      norm += std::abs(*(mElements+i));
   }
   return norm;
}
/**
 * RMS
 **/
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::Rms() const
{
   real_t rms(0);
   for (In i=I_0; i<mLength; ++i)
   {
      rms += midas::util::AbsVal2(*(mElements+i));
   }
   rms /= mLength;
   rms = sqrt(rms);
   return rms;
}

/**
* add values squared to get two-norm.
* */   
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::Norm2
   (  In aOff
   ,  In aN
   )  const
{
   if (-I_1 == aN) aN = mLength - aOff;
   real_t norm2(C_0);
   for (In i=aOff; i<aOff+aN; ++i)
   {
      norm2 += midas::util::AbsVal2(*(mElements+i));
   }
   return norm2;
}

/***************************************************************************//**
 * 
 ******************************************************************************/   
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::DiffNorm2
   (  const GeneralMidasVector<T>& arOther
   ,  In aOff
   ,  In aN
   )  const
{
   if (Size() != arOther.Size())
   {
      MIDASERROR("Size mismatch; Size() (which is "+std::to_string(Size())+") != arOther.Size() (which is "+std::to_string(arOther.Size())+").");
   }
   if (-I_1 == aN) aN = mLength - aOff;
   Uin end = std::min(Uin(aOff + aN), Uin(Size()));
   real_t diffnorm2(0);
   for (Uin i=aOff; i < end; ++i)
   {
      diffnorm2 += midas::util::AbsVal2(this->operator[](i) - arOther[i]);
   }
   return diffnorm2;
}


/***************************************************************************//**
 * 
 ******************************************************************************/   
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::DiffNorm
   (  const GeneralMidasVector<T>& arOther
   ,  In aOff
   ,  In aN
   )  const
{
   return std::sqrt(DiffNorm2(arOther, aOff, aN));
}

template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::DiffOneNorm
   (  const GeneralMidasVector<T>& arOther
   )  const
{
   if (Size() != arOther.Size())
   {
      MIDASERROR("Size mismatch; Size() (which is "+std::to_string(Size())+") != arOther.Size() (which is "+std::to_string(arOther.Size())+").");
   }
   real_t diffnorm(0);
   for (Uin i=I_0; i < Size(); ++i)
   {
      diffnorm += midas::util::AbsVal(this->operator[](i) - arOther[i]);
   }
   return diffnorm;
}

/***************************************************************************//**
 * 
 ******************************************************************************/   
template<class T>
template<bool CONJ_THIS>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::DiffNorm2Scaled
   (  const GeneralMidasVector& arOther
   ,  const T aThisScale
   ,  const T aOtherScale
   ,  In aOff
   ,  In aN
   )  const
{
   if (Size() != arOther.Size())
   {
      MIDASERROR("Size mismatch; Size() (which is "+std::to_string(Size())+") != arOther.Size() (which is "+std::to_string(arOther.Size())+").");
   }
   if (aN < 0) aN = mLength - aOff;
   Uin end = std::min(Uin(aOff + aN), Uin(Size()));
   real_t diffnorm2(0);
   for (Uin i=aOff; i < end; ++i)
   {
      auto this_i = this->operator[](i);
      if constexpr(CONJ_THIS)
      {
         this_i = midas::math::Conj(this_i);
      }
      diffnorm2 += midas::util::AbsVal2(aThisScale*this_i - aOtherScale*arOther[i]);
   }
   return diffnorm2;
}

/**
* add values squared to get two-norm. Second version.
* */
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::NormRobust() const
{
   real_t norm = real_t(C_0);
   for (In i = I_0; i < mLength; ++i)
   {
      real_t avi = std::abs(*(mElements+i));
      if (  norm < C_100
         && avi < C_100
         )
      {
         norm = sqrt(norm*norm + avi*avi);
      }
      else if  (  norm > avi  )
      {
         norm *= sqrt(I_1 + pow(avi/norm,C_2));
      }
      else 
      {
         norm = avi*sqrt(I_1 + pow(norm/avi,C_2));
      }
   }
   return norm;
}


/**
* Find maximum absolute value of all elements for max norm
* */
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::NormMax() const
{
   real_t norm = real_t(C_0);
   for (In i=I_0; i<mLength; ++i) 
   {
      norm = max(norm,std::abs(*(mElements+i)));
   }
   return norm;
}

/**
* Find usual norm = sqrt of twonorm
* */
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::Norm() const
{
   return std::sqrt(Norm2());
}

/**
* Find Minimum value
* */
template<class T>
typename GeneralMidasVector<T>::value_type GeneralMidasVector<T>::FindMinValue() const
{
   value_type min = T(C_NB_MAX);
   for (In i=I_0; i<mLength; ++i) 
   {
      if( mElements[i] < min  )
      {
         min = mElements[i];
      }
   }
   return min;
}
/**
* Find Maximum value
* */
template<class T>
typename GeneralMidasVector<T>::value_type GeneralMidasVector<T>::FindMaxValue() const
{
   value_type max = value_type(-C_NB_MAX);
   for (In i=I_0; i<mLength; ++i)
   {
      if (  mElements[i] > max   )
      {
         max = mElements[i];
      }
   }
   return max;
}
/**
  * Find Maximum absolute value
  * */
template<class T>
typename GeneralMidasVector<T>::real_t GeneralMidasVector<T>::FindMaxAbsValue() const
{
   real_t max = real_t(-C_NB_MAX);
   for(In i=I_0; i<mLength; ++i)
   {
      if (  std::abs(mElements[i]) > max   )
      {
         max = std::abs(mElements[i]);
      }
   }
   return max;
}
/**
* Dump vector to a file in readable format 
* */
template<class T>
void GeneralMidasVector<T>::DumpToReadableFileWithName(std::string aS) const
{
   ofstream out_stream(aS.c_str(),ios_base::out);
   MidasStreamBuf out_buf(out_stream);
   MidasStream out(out_buf);
   midas::stream::ScopedPrecision(25, out);
   out.setf(ios::scientific);
   out.setf(ios::showpoint);
   out.setf(ios::uppercase);
   for(In i=I_0; i<mLength; ++i)
   {
      out << i << " " << mElements[i] <<endl;
   }
}
/**
* Calculate and output various statistical quantities and histograms
* */
template<class T>
void GeneralMidasVector<T>::StatAnalysisAndOutPut
   (  Nb& arAve
   ,  Nb& arAveDev
   ,  Nb& arStdDev
   ,  Nb& arVar
   ,  Nb& arSkew
   ,  Nb& arKurt
   ,  std::string& arHeader
   ,  In aHistSize
   ,  Nb aMinHist
   ,  Nb aMaxHist
   ,  std::string aDir
   ,  std::string aFilNam1
   ,  std::string aFilNam2
   )  const
{
   Out72Char(Mout,'+','-','+');
   In n_hist_size = aHistSize;
   auto n_calcs = Size();
   Nb min_hist = aMinHist;
   Nb max_hist = aMaxHist;
   MidasVector hist_mid_vals(n_hist_size);
   std::vector<In> hist_scores(n_hist_size);
   MidasVector hist_rel(n_hist_size);

   StatAnalysis(arAve,arAveDev,arStdDev,arVar,arSkew,arKurt);
   midas::stream::ScopedPrecision(15, Mout);
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   Mout.setf(ios::showpoint);
   //p_out.OutPut(Mout,false);
   Mout << arHeader<< endl;
   Out72Char(Mout,'+','=','+');
   Mout << " Average value               " <<  setw(I_22) << arAve << endl;
   Mout << " Average (abs) Deviation     " <<  setw(I_22) << arAveDev << endl;
   Mout << " Standard Deviation          " <<  setw(I_22) << arStdDev << endl;
   Mout << " Variance                    " <<  setw(I_22) << arVar  << endl;
   Mout << " Skewness (third moment)     " <<  setw(I_22) << arSkew << endl;
   Mout << " Kurtosis (fourth moment)    " <<  setw(I_22) << arKurt << endl;
   Mout << " Minimum value               " <<  setw(I_22) << FindMinValue() << endl;
   Mout << " Maximum value               " <<  setw(I_22) << FindMaxValue() << endl;
   Nb sqr = Nb(sqrt(C_1*Nb(n_calcs)));
   Mout << " Std. error of mean          " <<  setw(I_22) << arStdDev/sqr << endl;
   Out72Char(Mout,'+','-','+');

   Nb delta = C_0;
   min_hist -= min_hist*C_NB_EPSILON*C_10; // Std. choice - avoid num prob.
   max_hist += min_hist*C_NB_EPSILON*C_10;
   MakeHisto(n_hist_size,hist_mid_vals,hist_scores,hist_rel, 
             min_hist,max_hist,delta);
   if (gAnalysisIoLevel > I_2)
   {
      Mout << " Histogram " << endl;
//              [-2.06422712E-03;-1.98441516E-03[       3   1.58200124E+01
      Mout << " Interval                            Count   Relative Count/Int.size"<<endl;
      midas::stream::ScopedPrecision(8, Mout);
      for (In ih=I_0;ih<n_hist_size;ih++)
      {
         Mout.setf(ios::showpos);
         Mout << " [" << hist_mid_vals[ih] -  delta*C_I_2;
         Mout << ";" << hist_mid_vals[ih] +  delta*C_I_2;
         if (ih<n_hist_size-I_1) Mout << "[";
         if (ih==n_hist_size-I_1) Mout << "]";
         Mout.unsetf(ios::showpos);
         Mout << "   " << setw(5) << hist_scores[ih];
         Mout << "   " << hist_rel[ih] << endl;
      }
   }
   if (n_hist_size>I_1) // Otherwise plot makes no sense
   {
      std::string hist_name = aDir + "/"+ aFilNam1; 
      ofstream hist_stream(hist_name.c_str(),ios_base::out);
      MidasStreamBuf hist_buf(hist_stream);
      MidasStream hist(hist_buf);
      midas::stream::ScopedPrecision(15, hist);
      hist.setf(ios::scientific);
      hist.setf(ios::uppercase);
      hist.setf(ios::showpoint);
      for (In ih=I_0;ih<n_hist_size;ih++)
         hist << " " << hist_mid_vals[ih] 
              << "  " << hist_rel[ih] << endl;
      hist_stream.close();

      Mout << " Creating gnuplot input and .ps files with name "
           << aDir + "/" + aFilNam2 << endl;
      Plot hst_plt(hist_mid_vals.Size());
      hst_plt.SetTitle(arHeader + "(" + std::to_string(n_calcs) + " data points)");
      hst_plt.SetXlabel(arHeader);
      hst_plt.SetYlabel("Relative count");
      hst_plt.SetXYvals(hist_mid_vals, hist_rel);
      hst_plt.SetLineStyle("histeps");
      hst_plt.AddAnaNormDist(arAve, arStdDev);
      hst_plt.SetPlotRangeRel(0, 0, 0.1, 0.1);
      hst_plt.MakePsFile(aDir + aFilNam2);
   }
   Out72Char(Mout,'+','-','+');
   Mout << endl;
}
/**
* Calculate various statistical quantities 
* */
template<class T>
void GeneralMidasVector<T>::StatAnalysis(Nb& arAve, Nb& arAveDev, Nb& arStdDev, Nb& arVar, Nb& arSkew, Nb& arKurt) const
{
   arAveDev = C_0;
   arStdDev = C_0;
   arVar    = C_0;
   arSkew   = C_0;
   arKurt   = C_0;
   arAve    = SumEle();
   if (mLength > I_1)
   {
      arAve /= mLength;
      Nb ep = C_0;
      for (In i=I_0;i<mLength;i++) 
      {
         Nb cur = mElements[i] - arAve;
         Nb cur2 = cur*cur;
         arAveDev += fabs(cur);
         ep       += cur;
         arVar    += cur2;
         arSkew   += cur2*cur;
         arKurt   += cur2*cur2;
      }
      arAveDev /= mLength;
      arVar = (arVar - ep*ep/mLength)/(mLength-I_1); // correction for num. reasons, Press, Ch.14
      arStdDev = sqrt(arVar);
      if (arVar != C_0)
      {
         arSkew   /= (mLength*arVar*arStdDev);
         arKurt   /= (mLength*arVar*arVar);
      }
      else
      {
         Mout << " NB! Skewness/Kurtosis not defined since variance is zero " << endl;
      }
   }
}
/**
* Set up a histogram 
* */
template<class T>
void GeneralMidasVector<T>::MakeHisto(In aNhistoLevels, GeneralMidasVector& arMidVals, 
 std::vector<In>& arScores, GeneralMidasVector& arRelScore, 
 Nb aMin, Nb aMax, Nb& arDelta) const
{
   if (mLength>I_1)
   {
      Nb range = aMax - aMin;
      if (range < C_0) MIDASERROR( " Range less than zero in GeneralMidasVector<T>::MakeHisto");
      arDelta = range/aNhistoLevels;
      if (aNhistoLevels > arScores.size()) 
         MIDASERROR("Not space enough for arScores in MakeHisto");
      if (aNhistoLevels > arRelScore.Size()) 
         MIDASERROR("Not space enough for arRelScore in MakeHisto");
      if (aNhistoLevels > arMidVals.Size()) 
         MIDASERROR("Not space enough for arMidVals in MakeHisto");
   
      for (In i=I_0;i<aNhistoLevels;i++)
      {
        arMidVals[i] = aMin + (i+C_I_2)*arDelta;
        arScores[i] = I_0;
      }
      
      for (In i=I_0;i<mLength;i++)
      {
         Nb rel = mElements[i]-aMin;
         In i_rel = In(rel/arDelta);
         if (i_rel>=aNhistoLevels) i_rel=aNhistoLevels-I_1;
         arScores[i_rel]++;
      }
      Nb l_inv = C_1/mLength;
      Nb l_inv_d =l_inv/arDelta;
      for (In i=I_0;i<aNhistoLevels;i++)
      {
         arRelScore[i] = arScores[i]*l_inv_d;
      }
   }
   else if (mLength == I_1 && aNhistoLevels == I_1)
   {
      arMidVals[I_0]  = aMin;
      arScores[I_0]   = I_1;
      arRelScore[I_0] = I_1;
   }
}
/**
* = assign operator
* */
template<class T>
GeneralMidasVector<T>& GeneralMidasVector<T>::operator=(const GeneralMidasVector<T>& arVec)
{
   if (this != &arVec)
   {
      //if (mLength != arVec.mLength) MidasWarning("MidasVector mismatch in MidasVector operator =");
      //if (mLength != arVec.mLength) SetNewSize(arVec.mLength); 
      if (  mLength != arVec.mLength   )
      {
         MIDASERROR("GeneralMidasVector mismatch in GeneralMidasVector operator = (" + std::to_string(mLength) + " vs. " + std::to_string(arVec.mLength) +")");
      }
      for(In i=I_0; i<mLength; ++i)
      {
         mElements[i] = *(arVec.mElements+i);
      }
   }
   return *this;
}

/**
* = assign operator
* */
template<class T>
GeneralMidasVector<T>& GeneralMidasVector<T>::operator=(GeneralMidasVector<T>&& arVec)
{
   std::swap(this->mLength, arVec.mLength);
   std::swap(this->mCapacity, arVec.mCapacity);
   std::swap(this->mElements, arVec.mElements);
   return *this;
}

/**
* MidasVector inner product. First argument is complex conjugated (for complex T).
* */
template
   <  class T
   >
T Dot
   (  const GeneralMidasVector<T>& arVec1
   ,  const GeneralMidasVector<T>& arVec2
   ,  In aBegin
   ,  In aNdot
   )
{ 
   return Dot(arVec1, arVec2, aBegin, aBegin, aNdot);
}

/**
* MidasVector inner product  
* */
template
   <  class T
   >
T Dot
   (  const GeneralMidasVector<T>& arVec1
   ,  const GeneralMidasVector<T>& arVec2
   ,  In aBegin1
   ,  In aBegin2
   ,  In aNdot
   )
{ 
   LOGCALL("MidasVector Dot");
   In n_dot_over = (aNdot == -I_1) ? arVec1.Size() : aNdot;

   // If we dot all elements, we require the vectors to have equal length
   if (  aNdot == -I_1
      && arVec1.Size() != arVec2.Size()
      )
   {
		std::stringstream ss;
		ss << "GeneralMidasVector mismatch in Dot product"
		   << "	arVec1.Size() = " << arVec1.Size() << '\n'
		   << "	arVec2.Size() = " << arVec2.Size() << '\n'
			<< "  aBegin1 = " << aBegin1 << '\n'
			<< "  aBegin2 = " << aBegin2 << '\n'
			<< "  aNdot  = " << aNdot << '\n'
			<< "  arVec1 = " << arVec1
			<< "  arVec2 = " << arVec2
			;
 		MIDASERROR(ss.str());
      //MIDASERROR( " GeneralMidasVector mismatch in Dot product");
   }

   T sum = T(C_0); 
   In end1 = aBegin1+n_dot_over;
   In end2 = aBegin2+n_dot_over;
   if (  end1>arVec1.Size()
      || end2>arVec2.Size()
      )
   {
      MIDASERROR(" Dot for Vectors go beyond !!");
   }

   for(In i=I_0; i<n_dot_over; ++i) 
   {
      sum += midas::math::Conj(*(arVec1.mElements+aBegin1+i)) * *(arVec2.mElements+aBegin2+i);
   }

   return sum;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template <class T>
T SumProdElems
   (  const GeneralMidasVector<T>& arA
   ,  const GeneralMidasVector<T>& arB
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   if (arA.Size() != arB.Size())
   {
      MIDASERROR("arA.Size() (which is "+std::to_string(arA.Size())+") != arB.Size() (which is "+std::to_string(arB.Size())+").");
   }

   const Uin end = std::min(Uin(arA.Size()), aEnd);
   T sum = 0;
   for(Uin i = aBeg; i < end; ++i)
   {
      sum += arA[i]*arB[i];
   }

   return sum;
}

/**
* << overload for ostream
* */
template<class T>
ostream& operator<<(ostream& arOut, const GeneralMidasVector<T>& arVec1)
{ 
   for(In i=I_0; i<arVec1.Size(); ++i)
   {
      arOut << " " << arVec1[i];
   }
   arOut << std::endl;

   return arOut;
}
/**
* axpy: y = y + a*x, a is a scalar, x, y are vectors.
* */
template
   <  class T
   ,  class U
   >
void StandardAxpy
   (  In aN
   ,  U aAlpha
   ,  const GeneralMidasVector<T>& arX
   ,  In aIncrX
   ,  GeneralMidasVector<T>& arY
   ,  In aIncrY
   )
{
//   #pragma omp parallel for
   for(In i=I_0; i<aN; ++i)
   {
      arY[i*aIncrY] += aAlpha*arX[i*aIncrX];
   }
}
/**
* zaxpy: z = y + a*x, a is a scalar, x, y, and z  are vectors.
* */
template<class T>
void StandardZaxpy
   (  In aN
   ,  T aAlpha
   ,  const GeneralMidasVector<T>& arX
   ,  In aIncrX
   ,  const GeneralMidasVector<T>& arY
   ,  In aIncrY
   ,  GeneralMidasVector<T>& arZ
   ,  In aIncrZ
   )
{
   for(In i=I_0; i<aN; ++i)
   {
      *(arZ.mElements+i*aIncrZ)  = aAlpha
                                 *  *(arX.mElements+i*aIncrX)
                                 +  *(arY.mElements+i*aIncrY)
                                 ;
   }
}

/**
* calculate root mean squared deviations
**/
template<class T>
void RmsDevPerElement
   (  Nb& arRms
   ,  const GeneralMidasVector<T>& arV1
   ,  const GeneralMidasVector<T>& arV2
   ,  const In& arNoOfData
   ,  const In& arOffsetV1
   ,  const In& arOffsetV2
   )
{
//*** check the size 
   if (  arOffsetV1 + arNoOfData > arV1.mLength )
   {
      MIDASERROR(" Out of range arV1 vector in RmsDevPerElement ");
   }

   if (  arOffsetV2 + arNoOfData > arV2.mLength )
   {
      MIDASERROR(" Out of range arV2 vector in RmsDevPerElement ");
   }

   arRms=C_0;
   for(In k = I_0; k < arNoOfData; ++k)
   {
      arRms += (arV1[k+arOffsetV1]-arV2[k+arOffsetV2]) * (arV1[k+arOffsetV1]-arV2[k+arOffsetV2]);
   }
   arRms = std::sqrt(arRms)/arNoOfData;
   return;
}

/**
*  Multiply with matrix
**/
template<class T>
void GeneralMidasVector<T>::MultiplyWithMatrix(const GeneralMidasMatrix<T>& aM)
{
   if(aM.Ncols() != mLength)
   {
      MIDASERROR("More columns than rows in mat-vec mult...");
   }
   GeneralMidasVector<T> tmp(aM.Nrows());
   for(In i=I_0; i<aM.Nrows(); ++i)
   {
      tmp[i]=T(C_0);
      for(In j=I_0; j<mLength; ++j)
      {
         tmp[i]+=aM[i][j]*mElements[j];
      }
   }
   SetNewSize(aM.Nrows());
   for(In i=I_0; i<mLength; ++i)
   {
      mElements[i]=tmp[i];
   }
}
/**
* Multiply with matrix from the left
**/
template<class T>
void GeneralMidasVector<T>::LeftMultiplyWithMatrix(const GeneralMidasMatrix<T>& aM) 
{
   if(aM.Nrows()!=mLength)
   {
      MIDASERROR("More columns than rows in mat-vec mult...");
   }
   GeneralMidasVector<T> tmp(aM.Ncols());
   for(In i=I_0; i<aM.Ncols(); ++i)
   {
      tmp[i]=T(C_0);
      for(In j=I_0; j<mLength; ++j)
      {
         tmp[i]+=mElements[j]*aM[j][i];
      }
   }
   SetNewSize(aM.Ncols());
   for(In k=I_0; k<mLength; ++k)
   {
      mElements[k]=tmp[k];
   }
}

/**
*
**/
template<class T>
GeneralMidasVector<T> GeneralMidasVector<T>::Conjugate() const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      GeneralMidasVector<T> tmp(mLength);
      for(In i=0; i<mLength; ++i)
      {
         tmp[i] = midas::math::Conj(mElements[i]);
      }
      return tmp;
   }
   else
   {
      return *this;
   }
}

template<class T>
void GeneralMidasVector<T>::PrintVertical(const std::string& arLabel)
{
   Mout << arLabel << endl;
   for(In i=I_0; i<mLength; ++i)
   {
      Mout << mElements[i] << std::endl;
   }
}

template<class T>
void GeneralMidasVector<T>::Normalize()
{
   T norm=Norm();
   for (In i=I_0; i<mLength;i++)
   { 
      mElements[i] /= norm;
   }
}

template<class T>
void GeneralMidasVector<T>::DumpVertical(const std::string& arLabel)
{
   std::ofstream out_file( arLabel.c_str(), ios_base::out );

   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, out_file);
   for(In i=I_0; i<mLength; ++i)
   {
      out_file << i+1 << "    " << mElements[i] << std::endl;
   }
   out_file.close();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T>
template<class InpIter>
void GeneralMidasVector<T>::Insert
   (  Uin aPos
   ,  InpIter aBeg
   ,  InpIter aEnd
   )
{
   // Assert position is in range.
   if (aPos > Size())
   {
      MIDASERROR("aPos (= "+std::to_string(aPos)+") > Size() (= "+std::to_string(Size())+").");
   }

   const Uin dist = std::distance(aBeg, aEnd);

   if (dist > 0)
   {
      // Set new size.
      SetNewSize(Size() + dist);

      // Move existing elements, looping backwards so as to not overwrite.
      // (In the loop body i will take values Size()-1, ..., aPos+dist.)
      for(Uin i = Size(); i-- > aPos+dist;)
      {
         this->operator[](i) = this->operator[](i-dist);
      }

      // Insert new range.
      for(Uin i = aPos; i < aPos + dist && aBeg != aEnd; ++i, ++aBeg)
      {
         this->operator[](i) = *aBeg;
      }
   }

   if (aBeg != aEnd)
   {
      MIDASERROR("aBeg != aEnd at the end; means implementation is faulty.");
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T>
void GeneralMidasVector<T>::Insert
   (  Uin aPos
   ,  T aVal
   )
{
   std::vector<T> tmp = {aVal};
   this->Insert(aPos, tmp.begin(), tmp.end());
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T>
void GeneralMidasVector<T>::Erase
   (  Uin aBeg
   ,  Uin aEnd
   )
{
   const Uin end = std::min(aEnd, Uin(Size()));
   if (aBeg < end)
   {
      const Uin dist = end - aBeg;
      for(Uin i = end; i < Size(); ++i)
      {
         this->operator[](i-dist) = this->operator[](i);
      }
      SetNewSize(Size()-dist);
   }
}

/************************************************************************************************
 * MPI interface
 ***********************************************************************************************/
#ifdef VAR_MPI

/**
 * Send data
 *
 * @param aRank   Recieving rank
 * @param aNData  Number of elements to send
 * @param aOffset Offset in mElements
 **/
template
   <  class T
   >
void GeneralMidasVector<T>::MpiSend
   (  In aRank
   ,  In aNData
   ,  In aOffset
   )  const
{
   // Set ndata
   auto ndata = aNData < I_0 ? mLength : aNData;
   assert(ndata+aOffset <= mLength);

   // Send size
   midas::mpi::detail::WRAP_Send(&mLength, 1, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::size_type>::Get(), aRank, 0, MPI_COMM_WORLD);

   // Send capacity
   midas::mpi::detail::WRAP_Send(&mCapacity, 1, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::size_type>::Get(), aRank, 0, MPI_COMM_WORLD);

   // Send data
   midas::mpi::detail::WRAP_Send(mElements+aOffset, ndata, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::value_type>::Get(), aRank, 0, MPI_COMM_WORLD);
}

/**
 * Recv data. Reallocate if mCapacity does not match.
 *
 * @param aRank   Sending rank
 * @param aNData  Number of elements to recv
 * @param aOffset Offset in mElements
 **/
template
   <  class T
   >
void GeneralMidasVector<T>::MpiRecv
   (  In aRank
   ,  In aNData
   ,  In aOffset
   )
{
   MPI_Status status;

   // Get new length
   midas::mpi::detail::WRAP_Recv(&mLength, 1, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::size_type>::Get(), aRank, 0, MPI_COMM_WORLD, &status);

   // Get new capacity
   size_type new_capacity;
   midas::mpi::detail::WRAP_Recv(&new_capacity, 1, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::size_type>::Get(), aRank, 0, MPI_COMM_WORLD, &status);

   // Check that new length and new capacity makes sense
   assert(mLength <= new_capacity);

   // Set ndata
   auto ndata = aNData < I_0 ? mLength : aNData;
   assert(ndata+aOffset <= mLength);

   // Reallocate if necessary
   if (  mCapacity != new_capacity
      )
   {
      Mout  << "MPI: Re-allocate GeneralMidasVector in MpiRecv!" << std::endl;

      // Deallocate
      this->Deallocate();

      mCapacity = new_capacity;

      // Allocate
      this->Allocate();
   }

   // Get data
   midas::mpi::detail::WRAP_Recv(mElements+aOffset, ndata, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::value_type>::Get(), aRank, 0, MPI_COMM_WORLD, &status);
}

/**
 * Bcast data.
 *
 * @param aRank   Source rank
 * @param aNData  Number of elements to send
 * @param aOffset Offset in mElements
 **/
template
   <  class T
   >
void GeneralMidasVector<T>::MpiBcast
   (  In aRank
   ,  In aNData
   ,  In aOffset
   )
{
   // Pack size and capacity
   std::array<size_type, 2> size_data = {{ this->mLength, this->mCapacity }};

   // Send size data
   midas::mpi::detail::WRAP_Bcast(size_data.data(), 2, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::size_type>::Get(), aRank, MPI_COMM_WORLD);

   // Get variables
   const auto& new_length = size_data[0];
   const auto& new_capacity = size_data[1];

   // Set ndata
   auto ndata = aNData < I_0 ? new_length : aNData;
   assert(ndata+aOffset <= new_length);

   // Reserve space and set new size
   if (  this->mCapacity != new_capacity
      )
   {
      this->Reserve(new_capacity);
   }
   if (  this->mLength != new_length
      )
   {
      this->SetNewSize(new_length, true);
   }


   // Send data
   midas::mpi::detail::WRAP_Bcast(mElements+aOffset, ndata, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::value_type>::Get(), aRank, MPI_COMM_WORLD);
}

/**
 * Ibcast data.
 * NB!!! mLength and mCapacity must be set correctly on all ranks!
 *
 * @param aRank   Source rank
 * @param apReq   Pointer to MPI_Request to wait for afterwards
 * @param aNData  Number of elements to send
 * @param aOffset Offset in mElements
 **/
template
   <  class T
   >
void GeneralMidasVector<T>::MpiIbcastElements
   (  In aRank
   ,  MPI_Request* apReq
   ,  In aNData
   ,  In aOffset
   )
{
   // Set ndata
   auto ndata = aNData < I_0 ? this->mLength : aNData;
   assert(ndata+aOffset <= this->mLength);

   // Send data
   midas::mpi::detail::WRAP_Ibcast(mElements+aOffset, ndata, midas::mpi::DataTypeTrait<typename GeneralMidasVector<T>::value_type>::Get(), aRank, MPI_COMM_WORLD, apReq);
}

#endif /* VAR_MPI */


#endif /* MIDASVECTOR_IMPL_H */
