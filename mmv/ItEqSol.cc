/**
************************************************************************
* 
* @file                ItEqSol.cc
*
* Created:             21-01-2003
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*                      Werner Gyorffy (werner@chem.au.dk)
*
* Short Description:   Implementing iterative solution of equations
* 
* Last modified: Sun Mar 29, 2009  04:19PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<fstream>
#include<string>
#include<utility>

// limbda headers
#include "libmda/numeric/float_eq.h"

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/UnitNumbers.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/ItEqSol.h"
#include "mmv/DataCont.h"
#include "util/Io.h"
#include "input/Input.h"
#include "mmv/Transformer.h"
#include "mmv/Diag.h"
#include "vcc/VccTransformer.h"
#include "mpi/MpiLog.h"

// using declarations
using std::endl;
using std::ofstream;
using std::ifstream;
using std::string;

// forward declarations
void BckUpVec(DataCont& arC) 
{
   if (arC.InMem()) 
      arC.DumpToDisc();
}

/**
* Constructor - set defaults
* */
ItEqSol::ItEqSol
   (  const In& aVecStorageLevel
   ,  const std::string& aAnalysisDir
   )
   :  mRedHam(I_0)
   ,  mRedSolVec(I_0)
   ,  mRedRhs(I_0)
   ,  mPreDiagMat(I_0)
   ,  mTarTriS(I_0)
   ,  mRedMetric(I_0)
{
   mRedDim           = I_0;    ///< Current dimension of reduced space
   mDim              = I_0;    ///< Current dimension of actual space 
   mNiter            = I_0;    ///< Current iteration number 
   mNnew             = I_0;    ///< Number of new vectors in a round
   mNmicro           = I_0;    ///< Nr of micro iteratios 

   mNeq              = I_0;    ///< Number of equations to be solved (nr of eig)
   mNiterMax         = I_10;   ///< Maximum iteration number 
   mRedDimMax        = I_20;   ///< Maximum dim of reduced space
   mNbreakDim        = I_20;   ///< Dim for which we break and restart 

   mRedDimMaxSet     = false;  ///< the reddimmax has been set and the corresponding allocations taken place.
   mLinEq            = false;  ///< True -> Linear equations
   mEigEq            = false;  ///< True -> EigenValue equations
   mNonLinEq         = false;  ///< True -> Non-linear equations
   mRestart          = false;  ///< True -> Restart 
   mDiis             = true;   ///< Use Diis extrapol for nonlin as default.
   mPaired           = false;  ///< Use special paired structure options
   mMaxDiis          = 6;      ///< Maximum number of diis vectors in extrapol.
   mEnerPrev         = C_0;    ///< Energy in prev. it for mNonLineq

   mDiagMeth         = "DSYEVD"; 
   ///< Default diagonalization mehod is MIDAS_JACOBI
   //mResidThreshold   = C_I_10_07;      ///< Threshold for norm of residual
   //mEnerThreshold    = C_I_10_07;      ///< Threshold for energy change
   mResidThreshold   = C_NB_MAX;        ///< Threshold for norm of residual
   mEnerThreshold    = C_NB_MAX;        ///< Threshold for energy change
   mMaxStep          = C_1;             ///< Maximum Step size is unit per default  

   mStorageLevel     = aVecStorageLevel;
   mAnalysisDir      = aAnalysisDir;

   mPreDiag          = false;             ///< Default is no prediag and 0 predim space.
   mSecOrd           = false;             ///< Default is not second order  
   mNpreDiag         = I_0;            
   mTimeIt           = false;             ///< Time the time consumed per iteration ?
   mAllowScaleStep   = true;             
   mAllowBackStep    = true;             
   mAllowTrustScale  = true; 

   mTargetSpace      = false;
   mNtargs           = I_0;
   mTargetFilNamBas  = "TargetVector_";
   mNimportantVecs   = I_0;
   mImag   = I_0;
   mTargetingMethod= "BESTHITS";
   mOverlapMin       = C_0;
   mOverlapSumMin       = C_8/C_10;
   mEnergyDiffMax      = C_5/C_AUTKAYS;
   mResidThrForOthers =  C_10_3;
   mEnerThrForOthers =  C_10_3;

   mLevelEq         = I_1;             ///< 1st set of equations to be solved at start. 
   mFileLab         = "ItEqSol"; 
   mVccvsVciPrec    = false;             ///< VCI or VCC  

   mTrustUp       = C_3;
   mTrustDown     = C_I_3;
   mMinTrustStep  = C_I_10_3;

   mOlsen         = true;
   mTrueHDiag     = false;              ///< Default is no true H diag transformer
   mTrueADiag     = false;              ///< Default is no true H diag transformer

   mpSmallTransf = NULL;
   mpSolvecD     = NULL;
   mNeqPrev = I_0;
   mSolVecFilNamBas = "";

   mLevel2Solver        = "DGESV"; ///< Default is NOT symmetry for CC!!!
   mThickRestart=false;         ///< thick restart
   mNresvecs=I_0;                 ///< Number of restart vectors
   mHarmonicRR=false;          ///< Use Harmonic Rayleigh-Ritz procedure to solve EigVal problems
   mEnerShift=C_0;             ///< Energy shift (sigma) used for (H-sigma) in Harmonic RR
}
/**
* Allocate 
* */
void ItEqSol::AllocRedDimMax()     
{
   if (  mEigEq && mNeq > mRedDimMax
      )
   {
      MidasWarning("ItEqSol: Number of equations is larger than maximum size of reduces space. Set mRedDimMax = mNeq = " + std::to_string(mNeq) + ".");
      mRedDimMax = mNeq;
      Mout  << " ItEqSol::AllocRedDimMax(): RESET mRedDimMax = " << mRedDimMax << std::endl;
   }
   // Allocate RedDimMax trial vectors and their transformed
   // At this stage they have zero size so not problem in that.
   mpTrials.reserve(mRedDimMax);
   mpTransTrials.reserve(mRedDimMax);

   string storage_mode = "OnDisc";
   if (mStorageLevel<I_1) storage_mode = "InMem"; 

   midas::mpi::WriteToLog("Storage mode is : '" + storage_mode + "'.");

   DataCont tmp_datacont;
   for (In i=I_0;i<mRedDimMax;i++)
   {
      mpTrials.push_back(tmp_datacont);
      mpTransTrials.push_back(tmp_datacont);
      string trials = mFileLab+"_Trial_" + std::to_string(i);
      mpTrials[i].NewLabel(trials,false);
      trials = mFileLab+"_Transf_Trial_" + std::to_string(i);
      mpTransTrials[i].NewLabel(trials,false);
      mpTrials[i].ChangeStorageTo(storage_mode);
      mpTransTrials[i].ChangeStorageTo(storage_mode);
      //Mout << mpTrials[i].Label() << " storage " << mpTrials[i].Storage() << std::endl;
      //Mout << mpTransTrials[i].Label() << " storage " << mpTransTrials[i].Storage() << std::endl;
 }
}
void ItEqSol::MatchTargetStateComponentsForPreDiag(In& arNpre,vector<In>& arAdd,vector<Nb>& arVals)
{
   std::string targ_components = mAnalysisDir + "/TargetVector_CompAdd";
   ifstream tv_components(targ_components.c_str());
   vector<In> ts_comps;
   In i;
   while (tv_components >> i) ts_comps.push_back(i);
   In n_ts_comp=ts_comps.size();
   Mout << " Find best match of " << n_ts_comp << " components " << endl;
   vector<Nb> ts_vals(n_ts_comp);
   Nb x=C_0;
   for (In i_c=I_0;i_c<n_ts_comp;i_c++) 
   {
      mDiagonal.DataIo(IO_GET,ts_comps[i_c],x);
      ts_vals[i_c]=x;
   }
   for (In i_c=I_0;i_c<n_ts_comp;i_c++) Mout << ts_comps[i_c] << " " << ts_vals[i_c] << endl; 
   mDiagonal.AddressOfExtrema(ts_vals,arAdd,arVals,arNpre,-I_2);
   Mout << " Match is " << arAdd << " " << endl;
}
/**
* Make the start guess 
* */
void ItEqSol::StartGuess
   (  MidasVector& arRedEigVal
   ,  vector<DataCont>& apSolVecs
   ,  vector<DataCont>& apRhs
   )
{
   if (gDebug)
   {
      Mout << " In ItEqSol::StartGuess " << std::endl;
   }
   Mout << "\n Equation type:                     ";
   if (mEigEq)
   {
      Mout << "Linear Eigenvalue equations " << std::endl;
   }
   if (mLinEq)
   {
      Mout << "Linear equations " << std::endl;
   }
   if (mNonLinEq) 
   {
      Mout << "Non-Linear equations " << std::endl;
      Mout << " Second order algorithm is used:    " << StringForBool(mSecOrd) << std::endl;
      Mout << " Diis algorithm is used             " << StringForBool(mDiis) << std::endl;
      if (mDiis)
      {
         Mout << " Maximum Diis vectors               " << mMaxDiis << std::endl;
      }
      if (mSecOrd)
      {
         Mout << " In SecOrd, Max Micro Iter:         " << mNmicroMax << std::endl;   
         Mout << " In SecOrd, Micro Thr Fac:          " << mMicroResFac << std::endl; 
      }
   }
   Mout << " Dimension of actual space:         " << mDim << std::endl;
   if (mPaired)
   {
      Mout << " Use paired structure of Eqs.       " << mPaired << std::endl;
   }
   Mout << " Number of equations to be solved:  " << mNeq << std::endl;
   if (mEigEq && mNeq > mDim) 
   {
      mNeq = mDim;
      Mout << " RESET Number of equations to solve: " << mNeq << std::endl;
   }
   Mout << " Max. Number of iterations :        " << mNiterMax << std::endl;
   Mout << " Max. Dimension of reduced space:   " << mRedDimMax << std::endl;
   Mout << " Break Dimension of reduced space:  " << mNbreakDim << std::endl;
   Mout << " Residual threshold                 " << mResidThreshold << std::endl;
   Mout << " Energy threshold                   " << mEnerThreshold << std::endl;
   if (mEigEq&& mOlsen)
   {
      Mout << " Olsen Algorithm                " << std::endl;
   }
   if (mEigEq&& !mOlsen)
   {
      Mout << " Davidson Algorithm                " << std::endl;
   }
   Mout << " Subspace Diag/LinEq Method         " << mDiagMeth << std::endl;
   Mout << " Restart flag is                    " << StringForBool(mRestart) << std::endl;
   Mout << " Target Space is                    " << StringForBool(mTargetSpace) << std::endl;
   if (mTargetSpace)
   {
      Mout << " The number of target states is     " << mNtargs << std::endl;
   }
   if (mRedDimMax > I_0)
   {
      Mout << " Trial vector storage mode:         " << mpTrials[I_0].Storage() << std::endl;
   }
   if (mPreDiag)
   {
      Mout << " Prediagonalization is             " << " on " << std::endl;
   }
   if (!mPreDiag)
   {
      Mout << " Prediagonalization is             " << " off " << std::endl;
   }
   Mout << " IoLevel:                           " << mIoLevel << std::endl;
   if (mImprovedPrecond)
   {
      Mout << " Improved Preconditioner            " << StringForBool(mImprovedPrecond) << std::endl;
   }
   if (mImprovedPrecond)
   {
      Mout << " Improved Preconditioner, excitation level  " << mPrecondExciLevel << std::endl;
   }
   Mout << " Equation solver at                 " << mLevelEq << "-th level" << std::endl;
   Mout << " True H diagonal:                   " << StringForBool(mTrueHDiag) << std::endl;
   Mout << " True A diagonal:                   " << StringForBool(mTrueADiag) << std::endl;
   //Mout << " Vcc instead of Vci                 " << StringForBool(mVccvsVciPrec) << std::endl;
   if (mNonLinEq && mSecOrd) 
   {
      apSolVecs[I_0].DumpToDisc();  // Write out for safety... 
      Mout << "\n Dump " << apSolVecs[I_0].Label() << " size " << apSolVecs[I_0].Size() << " norm " << apSolVecs[I_0].Norm() << std::endl; 
   }
   if (gDebug && mLinEq) 
   {
      Mout << " Norm of rhs " << std::endl;
      for(In i_eq = I_0; i_eq < mNeq; i_eq++)
      {
         Mout << " i_eq = " << i_eq << " " << apRhs[i_eq].Norm() << std::endl;
      }
      for(In i_eq = I_0; i_eq < mNeq; i_eq++) 
      {
         Mout << " i_eq = " << i_eq << " RHS: \n" << apRhs[i_eq] << std::endl;
      }
   } 

 // Create diagonal of matrix on a DataCont

 bool use_mdiagonal = true;
 if (use_mdiagonal && mLevelEq == I_1 ) // If 2nd, 3rd, ... set of equations do not make diagonal again, old is assumed read manually.
 {
    DataCont vec_of_ones(mDim,C_1,"InMem","vec_of_ones"); 
    string diag_lab = mFileLab + "_diagonal";
    mDiagonal.NewLabel(diag_lab,false);
    string storage_mode = "OnDisc";
    if (mStorageLevel<=I_1) storage_mode = "InMem"; 
    // Diagonal is also in mem
    mDiagonal.ChangeStorageTo(storage_mode);
    Mout << " Diagonal storage mode:             " << 
       mDiagonal.Storage() << endl;
    mDiagonal.SetNewSize(mDim);
    if (    gDebug
       ||   mIoLevel > I_20
       )
    {
      Mout << " Construct diagonal with mEnerPrev = " << this->mEnerPrev << std::endl;
    }
    mpTrf->Transform(vec_of_ones,mDiagonal,I_0,I_1,mEnerPrev);

    // Believe diagonal contains minus the diagonal!
    // Niels: Yes it does... for some reason...
    Nb norm_diag = mDiagonal.Norm();
    if (mIoLevel > I_3 || norm_diag > C_1*mDim) 
       Mout << " Norm of diagonal " << norm_diag << endl;
    if (gDebug || mIoLevel > I_14)
    {
      Mout << " Diagonal " << mDiagonal << std::endl;
    }
    
    // Check the size of the extremem elements of
    // diagonal, such that it can be use as diag^-1
    // NOTE: This is generally a bad idea
    //if (mNonLinEq) check_diag=true;
    if (mLinEq || mNonLinEq || (mEigEq && mPreDiag))
    {
       bool check_diag = true;
       if (check_diag)
       {
         In nmin = min(I_9,mDim);
         if (mPreDiag) nmin = min(mDim,mNpreDiag);
         vector<In> min_addresses;  
         vector<Nb> extrema;  
         Timer time_it;
         mDiagonal.AddressOfExtrema(min_addresses,extrema,nmin,-I_2);
         if (mTimeIt)
         {
            string s_time = " CPU  time used in mDiagonal Extrema serach ";
            time_it.CpuOut(Mout,s_time);
         }
         Mout << " Analysis of diagonal: " << endl;
         Mout << " min abs addresses " << min_addresses << endl; 
         Mout << " min abs values    " << extrema << endl; 
         Mout << " Prediagonal:      " << StringForBool(mPreDiag) << endl << endl; 
         if (mPreDiag)
         {
            Mout << " Set values of diagonal as large number " << endl;
            // Set extremum value to verly large number
            Nb large = C_NB_MAX/C_2;
            if (mEigEq && mTargetSpace)
            {
             // Find the components of the target vectors and find the closest match to them 
             MatchTargetStateComponentsForPreDiag(nmin,mPreDiagAdd,extrema);
            }
            else 
            {
             mPreDiagAdd = min_addresses;
            }
            sort(mPreDiagAdd.begin(),mPreDiagAdd.end()); /// Sort so they come in increasing order
            Mout << " prediag add " << mPreDiagAdd << endl;
            mPreDiagMat.SetNewSize(nmin,false,false);
            mPreDiagMat.Zero();
            for (In i = I_0;i<nmin;i++)
            {
               //Nb x;
               //mDiagonal.DataIo(IO_GET,min_addresses[i],x);
               //mPreDiagMat[i][i] = x;
               mDiagonal.DataIo(IO_PUT,mPreDiagAdd[i],large);
            }
            string storage_mode = "OnDisc";
            if (mStorageLevel<=I_1) storage_mode = "InMem"; 
            //TEST DataCont tmp1(mDim+I_1,C_0,storage_mode,"pd_tmp1"); 
            //TEST DataCont tmp2(mDim+I_1,C_0,storage_mode,"pd_tmp2"); 
            
            In current_dim = mDim;
            DataCont tmp1(current_dim,C_0,storage_mode,"pd_tmp1"); 
            DataCont tmp2(current_dim,C_0,storage_mode,"pd_tmp2"); 
            for (In i = I_0;i<nmin;i++)
            {
               //TEST tmp1.SetToUnitVec(mPreDiagAdd[i]+I_1);
               tmp1.SetToUnitVec(mPreDiagAdd[i]);
               //Mout << " tmp1 " << tmp1 << endl;
               //Mout << " Transform vector " << i << " mPreDiagAdd " << mPreDiagAdd[i] << endl;
               Nb shift = C_0;
               if (mNonLinEq)
                  mpTrf->PreDiagTransform(tmp1,tmp2,I_2,I_1,shift);
               else
                  mpTrf->PreDiagTransform(tmp1,tmp2,I_1,I_1,shift);
               Mout << "Pd_" << left << i << endl;
               //Mout << " Transformation done " << shift << mEnerPrev << endl;
               //Mout << " tmp2 " << tmp2 << endl;
               for (In j = I_0;j<nmin;j++)
               {
                  //TEST tmp2.DataIo(IO_GET,mPreDiagAdd[j]+I_1,mPreDiagMat[j][i]);
                  tmp2.DataIo(IO_GET,mPreDiagAdd[j],mPreDiagMat[j][i]);
               }
               //tmp2.DataIo(IO_GET,mPreDiagAdd[i]+I_1,mPreDiagMat[i][i]);
            }
            Mout << endl;
            mPreDiagMat.Scale(-C_1); // Invert sign
            if (mIoLevel > I_10) Mout << " PreDiag mat \n" << mPreDiagMat << endl;
         }
         //else //if (shift)
         else if (mNonLinEq && !mSecOrd)
         {
            In i_round = I_0;
            In n_round_max = I_5;
            Nb c_hit = C_I_10_12;
            while (fabs(extrema[I_0])< c_hit && i_round++ < n_round_max)
            {
               Nb shift = c_hit- extrema[I_0]; //C_10*extrema[I_0]+C_I_10;
               if (extrema[I_0]<C_0) 
               {
                  shift = -c_hit- extrema[I_0];
               }
               Mout << " Diagonal is shifted with " << shift << endl;
               Mout << " before norm = " << mDiagonal.Norm() << endl;
               Mout << " shift " << shift << endl;
               if (mIoLevel > I_10) Mout << " mDiagonal " << mDiagonal << endl;
               mDiagonal.Shift(shift);
               if (mIoLevel > I_10) Mout << " mDiagonal " << mDiagonal << endl;
               Mout << " Norm after " << mDiagonal.Norm()<<endl;
               mDiagonal.AddressOfExtrema(min_addresses,extrema,nmin,-I_2);
               Mout << " Analysis of NEW diagonal: " << endl;
               Mout << " min abs addresses NEW " << min_addresses << endl; 
               Mout << " min abs values    NEW " << extrema << endl; 
            } 
         } 
       } 
    } 
 }
 if (!mRestart && mEigEq)
 {
    vector<In> min_addresses;  
    vector<Nb> extrema;  
    // The minimum of diagonal is the max of minus diagonal,
    // therefore the last I_1
    In n_eq = mNeq;
    if (mPaired) n_eq = I_2*mNeq;
    n_eq = std::min(n_eq, mDim); // We cannot have more equations than the dimension of the space

    if (!mPaired) 
    { 
       mDiagonal.AddressOfExtrema(min_addresses, extrema, mNeq, I_1);
    }
    if (mPaired)  
    {
       mDiagonal.AddressOfExtrema(min_addresses, extrema, n_eq, -I_2);
    }
    if (mIoLevel > I_5)
    {
       Mout << " min addresses " << min_addresses << endl; 
       Mout << " min values    " << extrema << endl; 
    }
    mNnew = I_0;
    for (In i_eq=0;i_eq<n_eq;i_eq++)
    {
       if (mPaired && min_addresses[i_eq] >= mDim/I_2) continue; // no deexci
       if (mIoLevel > I_11)
       {
         Mout << " try new " << min_addresses[i_eq] << std::endl;
         Mout  << " mpTrials.size() = " << mpTrials.size() << "\n"
               << " mpTrials[mNnew].Size() = " << mpTrials[mNnew].Size() << "\n"
               << std::flush;
       }
       if (mNnew > mRedDimMax) 
         MIDASERROR(" too many new vectors, exceed RedDimMax");
       mpTrials[mNnew].SetNewSize(mDim);
       mpTrials[mNnew].SetToUnitVec(min_addresses[i_eq]);  
       if (mIoLevel > I_11)
       {
         Mout << " set to unit " << std::endl;
       }
       // Put to unit vectors to begin with
       if (Orthogonalize(mNnew,mNnew)) mNnew++;
       if (mIoLevel > I_11)
       {
         Mout << " mNnew = " << mNnew << std::endl;
       }
    }
    In n_to_go = n_eq - mNnew;
    if (mPaired && n_to_go>0)
    {
       In i_eq = I_0;
       while (i_eq<n_eq && mNnew < n_eq)
       {
         if (mPaired && min_addresses[i_eq] < mDim/I_2) 
         {
            i_eq++;
            continue; // no exci
         }
         if (mIoLevel > I_11)
         {
            Mout << " try new " << min_addresses[i_eq] << std::endl;
         }
         if (mNnew > mRedDimMax) 
            MIDASERROR(" too many new vectors, exceed RedDimMax");
         mpTrials[mNnew].SetNewSize(mDim);
         mpTrials[mNnew].SetToUnitVec(min_addresses[i_eq]);  
         if (mIoLevel > I_11)
         {
            Mout << " set to unit " << std::endl;
         }
         // Put to unit vectors to begin with
         if (Orthogonalize(mNnew,mNnew)) mNnew++;
         if (mIoLevel > I_11)
         {
            Mout << " mNnew = " << mNnew << std::endl;
         }
         i_eq++;
       }
    }
    Mout << "\n Created " << mNnew << " orthonormal start vectors " << std::endl;
    mRedDim = mNnew;

 }
 // For LinEq. the rhs is assumed to be set.
 // For NonLinEq. we generate the perturbation estimate H_o-1 e  
 else if (!mRestart && (mLinEq || mNonLinEq) )
 {
    mNnew = I_0;
    for (In i_eq=0;i_eq<mNeq;i_eq++) 
    {
       if (mNnew > mRedDimMax) 
       {
         MIDASERROR("Too many new vector. exceed mRedDimMax ");
       }
       if (use_mdiagonal)
       {

         if (mLinEq) 
         {
            mpTrials[mNnew] = apRhs[i_eq];
         }
         if (mNonLinEq) 
         {
            string storage_mode = "OnDisc";
            if (mStorageLevel<=I_1) storage_mode = "InMem"; 
            DataCont tmp(mDim,C_0,storage_mode,"vcc_start_0"); 
            mpTrials[mNnew].SetNewSize(mDim,false);
            mpTrials[mNnew].Zero();
            mpTrials[mNnew].DumpToDisc();  // For safety in prediag in memory.
            mpTrf->Transform(tmp, mpTrials[mNnew],I_1,I_1,mEnerPrev);
            Mout << " Energy in first trans = " << mEnerPrev << " (reference energy) \n" 
                 << " Norm of transformed vector " << mpTrials[mNnew].Norm() << std::endl;
         }
         if (mNonLinEq) InverseMatTimesVec(mpTrials[mNnew],C_0);
         if (mLinEq) InverseMatTimesVec(mpTrials[mNnew],arRedEigVal[i_eq]);
         Nb norms = mpTrials[mNnew].Norm();
         Mout << " Start guess norm: " << norms << endl;
       }
       else
       {
         Nb zero=C_0;
         if (mLinEq)
         {
            mpTrf->Transform(apRhs[i_eq],mpTrials[mNnew],I_0,I_1,zero);  
         }
         if (mNonLinEq) 
         {
            MIDASERROR(" NonLinEq has to be solved with diagonal present!");
         }
       }
       if (mLinEq)
       {
         bool let_small_vectors_pass = false;
         if (mNnew == I_0) let_small_vectors_pass = true; // Let first pass to be sure to get one!
         if (Orthogonalize(mNnew,mNnew,let_small_vectors_pass)) mNnew++;
         // if (Orthogonalize(mNnew,mNnew)) mNnew++;
       }
       else if (mNonLinEq) mNnew++;
    }
    mRedDim  = mNnew;
 }
 else // Restart calculation
 {
    Mout << " A restart calculation   " << std::endl;
    
    mNnew = I_0;
    for (In i_eq = I_0; i_eq < std::max(mNeq, mNresvecs); i_eq++)
    {
       if (mNnew > mRedDimMax)
       {
          MIDASERROR("Too many new vector. exceed mRedDimMax ");
       }

       mpTrials[mNnew].SetNewSize(mDim, false);
       mpTrials[mNnew] = apSolVecs[i_eq];
       // Restart vector turns out to be zero. Use guess from inverse diagonal
       if (mEigEq && mpTrials[mNnew].Norm() < C_NB_EPSMIN19)
       {
          if (i_eq < mNeq)
          {
             std::vector<In> min_addresses;  
             std::vector<Nb> extrema;  
             
             // The minimum of diagonal is the max of minus diagonal, therefore the last I_1
             if (!mPaired)
             {
               mDiagonal.AddressOfExtrema(min_addresses, extrema, mNeq, I_1);
             }
             if (mPaired)
             {
               mDiagonal.AddressOfExtrema(min_addresses, extrema, I_2*mNeq, -I_2);
             }

             if (mIoLevel > I_2)
             {
                Mout << " min addresses " << min_addresses << std::endl; 
                Mout << " min values    " << extrema << std::endl; 
             }

             mpTrials[mNnew].SetNewSize(mDim);
             mpTrials[mNnew].SetToUnitVec(min_addresses[i_eq]);  
          }
          else
          {
             continue;
          }
       }

       // Check orthogonal
       if (mLinEq || mEigEq)
       {
          if (Orthogonalize(mNnew, mNnew, mLinEq)) 
          {
             mNnew++;
          }
          else
          {
             Mout << " A restart vector was delete - rescue action " << std::endl;
             std::vector<In> min_addresses;  
             std::vector<Nb> extrema;  

             // The minimum of diagonal is the max of minus diagonal, therefore the last I_1
             mDiagonal.AddressOfExtrema(min_addresses, extrema, mNeq, I_1);
             if (mIoLevel > I_2)
             {
                Mout << " min addresses " << min_addresses << std::endl; 
                Mout << " min values    " << extrema << std::endl; 
             }
             mpTrials[mNnew].SetNewSize(mDim);
             mpTrials[mNnew].SetToUnitVec(min_addresses[i_eq]);  
             if (Orthogonalize(mNnew, mNnew)) 
             {
                mNnew++;
             }
          }
       }

       if (mNonLinEq)
       {
         mNnew++;
       }
       
       if (mIoLevel > I_5)
       {
         Mout << " Start vector: " << i_eq << " norm " << mpTrials[mNnew - I_1].Norm() << std::endl;
       }
    }
       
    if (mIoLevel > I_2)
    {
      Mout << "\n Restart with " << mNnew << " start vectors " << std::endl;
    }

    mRedDim  = mNnew;
 }
   
 if (mNnew == I_0)
 {
    MIDASERROR(" In ItEqSol no start vectors was created, STOP ?!?!? " );
 }

 if (gDebug)
 {
    Mout << " End of ItEqSol::StartGuess " << std::endl;
    for (In i_eq = I_0; i_eq < mNnew; i_eq++)
    {
       Mout << " start vec " << i_eq  << " is \n" << mpTrials[i_eq] << std::endl;
    }
 }

 // DEBUG OUTPUT:
 //Mout << " MADE STARTGUESS: " << std::endl;
 //for(In i=0; i<mpTrials.size(); ++i)
 //   Mout << " Trial " << i << " : " << mpTrials[i] << std::endl;
}

/**
 * Solve the equations
**/
bool ItEqSol::Solve
   (  MidasVector& arRedEigVal
   ,  vector<DataCont>& apSolVec
   )     
{
   vector<DataCont> dummy_vector;
   if ((*this).Solve(arRedEigVal, apSolVec, dummy_vector))
   {
      return true;
   }
   return false;
}
/**
* Solve the equations
* */
bool ItEqSol::Solve
   (  MidasVector& arRedEigVal
   ,  vector<DataCont>& apSolVec
   ,  vector<DataCont>& apRhs
   )     
{
   //Mout << " Starting to solve equations " << std::endl;
   if (gDebug || mIoLevel > I_10)
   {
      Mout << " In ItEqSol::Solve " << std::endl;
   }
   if (mNonLinEq && mNeq > I_1)
   {
      MIDASERROR("NonLinEq only implemented for 1 at a time ");
   }

   // Reset variables to start values 
   mRedDim           = I_0;          ///< Current dimension of reduced space
   mNnew             = I_0;
   mNiter            = I_0;
   mRedHam.SetNewSize(mRedDim,false);
   if (mHarmonicRR)  mRedMetric.SetNewSize(mRedDim,false);
   mRedSolVec.SetNewSize(mRedDim,false);
   mRedRhs.SetNewSize(mRedDim,false);
   mTarTriS.SetNewSize(mNtargs,mRedDim,false,C_0);
   vector<Nb> tar_svec;
   tar_svec.reserve(mRedDimMax);
   // mTarSolS.SetNewSize(mNtargs,mRedDim,false,C_0);
   bool diis_save = mDiis;
   bool break_now = false;
   //Nb c_norm_one_thr  = C_I_10_8;
   Nb c_norm_max_step_begin = mMaxStep; 
   Nb c_norm_max_step       = C_1; 
   Nb c_thr_close_zero      = sqrt(C_NB_EPSMIN19); 

   if (!mRedDimMaxSet) AllocRedDimMax(); // Allocate the trial and transf. vectors.
   
   bool converged=false;
   bool a_break_has_occurred = false;
   In n_trans = I_0;
   Nb e_curr = C_0;
   Nb diis_c = C_1;
   Nb tol_red_lineq = C_NB_EPSMIN19*C_10_2;
   vector<Nb> eig_val_old;
   vector<Nb> eig_val_diff;
   eig_val_old .reserve(mRedDimMax);
   eig_val_diff.reserve(mRedDimMax);
   if (mEigEq)
   {
      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
      {
         tar_svec.push_back(C_0);
         eig_val_old.push_back(C_0);
         eig_val_diff.push_back(C_0);
      }
   }
   
   // Allocate space for residual vecs (the actual vecs are on disc)
   string storage_mode = "OnDisc";
   if (mStorageLevel<=I_1) storage_mode = "InMem"; 
   
   if (mImprovedPrecond)
   {
      VccTransformer* vcc_trf = dynamic_cast<VccTransformer*>(mpTrf);
      if (!vcc_trf)
      {
         Mout << " Not a vcc transformer." << endl;
      }
      else // Excitation space set up
      {
         //bool is_it_vcc_rsp=false;
         //if (vcc_trf->NexciTransXvec()>mDim && vcc_trf->GetType()==32) is_it_vcc_rsp=true;
         mpSmallTransf = new VccTransformer(*vcc_trf, mPrecondExciLevel);

         Mout << " Dimensionality of result vector (mTransXvec): " << mpSmallTransf->NexciTransXvec() << std::endl
              << " Dimensionality of input vector (mXvec):       " << mpSmallTransf->NexciXvec() << std::endl;

         string solveco_storage_mode = "OnDisc";
         if (mStorageLevel==I_0) solveco_storage_mode = "InMem"; 

         mpSolvecD = new DataCont [I_1]; 
         mpSolvecO.reserve(mRedDimMax); 
         string slabel = "IMPREC_sol_vec_0_"; 
         mpSolvecD[I_0].NewLabel(slabel);
         mpSolvecD[I_0].ChangeStorageTo(storage_mode); 
         In nvecsize = mpSmallTransf->NexciTransXvec();
         if (vcc_trf->NexciTransXvec()>mDim) nvecsize--;
         mpSolvecD[I_0].SetNewSize(nvecsize,false);

         DataCont tmp_datacont;
         for (In i_eq=I_0;i_eq<mNeq;i_eq++)
         {
            mpSolvecO.push_back(tmp_datacont);
            string s = "IMPREC_sol_vec_1_"+std::to_string(i_eq);
            mpSolvecO[i_eq].NewLabel(s);
            mpSolvecO[i_eq].ChangeStorageTo(solveco_storage_mode);
            mpSolvecO[i_eq].SetNewSize(nvecsize,false);
            mpSolvecO[i_eq].Zero();
         }
      }
   }

   // Create start guess
   StartGuess(arRedEigVal, apSolVec, apRhs);
   if (mNresvecs > mNeq)
   {
      for (In i = mNeq; i < mNresvecs; i++)
      {
         apSolVec.pop_back();
      }
   }

   //// Allocate space for residual vecs (the actual vecs are on disc)
   //string storage_mode = "OnDisc";
   //if (mStorageLevel<=I_1) storage_mode = "InMem"; 

   //if (mImprovedPrecond)
   //{
   //   VccTransformer* vcc_trf = dynamic_cast<VccTransformer*>(mpTrf);
   //   if (!vcc_trf)
   //   {
   //      Mout << " Not a vcc transformer." << endl;
   //   }
   //   else // Excitation space set up
   //   {
   //      //bool is_it_vcc_rsp=false;
   //      //if (vcc_trf->NexciTransXvec()>mDim && vcc_trf->GetType()==32) is_it_vcc_rsp=true;
   //      mpSmallTransf = new VccTransformer(*vcc_trf, mPrecondExciLevel);

   //      Mout << " Dimensionality of result vector (mTransXvec): " << mpSmallTransf->NexciTransXvec() << std::endl
   //           << " Dimensionality of input vector (mXvec):       " << mpSmallTransf->NexciXvec() << std::endl;

   //      string solveco_storage_mode = "OnDisc";
   //      if (mStorageLevel==I_0) solveco_storage_mode = "InMem"; 

   //      mpSolvecD = new DataCont [I_1]; 
   //      mpSolvecO.reserve(mRedDimMax); 
   //      string slabel = "IMPREC_sol_vec_0_"; 
   //      mpSolvecD[I_0].NewLabel(slabel);
   //      mpSolvecD[I_0].ChangeStorageTo(storage_mode); 
   //      In nvecsize = mpSmallTransf->NexciTransXvec();
   //      if (vcc_trf->NexciTransXvec()>mDim) nvecsize--;
   //      mpSolvecD[I_0].SetNewSize(nvecsize,false);

   //      DataCont tmp_datacont;
   //      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
   //      {
   //         mpSolvecO.push_back(tmp_datacont);
   //         string s = "IMPREC_sol_vec_1_"+std::to_string(i_eq);
   //         mpSolvecO[i_eq].NewLabel(s);
   //         mpSolvecO[i_eq].ChangeStorageTo(solveco_storage_mode);
   //         mpSolvecO[i_eq].SetNewSize(nvecsize,false);
   //         mpSolvecO[i_eq].Zero();
   //      }
   //   }
   //}

   if (mLinEq || mEigEq) //Also if mGenEigEq
   {
      mpResidualVecs.reserve(mRedDimMax);
      DataCont tmp_datacont;
      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
      {
         mpResidualVecs.push_back(tmp_datacont);
         string s = mFileLab+"_Resid_"+std::to_string(i_eq);
         mpResidualVecs[i_eq].NewLabel(s);
         mpResidualVecs[i_eq].ChangeStorageTo(storage_mode);
         mpResidualVecs[i_eq].SetNewSize(mDim,false);
         //string s2 = mFileLab+"_SolVec_"+std::to_string(i_eq);
      }
      mResidNorms.SetNewSize(mNeq,false);
   }

   Nb normprev     = C_NB_MAX;
   Nb e_diff       = C_NB_MAX;
   //Nb e_diff_prev  = C_NB_MAX;
   Nb norm_trans_t = C_0;
   bool error_inc_break = false;
   bool backstep        = false;
   In i_backstep_ref    = I_0;
   In n_backstep_max    = I_5;
   Nb   c_backstep_min  = C_I_10;
   Nb   c_backstep_max  = C_I_2;
   Nb   c_trust         = C_1;

   DataCont save_old_try;
   if (mNonLinEq && !mDiis) 
   {
      string stry = mFileLab+"_Save_Old_Trial";
      save_old_try.NewLabel(stry);
      save_old_try.ChangeStorageTo(storage_mode);
      save_old_try.SetNewSize(mDim,false);
   }

   //***********************************************//
   // main loop to drive iterative equation solver  //
   //                                               //
   // Loop until all converged                      //
   //                                               //
   //***********************************************//
   Timer time_it;
   while (!converged && mNnew > I_0)
   {
      if (mTimeIt && mNiter > I_0) 
      {
         string s_time = " CPU time used in eq.sol. iteration " + std::to_string(mNiter) + ":";
         time_it.CpuOut(Mout,s_time);
         //time_it.WallOut(Mout," Wall time used in eq.solver this iteration: ");
      }
      if (mNonLinEq)
      {
         converged = true; 
      }
      // Accumulate iterations
      mNiter++;
      if (mIoLevel > I_11)
      {
         Mout << "\n\n ";
         if (mLevelEq > I_1)
         {
            Mout << "MICRO (Eq level: " << mLevelEq << ") ";
         }
         if (mLevelEq == I_1)
         {
            Mout << "MACRO (Eq level: " << mLevelEq << ") ";
         }
         Mout << "Iteration " << mNiter << " Reduced space dim: " << mRedDim;
         Mout << " # new trial vec: " << mNnew << std::endl;
      }

      /*********************************************
       * Carry out transformations of new vectors  *
       *********************************************/
      for (In i_new = mRedDim - mNnew; i_new < mRedDim; i_new++)
      {
         if (gDebug)
         {
            Mout << " Transforming: \n " << mpTrials[i_new].Label() << " to "
                 << mpTransTrials[i_new].Label() << std::endl;
         }
         if (mpTransTrials[i_new].Size() != mDim)
         {
           mpTransTrials[i_new].SetNewSize(mDim,false);
         }
         In i_vci = I_1;
         if (mLevelEq > I_1)
         {
            if (mVccvsVciPrec)
            {
               i_vci = I_2;
            }
         }
         mpTrf->Transform(mpTrials[i_new], mpTransTrials[i_new], i_vci, I_0, e_curr);  

         if (mHarmonicRR)
         {
            mpTransTrials[i_new].Axpy(mpTrials[i_new],-mEnerShift);
         }
         n_trans++;

         // for non-linear eq.
         // vectors come like:  trial, new-trial 
         if (mNonLinEq) 
         {
           // Check convergence 
           if (backstep) Mout << " This is a back-step, back-step nr.  " << i_backstep_ref << endl;
           Nb norm_trial   = mpTrials[i_new].Norm(); 
           norm_trans_t = mpTransTrials[i_new].Norm(); 
           Mout << " Iter: " << mNiter <<  " Total energy :            " 
              << setw(35) << e_curr << endl;
           Mout << " Iter: " << mNiter <<  " Norm of trial vec         " 
              << setw(35) << norm_trial   <<endl;
           // Mout << " Iter: " << mNiter <<  
           //    " Norm of trans trial       " 
           //    << setw(35) << norm_trans_t <<endl;
           Mout.precision(16);
           e_diff = e_curr-mEnerPrev;
           Mout << " Iter: " << mNiter <<  " Energy-change:            " 
              << setw(35) << e_diff << endl;
           Mout << " Iter: " << mNiter <<  " ||err-vec||:              " 
              << setw(35) << norm_trans_t << endl;
           if (mNiter>I_1) 
           {
              Mout << " Iter: " << mNiter<<" ||err-vec||/previous:     " 
                <<  setw(35) << (norm_trans_t/normprev) << endl;
           }
           if (norm_trial>C_NB_EPSMIN19)
           {
              Mout << " Iter: " << mNiter <<  " ||err-vec||/||vec||:      " 
            << setw(35) << norm_trans_t/norm_trial << endl;
           }
           if (mAllowTrustScale) Mout << " Iter: " << mNiter <<  " Step control factor       " 
            << setw(35) << c_trust << endl;
           if (norm_trans_t > mResidThreshold || fabs(e_diff) > mEnerThreshold) converged = false;
           if (mNiter>I_2 && !converged && e_diff > C_0) Mout << " NB: Energy increasing! " << endl;
           if (converged) mDiis = false;

           // Save the current bet under all circumstances.
           if (!mDiis) apSolVec[I_0] = mpTrials[mRedDim+mNnew-I_2];

           // Check if we are improving ...
           if (!converged && normprev < norm_trans_t
               && i_backstep_ref < n_backstep_max)
           {
              Mout << " normprev     " << normprev << endl; 
              Mout << " norm_trans_t " << norm_trans_t << endl; 
              Mout << " NB: Error vector is increasing in non-linear equations" << endl;
              Nb c_fact_2 = C_10_3;
              if (e_diff > C_0 && normprev*c_fact_2 < norm_trans_t)
              {
                 Mout << " The energy is also increasing " << endl;
                 Mout << " The increase in error norm is larger than a factor " << c_fact_2 << endl;
                 Mout << " Currently I am implemented to exit now "
                    << " - do you want to implement a better solution? "   << endl;
                 error_inc_break = true;
                 break;
              }
              else if (mAllowBackStep && normprev < norm_trans_t )
              {
                 Mout << " Back step a bit " << endl;
                 backstep = true;
                 i_backstep_ref++;
              }
              c_trust = max(mMinTrustStep,c_trust*mTrustDown); // decrease down to mMinTrustStep 
           }
           else
           {
              if (mNiter>I_1 && normprev > norm_trans_t*C_5) 
                 c_trust = min(C_1,c_trust*mTrustUp); // increase trust factor rapidly
              else if (normprev > norm_trans_t*(C_1+C_I_10_2)) // At least 1 % decrease
                 c_trust = min(C_1,c_trust*(C_1+mTrustUp/C_4)); // increase trust factor slowly
              normprev = norm_trans_t;
              mEnerPrev  = e_curr;
              //e_diff_prev = e_diff;
              backstep = false;
              i_backstep_ref = I_0;
              apSolVec[I_0].DumpToDisc(); 
           }

           // Create NR new step vector unless we need to back step 
           Nb norm_delta_t = C_0;
           if (!converged && !backstep)
           {
              InverseMatTimesVec(mpTransTrials[i_new],C_0);
              if (mAllowTrustScale && fabs(c_trust-C_1)>C_NB_EPSMIN19*C_10) 
              {
                 Mout << " Scale delta vector by " << c_trust << endl;
                 mpTransTrials[i_new].Scale(c_trust); 
              }
              norm_delta_t = mpTransTrials[i_new].Norm(); 
              Mout << " Iter: " << mNiter <<  " Norm of delta t           " 
                   << setw(35) << norm_delta_t <<endl;
              // Check size of new increment vector. 
              //if (mAllowScaleStep && norm_delta_t > norm_trial && norm_trial > C_NB_EPSMIN19)
              c_norm_max_step = min(norm_trial,c_norm_max_step_begin);
              if (mAllowScaleStep && norm_delta_t > c_norm_max_step && c_norm_max_step > c_thr_close_zero)
              {
                 Mout << " NBNBNB Norm of correction to vector is larger than max allowed " << c_norm_max_step << endl;
                 Mout << " I will rescale the correction vector ";
                 Nb scale = (c_norm_max_step/norm_delta_t)*mTrustDown;
                 mpTransTrials[i_new].Scale(scale);
                 c_trust = max(mMinTrustStep,c_trust*mTrustDown); // decrease down to mMinTrustStep 
              }
           }
           
           // If not mDiis we simply update here - make the (quasi) NR new trial vector.
           // Put after the old new. 
           if (!mDiis && !converged && !backstep)
           {
              save_old_try = mpTrials[i_new];
              In i_trial = mRedDim-mNnew;
              //Mout << " i_trial " << i_trial << endl;
              //Mout << " mRedDim " << mRedDim << endl;
              //Mout << " mNnew   " << mNnew   << endl;
              //Mout << " i-new   " << i_new   << endl;
              if (i_trial > mRedDimMax-I_1) break; // Next vector except if space full
              if (mpTransTrials[i_trial].Size() != mDim) mpTrials[i_trial].SetNewSize(mDim,false); 
              mpTrials[i_trial] = mpTransTrials[i_new];
              mpTrials[i_trial].Axpy(save_old_try,C_1);
              continue;
           }
           if (!mDiis && !converged && backstep)
           {
              Nb g0 = C_I_2*normprev;
              Nb g1 = C_I_2*norm_trans_t;
              Nb gprime0 = -norm_trans_t;
              Nb lambda = - gprime0/(C_2*(g1-g0-gprime0));
              Mout << " lambda calculated = " << lambda << endl;
              lambda = min(lambda,c_backstep_max);
              lambda = max(lambda,c_backstep_min);
              Mout << " Create back step with backstep lambda " << lambda << endl;
              mpTrials[i_new].Axpy(save_old_try,-C_1);
              mpTrials[i_new].Scale(lambda);
              Mout << " Lambda scaled delta vec; " << mpTrials[i_new].Norm() << endl;
              mpTrials[i_new].Axpy(save_old_try,C_1);
           }
           if (!mDiis && !backstep) continue;
         }
         // end if on line ~779 (~130+ lines)
      }
      /****************************************************
       * END for loop for transformations of new vectors  *
       ****************************************************/
      
      /**
       * BREAK LOOP
       *
       * if we have exceeded max dimension for reduced matrix,
       * or we have reached max iterations
       **/
      if (mRedDim > mRedDimMax || mNiter >= mNiterMax) break; // break main while

      // If nonlinear and not diis we dont need the rest:
      /********************************************
       * NB NB NB !!! continue while !!! NB NB NB *
       ********************************************/
      if (mNonLinEq && !mDiis) continue; // continue while !

      // Extend the reduced matrix for LinEq,EigEq
      In red_dim = mRedDim;
      if (mNonLinEq ) red_dim = min(mRedDim+I_1,mMaxDiis+I_1);

      bool use_old_part = true;
      if (a_break_has_occurred) use_old_part = false;
      mRedHam.SetNewSize(red_dim,use_old_part);
      if (mHarmonicRR) mRedMetric.SetNewSize(red_dim,use_old_part);
      if (mThickRestart && a_break_has_occurred)
      {
         mNnew=mRedDim;
      }
      
      // if using target space update overlap between targets and trials
      if (mTargetSpace)
      {
         mTarTriS.SetNewSize(mNtargs,red_dim,use_old_part);

         for (In i_new1=mRedDim-mNnew;i_new1<mRedDim;i_new1++)
         {
            for (In i_t=I_0;i_t<mNtargs;i_t++)
            {
               DataCont target;
               string name = mTargetFilNamBas + std::to_string(i_t);
               target.GetFromExistingOnDisc(mDim,name);
               target.SaveUponDecon();
               mTarTriS[i_t][i_new1] = Dot(target,mpTrials[i_new1]);
            }
         }
      }
      
      /********************************
       * Update reduced matrices
       ********************************/
      if ( mEigEq || mLinEq ) // if eigenvalue or linear equation
      {
         if (gDebug)
         {
           Mout << " Vectors and Transformed vectors: \n " << std::endl;
           for (In i_new1 = I_0; i_new1 < mRedDim; i_new1++)
           {
              Mout << "Vec nr " << i_new1 << "  \n" <<  mpTrials[i_new1] << std::endl;
              Mout << "T-vec  " << i_new1 << "  \n" <<  mpTransTrials[i_new1] << std::endl;
           }
         }
         if (!mHarmonicRR)
         {  
            ConstructReducedMat(mpTrials,mpTransTrials,mRedHam);
         }
         else
         {
            ConstructReducedMat(mpTransTrials,mpTrials,mRedHam);
            ConstructReducedMat(mpTransTrials,mpTransTrials,mRedMetric);
         }
      }

      if ( mNonLinEq ) // if non linear equation
      {
         if (mRedDim < mMaxDiis)
         {
            ConstructReducedMat(mpTransTrials,mpTransTrials,mRedHam);
         }
         else
         {
            In i_off = mRedDim-red_dim+I_1;
            for (In i_new1=I_0;i_new1<red_dim-I_1;i_new1++)
            {
               for (In i_new2=i_new1;i_new2<red_dim-I_1;i_new2++)
               {
                  In i1 = i_off+i_new1;
                  In i2 = i_off+i_new2;
                  mRedHam[i_new1][i_new2]=
                     Dot(mpTransTrials[i1],mpTransTrials[i2]);
                  if (i_new2!=i_new1) 
                     mRedHam[i_new2][i_new1] = mRedHam[i_new1][i_new2]; 
               }
            }
         }
           
         for (In i_new1=I_0;i_new1<red_dim;i_new1++) 
            mRedHam[i_new1][red_dim-I_1]=-diis_c;
         for (In i_new1=I_0;i_new1<red_dim;i_new1++) 
            mRedHam[red_dim-I_1][i_new1]=-diis_c;
         mRedHam[red_dim-I_1][red_dim-I_1]=C_0;

         Nb norm2redham = mRedHam.Norm2();
         norm2redham -= C_2*diis_c*(red_dim-I_1); // 2 times the -1 vector
         Nb normtest = fabs(norm2redham); //sqrt(fabs(norm2redham));
         normtest = normtest/(red_dim*red_dim);
         if (normtest < sqrt(C_NB_EPSMIN19))
         {
            Mout << " giving up Diis for numerical reasons " << endl;
            Mout << " normtest    " << normtest    << endl;
            Mout << " eps         " << C_NB_EPSMIN19 << endl;
            Mout << " sqrt(eps)   " << sqrt(C_NB_EPSMIN19) << endl;
            mDiis = false;
         }
         tol_red_lineq = C_NB_EPSMIN19*C_10/normtest; 
         Mout << " tol_red_lineq = " << tol_red_lineq << endl;
         // Set the norm for solving linear equations in accord with normtest
      }
      if (gDebug)
      {
         Mout << " Reduced matrix is: \n " << mRedHam << std::endl;
         if (mHarmonicRR)
         {
            Mout << " Reduced metric matrix is: \n " << mRedMetric << std::endl;
         }
      }
      /********************************
       * Update reduced matrices END
       ********************************/
      
      /***********************************
       * Solve the reduced space problem *
       ***********************************/
      // if: solve reduced eigenvalue problem
      if (mEigEq)
      {
         mRedSolVec.SetNewSize(mRedDim, false, false);    
         // Must be generalized to be non square for lin eq. <- wtf? this is eig val equation ?...
         arRedEigVal.SetNewSize(mRedDim, false);
         MidasMatrix tmpham(mRedHam);
         MidasMatrix tmpmet(mRedMetric);
         Timer time_la;

         // do reduaced solutions
         Diag(tmpham, mRedSolVec, arRedEigVal, mDiagMeth, mReImEigVal, tmpmet, true, mPaired);
         
         if (mTimeIt)
         {
            string s_time = " Wall time used in Diag ";
            time_la.WallOut(Mout, s_time);
         }
         
         if (mReImEigVal.size() > I_0)
         {
            Mout << " Complex eigen values Re Im "<< mReImEigVal << std::endl;
         }
         
         // if: harmonic rr
         if (mHarmonicRR || mEnerShift != C_0)
         {
            vector<MidasVector> eig_vec_re_im;
            eig_vec_re_im.reserve(mReImEigVal.size());

            In i=I_0;
            while (i<mRedDim-I_1)
            {
               if ( libmda::numeric::float_eq(arRedEigVal[i],arRedEigVal[i+I_1]) )
               {
                  MidasVector tmp_mv(mRedSolVec.Nrows());
                  mRedSolVec.GetCol(tmp_mv,i);
                  eig_vec_re_im.push_back(tmp_mv);
                  Mout << " Complex conjugate eig.vec #" <<  i << " is saved" << endl;
                  mRedSolVec.GetCol(tmp_mv,i+I_1);
                  eig_vec_re_im.push_back(tmp_mv);
                  Mout << " Complex conjugate eig.vec #" <<  i+I_1 << " is saved" << endl;
                  i+=I_1;
               }
               i+=I_1;
            }


            In n = arRedEigVal.Size();
            // Eigenvalues are sorted according to the distance from the shift
            // Shell sort,stroustrup
            for (In gap=n/2;0<gap;gap/=2)
               for (In i=gap;i<n;i++)
                  for (In j=i-gap;0<=j;j-=gap)
                  {
                     bool less;
                     if (mHarmonicRR)
                        less = fabs(C_1/arRedEigVal[j+gap]) < fabs(C_1/arRedEigVal[j]); 
                     else
                        less = fabs(arRedEigVal[j+gap]-mEnerShift) < fabs(arRedEigVal[j]-mEnerShift); 
                     if (less)
                     {
                        Nb tmp = arRedEigVal[j];
                        arRedEigVal[j] = arRedEigVal[j+gap];
                        arRedEigVal[j+gap] = tmp;
                        mRedSolVec.SwapCols(j,j+gap);
                     }
                  }
            if (mReImEigVal.size()>I_0)
            {
               In i=I_0;
               while (i<mReImEigVal.size())
               {
                  In j=I_0;
                  while (arRedEigVal[j]!=mReImEigVal[i]) j++;
                  //Mout << " j=  " << j << endl;
                  mRedSolVec.AssignCol(eig_vec_re_im[i],j);
                  mRedSolVec.AssignCol(eig_vec_re_im[i+I_1],j+I_1);
                  i+=I_2;
               }
            }
         }
         // end if: harmonic rr

         //Diag(MidasMatrix& aM, MidasMatrix& aEigVec, MidasVector& aEigVal, 
            //string aHow, bool aStoreEigVec=true);
          
         if (gDebug || mIoLevel > I_10)
         {
            Mout << " Eigen values in the reduced space of dim " << mRedDim  
                 << " are \n" << arRedEigVal << std::endl;
         }
         if (gDebug || mIoLevel > I_12)
         {
            Mout << " Eigen vector matrix \n " << mRedSolVec << std::endl;
         }
         //for (In i_eq=I_0;i_eq<mNeq;i_eq++)
         //{
            //eig_val_diff[i_eq] = arRedEigVal[i_eq]-eig_val_old[i_eq];
            //eig_val_old[i_eq] = arRedEigVal[i_eq];
         //}
         //if (gDebug || mIoLevel > 5)
            //Mout << " Difference in eigen values compared to previous iteration " 
            //<< " are \n" << eig_val_diff << endl;
      }
      // end if: solve reduced eigenvalue problem (started line ~1061)
      
      // if: solve reduced linear equations/non linear equations using diis
      if ( mLinEq || (mNonLinEq && mDiis) )
      {
         // Construct the reduced space Rhs. 
         //mRedRhs.SetNewSize(red_dim,mNeq,true,false);
         mRedRhs.SetNewSize(red_dim,mNeq,use_old_part,false);
         for (In i_eq=I_0;i_eq<mNeq;i_eq++)
         {
            if (mLinEq)
            {
               for (In i_new1=mRedDim-mNnew;i_new1<mRedDim;i_new1++)
                  mRedRhs[i_new1][i_eq] = Dot(mpTrials[i_new1],apRhs[i_eq]);
            }
            else
            {
               for (In i1=I_0;i1<red_dim;i1++) mRedRhs[i1][i_eq] = C_0;
               mRedRhs[red_dim-I_1][i_eq] = -diis_c;
            }
         }
         if (gDebug || mIoLevel > 10) Mout << " mRedRhs \n" << endl << mRedRhs << endl;

         // Solve the reduced space lineq 
         //mRedSolVec.SetNewSize(red_dim,mNeq,true,true);    
         mRedSolVec.SetNewSize(red_dim,mNeq,use_old_part,use_old_part);    
         bool gen_with_freq = mLinEq;
         if (gen_with_freq)
         {
            for (In i_eq=I_0;i_eq<mNeq;i_eq++)
            {
               MidasMatrix rhs_mat(red_dim,I_1);
               for (In i=I_0;i<red_dim;i++) 
               {
                   rhs_mat[i][I_0] = mRedRhs[i][i_eq];
               }
               MidasMatrix sol_mat(rhs_mat);
               //if (mNonLinEq) 
               //{
                  //sol = rhs; // start as rhs
               //}
               if (!mNonLinEq)
               {
                  for (In i=I_0;i<red_dim;i++) 
                      sol_mat[i][I_0] = mRedSolVec[i][i_eq];
                  //mRedSolVec.GetCol(sol,i_eq); // start as old sol.
               }
               MidasVector resids(mNeq);
               In iter=I_0;
               MidasMatrix red_ham(mRedHam);
               for (In i=I_0;i<red_ham.Ncols();i++)
               {
                   red_ham[i][i]-= arRedEigVal[i_eq];
               }
               if (gDebug || mIoLevel>I_10) 
                 Mout << " red_ham (incl freq) \n" << red_ham << endl;
               if (gDebug || mIoLevel>I_10) Mout << " SolMethd= " << mDiagMeth << endl;

               // Temporarily, I lowered the accuracy to solve the reduced size LinEq 
               //tol_red_lineq = C_NB_EPSMIN19*C_10_4; 
               //Mout << " tol_red_lineq " << tol_red_lineq << endl; 

               Timer time_la;
               In n_max_iter=I_200;
               LinEqSol(red_ham,sol_mat,rhs_mat,I_1,mDiagMeth,resids,iter,tol_red_lineq,n_max_iter);
               if (mTimeIt)
               {
                  string s_time = " CPU time used in LinEqSol ";
                  time_la.CpuOut(Mout,s_time);
               }
               //mRedSolVec.AssignCol(sol,i_eq);    
               for (In i=I_0;i<red_dim;i++) 
                   mRedSolVec[i][i_eq] = sol_mat[i][I_0]; 
               //LinEqSol(mRedHam,mRedSolVec,mRedRhs,mNeq,mDiagMeth,resids,
                        //iter,tol_red_lineq,I_200);
               if (gDebug || mIoLevel > I_5) 
                    Mout << " Sol of reduced eq: " << " resids: " << resids 
                     << " in (max among eq) less than " << iter << " iterations " << endl;
            }
            if (gDebug||mIoLevel>I_20) 
               Mout << " mRedSol \n" << endl << mRedSolVec << endl;
         }
         else
         {
            for (In i_eq=I_0;i_eq<mNeq;i_eq++)
            {
               MidasVector rhs(red_dim);
               mRedRhs.GetCol(rhs,i_eq);
               MidasVector sol(red_dim);
               if (mNonLinEq) 
               {
                  sol = rhs; // start as rhs
               }
               else
               {
                  mRedSolVec.GetCol(sol,i_eq); // start as old sol.
               }
               Nb resid;
               In iter; 
               if (!mRedHam.ConjGrad(sol,rhs,resid,iter,tol_red_lineq,I_200))
               {
                  Mout << " There was a problem in ConjGrad Solution " << endl;
                  Mout << " Reduced matrix \n" << mRedHam << endl;
                  Mout << " Reduced gradient \n" << rhs << endl;
                  Mout << " Reduced sol after conjgrad \n" << sol << endl;
                  string s1="Problem in solving reduced equations";
                  //MidasWarning(s1);
                  MIDASERROR("Problem in solving reduced equations");
               }
               if (resid > tol_red_lineq)
               {
                  Mout << " The resid was = " << resid << " thr = " << tol_red_lineq << endl;
                  if (resid < tol_red_lineq*C_10_2) Mout << " Ok I accept it " << endl;
                  else MIDASERROR("Problem in reduced equation solution");
               }
               mRedSolVec.AssignCol(sol,i_eq);    
               if (gDebug || mIoLevel > I_5) 
                  Mout << " Conj. Grad Sol of reduced eq: " << i_eq 
                  << " resid: " << resid 
                  << " in " << iter << " iterations " << endl;
            }
            if (gDebug||mIoLevel>I_20) 
               Mout << " mRedSol \n" << endl << mRedSolVec << endl;
         }
      }
      // end if: solve reduced linear equation/non linear equation (started line ~1176)
      
      // if: solve eigen-equation with harmonicrr
      if (mEigEq && mHarmonicRR)
      {
         In i = I_0;
         bool proceed=true;
         while (i<mRedDim-I_1)
         {
            if ( libmda::numeric::float_eq(arRedEigVal[i],arRedEigVal[i+I_1]) ) ++i;
            else
            {
               arRedEigVal[i]=mEnerShift+C_1/arRedEigVal[i];
               ++i;
            }
         }
         if (proceed) arRedEigVal[i]=mEnerShift+C_1/arRedEigVal[i];
      }
      // end if: solve eigen-equation with harmonicrr
      /***********************************
       * END solve reduced problem       *
       ***********************************/
      
      mImag = mReImEigVal.size()/I_2;

      /**
       * do targeting if requested
       **/
      // Find solutions vectors which are relevant to the target vectors
      // Has so far only been relevant for Eigenstates ....
      if (mTargetSpace && mEigEq)
      {
         vector<In> i_targs(mNtargs,-I_1);
         vector<Nb> s_targs(mNtargs);
         vector< pair<In,map<Nb,In> > > target_overlaps;
         target_overlaps.reserve(mNtargs);

         Mout << "\n Targeting strategy: " << mTargetingMethod << "\n" << endl; 

         // Minimum overlap squared to consider a solvec
         Nb overlap_min=C_I_10_4;
         if (mTargetingMethod=="OVERLAP")
         {
            if (overlap_min>mOverlapMin)
            {
               if (gDebug || mIoLevel > I_5) 
                  Mout << " OVERLAPMIN is terribly small. Watch out!" << endl;
               overlap_min=mOverlapMin;
            } 
            if (gDebug || mIoLevel > I_5) 
               Mout << " All the eigenvectors having overlaps with targets greater than " << overlap_min
                    << " are retained." << endl;
         }
         else if (mTargetingMethod=="ENERGY")
         {
            if (gDebug || mIoLevel > I_5) 
            {
               Mout << " All the eigenvectors having energies that differ less than " << mEnergyDiffMax 
                  << " a.u.\n relative to the energies of the best matching states are retained." << endl;
               Mout << " By default, eigenvectors having overlaps with targets less than " << overlap_min
               << " are not considered." << endl;
            }
         }
         else if (mTargetingMethod=="OVERLAPANDENERGY")
         {
            if (overlap_min>mOverlapMin)
            {
               if (gDebug || mIoLevel > I_5) 
                  Mout << " OVERLAPMIN is terribly small. Watch out!" << endl;
               overlap_min=mOverlapMin;
            } 
            if (gDebug || mIoLevel > I_5) 
            {
               Mout << " All the eigenvectors having energies that differ less than " << mEnergyDiffMax 
                  << " a.u.\n relative to the energies of the best matching states, but"
                  << "\n having overlaps with targets greater than " << overlap_min << " are retained." << endl;
            }
         }
         else if (mTargetingMethod=="OVERLAPSUM")
         {
            if (gDebug || mIoLevel > I_5) 
            {
               Mout << " All the eigenvectors having the largest overlaps with the targets are retained" <<
                  " in a way that the sum of overlaps should be not greater than " << mOverlapSumMin << 
                  "\n for each target." << endl;
               Mout << " By default, solution vectors having overlaps with targets less than " << overlap_min
                  << " are not considered." << endl;
            } 
         }
         else if (mTargetingMethod!="BESTHITS")
         {
            MIDASERROR(" Unknown targeting method is specified!");
         }

         // Loop over target space 
         for (In i_t=I_0;i_t<mNtargs;i_t++)
         {
            std::map<Nb,In> actual_target_candidates;
            // Find the red sol with max overlaps and store
            // first try
            Nb smax=C_0;
            In i_smax=-I_1;
            for (In i_new1=I_0;i_new1<mRedDim;i_new1++)
            {
               Nb x = C_0;
               for (In i_new2=I_0;i_new2<mRedDim;i_new2++)
                  x += mTarTriS[i_t][i_new2]*mRedSolVec[i_new2][i_new1];
               Nb s=x*x;

               // ian: i have commented below check out, as it will screw you over in some cases (fx in a #X Duplicates calculation)
               //      and I don't immediately see the reason for this check,
               //      as you will probably get fu#%'ed anyways in case of Imag eigvals
               //Nb energy_diff=C_NB_MAX;
               //if (i_new1>I_0) 
               //   energy_diff=arRedEigVal[i_new1]-arRedEigVal[i_new1-I_1];
               //if (energy_diff!=C_0) 
               {
                  if (s > smax)
                  {
                     smax=s;
                     i_smax=i_new1;
                  }
                  if (s > overlap_min)
                  {
                     if (gDebug || mIoLevel >= I_50) 
                        Mout << " overlap with SolVec #" << i_new1 << " = " << s << endl;
                     actual_target_candidates.insert(std::make_pair(s,i_new1));
                  }
               }
            }
            if (smax <= overlap_min)
            {
               if (gDebug || mIoLevel > I_5)
                  Mout << " overlap with SolVec #" << i_smax << " = " << smax << endl;
               actual_target_candidates.insert(std::make_pair(smax,i_smax));
               Mout << " There is no state with s^2 >" << overlap_min << " for targ.state #" << i_t << endl;
               Mout << " No worries. State with the largest overlap was added." << endl;
               string swar=" No state with s^2 > threshold occured - too large overlap_min?";
               MidasWarning(swar);
            }

            // Insert all hits
            target_overlaps.push_back(std::make_pair(i_t,actual_target_candidates));

            // Specify the best
            std::map<Nb,In>::reverse_iterator pos_best=actual_target_candidates.rbegin(); // we just take the last entry, 
            i_targs[i_t] = (*pos_best).second; // get vector number
            s_targs[i_t] = (*pos_best).first;  // get s^2
            
            // do some output
            if (gDebug || mIoLevel >= I_10)
            {
               Mout << " --------------" << endl;
               Mout <<    " Best for  tg #" << i_t << " Solvec #" <<
                  (*pos_best).second << " s^2 = "  << (*pos_best).first << endl;
               Mout << " --------------" << endl;
               for (map<Nb,In>::const_iterator ci=actual_target_candidates.begin();
                     ci != actual_target_candidates.end();ci++)
               {
                  Mout << " Target state #" << i_t << " Solvec #" << (*ci).second << " s^2 = " << (*ci).first << endl;
               }
            }
         }

         In n_targets_for_hit_states = target_overlaps.size();

         // Check the best solvecs for each targets
         // if one is taken more than once take the second best one etc.
         if (gDebug || mIoLevel >= I_10) 
            Mout << " Identify unique vector to each targets \n" << endl; 
         bool each_was_unique;
         do 
         {
            each_was_unique=true;
            for (In i=I_0;i<n_targets_for_hit_states;i++)
            {
               In count_while=I_0;
               while (count(i_targs.begin(),i_targs.end(),i_targs[i])!=I_1)
               {
                  each_was_unique=false;
                  count_while++;
                  if (count_while > 1000)
                     MIDASERROR("Error in identifying unique vector. Count > 1000.");
                  if (gDebug || mIoLevel >= I_10) 
                     Mout << " For target state " << i 
                          << " the first try for matching target state was already picked up " 
                          << "\n Relax - I'll find the best match not already taken: " << endl;

                  //for (In j=i+I_1;j<n_targets_for_hit_states;j++)
                  for (In j=I_0;j<n_targets_for_hit_states;j++)
                  {
                     if (i_targs[j]==i_targs[i] && i!=j)
                     {
                        if (s_targs[j]>s_targs[i]) 
                        {
                           In kk = I_0; 
                           In num_hits=((target_overlaps[i]).second).size();
                           Mout << " i: " << i << " j: " << j << " num_hits " << num_hits << endl; 
                           if (num_hits>I_1)
                           {
                              for (map<Nb,In>::reverse_iterator cix=((target_overlaps[i]).second).rbegin();
                                    cix != ((target_overlaps[i]).second).rend();cix++)
                              {
                                 if (kk==I_1)
                                 {
                                    i_targs[i]=(*cix).second;
                                    s_targs[i]=(*cix).first;
                                    kk=I_2;
                                 }
                                 if ((*cix).second==i_targs[i]) kk++;
                              }
                           }
                           else
                              MIDASERROR("Error in identifying unique vector.");
                        }                     
                        else // s_targs[j]<=s_targs[i]
                        {
                           In kk = I_0; 
                           In num_hits=((target_overlaps[j]).second).size();
                           if (num_hits>I_1)
                           {
                              for (map<Nb,In>::reverse_iterator cix=((target_overlaps[j]).second).rbegin();
                                    cix != ((target_overlaps[j]).second).rend();cix++)
                              {
                                 if (kk==I_1)
                                 {
                                    i_targs[j]=(*cix).second;
                                    s_targs[j]=(*cix).first;
                                    kk=I_2;
                                 }
                                 if ((*cix).second==i_targs[j]) kk++;
                              }
                           }
                           else
                              MIDASERROR("Error in identifying unique vector.");
                        }
                     }
                  }
               }
               if (gDebug || mIoLevel >= I_10)
                  Mout << " --------------\n" << " For target #" << i << " solvec #" 
                       << i_targs[i] << " s = " << s_targs[i] <<  endl; 
            }
         } while (!each_was_unique);

         In n=mRedDim;
         vector<In> tar_ivec (mRedDim,-I_1);
         tar_svec.assign(mRedDim,C_0);

         //States and their overlaps which have the largest overlaps with the target states
         if (gDebug || mIoLevel >= I_5) 
            Mout <<    "\n Best hits:" << "\n --------------" << endl;
         for (In i=I_0;i<n_targets_for_hit_states;i++)
         {
            tar_ivec[i_targs[i]]=i;
            tar_svec[i_targs[i]] = s_targs[i];
            if (gDebug || mIoLevel >= I_5) 
               Mout <<    " For target #" << i << " solvec #" 
                  << i_targs[i] << " s^2 = " << s_targs[i] << endl;
         }

         overlap_min=max(overlap_min,mOverlapMin);

         In i_target = n_targets_for_hit_states-I_1;
         if (mTargetingMethod!="BESTHITS")
         {
            // States and their overlaps which have large overlaps with all the target states
            // except the best ones
            if (gDebug || mIoLevel >= I_5) 
               Mout <<    "\n Other important hits:" << "\n --------------" << endl;
            for (vector<pair<In,map<Nb,In> > >::const_iterator ci=target_overlaps.begin();
                  ci != target_overlaps.end();ci++)
            {
               // THE LOOP WAS CHANGED TO REVERSE ORDER DUE TO OVERLAPSUM s_1^2>s_2^2>...>s_N^2
               Nb overlap_sum=C_0;
               for (map<Nb,In>::const_reverse_iterator rix=((*ci).second).rbegin();
                     rix != ((*ci).second).rend();rix++)
               {
                  if (tar_ivec[(*rix).second]==-I_1 && mTargetingMethod=="OVERLAP")
                  {
                     if (overlap_min<(*rix).first)
                     {
                        i_target++;
                        tar_ivec[(*rix).second]=i_target;
                        tar_svec[(*rix).second] = (*rix).first;
                        if (gDebug || mIoLevel >= I_10) 
                           Mout <<    " For target #" << (*ci).first << " solvec #" 
                              << (*rix).second << " s^2 = " << (*rix).first << endl;
                     }
                  }
                  if (tar_ivec[(*rix).second]==-I_1 && 
                        (mTargetingMethod=="ENERGY"||mTargetingMethod=="OVERLAPANDENERGY"))
                  {
                     Nb energy_diff=fabs(arRedEigVal[i_targs[(*ci).first]]-arRedEigVal[(*rix).second]);
                     if (gDebug || mIoLevel >= I_50) // I_50
                        Mout <<    " For target #" << (*ci).first << " solvec #" 
                           << (*rix).second << " |dE|= " 
                           << energy_diff << " a.u. " << endl;
                     if (energy_diff<mEnergyDiffMax && overlap_min<(*rix).first)
                     {
                        i_target++;
                        tar_ivec[(*rix).second]=i_target;
                        tar_svec[(*rix).second] = (*rix).first;
                        if (gDebug || mIoLevel >= I_10) 
                           Mout <<    " For target #" << (*ci).first << " solvec #" 
                              << (*rix).second << " s^2= " << (*rix).first << " |dE|= " 
                              << energy_diff << " a.u. " << endl;
                     }
                  }
                  if (mTargetingMethod=="OVERLAPSUM")
                  {
                     if (overlap_sum<mOverlapSumMin)
                     {
                        if (gDebug || mIoLevel > I_11)
                        {
                           Mout <<    " For target #" << (*ci).first << " Sum(s^2) = " << overlap_sum;
                        }
                        overlap_sum += (*rix).first;
                        if (gDebug || mIoLevel > I_11)
                        {
                           Mout << " solvec #" << (*rix).second << " s^2= " << (*rix).first
                                << " Sum(s^2)= " << overlap_sum << std::endl;
                        }
                        if (tar_ivec[(*rix).second]==-I_1)
                        {
                           //if (gDebug || mIoLevel >= I_50) 
                           //   Mout << " tar_ivec[(*rix).second] " << tar_ivec[(*rix).second] << endl;
                           i_target++;
                           tar_ivec[(*rix).second]=i_target;
                           tar_svec[(*rix).second] = (*rix).first;
                        } 
                     } 
                  }
               }
            }
         }
         
         mNimportantVecs = i_target+I_1;
         
         if (gDebug || mIoLevel > I_5) 
         {
            Mout << " Number of target states called:            " << mNtargs         
               << "\n Number of target states found:             "
               << n_targets_for_hit_states << endl;
            Mout << " --------------" << endl;
            Mout << " Number of best solution vecs:              " << n_targets_for_hit_states 
               << "\n Number of important solution vecs:         " << mNimportantVecs
               << "\n Number of all the solutions vecs:          " << mRedDim << endl;
         }

         // ian: i commented below code out...
         //mImag=I_0;
         /*for (In i=mRedDim-I_1;i>I_0;i--) 
         {
            if ( tar_ivec[i-I_1]!=-I_1)
            {
               if ( libmda::numeric::float_eq(arRedEigVal[i],arRedEigVal[i-I_1]) )
               {
                  i_target++;
                  //mImag++;
                  tar_ivec[i]=i_target;
                  //tar_svec[i] = C_0;
               }
            }
         }*/
         
         //mNimportantVecs = i_target+I_1;
         
         if (mImag > I_0 && (gDebug || mIoLevel > I_5)) 
         {
            Mout << " Number of imag. vectors temporarily added: " << mImag 
                 << "\n Number of virtual important solution vecs: " << mNimportantVecs << std::endl;
         }

         for (In i=I_0;i<mRedDim;i++)
         {
            if ( tar_ivec[i]==-I_1)
            {
               i_target++;
               tar_ivec[i]=i_target;
            }
         }

         // Shell sort,stroustrup ... ian: why is this implemented inline ??...
         for (In gap=n/2;0<gap;gap/=2)
            for (In i=gap;i<n;i++)
               for (In j=i-gap;0<=j;j-=gap)
               {
                  // order after energy 
                  //bool less = arRedEigVal[j+gap] < arRedEigVal[j]; 
                  //if (order_abs_vals) less = 
                  //fabs(arRedEigVal[j+gap]) < fabs(arRedEigVal[j]); 
                  // Order after target states and rest after standard 
                  // NB Assumes that target states are available for all!
                  // states from 0 to n_targs-I_1!!
                  bool less = (tar_ivec[j+gap]<tar_ivec[j]);
                  if (less)
                  {
                     std::swap(tar_ivec[j],tar_ivec[j+gap]);
                     std::swap(tar_svec[j],tar_svec[j+gap]);
                     std::swap(arRedEigVal[j],arRedEigVal[j+gap]);
                     mRedSolVec.SwapCols(j,j+gap);
                  }
               }
         // DEBUG
//         Mout  << " Reduced solution after sort:\n" << mRedSolVec << "\n Reduced eigvals after sort:\n" << arRedEigVal << std::endl;

         ofstream overlaps_stream(std::string(mAnalysisDir + "/Target_Overlaps").c_str());
         MidasStreamBuf overlaps_buf(overlaps_stream);
         MidasStream overlaps(overlaps_buf);
         for (In i=I_0;i<mNimportantVecs;i++)
            overlaps << i << " " << tar_svec[i] << endl;
      }
      // end if(mTargetSpace && mEigEq) at approx. line 1309 (yes this is 375+ lines)
      /**
       * END targeting
       **/
        
      /***********************************
       * Calculate residual vectors, find new vectors
       ***********************************/
      if (!mNonLinEq) converged = true; 
      mNnew =I_0;
      In n_not_conv = I_0;

      // if: eigeq and targeting
      if (mTargetSpace && mEigEq)
      {
         if (mNimportantVecs>mNeq)
         {
            DataCont tmp_datacont;
            for (In i_grow=mNeq;i_grow<mNimportantVecs;i_grow++)
            {
               mpResidualVecs.push_back(tmp_datacont);
               string s = mFileLab+"_Resid_"+std::to_string(i_grow);
               mpResidualVecs[i_grow].NewLabel(s);
               mpResidualVecs[i_grow].ChangeStorageTo(storage_mode);
               mpResidualVecs[i_grow].SetNewSize(mDim,false);
               apSolVec.push_back(tmp_datacont);
               s = mSolVecFilNamBas+std::to_string(i_grow);
               apSolVec[i_grow].NewLabel(s);
               apSolVec[i_grow].ChangeStorageTo(storage_mode);
               apSolVec[i_grow].SetNewSize(mDim,false);
               apSolVec[i_grow].SaveUponDecon(true);
               if (mImprovedPrecond)
               {
                  string solveco_storage_mode = "OnDisc";
                  if (mStorageLevel==I_0) solveco_storage_mode = "InMem"; 
                  mpSolvecO.push_back(tmp_datacont);
                  s = "IMPREC_sol_vec_1_"+std::to_string(i_grow);
                  mpSolvecO[i_grow].NewLabel(s);
                  mpSolvecO[i_grow].ChangeStorageTo(solveco_storage_mode);
                  mpSolvecO[i_grow].SetNewSize(mpSolvecD[I_0].Size(),false);
                  mpSolvecO[i_grow].Zero();
               }
               eig_val_old.push_back(C_0);
               eig_val_diff.push_back(C_0);
            }
            mNeq=mNimportantVecs;
         }
         else if (mNimportantVecs<mNeq)
         {
            for (In i_grow=mNeq-I_1;i_grow>mNimportantVecs-I_1;i_grow--)
            {
               apSolVec[i_grow].SaveUponDecon(false);
               apSolVec.pop_back();
               mpResidualVecs.pop_back();
               if (mImprovedPrecond) mpSolvecO.pop_back();
               eig_val_old.pop_back();
               eig_val_diff.pop_back();
            }
            mNeq=mNimportantVecs;
         }
      }
      // end if: eigeq and targeting
      
      std::map< In,map<In,Nb> > imag_info;
      // save info on imag eigvals
      if ( mImag>I_0 )
      {
         std::map<In,Nb> state_val;
         In i=I_0;
         Mout << " mReImEigVal: " << mReImEigVal << endl;
         while (i<mReImEigVal.size())
         {
            for (In idx=I_0;idx<mNeq-mImag;idx++)
            {
               if ( libmda::numeric::float_eq(mReImEigVal[i],arRedEigVal[idx]) )
               {
                  for (In idx_im=mNeq-mImag;idx_im<mNeq;idx_im++)
                  {
                     if ( libmda::numeric::float_eq(mReImEigVal[i],arRedEigVal[idx_im]) )
                     {
                        if (mHarmonicRR)
                        {
                           Nb denom=mReImEigVal[i]*mReImEigVal[i]+mReImEigVal[i+I_1]*mReImEigVal[i+I_1];
                           mReImEigVal[i]=mEnerShift + mReImEigVal[i]/denom; 
                           mReImEigVal[i+I_1]=-mReImEigVal[i+I_1]/denom; 
                           arRedEigVal[idx]   =mReImEigVal[i]; 
                           arRedEigVal[idx_im]=mReImEigVal[i]; 
                           //Mout << "  arRedEigVal[" <<idx <<"] = "    <<  arRedEigVal[idx]    << endl;
                           //Mout << "  arRedEigVal[" <<idx_im <<"] = " <<  arRedEigVal[idx_im] << endl;
                        }
                        
                        state_val.insert(std::make_pair(idx_im,mReImEigVal[i+I_1]));
                        //Mout << " idx_im,mReImEigVal[i+I_1]: " <<  idx_im << " , " << mReImEigVal[i+I_1] << endl;
                     }
                  }

                  state_val.insert(std::make_pair(idx,mReImEigVal[i]));
                  //Mout << " idx,mReImEigVal[i]: " <<  idx << " , " << mReImEigVal[i] << endl;
               }
            }
            for (map<In,Nb>::const_iterator ci=state_val.begin();
                  ci != state_val.end();ci++)
            {
               imag_info.insert(std::make_pair((*ci).first,state_val));
               //Mout << " (*ci).first : " <<  (*ci).first << endl;
            }
            i+=I_2;
         }
      } 
      // end if(mImag>I_0)
      
      
      // Calculate change in excitation energies in each iteration 
      if (mEigEq)
      {
         for (In i_eq = I_0; i_eq < mNeq; i_eq++)
         {
            eig_val_diff[i_eq] = arRedEigVal[i_eq] - eig_val_old[i_eq];
            eig_val_old [i_eq] = arRedEigVal[i_eq];
         }
         if (gDebug || mIoLevel > I_5)
         {
            Mout << " Difference in eigen values compared to previous iteration " 
                 << " are \n" << eig_val_diff << std::endl;
         }
      }

      mResidNorms.SetNewSize(mNeq, false);
      //In n_resids = mNeq-mImag; // ian: does this make sense??.. 
      In n_resids = mNeq;
      if (mEigEq) n_resids = min(mNeq,mRedDim); 

      //*** start loop over n_resids ***/
      for (In i_eq=I_0;i_eq<n_resids;i_eq++)
      {
         // for nonlilnear equations, do additional
         // addition and generate new vector, and 
         // no more (continue for)
         if (mNonLinEq)
         {
            apSolVec[i_eq].Zero();
            if (mDiis&&!backstep)
            {
               In i_off = mRedDim-red_dim+I_1;
               // Diis: sum_k=1,n wk*tk
               for (In i=I_0;i<red_dim-I_1;i++)
               {
                  apSolVec[i_eq].Axpy(mpTrials[i+i_off],
                        mRedSolVec[i][i_eq]);
               }
               // + wn*deltat_n
               //Mout << " cont   " << mRedSolVec[red_dim-I_2][i_eq] <<endl;
               //Mout << " vector " << mpTransTrials[mRedDim-I_1] << endl;
               apSolVec[i_eq].Axpy(mpTransTrials[mRedDim-I_1],
                     mRedSolVec[red_dim-I_2][i_eq]);
            }
            In i_trial = mRedDim+mNnew;
            //Mout << " i_trial " << i_trial << endl;
            //Mout << " mRedDim " << mRedDim << endl;
            //Mout << " mNnew   " << mNnew   << endl;
            //Mout << " mRedDimMax " << mRedDimMax << endl;
            if (i_trial > mRedDimMax-I_1) break; // Next vector except if space is full

            mpTrials[i_trial].SetNewSize(mDim,false); 
            if (mDiis && !backstep)
            {
               mpTrials[i_trial] = apSolVec[i_eq];
               Mout << " Diis guess for new vector generated! " << endl;
            }
            else if (!mDiis && !backstep)
            {
               mpTrials[i_trial] = mpTrials[i_trial-I_1];
               mpTrials[i_trial].Axpy(mpTransTrials[i_trial-I_1],C_1);
               Mout << " Non-diis guess for new vector generated! " << endl;
            }
            else if (backstep)
            {
               mpTrials[i_trial] = mpTrials[i_trial-I_1];
               mpTrials[i_trial].Axpy(mpTrials[i_trial-I_2],-C_1);
               mpTrials[i_trial-I_1].Axpy(mpTrials[i_trial],(c_backstep_min-C_1)); // try with c_back instead of full cor.
               //mpTrials[i_trial].Axpy(mpTransTrials[i_trial-I_2],-C_1);
               Mout << " Backstep-guess for new vector generated! " << endl;
            }
            mNnew++;
            continue; // continue loop over n_resids
         }
         // end if non linear equation

         bool standard_case=false;
         // If there are complex eigenvector
         if (mImag != I_0) 
         {
            std::map< In,map<In,Nb> >::iterator pos;
            pos = imag_info.find(i_eq);
            // Case of a complex eigenvector
            // If the eigenvector is complex the residual vectors are given by
            // r = Re(r)+i*Im(r)  and  r = Re(r)-i*Im(r)   
            // with
            // Re(r) = A*Re(x)-Re(lambda)*Re(x)+Im(lambda)*Im(x)
            // Im(r) = A*Im(x)-Re(lambda)*Im(x)-Im(lambda)*Re(x)
            // Now let's add Im(lambda)*Im(x) or Im(lambda)*Re(x) to the mpResidualVecs
            // Then calculate the norm of r=mResidNorms 
            if (pos != imag_info.end())
            {
               //if (i_eq<mNeq-mImag) 
               {
                  ResidualVector(i_eq,arRedEigVal,apSolVec,apRhs,break_now,false);
                  // Now Re(r) = A*Re(x)-Re(lambda)*Re(x)

                  // Here we calculate apSolVec and  mpResidualVecs for the Im part
                  //map<In,Nb>::iterator it=((*pos).second).rbegin();
                  In imagpart_idx=(*(((*pos).second).rbegin())).first;
                  Nb imagpart_eigval=(*(((*pos).second).rbegin())).second;
                  Mout << "imagpart_idx: " << imagpart_idx << endl;
                  Mout << "imagpart_eigval: " << imagpart_eigval << endl;
                  
                  ResidualVector(imagpart_idx,arRedEigVal,apSolVec,apRhs,break_now,false);
                  // Now Im(r) = A*Im(x)-Re(lambda)*Im(x)

                  mpResidualVecs[i_eq].Axpy(apSolVec[imagpart_idx],imagpart_eigval);
                  // Now Re(r) += Im(lambda)*Im(x)
                  
                  mpResidualVecs[imagpart_idx].Axpy(apSolVec[i_eq],-imagpart_eigval);
                  // Now Im(r) -= Im(lambda)*Re(x)

                  Nb norm_re = mpResidualVecs[i_eq].Norm();
                  Nb norm_im = mpResidualVecs[imagpart_idx].Norm();
                  Nb norm    = sqrt(norm_re*norm_re+norm_im*norm_im);
                  Mout << " Norm of Re: " << norm_re << " Norm of Im: " << norm_im 
                       << " Norm: " << norm << std::endl;
                  mResidNorms[i_eq] = norm; 
                  mResidNorms[imagpart_idx] = norm; // It is actually not necessary
               }
            }
            else
            {
               standard_case = true;
            }
         }
         else
         {
            standard_case = true;
         }

         // If the eigenvector is real compute the residual vector and its norm
         if (standard_case)
         {
            ResidualVector(i_eq, arRedEigVal, apSolVec, apRhs, break_now);
            mResidNorms[i_eq] = mpResidualVecs[i_eq].Norm();
         }

         Nb residual_threshold = mResidThreshold;
         Nb energy_threshold   = mEnerThreshold;
         if (mEigEq && mTargetSpace)
         {
            if (i_eq>=mNtargs)
            {
               if (mResidThreshold<C_NB_MAX*C_I_10_3)
                  residual_threshold = mResidThreshold*mResidThrForOthers;
               if (mEnerThreshold<C_NB_MAX*C_I_10_3)
                  energy_threshold =   mEnerThreshold*mEnerThrForOthers;
            }
         }
         
         /*** do some output ***/
         if (mEigEq)
         {
            Mout << " Eq: " << setw(2) << i_eq << " w= ";
            midas::stream::ScopedPrecision(16, Mout);
            Mout << arRedEigVal[i_eq];
            midas::stream::ScopedPrecision(3, Mout);
            Mout << " |(H-wS)C|= " << mResidNorms[i_eq];
            Mout << " w-w_o=" << setw(11) << right << eig_val_diff[i_eq];
            if (mTargetSpace) 
            {
               Mout.unsetf(ios::scientific);
               Mout << " s= " << tar_svec[i_eq];
               Mout.setf(ios::scientific);
            }
            if (mResidNorms[i_eq]> residual_threshold || 
               (mEigEq && fabs(eig_val_diff[i_eq]) > energy_threshold )) 
               Mout << " -C";
            else
               Mout << "   ";
            if (mTargetSpace && tar_svec[i_eq]< C_I_2) Mout << " -O"; 
            Mout << endl; 
         }
         Nb rhs_norm = C_0;
         if (mLinEq)
         {
            Mout << " Eq: " << setw(2) << i_eq;
            midas::stream::ScopedPrecision(4, Mout);
            Mout << " ||(H-w1)C-Rhs||= " << mResidNorms[i_eq];
            rhs_norm = apRhs[i_eq].Norm();
            Mout << " ||(H-w1)C-Rhs||/||Rhs|| = ";
            if (rhs_norm > C_0)  Mout << mResidNorms[i_eq]/rhs_norm;
            else Mout << " INF! NB! ||Rhs|| = zero! = ";
            midas::stream::ScopedPrecision(5, Mout);
            Mout << " w = " << arRedEigVal[i_eq];
            if (mResidNorms[i_eq]> mResidThreshold)
               Mout << " -C";
            else
               Mout << "   ";
            Mout << " It:"<<mNiter; 
            Mout << " R-Dim: " << mRedDim;
            if (mLevelEq>I_1) Mout << " MICRO"; 
            Mout << endl;
            //Mout << " Eq: " << setw(2) << i_eq << " Residual vector Norm:  " 
               //<< mResidNorms[i_eq] << endl; 
         }
         /*** end do some output ***/

         // Ove, store when In mem, else not available for restart 
         BckUpVec(apSolVec[i_eq]); 

         Nb norm_this = mResidNorms[i_eq]; //mpResidualVecs[i_eq].Norm();
         
         // add new trial vectors from residuals
         /* begin resid if */
         if (norm_this > residual_threshold || 
               (mEigEq && fabs(eig_val_diff[i_eq]) > energy_threshold ))
         {
            converged = false;
            n_not_conv++;
            // If not well, then there is not room for more anyway
            if (mRedDim+mNnew<mRedDimMax) 
            {
               bool standard_case=false;
               // If there are complex eigenvectors
               if (mImag!=I_0) 
               {
                  std::map< In,map<In,Nb> >::iterator pos;
                  pos = imag_info.find(i_eq);
                  // Case of a complex eigenvector
                  // If the eigenvector is complex the residual vectors are given by
                  // r = Re(r)+i*Im(r)  and  r = Re(r)-i*Im(r)   
                  // with
                  // Re(r) = A*Re(x)-Re(lambda)*Re(x)+Im(lambda)*Im(x)
                  // Im(r) = A*Im(x)-Re(lambda)*Im(x)-Im(lambda)*Re(x)
                  // Now let's add Im(lambda)*Im(x) or Im(lambda)*Re(x) to the mpResidualVecs
                  // Then calculate the norm of r=mResidNorms 
                  if (pos!=imag_info.end())
                  {
                     //if (i_eq<mNeq-mImag)
                     {
                        In i_trial = mRedDim+mNnew;
                        // the real and imag part of t is stored on mpTrials[i_trial] and mpTrials[i_trial+I_1]
                        mpTrials[i_trial].SetNewSize(mDim,false); 
                        mpTrials[i_trial+I_1].SetNewSize(mDim,false); 
                        // ensure right size as we start to work with these.

                        bool use_mdiagonal = true;
                        if (use_mdiagonal)
                        {
                           // Here we calculate apSolVec and  mpResidualVecs for the Im part
                           //map<In,Nb>::iterator it=((*pos).second).rbegin();
                           In imagpart_idx=(*(((*pos).second).rbegin())).first;
                           Nb imagpart_eigval=(*(((*pos).second).rbegin())).second;
                           Mout << "imagpart_idx: " << imagpart_idx << endl;
                           Mout << "imagpart_eigval: " << imagpart_eigval << endl;

                           Nb shift=arRedEigVal[i_eq];
                           if (mHarmonicRR)
                              shift=mEnerShift;

                           mpTrials[i_trial] = mpResidualVecs[i_eq];
                           mpTrials[i_trial+I_1] = mpResidualVecs[imagpart_idx];

                           // Calculate Re(x) standard part
                           if (mImprovedPrecond)
                           {
                              mpSolvecD[I_0].Reassign(apSolVec[i_eq],I_0,I_1,C_0,mpSolvecD[I_0].Size());
                           }

                           bool use_ImprovedPrecond = mImprovedPrecond;
                           if (mLevelEq == I_1 && mIoLevel > I_5) Mout << " InverseM. for the Davidson-term "  << endl;
                           if (mNiter==I_1 && mLevelEq == I_1 && mImprovedPrecond) mImprovedPrecond = false;
                           //clock_t t0 = clock();
                           InverseMatTimesVec(mpTrials[i_trial],shift,I_0);
                           // Note mDiagonal contains minus the diagonal so 
                           // we add RedEigval to get -(Ho-E)^-1
                           //Mout << " Time in solving Davidson at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;

                           if (use_ImprovedPrecond) mImprovedPrecond = true;

                           // Calculate Im(x) standard part
                           if (mImprovedPrecond)
                           {
                              mpSolvecD[I_0].Reassign(apSolVec[imagpart_idx],I_0,I_1,C_0,mpSolvecD[I_0].Size());
                           }

                           use_ImprovedPrecond = mImprovedPrecond;
                           if (mLevelEq == I_1 && mIoLevel > I_5) Mout << " InverseM. for the Davidson-term "  << endl;
                           if (mNiter==I_1 && mLevelEq == I_1 && mImprovedPrecond) mImprovedPrecond = false;
                           // Note mDiagonal contains minus the diagonal so 
                           // we add RedEigval to get -(Ho-E)^-1
                           //Mout << " Time in solving Davidson at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;

                           if (use_ImprovedPrecond) mImprovedPrecond = true;

                           bool found_div_zero = false;
                           if (mTrueHDiag && mNiter==I_1)
                           {
                              for (In i=I_0;i<mpTrials[i_trial].Size();i++)
                              {
                                 Nb valdiag;
                                 mDiagonal.DataIo(IO_GET,i,valdiag);
                                 valdiag+=arRedEigVal[i_eq];
                                 Nb val;
                                 apSolVec[i_eq].DataIo(IO_GET,i,val);
                                 //Werner: I think greater or less than zero may be defined in a better way   
                                 if ((fabs(val) > C_I_10_16*C_I_10_16) && (fabs(valdiag) < C_I_10_16) )
                                 {
                                    Nb val0;
                                    val0=C_0;
                                    mpTrials[i_trial].DataIo(IO_PUT,i,val0);
                                    found_div_zero = true;
                                    Mout << " Found zero element in (H_diag-Eo);   i = " << i << "   value = " << val0 << endl;
                                    Mout << " Davidson step with (1-CoCo*)(H_diag-Eo)(1-CoCo*)t = -r" << endl;
                                 }
                              }
                              for (In i=I_0;i<mpTrials[i_trial+I_1].Size();i++)
                              {
                                 Nb valdiag;
                                 mDiagonal.DataIo(IO_GET,i,valdiag);
                                 valdiag+=arRedEigVal[imagpart_idx];
                                 Nb val;
                                 apSolVec[imagpart_idx].DataIo(IO_GET,i,val);
                                 //Werner: I think greater or less than zero may be defined in a better way   
                                 if ((fabs(val) > C_I_10_16*C_I_10_16) && (fabs(valdiag) < C_I_10_16) )
                                 {
                                    Nb val0;
                                    val0=C_0;
                                    mpTrials[i_trial+I_1].DataIo(IO_PUT,i,val0);
                                    found_div_zero = true;
                                    Mout << " Found zero element in (H_diag-Eo);   i = " << i << "   value = " << val0 << endl;
                                    Mout << " Davidson step with (1-CoCo*)(H_diag-Eo)(1-CoCo*)t = -r" << endl;
                                 }
                              }
                           }
                           if (mNiter==I_1 && mLevelEq == I_1 && mImprovedPrecond)  found_div_zero = true;

                           if (mEigEq && mOlsen && !found_div_zero)
                           {
                              // the real and imag. part of y is stored as mpResidualVecs[i_eq], and mpResidualVecs[imagpart_idx], respectively

                              mpResidualVecs[i_eq]=apSolVec[i_eq];
                              mpResidualVecs[imagpart_idx]=apSolVec[imagpart_idx];
                              //mpResidualVecs[i_eq]=apSolVec[i_eq];
                              if (mIoLevel > I_2)
                              {
                                 Mout << " InverseM. for the Olsen-term "  << std::endl;
                              }

                              //clock_t t0 = clock();
                              InverseMatTimesVec(mpResidualVecs[i_eq],shift,I_1,i_eq);
                              //Mout << " Time in solving Olsen    at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
                              // re_y contains now y=-(H0-E0)^-1 Re(C0)

                              //clock_t t0 = clock();
                              InverseMatTimesVec(mpResidualVecs[imagpart_idx],shift,I_1,imagpart_idx); // imagpart_idx is not so clever input!!!
                              //Mout << " Time in solving Olsen    at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
                              // im_y contains now y=-(H0-E0)^-1 Im(C0)

                              Nb re_cxdot=-ComplexDotRe(apSolVec[i_eq],apSolVec[imagpart_idx],mpTrials[i_trial],mpTrials[i_trial+I_1]);
                              Nb im_cxdot=-ComplexDotIm(apSolVec[i_eq],apSolVec[imagpart_idx],mpTrials[i_trial],mpTrials[i_trial+I_1]);
                              Nb re_cydot= ComplexDotRe(apSolVec[i_eq],apSolVec[imagpart_idx],mpResidualVecs[i_eq],mpResidualVecs[imagpart_idx]);
                              Nb im_cydot= ComplexDotIm(apSolVec[i_eq],apSolVec[imagpart_idx],mpResidualVecs[i_eq],mpResidualVecs[imagpart_idx]);

                              Nb norm_factor=C_1/(re_cydot*re_cydot+im_cydot*im_cydot);

                              re_cxdot*=norm_factor;
                              im_cxdot*=norm_factor;

                              DataCont re_ycx(mDim,C_0);
                              re_ycx.Axpy(mpResidualVecs[i_eq],re_cxdot);
                              re_ycx.Axpy(mpResidualVecs[imagpart_idx],-im_cxdot);

                              DataCont im_ycx(mDim,C_0);
                              im_ycx.Axpy(mpResidualVecs[imagpart_idx],re_cxdot);
                              im_ycx.Axpy(mpResidualVecs[i_eq],im_cxdot);

                              mpTrials[i_trial].Axpy(re_ycx,re_cydot);
                              mpTrials[i_trial].Axpy(im_ycx,im_cydot);

                              mpTrials[i_trial+I_1].Axpy(im_ycx,re_cydot);
                              mpTrials[i_trial+I_1].Axpy(re_ycx,-im_cydot);

                              // c=x - y*(c*x/c*y) with x=-(H_0-E0)^-1(H-E0)C0
                              //Mout << " Full Newton step has been generated/Olsen Algo. " << endl;
                           }
                           else if (mEigEq)
                           {
                              if (mIoLevel > I_5) Mout << " step generated without Olsen Algo. " << endl;
                           }
                        }
                        else
                        {
                           Mout << i_eq << " true inverse dia. trans " << arRedEigVal[i_eq] << endl;
                           mpTrf->Transform(mpResidualVecs[i_eq],mpTrials[i_trial],I_0,-I_3,arRedEigVal[i_eq]);
                        }
                        mpTrials[i_trial].Normalize(); //Nothing happens if we normalize it here
                        mpTrials[i_trial+I_1].Normalize(); //Nothing happens if we normalize it here
                        // -3 has the effect of using no Projector, 
                        // check Vcc TransH for example 
                        //
                        // Now trial vectors should be ALMOST ready, 
                        // orthogonalize first and addition is done 
                        //mpTrials[i_trial].Normalize(); 
                        if (Orthogonalize(i_trial,mRedDim+mNnew,mLinEq,rhs_norm)) mNnew++;
                        if (Orthogonalize(i_trial+I_1,mRedDim+mNnew,mLinEq,rhs_norm)) mNnew++;
                     }
                  }
                  else standard_case=true;
               }
               else standard_case=true;
               // end if there are complex eigenvectors

               // If the eigenvector is real compute the residual vector and its norm
               if (standard_case)
               {
                  In i_trial = mRedDim+mNnew;
                  mpTrials[i_trial].SetNewSize(mDim,false); 
                  // ensure right size as we start to work with these.

                  bool use_mdiagonal = true;
                  if (use_mdiagonal)
                  {
                     Nb shift=arRedEigVal[i_eq];
                     if (mHarmonicRR)
                        shift=mEnerShift;
                     mpTrials[i_trial] = mpResidualVecs[i_eq];
                     if (mImprovedPrecond)
                     {
                        mpSolvecD[I_0].Reassign(apSolVec[i_eq],I_0,I_1,C_0,mpSolvecD[I_0].Size());
                     }
                     bool use_ImprovedPrecond = mImprovedPrecond;
                     if (mLevelEq == I_1 && mIoLevel >I_5) Mout << " InverseM. for the Davidson-term "  << endl;
                     if (mNiter==I_1 && mLevelEq == I_1 && mImprovedPrecond) mImprovedPrecond = false;
                     //clock_t t0 = clock();
                     //Mout << " The shift= " << shift << endl;
                       InverseMatTimesVec(mpTrials[i_trial],shift,I_0); 

                     if (  mIoLevel > I_12
                        )
                     {
                        Mout  << " Norm of new trial vector: " << mpTrials[i_trial].Norm() << std::endl;
                     }
                     
                     // Note mDiagonal contains minus the diagonal so 
                     // we add RedEigval to get -(Ho-E)^-1
                     //Mout << " Time in solving Davidson at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;

                     if (use_ImprovedPrecond) mImprovedPrecond = true;

                     bool found_div_zero = false;
                     if (mTrueHDiag && mNiter==I_1)
                     {
                        for (In i=I_0;i<mpTrials[i_trial].Size();i++)
                        {
                           Nb valdiag;
                           mDiagonal.DataIo(IO_GET,i,valdiag);
                           valdiag+=arRedEigVal[i_eq];
                           Nb val;
                           apSolVec[i_eq].DataIo(IO_GET,i,val);
                           //Werner: I think greater or less than zero may be defined in a better way   
                           if ((fabs(val) > C_I_10_16*C_I_10_16) && (fabs(valdiag) < C_I_10_16) )
                           {
                              Nb val0;
                              val0=C_0;
                              mpTrials[i_trial].DataIo(IO_PUT,i,val0);
                              found_div_zero = true;
                              Mout << " Found zero element in (H_diag-Eo);   i = " << i << "   value = " << val0 << endl;
                              Mout << " Davidson step with (1-CoCo*)(H_diag-Eo)(1-CoCo*)t = -r" << endl;
                           }
                        }
                     }
                     if (mNiter==I_1 && mLevelEq == I_1 && mImprovedPrecond)  found_div_zero = true;

                     if (mEigEq && mOlsen && !found_div_zero)
                     {
                        mpResidualVecs[i_eq] = apSolVec[i_eq];
                        
                        if (mIoLevel > I_2)
                        {
                           Mout << " InverseM. for the Olsen-term "  << std::endl;
                        }

                        //clock_t t0 = clock();
                        InverseMatTimesVec(mpResidualVecs[i_eq],shift,I_1,i_eq);
                        //Mout << " Time in solving Olsen    at level " << mLevelEq << " is: " <<  Nb(clock() - t0) / CLOCKS_PER_SEC << endl;
                        // mpResidualVecs[i_eq] contains now y=-(H0-E0)^-1C0
                        Nb xcdot=Dot(mpTrials[i_trial],apSolVec[i_eq]); 
                        Nb ycdot=Dot(mpResidualVecs[i_eq],apSolVec[i_eq]); 
                        Nb xcyc=-xcdot/ycdot;
                        // c=x - y*(c*x/c*y) with x=-(H_0-E0)^-1(H-E0)C0
                        mpTrials[i_trial].Axpy(mpResidualVecs[i_eq],xcyc);
                        //Mout << " Full Newton step has been generated/Olsen Algo. " << endl;
                     }
                     else if (mEigEq)
                     {
                        Mout << " step generated without Olsen Algo. " << endl;
                     }
                  }
                  else // use_mdiagonal == false
                  {
                     Mout << i_eq << " true inverse dia. trans " << arRedEigVal[i_eq] << endl;
                     mpTrf->Transform(mpResidualVecs[i_eq],mpTrials[i_trial],I_0,-I_3,arRedEigVal[i_eq]);
                  }
                  mpTrials[i_trial].Normalize(); //Nothing happens if we normalize it here
                  // -3 has the effect of using no Projector, 
                  // check Vcc TransH for example 
                  //
                  // Now trial vectors should be ALMOST ready, 
                  // orthogonalize first and addition is done 
                  //mpTrials[i_trial].Normalize(); 
                  if (Orthogonalize(i_trial,mRedDim+mNnew,mLinEq,rhs_norm)) mNnew++;
               }
               // end if real eigenvectors
            }
         }
         /* end resid if */

      }
      //*** end loop over n_resids ***/
      
      //Extra orthogonalization
      for (In ii=I_0;ii<I_1;ii++)
      {
         if (mIoLevel > I_5) Mout << " Extra orthogonalization for new vecs." << endl;
         In count_new=I_0;
         for (In j=mRedDim;j<mRedDim+mNnew;j++)
         { 
            if (Orthogonalize(j,j,false,C_0)) count_new++;
         }
         if (count_new!=mNnew) Mout << " Not all the update vectors have survived." << endl;
      }
      /***********************************
       * End: Calculate residual vectors, find new vectors ( a bit fuzzy, but think its around here)
       ***********************************/

      /**
       * do some book keeping and output for iteration
       **/
      if (mNiter > I_1)
      {
         mNeqPrev = mNeq;
      }
      if (mIoLevel > I_4)
      {
         Mout << "\n Added " << mNnew << " vectors to reduced space " << std::endl;
      }
      
      // If no new trial vectors have been added - make a rescue action
      // consisting of adding some unit vectors 
      if (!converged && mNnew == I_0 && mRedDim < mRedDimMax && mRedDim < mDim) 
      {
         Mout << " Rescue action - no new trial vectors were found outside existing space \n" 
              << " I will try to add some unit vectors" << endl;
         for (In j=I_0;j<mDim;j++)
         {
            In i_trial = mRedDim+mNnew;
            bool added_one = false;
            mpTrials[i_trial].SetToUnitVec(j);
            if (Orthogonalize(i_trial,mRedDim+mNnew)) 
            {
               mNnew++;
               added_one = true;
            }

            if (added_one)
            {
               if (mNnew >= n_not_conv) break; 
               // until we have as many as there are unsolved equations

               if (mRedDim+mNnew==mRedDimMax) break; 
               // If we have reached max dimension, stop this

               Mout << " mRedDim = " << mRedDim 
                    << " mRedDimMax = " << mRedDimMax << endl;
               Mout << " i_trial + I_1 = " << i_trial + I_1 << endl;
               if (i_trial+I_1 >= mRedDimMax) break;
               mpTrials[i_trial+I_1].SetNewSize(mDim,false); 
               // ensure right size of next one 
            }
         }
      }
      // Is the reduced space getting to large?
      if (mRedDim + mNnew > mRedDimMax) break; // break main while

      // Have we reached maximum nr of iterations or max dimension?
      if (mNiter == mNiterMax)  // ian: I'm påretty sure this check has been done WAY before
      {                         // on line ~935
         break; // break main while loop
      }
      // See if trial vectors are still orthonormal <- ian: what is this comment for ?

      /*****
       * Check for Break and handle
       *****/
      // If the current dimension + the new is beyond break dim, 
      // we simply restart......, 
      // 1. taking the best solution as start vector.
      // 2. along with the new trial vectors found above
      // (One could have tried to recover the corresponding transformed vector,
      // but for numerical safety we do not attempt to take advantage of this minor
      // CPU gain.)
      if (mNbreakDim < mRedDim + mNnew || break_now)
      {
         a_break_has_occurred = true;
         if (mNbreakDim < mRedDim + mNnew) 
            Mout << "\n\n Dimension of iterative space has reached the Break dimension "  << endl;
         if (break_now) Mout << " Break has been provoked by solution vectors deviating from norm one "  << endl;
         Mout << " I will take the current solution vectors as best guess "  << endl;
         Mout << " Before: mRedDim    = " << mRedDim;
         Mout << " mNnew      = " << mNnew;
         Mout << " mNbreakDim = " << mNbreakDim << endl;
         if (mNeq > mNbreakDim)
         {
            MIDASERROR(" mNeq > mNbreakDim - impossible calculation, increase ItEqBreakDim" );
         }
         In red_dim_old = mRedDim;
         In n_new_old = mNnew;  
         mNnew = I_0;
         if (mThickRestart)
         {
            std::multimap<Nb,In> restart_vecs;
            for (In idx_space=I_0;idx_space<mRedDim;idx_space++)
            {
               Nb de=C_NB_MAX;
               for (In idx_eq=I_0;idx_eq<mNeq;idx_eq++)
               {
                  de = min(fabs(arRedEigVal[idx_space]-arRedEigVal[idx_eq]),de);
               }
               if (gDebug || mIoLevel > 10)
               {
                  Mout << " state#" << idx_space << " arRedEigVal= "
                     << arRedEigVal[idx_space] << " dE= " << de << endl; 
               }
               restart_vecs.insert(std::make_pair(de,idx_space));
            }

            In thickbreakdim=mRedDim/2; // can be modified
            vector<DataCont> resvecs;
            resvecs.reserve(thickbreakdim);
            vector<DataCont> restransvecs;
            restransvecs.reserve(thickbreakdim);
            DataCont tmp_datacont;
            tmp_datacont.ChangeStorageTo("InMem");
            tmp_datacont.SetNewSize(mDim);
            DataCont tmp_datacont2;
            tmp_datacont2.ChangeStorageTo("InMem");
            tmp_datacont2.SetNewSize(mDim);

            In kk = I_0; 
            for (std::multimap<Nb,In>::const_iterator ci=restart_vecs.begin();
                  ci != restart_vecs.end();ci++)
            {
               tmp_datacont.Zero();
               tmp_datacont2.Zero();
               for (In i=I_0;i<mRedDim;i++)
               {     
                  tmp_datacont.Axpy(mpTrials[i],mRedSolVec[i][(*ci).second]);
                  tmp_datacont2.Axpy(mpTransTrials[i],mRedSolVec[i][(*ci).second]);
               }    
               resvecs.push_back(tmp_datacont);
               restransvecs.push_back(tmp_datacont2);
               if (kk<thickbreakdim-I_1) kk++;
               else break;
            }

            for (In idx=I_0;idx<thickbreakdim;idx++)
            {
               mpTrials[idx] = resvecs[idx];
               mpTransTrials[idx] = restransvecs[idx];
               bool vector_is_ready=true;
               // a bad way to prove the we are dealing with a nonsymmetric eigenvalue problem
               if (mDiagMeth=="DGEEVX" )
               {
                  if (!Orthogonalize(mNnew,mNnew,mLinEq)) vector_is_ready=false;
               }
               if (vector_is_ready) mNnew++;
            }
         }
         else
         {
            for (In i_eq=I_0;i_eq<mNeq;i_eq++)
            {
               mpTrials[i_eq] = apSolVec[i_eq];
               if (Orthogonalize(mNnew,mNnew,mLinEq)) 
               {
                  mNnew++;
               }
            }
         }
         Mout << " Restart iterative solution with the " << mNnew << " most optimal vectors " << endl;
           
         // just for safety, enter only if mpTrials has been used/resized
         if (red_dim_old > mNeq + n_new_old) 
         {
            for (In i_new=I_0;i_new<n_new_old;i_new++)
            {
               In i_trial = red_dim_old+i_new;
               mpTrials[mNnew] = mpTrials[i_trial];
               if (Orthogonalize(mNnew,mNnew,mLinEq)) 
               {
                  mNnew++;
               }
            }
            Mout << " and new (residual-based) trials vector giving in total " << mNnew 
               << " vectors in restart " << endl;
         }

         if (mThickRestart && mDiagMeth!="DGEEVX") // again the stupid check for nonsymmetric
         {
            mRedDim =mNnew-n_new_old;
            mNnew=n_new_old; // later on to increment only with the Trials which are not yet transformed. 
         }
         else 
         mRedDim  = I_0; // later mRedDim is increment with mNnew so it will be the correct mNnew upon restart 
         break_now = false;
      }
      else
      {
         a_break_has_occurred = false;
      }
         
      bool check_orthogonality=true;
      Nb orthogonality_sum_err_thr = C_NB_EPSMIN19*C_10_5;// lost a number of magnitudes 
      if (gDebug || check_orthogonality)
      {
         Nb sum_error=C_0;
         Nb max_error=C_0;
         for (In i=I_0;i<mRedDim+mNnew;i++)
         {
            for (In j=I_0;j<=i;j++)
            {
               Nb val = Dot(mpTrials[i], mpTrials[j]);
               //Mout << " Dot(i,j) i="<<i<<" j="<<j << " = " << val << endl;
               if (i!=j) sum_error+= C_2*fabs(val);
               if (i==j) sum_error+= fabs(val-C_1);
               if (i!=j && fabs(val)>max_error) max_error=fabs(val);
               if (i==j && fabs(val-C_1)>max_error) max_error=fabs(val-C_1); 
            }
         }
         sum_error/=(mRedDim+mNnew)*(mRedDim+mNnew);
         if (mIoLevel > I_6)
         {
            Mout << " Orthogonality mean error: " << sum_error << " max error: " << max_error << std::endl;
         }
         // If check fails then trigger break in next iteration 
         if (check_orthogonality && sum_error > orthogonality_sum_err_thr) 
         {
            Mout << " sum_error " << sum_error << " larger than orthogonality_sum_err_thr = "
                 << orthogonality_sum_err_thr << std::endl;
            Mout << " provokes break and restart of iterative procedure " << std::endl;
            
            break_now = true;
         }
      }
      
      if (!(mNonLinEq && !mDiis)) mRedDim += mNnew; // increase reduced space, but not if back step, then try again with last(now modif) guess.
      /**
       * end book keeping part
       **/
   }
   //***********************************************//
   // end main while loop                           //
   //                                               //
   // while startet on line ~748... (1750+ lines, seems about right...)
   //                                               //
   //***********************************************//
   
   
   if (mTimeIt && mNiter>I_0) 
   {
      string s_time = " CPU  time used in eq.sol. iteration " + std::to_string(mNiter) + ":";
      time_it.CpuOut(Mout,s_time);
      //time_it.WallOut(Mout," Wall time used in eq.solver this iteration: ");
   }

   // Printing out the overlap between targets and the converged states + 
   // printing out the overlap  between target states
   // printing out the energy difference  between target states
   if (mTargetSpace)
   {
      bool output_t_sol_overlap = true;
      if (output_t_sol_overlap)
      {
         Mout << "\n Overlap matrix ||<Phi_t|Phi_solvec_i>||^2 printed in file TargSolvecS.mat" << endl;
         ofstream targsolvecs_out_stream(std::string(mAnalysisDir + "/TargSolvecS.mat").c_str());
         MidasStreamBuf targsolvecs_out_buf(targsolvecs_out_stream);
         MidasStream targsolvecs_out(targsolvecs_out_buf);
         targsolvecs_out.setf(ios_base::scientific, ios_base::floatfield);
         midas::stream::ScopedPrecision(6, targsolvecs_out);
         for (In i=0;i<mNeq;i++)
         {
            for (In i_t=I_0;i_t<mNtargs;i_t++)
            {
               DataCont target;
               string name = mTargetFilNamBas + std::to_string(i_t);
               target.GetFromExistingOnDisc(mDim,name);
               target.SaveUponDecon();
               //Mout << " target Name " << target.Label() << endl;
               //Mout << " target conts " << target << endl;
               Nb s = Dot(target,apSolVec[i]);
               s=s*s;
               targsolvecs_out << setw(14) << right << s;
            }
            targsolvecs_out << endl;
         }
      }
      bool output_sol_energy = true;
      if (output_sol_energy)
      {
         Mout << "\n Energy distance matrix E_solvec_i-E_solvec_j printed in file SolvecE.mat" << endl;
         ofstream solvecs_e_out_stream(std::string(mAnalysisDir + "/SolvecE.mat").c_str());
         MidasStreamBuf solvecs_e_out_buf(solvecs_e_out_stream);
         MidasStream solvecs_e_out(solvecs_e_out_buf);
         solvecs_e_out.setf(ios_base::scientific, ios_base::floatfield);
         midas::stream::ScopedPrecision(6, solvecs_e_out);
         for (In i=0;i<mNeq;i++)
         {
            for (In j=I_0;j<mNeq;j++)
            {
               Nb de = (arRedEigVal[i]-arRedEigVal[j])*C_AUTKAYS;
               solvecs_e_out << setw(14) << right << de;
            }
            solvecs_e_out << endl;
         }
      }
   }

   Mout << "\n The number of H-transformations (after start-up) was " << n_trans << endl;
   if (!converged) 
   {
      MidasWarning("Equations are not converged in " + std::to_string(mNiter) 
         + " iterations, Dim reduced space  " + std::to_string(mRedDim) );
      if (mNiter >= mNiterMax) 
         Mout << " Maximum number of iterations exceeded " << endl;
      if (mRedDim >= mRedDimMax) 
         Mout << " Maximum dimension of reduced space exceeded " << endl;
      if (mNnew == I_0) 
         Mout << " No new vectors were added - so ?!?!?? " << endl;
      if (mNonLinEq && error_inc_break)
         Mout << " Error increased in iteration procedure!?!?!?  " << endl;
      if (mEigEq)
      {
         Mout << "\n\n The NONCONVERGED Eigen values "
            << " and residuals in the last iteration ";
         Mout << "were \n";
         for (In i_eq=I_0;i_eq<mNeq;i_eq++)
         {
            Mout << " " << i_eq << " " << arRedEigVal[i_eq] << " " 
               << mResidNorms[i_eq] << endl;
         }
      }
   }

   if (converged && mEigEq) 
   {
      Mout << "\n\n Equations are converged in " << mNiter 
         << " iterations, Dim reduced space  " << mRedDim << endl;
      Mout << "\n\n The converged Eigen values are: (eig,resid,eig-eig_0) \n";
      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
      {
         Mout << " ItEqRoot_" << left << setw(I_5) << i_eq << " " 
            << arRedEigVal[i_eq] << " " << mResidNorms[i_eq];
         Mout << " " << arRedEigVal[i_eq]-arRedEigVal[I_0];
         Mout << endl;
      }
   }
   if (mEigEq)
   {
      if (gDebug || mIoLevel > I_10)
      {
         Mout << "\n The remaining Eigen values in the reduced space ";
         Mout << " are: (eig,eig-eig_0) \n";

         for (In i = mNeq; i < mRedDim; i++)
         {
            Mout << i << " " << arRedEigVal[i];
            Mout << " " << arRedEigVal[i]-arRedEigVal[I_0];
            Mout << std::endl;
         }
      }
   }
   /*
   if (converged && mEigEq) 
   {
      Mout << "\n\n Equations are converged in " << mNiter 
         << " iterations, Dim reduced space  " << mRedDim << endl;
      Mout << "\n\n The final residuals are:  \n";
      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
      {
         Mout << "ItEq_" << left << setw(I_5) << i_eq << " " 
            << setw(30) << mResidNorms[i_eq] << endl;
      }
   }
   */

   arRedEigVal.SetNewSize(mNeq,true);
   if (mNonLinEq && mNeq > I_0) 
   {
      if (converged) Mout 
         << "\n\n Nonlinear equations are converged in "
         << mNiter << " iterations " << endl << endl;
      In i_eq = I_0;
      arRedEigVal[i_eq] = mEnerPrev;
   }
   if (gDebug) Mout << " End of ItEqSol::Solve " << endl;

   // Done, restore and return succes status
   mDiis = diis_save;
   if (converged) return true;
   return false;
}
/**
* Destructor - set defaults
* */
ItEqSol::~ItEqSol()
{
   if (mpSmallTransf != NULL)
      delete mpSmallTransf;
   mpSmallTransf = NULL;
   if (mpSolvecD     != NULL)
      delete[] mpSolvecD    ;
   mpSolvecD     = NULL;
}
/**
* Orthogonalize the mTrial number arNew agains the [0;arOld-] trials vecs
* Note: thr_again is the threshold for rechecking orthogonality of vectors
* after they should have been orthonormalized. If to many orders of magnitude
* of the norm is lost it is probably worthwhile to reort. to be sure
* linear dependencies sneak in.
* success or fail
* aRefSize is a reference size if small vectors are allowed to pass.
* For eigenvectors norm to one ensures a fixed scale - for lineq smaller vectors
* are sometimes needed. 
* */
bool ItEqSol::Orthogonalize(const In arNew,const In arOld,bool aLetSmallPass,Nb aRefSize,
              const In aRangeMin, const bool aDoNorm)
{
   Nb thr_again = C_I_10_1;
   Nb c_accept = sqrt(C_100/C_NB_EPSMIN19);
   //Nb c_accept = sqrt(sqrt(C_100/C_NB_EPSMIN19));
   //Mout << " c_accept " << c_accept << endl;
   In n_orto_max = I_5; // should be more than enough

   Nb norm_this = mpTrials[arNew].Norm();
   if ((!aLetSmallPass && norm_this < C_10*C_NB_EPSMIN19)
         || (aRefSize > C_NB_MIN && norm_this < C_10*C_NB_EPSMIN19*aRefSize))
   {
      Mout << " Trial vector skipped! It has very small norm on input " << norm_this << endl;
      return false;
   }
   // loop until we are sure about orthonormality of this vector with 
   // respect to old  
   bool orto = false; 
   In n_orto = I_0;
   while (!orto)
   {
      orto = true;
      if (gDebug || mIoLevel > I_20) 
         Mout << " norm of new trial vector before ortog " 
         << norm_this << endl;
      In i_min=aRangeMin;

      // Modified Gram-Schmidt algorithm
      //for (In i=I_0;i<arOld;i++)
      for (In i=i_min;i<arOld;i++)
      {
    mpTrials[arNew].Orthogonalize(mpTrials[i]);
      }

      //VccTransformer* vcc_trf = dynamic_cast<VccTransformer*>(mpTrf);
      //vcc_trf->CheckPurifies(mpTrials[arNew]); 
      mpTrf->CheckPurifies(mpTrials[arNew]); 

      Nb norm_now  = mpTrials[arNew].Norm();
      if (gDebug||mIoLevel>20) 
         Mout << " norm of new trial vector after  ortogonalization against old " << 
         norm_now << endl;
      // if the norm has decrease up to the number of digits in the accuracy, 
      // then..... forget it
      // Likewise if it can not be normalized since it is too close to zero.
      //Mout << norm_now << " thre " << c_accept *C_NB_EPSMIN19*norm_this << endl;
      bool norm_successful=true;
      if (aDoNorm )
      {
         norm_successful=mpTrials[arNew].Normalize();
      }

      if (!norm_successful|| 
            norm_now < c_accept *C_NB_EPSMIN19*norm_this)
      {
         Mout << " New vector was too close to zero after orthogonalization " 
            << endl;
         Mout << " norm now     " << norm_now;// << endl;
         Mout << " norm before ortog. " << norm_this << endl;
         //Mout << " c_accept     " << c_accept << endl;
         //Mout << " C_NB_EPSMIN19 " << C_NB_EPSMIN19<< endl;
         return false;
      }
      else if (norm_now < norm_this*thr_again) 
         //orto is equal to false, try one more round before we accept it.
      {
         orto = false;
         n_orto++;
         if (gDebug || mIoLevel > 3)
         {
            Mout << " Another round in Orthogonalize required " << endl;
            Mout << " Norm before " << norm_this << " Norm after " << norm_now;
            Mout << " in " << n_orto << " round " << endl;
         }
         norm_this = mpTrials[arNew].Norm();
         //norm_this = C_1; // it has just been normalized
      }
      else
      {
         if (gDebug || mIoLevel > 30)
         {
            Mout << " new trial vector ready " << mpTrials[arNew] << endl;
            Mout << " Norm of new trial vector it " << mpTrials[arNew].Norm(); 
         }
         return true;
      }
      if (n_orto > n_orto_max) 
         MIDASERROR(" Iterating too much in ItEqSol::Orthogonalize ");
   }
   return false;
}
/**
* Take (mDiagonal+arNb)-1 vec, 
* perhaps with a prediagonalization
* */
void ItEqSol::InverseMatTimesVec(DataCont& arDc, const Nb& arNb, In aI, In aIeq)
{
   if ((!mImprovedPrecond && !mPreDiag && !mSecOrd) || mLevelEq>I_1) 
   {
      if (mIoLevel > I_5) 
      {
         Mout << " InverseMatTimesVec -- Diagonal with norm " << mDiagonal.Norm() << " and aNb " << arNb << endl;
      }
      arDc.DirProd(mDiagonal,arNb,-I_1); 
   }
   else if (mPreDiag) 
   {
      Mout << " Generate (prediagonalized A)^-1*Vector" << endl;
      In nmin = mPreDiagAdd.size();
      MidasVector rhs(nmin);                  // the reduced part of the arDc.
      for (In i=0;i<nmin;i++) arDc.DataIo(IO_GET,mPreDiagAdd[i],rhs[i]);
      MidasVector sol(rhs);
      Nb resid=C_NB_MAX;
      In iter=I_0; 
      Nb tol_red_lineq = max(sqrt(C_NB_EPSMIN19*C_I_10_4)*
             min(C_1,rhs.Norm()),C_NB_EPSMIN19*C_10_4);
      Mout << " tol_red_lineq " << tol_red_lineq 
           << " rhs = " << rhs.Norm() << endl;
      for (In i=0;i<nmin;i++) mPreDiagMat[i][i]+=arNb;
      if (!mPreDiagMat.ConjGrad(sol,rhs,resid,iter,tol_red_lineq,I_200))
      {
         MidasWarning("No succes in solving prediag eq by ConjGrad");
         Mout << " Conj Grad. failed! \n"
              << " Rescue prediagonalization - invert matrix and multiply " 
              << endl;
         MidasMatrix eig_vec(nmin);
         MidasVector eig_val(nmin);
         Diag(mPreDiagMat,eig_vec,eig_val,"MIDAS_JACOBI",true); // Symmetric matrix assumed!
         MidasMatrix inverse(nmin,C_0);
         bool singular = false;
         Nb smallest=C_NB_MAX;
         for (In i=I_0;i<nmin;i++) 
         {
           if (smallest>fabs(eig_val[i])) smallest=fabs(eig_val[i]);
           if (fabs(eig_val[i])>C_NB_MIN) eig_val[i]=C_1/eig_val[i];
           else singular=true;
         }
         Mout << " Smallest absolute value of eig_val " << smallest << endl;
         if (singular) MIDASERROR("Singular matrix in prediag equations");
         for (In i=I_0;i<nmin;i++) 
            for (In k=I_0;k<nmin;k++) 
               for (In j=I_0;j<nmin;j++) 
                  inverse[i][k]+=eig_vec[i][j]*eig_val[j]*eig_vec[k][j];
         MidasMatrix test_unit(nmin);
         test_unit=inverse*mPreDiagMat; 
         Mout << " " << test_unit.SumEle() << " should be approximately: " 
            <<  nmin << endl;
         sol=inverse*rhs;
         MidasVector resid(nmin);
         resid = mPreDiagMat*sol;
         resid -= rhs;
         Mout << " Norm of residual for PreDiag equations = "  << resid.Norm() << endl;
         if (resid.Norm()>tol_red_lineq) 
          MidasWarning("Some prediag equations was not solved very accurately. Maybe Ok but please check");
      }
      for (In i=0;i<nmin;i++) mPreDiagMat[i][i]-=arNb;
      arDc.DirProd(mDiagonal,arNb,-I_1); 
      for (In i=I_0;i<nmin;i++) arDc.DataIo(IO_PUT,mPreDiagAdd[i],sol[i]);
   }
   else if (mSecOrd) 
   {
      Nb rhs_norm = arDc.Norm();
      Mout << "\n\n Solve linear equations for second order algorithm " << endl;
      Mout << " RHS vector norm = " << rhs_norm << endl;
      if (rhs_norm < mResidThreshold)
      {
         Mout << " Note! The RHS norm is less than residual threshold: " << mResidThreshold << endl;
         if (rhs_norm < C_NB_EPSMIN19*C_10)
         {
            Mout << " RHS is so small it may approach numerical problems!  " << endl;
            //Mout << " I will skip the solution of second order equations and return zero vector " << endl;
            //Mout << " In case of problems try decresasing residual threshold " << endl;
            //arDc.Zero();
         }
      }
      ItEqSol eqsol(mStorageLevel, mAnalysisDir);
      eqsol.SetTimeIt(mTimeIt);
      eqsol.SetIncreaseLevelEq();
      eqsol.SetVccvsVciPrec(true);
      eqsol.SetTransformer(mpTrf);
      eqsol.SetLinEq();
      eqsol.SetDim(mDim);

      // Do not pay off to solve too hard if eq. are too far from conv. and 
      // if the equat. are close to conv. it should on the other hand give some digits accuracy correct.
      Nb thr_fac = mMicroResFac;
      Nb resid_thr = rhs_norm*thr_fac; 
      if (resid_thr<mResidThreshold) 
      {
         Mout << " resid thr reset from " << resid_thr << " to " << mResidThreshold << endl; 
         resid_thr=mResidThreshold; 
      }
      eqsol.SetResidThreshold(resid_thr); 
      // Set max nr of it to maximum nr of micro iterations. 
      eqsol.SetNiterMax(mNmicroMax); 

      // Other we take from present equations.... 
      eqsol.SetRedDimMax(mRedDimMax);
      eqsol.SetBreakDim(mNbreakDim);
      //eqsol.SetDiagMeth("MIDAS_CG");
      //eqsol.SetDiagMeth(mLevel2Solver); // DO NOT USE DEFAULT BE SURE TO USE NONSYM! 
      eqsol.SetDiagMeth("DGESV"); 
      eqsol.SetIoLevel(mIoLevel/I_2);
      eqsol.SetImprovedPrecond(false);// Improved Preconditioning is turned off in the Second Set of Equations

      eqsol.SetTrueHDiag(mTrueHDiag);
      eqsol.SetNeq(mNeq);
      eqsol.SetTransformer(mpTrf);
      mpTrf->TrfUsingPreDiag(true);
      
      vector<DataCont> rhsvecs; 
      vector<DataCont> solvecs;
      rhsvecs.reserve(mRedDimMax);
      solvecs.reserve(mRedDimMax);
      DataCont tmp_datacont;
      for (In i_eq=I_0;i_eq<mNeq;i_eq++)
      {
         rhsvecs.push_back(tmp_datacont);  
         solvecs.push_back(tmp_datacont);
         string label = "SOALGO_rhs_vec"; 
         rhsvecs[i_eq].NewLabel(label);
         rhsvecs[i_eq]=arDc;
         rhsvecs[i_eq].SaveUponDecon();
         Mout << " SOALGO: Norm of start vector " << rhsvecs[i_eq].Norm() << endl;
         //Mout << " SOALGO: start vector label   " << rhsvecs[i_eq].Label() << endl;
         string slabel = "SOALGO_sol_vec"; 
         solvecs[i_eq].NewLabel(slabel);
         solvecs[i_eq] = rhsvecs[i_eq];
         solvecs[i_eq].Zero();
         //solvecs[i_eq].DirProd(mDiagonal,C_0,-I_1); 
         solvecs[i_eq].SaveUponDecon();
      }
      //eqsol.SetRestart(true);

      // Get diagonal cheeply and without too many problems 
      string diag_name = mDiagonal.Label();
      mDiagonal.DumpToDisc(diag_name);
      //Mout << " mDiagonal, dumped to disc " << mDiagonal << endl;
      eqsol.GetDiagonalFromFile(diag_name);
      MidasVector eig_vals(mNeq,C_0);
      if (eqsol.Solve(eig_vals,solvecs,rhsvecs))
      {
         Mout << " SOALGO Micro eq. converged to requested threshold " << endl;
      }
      else
      {
         Mout << " SOALGO Micro eq. not converged to requested threshold " << endl;
         bool rescue = false;  // OVE FIX 
         bool suc = false;
         if (rescue) 
         {
            Mout << " Rescue action: level shifted equations " << endl;
            if (mNeq!=I_1) MIDASERROR("Rescue action only imple. with 1 set of eq");
            In n_rescue = 5;
            eqsol.SetRestart(true);
            In n_min = I_1;
            vector<In> i_extrama(n_min);
            vector<Nb> extrama(n_min);
            mDiagonal.AddressOfExtrema(i_extrama,extrama,n_min,-I_2);
            Nb norm_diag = mDiagonal.Norm();
            Nb min_el = extrama[I_0];
            Nb level_shift = min_el - C_I_10*norm_diag/mDiagonal.Size();
            // freq = - level_shift , but diagonal is also actually minus diagonal.
            Mout << " Min ele " << min_el << " frq start (-levelshift) = " 
                << level_shift << endl;
            if (level_shift > C_0) MIDASERROR("Dont understand this level shift");
            Nb d_level_shift = level_shift/(n_rescue-I_1);
            for (In i=I_0;i<n_rescue;i++)
            {
               for (In j=I_0;j<mNeq;j++) eig_vals[j] = level_shift;
               if (eqsol.Solve(eig_vals,solvecs,rhsvecs)) suc=true;
               level_shift -= d_level_shift;
            }
         }
         if (!suc) 
         {
            string s1 = " SOALGO problems!"; 
            MidasWarning(s1);
         }
      }
      mpTrf->TrfUsingPreDiag(false);
      
      solvecs[I_0].Scale(-C_1);
      arDc = solvecs[I_0];
      Mout << " Solution vector copied to new increment vector \n\n" << endl;
      //Mout << " Transformed vector arDc " << arDc << endl;
      In n_micro_now = eqsol.Iter();
      mNmicro += n_micro_now;
      Mout << " Nr of Micro iterations in this macro " << n_micro_now 
         << " and accumulated " << mNmicro << endl;

   }
/**
* Prediagonalization to improve the preconditioning in the Olsen method.
* In the case of a vci[n] calc. instead of using only the diagonal of H as H0
* an improved H0 matrix is calculated as H0 = | Hvci[m] 0     |
*                                             | 0       Hdiag |
* where 0<m<=n. Solve the linear equation system (H0-E)*x=vector to obtain
* x=(H0-E)^-1*vector.
**/ 
   else if (mImprovedPrecond) 
   { 
      //Mout << " werner -- InverseMatTimesVec -- ImprovedPrecond " << endl;
      Mout << "\n\n Solve linear equations to obtain an improved preconditioner " << endl;

      Nb rhs_norm = arDc.Norm();
      Mout << " RHS vector norm = " << rhs_norm << endl;
      if (rhs_norm < mResidThreshold)
      {
         Mout << " Note! The RHS norm is less than residual threshold: " << mResidThreshold << endl;
         if (rhs_norm < C_NB_EPSMIN19*C_10)
         {
            Mout << " RHS is so small it may approach numerical problems!  " << endl;
            //Mout << " I will skip the solution of linear equations and return zero vector " << endl;
            //Mout << " In case of problems try decresasing residual threshold " << endl;
            //arDc.Zero();
         }
      }

      if(!mpSmallTransf) Mout << " mpSmallTransf is NULL" << std::endl;
      Mout << " ImprovedPrec-InverseMatTimesVec: TwoModeTrans = " << StringForBool(mpSmallTransf->GetTwoModeTrans()) << endl;
      Mout << " ImprovedPrec-InverseMatTimesVec: OneModeTrans = " << StringForBool(mpSmallTransf->GetOneModeTrans()) << endl;
      bool original_twomode=mpSmallTransf->GetTwoModeTrans();
      bool original_onemode=mpSmallTransf->GetOneModeTrans();
      if (mPrecondExciLevel==I_1 && original_twomode)
      {
         mpSmallTransf->SetSetTwoModeTrans(false);
         mpSmallTransf->SetSetOneModeTrans(true);
      }
      //if (mPrecondExciLevel==I_2 && original_twomode)
      //   mpSmallTransf->SetSetTwoModeTrans(true);


      In nvecsize = mpSmallTransf->NexciTransXvec();
      //Mout <<  " Dimension of the small vectors: " <<  mpSolvecD[I_0].Size() << endl; 
      //Mout <<  " Dimension of the small Xvecs: " << nvecsize << endl; 
      if ( mpSolvecD[I_0].Size()<nvecsize) nvecsize--;

      // Initialize common part of equation solver
      ItEqSol eqsol(mStorageLevel, mAnalysisDir);
      eqsol.SetTimeIt(mTimeIt);
      eqsol.SetIncreaseLevelEq();
      eqsol.SetTransformer(mpSmallTransf);
      eqsol.SetLinEq();
      eqsol.SetDim(nvecsize);
      Nb resid_thr = rhs_norm*C_I_10_4;
      eqsol.SetResidThreshold(resid_thr); // Do not pay off to solve too hard if eq. are too far from conv. and 
      // if the equat. are close to conv. it should on the other hand give some digits accuracy correct.
      eqsol.SetNiterMax(I_200);//mNiterMax
      eqsol.SetRedDimMax(I_300);//mRedDimMax
      eqsol.SetBreakDim(I_300);//mNbreakDim
      //eqsol.SetDiagMeth("MIDAS_CG");
      eqsol.SetDiagMeth(mLevel2Solver);
      eqsol.SetIoLevel(mIoLevel/I_2);
      eqsol.SetImprovedPrecond(false);// Improved Preconditioning is turned off in the Second Set of Equations
      eqsol.SetPrecondExciLevel(mPrecondExciLevel);
      eqsol.SetTrueHDiag(mTrueHDiag);
      eqsol.SetTrueADiag(mTrueADiag);
      eqsol.SetNeq(I_1);
      eqsol.SetOlsen(false);
      mpSmallTransf->TrfUsingPreDiag(false);

      DataCont smallvec(nvecsize);
      smallvec.Reassign(arDc,I_0,I_1,C_0,nvecsize);
     
      string storage_mode = "OnDisc";
      if (mStorageLevel<=I_1) storage_mode = "InMem"; 

      vector<DataCont> rhsvecs(I_1); 
      vector<DataCont> solvecs(I_1);
      DataCont tmp_datacont;
      rhsvecs.push_back(tmp_datacont);
      solvecs.push_back(tmp_datacont);

      //In i_eq=I_0;// Note that mNeq = 1
      //string label = "IMPREC_rhs_vec"; 
      //rhsvecs[i_eq].NewLabel(label,false);
      //rhsvecs[i_eq].ChangeStorageTo(storage_mode); 
      rhsvecs[I_0] = smallvec;
      // rhsvecs[i_eq].SaveUponDecon();
      Mout << " Norm of rhs vector " << rhsvecs[I_0].Norm() << endl;
      //Mout << " rhsvecs vector    " << rhsvecs[i_eq] << endl;
      //Mout << " ImprovedPrecond: rhsvecs vector label   " << rhsvecs[i_eq].Label() << endl;
      //string slabel = "IMPREC_sol_vec"; 
      string slabel = "IMPREC_sol_vec_" + std::to_string(aI);
      solvecs[I_0].NewLabel(slabel);
      solvecs[I_0].ChangeStorageTo(storage_mode); 
      //Mout << " ImprovedPrecond: solvecs vector label   " << solvecs[i_eq].Label() << endl;

      if (aI==I_0)
      {
         Mout << " Restart vector readin succesfull." << endl;
         solvecs[I_0] = mpSolvecD[I_0];
      }

      if (aI==I_1)
      {
         if (mNeqPrev==I_0 && aIeq==I_0)
         {
            Mout << "aIeq= " << aIeq << " mNeqPrev= " << mNeqPrev 
               <<   " RHS vec. is the starting guess." << endl;
            solvecs[I_0] = rhsvecs[I_0];
         }
         else if (aIeq<mNeqPrev)
         {
           //Mout << " Size of mpSolvecO[aIeq]: " << mpSolvecO[aIeq].Size() << endl;
           //Mout << " Norm of mpSolvecO[aIeq]: " << mpSolvecO[aIeq].Norm() << endl;
            Mout << "aIeq= " << aIeq << " mNeqPrev= " << mNeqPrev
               << " Previous sol. vec. is the starting guess." << endl;
            if (mpSolvecO[aIeq].Norm()!=C_0)// werner NEW
               solvecs[I_0] = mpSolvecO[aIeq];
            else
            {
               Mout << " The norm of mpSolvecO[aIeq] was zero. Let's use Sol. vec. #0." << endl;
               solvecs[I_0] = mpSolvecO[I_0];
            }
         }
         else if (aIeq>=mNeqPrev)
         {
            Mout << "aIeq= " << aIeq << " mNeqPrev= " << mNeqPrev
               << " Sol. vec. #0 is the starting guess." << endl;
            solvecs[I_0] = mpSolvecO[I_0];
         } 
      }
      //Mout << " Start vector: " << solvecs[i_eq] << endl;
      Mout << " Norm of start vector: " << solvecs[I_0].Norm() << endl;
      //solvecs[i_eq] = rhsvecs[i_eq];
      //solvecs[i_eq].Zero();
      //solvecs[i_eq].SaveUponDecon();
      //Mout << " ImprovedPrecond: solvecs vector label   " << solvecs[i_eq].Label() << endl;

      eqsol.SetRestart(true);

      // Get diagonal cheeply and without too many problems 
      string diag_name = mDiagonal.Label();
      mDiagonal.DumpToDisc(diag_name);
      if (mIoLevel>I_5) Mout << " mDiagonal, dumped to disc " << mDiagonal << endl;
      if (mIoLevel>I_5) Mout << " mDiagonal Size() = " << mDiagonal.Size() << std::endl;
      if (mIoLevel>I_5) Mout << " diagonal filename : " << diag_name << std::endl;
      eqsol.GetDiagonalFromFile(diag_name);
      MidasVector eig_vals(I_1,arNb);
      //Mout << " werner -- eig_vals: " << arNb << endl;
      if (eqsol.Solve(eig_vals,solvecs,rhsvecs))
      {
         Mout << " ImprovedPrecond micro eq. converged to requested threshold " << endl;
      }
      //Mout << " werner -- solvecs vector    " << solvecs[I_0] << endl;
      if (mIoLevel>I_5) Mout << " Norm of solution vector: " << solvecs[I_0].Norm() << endl;
      //if (aI==I_0) mpSolvecD[I_0] = solvecs[I_0];
      if (aI==I_1)
      {
         mpSolvecO[aIeq] = solvecs[I_0];
         Mout << " Solvec has been calculated at aIeq = " << aIeq << endl;
      }
      solvecs[I_0].Scale(-C_1);
      arDc.DirProd(mDiagonal,arNb,-I_1); 
      arDc.Reassign(solvecs[I_0],I_0,I_1,C_0,nvecsize);
      
      Mout << " Solution vector copied to new increment vector \n\n" << endl;
      //Mout << " Transformed vector arDc " << arDc << endl;
      In n_micro_now = eqsol.Iter();
      mNmicro += n_micro_now;
      Mout << " Nr of Micro iterations in this macro " << n_micro_now 
         << " and accumulated " << mNmicro << endl;


      /*
                  string storage_mode = "OnDisc";
                  if (mStorageLevel<=I_1) storage_mode = "InMem"; 
                  DataCont tmp1(mDim+I_1,C_0,storage_mode,"pd_tmp1"); 
                  DataCont tmp2(mDim+I_1,C_0,storage_mode,"pd_tmp2"); 
                  tmp1.SetToUnitVec(mPreDiagAdd[i]+I_1);
                  Nb shift = C_0;
                  (mpCalcClass->*mpPreDiagTransFunc)(tmp1,tmp2,I_2,I_1,shift);  
      */
      mpSmallTransf->SetSetTwoModeTrans(original_twomode);
      mpSmallTransf->SetSetOneModeTrans(original_onemode);
   }
}
/**
* Take (mDiagonal+arNb)-1 vec, 
* perhaps with a prediagonalization
* */
void ItEqSol::GetDiagonalFromFile(string aS)
{
   if (mIoLevel>I_5) Mout << " GetDiagonalFromFile mDim: " << mDim << std::endl;
   mDiagonal.GetFromExistingOnDisc(mDim,aS);
   //Mout << " mDiagonal, direct from disc " << mDiagonal << endl;
   //Mout << " label :        " << mDiagonal.Label() << endl;
   string storage_mode = "OnDisc";
   if (mStorageLevel<=I_1) storage_mode = "InMem"; 
   mDiagonal.ChangeStorageTo(storage_mode);
   //Mout << " GetDiagonalFromFile Diagonal storage mode: " << mDiagonal.Storage() << endl;
   string diag_lab = mFileLab + "_diagonal";
   mDiagonal.NewLabel(diag_lab,true,true);
   //Mout << " GetDiagonalFromFile Diagonal label:        " << mDiagonal.Label() << endl;
   mDiagonal.SaveUponDecon();
}


/**
* Compute the residual vector 
* */
void ItEqSol::ResidualVector
   (  const In& aIEq
   ,  MidasVector& arRedEigVal
   ,  vector<DataCont>& apSolVec
   ,  vector<DataCont>& apRhs
   ,  bool aBreakNow
   ,  const bool arNormTest
   )
{
   Nb c_norm_one_thr  = C_I_10_8;

   // Compute the Ritz vector called apSolVec 
   apSolVec[aIEq].Zero();
   for (In i = I_0; i < mRedDim; i++)
   {
      apSolVec[aIEq].Axpy(mpTrials[i], mRedSolVec[i][aIEq]);
   }

   if ((arNormTest && mIoLevel > I_10) || (mEigEq && fabs(apSolVec[aIEq].Norm() - C_1) < C_NB_EPSILON*C_10_2))
   {
      Mout << " Eq: " << setw(2) << aIEq << " Solutions vector(C=C'b) Norm: " 
           << apSolVec[aIEq].Norm();
      if (mEigEq)
      {
         Mout << " (should be one)";
      }
      Mout << std::endl; 
   }
   
   // Compute the residual vector for both Re and Im 
   if (mEigEq)
   {
      if ((fabs(apSolVec[aIEq].Norm() - C_1) > c_norm_one_thr) && arNormTest)
      {
         aBreakNow = true; // Trigger throw away and break in next iteration
         MIDASERROR("Norm of solution vector deviates significantly from 1. Better be stopped!"); 
         Mout << "Norm of solution vector " << apSolVec[aIEq].Norm() 
              << " deviates significantly from " << C_1 << std::endl;
      }
      mpResidualVecs[aIEq] = apSolVec[aIEq];
     
      if (mHarmonicRR)
      {
         mpResidualVecs[aIEq].Scale(-arRedEigVal[aIEq] + mEnerShift);
      }
      else
      {
         mpResidualVecs[aIEq].Scale(-arRedEigVal[aIEq]);
      }
   }
   else if (mLinEq)
   {
      mpResidualVecs[aIEq] = apRhs[aIEq];
      mpResidualVecs[aIEq].Scale(-C_1);

      if (fabs(arRedEigVal[aIEq]) > C_0)
      {
         mpResidualVecs[aIEq].Axpy(apSolVec[aIEq], -arRedEigVal[aIEq]);
      }
   }

   for (In i = I_0; i < mRedDim; i++)
   {
      mpResidualVecs[aIEq].Axpy(mpTransTrials[i], mRedSolVec[i][aIEq]);
   }
   
   if (mIoLevel > I_6)
   {
      Mout << " Sum of all ResidualVector elements = " << mpResidualVecs[aIEq].SumAbsEle() << std::endl;
   }
}

/**
* Calculate the real part of dot product between two complex dataconts.
* */
Nb ItEqSol::ComplexDotRe(DataCont& arDcReA,DataCont& arDcImA,DataCont& arDcReB,DataCont& arDcImB)
{
   Nb val;
   val=Dot(arDcReA,arDcReB);
   val+=Dot(arDcImA,arDcImB);
   return val;
}
/**
* Calculate the imag part of dot product between two complex dataconts.
* */
Nb ItEqSol::ComplexDotIm(DataCont& arDcReA,DataCont& arDcImA,DataCont& arDcReB,DataCont& arDcImB)
{
   Nb val;
   val=Dot(arDcReA,arDcImB);
   val-=Dot(arDcImA,arDcReB);
   return val;
}
/**
* Construct (new) elements of the reduced space matrix
* */
void ItEqSol::ConstructReducedMat
   (  vector<DataCont>& arLeftVecs
   ,  vector<DataCont>& arRightVecs
   ,  MidasMatrix& arRedMat
   ) 
{
   for (In i_new1=mRedDim-mNnew;i_new1<mRedDim;i_new1++)
   {
      for (In i_new2=I_0;i_new2<mRedDim;i_new2++)
      {
         arRedMat[i_new1][i_new2] = Dot(arLeftVecs[i_new1],arRightVecs[i_new2]);
      }
   }
   for (In i_new1=I_0;i_new1<mRedDim-mNnew;i_new1++)
   {
      for (In i_new2=mRedDim-mNnew;i_new2<mRedDim;i_new2++)
      {
         arRedMat[i_new1][i_new2] = Dot(arLeftVecs[i_new1],arRightVecs[i_new2]);
      }
   }
}
