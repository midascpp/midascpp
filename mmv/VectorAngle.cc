/**
 *******************************************************************************
 * 
 * @file    VectorAngle.cc
 * @date    18-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "mmv/VectorAngle.h"

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

#include "mmv/VectorAngle_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_VECTORANGLE(VEC_TMPL,T) \
   template midas::type_traits::RealTypeT<T> VectorAngle<false> \
      (  const VEC_TMPL<T>& arU \
      ,  const VEC_TMPL<T>& arV \
      ,  Uin aBeg \
      ,  Uin aEnd \
      ); \
   template midas::type_traits::RealTypeT<T> VectorAngle<true> \
      (  const VEC_TMPL<T>& arU \
      ,  const VEC_TMPL<T>& arV \
      ,  Uin aBeg \
      ,  Uin aEnd \
      ); \


// Instantiations.
INSTANTIATE_VECTORANGLE(GeneralMidasVector,float);
INSTANTIATE_VECTORANGLE(GeneralMidasVector,double);
INSTANTIATE_VECTORANGLE(GeneralMidasVector,long double);
INSTANTIATE_VECTORANGLE(GeneralMidasVector,std::complex<float>);
INSTANTIATE_VECTORANGLE(GeneralMidasVector,std::complex<double>);
INSTANTIATE_VECTORANGLE(GeneralMidasVector,std::complex<long double>);
INSTANTIATE_VECTORANGLE(GeneralDataCont,float);
INSTANTIATE_VECTORANGLE(GeneralDataCont,double);
INSTANTIATE_VECTORANGLE(GeneralDataCont,long double);
INSTANTIATE_VECTORANGLE(GeneralDataCont,std::complex<float>);
INSTANTIATE_VECTORANGLE(GeneralDataCont,std::complex<double>);
INSTANTIATE_VECTORANGLE(GeneralDataCont,std::complex<long double>);

#undef INSTANTIATE_VECTORANGLE

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
