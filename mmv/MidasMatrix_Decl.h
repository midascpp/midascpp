/**
************************************************************************
* 
* @file                MidasMatrix_Decl.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Midas Matrix class 
* 
* Last modified: Tue May 12, 2009  10:51AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASMATRIX_DECL_H_INCLUDED
#define MIDASMATRIX_DECL_H_INCLUDED

// Standard headers
#include <iostream> 
#include <string>
#include <complex>

// Libmda headers
#include "libmda/interface.h"
#include "libmda/expr/interface.h"
#include "libmda/util/index_check.h"
#include "libmda/util/dimensions_check.h"
#include "libmda/meta/std_wrappers.h"
#include "libmda/util/Requesting.h"
#include "libmda/IMDA.h"

// Midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "mmv_traits.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/ILapackUtils.h"
#include "util/type_traits/Complex.h"

// Forward declarations
template<class T> class GeneralMidasMatrix;
template<class T> class GeneralMidasVector;
template<class T> std::ostream& operator<<(std::ostream&,const GeneralMidasMatrix<T>&);
template<class T> GeneralMidasMatrix<T> Transpose(const GeneralMidasMatrix<T>&);
template<class T> void SqrtInvert(GeneralMidasMatrix<T>&);
template<class T> typename GeneralMidasMatrix<T>::value_type InvertSymmetric(GeneralMidasMatrix<T>&);
template<class T> typename GeneralMidasMatrix<T>::value_type InvertGeneral(GeneralMidasMatrix<T>&, const ilapack::util::RoutineName = ilapack::util::RoutineName::GESDD);
template<class T> typename GeneralMidasMatrix<T>::value_type Invert(GeneralMidasMatrix<T>&);
template<class T> T Dot(const GeneralMidasMatrix<T>&, const GeneralMidasMatrix<T>&);
template<class T> void HadamardProduct(GeneralMidasMatrix<T>&, const GeneralMidasMatrix<T>&);

// Index check policy for debug and production build
#ifdef MDEBUG
using matrix_dimensions_check = libmda::util::dimensions_check_stacktrace;
using matrix_index_check = libmda::util::index_check_stack_trace;
#else
using matrix_dimensions_check = libmda::util::dimensions_nocheck;
using matrix_index_check = libmda::util::index_nocheck;
#endif

// Class declaration
template<class T>
class GeneralMidasMatrix
   : public libmda::expression_interface< GeneralMidasMatrix<T>
                                        , traits<GeneralMidasMatrix<T> >
                                        , matrix_dimensions_check
                                        >
{
   public:
      typedef GeneralMidasMatrix<T> type;
      typedef typename traits<type>::value_type value_type; // T
      typedef typename traits<type>::size_type  size_type;  // In
      static const int order = traits<type>::order;         // 2
      using real_t = midas::type_traits::RealTypeT<T>;

   private:
      using libmda_interface = libmda::expression_interface< GeneralMidasMatrix<T>
                                                           , traits<GeneralMidasMatrix<T> >
                                                           , matrix_dimensions_check 
                                                           >;
      //! Index check policy
      using index_check_policy = matrix_index_check;

      In mNrows;          ///< Number of rows
      In mNcols;          ///< Number of columns
      T** mElements;      ///< Actual elements
      std::string mLabel; ///< Label for debugging
      
      void Allocate();    ///< Allocate matrix
      void Deallocate();  ///< Deallocate matrix

      //! Implementation for wrapping all of Is[Anti][Symmetric|Hermitian]().
      template<class F>
      bool IsSymmetricImpl(real_t aThr, const F& arFunc) const;

   public:
      /******************************************************************//**
      * @name Constructors, destructors and copy/move assignment
      **********************************************************************/
      //!@{
      //! Constructor and initialize from number
      GeneralMidasMatrix(const In aNrows, const In aNcols, const T aX);    
      
      //! Constructor only
      GeneralMidasMatrix(const In aNrows, const In aNcols);                
      
      //! Constructor only for square matrix
      explicit GeneralMidasMatrix(const In aNrows = 0);
      
      //! Constructor only for square matrix
      GeneralMidasMatrix(const In aNrows, const T aX);               
      
      //! Copy constructor
      GeneralMidasMatrix(const GeneralMidasMatrix<T>&);                  
      
      //! Move constructor
      GeneralMidasMatrix(GeneralMidasMatrix<T>&&);
      
      //! Construct diagonal matrix from a vector.
      GeneralMidasMatrix(const In, const GeneralMidasVector<T>&);        
      
      //! Construction from libmda expression
      template<class A, libmda::Require_order<A,2> = 0>
      GeneralMidasMatrix(const libmda::expr::expression_base<A>& aOther);

      //! Destructor
      ~GeneralMidasMatrix();

      //! Copy assignment
      GeneralMidasMatrix<T>& operator=(const GeneralMidasMatrix<T>&);

      //! Move assignment
      GeneralMidasMatrix<T>& operator=(GeneralMidasMatrix<T>&&);
      //!@}      


      
      /******************************************************************//**
      * @name Basic queries
      **********************************************************************/
      //!@{
      bool IsSquare() const;                                          ///< Is the matrix square?
      bool IsSymmetric(real_t arThr = C_NB_EPSILON*C_10_2) const;     ///< Is matrix symmetric?
      bool IsAntiSymmetric(real_t arThr = C_NB_EPSILON*C_10_2) const; ///< Is matrix anti-symmetric? 
      bool IsHermitian(real_t arThr = C_NB_EPSILON*C_10_2) const;     ///< Is matrix Hermitian?
      bool IsAntiHermitian(real_t arThr = C_NB_EPSILON*C_10_2) const; ///< Is matrix anti-Hermitian? 
      In Nrows() const; ///< Return number of rows
      In Ncols() const; ///< Return number of cols
      //!@}
        
      
      /******************************************************************//**
      * @name Libmda interface
      **********************************************************************/
      //!@{
      value_type&       at(const size_type aI, const size_type aJ);
      const value_type& at(const size_type aI, const size_type aJ) const;

      using libmda_interface::operator(); // For char expression interface
      using libmda_interface::operator=;  // To make assignable from libmda expressions
      
      size_type size() const;

      template<int N, libmda::iEnable_if<N==0> = 0>
      size_type extent() const;

      template<int N, libmda::iEnable_if<N==1> = 0>
      size_type extent() const;
      //!@}

      
      /******************************************************************//**
      * @name Change size and manage data
      **********************************************************************/
      //!@{
      T** GetElements() { return mElements; } ///< Get pointer to elements
      void SetNewSize(const In& aNewLength, const bool aSaveOldData = true, const bool aSetNewZero = false); ///< Redefine size of square matrix 
      void SetNewSize(const In& aNewRows, const In& aNewCols, const bool aSaveOldData = true, const bool aSetNewZero = false); ///< Redefine size of non square matrix 
      void SetPointerToNull();                                   ///< Set mElements to null pointer (use with caution!)
      void SetData(T** aElements, In aNrows, In aNcols);         ///< Set mElements to pointer (use with caution!)
      void StealData(GeneralMidasMatrix<T>& aVictimMat);         ///< Steal data from another matrix (use with caution!)
      void CopyDataFromPtr(T**, int, int, ilapack::col_major_t); ///< Copy data from pointer (column major)
      void CopyDataFromPtr(T**, int, int, ilapack::row_major_t); ///< Copy data from pointer (row major)
      void Clear();                                              ///< Clear matrix (leaves a 0-by-0 matrix)
      void SetLabel(std::string aLabel) { mLabel = aLabel; }     ///< Set label
      std::string GetLabel() { return mLabel; }                  ///< Get label
      //!@}

      
      /******************************************************************//**
      * @name Scale, shift, normalize, symmetrize, transpose etc.
      **********************************************************************/
      //!@{
      void Scale(const T& aX);                   ///< Scale entire matrix by aX
      void ScaleRow(const T& aX,const In& aRow); ///< Scale a row of the matrix by aX
      void ScaleCol(const T& aX,const In& aCol); ///< Scale a col of the matrix by aX
      void ShiftRowIndexToZero(const In& arIdx); ///< Change an index to be at zero
      void ShiftColIndexToZero(const In& arIdx); ///< Change an index to be at zero
      void ShiftIndexToZero(const In& arIdx);    ///< Change an index (row and col) to be at zero 
      void NormalizeColumns(); ///< Normalize all cols to unit norm
      void NormalizeRows();    ///< Normalize all rows to unit norm
      void OrthogLowdin();     ///< Orthonormalize columns using Lowdin approach 
      void Symmetrize();       ///< Symmetrize the matrix in-place
      GeneralMidasMatrix<T> Conjugate() const; ///< Conjugate transpose (Hermitian transpose)
      void ConjugateElems();                   ///< Conjugate elements without transposition
      void Transpose();                        ///< Transpose matrix in-place
      //!@}

      /******************************************************************//**
      * @name Calculate norms, sizes, deviations, trace etc.
      **********************************************************************/
      //!@{
      real_t NormRow(const In& aRow) const;  ///< Frobenius norm (2-norm) of a row 
      real_t NormCol(const In& aCol) const;  ///< Frobenius norm (2-norm) of a column
      real_t MetricNormCol(const In& aCol, const GeneralMidasMatrix<T>& aMetric) const; ///< CTSC norm of a column
      T      SumEle() const;    ///< Sum of all elements
      real_t SumAbsEle() const; ///< Sum of absolute values of all elements
      real_t Norm2() const;     ///< Sum of absolute squares of all elements
      real_t Norm() const;      ///< Frobenius norm, i.e square root of Norm2()
      T      Trace() const;     ///< Trace of square matrix (sum of diagonal elements)
      real_t Norm2Block(Uin aRowBeg, Uin aRowEnd, Uin aColBeg, Uin aColEnd) const; ///< Sum of absolute squares of elements in block (end-indices not included)
      T      TraceProduct(const GeneralMidasMatrix<T>& aM) const; ///< Trace of a matrix product
      real_t Norm2OutOfDiagonal() const;       ///< Sum of absolute squares of out-of-diagonal elements
      real_t NormOutOfDiagonal() const;        ///< Square root of Norm2OutOfDiagonal()
      T      SumOutOfDiagonal() const;         ///< Sum of out-of-diagonal elements 
      real_t SumAbsOutOfDiagonal() const;      ///< Sum of absolute values of out-of-diagonal elements 
      real_t SumAbsOutOfDiagonalUpper() const; ///< Sum of absolute values of upper-triangular out-of-diagonal elements.
      real_t Norm2Diagonal() const;            ///< Sum of absolute squares of diagonal elements
      real_t NormDiagonal() const;             ///< Square root of Norm2Diagonal()
      T      LargestElement() const;           ///< Find largest values of an element
      real_t LargestAbsElement() const;        ///< Find pargest absolute value of an element
      real_t DifferenceNorm(const GeneralMidasMatrix<T>&) const; ///< Frobenius norm of the difference between two matrices (scaled by 1/this.Norm())
      real_t DevFromSymmetric() const;         ///< Max. abs. deviation from symmetric matrix
      real_t DevFromAntiSymmetric() const;     ///< Max. abs. deviation from antisymmetric matrix
      //!@}


      /*********************************************************************************************//**
      * @name Getting/assigning/swapping rows, columns and blocks (use stride and offsets with caution!)
      *************************************************************************************************/
      //!@{
      GeneralMidasVector<T> GetDiag() const; ///< Get diagonal elements as vector
      void MakeDiagonal();                   ///< Set out-of diagonal elements to zero
      void SetDiagToNb(T aX = C_1);          ///< Set diagonal elements to number
      void Unit();                           ///< Set matrix to unit matrix
      void Zero();                 ///< Zero entire matrix
      void ZeroCol(In aCol);       ///< Zero column of matrix 
      void ZeroRow(In aRow);       ///< Zero row of matrix 
      void ZeroExceptCol(In aCol); ///< Zero all column except one
      void ZeroExceptRow(In aRow); ///< Zero all rows except one

      //! Copy the first elements of aM into this
      void Copy(const GeneralMidasMatrix<T>& aM, In aNewRow = -I_1, In aNewCol = -I_1);  
      
      //! Reassign this to the first elements of aM
      void Reassign(const GeneralMidasMatrix<T>& aM, In aNewRow=-I_1, In aNewCol = -I_1); 

      //! Arrange a vector in a matrix (default row by row)
      void MatrixFromVector(const GeneralMidasVector<T>& arV, bool aRowbyRow = true); 

      //! Reconstruct a full banded matrix from non-zero elements
      void FullBandMatrix(GeneralMidasMatrix<T>& arM);                                

      //! Copy row into vector
      void GetRow(GeneralMidasVector<T>& arV, In aIrow, In aIoff = I_0, In aIstride = I_1) const;
      
      //! Copy column into vector
      void GetCol(GeneralMidasVector<T>& arV, In aIcol, In aIoff = I_0, In aIstride = I_1) const;
      
      //! Copy offset column into vector
      void GetOffsetCol(GeneralMidasVector<T>& arV, In aIcol, In aIColOff = I_0) const;
      
      //! Copy offset row into vector
      void GetOffsetRow(GeneralMidasVector<T>& arV, In aIrow, In aIRowOff = I_0) const;
      
      //! Copy sub-column into vector
      void GetSubCol(GeneralMidasVector<T>& arV, const In aIcol, const In aStart, const In aEnd);
      
      //! Copy vector into row
      void AssignRow(const GeneralMidasVector<T>& arV, In aIrow, In aIoff = I_0, In aIstride = I_1);
      
      //! Add vector to row
      void AddToRow(const GeneralMidasVector<T>& arV, In aIrow, In aVecOffset = I_0, In aVecStride = I_1, In aMatOffset = I_0, In aMatStride = I_1);
      
      //! Copy vector into column
      void AssignCol(const GeneralMidasVector<T>& arV, In aICol, In aIoff = I_0, In aIstride = I_1, In aIMatOffset = I_0);
      
      //! Add vector to column
      void AddToCol(const GeneralMidasVector<T>& arV, In aICol, In aVecOffset = I_0, In aVecStride = I_1, In aMatOffset = I_0, In aMatStride = I_1);
      
      //! Assign this to a submatrix of arM (this is smaller than arM)
      void AssignToSubMatrix(const GeneralMidasMatrix<T>& arM,In aIrowStart,In aNrows,In aIcolStart, In aNcols);

      //! Assign a submatrix of this to arM (this is larger than arM)
      void AssignSubMatrixToMatrix(const GeneralMidasMatrix<T>& arM, In aRowStart, In aNrows, In aColStart, In aNcols);
      
      //! Add arM to a submatrix of this (this is larger than arM)
      void AddMatrixToSubMatrix(const GeneralMidasMatrix<T>& arM, In aRowStart, In aNrows, In aColStart, In aNcols);

      //! Convert vector to matrix (row major)
      GeneralMidasMatrix<T> ConvertVecToMat(const GeneralMidasVector<T>& arVec, In aNrows, In aNcols);

      //! Swap two columns of matrix
      void SwapCols(In aIcol1, In aIcol2);
      //!@}


      /******************************************************************//**
      * @name Friends
      **********************************************************************/
      //!@{
      friend T Dot<T>(const GeneralMidasMatrix<T>&,const GeneralMidasMatrix<T>&);       ///< Inner product
      friend std::ostream& operator<< <T>(std::ostream&, const GeneralMidasMatrix<T>&); ///< Output overload
      friend value_type InvertSymmetric<T>(GeneralMidasMatrix<T>&); ///< Invert a symmetric real matrix
      friend value_type InvertGeneral<T>(GeneralMidasMatrix<T>&, const ilapack::util::RoutineName);   ///< Invert a general real/complex matrix
      friend value_type Invert<T>(GeneralMidasMatrix<T>&);          ///< Invert a symmetric real or general real/complex matrix
      friend void SqrtInvert<T>(GeneralMidasMatrix<T>&);            ///< Inverse square root of general real matrix
      //!@}


      /*****************************************************************************//**
      * @name Misc.
      **********************************************************************************/
      //!@{
      inline T* operator[](In aI) const { index_check_policy::apply(aI,mNrows); return mElements[aI]; } ///< M[i][j]
      value_type&       operator()(const size_type aI, const size_type aJ)       { return at(aI,aJ); }  ///< M(i,j) (reference)
      const value_type& operator()(const size_type aI, const size_type aJ) const { return at(aI,aJ); }  ///< M(i,j) (const ref)
      
      //! Conjugate gradient solution of A*Sol=rhs to resid residual (to be better than aTol) in iter iterations (at most MaxIter).
      bool ConjGrad
         (  GeneralMidasVector<T>& arSol
         ,  const GeneralMidasVector<T>& arRhs
         ,  real_t& aResid
         ,  In& aIter
         ,  const real_t aTol = T(C_NB_EPSILON*C_10)
         ,  const In aMaxIter = I_10_2
         );

      //! Condition number as ||A^-1|| * ||A||. This function assumes symmetric matrix.
      real_t ConditionNumber() const;

      //! Condition number as s_max(A) / s_min(A).
      real_t ConditionNumberSVD() const;
      //!@}

};

//! Frobenius norm squared of the difference between two matrices
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffNorm2
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   );

//! Square root of DiffNorm2
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffNorm
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   );

//! Sum of absolute values of the elements in the matrix (arA - arB)
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffOneNorm
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   );

#endif // MIDASMATRIX_DECL_H_INCLUDED

