/**
************************************************************************
* 
* @file                Lanczos.cc
*
* Created:             23-12-2008
*
* Author:              Werner Gyorffy (werner@chem.au.dk)
*
* Short Description:   Implementing friend for solving linear equations
* 
* Last modified: Wed Aug 19, 2009  05:12PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordaaNce with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <string>
#include <algorithm>
#include <complex>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Plot.h"
#include "mmv/Diag.h"

// using declarations
using std::string;
using std::transform;
using std::complex;

// Numerical Recipes functions.
void NR_ludcmp(MidasMatrix &a, vector<In> &indx, Nb &d);
void NR_lubksb(MidasMatrix &a, vector<In> &indx, MidasVector &b);

extern "C" void zgtsv_(int *n, int *nrhs, complex<double> *dl, complex<double> *d, complex<double> *du,
      complex<double> *b, int *ldb, int *info);
extern "C" void zgbsv_(int *n, int *kl, int *ku, int *nrhs, complex<double> *ab, int *ldab, int *ipiv,
      complex<double> *b, int *ldb, int *info );
extern "C" void dstev_(char *jobz, int *n, double *d, double *e, double *z, int *ldz, double *work, int *info);

/**
* Solving Tx=B by Gaussian elimination with partial pivoting,
* where T is a general N-by-N tridiagonal matrix
* */
void TriDiagLinEqSol(const MidasVector& arDiag, const MidasVector& arSubDiag,
                     const MidasVector& arSuperDiag, const MidasVector& arRHS,
                     MidasVector& arSolVecRe,  MidasVector& arSolVecIm,
                     Nb aOmega, Nb aGamma)
{
   int N = arDiag.Size();
   int NRHS = I_1;
   int LDB = N;
   if (N!=LDB || arSubDiag.Size()!=N-I_1 ||  arSuperDiag.Size()!=N-I_1)
      MIDASERROR(" The vector dimensions do not correspond to an N-by-N matrix.");

   complex<double> *D = new complex<double>[N];
   complex<double> *B = new complex<double>[N]; // will be destroyed: RHS on entry, solution vector on exit.
   for(In i=I_0; i<N; i++)
   {
      D[i]=complex<double>(arDiag[i]-aOmega,-aGamma);
      B[i]=complex<double>(arRHS[i],C_0);
   }

   complex<double> *DL = new complex<double>[N-I_1];
   complex<double> *DU = new complex<double>[N-I_1];
   for(In i=I_0; i<N-I_1; i++)
   {
      DU[i]=complex<double>(arSuperDiag[i],C_0);
      //Mout << " DU[" << i << "] = " <<  DU[i] << endl;
      DL[i]=complex<double>(arSubDiag[i],C_0);
      //Mout << " DL[" << i << "] = " <<  DL[i] << endl;
   }
   int INFO = 0;

   //Mout << " Solving Tx=b equations by using ZGTSV " << endl;
   //Computes x of Tx=b linear system of equations 
   zgtsv_(&N,&NRHS,DL,D,DU,B,&LDB,&INFO);
   if(INFO > I_0)
      MIDASERROR(" Solving the equation system failed ... ");

   for (In i=I_0;i<N;i++)
   {
      // Mout << " B[" << i << "] = " <<  B[i] << endl;
      arSolVecRe[i]=B[i].real();
      arSolVecIm[i]=B[i].imag();
      //Mout << " solvec[" << i << "] = (" << arSolVecRe[i] << ") + i*(" << arSolVecIm[i]<< ")" << endl;
   }

   delete[] D;
   delete[] B;
   delete[] DL; 
   delete[] DU;
}
/**
* Solving AX=B by LU decomposition with partial pivoting,
* where A is a general N-by-N banded complex matrix
* of order N with KL subdiagonals and KU superdiagonals,
* and X and B are N-by-NRHS matrices. A=arMat is in band storage.
* */
void BandedLinEqSol(const MidasMatrix& arMat, const MidasVector& arRHS,
                     MidasVector& arSolVecRe,  MidasVector& arSolVecIm,
                     Nb aOmega, Nb aGamma)
{
   In bandwidth=(arMat.Nrows()-I_1)/I_2;
   int KL = bandwidth;
   int KU = bandwidth;
   int N = arMat.Ncols();
   int NRHS = I_1;
   int LDAB = N;

   //complex<double> omega(aOmega,aGamma); // complex shift
   complex<double> *AB = new complex<double>[(KL+KU+1)*N]; 
   In counter = I_0;
   for(In j=I_0; j<arMat.Ncols(); j++)
   {
      for(In i=I_0; i<arMat.Nrows(); i++)
      {
         if (i==j)
            AB[counter]=complex<double>(arMat[i][j]-aOmega,-aGamma);
         else
            AB[counter]=complex<double>(arMat[i][j]);
         counter++;
      }
   }
   int *IPIV = new int[N];
   complex<double> *B = new complex<double>[N];
   for(In i=I_0; i<N; i++)
      B[i]=complex<double>(arRHS[i]);

   int LDB = N;
   int INFO = 0;

   Mout << " Solving Ax=b equations by using ZGBSV " << endl;
   //Computes x of Ax=b linear system of equations 


   zgbsv_(&N,&KL,&KU,&NRHS,AB,&LDAB,IPIV,B,&LDB,&INFO);
   if (INFO > I_0)
      Mout << " The factorization has been completed,\n" 
         << " but the factor U is exactly singular,\n"
         << " so the solution could not be computed." << endl;
   else if (INFO < I_0)
   {
      Mout << " The " << -INFO << "-th argument had an illegal value" << endl;
      MIDASERROR(" Solving the equation system failed ... ");
      }

   for (In i=I_0;i<N;i++)
   {
      // Mout << " B[" << i << "] = " <<  B[i] << endl;
      arSolVecRe[i]=B[i].real();
      arSolVecIm[i]=B[i].imag();
      //Mout << " solvec[" << i << "] = (" << arSolVecRe[i] << ") + i*(" << arSolVecIm[i]<< ")" << endl;
   }

   delete[] AB;
   delete[] IPIV;
   delete[] B; 
    }
void DiagSymTridiag(const MidasVector& arDiag, const MidasVector& arSubDiag,
           const MidasVector& arSuperDiag, MidasVector& arEigVal, MidasMatrix& arEigVec)
{
   //Mout << " The method is DSTEV. " << endl;

   char JOBZ = 'V';
   int N = arDiag.Size();

   double *D = new double[N];
   double *E = new double[N-I_1];
   for(In i=I_0; i<N; i++)
      D[i]=arDiag[i];
   for(In i=I_0; i<N-I_1; i++)
      E[i]=arSubDiag[i];

   int LDZ = N;
   double *Z= new double[LDZ*N];
   double *WORK = new double[max(I_1,I_2*N-I_2)];
   int INFO = 0;

   //Computes the eigenvalues and the eigvectors of an N-by-N real symmetric matrix T
   dstev_(&JOBZ,&N,D,E,Z,&LDZ,WORK,&INFO);

   if(INFO != I_0)
      MIDASERROR(" Eigensolution failed ... ");

   for (In i=I_0;i<N;i++)
   {
      arEigVal[i]=D[i]; // in ascending order!
      //Mout << " eigval[" << i << "] = " <<  arEigVal[i]*C_AUTKAYS << endl;
   }
   // Z contains the orthonormal eigvecs
   In counter = I_0;
   for(In j=I_0; j<N; j++)
   {
      for(In i=I_0; i<N; i++)
      {
         arEigVec[i][j]=Z[counter];
         counter++;
      }
   }
   //arEigVec.NormalizeColumns();
   //aftertreat = true;

   delete[] D; 
   delete[] E;
   delete[] Z;
   delete[] WORK;

//#endif
}

void DiagT(const MidasVector& arDiag, const MidasVector& arSubDiag,
           const MidasVector& arSuperDiag, const In aNc)
{
   In dim = arDiag.Size();
   Mout << "dim: " << dim << endl;
   MidasMatrix T(dim, dim, C_0);
   for (In i=I_0; i<dim; ++i)
   {
      T(i,i) = arDiag[i];
      if (i != dim-I_1)
      {
         T(i,i+I_1) = arSuperDiag[i];
         T(i+I_1,i) = arSubDiag[i];
      }
   }

   MidasMatrix eigvec(dim);
   MidasVector eigval(dim);
   string dm="DGEEVX";
   Diag(T,eigvec, eigval, dm, true);
   //Mout << "T eigenvals: " << eigval*C_AUTKAYS << endl; // incorrect according to xlC!

   // Get Q matrix.
   //In aNc = mTrf->NexciXvec()-I_1;
   Mout << "aNc: " << aNc << endl;
   MidasMatrix Q(aNc, arDiag.Size(), C_0);// werner: arDiag.Size()==mChainLen
   for (In i=I_0; i<arDiag.Size(); ++i)
   {
      DataCont qj(0);
      ostringstream os;
      os << "laaNczos_q_" << i;
      qj.GetFromExistingOnDisc(aNc, os.str());
      qj.SaveUponDecon(true);
      Mout << "q=" << i << ": " << qj;
      MidasVector qjvec(aNc);
      qj.DataIo(IO_GET, qjvec, aNc);
      Mout << "q=" << i << ": " << qjvec;
      for (In j=I_0; j<aNc; ++j)
         Q(j,i) = qjvec[j];
   }
   Mout << "Q: " << Q;
   
   for (In i=I_0; i<arDiag.Size(); ++i)
   {
      Mout << "eigenval: " << eigval[i]*C_AUTKAYS << endl;
      MidasVector t_ev(dim);
      for (In j=I_0; j<dim; ++j)
         t_ev[j] = eigvec(j,i);
      Mout << "i=" << i << t_ev << endl;
      //Mout << "eigenvec: " << Q*t_ev << endl; // incorrect according to xlC!
   }
}
/*********************************************************************
 *                                                                   *
 * The below routines are stolen from Numerical Recipes to solve     *
 * linear equations.                                                 *
 *                                                                   *
 *********************************************************************/

void NR_ludcmp(MidasMatrix &a, vector<In> &indx, Nb &d)
{
   const Nb TINY=1.0e-20;
   In i,imax,j,k;
   imax=0;
   Nb big,dum,sum,temp;

   In n=a.Nrows();
   MidasVector vv(n);
   d=C_1;
   for (i=0;i<n;i++) {
      big=C_0;
      for (j=0;j<n;j++)
         if ((temp=fabs(a[i][j])) > big) big=temp;
      if (big == C_0) MIDASERROR("Singular matrix in routine ludcmp");
      vv[i]=C_1/big;
   }
   for (j=0;j<n;j++) {
      for (i=0;i<j;i++) {
         sum=a[i][j];
         for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
         a[i][j]=sum;
      }
      big=C_0;
      for (i=j;i<n;i++) {
         sum=a[i][j];
         for (k=0;k<j;k++) sum -= a[i][k]*a[k][j];
         a[i][j]=sum;
         if ((dum=vv[i]*fabs(sum)) >= big) {
            big=dum;
            imax=i;
         }
      }
      if (j != imax) {
         for (k=0;k<n;k++) {
            dum=a[imax][k];
            a[imax][k]=a[j][k];
            a[j][k]=dum;
         }
         d = -d;
         vv[imax]=vv[j];
      }
      indx[j]=imax;
      if (a[j][j] == C_0) a[j][j]=TINY;
      if (j != n-1) {
         dum=C_1/(a[j][j]);
         for (i=j+1;i<n;i++) a[i][j] *= dum;
      }
   }
}

// Stolen from NR.
void NR_lubksb(MidasMatrix &a, vector<In>& indx, MidasVector &b)
{
   In i,ii=0,ip,j;
   Nb sum;

   In n=a.Nrows();
   for (i=0;i<n;i++) {
      ip=indx[i]; // Ove: isnt this dangerous??
      sum=b[ip];
      b[ip]=b[i];
      if (ii != 0)
         for (j=ii-1;j<i;j++) sum -= a[i][j]*b[j];
      else if (sum != C_0)
         ii=i+1;
      b[i]=sum;
   }
   for (i=n-1;i>=0;i--) {
      sum=b[i];
      for (j=i+1;j<n;j++) sum -= a[i][j]*b[j];
      b[i]=sum/a[i][i];
   }
}
