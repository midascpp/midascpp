#include "StorageForSort.h"

#include <algorithm>
#include <vector>

AbsLessThanStorageForSort_t AbsLessThanStorageForSort;
LessThanStorageForSort_t LessThanStorageForSort;

namespace detail
{

/**
 * Sort eigenvalues with old positions saved in SortingArray.
 **/
void FindEigenvalueSortedOrder
   (  const MidasVector& arEigVals
   ,  bool aOrderAbsVals
   ,  std::vector<StorageForSort>& aSortingArray
   )
{
   aSortingArray.reserve(arEigVals.Size());
   for(In i = I_0; i < arEigVals.Size();i++)
   {
      aSortingArray.emplace_back(arEigVals[i],i);
   }
   
   if(aOrderAbsVals)
   {
      // we use stable sort to keep ordering of imaginary eigenvalue pairs
      std::stable_sort(aSortingArray.begin(), aSortingArray.end(), AbsLessThanStorageForSort);
   }
   else
   {
      std::stable_sort(aSortingArray.begin(), aSortingArray.end(), LessThanStorageForSort);
   }
}

/**
 * Sort first aNfirst eigenvalues with old positions saved in SortingArray
 **/
void FindEigenvalueSortedOrder
   (  const MidasVector& arEigVals
   ,  bool aOrderAbsVals
   ,  std::vector<StorageForSort>& aSortingArray
   ,  In aNfirst
   )
{
   aSortingArray.reserve(arEigVals.Size());
   for(In i = I_0; i < arEigVals.Size();i++)
   {
      aSortingArray.emplace_back(arEigVals[i],i);
   }
   
   //Sort only the first aNfirst eig.vals.
   In n = std::min(aNfirst, static_cast<In>(arEigVals.Size()));
   if(n < 0)   n = 0;

   if(aOrderAbsVals)
   {
      // we use stable sort to keep ordering of imaginary eigenvalue pairs
      std::stable_sort(aSortingArray.begin(), aSortingArray.begin() + n, AbsLessThanStorageForSort);
   }
   else
   {
      std::stable_sort(aSortingArray.begin(), aSortingArray.begin() + n, LessThanStorageForSort);
   }
}

/**
 * Sort range of eigenvalues with old positions saved in SortingArray
 **/
void FindEigenvalueSortedOrder
   (  const MidasVector& arEigVals
   ,  bool aOrderAbsVals
   ,  std::vector<StorageForSort>& aSortingArray
   ,  In aNbegin
   ,  In aNend
   )
{
   assert(aNbegin <= aNend);

   aSortingArray.reserve(arEigVals.Size());
   for(In i = I_0; i < arEigVals.Size();++i)
   {
      aSortingArray.emplace_back(arEigVals[i],i);
   }
   
   //Sort only the first aNfirst eig.vals.
   In n_end = std::min(aNend, static_cast<In>(arEigVals.Size()));
   if(n_end < 0)   n_end = 0;
   In n_begin = std::max(aNbegin, I_0);

   if (  aOrderAbsVals
      )
   {
      // we use stable sort to keep ordering of imaginary eigenvalue pairs
      std::stable_sort(aSortingArray.begin() + n_begin, aSortingArray.begin() + n_end, AbsLessThanStorageForSort);
   }
   else
   {
      std::stable_sort(aSortingArray.begin() + n_begin, aSortingArray.begin() + n_end, LessThanStorageForSort);
   }
}

} /* namespace detail */

/**
 * Sort  eigenvalue and eigenvectors
 **/
void Sort3
   (  MidasVector& arEigVals
   ,  MidasMatrix& arEigVecs
   ,  bool aOrderAbsVals
   )
{
   std::vector<StorageForSort> sortingarray;
   detail::FindEigenvalueSortedOrder(arEigVals, aOrderAbsVals, sortingarray);
   
   // temporaries
   MidasVector temp_eigvals(arEigVals.Size());
   MidasMatrix temp_eigvecs(arEigVecs.Nrows(),C_0);
   
   // sort eigenvalues and vectors according to "sortingarray"
   for(In i = I_0; i < sortingarray.size(); ++i)
   {
      //Eigenvalues:
      temp_eigvals[i] = arEigVals[sortingarray[i].GetPoss()];

      //Eigenvectors:
      MidasVector TempStorage(arEigVecs.Nrows());
      arEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss(),I_0,I_1);
      temp_eigvecs.AssignCol(TempStorage,i,I_0,I_1);
   }
   
   // save results
   arEigVals = temp_eigvals;
   arEigVecs = temp_eigvecs;
}

/**
 * Sort eigenvalue and eigenvectors with imag eigvals and left hand eigenvectors
 **/
void Sort3
   (  MidasVector& arEigVals
   ,  MidasMatrix& arEigVecs
   ,  MidasVector& arImEigVals
   ,  MidasMatrix& arLEigVecs
   ,  bool aOrderAbsVals
   )
{
   std::vector<StorageForSort> sortingarray;
   detail::FindEigenvalueSortedOrder(arEigVals,aOrderAbsVals,sortingarray);
   
   // temporaries
   MidasVector temp_eigvals(arEigVals.Size());
   MidasVector temp_im_eigvals(arImEigVals.Size());
   MidasMatrix temp_eigvecs(arEigVecs.Nrows(),C_0);
   MidasMatrix temp_leigvecs(arLEigVecs.Nrows(),C_0);
   
   // sort eigenvalues and vectors according to "sortingarray"
   for(In i = I_0; i < sortingarray.size(); ++i)
   {
      temp_eigvals[i] = arEigVals[sortingarray[i].GetPoss()];
      temp_im_eigvals[i] = arImEigVals[sortingarray[i].GetPoss()];
      MidasVector TempStorage(arEigVecs.Nrows());
      
      // we do not specifically take care of imag part of eigvec as the position from the second eigval pair
      // will put this in the correct position
      arEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss(),I_0,I_1);
      temp_eigvecs.AssignCol(TempStorage,i,I_0,I_1);
      
      // if we have left eigvecs we sort them as well
      if(temp_leigvecs.Nrows() != I_0)
      {
         arLEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss());
         temp_leigvecs.AssignCol(TempStorage, i,I_0,I_1);
      }
   }
   
   // We scale the imag part of lhs eigenvectors with -1 (complex conjugation of left-hand eigvals).
   // (is this a good idea to do in a sorting algorithm...??)
   for(In i = I_0; i<sortingarray.size(); ++i)
   {
      if(temp_im_eigvals[i] != C_0)
      {
         temp_leigvecs.ScaleCol(-C_1,i+I_1);
         ++i;
      }
   }
   
   // save results
   arEigVals = temp_eigvals;
   arImEigVals = temp_im_eigvals;
   arEigVecs = temp_eigvecs;
   arLEigVecs = temp_leigvecs;
}

/**
 * Sort the aNfirst eigenvalues and eigenvectors
 **/
void Sort3
   (  MidasVector& arEigVals
   ,  MidasMatrix& arEigVecs
   ,  In aNfirst
   ,  bool aOrderAbsVals
   )
{
   In n = std::min(aNfirst, static_cast<In>(arEigVals.Size()));
   std::vector<StorageForSort> sortingarray;
   detail::FindEigenvalueSortedOrder(arEigVals, aOrderAbsVals, sortingarray, aNfirst);
   
   // temporaries
   MidasVector temp_eigvals(arEigVals.Size());
   MidasMatrix temp_eigvecs(arEigVecs.Nrows(),C_0);
   
   // sort eigenvalues and vectors according to "sortingarray"
   for(In i = I_0; i < sortingarray.size(); ++i)
   {
      //Eigenvalues:
      temp_eigvals[i] = arEigVals[sortingarray[i].GetPoss()];

      //Eigenvectors:
      MidasVector TempStorage(arEigVecs.Nrows());
      arEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss(),I_0,I_1);
      temp_eigvecs.AssignCol(TempStorage,i,I_0,I_1);
   }
   
   // save results
   arEigVals = temp_eigvals;
   arEigVecs = temp_eigvecs;
}

/**
 * Sort range of eig val/vec pairs.
 **/
void Sort3
   (  MidasVector& arEigVals
   ,  MidasMatrix& arEigVecs
   ,  MidasVector& arImEigVals
   ,  MidasMatrix& arLEigVecs
   ,  In aNbegin
   ,  In aNend
   ,  bool aOrderAbsVals
   )
{
   std::vector<StorageForSort> sortingarray;
   detail::FindEigenvalueSortedOrder(arEigVals,aOrderAbsVals,sortingarray, aNbegin, aNend);
   
   // temporaries
   MidasVector temp_eigvals(arEigVals.Size());
   MidasVector temp_im_eigvals(arImEigVals.Size());
   MidasMatrix temp_eigvecs(arEigVecs.Nrows(),C_0);
   MidasMatrix temp_leigvecs(arLEigVecs.Nrows(),C_0);
   
   // sort eigenvalues and vectors according to "sortingarray"
   for(In i = I_0; i < sortingarray.size(); ++i)
   {
      temp_eigvals[i] = arEigVals[sortingarray[i].GetPoss()];
      temp_im_eigvals[i] = arImEigVals[sortingarray[i].GetPoss()];
      MidasVector TempStorage(arEigVecs.Nrows());
      
      // we do not specifically take care of imag part of eigvec as the position from the second eigval pair
      // will put this in the correct position
      arEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss(),I_0,I_1);
      temp_eigvecs.AssignCol(TempStorage, i, I_0, I_1);
      
      // if we have left eigvecs we sort them as well
      if(temp_leigvecs.Nrows() != I_0)
      {
         arLEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss());
         temp_leigvecs.AssignCol(TempStorage, i, I_0, I_1);
      }
   }
   
   // We scale the imag part of lhs eigenvectors with -1
   // (is this a good idea to do in a sorting algorithm...??)
   for(In i = I_0; i<sortingarray.size(); ++i)
   {
      if(temp_im_eigvals[i] != C_0)
      {
         temp_leigvecs.ScaleCol(-C_1,i+I_1);
         ++i;
      }
   }
   
   // save results
   arEigVals = temp_eigvals;
   arImEigVals = temp_im_eigvals;
   arEigVecs = temp_eigvecs;
   arLEigVecs = temp_leigvecs;
}

/**
 * Sort the first aNfirst eigenvalue and eigenvectors with imag eigvals and left hand eigenvectors
 **/
void Sort3
   (  MidasVector& arEigVals
   ,  MidasMatrix& arEigVecs
   ,  MidasVector& arImEigVals
   ,  MidasMatrix& arLEigVecs
   ,  In aNfirst
   ,  bool aOrderAbsVals
   )
{
   Sort3(arEigVals, arEigVecs, arImEigVals, arLEigVecs, I_0, aNfirst, aOrderAbsVals);
//   std::vector<StorageForSort> sortingarray;
//   detail::FindEigenvalueSortedOrder(arEigVals,aOrderAbsVals,sortingarray, aNfirst);
//   
//   // temporaries
//   MidasVector temp_eigvals(arEigVals.Size());
//   MidasVector temp_im_eigvals(arImEigVals.Size());
//   MidasMatrix temp_eigvecs(arEigVecs.Nrows(),C_0);
//   MidasMatrix temp_leigvecs(arLEigVecs.Nrows(),C_0);
//   
//   // sort eigenvalues and vectors according to "sortingarray"
//   for(In i = I_0; i < sortingarray.size(); ++i)
//   {
//      temp_eigvals[i] = arEigVals[sortingarray[i].GetPoss()];
//      temp_im_eigvals[i] = arImEigVals[sortingarray[i].GetPoss()];
//      MidasVector TempStorage(arEigVecs.Nrows());
//      
//      // we do not specifically take care of imag part of eigvec as the position from the second eigval pair
//      // will put this in the correct position
//      arEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss(),I_0,I_1);
//      temp_eigvecs.AssignCol(TempStorage,i,I_0,I_1);
//      
//      // if we have left eigvecs we sort them as well
//      if(temp_leigvecs.Nrows() != I_0)
//      {
//         arLEigVecs.GetCol(TempStorage, sortingarray[i].GetPoss());
//         temp_leigvecs.AssignCol(TempStorage, i,I_0,I_1);
//      }
//   }
//   
//   // We scale the imag part of lhs eigenvectors with -1
//   // (is this a good idea to do in a sorting algorithm...??)
//   for(In i = I_0; i<sortingarray.size(); ++i)
//   {
//      if(temp_im_eigvals[i] != C_0)
//      {
//         temp_leigvecs.ScaleCol(-C_1,i+I_1);
//         ++i;
//      }
//   }
//   
//   // save results
//   arEigVals = temp_eigvals;
//   arImEigVals = temp_im_eigvals;
//   arEigVecs = temp_eigvecs;
//   arLEigVecs = temp_leigvecs;
}

