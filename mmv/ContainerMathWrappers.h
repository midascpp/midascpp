/**
************************************************************************
* 
* @file    ContainerMathWrappers.h
*
* @date    17-12-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Wrappers for various math operations on containers
*
* @note
*     These can also be extended for template template types to call themselves 
*     "recursively", e.g. calculate norm for vector<vector<Nb>> without implementing 
*     a Norm function. I have an implementation of that somewhere, but let's see if it becomes necessary.
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CONTAINERMATHWRAPPERS_H_INCLUDED
#define CONTAINERMATHWRAPPERS_H_INCLUDED

#include <type_traits>
#include <complex>
#include <numeric>
#include <algorithm>
#include <functional>

#include "inc_gen/TypeDefs.h"
#include "util/IsDetected.h"
#include "util/StaticLoop.h"
#include "util/type_traits/Tuple.h"
#include "util/Math.h"
#include "util/Error.h"
#include "util/AbsVal.h"

namespace midas::mmv
{
namespace detail
{
//! For detection
template<typename T> using has_zero_member_type = decltype(std::declval<T>().Zero());
template<typename T> using has_zero_func_type = decltype(Zero(std::declval<T&>()));
template<typename T> using has_dot_member_type = decltype(std::declval<T>().Dot(std::declval<T>()));
template<typename T> using has_dot_func_type = decltype(Dot(std::declval<T>(), std::declval<T>()));
template<typename T, typename U> using has_axpy_member_type = decltype(std::declval<T>().Axpy(std::declval<const T&>(), std::declval<U>()));
template<typename T, typename U> using has_axpy_func_type = decltype(Axpy(std::declval<T&>(), std::declval<const T&>(), std::declval<U>()));
template<typename T> using has_norm2_member_type = decltype(std::declval<T>().Norm2());
template<typename T> using has_norm2_func_type = decltype(Norm2(std::declval<T>()));
template<typename T> using has_inplace_multiplication_type = decltype(std::declval<T>()*=std::declval<T>());
template<typename T> using has_hadamard_product_type = decltype(std::declval<T>().HadamardProduct(std::declval<T>()));
template<typename T> using has_hadamard_product_func_type = decltype(HadamardProduct(std::declval<T&>(), std::declval<T>()));
template<typename T, typename U> using has_scale_member_type = decltype(std::declval<T>().Scale(std::declval<U>()));
template<typename T, typename U> using has_scale_func_type = decltype(Scale(std::declval<T&>(),std::declval<U>()));
} /* namespace detail */

/**
 * Zero a number
 *
 * @param arVec
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr void WrapZero
   (  T& arVec
   )
{
   arVec = T(0.0);
}

/**
 * Wrap Zero function
 *
 * @param arVec      Vector to be zeroed
 **/
template
   <  typename T
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapZero
   (  T& arVec
   )
{
   // For tuples (including pair, tuple, and array)
   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      midas::util::StaticFor<0, std::tuple_size_v<T>>::Loop
         (  [&arVec](auto I)
            {
               WrapZero(std::get<I>(arVec));
            }
         );
   }
   else
   {
      [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_zero_member_type, T>;
      constexpr bool has_func = midas::util::IsDetectedV<detail::has_zero_func_type, T>;

      if constexpr (  has_func
                   )
      {
         Zero(arVec);
      }
      else if constexpr (  has_member
                        )
      {
         arVec.Zero();
      }
      else
      {
         using std::begin; using std::end;
         std::for_each(begin(arVec), end(arVec), [](auto& v) { WrapZero(v); });
      }
   }
}

/**
 * Scale a number
 *
 * @param arVec
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   ,  std::enable_if_t<std::is_floating_point_v<U> || midas::type_traits::IsComplexV<U>>* = nullptr
   >
inline constexpr void WrapScale
   (  T& arVec
   ,  U aScalar
   )
{
   arVec *= aScalar;
}

/**
 * Wrap Scale function
 *
 * @param arVec      Vector to be scaled
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   ,  std::enable_if_t<std::is_floating_point_v<U> || midas::type_traits::IsComplexV<U>>* = nullptr
   >
void WrapScale
   (  T& arVec
   ,  U aScalar
   )
{
   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      midas::util::StaticFor<0, std::tuple_size_v<T>>::Loop
         (  [&arVec,aScalar](auto I)
            {
               WrapScale(std::get<I>(arVec), aScalar);
            }
         );
   }
   else
   {
      [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_scale_member_type, T, U>;
      constexpr bool has_func = midas::util::IsDetectedV<detail::has_scale_func_type, T, U>;

      if constexpr (  has_func
                   )
      {
         Scale(arVec, aScalar);
      }
      else if constexpr (  has_member
                        )
      {
         arVec.Scale(aScalar);
      }
      else
      {
         using std::begin; using std::end;
         std::for_each(begin(arVec), end(arVec), [&aScalar](auto& v) { WrapScale(v, aScalar); });
      }
   }
}

/**
 * Wrap Dot function
 *
 * @param aLeft
 * @param aRight
 * @return
 *    <aLeft|aRight>
 **/
template
   <  typename T
   >
inline constexpr auto WrapDotProduct
   (  const T& aLeft
   ,  const T& aRight
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   )
{
   return midas::math::Conj(aLeft) * aRight;
}

/**
 * Wrap Dot function
 *
 * @param aLeft
 * @param aRight
 * @return
 *    <aLeft|aRight>
 **/
template
   <  typename T
   >
auto WrapDotProduct
   (  const T& aLeft
   ,  const T& aRight
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   )
{
   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      // Use return type of first element
      using dot_t = decltype(WrapDotProduct(std::declval<std::tuple_element_t<0,T>>(), std::declval<std::tuple_element_t<0,T>>()));

      dot_t result = 0;

      midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
         (  [&result, &aLeft, &aRight](auto I)
            {
               using elem_type = std::tuple_element_t<I,T>;
               static_assert(std::is_same_v<dot_t, decltype(WrapDotProduct(std::declval<elem_type>(), std::declval<elem_type>()))>, "Tuple elements do not have same dot-product return types!");
               result += WrapDotProduct(std::get<I>(aLeft), std::get<I>(aRight));
            }
         );

      return result;
   }
   else
   {
      [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_dot_member_type, T>;
      constexpr bool has_func = midas::util::IsDetectedV<detail::has_dot_func_type, T>;

      if constexpr   (  has_func
                     )
      {
         return Dot(aLeft, aRight);
      }
      else if constexpr (  has_member
                        )
      {
         return aLeft.Dot(aRight);
      }
      else
      {
         // Allow ADL for custom begin/end
         using std::begin;
         using std::end;
         using value_t = typename std::iterator_traits<decltype(begin(aLeft))>::value_type;
         using ret_t = decltype(WrapDotProduct(std::declval<value_t>(), std::declval<value_t>()));
         return std::inner_product
            (  begin(aLeft)
            ,  end(aLeft)
            ,  begin(aRight)
            ,  ret_t(0)
            ,  std::plus<>()
            ,  [](const value_t& i, const value_t& j)
               {
                  return WrapDotProduct(i,j);
               }
            );
      }
   }
}

/**
 * Wrap norm2 function
 *
 * @param aVec
 * @return
 *    ||aVec||^2
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr auto WrapNorm2
   (  const T& aVec
   )
{
   return midas::util::AbsVal2(aVec);
}

/**
 * Wrap norm2 function
 *
 * @param aVec
 * @return
 *    ||aVec||^2
 **/
template
   <  typename T
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
auto WrapNorm2
   (  const T& aVec
   )
{
   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      using norm_t = decltype(WrapNorm2(std::declval<std::tuple_element_t<0,T>>()));

      norm_t norm2 = 0;

      midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
         (  [&norm2, &aVec](auto I)
            {
               using elem_type = std::tuple_element_t<I,T>;
               static_assert(std::is_same_v<norm_t, decltype(WrapNorm2(std::declval<elem_type>()))>, "Tuple elements do not have same norm return type!");
               norm2 += WrapNorm2(std::get<I>(aVec));
            }
         );

      return norm2;
   }
   else
   {
      [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_norm2_member_type, T>;
      constexpr bool has_func   = midas::util::IsDetectedV<detail::has_norm2_func_type, T>;

      if constexpr   (  has_func
                     )
      {
         return Norm2(aVec);
      }
      else if constexpr (  has_member
                        )
      {
         return aVec.Norm2();
      }
      else
      {
         // Allow ADL for custom begin/end
         using std::begin;
         using std::end;
         using value_t = typename std::iterator_traits<decltype(begin(aVec))>::value_type;
         using ret_t = decltype(WrapNorm2(std::declval<value_t>()));
         return std::accumulate
            (  begin(aVec)
            ,  end(aVec)
            ,  ret_t(0)
            ,  [](ret_t i, const value_t& j)
               {
                  return i + WrapNorm2(j);
               }
            );
      }
   }
}

/**
 * Wrap norm
 *
 * @param aVec
 * @return
 *    ||aVec||
 **/
template
   <  typename T
   >
auto WrapNorm
   (  const T& aVec
   )
{
   return std::sqrt(WrapNorm2(aVec));
}

/**
 * Wrap Axpy function
 *
 * @param arVec
 * @param aRight
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr void WrapAxpy
   (  T& arVec
   ,  const T& aRight
   ,  U aScalar
   )
{
   arVec += aScalar*aRight;
}

/**
 * Wrap Axpy function
 *
 * @param arVec
 * @param aRight
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapAxpy
   (  T& arVec
   ,  const T& aRight
   ,  U aScalar
   )
{
   static_assert(std::is_floating_point_v<U> || midas::type_traits::IsComplexV<U>);

   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
         (  [&arVec, &aRight, &aScalar](auto I)
            {
               WrapAxpy(std::get<I>(arVec), std::get<I>(aRight), aScalar);
            }
         );
   }
   else
   {
      [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_axpy_member_type, T, U>;
      constexpr bool has_func = midas::util::IsDetectedV<detail::has_axpy_func_type, T, U>;

      if constexpr   (  has_func
                     )
      {
         Axpy(arVec, aRight, aScalar);
      }
      else if constexpr (  has_member
                        )
      {
         arVec.Axpy(aRight, aScalar);
      }
      else
      {
         // Allow ADL for custom begin/end
         using std::begin;
         using std::end;

         auto left_it = begin(arVec);
         auto right_it = begin(aRight);
         auto e = end(arVec);
         for(; left_it != e; ++left_it, ++right_it)
         {
            WrapAxpy(*left_it, *right_it, aScalar);
         }
      }
   }
}

/**
 * Wrap HadamardProduct
 *
 * @param arVec
 * @param aRight
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr void WrapHadamardProduct
   (  T& arVec
   ,  const T& aRight
   )
{
   arVec *= aRight;
}

/**
 * Wrap HadamardProduct
 *
 * @param arVec
 * @param aRight
 **/
template
   <  typename T
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapHadamardProduct
   (  T& arVec
   ,  const T& aRight
   )
{
   if constexpr   (  midas::type_traits::IsTupleV<T>
                  )
   {
      midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
         (  [&arVec, &aRight](auto I)
            {
               WrapHadamardProduct(std::get<I>(arVec), std::get<I>(aRight));
            }
         );
   }
   else
   {
      [[maybe_unused]] constexpr bool has_oper = midas::util::IsDetectedV<detail::has_inplace_multiplication_type, T>;
      [[maybe_unused]] constexpr bool has_memb = midas::util::IsDetectedV<detail::has_hadamard_product_type, T>;
      constexpr bool has_func = midas::util::IsDetectedV<detail::has_hadamard_product_func_type, T>;

      if constexpr   (  has_func
                     )
      {
         HadamardProduct(arVec, aRight);
      }
      else if constexpr (  has_memb
                        )
      {
         arVec.HadamardProduct(aRight);
      }
      else if constexpr (  has_oper
                        )
      {
         arVec *= aRight;
      }
      else
      {
         // Allow ADL
         using std::begin;
         using std::end;
         using std::distance;

         auto v_it = begin(arVec);
         auto v_end = end(arVec);
         auto r_it = begin(aRight);
         auto r_end = end(aRight);
         auto size_v = distance(v_it, v_end);
         auto size_r = distance(r_it, r_end);
         if (  size_v != size_r
            )
         {
            MIDASERROR("WrapHadamardProduct: Vector sizes do not match (" + std::to_string(size_v) + " vs. " + std::to_string(size_r) + ")");
         }
         for(; v_it != v_end; ++v_it, ++r_it)
         {
            WrapHadamardProduct(*v_it, *r_it);
         }
      }
   }
}

} /* namespace midas::mmv */


#endif /* CONTAINERMATHWRAPPERS_H_INCLUDED */
