#ifndef MMVT_H_INCLUDED
#define MMVT_H_INCLUDED

#include "mmv/MidasVector.h"
#include "tensor/NiceTensor.h"
#include "vcc/TensorSumAccumulator.h"

namespace midas::vcc
{
template<typename> class TensorSumAccumulator;
}

/**
 *
 **/
template<class T>
void ReassignMidasVectorFromNiceTensor
   (  GeneralMidasVector<T>& aVec
   ,  const NiceTensor<T>& aTensor
   )
{
   NiceTensor<T> tensor_simple;
   bool already_simple = (aTensor.Type() == BaseTensor<T>::typeID::SIMPLE);
   if (  !already_simple
      )
   {
      tensor_simple = aTensor.ToSimpleTensor();
   }

   const auto& tensor = already_simple ? aTensor : tensor_simple;

   aVec.SetNewSize(tensor.TotalSize(), false);
   tensor.GetTensor()->DumpInto(aVec.data());
}

/**
 *
 **/
template
   <  typename T
   >
void ReassignMidasVectorFromNiceTensor
   (  GeneralMidasVector<T>& aVec
   ,  const midas::vcc::TensorSumAccumulator<T>& aTensorAcc
   )
{
   ReassignMidasVectorFromNiceTensor(aVec, aTensorAcc.Tensor());
}

#endif /* MMVT_H_INCLUDED */
