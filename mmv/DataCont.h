#ifndef DATACONT_H_INCLUDED
#define DATACONT_H_INCLUDED

// include declaration
#include "DataCont_Decl.h"

//#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "DataCont_Impl.h"
//#endif /* DISABLE_PRECOMPILED_TEMPLATES */

// 
using DataCont = GeneralDataCont<Nb>;
using ComplexDataCont = GeneralDataCont<std::complex<Nb>>;

//
#include "DataContUtil.h"

#endif /* DATACONT_H_INCLUDED */
