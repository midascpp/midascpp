/**
************************************************************************
* 
* @file                Diag.h
*
* Created:             02-05-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Midas Diag functions forward declaration
* 
* Last modified: Tue May 02, 2015  10:51AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DIAG_H_INCLUDED
#define DIAG_H_INCLUDED

// std headers
#include<string>
#include<vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

// forward declarations
template<class T> class GeneralMidasMatrix;
typedef GeneralMidasMatrix<Nb> MidasMatrix;
template<class T> class GeneralMidasVector;
typedef GeneralMidasVector<Nb> MidasVector;

/**
 *
 **/
void MidasSVD(MidasMatrix&,MidasVector&,MidasMatrix&);
void SvBksb(MidasMatrix& u, MidasVector& w, MidasMatrix& v,MidasVector& b, MidasVector& x, bool aTikhonov=false, const Nb aTikhonovParam=C_0);

/**
 *
 **/
void Diag(MidasMatrix& aM, 
          MidasMatrix& aEigVec,
          MidasVector& aEigVal, 
          std::string aHow, 
          std::vector<Nb>& arEigValReIm,
           MidasMatrix& arMetric, 
          bool aStoreEigVec=true, 
          bool aOrderAbsVals=false,
          In aSuperDiag=I_0);

/**
 *
 **/
void Diag(MidasMatrix& aM,
          MidasMatrix& aEigVec,
          MidasVector& aEigVal, 
          std::string aHow, 
          bool aStoreEigVec=true,
          bool aOrderAbsVals=false,
          In aSuperDiag=I_0);

/**
 *
 **/
void Diag(MidasMatrix& aM, 
          MidasMatrix& aEigVec,
          MidasVector& aEigVal, 
          MidasVector& aImEigVal,
          MidasMatrix& aLEigVal,
          std::string aHow, 
          std::vector<Nb>& aEigValReIm,
          MidasMatrix& aMetric, 
          bool aStoreEigVec=true,
          bool aOrderAbsVals=false,
          In aSuperDiag=I_0);

/**
 *
 **/
void Jacobi(MidasMatrix& aM, MidasVector& aEigVal,MidasMatrix& aEigVec, In& aNrot);
void nr_jacobi(MidasMatrix& aM,MidasVector& aEigVal,MidasMatrix& aEigVec, In& aNrot);
void JacobiRot(MidasMatrix& aM, const Nb aS, const Nb aTau,const In aI, const In aJ, const In aK, const In aL);
void svdcmp(MidasMatrix& a, MidasVector& w, MidasMatrix& v);
void LinEqSol(MidasMatrix& arM, MidasMatrix& arSol,MidasMatrix& arRhs, In aNrhs, std::string aHow,MidasVector& arResid, In& aIter, 
               const Nb aTol= C_NB_EPSILON*C_10,const In aMaxIter = I_10_2);

template<class T> void SvBksb(GeneralMidasMatrix<T>& u, GeneralMidasVector<T>& w, GeneralMidasMatrix<T>& v,GeneralMidasVector<T>& b, 
                            GeneralMidasVector<T>& x, bool aTikhonov=false, const Nb aTikhonovParam=C_0)
                              {static_assert(!std::is_same<T,Nb>::value, "Error SvBksb not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void Diag(GeneralMidasMatrix<T>& aM, GeneralMidasMatrix<T>& aEigVec,GeneralMidasVector<T>& aEigVal, 
                            std::string aHow, std::vector<T>& arEigValReIm,GeneralMidasMatrix<T>& arMetric, bool aStoreEigVec=true, 
                            bool aOrderAbsVals=false, In aSuperDiag=I_0) 
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void Diag(GeneralMidasMatrix<T>& aM,GeneralMidasMatrix<T>& aEigVec,GeneralMidasVector<T>& aEigVal, std::string aHow, 
                            bool aStoreEigVec=true, bool aOrderAbsVals=false, In aSuperDiag=I_0)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void Diag(GeneralMidasMatrix<T>& aM, GeneralMidasMatrix<T>& aEigVec,GeneralMidasVector<T>& aEigVal, 
                            GeneralMidasVector<T>& aImEigVal,GeneralMidasMatrix<T>& aLEigVal,std::string aHow, std::vector<T>& aEigValReIm,
                            GeneralMidasMatrix<T>& aMetric, bool aStoreEigVec=true, bool aOrderAbsVals=false, In aSuperDiag=I_0)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void Jacobi(GeneralMidasMatrix<T>& aM, GeneralMidasVector<T>& aEigVal,GeneralMidasMatrix<T>& aEigVec, In& aNrot)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void nr_jacobi(GeneralMidasMatrix<T>& aM,GeneralMidasVector<T>& aEigVal,GeneralMidasMatrix<T>& aEigVec, In& aNrot)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void JacobiRot(GeneralMidasMatrix<T>& aM, const T aS, const T aTau,const In aI, const In aJ, const In aK, const In aL)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void svdcmp(GeneralMidasMatrix<T>& a, GeneralMidasVector<T>& w, GeneralMidasMatrix<T>& v)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}
template<class T> void LinEqSol(GeneralMidasMatrix<T>& arM, GeneralMidasMatrix<T>& arSol,GeneralMidasMatrix<T>& arRhs, In aNrhs, 
                                std::string aHow,GeneralMidasVector<T>& arResid, In& aIter,const T aTol= T(C_NB_EPSILON*C_10),const In aMaxIter = I_10_2)
                              {static_assert(std::is_same<T,Nb>::value, "Error SvBksb  not defined for GeneralMidas-structs, only for Nb types");}

#endif /* DIAG_H_INCLUDED */
