#ifndef MMV_TRAITS_H
#define MMV_TRAITS_H

#include "inc_gen/TypeDefs.h"
#include "libmda/interface.h"
#include "mmv/midas_matrix_oper_mult.h"
using midas::mmv::midas_matrix_oper_mult;

template<typename T>
class GeneralMidasMatrix;

template<typename T>
class GeneralMidasVector;

template<typename T>
struct traits<GeneralMidasMatrix<T> >
{
   typedef T                     value_type;
   typedef In                    size_type;
   typedef GeneralMidasMatrix<T> type;
   static const int order = 2;
};

template<typename T>
struct traits<GeneralMidasVector<T> >
{
   typedef T                     value_type;
   typedef In                    size_type;
   typedef GeneralMidasVector<T> type;
   static const int order = 1;
};

// not pretty but it works...
namespace libmda
{

template<typename T>
struct operator_traits<GeneralMidasMatrix<T> > : default_operator_traits
{
   template<class L, class R>
   using oper_mult = midas_matrix_oper_mult<L,R>;
};

template<typename T>
struct operator_traits<GeneralMidasVector<T> > : 
   //operator_traits<GeneralMidasMatrix<T> >
   default_operator_traits
{ 
   //using operator_traits<GeneralMidasMatrix<T> >::oper_mult;
   template<class L, class R>
   using oper_mult = midas_matrix_oper_mult<L,R>;
};

} // namespace libmda

#endif /* MMV_TRAITS_H */
