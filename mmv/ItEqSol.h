/**
************************************************************************
* 
* @file                ItEqSol.h
*
* Created:             21-01-2003
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Iterative Equation Solver, linear & eigenvalue.
* 
e Last modified: Sun Mar 29, 2009  06:39AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ITEQSOL_H
#define ITEQSOL_H

// std headers
#include <iostream> 
#include <string> 
#include <vector> 
#include <map> 
#include <algorithm> 

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "util/Timer.h"
#include "MidasMatrix.h"
#include "MidasVector.h"
#include "DataCont.h"
#include "mmv/Transformer.h"
#include "vcc/VccTransformer.h"

// using declarations
using std::ostream;
using std::string;
using std::vector;
using std::map;
using std::count;
using std::find;
using std::sort;

typedef void (*Ptvfna)(); // Pointer to void function with no args. 
typedef void (*PtrFn)(DataCont&,DataCont&,In,In,Nb);        
///< Pointer To Func void
typedef void (*PtrFnVoid1In)(In);           
///< Pointer To Func void, 1 In arg
typedef void (*PtrFnVoid1In1Nb)(In,Nb);     
///< Pointer To Func void, 1 In, 1Nb

/** 
*  A Midas class for solving equations and eigenvalue equations
*  by iterative techniques. 
*  Linear and eigenvalue equations by reduced space methods.
* */
class ItEqSol
{
   private:
      In mNeq;         ///< Current number of equations to be solved (nr of eig)
      In mDim;         ///< Current dimension of actual space 
      In mNiterMax;    ///< Maximum iteration number 
      In mRedDimMax;   ///< Maximum dim of reduced space
      In mNbreakDim;   ///< Dim for which we break and restart 
      bool mLinEq;     ///< True -> Linear equations
      bool mEigEq;     ///< True -> EigenValue equations
      bool mNonLinEq;  ///< True -> Non-linear equations
      bool mRestart;   ///< Restart the iterative calculation
      bool mDiis;      ///< Diis extrapolation.
      bool mRedDimMaxSet; ///< Has RedDimMax been set and activated!
      bool mPaired;     ///< Paired (Vscf-rsp) structure of eq. 
      In mMaxDiis;     ///< Max nr. of vectors in Diis extrapol

      In mNiter;       ///< Current iteration number 
      In mNnew;        ///< Number of new trial vectors;
      In mRedDim;      ///< Current dimension of reduced space
      
      //! Working analysis directory
      std::string mAnalysisDir; 
      
      //! Storage level for vectors 0:all in memory
      In mStorageLevel; // 1: trial&trans on disc, 2:diag also on disc, 3: everything on disc.
      
      //! Io level 
      In mIoLevel;     

      vector<DataCont> mpTrials;        ///< The trial vectors 
      vector<DataCont> mpTransTrials;   ///< The transformed trial vectors
      vector<DataCont> mpResidualVecs;  ///< DataCont with the residual vectors in 
      DataCont mDiagonal;        ///< DataCont with the diagonal of H
      Transformer* mpTrf;       ///< The transformer object.
      VccTransformer* mpSmallTransf;///< Pointer to the small Vcc transformer

      MidasMatrix  mRedHam;      ///< The reduced Hamiltonian
      MidasMatrix  mRedSolVec;   ///< The eigen vectors in the reduced space
      MidasVector  mResidNorms;  ///< The eigven values in the reduced space
      MidasMatrix  mRedRhs;      ///< The reduced right hand sides in a matrix

      string mDiagMeth;          ///< Diagonalization method
      Nb mResidThreshold;        ///< Threshold for norm of residual
      Nb mResidThresholdRel;     ///< Threshold, relative, for norm of residual
      In mRescueMax;             ///< Max number of vectors to be rescued
      Nb mEnerPrev;              ///< Energy in prev. it for mNonLineq
      Nb mEnerThreshold;         ///< Threshold for energy change
      Nb mMaxStep;               ///< Maximum size of step. 
      Nb mTrustUp;               ///< Trust Up scale factor 
      Nb mTrustDown;             ///< Trust Up scale factor 
      Nb mMinTrustStep;          ///< Trust Up scale factor 

      In mNmicro;      ///< Sum of total nr of micro iterations. 
      In mNmicroMax;   ///< Maximum nr of micro iterations in solving one set of equations 
      Nb mMicroResFac; ///< Thr factor for micro iterations/Relative (to rhs) threshold 

      bool mPreDiag;             ///< Use prediagonalization methods
      bool mImprovedPrecond;          ///< Use improved preconditioner in Olsen
      string mLevel2Solver;          ///< Linear equation solver method in impr. prec.
      bool mSecOrd;              ///< Use Second order method
      bool mLinSearch;           ///< Perform linear search along NR step if uncesseful.
      In   mNpreDiag;            ///< Dim of exp. diag. space 
      In   mPrecondExciLevel;     ///< Max. number of mode couplings included in the Hamiltonian
      vector<In> mPreDiagAdd;    ///< Address of Prediag vector components.
      MidasMatrix mPreDiagMat;   ///< Prediagonalizing matrix
      DataCont* mpSolvecD;       ///< Davidson solution vec
      //vector<DataCont> mpSolvecD;       ////< David.solution vec
      vector<DataCont> mpSolvecO;       ////< Olsen solution vec

      bool mThickRestart;           ///< restart with k best approximate eigenvecs + k others having eigvals near
      In mNresvecs;                 ///< Number of restart vectors

      bool mTimeIt;              ///< Time the iterations
      bool mAllowBackStep;       ///< Allow Back stepping
      bool mAllowScaleStep;      ///< Allow scaling of large steps 
      bool mAllowTrustScale;     ///< Allow scaling accurding to trust factor 
     
      bool mTargetSpace;         ///< Find roots corresponding to target states.
      In mNtargs;                ///< The number of target states
      string mTargetFilNamBas;   ///< The basis of the target state file names. 
      In mNimportantVecs;        ///< The number of solution vectors which have large overlaps with the target states.
      In mImag;                  ///< The number complex conjugate pairs which should be considered
      vector<Nb> mReImEigVal;
      MidasMatrix mTarTriS;      ///< The overlap mat between target and trial vectors
      //MidasMatrix mTarSolS;    ///< The overlap mat between target and sol. vectors
      MidasMatrix  mRedMetric;   ///< The reduced Metric matrix (B) for Harmonic RR
      string mTargetingMethod;
      Nb mOverlapMin;
      Nb mOverlapSumMin;
      Nb mEnergyDiffMax;
      Nb mResidThrForOthers;
      Nb mEnerThrForOthers;
      In mLevelEq;             ///< The level of the equation solver at which we are solving and equation set
      bool mVccvsVciPrec;    ///< Equations are to obtain Vcc vectors instead of Vci vectors: C_ref^vcc=1, C_ref^vci=variable.
      string mFileLab;           ///< Label for files 

      bool mOlsen;               ///< Use Olsen algorithm?
      bool mTrueHDiag;           ///< Use true H diagonal transformer
      bool mTrueADiag;           ///< Use true A diagonal transformer
      In mNeqPrev;               ///< In the case of improved precond. How many previous solvec are stored.
      string mSolVecFilNamBas;   ///< The basis of the solution vector file names. 
      bool mHarmonicRR;          ///< Use Harmonic Rayleigh-Ritz procedure to solve EigVal problems
      Nb mEnerShift;             ///< Energy shift (sigma) used for (H-sigma) in Harmonic RR
      
      void SetNonLinEq(bool aB) { mNonLinEq = aB; }
   public:
      
      //! constructor
      ItEqSol(const In& aVecStorageLevel, const std::string& aAnalysisDir);

      //! deconstructor 
      ~ItEqSol();

      bool Solve(MidasVector& arRedEigVal,vector<DataCont>& apSolVecs, 
            vector<DataCont>& apRhs);           
      bool Solve(MidasVector& arRedEigVal,vector<DataCont>& apSolVecs);           
      ///< Solve the equations, Fo is a function object for the function 
      // that carrious out the transformations.  
      void SetRedDimMax(const In& arRedDimMax) {mRedDimMax = arRedDimMax; 
         if (!mRedDimMaxSet) AllocRedDimMax(); else Mout << "RedDimMax has already been set, ignored this time" << endl; 
         mRedDimMaxSet = true;}
      void AllocRedDimMax(); // Allocation of vectors 
      void SetNiterMax(const In& arMax) {mNiterMax= arMax;}    
      void SetMicroMax(const In& arMax) {mNmicroMax = arMax;}    
      void SetMicroResFac(const Nb& arFac) {mMicroResFac= arFac;}    
      void SetBreakDim(const In& arMax) {mNbreakDim= arMax;}  
      void SetResidThreshold(const Nb& arThr) {mResidThreshold= arThr;}
      void SetResidThresholdRel(const Nb& arThr) {mResidThresholdRel= arThr;}
      void SetRescueMax(const In& arMax) {mRescueMax= arMax;}
      void SetEnerThreshold(const Nb& arThr) {mEnerThreshold= arThr;}
      void SetMaxStep(const Nb& arMaxStep) {mMaxStep= arMaxStep;}
      void SetLinEq()    {mEigEq = false; mLinEq = true;  SetNonLinEq(false); } //mNonLinEq = false;} 
      void SetEigEq()    {mEigEq = true;  mLinEq = false; SetNonLinEq(false); } //mNonLinEq = false;}
      void SetNonLinEq() {mEigEq = false; mLinEq = false; SetNonLinEq(true);  } //mNonLinEq = true;}
      void SetNeq(const In& arN) {mNeq = arN;}
      void SetDim(const In& arDim) {mDim = arDim;}
      //void SetPreDiagTransformer(Fptr  apF) {mpPreDiagTransFunc = apF;}
      //void SetPreparePreDiagTrans(Fptr2 apF) {mpPreparePreDiagTrans = apF;}
      //void SetRestoreAfterPreDiagTrans(Fptr2 apF) {mpRestoreAfterPreDiagTrans = apF;}
      //void SetTransformer(Fptr  apF) {mpTransFunc = apF;}
      void SetTransformer(Transformer* apTrf) {mpTrf = apTrf;}
      //void SetCalcClass(CalcClass* apC) {mpCalcClass = apC;}
      void SetDiagMeth(const string aS) {mDiagMeth = aS;}
      void SetRestart(const bool aRestart) {mRestart = aRestart;}
      void SetIoLevel(const In aIoLevel) {mIoLevel = aIoLevel;}
      void SetDiis(const bool aD) {mDiis = aD;}
      void SetMaxDiis(const In aM) {mMaxDiis = aM;}
      void SetPreDiag(const bool aM) {mPreDiag = aM;}
      void SetSecOrd(const bool aM) {mSecOrd= aM;}
      void SetLinSearch(const bool aM) {mLinSearch= aM;}
      void SetNpreDiag(const In aM) {mNpreDiag = aM;}
      void SetTimeIt(bool aTimeIt) {mTimeIt = aTimeIt;}
      void SetAllowScaleStep(bool aB=true) {mAllowBackStep = aB;}
      void SetAllowTrustScale(bool aB=true) {mAllowTrustScale = aB;}
      void SetAllowBackStep(bool aB=true) {mAllowScaleStep = aB;}
      void SetPaired(bool aPaired) {mPaired = aPaired;}
      void SetIncreaseLevelEq() {mLevelEq += I_1; mFileLab="ItEqSol" + std::to_string(mLevelEq);}
      void SetVccvsVciPrec(const bool aM) {mVccvsVciPrec=aM;} 
      void SetTrustUp(const Nb& arThr) {mTrustUp = arThr;}
      void SetTrustDown(const Nb& arThr) {mTrustDown = arThr;}
      void SetMinTrustStep(const Nb& arThr) {mMinTrustStep= arThr;}
      void SetOlsen(bool aOlsen=true) {mOlsen=aOlsen;} 
      void SetTrueHDiag(const bool aM) {mTrueHDiag=aM;} 
      void SetTrueADiag(const bool aM) {mTrueADiag=aM;} 
      void SetImprovedPrecond(bool aM) {mImprovedPrecond = aM;}
      void SetPrecondExciLevel(const In aM) {mPrecondExciLevel = aM;}
      void SetLevel2Solver(string aS) {mLevel2Solver = aS;}
      void SetNresvecs(const In aM) {mNresvecs = aM;}
      void SetHarmonicRR(const bool& arB){mHarmonicRR = arB;}
      void SetEnerShift(const Nb& arNb){mEnerShift = arNb;}

      // Member functions primarily for internal use 
      void StartGuess(MidasVector& arRedEigVal, 
            vector<DataCont>& apSolVecs,vector<DataCont>& apRhs);       ///< Create the start guess, 
      bool Orthogonalize(const In arNew,const In arOld,bool aLetSmallPass=false,Nb aRefSize=C_0,
           const In aRangeMin=I_0, const bool aDoNorm=true);

      void InverseMatTimesVec(DataCont& arVec, const Nb& arNb, In aI=I_0, In aIeq=I_0);
      void GetDiagonalFromFile(string aS);

      void SetTargetSpace(bool aB=true)      {mTargetSpace = aB;}    
      void SetNtargs(In aN=I_0)      {mNtargs = aN;}    
      void SetTargetFileName(string aS="TargetVector_") {mTargetFilNamBas = aS;}    
      In Iter(){return mNiter;} // Nr of iterations
      void SetTargetingMethod(const std::string& aS)
      {
         mTargetingMethod = aS;
      }    
      void SetOverlapMin(const Nb& arMin) {mOverlapMin= arMin;}  
      void SetOverlapSumMin(const Nb& arMin) {mOverlapSumMin= arMin;}  
      void SetEnergyDiffMax(const Nb& arE) {mEnergyDiffMax= arE;}  
      void SetResidThrForOthers(const Nb& arMin) {mResidThrForOthers= arMin;}  
      void SetEnerThrForOthers(const Nb& arMin) {mEnerThrForOthers= arMin;}  
 
      void MatchTargetStateComponentsForPreDiag(In& arNpre,vector<In>& arAdd,vector<Nb>& arVals); 
      void SetSolVecFileName(string aS) {mSolVecFilNamBas = aS;}    
      void ResidualVector(const In& aIeq, MidasVector& arRedEigVal, 
            vector<DataCont>& apSolVecs,vector<DataCont>& apRhs, 
            bool aBreakNow, const bool arNormTest=true);  
      ///< Compute the residual vector
      Nb ComplexDotRe(DataCont& arDcReA,DataCont& arDcImA,DataCont& arDcReB,DataCont& arDcImB);
      Nb ComplexDotIm(DataCont& arDcReA,DataCont& arDcImA,DataCont& arDcReB,DataCont& arDcImB);
      void ConstructReducedMat(vector<DataCont>& arLeftVecs,vector<DataCont>& arRightVecs,
            MidasMatrix& arRedMat); 
      ///< Create red mat 
};

#endif

