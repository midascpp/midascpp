/**
 *******************************************************************************
 * 
 * @file    VectorAngle_Impl.h
 * @date    18-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VECTORANGLE_IMPL_H_INCLUDED
#define VECTORANGLE_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "util/Error.h"
#include "util/AbsVal.h"
#include "util/Math.h"

/***************************************************************************//**
 * Computes the vector angle of vectors u and v, defined by
 * ```
 *    theta = acos |<u|v>|/(|u|*|v|)
 * ```
 * However, above formula is numerically unstable, especially for nearly
 * parallel vectors (and nearly is actually not very near, theta doesn't have
 * to be much below 1e-2 radians or so). 
 * This is because a lot of numerical precision is lost in computing the
 * Cauchy-Schwarz measure (as I like to call it), 
 * `CS(u,v) = |<u|v>|/(|u|*|v|)`, for near-parallel vectors, because it is
 * close to one, and then we take acos which gives something close to zero.
 *
 * I've come up with a more stable solution, which avoids the problem above.
 * For vectors u and v, introduce
 * ```
 *    <u|v> = |<u|v>| exp(i arg<u|v>) (polar form)
 *    u' = exp(i arg<u|v>) u / |u|
 *    v' = v / |v|
 * ```
 * Then
 * ```
 *    <u'|v'>  = exp(-i arg<u|v>)/(|u|*|v|) <u|v>
 *             = exp(-i arg<u|v>)/(|u|*|v|) |<u|v>| exp(i arg<u|v>)
 *             = |<u|v>|/(|u|*|v|)
 *             = CS(u,v)
 * ```
 * By construction, u', v' are normalized and <u'|v'> is real, so
 * ```
 *    |u'-v'|^2 = <u'-v'|u'-v'>
 *              = |u'| + |v'| - 2 Re<u'|v'>
 *              = 2(1 - <u'|v'>)
 * ```
 * Define `D = |u'-v'|`.
 * We have
 * ```
 *    CS(u,v)  = <u'|v'>
 *             = 1 - 1/2 |u'-v'|^2
 *             = 1 - 1/2 D^2
 * ```
 * So `theta = acos(1 - 1/2 D^2)`.
 * Now, do a Taylor series expansion of `acos(1 - 1/2 x^2)` around `x = 0`.
 * Look it up (look series for `acos(1-x)` up and substitute `x = 1/2 D^2`), or
 * calculate it yourself.
 * Note: acos'(1-x) is singular at x = 0, so no Taylor series for acos(1-x)
 * (but there is a series, it just involves half powers of x).
 * But acos(1-1/2 x^2) has no singularities, and thus has Taylor series, so
 * usual tools can be applied, although tedious. Well, at least if assuming `x
 * >= 0`; it has a kink at `x = 0`, but we don't care about `x < 0`.
 * Alternative derivation:
 * ```
 *    acos'(x)    = - 1/sqrt(1 - x^2)
 *    acos'(1-x)  = 1/sqrt(1 - (1-x)^2)
 *                = 1/sqrt(x(2 - x))
 *                = 1/sqrt(2x) (1 - 1/2 x)^(-1/2)
 * ```
 * The binomial theorem says
 * ```
 *    (1 + x)^(-1/2) = 1 - 1/2 x + 3/8 x^2 - 5/16 x^3 + 35/128 x^4 - 63/256 x^5 + ...
 * ```
 * Insert this, take the antiderivative, and collect `2x` factors to get
 * ```
 *    acos(1-x) = sqrt(2x) (1 + 1/24 2x + 3/640 (2x)^2 + 5/7168 (2x)^3 + 35/294912 (2x)^4 + ...)
 * ```
 * Insert `x = 1/2 D^2`, then
 * ```
 *    acos(1-1/2 D^2)
 *    = D (1 + 1/24 D^2 + 3/640 D^4 + 5/7168 D^6 + 35/294912 D^8 + ...)
 *    = D (1 + 1/24 D^2 (1 + 24*3/640 D^2 (1 + 640/3 5/7168 D^2))) + 35/294912 D^9 + ...
 *    = D (1 + 1/24 D^2 (1 + 9/80 D^2 (1 + 25/168 D^2))) + 35/294912 D^9 + ...
 *      |----------------------- T8(D) ----------------| + |-- R9(D) --|
 * ```
 * The residual term R9(D) is used to determine when to use Taylor expansion.
 * Use the T8(D), 8'th order Taylor expansion (8'th-order term is zero), if 
 * ```
 *    R9(D) <= eps * T8(D)    eps = machine epsilon for real_t
 * ```
 * and otherwise return `acos(1 - 1/2 D^2)`.
 * Note, if R9(D) is below machine precision then so are the rest of the
 * high-order terms as well.
 * The `acos(1 - 1/2 D^2)` is observed to be stable enough in the regime where
 * we'll end up applying it; approximate `T8(D) ~ D`, then we have
 * ```
 *    eps >~ R9(D)/D = 35/294912 D^8
 *    D   <~ ((294912/35)*eps)^(1/8)
 *    -->
 *    real_t = float;         eps = 1.19e-07;   D <~ 0.42183870023336567
 *    real_t = double;        eps = 2.22e-16;   D <~ 0.03419772155020818
 *    real_t = long double;   eps = 1.08e-19;   D <~ 0.013178956346378256
 * ```
 * NB! Not tested for `long double` at the moment, though. (MBH, Mar 2020)
 *
 * @note
 *    If one or both vectors are zero vectors, then return `acos(0) = PI/2`,
 *    signifying that the zero vector is orthogonal to any other vector,
 *    including itself.
 *    The empty vector (of dimension 0), is treated as a zero vector in this
 *    regard.
 *
 * @param[in] arU
 *    One vector, `u`. Must have same size as `v`.
 * @param[in] arV
 *    Another vector, `v`. Must have same size as `u`.
 * @param[in] aBeg
 *    If requested, compute angle of subvectors with indices in `[aBeg;aEnd)`.
 *    Default is entire vectors.
 * @param[in] aEnd
 *    If requested, compute angle of subvectors with indices in `[aBeg;aEnd)`.
 *    Default is entire vectors.
 * @return
 *    The vector angle;
 *    - `theta = acos(|<u|v>|/(|u|*|v|))`,   if `CONJ = false`
 *    - `theta = acos(|<u*|v>|/(|u*|*|v|))`, if `CONJ = true`
 *    where `u*` is `u` with all elements complex conjugated.
 ******************************************************************************/
template
   <  bool CONJ
   ,  template<typename...> class VEC_TMPL
   ,  typename T
   >
midas::type_traits::RealTypeT<T> VectorAngle
   (  const VEC_TMPL<T>& arU
   ,  const VEC_TMPL<T>& arV
   ,  Uin aBeg
   ,  Uin aEnd
   )
{
   if (Size(arU) != Size(arV))
   {
      std::stringstream ss;
      ss << "Size(arU) = " << Size(arU) << " != Size(arV) = " << Size(arV);
      MIDASERROR(ss.str());
   }

   aEnd = std::min(aEnd, Uin(Size(arU)));
   aBeg = std::min(aBeg, aEnd);
   const Uin n_slice = aEnd - aBeg;

   using real_t = midas::type_traits::RealTypeT<T>;
   const real_t n_u = sqrt(arU.Norm2(aBeg, n_slice));
   const real_t n_v = sqrt(arV.Norm2(aBeg, n_slice));

   if (n_u == real_t(0) || n_v == real_t(0))
   {
      return acos(real_t(0));
   }
   else
   {
      T dot = 0;
      if constexpr(CONJ)
      {
         // dot = <u*|v> = sum_i (u_i^*)^* v_i = sum_i u_i v_i
         // (one conj from u*, one from inner product)
         dot = SumProdElems(arU,arV,aBeg,aEnd);
      }
      else
      {
         // dot = <u|v> = sum_i u_i^* v_i  (regular inner product)
         dot = Dot(arU,arV,aBeg,n_slice);
      }
      T u_scale = 1./n_u;
      const T v_scale = 1./n_v;

      if constexpr(midas::type_traits::IsComplexV<T>)
      {
         if (std::imag(dot) == real_t(0))
         {
            u_scale *= std::real(dot) < real_t(0)? T(-1): T(1);
         }
         else
         {
            u_scale *= exp(T(0,std::arg(dot)));
         }
      }
      else
      {
         u_scale *= dot < 0? -1: 1;
      }

      const real_t dn2 = DiffNorm2Scaled<CONJ>(arU, arV, u_scale, v_scale, aBeg, n_slice);
      const real_t dn = sqrt(dn2);
      const real_t one_min_half_dn2 = (real_t(2) - dn2)/real_t(2);

      real_t taylor = 1.;
      taylor *= 25.*dn2/168.;
      taylor += 1.;
      taylor *= 9.*dn2/80.;
      taylor += 1.;
      taylor *= dn2/24.;
      taylor += 1.;
      taylor *= dn;
      const real_t residual = dn*35.*pow(dn2,4)/294912.;
      const real_t eps = std::numeric_limits<real_t>::epsilon();
      if (residual <= eps*taylor)
      {
         return taylor;
      }
      else
      {
         return acos(one_min_half_dn2);
      }
   }
}

#endif/*VECTORANGLE_IMPL_H_INCLUDED*/
