/**
************************************************************************
* 
* @file                DataCont_Impl.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing vector utilities for namespace DataCont.
* 
* Last modified: Mon Jan 18, 2010  01:53PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

// libmda headers
#include "libmda/numeric/float_eq.h"
#include "libmda/util/output_call_addr.h"

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "util/read_write_binary.h"
#include "util/FileSystem.h"
#include "util/AbsVal.h"
#include "mpi/Impi.h"

// forward declarations
extern In gMaxFileSize;
extern In gMaxBufSize;
extern MidasStream Mout;
extern void RmFile(string);
//extern void CopyFile(string,string);
//extern void MvFile(string,string);

/**
* Constructor from number
* */
template<class T>
GeneralDataCont<T>::GeneralDataCont
   ( const In aN
   , const T aX
   , const string& aStorage
   , const string& aFileLabel
   , bool aSaveUponDecon
   )
{
   string storage(aStorage);
   while(storage.find(" ")!= storage.npos) storage.erase(storage.find(" "),I_1); // Delete blanks
   transform(storage.begin(),storage.end(),storage.begin(),(In(*) (In))toupper);// All to uppercase
   mSaveUponDecon = aSaveUponDecon;
   if (storage == "INMEM") 
   {
      mAllInCore = true;
      mAllOnDisc = false;
   }
   else if (storage == "ONDISC") 
   {
      mAllInCore = false;
      mAllOnDisc = true; 
   }

   mNdata = aN;   
   mLabel     = aFileLabel;

   if (mAllInCore)                                              // Data are in core
   {
      mData.SetNewSize(mNdata);
      mData.PutToNb(aX,mNdata);
   }
   if (mAllOnDisc)                                              // Data are on disc.
   {
      DataIo(IO_PUT,mData,mNdata,0,1,0,1,true,aX); // mData is just a dummy in this call
   }
}

/**
* Constructor from MidasVector
* */
template<class T>
GeneralDataCont<T>::GeneralDataCont
   (  const GeneralMidasVector<T>& arV
   ,  const std::string& aStorage
   ,  const std::string& aFileLabel
   ,  bool aSaveUponDecon
   )
{
   std::string storage(aStorage);
   while(storage.find(" ")!= storage.npos) storage.erase(storage.find(" "),I_1); 
   // Delete blanks
   std::transform(storage.begin(),storage.end(),storage.begin(),(In(*) (In))toupper);
   // All to uppercase

   if (storage == "INMEM") 
   {
      mAllInCore = true;
      mAllOnDisc = false;
   }
   else if (storage == "ONDISC") 
   {
      mAllInCore = false;
      mAllOnDisc = true; 
   }
 
   mNdata     = arV.Size();
   mLabel     = aFileLabel;
   mSaveUponDecon = aSaveUponDecon;

   if (mAllInCore)                                              // Data are in core
   {
      mData.SetNewSize(mNdata);
      mData = arV; 
   }
   if (mAllOnDisc)                                              // Data are on disc.
   {
      DataIo(IO_PUT,arV,mNdata);
   }
}
/**
* Constructor from MidasMatrix
* */
template<class T>
GeneralDataCont<T>::GeneralDataCont
   ( const GeneralMidasMatrix<T>& arM
   , const string& aStorage
   , const string& aFileLabel
   , bool aSaveUponDecon
   )
{
   string storage(aStorage);
   while(storage.find(" ")!= storage.npos) storage.erase(storage.find(" "),I_1); 
   // Delete blanks
   transform(storage.begin(),storage.end(),storage.begin(),(In(*) (In))toupper);
   // All to uppercase

   if (storage == "INMEM") 
   {
      mAllInCore = true;
      mAllOnDisc = false;
   }
   else if (storage == "ONDISC") 
   {
      mAllInCore = false;
      mAllOnDisc = true; 
   }
 
   mNdata     = arM.Ncols()*arM.Nrows();
   mLabel     = aFileLabel;
   mSaveUponDecon = aSaveUponDecon;

   if (mAllInCore)                                              // Data are in core
   {
      mData.SetNewSize(mNdata);
      mData.MatrixRowByRow(arM); 
   }
   if (mAllOnDisc)                                              // Data are on disc.
   {
      DataIo(IO_PUT,arM,mNdata,arM.Nrows(),arM.Ncols());
   }
}
/**
* Copy Constructor 
* */
template<class T>
GeneralDataCont<T>::GeneralDataCont(const GeneralDataCont<T>& arDc)
{
   mNdata     = arDc.Size();
   mAllInCore = arDc.mAllInCore;
   mAllOnDisc = arDc.mAllOnDisc;
   mSaveUponDecon = arDc.mSaveUponDecon;
   mSameLabelWhenCopying = arDc.mSameLabelWhenCopying;

   // If data is in memory, not set to save at destruction, and asked to reuse
   // same label, do so - otherwise append "_copy", so files won't be
   // overwritten.
   if (mSameLabelWhenCopying && mAllInCore && !mSaveUponDecon)
   {
      mLabel     = arDc.mLabel;
   }
   else
   {
      mLabel     = arDc.mLabel + "_copy";
   }

   if (mAllInCore)                                              // Data are in core
   {
      mData.SetNewSize(mNdata);
      mData = arDc.mData;
      if (gDebug) 
      {
         Mout << " copy data in DataCont" << endl; 
      }
   }
   else if (mAllOnDisc)                                              // Data are on disc.
   {
      In n_data = mNdata;
      while (n_data > 0)
      {
         In i_extension = (n_data-1)/gMaxFileSize;

         std::ostringstream tmp;
         tmp << arDc.mLabel << "_" << i_extension;
         string file_name = tmp.str();

         std::ostringstream tmp2;
         tmp2 << mLabel << "_" << i_extension;
         string file_name2 = tmp2.str();

         if (gDebug) 
         {
            Mout << " copy " << file_name << " to " << file_name2 << endl; 
         }
         //CopyFile(file_name, file_name2);
         midas::filesystem::Copy(file_name, file_name2);

         n_data -= gMaxFileSize;
      }
   }
   else
   {
      MIDASERROR("NOT INMEM OR ON DISC!!!!!!");
   }
}

template<class T>
GeneralDataCont<T>::GeneralDataCont(const ZeroVector<T>& zero)
   : mAllInCore(true)
   , mAllOnDisc(false)
   , mSaveUponDecon(false)
   , mNdata(zero.Size())
   , mLabel("zerovec")
{
   mData.SetNewSize(Size(),false);
   mData.Zero();
}
/**
* To make it easy to create a zero DataCont and then read in something from an existing 
* */
template<class T>
bool GeneralDataCont<T>::UseAvailableOnDisc()
{
   Mout << "DataCont: Using avaiable data in " << Label() << endl;
   bool in_core = mAllInCore;
   bool on_disc = mAllOnDisc;
   mAllInCore = false;
   mAllOnDisc = true;
   GeneralDataCont<T> copy;
   copy.ChangeStorageTo("ONDISC");
   In n = Size();
   copy.SetNewSize(n);
   copy.NewLabel("use_available_tmp");
   //copy.SaveUponDecon(false);

   mAllInCore = in_core;
   mAllOnDisc = on_disc;
   copy.Zero(); // Zero data in container.

   bool read_while_still_data_algo = true;
   bool found_anything = false;
   if (read_while_still_data_algo)
   {
      In n_data_left = copy.mNdata;
      In buffer_size = min(gMaxBufSize,n_data_left);
      In add = I_0;
      while (n_data_left > I_0 && buffer_size > I_0)     // Batch over in core buffers. 
      {
         In in_this = min(buffer_size,n_data_left);

         Mout << " buffer      " << buffer_size << endl;
         Mout << " n_data_left " << n_data_left << endl;
         Mout << " add         " << add    << endl;
         Mout << " in_this     " << in_this<< endl;

         GeneralMidasVector<T> tmp1(in_this);
         Io err = DataIo(IO_GET | IO_ROBUST ,tmp1, in_this, add);
         if (Io::ERROR == err) 
         {
            //n_data_left = (n_data_left+I_1)/I_2;
            buffer_size = buffer_size/I_2;
            Mout << " Reduce size." << endl;
         }
         else
         {
            Mout << " Read in in vector of size " << in_this
                 << " at address " << add << "." <<endl;
            copy.DataIo(IO_PUT,tmp1,in_this,add);
            n_data_left -= in_this;
            add += in_this;
            found_anything = true;
         }
      }
      Mout << " Found " << add << " available data on disc." << endl;
      *this = copy;
      Mout << " Norm of vector is now " << Norm() << endl;
      //Mout << *this << endl;
   }
   return found_anything;
}
/**
* To make it easy to create a zero GeneralDataCont<T> and then read in something from an existing 
* */
template<class T>
void GeneralDataCont<T>::GetFromExistingOnDisc(const In aN,string aFileLabel)
{
    ChangeStorageTo("ONDISC");
    NewLabel(aFileLabel);
    SetNewSize(aN);
}
/**
* ReAssignment to a piece of another datacont.
* aWhichWay: I_1: this <-- arDc 
*           -I_1: this --> arDc
* */
template<class T>
void GeneralDataCont<T>::Reassign(GeneralDataCont<T>& arDc,In arOff,In aWhichWay,T aShift, const In aNdata)
//not needed for assignment only: mData()
{
   In n_data;
   if (aNdata==-I_1) 
   {
      n_data = mNdata;
   }
   else 
   {
      n_data = aNdata;
   }
   
   if (this == &arDc)
   {
      MIDASERROR("DataCont reassigned to itself, could cause strange behavoir, not tested so Exit!");
   }
   if (n_data > arDc.Size()-arOff)
   {
      MIDASERROR("Not enough elements in arDc for DataCont ReAssignment");
   }
   if (this == &arDc && arOff != I_0) 
   {
      MIDASERROR("DataCont::Reassign(), a datacont to itself with non unit off set, most likely an error");
   }
   if (Label() == arDc.Label() && arOff != I_0) 
   {
      MidasWarning("DataCont::Reassign(), a datacont to one with same Label and with non unit off set, might be an error");
   }
   
   if(!n_data) return; // if there are no elements to copy we just return

   //Mout << Label() << " in: " << *this << endl;
   //Mout << arDc.Label() << " ardcin: " << arDc << endl;
   if (mAllInCore && aShift == T(C_0))   // Data are in core, NOT TESTED, 21-01-2003
   {
      if (aWhichWay == -1)
      {
         arDc.DataIo(IO_PUT,mData,n_data,arOff);
      }
      else
      {
         arDc.DataIo(IO_GET,mData,n_data,arOff);
      }
   }
   else                                                              // Data are on disc.
   {
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(static_cast<In>(gMaxBufSize),n_data-add);

         GeneralMidasVector<T> tmp(in_this);

         //Mout << " n_data      " << n_data      << endl;
         //Mout << " i_extension " << i_extension << endl;
         //Mout << " add         " << add << endl;
         //Mout << " in_this     " << in_this << endl;
         if (aWhichWay == I_1)
         {
            arDc.DataIo(IO_GET,tmp,in_this,add+arOff);
            if (aShift!=C_0) for (In i=I_0;i<in_this;i++) tmp[i]+=aShift;
            DataIo(IO_PUT,tmp,in_this,add);
         }
         else if (aWhichWay == -I_1)
         {
            DataIo(IO_GET,tmp,in_this,add);
            if (aShift!=C_0) for (In i=I_0;i<in_this;i++) tmp[i]+=aShift;
            arDc.DataIo(IO_PUT,tmp,in_this,add+arOff);
         }
         else
            MIDASERROR("DataCont::ReAssign(): Unknown which way.");
         //Mout << " arX         " << arX << endl;
         //Mout << " tmp norm    " << tmp.Norm() << endl;
         //Mout << " tmp         " << tmp << endl;
         n_data -= in_this;
      }
   }
   //Mout << Label() << " out: " << *this << endl;
   //Mout << arDc.Label() << " ardcout: " << arDc << endl;
}

/***************************************************************************//**
 * Calculate the complex conjugate a datacont.
 *
 * @return
 *    Complex conjugate of a GeneralDataCont
 ******************************************************************************/
template<class T>
void GeneralDataCont<T>::ConjugateInPlace()
{
   auto conj = [](T& elem) -> void
   {
      elem = midas::math::Conj(elem);
   };
   this -> LoopOverElements(conj);
}
template<class T>
GeneralDataCont<T> GeneralDataCont<T>::Conjugate() const
{
   auto result = *this;
   result.ConjugateInPlace();
   return result;
}


/***************************************************************************//**
 * Calculate the dot product between elements of two dataconts. (Possibly only
 * the dot product of the ranges with index [aBegin, aBegin + aN).)
 *
 * @param[in] arDc1
 *    One DataCont.
 * @param[in] arDc2
 *    Another DataCont. Their sizes must be the same. Can be the same as arDc1
 *    in which case some optimization is done if data is on disk (file only
 *    read once).
 * @param[in] aBegin
 *    Index of first element in the DataCont%s for which to calculate dot
 *    product.
 * @param[in] aN
 *    Number of elements for which to calculate dot product. At most till the
 *    end of the containers.
 * @return
 *    Dot product of the ranges of elements.
 ******************************************************************************/
template<class T>
T  Dot
   (  const GeneralDataCont<T>& arDc1
   ,  const GeneralDataCont<T>& arDc2
   ,  const In aBegin
   ,  const In aN
   )
{
   if (&arDc1 == &arDc2)
   {
      return arDc1.Norm2(aBegin, aN);
   }
   else
   {
      T dot = 0;
      auto add_to_dot = [&dot](const T& a, const T&b) -> void
         {
            dot += midas::math::Conj(a) * b;
         };
      Uin beg = aBegin;
      Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
      arDc1.LoopOverElements(add_to_dot, arDc2, beg, end);
      return dot;
   }
}

/***************************************************************************//**
 * Calculate the sum of the products of the elements of two dataconts.
 * (Possibly only the dot product of the ranges with index [aBegin, aBegin +
 * aN).)
 * Equivalent to Dot for the case of real numbers.
 *
 * @param[in] arDc1
 *    One DataCont.
 * @param[in] arDc2
 *    Another DataCont. Their sizes must be the same.
 * @param[in] aBeg
 *    Index of first element in the DataCont%s for which to calculate sum.
 * @param[in] aEnd
 *    Index of first-beyond-last element in the DataCont%s for which to
 *    calculate sum.
 * @return
 *    Sum of products of the ranges of elements.
 ******************************************************************************/
template<class T>
T  SumProdElems
   (  const GeneralDataCont<T>& arDc1
   ,  const GeneralDataCont<T>& arDc2
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   T sum = 0;
   auto add_to_sum = [&sum](const T& a, const T&b) -> void
      {
         sum += a * b;
      };
   arDc1.LoopOverElements(add_to_sum, arDc2, aBeg, aEnd);
   return sum;
}

/**
 * @brief 
 *    Calculate standard Hadamard product of two DataCont%s and store the result in the first one.
 * 
 *    x[i] <- x[i] * y[i]
 * 
 * @param[in,out] arLeft      Left DataCont ('x' in above equation); contains result on output
 * @param[in] aRight          Right DataCont ('y' in above equation); not modified
 * @param[in] aBeg            Start index
 * @param[out] aEnd           End index
 */
template<typename T>
void HadamardProduct
   (  GeneralDataCont<T>& arLeft
   ,  const GeneralDataCont<T>& aRight
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   arLeft.LoopOverElements([](T& x, const T& y){ x*=y; }, aRight, aBeg, aEnd);
}

/**
* Calculate the dot product between two dataconts.
* */
template<class T>
void GeneralDataCont<T>::Orthogonalize(const GeneralDataCont<T>& arDc)
{
   // proj_u (v)  = <u|v>/<u|u> u
   T x = -Dot(*this,arDc) / Dot(arDc, arDc); // Overlap
   //Mout << " - Overlap  " << x << endl; 
   //Mout << " arDc   " << arDc << endl; 
   //Mout << " before " << *this << endl; 
   Axpy(arDc,x); // v1 <- v1 - <v1|v2>/<v2|v2>*v2
   //Mout << " after  " << *this << endl; 
}
/**
* Scale a datacont by a factor
* */
template<class T>
void GeneralDataCont<T>::Scale(const T aNb,In aBegin,In aN)
{
   if (aBegin==I_0 && aN==-I_1)
   {
      if (mAllInCore) 
      {
         mData = aNb*mData;
      }
      else
      {
         In n_data = mNdata;
         while (n_data > 0)/// Batch over in core buffers 
         {
            In i_extension = (n_data-1)/gMaxBufSize; 
            In add = i_extension*gMaxBufSize;
            In in_this = min(gMaxBufSize,n_data-add);
   
            GeneralMidasVector<T> tmp1(in_this);
            DataIo(IO_GET,tmp1,in_this,add);
   
            tmp1 = aNb*tmp1;
            DataIo(IO_PUT,tmp1,in_this,add);
            n_data -= in_this;
         }
      }
   }
   else
   {
      if (mAllInCore) 
      {
         mData.Scale(aNb,aBegin,aN);
      }
      else
      {
         In n_data = mNdata;
         In i_end   = aBegin+aN; //one past end...
         In i_begin = aBegin;
         while (n_data > 0)/// Batch over in core buffers 
         {
            In i_extension = (n_data-1)/gMaxBufSize; 
            In add = i_extension*gMaxBufSize;
            In in_this = min(gMaxBufSize,n_data-add);
   
            if (add >= i_end || add+in_this < i_begin)
            {
               //Mout << " No cont for add = " << add << endl;
            }
            else
            {
               In in_this2 = in_this;
               In i_begin_this = add;
               In i_end_this   = add+in_this;
               if (i_begin_this<i_begin) i_begin_this = i_begin;
               if (i_end_this>i_end) i_end_this = i_end;
               in_this2 = i_end_this - i_begin_this;

               GeneralMidasVector<T> tmp1(in_this2);
               DataIo(IO_GET,tmp1,in_this2,i_begin_this);
      
               tmp1.Scale(aNb); // Laeser kun den del der skal scaleres...
               DataIo(IO_PUT,tmp1,in_this2,i_begin_this);
            }
            n_data -= in_this;
         }
      }
   }
}
/**
* Shift a datacont by aNb
* */
template<class T>
void GeneralDataCont<T>::Shift(const T aNb)
{
   if (mAllInCore) 
   {
      mData.Shift(aNb);
   }
   else
   {
      In n_data = mNdata;
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(gMaxBufSize,n_data-add);

         GeneralMidasVector<T> tmp1(in_this);
         DataIo(IO_GET,tmp1,in_this,add);

         tmp1.Shift(aNb);
         DataIo(IO_PUT,tmp1,in_this,add);
         n_data -= in_this;
      }
   }
}

/**
* Take power of each element.
* */
template<class T>
void GeneralDataCont<T>::Pow(const T aNb)
{
   if (mAllInCore) 
   {
      mData.Pow(aNb);
   }
   else
   {
      In n_data = mNdata;
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(gMaxBufSize,n_data-add);

         GeneralMidasVector<T> tmp1(in_this);
         DataIo(IO_GET,tmp1,in_this,add);

         tmp1.Pow(aNb);
         DataIo(IO_PUT,tmp1,in_this,add);
         n_data -= in_this;
      }
   }
}

/**
* Normalize the vector on a DataCont 
* */
template<class T>
bool GeneralDataCont<T>::Normalize()
{
   T x = Dot(*this,*this); // Overlap
   T norm = sqrt(x);
   //Mout << " Norm of : " << Label() << " is " << norm << endl;
   if (norm < C_10*C_NB_MIN) return false; // Does C_10*C_NB_MIN make here any sense? E.g. C_10*C_NB_EPSILON seems to me better.
   T norminv = C_1/norm;
   Scale(norminv);
   return true;
}
/**
* IntermediateNormalize the vector so vec[aIdx] = C_1;
* */
template<class T>
bool GeneralDataCont<T>::IntermediateNormalize(In aIdx)
{
   T norm;
   DataIo(IO_GET, aIdx, norm);  
   if (std::abs(norm) < T(C_10*C_NB_EPSILON)) return false; 
   // If difference compared to one is to large, numerical problems may be on the way,
   // skip.
   Mout << " Intermediate normalization involves scaling by 1/" << left << norm << endl;
   T norminv = T(C_1)/norm;
   Scale(norminv);

   return true;
}

/**
* Assignment, operator =
* */
template<class T>
GeneralDataCont<T>& GeneralDataCont<T>::operator=(const GeneralDataCont<T>& arDc)//not needed for assignment only: mData()
{
   if (this == &arDc) 
   {
      Mout << "datacont assigned to itself, could cause strange behavoir ??  " << endl;
   }
   else
   {
      SetNewSize(arDc.Size());
      //mNdata     = arDc.Size();
      //mAllInCore = arDc.mAllInCore;
      //mAllOnDisc = arDc.mAllOnDisc;
      //mLabel     = arDc.mLabel + "_copy"; Change the data but not the logicals, storage & label 
      //mSaveUponDecon = arDc.mSaveUponDecon;
   
      if (mAllInCore && arDc.mAllInCore)                                              // Data are in core
      {
         if (mNdata != mData.Size()) mData.SetNewSize(mNdata,false);
         mData = arDc.mData;
         if (gDebug) Mout << " copy data in DataCont" << endl; 
      }
      else if (mAllInCore && arDc.mAllOnDisc) // To Data are in core, From on Disc.
      {
         arDc.DataIo(IO_GET,mData,mNdata);
      }
      else if (mAllOnDisc && arDc.mAllInCore) // To Data are in core, From on Disc.
      {
         DataIo(IO_PUT,arDc.mData,mNdata);
      }
      else if (mAllOnDisc && arDc.mAllOnDisc)                    // Data are on disc.
      {
         In n_data = mNdata;
         while (n_data > 0)
         {
            In i_extension = (n_data-1)/gMaxFileSize;
   
            std::ostringstream tmp;
            tmp << arDc.mLabel << "_" << i_extension;
            string file_name = tmp.str();
   
            std::ostringstream tmp2;
            tmp2 << mLabel << "_" << i_extension;
            string file_name2 = tmp2.str();
   
            if (gDebug) Mout << " copy " << file_name << " to " << file_name2 << endl; 
            if (file_name2 != file_name) 
            {
               //CopyFile(file_name,file_name2);
               midas::filesystem::Copy(file_name,file_name2);
            }
   
            n_data -= gMaxFileSize;
         }
      }
   }
   return *this;
}
/***************************************************************************//**
 * AXPY: Y <- a*X + Y, 
 * i.e. add-assign a scalar times another vector/DataCont (Y += a*X).
 *
 * This object is the Y (the vector being assigned to) in AXPY.
 *
 * @param[in] arDc
 *    The X (the other vector) in AXPY.
 * @param[in] arX
 *    The a (the scalar) in AXPY.
 ******************************************************************************/
template<class T>
void GeneralDataCont<T>::Axpy(const GeneralDataCont<T>& arDc, const T arX)//not needed for assignment only: mData()
{
   if (this == &arDc) MIDASERROR( "y=x in datacont axpy, could cause strange behavoir, not tested so Exit! ");
   if (mNdata != arDc.mNdata)
   {
      Mout << " this: " << Label() << " has " << mNdata << endl;
      Mout << " arDc: " << arDc.Label() << " has " << arDc.mNdata << endl;
      MIDASERROR ( " x,y dataconts must have same size in Datacont Axpy " );
   }

   //Mout << " Axpy, arX = " << arX << endl;
   //Mout << " arDc.storage " << arDc.Storage() << endl;
   //Mout << " this storage " << this->Storage() << endl;
   //Mout << " arDc " << arDc << endl;
   //Mout << " this " << *this << endl;
   if (mAllInCore && arDc.mAllInCore)                                              // Data are in core
   {
      mData += arX*arDc.mData;
   }
   else 
   {
      In n_data = mNdata;
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(gMaxBufSize,n_data-add);

         if (arDc.mAllOnDisc)
         {
            GeneralMidasVector<T> tmp(in_this);
            arDc.DataIo(IO_GET,tmp,in_this,add);
            //Mout << " add " << add << " in_this " << in_this << endl;
            //Mout << " tmp " << tmp << endl;
            DataIo(IO_PUT,tmp,in_this,add,I_1,I_0,I_1,false,C_0,true,arX);
         }
         else
         {
            DataIo(IO_PUT,arDc.mData,in_this,add,I_1,add,I_1,false,C_0,true,arX);
         }


         n_data -= in_this;
      }
   }
   //Mout << "after " << *this << endl;
}

/**
* DirProd usage:
* aI =  I_1: y_i <- y_i * (arX + x_i)
* aI = -I_1: y_i <- y_i / (arX + x_i)
* */
template<class T>
void GeneralDataCont<T>::DirProd(const GeneralDataCont<T>& arDc, const T arX, const In aI)//not needed for assignment only: mData()
{
   if((aI != I_1) && (aI != -I_1))
   {
      MIDASERROR("Don't know what to do in DataCont::DirProd");
   }
   if (this == &arDc)
   {
      MIDASERROR( "y=x in datacont DirProd, could cause strange behavoir, not tested so Exit! ");
   }
   if (mNdata != arDc.mNdata)
   {
      MIDASERROR ( " x,y dataconts must have same size in Datacont DirProd " );
   }

   if (mAllInCore && arDc.mAllInCore) // both in core
   {
      if (aI == I_1) for (In i=I_0;i<mNdata;i++) mData[i] *= (arX+arDc.mData[i]);
      if (aI == -I_1) for (In i=I_0;i<mNdata;i++) mData[i] /= (arX+arDc.mData[i]);
   }
   if (mAllOnDisc)  // else on disc.
   {
      In n_data = mNdata;
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(gMaxBufSize,n_data-add);

         GeneralMidasVector<T> tmp1(in_this);
         DataIo(IO_GET,tmp1,in_this,add);

         if (arDc.mAllOnDisc)
         {
            GeneralMidasVector<T> tmp2(in_this);
            arDc.DataIo(IO_GET,tmp2,in_this,add);
            if (aI==I_1) for (In i=I_0;i<in_this;i++) tmp1[i] *= (arX+tmp2[i]);
            if (aI==-I_1) for (In i=I_0;i<in_this;i++) tmp1[i] /= (arX+tmp2[i]);
         }
         else
         {
            if (aI == I_1) for (In i=I_0;i<in_this;i++) tmp1[i] *= (arX+arDc.mData[i+add]);
            if (aI == -I_1) for (In i=I_0;i<in_this;i++) tmp1[i] /= (arX+arDc.mData[i+add]);
         }

         DataIo(IO_PUT,tmp1,in_this,add);

         n_data -= in_this;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   LoopOverElementsImpl(*this, f, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const Uin aBeg
   ,  const Uin aEnd
   )  const
{
   LoopOverElementsImpl(*this, f, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   ,  class CONT_T
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const CONT_T& arOther
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   LoopOverElementsImpl(*this, f, arOther, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   ,  class CONT_T
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const CONT_T& arOther
   ,  const Uin aBeg
   ,  const Uin aEnd
   )  const
{
   LoopOverElementsImpl(*this, f, arOther, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   ,  class CONT_T
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const CONT_T& arOtherA
   ,  const CONT_T& arOtherB
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   LoopOverElementsImpl(*this, f, arOtherA, arOtherB, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class F
   ,  class CONT_T
   >
void GeneralDataCont<T>::LoopOverElements
   (  F&& f
   ,  const CONT_T& arOtherA
   ,  const CONT_T& arOtherB
   ,  const Uin aBeg
   ,  const Uin aEnd
   )  const
{
   LoopOverElementsImpl(*this, f, arOtherA, arOtherB, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class THIS_T
   ,  class F
   >
void GeneralDataCont<T>::LoopOverElementsImpl
   (  THIS_T& arThis
   ,  F&& f
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   using this_elem_t = std::conditional_t<std::is_const_v<THIS_T>, const T, T>;
   auto f_mod = [&f](this_elem_t& elem_this, const std::array<T,0>&) -> auto
      {
         return f(elem_this);
      };
   const std::array<const GeneralDataCont<T>*,0> v{};
   LoopOverElementsGenNumOthersImpl(arThis, f_mod, v, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class THIS_T
   ,  class F
   ,  class OTHER_T
   >
void GeneralDataCont<T>::LoopOverElementsImpl
   (  THIS_T& arThis
   ,  F&& f
   ,  const OTHER_T& arOther
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   using this_elem_t = std::conditional_t<std::is_const_v<THIS_T>, const T, T>;
   const std::array<const OTHER_T*,1> v = {&arOther};
   auto f_mod = [&f](this_elem_t& elem_this, const std::array<T,1>& elem_others) -> auto
      {
         return f(elem_this, elem_others[0]);
      };
   LoopOverElementsGenNumOthersImpl(arThis, f_mod, v, aBeg, aEnd);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class THIS_T
   ,  class F
   ,  class OTHER_T
   >
void GeneralDataCont<T>::LoopOverElementsImpl
   (  THIS_T& arThis
   ,  F&& f
   ,  const OTHER_T& arOtherA
   ,  const OTHER_T& arOtherB
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   using this_elem_t = std::conditional_t<std::is_const_v<THIS_T>, const T, T>;
   const std::array<const OTHER_T*,2> v = {&arOtherA, &arOtherB};
   auto f_mod = [&f](this_elem_t& elem_this, const std::array<T,2>& elem_others) -> auto
      {
         return f(elem_this, elem_others[0], elem_others[1]);
      };
   LoopOverElementsGenNumOthersImpl(arThis, f_mod, v, aBeg, aEnd);
}


/***************************************************************************//**
 * Loops through elements of the containers (arThis and the N ones pointed to
 * by arOthers) in the range [aBeg;aEnd) (or till end of containers, if aEnd
 * exceeds their sizes), performing for each set of elements the operation
 * given by the functor F, which must implement a
 * ```
 *    void operator()([const] T&, const std::array<T,N>&)
 * ```
 * where const'ness of the first argument shall match the const'ness of THIS_T.
 *
 * If THIS_T is not const, arThis will be written to, as prescribed by the
 * functor F.
 *
 * This function will handle data on disk as follows:
 *    -  if all containers are stored in memory (if OTHER_T is
 *       GeneralMidasVector, it's automatically registered as stored in memory),
 *       it loops through the entire range [aBeg;aEnd) in _one_ batch
 *    -  if not, then it reads in and processes batches of maximum size
 *       gMaxBufSize
 *    -  DataIo(IO_GET/IO_PUT,...) is only performed if data for specific
 *       container is _actually_ on disk. This is done through the use of some
 *       temporary GeneralMidasVector<T> batch containers that are only set to
 *       non-zero size if actually needed (i.e. for those containers that are
 *       on disk). (But yes, very minor overhead for these zero-sized
 *       GeneralMidasVector%s.)
 *    -  For containers with data in memory (as GeneralMidasVector<T>) that
 *       data is accessed directly, no DataIo(...) usage. So no unnecessary
 *       overhead there.
 *
 * @note
 *    If all containers do not have same size, it causes a hard error.
 *
 * @param[in] arThis
 *    Intended to be `*this` of the calling object.
 * @param[in,out] f
 *    Functor that performs element-wise operations on data of containers.
 * @param[in] arOthers
 *    Array with pointers to N other containers to be processed.
 * @param[in] aBeg
 *    Index of first element(s) to be processed.
 * @param[in] aEnd
 *    Index of first-beyond-last element(s) to be processed.
 ******************************************************************************/
template
   <  typename T
   >
template
   <  class THIS_T
   ,  class OTHER_T
   ,  class F
   ,  size_t N
   >
void GeneralDataCont<T>::LoopOverElementsGenNumOthersImpl
   (  THIS_T& arThis
   ,  F&& f
   ,  const std::array<const OTHER_T*,N>& arOthers
   ,  const Uin aBeg
   ,  const Uin aEnd
   )
{
   static_assert  
      (  std::is_same_v<OTHER_T,GeneralDataCont<T>>
      || std::is_same_v<OTHER_T,GeneralMidasVector<T>>
      ,  "Only works/tested for OTHER_T = GeneralDataCont<T> or GeneralMidasVector<T>."
      );

   // We need the const'ness of arThis to set some types right later.
   constexpr bool const_this = std::is_const_v<THIS_T>;
   using this_tmp_mv_t = std::conditional_t
      <  const_this
      ,  const GeneralMidasVector<T>
      ,  GeneralMidasVector<T>
      >;

   // Assertions on container sizes.
   const Uin size = arThis.Size();
   for(Uin n = 0; n < arOthers.size(); ++n)
   {
      if (size != arOthers[n]->Size())
      {
         MIDASERROR
            (  "arThis.Size() (which is "
            +  std::to_string(size)
            +  ") != arOthers["+std::to_string(n)+"]->Size() (which is "
            +  std::to_string(arOthers[n]->Size())
            +  ")."
            );
      }
   }

   // For each container (arThis and the ones in arOthers), setup:
   //    - temp. MidasVector, init. size 0, intended to hold batches if reading from disk
   //    - a [const] MidasVector ptr.; will point to either temp. (if read from disk) or data in mem.
   //    - index offset to account read-in temp. MidasVector having index 0 at
   //      different element than in-mem. mData.
   In this_offset = 0;
   this_tmp_mv_t* this_p_mv = nullptr;
   GeneralMidasVector<T> this_tmp_mv;

   std::array<T,N> elems = {};
   std::array<In,N> oths_offset = {};
   std::array<const GeneralMidasVector<T>*,N> oths_p_mv = {};
   std::array<GeneralMidasVector<T>,N> oths_tmp_mv;

   // If _everything_ is in memory, we just do one batch (because everything is
   // in memory anyway). Otherwise, we do batches according to gMaxBufSize.
   Uin buf_size = (arThis.mAllInCore && AllContainersInCore(arOthers))? arThis.mNdata: gMaxBufSize;

   // Then we process batches starting at address given by aBeg,
   // and set the end (first-beyond-last) as given by aEnd _or_ size of container.
   Uin curr_addr = aBeg;
   const Uin end = std::min(size, aEnd);
   while (curr_addr < end)
   {
      // Read batches of buf_size or until end of containers.
      // (while loop asserts curr_addr < end, so end-curr_addr is guaranteed positive.)
      Uin curr_batch_size = std::min(buf_size,end-curr_addr);

      // For all containers (arThis, arOthers):
      //    - if in mem.: set pointer (this_p_mv, oths_p_mv) to point to
      //      container's MidasVector data, and set offset equal to curr_addr
      //    - if on disk: read in a batch to temp. MidasVector (this_tmp_mv,
      //      oths_tmp_mv), then set pointer to point to that, and offset equal to 0.
      LoadAndSetupPointer(arThis, this_tmp_mv, this_p_mv, this_offset, curr_batch_size, curr_addr);
      for(Uin n = 0; n < arOthers.size(); ++n)
      {
         LoadAndSetupPointer(*arOthers[n], oths_tmp_mv[n], oths_p_mv[n], oths_offset[n], curr_batch_size, curr_addr);
      }

      // Loop through entire batch; at each iteration, load elements from
      // arOthers into local 'elems' array, then pass to functor.
      for(Uin i = 0; i < curr_batch_size; ++i)
      {
         LoadToArray(elems, oths_p_mv, i, oths_offset);
         f((*this_p_mv)[i+this_offset],elems);
      }

      // If arThis was not const in this call _and_ we didn't directly process
      // its data through this_p_mv (which would happen if it's in memory),
      // then load the data back into the container.
      if constexpr(!const_this)
      {
         if (this_p_mv != &arThis.mData)
         {
            arThis.DataIo(IO_PUT, this_tmp_mv, curr_batch_size, curr_addr);
         }
      }

      // Update the current address for next iteration.
      curr_addr += curr_batch_size;
   }
}


/**
 *
 **/
template<typename T>
template<size_t N>
void GeneralDataCont<T>::LoadToArray
   (  std::array<T,N>& arElems
   ,  const std::array<const GeneralMidasVector<T>*,N>& arConts
   ,  const Uin aIndex
   ,  const std::array<In,N>& arIndexOffset
   )
{
   for(Uin n = 0; n < arElems.size(); ++n)
   {
      arElems[n] = arConts[n]->operator[](aIndex + arIndexOffset[n]);
   }
}

/**
 *
 **/
template<typename T>
template<size_t N>
bool GeneralDataCont<T>::AllContainersInCore(const std::array<const GeneralDataCont<T>*,N>& arConts)
{
   for(const auto& gdc: arConts)
   {
      if (!gdc->mAllInCore)
      {
         return false;
      }
   }
   return true;
}
template<typename T>
template<size_t N>
bool GeneralDataCont<T>::AllContainersInCore(const std::array<const GeneralMidasVector<T>*,N>& arConts)
{
   return true;
}

/**
 *
 **/
template<typename T>
template<class CONT_T, class MV_T>
void GeneralDataCont<T>::LoadAndSetupPointer
   (  CONT_T& arCont
   ,  GeneralMidasVector<T>& arTmpMv
   ,  MV_T* & apPtr
   ,  In& arOffSet
   ,  Uin aBatchSize
   ,  Uin aAddress
   )
{
   if constexpr(std::is_same_v<std::remove_const_t<CONT_T>,GeneralDataCont<T>>)
   {
      if (arCont.mAllInCore)
      {
         apPtr = &(arCont.mData);
         arOffSet = aAddress;
      }
      else
      {
         arTmpMv.SetNewSize(aBatchSize);
         arCont.DataIo(IO_GET,arTmpMv,arTmpMv.Size(),aAddress);
         apPtr = &arTmpMv;
         arOffSet = 0;
      }
   }
   else
   {
      apPtr = &arCont;
      arOffSet = aAddress;
   }
}

/**
 * HadamardProduct for datacont
 *
 *     A <- scalar * A \HPROD B: a_{ij} <- scalar*a_{ij}*b{ij}
 *
 **/
template<class T>
template<class V>
void GeneralDataCont<T>::HadamardProduct(V&& arDc, const T arScal, typename GeneralDataCont<T>::Product::Normal_t)
{
   if (mNdata != arDc.Size())
   {
      MIDASERROR ( " x,y dataconts must have same size in Datacont HadamardProduct " );
   }
   
   LoopOverElements([arScal](T& t1,const T& t2){ t1 *= arScal*t2; }, std::forward<V>(arDc));
}

/**
 * Inverse HadamardProduct for datacont
 *
 *     A <- scalar * A \INVHPROD B: a_{ij} <- scalar*a_{ij}/b{ij}
 *
 **/
template<class T>
template<class V>
void GeneralDataCont<T>::HadamardProduct(V&& arDc, const T arScal, typename GeneralDataCont<T>::Product::Inverse_t)
{
   if (mNdata != arDc.Size())
   {
      MIDASERROR ( " x,y dataconts must have same size in Datacont HadamardProduct " );
   }
   
   LoopOverElements([arScal](T& t1,const T& t2){ t1 *= arScal/t2; }, std::forward<V>(arDc));
}

/**
* Dump To disc  
* */
template<class T>
void GeneralDataCont<T>::DumpToDisc(string aS) 
{ 
   string name = aS;
   bool store_ex = false;
   string save_name = mLabel;
   if (aS.size() == I_0) 
   {
      name = mLabel;
      store_ex = true;
   }

   if (mAllInCore)
   {
      mAllInCore = false;
      mAllOnDisc = true;
      if (!store_ex) mLabel = name;
      if (mNdata > 0) DataIo(IO_PUT,mData,mNdata); 
      if (!store_ex) mLabel = save_name;
      mAllOnDisc = false;
      mAllInCore = true;
   }
   if (mAllOnDisc)
   {
      GeneralDataCont<T> copy(*this); 
      copy.NewLabel(aS);
      copy.SaveUponDecon();
   }
}

/***************************************************************************//**
 * @warning
 *    Use this (cautiously - you _could_ end up overwriting files) if you know
 *    you'll be making some copies in your algorithm, but you don't care about
 *    updating the file label, because the data will stay in memory anyway.
 ******************************************************************************/
template<class T>
void GeneralDataCont<T>::SetSameLabelWhenCopying
   (  bool aSame
   )
{
   mSameLabelWhenCopying = aSame;
}

/**
* Deconstructor 
* */
template<class T>
GeneralDataCont<T>::~GeneralDataCont<T>
   (
   ) 
{ 
   if (mSaveUponDecon)
   {
      if (gDebug) Mout << " Deconstructing datacontainer " << mLabel << " - saving data" << endl;
      ChangeStorageTo("OnDisc");
   }
   else
   {
      if (gDebug) Mout << " Deconstructing datacontainer " << mLabel << " - deleting data" << endl;
      if (mAllOnDisc) Delete();        // If InMem then use automatic delete of vector
   }
}

/**
* Change the flag for saving upon deconstruction
* */
template<class T>
void GeneralDataCont<T>::SaveUponDecon(bool aBool) 
{
   mSaveUponDecon = aBool;
}

/**
* Return size of vector
* */
template<class T>
In GeneralDataCont<T>::Size() const { return mNdata; }

/**
* Change size of vector
* */
template<class T>
void GeneralDataCont<T>::SetNewSize(const In aN, bool aSaveOldData)
{ 
   if (mNdata == aN) return;

   mNdata = aN; 
   if (mAllInCore) 
   {
      mData.SetNewSize(mNdata,aSaveOldData);
      if (gDebug) Mout << " ReSet size of DataCont "<< mLabel << 
         " to size: "<< mNdata << endl; 
   }
}

/**
* Zero the values of the container
* */
template<class T>
void GeneralDataCont<T>::Zero() 
{
   if (mAllInCore) mData.Zero();

   if (mAllOnDisc)
   {
      DataIo(IO_PUT,mData,mNdata,0,1,0,1,true,T(C_0)); // mData is just a dummy in this call
   }
}

template<class T>
void GeneralDataCont<T>::SetToUnitVec(const In& arIdx)
{
   Zero();
   T one = T(C_1);
   DataIo(IO_PUT,arIdx,one);  // mData is just a dummy in this call
}

/**
 * Delete the contents of the container and replace with a container of zero
 * length. In particular delete the files!
 **/
template<class T>
void GeneralDataCont<T>::Delete
   (
   ) 
{ 
   if (mAllInCore) 
   {
      mNdata = 0;
      mData.SetNewSize(mNdata);
      if (gDebug) Mout << " Set size of data to zero"<< endl; 
   }
   if (mAllOnDisc) 
   {
      In n_data = mNdata;
      mNdata = 0;

      while (n_data > 0)
      {
         In i_extension = (n_data-I_1)/gMaxFileSize;
         std::ostringstream tmp;
         tmp << mLabel << "_" << i_extension;
         string file_name = tmp.str();
         //if (gDebug) Mout << " i_ext "<< i_extension << endl; 
         //if (gDebug) Mout << " ndata "<< n_data<< endl; 
         //if (gDebug) Mout << " gMaxFileSize "<< gMaxFileSize << endl; 
         if (gDebug) Mout << " Deleting " << file_name << endl; 
         midas::filesystem::Remove(file_name);

         n_data -= gMaxFileSize;
      }
   }
}
/**
* Change storage label only 
* */
template<class T>
bool GeneralDataCont<T>::ChangeStorageLabelOnly
   ( std::string aStorage
   )
{
   while(aStorage.find(" ")!= aStorage.npos) aStorage.erase(aStorage.find(" "),I_1); // Delete blanks
   transform(aStorage.begin(),aStorage.end(),aStorage.begin(),(In(*) (In))toupper);// All to uppercase
   if (aStorage == "INMEM") 
   {
      mAllInCore = true;
      mAllOnDisc = false;
   }
   else if (aStorage == "ONDISC") 
   {
      mAllInCore = false;
      mAllOnDisc = true; 
   }
   else
      MIDASERROR("DataCont::ChangeStorageLabelOnly(): aStorage not recognized");
   return false;
}

/**
 * Change storage to InMem OnDisc.
 * @param aStorage
 * @param aRobust
 * @param aCheck
 * @param aLetRestOnDisc
 * @return 
 **/
template<class T>
bool GeneralDataCont<T>::ChangeStorageTo
   (  std::string aStorage
   ,  bool aRobust
   ,  bool aCheck
   ,  bool aLetRestOnDisc
   )
{
   if (gDebug) 
      Mout << " DataCont::ChangeStorageTo() " << aStorage << " where mAllInCore = " << mAllInCore 
           << " mAllOnDisc = " << mAllOnDisc << " for label \"" << mLabel
           << "\", dim = " << mNdata << endl;
   while(aStorage.find(" ") != aStorage.npos)
      aStorage.erase(aStorage.find(" "),I_1); // Delete blanks.

   transform(aStorage.begin(),aStorage.end(),aStorage.begin(),(In(*) (In))toupper); // All to uppercase.

   if (aStorage == "INMEM" && mAllOnDisc ) // Change from OnDisc to InMem
   {
      mData.SetNewSize(mNdata);

      Io err = Io::ERROR;
      if (aRobust) 
      {
         err = DataIo(IO_GET | IO_ROBUST,mData,mNdata); 
      }
      else
      {
         err = DataIo(IO_GET,mData,mNdata); 
      }

      mAllInCore = true; // Must come after DataIo
      mAllOnDisc = false;

      In n_data = mNdata;
      while (n_data > 0)
      {
         In i_extension = (n_data-1)/gMaxFileSize;
         std::ostringstream tmp;
         tmp << mLabel << "_" << i_extension;
         string file_name = tmp.str();
         //if (gDebug) Mout << " i_ext "<< i_extension << endl; 
         //if (gDebug) Mout << " ndata "<< n_data<< endl; 
         //if (gDebug) Mout << " gMaxFileSize "<< gMaxFileSize << endl; 
         if (!aLetRestOnDisc)
         {
            if (gDebug) Mout << " Deleting " << file_name << endl; 
            midas::filesystem::Remove(file_name);
         }

         n_data -= gMaxFileSize;
      }
      if (err == Io::ERROR) return false;
   }
   else if (aStorage == "ONDISC" && mAllInCore) // Change from InMem to OnDisc 
   {
      mAllInCore = false; // Must come before DataIo
      mAllOnDisc = true; 

      if (mNdata > 0) 
      {
         DataIo(IO_PUT, mData, mNdata); 
      }
      mData.Clear(); // clear the data from the vector
   }
   else if (aCheck && aStorage == "ONDISC" && mAllOnDisc ) // Do not Change anything but check data is there
   {
      In n_data = mNdata;
      while (n_data > 0)/// Batch over in core buffers 
      {
         In i_extension = (n_data-1)/gMaxBufSize; 
         In add = i_extension*gMaxBufSize;
         In in_this = min(gMaxBufSize,n_data-add);

         GeneralMidasVector<T> tmp1(in_this);
         Io err = DataIo(IO_GET | IO_ROBUST,tmp1,in_this,add);
         if (Io::ERROR == err) return false;
         n_data -= in_this;
      }
   }
   return true;
}
/**
* Give a new label to the container, and change file names accordingly
* */
template<class T>
void GeneralDataCont<T>::NewLabel
   (  const std::string& aNewLabel
   ,  bool aMoveDataAlong
   ,  bool aCopy
   )
{
   if (mAllOnDisc && aMoveDataAlong)
   {
      In n_data = mNdata;
      while (n_data > 0)
      {
         In i_extension = (n_data-1)/gMaxFileSize;

         std::ostringstream tmp;
         tmp << mLabel << "_" << i_extension;
         string file_name = tmp.str();

         std::ostringstream tmp2;
         tmp2 << aNewLabel << "_" << i_extension;
         string file_name2 = tmp2.str();

         if (gDebug) 
         {
            Mout << " mv " << file_name << " to " << file_name2 << endl; 
         }

         if (!aCopy) 
         {
            midas::filesystem::Rename(file_name, file_name2);
         }
         else 
         {
            midas::filesystem::Copy(file_name, file_name2);
         }


         n_data -= gMaxFileSize;
      }
   }
   mLabel = aNewLabel;
}

/***************************************************************************//**
 * @param[in] arIo
 *    `IO_GET`, `IO_PUT` or `IO_GET | IO_ROBUST`. For the latter, a file I/O
 *    error will not cause the program to stop.
 * @param arVecMat
 *    Vector/matrix to assign to (for IO_GET) or get data from (for IO_PUT).
 * @param[in] arArgs
 *    Arguments to be forwarded to DataIoImpl(), see documentation there.
 * @return
 *    GeneralDataCont<T>::Io::SUCCESS if everything went fine. If there's a
 *    file I/O error (if data is on disk, see Storage()), the program generally
 *    stops with a MIDASERROR, unless called with `IO_GET | IO_ROBUST` in which
 *    case it'll continue but return GeneralDataCont<T>::Io::ERROR.
 ******************************************************************************/
// Vector version.
template <  class T
         >
template <  int N
         ,  class... Args
         ,  std::enable_if_t<bool(N & IO_GET), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  GeneralMidasVector<T>& arVecMat
   ,  Args&&... arArgs
   )  const
{
   return DataIoImpl(*this, arIo, arVecMat, std::forward<Args>(arArgs)...);
}

template <  class T
         >
template <  int N
         ,  class... Args
         ,  std::enable_if_t<bool(N & IO_PUT), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  const GeneralMidasVector<T>& arVecMat
   ,  Args&&... arArgs
   )
{
   return DataIoImpl(*this, arIo, arVecMat, std::forward<Args>(arArgs)...);
}

// Matrix version.
template <  class T
         >
template <  int N
         ,  class... Args
         ,  std::enable_if_t<bool(N & IO_GET), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  GeneralMidasMatrix<T>& arVecMat
   ,  Args&&... arArgs
   )  const
{
   return DataIoImpl(*this, arIo, arVecMat, std::forward<Args>(arArgs)...);
}

template <  class T
         >
template <  int N
         ,  class... Args
         ,  std::enable_if_t<bool(N & IO_PUT), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  const GeneralMidasMatrix<T>& arVecMat
   ,  Args&&... arArgs
   )
{
   return DataIoImpl(*this, arIo, arVecMat, std::forward<Args>(arArgs)...);
}

/***************************************************************************//**
 * @param[in] arIo
 *    `IO_GET`, `IO_PUT` or `IO_GET | IO_ROBUST`. For the latter, a file I/O
 *    error will not cause the program to stop.
 * @param[in] arAddress
 *    The address within the GeneralDataCont object to assign to/read from.
 * @param arNb
 *    Scalar (of type T) to assign to (for IO_GET) or get data from (for
 *    IO_PUT).
 * @return
 *    GeneralDataCont<T>::Io::SUCCESS if everything went fine. If there's a
 *    file I/O error (if data is on disk, see Storage()), the program generally
 *    stops with a MIDASERROR, unless called with `IO_GET | IO_ROBUST` in which
 *    case it'll continue but return GeneralDataCont<T>::Io::ERROR.
 ******************************************************************************/
template <  class T
         >
template <  int N
         ,  std::enable_if_t<bool(N & IO_GET), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  const In& arAddress
   ,  T& arNb
   )  const
{
   return DataIoImpl(*this, arIo, arAddress, arNb);
}

template <  class T
         >
template <  int N
         ,  std::enable_if_t<bool(N & IO_PUT), int>
         >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIo
   (  const IoType<N>& arIo
   ,  const In& arAddress
   ,  const T& arNb
   )
{
   return DataIoImpl(*this, arIo, arAddress, arNb);
}

/***************************************************************************//**
 * Take the data in arV and put into the GeneralDataCont object - or opposite
 * way.
 *
 * @note
 *    The argument offsets (aOffC, aOffV) are 0-based, regular C++ style.
 *    However, if ever diving into the implementation, notice that the FileIo()
 *    function (used if data is on disk) uses 1-based indexing.
 *
 * @warning
 *    Disclaimer: I didn't write the function, but have tried to write some
 *    more detailed comments about the arguments and their relations. But you
 *    should only have 95% trust that the details are correct, such as when
 *    certain arguments has effect and not. -MBH, July 2018.
 *
 * @param arDc
 *    The GeneralDataCont to read from (IO_GET) or write to (IO_PUT). Intended
 *    to be the `*this` from DataIo(). Making it an argument of a static
 *    function means that the template type DC can match both a const/non-const
 *    GeneralDataCont.
 * @param[in] aPutGet
 *    `IO_GET`, `IO_PUT` or `IO_GET | IO_ROBUST`. For the latter, a file I/O
 *    error will not cause the program to stop.
 * @param arV
 *    GeneralMidasVector<T> to assign to (for IO_GET) or get data from (for
 *    IO_PUT).
 *    Has _no effect_ if using IO_PUT and aPutToNb is true.
 * @param[in] aLen
 *    The number of data to be transferred.
 * @param[in] aOffC
 *    Offset, i.e. index of the place to begin in arDc (the DataCont).
 * @param[in] aStrideC
 *    The stride to use in arDc (the DataCont).
 * @param[in] aOffV
 *    Offset, i.e. index of the place to begin in arV (the MidasVector).
 *    Has _no effect_ if aPutToNb is true.
 * @param[in] aStrideV
 *    The stride to use in arV (the MidasVector).
 *    Has _no effect_ if aPutToNb is true.
 * @param[in] aPutToNb
 *    If true, a number (type T) will be assigned to elements of the container
 *    instead of a vector; starting at aOffC, aLen elements, with stride
 *    aStrideC.
 *    _Only_ has effect for IO_PUT.
 * @param[in] aNb
 *    The number to be used if aPutToNb is true.
 *    _Only_ has effect if aPutToNb is true.
 * @param[in] aAddTo
 *    If true: Instead of assigning to arV (for IO_GET) or arDc (for IO_PUT),
 *    will add the respective elements (i.e. += assignment) _multiplied_ by the
 *    aScaleFac; i.e. effectively performing an AXPY operation (according to
 *    given strides and offsets):
 *    ~~~
 *        arV  += aScaleFac*arDc    (IO_GET)
 *        arDc += aScaleFac*arV     (IO_PUT)
 *        arDc += aScaleFac*aNb     (IO_PUT and aPutToNb = true)
 *    ~~~
 * @param[in] aScaleFac
 *    The scaling factor to be used if aAddTo is true.
 *    _Only_ has effect if aAddTo is true.
 * @return
 *    GeneralDataCont<T>::Io::SUCCESS if everything went fine. If there's a
 *    file I/O error (if data is on disk, see Storage()), the program generally
 *    stops with a MIDASERROR, unless called with `IO_GET | IO_ROBUST` in which
 *    case it'll continue but return GeneralDataCont<T>::Io::ERROR.
 ******************************************************************************/
template
   <  class T
   >
template
   <  class DC
   ,  int N
   ,  class V
   ,  std::enable_if_t
      <  std::is_same_v<std::remove_const_t<V>, GeneralMidasVector<T>>
      ,  int
      >
   >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIoImpl
   (  DC& arDc
   ,  const IoType<N>& aPutGet
   ,  V& arV
   ,  In aLen
   ,  In aOffC
   ,  In aStrideC
   ,  In aOffV
   ,  In aStrideV
   ,  bool aPutToNb
   ,  T aNb
   ,  bool aAddTo
   ,  T aScaleFac
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   static_assert  (     constexpr_io_type == IO_GET
                     || constexpr_io_type == IO_PUT
                     || constexpr_io_type == (IO_GET | IO_ROBUST)
                  ,  "IoType must be IO_GET, IO_PUT, or IO_GET | IO_ROBUST."
                  );
            
   if (aStrideV == I_0)
   {
      MIDASERROR(" DataCont:DataIo - Not allowed zero vector stride. Label: " + arDc.Label());
   }
   if (aStrideC == I_0) 
   {
      MIDASERROR(" DataCont:DataIo - Not allowed zero data container stride. Label: " + arDc.Label());
   }
   if (!aPutToNb && (aOffV > arV.Size() || aOffV < I_0))
   {
      MIDASERROR(" DataCont:DataIo  - Vector offset is out of range. Label: " + arDc.Label());
   }
   if (aOffC > arDc.Size() || aOffC < I_0)
   {
      MIDASERROR(" DataCont:DataIo  - Container offset is out of range. Label: " + arDc.Label());
   }
   if (!aPutToNb && (aOffV + (aLen - I_1)*aStrideV > arV.Size() || aOffV + (aLen - I_1)*aStrideV < I_0))
   {
      MIDASERROR(" DataCont:DataIo  - Vector index goes out of range, stride/length ok? Label: " + arDc.Label());
   }
   if (I_1 + aOffC + (aLen - I_1)*aStrideC > arDc.Size() || I_1 + aOffC + (aLen - I_1)*aStrideC <= I_0)  
   {
      Mout << " 1+aOffC+(aLen-1)*aStrideC" << I_1 + aOffC + (aLen - I_1)*aStrideC << std::endl;
      Mout << "  aOffC+aLen*aStrideC " <<  aOffC + aLen*aStrideC << std::endl;
      
      MIDASERROR(" DataCont:DataIo  - Container index goes out of range stride/length ok? Label: " + arDc.Label());
   }

   In get_robust = I_0;

   if (arDc.mAllInCore) 
   {

      if (aStrideC == I_1 && aStrideV == I_1 && aOffC == I_0 && aOffV == I_0)
      {
         if constexpr(bool(constexpr_io_type & IO_PUT))
         {
            if (!aPutToNb)
            {
               arDc.mData.PieceIo(IO_PUT,arV,aLen,aOffC,aStrideC,aOffV,aStrideV,aAddTo,aScaleFac);
            }
            else
            {
               arDc.mData.PutToNb(aNb,aLen,aOffC,aStrideC);
            }
         }
         else if constexpr (bool(constexpr_io_type & IO_GET))
         {
            for (In i=I_0; i<aLen;++i) arV[i] = arDc.mData[i];
         }
         else
         {
            static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                           ,  "One of IO_PUT and IO_GET must be set!"
                           );
         }
      } 
      else
      {
         In i_cur_c  = aOffC;
         In i_cur_v  = aOffV;
         
         if (!aAddTo)
         {
            if constexpr(bool(constexpr_io_type & IO_PUT))
            {
               if (!aPutToNb)
               {
                  for (In i=I_0; i<aLen;i++) 
                  {
                     arDc.mData[i_cur_c] = arV[i_cur_v];
                     i_cur_c += aStrideC;
                     i_cur_v += aStrideV;
                  }
               }
               else
               {
                  for (In i=I_0; i<aLen;i++) 
                  {
                     arDc.mData[i_cur_c] = aNb;
                     i_cur_c += aStrideC;
                  }
               }
            }
            else if constexpr (bool(constexpr_io_type & IO_GET))
            {
               for (In i=I_0; i<aLen;i++) 
               {
                  arV[i_cur_v] = arDc.mData[i_cur_c];
                  i_cur_c += aStrideC;
                  i_cur_v += aStrideV;
               }
            }
            else
            {
               static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                              ,  "One of IO_PUT and IO_GET must be set!"
                              );
            }
         }
         else
         {
            if constexpr(bool(constexpr_io_type & IO_PUT))
            {
               if (!aPutToNb)
               {
                  for (In i=I_0; i<aLen;i++) 
                  {
                     arDc.mData[i_cur_c] += aScaleFac*arV[i_cur_v];
                     i_cur_c += aStrideC;
                     i_cur_v += aStrideV;
                  }
               }
               else
               {
                  T a_nb = aScaleFac*aNb;
                  for (In i=I_0; i<aLen;i++) 
                  {
                     arDc.mData[i_cur_c] = a_nb;
                     i_cur_c += aStrideC;
                  }
               }
            }
            else if constexpr (bool(constexpr_io_type & IO_GET))
            {
               for (In i=I_0; i<aLen;i++) 
               {
                  arV[i_cur_v] = aScaleFac*arDc.mData[i_cur_c];
                  i_cur_c += aStrideC;
                  i_cur_v += aStrideV;
               }
            }
            else
            {
               static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                              ,  "One of IO_PUT and IO_GET must be set!"
                              );
            }
         }
      }
   }
   if (arDc.mAllOnDisc) 
   {
      // If being IO_ROBUST set this to achieve desired robustness down the line.
      if constexpr(bool(constexpr_io_type & IO_ROBUST))
      {
         get_robust = -I_1;
      }

      //if (aAddTo) MIDASERROR("AddTo DataCont not implemented for OnDisc version");
      In i_add1 = I_1 + aOffC;
      In i_extension1 = (i_add1 - I_1)/gMaxFileSize;
      In i_add2 = I_1 + aOffC + aLen;
      In i_extension2 = (i_add2 - I_1)/gMaxFileSize;
      if (aStrideC == I_1 && aStrideV == I_1 && i_extension2 == i_extension1 && !aPutToNb && !aAddTo)
      {
         std::ostringstream tmp;
         tmp << arDc.Label() << "_" << i_extension1;
         string file_name = tmp.str();
         In i_add = i_add1;  
         if (i_extension1 > I_0)
         {
            i_add -= i_extension1*gMaxFileSize;
         }
         FileIo(file_name, arV, i_add, aLen, aPutGet, get_robust, aOffV);
         //if (loc_debug)
         //{
            //Mout << "DataCont::Dataio from " << file_name << " i_add1 " << i_add1 << endl;
            //Mout << "DataCont::Dataio aLen " << aLen << " aOffV " << aOffV << endl;
         //}
      }
      else
      {
         In get_robust3 = 0;
         In i_first_c = 0;
         In i_last_c  = 0;
         // i_first_c is the first element in c needed, etc.
         In stride_c = aStrideC;
         if (aStrideC >0) 
         {
            i_first_c = aOffC+1;  // On disc addressing is 1. based.
            i_last_c  = aOffC+1+(aLen-1)*aStrideC;  // On disc addressing is 1. based.
         }
         if (aStrideC <0) 
         {
            i_first_c = 1+aOffC+(aLen-1)*aStrideC;// On disc addressing is 1. based.
            i_last_c  = 1+aOffC;                  // On disc addressing is 1. based.
            stride_c  = -aStrideC;                // Change sign of stride and work upwards from i_first_c
         }
         //if (loc_debug) 
         //{
            //Mout << "DataCont::Dataio gMaxFileSize;     " << gMaxFileSize<< endl;
         //}
         In i_b1     = (i_first_c-1)/gMaxFileSize;
         In i_b2     = (i_last_c-1)/gMaxFileSize;
         //if (loc_debug) 
         //{
            //Mout << "DataCont::Dataio i_first_c: " << i_first_c << endl;
            //Mout << "DataCont::Dataio i_last_c:  " << i_last_c << endl;
            //Mout << "DataCont::Dataio i_b1 ;     " << i_b1<< endl;
            //Mout << "DataCont::Dataio i_b2 :     " << i_b2<< endl;
         //}


         // the first relevant element in each batch i_first_rel_c
         In i_first_rel_c = i_first_c;
         In i_off_v       = aOffV;

         for (In i_batch=i_b1;i_batch<=i_b2;i_batch++)
         {
            // i_first_in_batch is the first element in the c-container in this batch.
            // note that later an exception is made so not necessarily the first in the buffer
            // since it may be skipped in the readin if not needed.
            In i_first_in_batch = i_batch*gMaxFileSize+I_1;
            //In i_help = (i_batch+I_1)*gMaxFileSize;
            //In i_last_in_batch  = min(i_htlpe,arDc.mNdata);
            //In i_last_in_batch  = min(static_cast<In>((i_batch+I_1)*gMaxFileSize),arDc.mNdata);
            In i_last_in_batch  = min(static_cast<In>((i_batch+I_1)*gMaxFileSize),arDc.mNdata);

            // i_add is the address in the full container,
            // i_add_r is the reduced address in the piece on the current file
            // n_io is the number of elements read to be read into memory buffer( s!) 
            // rel: relevant
            // i_first_rel_c the first relevant in the container.
            // n_io the number of elements to io of the container.
            // if the batch does not include the next relevant element we can jump onwards.

            if (i_last_in_batch < i_first_rel_c) continue;
            In i_add = i_first_rel_c;
            In i_add_r = i_add - i_first_in_batch+1;
            In n_rel = (gMaxFileSize-i_add_r)/stride_c+1;
            if ((i_add+(n_rel-1)*stride_c) > i_last_c) n_rel = (i_last_c-i_add)/stride_c+1;
            In n_io  = (n_rel-1)*stride_c+1;
            //if (loc_debug) 
            //{
               //Mout << "DataCont::Dataio i_batch           " << i_batch << endl;
               //Mout << "DataCont::Dataio i_first_in_batch: " << i_first_in_batch << endl;
               //Mout << "DataCont::Dataio i_last_in_batch:  " << i_last_in_batch << endl;
               //Mout << "DataCont::Dataio n_rel  i_first_rel " 
                  //<< n_rel << " " << i_first_rel_c << endl;
               //Mout << "DataCont::Dataio i_add, i_add_r, n_io: " << 
               //i_add << " " << i_add_r << " " << n_io << endl;
            //}

            // Create the filename for the current container-piece
            // up to AND INCLUDING(therefore the -1) gMaxFileSize is in the first file.
            // the next gMaxFileSize is in the next etc.
            In i_extension = (i_add-1)/gMaxFileSize;
            std::ostringstream tmp;
            tmp << arDc.Label() << "_" << i_extension;
            string file_name = tmp.str();

            //Unit stride version
            if (aStrideC==I_1 && aStrideV == I_1 && !aPutToNb && !aAddTo)
            {
               In get_robust2 = get_robust;
               FileIo(file_name,arV,i_add_r,n_io,aPutGet,get_robust2,i_off_v);
               get_robust3 += get_robust2;
               i_off_v += n_io;
               //if (aPutToNb)
               //{
                     //FileIo(file_name,buf_vec,i_add_r_bv,n_buf_vec,IO_PUT);
               //}
            }
            //Non-unit stride version
            else
            {
               // Prepare some variables for the batching over memory
               // buffer vectors 
               // i_bv the adress in the buffer vector.
               // n_rel_bv: the nr of relevant elements in this buffer vector. 
               // i_v:  the address in the MidasVector
   
               In stride_v  = aStrideV; 
   
               // the first and last element in the part of the
               // container that is read is simply the first and last element needed.
   
               In i_off_bv   = 0;
               In i_add_r_bv = i_add_r;
               In n_io_bv    = n_io;
   
               while (n_io_bv >0)
               {
                  In i_bv = i_off_bv;
                  In i_v  = i_off_v;
   
                  In n_buf_vec = min(n_io_bv,gMaxBufSize);  // Read at most gMaxBufSize
                  GeneralMidasVector<T> buf_vec(n_buf_vec);
                  In n_rel_bv = 1+ (n_buf_vec-1-i_off_bv)/stride_c;
                  if (i_off_bv >= n_buf_vec) 
                  {
                     n_rel_bv = 0; // in that case no elements in this round 
                     n_buf_vec = 0;
                  }
   
                  //if (loc_debug) 
                  //{
                     //Mout << "DataCont::Dataio i_off_bv  : " << i_off_bv << endl;
                     //Mout << "DataCont::Dataio i_off_v   : " << i_off_v << endl;
                     //Mout << "DataCont::Dataio i_add_r   : " << i_add_r  << endl;
                     //Mout << "DataCont::Dataio i_add_r_bv: " << i_add_r_bv  << endl;
                     //Mout << "DataCont::Dataio n_rel_bv  : " << n_rel_bv << endl;
                     //Mout << "DataCont::Dataio n_buf_vec : " << n_buf_vec<< endl;
                     //Mout << "DataCont::Dataio n_io_bv   : " << n_io_bv  << endl;
                  //}
   
                  if constexpr(bool(constexpr_io_type & IO_PUT)) 
                  {
                     if (stride_c != 1 || aAddTo) 
                        FileIo(file_name,buf_vec,i_add_r_bv,n_buf_vec,IO_GET,get_robust,I_0);

                     if (!aPutToNb && !aAddTo)
                     {
                        for (In i_rel=0;i_rel<n_rel_bv;i_rel++)
                        {
                           buf_vec[i_bv]=arV[i_v];
                           //Mout << " i_bv, i_v:" << i_bv << " " << i_v << " " 
                            //    << buf_vec[i_bv] << " " << arV[i_v] << endl;
                           i_v  += stride_v;
                           i_bv += stride_c; 
                        }
                     }
                     else if (!aPutToNb)
                     {
                        //Mout << " In addto datacont, put " << endl;
                        //Mout << " test  buf_vec norm " << buf_vec.Norm() << endl;
                        //Mout << " test  arV     norm " << arV.Norm() << endl;
                        //Mout << " aScaleFac " << aScaleFac << endl;
                        //Mout << " buf_vec   " << buf_vec   << endl;
                        //Mout << " arV       " << arV       << endl;
                        for (In i_rel=0;i_rel<n_rel_bv;i_rel++)
                        {
                           buf_vec[i_bv]+= aScaleFac*arV[i_v];
                           //Mout << " i_bv, i_v:" << i_bv << " " << i_v << " " 
                            //    << buf_vec[i_bv] << " " << arV[i_v] << endl;
                           i_v  += stride_v;
                           i_bv += stride_c; 
                        }
                        //Mout << " buf_vec after " << buf_vec   << endl;
                     }
                     else
                     {
                        for (In i_rel=0;i_rel<n_rel_bv;i_rel++)
                        {
                           buf_vec[i_bv]=aNb;
                           i_v  += stride_v;
                           i_bv += stride_c; 
                        }
                     }
                     FileIo(file_name,buf_vec,i_add_r_bv,n_buf_vec,aPutGet,get_robust,I_0);
                  }
                  else if constexpr (bool(constexpr_io_type & IO_GET))
                  {
                     In get_robust2 = get_robust;
                     FileIo(file_name,buf_vec,i_add_r_bv,n_buf_vec,aPutGet,get_robust,I_0);
                     get_robust3 += get_robust2;
                     if (!aAddTo)
                     {
                        for (In i_rel=0;i_rel<n_rel_bv;i_rel++)
                        {
                           arV[i_v] = buf_vec[i_bv];
                           //Mout << " i_bv, i_v:" << i_bv << " " << i_v << " " 
                           //     << buf_vec[i_bv] << " " << arV[i_v] << endl;
                           i_v  += stride_v;
                           i_bv += stride_c; 
                        }
                     }
                     else
                     {
                        for (In i_rel=0;i_rel<n_rel_bv;i_rel++)
                        {
                           arV[i_v] += aScaleFac*buf_vec[i_bv];
                           //Mout << " i_bv, i_v:" << i_bv << " " << i_v << " " 
                           //     << buf_vec[i_bv] << " " << arV[i_v] << endl;
                           i_v  += stride_v;
                           i_bv += stride_c; 
                        }
                     }
                  }
                  else
                  {
                     static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                                    ,  "One of IO_PUT and IO_GET must be set!"
                                    );
                  }
                  i_off_v = i_v;
                  i_off_bv = i_bv;  // Now i_iff_bv is the last element in the buffer vector 
                                    // used in this round. Calculate now offset for the offset 
                                    // in the next round
                  i_off_bv -= gMaxBufSize;
   
                  i_add_r_bv  += gMaxBufSize; // Adress of next part in container that we read in.
                  n_io_bv     -= gMaxBufSize;
               }
   
   
               // End of batching over memory buffer vectors.
   
            }
            i_first_rel_c += n_rel*stride_c;
         }
         get_robust = get_robust3;
      }
   }
   if (get_robust != I_0)
   {
      return Io::ERROR;
   }
   else
   {
      return Io::SUCCESS;
   }

}

/***************************************************************************//**
 * Take the data in arM and put into the DataContainer - or opposite way.
 *
 * @warning
 *    Usage: take care that the contents is assumed to be in order according to
 *    the matrix! But if the dimensions of the original matrix was different, I
 *    mean another number of cols, then addressing goes wrong!  Therefore use
 *    aNcolOld = something.  Default is -1 leading it to be set to aNcols.  In
 *    this way it can read a submatrix in a matrix.
 *
 * @note
 *    The argument matrix always has zero offset and unit stride.
 *
 * @warning
 *    Disclaimer: I didn't write the function, but have tried to write some
 *    more detailed comments about the arguments and their relations. But you
 *    should only have 80% trust that the details are correct, such as when
 *    certain arguments has effect and not. -MBH, July 2018.
 *
 * @param arDc
 *    The GeneralDataCont to read from (IO_GET) or write to (IO_PUT). Intended
 *    to be the `*this` from DataIo(). Making it an argument of a static
 *    function means that the template type DC can match both a const/non-const
 *    GeneralDataCont.
 * @param[in] aPutGet
 *    `IO_GET`, `IO_PUT` or `IO_GET | IO_ROBUST`. For the latter, a file I/O
 *    error will not cause the program to stop.
 * @param arM
 *    GeneralMidasMatrix<T> to assign to (for IO_GET) or get data from (for
 *    IO_PUT).
 *    Has _no effect_ if using IO_PUT and aPutToNb is true.
 * @param[in] aLen
 *    The number of data to be transferred. _Must_ be equal to aNrows * aNcols;
 *    MIDASERROR otherwise. But after that check the variable is _not_ used for
 *    anything!
 * @param[in] aNrows
 *    The number of rows of data to transfer.
 * @param[in] aNcols
 *    The number of columns of data to transfer.
 * @param[in] aOffC
 *    Offset, i.e. index of the place to begin in arDc (the DataCont).
 * @param[in] aStrideC
 *    aStrideC is the stride in the DataContainer.  
 *    NOTE the stride used for both rows and cols!!!!
 *    (Can be used to read all even-even, all odd-odd indexed )
 *    USE WITH CAUTION
 * @param[in] aPutToNb
 *    If true, a number (type T) will be assigned to elements of the container
 *    instead of a matrix; starting at aOffC, aLen elements, with stride
 *    aStrideC.
 *    _Only_ has effect for IO_PUT.
 * @param[in] aNb
 *    The number to be used if aPutToNb is true.
 *    _Only_ has effect if aPutToNb is true.
 * @param[in] aNcolOld
 *    The number of columns in the original matrix. If equal to -1, the value
 *    of aNcols will be used. See warning above.
 * @param[in] aAddTo
 *    If true: Instead of assigning to arM (for IO_GET) or arDc (for IO_PUT),
 *    will add the respective elements (i.e. += assignment) _multiplied_ by the
 *    aScaleFac; i.e. effectively performing an AXPY operation (according to
 *    given strides and offsets):
 *    ~~~
 *        arM  += aScaleFac*arDc    (IO_GET)
 *        arDc += aScaleFac*arM     (IO_PUT)
 *        arDc += aScaleFac*aNb     (IO_PUT and aPutToNb = true)
 *    ~~~
 * @param[in] aScaleFac
 *    The scaling factor to be used if aAddTo is true.
 *    _Only_ has effect if aAddTo is true.
 * @return
 *    GeneralDataCont<T>::Io::SUCCESS if everything went fine. If there's a
 *    file I/O error (if data is on disk, see Storage()), the program generally
 *    stops with a MIDASERROR, unless called with `IO_GET | IO_ROBUST` in which
 *    case it'll continue but return GeneralDataCont<T>::Io::ERROR.
 ******************************************************************************/
template
   <  class T
   >
template
   <  class DC
   ,  int N
   ,  class M
   ,  std::enable_if_t
      <  std::is_same_v<std::remove_const_t<M>, GeneralMidasMatrix<T>>
      ,  int
      >
   >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIoImpl
   (  DC& arDc
   ,  const IoType<N>& aPutGet
   ,  M& arM
   ,  In aLen
   ,  In aNrows
   ,  In aNcols
   ,  In aOffC
   ,  In aStrideC
   ,  bool aPutToNb
   ,  T aNb
   ,  In aNcolOld
   ,  bool aAddTo
   ,  T aScaleFac
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   static_assert  (     constexpr_io_type == IO_GET
                     || constexpr_io_type == IO_PUT
                     || constexpr_io_type == (IO_GET | IO_ROBUST)
                  ,  "IoType must be IO_GET, IO_PUT, or IO_GET | IO_ROBUST."
                  );
   if (aLen != aNrows*aNcols)
   {
      MIDASERROR(" DataCont:DataIo - Matrix IO error" + arDc.Label());
   }

   In get_robust = I_0;
   
   In i_off_c = aOffC;
   In n_col_old = aNcols;
   if (aNcolOld != -I_1)
   {
      n_col_old = aNcolOld;
   }

   In row_off = I_0;
   In row_stride = I_1;  
   In col_off = I_0;
   In col_stride = I_1;  
   // Only tested for the above 4!
   bool all_ok = true;
   if (arDc.mAllInCore) 
   {
      if constexpr(bool(constexpr_io_type & IO_GET))
      {
         if (!aAddTo)
         {
            for (In i_row = I_0; i_row < aNrows; i_row++)
            {
               In i_row_nr = row_off + i_row*row_stride;
               for (In i_col = I_0; i_col < aNcols; i_col++)
               {
                  In i_col_nr = col_off + i_col*col_stride;
                  arM[i_row_nr][i_col_nr] = arDc.mData[i_off_c + i_col*aStrideC];
               }
               i_off_c += n_col_old*aStrideC; 
            }
         }
         else
         {
            for (In i_row = 0;i_row<aNrows;i_row++)
            {
               In i_row_nr = row_off + i_row*row_stride;
               for (In i_col = 0;i_col<aNcols;i_col++)
               {
                  In i_col_nr = col_off + i_col*col_stride;
                  arM[i_row_nr][i_col_nr] += aScaleFac*arDc.mData[i_off_c+i_col*aStrideC];
               }
               i_off_c += n_col_old*aStrideC; 
            }
         }
      }
      else if constexpr (bool(constexpr_io_type & IO_PUT))
      {
         if (!aAddTo)
         {
            for (In i_row = I_0; i_row < aNrows; i_row++)
            {
               In i_row_nr = row_off + i_row*row_stride;
               for (In i_col = I_0; i_col < aNcols; i_col++)
               {
                  In i_col_nr = col_off + i_col*col_stride;
                  arDc.mData[i_off_c + i_col*aStrideC] = arM[i_row_nr][i_col_nr];
               }
               i_off_c += n_col_old*aStrideC; 
            }
         }
         else
         {
            for (In i_row = 0;i_row<aNrows;i_row++)
            {
               In i_row_nr = row_off + i_row*row_stride;
               for (In i_col = 0;i_col<aNcols;i_col++)
               {
                  In i_col_nr = col_off + i_col*col_stride;
                  arDc.mData[i_off_c+i_col*aStrideC] += aScaleFac*arM[i_row_nr][i_col_nr];
               }
               i_off_c += n_col_old*aStrideC; 
            }
         }
      }
      else
      {
         static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                        ,  "One of IO_PUT and IO_GET must be set!"
                        );
      }
   }
   if (arDc.mAllOnDisc) 
   {
      // If being IO_ROBUST set this to achieve desired robustness down the line.
      if constexpr(bool(constexpr_io_type & IO_ROBUST))
      {
         get_robust = -I_1;
      }

      for (In i_row=I_0; i_row<aNrows; i_row++)
      {
         In i_row_nr = row_off + i_row*row_stride;
         GeneralMidasVector<T> tmp(arM.Ncols());
         if constexpr(bool(constexpr_io_type & IO_PUT))
         {
            arM.GetRow(tmp,i_row_nr);
         }
         Io err = arDc.DataIo(aPutGet,tmp,aNcols,i_off_c,aStrideC,col_off,col_stride,aPutToNb,aNb);
         if constexpr(bool(constexpr_io_type & IO_GET))
         {
            arM.AssignRow(tmp,i_row_nr);
         }
         i_off_c += n_col_old*aStrideC; // This solution also gives a stride to the rows, seems not easy to predict unless you know
         if (get_robust != 0 && err == Io::ERROR) all_ok = false;
      }
   }
   if (get_robust != I_0 && !all_ok)
   {
      return Io::ERROR;
   }
   else
   {
      return Io::SUCCESS;
   }
}

/***************************************************************************//**
 * Get/put a single element.
 *
 * @param arDc
 *    The GeneralDataCont to read from (IO_GET) or write to (IO_PUT). Intended
 *    to be the `*this` from DataIo(). Making it an argument of a static
 *    function means that the template type DC can match both a const/non-const
 *    GeneralDataCont.
 * @param[in] aPutGet
 *    `IO_GET`, `IO_PUT` or `IO_GET | IO_ROBUST`. For the latter, a file I/O
 *    error will not cause the program to stop.
 * @param[in] aAddress
 *    The address within the GeneralDataCont object to assign to/read from.
 * @param aNb
 *    Scalar (of type T) to assign to (for IO_GET) or get data from (for
 *    IO_PUT).
 * @return
 *    GeneralDataCont<T>::Io::SUCCESS if everything went fine. If there's a
 *    file I/O error (if data is on disk, see Storage()), the program generally
 *    stops with a MIDASERROR, unless called with `IO_GET | IO_ROBUST` in which
 *    case it'll continue but return GeneralDataCont<T>::Io::ERROR.
 ******************************************************************************/
template
   <  class T
   >
template
   <  class DC
   ,  int N
   ,  class U
   ,  std::enable_if_t
      <  std::is_same_v<std::remove_const_t<U>, T>
      ,  int
      >
   >
typename GeneralDataCont<T>::Io GeneralDataCont<T>::DataIoImpl
   (  DC& arDc
   ,  const IoType<N>& aPutGet
   ,  const In& aAddress
   ,  U& aNb
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   static_assert  (     constexpr_io_type == IO_GET
                     || constexpr_io_type == IO_PUT
                     || constexpr_io_type == (IO_GET | IO_ROBUST)
                  ,  "IoType must be IO_GET, IO_PUT, or IO_GET | IO_ROBUST."
                  );
            
   In get_robust = I_0;
   
   if (arDc.mAllInCore) 
   {
      if constexpr(bool(constexpr_io_type & IO_PUT))
      {
         arDc.mData[aAddress] = aNb;
      }
      else if constexpr (bool(constexpr_io_type & IO_GET))
      {
         aNb = arDc.mData[aAddress];
      }
      else
      {
         static_assert  (  !((constexpr_io_type & IO_GET) || (constexpr_io_type & IO_PUT))
                        ,  "One of IO_PUT and IO_GET must be set!"
                        );
      }
   }
   if (arDc.mAllOnDisc) 
   {
      In i_add1 = aAddress + I_1;
      In i_extension1 = (i_add1-I_1)/gMaxFileSize;
      std::ostringstream tmp;
      tmp << arDc.Label() << "_" << i_extension1;
      string file_name = tmp.str();
      if (i_extension1 > I_0) i_add1 -= i_extension1*gMaxFileSize;

      if constexpr(bool(constexpr_io_type & IO_ROBUST))
      {
         get_robust = -I_1;
      }
      FileIo(file_name, aNb, i_add1, aPutGet, get_robust); 
   }
   if (get_robust != 0)
   {
      return Io::ERROR;
   }
   else
   {
      return Io::SUCCESS;
   }
}

/***************************************************************************//**
 * @return
 *    Norm of entire container, or of elements in range [aOff,aOff+aN).
 ******************************************************************************/
template<class T>
typename GeneralDataCont<T>::real_t GeneralDataCont<T>::Norm(In aOff, In aN) const
{
   return sqrt(Norm2(aOff, aN));
}

/***************************************************************************//**
 * @return
 *    Calculate sum of aN elements staring at address aOff.
 ******************************************************************************/
template<class T>
T GeneralDataCont<T>::SumEle(In aOff, In aN) const
{
   T sum = 0;
   auto add_to_sum = [&sum](const T& a) -> void
      {
         sum += a;
      };
   Uin beg = aOff;
   Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
   LoopOverElements(add_to_sum, beg, end);
   return sum;
}

/***************************************************************************//**
 * @return
 *    Calculate sum of absolute values of elements of whole container or in
 *    range [aOff,aOff+aN).
 ******************************************************************************/
template<class T>
typename GeneralDataCont<T>::real_t GeneralDataCont<T>::SumAbsEle(In aOff, In aN) const
{
   real_t sum = 0;
   auto add_to_sum = [&sum](const T& a) -> void
      {
         sum += midas::util::AbsVal(a);
      };
   Uin beg = aOff;
   Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
   LoopOverElements(add_to_sum, beg, end);
   return sum;
}

/***************************************************************************//**
 * Calculate norm squared of aN elements staring at address aOff.
 *
 * @param[in] aOff
 *    Index of first element in the DataCont for which to calculate
 *    norm-squared.
 * @param[in] aN
 *    Number of elements for which to calculate dot product. If equal to -1
 *    (default) will calculate until end of container.
 * @return
 *    Norm-squared of range of elements.
 ******************************************************************************/
template<class T>
typename GeneralDataCont<T>::real_t GeneralDataCont<T>::Norm2(In aOff, In aN) const
{
   real_t norm2 = 0;
   auto add_to_norm2 = [&norm2](const T& a) -> void
      {
         norm2 += midas::util::AbsVal2(a);
      };
   Uin beg = aOff;
   Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
   LoopOverElements(add_to_norm2, beg, end);
   return norm2;
}

/***************************************************************************//**
 * Calculate difference norm squared of aN elements staring at address aOff.
 *
 * @param[in] arOther
 *    The container with which to measure difference norm.
 * @param[in] aOff
 *    Index of first element in the DataCont for which to calculate
 *    norm-squared.
 * @param[in] aN
 *    Number of elements for which to calculate dot product. If equal to -1
 *    (default) will calculate until end of container.
 * @return
 *    Difference norm-squared of range of elements.
 ******************************************************************************/
template<class T>
typename GeneralDataCont<T>::real_t GeneralDataCont<T>::DiffNorm2
   (  const GeneralDataCont<T>& arOther
   ,  In aOff
   ,  In aN
   )  const
{
   real_t diffnorm2 = 0;
   auto add_to_diffnorm2 = [&diffnorm2](const T& a, const T& b) -> void
      {
         diffnorm2 += midas::util::AbsVal2(a - b);
      };
   Uin beg = aOff;
   Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
   LoopOverElements(add_to_diffnorm2, arOther, beg, end);
   return diffnorm2;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<class T>
template<bool CONJ_THIS>
typename GeneralDataCont<T>::real_t GeneralDataCont<T>::DiffNorm2Scaled
   (  const GeneralDataCont& arOther
   ,  const T aThisScale
   ,  const T aOtherScale
   ,  In aOff
   ,  In aN
   )  const
{
   real_t diffnorm2 = 0;
   auto add_to_diffnorm2 = [&diffnorm2,aThisScale,aOtherScale](const T& a, const T& b) -> void
      {
         T eff_a = a;
         if constexpr(CONJ_THIS)
         {
            eff_a = midas::math::Conj(eff_a);
         }
         diffnorm2 += midas::util::AbsVal2(aThisScale*eff_a - aOtherScale*b);
      };
   Uin beg = aOff;
   Uin end = (aN < 0)? std::numeric_limits<Uin>::max(): beg + aN;
   LoopOverElements(add_to_diffnorm2, arOther, beg, end);
   return diffnorm2;
}

/**
* << overload for ostream, rather inefficient, not for large system
* */
template<class T>
std::ostream& operator<<(std::ostream& arOut, const GeneralDataCont<T>& arVec1)
{ 
   GeneralMidasVector<T> tmp(arVec1.Size());
   auto status = arVec1.DataIo(IO_GET | IO_ROBUST,tmp,arVec1.Size());
   if (status != GeneralDataCont<T>::Io::ERROR)
   {
      arOut << arVec1.Label() << ": ";
      for (In i=I_0;i<arVec1.Size();i++) arOut << " " << tmp[i];
      //arOut << endl;
   }
   else
   {
      arVec1.PrintDiagnostics(arOut);
   }
   return arOut;
}

/**
 * Intended for debug purposes. Prints internals of the object; label, storage
 * mode, amount of data, data itself (if any; and only first 100 elements).
 * Also tries to check for existance of associated file(s), and if any, tries
 * to print sizes and contents (only first 100 elements).
 *
 * @note
 *    Assumes associated file(s) to have name mLabel_<int>, with the integer
 *    starting at 0, and increasing continuously for different "chunks" of the
 *    container.
 *
 * @param arOut
 *    The ostream to print to.
 **/
template<class T>
void GeneralDataCont<T>::PrintDiagnostics
   (  std::ostream& arOut
   )  const
{
   // Object internal info.
   arOut << "GeneralDataCont<T>::PrintDiagnostics\n"
         << "Object internals:\n"
         << "   mAllInCore     = " << mAllInCore << '\n'
         << "   mAllOnDisc     = " << mAllOnDisc << '\n'
         << "   mSaveUponDecon = " << mSaveUponDecon << '\n'
         << "   mNdata         = " << mNdata << '\n'
         << "   mLabel         = " << mLabel << '\n'
         << "   mData.Size()   = " << mData.Size() << '\n'
         << "   mData          = (only first 100 elements)\n"
         ;
   for(Uin i = I_0; i < mData.Size() && i < I_100; ++i)
   {
      arOut << "      mData["<<std::setw(2)<<i<<"] = " << mData[i] << '\n';
   }
   // Everything we can dig out about associated file(s).
   arOut << "Associated file(s):\n";
   // Check for files of type mLabel_<int>, which is the DataCont filename format.
   Uin i = I_0;
   while(midas::filesystem::Exists(mLabel + "_" + std::to_string(i)))
   {
      const auto file = mLabel+"_"+std::to_string(i);
      arOut << file << '\n'
            << "   Exists         = " << midas::filesystem::Exists(file) << '\n'
            << "   IsFile         = " << midas::filesystem::IsFile(file) << '\n'
            << "   IsDir          = " << midas::filesystem::IsDir(file) << '\n'
            << "   IsSymlink      = " << midas::filesystem::IsSymlink(file) << '\n'
            ;
      const auto size = midas::filesystem::Size(file);
      arOut << "   Size/bytes     = " << size << '\n';
      arOut << "   Size/sizeof(T) = " << size/sizeof(T) << '\n';
      if (size > 0 && midas::filesystem::IsFile(file))
      {
         std::ifstream ifs(file, std::ios::in|std::ios::binary);
         arOut << "   Contents       = (only first 100 elements)\n";
         Uin i = I_0;
         T elem;
         while (read_binary(elem, ifs) && i < I_100)
         {
            arOut << "      type T "<<std::setw(2)<<i++<<" = " << elem << '\n';
         }
         ifs.close();
      }
      arOut << std::flush;
      ++i;
   }
   if (i == I_0)
   {
      arOut << "   Did not find file with name '" << mLabel << "_0'\n";
   }
   arOut << std::flush;
}

/**
 *
 **/
template<class T> class CpmAbsSmaller
{
   public:
      bool operator()(const T& a,const T& b) const {return std::abs(a) < std::abs(b);}
}; 
template<class T> class CpmAbsGreater
{
   public:
      bool operator()(const T& a,const T& b) const {return std::abs(a) > std::abs(b);}
}; 
/**
* Auxilary class for comparison of distances relative to a set of reference values. 
* */
template
   <  class T
   >
class NbVecCpmAbsExt
{
   public:
      using real_t = midas::type_traits::RealTypeT<T>;
      //
      const std::vector<T>& mrNbVec; // only data 
      In                    mMode;   // only data 
      NbVecCpmAbsExt
         (  const std::vector<T>& arNbVec
         ,  In aMode
         )
         :  mrNbVec(arNbVec)
         ,  mMode  (aMode)
      {
      }

      //
      bool operator()(const T& a,const T& b) const;
}; 
/***************************************************************************//**
 * Example (I _think_ this is how it works, but check the code to make sure for
 * yourself. -MBH, July 2018.):
 *
 * Say mrNbVec = {0, -7, 2, 13}.
 *
 * mMode < 0: Then (a = -8, b = 4) returns true,
 *    because |-8 - (-7)| = 1 which is < |4 - 2| = 2.
 *
 * mMode > 0: Then (a = -8, b = 4) returns true,
 *    because |-8 - 13| = 21 which is > |4 - (-7)| = 11.
 *
 * @return
 *    mMode < 0: Whether a is strictly closer than b to any of the values
 *               in mrNbVec.
 *    mMode > 0: Whether a is strictly farther away than b from any of the
 *               values in mrNbVec.
 *    Closer/farther is in an absolute value sense. The mrNbVec value can be
 *    different for a and b.
 ******************************************************************************/ 
template<class T> 
bool NbVecCpmAbsExt<T>::operator()(const T& a, const T& b) const 
{
   real_t a_ext(C_NB_MAX);
   real_t b_ext(C_NB_MAX);
   
   if (mMode>0) 
   {
      a_ext = b_ext = -C_NB_MAX;
   }

   for(const auto& a_iter : mrNbVec)
   { 
      if (mMode<0)
      { 
         a_ext = min(a_ext, std::abs(a-a_iter)); 
         b_ext = min(b_ext, std::abs(b-a_iter)); 
      }
      else 
      { 
         a_ext = max(a_ext, std::abs(a-a_iter)); 
         b_ext = max(b_ext, std::abs(b-a_iter)); 
      }
   }
   
   if (mMode>0) 
   {
      return std::abs(a_ext) > std::abs(b_ext);
   }

   return std::abs(a_ext) < std::abs(b_ext); // Standard is LessThan 
}

/***************************************************************************//**
 * Find aN extreme values; min/max for actual values or their absolute values.
 *
 * @param[out] arMinAdds
 *    Will hold the addresses/indices of the extremum values upon return.
 * @param[out] arExtrema
 *    Will hold the extremum values upon return.
 * @param[in] aN
 *    Number of extremum addresses/values to find.
 * @param[in] aMin
 *    Find addresses of...
 *    -  -1: minimum values.
 *    -  +1: maximum values.
 *    -  -2: minimum absolute values.
 *    -  +2: maximum absolute values.
 *    Any other value will cause a MIDASERROR.
 ******************************************************************************/
template<class T>
void GeneralDataCont<T>::AddressOfExtrema
   (  std::vector<In>& arMinAdds
   ,  std::vector<T>& arExtrema
   ,  const In aN
   ,  const In aMin
   )  const
{
   //Mout << " Add Ext : " << Label() << " size " << Size() << "storage " << Storage() << endl;
   //Mout << " aMin " << aMin << endl;
   arExtrema.clear();
   arExtrema.reserve(aN);
   vector<bool> a_found(aN,false);
   typedef typename std::vector<T>::iterator NbVecIt;

   In n_data = mNdata;
   while (n_data > 0)/// Batch over in core buffers 
   {
      In i_extension = (n_data-1)/(gMaxBufSize/I_2); 
      In add = i_extension*(gMaxBufSize/I_2);
      In in_this = min((gMaxBufSize/I_2),n_data-add);

      In nsize = arExtrema.size();
      arExtrema.reserve(nsize+in_this);

      GeneralMidasVector<T> tmp1(in_this);
      DataIo(IO_GET,tmp1,in_this,add);

      for (In i=I_0;i<in_this;i++)
         arExtrema.push_back(tmp1[i]);          // add a new part of the vector
      if (aMin == -I_1)
         std::sort(arExtrema.begin(),arExtrema.end());
      else if (aMin == I_1 )
         std::sort(arExtrema.begin(),arExtrema.end(),std::greater<Nb>());
      else if (aMin == -I_2 )
         std::sort(arExtrema.begin(),arExtrema.end(),CpmAbsSmaller<Nb>());
      else if (aMin == +I_2 )
         std::sort(arExtrema.begin(),arExtrema.end(),CpmAbsGreater<Nb>());
      else
         MIDASERROR ("Unknown aMin on input in DataCont::AddressOfExtrema " );
      
      NbVecIt a_iter=arExtrema.begin();               //keep only the interesting aN part
      In n_arExtrema = arExtrema.size(); 
      for (In i=I_0;i<min(aN,n_arExtrema);i++) a_iter++;
      arExtrema.erase(a_iter,arExtrema.end()); 

      n_data -= in_this;
   }
   
   n_data = mNdata;
   arMinAdds.clear();
   arMinAdds.resize(arExtrema.size(),-I_1);
   while (n_data > 0)/// Batch over in core buffers 
   {
      In i_extension = (n_data-1)/(gMaxBufSize/I_2); 
      In add = i_extension*(gMaxBufSize/I_2);
      In in_this = min((gMaxBufSize/I_2),n_data-add);

      GeneralMidasVector<T> tmp1(in_this);
      DataIo(IO_GET,tmp1,in_this,add);

      In j=-I_1;
      for(NbVecIt a_iter=arExtrema.begin();a_iter!=arExtrema.end();a_iter++) 
      {
         ++j;
         if (a_found[j]) continue;

         for (In i=I_0;i<in_this;i++)
         {
            // add if it has correct value and has not already been added.
            if (libmda::numeric::float_eq(tmp1[i],*a_iter) && 
                  (std::find(arMinAdds.begin(),arMinAdds.end(),i+add)==arMinAdds.end()))  
            {
               arMinAdds[j] = i+add;
               a_found[j] = true;
               break;   // break inner loop to ensure that each arExtrema
                        // only is represented once 
            }
         }
      }
      n_data -= in_this;
   }
   
   if (gDebug) 
   {
      Mout << " DataCont extremum elements are: " << endl;
      In i = 0;
      for(NbVecIt a_iter=arExtrema.begin();a_iter!=arExtrema.end();a_iter++) 
      {
         Mout << " value " << *a_iter << " found " << a_found[i] << " address " << arMinAdds[i] << endl;
         i++;
      }
   }
}

/***************************************************************************//**
 * Find the aN values and addresses that are either closest to (aMin = -2) or
 * farthest away from (aMin = +2) any of the values in arNbs.
 *
 * Compared with the other AddressOfExtreama(), in this version the "extremum
 * values" are relative to a set of input values.
 * So, it can determine the numbers and addresses for those that in an absolute
 * sense are closest to the input set arNbs.
 * (Or farthest away.)
 *
 * @param[in] arNbs
 *    The values used as reference for closeness/distance, see description
 *    above.
 * @param[out] arMinAdds
 *    Will hold the addresses/indices of the "extremum" values upon return.
 * @param[out] arExtrema
 *    Will hold the "extremum" values upon return.
 * @param[in] aN
 *    Number of extremum addresses/values to find.
 * @param[in] aMin
 *    Find addresses of...
 *    -  -2: values closest to one of those in arNbs.
 *    -  +2: values farthest away from one of those in arNbs.
 *    Any other value will cause a MIDASERROR.
 ******************************************************************************/
template<class T>
void GeneralDataCont<T>::AddressOfExtrema
   (  const std::vector<T>& arNbs
   ,  std::vector<In>& arMinAdds
   ,  std::vector<T>& arExtrema
   ,  const In aN
   ,  const In aMin
   )  const
{
   arExtrema.clear();
   arExtrema.reserve(aN);
   vector<bool> a_found(aN,false);
   typedef typename vector<T>::iterator NbVecIt;

   In n_data = mNdata;
   while (n_data > 0)/// Batch over in core buffers 
   {
      In i_extension = (n_data-1)/(gMaxBufSize/I_2); 
      In add = i_extension*(gMaxBufSize/I_2);
      In in_this = min((gMaxBufSize/I_2),n_data-add);

      In nsize = arExtrema.size();
      arExtrema.reserve(nsize+in_this);

      GeneralMidasVector<T> tmp1(in_this);
      DataIo(IO_GET,tmp1,in_this,add);

      for (In i=I_0;i<in_this;i++) 
         arExtrema.push_back(tmp1[i]);          // add a new part of the vector

      if (aMin == -I_2 || aMin == I_2) 
         std::sort(arExtrema.begin(),arExtrema.end(),NbVecCpmAbsExt<T>(arNbs,aMin));  //sort
      else 
         MIDASERROR ("Unknown aMin on input in DataCont::AddressOfExtrema " );
      
      NbVecIt a_iter=arExtrema.begin();                              //keep only the interesting aN part
      In n_arExtrema = arExtrema.size(); 
      for (In i=I_0;i<min(aN,n_arExtrema);i++) 
         a_iter++;
      arExtrema.erase(a_iter,arExtrema.end()); 

      n_data -= in_this;
   }

   n_data = mNdata;
   arMinAdds.clear();
   arMinAdds.resize(arExtrema.size(),-I_1);
   while (n_data > 0)/// Batch over in core buffers 
   {
      In i_extension = (n_data-1)/(gMaxBufSize/I_2); 
      In add = i_extension*(gMaxBufSize/I_2);
      In in_this = min((gMaxBufSize/I_2),n_data-add);

      GeneralMidasVector<T> tmp1(in_this);
      DataIo(IO_GET,tmp1,in_this,add);

      In j=-I_1;
      for(NbVecIt a_iter=arExtrema.begin();a_iter!=arExtrema.end();a_iter++) 
      {
         ++j;
         if (a_found[j]) continue;

         for (In i=I_0;i<in_this;i++)
         {
            // add if it has right value and has not already been added.
            if (libmda::numeric::float_eq(tmp1[i],*a_iter) && 
                  (std::find(arMinAdds.begin(),arMinAdds.end(),i+add)==arMinAdds.end()))  
            {
               arMinAdds[j] = i+add;
               a_found[j] = true;
               break;   // break inner loop to ensure that each arExtrema
                        // only is represented once 
            }
         }
      }
      n_data -= in_this;
   }
   if (gDebug) 
   {
      Mout << " DataCont (vers2) extremum elements are: " << endl;
      In i = 0;
      for(NbVecIt a_iter=arExtrema.begin();a_iter!=arExtrema.end();a_iter++) 
      {
         Mout << " value " << *a_iter << " found " << a_found[i] << " address " << arMinAdds[i] << endl;
         i++;
      }
   }
}

template<class T>
In GeneralDataCont<T>::SizeOut() const 
{
   Mout << " Output Size for Datacont with label " << mLabel << endl;
   Mout << " Bools sizeof =       "  << sizeof(bool)   << " times  3 " << endl;
   Mout << " In    sizeof =       "  << sizeof(In)     << " times  1 " << endl;
   Mout << " string sizeof =      "  << sizeof(string) << " times  1 " << endl;
   Mout << " string size   =      "  << mLabel.size() << endl;
   Mout << " MidasVector sizeof = "  << sizeof(mData)  << " times 1  " << endl;
   Mout << " mData contains       "  << mData.Size() << " T  of sizeof " 
      << sizeof(T) << endl;
   In tot_size =    mData.Size()*sizeof(T) + sizeof(mData)+sizeof(bool)*3
      +sizeof(In)+sizeof(mLabel)+mLabel.size();
   Mout << " Total size is roughly = " <<  tot_size << endl;
   return tot_size;
}

template<class T>
void GeneralDataCont<T>::TransformMx
   (  const In aNrows
   ,  const In aNcols
   ,  const GeneralDataCont<T>& arDcIn
   ,  GeneralDataCont<T>& arDcOut
   )  const
{
   if ((aNcols != arDcIn.mNdata) || (aNcols != arDcOut.mNdata))
   {
      Mout << "Error in DataCont::TransformMx(): "
           << "aDcIn or aDcOut has dimension different from aNrows."
           << endl;
      MIDASERROR("Error in DataCoint::TransformMx");
      return;
   }

   T dotprod;                            // Dot product of row i and aDcIn.
   for (In i=I_0; i<aNrows; i++)          // Loop over each row in matrix and make dot product
   {                                      // with vector in arDcIn.
      if (mAllInCore)
         if (arDcIn.mAllInCore)
         {
            //Mout << "DataCont::TransformMx(): All in core!" << endl;
            dotprod = T(C_0);
            for (In j=I_0; j<aNcols; j++)
               dotprod += mData[i*aNcols + j] * arDcIn.mData[j];
         }
         else        // Need to get arDcIn in chunks from disc.
         {
            //Mout << "DataCont::TransformMx(): arDcIn NOT IN CORE." << endl;
            dotprod = T(C_0);
            for (In i_chunk=I_0; i_chunk <= (aNcols-I_1)/gMaxBufSize; i_chunk++)
            {
               In addr = i_chunk * gMaxBufSize;
               In in_this = min(gMaxBufSize, aNcols-(i_chunk*gMaxBufSize));
               GeneralMidasVector<T> tmp(in_this);
               arDcIn.DataIo(IO_GET, tmp, in_this, addr);
               for (In j=I_0; j<in_this; j++)
                  dotprod += mData[i*aNcols + addr + j] * tmp[j];
            }
         }
      else    // Need to get matrix (this container) in chunks from disc.
      {
         //Mout << "DataCont::TransformMx(): Matrix NOT IN CORE" << endl;
         dotprod = T(C_0);
         In base_addr = i*aNcols;
         for (In i_chunk = I_0; i_chunk <= (aNcols-1)/gMaxBufSize; i_chunk++)
         {
            In addr = base_addr + i_chunk * gMaxBufSize;
            In in_this = min(gMaxBufSize, aNcols-(i_chunk*gMaxBufSize));
            GeneralMidasVector<T> tmp_row(in_this);
            DataIo(IO_GET, tmp_row, in_this, addr);
            if (arDcIn.mAllInCore)           // Decide how to get data from vector.
               for (In j=I_0; j<in_this; j++)
                  dotprod += tmp_row[j] * arDcIn.mData[i_chunk*gMaxBufSize + j];
            else                             // We also need arDcIn in chunks.
            {
               //Mout << "   ...and arDcIn neither." << endl;
               GeneralMidasVector<T> tmp_col(in_this);
               arDcIn.DataIo(IO_GET, tmp_col, in_this, i_chunk*gMaxBufSize);
               for (In j=I_0; j<in_this; j++)
                  dotprod += tmp_row[j] * tmp_col[j];
            }  
         }
      }
      arDcOut.DataIo(IO_PUT, i, dotprod);
   }
}


/**
 * calculate norm of difference
 **/
template<class T>
Nb DifferenceNorm(GeneralDataCont<T>& arDc1, GeneralDataCont<T>& arDc2)
{
   T diff_norm = 0;

   if (arDc1.Size() != arDc2.Size()) 
      MIDASERROR( "Sizes does not match in DataCont Difference Norm");

   for(int i=0; i<arDc1.Size(); ++i)
   {
      Nb element1;
      Nb element2;
      arDc1.DataIo(IO_GET, i, element1);
      arDc2.DataIo(IO_GET, i, element2);
      T diff = element1 - element2;
      diff_norm += diff*diff;
   }

   return sqrt(diff_norm);
}

/***************************************************************************//**
 * @note
 *    If data on disc, makes some (potentially large) temporary copies.
 ******************************************************************************/
template<typename T>
template<class InpIter>
void GeneralDataCont<T>::Insert
   (  Uin aPos
   ,  InpIter aBeg
   ,  InpIter aEnd
   )
{
   const Uin dist = std::distance(aBeg, aEnd);
   if (InMem())
   {
      mData.Insert(aPos, aBeg, aEnd);
      SetNewSize(mData.Size());
   }
   else if (OnDisc())
   {
      if (dist > 0)
      {
         GeneralMidasVector<T> tmp(Size() - aPos);
         if (tmp.Size() > 0)
         {
            DataIo(IO_GET, tmp, tmp.Size(), aPos);
         }
         SetNewSize(Size()+dist, true);
         GeneralMidasVector<T> range(std::vector<T>(aBeg, aEnd));
         DataIo(IO_PUT, range, range.Size(), aPos);
         if (tmp.Size() > 0)
         {
            DataIo(IO_PUT, tmp, tmp.Size(), aPos + dist);
         }
      }
   }
   else
   {
      MIDASERROR("Neither InMem() nor OnDisc(); faulty impl.");
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T>
void GeneralDataCont<T>::Insert
   (  Uin aPos
   ,  T aVal
   )
{
   std::vector<T> tmp = {aVal};
   this->Insert(aPos, tmp.begin(), tmp.end());
}

/***************************************************************************//**
 * @note
 *    If data on disc, makes some (potentially large) temporary copies.
 ******************************************************************************/
template<typename T>
void GeneralDataCont<T>::Erase
   (  Uin aBeg
   ,  Uin aEnd
   )
{
   const Uin end = std::min(aEnd, Uin(Size()));
   if (aBeg < end)
   {
      const Uin dist = end - aBeg;
      if (InMem())
      {
         mData.Erase(aBeg, end);
         SetNewSize(mData.Size());
      }
      else if (OnDisc())
      {
         GeneralMidasVector<T> tmp(Size() - end);
         if (tmp.Size() > 0)
         {
            DataIo(IO_GET, tmp, tmp.Size(), end);
            DataIo(IO_PUT, tmp, tmp.Size(), aBeg);
         }
         SetNewSize(aBeg + tmp.Size());
      }
      else
      {
         MIDASERROR("Neither InMem() nor OnDisc(); faulty impl.");
      }
   }
}
