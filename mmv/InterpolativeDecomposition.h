/**
************************************************************************
* 
* @file    InterpolativeDecomposition.h
*
* @date    20-02-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Interpolative decomposition (ID) for matrices.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef INTERPOLATIVEDECOMPOSITION_H_INCLUDED
#define INTERPOLATIVEDECOMPOSITION_H_INCLUDED

#include <memory>

#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/LapackInterface.h"

#include "mmv/MidasMatrix.h"

#include "util/CallStatisticsHandler.h"

namespace midas
{

namespace detail
{
/**
 *
 **/
template
   <  class T
   >
struct ID_struct_traits
{
   using ac_type = T;
   using t_type = T;
   using p_type = int;
};

} /* namespace detail */


/**
 * Struct for holding the matrix ID:
 *
 * A*P = A_c [I|T]
 *
 * A is an m x n matrix
 * P is an n x n permutation matrix
 * A_c is an m x k matrix consisting of the first k columns of A*P
 * I is a k x k identity matrix
 * T is a k x (n-k) matrix
 *
 * This can also be written as: A = A_c*X, where X = [I|T]*P^T
 **/
template
   <  class T
   >
struct ID_struct
{
   using ac_type = typename detail::ID_struct_traits<T>::ac_type;
   using t_type = typename detail::ID_struct_traits<T>::t_type;
   using p_type = typename detail::ID_struct_traits<T>::p_type;

   //! Error bound for the ID decomposition (norm of discarded R_{22} matrix)
   T mErrorBound = static_cast<T>(-1.);

   //! Estimated largest singular value of R
   T mSmax = static_cast<T>(0.);

   //! Estimated smallest singular value of the truncated R matrix
   T mSmin = static_cast<T>(0.);

   //! Estimated largest neglected singular value s_{rank+1}
   T mSRp1 = static_cast<T>(0.);

   //! Infos from lapack routines
   int mQrInfo = 0;
   int mLsInfo = 0;

   //! Dimensions of matrix
   int mNrows = 0;
   int mNcols = 0;

   //! Approximate rank of matrix
   int mRank = 0;

   //! The reduced-column-space matrix
   std::unique_ptr<ac_type[]> mAc = nullptr;
   
   //! The T matrix
   std::unique_ptr<t_type[]> mT = nullptr;

   //! The permutation (i'th column of A*P is the mP[i]'th column of A)
   std::unique_ptr<p_type[]> mP = nullptr;

   //! The inverse permutation (i'th column of X is mPt[i]'th column of [I|T]). 
   std::unique_ptr<p_type[]> mPt = nullptr;

   //! Get (i,j)'th element of X matrix. We have X*P = [I|T].
   t_type XElem
      (  int i
      ,  int j
      )
   {
      auto col = mPt[j]; // Column of [I|T] matrix
      if (  col < this->mRank
         )
      {
         // Return element of I matrix
         return (i == col) ? t_type(1.) : t_type(0.);
      }
      else if  (  this->mRank == 0
               )
      {
         // T matrix is zero
         return t_type(0.);
      }
      else
      {
         // Return element of T matrix
         auto t_col = col-this->mRank;

         return mT[i + this->mRank*t_col];
      }
   }
};


namespace detail
{

template
   <  class T
   >
int GetQrLwork
   (  int m
   ,  int n
   )
{
   int lwork_query = -1;
   int lda = std::max(1,m);
   int jpvt;
   T a;
   T tau;
   T work; // work array, will only use 1-element
   int info;
   
   lapack_interface::geqp3 (  &m, &n
                           ,  &a, &lda, &jpvt
                           ,  &tau, &work, &lwork_query, &info
                           );

   if (  info != 0
      )
   {
      MIDASERROR("info != 0 in GEQP3 workspace query!");
   }

   return static_cast<int>(work);
}

} /* namespace detail */


/**
 * Perform matrix ID
 *
 * @param aMatrix       Matrix stored in column-major pointer (Lapack style)
 * @param aMatrixNorm2  Norm2 of matrix
 * @param aRows         Number of rows
 * @param aCols         Number of columns
 * @param aRcond        Truncation threshold for the ID
 * @param aMaxRank      Max rank of decomposition (-1 means no max rank)
 * @param aRandomize    Use randomized ID algorithm
 * @param aSaveAc       Save the skeleton matrix A_c.
 * @param aAbsConv      Truncate based on absolute value of largest neglected singular value
 * @param aIoLevel      IO level
 *
 * @return
 *    ID_struct containing all necessary information
 **/
template
   <  class T
   >
ID_struct<T> InterpolativeDecomposition
   (  T* aMatrix
   ,  T aMatrixNorm2
   ,  int aRows
   ,  int aCols
   ,  T aRcond
   ,  int aMaxRank = -1
   ,  bool aRandomize = false
   ,  bool aSaveAc = true
   ,  bool aAbsConv = false
   ,  In aIoLevel = I_1
   )
{
   using p_type = typename ID_struct<T>::p_type;
   using t_type = typename ID_struct<T>::t_type;
   using ac_type = typename ID_struct<T>::ac_type;

   // Norm of matrix
   const auto mat_norm = std::sqrt(aMatrixNorm2);

   // Min dim
   const auto min_dim = std::min(aRows, aCols);

   // Save a copy of the matrix for generating the A_c matrix at the end
   std::unique_ptr<T[]> mat_copy = nullptr;
   auto size = aRows*aCols;
   if (  aSaveAc
      )
   {
      mat_copy = std::make_unique<T[]>(size);
      for(size_t i=0; i<size; ++i)
      {
         mat_copy[i] = aMatrix[i];
      }
   }

   // If aRandomize, we multiply aMatrix with random square matrix
   if (  aRandomize
      )
   {
      MIDASERROR("Randomization not implemented!");

      // Generate random normal-distributed matrix, G
      // ...

      // Multiply aMatrix with G: A <- G*A
      // ...
   }

   // Perform pivoted QR decomposition using GEQP3
   // NB: After this, aMatrix holds the upper triangle holds the R matrix of the QR decomp.
   // The elements below (together with the tau array) constitutes the Q matrix in terms of reflections.
   auto perm = std::make_unique<p_type[]>(aCols);
   int qr_info = 0;
   {
      LOGCALL("qr");
      int qr_lwork = detail::GetQrLwork<T>(aRows, aCols);
      int m = aRows;
      int n = aCols;
      int lda = std::max(1,m);
      auto jpvt = perm.get();
      auto tau = std::make_unique<T[]>(std::min(m,n));
      auto work = std::make_unique<T[]>(std::max(1,qr_lwork));

      T* mat_ptr = aMatrix;

      lapack_interface::geqp3 (  &m, &n
                              ,  mat_ptr, &lda, jpvt
                              ,  tau.get(), work.get(), &qr_lwork
                              ,  &qr_info
                              );

      if (  qr_info != 0
         )
      {
         MIDASERROR("InterpolativeDecomposition: QR info = " + std::to_string(qr_info));
      }

      // Shift perm in order to start indexing at 0
      for(size_t i=0; i<aCols; ++i)
      {
         --perm[i];
      }
   }


   // Truncate the QR decomposition
   std::unique_ptr<t_type[]> t_mat = nullptr;
   int ls_info = 0;
   int rank = libmda::numeric::float_numeq_zero(aMatrix[0], static_cast<T>(1.), 2) ? 0 : 1;
   const int& maxrank = min_dim;
   int rank_bound = aMaxRank < 0 ? maxrank : aMaxRank;
   T err_bound = static_cast<T>(-1.);
   T smin = aMatrix[0], smax = aMatrix[0];
   T srp1 = static_cast<T>(0.);
   if (  rank > 0
      )
   {
      // Truncate using estimated condition number.
      {
         LOGCALL("truncate");
         int irp1 = 0;
         auto work1 = std::make_unique<T[]>(rank_bound);
         auto work2 = std::make_unique<T[]>(rank_bound);
         work1[0] = static_cast<T>(1.);
         work2[0] = static_cast<T>(1.);
         int imin = 2, imax = 1;
         T sminpr, smaxpr;
         T s1, s2, c1, c2;
         while (  rank < rank_bound
               )
         {
            // Estimate min and max singular values of upper left (rank+1) x (rank+1) block matrix.
            lapack_interface::laic1(&imin, &rank, work1.get(), &smin, &aMatrix[rank*aRows], &aMatrix[rank*(aRows+1)], &sminpr, &s1, &c1);
            lapack_interface::laic1(&imax, &rank, work2.get(), &smax, &aMatrix[rank*aRows], &aMatrix[rank*(aRows+1)], &smaxpr, &s2, &c2);

            // Calculate error bound using approximate singular value (s_{rank+1})
            srp1 = sminpr;
            err_bound = std::sqrt(rank*(aCols-rank) + 1) * sminpr;


            if (  aIoLevel > I_8
               )
            {
               Mout  << " |=== Rank " << rank << " ===|\n"
                     << "    sminpr:         " << sminpr << "\n"
                     << "    smaxpr:         " << smaxpr << "\n"
                     << "    smaxpr/sminpr:  " << smaxpr/sminpr << "\n"
                     << "    aRcond:         " << aRcond << "\n"
                     << std::flush;
            }

            // Check convergence of truncation
            bool conv   =  aAbsConv
                        ?  libmda::numeric::float_gt(aRcond, sminpr)
                        :  libmda::numeric::float_gt(smaxpr*aRcond, sminpr);

            // If not converged, update approx singular vectors, increment rank, and set approx singular values.
            if (  !conv
               && rank < rank_bound
               )
            {
               for(int irank=0; irank<rank; ++irank)
               {
                  work1[irank] *= s1;
                  work2[irank] *= s2;
               }
               work1[rank] = c1;
               work2[rank] = c2;
               ++rank;
               smin = sminpr;
               smax = smaxpr;
            }
            // Else, break the loop
            else
            {
               if (  aIoLevel > I_5
                  )
               {
                  Mout  << " BREAK ID TRUNCATION!" << "\n"
                        << "    Rank:           " << rank << "\n"
                        << "    Err bound:      " << err_bound << "\n\n"
                        << std::flush;
               }
               
               break;
            }
         }
      }

      // Solve linear equations to obtain T: R_{11}*T = R_{12}
      unsigned t_size = rank*(aCols-rank);
      t_mat = std::make_unique<t_type[]>(t_size);
      {
         LOGCALL("solve triangular");
         // Load in R12 block. Will contain T matrix after solving linear equations.
         {
            auto* r12_ptr = t_mat.get();
            for(size_t j=rank; j<aCols; ++j)
            {
               T* ptr = aMatrix + aRows*j;
               for(size_t i=0; i<rank; ++i)
               {
                  *(r12_ptr++) = *(ptr++);
               }
            }
         }

         // Solve triangular system with TRTRS
         char uplo = 'U';
         char trans = 'N';
         char diag = 'N';
         int n = rank;
         int nrhs = aCols-rank;
         int lda = aRows;
         int ldb = std::max(1,n);

         lapack_interface::trtrs (  &uplo, &trans, &diag
                                 ,  &n, &nrhs
                                 ,  aMatrix, &lda
                                 ,  t_mat.get(), &ldb
                                 ,  &ls_info
                                 );

         if (  ls_info != 0
            )
         {
            MIDASERROR("InterpolativeDecomposition: TPTRS info = " + std::to_string(ls_info));
         }
      }
   }
   
   // Make ID_struct and load in results
   ID_struct<T> id;
   id.mErrorBound = err_bound;
   id.mSmin = smin;
   id.mSmax = smax;
   id.mSRp1 = srp1;
   id.mQrInfo = qr_info;
   id.mLsInfo = ls_info;
   id.mNrows = aRows;
   id.mNcols = aCols;
   id.mRank = rank;
   id.mP = std::move(perm);
   id.mT = std::move(t_mat);

   if (  aSaveAc
      )
   {
      id.mAc = std::make_unique<ac_type[]>(rank*aRows);
      auto* ptr = id.mAc.get();
      for(size_t icol=0; icol<rank; ++icol)
      {
         // Copy perm[icol]'th column of mat_copy into ptr.
         auto* copy_ptr = mat_copy.get() + id.mP[icol]*aRows;

         for(size_t irow=0; irow<aRows; ++irow)
         {
            *(ptr++) = *(copy_ptr++);
         }
      }
   }

   // Get inverse permutation
   auto inv_perm = std::make_unique<p_type[]>(aCols);
   for(int i=0; i<aCols; ++i)
   {
      inv_perm[id.mP[i]] = i;
   }

   id.mPt = std::move(inv_perm);

   return id;
}

/**
 * Perform matrix ID (calculate norm2 first)
 *
 * @param aMatrix       Matrix stored in column-major pointer (Lapack style)
 * @param aRows         Number of rows
 * @param aCols         Number of columns
 * @param aRcond        Truncation threshold for the ID
 * @param aMaxRank      Max rank of decomposition (-1 means no max rank)
 * @param aRandomize    Use randomized ID algorithm
 * @param aSaveAc       Save the skeleton matrix A_c.
 * @param aAbsConv      Truncate based on absolute value of largest neglected singular value
 * @param aIoLevel      IO level
 *
 * @return
 *    ID_struct containing all necessary information
 **/
template
   <  class T
   >
ID_struct<T> InterpolativeDecomposition
   (  T* aMatrix
   ,  int aRows
   ,  int aCols
   ,  T aRcond
   ,  int aMaxRank = -1
   ,  bool aRandomize = false
   ,  bool aSaveAc = true
   ,  bool aAbsConv = false
   ,  In aIoLevel = I_1
   )
{
   T norm2 = static_cast<T>(0.);
   auto size = aRows*aCols;

   T* ptr = aMatrix;
   for(size_t i=0; i<size; ++i)
   {
      norm2 += (*ptr) * (*ptr);
      ++ptr;
   }

   return InterpolativeDecomposition   (  aMatrix
                                       ,  norm2
                                       ,  aRows
                                       ,  aCols
                                       ,  aRcond
                                       ,  aMaxRank
                                       ,  aRandomize
                                       ,  aSaveAc
                                       ,  aAbsConv
                                       ,  aIoLevel
                                       );
}

/**
 * Perform matrix ID (copy to column-major pointer and calculate norm2 first)
 *
 * @param arMatrix      MidasMatrix
 * @param aRcond        Truncation threshold for the ID
 * @param aMaxRank      Max rank of decomposition (-1 means no max rank)
 * @param aRandomize    Use randomized ID algorithm
 * @param aSaveAc       Save the skeleton matrix A_c.
 * @param aAbsConv      Truncate based on absolute value of largest neglected singular value
 * @param aIoLevel      IO level
 *
 * @return
 *    ID_struct containing all necessary information
 **/
template
   <  class T
   >
ID_struct<T> InterpolativeDecomposition
   (  const GeneralMidasMatrix<T>& arMatrix
   ,  T aRcond
   ,  int aMaxRank = -1
   ,  bool aRandomize = false
   ,  bool aSaveAc = true
   ,  bool aAbsConv = false
   ,  In aIoLevel = I_1
   )
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);

   return InterpolativeDecomposition   (  a.get()
                                       ,  arMatrix.Nrows()
                                       ,  arMatrix.Ncols()
                                       ,  aRcond
                                       ,  aMaxRank
                                       ,  aRandomize
                                       ,  aSaveAc
                                       ,  aAbsConv
                                       ,  aIoLevel
                                       );
}

} /* namespace midas */

#endif /* INTERPOLATIVEDECOMPOSITION_H_INCLUDED */
