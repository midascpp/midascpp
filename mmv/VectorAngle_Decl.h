/**
 *******************************************************************************
 * 
 * @file    VectorAngle_Decl.h
 * @date    18-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VECTORANGLE_DECL_H_INCLUDED
#define VECTORANGLE_DECL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"

// Forward declarations.


//! The vector angle `acos(|<u|v>|/(N(u)N(v)))`; impl. with high res. for near-parallel vectors.
template
   <  bool CONJ = false
   ,  template<typename...> class VEC_TMPL
   ,  typename T
   >
midas::type_traits::RealTypeT<T> VectorAngle
   (  const VEC_TMPL<T>& arU
   ,  const VEC_TMPL<T>& arV
   ,  Uin aBeg = 0
   ,  Uin aEnd = std::numeric_limits<Uin>::max()
   );

#endif/*VECTORANGLE_DECL_H_INCLUDED*/
