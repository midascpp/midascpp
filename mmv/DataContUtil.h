#ifndef DATACONT_UTIL_H_INCLUDED
#define DATACONT_UTIL_H_INCLUDED

/**
 * interface for iterative solver and ODE
 **/
template<class T>
inline typename GeneralDataCont<T>::real_t Norm
   ( const GeneralDataCont<T>& aGDC
   )
{
   return aGDC.Norm();
}

/**
 * interface to it_solver
 **/
template<class T>
inline typename GeneralDataCont<T>::real_t Norm2
   (  const GeneralDataCont<T>& aGDC
   )
{
   return aGDC.Norm2();
}

template<class T>
inline typename GeneralDataCont<T>::real_t DiffNorm2
   (  const GeneralDataCont<T>& aGDC
   ,  const GeneralDataCont<T>& aOther
   )
{
   return aGDC.DiffNorm2(aOther);
}

template<bool CONJ_FIRST = false, class T>
inline typename GeneralDataCont<T>::real_t DiffNorm2Scaled
   (  const GeneralDataCont<T>& arFirst
   ,  const GeneralDataCont<T>& arSecond
   ,  const T aFirstScale = T(1)
   ,  const T aSecondScale = T(1)
   ,  In aOff = 0
   ,  In aN = -1
   )
{
   return arFirst.template DiffNorm2Scaled<CONJ_FIRST>(arSecond, aFirstScale, aSecondScale, aOff, aN);
}

/**
 *
 **/
template<class T>
inline void Orthogonalize
   ( GeneralDataCont<T>& aGDC
   , const GeneralDataCont<T>& aGDC2
   )
{
   aGDC.Orthogonalize(aGDC2);
}

/**
 *
 **/
template<class T>
inline void Scale
   (  GeneralDataCont<T>& aGDC
   ,  T aScalar
   )
{
   aGDC.Scale(aScalar);
}

/**
 *
 **/
template<class T>
inline void Zero
   ( GeneralDataCont<T>& aGDC
   )
{
   aGDC.Zero();
}

/**
 *
 **/
template<class T>
inline void Normalize(GeneralDataCont<T>& aGDC)
{
   aGDC.Normalize();
}

/**
 *
 **/
template<class T>
inline void Axpy
   ( GeneralDataCont<T>& aGDC
   , const GeneralDataCont<T>& aGDCother
   , T scal
   )
{
   aGDC.Axpy(aGDCother, scal);
}

/**
 *
 **/
template<class T>
inline auto Size
   (  const GeneralDataCont<T>& aGdc
   )
{
   return aGdc.Size();
}

/***************************************************************************//**
 * Mean Norm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations
 * I - Nonstiff Problems, page 167-168
 *
 *     ||e||^2 = (1/n) * sum_i (e[i] / sc[i])^2
 *
 * with `sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol`
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  typename ABSVAL_T
   >
ABSVAL_T OdeNorm2
   (  const GeneralDataCont<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralDataCont<PARAM_T>& aYOld
   ,  const GeneralDataCont<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   if (  aAbsTol <= C_0
      )
   {
      std::stringstream err;
      err
         << "Can only handle positive aAbsTol, but aAbsTol = " << aAbsTol
         << "."
         ;
      MIDASERROR(err.str());
   }

   const auto& size = aDeltaY.Size();
   if (  size != aYOld.Size()
      || size != aYNew.Size()
      )
   {
      std::stringstream err;
      err
         << "Size mismatch; aDeltaY.Size() = " << size
         << ", aYOld.Size() = " << aYOld.Size()
         << ", aYNew.Size() = " << aYNew.Size()
         << "."
         ;
      MIDASERROR(err.str());
   }

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   ABSVAL_T odenorm2 = 0;
   auto add_to_odenorm2 =
      [&odenorm2, abs = aAbsTol, rel = aRelTol]
      (const PARAM_T& delta_y, const PARAM_T& y_old, const PARAM_T& y_new)
      -> void
      {
         odenorm2 += AbsVal2(delta_y/(abs + std::max(AbsVal(y_old), AbsVal(y_new))*rel));
      };
   aDeltaY.LoopOverElements(add_to_odenorm2, aYOld, aYNew);

   return odenorm2;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  typename ABSVAL_T
   >
ABSVAL_T OdeMeanNorm2
   (  const GeneralDataCont<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralDataCont<PARAM_T>& aYOld
   ,  const GeneralDataCont<PARAM_T>& aYNew
   )
{
   return OdeNorm2(aDeltaY, aAbsTol, aRelTol, aYOld, aYNew)/aDeltaY.Size();
}

/***************************************************************************//**
 * MaxNorm2 for ODE. y has size n.
 *
 * From: Hairer, Nørslett, and Wanner: Solving Ordinary Differential Equations
 * I - Nonstiff Problems, page 167-168
 *
 * ||e||^2 = max_i (e[i] / sc[i])^2
 *
 * with sc[i] = abstol + max(|y_old[i]|, |y_new[i]|)*reltol
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  typename ABSVAL_T
   >
ABSVAL_T OdeMaxNorm2
   (  const GeneralDataCont<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralDataCont<PARAM_T>& aYOld
   ,  const GeneralDataCont<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   if (  aAbsTol <= C_0
      )
   {
      std::stringstream err;
      err
         << "Can only handle positive aAbsTol, but aAbsTol = " << aAbsTol
         << "."
         ;
      MIDASERROR(err.str());
   }
   const auto& size = aDeltaY.Size();
   if (  size != aYOld.Size()
      || size != aYNew.Size()
      )
   {
      std::stringstream err;
      err
         << "Size mismatch; aDeltaY.Size() = " << size
         << ", aYOld.Size() = " << aYOld.Size()
         << ", aYNew.Size() = " << aYNew.Size()
         << "."
         ;
      MIDASERROR(err.str());
   }

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   ABSVAL_T max = 0;
   auto update_max_odenorm2 =
      [&max, abs = aAbsTol, rel = aRelTol]
      (const PARAM_T& delta_y, const PARAM_T& y_old, const PARAM_T& y_new)
      -> void
      {
         max = std::max(max, AbsVal2(delta_y/(abs + std::max(AbsVal(y_old), AbsVal(y_new))*rel)));
      };
   aDeltaY.LoopOverElements(update_max_odenorm2, aYOld, aYNew);

   return max;
}


/***************************************************************************//**
 * Copy data to pointer.
 *
 * @note
 *    If T is complex, real/imag. parts are stored right after each other.
 *    The pointer must point to a pre-allocated block of memory of the right
 *    size - this is neither handled nor checked by this function.
 *    (Right size: aGdc.Size() (times 2, if T is complex).)
 ******************************************************************************/
template
   <  typename T
   ,  typename U
   >
void DataToPointer
   (  const GeneralDataCont<T>& aGdc
   ,  U* const apPtr
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   static_assert(std::is_floating_point_v<U> && std::is_floating_point_v<real_t>, "This only works for floating-point types!");

   U* ptr = apPtr;
   auto copy_data = [&ptr](const T& elem) -> void
      {
         *ptr = std::real(elem);
         ++ptr;
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            *ptr = std::imag(elem);
            ++ptr;
         }
      };
   aGdc.LoopOverElements(copy_data);
}

/***************************************************************************//**
 * Copy data from pointer.
 *
 * @note
 *    If T is complex, real/imag. parts are assumed to have been stored right
 *    after each other.
 *    The pointer must point to a pre-allocated block of memory of the right
 *    size - this is neither handled nor checked by this function.
 *    (Right size: aGdc.Size() (times 2, if T is complex).)
 ******************************************************************************/
template
   <  typename T
   ,  typename U
   >
void DataFromPointer
   (  GeneralDataCont<T>& aGdc
   ,  const U* const apPtr
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   static_assert(std::is_floating_point_v<U> && std::is_floating_point_v<real_t>, "This only works for floating-point types!");

   const U* ptr = apPtr;
   auto copy_data = [&ptr](T& elem) -> void
      {
         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            elem = T(*(ptr+0), *(ptr+1));
            ptr += 2;
         }
         else
         {
            elem = *ptr;
            ++ptr;
         }
      };
   aGdc.LoopOverElements(copy_data);
}

#endif /* DATACONT_UTIL_H_INCLUDED */
