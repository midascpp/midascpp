/**
 *******************************************************************************
 * 
 * @file    VectorAngle.h
 * @date    18-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VECTORANGLE_H_INCLUDED
#define VECTORANGLE_H_INCLUDED

#include "mmv/VectorAngle_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "mmv/VectorAngle_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*VECTORANGLE_H_INCLUDED*/
