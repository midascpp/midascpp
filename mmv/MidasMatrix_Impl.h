/**
************************************************************************
* 
* @file                MidasMatrix_Impl.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing vector utilities for namespace MidasMatrix.
* 
* Last modified: Mon Jun 08, 2009  04:13PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASMATRIX_IMPL_H_INCLUDED
#define MIDASMATRIX_IMPL_H_INCLUDED

// Standard headers
#include<algorithm>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<complex>
#include<utility>

// Midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "util/MidasStream.h"
#include "mmv/Diag.h"
#include "util/Io.h"
#include "util/Math.h"
#include "util/AbsVal.h"
#include "lapack_interface/LapackInterface.h"

extern bool gDebug;
extern In gAnalysisIoLevel;

/**
 * Constructor - assign all to a number.
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const In aNrows, const In aNcols, const T aX):
   mNrows(aNrows), mNcols(aNcols), mElements(nullptr)
{
   Allocate();
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) mElements[i][j] = aX;
}

/**
 * Constructor - allocate only
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const In aNrows, const In aNcols):
   mNrows(aNrows), mNcols(aNcols), mElements(nullptr)
{
   Allocate();
}

/**
 * Constructor - allocate only - assume square matrix. 
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const In aNrows):
   mNrows(aNrows), mNcols(aNrows), mElements(nullptr)
{
   Allocate();
}

/**
 * Constructor - assume square matrix - assign to number
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const In aNrows, const T aX):
   mNrows(aNrows), mNcols(aNrows), mElements(nullptr)
{
   Allocate();
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) mElements[i][j] = aX;
}

/**
 * Constructor - assume square matrix - assign diagonal to vector
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const In aNrows, const GeneralMidasVector<T>& arVec):
   mNrows(arVec.Size()), mNcols(aNrows), mElements(nullptr)
{
   if (mNrows != aNrows) MIDASERROR("Error in const. of matrix from vector");
   
   Allocate();

   for (In i=I_0;i<mNrows;i++) 
   {
      for (In j=I_0;j<mNcols;j++) mElements[i][j] = T(0);
      mElements[i][i] = arVec[i];
   }
}

/**
 * Copy constructor
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(const GeneralMidasMatrix<T>& arM):
   mNrows(arM.Nrows()), mNcols(arM.Ncols()), mElements(nullptr)
{
   Allocate();
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) mElements[i][j] = arM[i][j];
}

/**
 * Move constructor.
 *
 * @param[in,out] arM
 *    The matrix from which to move. It is left in a valid, _unspecified_
 *    state. (Actually, an empty matrix.)
 **/
template<class T>
GeneralMidasMatrix<T>::GeneralMidasMatrix(GeneralMidasMatrix<T>&& arM)
   :  mNrows(I_0)
   ,  mNcols(I_0)
   ,  mElements(nullptr)
{
   std::swap(mNrows, arM.mNrows);
   std::swap(mNcols, arM.mNcols);
   std::swap(mElements, arM.mElements);
}

/**
 * ctor from libmda expression
 **/
template<class T>
template<class A
       , libmda::Require_order<A,2>
       >
GeneralMidasMatrix<T>::GeneralMidasMatrix(const libmda::expr::expression_base<A>& aOther)
   : mNrows(aOther.template extent<0>())
   , mNcols(aOther.template extent<1>())
   , mElements(nullptr)
{
   Allocate();
   for(In i=I_0;i<mNrows;++i) 
      for(In j=I_0;j<mNcols;++j) 
         mElements[i][j] = aOther.at(i,j);
}

/**
 * Deconstructor 
 **/
template<class T>
GeneralMidasMatrix<T>::~GeneralMidasMatrix() 
{ 
   Deallocate();   
}

/**
 * Allocator function
 **/
template<class T>
void GeneralMidasMatrix<T>::Allocate()
{ 
   mElements = new T* [mNrows]; 
   for (In i=I_0;i<mNrows;i++) 
      mElements[i] = new T [mNcols]; 
}

/**
 * De-allocator function
 **/
template<class T>
void GeneralMidasMatrix<T>::Deallocate()
{ 
   if(mElements) 
   {
      for (In i=I_0;i<mNrows;i++) 
         delete[] mElements[int(i)]; 
      delete[] mElements;
      mElements = nullptr;
   }
}

/**
 * Clear the MidasMatrix leaving a 0x0 matrix
 **/
template<class T>
void GeneralMidasMatrix<T>::Clear()
{
   this->Deallocate();
   this->mNrows = I_0;
   this->mNcols = I_0;
}

/**
 * Copy assignment.
 *
 * @note
 *    Fails with a MIDASERROR if number of rows/columns doesn't match. This is
 *    so in order to avoid a lot of expensive (de)allocation, if used
 *    thoughtlessly.
 * @param arMat
 *    The matrix from which to copy.
 * @return
 *    A reference to this object.
 **/
template<class T>
GeneralMidasMatrix<T>& GeneralMidasMatrix<T>::operator=(const GeneralMidasMatrix<T>& arMat)
{
   if (this != &arMat)
   {
      if (mNrows  != arMat.Nrows() ) MIDASERROR( "MidasMatrix mismatch in MidasMatrix operator =");
      if (mNcols  != arMat.Ncols() ) MIDASERROR( "MidasMatrix mismatch in MidasMatrix operator =");
      for(In i=I_0;i<mNrows;i++) 
      {
         for(In j=I_0;j<mNcols;j++)
         {
            mElements[i][j] = arMat[i][j];
         }
      }
   }
   return *this;
}

/**
 * Move assignment.
 *
 * @param[in,out] aMat
 *    The matrix from which to move. It is left in a valid, _unspecified_
 *    state. (Actually containing the previous contents of this object.)
 * @return
 *    A reference to this object.
 **/
template<class T>
GeneralMidasMatrix<T>& GeneralMidasMatrix<T>::operator=(GeneralMidasMatrix<T>&& aMat)
{
   std::swap(this->mNrows, aMat.mNrows);
   std::swap(this->mNcols, aMat.mNcols);
   std::swap(this->mElements, aMat.mElements);
   std::swap(this->mLabel, aMat.mLabel);
   return *this;
}

/**
 *
 **/
template<class T>
typename GeneralMidasMatrix<T>::value_type& GeneralMidasMatrix<T>::at(const size_type aI, const size_type aJ) 
{  
   index_check_policy::apply(aI,mNrows);
   index_check_policy::apply(aJ,mNcols);
   return mElements[aI][aJ];
}

/**
 *
 **/
template<class T>
const typename GeneralMidasMatrix<T>::value_type& GeneralMidasMatrix<T>::at(const size_type aI, const size_type aJ) const 
{
   index_check_policy::apply(aI,mNrows);
   index_check_policy::apply(aJ,mNcols);
   return mElements[aI][aJ];
}

/**
 *
 **/
template<class T>
typename GeneralMidasMatrix<T>::size_type GeneralMidasMatrix<T>::size() const 
{ 
   return mNrows*mNcols; 
}

/**
 *
 **/
template<class T>
template<int N
       , libmda::iEnable_if<N==0>
       >
typename GeneralMidasMatrix<T>::size_type GeneralMidasMatrix<T>::extent() const
{ 
   return mNrows; 
}

/**
 *
 **/
template<class T>
template<int N
       , libmda::iEnable_if<N==1>
       >
typename GeneralMidasMatrix<T>::size_type GeneralMidasMatrix<T>::extent() const
{ 
   return mNcols; 
}

/**
* Redefine size of square matrix 
* */
template<class T>
void GeneralMidasMatrix<T>::SetNewSize(const In& aNewRows,const bool aSaveOldData,const bool aSetNewZero)
{
   SetNewSize(aNewRows,aNewRows,aSaveOldData,aSetNewZero);
}

/**
* Redefine size of non square matrix 
* */
template<class T>
void GeneralMidasMatrix<T>::SetNewSize
   (  const In& aNewRows
   ,  const In& aNewCols
   ,  const bool aSaveOldData
   ,  const bool aSetNewZero
   )
{
   // If we do not change the size, do nothing (except perhaps zeroing the data)
   if (  aNewRows == this->Nrows()
      && aNewCols == this->Ncols()
      )
   {
      if (  aSetNewZero
         )
      {
         this->Zero();
      }

      return;
   }
   else
   {
      if (!aSaveOldData)
      {
         this->Deallocate();
         mNrows = aNewRows;    
         mNcols = aNewCols;    
         this->Allocate();

         if (aSetNewZero)
         {
            this->Zero();
         }
      }
      else
      {
         GeneralMidasMatrix<T> old(*this);
         
         this->Deallocate();
         mNrows = aNewRows;    
         mNcols = aNewCols;    
         this->Allocate();
         
         if (aSetNewZero) 
         {
            this->Zero();
         }
         
         // copy elements
         auto nrows = min(Nrows(),old.Nrows());
         auto ncols = min(Ncols(),old.Ncols());
         for (In i=I_0;i<nrows;i++) 
            for (In j=I_0;j<ncols;j++) 
               mElements[i][j] = old[i][j];
      }
   }
}

/**
 * Set T** to nullptr (use with caution!)
 **/
template<class T>
void GeneralMidasMatrix<T>::SetPointerToNull()
{
   mElements = nullptr;
   
   mNrows = I_0;
   mNcols = I_0;
}

/**
 * Set data to pointer (use with caution!) 
 ***/
template<class T>
void GeneralMidasMatrix<T>::SetData(T** aElements, In aNrows, In aNcols)
{
   // first destroy the pointer to avoid mem leaks
   if(mElements != nullptr)
   {
      for (In i=I_0;i<mNrows;i++)
         delete[] mElements[int(i)];
      delete[] mElements;
   }
   mElements = aElements; // then assign new pointer
   mNrows=aNrows;  // set number of rows
   mNcols=aNcols;  // and cols
}

/**
* Steal data from another matrix (this is basically a move of the pointer!)
**/
template<class T>
void GeneralMidasMatrix<T>::StealData(GeneralMidasMatrix<T>& aVictimMat)
{
   // first delete old pointer to avoid memory leaks
   if(mElements != nullptr)
   {
      for (In i=I_0;i<mNrows;i++)
         delete[] mElements[int(i)];
      delete[] mElements;
   }
   
   // then steal pointer from victim matrix
   mElements = aVictimMat.mElements;
   mNrows = aVictimMat.mNrows;
   mNcols = aVictimMat.mNcols;
   
   // then nullify victims pointer (the offending matrix now owns the pointer)
   aVictimMat.SetPointerToNull();
}


/**
* Set matrix to unit matrix.
* Non square matrices will be unit in the square part and zero in the rest.
* */
template<class T>
void GeneralMidasMatrix<T>::Unit() 
{ 
   MakeDiagonal();
   SetDiagToNb(T(C_1));
}

/**
* Set Diagonal of matrix to a Nb.
* */
template<class T>
void GeneralMidasMatrix<T>::SetDiagToNb(T aX) 
{ 
   In n = min(mNrows,mNcols);
   for (In i=I_0;i<n;i++) 
      mElements[i][i] = aX;
}

/**
* Make matrix diagonal, 
* Non square matrices will be diagonal in the square part and zero in the rest.
* */
template<class T>
void GeneralMidasMatrix<T>::MakeDiagonal() 
{ 
   for (In i=I_0;i<mNrows;++i) 
   {
      auto lim = std::min(i, mNcols);
      for (In j=I_0;j<lim;++j) mElements[i][j] = T(C_0);
      for (In j=i+1;j<mNcols;++j) mElements[i][j] = T(C_0);
   }
}

/**
 * Checks that number of rows and columns are equal.
 *
 * @return
 *    Whether the matrix is square.
 **/
template<class T>
bool GeneralMidasMatrix<T>::IsSquare() const
{
   return (Ncols() == Nrows());
}

/**
 * Symmetrize matrix, i.e. \f$ M^{sym} = \tfrac{1}{2}(M + M^T) \f$. First
 * assert that it's square.
 **/
template<class T>
void GeneralMidasMatrix<T>::Symmetrize() 
{ 
   MidasAssert(IsSquare(), "Trying to symmetrize non-square matrix.");
   
   for (In i=I_0;i<mNrows;i++) 
   {
      for (In j=I_0;j<i;j++) 
      {
         mElements[i][j] = C_I_2*(mElements[i][j]+mElements[j][i]);
         mElements[j][i] = mElements[i][j];
      }
   }
}

/**
 * @param[in] aThr
 *    Absolute threshold to compare against.
 * @param[in] arFunc
 *    Callable with signature like `T arFunc(T elem_ij, T elem_ji)`, i.e. for
 *    each pair of elements (i,j) and (j,i) it determines how to compare them.
 * @return
 *    False if not square, or if any `|arFunc(ij, ji)| > aThr`. Else true.
 **/
template<class T>
template<class F>
bool GeneralMidasMatrix<T>::IsSymmetricImpl
   (  real_t aThr
   ,  const F& arFunc
   )  const
{
   if (!IsSquare())
   {
      return false;
   }

   for(Uin i = 0; i < Nrows(); ++i) 
   {
      for(Uin j = 0; j <= i; ++j) 
      {
         if (midas::util::AbsVal(arFunc(mElements[i][j], mElements[j][i])) > aThr) 
         {
            return false;
         }
      }
   }
   return true;
}

/**
 * Checks if matrix is symmetric to given threshold, i.e. if
 * \f$ |m_{ij} - m_{ji}| \leq \epsilon \f$ for all \f$ i,j \f$.
 *
 * @param arThr
 *    The threshold (\f$ \epsilon \f$ in this function description).
 **/
template<class T>
bool GeneralMidasMatrix<T>::IsSymmetric(real_t arThr) const
{ 
   return IsSymmetricImpl
      (  arThr
      ,  [](const T& ij, const T& ji) -> T {return ij - ji;}
      );
}

/**
* Is matrix antisymmetric?
* */
template<class T>
bool GeneralMidasMatrix<T>::IsAntiSymmetric(real_t arThr) const
{
   return IsSymmetricImpl
      (  arThr
      ,  [](const T& ij, const T& ji) -> T {return ij + ji;}
      );
}

/**
 * Checks if matrix is Hermitian to given threshold, i.e. if
 * \f$ |m_{ij}^* - m_{ji}| \leq \epsilon \f$ for all \f$ i,j \f$.
 *
 * @param arThr
 *    The threshold (\f$ \epsilon \f$ in this function description).
 **/
template<class T>
bool GeneralMidasMatrix<T>::IsHermitian(real_t arThr) const
{ 
   return IsSymmetricImpl
      (  arThr
      ,  [](const T& ij, const T& ji) -> T {return midas::math::Conj(ij) - ji;}
      );
}

/**
* Is matrix anti-Hermitian?
* */
template<class T>
bool GeneralMidasMatrix<T>::IsAntiHermitian(real_t arThr) const
{
   return IsSymmetricImpl
      (  arThr
      ,  [](const T& ij, const T& ji) -> T {return midas::math::Conj(ij) + ji;}
      );
}

/**
 * Loop through upper half triangle (excluding diagonal, since it's irrelevant
 * for symmetry) to find largest absolute difference 
 * \f$ |m_{i,j} - m_{j,i}| \f$.
 *
 * @note
 *    If matrix is non-square, only checks the square \f$ k \times k \f$ part
 *    contained in the matrix, where \f$ k = \min(n_{rows}, n_{cols}) \f$.
 *
 * @return
 *    The largest absolute difference between two elements that should be
 *    equal.
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::DevFromSymmetric() const
{
   real_t max_abs_diff = static_cast<real_t>(0);
   Uin min_cols_rows = std::min(Nrows(), Ncols());
   for(Uin i = I_0; i < min_cols_rows; ++i)
   {
      for(Uin j = i + I_1; j < min_cols_rows; ++j)
      {
         real_t abs_diff = midas::util::AbsVal(mElements[i][j] - mElements[j][i]);
         max_abs_diff = std::max(abs_diff, max_abs_diff);
      }
   }
   return max_abs_diff;
}

/**
 * Loop through upper half triangle (including diagonal, since it must be zero
 * for antisymmetry) to find largest absolute sum (which should be zero)
 * \f$ |m_{i,j} + m_{j,i}| \f$.
 *
 * @note
 *    If matrix is non-square, only checks the square \f$ k \times k \f$ part
 *    contained in the matrix, where \f$ k = \min(n_{rows}, n_{cols}) \f$.
 *
 * @return
 *    The largest absolute sum (deviation from zero) between two elements that
 *    should be numerically equal with opposite signs.
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::DevFromAntiSymmetric() const
{
   real_t max_abs_diff = static_cast<T>(0);
   Uin min_cols_rows = std::min(Nrows(), Ncols());
   for(Uin i = I_0; i < min_cols_rows; ++i)
   {
      for(Uin j = i; j < min_cols_rows; ++j)
      {
         real_t abs_diff = midas::util::AbsVal(mElements[i][j] + mElements[j][i]);
         max_abs_diff = std::max(abs_diff, max_abs_diff);
      }
   }
   return max_abs_diff;
}

/**
 * Zero matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::Zero()
{
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) mElements[i][j] = T(C_0);
}

/**
 * Zero col of matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::ZeroCol(In aCol)
{
   for (In i=I_0;i<mNrows;i++) mElements[i][aCol] = T(C_0);
}
/**
 * Zero row of matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::ZeroRow(In aRow)
{
   for (In j=I_0;j<mNcols;j++) mElements[aRow][j] = T(C_0);
}
/**
 * Zero matrix except col 
 **/
template<class T>
void GeneralMidasMatrix<T>::ZeroExceptCol(In aCol)
{
   for (In j=I_0;j<mNcols;j++) 
      if (j!= aCol) ZeroCol(j); 
}
/**
 * Zero matrix except row 
 **/
template<class T>
void GeneralMidasMatrix<T>::ZeroExceptRow(In aRow)
{
   for (In i=I_0;i<mNrows;i++) 
      if (i!= aRow) ZeroRow(i); 
}
/**
 * Transpose matrix in same container. Requires square matrix at this stage.
 **/
template<class T>
void GeneralMidasMatrix<T>::Transpose() 
{ 
   if(!IsSquare())
   {
      MIDASERROR(" Error in square matrix transpose ");
   }
   T tmp;
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<i;j++) 
      {
         tmp = mElements[i][j];
         mElements[i][j] = mElements[j][i];
         mElements[j][i] = tmp;
      }
}

/**
 * Transpose matrix in another container. Does not require square matrix.
 * But does in fact store an intermediate matrix!!!! Inefficient.
 **/
template<class T>
GeneralMidasMatrix<T> Transpose(const GeneralMidasMatrix<T>& aMat1)
{ 
   GeneralMidasMatrix<T> tmp(aMat1.Ncols(),aMat1.Nrows());
   for (In i=I_0;i<aMat1.Nrows();i++) 
      for (In j=I_0;j<aMat1.Ncols();j++) tmp[j][i] = aMat1[i][j];
   return tmp;
}

/**
 * Return number of rows. 
 **/
template<class T>
In GeneralMidasMatrix<T>::Nrows() const { return mNrows; }

/**
 * Return number of columns
 **/
template<class T>
In GeneralMidasMatrix<T>::Ncols() const { return mNcols; }

/**
 * Sum of elements (with sign)
 **/
template<class T>
T GeneralMidasMatrix<T>::SumEle() const
{
   T sum = T(C_0);
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) sum += mElements[i][j];
   return sum; 
}

/**
 * Add absolute values of all elements 
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::SumAbsEle() const
{
   real_t sum = real_t(C_0);
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) sum += std::abs(mElements[i][j]);
   return sum; 
}

/**
 * Add values squared to get two-norm.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::Norm2() const
{
   real_t norm2(C_0);
   for(In i=I_0; i<mNrows; ++i) 
   {
      for(In j=I_0; j<mNcols; ++j) 
      {
         norm2 += midas::util::AbsVal2(mElements[i][j]);
      }
   }

   return norm2;
}

/***************************************************************************//**
 * @param[in] aRowBeg
 *    The first row index of the block.
 * @param[in] aRowEnd
 *    The first-beyond-last row index of the block is aRowEnd or Nrows(),
 *    whichever is smaller.
 * @param[in] aColBeg
 *    The first column index of the block.
 * @param[in] aColEnd
 *    The first-beyond-last column index of the block is aColEnd or Ncols(),
 *    whichever is smaller.
 * @return
 *    The sum of `|elem|^2` for all `elem` in the specified block.
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::Norm2Block
   (  Uin aRowBeg
   ,  Uin aRowEnd
   ,  Uin aColBeg
   ,  Uin aColEnd
   )  const
{
   aRowEnd = (Nrows() < aRowEnd)? Nrows(): aRowEnd;
   aColEnd = (Ncols() < aColEnd)? Ncols(): aColEnd;
   real_t norm2 = 0;
   for(Uin i = aRowBeg; i < aRowEnd; ++i)
   {
      for(Uin j = aColBeg; j < aColEnd; ++j)
      {
         norm2 += midas::util::AbsVal2(mElements[i][j]);
      }
   }
   return norm2;
}

/**
 * Sum of out of diagonal elements 
 **/
template<class T>
T GeneralMidasMatrix<T>::SumOutOfDiagonal() const
{
   T sum = T(C_0);
   for (In i=I_0;i<mNrows;i++) 
   {
      for (In j=I_0;j<i;j++) sum  += mElements[i][j];
      for (In j=i+I_1;j<mNcols;j++) sum  += mElements[i][j];
   }
   return sum ;
}

/**
 * Sum of absolute values of out of diagonal elements 
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::SumAbsOutOfDiagonal() const
{
   real_t sum = real_t(C_0);
   for (In i=I_0;i<mNrows;i++) 
   {
      for (In j=I_0;j<i;j++) sum  += std::abs(mElements[i][j]);
      for (In j=i+I_1;j<mNcols;j++) sum  += std::abs(mElements[i][j]);
   }
   return sum ;
}

/**
 * Sum of absolute values of out of diagonal elements, upper part.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::SumAbsOutOfDiagonalUpper() const
{
   real_t sum = real_t(C_0);
   for (In i=I_0;i<mNrows;i++) 
      for (In j=i+I_1;j<mNcols;j++) sum  += std::abs(mElements[i][j]);
   return sum ;
}

/**
 * Sum of out of diagonal elements squared.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::Norm2OutOfDiagonal() const
{
   real_t norm(C_0);
   for (In i=I_0;i<mNrows;i++) 
   {
      for (In j=I_0;j<i;j++) norm += midas::util::AbsVal2(mElements[i][j]);
      for (In j=i+I_1;j<mNcols;j++) norm += midas::util::AbsVal2(mElements[i][j]);
   }
   return norm;
}

/**
 * Square root of sum of out-of-diagonal elements.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::NormOutOfDiagonal() const
{
   return std::sqrt(Norm2OutOfDiagonal());
}
/**
 * Square root of sum of diagonal elements.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::NormDiagonal() const
{
   if (mNcols != mNrows) MIDASERROR(" Not square matrix in requested NormDiagonal opration");
   return sqrt(Norm2Diagonal());
}

/**
 * Sum of diagonal elements squared.
 **/   
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::Norm2Diagonal() const
{
   if (mNcols != mNrows) MIDASERROR(" Not square matrix in requested Norm2Diagonal opration");
   real_t norm(C_0);
   for (In i=I_0;i<mNrows;i++) norm += midas::util::AbsVal2(mElements[i][i]);
   return norm;
}

/**
 * Find usual norm = sqrt of twonorm
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::Norm() const
{
   return sqrt(Norm2());
}
/**
 * Find largest element
 **/
template<class T>
T GeneralMidasMatrix<T>::LargestElement() const
{
   if (midas::type_traits::IsComplexV<T>) MIDASERROR(" Not implemented for complex matrices!");
   if (mNcols == 0) MIDASERROR(" Cannot find largest element of 0-by-0 matrix!");
   if (mNrows == 0) MIDASERROR(" Cannot find largest element of 0-by-0 matrix!");
   T largest=mElements[0][0];
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) 
         if (mElements[i][j] > largest) largest = mElements[i][j];
   return largest;
}

/**
 * Find largest element - absolute size
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::LargestAbsElement() const
{
   if (mNcols == 0) MIDASERROR(" Zero matrix in Largest Abs Element ");
   if (mNrows == 0) MIDASERROR(" Zero matrix in Largest Abs Element ");
   real_t largest=std::abs(mElements[0][0]);
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<mNcols;j++) 
         if (std::abs(mElements[i][j]) > largest) largest = std::abs(mElements[i][j]);
   return largest;
}

/**
 * Find trace of matrix.
 **/
template<class T>
T GeneralMidasMatrix<T>::Trace() const
{
   if (mNcols != mNrows) MIDASERROR(" Not square matrix in requested trace opration");
   T trace = T(C_0);
   for (In i=I_0;i<mNrows;i++) trace += mElements[i][i];
   return trace;
}
/**
 * Trace of a matrix product
 **/
template<class T>
T GeneralMidasMatrix<T>::TraceProduct(const GeneralMidasMatrix<T>& aM) const {
   T result=(C_0);
   if(mNrows != aM.Ncols()) MIDASERROR("Not square matrix product in TraceProduct.");
   for (In i=I_0;i<mNrows;i++) 
      for (In j=I_0;j<aM.Ncols();j++) 
         result += mElements[i][j]*aM[j][i];
   return result;
}
/**
 * Return a col of the matrix as a vector.
 **/
template<class T>
void GeneralMidasMatrix<T>::GetCol(GeneralMidasVector<T>& arV, In aIcol,In aIoff,In aIstride) const
{
   if (aIoff+(mNrows-I_1)*aIstride > arV.Size())
         MIDASERROR( " MidasMatrix::GetCol, index out of range");
   if (aIcol >= mNcols) MIDASERROR( " MidasMatrix::GetCol     aIcol > mNcols ");
   for (In i=I_0;i<mNrows;i++) arV[aIoff+i*aIstride] = mElements[i][aIcol];  
}

/**
 * Return an offset col of the matrix as a vector. Ie col size must equal mNrow-aIColOff
 **/
template<class T>
void GeneralMidasMatrix<T>::GetOffsetCol(GeneralMidasVector<T>& arV, In aIcol, In aIColOff) const
{
   if (  mNrows-aIColOff != arV.Size()
      )
   {
      Mout  << " Error in GeneralMidasMatrix::GetOffsetCol:\n"
            << "    mNrows          : " << mNrows << "\n"
            << "    mNcols          : " << mNcols << "\n"
            << "    Get column      : " << aIcol << "\n"
            << "    Column offset   : " << aIColOff << "\n"
            << "    arV.Size()      : " << arV.Size() << "\n"
            << std::flush;
      MIDASERROR("MidasMatrix::GetOffsetCol arV has wrong size!");
   }
   for (In i=aIColOff;i<mNrows;++i) arV[i-aIColOff] = mElements[i][aIcol];  
}

/** 
 * Return an offset row of the matrix as a vector, i.e. row size must equal mNcol-aIRowOff
 **/
template<class T>
void GeneralMidasMatrix<T>::GetOffsetRow(GeneralMidasVector<T>& arV, In aIrow, In aIRowOff) const
{
   if (  mNcols-aIRowOff != arV.Size()
      )
   {
      Mout  << " Error in GeneralMidasMatrix::GetOffsetRow:\n"
            << "    mNrows          : " << mNrows << "\n"
            << "    mNcols          : " << mNcols << "\n"
            << "    Get row         : " << aIrow << "\n"
            << "    Row offset      : " << aIRowOff << "\n"
            << "    arV.Size()      : " << arV.Size() << "\n"
            << std::flush;
      MIDASERROR("MidasMatrix::GetOffsetRow arV has wrong size!");
   }
   for (In i=aIRowOff;i<mNcols;++i) arV[i-aIRowOff] = mElements[aIrow][i];  
}

/**
 * Return subvec from aStart to aEnd of a col of the matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::GetSubCol(GeneralMidasVector<T>& arV, const In aIcol, const In aStart, const In aEnd)
{
   In range = aEnd-aStart+I_1;
   if (range != arV.Size()) MIDASERROR("MidasMatrix::GetSubCol Range and vector size does not fit! ");
   if (aEnd > mNrows) MIDASERROR("MidasMatrix::GetSubCol Out of range: aEnd larger than number of rows! ");
   for (In i=aStart; i<=aEnd; ++i) arV[i-aStart] = mElements[i][aIcol];  
}

/**
 * Assign a vector to the col of a matrix. Use with caution!
 **/
template<class T>
void GeneralMidasMatrix<T>::AssignCol(const GeneralMidasVector<T>& arV, In aIcol,In aIoff,In aIstride, In aIMatOffset)
{
   if (aIoff+(mNrows-I_1)*aIstride > arV.Size())
         MIDASERROR( " MidasMatrix::AssignCol, index out of range");
   if (aIcol > mNcols) MIDASERROR( " MidasMatrix::AssignCol, aIcol > mNcols ");
   for (In i=I_0;i<mNrows-aIMatOffset;i++) mElements[i+aIMatOffset][aIcol] = arV[aIoff+i*aIstride];
}

/**
 * Add a vector to the col of a matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::AddToCol(const GeneralMidasVector<T>& arV, In aIcol, In aVecOffset, In aVecStride, In aMatOffset, In aMatStride)
{
   // Since all the input options are integers this should perfom integer divison giving us the number of elements of interest.
   In num_elems_mat = (mNrows - aMatOffset) / aMatStride;
   In num_elems_vec = (arV.Size() - aVecOffset) / aVecStride;
   if (num_elems_mat != num_elems_vec)
   {
      std::stringstream ss;
      ss << "MidasMatrix::AddToCol. Targeting different number of elements in matrix and vector." << std::endl
         << "Number of targeted elements in Vec: " << num_elems_vec << std::endl
         << "Number of targeted elements in Mat: " << num_elems_mat;
      MIDASERROR(ss.str());
   }
   if (aIcol > mNcols)
   {
      std::stringstream ss;
      ss << "MidasMatrix::AddToCol." << std::endl
         << "Number of columns in matrix: " << mNcols << std::endl
         << "Trying to add a vector to column: " << aIcol << std::endl;
      MIDASERROR(ss.str());
   }
   for (In i = I_0; i < num_elems_mat; i++)
   {
      mElements[i*aMatStride + aMatOffset][aIcol] += arV[aVecOffset + i*aVecStride];
   }
}

/**
 * Get a row of the matrix as a vector.
 **/
template<class T>
void GeneralMidasMatrix<T>::GetRow(GeneralMidasVector<T>& arV, In aIrow,In aIoff,In aIstride) const
{
   if (aIoff+(mNcols-I_1)*aIstride > arV.Size())
         MIDASERROR( " MidasMatrix::GetRow, index out of range");
   if (aIrow > mNrows) MIDASERROR( " MidasMatrix::GetRow, aIrow > mNrows ");
   for (In i=I_0;i<mNcols;i++) arV[aIoff+i*aIstride] = mElements[aIrow][i];  
}

/**
 * Assign a vector to the row of a matrix.
 * Is the aIoff and aIstride ever tested?
 **/
template<class T>
void GeneralMidasMatrix<T>::AssignRow(const GeneralMidasVector<T>& arV, In aIrow,In aIoff,In aIstride)
{
   if (aIoff+(mNcols-I_1)*aIstride > arV.Size())
         MIDASERROR( " MidasMatrix::AssignRow, index out of range");
   if (aIrow > mNrows) MIDASERROR( " MidasMatrix::AssignRow, aIrow > mNrows ");
   for (In i=I_0;i<mNcols;i++) mElements[aIrow][i] = arV[aIoff+i*aIstride];
}

/**
 * Add a vector to the row of a matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::AddToRow(const GeneralMidasVector<T>& arV, In aIrow, In aVecOffset, In aVecStride, In aMatOffset, In aMatStride)
{
   // Since all the input options are integers this should perfom integer divison giving us the number of elements of interest.
   In num_elems_mat = (mNcols - aMatOffset) / aMatStride;
   In num_elems_vec = (arV.Size() - aVecOffset) / aVecStride;
   if (num_elems_mat != num_elems_vec)
   {
      std::stringstream ss;
      ss << "MidasMatrix::AddToRow. Targeting different number of elements in matrix and vector." << std::endl
         << "Number of targeted elements in Vec: " << num_elems_vec << std::endl
         << "Number of targeted elements in Mat: " << num_elems_mat;
      MIDASERROR(ss.str());
   }
   if (aIrow > mNrows)
   {
      std::stringstream ss;
      ss << "MidasMatrix::AddToRow." << std::endl
         << "Number of rows in matrix: " << mNrows << std::endl
         << "Trying to add a vector to row: " << aIrow << std::endl;
      MIDASERROR(ss.str());
   }
   for (In i = I_0; i < num_elems_mat; i++)
   {
      mElements[aIrow][i*aMatStride + aMatOffset] += arV[aVecOffset + i*aVecStride];
   }
}

/**
 * Assign a matrix to a submatrix of a bigger matrix. Use with caution!
 **/
template<class T>
void GeneralMidasMatrix<T>::AssignToSubMatrix(const GeneralMidasMatrix<T>& arM,In aIrowStart,In aNrows,In aIcolStart, In aNcols)
{
   if (aIcolStart+aNcols > arM.mNcols) MIDASERROR( " MidasMatrix::AssignToSubmatrix, col index out of range");
   if (aIrowStart+aNrows > arM.mNrows) MIDASERROR( " MidasMatrix::AssignToSubmatrix, row index out of range");
   SetNewSize(aNrows,aNcols,false); 
   for (In ir=0;ir<mNrows;ir++)
     for (In ic=0;ic<mNcols;ic++)
        mElements[ir][ic]=arM[aIrowStart+ir][aIcolStart+ic]; 
}

/**
 * Assign a submatrix of a big matrix to an entire (smaller) matrix. SubMat.AssignSubMatrixToMatrix(Matrix, ...)
 **/
template<class T>
void GeneralMidasMatrix<T>::AssignSubMatrixToMatrix(const GeneralMidasMatrix<T>& arM, In aRowStart, In aNrows, In aColStart, In aNcols)
{
   if (aRowStart + aNrows != mNrows) MIDASERROR("MidasMatrix::AssignSubMatrixTomatrix, row index out of range");
   if (aColStart + aNcols != mNcols) MIDASERROR("MidasMatrix::AssignSubMatrixTomatrix, col index out of range");
   if (aNrows > arM.mNrows) MIDASERROR("MidasMatrix::AssignSubMatrixTomatrix, matrix you copy from has fewer rows than aNrows provided");
   if (aNcols > arM.mNcols) MIDASERROR("MidasMatrix::AssignSubMatrixTomatrix, matrix you copy from has fewer cols than aNcols provided");
   for (In ir = 0; ir < arM.mNrows; ir++)
      for (In ic = 0; ic < arM.mNcols; ic++)
         mElements[ir + aRowStart][ic + aColStart] = arM[ir][ic];
}

/**
 * Add a (small) matrix to a submatrix of a (big) matrix. SubMat.AddMatrixToSubMatrix(Matrix, ...)
 **/
template<class T>
void GeneralMidasMatrix<T>::AddMatrixToSubMatrix(const GeneralMidasMatrix<T>& arM, In aRowStart, In aNrows, In aColStart, In aNcols)
{
   if (aRowStart + aNrows != mNrows) MIDASERROR("MidasMatrix::AddMatrixToSubMatrix, row index out of range");
   if (aColStart + aNcols != mNcols) MIDASERROR("MidasMatrix::AddMatrixToSubMatrix, col index out of range");
   if (aNrows > arM.mNrows) MIDASERROR("MidasMatrix::AddMatrixToSubMatrix, matrix you add from has fewer rows than aNrows provided");
   if (aNcols > arM.mNcols) MIDASERROR("MidasMatrix::AddMatrixToSubMatrix, matrix you add from has fewer cols than aNcols provided");
   for (In ir = 0; ir < arM.mNrows; ir++)
      for (In ic = 0; ic < arM.mNcols; ic++)
         mElements[ir + aRowStart][ic + aColStart] += arM[ir][ic];
}

/**
 * Convert MidasVector to MidasMatrix, if fx vector is needed for midas::lapack_interface::gemm
 **/
template<class T>
GeneralMidasMatrix<T> ConvertVecToMat(const GeneralMidasVector<T>& arVec, In aNrows, In aNcols)
{
   if (aNrows * aNcols != arVec.Size()) MIDASERROR("ConvertVecToMat, vector is not of the specified size");
   GeneralMidasMatrix<T> tmp(aNrows, aNcols);
   for (In i = I_0; i < aNrows; ++i) 
      for (In j = I_0; j < aNcols; ++j) tmp[i][j] = arVec[aNcols*i + j];
   return tmp;
}

/**
 * Swap two columns,
 **/
template<class T>
void GeneralMidasMatrix<T>::SwapCols(In aIcol1,In aIcol2)
{
   for (In ir=0;ir<mNrows;ir++)
   {
      T tmp = mElements[ir][aIcol1];
      mElements[ir][aIcol1] = mElements[ir][aIcol2];
      mElements[ir][aIcol2] = tmp;
   }
}

/**
 * MidasMatrix inner product, sum of elements multiplied. First argument is
 * conjugated (for complex T).
 **/
template<class T>
T Dot(const GeneralMidasMatrix<T>& aMat1, const GeneralMidasMatrix<T>& aMat2)
{ 
   if (aMat1.Nrows() != aMat2.Nrows() ) MIDASERROR( "MidasMatrix mismatch in MidasMatrix dot ");
   if (aMat1.Nrows() != aMat2.Nrows() ) MIDASERROR( "MidasMatrix mismatch in MidasMatrix dot ");
   T sum = T(C_0); 
   for (In i=I_0;i<aMat1.Nrows();i++) 
      for (In j=I_0;j<aMat1.Ncols();j++) sum += midas::math::Conj(aMat1[i][j])*aMat2[i][j];
   return sum;
}
/**
 * @brief Perform Hadamard product of matrices. Element-wise multiplication of first matrix with second.
 */
template<typename T>
void HadamardProduct
   (  GeneralMidasMatrix<T>& arLeft
   ,  const GeneralMidasMatrix<T>& aRight
   )
{
   const In nrows = arLeft.Nrows();
   const In ncols = arLeft.Ncols();
   if (  ncols != aRight.Ncols()
      || nrows != aRight.Nrows()
      )
   {
      MIDASERROR("MidasMatrix HadamardProduct: Dimensions of matrices mismatch!");
   }

   for(In irow=I_0; irow<nrows; ++irow)
   {
      for(In icol=I_0; icol<ncols; ++icol)
      {
         arLeft[irow][icol] *= aRight[irow][icol];
      }
   }
}

/**
 * MidasMatrix scale elements;
 **/
template<class T>
void GeneralMidasMatrix<T>::Scale(const T& aX)
{ 
   for (In i=I_0;i<Nrows();i++) 
      for (In j=I_0;j<Ncols();j++) mElements[i][j]*=aX;
}

/**
 * MidasMatrix scale a column elements;
 **/
template<class T>
void GeneralMidasMatrix<T>::ScaleCol(const T& aX, const In& aCol)
{ 
   for (In i=I_0;i<Nrows();i++) mElements[i][aCol]*=aX;
}

/**
 * MidasMatrix calculate norm of a column 
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::NormCol(const In& aCol) const
{ 
   real_t norm2(C_0);
   for(In i=I_0; i<Nrows(); ++i)
   {
      norm2 += midas::util::AbsVal2(mElements[i][aCol]);
   }
   return std::sqrt(norm2);
}

/**
 * MidasMatrix calculate metric norm of a column, i.e. C^TSC
 * Client is responsible for aMetric being a proper metric matrix (positive
 * definite, symmetric/hermitian).
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::MetricNormCol(const In& aCol, const GeneralMidasMatrix<T>& aMetric) const
{
   real_t sum(0);
   for (In i=I_0;i<Nrows();i++)
      for (In j=I_0;j<Nrows();j++) 
         sum += std::real(midas::math::Conj(mElements[i][aCol])*aMetric[i][j]*mElements[j][aCol]);
   sum=std::sqrt(sum);
   return sum;
}

/**
 * MidasMatrix scale a row elements;
 **/
template<class T>
void GeneralMidasMatrix<T>::ScaleRow(const T& aX, const In& aRow)
{ 
   for (In i=I_0;i<Ncols();i++) mElements[aRow][i]*=aX;
}
/**
 * MidasMatrix calculate norm of a row
 **/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::NormRow(const In& aRow) const
{ 
   real_t norm2(C_0);
   for(In i=I_0; i<Ncols(); ++i)
   {
      norm2 += midas::util::AbsVal2(mElements[aRow][i]);
   }
   return std::sqrt(norm2);
}

/**
 * MidasMatrix - normalize all columns to unity
 **/
template<class T>
void GeneralMidasMatrix<T>::NormalizeColumns()
{ 
   for (In i=I_0;i<Ncols();i++) ScaleCol(T(C_1)/NormCol(i),i);
}

/**
 * MidasMatrix - normalize all rows to unity
 **/
template<class T>
void GeneralMidasMatrix<T>::NormalizeRows()
{ 
   for (In i=I_0;i<Nrows();i++) ScaleRow(T(C_1)/NormRow(i),i);
}

/**
 * Orthonormalize all column vectors. 
 **/
template<class T>
void GeneralMidasMatrix<T>::OrthogLowdin()
{ 
   NormalizeColumns();
   GeneralMidasMatrix<T> overlap(Ncols()); 
   for (In i=I_0;i<Ncols();i++) 
   {
      for (In j=I_0;j<Ncols();j++) 
      {
         T sum=(0); 
         for (In k=I_0;k<Nrows();k++) sum += midas::util::AbsVal2(mElements[k][i]);
         overlap[i][j]=sum; 
      }
   }
   GeneralMidasMatrix<T> overlapsave(overlap); 
   SqrtInvert(overlap); 

   // Find new basis Y = X * S-1/2 
   GeneralMidasMatrix<T> scratch(*this); 
   *this = scratch*overlap; 

   Mout << " resulting matrix with orthonormal coulumns \n " << *this << std::endl; 
   scratch=*this;
   scratch.Conjugate(); 
   GeneralMidasMatrix<T> unit = scratch*(*this);
   Mout << " This is the new S matrix which should be the unit matrix  \n " << unit << std::endl; 
   MIDASERROR( " If you use the OrthogLowdin function you should check it further, OC"); 
}

/**
 * Copy the first elements
 **/
template<class T>
void GeneralMidasMatrix<T>::Copy(const GeneralMidasMatrix<T>& aM, In aRow, In aCol)
{
   MidasWarning("I am not completely safe! Implement more checks on sizes!");

   if(aRow == -I_1)
      aRow = aM.Nrows();

   if(aCol == -I_1)
      aCol = aM.Ncols();

   for(In i=I_0; i<aRow; i++)
   {
      for(In j=I_0; j<aCol; j++)
      {
         mElements[i][j] = aM[i][j];
      }
   }
}

/**
 * Reassign a matrix to the first aNewRow x aNewCol elements of another matrix.
 **/
template<class T>
void GeneralMidasMatrix<T>::Reassign(const GeneralMidasMatrix<T>& aM, In aNewRow, In aNewCol)
{
   if(aNewRow == -I_1)
      aNewRow = aM.Nrows();

   if(aNewCol == -I_1)
      aNewCol = aM.Ncols();
   
   SetNewSize(aNewRow, aNewCol, false);
   
   Copy(aM, aNewRow, aNewCol);
}

/**
 * << overload for ostream
 **/
template<class T>
ostream& operator<<(ostream& arOut, const GeneralMidasMatrix<T>& aMat1)
{ 
   int width = 23; // width to print a number in scientific with precision 16 plus a sign
   arOut << std::right;
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, arOut);
   //arOut.unsetf(ios::scientific);
   arOut.setf(ios::showpoint);
   //arOut.setf(ios::showpos);

   if (aMat1.Nrows()<=I_0) arOut << " # Rows =  " << aMat1.Nrows() << std::endl; 
   if (aMat1.Ncols()<=I_0) arOut << " # Cols =  " << aMat1.Ncols() << std::endl; 
   bool batch = true;

   if (!batch)
   {
      for (In i=I_0;i<aMat1.Nrows();i++)
      {
         for (In j=I_0;j<aMat1.Ncols();j++)
         {
            arOut << std::setw(width) << aMat1[i][j] << " ";
         }
         arOut << '\n';
      }
      arOut << std::endl;
   }
   else
   {
      In ncols = 0;
      In nprbat = 5;
      In nbat = (aMat1.Ncols()+nprbat-1)/nprbat;
      In nrest= aMat1.Ncols();
      In jadd = 0;
      for (In ibat=I_0;ibat<nbat;ibat++) 
      {
         ncols = min(nrest,nprbat);
         for (In i=I_0;i<aMat1.Nrows();i++)
         {
            for (In j=jadd;j<ncols+jadd;j++) 
            {
               arOut << std::setw(width) << aMat1[i][j] << " ";
            }
            arOut << '\n';
         }
         arOut << std::endl;
         nrest -= ncols;
         jadd += ncols;
      }
   }
   return arOut;
}

/**
 * ConjGrad method for solution of linear equations.
 **/
template<class T>
bool GeneralMidasMatrix<T>::ConjGrad(GeneralMidasVector<T>& arSol, const GeneralMidasVector<T>& arRhs, 
            real_t& aResid, In& aIter, const real_t aTol, const In aMaxIter ) 
{
   if (mNrows != mNcols) MIDASERROR("CongGrad: Matrix not square (symmetric)");
   if (arSol.Size() != mNcols) MIDASERROR("Size mismatch in ConjGrad between Sol. and matrix");
   if (arRhs.Size() != mNrows) MIDASERROR("Size mismatch in ConjGrad between Rhs. and matrix");

   // On input arSol is assumed to contain the initial guess.
   GeneralMidasVector<T> tmp_vec  = (*this)*arSol;
   GeneralMidasVector<T> residual  = arRhs - tmp_vec;
   GeneralMidasVector<T> trial     = residual;

   real_t res_norm2      = residual.Norm2();
   real_t res_norm       = sqrt(res_norm2);
   const real_t  stop_at = aTol*arRhs.Norm();  // Stop if |residual| < tol*|b|, there are other possibilities.
   Mout << "Here 4" << endl;

   if (res_norm == real_t(0)) // Initial guess happens to be exactly true!
   {
      aResid = real_t(0);
      aIter  = I_0;
      return true;
   }

   for (aIter=I_0; aIter < aMaxIter; aIter++)   // Loop over iterations.
   {
      GeneralMidasVector<T> mat_times_trial = (*this)*trial;
      T          trial_mat_trial = Dot(trial,mat_times_trial);
      T          alpha           = res_norm2/trial_mat_trial;

      arSol     += alpha*trial;
      residual  -= alpha*mat_times_trial;

      T beta    = T(C_1)/res_norm2;
      res_norm2  = residual.Norm2();
      res_norm   = sqrt(res_norm2); 
      if (res_norm <= stop_at) break;
      beta *= res_norm2;

      trial  = residual + beta*trial;
   }
   aResid = residual.Norm();
   Mout << " CG resid: " <<  aResid << " in " << aIter << " iters. ";
   bool test=true;
   if (test) 
   {
      // Be sure we have found a satisfactory solution.
      GeneralMidasVector<T> mat_times_sol = (*this)*arSol;  
      mat_times_sol -= arRhs;
      real_t norm = mat_times_sol.Norm();
      Mout << " ACTUAL resid: " <<  norm << endl;
      aResid = norm;
   }
   if (aIter == aMaxIter) 
   {
      return false;
   }
   else 
   {
      return true; // return true in all cases, so take care outside if numerical problems has occured.
   }

}

/**
 * Reorder a_ij to a matrix where arIdx is placed at zero and all subsequent
 **/
template<class T>
void GeneralMidasMatrix<T>::ShiftRowIndexToZero(const In& arIdx)
{
   if (arIdx==0) return;

   if (Ncols()>0)
   {
      GeneralMidasVector<T> row(Ncols());
      GetRow(row,arIdx,I_0,I_1);
      for (In irow=arIdx;irow>0;irow--)
         for (In j=0;j<Ncols();j++) mElements[irow][j] = mElements[irow-1][j];
      AssignRow(row,I_0,I_0,I_1);
   }
}

/**
 * Reorder a_ij to a matrix where arIdx is placed at zero and all subsequent
 **/
template<class T>
void GeneralMidasMatrix<T>::ShiftColIndexToZero(const In& arIdx)
{
   if (arIdx==0) return;
   if (Nrows()>0)
   {
      GeneralMidasVector<T> col(Nrows());
      GetCol(col,arIdx,I_0,I_1);
      for (In icol=arIdx;icol>0;icol--)
         for (In j=0;j<Ncols();j++) mElements[j][icol] = mElements[j][icol-1];
      AssignCol(col,I_0,I_0,I_1);
   }
}

/**
 * Reorder a_ij to a matrix where arIdx is placed at zero and all subsequent
 **/
template<class T>
void GeneralMidasMatrix<T>::ShiftIndexToZero(const In& arIdx)
{
   if (arIdx==0) return;
   ShiftRowIndexToZero(arIdx);
   ShiftColIndexToZero(arIdx);
}

/**
 * Organize a MidasVector in a matrix. Default build row by row
 **/
template<class T>
void GeneralMidasMatrix<T>::MatrixFromVector(const GeneralMidasVector<T>& arV, bool aRowbyRow)
{
   if (arV.Size() != Ncols()*Nrows())
   {
      Mout << " MidasMatrix::MatrixFromVector, size of vector and matrix does not fit" << std::endl;
      MIDASERROR(" MidasMatrix::MatrixFromVector, size of vector and matrix does not fit");
   }
   In i_v = 0;
   if (!aRowbyRow) //organize from columns
   {
      for (In i=0;i<Ncols();i++)
         for (In j=0;j<Nrows();j++)
            mElements[j][i] = arV[i_v++];
   }
   else
   {
      for (In i=0;i<Nrows();i++)
         for (In j=0;j<Ncols();j++)
            mElements[i][j] = arV[i_v++];
   }
   return;
}

/**
 *  Construct a banded matrix from the non-zero elements
 **/
template<class T>
void GeneralMidasMatrix<T>::FullBandMatrix(GeneralMidasMatrix<T>& arM)
{
   In n_rows=arM.Nrows();
   In n_cols=arM.Ncols();
   if (Nrows()!=arM.Nrows()) MIDASERROR(" Shape not compatible in MidasMatrix::FullBandMatrix ");
   for (In i=0; i<n_rows; i++)
   {
      In jhigh = min(i+n_cols-1,n_rows-1);
      for (In j=i; j<=jhigh; j++)
      {
         In jj=j-i;
         mElements[i][j]=arM[i][jj];
         mElements[j][i]=arM[i][jj];
      }
   }  
   return;
}

/**
 *  Invert a symmetric matrix
 *  Compute: A^-1 = U*d^-1*U^H
 **/  
template<class T>
typename GeneralMidasMatrix<T>::value_type InvertSymmetric(GeneralMidasMatrix<T>& aM) 
{
   MidasAssert(aM.Nrows()==aM.Ncols(),"Cannot invert a non-square matrix!");
   MidasAssert(!midas::type_traits::IsComplexV<T>, "Symmetric inverse not impl for complex");
   // Diagonalize matrix
   GeneralMidasMatrix<T> evec(aM.Nrows());
   GeneralMidasVector<T> eval(aM.Nrows());
   Diag(aM,evec,eval,"DGEEVX");
   if(gDebug) {
      Mout << "Eval:" << std::endl << eval << std::endl;
      Mout << "Evec:" << std::endl << evec << std::endl;
   }
   for(In i=I_0;i<aM.Nrows();i++) {
      for(In j=I_0;j<aM.Nrows();j++) {
         T tmp=T(C_0);
         for(In k=I_0;k<aM.Nrows();k++) {
            if(eval[k]==T(C_0)) {
               tmp+=evec[i][k]*(T(C_10_12))*midas::math::Conj(evec[j][k]);
               continue;
            }
            tmp+=evec[i][k]*(T(C_1)/eval[k])*midas::math::Conj(evec[j][k]);
         }
         aM[i][j]=tmp;
      }
   }
   Nb det = C_1;
   for(In i = I_0; i < aM.Nrows();i++)
      det*=eval[i];

   return det;
}

/**
 * Calculates (pseudo) inverse of general matrix using one of several methods:
 * 
 *    Using SVD:
 *       A = U*E*V^H => A^-1 = V*E^-1*U^H
 *    
 *       det(A) = det(U*E*V^H) = det(U)*det(E)*det(V^H) = sign()*det(E) = sign() * prod_k e_k
 *       where sign() = det(U)*det(V^T) = +-1
 *
 *       If determinant is zero, i.e the matrix is singular,
 *       then the Moore-Penrose pseudo inverse is returned.
 * 
 *       The SVD can be done using either GESDD (recommeded) or GESVD.
 * 
 *    Using LU factorisation:
 *       First, the LU factorisation is done using GETRF. Then, then inverse is
 *       computed using GETRI.
 *    
 *    ATTENTION:
 *    this function is a little inefficient at the moment as the determinant is
 *    calculated by LU decomposition, but the LU decomposition is not used for the SVD.
 *    Calculating determinant from SVD only gives absolute value, but with LU we also get the correct sign
 **/
template
   <  class T
   >
typename GeneralMidasMatrix<T>::value_type InvertGeneral
   (  GeneralMidasMatrix<T>& aM
   ,  const ilapack::util::RoutineName aMethod)
{
   using value_type = typename GeneralMidasMatrix<T>::value_type;
   MidasAssert(aM.Nrows() == aM.Ncols(), "Cannot invert a non-square matrix!");

   // Calculate inverse using SVD
   if (aMethod == ilapack::util::RoutineName::GESDD || aMethod == ilapack::util::RoutineName::GESVD)
   {
      auto lu = GETRF(aM); 
      auto determinant = lu.Determinant();
      auto svd = GESDD(aM);
      for (In i = 0; i < aM.Nrows(); ++i)
      {
         for (In j = 0; j < aM.Ncols(); ++j)
         {
            size_t u_idx = j;
            size_t vt_idx = svd.n*i;
            aM[i][j] = T(0.0);
            for (In k = 0; k < svd.Min(); ++k)
            {
               if (svd.s[k] != value_type(0.0))
               {
                  aM[i][j] += midas::math::Conj(svd.vt[vt_idx]) * midas::math::Conj(svd.u[u_idx]) / svd.s[k]; // u and vt are column-major
               }
               ++vt_idx;
               u_idx += svd.m;
            }
         }
      }
      return determinant;
   }
   // Calculate inverse using LU decomposition
   else if (aMethod == ilapack::util::RoutineName::GETRI)
   {
      MIDASERROR("Implement me!");
      auto lu = GETRF(aM);
      auto determinant = lu.Determinant();
      return determinant;
   }
   else
   {
      MIDASERROR("InvertGeneral: Unknown method!");
      return value_type(0);
   }
   
} 

/**
 *
 **/
template<class T>
GeneralMidasMatrix<T> InvertGeneralNonSquare(GeneralMidasMatrix<T> aM)
{
   using value_type = typename GeneralMidasMatrix<T>::value_type;
   if (aM.Nrows()==aM.Ncols()) 
   {
      value_type determinant=InvertGeneral(aM);
      return aM;
   }

   auto svd = GESDD(aM);
   
   for (In i = 0; i < aM.Nrows(); ++i)
   {
      for (In j = 0; j < aM.Ncols(); ++j)
      {
         size_t u_idx = j;
         size_t vt_idx = svd.n*i;
         aM[i][j] = T(0.0);
         for (In k = 0; k < svd.Min(); ++k)
         {
            if (svd.s[k] != value_type(0.0))
            {
               aM[i][j] += svd.vt[vt_idx] * svd.u[u_idx] / svd.s[k]; // u and vt are column-major
            }
            ++vt_idx;
            u_idx += svd.m;
         }
      }
   }

   return Transpose(aM);
} 

/**
 *
 **/
template<class T>
typename GeneralMidasMatrix<T>::value_type Invert(GeneralMidasMatrix<T>& aM)
{
   if (aM.IsSymmetric())
   {
      return InvertSymmetric(aM);
   }
   else
   {
      return InvertGeneral(aM);
   }
} 

/**
 *  Sqrt Invert matrix
 *  Compute: A^-1/2 = U^t*d^-1/2*U
 **/
template<class T>
void SqrtInvert(GeneralMidasMatrix<T>& aM) {
   if (aM.Nrows()!=aM.Ncols()) MIDASERROR("Cannot invert a non-square matrix!");
   // Diagonalize matrix
   GeneralMidasMatrix<T> evec(aM.Nrows());
   GeneralMidasVector<T> eval(aM.Nrows());
   Diag(aM,evec,eval,"DGEEVX");
   if(gDebug) {
      Mout << "Eval:" << std::endl << eval << std::endl;
      Mout << "Evec:" << std::endl << evec << std::endl;
   }
   for(In i=I_0;i<aM.Nrows();i++) {
      for(In j=I_0;j<aM.Nrows();j++) {
         T tmp=T(C_0);
         for(In k=I_0;k<aM.Nrows();k++) {
            if(eval[k]==T(C_0)) { 
               tmp+=evec[i][k]*(T(C_10_12))*evec[j][k];
               continue;
            }
            tmp+=evec[i][k]*(T(C_1)/sqrt(eval[k]))*evec[j][k];
         } 
         aM[i][j]=tmp;
      }
   }  
}

/**
 * Conjugate matrix in another container. Does not require square matrix.
 * But does in fact store an intermediate matrix!!!! Inefficient.
 **/
template<class T>
GeneralMidasMatrix<T> GeneralMidasMatrix<T>::Conjugate() const
{
   GeneralMidasMatrix<T> tmp(mNcols,mNrows);
   for (In i=I_0;i<mNrows;i++)
      for (In j=I_0;j<mNcols;j++) tmp[j][i] = midas::math::Conj(mElements[i][j]);
   return tmp;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template<class T>
void GeneralMidasMatrix<T>::ConjugateElems
   (
   )
{
   if constexpr(midas::type_traits::IsComplexV<T>)
   {
      for(Uin i = 0; i < mNrows; ++i)
      {
         for(Uin j = 0; j < mNcols; ++j)
         {
            mElements[i][j] = midas::math::Conj(mElements[i][j]);
         }
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
void GeneralMidasMatrix<T>::CopyDataFromPtr(T** aPtr, int aRows, int aCols, ilapack::col_major_t)
{
   SetNewSize(aRows,aCols);
   for(int i=0; i<Nrows(); ++i)
   {
      for(int j=0; j<Ncols(); ++j)
      {
         (*this)[i][j] = aPtr[j][i];
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
void GeneralMidasMatrix<T>::CopyDataFromPtr(T** aPtr, int aRows, int aCols, ilapack::row_major_t)
{
   SetNewSize(aRows,aCols);
   for(int i=0; i<Nrows(); ++i)
   {
      for(int j=0; j<Ncols(); ++j)
      {
         (*this)[i][j] = aPtr[i][j];
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
GeneralMidasVector<T> GeneralMidasMatrix<T>::GetDiag() const
{  
   if(Nrows() != Ncols())
   {
      MIDASERROR("GetDiag only works for square matrix");
   }

   GeneralMidasVector<T> diag(Nrows());
   for(In i=0; i<Nrows(); ++i)
   {
      diag[i] = mElements[i][i];
   }
   return diag;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::DifferenceNorm(const GeneralMidasMatrix<T>& aMat) const
{  
   // assert equal dimensions
   MidasAssert(((Nrows() == aMat.Nrows()) && (Ncols() == aMat.Ncols())),"Matrices are not equal dimension");
   
   // calculate difference norm
   real_t diff_norm = 0.0;
   for(In i=0; i<Nrows(); ++i)
   {
      for(In j=0; j<Ncols(); ++j)
      {
         auto diff = mElements[i][j] - aMat[i][j];
         diff_norm += midas::util::AbsVal2(diff);
      }
   }

   return sqrt(diff_norm)/Norm();
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::ConditionNumber() const
{
   // invert matrix
   GeneralMidasMatrix<T> invers(*this);
   auto det = InvertSymmetric(invers);
   
   // return condition number of matrix A as k(A) = ||A^-1|| * ||A||
   return this->Norm()*invers.Norm();
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t GeneralMidasMatrix<T>::ConditionNumberSVD() const
{
   // Do svd of matrix
   auto svd = GESDD(*this);
   
   // return condition number of matrix A as k(A) = s_max(A) / s_min(A)
   return svd.s[0]/svd.s[svd.Min()-1];
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffNorm2
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   )
{
   if (  arA.Nrows() != arB.Nrows()
      || arA.Ncols() != arB.Ncols()
      )
   {
      std::stringstream ss;
      ss << "Dimension mismatch;\n"
         << "   arA is " << arA.Nrows() << " x " << arA.Ncols() << '\n'
         << "   arB is " << arB.Nrows() << " x " << arB.Ncols() << '\n'
         ;
      MIDASERROR(ss.str());
   }

   typename GeneralMidasMatrix<T>::real_t dnorm2 = 0;
   for(Uin i = 0; i < arA.Nrows(); ++i)
   {
      for(Uin j = 0; j < arA.Ncols(); ++j)
      {
         dnorm2 += midas::util::AbsVal2(arA[i][j] - arB[i][j]);
      }
   }
   return dnorm2;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffNorm
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   )
{
   return std::sqrt(DiffNorm2(arA, arB));
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<class T>
typename GeneralMidasMatrix<T>::real_t DiffOneNorm
   (  const GeneralMidasMatrix<T>& arA
   ,  const GeneralMidasMatrix<T>& arB
   )
{
   if (  arA.Nrows() != arB.Nrows()
      || arA.Ncols() != arB.Ncols()
      )
   {
      std::stringstream ss;
      ss << "Dimension mismatch;\n"
         << "   arA is " << arA.Nrows() << " x " << arA.Ncols() << '\n'
         << "   arB is " << arB.Nrows() << " x " << arB.Ncols() << '\n'
         ;
      MIDASERROR(ss.str());
   }

   typename GeneralMidasMatrix<T>::real_t dnorm = 0;
   for(Uin i = 0; i < arA.Nrows(); ++i)
   {
      for(Uin j = 0; j < arA.Ncols(); ++j)
      {
         dnorm += midas::util::AbsVal(arA[i][j] - arB[i][j]);
      }
   }
   return dnorm;
}

#endif /* MIDASMATRIX_IMPL_H_INCLUDED */
