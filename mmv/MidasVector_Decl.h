/**
************************************************************************
* 
* @file                MidasVector.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen   (ove@chem.au.dk)  Original implementation
*                      Mikkel B. Hansen   (mbh@chem.au.dk?) Template rework
*                      Ian H. Godtliebsen (ian@chem.au.dk)  Interface separation+interface to libmda
*
* Short Description:   Declation of GeneralMidasVector class
* 
* Last modified: Thu Aug 13, 2009  12:19PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDASVECTOR_DECL_H_INCLUDED
#define MIDASVECTOR_DECL_H_INCLUDED

// std headers
#include <iostream> 
#include <string> 
#include <vector> 
#include <iterator>

// midas headers
#include "mmv_traits.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/MidasStream.h"
#include "util/Io.h"

// limbda headers
#include "libmda/interface.h"
#include "libmda/expr/interface.h"
#include "libmda/expr/expression.h" // for making constructor from binary_expression
#include "libmda/util/index_check.h"
#include "libmda/util/dimensions_check.h"

////////////////////////////////////////////////////
// some forward declarations of friends necessary
////////////////////////////////////////////////////
// The class itself
template <class T> class GeneralMidasVector;
template <class T> class GeneralMidasMatrix;
// the output overload operator
template <class T>
std::ostream& operator<<(std::ostream&,const GeneralMidasVector<T>&); 

//! The dot product function. First argument is complex conjugated (for complex T).
template <class T>
T Dot(const GeneralMidasVector<T>&,const GeneralMidasVector<T>&,In aBegin=I_0,In aNdots=-I_1);
template <class T>
T Dot(const GeneralMidasVector<T>&,const GeneralMidasVector<T>&,In aBegin1,In aBegin2,In aNdots);

//! Like Dot, but without the complex conjugation. (I.e. equivalent to Dot for real numbers.)
template <class T>
T SumProdElems
   (  const GeneralMidasVector<T>&
   ,  const GeneralMidasVector<T>&
   ,  const Uin aBeg = 0
   ,  const Uin aEnd = std::numeric_limits<Uin>::max()
   );

// the RmsDev... function
template <class T>
void RmsDevPerElement
   (  Nb& arRms
   ,  const GeneralMidasVector<T>&
   ,  const GeneralMidasVector<T>&
   ,  const In& arNoOfData
   ,  const In& arOffsetV1=I_0
   ,  const In& arOffsetV2=I_0
   );

// Mout
extern MidasStream Mout;

////////////////////////////////////////////////////
// setup index checking policies
////////////////////////////////////////////////////
#ifdef MDEBUG
// if debug build we check indices
using vector_dimensions_check = libmda::util::dimensions_check_stacktrace;
using vector_index_check = libmda::util::index_check_stack_trace;
#else // PRODUCTION BUILD
// if preoduction build we do not
using vector_dimensions_check = libmda::util::dimensions_nocheck;
using vector_index_check = libmda::util::index_nocheck;
#endif

/** 
*  A Midas class for vector algebra.
*  To test out the concepts of yang and to inteface with
*  the MidasMatrix class
* */
template <class T>
class GeneralMidasVector: 
   public libmda::expression_interface<GeneralMidasVector<T>
                                     , traits<GeneralMidasVector<T> >
                                     , vector_dimensions_check
                                     >
{
   public:
      typedef GeneralMidasVector<T>             type;
      typedef typename traits<type>::value_type value_type; // T
      typedef typename traits<type>::size_type  size_type;  // In
      static const int order = traits<type>::order;     // 1

      using real_t = midas::type_traits::RealTypeT<T>;

   private:
      // we interface to libmda to get expression template funcitonality
      using libmda_interface = libmda::expression_interface< GeneralMidasVector<T>
                                                           , traits<GeneralMidasVector<T> >
                                                           , vector_dimensions_check 
                                                           >; 
      // set index checking policy
      using index_check_policy = vector_index_check;

      size_type mCapacity;   ///< Number of allocated entries >= mLength.
      size_type mLength;     ///< Length of vector.
      value_type* mElements;   ///< Actual elements.

      void Allocate()   { mElements = new T[mCapacity]; }     ///< allocate space for elements
      void Deallocate() { if(mElements) delete[] mElements; mElements = nullptr; } ///< deallocate space for elements

      //! Implementation. Only to be called through PieceIo.
      template<class THIS, int N, class OTHER>
      static void PieceIoImpl
         (  THIS& arThis
         ,  const IoType<N>& aPutGet
         ,  OTHER&
         ,  In aLength
         ,  In aThisStart=I_0
         ,  In aThisStride=I_1
         ,  In aPieceStart=I_0
         ,  In aPieceStride=I_1
         ,  bool aAddTo=false
         ,  T aScaleFac=T(C_1)
         );

   public:
      explicit GeneralMidasVector(const In, const T* const);      ///< Constructor from vector.
      explicit GeneralMidasVector(const In=I_0, const T=T(C_0));  ///< Constructor from number.
      GeneralMidasVector(const GeneralMidasVector&);     ///< Copy constructor.
      explicit GeneralMidasVector(const vector<T>&);              ///< Copy constructor.
      
      ///> construction from libmda expression
      template<class A, libmda::Require_order<A,1> = 0> 
      GeneralMidasVector(const libmda::expr::expression_base<A>& aOther);

      ~GeneralMidasVector(); ///< destructor

      size_type Size() const; ///< return Length

      value_type& operator[](In i) {index_check_policy::apply(i,mLength); return *(mElements+i);} ///< overload [] - return reference
      const value_type& operator[](In i) const {index_check_policy::apply(i,mLength); return *(mElements+i);} ///< overload [] - return reference
      
      //CK HACK >>> needed temporarily for getting PES running with SELECTVIBS
      std::vector<T> GetStdVector()  const
      {
         std::vector<T> vec(Size()); 
         for (In i=I_0; i < Size();++i)
         {
            vec[i]=mElements[i];
         }
         return vec; 
      }
      //CK HACK
      value_type&       operator()(const size_type aI)       { return at(aI); }
      const value_type& operator()(const size_type aI) const { return at(aI); }
      
      value_type&       vec_access(const size_type aI)       { index_check_policy::apply(aI,mLength); return mElements[aI]; }
      const value_type& vec_access(const size_type aI) const { index_check_policy::apply(aI,mLength); return mElements[aI]; }
      
      //=======================================================================
      // libmda interface 
      //=======================================================================
      value_type&       at(const size_type aI)       { index_check_policy::apply(aI,mLength); return mElements[aI]; } ///< get element aI
      const value_type& at(const size_type aI) const { index_check_policy::apply(aI,mLength); return mElements[aI]; } ///< get element aI
      size_type size() const { return mLength; } ///< get total number of elements
      template<int N, libmda::iEnable_if<N==0> = 0> size_type extent() const { return mLength; } ///< get extent of dimension N
      using libmda_interface::operator(); ///< for char expression interface
      //=======================================================================
      // libmda interface end 
      //=======================================================================

      // get pointer to the data. functions uses no caps to 
      // have same interface as stl's vector class
            value_type* data()       { return mElements; } ///< get pointer to raw data (stl interface)
      const value_type* data() const { return mElements; } ///< get pointer to raw data (stl interface)

      GeneralMidasVector<T>& operator=(const GeneralMidasVector<T>&); ///< overload = 
      GeneralMidasVector<T>& operator=(GeneralMidasVector<T>&&); ///< move assignment
      using libmda_interface::operator=; ///< to make assignable from libmda expressions

      void Zero(); ///< zero all elements of the vector
      void UnitVector(In aI); ///< zero all elements of the vector except element aI. 

      void Reserve(const In aNewCapacity);              ///< Change capacity.
      void FillPart(const GeneralMidasVector<T>& arVec, const In& aStart);
      GeneralMidasVector<T> GetSubVec(const In& aStart, const In& aEnd) const;
      void Append(const GeneralMidasVector<T>& arNewVec);
      size_type Capacity() const {return mCapacity;}                 ///< get current capacity
      void SetNewSizeWithinCapacity(In aNewSize); // change size withing current capacity
      
      void SetNewSize(const In& aNewLength, const bool aSaveOldData=true); ///< Redefine length of vector.

      //@{
      //! Inserts range [aBeg;aEnd) into this starting at aPos.
      template<class InpIter>
      void Insert(Uin aPos, InpIter aBeg, InpIter aEnd);

      //! Insert value at aPos.
      void Insert(Uin aPos, T aVal);
      //@}

      //! Erases elements in range [aBeg;min(aEnd,Size())), reducing size accordingly.
      void Erase(Uin aBeg, Uin aEnd);

      template
         <  class U
         >
      void Scale(const U aNb,In aBegin=I_0,In aN=-I_1);                    ///< Scale with scalar

      void PutToNb(const T, In aLength, In aStart=I_0, In aStride=I_1);    ///< Put some elements to a number 
      void Clear(); ///< Clear the vector.
      
      inline void SetPointerToNull() { mCapacity=I_0; mLength=I_0; mElements = nullptr; } 
      ///< Sets the elements pointer to NULL without destroying it
      ///< Only use this function if you know what you are doing to avoid memory leaks !!
      void SetData(T*& aElements, In aCapacity, In aLength);
      void SetData(T*&& aElements, In aCapacity, In aLength) { SetData(aElements, aCapacity, aLength); }
      ///< set data to pointer... use with caution!
      void StealData(GeneralMidasVector<T>& aVictimVec);
      ///< steal data from another MidasVector... use with caution!

      //! Release data to unique_ptr. This obj. becomes empty. Safer than SetData/SetPointerToNull.
      std::unique_ptr<T[]> Release();
      
      void Pow(const In aIpow); ///< Set elements to some power
      void Pow(const Nb aIpow); ///< Set elements to some power
      void Pow(const vector<In> aPower); ///< Set elements to some string of powers
      
      void PutToDiag(GeneralMidasMatrix<T>& aM);                                       ///< Put to diagonal of matrix.

      //! Storage the matrix row by row in the vector.
      void MatrixRowByRow(const GeneralMidasMatrix<T>&, size_t aColOffSet= 0, size_t aRowOffSet = 0);

      //! Storage the matrix col. by col. in the vector.
      void MatrixColByCol(const GeneralMidasMatrix<T>&, size_t aColOffSet= 0, size_t aRowOffSet = 0);

      void Copy(const GeneralMidasVector<T>& aV, In aLength=-I_1); /// Copy the first aLength elements, default is whole vector
      void Reassign(const GeneralMidasVector<T>& aV, In aNewLength=-I_1);
      /// Reassign vector to the first elements of another vector, Size is reset, default is full vector
      
      //@{
      //! Io some elements to/from a vector. See PieceIoImpl for details on Args...
      template<int N, class... Args, std::enable_if_t<bool(N & IO_GET), int> = 0>
      void PieceIo(const IoType<N>&, GeneralMidasVector&, Args...) const;

      template<int N, class... Args, std::enable_if_t<bool(N & IO_PUT), int> = 0>
      void PieceIo(const IoType<N>&, const GeneralMidasVector&, Args...);
      //@}
      
      void ChangeSign();
      void Shift(const T aNb);
      void DumpToReadableFileWithName(std::string aName) const;
      void StatAnalysisAndOutPut(Nb& arAve, Nb& arAveDev, Nb& arStdDev, Nb& arVar, 
           Nb& arSkew, Nb& arCurt,std::string& arHeader,In aHistSize,
           Nb aMinHist,Nb aMaxHist,std::string aDir, std::string aFilNam1, 
           std::string aFilNam2) const;
      void StatAnalysis(Nb& arAve, Nb& arAveDev, Nb& arStdDev, Nb& arVar, Nb& arSkew, Nb& arCurt) const;
      void MakeHisto(In aNhistoLevels,GeneralMidasVector& arMidValues,
             std::vector<In>& arScores,GeneralMidasVector& arHistRel, 
             Nb aMin, Nb aMax,Nb& aDelta) const;
      
      value_type FindMinValue() const;
      value_type FindMaxValue() const;
      real_t FindMaxAbsValue() const;

      friend void RmsDevPerElement<>
         (  Nb&
         ,  const GeneralMidasVector<T>&
         ,  const GeneralMidasVector<T>&
         ,  const In&
         ,  const In&
         ,  const In&
         );
      ///< compute rms between elements of two MidasVectors
      

      friend T Dot<>(const GeneralMidasVector<T>&,const GeneralMidasVector<T>&,In,In);///< Inner product,-I_1 means all!
      friend T Dot<>(const GeneralMidasVector<T>&,const GeneralMidasVector<T>&,In,In,In);///< Inner product,-I_1 means all!

      friend std::ostream& operator<< <>(std::ostream&, const GeneralMidasVector<T>&);                    ///< Output overload

      template<typename TT>
      friend void StandardZaxpy
         (  In
         ,  Nb
         ,  const GeneralMidasVector<TT>&
         ,  In
         ,  const GeneralMidasVector<TT>&
         ,  In
         ,  GeneralMidasVector<TT>&
         ,  In
         );      /// Standard z=a*x+y

      void Print() { Mout << *this; }

      value_type SumEle(In aOff=I_0, In aN=-I_1) const; ///< Sum of aN element starting at address aOff.
      value_type ProductEle() const;             ///< compute the product of the elements.
      
      real_t SumAbsEle() const;              ///< Sum of absolute values of all elements.
      real_t ProductAbsEle() const;                   ///< compute the product of the absolute values of the elements
      real_t Norm1() const;                       ///< 1-norm of all elements
      
      real_t Norm2(In aOff=I_0, In aN=-I_1) const;
      ///< 2-norm of aN elements starting at address aOff.
      
      //! Norm^2 of difference between two vectors.
      real_t DiffNorm2(const GeneralMidasVector& arOther, In aOff=I_0, In aN=-I_1) const;

      //! sqrt(DiffNorm2)
      real_t DiffNorm(const GeneralMidasVector& arOther, In aOff=I_0, In aN=-I_1) const;

      //! Sum of absolute values of vector difference elements.
      real_t DiffOneNorm(const GeneralMidasVector& arOther) const;

      //! Return `|this_scale*(*this) - other_scale*other|^2`, or possibly with `Conj(*this)`.
      template<bool CONJ_THIS = false>
      real_t DiffNorm2Scaled
         (  const GeneralMidasVector& arOther
         ,  const T aThisScale = T(1)
         ,  const T aOtherScale = T(1)
         ,  In aOff = 0
         ,  In aN = -1
         )  const;

      void Normalize();
      real_t NormMax() const;    ///< Max-norm of all elements
      real_t Norm() const;       ///< Usual norm (sqrt 2-norm) 
      real_t NormRobust() const; ///< Usual norm in a version robust to large numbers
      real_t Rms() const;      ///< Usual rms sqrt( (sum squared)/n) 
      void MultiplyWithMatrix(const GeneralMidasMatrix<T>&);
      void LeftMultiplyWithMatrix(const GeneralMidasMatrix<T>&); ///<Left product with a matrix

      GeneralMidasVector<T> Conjugate() const;
      void DumpVertical(const std::string& arLabel);
      void PrintVertical(const std::string& arLabel);
      
      //============================================================
      // MidasVector iterator interface
      //============================================================
      //
      // normal iterator
      //
      class MidasVectorIter: public std::iterator<std::forward_iterator_tag, value_type>
      {
         public:
            typedef std::iterator<std::forward_iterator_tag, value_type> iter_type;
            typedef typename iter_type::difference_type difference_type;
         
         private:
            value_type* m_p;
         public:
            MidasVectorIter(value_type* x): m_p(x) {}
            MidasVectorIter(const MidasVectorIter& mit): m_p(mit.m_p) { }
            
            MidasVectorIter& operator++() {++m_p; return *this; }
            MidasVectorIter  operator++(int) {MidasVectorIter tmp(*this); operator++(); return tmp; }
            MidasVectorIter& advance(const difference_type i) { std::advance(m_p,i); return *this; }
            MidasVectorIter  advance(const difference_type i) const { MidasVectorIter tmp(*this); tmp.advance(i); return tmp; }
            bool operator==(const MidasVectorIter& rhs) { return m_p==rhs.m_p; }
            bool operator!=(const MidasVectorIter& rhs) { return m_p!=rhs.m_p; }
            T& operator*()             { return *m_p; }
            const T& operator*() const { return *m_p; }
      };
      
      using iterator       =       MidasVectorIter;
      using const_iterator = const MidasVectorIter;
      
      //
      // stl compliant iterator interface
      //
      iterator       begin()       { return MidasVectorIter(mElements); }
      iterator       end()         { return MidasVectorIter(mElements+mLength); }
      const_iterator begin() const { return MidasVectorIter(mElements); }
      const_iterator end()   const { return MidasVectorIter(mElements+mLength); }
      
      //
      // stride iterator
      //
      class MidasVectorStrideIter: public std::iterator<std::forward_iterator_tag, value_type>
      {
         public:  
            typedef std::iterator<std::forward_iterator_tag, value_type> iter_type;
            typedef typename iter_type::difference_type difference_type;
         
         private:
            value_type*           m_p;
            const difference_type m_stride;
         
         public:
            MidasVectorStrideIter(value_type* x, const difference_type stride):
               m_p(x), m_stride(stride) { }
            MidasVectorStrideIter(const MidasVectorIter& mit): 
               m_p(mit.m_p), m_stride(mit.stride) { }
            
            MidasVectorStrideIter& operator++() {m_p+=m_stride; return *this; }
            MidasVectorStrideIter  operator++(int) 
            {MidasVectorStrideIter tmp(*this); operator++(); return tmp; }
            MidasVectorStrideIter& advance(const difference_type i) { std::advance(m_p,i*m_stride); return *this; }
            bool operator==(const MidasVectorStrideIter& rhs) { return m_p==rhs.m_p; }
            bool operator!=(const MidasVectorStrideIter& rhs) { return m_p!=rhs.m_p; }
            T& operator*()             { return *m_p; }
            const T& operator*() const { return *m_p; }
      };
      
      using stride_iterator       =       MidasVectorStrideIter;
      using const_stride_iterator = const MidasVectorStrideIter;
      
      //
      // 'non-stl compliant' interface for MidasVectorStrideIter
      //
      stride_iterator       begin(const typename MidasVectorStrideIter::difference_type a_stride
                                , const typename MidasVectorStrideIter::difference_type a_length
                                , const typename MidasVectorStrideIter::difference_type a_offset=0) 
      { return MidasVectorStrideIter(mElements+a_offset, a_stride); }
      stride_iterator       begin(const typename MidasVectorStrideIter::difference_type a_stride)
      { return begin(a_stride,floor(mLength/a_stride),0); } // overload

      //
      stride_iterator       end(const typename MidasVectorStrideIter::difference_type a_stride
                              , const typename MidasVectorStrideIter::difference_type a_length
                              , const typename MidasVectorStrideIter::difference_type a_offset=0) 
      { 
         typename MidasVectorStrideIter::difference_type offset = a_offset+a_stride*a_length;
         if(offset>mLength)
         {
            MIDASERROR(" ITER POINTS BEYOND END !");
         }
         return MidasVectorStrideIter(mElements+offset, a_stride); 
      }
      stride_iterator       end(const typename MidasVectorStrideIter::difference_type a_stride)
      { return end(a_stride,floor(mLength/a_stride),0); } // overload
      
      //
      const_stride_iterator begin(const typename MidasVectorStrideIter::difference_type a_stride
                                , const typename MidasVectorStrideIter::difference_type a_length
                                , const typename MidasVectorStrideIter::difference_type a_offset=0) const
      { return MidasVectorStrideIter(mElements+a_offset, a_stride); }
      const_stride_iterator begin(const typename MidasVectorStrideIter::difference_type a_stride) const
      { return begin(a_stride,floor(mLength/a_stride),0); } // overload
      
      //
      const_stride_iterator end(const typename MidasVectorStrideIter::difference_type a_stride
                              , const typename MidasVectorStrideIter::difference_type a_length
                              , const typename MidasVectorStrideIter::difference_type a_offset=0) const
      { 
         typename MidasVectorStrideIter::difference_type offset = a_offset+a_stride*a_length;
         if(offset>mLength)
         {
            MIDASERROR(" ITER POINTS BEYOND END !");
         }
         return MidasVectorStrideIter(mElements+offset, a_stride); 
      }
      const_stride_iterator end(const typename MidasVectorStrideIter::difference_type a_stride) const
      { return end(a_stride,floor(mLength/a_stride),0); } // overload
      //============================================================
      // MidasVector iterator interface stop
      //============================================================


      //============================================================
      // MPI interface begin
      //============================================================
#ifdef VAR_MPI
   public:
      //! Send data
      void MpiSend
         (  In aRank
         ,  In aNData = -I_1
         ,  In aOffset = I_0
         )  const;

      //! Recv data
      void MpiRecv
         (  In aRank
         ,  In aNData = -I_1
         ,  In aOffset = I_0
         );

      //! Bcast data
      void MpiBcast
         (  In aRank
         ,  In aNData = -I_1
         ,  In aOffset = I_0
         );

      //! Ibcast only mElements
      void MpiIbcastElements
         (  In aRank
         ,  MPI_Request* apReq
         ,  In aNData = -I_1
         ,  In aOffset = I_0
         );

#endif /* VAR_MPI */
      //============================================================
      // MPI interface stop
      //============================================================
};


#endif /* MIDASVECTOR_DECL_H_INCLUDED */
