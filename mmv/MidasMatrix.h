#ifndef MIDASMATRIX_H_INCLUDED
#define MIDASMATRIX_H_INCLUDED

// Midas headers
#include "mmv/MidasMatrix_Decl.h"
#include "mmv/MidasMatrix_Impl.h"
#include "MidasVector.h"

// Aliases
using MidasMatrix = GeneralMidasMatrix<Nb>;
using ComplexMidasMatrix = GeneralMidasMatrix<std::complex<Nb> >;

// This is some old stuff that should probably be (re)moved
Nb pythag(const Nb a, const Nb b);
template<class T> inline const T MAX(const T &a, const T &b) {return b > a ? (b) : (a);}
template<class T> inline const T MIN(const T &a, const T &b) {return b < a ? (b) : (a);}
template<class T> inline const T SIGN(const T &a, const T &b) {return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

/**
 * Norm2 for ODE.
 * See OdeNorm2() for MidasVector.
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
ABSVAL_T OdeNorm2
   (  const GeneralMidasMatrix<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasMatrix<PARAM_T>& aYOld
   ,  const GeneralMidasMatrix<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   const auto nrows = aDeltaY.Nrows();
   assert(nrows == aYOld.Nrows());
   assert(nrows == aYNew.Nrows());
   const auto ncols = aDeltaY.Ncols();
   assert(ncols == aYOld.Ncols());
   assert(ncols == aYNew.Ncols());

   // AbsTol must be > 0
   assert(aAbsTol > C_0);

   ABSVAL_T result = 0;
   ABSVAL_T sc_i = 0;

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   for(Uin i=I_0; i<nrows; ++i)
   {
      for(Uin j=I_0; j<ncols; ++j)
      {
         sc_i = aAbsTol + std::max(AbsVal(aYOld[i][j]), AbsVal(aYNew[i][j]))*aRelTol;
         result += AbsVal2(aDeltaY[i][j]/sc_i);
      }
   }

   return result;
}

/**
 * Mean Norm2 for ODE.
 * See OdeMeanNorm2() for MidasVector.
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
auto OdeMeanNorm2
   (  const GeneralMidasMatrix<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasMatrix<PARAM_T>& aYOld
   ,  const GeneralMidasMatrix<PARAM_T>& aYNew
   )
{
   const Uin size = aDeltaY.Nrows() * aDeltaY.Ncols();
   assert(size > 0);
   return OdeNorm2(aDeltaY, aAbsTol, aRelTol, aYOld, aYNew) / size;
}

/**
 * MaxNorm2 for ODE.
 * See OdeMaxNorm2() for MidasVector.
 **/
template
   <  class PARAM_T
   ,  class ABSVAL_T
   >
auto OdeMaxNorm2
   (  const GeneralMidasMatrix<PARAM_T>& aDeltaY
   ,  ABSVAL_T aAbsTol
   ,  ABSVAL_T aRelTol
   ,  const GeneralMidasMatrix<PARAM_T>& aYOld
   ,  const GeneralMidasMatrix<PARAM_T>& aYNew
   )
{
   static_assert(std::is_floating_point_v<ABSVAL_T>, "ABSVAL_T must be floating-point.");

   const auto nrows = aDeltaY.Nrows();
   assert(nrows == aYOld.Nrows());
   assert(nrows == aYNew.Nrows());
   const auto ncols = aDeltaY.Ncols();
   assert(ncols == aYOld.Ncols());
   assert(ncols == aYNew.Ncols());

   // AbsTol must be > 0
   assert(aAbsTol > C_0);

   ABSVAL_T result = 0;
   ABSVAL_T sc_i = 0;

   using midas::util::AbsVal;
   using midas::util::AbsVal2;
   for(Uin i=I_0; i<nrows; ++i)
   {
      for(Uin j=I_0; j<ncols; ++j)
      {
         sc_i = aAbsTol + std::max(AbsVal(aYOld[i][j]), AbsVal(aYNew[i][j]))*aRelTol;
         result = std::max(result, AbsVal2(aDeltaY[i][j]/sc_i));
      }
   }

   return result;
}

/**
 * Axpy for ODE and other stuff.
 **/
template
   <  typename PARAM_T
   ,  typename PARAM_U
   ,  typename COEF_T
   >
void Axpy
   (  GeneralMidasMatrix<PARAM_T>& arThis
   ,  const GeneralMidasMatrix<PARAM_U>& aOther
   ,  COEF_T aCoef
   )
{
   auto nrows = arThis.Nrows();
   auto ncols = arThis.Ncols();

   assert(nrows == aOther.Nrows());
   assert(ncols == aOther.Ncols());

   for(In i=I_0; i<nrows; ++i)
   {
      for(In j=I_0; j<ncols; ++j)
      {
         arThis[i][j] += PARAM_T(aCoef * aOther[i][j]);
      }
   }
}

#endif /* MIDASMATRIX_H_INCLUDED */
