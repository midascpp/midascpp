#include <numeric>


/**
 *
 * @brief Calculates Solid Harmonics
 *
 * Purpose: Calculates Solid Harmonics
 *
 *          $ R_{l m} = \sqrt{\frac{4 \pi}{2 l + 1}} r^l Y_{l m} $
 *
 * The implementation is based on Eq. 5.1.16 in Ref[1]
 *
 *  r^l Y_lm (\theta,\phi) = [ (2l+1)/(4\pi) (l+m)!(l-m)!]^(1/2)
 *                         * \sum_pqs (-(x+iy)/2)^p ((x-iy)/2)^q z^s/p!q!r!
 *
 *
 * [1] D.A. Varshalovich, A.N. Moskalev and V.K. Khersonsky, 
 *     Quantum Theory of Angular Momentum, Nauka, Leningrad, 1975 (Russian) 
 *
 *
 *  @param l   
 *  @param m
 *  @param x cartesian vector. Solid Harmonic will be evaluated for the 
 *         point on the unit sphere corresponding to the direction of x  
 **/
template <class T>
std::complex<T> SolidHarmonicR
   (  int& l
   ,  int& m
   ,  std::vector<T>& x
   )
{

   std::complex<T> SolidRCartesian(0,0);

   auto fact = [] (const int& num) 
   {
      T retval = 1.0;

      for (int i = 1; i <= num; i++)
      {
         retval = retval * i;
      }
      return retval;
   };

   for (int p = 0; p <= l; p++)
   {
      int q = p - m;
      int r = l - p - q;

      if ((q >= 0) && (r >= 0)) 
      {
         SolidRCartesian = SolidRCartesian + ( std::pow(std::complex<T>(-0.5 * x[0], -0.5 * x[1]), p)
                                             * std::pow(std::complex<T>( 0.5 * x[0], -0.5 * x[1]), q)
                                             * std::pow(x[2],r)
                                             / (fact(p) * fact(q) * fact(r))
                                             );

      }
   }

   SolidRCartesian = SolidRCartesian * std::sqrt(fact(l + m) * fact(l - m));


   return SolidRCartesian;
}


/**
 *
 * @brief Calculates Spherical Harmonics
 *
 *
 * $ Y_{l m} = \sqrt{\frac{2l+1}{4\pi}} \frac{1}{r^l}  R_{l m} $
 *
 *
 **/
template <class T>
std::complex<T> SphericalHarmonicY
   (  int& l
   ,  int& m
   ,  std::vector<T>& x
   )
{
   T normsq = std::inner_product(std::begin(x), std::end(x), std::begin(x), 0.0);

   return SolidHarmonicR<T>(l, m, x) * std::sqrt(((2.0 * l) + 1) / (4.0 * M_PI)) * std::pow(normsq,-0.5 * l); 
}




