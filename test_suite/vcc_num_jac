#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   -------------
   Molecule:         Water
   Wave Function:    VCC
   Test Purpose:     Checking numerical VCC Jacobian used for
                     obtaining VCC response excitation energies.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc_num_jac.minp <<%EOF%
#0 MIDAS Input

#1 General
#2 IoLevel
    5 
#2 Time

#1 Vib

// Hamiltonian.
#2 Operator
   #3 OperFile
      vcc_num_jac.mop 
   #3 SetInfo
      type=energy

#2 Basis
   #3 Name
      basis
   #3 HoBasis 
      6
   #3 IoLevel
      1

#2 Vscf
   #3 IoLevel
      1
   #3 Name 
      scf0
   #3 Basis
      basis
   #3 Occup
      [3*0] 
   #3 Threshold
      1.0e-14
   #3 MaxIter
      20

#2 Vcc
   #3 Vcc
   #3 UseAllVscfModals
   #3 Transformer
       trf=orig outofspace=false t1transh=false
   #3 MaxExci
      2
   #3 ItEqResidThr
      1.0e-12
   #3 Basis 
      basis 
   #3 VecStorage 
      0
   #3 ItEqMaxDim
      150
   #3 ItEqMaxIt
      40
   #3 ItEqMaxItMicro
      40
   #3 ItEqMicroThrFac 
      0.0001
   #3 ItEqBreakDim
      100
   #3 Rsp
      #4 DiagMeth 
  LAPACK
      #4 NumVccJacobian
         1.0E-6
      #4 IoLevel
         3  
      #4 Eigen val
         6
      #4 ItEqResidThr
         1.0e-12
      #4 ItEqMaxIt
         100
      #4 ItEqMaxDim
         200
      #4 ItEqBreakDim
         150

#0 Midas Input End

%EOF%

#######################################################################
#  OPERATOR
#######################################################################
cat > vcc_num_jac.mop <<%EOF%
DALTON_FOR_MIDAS
 2.6583506951283198000000E-08    1     
 2.8029808305518600000000E-05    1     1     
 1.3248661900888692000000E-07    1     1     1     
-7.3313799475727137000000E-11    1     1     1     1     
 2.5919311497091257000000E-07    2     
 1.5239002544831237000000E-04    2     2     
 3.1792388313078845000000E-06    2     2     2     
 4.3954685224889545000000E-08    2     2     2     2     
-7.9296569310827181000000E-12    3     
 1.6080418447472766000000E-04    3     3     
 2.0577317627612501000000E-11    3     3     3     
 4.6942489007051336000000E-08    3     3     3     3     
 5.0012545216304716000000E-09    1     2     
-2.6612713099893881000000E-07    1     2     2     
-9.9230419436935335000000E-09    1     2     2     2     
-7.2289179797735414000000E-07    1     1     2     
-4.6007471610209905000000E-08    1     1     2     2     
 1.0190390753450629000000E-08    1     1     1     2     
-6.5369931689929217000000E-12    1     3     
-9.5447182957286714000000E-07    1     3     3     
 1.8360424292040989000000E-11    1     3     3     3     
 4.7748471843078732000000E-11    1     1     3     
-5.6161525208153762000000E-08    1     1     3     3     
 2.5295321393059567000000E-12    1     1     1     3     
-1.5461409930139780000000E-11    2     3     
 9.8001667083735811000000E-06    2     3     3     
 4.0984104998642579000000E-11    2     3     3     3     
 1.3028511602897197000000E-10    2     2     3     
 2.7204805519431829000000E-07    2     2     3     3     
 1.2391865311656147000000E-10    2     2     2     3     
-6.3096194935496897000000E-12    1     2     3     
-5.6911630963440984000000E-08    1     2     3     3     
 8.4696694102603942000000E-11    1     2     2     3     
 1.2744294508593157000000E-10    1     1     2     3     
%EOF%
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Check excitation energies-
# Rsp_Root_0 EigenValue                      7.1823735413574230E-03 au    1
# Rsp_Root_1 EigenValue                      1.4228037440818936E-02 au    1
# Rsp_Root_2 EigenValue                      1.6840227560717937E-02 au    1
# Rsp_Root_3 EigenValue                      1.7220793089452884E-02 au    1
# Rsp_Root_4 EigenValue                      2.1094141636028310E-02 au    1
# Rsp_Root_5 EigenValue                      2.3953785124301640E-02 au    1
CRIT1=`$GREP "Rsp_Root_0 EigenValue" $log | grep "7.182373541.*E-03" |  wc -l`
CRIT2=`$GREP "Rsp_Root_1 EigenValue" $log | grep "1.422803744.*E-02" |  wc -l`
CRIT3=`$GREP "Rsp_Root_2 EigenValue" $log | grep "1.684022756.*E-02" |  wc -l`
CRIT4=`$GREP "Rsp_Root_3 EigenValue" $log | grep "1.72207930[89].*E-02" |  wc -l`
CRIT5=`$GREP "Rsp_Root_4 EigenValue" $log | grep "2.109414163.*E-02" |  wc -l`
CRIT6=`$GREP "Rsp_Root_5 EigenValue" $log | grep "2.3953785124.*E-02" |  wc -l`

TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
echo $CRIT1 $CRIT2 $CRIT3 $CRIT4 $CRIT5 $CRIT6

CTRL[1]=6
ERROR[1]="EXCITATION ENERGIES NOT CORRECT"

PASSED=1
for i in 1
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vcc_num_jac.check
#######################################################################
