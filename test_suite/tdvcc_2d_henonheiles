#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="tdvcc_2d_henonheiles"

#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2M-Henon-Heiles
   Wave Function:    TDVCC/VCI
   Test Purpose:     Propagate and compare TDVCC/VCI states.
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0
   #3 KineticEnergy
      USER_DEFINED
   #3 OperInput
   #4 OperatorTerms
      -0.5000000000000 DDQ^2(Q0)
      -0.5000000000000 DDQ^2(Q1)
       0.5000000000000 Q^2(Q0)
       0.5000000000000 Q^2(Q1)
      -0.0372676666667 Q^3(Q1)
       0.1118030000000 Q^2(Q0) Q^1(Q1)
#2 Operator
   #3 Name
      h0_init
   #3 KineticEnergy
      USER_DEFINED
   #3 OperInput
   #4 OperatorTerms
      -0.5000000000000 DDQ^2(Q0)
      -0.5000000000000 DDQ^2(Q1)
      -1.0000000000000 Q^1(Q0)
       0.5000000000000 Q^2(Q0)
       0.5000000000000 Q^2(Q1)
#2 Operator
   #3 Name
      q0
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q0)
#2 Operator
   #3 Name
      q1
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q1)
#2 Operator
   #3 Name
      ip0
   #3 OperInput
   #4 OperatorTerms
      1.0 DDQ^1(Q0)
#2 Operator
   #3 Name
      ip1
   #3 OperInput
   #4 OperatorTerms
      1.0 DDQ^1(Q1)

#2 Basis
   #3 Name
      basis
   #3 Define
      Q0 Bsplines leftb=-20, rightb=20, dens=2.0, iord=10
      Q1 Bsplines leftb=-20, rightb=20, dens=2.0, iord=10

#2 Vscf
   #3 Name
      vscf_init
   #3 Basis
      basis
   #3 Oper
      h0_init
   #3 Threshold
      1.0e-15

#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      tdvci
   #3 CorrMethod
      vci
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [2*7]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e2
      #4 OutputPoints
         101
      #4 Tolerance
         1.e-15 1.e-15
      #4 MaxSteps
         -1
      #4 DatabaseSaveVectors
   #3 Properties
      energy
      phase
      autocorr
   #3 Statistics
      fvcinorm2
      norm2
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      ip0
      ip1
   #3 CompareFvciVecs

#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      tdvcc_tight
   #3 CorrMethod
      vcc
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [2*7]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e2
      #4 OutputPoints
         101
      #4 Tolerance
         1.e-15 1.e-15
      #4 MaxSteps
         -1
      #4 DatabaseSaveVectors
   #3 Properties
      energy
      phase
      autocorr
   #3 Statistics
      fvcinorm2
      norm2
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      ip0
      ip1
   #3 CompareFvciVecs

#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      tdvcc_loose
   #3 CorrMethod
      vcc
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [2*7]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e2
      #4 OutputPoints
         101
      #4 Tolerance
         1.e-7 1.e-7
      #4 MaxSteps
         -1
      #4 DatabaseSaveVectors
   #3 Properties
      energy
      phase
      autocorr
   #3 Statistics
      fvcinorm2
      norm2
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      ip0
      ip1
   #3 CompareFvciVecs

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Utilities
# ------------------------------------------------------------------------------
# For asserting deviations from zero.
#   $1: actual deviation
#   $2: threshold
#   return: 1 if <= threshold, 0 otherwise.
accept_dev()
{
   awk '\'' BEGIN{if('\''$1'\'' <= '\''$2'\''){printf("1")}else{printf("0")}} '\''
}

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")


# ------------------------------------------------------------------------------
# Check energy std deviations
# ------------------------------------------------------------------------------
estd_real=($($GREP "ENERGY, std \(real\)" $log | awk '\''{print $5}'\''))
estd_imag=($($GREP "ENERGY, std \(imag\)" $log | awk '\''{print $5}'\''))
CRIT_ITER_ESTD_VCI[0]=$(accept_dev "${estd_real[0]}" "2.0e-14")
CRIT_ITER_ESTD_VCI[1]=$(accept_dev "${estd_imag[0]}" "2.0e-16")
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_ESTD_VCI[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCI energy std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_ESTD_VCI[$i].")
done
CRIT_ITER_ESTD_VCC_TIGHT[0]=$(accept_dev "${estd_real[1]}" "6.0e-15")
CRIT_ITER_ESTD_VCC_TIGHT[1]=$(accept_dev "${estd_imag[1]}" "2.0e-15")
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_ESTD_VCC_TIGHT[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (tight) energy std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_ESTD_VCC_TIGHT[$i].")
done
CRIT_ITER_ESTD_VCC_LOOSE[0]=`$GREP "ENERGY, std \(real\)" $log | grep "4.42142...........E-07" |  wc -l`
CRIT_ITER_ESTD_VCC_LOOSE[1]=`$GREP "ENERGY, std \(imag\)" $log | grep "1.88736...........E-07" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_ESTD_VCC_LOOSE[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (loose) energy std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_ESTD_VCC_LOOSE[$i].")
done

# ------------------------------------------------------------------------------
# Check <q0> std deviations
# ------------------------------------------------------------------------------
q0std_imag=($($GREP "q0, std \(imag\)" $log | awk '\''{print $5}'\''))
CRIT_ITER_Q0STD_VCI[0]=`$GREP "q0, std \(real\)" $log | grep "5.80890183117.....E-01" |  wc -l`
CRIT_ITER_Q0STD_VCI[1]=$(accept_dev "${q0std_imag[0]}" "2.0e-16")
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_Q0STD_VCI[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCI <q0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_Q0STD_VCI[$i].")
done
CRIT_ITER_Q0STD_VCC_TIGHT[0]=`$GREP "q0, std \(real\)" $log | grep "5.8089130[5-6]........E-01" |  wc -l`
CRIT_ITER_Q0STD_VCC_TIGHT[1]=$(accept_dev "${q0std_imag[1]}" "4.0e-15")
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_Q0STD_VCC_TIGHT[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (tight) <q0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_Q0STD_VCC_TIGHT[$i].")
done
CRIT_ITER_Q0STD_VCC_LOOSE[0]=`$GREP "q0, std \(real\)" $log | grep "5.80957...........E-01" |  wc -l`
CRIT_ITER_Q0STD_VCC_LOOSE[1]=`$GREP "q0, std \(imag\)" $log | grep "7.38564...........E-08" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_Q0STD_VCC_LOOSE[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (loose) <q0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_Q0STD_VCC_LOOSE[$i].")
done

# ------------------------------------------------------------------------------
# Check <ip0> std deviations
# ------------------------------------------------------------------------------
ip0std_real=($($GREP "ip0, std \(real\)" $log | awk '\''{print $5}'\''))
CRIT_ITER_IP0STD_VCI[0]=$(accept_dev "${ip0std_real[0]}" "2.0e-16")
CRIT_ITER_IP0STD_VCI[1]=`$GREP "ip0, std \(imag\)" $log | grep "5.7939036329[01].....E-01" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_IP0STD_VCI[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCI <ip0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_IP0STD_VCI[$i].")
done
CRIT_ITER_IP0STD_VCC_TIGHT[0]=$(accept_dev "${ip0std_real[1]}" "4.0e-15")
CRIT_ITER_IP0STD_VCC_TIGHT[1]=`$GREP "ip0, std \(imag\)" $log | grep "5.7938919[3-4]........E-01" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_IP0STD_VCC_TIGHT[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (tight) <ip0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_IP0STD_VCC_TIGHT[$i].")
done
CRIT_ITER_IP0STD_VCC_LOOSE[0]=`$GREP "ip0, std \(real\)" $log | grep "9.14800...........E-08" |  wc -l`
CRIT_ITER_IP0STD_VCC_LOOSE[1]=`$GREP "ip0, std \(imag\)" $log | grep "5.79321...........E-01" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_IP0STD_VCC_LOOSE[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''VCC (loose) <ip0> std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_IP0STD_VCC_LOOSE[$i].")
done


# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
