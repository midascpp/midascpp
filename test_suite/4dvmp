#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy  in various 
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980), for operator
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals

                     Test covers both the 
                     FtesttranExpH (primitive full basis explicit H CI)
                     ExpHtesttran option.
                     In the later case both the full testtran which gives results
                     equivalent to the first, and in 
                     addition the single excited case, which in the modal
                     basis, gives the HF result for the reference state, 
                     which I thing is a nice check.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 4dvmp.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
  5
#2 BufSIZE
 120
#2 FileSize
 500
#2 Time
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
   5

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a0)
-0.5 DDQ^2(b0)
0.98 Q^2(a0)
0.245 Q^2(b0)
0.0064 Q^3(a0)
-0.08 Q^1(a0) Q^2(b0)
-0.5 DDQ^2(a1)
-0.5 DDQ^2(b1)
0.98 Q^2(a1)
0.245 Q^2(b1)
0.0064 Q^3(a1)
-0.08 Q^1(a1) Q^2(b1)
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a0 HO n_high=8,omeg=1.4
 b0 HO n_high=8,omeg=0.7
 a1 HO n_high=8,omeg=1.4
 b1 HO n_high=8,omeg=0.7
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Occup
 0 0 0 0 

#2 Vcc       // This a the VCC  input
#3 Name
   1vmp
#3 Vmp
 6 
#3 IoLevel
11
#3 Occup
 0 0 0 0 
#3 LimitModalBasis
 3 3 3 3 
#3 MaxExci
 4

#2 Vcc       // This a the VCC  input
#3 Name
   2vmp
#3 Vmp
 6 
#3 IoLevel
11
#3 Occup
 0 0 0 0 
#3 LimitModalBasis
 3 3 3 3 
#3 Iso
 1

#2 Vcc       // This a the VCC  input
#3 Name
   3vmp
#3 Vmp
 6 
#3 IoLevel
11
#3 Occup
 0 0 0 0 
#3 LimitModalBasis
 3 3 3 3 
#3 Iso
 2

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=10
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# FVCI energy:
CRIT01=`$GREP "O_2"  $log | grep "1.683064849057[5,6]"  | wc -l`
CRIT02=`$GREP "O_6"  $log | grep "2.0970018454684"  | wc -l`
CRIT03=`$GREP "O_13" $log | grep "2.0970018445869"  | wc -l`
# should be 6 4 2: 4 and 2 since the iso 1 is only exact to third order
#
#

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
`
CTRL[2]=12
ERROR[2]="Vmp ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02
echo $CRIT03

PASSED=1
for i in 1 2
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 4dvmp.check
#######################################################################
