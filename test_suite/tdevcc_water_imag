#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="tdevcc_water_imag"

#######################################################################
#  Input
#######################################################################
cat > ${TESTNAME}.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water 2M-coupled
   Wave Function:    TDEVCC (MatRep)
   Test Purpose:     Check imaginary time propagation gives ground state.
   Reference:
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
max_exci=2
n_modals=3
thr="1.0e-15"
ode_thr="1.0e-16"
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ${TESTNAME}.mop
#2 Basis
   #3 Name
      basis
   #3 BSplineBasis
      10
   #3 PrimBasisDensity
      0.8
   #3 TurningPoint
      10
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      basis
   #3 Oper
      h0
   #3 Threshold
      1.0e-15

#2 Vcc
   #3 Name
      extvcc_gs
   #3 UseVscf
      vscf
   #3 LimitModalBasis
      [3*${n_modals}]
   #3 MaxExci
      ${max_exci}
   #3 MatRep
      extvcc
   #3 ItEqResidThr
      ${thr}
   #3 ItEqMaxIt
      20
   #3 Crop
      3
   #3 Oper
      h0
   #3 Basis
      basis

#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      tdextvcc
   #3 CorrMethod
      extvcc
   #3 MaxExci
      ${max_exci}
   #3 Transformer
      matrep
   #3 ModalBasisFromVscf
      vscf
   #3 LimitModalBasis
      [3*${n_modals}]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 VccGs
      extvcc_gs
   #3 ImagTime
   #3 ImagTimeHaultThr
      ${thr}
   #3 PrintoutInterval
      1.e0
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e5
      #4 OutputPoints
         1001
      #4 Tolerance
         ${ode_thr} ${ode_thr}
      #4 MaxSteps
         -1
   #3 Properties
      energy
   #3 Statistics
      dnorm2vccgs

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASOPERATOR
#1SETINFO
type=energy
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
-1.3983481039758772E-11    Q^1(Q0)
 1.5756874643102492E-04    Q^2(Q0)
-1.3022827261011116E-10    Q^3(Q0)
 4.6707384626643034E-08    Q^4(Q0)
 4.5483926669476205E-07    Q^1(Q1)
 1.4937195226139011E-04    Q^2(Q1)
 3.0827675345790340E-06    Q^3(Q1)
 4.3788077164208516E-08    Q^4(Q1)
 3.7033530020380567E-08    Q^1(Q2)
 2.7465351735145305E-05    Q^2(Q2)
 1.4056156771857786E-07    Q^3(Q2)
-5.4826898576720851E-10    Q^4(Q2)
 2.2424728740588762E-10    Q^1(Q0)     Q^1(Q1)
 1.3244516594568267E-10    Q^1(Q0)     Q^2(Q1)
 1.0004441719502211E-11    Q^1(Q0)     Q^3(Q1)
 9.5331590728164883E-06    Q^2(Q0)     Q^1(Q1)
 2.7097757993033156E-07    Q^2(Q0)     Q^2(Q1)
-4.0927261579781771E-12    Q^3(Q0)     Q^1(Q1)
 1.6018475434975699E-10    Q^1(Q0)     Q^1(Q2)
 1.0061285138363019E-11    Q^1(Q0)     Q^2(Q2)
-1.2647660696529783E-12    Q^1(Q0)     Q^3(Q2)
-9.1266440449544461E-07    Q^2(Q0)     Q^1(Q2)
-5.4972815632936545E-08    Q^2(Q0)     Q^2(Q2)
-7.3896444519050419E-13    Q^3(Q0)     Q^1(Q2)
-2.8100544113840442E-10    Q^1(Q1)     Q^1(Q2)
-7.2744757062537246E-07    Q^1(Q1)     Q^2(Q2)
 9.8503392109705601E-09    Q^1(Q1)     Q^3(Q2)
-2.2016263301338768E-07    Q^2(Q1)     Q^1(Q2)
-4.5356273403740488E-08    Q^2(Q1)     Q^2(Q2)
-9.0807930064329412E-09    Q^3(Q1)     Q^1(Q2)
#0MIDASOPERATOREND
%EOF%


#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Utilities
# ------------------------------------------------------------------------------
# For asserting (absolute) deviations.
#   $1: value
#   $2: expected
#   $3: (abs) threshold
#   return: 1 if dev <= threshold, 0 otherwise.
accept_dev()
{
   awk '\''
      function abs(a){return a>0? a: -a}
      function dev(a,b){return abs(a-b)}
      function accept(a,b,thr){return dev(a,b) <= abs(thr)? 1: 0}
      BEGIN{printf("%d",accept('\''$1'\'','\''$2'\'','\''$3'\''))}
   '\''
}

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check ground state VSCF and EVCC energies
# ------------------------------------------------------------------------------
tol_E_gs="1.0e-16"
ctrl_E_gs_vscf="2.1058851687331424E-02"
ctrl_E_gs_extvcc="2.0986637774138860E-02"
E_gs_vscf=$($GREP -A1 "Vscf .* converged" $log | tail -n 1 | awk '\''{print $5}'\'')
E_gs_extvcc=$($GREP -A15 "Non-linear EVCC equations converged" $log | $GREP "Final energy .au. *=" | awk '\''{print $5}'\'')
TEST+=($(accept_dev $E_gs_vscf $ctrl_E_gs_vscf $tol_E_gs))
CTRL+=(1)
ERROR+=("Wrong VSCF energy. (Got ${E_gs_vscf}, expected ${ctrl_E_gs_vscf}, tol = ${tol_E_gs}.)")
TEST+=($(accept_dev $E_gs_extvcc $ctrl_E_gs_extvcc $tol_E_gs))
CTRL+=(1)
ERROR+=("Wrong EVCC energy. (Got ${E_gs_extvcc}, expected ${ctrl_E_gs_extvcc}, tol = ${tol_E_gs}.)")

# ------------------------------------------------------------------------------
# Check imag. time prop. energy
# ------------------------------------------------------------------------------
tol_E_compare="1.0e-16"
E_beg=$($GREP "ENERGY, at t_beg" $log | awk '\''{print $5}'\'')
E_end=$($GREP "ENERGY, at t_end" $log | awk '\''{print $5}'\'')
TEST+=($(accept_dev $E_beg $E_gs_vscf $tol_E_compare))
CTRL+=(1)
ERROR+=("E(t_beg) (= $E_beg) != E_vscf (= $E_gs_vscf). (tol = $tol_E_compare.)")
TEST+=($(accept_dev $E_end $E_gs_extvcc $tol_E_compare))
CTRL+=(1)
ERROR+=("E(t_end) (= $E_end) != E_extvcc (= $E_gs_extvcc). (tol = $tol_E_compare.)")

# ------------------------------------------------------------------------------
# Check amplitude norms
# ------------------------------------------------------------------------------
tol_norm="1.0e-15"
str_map=(tot KET BRA)

for i in $(seq 0 $(expr ${#str_map[*]} - 1))
do
   norm=$($GREP "DNORM2VCCGS .${str_map[$i]}., at t_end" $log | awk '\''{print $6}'\'')
   TEST+=($(accept_dev $norm 0.0 $tol_norm))
   CTRL+=(1)
   ERROR+=("'\''DNORM2VCCGS (${str_map[$i]}), at t_end'\'' = $norm > $tol_norm.")
done


# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
