#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         methanol
   Wave Function:    TURBOMOLE SCF/def2-SVP
   Test Purpose:     Check FreqAna with targeting. Note: Model/basis set _not_
                     suitable for quantitative results.
%EOF%

CALCNAME=freqana_target_methanol
TEMPLATEDIR=$(pwd)/generic/generic_${CALCNAME}
RUNSCRIPT=run.sh
INPUTSCRIPT=input.sh
VALIDATESCRIPT=validate.sh
MIDASIFCPROPINFO=midasifc.propinfo
MOLECULEINFO=molecule.mmol

export TMPDIR=$(pwd)/tmpouts/$CALCNAME
export SAVEDIR=$TMPDIR/savedir
export SETUPDIR=$TMPDIR/setup
export ANALYSISDIR=$TMPDIR/analysis

#######################################################################
#  Check for turbomole existence
#######################################################################
turbomole_bin=`grep "^turbomole_bin" ES_PROGRAMS | cut -d '"' -f2`

echo $turbomole_bin

export TURBODIR=$turbomole_bin
export PATH=$TURBODIR/scripts:$PATH

sysname=`sysname`

sysname

for prog in define dscf grad
do
   if test ! -x $turbomole_bin/bin/$sysname/$prog
   then
      echo turbomole_bin = $turbomole_bin
      echo sysname = `sysname`
      echo "turbomole_bin path in ES_PROGRAMS MUST be right for this test to run"
      echo "failed for executable: $prog"
   fi
done

#######################################################################
#  Directories
#######################################################################
mkdir -p $SETUPDIR

#######################################################################
#  Required files to savedir
#######################################################################
for file in $RUNSCRIPT $INPUTSCRIPT $VALIDATESCRIPT $MIDASIFCPROPINFO
do
   cp $TEMPLATEDIR/$file $SETUPDIR
done

for file in $RUNSCRIPT $INPUTSCRIPT $VALIDATESCRIPT
do
   sed -i.bk "s|__turbomole_bin_path__|$TURBODIR|g" $SETUPDIR/$file
   chmod +x $SETUPDIR/$file
done

#######################################################################
#  Molecule info
#######################################################################
cp $TEMPLATEDIR/$MOLECULEINFO ${CALCNAME}.mmol


#######################################################################
#  Midas input
#######################################################################
cat > ${CALCNAME}.minp << %EOF%
#0 MIDAS Input

#1 SINGLEPOINT
#2 Name
sp_freqana
#2 Type
SP_GENERIC
#2 InputcreatorScript
${INPUTSCRIPT}
#2 RunScript
${RUNSCRIPT}
#2 ValidationScript
${VALIDATESCRIPT}
#2 SETUPDIR
${SETUPDIR}

#1System
#2SysDef
#3Moleculefile
Midas
Molecule.mmol
#3Name
molecule
#2 ModSys
#3Sysnames
1
molecule
#3ModVibCoord
FreqAna
#4 DispFactor
1e-3
#4 SinglePointName
sp_freqana
#4 NumNodes
1
#4 ProcPerNode
4
#4 DumpInterval
4
#4 RotationThr
1e-6
#4 Targeting
#4 ItEqEnerThr
1
#4 ItEqResThr
1.0e-4
#4 ItEqBreakDim
1000
#4 ItEqMaxIt
1000
#4 OverlapSumMin
.8
#4 OverlapNMax
5

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1
dir=$MAINDIR/savedir/

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")


# ------------------------------------------------------------------------------
# Targeting setup correct?
# ------------------------------------------------------------------------------
CRIT=$($GREP "Dimension of space[[:space:]]*:[[:space:]]*\<18\>" $log | wc -l)
TEST+=($(expr $CRIT))
CTRL+=(1)
ERROR+=("Wrong dimension of space.")

CRIT=$($GREP "Number of equations[[:space:]]*:[[:space:]]*\<1\>" $log | wc -l)
TEST+=($(expr $CRIT))
CTRL+=(1)
ERROR+=("Wrong number of equations.")

# ------------------------------------------------------------------------------
# Targeting results.
# ------------------------------------------------------------------------------
# Conversion from eigenvalue to frequency (wavenumber):
# The Hessian has units of Hartree/(a.m.u. * bohr) - i.e. atomic units _except_
# for the a.m.u. instead of electron mass. Thus divide by amu. to m_e ratio to
# get eigenvalues in atomic units. Then squareroot for the frequency in atomic
# units, then convert to cm^-1. If needed...
# Example:
#  eig = 6.6532764588655779e-01
#  nu_tilde[cm^-1] = sqrt( eig / (1822.888486192 m_e amu^-1)) * 219474.63 cm^-1 a.u.^-1
#                  = 4192.9729388109945 cm^-1
#
# Targeting procedure
#     i  n_overlap               overlap_sum
#     0          1    9.9971350039907536e-01
# Eq. 0   : eig = 6.6532764588655779e-01       |r| = 3.1079695134327695e-05       |eig-eig_old|/|eig| = 1.4418128483280063e-07     C
#  On exit the reduced space is : 4

CRIT=$($GREP -A1 "n_overlap[[:space:]]*overlap_sum" $log | tail -n1 | $GREP "\<0\>.*\<1\>.*\<9\.997135[[:digit:]]*[eE]-01\>" | wc -l)
TEST+=($(expr $CRIT))
CTRL+=(1)
ERROR+=("Wrong n_overlap and overlap_sum.")

CRIT=$($GREP "Eq\. 0.*eig =" $log | tail -n1 | $GREP "6.653276[[:digit:]]*[eE]-01" | wc -l)
TEST+=($(expr $CRIT))
CTRL+=(1)
ERROR+=("Wrong eigenvalue.")

CRIT=$($GREP "Eq\. 0.*.r. =" $log | tail -n1 | $GREP "3.10[78][[:digit:]]*[eE]-05" | wc -l)
TEST+=($(expr $CRIT))
CTRL+=(1)
ERROR+=("Wrong residual.")

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > ${CALCNAME}.check
#######################################################################
