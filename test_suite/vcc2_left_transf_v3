#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         H2O as in ir_h2o_vci. test 
   Wave Function:    ??? 
   Test Purpose:     Check VCC[2] response properties using V3 transformer.
   Additional:       Properties are calculated using Dalton, DFT/B3LYP
                     and the aug-cc-pVTZ basis.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc2_left_transf_v3.minp <<%EOF%

#0 MIDAS Input 

// General input 

#1 general
#2 IoLevel
    5
#2 Time

// Vibrational input
#1 Vib

// Operator input
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ir_h2o_vci_h0.mop
   #3 KineticEnergy
      Simple

// Dipole operators
#2 Operator    // This is operator input
   #3 Name
       x_dipole
   #3 OperFile
       ir_h2o_vci_x.mop
   #3 SetInfo
       type=x_dipole
   #3 KineticEnergy
      None
#2 Operator    // This is operator input
   #3 Name
       y_dipole
   #3 OperFile
       ir_h2o_vci_y.mop
   #3 SetInfo
       type=y_dipole
   #3 KineticEnergy
      None
#2 Operator    // This is operator input
   #3 Name
       z_dipole
   #3 OperFile
       ir_h2o_vci_z.mop
   #3 SetInfo
       type=z_dipole
   #3 KineticEnergy
      None

#2 Basis     // This is basis input
   #3 Name
   basis_gauss
   #3 PrimBasisDensity
   0.8
   #3 GaussianBasis
   0.9
   #3 IoLevel
   6
   #3ScalBounds
   1.5 40

#2 Vscf      // This a the VSCF input
    #3 Restart
    #3 IoLevel
        1
    #3 Name 
        scf0
    #3 Oper
        h0
    #3 Basis
        basis_gauss
    #3 OccGroundState
    #3 Threshold
        1.0e-15
    #3 MaxIter
        40

#2 Vcc
    #3 Restart
    #3 Vcc
    #3 Transformer
      trf=v3
    #3 VscfThreshold
      1.0e-15
    #3 LimitModalBasis
      [3*3]
    #3 MaxExci
      2
    #3 ItEqResidThr
      1.0e-15
    #3 Oper
      h0
    #3 Basis 
      basis_gauss 
    #3 VecStorage 
      0
    #3 ItEqMaxDim
      200
    #3 ItEqMaxIt
      150
    #3 ItEqBreakDim
      200
    #3 ItEqMaxItMicro 
      150
    #3 ItEqMicroThrFac 
      0.0001
    #3 TimeIt
    #3 Rsp
        #4 TimeIt
        #4 DiagMeth
         LAPACK
        #4 IoLevel
            3
        #4 Eigen val
            11
        #4 ItEqResidThr
            1.0e-15
        #4 LambdaThreshold
            1.0e-15
        #4 ItEqMaxIt
            100
        #4 ItEqMaxDim
            200
        #4 ItEqBreakDim
            150
        #4 RspFunc
             1 h0
             1 x_dipole
             1 x_dipole NO2NP2
             1 y_dipole
             1 y_dipole NO2NP2
             1 z_dipole
             1 z_dipole NO2NP2

#0 Midas Input End
%EOF%

# Now we define all the operators...

cat > ir_h2o_vci_h0.mop <<%EOF%
DALTON_FOR_MIDAS
-1.3983481039758772000000E-11    1     
 1.5756874643102492000000E-04    1     1     
-1.3022827261011116000000E-10    1     1     1     
 4.6707384626643034000000E-08    1     1     1     1     
 4.5483926669476205000000E-07    2     
 1.4937195226139011000000E-04    2     2     
 3.0827675345790340000000E-06    2     2     2     
 4.3788077164208516000000E-08    2     2     2     2     
 3.7033530020380567000000E-08    3     
 2.7465351735145305000000E-05    3     3     
 1.4056156771857786000000E-07    3     3     3     
-5.4826898576720851000000E-10    3     3     3     3     
 2.2424728740588762000000E-10    1     2     
 1.3244516594568267000000E-10    1     2     2     
 1.0004441719502211000000E-11    1     2     2     2     
 9.5331590728164883000000E-06    1     1     2     
 2.7097757993033156000000E-07    1     1     2     2     
-4.0927261579781771000000E-12    1     1     1     2     
 1.6018475434975699000000E-10    1     3     
 1.0061285138363019000000E-11    1     3     3     
-1.2647660696529783000000E-12    1     3     3     3     
-9.1266440449544461000000E-07    1     1     3     
-5.4972815632936545000000E-08    1     1     3     3     
-7.3896444519050419000000E-13    1     1     1     3     
-2.8100544113840442000000E-10    2     3     
-7.2744757062537246000000E-07    2     3     3     
 9.8503392109705601000000E-09    2     3     3     3     
-2.2016263301338768000000E-07    2     2     3     
-4.5356273403740488000000E-08    2     2     3     3     
-9.0807930064329412000000E-09    2     2     2     3     
%EOF%

cat > ir_h2o_vci_x.mop <<%EOF%
DALTON_FOR_MIDAS
 5.9466261933083017000000E-03    1     
-6.9084653969708754000000E-10    1     1     
 1.6358854797345057000000E-07    1     1     1     
-3.8139283398130885000000E-12    1     1     1     1     
 8.1519183905138090000000E-08    2     
 7.6635677157709771000000E-10    2     2     
-1.1814304487513459000000E-11    2     2     2     
 1.6793121081384116000000E-12    2     2     2     2     
 3.6421274585127040000000E-09    3     
-7.4582181456856300000000E-11    3     3     
 9.6729966832230374000000E-13    3     3     3     
-6.4918922320864864000000E-13    3     3     3     3     
 5.5929864454224050000000E-05    1     2     
-9.6813214170085593000000E-07    1     2     2     
-1.2677817016679138000000E-08    1     2     2     2     
 7.2171140591548166000000E-11    1     1     2     
-8.8380136542554055000000E-12    1     1     2     2     
-9.2691439454450908000000E-09    1     1     1     2     
-1.2251871231805595000000E-04    1     3     
 1.1458751519493118000000E-06    1     3     3     
 3.6132391740065217000000E-10    1     3     3     3     
 1.2171142566019810000000E-11    1     1     3     
-9.7285929312462827000000E-12    1     1     3     3     
 7.7715807799555447000000E-09    1     1     1     3     
-1.6425522658677930000000E-09    2     3     
 1.5017041558612033000000E-11    2     3     3     
-1.3801961458477968000000E-12    2     3     3     3     
-9.8233102244450509000000E-12    2     2     3     
 1.3196157239395726000000E-13    2     2     3     3     
-1.6974224867611291000000E-12    2     2     2     3     
%EOF%

cat > ir_h2o_vci_y.mop <<%EOF%
DALTON_FOR_MIDAS
 8.8094570600241383000000E-15    1     
 2.0579515215545181000000E-14    1     1     
-3.9730892762765628000000E-14    1     1     1     
-6.7385565528687044000000E-14    1     1     1     1     
 3.1999408614138801000000E-16    2     
 3.8403586889375180000000E-14    2     2     
-3.2574694064581650000000E-15    2     2     2     
-1.0560113345207904000000E-13    2     2     2     2     
-1.5814514173919275000000E-15    3     
 1.7735807639239755000000E-14    3     3     
 2.2478608095957593000000E-15    3     3     3     
-1.8324910770300230000000E-14    3     3     3     3     
 2.1536863258180361000000E-15    1     2     
-7.3421903241223336000000E-14    1     2     2     
 1.1465895712826050000000E-14    1     2     2     2     
-1.4158683383691875000000E-14    1     1     2     
-4.6517856051405650000000E-13    1     1     2     2     
 2.0717942875917686000000E-14    1     1     1     2     
 2.2390977737264117000000E-15    1     3     
-5.0789583579486514000000E-14    1     3     3     
-2.4611069414207930000000E-15    1     3     3     3     
 2.2172343451965871000000E-14    1     1     3     
-2.3422196280492047000000E-13    1     1     3     3     
-5.5815443638190166000000E-14    1     1     1     3     
 2.0483972203442721000000E-14    2     3     
 3.0939562954175324000000E-14    2     3     3     
-3.2169454769875466000000E-14    2     3     3     3     
 1.3051627728937089000000E-14    2     2     3     
-1.2593085685454386000000E-13    2     2     3     3     
-9.8224815858746958000000E-14    2     2     2     3     
%EOF%

cat > ir_h2o_vci_z.mop <<%EOF%
DALTON_FOR_MIDAS
 1.8027356940564232000000E-08    1     
 1.6368545080247543000000E-05    1     1     
-5.4192206278003141000000E-12    1     1     1     
 2.8840849708444694000000E-09    1     1     1     1     
-1.6066597706836383000000E-03    2     
-2.4899345155482422000000E-05    2     2     
 1.6070990582761624000000E-07    2     2     2     
 6.6033383205876817000000E-09    2     2     2     2     
 6.5273750308395551000000E-03    3     
-3.3700807905656660000000E-05    3     3     
 2.6208677517125523000000E-07    3     3     3     
-1.0468585998069102000000E-09    3     3     3     3     
 1.1107390562870023000000E-09    1     2     
 1.3325340830760979000000E-11    1     2     2     
 2.4709123636057484000000E-12    1     2     2     2     
 5.3277693101705381000000E-07    1     1     2     
 1.5315734458454244000000E-08    1     1     2     2     
 2.1556090246122039000000E-12    1     1     1     2     
-3.9093883685836772000000E-10    1     3     
 6.1719518384961702000000E-12    1     3     3     
-5.3135273958559992000000E-13    1     3     3     3     
-3.8460020412856011000000E-07    1     1     3     
 3.0578206633435911000000E-10    1     1     3     3     
 1.0329515021112456000000E-12    1     1     1     3     
 3.3004144652126755000000E-05    2     3     
-4.5190854036647465000000E-07    2     3     3     
 1.4223994870832257000000E-08    2     3     3     3     
 5.2844697506770899000000E-07    2     2     3     
-9.6636032509422876000000E-09    2     2     3     3     
-4.1491672320148609000000E-09    2     2     2     3     
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep "
fi


# DIPOLE EXPECTATION VALUES
CRIT0=`$GREP "Order: 1  <x_dipole> =" $log | $GREP -v "No" | $GREP "\-1.9092529[0-9]" | wc -l`
CRIT1=`$GREP "Order: 1  <y_dipole> =" $log | $GREP -v "No" | $GREP "\-1.998839[0-9]" | wc -l` 
CRIT2=`$GREP "Order: 1  <z_dipole> =" $log | $GREP -v "No" | $GREP "4.5996960296" | wc -l`

TEST[3]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2`
CTRL[3]=3
ERROR[3]="EXPECTATION VALUES NOT CORRECT 1"
echo $CRIT0
echo $CRIT1
echo $CRIT2

# DIPOLE EXPECTATION VALUES: NO2NP2
CRIT40=`$GREP "Order: 1  <x_dipole> =" $log | $GREP  "No" | $GREP "\-1.9092529[0-9]" | wc -l`
CRIT41=`$GREP "Order: 1  <y_dipole> =" $log | $GREP  "No" | $GREP "\-1.998839[0-9]" | wc -l` 
CRIT42=`$GREP "Order: 1  <z_dipole> =" $log | $GREP  "No" | $GREP "4.5996960296" | wc -l`

TEST[4]=`expr $CRIT40 \+ $CRIT41 \+ $CRIT42`
CTRL[4]=3
ERROR[4]="EXPECTATION VALUES NOT CORRECT 2"
echo $CRIT40
echo $CRIT41
echo $CRIT42


PASSED=1
for i in 3 4 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}.
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi
' > vcc2_left_transf_v3.check
#######################################################################
