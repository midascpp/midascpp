#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Formaldehyde test 
   Wave Function:    VSCF/large basis, all sum_n <= 2 state, adapted HO basis 
   Test Purpose:     Check VSCF energy of lowest states 
   Reference:        Romanowski, Bowman & Harding, JCP 82, 4155 (1985)
                     Reproduces all energies (perhaps 2 places with an acceptable 
                     deviation of 1 on last digit) of paper.
                     (Despite unknown if pes includes coriolis or not).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > formaldehyd_rsp2.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
    5 
#2 Time
!#2 Debug 


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  5

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3 OperFile
formaldehyd_rsp2.op
#3operinput
#4operatorterms
-0.5 DDQ^2(1)
-0.5 DDQ^2(2)
-0.5 DDQ^2(3)
-0.5 DDQ^2(4)
-0.5 DDQ^2(5)
-0.5 DDQ^2(6)
#3 KineticEnergy
 USER_DEFINED

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 x
#3operinput
#4operatorterms
1.0 Q^0(1) Q^2(2) Q^0(3) Q^0(4) Q^0(5) Q^0(6) 

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 y
#3operinput
#4operatorterms
1.0 Q^2(1) Q^0(2) Q^0(3) Q^0(4) Q^0(5) Q^0(6) 

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 z
#3operinput
#4operatorterms
1.0 Q^2(1) Q^0(2) Q^0(3) Q^0(4) Q^0(5) Q^0(6) 
1.0 Q^0(1) Q^2(2) Q^0(3) Q^0(4) Q^0(5) Q^0(6) 

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
!1 HO n_high=24,omeg=2781.0 cm-1  // Differ just to make it difficult...
!2 HO n_high=24,omeg=1747.0 cm-1
!3 HO n_high=24,omeg=1500.0 cm-1
!4 HO n_high=24,omeg=1160.0 cm-1
!5 HO n_high=24,omeg=2844.0 cm-1 
!6 HO n_high=24,omeg=1245.0 cm-1
1 HO n_high=20,omeg=2781.0 cm-1  // Differ just to make it difficult...
2 HO n_high=24,omeg=1747.0 cm-1
3 HO n_high=25,omeg=1500.0 cm-1
4 HO n_high=30,omeg=1160.0 cm-1
5 HO n_high=20,omeg=2844.0 cm-1 
6 HO n_high=24,omeg=1245.0 cm-1
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Name 
scf00
#3 Oper
testoper
#3 Basis
basis
#3 Occup
 0 0 0 0 0 0 
!#3 OccuMax
! 2 2 2 2 2 2 
!#3 OccMaxSumN 
! 2
!#3 OccMaxExci
! 6
#3 Threshold
 1.0e-12
#3 MaxIter
 20
!#3 OnDisc
#3 Rsp
#4 DiagMeth 
  LAPACK
#4 Eigen val
 5  
#4 ItEqResidThr
 1.0e-10
#4 ItEqMaxIt
 30
#4 ItEqMaxDim
 140
#4 ItEqBreakDim
 30
!#4 VcisAsVscf 
#4 RspFunc
 1 testoper
 2 testoper testoper 0.0
 1 x
 1 y
 1 z
 2 x x 0.0
 2 y y 0.0
 2 x y 0.0
 2 y x 0.0
 2 z z 0.0
 2 x x 0.1
 2 y y 0.1
 2 x y 0.1
 2 y x 0.1
 2 z z 0.1

/*

#2 Vcc
#3 IoLevel
11
#3 Vci
 5  
#3 MaxExci
  1 
#3 ItEqResidThr
 1.0e-10
*/



/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

cat > formaldehyd_rsp2.op <<%EOF%
GEN_VSCF_95 N_PROD = 6
  1 0 0 0 0 0 2 0 0 0 0 0  8.95641e-05
  2 0 0 0 0 0 2 0 0 0 0 0  3.28072e-05
  3 0 0 0 0 0 2 0 0 0 0 0  2.47445e-05
  4 0 0 0 0 0 2 0 0 0 0 0  1.46576e-05
  5 0 0 0 0 0 2 0 0 0 0 0  9.41724e-05
  6 0 0 0 0 0 2 0 0 0 0 0  1.67271e-05
  1 0 0 0 0 0 3 0 0 0 0 0  1.58395e-06
  2 0 0 0 0 0 3 0 0 0 0 0 -3.09488e-07
  3 0 0 0 0 0 3 0 0 0 0 0 -7.42473e-09
  1 0 0 0 0 0 4 0 0 0 0 0  1.64404e-08
  2 0 0 0 0 0 4 0 0 0 0 0  2.09690e-09
  3 0 0 0 0 0 4 0 0 0 0 0  3.01839e-11
  4 0 0 0 0 0 4 0 0 0 0 0  7.72218e-10
  5 0 0 0 0 0 4 0 0 0 0 0  2.03858e-08
  6 0 0 0 0 0 4 0 0 0 0 0  6.16225e-10
  1 2 0 0 0 0 2 1 0 0 0 0  6.26340e-08
  1 3 0 0 0 0 2 1 0 0 0 0  1.64127e-08
  1 2 0 0 0 0 1 2 0 0 0 0 -1.55531e-07
  1 2 3 0 0 0 1 1 1 0 0 0 -2.18685e-07
  1 3 0 0 0 0 1 2 0 0 0 0 -1.40708e-07
  1 4 0 0 0 0 1 2 0 0 0 0 -4.76302e-07
  1 5 0 0 0 0 1 2 0 0 0 0  5.14105e-06
  1 5 6 0 0 0 1 1 1 0 0 0  2.37125e-08
  1 6 0 0 0 0 1 2 0 0 0 0 -3.61882e-07
  2 3 0 0 0 0 2 1 0 0 0 0  2.13373e-07
  2 3 0 0 0 0 1 2 0 0 0 0 -1.82567e-07
  2 4 0 0 0 0 1 2 0 0 0 0 -6.24048e-08
  2 5 0 0 0 0 1 2 0 0 0 0  3.54497e-07
  2 5 6 0 0 0 1 1 1 0 0 0 -5.32615e-07
  2 6 0 0 0 0 1 2 0 0 0 0 -2.62944e-08
  3 4 0 0 0 0 1 2 0 0 0 0 -6.14774e-08
  3 5 0 0 0 0 1 2 0 0 0 0  3.27825e-07
  3 5 6 0 0 0 1 1 1 0 0 0 -6.15477e-07
  3 6 0 0 0 0 1 2 0 0 0 0  1.24274e-07
  1 2 0 0 0 0 3 1 0 0 0 0  1.75202e-09
  1 3 0 0 0 0 3 1 0 0 0 0  4.35259e-10
  1 2 0 0 0 0 2 2 0 0 0 0 -5.11727e-09
  1 2 3 0 0 0 2 1 1 0 0 0 -1.84464e-08
  1 3 0 0 0 0 2 2 0 0 0 0 -1.54362e-08
  1 4 0 0 0 0 2 2 0 0 0 0 -2.35793e-08
  1 5 0 0 0 0 2 2 0 0 0 0  1.13248e-07
  1 5 6 0 0 0 2 1 1 0 0 0  9.64337e-10
  1 6 0 0 0 0 2 2 0 0 0 0 -1.84492e-08
  1 2 0 0 0 0 1 3 0 0 0 0  6.06809e-10
  1 2 3 0 0 0 1 2 1 0 0 0 -3.49433e-09
  1 2 3 0 0 0 1 1 2 0 0 0 -3.41881e-09
  1 2 4 0 0 0 1 1 2 0 0 0 -3.99302e-09
  1 2 5 0 0 0 1 1 2 0 0 0  1.23419e-08
  1 2 5 6 0 0 1 1 1 1 0 0 -4.33096e-08
  1 2 6 0 0 0 1 1 2 0 0 0 -1.98986e-09
  1 3 0 0 0 0 1 3 0 0 0 0 -8.19983e-10
  1 3 4 0 0 0 1 1 2 0 0 0 -3.47055e-09
  1 3 5 0 0 0 1 1 2 0 0 0  1.24585e-08
  1 3 5 6 0 0 1 1 1 1 0 0 -7.06454e-08
  1 3 6 0 0 0 1 1 2 0 0 0  2.71959e-09
  2 3 0 0 0 0 3 1 0 0 0 0 -2.29151e-09
  2 3 0 0 0 0 2 2 0 0 0 0  1.57200e-09
  2 4 0 0 0 0 2 2 0 0 0 0 -5.30463e-10
  2 5 0 0 0 0 2 2 0 0 0 0 -8.45341e-09
  2 5 6 0 0 0 2 1 1 0 0 0 -1.88770e-09
  2 6 0 0 0 0 2 2 0 0 0 0  1.00897e-10
  2 3 0 0 0 0 1 3 0 0 0 0  5.79834e-11
  2 3 4 0 0 0 1 1 2 0 0 0  8.16788e-10
  2 3 5 0 0 0 1 1 2 0 0 0 -2.49993e-08
  2 3 5 6 0 0 1 1 1 1 0 0 -8.88722e-09
  2 3 6 0 0 0 1 1 2 0 0 0  1.92595e-09
  3 4 0 0 0 0 2 2 0 0 0 0 -2.82284e-10
  3 5 0 0 0 0 2 2 0 0 0 0 -2.08768e-08
  3 5 6 0 0 0 2 1 1 0 0 0 -2.63248e-09
  3 6 0 0 0 0 2 2 0 0 0 0  1.54438e-09
  4 5 0 0 0 0 2 2 0 0 0 0 -2.78328e-08
  4 5 6 0 0 0 2 1 1 0 0 0 -3.41463e-10
  4 6 0 0 0 0 2 2 0 0 0 0  1.43632e-09
  5 6 0 0 0 0 3 1 0 0 0 0  4.41000e-09
  5 6 0 0 0 0 2 2 0 0 0 0 -2.01654e-08
  5 6 0 0 0 0 1 3 0 0 0 0  7.36786e-10
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=24,omeg=" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=8
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT11=`$GREP "E =  5.7962857892" $log | wc -l`
TEST[2]=`expr \
$CRIT11 \
`
CTRL[2]=1
ERROR[2]="VSCF ENERGY NOT CORRECT"

echo $CRIT1 
echo $CRIT2  
echo $CRIT3  
echo $CRIT11 

#
CRIT31=`$GREP "x" $log | $GREP "6.3372424159[5,6]" | wc -l`
CRIT32=`$GREP "y" $log | $GREP "4.12117760605[0-9]" | wc -l`
CRIT33=`$GREP "z" $log | $GREP "1.04584200220[0-9]" | wc -l`
CRIT34=`$GREP "<<x;x>>" $log | $GREP "1.09903535097[0-9]" | wc -l`
CRIT35=`$GREP "<<x;y>>" $log | $GREP "3.21442215[0-9]" | wc -l`
CRIT36=`$GREP "<<y;x>>" $log | $GREP "3.21442215[0-9]" | wc -l`
CRIT37=`$GREP "<<y;y>>" $log | $GREP "3.404183985[2,3][0-9]" | wc -l`
CRIT38=`$GREP "<<z;z>>" $log | $GREP "1.433024905[1,2][0-9]" | wc -l`
CRIT39=`$GREP "<<x;x>>" $log | $GREP "2.6009205519[0-9]" | wc -l`
CRIT40=`$GREP "<<x;y>>" $log | $GREP "9.0899845[1,2]" | wc -l`
CRIT41=`$GREP "<<y;x>>" $log | $GREP "9.0899845[1,2]" | wc -l`
CRIT42=`$GREP "<<y;y>>" $log | $GREP "1.759852845[4,5]" | wc -l`
CRIT43=`$GREP "<<z;z>>" $log | $GREP "4.36059159777[0-9]" | wc -l`
TEST[3]=`expr $CRIT31 \+ $CRIT32 \+ $CRIT33 \+ $CRIT34 \+ $CRIT35 \+ $CRIT36 \+ $CRIT37 \+ $CRIT38 \+ $CRIT39 \+ $CRIT40 \+ $CRIT41 \+ $CRIT42 \+ $CRIT43 `
CTRL[3]=13
ERROR[3]="VSCF RESPONSE FUNCTIONS NOT CORRECT"
echo $CRIT31 
echo $CRIT32 
echo $CRIT33 
echo $CRIT34 
echo $CRIT35 
echo $CRIT36 
echo $CRIT37 
echo $CRIT38 
echo $CRIT39 
echo $CRIT40 
echo $CRIT41 
echo $CRIT42 
echo $CRIT43 


PASSED=1
for i in 1 2 3
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > formaldehyd_rsp2.check
#######################################################################
