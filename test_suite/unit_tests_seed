#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > unit_tests_seed.info <<%EOF%
   Test Purpose:  Check that all unit tests runs without problems
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > unit_tests_seed.minp <<%EOF%
#0 Midas Input

#1 General
#2 Seed
1 2 3 4

#1 Test

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Get number of enabled tests from "Test flags" input summary
# ------------------------------------------------------------------------------
# First find the lines for the beginning and end of the "Test flags" section.
test_flags_beg=$($GREP -n "Test flags" $log | awk -F: '\''{print $1}'\'' | head -n 1)
test_flags_end=$($GREP -n "\+={70}\+" $log | awk -F: '\''($1 > '\''$test_flags_beg'\''){print $1}'\'' | head -n 1)
# Print only those lines, using sed, then pipe to grep for stuff.
# sed output should be something like this:
#     | Test flags                                                           |
#     |----------------------------------------------------------------------|
#     | Test Io Level                    | 2                                 |
#     | basetensor                       | <true/false>                      |
#     | bsplineplotter                   | <true/false>                      |
#     | calccode                         | <true/false>                      |
#     ...
#     +======================================================================+
#
# Get number of enabled tests for later validation.
n_tests=$(sed -n "${test_flags_beg},${test_flags_end}p" $log | $GREP -c "\<true\>")

# ------------------------------------------------------------------------------
#  ALL SUCCEEDED ASSERTION
# ------------------------------------------------------------------------------
# Did we get expected number of "SUCCESS"s?
TEST+=($($GREP -c "^ *SUCCESS *$" $log))
CTRL+=($n_tests)
ERROR+=("A UNIT TEST DID NOT SUCCEED.")

# ------------------------------------------------------------------------------
#  NO FAILS ASSERTION
# ------------------------------------------------------------------------------
# Did even a single test fail?
n_fails=$($GREP -c "UNIT TEST FAILED!" $log)
TEST+=($n_fails)
CTRL+=(0)
ERROR+=("A UNIT TEST HAS FAILED.")

# In case of failure, find out which tests failed.
if test $n_fails -gt 0
then
   lines_failed=($($GREP -n "UNIT TEST FAILED!" $log | awk -F: '\''{print $1}'\''))
   echo "FAILED TESTS:"
   for l_fail in ${lines_failed[@]}
   do
      sed -n "1,${l_fail}p" $log | $GREP "^ *NAME:" | tail -n 1 | sed "s/NAME: //"
   done
   echo ""
fi

# ------------------------------------------------------------------------------
# Final checks
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi
' > unit_tests_seed.check
#######################################################################

