#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    VSCF / 8 8 HO basis / VCC[2]   
   Test Purpose:     Check VCC[2] response properties 
   Reference:        Operator: Thompson & Truhlar, CPL 75, 87 (1980)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dho_vccrsp.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
   5
//#2 BufSIZE
 //5
//#2 FileSize
 //8
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  5

#2 Operator
#3 IoLevel
14
#3 Name
 u
#3operinput
#4operatorterms
1.0 Q^3(a) Q^0(b)

#2 Operator
#3 IoLevel
14
#3 Name
 y
#3operinput
#4operatorterms
0.5 Q^3(a) Q^0(b)

#2 Operator
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
0.98 Q^2(a)
0.245 Q^2(b)
0.0064 Q^3(a)
-0.08 Q^1(a) Q^2(b)
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 basis
#3 Define
 a HO n_high=8,omeg=1.4
 b HO n_high=8,omeg=0.7
#3 IoLevel
   9


#2 Vscf      // This a the VSCF input
#3 IoLevel
11 
#3 Occup
 0 0 
#3 Oper
 testoper 
#3 Threshold
 1.0e-15
#3 MaxIter
 20

#2 Vcc      
#3 UseOldVccSolver
#3 UseAllVscfModals
#3 IoLevel
 1
#3 Occup
 0 0 
#3 Oper
 testoper 
#3 VscfThreshold
 1.0e-14
#3 VscfMaxIter
 10
#3 Vcc 
#3 MaxExci
 2
#3 Transformer
  trf=v3 type=gen fileprefix=tst_
#3 ItEqResidThr
 1.0e-12
#3 DISABLECHECKMETHOD
#3 Rsp       // This a the VSCF input
#4 IoLevel
 4
#4 Eigen val
 5
#4 ItEqResidThr
 1.0e-12
#4 ItEqMaxIt
 80
#4 ItEqEnerThr
 1.0e-12
#4 ItEqMaxDim
 264
#4 DiagMeth
  LAPACK
#4 RspFunc
 1 testoper 
 1 testoper NO2NP2
 1 y
 1 y NO2NP2 
 1 u
 1 u NO2NP2 
-1 testoper Forstates [0..15] 
-1 u Forstates [0..15]
-1 u NOM Forstates [0..15] 
// DOES NOT WORK YET FOR VCC 
//-11 u ForLeftStates [0..4] ForRightStates [0..4]
//-11 y ForLeftStates [0..4] ForRightStates [0..4]
 2 u u 0.0
 2 testoper testoper 0.0
 2 y y 0.0
 2 testoper testoper 0.0
 2 u u 0.0
 2 u u 0.1
 2 u u -0.1

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.


%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=11
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4

# SCF energy:
CRIT1=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0493492614924[0-9][0-9]" | wc -l`
CRIT2=`$GREP "Occ: \[0,1\]" $log | $GREP "E =  1\.7429904052909[0-9][0-9]" | wc -l`
CRIT3=`$GREP "Occ: \[0,2\]" $log | $GREP "E =  2\.4296836007176[0-9][0-9]" | wc -l`
CRIT4=`$GREP "Occ: \[1,0\]" $log | $GREP "E =  2\.4496704954869[0-9][0-9]" | wc -l`
CRIT5=`$GREP "Occ: \[0,3\]" $log | $GREP "E =  3\.1092154093604[0-9][0-9]" | wc -l`
CRIT6=`$GREP "Occ: \[1,1\]" $log | $GREP "E =  3\.1441249282984[0-9][0-9]" | wc -l`
CRIT7=`$GREP "Occ: \[0,4\]" $log | $GREP "E =  3\.7813535099924[0-9][0-9]" | wc -l`
CRIT8=`$GREP "Occ: \[1,2\]" $log | $GREP "E =  3\.8316468350879[0-9][0-9]" | wc -l`
CRIT9=`$GREP "Occ: \[2,0\]" $log | $GREP "E =  3\.8499113594583[0-9][0-9]" | wc -l`
CRIT10=`$GREP "Occ: \[0,5\]" $log | $GREP "E =  4\.4459300371806[0-9][0-9]" | wc -l`
CRIT11=`$GREP "Occ: \[1,3\]" $log | $GREP "E =  4\.5120240417778[0-9][0-9]" | wc -l`
CRIT12=`$GREP "Occ: \[2,1\]" $log | $GREP "E =  4\.5451782900198[0-9][0-9]" | wc -l`
CRIT13=`$GREP "Occ: \[1,4\]" $log | $GREP "E =  5\.1850256725133[0-9][0-9]" | wc -l`
CRIT14=`$GREP "Occ: \[2,2\]" $log | $GREP "E =  5\.2335280439966[0-9][0-9]" | wc -l`
CRIT15=`$GREP "Occ: \[2,3\]" $log | $GREP "E =  5\.9147497010704[0-9][0-9]" | wc -l`


TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13 \+ $CRIT14 \+ $CRIT15`
CTRL[2]=1 
ERROR[2]="VSCF ENERGIES NOT CORRECT"


CRIT49=`$GREP "<u>"       $log | $GREP "2.707111816683[0-9]" | wc -l`
CRIT50=`$GREP "<<u;u>>\(" $log | $GREP "7.24193216125[0-9]" | wc -l`
CRIT51=`$GREP "<<u;u>>\(" $log | $GREP "7.27335778938[0-9]" | wc -l`
CRIT52=`$GREP "<<y;y>>\(" $log | $GREP "1.81048304031[0-9]" | wc -l`
CRIT53=`$GREP "<y>"       $log | $GREP "1.35355590834[0-9]" | wc -l`

TEST[3]=`expr $CRIT49 \+ $CRIT50 \+ $CRIT51 \+ $CRIT52 \+ $CRIT53`
CTRL[3]=9
ERROR[3]="VCC[2] G-X RESPONSE FUNCTIONS NOT CORRECT"
echo $CRIT49
echo $CRIT50
echo $CRIT51
echo $CRIT52
echo $CRIT53

#Order: -1  <0|u|1> = 4.0664897441710546E-01
#Order: -1  <0|u|2> = 4.9790239509122508E-01
CRIT61=`$GREP "<0|u" $log | $GREP "4.0664897441[0-9]" | wc -l`
CRIT62=`$GREP "<0|u" $log | $GREP "4.9790239509[0-9]" | wc -l`
TEST[3]=`expr $CRIT61 \+ $CRIT62`
CTRL[3]=4
ERROR[3]="VCC[2] G-X RESPONSE TRANSITION PROPERTIES NOT CORRECT"
echo $CRIT61
echo $CRIT62 

#CRIT62=`$GREP "<0|u|4>" $log | $GREP "\-11 " |  $GREP "4.9938482886[0-9]" | wc -l`
#CRIT63=`$GREP "<0|y|4>" $log | $GREP "\-11 " |  $GREP "2.4969241443[0-9]" | wc -l`
#TEST[7]=`expr $CRIT62 \+ $CRIT63`
#CTRL[7]=2
#ERROR[7]="VCC[2] X-X RESPONSE TRANSITION PROPERTIES NOT CORRECT"
#echo $CRIT62
#echo $CRIT63 

PASSED=1
#for i in 1 2 3 7 
for i in 1 2 3 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dho_vccrsp.check
#######################################################################
