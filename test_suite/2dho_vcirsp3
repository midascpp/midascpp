#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    VSCF / 3 3 HO basis / VCI[2]   
   Test Purpose:     Check VCI[2] response properties 
   Reference:        Operator: Thompson & Truhlar, CPL 75, 87 (1980)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dho_vcirsp3.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
   5
#2 BufSIZE
 5
#2 FileSize
 8
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  11

#2 Operator    // This is operator input
#3 Name
 u
#3 Product term
 1.0
 a Q^3
 b Q^0
! a Q^1 
! b Q^2

#2 Operator    // This is operator input
#3 Name
 y
#3 Product term
 0.5
 a Q^3
 b Q^0
! a Q^1 
! b Q^2


#2 Operator    // This is operator input
#3 Name
 testoper
#3 Product term
 -0.5 
 a DDQ^2 
#3 Product term
 -0.5 
 b DDQ^2 
#3 Product term
 0.98
 a Q^2 
#3 Product term
 0.245
 b Q^2 
#3 Product term
 0.0064
 a Q^3 
#3 Product term
 -0.08
 a Q^1 
 b Q^2 
!#3 Product term
! 0.0e-06
! a Q^3
! b Q^0

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a HO n_high=3,omeg=1.4
 b HO n_high=3,omeg=0.7
#3 IoLevel
   1

#2 Vscf      // This a the VSCF input
#3 IoLevel
 6 
#3 Occup
 0 0 
!#3 OccuMax 
! 0 1 
#3 Oper
 testoper 
#3 Threshold
 1.0e-15
#3 MaxIter
 20

#2 Vcc      
#3 IoLevel
 2 
#3 Occup
 0 0 
!#3 OccuMax 
! 0 1 
#3 Oper
 testoper 
#3 VscfThreshold
 1.0e-15
#3 VscfMaxIter
 40
#3 Vci 
 1
#3 Reference 
#3 MaxExci
 2
#3 ItEqResidThr
 1.0e-12
#3 Rsp       // This a the VSCF input
#4 IoLevel
 4
#4 Eigen val
 5
#4 ItEqResidThr
 1.0e-11
#4 ItEqMaxIt
 80
#4 ItEqEnerThr
 1.0e-11
#4 ItEqMaxDim
 164
#4 DiagMeth
  LAPACK
#4 RspFunc
! 1 z
 1 testoper 
 1 y
 1 u
-1 testoper Forstates 0-15 
-1 u Forstates 0-15 
 2 u u 0.0
! 2 testoper testoper 0.0
 2 y y 0.0
 2 testoper testoper 0.0
 2 u u 0.0
 2 u u 0.1
 2 u u -0.1
! 2 x x 1.0
! 2 x y 1.0
! 3 x z y 1.0
! 3 x z y 1.0 1.0
! 2 x z 1.0
! 4 x y z u 0.0 0.0 1.0
! 2 u u 0.0


/*
#2 Vcc       // This a the VSCF input
#3 Vci
 8
#3 MaxExci
 6 
#3 IoLevel
 10
#4 ItEqResidThr
 1.0e-10
*/

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.


%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=18
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4

# SCF energy:
CRIT1=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0493492614924[0-9][0-9]" | wc -l`
CRIT2=`$GREP "Occ: \[0,1\]" $log | $GREP "E =  1\.7429904052909[0-9][0-9]" | wc -l`
CRIT3=`$GREP "Occ: \[0,2\]" $log | $GREP "E =  2\.4296836007176[0-9][0-9]" | wc -l`
CRIT4=`$GREP "Occ: \[1,0\]" $log | $GREP "E =  2\.4496704954869[0-9][0-9]" | wc -l`
CRIT5=`$GREP "Occ: \[0,3\]" $log | $GREP "E =  3\.1092154093604[0-9][0-9]" | wc -l`
CRIT6=`$GREP "Occ: \[1,1\]" $log | $GREP "E =  3\.1441249282984[0-9][0-9]" | wc -l`
CRIT7=`$GREP "Occ: \[0,4\]" $log | $GREP "E =  3\.7813535099924[0-9][0-9]" | wc -l`
CRIT8=`$GREP "Occ: \[1,2\]" $log | $GREP "E =  3\.8316468350879[0-9][0-9]" | wc -l`
CRIT9=`$GREP "Occ: \[2,0\]" $log | $GREP "E =  3\.8499113594583[0-9][0-9]" | wc -l`
CRIT10=`$GREP "Occ: \[0,5\]" $log | $GREP "E =  4\.4459300371806[0-9][0-9]" | wc -l`
CRIT11=`$GREP "Occ: \[1,3\]" $log | $GREP "E =  4\.5120240417778[0-9][0-9]" | wc -l`
CRIT12=`$GREP "Occ: \[2,1\]" $log | $GREP "E =  4\.5451782900198[0-9][0-9]" | wc -l`
CRIT13=`$GREP "Occ: \[1,4\]" $log | $GREP "E =  5\.1850256725133[0-9][0-9]" | wc -l`
CRIT14=`$GREP "Occ: \[2,2\]" $log | $GREP "E =  5\.2335280439966[0-9][0-9]" | wc -l`
CRIT15=`$GREP "Occ: \[2,3\]" $log | $GREP "E =  5\.9147497010704[0-9][0-9]" | wc -l`


TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13 \+ $CRIT14 \+ $CRIT15`
CTRL[2]=2 
ERROR[2]="VSCF ENERGIES NOT CORRECT"

CRIT30=`$GREP "<<u,u>>\(0" $log | $GREP "7.1865406590[0-9][0-9]" | wc -l`
CRIT31=`$GREP "<<u,u>>\(0" $log | $GREP "7.4980131548[0-9][0-9]" | wc -l`
CRIT32=`$GREP "<<u,u>>\(" $log | $GREP "7.2175968690[7-9][0-9]" | wc -l`
CRIT33=`$GREP "<<u,u>>\(" $log | $GREP "7.5306517335[0-9][0-9]" | wc -l`

TEST[3]=`expr $CRIT30 \+ $CRIT31 \+ $CRIT32 \+ $CRIT33`
CTRL[3]=8
ERROR[3]="VCIS RESPONSE FUNCTIONS NOT CORRECT"
echo $CRIT30
echo $CRIT31
echo $CRIT32
echo $CRIT33


CRIT40=`$GREP "Rsp_Root_0" $log | $GREP "6.970482037812[0-9]" | wc -l`
CRIT41=`$GREP "Rsp_Root_1" $log | $GREP "1.348616038004[0-9]" | wc -l`

TEST[4]=`expr $CRIT40 \+ $CRIT41 `
CTRL[4]=8
ERROR[4]="VCIS RESPONSE EXCI. E. NOT CORRECT"
echo $CRIT40
echo $CRIT41

CRIT50=`$GREP "<<u,u>>\(" $log | $GREP "7.24193216125[0-9]" | wc -l`
CRIT51=`$GREP "<<u,u>>\(" $log | $GREP "7.27335778938[0-9]" | wc -l`
CRIT52=`$GREP "<<u,u>>\(" $log | $GREP "7.68354420928[0-9]" | wc -l`
CRIT53=`$GREP "<<u,u>>\(" $log | $GREP "7.71744575968[0-9]" | wc -l`

TEST[5]=`expr $CRIT50 \+ $CRIT51 \+ $CRIT52 \+ $CRIT53`
CTRL[5]=8
ERROR[5]="VCI[2] RESPONSE FUNCTIONS NOT CORRECT"
echo $CRIT50
echo $CRIT51
echo $CRIT52
echo $CRIT53


CRIT60=`$GREP "Rsp_Root_0" $log | $GREP "6.918665743605[0-9]" | wc -l`
CRIT61=`$GREP "Rsp_Root_1" $log | $GREP "1.337366615100[0-9]" | wc -l`

TEST[6]=`expr $CRIT60 \+ $CRIT61 `
CTRL[6]=8
ERROR[6]="VCI[2] RESPONSE EXCI. E. NOT CORRECT"
echo $CRIT60
echo $CRIT61


PASSED=1
for i in 1 2 3 4 5 6
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dho_vcirsp3.check
#######################################################################
