#!/usr/bin/env bash
#************************************************#
# Turbomole Validation script (using bash)
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: Midas validation script for 
#               validating electronic structure 
#               point made with TURBOMOLE.
#  
#************************************************#

#****************************************************************#
# specific validation definition for our TURBOMOLE calculation   #
#                                                                #
# User must provide                                              #
#  outfilefile         : outputfile from ES program              #
#  error_check         : array of lines to grep for in output    #
#                        that if not there will produce an error #
#  error_check_val     : array with the number of times the      #
#                        given line is presumed to be in output  #
#  suspicious_check    : array of line to grep for in output     #
#                        that if not there indicates suspicious  #
#                        behaviour that is not nescesaarily      #
#                        an error (e.g. not converged)           #
#  suspicious_check_val: array with the number of times the      #
#                        given line is presumed to be in output  #
#  files_to_copy_on_error     : files to copy back on error      #
#  files_to_copy_if_suspicious: copy back if suspicious          #
#****************************************************************#
validate_define(){
#***************************#
# !!! define validation !!! #
#***************************#
# outputfile from es program
   outputfile=TURBO.OUT

# define error checks
#   error_check[1]="ridft : all done";                 error_check_val[1]=1 
   error_check[1]="dscf : all done";                 error_check_val[1]=1 
   error_check[2]="grad : all done";                 error_check_val[2]=1 
   files_to_copy_on_error=TURBO.OUT 

# define suspicious behavior checks
   suspicious_check[1]="ENERGY CONVERGED"; suspicious_check_val[1]="1"
   files_to_copy_if_suspicious=TURBO.OUT
}

#****************************************************************#
# validate function that validates with "grep" and "wl -l"       #
# commands. What to grep for and number of lines is provided     #
# by user in define function.                                    #
#                                                                #
# On return must                                                 #
#  save number of errors in:               num_error             #
#  save number of suspicious behaviors in: num_suspicious        #
#  put files to copy back in:              files_to_copy         #
#****************************************************************#
validate(){
#*****************************#
# run define provided by user #
#*****************************#
   validate_define

#********************************************************************#
# check that input to validation is given correctly (automated part) #
#********************************************************************#
# check output file is there
   if [ ! -s $outputfile ]; then
      echo "file $outputfile does not exist or is empty"
      exit 40
   fi

# check checking parameters
   if [ ${#error_check[@]} -ne ${#error_check_val[@]} ]; then
      echo "size of error_check and error_check_val are different"
      exit 40
   fi
   if [ ${#suspicious_check[@]} -ne ${#suspicious_check_val[@]} ]; then
      echo "size of suspicious_check and suspicious_check_val are different"
      exit 40
   fi

#********************************#
# do validation (automated part) #
#********************************#
# Make error checks
   for ((i=1; i<=${#error_check[@]}; i++)); do
      check=`grep "${error_check[$i]}" $outputfile | wc -l`
      if [ $check -ne ${error_check_val[$i]} ]; then 
         ((num_error++))
      fi
   done

# Make suspicious checks
   for ((i=1; i<=${#suspicious_check[@]}; i++)); do
      check=`grep "${suspicious_check[$i]}" $outputfile | wc -l`
      if [ $check -ne ${suspicious_check_val[$i]} ]; then 
         ((num_suspicious++))
      fi
   done

# TODO: make some output on fail checks !

# copy back files?
   if [ $num_error -ne 0 ]; then files_to_copy=$files_to_copy" "$files_to_copy_on_error; fi
   if [ $num_suspicious -ne 0 ]; then files_to_copy=$files_to_copy" "$files_to_copy_if_suspicious; fi
}

#*************************************#
# generel validation wrapper function #
#*************************************#
validate_wrapper() {
# initialize number of errors and suspicious things
   num_error=0
   num_suspicious=0
   files_to_copy=

# run validation for specific calculation
   validate

# make error file
   echo $num_error 
   echo $num_suspicious 
   echo $files_to_copy 
}

#*******************************#
# Run validation script wrapper #
#*******************************#
validate_wrapper
