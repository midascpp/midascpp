#!/bin/bash
#
#  Example script for creating turbomole input
#  for use in MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  None
#
# exporting path so we know where
# `define` and `x2t` are.
#

export TURBODIR=__turbomole_bin_path__

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`
#
# function for creating a define file
#
setup_define() {
cat << %EOF% > non_interactive_define.def

title
a coord
*
no
b all def2-SVP
m "h" 1.0078250321000000000286334E+00
m "c" 1.2000000000000000000000000E+01
m "o" 1.5994914622099999999826903E+01
*
eht
y
0
y
scf
conv
9
iter
200

*
%EOF%
}
#desy
#m "n" 1.4003074005200000000381788E+01

#
# Create coord file from midasifc.xyz_input
#
# Turn this on when doing singlepoint calculations.
# For the geometry optimization it is risky as it overwrites the latest coord
# file from Turbomole. Make the coord file manualle with x2t.

x2t midasifc.xyz_input > coord

#
# Setup non-interactive define script
#
setup_define
#
# And run it
#
define < non_interactive_define.def > outputjunk
#
# The coord file can now be copied to
# midasifc.cartrot_xyz for symmetry
# module
#
t2x -c coord > midasifc.cartrot_xyz
