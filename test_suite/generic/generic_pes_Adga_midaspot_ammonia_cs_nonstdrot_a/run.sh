#!/bin/bash
#  Arguments:
#   : The scratch directory
#

cd $1

_SUBST_MIDASPOT_ -o energy -o dip_x -o dip_y -o dip_z midaspot.inp > midaspot.out

# Output should be like:
# dip_x = +1.829496586821681e-28
# dip_y = -5.977222800000080e-01
# dip_z = +0.000000000000000e+00
# energy = -5.646051319110000e+01

energy=$(grep "energy" midaspot.out | awk '{print $3}')
dip_x=$(grep "dip_x" midaspot.out | awk '{print $3}')
dip_y=$(grep "dip_y" midaspot.out | awk '{print $3}')
dip_z=$(grep "dip_z" midaspot.out | awk '{print $3}')

echo "GROUND_STATE_ENERGY     $energy" > midasifc.prop_general
echo "X_DIPOLE     $dip_x" >> midasifc.prop_general
echo "Y_DIPOLE     $dip_y" >> midasifc.prop_general
echo "Z_DIPOLE     $dip_z" >> midasifc.prop_general
