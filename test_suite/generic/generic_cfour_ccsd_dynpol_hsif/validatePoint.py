#!/usr/bin/env python
#*******************
# Validation script (using python)
#
#  author: Ian H. Godtliebsen (ian@chem.au.dk)
#
#  Description: Midas validation script for
#               validating electronic structure point
#               written in python.
#
#******************

import re

#***************************************************************#
#           !!! user defines validate definition !!!            #
#                                                               #
# outfile                    : output file from ES program      #
# error_check                : tuple of strings to be matched   #
#                              in outfile that if not found     #
#                              indicates an error!              #
# suspicious_check           : tuple of string to be matched    #
#                              in outfile that if not found     #
#                              indicates suspicioous behaviour  #
#                              that is not nescesarily an error #
# files_to_copy_on_error     : files to copy back on error      #
# files_to_copy_if_suspicious: files to copy back if            #
#                              suspicious behaviour is noticed  #
#***************************************************************#
class validate_definition:
   outfile=open('CFOUR.OUT','r')
   error_check      = ('executable xjoda finished with status[ ]+0',
                      'executable xsdcc finished with status[ ]+0',
                      'CCSD frequency-dependent Polarizability Tensor \(in a.u.\)')
   suspicious_check = ('A miracle come to pass. The CC iterations have converged.',)
   files_to_copy_on_error="CFOUR.OUT"
   files_to_copy_if_suspicious="CFOUR.OUT"

   #destructor
   def __del__(self):
      outfile.close()
   
#***************************************************#
# Rest should not be tinkered with by average user! #
#***************************************************#
# define validate line
def validate_line(validate_definition, 
      line_to_validate, 
      files_to_copy_if_no_match, 
      num_error, 
      files_to_copy):
   line_found = False
   for line in validate_definition.outfile.readlines():
      if re.search(line_to_validate, line):
         line_found = True
         
   if not line_found:
      num_error=num_error+1
      files_to_copy=files_to_copy+" "+files_to_copy_if_no_match
   
   validate_definition.outfile.seek(0)
   return (num_error, files_to_copy)

# define validate implementation
def validate_impl(validate_definition):
   # initialize variables
   num_error=0
   num_suspicious=0
   files_to_copy=""
   
   # error check
   for index in range(len(validate_definition.error_check)):
      (num_error, files_to_copy) = validate_line(validate_definition, 
                                            validate_definition.error_check[index], 
                                            validate_definition.files_to_copy_on_error, 
                                            num_error, 
                                            files_to_copy)
   
   # suspicious check
   for index in range(len(validate_definition.suspicious_check)):
      (num_suspicious, files_to_copy) = validate_line(validate_definition, 
                                            validate_definition.suspicious_check[index], 
                                            validate_definition.files_to_copy_if_suspicious, 
                                            num_suspicious, 
                                            files_to_copy)

   return (num_error, num_suspicious, files_to_copy)

# define validate wrapper
def validate_wrapper():
   # run validation
   (num_error, num_suspicious, files_to_copy) = validate_impl(validate_definition)
   
   print num_error
   print num_suspicious
   print files_to_copy

# define main funcion
def main():
   validate_wrapper()

# run main function
if __name__ == '__main__':
   main()
