#!/bin/bash
#
#  Example script for creating molpro input
#  for use in MidasCpp. Below for MP2/cc-pVDZ.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  None
#
# function for creating a molpro file
# from an xyz file
#
setup_molpro() {
cat << %EOF% > singlepoint.inp
***, Midas generate input for molpro. The number of nuclei is 3
geomtyp=xyz
geometry
%EOF%
# and number of coordinates
n_atoms=`cat midasifc.xyz_input | wc -l`
n_atoms=`expr $n_atoms - 2`
echo $n_atoms >> singlepoint.inp
# And a comment
cat << %EOF% >> singlepoint.inp
below follow input...
%EOF%
# Now write structure, first two lines are number of atoms
# and a blank line (std. xyz format)
tail -n$n_atoms midasifc.xyz_input | sed -e 's/^ //g' \
    | sed -e 's/[0-9][0-9][0-9][0-9][0-9][0-9]e/e/g' \
    | sed -e 's/[0-9][0-9][0-9][0-9][0-9][0-9]E/E/g' | sed 's/e/d/g' >>singlepoint.inp
#
# And now the calculation specification
#
cat << %EOF% >> singlepoint.inp
end

gthresh,energy=1.d-10,orbital=1.d-10
MAXIT=200
basis=vtz
hf,MAXIT=200
ccsd(t)-f12c,MAXIT=200

put,xyz,midasifc.cartrot_xyz,new
%EOF%
}
#
# Create coord file from midasifc.xyz_input
#
setup_molpro
