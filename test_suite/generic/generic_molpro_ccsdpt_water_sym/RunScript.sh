#!/bin/bash
#
#  Example script for running turbomole
#  through MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  $1 : The scratch directory

#
# function for creating a define file
#
create_property_file() {
   # below we get the energy (MP2) and dipole moment
   # component (MP2)
   energy=`grep "\!CCSD(T)-F12c total energy" singlepoint.out | awk -F' ' '{print $4}'`
   

   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general
}
#
# Change dir to $1 (the scratch dir)
#
cd $1
#
# All files are here, we may run
#

molpro_dir_path/bin/molpro -d $1 -m 120000000 -G 120000000 -o $1/singlepoint.out $1/singlepoint.inp

#
# After completion, setup file with properties
# according to midasifc.prop_info located in
# the savedir
#
create_property_file
