#!/bin/bash
n_atoms="_SUBST_N_ATOMS_"
n_lines=$(wc -l midasifc.xyz_input | awk '{print $1}')
xyz_beg=$((n_lines - n_atoms + 1))

awk '
   BEGIN{
      # Beginning of XYZ section.
      xyz_beg = '${xyz_beg}';
      # Rotation matrix. See README.
      r_xx = -0.500000000000000;   r_xy = 0.000000000000000;  r_xz =  0.866025403784439;
      r_yx =  0.000000000000000;   r_yy = 1.000000000000000;  r_yz = -0.000000000000000;
      r_zx = -0.866025403784439;   r_zy = 0.000000000000000;  r_zz = -0.500000000000000;
   }

   NR < xyz_beg {
      # Just print.
      print $0;
   }

   NR >= xyz_beg {
      # Expecting lines like:
      #     N   x_coord   y_coord   z_coord
      #
      # Transform with _inverse_ rotation matrix.
      x = r_xx*($2) + r_yx*($3) + r_zx*($4);
      y = r_xy*($2) + r_yy*($3) + r_zy*($4);
      z = r_xz*($2) + r_yz*($3) + r_zz*($4);
      printf("%s % 22.16E % 22.16E % 22.16E\n", $1, x, y, z);
   }
' midasifc.xyz_input > midasifc.cartrot_xyz

tail -n $n_atoms midasifc.cartrot_xyz > midaspot.inp
