Compared to the molecule in 

   generic_pes_Adga_midaspot_ammonia_cs

the nuclear positions (#1XYZ) and the vibrational coordinates (#1VIBCOORD,
#2COORDINATE) of this molecule has been rotated with the rotation matrix:

   R =
         -0.500000000000000   0.000000000000000   0.866025403784439
          0.000000000000000   1.000000000000000  -0.000000000000000
         -0.866025403784439   0.000000000000000  -0.500000000000000

   R^T = 
         -0.500000000000000   0.000000000000000  -0.866025403784439
          0.000000000000000   1.000000000000000   0.000000000000000
          0.866025403784439  -0.000000000000000  -0.500000000000000

corresponding to the Euler angles

   alpha =  pi/2     ( 90 deg)
   beta  =  2pi/3    (120 deg)
   gamma = -pi/2     (-90 deg)

It is a rotation about the C_3 axis of the molecule, which is the y axis in
this case, so the rotation matrix can in this case also be achieved from the
standard rotation matrix around y axis.
The C_3 axis is along the y axis, since the molecule is only described as
having C_s symmetry, and here the usual choice is to have the mirror plane in
the xy plane.

The rotation leaves N as it is but permutes the hydrogens, and thus changes the
mirror plane to _not_ be in the xy plane. Hopefully Midas will handle this
correctly.

================================================================================
The rotation matrix corresponds to the Euler angles through
   1. rotation by alpha around internal z axis
   2. rotation by beta  around internal x axis (called N at that stage)
   3. rotation by gamma around internal z axis (now called z')


    ^ z                ^ z             z' ^                  z' ^     ^ y'   
    |                  |                   \     ,¬ y            \   /       
    |          alpha   | ___,> y  beta      \  .´       beta      \ /        
    +-----> y  --->    +´         --->       +´         --->       +-----> x'
   /                    \                     \                              
  /                      \                     \                             
 v x                      v x(N)                v x(N)                       
 
Or look it up... :)

One can deduce that (a: alpha, b: beta, g: gamma)

   R_N(b)   = R_z(a) R_x(b) R_z(-a)
   R_z'(g)  = R_N(b) R_z(g) R_N(-b)

and thus that

   R(a,b,g) = R_z'(g) R_N(b) R_z(a)
            = R_N(b) R_z(g) R_N(-b) R_N(b) R_z(a)
            = R_N(b) R_z(g) R_z(a)
            = R_z(a) R_x(b) R_z(-a) R_z(g) R_z(a)
            = R_z(a) R_x(b) R_z(g)

which expressed the total rotation in terms of the initial axes.
Using (c: cosine, s: sine)

   R_z(a) =
               c(a)  -s(a) 0
               s(a)   c(a) 0
               0     0     1

   R_x(a) =    1     0     0  
               0     c(a)  -s(a)
               0     s(a)  c(a)

one gets

   R(a,b,g) =
               c(a)c(g) - s(a)c(b)s(g)    -c(a)s(g) - s(a)c(b)c(g)    s(a)s(b)
               s(a)c(g) + c(a)c(b)s(g)    -s(a)s(g) + c(a)c(b)c(g)   -c(a)s(b)
               s(b)s(g)                    s(b)c(g)                   c(b)

if I haven't made a typo. Anyway, the actual rotation matrix was computed
correctly - you can always check that it has orthonormal columns/rows if in
doubt.

/Mads Boettger Hansen, mb.hansen@chem.au.dk
