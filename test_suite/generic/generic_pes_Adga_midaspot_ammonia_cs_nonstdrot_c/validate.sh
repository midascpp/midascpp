#!/bin/bash

num_error=0
num_suspicious=0
files_to_copy=

if ! grep -q "energy" midaspot.out\
|| ! grep -q "dip_x" midaspot.out\
|| ! grep -q "dip_y" midaspot.out\
|| ! grep -q "dip_z" midaspot.out
then
   num_error=0
   files_to_copy="midaspot.out midasifc.xyz_input midasifc.cartrot_xyz midaspot.inp"
fi

echo $num_error
echo $num_suspicious
echo $files_to_copy
