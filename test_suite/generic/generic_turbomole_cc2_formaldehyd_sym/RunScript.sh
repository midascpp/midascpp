#!/bin/bash
#
#  Example script for running turbomole
#  through MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  $1 : The scratch directory
#
# exporting path so we know where
# e.g. jobex is
#
export TURBODIR=turbomole_bin_path

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`

#
# function for creating a define file
#
create_property_file() {
   # below we get the energy (MP2) and dipole moment
   # component (MP2)
   energy=`grep "\* *Final CC2 energy" TURBO.OUT | awk '{print $6}'`
   dip_x=`grep -A5 "dipole moment:"           TURBO.OUT \
          | tail -n3 | head -n1 | awk '{print $2}'`
   dip_y=`grep -A5 "dipole moment:"           TURBO.OUT \
          | tail -n2 | head -n1 | awk '{print $2}'`
   dip_z=`grep -A5 "dipole moment:"           TURBO.OUT \
          | tail -n1 | awk '{print $2}'`

   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general
   echo "X_DIPOLE   " $dip_x >> midasifc.prop_general
   echo "Y_DIPOLE   " $dip_y >> midasifc.prop_general
   echo "Z_DIPOLE   " $dip_z >> midasifc.prop_general
}
#
# Change dir to $1 (the scratch dir)
#
cd $1
#
# All files are here, we may run
#
dscf > TURBO.OUT
ricc2 >> TURBO.OUT
#
# After completion, setup file with properties
# according to midasifc.prop_info located in
# the savedir
#
create_property_file
