#!/bin/bash
#
#  Example script for creating turbomole input
#  for use in MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  None
#
# exporting path so we know where
# `define` and `x2t` are.
#
export TURBODIR=turbomole_bin_path

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`
#
# function for creating a define file
#
setup_define() {
cat << %EOF% > non_interactive_define.def

TITLE
a coord
desy
*
no
b all cc-pVDZ
m "h" 1
m "c" 12
m "o" 16
*
eht
y
0
y
scf
conv
8
iter
200

cc2
freeze
*
memory
500
cbas
b all cc-pVDZ
*
ricc2
cc2
oconv 7
conv 8
maxiter 200
*
resp
fop
*
*
*
%EOF%
}
#
# Create coord file from midasifc.xyz_input
#
x2t midasifc.xyz_input > coord
#
# Setup non-interactive define script
#
setup_define
#
# And run it
#
define < non_interactive_define.def > InputJunk
#
# The coord file can now be copied to
# midasifc.cartrot_xyz for symmetry
# module
#
echo "NOW CONVERT:"
pwd
which t2x
t2x -c coord > midasifc.cartrot_xyz
echo "DONE!"
