#!/bin/bash
#
#  Script for running turbomole
#  through MidasCpp.
#
#  Model:   SCF (HF)
#
#  Arguments:
#
#  $1 : The scratch directory
#  $2 : The done file to be touched upon
#       successful completion
#
# exporting path so we know where
# e.g. jobex is
#
export TURBODIR=__turbomole_bin_path__

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`
#
# function for creating a define file
#
#NB! REMEMBER TO ADJUST THE ARGUMENTS FOR THE GRADIENT SO THAT
# echo "N" > ...
# tail -n(N+1) gradient | head -nN | ...
# where N = number of atoms
create_property_file() {
   # below we get the energy and dipole moment
   energy=`grep "|  total energy      =" TURBO.OUT | awk '{print $5}'`
   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general

   echo "6" > midasifc.my_deriva
   tail -n7 gradient | head -n6 | sed -e 's/ /\n/g' | grep [0-9] | sed 's/D/E/g' >> midasifc.my_deriva
}
#
# Change dir to $1 (the scratch dir)
#
cd $1
#
# All files are here, we may run.
#
dscf > TURBO.OUT
grad >> TURBO.OUT
#
# After completion, setup file with properties
# according to midasifc.prop_info located in
# the savedir
#
create_property_file
