#!/bin/bash
#
#  Example script for running turbomole
#  through MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  $1 : The scratch directory
#
# exporting path so we know where
# e.g. jobex is
#
export TURBODIR=turbomole_bin_path

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`
#
# function for creating a define file
#
create_property_file() {
   # below we get the energy (MP2) and dipole moment
   # component (MP2)
   energy=`grep "             \*      total        :" \
           TURBO.OUT | awk '{print $4}'`
   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general

   echo "3" > midasifc.my_deriva
   tail -n4 gradient | head -n3 | sed -e 's/ /\n/g' | grep [0-9] | sed 's/D/E/g' >> midasifc.my_deriva
}
#
# Change dir to $1 (the scratch dir)
#
cd $1
#
# All files are here, we may run
#
dscf > TURBO.OUT
rimp2 -ri -level mp2 >> TURBO.OUT
#
# After completion, setup file with properties
# according to midasifc.prop_info located in
# the savedir
#
create_property_file
