#!/bin/bash
#
#  Example script for creating turbomole input
#  for use in MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  None
#
# exporting path so we know where
# `define` and `x2t` are.
#
export TURBODIR=turbomole_bin_path

PATH=$PATH:$TURBODIR/scripts
PATH=$PATH:$TURBODIR/bin/`sysname`
#
# function for creating a define file
#
setup_define() {
cat << %EOF% > non_interactive_define.def

TITLE
a coord
desy
*
no
b all cc-pVDZ
m "h" 1
m "o" 16
*
eht
y
0
y
scf
conv
8
iter
200

cc2
freeze
*
memory
500
cbas
b all cc-pVDZ
*
ricc2
mp2
oconv 8
conv 9
maxiter 200
*
resp
gradient
thrsemi 5
*
*
*
%EOF%
}

#
# Create coord file from midasifc.xyz_input
#
x2t midasifc.xyz_input > coord
#
# Setup non-interactive define script
#
setup_define
#
# And run it
#
define < non_interactive_define.def > outputjunk
#
# The coord file can now be copied to
# midasifc.cartrot_xyz for symmetry
# module
#
t2x -c coord > midasifc.cartrot_xyz
