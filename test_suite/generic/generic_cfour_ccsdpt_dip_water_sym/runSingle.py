#!/usr/bin/env python

# Run a single point in MidasCpp using CFOUR
# Last modified: Aug. 16, 2011
# Mikkel Bo Hansen (mbh@chem.au.dk)
#
# Requires the path of the cfour bin and basis directories
# (CFOUR_BIN and CFOUR_BAS below)
#
# Input: 
#     $1: scratch directory
#

import sys
from sys import stdin
import os
import re

au_a = 0.5291772108    # au to Angstrom
CFOUR_BIN='cfour_bin_path'
CFOUR_BAS='cfour_basis_path'

# Run cfour calculation; i.e. run xcfour
def run_calc():   
   # put CFOUR_BIN in path
   os.environ['PATH'] += os.pathsep + CFOUR_BIN
   # assume that ZMAT is available
   command='cp '+CFOUR_BAS+' .'
   os.system(command)
   command='xcfour > CFOUR.OUT'
   os.system(command)
#out=open('tissemand','w')
#out.write('WILL NOW RUN COMMAND: %s\n' % (command))
#out.write('PATH: %s' % (os.sys.path))
#out.close()

# Read geometry from MOL file supplied after a
# CFOUR run
# Output: midasifc.rotated_xyz
def create_rotated():
   # search for atoms in the MOL file. This example is for
   # water => only "H" and "O"
   out=open('midasifc.cartrot_xyz','w')
   n_atoms=0
   atoms=[]
   inp=open('MOLDEN','r')
   regex='\[ATOMS\] AU'
   while 1:
      line=inp.readline()
      if re.match(regex,line) is not None:
         break
   regex='\[Molden Format\]'
   while 1:
      line=inp.readline()
      if re.match(regex,line) is not None:
         break
      n_atoms+=1
      [name,d1,d2,x,y,z]=line.split()
      x=float(x)*au_a
      y=float(y)*au_a
      z=float(z)*au_a
      atom=[name,x,y,z]
      atoms.append(atom)
   out.write(str(len(atoms))+'\n\n')
   for i in range(0,len(atoms)):
      x_s="%18.12f " % atoms[i][1]
      y_s="%18.12f " % atoms[i][2]
      z_s="%18.12f " % atoms[i][3]
      s=atoms[i][0]+' '+x_s+' '+y_s+' '+z_s
      out.write(s)
      out.write("\n")
   out.close()
   return [atoms,n_atoms]

# Get the required properties
# Output: midasifc.prop_general
def write_prop(n_atoms,atoms):
   #read electronic energy from CFOUR.OUT
   regex='\s+E\(CCSD\(T\)\)'
   out=open('midasifc.prop_general','w')
   inp=open('CFOUR.OUT','r')
   while 1:
      line=inp.readline()
      if re.match(regex,line) is not None:
         [energy]=line.split()[2:3]
         s='GROUND_STATE_ENERGY   '+str(energy)+'\n'
         out.write(s)
         break
   # read dipole moment
   while 1:
      line=inp.readline()
      if re.match('.+Properties computed from the correlated',line) is not None:
         # read another line
         line=inp.readline()
         # next line is dipole moment
         dipole=inp.readline().split()[2:9:3]
         break
   out.write('X_DIPOLE   %s\n' % (dipole[0]))
   out.write('Y_DIPOLE   %s\n' % (dipole[1]))
   out.write('Z_DIPOLE   %s\n' % (dipole[2]))
   out.close()
      
def main():
   if len(sys.argv)!=2:
      print 'ERROR'
      sys.exit(10)
   #change directory to argv[1]
   dir=sys.argv[1]
   old_dir=os.getcwd()
   os.chdir(dir)
   run_calc()
   [atoms,n_atoms]=create_rotated()
   write_prop(n_atoms,atoms)
   os.chdir(old_dir)
   
if __name__ == '__main__':
   main()
