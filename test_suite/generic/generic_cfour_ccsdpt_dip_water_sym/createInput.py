#!/usr/bin/env python

# create files for running a single point in MidasCpp using CFOUR
# Last modified: Aug. 16, 2011
# Mikkel Bo Hansen (mbh@chem.au.dk)
#
# Input: 
#     $1: scratch directory
#     $2: file to touch when done
#

import sys
from sys import stdin
import os
import re

au_a = 0.5291772108    # au to Angstrom

def read_geometry():
   inp=open('midasifc.xyz_input','r')
   # first two lines are not needed
   inp.readline()
   inp.readline()
   atoms=[]
   while 1:
      line=inp.readline()
      if not line:
         break
      atom=line.split()
      atom[1]=float(atom[1])
      atom[2]=float(atom[2])
      atom[3]=float(atom[3])
      atoms.append(atom)
   return atoms

def write_zmat(atoms):
   out=open('ZMAT','w')
   out.write('#midas generated ZMAT\n')
   for i in range(0,len(atoms)):
      s=atoms[i][0]+' '+str(atoms[i][1])+' '+str(atoms[i][2])+' '+str(atoms[i][3])+'\n'
      out.write(s);
   out.write('\n')
   out.write('*CFOUR(CALC=CCSD(T)\n')
   out.write('COORD=CARTESIAN\n')
   out.write('DROP_MO=1\n')
   out.write('SCF_CONV=9\n')
   out.write('LINEQ_CONV=9\n')
   out.write('MEMORY=60000000\n')
   out.write('BASIS=PVDZ\n')
   out.write('PROP=FIRST_ORDER)\n\n')
   out.close()

def main():
   # read midasifc.xyz_input and transform into ZMAT file
   atoms=read_geometry()
   write_zmat(atoms)
   
if __name__ == '__main__':
   main()
