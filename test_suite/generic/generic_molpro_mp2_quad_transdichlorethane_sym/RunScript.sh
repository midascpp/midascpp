#!/bin/bash
#
#  Example script for running turbomole
#  through MidasCpp. Below for rimp2(fc)/TZVP.
#
#  Written by Mikkel Bo Hansen, Sept. 2010
#  (mbh@chem.au.dk)
#
#  Arguments:
#
#  $1 : The scratch directory

#
# function for creating a define file
#
create_property_file() {
   # below we get the energy (MP2) and dipole moment
   # component (MP2)
   energy=`grep "\!MP2 total energy" singlepoint.out | awk -F' ' '{print $4}'`
   
   if [ `grep "<1.1|QMXX|1.1>" singlepoint.out | wc -l` == "1" ]; then
      XX=`grep "<1.1|QMXX|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      XX=0
   fi
   
   if [ `grep "<1.1|QMXY|1.1>" singlepoint.out | wc -l` == "1" ]; then
      XY=`grep "<1.1|QMXY|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      XY=0
   fi
   
   if [ `grep "<1.1|QMXZ|1.1>" singlepoint.out | wc -l` == "1" ]; then
      XZ=`grep "<1.1|QMXZ|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      XZ=0
   fi

   if [ `grep "<1.1|QMYY|1.1>" singlepoint.out | wc -l` == "1" ]; then
      YY=`grep "<1.1|QMYY|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      YY=0
   fi

   if [ `grep "<1.1|QMYZ|1.1>" singlepoint.out | wc -l` == "1" ]; then
      YZ=`grep "<1.1|QMYZ|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      YZ=0
   fi

   if [ `grep "<1.1|QMZZ|1.1>" singlepoint.out | wc -l` == "1" ]; then
      ZZ=`grep "<1.1|QMZZ|1.1>" singlepoint.out | awk -F' ' '{print $4}'`
   else
      ZZ=0
   fi

   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general
   echo "MQUAD_XX    " $XX >> midasifc.prop_general
   echo "MQUAD_XY    " $XY >> midasifc.prop_general
   echo "MQUAD_YX    " $XY >> midasifc.prop_general
   echo "MQUAD_XZ    " $XZ >> midasifc.prop_general
   echo "MQUAD_ZX    " $XZ >> midasifc.prop_general
   echo "MQUAD_YY    " $YY >> midasifc.prop_general
   echo "MQUAD_YZ    " $YZ >> midasifc.prop_general
   echo "MQUAD_ZY    " $YZ >> midasifc.prop_general
   echo "MQUAD_ZZ    " $ZZ >> midasifc.prop_general
}
#
# Change dir to $1 (the scratch dir)
#
cd $1
#
# All files are here, we may run
#

molpro_dir_path/bin/molpro -d $1 -m 120000000 -G 120000000 -o $1/singlepoint.out $1/singlepoint.inp

#
# After completion, setup file with properties
# according to midasifc.prop_info located in
# the savedir
#
create_property_file
