#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         methanol
   Wave Function:    TURBOMOLE SCF/def2-SVP
   Test Purpose:     Check FreqAna with targeting. Note: Model/basis set _not_
                     suitable for quantitative results.
%EOF%

CALCNAME=freqana_hessian
MAINDIR=$(pwd)/Dir_${CALCNAME}
TEMPLATEDIR=$(pwd)/generic/generic_${CALCNAME}
RUNSCRIPT=run.sh
INPUTSCRIPT=input.sh
VALIDATESCRIPT=validate.sh
MIDASIFCPROPINFO=midasifc.propinfo
MOLECULEINFO=molecule.mmol
RESULTMOLECULEINFO=${MAINDIR}/${CALCNAME}_result.mmol

export TMPDIR=$(pwd)/tmpouts/$CALCNAME
export SAVEDIR=$TMPDIR/savedir
export SETUPDIR=$TMPDIR/setup
export ANALYSISDIR=$TMPDIR/analysis

#######################################################################
#  Check for turbomole existence
#######################################################################
turbomole_bin=`grep "^turbomole_bin" ES_PROGRAMS | cut -d '"' -f2`

echo $turbomole_bin

export TURBODIR=$turbomole_bin
export PATH=$TURBODIR/scripts:$PATH

sysname=`sysname`

sysname

for prog in define dscf grad
do
   if test ! -x $turbomole_bin/bin/$sysname/$prog
   then
      echo turbomole_bin = $turbomole_bin
      echo sysname = `sysname`
      echo "turbomole_bin path in ES_PROGRAMS MUST be right for this test to run"
      echo "failed for executable: $prog"
   fi
done

#######################################################################
#  Directories
#######################################################################
rm -rf  $MAINDIR
mkdir $MAINDIR
mkdir -p $SETUPDIR

#######################################################################
#  Required files to savedir
#######################################################################
for file in $RUNSCRIPT $INPUTSCRIPT $VALIDATESCRIPT $MIDASIFCPROPINFO
do
   cp $TEMPLATEDIR/$file $SETUPDIR
done

for file in $RUNSCRIPT $INPUTSCRIPT $VALIDATESCRIPT
do
   sed -i.bk "s|__turbomole_bin_path__|$TURBODIR|g" $SETUPDIR/$file
   chmod +x $SETUPDIR/$file
done

#######################################################################
#  Molecule info
#######################################################################
cp $TEMPLATEDIR/$MOLECULEINFO ${CALCNAME}.mmol


#######################################################################
#  Midas input
#######################################################################
cat > ${CALCNAME}.minp << %EOF%
#0 MIDAS Input

#1 SINGLEPOINT
#2 Name
sp_freqana
#2 Type
SP_GENERIC
#2 InputcreatorScript
${INPUTSCRIPT}
#2 RunScript
${RUNSCRIPT}
#2 ValidationScript
${VALIDATESCRIPT}
#2 SETUPDIR
${SETUPDIR}

#1System
#2SysDef
#3Moleculefile
Midas
Molecule.mmol
#3Name
molecule
#2 ModSys
#3Sysnames
1
molecule
#3RemoveGlobalTR
#3ModVibCoord
FreqAna
#4 MolInfoOutName
${RESULTMOLECULEINFO}
#4 DispFactor
1e-3
#4 SinglePointName
sp_freqana
#4 NumNodes
1
#4 ProcPerNode
4
#4 DumpInterval
4
#4 RotationThr
1e-6

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1
molinfo='${RESULTMOLECULEINFO}'

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Translation/rotation projected out?
# ------------------------------------------------------------------------------
# Full system has 18 degrees of freedom. We ask that trans/rot be projected
# out, so should end up with only 12.
TEST+=($($GREP -A1 "#1FREQ" $molinfo | awk '\''(NR==2){print $1}'\''))
CTRL+=(12)
ERROR+=("Wrong frequency count.")

TEST+=($($GREP -c "^ *[[:digit:]\.E\+]* *A *Q[[:digit:]]* *$" $molinfo))
CTRL+=(12)
ERROR+=("Wrong number of frequencies tabulated.")

TEST+=($($GREP -c "#2COORDINATE" $molinfo))
CTRL+=(12)
ERROR+=("Wrong number of coordinates found.")

# ------------------------------------------------------------------------------
# Frequencies
# ------------------------------------------------------------------------------
# Due to the Hessian being calculated semi-numerically it is not super stable
# w.r.t. the choice of displacement factor and coordinate basis (Cartesian in
# this case). So we do not expect more than 4-6 digits of accuracy.
# Example frequency block:
#     #1FREQ
#     12 cm-1
#      3.459149756812E+02 A Q0
#      1.156132828260E+03 A Q1
#      1.207838635835E+03 A Q2
#      1.280981901050E+03 A Q3
#      1.478424894963E+03 A Q4
#      1.606670376573E+03 A Q5
#      1.608218351202E+03 A Q6
#      1.623077602391E+03 A Q7
#      3.150552498926E+03 A Q8
#      3.211927651056E+03 A Q9
#      3.284808389560E+03 A Q10
#      4.192969792742E+03 A Q11

CRIT_FREQ[0]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "3.4591[[:digit:]]*E\+02 A Q0")
CRIT_FREQ[1]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.1561[[:digit:]]*E\+03 A Q1")
CRIT_FREQ[2]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.2078[[:digit:]]*E\+03 A Q2")
CRIT_FREQ[3]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.2809[[:digit:]]*E\+03 A Q3")
CRIT_FREQ[4]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.4784[[:digit:]]*E\+03 A Q4")
CRIT_FREQ[5]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.6066[[:digit:]]*E\+03 A Q5")
CRIT_FREQ[6]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.6082[[:digit:]]*E\+03 A Q6")
CRIT_FREQ[7]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "1.6230[[:digit:]]*E\+03 A Q7")
CRIT_FREQ[8]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "3.1505[[:digit:]]*E\+03 A Q8")
CRIT_FREQ[9]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "3.2119[[:digit:]]*E\+03 A Q9")
CRIT_FREQ[10]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "3.2848[[:digit:]]*E\+03 A Q10")
CRIT_FREQ[11]=$($GREP -A13 "#1FREQ" $molinfo | $GREP -c "4.1929[[:digit:]]*E\+03 A Q11")

for i in $(seq 0 11)
do
   TEST+=(${CRIT_FREQ[$i]})
   CTRL+=(1)
   ERROR+=("Wrong frequency, CRIT_FREQ[$i].")
done

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo $molinfo
  cat $molinfo
  echo THERE IS A PROBLEM 
  exit 1
fi

' > ${CALCNAME}.check
#######################################################################
