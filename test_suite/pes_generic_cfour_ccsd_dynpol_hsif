#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         HSiF
   Wave Function:    CFOUR CCSD/cc-pVDZ
   Test Purpose:     Check generic framework
%EOF%

#######################################################################
#  Check for cfour existence
#######################################################################
cfour_bin=`grep "^cfour_bin" ES_PROGRAMS | cut -d '=' -f2`
cfour_bin=`echo $cfour_bin | cut -d '"' -f2`
if [ ! -x $cfour_bin/xcfour ]; then
   echo $cfour_bin
   echo "cfour_bin path in ES_PROGRAMS MUST be right for this test to run"
   # 132 is TEST script error code!
   exit 132
fi
cfour_basis=`grep "^cfour_basis" ES_PROGRAMS | cut -d '=' -f2`
cfour_basis=`echo $cfour_basis | cut -d '"' -f2`
if [ ! -e $cfour_basis ]; then
   echo $cfour_basis
   echo "cfour_basis path in ES_PROGRAMS MUST be right for this test to run"
   # 132 is TEST script error code!
   exit 132
fi
#######################################################################
#  End of check
#######################################################################

MAINDIR=$PWD/Dir_cfour_ccsd_dynpol_hsif
export MAINDIR
rm -rf  $MAINDIR

SETUPDIR=$MAINDIR/level_1/setup
export SETUPDIR

SAVEDIR=$MAINDIR/level_1/savedir
export SAVEDIR

ANALYSISDIR=$MAINDIR/analysis
export ANALYSISDIR

TEMPLATEDIR=$PWD/generic/generic_cfour_ccsd_dynpol_hsif

#######################################################################
#  MOLECULE INPUT
#######################################################################
export MYSAVE=`echo $MAINDIR | sed -e 's/\//\\\\\//g'`
echo $MYSAVE
mkdir $MAINDIR
mkdir $MAINDIR/level_1
mkdir $SAVEDIR
mkdir $SETUPDIR
eval sed -e 's/PWD/$MYSAVE/g' $TEMPLATEDIR/pes.minp > pes_generic_cfour_ccsd_dynpol_hsif.minp

#######################################################################
#  REQUIRED FILES TO SETUPDIR
#######################################################################
cp $TEMPLATEDIR/midasifc.propinfo $SETUPDIR/.
cp $TEMPLATEDIR/createInput.py $SETUPDIR/.
cp $TEMPLATEDIR/validatePoint.py $SETUPDIR/.
cfour_bin=`echo $cfour_bin | sed -e 's/\//\\\\\//g'`
cfour_basis=`echo $cfour_basis | sed -e 's/\//\\\\\//g'`
eval sed -e 's/cfour_basis_path/$cfour_basis/g' $TEMPLATEDIR/runSingle.py > generic/tmpfile
eval sed -e 's/cfour_bin_path/$cfour_bin/g' generic/tmpfile > $SETUPDIR/runSingle.py
chmod +x $SETUPDIR/runSingle.py
rm generic/tmpfile
#######################################################################
#  NUMDER INPUT 
#######################################################################
cp $TEMPLATEDIR/midasifc.numder pes_generic_cfour_ccsd_dynpol_hsif.mmol
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL"
log=\$1
dir=$MAINDIR/savedir/

if [ \`uname\` = Linux ]; then
   GREP=\"egrep -a\"
else
   GREP=\"egrep\"
fi

# Is midas ended?
CRIT0=\`\$GREP \"Midas ended at\" \$log | wc -l\`
TEST[0]=\`expr \$CRIT0\`
CTRL[0]=1
ERROR[0]=\"MIDAS NOT ENDED\"

# Check for calculation of points
CRIT1=\`\$GREP \"Adga Iteration\" \$log | wc -l\`
CRIT2=\`\$GREP \" Total : 886 \" \$log | wc -l\`
TEST[1]=\`expr \$CRIT1\`
TEST[2]=\`expr \$CRIT2\`
CTRL[1]=11
CTRL[2]=1
ERROR[1]=\"No. of ADGA iterations not correct\"
ERROR[2]=\"Total no. of single point calculations not correct\"

# Coefficients, i.e. Dir_cfour...../savedir/prop_no_[1-10].mop
CRIT3=\`\$GREP \"2.759095................E-04 Q\^1\(Q0\) Q\^2\(Q1\)\" \$dir/prop_no_1.mop | wc -l\`
CRIT4=\`\$GREP \"3.851672................E-01 Q\^1\(Q2\)\" \$dir/prop_no_3.mop | wc -l\`
CRIT5=\`\$GREP \"1.521340................E-03 Q\^3\(Q1\) Q\^1\(Q2\)\" \$dir/prop_no_6.mop | wc -l\`
TEST[3]=\`expr \$CRIT3\`
TEST[4]=\`expr \$CRIT4\`
TEST[5]=\`expr \$CRIT5\`
CTRL[3]=1
CTRL[4]=1
CTRL[5]=1
ERROR[3]=\"prop_no_1.mop not correct\"
ERROR[4]=\"prop_no_3.mop not correct\"
ERROR[5]=\"prop_no_6.mop not correct\"

PASSED=1
for i in 0 1 2 3 4 5
do 
   if [ \${TEST[i]} -ne \${CTRL[i]} ]; then
     echo \${ERROR[i]}\"! \"\${TEST[i]}\" should be: \"\${CTRL[i]}
     PASSED=0
   fi
done 

if [ \$PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

" > pes_generic_cfour_ccsd_dynpol_hsif.check
#######################################################################
