#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
TESTNAME="mctdh2v_nonadiabatic"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Pyrazine 4M model
   Wave Function:    MCTDH[2,V]
   Test Purpose:     Calculate auto-correlation function and electronic state populations.
                     Check that the B vector for optimizing the MCTDH[n,V] constraint operators
                     is calculated correctly.
   Reference:
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

#1 General

#1 Vib
#2 IOLevel
 5

// Hamiltonian
#2 Operator
#3 Name
   h_mctdh
#3 OperFile
   ${TESTNAME}.mop
#3 SetInfo
   type=energy

// Hamiltonian for generating initial wave packet
#2 Operator
#3 Name
   h_ref
#3 OperFile
   ${TESTNAME}_ref.mop
#3 SetInfo
   type=energy

#2 Basis
#3 HoBasis
 40

// Do a VSCF calculation on h_ref
#2 Vscf
#3 Oper
   h_ref
#3 Name
   vscf_ref
// Target the vibrational ground state of the reference potential (S0 state) and place it on the excited electronic state (S2)
// NB: S0 is not included in the operator - only S1 and S2 are coupled.
#3 OccGenCombi
   4^1

// Do MCTDH on VSCF
#2 McTdH
#3 IoLevel
   5
#3 Method
   MCTDH[2,V]
#3 CheckBVector // Niels: Not impl. with MPI
#3 GOptRegularization
   STD 1.e-12
#3 ActiveSpace
   8 15 7 16
#3 Name
   mctdh
#3 Oper
   h_mctdh
#3 InitialWf
   VSCF vscf_ref
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 2.e2
      #5 OutputPoints
         201
      #5 Tolerance
         1.e-12 1.e-12
      #5 InitialStepSize
         1.e-2
#3 Properties
   energy
   halftimeautocorr
   statepopulations

#0 Midas Input End

%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0 MidasOperator
#1 ModeNames
Q1 Q6 Q9 Q10
#1 ElectronicDof
S
#1 Frequencies
4.62306256286198254818E-03 2.71577363589428069299E-03 5.60426900505923974993E-03 4.18574583394260620745E-03 1.0
#1 OperatorTerms
 2.31153128143099127409E-03   Q^2(Q1)
 1.35788681794714034649E-03   Q^2(Q6)
 2.80213450252961987497E-03   Q^2(Q9)
 2.09287291697130310372E-03   Q^2(Q10)
-1.55449559943610388785E-02   |0><0|(S)
 1.55449559943610388785E-02   |1><1|(S)
 1.84959251819430512027E-03   Q^1(Q1)  |0><0|(S)
 3.60363684351546918402E-03   Q^1(Q6)  |0><0|(S)
 5.33636657196493238708E-03   Q^1(Q9)  |0><0|(S)
 6.28413114665659060937E-03   Q^1(Q1)  |1><1|(S)
-4.97769335564114071696E-03   Q^1(Q6)  |1><1|(S)
 1.37662896347225658690E-03   Q^1(Q9)  |1><1|(S)
 7.64532540204933969141E-03   Q^1(Q10) |0><1|(S)
 7.64532540204933969141E-03   Q^1(Q10) |1><0|(S)
-4.25924444384502228801E-04   Q^2(Q10)
 7.93784986946095583735E-05   Q^1(Q1)  Q^1(Q6)  |0><0|(S)
-3.48383410937453080573E-04   Q^1(Q1)  Q^1(Q9)  |0><0|(S)
 1.49937164200929191416E-04   Q^1(Q6)  Q^1(Q9)  |0><0|(S)
-2.19025857509200444842E-04   Q^1(Q1)  Q^1(Q6)  |1><1|(S)
-1.13922845348745198639E-04   Q^1(Q1)  Q^1(Q9)  |1><1|(S)
 1.38912372715566730542E-04   Q^1(Q6)  Q^1(Q9)  |1><1|(S)
 4.06447312760361927336E-04   Q^1(Q1)  Q^1(Q10) |0><1|(S)
 4.06447312760361927336E-04   Q^1(Q1)  Q^1(Q10) |1><0|(S)
 7.34986099024162648809E-04   Q^1(Q6)  Q^1(Q10) |0><1|(S)
 7.34986099024162648809E-04   Q^1(Q6)  Q^1(Q10) |1><0|(S)
 9.26082484770444870278E-05   Q^1(Q9)  Q^1(Q10) |0><1|(S)
 9.26082484770444870278E-05   Q^1(Q9)  Q^1(Q10) |1><0|(S)
#0 MidasOperatorEnd
%EOF%

cat > ${TESTNAME}_ref.mop << %EOF%
#0 MidasOperator
#1 ModeNames
Q1 Q6 Q9 Q10
#1 ElectronicDof
S
#1 Frequencies
4.62306256286198254818E-03 2.71577363589428069299E-03 5.60426900505923974993E-03 4.18574583394260620745E-03 1.0
#1 OperatorTerms
 2.31153128143099127409E-03   Q^2(Q1)
 1.35788681794714034649E-03   Q^2(Q6)
 2.80213450252961987497E-03   Q^2(Q9)
 2.09287291697130310372E-03   Q^2(Q10)
-1.55449559943610388785E-02   |0><0|(S)
 1.55449559943610388785E-02   |1><1|(S)
#0 MidasOperatorEnd
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check autocorrelation function at t_beg and t_end
# ------------------------------------------------------------------------------
CRIT_ITER_S[0]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_beg:                1\.000000000000000.E+00" |  wc -l`
CRIT_ITER_S[1]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_end:                1\.507250149.......E-01" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_S[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''MCTDH auto-correlation function'\'', CRIT_ITER_S[$i].")
done

# ------------------------------------------------------------------------------
# Check S2 state population at t_beg and t_end
# ------------------------------------------------------------------------------
CRIT_ITER_SP0[0]=`$GREP -A2 "Electronic state population 0" $log | grep "\- At t_beg:                1\.000000000000000.E+00" |  wc -l`
CRIT_ITER_SP0[1]=`$GREP -A2 "Electronic state population 0" $log | grep "\- At t_end:                9\.30065764........E-01" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_SP0[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''S2 state population'\'', CRIT_ITER_SP0[$i].")
done

# ------------------------------------------------------------------------------
# Check S1 state population at t_beg and t_end
# ------------------------------------------------------------------------------
CRIT_ITER_SP1[0]=`$GREP -A2 "Electronic state population 1" $log | grep "\- At t_beg:                0\.000000000000000.E+00" |  wc -l`
CRIT_ITER_SP1[1]=`$GREP -A2 "Electronic state population 1" $log | grep "\- At t_end:                6\.98080630........E-02" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_SP1[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''S1 state population'\'', CRIT_ITER_SP1[$i].")
done

# ------------------------------------------------------------------------------
# Check that no B-vector checks failed
# ------------------------------------------------------------------------------
CRIT_ITER_BVEC=`$GREP "B-vector check: Relative difference is larger than expected." $log | wc -l`
TEST+=(${CRIT_ITER_BVEC})
CTRL+=(0)
ERROR+=("Wrong '\''B vector'\'', CRIT_ITER_BVEC.")

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

' > ${TESTNAME}.check
#######################################################################
