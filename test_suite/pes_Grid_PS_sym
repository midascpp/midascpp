#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         H2O
   Wave Function:    Partridge-Schwenke potential
   Test Purpose:     Check setup in Grid
                     normal coordinates
                     check calculation inertia components
                     check addition of Watson terms
                     Harmonic freqs. with Watson term included
                     specific derivatives for energy and inertia components
%EOF%

MAINDIR=$PWD/Dir_pes_Grid_PS_sym
export MAINDIR
rm -rf  $MAINDIR
mkdir $MAINDIR

SAVEDIR=$PWD/Dir_pes_Grid_PS_sym/savedir/
export SAVEDIR

ANALYSISDIR=$MAINDIR/analysis/
export ANALYSISDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Grid_PS_sym.minp <<%EOF%
#0 MIDAS Input
#1 general
#2 IoLevel
2
#2 AnalysisDir
$ANALYSISDIR
#1 singlepoint // here i will give the input for a model singlepoint (and test that i can write a comment)
#2 Name
partridge_water_pot
#2 tYpe
SP_MODEL
#2 PartridgePotential
#1 Pes
#2FITBAS
#3 NAME
poly_fit
#3FitFunctionsMaxOrder
[3*10]
#3FitBasisMaxOrder
[3*10]

#2 symmetry
C2V
#2 IoLevel
18 //only for test reasons
#2 maindir
$MAINDIR
#2 SaveIoDir
$SAVEDIR
#2 multilevelpes
partridge_water_pot 2
#2 PesGridScalFac
1.2 1.2 1.2
#2 StaticInfo
2         // MC Level
32  16   //  Grid points in each MC level
3/4 3/4   //  Fractioning
#2 ExtendedGridSettings
3 0 1 2 8 8 8 3/4 3/4 3/4 3/4 3/4 3/4
#2 EndOfExtendedGridSettings
#2 PesCalcMuTens
#2 Interpolate
 Spline
 Surface
 Natural
#2 PesFGMesh
4 4 4
#2 PesFGScalFac
1.0 1.0 1.0



#0 Midas Input End
%EOF%
#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_Grid_PS_sym.mmol <<%EOF%
#0 MOLECULE INPUT
#1 XYZ
3 Aa
comment
O#1    0.000000000000    0.000000000000 0.0
H#1    0.757390973309    0.586352753497 0.0
H#2   -0.757390973309    0.586352753497 0.0

#1 FREQ
3 cm-1
 1.6494968413962467E+03 A1
 3.8337828001194184E+03 A1
 3.9453552233505602E+03 B2

#1 VIBCOORD
au
#2 COORDINATE
 -2.1856071206933261E-17   -6.8064280142906802E-02   0.0000000000000000E+00
 -4.0955238123917437E-01    5.4025995916333314E-01   0.0000000000000000E+00
  4.0955238123917476E-01    5.4025995916333380E-01   0.0000000000000000E+00
#2 COORDINATE
 -8.8974954529188906E-17    4.8625096195909628E-02   0.0000000000000000E+00
 -5.7328278772467722E-01   -3.8596084656763274E-01   0.0000000000000000E+00
  5.7328278772467856E-01   -3.8596084656763396E-01   0.0000000000000000E+00
#2 COORDINATE
 -6.7575790584075535E-02   -5.4225158422113038E-17   0.0000000000000000E+00
  5.3638134704449603E-01    4.1525288027304541E-01   0.0000000000000000E+00
  5.3638134704449458E-01   -4.1525288027304452E-01   0.0000000000000000E+00
#0 MOLECULE INPUT END
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

mkdir $MAINDIR
mkdir $MAINDIR/Multilevels
mkdir $MAINDIR/Multilevels/level_1
mkdir $MAINDIR/Multilevels/level_1/setup
cat > $MAINDIR/Multilevels/level_1/setup/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

#Basic logic: check Mu tensors are calculated and basic spline and polynomial fitting
CRIT1=`$GREP "Mu tensors will be added to the list of properties" $log | wc -l`
CRIT2=`$GREP "Watson potential energy term will be added to the energy for the state no: 0" $log | wc -l`
CRIT3=`$GREP "Polynomial fitting will use frequency scaled coordinates" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="BASIC INPUT READING IS NOT CORRECT"
echo $CRIT1
echo $CRIT2
echo $CRIT3

# Basic logic: basic grid setup .
CRIT1=`$GREP "Mode combination:  \(0\)" $log | wc -l`                               # 1
CRIT2=`$GREP "Mode combination:  \(1\)" $log | wc -l`                               # 1
CRIT3=`$GREP "Mode combination:  \(2\)" $log | wc -l`                               # 1
CRIT4=`$GREP "Mode combination:  \(0,1\)" $log | wc -l`                             # 1
CRIT5=`$GREP "Mode combination:  \(0,2\)" $log | wc -l`                             # 1
CRIT6=`$GREP "Mode combination:  \(1,2\)" $log | wc -l`                             # 1
CRIT7=`$GREP "Mode combination:  \(0,1,2\)" $log | wc -l`                           # 1
CRIT8=`$GREP "Number of grid points: \(32\)" $log | wc -l`                          # 3
CRIT9=`$GREP "Number of grid points: \(16,16\)" $log | wc -l`                       # 3
CRIT10=`$GREP "Number of grid points: \(8,8,8\)" $log | wc -l`                      # 1
CRIT11=`$GREP "Fractioning:           \(3/4,3/4\)" $log | wc -l`                    # 3
CRIT12=`$GREP "Fractioning:           \(3/4,3/4,3/4,3/4\)" $log | wc -l`            # 3
CRIT13=`$GREP "Fractioning:           \(3/4,3/4,3/4,3/4,3/4,3/4\)" $log | wc -l`    # 1
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13`
CTRL[2]=21
ERROR[2]="GRID SETUP IS NOT CORRECT"
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
echo $CRIT6
echo $CRIT7
echo $CRIT8
echo $CRIT9
echo $CRIT10
echo $CRIT11
echo $CRIT12

# Calculation of inertia tensors: check for reference state
#CRIT1=`$GREP "eigenvalue no.1:  6\.15442766[0-9]" $log | wc -l`
#CRIT2=`$GREP "eigenvalue no.2:  1\.15625969[0-9]" $log | wc -l`
#CRIT3=`$GREP "eigenvalue no.3:  1\.77170245[0-9]" $log | wc -l`
#TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
#CTRL[3]=3
#ERROR[3]="INERTIA TENSORS WRONG"
#echo $CRIT1
#echo $CRIT2
#echo $CRIT3


# Electronic struture calculations
CRIT1=`$GREP "Total : 721" $log | wc -l`
CRIT2=`$GREP "Unique : 457" $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2`
CTRL[3]=2
ERROR[3]="NUMBER OF CALCULATIONS WRONG"
echo "LOL"
echo $CRIT1
echo $CRIT2
echo ${TEST[3]}


#CRIT1=`$GREP "bar\(#1_-7/16\) =    1\.0000000000000000000000E\+00 \*#1_-7/16\*" $log | wc -l`
#TEST[4]=`expr $CRIT1`
#CTRL[4]=1
#ERROR[4]="TRANSFORMED POTENTIALS ARE WRONG"
#echo $CRIT1

CRIT1=`$GREP "Coarse grid has been generated" $log | wc -l`
TEST[5]=`expr $CRIT1`
CTRL[5]=1
ERROR[5]="COARSE GRID HAS NOT BEEN GENERATED"
echo $CRIT1

# Harmonic freqs.
#1         1.6486104710199915643898E+03
#2         3.8262074715538374221069E+03
#3         3.9376974804986011804431E+03

CRIT1=`$GREP "1         1\.648610" $log | wc -l`
CRIT2=`$GREP "2         3\.826207" $log | wc -l`
CRIT3=`$GREP "3         3\.93769" $log | wc -l`

TEST[6]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[6]=3
ERROR[6]="CALCULATED HARMONIC FREQS WRONG"

#Specific derivatives
#Energy
CRIT1=`$GREP "Q\^1\(Q0\) Q\^1\(Q1\) Q\^2\(Q2\) = -2\.238793[0-9]" $log | wc -l`
CRIT2=`$GREP "Q\^1\(Q0\) Q\^1\(Q1\) Q\^4\(Q2\) = -1\.231039[3-4]" $log | wc -l`
CRIT3=`$GREP "Q\^1\(Q0\) Q\^4\(Q1\) Q\^2\(Q2\) = -2\.340726[3-4]" $log | wc -l`
#INERTIA_TRACE
CRIT4=`$GREP "Q\^1\(Q0\) Q\^2\(Q1\) Q\^2\(Q2\) = -1.539422[4-6]" $log | wc -l`
TEST[7]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[7]=4
ERROR[7]="SPECIFIC DERIVATIVES WRONG"

PASSED=1
for i in 0 1 2 3 5 6 7
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Grid_PS_sym.check
#######################################################################
