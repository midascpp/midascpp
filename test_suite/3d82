#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         3-dim ho. test 
   Wave Function:    VSCF 
   Test Purpose:     Check VSCF energy 
   Reference:        Christoffel and Bowman CPL 85, 220 (1982),
                     in a single/few places there are up to 2 diff. on last digit (4 decimal)
                     Differ is of order 1 on last digit (4 decimal) compared to 
                     Norris et al JCP 105, 11261 (1996).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 3d82.minp <<%EOF%
#0 MIDAS Input          

#1 general
#2 IoLevel
    2
#2 Time
#2 BufSIZE
      10000
#2 FileSize
      32767
!#2 Debug

#1 Vib
#2 IoLevel
  5

#2 Operator    // This is the operator input
#3 IoLevel
14
#3 Name
 bowman82
!#3 IgnqiOnly
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
-0.0 DDQ^3(b)
-0.0 DDQ^4(b)
-0.5 DDQ^2(c)
0.245 Q^2(a)
0.845 Q^2(b)
0.5 Q^2(c)
-0.01 Q^3(a)
-0.01 Q^3(b)
-0.1 Q^1(a) Q^2(b)
-0.1 Q^1(b) Q^2(c)
#3 KineticEnergy
 USER_DEFINED

#2 Operator    // This is operator input
#3 Name
aucbowman82
!#3 IgnqiOnly
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
-0.0 DDQ^3(b)
-0.0 DDQ^4(b)
-0.5 DDQ^2(c)
0.245 Q^2(a)
0.845 Q^2(b)
0.5 Q^2(c)
#3 KineticEnergy
 USER_DEFINED

#2 Basis    // This is the basis input
#3 Name
 smallbasis
#3 Define
 a HO n_high=2,omeg= 0.7,Mass=1.0
 b HO n_high=3,omeg= 1.3,Mass=1.0
 c HO n_high=4,omeg= 1.0,Mass=1.0
#3 IoLevel
   9

#2 Basis   // This is the basis input
#3 Name
 largebasis
#3 Define
 a HO n_high=32,omeg= 0.7,Mass=1.0
 b HO n_high=34,omeg= 1.3,Mass=1.0
 c HO n_high=36,omeg= 1.0,Mass=1.0
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 0
#3 OCCUmax
 2 1 1
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2 1 2


#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Oper
aucbowman82
#3 Basis
smallbasis
#3 Occup
 0 0 0
#3 OCCUmax
 2 1 1
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2 1 2

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
largebasis
#3 Occup
 0 0 0
#3 OCCUmax
 2 1 1
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2 1 2


#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Oper
aucbowman82
#3 Basis
largebasis
#3 Occup
 0 0 0
#3 OCCUmax
 2 1 1
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2 1 2

#0 Midas Input End 

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`                                               # 24
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`     #  4
CRIT3=`$GREP "n_high=2,omeg" $log | wc -l`                                          #  1
CRIT4=`$GREP "n_high=3,omeg" $log | wc -l`                                          #  1
CRIT5=`$GREP "n_high=4,omeg" $log | wc -l`                                          #  1
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[1]=31
#echo $CRIT1
#echo $CRIT2
#echo $CRIT3
#echo $CRIT4
#echo $CRIT5
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy: For the almost exact results, check both the 0000 and the 9999 possibility.
CRIT11=`$GREP "E =  1.5000000000000" $log | $GREP " Occ: \[0,0,0\]" | wc -l`
CRIT12=`$GREP "E =  1.4999999999999" $log | $GREP " Occ: \[0,0,0\]" | wc -l`
CRIT21=`$GREP "E =  2.2000000000000" $log | $GREP " Occ: \[1,0,0\]" | wc -l`
CRIT22=`$GREP "E =  2.1999999999999" $log | $GREP " Occ: \[1,0,0\]" | wc -l`
CRIT31=`$GREP "E =  2.5000000000000" $log | $GREP " Occ: \[0,0,1\]" | wc -l`
CRIT32=`$GREP "E =  2.4999999999999" $log | $GREP " Occ: \[0,0,1\]" | wc -l`
CRIT41=`$GREP "E =  2.8000000000000" $log | $GREP " Occ: \[0,1,0\]" | wc -l`
CRIT42=`$GREP "E =  2.7999999999999" $log | $GREP " Occ: \[0,1,0\]" | wc -l`
CRIT51=`$GREP "E =  2.9000000000000" $log | $GREP " Occ: \[2,0,0\]" | wc -l`
CRIT52=`$GREP "E =  2.8999999999999" $log | $GREP " Occ: \[2,0,0\]" | wc -l`
CRIT61=`$GREP "E =  3.2000000000000" $log | $GREP " Occ: \[1,0,1\]" | wc -l`
CRIT62=`$GREP "E =  3.1999999999999" $log | $GREP " Occ: \[1,0,1\]" | wc -l`
CRIT01=`$GREP "E =  1.4950326406276" $log | $GREP " Occ: \[0,0,0\]" | wc -l`
CRIT02=`$GREP "E =  2.1883657293667" $log | $GREP " Occ: \[1,0,0\]" | wc -l`
CRIT03=`$GREP "E =  2.48824970421[8,9][9,0]" $log | $GREP " Occ: \[0,0,1\]" | wc -l`
CRIT04=`$GREP "E =  2.778036688489" $log | $GREP " Occ: \[0,1,0\]" | wc -l`
CRIT05=`$GREP "E =  2.8783852088549" $log | $GREP " Occ: \[2,0,0\]" | wc -l`
CRIT06=`$GREP "E =  3.18150292234[5,6][0,9]" $log | $GREP " Occ: \[1,0,1\]" | wc -l`
CRIT07=`$GREP "E =  1.4951532268184" $log | $GREP " Occ: \[0,0,0\]" | wc -l`
CRIT08=`$GREP "E =  2.1891865352474" $log | $GREP " Occ: \[1,0,0\]" | wc -l`
CRIT09=`$GREP "E =  2.488371213755[6,7]" $log | $GREP " Occ: \[0,0,1\]" | wc -l`
CRIT010=`$GREP "E =  2.7783295454067" $log | $GREP " Occ: \[0,1,0\]" | wc -l`
CRIT011=`$GREP "E =  2.9120794048799" $log | $GREP " Occ: \[2,0,0\]" | wc -l`
CRIT012=`$GREP "E =  3.182338296507[6,7]" $log | $GREP " Occ: \[1,0,1\]" | wc -l`

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
\+ $CRIT04 \
\+ $CRIT05 \
\+ $CRIT06 \
\+ $CRIT07 \
\+ $CRIT08 \
\+ $CRIT09 \
\+ $CRIT010 \
\+ $CRIT011 \
\+ $CRIT012 \
\+ $CRIT11 \
\+ $CRIT12 \
\+ $CRIT21 \
\+ $CRIT22 \
\+ $CRIT31 \
\+ $CRIT32 \
\+ $CRIT41 \
\+ $CRIT42 \
\+ $CRIT51 \
\+ $CRIT52 \
\+ $CRIT61 \
\+ $CRIT62 \
`
echo $CRIT01 \
$CRIT02 \
$CRIT03 \
$CRIT04 \
$CRIT05 \
$CRIT06 \
$CRIT07 \
$CRIT08 \
$CRIT09 \
$CRIT010 \
$CRIT011 \
$CRIT012 \
$CRIT11 \
$CRIT12 \
$CRIT21 \
$CRIT22 \
$CRIT31 \
$CRIT32 \
$CRIT41 \
$CRIT42 \
$CRIT51 \
$CRIT52 \
$CRIT61 \
$CRIT62

CTRL[2]=24
ERROR[2]="VSCF ENERGIES NOT CORRECT"

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 3d82.check
#######################################################################
