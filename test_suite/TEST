#!/bin/sh
##!/bin/ksh
#########################################################################
#
# Shell script for running MIDAS test suite
#
# Original for dalton program 
# by Christof Haettig and Kasper Hald, Sep. 2000
# Adapted for Midas by Ove Christiansen
#
#########################################################################

#########################################################################
# define all the different test suites
#########################################################################

ESSENTIAL="\
git_version_info \
special_unit_tests \
unit_tests \
unit_tests_seed \
"

SHORT="\
1dho \
2dhouc_basic0 \
2dhouc_basic1 \
2dhouc_basic2 \
2dhouc_basic3 \
2dhouc_basic4 \
2dhouc_basic5 \
2dhouc \
2dho \
2dho_scrambled \
hobasis_scale_facts \
2dho_gauss \
2dho_Bsplines \
2dho_gauss_autogen \
2dho_Bsplines_autogen \
2dho_n \
2dho_2 \
2d_bowman78 \
3d82 \
3d82_d \
dup1dho \
dup1dho_coup \
2dho_vci \
formaldehyd_dup2 \
1dho_rsp \
1dho_rsp2 \
2dho_rsp \
2dho_rsp2 \
2dhou_vcirsp \
FranckCondon \
ir_h2o_vscf \
ir_h2o_vci \
ir_h2o_vci_2m_trf_gen \
ir_h2o_vci_2m_trf_vci2h2 \
raman_h2o_vscf \
raman_h2o_vci \
h2o_scalefreqs2 \
ex_state_trans_vci \
qrf_vci_static \
qrf_vci_dynamic \
qrf_vci_static_v3 \
qrf_vci_dynamic_v3 \
2dvci_tfc \
h2o_temp_dip \
h2o_temp_dip2 \
h2o_temp_dip3 \
formaldehyd_temp \
form_vci_twomode \
vcc_rsp_eig_t1 \
v3_h2_vcc2 \
v3_h2_vcc2_tensor \
v3_h2_vcc2_simpleint \
vapt_2d \
lan_water_small \
bandlan_water_small \
h2o_tempspec \
h2o_tempspec_lim \
mc_screen_vcc \
vci_water_dens \
vcc_water_dens \
vci_water_dens_it \
lintdh_1dho_squeeze \
exptdh_1dho_squeeze \
vscf_bsplines_backwards \
vscf_bsplines_unbounded \
vcc_2db_d2 \
formaldehyd_vcc_l \
2dvcc \
2dvcc_so \
vcc_2db \
vcc_2db_d2_min \
vcc_2db_d2_min_no \
3d82_vcc \
3d82_vcc_v3 \
3d82_vcc_no \
3d82_vcc_noun \
form_vcc_t1 \
form_vcc_onemode \
vmp_twomode \
form_vcc_twomode \
form_vcc_twomode_iso1 \
form_vcc_twomode_screen \
v3_vcc2pt3 \
formaldehyd_vcc \
formaldehyd_vcc_so \
formaldehyd_vcc_noun \
vcc_num_jac \
h2o_scalefreqs \
h2o_scalefreqs3 \
h2o_scalefreqs4 \
ext_range_nh3 \
formaldehyd \
formaldehyd_n \
formaldehyd_2 \
2dho_fvci \
2dho_fvci2 \
2dho_fvci3 \
3d82_fvci \
3d82_exphvci \
3d82_exphvci_maxexprmode \
3d82_exph_bt \
dup1dho_100 \
2dvci \
2dvmp \
4dvmp \
6dvmp \
2dvci_t \
2dvcih2 \
2dvcih2_t \
2dvcih2_tn \
formaldehyd_rsp \
tdvcc_2m_water_vccgs \
matrep_evcc_water \
tdevcc_water_imag \
tdvcc_compare_exotic \
mctdh_wf_initialization \
mctdh_ir_water \
tdvcc_2m_water_vccgs \
tdevcc_water_imag \
tdvcc_compare_exotic \
"

VCC2M=" \
unit_tests \
form_vcc_onemode \
form_vcc_twomode \
form_vcc_twomode_iso1 \
form_vcc_twomode_screen \
form_vci_twomode \
vmp_twomode \
ir_h2o_vci_2m_trf_vci2h2 \
vcc2_left_transf_2m \
"

# TDVCC2M also contained in SHORT/LONG
TDVCC2M=" \
tdvcc_2d_henonheiles \
tdvcc_2m_water \
tdvcc_2m_water_imag \
tdvcc_2m_water_pulse \
tdvcc_2m_water_vccgs \
tdvcc_restart_2m_water \
"

MCTDH=" \
mctdh_wf_initialization \
mctdh_ir_water \
mctdh2g0_ivr \
mctdh2d_ivr \
mctdh2v_ivr \
mctdh_exact_limit \
mctdh_imag \
mctdh_pulse \
mctdh2v_nonadiabatic \
mrmctdh2v_flux \
"

# TDMVCC also contained in SHORT/LONG
TDMVCC=" \
tdmvcc2h2_2d_hh \
tdmvcc2h2_2m_water \
tdmvcc2_matrep_2d_hh \
tdmvcc_matrep_2m_water \
tdmvcc_restart_2m_water \
tdmvcc_2h2_vs_matrep_2m_water \
tdmvcc_full_active_basis_2m_water \
"

# GENTIMTDVCC also contained in SHORT/LONG
GENTIMTDVCC=" \
timtdvcc2_gentrf_2m_water \
timtdvcc2_gentrf_3m_water \
"

# TEST not working in LONG: vci_sos_chfclbr ?
LONG=" \
6dvci \
2dho_vcirsp \
2dho_vccrsp \
vcc_2db_d3 \
formaldehyd_d2_iso1_vcc_noun \
formaldehyd_d2_vcc_noun \
formaldehyd_vcc_no \
v3_h3_vcc3 \
v3_h3_vcc2pt3_tensor \
v3_h3_vcc3_tensor \
v3_h3_vcc3_tensor_crop3 \
v3_h3_vcc3_tensor_rjac2 \
v3_h3_vcc3_tensor_nr \
v3_h3_vcc3_rsp \
v3_h3_vcc3_rsp_tensor \
v3_h3_vcc4 \
v3_h3_vcc4_tensor \
v3_h3_vcc3pt4 \
v3_h3_vcc3pt4F \
vcc2_left_transf_2m \
vcc2_left_transf_v3 \
watson_h2o_vci \
fvci_h2o_psc_keo \
excor_h2o_vci \
excor_h2o_vci_r2 \
simpcor_h2o_vci \
simpcor_co2_vci \
lintdh_2d_henon_heiles \
lintdh_2d_henon_heiles_qbasis \
exptdh_2d_henon_heiles \
excor_co2_vci \
nhlan_water_small \
nhbandlan_water_small \
vci_water_cubic_rsp \
vcc_water_dens_it \
v3_h2_vcc2_rsp \
cpvcc3_gs \
formaldehyd_vcc_no_l \
formaldehyd_vcc_noun_l \
cpvcc2pt3_rsp \
vcc_rsp_eig \
2dvcc_bs \
lan_water \
nhbandlan_water \
vci_lan_water_qr \
nhlan_water \
bandlan_water \
formaldehyd_rsp2 \
4dvci \
tdmvcc2h2_2d_hh \
tdmvcc2h2_2m_water \
tdmvcc2_matrep_2d_hh \
tdmvcc_matrep_2m_water \
tdvcc_2d_henonheiles \
tdvcc_2m_water \
tdvcc_2m_water_imag \
tdvcc_2m_water_pulse \
mctdh2g0_ivr \
mctdh2d_ivr \
mctdh2v_ivr \
mctdh_exact_limit \
mctdh_imag \
mctdh_pulse \
mctdh2v_nonadiabatic \
mrmctdh2v_flux \
timtdvcc2_gentrf_2m_water \
timtdvcc2_gentrf_3m_water \
delta_function_bsplines \
"

PES_MIDAS=" \
pes_Grid_PS \
pes_NumDer_PS \
pes_Adga \
pes_Adga_sym \
pes_Adga_modeorder \
pes_Adga_Filepot \
pes_Adga_PS \
pes_Adga_PS_orthonormal \
pes_Adga_Morse \
pes_Adga_AutoFit \
pes_Adga_DynamicExt \
pes_Adga_DoubleWell \
pes_MultilevelAdga_Morse \
pes_Adga_Update \
pes_Adga_ExtMcr \
pes_Adga_Msi \
pes_Adga_Dens \
pes_Adga_midaspot_ammonia_cs \
pes_Adga_midaspot_ammonia_cs_nonstdrot_a \
pes_Adga_midasdynlib_ammonia_cs \
pes_Restart_Difact \
pes_extrap1 \
pes_Adga_MultiState \
"

PES_DALTON=" \
pes_Grid_1 \
pes_Grid_2 \
pes_Grid_3 \
pes_Grid_3_sym \
pes_Grid_4 \
pes_Grid_5 \
pes_Grid_6 \
pes_NumDer_1 \
pes_NumDer_2 \
pes_NumDer_3 \
pes_ScreenMC_1 \
pes_ScreenMC_2 \
pes_c2h4_sym \
pes_c2h2f2_sym \
pes_Adga_Msi_Extrap \
pes_MultilevelAdga_sym \
"

PES_LONG=" \
pes_MultilevelAdga \
pes_MultilevelAdga_8 \
pes_MAdga_Extr1_2 \
pes_Adga_MultiState \
pes_Adga_Dif_Long \
"

PES_GENERIC_ORCA=" \
pes_Adga_Dif \
pes_Adga_GPR \
pes_Grid_Dif \
pes_Grid_Difact \
"

PES_GENERIC_CFOUR=" \
pes_generic_cfour_ccsd_dynpol_hsif \
pes_generic_cfour_ccsdpt_dip_water_sym \
"

PES_GENERIC_MOLPRO=" \
pes_generic_molpro_ccsdpt_water_sym \
pes_generic_molpro_mp2_quad_transdichlorethane_sym \
"

PES_GENERIC_TURBOMOLE=" \
pes_generic_turbomole_cc2_formaldehyd_sym \
pes_generic_turbomole_rimp2_extrapol_water_sym \
"

PES_POLYSPHERICAL=" \
pes_Adga_PsC
pes_Adga_PsC_InactiveModes
"

SYSTEM=" \
h2o_rot_vscf \
h2o_rot_vscfsa \
h2o_rot_vscffusa \
h2o_rot_hybrid \
h2o_rot_holcs \
falcon_preprun \
falcon_usehess_full \
falcon_usehess_incrsetup \
falcon_rotcoord \
falcon_calchess_incrsetup \
molfiles_convert \
normalcoord_calc \
"

FREQANA_TURBOMOLE=" \
freqana_hessian \
freqana_target_methanol \
"

MLEARN=" \
ml_gpr_water \
ml_gpr_hessian \
ml_gpr_pes \
ml_soap \
ml_gpr_sparse \
ml_gpr_symg2 \
"

#
# Internal fast tests for checking MidasCpp installation
# 
INSTALL=" \
2dho_Bsplines_autogen \
bandlan_water_small \
nhbandlan_water_small \
v3_h3_vcc3_tensor_crop3 \
pes_Adga_sym \
pes_Grid_PS \
pes_NumDer_PS \
pes_Adga_Msi \
pes_MultilevelAdga_Morse \
"

#
# Saved for reason of having a handle back in history, but may not work now, (can be tricky!).
#
HISTORIC=" \
pes_MultilevelAdga_historic \
pes_MultilevelAdga_historic_8 \
pes_MultilevelAdga_sym_historic \
pes_Adga_sym_historic \
pes_Adga_Msi_Extrap_historic \
pes_MAdga_Extr1_2_historic \
pes_generic_cfour_ccsd_dynpol_hsif_historic \
pes_generic_cfour_ccsdpt_dip_water_sym_historic \
pes_generic_molpro_ccsdpt_water_sym_historic \
pes_generic_molpro_mp2_quad_transdichlorethane_sym_historic \
pes_generic_turbomole_cc2_formaldehyd_sym_historic \
pes_generic_turbomole_rimp2_extrapol_water_sym_historic \
" 

#
# Super-sets of tests
#
INSTALLSHORT="$ESSENTIAL $INSTALL"
INSTALLLONG="$ESSENTIAL $SHORT $LONG $SYSTEM $PES_MIDAS"
ALLWF="$ESSENTIAL $SHORT $LONG"  # VCC2M is contained in SHORT and LONG
ALLTDWF="$TDVCC2M $TDMVCC $GENTIMTDVCC"
COREPES="$ESSENTIAL $PES_MIDAS $PES_DALTON"
ALLPES="$ESSENTIAL $PES_MIDAS $PES_DALTON $PES_LONG"
PESGENERIC="$PES_GENERIC_ORCA $PES_GENERIC_CFOUR $PES_GENERIC_MOLPRO $PES_GENERIC_TURBOMOLE $FREQANA_TURBOMOLE"
ALL="$ALLWF $ALLPES $PESGENERIC $SYSTEM $HISTORIC $MLEARN"

#########################################################################
# function usage(): print usage information 
#########################################################################
usage() {
 cat << %EOF%
usage: TEST [-h|-help|--help] [-nokeep] [-reftest] [-bell] [-benchmark]
            [-param "option list"] [-midas script] [-out outfile] testcase

       -h | -help | --help  : show this help description
       -nokeep              : do not keep *.minp  *.out, *.check files
       -reftest             : test reference file(s); do not run any
                              calculations
       -bell                : sound bell each time a test fails
       -benchmark           : print CPU time for each testcase as part of
                              the summary
       -param "option list" : pass "option list" to midas-script
       -out outfile         : write output into outfile instead of the
                              default "TESTLOG"

       where testcase might be one of the following:
          essential            -- run essential tests
          short                -- run all short test cases
          long                 -- run all long test cases
          installshort         -- run short tests for checking installation
          installlong          -- run long tests for checking installation
          vcc2m                -- run test cases for VCC[2H2]
          mctdh                -- run all MCTDH tests
          allwf                -- run all wave-function test cases
          corepes              -- run all core PES test cases
          allpes               -- run all PES test cases
          pesgeneric           -- run generic PES test cases
          mlearn               -- run all Machine Learning test cases
          all                  -- run all non-parallel test cases
                                  (= short + medium + long)
          <case1 [case2] ...>  -- run only the specified test case(s)
%EOF%
exit 1
}

#########################################################################
# function mypring(string): print to stdout and $listing
#########################################################################
myprint(){
 echo "$1";
 echo "$1" >> $listing;
}

#########################################################################
#########################################################################
# start with real work:
#########################################################################
#########################################################################

#########################################################################
# set defaults and evaluate parameters
#########################################################################
help=""
keep="true"
MIDAS="../scripts/midas"
paramlist=""
reftest=""
bell=""
benchmark=""
listing="TESTLOG"
emptystring="                                "

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


while [ -n "`echo $1 | grep '-'`" ]; do
   case $1 in
     "-h" | "-help" | "--help" ) help="true" ; break;;
     "--midas" )  shift; MIDAS=$1; shift;;
     "--out" ) shift; listing=$1; shift;;
     "--nokeep" ) keep="false"; shift;;
     "--param" ) shift; paramlist="$1"; shift;;
     "--reftest" ) reftest="true"; shift;;
     "--bell" ) bell="true"; shift;;
     "--benchmark" ) benchmark="true"; shift;;
     "--timings" ) timings="true"; shift;;
     * ) usage;;
   esac
done
if [ $help ]; then
  usage
fi

#########################################################################
# set list of test cases:
#########################################################################
testcase=$*
if   [ "$testcase" = "essential" ]; then
  testcase_category=$testcase
  testcase=$ESSENTIAL
elif [ "$testcase" = "short" ]; then
  testcase_category=$testcase
  testcase=$SHORT
elif [ "$testcase" = "long" ]; then
  testcase_category=$testcase
  testcase=$LONG
elif [ "$testcase" = "vcc2m" ]; then
  testcase_category=$testcase
  testcase=$VCC2M
elif [ "$testcase" = "tdvcc2m" ]; then
  testcase_category=$testcase
  testcase=$TDVCC2M
elif [ "$testcase" = "tdmvcc" ]; then
   testcase_category=$testcase
   testcase=$TDMVCC
elif [ "$testcase" = "mctdh" ]; then
  testcase_category=$testcase
  testcase=$MCTDH
elif [ "$testcase" = "timtdvcc2m" ]; then
  testcase_category=$testcase
  testcase=$TIMTDVCC2M
elif [ "$testcase" = "gentimtdvcc" ]; then
   testcase_category=$testcase
   testcase=$GENTIMTDVCC
elif [ "$testcase" = "allwf" ]; then
  testcase_category=$testcase
  testcase=$ALLWF
elif [ "$testcase" = "alltdwf" ]; then
  testcase_category=$testcase
  testcase=$ALLTDWF
elif [ "$testcase" = "pesmidas" ]; then
  testcase_category=$testcase
  testcase=$PES_MIDAS
elif [ "$testcase" = "pesdalton" ]; then
  testcase_category=$testcase
  testcase=$PES_DALTON
elif [ "$testcase" = "peslong" ]; then
  testcase_category=$testcase
  testcase=$PES_LONG
elif [ "$testcase" = "corepes" ]; then
  testcase_category=$testcase
  testcase=$COREPES
elif [ "$testcase" = "allpes" ]; then
  testcase_category=$testcase
  testcase=$ALLPES
elif [ "$testcase" = "pesgenericorca" ]; then
  testcase_category=$testcase
  testcase=$PES_GENERIC_ORCA
elif [ "$testcase" = "pesgenericcfour" ]; then
  testcase_category=$testcase
   testcase=$PES_GENERIC_CFOUR
elif [ "$testcase" = "pesgenericmolpro" ]; then
  testcase_category=$testcase
  testcase=$PES_GENERIC_MOLPRO
elif [ "$testcase" = "pesgenericturbomole" ]; then
  testcase_category=$testcase
  testcase=$PES_GENERIC_TURBOMOLE
elif [ "$testcase" = "freqanaturbomole" ]; then
  testcase_category=$testcase
  testcase=$FREQANA_TURBOMOLE
elif [ "$testcase" = "pesgeneric" ]; then
  testcase_category=$testcase
  testcase=$PESGENERIC
elif [ "$testcase" = "pespolyspherical" ]; then
  testcase_category=$testcase
  testcase=$PES_POLYSPHERICAL
elif [ "$testcase" = "system" ]; then
  testcase_category=$testcase
  testcase=$SYSTEM
elif [ "$testcase" = "installshort" ]; then
  testcase_category=$testcase
  testcase=$INSTALLSHORT
elif [ "$testcase" = "installlong" ]; then
  testcase_category=$testcase
  testcase=$INSTALLLONG
elif [ "$testcase" = "historic" ]; then
  testcase_category=$testcase
  testcase=$HISTORIC
elif [ "$testcase" = "mlearn" ]; then
  testcase_category=$testcase
  testcase=$MLEARN
elif [ "$testcase" = "all" ]; then
  testcase_category=$testcase
  testcase=$ALL
fi

#########################################################################
# check file for test listing:
#########################################################################
#if [  -s $listing ]; then
#   echo "$listing already exists... should it be deleted first? (y/n)"
#   read answer
#   if [ "$answer" = "yes"  -o  "$answer" = "y" ]; then
      echo > $listing
#   fi
#fi

myprint "#####################################################################"
myprint "                          MIDAS test suite"
myprint "#####################################################################"
myprint "midas script          : $MIDAS"
myprint "parameter list passed : $paramlist"
myprint "test listing          : $listing"
myprint "test cases            : $testcase"

#########################################################################
# loop over test cases:
#########################################################################
passedall="ALL TESTS ENDED PROPERLY!"
problems=""
numresult=0
for item in ${testcase}
do
   #clear tmpouts directory if it exists
   if [ -x tmpouts/$item ]; then
      echo "Removing all contents of tmpouts/$item..."
      rm -rf tmpouts/$item
   else
      echo "Making directory tmpouts..."
      mkdir tmpouts
  fi
  mkdir tmpouts/$item
  mkdir tmpouts/$item/savedir
  mkdir tmpouts/$item/analysis
  trouble=0
  myprint "###########################################################"
  myprint "start now with test $item:"
  myprint "-----------------------------------------------------------"
  chmod +x ./$item
  # mbh: possible NON-CLEAN termination => WARNING and SKIP!
  #      132 is used as error exit status
  ./$item
  if [ "$?" = "132" ]; then
    echo "TEST $item finished NON-CLEAN! Investigate!"
    if [ `echo $passedall | grep -c PROPER` -eq 1 ]; then
      passedall="THERE IS A PROBLEM IN TEST CASE"
    fi
    passedall="$passedall $item"
    problems="$problems$item\n"
    continue
  fi
  if [ -f $item.info ]; then
    cat $item.info | tee -a $listing
  fi
# Check if this is a multistep job
  numsteps=1
  if [ -r ./$item'__'1.dal ]; then
      multi=1
      numsteps=`ls ./$item\_\_[0-9].dal | wc -l`
      myprint "   This is a multi-step job consisting of $numsteps steps"
      rm -f ./$item'__'[0-9]'_'$item.mout
      chmod +x ./$item'__'[0-9].check
  else
      multi=0
      rm -f ./$item.mout
      chmod +x ./$item.check
  fi
# Multi-step tests loop over the different jobsteps, regular tests just
# go through this once
  step=1
  while [ "$step" -le "$numsteps" ]
  do
      if [ "$multi" -eq 1 ]; then
          if [ "$step" -gt 1 ]; then
              myprint ""
              myprint "start $item step $step:"
              myprint "-----------------------------------------------------------"
          fi
          inpfile=$item
          outfile=./$item'__'$step'_'$item.mout
          checkfile=./$item'__'$step.check
          reffile=./$item'__'$step.ref
          moldenfile=./$item'__'$step'_'$item.molden
      else
          inpfile=$item
          outfile=./$item.mout
          checkfile=./$item.check
          reffile=./$item.ref
          moldenfile=./$item.molden
      fi
# If it's a reftest, no calculation uis performed
      if [ "$reftest" = "true" ]; then
          myprint ""
          myprint "evaluate reference output file $reffile:"
          myprint "-----------------------------------------------------------"
          compressed="false"
          if [ ! -r $reffile ]; then
              if [ -r $reffile.gz ]; then
                  compressed="true"
                  gunzip -f $reffile.gz
              fi
          fi
          checkout=`$checkfile $reffile | tee -a $listing`
          if [ "$benchmark" = "true" ]; then
              if [ `$GREP "CPU  time used in MIDAS" $reffile | wc -l` = 1 ]; then
                  CPU_usage=`$GREP "CPU  time used in MIDAS" $reffile | sed s/"Total CPU  time used in Midas:"/""/`
              else
                  CPU_usage="N/A"
              fi
          fi
          if [ "$compressed" = "true" ]; then
              gzip -f --best $reffile
          fi
      else
          if [ "$step" -eq "$numsteps" ]; then
              $MIDAS $paramlist $inpfile #| \
              status=$?
              #grep -v '\*\*\**' | \
              #grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
              #grep -v '^$' | tee -a $listing
          else
              $MIDAS -D $paramlist $inpfile #| \
              status=$?
              #grep -v '\*\*\**' | \
              #grep -v 'OUTPUT FROM' | grep -v 'Version' | grep -v 'PID' | \
              #grep -v '^$' | tee -a $listing
          fi
          if [ "$status" -eq 0 ]; then
            myprint ""
            myprint "evaluate output file $outfile:"
            myprint "-----------------------------------------------------------"
            checkout=`$checkfile $outfile | tee -a $listing`
            if [ "$benchmark" = "true" ]; then
                if [ `$GREP "CPU  time used in MIDAS" $outfile | wc -l` = 1 ]; then
                    CPU_usage=`$GREP "CPU  time used in MIDAS" $outfile | sed s/"Total CPU  time used in Midas:"/""/`
                else
                    CPU_usage="N/A"
                fi
            fi
          else
             myprint ""
             myprint "Exit status != 0, no need to evaluate output file."
             myprint "THERE IS A PROBLEM"
          fi
      fi
      if [ "$benchmark" = "true" ]; then
          numresult=`expr $numresult \+ 1`
          if [ $step -eq 1 ]; then
              testlist[$numresult]="$item"
          else
              testlist[$numresult]="...step$step"
          fi
          timelist[$numresult]="$CPU_usage"
      fi
      echo "$checkout"
      if [ "$status" -eq 0 ]; then
        passed=`echo $checkout | grep "TEST ENDED PROPERLY"`
      else
        passed=
      fi
      if [ -n "$passed" ]; then
        if [ "$keep" = "false" ]; then
          rm -f $checkfile $outfile $item.info
        fi
      else
        trouble=`expr $trouble \+ 1`
        if [ "$bell" = "true" ]; then
          echo "\a"
        fi
        passedall="THERE IS A PROBLEM IN TEST CASE"
        if [ "$trouble" -eq 1 ]; then
            problems="$problems$item\n"
        fi
      fi
    step=`expr $step \+ 1`
  done
  if [ "$trouble" -eq 0 ]; then
      if [ "$keep" = "false" ]; then
          rm -f $inpfile.minp
      fi
  fi
  myprint ""
done

#########################################################################
# final result:
#########################################################################

myprint "#####################################################################"
myprint "                              Summary"
myprint "#####################################################################"
myprint ""

if [ "$benchmark" = "true" ]; then
    if [ "$numresult" -gt 0  ]; then
        ind=1
        while [ "$ind" -le "$numresult" ]
        do
          namelength=`expr length ${testlist[$ind]}`
          spclength=`expr 32 \- $namelength`
          spc=`expr substr "$emptystring" 1 $spclength`
          myprint "\t${testlist[$ind]}$spc${timelist[$ind]}"
          ind=`expr $ind \+ 1`
        done
        echo
    fi
fi

echo $passedall | tee -a $listing
printf "$problems" | tee -a $listing

if [ "$timings" = "true" ]; then
   myprint ""
   chmod +x TIMINGS.py
   python TIMINGS.py $testcase_category $testcase
   myprint "Made timings of the calculations. Open TIMINGS_$testcase_category.pdf to see plot of timings."
fi

if [ "$passedall" = "ALL TESTS ENDED PROPERLY!" ]; then
   exit 0
else
   exit 1
fi
