#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test - in reality uncoupled
   Wave Function:    VSCF / 32 32 HO basis 
   Test Purpose:     Check VSCF energy of direct calculated lowest states
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > dup1dho.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  11

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
0.98 Q^2(a)
#3 KineticEnergy
 USER_DEFINED
#3 Duplicate
 3
//Note 2*0.49 = 0.98 = 0.5*(1.4)**2

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a HO n_high=7,omeg=1.4
#3 Duplicate
 3


#2 Vscf      // This a the VSCF input
#3 Occup
 0 
#3 OCCUmax
 5 
#3 Duplicate
 3
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=11
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT01=`$GREP "Occ: " $log | $GREP " =  1\.400000000000[0-9][0-9]" | wc -l`
CRIT11=`$GREP "Occ: " $log | $GREP " =  1\.399999999999[0-9][0-9]" | wc -l`
CRIT02=`$GREP "Occ: " $log | $GREP " =  2\.799999999999[0-9][0-9]" | wc -l`
CRIT12=`$GREP "Occ: " $log | $GREP " =  2\.800000000000[0-9][0-9]" | wc -l`

#echo $CRIT01
#echo $CRIT11
#echo $CRIT02
#echo $CRIT12
TEST[2]=`expr $CRIT01 \+ $CRIT11 \+ $CRIT02 \+ $CRIT12 `
CTRL[2]=9
ERROR[2]="VSCF ENERGY NOT CORRECT"

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > dup1dho.check
#######################################################################
