#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         HF  
   Wave Function:    Dalton HF WF 
   Test Purpose:     Check setup in Grid
                     normal coordinates
                     VibPol
                     Harmonic freqs.
                     specific derivatives
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_pes_Grid_4
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Grid_4.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
    2
#2 MainDir
$MAINDIR

#1 singlepoint
#2 Dalton Input
**DALTON
.RUN WAVEFUNCTION
.RUN PROPERTIES
**INTEGRALS
.DIPLEN
**WAVE FUNCTIONS
.HF
*HF INP
.THRESH
1.0D-12
**END OF
#2 End of Dalton Input
#2 DaltonBasis
1  cc-pVDZ
9  cc-pVDZ
#2 Name
Dalton
#2 Type
SP_DALTON

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 FitBasis
#3name
fitbas_0
#3 FitFunctionsMaxOrder
8
#3 FitBasisMaxOrder
8

#2 USEFITBASIS
fitbas_0
#2 IoLevel
18 //only for test reasons
#2 VibPol
#2 multilevelpes
    Dalton 1
#2 StaticInfo
    1      // MC Level
    32     //  Grid points in each MC level
    1      //  Fractioning
#2 PesGridScalFac
1.2
#2 PesFGMesh
10
#2 Interpolate
 Spline
 Surface
 Natural
#2 PesFGScalFac
0.9

#0 Midas Input End
%EOF%
#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_Grid_4.mmol <<%EOF%
#0 Midas Molecule
#1 XYZ
2 au
comment
 H          0.0000000000            0.0000000000            1.6177435032
 F          0.0000000000            0.0000000000           -0.0858178630

#1 FREQ
1 cm-1
 0.4440827363279131E+04

#1 VIBCOORD
au
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00 9.7069630697167630E-01
 0.0000000000000000E+00 0.0000000000000000E+00-5.1493381079121707E-02

#0 Midas Molecule End
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

# Basic logic: basic grid setup .
CRIT1=`$GREP "Mode combination:  \(0\)" $log | wc -l`                               # 1
CRIT2=`$GREP "Number of grid points: \(32\)" $log | wc -l`                          # 1
CRIT3=`$GREP "Fractioning:           \(1,1\)" $log | wc -l`                         # 1
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="GRID SETUP IS NOT CORRECT"

# Electronic struture calculations

# MBH: Cannot check for these any longer since the code giving individual
# numbers of SPCs for mode combination levels has been altered. So we shall
# just check for total/unique numbers.
#CRIT1=`$GREP "Number of 1-mode calculations \(including reference\) in this iteration: 33" $log | wc -l`

CRIT2=`$GREP -A3 "Evaluating list of singlepoints" $log | $GREP "New : 33[[:space:]]" | wc -l`
CRIT3=`$GREP -A3 "Evaluating list of singlepoints" $log | $GREP "Unique : 33[[:space:]]" | wc -l`
TEST[2]=`expr $CRIT2 \+ $CRIT3`
CTRL[2]=2
ERROR[2]="NUMBER OF CALCULATIONS WRONG"


# Properties
#CRIT1=`$GREP "I found 4 properties in file" $log | wc -l`
#TEST[3]=`expr $CRIT1`
#CTRL[3]=1
#ERROR[3]="NO. OF PROPERTIES IS WRONG"

CRIT1=`$GREP "bar\(\*#1_-3/4\*\) =    1.0E\+00 \*#1_-3/4\*" $log | wc -l`
TEST[4]=`expr $CRIT1`
CTRL[4]=1
ERROR[4]="TRANSFORMED POTENTIALS ARE WRONG"

CRIT1=`$GREP "Coarse grid has been generated" $log | wc -l`
TEST[5]=`expr $CRIT1`
CTRL[5]=1
ERROR[5]="COARSE GRID HAS NOT BEEN GENERATED"

CRIT1=`$GREP "bar\(" $log | wc -l`
TEST[6]=`expr $CRIT1`
CTRL[6]=33
ERROR[6]="NO. OF TRANSFORMED POTENTIALS IS WRONG"

# Harmonic freqs.
#  4.44329483162702945E+03
CRIT1=`$GREP "1         4\.4432948[0-9]" $log | wc -l`
TEST[7]=`expr $CRIT1`
CTRL[7]=1
ERROR[7]="CALCULATED HARMONIC FREQS WRONG"

#Vibrational polarizability.
#Isotropic vibrational polarizability is: 8.1003636644313981E-02
CRIT1=`$GREP "Isotropic vibrational polarizability is: 8\.10036[0-9]" $log | wc -l`
TEST[8]=`expr $CRIT1`
CTRL[8]=1
ERROR[8]="ISOTROPIC VIBRATIONAL POLARIZABILITY WRONG"

# Specific derivatives
# energy
#V_1 = 5.01193440234029512E-05
#V_1_1 = 1.01281948697654490E-02
#V_1_1_1 = -2.23712100333245054E-03
#V_1_1_1_1 = 3.32779016864675570E-04
CRIT1=`$GREP "Q\^1\(Q0\) = \+5.011934[0-9]" $log | wc -l`
CRIT2=`$GREP "Q\^2\(Q0\) = \+1.012819[0-9]" $log | wc -l`
CRIT3=`$GREP "Q\^3\(Q0\) = \-2.237121[0-9]" $log | wc -l`
CRIT4=`$GREP "Q\^4\(Q0\) = \+3.327790[0-9]" $log | wc -l`
TEST[9]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[9]=4
ERROR[9]="SPECIFIC DERIVATIVES WRONG"

#  Calculated Harmonic vibrational frequencies in cm^-1:
#  *****************************************************
# 1         4.4432948315704288688721E+03
CRIT1=`$GREP "1         4.443294831.............E\+03" $log | wc -l`
TEST[10]=`expr $CRIT1`
CTRL[10]=1
ERROR[10]="HARMONIC FREQUENCIES WRONG"

PASSED=1
for i in 0 1 2 4 5 6 7 8 9 10
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Grid_4.check
#######################################################################
