#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy  in various 
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980), for operator
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals

                     Test covers both the 
                     FtesttranExpH (primitive full basis explicit H CI)
                     ExpHtesttran option.
                     In the later case both the full testtran which gives results
                     equivalent to the first, and in 
                     addition the single excited case, which in the modal
                     basis, gives the HF result for the reference state, 
                     which I thing is a nice check.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dvmp.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
  5
#2 BufSIZE
 4
#2 FileSize
 30
#2 Time
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
   5

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a0)
-0.5 DDQ^2(b0)
0.98 Q^2(a0)
0.245 Q^2(b0)
0.0064 Q^3(a0)
-0.08 Q^1(a0) Q^2(b0)
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a0 HO n_high=8,omeg=1.4
 b0 HO n_high=8,omeg=0.7
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Occup
 0 0 

#2 Vcc       // This a the VCC  input
#3 LimitModalBasis
 3 3 
#3 IoLevel
 25
#3 Basis
basis
#3 Occup
 0 0
#3 MaxExci
 2
#3 Vmp 
 6
#3 TimeIt
#3 Pade
#3 VecStorage
 1 
#3 Name
 Vmp_store_1

#2 Vcc       // This a the VCC  input
#3 LimitModalBasis
 3 3 
#3 IoLevel
11
#3 Basis
basis
#3 Occup
 0 0
#3 Vmp 
 6
#3 VecStorage
 2 
#3 Name
 Vmp_store_2

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=7
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# FVCI energy:
CRIT01=`$GREP "O_2"  $log | grep "8.41532424528[7,8]"  | wc -l`
CRIT02=`$GREP "O_6"  $log | grep "1.0485009227342"  | wc -l`
CRIT03=`$GREP "O_13" $log | grep "1.0485009222934"  | wc -l`

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
`
CTRL[2]=10
ERROR[2]="VMP ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02
echo $CRIT03

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dvmp.check
#######################################################################
