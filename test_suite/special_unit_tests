#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > special_unit_tests.info <<%EOF%
   Test Purpose: 
   Check that a few special unit test drivers function properly:
   - TestDrivers:       Must work for new unit tests to be added; this input
                        also tests that tests can be run individually.
   - UnitTestTemplate:  The templates to be copied when adding new unit tests;
                        asserts that somebody hasn't accidentally deleted/
                        modified them.
   - PlaygroundRun:     For testing experimental code.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > special_unit_tests.minp <<%EOF%
#0 Midas Input

#1 Test
   TestDrivers
   UnitTestTemplate
   PlaygroundRun

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check that no tests failed
# ------------------------------------------------------------------------------
TEST+=($($GREP -c "UNIT TEST FAILED!" $log))
CTRL+=(0)
ERROR+=("A unit test has failed.")

# ------------------------------------------------------------------------------
# Check "Test flags" input summary
# ------------------------------------------------------------------------------
# First find the lines for the beginning and end of the "Test flags" section.
test_flags_beg=$($GREP -n "Test flags" $log | awk -F: '\''{print $1}'\'' | head -n 1)
test_flags_end=$($GREP -n "\+={70}\+" $log | awk -F: '\''($1 > '\''$test_flags_beg'\''){print $1}'\'' | head -n 1)
# Print only those lines, using sed, then pipe to grep for stuff.
# sed output should be something like this:
#     | Test flags                                                           |
#     ...
#     | pesintegrals                     | false                             |
#     | playgroundrun                    | true                              |
#     | polyfitnd                        | false                             |
#     ...
#     | testdrivers                      | true                              |
#     | typetraits                       | false                             |
#     | unittesttemplate                 | true                              |
#     ...
#     +======================================================================+

check_tests=(testdrivers unittesttemplate playgroundrun)

TEST+=($(sed -n "${test_flags_beg},${test_flags_end}p" $log | $GREP -c "\<true\>"))
CTRL+=(${#check_tests[@]})
ERROR+=("Input summary error; wrong number of enabled unit tests.")

for t in ${check_tests[@]}
do
   TEST+=($(sed -n "${test_flags_beg},${test_flags_end}p" $log | $GREP -c "\<$t\>.*\<true\>"))
   CTRL+=(1)
   ERROR+=("Input summary error; '\''$t'\'' not listed as '\''true'\''.")
done

# ------------------------------------------------------------------------------
# Check individual tests are run
# ------------------------------------------------------------------------------
check_names=("TestDrivers test" "UnitTestTemplate test" "PlaygroundRun")
check_num_runs=(4 4 1)

for i in $(seq 0 $(expr ${#check_names[*]} - 1))
do
   TEST+=($($GREP -c "NAME: ${check_names[$i]}" $log))
   CTRL+=(1)
   ERROR+=("Test output error; '\''NAME: ${check_names[$i]}'\'' not found.")
   back="$(expr ${#TEST[*]} - 1)"

   # If above test passed, do specific test(s) for that part.
   if test ${TEST[$back]} -eq ${CTRL[$back]}
   then
      # Find [beg;end) of that test block.
      beg=$($GREP -n "NAME: ${check_names[$i]}" $log | awk -F: '\''{print $1}'\'')
      end=$($GREP -n "======================================================================" $log | awk -F: '\''($1 > '\''$beg'\''){print $1}'\'' | head -n 1)

      # Check the tests to run.
      TEST+=($(sed -n "${beg},${end}p" $log | $GREP "TESTS TO BE RUN:" | awk '\''(NR==1){print $NF}'\''))
      CTRL+=(${check_num_runs[$i]})
      ERROR+=("Unexpected number of '\''TESTS TO BE RUN'\'' for '\''${check_names[$i]}'\''")

      # Check success status.
      TEST+=($(sed -n "${beg},${end}p" $log | $GREP -c "SUCCESS"))
      CTRL+=(1)
      ERROR+=("Did not find '\''STATUS:  SUCCESS'\'' for '\''${check_names[$i]}'\''.")

      # Extra checks for UnitTestTemplate test.
      # Because developers are intended to copy/paste/edit this unit test,
      # there is a high(er) risk that they might edit the original by accident,
      # so this should check that it remains untouched.
      if test $i -eq 1
      then
         test_cases=(\
            "Brief description, for output file"\
            "Parameter test, float"\
            "Parameter test, double"\
            "Another test"\
            )
         for tc in $(seq 0 $(expr ${#test_cases[*]} - 1))
         do
            TEST+=($(sed -n "${beg},${end}p" $log | $GREP -c "${test_cases[$tc]}"))
            CTRL+=(1)
            ERROR+=("Did not find test case '\''${test_cases[$tc]}'\'' for '\''${check_names[$i]}'\''.")
         done
      fi
   fi
done

# ------------------------------------------------------------------------------
# Final checks
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo "THERE IS A PROBLEM"
  echo ""
  echo ">>>   Have you accidentally edited one of the           <<<"
  echo ">>>   TestDrivers, UnitTestTemplate, or PlaygroundRun   <<<"
  echo ">>>   unit tests in the <maindir>/test directory?       <<<"
  echo ""
  exit 1
fi

' > special_unit_tests.check
#######################################################################

