#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > ml_gpr_pes.info << %EOF%
   Molecule:         H2O
   Wave Function:    VSCF 
   Test Purpose:     Check Adga procedure using GPR Single Points 
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_ml_gpr_pes
export MAINDIR
rm -rf $MAINDIR

SAVEDIR=$MAINDIR/savedir
export SAVEDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > ml_gpr_pes.minp << %EOF%
#0 MidasInput

#1 General
#2 IoLevel
2
#2 MainDir
$MAINDIR 

#1 MLTASK        
#2 IoLevel
0
#2 Name             
ML_GPR                
#2 Database         
$MAINDIR/ml_gpr_pes_database.xyz              
#2 CoordType        
MINT
#2 HOPT
false
#2 HYPERPARAM
$MAINDIR/ml_gpr_pes_hparam.in
#2 Kernel
SCEXP
#2 READCOVAR
$MAINDIR/ml_gpr_pes_covar.info
#2 Noise
1e-8

#1 SinglePoint

#2 Name
single_point
#2 Type
SP_ML
#2 MLDriver
ML_GPR
#2 MLPROP
GROUND_STATE_ENERGY
#2 InputCreatorScript
InputCreatorScriptZERO.sh
#2 RunScript
RunScriptZERO.sh
#2 PropertyInfo
midasifc.propinfo

#1 System

#2 SysDef
#3 MoleculeFile
Midas
$MAINDIR/ml_gpr_pes.mmol

#1 Pes

#2 MLPES
5
#2 MLDriver
ML_GPR


#2 IoLevel
5

#2 FitBasis

#2 AdgaInfo       // 1M ADGA
1
vscf_adga
#2 MultiLevelPes  // 1M ADGA
single_point 1
#2 AdgaGridInitialDim
2
#2 AnalyzeStates
[3*4]
#2 AnalyzeDens
Mean
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-5
#2 ItResDensThr
1.0e-3
#2 DynamicAdgaExt
#2 Symmetry
C1
#2 NThreads
16 16
#2 DoExtInFirstIter

#1 Vib

#2 Operator
#3 IoLevel
14
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile                     // Potential Energy Operator
prop_no_1.mop
#3 KineticEnergy                // Kinetic Energy Operator
Simple

#2 Basis
#3 IoLevel
5
#3 Name
basis_bspline
#3 NoBasBeyondMaxPot
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 PrimBasisDensity
0.8
#3 GradScalBounds
1.5 30.0

#2 Vscf
#3 IoLevel
14
#3 Name
vscf_adga
#3 Oper
h0
#3 Basis
basis_bspline
#3 OccGroundState
#3 ScreenZero
1.0e-21
#3 Threshold
1.0e-13
#3 MaxIter
500

#1 Analysis

#0 MidasInputEnd
%EOF%

#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > $MAINDIR/ml_gpr_pes.mmol << %EOF%
#0MIDASMOLECULE

#1XYZ
3 AU
xyz coordinates output from Midas
O  0.000000000000E+00  0.000000000000E+00  7.398198744383E-01 ISO=16 SUB=1 TREAT=ACTIVE
H -1.431078457575E+00  0.000000000000E+00 -3.699099372191E-01 ISO=1 SUB=1 TREAT=ACTIVE
H  1.431078457575E+00  0.000000000000E+00 -3.699099372191E-01 ISO=1 SUB=1 TREAT=ACTIVE

#1FREQ
3 cm-1
 1.652733831623E+03 A Q6
 3.842680520910E+03 A Q7
 3.951446827643E+03 A Q8

#1VIBCOORD
AU
#2COORDINATE
 8.160780007777E-18  0.000000000000E+00  6.799861765824E-02 
 4.102010124074E-01  0.000000000000E+00 -5.395937038783E-01 
-4.102010124074E-01  0.000000000000E+00 -5.395937038783E-01 
#2COORDINATE
 6.038372539104E-16  0.000000000000E+00 -4.871438641639E-02 
 5.725844839200E-01  0.000000000000E+00  3.865663318436E-01 
-5.725844839200E-01  0.000000000000E+00  3.865663318436E-01 
#2COORDINATE
-6.753619875221E-02  0.000000000000E+00 -4.273358597802E-16 
 5.359242420740E-01  0.000000000000E+00  4.155824616542E-01 
 5.359242420740E-01  0.000000000000E+00 -4.155824616542E-01 

#1SIGMA
AU
#2SIGMACOORDINATE
 2.010856270510E-21  0.000000000000E+00  3.856002102149E-06 
 2.326129590010E-05  0.000000000000E+00 -3.059877580039E-05 
-2.326129590010E-05  0.000000000000E+00 -3.059877580039E-05 
#2SIGMACOORDINATE
 1.948471653711E-19  0.000000000000E+00 -1.493334635544E-05 
 1.755251999489E-04  0.000000000000E+00  1.185015217769E-04 
-1.755251999489E-04  0.000000000000E+00  1.185015217769E-04 
#2SIGMACOORDINATE
-2.189173795088E-05  0.000000000000E+00 -1.383799790593E-19 
 1.737188838822E-04  0.000000000000E+00  1.347103111580E-04 
 1.737188838822E-04  0.000000000000E+00 -1.347103111580E-04 

#0MIDASMOLECULEEND

%EOF%

#######################################################################
#  Database file 
#######################################################################
cat > $MAINDIR/ml_gpr_pes_database.xyz << %EOF%
3 
 Energy =     -76.3587709915
  O  0.000000000  0.000000000  0.391495816 
  H -0.757294103  0.000000000 -0.195747908 
  H  0.757294103  0.000000000 -0.195747908 
 Gradient 3
    -0.000000000 -0.000000000  0.000476105
    -0.000302012 -0.000000000 -0.000238053
     0.000302012 -0.000000000 -0.000238053
 Hessian 18
      1     1    0.364873177  0.000000000  0.000000000 -0.182436588  0.000000000 
      1     2   -0.141470455 -0.182436588  0.000000000  0.141470455 
      2     1    0.000000000  0.000000000  0.000000000  0.000000000  0.000000000 
      2     2    0.000000000  0.000000000  0.000000000  0.000000000 
      3     1    0.000000000  0.000000000  0.244239667 -0.108365351  0.000000000 
      3     2   -0.122119834  0.108365351  0.000000000 -0.122119834 
      4     1   -0.182436588  0.000000000 -0.108365351  0.199038732  0.000000000 
      4     2    0.124917903 -0.016602144  0.000000000 -0.016552552 
      5     1    0.000000000  0.000000000  0.000000000  0.000000000  0.000000000 
      5     2    0.000000000  0.000000000  0.000000000  0.000000000 
      6     1   -0.141470455  0.000000000 -0.122119834  0.124917903  0.000000000 
      6     2    0.115911550  0.016552552  0.000000000  0.006208283 
      7     1   -0.182436588  0.000000000  0.108365351 -0.016602144  0.000000000 
      7     2    0.016552552  0.199038732  0.000000000 -0.124917903 
      8     1    0.000000000  0.000000000  0.000000000  0.000000000  0.000000000 
      8     2    0.000000000  0.000000000  0.000000000  0.000000000 
      9     1    0.141470455  0.000000000 -0.122119834 -0.016552552  0.000000000 
      9     2    0.006208283 -0.124917903  0.000000000  0.115911550 
%EOF%

#######################################################################
#  Stored co-variance matrix 
#######################################################################
cat > $MAINDIR/ml_gpr_pes_covar.info << %EOF%
\$ALOGRITHM
1
\$DATAPOINTS
10
\$KERNEL
SQEXP-X
\$HYPERPARAMETER
4
2.81232875504906e+01
1.42399440673850e+01
1.42144833149897e+01
2.61157180651877e+01
\$DATA
2.8123287550668358e+01 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 -1.3869139628759239e-01 0.0000000000000000e+00 -1.3918868482417493e-01 0.0000000000000000e+00 0.0000000000000000e+00 -4.1234634028621105e-02 
0.0000000000000000e+00 1.9749577283070285e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 
0.0000000000000000e+00 0.0000000000000000e+00 1.9784952406082812e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 
0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 1.0768720814625503e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 
-3.9004580185996587e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 1.9613927910832546e-01 0.0000000000000000e+00 1.2443816696613155e-12 0.0000000000000000e+00 0.0000000000000000e+00 3.6864999328222825e-13 
0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 1.3893985405902265e-01 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 
-3.9144434071096206e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 1.9304273045942514e-02 0.0000000000000000e+00 1.9684255121010258e-01 0.0000000000000000e+00 0.0000000000000000e+00 3.6864599283553079e-13 
0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 7.5623402256904823e-02 0.0000000000000000e+00 0.0000000000000000e+00 
0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 7.5758857434114632e-02 0.0000000000000000e+00 
-1.1596534698334857e+00 0.0000000000000000e+00 0.0000000000000000e+00 0.0000000000000000e+00 5.7188889689096384e-03 0.0000000000000000e+00 5.7393944797225066e-03 0.0000000000000000e+00 0.0000000000000000e+00 5.8314664425109464e-02 
%EOF%

#######################################################################
#  Stored Hyper parameter 
#######################################################################
cat > $MAINDIR/ml_gpr_pes_hparam.in << %EOF%
2.812328755049e+01
1.423994406739e+01
1.421448331499e+01
2.611571806519e+01
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

#######################################################################
#  MidasCpp interface files
#######################################################################

cat > $INTERFACEDIR/InputCreatorScriptZERO.sh << %EOF%
#!/bin/bash

echo "NOW CONVERT:"
pwd
cp midasifc.xyz_input  midasifc.cartrot_xyz
echo "DONE!"
%EOF%

cat > $INTERFACEDIR/RunScriptZERO.sh << %EOF%
#!/bin/bash
#
#
create_property_file() 
{
   echo "GROUND_STATE_ENERGY     0.0000000000" > midasifc.prop_general
}
#
# Change dir to \$1 (the scratch dir)
#
cd \$1
#
# After completion, files with properties according to midasifc.prop_info can be located in the savedir
#
create_property_file
#
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did Midas end properly?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED PROPERLY,"

# Is the total number of ADGA iterations correct?
CRIT0=`$GREP "Adga Iteration:" $log | wc -l`
TEST[1]=`expr $CRIT0`
CTRL[1]=15
ERROR[1]="TOTAL NUMBER OF ADGA ITERATIONS IS INCORRECT,"

# Do we have the correct number of single points in the end?
CRIT0=`$GREP "Total : 45" $log | tail -n1 | wc -l`
TEST[2]=`expr $CRIT0`
CTRL[2]=1
ERROR[2]="THE CORRECT NUMBER OF SINGLEPOINTS IS NOT OBTAINED,"

# Is the VSCF energy correct?
grep "VSCF: vscf_adga_0" ml_gpr_pes.mout  | grep "cm-1" | grep "4.7301837" | wc -l
CRIT0=`$GREP "VSCF: vscf_adga_0" $log | $GREP "cm-1" | $GREP "4.7301837" | wc -l`
TEST[3]=`expr $CRIT0`
CTRL[3]=1
ERROR[3]="DID NOT FIND CORRECT VSCF ENERGY!,"


PASSED=1
for i in 0 1 2 3
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > ml_gpr_pes.check
#######################################################################
