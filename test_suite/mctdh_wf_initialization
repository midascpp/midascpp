#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2D Henon-Heiles potential
   Wave Function:    MCTDH
   Test Purpose:     Initialize wave packet from VSCF, VCI, MCTDH, and TDH wave functions. Simply check that it does not fail!
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > mctdh_wf_initialization.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

#1 General

#1 Vib
#2 IOLevel
 5

#2 Operator
#3 IoLevel
14
#3 Name
   hh_ref
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(Q0)
 0.5 Q^2(Q0)
 -0.5 Q^1(Q0)
 -0.5 DDQ^2(Q1)
 0.5 Q^2(Q1)
 -0.5 Q^1(Q1)
 0.000111803 Q^2(Q0) Q^1(Q1)
 -0.0000372676666667 Q^3(Q1)
#3 KineticEnergy
   USER_DEFINED

#2 Operator
#3 IoLevel
14
#3 Name
   ho_for_imag_time
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(Q0)
 0.5 Q^2(Q0)
 -0.8 Q^1(Q0)
 -0.5 DDQ^2(Q1)
 0.5 Q^2(Q1)
 -0.8 Q^1(Q1)
#3 KineticEnergy
   USER_DEFINED

#2 Operator
#3 Name
   hh
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(Q0)
 0.5 Q^2(Q0)
 -0.5 DDQ^2(Q1)
 0.5 Q^2(Q1)
 0.111803 Q^2(Q0) Q^1(Q1)
 -0.0372676666667 Q^3(Q1)
#3 KineticEnergy
   USER_DEFINED

#2 Basis
#3 Name
   bsplines
#3 BSplineBasis
   10
#3 NPrimBasisFunctions
   30
#3 TurningPoint
   10

// Do a VSCF calculation on the shifted HH
#2 Vscf
#3 Name
   vscf_ref
#3 Basis
   bsplines
#3 oper
   hh_ref

// Do MCTDH on VSCF
#2 McTdH
#3 IoLevel
   5
#3 Method
   MCTDH
#3 ActiveSpace
   [2*6]
#3 Name
   mctdh_vscf_init
#3 Basis
   bsplines
#3 Oper
   hh
#3 InitialWf
   VSCF vscf_ref_0.0
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 1.e0
      #5 OutputPoints
         11
      #5 Tolerance
         1.e-10 1.e-10
      #5 InitialStepSize
         1.e-4
#3 Properties           // Properties to calculate
   energy
   halftimeautocorr

// Do a VCI calculation on the shifted HH
#2 Vcc
#3 Method
   VCI[2]
#3 LimitModalBasis
   [2*6]
#3 Oper
   hh_ref
#3 Basis
   bsplines
#3 Name
   vci_ref

// Do MCTDH on VCI
#2 McTdH
#3 IoLevel
   5
#3 Method
   MCTDH
#3 ActiveSpace
   [2*6]
#3 Name
   mctdh_vci_init
#3 Basis
   bsplines
#3 Oper
   hh
#3 InitialWf
   VCI vci_ref
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 1.e0
      #5 OutputPoints
         11
      #5 Tolerance
         1.e-10 1.e-10
      #5 InitialStepSize
         1.e-4
#3 Properties           // Properties to calculate
   energy
   halftimeautocorr

// Do a VSCF calculation on the less shifted HO
#2 Vscf
#3 Name
   vscf_ref_for_mctdh
#3 Basis
   bsplines
#3 oper
   ho_for_imag_time

// Do imag-time MCTDH on VSCF
#2 McTdH
#3 IoLevel
   5
#3 Method
   MCTDH
#3 ImagTime
   1.e-12
#3 ActiveSpace
   [2*6]
#3 Name
   mctdh_ref
#3 Basis
   bsplines
#3 Oper
   hh_ref
#3 InitialWf
   VSCF vscf_ref_for_mctdh_0.0
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 1.e2
      #5 OutputPoints
         0
      #5 Tolerance
         1.e-10 1.e-10
      #5 InitialStepSize
         1.e-4

// Do MCTDH on MCTDH
#2 McTdH
#3 IoLevel
   5
#3 Method
   MCTDH
#3 ActiveSpace
   [2*6]
#3 Name
   mctdh_mctdh_init
#3 Basis
   bsplines
#3 Oper
   hh
#3 InitialWf
   MCTDH mctdh_ref
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 1.e0
      #5 OutputPoints
         11
      #5 Tolerance
         1.e-10 1.e-10
      #5 InitialStepSize
         1.e-4
#3 Properties           // Properties to calculate
   energy
   halftimeautocorr


#0 Midas Input End

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check energy at t_beg and t_end
# ------------------------------------------------------------------------------
# VSCF init
CRIT_ITER_E[0]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_beg:                1\.25928139791108..E+00" |  wc -l`
CRIT_ITER_E[1]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_end:                1\.2592813953437...E+00" |  wc -l`
# VCI init
CRIT_ITER_E[2]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_beg:                1\.25927619687174..E+00" |  wc -l`
CRIT_ITER_E[3]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_end:                1\.2592761943051...E+00" |  wc -l`
# Imag time
CRIT_ITER_E[4]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_beg:                8\.400351925622[4-5]...E-01" |  wc -l`
CRIT_ITER_E[5]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_end:                7\.50009568407.....E-01"     |  wc -l`
# MCTDH init
CRIT_ITER_E[6]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_beg:                1\.2592769.........E+00" |  wc -l`
CRIT_ITER_E[7]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_end:                1\.2592769.........E+00" |  wc -l`

for i in $(seq 0 7)
do
   TEST+=(${CRIT_ITER_E[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''MCTDH energy'\'', CRIT_ITER_E[$i].")
done

# ------------------------------------------------------------------------------
# Check autocorrelation function at t_beg and t_end
# ------------------------------------------------------------------------------
# VSCF init
CRIT_ITER_S[0]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_beg:                1\.0000000000000000E+00" |  wc -l`
CRIT_ITER_S[1]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_end:                7\.04789644331.....E-01" |  wc -l`
# VCI init
CRIT_ITER_S[2]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_beg:                1\.0000000000000004E+00" |  wc -l`
CRIT_ITER_S[3]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_end:                7\.04783015934.....E-01" |  wc -l`
# MCTDH init
CRIT_ITER_S[4]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_end:                7\.047823..........E-01" |  wc -l`
for i in $(seq 0 4)
do
   TEST+=(${CRIT_ITER_S[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''MCTDH auto-correlation function'\'', CRIT_ITER_S[$i].")
done

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > mctdh_wf_initialization.check
#######################################################################
