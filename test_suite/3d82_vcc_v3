#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         3-dim ho. test 
   Wave Function:    FVCI   
   Test Purpose:     Check FVCI energy 
   Reference:        Operator, and VSCF as Christoffel and Bowman CPL 85, 220 (1982),
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 3d82_vcc_v3.minp <<%EOF%
#0 MIDAS Input          

#1 general
#2 IoLevel
    2
#2 Time

#1 Vib
#2 IoLevel
  5

#2 Operator
#3 IoLevel
14
#3 Name
 bowman82
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
-0.0 DDQ^3(b)
-0.0 DDQ^4(b)
-0.5 DDQ^2(c)
0.245 Q^2(a)
0.845 Q^2(b)
0.5 Q^2(c)
-0.01 Q^3(a)
-0.01 Q^3(b)
-0.1 Q^1(a) Q^2(b)
-0.1 Q^1(b) Q^2(c)
#3 KineticEnergy
 USER_DEFINED

#2 Operator
#3 IoLevel
14
#3 Name
aucbowman82
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
-0.0 DDQ^3(b)
-0.0 DDQ^4(b)
-0.5 DDQ^2(c)
0.245 Q^2(a)
0.845 Q^2(b)
0.5 Q^2(c)

#2 Basis
#3 Name
 smallbasis
#3 Define
 a HO n_high=2,omeg= 0.7,Mass=1.0
 b HO n_high=3,omeg= 1.3,Mass=1.0
 c HO n_high=4,omeg= 1.0,Mass=1.0
#3 IoLevel
   9

#2 Basis
#3 Name
 largebasis
#3 Define
 a HO n_high=32,omeg= 0.7,Mass=1.0
 b HO n_high=34,omeg= 1.3,Mass=1.0
 c HO n_high=36,omeg= 1.0,Mass=1.0
#3 IoLevel
   9

#2 Vscf
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 0

#2 Vscf
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 1

#2 Vcc
#3 UseAllVscfModals
#3 Name
   1vcc
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 0
#3 Vcc
#3 Transformer
   trf=v3 t1transh=false
#3 MaxExci
 2
#3 ItEqEnerThr
 1.0E-12
#3 TimeIt
//#3 SizeIt

#2 Vcc
#3 UseAllVscfModals
#3 Name
   2vcc
#3 IoLevel
 2
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 1
#3 Vcc
#3 Transformer
   trf=v3 t1transh=false
#3 MaxExci
 2
#3 ItEqEnerThr
 1.0E-12
#3 TimeIt
//#3 SizeIt

#2 Vcc
#3 UseAllVscfModals
#3 Name
   3vcc
#3 IoLevel
 2
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 1
#3 Vcc
#3 Transformer
   trf=v3 t1transh=false
#3 MaxExci
 3
#3 ItEqEnerThr
 1.0E-12

#2 Vcc
#3 UseAllVscfModals
#3 UseOldVccSolver
#3 Name
   4vcc
#3 IoLevel
 2
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 0 1
#3 Vcc
#3 Transformer
   trf=v3 t1transh=false
#3 MaxExci
 3
#3 Iso
 1
#3 ItEqEnerThr
 1.0E-13
#3 TimeIt
//#3 SizeIt

#0 Midas Input End 

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`                                               # 2
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`     # 2
CRIT3=`$GREP "n_high=2,omeg" $log | wc -l`                                          # 1
CRIT4=`$GREP "n_high=3,omeg" $log | wc -l`                                          # 1
CRIT5=`$GREP "n_high=4,omeg" $log | wc -l`                                          # 1
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[1]=7
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

CRIT11=`$GREP "Final VCC" $log | grep "1.49388176852[1-3]" |  wc -l`
CRIT12=`$GREP "Final VCC" $log | grep "2.48581412957[4-6]" |  wc -l`
CRIT13=`$GREP "Final VCC" $log | grep "2.485819187318" |  wc -l`
CRIT14=`$GREP "VCC" $log | grep "2.485819381125" |  wc -l`

TEST[2]=`expr \
$CRIT11 \
\+ $CRIT12 \
\+ $CRIT13 \
\+ $CRIT14 \
`
echo $CRIT11 $CRIT12 
echo $CRIT13 $CRIT14 

CTRL[2]=4 
ERROR[2]="VCC ENERGIES NOT CORRECT"

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 3d82_vcc_v3.check
#######################################################################
