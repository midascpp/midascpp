#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         H2O
   Wave Function:    Dalton HF WF 
   Test Purpose:     Check setup in Grid
                     normal coordinates
                     check calculation inertia components
                     check addition of Watson terms
                     Harmonic freqs. with Watson term included
                     specific derivatives for energy and inertia components
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_pes_Grid_5
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Grid_5.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 singlepoint
#2 name
Dalton
#2 Dalton Input
**DALTON INPUT
.RUN WAVEFUNCTIONS
**WAVE FUNCTIONS
.HF
*END OF INPUT
#2 End of Dalton Input
#2 DaltonBasis
1 cc-pVDZ
8 cc-pVDZ
#2 type
SP_DALTON

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 FitBasis
#3name
fitbas_0
#3 FitFunctionsMaxOrder
[3*10]
#3 FitBasisMaxOrder
[3*10]
#3 FitFunctionsEffInertiaInvMaxOrder
[3*4]
#3 FitBasisEffInertiaInvMaxOrder
[3*4]

#2 USEFITBASIS
fitbas_0
#2 IoLevel
18 //only for test reasons
#2 multilevelpes
Dalton 2
#2 CalcEffInertiaInvTens
#2 StaticInfo
2       // MC Level
64  16  //  Grid points in each MC level
1   1   //  Fractioning
#2 ExtendedGridSettings
3 0 1 2 8 8 8 3/4 3/4 3/4 3/4 3/4 3/4 
#2 EndOfExtendedGridSettings
#2 Interpolate
 Spline
 Surface
 Natural
#2 PesFGMesh
12 8 6
#2 PesFGScalFac
0.9
//#2 PesPlot

#0 Midas Input End
%EOF%
#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_Grid_5.mmol <<%EOF%
#0 Midas Molecule
#1 XYZ
3 au 
comment
O       0.0000000000            0.0000000000           -0.0025175658
H#1     0.0000000000            1.4150041934           -1.0959044979
H#2     0.0000000000           -1.4150041934           -1.0959044979

#1 FREQ
3 cm-1
0.4212109672925103E+04
0.4113781219673689E+04
0.1775815535432357E+04

#1 VIBCOORD
au
#2 COORDinATE
0.0000000000000000E+00-6.7619297070807563E-02-2.5433639904632184E-13
0.0000000000000000E+00 5.3658368714876159E-01-4.1462321756492315E-01
0.0000000000000000E+00 5.3658368715461635E-01 4.1462321756896003E-01
#2 CoorDINATE
0.0000000000000000E+00 3.4789842409431797E-13-4.9391750654233992E-02
0.0000000000000000E+00-5.6845498871024902E-01 3.9194148459304878E-01
0.0000000000000000E+00 5.6845498870472255E-01 3.9194148458877820E-01
#2 coordinate
0.0000000000000000E+00-5.7865077056961033E-16-6.7508206202242835E-02
0.0000000000000000E+00 4.1590480091157034E-01 5.3570214075131206E-01
0.0000000000000000E+00-4.1590480091157078E-01 5.3570214075131162E-01
#0 Midas Molecule End
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

#Basic logic: check Mu tensors are calculated and basic spline and polynomial fitting
CRIT1=`$GREP "Will do a grid-based potential energy and\/or molecular property surface" $log | wc -l`                              
CRIT2=`$GREP "The grid is chosen on the basis of the harmonic oscillator quantum number 10" $log | wc -l`
CRIT3=`$GREP "Effective inertia inverse tensor will be added to the list of properties" $log | wc -l`
CRIT4=`$GREP "Watson potential energy term will be added to the" $log | wc -l`
CRIT5=`$GREP "The fitting routine will use frequency scaled coordinates" $log | wc -l`              

TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[1]=5
ERROR[1]="BASIC INPUT READING IS NOT CORRECT"

# Basic logic: basic grid setup .
CRIT1=`$GREP "Mode combination:  \(0\)" $log | wc -l`                               # 1
CRIT2=`$GREP "Mode combination:  \(1\)" $log | wc -l`                               # 1
CRIT3=`$GREP "Mode combination:  \(2\)" $log | wc -l`                               # 1
CRIT4=`$GREP "Mode combination:  \(0,1\)" $log | wc -l`                             # 1
CRIT5=`$GREP "Mode combination:  \(0,2\)" $log | wc -l`                             # 1
CRIT6=`$GREP "Mode combination:  \(1,2\)" $log | wc -l`                             # 1
CRIT7=`$GREP "Mode combination:  \(0,1,2\)" $log | wc -l`                           # 1
CRIT8=`$GREP "Number of grid points: \(64\)" $log | wc -l`                          # 3
CRIT9=`$GREP "Number of grid points: \(16,16\)" $log | wc -l`                       # 3
CRIT10=`$GREP "Number of grid points: \(8,8,8\)" $log | wc -l`                      # 1
CRIT11=`$GREP "Fractioning:           \(1,1\)" $log | wc -l`                        # 3
CRIT12=`$GREP "Fractioning:           \(1,1,1,1\)" $log | wc -l`                    # 3
CRIT13=`$GREP "Fractioning:           \(3/4,3/4,3/4,3/4,3/4,3/4\)" $log | wc -l`    # 1
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13`
CTRL[2]=21
ERROR[2]="GRID SETUP IS NOT CORRECT"

# Calculation of inertia tensors: check for reference state
#CRIT1=`$GREP "eigenvalue no.1:  5\.992661641" $log | wc -l`
#CRIT2=`$GREP "eigenvalue no.2:  1\.130141586" $log | wc -l`
#CRIT3=`$GREP "eigenvalue no.3:  1\.729407750" $log | wc -l`
#TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
#CTRL[3]=3
#ERROR[3]="INERTIA TENSORS WRONG"


# Electronic struture calculations
CRIT1=`$GREP "Total : 1177" $log | wc -l`
TEST[3]=`expr $CRIT1`
CTRL[3]=1
ERROR[3]="NUMBER OF CALCULATIONS WRONG"



CRIT1=`$GREP "bar\(\*#1_-7/16\*\) =    1\.0E\+00 \*#1_-7/16\*" $log | wc -l`
TEST[4]=`expr $CRIT1`
CTRL[4]=1
ERROR[4]="TRANSFORMED POTENTIALS ARE WRONG"

CRIT1=`$GREP "Coarse grid has been generated" $log | wc -l`
TEST[5]=`expr $CRIT1`
CTRL[5]=1
ERROR[5]="COARSE GRID HAS NOT BEEN GENERATED"

#CRIT1=`$GREP "first and last inertia prop. to be interpolated: 1   6" $log | wc -l`
#TEST[8]=`expr $CRIT1`
#CTRL[8]=1
#ERROR[8]="INERTIA PROPERTIES ARE NOT INTERPOLATED"

# Harmonic freqs.
# 4.2120880664933165462571E+03
# 4.1137597013971198975923E+03
# 1.7757393259049204061739E+03
                        
CRIT1=`$GREP "1         4\.2120880" $log | wc -l`
CRIT2=`$GREP "2         4\.11375970" $log | wc -l`
CRIT3=`$GREP "3         1\.775739325[8-9]" $log | wc -l`

TEST[6]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[6]=3
ERROR[6]="CALCULATED HARMONIC FREQS WRONG"

# Specific derivatives
# inertia component YZ
#V_1_2_3 = -1.7593228073809741099240E-06
#V_1_2_3_3 = -6.4787282565339260725484E-07
#V_1_2_2_3 = -3.9335545165585860267129E-07
#V_1_1_2_3 = -2.3262772572691826340855E-18

CRIT1=`$GREP "Q\^1\(Q0\) Q\^1\(Q1\) Q\^1\(Q2\) = -1\.75932280[0-9]" $log | wc -l`
CRIT2=`$GREP "Q\^1\(Q0\) Q\^1\(Q1\) Q\^2\(Q2\) = -6\.47872825[0-9]" $log | wc -l`
CRIT3=`$GREP "Q\^1\(Q0\) Q\^2\(Q1\) Q\^1\(Q2\) = -3\.93355451[0-9]" $log | wc -l`
TEST[7]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[7]=3
ERROR[7]="SPECIFIC DERIVATIVES WRONG"

#  Calculated Harmonic vibrational frequencies in cm^-1:
#  *****************************************************
# 1         4.2120880665007944116951E+03
# 2         4.1137597014004722950631E+03
# 3         1.7757393259121622577368E+03
CRIT1=`$GREP "1         4.212088066[45]............E\+03" $log | wc -l`
CRIT2=`$GREP "2         4.113759701[34]............E\+03" $log | wc -l`
CRIT3=`$GREP "3         1.775739325[89]............E\+03" $log | wc -l`
TEST[8]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[8]=3
ERROR[8]="HARMONIC FREQUENCIES WRONG"


PASSED=1
for i in 0 1 2 3 4 5 6 7 8
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Grid_5.check
#######################################################################
