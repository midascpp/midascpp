#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > pes_Adga_DoubleWell.info << %EOF%
   Molecule:         NH3
   Wave Function:    Double-well potential (NH3 inversion mode) 
   Test Purpose:     Check Adga procedure for analytical double-well potential with general fit-basis functions
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_pes_Adga_DoubleWell
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Adga_DoubleWell.minp << %EOF%
#0 MidasInput
#1 General
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
doublewell_singlepoint
#2 Type
SP_MODEL
#2 DoubleWellPot

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 IoLevel
2

#2 FitBasis
#3 Iolevel
5
#3 Name
fitbasis
#3 FitFunctionsMaxOrder
20
#3 FitBasisMaxOrder
20
#3 AddFitFuncsConserv
#3 FitMethod
SVD
#3 FitBasisDef
#4 SpecFitBasis
#5 FitFunctions
EXP(-1*alpha*Q*Q) Q^2 EXP(-3*alpha*Q*Q) Q^4 EXP(-5*alpha*Q*Q) Q^6 EXP(-7*alpha*Q*Q) Q^8
#5 Modes
0
#5 OptFunc
F(Q,omega,alpha,A) 0.5*omega*omega*Q*Q+A*EXP(-alpha*Q*Q) (0,0.1,0.1,0.1)
#5 OptFuncThr
1.0e-8
#5 Props
Energy

#2 UseFitBasis
fitbasis
#2 AdgaInfo
1
E_vscf_ref
#2 MultiLevelPes 
doublewell_singlepoint 1
#2 AnalyzeStates
[1*10]
#2 AdgaGridInitialDim
9
#2 NIterMax
20
#2 AdgaRelScalFact
1
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-6
#2 ItResDensThr
1.0e-3
#2 DynamicAdgaExt
#2 DoExtInFirstIter
#2 NThreads
4
#2 NormalCoordinateThreshold
1.0

#1 Vib
#2 Operator    // This is operator input
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
prop_no_1.mop
#3 SetInfo
type=energy

#2 Basis     // This is basis input
#3 IoLevel
4
#3 Name
basis_bspline
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 PrimBasisDensity
0.8
#3 GradScalBounds
1.5 50.0

#2 Vscf      // This the Vscf input
#3 IoLevel
2
#3 Name
E_vscf_ref
#3 Oper
h0
#3 Basis
basis_bspline
#3 OccGroundState
#3 ScreenZero
1.0e-21
#3 Threshold
1.0e-15
#3 MaxIter
200

#2 Vscf      // This the Vscf input
#3 IoLevel
2
#3 Name
E_vscf_state
#3 Oper
h0
#3 Basis
basis_bspline
#3 Occup
0
#3 OccuMax
9
#3 ScreenZero
1.0e-21
#3 Threshold
1.0e-15
#3 MaxIter
200

#1 Analysis

#0 MidasInputEnd
%EOF%

#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_Adga_DoubleWell.mmol << %EOF%
#0 MidasMolecule

#1 XYZ
2 au

N  0.0    0.0       0.0
H  0.0    0.0       0.0

#1 FREQ
1 cm-1
 -8.8331567775635767E+02

#1 VIBCOORD
au
#2 Coordinate
 -1.1261014919429690E-01   0.0000000000000000E+00   0.0000000000000000E+00
  5.2154924116060941E-01   0.0000000000000000E+00   0.0000000000000000E+00

#0 MidasMoleculeEnd
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did Midas end properly?
CRIT1=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT1`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED PROPERLY"

# Did we converge the 1-mode grids?
CRIT1=`$GREP "The Adga is converged for 1-mode grids" $log | wc -l`
TEST[1]=`expr $CRIT1`
CTRL[1]=1
ERROR[1]="1-MODE GRIDS ARE NOT CONVERGED"

# Do we have the correct number of single points in the end?
CRIT1=`$GREP -A3 "Evaluating list of singlepoints" $log | tail -n 4 | $GREP -c "Total : 25"`
TEST[2]=`expr $CRIT1`
CTRL[2]=1
ERROR[2]="THE CORRECT NUMBER OF SINGLEPOINTS IS NOT OBTAINED"

# Do we have the correct vscf energies?
CRIT1=`$GREP "8\.252451..........E\-01" $log | tail -n1 | wc -l`
CRIT2=`$GREP "9\.3244855.........E\+02" $log | tail -n1 | wc -l`
CRIT3=`$GREP "9\.677482..........E\+02" $log | tail -n1 | wc -l`
CRIT4=`$GREP "1\.60288...........E\+03" $log | tail -n1 | wc -l`
CRIT5=`$GREP "1\.8827886.........E\+03" $log | tail -n1 | wc -l`
CRIT6=`$GREP "2\.3849345.........E\+03" $log | tail -n1 | wc -l`
CRIT7=`$GREP "2\.8917090.........E\+03" $log | tail -n1 | wc -l`
CRIT8=`$GREP "3\.4531875.........E\+03" $log | tail -n1 | wc -l`
CRIT9=`$GREP "4\.047406..........E\+03" $log | tail -n1 | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9`
CTRL[3]=9
ERROR[3]="VSCF ENERGIES ARE WRONG"

PASSED=1
for i in 0 1 2 3
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Adga_DoubleWell.check
#######################################################################
