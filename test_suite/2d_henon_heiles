#!/bin/sh
#
# This is the script for generating files for a specific test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Testing VSCF on 2D Henon-Heiles potential with state-transfer operators in PES.
   -------------
   Molecule:         2D Henon-Heiles model potential
   Wave Function:    VSCF
   Test Purpose:     Check state-transfer operators in one mode.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2d_henon_heiles.minp <<%EOF%
#0 MIDAS Input
#1 General
   #2 IoLevel
      2 
   #2 Time

#1 Vib
   #2 IoLevel 
      1

#2 Operator
   #3 Name
      2dhh_ref
   #3 IoLevel
      3 
   #3 OperInput
   #4 OperatorTerms
      -0.5              DDQ^2(Q0)
       0.5              Q^2(Q0)
      -0.5              DDQ^2(Q1)
       0.5              Q^2(Q1)
       0.111803         Q^2(Q0) Q^1(Q1)
      -0.0372676666667  Q^3(Q1)

#2 Operator
   #3 Name
      2dhh_q0_ss
   #3 IoLevel
      3 
   #3 OperInput
   #4 OperatorTerms
       0.5              |0><0|(Q0)
       1.5              |1><1|(Q0)
       2.5              |2><2|(Q0)
       3.5              |3><3|(Q0)
       4.5              |4><4|(Q0)
       5.5              |5><5|(Q0)
       6.5              |6><6|(Q0)
       7.5              |7><7|(Q0)
      -0.5              DDQ^2(Q1)
       0.5              Q^2(Q1)
       0.0559015           |0><0|(Q0)  Q^1(Q1)
       0.07905665945699958 |0><2|(Q0)  Q^1(Q1)
       0.1677045           |1><1|(Q0)  Q^1(Q1)
       0.1369301508561938  |1><3|(Q0)  Q^1(Q1)
       0.07905665945699958 |2><0|(Q0)  Q^1(Q1)
       0.2795075           |2><2|(Q0)  Q^1(Q1)
       0.19364847643862318 |2><4|(Q0)  Q^1(Q1)
       0.1369301508561938  |3><1|(Q0)  Q^1(Q1)
       0.3913105           |3><3|(Q0)  Q^1(Q1)
       0.249999108088409   |3><5|(Q0)  Q^1(Q1)
       0.19364847643862318 |4><2|(Q0)  Q^1(Q1)
       0.5031135           |4><4|(Q0)  Q^1(Q1)
       0.30618512549375043 |4><6|(Q0)  Q^1(Q1)
       0.249999108088409   |5><3|(Q0)  Q^1(Q1)
       0.6149165           |5><5|(Q0)  Q^1(Q1)
       0.362283126152047   |5><7|(Q0)  Q^1(Q1)
       0.30618512549375043 |6><4|(Q0)  Q^1(Q1)
       0.7267195           |6><6|(Q0)  Q^1(Q1)
       0.362283126152047   |7><5|(Q0)  Q^1(Q1)
       0.8385225           |7><7|(Q0)  Q^1(Q1)
      -0.0372676666667     Q^3(Q1)

#2 Basis 
   #3 Name
      basis
   #3 HoBasis
      7
   #3 IoLevel
      1

#2 Vscf
   #3 Name 
      vscf_ref
   #3 Oper
      2dhh_ref
   #3 Basis
      basis

#2 Vscf
   #3 Name 
      vscf_q0_ss
   #3 Oper
      2dhh_q0_ss
 
#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check stuff from last iteration and num. iterations.
# ------------------------------------------------------------------------------
TEST+=($($GREP "Nonlinear equations are converged in 2 iterations" $log | wc -l)) # 1
CTRL+=(1)
ERROR+=("Number of iterations.")

TEST+=($($GREP "Iter: 2 Total energy :.* 2\.6434458893792[12]..E-02" $log | wc -l)) #1
CTRL+=(1)
ERROR+=("Last iteration, total energy.")

TEST+=($($GREP "Iter: 2 Norm of trial vec .* 2\.020[01].*E-11" $log | wc -l)) #1
CTRL+=(1)
ERROR+=("Last iteration, trial vector norm.")

TEST+=($($GREP "Iter: 2 ..err-vec..: .* 9\.[12].*E-16" $log | wc -l)) #1
CTRL+=(1)
ERROR+=("Last iteration, error vector norm.")

# ------------------------------------------------------------------------------
# Check final VCC energy.
# ------------------------------------------------------------------------------
TEST+=(`$GREP "Final Vcc energy .* 2.6434458893792...E-02 .* a.u." $log |  wc -l`) # 1
CTRL+=(1)
ERROR+=("VCC ENERGY NOT CORRECT")

# ------------------------------------------------------------------------------
# Check VCC rsp. iterations.
# ------------------------------------------------------------------------------
CRIT_RSP_ITER_E[0]=$($GREP "w= 5.322514807557[67]...E-03 ..H-wS.C.= 5.478E-07" $log | wc -l) # Iter 0
CRIT_RSP_ITER_E[1]=$($GREP "w= 5.322514781258[56]...E-03 ..H-wS.C.= 3.956E-07" $log | wc -l) # Iter 1
CRIT_RSP_ITER_E[2]=$($GREP "w= 5.322514698309[45]...E-03 ..H-wS.C.= 1.220E-08" $log | wc -l) # Iter 3
CRIT_RSP_ITER_E[3]=$($GREP "w= 5.322514698300[34]...E-03 ..H-wS.C.= 1.143E-09" $log | wc -l) # Iter 4

for i in $(seq 0 3)
do
   TEST+=(${CRIT_RSP_ITER_E[$i]})
   CTRL+=(1)
   ERROR+=("Rsp iterations not correct, CRIT_RSP_ITER_E[$i].")
done

# ------------------------------------------------------------------------------
# Check VCC rsp. energy.
# ------------------------------------------------------------------------------
TEST+=(`$GREP "Rsp_Root_0 EigenValue .* 5.3225146983002...E-03 au" $log |  wc -l`) # au
CTRL+=(1)
ERROR+=("RSP ENERGY NOT CORRECT")

# Check num. Rsp iters.
TEST+=($($GREP "Equations are converged in 7 iterations, Dim reduced space  7" $log | wc -l))
CTRL+=(1)
ERROR+=("NUM. RSP ITERATIONS WRONG")

# ------------------------------------------------------------------------------
# Check transformer calls
# ------------------------------------------------------------------------------
TEST+=($($GREP -c "Entering Vcc2TransReg<Nb>::Transform" $log))
CTRL+=(3)
ERROR+=("Wrong number of '\''Entering Vcc2TransReg<Nb>::Transform'\''.")

TEST+=($($GREP -c "Entering Vcc2TransRJac<Nb>::Transform" $log))
CTRL+=(7)
ERROR+=("Wrong number of '\''Entering Vcc2TransRJac<Nb>::Transform'\''.")

TEST+=($($GREP -c "Vcc2TransFixedAmps<Nb>; T1-transforming integrals" $log))
CTRL+=(1)
ERROR+=("Wrong number of '\''Vcc2TransFixedAmps<Nb>; T1-transforming integrals'\''.")

TEST+=($($GREP -c "Vcc2TransRJac<Nb>; computing T-based intermediates" $log))
CTRL+=(1)
ERROR+=("Wrong number of '\''Vcc2TransRJac<Nb>; computing T-based intermediates'\''.")


# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2d_henon_heiles.check
#######################################################################
