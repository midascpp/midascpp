#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > vscf_bsplines_backwards.info << %EOF%
   Molecule:         H2+
   Wave Function:    Morse potential 
   Test Purpose:     Check that the construction of basis is backwards compatible
%EOF%

MAINDIR=$PWD/Dir_vscf_bsplines_backwards
export MAINDIR
rm -rf  $MAINDIR

SAVEDIR=$MAINDIR/savedir
export SAVEDIR

ANALYSISDIR=$MAINDIR/analysis
export ANALYSISDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > vscf_bsplines_backwards.minp << %EOF%
#0 MidasInput
#1 General
#2 IoLevel
14

#1 Vib
#2 Operator    // This is operator input
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
vscf_bsplines_backwards.mop
#3 NoActiveTermsAlgo
#3 SetInfo
type=energy

#2 Basis     // This is basis input
#3 Name
basis_bspline
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 PrimBasisDensity
0.8
#3 ScalBounds
1.5 20.0

#2 Vscf      // This the Vscf input
#3 IoLevel
3
#3 Name
vscf
#3 Oper
h0
#3 Basis
basis_bspline
#3 Occup
0
#3 OccuMax 
5
#3 ScreenZero
0.1E-20
#3 MaxIter
100

#0 MidasInputEnd
%EOF%

#######################################################################
#  OPERATOR FILE   
#######################################################################
cat > vscf_bsplines_backwards.mop << %EOF%
SCALING FREQUENCIES N_FRQS=1
1.0943406010079899728926E-02 
DALTON_FOR_MIDAS
 1.0183606251020285715647E-09 1   
 5.4689215486988821887659E-03 1 1 
-1.2626383198295321706739E-03 1 1 1
 1.7004894065492315779684E-04 1 1 1 1
-1.6825730327782125773928E-05 1 1 1 1 1
 1.3379621486931635436935E-06 1 1 1 1 1 1
-8.9673088575283562188210E-08 1 1 1 1 1 1 1
 5.2241450783865374802835E-09 1 1 1 1 1 1 1 1
-2.7073475402840951448588E-10 1 1 1 1 1 1 1 1 1 
 1.2410265804251376130837E-11 1 1 1 1 1 1 1 1 1 1
-4.4849531368172532635124E-13 1 1 1 1 1 1 1 1 1 1 1
%EOF%

#######################################################################
#  GRID BOUNDS FILE   
#######################################################################
cat > vscf_bsplines_backwards.mbounds << %EOF%
#column order: mode l_pes_bound r_pes_bound l_pes_energy r_pes_energy l_gradient r_gradient l_max_energy_point r_max_energy_point
# BTW I CAN HAVE MORE THAN 1 COMMENT LINE :D
1 -3.1704439991034369938916E+01 7.1334989979827327033490E+01 1.0 1.0 1.0
# ALSO ONE HERE!!! 
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a -o"
else
   GREP="egrep"
fi

# Did Midas ended properly?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED PROPERLY"

# Did we converge the vscf calculation?
CRIT1=`$GREP "converged\!" $log | wc -l`
TEST[1]=`expr $CRIT1`
CTRL[1]=6
ERROR[1]="VSCF CALCULATION DID NOT CONVERGE"

# Do we have the correct vscf energies?
CRIT1=`$GREP "2\.2731656" $log | tail -n1 | wc -l`
CRIT2=`$GREP "4\.4183068" $log | tail -n1 | wc -l`
CRIT3=`$GREP "6\.4354183" $log | tail -n1 | wc -l`
CRIT4=`$GREP "8\.3244675" $log | tail -n1 | wc -l`
CRIT5=`$GREP "1\.00852803" $log | tail -n1 | wc -l`
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[2]=5
ERROR[2]="VSCF ENERGIES ARE WRONG"

PASSED=1
for i in 0 1 2
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vscf_bsplines_backwards.check
#######################################################################
