#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test - in reality uncoupled
   Wave Function:    VSCF / micro basis, one state only, adapted HO basis
                     (omega in both mode is 1.4 =  ho omeg
                      integral NOT reduced to one dim!)
   Test Purpose:     Check VSCF energy of lowest state in small basis.
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dhouc_basic5.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input
*/

// This the general input

#1 general
#2 IoLevel
    5
#2 BufSIZE
 120
#2 FileSize
 500
#2 Time
#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  5

#2 Operator    // This is operator input
#3 Name
 testoper
!#3 IgnQiOnly
#3 OperFile
2dhouc_basic5.op
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a HO n_high=2,omeg22=0.98
 b HO n_high=2,omeg22=0.245
#3 IoLevel
   1

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Name
scf00
#3 Oper
testoper
#3 Basis
basis
#3 Occup
 0 0
#3 Threshold
 1.0e-12
#3 MaxIter
 20
!#3 OnDisc


/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

cat > 2dhouc_basic5.op <<%EOF%
#0MIDASOPERATOR
#1OPERATORTERMS
-0.5   DDQ^2(a)
-0.5   DDQ^2(b)
 0.98  Q^2(a)
 0.245 Q^2(b)
#0MIDASOPERATOREND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 `
CTRL[1]=2
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT11=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0500000000000[0-9][0-9]" | wc -l`
CRIT12=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0499999999999[0-9][0-9]" | wc -l`
TEST[2]=`expr $CRIT11 \+ $CRIT12 `
CTRL[2]=1
ERROR[2]="VSCF ENERGY NOT CORRECT"

PASSED=1
for i in 1 2
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

' > 2dhouc_basic5.check
#######################################################################
