#!/usr/bin/env python

##!/bin/sh -
#'eval' 'exec python -- "\$0" "\$@"'


import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
import operator

plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True

def GetTimings(arTestCases):
   WallTimes = []

   for testcase in arTestCases:
      f = open(str(testcase)+'.out')
      lines = f.readlines()
      for line in lines:
         if re.search(r'Total Wall time used in Midas:', line):
            WallTimes.append(int(format(float(line.split()[6]), '.0f')))
            break

   return WallTimes

def PlotTimings(arTitle, arTestCases, Timings):

   #Define and order data for plotting
   my_tuple=zip(Timings,arTestCases)
   def getKey(item):
      return item[0]
   my_tuple_sorted = sorted(my_tuple, key=getKey)
   print "Timings and testcases sorted according to the timings:"
   print my_tuple_sorted
   Timings_sorted= zip(*my_tuple_sorted)[0]
   print "Timings_sorted:"
   print Timings_sorted
   arTestCases_sorted= zip(*my_tuple_sorted)[1]
   print "arTestCases_sorted:"
   print arTestCases_sorted
   y_pos = np.arange(len(arTestCases))
   #print "y_pos"
   #print y_pos

   #Set size of plot
   fig, ax = plt.subplots( nrows=1, ncols=1, figsize=(len(Timings_sorted)*3.0,len(y_pos)*3.0)) 
   my_fontsize = len(y_pos)*1.5

   #Make bar plot
   ax.barh(y_pos, Timings_sorted, align='center', alpha=1.0, color='#263056', height=0.1)
   ax.set_yticklabels(arTestCases_sorted, fontsize = my_fontsize)
   ax.xaxis.set_tick_params(labelsize=my_fontsize)

   ax.set_yticks(np.arange(min(y_pos), max(y_pos)+1, 1.0))

   ax.set_xlabel('Walltime in seconds', fontsize = my_fontsize)
   ax.set_facecolor('#FA7268')
   ax.set_title('Timings for the test-case "'+str(arTitle)+'". Total time: '+str(sum(map(int,Timings_sorted)))+' seconds.', y=1.08, size = my_fontsize)

   #Save figure
   fig.savefig('TIMINGS_'+arTitle+'.pdf') 
   plt.close(fig)

def main():
   parser = argparse.ArgumentParser()
   parser.add_argument('stuff', nargs='+')
   args = parser.parse_args()
   testcases_to_run=args.stuff[1:]
   Timings = GetTimings(testcases_to_run)
   testcase_title=args.stuff[0]
   PlotTimings(testcase_title, testcases_to_run, Timings)
   

if __name__ == '__main__':
    main()


