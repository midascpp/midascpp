#!/bin/sh
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > pes_Adga_Dif.info <<%EOF%
   -------------
   Molecule:         Dicyclopropyl ketone
   Wave Function:    ORCA/HF-3c 
   Test Purpose:     Check implementation for the DIF-ADGA: 
                     double incremental scheme
                     in Falcon coordinates (DIF) 
                     with adaptive adaptive density-guided 
                     approach to grid construction (ADGA)
   -------------
%EOF%

#######################################################################
#  Check for ORCA existence
#######################################################################
if [ ! `which orca` ]; then
   orca_bin=`grep "^orca_bin" ES_PROGRAMS | cut -d '=' -f2`
   orca_bin=`echo $orca_bin | cut -d '"' -f2`

   export PATH=$orca_bin:$PATH

   if [ ! -x $orca_bin/orca ]; then
      echo $orca_bin
      echo "orca_bin path in ES_PROGRAMS MUST be right for this test to run"
      # 132 is TEST script error code!
      exit 132
   fi
fi

#######################################################################
#  Setup directories
#######################################################################

MAINDIR=$PWD/Dir_pes_Adga_Dif
export MAINDIR
rm -rf $MAINDIR

FRAGMENTDIR=$MAINDIR/FC_savedir
export FRAGMENTDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

MOLFILESDIR=$PWD/generic/generic_pes_Adga_Dif
export MOLFILESDIR

mkdir $MAINDIR
mkdir $FRAGMENTDIR
mkdir $INTERFACEDIR

#######################################################################
#  Define interface file names
#######################################################################

ESPINPUT=InputCreatorScript.sh

ESPRUN=RunScript.sh

PROPINFO=midasifc.propinfo

#######################################################################
#  MidasCpp input
#######################################################################
cat > pes_Adga_Dif.minp <<%EOF%
#0 MidasInput

#1 General
#2 IoLevel
5
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
orca_spc
#2 Type
SP_GENERIC
#2 InputCreatorScript
$ESPINPUT
#2 RunScript
$ESPRUN
#2 PropertyInfo
$PROPINFO
#2 BohrUnits

#1 Pes
#2 AdgaGridInitialDim
8 
#2 NiterMax
2
#2 FitBasis
#3 AddFitFuncsConserv

#2 MultilevelPes
orca_spc 1
#2 DoubleIncremental
DIF
FcInfo
#2 AdgaInfo
1
#2 AnalyzeStates
[48*4]
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-6
#2 ItResDensThr 
1.0e-3
#2 ScreenOperCoef
OFF
0.0e0

#1 Vib
#2 Operator
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
prop_no_1.mop
#3 SetInfo
type=energy

#2 Basis
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 PrimBasisDensity
0.8
#3 MaxNoPrimBasisFunctions
150
#3 ScalBounds
1.5 30.0

#2 Vscf
#3 OccGroundState
#3 Threshold
1.0e-15
#3 ScreenZero
1.0e-21
      
#0 MidasInputEnd
%EOF%

#######################################################################
#  Fragment combination info                                          #
#######################################################################
cat > $FRAGMENTDIR/FcInfo << %EOF%
# FC Label  FC Subsystems  Interconnecting Modes  Auxiliary Modes
FC_0 20,     11    
FC_1 21,     11    
FC_2 22,     11    
%EOF%

#######################################################################
#  Molecule input files                                               #
#######################################################################
cp $MOLFILESDIR/* $FRAGMENTDIR/

#######################################################################
#  ORCA input creator script
#######################################################################
cat > $INTERFACEDIR/$ESPINPUT << %EOF%
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and ORCA                         #
#                                                                     #
#  Electronic structure model: HF-3c                                  #
#                                                                     #
#  Purpose: Create an ORCA input file                                 #
#                                                                     #
#######################################################################

# Give the Cartesian coordinates as input 
NAT=\`head -1 midasifc.xyz_input\`
echo " number of atoms" \$NAT

# Create an ORCA input script (orca.inp) to calculate gradient
echo "! Bohrs HF-3c VeryTightSCF" > orca.inp
echo "* xyz 0 1" >> orca.inp
cat midasifc.xyz_input | tail -\$NAT >> orca.inp
echo "END" >> orca.inp
%EOF%

#######################################################################
#  ORCA input run script
#######################################################################
cat > $INTERFACEDIR/$ESPRUN << %EOF%
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and ORCA                         #
#                                                                     #
#  Electronic structure model: HF-3c                                  #
#                                                                     #
#  Purpose: Run an ORCA input file and extract the final results      #
#                                                                     #
#######################################################################

# Define which properties should be extrated from the electronic structure calculation
create_property_file() 
{
   # Extract the energy
   energy=\`grep "FINAL SINGLE POINT ENERGY     " orca.out | awk '{print \$5}'\`

   # Store the energy for MidasCpp to read
   echo "GROUND_STATE_ENERGY     " \$energy > midasifc.prop_general
   
   NAT=\`grep "Atom   \|    basis set group =>" orca.out | wc -l \`
   echo \$NAT > midasifc.cartrot_xyz 
   echo " returned coordinates " >> midasifc.cartrot_xyz
   N=\$(expr \$NAT)
   AUtoANG=0.5291772108
   grep -A \$N "Coordinates:"  orca_property.txt | tail -\$N  | \
   awk 'BEGIN { }
        {printf ("%2s      %20.15f  %20.15f  %20.15f \n",\
                        \$1,\$2/'\$AUtoANG',\$3/'\$AUtoANG',\$4/'\$AUtoANG')}
         END { }' >> midasifc.cartrot_xyz 
}

# Change directory to the scratch directory
cd \$1

# Now start the electronic structure calculation
orca orca.inp > orca.out

# Extract property/properties
create_property_file
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/$PROPINFO << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),type=(energy)
%EOF%

#######################################################################
#  Check script
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did midas end correctly?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED CORRECTLY"

# Did we obtain the correct scaling factors, (#1 SCALEFACTORS)?
CRIT0=`$GREP "1\.32063212..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP "3\.73836379..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP "6\.94515124..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP "7\.70375710..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT4=`$GREP "1\.28514237..............E\-01" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[1]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=5
ERROR[1]="DID NOT OBTAIN THE CORRECT SCALING FACTORS"

# Did we obtain the correct linear operator coefficients, (#1 OPERATORTERMS)?
CRIT0=`$GREP "2\.030492................E\-04 Q\^2\(Q6\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP "4\.329507................E\-06 Q\^4\(Q11\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP "5\.124904................E\-04 Q\^1\(Q15\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP "6\.630626................E\-04 Q\^3\(Q46\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[2]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[2]=4
ERROR[2]="DID NOT OBTAIN THE CORRECT OPERATOR TERMS"

# VSCF: vscf00_0                 E =  1.6938496857315150E-01 a.u.
CRIT1=`$GREP "VSCF: energy_basis_0           E =  1\.693849..........E\-01" $log  |  wc -l` # 1
TEST[3]=`expr $CRIT1`
CTRL[3]=1
ERROR[3]="VSCF ENERGY NOT CORRECT"

PASSED=1
for i in 0 1 2 3
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Adga_Dif.check
#######################################################################
