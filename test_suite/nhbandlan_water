#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > nhbandlan_water.info <<%EOF%
   IR intensities
   Raman intensities
   -----------------
   Molecule:      water
   Wave Function: VCC[2]
   Test Purpose:  Check IR and Raman intensities for VCC using
                  NHerm Band Lanczos algorithm. 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > nhbandlan_water.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
    5 
#2 Time

#1 Vib

// Hamiltonian.
#2 Operator
   #3 Name
      h0
   #3 OperFile
      wh.mop 
   #3 SetInfo
      type=energy

// Dipole operators.
#2 Operator
   #3 Name
      x
   #3 OperFile
      wx.mop
   #3 SetInfo
      type=dipole_x
#2 Operator
   #3 Name
      y
   #3 OperFile
      wy.mop
   #3 SetInfo
      type=dipole_y
#2 Operator
   #3 Name
      z
   #3 OperFile
      wz.mop
   #3 SetInfo
      type=dipole_z

// Frequency 0.0428
#2 Operator    // This is operator input
   #3 Name
       xx
   #3 OperFile
       wxx.mop
   #3 SetInfo
       type=polarizability_xx frq_0=0.0428
#2 Operator    // This is operator input
   #3 Name
       yy
   #3 OperFile
       wyy.mop
   #3 SetInfo
       type=polarizability_yy frq_0=0.0428
#2 Operator    // This is operator input
   #3 Name
       zz
   #3 OperFile
       wzz.mop
   #3 SetInfo
       type=polarizability_zz frq_0=0.0428
#2 Operator    // This is operator input
   #3 Name
       xy
   #3 OperFile
       wxy.mop
   #3 SetInfo
       type=polarizability_xy frq_0=0.0428
#2 Operator    // This is operator input
   #3 Name
       xz
   #3 OperFile
       wxz.mop
   #3 SetInfo
       type=polarizability_xz frq_0=0.0428
#2 Operator    // This is operator input
   #3 Name
       yz
   #3 OperFile
       wyz.mop
   #3 SetInfo
       type=polarizability_yz frq_0=0.0428

#2 Basis
   #3 Name
      basis
   #3 HoBasis 
      10
   #3 IoLevel
      1

#2 Vscf
   #3 IoLevel
      1
   #3 Name 
      scf0
   #3 Oper
      h0 
   #3 Basis
      basis
   #3 Occup
      [3*0] 
   #3 Threshold
      1.0e-15
   #3 MaxIter
      20

#2 Vcc
   #3 Vcc
   #3 Transformer
      trf=v3 type=gen
   #3 UseOldVccSolver
   #3 VscfThreshold
      1.0e-15
   #3 LimitModalBasis
      [3*6]
   #3 MaxExci
      2
   #3 ItEqResidThr
      1.0e-9
   #3 Oper
      h0
   #3 Basis 
      basis 
   #3 VecStorage 
      0
   #3 ItEqMaxDim
      150
   #3 ItEqMaxIt
      60
   #3 ItEqBreakDim
      100
   #3 Rsp
      #4 IoLevel
         3  
      #4 ItEqResidThr
         1.0e-9
      #4 ItEqMaxIt
         100
      #4 ItEqMaxDim
         200
      #4 ItEqBreakDim
         150
      #4 ItEqResidThr
         1.0e-9
      #4 LambdaThreshold
         1.0e-9
      #4 Eigen val
         5
      #4 ADiagVccRefEnergy
      #4 RspFunc
          1 h0
         -1 x ForStates [0..1]
         -1 y ForStates [0..1]
         -1 z ForStates [0..1]
         -1 xx ForStates [0..1]
         -1 yy ForStates [0..1]
         -1 zz ForStates [0..1]
         -1 xy ForStates [0..1]
         -1 xz ForStates [0..1]
         -1 yz ForStates [0..1]
      #4 LanczosRsp
         ortho=full
         chainlen=215
         frq_range=(0:8000:5)
         order=2
         rsp=(nherm=true,order=2,opers=(x,y,z,xx,yy,zz,xy,xz,yz),gamma=10,name=water)
      #4 BandLanczosIR
         name=birspec blrsp=water analyze=minima
      #4 BandLanczosRaman
         name=bramspect blrsp=water freq=0.0428 analyze=minima

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATOR INPUTS
#######################################################################
cat > wh.mop << %EOF%
DALTON_FOR_MIDAS
 2.7010827352569322101772E-08    1     
 2.8424599776144532370381E-05    1     1     
 1.3386473085574834840372E-07    1     1     1     
-7.0940586738288402557373E-11    1     1     1     1     
 2.6339947112319350708276E-07    2     
 1.5633553209681849693879E-04    2     2     
 3.2519419619347900152206E-06    2     2     2     
 4.4874468585476279258728E-08    2     2     2     2     
-2.1032064978498965501785E-12    3     
 1.6460214658309268997982E-04    3     3     
-4.0927261579781770706177E-11    3     3     3     
 4.7789967538847122341394E-08    3     3     3     3     
 2.0551169654936529695988E-09    1     2     
-2.5825875127338804304600E-07    1     2     2     
-9.0590219770092517137527E-09    1     2     2     2     
-7.5936259236186742782593E-07    1     1     2     
-4.7161620386759750545025E-08    1     1     2     2     
 1.0610847311909310519695E-08    1     1     1     2     
-5.3134385780140291899443E-10    1     3     
-9.5156394763762364163995E-07    1     3     3     
 2.7625901566352695226669E-11    1     3     3     3     
 3.2969182939268648624420E-12    1     1     3     
-5.8064415497938171029091E-08    1     1     3     3     
 8.1001871876651421189308E-13    1     1     1     3     
 7.1281647251453250646591E-11    2     3     
 1.0034688898485910613090E-05    2     3     3     
-4.8942183639155700802803E-11    2     3     3     3     
 6.0367710830178111791611E-11    2     2     3     
 2.7796704671345651149750E-07    2     2     3     3     
-4.6895820560166612267494E-11    2     2     2     3     
-6.0538241086760535836220E-11    1     2     3     
-5.5532041187689173966646E-08    1     2     3     3     
-9.0949470177292823791504E-12    1     2     2     3     
-8.2650331023614853620529E-11    1     1     2     3     
%EOF%

cat > wx.mop << %EOF%
DALTON_FOR_MIDAS
-6.6842413704537194957048E-09    1     
 2.7495539082702343629487E-11    1     1     
-2.6577730496518208152656E-14    1     1     1     
-1.9243750023815340322887E-15    1     1     1     1     
 8.7031339808670034236256E-10    2     
 6.1701425681861050280839E-12    2     2     
 1.2638958758752200335762E-14    2     2     2     
-3.1465270588004975602716E-15    2     2     2     2     
-4.5339976190651877874024E-10    3     
-2.8781087697602811989638E-12    3     3     
-2.2037335532810066197077E-14    3     3     3     
-6.1553701643762560057115E-15    3     3     3     3     
-5.0966270013948344272623E-11    1     2     
-2.3065607893864664376150E-13    1     2     2     
-4.3501350609555308683704E-15    1     2     2     2     
 5.3229820683384212876683E-13    1     1     2     
 1.1974993275116593209433E-15    1     1     2     2     
-3.9632041703992253734432E-15    1     1     1     2     
 7.9850565047301312625167E-12    1     3     
-4.3111648587846334191188E-13    1     3     3     
-1.1618695020077829166329E-15    1     3     3     3     
 1.8745572242492096533842E-13    1     1     3     
-5.4883828439656145536767E-15    1     1     3     3     
-1.3329699935713902996253E-15    1     1     1     3     
-4.0115750349663257104998E-12    2     3     
-1.2951881256445723997279E-14    2     3     3     
 4.1679804139936543434478E-16    2     3     3     3     
-2.6757612433831219839872E-14    2     2     3     
-3.4093393532730237495279E-15    2     2     3     3     
 2.7575627639110927733580E-15    2     2     2     3     
 7.4893182621297349406972E-14    1     2     3     
-1.3630286434987975302275E-14    1     2     3     3     
 2.0054966422777370212496E-15    1     2     2     3     
 2.1152677182584804635099E-15    1     1     2     3     
%EOF%

cat > wy.mop << %EOF%
DALTON_FOR_MIDAS
-4.9001593381522873324146E-08    1     
 2.1395936980847815701582E-10    1     1     
-6.4726259905858915477629E-12    1     1     1     
-1.6155878275993810889543E-13    1     1     1     1     
 1.5000681274626974980791E-08    2     
 1.8566935688096359236747E-10    2     2     
-5.9880454340826453775545E-12    2     2     2     
-4.8299801838472880408841E-13    2     2     2     2     
 5.7958351563652948934546E-03    3     
-4.7253070033226318713560E-10    3     3     
-3.9184958257332258568795E-08    3     3     3     
-1.8600780637978431286683E-12    3     3     3     3     
-8.5428479653300186530542E-10    1     2     
 1.5127281214119169731967E-12    1     2     2     
 1.9547047756770437681680E-13    1     2     2     2     
 4.5488941888270788258312E-12    1     1     2     
-1.4716038715354042140313E-12    1     1     2     2     
-2.0063305201415759722620E-13    1     1     1     2     
-1.2390280233786450311828E-04    1     3     
 1.2157748766061793332938E-11    1     3     3     
 7.1190353993777488739170E-09    1     3     3     3     
 1.0311752102077492709853E-06    1     1     3     
-3.9046717248414353207409E-12    1     1     3     3     
 9.0275075175644259317664E-11    1     1     1     3     
 6.2527702367224866075723E-05    2     3     
 1.0908794700892343598753E-11    2     3     3     
 5.4826813748742875276321E-09    2     3     3     3     
-1.5422835796252698514763E-06    2     2     3     
-6.0163957149583779937529E-12    2     2     3     3     
 3.2687490526817075675581E-09    2     2     2     3     
-4.5407412490629239787410E-07    1     2     3     
-5.9409074881777712562325E-13    1     2     3     3     
 4.6225682445028004607934E-08    1     2     2     3     
 1.5683941400820855527343E-08    1     1     2     3     
%EOF%

cat > wz.mop << %EOF%
DALTON_FOR_MIDAS
 6.3833910875265020834490E-03    1     
-3.5784894862000982129757E-05    1     1     
 2.4662934638985944957312E-07    1     1     1     
-1.4804497627807222670526E-09    1     1     1     1     
-1.5543086316722742878937E-03    2     
-3.0080150906108826802665E-05    2     2     
 3.3112461927942149486626E-07    2     2     2     
 3.2061659993587454664521E-09    2     2     2     2     
 1.8193269113453425234184E-08    3     
 1.3248539434407291537354E-05    3     3     
-3.7019276533101219683886E-12    3     3     3     
 2.2643842356728782760911E-10    3     3     3     3     
 3.5258745710198269307512E-05    1     2     
 3.9485259595295474355225E-07    1     2     2     
-4.2361723018302654963918E-09    1     2     2     2     
-2.9966326708930068889458E-07    1     1     2     
-1.1178988401638889627066E-08    1     1     2     2     
 1.5950716747425985886366E-08    1     1     1     2     
-3.5149061439199158485280E-10    1     3     
-4.6105526063655588586698E-07    1     3     3     
-1.5549339593690092442557E-11    1     3     3     3     
 3.8671288393743452616036E-12    1     1     3     
-1.8701555859479412902147E-09    1     1     3     3     
 1.6409096303959813667461E-13    1     1     1     3     
 3.8473890739965099783149E-10    2     3     
 9.1314835959366291717743E-07    2     3     3     
 5.9419136277938378043473E-13    2     3     3     3     
 7.6125772352497733663768E-12    2     2     3     
 5.1161919145670253783464E-10    2     2     3     3     
-5.9141136432572238845751E-11    2     2     2     3     
 1.7475798586019664071500E-11    1     2     3     
-7.3213257678617083001882E-09    1     2     3     3     
 1.0097522817886783741415E-10    1     2     2     3     
 6.2116534138567658374086E-11    1     1     2     3     
%EOF%

cat > wxx.mop <<%EOF%
DALTON_FOR_MIDAS
 2.5441038218332324000000E-06    1     
 9.8557218136363645000000E-04    1     1     
 1.2067147281413781000000E-10    1     1     1     
 5.2907971337390336000000E-08    1     1     1     1     
-1.8479616998935811000000E-01    2     
 1.9357690421415441000000E-03    2     2     
-6.9976152730077956000000E-06    2     2     2     
 4.0639399401243281000000E-08    2     2     2     2     
-1.7457451027116377000000E-02    3     
 8.9410747154516912000000E-04    3     3     
-1.5493664324850442000000E-06    3     3     3     
-2.8331855972396625000000E-08    3     3     3     3     
-2.6635987637746439000000E-08    1     2     
 1.2353496003925102000000E-10    1     2     2     
-3.8141934055602178000000E-11    1     2     2     2     
-7.9818166938139257000000E-06    1     1     2     
 1.6386070456064772000000E-07    1     1     2     2     
-1.6463275187561521000000E-11    1     1     1     2     
-1.3069804793985895000000E-08    1     3     
 5.7944760101236170000000E-11    1     3     3     
-8.2138740253867581000000E-12    1     3     3     3     
-2.7566836848791354000000E-06    1     1     3     
 3.3429728318878915000000E-08    1     1     3     3     
-1.8260948309034575000000E-11    1     1     1     3     
 9.6160972728753791000000E-04    2     3     
-3.2169128800774160000000E-06    2     3     3     
 1.2203574328850664000000E-07    2     3     3     3     
-1.6462656589055769000000E-05    2     2     3     
 1.4712790630255768000000E-07    2     2     3     3     
 1.1711763647781481000000E-07    2     2     2     3     
 3.7684344533772673000000E-10    1     2     3     
-5.6090243560902309000000E-11    1     2     3     3     
-2.8407498575688805000000E-11    1     2     2     3     
 7.4806152383644076000000E-07    1     1     2     3     
%EOF%

cat > wxy.mop << %EOF%
DALTON_FOR_MIDAS
-3.2967992066539514000000E-14    1     
-2.1188297832302964000000E-13    1     1     
 1.5896385997374524000000E-13    1     1     1     
 8.0632618716847413000000E-13    1     1     1     1     
 2.8583872148193491000000E-14    2     
 3.5151076507073191000000E-14    2     2     
-6.6574990054143258000000E-14    2     2     2     
-1.4501473333044124000000E-13    2     2     2     2     
-4.6067476000298337000000E-15    3     
 4.5548453191080475000000E-16    3     3     
 2.7017672277035772000000E-15    3     3     3     
-1.0813962930630262000000E-14    3     3     3     3     
-6.9780554449448889000000E-14    1     2     
 4.4312131557767515000000E-13    1     2     2     
 1.7975327541422345000000E-13    1     2     2     2     
-2.5788631145712035000000E-13    1     1     2     
 4.4984017875511844000000E-13    1     1     2     2     
 2.9630016002952817000000E-13    1     1     1     2     
 2.3285589006968011000000E-14    1     3     
 1.5640645625392743000000E-13    1     3     3     
-5.0695141213933688000000E-15    1     3     3     3     
 1.7680222210060923000000E-13    1     1     3     
 9.7276353940308855000000E-13    1     1     3     3     
 2.1929687995603865000000E-14    1     1     1     3     
-2.9825973093623530000000E-14    2     3     
-1.3325357541413398000000E-13    2     3     3     
 6.6649692252173825000000E-15    2     3     3     3     
 2.8194872289905393000000E-13    2     2     3     
-4.3284592433511449000000E-13    2     2     3     3     
 2.0339899384953624000000E-13    2     2     2     3     
-1.9013198914315056000000E-15    1     2     3     
 1.9806025641763725000000E-13    1     2     3     3     
-4.9645476724588787000000E-13    1     2     2     3     
 5.2238788003007040000000E-13    1     1     2     3 
%EOF%

cat > wxz.mop <<%EOF%    
DALTON_FOR_MIDAS
 9.5421270105186251000000E-02    1     
 2.3582021613233195000000E-08    1     1     
 4.1290703764462755000000E-07    1     1     1     
-9.0862872781372062000000E-12    1     1     1     1     
 1.3081667114667701000000E-06    2     
-2.3927368109181341000000E-08    2     2     
 1.0686102986724291000000E-10    2     2     2     
-2.3937932844065636000000E-11    2     2     2     2     
 5.8443136680933829000000E-08    3     
 3.6250982915781939000000E-10    3     3     
-2.3673571378732819000000E-12    3     3     3     
-9.2940364122900862000000E-12    3     3     3     3     
-1.7458376822515748000000E-03    1     2     
 8.3621603342853845000000E-06    1     2     2     
-4.1627855867920616000000E-07    1     2     2     2     
-2.2162272017567375000000E-10    1     1     2     
-9.5729202342909048000000E-11    1     1     2     2     
-3.3412632355345195000000E-07    1     1     1     2     
 5.8074746411390654000000E-04    1     3     
-3.4275937620259489000000E-06    1     3     3     
 4.5367538004104091000000E-08    1     3     3     3     
-8.9537127712091547000000E-11    1     1     3     
-8.8319518365409522000000E-11    1     1     3     3     
-1.6748530601295997000000E-07    1     1     1     3     
 6.8924173000825018000000E-09    2     3     
-5.2654671090859466000000E-11    2     3     3     
-2.5942689209828900000000E-12    2     3     3     3     
 8.5710173137609221000000E-11    2     2     3     
-9.8720653512835105000000E-11    2     2     3     3     
-1.5847722958283123000000E-11    2     2     2     3     
 6.5215216452363389000000E-06    1     2     3     
-1.8091869119496451000000E-07    1     2     3     3     
-5.8793130730538934000000E-07    1     2     2     3     
-6.4667715626853806000000E-12    1     1     2     3     
%EOF%

cat > wyy.mop <<%EOF%
DALTON_FOR_MIDAS
 7.4049382803309527000000E-07    1     
-4.6147743084645754000000E-05    1     1     
 1.3095302620058646000000E-11    1     1     1     
-6.7131523451280373000000E-08    1     1     1     1     
-5.3954722495276997000000E-02    2     
 1.0951357630517577000000E-05    2     2     
 7.0888521719325581000000E-07    2     2     2     
 2.1117308790508105000000E-08    2     2     2     2     
-1.3277543348859666000000E-03    3     
 2.9066858559900766000000E-04    3     3     
 4.4738725257786882000000E-07    3     3     3     
-2.1911015224418406000000E-08    3     3     3     3     
-1.6214336540087970000000E-09    1     2     
-2.2964741219766438000000E-11    1     2     2     
 7.8806294823152712000000E-11    1     2     2     2     
-7.6404680271480174000000E-07    1     1     2     
-7.1628392106504180000000E-09    1     1     2     2     
 5.6751048305159202000000E-11    1     1     1     2     
-1.3915339991399378000000E-09    1     3     
-1.7422507880837657000000E-11    1     3     3     
 3.9808156770959613000000E-12    1     3     3     3     
 1.6584047983769779000000E-06    1     1     3     
-7.2917714533105027000000E-08    1     1     3     3     
 6.3664629124104977000000E-12    1     1     1     3     
 7.1015453421807706000000E-05    2     3     
 1.5299229971788009000000E-06    2     3     3     
 2.9065166273767318000000E-08    2     3     3     3     
-9.1093500032002339000000E-07    2     2     3     
-2.5693140059956932000000E-08    2     2     3     3     
-2.8529036910640571000000E-08    2     2     2     3     
 5.3013593515061075000000E-11    1     2     3     
 4.4465764403867070000000E-11    1     2     3     3     
 8.7155171968333889000000E-11    1     2     2     3     
-1.7661291451531724000000E-07    1     1     2     3     
%EOF%

cat > wyz.mop <<%EOF%
DALTON_FOR_MIDAS
-4.7646529813135450000000E-15    1     
 5.6436623973083661000000E-14    1     1     
-4.8949304334970530000000E-14    1     1     1     
-1.5763966751170447000000E-13    1     1     1     1     
 1.6429139311216500000000E-14    2     
 1.7465593568244010000000E-13    2     2     
-7.5949706455970120000000E-14    2     2     2     
-5.5657098811340681000000E-13    2     2     2     2     
 9.0132844461677985000000E-15    3     
 1.2050723667576241000000E-14    3     3     
-4.5914873115539140000000E-14    3     3     3     
 1.2590812923640013000000E-14    3     3     3     3     
 6.0746829912533207000000E-14    1     2     
 1.1308608231884239000000E-14    1     2     2     
-3.7454007973711451000000E-13    1     2     2     2     
 2.3740004115079187000000E-14    1     1     2     
-7.8880545159624345000000E-13    1     1     2     2     
-1.2758073392600520000000E-13    1     1     1     2     
 8.2780901317185954000000E-16    1     3     
 2.3445824104066207000000E-14    1     3     3     
-5.9496553788831024000000E-14    1     3     3     3     
-1.7417292443109974000000E-13    1     1     3     
-1.0956659497865972000000E-13    1     1     3     3     
 5.4609927421801188000000E-15    1     1     1     3     
 3.5361434283503818000000E-14    2     3     
-1.2678379666135141000000E-13    2     3     3     
-4.4652434104050862000000E-14    2     3     3     3     
-2.9407227536397091000000E-13    2     2     3     
-2.1013375984052147000000E-13    2     2     3     3     
-1.6037971460749899000000E-13    2     2     2     3     
 1.2207727893147382000000E-13    1     2     3     
-8.3675811444013323000000E-13    1     2     3     3     
 3.6233447983991524000000E-13    1     2     2     3     
-1.1710073320265957000000E-12    1     1     2     3     
%EOF%

cat > wzz.mop <<%EOF%
DALTON_FOR_MIDAS
 1.5922972309567740000000E-06    1     
 5.6194941623033401000000E-04    1     1     
 6.5547567373869242000000E-11    1     1     1     
 1.4322408503630868000000E-07    1     1     1     1     
-1.1701493213061198000000E-01    2     
 4.5847118680342192000000E-04    2     2     
 2.6069217469171235000000E-06    2     2     2     
 2.3396426485078337000000E-07    2     2     2     2     
 1.9433635768077551000000E-02    3     
 8.2160090155269927000000E-04    3     3     
 3.5927129733437368000000E-06    3     3     3     
-6.5151597894441693000000E-08    3     3     3     3     
 3.4413112359743536000000E-09    1     2     
-3.2670754990249407000000E-10    1     2     2     
 5.9173999034101143000000E-11    1     2     2     2     
-6.0251634010910493000000E-06    1     1     2     
 1.3183279747863708000000E-06    1     1     2     2     
 3.7516656448133290000000E-12    1     1     1     2     
 1.3436359580509816000000E-08    1     3     
-9.6697760909592034000000E-11    1     3     3     
 6.7039707118965453000000E-12    1     3     3     3     
 6.0391143534843650000000E-06    1     1     3     
 1.1602881500039075000000E-07    1     1     3     3     
 1.6704859717719955000000E-11    1     1     1     3     
-1.0036919606370986000000E-03    2     3     
 6.1537349509421801000000E-06    2     3     3     
 8.7755349653662051000000E-08    2     3     3     3     
-1.5099784320682375000000E-06    2     2     3     
 1.5060237501529627000000E-07    2     2     3     3     
 2.4205295545698391000000E-07    2     2     2     3     
 1.8383161659585312000000E-10    1     2     3     
 4.5929482439532876000000E-11    1     2     3     3     
 2.6133761821256485000000E-11    1     2     2     3     
 6.9311212769207486000000E-07    1     1     2     3
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is Band Lanczos Rsp done?
CRIT11=`$GREP "Response function calculations using Lanczos chains done." $log | wc -l`
TEST[1]=`expr $CRIT11`
CTRL[1]=1
ERROR[1]="NH-BAND LANCZOS RESPONSE NOT COMPLETED"

# Is the number of Lanczos iterations correct?
CRIT12=`$GREP "Stopped after 90 iterations." $log | wc -l`
CRIT22=`$GREP "The Krylov Space is fully deflated." $log | wc -l`
TEST[2]=`expr $CRIT12 + $CRIT22`
CTRL[2]=2
ERROR[2]="NUMBER OF ITERATIONS NOT CORRECT/NOT DEFLATED!"

# Is the number oof blocks correct
CRIT13=`$GREP "Number of blocks: 12" $log | wc -l`
CRIT23=`$GREP "Number of blocks: 13" $log | wc -l`
TEST[3]=`expr $CRIT13 + $CRIT23`
CTRL[3]=2
ERROR[3]="NUMBER OF BLOCKS IN NH BANDLANCZOS IR/RAMAN ANALYSIS INCORRECT"

# IR activities (results in km/mol compared):
CRIT14=`$GREP "Integrated intensity = 0.0000452350890" $log | wc -l`
CRIT24=`$GREP "Integrated intensity = 0.0000033705744" $log | wc -l`
TEST[4]=`expr \
$CRIT14 \
\+ $CRIT24`
CTRL[4]=2 
ERROR[4]="IR INTENSITIES NOT CORRECT"

# Raman activities (results in km/mol compared):
CRIT15=`$GREP "Integrated intensity = 0.0300809063073" $log | wc -l`
CRIT25=`$GREP "Integrated intensity = 0.0817864082947" $log | wc -l`
TEST[5]=`expr \
$CRIT15 \
\+ $CRIT25`
CTRL[5]=2 
ERROR[5]="RAMAN INTENSITIES NOT CORRECT"


PASSED=1
for i in 1 2 3 4 5
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}.
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > nhbandlan_water.check
#######################################################################

