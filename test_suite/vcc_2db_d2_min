#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    VCC / HO basis 
   Test Purpose:     Check FVCC energy 
   Reference:        Henon Heiles potential, test dates back to Bowman 78,
                     and Norris et al. JCM 1996
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc_2db_d2_min.minp <<%EOF%

#0 MIDAS Input

#1 general
#2 IoLevel
  1
#2 BufSIZE
 4
#2 FileSize
 30
#2 Time

#1 Vib
#2 IoLevel
   3

#2 Operator
#3 Name
 2d_bowman78
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
.146875 Q^2(a)
1.062905 Q^2(b)
-0.009390024 Q^3(a)
-0.1116 Q^1(a) Q^2(b)
#3 KineticEnergy
 USER_DEFINED
#3 Duplicate
  2

#2 Basis
#3 Name
 basis
#3 Define
 a HO n_high=3,omeg=0.541987084716970
 b HO n_high=3,omeg=1.458015774948954
#3 IoLevel
   1
#3 Duplicate
  2

#2 Vscf
#3 IoLevel
 1
#3 Occup
 0 0 
#3 Duplicate
  2

#2 Vcc
#3 LimitModalBasis
 4 4 
#3 Occup
 0 0 
#3 IoLevel
 5
#3 Vcc 
#3 Transformer
   trf=orig outofspace=true t1transh=false
#3 Iso
 1
#3 ITEQRESIDTHR
 1.0e-12
#3 ITEQMAXIT
 30  
#3 Duplicate
  2

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT31=`$GREP "VCC" $log | grep "1.983341864717" | wc -l`
TEST[3]=`expr \
$CRIT31 \
`
CTRL[3]=1
ERROR[3]="VCC ENERGY NOT CORRECT"
echo $CRIT31

PASSED=1
for i in 3 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vcc_2db_d2_min.check
#######################################################################
