#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="tdvcc_compare_exotic"

#######################################################################
#  Input
#######################################################################
cat > ${TESTNAME}.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water 2M-coupled
   Wave Function:    TDVCC/VCI/EVCC[2,3] (MatRep)
   Test Purpose:     Compare propagation of TDVCC states.
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
# Notes.
# Propagation time; oscillation periods are roughly
#  q0: 410 au
#  q1: 375 au
#  q2: 850 au
# Thus t_end = 50 au corresponds to ~1/8 oscillation for q0, q1, ~1/16 for q2.
# This seems to be a good point to check expectation values and such; they are
# substantially different from initial values (zero), and have propagated
# enough that errors can be detected. But not for too long, so the test can run
# relatively fast.
n_modals="3"
t_end="50"
tolerance="1.e-15"
corr_types="vci vcc extvcc"
coup_levels="3 2"
n_calcs="6"
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0_init
   #3 OperFile
      ${TESTNAME}_init.mop
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ${TESTNAME}.mop
   #3 SetInfo
      type=energy
%EOF%
for m in 0 1 2
do
   cat >> ${TESTNAME}.minp <<%EOF%
#2 Operator
   #3 Name
      q${m}
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q${m})
#2 Operator
   #3 Name
      ip${m}
   #3 OperInput
   #4 OperatorTerms
      1.0 DDQ^1(Q${m})
%EOF%
done
cat >> ${TESTNAME}.minp <<%EOF%
#2 Basis
   #3 Name
      basis
   #3 BSplineBasis
      10
   #3 PrimBasisDensity
      0.8
   #3 TurningPoint
      10
#2 Vscf
   #3 Name
      vscf_init
   #3 Basis
      basis
   #3 Oper
      h0_init
   #3 Threshold
      1.0e-15
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      basis
   #3 Oper
      h0
   #3 Threshold
      1.0e-15

%EOF%

for coup_level in $coup_levels
do
   for corr_type in $corr_types
   do
      cat >> ${TESTNAME}.minp << %EOF%
#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      td${corr_type}${coup_level}
   #3 CorrMethod
      ${corr_type}
   #3 MaxExci
      ${coup_level}
   #3 Transformer
      matrep
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [3*${n_modals}]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. ${t_end}
      #4 OutputPoints
         2
      #4 Tolerance
         ${tolerance} ${tolerance}
      #4 MaxSteps
         -1
   #3 Properties
      energy
      phase
      autocorr
      autocorr_ext
   #3 Statistics
      fvcinorm2
      norm2
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      q2
      ip0
      ip1
      ip2
   #3 CompareFvciVecs

%EOF%
   done
done

cat >> ${TESTNAME}.minp <<%EOF%
#0 Midas Input End
%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASOPERATOR
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
-1.3983481039758772E-11    Q^1(Q0)
 1.5756874643102492E-04    Q^2(Q0)
-1.3022827261011116E-10    Q^3(Q0)
 4.6707384626643034E-08    Q^4(Q0)
 4.5483926669476205E-07    Q^1(Q1)
 1.4937195226139011E-04    Q^2(Q1)
 3.0827675345790340E-06    Q^3(Q1)
 4.3788077164208516E-08    Q^4(Q1)
 3.7033530020380567E-08    Q^1(Q2)
 2.7465351735145305E-05    Q^2(Q2)
 1.4056156771857786E-07    Q^3(Q2)
-5.4826898576720851E-10    Q^4(Q2)
 2.2424728740588762E-10    Q^1(Q0)     Q^1(Q1)
 1.3244516594568267E-10    Q^1(Q0)     Q^2(Q1)
 1.0004441719502211E-11    Q^1(Q0)     Q^3(Q1)
 9.5331590728164883E-06    Q^2(Q0)     Q^1(Q1)
 2.7097757993033156E-07    Q^2(Q0)     Q^2(Q1)
-4.0927261579781771E-12    Q^3(Q0)     Q^1(Q1)
 1.6018475434975699E-10    Q^1(Q0)     Q^1(Q2)
 1.0061285138363019E-11    Q^1(Q0)     Q^2(Q2)
-1.2647660696529783E-12    Q^1(Q0)     Q^3(Q2)
-9.1266440449544461E-07    Q^2(Q0)     Q^1(Q2)
-5.4972815632936545E-08    Q^2(Q0)     Q^2(Q2)
-7.3896444519050419E-13    Q^3(Q0)     Q^1(Q2)
-2.8100544113840442E-10    Q^1(Q1)     Q^1(Q2)
-7.2744757062537246E-07    Q^1(Q1)     Q^2(Q2)
 9.8503392109705601E-09    Q^1(Q1)     Q^3(Q2)
-2.2016263301338768E-07    Q^2(Q1)     Q^1(Q2)
-4.5356273403740488E-08    Q^2(Q1)     Q^2(Q2)
-9.0807930064329412E-09    Q^3(Q1)     Q^1(Q2)
#0MIDASOPERATOREND
%EOF%

cat > ${TESTNAME}_init.mop << %EOF%
#0MIDASOPERATOR
#1SETINFO
type=energy
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
 1.5756874643102492E-04    Q^2(Q0)
 1.4937195226139011E-04    Q^2(Q1)
 2.7465351735145305E-05    Q^2(Q2)
#0MIDASOPERATOREND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Utilities
# ------------------------------------------------------------------------------
# For asserting (absolute) deviations.
#   $1: value
#   $2: expected
#   $3: (abs) threshold
#   return: 1 if dev <= threshold, 0 otherwise.
accept_dev()
{
   awk '\''
      function abs(a){return a>0? a: -a}
      function dev(a,b){return abs(a-b)}
      function accept(a,b,thr){return dev(a,b) <= abs(thr)? 1: 0}
      BEGIN{printf("%d",accept('\''$1'\'','\''$2'\'','\''$3'\''))}
   '\''
}

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Number and order of propagations
# ------------------------------------------------------------------------------
calcs=($($GREP "TDVCC propagation done for" $log | awk -F\'\'' '\''{print $2}'\''))
calcs_ctrl=(tdvci3 tdvcc3 tdextvcc3 tdvci2 tdvcc2 tdextvcc2)
TEST+=(${#calcs[*]})
CTRL+=(${#calcs_ctrl[*]})
ERROR+=("Unexpected number of TDVCC propagations.")

for i in $(seq 0 $(expr ${#calcs_ctrl[*]} - 1))
do
   if test "${calcs[$i]}" = "${calcs_ctrl[$i]}"
   then
      TEST+=(1)
   else
      TEST+=(0)
   fi
   CTRL+=(1)
   ERROR+=("Unexpected propagation name, i = $i; got '\''${calcs[$i]}'\'', exp. '\''${calcs_ctrl[$i]}'\''.")
done

# ------------------------------------------------------------------------------
# Energy conservation
# ------------------------------------------------------------------------------
# All calculations should start at same energy.
TEST+=($($GREP -c "ENERGY, at t_beg *= *2\.146534381754....E-02" $log))
CTRL+=(${#calcs_ctrl[*]})
ERROR+=("Wrong '\''ENERGY, at t_beg'\'' for one or more calculations.")

# All calculations should conserve energy.
e_span_re=($($GREP "ENERGY, span.real." $log | awk '\''{print $NF}'\''))
e_span_im=($($GREP "ENERGY, span.imag." $log | awk '\''{print $NF}'\''))

e_span_tol_re="1.e-16"
e_span_tol_im="1.e-16"
for i in $(seq 0 $(expr ${#e_span_re[*]} - 1))
do
   TEST+=($(accept_dev ${e_span_re[$i]} 0.0 $e_span_tol_re))
   TEST+=($(accept_dev ${e_span_im[$i]} 0.0 $e_span_tol_im))
   CTRL+=(1)
   CTRL+=(1)
   ERROR+=("'\''ENERGY, span(real)'\''; ${e_span_re[$i]} > $e_span_tol_re, i = $i (${calcs[i]}).")
   ERROR+=("'\''ENERGY, span(imag)'\''; ${e_span_im[$i]} > $e_span_tol_im, i = $i (${calcs[i]}).")
done

# ------------------------------------------------------------------------------
# Autocorrelation
# ------------------------------------------------------------------------------
# NB! The 3-mode calcs. should all be equivalent (exact limit)!
ctrl_ac_avg_end_re=(
4.7819532371587598E-01
4.7819532371587548E-01
4.7819532371587564E-01
4.7820904088073674E-01
4.7820273546045894E-01
4.7820080008767868E-01
)
ctrl_ac_avg_end_im=(
-8.6681026929570337E-01
-8.6681026929570382E-01
-8.6681026929570382E-01
-8.6680770752492631E-01
-8.6680113805776482E-01
-8.6679923964750105E-01
)
ac_avg_end_re=($($GREP "AUTOCORR .avg., at t_end" $log | awk '\''{print $6}'\''))
ac_avg_end_im=($($GREP "AUTOCORR .avg., at t_end" $log | awk -F'\''[()]'\'' '\''{print $4}'\''))

ac_avg_end_tol_re="5.e-14"
ac_avg_end_tol_im="5.e-15"
for i in $(seq 0 $(expr ${#ac_avg_end_re[*]} - 1))
do
   TEST+=($(accept_dev ${ac_avg_end_re[$i]} ${ctrl_ac_avg_end_re[$i]} $ac_avg_end_tol_re))
   TEST+=($(accept_dev ${ac_avg_end_im[$i]} ${ctrl_ac_avg_end_im[$i]} $ac_avg_end_tol_im))
   CTRL+=(1)
   CTRL+=(1)
   ERROR+=("'\''AUTOCORR .avg., at t_end'\'' (re); |${ac_avg_end_re[$i]} - (${ctrl_ac_avg_end_re[$i]})| > $ac_avg_end_tol_re, i = $i (${calcs[i]}).")
   ERROR+=("'\''AUTOCORR .avg., at t_end'\'' (im); |${ac_avg_end_im[$i]} - (${ctrl_ac_avg_end_im[$i]})| > $ac_avg_end_tol_im, i = $i (${calcs[i]}).")
done

# ------------------------------------------------------------------------------
# Autocorrelation, "extended"
# ------------------------------------------------------------------------------
ctrl_ac_avg_end_re=(
4.7819532371587598E-01
4.7819532371587548E-01
4.7819532371587564E-01
4.7820904088073674E-01
4.7820435632900071E-01
4.7819914119238166E-01
)
ctrl_ac_avg_end_im=(
-8.6681026929570337E-01
-8.6681026929570382E-01
-8.6681026929570382E-01
-8.6680770752492631E-01
-8.6680369936233459E-01
-8.6679664061657824E-01
)
ac_avg_end_re=($($GREP "AUTOCORR_EXT .avg., at t_end" $log | awk '\''{print $6}'\''))
ac_avg_end_im=($($GREP "AUTOCORR_EXT .avg., at t_end" $log | awk -F'\''[()]'\'' '\''{print $4}'\''))

ac_avg_end_tol_re="5.e-14"
ac_avg_end_tol_im="5.e-15"
for i in $(seq 0 $(expr ${#ac_avg_end_re[*]} - 1))
do
   TEST+=($(accept_dev ${ac_avg_end_re[$i]} ${ctrl_ac_avg_end_re[$i]} $ac_avg_end_tol_re))
   TEST+=($(accept_dev ${ac_avg_end_im[$i]} ${ctrl_ac_avg_end_im[$i]} $ac_avg_end_tol_im))
   CTRL+=(1)
   CTRL+=(1)
   ERROR+=("'\''AUTOCORR_EXT .avg., at t_end'\'' (re); |${ac_avg_end_re[$i]} - (${ctrl_ac_avg_end_re[$i]})| > $ac_avg_end_tol_re, i = $i (${calcs[i]}).")
   ERROR+=("'\''AUTOCORR_EXT .avg., at t_end'\'' (im); |${ac_avg_end_im[$i]} - (${ctrl_ac_avg_end_im[$i]})| > $ac_avg_end_tol_im, i = $i (${calcs[i]}).")
done

# ------------------------------------------------------------------------------
# Expectation values (q, ip)
# ------------------------------------------------------------------------------
# NB! The 3-mode calcs. should all be equivalent (exact limit)!
# NB! We check modes 1 and 2 (ip1 and q2) where most dynamics happen.
# NB! For the physical quantities, take imag. part of ip, real part of q.
ctrl_ip1_end=(
-2.0562402679169402E-02
-2.0562402679169398E-02
-2.0562402679169388E-02
-2.0548976716929013E-02
-2.0558443789986544E-02
-2.0562499813808994E-02
)
ctrl_q2_end=(
6.9636666200398305E-03
6.9636666200397837E-03
6.9636666200397846E-03
7.7905124138464123E-03
6.9941874910109348E-03
6.9565887060192549E-03
)
ip1_end=($($GREP "ip1, at t_end" $log | awk -F'\''[()]'\'' '\''{print $2}'\''))
q2_end=($($GREP "q2, at t_end" $log | awk '\''{print $5}'\''))

ip1_end_tol="5.e-14"
for i in $(seq 0 $(expr ${#ip1_end[*]} - 1))
do
   TEST+=($(accept_dev ${ip1_end[$i]} ${ctrl_ip1_end[$i]} $ip1_end_tol))
   CTRL+=(1)
   ERROR+=("'\''ip1, at t_end'\'' (im); |${ip1_end[$i]} - (${ctrl_ip1_end[$i]})| > $ip1_end_tol, i = $i (${calcs[$i]}).")
done

q2_end_tol="2.0e-12"
for i in $(seq 0 $(expr ${#q2_end[*]} - 1))
do
   TEST+=($(accept_dev ${q2_end[$i]} ${ctrl_q2_end[$i]} $q2_end_tol))
   CTRL+=(1)
   ERROR+=("'\''q2, at t_end'\''; |${q2_end[$i]} - (${ctrl_q2_end[$i]})| > $q2_end_tol, i = $i (${calcs[$i]}).")
done

# ------------------------------------------------------------------------------
# FVCI comparisons
# ------------------------------------------------------------------------------
# Read file names containing the FVCI comparisons (ket and bra).
# Extract numbers from last line (at t_end).
# The columns are the |v-r|_i values, where
#     i: index of the calculation, i = 0 being the reference, here tdvci3
#     r: reference FVCI ket/bra (i.e. the i = 0 one)
#     v: the FVCI ket/bra from the i=1,... calculations
# |v-r| is then the distance between the FVCI vectors.
# angle(r,v) is the acos(|<r|v>|/|r||v|) angle between r and v.
# The 3-mode calcs. are all in the exact limit, so they should give approx. zero.

ctrl_fvci_ket_dnorm=(
0.0   # VCI[3] (Not used.)
0.0   # VCC[3]
0.0   # ExtVCC[3]
7.1113267416494643e-03  # VCI[2]
4.8867744404010802e-03  # VCC[2]
4.8835070484392435e-03  # ExtVCC[2]
)
ctrl_fvci_ket_angle=(
0.0   # VCI[3] (Not used.)
0.0   # VCC[3]
0.0   # ExtVCC[3]
7.1113159035719707e-03  # VCI[2]
4.8867659137591017e-03  # VCC[2]
4.8835034707738643e-03  # ExtVCC[2]
)
tol_fvci_ket=(
0.0
2.e-15
2.e-15
1.e-13
1.e-13
1.e-13
)
ctrl_fvci_bra_dnorm=(
0.0   # VCI[3] (Not used.)
0.0   # VCC[3]
0.0   # ExtVCC[3]
7.1113267416494643e-03  # VCI[2]
7.1980154769758992e-03  # VCC[2]
4.9419307296063864e-03  # ExtVCC[2]
)
ctrl_fvci_bra_angle=(
0.0   # VCI[3] (Not used.)
0.0   # VCC[3]
0.0   # ExtVCC[3]
7.1113159035719707e-03  # VCI[2]
7.1980168847867879e-03  # VCC[2]
4.9419286676479664e-03  # ExtVCC[2]
)
tol_fvci_bra=(
0.0
2.e-15
2.e-15
1.e-13
1.e-13
1.e-13
)

file_fvci_ket=$($GREP "FVCI-kets *.file:" $log | awk -F'\''[:)]'\'' '\''{print $2}'\'' | awk '\''{print $1}'\'')
file_fvci_bra=$($GREP "FVCI-bras *.file:" $log | awk -F'\''[:)]'\'' '\''{print $2}'\'' | awk '\''{print $1}'\'')

# Diff norms
col=5
for i in $(seq 1 $(expr ${#calcs[*]} - 1))
do
   val_ket=$(tail -n1 $file_fvci_ket | awk '\''{print $'\''$col'\''}'\'')
   val_bra=$(tail -n1 $file_fvci_bra | awk '\''{print $'\''$col'\''}'\'')
   TEST+=($(accept_dev $val_ket ${ctrl_fvci_ket_dnorm[$i]} ${tol_fvci_ket[$i]}))
   TEST+=($(accept_dev $val_bra ${ctrl_fvci_bra_dnorm[$i]} ${tol_fvci_bra[$i]}))
   CTRL+=(1)
   CTRL+=(1)
   ERROR+=("Wrong '\''|v-r|_$i'\'' (last line, column $col of file '\''$file_fvci_ket'\''; |$val_ket - (${ctrl_fvci_ket_dnorm[$i]})| > ${tol_fvci_ket[$i]}")
   ERROR+=("Wrong '\''|v-r|_$i'\'' (last line, column $col of file '\''$file_fvci_bra'\''; |$val_bra - (${ctrl_fvci_bra_dnorm[$i]})| > ${tol_fvci_bra[$i]}")
   col=$(expr $col + 4)
done

# Angles
col=2
for i in $(seq 1 $(expr ${#calcs[*]} - 1))
do
   val_ket=$(tail -n1 $file_fvci_ket | awk '\''{print $'\''$col'\''}'\'')
   val_bra=$(tail -n1 $file_fvci_bra | awk '\''{print $'\''$col'\''}'\'')
   TEST+=($(accept_dev $val_ket ${ctrl_fvci_ket_angle[$i]} ${tol_fvci_ket[$i]}))
   TEST+=($(accept_dev $val_bra ${ctrl_fvci_bra_angle[$i]} ${tol_fvci_bra[$i]}))
   CTRL+=(1)
   CTRL+=(1)
   ERROR+=("Wrong '\''angle(r,v)_$i'\'' (last line, column $col of file '\''$file_fvci_ket'\''; |$val_ket - (${ctrl_fvci_ket_angle[$i]})| > ${tol_fvci_ket[$i]}")
   ERROR+=("Wrong '\''angle(r,v)_$i'\'' (last line, column $col of file '\''$file_fvci_bra'\''; |$val_bra - (${ctrl_fvci_bra_angle[$i]})| > ${tol_fvci_bra[$i]}")
   col=$(expr $col + 4)
done



# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
