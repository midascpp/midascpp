#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="timtdvcc2_gentrf_2m_water"

#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water 2M-coupled
   Wave Function:    TIM-TDVCC
   Test Purpose:     Propagate TIM-TDVCC state.
   Reference:
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0_init
   #3 OperFile
      ${TESTNAME}_init.mop
   #3 SetInfo
      type=energy
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ${TESTNAME}.mop
   #3 SetInfo
      type=energy
#2 Operator
   #3 Name
      q0
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q0)
#2 Operator
   #3 Name
      q1
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q1)
#2 Operator
   #3 Name
      q2
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q2)
#2 Basis
   #3 Name
      basis
   #3 BSplineBasis
      10
   #3 PrimBasisDensity
      0.8
   #3 TurningPoint
      10
#2 Vscf
   #3 Name
      vscf_init
   #3 Basis
      basis
   #3 Oper
      h0_init
   #3 Threshold
      1.0e-15
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      basis
   #3 Oper
      h0
   #3 Threshold
      1.0e-15

#2 TimTdvcc
   #3 IoLevel
      10
   #3 Name
      timtdvcc
   #3 CorrMethod
      vcc
   #3 MaxExci
      2
   #3 Transformer
      general
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [3*6]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e3
      #4 OutputPoints
         1000
      #4 Tolerance
         1.e-7 1.e-7
      #4 MaxSteps
         -1
   #3 Properties
      energy
      phase
      autocorr
   #3 Statistics
      fvcinorm2
      norm2
      dnorm2init
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      q2

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASOPERATOR
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
-1.3983481039758772E-11    Q^1(Q0)
 1.5756874643102492E-04    Q^2(Q0)
-1.3022827261011116E-10    Q^3(Q0)
 4.6707384626643034E-08    Q^4(Q0)
 4.5483926669476205E-07    Q^1(Q1)
 1.4937195226139011E-04    Q^2(Q1)
 3.0827675345790340E-06    Q^3(Q1)
 4.3788077164208516E-08    Q^4(Q1)
 3.7033530020380567E-08    Q^1(Q2)
 2.7465351735145305E-05    Q^2(Q2)
 1.4056156771857786E-07    Q^3(Q2)
-5.4826898576720851E-10    Q^4(Q2)
 2.2424728740588762E-10    Q^1(Q0)     Q^1(Q1)
 1.3244516594568267E-10    Q^1(Q0)     Q^2(Q1)
 1.0004441719502211E-11    Q^1(Q0)     Q^3(Q1)
 9.5331590728164883E-06    Q^2(Q0)     Q^1(Q1)
 2.7097757993033156E-07    Q^2(Q0)     Q^2(Q1)
-4.0927261579781771E-12    Q^3(Q0)     Q^1(Q1)
 1.6018475434975699E-10    Q^1(Q0)     Q^1(Q2)
 1.0061285138363019E-11    Q^1(Q0)     Q^2(Q2)
-1.2647660696529783E-12    Q^1(Q0)     Q^3(Q2)
-9.1266440449544461E-07    Q^2(Q0)     Q^1(Q2)
-5.4972815632936545E-08    Q^2(Q0)     Q^2(Q2)
-7.3896444519050419E-13    Q^3(Q0)     Q^1(Q2)
-2.8100544113840442E-10    Q^1(Q1)     Q^1(Q2)
-7.2744757062537246E-07    Q^1(Q1)     Q^2(Q2)
 9.8503392109705601E-09    Q^1(Q1)     Q^3(Q2)
-2.2016263301338768E-07    Q^2(Q1)     Q^1(Q2)
-4.5356273403740488E-08    Q^2(Q1)     Q^2(Q2)
-9.0807930064329412E-09    Q^3(Q1)     Q^1(Q2)
#0MIDASOPERATOREND
%EOF%

cat > ${TESTNAME}_init.mop << %EOF%
#0MIDASOPERATOR
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
 1.5756874643102492E-04    Q^2(Q0)
 1.4937195226139011E-04    Q^2(Q1)
 2.7465351735145305E-05    Q^2(Q2)
#0MIDASOPERATOREND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check energy average and std deviation
# ------------------------------------------------------------------------------
CRIT_ITER_EAVG[0]=`$GREP "ENERGY, avg \(real\)" $log | grep "2.146534148828....E-02" |  wc -l`
CRIT_ITER_EAVG[1]=`$GREP "ENERGY, avg \(imag\)" $log | grep "4.4910............E-12" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_EAVG[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''average energy (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_EAVG[$i].")
done
CRIT_ITER_ESTD[0]=`$GREP "ENERGY, std \(real\)" $log | grep "1.3503468.........E-09" |  wc -l`
CRIT_ITER_ESTD[1]=`$GREP "ENERGY, std \(imag\)" $log | grep "6.061.............E-12" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_ESTD[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''energy std deviation (key=$i : 0=real, 1=imag)'\'', CRIT_ITER_ESTD[$i].")
done

# ------------------------------------------------------------------------------
# Check min and max value of auto-correlation functions
# ------------------------------------------------------------------------------
CRIT_ITER_SAVG[0]=`$GREP "AUTOCORR \(avg\), min \(real\)" $log | grep "\-9.9863436498393...E-01" |  wc -l`
CRIT_ITER_SAVG[1]=`$GREP "AUTOCORR \(avg\), max \(real\)" $log | grep "1.0000000000000000E+00" |  wc -l`
CRIT_ITER_SAVG[2]=`$GREP "AUTOCORR \(avg\), min \(imag\)" $log | grep "\-9.9930361262685...E-01" |  wc -l`
CRIT_ITER_SAVG[3]=`$GREP "AUTOCORR \(avg\), max \(imag\)" $log | grep "9.846996206851....E-01" |  wc -l`
for i in $(seq 0 3)
do
   TEST+=(${CRIT_ITER_SAVG[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''average auto-correlation function (key=$i)'\'', CRIT_ITER_SAVG[$i].")
done
CRIT_ITER_SKET[0]=`$GREP "AUTOCORR \(<0\|t>\), min \(real\)" $log | grep "\-9.986356943919....E-01" |  wc -l`
CRIT_ITER_SKET[1]=`$GREP "AUTOCORR \(<0\|t>\), max \(real\)" $log | grep "1.0000000000000000E+00" |  wc -l`
CRIT_ITER_SKET[2]=`$GREP "AUTOCORR \(<0\|t>\), min \(imag\)" $log | grep "\-9.9930438558834...E-01" |  wc -l`
CRIT_ITER_SKET[3]=`$GREP "AUTOCORR \(<0\|t>\), max \(imag\)" $log | grep "9.847163032919....E-01" |  wc -l`
for i in $(seq 0 3)
do
   TEST+=(${CRIT_ITER_SKET[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''<0|t> auto-correlation function (key=$i)'\'', CRIT_ITER_SKET[$i].")
done
CRIT_ITER_SBRA[0]=`$GREP "AUTOCORR \(<t\|0>\*\), min \(real\)" $log | grep "\-9.9863303557592...E-01" |  wc -l`
CRIT_ITER_SBRA[1]=`$GREP "AUTOCORR \(<t\|0>\*\), max \(real\)" $log | grep "1.0000000000000000E+00" |  wc -l`
CRIT_ITER_SBRA[2]=`$GREP "AUTOCORR \(<t\|0>\*\), min \(imag\)" $log | grep "\-9.9930283966536...E-01" |  wc -l`
CRIT_ITER_SBRA[3]=`$GREP "AUTOCORR \(<t\|0>\*\), max \(imag\)" $log | grep "9.846829380783....E-01" |  wc -l`
for i in $(seq 0 3)
do
   TEST+=(${CRIT_ITER_SBRA[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''<t|0>* auto-correlation function (key=$i)'\'', CRIT_ITER_SBRA[$i].")
done


# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
