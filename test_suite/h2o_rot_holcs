#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

#######################################################################
#  INPUT
#######################################################################
cat > h2o_rot_holcs.info << %EOF%

   Molecule:         Water
   Wave Function:    VSCF
   Test Purpose:     Check the HOLC construction procedure with SimpleHO option.
                     HOLCs break the C2v symmetry of water in NC parametrization
                     but retain the Cs symmetry, check that this holds.
%EOF%

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > h2o_rot_holcs.minp << %EOF%

#0 MidasInput

#1 General
#2 IoLevel
2

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol
#3 Name
Water

#2 ModSys
#3 Sysnames
1
Water
#3 DefineSymmetry
Cs Y
#3 ModVibCoord
Rotcoord

#4 Measure
Localization 0.00001       // Standard HOLC weight
#4 Measure
Optimization 0.99999       // Standard HOLC weight
#5 CalculationType
HarmonicGroundState
#5 Name
vscf

#4 Sweeps
800
#4 ConvThr
1.0e-10
#4 GridFunction
   #5 OptParam
   GradHess
   #5 FourierPt
   2
   #5 NewtonConv
   1.0e-10

#1 Vib
#2 IoLevel
2

#2 Operator
#3 Name
h0
#3 OperFile
h2o_rot_holcs.mop
#3 KineticEnergy
Simple

#2 Basis
#3 IoLevel
6
#3 Name
basis
#3 HoBasis
20

#2 Vscf
#3 IoLevel
2
#3 Name
vscf
#3 Oper
h0
#3 Basis
basis
#3 OccGroundState
#3 ScreenZero
1.0e-21
#3 Threshold
1.0e-15
#3 MaxIter
200

#0 MidasInputEnd              // End comment
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > h2o_rot_holcs.mmol << %EOF%
#0 MidasMolecule

#1 XYZ
3 au
xyz coordinates output from Midas at the CCSD(F12*)(T*)/cc-pVQZ-F12 level of theory
 O  0.000000000000E+00  0.000000000000E+00 -7.394405002748E-01 ISO=16 SUB=1 TREAT=ACTIVE
 H -1.431340452709E+00  0.000000000000E+00  3.697202501374E-01 ISO=1  SUB=2 TREAT=ACTIVE
 H  1.431340452709E+00  0.000000000000E+00  3.697202501374E-01 ISO=1  SUB=3 TREAT=ACTIVE

#1 FREQ
3 cm-1
 1.650060212993E+03 A1 Q0
 3.836700488680E+03 A1 Q1
 3.947144092321E+03 B2 Q2

#1 VIBCOORD
au
#2 Coordinate
 -5.551997505914E-17  6.342465978685E-19 -6.801508890075E-02
  4.100073417479E-01  0.000000000000E+00  5.397244091642E-01
 -4.100073417479E-01 -6.737911016062E-18  5.397244091642E-01
#2 Coordinate
 -3.851698269728E-16  9.470211362709E-18 -4.869138660152E-02
 -5.727231805321E-01  0.000000000000E+00  3.863838199673E-01
  5.727231805321E-01 -1.006066751949E-16  3.863838199673E-01
#2 Coordinate
 -6.755255637324E-02  9.632916403616E-18  4.129298145023E-16
  5.360540457321E-01 -0.000000000000E+00 -4.153939103030E-01
  5.360540457321E-01 -1.023351702175E-16  4.153939103030E-01

#0 MidasMoleculeEnd
%EOF%

#######################################################################
#  OPERATOR FILE
#######################################################################
cat > h2o_rot_holcs.mop << %EOF%
#0MIDASOPERATOR
#1OPERATORTERMS
 2.826187250349e-05 Q^2(Q0)
 5.463362509790e-20 Q^1(Q0) Q^1(Q1)
-5.095432573327e-20 Q^1(Q0) Q^1(Q2)
 1.527978145530e-04 Q^2(Q1)
 6.776263578034e-20 Q^1(Q1) Q^1(Q2)
 1.617213319028e-04 Q^2(Q2)
#0MIDASOPERATOREND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did Midas end?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS DID NOT END"
echo $CRIT0

# Did we use the correct number of iterations?
CRIT0=`$GREP "Number of iterations done:           3" $log | wc -l`
TEST[1]=`expr $CRIT0`
CTRL[1]=1
ERROR[1]="NUMBER OF ITERATIONS IS INCORRECT"
echo $CRIT0

# Did we obtain the correct value of the hybrid optimization criterion?
CRIT0=`$GREP "Final value:                         2\.1471381087............e\-02" $log | wc -l`
TEST[2]=`expr $CRIT0`
CTRL[2]=1
ERROR[2]="FINAL OPTIMIZATION MEASURE VALUE WRONG"
echo $CRIT0

# Did we obtain the correct symmetry in rotated vibrational coordinates?
CRIT0=`$GREP "The mode Q[0-2] is now of symmetry:      AP" $log | wc -l`
TEST[3]=`expr $CRIT0`
CTRL[3]=3
ERROR[3]="DID NOT OBTAIN CORRECT SYMMETRY OF ROTATED VIBRATIONAL COORDINATES"
echo $CRIT0

PASSED=1
for i in 0 1 2 3
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}.
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

' > h2o_rot_holcs.check
#######################################################################
