#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         water  
   Wave Function:    Dalton HF WF 
   Test Purpose:     Check setup in NumDer
                     normal coordinates
                     VibPol
                     Harmonic freqs.
                     specific derivatives
                     no ES symmetry
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_NumDer_1
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_NumDer_1.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
15
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
dalton_singlepoint
#2 Type
SP_DALTON
#2 DaltonBasis
1  cc-pVDZ
6  cc-pVDZ
8  cc-pVDZ
#2 EsSymmetryOff
#2 Dalton Input
**DALTON INPUT
.RUN WAVEFUNCTIONS
.RUN PROPERTIES
.RUN RESPONSE
**WAVE FUNCTIONS
.HF
*SCF INP
.THRESH
 1.0D-12
**RESPONSE
*LINEAR
.DIPLEN
.FREQUE
 2
 0.000 0.040
.THCLR
1.0D-9
*END OF INPU

#2 End of Dalton Input

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 IoLevel
    5 
#2 NThreads
4
#2 VibPol
#2 Displacement
    Freqdisplacement
    0.010
#2 multilevelpes
   dalton_singlepoint 2 // 2 is not used
#2 NoScalCoordInFit
#2 TaylorInfo
    0 3 4

#0 Midas Input End
%EOF%
#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_NumDer_1.mmol <<%EOF%
#0 Midas Molecule
#1 XYZ
3 au
This is a comment
O    0.0000000000  0.0000000000  0.0093387698  ISO=16
H#1 -1.4150074244  0.0000000000  1.1027233577  ISO=1
H#2  1.4150074244  0.0000000000  1.1027233577  ISO=1

#1 FREQ
3 cm-1
 0.4212102203069695E+04
 0.4113771936080185E+04
 0.1775814493375378E+04

#1 VIBCOORD
au
#2 COORDINATE
-6.7619400810377511E-02 0.0000000000000000E+00 9.6240164893656664E-08
 5.3658571933560051E-01 0.0000000000000000E+00-4.1462278165119559E-01
 5.3658330139016164E-01 0.0000000000000000E+00 4.1462125424985669E-01
#2 COORDINATE
-1.3960812605287579E-07 0.0000000000000000E+00-4.9391653649491343E-02
-5.6845447849070363E-01 0.0000000000000000E+00 3.9193985878760185E-01
 5.6845669417309530E-01 0.0000000000000000E+00 3.9194157085846315E-01
#2 COORDINATE
 5.7440940107026776E-09 0.0000000000000000E+00-6.7508277174684522E-02
 4.1590393849936247E-01 0.0000000000000000E+00 5.3570273916449862E-01
-4.1590402966231776E-01 0.0000000000000000E+00 5.3570266872234651E-01
#0 Midas Molecule End
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2)
tens_order=(2),descriptor=(XX_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(0,0)
tens_order=(2),descriptor=(XY_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(0,1)
tens_order=(2),descriptor=(XZ_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(0,2)
tens_order=(2),descriptor=(YX_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(1,0)
tens_order=(2),descriptor=(YY_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(1,1)
tens_order=(2),descriptor=(YZ_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(1,2)
tens_order=(2),descriptor=(ZX_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(2,0)
tens_order=(2),descriptor=(ZY_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(2,1)
tens_order=(2),descriptor=(ZZ_POL_0.0000E+00),frq=(0),Rot_group=(1),element=(2,2)
tens_order=(2),descriptor=(XX_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(0,0)
tens_order=(2),descriptor=(XY_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(0,1)
tens_order=(2),descriptor=(XZ_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(0,2)
tens_order=(2),descriptor=(YX_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(1,0)
tens_order=(2),descriptor=(YY_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(1,1)
tens_order=(2),descriptor=(YZ_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(1,2)
tens_order=(2),descriptor=(ZX_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(2,0)
tens_order=(2),descriptor=(ZY_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(2,1)
tens_order=(2),descriptor=(ZZ_POL_4.0000E-02),frq=(0.04),Rot_group=(2),element=(2,2)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

# Basic logic: basic setup in numder.
CRIT1=`$GREP "Order of analytical derivative:    0" $log | wc -l`
CRIT2=`$GREP "Maximum order of mode coupling:    3" $log | wc -l`
CRIT3=`$GREP "Maximum order in Taylor expansion: 4" $log | wc -l`
CRIT5=`$GREP -A3 "Evaluating list of singlepoints" $log | $GREP "New : 57[[:space:]]" | wc -l`
CRIT6=`$GREP "Electronic state No\. 0" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT5 \+ $CRIT6`
CTRL[1]=5
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
echo $CRIT6
ERROR[1]="BASIC SETUP IN NUMDER NOT CORRECT"

# Order of the analytical derivative is 0
# Maximum number of mode couplings is 3
# Maximum order in Taylor expansion of potential (property) surface is 4
# Normal Coordinates used in NumDer section
# Type of displacement is FREQDISPLACEMENT
# Total number of calculations is 57

# Electronic struture calculations
#CRIT3=`$GREP "I found 22 properties in file" $log | wc -l`
#TEST[2]=`expr $CRIT3`
#CTRL[2]=1
#ERROR[2]="NUMBER OF PROPERTIES WRONG"

# Harmonic freqs.
CRIT1=`$GREP "4\.21215[0-9]" $log | wc -l`
CRIT2=`$GREP "4\.11381[0-9]" $log | wc -l`
CRIT3=`$GREP "1\.77580[0-9]" $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=3
ERROR[3]="CALCULATED HARMONIC FREQS WRONG"

#1         4.2121524502049405782600E+03
#2         4.1138190490375782246701E+03
#3         1.7758089922401572948729E+03

#Vibrational polarizability.
CRIT1=`$GREP "Isotropic vibrational polarizability is: 2.733285[0-9]" $log | wc -l`
TEST[4]=`expr $CRIT1`
CTRL[4]=1
ERROR[4]="ISOTROPIC VIBRATIONAL POLARIZABILITY WRONG"

#Isotropic vibrational polarizability is: 2.7332852221161602E-01

# Specific derivatives
CRIT1=`$GREP "For property GROUND_STATE_ENERGY, obtained derivative: 5\.45[56][[:digit:]]*[eE]-08" $log | wc -l`
CRIT2=`$GREP "For property XX_POL_0\.0000E\+00, obtained derivative: 2\.55[[:digit:]]*[eE]-03" $log | wc -l`
CRIT3=`$GREP "For property XZ_POL_0\.0000E\+00, obtained derivative: 2\.14776[[:digit:]]*[eE]-05" $log | wc -l`
CRIT4=`$GREP "For property GROUND_STATE_ENERGY, obtained derivative: \-1\.478[[:digit:]]*[eE]-07" $log | wc -l`
CRIT5=`$GREP "For property GROUND_STATE_ENERGY, obtained derivative: \-3\.60[[:digit:]]*[eE]-06" $log | wc -l`
TEST[5]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[5]=5
ERROR[5]="SPECIFIC DERIVATIVES WRONG"

#V_1_1_2_3
# Property number 1 derivative 5.4564452511840499937534E-08  <-- energy
#V_2_2
# Property number 5 derivative 2.5511429312885525177990E-03  <-- XX-POL
#V_1_2_2
# Property number 7 derivative 2.1477618235010709213384E-05  <-- XZ-POL
#V_2_2_3
# Property number 1 derivative -1.4788952285016421228647E-07
#V_2_2_2
# Property number 1 derivative -3.6018230957779451273382E-06


PASSED=1
for i in 0 1 3 4 5 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi


' > pes_NumDer_1.check
#######################################################################
##!$CHECK_SHELL
