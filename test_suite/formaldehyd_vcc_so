#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Formaldehyde test 
   Wave Function:    VSCF/large basis, all sum_n <= 2 state, adapted HO basis 
   Test Purpose:     Check VSCF energy of lowest states 
   Reference:        Romanowski, Bowman & Harding, JCP 82, 4155 (1985)
                     Reproduces all energies (perhaps 2 places with an acceptable 
                     deviation of 1 on last digit) of paper.
                     (Despite unknown if pes includes coriolis or not).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > formaldehyd_vcc_so.minp <<%EOF%

#0 MIDAS Input

#1 general
#2 IoLevel
    2 
#2 Time

#1 Vib
#2 IoLevel
 5

#2 Operator
#3 IoLevel
14
#3 Name
 testoper
#3 OperFile
formaldehyd_vcc_so.op
#3operinput
#4operatorterms
-0.5 DDQ^2(1)
-0.5 DDQ^2(2)
-0.5 DDQ^2(3)
-0.5 DDQ^2(4)
-0.5 DDQ^2(5)
-0.5 DDQ^2(6)
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 basis
#3 Define
1 HO n_high=20,omeg=2937.4 cm-1
2 HO n_high=20,omeg=1777.8 cm-1
3 HO n_high=20,omeg=1539.7 cm-1
4 HO n_high=20,omeg=1188.3 cm-1
5 HO n_high=20,omeg=3012.0 cm-1 
6 HO n_high=20,omeg=1269.4 cm-1
#3 IoLevel
   9

#2 Vscf
#3 IoLevel
11
#3 Name 
scf00
#3 Oper
testoper
#3 Basis
basis
#3 Occup
 0 0 0 0 0 0 
#3 OccuMax
 2 2 2 2 2 2 
#3 OccMaxSumN 
 1
#3 OccMaxExci
 2
#3 Threshold
 1.0e-12
#3 MaxIter
 20

#2 Vcc       
#3 Vcc
#3 Transformer
   trf=orig outofspace=true t1transh=false
#3 IoLevel
 3
#3 Occup
 0 0 0 0 0 0
#3 OccuMax
 0 0 0 1 0 0 
#3 OccMaxSumN 
 1
#3 OccMaxExci
 2
#3 LimitModalBasis
 2 2 2 2 2 2 
#3 MaxExci
 2
#3 VecStorage
 1 
#3 ITEQRESIDTHR
 1.0e-12
#3 ITEQENERTHR
 1.0e-12
#3 ITEQMAXIT
 15
#3 ITEQMAXDIM
 250
#3 ITEQBREAKDIM
 200

#0 Midas Input End
%EOF%

cat > formaldehyd_vcc_so.op <<%EOF%
GEN_VSCF_95 N_PROD = 6
  1 0 0 0 0 0 2 0 0 0 0 0  8.95641e-05
  2 0 0 0 0 0 2 0 0 0 0 0  3.28072e-05
  3 0 0 0 0 0 2 0 0 0 0 0  2.47445e-05
  4 0 0 0 0 0 2 0 0 0 0 0  1.46576e-05
  5 0 0 0 0 0 2 0 0 0 0 0  9.41724e-05
  6 0 0 0 0 0 2 0 0 0 0 0  1.67271e-05
  1 0 0 0 0 0 3 0 0 0 0 0  1.58395e-06
  2 0 0 0 0 0 3 0 0 0 0 0 -3.09488e-07
  3 0 0 0 0 0 3 0 0 0 0 0 -7.42473e-09
  1 0 0 0 0 0 4 0 0 0 0 0  1.64404e-08
  2 0 0 0 0 0 4 0 0 0 0 0  2.09690e-09
  3 0 0 0 0 0 4 0 0 0 0 0  3.01839e-11
  4 0 0 0 0 0 4 0 0 0 0 0  7.72218e-10
  5 0 0 0 0 0 4 0 0 0 0 0  2.03858e-08
  6 0 0 0 0 0 4 0 0 0 0 0  6.16225e-10
  1 2 0 0 0 0 2 1 0 0 0 0  6.26340e-08
  1 3 0 0 0 0 2 1 0 0 0 0  1.64127e-08
  1 2 0 0 0 0 1 2 0 0 0 0 -1.55531e-07
  1 2 3 0 0 0 1 1 1 0 0 0 -2.18685e-07
  1 3 0 0 0 0 1 2 0 0 0 0 -1.40708e-07
  1 4 0 0 0 0 1 2 0 0 0 0 -4.76302e-07
  1 5 0 0 0 0 1 2 0 0 0 0  5.14105e-06
  1 5 6 0 0 0 1 1 1 0 0 0  2.37125e-08
  1 6 0 0 0 0 1 2 0 0 0 0 -3.61882e-07
  2 3 0 0 0 0 2 1 0 0 0 0  2.13373e-07
  2 3 0 0 0 0 1 2 0 0 0 0 -1.82567e-07
  2 4 0 0 0 0 1 2 0 0 0 0 -6.24048e-08
  2 5 0 0 0 0 1 2 0 0 0 0  3.54497e-07
  2 5 6 0 0 0 1 1 1 0 0 0 -5.32615e-07
  2 6 0 0 0 0 1 2 0 0 0 0 -2.62944e-08
  3 4 0 0 0 0 1 2 0 0 0 0 -6.14774e-08
  3 5 0 0 0 0 1 2 0 0 0 0  3.27825e-07
  3 5 6 0 0 0 1 1 1 0 0 0 -6.15477e-07
  3 6 0 0 0 0 1 2 0 0 0 0  1.24274e-07
  1 2 0 0 0 0 3 1 0 0 0 0  1.75202e-09
  1 3 0 0 0 0 3 1 0 0 0 0  4.35259e-10
  1 2 0 0 0 0 2 2 0 0 0 0 -5.11727e-09
  1 2 3 0 0 0 2 1 1 0 0 0 -1.84464e-08
  1 3 0 0 0 0 2 2 0 0 0 0 -1.54362e-08
  1 4 0 0 0 0 2 2 0 0 0 0 -2.35793e-08
  1 5 0 0 0 0 2 2 0 0 0 0  1.13248e-07
  1 5 6 0 0 0 2 1 1 0 0 0  9.64337e-10
  1 6 0 0 0 0 2 2 0 0 0 0 -1.84492e-08
  1 2 0 0 0 0 1 3 0 0 0 0  6.06809e-10
  1 2 3 0 0 0 1 2 1 0 0 0 -3.49433e-09
  1 2 3 0 0 0 1 1 2 0 0 0 -3.41881e-09
  1 2 4 0 0 0 1 1 2 0 0 0 -3.99302e-09
  1 2 5 0 0 0 1 1 2 0 0 0  1.23419e-08
  1 2 5 6 0 0 1 1 1 1 0 0 -4.33096e-08
  1 2 6 0 0 0 1 1 2 0 0 0 -1.98986e-09
  1 3 0 0 0 0 1 3 0 0 0 0 -8.19983e-10
  1 3 4 0 0 0 1 1 2 0 0 0 -3.47055e-09
  1 3 5 0 0 0 1 1 2 0 0 0  1.24585e-08
  1 3 5 6 0 0 1 1 1 1 0 0 -7.06454e-08
  1 3 6 0 0 0 1 1 2 0 0 0  2.71959e-09
  2 3 0 0 0 0 3 1 0 0 0 0 -2.29151e-09
  2 3 0 0 0 0 2 2 0 0 0 0  1.57200e-09
  2 4 0 0 0 0 2 2 0 0 0 0 -5.30463e-10
  2 5 0 0 0 0 2 2 0 0 0 0 -8.45341e-09
  2 5 6 0 0 0 2 1 1 0 0 0 -1.88770e-09
  2 6 0 0 0 0 2 2 0 0 0 0  1.00897e-10
  2 3 0 0 0 0 1 3 0 0 0 0  5.79834e-11
  2 3 4 0 0 0 1 1 2 0 0 0  8.16788e-10
  2 3 5 0 0 0 1 1 2 0 0 0 -2.49993e-08
  2 3 5 6 0 0 1 1 1 1 0 0 -8.88722e-09
  2 3 6 0 0 0 1 1 2 0 0 0  1.92595e-09
  3 4 0 0 0 0 2 2 0 0 0 0 -2.82284e-10
  3 5 0 0 0 0 2 2 0 0 0 0 -2.08768e-08
  3 5 6 0 0 0 2 1 1 0 0 0 -2.63248e-09
  3 6 0 0 0 0 2 2 0 0 0 0  1.54438e-09
  4 5 0 0 0 0 2 2 0 0 0 0 -2.78328e-08
  4 5 6 0 0 0 2 1 1 0 0 0 -3.41463e-10
  4 6 0 0 0 0 2 2 0 0 0 0  1.43632e-09
  5 6 0 0 0 0 3 1 0 0 0 0  4.41000e-09
  5 6 0 0 0 0 2 2 0 0 0 0 -2.01654e-08
  5 6 0 0 0 0 1 3 0 0 0 0  7.36786e-10
%EOF%
cat > formaldehyd_vcc_so.corop <<%EOF%
1 4 5  8.52097e-1
1 4 6 -5.23385e-1
2 1 4 -5.06904e-1
2 2 4 -6.10299e-1
2 3 4 -6.08756e-1
3 1 5  3.33491e-2
3 1 6  9.14217e-1
3 2 5  5.12812e-1
3 2 6  3.31178e-1
3 3 5  8.57843e-1
3 3 6 -2.33514e-1
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=20,omeg=" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=15
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT11=`$GREP "E =  5.7962857892" $log | wc -l`
CRIT12=`$GREP "E =  8.6105003446" $log | wc -l`
CRIT13=`$GREP "E =  7.5467853974" $log | wc -l`
CRIT14=`$GREP "E =  7.3029568299" $log | wc -l`
CRIT15=`$GREP "E =  6.9477703152" $log | wc -l`
CRIT16=`$GREP "E =  8.6400617287" $log | wc -l`
CRIT17=`$GREP "E =  7.0447458734" $log | wc -l`
TEST[2]=`expr \
$CRIT11 \
\+ $CRIT12 \
\+ $CRIT13 \
\+ $CRIT14 \
\+ $CRIT15 \
\+ $CRIT16 \
\+ $CRIT17 \
`
echo $CRIT11 \
$CRIT12 \
$CRIT13 \
$CRIT14 \
$CRIT15 \
$CRIT16 \
$CRIT17 

CTRL[2]=7
ERROR[2]="VSCF ENERGY NOT CORRECT"

# VCC   testoper_basis_0.0       E =  0.0000000000000000E+00 Occ: [0,0,0,0,0,0]
# VCC   testoper_basis_4.0       E =  5.2462896790697808E-03 Occ: [0,0,0,1,0,0]

CRIT21=`$GREP "VCC" $log | grep "5.2462896790" |  wc -l`
CRIT22=`$GREP "VCC" $log | grep "2.4858141295" |  wc -l`
CRIT23=`$GREP "VCC" $log | grep "2.4858191873" |  wc -l`
CRIT24=`$GREP "VCC" $log | grep "2.4858193811" |  wc -l`

TEST[3]=`expr \
$CRIT21 \
\+ $CRIT22 \
\+ $CRIT23 \
\+ $CRIT24 
`
echo $CRIT21 \
$CRIT22 \
$CRIT23 \
$CRIT24 
                                                                                      
CTRL[3]=1
ERROR[3]="VCC ENERGY NOT CORRECT"

PASSED=1
for i in 1 2 3
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > formaldehyd_vcc_so.check
#######################################################################
