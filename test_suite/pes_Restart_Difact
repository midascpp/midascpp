#!/bin/sh
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > pes_Restart_Difact.info <<%EOF%
   -------------
   Molecule:         Methylfurfural
   Wave Function:    pre-calculated mop files (ORCA/HF-3c) 
   Test Purpose:     Check implementation for double incremental scheme
                     in Falcon coordinates with auxiliary coordinate transformation 
                     (DIFACT-2F2M) using pre-calculated .mop files for fragment combinations
   -------------
%EOF%

#######################################################################
#  Setup directories
#######################################################################

MAINDIR=$PWD/Dir_pes_Restart_Difact
export MAINDIR
rm -rf $MAINDIR

FRAGMENTDIR=$MAINDIR/FC_savedir
export FRAGMENTDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

MOLFILESDIR=$PWD/generic/generic_pes_Restart_Difact
export MOLFILESDIR

mkdir $MAINDIR
mkdir $FRAGMENTDIR
mkdir $INTERFACEDIR

#######################################################################
#  Define interface file names
#######################################################################

PROPINFO=midasifc.propinfo

#######################################################################
#  MidasCpp input
#######################################################################
cat > pes_Restart_Difact.minp <<%EOF%
#0 MidasInput

#1 General
#2 IoLevel
5
#2 MainDir
$MAINDIR

#1 Pes
#2 DoubleIncremental
DIFACT
FcInfo
Given

#0 MidasInputEnd
%EOF%

#######################################################################
#  Fragment combination info                                          #
#######################################################################
cat > $FRAGMENTDIR/FcInfo << %EOF%
# FC Label  FC Subsystems  Interconnecting Modes
FC_0 16      6    3
FC_1 17      12    3
FC_2 18      12    3
FC_4 16,18      12    9
FC_5 17,18      6    3
%EOF%

#######################################################################
#  Molecule input files                                               #
#######################################################################
cp $MOLFILESDIR/* $FRAGMENTDIR/

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/$PROPINFO << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

#######################################################################
#  Check script
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did midas end correctly?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED CORRECTLY"

# Did we obtain the correct scaling factors, (#1 SCALEFACTORS)?
CRIT0=`$GREP "2\.32749801..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP "3\.22547467..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP "3\.53237658..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP "6\.96866556..............E\-02" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT4=`$GREP "1\.22425135..............E\-01" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[1]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=5
ERROR[1]="DID NOT OBTAIN THE CORRECT SCALING FACTORS"

# Did we obtain the correct linear operator coefficients, (#1 OPERATORTERMS)?
# First check local modes, which were not transformed
CRIT0=`$GREP   "4\.38513716..............E\-03 Q\^2\(Q30\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP   "\-1.91198376..............E\-03 Q\^3\(Q39\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP   "4\.67521281..............E\-03 Q\^2\(Q35\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP   "2\.94258775..............E\-03 Q\^2\(Q15\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[2]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[2]=4
ERROR[2]="DID NOT OBTAIN THE CORRECT OPERATOR TERMS FOR LOCAL MODES"

# Now check transformed auxiliary modes
CRIT0=`$GREP   "1\.20135900..............E\-02 Q\^2\(Q6\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP   "7\.53180119..............E\-04 Q\^2\(Q11\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP   "2\.49277102..............E\-03 Q\^2\(Q16\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP   "2\.07137543..............E\-03 Q\^2\(Q14\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[3]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=4
ERROR[3]="DID NOT OBTAIN THE CORRECT OPERATOR TERMS FOR TRANSFORMED AUX MODES"

# Check couplings between non-transformed and transformed modes
CRIT0=`$GREP   "3\.03590023..............E\-04 Q\^2\(Q6\) Q\^1\(Q30\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP   "\-2\.81254535..............E\-04 Q\^1\(Q11\) Q\^1\(Q30\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP   "8\.08962295..............E\-04 Q\^2\(Q11\) Q\^1\(Q39\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP   "2\.69034451..............E\-04 Q\^1\(Q7\) Q\^1\(Q15\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[4]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[4]=4
ERROR[4]="DID NOT OBTAIN THE CORRECT OPERATOR TERMS FOR COUPLINGS of NON-AUX/AUX MODES"

# Check couplings for transformed modes
CRIT0=`$GREP   "\-8\.74105880..............E\-04 Q\^1\(Q6\) Q\^1\(Q10\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT1=`$GREP   "2\.86851913..............E\-05 Q\^2\(Q6\) Q\^2\(Q11\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT2=`$GREP   "\-1\.24764339..............E\-03 Q\^1\(Q6\) Q\^1\(Q8\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
CRIT3=`$GREP   "\-4\.57340752..............E\-03 Q\^1\(Q6\) Q\^1\(Q7\)" '"$MAINDIR/FinalSurfaces/savedir/prop_no_1.mop"' | wc -l` # 1
TEST[5]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[5]=4
ERROR[5]="DID NOT OBTAIN THE CORRECT OPERATOR TERMS FOR COUPLINGS OF TRANSFORMED AUX MODES"


PASSED=1
for i in 0 1 2 3 4 5 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Restart_Difact.check
#######################################################################
