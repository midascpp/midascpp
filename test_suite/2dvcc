#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy  in various 
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980), for operator
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals

                     Test covers both the 
                     FtesttranExpH (primitive full basis explicit H CI)
                     ExpHtesttran option.
                     In the later case both the full testtran which gives results
                     equivalent to the first, and in 
                     addition the single excited case, which in the modal
                     basis, gives the HF result for the reference state, 
                     which I thing is a nice check.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dvcc.minp <<%EOF%

#0 MIDAS Input

#1 general
#2 IoLevel
  1
#2 BufSIZE
 4
#2 FileSize
 30
#2 Time

#1 Vib

#2 IoLevel
  11

#2 Operator
#3 IoLevel
14
#3 Name
 tst
#3operinput
#4operatorterms
-0.5 DDQ^2(a0)
-0.5 DDQ^2(b0)
0.98 Q^2(a0)
0.245 Q^2(b0)
0.0064 Q^3(a0)
-0.08 Q^1(a0) Q^2(b0)
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 bas
#3 Define
 a0 HO n_high=8,omeg=1.4
 b0 HO n_high=8,omeg=0.7
#3 IoLevel
   1

#2 Vscf
#3 IoLevel
11
#3 Occup
 0 0 

#2 Vscf
#3 IoLevel
11
#3 Occup
 1 3 
#3 Name
 tst__abe 

#2 Vcc
#3 Name
   vmp8
#3 Vmp
 8
#3 VmpStart
#3 LimitModalBasis
 3 3 
#3 IoLevel
 5
#3 Vcc 
#3 UseOldVccSolver
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 TimeIt
#3 SizeIt
#3 Iso
 1
#3 ITEQRESIDTHR
 1.0e-12

#2 Vcc
#3 Name
   vcc2_h2start
#3 LimitModalBasis
 3 3 
#3 H2Start
#3 UseOldVccSolver
#3 IoLevel
 5
#3 MaxExci
 2
#3 Vcc 
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 VecStorage
 1 
#3 ITEQRESIDTHR
 1.0e-12

#2 Vcc
#3 Name
   vcc2
#3 LimitModalBasis
 3 3 
#3 IoLevel
 5
#3 MaxExci
 2
#3 Vcc 
#3 UseOldVccSolver
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 VecStorage
 2 
#3 ITEQRESIDTHR
 1.0e-12

#2 Vcc
#3 Name
   vcc2_other
#3 LimitModalBasis
 3 3 
#3 IoLevel
 5
#3 MaxExci
 2
#3 Vcc 
#3 UseOldVccSolver
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 VecStorage
 3 
#3 ITEQRESIDTHR
 1.0e-12

#2 Vcc
#3 Name
   vcc1
#3 LimitModalBasis
 2 4 
#3 Occup
 1 3 
#3 IoLevel
 5
#3 Vcc 
#3 UseOldVccSolver
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 MaxExci
 1
#3 ITEQRESIDTHR
 1.0e-12


#2 Vcc
#3 Name
   vcc_iso1
#3 LimitModalBasis
 2 4 
#3 Occup
 1 3 
#3 IoLevel
 5
#3 Vcc 
#3 UseOldVccSolver
#3 Transformer
    trf=orig outofspace=true t1transh=false
#3 Iso
 1
#3 ITEQRESIDTHR
 1.0e-12

#2 Vcc
#3 Name
   vcc2_exci
#3 LimitModalBasis
 2 4
#3 Occup
 1 3
#3 IoLevel
 5
#3 Vci
 8 
#3 MaxExci
 2 
#3 ITEQRESIDTHR
 1.0e-12

#0 Midas Input End

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=13
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# Vscf energy:
CRIT01=`$GREP "1.0493492614924" $log | grep "VSCF" | wc -l`
CRIT02=`$GREP "4.5120240417778" $log | grep "VSCF" | wc -l`

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
`
CTRL[2]=2 
ERROR[2]="VSCF ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02


CRIT03=`$GREP "Vcc" $log | grep "1.048500922293[3,4,5]" | grep energy | wc -l`
TEST[3]=`expr \
$CRIT03 \
`
CTRL[3]=4
ERROR[3]="VCC GROUND STATE ENERGIES NOT CORRECT"
echo $CRIT03



# check as Vcc and as Vci
CRIT04=`$GREP "Vcc" $log | grep "4.514748290161[5,6]" | grep energy | wc -l`
TEST[4]=`expr \
$CRIT04 \
`
CTRL[4]=1
ERROR[4]="VCC EXCITED STATE ENERGIES NOT CORRECT"
echo $CRIT04

CRIT05=`$GREP "Vci_Root" $log | grep "4.514748290161[5,6]" | wc -l`
TEST[5]=`expr \
$CRIT05 \
`
CTRL[5]=1
ERROR[5]="VCI EXCITED STATE ENERGIES NOT CORRECT"
echo $CRIT05


PASSED=1
for i in 1 2 3 4 5
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dvcc.check
#######################################################################
