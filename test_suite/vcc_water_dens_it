#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > vcc_water_dens_it.info <<%EOF%
   IR intensities
   Raman intensities
   -----------------
   Molecule:      water
   Wave Function: VCC[3]
   Test Purpose:  Test calculation of density matrix 
                  
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc_water_dens_it.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
    1 
!#2 Debug 
#2 Time

#1 Vib

// Hamiltonian.
#2 Operator
   #3 Name
      h0
   #3 OperFile
      wh.mop 
   #3 SetInfo
      type=energy

// Dipole operators.
#2 Operator
   #3 Name
      x
   #3 OperFile
      wx.mop
   #3 SetInfo
      type=x_dipole
#2 Operator
   #3 Name
      y
   #3 OperFile
      wy.mop
   #3 SetInfo
      type=y_dipole
#2 Operator
   #3 Name
      z
   #3 OperFile
      wz.mop
   #3 SetInfo
      type=z_dipole
#2 Operator
   #3 Name
      u1
   #3 OperFile
      u1.mop
#2 Operator
   #3 Name
      u2
   #3 OperFile
      u2.mop
#2 Operator
   #3 Name
      u3
   #3 OperFile
      u3.mop
#2 Operator
   #3 Name
      u14
   #3 OperFile
      u14.mop
#2 Operator
   #3 Name
      u24
   #3 OperFile
      u24.mop
#2 Operator
   #3 Name
      u34
   #3 OperFile
      u34.mop

#2 Basis
   #3 Name
      basis
   #3 HoBasis 
      5 
   #3 IoLevel
      1

#2 Vscf
   #3 IoLevel
      11 
   #3 Name 
      scf0
   #3 Oper
      h0 
   #3 Basis
      basis
   #3 Occup
      [3*0] 
   #3 Threshold
      1.0e-15
   #3 MaxIter
      20

#2 Vcc
   #3 Vcc
   #3 Transformer
      trf=v3 type=gen fileprefix=tst_
   #3 VscfThreshold
      1.0e-15
   #3 UseAllVscfModals
   #3 MaxExci
      3
   #3 ItEqResidThr
      1.0e-14
   #3 Oper
      h0
   #3 Basis 
      basis 
   //#3 VecStorage 
      //0
   #3 ItEqMaxDim
      150
   #3 ItEqMaxIt
      60
   #3 ItEqBreakDim
      100
   #3 DISABLECHECKMETHOD 
   #3 Rsp
      //#4 Eigen val
         //1
      #4 IoLevel
         3  
      #4 ItEqResidThr
         1.0e-14
      #4 ItEqMaxIt
         100
      #4 ItEqMaxDim
         200
      #4 ItEqBreakDim
         150
      #4 RspFunc
         1 h0
      #4 DensityAnalysis 
      #4 ItNaMo 
            3 
            1.0e-20  // INSANE tight. Just for making it take the 3 iterations in all cases. 

#0 Midas Input End
#0 Midas Input End
%EOF%

#######################################################################
#  OPERATOR INPUTS
#######################################################################
cat > wh.mop << %EOF%
DALTON_FOR_MIDAS
 2.7010827352569322101772E-08    1     
 2.8424599776144532370381E-05    1     1     
 1.3386473085574834840372E-07    1     1     1     
-7.0940586738288402557373E-11    1     1     1     1     
 2.6339947112319350708276E-07    2     
 1.5633553209681849693879E-04    2     2     
 3.2519419619347900152206E-06    2     2     2     
 4.4874468585476279258728E-08    2     2     2     2     
-2.1032064978498965501785E-12    3     
 1.6460214658309268997982E-04    3     3     
-4.0927261579781770706177E-11    3     3     3     
 4.7789967538847122341394E-08    3     3     3     3     
 2.0551169654936529695988E-09    1     2     
-2.5825875127338804304600E-07    1     2     2     
-9.0590219770092517137527E-09    1     2     2     2     
-7.5936259236186742782593E-07    1     1     2     
-4.7161620386759750545025E-08    1     1     2     2     
 1.0610847311909310519695E-08    1     1     1     2     
-5.3134385780140291899443E-10    1     3     
-9.5156394763762364163995E-07    1     3     3     
 2.7625901566352695226669E-11    1     3     3     3     
 3.2969182939268648624420E-12    1     1     3     
-5.8064415497938171029091E-08    1     1     3     3     
 8.1001871876651421189308E-13    1     1     1     3     
 7.1281647251453250646591E-11    2     3     
 1.0034688898485910613090E-05    2     3     3     
-4.8942183639155700802803E-11    2     3     3     3     
 6.0367710830178111791611E-11    2     2     3     
 2.7796704671345651149750E-07    2     2     3     3     
-4.6895820560166612267494E-11    2     2     2     3     
-6.0538241086760535836220E-11    1     2     3     
-5.5532041187689173966646E-08    1     2     3     3     
-9.0949470177292823791504E-12    1     2     2     3     
-8.2650331023614853620529E-11    1     1     2     3     
%EOF%

cat > u1.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  1     
%EOF%
cat > u2.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  2     
%EOF%
cat > u3.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  3     
%EOF%
cat > u14.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    1     1     1     1
%EOF%
cat > u24.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    2     2     2     2
%EOF%
cat > u34.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    3     3     3     3
%EOF%
#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Are the responses correct?
#Perform density matrix analysis
CRIT1=`$GREP "Density matrix " $log | $GREP "analysis" | wc -l`
CRIT2=`$GREP "Trace of mode " $log | $GREP "density " |  $GREP " 1.00000000......" | wc -l`
CRIT3=`$GREP "Trace of mode " $log | $GREP "density " |  $GREP " 9.99999999......" | wc -l`
CRIT4=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 9.98701858173...." | wc -l`
CRIT5=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 0.998701858173..." | wc -l`
CRIT6=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 1.29434305836...." | wc -l`
CRIT7=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 0.129434305836..." | wc -l`
CRIT8=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 9.98701858137....." | wc -l`
CRIT9=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 1.29434309665[7,8]..." | wc -l`
##CRIT1=`$GREP "Perform ,y,z>>\(" $log | $GREP "= -3.6074840677[78].....E-13" | wc -l`
TEST[1]=`expr $CRIT1`
CTRL[1]=12
ERROR[1]="IO not correct"
TEST[2]=`expr $CRIT2 \+ $CRIT3`
CTRL[2]=9 
ERROR[2]="Density matrices not correct"
echo $CRIT4 
echo $CRIT5 
echo $CRIT6 
echo $CRIT7 
echo $CRIT8 
echo $CRIT9 
TEST[3]=`expr $CRIT4 \+ $CRIT5 \+  $CRIT6 \+  $CRIT7 \+  $CRIT8 \+  $CRIT9`
CTRL[3]=6 
ERROR[3]="occupation numbers not correct"

# Function for getting out the "Norm of xi vector" for a m, p, q combination.
# Args:
# $1 mode
# $2 p
# $3 q
# $4 expected value
grep_xi_norm() {
   $GREP -A14 "Calculate expectation value for elementary shift operator  mode: $1 p: $2 q: $3" $log | $GREP -c "Norm of xi vector: * $4";
}
TEST[4]=$(grep_xi_norm 0 1 2 1.1460069881232...E-02)
TEST[5]=$(grep_xi_norm 0 2 1 7.2609508006515...E-03)
CTRL[4]=1
CTRL[5]=1
ERROR[4]="Wrong \"Norm of xi vector\" for mode = 0, p = 1, q = 2."
ERROR[5]="Wrong \"Norm of xi vector\" for mode = 0, p = 2, q = 1."

PASSED=1
for i in 1 2 3 4 5
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}"! "${TEST[i]}" should be: "${CTRL[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vcc_water_dens_it.check
#######################################################################

