#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         ethene, (ethylene)
   Wave Function:    CCSD, HF,  camB3LYP 
   Test Purpose:     Check Multilevel procedure with symmetry
                     Shepard extrapolation mono -> bi
                     Intrinsic Mode combination
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_pes_MAdga_Extr1_2
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  Define interface file names
#######################################################################

PROPINFO1=dalton_1.propinfo
PROPINFO2=dalton_2.propinfo
PROPINFO3=dalton_3.propinfo

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_MAdga_Extr1_2.minp <<%EOF%
#0 MIDAS Input
#1 general
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
dalton_1
#2 PropertyInfo
$PROPINFO1
#2 Type
SP_DALTON
#2 DaltonBasis
1  6-31G
6  6-31G
#2 Dalton Input
**DALTON INPUT
.DIRECT
.RUN PROPERTIES
**WAVE FUNCTIONS
.HF
*SCF INPUT
.THRESH
1.0D-08
**PROPERTIES
*END OF INPUT
#2 End of Dalton Input

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Singlepoint
#2 Name
dalton_2
#2 PropertyInfo
$PROPINFO2
#2 Type
SP_DALTON
#2 DaltonBasis
1  3-21G
6  3-21G
#2 Dalton Input
**DALTON INPUT
.DIRECT
.OPTIMIZE
.RUN PROPERTIES
*OPTIMIZE
.2NDORD
.MAX IT
0
**WAVE FUNCTIONS
.HF
*SCF INPUT
.THRESH
1.0D-10
**PROPERTIES
**INTEGRALS
*END OF INPUT
#2 End of Dalton Input

#1 Singlepoint
#2 Name
dalton_3
#2 PropertyInfo
$PROPINFO3
#2 Type
SP_DALTON
#2 DaltonBasis
1  STO-3G
6  STO-3G
#2 Dalton Input
**DALTON INPUT
.DIRECT
.RUN PROPERTIES
**WAVE FUNCTIONS
.HF
*SCF INPUT
.THRESH
1.0D-08
**PROPERTIES
*END OF INPUT
#2 End of Dalton Input

#1 Pes
#2 IoLevel
12
#2 FitBasis
#3name
fitbas_0
#3 FitFunctionsMaxOrder
[12*12]
#3 FitBasisMaxOrder
12 10 8 8

#2 USEFITBASIS
fitbas_0
#2 MULTILEVELPES
dalton_1 1
dalton_2 2
#2 BoundariesPreopt
dalton_3
#2 Symmetry
D2h
#2 NThreads
4

#2 AnalyzeStates
[12*2]      // Would be bettet to set 12*3 to address fundamental
#2 AdgaGridInitialDim
2
#2 AdgaInfo
2
#2 AdgaRelScalFact
2.5  20.0 //30.0  // quite loose, better 1.0 10.0 
#2 ItResEnThr
1.0e-6
#2 Interpolate
 Shepard
 Surface
 2 4 
#2 Extrapolate
1 2

#1 Vib
#2 Operator    // This is operator input
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
prop_no_1.mop
#3 SetInfo
type=energy

#2 Basis     // This is basis input
#3 Name
basis_gauss
#3 PrimBasisDensity
0.8
#3 GaussianBasis
1.00
#3 ReadBoundsFromFile
#3 ScalBounds
1.5 20.0

#2 Vscf      // This the Vscf input
#3 IoLevel
2
#3 Name
vscf00
#3 Oper
h0
#3 Basis
basis_gauss
#3 OccGroundState
#3 ScreenZero
0.1E-20
#3 MaxIter
50

#2 Vcc      // This the VFCI input
#3 IoLevel
2
#3 Name
vfci00
#3 Oper
h0
#3 Basis
basis_gauss
#3 VscfScreenZero
0.1E-16
#3 LimitModalBasis
[12*8]
#3 Vci
10
#3 MaxExci
2
#3 Reference
#3 AllFundamentals
//#3 EigValSeq
//5
#3 ItEqResidThr                // Convergence Threshold for the VCC error vector norm
1.0e-08 //
#3 ItEqEnerThr                 // Convergence Threshold for VCC energy between two iterations
1.0e-10 //
#3 ItEqMaxIt                 // maximum allowed number of interations
100
#3 ItEqMaxDim                 //maximum allowed dimension of the reduced space
600
#1 Analysis
#0 Midas Input End

%EOF%


#######################################################################
#   
#######################################################################
cat > prop_no_1.mop <<%EOF%
%EOF%
#######################################################################
#  MOLECULE INPUT 
#######################################################################
cat > pes_MAdga_Extr1_2.mmol <<%EOF%
#0 MIDASMOLECULE
#1 XYZ
6 au
comment
 C         0.0000000000            0.0000000000            1.2580463802
 C         0.0000000000            0.0000000000           -1.2580463802
 H         0.0000000000            1.7368277843            2.3324947342
 H         0.0000000000           -1.7368277843            2.3324947342
 H         0.0000000000            1.7368277843           -2.3324947342
 H         0.0000000000           -1.7368277843           -2.3324947342

#1 FREQ
12 cm-1
 0.3464945870918055E+04  B2u
 0.3437054711749664E+04  B3g
 0.3364467650602502E+04  Ag
 0.3340878071061098E+04  B1u
 0.1789966783023737E+04  Ag
 0.1583056491084732E+04  B1u
 0.1470999022976295E+04  Ag
 0.1337439562297737E+04  B3g
 0.1098213769534253E+04  Au
 0.1089504174710369E+04  B2g
 0.1084782186402990E+04  B3u
 0.8937299795962620E+03  B2u

#1 VIBCOORD
au
#2 COORDINATE
 0.0000000000000000E+00-6.6833563714072536E-02 0.0000000000000000E+00
 0.0000000000000000E+00-6.6833563714499084E-02 0.0000000000000000E+00
 0.0000000000000000E+00 3.9788790939518048E-01 2.5130038483377692E-01
 0.0000000000000000E+00 3.9788790939270974E-01-2.5130038483723838E-01
 0.0000000000000000E+00 3.9788790939191637E-01-2.5130038483782613E-01
 0.0000000000000000E+00 3.9788790939438967E-01 2.5130038483436673E-01
#2 COORDINATE
 0.0000000000000000E+00 6.6051347697506840E-02 0.0000000000000000E+00
 0.0000000000000000E+00-6.6051347697508506E-02 0.0000000000000000E+00
 0.0000000000000000E+00-3.9891717622463219E-01-2.5089955021318150E-01
 0.0000000000000000E+00-3.9891717622342887E-01 2.5089955021387012E-01
 0.0000000000000000E+00 3.9891717622342998E-01-2.5089955021390464E-01
 0.0000000000000000E+00 3.9891717622462980E-01 2.5089955021314847E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00 5.4514940806834868E-02
 0.0000000000000000E+00 0.0000000000000000E+00-5.4514940806874156E-02
 0.0000000000000000E+00-4.1153881506927070E-01-2.4698565997626654E-01
 0.0000000000000000E+00 4.1153881506998713E-01-2.4698565997414260E-01
 0.0000000000000000E+00-4.1153881506986562E-01 2.4698565997437708E-01
 0.0000000000000000E+00 4.1153881506914897E-01 2.4698565997649308E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00-4.1987824590643506E-02
 0.0000000000000000E+00 0.0000000000000000E+00-4.1987824590633320E-02
 0.0000000000000000E+00 4.1842292011446852E-01 2.4997092505437479E-01
 0.0000000000000000E+00-4.1842292011396520E-01 2.4997092505615981E-01
 0.0000000000000000E+00-4.1842292011446935E-01 2.4997092505441268E-01
 0.0000000000000000E+00 4.1842292011388477E-01 2.4997092505614887E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00-1.7161364289332148E-01
 0.0000000000000000E+00 0.0000000000000000E+00 1.7161364289335068E-01
 0.0000000000000000E+00-2.2476651897743799E-01 1.4900837600637512E-01
 0.0000000000000000E+00 2.2476651897947547E-01 1.4900837600512759E-01
 0.0000000000000000E+00-2.2476651897959776E-01-1.4900837600530289E-01
 0.0000000000000000E+00 2.2476651897756267E-01-1.4900837600654979E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00-6.5032933418574024E-02
 0.0000000000000000E+00 0.0000000000000000E+00-6.5032933418465888E-02
 0.0000000000000000E+00-2.7015032616298423E-01 3.8716801082733149E-01
 0.0000000000000000E+00 2.7015032616657070E-01 3.8716801082506686E-01
 0.0000000000000000E+00 2.7015032616669205E-01 3.8716801082486552E-01
 0.0000000000000000E+00-2.7015032616311513E-01 3.8716801082712493E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00 9.6143358940546986E-02
 0.0000000000000000E+00 0.0000000000000000E+00-9.6143358940550150E-02
 0.0000000000000000E+00-1.6785337184944821E-01 4.0602158372469166E-01
 0.0000000000000000E+00 1.6785337184970456E-01 4.0602158372523794E-01
 0.0000000000000000E+00-1.6785337184952931E-01-4.0602158372484032E-01
 0.0000000000000000E+00 1.6785337184968352E-01-4.0602158372505259E-01
#2 COORDINATE
 0.0000000000000000E+00 1.2387829153580175E-01 0.0000000000000000E+00
 0.0000000000000000E+00-1.2387829153580179E-01 0.0000000000000000E+00
 0.0000000000000000E+00-1.1594086615480925E-01 3.7849248395620566E-01
 0.0000000000000000E+00-1.1594086615431691E-01-3.7849248395625962E-01
 0.0000000000000000E+00 1.1594086615237342E-01 3.7849248395742202E-01
 0.0000000000000000E+00 1.1594086615675366E-01-3.7849248395504431E-01
#2 COORDINATE
 0.0000000000000000E+00 0.0000000000000000E+00 0.0000000000000000E+00
 0.0000000000000000E+00 0.0000000000000000E+00 0.0000000000000000E+00
 4.9805515638544123E-01 0.0000000000000000E+00 0.0000000000000000E+00
-4.9805515638892262E-01 0.0000000000000000E+00 0.0000000000000000E+00
-4.9805515638892345E-01 0.0000000000000000E+00 0.0000000000000000E+00
 4.9805515638544051E-01 0.0000000000000000E+00 0.0000000000000000E+00
#2 COORDINATE
 1.2349901072855884E-01 0.0000000000000000E+00 0.0000000000000000E+00
-1.2349901072856034E-01 0.0000000000000000E+00 0.0000000000000000E+00
-3.9655696530288859E-01 0.0000000000000000E+00 0.0000000000000000E+00
-3.9655696530288814E-01 0.0000000000000000E+00 0.0000000000000000E+00
 3.9655696530289702E-01 0.0000000000000000E+00 0.0000000000000000E+00
 3.9655696530289658E-01 0.0000000000000000E+00 0.0000000000000000E+00
#2 COORDINATE
-7.7409688301053742E-02 0.0000000000000000E+00 0.0000000000000000E+00
-7.7409688301051091E-02 0.0000000000000000E+00 0.0000000000000000E+00
 4.6085196319620636E-01 0.0000000000000000E+00 0.0000000000000000E+00
 4.6085196319244398E-01 0.0000000000000000E+00 0.0000000000000000E+00
 4.6085196319243743E-01 0.0000000000000000E+00 0.0000000000000000E+00
 4.6085196319619987E-01 0.0000000000000000E+00 0.0000000000000000E+00
#2 COORDINATE
 0.0000000000000000E+00-3.9058092684319083E-02 0.0000000000000000E+00
 0.0000000000000000E+00-3.9058092684315128E-02 0.0000000000000000E+00
 0.0000000000000000E+00 2.3252901655147731E-01-4.3000820385824218E-01
 0.0000000000000000E+00 2.3252901655132641E-01 4.3000820385788513E-01
 0.0000000000000000E+00 2.3252901655148436E-01 4.3000820385824651E-01
 0.0000000000000000E+00 2.3252901655133337E-01-4.3000820385789029E-01
#0 MIDASMOLECULEEND

Norm of Vectors
 0.9459387601310501E+00
 0.9471365013560236E+00
 0.9630203568521195E+00
 0.9766159780728722E+00
 0.5914358980893159E+00
 0.9486725642675992E+00
 0.8891570246609627E+00
 0.8108557465121503E+00
 0.9961103127743639E+00
 0.8121168131634905E+00
 0.9281824430606166E+00
 0.9792641446017969E+00
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/$PROPINFO3 << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),type=(energy)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0),type=(dipole_x)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1),type=(dipole_y)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2),type=(dipole_z)
%EOF%

cat > $INTERFACEDIR/$PROPINFO1 << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),type=(energy)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0),type=(dipole_x)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1),type=(dipole_y)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2),type=(dipole_z)
%EOF%

cat > $INTERFACEDIR/$PROPINFO2 << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),der_file=(midasifc.ederivs),type=(energy)
tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0),type=(dipole_x)
tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1),type=(dipole_y)
tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2),type=(dipole_z)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

# Basic logic: basic setup in numder.
CRIT1=`$GREP "Derivative information on GROUND\_STATE\_ENERGY is not available for level\_0" $log | wc -l`  # 1
CRIT2=`$GREP "Derivative information on GROUND\_STATE\_ENERGY is not available for level\_1" $log | wc -l`  # 1
CRIT3=`$GREP "Derivative information on GROUND\_STATE\_ENERGY is available for level\_2" $log | wc -l`      # 1
CRIT4=`$GREP "Derivative information is used to generate extrapolated surface" $log | wc -l`                # 1
CRIT5=`$GREP "Updating derivative information for property: GROUND_STATE_ENERGY" $log | wc -l`              # 2
CRIT6=`$GREP "The order used for the energy is 2" $log | wc -l`                                             # 3
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
CTRL[1]=9
ERROR[1]="BASIC SETUP of Multilevel is not correct"

# iterations
CRIT2=`$GREP "Total : 25" $log | wc -l`    # 3
CRIT3=`$GREP "Total : 49" $log | wc -l`    # 4
CRIT4=`$GREP "The Adga is converged for 1-mode grids" $log | wc -l`          # 3
TEST[2]=`expr $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[2]=10
ERROR[2]="NUMBER OF CALCULATIONS/ITERATIONS WRONG"

#  extrapolation
CRIT1=`$GREP "Multilevel Procedure for PES: Completed" $log  |  wc -l`             # 1
CRIT2=`$GREP "Multilevel Procedure: Parsing the mop files" $log  |  wc -l`         # 1
TEST[3]=`expr $CRIT1 \+ $CRIT2`
CTRL[3]=2
ERROR[3]="Intrinsic Mode Combination and merging of the prop file: WRONG"

# vci energies
# 0.0000000000000000E+00
# 3.2966728374916643E+03
# 3.2647148685165321E+03
# 3.1727078157680467E+03
# 3.2054720777001176E+03
# 1.8424178766302484E+03
# 1.6335320572133737E+03
# 1.5172882762247104E+03
# 1.3913387433874116E+03
# 1.1672996519955591E+03
# 1.1679789177441585E+03
# 1.1385519338077731E+03
# 9.6412067982657470E+02
CRIT1=`$GREP "T_[[:digit:]]{1,2}.*0\.00000000[[:digit:]]*E\+00" $log | wc -l`       #2
CRIT2=`$GREP "T_[[:digit:]]{1,2}.*3\.29667283[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT3=`$GREP "T_[[:digit:]]{1,2}.*3\.26471486[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT4=`$GREP "T_[[:digit:]]{1,2}.*3\.17270781[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT5=`$GREP "T_[[:digit:]]{1,2}.*3\.205472[01][[:digit:]]*E\+03" $log | wc -l`     #1
CRIT6=`$GREP "T_[[:digit:]]{1,2}.*1\.84241787[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT7=`$GREP "T_[[:digit:]]{1,2}.*1\.63353205[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT8=`$GREP "T_[[:digit:]]{1,2}.*1\.51728827[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT9=`$GREP "T_[[:digit:]]{1,2}.*1\.39133874[[:digit:]]*E\+03" $log | wc -l`       #1
CRIT10=`$GREP "T_[[:digit:]]{1,2}.*1\.1672996[5-8][[:digit:]]*E\+03" $log | wc -l`  #1
CRIT11=`$GREP "T_[[:digit:]]{1,2}.*1\.16797891[[:digit:]]*E\+03" $log | wc -l`      #1
CRIT12=`$GREP "T_[[:digit:]]{1,2}.*1\.13855193[[:digit:]]*E\+03" $log | wc -l`      #1
CRIT13=`$GREP "T_[[:digit:]]{1,2}.*9\.64120679[[:digit:]]*E\+02" $log | wc -l`      #1
TEST[4]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13`
CTRL[4]=14
ERROR[4]="VCI ENERGIES WRONG"

PASSED=1
for i in 0 1 2 3 4
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_MAdga_Extr1_2.check
#######################################################################
