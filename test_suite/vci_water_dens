#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > vci_water_dens.info <<%EOF%
   IR intensities
   Raman intensities
   -----------------
   Molecule:      water
   Wave Function: VCI[3]
   Test Purpose:  Test calculation of density matrix 
                  
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vci_water_dens.minp <<%EOF%
#0 MIDAS Input

#1 general
#2 IoLevel
    1 
!#2 Debug 
#2 Time

#1 Vib

// Hamiltonian.
#2 Operator
   #3 Name
      h0
   #3 OperFile
      wh.mop 
   #3 KineticEnergy
      SIMPLE

// Dipole operators.
#2 Operator
   #3 Name
      x
   #3 OperFile
      wx.mop
   #3 SetInfo
      type=x_dipole
#2 Operator
   #3 Name
      y
   #3 OperFile
      wy.mop
   #3 SetInfo
      type=y_dipole
#2 Operator
   #3 Name
      z
   #3 OperFile
      wz.mop
   #3 SetInfo
      type=z_dipole
#2 Operator
   #3 Name
      u1
   #3 OperFile
      u1.mop
#2 Operator
   #3 Name
      u2
   #3 OperFile
      u2.mop
#2 Operator
   #3 Name
      u3
   #3 OperFile
      u3.mop
#2 Operator
   #3 Name
      u14
   #3 OperFile
      u14.mop
#2 Operator
   #3 Name
      u24
   #3 OperFile
      u24.mop
#2 Operator
   #3 Name
      u34
   #3 OperFile
      u34.mop

#2 Basis
   #3 Name
      basis
   #3 HoBasis 
      5 
   #3 IoLevel
      1

#2 Vscf
   #3 IoLevel
      11 
   #3 Name 
      scf0
   #3 Oper
      h0 
   #3 Basis
      basis
   #3 Occup
      [3*0] 
   #3 Threshold
      1.0e-15
   #3 MaxIter
      20

#2 Vcc
   #3 Vci
      1 
   #3 Transformer
      trf=v3 type=gen
   #3 VscfThreshold
      1.0e-15
   //#3 LimitModalBasis
      //[3*3]
   #3 MaxExci
      3
   #3 ItEqResidThr
      1.0e-9
   #3 Oper
      h0
   #3 Basis 
      basis 
   #3 VecStorage 
      0
   #3 ItEqMaxDim
      150
   #3 ItEqMaxIt
      60
   #3 ItEqBreakDim
      100
   #3 DISABLECHECKMETHOD 
   #3 Rsp
      #4 IoLevel
         3  
      #4 ItEqResidThr
         1.0e-9
      #4 ItEqMaxIt
         100
      #4 ItEqMaxDim
         200
      #4 ItEqBreakDim
         150
      #4 ItEqResidThr
         1.0e-9
      #4 RspFunc
         1 h0
         1 x 
         1 y 
         1 z 
         1 u1
         1 u2
         1 u3
         1 u14
         1 u24
         1 u34
      #4 DensityAnalysis 

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATOR INPUTS
#######################################################################
cat > wh.mop << %EOF%
DALTON_FOR_MIDAS
 2.7010827352569322101772E-08    1     
 2.8424599776144532370381E-05    1     1     
 1.3386473085574834840372E-07    1     1     1     
-7.0940586738288402557373E-11    1     1     1     1     
 2.6339947112319350708276E-07    2     
 1.5633553209681849693879E-04    2     2     
 3.2519419619347900152206E-06    2     2     2     
 4.4874468585476279258728E-08    2     2     2     2     
-2.1032064978498965501785E-12    3     
 1.6460214658309268997982E-04    3     3     
-4.0927261579781770706177E-11    3     3     3     
 4.7789967538847122341394E-08    3     3     3     3     
 2.0551169654936529695988E-09    1     2     
-2.5825875127338804304600E-07    1     2     2     
-9.0590219770092517137527E-09    1     2     2     2     
-7.5936259236186742782593E-07    1     1     2     
-4.7161620386759750545025E-08    1     1     2     2     
 1.0610847311909310519695E-08    1     1     1     2     
-5.3134385780140291899443E-10    1     3     
-9.5156394763762364163995E-07    1     3     3     
 2.7625901566352695226669E-11    1     3     3     3     
 3.2969182939268648624420E-12    1     1     3     
-5.8064415497938171029091E-08    1     1     3     3     
 8.1001871876651421189308E-13    1     1     1     3     
 7.1281647251453250646591E-11    2     3     
 1.0034688898485910613090E-05    2     3     3     
-4.8942183639155700802803E-11    2     3     3     3     
 6.0367710830178111791611E-11    2     2     3     
 2.7796704671345651149750E-07    2     2     3     3     
-4.6895820560166612267494E-11    2     2     2     3     
-6.0538241086760535836220E-11    1     2     3     
-5.5532041187689173966646E-08    1     2     3     3     
-9.0949470177292823791504E-12    1     2     2     3     
-8.2650331023614853620529E-11    1     1     2     3     
%EOF%

cat > wx.mop << %EOF%
DALTON_FOR_MIDAS
-6.6842413704537194957048E-09    1     
 2.7495539082702343629487E-11    1     1     
-2.6577730496518208152656E-14    1     1     1     
-1.9243750023815340322887E-15    1     1     1     1     
 8.7031339808670034236256E-10    2     
 6.1701425681861050280839E-12    2     2     
 1.2638958758752200335762E-14    2     2     2     
-3.1465270588004975602716E-15    2     2     2     2     
-4.5339976190651877874024E-10    3     
-2.8781087697602811989638E-12    3     3     
-2.2037335532810066197077E-14    3     3     3     
-6.1553701643762560057115E-15    3     3     3     3     
-5.0966270013948344272623E-11    1     2     
-2.3065607893864664376150E-13    1     2     2     
-4.3501350609555308683704E-15    1     2     2     2     
 5.3229820683384212876683E-13    1     1     2     
 1.1974993275116593209433E-15    1     1     2     2     
-3.9632041703992253734432E-15    1     1     1     2     
 7.9850565047301312625167E-12    1     3     
-4.3111648587846334191188E-13    1     3     3     
-1.1618695020077829166329E-15    1     3     3     3     
 1.8745572242492096533842E-13    1     1     3     
-5.4883828439656145536767E-15    1     1     3     3     
-1.3329699935713902996253E-15    1     1     1     3     
-4.0115750349663257104998E-12    2     3     
-1.2951881256445723997279E-14    2     3     3     
 4.1679804139936543434478E-16    2     3     3     3     
-2.6757612433831219839872E-14    2     2     3     
-3.4093393532730237495279E-15    2     2     3     3     
 2.7575627639110927733580E-15    2     2     2     3     
 7.4893182621297349406972E-14    1     2     3     
-1.3630286434987975302275E-14    1     2     3     3     
 2.0054966422777370212496E-15    1     2     2     3     
 2.1152677182584804635099E-15    1     1     2     3     
%EOF%

cat > wy.mop << %EOF%
DALTON_FOR_MIDAS
-4.9001593381522873324146E-08    1     
 2.1395936980847815701582E-10    1     1     
-6.4726259905858915477629E-12    1     1     1     
-1.6155878275993810889543E-13    1     1     1     1     
 1.5000681274626974980791E-08    2     
 1.8566935688096359236747E-10    2     2     
-5.9880454340826453775545E-12    2     2     2     
-4.8299801838472880408841E-13    2     2     2     2     
 5.7958351563652948934546E-03    3     
-4.7253070033226318713560E-10    3     3     
-3.9184958257332258568795E-08    3     3     3     
-1.8600780637978431286683E-12    3     3     3     3     
-8.5428479653300186530542E-10    1     2     
 1.5127281214119169731967E-12    1     2     2     
 1.9547047756770437681680E-13    1     2     2     2     
 4.5488941888270788258312E-12    1     1     2     
-1.4716038715354042140313E-12    1     1     2     2     
-2.0063305201415759722620E-13    1     1     1     2     
-1.2390280233786450311828E-04    1     3     
 1.2157748766061793332938E-11    1     3     3     
 7.1190353993777488739170E-09    1     3     3     3     
 1.0311752102077492709853E-06    1     1     3     
-3.9046717248414353207409E-12    1     1     3     3     
 9.0275075175644259317664E-11    1     1     1     3     
 6.2527702367224866075723E-05    2     3     
 1.0908794700892343598753E-11    2     3     3     
 5.4826813748742875276321E-09    2     3     3     3     
-1.5422835796252698514763E-06    2     2     3     
-6.0163957149583779937529E-12    2     2     3     3     
 3.2687490526817075675581E-09    2     2     2     3     
-4.5407412490629239787410E-07    1     2     3     
-5.9409074881777712562325E-13    1     2     3     3     
 4.6225682445028004607934E-08    1     2     2     3     
 1.5683941400820855527343E-08    1     1     2     3     
%EOF%

cat > wz.mop << %EOF%
DALTON_FOR_MIDAS
 6.3833910875265020834490E-03    1     
-3.5784894862000982129757E-05    1     1     
 2.4662934638985944957312E-07    1     1     1     
-1.4804497627807222670526E-09    1     1     1     1     
-1.5543086316722742878937E-03    2     
-3.0080150906108826802665E-05    2     2     
 3.3112461927942149486626E-07    2     2     2     
 3.2061659993587454664521E-09    2     2     2     2     
 1.8193269113453425234184E-08    3     
 1.3248539434407291537354E-05    3     3     
-3.7019276533101219683886E-12    3     3     3     
 2.2643842356728782760911E-10    3     3     3     3     
 3.5258745710198269307512E-05    1     2     
 3.9485259595295474355225E-07    1     2     2     
-4.2361723018302654963918E-09    1     2     2     2     
-2.9966326708930068889458E-07    1     1     2     
-1.1178988401638889627066E-08    1     1     2     2     
 1.5950716747425985886366E-08    1     1     1     2     
-3.5149061439199158485280E-10    1     3     
-4.6105526063655588586698E-07    1     3     3     
-1.5549339593690092442557E-11    1     3     3     3     
 3.8671288393743452616036E-12    1     1     3     
-1.8701555859479412902147E-09    1     1     3     3     
 1.6409096303959813667461E-13    1     1     1     3     
 3.8473890739965099783149E-10    2     3     
 9.1314835959366291717743E-07    2     3     3     
 5.9419136277938378043473E-13    2     3     3     3     
 7.6125772352497733663768E-12    2     2     3     
 5.1161919145670253783464E-10    2     2     3     3     
-5.9141136432572238845751E-11    2     2     2     3     
 1.7475798586019664071500E-11    1     2     3     
-7.3213257678617083001882E-09    1     2     3     3     
 1.0097522817886783741415E-10    1     2     2     3     
 6.2116534138567658374086E-11    1     1     2     3     
%EOF%

cat > u1.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  1     
%EOF%
cat > u2.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  2     
%EOF%
cat > u3.mop << %EOF%
DALTON_FOR_MIDAS
 1.0  3     
%EOF%
cat > u14.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    1     1     1     1
%EOF%
cat > u24.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    2     2     2     2
%EOF%
cat > u34.mop << %EOF%
DALTON_FOR_MIDAS
 1.0    3     3     3     3
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Are the responses correct?
#Perform density matrix analysis
CRIT1=`$GREP "Density matrix " $log | $GREP "analysis" | wc -l`
CRIT2=`$GREP "Trace of mode " $log | $GREP "density " |  $GREP " 1.00000000......" | wc -l`
CRIT3=`$GREP "Trace of mode " $log | $GREP "density " |  $GREP " 9.99999999......" | wc -l`
CRIT4=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 9.9870185818......" | wc -l`
CRIT5=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 0.99870185818....." | wc -l`
CRIT6=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 1.29434305218....." | wc -l`
CRIT7=`$GREP "Mode 2 natural modal " $log | $GREP "occupation" |  $GREP " 0.129434305218..." | wc -l`
##CRIT1=`$GREP "Perform ,y,z>>\(" $log | $GREP "= -3.6074840677[78].....E-13" | wc -l`
TEST[1]=`expr $CRIT1`
CTRL[1]=4
ERROR[1]="IO not correct"
TEST[2]=`expr $CRIT2 \+ $CRIT3`
CTRL[2]=3 
ERROR[2]="Density matrices not correct"
TEST[3]=`expr $CRIT4 \+ $CRIT5 \+  $CRIT6 \+  $CRIT7`
CTRL[3]=2 
ERROR[3]="occupation numbers not correct"

PASSED=1
for i in 1 2 3 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}"! "${TEST[i]}" should be: "${CTRL[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vci_water_dens.check
#######################################################################

