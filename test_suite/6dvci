#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy  in various 
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980), for operator
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals

                     Test covers both the 
                     FtesttranExpH (primitive full basis explicit H CI)
                     ExpHtesttran option.
                     In the later case both the full testtran which gives results
                     equivalent to the first, and in 
                     addition the single excited case, which in the modal
                     basis, gives the HF result for the reference state, 
                     which I thing is a nice check.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 6dvci.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
  4


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
   4

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a0)
-0.5 DDQ^2(b0)
0.98 Q^2(a0)
0.245 Q^2(b0)
0.0064 Q^3(a0)
-0.08 Q^1(a0) Q^2(b0)
-0.5 DDQ^2(a1)
-0.5 DDQ^2(b1)
0.98 Q^2(a1)
0.245 Q^2(b1)
0.0064 Q^3(a1)
-0.08 Q^1(a1) Q^2(b1)
-0.5 DDQ^2(a2)
-0.5 DDQ^2(b2)
0.98 Q^2(a2)
0.245 Q^2(b2)
0.0064 Q^3(a2)
-0.08 Q^1(a2) Q^2(b2)
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a0 HO n_high=8,omeg=1.4
 b0 HO n_high=8,omeg=0.7
 a1 HO n_high=8,omeg=1.4
 b1 HO n_high=8,omeg=0.7
 a2 HO n_high=8,omeg=1.4
 b2 HO n_high=8,omeg=0.7
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Occup
 0 0 0 0 0 0 

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Occup
 1 3 1 3 1 3 

#2 Vcc       // This a the VCC  input
#3 Name
   1vci
#3 Vmp
 2 
#3 IoLevel
11
#3 Occup
 1 3 1 3 1 3
#3 LimitModalBasis
 2 4 2 4 2 4
#3 ExpHvci
 1
#3 MaxExci 
 1
#3 TestTransformer
#3 RepVciAsVcc

#2 Vcc       // This a the VCC  input
#3 Name
   2vci
#3 LimitModalBasis
 3 3 3 3 3 3
#3 IoLevel
11
#3 Basis
basis
#3 Occup
 0 0 0 0 0 0 
!#3 FvciExpH
#3 ExpHvci
 2
#3 TestTransformer
#3 MaxExci 
 2
#3 Vci
 6 
#3 RepVciAsVcc

#2 Vcc       // This a the VCC  input
#3 Name
   3vci
#3 LimitModalBasis
 3 3 3 3 3 3 
#3 IoLevel
11
#3 Basis
basis
#3 Occup
 0 0 0 0 0 0
!#3 FvciExpH
#3 ExpHvci
 6
#3 TestTransformer
!#3 MaxExci 
! 6
#3 Iso
 3
#3 Vci
 6 
#3 RepVciAsVcc

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`                                            #2
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=13
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# FVCI energy:
CRIT01=`$GREP "6.94539193721[1,2]" $log | grep "ExpHvci" | wc -l`
CRIT02=`$GREP "1.341019670303" $log | grep "ExpHvci" | wc -l`

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
`
CTRL[2]=6
ERROR[2]="EXPHVCI ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02


CRIT05=`$GREP "ItEqRoot_" $log | grep "6.94539193721[1,2]" |wc -l`

TEST[3]=`expr \
$CRIT05 \
`
CTRL[3]=3
ERROR[3]="VCI ENERGIES NOT CORRECT"
echo $CRIT05


PASSED=1
for i in 1 2 3
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 6dvci.check
#######################################################################
