#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="tdvcc_2m_water_vccgs"

#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water 2M-coupled
   Wave Function:    TDVCC
   Test Purpose:     Propagate TDVCC state.
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ${TESTNAME}.mop
   #3 SetInfo
      type=energy
#2 Basis
   #3 Name
      basis
  #3 BSplineBasis
      10
   #3 PrimBasisDensity
      0.8
   #3 TurningPoint
      10
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      basis
   #3 Oper
      h0
   #3 Threshold
      1.0e-15
#2 Vcc
   #3 Name
      vcc
   #3 UseVscf
      vscf
   #3 LimitModalBasis
      [3*6]
   #3 Method
      VCC[2H2]
   #3 ItEqResidThr
      1.0e-15
   #3 Rsp
      #4 ItEqResidThr
         1.0e-15
      #4 LambdaThreshold // Needed? Not in manual...
         1.0e-15
      #4 RspFunc
         1 h0
#2 Tdvcc
   #3 IoLevel
      10
   #3 Name
      tdvcc
   #3 CorrMethod
      vcc
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 ModalBasisFromVscf
      vscf_init
   #3 VccGs
      vcc
   #3 LimitModalBasis
      [3*6]
   #3 InitState
      vccgs
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         1
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e3
      #4 OutputPoints
         1000
      #4 Tolerance
         1.e-15 1.e-15
      #4 MaxSteps
         -1
   #3 Properties
      energy
      phase
   #3 Statistics
      norm2
      dnorm2init
      dnorm2vccgs

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASOPERATOR
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
-1.3983481039758772E-11    Q^1(Q0)
 1.5756874643102492E-04    Q^2(Q0)
-1.3022827261011116E-10    Q^3(Q0)
 4.6707384626643034E-08    Q^4(Q0)
 4.5483926669476205E-07    Q^1(Q1)
 1.4937195226139011E-04    Q^2(Q1)
 3.0827675345790340E-06    Q^3(Q1)
 4.3788077164208516E-08    Q^4(Q1)
 3.7033530020380567E-08    Q^1(Q2)
 2.7465351735145305E-05    Q^2(Q2)
 1.4056156771857786E-07    Q^3(Q2)
-5.4826898576720851E-10    Q^4(Q2)
 2.2424728740588762E-10    Q^1(Q0)     Q^1(Q1)
 1.3244516594568267E-10    Q^1(Q0)     Q^2(Q1)
 1.0004441719502211E-11    Q^1(Q0)     Q^3(Q1)
 9.5331590728164883E-06    Q^2(Q0)     Q^1(Q1)
 2.7097757993033156E-07    Q^2(Q0)     Q^2(Q1)
-4.0927261579781771E-12    Q^3(Q0)     Q^1(Q1)
 1.6018475434975699E-10    Q^1(Q0)     Q^1(Q2)
 1.0061285138363019E-11    Q^1(Q0)     Q^2(Q2)
-1.2647660696529783E-12    Q^1(Q0)     Q^3(Q2)
-9.1266440449544461E-07    Q^2(Q0)     Q^1(Q2)
-5.4972815632936545E-08    Q^2(Q0)     Q^2(Q2)
-7.3896444519050419E-13    Q^3(Q0)     Q^1(Q2)
-2.8100544113840442E-10    Q^1(Q1)     Q^1(Q2)
-7.2744757062537246E-07    Q^1(Q1)     Q^2(Q2)
 9.8503392109705601E-09    Q^1(Q1)     Q^3(Q2)
-2.2016263301338768E-07    Q^2(Q1)     Q^1(Q2)
-4.5356273403740488E-08    Q^2(Q1)     Q^2(Q2)
-9.0807930064329412E-09    Q^3(Q1)     Q^1(Q2)
#0MIDASOPERATOREND
%EOF%


#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check max difference between initial and propagated state (should be numerically zero)
# ------------------------------------------------------------------------------
CRIT_ITER_DNORM2INIT[1]=`$GREP "DNORM2INIT \(KET\), max" $log | grep "[3-4]\..*E-29" |  wc -l`
CRIT_ITER_DNORM2INIT[2]=`$GREP "DNORM2INIT \(BRA\), max" $log | grep "5\..*E-28" |  wc -l`

for i in $(seq 1 2)
do
   TEST+=(${CRIT_ITER_DNORM2INIT[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''difference norm compared to initial wave function (error in $i : 1=ket, 2=bra)'\'', CRIT_ITER_DNORM2INIT[$i].")
done

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
