#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         1-dim HO, squeezed potential
   Wave Function:    VSCF / 32 HO basis 
   Test Purpose:     Propagate VSCF state in squeezed V and check energy
   Reference:        Christiansen: Journal of Chemical Physics, 120, 2004
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > lintdh_1dho_squeeze.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

#1 General

#1 Vib
#2 IOLevel
 5

#2 Operator
#3 IoLevel
14
#3 Name
 vscfoper
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(a)
 0.0001125 Q^2(a)
#3 KineticEnergy
 USER_DEFINED

#2 Operator
#3 Name
 tdhoper
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(a)
 0.5001125 Q^2(a)
// Note 2*0.49 = 0.98 = 0.5*(1.4)**2
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 testbasis
#3 Define
 a HO n_high=31,omeg=0.015 // ,mass=1

#2 vscf
#3 Name
 VSCF-testoper
#3 Basis
 testbasis
#3 oper
 vscfoper

#2 TdH
#3 IoLevel
   5
#3 LinearTdH
#3 LinearTdHConstraint
   ZERO
#3 Name
 TDH-testoper
#3 Basis
 testbasis
#3 Oper
 tdhoper
#3 Integrator
   #4 IoLevel
      1
   #4 Type
      MIDAS DOPR853
   #4 TimeInterval
      0. 1.
   #4 OutputPoints
      20
   #4 Tolerance
      1.e-16 1.e-16
   #4 MaxSteps
      10000
   #4 InitialStepSize
      1.e-5
#3 Properties
   energy
   phase
   autocorr
   halftimeautocorr
#3 Spectra
   autocorr
   halftimeautocorr
#3 SpectrumEnergyShift
   E0

#0 Midas Input End

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do TdH:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=3
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT! "

# Squeezed potential energy:
CRIT1=`$GREP "t = 0\.0000000000000000E\+00" $log | $GREP "E = \(1\.6674166666666" | wc -l`
CRIT2=`$GREP "t = 1\.0000000000000000E\+00" $log | $GREP "E = \(1\.6674166666666" | wc -l`

#echo $CRIT1
#echo $CRIT2

TEST[2]=`expr $CRIT1 \+ $CRIT2`
CTRL[2]=2
ERROR[2]="TDH ENERGY NOT CORRECT! "

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > lintdh_1dho_squeeze.check
#######################################################################
