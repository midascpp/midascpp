#!/bin/sh
#
# This is a script for generating files for a specific MidasCpp test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not he old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > git_version_info.info <<%EOF%
   Test Purpose: 
   Check that the git information is correctly output in the header of the
   output file. Expected fields:
   - Git commit : <commit_hash> (clean)
   - Git branch : <branch_name>
   - Git date   : YYYY-MM-DD hh:mm:ss +hhmm
   NB! If you have uncommitted local changes, this test will fail, and that can
   be alright. But for a clean checkout of a commit, this must pass.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > git_version_info.minp <<%EOF%
#0 Midas Input
#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################

echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check that this is a git repo
# ------------------------------------------------------------------------------
test_git_repo_true=$($GREP -c "Git info   : Built from a git repository." $log)
test_git_repo_false=$($GREP -c "Git info   : N/A \(Not built from a git repository.\)" $log)
test_git_not_working=$($GREP -c "Git info   : N/A \(.*command not working.\)" $log)
# Exactly one of the above must be true.
TEST+=($(expr $test_git_repo_true + $test_git_repo_false + $test_git_not_working))
CTRL+=(1)
ERROR+=("Bad \"Git info:\" line.")

# ------------------------------------------------------------------------------
# Check the git header information, _if_ it is a git repository
# ------------------------------------------------------------------------------
if test "$test_git_repo_true" -eq "1"
then
   TEST+=($($GREP -c "Git branch : " $log))
   CTRL+=(1)
   ERROR+=("Could not find branch name in output file header.")

   TEST+=($($GREP -c "Git date   : ....-..-.. ..:..:.. +...." $log))
   CTRL+=(1)
   ERROR+=("Could not find commit date in output file header.")

   test_commit=$($GREP -c "Git commit : [0-9a-f]*" $log)
   TEST+=($test_commit)
   CTRL+=(1)
   ERROR+=("Could not find commit hash in output file header.")

   # ---------------------------------------------------------------------------
   # Furthermore check the clean/dirty tag, if "Commit  :" was found
   # ---------------------------------------------------------------------------
   if test "$test_commit" -eq "1"
   then
      test_commit_dirty=$($GREP -c "Git commit : [0-9a-f]* \(dirty\)" $log)
      TEST+=($test_commit_dirty)
      CTRL+=(0)
      ERROR+=("Commit is listed as \"(dirty)\".")

      test_commit_clean=$($GREP -c "Git commit : [0-9a-f]* \(clean\)" $log)
      TEST+=($test_commit_clean)
      CTRL+=(1)
      ERROR+=("Commit is not listed as \"(clean)\".")
   fi
fi

# ------------------------------------------------------------------------------
# Final checks
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

# Some extra information if the commit was dirty/not clean.
if test "$test_git_repo_true" -eq "1"
then
   if test "$test_commit_dirty" -ne "0" -o "$test_commit_clean" -ne "1"
   then
      echo ""
      echo "Help:"
      echo "It looks like there were local uncommitted changes in your git"
      echo "repository when compiling the program. If this was intended you can"
      echo "disregard this failed test."
      echo "But if this was intended to be a compilation from a clean commit"
      echo "checkout, something is wrong... Possible explanations:"
      echo "- you had files not related to MidasCpp in the repository; try"
      echo "  moving them elsewhere?"
      echo "- there were auxilliary output files from test_suite, docu, and/or"
      echo "  manual; maybe something should have been added to .gitignore?"
      echo ""
   fi 
fi

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo "THERE IS A PROBLEM"
  exit 1
fi

' > git_version_info.check
#######################################################################

