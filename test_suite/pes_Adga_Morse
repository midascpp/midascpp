#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  INPUT
#######################################################################
cat > pes_Adga_Morse.info << %EOF%
   Molecule:         H2+
   Wave Function:    Morse potential
   Test Purpose:     Check Adga procedure with general fit-basis functions, (defined in external file)
%EOF%

#######################################################################
#  SETUP PROPINFO
#######################################################################

MAINDIR=$PWD/Dir_pes_Adga_Morse
export MAINDIR
rm -rf  $MAINDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Adga_Morse.minp << %EOF%
#0 MidasInput

#1 General
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
morse_singlepoint
#2 Type
SP_MODEL
#2 MorsePotential

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 IoLevel
5

#2 FitBasis
#3 IoLevel
5
#3 FitFunctionsMaxOrder
12
#3 FitBasisMaxOrder
20
#3 Name
fitbasis
#3 DefFromFile
$PWD/pes_Adga_Morse.mfitbas
#3 FitMethod
SVD

#2 UseFitBasis
fitbasis
#2 AdgaInfo
1
E_vscf_ref
#2 MultiLevelPes
morse_singlepoint 1
#2 AnalyzeDens
Max
#2 AnalyzeStates
[1*6]
#2 AdgaGridInitialDim
3
#2 NIterMax
10
#2 ItExpGridScalFact
2
#2 PesGridScalFac
1.2
#2 PesFGMesh
4
#2 PesFGScalFac
1.0
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-6
#2 ItResDensThr
1.0e-3
#2 NormalCoordinateThreshold
1e-7

#1 Vib
#2 Operator    // This is operator input
#3 IoLevel
2
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
prop_no_1.mop
#3 SetInfo
type=energy

#2 Basis     // This is basis input
#3 IoLevel
5
#3 Name
basis_bspline
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 NprimBasisFunctions
200
#3 ScalBounds
1.5 20.0

#2 Vscf      // This the Vscf input
#3 IoLevel
3
#3 Name
E_vscf_ref
#3 Oper
h0
#3 Basis
basis_bspline
#3 OccGroundState
#3 ScreenZero
1.0e-21
#3 MaxIter
50

#2 Vscf      // This the Vscf input
#3 IoLevel
3
#3 Name
E_vscf_state
#3 Oper
h0
#3 Basis
basis_bspline
#3 Occup
0
#3 OccuMax
5
#3 ScreenZero
1.0e-21

#1 Analysis

#0 MidasInputEnd
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > pes_Adga_Morse.mmol << %EOF%
#0 MidasMolecule

#1 XYZ
2 au

H  0.0    0.0   -0.9986
H  0.0    0.0    0.9986

#1 FREQ
1 cm-1
2401.8

#1 VIBCOORD
au
#2 Coordinate
   0.00000    0.00000   -7.0435635697260546E-01
   0.00000    0.00000    7.0435635697260546E-01

#0 MidasMoleculeEnd
%EOF%

#######################################################################
#  FITBAS INPUT
#######################################################################
cat > pes_Adga_Morse.mfitbas << %EOF%
#0 MidasFitInfo
#1 SpecFitBasis
#2 FitFunctions
EXPAND(EXP(-\$i*alpha*(Q)),\$i,[1..4;1])
#2 Modes
0
#2 OptFunc
F(Q,alpha,De) De*(1-EXP(-alpha*(Q)))*(1-EXP(-alpha*(Q))) (0,0.1,0.1)
#2 OptFuncThr
1.0e-12
#2 Props
Energy

#0 MidasFitInfoEnd
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),type=(energy)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a -o"
else
   GREP="egrep"
fi

# Did Midas end properly?
CRIT1=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT1`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED PROPERLY"

# Did we converge the 1-mode grids?
CRIT1=`$GREP "The Adga is converged for 1-mode grids" $log | wc -l`
TEST[1]=`expr $CRIT1`
CTRL[1]=1
ERROR[1]="1-MODE GRIDS ARE NOT CONVERGED"

# Do we obtain the right Morse variables?
CRIT1=`$GREP "2.3087515976" $log | tail -n1 | wc -l`
CRIT2=`$GREP "1.0260000000" $log | tail -n1 | wc -l`
TEST[2]=`expr $CRIT1 \+ $CRIT2`
CTRL[2]=2
ERROR[2]="MORSE VARIABLES ARE NOT DETERMINED CORRECT"

# Do we have the correct vscf energies?
CRIT1=`$GREP "2.2731657" $log | wc -l`
CRIT2=`$GREP "4.4183075" $log | wc -l`
CRIT3=`$GREP "6.4354253" $log | wc -l`
CRIT4=`$GREP "8.3245191" $log | wc -l`
CRIT5=`$GREP "1.00855891" $log | tail -n1 | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[3]=5
ERROR[3]="VSCF ENERGIES ARE WRONG"

PASSED=1
for i in 0 1 2 3
do
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

' > pes_Adga_Morse.check
#######################################################################
