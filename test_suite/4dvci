#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         4-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy  in various 
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980), for operator
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals

                     Test covers both the 
                     FtesttranExpH (primitive full basis explicit H CI)
                     ExpHtesttran option.
                     In the later case both the full testtran which gives results
                     equivalent to the first, and in 
                     addition the single excited case, which in the modal
                     basis, gives the HF result for the reference state, 
                     which I thing is a nice check.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 4dvci.minp <<%EOF%

#0 MIDAS Input

#1 general
#2 IoLevel
  5
#2 BufSIZE
 120
#2 FileSize
 500
#2 Time
!#2 NumLinAlg
!    MIDAS

#1 Vib
#2 IoLevel
 5

#2 Operator
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a0)
-0.5 DDQ^2(b0)
0.98 Q^2(a0)
0.245 Q^2(b0)
0.0064 Q^3(a0)
-0.08 Q^1(a0) Q^2(b0)
-0.5 DDQ^2(a1)
-0.5 DDQ^2(b1)
0.98 Q^2(a1)
0.245 Q^2(b1)
0.0064 Q^3(a1)
-0.08 Q^1(a1) Q^2(b1)
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 basis
#3 Define
 a0 HO n_high=8,omeg=1.4
 b0 HO n_high=8,omeg=0.7
 a1 HO n_high=8,omeg=1.4
 b1 HO n_high=8,omeg=0.7
#3 IoLevel
   9

#2 Vscf
#3 IoLevel
11
#3 Occup
 0 0 0 0 

#2 Vscf
#3 IoLevel
11
#3 Occup
 1 3 1 3

#2 Vcc
#3 Name
   1vci
#3 ItEqMaxIt
50
#3 IoLevel
11
#3 Occup
 1 3 1 3
#3 LimitModalBasis
 2 4 2 4
#3 ExpHvci
 1
#3 MaxExci 
 1
#3 TestTransformer
#3 Vci 
 9
!#3 RepVciAsVcc
#3 ITEQRESIDTHR
 1.0e-14

#2 Vcc
#3 Name
   2vci
#3 ItEqMaxIt
50
#3 IoLevel
11
#3 Occup
 0 0 0 0 
#3 LimitModalBasis
 3 3 3 3 
#3 ExpHvci
 1
#3 MaxExci 
 1
#3 TestTransformer
#3 Vci 
 4
!#3 RepVciAsVcc
#3 ITEQRESIDTHR
 1.0e-14

#2 Vcc
#3 Name
   3vci
#3 ItEqMaxIt
50
#3 LimitModalBasis
 3 3 3 3
#3 IoLevel
11
#3 Basis
basis
#3 Occup
 0 0 0 0 
#3 ExpHvci
 2
#3 TestTransformer
#3 MaxExci 
 2
#3 Vci 
 4
!#3 RepVciAsVcc
#3 ITEQRESIDTHR
 1.0e-14

#2 Vcc
#3 Name
   4vci
#3 ItEqMaxIt
50
#3 LimitModalBasis
 3 3 3 3
#3 IoLevel
11
#3 Basis
basis
#3 Occup
 0 0 0 0 
#3 ExpHvci
 4
#3 TestTransformer
#3 Iso
 2
#3 Vci 
 4
#3 VecStorage
 3 
!#3 RepVciAsVcc
#3 ITEQRESIDTHR
 1.0e-14

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=12
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# FVCI energy:
CRIT01=`$GREP "6.94539193721[12]....E-01" $log | grep "ExpHvci" | wc -l`
CRIT02=`$GREP "1.3410196703035...E\+00" $log | grep "ExpHvci" | wc -l`
TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
`
CTRL[2]=4
ERROR[2]="ExpVCI ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02

CRIT05=`$GREP "ItEqRoot_" $log | grep "6.94539193721[12]" | wc -l`
#ItEqRoot_0     1.0485009222934525E+00 8.6145512249276225E-10
#ItEqRoot_1     1.7430401160146431E+00 6.8388961558403966E-17


TEST[3]=`expr \
$CRIT05 \
`
CTRL[3]=2
ERROR[3]="VCI ENERGIES NOT CORRECT"
echo $CRIT05



PASSED=1
for i in 1 2 3 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 4dvci.check
#######################################################################
