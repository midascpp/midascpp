#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"

TESTNAME="tdmvcc_full_active_basis_2m_water"

#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water 2M-coupled
   Wave Function:    TDMVCC
   Test Purpose:     Ensure that the full active basis approach gives the same result as "normal" tdmvcc
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%
#0 MidasInput
#1 General

#1 Vib
#2 IoLevel
   5
#2 Operator
   #3 Name
      h0_init
   #3 OperFile
      ${TESTNAME}_init.mop
   #3 SetInfo
      type=energy
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ${TESTNAME}.mop
   #3 SetInfo
      type=energy
#2 Operator
   #3 Name
      q0
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q0)
#2 Operator
   #3 Name
      q1
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q1)
#2 Operator
   #3 Name
      q2
   #3 OperInput
   #4 OperatorTerms
      1.0 Q^1(Q2)
#2 Basis
   #3 Name
      basis
   #3 BSplineBasis
      10
   #3 NPrimBasisFunctions
      30
   #3 TurningPoint
      10
#2 Vscf
   #3 Name
      vscf_init
   #3 Basis
      basis
   #3 Oper
      h0_init
   #3 Threshold
      1.0e-15
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      basis
   #3 Oper
      h0
   #3 Threshold
      1.0e-15

#2 Tdvcc
   #3 IoLevel
      3
   #3 Name
      tdmvcc2
   #3 CorrMethod
      tdmvcc
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 Constraint
      variational
   #3 InvDensRegularization
      XSVD 1.e-8
   #3 GOptRegularization
      XSVD 1.e-12
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [3*10]
   #3 TdModalBasis
      [3*10]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         6
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e3
      #4 OutputPoints
         1001
      #4 Tolerance
         1.e-14 1.e-14
      #4 MaxSteps
         -1
      #4 InitialStepSize
         1.e-1
   #3 Properties
      energy
      phase
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      q2

#2 Tdvcc
   #3 IoLevel
      3
   #3 Name
      tdmvcc2_full_active_basis
   #3 CorrMethod
      tdmvcc
   #3 MaxExci
      2
   #3 Transformer
      vcc2h2
   #3 Constraint
      variational
   #3 InvDensRegularization
      XSVD 1.e-8
   #3 GOptRegularization
      XSVD 1.e-12
   #3 ModalBasisFromVscf
      vscf_init
   #3 LimitModalBasis
      [3*10]
   #3 TdModalBasis
      [3*10]
   #3 InitState
      vscfref
   #3 Oper
      h0
   #3 Basis
      basis
   #3 Integrator
      #4 IoLevel
         6
      #4 Type
         MIDAS DOPR853
      #4 TimeInterval
         0. 1.e3
      #4 OutputPoints
         1001
      #4 Tolerance
         1.e-14 1.e-14
      #4 MaxSteps
         -1
      #4 InitialStepSize
         1.e-1
   #3 FullActiveBasis
   #3 Properties
      energy
      phase
   #3 SpectrumEnergyShift
      E0
   #3 ExpectationValues
      q0
      q1
      q2

#0 Midas Input End
%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASMOPINPUT
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
-1.3983481039758772E-11    Q^1(Q0)
 1.5756874643102492E-04    Q^2(Q0)
-1.3022827261011116E-10    Q^3(Q0)
 4.6707384626643034E-08    Q^4(Q0)
 4.5483926669476205E-07    Q^1(Q1)
 1.4937195226139011E-04    Q^2(Q1)
 3.0827675345790340E-06    Q^3(Q1)
 4.3788077164208516E-08    Q^4(Q1)
 3.7033530020380567E-08    Q^1(Q2)
 2.7465351735145305E-05    Q^2(Q2)
 1.4056156771857786E-07    Q^3(Q2)
-5.4826898576720851E-10    Q^4(Q2)
 2.2424728740588762E-10    Q^1(Q0)     Q^1(Q1)
 1.3244516594568267E-10    Q^1(Q0)     Q^2(Q1)
 1.0004441719502211E-11    Q^1(Q0)     Q^3(Q1)
 9.5331590728164883E-06    Q^2(Q0)     Q^1(Q1)
 2.7097757993033156E-07    Q^2(Q0)     Q^2(Q1)
-4.0927261579781771E-12    Q^3(Q0)     Q^1(Q1)
 1.6018475434975699E-10    Q^1(Q0)     Q^1(Q2)
 1.0061285138363019E-11    Q^1(Q0)     Q^2(Q2)
-1.2647660696529783E-12    Q^1(Q0)     Q^3(Q2)
-9.1266440449544461E-07    Q^2(Q0)     Q^1(Q2)
-5.4972815632936545E-08    Q^2(Q0)     Q^2(Q2)
-7.3896444519050419E-13    Q^3(Q0)     Q^1(Q2)
-2.8100544113840442E-10    Q^1(Q1)     Q^1(Q2)
-7.2744757062537246E-07    Q^1(Q1)     Q^2(Q2)
 9.8503392109705601E-09    Q^1(Q1)     Q^3(Q2)
-2.2016263301338768E-07    Q^2(Q1)     Q^1(Q2)
-4.5356273403740488E-08    Q^2(Q1)     Q^2(Q2)
-9.0807930064329412E-09    Q^3(Q1)     Q^1(Q2)
#0MIDASMOPINPUTEND
%EOF%

cat > ${TESTNAME}_init.mop << %EOF%
#0MIDASMOPINPUT
#1MODENAMES
Q0 Q1 Q2
#1SCALEFACTORS
1.0 1.0 1.0
#1OPERATORTERMS
 1.5756874643102492E-04    Q^2(Q0)
 1.4937195226139011E-04    Q^2(Q1)
 2.7465351735145305E-05    Q^2(Q2)
#0MIDASMOPINPUTEND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Utilities
# ------------------------------------------------------------------------------
# For asserting deviations from zero.
#   $1: actual deviation
#   $2: threshold
#   return: 1 if <= threshold, 0 otherwise.
accept_dev()
{
   awk '\'' BEGIN{if('\''$1'\'' <= '\''$2'\''){printf("1")}else{printf("0")}} '\''
}
# For asserting (absolute) deviations.
#   $1: value
#   $2: expected
#   $3: (abs) threshold
#   return: 1 if dev <= threshold, 0 otherwise.
accept_dev_2()
{
      awk '\''
      function abs(a){return a>0? a: -a}
      function dev(a,b){return abs(a-b) / abs(b)}
      function accept(a,b,thr){return dev(a,b) <= abs(thr)? 1: 0}
      BEGIN{printf("%d",accept('\''$1'\'','\''$2'\'','\''$3'\''))}
   '\''
}

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Number and order of propagations
# ------------------------------------------------------------------------------
calcs=($($GREP "TDVCC propagation done for" $log | awk -F\'\'' '\''{print $2}'\''))
calcs_ctrl=(tdmvcc2 tdmvcc2_full_active_basis)
TEST+=(${#calcs[*]})
CTRL+=(${#calcs_ctrl[*]})
ERROR+=("Unexpected number of TDVCC propagations.")

for i in $(seq 0 $(expr ${#calcs_ctrl[*]} - 1))
do
   if test "${calcs[$i]}" = "${calcs_ctrl[$i]}"
   then
      TEST+=(1)
   else
      TEST+=(0)
   fi
   CTRL+=(1)
   ERROR+=("Unexpected propagation name, i = $i; got '\''${calcs[$i]}'\'', exp. '\''${calcs_ctrl[$i]}'\''.")
done

# ------------------------------------------------------------------------------
# Check phase, energy and <q> expectation values ends up at the same point.
# ------------------------------------------------------------------------------
properties=(PHASE ENERGY q0 q1 q2)
properties_tol=(1.e-14 1.e-15 1.e-10 1.e-13 1.e-11)
for i in $(seq 0 $(expr ${#properties[*]} - 1))
do
   prop_end_re=($($GREP "${properties[$i]}, at t_end *=" $log | awk '\''{print $5}'\''))
   prop_end_tol="${properties_tol[$i]}"
   TEST+=($(accept_dev_2 ${prop_end_re[0]} ${prop_end_re[1]} $prop_end_tol))
   CTRL+=(1)
   ERROR+=("${properties[$i]} at the end of tdmvcc2 and tdmvcc2_full_active_basis does not agree. ${properties[$i]}(tdmvcc2) = ${prop_end_re[0]}, ${properties[$i]}(tdmvcc2_full_active_basis) = ${prop_end_re[1]}")
done

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi


' > ${TESTNAME}.check
#######################################################################
