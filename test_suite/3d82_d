#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         3-dim ho. test, test of duplicate functionality
   Wave Function:    VSCF 
   Test Purpose:     Check VSCF energy 
   Reference:        Christoffel and Bowman CPL 85, 220 (1982),
                     in a single/few places there are up to 2 diff. on last digit (4 decimal)
                     Differ is of order 1 on last digit (4 decimal) compared to 
                     Norris et al JCP 105, 11261 (1996).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 3d82_d.minp <<%EOF%
#0 MIDAS Input          

#1 general
#2 IoLevel
    2
#2 Time
#2 BufSIZE
      10000
#2 FileSize
      32767
!#2 Debug

#1 Vib
#2 IoLevel
  5

#2 Operator    // This is the operator input
#3 IoLevel
14
#3 Name
 bowman82
!#3 IgnqiOnly
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(a_1)
-0.0 DDQ^3(a_1)
-0.0 DDQ^4(a_1)
-0.5 DDQ^2(a_2)
0.245 Q^2(a)
0.845 Q^2(a_1)
0.5 Q^2(a_2)
-0.01 Q^3(a)
-0.01 Q^3(a_1)
-0.1 Q^1(a) Q^2(a_1)
-0.1 Q^1(a_1) Q^2(a_2)
#3 KineticEnergy
 USER_DEFINED

#2 Basis    // This is the basis input
#3 Name
 smallbasis
#3 Define
 a HO n_high=32
#3 Duplicate
   3 
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11
#3 Oper
bowman82
#3 Basis
smallbasis
#3 Occup
 0 
#3 OCCUmax
 2 
#3 Duplicate
 3
#3 OccMaxSumN
 2
#3 OccMaxExPrMode
 2 


#0 Midas Input End 

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=32" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 `
CTRL[1]=14
#echo $CRIT1
#echo $CRIT2
#echo $CRIT3
#echo $CRIT4
#echo $CRIT5
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy: For the almost exact results, check both the 0000 and the 9999 possibility.
CRIT01=`$GREP "E =  1.495032640627[6,7]" $log | $GREP " Occ: \[0,0,0\]" | wc -l`
CRIT02=`$GREP "E =  2.188365729366[7,8]" $log | $GREP " Occ: \[1,0,0\]" | wc -l`
CRIT03=`$GREP "E =  2.4882497042189" $log | $GREP " Occ: \[0,0,1\]" | wc -l`
CRIT04=`$GREP "E =  2.7780366884895" $log | $GREP " Occ: \[0,1,0\]" | wc -l`
CRIT05=`$GREP "E =  2.87838520885[4,5][0,9]" $log | $GREP " Occ: \[2,0,0\]" | wc -l`
CRIT06=`$GREP "E =  3.1815029223459" $log | $GREP " Occ: \[1,0,1\]" | wc -l`

echo $CRIT01 \
$CRIT02 \
$CRIT03 \
$CRIT04 \
$CRIT05 \
$CRIT06 

TEST[2]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
\+ $CRIT04 \
\+ $CRIT05 \
\+ $CRIT06 \
`
#echo $CRIT01
#echo $CRIT02
#echo $CRIT03
#echo $CRIT04
#echo $CRIT05
#echo $CRIT06
CTRL[2]=6
ERROR[2]="VSCF ENERGIES NOT CORRECT"

PASSED=1
for i in 1 2 
do 
   if [ "${TEST[i]}" != "${CTRL[i]}" ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 3d82_d.check
#######################################################################
