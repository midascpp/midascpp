#ifndef MIDAS_ALGO_KABSCH_HPP_INCLUDED
#define MIDAS_ALGO_KABSCH_HPP_INCLUDED

#include "mmv/MidasMatrix.h"

namespace midas
{
namespace algo
{

//! Find centroid of a set of points
MidasVector FindCentroid
   (  const MidasMatrix& aP
   );

//! Find and return the optimal rotation to rotate a set of P points onto a set of Q points
MidasMatrix Kabsch
   (  MidasMatrix& aP
   ,  MidasMatrix& aQ
   );

} /* namespace algo */
} /* namespace midas */

#endif /* MIDAS_ALGO_KABSCH_HPP_INCLUDED */
