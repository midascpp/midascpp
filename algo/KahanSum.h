#pragma once
#ifndef MIDAS_ALGO_KAHANSUM_H_INCLUDED
#define MIDAS_ALGO_KAHANSUM_H_INCLUDED

namespace midas
{
namespace algo
{

void KahanSum
   (  double& sum
   ,  double  element
   ,  double& accumulation
   )
{
   auto y       = element - accumulation;
   auto t       = sum + y;
   accumulation = (t - sum) - y;
   sum = t;
}

void NeumaierSum
   (  double& sum
   ,  double  element
   ,  double& accumulation
   )
{
   auto t = sum + element;
   if (std::fabs(sum) >= std::fabs(element))
   {
      accumulation += (sum - t) + element; // If sum is bigger, low-order digits of input[i] are lost.
   }
   else
   {
      accumulation += (element - t) + sum; // Else low-order digits of sum are lost.
   }
   
   sum = t;
}

} /* namespace algo  */
} /* namespace midas */

#endif /* MIDAS_ALGO_KAHANSUM_H_INCLUDED */
