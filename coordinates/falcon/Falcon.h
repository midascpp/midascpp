/*
************************************************************************
*  
* @file                Falcon.h
*
* Created:             02-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the modification of vibrational coordinates
*
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FALCON_H
#define FALCON_H

#include "system/System.h"
#include "input/FalconCalcDef.h"

using namespace midas;

using FGInfoIter = std::map<GlobalFGNr,std::set<GlobalSubSysNr>>::const_iterator ;
using FGIter = std::set<GlobalFGNr>::const_iterator ;
using ConnIter = std::set<std::set<GlobalFGNr>>::const_iterator ;
using CoupIter = std::multimap<Nb, std::set<GlobalFGNr>>::const_iterator ;
using RelaxIter= std::set<std::set<GlobalSubSysNr>>::const_iterator;

void FalconDrv(input::FalconCalcDef*, System*);

class Falcon
{
   private:
      System*                                         mpSystem;
      input::FalconCalcDef*                           mpCalcDef;
      std::map<GlobalFGNr, set<GlobalSubSysNr>>       mCurrentFGInfo;
      set<set<GlobalFGNr>>                            mCurrentConnection; 
      set<GlobalFGNr>                                 mActiveFusionGroups;
      set<set<GlobalSubSysNr>>                        mToBeRelaxed;
      set<set<GlobalModeNr>>                          mConnModesToBeRelaxed;
      set<set<GlobalSubSysNr>>                        mToBeKept;
      std::function<Nb(const std::set<GlobalFGNr>&)>  mCoupEstFunction;
      bool                                            mCouplingToActive;
      
      //get function
      std::function<Nb(const std::set<GlobalFGNr>&)> EstCoupFunction(const input::CoupEstType& arCoupEstType);

      //info 
      set<GlobalFGNr> GetFGSet() const; 
      vector<set<GlobalSubSysNr>> GetFGSubSysVec() const;
      Nb CalcMaxPairCoupEst(const set<GlobalFGNr>& arFGSet, 
            const std::function<Nb(const GlobalSubSysNr&,const GlobalSubSysNr&)>& arFunc);
      Nb MaxCoupEst(const set<GlobalFGNr>& arFGSet) const;
      std::multimap<Nb, set<GlobalFGNr>> CouplingMap() const;
      bool AreConnectedFG(const GlobalFGNr&, const GlobalFGNr&) const ;
      set<GlobalFGNr> ConnectedFG(const GlobalFGNr&) const;
      bool IsToBeRelaxed(const set<GlobalFGNr>& arFGSet) const
             {return IsToBeIncluded(arFGSet,mpCalcDef->GetMaxSubSysForRelax(),mpCalcDef->GetMinCouplingForRelax());}
      bool IsToBeIncluded(const set<GlobalFGNr>& arFGSet, const In& arMaxSubSys, const Nb& arMinCoupling ) const;
      CalcModeType CalcModeTypeForAdd(const set<GlobalFGNr>& arFGSet) const;
      set<set<GlobalFGNr>> FusionGroupSet();
     
      //do stuff
      GlobalFGNr DoFusion();
      void Update(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr);
      void UpdateActiveFusionGroups(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr);
      void UpdateConnectivity(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr);
      void EraseFromToBeRelaxed(const GlobalFGNr& arFGNo);
      void EraseFromConnection(const GlobalFGNr& arFGNo);
      void DoInitialStep();
      void DoIterations();
      void Relax();
      void OptimizeInterConn();
      void RelaxInterConn();
      void AddSigmas(const std::set<GlobalModeNr>& arVibSet, const input::HessType& arHessType);

      void CalcGlobalSigmas(const std::vector<GlobalModeNr>& arVibVec);
      void OptimizeModes(const std::set<GlobalModeNr>& arVibSet,const input::ModeOptType& arModeOptType);
      void AddModesForFG(const GlobalFGNr& arFGNo,const CalcModeType& arCalcModeType);

      // handle degeneracies
      void AddDegConnFG(set<GlobalFGNr>& arFGSet) const;
      void RecurseAddDegConnFG(set<GlobalFGNr>& arFGSet, const GlobalFGNr& arFGNr, const Nb& Value) const;
      void RecurseAddDegConnFGWithAct(set<GlobalFGNr>& arFGSet, const GlobalFGNr& arFGNr, const Nb& arValue,std::set<GlobalFGNr>& arActFG) const;

   public:
      Falcon();
      Falcon(input::FalconCalcDef*,System* apSystem);
      void DoFalcon(); 
      void DoPrepRun();
      void PrintSettings() const;
      bool Converged();
      void DoSeparationStep(const In& arStart=I_0);
      void PrintFGInfo() const; 
      void PrintCurrentConn() const; 
      void RelaxAllRel() {AddSigmas(mpSystem->GetGlobalRelSet(), mpCalcDef->GetHessType()); 
                       OptimizeModes(mpSystem->GetGlobalRelSet(),mpCalcDef->GetModeOptType());}
      void RelaxAllRot() {AddSigmas(mpSystem->GetGlobalRotSet(), mpCalcDef->GetHessType()); 
                       OptimizeModes(mpSystem->GetGlobalRotSet(),mpCalcDef->GetModeOptType());}
      void DoFalconForAux();
};
#endif //FALCON_H
