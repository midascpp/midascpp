/**
************************************************************************
*
* @file                FalconDrv.cc
*
* Created:             05-02-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Driver for coordinate generation
*
* Last modified:       19-05-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "util/FileSystem.h"
#include "coordinates/falcon/Falcon.h"
#include "input/Input.h"
#include "pes/doubleincremental/GeneralCombiRange.h"
#include "pes/molecule/MoleculeInfo.h"

using FragCombiRange=GeneralCombiRange<GlobalSubSysNr>;
//forward declaration
bool IncludeFc(const System* const apSystem, const std::set<GlobalSubSysNr>& arCombiSet, const Nb& arMaxIncrDist, const Nb& arIncrBondDist);

using namespace midas;

/**
* Run coordinate generation
* */
void FalconDrv(input::FalconCalcDef* apCalcDef, System* apSystem)
{

   Mout << endl << endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin Iterative Merging Subsystems ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << endl << endl;

   Falcon iter_merge_sys(apCalcDef,apSystem);
   iter_merge_sys.PrintSettings();
   if (apCalcDef->GetPrepRun())
   {
     iter_merge_sys.DoPrepRun();
   }
   else
   {
      iter_merge_sys.DoFalcon();
   }


   if(apCalcDef->GetMaxIncrLevel()>I_0)
   {
      Mout << " Removing all remaining translations and rotations from the system " << endl;
      apSystem->RemoveModes(apSystem->GetGlobalTransSet());
      apSystem->RemoveModes(apSystem->GetGlobalRotSet());
      std::string fc_save_dir = gMainDir + "/FC_savedir";
      filesystem::Mkdir(fc_save_dir);

      std::string file_name = fc_save_dir + "/incr_input";
      ofstream incr_file(file_name.c_str());
      incr_file.setf(ios::scientific);
      incr_file.setf(ios::uppercase);
      incr_file.precision(I_22);

      // Wrtie comment line for file
      incr_file << "# FC Label  FC Subsystems  Interconnecting Modes  Auxiliary Modes" << std::endl;

      Mout << " Writing input data for incremental calculations with fragment mode combination " << endl;
      Mout << " Settings: " << endl;
      Mout << " Maximal FC level : " << apCalcDef->GetMaxIncrLevel() <<  endl;
      Mout << " MaxDist : " << apCalcDef->GetMaxIncrDist() <<  endl;
      Mout << " Only bonded within : " << apCalcDef->GetIncrBondDist() <<  endl;
      Mout << " FC savedir : " << fc_save_dir <<  endl;

      if (apCalcDef->GetMaxDistForConn() < apCalcDef->GetIncrBondDist())
      {
         MidasWarning("MaxDistForConn smaller than bond distance int incremental set up -- set MaxDistForConn to "+std::to_string(apCalcDef->GetIncrBondDist()));
         apCalcDef->SetMaxDistForConn(apCalcDef->GetIncrBondDist());
      }

      FragCombiRange fcr(apSystem->GetGlobalSubSysSet(),apCalcDef->GetMaxIncrLevel());
      fcr.Print();

      auto mode_map_rigid= apSystem->GetModeNameMapRigid();

      for (In i=I_0; i < fcr.NoOfCombis(); ++i  )
      {
         std::set<GlobalSubSysNr> combi_set=fcr.CombiSet(i);
         if (IncludeFc(apSystem,combi_set,apCalcDef->GetMaxIncrDist(),apCalcDef->GetIncrBondDist()))
         {
            System sub_sys=apSystem->GetNRCappedSubSystem(combi_set, apCalcDef->GetCapping(), apCalcDef->GetProj());
            if (sub_sys.NoOfModes()>I_0)
            {
               // first write the full thing
               sub_sys.Write(fc_save_dir+"/FC_"+std::to_string(i)+".mmol","MIDAS");
               sub_sys.Write(fc_save_dir+"/FC_"+std::to_string(i)+".molden","MOLDEN");
               sub_sys.Write(fc_save_dir+"/FC_"+std::to_string(i)+".minfo","SINDO");
               // and then the relative ones
               std::set<GlobalModeNr> inter_mode_no;
               for (auto it : mode_map_rigid)
               {
                  std::set<GlobalModeNr> mode_no=sub_sys.GetModeNrFromTags(it.second);
                  if (mode_no.size()== 0) continue;
                  for (const string &tag: it.second)
                  {
                     GlobalModeNr global_nr=apSystem->GetModeNrFromTag(tag);
                     std::set<GlobalSubSysNr> in_mode_set = apSystem->GetSubSysInModeSet({global_nr});
                     if (!std::includes(combi_set.begin(),combi_set.end(), in_mode_set.begin(),in_mode_set.end()))
                       inter_mode_no.emplace(sub_sys.GetModeNrFromTag(tag));
                  }
               }
               if(inter_mode_no.size()>I_0)
               {
                  molecule::MoleculeInfo mol_info(sub_sys,combi_set,inter_mode_no); // writes the uncapped version
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_INTER.mmol","MIDAS"));
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_INTER.molden","MOLDEN"));
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_INTER.minfo","SINDO"));
               }
               // and finally the auxilliary ones
               In n_aux= 9999;
               if ((inter_mode_no.size()>I_0) && (apCalcDef->GetAddRotToAux() || combi_set.size()>I_1))
               {
                  System new_falc=*apSystem;
                  new_falc.ReInitforAux(combi_set);
                  Falcon ims(apCalcDef,&new_falc);

                  if (combi_set.size()>I_1)
                  {
                     ims.DoFalconForAux();
                  }
                  new_falc.RemoveModes(new_falc.GetGlobalTransSet());
                  if (apCalcDef->GetAddRotToAux())
                  {
                     ims.RelaxAllRot();
                  }
                  else
                  {
                     new_falc.RemoveModes(new_falc.GetGlobalRotSet());
                  }
                  System sub_sys_falc;
                  sub_sys_falc=new_falc.GetNRCappedSubSystem(combi_set, apCalcDef->GetCapping(), false, apCalcDef->GetAddRotToAux());


                  // first  the uncapped orthonormal version
                  molecule::MoleculeInfo mol_info(sub_sys_falc,combi_set,sub_sys_falc.GetGlobalNameSet("AUX"));
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_REL.mmol","MIDAS"));
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_REL.molden","MOLDEN"));
                  mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(fc_save_dir+"/FC_"+std::to_string(i)+"_REL.minfo","SINDO"));

                  n_aux=mol_info.GetNoOfVibs();

                  sub_sys_falc.Write(fc_save_dir+"/FC_"+std::to_string(i)+"_AUX_CAPPED.mmol","MIDAS");
                  sub_sys_falc.Write(fc_save_dir+"/FC_"+std::to_string(i)+"_AUX_CAPPED.molden","MOLDEN");
                  sub_sys_falc.Write(fc_save_dir+"/FC_"+std::to_string(i)+"_AUX_CAPPED.minfo","SINDO");


               }
               incr_file << "FC_" << std::to_string(i)<< " " << fcr.Combi(i) << "     " << inter_mode_no.size() <<  "    "  << n_aux << endl;

               Mout << "FC_" << std::to_string(i)<< " " << fcr.Combi(i) << "     " << inter_mode_no.size() <<  "    "  << n_aux << endl;
            }
            else
            {
               Mout << " Fragment combination no " << i <<  " ("  << fcr.Combi(i) << ") contains no non-rigid modes and is therefore removed" << endl;
            }
        }
      }
      MidasWarning("Incremental setup does not check for overlaps of fragment combinations!!");
      incr_file.close();
   }
}


bool IncludeFc(const System* const apSystem, const std::set<GlobalSubSysNr>& arCombiSet, const Nb& arMaxIncrDist, const Nb& arIncrBondDist)
{
   bool include=true;
   if (apSystem->NoOfAtomsInSubs(arCombiSet) < I_2)
      return false;
   else if ((arCombiSet.size()==I_1))
      return true;
   else
   {
      std::set<std::set<GlobalSubSysNr>> connections;
      std::set<GlobalSubSysNr>::const_iterator it1=arCombiSet.begin();
      while (it1!=arCombiSet.end() && include)
      {
         Nb min_bond_dist=C_NB_MAX;
         std::set<GlobalSubSysNr>::const_iterator it2=it1;
         ++it2;
         while (it2!=arCombiSet.end() && include)
         {
            Nb min_dist=apSystem->MinNucDist(*it1,*it2);
            if (min_dist > arMaxIncrDist)
            {
               include=false;
            }
            if (min_dist < arIncrBondDist)
            {
               //cout << "found connection " << *it1 << " " << *it2 << endl;
               //cout << "min_dist" << min_dist << endl;
               std::set<GlobalSubSysNr> con_set;
               con_set.emplace(*it1);
               con_set.emplace(*it2);
               connections.emplace(con_set);
            }
            ++it2;
         }
         ++it1;
      }
      if (include && connections.size()==I_0)
         include=false;
      if (include && connections.size()>I_0)
      {
         auto it=arCombiSet.begin();
         while (it!=arCombiSet.end() && include)
         {
            std::set<GlobalSubSysNr> combined;
            auto it_con=connections.begin();
            while (it_con!=connections.end())
            {
               if (it_con->find(*it)!=it_con->end())
               {
                  combined.insert(it_con->begin(),it_con->end());
                  connections.erase(it_con++);
               }
               else
                  ++it_con;
            }
            if (combined.size() > I_0)
               connections.emplace(combined);
            else
              include=false;
            ++it;
         }
         if (connections.size()!=I_1)
            include=false;
      }
   }
   return include;
}
