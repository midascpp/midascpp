/*
************************************************************************
*
* @file                Falcon.cc
*
* Created:             02-02-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles the modification of vibrational coordinates
*
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "coordinates/falcon/Falcon.h"
#include "coordinates/freqana/FreqAna.h"
#include "coordinates/rotcoord/RotCoordBase.h"
#include "coordinates/rotcoord/RotCoordDrv.h"

using namespace midas;

/* **********************************************************************
*  Construction
********************************************************************** */
Falcon::Falcon()
   : mCouplingToActive(false)
{
}

/* **********************************************************************
********************************************************************** */

Falcon::Falcon(input::FalconCalcDef* apCalcDef,System* apSystem)
   : mpSystem(apSystem)
   , mpCalcDef(apCalcDef)
   , mCouplingToActive(false)
{
   std::set<std::set<GlobalSubSysNr>> subsys_set_ori = mpSystem->SubSysGroups();
   In fg=I_0;
   for_each(subsys_set_ori.begin(),subsys_set_ori.end(),[this,&fg](std::set<GlobalSubSysNr> set)
           {mCurrentFGInfo.emplace(fg++,set);});


   // set up initialialize
   FGInfoIter it_fg=mCurrentFGInfo.begin();
   mCoupEstFunction= EstCoupFunction(input::CoupEstType::DIST);
   PrintFGInfo();
   while (it_fg!=mCurrentFGInfo.end())
   {
      if (any_of(it_fg->second.begin(),it_fg->second.end(),[this](GlobalSubSysNr no){return mpSystem->ContainsFrozenNuc(no);}))
      {
         if (any_of(it_fg->second.begin(),it_fg->second.end(),[this](GlobalSubSysNr no){return mpSystem->ContainsActiveNuc(no);}))
         {
            MIDASERROR(" Groups contains frozen and active atoms, ie cannot be assigned");
         }
         else
         {
            mCurrentFGInfo.erase(it_fg++);
         }
      }
      else
      {
         if (any_of(it_fg->second.begin(),it_fg->second.end(),[this](GlobalSubSysNr no){return mpSystem->ContainsActiveNuc(no);}))
         {
            //Mout << " Set active " << it_fg->first << endl;
            mActiveFusionGroups.emplace(it_fg->first);
         }
         for (FGInfoIter it_fg2=mCurrentFGInfo.begin() ;it_fg2!=it_fg; ++it_fg2)
         {
            if ( (C_1/MaxCoupEst({it_fg->first,it_fg2->first})) <  mpCalcDef->GetMaxDistForConn())
            {
               std::set<GlobalFGNr> set={it_fg->first,it_fg2->first};
               mCurrentConnection.emplace(set);
            }
         }
         it_fg++;
      }
   }
   mCoupEstFunction= EstCoupFunction(apCalcDef->GetCoupEstType());
}

/* **********************************************************************
********************************************************************** */
std::function<Nb(const set<GlobalFGNr>& arFGSet)> Falcon::EstCoupFunction(const input::CoupEstType& arCoupEstType)
{
   // set function
   std::function<Nb(const std::set<GlobalFGNr>&)> coup_est;
   switch(arCoupEstType)
   {
      case(input::CoupEstType::DIST):
      {
         mCouplingToActive=false;
         //Mout << " used dist" << endl;
         coup_est = [this](const set<GlobalFGNr>& set)
                {std::function<Nb(const GlobalSubSysNr&, const GlobalSubSysNr&)> sub_pair_coup;
                 sub_pair_coup=[this](const GlobalSubSysNr& no1, const GlobalSubSysNr& no2)
                        {return C_1/mpSystem->MinNucDist(no1,no2);};
                 return CalcMaxPairCoupEst(set,sub_pair_coup);};
         break;
      }
      case(input::CoupEstType::DISTACT):
      {
         mCouplingToActive=true;
         //Mout << " used distact" << endl;
         coup_est = [this](const set<GlobalFGNr>& set)
                {std::function<Nb(const GlobalSubSysNr&, const GlobalSubSysNr&)> sub_pair_coup;
                 sub_pair_coup=[this](const GlobalSubSysNr& no1, const GlobalSubSysNr& no2)
                       {return C_1/mpSystem->MinNucDistAct(no1,no2);};
                 return CalcMaxPairCoupEst(set,sub_pair_coup);};
         break;
      }
      case(input::CoupEstType::NUCCOUPHESSNORM):
      {
         mCouplingToActive=false;
         //Mout << " used norm of the nuclear coupling in the hessian" << endl;
         coup_est = [this](const set<GlobalFGNr>& set)
                {std::function<Nb(const GlobalSubSysNr&, const GlobalSubSysNr&)> sub_pair_coup;
                 sub_pair_coup=[this](const GlobalSubSysNr& no1, const GlobalSubSysNr& no2)
                       {return mpSystem->MaxNucCoupNorm(no1,no2);};
                 return CalcMaxPairCoupEst(set,sub_pair_coup);};
         break;
      }
      case(input::CoupEstType::NUCCOUPHESSNORMACT):
      {
         mCouplingToActive=true;
         //Mout << " used norm of the nuclear coupling in the hessian" << endl;
         coup_est = [this](const set<GlobalFGNr>& set)
                {std::function<Nb(const GlobalSubSysNr&, const GlobalSubSysNr&)> sub_pair_coup;
                 sub_pair_coup=[this](const GlobalSubSysNr& no1, const GlobalSubSysNr& no2)
                       {return mpSystem->MaxNucCoupNormAct(no1,no2);};
                 return CalcMaxPairCoupEst(set,sub_pair_coup);};
         break;
      }
      case(input::CoupEstType::ERROR):
      default:
      {
         MIDASERROR("CoupEstType not known");
      }
   }
   return coup_est;
}
/* **********************************************************************
*  print info
********************************************************************** */
void Falcon::PrintSettings() const
{
   Out72Char(Mout,'-','-','-');
   OneArgOut72(Mout,"  Settings for FALCON: ",' ');
   Out72Char(Mout,'-','-','-');
   Mout << "Optimization type:                     " << mpCalcDef->GetModeOptType() << endl;
   Mout << "Hessian type:                          " << mpCalcDef->GetHessType() << endl;
   Mout << "Coupling estimate:                     " << mpCalcDef->GetCoupEstType() << endl;
   Mout << "Connectivity threshold                 " << mpCalcDef->GetMaxDistForConn() << " bohr " <<  endl;
   Mout << "Degeneracy threshold                   " << mpCalcDef->GetDegThresh() <<   endl;
   Mout << "Preparation run only                   " ;
   if (mpCalcDef->GetPrepRun())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Slow fusion                            " ;
   if (mpCalcDef->GetSlowFusion())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Fusion inactive                        " ;
   if (mpCalcDef->GetFusionInactive())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Restart                                " ;
   if (mpCalcDef->GetRestart())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Calculating all sigmas                 " ;
   if (mpCalcDef->GetCalcAllSigmas())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Relax all calculated modes             " ;
   if (mpCalcDef->GetRelaxAll())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Mout << "Separate out the translations and      " << endl;
   Mout << "translation the whole active system    " ;
   if (mpCalcDef->GetRotateAll())
   {
      Mout << "on " << endl;
   }
   else
   {
      Mout << "off " << endl;
   }
   Out72Char(Mout,'-','-','-');
   Mout << "Convergence settings" << std::endl;
   Mout << "   Fusion " << std::endl;
   Mout << "      Maximal number of subsys        " << mpCalcDef->GetMaxSubSysForFusion() << endl;
   Mout << "      Minimal coupling                 " << mpCalcDef->GetMinCouplingForFusion() << endl;
   Mout << "   Full addition " << std::endl;
   Mout << "      Maximal number of subsys        " << mpCalcDef->GetMaxSubSysForFullAdd() << endl;
   Mout << "      Minimal coupling                 " << mpCalcDef->GetMinCouplingForFullAdd() << endl;
   Mout << "   Relaxation " << std::endl;
   Mout << "      Maximal number of subsys        " << mpCalcDef->GetMaxSubSysForRelax() << endl;
   Mout << "      Minimal coupling                 " << mpCalcDef->GetMinCouplingForRelax() << endl;
   Out72Char(Mout,'-','-','-');
   Mout << std::endl;
   Mout << std::endl;

}
/* **********************************************************************
*  Do the modification
********************************************************************** */
bool Falcon::Converged()
{
   return (mCurrentFGInfo.size() == I_1 || mCurrentConnection.size()== I_0
          || mActiveFusionGroups.size() == I_0);
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoPrepRun()
{

   PrintFGInfo();

   In step = I_0;
   while (!Converged() )
   {
      Mout << std::endl << "Iteration " << step++ << std::endl;
      //GlobalFGNr new_start= DoFusion();
      DoFusion();
      PrintFGInfo();
      PrintCurrentConn();

   }

   mpSystem->UpdateSubSys(GetFGSubSysVec());
   mpSystem->Write(gMainDir + "/" + "Preprun.mmol","MIDAS");
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoFalcon()
{


   DoInitialStep();
   PrintFGInfo();

   DoIterations();

   Relax();
   if (mpSystem->NoOfModesWithSigma()>I_0)
   {
      mpSystem->SortModes(GetFGSubSysVec());
      mpSystem->WriteRedHess(mpSystem->ModesWithSigma(), gMainDir + "/" + "Red_hess");
      mpSystem->Write(gMainDir + "/" + "Red_hess.minfo","SINDO");
      mpSystem->Write(gMainDir + "/" + "Red_hess.molden","MOLDEN");
      mpSystem->Write(gMainDir + "/" + "Red_hess.mmol","MIDAS");
   }
   mpSystem->SortModesAfterFreq();
   mpSystem->ReSetModeTags();
   mpSystem->Write(gMainDir + "/" + "Final.minfo", "SINDO");
   mpSystem->Write(gMainDir + "/" + "Final.molden", "MOLDEN"); 
   mpSystem->Write(gMainDir + "/" + "Final.mmol", "MIDAS");
   mpSystem->Write(gMainDir + "/" + "FinalVib.mmol", "MIDAS", true);
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoFalconForAux()
{
   // we need to include all in the active fusion groups, since we do not want to add additional modes
   for (auto it=mCurrentFGInfo.begin(); it != mCurrentFGInfo.end(); ++it)
   {
     mActiveFusionGroups.emplace(it->first);
   }
   DoIterations();
   RelaxInterConn();
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoInitialStep()
{
   //separating out local translations and rotations and initial registering for relaxation
   Out72Char(Mout,'-','-','-');
   OneArgOut72(Mout," Start initial separation of vibrations from local rotations and translations ",' ');
   Out72Char(Mout,'-','-','-');

   for (FGIter it = mActiveFusionGroups.begin(); it != mActiveFusionGroups.end(); ++it)
   {
      if (!mpCalcDef->GetRestart())
      {
         set<GlobalModeNr> vib_set=mpSystem->SeparateOutTRforSubSys(mCurrentFGInfo.at(*it));
      }
      else
      {
         mpSystem->IdentifyTRforSubSys(mCurrentFGInfo.at(*it));
      }

      if (IsToBeRelaxed(set<GlobalFGNr>({*it})))
         mToBeRelaxed.emplace(mCurrentFGInfo.at(*it));
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoIterations()
{
   In step = I_1;

   while (!Converged())
   {
      Mout << std::endl << "Iteration " << step << std::endl;
      GlobalFGNr new_start= DoFusion();
      PrintFGInfo();
      PrintCurrentConn();

      //separating out local translations and rotations
      DoSeparationStep(new_start);
      //mpSystem->Write("step"+ std::to_string(step) +".molden","MOLDEN");
      ++step;
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::DoSeparationStep(const In& arStart)
{
   for (FGInfoIter it=mCurrentFGInfo.begin(); it!=mCurrentFGInfo.end(); ++it)
   {
      // only modify new and active groups
      if ((it->first >= arStart) && mActiveFusionGroups.find(it->first)!=mActiveFusionGroups.end())
      {
         Mout << " Separate out translation and rotation for fusion group " << it->first << endl;
         set<GlobalModeNr> vib_set;
         vib_set=mpSystem->SeparateOutTRforSubSys(it->second,ModeGroupType::TRANSROT);
         if (mToBeRelaxed.find(it->second) == mToBeRelaxed.end())
            mConnModesToBeRelaxed.emplace(vib_set);
      }
   }
   MidasAssert((mpSystem->IsOrthonormal()), "Orthonormality got lost during iterative merge");
}
/* **********************************************************************
*  Relax
********************************************************************** */
void Falcon::Relax()
{
   In n=I_0;
   if (mpCalcDef->GetRotateAll())
   {
      MidasWarning (" Rotate all is set, i.e., ruin all the locality.., at local relaxation settings");
      std::set<GlobalSubSysNr> sub_set;
      //take mToBeRelaxed here as it contains all subsystems with modes, maybe not very intuitive..
      for_each(mToBeRelaxed.begin(),mToBeRelaxed.end(),[&sub_set](std::set<GlobalSubSysNr> nrs)
             {sub_set.insert(nrs.begin(),nrs.end());});
      set<GlobalModeNr> vib_set =mpSystem->SeparateOutTRforSubSys(sub_set,ModeGroupType::TRANSROT);
      mpSystem->Write("rotatedall.molden","MOLDEN");
      mToBeRelaxed.clear();
      mToBeRelaxed.emplace(sub_set);
      mConnModesToBeRelaxed.clear();
      mCurrentFGInfo.clear();
      mCurrentFGInfo.emplace(0,sub_set);
   }
   if (mpCalcDef->GetRelaxAll())
   {
      MidasWarning (" Relax all is set, i.e., ignore all other to be relaxed settings");
      mToBeRelaxed.clear();
      mToBeRelaxed.emplace(mpSystem->GetGlobalSubSysSet());
      mConnModesToBeRelaxed.clear();
      mCurrentFGInfo.clear();
      mCurrentFGInfo.emplace(0,mpSystem->GetGlobalSubSysSet());
   }
   // we calculate all Sigmas first and then rotate afterwards
   std::set<GlobalModeNr> all_for_sigma;
   if (mpCalcDef->GetCalcAllSigmas())
   {
      all_for_sigma = mpSystem->GetGlobalModeSet();
   }
   else
   {
      for (RelaxIter it=mToBeRelaxed.begin(); it!=mToBeRelaxed.end(); ++it)
      {
         set<GlobalModeNr> vib_set=mpSystem->ModesWithinSubs(*it,ModeGroupType::VIB);
         all_for_sigma.insert(vib_set.begin(),vib_set.end());
      }
      for (auto it=mConnModesToBeRelaxed.begin(); it!=mConnModesToBeRelaxed.end(); ++it)
      {
         all_for_sigma.insert(it->begin(),it->end());
      }
   }
   AddSigmas(all_for_sigma,mpCalcDef->GetHessType());
   //mpSystem->WriteRedHess(mpSystem->ModesWithSigma(),"full_red_hess_before_relax");
   //first intra_subsystem
   for (RelaxIter it=mToBeRelaxed.begin(); it!=mToBeRelaxed.end(); ++it)
   {
      Mout << " I relax all vibrations in the subsystems " ;
      for_each(it->begin(),it->end(),[](const GlobalSubSysNr& no) {Mout << no  << " , " ;});
      Mout << endl;
      set<GlobalModeNr> vib_set=mpSystem->ModesWithinSubs(*it,ModeGroupType::VIB);

      Mout << " With modes " ;
      for_each(vib_set.begin(),vib_set.end(),[](const GlobalModeNr& no) {Mout << no  << " , " ;});

      Mout << endl;
      if (vib_set.size()>I_0)
         OptimizeModes(vib_set,mpCalcDef->GetModeOptType());
  }
   mToBeRelaxed.clear();

   OptimizeInterConn();

   mpSystem->Write("relaxed.molden","MOLDEN");
}
/* **********************************************************************
*  optimize only inter-connecting modes (expects sigmas to be there!!)
********************************************************************** */
void Falcon::OptimizeInterConn()
{
   for (auto it=mConnModesToBeRelaxed.begin(); it!=mConnModesToBeRelaxed.end(); ++it)
   {
      Mout << " I relax connecting modes " ;
      for_each(it->begin(),it->end(),[](const GlobalModeNr& no) {Mout << no  << " , " ;});
      Mout << endl;
      if (it->size()>I_0)
         OptimizeModes(*it,mpCalcDef->GetModeOptType());
   }
   mConnModesToBeRelaxed.clear();
}
/* **********************************************************************
*  Relax only inter-connecting modes (calculates sigmas first)
********************************************************************** */
void Falcon::RelaxInterConn()
{
   std::set<GlobalModeNr> all_for_sigma;
   for (auto it=mConnModesToBeRelaxed.begin(); it!=mConnModesToBeRelaxed.end(); ++it)
   {
      all_for_sigma.insert(it->begin(),it->end());
   }
   AddSigmas(all_for_sigma,mpCalcDef->GetHessType());
   OptimizeInterConn();
}
/* **********************************************************************
*  AddSigmas
********************************************************************** */
void Falcon::AddSigmas(const set<GlobalModeNr>& arVibSet, const input::HessType& arHessType)
{
    switch(arHessType)
    {
       case(input::HessType::NO):
       {
          return;
          break;
       }
       case(input::HessType::GIVEN):
       {

          mpSystem->UpdateSigmas(arVibSet);
          break;
       }
       case(input::HessType::CALC):
       {
          std::vector<GlobalModeNr> mode_vec;
          mode_vec.reserve(arVibSet.size());
          for (auto it=arVibSet.begin(); it!=  arVibSet.end(); ++it)
          {
             if (!mpSystem->ModeHasSigma(*it))
             {
                mode_vec.emplace_back(*it);
             }
          }
          CalcGlobalSigmas(mode_vec);
          break;
       }
       case(input::HessType::ERROR):
       default:
       {
          MIDASERROR(" HessType not known");
       }
    }
}
/* **********************************************************************
********************************************************************** */
void Falcon::CalcGlobalSigmas(const std::vector<GlobalModeNr>& arVibVec)
{
   // Create setup directory
   auto source_directory = gMainDir + "/InterfaceFiles/";
   auto setup_directory = gMainDir + "/setup";
   midas::filesystem::Mkdir(setup_directory);
   auto target_directory = setup_directory + "/";

   // A small hack to access a single point calculation name through FreqAna CalcDef
   auto sp_info = SinglePointCalc(mpCalcDef->GetpFreqAnaCalcDef()->GetSinglePointName()).SpInfo();

   // Find and copy interface files into setup
   auto input_creator_script = sp_info.find("INPUTCREATORSCRIPT")->second;
   if (!input_creator_script.empty())
   {
      midas::filesystem::Copy
         (  source_directory + input_creator_script
         ,  target_directory + input_creator_script
         );


      if (!midas::filesystem::IsExecutable(target_directory + input_creator_script))
      {
         auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + input_creator_script);
         midas::filesystem::Chmod
            (  target_directory + input_creator_script
            ,  S_IXUSR | file_permissions
            );
      }
   }

   auto run_script = sp_info.find("RUNSCRIPT")->second;
   if (!run_script.empty())
   {
      midas::filesystem::Copy
         (  source_directory + run_script
         ,  target_directory + run_script
         );

      if (!midas::filesystem::IsExecutable(target_directory + run_script))
      {
         auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + run_script);
         midas::filesystem::Chmod
            (  target_directory + run_script
            ,  S_IXUSR | file_permissions
            );
      }
   }

   auto validation_script = sp_info.find("VALIDATIONSCRIPT")->second;
   if (!validation_script.empty())
   {
      midas::filesystem::Copy
         (  source_directory + validation_script
         ,  target_directory + validation_script
         );

      if (!midas::filesystem::IsExecutable(target_directory + validation_script))
      {
         auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + validation_script);
         midas::filesystem::Chmod
            (  target_directory + validation_script
            ,  S_IXUSR | file_permissions
            );
      }
   }

   auto property_info_file = sp_info.find("PROPERTYINFO")->second;
   if (!property_info_file.empty())
   {
      midas::filesystem::Copy
         (  source_directory + property_info_file
         ,  target_directory + property_info_file
         );
   }
   // All files are now in setup directory

   std::set<GlobalModeNr> vib_set;
   vib_set.insert(arVibVec.begin(),arVibVec.end());
   molecule::MoleculeInfo mol_info(*mpSystem,mpSystem->GetGlobalSubSysSet(),vib_set);
   FreqAna freq_ana(&mol_info,mpCalcDef->GetpFreqAnaCalcDef());
   Mout << " Calculating all required Hessian information " << endl;
   freq_ana.Preparations();
   MidasMatrix sigma_matrix=freq_ana.CalcSigma();
   for(In i=I_0; i< arVibVec.size(); ++i)
   {
      MidasVector sigma(I_3*mol_info.GetNumberOfNuclei());
      sigma_matrix.GetCol(sigma,i);
      for (In j=I_0; j< sigma.size();++j)
      {
         sigma.at(j)/=sqrt(mpSystem->GetNuclei(j/I_3).GetMass())*C_FAMU;
      }
      std::pair<std::vector<GlobalSubSysNr>,std::vector<MidasVector>> sigma_pair = mpSystem->DevideFullVecIntoSubs(sigma);
      mpSystem->AddSigmaToMode(arVibVec.at(i),std::move(sigma_pair.first),std::move(sigma_pair.second));
   }
}
/* **********************************************************************
*  Optimize
********************************************************************** */
void Falcon::OptimizeModes(const set<GlobalModeNr>& arVibSet, const input::ModeOptType& arModeOptType)
{
    switch(arModeOptType)
    {
       case(input::ModeOptType::NO):
       {
          return;
          break;
       }
       case(input::ModeOptType::HESSIAN):
       {
          mpSystem->DiagRedHess(arVibSet);
          break;
       }
       case(input::ModeOptType::ROTCOORD):
       {
          std::string label=std::to_string(*arVibSet.begin());

          molecule::MoleculeInfo mol_info(*mpSystem,mpSystem->GetGlobalSubSysSet(),arVibSet);
          input::RotCoordCalcDef rcd=*(mpCalcDef->GetpRotCoordCalcDef());
          if (mpSystem->HasHessian())
          {
             mpSystem->DiagRedHess(arVibSet);
             mpSystem->WriteRedHess(arVibSet,"tmp");
          }
          //gOperatorDefs.ReSet();
          RotCoordDrv(&rcd,&mol_info);
          mpSystem->ReplaceModes(arVibSet,mol_info);
          break;
       }
       case(input::ModeOptType::ERROR):
       default:
       {
          MIDASERROR(" ModeOptType not known");
       }
    }
}
/* **********************************************************************
*  Fusion the groups
********************************************************************** */
GlobalFGNr Falcon::DoFusion()
{
   Mout << " Do Fusion " << endl;

   // getting first new fusion group number
   GlobalFGNr startnew = (mCurrentFGInfo.rbegin()->first) + I_1;

   set<set<GlobalFGNr>> fusion_group_set=FusionGroupSet();
   Mout << " Set up Fusion group set" << endl;
   GlobalFGNr new_fg=startnew;
   for (set<set<GlobalFGNr>>::const_iterator it=fusion_group_set.begin(); it !=fusion_group_set.end(); ++it)
   {
      if (!IsToBeIncluded(*it,mpCalcDef->GetMaxSubSysForFusion(),mpCalcDef->GetMinCouplingForFusion()))
      {
         for (FGIter it_fg=it->begin(); it_fg!=it->end(); ++it_fg)
         {
             Mout << " Fusion group " << *it_fg << " is finished " << endl;
             Mout << " it will be removed from connections and active fusion groups " << endl;
             EraseFromConnection(*it_fg);
             mActiveFusionGroups.erase(*it_fg);
         }
         continue;
      }
      bool is_active=any_of(it->begin(),it->end(),[this](const GlobalFGNr& no)
             {return mActiveFusionGroups.find(no)!=mActiveFusionGroups.end();});
      bool is_to_be_relaxed = (is_active && IsToBeRelaxed(*it));
      CalcModeType calc_mode_type=CalcModeTypeForAdd(*it);

      set<GlobalSubSysNr> new_sub_sys;
      for (FGIter it_fg=it->begin(); it_fg!=it->end(); ++it_fg)
      {
         // merge subsystem to new
         new_sub_sys.insert(mCurrentFGInfo[*it_fg].begin(),mCurrentFGInfo[*it_fg].end());

         // if fusion group was not active, its modes have to be added to the system
         // this also cannot have been in the to be relaxed group. Hence, only
         // the others are removed
         if (is_active && mActiveFusionGroups.find(*it_fg)==mActiveFusionGroups.end()&& !mpCalcDef->GetPrepRun())
            AddModesForFG(*it_fg,calc_mode_type);
         // erase old information
         if (is_to_be_relaxed)
            EraseFromToBeRelaxed(*it_fg) ;

         auto size=mCurrentFGInfo.erase(*it_fg);
         MidasAssert((size==1)," Tried to erase non-existing fusion group");
         // update Active Fusion Groups and connectivity
         if (is_active) UpdateActiveFusionGroups(*it_fg,new_fg);
         UpdateConnectivity(*it_fg,new_fg);
      }
      // Add Fusion Groups where needed
      if (is_to_be_relaxed) mToBeRelaxed.emplace(new_sub_sys);
      mCurrentFGInfo.emplace(new_fg++,new_sub_sys);
   }
   return startnew;
}
/* **********************************************************************
********************************************************************** */
void Falcon::EraseFromConnection(const GlobalFGNr& arFGNo)
{
   Mout << " Remove fusion group " << arFGNo << " from the connection map " << endl;
   std::set<std::set<GlobalFGNr>>::iterator it=mCurrentConnection.begin();
   while(it!=mCurrentConnection.end())
   {
      if (it->find(arFGNo)!=it->end())
      {
          mCurrentConnection.erase(it++);
      }
      else
         it++;
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::EraseFromToBeRelaxed(const GlobalFGNr& arFGNo)
{
   auto size=mToBeRelaxed.erase(mCurrentFGInfo.at(arFGNo));
   if(size!=1)
   {
      MidasWarning(" Tried to erase non-existing fusion group in ToBeRelaxed No:"+std::to_string(arFGNo));
      Mout << " Trying to erase possible subsets " << endl;
      std::set<std::set<GlobalSubSysNr>>::iterator it=mToBeRelaxed.begin();
      while(it!=mToBeRelaxed.end())
      {
         if (std::includes(mCurrentFGInfo.at(arFGNo).begin(),mCurrentFGInfo.at(arFGNo).end(),
                it->begin(),it->end()))
         {
             Mout << " Found a subset to remove " << endl;
             mToBeRelaxed.erase(it++);
         }
         else
            it++;
      }
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::AddModesForFG(const GlobalFGNr& arFGNo,const CalcModeType& arCalcModeType)
{
   switch(arCalcModeType)
   {
      case(CalcModeType::CART):
      {
         for_each(mCurrentFGInfo.at(arFGNo).begin(), mCurrentFGInfo.at(arFGNo).end(),
                [this](GlobalSubSysNr no){mpSystem->AddCartCoordForSub(no);});
         set<GlobalModeNr> vib_set=mpSystem->SeparateOutTRforSubSys(mCurrentFGInfo.at(arFGNo));
         break;
      }
      case(CalcModeType::LOCALTRANSROT):
      {
         mpSystem->AddTransForSet(mCurrentFGInfo.at(arFGNo));
         mpSystem->AddRotForSet(mCurrentFGInfo.at(arFGNo));
         break;
      }
      case(CalcModeType::NO):
      {
         break;
      }
      case(CalcModeType::ERROR):
      default:
      {
         MIDASERROR("Unknown type of modes to add");
      }
   }
}
/* **********************************************************************
********************************************************************** */
set<set<GlobalFGNr>> Falcon::FusionGroupSet()
{
   set<set<GlobalFGNr>> fusion_group_set;
   // getting Coupling map
   std::multimap<Nb, set<GlobalFGNr>> coup_map=CouplingMap();

   if (coup_map.empty())
   {
      MidasWarning("Could not find any couplings - will fusion all that is left");
      set<GlobalFGNr> fg_set;
      for (FGInfoIter it = mCurrentFGInfo.begin(); it != mCurrentFGInfo.end() ; ++it)
      {
         fg_set.emplace(it->first);
      }
      fusion_group_set.emplace(std::move(fg_set));
      return fusion_group_set;
   }

   set<GlobalFGNr> merged_FG; //= set for all groups already fusioned/not to be fusioned
   std::function<bool(GlobalFGNr)> AlreadyMerged=[&merged_FG](GlobalFGNr no) {return merged_FG.find(no)!=merged_FG.end();};

   // setting fusion group
   for (std::multimap<Nb, set<GlobalFGNr>>::reverse_iterator rit=coup_map.rbegin(); rit!=coup_map.rend(); ++rit)
   {
      Mout << " Coupling estimate = " << rit->first << " for fusion groups " ;
      for_each(rit->second.begin(),rit->second.end(), [](GlobalFGNr no) {Mout << no << " " ;} );
      Mout << endl;

      if (std::none_of(rit->second.begin(),rit->second.end(),AlreadyMerged))
      {
          set<GlobalFGNr> fusion = rit->second;
          //check for degeneracies
          AddDegConnFG(fusion);
          //checking the added modes whether they are already in merged_FG
          bool include=std::none_of(fusion.begin(),fusion.end(),AlreadyMerged);
          if (include)
          {
             //Mout << " Added for fusion " << endl ;
             fusion_group_set.emplace(fusion);
          }
          if (include || mpCalcDef->GetSlowFusion())
          {
             if (!include)
             {
                Mout << " Blocked non-fusioned " << endl;
                for_each(fusion.begin(),fusion.end(), [](GlobalFGNr no) {Mout << no << " " ;} );
                Mout << endl;
             }
             merged_FG.insert(fusion.begin(),fusion.end());
          }
      }
      else if (mpCalcDef->GetSlowFusion())
      {
         Mout << " Blocked non-fusioned " << endl;
         for_each(rit->second.begin(),rit->second.end(), [](GlobalFGNr no) {Mout << no << " " ;} );
         Mout << endl;
         merged_FG.insert(rit->second.begin(),rit->second.end());
      }
   }

    return fusion_group_set;
}
/* **********************************************************************
********************************************************************** */
void Falcon::AddDegConnFG(set<GlobalFGNr>& arFGSet) const
{
   set<GlobalFGNr> initial_set=arFGSet;
   for (FGIter it=initial_set.begin(); it!=initial_set.end() ; ++it)
   {
      if (!mCouplingToActive)
      {
         RecurseAddDegConnFG(arFGSet,*it,mCoupEstFunction(initial_set));
      }
      else
      {
         set<GlobalFGNr> active_set;
         for_each(initial_set.begin(),initial_set.end(),[&active_set,this](const GlobalFGNr& no)
                {if (mActiveFusionGroups.find(no)!=mActiveFusionGroups.end()) active_set.emplace(no);});
         RecurseAddDegConnFGWithAct(arFGSet,*it,mCoupEstFunction(initial_set),active_set);
      }
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::RecurseAddDegConnFG(set<GlobalFGNr>& arFGSet, const GlobalFGNr& arFGNr, const Nb& arValue) const
{
   set<GlobalFGNr> connected=ConnectedFG(arFGNr);
   set<GlobalFGNr> pair;
   pair.emplace(arFGNr);
   for (FGIter it=connected.begin(); it!=connected.end(); ++it)
   {
      pair.emplace(*it);
      if (fabs(mCoupEstFunction(pair)-arValue) < mpCalcDef->GetDegThresh()) //taking all that are degerate
      {
         auto empl=arFGSet.emplace(*it);
         if (empl.second)
         {
            Mout << " Adding fusion group " << *it << " due to degeneracy " << endl;
            Mout << " Coupling estimate to " << arFGNr << " =  " << mCoupEstFunction(pair) << endl;
            RecurseAddDegConnFG(arFGSet,*it,arValue);
         }
      }
      pair.erase(*it);
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::RecurseAddDegConnFGWithAct(set<GlobalFGNr>& arFGSet, const GlobalFGNr& arFGNr, const Nb& arValue, set<GlobalFGNr>& arActiveSet) const
{
   set<GlobalFGNr> connected=ConnectedFG(arFGNr);
   std::vector<GlobalFGNr> pair{-I_1,-I_1};
   for (FGIter it=connected.begin(); it!=connected.end(); ++it)
   {
      pair[0]=*it;
      Nb max_coup=C_0;
      if (mActiveFusionGroups.find(*it)!=mActiveFusionGroups.end() && arFGNr!= pair.at(0))
      {
         pair[1]=arFGNr;
         max_coup=max(mCoupEstFunction(std::set<GlobalFGNr>(pair.begin(),pair.end())),max_coup);
      }
      for (FGIter it_act=arActiveSet.begin(); it_act!=arActiveSet.end(); ++it_act)
      {
         if (*it_act!= pair.at(0))
         {
            pair[1]=*it_act;
            max_coup=max(mCoupEstFunction(std::set<GlobalFGNr>(pair.begin(),pair.end())),max_coup);
         }
      }
      // include it if it is either degenerate or the coupling is indeed larger
      //Mout << " Maximal coupling in degeneracy - reference value : " << max_coup  << " - " << arValue << endl;
      if ((arValue-max_coup) < mpCalcDef->GetDegThresh())
      {
         if (mActiveFusionGroups.find(*it)!=mActiveFusionGroups.end()) arActiveSet.emplace(*it);
         auto empl=arFGSet.emplace(*it);
         if (empl.second)
         {
            Mout << " Adding fusion group " << *it << " due to degeneracy / or larger coupling" << endl;
            Mout << " Coupling estimate to " << arFGNr << " =  " << max_coup << endl;
            RecurseAddDegConnFGWithAct(arFGSet,*it,arValue,arActiveSet);
         }
      }
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::Update(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr)
{
   UpdateActiveFusionGroups(arOldFGNr,arNewFGNr);
   UpdateConnectivity(arOldFGNr,arNewFGNr);
}
/* **********************************************************************
********************************************************************** */
void Falcon::UpdateActiveFusionGroups(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr)
{
   //Mout << " Update active fusion groups " << arOldFGNr << " -> "  << arNewFGNr << endl ;
   mActiveFusionGroups.erase(arOldFGNr);
   mActiveFusionGroups.emplace(arNewFGNr);
}
/* **********************************************************************
********************************************************************** */
void Falcon::UpdateConnectivity(const GlobalFGNr& arOldFGNr, const GlobalFGNr& arNewFGNr)
{
   //Mout << " Update connectivity " << arOldFGNr << " -> " <<  arNewFGNr << endl ;
   std::set<std::set<GlobalFGNr> > new_con;
   std::set<std::set<GlobalFGNr> > old_con;
   for (ConnIter it_con=mCurrentConnection.begin(); it_con != mCurrentConnection.end(); ++it_con)
   {
      if (it_con->find(arOldFGNr)!= it_con->end())
      {
         set<GlobalFGNr> new_fg_set=*it_con;
         old_con.emplace(*it_con);
         new_fg_set.erase(arOldFGNr);
         auto empl= new_fg_set.emplace(arNewFGNr);
         if (empl.second)
         {
            new_con.emplace(new_fg_set);
         }
      }
   }
   for (ConnIter it=old_con.begin(); it !=old_con.end() ; ++it)
   {
      mCurrentConnection.erase(*it);
   }
   mCurrentConnection.insert(new_con.begin(),new_con.end());
}
/* **********************************************************************
********************************************************************** */
std::multimap<Nb, set<GlobalFGNr>> Falcon::CouplingMap() const
{
   std::multimap<Nb, set<GlobalFGNr>> coup_map;
   for (ConnIter it_con = mCurrentConnection.begin() ; it_con != mCurrentConnection.end(); ++it_con)
   {
      if (mpCalcDef->GetFusionInactive() ||
             any_of(it_con->begin(),it_con->end(),[this](GlobalFGNr no)
                    {return (mActiveFusionGroups.find(no)!= mActiveFusionGroups.end());}))
      {
         coup_map.emplace(mCoupEstFunction(*it_con),*it_con);
      }
   }
   return coup_map;
}
/* **********************************************************************
********************************************************************** */
CalcModeType Falcon::CalcModeTypeForAdd(const set<GlobalFGNr>& arFGSet) const
{
   CalcModeType calc_mode_type=CalcModeType::NO;


   if (IsToBeIncluded(arFGSet,mpCalcDef->GetMaxSubSysForFullAdd(),mpCalcDef->GetMinCouplingForFullAdd()))
   {
      calc_mode_type=CalcModeType::CART;
   }
   else
   {
      calc_mode_type=CalcModeType::LOCALTRANSROT;
   }

   return calc_mode_type;
}
/* **********************************************************************
********************************************************************** */
bool Falcon::IsToBeIncluded(const set<GlobalFGNr>& arFGSet, const In& arMaxSubSys, const Nb& arMinCoupling ) const
{
   In n_sub=I_0;
   for_each(arFGSet.begin(),arFGSet.end(),[&n_sub,this](GlobalFGNr no){n_sub+=mCurrentFGInfo.at(no).size();});

   bool to_be_relaxed=(n_sub  <= arMaxSubSys);
   //if ( to_be_relaxed) Mout << " number of subsystems satisfied " << endl;
   if (arFGSet.size()>I_1)
   {
      //Mout << " MaxCoupEst(arFGSet) " << MaxCoupEst(arFGSet) <<  endl;
      //Mout << " arMinCoupling " << arMinCoupling << endl;
      to_be_relaxed = (to_be_relaxed && (MaxCoupEst(arFGSet) > arMinCoupling));
   }
   return to_be_relaxed;
}
/* **********************************************************************
********************************************************************** */
Nb Falcon::MaxCoupEst(const set<GlobalFGNr>& arFGSet) const
{
   Nb max_coup_est=C_0;
   for (FGIter it1=arFGSet.begin();it1 != arFGSet.end(); ++it1)
      for (FGIter it2=arFGSet.begin();it2 != it1; ++it2)
      {
         max_coup_est=max(mCoupEstFunction(set<GlobalFGNr>({*it1,*it2})),max_coup_est);
      }
   return max_coup_est;
}
/* **********************************************************************
********************************************************************** */
Nb Falcon::CalcMaxPairCoupEst(const set<GlobalFGNr>& arFGSet, const std::function<Nb(const GlobalSubSysNr&,const GlobalSubSysNr&)>& arFunc)
{
   //Mout << " Entered Caclulation of Coupling Estimates" << endl;
   vector<GlobalFGNr> fg_vec(arFGSet.begin(),arFGSet.end());
   if (fg_vec.size() !=I_2)
   {
      MIDASERROR("Number of fusion groups larger that 2 in CalcMaxPairCoupEst");
   }
   Nb coup_est=C_0;
   for (SubSysIter it1= mCurrentFGInfo.at(fg_vec[I_0]).begin(); it1!=mCurrentFGInfo.at(fg_vec[I_0]).end() ; ++it1)
   {
      for (SubSysIter it2= mCurrentFGInfo.at(fg_vec[I_1]).begin(); it2!=mCurrentFGInfo.at(fg_vec[I_1]).end() ; ++it2)
      {
         Nb est = arFunc(*it1,*it2);
         //Mout << " Coupling between subsystems " << *it1 << " " << *it2 << " " << est << endl;
         MidasAssert((est>=C_0),"Calculation of coupling estimates went wrong");
         coup_est=max(coup_est,est);
      }
   }
   return coup_est;
}
/* **********************************************************************
********************************************************************** */
bool Falcon::AreConnectedFG(const GlobalFGNr& arFg1, const GlobalFGNr& arFg2) const
{
   set<GlobalFGNr> fg_set;
   fg_set.emplace(arFg1);
   fg_set.emplace(arFg2);
   bool conn=false;
   if (mCurrentConnection.find(fg_set)!=mCurrentConnection.end())
   {
      conn=true;
   }
   return conn;
}
/* **********************************************************************
********************************************************************** */
set<GlobalFGNr> Falcon::ConnectedFG(const GlobalFGNr& arFG) const
{
   set<GlobalFGNr> set;
   for (FGInfoIter it=mCurrentFGInfo.begin();it!=mCurrentFGInfo.end(); ++it)
   {
      if(AreConnectedFG(arFG,it->first))
         set.emplace(it->first);
   }
   return set;
}
/* **********************************************************************
********************************************************************** */
set<GlobalFGNr> Falcon::GetFGSet() const
{
   std::set<GlobalFGNr> set;
   for (FGInfoIter it=mCurrentFGInfo.begin();it!=mCurrentFGInfo.end(); ++it)
   {
      auto empl =set.emplace(it->first);
      MidasAssert (empl.second,"found double fusion group");
   }
   return set;
}
/* **********************************************************************
********************************************************************** */
vector<set<GlobalSubSysNr>> Falcon::GetFGSubSysVec() const
{
   vector<set<GlobalSubSysNr>> vec;
   for (FGInfoIter it=mCurrentFGInfo.begin();it!=mCurrentFGInfo.end(); ++it) vec.emplace_back(it->second);
   return vec;
}
/* **********************************************************************
* printing
********************************************************************** */
void Falcon::PrintFGInfo() const
{
   Mout << " The current fusion group situation is: " << std::endl;
   for (FGInfoIter it_fg = mCurrentFGInfo.begin();  it_fg != mCurrentFGInfo.end(); ++it_fg)
   {
      Mout << "  Fusion group " << it_fg->first << " contains subsystems ";
      for (SubSysIter it_sub = it_fg->second.begin(); it_sub != it_fg->second.end(); ++it_sub)
      {
         Mout << *it_sub;
      }
      Mout << std::endl;
   }
}
/* **********************************************************************
********************************************************************** */
void Falcon::PrintCurrentConn() const
{
   Mout << " The current connectivity is: " << endl;
   for (ConnIter it_con= mCurrentConnection.begin() ;  it_con != mCurrentConnection.end(); ++it_con)
   {
      for (FGIter it_fg = it_con->begin(); it_fg != it_con->end(); ++it_fg)
      {
         Mout << *it_fg << " , " ;
      }
      Mout << endl;
   }
}
/* **********************************************************************
********************************************************************** */
