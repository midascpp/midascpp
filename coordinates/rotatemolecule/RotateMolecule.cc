/*
************************************************************************
*  
* @file                RotateMolecule.cc
*
* Created:             2016-09-23
*
* Author:              Diana Madsen (diana@chem.au.dk)
*
* Short Description:   Rotates molecule: the reference structure and the normal coordinates 
*                      
*                      
* 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <fstream>
#include <string>
#include <sstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/ModSysCalcDef.h"
#include "system/System.h"
#include "pes/molecule/MoleculeInfo.h"
#include "inc_gen/Const.h"
#include "coordinates/rotatemolecule/RotateMolecule.h"

// using declarations
using namespace midas;

/**
 * Rotate the molecule.
 **/
void DoRotateMolecule
   (  const input::ModSysCalcDef& arCalcDef
   ,  const System& arSystem
   )
{
   Mout << "Starting DoRotateMolecule " << std::endl;
   MidasAssert((!arSystem.HasHessian()),"No rotation of Hessian is made!");

   //IMPLEMENT LATER: If Hessian is in the .mol file, write warning (since not rotated.) .mol file should be selfconsistent.
   molecule::MoleculeInfo mol_info(arSystem, arSystem.GetGlobalSubSysSet(), arSystem.GetGlobalModeSet());
   
   In n_atoms = arSystem.GetNucleiNr();
   Mout << "n_atoms are: " << n_atoms << std::endl;
   
   std::string axis = arCalcDef.GetAxis();
   Nb angle = arCalcDef.GetAngle();
   
   Mout << "The axis is: " << axis << std::endl;
   Mout << "The angle is: " << angle << std::endl;
   
   // Create 3*3 rotation matrix
   MidasMatrix rot1(3);
   rot1.Unit();

   if (axis == "x")
   {
      Mout << "Doing rotation around x-axis with the angle " << angle << std::endl;
      
      rot1[1][1] =  cos(angle);
      rot1[1][2] = -sin(angle);
      rot1[2][1] =  sin(angle);
      rot1[2][2] =  cos(angle);
   }
   else if (axis == "y")
   {
      Mout << "Doing rotation around y-axis with the angle " << angle << std::endl;
      
      rot1[0][0] =  cos(angle);
      rot1[0][2] =  sin(angle);
      rot1[2][0] = -sin(angle);
      rot1[2][2] =  cos(angle);
   }
   else if (axis == "z")
   {
      Mout << "Doing rotation around z-axis with the angle " << angle << std::endl;
      
      rot1[0][0] =  cos(angle);
      rot1[0][1] = -sin(angle);
      rot1[1][0] =  sin(angle);
      rot1[1][1] =  cos(angle);
   }
   else
   {
      MIDASERROR("Do not recognize axis '" + axis + "'.");
   }
   
   // Rotate molecule
   mol_info.Rotate(rot1);
   
   // Write new molecule to disc.
   mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo{"molecule_rot.mol","MIDAS"});
}
