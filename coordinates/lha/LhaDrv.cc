/*
************************************************************************
*  
* @file                LhaDrv.cc
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Driver for diagonalizing and transforming harmonic 
*                      potentials.
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <memory>

#include "LhaDrv.h"

#include "input/ModSysCalcDef.h"
#include "input/Input.h"
#include "libmda/numeric/float_eq.h"
#include "pes/molecule/MoleculeInfo.h"
#include "system/System.h"
#include "system/operator_utils.h"
#include "util/Io.h"
#include "lapack_interface/GESV.h"

//! Forward decl
std::pair<MidasVector, MidasMatrix> DiagonalizeAndWrite
   (  MidasVector& aGradient
   ,  MidasMatrix& aHessian
   ,  const System& aSystem
   ,  const std::string& aName
   );

/**
 * Driver for Local Harmonic Approximation
 *
 * @param aCalcDef      CalcDef
 * @param aSystem       System to modify
 **/
void LhaDrv
   (  const midas::input::ModSysCalcDef& aCalcDef
   ,  const System& aSystem
   )
{
   Mout << std::endl << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Entering Local Harmonic Approximation (LHA) module ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;

   // Get LhaCalcDef from ModSysCalcDef
   auto* lha_calcdef = aCalcDef.pLhaCalcDef();

   // Do we need to project out translation and rotation?
   In proj_out_t_r = lha_calcdef->GetProjectOutTransRot();

   // Do we want to write bounds for transformed potential? Get harmonic turning point.
   In write_bounds = lha_calcdef->GetWriteBounds();

   // Do we need to check the eigenvalues of the Hessian?
   bool check_system_hessian = lha_calcdef->GetCheckHessian();

   // Ignore gradient in mop and mbounds files
   bool ignore_gradient = lha_calcdef->GetIgnoreGradient();
   if (  ignore_gradient
      )
   {
      Mout  << " NB: Ignoring gradient information!" << std::endl;
   }

   // Do we have a reference system?
   const auto& ref_system_name = lha_calcdef->GetReferenceSystem();
   bool has_ref_system = !ref_system_name.empty();
   std::unique_ptr<System> ref_system = nullptr;
   In ref_system_idx = 0;
   if (  has_ref_system
      )
   {
      for(const auto& sysdef : gSystemDefs)
      {
         if (  sysdef.Name() == ref_system_name
            )
         {
            ref_system = std::make_unique<System>(&sysdef);
            break;
         }
         ++ref_system_idx;
      }
      MidasAssert(bool(ref_system), "Reference system '" + ref_system_name + "' not found!");
   }

   // 0) Sanity checks.
   MidasAssert(aSystem.HasHessian(), "System for LHA must contain a Hessian.");
   if (  !aSystem.HasGradient()
      && !ignore_gradient
      )
   {
      MidasWarning("No gradient given in system for LHA. We assume it is zero!");
   }
   if (  has_ref_system
      )
   {
      // Check that nuclei are at the same positions for the two systems
      auto nnuc = aSystem.GetNucleiNr();
      MidasAssert(ref_system->GetNucleiNr() == nnuc, "The two systems must have same number of nuclei!");

      for(In inuc=I_0; inuc<nnuc; ++inuc)
      {
         if (  !(aSystem.GetNuclei(inuc) == ref_system->GetNuclei(inuc))
            )
         {
            MIDASERROR("Nuclei (types and positions) must be the same for the two systems!");
         }
      }
   }

   // Set up trans-rot projection matrix if needed.
   // Just construct if from aSystem, since the geometry of aSystem and ref_system must be the same.
   MidasMatrix tr_proj_mat;
   if (  proj_out_t_r
      )
   {
      tr_proj_mat = aSystem.SetUpTotalTRProjectionMatrix(true);
   }

   // 1) If we have a reference system without modes, diagonalize the Hessian and overwrite ref_system mop file.
   // Else, just get the normal modes
   MidasMatrix ref_normal_modes;
   MidasVector ref_freqs;
   if (  has_ref_system
      )
   {
      if (  ref_system->GetModeNr() == (ref_system->GetNucleiNr()*I_3 - I_6)
         || ref_system->GetModeNr() == (ref_system->GetNucleiNr()*I_3 - I_5)
         )
      {
         // Get normal modes (as columns).
         // NB: Passing true to this function disables the inverse mass scaling and thus it should return the L matrix that diagonalizes the mass-weighted Hessian.
         ref_normal_modes = ref_system->GetOrderedModeMatrix(true);
         ref_freqs.SetNewSize(ref_system->GetModeNr());
         for(In i=I_0; i<ref_freqs.Size(); ++i)
         {
            ref_freqs[i] = ref_system->GetFreq(i);
         }

         // Check orthonormality
         auto nm_t = Transpose(ref_normal_modes);
         MidasMatrix overlap = nm_t * ref_normal_modes;
         bool non_ortho = false;
         for(In i=I_0; i<overlap.Nrows(); ++i)
         {
            non_ortho = non_ortho || libmda::numeric::float_neq(overlap[i][i], C_1);

            for(In j=i+I_1; j<overlap.Ncols(); ++j)
            {
               non_ortho = non_ortho || !libmda::numeric::float_numeq_zero(overlap[i][j], C_1, 10);
            }
         }

         if (  non_ortho
            )
         {
            MidasWarning("Non-orthonormal normal modes read in from ref_system.");
            Mout  << " Non-orthonormal normal modes from ref_system:\n" << ref_normal_modes << "\n"
                  << " Overlap matrix:\n" << overlap << std::endl;
         }
      }
      else
      {
         MidasAssert(ref_system->HasHessian(), "If reference system has no modes, it must contain a Hessian!");

         MidasMatrix ref_hessian = ref_system->GetHessian();
         MidasVector ref_gradient = ignore_gradient ? MidasVector() : ref_system->GetGradient();

         Mout  << " Diagonalizing reference-system Hessian to obtain normal coordinates." << std::endl;

         // Project out translation and rotation
         if (  proj_out_t_r
            )
         {
            Mout  << "    Projecting out translation and rotation of reference system." << std::endl;
            for(In i=I_0; i<proj_out_t_r; ++i)
            {
               midas::system::util::transform_grad_hess(ref_gradient, ref_hessian, tr_proj_mat);
            }
         }

         auto diag = DiagonalizeAndWrite(ref_gradient, ref_hessian, *ref_system, ref_system_name + "_diag");
         ref_freqs = std::move(diag.first);
         ref_normal_modes = std::move(diag.second);
      }
   }

   // 2) Project out translation and rotation if requested
   auto sys_hessian = aSystem.GetHessian();
   auto sys_gradient = ignore_gradient ? MidasVector() : aSystem.GetGradient();
   if (  proj_out_t_r
      )
   {
      Mout  << " Projecting out translation and rotation." << std::endl;
      for(In i=I_0; i<proj_out_t_r; ++i)
      {
         midas::system::util::transform_grad_hess(sys_gradient, sys_hessian, tr_proj_mat);
      }
   }

   // 4) If we have a reference system, transform system to same coordinates as reference.
   //    Otherwise, diagonalize system and print harmonic potential.
   if (  has_ref_system
      )
   {
      // Transform gradient and Hessian using modes from ref_system
      Mout  << " Transforming gradient and Hessian to reference-system coordinates." << std::endl;
      midas::system::util::transform_grad_hess(sys_gradient, sys_hessian, ref_normal_modes);

      // Write new .mop file
      std::string new_mop_file = "transformed_system.mop";
      Mout  << "    Transformed system written to:\n"
            << "       - Operator file:      " << new_mop_file << "\n"
            << std::flush;

      // Write mop file
      midas::system::util::write_operator(sys_gradient, sys_hessian, new_mop_file);

      // Check if Hessian has negative eigenvalues (we also need eigvals for bounds)
      if (  check_system_hessian
         )
      {
         auto eig = SYEVD(sys_hessian);
         In n_neg_eigvals = I_0;
         In n = eig._n;
         for(In i=I_0; i<n; ++i)
         {
            // If negative and not numerically zero, we have a negative frequency
            if (  libmda::numeric::float_neg(eig._eigenvalues[i])
               && !libmda::numeric::float_numeq_zero(eig._eigenvalues[i], eig._eigenvalues[n-I_1], 10)
               )
            {
               ++n_neg_eigvals;
            }
            // Negative eigenvalues come first due to sorting.
            else
            {
               break;
            }
         }
         if (  n_neg_eigvals > I_0
            )
         {
            MidasWarning("LHA: System Hessian has negative eigenvalues. Potential will be unbound!");

            MidasMatrix eigvecs;
            LoadEigenvectors(eig, eigvecs);

            MidasVector eigvec(n);
            Mout  << " Found negative eigenvalues and corresponding eigenvectors (in reference-system coordinates):" << std::endl;
            for(In i=I_0; i<n_neg_eigvals; ++i)
            {
               eigvecs.GetCol(eigvec, i);
               Mout  << "    - Eigval: " << eig._eigenvalues[i] << ",   Coordinate: " << eigvec << std::endl;
            }
         }
      }

      // Calculate position of minimum (relevant for determining basis-set boundaries)
      if (  write_bounds >= I_0
         )
      {
         auto nmodes = sys_hessian.Ncols();

         MidasVector min_pos(nmodes, C_0);

         if (  !ignore_gradient
            )
         {
            auto lin_sol = GESV(sys_hessian, sys_gradient); // NB: lin_sol now contains the solution with opposite sign!
            LoadSolution(lin_sol, min_pos);
            min_pos.Scale(-C_1);
            Mout  << " Position of minimum of transformed harmonic potential:\n" << min_pos << std::endl;
         }

         midas::mpi::OFileStream bounds_out(gMainDir + "/transformed_system.mbounds");
         bounds_out << std::scientific << std::setprecision(16);
         midas::mpi::OFileStream ref_bounds_out(gMainDir + "/" + ref_system_name + ".mbounds");
         ref_bounds_out << std::scientific << std::setprecision(16);
         midas::mpi::OFileStream cmb_bounds_out(gMainDir + "/combined_system.mbounds");
         cmb_bounds_out << std::scientific << std::setprecision(16);

         size_t width1 = 10;
         size_t width2 = 26;
         bounds_out << std::setw(width1) << "# Mode" << std::setw(width2) << "Left bound" << std::setw(width2) << "Right bound" << std::endl;
         ref_bounds_out << std::setw(width1) << "# Mode" << std::setw(width2) << "Left bound" << std::setw(width2) << "Right bound" << std::endl;
         cmb_bounds_out << std::setw(width1) << "# Mode" << std::setw(width2) << "Left bound" << std::setw(width2) << "Right bound" << std::endl;
         for(In imode = I_0; imode<nmodes; ++imode)
         {
            std::string modename = "Q" + std::to_string(imode);
            Nb abs_omega = std::sqrt(std::fabs(sys_hessian[imode][imode]));   // Get quasi frequency (abs sqrt of diagonal Hessian element)
            Nb delta = std::sqrt((C_2/abs_omega)*(Nb(write_bounds) + C_I_2));
            Nb tbl = min_pos[imode] - delta;
            Nb tbr = min_pos[imode] + delta;
            bounds_out  << std::setw(width1) << modename << std::setw(width2) << tbl << std::setw(width2) << tbr << std::endl;
            Nb ref_delta = std::sqrt((C_2/std::fabs(ref_freqs[imode]))*(Nb(write_bounds) + C_I_2));
            ref_bounds_out << std::setw(width1) << modename << std::setw(width2) << -ref_delta << std::setw(width2) << ref_delta << std::endl;
            cmb_bounds_out << std::setw(width1) << modename << std::setw(width2) << std::min(tbl, -ref_delta) << std::setw(width2) << std::max(tbr, ref_delta) << std::endl;
         }

         bounds_out.close();
         ref_bounds_out.close();
         cmb_bounds_out.close();
      }
   }
   else
   {
      // Diagonalize Hessian
      Mout  << " No reference system given! Diagonalize Hessian to obtain normal coordinates." << std::endl;
      DiagonalizeAndWrite(sys_gradient, sys_hessian, aSystem, "diagonalized_system");
   }
}

/**
 * Diagonalize Hessian and write .mop and .mmol
 *
 * @param aGradient     Gradient (mass-weighted and projected). Will be reduced to vib basis.
 * @param aHessian      Hessian (mass-weighted and projected). Will be reduced to vib basis.
 * @param aSystem       System
 * @param aName         Name for files
 *
 * @return
 *    Pair of frequencies and normal modes (orthonormal matrix)
 **/
std::pair<MidasVector, MidasMatrix> DiagonalizeAndWrite
   (  MidasVector& aGradient
   ,  MidasMatrix& aHessian
   ,  const System& aSystem
   ,  const std::string& aName
   )
{
   // Diagonalize
   Mout  << "    Diagonalizing." << std::endl;
   auto diag = midas::system::util::diagonalize_hessian(aHessian, true);

   Mout  << "    Frequencies:\n" << diag.first << "\n"
         << "    Eigenvectors:\n" << diag.second << "\n"
         << std::flush;

   // Transform gradient and Hessian using new normal modes
   Mout  << "    Obtaining reduced gradient and Hessian from vibrational normal modes." << std::endl;
   midas::system::util::transform_grad_hess(aGradient, aHessian, diag.second);

   // Sanity check that Hessian diagonal is equal to frequencies. If not, then the Hessian is not symmetric!
   const auto nfreq = diag.first.Size();
   bool err=false;
   for(In ifreq=I_0; ifreq<nfreq; ++ifreq)
   {
      if (  libmda::numeric::float_neq(std::pow(diag.first[ifreq], 2), aHessian[ifreq][ifreq], 10)
         )
      {
         MidasWarning("Reduced Hessian does not contain correct frequencies on the diagonal.");
         err = true;
      }
   }
   if (  err
      )
   {
      Mout  << " Reduced Hessian:\n" << aHessian << std::endl;
   }

   // Write new .mol and .mop files
   std::string new_mop_file = aName + ".mop";
   std::string new_mol_file = aName + ".mmol";
   Mout  << "    Diagonalized system written to:\n"
         << "       - Molecule file:      " << new_mol_file << "\n"
         << "       - Operator file:      " << new_mop_file << "\n"
         << std::flush;

   // Write mop file
   midas::system::util::write_operator(aGradient, aHessian, new_mop_file);

   // Write mol file
   const auto& nuc_vec = aSystem.GetNucleiVec();

   // Construct normal coordinates with inverse mass weigting: U = M^{-1/2} L
   auto y_t = Transpose(diag.second);
   for(In ilvec=I_0; ilvec<y_t.Nrows(); ++ilvec)
   {
      for(In ielem=I_0; ielem<y_t.Ncols(); ++ielem)
      {
         y_t[ilvec][ielem] /= std::sqrt(nuc_vec.at(ielem/I_3).GetMass());
      }
   }
   midas::molecule::MoleculeInfo mol_info(nuc_vec, std::move(y_t), diag.first);
   mol_info.WriteMoleculeFile(midas::molecule::MoleculeFileInfo(new_mol_file, "MIDAS"));

   // Return freqs and normal modes (L matrix) for later use
   return diag;
}
