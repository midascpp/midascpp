/*
************************************************************************
*  
* @file                LhaDrv.h
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Driver for diagonalizing and transforming harmonic 
*                      potentials.
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LHADRV_H_INCLUDED
#define LHADRV_H_INCLUDED

// Forward decl
namespace midas::input
{
   class ModSysCalcDef;
}
class System;

//! Driver for Local Harmonic Approximation
void LhaDrv(const midas::input::ModSysCalcDef& aCalcDef, const System& aSystem);

#endif /* LHADRV_H_INCLUDED */
