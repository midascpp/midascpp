/*
************************************************************************
*  
* @file                ParGridMeasureFunction.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the parallel functions for the  
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef PARGRIDMEASUREFUNCTION_H
#define PARGRIDMEASUREFUNCTION_H

#include<vector>
#include<complex>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/GridMeasureFunction.h"
#include "mpi/ForLoop.h" //Odd at it might seem this needs to be included all the time, since the loop expander should be used even without mpi
#include "mpi/Interface.h"

using namespace midas;

/**
 *
 **/
class ParGridMeasureFunction
   :  public GridMeasureFunction
{
   protected:
      Nb CalcParGradElement(In aI) const;
       
   public:
      void CalculateGradient(MidasVector& aGradVect) const
      {
         midas::mpi::ForLoop(std::bind(std::mem_fn(&ParGridMeasureFunction::CalcParGradElement), this, std::placeholders::_1), 
                midas::mpi::get_Interface().GetMpiNrProc(), aGradVect);
      }
      ParGridMeasureFunction(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* apMolInfo): GridMeasureFunction(apCalcDef,apMolInfo) 
      {
         mName += " with parallel gradient calculations";
      }
      ~ParGridMeasureFunction() 
      {
      }
};
#endif //PARGRIDMEASUREFUNCTION_H
