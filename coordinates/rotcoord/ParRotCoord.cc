/*
************************************************************************
*  
* @file                ParRotCoord.cc
*
* Created:             11-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the rotatoin coordinates 
*                      of coordinates only for parallel algorithm, including MPI
*                      Based on VscfParOptCoord by Bo Thomsen. 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <complex>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <random>

// midas headers
#include "coordinates/rotcoord/ParRotCoord.h"
#include "input/RotCoordCalcDef.h"
#include "mmv/MidasMatrix.h"
#include "input/Input.h"

#ifdef VAR_MPI
#include <mpi.h>
#include "mpi/CommunicatorManager.h"
#include "mpi/CommunicatorHandle.h"
#include "mpi/UtilFunc.h"
#endif //VAR_MPI

#include "mpi/Interface.h"
#include "mpi/ForLoop.h"

//forward declarations
std::pair<In, In> GetPairForTaskNr(In,In);
In GetTaskNrForPair(pair<In, In> aPair, In aNrOfModes);

/**
* Constructor
**/ 
ParRotCoord::ParRotCoord
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* arMolInfo
   ) 
   :  RotCoordBase
      (  apCalcDef
      ,  arMolInfo
      ) 
{
   if (mpCalcDef->GetRandomIter() != I_0)
   {
      DoRandomRotations();
   }
}

/**
* Does a series of random rotations on the operators before doing the calculation
**/ 
void ParRotCoord::DoRandomRotations()
{
   for(In h = I_0; h < mpCalcDef->GetRandomIter(); ++h)
   {
      MidasVector rotations(mSpaceSize);
      rotations.Zero();
      std::random_device random;
      In i = 0;
      if(I_0 == midas::mpi::get_Interface().GetMpiRank())
      {
         for(In j = 0; j < mNrOfModes; ++j)
         {
            for(In k = j+1; k < mNrOfModes; ++k)
        {
               Nb rand_ang = 0.25*C_PI*((Nb(random()) - Nb(random.max()/2))/Nb(random.max()));
               
               if (gIoLevel > I_5)
               {
                  Mout << " Updating operator by rotating " << rand_ang << " in " << j << "," << k << std::endl;
               }

               rotations[i++] = rand_ang;
        }
     }
      }   
      #ifdef VAR_MPI
         midas::mpi::MPIBroadcastMidasVector(rotations);
         //BCast rotations to all nodes
      #endif //VAR_MPI

      i = 0;
      for(In j = 0; j < mNrOfModes; ++j)
      {
         for(In k = j+1; k < mNrOfModes; ++k)
         {
            UpdRotMat(rotations[i++],j,k);
         }
      }
   }
   if(I_0 == midas::mpi::get_Interface().GetMpiRank())
   {
      string mpi_str = midas::mpi::get_Interface().GetMpiTreeString();
      Finalize("_rand"+mpi_str);
   }
}

/**
* Optimization of coordinates
**/ 
void ParRotCoord::DoOpt()
{
   In iter = 0;
   pair<In, In> calc_pair;
   MidasVector rotations(mSpaceSize);
   while(!CheckIsConv(iter))
   {
      midas::mpi::ForLoop
         (  std::bind
               (  std::mem_fn(&ParRotCoord::CalcMin)
               ,  this
               ,  std::placeholders::_1
               ,  iter
               )
         ,  midas::mpi::get_Interface().GetMpiNrProc()
         ,  rotations
         );

      if(I_0 == midas::mpi::get_Interface().GetMpiRank())
      {
         RemoveCoupledRotations(rotations);
      }
      #ifdef VAR_MPI
      midas::mpi::MPIBroadcastMidasVector(rotations);
      //BCast rotations to all nodes, this is just me being paranoid and ensuring that all 
        //do exactly the same rotations in the end...
      #endif //VAR_MPI
      //Mout << "Rotations : " << rotations << endl;
      for(In i = 0; i < mSpaceSize; ++i)
      {
         calc_pair = GetPairForTaskNr(i,mNrOfModes);
         if (fabs(rotations[i]) >= C_100*C_NB_EPSILON)
         {
            if (gIoLevel > I_5)
            {
               Mout << "  Updating operator by rotating " << rotations[i] << " in " << calc_pair.first << "," << calc_pair.second << std::endl;
            }

            UpdRotMat(rotations[i],calc_pair.first,calc_pair.second);
         }
         else
         {
            //if(mIoLevel > I_10)
               Mout << "Ignoring rotations in " << calc_pair.first << "," << calc_pair.second << endl;
         }
      }
      ++iter;
   }
   Mout << "Procedure done after " << iter << " iterations ..." << endl;
   if(I_0 == midas::mpi::get_Interface().GetMpiRank())
   {
      string mpi_str = midas::mpi::get_Interface().GetMpiTreeString();
      Finalize(mpCalcDef->GetNewSuffix()+mpi_str);
      Summarize(iter);
   }
}

/**
* Return angle for loop over mode pairs 
**/ 
Nb ParRotCoord::CalcMin(In aI, In aIter) const
{
   pair<In, In> calc_pair = GetPairForTaskNr(aI, mNrOfModes);
   return mMeasureFunction->GetAngle(calc_pair.first,calc_pair.second);
}

/**
* Remove coupled rotations
**/ 
void ParRotCoord::RemoveCoupledRotations(MidasVector& arRotations) const
{
   map<Nb, pair<In, In> > rot_pairs;
   
   for(In i = I_0; i < arRotations.size(); ++i)
      rot_pairs.insert(make_pair(fabs(arRotations[i]), GetPairForTaskNr(i,mNrOfModes)));

   pair<In, In> curr_pair;
   while(I_0 != rot_pairs.size())
   {
      auto it = rot_pairs.end();
      --it;
      curr_pair = it->second;
      for(auto it2 = rot_pairs.begin(); it2 != rot_pairs.end();)
      {
         if(it2 == it)
         {
            it2 = rot_pairs.erase(it2);
         }
         else if(it2->second.first == curr_pair.first || it2->second.first == curr_pair.second ||
            it2->second.second == curr_pair.first || it2->second.second == curr_pair.second  )
         {
            arRotations[GetTaskNrForPair(it2->second,mNrOfModes)] = C_0;
            it2 = rot_pairs.erase(it2);
         }
         else
            ++it2;
      }
   }
}

