/*
************************************************************************
*  
* @file                SerGridMeasureFunction.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the serial functionsfor the  
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SERGRIDMEASUREFUNCTION_H
#define SERGRIDMEASUREFUNCTION_H

#include<vector>
#include<complex>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/GridMeasureFunction.h"

using namespace midas;

/**
 *
**/
class SerGridMeasureFunction: public GridMeasureFunction
{
   protected:
      //1
      void CalculateSerGradient(MidasVector& aGradVect) const;
   
   public:
      //! Constructor
      SerGridMeasureFunction
         (  const input::RotCoordCalcDef* const apCalcDef
         ,  molecule::MoleculeInfo* apMolInfo
         )
         :  GridMeasureFunction
            (  apCalcDef
            ,  apMolInfo
            )
      {
         mName += " with serial gradient calculations";
      }
     
      //! Destructor
      ~SerGridMeasureFunction() 
      {
      }

      //! Calculate the gradient vector
      void CalculateGradient(MidasVector& aGradVect) const 
      {
         CalculateSerGradient(aGradVect);
      }
};

#endif //SERGRIDMEASUREFUNCTION_H
