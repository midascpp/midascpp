/*
************************************************************************
*  
* @file                BaseOptMeasure.cc
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the optimization of coordinates 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "coordinates/rotcoord/BaseOptMeasure.h"
#include "coordinates/rotcoord/VscfOptMeasure.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/LocMeasure.h"
#include "coordinates/rotcoord/HybridMeasure.h"

/**
 * Factory
**/
typename BaseOptMeasure::measure_ptr_t BaseOptMeasure::Factory
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* apMolInfo
   )  
{ 
   In no_of_measures = apCalcDef->NMeasures();

   MidasAssert(no_of_measures > I_0, "No measures found in RotCoordCalcDef");
  
   if (  no_of_measures == I_1
      )
   {
      if (  apCalcDef->GetLocalize()
         )
      {
         return std::make_unique<LocMeasure>(apMolInfo);
      }
      else
      {
         return VscfBaseOptMeasure::Factory(&apCalcDef->GetVscfCalcDef(I_0));
      }
   }
   else
   {
      return std::make_unique<HybridMeasure>(apCalcDef, apMolInfo);
   }
}

/**
 * Initialization of the Measure 
**/
void BaseOptMeasure::InitMeasure()
{
   std::pair<Nb, bool> first_calc = Eval(-I_1, -I_1, C_0);
   if (!first_calc.second)
   {
      MIDASERROR("Initial measure calculation failed");
   }
   mCurrentValue = first_calc.first;
}

/**
 * Function for updating the measure
**/
void BaseOptMeasure::Update
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )
{
   UpdateData(aQFirst, aQSecond, aAngle);
   std::pair<Nb, bool> calc = Eval(-I_1, -I_1, C_0);
   if (!calc.second)
   {
      MIDASERROR("Calculation failed after update!");
   }
   mCurrentValue = calc.first;
   
   if (gIoLevel > I_5)
   {
      Mout << "   Optimization measure: " << mCurrentValue << std::endl;
   }
}

