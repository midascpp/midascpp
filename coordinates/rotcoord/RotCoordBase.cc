/*
************************************************************************
*
* @file                RotCoord.cc
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles the rotation of coordinates
*                      based on the optimized coordinates class written
*                      by Bo Thomsen
*
*
* Rev. Date            Comments:
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/


// std headers
#include <string>
#include <fstream>
#include <sstream>

// midas headers
#include "util/Error.h"
#include "input/Input.h"
#include "mmv/MidasMatrix.h"
#include "coordinates/rotcoord/RotCoordBase.h"
#include "coordinates/rotcoord/SerRotCoord.h"
#include "coordinates/rotcoord/ParRotCoord.h"
#include "coordinates/rotcoord/BaseMeasureFunction.h"
#include "pes/molecule/SymInfo.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/molecule/PointGroupSymmetry.h"

class VscfOptMeasure;

/**
 * Constructor
**/
RotCoordBase::RotCoordBase
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* arMolInfo
   )
   :  mpCalcDef(apCalcDef)
   ,  mpMolInfo(arMolInfo)
   ,  mIoLevel(gIoLevel)
   ,  mNrOfModes(I_0)
   ,  mInitValue(C_0)
   ,  mMeasureFunction
      (  BaseMeasureFunction::Factory
            (  apCalcDef
            ,  arMolInfo
            )
      )
{
   mNrOfModes = mMeasureFunction->GetNrOfModes();
   mSpaceSize = In(Nb(mNrOfModes*(mNrOfModes - I_1))/C_2);

   mCurrentRotMatrix.SetNewSize(mNrOfModes);
   mCurrentRotMatrix.Unit();

   if (mpCalcDef->GetDoFreqScreen())
   {
      if (mpCalcDef->GetMinRotAng() > I_0)
      {
         MIDASERROR("Screening of mode-pair rotations cannot work with a minimum rotation angle larger than zero!");
      }

      std::vector<std::string> OptMeasure = midas::util::StringVectorFromString(mMeasureFunction->GetMeasureName());
      bool CanCalcFreqs = false;
      for (In j = I_0; j < OptMeasure.size(); ++j)
      {
         if (OptMeasure[j] == "VSCF")
         {
            CanCalcFreqs = true;
         }
      }
      if (!CanCalcFreqs)
      {
         MIDASERROR("The frequency screening of mode-pair rotations needs to have access to (quasi-)frequencies! Set Evscf as measure in input!");
      }
   }

   for (In i = I_0; i < apCalcDef->GetNoOpersForRot(); ++i)
   {
      In oper_nr = gOperatorDefs.GetOperatorNr(apCalcDef->GetOperForRotI(i));
      if (-I_1 == oper_nr)
      {
         MIDASERROR("Unknown operator "+apCalcDef->GetOperForRotI(i));
      }
      mOpersForRot.emplace_back(&gOperatorDefs[oper_nr]);
   }
}

/**
 * Factory
**/
typename RotCoordBase::rotcoord_ptr_t RotCoordBase::Factory
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* arMolInfo
   )
{
   if (apCalcDef->GetDoRotsAtSweepEnd())
   {
      return std::make_unique<ParRotCoord>(apCalcDef, arMolInfo);
   }
   else
   {
      return std::make_unique<SerRotCoord>(apCalcDef, arMolInfo);
   }
}

/**
 * Preparations
 **/
void RotCoordBase::Preparations()
{
   mMeasureFunction->Init();
   mInitValue = mMeasureFunction->GetCurrentValue();

   auto rotation_algo = "serial";
   // Pair-wise rotation of vibrational coordinates at the end of each sweep corresponds to using the parallel implementation
   if (mpCalcDef->GetDoRotsAtSweepEnd())
   {
      rotation_algo = "parallel";
   }

   Out72Char(Mout,'-','-','-');
   OneArgOut72(Mout," Settings for rotation of coordinates: ",' ');
   Out72Char(Mout,'-','-','-');
   Mout << " Optimization function type:            " << mMeasureFunction->GetName() << std::endl;
   Mout << " Optimization measure:                  " << mMeasureFunction->GetMeasureName() << std::endl;
   Mout << " Pair-wise rotation of modes:           " << "Performed in " << rotation_algo << std::endl;
   Mout << " Convergence criterion:                 " << mMeasureFunction->GetConvCrit() << std::endl;
   Mout << " Convergence threshold:                 " << mpCalcDef->GetConvThr() << std::endl;
   Mout << " Intial value:                          " << mInitValue << " " << mMeasureFunction-> GetMeasureUnit() << std::endl;
   Out72Char(Mout,'-','-','-');
   Mout << std::endl;
}

/**
 * Update current rotation matrix and Measure
**/
void RotCoordBase::UpdRotMat
   (  Nb aAngle
   ,  In aQFirst
   ,  In aQSecond
   )
{
   if ((gDebug || gIoLevel > I_7) && mMeasureFunction->KnowsFrequencies())
   {
      std::vector<Nb> curr_freqs = mMeasureFunction->GetCurrentFreqs();

      Mout << "   Frequency of mode " << aQFirst << ": " << curr_freqs[aQFirst]*C_AUTKAYS << std::endl;
      Mout << "   Frequency of mode " << aQSecond << ": " << curr_freqs[aQSecond]*C_AUTKAYS << std::endl;
   }

   // Rotate arRotMat for (aQFirst,aQSecond) with angle aAngle
   MidasMatrix rot(mCurrentRotMatrix.Nrows());
   rot.Unit();
   rot[aQFirst][aQFirst] = cos(aAngle);
   rot[aQFirst][aQSecond] = sin(aAngle);
   rot[aQSecond][aQFirst] = -sin(aAngle);
   rot[aQSecond][aQSecond] = cos(aAngle);

   if (gDebug || gIoLevel > I_12)
   {
      Mout << std::endl << "  The rotation matrix for this iteration: " << std::endl;
      Mout << rot << std::endl;
   }

   MidasMatrix temp = rot*mCurrentRotMatrix;
   mCurrentRotMatrix = temp;

   if (gDebug || gIoLevel > I_12)
   {
      Mout << std::endl << "  The total rotation matrix at this iteration is: " << std::endl;
      Mout << mCurrentRotMatrix << std::endl;
   }

   if (mpMolInfo != nullptr)
   {
      mpMolInfo->MixNormalCoordByRotation(rot);
   }
   mMeasureFunction->Update(aQFirst, aQSecond, aAngle);

   // Dont quite like it, needs to give an energy operator first!
   if (mOpersForRot.size() > I_0)
   {
      mOpersForRot[I_0].TransformCurrOper(aQFirst, aQSecond, aAngle);

      for (In i = I_1; i < mOpersForRot.size(); ++i)
      {
         mOpersForRot[i].TransformCurrOper(aQFirst, aQSecond, aAngle, mOpersForRot[I_0].GetScaleFrequencies());
      }
   }
}

/**
 *  Check convergence
**/
bool RotCoordBase::CheckIsConv(In aIt)
{

   if (I_0 == aIt)
   {
      Out72Char(Mout,'-','-','-');
      OneArgOut72(Mout,"Starting rotation of coordinates",' ');
      Out72Char(Mout,'-','-','-');
      Mout << std::endl;

      if (gIoLevel > I_4)
      {
         Mout << " Iteration " << aIt + I_1 << std::endl;
      }

      return false;
   }

   std::pair<bool,Nb> conv_crit = mMeasureFunction->ConvCrit(aIt);
   bool conv = (conv_crit.first && std::fabs(conv_crit.second) < mpCalcDef->GetConvThr());

   if (mpCalcDef->GetNumberOfSweeps() <= aIt)
   {
      if (!conv)
      {
         MidasWarning ("Rotation of Coordinates did not converge, due to maximum number of iterations reached");
      }
      return true;
   }

   if (!conv)
   {
      if (gIoLevel > I_4)
      {
         Mout << std::endl << " Iteration " << aIt + I_1 << std::endl;
      }
   }

   return conv;

}

/**
 *  Finalize
**/
void RotCoordBase::Finalize(const std::string& aPostFix)
{
   mMeasureFunction->Finalize(aPostFix);
   for (In i = I_0; i < mOpersForRot.size(); ++i)
   {
      mOpersForRot[i].DumpPotential(aPostFix);
   }

   if (mpMolInfo != nullptr)
   {
      if (mMeasureFunction->KnowsFrequencies())
      {
         FrequencyData frequency_data(FrequencyType::Key());
         std::vector<Nb> freqs = mMeasureFunction->GetCurrentFreqs();
         frequency_data.Initialize(origin_type::CALCULATED, std::move(freqs),"AU");

         for (In i = I_0; i < gModSysCalcDefs.size(); ++i)
         {
            if (gModSysCalcDefs[i].GetSymmetryDefined())
            {
               auto syminfo = midas::molecule::PseudoSymmetryAnalysis(*mpMolInfo, SymmetryThresholds());
               mpMolInfo->ReInitFreqSymm(frequency_data);
            }
            else
            {
               mpMolInfo->ReInitFrequencies(frequency_data);
            }
         }
      }
      else
      {
         MidasWarning("The frequencies in the .mop file have not been updated");
      }
      DumpCoordinateFile(aPostFix);
   }
   if (!mpCalcDef->GetRotMatFile().empty())
   {
      DumpRotMatrix();
   }
}

/**
 *  Summarize
**/
void RotCoordBase::Summarize(const In& aIt)
{
   Mout << std::endl;
   Out72Char(Mout,'-','-','-');
   OneArgOut72(Mout," Summary of rotation of coordinates",' ');
   Out72Char(Mout,'-','-','-');
   Mout << " Number of iterations done:           " << aIt << endl;
   midas::stream::ScopedPrecision(22, Mout);
   Mout << " Initial value:                       " << mInitValue << " " << mMeasureFunction->GetMeasureUnit() << endl ;
   Mout << " Final value:                         " << mMeasureFunction->GetCurrentValue() <<" " << mMeasureFunction->GetMeasureUnit() <<  endl;
   Out72Char(Mout,'-','-','-');
}

/**
 * Dumps Rotation matrix to file
**/
void RotCoordBase::DumpRotMatrix() const
{
   ofstream file(mpCalcDef->GetRotMatFile());
   if (!file.is_open())
   {
      MIDASERROR("Could not open file " + mpCalcDef->GetRotMatFile() + " for writing");
   }
   for (In i = I_0; i < mCurrentRotMatrix.Nrows(); ++i)
   {
      for (In j = I_0; j < mCurrentRotMatrix.Ncols(); ++j)
      {
         file << mCurrentRotMatrix[i][j] << " ";
      }
      file << std::endl;
   }
}

/**
 * Dumps the new optimized coordinates to NumDer and minfo file, and outputs some statistics over the result of the rotation
**/
void RotCoordBase::DumpCoordinateFile
   (  const std::string& aPostFix
   )  const
{
   mpMolInfo -> WriteMoleculeFile({"Molecule" + aPostFix + ".mmol", "MIDAS"});
   mpMolInfo -> WriteMoleculeFile({"Input" + aPostFix + ".molden", "MOLDEN"});
   mpMolInfo -> WriteMoleculeFile({"Input" + aPostFix + ".minfo", "SINDO"});
}
