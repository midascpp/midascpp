/*
************************************************************************
*  
* @file                ParGridMeasureFunction.cc
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the serial functionsfor the  
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<vector>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "coordinates/rotcoord/ParGridMeasureFunction.h"
#include "coordinates/rotcoord/ParRotCoord.h"

//forward declaration
std::pair<In, In> GetPairForTaskNr(In,In);

/**
* Calculate gradient element for parallel computations 
**/
Nb ParGridMeasureFunction::CalcParGradElement(In aI) const
{
   In nmodes = mOptMeasure->GetNrOfModes();
   std::pair<In, In> calc_pair = GetPairForTaskNr(aI, nmodes);
   return CalculateGradElement(calc_pair.first,calc_pair.second);
}

