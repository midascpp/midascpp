/*
************************************************************************
*  
* @file                LocMeasure.cc
*
* Created:             26-11-2014
*
* Author:              Emil Lund Klinting (Klint@chem.au.dk)
*
* Short Description:   Contains the functions needed for the localization
*                      measure, which includes  evaluation of the localization
*                      criterion
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <fstream>
#include <string>
#include <sstream>


// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/molecule/MoleculeReader.h"
#include "pes/molecule/MoleculeInfo.h"
#include "coordinates/rotcoord/LocMeasure.h"

/**
 * Constructor
**/
LocMeasure::LocMeasure
   (  midas::molecule::MoleculeInfo* arMolInfo
   )
   :  mMoleculeInfo(arMolInfo)
{
   MidasAssert(arMolInfo != nullptr, "No Molecule information given in Localization Measure");
   mMeasureType = "Localization";
   mMeasureUnit = "";
   mNrOfModes = mMoleculeInfo->GetNoOfVibs();
   //PairDistance(); //Emil: Should this be outcommented? 
   //DistanceFourthMC(); //Emil: Should this be outcommented?
}

/**
 * Function for calculating the atomic contributions for similar modes
**/
std::vector<Nb> LocMeasure::AtomicContri_i(In aQFirst) const
{
   In natoms=mMoleculeInfo->GetNumberOfNuclei();
   
   In nmodes=mMoleculeInfo->GetNoOfVibs();
   if(nmodes != mNrOfModes)  
   {
      MIDASERROR("Number of modes are not equal!");
   }
   
   MidasVector normalmode_i; 
   mMoleculeInfo->GetNormalMode(normalmode_i, aQFirst);  
   
   vector<Nb> atomic_contribution_i(natoms, C_0); 
    
   for (In i=I_0; i<natoms; ++i)   
   {                              
      for (In j=I_0; j<I_3; ++j)  
      {
         atomic_contribution_i[i] += normalmode_i[I_3*i+j]*normalmode_i[I_3*i+j];
      
      }
      Nb mass_factor= mMoleculeInfo->GetNuclMassi(i);
      atomic_contribution_i[i] *= mass_factor;
   }  
   return atomic_contribution_i; 
}

/**
* Function for calculating the atomic contributions for dissimilar modes
**/
std::vector<Nb> LocMeasure::AtomicContri_ij(In aQFirst, In aQSecond) const
{
   In natoms=mMoleculeInfo->GetNumberOfNuclei();
   
   In nmodes=mMoleculeInfo->GetNoOfVibs();
   if(nmodes != mNrOfModes)  
   {
      MIDASERROR("Number of modes are not equal!");
   }
   
   MidasVector normalmode_i; 
   mMoleculeInfo->GetNormalMode(normalmode_i, aQFirst);  

   MidasVector normalmode_j;
   mMoleculeInfo->GetNormalMode(normalmode_j, aQSecond);

   std::vector<Nb> atomic_contribution_ij(natoms, C_0);
   
   for (In i=I_0; i<natoms; ++i)   
   {                              
      for (In j=I_0; j<I_3; ++j)  
      {
         atomic_contribution_ij[i] += normalmode_i[I_3*i+j]*normalmode_j[I_3*i+j];
      }
      Nb mass_factor= mMoleculeInfo->GetNuclMassi(i);
      atomic_contribution_ij[i] *= mass_factor;
   }  
   return atomic_contribution_ij;
}

/**
* Function for calculating the A value
**/
Nb LocMeasure::CalculateA(In aQFirst, In aQSecond) const
{
   Nb a = C_0;  

   auto atomic_contribution_i = AtomicContri_i(aQFirst);   
   auto atomic_contribution_j = AtomicContri_i(aQSecond);   
   auto atomic_contribution_ij = AtomicContri_ij(aQFirst, aQSecond);   

   In natoms = mMoleculeInfo->GetNumberOfNuclei();

   for (In i=I_0; i<natoms; ++i) 
   {                             
      a += atomic_contribution_ij[i]*atomic_contribution_ij[i] - C_I_4*(atomic_contribution_i[i] - atomic_contribution_j[i])*(atomic_contribution_i[i] - atomic_contribution_j[i]);
   }
   return a;
}

/**
* Function for calculating the B value
**/
Nb LocMeasure::CalculateB(In aQFirst, In aQSecond) const
{
   Nb b = C_0;

   auto atomic_contribution_i = AtomicContri_i(aQFirst);   
   auto atomic_contribution_j = AtomicContri_i(aQSecond);   
   auto atomic_contribution_ij = AtomicContri_ij(aQFirst, aQSecond);   

   In natoms=mMoleculeInfo->GetNumberOfNuclei();

   for (In i=I_0; i<natoms; ++i) 
   {                             
      b += (atomic_contribution_i[i] - atomic_contribution_j[i])*atomic_contribution_ij[i];
   }
   return b;
}

/**
* Function for calculating the angle alpha
**/
Nb LocMeasure::AnglePM(In aQFirst, In aQSecond) const 
{
   Nb a = CalculateA(aQFirst, aQSecond);
   Nb b = CalculateB(aQFirst, aQSecond);

   Nb alpha = C_I_4*acos(-a/(sqrt(a*a + b*b))); 

   if(std::sin(C_4*alpha) - b/(std::sqrt(a*a + b*b)) > C_I_10_6)  
   {                                                   
      alpha = -alpha;                                  
   }

   Mout << "This is alpha: " << alpha << endl;
   
   return alpha;      
}

/**
* Function for calculating the atomic localization criterion
**/
Nb LocMeasure::kiPM() const
{   
   In natoms=mMoleculeInfo->GetNumberOfNuclei();  
   In nmodes=mMoleculeInfo->GetNoOfVibs();  

   Nb p = C_0;

   for(In k=I_0; k<nmodes; ++k)
   {
      auto atomic_contribution_i = AtomicContri_i(k);   
                                                 
      for(In i=I_0; i<natoms; ++i) 
      {
         p += atomic_contribution_i[i]*atomic_contribution_i[i];
      }
   }
   
   return p;
}

/**
* Function for calculating the variation in the localization criterion with 
* different rotation angle gamma
**/
Nb LocMeasure::kiPM_rot(In aQFirst, In aQSecond, Nb aGamma) const 
{
   Nb p_rot = C_0;
  
   if(aQFirst == -I_1 && aQSecond == -I_1)   
   {
      return kiPM();
   }
   
   Nb a = CalculateA(aQFirst, aQSecond);
   Nb b = CalculateB(aQFirst, aQSecond);

   p_rot = mCurrentValue + a - a*cos(C_4*aGamma) + b*sin(C_4*aGamma);
   
   return p_rot;
}

/**
* Function for calculating the "energy" of the localization criterion 
**/
std::pair<Nb, bool> LocMeasure::Eval(In aQFirst, In aQSecond, Nb aAngle) 
{
   Nb loccrit = -C_1 * kiPM_rot(aQFirst, aQSecond, aAngle);
  
   return std::make_pair(loccrit, true);
}

/**
* Return the number of modes
**/
void LocMeasure::UpdateData(In aQFirst, In aQSecond, Nb aAngle)
{
   mCurrentValue = -kiPM(); 
}  


/**
* Calculate the centers of vibration, which are then put into a single vector
**/
std::vector<std::vector<Nb>> LocMeasure::CentersOfVib() const
{
   std::vector<std::vector<Nb>> vib_center;
   
   const auto& atomic_coordinates = mMoleculeInfo->GetCoord();

   In natoms=mMoleculeInfo->GetNumberOfNuclei();
   In nmodes=mMoleculeInfo->GetNoOfVibs();  

   vib_center.reserve(nmodes);
   
   for(In k=I_0; k<nmodes; ++k)
   {
      auto atomic_contribution_i = AtomicContri_i(k);   
      std::vector<Nb> center (I_3,C_0);   
      for (In i=I_0; i<natoms; ++i) 
      {                           
         center[0] += atomic_contribution_i[i] * atomic_coordinates[i].X();  
         center[1] += atomic_contribution_i[i] * atomic_coordinates[i].Y();  
         center[2] += atomic_contribution_i[i] * atomic_coordinates[i].Z();  
      }
      vib_center.push_back(center);
   }
 
   return vib_center; 
}

/**
 * Calculate the pair distance d(k,l) between centers of vibration
**/
void LocMeasure::PrintPairDistances() const
{
   Nb pair_distance;
   
   In nmodes = mMoleculeInfo->GetNoOfVibs();  

   std::vector<std::vector<Nb>> vib_center = CentersOfVib();

   Mout << " Pair distances are determined to be: " << std::endl; 
   for (In k = I_0; k < nmodes; ++k)
   {
      for (In j = I_0; j < k; ++j) 
      {
         pair_distance = C_0;
   
         for (In l = I_0; l < I_3; ++l)
         { 
            pair_distance += (vib_center[k][l] - vib_center[j][l])*(vib_center[k][l] - vib_center[j][l]); 
         } 
         Mout << "d(" << k << "," << j << ") = " << std::sqrt(pair_distance) << std::endl; 
      }
   }
   
}

/**
 * Calculate the pair distance d(k,l) between centers of vibration
**/
Nb LocMeasure::PairDistance
   (  const In& aMode1
   ,  const In& aMode2
   )  const
{
   Nb pair_distance = C_0;
   
   std::vector<std::vector<Nb>> vib_center = CentersOfVib();
 
   for (In l = I_0; l < I_3; ++l)
   { 
      pair_distance += (vib_center[aMode1][l] - vib_center[aMode2][l])*(vib_center[aMode1][l] - vib_center[aMode2][l]); 
   }         
   
   return std::sqrt(pair_distance); 
}

/**
 * Function for calculating the fourth order MC
**/
void LocMeasure::DistanceFourthMC() const
{
   Nb fourth_mc_distance; 

   In nmodes = mMoleculeInfo->GetNoOfVibs();  

   std::vector<std::vector<Nb>> vib_center = CentersOfVib();
   
   Mout << " The fourth order MC is determined to be: " << std::endl;
   for (In i = I_0; i < nmodes; ++i)
   {
      for (In j = I_0; j < i; ++j)
      {
         Nb dist1 = PairDistance(i,j);

         for (In k = I_0; k < j; ++k)
         {
            Nb dist2 = PairDistance(i,k);
            Nb dist3 = PairDistance(j,k);

            for (In l = I_0; l < k; ++l)
            {
               Nb dist4 = PairDistance(i,l);
               Nb dist5 = PairDistance(j,l);
               Nb dist6 = PairDistance(k,l);
               
               fourth_mc_distance = (dist1 + dist2 + dist3 + dist4 + dist5 + dist6)/I_6;
               Mout << fourth_mc_distance << " " << l << " " << k << " " << j << " " << i << std::endl;
            }
         }
      }
   }
}

/**
 * Finalize
**/
void LocMeasure::Finalize
   (  const std::string& arPostFix
   )
{
}

