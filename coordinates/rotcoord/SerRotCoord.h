/*
************************************************************************
*  
* @file                SerRotCoord.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenog (ckonig@chem.au.dk)
*
* Short Description:   Handles the optimization of coordinates by finding 
*                      the minima with respect to rotation sets
*                      of coordinates only for serial algorithm
*                      based on VscfSerOPtCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SERROTCOORD_H
#define SERROTCOORD_H
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/RotCoordBase.h"

using namespace midas;

class SerRotCoord : public RotCoordBase
{
   private:
      void DoRandomRotations();
   public:
      void DoOpt();
      SerRotCoord(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* apMolInfo);
};
#endif //SERROTCOORD_H
