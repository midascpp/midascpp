/*
************************************************************************
*  
* @file                VscfOptMeasure.cc
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Vscf Measures 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "coordinates/rotcoord/OneStateVscfOptMeasure.h"
#include "coordinates/rotcoord/SaVscfOptMeasure.h"
#include "coordinates/rotcoord/VscfOptMeasure.h"
#include "input/RotCoordCalcDef.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "vscf/VscfDrv.h"

//forward declarations
In FindVScfCalcNumber(const string&);

/**
 * Constructor 
**/
VscfOptMeasure::VscfOptMeasure
   (  const input::RotCoordVscfCalcDef* const apCalcDef
   )
   :  VscfBaseOptMeasure(apCalcDef)
{ 

   mCalcNo=FindVScfCalcNumber(apCalcDef->GetName()); 
   mCalcDef= &gVscfCalcDef[mCalcNo];
   mOperNo=gOperatorDefs.GetOperatorNr(mCalcDef->Oper());
   if (mOperNo == -1) MIDASERROR("Operator not found in VscfOptMeasure");
   mOperRotHandler.reset(new OperRotHandler(&gOperatorDefs[mOperNo],apCalcDef->GetKeepMaxCoupLevel()));
   if (apCalcDef -> GetRotZeroFirst())
   {
      for (In i=0; i < GetNrOfModes(); ++i)
         for (In j=0; j < GetNrOfModes(); ++j)
            mOperRotHandler->TransformCurrOper(i, j,C_0);
   }

   string basis_name = mCalcDef->Basis();
   mBasNr=-1;
   for (In i = I_0; i < gBasis.size(); i++) 
   {
      if (gBasis[i].GetmBasName() == basis_name)
      {
         mBasNr = i;
      }
   }
   if (mBasNr == -1) MIDASERROR("Basis not found in VscfOptMeasure");
   if (!gDebug) Mout.Mute();
   bool succ=midas::vscf::VscfPreps(mCalcNo, mOperNo, mBasNr);
   if (!gDebug) Mout.Unmute();
   if (!succ)
   {
      MIDASERROR("VscfPreparation failed");
   }
   //mOperRotHandler= new OperRotHandler(&gOperatorDefs[oper_nr]);
  
   mOperRotHandler.reset(new OperRotHandler(&gOperatorDefs[mOperNo],apCalcDef->GetKeepMaxCoupLevel()));
  
   if (apCalcDef -> GetRotZeroFirst())
   {
      for (In i=0; i < GetNrOfModes(); ++i)
         for (In j=0; j < GetNrOfModes(); ++j)
            mOperRotHandler->TransformCurrOper(i, j, C_0);
   }
}

/**
* Factory 
**/
typename VscfOptMeasure::measure_ptr_t VscfOptMeasure::Factory
   (  const input::RotCoordVscfCalcDef* const apCalcDef
   )
{
   switch(apCalcDef->GetCalcType())
   {
      case input::RotCoordVscfCalcType::GROUNDSTATE:
      {
         return std::make_unique<OneStateVscfOptMeasure>(apCalcDef);
         break;
      }
      case input::RotCoordVscfCalcType::STATEAVERAGE:
      {
         return std::make_unique<SaVscfOptMeasure>(apCalcDef);
         break;
      }
      default:
      {
         MIDASERROR("Unknown method in VscfOptMeasure");
      }
   }
   return nullptr;
}

