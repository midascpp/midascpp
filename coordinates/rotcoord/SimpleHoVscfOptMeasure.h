/*
************************************************************************
*  
* @file                SimpleHoVscfOptMeasure.h
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles simple HO "VSCF" measure 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SIMPLEHOVSCFOPTMEASURE_H
#define SIMPLEHOVSCFOPTMEASURE_H
#include<memory>


#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/VscfBaseOptMeasure.h"

using namespace midas;

/**
 *
 **/
class SimpleHoVscfOptMeasure
   :  public VscfBaseOptMeasure
{
   protected:
  
   public:
      SimpleHoVscfOptMeasure(const input::RotCoordVscfCalcDef* const apCalcDef);
      virtual ~SimpleHoVscfOptMeasure()
      {
      }    
      virtual std::pair<Nb, bool> Eval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0);
      virtual std::pair<Nb, bool> RelEval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0);
};

#endif //SIMPLEHOVSCFOPTMEASURE_H
