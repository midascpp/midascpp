/**
************************************************************************
* 
* @file                RotCoordDrv.cc
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Driver for Coordinate Rotations
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/RotCoordCalcDef.h"
#include "input/Input.h"
#include "coordinates/rotcoord/RotCoordBase.h"

/**
 * Run coordinate rotations
 **/
void RotCoordDrv
   (  const input::RotCoordCalcDef* const apRotCoordCalcDef
   ,  molecule::MoleculeInfo* arMolInfo
   )
{

   Mout << std::endl << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin Rotation of Coordinates ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
  
   auto rot_coord = RotCoordBase::Factory(apRotCoordCalcDef, arMolInfo);   

   rot_coord->Preparations();
      
   if (  apRotCoordCalcDef->GetGridCalcDef().GetPlotOnly()
      )
   { 
      Mout << "Entered plotonly " << std::endl;
      rot_coord->PlotAllRotations();
      Mout << "PlotOnly done" << std::endl;
   }
   else
   {
      rot_coord->DoOpt();
   }   
}

