/**
************************************************************************
*  
* @file                OperRotHandler.h
*
* Created:             14-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Handles the rotation of potential operator
* 
* Rev. Date            03-12-2015 (C. Koenig) 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef OPERROTHANDLER_H
#define OPERROTHANDLER_H

#include <vector>
using std::vector;
#include <map>
using std::map;
#include "coordinates/rotcoord/OperTransformHandler.h"
//Forward declaring since we are not really using here
class OpDef;

class OperRotHandler: public OperTransformHandler
{
   private:
      OperRotHandler();
      void GenerateNewOperTensor(map<vector<GlobalModeNr>, Nb>&, In, In, Nb);
      void NaiveTransform(map<vector<GlobalModeNr>, Nb>&, In, In, Nb);
      void Transform(map<vector<GlobalModeNr>, Nb>&,In,In,Nb) const;
      void TransformElement(map<vector<GlobalModeNr>, Nb>&,map<vector<GlobalModeNr>, Nb>::iterator, In, In, Nb, In) const;
   public:
      void TransformCurrOper(In, In, Nb);
      void TransformCurrOper(In, In, Nb, MidasVector);
      void GenerateNewOper(In, In, Nb, OpDef&) const;
      OperRotHandler(OpDef* apOpDef, bool arKeepMaxCoupLevel = false): OperTransformHandler(apOpDef,arKeepMaxCoupLevel){;}
      std::pair<Nb,bool> TwoModeSimpleHoEnergyAfterRot(In aQFirst, In aQSecond, Nb aAngle) const;
    

};
#endif //OPERROTHANDLER_H
