/*
************************************************************************
*  
* @file                OperTransLinCombHandler.cc
*
* Created:             04-12-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles linear combinations of modes in a potential also 
*                      leading to nonorthonormal and/or redundant bases
* 
* Rev. Date            
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<vector>
#include<map>
#include<algorithm>
#include<set>
#include <fstream>

#include "coordinates/rotcoord/OperTransLinCombHandler.h"

In factorial(const In& n)
{
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

template <class T>
In permutations(const std::vector<T>& arVec)
{
   In perm = factorial(arVec.size());
   
   std::vector<T> unique_vec=arVec;
   auto it = std::unique(unique_vec.begin(), unique_vec.end()); 
   unique_vec.resize(std::distance(unique_vec.begin(),it));  
   
   std::vector<In> count_vec;
   for (T t : unique_vec)
   {
      perm/= factorial(std::count(arVec.begin(), arVec.end(),t));
   }
   
   return perm; 
}

/**
 *
**/
void OperTransLinCombHandler::TransformCurrOper
   (  const MidasMatrix& arRotMat
   ,  const std::vector<string>& arOldModeNames
   ,  const std::vector<string>& arNewModeNames
   ,  const bool& arKeepOldModes
   )
{
   
   MidasAssert((arRotMat.Nrows()==arNewModeNames.size()) && (arRotMat.Ncols()==arOldModeNames.size()),
          "Rotation matrix dimension does not fit to old ModeName vectors in OperTransLinCombHandler::TransformCurrOper" );

   MidasVector coef_vec(arOldModeNames.size(),C_0);
   for (In i=I_0; i < arNewModeNames.size(); ++i)
   {
      arRotMat.GetRow(coef_vec,i);
      AddLinComb(arOldModeNames,coef_vec,arNewModeNames.at(i)); 
   }
  
   if (!arKeepOldModes)
   {
      RemoveModes(arOldModeNames);
   }

}

void OperTransLinCombHandler::AddLinComb(const std::vector<string>& arOldModeNames, const MidasVector& arCoeffVec, const string& arNewName)
{

   std::vector<GlobalModeNr> mode_no_vec=GetModeNrVector(arOldModeNames);
   std::set<GlobalModeNr> old_mode_no_set;
   old_mode_no_set.insert(mode_no_vec.begin(),mode_no_vec.end());

   GlobalModeNr new_mode_no=*std::max_element(mModeNumbers.begin(),mModeNumbers.end())+I_1;
   mModeNumbers.push_back(new_mode_no);
   MidasAssert((std::find(mModeNames.begin(),mModeNames.end(),arNewName)==mModeNames.end()),"Cannot add a mode with an existing mode name ");
   mModeNames.push_back(arNewName);

   std::map<std::vector<GlobalModeNr>, Nb> work_map=mCurrOper;
   std::map<std::vector<GlobalModeNr>, Nb> next_map;
   std::map<std::vector<GlobalModeNr>, Nb> finished_map;

   while (work_map.size() > I_0)
   {
      next_map.clear();
      for(auto it = work_map.begin(); it != work_map.end(); ++it)
      {  
         if (std::any_of(mode_no_vec.begin(),mode_no_vec.end(),[it](const GlobalModeNr no)
                   {return (std::find(it->first.begin(),it->first.end(),no)!=it->first.end());}))
         {
            std::vector<GlobalModeNr> tmp_vec=it->first;

            // follwing not particular beautiful code.. change!! 
            std::map<GlobalModeNr,In> first_entry_map;
            for (auto it_mode = tmp_vec.begin(); it_mode !=tmp_vec.end(); ++it_mode)
            {
               if (std::find(it->first.begin(),it->first.end(),*it_mode)!=it->first.end())
               {
                  auto it_old_mode=std::find_first_of(tmp_vec.begin(),tmp_vec.end(),mode_no_vec.begin(),mode_no_vec.end());
                  first_entry_map.emplace(*it_old_mode,std::distance(tmp_vec.begin(),it_old_mode));
               }
            }
            std::vector<GlobalModeNr> old_vec = tmp_vec;

            for (auto it_map = first_entry_map.begin(); it_map != first_entry_map.end(); ++it_map)
            {
               tmp_vec = old_vec;
               tmp_vec[it_map->second] = new_mode_no;
               std::sort(tmp_vec.begin(),tmp_vec.end());
               if (finished_map.find(tmp_vec)!=finished_map.end()) continue; // to prevent doing things twice 
               
               if (mKeepMaxCoupLevel)
               {
                   std::set<GlobalModeNr> tmp_set;
                   tmp_set.insert(tmp_vec.begin(),tmp_vec.end());
                   
                   std::set<GlobalModeNr> old_set_in;
               
                   std::set_intersection(old_mode_no_set.begin(),old_mode_no_set.end(),
                          tmp_set.begin(),tmp_set.end(),std::inserter(old_set_in,old_set_in.begin()));
                   
                   if (tmp_set.size()-old_set_in.size()>mMaxCoupLevel) 
                   {
                      /*
                      Mout << " Kicked out due to maximal ModeCombiOrder " << mMaxCoupLevel << endl;;
                      std::for_each(tmp_vec.begin(),tmp_vec.end(),[](GlobalModeNr no){Mout << no << " " ;});
                      Mout << endl;
                      */
                      continue; 
                   }
               }
               //Mout << " Working on ";
               //std::for_each(tmp_vec.begin(),tmp_vec.end(),[](GlobalModeNr no){Mout << no << " " ;});
               //Mout << endl;
               
               Nb coef = C_0;
               bool include=false;
               for (In i=I_0;i < mode_no_vec.size(); ++i)
               {
                  /*
                  Mout << " mode " << i << "  oldname " << arOldModeNames.at(i) << " coef " << arCoeffVec[i] << endl;
                  */
                  std::vector<GlobalModeNr> old_tmp_vec =old_vec;
                  old_tmp_vec[it_map->second]=mode_no_vec.at(i);
                  std::sort(old_tmp_vec.begin(),old_tmp_vec.end());
                  if (work_map.find(old_tmp_vec)!=work_map.end())
                  {
                     coef+= arCoeffVec[i]*work_map.at(old_tmp_vec)/permutations(old_tmp_vec);
                     /*
                     Mout << " added  contribution " << arCoeffVec[i] << " * " <<  work_map.at(old_tmp_vec) << "/" << permutations(old_tmp_vec) <<  " from " ; 
                     std::for_each(old_tmp_vec.begin(),old_tmp_vec.end(),[](GlobalModeNr no){Mout << no << " " ;});
                     Mout << endl; 
                     */
                     include=true;
                  }
                  else 
                  {
                     //Mout << " did not find the operator term ";
                     //std::for_each(old_tmp_vec.begin(),old_tmp_vec.end(),[](GlobalModeNr no){Mout << no << " " ;});
                     //Mout << endl;
                  }
               } 
               if (include)
               { 
                  //Mout << " added  with coef = "  << coef <<  " * " << permutations(tmp_vec) << endl << "---" << endl;
                  coef *= permutations(tmp_vec);
                  next_map.emplace(tmp_vec,coef);
                  finished_map.emplace(tmp_vec,coef);
               }
               /*
               else 
               {
                  Mout << " did not find any term for ";
                  std::for_each(tmp_vec.begin(),tmp_vec.end(),[](GlobalModeNr no){Mout << no << " " ;});
                  Mout << endl;
               }
               */
            }
         }
      }
      work_map.swap(next_map);
   }
 
   // scaling factors   
   Nb scalfac=C_1;
   std::vector<GlobalModeNr> quad_term(I_2,new_mode_no);
   auto it_quad=finished_map.find(quad_term);
   if (it_quad!=finished_map.end())
   {
      scalfac=C_2*it_quad->second;
   } 
   else
   {
      MidasWarning("Did not find harmonic term in potential transformation");
   }
   if (mScalFactors.Size()!=I_0)
   {
      mScalFactors.SetNewSize(mScalFactors.Size()+I_1);
      mScalFactors[mScalFactors.Size()-I_1]=scalfac;  
   }
   if (mFrequencies.Size()!=I_0)
   {
      mFrequencies.SetNewSize(mFrequencies.Size()+I_1);
      mFrequencies[mFrequencies.Size()-I_1]=sqrt(scalfac);  
   }
   
 
   mCurrOper.insert(finished_map.begin(),finished_map.end()); 
}


void OperTransLinCombHandler::RemoveModes(const std::vector<string>& arModeNames)
{
   // first the operator terms
   std::vector<GlobalModeNr> mode_no_vec=GetModeNrVector(arModeNames);
   for(auto it = mCurrOper.begin(); it != mCurrOper.end();)
   {  
      if (std::any_of(mode_no_vec.begin(),mode_no_vec.end(),[it](const GlobalModeNr no)
                {return (std::find(it->first.begin(),it->first.end(),no)!=it->first.end());}))
      {
         it=mCurrOper.erase(it);
      }
      else 
      {
         ++it;
      }
   }

   // make new scaling midas vectors
   if (mScalFactors.Size()>I_0)
   {
      MidasVector new_scalfactors(mScalFactors.Size()-arModeNames.size());
      In n=I_0;
      for (In i=I_0; i < mScalFactors.Size(); ++i)
      {
          if (std::find(arModeNames.begin(),arModeNames.end(),mModeNames.at(i))==arModeNames.end())
          {
             new_scalfactors[n++]=mScalFactors[i];
          }
      }
      MidasAssert(n==new_scalfactors.Size(),"Size of new and old scaling factor vector does not fit");
      mScalFactors.SetNewSize(n,false);
      mScalFactors=new_scalfactors;
   }
   if (mFrequencies.Size()>I_0)
   {
      MidasVector new_freqs(mFrequencies.Size()-arModeNames.size());
      In n=I_0;
      for (In i=I_0; i < mFrequencies.Size(); ++i)
      {
          if (std::find(arModeNames.begin(),arModeNames.end(),mModeNames.at(i))==arModeNames.end())
          {
             new_freqs[n++]=mFrequencies[i];
          }
      }
      MidasAssert(n==new_freqs.Size(),"Size of new and old scaling factor vector does not fit");
      mFrequencies.SetNewSize(n,false);
      mFrequencies=new_freqs;
   }


   // remove modes from the vectors
   for (In i=I_0; i < arModeNames.size(); ++i)
   {
      // find index
      auto it_mode=std::find(mModeNames.begin(),mModeNames.end(),arModeNames.at(i));
      In index = std::distance(mModeNames.begin(),it_mode);
      // erase it from mode names
      mModeNames.erase(it_mode);
      // erase it from the mode numbers
      auto it_mode_numbers=mModeNumbers.begin();
      std::advance(it_mode_numbers,index);
      mModeNumbers.erase(it_mode_numbers);
   }


}
