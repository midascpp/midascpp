/*
************************************************************************
*  
* @file                VscfOptMeasure.h
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles Vscf for the optimization procedure
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef VSCFOPTMEASURE_H
#define VSCFOPTMEASURE_H
#include<memory>


#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "input/VscfCalcDef.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "coordinates/rotcoord/BaseOptMeasure.h"
#include "coordinates/rotcoord/VscfBaseOptMeasure.h"

using namespace midas;

/**
 *
 **/
class VscfOptMeasure
   :  public VscfBaseOptMeasure
{
   protected:
      In mBasNr;           ///> Identification number of the basis used
      
   public:
      using VscfBaseOptMeasure::measure_ptr_t;

      VscfOptMeasure(const input::RotCoordVscfCalcDef* const apCalcDef);
      virtual ~VscfOptMeasure() = default;

      std::pair<Nb, bool> RelEval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0) {return Eval(aQFirst,aQSecond,aAngle);}

      static measure_ptr_t Factory(const input::RotCoordVscfCalcDef* const apCalcDef);
};

#endif //VSCFOPTMEASURE_H
