/*
************************************************************************
*  
* @file                BaseOptMeasure.h
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles the optimization measure 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BASEOPTMEASURE_H
#define BASEOPTMEASURE_H
#include <string>
#include <utility>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/RotCoordCalcDef.h"

using namespace midas;

namespace midas::molecule
{
class MoleculeInfo;
} /* namespace midas::molecule */

/**
 *
 **/
class BaseOptMeasure
{
   public:
      using measure_ptr_t = std::unique_ptr<BaseOptMeasure>;

      //! Factory function
      static measure_ptr_t Factory(const input::RotCoordCalcDef* const arCalcDef, molecule::MoleculeInfo* arMolInfo); 
      
      //!@{
      //! Getters
      const std::string& GetName() const {return mMeasureType;}
      const std::string& GetUnit() const {return mMeasureUnit;}
      Nb GetCurrentValue()  const {return mCurrentValue;}
      //!@}
      
      void InitMeasure();
      void Update(In QFirst, In QSecond, Nb aAngle);
      virtual In GetNrOfModes() const = 0;
      virtual std::pair<Nb, bool> Eval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0) = 0;
      virtual std::pair<Nb, bool> RelEval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0) = 0;
      virtual void UpdateData(In QFirst, In QSecond, Nb aAngle) = 0;
      virtual void Finalize(const std::string&) = 0;
      virtual bool KnowsFrequencies() const = 0;
      virtual std::vector<Nb> GetCurrentFreqs() const = 0;
      virtual ~BaseOptMeasure() = default;

   protected:
      //! Descriptor for type of measure
      std::string mMeasureType = "";
      //! Unit of the measure
      std::string mMeasureUnit = "";
      //! Current value of the measure
      Nb mCurrentValue = C_0;
};

#endif //BASEOPTMEASURE_H
