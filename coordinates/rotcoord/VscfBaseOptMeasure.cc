/*
************************************************************************
*  
* @file                VscfBaseOptMeasure.cc
*
* Created:             08-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Vscf Measures 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "coordinates/rotcoord/VscfBaseOptMeasure.h"
#include "coordinates/rotcoord/VscfOptMeasure.h"
#include "coordinates/rotcoord/SimpleHoVscfOptMeasure.h"
#include "input/RotCoordCalcDef.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"

/**
* Constructor 
**/
VscfBaseOptMeasure::VscfBaseOptMeasure
   (  const input::RotCoordVscfCalcDef* const apCalcDef
   )
   :  mOperRotHandler(nullptr)
{ 
   mMeasureType="VSCF";
   mMeasureUnit="Hartree";
}

/**
 * Factory 
**/
typename VscfBaseOptMeasure::measure_ptr_t VscfBaseOptMeasure::Factory
   (  const input::RotCoordVscfCalcDef* const apCalcDef
   )
{
   switch(apCalcDef->GetCalcType())
   {
      case input::RotCoordVscfCalcType::GROUNDSTATE:
      case input::RotCoordVscfCalcType::STATEAVERAGE:
      {
         return VscfOptMeasure::Factory(apCalcDef);
         break;
      }
      case input::RotCoordVscfCalcType::HARMONICGROUNDSTATE:
      {
         return std::make_unique<SimpleHoVscfOptMeasure>(apCalcDef);
         break;
      }
      default:
      {
         MIDASERROR("Unknown method in VscfOptMeasure");
      }
   }
   return nullptr;
}

/**
*  Return the number of modes 
**/
In VscfBaseOptMeasure::GetNrOfModes() const
{
   return mOperRotHandler->GetNrOfModes();
}

/**
* Rotate operator and update current value  
**/
void VscfBaseOptMeasure::UpdateData(In aQFirst, In aQSecond, Nb aAngle)
{
   mOperRotHandler->TransformCurrOper(aQFirst, aQSecond, aAngle);
}

/**
* Finalize = Dump potential   
**/
void VscfBaseOptMeasure::Finalize(const std::string& arPostFix) 
{
   mOperRotHandler->DumpPotentialNew(arPostFix);
}

/**
 * Give current frequencies   
**/
std::vector<Nb> VscfBaseOptMeasure::GetCurrentFreqs() const
{
   if (gDebug || gIoLevel > I_8)
   {
      Mout << "  Update frequencies with those found in the rotated potential " << std::endl; 
   }

   return mOperRotHandler->GetQuasiFrequencies();
}

/**
* AUXILIARY FUNCTIONS   
**/

/**
 * Find VSCF input number   
**/
In FindVScfCalcNumber(const string& arStr)
{
   In calc_number = -I_1;
   for (In i = I_0; i < gVscfCalcDef.size(); ++i )
   {
      if (gVscfCalcDef[i].Prefix() == arStr)
      {
         calc_number = i;
         break;
      }
   }
   if (calc_number == -I_1)
   {
      MIDASERROR("Could not find VSCF input for the optimization procedure"); 
   }
   return calc_number;  
}


