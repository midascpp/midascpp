/*
************************************************************************
*  
* @file                HybridMeasure.cc
*
* Created:             12-03-2015
*
* Author:              Emil Lund Klinting (Klint@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), extension to multiple VSCF measures
*
* Short Description:   Contains the functions needed for utilizing the hybrid
*                      measure which combines localization and optimization
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "HybridMeasure.h"
#include "VscfBaseOptMeasure.h"
#include "LocMeasure.h"
#include "pes/molecule/MoleculeInfo.h"
#include "input/RotCoordCalcDef.h"
#include "libmda/numeric/float_eq.h"

/**
 * c-tor
 *
 * @param apCalcDef
 * @param apMolInfo
 **/
HybridMeasure::HybridMeasure
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* apMolInfo
   )
{
   // Init some base-class stuff
   mMeasureUnit = "";
   mMeasureType = "";

   // Init measures
   Nb tot_weight = C_0;
   auto n_opt_measures = apCalcDef->NOptMeasures();
   mOptMeasures.reserve(n_opt_measures);
   for(In i=I_0; i<n_opt_measures; ++i)
   {
      mOptMeasures.emplace_back
         (  std::make_pair
               (  apCalcDef->GetEvscfWeight(i)
               ,  VscfBaseOptMeasure::Factory(&apCalcDef->GetVscfCalcDef(i))
               )
         );

      mMeasureType += std::to_string(mOptMeasures.back().first) + " " + mOptMeasures.back().second->GetName() + " ";
      tot_weight += mOptMeasures.back().first;
   }

   // Init localization measure
   if (  apCalcDef->GetLocalize()
      )
   {
      mLocMeasure = std::make_pair(apCalcDef->GetLocWeight(), std::make_unique<LocMeasure>(apMolInfo));
      mMeasureType += std::to_string(mLocMeasure.first) + " " + mLocMeasure.second->GetName();
      tot_weight += mLocMeasure.first;
   }

   // Sanity checks
   MidasAssert(libmda::numeric::float_eq(tot_weight, C_1, 10), "Sum of weights should be 1 in HybridMeasure!");

   auto nmodes = mOptMeasures.front().second->GetNrOfModes();
   bool err = false;
   for(In i=I_1; i<mOptMeasures.size(); ++i)
   {
      if (  mOptMeasures[i].second->GetNrOfModes() != nmodes
         )
      {
         err = true;
         break;
      }
   }
   if (  mLocMeasure.second
      )
   {
      err = err || (mLocMeasure.second->GetNrOfModes() != nmodes);
   }
   if (  err
      )
   {
      MIDASERROR("Number of modes for different measures do not match!");
   }
}

/**
 * Evaluate measure
 *
 * @param aQFirst       First coordinate
 * @param aQSecond      Second coordinate
 * @param aAngle        Angle to rotate
 * @return
 *    pair of function value and bool (false if evaluation failed)
 **/
typename HybridMeasure::ret_t HybridMeasure::Eval
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )
{
   ret_t result = { C_0, true };
   for(auto& measure : mOptMeasures)
   {
      auto tmp_result = measure.second->Eval(aQFirst, aQSecond, aAngle);
      result.first += measure.first * tmp_result.first;
      result.second = result.second && tmp_result.second;
   }
   if (  mLocMeasure.second
      )
   {
      auto tmp_result = mLocMeasure.second->Eval(aQFirst, aQSecond, aAngle);
      result.first += mLocMeasure.first * tmp_result.first;
      result.second = result.second && tmp_result.second;
   }
   return result;
}
typename HybridMeasure::ret_t HybridMeasure::RelEval
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )
{
   return this->Eval(aQFirst, aQSecond, aAngle);
}

/**
 * Does the measure know frequencies
 *
 * @return
 *    True if _one_ VSCF measure is included
 **/
bool HybridMeasure::KnowsFrequencies
   (
   )  const
{
   return mOptMeasures.size() == 1;
}

/**
 * Frequencies
 *
 * @return
 *    Frequencies of the first VSCF optimization measure
 **/
std::vector<Nb> HybridMeasure::GetCurrentFreqs
   (
   )  const
{
   if (  mOptMeasures.size() == 1
      )
   {
      return mOptMeasures.front().second->GetCurrentFreqs();
   }
   else
   {
      MIDASERROR("HybridMeasure only knows frequencies if we have _one_ VSCF measure (otherwise not defined).");
      return std::vector<Nb>();
   }
}

/**
 *  Update data for all measures
 *
 *  @param aQFirst      First coordinate
 *  @param aQSecond     Second coordinate
 *  @param aAngle       Rotation angle
 **/
void HybridMeasure::UpdateData
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )
{
   for(auto& m : mOptMeasures)
   {
      m.second->UpdateData(aQFirst, aQSecond, aAngle);
   }
   if (  mLocMeasure.second
      )
   {
      mLocMeasure.second->UpdateData(aQFirst, aQSecond, aAngle);
   }
}

/**
 * Finalize
 *
 * @param aPostFix      Postfix for oper file
 **/
void HybridMeasure::Finalize
   (  const std::string& aPostFix
   )
{
   for(auto& m : mOptMeasures)
   {
      m.second->Finalize(aPostFix);
   }
   if (  mLocMeasure.second
      )
   {
      mLocMeasure.second->Finalize(aPostFix);
   }
}

/**
 * @return
 *    Number of modes
 **/
In HybridMeasure::GetNrOfModes
   (
   )  const
{
   if (  mLocMeasure.second
      )
   {
      return mLocMeasure.second->GetNrOfModes();
   }
   else if  (  !mOptMeasures.empty()
            )
   {
      return mOptMeasures.front().second->GetNrOfModes();
   }
   else
   {
      MIDASERROR("No measures in HybridMeasure. This should not happen!");
      return I_0;
   }
}
