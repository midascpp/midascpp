/*
************************************************************************
*  
* @file                SerGridMeasureFunction.cc
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the serial functions for the  
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<vector>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "coordinates/rotcoord/SerGridMeasureFunction.h"

/**
 * Calculate the gradient serially 
**/
void SerGridMeasureFunction::CalculateSerGradient
   (  MidasVector& aGradVect
   )  const
{
   In counter = I_0;
   if (mDoGridFreqScreen)
   {
      MidasAssert(mOptMeasure->KnowsFrequencies(),"Cannot Screen Modes if Frequencies not known");
      for (In j = I_0; j < mOptMeasure->GetNrOfModes(); ++j)
      {
         for (In k = j + I_1; k < mOptMeasure->GetNrOfModes(); ++k)
         {
            std::vector<Nb> curr_freqs = mOptMeasure->GetCurrentFreqs();
            if (  mDoGridFreqScreen
               && std::abs(curr_freqs[j] - curr_freqs[k]) > mGridFreqScreenMaxFreq 
               )
            {
               continue;
            }
            else
            {
               aGradVect[counter] = CalculateGradElement(j, k);
               ++counter;
            }
         }
      }
   }
   else
   {
      for (In j = I_0; j < mOptMeasure->GetNrOfModes(); ++j)
      {
         for (In k = j + I_1; k < mOptMeasure->GetNrOfModes(); ++k)
         {
            aGradVect[counter] = CalculateGradElement(j, k);
            ++counter;
         }
      }
      
   }
}

