/*
************************************************************************
*  
* @file                OperRotHandler.cc
*
* Created:             14-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Handles the rotation of potential operator
* 
* Rev. Date            17-12-2014 (C.Koenig)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<vector>
#include<map>
#include<algorithm>
#include<set>
#include <fstream>
#include "input/OpDef.h"
#include "mmv/MidasMatrix.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/Input.h"
#include "operator/OneModeOper.h"

/**
 *
**/
void OperRotHandler::GenerateNewOper
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   ,  OpDef& arOper
   )  const
{
   std::map<std::vector<GlobalModeNr>, Nb> new_oper;
   Transform(new_oper,aQFirst, aQSecond, aAngle);
   GiveOper(new_oper,arOper);
}

/**
 *
**/
void OperRotHandler::TransformCurrOper
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   ,  MidasVector aFreqScal
   )
{
   Transform(mCurrOper,aQFirst, aQSecond, aAngle);
   std::vector<GlobalModeNr> vec;
   if(mScaled)
   {
      mFrequencies = aFreqScal;
   }
}

/**
 *
**/
void OperRotHandler::TransformCurrOper
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )
{
   Transform(mCurrOper,aQFirst, aQSecond, aAngle);
   std::vector<GlobalModeNr> vec;
   if (mScaled)
   {
      for(In i = I_0; i < mOpDef->mScalFactors.size(); ++i)
      {
         vec.push_back(i);
         vec.push_back(i);
         std::map<std::vector<GlobalModeNr>, Nb>::const_iterator it = mCurrOper.find(vec);
         if (it!= mCurrOper.end())
         {
            if (it->second < C_0)
            {
               MidasWarning("Found a negative frequency: " + std::to_string(it->second) + ", will set this to a positive value before using it ", true);
            }
            mFrequencies[i] = std::sqrt(C_2 * std::fabs(it->second));
         }
         else
         {
            mFrequencies[i] = C_0;
         }
         vec.clear();
      }
   }
}

/**
 *
**/
std::pair<Nb, bool> OperRotHandler::TwoModeSimpleHoEnergyAfterRot
   (  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   ) const
{
   std::vector<GlobalModeNr> mode_vec(I_2,-I_1);
   mode_vec[I_0]=aQFirst;
   mode_vec[I_1]=aQFirst;
   MidasMatrix simple_2_mode_ho_pot(I_2,I_2,I_0);
   simple_2_mode_ho_pot[I_0][I_0] = C_2*mCurrOper.at(mode_vec);
   mode_vec[I_1]=aQSecond;
   std::sort(mode_vec.begin(),mode_vec.end());
   simple_2_mode_ho_pot[I_1][I_0] = mCurrOper.at(mode_vec);
   simple_2_mode_ho_pot[I_0][I_1] = mCurrOper.at(mode_vec);
   mode_vec[I_0]=aQSecond;
   mode_vec[I_1]=aQSecond;
   simple_2_mode_ho_pot[I_1][I_1] = C_2*mCurrOper.at(mode_vec);

   MidasMatrix rotation(I_2,I_2,I_0);
   rotation[I_0][I_0] = cos(aAngle);
   rotation[I_1][I_1] = cos(aAngle);
   rotation[I_1][I_0] = sin(aAngle);
   rotation[I_0][I_1] = -sin(aAngle);
  
   bool success = true;

   Nb contr = rotation[I_0][I_0] * simple_2_mode_ho_pot[I_0][I_0] * rotation[I_0][I_0] 
               + rotation[I_0][I_0] * simple_2_mode_ho_pot[I_0][I_1] * rotation[I_1][I_0] 
               + rotation[I_1][I_0] * simple_2_mode_ho_pot[I_1][I_0] * rotation[I_0][I_0]
               + rotation[I_1][I_0] * simple_2_mode_ho_pot[I_1][I_1] * rotation[I_1][I_0];
   contr = std::fabs(contr);
   if (contr < I_0)
   {
      success = false;
   }
   Nb simple_ho_en = std::sqrt(contr);
   
   contr = rotation[I_0][I_1] * simple_2_mode_ho_pot[I_0][I_0] * rotation[I_0][I_1]
            + rotation[I_0][I_1] * simple_2_mode_ho_pot[I_0][I_1] * rotation[I_1][I_1] 
            + rotation[I_1][I_1] * simple_2_mode_ho_pot[I_1][I_0] * rotation[I_0][I_1]
            + rotation[I_1][I_1] * simple_2_mode_ho_pot[I_1][I_1] * rotation[I_1][I_1];
   contr = std::fabs(contr);
   if (contr < I_0)
   {
      success = false;
   }
   simple_ho_en += std::sqrt(contr);
   simple_ho_en /=C_2;
   
   return std::make_pair(simple_ho_en,success);
}

void OperRotHandler::NaiveTransform
   (  map<vector<GlobalModeNr>, Nb>& arOpForTrans
   ,  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle)
{
   MidasMatrix rotation(I_3);
   rotation.Unit();
   rotation[aQFirst][aQFirst] = cos(aAngle);
   rotation[aQSecond][aQSecond] = cos(aAngle);
   rotation[aQFirst][aQSecond] = sin(aAngle);
   rotation[aQSecond][aQFirst] = -sin(aAngle);
   Mout << "Rotation matrix : " << std::endl;
   Mout << rotation << endl;
   for(map<vector<GlobalModeNr>, Nb>::iterator it = mCurrOper.begin(); it != mCurrOper.end();++it)
   {
      std::map<std::vector<GlobalModeNr>, Nb> store;
      std::map<std::vector<GlobalModeNr>, Nb> store3;
      store.insert(std::make_pair(it->first,it->second));
      for(In i = 0; i < it->first.size(); ++i)
      {
         std::map<std::vector<GlobalModeNr>, Nb> store2;
         for(auto it2 = store.begin(); it2 != store.end(); ++it2)
         {
            for(In j = 0; j < 3; ++j)
            {
               if(it2->first.size() < i+1)
               {
                  std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted =
                     store3.insert(std::make_pair(it2->first,it2->second));
                  if(!poss_inserted.second)
                     poss_inserted.first->second += rotation[it->first[i]][j]*it2->second;
               }
               std::vector<GlobalModeNr> modes = it2->first;
               modes[i] = j;
               //Mout << "Rotating : " << it2->first << " into " << modes << endl;
               //Mout << "Coeff    : " << it2->second << " into " << rotation[it->first[i]][j] << " * " << it2->second << " = " << rotation[it->first[i]][j]*it2->second << endl;
               std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted = 
                     store2.insert(std::make_pair(modes,rotation[it->first[i]][j]*it2->second));
               if(!poss_inserted.second)
                  poss_inserted.first->second += rotation[it->first[i]][j]*it2->second;
            }
         }
         store.clear();
         for(auto it2 = store2.begin(); it2 != store2.end(); ++it2)
         {
            std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted =
                     store.insert(std::make_pair(it2->first,it2->second));
            if(!poss_inserted.second)
               poss_inserted.first->second += it2->second;
         }
         
      }
      for(auto it2 = store.begin(); it2 != store.end(); ++it2)
      {
         std::vector<GlobalModeNr> mode = it2->first;
         std::sort(mode.begin(),mode.end());
         std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted =
                  arOpForTrans.insert(std::make_pair(mode,it2->second));
         if(!poss_inserted.second)
            poss_inserted.first->second += it2->second;
      }
      for(auto it2 = store3.begin(); it2 != store3.end(); ++it2)
      {
         std::vector<GlobalModeNr> mode = it2->first;
         std::sort(mode.begin(),mode.end());
         std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted =
                  arOpForTrans.insert(std::make_pair(mode,it2->second));
         if(!poss_inserted.second)
            poss_inserted.first->second += it2->second;
      }
   }
}

void OperRotHandler::Transform
   (  std::map<std::vector<GlobalModeNr>, Nb>& arOpForTrans
   ,  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   )  const 
{
   //Transform the operator in the class and store it on the mCurrOperMap
   arOpForTrans = mCurrOper;
   std::map<std::vector<GlobalModeNr>, Nb> work_map;
   std::map<std::vector<GlobalModeNr>, Nb> next_map;
   std::map<std::vector<GlobalModeNr>, Nb> finished_map;
   for(auto it = arOpForTrans.begin(); it != arOpForTrans.end();)
   {  
      if (  std::find(it->first.begin(),it->first.end(),aQFirst) != it->first.end()
         || std::find(it->first.begin(),it->first.end(),aQSecond) != it->first.end()
         )
      {
         work_map.insert(std::make_pair(it->first,it->second));
         it = arOpForTrans.erase(it);
      }
      else
      {
         ++it;
      }
   }
   /*Mout << "Oper is now (Inside transform): " << endl;
   for(map<vector<GlobalModeNr>, Nb>::iterator it = arOpForTrans.begin(); it != arOpForTrans.end(); ++it)
      Mout << it->first << " : " << it->second << endl;*/
   for(In i = 0; i < mOrder; ++i)
   {
      next_map.clear();
      for(auto it = work_map.begin(); it != work_map.end(); ++it)
      {
         if(it->first.size() == i+1)
         {
            //Mout << "Going to oper" << endl;
            TransformElement(finished_map, it, aQFirst, aQSecond, aAngle, i);
         }
         else
         {
            //Mout << "Going to next iter" << endl;
            TransformElement(next_map, it, aQFirst, aQSecond, aAngle, i);
         }
      }
      work_map.swap(next_map);
      /*Mout << "Oper is now (Inside transform) sweep " << i << " : " << endl;
      for(map<vector<GlobalModeNr>, Nb>::iterator it = arOpForTrans.begin(); it != arOpForTrans.end(); ++it)
         Mout << it->first << " : " << it->second << endl;*/
   }
   for(auto it = finished_map.begin(); it != finished_map.end(); ++it)
   {
      std::vector<GlobalModeNr> code = it->first;
      std::sort(code.begin(),code.end());
      std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted = arOpForTrans.insert(std::make_pair(code,it->second));
      if(!poss_inserted.second)
         poss_inserted.first->second += it->second;
   }
}


void OperRotHandler::TransformElement
   (  std::map<std::vector<GlobalModeNr>, Nb>& arDestination
   ,  std::map<std::vector<GlobalModeNr>, Nb>::iterator aElement
   ,  In aQFirst
   ,  In aQSecond
   ,  Nb aAngle
   ,  In aPoss
   )  const
{
   if(aElement->first[aPoss] == aQFirst)
   {
      std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted = arDestination.insert(std::make_pair(aElement->first,cos(aAngle)*aElement->second));
      if(!poss_inserted.second)
         poss_inserted.first->second += std::cos(aAngle)*aElement->second;
      std::vector<GlobalModeNr> mode_vector = aElement->first;
      mode_vector[aPoss] = aQSecond;
      if (IncludeTerm(mode_vector))
      {
        //Mout << "Inserting : " << mode_vector << ", " << sin(aAngle)*aElement->second << endl;
        poss_inserted = arDestination.insert(std::make_pair(mode_vector,-sin(aAngle)*aElement->second));
        if(!poss_inserted.second)
           poss_inserted.first->second -= std::sin(aAngle)*aElement->second;
      }
   }
   else if(aElement->first[aPoss] == aQSecond)
   {
      std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted = arDestination.insert(std::make_pair(aElement->first,cos(aAngle)*aElement->second));
      if(!poss_inserted.second)
         poss_inserted.first->second += cos(aAngle)*aElement->second;
      std::vector<GlobalModeNr> mode_vector = aElement->first;
      mode_vector[aPoss] = aQFirst;
      if (IncludeTerm(mode_vector))
      {
         //Mout << "Inserting : " << mode_vector << ", " << -sin(aAngle)*aElement->second << endl;
         poss_inserted = arDestination.insert(std::make_pair(mode_vector,std::sin(aAngle)*aElement->second));
         if(!poss_inserted.second)
            poss_inserted.first->second += std::sin(aAngle)*aElement->second;
      }
   }
   else
   {
      std::pair<std::map<std::vector<GlobalModeNr>, Nb>::iterator, bool> poss_inserted = arDestination.insert(std::make_pair(aElement->first,aElement->second));
      if(!poss_inserted.second)
         poss_inserted.first->second += aElement->second;
   }
}

