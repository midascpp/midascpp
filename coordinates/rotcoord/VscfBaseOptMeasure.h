/*
************************************************************************
*  
* @file                VscfBaseOptMeasure.h
*
* Created:             08-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles Vscf for the optimization procedure
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef VSCFBASEOPTMEASURE_H
#define VSCFBASEOPTMEASURE_H
#include<memory>


#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "input/VscfCalcDef.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "coordinates/rotcoord/BaseOptMeasure.h"

using namespace midas;

/**
 *
 **/
class VscfBaseOptMeasure
   :  public BaseOptMeasure
{
   protected:
      In                                     mCalcNo;
      In                                     mOperNo;
      VscfCalcDef*                           mCalcDef = nullptr;  ///> Settings for the VSCF calculations
      std::unique_ptr<OperRotHandler>        mOperRotHandler;     ///> Contains the operator and handles its rotation, etc
      
   public:
      using BaseOptMeasure::measure_ptr_t;

      VscfBaseOptMeasure(const input::RotCoordVscfCalcDef* const apCalcDef);
      static measure_ptr_t Factory(const input::RotCoordVscfCalcDef* const apCalcDef);
      In GetNrOfModes() const override;
      void UpdateData(In QFirst,In QSecond, Nb aAngle) override;
      void Finalize(const std::string&) override;
      bool KnowsFrequencies() const override {return true;}
      std::vector<Nb> GetCurrentFreqs() const override;
      virtual ~VscfBaseOptMeasure() = default;
};

#endif //VSCFOPTMEASURE_H
