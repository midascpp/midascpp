/**
************************************************************************
* 
* @file                SaVscfOptMeasure.h
*
* Created:             12-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   class calculating the state average VSCF energy for  
*                      Based in StateAverageCalculationForOpt by Bo Thomsen
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


#include <string>
#include <utility>
#include <iostream>
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "vscf/Vscf.h"
#include "input/VscfCalcDef.h"
#include "input/BasDef.h"
#include "input/Input.h"
#include "ni/OneModeInt.h"
#include "coordinates/rotcoord/SaVscfOptMeasure.h"
#include "mpi/Interface.h"

/**
* Set up and do the actual calcualtion   
**/
std::pair<Nb, bool> SaVscfOptMeasure::CalcSaVscfEnergy(In aQFirst, In aQSecond, Nb aAngle)
{
   if (!gDebug) Mout.Mute();
   OpDef new_oper;
   mOperRotHandler->GenerateNewOper(aQFirst, aQSecond, aAngle, new_oper);
   BasDef basis(gBasis[mBasNr]);
   basis.InitBasis(new_oper);
   std::string mpi_str = midas::mpi::get_Interface().GetMpiTreeString();
   new_oper.SetName("Rotated_oper"+mpi_str);
   basis.SetmBasName("Rotated_basis_rank"+mpi_str);
   OneModeInt one_mode_int(&new_oper,&basis,"InMem",false);
   one_mode_int.CalcInt();
   VscfCalcDef calc_def(*mCalcDef);
   calc_def.SetName("Rotated_rank"+mpi_str);
   Vscf new_vscf(&calc_def,&new_oper,&basis,&one_mode_int);
   new_vscf.StartGuess();
   new_vscf.Solve();
   new_vscf.Finalize();
   if (!gDebug) Mout.Unmute();
   return std::make_pair(calc_def.GetEfinalStateAverage(),new_vscf.Converged());
}
