/*
************************************************************************
*  
* @file                RotCoordBase.h
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Handles the rotation of coordinates 
*                      based on the optimized coordinates class written
*                      by Bo Thomsen
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ROTCOORD_H
#define ROTCOORD_H
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "coordinates/rotcoord/BaseMeasureFunction.h"
#include "coordinates/rotcoord/RotCoordDrv.h"

namespace midas::molecule
{
class MoleculeInfo;
} /* namespace midas::molecule */

class BaseMeasureFunction;

using namespace midas;

/**
 *
 **/
class RotCoordBase
{
   public:
      using rotcoord_ptr_t = std::unique_ptr<RotCoordBase>;

      RotCoordBase(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* arMolInfo);

      static rotcoord_ptr_t Factory(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* arMolInfo);

      virtual ~RotCoordBase() = default;

      void Preparations();
      virtual void DoOpt() = 0;

      void PlotAllRotations() {mMeasureFunction->PlotAllRotations();}

   protected:
      const input::RotCoordCalcDef* const    mpCalcDef;          ///< contains all the input information
      molecule::MoleculeInfo*                mpMolInfo;          ///< carries molecule informations 
      In                                     mIoLevel;           ///< Sets Io Level for RotCoord
      In                                     mNrOfModes;         ///< Number of modes
      In                                     mSpaceSize;         ///< Total number of unique mode pairs
      MidasMatrix                            mCurrentRotMatrix;  ///< Current rotation matrix
      Nb                                     mInitValue;         ///< Initial value of the optimization measure
      std::unique_ptr<BaseMeasureFunction>   mMeasureFunction;   ///< Handles the measure
      std::vector<OperRotHandler>            mOpersForRot;       ///< Takes operators to rotate along !First has to be an energy operator!

      void UpdRotMat(Nb aAngle, In aQFirst, In aQSecond);
      bool CheckIsConv(In aIt);
      void Finalize(const string&);
      void Summarize(const In& aIt);
      void DumpCoordinateFile(const std::string&) const;
      void DumpRotMatrix() const;
};
#endif //ROTCOORD_H
