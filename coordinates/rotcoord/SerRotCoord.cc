/*
************************************************************************
*  
* @file                SerRotCoord.cc
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig(ckonig@chem.au.dk)
*
* Short Description:   Handles the optimization of coordinates by finding 
*                      the minima with respect to rotation sets
*                      of coordinates only for serial algorithm
*                      based on VscfSerOPtCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <complex>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <random>

// Midas header
#include "coordinates/rotcoord/SerRotCoord.h"
#include "mmv/MidasMatrix.h"

/**
 *
**/
SerRotCoord::SerRotCoord
   (  const input::RotCoordCalcDef* const apCalcDef
   ,  molecule::MoleculeInfo* apMolInfo 
   ) 
   :  RotCoordBase
      (  apCalcDef
      ,  apMolInfo
      )
{
   if (mpCalcDef->GetRandomIter() != I_0)
   {  
      DoRandomRotations();
   }
}

/**
 * Does a series of random rotations on the operators before doing the calculation
**/ 
void SerRotCoord::DoRandomRotations()
{
   for (In i = I_0; i < mpCalcDef->GetRandomIter(); ++i)
   {
      std::random_device random;
      for (In j = I_0; j < mNrOfModes; ++j)
      {
         for (In k = j + I_1; k < mNrOfModes; ++k)
         {
            Nb rand_ang = 0.25*C_PI*((Nb(random()) - Nb(random.max()/I_2))/Nb(random.max()));
            
            if (gIoLevel > I_5)
            {
               Mout << "  Updating operator by random rotation of angle " << rand_ang << " for modes " << j << "," << k << std::endl;
            }

            UpdRotMat(rand_ang, j, k);
         }
      }
   }
   Finalize("_rand");
}

/**
 * Serial algorithm for optimizing the coordinates
**/
void SerRotCoord::DoOpt()
{
   In iter = I_0;
   while (!CheckIsConv(iter))
   {
      for (In j = I_0; j < mNrOfModes; ++j)
      {
         for (In k = j + I_1; k < mNrOfModes; ++k)
         {
            Nb angle;
            bool angle_set=false;
            if (mpCalcDef->GetDoFreqScreen())
            {
               MidasAssert(mMeasureFunction->KnowsFrequencies()," Cannot screen modes after frequencies, when frequencies are unknown..");
               std::vector<Nb> curr_freqs = mMeasureFunction->GetCurrentFreqs();
               if (std::abs(curr_freqs[j] - curr_freqs[k]) > mpCalcDef->GetFreqScreenMaxFreq()/C_AUTKAYS)
               {
                  if (gDebug || gIoLevel > I_8)
                  {
                     Mout << " The difference in frequency between modes " << j << "," << k << " is larger than " << mpCalcDef->GetFreqScreenMaxFreq() << "! The rotation angle is set to zero to impose no rotation between these modes!" << std::endl;
                  }

                  angle = C_0;
                  angle_set=true;
               }
            }

            if (!angle_set)
            {
               angle = mMeasureFunction->GetAngle(j, k, iter);
            }

            if (std::fabs(angle) > mpCalcDef->GetMinRotAng())
            {
               if (gIoLevel > I_5)
               {
                  Mout << "  Updating operator by rotation of angle " << angle << " for modes " << j << "," << k << std::endl;
               }

               UpdRotMat(angle, j, k);
            }
         }
      }
      ++iter;
   }

   if (gDebug || gIoLevel > I_8)
   {
      Mout << std::endl << " End of coordinate rotation! Procedure done after " << iter << " iterations " << std::endl;
   }
   Finalize(mpCalcDef->GetNewSuffix());
   Summarize(iter);
}

