/*
************************************************************************
*  
* @file                GridMeasureFunction.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the type of function for finding 
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomseb
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef GRIDMEASUREFUNCTION_H
#define GRIDMEASUREFUNCTION_H

#include<vector>
#include<complex>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "input/RotCoordCalcDef.h"
#include "coordinates/rotcoord/BaseMeasureFunction.h"

using namespace midas;

/**
 *
 **/
class GridMeasureFunction
   :  public BaseMeasureFunction
{
   private:
      const input::RotCoordGridCalcDef* const mpGridCalcDef;
      In mFourierPts;                  ///< Number of Fourier points

      void GenerateFourierCoeff(std::vector<std::complex<Nb> >&, const MidasVector&, const MidasVector&) const;
      Nb CalculateFourierPt(const std::vector<std::complex<Nb> >&, Nb) const;
      Nb GetAngleOnGrid(In, In, In aIter = I_0) const;
      std::pair<bool,Nb> ConvCritOnGrid(In aIter = I_0) ;

   protected:
      In   mSpaceSize;
      bool mDoGridFreqScreen;            ///< Do frequency screening 
      Nb   mGridFreqScreenMaxFreq;       ///< Max screening frequnecy
      
      Nb GenGridCalcMin(In, In) const;
      Nb NewtonStepper(Nb, In, In) const;
      bool CalcGradHess(Nb, Nb&, Nb&, In, In) const;
      Nb CalculateGradElement(In aQFirst, In aQSecond) const;
      void CreatePlotFiles(In aSweepNr) const;
      void PlotRotation(In aQFirst, In aQSecond, In aSweepNr) const;
   
   public:
      using BaseMeasureFunction::func_ptr_t;

      GridMeasureFunction(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* arMolInfo);
      static func_ptr_t Factory(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* apMolInfo);
      virtual void CalculateGradient(MidasVector&) const = 0; //Calculates the gradient vector
      ~GridMeasureFunction() 
      {
      }
      std::pair<bool,Nb> ConvCrit(In aIter) {return ConvCritOnGrid(aIter);}
      Nb GetAngle(In aQFirst, In aQSecond, In aIter = I_0) const {return GetAngleOnGrid(aQFirst, aQSecond, aIter);}
      void PlotAllRotations() const;
};
#endif //GRIDMEASUREFUNCTION_H
