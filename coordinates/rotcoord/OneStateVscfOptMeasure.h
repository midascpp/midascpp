/**
************************************************************************
* 
* @file                OneStateVscfOptMeasure.h
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Runs the one state VSCF calculations 
*                      based on VscfCalculationForOpt.h by 
*                      Bo Thomsen           
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONESTATEVSCFOPTMEASURE_H
#define ONESTATEVSCFOPTMEASURE_H
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/VscfOptMeasure.h"

using namespace midas;

class OneStateVscfOptMeasure
   :  public VscfOptMeasure
{
   private:
      std::pair<Nb, bool> CalcVscfEnergy(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0);

   public:
      OneStateVscfOptMeasure(const input::RotCoordVscfCalcDef* const apCalcDef)
         :  VscfOptMeasure(apCalcDef) 
      {
         mMeasureType = "One State "+mMeasureType ;
      }

      std::pair<Nb, bool> Eval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0) 
      {
         return CalcVscfEnergy(aQFirst, aQSecond, aAngle);
      }

      virtual ~OneStateVscfOptMeasure() = default;
};
#endif //ONESTATEVSCFOPTMEASURE_H
