/**
************************************************************************
*  
* @file                OperTransLinCombHandler.h
*
* Created:             04-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles linear combinations of modes in a potential also 
*                      leading to nonorthonormal and/or redundant bases
* 
* Rev. Date            
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef OPERTRANSLINCOMBHANDLER_H
#define OPERTRANSLINCOMBHANDLER_H

#include <vector>
using std::vector;
#include <map>
using std::map;
#include "coordinates/rotcoord/OperTransformHandler.h"


class OperTransLinCombHandler
   : public OperTransformHandler
{
   private:
      void AddLinComb(const std::vector<string>& arOldModeNames, const MidasVector& arCoeffVec,const string& arNewName);
      void RemoveModes(const std::vector<string>& arModeNames);
      OperTransLinCombHandler():OperTransformHandler() {;}
   public:
      OperTransLinCombHandler(OpDef* apOpDef,const std::vector<string>& arModeNames, bool arKeepMaxCoupLevel = false): OperTransformHandler(apOpDef,arKeepMaxCoupLevel) {mModeNames=arModeNames;}

      //!
      void TransformCurrOper
         (  const MidasMatrix& arRotMat
         ,  const std::vector<string>& arOldModeNames
         ,  const std::vector<string>& arNewModeNames
         ,  const bool& arKeepOldModes = false
         );

      //!
      friend class TransformPot;
};
#endif //OPERTRANSLINCOMBHANDLER_H
