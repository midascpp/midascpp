/*
************************************************************************
*  
* @file                ParRotCoord.h
*
* Created:             11-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the rotatoin coordinates 
*                      of coordinates only for parallel algorithm, including MPI
*                      Based on VscfParOptCoord by Bo Thomsen. 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef PARROTCOORD_H
#define PARROTCOORD_H
#include <vector>
#include <utility>
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/RotCoordBase.h"
#include "pes/molecule/MoleculeInfo.h"

using namespace midas;

/**
 * Class for parallel coordinate rotation
 **/
class ParRotCoord
   :  public RotCoordBase
{
   private:
      Nb CalcMin(In aI, In aIter) const;

      void DoRandomRotations();
      void RemoveCoupledRotations(MidasVector& arRotations) const;
   public:
      void DoOpt();
      ParRotCoord(const input::RotCoordCalcDef* const apCalcDef, molecule::MoleculeInfo* arMolInfo);

      ParRotCoord& operator=(const ParRotCoord&) = delete;
      ParRotCoord(const ParRotCoord&) = delete;
      ParRotCoord() = delete;
      ~ParRotCoord() = default;
};
#endif //PARROTCOORD_H
