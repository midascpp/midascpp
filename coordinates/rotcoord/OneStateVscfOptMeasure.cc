/*
************************************************************************
*  
* @file                OneStateVscfOptMeasure.cc
*
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Vscf Measures 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <utility>

#include "coordinates/rotcoord/OneStateVscfOptMeasure.h"
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/OperRotHandler.h"
#include "vscf/Vscf.h"
#include "input/VscfCalcDef.h"
#include "input/BasDef.h"
#include "ni/OneModeInt.h"
#include "input/Input.h"
#include "mpi/Interface.h"

/**
 * Set up and do the actual calcualtion   
**/
std::pair<Nb, bool> OneStateVscfOptMeasure::CalcVscfEnergy(In aQFirst, In aQSecond, Nb aAngle)
{
   // We mute the Mout stream to avoid unessesary output
   if (!gDebug)
   {
      Mout.Mute();
   }
   OpDef new_oper;
   // Generate the operator
   mOperRotHandler->GenerateNewOper(aQFirst, aQSecond, aAngle, new_oper);
   // Create a basis using the settings from the old basis settings
   BasDef basis(gBasis[mBasNr]);
   //Initialize the basis, ie set frequencies for the HO's and so forth
   basis.InitBasis(new_oper);
   // Get a string which makes the following string unique for each seperate mpi processes
   std::string mpi_str = midas::mpi::get_Interface().GetMpiTreeString();
   //Set unique names for the different processes
   new_oper.SetName("Rotated_oper" + mpi_str);
   basis.SetmBasName("Rotated_basis_rank" + mpi_str);
   // Create and calculate the new integrals
   OneModeInt one_mode_int(&new_oper,&basis,"InMem",false);
   one_mode_int.CalcInt();
   //Copy the calc def to a new calc def
   VscfCalcDef calc_def(*mCalcDef);
   //Dont save anything, we don't really need to restart here so no reason to do it
   calc_def.SetSave(false);
   calc_def.SetName("Rotated_rank"+mpi_str);
   //Create a new vscf calculation using all the setup done so far
   Vscf new_vscf(&calc_def,&new_oper,&basis,&one_mode_int);
   //Do the vscf calculation
   new_vscf.StartGuess();
   new_vscf.Solve();
   //Unmute Mout so we can get output from hereon out...
   Mout.Unmute();
   //Return the energy and a bool telling if the calculation converged
   return std::make_pair(new_vscf.GetEtot(),new_vscf.Converged());
}
