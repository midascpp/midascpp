/*
************************************************************************
*  
* @file                HessianTransformer.h
*
* Created:             15-07-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Hessian transformer.
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef HESSIANTRANSFORMER_H_INCLUDED
#define HESSIANTRANSFORMER_H_INCLUDED

#include<memory>
#include<vector>
#include"coordinates/freqana/FreqAna.h"
#include"mmv/DataCont.h"

class HessianTransformer
{
   private:
      const FreqAna* const mpFreqAna;
      const In             mDim;

   public:
      //Constructors etc.:
      explicit HessianTransformer(const FreqAna* const apFreqAna)
         : mpFreqAna(apFreqAna)
         , mDim(apFreqAna->VectorDimension())
      {
      }

      void Transform(DataCont& arDcIn, DataCont& arDcOut, const In& dummy_a = I_0, const In& dummy_b = I_1, const Nb& dummy_c = C_0) const;
         //^Dummy arguments are there for compliance with the iterative equation solvers in 
         //it_solver/

      In Dim() const 
      {
         return mDim;
      }

      DataCont TemplateZeroVector() const
      {
         return DataCont(mDim, C_0);
      }
};

#endif //HESSIANTRANSFORMER_H_INCLUDED
