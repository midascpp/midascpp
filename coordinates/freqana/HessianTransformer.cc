#include"coordinates/freqana/HessianTransformer.h"
#include"mmv/DataCont.h"
#include"mmv/MidasMatrix.h"

void HessianTransformer::Transform(DataCont& arDcIn, DataCont& arDcOut, const In& dummy_a, const In& dummy_b, const Nb& dummy_c) const
{
   //Extract vector(s) to be transformed from DataCont arDcIn and store in matrix.
   MidasMatrix mat_in(I_1, Dim());
   arDcIn.DataIo(IO_GET, mat_in, mat_in.Nrows()*mat_in.Ncols(), mat_in.Nrows(), mat_in.Ncols());

   //Mass-weight:
   for(In i=I_0; i<mat_in.Ncols(); ++i)
   {
      mat_in[I_0][i] /= sqrt(mpFreqAna->MolInfo().GetNuclMassi(i/I_3));
   }

   //Transform (row) vectors of the matrix with transformer of FreqAna.
   MidasMatrix mat_out = mpFreqAna->HessianTransformation(mat_in);

   //Mass-weight:
   for(In i=I_0; i<mat_out.Nrows(); ++i)
   {
      mat_out[i][I_0] /= sqrt(mpFreqAna->MolInfo().GetNuclMassi(i/I_3));
   }

   //Put result in DataCont arDcOut.
   arDcOut.DataIo(IO_PUT, mat_out, mat_out.Nrows()*mat_out.Ncols(), mat_out.Nrows(), mat_out.Ncols());
}
