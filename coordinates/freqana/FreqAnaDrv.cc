/**
************************************************************************
*
* @file                FreqAnaDrv.cc
*
* Created:             21-04-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Driver for Frequency Analysis

* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers

// midas headers
#include "FreqAna.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/FreqAnaCalcDef.h"
#include "it_solver/EigenvalueSolver.h"

//MBH-NB!
//Testing:
#include <random>
#include "coordinates/freqana/HessianTransformer.h"
#include "it_solver/EigenvalueSolver.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

/**
* Run frequency analysis
* */
void FreqAnaDrv(molecule::MoleculeInfo& arMolInfo, const input::FreqAnaCalcDef& arCalcDef)
{
   Mout << endl << endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin Frequency Analysis ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << endl << endl;

   FreqAna freqana(&arMolInfo, &arCalcDef);
   freqana.Preparations();

   if(!arCalcDef.GetTargeting()) //Full frequency analysis.
   {
      freqana.DoFreqAna();
   }
   else  //Do targeting.
   {
      //Set up transformer, then solver:
      Mout << "Frequency analysis with targeting; setting up transformer and solver." << std::endl;
      HessianTransformer hessian_trans(&freqana);
      TargetingHessianNoPrecondEigenvalueSolver solver(hessian_trans);

      //Set targets (also handles mass-weighting and normalization):
      Mout << "Frequency analysis with targeting; initializing solver." << std::endl;
      solver.Targets()           = freqana.TargetsFromMolInfo();
      solver.EigenvalueEpsilon() = arCalcDef.GetItEqEnerThr();
      solver.ResidualEpsilon()   = arCalcDef.GetItEqResThr();
      solver.BreakDim()          = arCalcDef.GetItEqBreakDim();
      solver.OverlapSumMin()     = arCalcDef.GetOverlapSumMin();
      solver.OverlapNMax()       = arCalcDef.GetOverlapNMax();
      solver.SetMaxIter(arCalcDef.GetItEqMaxIt());
      solver.SetNeq(solver.Targets().size());
      solver.Initialize();

      //Happy solving!
      Mout << "Frequency analysis with targeting; calling solver." << std::endl;
      Mout << std::endl;

      bool converged = solver.Solve();

      Mout << std::endl;
      Mout << "Frequency analysis with targeting; returning from solver." << std::endl;

      if(!converged)
      {
         MidasWarning("It.eq.sol. not converged! Writing results from last iteration to MolInfo.");
      }

      //Extract results:
      auto& eigvals = solver.Eigenvalues();
      auto& eigvecs = solver.SolVec();
         //Type of SolVec:       StandardContainer<SOL_T>
         //Type of Eigenvalues:  EigenvalueContainer<MidasVector, MidasVector>
         //StandardContainer<T> is basically an std::vector<T>.
         //For TargetingHessianEigenvalueSolver:
         //    SOL_T = DataCont.

      //Write to molecule info, then to file:
      Mout << "Frequency analysis with targeting; writing results to MoleculeInfo file." << std::endl;
      freqana.WriteEigenDataToMolInfo(eigvals, eigvecs, false, true);
      freqana.WriteMoleculeFile(freqana.CalcDef().GetMolInfoOutName());
      freqana.WriteMoleculeFile(freqana.CalcDef().GetMolInfoOutName()+".molden","MOLDEN"); // Now we also get a Molden file whenever we do a frequency analysis
      freqana.WriteMoleculeFile(freqana.CalcDef().GetMolInfoOutName()+".minfo","SINDO"); // Now we also get a Sindo file whenever we do a frequency analysis

      Mout << "Frequency analysis with targeting; end." << std::endl;
   }
}
