/**
************************************************************************
* 
* @file                TransformPotDrv.cc
*
* Created:             07-12-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Driver for Potential transformation (polynomial) 
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/TransformPotCalcDef.h"
#include "coordinates/transformpot/TransformPot.h"

/**
* Run coordinate rotations
* */
void TransformPotDrv(input::TransformPotCalcDef* apTransformPotCalcDef,System* apSystem)
{

   Mout << endl << endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin coordinate transformation of the potential ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << endl << endl;

   TransformPot trans_pot(apTransformPotCalcDef,apSystem); 
  
   trans_pot.DoTransform(); 
}
