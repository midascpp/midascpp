/**
************************************************************************
* 
* @file                TransformPot.h
*
* Created:             07-12-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Potential transformation (polynomial) 
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include<memory>
// midas headers
#include "input/TransformPotCalcDef.h"
#include "system/System.h"

class TransformPot
{
   private:
     
     const input::TransformPotCalcDef*    mpCalcDef;
     const System*                        mpFinalSystem;
     TransformPot() = delete;
   
   public:
     
      //!
      TransformPot
         (  const input::TransformPotCalcDef* apCalcDef
         ,  const System* apSystem
         )
         :  mpCalcDef(apCalcDef)
         ,  mpFinalSystem(apSystem)
      {
      }

      //!
      void DoTransform() const;

      //!
      MidasMatrix SetUpRotationMatrix(const molecule::MoleculeInfo& arMolInfoFinal, const molecule::MoleculeInfo& arMolInfoRef) const;
     
};
