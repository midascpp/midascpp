/**
************************************************************************
* 
* @file                TransformPot.cc
*
* Created:             07-12-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Potential transformation (polynomial) 
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "coordinates/transformpot/TransformPot.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"
#include "coordinates/rotcoord/OperTransLinCombHandler.h"
#include "input/InputOperFromFile.h"
#include "input/OpDef.h"

/**
 *
**/
void TransformPot::DoTransform
   (
   )  const
{
   System modified_final_system=*mpFinalSystem; 
   modified_final_system.RemoveTransRotContrNotOrthonorm(modified_final_system.GetGlobalSubSysSet()); 
 
   molecule::MoleculeInfo mol_modified_final(modified_final_system,modified_final_system.GetGlobalSubSysSet(),modified_final_system.GetGlobalModeSet());
   molecule::MoleculeInfo mol_ref(mpCalcDef->GetInMolFileInfo());
 
   MidasMatrix rot_mat=SetUpRotationMatrix(mol_modified_final,mol_ref);


   // check rotation 
   MidasMatrix ref_modes = mol_ref.NormalCoordMw();

   In n=I_0;
   while (n < I_3*mol_ref.GetNumberOfNuclei())
   {
      In i=I_0;
      while ((i < rot_mat.Ncols())&& n < I_3*mol_ref.GetNumberOfNuclei()) 
      {
         MidasVector vec_rot(rot_mat.Ncols(),C_0);
         rot_mat.GetRow(vec_rot,i);
         MidasVector vec_ref(ref_modes.Nrows(),C_0);
         ref_modes.GetCol(vec_ref,n);
         Nb dot=Dot(vec_rot,vec_ref);
         MidasVector vec_final_modified(I_3*mol_modified_final.GetNumberOfNuclei(),C_0);
         mol_modified_final.GetNormalModeMw(vec_final_modified,i);
         MidasAssert((dot - vec_final_modified[n]) < C_I_10_10, " Something went wrong in finding the rotation matrix");
         ++i;
      }
      ++n;
   }

   // do potential transformation
   std::vector<std::string> mode_tags_final=mol_modified_final.GetModeNames();
   std::vector<std::string> mode_tags_ref=mol_ref.GetModeNames();

   OpDef op_def;
   op_def.SetOpFile(mpCalcDef->GetOperatorFileName());
   op_def.InitLastOper(mpCalcDef->GetOperatorFileName(), C_1, -I_1, -I_1, -I_1,false);
   midas::input::InputOperFromFile(op_def, I_0, C_0);
   op_def.InitOpRange();  
   op_def.Reorganize(true);
   op_def.CleanOper();

   OperTransLinCombHandler op_trans(&op_def,mode_tags_ref,mpCalcDef->GetKeepMaxCoupLevel()); 

   op_trans.TransformCurrOper(rot_mat,mode_tags_ref,mode_tags_final);
   op_trans.DumpPotentialNew("_new");

   // check the backtransformation
   MidasMatrix rot_mat2=InvertGeneralNonSquare(rot_mat);
   
   op_trans.TransformCurrOper(rot_mat2,mode_tags_final,mode_tags_ref);

   for (In i = I_0; i < mol_ref.GetNoOfVibs() ; ++i) 
      op_trans.SetFreq(mol_ref.GetModeNames().at(i),mol_ref.GetFreqI(i)/C_AUTKAYS);

   op_trans.DumpPotentialNew("_back");
   
}

/**
 *
**/
MidasMatrix TransformPot::SetUpRotationMatrix
   (  const molecule::MoleculeInfo& arMolInfoFinal
   ,  const molecule::MoleculeInfo& arMolInfoRef
   )  const
{
   MidasAssert(arMolInfoFinal.GetNumberOfNuclei()==arMolInfoRef.GetNumberOfNuclei()," Number of nuclei not the same in rotation matrix"); 
   MidasAssert(arMolInfoFinal.GetCoord()==arMolInfoRef.GetCoord()," Nuclei not the same for rotation matrix "); 
   MidasMatrix rot_mat(arMolInfoFinal.GetNoOfVibs(),arMolInfoRef.GetNoOfVibs(),C_0);

   //check orthogonality
   for (In i=I_0; i < arMolInfoRef.GetNoOfVibs() ; ++i)
   {
      MidasVector mode_1(I_3*arMolInfoRef.GetNumberOfNuclei()); 
      arMolInfoRef.GetNormalModeMw(mode_1,i);
      for (In j=i; j < arMolInfoRef.GetNoOfVibs() ; ++j)
      {
         MidasVector mode_2(I_3*arMolInfoRef.GetNumberOfNuclei()); 
         arMolInfoRef.GetNormalModeMw(mode_2,j);
         Nb check=Dot(mode_1,mode_2);
         if (i==j) check-=C_1;
         MidasAssert(fabs(check) < C_I_10_12, "reference molecule not orthonomal in Rotation matrix"); 
      }
   }

   // set up the actual matrix
   for (In i=I_0; i < arMolInfoFinal.GetNoOfVibs() ; ++i)
   {
      MidasVector mode_final(I_3*arMolInfoFinal.GetNumberOfNuclei()); 
      arMolInfoFinal.GetNormalModeMw(mode_final,i);
      for (In j=I_0; j < arMolInfoRef.GetNoOfVibs() ; ++j)
      {
         MidasVector mode_ref(I_3*arMolInfoRef.GetNumberOfNuclei()); 
         arMolInfoRef.GetNormalModeMw(mode_ref,j);
         rot_mat[i][j]=Dot(mode_final,mode_ref);
      }
   }

   //
   return rot_mat;
}
