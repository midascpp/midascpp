/*
************************************************************************
*  
* @file                NormalCoordDrv.h
*
* Created:             26-04-2021
*
* Author:              Bo Thomsen (DrBoThomsen@gmail.com)
*
* Short Description:   Driver for generating the normal coordinates
*                      given a hessian in the molecule input.
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NORMALCOORDDRV_H_INCLUDED
#define NORMALCOORDDRV_H_INCLUDED

// Forward decl
class System;

namespace midas
{

// Forward decl
namespace input
{
   class ModSysCalcDef;
}

//! Driver for generating the normal coordinates given a hessian on input
void NormalCoordDrv(const midas::input::ModSysCalcDef& aCalcDef, const System& aSystem);

} // namespace midas

#endif /* NORMALCOORDDRV_H_INCLUDED */