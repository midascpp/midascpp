/*
************************************************************************
*  
* @file                NormalCoordDrv.cc
*
* Created:             26-04-2021
*
* Author:              Bo Thomsen (DrBoThomsen@gmail.com)
*
* Short Description:   Driver for generating the normal coordinates
*                      given a hessian in the molecule input.
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "NormalCoordDrv.h"
#include "pes/molecule/MoleculeInfo.h"
#include "system/System.h"
#include "system/operator_utils.h"
#include "input/Input.h"

namespace midas
{

/**
 * Normalcoordinate driver
 *
 * @param aCalcDef
 * @param aSystem
**/
void NormalCoordDrv
   (  const input::ModSysCalcDef& aCalcDef
   ,   const System& aSystem
   )
{
   Mout << std::endl << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Entering Normal Coordinate module ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
   
   MidasAssert(aSystem.HasHessian(), "The molecule file must contain a precalculated hessian.");
   
   // Get calculation definition from ModSysCalcDef
   auto nc_calcdef = aCalcDef.GetpNormalCoordCalcDef();
   
   // Do we need to project out translation and rotation?
   In proj_out_t_r = nc_calcdef.GetProjectOutTransRot();
   
   // Set up trans-rot projection matrix if needed.
   // Just construct if from aSystem, since the geometry of aSystem and ref_system must be the same.
   MidasMatrix tr_proj_mat;
   if (  proj_out_t_r
      )
   {
      tr_proj_mat = aSystem.SetUpTotalTRProjectionMatrix(true);
   }
   
   // Ignore gradient in mop and mbounds files
   bool ignore_gradient = nc_calcdef.GetIgnoreGradient();
   if (  ignore_gradient
      )
   {
      Mout  << " NB: Ignoring gradient information!" << std::endl;
   }
   
   MidasMatrix hessian = aSystem.GetHessian();
   MidasVector gradient = ignore_gradient ? MidasVector() : aSystem.GetGradient();

   Mout  << " Diagonalizing reference-system Hessian to obtain normal coordinates." << std::endl;

   // Project out translation and rotation
   if (  proj_out_t_r  )
   {
      Mout  << "    Projecting out translation and rotation of reference system." << std::endl;
      for(In i=I_0; i<proj_out_t_r; ++i)
      {
         midas::system::util::transform_grad_hess(gradient, hessian, tr_proj_mat);
      }
   }
   
   // Diagonalize
   Mout  << "    Diagonalizing." << std::endl;
   auto diag = midas::system::util::diagonalize_hessian(hessian, true);

   Mout  << "    Frequencies:\n" << diag.first << "\n"
         << "    Eigenvectors:\n" << diag.second << "\n"
         << std::flush;

   // Transform gradient and Hessian using new normal modes
   Mout  << "    Obtaining reduced gradient and Hessian from vibrational normal modes." << std::endl;
   midas::system::util::transform_grad_hess(gradient, hessian, diag.second);

   // Sanity check that Hessian diagonal is equal to frequencies. If not, then the Hessian is not symmetric!
   const auto nfreq = diag.first.Size();
   bool err=false;
   for(In ifreq=I_0; ifreq<nfreq; ++ifreq)
   {
      if (  libmda::numeric::float_neq(std::pow(diag.first[ifreq], 2), hessian[ifreq][ifreq], 10)
         )
      {
         MidasWarning("Reduced Hessian does not contain correct frequencies on the diagonal.");
         err = true;
      }
   }
   if (  err
      )
   {
      Mout  << " Reduced Hessian:\n" << hessian << std::endl;
   }

   // Write new .mol file
   std::string new_mol_file = nc_calcdef.GetFileName();
   Mout  << "    Normal coordinates written to: " << new_mol_file << "\n" << std::flush;

   // Write mol file
   const auto& nuc_vec = aSystem.GetNucleiVec();

   // Construct normal coordinates with inverse mass weigting: U = M^{-1/2} L
   auto y_t = Transpose(diag.second);
   for(In ilvec=I_0; ilvec<y_t.Nrows(); ++ilvec)
   {
      for(In ielem=I_0; ielem<y_t.Ncols(); ++ielem)
      {
         y_t[ilvec][ielem] /= std::sqrt(nuc_vec.at(ielem/I_3).GetMass());
      }
   }
   midas::molecule::MoleculeInfo mol_info(nuc_vec, std::move(y_t), diag.first);
   mol_info.WriteMoleculeFile(midas::molecule::MoleculeFileInfo(new_mol_file, "MIDAS"));
}

} //namespace midas