/**
************************************************************************
* 
* @file                assert_link.h
*
* Created:             31-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Link to used part of cassert/assert.h (due to diff. comp)
* 
* Last modified: man mar 21, 2005  11:26
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ASSERT_LINK_H
#define ASSERT_LINK_H


#ifdef USE_ASSERT_H
/**
* Section for systems not supporting the cassert system but which has
* assert.h
* */
#include <assert.h>


#else
/**
* Section for systems supporting the cassert
* */
#include<cassert>


#endif

#endif

