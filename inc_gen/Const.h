/**
************************************************************************
* 
* @file                Const.h
*
* Created:             25-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Provide const constants for numerics
* 
* Last modified: Wed Jan 14, 2009  02:06PM ove
* 
* Detailed  Description: 
* Give numeric constants for
* A:  0,1,2,..., 1/2,1/3 etc
* B:  Other relevant constants
* Assumes that typedefs.h has been included to
* define the types In and Nb.
* 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef CONST_H
#define CONST_H

#include <limits>
#include <complex>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/math_link.h"

/**
* Integer constants, i-zero etc.
* */

constexpr In I_0         =  0;
constexpr In I_1         =  1;
constexpr In I_2         =  2;
constexpr In I_3         =  3;
constexpr In I_4         =  4;
constexpr In I_5         =  5;
constexpr In I_6         =  6;
constexpr In I_7         =  7;
constexpr In I_8         =  8;
constexpr In I_9         =  9;
constexpr In I_10        = 10;
constexpr In I_11        = 11;
constexpr In I_12        = 12;
constexpr In I_13        = 13;
constexpr In I_14        = 14;
constexpr In I_15        = 15;
constexpr In I_16        = 16;
constexpr In I_17        = 17;
constexpr In I_18        = 18;
constexpr In I_19        = 19;
constexpr In I_20        = 20;
constexpr In I_21        = 21;
constexpr In I_22        = 22;
constexpr In I_23        = 23;
constexpr In I_24        = 24;
constexpr In I_25        = 25;
constexpr In I_26        = 26;
constexpr In I_27        = 27;
constexpr In I_28        = 28;
constexpr In I_29        = 29;
constexpr In I_30        = 30;
constexpr In I_31        = 31;
constexpr In I_32        = 32;
constexpr In I_33        = 33;
constexpr In I_34        = 34;
constexpr In I_35        = 35;
constexpr In I_36        = 36;
constexpr In I_37        = 37;
constexpr In I_38        = 38;
constexpr In I_39        = 39;
constexpr In I_40        = 40;
constexpr In I_48        = 48;
constexpr In I_50        = 50;
constexpr In I_60        = 60;
constexpr In I_64        = 60;
constexpr In I_70        = 70;
constexpr In I_72        = 72;
constexpr In I_80        = 80;
constexpr In I_90        = 90;
constexpr In I_100       = 100;
constexpr In I_110       = 110;
constexpr In I_120       = 120;
constexpr In I_130       = 130;
constexpr In I_150       = 150;
constexpr In I_180       = 180;
constexpr In I_200       = 200;
constexpr In I_256       = 256;
constexpr In I_300       = 300;
constexpr In I_400       = 400;
constexpr In I_500       = 500;
constexpr In I_512       = 512;
constexpr In I_600       = 600;
constexpr In I_700       = 700;
constexpr In I_800       = 800;
constexpr In I_900       = 900;
constexpr In I_10_2      = 100;
constexpr In I_10_3      = 1000;
constexpr In I_2560      = 2560;
constexpr In I_5120      = 5120;
constexpr In I_10_4      = 10000;
constexpr In I_10_5      = 100000;
constexpr In I_10_6      = 1000000;
constexpr In I_10_7      = 10000000;
constexpr In I_10_8      = 100000000;
constexpr In I_10_9      = 1000000000;

/**
* Numbers (float, double, long double), const-zero etc. 
* Distinguish between long double and float/double.
* */

constexpr Nb C_0         =  Nb(I_0);
constexpr Nb C_1         =  Nb(I_1);
constexpr Nb C_2         =  Nb(I_2);
constexpr Nb C_3         =  Nb(I_3);
constexpr Nb C_4         =  Nb(I_4);
constexpr Nb C_5         =  Nb(I_5);
constexpr Nb C_6         =  Nb(I_6);
constexpr Nb C_7         =  Nb(I_7);
constexpr Nb C_8         =  Nb(I_8);
constexpr Nb C_9         =  Nb(I_9);
constexpr Nb C_10        = Nb(I_10);
constexpr Nb C_11        = Nb(I_11);
constexpr Nb C_12        = Nb(I_12);
constexpr Nb C_13        = Nb(I_13);
constexpr Nb C_14        = Nb(I_14);
constexpr Nb C_15        = Nb(I_15);
constexpr Nb C_16        = Nb(I_16);
constexpr Nb C_17        = Nb(I_17);
constexpr Nb C_18        = Nb(I_18);
constexpr Nb C_19        = Nb(I_19);
constexpr Nb C_20        = Nb(I_20);
constexpr Nb C_21        = Nb(I_21);
constexpr Nb C_22        = Nb(I_22);
constexpr Nb C_23        = Nb(I_23);
constexpr Nb C_24        = Nb(I_24);
constexpr Nb C_25        = Nb(I_25);
constexpr Nb C_26        = Nb(I_26);
constexpr Nb C_27        = Nb(I_27);
constexpr Nb C_28        = Nb(I_28);
constexpr Nb C_29        = Nb(I_29);
constexpr Nb C_30        = Nb(I_30);
constexpr Nb C_31        = Nb(I_31);
constexpr Nb C_32        = Nb(I_32);
constexpr Nb C_33        = Nb(I_33);
constexpr Nb C_34        = Nb(I_34);
constexpr Nb C_35        = Nb(I_35);
constexpr Nb C_36        = Nb(I_36);
constexpr Nb C_37        = Nb(I_37);
constexpr Nb C_38        = Nb(I_38);
constexpr Nb C_39        = Nb(I_39);
constexpr Nb C_40        = Nb(I_40);
constexpr Nb C_48        = Nb(I_48);
constexpr Nb C_50        = Nb(I_50);
constexpr Nb C_60        = Nb(I_60);
constexpr Nb C_64        = Nb(I_64);
constexpr Nb C_70        = Nb(I_70);
constexpr Nb C_80        = Nb(I_80);
constexpr Nb C_90        = Nb(I_90);
constexpr Nb C_100       = Nb(I_100);
constexpr Nb C_180       = Nb(I_180);
constexpr Nb C_200       = Nb(I_200);
constexpr Nb C_256       = Nb(I_256);
constexpr Nb C_300       = Nb(I_300);
constexpr Nb C_400       = Nb(I_400);
constexpr Nb C_500       = Nb(I_500);
constexpr Nb C_512       = Nb(I_512);
constexpr Nb C_600       = Nb(I_600);
constexpr Nb C_700       = Nb(I_700);
constexpr Nb C_800       = Nb(I_800);
constexpr Nb C_900       = Nb(I_900);
constexpr Nb C_10_1      = Nb(I_10);
constexpr Nb C_10_2      = Nb(I_10_2);
constexpr Nb C_10_3      = Nb(I_10_3);
constexpr Nb C_2560      = Nb(I_2560);
constexpr Nb C_5120      = Nb(I_5120);
constexpr Nb C_10_4      = Nb(I_10_4);
constexpr Nb C_10_5      = Nb(I_10_5);
constexpr Nb C_10_6      = Nb(I_10_6);
constexpr Nb C_10_7      = Nb(I_10_7);
constexpr Nb C_10_8      = Nb(I_10_8);
constexpr Nb C_10_9      = Nb(I_10_9);
constexpr Nb C_10_10     = I_10_9*C_10;
constexpr Nb C_10_11     = I_10_9*C_10_2;  
constexpr Nb C_10_12     = I_10_9*C_10_3;
constexpr Nb C_10_13     = I_10_9*C_10_4;      
constexpr Nb C_10_14     = I_10_9*C_10_5;      
constexpr Nb C_10_15     = I_10_9*C_10_6;      
constexpr Nb C_10_16     = I_10_9*C_10_7;      

/*
#ifdef L_LONGDOUBLE

const Nb C_0         =  0.0L;
const Nb C_1         =  1.0L;
const Nb C_2         =  2.0L;
const Nb C_3         =  3.0L;
const Nb C_4         =  4.0L;
const Nb C_5         =  5.0L;
const Nb C_6         =  6.0L;
const Nb C_7         =  7.0L;
const Nb C_8         =  8.0L;
const Nb C_9         =  9.0L;
const Nb C_10        = 10.0L;
const Nb C_11        = 11.0L;
const Nb C_12        = 12.0L;
const Nb C_20        = 20.0L;
const Nb C_30        = 30.0L;
const Nb C_40        = 40.0L;
const Nb C_50        = 50.0L;
const Nb C_60        = 60.0L;
const Nb C_70        = 70.0L;
const Nb C_80        = 80.0L;
const Nb C_90        = 90.0L;
const Nb C_100       = 100.0L;
const Nb C_180       = 180.0L;
const Nb C_200       = 200.0L;
const Nb C_300       = 300.0L;
const Nb C_400       = 400.0L;
const Nb C_500       = 500.0L;
const Nb C_600       = 600.0L;
const Nb C_700       = 700.0L;
const Nb C_800       = 800.0L;
const Nb C_900       = 900.0L;
const Nb C_10_1      = 10.0L;
const Nb C_10_2      = 100.0L;
const Nb C_10_3      = 1000.0L;
const Nb C_10_4      = 10000.0L;
const Nb C_10_5      = 100000.0L;
const Nb C_10_6      = 1000000.0L;
const Nb C_10_7      = 10000000.0L;
const Nb C_10_8      = 100000000.0L;
const Nb C_10_9      = 1000000000.0L;
const Nb C_10_10     = 10000000000.0L;
const Nb C_10_11     = 100000000000.0L;
const Nb C_10_12     = 1000000000000.0L;
const Nb C_10_13     = 10000000000000.0L;
const Nb C_10_14     = 100000000000000.0L;
const Nb C_10_15     = 1000000000000000.0L;
const Nb C_10_16     = 10000000000000000.0L;

#else

const Nb C_0         =  0.0;
const Nb C_1         =  1.0;
const Nb C_2         =  2.0;
const Nb C_3         =  3.0;
const Nb C_4         =  4.0;
const Nb C_5         =  5.0;
const Nb C_6         =  6.0;
const Nb C_7         =  7.0;
const Nb C_8         =  8.0;
const Nb C_9         =  9.0;
const Nb C_10        = 10.0;
const Nb C_11        = 11.0;
const Nb C_12        = 12.0;
const Nb C_20        = 20.0;
const Nb C_30        = 30.0;
const Nb C_40        = 40.0;
const Nb C_50        = 50.0;
const Nb C_60        = 60.0;
const Nb C_70        = 70.0;
const Nb C_80        = 80.0;
const Nb C_90        = 90.0;
const Nb C_100       = 100.0;
const Nb C_180       = 180.0;
const Nb C_200       = 200.0;
const Nb C_300       = 300.0;
const Nb C_400       = 400.0;
const Nb C_500       = 500.0;
const Nb C_600       = 600.0;
const Nb C_700       = 700.0;
const Nb C_800       = 800.0;
const Nb C_900       = 900.0;
const Nb C_10_1      = 10.0;
const Nb C_10_2      = 100.0;
const Nb C_10_3      = 1000.0;
const Nb C_10_4      = 10000.0;
const Nb C_10_5      = 100000.0;
const Nb C_10_6      = 1000000.0;
const Nb C_10_7      = 10000000.0;
const Nb C_10_8      = 100000000.0;
const Nb C_10_9      = 1000000000.0;
const Nb C_10_10     = 10000000000.0;
const Nb C_10_11     = 100000000000.0;
const Nb C_10_12     = 1000000000000.0;
const Nb C_10_13     = 10000000000000.0;
const Nb C_10_14     = 100000000000000.0;
const Nb C_10_15     = 1000000000000000.0;
const Nb C_10_16     = 10000000000000000.0;

#endif
*/

/**
 * fractions (float, double, long double). 
**/
constexpr Nb C_I_2        =  C_1/C_2;  
constexpr Nb C_I_3        =  C_1/C_3;  
constexpr Nb C_I_4        =  C_1/C_4;  
constexpr Nb C_I_5        =  C_1/C_5;  
constexpr Nb C_I_6        =  C_1/C_6;  
constexpr Nb C_I_7        =  C_1/C_7;  
constexpr Nb C_I_8        =  C_1/C_8;  
constexpr Nb C_I_9        =  C_1/C_9;  
constexpr Nb C_I_10       =  C_1/C_10;  
constexpr Nb C_I_20       =  C_1/C_20;  
constexpr Nb C_I_100      =  C_1/C_100;  
constexpr Nb C_I_10_1     =  C_1/C_10_1;  
constexpr Nb C_I_10_2     =  C_1/C_10_2;  
constexpr Nb C_I_10_3     =  C_1/C_10_3;  
constexpr Nb C_I_10_4     =  C_1/C_10_4;  
constexpr Nb C_I_10_5     =  C_1/C_10_5;  
constexpr Nb C_I_10_6     =  C_1/C_10_6;  
constexpr Nb C_I_10_7     =  C_1/C_10_7;  
constexpr Nb C_I_10_8     =  C_1/C_10_8;  
constexpr Nb C_I_10_9     =  C_1/C_10_9;  
constexpr Nb C_I_10_10    =  C_1/C_10_10;  
constexpr Nb C_I_10_11    =  C_1/C_10_11;  
constexpr Nb C_I_10_12    =  C_1/C_10_12;  
constexpr Nb C_I_10_13    =  C_1/C_10_13;  
constexpr Nb C_I_10_14    =  C_1/C_10_14;  
constexpr Nb C_I_10_15    =  C_1/C_10_15;  
constexpr Nb C_I_10_16    =  C_1/C_10_16;  

constexpr Nb C_M_1    =  -C_1;  

/**
 * complex numbers. 
**/
constexpr std::complex<Nb> CC_0      (C_0,C_0);
constexpr std::complex<Nb> CC_1      (C_1,C_0);
constexpr std::complex<Nb> CC_I      (C_0,C_1);
constexpr std::complex<Nb> CC_I_2    (C_I_2,C_0);
constexpr std::complex<Nb> CC_I_3    (C_I_3,C_0);
constexpr std::complex<Nb> CC_I_6    (C_I_6,C_0);
constexpr std::complex<Nb> CC_M_I    (C_0,-C_1);
constexpr std::complex<Nb> CC_M_1    (-C_1,C_0);
constexpr std::complex<Nb> CC_M_I_2  (C_0,-C_I_2);
constexpr std::complex<Nb> CC_1_1    (C_1,C_1);
constexpr std::complex<Nb> CC_1_2    (C_1,C_2);
constexpr std::complex<Nb> CC_1_3    (C_1,C_3);
constexpr std::complex<Nb> CC_2_1    (C_2,C_1);
constexpr std::complex<Nb> CC_2_2    (C_2,C_2);
constexpr std::complex<Nb> CC_2_3    (C_2,C_3);

/**
* Math constants 
* */


constexpr Nb C_PI   =  3.14159265358979323846264338L;

/**
* Constants marked with "*" are from:
*    Peter J. Mohr and Barry N. Taylor, CODATA Recommended Values of the 
*    Fundamental Physical Constants: 2002, published in Rev. Mod. Phys.
*    vol. 77(1) 1-107 (2005).
* See also http://physics.nist.gov/constants/.
*
* */

/// Fundamental constants 
constexpr Nb C_LIGHT     = 299792458L;                      // * Exact
constexpr Nb C_MUZERO    = C_4*C_PI*1.e-07L;
constexpr Nb C_EPSZERO   = C_1/(C_MUZERO*C_LIGHT*C_LIGHT);
constexpr Nb C_PLANCK    = 6.6260693e-34L;                  // * Uncertainty: (11)
constexpr Nb C_HBAR      = C_PLANCK/(C_2*C_PI);
constexpr Nb C_ECHARGE   = 1.60217653e-19L;                 // * Uncertainty: (14)
constexpr Nb C_I_ALPHA   = 137.03599911L;                   // * Uncertainty: (46)
constexpr Nb C_ALPHA     = C_1/C_I_ALPHA;

constexpr Nb C_MOL       = 6.0221415e23L;                   // * Uncertainty: (10)
constexpr Nb C_TANG      = 0.5291772108e0L;                 // * Uncertainty: (18)
constexpr Nb C_FAMU      = 1822.88848e0L;

/// Conversion factors of energy 
constexpr Nb C_AUTEV     = 27.2113845e0L;                   // * Uncertainty: (23)
constexpr Nb C_AUTJ      = 4.35974417e-18L;                 // * Uncertainty: (75)
constexpr Nb C_AUTKAYS   = 219474.6313705e0L;               // * Uncertainty: (15)
constexpr Nb C_AUTHZ     = 6.579683920721e15L;              // * Uncertainty: (55)
constexpr Nb C_AUTKJMOL  = C_AUTJ*C_MOL*1.e-3L; 
constexpr Nb C_AUTKCMOL  = C_AUTKJMOL/4.184e0L;
constexpr Nb C_AUTKJML   = C_AUTKJMOL;          
constexpr Nb C_AUTKCML   = C_AUTKCMOL;
constexpr Nb C_AUTNM     = 1.e7L/C_AUTKAYS;       
constexpr Nb C_AUTNM2    = 1.0e09L*C_PLANCK*C_LIGHT/C_AUTJ;
constexpr Nb C_AUTAJOUL  = 1.0e18L*C_AUTJ;
constexpr Nb C_KB        = 8.617343e-5L / C_AUTEV;           // * Uncertainty: (15)

/// Some conversion factors for pol and hyppol from Shelton and Rice, Chem. Rev. 94,3 (1994) - Not checked
constexpr Nb C_DPAUSI    = 1.648778e-41L;
constexpr Nb C_QPAUSI    = 4.617048e-62L;
constexpr Nb C_DPAUESU   = 1.4817e-25L;
constexpr Nb C_DPAUA3    = 0.14818475L;
constexpr Nb C_D1HPAUSI  = 3.206361e-53L;
constexpr Nb C_D1HPAUESU = 8.6392e-33L;
constexpr Nb C_D2HPAUSI  = 6.25377e-65L;
constexpr Nb C_D2HPAUESU = 5.0367e-40L;

/// Some other au const and conversion factors 
constexpr Nb C_OAUCM     = 2.19474625e5L;  // Check this difference
constexpr Nb C_OAUEV     = 2.72113957e1L;  // Check this difference
constexpr Nb C_AUTIME    = C_HBAR/C_AUTJ;
constexpr Nb C_TKMML     = 974.864e0L;        // I think this is of no use - Peter.
constexpr Nb C_TESLA     = (C_TANG*C_TANG*C_ECHARGE/C_HBAR)*1.e-20L;
constexpr Nb C_AUTK      = 3.1577465e5L;

/// Conversion of E(fi) * |mu(fi)| in au to IR intensity
/// N(A) * 1/c^2 * 1/(6 e_0 h_bar^2) * E(fi) * |mu(fi)|^2 in km/mol.
constexpr Nb C_EXDIP_AU_KMMOL   = C_I_10_3 * C_MOL/(C_LIGHT*C_LIGHT)/(C_6*C_EPSZERO*C_HBAR*C_HBAR) *
                              C_AUTJ * (C_TANG*C_I_10_10*C_TANG*C_I_10_10*C_ECHARGE*C_ECHARGE);

/// Some numerical limits, Nb=float,double, long double , In=int,short int, long 
constexpr In C_NB_SIZE_OFF     = sizeof(Nb);
constexpr Nb C_NB_MIN          = std::numeric_limits<Nb>::min();
constexpr Nb C_NB_MAX          = std::numeric_limits<Nb>::max();
constexpr Nb C_NB_EPSILON      = std::numeric_limits<Nb>::epsilon();
constexpr Nb C_NB_EPSMIN19     = max(std::numeric_limits<Nb>::epsilon(),C_1/(C_10_16*C_10_3));
// Own num zeros
constexpr Nb C_NUM_ZERO_1 = C_10_1*C_NB_EPSILON;
constexpr Nb C_NUM_ZERO_2 = C_10_2*C_NB_EPSILON;
constexpr Nb C_NUM_ZERO_3 = C_10_3*C_NB_EPSILON;
constexpr Nb C_NUM_ZERO_4 = C_10_4*C_NB_EPSILON;
constexpr Nb C_NUM_ZERO_5 = C_10_5*C_NB_EPSILON;
constexpr Nb C_NUM_ZERO_6 = C_10_6*C_NB_EPSILON;

//const Nb C_NB_MIN_EXP10    = std::numeric_limits<Nb>::min_exponent10;
//const Nb C_NB_MAX_EXP10    = std::numeric_limits<Nb>::max_exponent10;
//const Nb C_NB_DIGITS10     = std::numeric_limits<Nb>::digits;
//const Nb C_NB_INFINITY     = std::numeric_limits<Nb>::infinity();
//const Nb C_NB_ROUND_ERROR  = std::numeric_limits<Nb>::round_error();
//const Nb C_NB_ROUND_STYLE  = std::numeric_limits<Nb>::round_style;

constexpr In C_IN_SIZE_OFF     = sizeof(In);
constexpr In C_IN_MIN          = std::numeric_limits<In>::min();
constexpr In C_IN_MAX          = std::numeric_limits<In>::max();

/***************************************************************************//**
 * @brief
 *    Template struct for IO_* constants.
 * 
 * Allows for compile-time distinguishability of input/output calls to e.g.
 * GeneralDataCont::DataIo().
 *
 * @note
 *    Replaces an old implementation using `const In IO_GET = I_1` etc. which
 *    is why the implicit conversion to `int` is needed for backwards
 *    compatibility in some of the code.
 ******************************************************************************/
template<int N>
struct IoType
{
   //! Allows for implicit `int` conversion; necessary in some old code.
   constexpr operator int() const noexcept
   {
      return N;
   }

   //@{
   //! Bitwise and/or; necessary for e.g. compound `IO_GET | IO_ROBUST`.
   template<int M>
   constexpr auto operator&(const IoType<M>&) const noexcept
   {
      return IoType<N & M>();
   }
   template<int M>
   constexpr auto operator|(const IoType<M>&) const noexcept
   {
      return IoType<N | M>();
   }
   //@}
};

//@{
//! IO_* constexpr objects (different types due to different tmpl. instantiations).
constexpr IoType<1> IO_GET;
constexpr IoType<2> IO_PUT;
constexpr IoType<4> IO_ROBUST;
//@}

inline std::string ToString(const IoType<int(IO_GET)>&) {return "IO_GET";}
inline std::string ToString(const IoType<int(IO_PUT)>&) {return "IO_PUT";}
inline std::string ToString(const IoType<int(IO_GET | IO_ROBUST)>&) {return "IO_GET | IO_ROBUST";}

/*
#ifdef L_DOUBLE
const Nb C_NB_MIN        = DBL_MIN;
const Nb C_NB_MAX        = DBL_MAX;
const Nb C_NB_EPS        = DBL_EPSILON;
const Nb C_NB_MAX_10_EXP = DBL_MAX_10_EXP;
#endif
#ifdef L_FLOAT 
const Nb C_NB_MIN        = FLT_MIN;
const Nb C_NB_MAX        = FLT_MAX;
const Nb C_NB_EPS        = FLT_EPSILON;
const Nb C_NB_MAX_10_EXP = FLT_MAX_10_EXP;
#endif
#ifdef L_LONGDOUBLE
const Nb C_NB_MIN        = LDBL_MIN;
const Nb C_NB_MAX        = LDBL_MAX;
const Nb C_NB_EPS        = LDBL_EPSILON;
const Nb C_NB_MAX_10_EXP = LDBL_MAX_10_EXP;
#endif
*/

#endif /* CONST_H_INCLUDED */
