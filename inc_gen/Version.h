/**
***********************************************************************
*
* \file:               Version.h          
* 
* Created:             27-02-2002          
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Give current version number/date of stability
*
* Last modified: Mon Nov 06, 2006  12:20PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
* 
***********************************************************************
*/

#ifndef VERS_H_INCLUDED
#define VERS_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"

/**
* Midas version 
*/
const In MIDAS_VERSION       = 2022;
const In MIDAS_SUBVERSION    = 04;
const In MIDAS_SUBSUBVERSION = 0;

const bool MIDAS_RC          = false;

inline std::string MidasVersion()
{
   std::string subversion = std::to_string(MIDAS_SUBVERSION);
   subversion             = std::string(2 - subversion.length(), '0') + subversion;

   std::string version = 
      {  std::to_string(MIDAS_VERSION)     + "."
      +  subversion                        + "."
      +  std::to_string(MIDAS_SUBSUBVERSION)
      };

   if(MIDAS_RC)
   {
      version + " (RC)";
   }
   
   return version;
}

#endif /* VERS_H_INCLUDED */
