/**
************************************************************************
* 
* @file                Warnings.h
*
* Created:             20-12-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store warnigns
* 
* Last modified: man mar 21, 2005  11:28
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef WARNINGS_H_INCLUDED
#define WARNINGS_H_INCLUDED

// std headers
#include <iostream>
#include <string>
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/MidasStream.h"

extern MidasStream Mout;

/**
 * Warnings caught during input (and thereafter actually also).
 **/
void MidasWarning(const std::string& aWarning, bool aForce = false); // this should be the only interface!
void MidasWarning(std::string&& aWarning     , bool aForce = false);

/**
 * Print Warning if true 
 **/
void MidasWarningIf(bool aPrint, const std::string& aWarning);

/**
 * function to print out warnings
 **/
void PrintMidasWarnings(std::ostream& aOs=Mout);

#endif /* WARNINGS_H_INCLUDED */
