/**
************************************************************************
* 
* @file                Warnings.cc
*
* Created:             20-12-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store warnings
* 
* Last modified: man mar 21, 2005  11:29
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "Warnings.h"

#include <string>
#include <vector>
#include <set>
#include <mutex>

#include "util/Error.h"
#include "input/ProgramSettings.h"

using namespace midas;

/**
 * Struct for holding warnings, and a mutex to protect against data races.
 **/
std::set<std::string> gWarnings;      ///< The warnings
std::mutex            warning_mutex;  ///< Mutex to protect gWarnings against data races

/**
 * Print warning directly to 'Mout'
 *
 * @param aWarning   The warning to be printed.
 **/
void PrintWarningDirectly
   (  const std::string& aWarning
   )
{
   bool mute = Mout.Muted();
   Mout.Unmute();
   Mout << "[WARNING:] " << aWarning << std::endl;
   if(mute) Mout.Mute();
}

/** 
 * Overload for MidasWarning for 'const std::string&' (l-value).
 * Will call MidasWarning for r-value. See below for documentation.
 *
 * @param aWarning     The warning.
 * @param aForce       Force printing.
 **/
void MidasWarning
   (  const std::string& aWarning
   ,  bool  aForce
   )
{
   std::string warning_copy = aWarning;
   MidasWarning(std::move(warning_copy), aForce);
}

/**
 * Print a MidasCpp warning. 
 *
 * All warnings are printed to Mout directly as well as gathered in a vector
 * of warnings to be printed at the end of MidasCpp execution as well.
 *
 * If the current warning has already been issued, it will not be printed again, 
 * unless it is forced by setting parameter aForce to true.
 *
 * @param aWarning    The warning.
 * @param aForce      Force printing.
 **/
void MidasWarning
   (  std::string&& aWarning
   ,  bool aForce
   )
{
   std::unique_lock<std::mutex> lock(warning_mutex);

   static bool warning_as_error = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_WARNING_AS_ERROR") == "1";

   if (  warning_as_error  )
   {
      lock.unlock();
      MIDASERROR("Error triggered by WARNING : '" + aWarning + "'.");
   }

   if(aForce || (gWarnings.find(aWarning) == gWarnings.end()))
   {
      PrintWarningDirectly(aWarning);
      gWarnings.insert(std::move(aWarning));
   }
}

/**
 * Print warning if condition is met. 
 * If condition is true the warning will get printed.
 *
 * @param aPrint    The condition.
 * @param aWarning  The warning.
 **/
void MidasWarningIf
   (  bool aPrint
   ,  const std::string& aWarning
   )
{
   if(aPrint)
   {
      MidasWarning(aWarning);
   }
}

/**
 * Print all gathered warning to 'Mout'.
 * This will be called at the end of execution to dump all warnings.
 *
 * @param aOs   The ostream to print warnings to.
 **/
void PrintMidasWarnings
   (  std::ostream& aOs
   )
{
   std::lock_guard<std::mutex> lock(warning_mutex);
   
   if (gWarnings.size()>0) 
   {
      aOs << "[WARNING:] MIDAS MAIN WARNINGS: " << std::endl;
      //for (In i_war=0; i_war<gWarnings.size(); ++i_war) 
      for (auto it=gWarnings.begin(); it!=gWarnings.end(); ++it)
      {
         aOs << "[WARNING:] " << (*it) << std::endl;
      }
   }
}
