/**
************************************************************************
* 
* @file                MaxFiles.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Provides maximum number of files open, interface c file io routines.
* 
* Last modified: man mar 21, 2005  11:27
*
* Detailed  Description: 
*
* MAX_FILE is used both in C and C++ routines and is given here for
* common reference.
* C routines: DirFileIo.c 
* C++ routines: LinkFileIo.cc
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MAXFILES_H
#define MAXFILES_H

#define MAX_FILE 99

#endif

