/**
************************************************************************
* 
* @file                time_link.h
*
* Created:             31-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Link to used part of ctime/time.h (due to diff. comp)
* 
* Last modified: man mar 21, 2005  11:27
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TIME_LINK_H
#define TIME_LINK_H


#ifdef USE_TIME_H
/**
* Section for systems not supporting the cstdlib system but which has
* time.h
* */
#include <time.h>


#else
/**
* Section for systems supporting the ctime system.
* */
#include<ctime>
using std::ctime;
using std::difftime;
using std::time;
using std::clock;

#endif

#endif

