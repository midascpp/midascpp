/**
************************************************************************
* 
* @file                stdlib_link.h
*
* Created:             31-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Link to used part of cstdlib/stdlib.h (due to diff. comp)
* 
* Last modified: man mar 21, 2005  11:27
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef STDLIB_LINK_H
#define STDLIB_LINK_H


#ifdef USE_STDLIB_H
/**
* Section for systems not supporting the cstdlib system but which has
* stdlib.h
* */
#include <stdlib.h>


#else
/**
* Section for systems supporting the cstdlib
* */
#include<cstdlib>


#endif

#endif

