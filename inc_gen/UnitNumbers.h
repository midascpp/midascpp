/**
************************************************************************
* 
* @file                UnitNumbers.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Tells if unitnumbers is occupied or not.
* 
* Last modified: man mar 21, 2005  11:27
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef UNITNUMBERS_H_INCLUDED
#define UNITNUMBERS_H_INCLUDED

#include <vector>

#include "MaxFiles.h"

std::vector<bool> gUnitNumbers(MAX_FILE);

#endif /* UNITNUMBERS_H_INCLUDED */
