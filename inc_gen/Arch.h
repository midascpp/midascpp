/**
 *******************************************************************************
 * 
 * @file    Arch.h
 * @date    07-09-2021
 * @author  Mads Greisen Højlund (mgh@chem.au.dk)
 * 
 * @details
 *    This detects the architecutre and defines a macro that can be used, e.g.,
 *    for specializing unit tests based on architecture. The list is not 
 *    at all exhaustive and should be expanded as needed. 
 *    Note that the __GNUC__ macro is defined by several GNU compliant 
 *    compilers such as GCC and clang. GCC and clang both define the 
 *    __aarch64__ macro so we currently don't need to distinguish.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#pragma once
#ifndef MIDASCPP_INC_GEN_ARCH_H_INCLUDED
#define MIDASCPP_INC_GEN_ARCH_H_INCLUDED

#ifdef __GNUC__
#ifdef __aarch64__
#define ARCH_ARM 
#endif /* __aarch64__ */
#endif /* __GNUC__ */

#endif /* MIDASCPP_INC_GEN_ARCH_H_INCLUDED */
