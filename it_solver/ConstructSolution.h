#ifndef CONSTRUCTSOLUTION_H_INCLUDED
#define CONSTRUCTSOLUTION_H_INCLUDED

#include "IES.h"
#include "SolutionVectorContainer.h"
#include "ComplexVector.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"

/**
 * General implementation for constructing solution vectors.
 *
 * Uses the 'IES_Axpy' interface.
 *
 * @param aSolVec    Output parameter. A container, which after the call this holds new solution vectors.
 * @param aTrials    The trials used to construct the solution.
 * @param red        The reduced space matrix.
 * @param aNeq       The number of equations/eigenvalues.
 * @param aDim       The dimensionality of the solution vectors.
 **/
template
   <  class SOL
   ,  class TRIALS
   ,  class RED
   >
void ConstructSolutionImpl
   (  SOL& aSolVec
   ,  TRIALS& aTrials
   ,  const RED& red
   ,  size_t aNeq
   ,  size_t aDim
   )
{
   aSolVec.Clear();

   for(size_t i=0; i<aNeq; ++i)
   {
      aSolVec.emplace_back(aDim,C_0);
      for(size_t j=0; j<aTrials.size(); ++j)
      {
         IES_Axpy(aSolVec[i],aTrials[j],red[j][i]);
      }
   }
};

/**
 * Specialization of implementation for constructing solution for class 'TensorDataCont'.
 * This is needed as 'TensorDataCont' does not have the needed constructor used 
 * in the general implemantation.
 *
 * Uses the 'IES_Axpy' interface for TensorDataCont.
 *
 * @param aSolVec    Output parameter. A container, which after the call this holds new solution vectors.
 * @param aTrials    The trials used to construct the solution.
 * @param red        The reduced space matrix.
 * @param aNeq       The number of equations/eigenvalues.
 * @param aDim       The dimensionality of the solution vectors.
 **/
template
   <  class TRIALS
   ,  class RED
   >
void ConstructSolutionImpl
   (  StandardContainer<TensorDataCont>& aSolVec
   ,  TRIALS& aTrials
   ,  const RED& red
   ,  size_t aNeq
   ,  size_t aDim
   )
{
   aSolVec.Clear();

   for(size_t i=0; i<aNeq; ++i)
   {
      // use shape of trials, then zero
      aSolVec.emplace_back(aTrials.back());
      aSolVec.back().Zero();
      for(size_t j=0; j<aTrials.size(); ++j)
      {
         IES_Axpy(aSolVec[i],aTrials[j],red[j][i]);
      }
   }
};

/**
 * Specialization of implementation for constructing solution for a container of ComplexVectors.
 * This is needed as real and imaginary parts needs to taken care of separately.
 *
 * Uses the 'IES_Axpy' interface for underlying real and imag vector types.
 *
 * @param aSolVec    Output parameter. A container, which after the call this holds new solution vectors.
 * @param aTrials    The trials used to construct the solution.
 * @param red        The reduced space matrix.
 * @param aNeq       The number of equations/eigenvalues.
 * @param aDim       The dimensionality of the solution vectors.
 **/
template
   <  class T
   ,  class U
   ,  class TRIALS
   ,  class RED
   >
void ConstructSolutionImpl
   (  StandardContainer<ComplexVector<T,U> >& aSolVec
   ,  TRIALS& aTrials
   ,  const RED& red
   ,  size_t aNeq
   ,  size_t aDim
   )
{
   aSolVec.Clear();

   for(size_t i=0; i<aNeq; ++i)
   {
      aSolVec.emplace_back();
      aSolVec.back().Re().SetNewSize(aDim);
      aSolVec.back().Re().Zero();
      aSolVec.back().Im().SetNewSize(aDim);
      aSolVec.back().Im().Zero();
      for(size_t j=0; j<aTrials.size(); ++j)
      {
         IES_Axpy(aSolVec[i].Re()
                , aTrials[j]
                , red[i].Re()[j]
                );
         IES_Axpy(aSolVec[i].Im()
                , aTrials[j]
                , red[i].Im()[j]
                );
      }
   }
};

/**
 * Write solution vectors to disc. Specialization for TensorDataCont.
 *
 * First removes backed up files from the iteration before the last,
 * then saves files from last iteration as back-up.
 * Lastly it loops over solution vectors dumping them to disc one-by-one.
 *
 * Using this functon you will only have the last iteration on disc,
 * when calculation finishes.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param aName     A common name for the individual solution vector files.
 **/
inline void WriteSolutionToDiscImpl
   (  StandardContainer<TensorDataCont>& aSolVec
   ,  const std::string& aName
   )
{
   // Check for old backups and remove them
   size_t j=0;
   auto old_backup_file = "backup_" + aName + "_";
   while (  InquireFile(old_backup_file + std::to_string(j))
         )
   {
      auto file = old_backup_file + std::to_string(j);
      if (  std::remove(file.c_str())
         )
      {
         MIDASERROR("Error in removing old backup vector.");
      }

      ++j;
   }

   // Make backup of previous write
   size_t k=0;
   while (  InquireFile(aName + "_" + std::to_string(k))
         )
   {
      auto old_name = aName + "_" + std::to_string(k);
      auto new_name = "backup_" + old_name;
      if (  std::rename(old_name.c_str(), new_name.c_str())
         )
      {
         MIDASERROR("Error in renaming backup vector.");
      }

      ++k;
   }

   // Write new vectors
   for(size_t i=0; i<aSolVec.size(); ++i)
   {
      std::string name = aName+"_"+std::to_string(i);
      aSolVec[i].WriteToDisc(name);
   }
}

/**
 * Write solution vectors to disc. Specialization for TensorDataCont.
 *
 * Loops over solution vectors dumping them to disc one-by-one,
 * naming files after iteration number.
 *
 * Using this function you will have all solution vectors for all iterations saved on disc,
 * when the calculation finishes.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param iter      Iteration number.
 **/
inline void WriteSolutionToDiscImpl
   (  StandardContainer<TensorDataCont>& aSolVec
   ,  size_t iter
   )
{
   for(int i=0; i<aSolVec.size(); ++i)
   {
      std::string name = "temp_sol_vec_it" + std::to_string(iter) + "_" + std::to_string(i);
      aSolVec[i].WriteToDisc(name);
   } 

   // remove old solution vectors
   size_t j = 0;
   auto old_name = "temp_sol_vec_it" + std::to_string(iter-1) + "_";

   while (  InquireFile(old_name + std::to_string(j))
         )
   {
      auto file = old_name + std::to_string(j);
      if (  std::remove(file.c_str())
         )
      {
         MIDASERROR("Error in removing old solution vectors.");
      }

      ++j;
   }
}

/**
 * Write solution vectors to disc. Specialization for GeneralDataCont.
 *
 * Loops over solution vectors dumping them to disc one-by-one.
 * Presumably backs-up old files using the GeneralDataCont back-up implementation.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param aName     A common name for the individual solution vector files.
 **/
template
   <  class T
   >
void WriteSolutionToDiscImpl
   (  StandardContainer<GeneralDataCont<T> >& aSolVec
   ,  const std::string& aName
   )
{
   for(int i=0; i<aSolVec.size(); ++i)
   {
      DataCont temp = aSolVec[i];
      temp.NewLabel(aName+"_"+std::to_string(i));
      temp.SaveUponDecon(true);
   }
}

/**
 * Write solution vectors to disc. Specialization for GeneralDataCont.
 *
 * Loops over solution vectors dumping them to disc one-by-one,
 * naming files after iteration number.
 *
 * Using this function you will have all solution vectors for all iterations saved on disc,
 * when the calculation finishes.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param iter      Iteration number.
 **/
template
   <  class T
   >
void WriteSolutionToDiscImpl
   (  StandardContainer<GeneralDataCont<T> >& aSolVec
   ,  size_t iter
   )
{
   for(int i=0; i<aSolVec.size(); ++i)
   {
      DataCont temp = aSolVec[i];
      temp.NewLabel("temp_sol_vec_it" + std::to_string(iter) + "_" + std::to_string(i));
      temp.SaveUponDecon(true);
   } 

   // remove old solution vectors
   for(int i=0; i<aSolVec.size(); ++i)
   {
      std::string label = "temp_sol_vec_it" + std::to_string(iter-1) + "_" + std::to_string(i) + "_0";
      if(InquireFile(label))
      {
         RmFile(label);
      }
   } 
}

/**
 * Write solution vectors to disc. Specialization for ComplexVector of GeneralDataCont.
 *
 * Loops over solution vectors dumping them to disc one-by-one,
 * in separate files for real and imaginary parts.
 * Presumably backs-up old files using the GeneralDataCont back-up implementation.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param aName     A common name for the individual solution vector files.
 **/
template
   <  class T
   ,  class U
   >
void WriteSolutionToDiscImpl
   (  StandardContainer<ComplexVector<GeneralDataCont<T>, GeneralDataCont<U> > >& aSolVec
   ,  const std::string& aName
   )
{
   for(int i=0; i<aSolVec.size(); ++i)
   {
      DataCont temp_re = aSolVec[i].Re();
      temp_re.NewLabel(aName + "_re_" + std::to_string(i));
      temp_re.SaveUponDecon(true);
      
      DataCont temp_im = aSolVec[i].Im();
      temp_im.NewLabel(aName + "_im_" + std::to_string(i));
      temp_im.SaveUponDecon(true);
   }  
}

/**
 * Write solution vectors to disc. Specialization for ComplexVector of GeneralDataCont.
 *
 * Loops over solution vectors dumping them to disc one-by-one,
 * with a separate file for real and imaginary parts,
 * naming files after iteration number.
 *
 * Using this function you will have all solution vectors for all iterations saved on disc,
 * when the calculation finishes.
 *
 * @param aSolVec   A container of solutions vectors to be written.
 * @param iter      Iteration number.
 **/
template
   <  class T
   ,  class U
   >
void WriteSolutionToDiscImpl
   (  StandardContainer<ComplexVector<GeneralDataCont<T>,GeneralDataCont<U> > >& aSolVec
   ,  size_t iter
   )
{
   for(int i=0; i<aSolVec.size(); ++i)
   {
      DataCont temp_re = aSolVec[i].Re();
      temp_re.NewLabel("temp_sol_vec_re_it" + std::to_string(iter) + "_" + std::to_string(i));
      temp_re.SaveUponDecon(true);
      
      DataCont temp_im = aSolVec[i].Im();
      temp_im.NewLabel("temp_sol_vec_im_it" + std::to_string(iter) + "_" + std::to_string(i));
      temp_im.SaveUponDecon(true);
   }  
   
   // remove old solution vectors
   for(int i=0; i<aSolVec.size(); ++i)
   {
      std::string label_re = "temp_sol_vec_re_it" + std::to_string(iter-1) + "_" + std::to_string(i) + "_0";
      if(InquireFile(label_re))
      {
         RmFile(label_re);
      }
      std::string label_im = "temp_sol_vec_im_it" + std::to_string(iter-1) + "_" + std::to_string(i) + "_0";
      if(InquireFile(label_im))
      {
         RmFile(label_im);
      }
   } 
}

#endif /* CONSTRUCTSOLUTION_H_INCLUDED */
