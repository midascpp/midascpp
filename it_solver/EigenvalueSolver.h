#ifndef EIGENVALUEEQUATIONINTERFACE_H_INCLUDED
#define EIGENVALUEEQUATIONINTERFACE_H_INCLUDED

#include "mmv/DataCont.h"

#include "IterativeEquationSolver.h"
#include "EigenvalueEquation.h"
#include "residual/ResidualMaker.h"
#include "transformer/MidasDataContTransformer.h"
#include "reduced/Interface.h"
#include "expand/Interface.h"
#include "precon/Interface_decl.h"
#include "start/EigenvalueStartGuesser.h"
#include "finalize/NoFinalizer.h"
#include "conv/ConvergenceChecker.h"
#include "coordinates/freqana/HessianTransformer.h"

//
// Non hermitian eigenvalue solver
//
using NonHermitianEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , MidasDataContTransformer
                                         , DiagonalEigenvalueStartGuesser
                                         , MidasComplexDiagonalPreconditioner
                                         , ResidualMaker
                                         , ImagEigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedNonHermitianEigenvalueSolver
                                         , NoFinalizer
                                         >
                       >;

//
// hermitian eigenvalue solver
//                      
using HermitianEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , MidasDataContTransformer
                                         , DiagonalEigenvalueStartGuesser
                                         , MidasDiagonalEigenvaluePreconditioner
                                         , ResidualMaker
                                         , EigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedHermitianEigenvalueSolver
                                         , NoFinalizer
                                         >
                       >;

//
// hermitian eigenvalue solver with targeting, using MidasDataContTransformer
//                      
using TargetingHermitianEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , MidasDataContTransformer
                                         , DiagonalEigenvalueStartGuesser
                                         , MidasDiagonalEigenvaluePreconditioner
                                         , ResidualMaker
                                         , EigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedTargetingHermitianEigenvalueSolver
                                         , NoFinalizer
                                         >
                       >;

//
// non-hermitian eigenvalue solver with targeting, using HessianTransformer
//                      
using TargetingHessianEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , HessianTransformer
                                         , TargetingStartGuesser
                                         , MidasImagDiagonalPreconditioner
                                         , ResidualMaker
                                         , ImagEigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedTargetingEigenvalueSolver
                                         , NoFinalizer
                                         >
                       >;

//
// non-hermitian eigenvalue solver with targeting, using HessianTransformer
//                      
using TargetingHessianNoPrecondEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , HessianTransformer
                                         , TargetingStartGuesser
                                         , NoPreconditioner
                                         , ResidualMaker
                                         , ImagEigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedTargetingEigenvalueSolver
                                         , NoFinalizer
                                         >
                       >;

#include "precon/Interface_impl.h"

#endif /* EIGENVALUEEQUATIONINTERFACE_H_INCLUDED */
