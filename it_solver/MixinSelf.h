#ifndef MIXINSELF_H_INCLUDED
#define MIXINSELF_H_INCLUDED

template<class A>
struct MixinSelf
{
   A& self()
   { 
      return static_cast<A&>(*this); 
   }

   A const& self() const
   { 
      return static_cast<A const&>(*this); 
   }
};

#endif /* MIXINSELF_H_INCLUDED */
