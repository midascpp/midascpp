/**
************************************************************************
* 
* @file    RestartReader.h
*
* @date    13-10-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Class for reading in restart vectors.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef RESTARTREADER_H_INCLUDED
#define RESTARTREADER_H_INCLUDED

#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"
#include "it_solver/SolutionVectorContainer.h"
#include "it_solver/ComplexVector.h"

#include <type_traits>


namespace detail
{
////! Identifier for std::enable_if
//template<typename>
//struct is_tensor_xvec_ptr_vector : std::false_type {};
//
////! Identifier for std::enable_if
//template<std::size_t N>
//struct is_tensor_xvec_ptr_vector<std::vector<std::unique_ptr<TensorXvec<N>>>> : std::true_type {}; 
//
////! Identifier for std::enable_if
//template<std::size_t N>
//struct is_tensor_xvec_ptr_vector<StandardContainer<std::unique_ptr<TensorXvec<N>>>> : std::true_type {}; 


/**
 * Class for reading in restart vectors
 **/
class RestartReader
{
   public:
      /**
       * Read in restart vector and add to space.
       * TensorDataCont version.
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t  <  (  std::is_same_v<SPACE_T, StandardContainer<TensorDataCont>>
                                 || std::is_same_v<SPACE_T, std::vector<TensorDataCont>>
                                 )
                              >* = nullptr
         >
      void AddRestartVector
         (  const std::string& name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         auto size = trans.Dim();
         size_t j=0;
         while (  true
               )
         {
            auto name_j = name + std::to_string(j);

            if (  !InquireFile(name_j)
               )
            {
               //if (  j == 0
               //   )
               //{
               //   MidasWarning("Could not find vector: " + name_j + ". No restart vectors were added!");
               //}

               break;
            }

            Mout << " Adding restart vector: " << name_j << std::endl;
            
            TensorDataCont vec;
            vec.ReadFromDisc(name_j);

            size_t vec_size = vec.TotalSize();
            if (  vec_size != size
               )
            {
               if (  vec_size < size
                  )
               {
                  Mout  << "    Restart vector is too small: Add zeros for the remaining elements!" << std::endl;
               }
               else
               {
                  Mout  << "    Restart vector is too large: Truncate at the correct size!" << std::endl;
               }

               auto tmp_vec = trans.TemplateZeroVector();

               auto n_tens = vec.Size();
               for(size_t i = 0; i<n_tens; ++i)
               {
                  tmp_vec.GetModeCombiData(i) = vec.GetModeCombiData(i);
               }

               vec = std::move(tmp_vec);
            }
            
            space.emplace_back(vec);

            ++j;
         }
      }

      ///**
      // * Read in restart vector and add to space.
      // * TensorXvec version.
      // **/
      //template
      //   <  typename SPACE_T
      //   ,  typename TRANS
      //   //,  std::enable_if_t<detail::is_tensor_xvec_ptr_vector<SPACE_T>::value>* = nullptr
      //   >
      //void AddRestartVector
      //   (  const std::string& name
      //   ,  const TRANS& trans
      //   ,  SPACE_T& space
      //   )  const
      //{
      //   MidasWarning("AddRestartVector not implemented for TensorXvec");
      //}

      /**
       * Read in restart vector and add to space.
       * DataCont version.
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t< !  (  std::is_same_v<SPACE_T, StandardContainer<TensorDataCont>>
                                 || std::is_same_v<SPACE_T, std::vector<TensorDataCont>>
                                 //|| detail::is_tensor_xvec_ptr_vector<SPACE_T>::value
                                 )
                            >* = nullptr
         >
      void AddRestartVector
         (  const std::string& name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         size_t size = trans.Dim();

         size_t j=0;
         while (  true
               )
         {
            std::string name_j = name + std::to_string(j);
            
            if(!InquireFile(name_j + "_0"))
            {
               if (  j == 0
                  )
               {
                  MidasWarning("Could not find vector: " + name_j + "_0" + ". No restart vectors were added!");
               }

               break;
            }
            Mout << " Adding restart vector: " << name_j << std::endl;
            
            DataCont vec;
            vec.GetFromExistingOnDisc(size, name_j);
            vec.ChangeStorageTo("InMem");

            space.emplace_back(vec);

            vec.SaveUponDecon(true);

            ++j;
         }
      }
};


/**
 * Class for reading in complex restart vectors
 **/
class ComplexRestartReader
{
   public:
      /**
       * Read in restart vector and add to space.
       * TensorDataCont version.
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t<  std::is_same_v<SPACE_T, std::vector<ComplexVector<TensorDataCont, TensorDataCont>> >>* = nullptr
         >
      void AddRestartVector
         (  const std::string& name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         MIDASERROR("Not impl!");
      }

      /**
       * Read in restart vector and add to space.
       * TensorDataCont version.
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t<std::is_same_v<SPACE_T, std::vector<ComplexVector<TensorDataCont, TensorDataCont>> >>* = nullptr
         >
      void AddComplexRestartVector
         (  const std::string& re_name
         ,  const std::string& im_name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         MIDASERROR("Not impl!");
      }

      /**
       * Read in restart vector and add to space.
       * DataCont version.
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t<!std::is_same_v<SPACE_T, std::vector<ComplexVector<TensorDataCont, TensorDataCont> > > >* = nullptr
         >
      void AddRestartVector
         (  const std::string& name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         size_t size = trans.Dim();

         size_t j=0; // re init j
         while(true)
         {
            std::string name_j = name + std::to_string(j);
            
            if(!InquireFile(name_j + "_0"))
            {
               //if (  j == 0
               //   )
               //{
               //   MidasWarning("Could not find vector: " + name_j + "_0" + ". No restart vectors were added!");
               //}

               break;
            }
            Mout << " Adding restart vector: " << name_j << std::endl;
            
            DataCont vec;
            vec.GetFromExistingOnDisc(size, name_j);
            vec.ChangeStorageTo("InMem");
            DataCont zero_vec(size, C_0);

            space.emplace_back(vec, zero_vec);

            vec.SaveUponDecon(true);

            ++j;
         }
      }

      /**
       * Read in complex restart vector and add to space
       **/
      template
         <  typename SPACE_T
         ,  typename TRANS
         ,  std::enable_if_t<!std::is_same_v<SPACE_T, std::vector<ComplexVector<TensorDataCont, TensorDataCont> > >>* = nullptr
         >
      void AddComplexRestartVector
         (  const std::string& re_name
         ,  const std::string& im_name
         ,  const TRANS& trans
         ,  SPACE_T& space
         )  const
      {
         size_t size = trans.Dim();

         size_t j = 0;
         while(true)
         {
            std::string re_name_j = re_name + std::to_string(j);
            std::string im_name_j = im_name + std::to_string(j);
            
            if(!InquireFile(re_name_j+"_0") || !InquireFile(im_name_j+"_0"))
            {
               //if (  j == 0
               //   )
               //{
               //   MidasWarning("Could not find vectors: " + re_name_j + "_0" + " and " + im_name_j + "_0. No restart vectors were added!");
               //}

               break;
            }
            Mout << " Adding restart vector: " << re_name_j << std::endl;
            Mout << " Adding restart vector: " << im_name_j << std::endl;

            DataCont re_vec;
            re_vec.GetFromExistingOnDisc(size, re_name_j);
            re_vec.ChangeStorageTo("InMem");
            DataCont im_vec;
            im_vec.GetFromExistingOnDisc(size, im_name_j);
            im_vec.ChangeStorageTo("InMem");
         
            space.emplace_back(re_vec, im_vec);

            re_vec.SaveUponDecon(true);
            im_vec.SaveUponDecon(true);
            
            ++j;
         }
      }
};

} /* namespace detail */


#endif /* RESTARTREADER_H_INCLUDED */
