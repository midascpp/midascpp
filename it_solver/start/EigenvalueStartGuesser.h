#ifndef STARTGUESSER_H_INCLUDED
#define STARTGUESSER_H_INCLUDED

#include "mmv/DataCont.h"
#include "it_solver/IES.h"
#include "it_solver/IES_Macros.h"
#include "it_solver/transformer/MidasTensorDataContTransformer.h"
#include "it_solver/start/RestartReader.h"

/**
 * Determine lowest-eigenvector approximations from diagonal elements
 **/
template
   <  class A
   >
class DiagonalEigenvalueStartGuesserImpl
   :  public A
   ,  public detail::RestartReader
{
   public:
      /**
       *
       **/
      template
         <  typename TRANSFORMER
         ,  std::enable_if_t<!std::is_same_v<TRANSFORMER, MidasTensorDataContTransformer>>* = nullptr
         >
      std::vector<In> FindMinimumAddresses
         (  const TRANSFORMER& a
         )
      {
         DataCont vec_of_ones(a.Dim(),C_1,"InMem");
         DataCont diagonal(a.Dim(),C_0,"InMem");
         a.Transform(vec_of_ones,diagonal,I_0,I_1);
         
         std::vector<Nb> extrema;
         std::vector<In> min_addr;
         diagonal.AddressOfExtrema(min_addr,extrema,this->self().Neq(),-I_2); // -I_2 ??
         
         if (  this->self().IoLevel() > I_5
            )
         {
            this->self().Ostream() << " Extrema: " << extrema << std::endl;
            this->self().Ostream() << " Min Addr: " << min_addr << std::endl;
         }

         return min_addr;
      }

      /**
       *
       **/
      template
         <  typename TRANSFORMER
         ,  std::enable_if_t<std::is_same_v<TRANSFORMER, MidasTensorDataContTransformer>>* = nullptr
         >
      std::vector<In> FindMinimumAddresses
         (  const TRANSFORMER& a
         )
      {
         // Construct vector of ones
         TensorDataCont vec_of_ones(a.Vss(), BaseTensor<Nb>::typeID::SIMPLE);
         vec_of_ones.ElementwiseScalarAddition(C_1);

         TensorDataCont diagonal(a.Vss(), BaseTensor<Nb>::typeID::SIMPLE);
         a.Transform(vec_of_ones, diagonal, I_0, I_1);

         MidasWarning("Niels: Converting TensorDataCont to DataCont in TensorDataContStartGuesser::FindMinimumAddresses!");
         auto dc_diag = DataContFromTensorDataCont(diagonal);
         
         std::vector<Nb> extrema;
         std::vector<In> min_addr;
         dc_diag.AddressOfExtrema(min_addr, extrema, this->self().Neq(), -I_2); // -I_2 ??

         if (  this->self().IoLevel() > I_5
            )
         {
            this->self().Ostream() << " Extrema: " << extrema << std::endl;
            this->self().Ostream() << " Min Addr: " << min_addr << std::endl;
         }
         
         return min_addr;
      }

      /**
       *
       **/
      template
         <  class TRANSFORMER
         ,  class TRIALS
         >
      void AddFirst
         (  const TRANSFORMER& a
         ,  TRIALS& trials
         )
      {
         auto min_addr = FindMinimumAddresses(a);

         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            auto unit(this->self().Atrans().TemplateUnitVector(min_addr[i]));
            this->self().UpdateMetric();
            this->self().AddTrialVector(trials, unit);
         }
      }

      /**
       *
       **/
      bool Restart
         (  const std::vector<std::string>& restart_vec
         )
      {
         if (  restart_vec.empty()
            )
         {
            return false;
         }

         for(const auto& restart : restart_vec)
         {
            auto name = restart + "_";
            AddRestartVector(name, this->self().Atrans(), this->self().Trials());
         }

         // Update metric
         this->self().UpdateMetric();

         return ( this->self().Trials().size() > 0 );
      }
};



/**
 * Start guesser for use with targeting. Uses targets as initial trial vectors.
 **/
template
   <  class A
   >
class TargetingStartGuesserImpl
   :  public A
   ,  public detail::RestartReader
{
   private:
      //! Write target vectors to disc for TensorDataCont
      template
         <  typename TARGET_T
         ,  std::enable_if_t<std::is_same_v<TARGET_T, std::vector<TensorDataCont>>>* = nullptr
         >
      void WriteTargetVectorsImpl
         (  const TARGET_T& targets
         )  const
      {
         for(size_t i=0; i<targets.size(); ++i)
         {
            std::string name = "target_"+std::to_string(i);
            targets[i].WriteToDisc(name);
         }
      }

      //! Write target vectors to disc for DataCont
      template
         <  typename TARGET_T
         ,  std::enable_if_t<std::is_same_v<TARGET_T, std::vector<DataCont>>>* = nullptr
         >
      void WriteTargetVectorsImpl
         (  const TARGET_T& targets
         )  const
      {
         for(size_t i=0; i<targets.size(); ++i)
         {
            DataCont temp = targets[i];
            temp.NewLabel("target_"+std::to_string(i));
            temp.SaveUponDecon(true);
         }
      }

   public:
      /**
       * Add targets as first trials
       **/
      template
         <  class TRANSFORMER
         ,  class TRIALS
         >
      void AddFirst
         (  const TRANSFORMER& a
         ,  TRIALS& trials
         )
      {
         // Must be a copy of targets, since the vector is cleared in ExpandSpaceImpl
         auto targets = this->self().Targets();

         this->self().Converged().resize(targets.size());
         for(int i = 0; i < this->self().Converged().size(); ++i)
         {
            this->self().Converged()[i] = false;
         }
         if(!(this->self().ExpandSpaceImpl(trials, targets)))
         {
            MIDASERROR("No targets/start guess trial vectors added. Maybe you forgot to provide them?");
         }

         Mout  << " Write target vectors to disc for later restart." << std::endl;
         this->WriteTargetVectors();
      }

      /**
       * Interface to WriteTargetVectors
       **/
      void WriteTargetVectors
         (
         )  const
      {
         this->WriteTargetVectorsImpl(this->self().Targets());
      }
               
      /**
       *
       **/
      bool Restart
         (  const std::vector<std::string>& restart_vec
         )
      {
         if (  restart_vec.empty()
            )
         {
            return false;
         }

         for(const auto& restart : restart_vec)
         {
            auto name = restart + "_";
            AddRestartVector(name, this->self().Atrans(), this->self().Trials());
         }

         // Clear target vectors and read in restart
         auto new_targets = this->self().Targets();
         new_targets.clear();

         std::string target_name = "target_";
         AddRestartVector(target_name, this->self().Atrans(), new_targets);

         // If no new targets were added, we keep the default ones.
         if (  new_targets.empty()
            )
         {
            MidasWarning("Did not read any restart target vectors. Use default!");
         }
         // Else, we replace the default-generated target vectors with the restart
         else
         {
            std::swap(this->self().Targets(), new_targets);
         }

         // Update metric
         this->self().UpdateMetric();

         // Restart is a success, if we have both a trial and a target space.
         return   (  this->self().Trials().size() > 0 
                  && this->self().Targets().size() > 0
                  );
      }
};

/**
 * Base class for eigenvalue start guesser.
 **/
template
   <  template<class> class IMPL
   ,  class A
   >
class EigenvalueStartGuesserBase
   :  public IMPL<A>
{
   private:
      MAKE_VARIABLE(std::vector<std::string>, Restart);

      template
         <  class TRANSFORMER
         ,  class TRIALS
         >
      void MakeStartGuessImpl
         (  const TRANSFORMER& a
         ,  TRIALS& trials
         )
      {
         // If not restart, call AddFirst
         if (  !IMPL<A>::Restart(this->mRestart)
            )
         {
            IMPL<A>::AddFirst(a, trials);
         }
      }
   public:
      void MakeStartGuess()
      {
         MakeStartGuessImpl(this->self().Atrans(),this->self().Trials());
         this->self().EigenvaluesOld().SetNewSize(this->self().Neq());
         this->self().EigenvaluesOld().Zero();
      }

      void AddRestart
         (  const std::string& str
         )
      {
//         Mout << " Adding restart string: " << str << std::endl;
         if(std::find(mRestart.begin(),mRestart.end(),str) == mRestart.end())
         {
            mRestart.emplace_back(str);
         }
      }

      void ClearRestart
         (
         )
      {
         mRestart.clear();
      }
};

template<class A>
using DiagonalEigenvalueStartGuesser = EigenvalueStartGuesserBase<DiagonalEigenvalueStartGuesserImpl, A>;

template<class A>
using TargetingStartGuesser = EigenvalueStartGuesserBase<TargetingStartGuesserImpl, A>;

#endif /* STARTGUESSER_H_INCLUDED */
