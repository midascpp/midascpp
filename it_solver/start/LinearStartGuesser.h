#ifndef LINEARSTARTGUESSER_H_INCLUDED
#define LINEARSTARTGUESSER_H_INCLUDED

#include <vector>

#include "libmda/util/type_info.h"

#include "util/Error.h"
#include "util/Io.h"
#include "mmv/DataCont.h"

#include "../ComplexVector.h"
#include "../ZeroVector.h"
#include "RestartReader.h"



/**
 *
 **/
template
   <  class A
   >
class BasicLinearStartGuesser
   :  public A
   ,  public detail::RestartReader
{
   private:
      //!
      template
         <  class TRIALS
         >
      void AddFirstImpl
         (  TensorDataCont& trans_y
         ,  TRIALS& trials
         ,  bool normalize = true
         )
      {
         trials.emplace_back(trans_y);

         if (  normalize
            )
         {
            trials.back().Normalize();
         }
      }

      //!
      template
         <  class TRIALS
         >
      void AddFirstImpl
         (  DataCont& trans_y
         ,  TRIALS& trials
         ,  bool normalize = true
         )
      {
         trials.emplace_back(trans_y);
         trials.back().ChangeStorageTo("InMem");

         if (  normalize
            )
         {
            trials.back().Normalize();
         }
      }

   public:
      //!
      template
         <  class Y
         >
      using trans_y_t = Y;
      
      //!
      template
         <  class Y
         ,  class TRIALS
         >
      void AddFirst
         (  Y& trans_y
         ,  TRIALS& trials
         )
      {
         this->AddFirstImpl(trans_y[0], trials);
      }

      //!
      template
         <  class Y
         ,  class TRIALS
         >
      void AddAllFirst
         (  Y& trans_y
         ,  TRIALS& trials
         )
      {
         for(auto& ty : trans_y)
         {
            // Add vector without normalizing
            this->AddFirstImpl(ty, trials, false);
         }
      }

      //!
      template
         <  class Y
         >
      bool Restart
         (  Y& trans_y
         ,  Y& y
         ,  const std::vector<std::string>& restart
         )  const
      {
         size_t size = 0;
         
         if constexpr   (  std::is_same_v<Y, std::vector<TensorDataCont>>
                        )
         {
            size = y[0].TotalSize();
         }
         else
         {
            size = y[0].Size();
         }

         assert(size == this->self().Atrans().Dim());

         for(size_t i=0; i<restart.size(); ++i)
         {
            std::string name = restart[i] + "_";
            AddRestartVector(name, this->self().Atrans(), trans_y);
         }

         return (trans_y.size() != 0);
      }
};

/**
 *
 **/
template
   <  class A
   >
class LinearStartGuesserImpl
   :  public BasicLinearStartGuesser<A>
{
   public:
      template
         <  class Y
         >
      void MakeTransY
         (  Y& trans_y
         ,  Y& y
         )
      {
         for(size_t i=0; i<y.size(); ++i)
         {
            trans_y.emplace_back(y[i]);
            this->self().PreconditionImpl(trans_y.back(),0.0);
         }
      }
};

/**
 *
 **/
template
   <  class A
   >
class FreqShiftedLinearStartGuesserImpl
   :  public BasicLinearStartGuesser<A>
{
   public:
      template
         <  class Y
         >
      void MakeTransY
         (  Y& trans_y
         ,  Y& y
         )
      {
         assert(y.size() == this->self().Atrans().Frequencies().Size());

         for(size_t i=0; i<y.size(); ++i)
         {
            trans_y.emplace_back(y[i]);
            this->self().PreconditionImpl(trans_y.back(),this->self().Atrans().Frequencies()[i]);
         }
      }
};

/**
 *
 **/
template
   <  class A
   >
class IndividuallyFreqShiftedLinearStartGuesserImpl
   :  public BasicLinearStartGuesser<A>
{
   public:
      template<class Y>
      void MakeTransY(Y& trans_y, Y& y)
      {
         auto frq_size = this->self().Atrans().Frequencies().Size();

         trans_y.reserve(y.size()*frq_size);
         
         for(size_t y_idx=0; y_idx<y.size(); ++y_idx)
         {
            for(size_t frq_idx=0; frq_idx<frq_size; ++frq_idx)
            {
               trans_y.emplace_back(y[y_idx]);
               trans_y.back().ChangeStorageTo("InMem");
               
               this->self().PreconditionImpl(trans_y.back(),this->self().Atrans().Frequencies()[frq_idx]);
            }
         }

//         return trans_y;
      }
};

template<class A>
class ComplexFreqShiftedLinearStartGuesserImpl
   :  public A
   ,  public detail::ComplexRestartReader
{
   public:
      template<class Y>
      using trans_y_t = std::vector<ComplexVector<DataCont,DataCont> >;

      template<class Y>
      void MakeTransY(trans_y_t<Y>& trans_y, Y& y)
      {
         //Mout << " make trans y " << std::endl;
         for(size_t i=0; i<y.size(); ++i)
         {
            //Mout << " making : " << i << std::endl;
            trans_y.emplace_back(y[i].Re(),y[i].Im());
            trans_y.back().Re().ChangeStorageTo("InMem");
            trans_y.back().Im().ChangeStorageTo("InMem");
            //Mout << " calling precon " << std::endl;
            this->self().PreconditionImpl(trans_y.back(),this->self().Atrans().Frequency(i),trans_y,i);
            //Mout << " after calling precon " << std::endl;
         }
      }

      template<class Y, class TRIALS>
      void AddFirst(Y& trans_y, TRIALS& trials)
      {
         trials.emplace_back(trans_y[0].Re());
         trials.back().ChangeStorageTo("InMem");
         trials.back().Normalize();
         trials.emplace_back(trans_y[0].Im());
         trials.back().ChangeStorageTo("InMem");
         trials.back().Normalize();
      }

      template
         <  class Y
         ,  class TRIALS
         >
      void AddAllFirst
         (  Y& trans_y
         ,  TRIALS& trials
         )
      {
         for(const auto& ty : trans_y)
         {
            trials.emplace_back(ty.Re());
            trials.back().ChangeStorageTo("InMem");
            trials.emplace_back(ty.Im());
            trials.back().ChangeStorageTo("InMem");
         }
      }

      template<class Y>
      bool Restart(trans_y_t<Y>& trans_y, Y& y, const std::vector<std::string>& restart) const
      {
         auto size = y[0].Re().Size();
         assert(size == this->self().Atrans().Dim());

         for(size_t i=0; i<restart.size(); ++i)
         {
            // restart from complex start guess
            std::string re_name = restart[i] + "_re_";
            std::string im_name = restart[i] + "_im_";
            AddComplexRestartVector(re_name, im_name, this->self().Atrans(), trans_y);
//            AddComplexRestartVector(re_name,im_name,size,trans_y);
            
            std::string re_name2 = restart[i] + "re_";
            std::string im_name2 = restart[i] + "im_";
            AddComplexRestartVector(re_name2, im_name2, this->self().Atrans(), trans_y);
//            AddComplexRestartVector(re_name2, im_name2, size, trans_y);

            // restart from non-complex start guess
            std::string name = restart[i] + "_";
            AddRestartVector(name, this->self().Atrans(), trans_y);
//            AddRestartVector(name, size, trans_y);
            
            std::string name2 = restart[i];
            AddRestartVector(name2, this->self().Atrans(), trans_y);
//            AddRestartVector(name2,size,trans_y);
         }

         return (trans_y.size() != 0);
      }
};


/**
 *
 **/
template
   <  template<class> class IMPL
   ,  class A
   >
class LinearStartGuesserBase
   :  public IMPL<A>
{
   private:
      //! Names of restart vectors
      std::vector<std::string> mRestart;

      //! Add restart vectors without orthogonalization
      bool mNonOrthoRestart = false;

      /**
       *
       **/
      template
         <  class TRANSFORMER
         ,  class TRIALS
         ,  class Y
         >
      void MakeStartGuessImpl
         (  const TRANSFORMER& a
         ,  TRIALS& trials
         ,  Y& y
         )
      {
         //Mout << " MAKE START GUESS " << std::endl;
         assert(y.size() > 0); // check that we actually have a rhs
         
         typename IMPL<A>::template trans_y_t<Y> trans_y;

         if (  !IMPL<A>::Restart(trans_y, y, mRestart)
            )
         {
//            Mout << " did not restart " << std::endl;
            IMPL<A>::MakeTransY(trans_y, y);
//            Mout << " after make trans " << std::endl;
         }

//         Mout << " before expand " << std::endl;
         if (  this->mNonOrthoRestart
            )
         {
            IMPL<A>::AddAllFirst(trans_y, trials);
         }
         else if  (  !(this->self().ExpandSpaceImpl(trials, trans_y))
                  )
         {
            // if no one gets added we add the first to have at least one trial
            //Mout << " No start vector added, I'm adding first R.H.S. vector " << std::endl;
            IMPL<A>::AddFirst(trans_y,trials);
         }
         //Mout << " DONE WITH START GUESS " << std::endl;
      }

   public:
      void MakeStartGuess()
      {
         MakeStartGuessImpl(this->self().Atrans(),this->self().Trials(),this->self().Y());
      }
      
      void AddRestart(const std::string& str)
      {
         //MidasWarning("Warning commented out AddRestart() for LinearStartGuesser");
//         Mout << " Adding restart string: " << str << std::endl;
         if(std::find(mRestart.begin(),mRestart.end(),str) == mRestart.end())
         {
            mRestart.emplace_back(str);
         }
      }

      void SetNonOrthoRestart
         (  bool aNOR
         )
      {
         mNonOrthoRestart = aNOR;
      }

      void ClearRestart()
      {
         mRestart.clear();
      }
};

// interface 
template<class A>
using LinearStartGuesser = LinearStartGuesserBase<LinearStartGuesserImpl,A>;

template<class A>
using FreqShiftedLinearStartGuesser = LinearStartGuesserBase<FreqShiftedLinearStartGuesserImpl,A>;

template<class A>
using IndividuallyFreqShiftedLinearStartGuesser = LinearStartGuesserBase<IndividuallyFreqShiftedLinearStartGuesserImpl,A>;

template<class A>
using ComplexFreqShiftedLinearStartGuesser = LinearStartGuesserBase<ComplexFreqShiftedLinearStartGuesserImpl,A>;

#endif /* STARTGUESSER_H_INCLUDED */
