#ifndef ITERATIVEEQUATIONBASE_H_INCLUDED
#define ITERATIVEEQUATIONBASE_H_INCLUDED

#include "libmda/util/type_sink.h"

#include "MixinSelf.h"
#include "IES_Macros.h"
#include "input/RspCalcDef.h"

#include "mpi/Impi.h"

#include "util/MidasStream.h"
extern MidasStream Mout;

template<class A>
class IterativeEquationBase
   :  public MixinSelf<A>
{
   private:
      using This = IterativeEquationBase<A>;
      
      MidasStream& mOstream = Mout;

      MAKE_VARIABLE(size_t,Neq,5);
      MAKE_VARIABLE(bool,Solved,false);
      MAKE_VARIABLE(bool,IsConverged,false);
      MAKE_VARIABLE(size_t,BreakDim,1000);
      MAKE_VARIABLE(In, IoLevel, I_1);

      MAKE_VARIABLE(const RspCalcDef*,CalcDef,nullptr);

      /**
       * some implementation details... (struct is used because we can't have namespace inside class)
       **/
      struct detail
      {
         // clear if trials
         template<class T, class = void>
         struct ClearIfTrialsImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };
         
         template<class T>
         struct ClearIfTrialsImpl<T
                                , typename std::enable_if<libmda::util::type_sink<decltype(std::declval<T*>()->self().Trials())>::value>::type
                                >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().Trials().Clear();
            }
         };

         // clear if targets
         template<class T, class = void>
         struct ClearIfTargetsImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };

         template<class T>
         struct ClearIfTargetsImpl
            <  T
            ,  typename std::enable_if_t<libmda::util::type_sink<decltype(std::declval<T*>()->self().Targets())>::value>
            >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().ResetTargeter();
            }
         };

         // clear if metric
         template<class T, class = void>
         struct ClearIfMetricImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };

         template<class T>
         struct ClearIfMetricImpl
            <  T
            ,  typename std::enable_if_t<libmda::util::type_sink<decltype(std::declval<T*>()->self().SubspaceMetric())>::value>
            >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().ClearMetric();
            }
         };

         // clear if sigma
         template<class T, class = void>
         struct ClearIfSigmaImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };
         
         template<class T>
         struct ClearIfSigmaImpl<T
                                , typename std::enable_if<libmda::util::type_sink<decltype(std::declval<T*>()->self().Sigma())>::value>::type
                                >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().Sigma().Clear();
            }
         };
         
         // clear if gamma
         template<class T, class = void>
         struct ClearIfGammaImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };
         
         template<class T>
         struct ClearIfGammaImpl<T
                                , typename std::enable_if<libmda::util::type_sink<decltype(std::declval<T*>()->self().Gamma())>::value>::type
                                >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().Gamma().Clear();
            }
         };
         
         // clear if Old reduced
         template<class T, class = void>
         struct ClearIfReducedSolutionImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };
         
         template<class T>
         struct ClearIfReducedSolutionImpl<T
                                         , typename std::enable_if<libmda::util::type_sink<decltype(std::declval<T*>()->self().ReducedSolution())>::value>::type
                                         >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().ReducedSolution().Clear();
            }
         };
         
         // clear if Old reduced
         template<class T, class = void>
         struct ClearIfOldReducedSolutionImpl
         {
            void operator()(T* const ptr) const
            {
            }
         };
         
         template<class T>
         struct ClearIfOldReducedSolutionImpl<T
                                            , typename std::enable_if<libmda::util::type_sink<decltype(std::declval<T*>()->self().OldReducedSolution())>::value>::type
                                            >
         {
            void operator()(T* const ptr) const
            {
               ptr->self().OldReducedSolution().Clear();
            }
         };
      }; /* struct detail */
      
      //
      // clear trials
      //
      void ClearIfTrials()
      {
         typename detail::template ClearIfTrialsImpl<This>()(this);
      }
      void ClearIfTargets()
      {
         typename detail::template ClearIfTargetsImpl<This>()(this);
      }
      void ClearIfMetric()
      {
         typename detail::template ClearIfMetricImpl<This>()(this);
      }
      void ClearIfSigma()
      {
         typename detail::template ClearIfSigmaImpl<This>()(this);
      }
      void ClearIfGamma()
      {
         typename detail::template ClearIfGammaImpl<This>()(this);
      }
      void ClearIfReducedSolution()
      {
         typename detail::template ClearIfReducedSolutionImpl<This>()(this);
      }
      void ClearIfOldReducedSolution()
      {
         typename detail::template ClearIfOldReducedSolutionImpl<This>()(this);
      }

      //IterativeEquationBase() = delete;
      IterativeEquationBase(const IterativeEquationBase&) = delete;
      IterativeEquationBase& operator=(const IterativeEquationBase&) = delete;

   public:
      using MixinSelf<A>::self;
      
      explicit IterativeEquationBase(RspCalcDef* calcdef = nullptr): mCalcDef(calcdef)
      {
      }
      
      //
      // init iterative equation base
      //
      void Initialize()
      {
      }

      //! Write solution vector for iteration number
      template
         <  typename SOL_T
         ,  std::enable_if_t<!std::is_same_v<SOL_T, StandardContainer<TensorDataCont>>>* = nullptr
         >
      void WriteSolutionToDisc
         (  SOL_T& aSolVec
         ,  size_t aItNum
         )  const
      {
#ifndef VAR_MPI
         WriteSolutionToDiscImpl(aSolVec, aItNum);
#else
         if (  midas::mpi::IsMaster())
         {  
            WriteSolutionToDiscImpl(aSolVec, aItNum);
         }

         midas::mpi::detail::WRAP_Barrier(MPI_COMM_WORLD);
#endif /* VAR_MPI */
      }

      //! Write solution vector to given name
      template
         <  typename SOL_T
         ,  std::enable_if_t<!std::is_same_v<SOL_T, StandardContainer<TensorDataCont>>>* = nullptr
         >
      void WriteSolutionToDisc
         (  SOL_T& aSolVec
         ,  const std::string& aName
         )  const
      {
#ifndef VAR_MPI
         WriteSolutionToDiscImpl(aSolVec, aName);
#else
         if (  midas::mpi::IsMaster())
         {  
            WriteSolutionToDiscImpl(aSolVec, aName);
         }
         
         midas::mpi::detail::WRAP_Barrier(MPI_COMM_WORLD);
#endif /* VAR_MPI */
      }

      //! Decompose solution vector
      template
         <  typename SOL_T
         ,  std::enable_if_t<std::is_same_v<SOL_T, StandardContainer<TensorDataCont>>>* = nullptr
         >
      void DecomposeSolution
         (  SOL_T& aSolVec
         )
      {
         auto trf_decompinfo = this->self().Atrans().GetDecompInfo();
         auto trial_decompinfo = this->self().DecompInfo();

         // If we are using decomposition, decompose all solvecs to transformer threshold using the decompinfo of the trials
         if (  !trf_decompinfo.empty()
            )
         {
            MidasWarning("IterativeEquationBase::DecomposeSolution is using transformer threshold. Is that good enough?");

            auto thresh = midas::tensor::GetDecompThreshold(trf_decompinfo, false);
            Mout  << " Decomposing solution vector:\n"
                  << "    Threshold:   " << thresh << "\n"
                  << std::flush;

            for(size_t i=0; i<aSolVec.size(); ++i)
            {
               aSolVec[i].Decompose(trial_decompinfo, thresh);
            }
         }
      }


      //! Write solution vector for iteration number (TensorDataCont version)
      template
         <  typename SOL_T
         ,  std::enable_if_t<std::is_same_v<SOL_T, StandardContainer<TensorDataCont>>>* = nullptr
         >
      void WriteSolutionToDisc
         (  SOL_T& aSolVec
         ,  size_t aItNum
         )
      {
         this->DecomposeSolution(aSolVec);

#ifndef VAR_MPI
         WriteSolutionToDiscImpl(aSolVec, aItNum);
#else
         if (  midas::mpi::IsMaster())
         {  
            WriteSolutionToDiscImpl(aSolVec, aItNum);
         }
         
         midas::mpi::detail::WRAP_Barrier(MPI_COMM_WORLD);
#endif /* VAR_MPI */
      }

      //! Write solution vector to given name (TensorDataCont version)
      template
         <  typename SOL_T
         ,  std::enable_if_t<std::is_same_v<SOL_T, StandardContainer<TensorDataCont>>>* = nullptr
         >
      void WriteSolutionToDisc
         (  SOL_T& aSolVec
         ,  const std::string& aName
         )
      {
         this->DecomposeSolution(aSolVec);

#ifndef VAR_MPI
         WriteSolutionToDiscImpl(aSolVec, aName);
#else
         if (  midas::mpi::IsMaster())
         {  
            WriteSolutionToDiscImpl(aSolVec, aName);
         }
         
         midas::mpi::detail::WRAP_Barrier(MPI_COMM_WORLD);
#endif /* VAR_MPI */
      }

      
      //
      // set calcdef
      //
      void SetCalcDef(const RspCalcDef* calcdef)
      {
         mCalcDef = calcdef;
      }
      
      //
      // get reduced size
      //
      size_t ReducedSize() const
      {
         return this->self().Trials().size(); 
      }
      
      //
      // should we break and restart
      //
      bool Break() const
      {
         return (ReducedSize() > mBreakDim);
      }
   
      /**
       * Clear the solver.
       *
       **/
      void ClearSolver()
      {
         Mout << " Clearing iterative solver." << std::endl;

         ClearIfTrials();
         ClearIfSigma();
         ClearIfGamma();

         // Clear targets
         ClearIfTargets();

         // Clear metric
         ClearIfMetric();
         
         ClearIfReducedSolution();
         ClearIfOldReducedSolution();

         this->self().Residuals().Clear();
         this->self().SolVec().Clear();

         this->mSolved = false;
         this->mIsConverged = false;
         
         //for(size_t i=0; i<this->self().Converged().size(); ++i)
         //{
         //   this->self().Converged()[i] = false;
         //}
      }

      //
      //
      //
      MidasStream& Ostream() const
      {
         return mOstream;
      }
      
      //
      // temporarily here.. should be deleted at some point  (14/05/14)
      //
      void ConstructSolution()
      {
         Mout << " WARNING NOT CONSTRUCTING SOLUTION " << std::endl;
         std::cout << " WARNING NOT CONSTRUCTING SOLUTION " << std::endl;
      }

      void Output() const {};
};

#endif /* ITERATIVEQUATIONBASE_H_INCLUDED */
