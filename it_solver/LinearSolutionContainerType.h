#ifndef LINEARSOLUTIONCONTAINERTYPE_H_INCLUDED
#define LINEARSOLUTIONCONTAINERTYPE_H_INCLUDED

#include "EigenvectorContainer.h"
#include "it_solver/reduced/ReducedEigenvalueEquation.h"
#include "it_solver/reduced/ReducedGeneralEigenvalueEquation.h"

/**
 *
 **/
template<class REE>
struct EigenvectorContainerType_Impl;

template<class M>
struct EigenvectorContainerType_Impl<ReducedEigenvalueEquation<M> >
{
   using type = EigenvectorContainer<M,M>;
};

template<class M>
struct EigenvectorContainerType_Impl<ReducedHermitianEigenvalueEquation<M> >
{
   using type = M;
};

template<class M>
struct EigenvectorContainerType_Impl<ReducedGeneralEigenvalueEquation<M,M> >
{
   using type = EigenvectorContainer<M,M>;
};

template<class M>
struct EigenvectorContainerType_Impl<ReducedGeneralHermitianEigenvalueEquation<M,M> >
{
   using type = M;
};

template<class REE>
using EigenvectorContainerType = typename EigenvectorContainerType_Impl<REE>::type;


#endif /* EIGENVECTORCONTAINERTYPE_H_INCLUDED */
