#ifndef CONVERGENCEHOLD_H_INCLUDED
#define CONVERGENCEHOLD_H_INCLUDED

#include <vector>

#include "it_solver/IES_Macros.h"

template<class A>
class ConvergenceHolder: public A
{
   MAKE_VARIABLE(std::vector<bool>,Converged);
   
   public:
      void Initialize()
      {
         A::Initialize();

         mConverged.resize(this->self().Neq());
         for(size_t i=0; i<mConverged.size(); ++i)
         {
            mConverged[i] = false;
         }
      }

      void ClearSolver()
      {
         //Mout << " Clearing convergence holder " << std::endl;
         for(size_t i=0; i<mConverged.size(); ++i)
         {
            mConverged[i] = false;
         }
         A::ClearSolver();
      }
};

#endif /* CONVERGENCEHOLD_H_INCLUDED */
