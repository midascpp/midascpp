#ifndef RESIDUALEPSILONHOLDER_H_INCLUDED
#define RESIDUALEPSILONHOLDER_H_INCLUDED

#include "it_solver/IES_Macros.h"

template<class A>
class ResidualEpsilonHolder: public A
{
   MAKE_VARIABLE(Nb,ResidualEpsilon,1e-5);
   MAKE_VARIABLE(Nb,ResidualEpsilonRel,1e-5);

   public:
      void SetResidualEpsilon(Nb aResidualEpsilon) 
      { 
         mResidualEpsilon = aResidualEpsilon; 
      }
      void SetResidualEpsilonRel(Nb aResidualEpsilonRel) 
      { 
         mResidualEpsilonRel = aResidualEpsilonRel; 
      }
};


#endif /* RESIDUALEPSILONHOLDER_H_INCLUDED */
