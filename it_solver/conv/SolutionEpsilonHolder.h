#ifndef SOLUTIONEPSILONHOLDER_H_INCLUDED
#define SOLUTIONEPSILONHOLDER_H_INCLUDED

#include "it_solver/IES_Macros.h"

template<class A>
class SolutionEpsilonHolder: public A
{
   MAKE_VARIABLE(Nb,SolutionEpsilon,1e-5);

   public:
      void SetSolutionEpsilon(Nb aSolutionEpsilon) 
      { 
         mSolutionEpsilon = aSolutionEpsilon; 
      }
};


#endif /* SOLUTIONEPSILONHOLDER_H_INCLUDED */
