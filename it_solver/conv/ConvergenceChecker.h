#ifndef CONVERGENCECHECKER_H_INCLUDED
#define CONVERGENCECHECKER_H_INCLUDED

#include<iomanip>

#include "inc_gen/Const.h"
#include "ConvergenceHolder.h"
#include "ResidualEpsilonHolder.h"
#include "EigenvalueEpsilonHolder.h"
#include "it_solver/IES.h"
#include "it_solver/IES_Macros.h"

namespace detail
{

/**
 *
 **/
template<class A>
class CheckIthConvergenceNoImag
{
   public:
      template
         <  class T
         ,  class U
         ,  class V
         >
      bool Apply
         (  const T& eigvals
         ,  U& eigvalsold
         ,  V& residuals
         ,  size_t i_neq
         ,  Nb eig_eps
         ,  Nb res_eps
         ,  std::ostream& os
         ,  bool convert_units = false
         )
      {
         bool converged = true;
         auto eigval_diff = i_neq < eigvalsold.Size() ? fabs(eigvals[i_neq]-eigvalsold[i_neq]) : fabs(eigvals[i_neq]);
         auto eigval_norm = fabs(eigvals[i_neq]);
         auto eigval_diffnorm = eigval_diff/eigval_norm;
         auto residual_norm = IES_Norm(residuals[i_neq]);
         
         //
         // some output
         //
         In int_width = I_4;
         In nb_width  = os.precision() + I_10;
         os << " Eq. "                     << std::setw(int_width) << i_neq 
            << ": eig = "                  << std::setw(nb_width)  << eigvals[i_neq];
         if (  convert_units
            )
         {
            os << " a.u. = " << eigvals[i_neq]*C_AUTKAYS << " cm^-1";
         }
         os << "   |r| = "                 << std::setw(nb_width)  << residual_norm
            << "   |eig-eig_old|/|eig| = " << std::setw(nb_width)  << eigval_diffnorm;
            
         bool eigval_converged = eigval_diffnorm < eig_eps;
         bool residual_converged = residual_norm < res_eps;
         if(eigval_converged && residual_converged) // check convergence for both eigval and residual
         {
            // if converged we show in output
            os << " C";
         }
         else 
         {
            converged = false;
         }
         os << "\n";
         
         return converged;
      }

};

/**
 *
 **/
template<class A>
class CheckIthConvergenceImag
{
   public:
      template
         <  class T
         ,  class U
         ,  class V
         >
      bool Apply
         (  const T& eigvals
         ,  U& eigvalsold
         ,  V& residuals
         ,  size_t i_neq
         ,  bool second_complex_val    // True if this is the second eigval of a complex pair
         ,  Nb eig_eps
         ,  Nb res_eps
         ,  std::ostream& os
         ,  bool convert_units = false
         )
      {
         bool converged = true;

         size_t i_pair = second_complex_val ? i_neq - 1 : i_neq + 1;

         // Take diff of complex number
         auto eigval_norm = std::sqrt(std::fabs(std::pow(eigvals.Re()[i_neq], 2) + std::pow(eigvals.Im()[i_neq], 2)));
         auto eigval_diff  =  i_neq < eigvalsold.Re().Size()
                           ?  std::sqrt(std::fabs(std::pow(eigvals.Re()[i_neq]-eigvalsold.Re()[i_neq], 2) + std::pow(eigvals.Im()[i_neq]-eigvalsold.Im()[i_neq], 2)))
                           :  eigval_norm;
         auto eigval_diffnorm = eigval_diff/eigval_norm;
         auto residual_norm   =  eigvals.Im()[i_neq]
                              ?  std::sqrt(IES_Norm2(residuals[i_neq]) + IES_Norm2(residuals[i_pair]))
                              :  IES_Norm(residuals[i_neq]);
         
         //
         // some output
         //
         In int_width = I_4;
         In nb_width  = os.precision() + I_10;
         os << " Eq. "                     << std::setw(int_width) << i_neq 
            << ": eig = "                  << std::setw(nb_width)  << eigvals.Re()[i_neq]; 
         if (  convert_units
            )
         {
            os << " a.u. = " << eigvals.Re()[i_neq]*C_AUTKAYS << " cm^-1";
         }
         os << "   |r| = "                 << std::setw(nb_width)  << residual_norm
            << "   |eig-eig_old|/|eig| = " << std::setw(nb_width)  << eigval_diffnorm;
            
         bool eigval_converged = eigval_diffnorm < eig_eps;
         bool residual_converged = residual_norm < res_eps;
         if(eigval_converged && residual_converged) // check convergence for both eigval and residual
         {
            // if converged we show in output
            os << " C";
         }
         else 
         {
            converged = false;
         }

         if (  eigvals.Im()[i_neq]
            )
         {
            os << "   Im[eig] = " << std::setw(nb_width) << eigvals.Im()[i_neq];
            if (  convert_units
               )
            {
               os << " a.u. = " << eigvals.Im()[i_neq]*C_AUTKAYS << " cm^-1";
            }
         }

         os << "\n";
         
         return converged;
      }

};

} /* namespace detail */

template<class A>
class EigenvalueConvergenceChecker: 
   public EigenvalueEpsilonHolder<
          ResidualEpsilonHolder<
          ConvergenceHolder<
            A
          > > >
 , private detail::CheckIthConvergenceNoImag<EigenvalueConvergenceChecker<A> >
{
   using This = EigenvalueConvergenceChecker<A>;
   
   private:
      MAKE_VARIABLE(bool, ConvertEigvalUnits, false);

   public:
      bool CheckConvergence()
      {
         bool converged = true;
         this->self().Converged().resize(this->self().Neq());
         for(size_t i_neq=0; i_neq<this->self().Neq(); ++i_neq)
         {
            this->self().Converged()[i_neq] = 
               detail::CheckIthConvergenceNoImag<This>::Apply(this->self().Eigenvalues()
                                                            , this->self().EigenvaluesOld()
                                                            , this->self().Residuals()
                                                            , i_neq
                                                            , this->self().EigenvalueEpsilon()
                                                            , this->self().ResidualEpsilon()
                                                            , this->self().Ostream()
                                                            ,  this->mConvertEigvalUnits
                                                            );
            if(!this->self().Converged()[i_neq])
            {
               converged = false;
            }
         }

         // update eigvals old
         // maybe move this to a function of its own... fine here for now
         this->self().EigenvaluesOld().SetNewSize(this->self().Eigenvalues().Size());
         this->self().EigenvaluesOld() = this->self().Eigenvalues();

         return converged;
      }
};

template<class A>
class ImagEigenvalueConvergenceChecker: 
   public EigenvalueEpsilonHolder<
          ResidualEpsilonHolder<
          ConvergenceHolder<
            A
          > > >
 , private detail::CheckIthConvergenceImag<ImagEigenvalueConvergenceChecker<A> >
{
   using This = ImagEigenvalueConvergenceChecker<A>;

   private:
      MAKE_VARIABLE(bool, ConvertEigvalUnits, false);

   public:
      bool CheckConvergence()
      {
         bool converged = true;
         bool second_complex_val = false;
         this->self().Converged().resize(this->self().Neq());

         for(size_t i_neq=0; i_neq<this->self().Neq(); ++i_neq)
         {
            
            this->self().Converged()[i_neq] = 
               detail::CheckIthConvergenceImag<This>::Apply (  this->self().Eigenvalues()
                                                            ,  this->self().EigenvaluesOld()
                                                            ,  this->self().Residuals()
                                                            ,  i_neq
                                                            ,  second_complex_val
                                                            ,  this->self().EigenvalueEpsilon()
                                                            ,  this->self().ResidualEpsilon()
                                                            ,  this->self().Ostream()
                                                            ,  this->mConvertEigvalUnits
                                                            );

            // Set bool if we are at the second eigval of a complex pair
            if (  this->self().Eigenvalues().Im()[i_neq]
               )
            {
               second_complex_val = !second_complex_val;
            }
            else
            {
               second_complex_val = false;
            }

            if(!this->self().Converged()[i_neq])
            {
               converged = false;
            }
         }

         // update eigvals old
         // maybe move this to a function of its own... fine here for now
         this->self().EigenvaluesOld().Re().SetNewSize(this->self().Eigenvalues().Re().Size());
         this->self().EigenvaluesOld().Im().SetNewSize(this->self().Eigenvalues().Im().Size());
         this->self().EigenvaluesOld() = this->self().Eigenvalues();

         return converged;
      }
};

#endif /*  CONVERGENCECHECKER_H_INCLUDED */
