#ifndef EIGENVALUEEPSILONHOLDER_H_INCLUDED
#define EIGENVALUEEPSILONHOLDER_H_INCLUDED

#include "it_solver/IES_Macros.h"

template<class A>
class EigenvalueEpsilonHolder: public A
{
   MAKE_VARIABLE(Nb,EigenvalueEpsilon,1e-5);
   public:
      void SetEigenvalueEpsilon(Nb aEigenvalueEpsilon) 
      { 
         mEigenvalueEpsilon = aEigenvalueEpsilon; 
      }
};


#endif /* EIGENVALUEEPSILONHOLDER_H_INCLUDED */
