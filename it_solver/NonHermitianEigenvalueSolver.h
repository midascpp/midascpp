#ifndef NONHERMITIANEIGENVALUESOLVER_H_INCLUDED
#define NONHERMITIANEIGENVALUESOLVER_H_INCLUDED

#include "mmv/DataCont.h"
#include "transformer/MidasDataContTransformer.h"
#include "start/EigenvalueStartGuesser.h"
#include "precon/Interface_decl.h"
#include "residual/ResidualMaker.h"
#include "conv/ConvergenceChecker.h"
#include "expand/Interface.h"
#include "reduced/Interface.h"
#include "IterativeEquationSolver.h"
#include "EigenvalueEquation.h"
#include "finalize/NoFinalizer.h"

using NonHermitianEigenvalueSolver = 
IterativeEquationSolver<EigenvalueEquation<DataCont
                                         , DataCont
                                         , DataCont
                                         , DataCont
                                         , MidasDataContTransformer
                                         , DiagonalEigenvalueStartGuesser
                                         , MidasComplexDiagonalPreconditioner
                                         , ResidualMaker
                                         , ImagEigenvalueConvergenceChecker
                                         , StandardSpaceExpander
                                         , ReducedNonHermitianEigenvalueSolver
                                         , NoFinalizer
                                         >
                      >;

#include "precon/Interface_impl.h"

#endif /* NONHERMITIANEIGENVALUESOLVER_H_INCLUDED */
