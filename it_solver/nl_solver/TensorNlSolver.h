/**
************************************************************************
* 
* @file    TensorNlSolver.h
*
* @date    23-11-2015
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Non-linear equation solver for VCC able to use
*     either full or CP-decomposed tensors
*
* The equation solver is used for VCC ground-state calculations
* (or state-specific excited states) and has the following 
* algorithms for solving the non-linear VCC equations:
*    - Perturbative quasi-Newton update (default)
*    - IDJA
*    - CROP
*    - DIIS
*    - Newton-Raphson (with or without truncation of the Jacobian excitation space)
* Furthermore there is a line search that shortens the step size
* when the equations diverge.
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TENSOR_NL_SOLVER_H_INCLUDED
#define TENSOR_NL_SOLVER_H_INCLUDED

#include <iostream> 
#include <string> 
#include <vector> 
#include <string> 
#include <memory>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "tensor/NiceTensor.h"
#include "input/Input.h"
#include "vcc/TensorDataCont.h"
#include "tensor/TensorDecompInfo.h"
#include "it_solver/nl_solver/NewtonRaphsonInfo.h"
#include "it_solver/nl_solver/RankStatistics.h"
#include "it_solver/LinearEquationInterface.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "tensor/TensorDecomposer.h"
#include "mmv/DataCont.h"
#include "mpi/Impi.h"

namespace midas::vcc
{
// Forward declarations
class TransformerV3;
} /* namespace midas::vcc */

template<class T>
class LaplaceQuadrature;


/**
 * @brief
 *     Non-linear equation solver for VCC able to use
 *     either full or decomposed tensors
 *
 * The equation solver is used for VCC ground-state calculations
 * (or state-specific excited states) and has the following 
 * algorithms for solving the non-linear VCC equations:
 *    - Perturbative quasi-Newton update (default)
 *    - IDJA
 *    - CROP
 *    - DIIS
 *    - Newton-Raphson (full or with truncated Jacobian excitation space)
 *
 * It is possible to use different types of preconditioners (approximate
 * inverse Jacobians) for quasi-Newton, DIIS, and CROP.
 *  - 0th-order Jacobian (default)
 *  - IDJA Jacobian approximation
 *  - No preconditioner
 *  - Improved preconditioning (truncated Newton-Raphson)
 * It is also possible to use the absolute value of any preconditioner
 * (as long as it is SimpleTensor), but this is not really recommended.
 *
 * Furthermore there is a line search that shortens the step size
 * when the equations diverge. The line search can also be set to 
 * shorten the step when the residual norm decreases less than a 
 * given percentage, but this is turned off as default since the 
 * optimum value is very system-dependent.
 *
 * The tensor decompositions are controlled by the set of 
 * TensorDecompInfo%s. When solving the VCC equations with decomposed
 * tensors, there are additional possibilities such as adaptive
 * decomposition thresholds (highly recommended!) and using the
 * previous VCC amplitudes as starting guess for recompressions.
 * The adaptive decomposition thresholds are set to some factor
 * times the norm of the previous amplitude update. Thereby the
 * tensors are fitted to higher accuracy than the order of magnitude 
 * where the amplitudes are still changing.
 *
 * When using CP-decomposed tensors, it is necessary to employ
 * Laplace-decomposed energy-denominator tensors for the inverse
 * 0th-order Jacobian. The number of grid points for the Laplace
 * fittings can be changed if higher/lower accuracy is requested.
 **/
class TensorNlSolver
{
   private:
      /** @name Solver members **/
      //!@{

      //! Pointer to the VCC transformer
      midas::vcc::TransformerV3* mpTrf;

      //! Timer
      Timer mTimer;

      //! Set of TensorDecompInfo%s to control decompositions
      TensorDecompInfo::Set mDecompInfoSet;

      //! NewtonRaphsonInfo to control Newton-Raphson update
      NewtonRaphsonInfo mNrInfo;

      //! Trial-vector subspace. Only contains previous trial vector if no CROP or DIIS is used.
      std::list<TensorDataCont> mTrials;

      //! Residual-vector subspace. Only contains previous error vector if no CROP or DIIS is used.
      std::list<TensorDataCont> mResiduals;

      //! Optimized coefficient vector from DIIS/CROP saved for backstep
      std::vector<Nb> mSubspaceCoeffs;

      //! Saved update for backstep with Newton-Raphson
      TensorDataCont mPrevNewtonRaphsonUpdate;

      //! Preconditioner (diagonal inverse-Jacobian approximation) used for IDJA and true A diagonal
      TensorDataCont mPreconditioner;
      
      //! Rank statistics for amplitudes
      RankStatistics mAmplitudeRankStatistics;
      
      //! Rank statistics for error vectors
      RankStatistics mErrorVectorRankStatistics;
      
      //! Current iteration number
      In mNIter = I_1;
      
      //! Convergence status
      bool mConverged = false;
      
      //! Perform backstep in next iteration
      bool mBackstep = false;
      
      //! Calculated step size for line search
      Nb mBackstepLambda = C_1;
      
      //! Number of performed backsteps (where the equations have diverged)
      In mNBacksteps = C_0;
      
      //! Number of performed backtracking iterations (where the convergence has only been slow)
      In mNBacktracks = C_0;
      
      //! Current VCC energy
      Nb mEnergy = C_0;
      
      //! VCC energy from previous iteration
      Nb mPreviousEnergy;
      
      //! Current residual norm
      Nb mResidualNorm = C_1;
      
      //! Current largest residual-tensor norm
      Nb mResidualMaxNorm = C_1;
      
      //! Previous residual norm
      Nb mPreviousResidualNorm;
      
      //! Previous residual max norm
      Nb mPreviousResidualMaxNorm;
      
      //! Norm of the previous amplitude update
      Nb mUpdateNorm = C_1;
      
      //! Adaptive amplitude decomposition threshold
      Nb mAmpDecompThresh;
      
      //! Minimum amplitude decomposition threshold (set in mDecompInfoSet)
      Nb mMinDecompThresh;
      
      //! Adaptive transformer decomposition threshold
      Nb mTrfDecompThresh;
      
      //! Minimum transformer decomposition threshold
      Nb mMinTrfDecompThresh;
      
      //! Error-vector norms relative to update norms
      std::vector<Nb> mRelativeErrorVectorNorms;
      
      //! Amplitude norms
      std::vector<Nb> mAmplitudeNorms;
      
      //! Divergence flag
      bool mDiverging = false;
      
      //! Slow convergence flag
      bool mSlowConvergence = false;
      
      //! Flag for showing if restart vector contains SimpleTensor%s
      bool mRestartFromSimple = false;
      
      //!@}


      /** @name Laplace-decomposition definitions **/
      //!@{

      //! Set of LaplaceInfo
      midas::tensor::LaplaceInfo::Set mLaplaceInfo;

      //! Vector of (optimized) Laplace points
      std::vector<LaplaceQuadrature<Nb>> mLaplaceQuadVector;
      
      //!@}


      /** @name Debug options **/
      //!@{

      //! Compare accuracy of Laplace fit to exact result
      bool mCheckLaplaceFit = false;
      
      //! Save rank info of decomposed tensors and print it at the end
      bool mSaveRankInfo = false;
      
      //! Perform rank analysis
      bool mFullRankAnalysis = false;
      
      //! Calculate angles and norms in IDJA update
      bool mCheckIdja = false;
      
      //! Calculate angles and norms in Newton-Raphson update
      bool mCheckNewtonRaphson = false;
      
      //! Break after nth transform (for restart debug)
      In mBreakBeforeTransform = -I_1;
      
      //!@}


      /** @name Stopping criteria **/
      //!@{

      //! The residual convergence threshold
      Nb mResidualThreshold = C_I_10_8;
      
      //! The energy convergence threshold
      Nb mEnergyThreshold = C_0;
      
      //! Maximum number of iterations
      In mMaxIter = I_50;
      
      //! Check convergence based on largest error tensor norm
      bool mMaxNormConvCheck = false;
      
      //! Maximum number of backsteps before break
      In mMaxBacksteps = I_10;
      
      //! Maximum number of backtracking iterations if mBacktrackingThresh > 0.
      In mMaxBacktracks = I_1;
      
      //!@}
      

      /** @name Solver-method definitions **/
      //!@{

      //! Do Newton-Raphson optimization
      bool mNewtonRaphson = false;
      
      //! Update the inverse Jacobian diagonal using Improved Diagonal Jacobian Approximation
      bool mIdja = false;
      
      //! Update inv Jac if residual change norm is larger than this
      Nb mIdjaThresh = C_I_10_4;
      
      //! Initialize inverse Jacobian approximation to unit matrix (vector of ones) for IDJA
      bool mIdjaUnitMatrixGuess = false;
      
      //! Use true Jacobian diagonal as preconditioner
      bool mTrueADiag = false;
      
      //! Do CROP update
      bool mCrop = false;
      
      //! Do DIIS update
      bool mDiis = false;
      
      //! Number of CROP/DIIS vectors
      In mNSubspaceVecs = I_3;
      
      //! Allow the use of improved preconditioning in the calculation of the CROP/DIIS matrix
      bool mImprovedPreconInSubspaceMatrix = false;
      
      //! Use preconditioner in CROP, DIIS, and quasi-Newton
      bool mUsePrecon = true;
      
      //! Use line search if error decrease is smaller than this threshold
      Nb mBacktrackingThresh = C_0;
      
      //! Allow backstep if the equations diverge
      bool mAllowBackstep = true;
      
      //!@}
      

      /** @name Tensor-decomposition definitions **/
      //!@{

      //! Update the decomposition threshold during the iterations
      bool mAdaptiveDecompThreshold = false;
      
      //! Dynamic decomposition threshold in first iteration
      Nb mFirstDecompThreshold = C_I_10_2;
      
      //! Scale the amplitude update norm to obtain new decomposition threshold
      Nb mThresholdScaling = C_I_10_2;
      
      //! Update decomposition thresholds for transformer
      bool mAdaptiveTrfDecompThreshold = false;
      
      //! Relative decomp threshold for transformer (wrt. amplitude decomp threshold)
      Nb mTrfRelThreshScal = C_I_10_4;
      
      //! Save the preconditioned residuals instead of the actual error vectors
      bool mPreconSavedResiduals = false;
      
      //! Use individual thresholds for decomposing the MC tensors in the residuals
      bool mIndividualMcDecompThresh = false;
      
      //! Scaling factors (absolute, amplitude-norm relative) for individual decomp thresholds
      std::pair<Nb,Nb> mIndividualMcDecompThreshScaling = {C_1, C_0};
      
      //! Scale individual decomposition thresholds by tensor order
      bool mScaleThresholdWithTensorOrder = false;
      
      //! Threshold scaling parameter for scaling with tensor order
      Nb mTensorOrderThresholdScaling = C_5;
      
      //! Use the previous amplitudes and update as initial guess for decompositions
      bool mAdaptiveDecompGuess = false;

      //!@}


      /** @name Restart options **/
      //!@{
      
      //! Restart the calculation
      bool mRestart = false;

      //! Name of calculation to restart from
      std::string mRestartName = "";
      
      //!@}
      

      /** @name Other definitions **/
      //!@{
      
      //! Degree of indentation of the output
      In mIndent = I_3;

      //!@}

   public:
      /** @name Constructors and destructor **/
      //!@{

      //! Constructor
      TensorNlSolver()
         :  mAmplitudeRankStatistics("Amplitudes")
         ,  mErrorVectorRankStatistics("Error vector")
      {
      }

      //! Delete copy constructor
      TensorNlSolver(const TensorNlSolver&) = delete;

      //! Delete move constructor
      TensorNlSolver(TensorNlSolver&&) = delete;

      //! Default destructor
      ~TensorNlSolver() = default;

      //!@}
      
      //! Solver interface: Solve the VCC equations taking trial amplitudes as argument.
      bool  Solve
         (  TensorDataCont& 
         );

   private:
      /** @name Solver methods **/
      //!@{

      //! Perform perturbative quasi-Newton update.
      Nb QuasiNewtonUpdate
         (  TensorDataCont&
         ,  const TensorDataCont&
         )  const;

      //! Perform Newton-Raphson update.
      Nb NewtonRaphsonUpdate
         (  TensorDataCont&
         ,  const TensorDataCont&
         );

      //! Transform TensorDataCont with inverse Jacobian (full or truncated)
      TensorDataCont InverseJacobianTransformation
         (  const TensorDataCont&
         ,  const TensorDataCont&
         );

      //! Solve linear equations for Newton-Raphson (templates must be in header files!)
      template
         <  class SOLVER
         >
      TensorDataCont SolveNewtonRaphsonEquations
         (  MidasTensorDataContTransformer* aTrf
         ,  std::vector<TensorDataCont>& aRhs
         )
      {
         // Set convergence threshold of linear system
         const Nb& ts = this->mNrInfo.mAdaptiveThresholdScaling;
         Nb thresh =    this->mNrInfo.mAdaptiveLinSysThreshold 
                   ?    std::max(this->mResidualNorm*ts, this->mResidualThreshold*C_I_10_1)  // Never smaller than 1/10 * non-linear threshold
                   :    this->mResidualThreshold;
   
         SOLVER linsol(*aTrf, aRhs);
         linsol.Initialize();
         linsol.SetMaxIter(this->mNrInfo.mMaxIter);
         linsol.SetResidualEpsilon(thresh);

         if ( ! linsol.Solve() )
         {
            MidasWarning("TensorNlSolver: Newton-Raphson linear equations not converged to requested threshold!");
         }
         
         // return solution
         return linsol.Solution().back();
      }


      //! Perform CROP or DIIS update.
      Nb SubspaceUpdate
         (  TensorDataCont&
         ,  const TensorDataCont&
         );

      //! Solve the linear system for symmetrically preconditioned DIIS/CROP
      std::vector<Nb> CalculateSubspaceCoefficientsSym
         (  
         )  const;

      //! Solve the linear system for non-symmetrically preconditioned DIIS/CROP (improved precon or Newton-Raphson)
      std::vector<Nb> CalculateSubspaceCoefficientsNSym
         (  const TensorDataCont&
         );

      //! Decompose the new, optimized CROP vectors
      void DecomposeOptimizedCropVectors
         (
         );

      //! Update the inverse Jacobian using Improved Diagonal Jacobian Approximation.
      Nb IdjaUpdate
         (  const TensorDataCont&
         ,  const TensorDataCont&
         ,  const TensorDataCont&
         ,  const TensorDataCont&
         );
      
      //! Calculate new step size using quadratic line search. 
      void QuadraticLineSearch
         (  Nb&
         )  const;

      //! Generate energy denominators for perturbative update.
      TensorDataCont EnergyDenominators
         (  const In& arStartingOrder=I_1
         )  const;

      //! Calculate true inverse Jacobian diagonal
      TensorDataCont TrueInvJacobianDiagonal
         (  const TensorDataCont&
         )  const;

      //! Precondition a TensorDataCont with the (inverse) 0th-order Jacobian
      void PreconditionA0
         (  TensorDataCont&
         ,  const In& arFlag=-I_3
         )  const;
      //!@}


      /** @name Wrappers for primary solver steps **/
      //!@{

      //! Initialize LaplaceQuadrature%s if needed
      bool InitLaplaceQuadrature
         (
         );
      
      //! Initialize preconditioner if needed
      bool InitPreconditioner
         (  const TensorDataCont&
         );

      //! Recompress the VCC amplitudes and save ranks if necessary
      void RecompressAmplitudes
         (  TensorDataCont&
         );

      //! Transform the VCC amplitudes to obtain the error vector
      bool TransformAmplitudes
         (  TensorDataCont&
         ,  TensorDataCont&
         );

      //! Check the convergence of the VCC equations. Return true if the iteration loop can continue.
      bool CheckConvergence
         (
         );

      //! Save the VCC amplitudes on disc if they are better than the previous ones
      void SaveAmplitudes
         (  const TensorDataCont&
         );

      //! Backstep
      void Backstep
         (  TensorDataCont&
         );

      //! Update preconditioner
      void UpdatePreconditioner
         (  const TensorDataCont&
         ,  const TensorDataCont&
         );

      //! Update amplitudes
      void UpdateAmplitudes
         (  TensorDataCont&
         ,  TensorDataCont&
         );

      //! Update adaptive decomp thresholds
      void UpdateAdaptiveDecompThresholds
         (
         );
      //!@}


      /** @name Solver utils **/
      //!@{

      //! Check if the restart vector containts SimpleTensor%s, etc.
      void CheckRestartVector
         (  TensorDataCont&
         );

      //! Read in restart vectors for CROP and DIIS
      void RestartCropDiis
         (  TensorDataCont&
         );

      //! Check if Laplace decomposition is necessary using mDecompInfoSet
      bool UseLaplaceDenominators
         (  const In&
         )  const;

      //! Update the decomposition thresholds
      void UpdateAmplitudeDecompThresholds
         (  const Nb&
         );

      //! Get largest decomposition threshold in mDecompInfoSet
      Nb GetMaxDecompThreshold
         (
         )  const;

      //! Update individual MC decomposition thresholds
      void UpdateRelativeErrorVectorNorms
         (  const TensorDataCont&
         );

      //! Update amplitude norms
      void UpdateAmplitudeNorms
         (  const TensorDataCont&
         );

      //! Output vector along with MC info and ranks
      void OutputVectorInfo
         (  const TensorDataCont&
         ,  const std::string&
         )  const;

      //! Output iteration info
      void OutputIterationInfo
         (
         )  const;

      //! Util used for indentation. Creates string of whitespace.
      const std::string Idt
         (  const In& aLevel
         )  const 
      {
         return std::string(mIndent*aLevel+I_1,' ');
      }
      //!@}
      
      /** @name Wrappers and templates **/
      //!@{

      //! Wrapper for writing TensorDataCont%s to disc
      void WriteToDisc
         (  const TensorDataCont& aTdc
         ,  const std::string& aName
         ,  midas::mpi::OFileStream::StreamType aType = midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED
         )  const
      {
         aTdc.WriteToDisc(aName, aType);
      }

      //! Wrapper for decompositions
      template
         <  typename... Ts
         >
      void Decompose
         (  TensorDataCont& arTdc
         ,  Ts&&... ts
         )  const
      {
         arTdc.Decompose(std::forward<Ts>(ts)...);
      }

      //!@}
      
   public:
      /** @name Getters **/
      //!@{
      const Nb& GetEnergy() const { return mEnergy; }

      const In& GetNIter() const { return mNIter; }

      TensorDecompInfo::Set& GetDecompInfoSet() { return mDecompInfoSet; }
      //!@}

      /** @name Setters **/
      //!@{
      void SetTransformer( midas::vcc::TransformerV3* apTrf )  { mpTrf = apTrf; }

      void SetResidualThreshold( const Nb& arThreshold )       { mResidualThreshold = arThreshold; }
      void SetEnergyThreshold( const Nb& arThreshold )         { mEnergyThreshold = arThreshold; }
      void SetMaxIter( const In& arMaxIter )                   { mMaxIter = arMaxIter; }
      void SetMaxNormConvCheck( const bool& aB )               { mMaxNormConvCheck = aB; }

      void SetDecompInfoSet( TensorDecompInfo::Set& aSet )     { mDecompInfoSet = aSet; }

      void SetSaveRankInfo( bool aSaveRankInfo )               { mSaveRankInfo = aSaveRankInfo; }
      void SetFullRankAnalysis( bool aFull )                   { mFullRankAnalysis = aFull; }

      void SetNewtonRaphson( bool aNR )                        { mNewtonRaphson = aNR; }
      void SetNewtonRaphsonInfo( NewtonRaphsonInfo& aInfo )    { mNrInfo = aInfo; }
      void SetIdja( bool aIdja )                               { mIdja = aIdja; }
      void SetIdjaThresh( Nb aThresh )                         { mIdjaThresh = aThresh; }
      void SetIdjaUnitMatrixGuess(bool aB)                     { mIdjaUnitMatrixGuess = aB; }
      void SetTrueADiag(bool aB)                               { mTrueADiag = aB; }
      void SetCrop( bool aCrop )                               { mCrop = aCrop; }
      void SetDiis( bool aDiis )                               { mDiis = aDiis; }
      void SetNSubspaceVecs( const In& aVecs )                 { mNSubspaceVecs = aVecs; }
      void SetImprovedPreconInSubspaceMatrix( bool aB )        { mImprovedPreconInSubspaceMatrix = aB; }
      void SetUsePrecon( const bool& aPrecon )                 { mUsePrecon = aPrecon; }
      void SetAllowBackstep( const bool& aB )                  { mAllowBackstep = aB; }
      void SetBacktrackingThresh( Nb aNb )                     { mBacktrackingThresh = aNb; }
      void SetMaxBacksteps( In aIn )                           { mMaxBacksteps = aIn; }
      void SetMaxBacktracks( In aIn )                          { mMaxBacktracks = aIn; }
      
      void SetAdaptiveDecompThreshold( bool aAdapt )           { mAdaptiveDecompThreshold = aAdapt; }
      void SetAdaptiveTrfDecompThreshold( bool aAdapt )        { mAdaptiveTrfDecompThreshold = aAdapt; }
      void SetFirstDecompThreshold( Nb aThresh )               { mFirstDecompThreshold = aThresh; }
      void SetThresholdScaling( const Nb& arScaling )          { mThresholdScaling = arScaling; }
      void SetTrfRelThreshScal( const Nb& arScaling )          { mTrfRelThreshScal = arScaling; }
      void SetPreconSavedResiduals( bool aB )                  { mPreconSavedResiduals = aB; }
      void SetIndividualMcDecompThresh( bool aB )              { mIndividualMcDecompThresh = aB; }
      void SetIndividualMcDecompThreshScaling
         (  const std::pair<Nb,Nb>& aThresh
         )        
      {
         mIndividualMcDecompThreshScaling = aThresh;
      }
      void SetScaleThresholdWithTensorOrder( bool aB )         { mScaleThresholdWithTensorOrder = aB; }
      void SetTensorOrderThresholdScaling( Nb aNb )            { mTensorOrderThresholdScaling = aNb; }
      void SetAdaptiveDecompGuess( bool aAdaGuess )            { mAdaptiveDecompGuess = aAdaGuess; }

      void SetLaplaceInfo(LaplaceInfo::Set& aLaplaceInfo)      { mLaplaceInfo = aLaplaceInfo; }
      void SetCheckLaplaceFit( bool aCheck )                   { mCheckLaplaceFit = aCheck; }
      void SetCheckIdja( bool aCheck )                         { mCheckIdja = aCheck; }
      void SetCheckNewtonRaphson( bool aCheck )                { mCheckNewtonRaphson = aCheck; }
      void SetBreakBeforeTransform(In aBreak)                  { mBreakBeforeTransform = aBreak; }

      void SetRestart( bool aRestart )                         { mRestart = aRestart; }
      void SetRestartName(const std::string& mName)            { mRestartName = mName; }
      void SetRelativeErrorVectorNorms
         (  const std::vector<Nb>& aNorms
         )
      {
         this->mRelativeErrorVectorNorms = aNorms;
      }
      //!@}


      /** @name Operations **/
      //!@{
      //! Delete copy and move assignment operators
      TensorNlSolver& operator=( const TensorNlSolver& ) = delete;
      TensorNlSolver& operator=( TensorNlSolver&& ) = delete;
      //!@}
};

#endif /* TENSOR_NL_SOLVER_H_INCLUDED */
