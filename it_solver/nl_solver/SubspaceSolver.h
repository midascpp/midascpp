/**
************************************************************************
* 
* @file    SubspaceSolver.h
*
* @date    11-12-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Separate CROP and DIIS solver for general non-linear equations
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CROPSOLVER_H_INCLUDED
#define CROPSOLVER_H_INCLUDED

#include <list>
#include <string>
#include <tuple>

#include "util/type_traits/Function.h"
#include "util/SanityCheck.h"
#include "util/IsDetected.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "lapack_interface/math_wrappers.h"
#include "mmv/ContainerMathWrappers.h"

namespace midas::nlsolver
{
/**
 * Subspace solver interface (agnostic of everything but function type).
 **/
template
   <  typename F
   >
class SubspaceSolverIfc
{
   public:
      //! Alias
      using vec_t = std::decay_t<midas::type_traits::ReturnType<F>>;

      //! Check that F takes same arg type and return type
      static_assert(std::is_same_v<vec_t, std::decay_t<midas::type_traits::ArgumentType<F, 0> > >, "F must return vec_t and take vec_t as argument.");
      static_assert(midas::type_traits::ArgumentCount<F> == 1, "F must take 1 argument of type vec_t.");

      //! c-tor
      SubspaceSolverIfc
         (  F aF
         ,  In aDim
         ,  In aMaxIter
         )
         :  mF(aF)
         ,  mDim(aDim)
         ,  mMaxIter(aMaxIter)
      {
      }

      // Virtual d-tor
      virtual ~SubspaceSolverIfc() = default;

      //! Number of iterations spent in solver.
      In NumIter() const {return this->mNumIter;}

      //! After Solve(): Norm of last residual vector (converged or at max. iter.).
      Nb LastResNorm() const {return this->mLastResNorm;}

      //! Method
      virtual std::string Method() const noexcept = 0;

      //! Solve
      bool Solve
         (  vec_t& arVec
         )
      {
         Mout  << " ####################################################################\n"
               << " #      Starting " << this->Method() << "(" << this->mDim << ") solver                                     #\n"
               << " ####################################################################\n"
               << std::flush;

         bool solved = this->SolveImpl(arVec);

         Mout  << " ####################################################################" << std::endl;

         return solved;
      }

   protected:
      //! Function
      F mF;

      //! Dimension of subspace
      In mDim = I_3;

      //! Maximum iterations
      In mMaxIter = I_50;

      //! Number of iterations used.
      In mNumIter = I_0;

      //! After Solve(): Norm of last residual vector (converged or at max. iter.).
      Nb mLastResNorm = -C_1;

   private:
      virtual bool SolveImpl(vec_t&) = 0;
};

namespace detail
{
//! Forward decl
template<typename VEC_T> struct NoPreconditioner;
template<typename VEC_T> class DiagonalPreconditioner;
template<typename, bool> class GeneralPreconditioner;
template<typename VEC_T> class ResidualNormConvCheck;

/**
 * Subspace solver
 **/
template
   <  typename F
   ,  bool CROP
   ,  typename PRECON
   ,  template<typename> typename CONV
   >
class SubspaceSolver
   :  public SubspaceSolverIfc<F>
   ,  public PRECON
   ,  public CONV<std::decay_t<midas::type_traits::ReturnType<F>>>
{
   public:
      //! Alias
      using typename SubspaceSolverIfc<F>::vec_t;
      using Conv = CONV<vec_t>;
      using Precon = PRECON;

      //! C-tor
      template
         <  typename... Ts
         >
      SubspaceSolver
         (  F aF
         ,  In aDim
         ,  In aMaxIter
         ,  Nb aThreshold
         ,  Ts&&... aTs
         )
         :  SubspaceSolverIfc<F>(aF, aDim, aMaxIter)
         ,  PRECON(std::forward<Ts>(aTs)...)
         ,  CONV<vec_t>(aThreshold)
      {
      }

      //! Method
      std::string Method
         (
         )  const noexcept override
      {
         if constexpr   (  CROP
                        )
         {
            return std::string("CROP");
         }
         else
         {
            return std::string("DIIS");
         }
      }

   private:
      //! Trials
      std::list<vec_t> mTrials;

      //! Residuals
      std::list<vec_t> mResiduals;

      //! Solve implementation
      bool SolveImpl
         (  vec_t& arVec
         )  override
      {
         const auto old_flags = Mout.flags();
         Mout << std::right;
         bool conv = false;
         bool fail = false;
         this->mNumIter = I_0;
         auto error_vector = arVec;
         Uin w_iter = std::to_string(this->mMaxIter).size();

         for(In iiter = I_0; iiter<this->mMaxIter; ++iiter)
         {
            ++this->mNumIter;

            //! Do transform
            error_vector = this->mF(arVec);

            // Check convergence
            auto conv_info = Conv::Converged(arVec, error_vector);
            conv = std::get<0>(conv_info);
            fail = std::get<1>(conv_info);
            Mout  << " Iteration " << std::setw(w_iter) << iiter << ": " << std::get<2>(conv_info) << std::endl;
            if (  conv
               || fail
               || iiter + 1 >= this->mMaxIter // No need to update if hitting max. iter.
               )
            {
               // Calculate residual norm - then break loop.
               this->mLastResNorm = midas::mmv::WrapNorm(error_vector);
               break;
            }

            // Update arVec
            this->mTrials.push_back(arVec);
            this->mResiduals.push_back(error_vector);
            if (  this->mTrials.size() > this->mDim
               )
            {
               this->mTrials.pop_front();
            }
            if (  this->mResiduals.size() > this->mDim
               )
            {
               this->mResiduals.pop_front();
            }
            In n = this->mTrials.size();

            if (  n > I_1
               )
            {
               using dot_t = decltype(midas::mmv::WrapDotProduct(std::declval<vec_t>(), std::declval<vec_t>()));
               auto coeffs = std::make_unique<dot_t[]>(n+1);

               if (  Precon::Hermitian()
                  || !Precon::PreconReduced()
                  )
               {
                  // Allocate matrix and RHS vector for linear system
                  auto matrix = std::make_unique<dot_t[]>((n+1)*(n+2)/2);
                  auto matrix_ptr = matrix.get();

                  // Loop over subspace vectors
                  In i=I_0;
                  for(auto left=this->mResiduals.begin(); left!=this->mResiduals.end(); ++left)
                  {
                     for(auto right=left; right!=this->mResiduals.end(); ++right)
                     {
                        if (  Precon::PreconReduced()
                           )
                        {
                           // Use error_vector as tmp
                           error_vector = (*right);
                           Precon::Precondition(error_vector);

                           // Calculate dot
                           *(matrix_ptr++) = midas::mmv::WrapDotProduct(*left, error_vector);
                        }
                        else
                        {
                           // Calculate dot
                           *(matrix_ptr++) = midas::mmv::WrapDotProduct(*left, *right);
                        }
                     }

                     // Last elements of first n-1 rows are -1
                     *(matrix_ptr++) = -C_1;
                     // All RHS elements except the last are 0
                     coeffs[i++] = C_0;
                  }
                  // The last matrix element (n,n) is 0
                  *(matrix_ptr++) = C_0;
                  // The last RHS element is -1
                  coeffs[n] = -C_1;

                  // Solve the linear system
                  int info;
                  if constexpr   (  midas::type_traits::IsComplexV<dot_t>
                                 )
                  {
                     char l='L';
                     int nn = n+1;
                     int nrhs = 1;
                     midas::lapack_interface::hptrfs
                        (  &l,  &nn,  &nrhs
                        ,  matrix.get()
                        ,  coeffs.get()
                        ,  &nn,  &info
                        );
                  }
                  else
                  {
                     char l='L';
                     int nn = n+1;
                     int nrhs = 1;
                     midas::lapack_interface::sptrfs
                        (  &l,  &nn,  &nrhs
                        ,  matrix.get()
                        ,  coeffs.get()
                        ,  &nn,  &info
                        );
                  }
                  if (  info != 0
                     )
                  {
                     MIDASERROR("SPTRF/SPTRS failed! (info = "+std::to_string(info)+")");
                  }
               }
               // General case
               else
               {
                  auto matrix = std::make_unique<dot_t[]>((n+1)*(n+1));
                  auto matrix_ptr = matrix.get();
                  auto coefs_ptr = coeffs.get();
                  for(const auto& j : this->mResiduals)
                  {
                     error_vector = j;
                     Precon::Precondition(error_vector);
                     for(const auto& i : this->mResiduals)
                     {
                        (*matrix_ptr++) = midas::mmv::WrapDotProduct(i, error_vector);
                     }

                     // Last element in first n rows are -1
                     (*matrix_ptr++) = -1.0;

                     // RHS has zeros in first n elements
                     (*coefs_ptr++) = 0.0;
                  }

                  // Last column is missing
                  for(In k=I_0; k<n; ++k)
                  {
                     (*matrix_ptr++) = -1.0;
                  }
                  *(matrix_ptr) = 0.0;
                  *(coefs_ptr) = -1.0;

                  // Call gesv
                  int nn = n+1;
                  int nrhs = 1;
                  int lda = std::max(nn, 1);
                  auto ipiv = std::make_unique<int[]>(nn);
                  int ldb = std::max(nn, 1);
                  int info;
                  midas::lapack_interface::gesv_
                     (  &nn, &nrhs
                     ,  matrix.get(), &lda
                     ,  ipiv.get()
                     ,  coeffs.get(), &ldb
                     ,  &info
                     );

                  if (  info != 0
                     )
                  {
                     MIDASERROR("GESV failed! (info = "+std::to_string(info)+")");
                  }
               }

               // Make linear combinations
               midas::mmv::WrapZero(arVec);
               midas::mmv::WrapZero(error_vector);
               In i = I_0;
               auto it_trials = mTrials.begin();
               auto it_resids = mResiduals.begin();
               for(; it_trials != mTrials.end(); ++it_trials, ++it_resids)
               {
                  midas::mmv::WrapAxpy(arVec, *it_trials, coeffs[i]);
                  midas::mmv::WrapAxpy(error_vector, *it_resids, coeffs[i]);
                  ++i;
               }

               // Optimize the subspace vectors if using CROP
               if constexpr   (  CROP
                              )
               {
                  this->mTrials.back() = arVec;
                  this->mResiduals.back() = error_vector;
               }
            }

            // Remove oldest vectors if full space
            if (  this->mTrials.size() >= this->mDim
               )
            {
               this->mTrials.pop_front();
            }
            if (  this->mResiduals.size() >= this->mDim
               )
            {
               this->mResiduals.pop_front();
            }
            
            // Precondition update (which is saved in error_vector)
            Precon::Precondition(error_vector);

            // Update
            midas::mmv::WrapAxpy(arVec, error_vector, -C_1);
         }

         Mout.flags(old_flags);
         return conv;
      }
};



/**
 * No preconditioner
 **/
template
   <  typename VEC_T
   >
struct NoPreconditioner
{
   //! Do nothing
   void Precondition
      (  VEC_T&
      )
   {
   }

   //! Precondition reduced equations
   constexpr bool PreconReduced
      (
      )  const noexcept
   {
      return false;
   }

   //! Is Hermitian?
   constexpr bool Hermitian
      (
      )  const noexcept
   {
      return true;
   }
};

/**
 * Diagonal preconditioner
 **/
template
   <  typename VEC_T
   >
class DiagonalPreconditioner
{
   private:
      //! Inverse-Jacobian-diagonal approximation
      VEC_T mInvJac;

      //! Precondition reduced equations
      bool mPreconReduced = true;

   public:
      //! C-tor
      DiagonalPreconditioner
         (  const VEC_T& aInvJac
         ,  bool aPreconReduced = true
         )
         :  mInvJac(aInvJac)
         ,  mPreconReduced(aPreconReduced)
      {
      }

      //! C-tor
      DiagonalPreconditioner
         (  VEC_T&& aInvJac
         ,  bool aPreconReduced = true
         )
         :  mInvJac(std::move(aInvJac))
         ,  mPreconReduced(aPreconReduced)
      {
      }

      //! Precondition
      void Precondition
         (  VEC_T& arVec
         )
      {
         midas::mmv::WrapHadamardProduct(arVec, this->mInvJac);
      }

      //! Get preconreduced
      bool PreconReduced() const { return mPreconReduced; }

      //! Is Hermitian? Niels: Actually, this is only true if all elements are real...
      constexpr bool Hermitian
         (
         )  const noexcept
      {
         return true;
      }
};

/**
 * @brief General preconditioner holding function
 */
template
   <  typename F
   ,  bool HERMITIAN
   >
class GeneralPreconditioner
{
   public:
      //! Sanity check
      static_assert(midas::type_traits::ArgumentCount<F> == 1, "Preconditioner must take one argument!");
      static_assert(std::is_reference_v<midas::type_traits::ArgumentType<F, 0>>, "Preconditioner must take reference argument");

      //! Vector type
      using vec_t = std::remove_reference_t<midas::type_traits::ArgumentType<F, 0>>;

   private:
      //! Function object
      F mPreconditioner;

      //! Precondition reduced equations?
      bool mPreconReduced = true;
   
   public:
      //! c-tor
      GeneralPreconditioner
         (  F aF
         ,  bool aPreconReduced = true
         )
         :  mPreconditioner(aF)
         ,  mPreconReduced(aPreconReduced)
      {
      }

      //! Precondition
      void Precondition
         (  vec_t& arVec
         )
      {
         mPreconditioner(arVec);
      }

      //! Get preconreduced
      bool PreconReduced() const noexcept { return mPreconReduced; }

      //! Is Hermitian.
      constexpr bool Hermitian
         (
         )  const noexcept
      {
         return HERMITIAN;
      }
};

/**
 * Residual-norm convergence checker
 **/
template
   <  typename VEC_T
   >
class ResidualNormConvCheck
{
   private:
      //! Threshold
      Nb mThresh = 1.e-6;

   public:
      //! C-tor
      ResidualNormConvCheck
         (  Nb aThresh
         )
         :  mThresh(aThresh)
      {
      }

      //! Check
      auto Converged
         (  const VEC_T& aTrial
         ,  const VEC_T& aResidual
         )
      {
         Nb res_norm = midas::mmv::WrapNorm(aResidual);
         bool conv = (res_norm < this->mThresh);
         bool fail = !midas::util::IsSane(res_norm);
         std::ostringstream oss;
         oss << std::scientific << std::setprecision(5);
         oss << "|res| = " << res_norm;

         std::string str = oss.str();
         return std::make_tuple(std::move(conv), std::move(fail), std::move(str));
      }
};

} /* namespace detail */

/**
 * @name Interface
 *
 * Definition of a given solver type (taking only F as template parameter) and a Make<SolverType> function allowing for e.g.
 *    auto solver = midas::nlsolver::MakeCropSolver(function, args);
 * instead of,
 *    CropSolver<function-type> solver(function, args);
 **/
//!@{

//! CROP solver with no preconditioner
template<typename F> using CropSolver = detail::SubspaceSolver<F, true, detail::NoPreconditioner<std::decay_t<midas::type_traits::ReturnType<F>>>, detail::ResidualNormConvCheck>;
template
   <  typename F
   ,  typename... Ts
   >
auto MakeCropSolver
   (  F aF
   ,  Ts&&... aTs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new CropSolver<F>(aF, std::forward<Ts>(aTs)...));
}

//! DIIS solver with no preconditioner
template<typename F> using DiisSolver = detail::SubspaceSolver<F, false, detail::NoPreconditioner<std::decay_t<midas::type_traits::ReturnType<F>>>, detail::ResidualNormConvCheck>;
template
   <  typename F
   ,  typename... Ts
   >
auto MakeDiisSolver
   (  F aF
   ,  Ts&&... aTs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new DiisSolver<F>(aF, std::forward<Ts>(aTs)...));
}

//! CROP solver with diagonal preconditioner
template<typename F> using DiagPreconCropSolver = detail::SubspaceSolver<F, true, detail::DiagonalPreconditioner<std::decay_t<midas::type_traits::ReturnType<F>>>, detail::ResidualNormConvCheck>;
template
   <  typename F
   ,  typename... Ts
   >
auto MakeDiagPreconCropSolver
   (  F aF
   ,  Ts&&... aTs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new DiagPreconCropSolver<F>(aF, std::forward<Ts>(aTs)...));
}

//! DIIS solver with diagonal preconditioner
template<typename F> using DiagPreconDiisSolver = detail::SubspaceSolver<F, false, detail::DiagonalPreconditioner<std::decay_t<midas::type_traits::ReturnType<F>>>, detail::ResidualNormConvCheck>;
template
   <  typename F  
   ,  typename... Ts
   >
auto MakeDiagPreconDiisSolver
   (  F aF
   ,  Ts&&... aTs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new DiagPreconDiisSolver<F>(aF, std::forward<Ts>(aTs)...));
}

//! CROP solver with general preconditioner
template<typename F, typename G, bool HERMITIAN> using PreconCropSolver = detail::SubspaceSolver<F, true, detail::GeneralPreconditioner<G, HERMITIAN>, detail::ResidualNormConvCheck>;
template
   <  typename F
   ,  typename G
   ,  typename... PreconArgs
   >
auto MakeHermitianPreconCropSolver
   (  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   ,  G aPrecon
   ,  PreconArgs&&... aPreconArgs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new PreconCropSolver<F, G, true>(aF, aDim, aMaxIter, aThreshold, aPrecon, std::forward<PreconArgs>(aPreconArgs)...));
}
template
   <  typename F
   ,  typename G
   ,  typename... PreconArgs
   >
auto MakeNonHermitianPreconCropSolver
   (  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   ,  G aPrecon
   ,  PreconArgs&&... aPreconArgs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new PreconCropSolver<F, G, false>(aF, aDim, aMaxIter, aThreshold, aPrecon, std::forward<PreconArgs>(aPreconArgs)...));
}

//! DIIS solver with diagonal preconditioner
template<typename F, typename G, bool HERMITIAN> using PreconDiisSolver = detail::SubspaceSolver<F, false, detail::GeneralPreconditioner<G, HERMITIAN>, detail::ResidualNormConvCheck>;
template
   <  typename F
   ,  typename G
   ,  typename... PreconArgs
   >
auto MakeHermitianPreconDiisSolver
   (  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   ,  G aPrecon
   ,  PreconArgs&&... aPreconArgs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new PreconDiisSolver<F, G, true>(aF, aDim, aMaxIter, aThreshold, aPrecon, std::forward<PreconArgs>(aPreconArgs)...));
}
template
   <  typename F
   ,  typename G
   ,  typename... PreconArgs
   >
auto MakeNonHermitianPreconDiisSolver
   (  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   ,  G aPrecon
   ,  PreconArgs&&... aPreconArgs
   )
{
   return std::unique_ptr<SubspaceSolverIfc<F>>(new PreconDiisSolver<F, G, false>(aF, aDim, aMaxIter, aThreshold, aPrecon, std::forward<PreconArgs>(aPreconArgs)...));
}
//!@}

/**
 * @name Factories
 **/
//!@{

/**
 * Like SubspaceSolverFactory() if you know at compile-time that there is no
 * preconditioner (and you don't have to/want to pass dummy arguments for one).
 *
 * See SubspaceSolverFactory for arguments.
 **/
template
   <  typename F
   >
std::unique_ptr<SubspaceSolverIfc<F>> SubspaceSolverFactoryNoPrecon
   (  bool aCrop
   ,  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   )
{
   std::unique_ptr<SubspaceSolverIfc<F>> p(nullptr);
   if (aCrop)
   {
      p = MakeCropSolver(aF, aDim, aMaxIter, aThreshold);
   }
   else
   {
      p = MakeDiisSolver(aF, aDim, aMaxIter, aThreshold);
   }
   return p;
}

/**
 * SubspaceSolverIfc factories to be used when the solver settings, such as
 * CROP/DIIS and diag. precon./no precon., is only known at runtime.
 * The function template parameter F must be specified at compile-time.
 *
 * @param[in] aDiagPrecon
 *    true: use diagonal preconditioner. false: no preconditioner.
 *    If false, aPreconArgs are _not_ used.
 * @param[in] aCrop
 *    true: CROP algorithm. false: DIIS algorithm.
 * @param[in] aF
 *    The function, see SubspaceSolver.
 * @param[in] aDim
 *    The subspace dimension, see SubspaceSolver.
 * @param[in] aMaxIter
 *    The max. num. itertions, see SubspaceSolver.
 * @param[in] aThreshold
 *    The convergence threshold, see SubspaceSolver.
 * @param[in] aPreconArgs
 *    The arguments for the preconditioner. _Only_ used if aDiagPrecon = true.
 * @return
 *    unique_ptr to a SubspaceSolverIfc.
 **/
template
   <  typename F
   ,  typename... Ts
   >
std::unique_ptr<SubspaceSolverIfc<F>> SubspaceSolverFactory
   (  bool aDiagPrecon
   ,  bool aCrop
   ,  F aF
   ,  In aDim
   ,  In aMaxIter
   ,  Nb aThreshold
   ,  Ts&&... aPreconArgs
   )
{
   std::unique_ptr<SubspaceSolverIfc<F>> p(nullptr);
   if (aDiagPrecon)
   {
      if (aCrop)
      {
         p = MakeDiagPreconCropSolver(aF, aDim, aMaxIter, aThreshold, std::forward<Ts>(aPreconArgs)...);
      }
      else
      {
         p = MakeDiagPreconDiisSolver(aF, aDim, aMaxIter, aThreshold, std::forward<Ts>(aPreconArgs)...);
      }
   }
   else
   {
      p = SubspaceSolverFactoryNoPrecon(aCrop, aF, aDim, aMaxIter, aThreshold);
   }
   return p;
}

} /* namespace midas::nlsolver */

#endif /* CROPSOLVER_H_INCLUDED */
