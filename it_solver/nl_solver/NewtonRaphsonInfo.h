/**
************************************************************************
* 
* @file    NewtonRaphsonInfo.h
*
* @date    09-09-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Struct containing definitions for Newton-Raphson method
*     in TensorNlSolver.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef NEWTON_RAPHSON_INFO_H_INCLUDED
#define NEWTON_RAPHSON_INFO_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/**
 * @brief
 *    Definitions for TensorNlSolver when using Newton-Raphson method
 *    in TensorNlSolver.
 **/
struct NewtonRaphsonInfo
{
   /** @name Definitions **/
   //!@{

   //! Maximum number of iterations
   In mMaxIter = I_100;
   //!@}

   //! Reduce the excitation space for full Jacobian, use 0th-order for the rest
   In mJacobianMaxExci = -I_1;

   //! Increase the excitation level in the Jacobian if the error increases less than 
   //  this threshold compared to the current largest error change
   Nb mAdaptiveMaxExci = -C_1;

   //! Set the convergence threshold relative to current residual norm.
   bool mAdaptiveLinSysThreshold = true;

   //! Set adaptive threshold scaling factor (default is the same as for the old solver)
   Nb mAdaptiveThresholdScaling = static_cast<Nb>(2.e-2);

   //! Maximum norm of the Newton-Raphson update. Turned off as default.
   Nb mStepSizeControl = -C_1;

   //! Use diagonal preconditioner for linear equations
   bool mDiagonalPreconditioner = true;
   //!@}
   

   /** @name Calculation variables **/
   //!@{
   
   //! Largest relative error change
   Nb mMaxConvergenceRate = C_0;

   //!@}



   /** @name Constructors and destructor **/
   //!@{

   //! Default constructor
   NewtonRaphsonInfo() = default;

   //! Default copy constructor
   NewtonRaphsonInfo( const NewtonRaphsonInfo& ) = default;

   //! Default move constructor
   NewtonRaphsonInfo( NewtonRaphsonInfo&& ) = default;

   //! Default destructor
   ~NewtonRaphsonInfo() = default;
   //!@}
   

   /** @name Operations **/
   //!@{

   //! Default copy assignment
   NewtonRaphsonInfo& operator=( const NewtonRaphsonInfo& ) = default;

   //! Default move assignment
   NewtonRaphsonInfo& operator=( NewtonRaphsonInfo&& ) = default;

   //! Output operator
   friend std::ostream& operator<<
      (  std::ostream& os
      ,  const NewtonRaphsonInfo& aInfo
      )
   {
      os << " NewtonRaphsonInfo: mMaxIter                   = " << aInfo.mMaxIter << "\n"
         << "                    mJacobianMaxExci           = " << aInfo.mJacobianMaxExci << "\n"
         << "                    mAdaptiveMaxExci           = " << aInfo.mAdaptiveMaxExci << "\n"
         << "                    mAdaptiveLinSysThreshold   = " << std::boolalpha << aInfo.mAdaptiveLinSysThreshold << std::noboolalpha << "\n"
         << "                    mAdaptiveThresholdScaling  = " << aInfo.mAdaptiveThresholdScaling << "\n"
         << "                    mDiagonalPreconditioner    = " << std::boolalpha << aInfo.mDiagonalPreconditioner << std::noboolalpha << "\n"
         << "                    mStepSizeControl           = " << aInfo.mStepSizeControl;

      return os;
   }
   //!@}

};

#endif /* NEWTON_RAPHSON_INFO_H_INCLUDED */
