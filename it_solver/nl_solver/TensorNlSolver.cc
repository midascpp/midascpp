/**
************************************************************************
* 
* @file    TensorNlSolver.cc
*
* @date    24-11-2015
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementing member functions for TensorNlSolver class
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "it_solver/nl_solver/TensorNlSolver.h"
#include "util/read_write_binary.h"
#include "vcc/TransformerV3.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc/Vcc.h"
#include "tensor/LaplaceQuadrature.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "util/Io.h"


/**
 * Solve the VCC equations.
 *
 * @param arAmplitudes
 *    The trial amplitudes in TensorDataCont format. These are updated during the iterations.
 * @return
 *    True if the equations are converged.
 */
bool TensorNlSolver::Solve
   (  TensorDataCont& arAmplitudes
   )
{
   Mout << "\n \n" << std::endl;
   Out72Char( Mout, '#', '#', '#' );
   OneArgOut72(Mout," Starting tensor non-linear equation solver ",'#');
   Out72Char( Mout, '#', '#', '#' );
   Mout << std::endl;

   /*** Initialize vector of LaplaceQuadrature%s ***/
   if (  !this->mDecompInfoSet.empty()
      )
   {
      // Check that we have the sufficient LaplaceInfo
      if (  this->mLaplaceInfo.empty()
         )
      {
         MIDASERROR("TensorNlSolver: No LaplaceInfo found! Use #3LAPLACEINFO for tensor-decomposed calculations.");
      }
   
      this->InitLaplaceQuadrature();
   }

   /*** Run EnergyDenominators if CheckLaplaceFit is set ***/
   if (  this->mCheckLaplaceFit  )
   {
      Mout << " ##### Checking EnergyDenominators #####" << std::endl;
      this->EnergyDenominators();
      Mout << " #######################################\n" << std::endl;
   }

   /*** Mute Mout if no details of each step of the flow is wanted ***/
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();
   bool unmute = !Mout.Muted();
   if (  io < I_5  )
   {
      Mout.Mute();
   }

   /*** Initialize error vector ***/
   auto error_vector = arAmplitudes;   // should initialize to same shape, but not same data

   /*** In case of restart ***/
   if ( this->mRestart )
   {
      this->CheckRestartVector( arAmplitudes );

      if (  this->mCrop || this->mDiis )
      {
         const std::string method = this->mCrop ? "CROP" : "DIIS";
         Mout << " ##### Restarting " + method + " calculation #####" << std::endl;
         this->RestartCropDiis(error_vector);
         Mout << " ##### " + method + " read in successfully   #####" << std::endl;
      }

      if (  !this->mDecompInfoSet.empty()
         )
      {
         Mout << " First decomp threshold: " << this->mFirstDecompThreshold << std::endl;
      }
   }

   /*** Initialize inverse-Jacobian approximation if needed ***/
   this->InitPreconditioner( arAmplitudes );


   /*** Add vectors to subspaces if not done by CROP or DIIS ***/
   if ( !( this->mCrop || this->mDiis ) )
   {
      this->mTrials.push_back(arAmplitudes);
      this->mResiduals.push_back(error_vector);
   }


   /*** Initialize solver variables ***/
   // CP decomposition variables
   const Nb& trfts = this->mTrfRelThreshScal;
   this->mMinTrfDecompThresh = this->mpTrf->GetMaxDecompThreshold(); // Smallest threshold limited by the largest in TensorDecompInfo of mpTrf
   this->mMinDecompThresh = this->GetMaxDecompThreshold();
   this->mAmpDecompThresh = this->mFirstDecompThreshold;
   this->mTrfDecompThresh = std::max(this->mMinTrfDecompThresh, this->mAmpDecompThresh*trfts);

   // Reserve space for subspace vectors
   if (  this->mCrop || this->mDiis )
   {
      this->mSubspaceCoeffs.reserve(this->mNSubspaceVecs);
   }

   // Resize vector containing relative error norms
   if (  this->mIndividualMcDecompThresh
      && this->mRelativeErrorVectorNorms.size() != arAmplitudes.Size()
      )
   {
      auto size = arAmplitudes.Size();

      // Resize vector
      this->mRelativeErrorVectorNorms.resize(size);

      // Initialize to 1/||A0^-1||
      auto a0 = this->EnergyDenominators();
      for(unsigned i=0; i<size; ++i)
      {
         this->mRelativeErrorVectorNorms[i] = C_1/a0.GetModeCombiData(i).Norm();
      }
   }

   // Resize and initialize vector containing amplitude norms
   {
      auto size = arAmplitudes.Size();
      this->mAmplitudeNorms.resize( size );

      for(size_t i=0; i<size; ++i)
      {
         this->mAmplitudeNorms[i] = arAmplitudes.GetModeCombiData(i).Norm();
      }
   }


   /*** Track tensor ranks if mSaveRankInfo is set ***/
   if (  !this->mDecompInfoSet.empty()
      && this->mSaveRankInfo
      )
   {
      this->mAmplitudeRankStatistics.Reserve( this->mMaxIter );

      this->mErrorVectorRankStatistics.Reserve( this->mMaxIter );
   }


   /*** Set starting threshold in the case of adaptive threshold ***/
   if ( this->mAdaptiveDecompThreshold )
   {
      this->UpdateAmplitudeDecompThresholds(this->mAmpDecompThresh);
   }
   if ( this->mAdaptiveTrfDecompThreshold )
   {
      this->mpTrf->UpdateDecompThresholds(this->mTrfDecompThresh);
   }


   /************************************************/
   /*               MAIN SOLVER LOOP               */
   /************************************************/
   while ( !this->mConverged )
   {
      /*** Output iteration number ***/
      Mout << Idt(0)+"##### Starting iteration number " << this->mNIter << " #####" << std::endl;
      Mout << std::endl;


      /*** Set previous energy and residual norm if this is not a backstep iteration ***/
      if ( !this->mBackstep )
      {
         this->mPreviousEnergy = this->mEnergy;
         this->mPreviousResidualNorm = this->mResidualNorm;
         this->mPreviousResidualMaxNorm = this->mResidualMaxNorm;
      }


      /*** Recompress and save the VCC amplitudes ***/
      this->RecompressAmplitudes( arAmplitudes );
      this->SaveAmplitudes( arAmplitudes );


      /*** Calculate error vector and energy ***/
      if ( !this->TransformAmplitudes( arAmplitudes, error_vector ) )
      {
         MidasWarning("TensorNlSolver: Error-vector norm evaluates to " + std::to_string(this->mResidualNorm) + ". I quit!");
         break;
      }


      /*** Check convergence. Break loop if maximum backsteps is reached. ***/
      if ( this->CheckConvergence() )
      {
         // Reset to values before divergence
         MidasWarning("TensorNlSolver: Maximum number of backsteps reached. I reset amplitudes to previous best and quit!");
         this->mResidualNorm = this->mPreviousResidualNorm;
         this->mResidualMaxNorm = this->mPreviousResidualMaxNorm;
         arAmplitudes = this->mTrials.back();
         error_vector = this->mResiduals.back();
         break;
      }


      /*** Set backstep flag ***/
      if ( !(this->mDiverging || this->mSlowConvergence) )
      {
         this->mBackstep = false;
      }


      /*** Update amplitudes and decomposition thresholds if not converged ***/
      if ( !this->mConverged )
      {
         // If backstep iteration
         if ( this->mBackstep )
         {
            this->Backstep( arAmplitudes );
         }
         // If standard amplitude update
         else
         {
            // Calculate relative error-vector norms before CROP recompression.
            if (  this->mIndividualMcDecompThresh  )
            {
               this->UpdateRelativeErrorVectorNorms( error_vector );
            }

            // Update preconditioner
            this->UpdatePreconditioner( arAmplitudes, error_vector );

            // Update amplitudes
            this->UpdateAmplitudes( arAmplitudes, error_vector );

            // Update decomposition thresholds
            this->UpdateAdaptiveDecompThresholds();
         }

         // Update amplitude norms
         this->UpdateAmplitudeNorms(arAmplitudes);
      }

      /*** Output convergence data ***/
      this->OutputIterationInfo();


      /*** Increment mNIter ***/
      if (  this->mNIter >= this->mMaxIter
         && !this->mConverged
         )
      {
         MidasWarning("TensorNlSolver: Maximum iterations reached!");
         break;
      }
      if ( !this->mConverged )
      {
         ++this->mNIter;
      }
   }
   /************************************************/
   /*          END MAIN SOLVER LOOP                */
   /************************************************/
   
   /*** Output a summary (might be moved to outside solver at some point) ***/
   // Unmute Mout if previously muted to output summary and rank statistics
   if (unmute)
   {
      Mout.Unmute();
   }
   Mout << std::endl;
   Out72Char( Mout, '#', '#', '#' );
   OneArgOut72(Mout," Final results of VCC calculation ",'#');
   Out72Char( Mout, '#', '#', '#' );
   Mout << std::endl;
   // Basic summary for IoLevel = 1
   Mout << " Final VCC energy    = " << this->mEnergy << "  a.u" << std::endl;
   // Detailed summary for IoLevel > 1
   if (  io > I_1 )
   {
      Mout << " Final VCC energy    = " << this->mEnergy*C_AUTKAYS << "  cm^-1" << "\n"
           << " Final VCC energy    = " << this->mEnergy*C_AUTEV << "  eV" << "\n"
           << " VCC amplitude norm  = " << arAmplitudes.Norm() << "\n" 
           << " Final residual norm = " << this->mResidualNorm << "\n"
           << " Largest MC error    = " << this->mResidualMaxNorm
           << std::endl; 
   }
   Mout << std::endl;
   Out72Char( Mout, '#', '#', '#' );
   Mout << std::endl << std::endl;
   
   // Write final error vector (for testing)
   if (  gDebug
      )
   {
      const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
      this->WriteToDisc(error_vector, calc_name + "_Vcc_final_error_vec");
   }

   // Print rank statistics
   if (  !this->mDecompInfoSet.empty()
      )
   {
      Mout << std::fixed;
      midas::stream::ScopedPrecision(3, Mout);

      // Only analyze final ranks if we have not saved them from previous iterations
      if (  !this->mSaveRankInfo
         )
      {
         this->mAmplitudeRankStatistics.EmplaceRankInfo(arAmplitudes);
         this->mErrorVectorRankStatistics.EmplaceRankInfo(error_vector);
      }

      Mout << this->mAmplitudeRankStatistics << std::endl;
      Mout << this->mErrorVectorRankStatistics << std::endl;

      Mout << std::scientific;
   }
   if (  !this->mDecompInfoSet.empty()
      && this->mFullRankAnalysis
      )
   {
      Mout << std::fixed;
      midas::stream::ScopedPrecision(3, Mout);
      
      this->mAmplitudeRankStatistics.PerformRankAnalysis(arAmplitudes, this->mpTrf->GetXvecModeCombiOpRange());
      Mout << std::scientific;
   }
   Mout << std::endl;


   // Return convergence status
   return this->mConverged;
}



/**
 * Set the decomposition thresholds in all TensorDecompInfo%s.
 *
 * Change the values of the tensor-decomposition thresholds in all
 * TensorDecompInfo%s. This enables us to use adaptive decomposition
 * thresholds.
 *
 * @param arNewThreshold
 *    The new threshold
 */
void TensorNlSolver::UpdateAmplitudeDecompThresholds
   (  const Nb& arNewThreshold
   )
{
   midas::tensor::UpdateDecompThresholds(this->mDecompInfoSet, arNewThreshold);
}

/**
 * Get the largest decomp threshold in the info set.
 *
 * @return     Largest (loosest) threshold
 **/
Nb TensorNlSolver::GetMaxDecompThreshold
   (
   )  const
{
   return midas::tensor::GetDecompThreshold(this->mDecompInfoSet, false);
}

/**
 * Update the vector of error-vector norms relative to the update norms.
 * These are used for decomposition of the CROP subspace and inside the 
 * transformer.
 *
 * @param arErrorVector
 *    The VCC error vector
 **/
void TensorNlSolver::UpdateRelativeErrorVectorNorms
   (  const TensorDataCont& arErrorVector
   )
{
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();

   auto tmp = arErrorVector;
   this->PreconditionA0( tmp );

   TensorDataCont a0;

   const auto size = this->mRelativeErrorVectorNorms.size();

   for( size_t i=0; i<size; ++i )
   {
      //(||e^m||/||e^m*A_0^m||) for each MC
      auto norm = arErrorVector.GetModeCombiData(i).Norm();
      auto precon_norm = tmp.GetModeCombiData(i).Norm();

      if (  precon_norm > C_0 )
      {
         this->mRelativeErrorVectorNorms[i] = norm / precon_norm;
      }
      else
      {
         // Initialize A0 if needed
         if (  a0.Size() != size
            )
         {
            a0 = this->EnergyDenominators();
         }

         // Set relative norm to 1/||A0^-1||
         this->mRelativeErrorVectorNorms[i] = C_1 / a0.GetModeCombiData(i).Norm();
      }

      if (  io > I_9
         || gDebug
         )
      {
         Mout << "Relative errvec norm " << i << " = " << this->mRelativeErrorVectorNorms[i] << std::endl;
      }
   }

   // Write relative norms to file for restart
   const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
   const std::string file_name = calc_name + "_Vcc_relative_errvec_norms";

   // Remove _prev (to make the code more portable) and rename old to _prev
   const std::string file_name_prev = file_name + "_prev";
   if (  InquireFile(file_name_prev)
      )
   {
      std::remove(file_name_prev.c_str());
   }
   std::rename(file_name.c_str(), file_name_prev.c_str());

   // Write new file
   midas::mpi::OFileStream output(file_name, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::out | std::ios::binary);
   if (  !this->mRelativeErrorVectorNorms.empty()
      )
   {
      // Use function from util/read_write_binary.h
      write_binary_array(&this->mRelativeErrorVectorNorms[0], this->mRelativeErrorVectorNorms.size(), output);
   }
   output.close();
}

/**
 * Update amplitude norms
 *
 * @param arAmplitudes     The VCC amplitudes
 **/
void TensorNlSolver::UpdateAmplitudeNorms
   (  const TensorDataCont& arAmplitudes
   )
{
   auto size = arAmplitudes.Size();

   assert(this->mAmplitudeNorms.size() >= size);

   for(size_t i=0; i<size; ++i)
   {
      this->mAmplitudeNorms[i] = arAmplitudes.GetModeCombiData(i).Norm();
   }
}

/**
 * Determines whether Laplace denominators are needed or not based on 
 * the TensorDecompInfo and tensor order.
 *
 * @param aOrder
 *    The requested tensor order
 *
 * @return
 *    True if Laplace denominators are needed.
 **/
bool TensorNlSolver::UseLaplaceDenominators
   (  const In& aOrder
   )  const
{
   bool do_laplace = false;

   for(const auto& decomp_info : this->mDecompInfoSet)
   {
      do_laplace = (do_laplace || decomp_info.at("DECOMPTOORDER").get<In>() <= aOrder);
   }

   return do_laplace;
}

/**
 * Initialize mLaplaceQuadVector
 **/
bool TensorNlSolver::InitLaplaceQuadrature
   (
   )
{
   // Declare needed pointers and references
   const auto* const vcc = this->mpTrf->pVcc();
   const auto& mc_op_range = this->mpTrf->GetTransXvecModeCombiOpRange();
   const auto& nmodals = vcc->pVccCalcDef()->GetNModals();
   const auto& eigvals = vcc->GetEigVal();
   const auto& offsets = vcc->GetOccModalOffSet();
   
   // Reserve space in vector
   auto mcr_size = mc_op_range.Size();
   auto size = mcr_size - I_1;
   this->mLaplaceQuadVector.reserve(size);

   // Loop over mode combis
   size_t ilap = 0;
   for(const auto& imc : mc_op_range)
   {
      if (  imc.Size() == I_0
         )
      {
         continue;
      }

      const auto& mcvec = imc.MCVec();

      this->mLaplaceQuadVector.emplace_back(*this->mLaplaceInfo.begin(), mcvec, nmodals, eigvals, offsets, 0.);
      ++ilap;
   }

   return true;
}

/**
 * Initialize mPreconditioner if it is needed.
 * The possibilities are:
 *    - No initialization
 *    - Initialize to inverse 0th-order Jacobian
 *    - Initialize to true Jacobian diagonal inverse
 *    - Initalaize to vector of ones (unit matrix)
 *
 * @param arAmplitudes
 *    The VCC amplitudes used for initializing true Jacobian diag.
 *
 * @return
 *    True if mPreconditioner has been initialized
 **/
bool TensorNlSolver::InitPreconditioner
   (  const TensorDataCont& arAmplitudes
   )
{
   if (  this->mIdja
      && !this->mTrueADiag
      )
   {
      if ( this->mIdjaUnitMatrixGuess  )
      {
         this->mPreconditioner = arAmplitudes; // Set shape
         this->mPreconditioner.Zero();   // Zero
         this->mPreconditioner.ElementwiseScalarAddition(C_1); // Add ones
         return true;
      }
      else
      {
         this->mPreconditioner = this->EnergyDenominators();
         return true;
      }
   }
   else if  (  this->mTrueADiag  )   // If true A diagonal
   {
      this->mPreconditioner = this->TrueInvJacobianDiagonal(arAmplitudes);
      return true;
   }

   return false;
}

/**
 * Calculate the 0th-order inverse Jacobian using the appropriate tensor format
 * determined by TensorDecompInfo.
 *
 * The inverse Jacobian for a given MC is set to the corresponding
 * EnergyDenominatorTensor for Laplace-decomposed denominators
 * and ExactEnergyDenominatorTensor for SimpleTensor%s.
 * If the TensorNlSolver::mCheckLaplaceFit is set,
 * the errors of the Laplace fittings are printed.
 *
 * @param   arStartingOrder
 *    Calculate denominators from this order and up
 *    
 * @return
 *    TensorDataCont of modal energy denominators.
 **/
TensorDataCont TensorNlSolver::EnergyDenominators
   (  const In& arStartingOrder
   )  const
{
   // Assert sane starting order
   MidasAssert(arStartingOrder >= I_0, "TensorNlSolver::EnergyDenominators(): Starting order smaller than zero!");

   // Declare needed pointers and references
   const auto* const vcc = this->mpTrf->pVcc();
   const auto& mc_op_range = this->mpTrf->GetTransXvecModeCombiOpRange();
   const auto& nmodals = vcc->pVccCalcDef()->GetNModals();
   const auto& eigvals = vcc->GetEigVal();
   const auto& offsets = vcc->GetOccModalOffSet();

   // Check mLaplaceQuadVector
   bool have_laplace_quad = !this->mLaplaceQuadVector.empty();
   In num_points = I_4;

   // Declare error values for CheckLaplaceFit
   Nb error2 = C_0;  // The sum of squared difference norms
   Nb max_error = C_0;

   // Declare result of given size
   In result_size
      =  mc_op_range.Size()   // Size of the MCR (including ref)
      -  mc_op_range.ReducedSize(arStartingOrder-I_1); // Minus number of lower-order MCs (including ref)
   TensorDataCont result(result_size);

   // Loop over mode combinations
   In count = I_0;
   In mc_size = I_0;
   In mc_count = I_0;
   for(const auto& mode_combi : mc_op_range)
   {
      mc_size = mode_combi.Size();
      const auto& mcvec = mode_combi.MCVec();

      // Exclude reference configuration
      if ( mc_size < arStartingOrder )
      {
         if (  mc_size != 0
            )
         {
            ++mc_count;
         }

         continue;
      }

      auto& tens = result.GetModeCombiData(count);

      // If CanonicalTensor%s are used
      if (  this->UseLaplaceDenominators(mc_size)
         )
      {
         if (  this->mCheckLaplaceFit
            || gDebug
            )
         {
            bool initialized = !this->mLaplaceQuadVector.at(mc_count).IsNullPtr();
            bool mute = Mout.Muted();
            Mout.Unmute();
            Mout  << " == Constructing EnergyDenominatorTensor == \n"
                  << "  MC:         " << mode_combi << "\n"
                  << "  mc_count:   " << mc_count << "\n"
                  << std::flush;
            if (  initialized
               )
            {
               Mout  << "  Num points: " << this->mLaplaceQuadVector.at(mc_count).NumPoints() << "\n"
                     << std::flush;
            }
            else
            {
               Mout  << "  Uninitialized quadrature!\n"
                     << std::flush;
            }
            if (  mute  )
            {
               Mout.Mute();
            }
         }

         tens  =  have_laplace_quad
               ?  NiceTensor<Nb>( new EnergyDenominatorTensor<Nb>(mcvec, nmodals, eigvals, offsets, this->mLaplaceQuadVector.at(mc_count)) )
               :  NiceTensor<Nb>( new EnergyDenominatorTensor<Nb>(mcvec, nmodals, eigvals, offsets, num_points) );

         if (  this->mCheckLaplaceFit
            || gDebug
            )
         {
            bool mute = Mout.Muted();
            Mout.Unmute();

            // Check quality of Laplace fit
            const auto exact  = NiceTensor<Nb>( new ExactEnergyDenominatorTensor<Nb>(mcvec, nmodals, eigvals, offsets) );
            const Nb diff_norm2 = safe_diff_norm2(tens.GetTensor(), exact.GetTensor());
   
            Mout  << Idt(2)+"Laplace error in mode combi: " << mode_combi << " :\n"
                  << Idt(2)+"Abs        = " << std::sqrt(diff_norm2) << "\n"
                  << Idt(2)+"Norm rel   = " << std::sqrt(diff_norm2) / exact.Norm() << std::endl;
            Mout << Idt(2)+"Quad points = " << tens.StaticCast<CanonicalTensor<Nb>>().GetRank() << std::endl;

            error2 += diff_norm2;
            max_error = std::max<Nb>(std::sqrt(diff_norm2)/exact.TotalSize(), max_error);

            if (  mute  )
            {
               Mout.Mute();
            }
         }
      }
      else  // If SimpleTensor
      {
         tens = NiceTensor<Nb>( new ExactEnergyDenominatorTensor<Nb>(mcvec, nmodals, eigvals, offsets) );
      }

      ++count;
      ++mc_count;
   }

   if (  this->mCheckLaplaceFit
      || gDebug
      )
   {
      bool mute = Mout.Muted();
      Mout.Unmute();

      Mout << Idt(2)+"Mean Laplace error = " << std::sqrt(error2)/result.TotalSize() << std::endl;
      Mout << Idt(2)+"|error|/|norm|     = " << std::sqrt(error2)/result.Norm() << std::endl;
      Mout << Idt(2)+"Max Laplace error  = " << max_error << std::endl;
      
      if (  mute  )
      {
         Mout.Mute();
      }
   }

   return result;
}


/**
 * Precondition the input TensorDataCont by performing the elementwise (Hadamard) product
 * with the inverse 0th-order Jacobian (modal-energy denominators).
 *
 * @param arTdcIn
 *    The input TensorDataCont
 * @param arFlag
 *    Inverse or non-inverse transformation (-3 gives A0^-1, -2 or higher gives A0)
 **/
void TensorNlSolver::PreconditionA0
   (  TensorDataCont& arTdcIn
   ,  const In& arFlag
   )  const
{
   // Call the transformer method and assign the result
   arTdcIn = this->mpTrf->TensorTransformerA0(arTdcIn, arFlag, C_0, this->mLaplaceQuadVector);
}


/**
 * Return the true inverse Jacobian diagonal based on the given set of amplitudes
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 * @return
 *    The inverse Jacobian diagonal
 **/
TensorDataCont TensorNlSolver::TrueInvJacobianDiagonal
   (  const TensorDataCont& arAmplitudes
   )  const
{
   auto result = arAmplitudes; // Set shape

   auto ones = arAmplitudes; // Set shape
   ones.Zero();  // Zero
   ones.ElementwiseScalarAddition(C_1); // Add ones
   Nb e_vscf = this->mpTrf->pVcc()->GetEtot();
   this->mpTrf->Transform(ones, result, I_0, -I_3, e_vscf); // Transform

   // Niels: The transformer actually returns the negative diagonal for some reason
   result.Scale(-C_1);
   MidasWarning("Niels: TrueHDiag for TensorNlSolver is not a good idea!");

   if (  gDebug   )
   {
      auto denom = this->EnergyDenominators();
      auto diff = result;
      diff -= denom;
      Mout << "\n|| A1 - A0 || = " << diff.Norm() << "\n" << std::endl;
   }


   // Decompose if needed
   if ( !this->mDecompInfoSet.empty() )
   {
      result.Decompose(this->mDecompInfoSet);
   }
   
   return result;
}


/**
 * The inverse Jacobian is updated by requiring the approximate inverse Jacobian
 * to satisfy the secant equation \f$ B_{k+1}s_k = y_k \f$ (with some slight modifications)
 * under the constraint that \f$B\f$ must remain diagonal and the deviation between \f$B_k\f$ and \f$B_{k+1}\f$
 * is minimized. This is explained further in [10.1155/2013/875935](https://www.hindawi.com/journals/jam/2013/875935/).
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 * @param arPrevAmps
 *    The previous amplitudes
 * @param arErrorVec
 *    The current error vector
 * @param arPrevErrorVec
 *    The previous error vector
 *
 * @return
 *    The norm of y = e - e_prev (that determines if the update is performed).
 *    If mNIter = 1 - i.e. no previous error vector is available - return -1.
 **/
Nb TensorNlSolver::IdjaUpdate
   (  const TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arPrevAmps
   ,  const TensorDataCont& arErrorVec
   ,  const TensorDataCont& arPrevErrorVec
   )
{
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();

   Nb delta_e_norm = -C_1;

   // Update energy denominators (inverse Jacobian)
   if ( this->mNIter > I_1 )
   {
      // Short flow at IoLevel 3
      Mout << Idt(1)+"## Performing IDJA update ##" << std::endl;

      // Init inverse Jacobian update as e_n - e_{n-1}
      auto inv_jac_update = arErrorVec;
      inv_jac_update -= arPrevErrorVec;

      delta_e_norm = inv_jac_update.Norm();

      // More details at IoLevel 7
      if (  io > I_7 )
      {
         Mout << Idt(2)+"|| e_n - e_{n-1} || = " << delta_e_norm << std::endl;
      }

      if ( delta_e_norm >= this->mIdjaThresh )
      {
         // Init s = t_{k}-t_{k-1}
         TensorDataCont s = arAmplitudes;
         s -= arPrevAmps;

         Nb s_norm2 = s.Norm2();

         if (  io > I_6 )
         {
            Mout << Idt(2)+"s norm2 = " << s_norm2 << std::endl;
         }

         Nb update_coeff = C_0;
         {
            auto& y = inv_jac_update;

            // Initialize the coefficient for modifying y to the previous error vector norm
            Nb y_coeff = this->mPreviousResidualNorm;

            Nb vk_dot = -Dot(y, s) / s_norm2;

            if (  io > I_6 )
            {
               Mout << Idt(2)+"vk_dot = " << vk_dot << std::endl;
            }

            y_coeff *= (C_1 + std::max(vk_dot, C_0));

            if (  io > I_6 )
            {
               Mout << Idt(2)+"Final y_coeff = " << y_coeff << std::endl;
               Mout << std::endl;
            }

            // Perform the Li and Fukushima modification
            y.Axpy(s, y_coeff);

            // Calculate coefficient of the inverse-Jacobian update
            update_coeff += Dot(y, s);

            auto tmp_By = this->mPreconditioner;
            tmp_By *= y;
            update_coeff -= Dot(y,tmp_By);
         }

         // Save y for debug checking the quasi-Cauchy condition
         TensorDataCont y_ref;
         if (  gDebug
            || this->mCheckIdja
            )
         {
            y_ref = inv_jac_update;
         }

         // Finish inv_jac_update by squaring all elements and finishing the coefficient
         inv_jac_update *= inv_jac_update;
         Nb denom = inv_jac_update.Norm2();
         update_coeff /= denom;

         // Save old Jacobian as guess for CP Decomposition
         TensorDataCont prev_inv_jac;
         if (  !this->mDecompInfoSet.empty()
            || this->mCheckIdja
            || gDebug
            )
         {
            prev_inv_jac = this->mPreconditioner;
         }

         // Perform Jacobian update
         this->mPreconditioner.Axpy(inv_jac_update, update_coeff);

         // Debug: Calculate angle between old and new inverse Jacobian
         if (  gDebug
            || this->mCheckIdja
            )
         {
            bool mute = Mout.Muted();
            Mout.Unmute();

            // Check angles and norms
            auto ref_inv_jac = this->EnergyDenominators();
            Nb ref_norm = ref_inv_jac.Norm();
            Nb prev_norm = prev_inv_jac.Norm();
            Nb norm = this->mPreconditioner.Norm();

            Mout  << Idt(3)+"# Checking IDJA Jacobian compared to previous and 0th-order #" << std::endl;
            Mout  << Idt(3)+"Angle between old and new inverse Jacobian = " 
                  << Angle(prev_inv_jac, this->mPreconditioner, prev_norm, norm) << "\n"
                  << Idt(3)+"Angle between 0th-order and new inverse Jacobian = " 
                  << Angle(ref_inv_jac, this->mPreconditioner, ref_norm, norm) << "\n"
                  << Idt(3)+"Previous norm = "   << prev_norm << "\n"
                  << Idt(3)+"Current norm  = "   << norm << "\n"
                  << Idt(3)+"0th-order norm   = " << ref_norm 
                  << std::endl << std::endl;

            // Check quasi-Cauchy condition
            Nb qc = Dot(y_ref, s);
            {
               auto tmp = y_ref;
               tmp *= this->mPreconditioner;
               qc -= Dot(y_ref, tmp);
            }
            Mout << Idt(3)+"Checking quasi-Cauchy condition:" << std::endl;
            Mout << Idt(3)+"ys - yBy = " << qc << " (should be zero)" << std::endl;
            Mout << std::endl;

            if (  mute  )
            {
               Mout.Mute();
            }
         }

         // Decompose inverse Jacobian using previous as guess
         if ( !this->mDecompInfoSet.empty() )
         {
            this->mPreconditioner.Decompose(this->mDecompInfoSet, prev_inv_jac);
         }
      }
      else
      {
         Mout << Idt(2)+"Skipped IDJA update because the norm of the error vector change is: " 
              << delta_e_norm << " < " << this->mIdjaThresh << std::endl;
      }
      Mout << std::endl;
   }

   // Return norm of the error-vector change (or -1 if mNIter = 1)
   return delta_e_norm;
}


/**
 * Update all amplitudes with the perturbative quasi-Newton method 
 * using a pre-calculated inverse diagonal Jacobian.
 *
 * The amplitudes are updated using 
 * \f$ \Delta t_{\mu^{\mathbf{m}}}^{(n+1)} = -(A_{diag}^{-1})_{\mu^{\mathbf{m}}}*e_{\mu^{\mathbf{m}}}^{(n)} \f$
 * with \f$ A_{diag} \f$ being a diagonal representation of the Jacobian. 
 * This is the 0th-order VCC Jacobian unless it has been updated by IDJA.
 * The function can also scale the update by a given step size.
 *
 * @param  arAmplitudes 
 *    Amplitudes as TensorDataCont
 * @param  arErrorVector 
 *    Error vector as TensorDataCont
 *
 * @return   The norm of the update step.
 **/
Nb TensorNlSolver::QuasiNewtonUpdate
   (  TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arErrorVector
   )  const
{
   // Construct delta_t (without the minus sign)
   TensorDataCont quasi_newton_update = arErrorVector;
   if (  this->mIdja
      || this->mTrueADiag
      )
   {
      quasi_newton_update *= this->mPreconditioner;
   }
   else if  (  this->mUsePrecon  )
   {
      this->PreconditionA0(quasi_newton_update);
   }

   // Subtract scaled delta_t
   arAmplitudes.Axpy( quasi_newton_update, -this->mBackstepLambda );

   return this->mMaxNormConvCheck ? quasi_newton_update.MaxNorm() : quasi_newton_update.Norm();
}


/**
 * The amplitudes are updated based on the full Newton-Raphson method
 * where the exact VCC Jacobian is used. This involves solving the
 * linear equations \f$ \mathbf{A} \Delta \mathbf{t} = - \mathbf{e} \f$
 * using a linear equation solver.
 * In practice the negative update is calculated and thereafter subtracted
 * from the current amplitudes.
 * This function is not used for backsteps. Instead the update vector is saved
 * and scaled with the new step size if backstep is called.
 * If mNrInfo.mJacobianMaxExci is smaller than the excitation level of the transformer,
 * the linear equations are only solved in the reduced space, and the rest of the update
 * is calculated from the modal-energy denominators.
 *
 * @param arAmplitudes
 *    The VCC trial vector.
 * @param arErrorVector
 *    The error vector.
 *
 * @return
 *    The norm of the update step.
 **/
Nb TensorNlSolver::NewtonRaphsonUpdate
   (  TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arErrorVector
   )
{
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();

   Mout << Idt(2)+"Performing Newton-Raphson update." << std::endl;
   Mout << std::endl;

   // Transform error vector with inverse Jacobian to obtain the update (with a minus sign)
   auto update = this->InverseJacobianTransformation(arAmplitudes, arErrorVector);

   // Calculate update norm
   auto update_norm = update.Norm();

   // Calculate angle between Newton-Raphson update and 0th-order update
   if (  this->mCheckNewtonRaphson
      || gDebug
      )
   {
      bool mute = Mout.Muted();
      Mout.Unmute();

      Mout << std::endl;
      Mout << Idt(3)+"# Comparing Newton-Raphson update to 0th-order update #" << std::endl;
      auto qn_update = arErrorVector;
      this->PreconditionA0( qn_update );
      auto qn_norm = qn_update.Norm();
      auto error_norm = arErrorVector.Norm();
      

      Mout  << Idt(3)+"Angle between 0th-order update and full Newton-Raphson = " 
            << Angle(update, qn_update, update_norm, qn_norm) << "\n"
            << Idt(3)+"Angle between residual vector and full Newton-Raphson update = "
            << Angle(update, arErrorVector, update_norm, error_norm) 
            << std::endl;
      if (  this->mTrueADiag  )
      {
         auto true_diag_update = this->TrueInvJacobianDiagonal(arAmplitudes);
         true_diag_update *= arErrorVector;
         auto true_diag_update_norm = true_diag_update.Norm();
         Mout  << Idt(3)+"Angle between true-Jacobian-diagonal update and full Newton-Raphson update = "
               << Angle(update, true_diag_update, update_norm, true_diag_update_norm) << "\n"
               << Idt(3)+"Norm of true Jacobian diagonal update   = " << true_diag_update_norm
               << std::endl;
      }
      Mout  << Idt(3)+"Norm of 0th-order update                = " << qn_norm << "\n"
            << Idt(3)+"Norm of residual                        = " << error_norm << "\n"
            << Idt(3)+"Norm of Newton-Raphson update           = " << update_norm
            << std::endl << std::endl;

      if (  mute  )
      {
         Mout.Mute();
      }
   }

   // Rescale update if the norm is larger than maximum allowed step size.
   // As default there is no restriction, since the line search takes care of it all in case of divergence.
   Nb step_size = C_1;
   const Nb& delta = this->mNrInfo.mStepSizeControl;
   if (  update_norm > delta
      && delta > C_0
      )
   {
      // Output detailed flow at IoLevel 4
      Mout << std::endl;
      Mout  << Idt(2)+"Norm of Newton-Raphson update is "
            << update_norm 
            << " > Delta = "
            << delta
            << ".\nThe update will be rescaled to norm Delta!" 
            << std::endl;
      step_size = delta / update_norm;
      update_norm = delta;
   }

   arAmplitudes.Axpy(update, -step_size);

   // Save update
   std::swap(this->mPrevNewtonRaphsonUpdate, update);

   return this->mMaxNormConvCheck ? step_size*update.MaxNorm() : update_norm;
}


/**
 * Transform a TensorDataCont by the inverse Jacobian by solving linear
 * equations. The inverse Jacobian can either be the full Jacobian
 * or a truncated approximation (e.g. using the VCC[2] Jacobian for VCC[3]
 * with the 0th-order approximation for the triples).
 *
 * @param arAmplitudes
 *    The VCC amplitudes needed to construct the Jacobian transformer
 * @param arRhs
 *    The right-hand side (the vector to transform by the inverse Jacobian)
 * @return
 *    The transformed RHS
 **/
TensorDataCont TensorNlSolver::InverseJacobianTransformation
   (  const TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arRhs
   )
{
   // Mute Mout during Jacobian transformations if IoLevel < 6
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();
   bool unmute = !Mout.Muted();
   if (  io < I_6 )
   {
      Mout.Mute();
   }

   // Set references
   const auto& precon_exci = this->mNrInfo.mJacobianMaxExci;
   const auto& trf_exci = this->mpTrf->GetMaxExciLevel();

   // Initialize size of reduced amplitude space
   const In full_size = arAmplitudes.Size();
   In reduced_size = C_0;

   // Set transformer to VCCJAC
   this->mpTrf->SetToVccJac(arAmplitudes);

   // Declare solver variables
   std::unique_ptr<midas::vcc::TransformerV3> new_trf = nullptr;   // New transformer used for improved preconditioning
   std::unique_ptr<MidasTensorDataContTransformer> ies_trf = nullptr;   // Transformer interface for linear eq solver
   std::vector<TensorDataCont> rhs;
   rhs.reserve(I_1);

   if (  precon_exci < trf_exci && precon_exci > I_0  )
   {
      // Calculate size of reduced amplitudes and residuals. Exclude reference configuration.
      reduced_size = this->mpTrf->GetXvecModeCombiOpRange().ReducedSize(precon_exci, I_1);

      // Init new transformer and rhs
      TensorDataCont reduced_amplitudes(reduced_size);
      TensorDataCont reduced_rhs(reduced_size);
      for(In i=I_0; i<reduced_size; ++i)
      {
         reduced_amplitudes.GetModeCombiData(i) = arAmplitudes.GetModeCombiData(i);
         reduced_rhs.GetModeCombiData(i) = arRhs.GetModeCombiData(i);
      }

      new_trf = std::unique_ptr<midas::vcc::TransformerV3>( new midas::vcc::TransformerV3(*this->mpTrf, precon_exci, reduced_amplitudes) );
      ies_trf = std::unique_ptr<MidasTensorDataContTransformer>( new MidasTensorDataContTransformer(*new_trf, true) ); // exclude ref.

      rhs.emplace_back(reduced_rhs);
   }
   else
   {
      ies_trf = std::unique_ptr<MidasTensorDataContTransformer>( new MidasTensorDataContTransformer(*this->mpTrf, true) ); // exclude ref.
      rhs.emplace_back(arRhs);
   }

   // Solve the linear equations
   auto result =  this->mNrInfo.mDiagonalPreconditioner
               ?  this->SolveNewtonRaphsonEquations<TensorDiagPreconLinearEquationSolver>(ies_trf.get(), rhs)
               :  this->SolveNewtonRaphsonEquations<TensorLinearEquationSolver>(ies_trf.get(), rhs);

   // Add the rest of the elements if reducing the Jacobian exci level.
   if (  precon_exci < trf_exci && precon_exci > I_0 )
   {
      TensorDataCont total_result(full_size);
      const In reduced_size = result.Size();
      auto rest_size = full_size - reduced_size;

      // Set the first part of the update
      In count = I_0;
      for(In i=I_0; i<reduced_size; ++i)
      {
         total_result.GetModeCombiData(count) = result.GetModeCombiData(i);
         ++count;
      }
      // Set the rest of the update
      for(In j=I_0; j<rest_size; ++j)
      {
         total_result.GetModeCombiData(count) = arRhs.GetModeCombiData(count);
         if (  this->mUsePrecon  )
         {
            // Construct preconditioner for the rest and multiply
            auto rest_precon = this->EnergyDenominators(precon_exci+I_1);
            total_result.GetModeCombiData(count) *= rest_precon.GetModeCombiData(j);
         }
         ++count;
      }

      result = std::move(total_result);
   }

   // Reset transformer to VCC
   this->mpTrf->SetType(TRANSFORMER::VCC);

   if (  unmute   )
   {
      Mout.Unmute();
   }

   // Return transformed vector
   return result;
}



/**
 * The amplitudes are updated using a linear combination of previous 
 * trial vectors and residuals.
 * If CROP is used, the subspaces are optimized which allows us to 
 * store less vectors (3 would be enough if the Jacobian was symmetric).
 * However, when using decomposed tensors the optimized CROP vectors 
 * need recompression which can be expensive and potentially inaccurate.
 * See Ref. [10.1063/1.2928803](http://scitation.aip.org/content/aip/journal/jcp/128/20/10.1063/1.2928803).
 *
 * @param arAmplitudes
 *    VCC amplitudes
 * @param arErrorVector
 *    VCC error vector
 *
 * @return
 *    Norm of update 
 **/
Nb TensorNlSolver::SubspaceUpdate
   (  TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arErrorVector
   )
{
   const auto& io = this->mpTrf->pVccCalcDef()->IoLevel();

   // Initialize optimized residual (called update)
   TensorDataCont update = arErrorVector;

   // Precondition saved residuals
   if (  this->mPreconSavedResiduals   )
   {
      this->PreconditionA0( update );
   }

   // Add x_tilde and V(x_tilde) to subspace
   this->mTrials.emplace_back(arAmplitudes);
   this->mResiduals.emplace_back(update);

   // Remove old vectors if subspace is too big
   if ( this->mTrials.size() > this->mNSubspaceVecs )
   {
      this->mTrials.pop_front();
   }
   if ( this->mResiduals.size() > this->mNSubspaceVecs )
   {
      this->mResiduals.pop_front();
   }

   const In n = this->mTrials.size();

   const std::string method = this->mCrop ? "CROP" : "DIIS";

   // Calculate CROP/DIIS matrix if subspace is larger than 1
   // (assume symmetric B due to symmetric inverse 0th-order Jacobian)
   // and solve linear system.
   if ( n > I_1 )
   {
      Mout << Idt(2)+"Performing " + method + " update using " << n << " subspace vectors." << std::endl;
      Mout << std::endl;
      MidasAssert(this->mResiduals.size() == n, "TensorNlSolver: " + method + " subspaces have different sizes!");

      // Set size of subspace coefficients
      this->mSubspaceCoeffs.resize(n);

      // If Newton-Raphson we use non-symmetric version
      if (  this->mNewtonRaphson
         && this->mImprovedPreconInSubspaceMatrix
         )
      {
         this->mSubspaceCoeffs = this->CalculateSubspaceCoefficientsNSym(arAmplitudes);
      }
      else
      {
         this->mSubspaceCoeffs = this->CalculateSubspaceCoefficientsSym();
      }

      // Output vector elements (c_{n+1} -> c_0 ) for IoLevel > 5
      if (  io > I_5 )
      {
         Mout << Idt(2) + method + " coefficient vector (c_{n+1}, c_n, c_{n-1},...):" << std::endl;
         auto rit = this->mSubspaceCoeffs.rbegin();
         for(; rit!=this->mSubspaceCoeffs.rend(); ++rit)
         {
            Mout << Idt(2) << *rit << std::endl;
         }
         Mout << std::endl;
      }

      // Calculate optimized trials and residuals
      update = LinComb(this->mResiduals, this->mSubspaceCoeffs);
      arAmplitudes = LinComb(this->mTrials, this->mSubspaceCoeffs);

      // If CROP: Replace last vectors with the optimized ones and decompose
      if ( this->mCrop )
      {
         // Optimize the subspace vectors
         this->mTrials.back() = arAmplitudes;
         this->mResiduals.back() = update;

         // Decompose optimized vectors
         this->DecomposeOptimizedCropVectors();
      }
   }

   // Write restart vectors
   {
      const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
      const std::string trial_file_name = calc_name + "_Vcc_" + method + "_trial_" + std::to_string(this->mNIter);
      const std::string resid_file_name = calc_name + "_Vcc_" + method + "_resid_" + std::to_string(this->mNIter);

      if (  gDebug   )
      {
         Mout  << " Writing CROP restart vectors:" << std::endl;
         Mout  << "    Norms of vectors:" << std::endl;
      }
      if (  n > I_1  )
      {
         this->WriteToDisc(this->mTrials.back(), trial_file_name);
         this->WriteToDisc(this->mResiduals.back(), resid_file_name);
         if (  gDebug   )
         {
            Mout  << "    Amp: " << this->mTrials.back().Norm() << "\n"
                  << "    Err: " << this->mResiduals.back().Norm() << "\n"
                  << std::flush;
         }
      }
      else if  (  n == I_1 )
      {
         this->WriteToDisc(arAmplitudes, trial_file_name);
         this->WriteToDisc(arErrorVector, resid_file_name);
         if (  gDebug   )
         {
            Mout  << "    Amp: " << arAmplitudes.Norm() << "\n"
                  << "    Err: " << arErrorVector.Norm() << "\n"
                  << std::flush;
         }
      }
   }

   // Remove oldest vectors. Amplitudes and error vectors will be added in next update.
   if ( this->mTrials.size() >= this->mNSubspaceVecs )
   {
      this->mTrials.pop_front();
   }
   if ( this->mResiduals.size() >= this->mNSubspaceVecs )
   {
      this->mResiduals.pop_front();
   }

   // Perform update
   Nb update_norm = C_0;
   if ( this->mPreconSavedResiduals )
   {
      arAmplitudes.Axpy( update, -this->mBackstepLambda );
      update_norm = update.Norm();
   }
   else if  ( this->mNewtonRaphson )
   {
      update_norm = this->NewtonRaphsonUpdate( arAmplitudes, update );
   }
   else  // The use of preconditioning is taken care of in QuasiNewtonUpdate
   {
      update_norm = this->QuasiNewtonUpdate( arAmplitudes, update );
   }

   return update_norm;
}


/**
 * Recompress the new, optimized CROP vectors to avoid rank growth.
 **/
void TensorNlSolver::DecomposeOptimizedCropVectors
   (
   )
{
   if ( !this->mDecompInfoSet.empty() )
   {
      const auto& io = this->mpTrf->pVccCalcDef()->IoLevel();

      midas::stream::ScopedPrecision(3, Mout);

      // Update thresholds for trial-vector decomposition
      if (  this->mAdaptiveDecompThreshold   )
      {
         this->UpdateAmplitudeDecompThresholds( this->mAmpDecompThresh );
      }

      Mout << Idt(2)+"Decomposing new CROP subspace vectors." << std::endl;
//      Mout << Idt(2)+"Decomposing new CROP subspace trial vectors with T_CP = " << this->mAmpDecompThresh << std::endl;

      this->mTrials.back().Decompose( this->mDecompInfoSet );

      if (  io > I_5 )
      {
         Mout << Idt(3)+"Average ranks of highest-order CROP trials:" << std::endl;
         Mout << Idt(3);
         for(const auto& tdc : this->mTrials)
         {
            Mout << std::setw(12) << tdc.AverageRankVec().back();
         }
         Mout << std::endl;
      }

      // Decompose preconditioned residuals like the amplitudes
      if (  this->mPreconSavedResiduals   )
      {
         this->mResiduals.back().Decompose( this->mDecompInfoSet );
      }
      // Decompose with argument thresholds
      else if  (  this->mIndividualMcDecompThresh  )
      {
         auto thresholds = this->mRelativeErrorVectorNorms;

         // Scale all relative norms by amplitude decomp threshold
         auto scaling = this->mAmpDecompThresh;
         std::transform
            (  thresholds.begin()
            ,  thresholds.end()
            ,  thresholds.begin()
            ,  [&scaling](Nb aNb) { return aNb*scaling; }
            );

         this->mResiduals.back().Decompose( this->mDecompInfoSet, thresholds );
      }
      // Otherwise: Decompose pure residuals using the decomp info of the transformer
      else
      {
         if (  this->mAdaptiveTrfDecompThreshold   )
         {
            this->mpTrf->UpdateDecompThresholds( this->mTrfDecompThresh );
         }
         this->mResiduals.back().Decompose( this->mpTrf->GetDecompInfo() );
      }

      if (  io > I_5 )
      {
         Mout << Idt(3)+"Average ranks of highest-order CROP residuals:" << std::endl;
         Mout << Idt(3);
         for(const auto& tdc : this->mResiduals)
         {
            Mout << std::setw(12) << tdc.AverageRankVec().back();
         }
         Mout << std::endl;
      }
   }
}


/**
 * Solve linear system for DIIS or CROP update using
 * a symmetric preconditioner (inverse Jacobian diagonal or IDJA).
 *
 * @return  The vector of coefficients
 **/
std::vector<Nb> TensorNlSolver::CalculateSubspaceCoefficientsSym
   (
   )  const
{
   const auto& io = this->mpTrf->pVccCalcDef()->IoLevel();

   const In n = this->mTrials.size();

   // Allocate matrix and RHS vector for linear system
   auto matrix = std::unique_ptr<Nb[]>(new Nb[(n+1)*(n+2)/2]);
   auto coeffs = std::unique_ptr<Nb[]>(new Nb[n+1]);
   auto matrix_ptr = matrix.get();

   // Loop over subspace vectors
   TensorDataCont tmp;
   In i=I_0;
   for(auto left=this->mResiduals.begin(); left!=this->mResiduals.end(); ++left)
   {
      for(auto right=left; right!=this->mResiduals.end(); ++right)
      {
         tmp = (*right);

         // Precondition
         if (  this->mUsePrecon  )
         {
            if (  this->mPreconSavedResiduals   )
            {
               // Transform one side with A0 to get only one A0^-1
               this->PreconditionA0( tmp, -I_2 );
            }
            else if  (  this->mIdja
                     || this->mTrueADiag
                     )
            {
               tmp *= this->mPreconditioner;
            }
            else
            {
               this->PreconditionA0(tmp);
            }
         }

         // Calculate dot
         *(matrix_ptr++) = Dot(*left,tmp);
      }

      // Last elements of first n-1 rows are -1
      *(matrix_ptr++) = -C_1;
      // All RHS elements except the last are 0
      coeffs[i++] = C_0;
   }
   // The last matrix element (n,n) is 0
   *(matrix_ptr++) = C_0;
   // The last RHS element is -1
   coeffs[n] = -C_1;

   // Output matrix
   if (  io > I_6
      || gDebug
      )
   {
      Mout << Idt(2)+"B matrix:" << std::endl;
      midas::stream::ScopedPrecision(12, Mout);

      auto matrix_ptr = matrix.get();

      for(size_t i=I_0; i<n+1; ++i)
      {
         Mout << Idt(2);
         for(size_t k=I_0; k<i; ++k)
         {
            Mout << std::setw(20) << "";
         }
         for(size_t j=i; j<n+1; ++j)
         {
            Mout << std::setw(20) << *(matrix_ptr++);
         }
         Mout << std::endl;
      }
      Mout << std::endl;
   }

   // Solve the linear system
   char l='L';
   int nn = n+1;
   int nrhs = 1;
   int info;
   midas::lapack_interface::sptrfs
      (  &l,  &nn,  &nrhs
      ,  matrix.get()
      ,  coeffs.get()
      ,  &nn,  &info
      );

   // Pack coeffs into vector
   std::vector<Nb> result(n);
   for(In i=I_0; i<n; ++i)
   {
      result[i] = coeffs[i];
   }

   return result;
}


/**
 * Solve the linear equations for CROP with a non-symmetric preconditioner
 * (e.g. improved preconditioner from truncated Newton-Raphson).
 *
 * @param arAmplitudes
 *    The VCC amplitudes used for initializing the Jacobian transformer
 * @return
 *    The subspace coefficients
 **/
std::vector<Nb> TensorNlSolver::CalculateSubspaceCoefficientsNSym
   (  const TensorDataCont& arAmplitudes
   )
{
   // Declare transformed residuals
   std::list<TensorDataCont> trans_resids;

   // Transform the residuals
   for(const auto& resid : this->mResiduals)
   {
      // Niels: This should be done more efficiently by solving all linear systems at the same time!
      trans_resids.push_back( this->InverseJacobianTransformation(arAmplitudes, resid) );
   }

   // Create the CROP matrix
   const In n = this->mTrials.size();

   // Allocate matrix and RHS vector for linear system
   auto matrix = std::unique_ptr<Nb[]>(new Nb[(n+1)*(n+1)]);
   auto coeffs = std::unique_ptr<Nb[]>(new Nb[n+1]);
   auto matrix_ptr = matrix.get();

   // Loop over subspace vectors
   In i=I_0;
   for(auto left=this->mResiduals.begin(); left!=this->mResiduals.end(); ++left)
   {
      for(auto right=trans_resids.begin(); right!=trans_resids.end(); ++right)
      {
         // Calculate dot
         *(matrix_ptr++) = Dot(*left,*right);
      }

      // Last elements of first n-1 rows are -1
      *(matrix_ptr++) = -C_1;
      // All RHS elements except the last are 0
      coeffs[i++] = C_0;
   }
   // The last row is {-1,-1,...,-1,0}
   for(size_t i=I_0; i<n; ++i)
   {
      *(matrix_ptr++) = -C_1;
   }
   *(matrix_ptr++) = C_0;

   // The last RHS element is -1
   coeffs[n] = -C_1;

   // Solve the linear system
   In N = n+I_1;
   In nrhs = I_1;
   auto lin_sol = GESV(matrix.get(), coeffs.get(), N, nrhs);

   // Pack coeffs into vector (excluding the Lagrange multiplier)
   std::vector<Nb> result(n);
   for(In i=I_0; i<n; ++i)
   {
      result[i] = lin_sol.solution[i];
   }

   return result;

}


/**
 * Calculate new step size using quadratic backtracking line search.
 *
 * The new step size is determined by minimizing a 2nd-order Taylor expansion of 
 * \f$ g(\lambda) = \frac{1}{2}||\mathbf{f}(\mathbf{x}+\lambda \Delta \mathbf{x})||^2 \f$. 
 * Ref [D.V. Fedorov, Yet another introduction to numerical methods](http://users-phys.au.dk/fedorov/Numeric/now/Book/main.pdf) page 47-48
 *
 * @param aLambda
 *    The current step length, which is modified inside the function.
 **/
void TensorNlSolver::QuadraticLineSearch
   (  Nb& aLambda
   )  const
{
   auto g0 = C_I_2*(this->mPreviousResidualNorm)*(this->mPreviousResidualNorm);
   auto g1 = C_I_2*(this->mResidualNorm)*(this->mResidualNorm);
   auto gp0 = -C_2*g0;

   auto c = (g1-g0-gp0*aLambda)/(aLambda*aLambda);

   aLambda = -gp0/(C_2*c);
}


/**
 * Output a TensorDataCont in SimpleTensor format along with
 * the corresponding ranks and mode combinations.
 *
 * @param aVec
 *    The vector to output.
 * @param aName
 *    The name of the vector.
 **/
void  TensorNlSolver::OutputVectorInfo
   (  const TensorDataCont& aVec
   ,  const std::string& aName
   )  const
{
   const auto& mcr = this->mpTrf->GetXvecModeCombiOpRange();
   In i = I_0;
   Mout << "## " + aName + ": ##" << std::endl;
   for(const auto& mc : mcr)
   {
      // Exclude reference
      if ( mc.Size() == I_0 )
      {
         continue;
      }
      const auto& tens = aVec.GetModeCombiData(i);
      Mout << "MC: " << mc << std::endl;
      if ( tens.Type() == BaseTensor<Nb>::typeID::CANONICAL )
      {
         Mout << "Fitted to rank: " << static_cast<CanonicalTensor<Nb>*>(tens.GetTensor())->GetRank() << std::endl;
      }
      Mout << tens.ToSimpleTensor() << std::endl;
      Mout << std::endl;
      ++i;
   }
   Mout << "###########################" << std::endl;
   Mout << std::endl;
}

/**
 * Check the restart vector and set the necessary flags.
 * Recompose if restarting a SimpleTensor calculation from
 * a CP vector.
 *
 * @param arRestartVec
 *    The restart vector
 **/
void TensorNlSolver::CheckRestartVector
   (  TensorDataCont& arRestartVec
   )
{
   // Check if the restart vector is SimpleTensor
   for(In i=I_0; i<arRestartVec.Size(); ++i)
   {
      if ( arRestartVec.GetModeCombiData(i).Type() == BaseTensor<Nb>::typeID::SIMPLE ) 
      {
         this->mRestartFromSimple = true;
         break;
      }
   }

   // Recompose amplitudes if no decomp info is given.
   // Allows restart of SimpleTensor calculation from CanonicalTensor guess.
   if ( this->mDecompInfoSet.empty() )
   {
      arRestartVec.Recompose();
   }
}

/**
 * Read in restart vectors for CROP and DIIS
 *
 * @param arErrorVector    The error vector to read in
 **/
void TensorNlSolver::RestartCropDiis
   (  TensorDataCont& arErrorVector
   )
{
   // Sanity check
   if (  !(this->mCrop || this->mDiis) )
   {
      MidasWarning("TensorNlSolver: Trying to read in CROP/DIIS vectors for a different type of calculation???");
      return;
   }

   // Declarations
   const std::string method = this->mCrop ? "CROP" : "DIIS";

   // Read in error vector
   const std::string errvec_name = this->mRestartName + "_Vcc_resid_tensor_0_0";
   Mout << " Reading error vector." << std::endl;
   arErrorVector.ReadFromDisc( errvec_name );
   Mout << " Norm of read error vector = " << arErrorVector.Norm() << std::endl;

   // Search for newest CROP/DIIS vectors
   const std::string trial_name_base = this->mRestartName + "_Vcc_" + method + "_trial_";
   const std::string resid_name_base = this->mRestartName + "_Vcc_" + method + "_resid_";
   In latest = I_0;
   while (  InquireFile(trial_name_base+std::to_string(latest+I_1))
         && InquireFile(resid_name_base+std::to_string(latest+I_1))
         )
   {
      // If the next file exists, increment latest.
      ++latest;
   }
   Mout << " The latest vector is from iteration: " << latest << std::endl;

   // Stop if no restart vectors are found
   if (  latest == I_0  )
   {
      MidasWarning("TensorNlSolver: No restart vectors found for " + method );
      return;
   }

   // Add vectors to subspaces
   In nvecs = std::min(this->mNSubspaceVecs, latest) - I_1;   // N-1, since the last vectors are stored in arAmplitudes and error_vector
   In startvec = latest - nvecs;
   Mout  << " Reading " << nvecs << " CROP vectors starting from vector nr. " << startvec << "." << std::endl;

   this->mResiduals.resize(nvecs);
   this->mTrials.resize(nvecs);
   auto resid_it = this->mResiduals.begin();
   auto trial_it = this->mTrials.begin();
   for(In i=I_0; i<nvecs; ++i) // Read in
   {
      resid_it->ReadFromDisc(resid_name_base+std::to_string(startvec+i));
      trial_it->ReadFromDisc(trial_name_base+std::to_string(startvec+i));
      Mout  << "    Norms of " << i << ". CROP vectors:\n"
            << "       Amp: " << trial_it->Norm() << "\n"
            << "       Err: " << resid_it->Norm() << "\n"
            << std::flush;
      std::advance(resid_it, 1);
      std::advance(trial_it, 1);
   }
   resid_it = this->mResiduals.begin();
   trial_it = this->mTrials.begin();
   for(In j=I_0; j<nvecs; ++j) // Check type
   {
      if (  !resid_it->AllOfType(BaseTensor<Nb>::typeID::CANONICAL)
         || !trial_it->AllOfType(BaseTensor<Nb>::typeID::CANONICAL)
         )
      {
         Mout << "Restarting from non-CanonicalTensor " + method + " vectors!" << std::endl;
         this->mRestartFromSimple = true;
         break;
      }
      std::advance(resid_it, 1);
      std::advance(trial_it, 1);
   }

   // Recompose if mRestartFromSimple is true
   if (  this->mRestartFromSimple
      )
   {
      resid_it = this->mResiduals.begin();
      trial_it = this->mTrials.begin();
      for(In k=I_0; k<nvecs; ++k)
      {
         resid_it->Recompose();
         trial_it->Recompose();
         std::advance(resid_it, 1);
         std::advance(trial_it, 1);
      }

      arErrorVector.Recompose();
   }
}


/**
 * Recompress the VCC amplitudes if necessary.
 * 
 * @param arAmplitudes
 *    The VCC amplitudes
 **/
void TensorNlSolver::RecompressAmplitudes
   (  TensorDataCont& arAmplitudes
   )
{
   bool first_restart_iter = this->mRestart && (this->mNIter == I_1);
   if (  !this->mDecompInfoSet.empty()    // If decomposed calculation
      && (  !first_restart_iter           // Not if first restart iteration (the restart vector is assumed to have optimal rank)
         || this->mRestartFromSimple      // Decompose if we are restarting from a SimpleTensor
         )
      )
   {
      Mout << Idt(1) << "## Decomposing amplitudes ##" << std::endl;
      this->mTimer.Reset();
      if (  this->mNIter > I_1
         && this->mAdaptiveDecompGuess
         && !this->mCrop      // AdaptiveDecompGuess is not possible with CROP since the previous trials have been optimized.
         )
      {
         arAmplitudes.Decompose(this->mDecompInfoSet, this->mTrials.back());
      } 
      else
      {
         arAmplitudes.Decompose(this->mDecompInfoSet);
      }
      if (  gTime )
      {
         this->mTimer.CpuOut (Mout, Idt(2)+"CPU time spent on decomposing amplitudes:  ");
         this->mTimer.WallOut(Mout, Idt(2)+"Wall time spent on decomposing amplitudes: ");
      }
      Mout << std::endl;
   }

   // Output vector for debug
   if ( gDebug )
   {
      this->OutputVectorInfo(arAmplitudes, "VCC AMPLITUDE VECTOR: Iteration " + std::to_string(this->mNIter));
   }

   // Save the rank information
   if (  this->mSaveRankInfo
      && !this->mDecompInfoSet.empty()
      )
   {
      this->mAmplitudeRankStatistics.EmplaceRankInfo( arAmplitudes );

      // Const ref to name
      const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
      const auto& analysisdir = this->mpTrf->pVccCalcDef()->GetmVscfAnalysisDir();
      this->mAmplitudeRankStatistics.WriteToDisc(analysisdir + "/" + calc_name + "_Vcc_amplitude_ranks");
   }
}

/**
 * Transform the VCC amplitudes to obtain the error vector.
 * Check that the norm of the error vector is not nan.
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 * @param arErrorVector
 *    The VCC error vector
 *
 * @return
 *    True if the residual norm is sane.
 **/
bool TensorNlSolver::TransformAmplitudes
   (  TensorDataCont& arAmplitudes
   ,  TensorDataCont& arErrorVector
   )
{
   // Debug: Break here to pretend that something went wrong during the transform (time limit reached, etc.)
   if (  this->mBreakBeforeTransform > I_0
      && this->mNIter == this->mBreakBeforeTransform
      )
   {
      Mout  << " For debug purposes, exit program before transformation!" << std::endl;
      exit(1);
   }

   // Calculate individual MC thresholds for transformer
   std::vector<Nb> thresholds;
   if (  !this->mDecompInfoSet.empty()
      && this->mIndividualMcDecompThresh
      && this->mIndividualMcDecompThreshScaling.first > C_0 // if scaling is negative, only use the individual threshold for CROP subspace
      )
   {
      auto size = this->mRelativeErrorVectorNorms.size();
      thresholds.resize( size );

      for( size_t i=0; i<size; ++i )
      {
         auto scaling   =  this->mAmpDecompThresh * this->mIndividualMcDecompThreshScaling.first      // Absolute thresh
                        +  this->mAmplitudeNorms[i] * this->mIndividualMcDecompThreshScaling.second;  // Relative thresh
         auto i_thresh = this->mRelativeErrorVectorNorms[i]*scaling; // Scale relative to norm of preconditioner

         // Scale with tensor order (not really tested...)
         if (  this->mScaleThresholdWithTensorOrder   )
         {
            const auto& order = arAmplitudes.GetModeCombiData(i).NDim();
            auto order_scaling   =  order > I_3
                                 //?  (order-I_3)*this->mTensorOrderThresholdScaling
                                 ?  std::pow(this->mTensorOrderThresholdScaling, order-I_3)
                                 :  I_1;
            i_thresh *= order_scaling;
         }

         // Compare to smallest allowed decomp threshold
         thresholds[i] = std::max( i_thresh, this->mMinTrfDecompThresh );

//         Mout << "INDIVIDUAL THRESHOLD " << i << " = " << thresholds[i] << std::endl;
//         Mout << "AMPLITUDE NORM = " << this->mAmplitudeNorms[i] << std::endl;
      }
   }

   // Transform amplitudes
   Mout << Idt(1) << "## Transforming amplitudes ##" << std::endl;
   this->mTimer.Reset();

   // If 'thresholds' is empty, no individual decomp thresholds are used.
   this->mpTrf->Transform
      (  arAmplitudes
      ,  arErrorVector
      ,  I_1
      ,  I_1
      ,  this->mEnergy
      ,  thresholds
      );

   if (  gTime )
   {
      this->mTimer.CpuOut (Mout, Idt(2)+"CPU time spent on transforming amplitudes:  ");
      this->mTimer.WallOut(Mout, Idt(2)+"Wall time spent on transforming amplitudes: ");
   }
   Mout << std::endl;

   // Write error vector to disk if using DIIS or CROP
   if (  (  this->mDiis
         || this->mCrop
         )
      )
   {
      const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
      this->WriteToDisc(arErrorVector, calc_name + "_Vcc_resid_tensor_0_0");
   }

   // Output vector for debug
   if ( gDebug )
   {
      this->OutputVectorInfo(arErrorVector, "VCC ERROR VECTOR: Iteration " + std::to_string(this->mNIter));
   }

   // Save rank information
   if (  this->mSaveRankInfo
      && !this->mDecompInfoSet.empty()
      )
   {
      this->mErrorVectorRankStatistics.EmplaceRankInfo( arErrorVector );

      // Const ref to name
      const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
      const auto& analysisdir = this->mpTrf->pVccCalcDef()->GetmVscfAnalysisDir();
      this->mErrorVectorRankStatistics.WriteToDisc(analysisdir + "/" + calc_name + "_Vcc_errvec_ranks");
   }

   // Calculate residual norm and check for nan
   this->mResidualNorm = arErrorVector.Norm();
   this->mResidualMaxNorm = arErrorVector.MaxNorm();
   if (  this->mResidualNorm != this->mResidualNorm
      || this->mResidualMaxNorm != this->mResidualMaxNorm
      ) 
   {
      return false;
   }

   return true;
}


/**
 * The convergence can be checked based on:
 *  - Norm of the full error vector (default).
 *  - Maximum error of the individual MCs.
 *  - Energy change.
 *
 * @return
 *    True if the iteration loop must break.
 */
bool TensorNlSolver::CheckConvergence
   (
   ) 
{
   Mout << Idt(1) << "## Checking convergence ##" << std::endl;

   // Output convergence rate
   Nb relative_error_change = (this->mResidualNorm - this->mPreviousResidualNorm)/this->mPreviousResidualNorm;
   if ( this->mNIter > I_1 )
   {
      Mout << Idt(2)+"(||e||-||e_prev||)/||e_prev|| = " << relative_error_change << std::endl;

      if (  this->mBacktrackingThresh > C_0  )
      {
         Mout << Idt(2)+"Backtracking threshold = " << this->mBacktrackingThresh << std::endl;
      }

      // Check if we need to increase the excitation level for reduced-Jacobian Newton-Raphson
      if (  this->mNewtonRaphson
         && this->mNrInfo.mAdaptiveMaxExci > C_0
         )
      {
         Mout << Idt(2)+"Largest relative error change = " << this->mNrInfo.mMaxConvergenceRate << std::endl;

         if (  std::abs(relative_error_change) < this->mNrInfo.mAdaptiveMaxExci*this->mNrInfo.mMaxConvergenceRate
            )
         {
            Mout  << Idt(2)+"Relative error change is too small. Attempt to increase Jacobian excitation level:\n"
                  << Idt(3)+"Current Jacobian max exci level = " << this->mNrInfo.mJacobianMaxExci << std::endl;

            this->mNrInfo.mJacobianMaxExci = std::min(this->mNrInfo.mJacobianMaxExci+I_1, this->mpTrf->GetMaxExciLevel()-I_1);

            Mout  << Idt(3)+"New Jacobian max exci level     = " << this->mNrInfo.mJacobianMaxExci << std::endl;

            // If we can still increase exci level, reset mMaxConvergenceRate to 0
            if (  this->mNrInfo.mJacobianMaxExci < this->mpTrf->GetMaxExciLevel()-I_1
               )
            {
               Mout  << Idt(2)+"Reset largest relative error change to 0." << std::endl;
               this->mNrInfo.mMaxConvergenceRate = C_0;
            }
         }
         else
         {
            this->mNrInfo.mMaxConvergenceRate = std::max(this->mNrInfo.mMaxConvergenceRate, std::abs(relative_error_change));
         }
      }
   }
   Mout << std::endl;


   // Check current convergence status
   bool converged = false;

   // Check for residual norm convergence
   if ( this->mMaxNormConvCheck )
   {
      converged = (this->mResidualMaxNorm < this->mResidualThreshold);
   }
   else
   {
      converged = (this->mResidualNorm < this->mResidualThreshold);
   }

   // Check for energy convergence (Only if energy threshold is set to something reasonable. Default is C_MAX_NB)
   if (  this->mNIter > I_1
      && this->mEnergyThreshold < C_1
      )
   {
      const Nb energy_change = std::abs(this->mEnergy - this->mPreviousEnergy);

      converged = converged && (energy_change < this->mEnergyThreshold);
   }

   this->mConverged = converged;


   // Set flags
   this->mDiverging = (this->mResidualNorm > this->mPreviousResidualNorm) && (this->mNIter > I_1);
   this->mSlowConvergence  
      =  (std::abs(relative_error_change) < this->mBacktrackingThresh*this->mBackstepLambda) 
      && (this->mNIter > I_1)
      && !this->mDiverging;

   // Determine whether to break the iteration loop
   bool break_loop = false;
   if ( this->mDiverging )
   {
      MidasWarning("TensorNlSolver: Error increasing!");
      if ( this->mNBacksteps >= this->mMaxBacksteps )
      {
         // Break the iteration loop and quit
         break_loop = true;
      }
      // Backstep in next iteration if allowed
      this->mBackstep = this->mAllowBackstep;
   }
   else if  ( this->mSlowConvergence )
   {
      this->mBackstep = this->mAllowBackstep && (this->mNBacktracks < this->mMaxBacktracks);
   }


   return break_loop;
}


/**
 * Save the VCC amplitudes
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 **/
void TensorNlSolver::SaveAmplitudes
   (  const TensorDataCont& arAmplitudes
   )
{
   // Const ref to name
   const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
   const std::string file_name = calc_name + "_Vcc_vec_tensor_0_0";

   // Remove _prev (to make the code more portable) and rename old to _prev
   const std::string file_name_prev = file_name + "_prev";
   if (  InquireFile(file_name_prev)
      )
   {
      std::remove(file_name_prev.c_str());
   }
   std::rename(file_name.c_str(), file_name_prev.c_str());

   // Write trial vector
   this->WriteToDisc(arAmplitudes, file_name);

   //// Niels: Uncomment the following if the restart vectors should also be saved as DataConts
   //DataCont amp_dc = DataContFromTensorDataCont(arAmplitudes);
   //const std::string str = calc_name + "_Vcc_vec_"+std::to_string(0);
   //amp_dc.NewLabel(str,false);
   //amp_dc.SaveUponDecon();
}


/**
 * Perform backstep iteration
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 **/
void TensorNlSolver::Backstep
   (  TensorDataCont& arAmplitudes
   )
{
   if ( this->mDiverging )
   {
      Mout << Idt(1)+"## Divergence detected: Performing quadratic backtracking line search ##" << std::endl;
   }
   else if  ( this->mSlowConvergence )
   {
      Mout << Idt(1)+"## Convergence too slow: Performing quadratic backtracking line search ##" << std::endl;
   }
   Mout << Idt(2)+"Previous error vector norm = " << this->mPreviousResidualNorm << std::endl;
   Mout << Idt(2)+"Current error vector norm  = " << this->mResidualNorm << std::endl;

   // Perform quadratic line search (see D. Fedorov "Introduction to Numerical Methods")
   this->QuadraticLineSearch( this->mBackstepLambda );

   Mout << Idt(2)+"Step size reduced to: " << this->mBackstepLambda << std::endl;
   Mout << std::endl;

   // Reset amplitudes to previous trial vector and redo update with new step size
   // If DIIS the optimized trial vector needs to be re-calculated
   arAmplitudes = this->mDiis ? LinComb(this->mTrials, this->mSubspaceCoeffs) : this->mTrials.back();

   if ( this->mNewtonRaphson )   // If Newton-Raphson we reuse the saved Newton-Raphson update
   {
      arAmplitudes.Axpy( this->mPrevNewtonRaphsonUpdate, -this->mBackstepLambda );
      this->mUpdateNorm = std::abs( this->mBackstepLambda ) * this->mPrevNewtonRaphsonUpdate.Norm();
   }
   else if  ( this->mDiis )  // We need to re-calculate the optimized residual for DIIS
   {
      auto update = LinComb(this->mResiduals, this->mSubspaceCoeffs);
      this->mUpdateNorm = this->QuasiNewtonUpdate( arAmplitudes, update );
   }
   else   // Otherwise simply re-perform the last update with new step size. This works for CROP and quasi-Newton.
   {
      this->mUpdateNorm = this->QuasiNewtonUpdate( arAmplitudes, this->mResiduals.back() );
   }

   if ( this->mDiverging ) 
   {
      ++this->mNBacksteps;
      Mout << Idt(2)+"Performed backstep " << this->mNBacksteps << " of at most " << this->mMaxBacksteps << std::endl;
      Mout << std::endl;

      if (  this->mIdja
         && this->mBackstepLambda < C_I_10_2
         )
      {
         Mout << Idt(2)+"IDJA resulted in lambda < 1.e-2. Inverse Jacobian is reset to modal energy denominators!" << std::endl;
         if ( this->mIdjaUnitMatrixGuess )
         {
            // Reset to unit matrix
            this->mPreconditioner = arAmplitudes; // Set shape
            this->mPreconditioner.Zero();   // Zero
            this->mPreconditioner.ElementwiseScalarAddition(C_1);   // Add ones
         }
         else
         {
            // Reset to 0th-order inverse Jacobian
            this->mPreconditioner = this->EnergyDenominators();
         }
      }
   }
   else if  ( this->mSlowConvergence )
   {
      ++this->mNBacktracks;
      Mout << Idt(2)+"Quadratic backtracking iteration " << this->mNBacktracks << " of at most " << this->mMaxBacktracks << std::endl;
      Mout << std::endl;
   }
}


/**
 * Wrapper function for updating the preconditioner
 * when using IDJA or true Jacobian diagonal.
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 * @param arErrorVector
 *    The VCC error vector
 **/
void TensorNlSolver::UpdatePreconditioner
   (  const TensorDataCont& arAmplitudes
   ,  const TensorDataCont& arErrorVector
   )
{
   // Update inverse-Jacobian approximation if IDJA
   if (  this->mIdja
      && !this->mTrials.empty()     // this->mTrials.back() causes undefined behavior if empty
      )
   {
      this->IdjaUpdate( arAmplitudes
                      , this->mTrials.back()
                      , arErrorVector
                      , this->mResiduals.back()
                      );
   }
}


/**
 * Wrapper function for updating the VCC amplitudes
 *
 * @param arAmplitudes
 *    The VCC amplitudes
 * @param arErrorVector
 *    The VCC error vector
 **/
void TensorNlSolver::UpdateAmplitudes
   (  TensorDataCont& arAmplitudes
   ,  TensorDataCont& arErrorVector
   )
{
   Mout << Idt(1)+"## Performing amplitude update ## " << std::endl;

   // Set previous amplitudes and error vector if not done by CROP or DIIS
   if ( !(  this->mCrop || this->mDiis )  )
   {
      this->mTrials.back() = arAmplitudes;
      this->mResiduals.back() = arErrorVector;
   }

   // Perform the update
   this->mTimer.Reset();

   if (  this->mCrop || this->mDiis )  // SubspaceUpdate calls NewtonRaphsonUpdate or QuasiNewtonUpdate
   {
      this->mUpdateNorm = this->SubspaceUpdate( arAmplitudes, arErrorVector );
   }
   else if  ( this->mNewtonRaphson )
   {
      this->mUpdateNorm = this->NewtonRaphsonUpdate( arAmplitudes, arErrorVector );
   }
   else
   {
      this->mUpdateNorm = this->QuasiNewtonUpdate( arAmplitudes, arErrorVector );
   }
   if (  gTime )
   {
      this->mTimer.CpuOut (Mout, Idt(2)+"CPU time spent on updating amplitudes:  ");
      this->mTimer.WallOut(Mout, Idt(2)+"Wall time spent on updating amplitudes: ");
   }
   Mout << std::endl;

   // Reset step size and number of backtracking iterations
   this->mBackstepLambda = C_1;
   this->mNBacktracks = I_0;
}


/**
 * Update adaptive decomp thresholds for both amplitudes and error vectors
 **/
void TensorNlSolver::UpdateAdaptiveDecompThresholds
   (
   )
{
   const Nb& ts = this->mThresholdScaling;
   const Nb& trfts = this->mTrfRelThreshScal;

   this->mAmpDecompThresh = std::max( this->mMinDecompThresh, this->mUpdateNorm*ts );

   if ( this->mAdaptiveDecompThreshold )
   {
      this->UpdateAmplitudeDecompThresholds( this->mAmpDecompThresh );
   }
   if ( this->mAdaptiveTrfDecompThreshold )
   {
      this->mTrfDecompThresh = std::max( this->mMinTrfDecompThresh, this->mAmpDecompThresh*trfts );
      this->mpTrf->UpdateDecompThresholds( this->mTrfDecompThresh );
   }

   // Write amplitude decomp threshold
   const std::string& calc_name = this->mpTrf->pVccCalcDef()->GetName();
   const std::string file_name = calc_name + "_Vcc_decomp_threshold";

   // Remove _prev (to make the code more portable) and rename old to _prev
   const std::string file_name_prev = file_name + "_prev";
   if (  InquireFile(file_name_prev)
      )
   {
      std::remove(file_name_prev.c_str());
   }
   std::rename(file_name.c_str(), file_name_prev.c_str());

   // Write new file
   midas::mpi::OFileStream output( file_name );
   output << std::setprecision(16) << this->mAmpDecompThresh << std::endl;
   output.close();
}


/**
 * Print information from the current iteration
 **/
void TensorNlSolver::OutputIterationInfo
   (
   )  const
{
   auto io = this->mpTrf->pVccCalcDef()->IoLevel();

   // Unmute Mout
   bool mute = Mout.Muted();
   Mout.Unmute();


   // Detailed flow for IoLevel 4
   if (  io > I_3 )
   {
      Mout << Idt(1)+"## Results of iteration " << this->mNIter << " ##" << "\n"
           << Idt(2)+"Error vector norm                       = "  << this->mResidualNorm << "\n"
           << Idt(2)+"Largest MC error                        = "  << this->mResidualMaxNorm << "\n"
           << Idt(2)+"VCC energy                              = "  << this->mEnergy << "  a.u." << "\n"
           << Idt(2)+"VCC energy                              = "  << this->mEnergy*C_AUTKAYS << "  cm^-1" << "\n"
           << Idt(2)+"VCC energy                              = "  << this->mEnergy*C_AUTEV << "  eV" << std::endl;
      if ( this->mNIter > I_1 )
      {
         Mout << Idt(2)+"Energy change (E_current - E_previous)  = " << this->mEnergy - this->mPreviousEnergy << std::endl;
      }
      if (  this->mAdaptiveDecompThreshold
         && !this->mConverged
         )
      {
         Mout << Idt(2)+"New amplitude decomposition threshold   = " << this->mAmpDecompThresh << std::endl;
      }
      if (  this->mAdaptiveTrfDecompThreshold
         && !this->mIndividualMcDecompThresh
         && !this->mConverged
         )
      {
         Mout << Idt(2)+"New transformer decomposition threshold = " << this->mTrfDecompThresh << std::endl;
      }
      Mout << std::endl;
      Mout << std::endl;
      Mout << std::endl;
   }
   // Short flow for IoLevel 3
   else if  (  io > I_2 )
   {
      midas::stream::ScopedPrecision(6, Mout);

      const std::string iter = "Iteration "+std::to_string(this->mNIter)+":";

      Mout  << std::setw(16) << iter
            << "VCC energy = " << this->mEnergy << "   "
            << "Error norm = " << this->mResidualNorm 
            << std::endl;
   }
   // Output dot to know the solver lives if no flow is printed
   else
   {
      Mout << ".";
   }

   // Reset Mout mute status
   if (  mute  )
   {
      Mout.Mute();
   }
}
