#ifndef SOLUTIONVECTORCONTAINER_H_INCLUDED
#define SOLUTIONVECTORCONTAINER_H_INCLUDED

#include <vector>

template<template <class...> class ContainerType, class VectorType, class... Us>
class VectorContainer: public ContainerType<VectorType, Us...>
{
   public:
      // public interface
      using vector_type = VectorType;
      using container_type = ContainerType<VectorType, Us...>;

      template<class... Ts>
      VectorContainer(Ts&&... ts): container_type(std::forward<Ts>(ts)...)
      {
      }

      void Clear()
      {
         this->clear();
      }
};

template<class T>
using StandardContainer = VectorContainer<std::vector, T, std::allocator<T> >;

#endif /* SOLUTIONVECTORCONTAINER_H_INCLUDED */
