#ifndef ITERATIVEEQUATIONSOLVER_H_INCLUDED
#define ITERATIVEEQUATIONSOLVER_H_INCLUDED

#include <iostream>

#include "libmda/util/multiple_return.h"

#include "util/Timer.h"
#include "it_solver/IES_Macros.h"

//#include "InitializeIterativeSolverFromCalcDef.h"
#include "ConstructSolution.h"


template<class SolverType>
class IterativeEquationSolver
   :  public SolverType
{
   private:
      // data members
      char mChar = '='; // output delimeter character
      unsigned mMaxIter = 100;
      bool mSaveToDisc = false; // save solution vectors to disc each iteration
      unsigned mItNum = 0; // Iteration number

      // member functions
      explicit IterativeEquationSolver() = delete;
      explicit IterativeEquationSolver(const IterativeEquationSolver<SolverType>&) = delete;
      IterativeEquationSolver<SolverType>& operator=(const IterativeEquationSolver<SolverType>&) = delete;
      
      /**
       *
       * Solve driver returns (converged,dobreak)
       *
       **/
      libmda::util::return_type<bool,bool> SolveDriver
         (  unsigned& it_num
         )
      {
         bool converged = false;
         bool dobreak = false;
         
         //Mout << " starting solve driver " << std::endl;
         //Mout << " residual size when started: " << this->self().Residuals().size() << std::endl;

         SolverType::Ostream() << std::string(10,mChar) 
                               <<" Starting Iteration " << 0 << " (init) " 
                               << std::string(10,mChar) << "\n" << std::endl;

         // do a first pseudo iteration
         //SolverType::Ostream() << " clear solver " << std::endl;
         SolverType::ClearSolver(); // we clear any trial vectors before we start
         //SolverType::Ostream() << " make start guess " << std::endl;
         SolverType::MakeStartGuess();
         //SolverType::Ostream() << " do reduced " << std::endl;
         //Mout << " do reduced " << std::endl;
         SolverType::DoReducedSolution();
         //SolverType::Ostream() << " make residual " << std::endl;
         SolverType::MakeResidual();
         //Mout << " Check convergence " << std::endl;
         //SolverType::Ostream() << " check convergence " << std::endl;
         converged = SolverType::CheckConvergence();

         // main loop
         //SolverType::Ostream() << " start loop " << std::endl;
         while((!converged) && (++it_num <= mMaxIter))
         {
            // if stream is muted we just output a dot, so we know the program is alive
            if(SolverType::Ostream().Muted())
            {
               SolverType::Ostream().Unmute();
               SolverType::Ostream() << ".";
               SolverType::Ostream().Mute();
            }

            SolverType::Ostream() << std::string(10,mChar)
                                  << " Starting Iteration " << it_num << " "
                                  << std::string(10,mChar) << "\n" << std::endl;

            // solve the problem
            SolverType::Precondition();
            if(!SolverType::ExpandSpace())
            {
               SolverType::Ostream() << " did not add any new vectors, might as well stop! " << std::endl;
               break;
            }
            SolverType::DoReducedSolution();
            SolverType::MakeResidual();
            converged = SolverType::CheckConvergence();
            
            // save to disc for security against crash
            if(mSaveToDisc)
            {
               SolverType::ConstructSolution();
               SolverType::WriteSolutionToDisc(this->self().SolVec(), it_num);
            }
            
            // print iteration summary
            Summary(it_num);
            
            // check for break
            if(SolverType::Break() && !converged)
            {
               SolverType::Ostream() << " will break and restart " << std::endl;
               SolverType::ConstructSolution();
               SolverType::WriteSolutionToDisc(this->self().SolVec(), "break");
               this->self().AddRestart("break");
               dobreak = true;
               break; // break loop
            }
         }
         // if stream is muted we break the line of dots.
         if(SolverType::Ostream().Muted())
         {
            SolverType::Ostream().Unmute();
            SolverType::Ostream() << std::endl;
            SolverType::Ostream().Mute();
         }
         
         // we construct the solution from trials and reduced solutions
         SolverType::ConstructSolution();
         
         // return whether or not equations converged
         return libmda::util::ret(converged,dobreak);
      }
   
      /***************
       * ouput iteration summary
       ***************/
      void Summary(In it_num)
      {
            SolverType::Ostream() << "\n ~~~ Iteration " << it_num << " summary ~~~ \n"
                                  << std::setw(20) << "    trial space dimension " << ": " << SolverType::ReducedSize() << "\n"
                                  << "\n" << std::flush;
      }

      /***************
       * ouput solver information
       ***************/
      void Output()
      {
         SolverType::Ostream() << std::left << "\n"
                  << std::setw(30) << " Equation type "       << ": " << SolverType::Type() << "\n"
                  << std::setw(30) << " Dimension of space "  << ": " << SolverType::Atrans().Dim() << "\n"
                  << std::setw(30) << " Number of equations " << ": " << SolverType::Neq() << "\n"
                  << std::setw(30) << " Residual threshold, abs "  << ": " << SolverType::ResidualEpsilon() << "\n"
                  << std::setw(30) << " Residual threshold, rel "  << ": " << SolverType::ResidualEpsilonRel() << "\n"
                  << std::setw(30) << " Maximum iterations "  << ": " << mMaxIter << "\n"
                  << std::setw(30) << " Break dimension "     << ": " << SolverType::BreakDim() << "\n";
         SolverType::Output();
         SolverType::Ostream() << "\n" << std::flush;
      }

      /**
       * finalizer
       **/
      void Finalize()
      {
         //SolverType::ClearSolver(); // clear here ?
         SolverType::Solved() = true; // the solver has now been run
         SolverType::Finalize(); // finalize calculation
      }

   public:
      /**
       * Constructor from parameters
       **/
      template<class... Ts>
      explicit IterativeEquationSolver(Ts&&... ts): SolverType(std::forward<Ts>(ts)...)
      {
      }

      /****
       * Init solver
       ***/
      void Initialize()
      {
         SolverType::Initialize();
      }

      /***
       * Solve interface
       ***/
      bool Solve()
      {
         Timer time;
         SolverType::Ostream() << std::string(8,mChar)
                               << " Starting solving iterative equation " 
                               << std::string(8,mChar) << "\n" << std::endl;
         
         if (  SolverType::Solved()
            )
         {
            MIDASERROR("Iterative problem already solved... probably a bug I'm stopping");
         }

         Output();
        
         // loop while equations break and we are not converged
         this->mItNum = 0;
         bool dobreak = true;
         while(dobreak && !SolverType::IsConverged())
         { 
            libmda::util::ret(SolverType::IsConverged(),dobreak) = SolveDriver(this->mItNum);
         }
         
         //Mout << " whatup? " << std::endl;
         SolverType::Ostream() << std::string(10, mChar)
                               << " Done solving iterative equation "
                               << std::string(10, mChar) << "\n" << std::endl;
         SolverType::Ostream() << " On exit the reduced space is : " << SolverType::ReducedSize() << "\n" << std::endl;
         
         time.CpuOut (SolverType::Ostream()," CPU  time used in IterativeEquationSolver::Solve()\t");
         time.WallOut(SolverType::Ostream()," Wall time used in IterativeEquationSolver::Solve()\t");

         
         // finalize calculation
         Finalize();
         return SolverType::IsConverged();
      }

      
      //
      //
      //
      void SetMaxIter(In aMaxIter) 
      { 
         mMaxIter = aMaxIter; 
      }

      In MaxIter() const
      {
         return mMaxIter;
      }

      void SetChar(char c)
      {
         mChar = c;
      }

      //! Iteration number
      unsigned ItNum() const
      {
         return mItNum;
      }
      
      //
      //
      //
      void SetSaveToDisc(bool aSaveToDisc)
      {
         mSaveToDisc = aSaveToDisc;
      }

      bool SaveToDisc() const
      {
         return mSaveToDisc;
      }
};

#endif /* ITERATIVEEQUATIONSOLVER_H_INCLUDED */
