#ifndef NOFINALIZER_H_INCLUDED
#define NOFINALIZER_H_INCLUDED

template<class A>
class NoFinalizer: public A
{
   public:
      void Finalize() const
      {
      }
};

#endif /* NOFINALIZER_H_INCLUDED */
