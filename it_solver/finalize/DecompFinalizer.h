#ifndef DECOMPFINALIZER_H_INCLUDED
#define DECOMPFINALIZER_H_INCLUDED

#include <fstream>

template<class A>
class DecompFinalizer: public A
{
   std::string mAnalysisName;
   int mExciLevel = 3;

   private:
      template<class TRIALS>
      void FinalizeImpl(TRIALS& trials)
      {
         std::string comp_name = mAnalysisName + "_compression.dat";
         std::ofstream comp_file(comp_name);
         std::string mode_name = mAnalysisName + "_mode_combis.dat";
         std::ofstream mode_file(mode_name);
         std::string coll_name = mAnalysisName + "_collection.dat";
         std::ofstream coll_file(coll_name);

         for(size_t i=0; i<trials.size(); ++i)
         {
            comp_file << " *---------- VECTOR " << std::to_string(i) << " --- ----------*\n";
            mode_file << " *---------- VECTOR " << std::to_string(i) << " --- ----------*\n";
            coll_file << " *---------- VECTOR " << std::to_string(i) << " --- ----------*\n";
            trials[i]->AnalyseCollectiveAbsDataDistribution(mExciLevel,coll_file);
            trials[i]->PrintCompressionData(comp_file);
            trials[i]->PrintModeCombisData(mode_file);
            comp_file << " *---------- VECTOR " << std::to_string(i) << " END ----------*\n" << std::endl;
            mode_file << " *---------- VECTOR " << std::to_string(i) << " END ----------*\n" << std::endl;
            coll_file << " *---------- VECTOR " << std::to_string(i) << " END ----------*\n" << std::endl;
         }
      }

   public:
      void Finalize()
      {
         FinalizeImpl(this->self().Trials());
      }

      void SetAnalysisName(const std::string& aAnalysisName)
      {
         mAnalysisName = aAnalysisName;
      }
};

#endif /* DECOMPFINALIZER_H_INCLUDED */
