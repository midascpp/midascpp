#ifndef IES_MACROS_H_INCLUDED
#define IES_MACROS_H_INCLUDED

#include <utility>
#include <tuple>

// macros for making variables
#define MAKE_VARIABLE_2(TYPE,NAME) \
   private: \
      TYPE m##NAME; \
      using NAME##_t = TYPE; \
   public: \
      TYPE& NAME() \
      { \
         return m##NAME; \
      } \
      const typename std::remove_const<TYPE>::type& NAME() const \
      { \
         return m##NAME; \
      } \
   private: \

#define MAKE_VARIABLE_3(TYPE,NAME,VALUE) \
   private: \
      TYPE m##NAME = VALUE; \
      using NAME##_t = TYPE; \
   public: \
      TYPE& NAME() \
      { \
         return m##NAME; \
      } \
      const typename std::remove_const<TYPE>::type& NAME() const \
      { \
         return m##NAME; \
      } \
   private: \


#define MAKE_REFVARIABLE(TYPE,NAME) \
   private: \
      TYPE& m##NAME; \
      using NAME##_t = TYPE; \
   public: \
      TYPE& NAME() \
      { \
         return m##NAME; \
      } \
      const typename std::remove_const<TYPE>::type& NAME() const \
      { \
         return m##NAME; \
      } \
   private: \

#define MAKE_CONSTREFVARIABLE(TYPE,NAME) \
   private: \
      const TYPE& m##NAME; \
      using NAME##_t = const TYPE; \
   public: \
      const typename std::remove_const<TYPE>::type& NAME() \
      { \
         return m##NAME; \
      } \
   private: \


// helper for below
#define _ARG3(_0,_1,_2,_3,...) _3
// macro that will counter number of arguments up to three
#define NARG3(...) _ARG3(__VA_ARGS__,3,2,1,0)

#define _MAKE_VARIABLE_2(a,b) MAKE_VARIABLE_2(a,b)
#define _MAKE_VARIABLE_3(a,b,c) MAKE_VARIABLE_3(a,b,c)

#define __MAKE_VARIABLE(N,...) _MAKE_VARIABLE_ ## N (__VA_ARGS__)
#define _MAKE_VARIABLE(N,...) __MAKE_VARIABLE(N,__VA_ARGS__)

#define MAKE_VARIABLE(...) _MAKE_VARIABLE(NARG3(__VA_ARGS__),__VA_ARGS__)

#endif /* IES_MACROS_H_INCLUDED */
