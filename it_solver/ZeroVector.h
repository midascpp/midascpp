#ifndef ZEROVECTOR_H_INCLUDED
#define ZEROVECTOR_H_INCLUDED

#include <string>

template<class T=double>
class ZeroVector
{
   private:
      size_t mSize;
   
   public:
      ZeroVector(): mSize(0)
      {
      }

      ZeroVector(size_t aSize): mSize(aSize)
      {
      }

      size_t Size() const
      {
         return mSize;
      }
      
      T operator[](size_t i) const
      {
         return T(0);
      }

      T operator()(size_t i) const
      {
         return T(0);
      }

      void Zero() const
      {
      }

      std::string Storage() const
      {
         return "InMem";
      }

      void SetNewSize(size_t i)
      {
         mSize = i;
      }
};

template<class T>
std::ostream& operator<<(std::ostream& os, const ZeroVector<T>& zv)
{
   for(size_t i=0; i<zv.Size(); ++i)
   {
      os << T(0) << " ";
   }
   return os;
}

template<class T, class U>
auto Dot(ZeroVector<U>& zero1, ZeroVector<T>& zero2) 
   -> decltype(std::declval<U>()*std::declval<T>())
{
   return {0};
}

template<class T, class U>
U Dot(const ZeroVector<U>& zero1, const ZeroVector<T>& zero2) 
   //-> decltype(std::declval<U>()*std::declval<T>())
{
   return U(0);
}

template<class T, class U>
U Dot(T&& t, const ZeroVector<U>& zero) 
   //-> decltype(std::declval<U>())
{
   return U(0);
   //return {0};
}

template<class T, class U>
U Dot(T&& t, ZeroVector<U>& zero) 
   //-> decltype(std::declval<U>())
{
   return U(0);
   //return {0};
}

template<class T, class U>
U Dot(const ZeroVector<U>& zero, T&& t) 
   //-> decltype(std::declval<U>())
{
   return U(0);
   //return {0};
}

template<class T, class U>
U Dot(ZeroVector<U>& zero, T&& t) 
   //-> decltype(std::declval<U>())
{
   return U(0);
   //return {0};
}

template<class U>
U Norm(const ZeroVector<U>&)
{
   return U(0);
}

template<class U>
inline void Zero(ZeroVector<U>&)
{
}

template<class T, class U>
void Axpy(T&&, const ZeroVector<U>&, const U&)
{
}

#endif /* ZEROVECTOR_H_INCLUDED */
