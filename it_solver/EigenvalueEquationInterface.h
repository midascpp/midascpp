#ifndef EIGENVALUEEQUATIONINTERFACE_H_INCLUDED
#define EIGENVALUEEQUATIONINTERFACE_H_INCLUDED

#include "reduced/Interface.h"
#include "expand/Interface.h"
#include "precon/Interface_decl.h"
#include "precon/Interface_impl.h"

#endif /* EIGENVALUEEQUATIONINTERFACE_H_INCLUDED */
