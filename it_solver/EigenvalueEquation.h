#ifndef EIGENVALUEEQUATION_H_INCLUDED
#define EIGENVALUEEQUATION_H_INCLUDED

#include "reduced/DEBUG.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "it_solver/SolutionVectorContainer.h"
#include "mmv/StorageForSort.h"
#include "IES.h"
#include "IterativeEquationBase.h"
#include "it_solver/EigenvalueContainerType.h"
#include "it_solver/EigenvectorContainerType.h"

/**
 * Construct solution vectors with mat/vec (real case)
 *
 * @param aSolVec
 * @param aAtrans
 * @param aTrials
 * @param aReducedEigenvector
 * @param aReducedEigenvalues
 * @param aNeq
 **/
template
   <  class SOL
   ,  class TRANS
   ,  class TRIALS
   ,  class M
   ,  class V
   >
void ConstructEigenvalueSolutionImpl
   (  SOL& aSolVec
   ,  TRANS& aAtrans
   ,  TRIALS& aTrials
   ,  M& aReducedEigenvector
   ,  V& aReducedEigenvalues
   ,  size_t aNeq
   )
{
   aSolVec.clear();

   for(size_t i=0; i<aNeq; ++i)
   {
      aSolVec.emplace_back(aAtrans.TemplateZeroVector());
      for(size_t j=0; j<aTrials.size(); ++j)
      {
         IES_Axpy(aSolVec[i],aTrials[j],aReducedEigenvector[j][i]);
      }
   }
   
   aReducedEigenvalues.SetNewSize(aNeq,true);
};

/**
 * Construct solution vectors in the complex case
 *
 * @param aSolVec
 * @param aAtrans
 * @param aTrials
 * @param aReducedEigenvectors   The Eigenvectors
 * @param aReducedEigenvalues
 * @param aNeq
 **/
template
   <  class SOL
   ,  class TRANS
   ,  class TRIALS
   ,  class M
   ,  class V
   >
void ConstructEigenvalueSolutionImpl
   (  SOL& aSolVec
   ,  TRANS& aAtrans
   ,  TRIALS& aTrials
   ,  EigenvectorContainer<M,M>& aReducedEigenvectors
   ,  EigenvalueContainer<V,V>& aReducedEigenvalues
   ,  size_t aNeq
   )
{
   //DEBUG_CheckEigenvectorOrthonormality(aReducedEigenvectors);
   Mout << " SOLVEC size when calling construct solution: " << aSolVec.size() << std::endl;
   Mout << " Number of equations:                       : " << aNeq << std::endl;

   bool imag = false;
   size_t i;
   for(i=0; i<aNeq; ++i)
   {
      if(aReducedEigenvalues.Im()[i] == C_0)
      {
         // no imag part
         aSolVec.emplace_back(aAtrans.TemplateZeroVector());
         for(size_t j=0; j<aTrials.size(); ++j)
         {
            IES_Axpy(aSolVec[i],aTrials[j],aReducedEigenvectors.Rhs()[j][i]);
         }
      }
      else
      {
         // we have an imag part
         imag = true;
         aSolVec.emplace_back(aAtrans.TemplateZeroVector());
         aSolVec.emplace_back(aAtrans.TemplateZeroVector());
         for(size_t j=0; j<aTrials.size(); ++j)
         {
            IES_Axpy(aSolVec[i],aTrials[j],aReducedEigenvectors.Rhs()[j][i]);
            IES_Axpy(aSolVec[i+1],aTrials[j],aReducedEigenvectors.Rhs()[j][i+1]);
         }
         ++i;
      }
   }
   
   // we throw away all the unconverged eigenvalues that we have not requested
   aReducedEigenvalues.Re().SetNewSize(i,true);
   aReducedEigenvalues.Im().SetNewSize(i,true);

   if(aSolVec.size() > aNeq)
   {
      if(imag)
      {
         MidasWarning(" NB last eigenvalue was imaginary, so there is an extra entry in the set of solution vectors ");
      }
      else
      {
         MIDASERROR("SHOULD NOT GET HERE");
      }
   }
}


/**
 *
 **/
template<class SOL_T
       , class TRIAL_T
       , class SIGMA_T
       , class RESID_T
       , class ATRANS
       , template<class> class STARTPOLICY
       , template<class> class PRECONPOLICY
       , template<class> class RESIDPOLICY
       , template<class> class CONVPOLICY
       , template<class> class EXPANDPOLICY
       , template<class> class REDUCEDPOLICY
       , template<class> class FINALIZEPOLICY
       >
class EigenvalueEquation: 
   public STARTPOLICY <
          PRECONPOLICY<
          RESIDPOLICY <
          CONVPOLICY  <
          EXPANDPOLICY<
          REDUCEDPOLICY<
          FINALIZEPOLICY<
            IterativeEquationBase<EigenvalueEquation<SOL_T,TRIAL_T,SIGMA_T,RESID_T,ATRANS,STARTPOLICY,PRECONPOLICY,RESIDPOLICY,CONVPOLICY,EXPANDPOLICY,REDUCEDPOLICY,FINALIZEPOLICY> > 
          > > > > > > >
{
   using This = IterativeEquationBase<EigenvalueEquation<SOL_T,TRIAL_T,SIGMA_T,RESID_T,ATRANS,STARTPOLICY,PRECONPOLICY,RESIDPOLICY,CONVPOLICY,EXPANDPOLICY,REDUCEDPOLICY,FINALIZEPOLICY> >;
   
   using REDUCED = REDUCEDPOLICY<FINALIZEPOLICY<This> >;
   friend REDUCED;

   private:
      MAKE_REFVARIABLE(ATRANS,Atrans);

      MAKE_VARIABLE(StandardContainer<SOL_T>,SolVec);
      MAKE_VARIABLE(StandardContainer<TRIAL_T>,Trials);
      MAKE_VARIABLE(StandardContainer<SIGMA_T>,Sigma);
      MAKE_VARIABLE(StandardContainer<RESID_T>,Residuals);

      MAKE_VARIABLE(EigenvectorContainerType<typename REDUCED::REDUCEDEQ>,ReducedEigenvectors);
      
      MAKE_VARIABLE(EigenvalueContainerType<typename REDUCED::REDUCEDEQ>,Eigenvalues);
      MAKE_VARIABLE(EigenvalueContainerType<typename REDUCED::REDUCEDEQ>,EigenvaluesOld);
      
      void DoTransforms()
      {
         this->DoTransformsImpl(mAtrans,mTrials,mSigma,this->self().IoLevel());
      }

   public:
      void ConstructSolution()
      {
         this->mSolVec.Clear();
         ConstructEigenvalueSolutionImpl
            (  mSolVec
            ,  mAtrans
            ,  mTrials
            ,  mReducedEigenvectors
            ,  mEigenvalues
            ,  this->Neq()
            );
      }

//      template
//         <  typename T=SOL_T
//         ,  typename std::enable_if<std::is_same<T, TensorDataCont>::value>::type* = nullptr
//         >
//      void ConstructSolution()
//      {
//         this->mSolVec.Clear();
//         ConstructDecomposedEigenvalueSolutionImpl
//            (  mSolVec
//            ,  mAtrans
//            ,  mTrials
//            ,  mReducedEigenvectors
//            ,  mEigenvalues
//            ,  this->Neq()
//            ,  this->self().DecompInfo()
//            );
//      }

      explicit EigenvalueEquation(ATRANS& aAtrans): mAtrans(aAtrans)
      {
      }

      /**
       * Check that the solution is still a solution.
       *
       * !! NB: This will destroy the state of your solver.
       **/
      bool CheckConvergedSolution
         (  const TRIAL_T& aTrial
         ,  Nb aEigValRe
         ,  Nb aEigValIm
         )
      {
         // Clear solver and reset reduced eigenvectors
         this->self().ClearSolver(); // we clear any trial vectors before we start
         this->mReducedEigenvectors.Rhs().SetNewSize(1,1);
         this->mReducedEigenvectors.Rhs()(0,0) = C_1;
         this->mReducedEigenvectors.Lhs().SetNewSize(1,1);
         this->mReducedEigenvectors.Lhs()(0,0) = C_1;
         this->SetNeq(I_1);

         // Add trial and eigval
         this->self().mTrials.emplace_back(aTrial);
         mEigenvalues.SetNewSize(1);
         mEigenvalues.Re()[0] = aEigValRe;
         mEigenvalues.Im()[0] = aEigValIm;

         // Do reduced solution (transforms, etc.)
         this->self().DoTransforms();

         // Make residual
         this->self().MakeResidual();

         // Check convergence
         return this->self().CheckConvergence();
      }
      
      void SetNeq(In aNeq)
      {
         this->self().Neq() = aNeq;
      }

      SolVec_t& Solution()
      {
         return mSolVec;
      }

      Eigenvalues_t& GetEigenvalues()
      {
         return mEigenvalues;
      }

      std::string Type() const
      {
         return {"Eigenvalue Equation"};
      }
};

#endif /* EIGENVALUEEQUATION_H_INCLUDED */
