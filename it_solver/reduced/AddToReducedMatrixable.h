#ifndef ADDTOREDUCEDMATRIXABLE_H_INCLUDED
#define ADDTOREDUCEDMATRIXABLE_H_INCLUDED

#include "it_solver/IES.h"
//#include "mmv/MidasMatrix.h"

enum TransformerType { TRANS_HERM, TRANS_NHERM };

class AddToReducedMatrixableY
{
   public:
      /**
       * Add "reduced" entries for the new trials in Y type matrix
       **/
      template<class T, class Y, class M>
      void AddToReducedMatrixYImpl(T& trials, Y& y, M& mat)
      {
         // Assumes mat has MidasMatrix interface
         auto old_dim = mat.Nrows();
         auto new_dim = trials.size();
         auto y_dim = y.size();
         mat.SetNewSize(new_dim,y_dim,true,true);
         for(size_t i=old_dim; i<new_dim; ++i)
         {
            for(size_t y_idx=0; y_idx<y_dim; ++y_idx)
            {
               mat[i][y_idx] = IES_Dot(trials[i],y[y_idx]);
            }
         }
      }
};

class AddToReducedMatrixableComplexY
{
   public:
      /**
       * Add "reduced" entries for the new trials in Y type matrix
       **/
      template<class T, class Y, class M>
      void AddToReducedMatrixComplexYImpl(T& trials, Y& y, M& mat)
      {
         // Assumes mat has MidasMatrix interface
         assert(mat.Nrows()%2 == 0); // assert that we have a multiple of 2 number of rows

         auto old_dim = mat.Nrows()/2;
         auto new_dim = trials.size();
         auto new_trials = new_dim - old_dim;
         auto y_dim = y.size();
         mat.SetNewSize(2*new_dim,y_dim,true,true);
         
         // move imag part #new_trials positions down
         for(int i=2*old_dim-1; i>=old_dim; --i)
         {
            for(int y_idx=0; y_idx<y_dim; ++y_idx)
            {
               mat[i+new_trials][y_idx] = mat[i][y_idx];
            }
         }

         // than calculate new entries
         for(int i=old_dim; i<new_dim; ++i)
         {
            for(int y_idx=0; y_idx<y_dim; ++y_idx)
            {
               mat[i][y_idx] = IES_Dot(trials[i],y[y_idx].Re());
               mat[i+new_dim][y_idx] = -IES_Dot(trials[i],y[y_idx].Im()); 
            }
         }
      }
};

class AddToReducedMatrixableS
{
   protected:
      template
         <  class T
         ,  class S
         >
      void AddToReducedMatrixSImpl
         (  T& trials
         ,  S& mat
         )
      {
         // Assumes mat has MidasMatrix interface
         auto old_dim = mat.Ncols();
         auto new_dim = trials.size();
         mat.SetNewSize(new_dim,true,true);
         for(size_t i=old_dim; i<new_dim; ++i)
         {
            for(size_t j=0; j<new_dim; ++j)
            {
               mat[i][j]   =  i == j
                           ?  IES_Norm2(trials[i])
                           :  IES_Dot(trials[i], trials[j]);
            }
            for(size_t j=0; j<old_dim; ++j)
            {
               mat[j][i] = mat[i][j];
            }
         }
      
      }
};

template<TransformerType, class A>
class AddToReducedMatrixable;

template<class A>
class AddToReducedMatrixable<TransformerType::TRANS_NHERM, A>:
   public A
{
   protected:
      /**
       * Add "reduced" entries for the new trials
       **/
      template<class T, class U, class M>
      void AddToReducedMatrixImpl(T& trials, U& trans, M& mat)
      {
         // Assumes mat has MidasMatrix interface
         auto old_dim = mat.Ncols();
         auto new_dim = trials.size();
         assert(trans.size()==new_dim);
         mat.SetNewSize(new_dim,true,true);
         for(size_t i=old_dim; i<new_dim; ++i)
         {
            for(size_t j=0; j<new_dim; ++j)
            {
               mat[i][j] = IES_Dot(trials[i],trans[j]);
            }
            for(size_t j=0; j<old_dim; ++j)
            {
               mat[j][i] = IES_Dot(trials[j],trans[i]);
            }
         }
      }
};


template<class A>
class AddToReducedMatrixable<TransformerType::TRANS_HERM, A>:
   public A
{
   protected:
      /**
       * Add "reduced" entries for the new trials symmetric version
       **/
      template<class T, class U, class M>
      void AddToReducedMatrixImpl(T& trials, U& trans, M& mat)
      {
         // Assumes mat has MidasMatrix interface
         auto old_dim = mat.Ncols();
         auto new_dim = trials.size();
         //assert(trials.size()==new_dim);
         assert(trans.size()==new_dim);
         mat.SetNewSize(new_dim,true,true);
         for(size_t i=old_dim; i<new_dim; ++i)
         {
            for(size_t j=0; j<new_dim; ++j)
            {
               mat[i][j] = IES_Dot(trials[i],trans[j]);
            }
            for(size_t j=0; j<old_dim; ++j)
            {
               mat[j][i] = mat[i][j];
            }
         }
      }
};

#endif /* ADDTOREDUCEDMATRIXABLE_H_INCLUDED */
