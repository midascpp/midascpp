#ifndef TRANSFORMIMPLEMENTATION_H_INCLUDED
#define TRANSFORMIMPLEMENTATION_H_INCLUDED

#include "it_solver/IES.h"
#include "util/CallStatisticsHandler.h"

namespace detail
{

class TransformImplementation
{
   public:
      /**
       *
       **/
      template
         <  class F
         ,  class U
         ,  class V
         >
      void DoTransformsImpl
         (  F& transformer
         ,  U& trials
         ,  V& transtrials
         ,  In iolevel
         )
      {
         LOGCALL("transforms");
         auto old_dim = transtrials.size();
         auto new_dim = trials.size();

         Timer timer;
         size_t count = 0;
         for(size_t i=old_dim; i<new_dim; ++i)
         {
            if (  gTime
               || iolevel > I_7
               )
            {
               Mout  << " == Transforming trial vector " << count++ << " == " << std::endl;
            }
            timer.Reset();
            transtrials.emplace_back(transformer.TemplateZeroVector());
            transformer.Transform(trials[i],transtrials.back());
            if (  gTime
               )
            {
               timer.CpuOut(Mout, "    CPU time spent on transforming trial vector:  ");
               timer.WallOut(Mout, "    Wall time spent on transforming trial vector: ");
            }
         }
         assert(transtrials.size()==trials.size()); // sanity check
      }
};

} /* namespace detail */

#endif /* TRANSFORMIMPLEMENTATION_H_INCLUDED */
