#ifndef CHECKFORCOMPLEXEIGENVALUES_H_INCLUDED
#define CHECKFORCOMPLEXEIGENVALUES_H_INCLUDED

#include "lapack_interface/EigenvalueUtils.h"

template<class A>
bool CheckForComplexEigenvalues(const A& mat)
{
   //auto eig_sol = GEEV(mat);
   //return has_complex_eigenvalues(eig_sol);
   return false;
}

#endif /* CHECKFORCOMPLEXEIGENVALUES_H_INCLUDED */
