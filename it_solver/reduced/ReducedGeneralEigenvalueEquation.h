#ifndef REDUCEDGENERALEIGENVALUEEQUATION_H_INCLUDED
#define REDUCEDGENERALEIGENVALUEEQUATION_H_INCLUDED

#include "ReducedEquationBase.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/LapackInterface.h"
#include "Eigensolvable.h"
#include "MatrixTag.h"

/**
 * Reduced General Eigenvalue equation (RGGE)
 * AX = eBX
 **/
template
   <  class A
   ,  class B
   ,  TransformerType TT
   ,  class REDUCED = ReducedEquation
         <  TT
         ,  Eigensolvable<TT, PROBLEM_TYPE::PROBLEM_TYPE_GEN>
         >
   >
class RGEE_Impl
   :  public REDUCED
{
   private:
      A mA;
      B mB;


   public:
      explicit RGEE_Impl(): REDUCED(), mA(), mB()
      {
      }

      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         REDUCED::AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatB_t)
      {
         REDUCED::AddToReducedMatrixImpl(trials,trans,mB);
      }
      
      /**
       * Evaluate interface
       **/
      template<class T, class U>
      void Evaluate(T& aReducedEigVecs, U& aReducedEigVals)
      {
         //REDUCED::EigenSolution(mA,mB,aReducedEigVecs,aReducedEigVals,ilapack::iDSYGVD);
         REDUCED::EigenSolution(mA,mB,aReducedEigVecs,aReducedEigVals);
      }

      /**
       * Calculate Rayleigh quotients as more accurate eigenvalue approximations (real version)
       *
       * @param aReducedEigVecs        The reduced eigenvectors (problem must be solved first!)
       * @param aEigVals               The reduced eigenvalues
       * @param aHarmonicRayleighRitz  Use mB as V^T A V instead of mA.
       **/
      MidasVector CalculateRayleigQuotients
         (  const MidasMatrix& aReducedEigVecs
         ,  const MidasVector& aEigVals
         ,  bool aHarmonicRayleighRitz
         )  const
      {
         auto size = aEigVals.Size();
         assert(size == aReducedEigVecs.Ncols());
         auto r_coefs = aEigVals;
         r_coefs.Zero();

         MidasVector col_i(aReducedEigVecs.Nrows());

         for(size_t i=0; i<size; ++i)
         {
            aReducedEigVecs.GetCol(col_i, i);
            if (  aHarmonicRayleighRitz
               )
            {
               MidasVector tmp = col_i;
               tmp.MultiplyWithMatrix(mB);
               r_coefs[i] = Dot(col_i, tmp);
            }
            else
            {
               MidasVector tmp = col_i;
               tmp.MultiplyWithMatrix(mA);
               r_coefs[i] = Dot(col_i, tmp);
            }
         }

         return r_coefs;
      }



      /**
       * Calculate Rayleigh quotients as more accurate eigenvalue approximations (complex version)
       *
       * @param aReducedEigVecs        The reduced eigenvectors (problem must be solved first!)
       * @param aEigVals               The reduced eigenvalues
       * @param aHarmonicRayleighRitz  Use mB = V^T A^T V instead of mA = V^T A V (which is not the case for HarmonicRR).
       **/
      EigenvalueContainer<MidasVector, MidasVector> CalculateRayleighQuotients
         (  const EigenvectorContainer<MidasMatrix, MidasMatrix>& aReducedEigVecs
         ,  const EigenvalueContainer<MidasVector, MidasVector>& aEigVals
         ,  bool aHarmonicRayleighRitz
         )  const
      {
//         Mout  << " Reduced eigenvalues (Re):\n" << aEigVals.Re() << "\n"
//               << " Reduced eigenvalues (Im):\n" << aEigVals.Im() << "\n"
//               << std::flush;

         auto size = aEigVals.Re().Size();
         assert(size == aReducedEigVecs.Rhs().Ncols());
         
         auto r_coefs = aEigVals;
         r_coefs.Re().Zero();
         r_coefs.Im().Zero();

         MidasVector col_i(aReducedEigVecs.Rhs().Nrows());
         MidasVector col_ip1(aReducedEigVecs.Rhs().Nrows());

         for(size_t i=0; i<size; ++i)
         {
            aReducedEigVecs.Rhs().GetCol(col_i, i);
            if (  aEigVals.Im()[i]
               )
            {
               aReducedEigVecs.Rhs().GetCol(col_ip1, i);

               MidasVector col_i_trans = col_i;
               MidasVector col_ip1_trans = col_ip1;

               if (  aHarmonicRayleighRitz
                  )
               {
                  col_i_trans.MultiplyWithMatrix(mB);
                  col_ip1_trans.MultiplyWithMatrix(mB);
               }
               else
               {
                  col_i_trans.MultiplyWithMatrix(mA);
                  col_ip1_trans.MultiplyWithMatrix(mA);
               }

//               Mout  << " col_i:\n" << col_i << "\n"
//                     << " col_ip1:\n" << col_ip1 << "\n"
//                     << " col_i_trans:\n" << col_i_trans << "\n"
//                     << " col_ip1_trans:\n" << col_ip1_trans << "\n"
//                     << std::flush;

               r_coefs.Re()[i] += Dot(col_i, col_i_trans);
               r_coefs.Re()[i] += Dot(col_ip1, col_ip1_trans);

               r_coefs.Im()[i] += Dot(col_i, col_ip1_trans);
               r_coefs.Im()[i] -= Dot(col_ip1, col_i_trans);

               // If we are using B = V^T A^T V, the imag parts have negative sign
               if (  aHarmonicRayleighRitz
                  )
               {
                  r_coefs.Im()[i] = -r_coefs.Im()[i];
               }

               ++i;
            }
            else
            {
               MidasVector col_i_trans = col_i;
               if (  aHarmonicRayleighRitz
                  )
               {
                  col_i_trans.MultiplyWithMatrix(mB);
               }
               else
               {
                  col_i_trans.MultiplyWithMatrix(mA);
               }

//               Mout  << " col_i:\n" << col_i << "\n"
//                     << " col_i_trans:\n" << col_i_trans << "\n"
//                     << std::flush;

               r_coefs.Re()[i] = Dot(col_i, col_i_trans);
               r_coefs.Im()[i] = C_0;
            }
         }

//         Mout  << " Reyleigh quotients (Re):\n" << r_coefs.Re() << "\n"
//               << " Reyleigh quotients (Im):\n" << r_coefs.Im() << "\n"
//               << std::flush;

         return r_coefs;
      }

      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         mA.SetNewSize(I_0,I_0);
         mB.SetNewSize(I_0,I_0);
      }

      /**
       * Get matrix
       **/
      const A& GetAMatrix() const
      {
         return mA;
      }

      /**
       * Get matrix
       **/
      const B& GetBMatrix() const
      {
         return mB;
      }
};

// interface 
template<class A, class B>
using ReducedGeneralEigenvalueEquation = RGEE_Impl<A,B,TransformerType::TRANS_NHERM>;

template<class A, class B>
using ReducedGeneralHermitianEigenvalueEquation = RGEE_Impl<A,B,TransformerType::TRANS_HERM>;

#endif /* REDUCEDGENERALEIGENVALUEEQUATION_H_INCLUDED */
