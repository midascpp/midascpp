#ifndef REDUCED_INTERFCE_H_INCLUDED
#define REDUCED_INTERFCE_H_INCLUDED

// reduced space solvers
#include "ReducedGeneralEigenvalueEquation.h"
#include "ReducedEigenvalueEquation.h"
#include "ReducedLinearEquation.h"

#include "ReducedSolver.h"
#include "ReducedLinearSolver.h"

#include "Targeter.h"

template<class A>
using ReducedHermitianEigenvalueSolver = ReducedSolver<ReducedHermitianEigenvalueEquation<MidasMatrix>, NoTargeter<A> >;

template<class A>
using ReducedTargetingEigenvalueSolver = ReducedSolver<ReducedEigenvalueEquation<MidasMatrix>, OverlapSumTargeter<DataCont, A> >;

template<class A>
using ReducedTargetingHermitianEigenvalueSolver = ReducedSolver<ReducedHermitianEigenvalueEquation<MidasMatrix>, OverlapSumTargeter<DataCont, A> >;

template<class A>
using ReducedNonHermitianEigenvalueSolver = ReducedSolver<ReducedEigenvalueEquation<MidasMatrix>, NoTargeter<A> >;

template<class A>
using NonOrthoReducedSolver = NonOrthogonalReducedSolver<ReducedGeneralEigenvalueEquation<MidasMatrix,MidasMatrix>, NoTargeter<A> >;

template<class A>
using TensorNonOrthoReducedTargetingSolver = NonOrthogonalReducedSolver<ReducedGeneralEigenvalueEquation<MidasMatrix,MidasMatrix>, OverlapSumTargeter<TensorDataCont, A> >;

template<class A>
using TensorReducedTargetingSolver = ReducedSolver<ReducedEigenvalueEquation<MidasMatrix>, OverlapSumTargeter<TensorDataCont, A> >;

template<class A>
using ReducedNonHermitianLinearSolver = ReducedLinearSolver<ReducedNonHermitianLinearEquation<MidasMatrix,MidasMatrix>, A>;

template<class A>
using ReducedNonHermitianFreqShiftedLinearSolver = ReducedFreqShiftedLinearSolver<ReducedNonHermitianFreqShiftedLinearEquation<MidasMatrix,MidasMatrix>, A>;

template<class A>
using NonOrthoReducedNonHermitianFreqShiftedLinearSolver = NonOrthoReducedFreqShiftedLinearSolver<ReducedNonHermitianFreqShiftedLinearEquation<MidasMatrix, MidasMatrix>, A>;

template<class A>
using ReducedNonHermitianIndividuallyFreqShiftedLinearSolver = ReducedFreqShiftedLinearSolver<ReducedNonHermitianIndividuallyFreqShiftedLinearEquation<MidasMatrix,MidasMatrix>, A>;

template<class A>
using ReducedNonHermitianComplexFreqShiftedLinearSolver = ReducedFreqShiftedLinearSolver<ReducedNonHermitianComplexFreqShiftedLinearEquation<MidasMatrix,MidasMatrix>, A>;

#endif /* REDUCED_INTERFCE_H_INCLUDED */
