#ifndef REDUCEDSOLVER_H_INCLUDED
#define REDUCEDSOLVER_H_INCLUDED

#include <algorithm>
#include <set>
#include <utility>
#include "TransformImplementation.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "it_solver/IES_Macros.h"
#include "util/CallStatisticsHandler.h"

/**
 *
 **/
template
   <  class REDUCED
   ,  class A
   >
class ReducedSolver 
   :  public A
   ,  private REDUCED
   ,  protected detail::TransformImplementation
{
   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void Initialize()
      {
         A::Initialize();
      }

      void DoReducedSolution()
      {
         LOGCALL("ortho reduced");
         Mout << " Doing reduced solutions " << std::endl;
         this->self().DoTransforms();

         REDUCED::AddToReducedMatrix(this->self().Trials(), this->self().Sigma(), matrix_tag::MatA);

         REDUCED::Evaluate(this->self().ReducedEigenvectors(),this->self().Eigenvalues());

         A::TargetAndSortSolution(this->self().Eigenvalues(), this->self().ReducedEigenvectors());
      }
      
      void ClearSolver()
      {
         REDUCED::ClearReducedSolution();
         A::ClearSolver();
      }

      //! Harmonic Rayleigh-Ritz can only be used with NonOrtho solver
      constexpr bool HarmonicRayleighRitz
         (
         )  const
      {
         return false;
      }

      //! Shift is just zero, since we are not using HarmonicRR
      constexpr Nb HarmonicRrShift
         (
         )  const
      {
         return C_0;
      }
};

/**
 *
 **/
template
   <  class REDUCED
   ,  class A
   >
class NonOrthogonalReducedSolver
   :  public A
   ,  private REDUCED
   ,  protected detail::TransformImplementation
{
   private:
      //! Use the harmonic Rayleigh-Ritz procedure
      MAKE_VARIABLE(bool, HarmonicRayleighRitz, false);

      //! Use the Rayleigh quotients as improved eigenvalue approximations
      MAKE_VARIABLE(bool, UseRayleighQuotientsAsEigvals, false);

      //! Refine the eigenvalues by minimizing ||(A-rho*I)x|| over all x. Rho can be the reduced eigenvalue or the Rayleigh quotient.
      MAKE_VARIABLE(bool, RefinedHarmonicRr, false);

      //! Shift the harmonic eigenvalue problem in order to separate eigenvalues close to the shift
      MAKE_VARIABLE(Nb, HarmonicRrShift, C_0);

      //! Adapt the shift to the average of the non-converged eigenvalues.
      MAKE_VARIABLE(Nb, AdaptiveHarmonicRrShift, false);


   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void Initialize()
      {
         A::Initialize();
      }

      void DoReducedSolution()
      {
         LOGCALL("non-ortho reduced");
         Mout << " Doing non-orthogonal reduced solutions " << std::endl;

         // Do transformations
         this->self().DoTransforms();

         // Construct matrices
         if (  this->mHarmonicRayleighRitz
            )
         {
            // Update the shift
            if (  this->mAdaptiveHarmonicRrShift
               )
            {
               this->UpdateHarmonicRrShift(this->self().Eigenvalues());
            }

            // Shift the basis if we are using a shift
            if (  this->mHarmonicRrShift
               )
            {
               auto shifted_sigma = this->self().Sigma();
               for(size_t i = 0; i<this->self().Sigma().size(); ++i)
               {
                  IES_Axpy(shifted_sigma[i], this->self().Trials()[i], -this->mHarmonicRrShift);
               }
               REDUCED::AddToReducedMatrix(shifted_sigma, shifted_sigma, matrix_tag::MatA);
               REDUCED::AddToReducedMatrix(shifted_sigma, this->self().Trials(), matrix_tag::MatB);
            }
            else
            {
               REDUCED::AddToReducedMatrix(this->self().Sigma(), this->self().Sigma(), matrix_tag::MatA);
               REDUCED::AddToReducedMatrix(this->self().Sigma(), this->self().Trials(), matrix_tag::MatB);
            }
         }
         else
         {
            REDUCED::AddToReducedMatrix(this->self().Trials(), this->self().Sigma(), matrix_tag::MatA);
            REDUCED::AddToReducedMatrix(this->self().Trials(), this->self().Trials(), matrix_tag::MatB);
         }

         // Output matrix if requested
         if (  this->self().IoLevel() >= I_10
            )
         {
            Mout  << " Reduced matrix:\n" << REDUCED::GetAMatrix() << std::endl;

            Mout  << " Overlap matrix:\n" << REDUCED::GetBMatrix() << std::endl;
         }

         // Solve reduced problem
         REDUCED::Evaluate(this->self().ReducedEigenvectors(), this->self().Eigenvalues());

         // Output reduced solution if requested
         if (  this->self().IoLevel() >= I_10
            )
         {
            Mout  << " Reduced eigenvalues (Re):\n" << this->self().Eigenvalues().Re() << "\n"
                  << " Reduced eigenvalues (Im):\n" << this->self().Eigenvalues().Im() << "\n"
                  << " Reduced eigenvectors (Rhs):\n" << this->self().ReducedEigenvectors().Rhs() << "\n"
                  << " Reduced eigenvectors (Lhs):\n" << this->self().ReducedEigenvectors().Lhs() << "\n"
                  << std::flush;
         }

         // Aftertreatment in case of harmonic Rayleigh-Ritz
         if (  this->mHarmonicRayleighRitz
            )
         {
            if (  this->mUseRayleighQuotientsAsEigvals
               )
            {
               auto r_coefs = REDUCED::CalculateRayleighQuotients(this->self().ReducedEigenvectors(), this->self().Eigenvalues(), this->mHarmonicRayleighRitz);

               Mout  << " Replacing reduced eigenvalues by Rayleigh quotients." << std::endl;
               this->self().Eigenvalues() = r_coefs;
            }

            // Refine the harmonic eigenvectors
            if (  this->mRefinedHarmonicRr
               )
            {
               MidasWarning("Refined harmonic Rayleigh-Ritz has not been implemented yet!");

               // FOR ALL EIGENVALUES, DO THE FOLLOWING:
               // 1)
               // Construct S_i matrix from G, H, eigval_i, and shift (G = U^T (A-shift*I)^T (A-shift*I) U, and H = U^T (A-shift*I)^T U, where U is the trial space).
               // S_i = G + (sigma-rho_i)(H^T + H) + |sigma-rho_i|^2 I, where rho_i is the i'th reduced eigenvalue or Rayleigh quotient.
               // ...

               // 2)
               // Find smallest eigenvalue and (normalized) eigenvector of S_i matrix. Use e.g. LAPACK SYEVR.
               // Update the reduced eigenvector for eigenvalue i.
               // ...

               // 3)
               // Optionally calculate Rayleigh quotient with the refined eigenvector and replace the eigenvalue.
               // ...
            }

            // Shift the eigenvalues before sorting
            if (  this->mHarmonicRrShift
               )
            {
               this->ShiftEigenvalues(this->self().Eigenvalues(), this->mHarmonicRrShift);
            }
         }

         // Do targeting and solution sorting
         A::TargetAndSortSolution(this->self().Eigenvalues(), this->self().ReducedEigenvectors());
      }

      /**
       *    Add a constant shift to the eigenvalues
       **/
      void ShiftEigenvalues
         (  MidasVector& eigvals
         ,  Nb shift
         )
      {
         for(size_t i=0; i<eigvals.Size(); ++i)
         {
            eigvals[i] += shift;
         }
      }

      /**
       *    Add a constant shift to the real part of the eigenvalues
       **/
      void ShiftEigenvalues
         (  EigenvalueContainer<MidasVector, MidasVector>& eigvals
         ,  Nb shift
         )
      {
         for(size_t i=0; i<eigvals.Re().Size(); ++i)
         {
            eigvals.Re()[i] += shift;
         }
      }

      /**
       *    Set the shift parameter to be the average of the non-converged eigenvalues
       **/
      void UpdateHarmonicRrShift
         (  MidasVector& eigvals
         )
      {
         if (  eigvals.Size() != this->self().Neq()
            )
         {
            //Mout  << " DEBUG: Number of eigenvalues is not equal to number of equations - should be fine if we are in the first iteration..." << std::endl;
            return;
         }

         Nb sum = C_0;
         size_t n_vals = 0;

         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            if (  !this->self().Converged()[i]
               )
            {
               ++n_vals;
               sum += eigvals[i];
            }
         }
         auto new_shift = sum / n_vals;

         Mout  << " Updating harmonic Rayleigh-Ritz shift from: " << this->mHarmonicRrShift << " to: " << new_shift << std::endl;
         this->mHarmonicRrShift = new_shift;
      }

      /**
       *    Set the shift parameter to be the average of the non-converged eigenvalues
       **/
      void UpdateHarmonicRrShift
         (  EigenvalueContainer<MidasVector, MidasVector>& eigvals
         )
      {
         if (  eigvals.Re().Size() != this->self().Neq()
            )
         {
            //Mout  << " DEBUG: Number of eigenvalues is not equal to number of equations - should be fine if we are in the first iteration..." << std::endl;
            return;
         }

         Nb sum = C_0;
         size_t n_vals = 0;

         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            if (  !this->self().Converged()[i]
               )
            {
               ++n_vals;
               sum += eigvals.Re()[i];
            }
         }
         auto new_shift = sum / n_vals;

         Mout  << " Updating harmonic Rayleigh-Ritz shift from: " << this->mHarmonicRrShift << " to: " << new_shift << std::endl;
         this->mHarmonicRrShift = new_shift;
      }

      
      void ClearSolver()
      {
         REDUCED::ClearReducedSolution();
         A::ClearSolver();
      }
};

#endif /* REDUCEDSOLVER_H_INCLUDED */
