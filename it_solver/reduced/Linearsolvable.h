#ifndef LINEARSOLVABLE_H_INCLUDED
#define LINEARSOLVABLE_H_INCLUDED

#include "it_solver/reduced/ProblemType.h"
#include "lapack_interface/ILapack.h"
#include "it_solver/EigenvectorContainer.h"
#include "it_solver/EigenvalueContainer.h"

class Linearloadable
{
   private:
      template<class T>
      void SetReducedSolution(GeneralMidasMatrix<T>& aSolution, Linear_eq_struct<T>& aLinear)
      {
         //aSolution.CopyDataFromPtr(aLinear._solution, aLinear._n, aLinear._n_rhs, ilapack::col_major);
         LoadSolution(aLinear, aSolution);
      }

   protected:
      /**
       * load linear solution from Linear_eq_struct
       **/
      template<class T, class L>
      void LoadLinearEqData(T& aSolution, L& aLinear)
      {
         SetReducedSolution(aSolution,aLinear);
      }

};

template<TransformerType, PROBLEM_TYPE>
class Linearsolvable;

template<>
class Linearsolvable<TransformerType::TRANS_NHERM, PROBLEM_TYPE::PROBLEM_TYPE_STD>: 
   protected Linearloadable
{
   protected:
      template<class MATA, class MATY, class SOL>
      void LinearSolution(const MATA& aA, const MATY& aY, SOL& aSolution)
      {
         auto lin_sol = GESV(aA,aY);
         Linearloadable::LoadLinearEqData(aSolution,lin_sol);
      }
};

#endif /* LINEARENSOLVABLE_H_INCLUDED */
