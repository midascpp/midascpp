/**
************************************************************************
* 
* @file    Targeter.h
*
* @date    16-08-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Just collecting all targeting (like the OverlapSum targeter written by 
*     Mads B. Hansen) in one framework.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TARGETER_H_INCLUDED
#define TARGETER_H_INCLUDED

#include <tuple>

#include "it_solver/reduced/ReducedSolver.h"
#include "mmv/StorageForSort.h"

namespace detail
{

class SolutionSorter
{
   public:
      /**
       * 
       **/
      void SortSolution
         (  MidasVector& eigvals
         ,  MidasMatrix& eigvecs
         ,  In n_first = -I_1
         ,  bool order_abs_vals = true
         )
      {
         MidasVector dummy_im_eigvals(eigvals.Size(),C_0);
         MidasMatrix dummy_lhs_eigvecs;
         
         if (  n_first > -I_1
            )
         {
            Sort3(eigvals, eigvecs, dummy_im_eigvals, dummy_lhs_eigvecs, n_first, order_abs_vals);
         }
         else
         {
            Sort3(eigvals, eigvecs, dummy_im_eigvals, dummy_lhs_eigvecs, order_abs_vals);
         }
      }

      /**
       *
       **/  
      void SortSolution
         (  MidasVector& eigvals
         ,  MidasMatrix& eigvecs
         ,  In n_begin
         ,  In n_end
         ,  bool order_abs_vals
         )
      {
         MidasVector dummy_im_eigvals(eigvals.Size(),C_0);
         MidasMatrix dummy_lhs_eigvecs;
         Sort3(eigvals, eigvecs, dummy_im_eigvals, dummy_lhs_eigvecs, n_begin, n_end, order_abs_vals);
      }
      
      /**
       *
       **/  
      void SortSolution
         (  EigenvalueContainer<MidasVector,MidasVector>& eigvals
         ,  EigenvectorContainer<MidasMatrix,MidasMatrix>& eigvecs
         ,  In n_first = -I_1
         ,  bool order_abs_vals = true
         )
      {
         if (  n_first > -I_1
            )
         {
            Sort3(eigvals.Re(), eigvecs.Rhs(), eigvals.Im(), eigvecs.Lhs(), n_first, order_abs_vals);
         }
         else
         {
            Sort3(eigvals.Re(), eigvecs.Rhs(), eigvals.Im(), eigvecs.Lhs(), order_abs_vals);
         }
      }

      /**
       *
       **/  
      void SortSolution
         (  EigenvalueContainer<MidasVector,MidasVector>& eigvals
         ,  EigenvectorContainer<MidasMatrix,MidasMatrix>& eigvecs
         ,  In n_begin
         ,  In n_end
         ,  bool order_abs_vals
         )
      {
         Sort3(eigvals.Re(), eigvecs.Rhs(), eigvals.Im(), eigvecs.Lhs(), n_begin, n_end, order_abs_vals);
      }
};

} /* namespace detail */



template
   <  class A
   >
class NoTargeter
   :  public A
   ,  private detail::SolutionSorter
{
   private:

   public:
      //! Constructor
      explicit NoTargeter()
      {
      }

      template
         <  class... Ts
         >
      void TargetAndSortSolution
         (  Ts&&...
         )
      {
         // No targeting, just sort according to eigenvalue
         SortSolution(this->self().Eigenvalues(), this->self().ReducedEigenvectors());
      }

      //! Reset
      void ResetTargeter
         (
         )
      {
         return;
      }

};


template
   <  class TARGET_T
   ,  class A
   >
class OverlapSumTargeter
   :  public A
   ,  private detail::SolutionSorter
{
   private:
      //! The target vectors that we are trying to hit.
      MAKE_VARIABLE(std::vector<TARGET_T>, Targets);

      //! Overlap matrix <target|trial>.
      MAKE_VARIABLE(MidasMatrix, Overlaps);

      //! Add vectors until the sum of squared overlaps reach this value.
      MAKE_VARIABLE(Nb, OverlapSumMin, static_cast<Nb>(0.8));

      //! Maximum number of hit vectors per target.
      MAKE_VARIABLE(In, OverlapNMax, I_10_3);

      //! Minimum allowed overlap for included hit vector.
      MAKE_VARIABLE(Nb, OverlapMin, C_I_10_4);

      //! Overlap sums from previous iteration.
      MAKE_VARIABLE(std::vector<Nb>, OverlapSumLastIter);

      //! Numbers of vectors from previous iteration.
      MAKE_VARIABLE(std::vector<In>, OverlapNLastIter);

      //! Numbers of complex eigenpairs from previous iteration.
      MAKE_VARIABLE(std::vector<In>, OverlapNImagLastIter);

      //! Indices of previous hit vectors.
      MAKE_VARIABLE(std::vector<In>, PreviousHits);

      //! Importance measures of previous hit vectors.
      MAKE_VARIABLE(std::vector<Nb>, PreviousHitImportances);

      //! Eigenvalues of previous hit vectors.
      MAKE_VARIABLE(std::vector<Nb>, PreviousHitEigvals);

      //! Replace the target vectors by reduced solutions in the first iteration.
      MAKE_VARIABLE(bool, PreDiagonalization, false);


      //! Target implementation for real systems. 
      // NB: This needs to be modified like the complex version (calculate importance measures, etc.)
      void TargetingImpl
         (  MidasVector& arEigVals
         ,  MidasMatrix& arEigVecs
         )
      {
         //Update overlap matrix:
         In old_cols = mOverlaps.Ncols();
         In new_cols = this->self().Trials().size();
         mOverlaps.SetNewSize(mTargets.size(), new_cols);
         for(In j=old_cols; j<mOverlaps.Ncols(); ++j)
         {
            for(In i=I_0; i<mOverlaps.Nrows(); ++i)
            {
               mOverlaps[i][j] = IES_Dot(mTargets[i], this->self().Trials()[j]);
            }
         }

         //Matrix of overlaps between current eigenvectors and targets:
         MidasMatrix overlap_eigvec = mOverlaps * arEigVecs;

         //Now we loop over the target vectors to find eigenvectors whose overlap-squared
         //accumulate to above desired value.
         //An std::set holds the indices of the eigenvectors that fulfil the requirement.
         std::set<In> indices;
         for(In i=0; i<overlap_eigvec.Nrows(); ++i)
         {
            //Actually, we look at the (modulus) square values:
            for(In j=0; j<overlap_eigvec.Ncols(); ++j)
            {
               overlap_eigvec[i][j] *= overlap_eigvec[i][j];
            }

            //A permutation vector used when finding the largest overlaps.
            //It's first n entries hold the indices of the elements with largest
            //overlaps, in decreasing order (at the end of the iteration, that is).
            std::vector<int> perm(overlap_eigvec.Ncols());
            for(In k=I_0; k<perm.size(); ++k)
            {
               perm[k] = k;
            }

            //Consecutively find largest element (of remaining) until overlap sum
            //reaches the goal:
            Nb overlap_sum = C_0;
            In n = I_0;
            In n_max = std::min(mOverlapNMax, overlap_eigvec.Ncols());

            while((overlap_sum < mOverlapSumMin) && (n < n_max))
            {
               Nb overlap_max = C_0;
               //Locate maximal element among the remaining ones in the row:
               In max = n;
               for(In j = max + I_1; j<perm.size(); ++j)
               {
                  if(overlap_eigvec[i][perm[max]] < overlap_eigvec[i][perm[j]])
                  {
                     overlap_max = overlap_eigvec[i][perm[j]];
                     max = j;
                  }
               }

               // As in the old ItEqSol, we only consider vectors with significant overlap
               if (  overlap_max < this->mOverlapMin
                  )
               {
                  break;
               }

               //Update permutation vector, so that perm[n] is now the index of the
               //largest remaining element.
               std::swap(perm[n], perm[max]);

               //Update the rest of the variables:
               overlap_sum += overlap_eigvec[i][perm[n]];
               indices.insert(perm[n]);
               n++;
            }
            //Update the overlapsum from last iteration for the current target:
            mOverlapSumLastIter[i] = overlap_sum;
            mOverlapNLastIter[i]   = n;
         }

         //Now the std::set<In> indices holds the indices of the eigenvectors whose
         //residuals should be used in the next iteration of the solver.
         this->self().Neq() = indices.size();

         //Bring those eigenvectors to "front" of the eigenvector matrix, sort them
         //according to eigenvalue.
         In col = I_0;
         for(auto it = indices.begin(); it != indices.end(); ++it)
         {
            //Swap columns number 'col' and '*it', as well as eigenvalues:
            arEigVecs.SwapCols(col, *it);
            std::swap(arEigVals[col], arEigVals[*it]);
            ++col;
         }

         SortSolution(this->self().Eigenvalues(), this->self().ReducedEigenvectors(), indices.size(), false);
      }


      /**
       * Target implementation (complex case)
       **/
      void TargetingImpl
         (  EigenvalueContainer<MidasVector, MidasVector>& arEigVals
         ,  EigenvectorContainer<MidasMatrix, MidasMatrix>& arEigVecs
         )
      {
         size_t n_targets = this->mTargets.size();
         size_t n_trials  = this->self().Trials().size();

         //Update overlap matrix:
         In old_cols = mOverlaps.Ncols();
         In new_cols = n_trials;
         mOverlaps.SetNewSize(n_targets, new_cols);
         for(In j=old_cols; j<n_trials; ++j)
         {
            for(In i=I_0; i<n_targets; ++i)
            {
               mOverlaps[i][j] = IES_Dot(mTargets[i], this->self().Trials()[j]);
            }
         }


         //Matrix of overlaps between current eigenvectors and targets:
         MidasMatrix overlap_eigvec = mOverlaps * arEigVecs.Rhs();
         //^Note: Only looks at the right-hand-side here. For real eigenpairs the
         //eigenvector matrix has one column corresponding to that eigenvector.
         //For complex eigenpairs the eigenvector matrix has two columns; first is
         //the real part, second is the imag. part.
         //The eigenvalue container holds two vectors, one for real/imag. parts.
         //If imag. part equals zero, it only takes up one element; if it's non-zero
         //the eigenvalue vector has two consecutive elements of the form
         //(Re val, Im val), (Re val, -Im val), i.e. the conjugate pair corresponding
         //to Re vec + Im vec, and Re vec - Im vec. 

         //Now we loop over the target vectors to find eigenvectors whose overlap-squared
         //accumulate to above desired value.
         //A std::set holds the indices of the eigenvectors that fulfil the requirement.
         std::set<In> hits;

         for(In i=0; i<overlap_eigvec.Nrows(); ++i)
         {
            auto n_hits_before = hits.size();

            //Actually, we look at the (modulus) square values:
            for(In j=0; j<overlap_eigvec.Ncols(); ++j)
            {
               overlap_eigvec[i][j] *= overlap_eigvec[i][j];
                  //^Note: In case of complex eigenvectors the first column for that eig.vec.
                  //is Re^2, while the second column is Im^2. The modulus squared of the
                  //overlap is then the sum of the two elements. The overlap is the same
                  //for both the eig.vec. and it's complex conjugate, since the target
                  //vector is real.
            }
            
            //A permutation vector used when finding the largest overlaps.
            //It's first n entries hold the indices of the elements with largest
            //overlaps, in decreasing order (at the end of the iteration, that is).
            std::vector<int> perm(overlap_eigvec.Ncols());
            for(In k=I_0; k<perm.size(); ++k)
            {
               perm[k] = k;
            }

            //Consecutively find largest element (of remaining) until overlap sum
            //reaches the goal:
            Nb overlap_sum = C_0;
            In n = I_0;
            In n_max = std::min(mOverlapNMax, overlap_eigvec.Ncols());
            In n_imag = I_0;

            while (  overlap_sum < mOverlapSumMin
                  && n < n_max
                  )
            {
               //Locate maximal element among the remaining ones in the row:
               In max = n;
               bool is_complex_max = false;
               Nb overlap_max = C_0;
               for(In j = max; j<perm.size(); ++j)
               {
                  //Account for complex eigenvalue/vector:
                  bool is_complex_j = (arEigVals.Im()[perm[j]] != C_0);
                  Nb overlap_j   =  is_complex_j
                                 ?  C_2*(overlap_eigvec[i][perm[j]] + overlap_eigvec[i][perm[j+I_1]])    //|o|^2 = Re^2 +Im^2 for both complex eigvecs (hence the factor 2)
                                 :  overlap_eigvec[i][perm[j]];                                          //|o|^2 = Re^2

                  //Check overlaps/update:
                  if (  overlap_max < overlap_j
                     )
                  {
                     max = j;
                     is_complex_max = is_complex_j;
                     overlap_max    = overlap_j;
                  }

                  if(is_complex_j)
                     ++j;
               }

               // As in the old ItEqSol, we only consider vectors with significant overlap
               // unless we have not included any vectors yet for the current target
               if (  overlap_max < this->mOverlapMin
                  && n > I_0
                  )
               {
                  Mout  << " Vector with overlap: " << overlap_max << " < " << this->mOverlapMin << " will not be included for target " << i << "." << std::endl;
                  break;
               }

               //Having found the maximum overlap, update the overlap_sum:
               overlap_sum += overlap_max;

               //Then update permutation vector, set of indices, n (number of indices added to set):
               std::swap(perm[n], perm[max]);
               hits.insert(perm[n]);
               ++n;

               //If this eig.val./vec. was a complex conjugate pair, also need the imaginary
               //contribution. Just repeat the same statements - the increment of n above means
               //that we're now dealing with the imag. part.
               if (  is_complex_max
                  )
               {
                  std::swap(perm[n], perm[max + I_1]);
                  hits.insert(perm[n]);
                  ++n;
                  ++n_imag;
               }
            }

            //Update the overlapsum from last iteration for the current target:
            mOverlapSumLastIter[i] = overlap_sum;
            mOverlapNLastIter[i]   = n;
            mOverlapNImagLastIter[i] = n_imag;

            if (  hits.size() == n_hits_before
               )
            {
               Mout  << " No new vectors added for target " << i << std::endl;;
            }
         }

         //Now the std::set<In> hits holds the indices of the eigenvectors whose
         //residuals should be used in the next iteration of the solver.
         this->self().Neq() = hits.size();

         // Calculate importance measures for all hits (sum of squared overlaps with all targets)
         std::vector<Nb> hit_importance(hits.size());
         size_t count = 0;
         bool is_real_part = true;
         for(const auto& hit : hits)
         {
            bool is_complex_hit = arEigVals.Im()[hit] != C_0;
            for(size_t i=0; i<this->mTargets.size(); ++i)
            {
               // Take both real and imag parts into account
               if (  is_complex_hit
                  )
               {
                  hit_importance[count] += overlap_eigvec[i][hit];

                  // If we are looking at the real part of the sol vec, the imag part is the next.
                  // Otherwise, we need to take the previous vector into account.
                  hit_importance[count]   += is_real_part
                                          ?  overlap_eigvec[i][hit+1]
                                          :  overlap_eigvec[i][hit-1];
               }
               else
               {
                  hit_importance[count] += overlap_eigvec[i][hit];
               }
            }
            if (  is_complex_hit
               )
            {
               is_real_part = !is_real_part;
            }
            ++count;
         }

         // Save the importance measures
         this->mPreviousHits.clear();
         this->mPreviousHitImportances = hit_importance;
         this->mPreviousHitEigvals.clear();
         for(const auto& hit : hits)
         {
            this->mPreviousHits.emplace_back(hit);
            this->mPreviousHitEigvals.emplace_back(arEigVals.Re()[hit]);
         }


         // Bring all hits to the "front" of the eigenvector matrix
         In col = I_0;
         for(auto it = hits.begin(); it != hits.end(); ++it)
         {
            //Swap columns number 'col' and '*it', as well as eigenvalues:
            arEigVecs.Rhs().SwapCols(col, *it);
            arEigVecs.Lhs().SwapCols(col, *it);
            std::swap(arEigVals.Re()[col], arEigVals.Re()[*it]);
            std::swap(arEigVals.Im()[col], arEigVals.Im()[*it]);

            ++col;
         }

         // Sort the hits according to eigenvalue
         this->SortSolution(arEigVals, arEigVecs, hits.size(), false);
      }

      //! Output stuff
      void OutputTargetingStatus
         (
         )  const
      {
         bool imag_added = std::count(mOverlapNImagLastIter.begin(), mOverlapNImagLastIter.end(), I_0) != mOverlapNImagLastIter.size();

         //Some information from the targeting procedure:
         std::ios_base::fmtflags old_flags = Mout.flags();
         In w0 = I_5;
         In w1 = I_10;
         In w2 = Mout.precision() + I_9;
         Mout << " The targeting procedure found, for target_i, the following\n"
              << " overlap sums and numbers of overlapping eigenvectors:\n";
         Mout << std::right;
         Mout << std::setw(w0) << "i"           << ' ';
         Mout << std::setw(w1) << "n_overlap"   << ' ';
         if (  imag_added
            )
         {
            Mout << std::setw(w1) << "n_imag"   << ' ';
         }
         Mout << std::setw(w2) << "overlap_sum" << ' ';
         Mout << std::endl;
         Mout << std::right;
         for(In i=I_0; i<mOverlapSumLastIter.size(); ++i)
         {
            Mout << std::setw(w0) << i                       << ' ';
            Mout << std::setw(w1) << mOverlapNLastIter[i]    << ' ';
            if (  imag_added
               )
            {
               Mout << std::setw(w1) << mOverlapNImagLastIter[i]    << ' ';
            }
            Mout << std::setw(w2) << mOverlapSumLastIter[i]  << ' ';
            Mout << std::endl;
         }
         Mout << std::endl;

         // Output info on hit importance
         if (  !this->mPreviousHits.empty()
            && this->self().IoLevel() > I_7
            )
         {
            Mout  << " The accumulated importance of reduced solution vector i for all targets\n"
                  << " as well as the reduced eigenvalue:\n"
                  << std::endl;
            Mout  << std::right;
            Mout  << std::setw(w0) << "i"          << ' ';
            Mout  << std::setw(w2) << "importance" << ' ';
            Mout  << std::setw(w2) << "eigenvalue" << ' ';
               Mout  << std::endl;
            Mout  << std::right;
            for(size_t i=0; i<mPreviousHits.size(); ++i)
            {
               Mout  << std::setw(w0) << this->mPreviousHits[i] << ' ';
               Mout  << std::setw(w2) << this->mPreviousHitImportances[i] << ' ';
               Mout  << std::setw(w2) << this->mPreviousHitEigvals[i] << ' ';
               Mout  << std::endl;
            }
            Mout  << std::endl;
         }

         Mout.flags(old_flags);
      }

   public:
      //! Initialize members and perform checks
      void Initialize()
      {
         //If the number of targets is greater than the dimension of the full space,
         //discard the redundant targets:
         if (  mTargets.size() > this->self().Atrans().Dim()
            )
         {
            mTargets.resize(this->self().Atrans().Dim());
            MidasWarning("Target space greater than full space. Discarded redundant targets.");
         }
         
         //Assert that targets are of right dimension for the problem at hand:
         for(In i=I_0; i<mTargets.size(); ++i)
         {
            MidasAssert(mTargets[i].TotalSize() == this->self().Atrans().Dim(), 
                  "Wrong dimension of target " 
                  + std::to_string(i) 
                  + " (dim is "
                  + std::to_string(mTargets[i].TotalSize())
                  + ", should be " 
                  + std::to_string(this->self().Atrans().Dim()) 
                  + ")");
         }

         //Resize the OverlapSumLastIter and the OverlapNLastIter to the number of targets:
         mOverlapSumLastIter.resize(mTargets.size(), C_0);
         mOverlapNLastIter.resize(mTargets.size(), I_0);
         mOverlapNImagLastIter.resize(mTargets.size(), I_0);
      }

      //! Reset overlaps
      void ResetTargeter
         (
         )
      {
         this->mOverlaps.SetNewSize(0,0);
      }

      //! Targeting interface
      template
         <  class VALS_T
         ,  class VECS_T
         >
      void TargetAndSortSolution
         (  VALS_T& arEigVals
         ,  VECS_T& arEigVecs
         )
      {
         // Update target space in first iteration if we are using pre-diagonalization.
         // NB: This could also be done in each iteration (root homing), but that requires 
         // us to be able to determine unique best hits in each iteration.
         if (  this->mPreDiagonalization
            )
         {
            // Sort reduced solution first
            this->SortSolution(this->self().Eigenvalues(), this->self().ReducedEigenvectors(), -I_1, false);

            // Construct solution vector
            this->self().ConstructSolution();

            // Sanity check
            assert(this->self().Solution().size() == this->mTargets.size());

            // Replace targets
            this->mTargets = this->self().Solution();

            // Write new targets to disc
            this->self().WriteTargetVectors();

            if (  gDebug
               )
            {
               Mout  << " Replacing initial targets by solution vector..." << std::endl;
               Mout  << " New targets:\n" << std::endl;
               size_t count = 0;
               for(const auto& target : this->mTargets)
               {
                  Mout  << " Target " << count << ":\n" << target << std::endl;

                  ++count;
               }
            }

            // Only do this in the first iteration
            this->mPreDiagonalization = false;
         }

         // Do targeting
         this->TargetingImpl(arEigVals, arEigVecs);

         // Output some info
         this->OutputTargetingStatus();
      }
};

#endif /* TARGETER_H_INCLUDED */
