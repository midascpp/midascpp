#ifndef REDUCEDEQUATIONBASE_H_INCLUDED
#define REDUCEDEQUATIONBASE_H_INCLUDED

#include "lapack_interface/ILapack.h"
#include "lapack_interface/LapackInterface.h"
#include "it_solver/IES.h"
#include "AddToReducedMatrixable.h"

/**
 * base class for reduced equations
 ***/
template<TransformerType TT, class EQTYPE>
class ReducedEquation: public AddToReducedMatrixable<TT,EQTYPE>
{
};

#endif /* REDUCEDEQUATIONBASE_H_INCLUDED */
