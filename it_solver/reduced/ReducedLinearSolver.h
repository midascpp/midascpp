#ifndef REDUCEDLINEARSOLVER_H_INCLUDED
#define REDUCEDLINEARSOLVER_H_INCLUDED

#include "TransformImplementation.h"
#include "MatrixTag.h"

template
   <  class REDUCED
   ,  class A
   >
class ReducedLinearSolver
   :  public A
   ,  private REDUCED
   ,  protected detail::TransformImplementation
{
   private:
      MAKE_VARIABLE(bool, MinimumResidual, false);

   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void DoReducedSolution()
      {
         //Mout << " Doing reduced linear solutions " << std::endl;
         this->self().DoTransforms();

         if (  this->mMinimumResidual
            )
         {
            // Use matrices A_k = U^T A^T A U and Y_k = U^T A^T Y
            REDUCED::AddToReducedMatrix(this->self().Sigma(),this->self().Sigma(),matrix_tag::MatA);
            REDUCED::AddToReducedMatrix(this->self().Sigma(),this->self().Y(),matrix_tag::MatY);
         }
         else
         {
            // Use matrices A_k = U^T A U and Y_k = U^T Y
            REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Sigma(),matrix_tag::MatA);
            REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Y(),matrix_tag::MatY);
         }
         std::swap(this->self().OldReducedSolution(),this->self().ReducedSolution()); // save reduced solution

         REDUCED::Evaluate(this->self().ReducedSolution());
      }

      void ClearSolver()
      {
         REDUCED::ClearReducedSolution();
         A::ClearSolver();
      }
};

/**
 *
 **/
template
   <  class REDUCED
   ,  class A
   >
class ReducedFreqShiftedLinearSolver
   :  public A
   ,  private REDUCED
   ,  protected detail::TransformImplementation
{
   private:
      MAKE_VARIABLE(bool, MinimumResidual, false);

   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void DoReducedSolution()
      {
         this->self().DoTransforms();

         // A matrix is used in both cases
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Sigma(),matrix_tag::MatA);

         if (  this->mMinimumResidual
            )
         {
            // For minres, we need A^T A and A^T Y
            REDUCED::AddToReducedMatrix(this->self().Sigma(), matrix_tag::MatAtA);
            REDUCED::AddToReducedMatrix(this->self().Sigma(), this->self().Y(), matrix_tag::MatY);
         }
         else
         {
            // Else, we need Y
            REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Y(),matrix_tag::MatY);
         }
         std::swap(this->self().OldReducedSolution(),this->self().ReducedSolution()); // save reduced solution

         REDUCED::Evaluate(this->self().ReducedSolution(),this->self().Atrans());
      }
      
      void ClearSolver()
      {
         REDUCED::ClearReducedSolution();
         A::ClearSolver();
      }
};

/**
 *
 **/
template
   <  class REDUCED
   ,  class A
   >
class NonOrthoReducedFreqShiftedLinearSolver
   :  public A
   ,  private REDUCED
   ,  protected detail::TransformImplementation
{
   private:
      MAKE_VARIABLE(bool, MinimumResidual, false);

   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void DoReducedSolution()
      {
         this->self().DoTransforms();

         // matrices used in both cases
         REDUCED::AddToReducedMatrix(this->self().Trials(), matrix_tag::MatS);
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Sigma(),matrix_tag::MatA);

         if (  this->mMinimumResidual
            )
         {
            // If minres, we ned A^T A and A^T Y
            REDUCED::AddToReducedMatrix(this->self().Sigma(),matrix_tag::MatAtA);
            REDUCED::AddToReducedMatrix(this->self().Sigma(),this->self().Y(),matrix_tag::MatY);
         }
         else
         {
            // else we need Y
            REDUCED::AddToReducedMatrix(this->self().Trials(), this->self().Y(), matrix_tag::MatY);
         }
         std::swap(this->self().OldReducedSolution(),this->self().ReducedSolution()); // save reduced solution

         REDUCED::Evaluate(this->self().ReducedSolution(),this->self().Atrans());
      }
      
      void ClearSolver()
      {
         REDUCED::ClearReducedSolution();
         A::ClearSolver();
      }
};

#endif /* REDUCEDLINEARSOLVER_H_INCLUDED */
