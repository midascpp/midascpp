#ifndef REDUCEDEIGENVALUEEQUAITION_H_INCLUDED
#define REDUCEDEIGENVALUEEQUAITION_H_INCLUDED

#include "ReducedEquationBase.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/LapackInterface.h"
#include "DEBUG.h"
#include "AddToReducedMatrixable.h"
#include "Eigensolvable.h"
#include "MatrixTag.h"

/**
 * Reduced eigevalue equation (REE)
 * AX = eX
 **/
template<class A
       , TransformerType TT
       , class REDUCED = ReducedEquation<TT
                                       , Eigensolvable<TT,PROBLEM_TYPE::PROBLEM_TYPE_STD> 
                                       > 
       >
class REE_Impl: public REDUCED
{

   private:
      A mA;
  
   public:
      explicit REE_Impl(): REDUCED(), mA()
      {
      }
      
      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         REDUCED::AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      /**
       * Evaluate interface
       **/
      template<class T, class U>
      void Evaluate(T& aReducedEigVecs, U& aReducedEigVals)
      {
         REDUCED::EigenSolution(mA,aReducedEigVecs,aReducedEigVals,ilapack::iDGEEV);
         DEBUG_CheckEigenSolution(mA,aReducedEigVecs,aReducedEigVals);
      }

      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         mA.SetNewSize(I_0,I_0);
      }

      /**
       * Get matrix
       **/
      const A& GetAMatrix() const
      {
         return mA;
      }
};

// interface
template<class A>
using ReducedEigenvalueEquation = REE_Impl<A,TransformerType::TRANS_NHERM>;

template<class A>
using ReducedHermitianEigenvalueEquation = REE_Impl<A,TransformerType::TRANS_HERM>;

#endif /* REDUCEDEIGENVALUEEQUAITION_H_INCLUDED */
