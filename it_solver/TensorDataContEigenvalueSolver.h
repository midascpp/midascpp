/**
************************************************************************
* 
* @file    TensorDataContEigenvalueSolver.h
*
* @date    13-7-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Eigenvalue solver from TensorDataCont calculations.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef TENSORDATACONTEIGENVALUESOLVER_H_INCLUDED
#define TENSORDATACONTEIGENVALUESOLVER_H_INCLUDED

#include "vcc/TensorDataCont.h"

#include "reduced/Interface.h"
#include "precon/Interface_decl.h"
#include "expand/Interface.h"
#include "transformer/MidasTensorDataContTransformer.h"
#include "start/EigenvalueStartGuesser.h"
#include "residual/ResidualMaker.h"
#include "conv/ConvergenceChecker.h"
#include "IterativeEquationSolver.h"
#include "EigenvalueEquation.h"
#include "finalize/TensorDataContFinalizer.h"

using TensorDataContEigenvalueSolver = 
IterativeEquationSolver
   <  EigenvalueEquation
         <  TensorDataCont    // SOL_T
         ,  TensorDataCont    // TRIAL_T
         ,  TensorDataCont    // SIGMA_T
         ,  TensorDataCont    // RESID_T
         ,  MidasTensorDataContTransformer
         ,  DiagonalEigenvalueStartGuesser                        // NB: Constructing energy differences in full dimension...
         ,  MidasTensorDiagonalEigenvaluePreconditioner
         ,  ResidualMaker                                         // Maybe make separate class. Recompression in complex case needs attention!
         ,  ImagEigenvalueConvergenceChecker
         ,  TensorDataContSpaceExpander
         ,  NonOrthoReducedSolver
         ,  TensorDataContFinalizer
         >
   >;

using TensorDataContTargetingEigenvalueSolver = 
IterativeEquationSolver
   <  EigenvalueEquation
         <  TensorDataCont    // SOL_T
         ,  TensorDataCont    // TRIAL_T
         ,  TensorDataCont    // SIGMA_T
         ,  TensorDataCont    // RESID_T
         ,  MidasTensorDataContTransformer
         ,  TargetingStartGuesser
         ,  MidasTensorDiagonalEigenvaluePreconditioner
         ,  ResidualMaker                                         // Maybe make separate class. Recompression in complex case needs attention!
         ,  ImagEigenvalueConvergenceChecker
         ,  TensorDataContSpaceExpander
         ,  TensorNonOrthoReducedTargetingSolver
         ,  TensorDataContFinalizer
         >
   >;

#include "precon/Interface_impl.h"

#endif /* TENSORDATACONTEIGENVALUESOLVER_H_INCLUDED */
