#ifndef FAKETRANSFORMER_H_INCLUDED
#define FAKETRANSFORMER_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "test/RandomMatrix.h"

class FakeTransformer
{
   private:
      const MidasMatrix mMat;
      const MidasVector mDiag;
   
   public:
      explicit FakeTransformer(In aI): mMat(midas::test::UnitMatrix(aI)), mDiag(mMat.GetDiag())
      {
      }

      explicit FakeTransformer(const MidasMatrix& mat): mMat(mat), mDiag(mMat.GetDiag())
      {
      }
      
      explicit FakeTransformer(MidasMatrix&& mat): mMat(std::move(mat)), mDiag(mMat.GetDiag())
      {
      }

      void Transform(DataCont& in, DataCont& out, In i, In j, Nb& energy) const
      {
         if(i == I_0)
         {
            // diagonal
            out = in;
            out.HadamardProduct(mDiag,C_1,DataCont::Product::Normal);
         }
         else
         {
            auto out_vec = out.GetVector();
            auto in_vec  =  in.GetVector();
            
            (*out_vec) = mMat*(*in_vec);
         }
      }

      size_t VectorDimension() const
      {
         return mMat.Ncols();
      }

      const MidasMatrix& GetMatrix() const
      {
         return mMat;
      }
      
      MidasMatrix Clone() const
      {
         MIDASERROR("Not implemented");
         return mMat;
      }

      MidasMatrix CloneImprovedPrecon(In aLevel) const
      {
         MIDASERROR("Not implemented");
         return mMat;
      }
};

#endif /* FAKETRANSFORMER_H_INCLUDED */
