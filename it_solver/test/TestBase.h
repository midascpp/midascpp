#ifndef ITSOLVER_TESTBASE_H_INCLUDED
#define ITSOLVER_TESTBASE_H_INCLUDED

#include "../IES_Macros.h"

namespace midas
{
namespace test
{

template<class A>
class MixinSelf
{
   public:
      A& self()
      {
         return static_cast<A&>(*this);
      }
      
      const A& self() const
      {
         return static_cast<const A&>(*this);
      }

      void Initialize()
      {
      }
};

template<class TRANS, class A>
class TransformerHolder: public A
{
   MAKE_VARIABLE(TRANS,Atrans);
   public:
      template<class... Ts>
      TransformerHolder(TRANS&& trans, Ts&&... ts): 
         mAtrans(std::forward<TRANS>(trans))
       , A(std::forward<Ts>(ts)...)
      {
      }
};

template<class RES, class A>
class ResidualHolder: public A
{
   MAKE_VARIABLE(RES,Residuals)
   public:
      template<class... Ts>
      ResidualHolder(RES&& res, Ts&&... ts): 
         mResiduals(std::forward<RES>(res))
       , A(std::forward<Ts>(ts)...)
      {
      }
};


} /* namespace test */
} /* namespace midas */

#endif /* ITSOLVER_TESTBASE_H_INCLUDED */
