/**
************************************************************************
* 
* @file                PreconTest.h
*
* Created:             30-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Test case for turning strings into numbers and vectors
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ITSOLVER_PRECONTEST_H_INCLUDED
#define ITSOLVER_PRECONTEST_H_INCLUDED

#include "libmda/testing/testing_interface.h"

#include "TestBase.h"
#include "FakeTransformer.h"

#include "test/ConstructDummy.h"
#include "test/RandomVector.h"
#include "test/RandomDataCont.h"
#include "test/RandomMatrix.h"

#include "it_solver/precon/ComplexFreqShiftedPreconditioner_decl.h"
#include "it_solver/precon/ComplexFreqShiftedPreconditioner_impl.h"
#include "it_solver/transformer/MidasComplexFreqShiftedTransformer.h"

#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "mmv/Transformer.h"

namespace midas
{
namespace test
{

template<class TRANS, class RES>
class FakeComplexPrecon: public TransformerHolder<TRANS,
                                ResidualHolder<RES,
                                ComplexFreqShiftedPreconditioner<
                                   MixinSelf<FakeComplexPrecon<TRANS,RES> >
                                > > >
{
   using interface = TransformerHolder<TRANS,
                     ResidualHolder<RES,
                     ComplexFreqShiftedPreconditioner<
                     MixinSelf<FakeComplexPrecon<TRANS,RES> >
                     > > >;
   public:
      template<class... Ts>
      FakeComplexPrecon(Ts&&... ts): interface(std::forward<Ts>(ts)...)
      {
      }

      std::vector<bool> Converged()
      {
         MIDASERROR("not implemented");
      }
};

template<class TRANS, class RES>
FakeComplexPrecon<TRANS,RES> MakeFakeComplexPrecon(TRANS&& trans, RES&& res)
{
   return FakeComplexPrecon<TRANS,RES>(std::forward<TRANS>(trans),std::forward<RES>(res));
}
                                          

template<class TRANS, class REFRQ, class IMFRQ>
MidasComplexFreqShiftedTransformerImpl<TRANS,REFRQ,IMFRQ> MakeComplexFreqShiftedTransformer(TRANS&& trans
                                                                                          , REFRQ&& refrq
                                                                                          , IMFRQ&& imfrq
                                                                                          )
{
   return MidasComplexFreqShiftedTransformerImpl<TRANS,REFRQ,IMFRQ>(std::forward<TRANS>(trans)
                                                                  , std::forward<REFRQ>(refrq)
                                                                  , std::forward<IMFRQ>(imfrq)
                                                                  );
}

struct ComplexPreconTest: public virtual unit_test
{
   template<class PRECON, class MAT, class RES>
   bool assert_precon(PRECON&& precon, MAT&& mat, RES&& res) const
   {
      size_t prec = 40;

      for(int i=0; i<res.size(); ++i)
      {  
         Mout << " RES " << i << std::endl;
         auto& re_vec = *(res[i].Re().GetVector());
         auto& im_vec = *(res[i].Im().GetVector());
         MidasVector re_result(res[i].Re().Size());
         MidasVector im_result(res[i].Re().Size());
         
         for(int j=0; j<res[i].Re().Size(); ++j)
         {
            Mout << " RES " << i << " " << j << std::endl;
            Nb divisor = (mat[j][j] - precon.Atrans().ReFrequency(i))*(mat[j][j]-precon.Atrans().ReFrequency(i)) 
                       + precon.Atrans().ImFrequency(i)*precon.Atrans().ImFrequency(i);
            
            re_result[j] = ( (mat[j][j]-precon.Atrans().ReFrequency(i))*re_vec[j] + precon.Atrans().ImFrequency(i)*im_vec[j] )/divisor;
            im_result[j] = ( precon.Atrans().ImFrequency(i)*re_vec[j] - (mat[j][j]-precon.Atrans().ReFrequency(i))*im_vec[j] )/divisor;
            
            Nb re_test;
            precon.Residuals()[i].Re().DataIo(IO_GET,j,re_test);
            Nb im_test;
            precon.Residuals()[i].Im().DataIo(IO_GET,j,im_test);
            
            Mout << std::setprecision(20) << std::scientific;
            Mout << re_result[j] << " " << re_test << std::endl;
            Mout << im_result[j] << " " << im_test << std::endl;

            if(!libmda::numeric::float_eq(re_result[j],re_test,prec) 
            || !libmda::numeric::float_eq(im_result[j],im_test,prec))
            {
               return false;
            }
         }
      }

      return true;   
   }
   
   void do_test() throw(test_failed)
   {
      int size = 100;
      std::vector<ComplexVector<DataCont,DataCont> > res { ComplexVector<DataCont,DataCont>(RandomDataCont<Nb>(size),RandomDataCont<Nb>(size)) 
                                                         , ComplexVector<DataCont,DataCont>(RandomDataCont<Nb>(size),RandomDataCont<Nb>(size)) 
                                                         };
      auto res2 = res;
      auto fake_trans = FakeTransformer(size);
      auto fake_precon = MakeFakeComplexPrecon(MakeComplexFreqShiftedTransformer(fake_trans
                                                                               , RandomVector<Nb>(2)
                                                                               , RandomVector<Nb>(2)
                                                                               )
                                             , res
                                             );
      fake_precon.Initialize();
      fake_precon.Precondition();
         
      //UNIT_ASSERT(assert_precon(fake_precon,fake_trans.GetMatrix(),res2),"FAILED!");
      bool assertion = assert_precon(fake_precon,fake_trans.GetMatrix(),res2);
      UNIT_ASSERT(assertion,"FAILED!");
   }
};

} /* namespace test */
} /* namespace midas */

#endif /* ITSOLVER_PRECONTEST_H_INCLUDED */
