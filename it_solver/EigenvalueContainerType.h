#ifndef EIGENVALUECONTAINERTYPE_H_INCLUDED
#define EIGENVALUECONTAINERTYPE_H_INCLUDED

#include "EigenvalueContainer.h"
#include "it_solver/reduced/ReducedEigenvalueEquation.h"
#include "it_solver/reduced/ReducedGeneralEigenvalueEquation.h"

/**
 *
 **/
template<class REE>
struct EigenvalueContainerType_Impl;

template<class M>
struct EigenvalueContainerType_Impl<ReducedEigenvalueEquation<M> >
{
   using type = EigenvalueContainer<MidasVector,MidasVector>;
};

template<class M>
struct EigenvalueContainerType_Impl<ReducedHermitianEigenvalueEquation<M> >
{
   using type = MidasVector;
};

template<class M>
struct EigenvalueContainerType_Impl<ReducedGeneralEigenvalueEquation<M,M> >
{
   using type = EigenvalueContainer<MidasVector,MidasVector>;
};

template<class M>
struct EigenvalueContainerType_Impl<ReducedGeneralHermitianEigenvalueEquation<M,M> >
{
   using type = MidasVector;
};

template<class REE>
using EigenvalueContainerType = typename EigenvalueContainerType_Impl<REE>::type;

#endif /* EIGENVALUECONTAINERTYPE_H_INCLUDED */
