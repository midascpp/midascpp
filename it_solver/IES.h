#ifndef IES_H_INCLUDED
#define IES_H_INCLUDED

namespace ies
{
namespace detail
{

template<class T>
struct type_sink: std::true_type
{
};

template<class... Ts>
struct pack
{
};

} /* namespace detail */
} /* namespace ies */

/**
 *
 **/
#define STRINGYFY(STR) #STR
#define CREATE_INTERFACE(NAME) \
template<class, class=void> \
struct IES_##NAME##_check: std::false_type \
{ \
}; \
 \
template<class... Us> \
struct IES_##NAME##_check<ies::detail::pack<Us...> \
                        , typename std::enable_if<ies::detail::type_sink<decltype(NAME(std::declval<Us>()...))>::value>::type \
                        >: std::true_type \
{ \
}; \
\
template<class... Us> \
struct IES_##NAME##_type \
{ \
   static const bool value = IES_##NAME##_check<ies::detail::pack<Us...> >::value; \
   \
   static_assert(value,"No " STRINGYFY(NAME) " function supplied!"); \
   using type = decltype(NAME(std::declval<Us>()...)); \
}; \
 \
template<class... Ts> \
inline auto IES_##NAME (Ts&&... ts) \
   -> typename IES_##NAME##_type<Ts...>::type \
{ \
   return NAME(std::forward<Ts>(ts)...); \
} 

CREATE_INTERFACE(Norm)
CREATE_INTERFACE(Norm2)
CREATE_INTERFACE(Normalize)
CREATE_INTERFACE(Dot)
CREATE_INTERFACE(Orthogonalize)
CREATE_INTERFACE(Scale)
CREATE_INTERFACE(Zero)
CREATE_INTERFACE(Axpy)
#undef CREATE_INTERFACE
#undef STRINGYFY

#endif /* IES_H_INCLUDED */
