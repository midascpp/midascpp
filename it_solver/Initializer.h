#ifndef INITIALIZER_H_INCLUDED
#define INITIALIZER_H_INCLUDED

template<class A>
class NoInititalizer: public A
{
};

template<class A>
class LinearEquationInitializer: public A
{
   public:
      void Initialize()
      {
         this->self().Neq() = this->self().Y().size();

         A::Initialize();
      }
};

template<class A>
class IndividuallyFreqShiftedLinearEquationInitializer: public A
{
   public:
      void Initialize()
      {
         this->self().Neq() = this->self().Y().size() * this->self().Atrans().Frequencies().Size();

         A::Initialize();
      }
};

#endif /*INITIALIZER_H_INCLUDED */
