#ifndef EIGENVALUECONTAINER_H_INCLUDED
#define EIGENVALUECONTAINER_H_INCLUDED

template<class... Vecs>
class EigenvalueContainer;

//template<class Vec>
//class EigenvalueContainer<Vec>: public Vec
//{
//   public:
//      template<class... Ts>
//      EigenvalueContainer(Ts&&... ts): Vec(std::forward<Ts>(ts)...)
//      {
//      }
//};

template<class ReVec, class ImVec>
class EigenvalueContainer<ReVec,ImVec>
{
   private:
      ReVec mRe;
      ImVec mIm;
   public:
      explicit EigenvalueContainer(): mRe(), mIm()
      {
      }

      void SetNewSize(In i)
      {
         mRe.SetNewSize(i);
         mIm.SetNewSize(i);
      }
      
      void Zero()
      {
         mRe.Zero();
         mIm.Zero();
      }

      ReVec&       Re()       {return mRe;}
      const ReVec& Re() const {return mRe;}
      
      ImVec&       Im()       {return mIm;}
      const ImVec& Im() const {return mIm;}
};


#endif /* EIGENVALUECONTAINER_H_INCLUDED */
