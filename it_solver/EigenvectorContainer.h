#ifndef EIGENVECTORCONTAINER_H_INCLUDED
#define EIGENVECTORCONTAINER_H_INCLUDED

/**
 *
 **/
template<class LHS, class RHS>
class EigenvectorContainer
{
   private:
      LHS mLhs;
      RHS mRhs;
   
   public:
      explicit EigenvectorContainer(): mLhs(), mRhs()
      {
      }

      LHS&       Lhs()       {return mLhs;}
      const LHS& Lhs() const {return mLhs;}
      
      RHS&       Rhs()       {return mRhs;}
      const RHS& Rhs() const {return mRhs;}
};


#endif /* EIGENVECTORCONTAINER_H_INCLUDED */
