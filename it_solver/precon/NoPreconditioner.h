#ifndef NOPRECONDITONER_H_INCLUDED
#define NOPRECONDITONER_H_INCLUDED

template<class A>
class NoPreconditioner: public A
{
   public:
      explicit NoPreconditioner()
      {
      }

      template<class... Ts>
      void PreconditionImpl(Ts&&...) const
      {
      }

      void Precondition() const
      {
      }
};

#endif /* NOPRECONDITONER_H_INCLUDED */
