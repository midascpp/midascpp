#ifndef COMPLEXFREQSHIFTEDPRECONDITIONER_IMPL_H_INCLUDED
#define COMPLEXFREQSHIFTEDPRECONDITIONER_IMPL_H_INCLUDED

#include <complex>

#include "ComplexFreqShiftedPreconditioner_decl.h"

#include "../IES.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/Transformer.h"
#include "vcc/TransformerV3.h"
#include "it_solver/ComplexVector.h"
#include "it_solver/transformer/MidasComplexFreqShiftedTransformer.h"
#include "it_solver/transformer/MidasComplexFreqShiftedSubspaceTransformer.h"
#include "it_solver/LinearEquationInterface.h"
#include "it_solver/InitializeIterativeSolverFromCalcDef.h"
#include "it_solver/OrthogonalizeSpace.h"

#include "libmda/numeric/float_eq.h"

/**
 * c-tor
 **/
template<class A>
ComplexFreqShiftedPreconditioner<A>::ComplexFreqShiftedPreconditioner()
{
}

/**
 * init 
 **/
template<class A> 
void ComplexFreqShiftedPreconditioner<A>::Initialize()
{
   //Mout << " INITIALIZING OLD PRECONDITIONER " << std::endl;
   A::Initialize();

   mAdiag.SetNewSize(this->self().Atrans().Dim());
   mBdiag.SetNewSize(this->self().Atrans().Dim());
   
   DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem");
   mBdiag = vec_of_ones; // we have at this point no B-mat, i.e. B = I...
   
   Nb e_dummy = 0.0; // dummy input for Transform
   this->self().Atrans().Transform(vec_of_ones, mAdiag, I_0, I_1, e_dummy);
   mAdiag.Scale(-C_1); // hmm gets -diag above... (so we need to change sign...)
  
   //Mout << " A diag: " << mAdiag << std::endl;
   //Mout << " B diag: " << mBdiag << std::endl;

   // do squared
   mAdiagSquared = mAdiag;
   mAdiagSquared.HadamardProduct(mAdiag,C_1,DataCont::Product::Normal);
   mBdiagSquared = mBdiag;
   mBdiagSquared.HadamardProduct(mBdiag,C_1,DataCont::Product::Normal);
   mAdiagBdiag = mAdiag;
   mAdiagBdiag.HadamardProduct(mBdiag,C_1,DataCont::Product::Normal);

   // do Olsen correction??
   mOlsen = this->self().CalcDef()->RspComplexOlsen();
}

/**
 * precon impl
 **/
template<class A>
void ComplexFreqShiftedPreconditioner<A>::PreconditionImpl(ComplexVector<DataCont,DataCont>& v
                                                         , const std::complex<Nb> frq
                                                         , std::vector<ComplexVector<DataCont,DataCont> >&
                                                         , In i_eq // not currently used here, but part of the interface for PreconditionImpl
                                                         ) const
{
   //Mout << " Begin PRECON! " << std::endl;
   //std::cout << frq << std::endl;
   //std::cout << " BEFORE PRECON \n" << v << std::endl;
   auto v_copy = v;

   auto inv_squared = InverseSquared(frq);
   
   auto a_copy = mAdiag;
   IES_Axpy(a_copy, mBdiag, -frq.real()); // a_copy <- (A_0 - frq_re B_0)
   
   /////
   // real part
   /////
   v.Re().HadamardProduct(a_copy,C_1,DataCont::Product::Normal);        // re <- (A_0 - frq_re B_0) re
   v.Im().HadamardProduct(mBdiag,frq.imag(),DataCont::Product::Normal); // im <- frq_im B_0 im
   IES_Axpy(v.Re(), v.Im(), C_1);          // re <- (A_0 - frq_re B_0) re + frq_im B_0 im
   v.Re().HadamardProduct(inv_squared,C_1,DataCont::Product::Inverse);
   
   /////
   // imag part (using v_copy, and saving result in re)
   /////
   v_copy.Re().HadamardProduct(mBdiag,frq.imag(),DataCont::Product::Normal); // re <- frq_im B_0 re
   v_copy.Im().HadamardProduct(a_copy,C_1,DataCont::Product::Normal);        // im <- (A_0 - frq_re B_0) im
   IES_Axpy(v_copy.Re(),v_copy.Im(),-C_1);    // re <- frq_im B_0 re - (A_0 - frq_re B_0) im
   v_copy.Re().HadamardProduct(inv_squared,C_1,DataCont::Product::Inverse);

   v.Im() = v_copy.Re(); // save result in v
   //Mout << " AFTER PRECON \n" << v << std::endl;
   //exit(42);
}

/**
 *
 **/
template<class A>
void ComplexFreqShiftedPreconditioner<A>::PreconditionOlsenImpl(ComplexVector<DataCont, DataCont>& v
                                                              , const std::complex<Nb> frq
                                                              , const ComplexVector<DataCont, DataCont>& sol
                                                              , In i_eq
                                                              ) const
{
   std::vector<ComplexVector<DataCont,DataCont> > dummy; // dummy argument !!

   ComplexVector<DataCont, DataCont> y { this->self().Y()[i_eq].Re(), this->self().Y()[i_eq].Im() };
   y.Im().Scale(-C_1); // y^im is zero for now, so this has no effect (but it might have at some point)
   ComplexVector<DataCont, DataCont> y_copy(y);
   ComplexVector<DataCont, DataCont> sol_transformed = v; // sol_transformed <- r
   IES_Axpy(sol_transformed.Re(), y.Re(),  C_1); // sol_transformed <- r + y ( = A*x_0 )
   //IES_Axpy(sol_transformer.Im(), y.Im(), -C_1); // commented out as y_im is a ZeroVector
   //
   ////
   //// TRANSFORMED SOLUTION TEST
   ////
   //ComplexVector<DataCont, DataCont> test_transformed_sol(sol_transformed.Re().Size()
   //                                                     , sol_transformed.Im().Size()
   //                                                     );
   //IES_Zero(test_transformed_sol.Re());
   //IES_Zero(test_transformed_sol.Im());

   //auto& sigma = this->self().Sigma();
   //auto& gamma = this->self().Trials();
   //auto& reduced_sol = this->self().ReducedSolution();
   //for(int j = 0; j < sigma.size(); ++j)
   //{
   //   IES_Axpy(test_transformed_sol.Re(), sigma[j],  reduced_sol[i_eq].Re()[j]);
   //   IES_Axpy(test_transformed_sol.Re(), gamma[j], -frq.real()*reduced_sol[i_eq].Re()[j]);
   //   IES_Axpy(test_transformed_sol.Re(), gamma[j],  frq.imag()*reduced_sol[i_eq].Im()[j]);

   //   IES_Axpy(test_transformed_sol.Im(), gamma[j],  frq.imag()*reduced_sol[i_eq].Re()[j]);
   //   IES_Axpy(test_transformed_sol.Im(), sigma[j], -reduced_sol[i_eq].Im()[j]);
   //   IES_Axpy(test_transformed_sol.Im(), gamma[j],  frq.real()*reduced_sol[i_eq].Im()[j]);
   //}
   ////Mout << " DIFFERENCENORM RE: " << DifferenceNorm(test_transformed_sol.Re(), sol_transformed.Re()) << std::endl;
   ////Mout << " DIFFERENCENORM IM: " << DifferenceNorm(test_transformed_sol.Im(), sol_transformed.Im()) << std::endl;

   ////
   //// TRANSFORMED SOLUTIOB TEST END
   ////

   // Make "precondtioned transformed solution": s^P = A_0^-1*A*x_0
   PreconditionImpl(sol_transformed, frq, dummy, i_eq); // sol_transformed <- A_0^-1*A*x_0
   // Make "preconditioned rhs": y^P = M^-1*y 
   PreconditionImpl(y, frq, dummy, i_eq); // y <- A_0^-1*y

   // Make dots s^T*r^P and s^T*s^P
   auto dot_s_r = IES_Dot(sol, sol_transformed); // dot_s_r <- x_0^T * s^P ( = x_0^T * A_0^-1 * A * x_0 )
   auto dot_s_s = IES_Dot(sol, y); // dot_s_s <- x_0^T * y^P ( = x_0^T A_0^-1 * y )
   auto factor = dot_s_r / dot_s_s;
   // Make actual Olsen correction
   IES_Scale(y_copy,factor); // y_copy <- (<x_0^T, s^P> / <x_0^T, y^P>) * y
   PreconditionImpl(y_copy, frq, dummy, i_eq);
   
   //Mout << " Y_COPY RE:\n" << y_copy.Re() << std::endl;
   //Mout << " Y_COPY IM:\n" << y_copy.Im() << std::endl;

   IES_Axpy(sol_transformed.Re(), y_copy.Re(), -C_1); /// + ??
   IES_Axpy(sol_transformed.Im(), y_copy.Im(), -C_1);
   
   // add correction to preconditioned residual
   v.Re() = sol_transformed.Re();
   v.Im() = sol_transformed.Im();
}
      
/**
 * precon
 **/
template<class A>
void ComplexFreqShiftedPreconditioner<A>::Precondition()
{
   if(mOlsen)
   {
      this->self().ConstructSolution(); // olsen correction needs current best solution
   }

   for(size_t i = 0; i<this->self().Residuals().size(); ++i)
   {
      if(!this->self().Converged()[i])
      {
         if(mOlsen)
         { // do olsen correction?
            //Mout << " Doing 'Olsen' correction" << std::endl;
            PreconditionOlsenImpl(this->self().Residuals()[i],this->self().Atrans().Frequency(i),this->self().Solution()[i],i);
            //Mout << " DOT AFTER SOLUTION NORM: " << IES_Dot(this->self().Residuals()[i], this->self().Solution()[i])/IES_Norm(this->self().Solution()[i]) << std::endl;
            //Mout << " DOT AFTER SOLUTION NORM: " << IES_Dot(this->self().Residuals()[i], this->self().Solution()[i])/IES_Norm(this->self().Solution()[i]) << std::endl;
            //Mout << " DOT AFTER RE: " << IES_Dot(this->self().Residuals()[i].Re(), this->self().Solution()[i].Re())/IES_Norm(this->self().Residuals()[i]) << std::endl;
            //Mout << " DOT AFTER IM: " << IES_Dot(this->self().Residuals()[i].Im(), this->self().Solution()[i].Im())/IES_Norm(this->self().Residuals()[i]) << std::endl;
            //Mout << " NORM SOLUTION: " << IES_Norm(this->self().Solution()[i]) << std::endl;
            //Mout << " NORM RESIDUAL: " << IES_Norm(this->self().Residuals()[i]) << std::endl;
         }
         else
         { // or just normal diag precon
            PreconditionImpl(this->self().Residuals()[i],this->self().Atrans().Frequency(i),this->self().Residuals(),i);
         }
      }
   }
}

/**
 * Improved c-tor
 **/
template<class A>
ImprovedComplexFreqShiftedPreconditioner<A>::ImprovedComplexFreqShiftedPreconditioner(): mpTransformerA(nullptr)
                                                                                       , mSmallDim(0)
                                                                                       , mDiffDim(0)
{
   //Mout << " constructing precon... " << std::endl;
}

/**
 * d-tor
 **/
template<class A>
ImprovedComplexFreqShiftedPreconditioner<A>::~ImprovedComplexFreqShiftedPreconditioner()
{
   TransformerDestroy(mpTransformerA);
   //TransformerDestroy(mpTransformerB);
}

/**
 *
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::InitializeImprovedPreconditioner()
{
   ///////////////////////////////////////////////////////////////////////////
   // setup A and B diag and diag^2
   ///////////////////////////////////////////////////////////////////////////
   // set size of A and B diag part
   mAdiag.SetNewSize(static_cast<In>(mDiffDim));
   mBdiag.SetNewSize(static_cast<In>(mDiffDim));

   // Get diagonal of A transformer
   DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem","vec_of_ones");
   DataCont diag(this->self().Atrans().Dim(),C_0,"InMem","diag");
   
   Nb e_dummy = 0.0; // dummy input for Transform
   this->self().Atrans().Transform(vec_of_ones,diag,I_0,I_1,e_dummy);
   diag.Scale(-C_1); // hmm gets -diag above... (so we need to change sign...)
   
   // reassign elements to A_diag and B_diag
   mAdiag.Reassign(diag,mSmallDim,I_1);
   mBdiag.Reassign(vec_of_ones,mSmallDim,I_1);

   //Mout << " A diag: " << mAdiag << std::endl;
   //Mout << " B diag: " << mBdiag << std::endl;

   //// do squared
   mAdiagSquared = mAdiag;
   mAdiagSquared.HadamardProduct(mAdiag,C_1,DataCont::Product::Normal);
   mBdiagSquared = mBdiag;
   mBdiagSquared.HadamardProduct(mBdiag,C_1,DataCont::Product::Normal);
   mAdiagBdiag = mAdiag;
   mAdiagBdiag.HadamardProduct(mBdiag,C_1,DataCont::Product::Normal);
}

/**
 *
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::InitializeImprovedPreconditionerExplicit()
{
   //////////////////////////////////////////////////////////////////////////
   // setup explicit A_0
   //////////////////////////////////////////////////////////////////////////
   this->self().Ostream() << " Initialize Improved EXPLICIT preconditioner " << std::endl;
   if(!mpTransformerA->Null()) mpTransformerA->ConstructExplicit(mExplicitAZeroMatrix);
   
   auto large_dim = this->self().Atrans().Dim();
   mExplicitAZeroMatrix.SetNewSize(large_dim,large_dim,true,true); // saveold=true, zero=true
   for(int i = mSmallDim; i < this->self().Atrans().Dim(); ++i)
   {
      mExplicitAZeroMatrix[i][i] = (*mAdiag.GetVector())[i - mSmallDim];
   }

   mExplicitBZeroMatrix.SetNewSize(large_dim,large_dim,false,true); // saveold=false, zero=true
   for(int i = 0; i < large_dim; ++i)
   {
      mExplicitBZeroMatrix[i][i] = 1;
   }
   this->self().Ostream() << " Done Initialize Improved EXPLICIT preconditioner " << std::endl;
}
 
/**
 * init
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::Initialize()
{
   //Mout << " INITIALIZING PRECONDITIONER " << std::endl;
   A::Initialize();

   ///////////////////////////////////////////////////////////////////////////
   // setup transformer levels
   ///////////////////////////////////////////////////////////////////////////
   mLevel = this->CalcDef()->GetRspComplexImprovedPrecondExciLevel();
   mExplicit = this->CalcDef()->GetRspComplexImprovedPrecondExplicit();

   // Initialize small transformer
   mpTransformerA = mLevel ? this->self().Atrans().CloneImprovedPrecon(mLevel) : new NullTransformer();
   
   MidasComplexFreqShiftedTransformer trf_a(*mpTransformerA
                                          , this->self().Atrans().GetReFrq()
                                          , this->self().Atrans().GetImFrq()
                                          );
   
   // get dimensions of problems
   mSmallDim = trf_a.Dim();
   mDiffDim = this->self().Atrans().Dim() - mSmallDim;
   //Mout << " Total dim : " << this->self().Atrans().Dim() << std::endl;
   //Mout << " small dim : " << mSmallDim << std::endl;
   //Mout << " diff dim  : " << mDiffDim << std::endl;
   
   InitializeImprovedPreconditioner();

   if(mExplicit)
   {
      InitializeImprovedPreconditionerExplicit();
   }
   
   // get some statistics
   if(false)
   {
      AnalyseConditionNumber();
   }
}
     
/**
 * precon impl
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::PreconditionImpl(ComplexVector<DataCont,DataCont>& v
                                                                 , const std::complex<Nb> frq
                                                                 , std::vector<ComplexVector<DataCont,DataCont> >& trans_y
                                                                 , In i_eq
                                                                 ) const
{
   //Mout << " PRECONIMPL " << std::endl;
   ///////////////////////////////////////////////////////////////////////////
   // if requested do EXPLICIT PRECOND!
   ///////////////////////////////////////////////////////////////////////////
   if(mExplicit)
   {
      //Mout << " EXPLCIT " << std::endl;
      this->self().Ostream() << " Improved EXPLICIT precon eq. " << i_eq << "!";
      //Mout << " vector before precond : " << std::endl;
      //Mout << v << std::endl;
      
      // make M^-1
      MidasMatrix full;
      ConstructFullMatrix(mExplicitAZeroMatrix,mExplicitBZeroMatrix,frq,full);
      auto determinant = InvertGeneral(full);
      
      // make rhs
      MidasVector v_before(2*v.Re().Size());
      for(int i = 0; i < v.Re().Size(); ++i)
      {
         v_before[i] = (*v.Re().GetVector())[i];
         //v_before[i + v.Re().Size()] = - (*v.Im().GetVector())[i];
         v_before[i + v.Re().Size()] = (*v.Im().GetVector())[i];
      }
      
      // do precon
      MidasVector v_after(2*v.Re().Size());
      v_after = full * v_before;
      
      // load back result
      for(int i = 0; i < v.Re().Size(); ++i)
      {
         (*v.Re().GetVector())[i] =  v_after[i];
         //(*v.Im().GetVector())[i] = -v_after[i + v.Re().Size()];
         (*v.Im().GetVector())[i] = v_after[i + v.Re().Size()];
      }
      
      //Mout << " preconditioned vector " << std::endl;
      //Mout << v << std::endl;

      this->self().Ostream() << "\n";
      return; // return
   }
   
   //Mout << " NOT EXPLICIT " << std::endl;
   ///////////////////////////////////////////////////////////////////////////
   // Precon small system
   ///////////////////////////////////////////////////////////////////////////
   if(mLevel > 0)
   {
      // make right hand side of small system
      std::vector<ComplexVector<DataCont,DataCont> > y(1);
      y[0].Re().SetNewSize(mSmallDim);
      y[0].Im().SetNewSize(mSmallDim);
      y[0].Re().Reassign(v.Re(),0,I_1,C_0,mSmallDim);
      y[0].Im().Reassign(v.Im(),0,I_1,C_0,mSmallDim);
      y[0].Im().Scale(-C_1); // we change sign of imag part as solver treats y^R + i*y^I as (y^R, -y^I)
                             // and we want to solve for (y^R, y^I)
      
      // make transformer
      MidasVector frq_re(1); frq_re[0] = frq.real();
      MidasVector frq_im(1); frq_im[0] = frq.imag();
      MidasComplexFreqShiftedTransformer trf_a(*mpTransformerA
                                             , frq_re
                                             , frq_im
                                             );
      
      // setup iterative solver
      ComplexFreqShiftedLinearEquationSolver2 ies_precon(trf_a, y);
      InitializeLinearSolverFromPreconRspCalcDef(ies_precon,this->self().CalcDef());
      ies_precon.SetResidualEpsilon(this->self().CalcDef()->GetRspPreconItEqResidThr());
      ies_precon.SetSolutionEpsilon(this->self().CalcDef()->GetRspPreconItEqSolThr());
      ies_precon.SetMaxIter(this->self().CalcDef()->GetRspPreconItEqMaxIter());
      ies_precon.RelativeConvergence() = true;
      ies_precon.SetChar('~');
      
      // do some output stuff
      this->self().Ostream() << " Improved precon eq. " << i_eq << ": ";
      
      if(this->self().CalcDef()->GetRspComplexImprovedPrecondMute()) this->self().Ostream().Mute(); // we mute Mout to not flood output file, Solve will output dots pr iter
      else this->self().Ostream() << "\n";
      
      // actually solve equations
      if(!ies_precon.Solve())
      {
         //MIDASERROR("Could not solve improved precon equations");
         MidasWarning("Could not solve improved precon equations");
      }
      this->self().Ostream().Unmute(); // unmute Mout again... if not muted this does nothing
      this->self().Ostream() << "\n" << std::flush;

      /// copy back result
      auto result = ies_precon.Solution();
      v.Re().Reassign(result[0].Re(),0,I_1,C_0,mSmallDim);
      v.Im().Reassign(result[0].Im(),0,I_1,C_0,mSmallDim);
   }
   //Mout << " v after : " << std::endl;
   //Mout << v << std::endl;
   
   ///////////////////////////////////////////////////////////////////////////
   // Precon rest
   ///////////////////////////////////////////////////////////////////////////
   ComplexVector<DataCont,DataCont> v_rest{ {static_cast<In>(mDiffDim),C_0,"InMem","v_rest_re"}
                                          , {static_cast<In>(mDiffDim),C_0,"InMem","v_rest_im"} 
                                          };
   v_rest.Re().Reassign(v.Re(),mSmallDim,I_1);
   v_rest.Im().Reassign(v.Im(),mSmallDim,I_1);
   ComplexVector<DataCont,DataCont> v_copy = v_rest;

   auto inv_squared = InverseSquared(frq);
   //
   auto a_copy = mAdiag;
   IES_Axpy(a_copy, mBdiag, -frq.real()); // a_copy <- (A_0 - frq_re B_0)

   // real part
   v_rest.Re().HadamardProduct(a_copy,C_1,DataCont::Product::Normal);        // re <- (A_0 - frq_re B_0) re
   v_rest.Im().HadamardProduct(mBdiag,frq.imag(),DataCont::Product::Normal); // im <- frq_im B_0 im
   IES_Axpy(v_rest.Re(),v_rest.Im(),C_1);          // re <- (A_0 - frq_re B_0) re + frq_im B_0 im
   v_rest.Re().HadamardProduct(inv_squared,C_1,DataCont::Product::Inverse);
   
   // imag part (using v_copy, and saving result in re)
   v_copy.Re().HadamardProduct(mBdiag,frq.imag(),DataCont::Product::Normal); // re <- frq_im B_0 re
   v_copy.Im().HadamardProduct(a_copy,C_1,DataCont::Product::Normal);        // im <- (A_0 - frq_re B_0) im
   IES_Axpy(v_copy.Re(),v_copy.Im(),-C_1);    // re <- frq_im B_0 re - (A_0 - frq_re B_0) im
   v_copy.Re().HadamardProduct(inv_squared,C_1,DataCont::Product::Inverse);

   v_rest.Im() = v_copy.Re(); // save result in v

   // copy back result
   //Mout << setprecision(17);
   //Mout << " copy back result " << std::endl;
   //Mout << " v: " << std::endl;
   //Mout << v << std::endl;
   //Mout << " v_rest:  " << std::endl;
   //Mout << v_rest << std::endl;
   //Mout << " precon impl: trans_y size : " << trans_y.size() << " line: " << __LINE__ << std::endl;
   v_rest.Re().Reassign(v.Re(),mSmallDim,-I_1);
   v_rest.Im().Reassign(v.Im(),mSmallDim,-I_1);
   //Mout << " precon impl: trans_y size : " << trans_y.size() << " line: " << __LINE__ << std::endl;
   //Mout << std::scientific << std::setprecision(16);
   //Mout << " v after: " << std::endl;
   //Mout << v << std::endl;
   //Mout << " precon impl: trans_y size : " << trans_y.size() << " line: " << __LINE__ << std::endl;
   //Mout << " exit precon " << std::endl;
   //exit(42);
}

/**
 * precon
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::Precondition()
{
   //Mout << " === Starting Improved Precon === " << std::endl;
   for(size_t i = 0; i<this->self().Residuals().size(); ++i)
   {
      if(!this->self().Converged()[i])
      {
         //Mout << " *** Preconditioning " << i << "th equation *** " << std::endl;
         PreconditionImpl(this->self().Residuals()[i]
                        , this->self().Atrans().Frequency(i)
                        , this->self().Residuals() // hmm why is this passed ? :S
                        , i
                        );
      }
   }
   this->self().Ostream() << std::endl; // make line break
   //Mout << " ==== Ending Improved Precon ==== " << std::endl;
}

/**
 *
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::Output() const
{
   A::Output();

   auto& os = A::Ostream();
   os << " ImprovedPrecon:\n"
      << "     Level: " << mLevel << "\n"
      << "     SmallDim: " << mSmallDim << "\n"
      << "     Residual Threshold: " << this->self().CalcDef()->GetRspPreconItEqResidThr() << "\n"
      << "     Solution Threshold: " << this->self().CalcDef()->GetRspPreconItEqSolThr() << "\n"
      << std::flush;
}

/**
 * Analyse condition numbers of the original system and the preconditioned system
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::AnalyseConditionNumber() //const
{
   Mout << "???????????????????? ANALYSE CONDITION NUMBER ???????????????????" << std::endl;
   Mout << std::left
        << std::setw(20) << " Dim "      << " : " << this->self().Atrans().Dim() << "\n"
        << std::setw(20) << " SmallDim " << " : " << mSmallDim << "\n";
   Mout << "-----------------------------------------------------------------" << std::endl;

   bool print_matrices = true;

   auto dim = this->self().Atrans().Dim();

   MidasMatrix mat;
   this->self().Atrans().ConstructExplicit(mat);
   if(print_matrices && dim <= 10) Mout << " MATRIX: \n" << mat << std::endl;
   
   MidasMatrix small_mat;
   if(!mpTransformerA->Null()) mpTransformerA->ConstructExplicit(small_mat);
   if(print_matrices && dim <= 10) Mout << " SMALL_MATRIX: \n" << small_mat << std::endl;
   small_mat.SetNewSize(this->self().Atrans().Dim(),this->self().Atrans().Dim(),true,true);
   if(print_matrices && dim <= 10) Mout << " SMALL_MATRIX_RESIZE: \n" << small_mat << std::endl;
   for(int i = mSmallDim; i < this->self().Atrans().Dim(); ++i)
   {
      small_mat[i][i] = (*mAdiag.GetVector())[i - mSmallDim];
   }
   if(print_matrices && dim <= 10) Mout << " SMALL_MATRIX_NEW: \n" << small_mat << std::endl;

   // Get diagonal of A transformer (A_0 is approximate diagonal, not the true one)
   DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem","vec_of_ones");
   DataCont diag(this->self().Atrans().Dim(),C_0,"InMem","diag");
      
   Nb e_dummy = 0.0; // dummy input for Transform
   this->self().Atrans().Transform(vec_of_ones,diag,I_0,I_1,e_dummy);
   diag.Scale(-C_1); // hmm gets -diag above... (so we need to change sign...)
   
   MidasMatrix mat_diag(mat.Nrows(),mat.Ncols(), C_0);
   for(int i = 0; i < mat_diag.Nrows(); ++i) mat_diag[i][i] = (*(diag.GetVector()))[i];
   
   // make unit B
   MidasMatrix b(this->self().Atrans().Dim(),this->self().Atrans().Dim(),C_0);
   b.SetDiagToNb(C_1);
   
   //////////////////////////////////////////////////////////////////////////////////
   // loop over frequencies and make statistics for each
   //////////////////////////////////////////////////////////////////////////////////
   for(int freq_idx = 0; freq_idx < this->self().Atrans().NFrequencies(); ++freq_idx)
   {
      auto freq = this->self().Atrans().Frequency(freq_idx);
      // construct full matrices
      MidasMatrix full; 
      ConstructFullMatrix(mat,b,freq,full);
      if(print_matrices && dim <= 10) Mout << " FULL: \n" << full << std::endl;
      MidasMatrix full_precon; 
      ConstructFullMatrix(small_mat,b,freq,full_precon);
      if(print_matrices && dim <= 10) Mout << " FULL_PRECON: \n" << full_precon << std::endl;
      MidasMatrix full_precon_diag;
      ConstructFullMatrix(mat_diag,b,freq,full_precon_diag);
      if(print_matrices && dim <= 10) Mout << " FULL_PRECON_DIAG: \n" << full_precon_diag << std::endl;

      // calculate A_0^-1
      MidasMatrix full_precon_invert(full_precon);
      auto determinant = InvertGeneral(full_precon_invert);

      MidasMatrix full_precon_identity_rhs(full_precon.Nrows(),full_precon.Ncols());
      MidasMatrix full_precon_identity_lhs(full_precon.Nrows(),full_precon.Ncols());
      full_precon_identity_rhs = full_precon * full_precon_invert;
      full_precon_identity_lhs = full_precon_invert * full_precon;
      
      // calculate A_diag^-1
      MidasMatrix full_precon_diag_invert(full_precon_diag);
      auto determinant_diag = InvertGeneral(full_precon_diag_invert);
      
      // construct A_0^-1 * A
      MidasMatrix full_precon_mult(full_precon.Nrows(),full_precon.Ncols());
      full_precon_mult = full_precon_invert * full;
      
      // construct A_diag^-1 * A
      MidasMatrix full_precon_diag_mult(full_precon_diag.Nrows(),full_precon_diag.Ncols());
      full_precon_diag_mult = full_precon_diag_invert * full;

      // calculate condition numbers
      //auto cond_full = full.ConditionNumber();
      auto cond_full_svd = full.ConditionNumberSVD();
      //auto cond_full_precon = full_precon.ConditionNumber();
      auto cond_full_precon_svd = full_precon.ConditionNumberSVD();
      //auto cond_full_precon_invert = full_precon_invert.ConditionNumber();
      auto cond_full_precon_invert_svd = full_precon_invert.ConditionNumberSVD();
      //auto cond_full_precon_mult = full_precon_mult.ConditionNumber();
      auto cond_full_precon_mult_svd = full_precon_mult.ConditionNumberSVD();
      
      auto cond_full_precon_diag_svd = full_precon_diag.ConditionNumberSVD();
      auto cond_full_precon_diag_invert_svd = full_precon_diag_invert.ConditionNumberSVD();
      auto cond_full_precon_diag_mult_svd = full_precon_diag_mult.ConditionNumberSVD();

      Mout << "-----------------------------------------------------------------" << std::endl;
      Mout << std::left << "STATISTICS "<< freq_idx << " : " << freq << "\n" 
           << std::setw(30) << " |A - A_0|^2 " << " : " << mat.DifferenceNorm(small_mat) << "\n"
           << std::setw(30) << " |full - full_0|^2 " << " : " << full.DifferenceNorm(full_precon) << "\n"
           << std::setw(30) << " |full - full_diag|^2 " << " : " << full.DifferenceNorm(full_precon_diag) << "\n"
           << std::setw(30) << " |full_0 - full_diag|^2 " << " : " << full_precon.DifferenceNorm(full_precon_diag) << "\n"
           << "\n"
           << std::setw(30) << " det|full_0| " << " : " << determinant << "\n"
           << std::setw(30) << " det|full_diag| " << " : " << determinant_diag << "\n"
           << std::setw(30) << " |full_0_identity_rhs|^2 " << " : " << full_precon_identity_rhs.Norm2() << "\n"
           << std::setw(30) << " |full_0_identity_lhs|^2" << " : " << full_precon_identity_lhs.Norm2() << "\n"
           << "\n"
           //<< std::setw(30) << " k(full) " << " : " << cond_full << "\n"
           << std::setw(30) << " k(full)_svd " << " : " << cond_full_svd << "\n"
           << "\n"
           //<< std::setw(30) << " k(full_0) " << " : " << cond_full_precon << "\n"
           << std::setw(30) << " k(full_0)_svd " << " : " << cond_full_precon_svd << "\n"
           //<< std::setw(30) << " k(full_0^-1) " << " : " << cond_full_precon_invert << "\n"
           << std::setw(30) << " k(full_0^-1)_svd " << " : " << cond_full_precon_invert_svd << "\n"
           //<< std::setw(30) << " k(full_0^-1 * full) " << " : " << cond_full_precon_mult << "\n"
           << std::setw(30) << " k(full_0^-1 * full)_svd " << " : " << cond_full_precon_mult_svd << " <- \n"
           << "\n"
           << std::setw(30) << " k(full_diag)_svd " << " : " << cond_full_precon_diag_svd << "\n"
           << std::setw(30) << " k(full_diag^-1)_svd " << " : " << cond_full_precon_diag_invert_svd << "\n"
           << std::setw(30) << " k(full_diag^-1 * full)_svd " << " : " << cond_full_precon_diag_mult_svd << " <- \n"
           << "\n"
           << " BEST: " << (libmda::numeric::float_lt(cond_full_precon_mult_svd,cond_full_precon_diag_mult_svd) ? " IMPROVED " : " DIAG ") << "\n"
           << std::flush;
   }

   Mout << "?????????????????????????????????????????????????????????????????" << std::endl;
   //exit(42);
}

/**
 * construct matrix 
 *
 *     ( A - e^re B   e^im B        )
 *     ( e^Im B       -(A - e^re B) )
 *
 **/
template<class A>
void ImprovedComplexFreqShiftedPreconditioner<A>::ConstructFullMatrix(const MidasMatrix& aA
                                                                    , const MidasMatrix& aB
                                                                    , const std::complex<Nb>& aFreq
                                                                    , MidasMatrix& aMat
                                                                    ) const
{
   // check that matrices are of equal dimension
   MidasAssert((aA.Ncols() == aB.Ncols()) && (aA.Nrows() == aB.Nrows()),"Matrices are not of equal dimension.");
   aMat.SetNewSize(2*aA.Nrows(),2*aA.Ncols());

   // construct 'diagonal'  ( A - e^re B )
   for(size_t i = 0; i < aA.Nrows(); ++i)
   {
      for(size_t j = 0; j < aA.Ncols(); ++j)
      {
         aMat[i][j] = aA[i][j] - aFreq.real() * aB[i][j];    //  ( A - e^re B )
         aMat[i + aA.Nrows()][j + aA.Ncols()] = -aMat[i][j]; // -( A - e^re B )
      }
   }
   // construct 'off-diagonal' ( e^im B )
   for(size_t i = aA.Nrows(); i < 2*aA.Nrows(); ++i)
   {
      for(size_t j = 0; j < aA.Ncols(); ++j)
      {
         aMat[i][j] = aFreq.imag() * aB[i - aA.Nrows()][j];
         aMat[j][i] = aMat[i][j];
      }
   }
}

template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::InitializeSubspace()
{
   if(this->CalcDef()->ItEqSubspacePreconFull())
   {
      // if requested construct sigma as unit matrix
      Nb one = C_1;
      for(In i = 0; i < this->self().Atrans().Dim(); ++i)
      {
         mS.emplace_back(this->self().Atrans().TemplateUnitVector(i));
      }
   }
   else
   {
      // else construct/read in Sigma 
      auto subspace_vectors = this->CalcDef()->ItEqSubspacePreconVector();
      for(int i = 0; i < subspace_vectors.size(); ++i)
      {
         if(!InquireFile(subspace_vectors[i] + "_0"))
         {
            MidasWarning("Did not find subspace vector: " + subspace_vectors[i]);
            continue;
         }
         mS.emplace_back();
         mS.back().GetFromExistingOnDisc(this->self().Atrans().Dim(),subspace_vectors[i]);
         mS.back().ChangeStorageTo("InMem");
         mS.back().NewLabel("suspace_vector_" + std::to_string(i));
         mS.back().SaveUponDecon(true);
      }
      OrthonormalizeSpace(mS);
      DEBUG_CheckOrthogonality(mS);
   }
}

/**
 *
 **/
template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::InitializeTransformedSubspace(const std::vector<DataCont>& aS
                                                                              , std::vector<DataCont>& aSigma
                                                                              , std::vector<DataCont>& aLSigma
                                                                              , MidasMatrix& aDelta
                                                                              , DataCont& aAdiag
                                                                              )
{
   // const_cast subspace because Transform(...) is const incorrect.
   std::vector<DataCont>& subspace = const_cast<std::vector<DataCont>&>(aS);

   // Transform S ( \Sigma = A S )
   for(In i = 0; i < subspace.size(); ++i)
   {
      aSigma.emplace_back(subspace[i].Size());
      Nb dummy = 0.0;
      mpTransformerA->Transform(subspace[i], aSigma[i], 1, 0, dummy);
   }
   // Left Transform s ( L^\Sigma = S^\dagger A )
   for(In i = 0; i < subspace.size(); ++i)
   {
      aLSigma.emplace_back(subspace[i].Size());
      Nb dummy = 0.0;
      mpTransformerLeftA->Transform(subspace[i], aLSigma[i], 1, 0, dummy);
   }
   // calc \Delta  = S^\dagger A S = S^\dagger \Sigma
   aDelta.SetNewSize(subspace.size(), subspace.size(), false, false);
   for(In i = 0; i < subspace.size(); ++i)
   {
      for(In j = 0; j < subspace.size(); ++j)
      {
         aDelta[i][j] = IES_Dot(subspace[i], aSigma[j]);
      }
   }
   // Get A_diag
   DataCont vec_of_ones(this->self().Atrans().Dim(), C_1, "InMem");
   aAdiag.SetNewSize(int(this->self().Atrans().Dim()));
   Nb e_dummy = 0.0; // dummy input for Transform
   mpTransformerA->Transform(vec_of_ones, aAdiag, I_0, I_1, e_dummy);
   aAdiag.Scale(-C_1); // hmm gets -diag above... (so we need to change sign...)
}

/**
 * init
 **/
template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::Initialize()
{
   Mout << " INITIALIZING SUBSPACE PRECONDITIONER " << std::endl;
   A::Initialize();

   ///////////////////////////////////////////////////////////////////////////
   // setup transformer levels
   ///////////////////////////////////////////////////////////////////////////
   //auto level = this->self().Atrans().Level();
   mpTransformerA     = this->self().Atrans().Clone();
   mpTransformerLeftA = this->self().Atrans().Clone();
   //*************************** HARDCODED:
   if(mpTransformerA->Type() != "VCCJAC")
      MIDASERROR("ONLY IMPLEMENTED FOR VCCJAC");

   auto* ptr = dynamic_cast<midas::vcc::TransformerV3*>(mpTransformerLeftA);
   if(ptr)
      ptr->SetType(TRANSFORMER::LVCCJAC);
   else
      MIDASERROR("NOT V3 TYPE!");
   //*************************** HARDCODED END!
   
   InitializeSubspace();
   InitializeTransformedSubspace(mS,mSigma,mLSigma,mDelta,mAdiag);
}

/**
 * precon impl
 **/
template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::PreconditionImpl(ComplexVector<DataCont,DataCont>& v
                                                                 , const std::complex<Nb> frq
                                                                 , std::vector<ComplexVector<DataCont,DataCont> >& trans_y
                                                                 , In i_eq
                                                                 ) const
{
   // make right hand side of small system
   //In dim = this->self().Atrans().Dim();
   In dim = mpTransformerA->VectorDimension();
   std::vector<ComplexVector<DataCont,DataCont> > y(1);
   y[0].Re().SetNewSize(dim);
   y[0].Im().SetNewSize(dim);
   y[0].Re().Reassign(v.Re(),0,I_1,C_0,dim);
   y[0].Im().Reassign(v.Im(),0,I_1,C_0,dim);
   y[0].Im().Scale(-C_1); // we change sign of imag part as solver treats y^R + i*y^I as (y^R, -y^I)
   // and we want to solve for (y^R, y^I)

   // make transformer
   MidasVector frq_re(1); frq_re[0] = frq.real();
   MidasVector frq_im(1); frq_im[0] = frq.imag();
   MidasComplexFreqShiftedSubspaceTransformer trf_a(*mpTransformerA
                                                  , frq_re
                                                  , frq_im
                                                  , mS
                                                  , mSigma
                                                  , mLSigma
                                                  , mDelta
                                                  , mAdiag
                                                  );

   // setup iterative solver
   ComplexFreqShiftedSubSubspaceLinearEquationSolver ies_precon(trf_a, y);
   InitializeLinearSolverFromPreconRspCalcDef(ies_precon,this->self().CalcDef());
   ies_precon.SetResidualEpsilon(this->self().CalcDef()->GetRspPreconItEqResidThr());
   ies_precon.SetSolutionEpsilon(this->self().CalcDef()->GetRspPreconItEqSolThr());
   ies_precon.SetMaxIter(this->self().CalcDef()->GetRspPreconItEqMaxIter());
   ies_precon.RelativeConvergence() = true;
   ies_precon.SetChar('~');

   // do some output stuff
   this->self().Ostream() << " Subspace precon eq. " << i_eq << ": ";

   if(this->self().CalcDef()->GetRspComplexImprovedPrecondMute()) 
      this->self().Ostream().Mute(); // we mute Mout to not flood out
   else 
      this->self().Ostream() << "\n";

   // actually solve equations
   if(!ies_precon.Solve())
   {
      //MIDASERROR("Could not solve improved precon equations");
      MidasWarning("Could not solve improved precon equations");
   }
   this->self().Ostream().Unmute(); // unmute Mout again... if not muted this does nothing
   this->self().Ostream() << "\n" << std::flush;

   /// copy back result
   auto result = ies_precon.Solution();
   v.Re().Reassign(result[0].Re(),0,I_1,C_0,dim);
   v.Im().Reassign(result[0].Im(),0,I_1,C_0,dim);

}


/**
 * precon
 **/
template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::Precondition()
{
   //Mout << " === Starting Improved Precon === " << std::endl;
   for(size_t i = 0; i<this->self().Residuals().size(); ++i)
   {
      if(!this->self().Converged()[i])
      {
         //Mout << " *** Preconditioning " << i << "th equation *** " << std::endl;
         PreconditionImpl(this->self().Residuals()[i]
                        , this->self().Atrans().Frequency(i)
                        , this->self().Residuals() // hmm why is this passed ? :S
                        , i
                        );
      }
   }
   this->self().Ostream() << std::endl; // make line break
   //Mout << " ==== Ending Improved Precon ==== " << std::endl;
}

/**
 * d-tor
 **/
template<class A>
ComplexFreqShiftedSubspacePreconditioner<A>::~ComplexFreqShiftedSubspacePreconditioner()
{
   TransformerDestroy(mpTransformerA);
   TransformerDestroy(mpTransformerLeftA);
   //TransformerDestroy(mpTransformerB);
}

/**
 *
 **/
template<class A>
void ComplexFreqShiftedSubspacePreconditioner<A>::Output() const
{
   A::Output();

   auto& os = A::Ostream();
   os << " Subspace Precond:\n"
      << "     Subspace Size     : " << mS.size() << "\n"
      << "     Residual Threshold: " << this->self().CalcDef()->GetRspPreconItEqResidThr() << "\n"
      << "     Solution Threshold: " << this->self().CalcDef()->GetRspPreconItEqSolThr() << "\n"
      << std::flush;
}

#endif /* COMPLEXFREQSHIFTEDPRECONDITIONER_IMPL_H_INCLUDED */
