#ifndef MIDASIMAGDIAGONALPRECONDITIONER_H_INCLUDED
#define MIDASIMAGDIAGONALPRECONDITIONER_H_INCLUDED

#include "mmv/DataCont.h"

//template<class RESID_T, class A>
template<class A>
class MidasImagDiagonalPreconditioner: public A
{
   private:
      mutable DataCont mDiag; // must be mutable due to less fortunate DataCont interface/design
      
      void PreconditionImpl(DataCont& v, const Nb eigval) const
      {
         v.DirProd(mDiag,eigval,-I_1);
      }

      //static_assert(std::is_same_v<RESID_T,DataCont>,"RESID_T not DataCont");

   public:
      MidasImagDiagonalPreconditioner()
      {
      }
      
      void Initialize()
      {
         A::Initialize();

         mDiag.SetNewSize(this->self().Atrans().Dim());
         DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem");
         Nb e_dummy = 0.0; // dummy input for Transform
         this->self().Atrans().Transform(vec_of_ones,mDiag,I_0,I_1,e_dummy);
      }
      
      void Precondition()
      {
         for(size_t i = 0; i<this->self().Residuals().size(); ++i)
         {
            PreconditionImpl(this->self().Residuals()[i],this->self().Eigenvalues().Re()[i]);
         }
      }
};

#endif /* MIDASIMAGDIAGONALPRECONDITIONER_H_INCLUDED */
