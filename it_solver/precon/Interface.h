#ifndef PRECON_INTERFACE_H_INCLUDED
#define PRECON_INTERFACE_H_INCLUDED

#include "NoPreconditioner.h"
#include "MidasDiagonalPreconditioner.h"
#include "MidasTensorDiagonalPreconditioner.h"
#include "MidasImagDiagonalPreconditioner.h"
#include "MidasFreqShiftedDiagonalPreconditioner.h"
#include "ComplexFreqShiftedPreconditioner.h"

#endif /* PRECON_INTERFACE_H_INCLUDED */
