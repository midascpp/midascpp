#ifndef COMPLEXFREQSHIFTEDPRECONDITIONER_H_INCLUDED
#define COMPLEXFREQSHIFTEDPRECONDITIONER_H_INCLUDED

#include <complex>

#include "../IES.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/Transformer.h"
#include "it_solver/ComplexVector.h"
#include "it_solver/transformer/MidasComplexFreqShiftedTransformer.h"

//template<class RESID_T, class A>
/**
 * precon
 **/
template<class A>
class ComplexFreqShiftedPreconditioner
   : public A
{
   private:
      mutable DataCont mAdiag; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mBdiag; // must be mutable due to less fortunate DataCont interface/design
      
      mutable DataCont mAdiagSquared; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mBdiagSquared; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mAdiagBdiag; // must be mutable due to less fortunate DataCont interface/design
      
      bool mOlsen = false; // do olsen correction?

      /**
       * construct inverse squared matrix for diagonal A, B, and \f$ \omega \f$.
       **/
      DataCont InverseSquared(const std::complex<Nb> frq) const
      {
         DataCont inv_squared(mAdiagSquared);
         IES_Axpy(inv_squared,mBdiagSquared,frq.real()*frq.real() + frq.imag()*frq.imag());
         IES_Axpy(inv_squared,mAdiagBdiag,-2.0*frq.real());
         return inv_squared;
      }


   public:
      ComplexFreqShiftedPreconditioner();
      
      void Initialize();
      
      void PreconditionImpl(ComplexVector<DataCont,DataCont>& v
                          , const std::complex<Nb> frq
                          , std::vector<ComplexVector<DataCont,DataCont> >&
                          , In i_eq
                          ) const;

      void PreconditionOlsenImpl(ComplexVector<DataCont, DataCont>& v
                               , const std::complex<Nb> frq
                               , const ComplexVector<DataCont, DataCont>& sol
                               , In i_eq
                               ) const;
      
      void Precondition();
};

/**
 * Improved
 **/
template<class A>
class ImprovedComplexFreqShiftedPreconditioner
   : public A
{
   private:
      // internal data members
      mutable DataCont mAdiag; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mBdiag; // must be mutable due to less fortunate DataCont interface/design
      
      mutable DataCont mAdiagSquared; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mBdiagSquared; // must be mutable due to less fortunate DataCont interface/design
      mutable DataCont mAdiagBdiag; // must be mutable due to less fortunate DataCont interface/design
      
      bool mExplicit = false; ///< do explicit the precondition with full M^-1
      MidasMatrix mExplicitAZeroMatrix; // explicit A_0
      MidasMatrix mExplicitBZeroMatrix; // explicit B_0

      // transformer stuff
      Transformer* mpTransformerA;
      In mSmallDim;
      In mDiffDim;
      
      // input members
      In mLevel = 0;
      
      /**
       * construct inverse squared matrix for diagonal A, B, and \f$ \omega \f$
       **/
      DataCont InverseSquared(const std::complex<Nb> frq) const
      {
         DataCont inv_squared(mAdiagSquared);
         IES_Axpy(inv_squared,mBdiagSquared,frq.real()*frq.real() + frq.imag()*frq.imag());
         IES_Axpy(inv_squared,mAdiagBdiag,-2*frq.real());
         return inv_squared;
      }

      /**
       * construct full matrix for specific A, B, and \f$ \omega \f$.
       **/
      void ConstructFullMatrix(const MidasMatrix&
                             , const MidasMatrix&
                             , const std::complex<Nb>&
                             , MidasMatrix&
                             ) const;

      /**
       *
       **/
      void InitializeImprovedPreconditioner();
      void InitializeImprovedPreconditionerExplicit();

   public:
      /**
       *
       **/
      ImprovedComplexFreqShiftedPreconditioner();

      /**
       *
       **/
      ~ImprovedComplexFreqShiftedPreconditioner();
      
      /**
       *
       **/
      void Initialize();
      
      /**
       *
       **/
      void PreconditionImpl(ComplexVector<DataCont,DataCont>& v
                          , const std::complex<Nb> frq
                          , std::vector<ComplexVector<DataCont,DataCont> >& trans_y
                          , In i_eq = 0
                          ) const;
      
      /**
       *
       **/
      void Precondition();

      /**
       *
       **/
      void Output() const;
      
      /**
       * analyse condition number of original system and of preconditioned system
       **/
      void AnalyseConditionNumber(); // const;
};


/**
 * precon
 **/
template<class A>
class ComplexFreqShiftedSubspacePreconditioner
   : public A
{
   private:
      Transformer* mpTransformerA = nullptr;
      Transformer* mpTransformerLeftA = nullptr;
      
      mutable std::vector<DataCont> mS;
      mutable std::vector<DataCont> mSigma;
      mutable std::vector<DataCont> mLSigma;
      MidasMatrix mDelta;
      mutable DataCont mAdiag;

      /**
       *
       **/
      void InitializeSubspace();

      /**
       *
       **/
      void InitializeTransformedSubspace(const std::vector<DataCont>& aS
                                       , std::vector<DataCont>& aSigma
                                       , std::vector<DataCont>& aLSigma
                                       , MidasMatrix& aDelta
                                       , DataCont& aAdiag
                                       );

   public:
      /**
       *
       **/
      ComplexFreqShiftedSubspacePreconditioner() 
      {
      }
      
      /**
       *
       **/
      ~ComplexFreqShiftedSubspacePreconditioner();
      
      /**
       *
       **/
      void Initialize();
      
      /**
       *
       **/
      void PreconditionImpl(ComplexVector<DataCont,DataCont>& aV
                          , const std::complex<Nb> aFrq
                          , std::vector<ComplexVector<DataCont,DataCont> >& aTransY
                          , In aIeq = 0
                          ) const;
      
      /**
       *
       **/
      void Precondition();
      
      /**
       *
       **/
      void Output() const;
};

#endif /* COMPLEXFREQSHIFTEDPRECONDITIONER_H_INCLUDED */
