#ifndef MIDASDIAGONALPRECONDITIONER_H_INCLUDED
#define MIDASDIAGONALPRECONDITIONER_H_INCLUDED

#include "mmv/DataCont.h"

template<class A>
class MidasDiagonalPreconditionerBase: public A
{
   private:
      mutable DataCont mDiag;
      
   public:
      MidasDiagonalPreconditionerBase()
      {
      }

      void Initialize()
      {
         A::Initialize();
         
         mDiag.SetNewSize(this->self().Atrans().Dim(),false);
         DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem");
         Nb e_dummy = 0.0; // dummy input for Transform
         this->self().Atrans().Transform(vec_of_ones,mDiag,I_0,I_1,e_dummy);
      }
      
      void PreconditionImpl(DataCont& v, const Nb eigval) const
      {
         v.DirProd(mDiag,eigval,-I_1);
      }
}; 

template<class A>
class MidasDiagonalEigenvaluePreconditioner: public MidasDiagonalPreconditionerBase<A>   
{
   public:
      void Precondition()
      {
         for(size_t i=0; i<this->self().Residuals().size(); ++i)
         {
            MidasDiagonalPreconditionerBase<A>::PreconditionImpl(this->self().Residuals()[i]
                                                               , this->self().Eigenvalues()[i]
                                                               );
         }
      }
};

#endif /* MIDASDIAGONALPRECONDITIONER_H_INCLUDED */
