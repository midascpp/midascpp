#ifndef MIDASCOMPLEXDIAGONALPRECONDITIONER_H_INCLUDED
#define MIDASCOMPLEXDIAGONALPRECONDITIONER_H_INCLUDED

#include "mmv/DataCont.h"

//template<class RESID_T, class A>
template<class A>
class MidasComplexDiagonalPreconditioner: public A
{
   private:
      mutable DataCont mDiag; // must be mutable due to less fortunate DataCont interface/design

      template<class RES>
      void PreconditionIth(RES& res, const MidasVector& eigvals, size_t i) const
      {
         res[i].DirProd(mDiag,eigvals[i],-I_1);
      }

      template<class RES>
      void PreconditionComplexIth(RES& res, const EigenvalueContainer<MidasVector,MidasVector>& eigvals, size_t i) const
      {
         // This is wrong !!
         auto copy_res_re = res[i];
         auto inv_eig_im = eigvals.Im()[i];
         
         // real part
         res[i].DirProd(mDiag,eigvals.Re()[i],-I_1);
         IES_Axpy(res[i],res[i+1],inv_eig_im);
         
         // imag part
         res[i+1].DirProd(mDiag,eigvals.Re()[i],-I_1);
         IES_Axpy(res[i+1],copy_res_re,-inv_eig_im);
      }
      
      template<class RES>
      void PreconditionImpl(RES& res, const MidasVector& eigvals) const
      {
         for(size_t i=0; i<res.size(); ++i)
         {
            PreconditionIth(res,eigvals,i);
         }
      }

      template<class RES>
      void PreconditionImpl(RES& res, const EigenvalueContainer<MidasVector,MidasVector>& eigvals) const
      {
         for(size_t i=0; i<res.size(); ++i)
         {
            if(eigvals.Im()[i])
            { // complex
               PreconditionComplexIth(res,eigvals,i);
               ++i; // we have complex pair, so i+1 has been processed
            }
            else
            { // not complex
               PreconditionIth(res,eigvals.Re(),i);
            }
         }
      }

      //static_assert(std::is_same_v<RESID_T,DataCont>,"RESID_T not DataCont");

   public:
      MidasComplexDiagonalPreconditioner() = default;
      
      void Initialize()
      {
         A::Initialize();

         mDiag.SetNewSize(this->self().Atrans().Dim());
         DataCont vec_of_ones(this->self().Atrans().Dim(),C_1,"InMem");
         Nb e_dummy = 0.0; // dummy input for Transform
         this->self().Atrans().Transform(vec_of_ones,mDiag,I_0,I_1,e_dummy);
      }
      
      void Precondition()
      {
         PreconditionImpl(this->self().Residuals(),this->self().Eigenvalues());
      }
};

#endif /* MIDASCOMPLEXDIAGONALPRECONDITIONER_H_INCLUDED */
