#ifndef MIDASFREQSHIFTEDDIAGONALPRECONDITIONER_H_INCLUDED
#define MIDASFREQSHIFTEDDIAGONALPRECONDITIONER_H_INCLUDED

#include "MidasDiagonalPreconditioner.h"

//template<class RESID_T, class A>
template<class A>
class MidasFreqShiftedDiagonalPreconditioner: public MidasDiagonalPreconditionerBase<A>
{
   public:
      void Precondition()
      {
         assert(this->self().Residuals().size() == this->self().Atrans().Frequencies().Size());
         for(size_t i=0; i<this->self().Residuals().size(); ++i)
         {
            MidasDiagonalPreconditionerBase<A>::PreconditionImpl(this->self().Residuals()[i]
                                                               , this->self().Atrans().Frequencies()[i]
                                                               );
         }
      }
};

template<class A>
class MidasIndividuallyFreqShiftedDiagonalPreconditioner: public MidasDiagonalPreconditionerBase<A>
{
   public:
      void Precondition()
      {
         //assert(this->self().Residuals().size() == this->self().Atrans().Frequencies().Size());
         size_t frq_size = this->self().Atrans().Frequencies().Size();
         for(size_t y_idx=0; y_idx<this->self().Y().size(); ++y_idx)
         {
            for(size_t frq_idx=0; frq_idx<frq_size; ++frq_idx)
            {
               MidasDiagonalPreconditionerBase<A>::PreconditionImpl(this->self().Residuals()[y_idx*frq_size+frq_idx]
                                                                  , this->self().Atrans().Frequencies()[frq_idx]
                                                                  );
            }
         }
      }
};

#endif /* MIDASFREQSHIFTEDDIAGONALPRECONDITIONER_H_INCLUDED */
