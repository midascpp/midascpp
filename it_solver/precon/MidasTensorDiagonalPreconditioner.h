/**
************************************************************************
* 
* @file    MidasTensorDiagonalPreconditioner.h
*
* @date    29-09-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Diagonal preconditioner for TensorDataCont solver
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef MIDASTENSORDIAGONALPRECONDITIONER_H_INCLUDED
#define MIDASTENSORDIAGONALPRECONDITIONER_H_INCLUDED

#include "vcc/TensorDataCont.h"
#include "it_solver/SolutionVectorContainer.h"
#include "it_solver/EigenvalueContainer.h"
#include "util/CallStatisticsHandler.h"

template<class A>
class MidasTensorDiagonalPreconditionerBase
   :  public A
{
   private:
      //! The inverse-diagonal approximation
      TensorDataCont mInvDiag;

      //! The eigenvalue used in the currently saved mInvDiag
      Nb mEnergy;

      //! Balance tensors after preconditioning
      MAKE_VARIABLE(bool, BalanceAfterPrecon, false);

      //! Recompress tensors after preconditioning
      MAKE_VARIABLE(bool, RecompAfterPrecon, false);

      //! Use absolute value of preconditioner
      MAKE_VARIABLE(bool, AbsPrecon, false);

   protected:
      //! Use Olsen algorithm (experimental for linear equations)
      bool mOlsen = false;

      //! Decomp threshold for Olsen algorithm
      Nb mOlsenDecompRelThresh = C_M_1;
      
   public:
      MidasTensorDiagonalPreconditionerBase()
      {
      }

      //! Initialize with zero energy
      void Initialize()
      {
         LOGCALL("init");
         A::Initialize();

         // Create vector of ones to transform
         auto ones(this->self().Atrans().TemplateZeroVector());
         ones.ElementwiseScalarAddition(C_1);
         
         // Set shape of mInvDiag and initialize energy to 0
         this->mInvDiag = ones;
         this->mEnergy = C_0;

         this->self().Atrans().Transform(ones, mInvDiag, I_0, -I_3, this->mEnergy);

         if (  this->mAbsPrecon
            )
         {
            this->mInvDiag.Abs();
         }
      }

      //! Create new diagonal with non-zero energy
      void ReInitialize()
      {
         LOGCALL("re-init");
//         Mout << "    Re-init 0th-order inverse-Jacobian diagonal..." << std::endl;
         auto ones(this->self().Atrans().TemplateZeroVector());
         ones.ElementwiseScalarAddition(C_1);
         this->self().Atrans().Transform(ones, mInvDiag, I_0, -I_3, this->mEnergy);

         if (  this->mAbsPrecon
            )
         {
            this->mInvDiag.Abs();
         }
      }
      
      //! Precondition (implementation)
      void PreconditionImpl
         (  TensorDataCont& v
         ,  const Nb eigval
         )
      {
//         Mout << " == Preconditioning == " << std::endl;
         // If new energy we have to re-initialize the inverse diagonal
         if (  libmda::numeric::float_neq(eigval, this->mEnergy) //std::abs(eigval-this->mEnergy) > C_NB_EPSILON
            )
         {
            this->mEnergy = eigval;
            this->ReInitialize();
         }
         
         // Check sanity
         if (  !this->mInvDiag.SanityCheck()
            )
         {
            MIDASERROR("Energy denominators: Sanity check failed!");
         }

         // Perform preconditioning
         v *= this->mInvDiag;

         if (  this->mBalanceAfterPrecon
            )
         {
            // Balance the tensors
            v.Balance();
         }

         if (  this->mRecompAfterPrecon
            )
         {
            if (  !this->self().DecompInfo().empty()
               )
            {
               auto thresh = IES_Norm(v)*midas::tensor::GetDecompThreshold(this->self().DecompInfo(), true);

               v.Decompose(this->self().DecompInfo(), thresh);
            }
         }
      }

      //!
      void PreconditionComplexImpl
         (  StandardContainer<TensorDataCont>& res
         ,  const EigenvalueContainer<MidasVector, MidasVector>& eigs
         ,  size_t i
         )
      {
         MidasWarning("Complex Davidson preconditioner is approximate!");

         const auto& re_eig = eigs.Re()[i];
         const auto& im_eig = eigs.Im()[i];

         if (  libmda::numeric::float_neq(re_eig, this->mEnergy)
            )
         {
            this->mEnergy = re_eig;
            this->ReInitialize();
         }
         
         // Check sanity
         if (  !this->mInvDiag.SanityCheck()
            )
         {
            MIDASERROR("Energy denominators: Sanity check failed!");
         }
      
         // NB: The next step is approximate!
         // For the eigenvector v = x + i*y with eigenvalue lambda = omega + i*gamma we have stored the 
         // vectors x and y as res[i] and res[i+1], respectively.
         // The real and imaginary parts of the preconditioned vector should be (for element mu):
         // xprecon_mu = (x_mu(epsilon_mu - omega) - gamma*y_mu) / ( (epsilon_mu - omega)^2 + gamma^2 )
         // yprecon_mu = (y_mu(epsilon_mu - omega) + gamma*x_mu) / ( (epsilon_mu - omega)^2 + gamma^2 )
         //
         // Since we cannot (yet) construct the denominator in CP format, we approximate gamma^2 = 0:
         // xprecon_mu = x_mu / (epsilon_mu - omega) - gamma*y_mu/(epsilon_mu-omega)^2
         // yprecon_mu = y_mu / (epsilon_mu - omega) + gamma*x_mu/(epsilon_mu-omega)^2
         auto& re_vec = res[i];
         auto& im_vec = res[i+1];

         im_vec *= this->mInvDiag;
         re_vec *= this->mInvDiag;
         {
            auto prec_re_vec_copy = re_vec;
            {
               auto prec_im_vec_copy = im_vec;

               prec_im_vec_copy *= this->mInvDiag;
               re_vec.Axpy(prec_im_vec_copy, -im_eig);
            }
            
            prec_re_vec_copy *= this->mInvDiag;
            im_vec.Axpy(prec_re_vec_copy, im_eig);
         }

         if (  this->mBalanceAfterPrecon
            )
         {
            // Balance the tensors
            re_vec.Balance();
            im_vec.Balance();
         }

         if (  this->mRecompAfterPrecon
            )
         {
            if (  !this->self().DecompInfo().empty()
               )
            {
               auto thresh = midas::tensor::GetDecompThreshold(this->self().DecompInfo(), true);
               auto re_thresh = IES_Norm(re_vec)*thresh;
               auto im_thresh = IES_Norm(im_vec)*thresh;

               re_vec.Decompose(this->self().DecompInfo(), re_thresh);
               im_vec.Decompose(this->self().DecompInfo(), im_thresh);
            }
         }
      }

      /** Perform preconditioning using the Olsen algorithm.
       *  The preconditioned vector is set to be orthogonal to the current best eigenvector approximation.
       **/
      void PreconditionOlsenImpl
         (  TensorDataCont& res
         ,  Nb eigval
         ,  TensorDataCont& sol
         )
      {
         // If new energy we have to re-initialize the inverse diagonal
         if (  libmda::numeric::float_neq(eigval, this->mEnergy) //std::abs(eigval-this->mEnergy) > C_NB_EPSILON
            )
         {
            this->mEnergy = eigval;
            this->ReInitialize();
         }
         
         // Check sanity
         if (  !this->mInvDiag.SanityCheck()
            )
         {
            MIDASERROR("Energy denominators: Sanity check failed!");
         }
         
         // Calculate standard Davidson vector
         res *= this->mInvDiag;

         // Calculate coefficient for Olsen correction
         Nb coef = C_0;
         {
            auto num = IES_Dot(sol, res);

            auto precon_sol = sol;
            precon_sol *= this->mInvDiag;

            auto denom = IES_Dot(sol, precon_sol);

            coef = -num/denom;

            // Perform Olsen correction
            IES_Axpy(res, precon_sol, coef);
         }

         // Balance
         if (  this->mBalanceAfterPrecon
            )
         {
            res.Balance();
         }

         // Recompress new vector
         if (  this->mRecompAfterPrecon
            )
         {
            if (  !this->self().DecompInfo().empty()
               )
            {
               auto thresh = IES_Norm(res)*midas::tensor::GetDecompThreshold(this->self().DecompInfo(), true);

               res.Decompose(this->self().DecompInfo(), thresh);
            }
         }
      }

      //!
      void PreconditionComplexOlsenImpl
         (  StandardContainer<TensorDataCont>& res
         ,  const EigenvalueContainer<MidasVector, MidasVector>& eigs
         ,  StandardContainer<TensorDataCont>& sol
         ,  size_t i
         )
      {
         MidasWarning("Complex Olsen preconditioner is approximate!");

         const auto& re_eig = eigs.Re()[i];
         const auto& im_eig = eigs.Im()[i];

         if (  libmda::numeric::float_neq(re_eig, this->mEnergy)
            )
         {
            this->mEnergy = re_eig;
            this->ReInitialize();
         }
         
         // Check sanity
         if (  !this->mInvDiag.SanityCheck()
            )
         {
            MIDASERROR("Energy denominators: Sanity check failed!");
         }
      
         // NB: The next step is approximate like for Davidson (see above)!
         auto& re_res = res[i];
         auto& im_res = res[i+1];

         auto& re_sol = sol[i];
         auto& im_sol = sol[i+1];

         // Init Olsen coefs
         auto c_re = C_0;
         auto c_im = C_0;

         // Precondition residuals
         im_res *= this->mInvDiag;
         re_res *= this->mInvDiag;
         {
            auto prec_re_res_copy = re_res;
            {
               auto prec_im_res_copy = im_res;

               prec_im_res_copy *= this->mInvDiag;
               re_res.Axpy(prec_im_res_copy, -im_eig);
            }
            
            prec_re_res_copy *= this->mInvDiag;
            im_res.Axpy(prec_re_res_copy, im_eig);
         }

         // Precondition reduced solutions
         {
            // Copies needed for dots in coefs
            auto im_sol_copy = im_sol;
            auto re_sol_copy = re_sol;

            im_sol *= this->mInvDiag;
            re_sol *= this->mInvDiag;
            {
               auto prec_re_sol_copy = re_sol;
               {
                  auto prec_im_sol_copy = im_sol;

                  prec_im_sol_copy *= this->mInvDiag;
                  re_sol.Axpy(prec_im_sol_copy, -im_eig);
               }
               
               prec_re_sol_copy *= this->mInvDiag;
               im_sol.Axpy(prec_re_sol_copy, im_eig);
            }

            // Calculate dots between preconditioned and non-preconditioned solvecs.
            auto ss_RR = IES_Dot(re_sol_copy, re_sol);
            auto ss_II = IES_Dot(im_sol_copy, im_sol);
            auto ss_RI = IES_Dot(re_sol_copy, im_sol);
            auto ss_IR = IES_Dot(im_sol_copy, re_sol);

            // Calculate dots between non-precon solvecs and precon residuals
            auto sr_RR = IES_Dot(re_sol_copy, re_res);
            auto sr_II = IES_Dot(im_sol_copy, im_res);
            auto sr_RI = IES_Dot(re_sol_copy, im_res);
            auto sr_IR = IES_Dot(im_sol_copy, re_res);

            // Calculate combinations
            auto a_ss = ss_RR + ss_II;
            auto b_ss = ss_RI - ss_IR;

            auto a_sr = sr_RR + sr_II;
            auto b_sr = sr_RI - sr_IR;
            

            // Calculate Olsen coefs
            auto inv_denom = C_1 / (std::pow(a_ss, 2) + std::pow(b_ss, 2));
            c_re = inv_denom * (a_ss*a_sr + b_sr*b_ss);
            c_im = inv_denom * (a_ss*b_sr - a_sr*b_ss);
         }

         // Correct the preconditioned residuals by adding components of the reduced solutions
         re_res.Axpy(re_sol, -c_re);
         re_res.Axpy(im_sol, c_im);

         im_res.Axpy(re_sol, -c_im);
         im_res.Axpy(im_sol, -c_re);


         if (  this->mBalanceAfterPrecon
            )
         {
            // Balance the tensors
            re_res.Balance();
            im_res.Balance();
         }

         if (  this->mRecompAfterPrecon
            )
         {
            if (  !this->self().DecompInfo().empty()
               )
            {
               auto thresh = midas::tensor::GetDecompThreshold(this->self().DecompInfo(), true);
               auto re_thresh = IES_Norm(re_res)*thresh;
               auto im_thresh = IES_Norm(im_res)*thresh;

               re_res.Decompose(this->self().DecompInfo(), re_thresh);
               im_res.Decompose(this->self().DecompInfo(), im_thresh);
            }
         }
      }

      //!
      void SetBalanceAfterPrecon(bool aB)
      {
         this->mBalanceAfterPrecon = aB;
      }

      //!
      void SetRecompAfterPrecon(bool aB)
      {
         this->mRecompAfterPrecon = aB;
      }

      //!
      bool& Olsen()
      {
         return mOlsen;
      }

      //!
      Nb& OlsenDecompRelThresh()
      {
         return mOlsenDecompRelThresh;
      }
}; 

template<class A>
class MidasTensorDiagonalEigenvaluePreconditioner
   :  public MidasTensorDiagonalPreconditionerBase<A>   
{
   private:
      MAKE_VARIABLE(bool, UseHarmonicRrShiftInPrecon, false);

   public:
      void Precondition()
      {
         LOGCALL("precon");
         // Olsen correction requires current best solution
         if (  this->mOlsen
            )
         {
            LOGCALL("construct solution");
            this->self().ConstructSolution();
         }

         for(size_t i=0; i<this->self().Residuals().size(); ++i)
         {
            bool complex_eig = this->self().Eigenvalues().Im()[i] != C_0;

            bool harmonicrr = this->self().HarmonicRayleighRitz() && this->mUseHarmonicRrShiftInPrecon;

            if(!this->self().Converged()[i])
            {
               // Use Olsen algorithm?
               if (  this->mOlsen
                  )
               {
                  LOGCALL("olsen");
                  auto& sol_i = this->self().Solution()[i];

                  // If using CP tensors, we recompress the solution vector first using the info for trial vector and thresh for transformer
                  const auto& trf_decompinfo = this->self().Atrans().GetDecompInfo();
                  const auto& trial_decompinfo = this->self().DecompInfo();
                  if (  !trf_decompinfo.empty()
                     )
                  {
                     LOGCALL("recompress solution");

                     // Get threshold for recompression of solution
                     auto trf_thresh = midas::tensor::GetDecompThreshold(trf_decompinfo, false);
                     auto thresh =  this->mOlsenDecompRelThresh > C_0
                                 ?  std::max(this->mOlsenDecompRelThresh * IES_Norm(sol_i), trf_thresh)
                                 :  trf_thresh;

                     Mout  << " Recompressing solution vector before Olsen update. T_CP = " << thresh << std::endl;
                     
                     sol_i.Decompose(trial_decompinfo, thresh);

                     // If we have a complex eigenpair, we need to recompress the imag part as well
                     if (  complex_eig
                        && !harmonicrr
                        )
                     {
                        this->self().Solution()[i+1].Decompose(trial_decompinfo, thresh);
                     }
                  }

                  // If we are using harmonic Rayleigh-Ritz, we use the energy shift as eigval
                  if (  harmonicrr
                     )
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionOlsenImpl
                        (  this->self().Residuals()[i]
                        ,  this->self().HarmonicRrShift()
                        ,  sol_i
                        );
                  }
                  else if  (  complex_eig
                           )
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionComplexOlsenImpl
                        (  this->self().Residuals()
                        ,  this->self().Eigenvalues()
                        ,  this->self().Solution()
                        ,  i
                        );
                     ++i;  // Residual i+1 has now also been preconditioned
                  }
                  else
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionOlsenImpl
                        (  this->self().Residuals()[i]
                        ,  this->self().Eigenvalues().Re()[i]
                        ,  sol_i
                        );
                  }
               }
               // Else use Davidson
               else
               {
                  LOGCALL("davidson");
                  // If we are using harmonic Rayleigh-Ritz, we use the energy shift as eigval
                  if (  harmonicrr
                     )
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionImpl
                        (  this->self().Residuals()[i]
                        ,  this->self().HarmonicRrShift()
                        );
                  }
                  else if  (  complex_eig
                           )
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionComplexImpl
                        (  this->self().Residuals()
                        ,  this->self().Eigenvalues()
                        ,  i
                        );
                     ++i;  // Residual i+1 has now also been preconditioned
                  }
                  else
                  {
                     MidasTensorDiagonalPreconditionerBase<A>::PreconditionImpl
                        (  this->self().Residuals()[i]
                        ,  this->self().Eigenvalues().Re()[i]
                        );
                  }
               }
            }
         }
      }
};

template
   <  class A
   >
class MidasTensorDiagonalLinearPreconditioner
   :  public MidasTensorDiagonalPreconditionerBase<A>
{
   public:
      void Precondition()
      {
         LOGCALL("precon");
         // Olsen correction requires current best solution
         if (  this->mOlsen
            )
         {
            LOGCALL("construct solution");
            this->self().ConstructSolution();
         }


         for(size_t i=0; i<this->self().Residuals().size(); ++i)
         {
            if (  !this->self().Converged()[i]
               )
            {
               // Olsen-like update
               if (  this->mOlsen
                  )
               {
                  LOGCALL("olsen");
                  auto& sol_i = this->self().Solution()[i];

                  // If using CP tensors, we recompress the solution vector first using the info for trial vector and thresh for transformer
                  const auto& trf_decompinfo = this->self().Atrans().GetDecompInfo();
                  const auto& trial_decompinfo = this->self().DecompInfo();
                  if (  !trf_decompinfo.empty()
                     )
                  {
                     LOGCALL("recompress solution");

                     // Get threshold for recompression of solution
                     auto trf_thresh = midas::tensor::GetDecompThreshold(trf_decompinfo, false);
                     auto thresh =  this->mOlsenDecompRelThresh > C_0
                                 ?  std::max(this->mOlsenDecompRelThresh * IES_Norm(sol_i), trf_thresh)
                                 :  trf_thresh;

                     Mout  << " Recompressing solution vector before Olsen update. T_CP = " << thresh << std::endl;
                     
                     sol_i.Decompose(trial_decompinfo, thresh);
                  }

                  MidasTensorDiagonalPreconditionerBase<A>::PreconditionOlsenImpl
                     (  this->self().Residuals()[i]
                     ,  C_0
                     ,  sol_i
                     );
               }
               // Davidson-like update
               else
               {
                  MidasTensorDiagonalPreconditionerBase<A>::PreconditionImpl
                     (  this->self().Residuals()[i]
                     ,  C_0
                     );
               }
            }
         }
      }
};

template
   <  class A
   >
class MidasTensorFreqShiftedDiagonalPreconditioner
   :  public MidasTensorDiagonalPreconditionerBase<A>
{
   public:
      void Precondition()
      {
         LOGCALL("precon");
         // Olsen correction requires current best solution
         if (  this->mOlsen
            )
         {
            LOGCALL("construct solution");
            this->self().ConstructSolution();
         }

         assert(this->self().Residuals().size() == this->self().Atrans().Frequencies().Size());
         for(size_t i=0; i<this->self().Residuals().size(); ++i)
         {
            if (  !this->self().Converged()[i]
               )
            {
               // Olsen-like update
               if (  this->mOlsen
                  )
               {
                  LOGCALL("olsen");
                  auto& sol_i = this->self().Solution()[i];

                  // If using CP tensors, we recompress the solution vector first using the info for trial vector and thresh for transformer
                  const auto& trf_decompinfo = this->self().Atrans().GetDecompInfo();
                  const auto& trial_decompinfo = this->self().DecompInfo();
                  if (  !trf_decompinfo.empty()
                     )
                  {
                     LOGCALL("recompress solution");

                     // Get threshold for recompression of solution
                     auto trf_thresh = midas::tensor::GetDecompThreshold(trf_decompinfo, false);
                     auto thresh =  this->mOlsenDecompRelThresh > C_0
                                 ?  std::max(this->mOlsenDecompRelThresh * IES_Norm(sol_i), trf_thresh)
                                 :  trf_thresh;

                     Mout  << " Recompressing solution vector before Olsen update. T_CP = " << thresh << std::endl;

                     sol_i.Decompose(trial_decompinfo, thresh);
                  }

                  MidasTensorDiagonalPreconditionerBase<A>::PreconditionOlsenImpl
                     (  this->self().Residuals()[i]
                     ,  this->self().Atrans().Frequencies()[i]
                     ,  sol_i
                     );
               }
               // Davidson-like update
               else
               {
                  MidasTensorDiagonalPreconditionerBase<A>::PreconditionImpl
                     (  this->self().Residuals()[i]
                     ,  this->self().Atrans().Frequencies()[i]
                     );
               }
            }
         }
      }
};

template
   <  class A
   >
class MidasTensorIndividuallyFreqShiftedDiagonalPreconditioner
   :  public MidasTensorDiagonalPreconditionerBase<A>
{
   public:
      void Precondition()
      {
         LOGCALL("precon");
         //assert(this->self().Residuals().size() == this->self().Atrans().Frequencies().Size());
         size_t frq_size = this->self().Atrans().Frequencies().Size();
         for(size_t y_idx=0; y_idx<this->self().Y().size(); ++y_idx)
         {
            for(size_t frq_idx=0; frq_idx<frq_size; ++frq_idx)
            {
               MidasTensorDiagonalPreconditionerBase<A>::PreconditionImpl(this->self().Residuals()[y_idx*frq_size+frq_idx]
                                                                  , this->self().Atrans().Frequencies()[frq_idx]
                                                                  );
            }
         }
      }
};

#endif /* MIDASTENSORDIAGONALPRECONDITIONER_H_INCLUDED */
