#ifndef RESIDUALMAKER_H_INCLUDED
#define RESIDUALMAKER_H_INCLUDED

#include <type_traits>

#include "it_solver/IES.h"
#include "it_solver/EigenvectorContainer.h"
#include "it_solver/EigenvalueContainer.h"
#include "it_solver/SolutionVectorContainer.h"
#include "vcc/TensorSumAccumulator.h"
#include "libmda/numeric/float_eq.h"
#include "util/CallStatisticsHandler.h"

template<class A>
class ResidualMaker: public A
{
   private:
      /**
       * r = A*x - e*x
       **/
      template
         <  class SIG
         ,  class GAM
         ,  class RES
         ,  std::enable_if_t
            <  (  !std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               || !std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               || !std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  GAM& gam
         ,  MidasMatrix& vec
         ,  MidasVector& val
         ,  RES& res
         ,  size_t i
         )
      {
         //res.emplace_back(sig.back().Size());
         // use templatezerovector for data types which are not initialized from a number...
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         
         IES_Zero(res.back());
         for(size_t j=0; j<gam.size(); ++j)
            IES_Axpy(res.back(),gam[j],vec[j][i]);
         
         //DEBUG_CheckSolutionVectorNorm(res.back());
         
         IES_Scale(res.back(),-val[i]);
         
         for(size_t j=0; j<sig.size(); ++j)
            IES_Axpy(res.back(),sig[j],vec[j][i]);
      }

      /**
       *
       **/
      template
         <  class SIG
         ,  class GAM
         ,  class RES
         ,  std::enable_if_t
            <  (  std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               && std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               && std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  GAM& gam
         ,  MidasMatrix& vec
         ,  MidasVector& val
         ,  RES& res
         ,  size_t i
         )
      {
         // Add zero vector (for shape)
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
      
         // Get decompinfo to check if we are using CP tensors
         const auto& decompinfo = this->self().Atrans().GetDecompInfo();
         if (  decompinfo.empty()
            )
         {
            IES_Zero(res.back());
            for(size_t j=0; j<gam.size(); ++j)
            {
               IES_Axpy(res.back(),gam[j],vec[j][i]);
            }
            
            IES_Scale(res.back(),-val[i]);
            
            for(size_t j=0; j<sig.size(); ++j)
            {
               IES_Axpy(res.back(),sig[j],vec[j][i]);
            }
      
            return;
         }
         else
         {
            const auto trfrank = this->self().Atrans().GetAllowedRank();
            const auto n_mcs = res.back().Size();
      
            // Construct decomposer
            #pragma omp parallel
            {
               midas::tensor::TensorDecomposer decomposer(decompinfo);
      
               #pragma omp for schedule(dynamic)
               for(size_t i_mc=0; i_mc<n_mcs; ++i_mc)
               {
                  auto& tens = res.back().GetModeCombiData(i_mc);
                  const auto& dims = tens.GetDims();
                  midas::vcc::TensorSumAccumulator<Nb> sum_acc(dims, &decomposer, trfrank);

                  for(size_t j=0; j<gam.size(); ++j)
                  {
                     auto coef = vec[j][i];
                     const auto& add_gam = gam[j].GetModeCombiData(i_mc);

                     sum_acc.Axpy(add_gam, coef);
                  }
                  sum_acc.Tensor().Scale(-val[i]);
                  for(size_t k=0; k<sig.size(); ++k)
                  {
                     auto coef = vec[k][i];
                     const auto& add_sig = sig[k].GetModeCombiData(i_mc);
                     sum_acc.Axpy(add_sig, coef);
                  }

                  const auto& sum_tens = sum_acc.Tensor();
      
                  // Evaluate sum
                  if (  sum_tens.NDim() == 0
                     || sum_tens.Type() == BaseTensor<Nb>::typeID::SCALAR
                     )
                  {
                     Nb scalar;
                     sum_acc.EvaluateSum().GetTensor()->DumpInto(&scalar);
                     tens.ElementwiseScalarAddition(scalar);
                  }
                  else
                  {
                     tens = sum_acc.EvaluateSum();
                  }
               }
            }
         }
      }

      template
         <  class SIG
         ,  class GAM
         ,  class RES
         ,  std::enable_if_t
            <  (  !std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               || !std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               || !std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualImag
         (  SIG& sig
         ,  GAM& gam
         ,  EigenvectorContainer<MidasMatrix,MidasMatrix>& vec
         ,  EigenvalueContainer<MidasVector,MidasVector>& val
         ,  RES& res
         ,  size_t i
         )
      {
         //res.emplace_back(sig.back().Size());
         //res.emplace_back(sig.back().Size());
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         auto&& re_res = res[res.size() - 2];
         auto&& im_res = res[res.size() - 1];

         IES_Zero(re_res);
         IES_Zero(im_res);

         for(size_t j=0; j<gam.size(); ++j)
         {
            // real residual
            IES_Axpy(re_res,gam[j],-val.Re()[i]*vec.Rhs()[j][i]);    // -gam * e^re * x^re
            IES_Axpy(re_res,gam[j], val.Im()[i]*vec.Rhs()[j][i+1]);  //  gam * e^im * x^im
            //imag residual
            IES_Axpy(im_res,gam[j],-val.Im()[i]*vec.Rhs()[j][i]);    // -gam * e^im * x^re
            IES_Axpy(im_res,gam[j],-val.Re()[i]*vec.Rhs()[j][i+1]);  // -gam * e^re * x^im
         }
         
         
         
         for(size_t j=0; j<sig.size(); ++j)
         {
            // real residual
            IES_Axpy(re_res,sig[j],vec.Rhs()[j][i]);
            // imag residual
            IES_Axpy(im_res,sig[j],vec.Rhs()[j][i+1]);
         }
      }


      /**
       *
       **/
      template
         <  class SIG
         ,  class GAM
         ,  class RES
         ,  std::enable_if_t
            <  (  std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               && std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               && std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualImag
         (  SIG& sig
         ,  GAM& gam
         ,  EigenvectorContainer<MidasMatrix,MidasMatrix>& vec
         ,  EigenvalueContainer<MidasVector,MidasVector>& val
         ,  RES& res
         ,  size_t i
         )
      {
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         auto&& re_res = res[res.size() - 2];
         auto&& im_res = res[res.size() - 1];

         // Get decompinfo to check if we are using CP tensors
         const auto& decompinfo = this->self().Atrans().GetDecompInfo();
         if (  decompinfo.empty()
            )
         {
            IES_Zero(res.back());
            for(size_t j=0; j<gam.size(); ++j)
            {
               // real residual
               IES_Axpy(re_res,gam[j],-val.Re()[i]*vec.Rhs()[j][i]);    // -gam * e^re * x^re
               IES_Axpy(re_res,gam[j], val.Im()[i]*vec.Rhs()[j][i+1]);  //  gam * e^im * x^im
               //imag residual
               IES_Axpy(im_res,gam[j],-val.Im()[i]*vec.Rhs()[j][i]);    // -gam * e^im * x^re
               IES_Axpy(im_res,gam[j],-val.Re()[i]*vec.Rhs()[j][i+1]);  // -gam * e^re * x^im
            }
            
            for(size_t j=0; j<sig.size(); ++j)
            {
               // real residual
               IES_Axpy(re_res,sig[j],vec.Rhs()[j][i]);
               // imag residual
               IES_Axpy(im_res,sig[j],vec.Rhs()[j][i+1]);
            }
      
            return;
         }
         else
         {
            const auto trfrank = this->self().Atrans().GetAllowedRank();
            const auto n_mcs = res.back().Size();
      
            #pragma omp parallel
            {
               // Construct decomposers
               midas::tensor::TensorDecomposer re_decomposer(decompinfo);
               midas::tensor::TensorDecomposer im_decomposer(decompinfo);

               #pragma omp for schedule(dynamic)
               for(size_t i_mc=0; i_mc<n_mcs; ++i_mc)
               {
                  auto& re_tens = re_res.GetModeCombiData(i_mc);
                  auto& im_tens = im_res.GetModeCombiData(i_mc);
                  const auto& dims = re_tens.GetDims();

                  midas::vcc::TensorSumAccumulator<Nb> re_sum_acc(dims, &re_decomposer, trfrank);
                  midas::vcc::TensorSumAccumulator<Nb> im_sum_acc(dims, &im_decomposer, trfrank);

                  // Add terms scaled by val.Re
                  for(size_t j=0; j<gam.size(); ++j)
                  {
                     const auto& add_gam = gam[j].GetModeCombiData(i_mc);

                     re_sum_acc.Axpy(add_gam, vec.Rhs()[j][i]);
                     im_sum_acc.Axpy(add_gam, vec.Rhs()[j][i+1]);
                  }
                  re_sum_acc.Tensor().Scale(-val.Re()[i]);
                  im_sum_acc.Tensor().Scale(-val.Re()[i]);

                  // Add terms scaled by val.Im
                  for(size_t j=0; j<gam.size(); ++j)
                  {
                     const auto& add_gam = gam[j].GetModeCombiData(i_mc);
                     
                     // Cannot scale terms after the loop
                     // This could give problems with accuracy after recompressions, but let's see...
                     re_sum_acc.Axpy(add_gam, val.Im()[i]*vec.Rhs()[j][i+1]);
                     im_sum_acc.Axpy(add_gam, -val.Im()[i]*vec.Rhs()[j][i]);
                  }

                  // Sigma terms
                  for(size_t k=0; k<sig.size(); ++k)
                  {
                     const auto& add_sig = sig[k].GetModeCombiData(i_mc);
                     re_sum_acc.Axpy(add_sig, vec.Rhs()[k][i]);
                     im_sum_acc.Axpy(add_sig, vec.Rhs()[k][i+1]);
                  }

                  const auto& re_sum_tens = re_sum_acc.Tensor();
      
                  // Evaluate sum
                  if (  re_sum_tens.NDim() == 0
                     || re_sum_tens.Type() == BaseTensor<Nb>::typeID::SCALAR
                     )
                  {
                     Nb re_scalar;
                     re_sum_acc.EvaluateSum().GetTensor()->DumpInto(&re_scalar);
                     re_tens.ElementwiseScalarAddition(re_scalar);

                     Nb im_scalar;
                     im_sum_acc.EvaluateSum().GetTensor()->DumpInto(&im_scalar);
                     im_tens.ElementwiseScalarAddition(im_scalar);
                  }
                  else
                  {
                     re_tens = re_sum_acc.EvaluateSum();
                     im_tens = im_sum_acc.EvaluateSum();
                  }
               }
            }
         }
      }

      /**
       *
       **/
      template<class SIG, class GAM, class RES>
      void MakeResidualImpl(SIG& sig, GAM& gam, MidasMatrix& vec, MidasVector& val, RES& res)
      {
         assert(res.size() == 0);

         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            MakeIthResidualNoImag(sig,gam,vec,val,res,i);
         }
      }

      /**
       *
       **/
      template<class SIG, class GAM, class RES>
      void MakeResidualImpl
         (  SIG& sig
         ,  GAM& gam
         ,  EigenvectorContainer<MidasMatrix,MidasMatrix>& vec
         ,  EigenvalueContainer<MidasVector,MidasVector>& val
         ,  RES& res
         )
      {
         assert(res.size() == 0);

         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            if (  this->self().IoLevel() > I_7
               )
            {
               Mout  << " == Make residual for Eq. " << i << " == " << std::endl;
            }
            Timer timer;

            if(val.Im()[i]!=C_0)
            {
               MakeIthResidualImag(sig, gam, vec, val, res, i);
               ++i;
            }
            else
            {
               MakeIthResidualNoImag(sig, gam, vec.Rhs(), val.Re(), res, i);
            }

            if (  this->self().IoLevel() > I_7
               && gTime
               )
            {
               timer.CpuOut(Mout, " CPU time spent on constructing residual:  ");
               timer.WallOut(Mout, " Wall time spent on constructing residual: ");
            }
         }
      }
   public:
      
      /**
       *
       **/
      void MakeResidual()
      {
         LOGCALL("residual");
         MakeResidualImpl
            (  this->self().Sigma()
            ,  this->self().Trials()    // Gamma is equal to Trials for standard eigenvalue problems.
            ,  this->self().ReducedEigenvectors()
            ,  this->self().Eigenvalues()
            ,  this->self().Residuals()
            );
      }
};


#endif /* RESIDUALMAKER_H_INCLUDED */
