#ifndef FREQSCALEDLINEARRESIDUALMAKER_H_INCLUDED
#define FREQSCALEDLINEARRESIDUALMAKER_H_INCLUDED

#include "util/Error.h"
#include "it_solver/IES.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include <type_traits>

template
   <  class A
   >
class FreqShiftedLinearResidualMaker
   :  public A
{
   private:
      /**
       * r = A*x - e*x (DataCont version)
       **/
      template
         <  class SIG
         ,  class GAM
         ,  class Y
         ,  class RES
         ,  std::enable_if_t
            <  (  !std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               || !std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               || !std::is_same_v<std::vector<TensorDataCont>, Y>
               || !std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  GAM& gam
         ,  Y& y
         ,  MidasMatrix& sol
         ,  const MidasVector& frq
         ,  RES& res
         ,  size_t i
         )
      {
         res.emplace_back(sig[i].Size());
         IES_Zero(res.back());
         
         for(size_t j=0; j<sig.size(); ++j)
         {
            IES_Axpy(res.back(),gam[j],-frq[i]*sol[j][i]);
            IES_Axpy(res.back(),sig[j],sol[j][i]);
         }
         
         IES_Axpy(res.back(),y[i],-C_1);
      }


      /**
       * r = A*x - e*x (Tensor version)
       **/
      template
         <  class SIG
         ,  class GAM
         ,  class Y
         ,  class RES
         ,  std::enable_if_t
            <  (  std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               && std::is_same_v<StandardContainer<TensorDataCont>, GAM>
               && std::is_same_v<std::vector<TensorDataCont>, Y>
               && std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  GAM& gam
         ,  Y& y
         ,  MidasMatrix& sol
         ,  const MidasVector& frq
         ,  RES& res
         ,  size_t i
         )
      {
         // Add zero vector (for shape)
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
         
         // Get decompinfo to check if we are using CP tensors
         const auto& decompinfo = this->self().Atrans().GetDecompInfo();
         if (  decompinfo.empty()
            )
         {
            IES_Zero(res.back());

            for(size_t j=0; j<sig.size(); ++j)
            {
               IES_Axpy(res.back(),gam[j],-frq[i]*sol[j][i]);
               IES_Axpy(res.back(),sig[j],sol[j][i]);
            }
            
            IES_Axpy(res.back(),y[i],-C_1);
         }
         else
         {
            const auto trfrank = this->self().Atrans().GetAllowedRank();
            const auto n_mcs = res.back().Size();
      
            // Construct decomposer
            #pragma omp parallel
            {
               midas::tensor::TensorDecomposer decomposer(decompinfo);
      
               #pragma omp for schedule(dynamic)
               for(size_t i_mc=0; i_mc<n_mcs; ++i_mc)
               {
                  auto& tens = res.back().GetModeCombiData(i_mc);
                  const auto& dims = tens.GetDims();
                  midas::vcc::TensorSumAccumulator<Nb> sum_acc(dims, &decomposer, trfrank);

                  // Subtract RHS
                  sum_acc.Axpy(y[i].GetModeCombiData(i_mc), -C_1);

                  // Add transformed vectors and frequencies
                  for(size_t j=0; j<sig.size(); ++j)
                  {
                     const auto& coef = sol[j][i];
                     const auto& trans_trial_j_imc = sig[j].GetModeCombiData(i_mc);
                     const auto& trial_j_imc = gam[j].GetModeCombiData(i_mc);

                     sum_acc.Axpy(trial_j_imc, -frq[i]*coef);
                     sum_acc.Axpy(trans_trial_j_imc, coef);
                  }

                  const auto& sum_tens = sum_acc.Tensor();

                  // Evaluate sum
                  if (  sum_tens.NDim() == 0
                     || sum_tens.Type() == BaseTensor<Nb>::typeID::SCALAR
                     )
                  {
                     Nb scalar;
                     sum_acc.EvaluateSum().GetTensor()->DumpInto(&scalar);
                     tens.ElementwiseScalarAddition(scalar);
                  }
                  else
                  {
                     tens = sum_acc.EvaluateSum();
                  }
               }
            }
         }
      }
      

      template
         <  class SIG
         ,  class GAM
         ,  class Y
         ,  class RES
         >
      void MakeResidualImpl
         (  SIG& sig
         ,  GAM& gam
         ,  Y& y
         ,  MidasMatrix& sol
         ,  const MidasVector& frq
         ,  RES& res
         )
      {
         assert(res.size() == 0);
         assert(gam.size() == sig.size());
         
         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            MakeIthResidualNoImag(sig,gam,y,sol,frq,res,i);
         }
      }

   public:
      
      void MakeResidual()
      {
         LOGCALL("residual");
         MakeResidualImpl(this->self().Sigma()
                        , this->self().Trials()
                        , this->self().Y()
                        , this->self().ReducedSolution()
                        , this->self().Atrans().Frequencies()
                        , this->self().Residuals()
                        );
      }
};

template<class A>
class IndividuallyFreqShiftedLinearResidualMaker: public A
{
   private:
      /**
       * r = A*x - e*x
       **/
      template<class SIG, class GAM, class Y, class RES>
      void MakeIthResidualNoImag(SIG& sig, GAM& gam, Y& y, MidasMatrix& sol, const MidasVector& frq, RES& res, size_t y_idx, size_t frq_idx)
      {
         res.emplace_back(sig.back().Size());
         IES_Zero(res.back());
         
         auto sol_idx = y_idx*frq.Size() + frq_idx;

         for(size_t j=0; j<sig.size(); ++j)
         {
            IES_Axpy(res.back(),gam[j],-frq[frq_idx]*sol[j][sol_idx]);
            IES_Axpy(res.back(),sig[j],sol[j][sol_idx]);
         }
         
         IES_Axpy(res.back(),y[y_idx],-C_1);
      }
      

      template<class SIG, class GAM, class Y, class RES>
      void MakeResidualImpl(SIG& sig, GAM& gam, Y& y, MidasMatrix& sol, const MidasVector& frq, RES& res)
      {
         assert(res.size() == 0);
         assert(gam.size() == sig.size());
            
         for(size_t y_idx=0; y_idx<y.size(); ++y_idx)
         {
            for(size_t frq_idx=0; frq_idx<frq.Size(); ++frq_idx)
            {
               MakeIthResidualNoImag(sig,gam,y,sol,frq,res,y_idx,frq_idx);
            }
         }
      }

   public:
      
      void MakeResidual()
      {
         LOGCALL("residual");
         MakeResidualImpl(this->self().Sigma()
                        , this->self().Trials()
                        , this->self().Y()
                        , this->self().ReducedSolution()
                        , this->self().Atrans().Frequencies()
                        , this->self().Residuals()
                        );
      }
};

template<class A>
class ComplexFreqShiftedLinearResidualMaker: public A
{
   private:
      /**
       * r = A*x - e*x
       **/
      template<class SIG, class GAM, class Y, class SOL, class TRANS, class RES>
      void MakeIthResidualNoImag(SIG& sig, GAM& gam, Y& y, SOL& sol, TRANS& trans, RES& res, size_t i)
      {
         res.emplace_back(sig[0].Size(),sig[0].Size());
         IES_Zero(res.back());
         
         for(size_t j=0; j<sig.size(); ++j) // sum j
         {
            // real residual
            IES_Axpy(res.back().Re(),gam[j],-trans.ReFrequency(i)*sol[i].Re()[j]);
            IES_Axpy(res.back().Re(),sig[j],sol[i].Re()[j]);
            IES_Axpy(res.back().Re(),gam[j],trans.ImFrequency(i)*sol[i].Im()[j]);

            // imag part
            IES_Axpy(res.back().Im(),gam[j],trans.ImFrequency(i)*sol[i].Re()[j]);
            IES_Axpy(res.back().Im(),sig[j],-sol[i].Im()[j]);
            IES_Axpy(res.back().Im(),gam[j],trans.ReFrequency(i)*sol[i].Im()[j]);
         }
         
         IES_Axpy(res.back().Re(),y[i].Re(),-C_1); // real part
         IES_Axpy(res.back().Im(),y[i].Im(), C_1); // imag part
      }
      

      template<class SIG, class GAM, class Y, class SOL, class TRANS, class RES>
      void MakeResidualImpl(SIG& sig, GAM& gam, Y& y, SOL& sol, TRANS& trans, RES& res)
      {
         //assert(res.size() == 0);
         //assert(gam.size() == sig.size());
         MidasAssert(res.size() == 0,"residual size is not zero");
         MidasAssert(gam.size() == sig.size(),"sigma and gamma are different sizes");
         
         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            MakeIthResidualNoImag(sig,gam,y,sol,trans,res,i);
         }
      }

   public:
      
      void MakeResidual()
      {
         LOGCALL("residual");
         MakeResidualImpl(this->self().Sigma()
                        , this->self().Trials()
                        , this->self().Y()
                        , this->self().ReducedSolution()
                        , this->self().Atrans()
                        , this->self().Residuals()
                        );
      }
};

#endif /* FREQSCALEDLINEARRESIDUALMAKER_H_INCLUDED */
