#ifndef COMPLEXSPACEEXPANDER_IMPL_H_INCLUDED
#define COMPLEXSPACEEXPANDER_IMPL_H_INCLUDED


//typedef std::multimap<Nb,int,std::greater<Nb>> NormMap; //the double is the key (here the norm), the int is the value (here the index)
/**
 * TryToAddTrial - The function is called in the ExpandSpaceImpl function. 
 *
 * @param trials - Input: trial vectors that are making up the trialspace. Output: the previous trial space plus added trial vectors.
 * @param residual - Are changed in orthogonalize         
 * @param idx
 * @param do_discard_check
 * @param added_vector
 * @param n_added_vec
 * @param norm_map
 **/
template<template<class> class ORTHO, template<class> class ADDTRIAL, class A>
template<class T, class R>
void ComplexSpaceExpander<ORTHO, ADDTRIAL, A>::TryToAddTrial
   (  T& trials
   ,  R& residual
   ,  size_t idx
   ,  bool do_discard_check
   ,  bool& added_vector
   ,  In& n_added_vec
   ,  NormMap& norm_map
   )
{
   // equation not converged, we try to add new vector
   auto norm = IES_Norm(residual);
   const Nb eps = mResidualThrow; 
   if(norm > eps) 
   {
      //   this->self().UpdateMetric(); // update metric
      //For running without orthogonalization: outcomment the UpdateMetric() above, and make the following if loop, and corresponding else, as a comment. 
      // norm ok: try to orthogonalize
      if(this->self().Orthogonalize(trials,residual, idx, do_discard_check, norm_map))
      {
         // ortho ok: add vector
         if(this->self().AddTrialVector(trials,residual))
         {
            added_vector = true;
            ++n_added_vec;
            if(gDebug)
            {
               this->self().Ostream() <<" norm, for add trial success" << norm <<  "\n" << std::flush;
            }
         }
         else
         {  //add trial failed
            MidasWarning("Add trial failed");
         }
      }
      else 
      {  //ortho failed
         MidasWarning("could not orthogonalize");
      }
   }
   else
   {  //norm to small 
      this->self().Ostream() <<" norm, for add trial failed" << norm <<  "\n" << std::flush;
      MidasWarning("Norm too small");
   }
}

/**
 * ExpandSpaceImpl - The function will try to add trial vectors to the space. If the orthogonalization gives a vector that can be added to the space, it will be added. Else, a number of vectors with the largest orthogonal components will be added as a rescue action.
 * @param trials             
 * @param residuals          
 * @return - bool, whether a vector has been added or not.
 **/
template<template<class> class ORTHO, template<class> class ADDTRIAL, class A>
template<class T, class R>
bool ComplexSpaceExpander<ORTHO, ADDTRIAL, A>::ExpandSpaceImpl
   (  T& trials
   ,  R& residuals
   )
{
   bool added_vector = false;
   In n_added_vec = 0;
   In n_candidates = 0;
   Nb discard_max_norm =0.0;
   NormMap norm_map; //create empty container

   for(size_t i=0; i<residuals.size(); ++i)
   {
      if(i>=this->self().Converged().size() || !this->self().Converged()[i])
      {
         TryToAddTrial(trials,residuals[i].Re(),2*i,true,added_vector,n_added_vec,norm_map);
         TryToAddTrial(trials,residuals[i].Im(),2*i+1,true,added_vector,n_added_vec,norm_map);
         n_candidates+=2;
      }
      else
      { // equation already converged
         //Mout << "Equation converged, not adding new trial " << std::endl;
      }  
   }

   In n_discarded = n_candidates - n_added_vec;
   if(gDebug)
   {
      this->self().Ostream() << "The size of norm_map " << norm_map.size() << "\n" 
                             << " Candidates: " << n_candidates << "\n"
                             << " Added     : " << n_added_vec << "\n"
                             << " Discarded : " << n_discarded << "\n" 
                             << " Subspace  : " << trials.size() << "\n\n" << std::flush;

               this->self().Ostream() << "Print out the norms and indecies from norm_map " << "\n" << std::flush;
               for(auto& i : norm_map)
               {
                  this->self().Ostream() << "norm: " << i.first << "\n"
                                         << "index: " << i.second <<  "\n" << std::flush;
               }
   }

   // Rescue action if no vectors have been added
   int n_rescue = this->self().CalcDef()->GetRspItEqRescueMax(); 

   this->self().Ostream() << "n_rescue is equal to " << n_rescue << "\n\n" << std::flush;  
   int counter = 0;
   if(n_added_vec == 0)
   {
      for(auto& i : norm_map)
      {
         if( counter >= n_rescue)
         {
            this->self().Ostream() << "Break from adding rescue vectors." << "\n" << std::flush;
            break;
         }

         this->self().Ostream() << "The value of the i index is:  " << i.second  << "\n\n" << std::flush;
         if ( i.second % 2 == 0) //the index is even, and the vector is real
         {
            In a = i.second/2;
                 TryToAddTrial(trials,residuals[a].Re(),a,false,added_vector,n_added_vec,norm_map);
            ++counter;
         }
         else
         {
            In a = (i.second-1)/2;
            TryToAddTrial(trials,residuals[a].Im(),a,false,added_vector,n_added_vec,norm_map);
            ++counter;
         }
      }
   }

   //In n_discarded = n_candidates - n_added_vec;
   this->self().Ostream() << " Candidates: " << n_candidates << "\n"
                          << " Added     : " << n_added_vec << "\n"
                          << " Discarded : " << n_discarded << "\n"
                          << " Subspace  : " << trials.size() << "\n\n" << std::flush;
   // some output
   //DEBUG_CheckOrthogonalityOfNewTrialNoError(trials,n_added_vec);
   if(CheckLinearDependence())
      DEBUG_CheckLinearDependence(trials);

   residuals.clear();
   return added_vector;
}


#endif /* COMPLEXSPACEEXPANDER_IMPL_H_INCLUDED */
