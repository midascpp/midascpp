#ifndef STANDARDTRIALADDER_H_INCLUDED
#define STANDARDTRIALADDER_H_INCLUDED

template<class A>
class StandardTrialAdder: public A
{
   protected:
      /**
       * standard trial adder... just pushes back the vector.
       **/
      template<class T, class RES>
      bool AddTrialVector(T& trials, RES& res)
      {
         trials.emplace_back(res);
         return true; // we can always add the trial
      }
};

#endif /* STANDARDTRIALADDER_H_INCLUDED */
