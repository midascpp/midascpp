#ifndef METRICHOLDER_H_INCLUDED
#define METRICHOLDER_H_INCLUDED

#include "../IES.h"
#include "../IES_Macros.h"

template
   <  class A
   >
class MetricHolder
   :  public A
{
   private:
     MAKE_VARIABLE(MidasMatrix,SubspaceMetric);
   public:

      void UpdateMetric()
      {
         //std::cout << " STARTING UPDATE METRIC " << std::endl;
         auto& trials = this->self().Trials();
         
         if(mSubspaceMetric.Ncols() != trials.size())
         {
            auto old_nrow =  mSubspaceMetric.Nrows();
            //auto old_ncol = mSubspaceMetric.Ncols();
            In size = trials.size();
            mSubspaceMetric.SetNewSize(size,size,true); // reset size saving old data
            for(In i=old_nrow; i<trials.size(); ++i)
            {
               for(In j=0; j<i; ++j)
               {
                  mSubspaceMetric[i][j] = IES_Dot(trials[i],trials[j]);
                  mSubspaceMetric[j][i] = mSubspaceMetric[i][j];
               }
               mSubspaceMetric[i][i] = IES_Norm2(trials[i]);
            }
         }
         //std::cout << " ending UPDATE METRIC " << std::endl;
      }

      //! Get norm of of the part of res that is orthogonal to trials
      template
         <  class RES
         ,  class TRIALS
         >
      Nb OrthogonalComplementNorm
         (  const RES& res
         ,  const TRIALS& trials
         )  const
      {
         // Calculate norm of orthogonal part of vector:
         // ||res_ortho||^2 = ||res||^2 + \sum_{ij} (<trials_i|res><res|trials_j> / ||trials_i||^2||trials_j||^2) - 2 \sum_i (|<trials_i|res>|^2 / ||trials_i||^2)
         auto ortho_norm2 = IES_Norm2(res);

         const auto& metric = this->SubspaceMetric();

         // Calculate dots
         std::vector<Nb> dots(trials.size());
         for(size_t k=0; k<trials.size(); ++k)
         {
            dots[k] = IES_Dot(res, trials[k]);
         }

         for(size_t i=0; i<trials.size(); ++i)
         {
            ortho_norm2 -= C_2*std::pow(dots[i], 2) / metric[i][i];  // ||trials_i||^2 should be 1

            // Add metric terms
            for(size_t j=0; j<trials.size(); ++j)
            {
               ortho_norm2 += (dots[i]*dots[j]*metric[i][j]) / (metric[i][i]*metric[j][j]);
            }
         }

         return std::sqrt(ortho_norm2);
      }

      //! Clear metric
      void ClearMetric
         (
         )
      {
         this->mSubspaceMetric.SetNewSize(I_0, I_0);
      }
};

#endif /* METRICHOLDER_H_INCLUDED */
