#ifndef COMPLEXSPACEEXPANDER_H_INCLUDED
#define COMPLEXSPACEEXPANDER_H_INCLUDED

#include "inc_gen/Warnings.h"
#include "it_solver/IES.h"
#include "it_solver/DEBUG.h"

typedef std::multimap<Nb,int,std::greater<Nb>> NormMap; //the double is the key (here the norm), the int is the value (here the index)

template<template<class> class ORTHO, template<class> class ADDTRIAL, class A>
class ComplexSpaceExpander: 
   public ORTHO<ADDTRIAL<A> >
{
   private:
      Nb mResidualThrow = 1e-19; // norm threshold for adding residual
                                 // if residual has norm below this upon entry it is discarded

      MAKE_VARIABLE(bool, CheckLinearDependence, false);


      template<class T, class R>
      void TryToAddTrial(T& trials, R& residual, size_t idx, bool do_discard_check, bool& added_vector, In& n_added_vec, NormMap& norm_map);

   protected:
   //public:
      /**
       *
       **/
      template<class T, class R>
      bool ExpandSpaceImpl(T& trials, R& residuals);

   public:
      bool ExpandSpace()
      {
         return ExpandSpaceImpl(this->self().Trials(),this->self().Residuals());
      }

      void SetResidualThrow(const Nb aResidualThrow) { mResidualThrow = aResidualThrow; }
};

#include "ComplexSpaceExpander_impl.h"

#endif /* COMPLEXSPACEEXPANDER_H_INCLUDED */
