#ifndef NONORTHOGONALIZER_H_INCLUDED
#define NONORTHOGONALIZER_H_INCLUDED

template<class A>
class NonOrthogonalizer: public A
{
   public:
      template<class TRIALS, class RESIDUAL>
      bool Orthogonalize(TRIALS& trials, RESIDUAL& res)
      {
         return true;
      }
};

#endif /* NONORTHOGONALIZER_H_INCLUDED */
