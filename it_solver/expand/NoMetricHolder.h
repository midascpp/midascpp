#ifndef NOMTRICHOLDER_H_INCLUDED
#define NOMTRICHOLDER_H_INCLUDED

template<class A>
class NoMetricHolder: public A
{
   public:
      void UpdateMetric()
      {
      }
};

#endif /* NOMTRICHOLDER_H_INCLUDED */
