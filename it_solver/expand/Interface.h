#ifndef EXPAND_INTERFACE_H_INCLUDED
#define EXPAND_INTERFACE_H_INCLUDED

// trial adders
#include "StandardTrialAdder.h"
#include "TensorDataContTrialAdder.h"

// ortho
#include "NonOrthogonalizer.h"
#include "IterativeOrthogonalizer.h"
#include "IterativeNonOrthogonalOrthogonalizer.h"

// space expanders
#include "SpaceExpander.h"
#include "ComplexSpaceExpander.h"

// metric holder
#include "NoMetricHolder.h"
#include "MetricHolder.h"

template<class A>
using StandardSpaceExpander = NoMetricHolder<SpaceExpander<IterativeOrthogonalizer,StandardTrialAdder,A> >;

template<class A>
using NonOrthogonalSpaceExpander = MetricHolder<SpaceExpander<IterativeNonOrthogonalOrthogonalizer,StandardTrialAdder,A> >;

template<class A>
using NonOrthoSpaceExpander = NoMetricHolder<SpaceExpander<NonOrthogonalizer,StandardTrialAdder,A> >;

template<class A>
using TensorDataContSpaceExpander = MetricHolder<SpaceExpander<IterativeNonOrthogonalOrthogonalizer, TensorDataContTrialAdder, A> >;

template<class A>
using TensorDataContComplexSpaceExpander = MetricHolder<ComplexSpaceExpander<IterativeNonOrthogonalOrthogonalizer, TensorDataContTrialAdder, A> >;

template<class A>
using StandardComplexSpaceExpander = NoMetricHolder<ComplexSpaceExpander<IterativeOrthogonalizer,StandardTrialAdder,A> >;

template<class A>
using DebugCpComplexSpaceExpander = MetricHolder<ComplexSpaceExpander<IterativeNonOrthogonalOrthogonalizer,StandardTrialAdder,A> >;
#endif /* EXPAND_INTERFACE_H_INCLUDED */
