#ifndef ITERATIVEORTHOGONALIZER_H_INCLUDED
#define ITERATIVEORTHOGONALIZER_H_INCLUDED

#include "it_solver/IES.h"
#include "it_solver/IES_Macros.h"
#include "../DEBUG.h"

typedef std::multimap<Nb,int,std::greater<Nb>> NormMap; //the double is the key (here the norm), the int is the value (here the index)

template<class A>
class IterativeOrthogonalizer: public A
{
   private:
      MAKE_VARIABLE(In,OrthoMax,10);
      MAKE_VARIABLE(Nb,ThreshOrthoDiscard,1e-11);
      MAKE_VARIABLE(Nb,ThreshOrthoRetry,1e-1);

      /**
       * orthogonalize res against trials
       **/
      template<class TRIALS, class RESIDUAL>
     // void OrthogonalizeImpl(TRIALS& trials, RESIDUAL& res, bool& add)
      void OrthogonalizeImpl(TRIALS& trials, RESIDUAL& res, size_t idx, bool do_discard_check, bool& add, NormMap& norm_map)
      {
         const In northo_max = mOrthoMax;
         const Nb thr_discard = mThreshOrthoDiscard;
         const Nb thr_reortho = mThreshOrthoRetry;
         In northo = 0;
         add=false;
   
         while(northo<northo_max)
         {
            auto norm_before = IES_Norm(res);
            for(size_t i=0; i<trials.size(); ++i)
            {
               IES_Orthogonalize(res,trials[i]);
               //DEBUG_CheckDots(res,trials);
            }
            auto norm_after = IES_Norm(res);
   
            auto norm_ratio = norm_after/norm_before;
            //if(norm_ratio < thr_discard)
            if(do_discard_check && (norm_ratio < thr_discard))
            {
               // do not add the vector
               add = false;
               //Store all the vectors that have been discarded with map. They will be ordered from highest to lowest norm. Later, n_rescue vectors with the largest norm will be added.
               norm_map.emplace(norm_after, idx);
               break; // break the while
            }
            else if(norm_ratio < thr_reortho)
            {
               // take another trip in ortho loop
               IES_Scale(res,(C_1/norm_after));
            }
            else
            {
               // add the vector
               add=true;
               IES_Scale(res,(C_1/norm_after));
               break; // break while
            }
            
            ++northo;
         }
   
         if(northo==northo_max)
         {
            MIDASERROR("COULD NOT ORTHOGONALIZE");
         }
      }

   protected:
      explicit IterativeOrthogonalizer()
      {
      }
      
      /**
       * Orthogonalize residual to a set of trials
       **/
      template<class TRIALS, class RESIDUAL>
      bool Orthogonalize(TRIALS& trials, RESIDUAL& res, size_t idx, bool do_discard_check, NormMap& norm_map)
      {
         bool add;
         OrthogonalizeImpl(trials, res, idx, do_discard_check, add, norm_map);
         return add;
      }
      
      template<class TRIALS, class RESIDUAL>
      bool Orthogonalize(TRIALS& trials, RESIDUAL& res)
      {
         bool add;
         //OrthogonalizeImpl(trials, res, add);
         Nb discard_max_norm = Nb(0.0);
         bool do_discard_check = true;
         typedef std::multimap<Nb,int,std::greater<Nb>> NormMap; //the double is the key (here the norm), the int is the value (here the index)
         NormMap norm_map; //create empty container
         OrthogonalizeImpl(trials, res, 0, do_discard_check, add, norm_map);
         return add;
      }

   public:
      void SetOrthoMax(const In aOrthoMax) 
      { 
         mOrthoMax = aOrthoMax; 
      }
      
      void SetThreshOrthoDiscard(const Nb aThreshOrthoDiscard) 
      { 
         mThreshOrthoDiscard = aThreshOrthoDiscard; 
      }
      
      void SetThreshOrthoRetry(const Nb aThreshOrthoRetry) 
      { 
         mThreshOrthoRetry = aThreshOrthoRetry; 
      }
};

#endif /* ITERATIVEORTHOGONALIZER_H_INCLUDED */
