/**
************************************************************************
* 
* @file    TensorDataContTrialAdder.h
*
* @date    13-7-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Trial adder for TensorDataContEigenvalueSolver
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TENSORDATACONTTRIALADDER_H_INCLUDED
#define TENSORDATACONTTRIALADDER_H_INCLUDED

#include "vcc/TensorDataCont.h"
#include "util/Timer.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/TensorDecomposer.h"
#include "it_solver/SolutionVectorContainer.h"
#include "util/CallStatisticsHandler.h"
#include "it_solver/IES.h"

template<class A>
class TensorDataContTrialAdder
   :  public A
{
   private:
      //! TensorDecompInfo for recompression of new trial vectors
      MAKE_VARIABLE(midas::tensor::TensorDecompInfo::Set, DecompInfo);
      MAKE_VARIABLE(Nb, OrthoScreening, static_cast<Nb>(1.e-20));

      bool TestLinearDependency
         (  const TensorDataCont& new_trial
         ,  const StandardContainer<TensorDataCont>& trials
         )
      {
//         Mout  << " Test linear dependency of new trial:" << std::endl;

         auto ortho_norm = this->self().OrthogonalComplementNorm(new_trial, trials);

//         Mout  << "    Norm of orthogonal complement:   " << ortho_norm << std::endl;

         return (ortho_norm > 1e-3);
      }

   public:
      bool AddTrialVector
         (  StandardContainer<TensorDataCont>& trials
         ,  TensorDataCont& res
         )
      {
         LOGCALL("add trial");
         // Normalize vector
         IES_Normalize(res);

         // Test linear dependency
         bool lin_indep = TestLinearDependency(res, trials);

         // Check sanity of new trial vector
         if (  !res.SanityCheck()
            )
         {
            MIDASERROR("TensorDataContTrialAdder: Sanity check of new trial vector failed!");
         }
         
         if (  lin_indep
            )
         {
            trials.emplace_back(res);
            return true;
         }
         else
         {
            MidasWarning("Did not add TensorDataCont trial vector due to linear dependency!");
            return false;
         }
      }

      Nb MinTrialDecompThreshold() const
      {
         return midas::tensor::GetDecompThreshold(this->mDecompInfo, true);
      }

      void SetDecompInfo(const midas::tensor::TensorDecompInfo::Set& aInfo)
      {
         this->mDecompInfo = aInfo;
      }
};

#endif /* TENSORDATACONTTRIALADDER_H_INCLUDED */
