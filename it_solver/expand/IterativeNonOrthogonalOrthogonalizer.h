#ifndef ITERATIVENONORTHOGONALORTHOGONALIZER_H_INCLUDED
#define ITERATIVENONORTHOGONALORTHOGONALIZER_H_INCLUDED

#include "it_solver/IES.h"
#include "../DEBUG.h"

#include "util/Error.h"
#include "util/CallStatisticsHandler.h"
#include "lapack_interface/LapackInterface.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

#include "vcc/TensorSumAccumulator.h"

inline MidasVector LinearEquationSolutionWrapper(const MidasMatrix& aS, const MidasVector& aV)
{
   //Timer time;
   //std::cout << " starting to calc c " << std::endl;
   Linear_eq_struct<double> sol = POSV(aS,aV);
   
   if (  sol.info != 0
      )
   {
      MidasWarning("LinearEquationSolutionWrapper: POSV info = " + std::to_string(sol.info));
   }

   //std::cout << " solved linear equations " << std::endl;
   MidasVector c;
   
   if(sol.n > 0)
   {
      c.SetData(sol.solution.release(),sol.n,sol.n);

      ////clean up... we need to destroy the double**, but we are keeping the solution as a double* in c
      //delete[] sol._solution;
      //sol._solution = nullptr; // remember to set _solution to NULL 
      //                         // so Linear_eq_struct doesn't try to destroy it again
   }

   //sol._n = 0; // this is not nessecary (but we could do it for cleanness :O)
   //sol._n_rhs = 0; // but it doesn't matter as sol is deconstructed instantly after this
   //time.CpuOut(Mout, " CPU time used in TensorEigSolver::LinearEquationWrapper ");
   //std::cout << " returning c " << std::endl;
   return c;
}

template<class TRIALS, class RES>
MidasVector CalculateV(TRIALS& trials, RES& res)
{
   MidasVector v(trials.size());
   for(int i=0; i<v.size(); ++i)
   {
      v[i] = IES_Dot(trials[i],res);
   }
   return v;
}

template<class TRIALS>
MidasMatrix CalculateS(TRIALS& trials)
{
   MidasMatrix s(int(trials.size()),int(trials.size()));
   for(int i=0; i<trials.size(); ++i)
   {
      // Exploit symmetry
      for(int j=i+1; j<trials.size(); ++j)
      {
         s[i][j] = IES_Dot(trials[i],trials[j]);
         s[j][i] = s[i][j];
      }

      // Calculate diagonal
      s[i][i] = IES_Norm2(trials[i]);
   }
   return s;
}

template
   <  class A
   >
class NonOrthogonalSpaceOrthogonalizer
   :  public A
{
   private:
      //! Ortho with coeffs
      template
         <  class TRIALS
         ,  class COEF
         ,  class RES
         ,  std::enable_if_t
            <  (  !std::is_same_v<StandardContainer<TensorDataCont>, TRIALS>
               || !std::is_same_v<TensorDataCont, RES>
               )
            >* = nullptr
         >
      void OrthogonalizeWithCoefficients
         (  TRIALS& trials
         ,  const COEF& coef
         ,  RES& res
         )
      {
         MidasAssert(trials.size() == coef.size(),"trials and coefficients does not have same size.");
         for(int i=0; i<trials.size(); ++i)
         {
            if (  std::isfinite(coef[i])
               && !std::isnan(coef[i])
               )
            {
               IES_Axpy(res,trials[i],-coef[i]);
            }
            else
            {
               MIDASERROR("OrthogonalizeWithCoefficients: coef = " + std::to_string(coef[i]) + " detected!");
            }
         }
      }


      //! Ortho with coeffs
      template
         <  class TRIALS
         ,  class COEF
         ,  class RES
         ,  std::enable_if_t
            <  (  std::is_same_v<StandardContainer<TensorDataCont>, TRIALS>
               && std::is_same_v<TensorDataCont, RES>
               )
            >* = nullptr
         >
      void OrthogonalizeWithCoefficients
         (  TRIALS& trials
         ,  const COEF& coef
         ,  RES& res
         )
      {
         LOGCALL("ortho with coefs");
         MidasAssert(trials.size() == coef.size(),"trials and coefficients does not have same size.");

         // Check coefs
         for(const auto& c : coef)
         {
            if (  !std::isfinite(c)
               || std::isnan(c)
               )
            {
               MIDASERROR("OrthogonalizeWithCoefficients: coef = " + std::to_string(c) + " detected!");
            }
         }

         const auto& metric = this->self().SubspaceMetric();

         // Get decompinfo for trials (as copy) to check if we are using CP tensors
         const auto& decompinfo = this->self().DecompInfo();

         if (  decompinfo.empty()
            )
         {
            for(int i=0; i<trials.size(); ++i)
            {
               IES_Axpy(res,trials[i],-coef[i]);
            }
         }
         else
         {
            // Get norm of orthogonal complement
            auto ortho_norm = this->self().OrthogonalComplementNorm(res, trials);

            // We decompose to same accuracy as most accurate trial (but norm relative)
            auto decomp_thresh = ortho_norm*this->self().MinTrialDecompThreshold();

            if (  this->self().IoLevel() > I_7
               )
            {
               Mout  << " == Performing subspace orthogonalization using CP TensorDataCont == \n"
                     << "    Norm of orthogonal complement:    " << ortho_norm << "\n"
                     << "    Decomp threshold:                 " << decomp_thresh << "\n"
                     << std::flush;
            }

            #pragma omp parallel
            {
               // Construct decomposer
               midas::tensor::TensorDecomposer decomposer(decompinfo);
      
               auto trfrank = this->self().Atrans().GetAllowedRank();
               auto n_mcs = res.Size();
      
               #pragma omp for schedule(dynamic)
               for(size_t i_mc=0; i_mc<n_mcs; ++i_mc)
               {
                  auto& tens = res.GetModeCombiData(i_mc);
                  const auto& dims = tens.GetDims();
                  midas::vcc::TensorSumAccumulator<Nb> sum_acc(dims, &decomposer, trfrank, decomp_thresh);
                  sum_acc += tens;
                  
                  for(size_t i=0; i<trials.size(); ++i)
                  {
                     const auto& trial = trials[i].GetModeCombiData(i_mc);
                     const auto& c = -coef[i];
                     auto norm_times_coef = c*std::sqrt(metric[i][i]);

                     // Only add tensor if coef*trial_norm > mOrthoScreening
                     if (  libmda::numeric::float_gt(std::abs(norm_times_coef), this->self().OrthoScreening())
                        )
                     {
                        sum_acc.Axpy(trial, c);
                     }
                     else
                     {
                        // Only output this message once for each trial
                        if (  i_mc == 0
                           && this->self().IoLevel() > I_7
                           )
                        {
                           Mout  << " NonOrthogonalSpaceOrthogonalizer: Ignoring Axpy with coef:   " << coef[i] << "   and coef*||trials_i||:   " << norm_times_coef << std::endl;
                        }
                     }
                  }

                  // Evaluate sum
                  const auto& sum_tens = sum_acc.Tensor();
                  if (  sum_tens.NDim() == 0
                     || sum_tens.Type() == BaseTensor<Nb>::typeID::SCALAR
                     )
                  {
                     Nb scalar;
                     sum_acc.EvaluateSum().GetTensor()->DumpInto(&scalar);
                     tens.ElementwiseScalarAddition(scalar);
                  }
                  else
                  {
                     tens = sum_acc.EvaluateSum();
                  }
               }
            }
         }
      }

   public:
      template
         <  class RES
         ,  class TRIALS
         >
      void OrthogonalizeAgainstSpace(TRIALS& trials, RES& res)
      {
         auto v = CalculateV(trials,res);
         auto& s = this->self().SubspaceMetric();
         
         //std::cout << " S size: " << s.Nrows() << " " << s.Ncols() << std::endl;
         //std::cout << " v size: " << v.Size() << std::endl;
         auto c = LinearEquationSolutionWrapper(s,v);
         //std::cout << " after calculating c " << std::endl;     
         this->OrthogonalizeWithCoefficients(trials,c,res);
         //std::cout << " after orthogonalizing with c " << std::endl;     
      }


};

template<class A>
class IterativeNonOrthogonalOrthogonalizer
   :  public NonOrthogonalSpaceOrthogonalizer<A>
{
   private:
      MAKE_VARIABLE(In,OrthoMax,10);
      MAKE_VARIABLE(Nb,ThreshOrthoDiscard,1e-11);
      MAKE_VARIABLE(Nb,ThreshOrthoRetry,1e-1);

      /**
       * orthogonalize res against trials
       **/
      template
         <  class TRIALS
         ,  class RESIDUAL
         >
      void OrthogonalizeImpl
         (  TRIALS& trials
         ,  RESIDUAL& res
         ,  bool do_discard_check
         ,  bool& add
         ,  NormMap& norm_map
         )
      {
         Timer ortho_timer;
         const In northo_max = mOrthoMax;
         const Nb thr_discard = mThreshOrthoDiscard;
         const Nb thr_reortho = mThreshOrthoRetry;
         In northo = 0;
         add=false;
         this->self().UpdateMetric(); // update metric
   
         while (  northo < northo_max
               )
         {
            auto norm_before = IES_Norm(res);


            // Check sanity...
            if (  norm_before == C_0
               || std::isnan(norm_before)
               || !std::isfinite(norm_before)
               )
            {
               MidasWarning("Norm of new trial vector = " + std::to_string(norm_before) + " before orthogonalization...");
            }

            this->OrthogonalizeAgainstSpace(trials,res);
            auto norm_after = IES_Norm(res);
            
            if (  this->self().IoLevel() > I_7
               )
            {
               Mout  << "    Norm of new trial vector before orthogonalization: " << norm_before << std::endl;
               Mout  << "    Norm of new trial vector after orthogonalization: " << norm_after << std::endl;
            }

            // Check sanity...
            if (  norm_after == C_0
               || std::isnan(norm_after)
               || !std::isfinite(norm_after)
               )
            {
               MidasWarning("Norm of new trial vector = " + std::to_string(norm_after) + " after orthogonalization...");
            }
   
            auto norm_ratio = norm_after/norm_before;
            if (  do_discard_check
               && norm_ratio < thr_discard
               )
            {
               // do not add the vector
               add = false;
               Mout  << " Norm ratio:  " << norm_ratio << "\n"
                     << " Threshold:   " << thr_discard << "\n"
                     << " NOrtho:      " << northo << "\n"
                     << std::flush;
               break; // break the while
            }
            else if  (  norm_ratio < thr_reortho
                     )
            {
               // take another trip in ortho loop
               IES_Scale(res, (C_1/norm_after));
            }
            else
            {
               // add the vector
               add=true;
               IES_Scale(res, (C_1/norm_after));
               break; // break while
            }
            
            ++northo;
         }

         if (  northo == northo_max
            )
         {
            MIDASERROR("COULD NOT ORTHOGONALIZE");
         }

         if (  this->self().IoLevel() > I_7
            )
         {
            Mout << "    Orthogonalization done in " << northo << " iterations." << std::endl;

            if (  gTime
               )
            {
               ortho_timer.CpuOut(Mout, "    CPU time spent in orthogonalization loop:  ");
               ortho_timer.WallOut(Mout, "    Wall time spent in orthogonalization loop: ");
            }
         }

         //DEBUG_CheckDots(res,trials);
      }

   protected:
      explicit IterativeNonOrthogonalOrthogonalizer()
      {
      }
      
      /**
       * Orthogonalize residual to a set of trials
       **/
      template
         <  class TRIALS
         ,  class RESIDUAL
         >
      bool Orthogonalize
         (  TRIALS& trials
         ,  RESIDUAL& res
         ,  size_t idx
         ,  bool do_discard_check
         ,  NormMap& norm_map
         )
      {
         bool add;
         OrthogonalizeImpl(trials, res, do_discard_check, add, norm_map);
         return add;
      }
      
      //!
      template
         <  class TRIALS
         ,  class RESIDUAL
         >
      bool Orthogonalize
         (  TRIALS& trials
         ,  RESIDUAL& res
         )
      {
         bool add;
         Nb discard_norm = Nb(0.0);
         bool do_discard_check = true;
         typedef std::multimap<Nb,int,std::greater<Nb>> NormMap; //the double is the key (here the norm), the int is the value (here the index)
         NormMap norm_map; //create empty container
         OrthogonalizeImpl(trials, res, do_discard_check, add, norm_map);
         return add;
      }

   public:
      void SetOrthoMax(const In aOrthoMax) 
      { 
         mOrthoMax = aOrthoMax; 
      }
      
      void SetThreshOrthoDiscard(const Nb aThreshOrthoDiscard) 
      { 
         mThreshOrthoDiscard = aThreshOrthoDiscard; 
      }
      
      void SetThreshOrthoRetry(const Nb aThreshOrthoRetry) 
      { 
         mThreshOrthoRetry = aThreshOrthoRetry; 
      }
};

#endif /* ITERATIVENONORTHOGONALORTHOGONALIZER_H_INCLUDED */
