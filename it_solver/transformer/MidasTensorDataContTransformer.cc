#include "MidasTensorDataContTransformer.h"
#include "vcc/TransformerV3.h"

/**
 * Constructor
 * @param t
 * @param aExcludeRef
 **/
MidasTensorDataContTransformer::MidasTensorDataContTransformer
   (  midas::vcc::TransformerV3& t
   ,  const bool aExcludeRef
   )
   :  mTrans(t)
   ,  mDim(t.VectorDimension())
   ,  mVSS(t.GetTransXvecModeCombiOpRange(), t.GetNModals(), aExcludeRef)
   ,  mType(t.GetTensorType())
{
}

/**
 * This is Transform function for primary use.
 * @param in
 * @param out
 * @param i
 * @param j
 * @param e
 **/
void MidasTensorDataContTransformer::Transform
   (  const TensorDataCont& in
   ,  TensorDataCont& out
   ,  In i
   ,  In j
   ,  Nb e
   )  const
{
   mTrans.Transform(in,out,i,j,e);
}

/**
 * 
 **/
In MidasTensorDataContTransformer::Dim() const
{
   return mDim;
}

/**
 *
 **/
const VccStateSpace& MidasTensorDataContTransformer::Vss() const
{
   return mVSS;
}

/**
 *
 **/
const BaseTensor<Nb>::typeID& MidasTensorDataContTransformer::TensorType() const
{
   return mType;
}

/**
 *
 **/
const midas::tensor::TensorDecompInfo::Set& MidasTensorDataContTransformer::GetDecompInfo() const
{
   return mTrans.GetDecompInfo();
}

/**
 *
 **/
In MidasTensorDataContTransformer::GetAllowedRank() const
{
   return mTrans.GetAllowedRank();
}

/**
 * Return a vector (full of zeros) of the appropriate type, size and
 * internal "structure" (e.g. relevant for the TensorDataCont equivalent
 * of this function in MidasTensorDataContTransformer. Used for
 * subsequent Axpys on the vector returned.
 * "Template" not to be confused with a C++ template, but as in the
 * "structure" sense above.
 **/
TensorDataCont MidasTensorDataContTransformer::TemplateZeroVector() const
{
   // Construct and return a zero TensorDataCont from a VccStateSpace&.
   // As of this writing the TDC constructor also takes a third argument,
   // whether to include an element for the reference state, which
   // oftenmost is not the case.
   return TensorDataCont(mVSS, mType);
}

/**
 *
 **/
TensorDataCont MidasTensorDataContTransformer::TemplateUnitVector(Uin aIndex) const
{
   return TensorDataCont(mVSS, aIndex, mType);
}



/**
 *
 **/
MidasTensorDataContFreqShiftedTransformer::MidasTensorDataContFreqShiftedTransformer
   (  midas::vcc::TransformerV3& aTrf
   ,  const MidasVector& aFrq
   ,  bool aExclRef
   )
   :  MidasTensorDataContTransformer(aTrf, aExclRef)
   ,  mFrq(aFrq)
{
}

/**
 *
 **/
const MidasVector& MidasTensorDataContFreqShiftedTransformer::Frequencies
   (
   )  const
{
   return mFrq;
}

/**
 *
 **/
auto MidasTensorDataContFreqShiftedTransformer::Frequency
   (  size_t i
   )  const
{
   return mFrq[i];
}
