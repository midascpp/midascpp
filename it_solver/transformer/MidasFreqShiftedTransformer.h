#ifndef MIDASFREQSCALEDTRANSFORMER_H_INCLUDED
#define MIDASFREQSCALEDTRANSFORMER_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/Transformer.h"

class MidasFreqShiftedTransformer
{
   private:
      Transformer& mTrans;
      In mDim;
      const MidasVector& mFrq;

   public:
      explicit MidasFreqShiftedTransformer(Transformer& t, const MidasVector& frq): 
         mTrans(t), mDim(t.VectorDimension()), mFrq(frq)
      {
      }

      void Transform(DataCont& in, DataCont& out, In i=I_1, In j=I_0, Nb e=0.0) const
      {
         mTrans.Transform(in,out,i,j,e);
      }

      In Dim() const
      {
         return mDim;
      }

      DataCont TemplateZeroVector() const
      {
         return DataCont(Dim(), C_0);
      }

      DataCont TemplateUnitVector(Uin aIndex) const
      {
         // Default construct (size 0), resize and set to unit.
         DataCont unit_dc;
         unit_dc.SetNewSize(Dim());
         unit_dc.SetToUnitVec(aIndex);
         return unit_dc;
      }

      const MidasVector& Frequencies() const
      {
         return mFrq;
      }

      auto Frequency(size_t i) const -> decltype(mFrq[i])
      {
         return mFrq[i];
      }
};

#endif /* MIDASFREQSCALEDTRANSFORMER_H_INCLUDED */
