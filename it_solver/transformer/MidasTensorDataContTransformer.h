#ifndef MIDASTENSORDATACONTTRANSFORMER_H_INCLUDED
#define MIDASTENSORDATACONTTRANSFORMER_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "vcc/VccStateSpace.h"
#include "mmv/MidasVector.h"
#include "tensor/DirProdHelpers.h"
#include "tensor/TensorDecompInfo.h"
#include "vcc/TensorDataCont.h"

// Forward decl
namespace midas::vcc
{
class TransformerV3;
} /* namespace midas::vcc */

/**
 * MidasTensorDataContTransformer class
 **/
class MidasTensorDataContTransformer
{
   private:
      midas::vcc::TransformerV3& mTrans;
      In mDim;
      VccStateSpace mVSS;
      BaseTensor<Nb>::typeID mType;

   public:
      explicit MidasTensorDataContTransformer
         (  midas::vcc::TransformerV3&
         ,  const bool=false
         );

      // This is Transform function for primary use.
      void Transform
         (  const TensorDataCont&
         ,  TensorDataCont&
         ,  In=I_1
         ,  In=I_0
         ,  Nb=0.0
         )  const;

      In Dim() const;

      const VccStateSpace& Vss() const;

      const BaseTensor<Nb>::typeID& TensorType() const;

      const midas::tensor::TensorDecompInfo::Set& GetDecompInfo() const;

      In GetAllowedRank() const;

      // Return a vector (full of zeros) of the appropriate type, size and
      // internal "structure" (e.g. relevant for the TensorDataCont equivalent
      // of this function in MidasTensorDataContTransformer. Used for
      // subsequent Axpys on the vector returned.
      // "Template" not to be confused with a C++ template, but as in the
      // "structure" sense above.
      TensorDataCont TemplateZeroVector() const;

      TensorDataCont TemplateUnitVector(Uin aIndex) const;
};

/**
 * Freq shifted version
 **/
class MidasTensorDataContFreqShiftedTransformer
   :  public MidasTensorDataContTransformer
{
   private:
      using Base = MidasTensorDataContTransformer;

      const MidasVector& mFrq;

   public:
      explicit MidasTensorDataContFreqShiftedTransformer
         (  midas::vcc::TransformerV3&
         ,  const MidasVector&
         ,  bool=false
         );

      const MidasVector& Frequencies
         (
         )  const;

      auto Frequency
         (  size_t i
         )  const;
};

#endif /* MIDASTENSORDATACONTTRANSFORMER_H_INCLUDED */
