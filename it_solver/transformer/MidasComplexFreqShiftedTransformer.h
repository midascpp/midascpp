#ifndef MIDASCOMPLEXFREQSCALEDTRANSFORMER_H_INCLUDED
#define MIDASCOMPLEXFREQSCALEDTRANSFORMER_H_INCLUDED

#include <complex>

#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/Transformer.h"

template<class TRANS, class REFRQ = const MidasVector&, class IMFRQ = const MidasVector&>
class MidasComplexFreqShiftedTransformerImpl
{
   private:
      TRANS mTrans;
      In mDim;
      REFRQ mReFrq;
      IMFRQ mImFrq;

   public:
      explicit MidasComplexFreqShiftedTransformerImpl(TRANS&& t
                                                    , REFRQ&& re_frq 
                                                    , IMFRQ&& im_frq
                                                    ): 
         mTrans(std::forward<TRANS>(t))
       , mDim(mTrans.VectorDimension())
       , mReFrq(std::forward<REFRQ>(re_frq))
       , mImFrq(std::forward<IMFRQ>(im_frq))
      {
         assert(mReFrq.size() == mImFrq.size()); // 
      }

      void Transform(DataCont& in, DataCont& out, In i=I_1, In j=I_0, Nb e=0.0) const
      {
         mTrans.Transform(in,out,i,j,e);
      }

      In Dim() const
      {
         return mDim;
      }

      DataCont TemplateZeroVector() const
      {
         return DataCont(Dim(), C_0);
      }

      DataCont TemplateUnitVector(Uin aIndex) const
      {
         // Default construct (size 0), resize and set to unit.
         DataCont unit_dc;
         unit_dc.SetNewSize(Dim());
         unit_dc.SetToUnitVec(aIndex);
         return unit_dc;
      }

      auto NFrequencies() const -> decltype(mReFrq.size())
      {
         return mReFrq.size();
      }

      std::complex<Nb> Frequency(size_t i) const
      {
         return {mReFrq[i],mImFrq[i]};
      }

      auto ReFrequency(size_t i) const
         -> decltype(mReFrq[i])
      {
         return mReFrq[i];
      }
      
      auto ImFrequency(size_t i) const
         -> decltype(mImFrq[i])
      {
         return mImFrq[i];
      }

      REFRQ& GetReFrq() const
      {
         return mReFrq;
      }

      IMFRQ& GetImFrq() const
      {
         return mImFrq;
      }

      auto CloneImprovedPrecon(In aLevel) const 
         -> decltype(mTrans.CloneImprovedPrecon(aLevel))
      {
         auto ptr = mTrans.CloneImprovedPrecon(aLevel);
         return ptr;
      }
      
      auto Clone() const 
         -> decltype(mTrans.Clone())
      {
         auto ptr = mTrans.Clone();
         return ptr;
      }

      void ConstructExplicit(MidasMatrix& mat) const
      {
         mTrans.ConstructExplicit(mat);
      }
};

using MidasComplexFreqShiftedTransformer = MidasComplexFreqShiftedTransformerImpl<Transformer&>;

#endif /* MIDASCOMPLEXFREQSCALEDTRANSFORMER_H_INCLUDED */
