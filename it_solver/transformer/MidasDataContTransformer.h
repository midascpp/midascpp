#ifndef MIDASDATACONTTRANSFORMER_H_INCLUDED
#define MIDASDATACONTTRANSFORMER_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/Transformer.h"

class MidasDataContTransformer
{
   private:
      Transformer& mTrans;
      In mDim;

   public:
      explicit MidasDataContTransformer(Transformer& t): mTrans(t), mDim(t.VectorDimension())
      {
      }

      void Transform(DataCont& in, DataCont& out, In i=I_1, In j=I_0, Nb e=0.0) const
      {
         mTrans.Transform(in,out,i,j,e);
      }

      In Dim() const
      {
         return mDim;
      }

      // Return a vector (full of zeros) of the appropriate type, size and
      // internal "structure" (e.g. relevant for the TensorDataCont equivalent
      // of this function in MidasTensorDataContTransformer. Used for
      // subsequent Axpys on the vector returned.
      // "Template" not to be confused with a C++ template, but as in the
      // "structure" sense above.
      DataCont TemplateZeroVector() const
      {
         return DataCont(Dim(), C_0);
      }

      DataCont TemplateUnitVector(Uin aIndex) const
      {
         // Default construct (size 0), resize and set to unit.
         DataCont unit_dc;
         unit_dc.SetNewSize(Dim());
         unit_dc.SetToUnitVec(aIndex);
         return unit_dc;
      }
};

#endif /* MIDASDATACONTTRANSFORMER_H_INCLUDED */
