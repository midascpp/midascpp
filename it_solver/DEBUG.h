#ifndef ITSOLVER_DEBUG_H_INCLUDED
#define ITSOLVER_DEBUG_H_INCLUDED

#include <sstream>
#include <iomanip>

#include "util/MidasStream.h"
#include "IES.h"
#include "inc_gen/Warnings.h"
#include "lapack_interface/LapackInterface.h"

extern MidasStream Mout;

/**
 * DEBUG function
 **/
template<class T>
void DEBUG_CheckSolutionVectorNorm(T& solvec)
{
   auto norm = IES_Norm(solvec);
   if(std::fabs(norm-1.0) > 1e-8)
   {
      Mout << " NORM : " << norm << std::endl;
      Mout << " fabs(norm-1.0) = " << fabs(norm-1.0) << std::endl;
      MIDASERROR("NORM ERROR");
   }
}

/**
 * DEBUG
 **/
template<class T>
void DEBUG_CheckOrthogonality(T& trials)
{
   MidasMatrix s_mat(trials.size(),trials.size(),C_0);
   bool orthogonal = true;
   bool normalized = true;
   for(size_t i=0; i<trials.size(); ++i)
   {
      {
         auto dot = IES_Dot(trials[i],trials[i]);
         s_mat[i][i] = dot;
         if(fabs(dot-1.0) > 1e-12)
         {
            normalized = false;
            //MIDASERROR("TRIAL-SPACE NOT NORMALIZED");
         }
      }
      for(size_t j=i+1; j<trials.size(); ++j)
      {
         auto dot = IES_Dot(trials[i],trials[j]);
         s_mat[i][j] = dot;
         if(dot > 1e-12)
         {
            orthogonal = false;
            //MIDASERROR("TRIAL SPACE NOT ORTHOGONAL");
         }
      }
   }
   
   if(!normalized)
   {
      Mout << s_mat << std::endl;
      MIDASERROR("TRIAL-SPACE NOT NORMALIZED");
   }

   if(!orthogonal)
   {
      Mout << s_mat << std::endl;
      MIDASERROR("TRIAL SPACE NOT ORTHOGONAL");
   }
}

template<class T>
void DEBUG_CheckOrthogonalityOfNewTrialNoError(T& trials, int n)
{
   bool orthogonal = true;
   bool normalized = true;
   double worst_normal = 0;
   double worst_dot = 0;
   
   for(int j=trials.size()-n; j<trials.size(); ++j)
   {
      auto dot = IES_Dot(trials[j],trials[j]);
      if(fabs(dot-1.0) > 1e-12)
      {
         if(dot > worst_normal)
         {
            worst_normal = dot;
         }
         normalized = false;
      }
      for(size_t i=0; i<j; ++i)
      {
         dot = IES_Dot(trials[i],trials[j]);
         if(dot > 1e-12)
         {
            if(dot > worst_dot)
            {
               worst_dot = dot;
            }
            orthogonal = false;
         }
      }
   }
   
   if(!normalized)
   {
      //Mout << s_mat << std::endl;
      Mout << "TRIAL-SPACE NOT NORMALIZED: " << worst_normal << std::endl;
      std::stringstream sstr;
      sstr << std::scientific << worst_normal;
      MidasWarning("TRIAL-SPACE NOT NORMALIZED: "+sstr.str());
   }

   if(!orthogonal)
   {
      //Mout << s_mat << std::endl;
      Mout << "TRIAL-SPACE NOT ORTHOGONAL: " << worst_dot << std::endl;
      std::stringstream sstr;
      sstr << std::scientific << worst_dot;
      MidasWarning("TRIAL SPACE NOT ORTHOGONAL: "+sstr.str());
   }
}

template<class T>
void DEBUG_CheckLinearDependence(T& trials)
{
   MidasMatrix s_mat(trials.size(),trials.size(),C_0);
   for(size_t i=0; i<trials.size(); ++i)
   {
      {
         auto dot = IES_Dot(trials[i],trials[i]);
         s_mat[i][i] = dot;
      }
      for(size_t j=i+1; j<trials.size(); ++j)
      {
         auto dot = IES_Dot(trials[i],trials[j]);
         s_mat[i][j] = dot;
         s_mat[j][i] = s_mat[i][j];
      }
   }

   auto eig_sol = SYEVD(s_mat);

   for(size_t i=0; i<eig_sol._num_eigval; ++i)
   {
      if(fabs(eig_sol._eigenvalues[i]-1.0) > 1e-12)
      {
         std::stringstream sstr;
         sstr << std::scientific << fabs(eig_sol._eigenvalues[i]-1.0);
         Mout << "TRAIL SPACE IS LINEAR DEPENDENT: " << fabs(eig_sol._eigenvalues[i]-1.0) << std::endl;
         MidasWarning("TRIAL SPACE IS LINEAR DEPENDENT: "+sstr.str());
      }
   }
}

template<class T>
void DEBUG_CheckLinearDependenceSVD(T& trials)
{
   MidasMatrix s_mat(trials.size(),trials.size(),C_0);
   for(size_t i=0; i<trials.size(); ++i)
   {
      {
         auto dot = IES_Dot(trials[i],trials[i]);
         s_mat[i][i] = dot;
      }
      for(size_t j=i+1; j<trials.size(); ++j)
      {
         auto dot = IES_Dot(trials[i],trials[j]);
         s_mat[i][j] = dot;
         s_mat[j][i] = s_mat[i][j];
      }
   }

   auto svd_sol = GESVD(s_mat);

   for(size_t i=0; i<svd_sol.Min(); ++i)
   {
      if(svd_sol.s[i] < 1e-10)
      {
         std::stringstream sstr;
         sstr << std::scientific << svd_sol.s[i];
         Mout << "TRAIL SPACE IS LINEAR DEPENDENT SVD: " << svd_sol.s[i] << std::endl;
         MidasWarning("TRIAL SPACE IS LINEAR DEPENDENT SVD: "+sstr.str());
      }
   }
}


template<class T, class U>
void DEBUG_CheckDots(T& res, U& trials)
{
   Mout << " Checking dots " << std::endl;
   for(size_t i=0; i<trials.size(); ++i)
   {
      auto dot = IES_Dot(res,trials[i]);
      Mout << " dot_" << i << " = " << dot << std::endl;
   }
   Mout << " Done checking dots " << std::endl;
}

#endif /* ITSOLVER_DEBUG_H_INCLUDED */
