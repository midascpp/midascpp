#ifndef GENERALEIGEN_H_INCLUDED
#define GENERALEIGEN_H_INCLUDED

#include "it_solver/transformer/TransformerBase.h"
#include "it_solver/reduced/ReducedGeneralEigenvalueEquation.h"
#include "it_solver/reduced/ReducedEigenvalueEquation.h"
#include "SolutionVectorContainer.h"
#include "mmv/StorageForSort.h"
#include "IES.h"
#include "ItEquation.h"

/**
 *
 **/
template<class SOL_T
       , class TRIAL_T
       , class SIGMA_T
       , class GAMMA_T
       , class RESID_T
       , class ATRANS
       , class STRANS
       , template<class> class STARTPOLICY
       , template<class, class> class PRECONPOLICY
       , template<class> class RESIDPOLICY
       , template<class> class CONVPOLICY
       , template<class> class EXPANDPOLICY
       , template<class> class REDUCEDPOLICY
       >
class GeneralEigenvalueEquation: 
   public IterativeEquationBase<>
{
   private:
      ATRANS& mAtrans;
      STRANS& mStrans;

      StandardContainer<SOL_T> mSolVec;
      StandardContainer<TRIAL_T> mTrials;
      
      StandardContainer<SIGMA_T> mSigma;
      StandardContainer<GAMMA_T> mGamma;

      StandardContainer<RESID_T> mResiduals;

      ReducedGeneralEigenvalueEquation<MidasMatrix,MidasMatrix> mReduced;
      MidasMatrix mReducedEigenvectors;
      MidasVector mReducedEigenvalues;
      MidasVector mReducedEigenvaluesOld;
      
      void DoTransforms()
      {
         ItEquation::DoTransformsImpl<SIGMA_T>(mAtrans,mTrials,mSigma,this->self().IoLevel());
         ItEquation::DoTransformsImpl<GAMMA_T>(mStrans,mTrials,mGamma,this->self().IoLevel());
      }

   public:
      explicit GeneralEigen(ATRANS& aAtrans, STRANS& aStrans):
         mAtrans(aAtrans), mStrans(aStrans)
      {
      }

      void MakeStartGuess()
      {
         ItEquation::MakeStartGuessImpl(mAtrans,mTrials);
      }

      void DoReducedSolution()
      {
         DoTransforms();
         mReduced.AddToReducedMatrix(mTrials,mSigma,mTrials.size(),matrix_tag::MatA);
         mReduced.AddToReducedMatrix(mTrials,mGamma,mTrials.size(),matrix_tag::MatB);
         mReduced.Evaluate(mReducedEigenvectors,mReducedEigenvalues);
         ItEquation::SortSolution(mReducedEigenvalues,mReducedEigenvectors);
      }

      void MakeResidual()
      {
         ItEquation::MakeResidualImpl(mSigma,mGamma,mReducedEigenvectors,mReducedEigenvalues,mResiduals);
      }
      
      void Precondition()
      {
         // no precnditioner
      }

      bool ExpandSpace()
      {
         return ItEquation::ExpandSpaceImpl(mTrials,mResiduals);
      }
};


#endif /*GENERAL_IT_EIGENSOLVER_H_INCLUDED */
