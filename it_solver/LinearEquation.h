#ifndef LINEAREQUATION_H_INCLUDED
#define LINEAREQUATION_H_INCLUDED

#include "it_solver/SolutionVectorContainer.h"
#include "IES.h"
#include "IterativeEquationBase.h"
#include "ConstructSolution.h"
#include "it_solver/EigenvalueContainerType.h"
#include "it_solver/EigenvectorContainerType.h"
#include "it_solver/ReducedType.h"
#include "libmda/util/output_call_addr.h"

/**
 *
 **/
template<class SOL_T
       , class TRIAL_T
       , class SIGMA_T
       , class RESID_T
       , class ATRANS
       , class Y_T
       , template<class> class INITPOLICY
       , template<class> class STARTPOLICY
       , template<class> class PRECONPOLICY
       , template<class> class RESIDPOLICY
       , template<class> class CONVPOLICY
       , template<class> class EXPANDPOLICY
       , template<class> class REDUCEDPOLICY
       , template<class> class FINALIZEPOLICY
       >
class LinearEquation: 
   public INITPOLICY<
          STARTPOLICY <
          PRECONPOLICY<
          RESIDPOLICY <
          CONVPOLICY  <
          EXPANDPOLICY<
          REDUCEDPOLICY<
          FINALIZEPOLICY<
            IterativeEquationBase<LinearEquation<SOL_T,TRIAL_T,SIGMA_T,RESID_T,ATRANS,Y_T,INITPOLICY,STARTPOLICY,PRECONPOLICY,RESIDPOLICY,CONVPOLICY,EXPANDPOLICY,REDUCEDPOLICY,FINALIZEPOLICY> > 
          > > > > > > > >
{
   using This = IterativeEquationBase<LinearEquation<SOL_T,TRIAL_T,SIGMA_T,RESID_T,ATRANS,Y_T,INITPOLICY,STARTPOLICY,PRECONPOLICY,RESIDPOLICY,CONVPOLICY,EXPANDPOLICY,REDUCEDPOLICY,FINALIZEPOLICY> >;
   
   using REDUCED = REDUCEDPOLICY<FINALIZEPOLICY<This> >;
   friend REDUCED;

   private:
      MAKE_REFVARIABLE(ATRANS,Atrans);
      MAKE_REFVARIABLE(std::vector<Y_T>, Y);

      MAKE_VARIABLE(StandardContainer<SOL_T>,SolVec);
      MAKE_VARIABLE(StandardContainer<TRIAL_T>,Trials);
      MAKE_VARIABLE(StandardContainer<SIGMA_T>,Sigma);
      MAKE_VARIABLE(StandardContainer<RESID_T>,Residuals);

      MAKE_VARIABLE(ReducedType<SOL_T>,ReducedSolution);
      MAKE_VARIABLE(ReducedType<SOL_T>,OldReducedSolution,ReducedType<SOL_T>(0));
      
      void DoTransforms()
      {
         this->DoTransformsImpl(mAtrans,mTrials,mSigma,this->self().IoLevel());
      }

   protected:

   public:
      explicit LinearEquation(ATRANS& aAtrans, std::vector<Y_T>& aY): mAtrans(aAtrans), mY(aY)//, This()
      {
      }
      
      void ConstructSolution()
      {
         ConstructSolutionImpl(mSolVec,mTrials,mReducedSolution,this->self().Neq(),mAtrans.Dim());
      }

      /**
       * Check that the solution is still a solution.
       *
       * !! NB: This will destroy the state of your solver.
       **/
      bool CheckConvergedSolution
         (  const TRIAL_T& aTrial
         )
      {
         // Clear solver
         this->self().ClearSolver(); // we clear any trial vectors before we start
         this->mReducedSolution.SetNewSize(1,1);
         this->mReducedSolution(0,0) = C_1;
         this->self().Neq() = I_1;

         // Add trial
         mTrials.emplace_back(aTrial);

         // Do reduced solution (transforms, etc.)
         this->self().DoTransforms();

         // Make residual
         this->self().MakeResidual();

         // Check convergence
         return this->self().CheckConvergence();
      }


      void SetNeq(In aNeq) 
      {
         MIDASERROR("Linear EQ, mNeq should not be reset"
                  , "Called from: " + libmda::util::return_address<0>());
      }

      const decltype(mSolVec)& Solution() const
      {
         return mSolVec;
      }

      decltype(mSolVec)& Solution()
      {
         return mSolVec;
      }

      decltype(mY) Rhs() const
      {
         return mY;
      }

      std::string Type() const
      {
         return {"Linear Equation"};
      }
};

//
//
//
template<class... Ts>
constexpr bool is_linear_solver(LinearEquation<Ts...>&& t)
{
   return true;
}

constexpr bool is_linear_solver(...)
{
   return false;
}

#endif /* LINEAREQUATION_H_INCLUDED */
