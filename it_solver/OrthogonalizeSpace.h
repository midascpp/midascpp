#ifndef ORTHOGONALIZESPACE_H_INCLUDED
#define ORTHOGONALIZESPACE_H_INCLUDED

#include <vector>
#include "mmv/DataCont.h"

/**
 * @brief iteratively orthogonaize vector in space
 **/
inline void IterativeOrthonormalize(std::vector<DataCont>& trials, DataCont& res, bool& add)
{
   const In northo_max = 20;
   const Nb thr_discard = 1e-19;
   const Nb thr_reortho = 1e-2;
   In northo = 0;
   add=false;

   while(northo<northo_max)
   {
      auto norm_before = IES_Norm(res);
      for(size_t i=0; i<trials.size(); ++i)
      {
         IES_Orthogonalize(res,trials[i]);
         //DEBUG_CheckDots(res,trials);
      }
      auto norm_after = IES_Norm(res);

      auto norm_ratio = norm_after/norm_before;
      if(norm_ratio < thr_discard)
      {
         // do not add the vector
         add = false;
         break; // break the while
      }
      else if(norm_ratio < thr_reortho)
      {
         // take another trip in ortho loop
         IES_Scale(res,(C_1/norm_after));
      }
      else
      {
         // add the vector
         add=true;
         IES_Scale(res,(C_1/norm_after));
         break; // break while
      }

      ++northo;
   }

   if(northo==northo_max)
   {
      MIDASERROR("COULD NOT ORTHOGONALIZE");
   }
}


/**
 * Orthonormalize space
 **/
inline void OrthonormalizeSpace(std::vector<DataCont>& aSpace)
{
   std::vector<DataCont> ortho_space;
   bool add;

   for(In i = 0; i < aSpace.size(); ++i)
   {
      IterativeOrthonormalize(ortho_space,aSpace[i],add);
      if(add)
      {
         ortho_space.emplace_back(std::move(aSpace[i]));
      }
   }

   aSpace = std::move(ortho_space);
}

#endif /* ORTHOGONALIZESPACE_H_INCLUDED */
