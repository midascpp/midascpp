#ifndef LINEAREQUATIONINTERFACE_H_INCLUDED
#define LINEAREQUATIONINTERFACE_H_INCLUDED

#include "IterativeEquationSolver.h"
#include "LinearEquation.h"
#include "reduced/Interface.h"
#include "expand/Interface.h"
#include "precon/Interface_decl.h"
#include "start/LinearStartGuesser.h"
#include "residual/LinearResidualMaker.h"
#include "residual/FreqShiftedLinearResidualMaker.h"
#include "conv/LinearConvergenceChecker.h"
#include "transformer/MidasDataContTransformer.h"
#include "transformer/MidasTensorDataContTransformer.h"
#include "transformer/MidasFreqShiftedTransformer.h"
#include "transformer/MidasComplexFreqShiftedTransformer.h"
#include "transformer/MidasComplexFreqShiftedSubspaceTransformer.h"
#include "mmv/DataCont.h"
#include "Initializer.h"
#include "finalize/NoFinalizer.h"
#include "finalize/TensorDataContFinalizer.h"
#include "ComplexVector.h"
#include "ZeroVector.h"

// Forward decl
class MidasTensorDataContTransformer;


using LinearEquationSolver = 
IterativeEquationSolver<LinearEquation<DataCont
                                     , DataCont
                                     , DataCont
                                     , DataCont
                                     , MidasDataContTransformer
                                     , DataCont
                                     , LinearEquationInitializer
                                     , LinearStartGuesser
                                     , NoPreconditioner
                                     , LinearResidualMaker
                                     , LinearConvergenceChecker
                                     , StandardSpaceExpander
                                     , ReducedNonHermitianLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using FreqShiftedLinearEquationSolver = 
IterativeEquationSolver<LinearEquation<DataCont
                                     , DataCont
                                     , DataCont
                                     , DataCont
                                     , MidasFreqShiftedTransformer
                                     , DataCont
                                     , LinearEquationInitializer
                                     , FreqShiftedLinearStartGuesser
                                     , MidasFreqShiftedDiagonalPreconditioner
                                     , FreqShiftedLinearResidualMaker
                                     , LinearConvergenceChecker
                                     , StandardSpaceExpander
                                     , ReducedNonHermitianFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using IndividuallyFreqShiftedLinearEquationSolver = 
IterativeEquationSolver<LinearEquation<DataCont
                                     , DataCont
                                     , DataCont
                                     , DataCont
                                     , MidasFreqShiftedTransformer
                                     , DataCont
                                     , IndividuallyFreqShiftedLinearEquationInitializer
                                     , IndividuallyFreqShiftedLinearStartGuesser
                                     , MidasIndividuallyFreqShiftedDiagonalPreconditioner
                                     , IndividuallyFreqShiftedLinearResidualMaker
                                     , LinearConvergenceChecker
                                     , StandardSpaceExpander
                                     , ReducedNonHermitianIndividuallyFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using ComplexFreqShiftedLinearEquationSolver =
IterativeEquationSolver<LinearEquation<ComplexVector<DataCont,DataCont> // SOL_T
                                     , DataCont // TRIAL_T
                                     , DataCont // SIGMA_T
                                     , ComplexVector<DataCont,DataCont> // RESID_T
                                     , MidasComplexFreqShiftedTransformer
                                     , ComplexVector<DataCont,ZeroVector<Nb> > // Y_T
                                     , LinearEquationInitializer
                                     , ComplexFreqShiftedLinearStartGuesser
                                     , ComplexFreqShiftedPreconditioner
                                     , ComplexFreqShiftedLinearResidualMaker
                                     , ComplexLinearConvergenceChecker
                                     , StandardComplexSpaceExpander
                                     , ReducedNonHermitianComplexFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using ComplexFreqShiftedLinearEquationSolver2 =
IterativeEquationSolver<LinearEquation<ComplexVector<DataCont,DataCont> // SOL_T
                                     , DataCont // TRIAL_T
                                     , DataCont // SIGMA_T
                                     , ComplexVector<DataCont,DataCont> // RESID_T
                                     , MidasComplexFreqShiftedTransformer
                                     , ComplexVector<DataCont,DataCont > // Y_T
                                     , LinearEquationInitializer
                                     , ComplexFreqShiftedLinearStartGuesser
                                     , ComplexFreqShiftedPreconditioner
                                     , ComplexFreqShiftedLinearResidualMaker
                                     , ComplexLinearConvergenceChecker
                                     , StandardComplexSpaceExpander
                                     , ReducedNonHermitianComplexFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using ImprovedComplexFreqShiftedLinearEquationSolver =
IterativeEquationSolver<LinearEquation<ComplexVector<DataCont,DataCont> // SOL_T
                                     , DataCont // TRIAL_T
                                     , DataCont // SIGMA_T
                                     , ComplexVector<DataCont,DataCont> // RESID_T
                                     , MidasComplexFreqShiftedTransformer
                                     , ComplexVector<DataCont,ZeroVector<Nb> > // Y_T
                                     , LinearEquationInitializer
                                     , ComplexFreqShiftedLinearStartGuesser
                                     , ImprovedComplexFreqShiftedPreconditioner
                                     , ComplexFreqShiftedLinearResidualMaker
                                     , ComplexLinearConvergenceChecker
                                     , StandardComplexSpaceExpander
                                     , ReducedNonHermitianComplexFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using ComplexFreqShiftedSubspaceLinearEquationSolver =
IterativeEquationSolver<LinearEquation<ComplexVector<DataCont,DataCont> // SOL_T
                                     , DataCont // TRIAL_T
                                     , DataCont // SIGMA_T
                                     , ComplexVector<DataCont,DataCont> // RESID_T
                                     , MidasComplexFreqShiftedTransformer
                                     , ComplexVector<DataCont,ZeroVector<Nb> > // Y_T
                                     , LinearEquationInitializer
                                     , ComplexFreqShiftedLinearStartGuesser
                                     , ComplexFreqShiftedSubspacePreconditioner
                                     , ComplexFreqShiftedLinearResidualMaker
                                     , ComplexLinearConvergenceChecker
                                     , StandardComplexSpaceExpander
                                     , ReducedNonHermitianComplexFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using ComplexFreqShiftedSubSubspaceLinearEquationSolver =
IterativeEquationSolver<LinearEquation<ComplexVector<DataCont,DataCont> // SOL_T
                                     , DataCont // TRIAL_T
                                     , DataCont // SIGMA_T
                                     , ComplexVector<DataCont,DataCont> // RESID_T
                                     , MidasComplexFreqShiftedSubspaceTransformer
                                     , ComplexVector<DataCont,DataCont> // Y_T
                                     , LinearEquationInitializer
                                     , ComplexFreqShiftedLinearStartGuesser
                                     , ComplexFreqShiftedPreconditioner
                                     , ComplexFreqShiftedLinearResidualMaker
                                     , ComplexLinearConvergenceChecker
                                     , StandardComplexSpaceExpander
                                     , ReducedNonHermitianComplexFreqShiftedLinearSolver
                                     , NoFinalizer
                                     >
                        >;

using TensorLinearEquationSolver =
IterativeEquationSolver<LinearEquation<TensorDataCont // SOL_T
                                     , TensorDataCont // TRIAL_T
                                     , TensorDataCont // SIGMA_T
                                     , TensorDataCont // RESID_T
                                     , MidasTensorDataContTransformer
                                     , TensorDataCont // Y_T
                                     , LinearEquationInitializer
                                     , LinearStartGuesser
                                     , NoPreconditioner
                                     , LinearResidualMaker
                                     , LinearConvergenceChecker
                                     , TensorDataContSpaceExpander
                                     , ReducedNonHermitianLinearSolver
                                     , TensorDataContFinalizer
                                     >
                        >;

using TensorDiagPreconLinearEquationSolver =
IterativeEquationSolver<LinearEquation<TensorDataCont // SOL_T
                                     , TensorDataCont // TRIAL_T
                                     , TensorDataCont // SIGMA_T
                                     , TensorDataCont // RESID_T
                                     , MidasTensorDataContTransformer
                                     , TensorDataCont // Y_T
                                     , LinearEquationInitializer
                                     , LinearStartGuesser
                                     , MidasTensorDiagonalLinearPreconditioner
                                     , LinearResidualMaker
                                     , LinearConvergenceChecker
                                     , TensorDataContSpaceExpander
                                     , ReducedNonHermitianLinearSolver
                                     , TensorDataContFinalizer
                                     >
                        >;

using TensorFreqShiftedLinearEquationSolver = 
IterativeEquationSolver<LinearEquation<TensorDataCont
                                     , TensorDataCont
                                     , TensorDataCont
                                     , TensorDataCont
                                     , MidasTensorDataContFreqShiftedTransformer
                                     , TensorDataCont
                                     , LinearEquationInitializer
                                     , FreqShiftedLinearStartGuesser
                                     , NoPreconditioner
                                     , FreqShiftedLinearResidualMaker
                                     , LinearConvergenceChecker
                                     , TensorDataContSpaceExpander
                                     , NonOrthoReducedNonHermitianFreqShiftedLinearSolver
                                     , TensorDataContFinalizer
                                     >
                        >;

using TensorDiagPreconFreqShiftedLinearEquationSolver = 
IterativeEquationSolver<LinearEquation<TensorDataCont
                                     , TensorDataCont
                                     , TensorDataCont
                                     , TensorDataCont
                                     , MidasTensorDataContFreqShiftedTransformer
                                     , TensorDataCont
                                     , LinearEquationInitializer
                                     , FreqShiftedLinearStartGuesser
                                     , MidasTensorFreqShiftedDiagonalPreconditioner
                                     , FreqShiftedLinearResidualMaker
                                     , LinearConvergenceChecker
                                     , TensorDataContSpaceExpander
                                     , NonOrthoReducedNonHermitianFreqShiftedLinearSolver
                                     , TensorDataContFinalizer
                                     >
                        >;

//using TensorIndividuallyFreqShiftedLinearEquationSolver = 
//IterativeEquationSolver<LinearEquation<TensorDataCont
//                                     , TensorDataCont
//                                     , TensorDataCont
//                                     , TensorDataCont
//                                     , MidasTensorDataContFreqShiftedTransformer
//                                     , TensorDataCont
//                                     , IndividuallyFreqShiftedLinearEquationInitializer
//                                     , IndividuallyFreqShiftedLinearStartGuesser
//                                     , NoPreconditioner
//                                     , IndividuallyFreqShiftedLinearResidualMaker    // Needs generalization!
//                                     , LinearConvergenceChecker
//                                     , TensorDataContSpaceExpander
//                                     , ReducedNonHermitianIndividuallyFreqShiftedLinearSolver
//                                     , NoFinalizer
//                                     >
//                        >;
//
//using TensorDiagPreconIndividuallyFreqShiftedLinearEquationSolver = 
//IterativeEquationSolver<LinearEquation<TensorDataCont
//                                     , TensorDataCont
//                                     , TensorDataCont
//                                     , TensorDataCont
//                                     , MidasTensorDataContFreqShiftedTransformer
//                                     , TensorDataCont
//                                     , IndividuallyFreqShiftedLinearEquationInitializer
//                                     , IndividuallyFreqShiftedLinearStartGuesser
//                                     , MidasTensorIndividuallyFreqShiftedDiagonalPreconditioner
//                                     , IndividuallyFreqShiftedLinearResidualMaker    // Needs generalization!
//                                     , LinearConvergenceChecker
//                                     , TensorDataContSpaceExpander
//                                     , ReducedNonHermitianIndividuallyFreqShiftedLinearSolver
//                                     , NoFinalizer
//                                     >
//                        >;

#include "precon/Interface_impl.h"

#endif /* LINEAREQUATIONINTERFACE_H_INCLUDED */
