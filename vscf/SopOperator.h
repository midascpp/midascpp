/**
************************************************************************
* 
* @file                SopOperator.h
*
* Created:             21-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for holding an sum over product operator
* 
* Last modified: man mar 21, 2005  11:23
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SOPOPERATOR_H
#define SOPOPERATOR_H

class SopOperator;
class ModePropOper;

class SopOperator
{
   private:
      In mNterms;                          ///< Number of terms 
      vector<Nb> mCoeffs;                  ///< Coefficient for the terms
      vector<ModeProdOper> mModeProdOperators;  ///< Operator definitions
   public:
      void GiveCoef(Nb aCoef, In i) {mCoeffs[i] = aCoef;}  ///< Give Coeff;
      void AddProductTerm(Nb aCoef, ModeProdOper aModeProdOper)
      {mCoeffs.push_back(aCoef); mModeProdOperators.push_back(aModeProdOper);}
                                                ///< Add new mode-oper factor 
      Nb Coef(In i) { return mCoeffs[i];} 
      ModeProdOper OperProd(In i) {return mModeProdOperators[i];} 
};

#endif

