/**
************************************************************************
* 
* @file                RamanData.cc
*
* Created:             11-9-2002
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing RamanData class methods.
* 
* ?????? Last modified: Mon Oct 31, 2005  11:46PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <iostream>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/OpInfo.h"

#include "vscf/RamanData.h"

using namespace std;

void RamanData::SetInitState(const In ai)
{
   mInitState = ai;
}

void RamanData::SetFinalState(const In af)
{
   mFinalState = af;
}

void RamanData::SetOmegaExt(const Nb aFreq)
{
   mOmegaExt = aFreq;
}

void RamanData::SetOmegafi(const Nb afi)
{
   mOmegafi = afi;
}

void RamanData::SetAlphaElement(const oper::PropertyType& aIndex, const Nb aValue)
{
   if       (oper::polarizability_xx == aIndex) mAlphaXX = aValue;
   else if  (oper::polarizability_yy == aIndex) mAlphaYY = aValue;
   else if  (oper::polarizability_zz == aIndex) mAlphaZZ = aValue;
   else if  (oper::polarizability_xy == aIndex) mAlphaXY = aValue;
   else if  (oper::polarizability_xz == aIndex) mAlphaXZ = aValue;
   else if  (oper::polarizability_yz == aIndex) mAlphaYZ = aValue;
   else
      Mout << "RamanData::SetAlphaElement() error: Bad index." << endl;
}

void RamanData::GetInvariants(Nb& a2, Nb& gamma2) const
{
   a2 = (mAlphaXX + mAlphaYY + mAlphaZZ) / I_3;
   a2 = a2*a2;
   
   gamma2 = ( pow(mAlphaXX-mAlphaYY, I_2) + pow(mAlphaYY-mAlphaZZ, I_2)
            + pow(mAlphaZZ-mAlphaXX, I_2) ) / I_2
            + ( pow(mAlphaXY, I_2) + pow(mAlphaXZ, I_2) + pow(mAlphaYZ, I_2) ) * I_3;
}

Nb RamanData::GetDepolRatio() const
{
   Nb a2, gamma2;

   GetInvariants(a2, gamma2);

   return 3*gamma2 / (45*a2 + 4*gamma2);
}

Nb RamanData::GetCrossSect() const
{
   Nb a2, gamma2;
   Nb k_omega = pow(C_ALPHA, 4);
   Nb omega_s = mOmegaExt - mOmegafi;
   
   GetInvariants(a2, gamma2);
   return k_omega * pow(omega_s, 4)
          * (45*a2 + 7*gamma2) / 45;
}

Nb RamanData::GetRamanActivity() const
{
   Nb a2, gamma2;
   GetInvariants(a2, gamma2);

   Nb act = 45*a2 + 7*gamma2;
   
   // This is to comply with e.g. Dalton in the Harmonic oscillator limit.
   act *= 2 * mOmegafi;

   return act;
}

ostream& operator<<(ostream& as, const RamanData& ar)
{
   Nb a2, gamma2;
   ar.GetInvariants(a2, gamma2);
   
   as.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(5, as);
   as << "Raman data for transition (atomic units): "
      << ar.mFinalState << "<-" << ar.mInitState << endl
      << "   omega_fi =  " << ar.mOmegafi << endl
      << "   omega_ext = " << ar.mOmegaExt << endl
      << "   omega_s =   " << ar.mOmegaExt - ar.mOmegafi << endl << endl
      << "                    "
      << ar.mAlphaXX << "  " << ar.mAlphaXY << "  " << ar.mAlphaXZ << endl
      << "   alpha =          "
      << ar.mAlphaXY << "  " << ar.mAlphaYY << "  " << ar.mAlphaYZ << endl
      << "                    "
      << ar.mAlphaXZ << "  " << ar.mAlphaYZ << "  " << ar.mAlphaZZ << endl << endl
      << "   Invariants:     a^2 = " << a2 << endl
      << "               gamma^2 = " << gamma2 << endl << endl;;
   as.setf(ios_base::fixed, ios_base::floatfield);
   midas::stream::ScopedPrecision(3, as);
   as << "   Depol. ratio:       (pi/2, |_i) =          " << ar.GetDepolRatio() << endl
      << "   Raman activity:                            "
      << ar.GetRamanActivity() << endl;
   as.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(5, as);
   as << "   Diff. cross. sect.: (pi/2, |_s+||s, |_i) = " << ar.GetCrossSect() << endl
      << endl;
   return as;
}
