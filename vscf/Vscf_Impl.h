/**
************************************************************************
* 
* @file    Vscf_Impl.h
*
* @date    26-01-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of VSCF template functions.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef VSCF_IMPL_H_INCLUDED
#define VSCF_IMPL_H_INCLUDED

#include "vscf/Vscf.h"
#include "vcc/TensorDataCont.h"
#include "input/Input.h"
#include "tensor/TensorDecomposer.h"

using namespace midas;

/**
 *
 **/
template
   <  class SOLVER
   >
auto Vscf::RunComplexRspEqSolve
   (  std::vector<ComplexVector<DataCont,ZeroVector<> > >& y
   ,  MidasComplexFreqShiftedTransformer& midas_trf
   ,  const std::string& arKey
   ) 
   -> typename std::remove_reference<decltype(std::declval<SOLVER>().Solution())>::type
{
   SOLVER ies(midas_trf,y);

   InitializeLinearSolverFromRspCalcDef(ies, mpVscfCalcDef);
   ies.SetSaveToDisc(mpVscfCalcDef->ItEqSaveToDisc());
   ies.CheckLinearDependence() = mpVscfCalcDef->RspItEqCheckLinearDependenceOfSpace();
   ies.RelativeConvergence() = mpVscfCalcDef->RspRelConv();
   
   ies.AddRestart(mpVscfCalcDef->GetName()+arKey);
   for(auto it = mpVscfCalcDef->ItEqRestartVector().begin(); it!=mpVscfCalcDef->ItEqRestartVector().end(); ++it)
   {
      ies.AddRestart(*it);
   }
   
   if(!ies.Solve())
   {
      MidasWarning("Complex Linear Solver not converged! :(");
   }

   auto result = ies.Solution();

   return result;
}

/**
 *
 **/
template
   <  class SOLVER
   >
void Vscf::TensorRspEigSolve
   (  SOLVER& aSolver
   ,  bool aRight
   )
{
   // Add restarts
   std::string key = std::string("_rsp_eigvec_") + std::string(aRight ? "":"left_");
   for(const auto& restart : this->mpVscfCalcDef->ItEqRestartVector())
   {
      auto restart_key = restart + key + "tensor";
      Mout  << " Try to add restart vector with basename: " << restart_key << std::endl;
      aSolver.AddRestart(restart_key);
   }

   // Solve the equations
   Mout << " Solving eigenvalue equations..." << std::endl;
   bool initial_conv = true;
   if (  !aSolver.Solve()
      )
   {
      initial_conv = false;
      Mout << " Tensor rsp eig solver not converged :( " << std::endl;
      MidasWarning("Tensor rsp eig solver not converged");
   }

   // Get eigvals
   auto eigvals = aSolver.GetEigenvalues();
   bool update_eigvals = true; //false;

   // Copy eigvecs and clear solver
   bool reiterate = this->mpVscfCalcDef->ReiterateTensorRspSolver();
   bool recompress_eigvecs = this->mpVscfCalcDef->RecompressCpvccEigVecs();
   size_t max_reiter = 10;
   size_t n_reiter = 0;
   auto eigvecs = aSolver.Solution();
   aSolver.ClearSolver();
   decltype(eigvecs) recomp_eigvecs;

   // Repeat solving equations until the recompressed solution vectors are still solutions
   const auto& decompinfo = this->mpVscfCalcDef->GetVccRspDecompInfo();
   const std::string eigvec_filename = mpVscfCalcDef->GetName() + key + "tensor";

   // Reference to the eigenvectors that are replaced during recompression
   auto& aux_eigvecs =  reiterate || decompinfo.empty() || !recompress_eigvecs
                     ?  eigvecs
                     :  recomp_eigvecs;

   if (  !decompinfo.empty()
      && recompress_eigvecs
      )
   {
      // Initialize recompressed solution
      aux_eigvecs = eigvecs;

      midas::tensor::TensorDecomposer decomposer(decompinfo);
      Nb decomp_thresh = this->mpVscfCalcDef->GetRspItEqResidThr() * C_I_10_1;
      std::vector<bool> conv(eigvecs.size(), false);
      while (  true
            )
      {
         Mout  << " Recompress eigenvectors to T_CP = " << decomp_thresh << std::endl;

         // Recompress solution vectors
         for(In i=0; i<aux_eigvecs.size(); ++i)
         {
            if (  !conv[i]
               )
            {
               Mout  << "    Recompress eigvec " << i << std::endl;
               for(In itens=I_0; itens<aux_eigvecs[i].Size(); ++itens)
               {
                  aux_eigvecs[i].GetModeCombiData(itens) = decomposer.Decompose(eigvecs[i].GetModeCombiData(itens), decomp_thresh);
               }
            }
         }

         // Write decomposed solution to disk
         for(In i=0; i<aux_eigvecs.size(); ++i)
         {
            const std::string eigvec_filename_i = eigvec_filename + "_" + std::to_string(i);
            aux_eigvecs[i].WriteToDisc(eigvec_filename_i, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
         }

         // If the first calculation did not converge, just break...
         if (  !initial_conv
            )
         {
            Mout  << " Initial eigenvalue equations not converged. Write recompressed eigvecs to disk without further checks!" << std::endl;
            MidasWarning("Initial eigenvalue equations not converged. Write recompressed eigvecs to disk without further checks!");
            break;
         }

         if (  reiterate
            )
         {
            // Read in recompressed eigvecs and solve again
            Mout  << " Read in recompressed eigvecs and solve again." << std::endl;
            aSolver.ClearSolver();
            aSolver.ClearRestart();
            aSolver.ResetTargeter();
            aSolver.AddRestart(eigvec_filename);

            if (  !aSolver.Solve()
               )
            {
               Mout << " Tensor rsp eig solver not converged :( " << std::endl;
               MidasWarning("Tensor rsp eig solver not converged");
               break;
            }

            // Set new eigenvectors
            eigvecs = aSolver.Solution();

            if (  update_eigvals
               )
            {
               std::swap(eigvals, aSolver.GetEigenvalues());
            }

            if (  aSolver.ItNum() != 0
               )
            {
               if (  n_reiter > max_reiter
                  )
               {
                  Mout  << " CP-VCC rsp eigenvalue equations not converged! T_CP = " << decomp_thresh << ", n_reiter = " << n_reiter << std::endl;
                  MidasWarning("CP-VCC rsp eigenvalue equations not converged!");
                  break;
               }
               else
               {
                  Mout  << " Solution check failed. Reiterate!" << std::endl;
                  ++n_reiter;
               }
            }
            else
            {
               Mout  << " Eigenvector check complete!" << std::endl;
               break;
            }
         }
         else
         {
            // Check convergence of recompressed solvec
            Mout  << " Check recompressed eigenvectors." << std::endl;
            for(In i=I_0; i<aux_eigvecs.size(); ++i)
            {
               if (  !conv[i]
                  )
               {
                  Mout  << "    Check vector " << i << std::endl;
                  
                  // Needs fix
                  if (  eigvals.Im()[i]
                     )
                  {
                     MIDASERROR("Need to add imag part of complex eigvec pair!");
                  }

                  conv[i] = aSolver.CheckConvergedSolution(aux_eigvecs[i], eigvals.Re()[i], eigvals.Im()[i]);
               }
            }
            bool allconv = true;
            for(const auto& c : conv)
            {
               if (  !c
                  )
               {
                  allconv = false;
                  break;
               }
            }
            if (  allconv
               || decomp_thresh < C_I_10_14
               )
            {
               if (  !allconv
                  )
               {
                  MidasWarning("Eigenvectors recompressed to T_CP = " + std::to_string(decomp_thresh) + " is no longer a solution!");
               }
               Mout  << " All converged!" << std::endl;
               break;
            }
            else
            {
               Mout  << " Recompression not accurate enough. Increase accuracy!" << std::endl;
               decomp_thresh *= C_I_10_1;
            }
         }
      }
   }
   else
   {
      // Write solution to disk
      for(In i=0; i<eigvecs.size(); ++i)
      {
         const std::string eigvec_filename_i = eigvec_filename + "_" + std::to_string(i);

         // If MPI, all ranks write same vector. This is how the DataCont implementation works, but we could consider switching to only reading/writing from master. 
         // This requires Bcasts, however!
         eigvecs[i].WriteToDisc(eigvec_filename_i, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
      }
   }

   // Write eigenvalues to disk
   {
      const std::string eigval_re_filename = mpVscfCalcDef->GetName() + "_rsp_eigval_re" + (aRight? "":"_left") + "_tensor";
      const std::string eigval_im_filename = mpVscfCalcDef->GetName() + "_rsp_eigval_im" + (aRight? "":"_left") + "_tensor";
      
      DataCont eigval_re(eigvals.Re(), "InMem", eigval_re_filename, true);
      DataCont eigval_im(eigvals.Im(), "InMem", eigval_im_filename, true);
   }

   // Analysis
   if (  !aux_eigvecs.back().CheckOverflow()
      )
   {
      // Convert solution vector to DataCont
      auto size = eigvecs.size();
      std::vector<DataCont> dc_solvec;
      dc_solvec.reserve(size);
      for(size_t i=0; i<size; ++i)
      {
         dc_solvec.emplace_back(DataContFromTensorDataCont(aux_eigvecs[i]));
      }

      RspEigOutput(eigvals.Re(), dc_solvec);
   }
}


/**
* Drive calculation of response functions.
* */
template
   <  class VEC_T
   >
void Vscf::RspFuncDrv()
{
   // Template stuff
   constexpr bool is_tensor = std::is_same_v<TensorDataCont, VEC_T>;
   constexpr bool is_dc = std::is_same_v<DataCont, VEC_T>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   Out72Char(Mout,' ','*','*');
   Mout << "\n\n Begin Response Function Calculation\n\n"  << endl;

   if (mpVscfCalcDef->RspIoLevel()>I_5) 
      Mout << " In RspFuncDrv; find response functions" << endl;
   Mout << " Calculate " << mpVscfCalcDef->NrspFuncs() << " response functions." << endl;

   if (  !mpVscfCalcDef->Variational()
      )
   {
      if constexpr   (  is_dc
                     )
      {
         this->CalculateEta0(); 
      }
      else if constexpr (  is_tensor
                        )
      {
         this->CalculateTensorEta0(); 
      }
      else // will not get here due to assertion
      {
         auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
         MIDASERROR("Vscf::RspFuncDrv not implemented for type: " + type);
      }
   }
   this->PrepareRspOpers<VEC_T>();    // Prepare the response operators
   this->FindRspEqToSolve<VEC_T>();   // Find the response equations to solve 
   this->RspEqSolveDrv<VEC_T>();      // Solve the response equations

   if (  mpVscfCalcDef->GetNumVccF() != C_0
      )
   {
      if constexpr   (  is_tensor
                     )
      {
         MIDASERROR("Numerical VCC F transform not implemented for TensorDataCont!");
      }
      else
      {
         this->CalcNumVccF();
      }
   }
   
   this->RspContract<VEC_T>(); // Contract response vectors with vectors to get final response functions
   this->PertTheoryZPVC();     // Construct the PT ZPVC to the response operators

   Mout << "\n\n Results for response functions " << std::endl;
   Out72Char(Mout,'+','-','+');
   for(In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp) 
   {
      Mout << "Order: " << mpVscfCalcDef->GetRspFunc(i_rsp).GetNorder() << " ";
      mpVscfCalcDef->RspFuncOut(i_rsp);
   }
   Out72Char(Mout,'+','-','+');

   this->mpVscfCalcDef->PrintRspToFile();

   Mout << "\n\n End of Response Function Calculation \n\n"  << std::endl;

   std::vector<RspFunc> sos_rsp_funcs;
   if (  mpVscfCalcDef->GetSosRsp()
      )
   {
      // If there are excited states calculated, use potentially info 
      if (  mpVscfCalcDef->GetRspNeig() > I_0
         )
      {
         this->SosRspFunc<VEC_T>(sos_rsp_funcs);

         // Where to find eigenvalues...
         std::string eig_key = is_tensor ? "_rsp_eigval_re_tensor" : "_rsp_eigval";

         this->DoIR(eig_key);
         this->DoRaman(eig_key);
      }
      Mout << "N SOS RF = " << sos_rsp_funcs.size() << std::endl;

      // add the SOS response functions to the total set of
      // response functions
      for(In i=0; i<sos_rsp_funcs.size(); ++i)
      {
         mpVscfCalcDef->AddRspFunc(sos_rsp_funcs[i]);
      }
   }

   // mbh: if some approximate frequency dependent response functions
   // has been requested, calculate them here
   // Mout << "NOW DO " << mpVscfCalcDef->NApproxRspFunc() << " APPROX RF" << endl;
   if (  mpVscfCalcDef->NApproxRspFunc() > I_0
      ) 
   {
      Mout << "\n\n Results for APPROXIMATE(!) response functions " << std::endl;
      Out72Char(Mout,'+','-','+');
      for(In iar=I_0; iar<mpVscfCalcDef->NrspFuncs(); ++iar) 
      {
         if (  !mpVscfCalcDef->GetRspFunc(iar).GetApproxRspFunc()
            )
         {
            continue;
         }

         // Calculate f-function
         for(In sos=I_0; sos<sos_rsp_funcs.size(); ++sos) 
         {
            // Calculate the F function.
            if (  sos_rsp_funcs[sos] == mpVscfCalcDef->GetRspFunc(iar)
               ) 
            {
               // Get static part
               Nb f_static = sos_rsp_funcs[sos].Value();

               // Now get all frq. dep. parts!
               for(In sos2=I_0; sos2<sos_rsp_funcs.size(); ++sos2) 
               {
                  bool match = true;
                  if (  sos_rsp_funcs[sos2].GetNopers() != mpVscfCalcDef->GetRspFunc(iar).GetNopers()
                     )
                  {
                     continue;
                  }

                  for(In op=I_0; op<sos_rsp_funcs[sos].GetNopers(); ++op) 
                  {
                     if (  sos_rsp_funcs[sos].GetRspOp(op) != sos_rsp_funcs[sos2].GetRspOp(op)
                        )
                     {
                        match=false;
                     }
                  }

                  if (  match
                     ) 
                  {
                     // we have a match! Print result
                     Nb f_dynamic = sos_rsp_funcs[sos2].Value();
                     Nb f_func = f_dynamic/f_static;
                     Mout << "F FUNCTION = " << f_func << " , " 
                          << sos_rsp_funcs[sos2].GetRspFrq(I_1) << " , #";
                     for(In oper=I_0; oper<sos_rsp_funcs[sos2].GetNopers(); ++oper)
                     {
                        Mout << sos_rsp_funcs[sos2].GetRspOp(oper);
                     }
                     Mout << "# , " << mpVscfCalcDef->GetRspFunc(iar).Value()*f_func << std::endl;
                     RspFunc rsp_out = sos_rsp_funcs[sos2];
                     Nb value = mpVscfCalcDef->GetRspFunc(iar).Value()*f_func;
                     rsp_out.SetValue(value);
                     Mout << rsp_out << std::endl;
                  }
               }
            }
         }
      }
      Out72Char(Mout,'+','-','+');
   }

   // Do density-matrix analysis
   if (  mpVscfCalcDef->DoDensityMatrixAnalysis()
      )
   {
      if constexpr   (  is_tensor
                     )
      {
         MIDASERROR("Density-matrix analysis not implemented for TensorDataCont!");
      }
      else
      {
         this->DensityMatrixAnalysis(); 
      }
   } 

   Out72Char(Mout,' ','*','*');
}

/**
* Prepare the response operators
* */
template
   <  class VEC_T
   >
void Vscf::PrepareRspOpers()
{
   constexpr bool is_tensor = std::is_same_v<TensorDataCont, VEC_T>;
   constexpr bool is_dc = std::is_same_v<DataCont, VEC_T>;

   // Loop through response functions and extract operator names.
   std::set<std::string> rsp_ops;
   In first_order=I_0;
   for (In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      In n_order = (mpVscfCalcDef->GetRspFunc(i_rsp)).GetNorder();
      if (  n_order == I_1
         )
      {
         ++first_order;
      }
      for(In i_ord=I_0; i_ord<std::abs(n_order%I_10); ++i_ord)
      {
         rsp_ops.insert((mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(i_ord));
      }
   }

   // List operator names.
   In n_rsp_ops = rsp_ops.size();
   mpVscfCalcDef->SetNrspOps(n_rsp_ops);
   Mout << "\n There are " << n_rsp_ops << " different response operators:" << std::endl;
   for(const auto& op : rsp_ops)
   {
         Mout << "    Oper = " << op << std::endl;
   }

   In not_found = I_0;
   In i_c = I_0;
   for (const auto& op : rsp_ops)
   {
      // Find operator number
      std::string oper_name = op;
      In i_oper = gOperatorDefs.GetOperatorNr(oper_name);
      mpVscfCalcDef->AddRspOp(oper_name, i_c);
  
      // Check that non-energy operators does not contain kinetic energy operator terms
      if (  gOperatorDefs[i_oper].GetOperWithKeoTerms()
         && !  OpDef::msOpInfo[oper_name].GetType().Is(oper::energy)
         )
      {
         MIDASERROR("Kinetic energy operator terms detected in non-energy operator " + oper_name + ", please investigate");
      }
      
      if (  i_oper > -I_1
         )
      {
         // Calculate rhs response intermediate vectors 
         std::string s1 = "_eta_vec_"; 
         if (  !mpVscfCalcDef->Variational()
            )
         {
            s1 = "_xi_vec_";
         }
         std::string s = mpVscfCalcDef->GetName() + s1 + oper_name;
         Nb expt_val = C_0;

         // Initialize pointer to vector
         std::unique_ptr<VEC_T> eta_vec = nullptr;

         // If DataCont
         if constexpr   (  is_dc
                        )
         {
            eta_vec = std::make_unique<DataCont>(NrspPar(), C_0, "OnDisc", s, true); 
         }
         // If TensorDataCont
         else if constexpr (  is_tensor
                           )
         {
            // Niels: We do not have the necessary info (TensorDecompInfo) available to initialize the TensorDataCont here
            // This will be done in the CalculateRhs function
            eta_vec = std::make_unique<TensorDataCont>();
         }
         else
         {
            auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
            MIDASERROR("Vscf::PrepareRspOpers not implemented for type: " + type);
         }

         // Perform calculation
         this->CalculateRhs(*eta_vec, oper_name, i_oper, expt_val);

         mpVscfCalcDef->AddRspOpExptVal(expt_val, i_c);

         // Write TensorDataCont solution
         if constexpr   (  is_tensor
                        )
         {
            eta_vec->WriteToDisc(s + "_tensor", midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
         }
      }
      else 
      {
         ++not_found;
         Mout << " Operator " << oper_name << " NOT FOUND." << std::endl;
      }

      ++i_c;
   }

   // Catch error
   if (  not_found > 0
      )
   {
      MIDASERROR("Some response operators have not been defined.");
   }
}

/**
*  Drive solution of response equations AND construction of various vectors. 
*
*  Templated for DataCont and TensorDataCont
* */
template
   <  class VEC_T
   >
void Vscf::RspEqSolveDrv()
{
   this->SolveZerothOrderEq<VEC_T>();    // Solve N_order = 0 equations
   this->SolveFirstOrderEq<VEC_T>();     // Solve all N_order = 1 standard equations
   this->SolveVccMEq<VEC_T>();           // Solve for VCC M vector.
   this->SolveXToXTransitions<VEC_T>();  // solve all N_order = -11 equations
   this->CreateSigmaVectors<VEC_T>();    // Create sigma intermediates for use in quad. rsp.
   this->SolveSecondOrderEq<VEC_T>();    // Solve all N_order = 2 equations
   this->CalcNonVarEtaX<VEC_T>();        // For non-variational methods, calcluate etaX vectors if necessary.
}

/**
*  Solve all zeroth-order equations
* */
template
   <  class VEC_T
   >
void Vscf::SolveZerothOrderEq() 
{
   constexpr bool is_tensor = std::is_same_v<TensorDataCont, VEC_T>;
   constexpr bool is_dc = std::is_same_v<DataCont, VEC_T>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   // Zeoth order left equations , THERE SHOULD BE AT MOST ONE
   In i_0l_begin=I_0;
   std::vector<In> n_0l;
   In n_eqs_tot = mpVscfCalcDef->NrspVecs();
   for(In i=I_0; i<n_eqs_tot; ++i)
   {
      if (  mpVscfCalcDef->GetRspVecInf(i).GetNorder() < I_0
         ) 
      {
         ++i_0l_begin;
      }
      else 
      {
         if (  mpVscfCalcDef->GetRspVecInf(i).GetNorder() != I_0
            || mpVscfCalcDef->GetRspVecInf(i).GetRight()
            ) 
         {
            break;
         }
         n_0l.push_back(i);
      }
   }
   
   if (  n_0l.size() > I_0
      )  
   {
      Mout << " Solve zeroth-order multiplier equations " << std::endl;
      
      if (  n_0l.size() > I_1
         )
      {
         MIDASERROR(" Surprised to find more than one left 0-order vector."); 
      }

      // Solve the actual equations for t-bar. 
      std::string seta = mpVscfCalcDef->GetName() + "_eta0_vec"; 
      std::string key = "_mul0_vec_";
      bool right = false;

      if constexpr   (  is_dc
                     )
      {
         MidasVector frq_vec(I_1,C_0);
         MidasVector gamma_vec(I_1,C_0);
         std::vector<DataCont> rsp_rhss(I_1);
         {
            DataCont eta0; 
            eta0.GetFromExistingOnDisc(NrspPar(), seta);
            eta0.SaveUponDecon(); 
            rsp_rhss[I_0]=eta0; 
            rsp_rhss[I_0].Scale(C_M_1);  // Right hand side is -eta0.
         }

         RspEqSolve(I_1,rsp_rhss,frq_vec, key, gamma_vec, right);
      }
      else if constexpr (  is_tensor
                        )
      {
         // Read RHS and scale by -1
         std::vector<TensorDataCont> rhs(1);
         rhs[0].ReadFromDisc(seta + "_tensor");
         rhs[0].Scale(-C_1);

         // Set up frequency vectors (not used here)
         MidasVector frq_vec(I_1,C_0);
         MidasVector gamma_vec(I_1,C_0);

         // Solve equations
         auto tensor_key = key + "tensor";
         this->TensorRspEqSolve(rhs, tensor_key, frq_vec, gamma_vec, right);
      }
      else
      {
         auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
         MIDASERROR("Vscf::SolveZerothOrderEq not implemented for type: " + type);
      }

      // Set evaluation flag
      mpVscfCalcDef->GetRspVecInf(i_0l_begin).SetHasBeenEval(true);
   }
}

/**
*  Solve all first order equations
**/
template
   <  class VEC_T
   >
void Vscf::SolveFirstOrderEq() 
{
   constexpr bool is_tensor = std::is_same_v<TensorDataCont, VEC_T>;
   constexpr bool is_dc = std::is_same_v<DataCont, VEC_T>;

   In n_eqs_tot = mpVscfCalcDef->NrspVecs();

   // Set up storage mode for DataCont version
   std::string storage_mode = "OnDisc";
   if (  mpVscfCalcDef->VecStorage()==I_0
      )
   {
      storage_mode = "InMem";
   }

   // First order right equations of standard type.
   std::vector<In> n_1r;
   std::vector<In> asym_1r;
   for(In i=I_0; i<n_eqs_tot; ++i)
   {
      const auto& vecinf = mpVscfCalcDef->GetRspVecInf(i);
      if (  vecinf.GetNorder() == I_1
         && vecinf.GetRight()
         && !vecinf.IsNonStandardType()
         ) 
      {
         if (  vecinf.GetRspOp(I_0).find("ANTI") != string::npos
            && mpVscfCalcDef->GetAlsoLinearAsym()
            ) 
         {
            asym_1r.push_back(i);
         }
         else 
         {
            n_1r.push_back(i);
         }
      }
   }
   std::sort(n_1r.begin(), n_1r.end());   
      
   if (  n_1r.size() == 0
      )
   {
      return;
   }

   // Find right hand sides 
   Mout << " Indices of first-order right parameter equations: " << n_1r << std::endl;
   std::set<std::string> set_of_rhs;
   for(In i=I_0; i<n_1r.size(); ++i)
   { 
      In i_r=n_1r[i];
   
      if (  gDebug
         )
      {
         Mout << " Solving for vector: " << mpVscfCalcDef->GetRspVecInf(i_r) << std::endl;
      }
      
      // Find rhs..
      if (  mpVscfCalcDef->GetRspVecInf(i_r).GetNorder() == I_1
         )
      {
         if (  gDebug
            )
         {
            Mout << " requires RHS with operator " << mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0) << std::endl;
         }

         set_of_rhs.insert(mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0));
      }
   }
   
   // Create right hand sides of 1st ord. eq.
   Mout << " There are " << set_of_rhs.size() << " required rhs vectors " << std::endl;
   std::vector<VEC_T> rsp_rhss;
   rsp_rhss.reserve(mpVscfCalcDef->GetRspRedDimMax());
   MidasVector frq_vec(n_1r.size(), C_0);
   MidasVector gamma_vec(n_1r.size(), C_0);

   for(const auto& oper : set_of_rhs)
   {
      for(In i_eq=I_0; i_eq<n_1r.size(); ++i_eq)
      { 
         In i = n_1r[i_eq];
         if (  mpVscfCalcDef->GetRspVecInf(i).GetRspOp(I_0) == oper
            )
         {
            // Set frequency and gamma for i_eq
            frq_vec[i_eq] = mpVscfCalcDef->GetRspVecInf(i).GetRspFrq(I_0);
            gamma_vec[i_eq] = mpVscfCalcDef->GetRspVecInf(i).Gamma();
            
            Mout << " pairing frq: " << frq_vec[i_eq] << " with gamma: " << gamma_vec[i_eq] << std::endl;

            // Get precalculated RHS vector form disc.
            // For VSCF: Actually rhs = S^2 g = -(eta_i, eta_i)
            //           And eta_i contains plus and minus eta_i in it.
            // For VCI:  Eta vector.
            // For VCC:  Xi vector.
            std::string s1 = mpVscfCalcDef->Variational() ? "_eta_vec_" : "_xi_vec_"; 
            std::string vecname = mpVscfCalcDef->GetName() + s1 + oper;

            // Niels: What is this one used for???
            std::string s2 = mpVscfCalcDef->GetName() + "_rsp_rhs_vec_";

            if constexpr   (  is_dc
                           )
            {
               s2 += std::to_string(i_eq); 
               DataCont vec;
               vec.GetFromExistingOnDisc(NrspPar(),vecname);
               vec.SaveUponDecon(true);
               rsp_rhss.push_back(DataCont());
               rsp_rhss[i_eq].NewLabel(s2);
               rsp_rhss[i_eq] = vec; 

               In n_scale = NrspPar();
               if (!mpVscfCalcDef->DoNoPaired()) n_scale /= I_2;
               rsp_rhss[i_eq].Scale(C_M_1,I_0,n_scale);
               rsp_rhss[i_eq].ChangeStorageTo(storage_mode);
               rsp_rhss[i_eq].SaveUponDecon(false);
            }
            else if constexpr (  is_tensor
                              )
            {
               TensorDataCont tdc_vec;
               tdc_vec.ReadFromDisc(vecname + "_tensor");
               rsp_rhss.emplace_back(tdc_vec);

               if (  !mpVscfCalcDef->DoNoPaired()
                  )
               {
                  MIDASERROR("This should only happen for VSCF response, which is not implemented for TensorDataCont!");
               }

               rsp_rhss[i_eq].Scale(C_M_1);

               // Save RHS
               s2 += "tensor_" + std::to_string(i_eq); 
               rsp_rhss[i_eq].WriteToDisc(s2, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
            }
         }
      }
   }
   
   // Solve 1-order parameter equations 
   if (  mpVscfCalcDef->IoLevel() > I_10
      )
   {
      Mout << " frequency vector " << frq_vec << endl;
   }
   
   // DataCont version
   if constexpr   (  is_dc
                  )
   {
      this->RspEqSolve(n_1r.size(),rsp_rhss,frq_vec,"_p1rsp_vec_",gamma_vec);
   }
   // TensorDataCont version
   else if constexpr (  is_tensor
                     )
   {
      this->TensorRspEqSolve(rsp_rhss, "_p1rsp_vec_tensor", frq_vec, gamma_vec);
   }
   // Error
   else
   {
      auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
      MIDASERROR("Vscf::SolveFirstOrderEq not implemented for type: " + type);
   }

   In i_new_rsp = n_1r.size();
   for(In i=I_0; i<n_1r.size(); ++i) 
   {
      // Set evaluation flag
      In i_r = n_1r[i];
      this->mpVscfCalcDef->GetRspVecInf(i_r).SetHasBeenEval(true);

      // Check if we need to calculate asymmetric response vectors
      if (  !mpVscfCalcDef->GetAlsoLinearAsym()
         )
      {
         continue;
      }

      /* mbh: at this point we can also set the asymmetric response vectors, they are simply
      *  related to the symmetric ones via a phase factor! (Only in the case of an approximate
      *  electronic asym. linear response function)         
      * 
      *  find out whether the current response vector should be copied to an asym one*/
      std::string new_name;
      bool found_oper = false;
      In j_r = I_0;
      Nb el_asym_frq = C_0, el_sym_frq = C_0;
      for(In j=I_0; j<asym_1r.size(); ++j) 
      {
         j_r=asym_1r[j];
         std::string test_str = this->mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0);
         test_str.erase(test_str.find("_ANTI"), 5);
         const auto& op_info_sym  = OpDef::msOpInfo[mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0)];
         const auto& op_info_asym = OpDef::msOpInfo[mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0)];
         el_sym_frq  = op_info_sym .GetFrq(0);
         el_asym_frq = op_info_asym.GetFrq(0);
         if (  mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0) == test_str
            && mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(I_0) == mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(I_0)
            && el_asym_frq == el_sym_frq
            )
         {
            if (  gDebug
               )
            {
               Mout << "Adding Rsp. vec. i = " << i_new_rsp << " j_r = " << j_r << std::endl;
            }
            
            // Set new_name
            if constexpr   (  is_dc
                           )
            {
               new_name = this->mpVscfCalcDef->GetName()+"_p1rsp_vec_"+std::to_string(i_new_rsp);
            }
            else if constexpr (  is_tensor
                              )
            {
               new_name = this->mpVscfCalcDef->GetName()+"_p1rsp_vec_tensor_"+std::to_string(i_new_rsp);
            }
            // Niels: If none of these types, this function will have failed already...

            ++i_new_rsp;
            this->mpVscfCalcDef->GetRspVecInf(j_r).SetHasBeenEval(true);
            found_oper=true;
         }
         if (  found_oper
            )
         {
            break;
         }
      }
      if (  !found_oper
         )
      {
         continue;
      }

      // Find scaling factor for asym rsp vec
      Nb scale_fac=C_0;
      if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("X") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
         )
      {
         // XX, XY, or XZ: if it is Z, we just take that
         // otherwise we go with X
         if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Z") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
            )
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(2);
         }
         else
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(0);
         }
      }
      else if  (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Y") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
               )
      {
         // YY, or YZ: if it is Z, we just take that
         // otherwise we go with X
         if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Z") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
            )
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(2);
         }
         else
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(1);
         }
      }
      else
      {
         scale_fac = mpVscfCalcDef->GetAsymScale(2);
      }

      // Set final scaling factor
      scale_fac=(-el_asym_frq/scale_fac);

      if (  gDebug
         )
      {
         Mout  << "Scale fac for: " << mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0)
               << " is: " << scale_fac << std::endl;
      }

      std::string base_name = this->mpVscfCalcDef->GetName()+"_p1rsp_vec_";

      if constexpr   (  is_dc
                     )
      {
         std::string name = this->mpVscfCalcDef->GetName()+"_p1rsp_vec_"+std::to_string(i);
         DataCont rsp_vec;
         rsp_vec.GetFromExistingOnDisc(NrspPar(), name);
         rsp_vec.SaveUponDecon(true);
         DataCont new_rsp_vec;
         new_rsp_vec.SetNewSize(NrspPar());
         new_rsp_vec.NewLabel(new_name);
         new_rsp_vec.SaveUponDecon(true);
         new_rsp_vec=rsp_vec;
         new_rsp_vec.Scale(scale_fac);
      }
      else if constexpr (  is_tensor
                        )
      {
         TensorDataCont rsp_vec;
         rsp_vec.ReadFromDisc(base_name + "tensor_" + std::to_string(i));
         rsp_vec.Scale(scale_fac);
         rsp_vec.WriteToDisc(new_name, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
      }

   }

   // Now move the response vectors such that they correspond to the
   // number they have in the RspVecInf set.
   if (  mpVscfCalcDef->GetAlsoLinearAsym()
      ) 
   {
      In i_1r_begin=n_1r[I_0];
      // first move sym. to name_tmp_1, etc...
      for (In i=I_0; i<n_1r.size(); ++i) 
      {
         std::string sym_new_name=mpVscfCalcDef->GetName() + "_p1rsp_vec_" + (is_tensor ? "tensor_" : "") + std::to_string(i) + "_tmp";
         std::string name = mpVscfCalcDef->GetName() + "_p1rsp_vec_" + (is_tensor ? "tensor_" : "") + std::to_string(i);

         // Remove old file if it exists
         if (  InquireFile(sym_new_name)
            )
         {
            std::remove(sym_new_name.c_str());
         }

         std::rename(name.c_str(), sym_new_name.c_str());

//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(sym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving sym: " << i << " to tmp..." << std::endl;
         }
      }
      // Move all the asym. vectors
      for(In i=I_0; i<asym_1r.size(); ++i) 
      {
         In j_r=asym_1r[i];
         std::string asym_new_name = mpVscfCalcDef->GetName()+"_p1rsp_vec_" + (is_tensor ? "tensor_" : "")+std::to_string(j_r-i_1r_begin);
         std::string name = mpVscfCalcDef->GetName()+"_p1rsp_vec_" + (is_tensor ? "tensor_" : "")+std::to_string(i+n_1r.size());

         // Remove old file if it exists
         if (  InquireFile(asym_new_name)
            )
         {
            std::remove(asym_new_name.c_str());
         }

         std::rename(name.c_str(), asym_new_name.c_str());
//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(asym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving asym: " << i+n_1r.size() << " to: " << j_r-i_1r_begin << std::endl;
         }
      }
      // Now move all the sym vectors
      for(In i=I_0; i<n_1r.size(); ++i) 
      {
         In j_r=n_1r[i];
         std::string sym_new_name=mpVscfCalcDef->GetName()+"_p1rsp_vec_" + (is_tensor ? "tensor_" : "")+std::to_string(j_r-i_1r_begin);
         std::string name=mpVscfCalcDef->GetName()+"_p1rsp_vec_" + (is_tensor ? "tensor_" : "")+std::to_string(i)+"_tmp";

         // Remove old file if it exists
         if (  InquireFile(sym_new_name)
            )
         {
            std::remove(sym_new_name.c_str());
         }

         std::rename(name.c_str(), sym_new_name.c_str());

//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(sym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving sym: " << i << "_tmp to: " << j_r-i_1r_begin << std::endl;
         }
      }
   }
   Mout << " Solved all the needed response equations I could." << std::endl;
}


/**
 * Solve driver for different solver types (using the it_solver framework)
 *
 *    @param aSolver       The solver
 *    @param aRestartKey   Name extension for restart vector
 **/
template
   <  class SOLVER
   >
void Vscf::TensorRspEqSolveImpl
   (  SOLVER& aSolver
   ,  const std::string& aRestartKey
   )
{
   // Set lambda threshold if we are solving 0th-order multipliers
   if (  aRestartKey == "_mul0_vec_tensor"
      )
   {
      Mout  << " Set lambda threshold for TensorDataCont linear solver." << std::endl;

      aSolver.SetResidualEpsilon(this->mpVscfCalcDef->LambdaThreshold());
      aSolver.SetResidualEpsilonRel(this->mpVscfCalcDef->LambdaThreshold());
   }

   // Add restarts
   for(const auto& restart : this->mpVscfCalcDef->ItEqRestartVector())
   {
      auto restart_name = restart + aRestartKey;

      Mout  << " Try to add restart vector with basename: " << restart_name << std::endl;
      aSolver.AddRestart(restart_name);
   }

   // Set threshold for trial-space recompression
   // This must be more accurate than in the eigenvalue case!
//   auto& solver_decompinfo = aSolver.DecompInfo();
//   if (  !solver_decompinfo.empty()
//      )
//   {
//      auto thresh = midas::tensor::GetDecompThreshold(solver_decompinfo);
//      auto new_thresh = std::min(thresh, std::min(aSolver.ResidualEpsilon(), aSolver.ResidualEpsilonRel()) * C_I_10_1);
//
//      Mout  << " Linear response equations: Set new threshold for trial-space recompression.\n"
//            << "    old thresh:  " << thresh << "\n"
//            << "    new thresh:  " << new_thresh << "\n"
//            << std::flush;
//
//      midas::tensor::UpdateDecompThresholds(solver_decompinfo, new_thresh);
//   }

   // Solve the equations
   Mout << " Solving linear equations..." << std::endl;
   bool initial_conv = true;
   if (  !aSolver.Solve()
      )
   {
      initial_conv = false;
      Mout << " Tensor rsp solver not converged :( " << std::endl;
      MidasWarning("Tensor rsp solver not converged");
   }

   // Make copy of solution, and clear solver.
   bool reiterate = this->mpVscfCalcDef->ReiterateTensorRspSolver();
   bool reiter_increase_acc = true;
   size_t max_reiter = 10;
   size_t n_reiter = 0;
   auto solution = aSolver.Solution();
   aSolver.ClearSolver();
   std::vector<Nb> sol_norms(solution.size());
   for(size_t isol=0; isol<solution.size(); ++isol)
   {
      sol_norms[isol] = solution[isol].Norm();
   }

   decltype(solution) recomp_solution;

   // Repeat solving equations until the recompressed solution vectors are still solutions
   const auto& decompinfo = this->mpVscfCalcDef->GetVccRspDecompInfo();
   const std::string sol_filename = mpVscfCalcDef->GetName() + aRestartKey;

   // Reference to the solution vectors that are replaced during recompression
   auto& aux_solvec  =  reiterate || decompinfo.empty()
                     ?  solution
                     :  recomp_solution;

   if (  !decompinfo.empty()
      )
   {
      // Initialize recompressed solution
      aux_solvec = solution;

      midas::tensor::TensorDecomposer decomposer(decompinfo);
      Nb decomp_thresh  =  ( aRestartKey == "_mul0_vec_tensor" )
                        ?  this->mpVscfCalcDef->LambdaThreshold() * C_I_10_1
                        :  this->mpVscfCalcDef->GetRspItEqResidThr() * C_I_10_1;
      std::vector<bool> conv(solution.size(), false);
      while (  true
            )
      {
         Mout  << " Recompress solution vectors" << std::endl;

         // Recompress solution vectors
         for(In i=0; i<aux_solvec.size(); ++i)
         {
            if (  !conv[i]
               )
            {
               Mout  << "    Recompress solvec " << i << " of norm = " << sol_norms[i] << " to T_CP = " << decomp_thresh << std::endl;
               for(In itens=I_0; itens<aux_solvec[i].Size(); ++itens)
               {
                  aux_solvec[i].GetModeCombiData(itens) = decomposer.Decompose(solution[i].GetModeCombiData(itens), decomp_thresh);
               }
            }
         }

         // Write decomposed solution to disk
         for(In i=0; i<solution.size(); ++i)
         {
            const std::string sol_filename_i = sol_filename + "_" + std::to_string(i);
            aux_solvec[i].WriteToDisc(sol_filename_i, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
         }

         // If the first calculation did not converge, just break...
         if (  !initial_conv
            )
         {
            Mout  << " Initial linear equations not converged. Write recompressed solutions to disk without further checks!" << std::endl;
            MidasWarning("Initial linear equations not converged. Write recompressed solutions to disk without further checks!");
            break;
         }

         if (  reiterate
            )
         {
            // Read in recompressed solvecs and solve again
            Mout  << " Read in recompressed solution vectors and solve again." << std::endl;
            aSolver.ClearSolver();
            aSolver.ClearRestart();
            aSolver.AddRestart(sol_filename);
            aSolver.SetNonOrthoRestart(true);

            if (  !aSolver.Solve()
               )
            {
               Mout << " Tensor rsp solver not converged :( " << std::endl;
               MidasWarning("Tensor rsp solver not converged");
               break;
            }

            // Set new solvec
            solution = aSolver.Solution();

            if (  aSolver.ItNum() != 0
               )
            {
               if (  n_reiter > max_reiter
                  || decomp_thresh < C_I_10_14
                  )
               {
                  Mout  << " CP-VCC rsp linear equations not converged! T_CP = " << decomp_thresh << ", n_reiter = " << n_reiter << std::endl;
                  MidasWarning("CP-VCC rsp linear equations not converged!");
                  break;
               }
               else
               {
                  Mout  << " Solution check failed. Reiterate!" << std::endl;
                  if (  reiter_increase_acc
                     )
                  {
                     decomp_thresh *= C_I_10_1;
                  }
                  ++n_reiter;
               }
            }
            else
            {
               Mout  << " Solution-vector check complete!" << std::endl;
               break;
            }
         }
         else
         {
            // Check convergence of recompressed solvec
            Mout  << " Check recompressed solution vectors." << std::endl;
            for(In i=I_0; i<aux_solvec.size(); ++i)
            {
               if (  !conv[i]
                  )
               {
                  Mout  << "    Check solvec " << i << std::endl;
                  conv[i] = aSolver.CheckConvergedSolution(aux_solvec[i]);
               }
            }
            bool allconv = true;
            for(const auto& c : conv)
            {
               if (  !c
                  )
               {
                  allconv = false;
                  break;
               }
            }
            if (  allconv
               || decomp_thresh < C_I_10_14
               )
            {
               if (  !allconv
                  )
               {
                  MidasWarning("Solution vector recompressed to T_CP = " + std::to_string(decomp_thresh) + " is no longer a solution!");
               }
               Mout  << " All converged!" << std::endl;
               break;
            }
            else
            {
               Mout  << " Recompression not accurate enough. Increase accuracy!" << std::endl;
               decomp_thresh *= C_I_10_1;
            }
         }
      }
   }
   else
   {
      // Write solution to disk
      for(In i=0; i<solution.size(); ++i)
      {
         const std::string sol_filename_i = sol_filename + "_" + std::to_string(i);
         solution[i].WriteToDisc(sol_filename_i, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
      }
   }

   // Solution-vector analysis (if no overflow)
   if (  !aux_solvec.back().CheckOverflow()
      )
   {
      // Convert solution vector to DataCont
      auto size = aux_solvec.size();
      std::vector<DataCont> dc_solvec;
      dc_solvec.reserve(size);
      for(size_t i=0; i<size; ++i)
      {
         dc_solvec.emplace_back(DataContFromTensorDataCont(aux_solvec[i]));
      }

      this->LinRspOutput(dc_solvec);
   }
}

/**
 * Solve M(omega_f) (A + omega_f*I) = F*R(f) for M(omega_f).
 **/
template
   <  class VEC_T
   >
void Vscf::SolveVccMEq()
{
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   std::string storage = "OnDisc";
   if (  mpVscfCalcDef->VecStorage() == I_0
      )
   {
      storage = "InMem";
   }

   // Find indices for M equations.
   std::vector<In> m_idx;
   for(In i=I_0; i<mpVscfCalcDef->NrspVecs(); ++i)
   {
      const RspVecInf& vecinf = mpVscfCalcDef->GetRspVecInf(i);
      if (  vecinf.GetNorder() == I_1
         && vecinf.GetNonStandardType() == "M"
         )
      {
         m_idx.push_back(i);
      }
   }

   // Quit if we have no equations to solve
   In neqs = m_idx.size();
   if (  neqs == 0
      )
   {
      return;
   }

   Mout << " Solving for VCC M vectors." << std::endl;
   
   // Setup right-hand sides and frequencies.
   std::vector<VEC_T> rhss;     // The right hand sides of the equations, i.e. F*R(f) for state f.
   rhss.reserve(neqs);
   MidasVector freqs(neqs);   // The frequencies used to shift A.
   MidasVector gamma(neqs, C_0);
   for(In i=I_0; i<neqs; ++i)
   {
      const RspVecInf& vecinf = mpVscfCalcDef->GetRspVecInf(m_idx[i]);
      In state = vecinf.RightState();
      freqs[i] = -vecinf.GetRspFrq(I_0);

      // Calculate FR
      std::unique_ptr<VEC_T> tmp_fr = nullptr;
      if constexpr   (  is_dc
                     )
      {
         tmp_fr = std::make_unique<DataCont>(NrspPar(), C_0, storage, "tmp", false);
      }
      else if constexpr (  is_tensor
                        )
      {
         // Niels: We do not have the info to construct the right shape of TensorDataCont in Vscf.
         // However, this can easily be done in Vcc::CalculateFR.
         tmp_fr = std::make_unique<TensorDataCont>();
      }

      this->CalculateFR(*tmp_fr, state);
      tmp_fr->Scale(-C_1);
      rhss.emplace_back(*tmp_fr);

      // Save FR vectors
      if constexpr   (  is_dc
                     )
      {
         rhss.back().NewLabel(mpVscfCalcDef->GetName() + "_rsp_vcc_fr_" + std::to_string(state));
         rhss.back().SaveUponDecon();
      }
      else if constexpr (  is_tensor
                        )
      {
         std::string name = this->mpVscfCalcDef->GetName() + std::string("_rsp_vec_fr_tensor_") + std::to_string(state);
         rhss.back().WriteToDisc(name, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
      }
   }

   // Solve equations.
   if constexpr   (  is_dc
                  )
   {
      this->RspEqSolve(neqs, rhss, freqs, "_rsp_vcc_m_", gamma, false);
   }
   else if constexpr (  is_tensor
                     )
   {
      this->TensorRspEqSolve(rhss, "_rsp_vcc_m_tensor", freqs, gamma, false);
   }

   // Set "evaluated" flag to true even though we don't know if equations are converged.
   for (In i=I_0; i<neqs; ++i)
   {
      this->mpVscfCalcDef->GetRspVecInf(m_idx[i]).SetHasBeenEval(true);
   }
}

/**
*
*
**/
template
   <  class VEC_T
   >
void Vscf::SolveSecondOrderEq()
{
   // Fun with templates
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   Mout << "Entering SolveSecondOrderEq" << std::endl;

   In n_eqs_tot = mpVscfCalcDef->NrspVecs();
   std::string storage_mode = "OnDisc";
   if (  this->mpVscfCalcDef->VecStorage() == I_0
      )
   {
      storage_mode = "InMem";
   }
      
   // second order equations
   In i_2r_begin=I_0;
   std::vector<In> n_2r;
   std::vector<In> asym_2r;
   for(In i=I_0; i<n_eqs_tot; ++i)
   {
      if (  this->mpVscfCalcDef->GetRspVecInf(i).GetNorder() < I_2
         )
      {
         ++i_2r_begin;
      }
      else
      {
         if (  this->mpVscfCalcDef->GetRspVecInf(i).GetNorder() != I_2
            )
         {
            break;
         }
         if (  this->mpVscfCalcDef->GetRspVecInf(i).GetNonStandardType() == "SECOND_ORDER_RSP_VEC"
            )
         {
            n_2r.push_back(i);
         }
      }
   }
   Mout << "I have " << n_2r.size() << " 2nd order rsp. vecs. to solve" << std::endl;

   if (  n_2r.size() == 0
      )
   {
      return;
   }


   // If we have second-order equations to solve
   if constexpr   (  is_tensor
                  )
   {
      MIDASERROR("Vscf::SolveSecondOrderEq only implemented for VCI, which is not implemented for TensorDataCont!");
   }
   else
   {
      // mbh: check for vectors related by permutation
      std::vector<std::vector<In> > book_keep;
      for (In i=I_0; i<n_2r.size(); ++i)
      {
         In i_r=n_2r[i];
         std::string c_op1 = mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(0);
         std::string c_op2 = mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(1);
         Nb c_f1 = mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(0);
         Nb c_f2 = mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(1);
         std::vector<In> tmp_book;
         for (In j=i; j<n_2r.size(); ++j)
         {
            bool cont=false;
            for(In k=0; k<book_keep.size(); ++k)
            {
               for(In l=0; l<book_keep[k].size(); ++l)
               {
                  if (  j==book_keep[k][l]
                     )
                  {
                     cont=true;
                  }
               }
            }
            if (  cont
               )
            {
               continue;
            }
            if (  i == j
               )
            {
               tmp_book.push_back(j);
               continue;
            }

            In j_r=n_2r[j];
            std::string op1=mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(0);
            std::string op2=mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(1);
            Nb f1=mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(0);
            Nb f2=mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(1);
            if (  c_op1 == op2
               && c_f1 == f2
               && c_op2 == op1
               && c_f2 == f1
               )
            {
               Mout  << "Equal SO rsp. vecs: " << std::endl
                     << mpVscfCalcDef->GetRspVecInf(j_r) << std::endl
                     << mpVscfCalcDef->GetRspVecInf(i_r) << std::endl;
               tmp_book.push_back(j);
            }
         }
         if (  tmp_book.size() > 0
            )
         { 
            Mout << "Inserting book: " << tmp_book << std::endl;
            book_keep.push_back(tmp_book);
         }
      }

      std::vector<In> n_2r_unique;
      for(In i=0; i<book_keep.size(); ++i)
      {
         n_2r_unique.push_back(n_2r[book_keep[i][0]]);
      }
      std::vector<In> n_2r_save = n_2r;
      n_2r = n_2r_unique;
      Mout << "OLD: " << n_2r_save << std::endl;
      Mout << "NEW: " << n_2r << std::endl;

      // calculate the second order right-hand sides:
      //
      // VCI: -P(XY)F^X*\lambda_Y(\omega_y)
      std::vector<VEC_T> rhss(n_2r.size());
      MidasVector frq_vec(n_2r.size(),C_0);
      MidasVector gamma_vec(n_2r.size(),C_0);
      for (In i=I_0; i<n_2r.size(); ++i)
      {
         In i_r = n_2r[i];
         // loop over the XY YX permutations
         for(In i_permut=I_0; i_permut<I_2; ++i_permut)
         {
            Nb frq = mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(i_permut);
            frq_vec[i]+=frq;
            std::string oper = mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(i_permut);
            if (  gDebug
               )
            {
               Mout << "Second order eq, op[" << i_permut << "]" << oper
                    << " and frq = " << frq_vec[i] << std::endl;
            }
            // find first order equation with this info
            In first_order_index = -I_1;
            for(In i_fo=I_0; i_fo<n_eqs_tot; ++i_fo)
            {
               if (  mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() < I_1
                  )
               {
                  continue;
               }
               if (  mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() > I_1
                  )
               {
                  MIDASERROR("Did not find requested first order equation");
               }
               ++first_order_index;
               Nb first_order_frq = mpVscfCalcDef->GetRspVecInf(i_fo).GetRspFrq(I_0);
               std::string first_order_op = mpVscfCalcDef->GetRspVecInf(i_fo).GetRspOp(I_0);
               if (  first_order_frq == frq
                  && first_order_op == oper
                  )
               {
                  break;
               }
            }
            if (  first_order_index == -I_1
               )
            {
               MIDASERROR("First order equation not found!");
            }

            std::string data_name = mpVscfCalcDef->GetName() + "_p1rsp_vec_" + std::to_string(first_order_index);
            DataCont first_order_vec;

            first_order_vec.GetFromExistingOnDisc(NrspPar(), data_name);
            first_order_vec.SaveUponDecon(true);

            // contract this with the correct F matrix
            In oper_nr = -I_1;
            // here change operator to the one not defining 
            // the above first order vector
            oper = mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_1-i_permut%I_2);
            oper_nr = gOperatorDefs.GetOperatorNr(oper);
            if (  oper_nr == -I_1
               )
            {
               MIDASERROR("In SolveSecondOrderEq: Did not find the requested operator!");
            }

            // Make transformation
            std::string so_name = mpVscfCalcDef->GetName() + "_sec_ord_rhs_tmp_" + std::to_string(i);

            DataCont so_rhs(NrspPar(), C_0, "OnDisc", so_name, false);

            Nb expt_value = C_0;

            this->CalculateSigma(first_order_vec, so_rhs, oper, oper_nr, expt_value);

            if (  i_permut == I_0
               )
            {
               rhss[i] = so_rhs;
            }
            else
            {
               rhss[i].Axpy(so_rhs, C_1);
            }
         }
         std::string rhs_name = mpVscfCalcDef->GetName() + "_sec_ord_rhs_vec_" + std::to_string(i);
         rhss[i].NewLabel(rhs_name);
         rhss[i].Scale(C_M_1,I_0,NrspPar());
         rhss[i].SaveUponDecon(false);
      }
      // We have a vector of all the RHS
      // and a vector with all the frequencies
      std::string key = "_unique_p2rsp_vec_";
      this->RspEqSolve(n_2r.size(), rhss, frq_vec, key, gamma_vec);

      // mbh: now treat the equivalent ones
      for(In i=I_0; i<book_keep.size(); ++i)
      {
         std::string u_name = mpVscfCalcDef->GetName() + "_unique_p2rsp_vec_" + std::to_string(i);
//         std::string u_name = mpVscfCalcDef->GetName() + "_unique_p2rsp_vec_" + (is_tensor ? "tensor_" : "") + std::to_string(i);

         // Niels: This would be the general way, I suppose...
//         // Copy file
//         for(In j=I_0; j<book_keep[i].size(); ++j)
//         {
//            std::string new_name = mpVscfCalcDef->GetName() + "_p2rsp_vec_" + (is_tensor ? "tensor_" : "") + std::to_string(book_keep[i][j]);
//
//            std::filesystem::copy(u_name, new_name);
//         }
//
//         // Remove original file
//         std::remove(u_name.c_str());

         DataCont u_data;
         u_data.SetNewSize(I_0);
         u_data.GetFromExistingOnDisc(NrspPar(), u_name);
         u_data.SaveUponDecon(false);
         if (  !u_data.ChangeStorageTo("InMem", true)
            )
         {
            MIDASERROR("In second-order rsp part...");
         }
         for(In j=I_0; j<book_keep[i].size(); ++j)
         {
            DataCont data(u_data);
            data.SaveUponDecon(true);
            std::string name=mpVscfCalcDef->GetName()+"_p2rsp_vec_"+std::to_string(book_keep[i][j]);
            data.NewLabel(name);
         }
      }

      n_2r=n_2r_save;

      Mout << " Solved all the needed SECOND ORDER response equations I could " << std::endl;
      for (In i=I_0; i<n_2r.size(); ++i) 
      {
         In i_r=n_2r[i];
         mpVscfCalcDef->GetRspVecInf(i_r).SetHasBeenEval(true);
      }
   }
}

/**
*  Compute all sigma vectors in order to compute matrix elements: 
*
*  <i|X|j> = F^X * U^(j) * U^i = sigma^(xj) * U^i
*
* */
template
   <  class VEC_T
   >
void Vscf::SolveXToXTransitions
   (
   )
{
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   // Make set of sigma vectors: sigma(x,i) = F^x * U^(i)
   In i_ref = I_0;

   // loop over all rspvecs and generate them
   for(In i=I_0; i<mpVscfCalcDef->NrspVecs(); ++i) 
   { 
      // Select the right ones meaning n_order = -11
      if (  mpVscfCalcDef->GetRspVecInf(i).GetNorder() == -I_11
         ) 
      {
         // Niels: This should only be called for quadratic response, which is not available for VCC and therefore not for TensorDataCont
         if constexpr   (  is_tensor
                        )
         {
            MIDASERROR("X->X transitions only implemented for VCI and therefore not for TensorDataCont!");
         }

         //Get the correct eigenvector
         In eig_vec_nr = mpVscfCalcDef->GetRspVecInf(i).LeftState();
         auto eigvec = std::make_unique<VEC_T>();

         std::string vecname = "";
         if constexpr   (  is_dc
                        )
         {
            vecname = this->mpVscfCalcDef->GetName() + "_rsp_eigvec_" + std::to_string(eig_vec_nr);
            eigvec->GetFromExistingOnDisc(NrspPar(), vecname);
            eigvec->SaveUponDecon();
         }
         else if constexpr (  is_tensor
                           )
         {
            vecname = this->mpVscfCalcDef->GetName() + "_rsp_eigvec_tensor_" + std::to_string(eig_vec_nr);
            eigvec->ReadFromDisc(vecname);
         }

         //get the right operator and oper. number
         std::string opername = this->mpVscfCalcDef->GetRspVecInf(i).GetRspOp(I_0);
         Nb exptval = C_0;
         In oper_nr = gOperatorDefs.GetOperatorNr(opername);
         if (  oper_nr == -I_1
            )
         {
            MIDASERROR("In SolveXToX: Did not find the requested operator!");
         }

         //set the output, initialize to 0 vector
         if (  gDebug
            ) 
         {
            Mout  << "Calling calc. Sigma with eigenvector: " << vecname << "\n"
                  << *eigvec
                  << std::endl;
         }

         std::unique_ptr<VEC_T> sigma = nullptr;
         if constexpr   (  is_dc
                        )
         {
            sigma = std::make_unique<DataCont>(NrspPar(),C_0,"OnDisc",mpVscfCalcDef->GetName()+"_sigma_vec_"+std::to_string(i_ref),true);
         }
         else if constexpr (  is_tensor
                           )
         {
            // Niels: We cannot initialize TensorDataCont to right shape from Vscf class. We do not have TensorDecompInfo.
            sigma = std::make_unique<TensorDataCont>();
         }

         // Calculate sigma vector
         this->CalculateSigma(*eigvec, *sigma, opername, oper_nr, exptval);

         // Save sigma vector for TensorDataCont
         if constexpr   (  is_tensor
                        )
         {
            std::string name = this->mpVscfCalcDef->GetName() + "_sigma_vec_tensor_" + std::to_string(i_ref);
            sigma->WriteToDisc(name, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
         }

         if (  gDebug
            ) 
         {
            Mout << "After calc. Sigma, Sigma vector is: "
                 << mpVscfCalcDef->GetName()+"_sigma_vec_" + (is_tensor ? "tensor_" : "") + std::to_string(i_ref) << "\n"
                 << *sigma << std::endl;
         }
         ++i_ref;
      }
   }
}

/**
*  Solve variational second order RE. Essentially means make a set of 
*  vectors:
*            <<X;Y,Z>>_(y,z) = P(X,Y,Z) * F^Z * L^X(wx) * L^Y(wy) = P(X,Y,Z) * vec^(z,x) * L^Y(wy)
*
*  Then use the ResidCont. Routine to make the vec * L contractions
**/
template
   <  class VEC_T
   >
void Vscf::CreateSigmaVectors() 
{
   // Niels: Not implemented for TensorDataCont as it is only used in quadratic response.
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      for(In i=I_0; i<mpVscfCalcDef->NrspVecs(); ++i) 
      {
         if (  this->mpVscfCalcDef->GetRspVecInf(i).GetNorder() == I_2
            && !mpVscfCalcDef->GetRspVecInf(i).IsNonStandardType()
            ) 
         {
            MIDASERROR("Vscf::CreateSigmaVectors not implemented for TensorDataCont! Only used for quadratic response, which is not implemented for VCC.");
         }
      }
      return;
   }
   else
   {
      // first check for order 
      In i_ref=I_0;
      for(In i=I_0; i<mpVscfCalcDef->NrspVecs(); ++i) 
      {
         if (  this->mpVscfCalcDef->GetRspVecInf(i).GetNorder() == I_2
            && !mpVscfCalcDef->GetRspVecInf(i).IsNonStandardType()
            ) 
         {
            // We have to generate a sigma vector.
            In lin_rsp=-I_1;
            DataCont rsp_vec;
            std::string rv_op;
            for(In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspVecs(); ++i_rsp) 
            {  
               if (  this->mpVscfCalcDef->GetRspVecInf(i_rsp).GetNorder() == I_1
                  ) 
               {
                  ++lin_rsp;
                  if (  mpVscfCalcDef->GetRspVecInf(i).GetRspOp(I_1) == mpVscfCalcDef->GetRspVecInf(i_rsp).GetRspOp(I_0)
                     && mpVscfCalcDef->GetRspVecInf(i).GetRspFrq(I_1) == mpVscfCalcDef->GetRspVecInf(i_rsp).GetRspFrq(I_0)
                     ) 
                  {
                     rv_op=(mpVscfCalcDef->GetRspVecInf(i_rsp)).GetRspOp(I_0);
                     std::string vecname = mpVscfCalcDef->GetName() + "_p1rsp_vec_" + std::to_string(lin_rsp);
                     rsp_vec.GetFromExistingOnDisc(NrspPar(), vecname);
                     rsp_vec.SaveUponDecon(true);
                     break;
                  }
               }
            }
            // We have the correct Rsp. vec. transform it by the right F.
            std::string OperName = (mpVscfCalcDef->GetRspVecInf(i)).GetRspOp(I_0);
            In OperNr = gOperatorDefs.GetOperatorNr(OperName);
            if (  OperNr == -I_1
               )
            {
               MIDASERROR("In CreateSigmaVectors: Did not find the requested operator!");
            }
            // Make transformation
            std::string sigma_name = mpVscfCalcDef->GetName() + "_qrf_vec_" + std::to_string(i_ref);
            DataCont sigma_vec(NrspPar(), C_0, "OnDisc", sigma_name, true);
            Nb ExptValue = C_0;
            this->CalculateSigma(rsp_vec,sigma_vec,OperName,OperNr,ExptValue);
            ++i_ref;
         }
      }
   }
}

/**
 * For non-variational methods, calcluate etaX vectors if necessary.
 **/
template
   <  class VEC_T
   >
void Vscf::CalcNonVarEtaX()
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   // Not needed for variational wave functions
   if (  mpVscfCalcDef->Variational()
      )
   {
      return;
   }
   
   Mout  << " Identifying and calculating required etaX vectors for non-variational wave function." << std::endl;

   // Set of operators for which etaX is needed.
   std::set<std::string> opers;
   
   // Loop over all response functions and check if etaX is needed.
   for (In i=I_0; i<mpVscfCalcDef->NrspFuncs(); ++i)
   {
      const RspFunc& rsp = mpVscfCalcDef->GetRspFunc(i);

      if (  rsp.GetNorder() == -I_1
         )
      {
         opers.insert(rsp.GetRspOp(I_0));
      }
      else if  (  rsp.GetNorder() == I_2
               )
      {
         opers.insert(rsp.GetRspOp(I_0));
         opers.insert(rsp.GetRspOp(I_1));
      }
   }

   for(const auto& op : opers)
   {
      // Get operator number;
      In i_oper = gOperatorDefs.GetOperatorNr(op);
      if (  i_oper == -I_1
         )
      {
         MIDASERROR("Operator " + op + " not found in operators!");
      }

      std::string eta_name = this->mpVscfCalcDef->GetName() + "_eta_vec_" + op;
      std::unique_ptr<VEC_T> eta_vec = nullptr;
      if constexpr   (  is_dc
                     )
      {
         eta_vec = std::make_unique<DataCont>(NrspPar(), C_0, "InMem", eta_name, true); 
      }
      else if constexpr (  is_tensor
                        )
      {
         // Niels: Still, we cannot initalize to correct shape from Vscf class...
         eta_vec = std::make_unique<TensorDataCont>();
         eta_name += "_tensor";
      }

      Nb dummy = C_0;

      this->CalculateEta(*eta_vec, op, i_oper, dummy);

      // Save TensorDataCont result
      if constexpr   (  is_tensor
                     )
      {
         eta_vec->WriteToDisc(eta_name, midas::mpi::OFileStream::StreamType::MPI_ALL_DISTRIBUTED);
      }
   }
}

/**
*   Contract response vectors with vectors to get final response functions
* */
template
   <  class VEC_T
   >
void Vscf::RspContract()
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   if (  mpVscfCalcDef->RspIoLevel() > I_1
      )
   {
      Mout << " Contracting response vectors with matrices and vectors to give"
           << " final response functions." << std::endl;
   }
   for (In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      In n_order = this->mpVscfCalcDef->GetRspFunc(i_rsp).GetNorder();
      if (  gDebug
         )
      {
         Mout << " i_rsp = " << i_rsp << " n_order = " << n_order << std::endl;
      }
      
      if (  n_order == I_1
         ) 
      {
         this->RspExptValue<VEC_T>(i_rsp);
      }
      else if  (  n_order == I_2
               ) 
      {
         this->LrfContract<VEC_T>(i_rsp);
      }
      else if  (  n_order == -I_1
               ) 
      {
         this->ResidContractGtoX<VEC_T>(i_rsp);
      }
      else if  (  n_order == -I_11
               ) 
      {
         if constexpr   (  is_tensor
                        )
         {
            MIDASERROR("Vscf::RspContract: XtoX not impl for TensorDataCont!");
         }
         this->ResidContractXtoX(i_rsp);
      }
      else if  (  n_order == I_3
               ) 
      {
         if constexpr   (  is_tensor
                        )
         {
            MIDASERROR("Vscf::RspContract: Quad. rsp. not impl for TensorDataCont!");
         }
         this->QrfContract(i_rsp);
      }
      else if  (  n_order == I_4
               ) 
      {
         if constexpr   (  is_tensor
                        )
         {
            MIDASERROR("Vscf::RspContract: Cubic rsp. not impl for TensorDataCont!");
         }
         this->CrfContract(i_rsp);
      }
      else if  (  n_order > I_4
               ) 
      {
         Mout << " Response not implemented to this high order yet." << std::endl;
      }
   }
}

/**
 *   Construct linear response function 
 **/
template
   <  class VEC_T
   >
void Vscf::RspExptValue
   (  In& arIrsp
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   if (  mpVscfCalcDef->RspIoLevel()>I_5
      ) 
   {
      Mout << " Expectation value read from rsp op expectation value list." << std::endl;
   }

   Nb value = mpVscfCalcDef->GetRspOpExpVal(mpVscfCalcDef->GetRspFunc(arIrsp).GetRspOp(I_0));

   // If not variational, add extra contribution.
   if (  !mpVscfCalcDef->Variational()
      )
   {
      Mout << " Add non-variational contribution." << endl; 

      // Use 2n+2 rule
      if (  !mpVscfCalcDef->GetRspFunc(arIrsp).NoTwoNPlusTwo()
         )
      {
         // Use 2n+2 rule, that is add t-bar * xi^X , THE SMART WAY
         Mout << " RspExptValue, 2n+2 eval" << std::endl;

         RspVecInf rsp_vec_inf(I_0, false);

         // Find equation number.
         In i_eq = mpVscfCalcDef->FindRspEq(rsp_vec_inf); 
         if (  !mpVscfCalcDef->GetRspVecInf(i_eq).HasBeenEval()
            )
         {
            MIDASERROR("Multipliers were requested but have not been solved for yet");
         }

         Nb add_value = C_0;
         std::string vecname = mpVscfCalcDef->GetName() + "_mul0_vec_";
         std::string oper_name = mpVscfCalcDef->GetRspFunc(arIrsp).GetRspOp(I_0);
         std::string s_xi = mpVscfCalcDef->GetName() + "_xi_vec_" + oper_name;
         if constexpr   (  is_dc
                        )
         {
            vecname += std::to_string(i_eq);
            DataCont mul0_vec;
            mul0_vec.GetFromExistingOnDisc(NrspPar(), vecname);
            mul0_vec.SaveUponDecon(true);

            DataCont xi_vec; 
            xi_vec.GetFromExistingOnDisc(NrspPar(),s_xi); 
            xi_vec.SaveUponDecon(true);

            add_value = Dot(mul0_vec,xi_vec);
         }
         else if constexpr (  is_tensor
                           )
         {
            vecname += "tensor_" + std::to_string(i_eq);
            s_xi += "_tensor";
         
            TensorDataCont mul0;
            mul0.ReadFromDisc(vecname);

            TensorDataCont xi;
            xi.ReadFromDisc(s_xi);

            add_value = Dot(mul0, xi);
         }

         value += add_value; 
         Mout << " non-variational correction " << add_value << " accumulated " << value << std::endl; 
      }
      // Do not use 2n+2 rule (the stupid/debug way)
      else
      {
         // Do not use 2n+2 rule, that is add eta^0 * t^X, THE STUPID WAY
         Mout << " RspExptValue, No 2n+2 eval" << std::endl;
         
         // Get the response vector
         VEC_T rsp_vec;
         this->GetFirstOrderRsp(mpVscfCalcDef->GetRspFunc(arIrsp).GetRspOp(I_0), C_0, rsp_vec);

         if constexpr   (  is_dc
                        )
         {
            rsp_vec.SaveUponDecon(true);
         }
           
         // Get eta0 vector vector.
         std::string s = mpVscfCalcDef->GetName() + "_eta0_vec"; 
         VEC_T eta_vec;

         if constexpr   (  is_dc
                        )
         {
            eta_vec.GetFromExistingOnDisc(NrspPar(),s);
            eta_vec.SaveUponDecon(true);
         }
         else if constexpr (  is_tensor
                           )
         {
            s += "_tensor";
            eta_vec.ReadFromDisc(s);
         }

         Nb add_value = Dot(rsp_vec, eta_vec);
         value += add_value; 
         Mout << " non-variational correction " << add_value << " accumulated " << value << std::endl; 
      }
   }

   mpVscfCalcDef->GetRspFunc(arIrsp).SetValue(value);
   mpVscfCalcDef->GetRspFunc(arIrsp).SetHasBeenEval(true);
}

/**
* Construct linear response function 
* */
template
   <  class VEC_T
   >
void Vscf::LrfContract
   (  In& arIrsp
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   if (  mpVscfCalcDef->RspIoLevel()>I_5
      )
   {
      Mout << " Calculate linear response function " << arIrsp << std::endl;
   }
  
   if (  !mpVscfCalcDef->Variational()
      )
   {
      if constexpr   (  is_dc
                     )
      {
         this->LrfContractNonVar(arIrsp);
      }
      else if constexpr (  is_tensor
                        )
      {
         this->TensorLrfContractNonVar(arIrsp);
      }

      return; 
   }

   // Niels: Variational wave functions not implemented for TensorDataCont yet!
   if constexpr   (  is_tensor
                  )
   {
      MIDASERROR("Linear response function for TensorDataCont only implemented for non-variational wave functions (i.e. VCC)!");
   }
   
   RspFunc& rsp_fct = mpVscfCalcDef->GetRspFunc(arIrsp);
   
   // get response function data
   string lr_op  = rsp_fct.GetRspOp(I_1);
   Nb lr_frq     = rsp_fct.GetRspFrq(I_1);
   string eta_op = rsp_fct.GetRspOp(I_0);
   bool asym     = rsp_fct.GetDoAsymmetric();
   Nb value = C_0;
   
   In nvecs = I_1;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   if (!also_de_ex)
   {
      nvecs=I_2;         // VCI or VCC: ex + de-ex. separately.
   }
   
   // Get response vectors
   if(!rsp_fct.ComplexRspFunc() && !mpVscfCalcDef->ForceComplexRsp())
   {
      /**
       * evaluate response function non-complex case
       **/
      for (In i=I_0;i<nvecs;i++)
      {
         // For VCI change sign of freq for second term
         if (i==I_1)
         {
            lr_frq *= C_M_1;
         }
         
         // Get response vector.
         DataCont rsp_vec;
         GetFirstOrderRsp(lr_op, lr_frq, rsp_vec);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of rsp vec " << rsp_vec.Norm() << endl;
         }
      
         // Get eta vector. 
         if (mpVscfCalcDef->RspIoLevel()>I_10)
         {
            Mout << " Requires eta vector with op = " << eta_op << endl;
         }
         DataCont eta_vec;
         eta_vec.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->GetName()+"_eta_vec_"+eta_op); 
         eta_vec.SaveUponDecon(true);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of eta vec " << eta_vec.Norm() << endl;
         }
         
         // Make contraction.
         if (also_de_ex)
         {
            In half = NrspPar()/I_2;
            Nb value_e = Dot(eta_vec,rsp_vec,I_0,half);
            Nb value_d = Dot(eta_vec,rsp_vec,half,half);
            value += value_d + value_e;
         }
         else 
         {
            if (asym && i==I_1)
               value -= Dot(eta_vec,rsp_vec);
            else
               value += Dot(eta_vec,rsp_vec);
         }
      }
   
      rsp_fct.SetValue(value);
   }
   else
   {
      /**
       * complex case
       **/
      Nb im_value = C_0;
      Nb gamma = rsp_fct.Gamma();

      for (In i=I_0;i<nvecs;i++)
      {
         // For VCI change sign of freq for second term
         if (i==I_1)
         {
            lr_frq *= C_M_1; // multiply by -1
            gamma  *= C_M_1; // -1
         }
         
         // Get response vector.
         ComplexVector<DataCont,DataCont> rsp_vec;
         GetFirstOrderRsp(lr_op, lr_frq, gamma, rsp_vec);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of rsp vec " << rsp_vec.Norm() << endl;
         }
      
         // Get eta vector. 
         if (mpVscfCalcDef->RspIoLevel()>I_10)
         {
            Mout << " Requires eta vector with op = " << eta_op << endl;
         }
         DataCont eta_vec;
         eta_vec.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->GetName()+"_eta_vec_"+eta_op); 
         eta_vec.SaveUponDecon(true);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of eta vec " << eta_vec.Norm() << endl;
         }
         
         // Make contraction.
         if (also_de_ex)
         {
            In half = NrspPar()/I_2;
            // real part
            Nb value_e = Dot(eta_vec,rsp_vec.Re(),I_0,half);
            Nb value_d = Dot(eta_vec,rsp_vec.Re(),half,half);
            value += value_d + value_e;
            // im part
            Nb im_value_e = Dot(eta_vec,rsp_vec.Im(),I_0,half);
            Nb im_value_d = Dot(eta_vec,rsp_vec.Im(),half,half);
            im_value += im_value_d + im_value_e;
         }
         else 
         {
            if (asym && i==I_1)
            {
               value    -= Dot(eta_vec,rsp_vec.Re());
               im_value -= Dot(eta_vec,rsp_vec.Im());
            }
            else
            {
               //std::cout << " dot    = " << Dot(eta_vec,rsp_vec.Re()) << std::endl;
               //std::cout << " im_dot = " << Dot(eta_vec,rsp_vec.Im()) << std::endl;
               value    += Dot(eta_vec,rsp_vec.Re());
               im_value += Dot(eta_vec,rsp_vec.Im());
            }
         }
         
         MidasWarning("File association for Variational linear response not tested!");
         rsp_fct.AssociateFile({lr_op}, analysis::rsp().linear_rsp_vec + std::string((i==I_1 ? "+" : "-")) + "re", rsp_vec.Re().Label());
         rsp_fct.AssociateFile({lr_op}, analysis::rsp().linear_rsp_vec + std::string((i==I_1 ? "+" : "-")) + "im", rsp_vec.Im().Label());
      }

      rsp_fct.SetValue(value);
      rsp_fct.SetImValue(im_value);
   }
   
   rsp_fct.SetHasBeenEval(true);
}

/**
*   Construct residue of linear response function 
* */
template
   <  class VEC_T
   >
void Vscf::ResidContractGtoX
   (  In& arIrsp
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   RspFunc& rsp_fct = mpVscfCalcDef->GetRspFunc(arIrsp);
   In i_state = rsp_fct.RightState();
   std::string eta_op = rsp_fct.GetRspOp(I_0);
   
   if (  mpVscfCalcDef->RspIoLevel() > I_5
      )
   {
      Mout << " Calculate one-photon transition matrix elements for rsp. function No. "
           << arIrsp << "." << std::endl
           << "    State:    " << i_state<< std::endl
           << "    Operator: " << eta_op << std::endl;
   }

   if (  i_state >= mpVscfCalcDef->GetRspNeig()
      ) 
   {
      Mout << " Residual requested for state outside calculated space - skipped." << std::endl;
      rsp_fct.SetValue(C_NB_MAX);
      rsp_fct.SetHasBeenEval(false);
      return;
   }
  
   // Get eta vector 
   std::string s = mpVscfCalcDef->GetName() + "_eta_vec_" + eta_op; 
   VEC_T eta_vec;

   if constexpr   (  is_dc
                  )
   {
      eta_vec.GetFromExistingOnDisc(NrspPar(),s); 
      eta_vec.SaveUponDecon();
   }
   else if constexpr (  is_tensor
                     )
   {
      s += "_tensor";
      eta_vec.ReadFromDisc(s);
   }
   if (  gDebug
      )
   {
      Mout << " Norm of eta vec " << eta_vec.Norm() << std::endl;
   }

   // Get eigenvector 
   std::string s2 = mpVscfCalcDef->GetName() + "_rsp_eigvec_";
   VEC_T eig_vec;

   if constexpr   (  is_dc
                  )
   {
      s2 += std::to_string(i_state);
      eig_vec.GetFromExistingOnDisc(NrspPar(), s2);
      eig_vec.SaveUponDecon();
   }
   else if constexpr (  is_tensor
                     )
   {
      s2 += "tensor_" + std::to_string(i_state);
      eig_vec.ReadFromDisc(s2);
   }
   if (  gDebug
      )
   {
      Mout << " Norm of right eig vec " << eig_vec.Norm() << std::endl;
   }

   // contract 
   Nb value = C_0;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   if (  also_de_ex
      ) 
   {
      if constexpr   (  is_tensor
                     )
      {
         MIDASERROR("DoNoPaired not impl for TensorDataCont!");
      }
      else
      {
         In half = NrspPar()/I_2;
         Nb value_e = Dot(eta_vec,eig_vec,I_0,half);
         Nb value_d = Dot(eta_vec,eig_vec,half,half);
         if (  gDebug
            )
         {
            Mout << " Value e = " << value_e << " Value d = " << value_d << std::endl;
         }
         value = value_d + value_e;
      }
   }
   else 
   {
      value = Dot(eta_vec, eig_vec);  
   }

   if (  !mpVscfCalcDef->Variational()
      )
   {
      value = this->ResidContractGtoXnonVar(arIrsp, eig_vec, value);
   }

   rsp_fct.SetValue(value);
   rsp_fct.SetHasBeenEval(true);
}

/**
*   Find the response equations to solve 
* */
template
   <  class VEC_T
   >
void Vscf::FindRspEqToSolve()
{
   std::set<RspVecInf> rsp_vec_inf_set;
   for (In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      this->AddRspVecToSet<VEC_T>(rsp_vec_inf_set, mpVscfCalcDef->GetRspFunc(i_rsp));
   }
   
   mpVscfCalcDef->ClearRspVecContainer(); 
   for(const auto& inf : rsp_vec_inf_set)
   {
      mpVscfCalcDef->AddRspVec(inf);
   }
   
   mpVscfCalcDef->SortRspVecs(); // sort rvi vector according to order
   
   if (  rsp_vec_inf_set.size() > I_0
      ) 
   {
      Mout << "\n\n Response vectors to solve for:" << std::endl;
      In ic = I_0;
      for(const auto& inf : rsp_vec_inf_set)
      {
         Mout << setw(I_3) << ic++ << ": " << inf << std::endl;
      }
      
      Mout << " A total of " << mpVscfCalcDef->NrspVecs()
           << " response equations is to be solved.\n " << std::endl;
   }
}

/**
* Add the response equations to solve for this particular
* response function.
* */
template
   <  class VEC_T
   >
void Vscf::AddRspVecToSet
   (  std::set<RspVecInf>& arRspVecInfSet
   ,  const RspFunc& arRspFunc
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   In n_order = arRspFunc.GetNorder();
   bool var = mpVscfCalcDef->Variational();
   bool qrf_first_time=true;
   if (  !var
      )
   {
      RspVecInf newrsp(I_0, false);
      arRspVecInfSet.insert(newrsp);
   }
   if (  n_order > I_1
      )
   {
      for(In i_ord=I_0; i_ord<n_order; ++i_ord)
      {  
         // Unless no2np2 vcc rsp.
         if (  !( var
               && n_order == I_2
               && i_ord == I_0
               )
            && !( !var
               && n_order == I_2
               && arRspFunc.NoTwoNPlusTwo()
               && i_ord == I_0
               )
            ) 
         {
            if (  mpVscfCalcDef->RspIoLevel() > I_10
               )
            {
               Mout << " Add first order parameters for rsp func " << arRspFunc << std::endl;
            }
            RspVecInf newrsp(I_1, true);
            newrsp.SetOp(arRspFunc.GetRspOp(i_ord), I_0);
            newrsp.SetFrq(arRspFunc.GetRspFrq(i_ord), I_0);
            newrsp.SetGamma(libmda::numeric::float_neg(arRspFunc.GetRspFrq(i_ord)) ? -fabs(arRspFunc.Gamma()) : fabs(arRspFunc.Gamma()));
            arRspVecInfSet.insert(newrsp);
            if (  mpVscfCalcDef->RspIoLevel() > I_10
               )
            {
               Mout << " Add " << newrsp << std::endl;
            }
            if (  mpVscfCalcDef->DoNoPaired()
               )
            {
               // insert negative frequency vector
               RspVecInf newrsp(I_1,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_ord),I_0);
               newrsp.SetFrq(-arRspFunc.GetRspFrq(i_ord),I_0);
               newrsp.SetGamma(libmda::numeric::float_neg(-arRspFunc.GetRspFrq(i_ord)) ? -fabs(arRspFunc.Gamma()) : fabs(arRspFunc.Gamma()));
               arRspVecInfSet.insert(newrsp);
               if (  mpVscfCalcDef->RspIoLevel() > I_10
                  )
               {
                  Mout << " Add " << newrsp << std::endl;
               }
            }
         }
         //mbh: in case of order = 3 => QRF. if variational then we
         //     need to solve some 1st order response equations, add
         //     them to the set
         if (  var
            && n_order == I_3
            && qrf_first_time
            ) 
         {
            qrf_first_time=false;
            // first add set of sigma vectors
            for(In i_sigma=I_0; i_sigma<I_3; ++i_sigma) 
            {
               RspVecInf newrsp(I_2,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_sigma),I_0);
               if (  i_sigma == I_0
                  ) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
               else if  (  i_sigma == I_1
                        ) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
               else if (  i_sigma == I_2
                  ) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
            }
            // now add a number of 1st order RF. No need to check
            // for SHG as the set will only add unique entries
            for(In i_qrf=I_0; i_qrf<I_3; ++i_qrf) 
            {
               RspVecInf newrsp(I_1,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_qrf),I_0);
               newrsp.SetFrq(arRspFunc.GetRspFrq(i_qrf),I_0);
               newrsp.SetGamma(arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
               // also neq. frq.
               newrsp.SetOp(arRspFunc.GetRspOp(i_qrf),I_0);
               newrsp.SetFrq(-arRspFunc.GetRspFrq(i_qrf),I_0);
               newrsp.SetGamma(-arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
            }
         }
         if (  var
            && n_order == I_4
            )
         {
            // cubic response, add second order rsp vectors
            RspVecInf newrsp(I_2,true,true);
            std::vector<std::pair<std::string,Nb> > oper_frq_vec;
            oper_frq_vec.push_back(std::make_pair(arRspFunc.GetRspOp(I_0),arRspFunc.GetRspFrq(I_0)));
            oper_frq_vec.push_back(std::make_pair(arRspFunc.GetRspOp(I_1),arRspFunc.GetRspFrq(I_1)));
            oper_frq_vec.push_back(std::make_pair(arRspFunc.GetRspOp(I_2),arRspFunc.GetRspFrq(I_2)));
            oper_frq_vec.push_back(std::make_pair(arRspFunc.GetRspOp(I_3),arRspFunc.GetRspFrq(I_3)));
            std::sort(oper_frq_vec.begin(), oper_frq_vec.end());
            do
            {
               newrsp.SetOp(oper_frq_vec[0].first,I_0);
               newrsp.SetOp(oper_frq_vec[1].first,I_1);
               newrsp.SetFrq(oper_frq_vec[0].second,I_0);
               newrsp.SetFrq(oper_frq_vec[1].second,I_1);
               newrsp.SetGamma(arRspFunc.Gamma());
               newrsp.SetNonStandardType("SECOND_ORDER_RSP_VEC");
               arRspVecInfSet.insert(newrsp);
               // also negative frq. (~ to de-ex. part)
               newrsp.SetFrq(-oper_frq_vec[0].second,I_0);
               newrsp.SetFrq(-oper_frq_vec[1].second,I_1);
               newrsp.SetGamma(-arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
            }  while (  std::next_permutation(oper_frq_vec.begin(),oper_frq_vec.end())
                     );
            // Now add all linear equations
            RspVecInf linvecinf(I_1,true);
            for(In icr=I_0; icr<4; ++icr)
            {
               linvecinf.SetOp(oper_frq_vec[icr].first,I_0);
               linvecinf.SetFrq(oper_frq_vec[icr].second,I_0);
               arRspVecInfSet.insert(linvecinf);
               // also de-ex.
               linvecinf.SetFrq(-oper_frq_vec[icr].second,I_0);
               arRspVecInfSet.insert(linvecinf);
            }
         }
         if (  !var
            && (  n_order > I_2
               || (  n_order > I_1
                  && arRspFunc.NoTwoNPlusTwo()
                  )
               )
            )
         {
            if (  mpVscfCalcDef->RspIoLevel() > I_10
               )
            {
               Mout << " Add first order multipliers for rsp func " << arRspFunc << endl;
            }
            RspVecInf newrsp(I_1,true);
            newrsp.SetOp(arRspFunc.GetRspOp(i_ord),I_0);
            newrsp.SetFrq(arRspFunc.GetRspFrq(i_ord),I_0);
            newrsp.SetGamma(arRspFunc.Gamma());
            newrsp.SetRight(false); 
            arRspVecInfSet.insert(newrsp);
            if (  mpVscfCalcDef->RspIoLevel() > I_10
               )
            {
               Mout << " Add " << newrsp << std::endl;
            }
         }
         // Actually there will be more equations for non-linear response but
         // this is neglected for the moment....
      }
   }
   if (  n_order == -I_11
      ) 
   {
      RspVecInf newrsp(n_order,true);
      // Need to set some things in order to add it 
      // to the set due to the compare function
      // OP and L state should do the trick
      newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_0);
      newrsp.SetLeftState(arRspFunc.LeftState());
      arRspVecInfSet.insert(newrsp);
   }
   if (  !var
      && n_order == I_1
      && arRspFunc.NoTwoNPlusTwo()
      )
   {
      RspVecInf newrsp(I_1, true);
      newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_0);
      newrsp.SetFrq(C_0,I_0);
      newrsp.SetGamma(arRspFunc.Gamma());
      arRspVecInfSet.insert(newrsp);
   }
   if (  !var
      && n_order == -I_1
      )
   {
      // Make sure that first order responses for frequencies corresponding to
      // excitation energies are calculated.

      // Get final state and corresponding energy.
      In nroots = mpVscfCalcDef->GetRspNeig();
      In state = arRspFunc.RightState();
      const string& oper_name = arRspFunc.GetRspOp(I_0);
      Nb eigval = C_0;
      if (  state >= nroots
         )
      {
         Mout << " Transition matrix element for gs -> state " << state
              << " with operator '" << oper_name << "':" << std::endl
              << " Residual for state is outside calculated space - skipped." << std::endl;
         return;
      }

      // Check eigenvectors
      if constexpr   (  is_dc
                     )
      {
         DataCont left_eig;
         left_eig.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->GetName() + "_rsp_eigvec_left_" + std::to_string(arRspFunc.RightState()));
         left_eig.SaveUponDecon();
         if (  left_eig.Norm() == C_0
            )
         {
            return;
         }

         DataCont eigval_dc;
         eigval_dc.GetFromExistingOnDisc(nroots, mpVscfCalcDef->GetName() + "_rsp_eigval");
         eigval_dc.SaveUponDecon();
         eigval_dc.DataIo(IO_GET, state, eigval);
      }
      else if constexpr (  is_tensor
                        )
      {
         TensorDataCont left_eig;
         std::string s = mpVscfCalcDef->GetName() + "_rsp_eigvec_left_tensor_" + std::to_string(arRspFunc.RightState());

         left_eig.ReadFromDisc(s);

         if (  left_eig.Norm() == C_0
            )
         {
            return;
         }

         DataCont eigval_dc;
         eigval_dc.GetFromExistingOnDisc(nroots, mpVscfCalcDef->GetName() + "_rsp_eigval_re_tensor");
         eigval_dc.SaveUponDecon();
         eigval_dc.DataIo(IO_GET, state, eigval);
      }

      // Add response equation.
      if (  arRspFunc.UseMvec()
         )
      {
         RspVecInf newrsp(I_1,false,true);
         newrsp.SetNonStandardType("M");
         newrsp.SetFrq(eigval,I_0);
         newrsp.SetGamma(arRspFunc.Gamma());
         newrsp.SetRightState(arRspFunc.RightState());
         arRspVecInfSet.insert(newrsp);
      }
      else
      {
         RspVecInf newrsp(I_1,true);
         newrsp.SetOp(oper_name,I_0);
         newrsp.SetFrq(-eigval,I_0);
         newrsp.SetGamma(-arRspFunc.Gamma());
         arRspVecInfSet.insert(newrsp);
      }
   }
}


/**
* Drive calculation of SOS response functions 
* based upon calculated transition moments
* */
template
   <  class VEC_T
   >
void Vscf::SosRspFunc
   (  std::vector<RspFunc>& aVec
   )
{
   // Set bools for template stuff
   constexpr bool is_tensor = std::is_same_v<VEC_T, TensorDataCont>;
   constexpr bool is_dc = std::is_same_v<VEC_T, DataCont>;
   static_assert(is_tensor || is_dc, "Only implemented for DataCont and TensorDataCont!");

   In n_roots = mpVscfCalcDef->GetRspNeig();
   if (  n_roots > I_0
      ) 
   {
      Mout << " Calculation of SOS response functions " << std::endl; 
   }
   else 
   {
      Mout << " No excited states calculated " << std::endl; 
      Mout << " Calculation of SOS response functions skipped " << std::endl; 
      return;
   }

   // Find vector of operators for which there exist <o!X!i> elements.
   std::set<std::string> trans_op;
   std::set<std::string> xtox_op;
   for(In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      In n_order = mpVscfCalcDef->GetRspFunc(i_rsp).GetNorder();

      // transition moments
      if (  n_order == -I_1
         )
      {
         auto eta_op = mpVscfCalcDef->GetRspFunc(i_rsp).GetRspOp(I_0);
         trans_op.insert(eta_op);
         for(In j_rsp=I_0; j_rsp<mpVscfCalcDef->NrspFuncs(); ++j_rsp)
         {
            if (  mpVscfCalcDef->GetRspFunc(j_rsp).GetNorder() == -I_11
               )
            {
               xtox_op.insert(mpVscfCalcDef->GetRspFunc(j_rsp).GetRspOp(I_0));
            }
         }
      }
   }

   // Output operator names
   Mout << " Operators with transition matrix elements " << std::endl;
   for(const auto& op : trans_op)
   {
      Mout << op << std::endl;
   }
   Mout << " And with trs. mat. el. between two exc. states " << std::endl;
   for(const auto& op : xtox_op)
   {
      Mout << op << std::endl;
   }

   DataCont eig_val_dc; 
   eig_val_dc.GetFromExistingOnDisc(n_roots,mpVscfCalcDef->GetName() + "_rsp_eigval" + (is_tensor ? "_re_tensor" : ""));
   MidasVector eig_vals(n_roots);
   eig_val_dc.DataIo(IO_GET, eig_vals, n_roots);
   eig_val_dc.SaveUponDecon();
   Mout << " eig_vals " << eig_vals << std::endl;

   // Loop over X operators
   Nb frq_b = mpVscfCalcDef->SosFreqBegin(); 
   Nb frq_e = mpVscfCalcDef->SosFreqEnd();
   In n_frq_steps = mpVscfCalcDef->SosFreqSteps();

   /**   mbh: compute SOS response functions here:
    *    Remaining:
    *    1) Damping
    *    2) Need check to ensure equal sizes of <0|X|i> and <i|X|j>
    **/
   if (  trans_op.size() == 0
      )
   {
      return;
   }

   this->LinearSosRsp(trans_op, xtox_op, eig_vals, aVec);

   if (  xtox_op.size() == 0
      )
   {
      return;
   }

   // Niels: Quadratic and cubic response not implemented for TensorDataCont (and VCC) yet!
   if constexpr   (  is_tensor
                  )
   {
      MIDASERROR("Quadratic and cubic SOS response not implemented for TensorDataCont!");
   }
   else
   {
      this->QuadraticSosRsp(trans_op, xtox_op, eig_vals, aVec);
      this->CubicSosRsp(trans_op, xtox_op, eig_vals, aVec);
   }

   return;
}


#endif /* VSCF_IMPL_H_INCLUDED */
