/**
************************************************************************
* 
* @file                VscfRspEta.cc
*
* Created:             01-05-2009
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Vscf Response eta vector
* 
* Last modified: Thu Jun 11, 2009  05:56PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<set>
#include<string>
using std::set;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"
#include "input/GlobalOperatorDefinitions.h"

/**
 * Calcaualte Rhs vector for VSCF. Not implemented for TensorDataCont.
 **/
void Vscf::CalculateEta
   (  TensorDataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   MIDASERROR("Not impl!");
}

/**
*   Caluclate Eta vector, VSCF.
* */
void Vscf::CalculateEta
   (  DataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   if (gDebug)
   {
      Mout << "\n\n Vscf::CalculateEta - Calculate eta vector ";
      Mout << " arOperName = " << arOperName << " nr = " <<  arIoper << endl; 
   }
   arEta.SetToUnitVec(arIoper); // Test....
   OpDef* p_opdef = &gOperatorDefs[arIoper];
   Nb expt_size = I_1;

   // Calculate integrals - prepare arrays and addressing 
   bool save = true;
   string storage = "InMem";
   if (mpVscfCalcDef->IntStorage()!=I_0) storage = "OnDisc";
   OneModeInt one_mode_int(&gOperatorDefs[arIoper],mpBasDef,storage,save);
   Mout << "\n Calculate integrals for operator " << arOperName << endl;
   one_mode_int.CalcInt();
   // If we are doing OneState temp. ave. construct a^{m,t}
   // time it!
   Timer onestate_time;
   if(mpVscfCalcDef->OneStateVscfAve()) 
   {
      mpVscfCalcDef->SetProperty(arOperName);
      // old version: ConstructTempAve(&one_mode_int);
      NewConstructTempAve(&one_mode_int);
      // print the timing after each property
      onestate_time.CpuOut(Mout,"Time doing the "+arOperName+"Therm. ave. ");
   }

   // Calculate first the one mode occupied integrals 
   vector<In> one_mode_int_occ_off(mpVscfCalcDef->GetNmodesInOcc()); 
              ///< Offsets for OneModeIntOcc for each mode
   MidasVector one_mode_int_occ;    ///< The one-mode occupied integrals.
   In n_opers                   = 0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      //mOneModeIntOccOff[i_op_mode]      = n_opers;
      one_mode_int_occ_off[i_op_mode]   = n_opers;
      n_opers                           += p_opdef->NrOneModeOpers(i_op_mode);
   }
   //if (gDebug)
   //{
      //Mout << " n_opers new calc. " << n_opers << endl;
      //Mout << " n_opers from old  " << one_mode_int.NoperTot() << endl;
   //}
   one_mode_int_occ.SetNewSize(one_mode_int.GetmTotNoOneModeOpers());

   // Calculate the h_ii integrals 
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);

      In n_op = p_opdef->NrOneModeOpers(i_op_mode);
      MidasVector modal(nbas);
      mOccModals.PieceIo(IO_GET,modal,nbas,OccModalOffSet(i_op_mode));
      //if (gDebug) 
      //{
         //Mout << " n_op = " << n_op << " n_bas " << nbas << endl;
         //Mout << " OffSet for modal: " 
         //<< OccModalOffSet(i_op_mode) << " for i_op_mode" << i_op_mode << endl;
         //Mout << " i_op_mode, modal norm, sumele: " 
         //<< i_op_mode << " " << modal.Norm() << " " << modal.SumEle() << endl;
      //}
      In add = one_mode_int_occ_off[i_op_mode];
   
      for (LocalOperNr i_op=0;i_op<n_op;i_op++)
      {
         Nb trans_int=C_0;
         one_mode_int.IntBracket(modal,i_op_mode,i_op,trans_int);
         one_mode_int_occ[add+i_op] = trans_int;
         //if (gDebug) 
         //{
            //Mout << " CalculateEta: " << setw(10) << left 
            //<< p_opdef->GetOneModeOper(i_op_mode,i_op) 
               //<< "i_op " << i_op << " mode " << i_op_mode << endl;
            //Mout << " trans int, add+i_op " << trans_int 
            //<< " " << add+i_op << endl;
         //}
      }
   }
   if (gDebug) 
   {
      Mout << " Norm of full set of one-mode oper integrals = "  
         << one_mode_int_occ.Norm() << endl;
      Mout << " Norm of Full set of reference one-mode oper integrals = " 
      << mOneModeIntOcc.Norm() << endl;
      Mout << " Full set of one-mode oper integrals \n" 
      << one_mode_int_occ << endl;
      Mout << " Full set of reference one-mode oper integrals \n" 
      << mOneModeIntOcc << endl;
   }

   if (!p_opdef->UseActiveTermsAlgo())
   {
      Mout << " I was to lazy to implement anything else than "
         << " the ActiveTerms Algorithm " << endl;
      MIDASERROR(" Only active terms algo implemented in Rsp ");
   }

   // Loop over modes...
   In add = I_0;
   In half = NrspPar()/I_2;
   In n_sc_1 = I_0;
   In n_sc_2 = I_0;
   In n_nsc  = I_0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);
      if (nbas<=I_1) continue;

      MidasMatrix fock(nbas,C_0);
      if (gDebug) Mout << " i_op_mode " << i_op_mode << endl;
      Nb factor_for_unit = 0;
      for (In i_act_term=0;i_act_term<p_opdef->NactiveTerms(i_op_mode);i_act_term++)
      {
         In i_term = p_opdef->TermActive(i_op_mode,i_act_term);
         {
            Nb fact = p_opdef->Coef(i_term);
            if (fabs(fact) < mpVscfCalcDef->ScreenZeroCoef()) 
            {
               n_sc_1++;
               continue;
            }


      
            In i_fac_thismode = -1;
            if (gDebug) Mout << " factors " << p_opdef->NfactorsInTerm(i_term) 
               << " in term " << i_term << endl; 
            for (In i_fac=0;i_fac<p_opdef->NfactorsInTerm(i_term);i_fac++)
            {
               LocalModeNr i_op_mode2  = p_opdef->ModeForFactor(i_term,i_fac);
               if (gDebug) Mout << " i_op_mode " << i_op_mode 
                      << " i_op_mode2" << i_op_mode2 << endl;
               if (i_op_mode == i_op_mode2) 
               {
                  i_fac_thismode = i_fac;
               }
               else
               {
                  LocalOperNr i_oper     = p_opdef->OperForFactor(i_term,i_fac);
                  fact *= one_mode_int_occ[one_mode_int_occ_off[i_op_mode2]
                     +i_oper];
                  if (gDebug) Mout << " i_term " << i_term 
                      << "i_op_mode2 " << i_op_mode2
                      << " fact " << fact << " this cont " << 
                     one_mode_int_occ[one_mode_int_occ_off[i_op_mode2]+i_oper] 
                     << endl;
               }
            }
   
            //if ((fabs(fact)/expt_size) < mpVscfCalcDef->ScreenZero()) continue;

   
            if (i_fac_thismode >-1) 
            {
               LocalOperNr i_oper     = p_opdef->OperForFactor(i_term,i_fac_thismode);
               //if (gDebug) Mout << " fock before " << endl << fock << endl;
               Nb cont_size_est = fabs(fact)*mOneModeOperNorms[mOneModeIntOccOff[i_op_mode]+i_oper];
               //Mout << " fact " << fact << " cont_size_est " << cont_size_est << endl;
               if ((cont_size_est/expt_size) < mpVscfCalcDef->ScreenZero()) 
               {
                  //Mout << " Screened something " << endl;
                  n_sc_2++;
                  continue;
               }
               n_nsc++;

               one_mode_int.AddIntTo(fock,i_op_mode,i_oper,fact);
               //if (gDebug) Mout << "CalculateEta: " << setw(10) << p_opdef->GetOneModeOper(i_op_mode,i_oper) 
                     //<< "i_op " << i_oper << " i_term " << i_term << " fact: " << fact 
                        //<< " mode " << i_op_mode << endl;
               //if (gDebug) Mout << " fock after  " << endl << fock << endl;
            }
            else if (i_fac_thismode ==-1)  
               // This term has no operator in the product - 
               // add to the sum over products over a unit operator.
               // This does not contribute to eta vector!
               // The Eai goes through it all and annihilates vacuum!
            {
               factor_for_unit += fact;
            }
         }
      }
      if (factor_for_unit != I_0) MIDASERROR( "Error in CalculateEta, factor_for_unit ??  ");

      // Transform to modal basis.
      TransInt(fock,i_op_mode);
      // Store interesting part on eta vector
      if (gDebug) Mout << " F-" << arOperName << " matrix \n" << fock << endl;
      MidasVector tmp(nbas-I_1);
      for (In a=I_0;a<nbas-I_1;a++) tmp[a] = fock[a+I_1][I_0];
      if (gDebug) Mout << " Put this part on eta vec " << tmp << endl;
      arEta.DataIo(IO_PUT,tmp,nbas-I_1,add);
      if (!mpVscfCalcDef->DoNoPaired())
      {
         //add += nbas-I_1;
         tmp.Scale(C_M_1);
         // eta_-i = -eta_i (-eta_i^*, complex conj. skipped, assumed real/REAL)
         if (gDebug) Mout << " Put this other part on eta vec " << tmp << endl;
         arEta.DataIo(IO_PUT,tmp,nbas-I_1,add+half);
      }
      add += nbas-I_1;
   }

   arExptValue = C_0;
   for (In i_term=0;i_term<p_opdef->Nterms();i_term++)
   {
      Nb fact = p_opdef->Coef(i_term);
      for (In i_fac=0;i_fac<p_opdef->NfactorsInTerm(i_term);i_fac++)
      {
         LocalModeNr i_op_mode  = p_opdef->ModeForFactor(i_term,i_fac);
         LocalOperNr i_oper     = p_opdef->OperForFactor(i_term,i_fac);
         fact *= one_mode_int_occ[one_mode_int_occ_off[i_op_mode]+i_oper];
         //if (gDebug) Mout << " i_term " << i_term << " i_op_mode " << i_op_mode 
           //<< " fact " << fact << " this cont " << 
           //one_mode_int_occ[one_mode_int_occ_off[i_op_mode]+i_oper] << endl;
      }
      if (mpVscfCalcDef->RspIoLevel() > I_14)
      {
         Mout << " i_term " << i_term << " Expvt value contribution: " << fact << std::endl;
      }
      arExptValue += fact;
   }
   if (gDebug) 
   {
      Mout << " At the end of the day eta-" << arOperName << " has norm " 
         << arEta.Norm() << endl;
      Mout <<" eta is \n" << arEta << endl;
   }
}
/**
*  Set a new operator to be the relevant operator.
* */
void Vscf::PrepareNewOccInt(In arIoper)
{
   OpDef* mpOpDef = &gOperatorDefs[arIoper];

   bool save = true;
   string storage = "InMem";
   OneModeInt one_mode_int(&gOperatorDefs[arIoper],mpBasDef,storage,save);
   Mout << "\n Calculate integrals for operator " << endl;
   one_mode_int.CalcInt();

   // Calculate first the one mode occupied integrals 
   In n_opers                   = I_0;
   for (LocalModeNr i_op_mode=I_0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      //mOneModeIntOccOff[i_op_mode]      = n_opers;
      mOneModeIntOccOff[i_op_mode]   = n_opers;
      n_opers                       += mpOpDef->NrOneModeOpers(i_op_mode);
   }
   mOneModeIntOcc.SetNewSize(n_opers);    ///< The one-mode occupied integrals.

   // Calculate the h_ii integrals 
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);

      In n_op = mpOpDef->NrOneModeOpers(i_op_mode);
      MidasVector modal(nbas);
      mOccModals.PieceIo(IO_GET,modal,nbas,OccModalOffSet(i_op_mode));
      //if (gDebug) 
      //{
         //Mout << " n_op = " << n_op << " n_bas " << nbas << endl;
         //Mout << " OffSet for modal: " 
         //<< OccModalOffSet(i_op_mode) << " for i_op_mode" << i_op_mode << endl;
         //Mout << " i_op_mode, modal norm, sumele: " 
         //<< i_op_mode << " " << modal.Norm() << " " << modal.SumEle() << endl;
      //}
      In add = mOneModeIntOccOff[i_op_mode];
   
      for (LocalOperNr i_op=0;i_op<n_op;i_op++)
      {
         Nb trans_int=C_0;
         one_mode_int.IntBracket(modal,i_op_mode,i_op,trans_int);
         mOneModeIntOcc[add+i_op] = trans_int;
         //if (gDebug) 
         //{
            //Mout << " PrepareNewOper: " << setw(10) << left 
            //<< mpOpDef->GetOneModeOper(i_op_mode,i_op) 
               //<< "i_op " << i_op << " mode " << i_op_mode << endl;
            //Mout << " trans int, add+i_op " << trans_int 
            //<< " " << add+i_op << endl;
         //}
      }
   }

}
