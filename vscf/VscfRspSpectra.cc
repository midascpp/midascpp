/**
************************************************************************
* 
* @file                VscfRspSpectra.cc
*
* Created:             01-05-2009
*
* Author:              Peter Seidler 
*                      Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Rsp Spectra IR/Raman 
* 
* Last modified: Sat May 15, 2010  07:10PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<set>
#include<string>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "vscf/VscfRspTransformer.h"
#include "util/Plot.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"

/**
 *    Drive calculation of IR intensities/spectra 
 **/
void Vscf::DoIR
   (  const std::string& aEigValFileKey
   )  const
{
   std::vector<IRData> ir_data;         // Vector of IR data structures.
 
   // Get number of response eigenvalues (energies) calculated...
   In n_roots = mpVscfCalcDef->GetRspNeig();

   // ... and read them from disc. 
   DataCont eig_val_dc;
   MidasVector eig_vals(n_roots);
   eig_val_dc.GetFromExistingOnDisc(n_roots, mpVscfCalcDef->GetName() + aEigValFileKey);
   eig_val_dc.DataIo(IO_GET, eig_vals, n_roots);
   eig_val_dc.SaveUponDecon();

   // Get data for matrix elements of type <0|*_dipole|i> (response order -1).
   for(In i_rsp = I_0; i_rsp < mpVscfCalcDef->NrspFuncs(); ++i_rsp)   // Loop over all rsp. funcs.
   {
      const RspFunc& rsp = mpVscfCalcDef->GetRspFunc(i_rsp);
      const OpInfo op_info = OpDef::msOpInfo[rsp.GetRspOp(I_0)];

      // Do we have an *_dipole transition moment? 
      if (  -I_1 == rsp.GetNorder()
         && rsp.HasBeenEval()
         && op_info.IsDipole()
         )
      {
         In left_state  = rsp.LeftState();
         In right_state = rsp.RightState();
         
         // Search for IR data structure which already has the same left and right state.
         bool b_set = false; // We haven't found one yet.
         for(auto& it_ir : ir_data)
         {
            if (  it_ir.GetInitState() == left_state
               && it_ir.GetFinalState() == right_state
               )
            {
               it_ir.SetMu(op_info.GetType(), rsp.Value());
               b_set = true;
            }
         }
         if (  false == b_set
            )
         {
            IRData new_data;
            new_data.SetInitState(left_state);
            new_data.SetFinalState(right_state);
            new_data.SetOmegafi(eig_vals[right_state]); // OBS: Supposing left state is ground st.
            new_data.SetMu(op_info.GetType(), rsp.Value());
            ir_data.push_back(new_data);
         }
      }
   }
  
   // Done with the calculation. Print results (if any.)
   if (  ir_data.empty()
      )
   {
     return;
   }

   Mout << " IR data calculated:" << std::endl;
   Out72Char(Mout,'+','-','+');
   Mout << std::endl;
   
   Mout << " Trans.    E(f)-E(i)                 Trans. dip. moment squared" << std::endl
        << "           (au)                      |mu|^2 (au)" << std::endl;
   Mout.setf(ios_base::scientific, ios_base::floatfield);
   for(const auto& it_ir : ir_data)
   {
      midas::stream::ScopedPrecision(6, Mout);
      Mout << setw(3) << it_ir.GetFinalState() << "<-"
           << setw(2) << it_ir.GetInitState();
      midas::stream::ScopedPrecision(16, Mout);
      Mout << setw(26) << it_ir.GetOmegafi()
           << setw(26) << it_ir.GetMu2()
           << std::endl;
   }
   Mout << std::endl;

   Mout << " Trans.    E(f)-E(i)                 IR intensity" << std::endl
        << "           (cm^-1)                   (km/mol)" << std::endl;
   Mout.setf(ios_base::scientific, ios_base::floatfield);

   for(const auto& it_ir : ir_data)
   {
      midas::stream::ScopedPrecision(6, Mout);
      Mout << setw(3) << it_ir.GetFinalState() << "<-"
           << setw(2) << it_ir.GetInitState();
      midas::stream::ScopedPrecision(16, Mout);
      Mout << setw(26) << it_ir.GetOmegafi() * C_AUTKAYS
           << setw(26) << C_EXDIP_AU_KMMOL * it_ir.GetMu2() * it_ir.GetOmegafi()
           << std::endl;
   }
   Mout << std::endl;

   Mout << " Trans.    E(f)-E(i)                 Oscillator strength" << std::endl
        << "           (cm^-1)                   (2/3)*(m_e/e^2/h_bar^2)*(E(f)-E(i))*|mu|^2" << std::endl;
   Mout.setf(ios_base::scientific, ios_base::floatfield);

   for(const auto& it_ir : ir_data)
   {
      midas::stream::ScopedPrecision(6, Mout);
      Mout << setw(3) << it_ir.GetFinalState() << "<-"
           << setw(2) << it_ir.GetInitState();
      midas::stream::ScopedPrecision(16, Mout);
      Mout << setw(26) << it_ir.GetOmegafi() * C_AUTKAYS
           << setw(26) << it_ir.GetOscStrength()
           << std::endl;
   }
   Mout << std::endl;

   IRPrepareAnalysis(ir_data);

   Out72Char(Mout,'+' ,'-','+');  
   Mout << " IR end." << std::endl << std::endl;
}

/**
 *
 **/
void Vscf::IRPrepareAnalysis
   (  const std::vector<IRData>& aData
   )  const
{
   if(!midas::mpi::IsMaster())
   {
      return;
   }
   if ("" == mpVscfCalcDef->GetmVscfAnalysisDir())
   {
      Mout << " Analysis directory not set. No data file will be generated." << std::endl;
      return;
   }

   std::string filename = mpVscfCalcDef->GetmVscfAnalysisDir() + "/midas_ir_" + mpVscfCalcDef->GetName();
   Mout << " Generating data file \"" << filename <<"\" for analysis program" << endl;

   midas::mpi::OFileStream odat(filename);
   
   odat.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(16, odat);
   for(const auto& it : aData)
   {
      // Avoid noise and zero intensities.
      if (  it.GetOscStrength() < C_I_10_10*C_I_10_10
         )
      {
         continue;
      }
      
      odat << setw(6) << it.GetFinalState() << "\t"
           << setw(6) << it.GetInitState() << "\t"
           << setw(26) << it.GetOmegafi() * C_AUTKAYS << "\t"
           << setw(26) << C_EXDIP_AU_KMMOL * it.GetMu2() * it.GetOmegafi()
           << std::endl;
   }

   // Append filename to list of files read by analysis program.
   filename = mpVscfCalcDef->GetmVscfAnalysisDir() + "/SpectrumFiles";
   midas::mpi::OFileStream ms(filename, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);
   ms << "midas_ir_" << mpVscfCalcDef->GetName() << endl;
}

/**
 *
 **/
void Vscf::DoRaman
   (  const std::string& aEigValFileKey
   )  const
{
   std::vector<RamanData> raman_data;   // Vector of Raman data structures.      
   
   // Get number of response eigenvalues (energies) calculated...
   In n_roots = mpVscfCalcDef->GetRspNeig();

   // ... and read them from disc. 
   DataCont eig_val_dc;
   MidasVector eig_vals(n_roots);
   eig_val_dc.GetFromExistingOnDisc(n_roots,mpVscfCalcDef->GetName() + aEigValFileKey);
   eig_val_dc.DataIo(IO_GET, eig_vals, n_roots);
   eig_val_dc.SaveUponDecon();

   // Get data for matrix elements of type <0|**_pol|i> (response order -1).
   for (In i_rsp = I_0; i_rsp < mpVscfCalcDef->NrspFuncs(); ++i_rsp)   // Loop over all rsp. funcs.
   {
      const RspFunc& rsp = mpVscfCalcDef->GetRspFunc(i_rsp);
      const OpInfo op_info = OpDef::msOpInfo[rsp.GetRspOp(I_0)];

      // Do we have an **_pol transition moment? 
      if (  -I_1 == rsp.GetNorder()
         && op_info.IsAlpha()
         )
      {
         In left_state  = rsp.LeftState();
         In right_state = rsp.RightState();
         Nb ext_frq     = op_info.GetFrq(0);
         
         // Search for Raman data structure which already has the same left and right
         // state and the same frequency.
         bool b_set = false; // We haven't found one yet.
         for(auto& it_raman : raman_data)
         {
            if (  it_raman.GetInitState() == left_state
               && it_raman.GetFinalState() == right_state
               && it_raman.GetOmegaExt() == ext_frq
               )
            {
               it_raman.SetAlphaElement(op_info.GetType(), rsp.Value());
               b_set = true;
            }
         }
         if (  false == b_set
            )
         {
            RamanData new_data;
            new_data.SetInitState(left_state);
            new_data.SetFinalState(right_state);
            new_data.SetOmegaExt(ext_frq);
            new_data.SetOmegafi(eig_vals[right_state]); // OBS: Supposing left state is ground st.
            new_data.SetAlphaElement(op_info.GetType(), rsp.Value());
            raman_data.push_back(new_data);
         }
      }
   }
  
   // Done with the calculation. Print results (If any.)
   if (  raman_data.empty()
      )
   {
     return;
   }
     
   Mout << " Raman data calculated:" << std::endl;
   Out72Char(Mout,'+','-','+');

   // Separate output into different external frequencies.
   while (  !raman_data.empty()
         )
   {
      // Extract all RamanData with same external freq into tmp.
      std::vector<RamanData> tmp;
      auto it = raman_data.begin();
      Nb omega_ext = it->GetOmegaExt();
      tmp.push_back(*it);
      raman_data.erase(it);
      while (  it != raman_data.end()
            )
      {
         if (  it->GetOmegaExt() == omega_ext
            )
         {
            tmp.push_back(*it);
            raman_data.erase(it);
         } 
         else
         {
            ++it;
         }
      }

      // Print data for current external frequency.
      Mout.setf(ios_base::fixed, ios_base::floatfield);
      midas::stream::ScopedPrecision(6, Mout);
      Mout << " Raman data for external freq. " << omega_ext << " au = "
           << C_PLANCK * C_LIGHT / (omega_ext * C_AUTJ) * C_10_9 << " nm" << std::endl << std::endl;
           
      Mout << "   Trans.   E(f)-E(i)  a^2        gamma^2    Activity   Depol. Ratio"
           << std::endl
           << "            (au)       (au)       (au)       (au)" << std::endl;
      for(const auto& it : tmp)
      {
         Nb a2, gamma2;
         it.GetInvariants(a2, gamma2);
         Mout << setw(4) << it.GetFinalState() << "<-"
              << setw(2) << it.GetInitState()
              << setw(12) << it.GetOmegafi()
              << setw(11) << a2 << setw(11) <<  gamma2
              << setw(11) << it.GetRamanActivity()
              << setw(11) << it.GetDepolRatio()
              << std::endl;
      }      
   
      Mout << std::endl
           << "   Trans    Activity                  Diff. cross. sect." << std::endl
           << "            (Ang^4 / amu)             (cm^2 1/Sr) (Temp. = 0)" << std::endl;
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      for(const auto& it : tmp)
      {
         midas::stream::ScopedPrecision(4, Mout);
         Mout << setw(4) << it.GetFinalState() << "<-"
              << setw(2) << it.GetInitState();
         midas::stream::ScopedPrecision(16, Mout);
         Mout << setw(26) << it.GetRamanActivity() * std::pow(C_TANG,I_4) * C_FAMU
              << setw(26) << it.GetCrossSect() * C_TANG*C_TANG * C_1/(C_10_8*C_10_8)
              << std::endl;
      }
      Mout << std::endl;
      RamanPrepareAnalysis(tmp);
      Mout << std::endl;
   }
   
   Out72Char(Mout,'+' ,'-','+');  
   Mout << " Raman end." << std::endl << std::endl;
}

/**
 *
 **/
void Vscf::RamanPrepareAnalysis
   (  const std::vector<RamanData>& aData
   )  const
{
   if(!midas::mpi::IsMaster())
   {
      return;
   }
   if ("" == mpVscfCalcDef->GetmVscfAnalysisDir())
   {
      Mout << " Analysis directory not set. No data file will be generated." << std::endl;
      return;
   }

   std::ostringstream fn;
   fn << "midas_raman_" << aData[0].GetOmegaExt() << "_" << mpVscfCalcDef->GetName();
   std::string filename = fn.str();

   Mout << " Generating data file \"" << filename <<"\" for analysis program" << std::endl;
  
   std::string filename_save = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" + filename;
   midas::mpi::OFileStream odat(filename_save);
   odat.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(8, odat);
   odat << "ext_freq = " << aData[0].GetOmegaExt() * C_AUTKAYS << std::endl;
   midas::stream::ScopedPrecision(16, odat);
   for(const auto& it : aData)
   {
      // Avoid noise and zero activities.
      if (  it.GetRamanActivity() < C_I_10_10*C_I_10_10
         )
      {
         continue;
      }

      odat << setw(6) << it.GetFinalState() << "\t"
           << setw(6) << it.GetInitState() << "\t"
           << setw(26) << it.GetOmegafi() * C_AUTKAYS << "\t"
           << setw(26) << it.GetRamanActivity() * std::pow(C_TANG, I_4) * C_FAMU << std::endl;
   }

   // Append filename to list of files read by analysis program.
   std::string filelist = mpVscfCalcDef->GetmVscfAnalysisDir() + "/SpectrumFiles";
   midas::mpi::OFileStream ms(filelist, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);
   ms << filename << endl;
}
