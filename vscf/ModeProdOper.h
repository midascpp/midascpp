/**
************************************************************************
* 
* @file                ModeProdOper.h
*
* Created:             18-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for holding an operator product term 
* 
* Last modified: man mar 21, 2005  11:23
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MODEPRODOPER
#define MODEPRODOPER

class ModeProdOper
{
   private:
      In mNmodesInThis;                      ///< Integer numbers defining the modes in it
      vector<In> mOper;                      ///< Integer numbers defining the operators
      vector<string> mOperStrings;           ///< strings defining the operators
   public:
      void AddOper(In aOp, string aOpS)
      {mOper.push_back(aOp); mOperStrings.push_back(aOpS); mNmodesInThis++;}
                                                                ///< Add new product factor
      In     Nmodes(Nb aCoef) {mNmodesInThis; }                       ///< Give Coeff;
      string OperString(In i) { return mOperStrings[i];}        ///< give string for op
      In     Oper(In i) { return mOper[i];}                     ///< give integer for op
};

#endif

