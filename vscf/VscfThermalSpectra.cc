/**
************************************************************************
* 
* @file                VscfThermalSpectra.cc
*
* Created:             03-12-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Vscf thermal spectra generator
*
*                      Requires modal coefficients and one-mode
*                      integrals
* 
* Last modified: Fri Dec 03, 2010  11:06AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<set>
#include<string>
#include<complex>
#ifdef VAR_MPI
#include <mpi.h>
#include "mpi/Interface.h"
#include "mpi/Impi.h"
#endif

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "ni/OneModeInt.h"
#include "input/ThermalSpectrumDef.h"
#include "fft_wrapper/FFTWrapper.h"
#include "input/GlobalOperatorDefinitions.h"
#include "vcc/ModalIntegrals.h"

// using declarations
using std::set;
using std::complex;

/**
* Search for peaks by finding positions of most negative curvature.
**/
void FindPeaksInSpectra(const MidasVector& aInty, Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep,ThermalSpectrumDef& aSpec)
{
   vector<Nb> peak_pos; 
   vector<Nb> peak_intss; 

   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   Mout << " nfrq = " << nfrq << endl;  
   for (In i=I_3; i<nfrq-I_2; ++i)
   {
      // Seidler: This should be optimized to avoid recalculation.
      Nb der0   = (aInty[i+I_1]-aInty[i-I_1])/aFrqStep/C_2;
      Nb derp1  = (aInty[i+I_2]-aInty[i])/aFrqStep/C_2;
      Nb derm1  = (aInty[i]-aInty[i-I_2])/aFrqStep/C_2;
      Nb derp2  = (aInty[i+I_3]-aInty[i+I_1])/aFrqStep/C_2;
      Nb derm2  = (aInty[i-I_1]-aInty[i-I_3])/aFrqStep/C_2;
      Nb curv0  = (derp1-derm1)/aFrqStep/C_2;
      Nb curvp1 = (derp2-der0)/aFrqStep/C_2;
      Nb curvm1 = (der0-derm2)/aFrqStep/C_2;
      if (curv0<C_0 && curv0<curvm1 && curv0<curvp1)
      {
        peak_pos.push_back(aFrqStart+i*aFrqStep);
        peak_intss.push_back(aInty[i]);
      }
      Mout << " " << i << " " << aInty[i] << endl; 
   }
   string name=gAnalysisDir+"/"+aSpec.GetPrefix()+"td_vvscf";
   string filename = name + "_stick.dat";
   midas::mpi::OFileStream file(filename);
   file << "# IR stick spectrum generated from full spectrum." << endl
   << "# Columns: freq. (cm-1)     Absorption" << endl;
   file.precision(16);
   Mout << " Peak positions, intensities and intensitites normalized to max=1 " << endl; 
   Nb max = C_0;
   for (In i=I_0; i<peak_intss.size(); ++i)
   {
      if (peak_intss[i] > max) max = peak_intss[i]; 
   }
   for (In i=I_0; i<peak_intss.size(); ++i)
   {
      file << peak_pos[i]<< "    0.0" << endl
      << peak_pos[i]<< "    " << peak_intss[i] << endl
      << peak_pos[i]<< "    0.0" << endl;
      Mout << " " << peak_pos[i]<< " " << peak_intss[i]; 
      if (max>C_0) Mout << " " << peak_intss[i]/max << endl; 
   }
}


void Vscf::ConstructThermalSpectrum(ThermalSpectrumDef& aSpec)
{
   Nb temperature=aSpec.GetTemp();

   if (mpVscfCalcDef->IsThermalOccupThr())
   { 
      ThermalLimit(temperature); 
      Mout << " Thermal spectrum calculation employs a tailored VSCF modal basis of (mode,dim): " << endl; 
      for (In i=I_0; i<mpVscfCalcDef->GetNmodesInOcc(); i++)
         Mout << "("<<i<<","<<mpVscfCalcDef->Nmodals(i)<<")";
      Mout << endl; 
   } 
   else
   {
      mpVscfCalcDef->SetModalLimitSize(mpVscfCalcDef->GetNmodesInOcc()); 
      for (In i=I_0; i<mpVscfCalcDef->GetNmodesInOcc(); i++) mpVscfCalcDef->SetModalLimit(i,mpBasDef->Nbas(i)); 
   }

   Nb frq_start=C_0;
   Nb frq_end=aSpec.GetMaxFrq();
   In n_step=aSpec.GetNstep();
   Nb frq_step=frq_end/n_step;
   auto t_end = C_2/frq_end;
   // 1) compute the one-mode populations
   mpVscfCalcDef->OccupancyResize(I_1,mEigVal.Size()+1,false);
   //mpVscfCalcDef->OccupancyResize(I_1,mEigVal.Size(),false);
   mpVscfCalcDef->SetOccupancy(I_0,I_0,temperature);
   std::vector<Nb> one_mode_part_func=CalcOneModePartFunc(temperature,I_0,true);
   MidasVector populations(mEigVal.Size()+I_1);
   // OVE: WHY STORE THEM EXPLICITYLY, THEY ARE IN CALCDEF?????
   // OVE: WHY + 1 above ? 
   mpVscfCalcDef->GetOccupancy().GetRow(populations,I_0);
   // 2) Init vector to hold G(t) function values
   std::vector<complex<Nb> > gt_vector(2*n_step+1,complex<Nb>(C_0,C_0));
   // 3) Construct G(t) function
   ConstructZFunction(gt_vector,populations,aSpec);
   
   Mout  << " For test purposes the last point of the G(t) function is reported: " << gt_vector.back() << std::endl;

   // 4) Do the transform
   FFTWrapper<std::complex<Nb>, std::vector> fft;
   auto fft_result = fft.Compute(gt_vector, t_end, FFTWrapper<std::complex<Nb>, std::vector>::transformType::FORWARD, I_8);
   auto& frq_vector = fft_result.first;
   const auto& fft_vector = fft_result.second;

   // 5) Transform frequency to angular frequency
   auto two_pi = C_2*C_PI;
   for(auto& frq : frq_vector)
   {
      frq *= two_pi;
   }

   // 6) Print result - negative frequencies first
   std::string file_name  = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" + aSpec.GetPrefix() + ".dat";
   std::string total_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" + aSpec.GetPrefix() + "_total.dat";
   midas::mpi::OFileStream output(file_name );
   midas::mpi::OFileStream total (total_name);
   
   output.setf(ios::scientific);
   total .setf(ios::scientific);
   output.precision(16);
   total .precision(16);
   
   output << "# Frq (w/cm^-1)  : -" << frq_end << " <= w <= " << frq_end << endl;
   output << "# Gamma (g/cm^-1): "<< aSpec.GetGamma() << endl;
   output << "# Nstep          : "<< aSpec.GetNstep() << endl;
   output << "# Temperature (K): "<< aSpec.GetTemp() << endl;
   
   total << "# Frq (w/cm^-1)  : 0.0 <= w <= " << frq_end << endl;
   total << "# Gamma (g/cm^-1): "<< aSpec.GetGamma() << endl;
   total << "# Nstep          : "<< n_step << endl;
   total << "# Temperature (K): "<< aSpec.GetTemp() << endl;
   // we only sample from 0 --> T in time-domain => factor of 2
   // plus, the Lorentzian is multiplied by a factor of 1/pi =>
   // rescale

   // Niels: I have tried to rewrite the old code such that it allows for zero padding in the FFT and does not assume anything 
   // about the frequencies and number of points.
   // I am not quite sure how to reproduce the old results exactly. First of all, the frequencies are not the same because we now zeropad.
   // Secondly, in the old code, the absorption and emission was scaled by a "delta" and the frequency which amounted to just scaling by the index 
   // of the FFT point. It was probably correct, but I cannot see why.
   auto npoints = fft_vector.size();
   MidasAssert(npoints % 2 == 0, "Number of points in FFT should be even!");
   MidasVector abspemis(npoints/2,C_0); 
   size_t count = 0;
   for(In i=I_0; i<npoints; ++i)
   {
      const auto& frq = frq_vector[i];

      // Calculate absorption
      auto absorp = C_EXDIP_AU_KMMOL*fft_vector[i].real()/C_AUTKAYS;
      output   << frq << "   " << absorp << "   " << fft_vector[i].real() << "   " << fft_vector[i].imag() << std::endl;

      // For positive frq, calculate emission and sum as well
      auto sum = absorp;
      if (  frq >= C_0
         )
      {
         // emission is calculated from fft_vector at negative frq.
         // The zero frq is located at npoints/2, and then we go 'i' back from that.
         auto emis = -C_EXDIP_AU_KMMOL*fft_vector[npoints/2-i].real()/C_AUTKAYS;

         sum += emis;
         total << frq << "   " << sum << std::endl;

         abspemis[count++] = sum;
      }
   }

   Mout << "\n Absorption Data are on files: \n " << file_name << endl;
   Mout << " Absorption+Emission Data are on files : \n " << total_name << endl;

   FindPeaksInSpectra(abspemis,frq_start,frq_end,frq_step,aSpec); 
}

/**
 * Actual calculation of time and temperature dependent Z function. 
 **/
void Vscf::ConstructZFunction
   (  vector<complex<Nb> >& aGTvec
   ,  const MidasVector& aPop
   ,  ThermalSpectrumDef& aSpec
   )
{
   Nb frq_start=C_0;
   Nb frq_end=aSpec.GetMaxFrq();
   In n_step=aSpec.GetNstep();
   // construct vector of z-values with corresponding
   // vector of function values
   Nb sampling_period=C_2*frq_end;
   Mout << "Sampling period = " << sampling_period << endl;
   Nb time_step=C_1/(sampling_period*C_LIGHT*C_10_2*C_AUTIME);
   Mout << "VscfThermalSpectra will use time step of: " << time_step << endl;
   // for now just do n_step steps in each direction
   
   for(In n_t_op=0;n_t_op<aSpec.GetOperators().size();++n_t_op)
   {
      In op_nr=gOperatorDefs.GetOperatorNr(aSpec.GetOperator(n_t_op));
      if(op_nr==-I_1)
         MIDASERROR("Operator not found in Thermal spectrum generation.");
      map<pair<In,In>,complex<Nb> > b_intermediate;
      vector<complex<Nb> > inact_intermed;
      vector<vector<In> > act_terms;
      // calculate primitive integrals for this operator
      bool save = true;
      string storage = "InMem";
      if (mpVscfCalcDef->IntStorage()!=I_0) 
      {
         storage = "OnDisc";
         if(midas::mpi::GlobalSize() > I_1) 
         {
            Mout << "Warning! for MPI calculations, intgral storage in "
                 << "thermal spectrum MUST be InMem ($3INTSTORAGE = 0)" << endl;
            storage="InMem";
         }
      }
      OneModeInt one_mode_int(&gOperatorDefs[op_nr],mpBasDef,storage,save);
      one_mode_int.CalcInt();
      Mout << " Primitive integrals done for operator " << gOperatorDefs[op_nr].Name() << endl; 
   
      Mout << " Update all one mode integrals to prepare modal basis calcs " << endl; 
      /// Operator is updated and modal integrals are calculated. 
      PutOperatorToWorkingOperator(&gOperatorDefs[op_nr],&one_mode_int); 
      //Mout << " Modal Integrals ready  " << endl; 

      In ns=0;
      // Get gamma
      Nb gamma=aSpec.GetGamma()/C_2;
      gamma*=C_AUTIME*C_LIGHT*C_10_2;
      // Check maximum decay
      Nb max_decay=C_2*Nb(n_step)*time_step;
      max_decay=gamma*max_decay*C_2*C_PI;
      max_decay=exp(-max_decay);
      if(max_decay>C_I_10_5) 
      {
         Mout << "WARNING: Maximum decay function is: " << max_decay << "." << endl;
         Mout << "Consider increasing the number of time-steps." << endl;
      }
      // Done checking, now work!
      Mout << " Required iterations: " << 2*n_step+1 << endl
           << " Progress (one . is 1%):" << endl << " ";
      while(ns<2*n_step+1)
      {
         // 0) write progress
         if(ns>0 && n_step > 50 && ns%((2*n_step+1)/100)==0) // WARNING, The 50 necessary to avoid div by zero in % operation 
             Mout << "." << std::flush;
#ifdef VAR_MPI
         // only calculate the a subset of function values
         if(ns%midas::mpi::GlobalSize() != midas::mpi::GlobalRank() ) 
         {
            ns++;
            continue;
         }
#endif
         In i_time=ns;
         // 1) Clear intermediate quantities
         //ClearIntermediateXandY(op_nr);
         // 2) Set time and compute value of G(t)
         Nb this_time=(Nb)i_time*time_step;
         complex<Nb> this_g=ValueForTime(this_time,aPop,op_nr,one_mode_int,b_intermediate,
            inact_intermed,act_terms);
         // 3) Multiply by exp(-2*PI*(\gamma/2)*t) => spectrum convoluted by Lorentzian
         //    with FWHM = \gamma. \gamma should be in s^-1 (in au) to make exponential
         //    dimensionless
         //Mout << " op_nr " << op_nr << " aG: " << this_g << endl; 
         Nb fac=exp(-C_2*C_PI*gamma*this_time);
         this_g*=fac;
         // 4) Add and increment
         aGTvec[ns]+=this_g;
         //Mout << " after add and increment " << this_g << " step " << ns << endl; 
         ns++;
      }
      Mout << endl;

      //ClearIntermediateZ(op_nr);
      b_intermediate.clear();
      inact_intermed.clear();
      act_terms.clear();
      RestoreOrigOper(); 
   }
   // now do MPI part
#ifdef VAR_MPI
   // 1) make arrays of real and imag. parts
   double *re_gt=new double[aGTvec.size()];
   double *im_gt=new double[aGTvec.size()];
   for(In i=0;i<aGTvec.size();i++) 
   {
      re_gt[i]=aGTvec[i].real();
      im_gt[i]=aGTvec[i].imag();
   }
   
   if(midas::mpi::GlobalRank() == I_0)
   {
      // 2) receive contributions from all other ranks
      for(In i = I_1; i < midas::mpi::GlobalSize(); i++) 
      {
         double *cont=new double[aGTvec.size()];
         // real part
         midas::mpi::detail::WRAP_Recv(cont,aGTvec.size(),MPI_DOUBLE,i,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         for(In j=0;j<aGTvec.size();j++) 
         {
            re_gt[j]+=cont[j];
         }
         // imag. part
         midas::mpi::detail::WRAP_Recv(cont,aGTvec.size(),MPI_DOUBLE,i,1,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         for(In j=0;j<aGTvec.size();j++) 
         {
            im_gt[j]+=cont[j];
         }
         // remember to free cont
         delete[] cont;
      }
   }
   else 
   {
      midas::mpi::detail::WRAP_Send(re_gt,aGTvec.size(),MPI_DOUBLE,0,0,MPI_COMM_WORLD);
      midas::mpi::detail::WRAP_Send(im_gt,aGTvec.size(),MPI_DOUBLE,0,1,MPI_COMM_WORLD);
   }
   midas::mpi::Barrier();
   
   // 3) Broadcast complete GT vectors to all other ranks
   midas::mpi::detail::WRAP_Bcast(re_gt,aGTvec.size(),MPI_DOUBLE,0,MPI_COMM_WORLD);
   midas::mpi::detail::WRAP_Bcast(im_gt,aGTvec.size(),MPI_DOUBLE,0,MPI_COMM_WORLD);
   
   // finally, put the c-arrays into the aGTvec
   for(In i=0;i<aGTvec.size();i++) 
   {
      aGTvec[i]=complex<Nb>(re_gt[i],im_gt[i]);
   }
   
   // remember to free arrays
   delete[] re_gt;
   delete[] im_gt;
#endif
}

complex<Nb> Vscf::ValueForTime(const Nb& aT,const MidasVector& aPop, const In op_nr,
   OneModeInt& aOneModeInt, map<pair<In,In>,complex<Nb> >& aBintermed,
   vector<complex<Nb> >& aInactIntermed, vector<vector<In> >& aActTerms)
{
   // loop over operator terms
   complex<Nb> result=complex<Nb>(C_0,C_0);
   // sum over N_t
   In int_terms=I_0;
   if(aActTerms.size()==I_0)
      CreateActTermsVec(aT,aPop,op_nr,aOneModeInt,aBintermed,aInactIntermed,aActTerms);
   for(In i=I_0;i<gOperatorDefs[op_nr].Nterms();++i)
   {
      // Find the modes active in term t
      vector<In> modes_t;
      for(In k=I_0;k<gOperatorDefs[op_nr].NfactorsInTerm(i);k++) 
      {
         modes_t.push_back(gOperatorDefs[op_nr].ModeForFactor(i,k));
      }
      Nb coef_help=gOperatorDefs[op_nr].Coef(i);
      complex<Nb> c_t=complex<Nb>(coef_help,C_0);
      // first active
      for(In j1=I_0;j1<aActTerms[i].size();j1++)
      {
         In j=aActTerms[i][j1];
         // Find the modes active in term t'
         vector<In> modes_tp;
         for(In k=I_0;k<gOperatorDefs[op_nr].NfactorsInTerm(j);k++) 
         {
            modes_tp.push_back(gOperatorDefs[op_nr].ModeForFactor(j,k));
         }
         coef_help=gOperatorDefs[op_nr].Coef(j);
         complex<Nb> c_tp=complex<Nb>(coef_help,C_0);
         // now we make the intersection, and (maybe) compute the aterm
         vector<In> intersection(modes_t.size()+modes_tp.size(),I_0);
         vector<In>::iterator it;
         it=set_intersection(modes_t.begin(),modes_t.end(),modes_tp.begin(),
            modes_tp.end(),intersection.begin());
         intersection.erase(it,intersection.end());
         //Mout << "Modes_t: " << modes_t << " Modes_tp: " << modes_tp
              //<< " => Intersec: " << intersection << endl;
         complex<Nb> a_term=complex<Nb>(C_1,C_0);
         pair<In,In> p=std::make_pair(i,j);
         complex<Nb> b_term=aBintermed[p];
         // now A-term.
         a_term=ComputeATerm(aT,intersection,aPop,aOneModeInt,i,j,op_nr);
         if(i!=j) 
         {
            a_term+=ComputeATerm(aT,intersection,aPop,aOneModeInt,j,i,op_nr);
            a_term/=C_2;
         }
         complex<Nb> contrib=c_t*c_tp*a_term*b_term;
         //Mout << " j1, " << j1 << " a " << a_term << " b " << b_term << endl; 
         if(i==j)
            contrib/=C_2;
         result+=contrib;
         //Mout << " contrib " << contrib << " result " << result << endl; 
      }
      // then inactive
      result+=c_t*aInactIntermed[i];
   }
   result*=C_2;
   return result;
}

void Vscf::CreateActTermsVec(const Nb& aT,const MidasVector& aPop, const In op_nr,
   OneModeInt& aOneModeInt, map<pair<In,In>,complex<Nb> >& aBintermed,
   vector<complex<Nb> >& aInactIntermed, vector<vector<In> >& aActTerms)
{
   In max_pairs=I_0;
   for(In i=I_0;i<gOperatorDefs[op_nr].Nterms();i++)
   {
      if(aInactIntermed.size()==I_0) {
         aInactIntermed.resize(gOperatorDefs[op_nr].Nterms(),C_0);
      }
      vector<In> modes_t;
      for(In k=I_0;k<gOperatorDefs[op_nr].NfactorsInTerm(i);k++) {
         modes_t.push_back(gOperatorDefs[op_nr].ModeForFactor(i,k));
      }
      vector<In> addition;
      for(In j=i;j<gOperatorDefs[op_nr].Nterms();j++)
      {
         vector<In> modes_tp;
         for(In k=I_0;k<gOperatorDefs[op_nr].NfactorsInTerm(j);k++) {
            modes_tp.push_back(gOperatorDefs[op_nr].ModeForFactor(j,k));
         }
         vector<In> intersection(modes_t.size()+modes_tp.size(),I_0);
         vector<In>::iterator it;
         it=set_intersection(modes_t.begin(),modes_t.end(),modes_tp.begin(),
            modes_tp.end(),intersection.begin());
         intersection.erase(it,intersection.end());
         if(intersection.size()>I_0)
            addition.push_back(j);
         // compute B-terms in the same go!
         complex<Nb> b_term=complex<Nb>(C_1,C_0);
         complex<Nb> bp_term=complex<Nb>(C_1,C_0);
         vector<In> diff(modes_t.size(),I_0);
         it=set_difference(modes_t.begin(),modes_t.end(),intersection.begin(),
            intersection.end(),diff.begin());
         diff.erase(it,diff.end());
         // difference set is set up, compute!
         if(diff.size()!=I_0)
            b_term=ComputeBTerm(diff,aPop,aOneModeInt,i,op_nr);
         diff.clear();
         diff.resize(modes_tp.size());
         it=set_difference(modes_tp.begin(),modes_tp.end(),intersection.begin(),
            intersection.end(),diff.begin());
         diff.erase(it,diff.end());
         // difference set is set up, compute!
         if(diff.size()!=I_0)
            bp_term=ComputeBTerm(diff,aPop,aOneModeInt,j,op_nr);
         // update aBintermed
         pair<In,In> p1=std::make_pair(i,j);
         complex<Nb> p2=b_term*bp_term;
         aBintermed.insert(std::make_pair(p1,p2));
         Nb coef_help=gOperatorDefs[op_nr].Coef(j);
         complex<Nb> c_tp=complex<Nb>(coef_help,C_0);
         if(intersection.size()==I_0)
            aInactIntermed[i]+=c_tp*b_term*bp_term;
      }
      //Mout << "Done with term: " << i << " of: " << gOperators[op_nr].Nterms() << endl;
      aActTerms.push_back(addition);
      if(addition.size()>max_pairs)
         max_pairs=addition.size();
   }
   //Mout << "Done with active terms manip. Max. number of active pairs: " << max_pairs << endl;
}

complex<Nb> Vscf::ComputeATerm(const Nb& aT,const vector<In>& aModes,
   const MidasVector& aPop,OneModeInt& aOneModeInt, 
   const In& aTerm, const In& aTermP, const In& aOpNr)
{
   // now compute the stuff

   // New way - old simple equations 
   // sum_i,f p_i mu_if^s mu_if^sp ep(iDe_f t) exp(-iDe_i t) 
   string all="all"; 
   complex<Nb> result2(C_1,C_0);
   for(In i=0;i<aModes.size();i++)
   {
      In i_g_mode   = gOperatorDefs[aOpNr].GetGlobalModeNr(aModes[i]);
      In i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In n_modals   = mpVscfCalcDef->Nmodals(i_bas_mode); 
      In n_bas      = mpBasDef->Nbas(i_bas_mode); 
      In pop_offset=mOccModalOffSet[i_g_mode]+1;
      In oper =aOneModeInt.GetmpOpDef()->OperForOperMode(aTerm,aModes[i]);
      In operp=aOneModeInt.GetmpOpDef()->OperForOperMode(aTermP,aModes[i]);

      MidasVector mus(n_modals*n_modals); 
      MidasVector musp(n_modals*n_modals); 
      mus.MatrixRowByRow(pIntegrals()->GetIntegrals(i_g_mode, oper));
      musp.MatrixRowByRow(pIntegrals()->GetIntegrals(i_g_mode, operp));

      MidasVector modal_energies(n_modals,C_0);
      mEigVal.DataIo(IO_GET,modal_energies,n_modals-1,mOccModalOffSet[i_g_mode]+1,1,1);

      complex<Nb> contrib=complex<Nb>(C_0,C_0);
      for (In im=I_0; im< n_modals; im++)
      {
         //Nb cos_part_i=cos(modal_energies[im]*aT);
         //Nb sin_part_i=sin(modal_energies[im]*aT);
         for (In fm=I_0; fm< n_modals; fm++)
         {
            Nb factor =  aPop[pop_offset+im]*mus[im*n_modals+fm]*musp[im*n_modals+fm]; 
            Nb cos_part=cos((modal_energies[fm]-modal_energies[im])*aT);
            Nb sin_part=sin((modal_energies[fm]-modal_energies[im])*aT);
            Nb re_part = cos_part*factor; 
            Nb im_part = sin_part*factor; 
            contrib+= complex<Nb>(re_part,im_part);
            //Mout << " im , fm " << im << " " << fm << " pop " << aPop[pop_offset+im] << " contrib " << contrib << endl; 
         }
      }
      result2*=contrib;
   }
   return result2; 
}


complex<Nb> Vscf::ComputeBTerm(const vector<In>& aModes,
   const MidasVector& aPop,OneModeInt& aOneModeInt, 
   const In& aTerm, const In& aOpNr)
{
   // now compute the stuff
   // 1) product over the modes in populations vector
   complex<Nb> result2=complex<Nb>(C_1,C_0);

   // New way = old equation, straight! 

   Nb int_val; 
   for(In i=0;i<aModes.size();i++)
   {
      In i_g_mode   = gOperatorDefs[aOpNr].GetGlobalModeNr(aModes[i]);
      In i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In n_modals   = mpVscfCalcDef->Nmodals(i_bas_mode); 
      In n_bas      = mpBasDef->Nbas(i_bas_mode); 
      In pop_offset=mOccModalOffSet[i_g_mode]+1;
      In oper=aOneModeInt.GetmpOpDef()->OperForOperMode(aTerm,aModes[i]);

      Nb  contrib=C_0;
      for (In im=I_0; im< n_modals; im++)
      {
         int_val = pIntegrals()->GetElement(i_g_mode,oper,im,im); 
         contrib+=aPop[pop_offset+im]*int_val; 
         //Mout << " im " << im << " pop " << aPop[pop_offset+im] << " int " << int_val << " contrib " << contrib << endl; 
      }
      result2*=complex<Nb>(contrib,C_0);
      //Mout << " NEW mode " << i_g_mode << " contribu " << contrib << endl; 
   }
//   Mout << " BTerm result new way " << result2 << endl; 
   return result2;
 
}

