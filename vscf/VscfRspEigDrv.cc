/**
************************************************************************
* 
* @file                VscfRspEigDrv.cc
*
* Created:             01-05-2009
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driving response eigenvalue calculation 
* 
* Last modified: Sat May 15, 2010  11:59PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<set>
#include<string>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"
#include "mpi/Impi.h"

#include "it_solver/NonHermitianEigenvalueSolver.h"
#include "it_solver/TensorDataContEigenvalueSolver.h"

// using declaration
using std::set;

/**
* Drive calculation of response eigenvalues 
* */
void Vscf::RspEigDrv(bool aRight)
{
   if (gDebug || mpVscfCalcDef->RspIoLevel() > I_5) 
   {
      Mout << " In Vscf::RspEigDrv(): Find response eigenvalues." << endl;
   }
   Out72Char(Mout,' ','*','*');
   Mout << "\n\n Begin response Eigenvalue Calculation." << endl;
   if (!aRight)
   {
      Mout << " NB: Calculated solutions are left eigenvalues and eigenvectors.\n" << std::endl;
   }
   else
   {
      Mout << "\n" << std::endl;
   }
  
   bool restart = mpVscfCalcDef->RspRestart();
   In n_roots   = mpVscfCalcDef->GetRspNeig();
   In nvecsize  = NrspPar();
   if (n_roots>nvecsize) 
   {
      Mout << " The number of roots is reset to the dimension of the space." << endl;
      n_roots = nvecsize;
   }

   In n_targs = I_0;
   if (mpVscfCalcDef->RspAllFirstOvertones() || mpVscfCalcDef->RspAllFundamentals() ||
       mpVscfCalcDef->RspFundamentals() || mpVscfCalcDef->RspFirstOvertones())
   {
      RspAutoTarget();
   }
   if (mpVscfCalcDef->RspTargetSpace()) 
   {
      n_targs = PrepareRspTargetStates();               // Prepare response target states.
      if (n_targs > nvecsize)
      {
         n_targs = nvecsize;
      }
      if(n_targs == 0)
      {
         MIDASERROR("n_targs is 0");
      }
      n_roots = n_targs;
      Mout << " Number of roots reset to the number of target states." << endl;
   }
   Mout << " Solve for " << mpVscfCalcDef->GetRspNeig() << " response eigenvalues." << std::endl;
   Mout << " I will actually solve for " << n_roots << " roots..." << std::endl;

   // get transformer 
   Transformer* trf = GetRspTransformer(aRight);
   const string eigvec_filename = mpVscfCalcDef->GetName() + "_rsp_eigvec_" + (aRight? "":"left_");
   const string eigval_filename = mpVscfCalcDef->GetName() + "_rsp_eigval" + (aRight? "":"_left");
   
   //
   // Setup iterative equation solver
   //
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   ItEqSol eqsol(mpVscfCalcDef->VecStorage(), mpVscfCalcDef->GetmVscfAnalysisDir());
   eqsol.SetEigEq();
   eqsol.SetNeq(n_roots);
   eqsol.SetTransformer(trf);
   eqsol.SetDim(nvecsize);
   eqsol.SetResidThreshold(mpVscfCalcDef->GetRspItEqResidThr());
   eqsol.SetResidThresholdRel(mpVscfCalcDef->GetRspItEqResidThrRel());
   eqsol.SetRescueMax(mpVscfCalcDef->GetRspItEqRescueMax());
   eqsol.SetEnerThreshold(mpVscfCalcDef->GetRspItEqEnerThr());
   eqsol.SetNiterMax(mpVscfCalcDef->GetRspItEqMaxIter());
   eqsol.SetRedDimMax(mpVscfCalcDef->GetRspRedDimMax());
   eqsol.SetBreakDim(mpVscfCalcDef->GetRspRedBreakDim());
   eqsol.SetIoLevel(mpVscfCalcDef->RspIoLevel());
   eqsol.SetTimeIt(mpVscfCalcDef->RspTimeIt());
   eqsol.SetPaired(also_de_ex);
   eqsol.SetDiagMeth(mpVscfCalcDef->RspDiagMeth());
   eqsol.SetOlsen(mpVscfCalcDef->Olsen());
   eqsol.SetTrueHDiag(mpVscfCalcDef->GetRspTrueHDiag());
   eqsol.SetTrueADiag(mpVscfCalcDef->GetRspTrueADiag());
   eqsol.SetImprovedPrecond(mpVscfCalcDef->GetRspImprovedPrecond());
   eqsol.SetPrecondExciLevel(mpVscfCalcDef->GetRspPrecondExciLevel());
   eqsol.SetSolVecFileName(eigvec_filename);
   eqsol.SetLevel2Solver(mpVscfCalcDef->GetRspLevel2Solver());
   eqsol.SetNresvecs(mpVscfCalcDef->GetRspNresvecs());
   eqsol.SetHarmonicRR(mpVscfCalcDef->GetRspHarmonicRR());
   eqsol.SetEnerShift(mpVscfCalcDef->GetRspEnerShift());
   
   vector<DataCont> eig_vecs;         // The solution vectors on return
   eig_vecs.reserve(mpVscfCalcDef->GetRspRedDimMax());
   string storage_mode = "OnDisc";
   if (mpVscfCalcDef->VecStorage() == I_0) storage_mode = "InMem";
   DataCont tmp_datacont;

   // setup structure for eigenvectors
   for (In i_eq=I_0; i_eq<n_roots; i_eq++)
   {
      eig_vecs.push_back(tmp_datacont);
      eig_vecs[i_eq].NewLabel(eigvec_filename + std::to_string(i_eq));

      bool res_suc = false;
      if (restart) 
      {
         if (RspRestartFromPrev(trf,eig_vecs[i_eq],i_eq,nvecsize,storage_mode))
         {
            Mout << " Restart vector read in succesfully for root " << i_eq << "." << endl;
            Mout << " Norm of restart vector: " << eig_vecs[i_eq].Norm()
                 << ", size: " << eig_vecs[i_eq].Size() << std::endl;
            res_suc=true;
         } 
         else          
         {
            Mout << " Restart failed." << endl; 
            Mout << " Set vector #" << i_eq << " to zero vector first, later standard guess." << endl; 
            eig_vecs[i_eq].ChangeStorageTo("OnDisc");
            eig_vecs[i_eq].SetNewSize(nvecsize);
            eig_vecs[i_eq].Zero();
         }
      }
      else
      {
         eig_vecs[i_eq].ChangeStorageTo(storage_mode);
         eig_vecs[i_eq].SetNewSize(nvecsize);
         eig_vecs[i_eq].Zero();
      }
      if (!res_suc && mpVscfCalcDef->RspTargetSpace())
      {
         GetTargetForState("RspTargetVector", i_eq, eig_vecs[i_eq]);
      }

      eig_vecs[i_eq].SaveUponDecon(true);
   }

   In num_vecs=mpVscfCalcDef->GetRspNresvecs();
   if (num_vecs>n_roots)
   {
      for (In i_eq=n_roots;i_eq<num_vecs;i_eq++)
      {
         eig_vecs.push_back(tmp_datacont);
         eig_vecs[i_eq].NewLabel(eigvec_filename + std::to_string(i_eq));

         //bool res_suc = false;
         //civecs[i_eq].ChangeStorageTo(storage_mode);
         //const ModeCombiOpRange& mcr = (trf.GetTransXvec()).GetModeCombiOpRange();
         if (RspRestartFromPrev(trf,eig_vecs[i_eq],i_eq,nvecsize,storage_mode))
         {
            Mout << " Restart (trial) vector read in succesfully for root " << i_eq << "." << endl;
            Mout   << " Norm of restart vector: " << eig_vecs[i_eq].Norm()
               << ", size: " << eig_vecs[i_eq].Size() << endl;
            //res_suc=true;
         } 
         else          
         {
            Mout << " Restart failed." << endl; 
            //Mout << " Restart flag turned off and start vector guess is default/vmp/vcih2."
            //   << endl;
            Mout << " Set vector #" << i_eq << " to zero vector first, later it will be deleted." << endl; 
            eig_vecs[i_eq].ChangeStorageTo("OnDisc");
            eig_vecs[i_eq].SetNewSize(nvecsize);
            eig_vecs[i_eq].Zero();
            //res_suc = false;
         }
         eig_vecs[i_eq].SaveUponDecon(true);
      }
   }

   restart = restart || mpVscfCalcDef->RspTargetSpace();
   if (mpVscfCalcDef->RspTargetSpace())
   {
      eqsol.SetTargetSpace(mpVscfCalcDef->RspTargetSpace());
      eqsol.SetTargetFileName("RspTargetVector_");
      eqsol.SetNtargs(n_targs);
      eqsol.SetTargetingMethod(mpVscfCalcDef->GetRspTargetingMethod());
      eqsol.SetOverlapMin(mpVscfCalcDef->GetRspOverlapMin());
      eqsol.SetOverlapSumMin(mpVscfCalcDef->GetRspOverlapSumMin());
      eqsol.SetEnergyDiffMax(mpVscfCalcDef->GetRspEnergyDiffMax());
      eqsol.SetResidThrForOthers(mpVscfCalcDef->GetRspResidThrForOthers());
      eqsol.SetEnerThrForOthers(mpVscfCalcDef->GetRspEnerThrForOthers());
   }
   eqsol.SetRestart(restart);
   WriteRspRestartInfo(trf,nvecsize);
   MidasVector eig_vals;
   bool sequence=mpVscfCalcDef->GetRspEigValSeqSet(); // change it to Rsp...
   Timer time_solve;
   if (sequence)
   {
      In n_roots_at_a_time = mpVscfCalcDef->GetRspEigValSeq(); 
      In n_seq       = n_roots/n_roots_at_a_time;
      Mout << "\n Sequence calculation of eigenvalues with " << n_roots_at_a_time
           << " roots at a time in " << n_seq << " batches." << std::endl;
      In n_rest = n_roots - n_seq*n_roots_at_a_time;
      if (n_rest!=I_0)
         n_seq++;
      In n_roots_now = I_0;
      for (In i_seq=I_0; i_seq<n_seq; i_seq++)
      {
         if (i_seq!=n_seq-I_1 || n_rest == I_0)
            n_roots_now += n_roots_at_a_time;
         if (i_seq==n_seq-I_1 && n_rest!= I_0)
            n_roots_now += n_rest;
         eqsol.SetNeq(n_roots_now);
         if (mpVscfCalcDef->RspTargetSpace())
         {
            eqsol.SetNtargs(n_roots_now);
         }
         Mout << endl << " Sequence nr. " << i_seq << " with " << n_roots_now << " roots." << endl;
         eqsol.Solve(eig_vals, eig_vecs);
         eqsol.SetRestart(true); // Now restart should certainly be set. 
      }
   }
   // Solve it.
   else if (eqsol.Solve(eig_vals,eig_vecs))
   {
      Mout << " Response eigen-value equations converged to requested threshold." << endl;
   }
   else
   {
      Mout << " Response eigen-value equations NOT converged to requested threshold." << endl;
      string s1 = " Response eigen-value equations not converged, for calc. = " + mpVscfCalcDef->GetName();
      MidasWarning(s1);
   }
   if (gTime)
   {
      string s_time = " Wall time used in Solve ";
      time_solve.WallOut(Mout,s_time);
   }
   
   // In case of a left-hand calculation, match new solutions with previously calculated
   // right-hand solutions. This is important for consistency between solution index
   // numbers.
   if (! aRight)
   {
     MatchLeftRightRspEigVecs(eig_vals, eig_vecs);
   }
   DataCont eig_val_dc(eig_vals,"OnDisc",eigval_filename,true);
   
   RspEigOutput(eig_vals, eig_vecs);
   
   //
   // print out transformer summary
   //
   Mout << "\n" << trf << std::endl;
 
   // Update number of roots which may have changed due to extra solutions found by
   // targeting strategies. For consistency, we only do this for right-hand calculations.
   if (aRight)
      mpVscfCalcDef->SetRspNeig(eig_vals.Size());

   CleanupRspTransformer(trf);

   Mout << "\n\n End of response eigenValue calculation\n\n" << endl;
   Out72Char(Mout,' ','*','*');
}

/**
 * Write summary of response excitation energy calculation done
 * in RspEigDrv().
 */
void Vscf::RspEigOutput(MidasVector& aEigVals, vector<DataCont>& aEigVecs)
{
   In n_roots = aEigVals.Size();
   
   if (  n_roots != aEigVecs.size()
      )
   {
      Mout  << " N_ROOTS:  " << n_roots << "\n"
            << " N_VECS:   " << aEigVecs.size() << "\n"
            << std::flush;
      MIDASERROR("Number of eigvecs and eigvals do not match!");
   }

   Mout << "\n\n Results of reduced space iterative response eigenvalue solution in au and cm-1..."
        << endl;
   In n_out = min(I_5,NrspPar());
   Mout << "\n\n Analysis of eigen vector includes the " << n_out
        << " largest elements of vector.\n"
        << " The first addresses set refers to modes and modals excited to." << endl;
   //Mout << "\n Nb! modals space ordering with occupied first "
      //<< "(no matter which as zero " <<endl;
   Nb conv = C_AUTKAYS;
   Nb e_ref_tot =  mpVscfCalcDef->GetEfinal(); // Give only VSCF: GetEtot();
   Mout << "\n Reference state Total Energy: " << e_ref_tot << " au\n"
        << " Reference state Total Energy: " << e_ref_tot*conv << " cm-1" << endl;
   
   // Vectors storing the address and coefficient of the largest element for
   // each root. Used for summary.
   vector<In> summ_addr(n_roots);
   vector<Nb> summ_coef(n_roots);
   vector<In> summ_ex(n_roots);
   
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   midas::stream::ScopedPrecision(16, Mout);
   for (In i=I_0;i<n_roots;i++)
   {
      In ex=I_1;
      Mout << endl << endl << endl;
      if (also_de_ex)
      {
          ex=ExciOrDeExci(aEigVecs[i]);
          if (ex==I_1) Mout << " Primarily excitation vector" << endl;
          if (ex==-I_1) Mout << " Primarily de-excitation vector" << endl;
      }
      summ_ex[i] = ex;      // For summary.
      Mout << " Rsp_Root_" << i << " EigenValue                      " 
           << aEigVals[i] << " au  "
           << "  " << ex << endl;
      Mout << " Rsp_Root_" << i << " EigenValue                      " 
           << aEigVals[i]*conv << " cm-1"
           << "  " << ex << endl;
      Mout << " Rsp_Root_" << i << " Total Energy                    " 
           << aEigVals[i]+ex*e_ref_tot << " au  "
           << "  " << ex <<  endl;
      Mout << " Rsp_Root_" << i << " Total Energy                    " 
           << (aEigVals[i]+ex*e_ref_tot)*conv << " cm-1"
           << "  " << ex << endl;

      // If also de-excitation part, then renormalize eigenvectors. The problem is that the
      // solver uses normal orthogonal vectors. The true requirement is XSX=1; 
      if (also_de_ex)
      {
         In half = NrspPar()/I_2;
         Nb ols=Dot(aEigVecs[i],aEigVecs[i],I_0,half);
         ols-=Dot(aEigVecs[i],aEigVecs[i],half,half);
         Nb ol=fabs(ols); 
         Nb oli=C_1/fabs(ol); 
         //Mout << " ols " << ols << " ol " << ol << " oli " << oli << endl; 
         if (fabs(ol-C_1)>C_NB_EPSILON*C_10) 
         {
            Mout << " scaling eigenvector to unit X S^2 X norm " << endl; 
            aEigVecs[i].Scale(sqrt(oli)); 
         } 

         MidasVector o(n_roots,C_0);
         for (In j=I_0;j<n_roots;j++)
         {
            o[j]=Dot(aEigVecs[i],aEigVecs[j],I_0,half);
            o[j]-=Dot(aEigVecs[i],aEigVecs[j],half,half);
            //Mout << " Overlap " << i << " " << j << " " << o[j]<<endl; 
         }
         o[i]=o[i]-ols*oli; 
         if (mpVscfCalcDef->RspIoLevel() > 5) Mout << "Overlap vector -delta_i norm " << o.Norm() << endl; 
         if (gDebug || mpVscfCalcDef->RspIoLevel() > 20) 
         {
            Mout << "Overlap vector -delta_i " << endl; 
            for (In i=0;i<o.Size();i++) Mout << " " << o[i] << endl; 
         } 
         if (o.Norm()>C_NB_EPSILON*C_10_5) 
         {
            Mout << "Overlap vector -delta_i " << o << endl; 
            string s = " Too large overlaps in paired rsp eig";
            Mout <<  s  << endl; 
            MidasWarning(s); 
         }
      }

      if (gDebug || mpVscfCalcDef->RspIoLevel()>100)
      {
         Mout << "\n Rsp_Root_" << i << " eig vec" <<  endl << " " << aEigVecs[i] << endl;
      }

      if (aEigVecs[i].Norm() == C_0)
      {
         Mout << endl
              << " Vector has zero norm. Probably a dummy left-hand solution." << endl << endl;
         continue;
      }

      Mout << "\n Largest elements of vector:\n" << endl;
      vector<Nb> largest_coef(n_out);
      vector<In> add_largest_coef(n_out);
      aEigVecs[i].AddressOfExtrema(add_largest_coef,largest_coef,n_out,I_2);
      summ_addr[i] = add_largest_coef[I_0];             // For summary.
      summ_coef[i] = largest_coef[I_0];
      
      for (In j=I_0; j<n_out; j++)
      {
         if (largest_coef[j] >= C_0) Mout << " +";
         else Mout << " ";
         Mout << setw(23) << left << largest_coef[j];
         if (largest_coef[j] < C_0) Mout << " ";
         Mout << "* Psi_" <<  setw(9) << left << add_largest_coef[j];
         RspModesAndLevelsOut(Mout,add_largest_coef[j]);
         Mout << endl;
      }
      if(mpVscfCalcDef->GetMolMCs().size() != I_0)
         PrintWeightInfo(mpVscfCalcDef->GetMolMCs(), aEigVecs[i], I_1);
   }
   
   // Short summary:
   Mout << "\n\n Summary of response eigenvalues:\n";
   Out72Char(Mout, '+', '-', '+');
   Mout << " Root:    Exci. modes:         Coef:   Exci. energy (cm-1):    "
        << "Tot. energy (cm-1):\n";
   Mout.setf(ios_base::fixed, ios_base::floatfield);
   for (In i=I_0; i<n_roots; i++)
   {
      In ex = summ_ex[i];
      Mout << setw(5) << right << i << "     ";
      if (aEigVecs[i].Norm() == C_0)
      {
         Mout << "Dummy (zero norm)" << endl;
         continue;
      }
      RspModesAndLevelsOut(Mout, summ_addr[i], false);
      midas::stream::ScopedPrecision(3, Mout);
      Mout << right << setw(6) << summ_coef[i];
      midas::stream::ScopedPrecision(15, Mout);
      Mout << setw(23) << aEigVals[i]*conv
           << setw(23) << (aEigVals[i]+ex*e_ref_tot)*conv << endl;
   }
   Mout << endl;
   
   RspExciLevelWeight(aEigVecs, n_roots);
   
   Mout.setf(ios_base::scientific, ios_base::floatfield);
}

/**
* For an address aI find the modes and levels excited. 
* */
In Vscf::ExciOrDeExci(DataCont& arDc)
{
   In ex=I_1;
   if (!mpVscfCalcDef->DoNoPaired())
   {
      In i_begin = I_0;
      In n = NrspParHalf();
      Nb norm_ex = Dot(arDc,arDc,i_begin,n);
      Nb norm_deex = Dot(arDc,arDc,i_begin+n,n);
      if (norm_deex>norm_ex) ex=-I_1;
   }
   return ex;
}
/**
* For an address aI find the modes and levels excited. 
* */
void Vscf::RspModesAndLevelsOut(ostream& arOut, const In& aI, bool aAllOut)
{
   In mode     =  -I_1;
   In mode_lev =  -I_1;
   In nmodes_all = pVscfCalcDef()->GetNmodesInOcc();
   In n_half     = NrspPar()/I_2;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   bool de_ex = false;
   In i_input = aI;
   if (also_de_ex && i_input >= n_half) de_ex=true;
   In add = I_0;
   if (de_ex) i_input-=n_half;
   bool found = false;
   for (LocalModeNr i_op_mode=I_0;i_op_mode<nmodes_all;i_op_mode++) 
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);
      if (!found)
      {
         if (i_input < add+nbas-I_1)
         {
            mode     = i_op_mode;
            mode_lev = i_input-add+I_1;
            found=true;
         }
      }
      //Mout << " i_input " << i_input << " add + " << add+nbas-I_1;
      //Mout << " mode_lev " << mode_lev << endl;
      add += nbas-I_1;
   }
   if (!found)
   {
      mode     = nmodes_all-I_1;
      mode_lev = i_input-add+I_1;
   }
   if (de_ex) mode_lev=-mode_lev;

   ostringstream out;
   out << "(";
   out << mode<<":"<<mode_lev; 
   out << ")";
   string out_string = out.str();
   arOut << left << setw(20) <<  out_string;

   In idx = mode_lev;
   if (aAllOut)
   {
      vector<In> all(nmodes_all);
      for (In i=I_0;i<nmodes_all;i++) 
      {
         all[i] = pVscfCalcDef()->GetOccMode(i);
      }
      In iocc = pVscfCalcDef()->GetOccMode(mode);
      if (!de_ex)
      {
         if (idx>iocc) 
            all[mode] =idx; 
         else 
            all[mode] =idx-I_1; 
      }
      else
      {
         if (-idx>iocc) 
            all[mode] =idx; 
         else 
            all[mode] =idx+I_1; 
      }
      ostringstream out;
      out << " [";
      for (In i=I_0;i<nmodes_all;i++) 
      {
         out << all[i];
         if (i!=nmodes_all-I_1) out <<",";
      }
      out << "]";
      string out_string = out.str();
      arOut << right << setw(20) <<  out_string;
   }
}

/**
* From the CalcDef generate Target Vector info
* */
void Vscf::RspAutoTarget()
{
   bool all_fund   = mpVscfCalcDef->RspAllFundamentals();
   bool all_firsto = mpVscfCalcDef->RspAllFirstOvertones();
   bool fund       = mpVscfCalcDef->RspFundamentals();
   bool firsto     = mpVscfCalcDef->RspFirstOvertones();
   
   std::string name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/RspTargetVectors";
   midas::mpi::OFileStream target_ascii(name);
   In n_modes = pOpDef()->NmodesInOp();
   In n_targs = I_0; 
   if (all_fund)   n_targs+= n_modes;
   if (all_firsto) n_targs+= n_modes;
   if (all_fund)   fund=false;
   if (all_firsto) firsto=false;
   vector<In> funds;
   vector<In> firstos;
   if (fund) 
   {
      mpVscfCalcDef->GetRspFundamentals(funds);
      n_targs+=funds.size();
   }
   if (firsto) 
   {
      mpVscfCalcDef->GetRspFirstOvertones(firstos);
      n_targs+=firstos.size();
   }

   target_ascii << n_targs << endl;
   In i_state = I_0;
   if (fund||firsto)
   {
      In i_state = I_0;
      vector<In> modes_occ(n_modes);
      for (In j=I_1;j<=I_2;j++)
      {
         if (j==I_1 && !fund)   continue;
         if (j==I_2 && !firsto) continue;
         In n_actual=I_0;
         if (j==I_1 && fund)   n_actual=funds.size();
         if (j==I_2 && firsto) n_actual=firstos.size();
         for (In i_st=I_0;i_st<n_actual;i_st++) // runs over actual states
         {
            target_ascii << i_state++ << " " << I_1 << endl;
            target_ascii.precision(I_25);
            target_ascii.setf(ios::scientific);
            target_ascii << C_1 << endl;
            for (In i=I_0;i<n_modes;i++) modes_occ[i]=I_0;
            if (j==I_1&&fund) modes_occ[funds[i_st]]=I_1;
            if (j==I_2&&firsto) modes_occ[firstos[i_st]]=I_2;
            for (In i=I_0;i<n_modes;i++) target_ascii << modes_occ[i] << " "; 
            target_ascii << endl;
         }
      }
   }
   if (all_fund || all_firsto) 
   {
      vector<In> modes_occ(n_modes);
      for (In j=I_1;j<=I_2;j++)
      {
         if (j==I_1 && !all_fund)   continue;
         if (j==I_2 && !all_firsto) continue;
         for (In i_st=I_0;i_st<n_modes+I_1;i_st++)
         {
            if (i_st == I_0) continue;
            target_ascii << i_state++ << " " << I_1 << endl;
            target_ascii.precision(I_25);
            target_ascii.setf(ios::scientific);
            target_ascii << C_1 << endl;
            for (In i=I_0;i<n_modes;i++) modes_occ[i]=I_0;
            if (i_st>I_0) modes_occ[i_st-I_1] = j;
            for (In i=I_0;i<n_modes;i++) target_ascii << modes_occ[i] << " "; 
            target_ascii << endl;
         }
      }
   }

   mpVscfCalcDef->SetRspTargetSpace(true);           // Use Rsp. target space from now on. 
}

/**
* Find the address for a particular rsp vector of modal occups. 
* Special to VSCF, 0 1 0 -> address. 0 -1 0 is int. as being in deexc part. 
* */
In Vscf::RspAddressForOccVec(vector<In>& arIvec, bool aFullSpace)
{
   //Mout << " get address for occ vec = " << arIvec << endl;
   In n_modes = pOpDef()->NmodesInOp();
   In non_zero_found=I_0;
   for (In i=I_0;i<n_modes;i++) if(arIvec[i]!=I_0) non_zero_found++;
   if (non_zero_found!=I_1) 
   {
       Mout << "Problem with adresses in Vscf Rsp 1 = " << non_zero_found << " ? " << endl;
       return -I_1;
   }

   In i_add = I_0;
   for (LocalModeNr i=I_0;i<n_modes;i++)
   {
      In occ = I_0; // NOT!!: pVscfCalcDef()->GetOccMode(i);
      In rel_to_occ = arIvec[i]-occ;
      if (rel_to_occ > I_0) 
      {
         i_add+= rel_to_occ-I_1;
         break;
      }
      else if (rel_to_occ < I_0) 
      {
         i_add+= NrspParHalf()-rel_to_occ-I_1;
         break;
      }
      else
      {
         GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i);
         LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
         In nbas = aFullSpace ?  mpBasDef->Nbas(i_bas_mode) : this->mpVscfCalcDef->Nmodals(i);
         i_add+= max(nbas-I_1,I_0);
      }
   }
   //Mout << " i_add found = " << i_add << endl;
   return i_add;
}


/**
* Prepare Rsp Target Space Vectors
* Return the number of rsp target space vectors 
* */
In Vscf::PrepareRspTargetStates()
{
   midas::mpi::WriteToLog("Entered Vscf::PrepareRspTargetStates()");

   Mout << "Prepare Rsp target space " << endl;
   std::string base_name = "RspTargetVector_";
   In idx = 0;
   while(true)
   {
      if(!InquireDataContFile(base_name + std::to_string(idx)) )
      {
         break;
      }
      ++idx;
   }

   if(idx)
   {
      Mout << " Have successfully found " << idx << " RspTargetVectors.\n" 
           << " Will not construct any new ones " << std::endl;
      return idx;
   }
   
   std::string name  = mpVscfCalcDef->GetmVscfAnalysisDir() + "/RspTargetVectors";
   auto target_ascii = midas::mpi::FileToStringStream(name);
   
   In n_ts = I_0;
   target_ascii >> n_ts;
   Mout << " There should be " << n_ts << " target states " << endl;
   if(gDebug)
   {
      Mout << " There should be " << n_ts << " target states " << endl;
   }

   In nvecsize = NrspPar(); 
   vector<In> states_corresponds(n_ts);

   In n_modes = pOpDef()->NmodesInOp();
   In i_st;
   In n_contribs;
   Nb coef;
   In n_neglected = I_0;
   for (In i_ts = I_0; i_ts<n_ts; i_ts++)
   {
      target_ascii >> i_st >> n_contribs;
      if (i_st != i_ts) 
      {
         Mout << " Target state nr. must begin with state zero and increase by one for each state " << endl;
         MIDASERROR("Error in Target State numbering ");
      }
      In i_st_in = i_st;
      i_st -= n_neglected; // Subtract the number of neglected states      
      if (mpVscfCalcDef->IoLevel()>I_10)
      { 
          Mout << " state nr = " << i_st << " with " << n_contribs << " contributions " << endl;
      }
      string s2 = "RspTargetVector_"+std::to_string(i_st);
      string storage_mode = "OnDisc";
      DataCont target_vec(nvecsize,C_0,storage_mode,s2,false);

      In n_in=I_0;
      for (In i_c = I_0;i_c<n_contribs;i_c++)
      {
         target_ascii >> coef;
         vector<In> ivec(n_modes);
         for (In i_c=I_0;i_c<n_modes;i_c++) target_ascii >> ivec[i_c];
         In i_add = RspAddressForOccVec(ivec);
         if (mpVscfCalcDef->IoLevel()>I_10) 
         {
             Mout << " coef " << coef << endl;
             Mout << " ivec " << ivec << endl;
             Mout << " i_add " << i_add << endl;
         }
         if (i_add >=I_0)
         {
            target_vec.DataIo(IO_PUT,i_add,coef);
            n_in++;
         }
      }

      if (n_in==n_contribs)
      {
         target_vec.Normalize();
         target_vec.SaveUponDecon();
         if (gDebug || mpVscfCalcDef->IoLevel()>I_100)
         {
            Mout << " target vec nr " << i_st << " " << target_vec << endl;
         }
         states_corresponds[i_ts] = i_st;
      }
      else
      {
         Mout << " Component of target vector state " << i_st_in 
              << " not in exci space. Will be ignored " << std::endl;
         MidasWarning(" A target vector state was outside excitation space - it is ignored ");  
         n_neglected++;
         states_corresponds[i_ts] = -I_1; 
      }
   }

   Mout << " Rsp Target Vectors have been produced " << endl;
   std::string targ_compar = mpVscfCalcDef->GetmVscfAnalysisDir() + "/RspTargetVectorsCompared";
   midas::mpi::OFileStream rel(targ_compar);
   for (In i_ts = I_0;i_ts<n_ts;i_ts++)
   {
      rel << " " << i_ts;
      rel << " " << states_corresponds[i_ts];
      if (states_corresponds[i_ts]<I_0) rel << " Outside space - not targetted ";
      rel << endl;
   }
   
   return n_ts - n_neglected;
}

void Vscf::GetTargetForState
   (  const string& aTargetVectors
   ,  In aI
   ,  DataCont& arDcIn
   )
{
   string name = aTargetVectors + "_" + std::to_string(aI);
   In vecsize = arDcIn.Size();
   DataCont target;
   target.GetFromExistingOnDisc(vecsize, name);
   Mout << " Assigning contents of " << name << " to " << arDcIn.Label() << endl;
   arDcIn = target;
   target.SaveUponDecon();
}   

/**
 * Rsp eigenvalue driver using the NiceTensor framework and TensorDataCont solution vectors.
 *
 * @param aRight        Do right transform
 **/
void Vscf::TensorRspEigDrv
   (  bool aRight
   )
{
   Out72Char(Mout,' ','*','*');
   Mout << " \n\n Begin Tensor Response Eigenvalue calculation. \n" << std::endl;

   // Set type of transformer
   auto* trf = static_cast<midas::vcc::TransformerV3*>(GetRspTransformer(aRight));

   // Construct transformer for it_solver framework
   Mout << " Initializing MidasTensorDataContTransformer..." << std::endl;
   MidasTensorDataContTransformer tdc_trf(*trf, true);   // exclude ref from state space
   Mout << " Trf initalized!" << std::endl;

   // If we are using targeting
   if (  mpVscfCalcDef->RspAllFirstOvertones()
      || mpVscfCalcDef->RspAllFundamentals()
      || mpVscfCalcDef->RspFundamentals()
      || mpVscfCalcDef->RspFirstOvertones()
      )
   {
      // Construct targets
      auto targets = this->PrepareTensorDataContTargetStates(*trf);

      // Construct solver
      Mout << " Initializing TensorDataContTargetingEigenvalueSolver" << std::endl;
      TensorDataContTargetingEigenvalueSolver solver(tdc_trf);
      InitializeTargetingTensorEigenvalueSolverFromRspCalcDef(solver, this->mpVscfCalcDef, *targets);
      solver.SetAnalysisName(this->mpVscfCalcDef->GetName()+"_analysis"+(aRight? "":"_left")); // Analysis dir for finalizer (if implemented at some point)
      solver.Initialize();
      Mout << " Solver initialized!" << std::endl;

      // Add restart and solve equations
      this->TensorRspEigSolve(solver, aRight);
   }
   else
   {
      // Construct solver
      Mout << " Initializing TensorDataContEigenvalueSolver" << std::endl;
      TensorDataContEigenvalueSolver solver(tdc_trf);
      InitializeTensorEigenvalueSolverFromRspCalcDef(solver, this->mpVscfCalcDef);
      solver.SetAnalysisName(this->mpVscfCalcDef->GetName()+"_analysis"+(aRight? "":"_left")); // Analysis dir for finalizer (if implemented at some point)
      solver.Initialize();
      Mout << " Solver initialized!" << std::endl;

      // Add restart and solve equations
      this->TensorRspEigSolve(solver, aRight);
   }

   Mout << "\n" << trf << std::endl;
   CleanupRspTransformer(trf);
}

/**
 * Prepare TensorDataCont target states
 * 
 * @return
 *    Targets as vector of TensorDataCont%s
 **/
std::unique_ptr<std::vector<TensorDataCont>> Vscf::PrepareTensorDataContTargetStates
   (  const midas::vcc::TransformerV3& aTrf
   )
{
   // Init state space
   std::vector<In> nmodals;
   this->mpVscfCalcDef->Nmodals(nmodals);
   VccStateSpace vss(aTrf.GetXvecModeCombiOpRange(), nmodals, true); // Exclude ref

   // Type ID for target tensors
   auto type_id   =  this->mpVscfCalcDef->GetVccRspDecompInfo().empty()
                  ?  BaseTensor<Nb>::typeID::SIMPLE
                  :  BaseTensor<Nb>::typeID::CANONICAL;

   // Create occupation vectors of target states
   auto n_modes = this->mpVscfCalcDef->GetNmodesInOcc();
   size_t n_targets = 0;

   // Get n_targets
   bool all_fund   = mpVscfCalcDef->RspAllFundamentals();
   bool all_firsto = mpVscfCalcDef->RspAllFirstOvertones();
   bool fund       = mpVscfCalcDef->RspFundamentals();
   bool firsto     = mpVscfCalcDef->RspFirstOvertones();

   // Vector to hold excited modes if not all fundamentals or first overtones are requested
   std::vector<In> fund_modes;
   std::vector<In> firsto_modes;

   if (  all_fund
      )
   {
      n_targets += n_modes;
   }
   if (  all_firsto
      )
   {
      n_targets += n_modes;
   }
   if (  fund
      && !all_fund
      )
   {
      this->mpVscfCalcDef->GetRspFundamentals(fund_modes);
      n_targets += fund_modes.size();
   }
   if (  firsto
      && !all_firsto
      )
   {
      this->mpVscfCalcDef->GetRspFirstOvertones(firsto_modes);
      n_targets += firsto_modes.size();
   }

   // Coefficient for occupation vectors
   In coef = all_fund || fund ? I_1 : I_2;

   // Create occupation vectors
   std::vector<std::vector<In>> occ_vecs;
   occ_vecs.reserve(n_targets);

   if (  fund
      || all_fund
      )
   {
      auto n_fund = all_fund ? n_modes : fund_modes.size();
      for(In v=I_0; v<n_fund; ++v)
      {
         std::vector<In> ov(n_modes, I_0);

         In ex_mode = all_fund ? v : fund_modes[v];

         ov[ex_mode] = I_1;

         occ_vecs.emplace_back(std::move(ov));
      }
   }
   if (  firsto
      || all_firsto
      )
   {
      auto n_firsto = all_firsto ? n_modes : firsto_modes.size();
      for(In v=I_0; v<n_firsto; ++v)
      {
         std::vector<In> ov(n_modes, I_0);

         In ex_mode = all_firsto ? v : firsto_modes[v];

         ov[ex_mode] = I_2;

         occ_vecs.emplace_back(std::move(ov));
      }
   }

   // Init result
   auto targets_ptr = std::make_unique<std::vector<TensorDataCont>>(0);
   auto& targets = *static_cast<std::vector<TensorDataCont>*>(targets_ptr.get());
   targets.reserve(n_targets);
   
   // Add targets
   for(size_t i = 0; i<n_targets; ++i)
   {
      // Get address
      auto addr = this->RspAddressForOccVec(occ_vecs[i], false); // use restricted modal basis instead of large VSCF basis

      Mout << " Prepare target with address: " << addr << std::endl;
      Mout << " Occupation vector:\n [ ";
      for(size_t m=0; m<n_modes; ++m)
      {
         Mout << occ_vecs[i][m];
         if (  m == n_modes-1
            )
         {
            Mout << " ]" << std::endl;
         }
         else
         {
            Mout << ", ";
         }
      }


      // Construct TensorDataCont
      targets.emplace_back(vss, addr, type_id);
   }

   return targets_ptr;
}
