/**
 ************************************************************************
 * 
 * @file                VscfRspTransformer.h
 *
 * Created:             6-2-2008
 *
 * Author:              Ove Christiansen (ove@chem.au.dk)
 *
 * Short Description:   VSCF response transformer.
 * 
 * Last modified: Tue Aug 11, 2009  05:40PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef VSCFRSPTRANSFORMER_H
#define VSCFRSPTRANSFORMER_H

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/Transformer.h"

// Forward decl
class Vscf;
class VscfCalcDef;
class OpDef;

template<class T>
class GeneralDataCont;


class VscfRspTransformer
   :  public Transformer
{
private:
   Vscf*        mpVscf;            // For acces to integrals, basis, etc.
   VscfCalcDef* mpVscfCalcDef;
   OpDef*       mpOpDef;

   void RspTransH0(DataCont& arDcIn,DataCont& arDcOut, In aJ, Nb& aNb);
      
public:
   VscfRspTransformer(Vscf* apVscf, VscfCalcDef* apVscfCalcDef, OpDef* apOpDef);
   VscfRspTransformer(VscfRspTransformer const* apTrf): 
      VscfRspTransformer(apTrf->mpVscf,apTrf->mpVscfCalcDef,apTrf->mpOpDef)
   {
   }

   ~VscfRspTransformer() 
   {
   }

   void Transform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb);

   void PreparePreDiag();
   void PreDiagTransform(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, Nb& aNb);
   void RestorePreDiag();
   
   void ConstructExplicit(MidasMatrix&) { MIDASERROR("VscfRspTransformer::ConstructExplicit() not implemented yet. IMPLEMENT ME!"); }

   void CheckPurifies(DataCont&); 
   
   In VectorDimension() const;

   std::ostream& Print(std::ostream& ostream) const 
   { 
      ostream << " no printout for VscfRspTransformer \n";
      return ostream;
   }

   Transformer* Clone() const;
   Transformer* CloneImprovedPrecon(In aLevel) const;

   std::string Type() const { return "VSCFRSP"; }
};

#endif  // VSCFRSPTRANSFORMER_H
