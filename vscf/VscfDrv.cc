/**
************************************************************************
* 
* @file                VscfDrv.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for MidasCPP Vscf calculations
 
* Last modified:       29-06-2014 (C. Koenig)
*                      removed OptVscf
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <map>
#include <algorithm>

// midas headers
#include "vscf/VscfDrv.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Plot.h"
#include "util/Timer.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/VscfCalcDef.h"
#include "vscf/Vscf.h"
#include "ni/OneModeInt.h"
#include "inc_gen/Warnings.h"
#include "nuclei/Nuclei.h"
#include "analysis/StatMechanics.h"
#include "input/GlobalOperatorDefinitions.h"

#include "mpi/Impi.h"

namespace midas::vscf
{

namespace detail
{
/**
 *
 **/
Nb HOLnPartFuncTDeriv
   (  Vscf* apVscf
   ,  Nb aTemp
   )
{
   In n_modes=apVscf->pOpDef()->NmodesInOp();
   // loop over modes
   Nb result=C_0;
   for(In i=I_0; i<n_modes; ++i)
   {
      Nb ho_ener=apVscf->pBasDef()->GetOmeg(i);
      Nb ho_contrib=C_I_2+C_1/(exp(ho_ener/(C_KB*aTemp))-C_1);
      ho_contrib*=ho_ener/(C_KB*aTemp*aTemp);
      result+=ho_contrib;
   }
   return result;
}

/**
 *
 **/
Nb HOLnPartFunc
   (  Vscf* apVscf
   ,  Nb aTemp
   )
{
   In n_modes=apVscf->pOpDef()->NmodesInOp();
   // loop over modes
   Nb result=C_0;
   for(In i=I_0; i<n_modes; ++i)
   {
      Nb ho_ener=apVscf->pBasDef()->GetOmeg(i);
      Nb cont=ho_ener/(C_2*C_KB*aTemp);
      cont+=log(C_1-exp(-ho_ener/(C_KB*aTemp)));
      result-=cont;
   }
   return result;
}

/**
 *
 **/
Nb HOLnPartFuncT2Deriv
   (  Vscf* apVscf
   ,  Nb aTemp
   )
{
   In n_modes=apVscf->pOpDef()->NmodesInOp();
   // easy term first :)
   Nb dlnz_dt=HOLnPartFuncTDeriv(apVscf,aTemp);
   Nb result=dlnz_dt*(-C_2/aTemp);
   // loop over modes
   for(In i=I_0; i<n_modes; ++i)
   {
      Nb ho_ener=apVscf->pBasDef()->GetOmeg(i);
      Nb e_kt2=ho_ener/(C_KB*aTemp*aTemp);
      Nb denom=exp(ho_ener/(C_KB*aTemp))-C_1;
      Nb ho_contrib=(e_kt2*e_kt2*exp(ho_ener/(C_KB*aTemp)))/(denom*denom);
      result+=ho_contrib;
      //cout << "contrib = " << ho_contrib << " => HO result = " << result << std::endl;
   }
   return result;
}

/**
 * Performing the preparations for a Vscf calculations
 * This is also needed by the RotCoord framework
 *
 * @param arCalcDef  VscfCalcDef
 * @param aIOper     Operator index
 * @param aIBasis    Basis index
 *
 * @return
 *    False if the requested state is outside the space spanned by the basis set
 **/
bool VscfPrepsImpl
   (  VscfCalcDef& arCalcDef
   ,  const In& aIOper
   ,  const In& aIBasis
   ) 
{
   // Check whether analysis directory exists, and if it doesn't create it.
   if (!midas::filesystem::Exists(arCalcDef.GetmVscfAnalysisDir()))
   {
      if (midas::filesystem::Mkdir(arCalcDef.GetmVscfAnalysisDir(), false) != 0)
      {
         midas::filesystem::ErrorCheck("Could not create '" + arCalcDef.GetmVscfAnalysisDir() + "'.");
      }
   }
   
   In i_oper = aIOper;
   In i_basis = aIBasis;

   // Check if we are doing ADGA. This flag is set by AdgaSurface
   if (  arCalcDef.GetAdga()
      ) 
   {
      if (  arCalcDef.IoLevel() > I_10
         )
      {
         Mout << " gOperators size " << gOperatorDefs.GetNrOfOpers() << std::endl;
      }
      gOperatorDefs[i_oper].ReReadIn();
   }
   
   if (  i_basis >= I_0
      )
   {
      gBasis[i_basis].InitBasis(gOperatorDefs[i_oper]);
   }
   // No basis is allowed if all
   else if  (  gOperatorDefs[i_oper].StateTransferForAll()
            )
   {
      if (  arCalcDef.IoLevel() > I_5
         )
      {
         Mout  << " No basis initialized for VSCF calculation '" << arCalcDef.GetName() << "' because all operators are state transfer." << std::endl;
      }
   }
   else
   {
      MIDASERROR("No basis given for VSCF!");
   }

   if (  arCalcDef.IoLevel() > I_10
      )
   {
      Mout << "\n\n\n Do Vscf:  " << arCalcDef.GetName() << " calculation \n" << std::endl;
   }

   if (  arCalcDef.GetNmodesInOcc() == I_0
      )
   {
      arCalcDef.ZeroOcc(gOperatorDefs[i_oper].NmodesInOp());
   }

   if (  gOperatorDefs[i_oper].NmodesInOp() != arCalcDef.GetNmodesInOcc()
      )
   {
      Mout << " Mismatch between number of modes in scf occupation vector: " 
           << arCalcDef.GetNmodesInOcc() << std::endl;
      Mout << " and nr of modes in operator:  "
           << gOperatorDefs[i_oper].NmodesInOp() << std::endl;

      MIDASERROR(" Mismatch between number of modes in scf occupation vector and nr of modes in operator" );
   }
   
   if (  !gOperatorDefs[i_oper].StateTransferForAll()
      )
   {
      bool outside_basis_space = false;
      for(LocalModeNr i_op_mode = I_0; i_op_mode < arCalcDef.GetNmodesInOcc(); ++i_op_mode)
      {
         In occ = arCalcDef.GetOccMode(i_op_mode);
         GlobalModeNr i_g_mode = gOperatorDefs[i_oper].GetGlobalModeNr(i_op_mode);
         LocalModeNr i_bas_mode = gBasis[i_basis].GetLocalModeNr(i_g_mode);
         In nbas = gBasis[i_basis].Nbas(i_bas_mode);
         if ( occ >= nbas
            )
         {
            outside_basis_space=true;
            continue;
         }
      }
      if (  outside_basis_space
         )
      {
         std::string s = arCalcDef.GetName() + " not possible as occupation outside basis ";
         Mout <<  " " << s << std::endl;
         MidasWarning(s);
         return false;
      }
   }

   return true;
}

/**
 * Function to make all single excited states from a calcdef and add to the vector of calcdefs
 *
 * @param arCalcDefs    Vector of calcdefs
 * @param i_calc        The calculation index to make single excitations from
 **/
void MakeAllSingleExcitations
   (  std::vector<VscfCalcDef>& arCalcDefs
   ,  In i_calc
   )
{
   Mout << "Will now make all single excited states:" << std::endl;

   // Add all single escited states to arCalcDefs
   std::string basename = arCalcDefs[i_calc].GetName()+"_Vscf_fast_ave_";
   In i_basis=I_0;

   // find basis set number
   for(In j = I_0; j < gBasis.size(); ++j)
   {
      if (  arCalcDefs[i_calc].Basis() == gBasis[j].GetmBasName()
         )
      {
         i_basis = j;
      }
   }
   
   // Sanity check
   if (  !gBasis[i_basis].GetmUseHoBasis()
      )
   {
      MIDASERROR("midas::vscf::detail::MakeAllSingleExcitations only works with HO basis. Abort!");
   }

   // determine the number of modes
   std::string oper = arCalcDefs[i_calc].Oper();
   In oper_nr = gOperatorDefs.GetOperatorNr(oper);
   OpDef* op_def = &gOperatorDefs[oper_nr];
   In n_modes = op_def->NmodesInOp();

   for(In j = I_0; j < n_modes; ++j) 
   {
      // determine number of HO bf's in each mode
      if (  gBasis[i_basis].GetmUseHoBasis()
         ) 
      {
         for (In k = I_1; k <= gBasis[i_basis].GetmHoQnrs()[j]; ++k)
         {
            std::string name = basename + std::to_string(j) + "_" + std::to_string(k);
            std::vector<In> vec(n_modes, I_0);
            vec[j] = k;
            Mout << vec << std::endl;
            arCalcDefs.push_back(arCalcDefs[i_calc]);
            arCalcDefs.back().SetFastTempAve(true);
            arCalcDefs.back().SetName(name);
            arCalcDefs.back().Reserve();
            arCalcDefs.back().SetOccVec(vec);
         }
      }
   }
}

/**
 *  Function to summarize onestate averages and stuff
 *  Note that we cannot generate lists of contributions
 *  for the individual states as for ss-VSCF (it is possible
 *  but that should at mst be a debug option and should
 *  be implemented in the VscfRspDrv file - almost done)
 *
 *  @param apVscf  Pointer to the vscf calculation.
 **/
void SummarizeOneStateAve
   (  Vscf* apVscf
   ) 
{
   // for printing purposes
   std::string base = apVscf->pVscfCalcDef()->GetmVscfAnalysisDir() + "/Vscf_v_Prop_";
   
   if (  gIoLevel > I_9
      ) 
   {
      auto eval=apVscf->GetEigVal();
      eval.Scale(C_AUTKAYS);
      Mout << "EIGEN VALUES FROM v-VSCF:\n" << eval << std::endl;
   }

   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," Summary of Vscf OneState Temp. effects",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   Mout << std::endl;
   Out72Char(Mout,'-','-','-');
   Out72Char(Mout,'|',' ','|');
   OneArgOut72(Mout," Partition functions",'|');
   Out72Char(Mout,'|',' ','|');
   Out72Char(Mout,'-','-','-');

   In n_modes = apVscf->pOpDef()->NmodesInOp();

   // print partition functions
   for(In i_w=I_0; i_w<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows(); ++i_w)
   {
      Mout << "T = " << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_0) << ":" << std::endl;
      for(In i_w2=I_0; i_w2<n_modes; ++i_w2)
      {
         Mout << "\tPF for mode: " << setw(I_4) << i_w2 << " = " 
               << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_1+i_w2) << std::endl;
      }
      Mout << "\tTotal PF          = "
            << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_1+n_modes) << std::endl;
   }
   Out72Char(Mout,'-','-','-');
   Mout << std::endl;

   // end of print partition functions, now print averages
   In i_start=I_4+n_modes;
   In i_prop=I_0;
   while (  i_start < (apVscf->pVscfCalcDef()->GetOneStateMatrix()).Ncols()
         )
   { 
      // loop over temperaturtes for each property
      std::string prop=apVscf->pVscfCalcDef()->GetProperty(i_prop);
      Out72Char(Mout,'-','-','-');
      Out72Char(Mout,'|',' ','|');
      std::string exp_val=StringForNb(apVscf->pVscfCalcDef()->GetRspOpExpVal(prop),I_22);
      OneArgOut72(Mout," Temp. Ave. for property "+prop+" = "+exp_val+" (0K)",'|');
      Out72Char(Mout,'|',' ','|');
      Out72Char(Mout,'-','-','-');
      // open data file
      std::vector<Nb> tmp_x;
      std::vector<Nb> tmp_y;
      for(In i_w=I_0;i_w<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows();i_w++) {
         Nb temp=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_0);
         Mout << "For T = " << setw(I_8) << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_0)
               << " Prop = " << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,i_start) << std::endl;
         Mout << "E/kT^2 = " << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,i_start)/(C_KB*temp*temp) << std::endl;
         tmp_x.push_back(apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_0));
         tmp_y.push_back(apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,i_start));
      }
      // also make a plot file intended for gnuplot
      MidasVector onestate_x(tmp_x);
      MidasVector onestate_y(tmp_y);
      Plot plot(onestate_x.Size());
      plot.SetXYvals(onestate_x,onestate_y);
      plot.SetTitle(prop+" as a function of T");
      plot.SetXlabel("T in K");
      plot.SetYlabel("Property in au");
      plot.UnsetKey();
      plot.MakeGnuPlot(base+std::to_string(i_start));
      // If we have calculated the temperature average of the energy,
      // also print the difference to the reference energy (i.e. 0K)
      if(prop == apVscf->pOpDef()->Name()) {
         Out72Char(Mout,'-','-','-');
         Mout << std::endl;
         std::string prop=apVscf->pVscfCalcDef()->GetProperty(i_prop);
         std::string exp_val=StringForNb(apVscf->pVscfCalcDef()->GetRspOpExpVal(prop),I_22);
         Out72Char(Mout,'-','-','-');
         Out72Char(Mout,'|',' ','|');
         OneArgOut72(Mout," Energies relative to: "+exp_val+" (0K)",'|');
         Out72Char(Mout,'|',' ','|');
         Out72Char(Mout,'-','-','-');
         for(In i_w=I_0;i_w<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows();i_w++) {
            Nb rel_ener=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,i_start)-apVscf->GetEtot();
            Mout << "For T = " << setw(I_8) << apVscf->pVscfCalcDef()->GetOneStateMatrix(i_w,I_0)
               << " Rel. E = " << rel_ener << std::endl;
         }
      }
      Out72Char(Mout,'-','-','-');
      Mout << std::endl;
      i_start++;
      i_prop++;
   }
   // print thermodynamic properties
   // loop over temperatures
   // First: The ensemble energy
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," Thermodynamic properties",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   // U = kT^2 dlnZ/dT
   Mout << "   Internal energy, U:" << std::endl;
   Mout << " T                      HO                     v-VSCF     " << std::endl;
   for(In i_t=I_0; i_t<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows(); ++i_t)
   {
      Nb temp=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,I_0);
      // HO dlnZ/dT always entry number n_modes+4
      Nb dlnz_dt=HOLnPartFuncTDeriv(apVscf,temp);
      Nb ho=temp*temp*C_KB*dlnz_dt;
      // v-VSCF dlnZ/dT always third last entry!
      In col=n_modes+I_2;
      Nb v_vscf=(temp*temp*C_KB)*apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col);
      // output in SI units
      ho*=C_AUTKJMOL;
      v_vscf*=C_AUTKJMOL;
      Mout << " " << temp << " " << ho << " " << v_vscf << std::endl;
   }
   // S = klnZ + kT(dlnZ/dT)
   Mout << "   Entropy, S:" << std::endl;
   Mout << " T                      HO                     v-VSCF     " << std::endl;
   for(In i_t=I_0; i_t<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows(); ++i_t)
   {
      Nb temp=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,I_0);
      // HO dlnZ/dT always last entry!
      Nb lnz=HOLnPartFunc(apVscf,temp);
      Nb dlnz_dt=HOLnPartFuncTDeriv(apVscf,temp);
      Nb ho=C_KB*lnz+C_KB*temp*dlnz_dt;
      // HO lnZ always second last entry!
      // v-VSCF dlnZ/dT always third last entry!
      In col=n_modes+I_2;
      Nb v_vscf=(temp/C_AUTK)*apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col);
      // v-VSCF Z always fourth last entry!
      col=n_modes+I_1;
      v_vscf+=log(apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col))/C_AUTK;
      // the GS energy must be added to the v-VSCF result,
      // E_vscf/kT * k = E_vscf/T
      v_vscf-=(apVscf->pVscfCalcDef()->GetEfinal()/temp);
      // output in SI units
      ho*=C_AUTKJMOL*C_10_3;
      v_vscf*=C_AUTKJMOL*C_10_3;
      Mout << " " << temp << " " << ho << " " << v_vscf << std::endl;
   }
   // A = U - TS = -kTlnZ 
   Mout << "   Arbeits function, A:" << std::endl;
   Mout << " T                      HO                     v-VSCF     " << std::endl;
   for(In i_t=I_0; i_t<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows(); ++i_t)
   {
      Nb temp=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,I_0);
      // HO lnZ always second last entry!
      Nb lnz=HOLnPartFunc(apVscf,temp);
      Nb ho=-C_KB*temp*lnz;
      // v-VSCF Z always fourth last entry!
      In col=n_modes+I_1;
      Nb v_vscf=C_M_1*log(apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col))*temp/C_AUTK;
      // the GS energy must be added to the v-VSCF result,
      // E_vscf/kT * kT = E_vscf
      v_vscf+=apVscf->pVscfCalcDef()->GetEfinal();
      // output in SI units
      ho*=C_AUTKJMOL;
      v_vscf*=C_AUTKJMOL;
      Mout << " " << temp << " " << ho << " " << v_vscf << std::endl;
   }
   // C_v = dU/dT
   Mout << "   Heat capacity (const. V), C_v:" << std::endl;
   Mout << " T                      HO                     v-VSCF     " << std::endl;
   for(In i_t=I_0; i_t<(apVscf->pVscfCalcDef()->GetOneStateMatrix()).Nrows(); ++i_t)
   {
      Nb temp=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,I_0);
      // HO dlnZ/dT always last entry!
      Nb dlnz_dt=HOLnPartFuncTDeriv(apVscf,temp);
      Nb d2lnz_dt2=HOLnPartFuncT2Deriv(apVscf,temp);
      Nb ho=C_2*C_KB*temp*dlnz_dt + C_KB*temp*temp*d2lnz_dt2;
      // HO lnZ always second last entry!
      // v-VSCF dlnZ/dT always second last entry!
      // v-VSCF d2lnZ/dT2 always last entry
      In col=n_modes+I_2;
      dlnz_dt=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col);
      col=n_modes+I_3;
      d2lnz_dt2=apVscf->pVscfCalcDef()->GetOneStateMatrix(i_t,col);
      Nb v_vscf=C_2*C_KB*temp*dlnz_dt + C_KB*temp*temp*d2lnz_dt2;
      // output in SI units
      ho*=C_AUTKJMOL*C_10_3;
      v_vscf*=C_AUTKJMOL*C_10_3;
      Mout << " " << temp << " " << ho << " " << v_vscf << std::endl;
   }
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," End of Vscf OneState Temp. effects",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
   Mout << std::endl;
}

} /* namespace detail */


/**
 * Run VSCF calculation from single calcdef pointer
 *
 * @param arCalcDef
 * @param aRestartIntegrals
 **/
void RunVscf
   (  VscfCalcDef& arCalcDef
   ,  bool aRestartIntegrals
   )
{
   if constexpr (MPI_DEBUG)
   {
      mpi::WriteToLog("When entering RunVscf, MpiRegion is : '" + std::string(mpi::IsMpiRegion() ? "true" : "false") + "'.");
   }

   // Find operator number
   std::string oper_name = arCalcDef.Oper();
   In i_oper = gOperatorDefs.GetOperatorNr(oper_name);

   // Find basis number
   std::string basis_name = arCalcDef.Basis();
   In i_basis = -I_1;
   for (In i = I_0; i < gBasis.size(); ++i)
   {
      if (gBasis[i].GetmBasName() == basis_name)
      {
         i_basis = i;
      }
   }
   if (  (  i_basis == -I_1 
         && !gOperatorDefs.GetOperator(oper_name).StateTransferForAll()
         )
      || i_oper == -I_1
      )
   {
      MIDASERROR("Basis or Operator not found in VccDrv"); //mbh: if adga, re-readin operator
   }

   bool succ = detail::VscfPrepsImpl(arCalcDef, i_oper, i_basis);
   if (  !succ
      )
   {
      return;
   }

   // At this point, the OpDef should have been read in, whether we provided it
   // as input, or it's being re-read-in as part of ADGA.
   if (!gOperatorDefs[i_oper].ProperlyReadFromFile())
   {
      std::stringstream err;
      err
         << "OpDef has unexpectedly not been properly read from file.\n"
         << "   name = " << gOperatorDefs[i_oper].Name() << '\n'
         << "   file = " << gOperatorDefs[i_oper].GetOpFile() << '\n'
         ;
      if (midas::filesystem::IsFile(gOperatorDefs[i_oper].GetOpFile()))
      {
         err << "(The file exists, though...)\n";
      }
      else
      {
         err
            << "The file does not exist.\n"
            << "Have you given the correct filename in your input?\n"
            ;
      }
      MIDASERROR(err.str());
   }

   // Check that the operator contains both potential and kinetic energy operators, otherwise issue a warning
   const auto& has_keo_terms = gOperatorDefs[i_oper].GetOperWithKeoTerms();
   if (!has_keo_terms)
   {
      std::stringstream keo_terms_warning;
      keo_terms_warning << "No kinetic energy operator terms detected in operator "
                        << oper_name
                        << " for use in VSCF, please investigate";
      MIDASERROR(keo_terms_warning.str());
   }

   //
   bool save = true;
   std::string storage = "InMem";
   if (arCalcDef.IntStorage() != I_0)
   {
      storage = "OnDisc";
   }
   BasDef* basdef = nullptr;
   if (  i_basis > -I_1
      )
   {
      basdef = &gBasis[i_basis];
   }
   OneModeInt one_mode_int(&gOperatorDefs[i_oper], basdef, storage, save);
   
   if (arCalcDef.IoLevel() > I_6)
   {
      Mout << "\n\n";
      Mout << " Operator:                            " 
           << one_mode_int.OperName() << std::endl;
      Mout << " Operator-Type:                       " 
           << gOperatorDefs[i_oper].Type() << std::endl; 
      Mout << " Number of terms in operator:         "
           << gOperatorDefs[i_oper].Nterms() << std::endl; 
      if (  basdef
         )
      {
         Mout << " Basis:                               " 
              << gBasis[i_basis].GetmBasName() << std::endl; 
         Mout << " Basis-Type:                          " 
              << gBasis[i_basis].GetmBasSetType() << std::endl; 
      }
      else
      {
         Mout  << " Basis:                              "
               << "NoBasis"
               << std::endl;
      }
      Mout << " Number of modes in vscf              " 
           << gOperatorDefs[i_oper].NmodesInOp() << std::endl; 
      if (gOperatorDefs[i_oper].NmodesInOp() < I_100)
      {
         Mout << " Occupation vector:                   " 
              << arCalcDef.OccupVec() << std::endl;
      }
      Mout << " One Mode Integral:                   " 
           << one_mode_int.OperName() << std::endl;
      Mout << " Kinetic energy operator provided:    " 
           << has_keo_terms << std::endl;
      Mout << " Total number of one-mode operators:  " 
           << one_mode_int.GetmTotNoOneModeOpers() << std::endl;
      Mout << " Number of one mode integrals:        " 
           << one_mode_int.GetmTotNoOneModeIntegrals() << std::endl;
      Mout << " Int storage mode for vscf:           " 
           << arCalcDef.IntStorage() << std::endl;
      Mout << " Vec storage mode for vscf:           " 
           << arCalcDef.VecStorage() << std::endl << std::endl;
   }

   //
   bool restart_ok = false;
   if (  aRestartIntegrals
      )
   {
      restart_ok = one_mode_int.RestartInt();
   }
   if (  !restart_ok
      )
   {
      one_mode_int.CalcInt();
   }

   // Run the actual VSCF calculation
   Vscf vscf(&arCalcDef, &gOperatorDefs[i_oper], basdef, &one_mode_int);
   vscf.StartGuess();
   vscf.Solve();

   // 
   if (  vscf.Converged()
      )
   {
      if (arCalcDef.IoLevel() > I_2)
      {
         Mout << " Vscf " << left << setw(25) << vscf.Name() << " converged!  E = " << vscf.GetEtot() << " in N_it = " << vscf.GetIter() << std::endl;
      }

      arCalcDef.SetDone(vscf.GetEtot());
      if (  arCalcDef.Save() 
         || arCalcDef.IoLevel() > I_1
         || arCalcDef.DoRsp()
         || arCalcDef.Calc1pDensities()
         || arCalcDef.DoStateAve()
         )
      {
         vscf.Finalize();
      }

      if (arCalcDef.DumpModalsForMatRep())
      {
         vscf.DumpModalsForMatRep();
      }

      if (arCalcDef.DoRsp())
      {
         vscf.SetRspRefE(vscf.GetEtot());
         if (arCalcDef.RspDiagMeth()=="LAPACK" ||
               arCalcDef.RspDiagMeth()=="DGEEVX" ) 
            arCalcDef.SetRspDiagMeth("DGEEVX");
         else if (arCalcDef.RspDiagMeth()=="DGEEV") 
            arCalcDef.SetRspDiagMeth("DGEEV");
         else
         {
            MidasWarning("Only DGEEV, DGEEVX (equiv. LAPACK) allowed DiagMeth for Vscf Response. Override! ");
            arCalcDef.SetRspDiagMeth("DGEEVX");
         }
         if (arCalcDef.GetRspLevel2Solver()=="MIDAS" ||
               arCalcDef.GetRspLevel2Solver()=="LAPACK")
            arCalcDef.SetRspLevel2Solver("DGESV");
         else if (arCalcDef.GetRspLevel2Solver()=="DGESV" || 
               arCalcDef.GetRspLevel2Solver()=="SVD") 
            arCalcDef.SetRspLevel2Solver(arCalcDef.GetRspLevel2Solver());
         else
         {
            MidasWarning("Only DGESV (LAPACK or MIDAS), or SVD are allowed Level2Solver. Override! ");
            arCalcDef.SetRspLevel2Solver("DGESV");
         }
         if (arCalcDef.GetRspHarmonicRR())
         {
            arCalcDef.SetRspDiagMeth("DGGEV");
            arCalcDef.SetRspLevel2Solver("DGESV");
         }
         // mbh: if TempAve, but no properties, do temperature averaging here!
         //            otherwise it is done in the response module.
         if((vscf.pVscfCalcDef())->OneStateVscfAve() && gTempSet) 
         {
            // set size of dimension one of temp. matrix
            // onestate matrix contain T, one-mode pf for all modes, total pf,
            // dlnZ/dT, and d2lnZ/dT2 for v-VSCF
            vscf.pVscfCalcDef()->OneStateMatrixResize(gTempSteps,vscf.pOpDef()->NmodesInOp()+I_4,false);
            // start by setting the dimension of the occupancy matrix
            // dim(OCCU) = gTempSteps x (dim(mOccModalOffSet)+1)
            vscf.pVscfCalcDef()->OccupancyResize(gTempSteps,vscf.GetEigVal().Size()+I_1,false);
            // loop over temperatures
            Nb temp_step=(gTempLast-gTempFirst)/(Nb)(gTempSteps-I_1);
            for(In i_t=I_0;i_t<gTempSteps;i_t++) 
            {
               Nb curr_temp=gTempFirst+(Nb)i_t*temp_step;
               vscf.pVscfCalcDef()->SetOneStateMatrix(i_t,I_0,curr_temp);
               vscf.pVscfCalcDef()->SetOccupancy(i_t,I_0,curr_temp);
               // calculate one-mode partition functions
               if (vscf.pVscfCalcDef()->ModalLimitsInput()) 
                  MidasWarning("Calculate One Mode Partitioning Function for temperature averaging only up to the given modal limit");
               std::vector<Nb> OneModePartFunc=vscf.CalcOneModePartFunc(curr_temp,i_t, vscf.pVscfCalcDef()->ModalLimitsInput());
               Nb PartFunc=C_1;
               for(In i_pf=I_0;i_pf<OneModePartFunc.size();i_pf++) {
                  vscf.pVscfCalcDef()->SetOneStateMatrix(i_t,i_pf+I_1,OneModePartFunc[i_pf]);
                  PartFunc*=OneModePartFunc[i_pf];
               }
               vscf.pVscfCalcDef()->SetOneStateMatrix(i_t,OneModePartFunc.size()+I_1,PartFunc);
               // calculate dlnZ/dT for current T
               Nb dlnz_dt=vscf.CalcOneStateTempDeriv(curr_temp,i_t);
               // calculate d2lnZ/dT2 for current T
               Nb d2lnz_dt2=vscf.CalcOneStateTemp2Deriv(curr_temp,i_t,dlnz_dt);
               vscf.pVscfCalcDef()->SetOneStateMatrix(i_t,OneModePartFunc.size()+I_2,dlnz_dt);
               vscf.pVscfCalcDef()->SetOneStateMatrix(i_t,OneModePartFunc.size()+I_3,d2lnz_dt2);
               // Do the remaining partition function in a usual stat. mech. 
               // fashion
               StatMechanics StatMech(curr_temp);
               StatMech.RotPartFunc();
               StatMech.TrsPartFunc();
               Mout << "Statistical Mechanics PF(T=" << curr_temp << "):" << std::endl << StatMech << std::endl;
            }
         }
         // Done
         //Mout << "Now entering response..." << std::endl;
         vscf.RspDrv();
         //Mout << "Done in response..." << std::endl;
         // Output temperature averages, partition functions etc. if we have
         // done a OneState temp. ave.
         if((vscf.pVscfCalcDef())->OneStateVscfAve() && gTempSet) 
         {
            // used to be here, now in a separate function
            // to give a more clear cut!
            detail::SummarizeOneStateAve(&vscf);
         }
         if (!arCalcDef.Save()) vscf.RspErase();
      }
      // mbh: following for thermal spectra
      bool ground_state_calc=true;
      for(In i=0; i<arCalcDef.OccupVec().size(); ++i) 
      {
         if (  arCalcDef.OccupVec()[i] != 0
            ) 
         {
            ground_state_calc=false;
            break;
         }
      }
      if (  ground_state_calc
         )
      {
         In n_spec=arCalcDef.GetNThermalSpectra();
         Timer time_spec;
         for(In i_spec=I_0;i_spec<n_spec;++i_spec) 
         {
            Mout << "MidasCpp will now construct a thermal spectrum." << std::endl;
            vscf.ConstructThermalSpectrum(arCalcDef.GetThermalSpectrum(i_spec));
         }
         if (arCalcDef.TimeIt()) 
         {
            time_spec.CpuOut(Mout,"\n CPU time used in spectrum: ");
            time_spec.WallOut(Mout,"\n Wall time used in spectrum: ");
         }
      }
      // mbh: end thermal spectra
   }
   else 
   {
      Mout << " Vscf " << left << setw(25) << vscf.Name() << " is NOT converged!  E = " << vscf.GetEtot() << " in N_it = " << vscf.GetIter() << std::endl;
      std::string s1 = " Vscf " + vscf.Name() + " is NOT converged! ";
      MidasWarning(s1);
   }
   if (  !arCalcDef.Save()
      )
   {
      vscf.Erase();
   }
}

/**
 * Run vscf studies 
 **/
void VscfDrv
   (
   )
{
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin VSCF calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;


   // mbh: fast temp. ave.
   In old_size = gVscfCalcDef.size();
   for (In i = I_0; i < old_size; i++)
   {
      if (  gVscfCalcDef[i].FastTempAve()
         ) 
      {
         detail::MakeAllSingleExcitations(gVscfCalcDef, i);
      }
   }
   // mbh: end fast temp. ave.

   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, Mout);
   Mout.setf(ios::showpoint);
  
   // Make a map over oper/basis combinations used in the whole set of vscf calculations.
   std::map<std::string,In> oper_bas_map;
   for (In i_calc = gVscfCalcDef.size() - I_1; i_calc >= I_0; --i_calc)
   {
      std::string oper_name = gVscfCalcDef[i_calc].Oper();
      std::string basis_name = gVscfCalcDef[i_calc].Basis();
      std::string oper_bas = oper_name + "_" + basis_name;
      oper_bas_map[oper_bas] = i_calc;
   }
   if (  gVibIoLevel > I_8
      )
   {
      Mout << " Nr. | oper_basis " << std::endl;
      for(const auto& po : oper_bas_map)
      {
         Mout << "  " << po.second << "  | ";
         Mout << po.first << std::endl;
      }
   }

   // Call implementation for global vector of calcdefs.
   In ncalc = gVscfCalcDef.size();
   for (In icalc = I_0; icalc < ncalc; ++icalc)
   {
      auto& calc = gVscfCalcDef[icalc];
      
      // index of first calculation using this operator and basis
      auto oper_name = calc.Oper();
      auto basis_name = calc.Basis();
      std::string oper_bas = oper_name + "_" + basis_name;
      In i_first_calc = oper_bas_map[oper_bas];

      // Do we need to re-calculate integrals?
      bool restart_int = icalc > i_first_calc;

      // Run calculation
      RunVscf(calc, restart_int);
   }

   // Sort the calculations according to oper/basis/increasing energy.
   std::sort(gVscfCalcDef.begin(), gVscfCalcDef.end(), VscfCalcDefLess);

   // Output the vscf summary 
   Mout << std::endl;
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," Summary of Vscf state optimizations ",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');

   In n_units = 3;

   for (In i_units=0; i_units<n_units; ++i_units)
   {
      Nb conv = C_0;
      if (i_units == 0) 
      {
         Mout << "\n\n Units is direct accordance with input " << std::endl;
         conv = C_1;
      }
      if (i_units == 1) 
      {
         conv = C_AUTKAYS;
         Mout << "\n\n Units is converted with " << conv << " corresponding to au to cm-1 " << std::endl;
      }
      if (i_units == 2) 
      {
         conv = C_AUTEV;  
         Mout << "\n\n Units is converted with " << conv << " corresponding to au to eV " << std::endl;
      }

      Mout << "\n\n";
      Mout << " Calculations ordered after operator/basis/name/increasing energy: " << std::endl;
      Mout << " +---------------------------------------------------------------+ " << std::endl;
      for (In i_calc =0;i_calc<gVscfCalcDef.size();i_calc++)
      {
         if (gVscfCalcDef[i_calc].Done())
         {
            Mout << " VSCF: " << left << setw(24) << gVscfCalcDef[i_calc].GetName();
            Mout << " E = " << right << setw(23) << gVscfCalcDef[i_calc].GetEfinal()*conv;
            if (gVscfCalcDef[i_calc].GetNmodesInOcc() < 25)
            {
               Mout << " Occ: [";
               for (In i_op_mode=0;i_op_mode< gVscfCalcDef[i_calc].GetNmodesInOcc()-1;i_op_mode++)
                  Mout << gVscfCalcDef[i_calc].GetOccMode(i_op_mode) << ",";
               Mout << gVscfCalcDef[i_calc].GetOccMode(gVscfCalcDef[i_calc].GetNmodesInOcc()-1) << "]";
               if (i_units==0) Mout << " au"; 
               if (i_units==1) Mout << " cm-1"; 
               if (i_units==2) Mout << " eV"; 
            }
            Mout << std::endl;
         }

         if (gVscfCalcDef[i_calc].DoStateAve())
         {
            Mout << " sa-vVSCF: " << left << setw(20) << gVscfCalcDef[i_calc].GetName();
            Mout << " E = " << right << setw(23) << gVscfCalcDef[i_calc].GetEfinalStateAverage()*conv;
            if (i_units == I_0)
            {
               Mout << " au"; 
            }
            else if (i_units == I_1)
            {
               Mout << " cm-1"; 
            }
            else
            {
               Mout << " eV";
            }
            Mout << std::endl;
         }
      }

      if (gVscfCalcDef.size()>1)
      {
         Mout << "\n\n";
         Mout << " Excitation energies relative to the lowest state: " << std::endl;
         Mout << " +-----------------------------------------------+ " << std::endl;
         for (In i_calc =1;i_calc<gVscfCalcDef.size();i_calc++)
         {
            if (gVscfCalcDef[i_calc].Done())
            {
               Mout << " VSCF: " << left << setw(24) << gVscfCalcDef[i_calc].GetName();
               Mout << " w = " << right << setw(23) << 
                     (gVscfCalcDef[i_calc].GetEfinal()-gVscfCalcDef[0].GetEfinal())*conv;
               if (gVscfCalcDef[i_calc].GetNmodesInOcc() < 25)
               {
                  Mout  << " Occ: [";
                  for (In i_op_mode=0;i_op_mode< gVscfCalcDef[i_calc].GetNmodesInOcc()-1;i_op_mode++)
                     Mout << gVscfCalcDef[i_calc].GetOccMode(i_op_mode) << ",";
                  Mout << gVscfCalcDef[i_calc].GetOccMode(gVscfCalcDef[i_calc].GetNmodesInOcc()-1) 
                     << "]";
               }
               if (i_units==0) Mout << " au"; 
               if (i_units==1) Mout << " cm-1"; 
               if (i_units==2) Mout << " eV"; 
               Mout << std::endl;
            }
         }
      }
   }

   // 
   // Construct info for calculation of temperature effects and populations.
   //
   {
      // mbh: std::vector<VscfCalcDef*> help(gVscfCalcDef.size());
      std::vector<VscfCalcDef*> help_ss;
      std::vector<VscfCalcDef*> help_fa;
      for (In i=I_0;i<gVscfCalcDef.size();i++)
      {
         if(gVscfCalcDef[i].FastTempAve()) help_fa.push_back(&gVscfCalcDef[i]);
         else help_ss.push_back(&gVscfCalcDef[i]);
      }
      if (help_fa.size()!=I_0)
         ExtractPropsAndSaveOnFiles(help_fa,"Vscf_fast_ave");
      if (help_ss.size()!=I_0)
         ExtractPropsAndSaveOnFiles(help_ss,"Vscf_ss");
   }

   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," VSCF calculations done ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
}

/**
 * Performing the preparations for a Vscf calculations
 * This is also needed by the RotCoord framework
 *
 * @param aICalc     VSCF calc index
 * @param aIOper     Operator index
 * @param aIBasis    Basis index
 *
 * @return
 *    False if the requested state is outside the space spanned by the basis set
 **/
bool VscfPreps
   (  const In& aICalc
   ,  const In& aIOper
   ,  const In& aIBasis
   )
{
   return detail::VscfPrepsImpl(gVscfCalcDef[aICalc], aIOper, aIBasis);
}


} /* namespace midas::vscf */
