/**
************************************************************************
* 
* @file                VscfDrv.h
*
* Created:             04-10-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   VscfDrv
* 
* Last modified:       
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VSCFDRV_H_INCLUDED
#define VSCFDRV_H_INCLUDED

#include <vector>
#include "inc_gen/TypeDefs.h"

//! Forward decl
class Vscf;
class VscfCalcDef;

namespace midas::vscf
{

/**
 * Vscf driver function
 *
 * Calls RunVscf() for all VscfCalcDef%s in the global calcdef vector.
 **/
void VscfDrv
   (
   );

/**
 * Performing the preparations for a Vscf calculations
 * This is also needed by the RotCoord framework
 *
 * @param aICalc     VSCF calc index
 * @param aIOper     Operator index
 * @param aIBasis    Basis index
 *
 * @return
 *    False if the requested state is outside the space spanned by the basis set
 **/
bool VscfPreps
   (  const In& aICalc
   ,  const In& aIOper
   ,  const In& aIBasis
   );

/**
 * Run single VSCF calculation from VscfCalcDef
 *
 * NB: The argument calcdef should ideally be const, but this is not possible 
 *     due to variables being stored in the calcdef (bad practice).
 *
 * @param arCalcDef          The calcdef.
 * @param aRestartIntegrals  Set true if a calculation with same operator and basis has been done before.
 **/
void RunVscf
   (  VscfCalcDef& arCalcDef
   ,  bool aRestartIntegrals = false
   );

} /* namespace midas::vscf */

#endif /* VSCFDRV_H_INCLUDED */
