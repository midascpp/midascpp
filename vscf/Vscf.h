/**
************************************************************************
* 
* @file                Vscf.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Vscf class and function declarations.
* 
* Last modified:       27-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VSCF_H_INCLUDED
#define VSCF_H_INCLUDED

// std headers
#include <iostream> 
#include <string> 
#include <vector> 
#include <string> 
#include <complex> 
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasMatrix.h"
#include "input/VscfCalcDef.h"
#include "input/RspCalcDef.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "input/ThermalSpectrumDef.h"
#include "vscf/VscfRspTransformer.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"
#include "analysis/StatMechanics.h"
#include "it_solver/ComplexVector.h"
#include "it_solver/ZeroVector.h"
#include "it_solver/transformer/MidasComplexFreqShiftedTransformer.h"

// Forward decl.
class MidasTensorDataContTransformer;
namespace midas::vcc
{
class TransformerV3;
} /* namespace midas::vcc */
class OneModeInt;
class BasDef;
class OpDef;
template<typename> class ModalIntegrals;
template
   <  typename T
   >
class GeneralTensorDataCont;
using TensorDataCont = GeneralTensorDataCont<Nb>;

class Vscf
{
   private:
      //Logical iteration info
      In mNiterGlobal;                          ///< Number of global iterations used
      In mNiterLocal;                           ///< Number of local iterations used
      bool mConverged;                          ///< Scf has been converged?
      OpDef* mpOpDef;                           ///< A pointer to the operator definition 
      OpDef* mpOpDefOrig;                       ///< A pointer to the original operator definition 
      const BasDef* mpBasDef;                         ///< A pointer to the basis definition 
      VscfCalcDef* mpVscfCalcDef;               ///< A pointer to the VscfCalcDef
      OneModeInt* mpOneModeInt;                 ///< A pointer to the one mode integrals.
      OneModeInt* mpOneModeIntOrig;             ///< A pointer to the original one mode integrals.
      In mNmodalCoef;                           ///< The number of modal coefficients.
      In mNoccModalCoef;                        ///< The number of occupied modal coefficients.
      std::vector<In> mOccModalOffSet;               ///< Add. of the occ. modals in mOccModals; 
                                                    //(or eigval i mEigVal)
      std::vector<In> mModalOffSet;                  ///< Add. of the occ. modals in mModals;
      //Numerical quantities 
      Nb mEprevIt;                              ///< Storing Scf energy of previous iteration
      Nb mEthisIt;                              ///< Total Scf energy 
      DataCont mModals;                         ///< The set of all modals
      DataCont mEigVal;                         ///< The eigenvalues for all modals 
      MidasVector mOccModals;                   ///< The occupied modals
      MidasVector mOccEigVal;                   ///< The eigenvalues for the occupied modes

      std::vector<In> mOneModeIntOccOff;             ///< Offsets for OneModeIntOcc for each mode
      MidasVector mOneModeIntOcc;               ///< The one-mode occupied integrals.
      MidasVector mFockNorms;                   ///< The norm of the one mode Fock mats
      MidasVector mOneModeOperNorms;            ///< The norms of the one-mode operators 

      In mNrspPar;                               ///< Number of response parameters
      In mNrspParHalf;                           ///< Number of response parameters half size (same for VCI!)

      Nb mRspRefE;                              ///< Energy for Rsp. Ref. state

      // mbh: Following for thermal spectra
      map<In, std::complex<Nb> **> mXmatrix;
      map<In, std::complex<Nb> **> mYmatrix;
      map<In, std::complex<Nb> **> mZmatrix;

      //! Called from DoIR() to write data for spectrum generation by analysis program.
      void IRPrepareAnalysis
         (  const std::vector<IRData>& aData
         )  const;
      
      //! Called from DoRaman() to write data for spectrum generation by analysis program.
      void RamanPrepareAnalysis
         (  const std::vector<RamanData>& aData
         )  const;

      bool mOldModalsErr;                       ///< False if the Old modals are found (for restarted calcualtions)
      bool mItNaMoConv = false;                 ///< Is iterative natural modals determination converged

   protected:
      //TEMPORARY(oh really?): DATA AS PUBLIC
      // Niels: I made it protected now. At last...
      std::unique_ptr<ModalIntegrals<Nb>> mOneModeModalBasisInt;    ///< The set of all one-modal operator integrals.

      //! Number of primitive basis functions
      In NBas
         (  LocalModeNr aMode
         )  const;
      
      
   public:
      Vscf
         (  VscfCalcDef* apCalcDef
         ,  OpDef* apOpDef 
         ,  const BasDef* const apBasDef 
         ,  OneModeInt* apOneModeInt
         );                   ///< Constructor 

      virtual ~Vscf();
    
      In GetIter() const {return mNiterGlobal;}         ///< Get  number of iter.
      In GetIterLocal() const {return mNiterGlobal;}    ///< Get  local number of iter.
      In OccModalOffSet(const In aOperMode) const 
        {return mOccModalOffSet[aOperMode];}            ///< Get  offset in nbas(i_mode) array 
      const std::vector<In>& GetOccModalOffSet() const
      { 
         return mOccModalOffSet;
      }
      In ModalOffSet(const In aOperMode) const 
        {return mModalOffSet[aOperMode];}               ///< Get  offset in nbas*nbas(imode) array.

      In ModalAddress(const LocalModeNr aOperMode, const In& aModalNr) const;                       
                                                        ///< Get  Modal Adress.
      Nb GetEtot() const {return mEthisIt;}             ///< Get  total energy
      Nb GetRspRefE() const {return mRspRefE;}          ///< Get ref. energy 
      void SetRspRefE(Nb aRspRefE) {mRspRefE=aRspRefE;}   ///< Get ref. energy 
      const Nb& OneModeIntOcc(const In& arI) const {return mOneModeIntOcc[arI];}           ///< One-mode occ. ints.
      const In& OneModeIntOccOff(const In& arI) const {return mOneModeIntOccOff[arI];}     ///< Offsets for one-mode occ. ints.
      const Nb& GetOccEigVal(const In& aOperMode) const {return mOccEigVal[aOperMode];}    ///< Get the occupied modal energy for mode aOperMode.
      void SetItNaMoConv(bool aB){mItNaMoConv=aB;}        ///< Set wether natural modals converged iteration complete (if relevant)  
      bool ItNaMoConv() const {return mItNaMoConv;} ///< is natural modals converged iteration complete (if relevant)  

      Nb GetEigVal(const In aOperMode, const In aEigValNr); ///< Get the modal energy for a modal
      void Energy();                                    ///< Calculate the energy
      void CalculateStateAveTransformedInt(std::vector<MidasVector>& arVecOfTransInt, const std::vector<MidasVector>& arVecOfWeightsVec);
       ///< Calculate Transformed integrals 
      void CalculateStateAveWeights(std::vector<MidasVector>& arVecOfWeightsVec);  ///< Set state average weights 
      void VirtVscfStateAve();                          ///< Calculate the state average energy 
      Nb VirtVscfStateAveFactWeig();                    ///< Calculate the state average energy for factorizable weights 
      Nb VirtVscfStateAveFundOnly();                    ///< Calculate the state average energy for fundamentals only.
      std::string Name(bool restart=false)
      {return mpVscfCalcDef->GetName(restart);}           ///< Give the name of the scf.
      bool Converged() const {return mConverged;}    ///< Get  if converged
      void DiagonalizeOneModeFock(LocalModeNr aOperMode,
            MidasMatrix& aFock, 
            bool aFinal= false,
            bool aStoreAll=false);              ///< Solve scf equations for one mode 

      void FixModalPhases(MidasMatrix& aModals);
      void FixModalPhasesRestartCheck(MidasMatrix& aModals,LocalModeNr aOperMode);
      ///< Fix the modal phases such that largest coefficient is positive.
      
      void ConstructOneModeFock(LocalModeNr aOperMode, MidasMatrix& arFock,
                                 bool aActiveTerms=false, In aHlevel = I_0,
                                 In* arNsc1=NULL, In* arNsc2=NULL, In* arNtot=NULL);
      ///< Construct Fock matrix for one mode.
      
      void ConstructPartialOneModeFock(LocalModeNr aOperMode, MidasMatrix& arFock, In aHlevel);
      ///< Construct Fock matrix using only a given MC level in the Hamiltonian.
      
      void BackupOldModals();                   ///< Backup the old modals in restart to later check new vs old for consistency 
      void StartGuess();                        ///< Initiate the vscf by a Start guess (input/restart)
      void Solve();                             ///< Solve scf equations.
      void Finalize(bool aRe=true);             ///< Final transforms, and analysis after convergence.
      void AllOccOneModeInt();                  ///< Generate all occupied one mode int.
      void AllOneModeOperNorms();               ///< Generate norms of one mode opers.
      void UpdateOccOneModeInt(LocalModeNr aOperMode);   ///< Update occupied one mode integrals for a particular mode
      void SaveOccModals();                     ///< Save the occupied modals on disc.
      void Erase();                             ///< Erase modals and other stuff 
      void RspErase();                          ///< Erase response vectors and stuff 
      bool GetOccModalsFromFile(const std::string& arFileName);  ///< get the occupied modals from file , true if success
      OpDef* pOpDef() const {return mpOpDef;}   ///< Return pointer to operator def.
      OpDef* pOpDefOrig() const {return mpOpDefOrig;}   ///< Return pointer to original operator def.
      const BasDef* const pBasDef() const {return mpBasDef;}///< Return pointer to basis def
      VscfCalcDef* pVscfCalcDef() const {return mpVscfCalcDef;} ///< Return pointer to calc def
      OneModeInt* pOneModeInt() {return mpOneModeInt;} ///< Return pointer to one mode int.
      const OneModeInt* pOneModeInt() const {return mpOneModeInt;} ///< Return pointer to one mode int.
      DataCont* pModals() {return &mModals;} ///< Return pointer to Modals
      const DataCont* pModals() const {return &mModals;} ///< Return pointer to Modals
      void SetOpDef(OpDef* apOpDef) {mpOpDefOrig = mpOpDef; mpOpDef = apOpDef;}//Set operator to a new one
      void RestoreOpDef() {mpOpDef = mpOpDefOrig; }//Set operator to the original one
      void SetOneModeInt(OneModeInt* apOneModeInt) {mpOneModeIntOrig= mpOneModeInt; mpOneModeInt= apOneModeInt;}
                                                   //Set integrals to new ones
      void RestoreOneModeInt() {mpOneModeInt= mpOneModeIntOrig; }//Set integrals to the original ones
      void ResetOccInt();                       /// Recalc. mOneModeIntOcc with present operator
      void TransInt(MidasMatrix& arM,LocalModeNr arOperMode);///< Transform primitive integrals to VSCF modal integrals
      void RspDrv();                            ///< Drive the calculation of response properties. 
      void RspEigDrv(bool aRight=true);         ///< Drive the calculation of response eigen values 
      void RspEigOutput(MidasVector& aEigVals, std::vector<DataCont>& aEigVecs);
      //void RspDecompEigDrv(bool aRight);
      void TensorRspEigDrv(bool aRight);
      virtual void MatchLeftRightRspEigVecs(MidasVector& aEigVals, std::vector<DataCont>& aEigVecs)
      {
         MIDASERROR("Vscf::MatchLeftRightRspEigVecs() not implemented.");
      }
      ///< Print results from RspEigDrv().

      template
         <  class VEC_T = DataCont
         >
      void RspFuncDrv();                        ///< Drive the calculation of response functions

      //! Solution-vector analysis
      void LinRspOutput
         (  std::vector<DataCont>&
         );

      //! Calculate SOS Rsp func based on calc. trans. mom. 
      template
         <  class VEC_T = DataCont
         >
      void SosRspFunc
         (  std::vector<RspFunc>&
         );

      void LinearSosRsp
         (  const std::set<std::string>&
         ,  const std::set<std::string>&
         ,  const MidasVector&
         ,  std::vector<RspFunc>&
         );

      void QuadraticSosRsp
         (  std::set<std::string>
         ,  std::set<std::string>
         ,  MidasVector
         ,  std::vector<RspFunc>&
         );

      void CubicSosRsp
         (  std::set<std::string>
         ,  std::set<std::string>
         ,  MidasVector
         ,  std::vector<RspFunc>&
         );

      void CheckStates
         (  std::set<RspFunc>&
         ,  const std::set<std::string>&
         ,  const std::set<std::string>&
         );

      template
         <  class VEC_T = DataCont
         >
      void RspEqSolveDrv();                     ///< Drive solution of rsp eq. 

      void RspEqSolve
         (  In aNeq
         ,  std::vector<DataCont>& apRhsDc
         ,  MidasVector& arFrqVec
         ,  std::string arKey
         ,  const MidasVector& aGamma
         ,  bool aRight=true
         ); ///< solve rsp eq. 

      //! Solve linear equations with TensorDataCont
      void TensorRspEqSolve
         (  std::vector<TensorDataCont>&
         ,  const std::string& aRestartKey
         ,  const MidasVector& aFrequencies
         ,  const MidasVector& aGamma
         ,  bool = true
         );

      //! Solve equations with specific template solver
      template
         <  class SOLVER
         >
      void TensorRspEqSolveImpl
         (  SOLVER&
         ,  const std::string&
         );

      template
         <  class VEC_T = DataCont
         >
      void PrepareRspOpers();                   ///< Prepare the rsp opers 

      template
         <  class VEC_T = DataCont
         >
      void FindRspEqToSolve();                  ///< Find rsp eq to solve 

      template
         <  class VEC_T = DataCont
         >
      void SolveZerothOrderEq();                ///< Solve all n_order = 0 equations

      template
         <  class VEC_T = DataCont
         >
      void SolveFirstOrderEq();                 ///< Solve all n_order = 1 equations

      template
         <  class VEC_T = DataCont
         >
      void SolveVccMEq();                       ///< Solve for VCC M vectors.

      template
         <  class VEC_T = DataCont
         >
      void SolveXToXTransitions();              ///< Solve all n_order = -11 equations

      template
         <  class VEC_T = DataCont
         >
      void CreateSigmaVectors();                ///< Create sigma vectors for use in QR

      template
         <  class VEC_T = DataCont
         >
      void SolveSecondOrderEq();                ///< Solve all n_order = 2 equations

      template
         <  class VEC_T = DataCont
         >
      void CalcNonVarEtaX();                    ///< Calcuate etaX vectors for non-variational wave functions.

      virtual void CalculateFR
         (  DataCont& aRes
         ,  In aState
         )
      {
         MIDASERROR("Vscf::CalculateFR() not implemented.");
      } 

      virtual void CalculateFR
         (  TensorDataCont& aRes
         ,  In aState
         )
      {
         MIDASERROR("Vscf::CalculateFR() not implemented.");
      } 
      
      //! Contract rsp vectors etc. to give final response functions
      template
         <  class VEC_T = DataCont
         >
      void RspContract();
      
      //! Construct resp. func.  expt value 
      template
         <  class VEC_T = DataCont
         >
      void RspExptValue
         (  In& aIrsp
         );

      //! Construct line. resp. func. 
      template
         <  class VEC_T = DataCont
         >
      void LrfContract
         (  In& aIrsp
         );

      //! Construct linear response function for non-variational wave function.
      virtual void LrfContractNonVar
         (  In aRspFunc
         )
      {
         MIDASERROR("Vscf::LrfContractNonVar() not implemented");
      }

      //! Construct linear response function for non-variational wave function.
      virtual void TensorLrfContractNonVar
         (  In aRspFunc
         )
      {
         MIDASERROR("Vscf::TensorLrfContractNonVar() not implemented");
      }
      
      void QrfContract(In& aIrsp);              ///< Construct quadr. resp. func.    
      void CrfContract(In& aIrsp);              ///< Construct quadr. resp. func.    
      Nb ComputeGXContribution(std::vector<std::pair<std::string,Nb> >);
      Nb ComputeHContribution(std::vector<std::pair<std::string,Nb> >);
      Nb ComputeFXContribution(std::vector<std::pair<std::string,Nb> >);
      Nb ComputeFContribution(std::vector<std::pair<std::string,Nb> >);
      void CheckSecondOrderParams(std::vector<std::pair<std::string,Nb> >);
      std::string GetSecondOrderRspVecName(Nb,Nb,std::string,std::string);
      std::string GetFirstOrderRspVecName(Nb,std::string);
      void QrfContribution
         (  Nb& qrf_value
         ,  std::string& sigma_op1
         ,  std::string& sigma_op2
         ,  Nb& sigma_frq
         ,  std::string& rsp_op
         ,  Nb& rsp_frq
         );

      //! Construct transition matrix elements g->x
      template
         <  class VEC_T = DataCont
         >
      void ResidContractGtoX(In& aIrsp);

      void ResidContractXtoX(In& aIrsp);         ///< Construct transition matrix elements x->x
      
      //! Add non-variational correction for g->x trasition matrix element. 
      virtual Nb ResidContractGtoXnonVar
         (  In aRspFunc
         ,  DataCont& aReigVec
         ,  Nb aEtaR
         )
      {
         MIDASERROR("Vscf::ResidContractGtoXnonVar() not implemented."); return -C_1;
      }

      //! Add non-variational correction for g->x trasition matrix element. 
      virtual Nb ResidContractGtoXnonVar
         (  In aRspFunc
         ,  const TensorDataCont& aReigVec
         ,  Nb aEtaR
         )
      {
         MIDASERROR("Vscf::ResidContractGtoXnonVar() not implemented."); return -C_1;
      }

      virtual void CalcNumVccF()
      {
         MIDASERROR("Vscf::CalcNumVccF() not implemented.");
      }
      
      //! Put first order response parameters in aDc.
      void GetFirstOrderRsp
         (  const std::string& aOper
         ,  Nb aFrq
         ,  DataCont& aDc
         );

      //! Put first order response parameters in aDc.
      void GetFirstOrderRsp
         (  const std::string& aOper
         ,  Nb aFrq
         ,  TensorDataCont& aDc
         );

      //! Put first order response parameters in aDc.
      void GetFirstOrderRsp
         (  const std::string& aOper
         ,  Nb aFrq
         ,  Nb aGamma
         ,  ComplexVector<DataCont, DataCont>& aDc
         );
     
      void GetMvec(In aRightState, Nb aFrq, DataCont& aDc);
      void GetMvec(In aRightState, Nb aFrq, TensorDataCont& aDc);
      
      virtual void PrepareNrspPar();                    ///< Nr of parameters in response
      virtual void CalculateRhs(DataCont& arRhs,const std::string& arOperName, In& arOperNr,Nb& arExptValue); ///< Calculate Rhs vector 
      virtual void CalculateRhs(TensorDataCont& arRhs,const std::string& arOperName, In& arOperNr,Nb& arExptValue); ///< Calculate Rhs vector 

      virtual void CalculateEta
         (  DataCont& arEta
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Eta vector 

      virtual void CalculateEta
         (  TensorDataCont& arEta
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Eta vector 

      virtual void CalculateXi
         (  DataCont& arXi
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Xi vector 

      virtual void CalculateXi
         (  TensorDataCont& arXi
         ,  const std::string& arOperName
         ,  In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Xi vector 

      virtual void CalculateEta0(); ///< Calculate eta0 vector 
      virtual void CalculateTensorEta0();

      virtual void CalculateSigma
         (  DataCont& arEigVec
         ,  DataCont& arSigma
         ,  const std::string& arOperName
         ,  const In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Sigma vector 

      virtual void CalculateSigma
         (  const TensorDataCont& arEigVec
         ,  TensorDataCont& arSigma
         ,  const std::string& arOperName
         ,  const In& arOperNr
         ,  Nb& arExptValue
         ); ///< Calculate Sigma vector 

      virtual void GeneralFTransformation(DataCont&,DataCont&,Nb&); ///< Calculate F transform

      template
         <  class VEC_T = DataCont
         >
      void AddRspVecToSet
         (  std::set<RspVecInf>& arRspVecInfSet
         ,  const RspFunc& arRspFunc
         ); //< Add to set of arRspVecInfSet

      In NrspPar() const {return mNrspPar;}           ///< Nr of parameters in response
      In NrspParHalf() const {return mNrspParHalf;}   ///< Nr of parameters in response, half
      virtual void RspModesAndLevelsOut(std::ostream& arOut, const In& aI, bool aAllOut = true);
      ///< Modes and levels out 
      
      In ExciOrDeExci(DataCont& arDc); ///< Is the vector an excitation of a de-excitation primarily

      void PrepareNewOccInt(In arNewOper); ///< Prepare new operator 
      void SetNrspPar(In aN) {mNrspPar=aN;}///< Set the Number of response parameters
      void SetNrspParHalf(In aN) {mNrspParHalf=aN;}///< Set the Number of response parameters


      void RspAutoTarget();     ///< Create fund,...etc targets for rsp calc. 
      In RspAddressForOccVec(std::vector<In>& arIvec, bool=true);       ///< Prepare rsp target vectors. 

      virtual In PrepareRspTargetStates();              ///< Prepare rsp target vectors. 

      //! Prepare target states in TensorDataCont format
      std::unique_ptr<std::vector<TensorDataCont>> PrepareTensorDataContTargetStates
         (  const midas::vcc::TransformerV3&
         );

      virtual void RspExciLevelWeight(std::vector<DataCont>& aEigVecs, In aRoots) {return;};
      
      void GetTargetForState(const std::string& aTargetVectors, In aI, DataCont& arDcIn);
      ///< Put vector in file aTargetVectors_[aI] into data container arDcIn.

      //! Extract IR data from calculated transition moments.
      void DoIR
         (  const std::string& = "_rsp_eigval"
         )  const;

      //! Extract Raman data from calculated transition moments.
      void DoRaman
         (  const std::string& = "_rsp_eigval"
         )  const;

      void AnalyzeResponseFunctions();
      std::vector<Nb> CalcOneModePartFunc(Nb aTemp,In aTempRow,bool aUseLim=false);
      Nb CalcOneStateTempDeriv(Nb,In);
      Nb CalcOneStateTemp2Deriv(Nb,In,Nb);
      void ConstructTempAve(OneModeInt* aOneModeInt);
      void NewConstructTempAve(OneModeInt* aOneModeInt);
      Nb CalculateTransInt(OneModeInt* a0, In aTempCol, In aTerm, In aFacInTerm);
      void GetTransformedDensity(MidasMatrix&,In,In,In,bool=false,In=I_0);
      void GetTransformedDensity(MidasMatrix&,In,In);
      void ThermalDensity(Nb);
      void ThermalLimit(Nb aTemp);
      void ThermalLimitForMode(In aBas, In aMode, Nb aTemp); 

      //!
      const DataCont& GetEigVal() const {return mEigVal;}

      //! Extract wave functions and 1p densities
      void CalcDensities();

      //!
      void DensPrepareIterative(const std::string& arModeLabel, MidasVector& arGridVals, MidasVector& arMeanVals, MidasVector& arMaxVals) const;

      //!
      void DensPrepareAnalysis(const std::string& arModeLabel, const In& i_state, MidasVector& arGridVals, MidasVector& arFuncVals, MidasVector& arDensVals) const;
      
      //!
      void ReadPotentialFromFile(const std::string& arModeLabels, MidasVector& arGrid, const Uin& arPropNo) const;
      
      //! Organize data for plotting
      void PertTheoryZPVC();

      virtual Transformer* GetRspTransformer(bool aRight=true);
      virtual void CleanupRspTransformer(Transformer* aTrf);
      virtual bool RspRestartFromPrev(Transformer* apTrf, DataCont& arDc, const In& arIEq,
            const In& arNVecSize, std::string aStorage);
      virtual void WriteRspRestartInfo(Transformer* apTrf, const In& arNVecSize);

      virtual void LanczosChainRspDrv()
      {
         MIDASERROR("Response functions using Lanczos chains are not supported in VSCF.");
      }

      void ConstructThermalSpectrum(ThermalSpectrumDef&);
      void ConstructZFunction(std::vector<std::complex<Nb> >&,const MidasVector&,ThermalSpectrumDef&);
      std::complex<Nb> ValueForTime(const Nb&,const MidasVector&,const In,OneModeInt&,
         map<std::pair<In,In>,std::complex<Nb> >&, std::vector<std::complex<Nb> >&,
         std::vector<vector<In> >&);
      void CreateActTermsVec(const Nb&,const MidasVector&,const In,OneModeInt&,
         map<std::pair<In,In>,std::complex<Nb> >&, std::vector<std::complex<Nb> >&,
         std::vector<vector<In> >&);
      std::complex<Nb> ComputeATerm(const Nb&,const std::vector<In>&,
         const MidasVector&,OneModeInt&, const In&, const In&,const In&);
      std::complex<Nb> ComputeBTerm(const std::vector<In>&,const MidasVector&,OneModeInt&,
         const In&,const In&);
      void RetrieveXandYMatrices(const In&,std::complex<Nb>**,std::complex<Nb>**,const MidasVector& ,
         const Nb&,const In&);
      void ClearIntermediateXandY(const In&);
      void ClearIntermediateZ(const In&);
      void RetrieveZMatrix(const In&,std::complex<Nb>**,const MidasVector&, const In&);
      bool CheckXandYPresent(const In&,std::complex<Nb>**,std::complex<Nb>**,const In&);
      
      virtual void BandLanczosChainRspDrv()
      {
         MIDASERROR("Response functions using BandLanczos chains are not supported in VSCF.");
      }

      bool OldModalsErr(){return mOldModalsErr;}
      void SetOldModalsErr(bool);
      
      const ModalIntegrals<Nb>* pIntegrals() const;
      ModalIntegrals<Nb>* pIntegrals();
      void UpdateIntegrals(); ///< Update modal integrals. 
      void PutOperatorToWorkingOperator(OpDef* apOpDef,OneModeInt* apOneModeInt); ///< Change operator 
      void RestoreOrigOper();             ///< Restore old operator.
      void IntAnalysis();
      Nb** MaxInt(const std::string& arType="all");
      std::vector<Nb> GenScreenEstimates(Nb** appMaxInt, const std::string& arType="all");
      std::vector<Nb> MinRecEigValPerMc();

      
      //virtual void ComplexRspDrv();
      void ComplexRspEqSolve
         (  In aNeq
         ,  std::vector<DataCont>& arRhsDc
         ,  MidasVector& arFrqVec
         ,  std::string arKey
         ,  const MidasVector& aGamma
         ,  bool aRight
         );

      template
         <  class SOLVER
         >
      auto RunComplexRspEqSolve
         (  std::vector<ComplexVector<DataCont,ZeroVector<> > >& y
         ,  MidasComplexFreqShiftedTransformer& midas_trf
         ,  const std::string& arKey
         ) 
         -> typename std::remove_reference<decltype(std::declval<SOLVER>().Solution())>::type;

      // Solve equations with TensorDataCont
      template
         <  class SOLVER
         >
      void TensorRspEigSolve
         (  SOLVER& aSolver
         ,  bool aRight
         );
      
      //virtual ComplexRspCalcDef& ComplexRspCalc(In i)
      //{
      //   return mpVscfCalcDef->ComplexRspCalc(i);
      //}
      std::vector<ComplexVector<DataCont,ZeroVector<Nb> > > GetComplexRhs(std::vector<DataCont>&) const;

      virtual void PrintWeightInfo(const std::vector<vector<In> >&, DataCont&, In) {;}

      friend class VscfRspTransformer;

      virtual void DensityMatrixAnalysis();                                     ///< Calculate Density Matrix and analyse 
      virtual void CalcGsDensityMatrixVscfBasis(std::vector<MidasMatrix>& arDmats);                  ///< Calculate GS Density Matrix 
      virtual void NaturalModalAnalysis(std::vector<MidasMatrix>& arDmats);  ///< Natural modal analysis of Density Matrices 
      void TransformDensityFromVscfToPrim(MidasMatrix& arDensVscf,MidasMatrix& arDenPrim,const In& arM); /// Transform D 

      //! Dump modals under special filename (so not overwritten) for later use by MatRep Vcc calc.
      void DumpModalsForMatRep() const;

};
#endif /* VSCF_H_INCLUDED */

// Include implementation
#include "vscf/Vscf_Impl.h"

