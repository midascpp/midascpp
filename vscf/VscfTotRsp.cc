/**
************************************************************************
* 
* @file                VscfTotRsp.cc
*
* Created:             01-05-2009
*
* Author:              Mikkel Bo Hansen 
*                      Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing total response = el+vib
* 
* Last modified: Thu Jun 11, 2009  05:56PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<set>
#include<string>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"

using namespace midas;

/**
 *
 **/
bool IsEnergyOrDipole
   (  const OpInfo& aOpInfo
   )
{
   return   aOpInfo.GetType().Is(oper::unknown)
         || aOpInfo.GetType().Is(oper::energy) 
         || aOpInfo.GetType().Is(oper::dipole) 
         ;
}

/**
 *  Find a specific polarizability term
 **/
bool CheckAlpha
   (  const oper::axis aAxis1
   ,  const oper::axis aAxis2
   ,  const Nb aFrq
   ,  const std::vector<TotalRspFunc>& aV
   ,  Nb& aVal
   ,  Nb& aPV
   ,  Nb& aZP
   ) 
{
   for(In i = I_0; i < aV.size(); ++i) 
   {
      const auto& op_info1 = OpDef::msOpInfo[aV[i].GetRspOp(I_0)];
      const auto& op_info2 = OpDef::msOpInfo[aV[i].GetRspOp(I_1)];
      
      if (  op_info1.GetType().m_axis[0] == aAxis1
         && op_info2.GetType().m_axis[0] == aAxis2
         && aFrq == aV[i].GetRspFrq(I_1)
         ) 
      {
         aVal += aV[i].Value();
         aPV  += aV[i].GetPVLPlus();
         aZP  += aV[i].GetZPVC();
         return true;
      }
   }
   return false;
}

/**
 *  Find a specific 1st hyper-polarizability term
 **/
bool CheckBeta
   (  const oper::axis aS1
   ,  const oper::axis aS2
   ,  const oper::axis aS3
   ,  const Nb aFrq1
   ,  const Nb aFrq2
   ,  const std::vector<TotalRspFunc>& aV
   ,  Nb& aVal
   ,  Nb& aPVLPlus
   ,  Nb& aPVLMinus
   ,  Nb& aPVQ
   ,  Nb& aZP
   ) 
{
   for(In i = I_0; i < aV.size(); i++) 
   {
      const auto& op_info1 = OpDef::msOpInfo[aV[i].GetRspOp(I_0)];
      const auto& op_info2 = OpDef::msOpInfo[aV[i].GetRspOp(I_1)];
      const auto& op_info3 = OpDef::msOpInfo[aV[i].GetRspOp(I_2)];

      if (  op_info1.GetType().m_axis[0] == aS1 
         && op_info2.GetType().m_axis[0] == aS2 
         && op_info3.GetType().m_axis[0] == aS3 
         && aFrq1 == aV[i].GetRspFrq(I_1) 
         && aFrq2 == aV[i].GetRspFrq(I_2)
         ) 
      {
         aVal      += aV[i].Value();
         aPVLPlus  += aV[i].GetPVLPlus();
         aPVLMinus += aV[i].GetPVLMinus();
         aPVQ      += aV[i].GetPVQ();
         aZP       += aV[i].GetZPVC();
         return true;
      }
   }
   return false;
}

/**
 *  Find a specific mixed term, i.e. a linear response function with two
 *  specific operators.
 *
 *  aS1   = dipole operator
 *  aS2   = polarizability operator
 *  aFrq1 = freq. of the electronic LR operator
 *  aFrq2 = freq. of the vibrational LR function
 *
 **/
bool CheckMixedTerms
   (  const oper::axis aS1
   ,  const oper::axis aS2[2]
   ,  const std::vector<RspFunc>& aV
   ,  const Nb aFrq1
   ,  const Nb aFrq2
   ,  Nb& aSymVal
   ,  Nb& aAsymVal
   ,  const Nb aAsym_Sign
   ) 
{ 
   auto are_equal = [](const oper::axis a1[2], const oper::axis a2[2]) -> bool
   {
      return ((a1[0] == a2[0]) && (a1[1] == a2[1]));
   };

   bool plusplus   = false;
   bool minusminus = false;
   if (  (  gOperatorDefs.GetNrOfModes() == I_1 
         && aS2[0] != aS2[1]
         ) 
      || (aFrq1 == C_0 && aFrq2 == C_0)
      )
   {
      minusminus = true;
   }

   for(In i=I_0;i<aV.size();i++) 
   {
      const OpInfo& op_info1 = OpDef::msOpInfo[aV[i].GetRspOp(I_0)];
      const OpInfo& op_info2 = OpDef::msOpInfo[aV[i].GetRspOp(I_1)];
      oper::axis pol_op[2]     = { op_info2.GetType().GetAxis(0), op_info2.GetType().GetAxis(1)};
      oper::axis pol_op_rev[2] = { op_info2.GetType().GetAxis(1), op_info2.GetType().GetAxis(0)};

      // symmetric part
      if (  !IsEnergyOrDipole(op_info2) 
         && !aV[i].GetDoAsymmetric() 
         && !plusplus
         )
      { // ++ combination
         if (  aS1 == op_info1.GetType().GetAxis(0)
            && (are_equal(aS2, pol_op) || are_equal(aS2, pol_op_rev))
            && fabs(op_info2.GetFrq(0)) == fabs(aFrq1) 
            && fabs(aV[i].GetRspFrq(I_1)) == fabs(aFrq2)
            ) 
         { 
            // as we are looking for a sym. RF!
            aSymVal += aV[i].Value();
            plusplus = true;
         }
      }
      // anti-symmetric part
      if (  !IsEnergyOrDipole(op_info2)    // oi2.GetType() > 200 
         && aV[i].GetDoAsymmetric() 
         && !minusminus
         ) 
      { // -- combination, pos. frq.
         if (  aS1 == op_info1.GetType().GetAxis(0)
            && (  (are_equal(aS2, pol_op)     && op_info2.GetFrq(0) == aFrq1) 
               || (are_equal(aS2, pol_op_rev) && op_info2.GetFrq(0) == aFrq1) 
               ) 
            && aV[i].GetRspFrq(I_1) == aFrq2
            ) 
         {
            if(are_equal(aS2, pol_op_rev))
            {
               aAsymVal -= aAsym_Sign*aV[i].Value();
            }
            else
            {
               aAsymVal += aAsym_Sign*aV[i].Value();
            }
            minusminus = true;
         }
         else if
            (  aS1 == op_info1.GetType().GetAxis(0) 
            && (  (are_equal(aS2, pol_op)     && op_info2.GetFrq(0) == C_M_1 * aFrq1)
               || (are_equal(aS2, pol_op_rev) && op_info2.GetFrq(0) == C_M_1 * aFrq1)
               ) 
            && aV[i].GetRspFrq(I_1) == aFrq2
            ) 
         {
            if(are_equal(aS2, pol_op_rev))
            {
               aAsymVal += aAsym_Sign*aV[i].Value();
            }
            else
            {
               aAsymVal -= aAsym_Sign*aV[i].Value();
            }
            minusminus = true;
         }
      }
      else if(plusplus && minusminus) 
      {
         return true;
      }
   }

   if(plusplus && minusminus)
   {
      return true;
   }
   
   return false;
}

/**
*  Analyze response functions to see whether it is possible
*  to generate the total (el+vib) response.
**/
void Vscf::AnalyzeResponseFunctions
   (
   ) 
{
   std::vector<RspFunc> linrsp_zpvc;
   std::vector<RspFunc> mixed_terms;
   std::vector<RspFunc> qrsp_zpvc;
   std::vector<TotalRspFunc> total_lr,total_qr;
   std::set<Nb> lr_frq,qr_frq1,qr_frq2;
   OpInfo op_info1;
   OpInfo op_info2;
   OpInfo op_info3;
   OpInfo op_info4;
   Nb value;
   for(In i_rf=I_0;i_rf<mpVscfCalcDef->NrspFuncs();i_rf++) 
   {
      if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() < I_1)
      {
         continue;
      }
      if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() > I_1)
      {
         continue;
      }
      else 
      {
         // check if that value is either a polarizability or a hyperpol.
         // determine by name: pol => xx_pol_frq, h. pol => xxx_pol_frq1_frq2
         string opername=(mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_0);
         op_info1=OpDef::msOpInfo[opername];
         if (  (  op_info1.GetType().Is(oper::dipole) 
               || op_info1.GetType().Is(oper::polarizability2)
               )
            && opername.find("ANTI")==opername.npos
            )
         {
            // we have a polarizability expt. val
            linrsp_zpvc.push_back(mpVscfCalcDef->GetRspFunc(i_rf));
         }
         else if  (  op_info1.GetType().Is(oper::polarizability3))
         {
            // we have a hyperpolarizability expt. val
            qrsp_zpvc.push_back(mpVscfCalcDef->GetRspFunc(i_rf));
         }
      }
   }
   // we have the LR expt vals, get the relevant LRF's and make total LR
   for(In i_rf=I_0;i_rf<mpVscfCalcDef->NrspFuncs();i_rf++) 
   {
      if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() < I_2)
      {
         continue;
      }
      else if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() > I_2)
      {
         continue;
      }
      else 
      {
         const std::string& opername1 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_0);
         const std::string& opername2 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_1);
         op_info1 = OpDef::msOpInfo[opername1];
         op_info2 = OpDef::msOpInfo[opername2];
         if (  IsEnergyOrDipole(op_info1)
            && IsEnergyOrDipole(op_info2)
            )
         {
            // we have a LR with dipole opers. Now see if we have the expt. val.
            for(In i_ev=I_0;i_ev<linrsp_zpvc.size();i_ev++) 
            {
               const std::string& exp_opername = linrsp_zpvc[i_ev].GetRspOp(I_0);
               op_info3 = OpDef::msOpInfo[exp_opername];
               
               if (  op_info1.GetType().GetAxis(0) == op_info3.GetType().GetAxis(0)
                  && op_info2.GetType().GetAxis(0) == op_info3.GetType().GetAxis(1)
                  && op_info3.GetFrq(0) == (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspFrq(I_1)
                  ) 
               {
                  value=(mpVscfCalcDef->GetRspFunc(i_rf)).Value();
                  TotalRspFunc rf(I_2);
                  rf.SetOp(opername1);
                  rf.SetOp(opername2,I_1);
                  rf.SetFrq(op_info3.GetFrq(0),I_1);
                  rf.SetHasBeenEval(true);
                  value-=linrsp_zpvc[i_ev].Value();
                  rf.SetValue(value);
                  // Pol. does not have asym. part
                  rf.SetPVL((mpVscfCalcDef->GetRspFunc(i_rf)).Value(),C_0);
                  rf.SetZPVC(C_M_1*linrsp_zpvc[i_ev].Value());
                  total_lr.push_back(rf);
                  lr_frq.insert(op_info3.GetFrq(0));
               }
            }
         }
         else if  
            (  ( IsEnergyOrDipole(op_info1) && !IsEnergyOrDipole(op_info2))
            || (!IsEnergyOrDipole(op_info1) &&  IsEnergyOrDipole(op_info2))
            ) 
         {
            // Mixed term, add it for use later
            mixed_terms.push_back(mpVscfCalcDef->GetRspFunc(i_rf));
         }
      }
   }
   // we have the QR expt vals, get the relevant RF's and make total QR
   for(In i_rf=I_0;i_rf<mpVscfCalcDef->NrspFuncs();i_rf++) 
   {
      if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() < I_3)
      {
         continue;
      }
      else if((mpVscfCalcDef->GetRspFunc(i_rf)).GetNorder() > I_3)
      {
         break; // Is this correct? Are response functions sorted?
      }
      else 
      {
         const std::string& opername1 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_0);
         const std::string& opername2 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_1);
         const std::string& opername3 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspOp(I_2);
         op_info1 = OpDef::msOpInfo[opername1];
         op_info2 = OpDef::msOpInfo[opername2];
         op_info3 = OpDef::msOpInfo[opername3];

         if (  IsEnergyOrDipole(op_info1) 
            && IsEnergyOrDipole(op_info2) 
            && IsEnergyOrDipole(op_info3)
            ) 
         {
            // we have a QR with dipole opers. Now see if we have the expt. val.
            for(In i_ev=I_0;i_ev<qrsp_zpvc.size();i_ev++) 
            {
               const std::string& exp_opername=qrsp_zpvc[i_ev].GetRspOp(I_0);
               op_info4 = OpDef::msOpInfo[exp_opername];

               if (  op_info1.GetType().GetAxis(0) == op_info4.GetType().GetAxis(0)
                  && op_info2.GetType().GetAxis(0) == op_info4.GetType().GetAxis(1)
                  && op_info3.GetType().GetAxis(0) == op_info4.GetType().GetAxis(2)
                  && op_info4.GetFrq(0) == (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspFrq(I_1) 
                  && op_info4.GetFrq(2) == (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspFrq(I_2)
                  ) 
               {
                  value=(mpVscfCalcDef->GetRspFunc(i_rf)).Value();
                  // next line - as we are reporting in the Rsp. func. repr.
                  value-=qrsp_zpvc[i_ev].Value();
                  // that was the PV and VA contributions, now the mixed terms
                  oper::axis s1    = op_info1.GetType().GetAxis(0);
                  oper::axis s2    = op_info2.GetType().GetAxis(0);
                  oper::axis s3    = op_info3.GetType().GetAxis(0);

                  oper::axis s4[2] = { op_info2.GetType().GetAxis(0), op_info3.GetType().GetAxis(0) };
                  oper::axis s5[2] = { op_info3.GetType().GetAxis(0), op_info2.GetType().GetAxis(0) };
                  oper::axis s6[2] = { op_info1.GetType().GetAxis(0), op_info3.GetType().GetAxis(0) };
                  oper::axis s7[2] = { op_info3.GetType().GetAxis(0), op_info1.GetType().GetAxis(0) };
                  oper::axis s8[2] = { op_info1.GetType().GetAxis(0), op_info2.GetType().GetAxis(0) };
                  oper::axis s9[2] = { op_info2.GetType().GetAxis(0), op_info1.GetType().GetAxis(0) };
                  
                  Nb frq1 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspFrq(I_1);
                  Nb frq2 = (mpVscfCalcDef->GetRspFunc(i_rf)).GetRspFrq(I_2);
                  Nb sym_val  = C_0;
                  Nb asym_val = C_0;
                  
                  if (  CheckMixedTerms(s1, s4, mixed_terms, frq2,        (frq1+frq2), sym_val, asym_val, C_1)   // << X,<<Y,Z>>w_z >>-w_x
                     && CheckMixedTerms(s1, s5, mixed_terms, frq1,        (frq1+frq2), sym_val, asym_val, C_1)   // << X,<<Z,Y>>w_y >>-w_x
                     && CheckMixedTerms(s2, s6, mixed_terms, frq2,        C_M_1*frq1,  sym_val, asym_val, C_1)   // << Y,<<X,Z>>w_z >>-w_y
                     && CheckMixedTerms(s2, s7, mixed_terms, (frq1+frq2), C_M_1*frq1,  sym_val, asym_val, C_M_1) // << Y,<<Z,X>>w_x >>-w_y
                     && CheckMixedTerms(s3, s8, mixed_terms, frq1,        C_M_1*frq2,  sym_val, asym_val, C_1)   // << Z,<<X,Y>>w_y >>-w_z
                     && CheckMixedTerms(s3, s9, mixed_terms, (frq1+frq2), C_M_1*frq2,  sym_val, asym_val, C_M_1)
                     ) 
                  {           // << Z,<<Y,X>>w_x >>-w_z
                     // minus, as LR op is assumed to be polarizability, not LR
                     value -= C_I_2*(sym_val+asym_val);
                  }
                  else 
                  {
                     Mout << "Could not generate total response, some mixed terms missing..." << endl;
                     break;
                  }
                  TotalRspFunc rf(I_3);
                  rf.SetOp(opername1);
                  rf.SetOp(opername2,I_1);
                  rf.SetOp(opername3,I_2);
                  rf.SetFrq(op_info4.GetFrq(0),I_1);
                  rf.SetFrq(op_info4.GetFrq(1),I_2);
                  rf.SetValue(value);
                  rf.SetPVL(C_I_2*sym_val,C_I_2*asym_val);
                  rf.SetPVQ((mpVscfCalcDef->GetRspFunc(i_rf)).Value());
                  rf.SetZPVC(-qrsp_zpvc[i_ev].Value());
                  rf.SetHasBeenEval(true);
                  total_qr.push_back(rf);
                  qr_frq1.insert(op_info4.GetFrq(0));
                  qr_frq2.insert(op_info4.GetFrq(1));
                  break;
               }
            }
         }
      }
   }
   //try to print the results...
   Mout << " TOTAL LINEAR RESPONSE FUNCTIONS (ZPVC + PV)   : " << endl;
   for(In i=I_0;i<total_lr.size();i++) 
   {
      Mout << total_lr[i] << endl;
   }
   Mout << " TOTAL QUADRATIC RESPONSE FUNCTIONS (ZPVC + PV): " << endl;
   for(In i=I_0;i<total_qr.size();i++) 
   {
      Mout << total_qr[i] << endl;
   }
   // Some futher checks:
   Mout << "\n Properties from response functions, polarizabilities:" << endl << endl;
   for(std::set<Nb>::const_iterator i=lr_frq.begin(); i!=lr_frq.end();i++) 
   {
      // check if total alfa_iso is possible
      Nb xx=C_0,yy=C_0,zz=C_0;
      Nb xx_pv=C_0,yy_pv=C_0,zz_pv=C_0;
      Nb xx_zp=C_0,yy_zp=C_0,zz_zp=C_0;
      
      if (  CheckAlpha(oper::axis::x, oper::axis::x, *i, total_lr, xx, xx_pv, xx_zp)
         && CheckAlpha(oper::axis::y, oper::axis::y, *i, total_lr, yy, yy_pv, yy_zp)
         && CheckAlpha(oper::axis::z, oper::axis::z, *i, total_lr, zz, zz_pv, zz_zp)
         ) 
      {
         Mout << "1st Order properties for frq=" << *i << endl << endl;
         /* Old output format
         Mout << " TOTAL Iso(" << *i << ")   = " << C_M_1*(xx+yy+zz)/C_3 << endl;
         Mout << " PV    Iso(" << *i << ")   = " << C_M_1*(xx_pv+yy_pv+zz_pv)/C_3 << endl;
         Mout << " ZPVC  Iso(" << *i << ")   = " << C_M_1*(xx_zp+yy_zp+zz_zp)/C_3 << endl;
         */
         Mout << " PV    Iso   = " << C_M_1*(xx_pv+yy_pv+zz_pv)/C_3 << endl;
         Mout << " ZPVC  Iso   = " << C_M_1*(xx_zp+yy_zp+zz_zp)/C_3 << endl;
         Mout << " TOTAL Iso   = " << C_M_1*(xx+yy+zz)/C_3 << endl;
         Nb aniso=sqrt(((xx-yy)*(xx-yy)+(xx-zz)*(xx-zz)+(yy-zz)*(yy-zz))*C_I_2);
         Nb aniso_pv=sqrt(((xx_pv-yy_pv)*(xx_pv-yy_pv)+(xx_pv-zz_pv)*(xx_pv-zz_pv)+(yy_pv-zz_pv)*(yy_pv-zz_pv))*C_I_2);
         Nb aniso_zp=sqrt(((xx_zp-yy_zp)*(xx_zp-yy_zp)+(xx_zp-zz_zp)*(xx_zp-zz_zp)+(yy_zp-zz_zp)*(yy_zp-zz_zp))*C_I_2);
         /* Old output format
         Mout << " TOTAL Aniso(" << *i << ") = " << aniso << endl;
         Mout << " PV    Aniso(" << *i << ") = " << aniso_pv << endl;
         Mout << " ZPVC  Aniso(" << *i << ") = " << aniso_zp << endl;
         */
         Mout << " PV    Aniso = " << aniso_pv << endl;
         Mout << " ZPVC  Aniso = " << aniso_zp << endl;
         Mout << " TOTAL Aniso = " << aniso << endl << endl;
      }
   }
   Mout << "\n Properties from response functions, hyper-polarizabilities:" << endl << endl;
   for(std::set<Nb>::const_iterator i=qr_frq1.begin(); i!=qr_frq1.end();i++) 
   {
      for(std::set<Nb>::const_iterator j=qr_frq2.begin(); j!=qr_frq2.end();j++) 
      {
         Nb xxz=C_0,yyz=C_0,zzz=C_0,xzx=C_0,yzy=C_0,zxx=C_0,zyy=C_0;
         Nb xxz_pvlplus=C_0,yyz_pvlplus=C_0,zzz_pvlplus=C_0,xzx_pvlplus=C_0,yzy_pvlplus=C_0,zxx_pvlplus=C_0,zyy_pvlplus=C_0;
         Nb xxz_pvlminus=C_0,yyz_pvlminus=C_0,zzz_pvlminus=C_0,xzx_pvlminus=C_0,yzy_pvlminus=C_0,zxx_pvlminus=C_0,zyy_pvlminus=C_0;
         Nb xxz_pvq=C_0,yyz_pvq=C_0,zzz_pvq=C_0,xzx_pvq=C_0,yzy_pvq=C_0,zxx_pvq=C_0,zyy_pvq=C_0;
         Nb xxz_zp=C_0,yyz_zp=C_0,zzz_zp=C_0,xzx_zp=C_0,yzy_zp=C_0,zxx_zp=C_0,zyy_zp=C_0;

         oper::axis s1 = oper::axis::x;
         oper::axis s2 = oper::axis::y; 
         oper::axis s3 = oper::axis::z;
         
         if (  CheckBeta(s1, s1, s3, *i, *j, total_qr, xxz, xxz_pvlplus, xxz_pvlminus, xxz_pvq, xxz_zp)
            && CheckBeta(s2, s2, s3, *i, *j, total_qr, yyz, yyz_pvlplus, yyz_pvlminus, yyz_pvq, yyz_zp)
            && CheckBeta(s3, s3, s3, *i, *j, total_qr, zzz, zzz_pvlplus, zzz_pvlminus, zzz_pvq, zzz_zp) 
            && CheckBeta(s1, s3, s1, *i, *j, total_qr, xzx, xzx_pvlplus, xzx_pvlminus, xzx_pvq, xzx_zp)
            && CheckBeta(s2, s3, s2, *i, *j, total_qr, yzy, yzy_pvlplus, yzy_pvlminus, yzy_pvq, yzy_zp) 
            && CheckBeta(s3, s1, s1, *i, *j, total_qr, zxx, zxx_pvlplus, zxx_pvlminus, zxx_pvq, zxx_zp)
            && CheckBeta(s3, s2, s2, *i, *j, total_qr, zyy, zyy_pvlplus, zyy_pvlminus, zyy_pvq, zyy_zp)
            )
         {
            Mout << "2nd Order properties for frq1=" << *i << " and frq2=" << *j << endl << endl;
            //Nb beta_par=xxz+yyz+C_3*zzz+xzx+yzy+zxx+zyy;
            Nb beta_par_pvl_plus=xxz_pvlplus+yyz_pvlplus+C_3*zzz_pvlplus+xzx_pvlplus+yzy_pvlplus
               +zxx_pvlplus+zyy_pvlplus;
            Nb beta_par_pvl_minus=xxz_pvlminus+yyz_pvlminus+C_3*zzz_pvlminus+xzx_pvlminus+yzy_pvlminus
               +zxx_pvlminus+zyy_pvlminus;
            Nb beta_par_pvq=xxz_pvq+yyz_pvq+C_3*zzz_pvq+xzx_pvq+yzy_pvq+zxx_pvq+zyy_pvq;
            Nb beta_par_zp=xxz_zp+yyz_zp+C_3*zzz_zp+xzx_zp+yzy_zp+zxx_zp+zyy_zp;
            Mout << " PVL+  beta_|| = " << -beta_par_pvl_plus/C_5 << endl;
            Mout << " PVL-  beta_|| = " << -beta_par_pvl_minus/C_5 << endl;
            Mout << " PVL   beta_|| = " << -(beta_par_pvl_minus+beta_par_pvl_plus)/C_5 << endl;
            Mout << " PVQ   beta_|| = " << -beta_par_pvq/C_5 << endl;
            Mout << " ZPVC  beta_|| = " << -beta_par_zp/C_5 << endl;
            Mout << " TOTAL beta_|| = " << -(beta_par_pvl_plus+beta_par_pvl_minus+beta_par_pvq+beta_par_zp)/C_5 << endl << endl;
            //Mout << " TOTAL beta_|| = " << -beta_par/C_5 << endl << endl;
            /* Old output format
            Mout << " TOTAL beta_||(" << *i << "," << *j << ") = " << C_M_1*beta_par/C_5 << endl;
            Mout << " PV    beta_||(" << *i << "," << *j << ") = " << C_M_1*beta_par_pv/C_5 << endl;
            Mout << " ZPVC  beta_||(" << *i << "," << *j << ") = " << C_M_1*beta_par_zp/C_5 << endl << endl;
            */
         }
      }
   }
   Mout << "\nWARNING: TOTAL QRF's MAY NOT BE COMPLETELY CORRECT YET!\n" << endl;
}

/**
*  Calculate and output ZPVC's as calculated by perturbation
*  theory.
**/
void Vscf::PertTheoryZPVC() 
{
   Mout << "Entering PT-ZPVC" << endl;
   // loop over all response operators, and compute ZPVC's as given
   // by Ruden et al, JCP, 118, 9573 (2003)
   // The potential energy operator is always just mpOpDef!
   vector< pair<string,Nb> > result;
   In modes=mpOpDef->NmodesInOp();
   map<string,In> opers=mpVscfCalcDef->GetRspOpers();
   // Maybe it is easiest to just make two vectors containing the
   // 2M3T part of the potential and harmonic frequencies.
   map<string,Nb> fkll;
   for(LocalModeNr i_m1=I_0;i_m1<modes;i_m1++) 
   {
      for(LocalModeNr i_m2=I_0;i_m2<modes;i_m2++) 
      {
         // Get the right term in the Hamiltonian
         string term = std::to_string(i_m1) + "_" + std::to_string(i_m2) + std::to_string(i_m2);
         Nb coef = mpOpDef->GetCoefForPairOper(i_m1, i_m2, I_1, I_2);
         // Remember: coef = 1/6 * d3E/dq3 => d3E/dq3 = 6 * coef!
         if(i_m1!=i_m2)
            coef*=C_2;
         else
            coef*=C_6;
         fkll.insert(make_pair(term,coef));
      }
   }
   // We now have a map with all F_KLL terms
   for(map<string,In>::iterator i_op=opers.begin();i_op!=opers.end();i_op++) 
   {
      // now loop over all modes (twice)
      In oper_nr=gOperatorDefs.GetOperatorNr(i_op->first);
      if(oper_nr==-1)
      {
         MIDASERROR("Operator "+i_op->first+" not found");
      }
      OpDef* op_def = &gOperatorDefs[oper_nr];
      Nb single_result_a = C_0;
      Nb single_result_b = C_0;
      for(LocalModeNr k = I_0; k < modes; k++) 
      {
         // first term, 1/omega_i_m1 * d^2/dQ_kA^2
         Nb omega_k = mpOpDef->HoOmega(k);
         Nb dp2_dq2 = op_def->GetCoefForPairOper(k,k,I_2,I_0);
         //Mout << "For mode: " << k << ", d2p/dq2 = " << dp2_dq2 << " omega = " << omega_k << endl;
         // Remember: coef = 1/2 * dp2_dq2 => dp2_dq2 = 2 * coef
         dp2_dq2*=C_2;
         single_result_a+=(dp2_dq2/omega_k);
         Nb help_sum=C_0;
         for(In l=I_0;l<modes;l++) 
         {
            string term=std::to_string(k)+"_"+std::to_string(l)+std::to_string(l);
            Nb f_term=fkll.find(term)->second;
            Nb omega_l=mpOpDef->HoOmega(l);
            help_sum+=(f_term/omega_l);
         }
         Nb dp_dq=op_def->GetCoefForPairOper(k,k,I_1,I_0);
         // Mout << "For mode: " << k << ", dp/dq = " << dp_dq << endl;
         Nb contrib=dp_dq*help_sum;
         contrib/=(omega_k*omega_k);
         single_result_b+=contrib;
         // if this is the energy, remember to add the HO
         // energy
         if(mpOpDef->Name()==op_def->Name())
            single_result_a+=omega_k;
      }
      //Nb single_result=(single_result_a+single_result_b)/C_4;
      // Try to do it the "Barone" way...
      Nb single_result=(single_result_a-single_result_b)/C_4;
      result.push_back(make_pair(op_def->Name(),single_result));
   } 
   // print results
   Mout << endl;
   Out72Char(Mout,'#','#','#');
   Mout << "# PERTURBATION THEORY ZPVC's                                           #" << endl;
   Out72Char(Mout,'#','#','#');
   for(In i=I_0;i<result.size();i++) 
   {
      string this_oper_string="PT-ZPVC("+result[i].first+")";
      Mout << setw(40) << this_oper_string << " = " << result[i].second << endl;
   }
   Out72Char(Mout,'#','#','#');
}
