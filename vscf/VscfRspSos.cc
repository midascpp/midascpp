/**
************************************************************************
* 
* @file                VscfRspSos.cc
*
* Created:             01-05-2009
*
* Author:              Mikkel Bo Hansen 
*                      Ove Christiansen (ove@chem.au.dk)
*          
*
* Short Description:   Implementing Vscf Rsp sum over states 
* 
* Last modified: Mon Jun 15, 2009  04:38PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<set>
#include<string>
using std::set;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"

/**
 *
 **/
void Vscf::LinearSosRsp
   (  const std::set<std::string>& aGtox
   ,  const std::set<std::string>& aXtox
   ,  const MidasVector& aEig
   ,  std::vector<RspFunc>& aVec
   )
{
   Mout << "\nEntering LinearSosRsp...\n" << std::endl;
   std::vector<std::string> ops;
   for(const auto& s_op : aGtox)
   {
      ops.insert(ops.begin(),2,s_op);
   }
   std::sort(ops.begin(), ops.end());
   std::set<std::vector<std::string>> unique_rf;
   do
   {
      std::vector<std::string> v;
      v.push_back(ops[0]);
      v.push_back(ops[1]);
      unique_rf.insert(v);
   }  while (  std::next_permutation(ops.begin(), ops.end())
            );

   // make a set of all response functions (cannot search a vector)
   std::set<RspFunc> rsp_funcs;
   for(In i=0; i<mpVscfCalcDef->NrspFuncs(); ++i)
   {
      rsp_funcs.insert(mpVscfCalcDef->GetRspFunc(i));
   }

   // mbh: here check which states we have <0|X|i> for...
   std::set<std::string> dummy;
   dummy.clear();
   CheckStates(rsp_funcs, aGtox, dummy);
   for(auto& unique : unique_rf)
   {
      Nb frq=C_0;
      Nb frq_inc=C_0;
      if (  mpVscfCalcDef->SosFreqSteps() > I_0
         )
      {
         frq_inc = (mpVscfCalcDef->SosFreqEnd()-mpVscfCalcDef->SosFreqBegin()) / (Nb(mpVscfCalcDef->SosFreqSteps()));
      }
      for(In i_frq=0; i_frq<mpVscfCalcDef->SosFreqSteps()+1; ++i_frq)
      {
         frq = mpVscfCalcDef->SosFreqBegin() + Nb(i_frq)*frq_inc;

         // now loop over all permutations of the first four operators
         std::vector<std::string> unique_ops = unique;
         std::vector<std::pair<std::string, Nb> > current_ops;
         current_ops.push_back(std::make_pair(unique_ops[0],-frq));
         current_ops.push_back(std::make_pair(unique_ops[1],frq));
         std::vector<std::pair<std::string, Nb> > start_ops=current_ops;
         std::sort(current_ops.begin(), current_ops.end());
         Nb sos_lrf=C_0;
         In act_perm=I_0;
         In count=0;
         do
         {
            ++act_perm;

            // find all <0|op(0)|i>
            bool level1 = true;
            In state_i = -I_1;
            while (  level1
                  )
            {
               state_i++;
               RspFunc help(-1);
               help.SetOp(current_ops[0].first);
               help.SetLeftState(0);
               help.SetRightState(state_i);
               if (  rsp_funcs.find(help) != rsp_funcs.end()
                  )
               {
                  Nb oxi = (*rsp_funcs.find(help)).Value();

                  // all <j|op[1]|0>
                  help.SetOp(current_ops[1].first);
                  help.SetLeftState(0);
                  help.SetRightState(state_i);
                  if (  rsp_funcs.find(help) != rsp_funcs.end()
                     )
                  {
                     Nb iyo = (*rsp_funcs.find(help)).Value();
                     Nb w_i = aEig[state_i];

                     // contribution only if ex
                     if (  w_i < C_0
                        )
                     {
                        continue;
                     }
                     sos_lrf += oxi*iyo/(current_ops[1].second-w_i);

                     ++count;
                  }
               }
               else
               {
                  level1 = false;
               }
            }
   
         }  while (  std::next_permutation(current_ops.begin(), current_ops.end())
                  );

         Mout << "Count is: " << count << " act perm is: " << act_perm << std::endl;
         Nb fac = C_2/(Nb(act_perm));
         Mout << "Factor is: " << fac << std::endl;
         sos_lrf *= fac;
         Mout << "Linear response (SOS) for:" << std::endl << "<<";
         Mout << start_ops[0].first << ";";
         Mout << start_ops[1].first;
         Mout << ">>(";
         Mout << start_ops[1].second;
         Mout << ") = " << sos_lrf << std::endl;

         // add to aVec
         RspFunc rsp(I_2);
         rsp.SetSos(true);
         rsp.SetOp(start_ops[0].first,I_0);
         rsp.SetOp(start_ops[1].first,I_1);
         rsp.SetFrq(start_ops[1].second,I_1);
         rsp.SetValue(sos_lrf);
         rsp.SetHasBeenEval(true);
         aVec.push_back(rsp);
      } // end frq loop
   }
}

/**
 *
 **/
void Vscf::QuadraticSosRsp(set<string> aGtox, set<string> aXtox, MidasVector aEig,
   vector<RspFunc>& aVec) {
   Mout << "\nEntering QuadraticSosRsp...\n" << endl;
   vector<string> ops;
   for (set<string>::const_iterator i=aGtox.begin();i!=aGtox.end();i++) {
      if(aXtox.find(*i)!=aXtox.end()) {
         string s_op=*i;
         ops.insert(ops.begin(),3,s_op);
      }
   }
   sort(ops.begin(),ops.end());
   set<vector<string> > unique_rf;
   do {
      vector<string> v;
      v.push_back(ops[0]);
      v.push_back(ops[1]);
      v.push_back(ops[2]);
      unique_rf.insert(v);
   } while(next_permutation(ops.begin(),ops.end()));
   // make a set of all response functions (cannot search a vector)
   set<RspFunc> rsp_funcs;
   for(In i=0;i<mpVscfCalcDef->NrspFuncs();i++)
      rsp_funcs.insert(mpVscfCalcDef->GetRspFunc(i));
   CheckStates(rsp_funcs,aGtox,aXtox);
   for(set<vector<string> >::iterator i_unique=unique_rf.begin();i_unique!=unique_rf.end();i_unique++) {
      // now loop over all permutations of the first four operators
      vector<string> unique_ops=*i_unique;
      vector<Nb> frq;
      frq.push_back(mpVscfCalcDef->GetSpecialQrfFrq(I_0));
      Nb sum_frq=mpVscfCalcDef->GetSpecialQrfFrq(I_0);
      frq.push_back(mpVscfCalcDef->GetSpecialQrfFrq(I_1));
      sum_frq+=mpVscfCalcDef->GetSpecialQrfFrq(I_1);
      frq.push_back(-sum_frq);
      // now loop over all permutations of the first four operators
      vector<pair<string,Nb> > current_ops;
      current_ops.push_back(make_pair(unique_ops[0],frq[2]));
      current_ops.push_back(make_pair(unique_ops[1],frq[0]));
      current_ops.push_back(make_pair(unique_ops[2],frq[1]));
      sort(current_ops.begin(),current_ops.end());
      Nb sos_qrf=C_0;
      In act_perm=I_0;
      In count=0;
      do {
         act_perm++;
         // find all <0|op(0)|i>
         bool level1=true;
         In state_i=-1;
         while(level1) {
            state_i++;
            RspFunc help(-1);
            help.SetOp(current_ops[0].first);
            help.SetLeftState(0);
            help.SetRightState(state_i);
            if(rsp_funcs.find(help)!=rsp_funcs.end()) {
               Nb oxi=(*rsp_funcs.find(help)).Value();
               // find all <i|op[1]|j>
               bool level2=true;
               In state_j=-1;
               while(level2) {
                  state_j++;
                  RspFunc help1(-11);
                  help1.SetOp(current_ops[1].first);
                  if(state_i>state_j) {
                     help1.SetLeftState(state_j);
                     help1.SetRightState(state_i);
                  }
                  else {
                     help1.SetLeftState(state_i);
                     help1.SetRightState(state_j);
                  }
                  if(rsp_funcs.find(help1)!=rsp_funcs.end()) {
                     // create <i|\bar{y}|j>
                     Nb iyj=(*rsp_funcs.find(help1)).Value();
                     if(state_i==state_j) {
                        In op_nr=mpVscfCalcDef->GetRspOpNr(current_ops[1].first);
                        iyj-=mpVscfCalcDef->GetRspOpExpVal(op_nr);
                     }
                     // all <j|op[2]|0>
                     help.SetOp(current_ops[2].first);
                     help.SetLeftState(0);
                     help.SetRightState(state_j);
                     if(rsp_funcs.find(help)!=rsp_funcs.end()) {
                        Nb jzo=(*rsp_funcs.find(help)).Value();
                        Nb w_i=aEig[state_i];
                        Nb w_j=aEig[state_j];
                        // contribution
                        Nb denom=(current_ops[0].second+w_i);
                        denom*=(current_ops[2].second-w_j);
                        sos_qrf-=oxi*iyj*jzo/denom;
                        count++;
                     }
                  }
                  else
                     level2=false;
               }
            }
            else
               level1=false;
         }

      } while(next_permutation(current_ops.begin(),current_ops.end()));
      Mout << "Count is: " << count << " act perm is: " << act_perm << endl;
      Nb fac=6.e0/(Nb(act_perm));
      Mout << "Factor is: " << fac << endl;
      sos_qrf*=fac;
      Mout << "Quadratic response (SOS) for:" << endl << "<<";
      for(In i=0;i<3;i++)
         Mout << current_ops[i].first << ",";
      Mout << ">>(";
      for(In i=0;i<3;i++)
         Mout << current_ops[i].second << ",";
      Mout << ") = " << sos_qrf << endl;
      // add to aVec
      RspFunc rsp(I_3);
      rsp.SetSos(true);
      rsp.SetOp(current_ops[0].first,I_0);
      rsp.SetOp(current_ops[1].first,I_1);
      rsp.SetOp(current_ops[2].first,I_2);
      rsp.SetFrq(current_ops[1].second,I_1);
      rsp.SetFrq(current_ops[2].second,I_2);
      rsp.SetValue(sos_qrf);
      rsp.SetHasBeenEval(true);
      aVec.push_back(rsp);
   } 
}

/**
 *
 **/
void Vscf::CubicSosRsp(set<string> aGtox, set<string> aXtox, MidasVector aEig,
   vector<RspFunc>& aVec) {
   Mout << "\nEntering CubicSosRsp...\n" << endl;
   vector<string> ops;
   for (set<string>::const_iterator i=aGtox.begin();i!=aGtox.end();i++) {
      if(aXtox.find(*i)!=aXtox.end()) {
         string s_op=*i;
         ops.insert(ops.begin(),4,s_op);
      }
   }
   sort(ops.begin(),ops.end());
   set<vector<string> > unique_rf;
   do {
      vector<string> v;
      v.push_back(ops[0]);
      v.push_back(ops[1]);
      v.push_back(ops[2]);
      v.push_back(ops[3]);
      unique_rf.insert(v);
   } while(next_permutation(ops.begin(),ops.end()));
   // make a set of all response functions (cannot search a vector)
   set<RspFunc> rsp_funcs;
   for(In i=0;i<mpVscfCalcDef->NrspFuncs();i++)
      rsp_funcs.insert(mpVscfCalcDef->GetRspFunc(i));
   CheckStates(rsp_funcs,aGtox,aXtox);
   for(set<vector<string> >::iterator i_unique=unique_rf.begin();i_unique!=unique_rf.end();i_unique++) {
      // now loop over all permutations of the first four operators
      vector<string> unique_ops=*i_unique;
      //Nb frq=(mpVscfCalcDef->SosFreqEnd()-mpVscfCalcDef->SosFreqBegin())/(Nb(mpVscfCalcDef->SosFreqSteps()));
      for(In i_frq=0;i_frq<mpVscfCalcDef->GetNCrfFrq();i_frq++) {
         vector<Nb> frq;
         frq.push_back(mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_0));
         Nb sum_frq=mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_0);
         frq.push_back(mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_1));
         sum_frq+=mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_1);
         frq.push_back(mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_2));
         sum_frq+=mpVscfCalcDef->GetSpecialCrfFrq(i_frq,I_2);
         frq.push_back(-sum_frq);
         // now loop over all permutations of the first four operators
         vector<pair<string,Nb> > current_ops;
         current_ops.push_back(make_pair(unique_ops[0],frq[3]));
         current_ops.push_back(make_pair(unique_ops[1],frq[0]));
         current_ops.push_back(make_pair(unique_ops[2],frq[1]));
         current_ops.push_back(make_pair(unique_ops[3],frq[2]));
         sort(current_ops.begin(),current_ops.end());
         Nb sos_crf_sec=C_0;
         Nb sos_crf_first=C_0;
         In act_perm=I_0;
         In count=0;
         do {
            act_perm++;
            // find all <0|op(0)|i>
            bool level1=true;
            In state_i=-1;
            while(level1) {
               state_i++;
               RspFunc help(-1);
               help.SetOp(current_ops[0].first);
               help.SetLeftState(0);
               help.SetRightState(state_i);
               if(rsp_funcs.find(help)!=rsp_funcs.end()) {
                  Nb oxi=(*rsp_funcs.find(help)).Value();
                  // also find <0|op[1]|i>
                  help.SetOp(current_ops[1].first);
                  help.SetLeftState(0);
                  help.SetRightState(state_i);
                  Nb oyi=C_0;
                  if(rsp_funcs.find(help)!=rsp_funcs.end())
                     oyi=(*rsp_funcs.find(help)).Value();
                  else {
                     level1=false;
                     continue;
                  }
                  // find all <i|op[1]|j>
                  bool level2=true;
                  In state_j=-1;
                  while(level2) {
                     state_j++;
                     RspFunc help1(-11);
                     help1.SetOp(current_ops[1].first);
                     if(state_i>state_j) {
                        help1.SetLeftState(state_j);
                        help1.SetRightState(state_i);
                     }
                     else {
                        help1.SetLeftState(state_i);
                        help1.SetRightState(state_j);
                     }
                     if(rsp_funcs.find(help1)!=rsp_funcs.end()) {
                        // create <i|\bar{y}|j>
                        Nb iyj=(*rsp_funcs.find(help1)).Value();
                        if(state_i==state_j) {
                           In op_nr=mpVscfCalcDef->GetRspOpNr(current_ops[1].first);
                           iyj-=mpVscfCalcDef->GetRspOpExpVal(op_nr);
                        }
                        // also find <0|op[2]|j>
                        help.SetOp(current_ops[2].first);
                        help.SetLeftState(0);
                        help.SetRightState(state_j);
                        Nb ozj=C_0;
                        if(rsp_funcs.find(help)!=rsp_funcs.end())
                           ozj=(*rsp_funcs.find(help)).Value();
                        else {
                           level2=false;
                           continue;
                        }
                        // also find <0|op[3]|j>
                        help.SetOp(current_ops[3].first);
                        help.SetLeftState(0);
                        help.SetRightState(state_j);
                        Nb ouj=C_0;
                        if(rsp_funcs.find(help)!=rsp_funcs.end())
                           ouj=(*rsp_funcs.find(help)).Value();
                        else {
                           level2=false;
                           continue;
                        }
                        // all <j|op[2]|k>
                        bool level3=true;
                        In state_k=-1;
                        while(level3) {
                           state_k++;
                           help1.SetOp(current_ops[2].first);
                           if(state_j>state_k) {
                              help1.SetLeftState(state_k);
                              help1.SetRightState(state_j);
                           }
                           else {
                              help1.SetLeftState(state_j);
                              help1.SetRightState(state_k);
                           }
                           if(rsp_funcs.find(help1)!=rsp_funcs.end()) {
                              // create <j|\bar{z}|k>
                              Nb jzk=(*rsp_funcs.find(help1)).Value();
                              if(state_j==state_k) {
                                 In op_nr=mpVscfCalcDef->GetRspOpNr(current_ops[2].first);
                                 jzk-=mpVscfCalcDef->GetRspOpExpVal(op_nr);
                              }
                              // all <k|op[3]|0>
                              help.SetOp(current_ops[3].first);
                              help.SetLeftState(0);
                              help.SetRightState(state_k);
                              if(rsp_funcs.find(help)!=rsp_funcs.end()) {
                                 Nb kuo=(*rsp_funcs.find(help)).Value();
                                 Nb w_i=aEig[state_i];
                                 Nb w_j=aEig[state_j];
                                 Nb w_k=aEig[state_k];
                                 // contribution
                                 Nb denom=(current_ops[0].second+w_i);
                                 denom*=(current_ops[2].second+current_ops[3].second-w_j);
                                 denom*=(current_ops[3].second-w_k);
                                 sos_crf_sec-=oxi*iyj*jzk*kuo/denom;
                                 denom=(current_ops[0].second+w_i);
                                 denom*=(current_ops[1].second-w_i);
                                 denom*=(current_ops[3].second-w_j);
                                 // Divide by aEig.Size() to avoid overcounting
                                 sos_crf_first+=oxi*oyi*ozj*ouj/(Nb(aEig.Size())*denom);
                              }
                              else {
                                 Mout << "Did not find <k|u|o> for state: " << state_k << endl;
                                 level3=false;
                              }
                              count++;
                           }
                           else
                              level3=false;
                        }
                     }
                     else
                        level2=false;
                  }
               }
               else
                  level1=false;
            }
            // add the second part
         } while(next_permutation(current_ops.begin(),current_ops.end()));
         Mout << "Count is: " << count << " act perm is: " << act_perm << endl;
         Nb sos_crf=sos_crf_sec+sos_crf_first;
         Nb fac=24.e0/(Nb(act_perm));
         Mout << "Factor is: " << fac << endl;
         sos_crf*=fac;
         Mout << "Cubic response (SOS) for:" << endl << "<<";
         for(In i=0;i<4;i++)
            Mout << current_ops[i].first << ",";
         Mout << ">>(";
         for(In i=0;i<4;i++)
            Mout << current_ops[i].second << ",";
         Mout << ") = " << sos_crf << endl;
         Mout << "With first order part  = " << sos_crf_first*fac << endl;
         Mout << "With second order part = " << sos_crf_sec*fac << endl;
         // add to aVec
         RspFunc rsp(I_4);
         rsp.SetSos(true);
         rsp.SetOp(current_ops[0].first,I_0);
         rsp.SetOp(current_ops[1].first,I_1);
         rsp.SetOp(current_ops[2].first,I_2);
         rsp.SetOp(current_ops[3].first,I_3);
         rsp.SetFrq(current_ops[1].second,I_1);
         rsp.SetFrq(current_ops[2].second,I_2);
         rsp.SetFrq(current_ops[3].second,I_3);
         rsp.SetValue(sos_crf);
         rsp.SetHasBeenEval(true);
         aVec.push_back(rsp);
      } // end of frq loop
   } while(next_permutation(ops.begin(),ops.end()));
}

/**
 *
 **/
void Vscf::CheckStates
   (  std::set<RspFunc>& aS
   ,  const std::set<std::string>& aGtox
   ,  const std::set<std::string>& aXtox
   )
{
   std::map<std::string,In> state_count_gtox;
   std::map<std::string,In> state_count_xtox;
   In min_state = I_0;
   for(auto& it : aGtox)
   {
      // find maximum left state
      //bool found_max=false;
      In left_state = I_0;
      while (  true
            )
      {
         RspFunc help(-1);
         help.SetOp(it);
         help.SetLeftState(left_state);
         if (  aS.find(help) == aS.end()
            )
         {
            break;
         }
         ++left_state;
      }

      if (  left_state > min_state
         )
      {
         min_state = left_state;
      }

      state_count_gtox.insert(std::make_pair(it, left_state));
      Mout << "For operator: " << it << " max state is: " << left_state << std::endl;
   }

   for(auto& it : aXtox)
   {
      // enough to find maximum right state, but must
      // check that all lower left states exist
      //bool found_max=false;
      In left_state = I_0;
      In right_state = I_0;
      while (  true
            )
      {
         RspFunc help(-11);
         help.SetOp(it);
         help.SetLeftState(left_state);
         help.SetRightState(right_state);
         if (  aS.find(help) == aS.end()
            )
         {
            break;
         }
         ++right_state;
      }
      // now check lower left states
      for(In i=I_0; i<right_state; ++i)
      {
         RspFunc help(-11);
         help.SetOp(it);
         help.SetLeftState(i);
         help.SetRightState(right_state);
         if (  aS.find(help) == aS.end()
            )
         {
            left_state = i;
            break;
         }
      }
      right_state = left_state+1;
      if (  right_state < min_state
         )
      {
         min_state = right_state;
      }

      state_count_xtox.insert(std::make_pair(it, right_state));
      Mout << "For operator: " << it << " max state is: " << right_state << std::endl;
   }
   // we know the minimum state, now treat RspFunc set
   for(auto& it : aGtox)
   {
      RspFunc help(-1);
      help.SetOp(it);
      In start = min_state+1;
      help.SetLeftState(start);
      while (  aS.find(help) != aS.end()
            )
      {
         aS.erase(aS.find(help));
         ++start;
         help.SetLeftState(start);
      }
   }
   for(auto& it : aXtox)
   {
      RspFunc help(-11);
      help.SetOp(it);
      In start=min_state+1;
      help.SetRightState(0);
      help.SetLeftState(start);
      while (  aS.find(help) != aS.end()
            )
      {
         aS.erase(aS.find(help));
         ++start;
         help.SetLeftState(start);
      }
      start = min_state+1;
      help.SetLeftState(0);
      help.SetRightState(start);
      while (  aS.find(help) != aS.end()
            )
      {
         aS.erase(aS.find(help));
         ++start;
         help.SetRightState(start);
      }
   }
}
