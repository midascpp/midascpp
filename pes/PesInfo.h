/**
************************************************************************
* 
* @file                PesInfo.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PesInfo datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


#ifndef PESINFO_H
#define PESINFO_H

// std headers
#include <string>
#include <vector>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/SymInfo.h"
#include "pes/molecule/PointGroupSymmetry.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/CalculationList.h"
#include "pes/MultiLevelInfo.h"
#include "pes/ScalingInfo.h"

#include "pes/PesMpi.h"
#include "mpi/Impi.h"

// forward declarations
template<class T> class MidasFunctionWrapper;
class GridType;
class CalcCode;

namespace FileHandlerStreams
{
   class FileHandlerWithStreams;
}

/**
 * Pes info class.
 **/
class PesInfo 
{
   private:

      //!
      const molecule::MoleculeInfo&       mMolecule;

      //! Holds information on current multi level.
      pes::MultiLevelInfo                 mMultiLevelInfo;

      //! Scaling info.
      pes::ScalingInfo                    mScalingInfo;

      //! Symmetry info.
      midas::molecule::SymInfo            mSymInfo;
      
      //! Main directory for this subsystem multilevel calculation
      std::string                         mSubSystemDir = "";

      //! Subsystem name (e.g. subsystem_0)
      const std::string mSubSystemName;

      //! Total number of properties/surfaces for this multilevel calculation 
      In                                  mNoSurfaceProps;
      
      //! Keeps track of the current MC level.
      In                                  mMCLevel;

      //! The current iteration.
      In                                  mIterNo; 
      
      // information on how to do singlepoints for given surface (SHOULD LIVE IN MULTILEVEL INFO)
      std::string                         mSinglePointName = "DEFAULT"; 
      
      // MIGHT GO AWAY
      bool                                mUseDerivatives;
      std::vector<std::string>            mPropWithDer;
      In                                  mExtrapolateFrom = -1;

      // WILL GO AWAY!!!
      CalculationList mExtrapCalcList;

      //! Do deal with ML guided PES constructions
      bool mDoMLPes = false;

      //!@{
      /**
       * Delete copy constructor and copy assignment.
       * We do not want these to be called, if they were strange errors would occur in the program
       **/
      PesInfo(const PesInfo&)            = delete; 
      PesInfo& operator=(const PesInfo&) = delete;
      //!@}

   public:

      //! Default constructor 
      PesInfo(const PesCalcDef& aPesCalcDef, const molecule::MoleculeInfo& aMolecule, const std::string aSubSystemDir, const std::string aSubSystemName);
      
      //! Enable move constructor
      PesInfo(PesInfo&&) = default;
      
      //! Get MultiLevelInfo.
      const pes::MultiLevelInfo& GetMultiLevelInfo() const { return mMultiLevelInfo; }
      
      //! Get ScalingInfo.
      const pes::ScalingInfo& GetScalingInfo() const { return mScalingInfo; }
      pes::ScalingInfo& GetScalingInfo() { return mScalingInfo; }
      
      //! Get SymInfo.
      const molecule::SymInfo& GetSymInfo() const { return mSymInfo; }

      //! Update level information for multilevel scheme
      void Update(const PesCalcDef& aPesCalcDef, In aLevel = -1, const std::string& aSubDir = "");
     
      //@{
      //! PesInfo class setters
      void SetmNoSurfaceProps(const In& aIn)    {mNoSurfaceProps = aIn;}
      void SetIterNo(const In& aIn)             {mIterNo = aIn;}
      void SetMCLevel(const In& aIn)            {mMCLevel = aIn;}
      //@}

      //@{
      //! PesInfo class getters
      const In& GetmNoSurfaceProps() const      {return mNoSurfaceProps;}
      const In& GetIterNo() const               {return mIterNo;}
      const In& GetMCLevel() const              {return mMCLevel;}
      //@}

      //! Get name of subsystem working directory
      const std::string& GetmSubSystemDir() const {return mSubSystemDir;}

      //! Get subsystem name
      const std::string& GetmSubSystemName() const {return mSubSystemName;}
      
      //! Get single point name (should this be in MultiLevelInfo? )
      const std::string& GetSinglePointName() const { return mSinglePointName; } 
      
      //{@
      //! Symmetry stuff  (MOST WILL PROBABLY BE REMOVED !)
      void SetPointGroup(const std::string& aGroup) 
      { 
         if (aGroup != "C1")
         {
            mSymInfo = midas::molecule::SymmetryAnalysis(const_cast<molecule::MoleculeInfo&>(mMolecule), gPesCalcDef.PointGroupSymThrs());
            if(mSymInfo.GetPointGroup() != aGroup)
            {
               std::stringstream ss_err;
               ss_err   << "I have identified a different point group ("
                        << mSymInfo.GetPointGroup()
                        << ") than given in input ("
                        << aGroup
                        << ").";
               MIDASERROR(ss_err.str());
            }
         }
      }
      In Nclass() const { return mSymInfo.Nclass(); }
      In GetIrrepNr(string aLab) const { return mSymInfo.GetIrrepNr(aLab); } 
      Nb GetCharacter(In aIrep, In aIclass) const {return mSymInfo.GetCharacter(aIrep, aIclass); }
      const MidasMatrix& GetRotation(In aOp) const { return mSymInfo.GetRotation(aOp); }
      const MidasMatrix& GetInverseRotation(In aOp) const { return mSymInfo.GetInverseRotation(aOp); }
      const std::vector<In>& GetRelation(In aOp) const { return mSymInfo.GetRelation(aOp); }
      const std::vector<In>& GetInverseRelation(In aOp) const { return mSymInfo.GetInverseRelation(aOp); }
      In NopSym() const {return mSymInfo.NopSym();}
      std::string GetIrLab(In aIrep) const { return mSymInfo.GetIrLab(aIrep); }
      //@}
      
      //!@{ 
      //! Molecule interface
      const molecule::MoleculeInfo& GetMolecule() const        {return mMolecule;}
      //!@}
      
      const In& GetExtrapFrom() const                          {return mExtrapolateFrom;}
      const bool& GetmUseDerivatives() const                   {return mUseDerivatives;}
      const std::vector<std::string>& GetmPropWithDer() const  {return mPropWithDer;}

      //Functions for setting variables
      void SetExtrapFrom(const In& aMcr)                       {mExtrapolateFrom = aMcr;}
      void SetmUseDerivatives(const bool& aBool)               {mUseDerivatives = aBool;}
      void SetmPropWithDer(const std::string& aStr)            {mPropWithDer.push_back(aStr);}
      
      //! Clean the mPropWithDer vector
      void CleanmPropWithDer()                                 {mPropWithDer.clear();}

      //!@{
      //! Some derivative/extrap calclist stuff.... ALL should GO AWAY! 
      //DerCalcListInterface
      void CleanUpDerCalcList() { mExtrapCalcList.Clean(); }  // CAN BE REMOVED IF EXTRAPCALCLIST IS NOT USED
      In GetNrOfDerCalcs() {return mExtrapCalcList.GetNrOfNewCalcs();} // MOST REFERENCES CAN JUST BE REMOVED...
      //bool IsOnListDer(const std::string& aSearchString) {return mExtrapCalcList.IsOnList(aSearchString);} // only used to construct EXTRAPCALCLIST, CAN BE REMOVED!
      bool AddDerCalc(const CalcCode& aAddEntry) {return mExtrapCalcList.AddEntry(aAddEntry);} // CAN BE REMOVED
      std::string GetCalcCodeFromCalcNumber(In aCalcNumber) { return mExtrapCalcList.GetCalcCodeFromCalcNumber(aCalcNumber); }
      //!@}

      //! Getter and Setter for ML related options
      const bool& GetMLPes() const     {return mDoMLPes;}
      void SetMLPes(const bool& aBool) {mDoMLPes = aBool;}

      //! Operator output overload
      friend std::ostream& operator<<(ostream& aOs, const PesInfo& aPesInfo);
};

#endif/*PESINFO_H*/
