/**
************************************************************************
* 
* @file                AdgaSurface.h
*
* Created:             10-08-2005
*
* Author:              Jacob Kongsted & Ove Christiansen 
*
* Short Description:   Store NumDer declarations. 
* 
* Last modified: Thu Jul 15, 2010  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ADGASURFACE_H_INCLUDED
#define ADGASURFACE_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "potentials/MidasPotential.h"
#include "pes/Surface.h"
#include "pes/GridType.h"
#include "pes/doubleincremental/DincrInfo.h"

// using declarations
using std::vector;
using std::string;

class GridType;
class ModeCombiOpRange;
class Molecule;
class PesInfo;
class ItGridHandler;

namespace midas
{
namespace pes
{

/**
 * Class to drive the construction of ADGA PES
 **/
class AdgaSurface 
   : public Surface
{
   private:
      
      //! The potential we are creating (used for evaluation of VSCF)
      std::vector<MidasPotential> mProperties;

      //! VscfCalcDef for doing Vscf calculation in an Adga context
      std::vector<VscfCalcDef> mAdgaVscfCalcDef;

      //! Double incremental info
      DincrInfo mDincrInfo;

      //! Special care needs to be taken if the primitive basis is of type distributed Gaussian
      bool mUseGaussianBasis;
         
      //! Initialize the Adga 
      void InitLevel(const In&, const In&, std::vector<Derivatives>&, std::vector<ItGridHandler>&);
      
      //!
      void InitializeIterGrid(const In& aNmodes, const ModeCombiOpRange& arModeCombiOpRange, const GridType& arGridType, const In& arCurrentSubsystem, ItGridHandler& arItGridHandler) const;
      
      //!
      void UpdateGridType(const In, const ModeCombiOpRange&, const ItGridHandler&, GridType&) const;

      //! Run 1-mode surface
      void Run1ModeSurface
         (  const In& arMultilevel
         ,  std::vector<GridType>& arGridFractions
         ,  const In& arMaxMcl
         ,  std::vector<ModeCombiOpRange>& arSubsystemMcr
         ,  std::vector<ItGridHandler>& arSubsystemIterGrids
         ,  std::vector<std::vector<bool> >& arSkipMcInFit
         ,  std::vector<Derivatives>& arDerivatives
         );
      
      //! Do interpolation and fitting for 1-mode grids
      void Get1ModeAnalyticalSurfaces
         (  GridType& arGridFractions
         ,  const In& arMaxMcl
         ,  const ModeCombiOpRange& arMcr
         ,  ItGridHandler& arItGridHandler
         ,  const std::vector<bool>& arSkipMcInFit
         ,  Derivatives& arDerivatives
         ,  const In& arCurrentMcl
         ,  const In& arCurrentSubsystem
         );
      
      //! Reads the potential from last iteration
      void RereadProperties(const In& arCurrentSubsystem);
      
      //! Save MeanDens_Q<mode>_ or MaxDens_Q<mode> files
      void SaveMeanDensFiles(const In& arCurrentMcl, const In& arCurrentAdgaIter, const In& arCurrentSubsystem) const;

      //! Run N-mode surface
      void RunNModeSurfaces
         (  const In& arMultilevel
         ,  std::vector<GridType>& arGridFractions
         ,  const In& arMaxMcl
         ,  std::vector<ModeCombiOpRange>& arSubsystemMcr
         ,  std::vector<ItGridHandler>& arSubsystemIterGrids
         ,  std::vector<std::vector<bool> >& arSkipMcInFit
         ,  std::vector<Derivatives>& arDerivatives
         );
      
      //!
      void SetExtrap(const In&, const ModeCombiOpRange&, const bool&) const;
      
      //! 
      void ResetIterGrid(const In&, const ModeCombiOpRange&, ItGridHandler& arItGridHandler) const;
      
      //! Calculate integrals.
      void CalcInt(const In&, const ModeCombiOpRange&, const bool&, const In&, ItGridHandler&);
      
      //! Check convergence
      bool CheckConv(const In& aNmodes,  const In& arSubsystem, const ModeCombiOpRange& arModeCombiOpRange, ItGridHandler& arItGridHandler) const;
      
      //! Get the coordinates/position of all new grid points for calculation 
      void SetNewGridPoints(const In&, const ModeCombiOpRange&, const bool&, const In&, ItGridHandler&, std::vector<bool>&) const;

      //! Something with subpartitions of modecombis.
      void Deconstruct(const ModeCombi&, std::vector<std::vector<In> >&,  std::vector<In>&, const ModeCombiOpRange&) const;

      //! Find all subsets for a given mode combination 
      void FindSubsetsOfMc(const ModeCombi& arModeCombi, const ModeCombiOpRange& arModeCombiOpRange, std::vector<std::vector<In> >& arMcSubsets, std::vector<In>& arMcSubsetsPosInMcr) const;

      //! Determine if a mode combination contributes meaningful to the complete surface, i.e. whether or not it is a very shallow inter-connecting mode
      void CheckSubsystemMcConv(const In& arCurrMcl, const In& arSubsystem, const ModeCombiOpRange& arMcr, const ItGridHandler& arItGridHandler);
 
      //! Checks that the same mode in different fragments have the same potential boundaries
      void CheckSubsystemBoundaries(const In& arCurrMcl, const std::vector<ModeCombiOpRange>& arSubsystemMcr, std::vector<ItGridHandler>& arSubsystemIterGrids, std::vector<std::vector<bool> >& arSkipMcInFit);
      
      //! Print info. on ADGA iter., level, coupling, singlepoints per mode.
      void PrintAdgaIterInfo(std::ostream& arOs, const In& aLevel, const In& aIter, const In& aCoup, const ModeCombiOpRange& arMcr, const ItGridHandler& arItGridHandler) const;
      
      //! Save the potential
      void SavePotentials(const ModeCombiOpRange&, const bool& arPlotForAllMcs, const In&, const ItGridHandler&) const;
      
      //! Dump grid bounds
      void DumpOneModeGridBounds(const In&, const ModeCombiOpRange&, const In&, ItGridHandler&) const;
      
      //! Find point with largest potential energy value
      std::vector<Nb> GetMaxEnergyCoords(const std::vector<Nb>&, const std::vector<Nb>&, const std::vector<Nb>&, const std::vector<In>&, const In&, const In&) const;
      
      //!
      std::vector<std::vector<Nb>> GetOneModeGridBoundsMap(const In& aNmodes, const ModeCombiOpRange& arModeCombiOpRange, const ItGridHandler& arItGridHandler) const;
 
      //! Merge surface(s) together if using double incremental expansion
      void MergeFragmentSurface() const;

      //! Wrapper for doing VSCF
      void DoVscf();
      
      //! Initialize Vscf for Adga
      void InitializeAdgaVscf();

      //! Bool for letting the Adga know that the maximum potential value is required by basis
      bool mAdgaSetMaxPot;

   public:

      //! Constructor
      AdgaSurface(std::vector<PesInfo>& aPesInfos, const std::string& aPesMainDir);

      //! Destructor
      ~AdgaSurface() = default;

      //! Run Adga surface 
      void RunSurface();

};

} /* namespace pes */
} /* namespace midas */

#endif /* ADGASURFACE_H_INCLUDED */
