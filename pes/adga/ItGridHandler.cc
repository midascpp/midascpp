/**
************************************************************************
*
* @file                ItGridHandler.cc
*
* Created:             24-10-2006
*
* Author:              D. Toffoli (toffoli@chem.au.dk) and M. Sparta (msparta@chem.au.dk)
*
* Short Description:   stores informations about the dynamic grids
*                      for the modecombination
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/


#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <exception>

#include "pes/adga/ItGridHandler.h"
#include "pes/adga/ItGrid.h"

#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/FileSystem.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "potentials/MidasPotential.h"
#include "pes/PesFuncs.h"
#include "pes/GridType.h"

/***************************************************************************//**
 * Simply returns a reference to an element of ItGridHandler::mVecOfGrids.
 * Introduced because this is done quite often, but using some address passed
 * as argument to one of these member functions. That address usually comes
 * from some ModeCombi, but _anything_ can have happened to that address,
 * basically. So high risk of out-of-range errors.  Hence the need for
 * something a little more stable, unified approach -- at least easier to debug
 * than a pure segfault.
 *
 * If accessed out of range, a MIDASERROR is thrown.
 *
 * @note
 *    The const/non-const versions could probably be squashed together using
 *    templates or something. Feel free to make it smarter if you feel like it.
 *
 * @param[in] i
 *    The index of the element to (try to) access.
 * @return
 *    Reference to ItGrid element of ItGridHandler::mVecOfGrids.
 ******************************************************************************/
ItGrid& ItGridHandler::VecOfGridsAt
   (  std::size_t i
   )
{
   try
   {
      return mVecOfGrids.at(i);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR(oor.what());
      return mVecOfGrids.front(); // To quelch the no-return-value warning.
   }
}

const ItGrid& ItGridHandler::VecOfGridsAt
   (  std::size_t i
   )  const
{
   try
   {
      return mVecOfGrids.at(i);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR(oor.what());
      return mVecOfGrids.front(); // To quelch the no-return-value warning.
   }
}

/**
* Default constructor
* */
ItGridHandler::ItGridHandler
   (  In aNmodes
   ,  std::vector<In> aProperties
   ,  Uin aPropNo
   )
   :  mDensities(aNmodes, aProperties)
   ,  mNumModes(aNmodes)
   ,  mCurr1dIter(I_0)
   ,  mCurrNdIter(I_0)
   ,  mConv1dGrid(false)
   ,  mConvNdGrid(false)
{
} 

/**
 * Load mean-density files from disc into memory stored in ItGridDensity structure.
 * These densities are later used for evalulating integrals in each box.
 **/
void ItGridHandler::LoadMeanDensFiles
   (  const std::string& aAnalysisDir
   ,  const std::vector<std::string>& aModeLabels
   )
{
   mDensities.LoadFiles(aAnalysisDir, aModeLabels);
}

/**
 * Reinitialize the iterative grid for new multilevel
 *
 * @param aMultiLevel the current level (passed for historic reasons)
 **/
void ItGridHandler::ReInitialize
   (  const In aMultiLevel
   )
{
   if (gDebug || gPesIoLevel > I_9)
   {
      Mout << " Reinitialize the Adga procedure" << std::endl;
   }

   SetCurr1dIter(I_0);
   SetCurrNdIter(I_0);

   for (In igr = I_0; igr < mVecOfGrids.size(); ++igr)
   {
      mVecOfGrids[igr].ReIni(aMultiLevel);
      if (gPesIoLevel > I_14)
      {
         Mout << mVecOfGrids[igr] << std::endl;
      }   
   }
}

/**
 * Return the potential grid boundaries.
 *
 * @param aAdd address of modecombi.
 * @param arIvec contains left and right potential boundaries (fraction logic) on return.
 **/
void ItGridHandler::GetCurrBounds
   (  const In aAdd
   ,  std::vector<In>& arIvec
   )  const
{
   const auto& grid_box = VecOfGridsAt(aAdd - I_1);
   for (In i = I_0; i < grid_box.GetNmodes(); ++i)
   {
      arIvec.push_back(grid_box.GetLbound(i));
   }
   for (In i = I_0; i < grid_box.GetNmodes(); ++i)
   {
      arIvec.push_back(grid_box.GetRbound(i));
   }
   return;
}

/**
 * Return the potential grid boundaries.
 *
 * @param aAdd address of modecombi.
 * @param arNbVec contains left and right potential boundaries (Q units) on return.
 **/
void ItGridHandler::GetCurrQbounds
   (  const In& aAdd
   ,  std::vector<Nb>& arNbVec
   )  const
{
   const auto& grid_box = VecOfGridsAt(aAdd - I_1);

   for (In i = I_0; i < grid_box.GetNmodes(); ++i)
   {
      // Obtain the left potential boundary
      arNbVec.push_back(grid_box.GetQleft(i));
      
      // Obtain the right potential boundary
      arNbVec.push_back(grid_box.GetQright(i));
   }

   return;
}

/**
 * Insert a mode combi grid at the end of the vector.
 * 
 * @param arModeCombi     
 * @param arGridType      
 * @param arScalFacts
 * @param aNoProps
 *    Number of properties we are working on.
 **/
void ItGridHandler::InsertModeCombiGrid
   (  const ModeCombi& arModeCombi
   ,  const GridType& arGridType
   ,  const std::pair<MidasVector, MidasVector>& arScalFacts
   ,  const In& aNoProps
   )
{
   In add = arModeCombi.Address();
   if (gPesIoLevel > I_11)
   {
     Mout << " Grid initialization for mode combi " << arModeCombi << " add: " << add << std::endl;
   }

   if (mVecOfGrids.size() != (add - I_1)) 
   {
      MidasWarning("Warning: Addresses are not consistent in InsertModeCombiGrid ");
   }

   std::vector<In> curr_mode = arModeCombi.MCVec();
   mVecOfGrids.emplace_back(curr_mode, mDensities, aNoProps);
   
   if (arModeCombi.Size() == I_1)
   {
      // Get the total number of available grid point positions for this mode combination, note that this is dependent of the maximum number of allowed Adga iterations
      std::vector<In> g_pts(arModeCombi.Size());
      g_pts = arGridType.GridPoints(add);
      
      // The total number of available grid point positions for the left and right side of the surface is the same, (the number of actual grid points can still vary)
      In n_pts = g_pts[I_0] / I_2;
      
      std::vector<string> fr_vec = arGridType.Fractions(add);
      if (fr_vec.size() != I_2) 
      {
         MIDASERROR("size different from 2 in the fractioning");
      }
      
      //
      std::string fr_left = fr_vec[I_0];
      std::string fr_right = fr_vec[I_1];
      std::string s_n_pts = std::to_string(n_pts);
      
      // Get the left potential boundary fraction
      In l_bnd = -I_1 * midas::util::FromString<In>(PesFuncs::StringNominator(PesFuncs::StringMultiplication(fr_left, s_n_pts)));
      In l_bnd_denom = midas::util::FromString<In>(PesFuncs::StringDenominator(PesFuncs::StringMultiplication(fr_left, s_n_pts)));

      // Get the right potential boundary fraction
      In r_bnd = midas::util::FromString<In>(PesFuncs::StringNominator(PesFuncs::StringMultiplication(fr_right, s_n_pts)));
      In r_bnd_denom = midas::util::FromString<In>(PesFuncs::StringDenominator(PesFuncs::StringMultiplication(fr_right, s_n_pts)));

      // Check that fraction denominators make sense
      if (r_bnd_denom != I_1 || l_bnd_denom != I_1) 
      {
         MIDASERROR("DENOMINATOR IS NOT 1");
      }

      // Set the scaling for iterative grids, which is used to go between the fraction logic and Q-units throughout the code 
      Nb l_q_scal_fact = C_0;
      Nb r_q_scal_fact = C_0;
      if (  gPesCalcDef.GetmNormalcoordInPes()
         || gPesCalcDef.GetmFalconInPes()
         )
      {
         l_q_scal_fact = (arScalFacts.first[curr_mode[I_0]] * std::sqrt(C_FAMU)) / Nb(n_pts);
         r_q_scal_fact = (arScalFacts.second[curr_mode[I_0]] * std::sqrt(C_FAMU)) / Nb(n_pts);
      }
      else if (gPesCalcDef.GetmPscInPes())
      {
         l_q_scal_fact = arScalFacts.first[curr_mode[I_0]] / Nb(n_pts);
         r_q_scal_fact = arScalFacts.second[curr_mode[I_0]] / Nb(n_pts);
      }
      
      mVecOfGrids.back().SetBounds(I_0, l_bnd, r_bnd, arModeCombi.Size());
      mVecOfGrids.back().SetmQScales(I_0, l_q_scal_fact, r_q_scal_fact);
      
      // Output mode grid
      if (gPesIoLevel > I_9)
      {
         Mout << "  Mode grid for MC: " << curr_mode << std::endl << mVecOfGrids.back() << std::endl;
      }
   }
}

/**
 * Update the grid for a mode combination
 *
 * @param aAdd address of mode combi
**/
void ItGridHandler::UpdateGridForMc
   (  const In aAdd
   )
{
   auto& grid_box = VecOfGridsAt(aAdd - I_1);
   
   // Update grid
   grid_box.UpdateGrid(); 
   return;
}


/**
 * Save the potential value on mode specific grids for later processing.
 * The q1 vector contain all the x-values for the *.mplot_it* files
 * The q_pt vector ends up containing all the x-values for the *_it*.mplot files, note that some points are shared between Potential and Point files
 *
 * @param aAdd
 * @param arModeCombi
 * @param aProperties
 * @param aPropNo
 * @param aMcl
 * @param aBasisBounds
 * @param aAnalysisDir
 * @param arMcLabels
 * @param aExtension       Optional argument to give an extension for the filename
 **/
void ItGridHandler::SavePotential
   (  const In& aAdd
   ,  const std::vector<In>& arModeCombi
   ,  const std::vector<MidasPotential>& aProperties
   ,  const Uin& aPropNo
   ,  const In& aMcl
   ,  const std::vector<std::pair<Nb, Nb> >& aBasisBounds
   ,  const std::string& aAnalysisDir
   ,  const std::vector<std::string>& arMcLabels
   ,  const std::string& aExtension
   )  const 
{
   In curr_iter = -I_1; 
   In n_modes = arModeCombi.size();
   if (n_modes == I_1)
   {
      if (  Is1dConv() &&
            (  gPesCalcDef.GetmCalcDensAtEachMcl()
            || gPesCalcDef.GetmDoAdgaExtAtEachMcl()
            )
         )
      {
         curr_iter = GetCurrNdIter();
      }
      else
      {
         curr_iter = GetCurr1dIter();
      }
   }
   else if (n_modes == I_2)
   {
      curr_iter = GetCurrNdIter();
   }
   else
   {
      MIDASERROR(" Adga procedure not implemented yet ");
   }
     
   // Contains the fitted potential of the current iteration
   std::string curr_potential_file = aAnalysisDir + "/" + "Potential_Prop" + std::to_string(aPropNo) + "_" + aExtension; 
   // contains the fitted potential of a specific iteration
   std::string iter_potential_file = aAnalysisDir + "/" + "Potential_Prop" + std::to_string(aPropNo) + "_" + aExtension; 
   // Contains the fitted points of the current iteration
   std::string curr_point_file = aAnalysisDir + "/" + "Point_Prop" + std::to_string(aPropNo) + "_" + aExtension;
   // Contains the fitted points of the current iteration
   std::string iter_point_file = aAnalysisDir + "/" + "Point_Prop" + std::to_string(aPropNo) + "_" + aExtension; 

   // Loop over modes in the mode combination and add these to file name
   for (In i_mode = I_0; i_mode < n_modes; i_mode++)
   {
      curr_potential_file += arMcLabels[i_mode];
      iter_potential_file += arMcLabels[i_mode];
      curr_point_file += arMcLabels[i_mode];
      iter_point_file += arMcLabels[i_mode];

      //
      if (n_modes > I_1 && i_mode < n_modes - I_1) 
      {
         curr_potential_file += "_";
         iter_potential_file += "_";
         curr_point_file += "_";
         iter_point_file += "_";
      }
   }
   curr_potential_file += ".mplot";
   curr_point_file += ".mplot";
  
   // Append the mode combination level and iteration number to the file name
   iter_potential_file += "_" + std::to_string(aMcl) + "m_it" + std::to_string(curr_iter) + ".mplot";
   iter_point_file += "_" + std::to_string(aMcl) + "m_it" + std::to_string(curr_iter) + ".mplot";

   // Output
   if (gPesIoLevel > I_10)
   {
      Mout << " The potential values for mode " << arMcLabels << "are dumped on file: " << std::endl << "  " << iter_potential_file << std::endl;
   }

   ofstream datafile;
   ofstream pointfile;
   ofstream new_datafile;
   datafile.open(curr_potential_file.c_str(), ios_base::out);
   pointfile.open(curr_point_file.c_str(), ios_base::out);
   datafile.setf(ios::scientific);
   datafile.setf(ios::uppercase);
   datafile.precision(22);
   pointfile.setf(ios::scientific);
   pointfile.setf(ios::uppercase);
   pointfile.precision(22);

   if (arModeCombi.size() == I_1)
   {
      // The grid of fitted points
      const auto& grid_box = VecOfGridsAt(aAdd - I_1);

      // If we have information on the basis set boundaries, then we plot the potential all the way out to these points, otherwise we plot to the potential boundaries
      Nb left_bound = C_0;
      Nb right_bound = C_0;
      if (aBasisBounds[I_0].first != C_0 && aBasisBounds[I_0].second != C_0)
      {
         left_bound = aBasisBounds[I_0].first;
         right_bound = aBasisBounds[I_0].second;
      }
      else
      {
         left_bound = grid_box.GetQleft(I_0);
         right_bound = grid_box.GetQright(I_0);
      }

      std::vector<Nb> q1;
      MidasVector q_pt(I_1, C_0);
      In n_pts = I_150;

      for (In i_q = I_0; i_q <= n_pts; i_q++)
      {
         q1.push_back(left_bound * (Nb(Nb(n_pts - i_q)/n_pts)));
      }
      for (In i_q = I_1; i_q <= n_pts; i_q++)
      {
         q1.push_back(right_bound * (Nb(Nb(i_q)/n_pts)));
      }

      for (In p = I_0; p < q1.size(); p++)
      {
         q_pt[I_0] = q1[p];
         datafile << q1[p] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pt) << std::endl;

         if (gPesIoLevel > I_13)
         {
            Mout << " Loading into the 1M datafile (Potential in analysis): " << q1[p] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pt) << std::endl;
         }
      }
     
      for (In i = I_0; i < grid_box.GetNbounds(); i++)
      {
         q_pt[I_0] = grid_box.GetQBoxBound(I_0, i);

         pointfile << q_pt[I_0] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pt) << std::endl;

         if (gPesIoLevel > I_13)
         {
            Mout << " Loading into the 1M pointfile (Point in analysis): " << q_pt[I_0] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pt) << std::endl;
         }
      }
      
      datafile.close();
      pointfile.close();

      // Copy for later analysis
      midas::filesystem::Copy(curr_potential_file, iter_potential_file);
      midas::filesystem::Copy(curr_point_file, iter_point_file);

      return;
   }
   else
   {
      // The grid of fitted points
      const auto& grid_box = VecOfGridsAt(aAdd - I_1);

      // If we have information on the basis set boundaries, then we plot the potential all the way out to these points, otherwise we plot to the potential boundaries
      Nb left_bound_1 = C_0;
      Nb right_bound_1 = C_0;
      Nb left_bound_2 = C_0;
      Nb right_bound_2 = C_0;
      if (  aBasisBounds[I_0].first != C_0 && aBasisBounds[I_0].second != C_0
         && aBasisBounds[I_1].first != C_0 && aBasisBounds[I_1].second != C_0
         )
      {
         left_bound_1 = aBasisBounds[I_0].first;
         right_bound_1 = aBasisBounds[I_0].second;
         left_bound_2 = aBasisBounds[I_1].first;
         right_bound_2 = aBasisBounds[I_1].second;
      }
      else
      {
         left_bound_1 = grid_box.GetQleft(I_0);
         right_bound_1 = grid_box.GetQright(I_0);
         left_bound_2 = grid_box.GetQleft(I_1);
         right_bound_2 = grid_box.GetQright(I_1);
      }

      std::vector<Nb> q1, q2;
      std::vector<MidasVector> q_vals;
      In n_pts = I_20;

      for (In i_q = I_0; i_q <= n_pts; i_q++)
      {
         q1.push_back(left_bound_1 * (Nb(Nb(n_pts - i_q)/n_pts)));
         q2.push_back(left_bound_2 * (Nb(Nb(n_pts - i_q)/n_pts)));
      }
      for (In i_q = I_1; i_q <= n_pts; i_q++)
      {
         q1.push_back(right_bound_1 * (Nb(Nb(i_q)/n_pts)));
         q2.push_back(right_bound_2 * (Nb(Nb(i_q)/n_pts)));
      }

      In k = I_0;
      for (In i_q1 = I_0; i_q1 < q1.size(); i_q1++) 
      {
         for (In i_q2 = I_0; i_q2 < q2.size(); i_q2++) 
         {
            MidasVector q_vec(I_2, C_0);
            q_vec[I_0] = q1[i_q1];
            q_vec[I_1] = q2[i_q2];
            q_vals.push_back(q_vec);
         }
      }

      // Single call to EvaluatePotential
      MidasVector pot_vals(q_vals.size(), C_0);
      aProperties[aPropNo-1].EvaluatePotential(arModeCombi, q_vals, pot_vals);
         
      k = I_0;
      for (In p = I_0; p < q1.size(); p++)
      {
         for (In q = I_0; q < q2.size(); q++)
         {
            datafile << q1[p] << "\t" << q2[q] << "\t" << pot_vals[k] << std::endl;
         
            if (gPesIoLevel > I_13)
            {
               Mout << " Loading into the 2M datafile (Potential in analysis): " << q1[p] << "\t" << q2[q] << "\t" << pot_vals[k] << std::endl;
            }

            k++;

         }
         datafile << std::endl;
      }
      
      MidasVector q_pts(I_2, C_0);
      for (In p = I_0; p < grid_box.GetNbounds(I_0); p++)
      {
         q_pts[I_0] = grid_box.GetQBoxBound(I_0, p);

         for (In q = I_0; q < grid_box.GetNbounds(I_1); q++)
         {
            q_pts[I_1] = grid_box.GetQBoxBound(I_1, q);

            pointfile << q_pts[I_0] << "\t" << q_pts[I_1] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pts) << std::endl;
         
            if (gPesIoLevel > I_13)
            {
               Mout << " Loading into the 2M pointfile (Point in analysis): " << q_pts[I_0] << "\t" << q_pts[I_1] << "\t" << aProperties[aPropNo - 1].EvaluatePotential(arModeCombi, q_pts) << std::endl;
            }
         }
         pointfile << std::endl;
      }
      
      datafile.close();
      pointfile.close();

      // Copy for later analysis:
      CopyFile(curr_potential_file, iter_potential_file);
      CopyFile(curr_point_file, iter_point_file);

      if (  curr_iter >= I_1 
         && gPesCalcDef.GetmPesPlotAll()
         && !gPesCalcDef.GetmCalcDensAtEachMcl() 
         && !gPesCalcDef.GetmDoAdgaExtAtEachMcl()
         ) 
      {
         Mout << "Saving difference" << std::endl; 
         string data_save_curr_name = aAnalysisDir + "/" + "Iter_" + std::to_string(curr_iter)     + "_potential_";
         string data_save_prev_name = aAnalysisDir + "/" + "Iter_" + std::to_string(curr_iter-I_1) + "_potential_";
         for (In i_mode = I_0; i_mode < n_modes; i_mode++)
         {
            data_save_curr_name +="q"+(std::to_string(arModeCombi[i_mode]));
            data_save_prev_name +="q"+(std::to_string(arModeCombi[i_mode]));
            if (n_modes > I_1 && i_mode < n_modes - I_1) 
            {
               data_save_curr_name+="_";
               data_save_prev_name+="_";
            }
         }
         data_save_curr_name+= ".mplot";
         data_save_prev_name+= ".mplot";
         
         string data_rel_name = aAnalysisDir + "/" + "Rel_" + std::to_string(curr_iter) + "_";
         string data_abs_name = aAnalysisDir + "/" + "Abs_" + std::to_string(curr_iter) + "_";
         for (In i_mode=I_0; i_mode<n_modes; i_mode++)
         {
            data_rel_name +="q"+(std::to_string(arModeCombi[i_mode]));
            data_abs_name +="q"+(std::to_string(arModeCombi[i_mode]));
            if (n_modes>1 && i_mode<n_modes-1) 
            {
               data_rel_name+="_";
               data_abs_name+="_";
            }
         }
         data_rel_name+= ".mplot";
         data_abs_name+= ".mplot";
         ofstream rel_file;
         ofstream abs_file;
         rel_file.open(data_rel_name.c_str(),ios_base::out);
         rel_file.setf(ios::scientific);
         rel_file.setf(ios::uppercase);
         rel_file.precision(22);
         abs_file.open(data_abs_name.c_str(),ios_base::out);
         abs_file.setf(ios::scientific);
         abs_file.setf(ios::uppercase);
         abs_file.precision(22);
  
         Mout << " For mode " << arModeCombi << " The relative differences are dumped on file: " << data_rel_name << std::endl;
         Mout << " For mode " << arModeCombi << " The absolute differences are dumped on file: " << data_abs_name << std::endl;
  
         ifstream curr_data(data_save_curr_name.c_str());
         ifstream prev_data(data_save_prev_name.c_str());
         Nb c_curr_check = C_0;
         
         if (curr_data.fail())
         {
            MIDASERROR(" File with current potential not found");
         }
         if (prev_data.fail())
         {
            MIDASERROR(" File with previous potential not found");
         }
         string scurr=" ";
         string sprev=" ";
         while (!curr_data.eof() && !prev_data.eof())
         {
            while (getline(curr_data,scurr) && getline(prev_data,sprev)) // get new lines 
            {
               istringstream input_scurr(scurr);
               istringstream input_sprev(sprev);
               Nb x_curr, x_prev;
               while(input_scurr >> x_curr && input_sprev >> x_prev)
               {
                  if (x_curr != c_curr_check) // useful to plot as surface in gnuplot 
                  {
                     rel_file << endl;
                     abs_file << endl;
                  }
                  
                  Nb y_curr, y_prev;
                  while(input_scurr >> y_curr && input_sprev >> y_prev)
                  {
                     Nb z_curr, z_prev;
                     while(input_scurr >> z_curr && input_sprev >> z_prev)
                     {
                        if (x_prev == x_curr && y_prev == y_curr)
                        {
                           c_curr_check = x_curr;
                           rel_file << x_curr << "\t" <<  y_curr << "\t";
                           if (z_curr == C_0)
                              rel_file << C_0 <<  endl;
                           else
                              rel_file << fabs((z_prev-z_curr)/z_curr) <<  endl;
                           abs_file << x_curr << "\t" <<  y_curr << "\t" << (z_prev-z_curr) <<  endl;
                        }
                     }
                  }
               }
            }
            rel_file << endl;
            abs_file << endl;
            
            rel_file.close();
            abs_file.close();
         }
      }
   }

   return;
}

/**
 * Calculate the integrals pertaining to a particular mode combination
 *
 * @param aAdd         The address of the integrals.
 * @param aPrint       Print debug information on calculated integrals.
 * @param arProperties   The properties to calculate integrals for.
 * @param aCurrentIter The current iteration.
 *
 * @return   Return true if integrals where calculated.
 **/
bool ItGridHandler::CalcIntForMc
   (  const In& aAdd
   ,  const bool& aPrint
   ,  const std::vector<MidasPotential>& arProperties
   ,  const In& aCurrentIter
   )
{
   auto& grid_box = VecOfGridsAt(aAdd - I_1);
   
   In n_int = grid_box.GetNrOfBoxes();
   
   if (gPesIoLevel > I_12)
   {
      Mout << " Number of integration boxes for MC " << aAdd << " is: " << n_int << std::endl;
   }
   
   // If converged, do not calculate integrals. However, if we have to check
   // the extensions on each MCL we have to recalculate the 1D grid integrals!
   if (grid_box.GetConv() && !(grid_box.GetNmodes() == I_1 && gPesCalcDef.GetmDoAdgaExtAtEachMcl()))
   {
      if (gPesIoLevel > I_12)
      {
         Mout << " Grid add: " << aAdd << " is converged. Integration procedure is skipped " << std::endl;
      }
      return false; 
   }
   
   // If grid is subdivided into new intervals, then we need to recalculate integrals over the new intevals. ResizeIntegrals clears the stored integral information
   if (grid_box.GetNrOfIntegrals() != n_int) 
   {  
      grid_box.ResizeIntegrals(n_int);
   }
  
   //
   grid_box.CalcIntForBoxes(aPrint, arProperties, aCurrentIter, mNumModes);
   
   if (gPesIoLevel > I_12)
   {
      Mout << "  Nr. of sector is: " << n_int;
   }
  
   return true;
}

/**
 * Check the convergency and eventually prepare the list with the new point to be computed
 * CheckConvergence checks the Adga criteria concerning present and last iterations
 * CheckExtension checks if the Adga grid should be eroperties
 * 
 * @param aAdd
 * @param arModeCombi
 *
 * @return     Returns true if mode grid is converged, false otherwise.
 **/
bool ItGridHandler::CheckConvForMc
   (  In aAdd
   ,  const std::vector<In>& arModeCombi
   )
{
   auto& mode_grid = VecOfGridsAt(aAdd - I_1);
   
   //We only update the 1D boundaries when the given 1D grid is unconverged,
   // unless we update the Densities on each MC level and therefore check the 
   // boundaries each time a new density is calculated.
   if(mode_grid.GetNmodes() == I_1 && (gPesCalcDef.GetmDoAdgaExtAtEachMcl() || !mode_grid.GetConv()))
   {
      mode_grid.CheckExtension();
   }
   
   // If grid was already converged we do not check for convergence again, we just return
   if (mode_grid.GetConv())
   {
      if (gPesIoLevel > I_6)
      {
         std::stringstream s_str;
         s_str << " Mode combination " << arModeCombi << "  C ";
         OneArgOut72(Mout, s_str.str(), 'C');
      }

      return mode_grid.GetConv();
   }

   // CheckConvergence for every property surface we are working on.
   // Check absolute and relative convergence criteria
   In curr_iter = GetCurr1dIter();
   if (  curr_iter > I_0 
      || (curr_iter == I_0 && gPesCalcDef.GetmDoExtInFirstIter())
      )
   {
      mode_grid.CheckConvergence();
   }
  
   //
   if (mode_grid.GetConv())
   { 
      if (gPesIoLevel > I_6)
      {
         std::stringstream s_str;
         s_str << " Mode combination " << arModeCombi << " +C ";
         OneArgOut72(Mout, s_str.str(), 'C');
      }
   }
   else
   {
      if (gPesIoLevel > I_6)
      {
         std::stringstream s_str;
         s_str << " Mode combination " << arModeCombi;
         OneArgOut72(Mout, s_str.str(), 'C');
      }
   }

   return mode_grid.GetConv();
}

/**
 * Generate the list of Kvecs for Numder
 **/
void ItGridHandler::GetKvecsVecForModeCombi
   (  In aAdd
   ,  const std::vector<In>& arModeCombi
   ,  std::vector<std::vector<In> >& arKvecsVec
   )  const
{
   const auto& grid_box = VecOfGridsAt(aAdd - I_1);
   std::vector<In> grid_mode=grid_box.GetModeCombi();
   if (grid_mode != arModeCombi) 
   {
      MIDASERROR(" Wrong grid address in GetKvecsVecForModeCombi ");
   }
   grid_box.GetKvecsVec(arKvecsVec);
   return;
}

void ItGridHandler::FillXigGhosts
   (  const In& aAdd
   ,  std::vector<vector<In> >& arInXig
   )  const
{
   if (mCurrNdIter<I_0) 
   {
      return;

   }
   else
   {
      const auto& grid_box = VecOfGridsAt(aAdd - I_1);
      grid_box.FillXigGhosts(arInXig);
   }
   return;
}
