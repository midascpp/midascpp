/**
************************************************************************
*
* @file                ItGridDensities.h
*
* Created:             05-09-2017
*
* Author:              Ian H. Godtliebsen
*
* Short Description:   Stores data for box integral evaluation.
*                      
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ITGRIDDENSITIES_H_INCLUDED
#define ITGRIDDENSITIES_H_INCLUDED

#include <string>
#include <vector>

#include "inc_gen/TypeDefs.h"

/**
 *
 **/
class ItGridDensities
{
   private:
      //! The indicies of the properties used in ADGA
      std::vector<In> mPropIdxs;
      //! The number of modes.
      In mNmodes;
      //! A grid values vector for each property and each mode
      std::vector<std::vector<std::vector<Nb> > > mGridValues;
      //! A potential values vector for each property and each mode
      std::vector<std::vector<std::vector<Nb> > > mPotValues;

   public:
      //! Constructor from number of modes and the Property indecies
      ItGridDensities(In aNmodes, const std::vector<In>& aPropIdxs);

      //! Pre-load files.
      void LoadFiles(const std::string& aAnalysisDir, const std::vector<std::string>& aModeLabels);

      //! Get grid values.
      const std::vector<Nb>& GetGridValues(In aPropIdx, In aMode) const;

      //! Get potential surface values.
      const std::vector<Nb>& GetPotValues(In aPropIdx, In aMode) const; 

};

#endif /* ITGRIDDENSITIES_H_INCLUDED */
