/**
*************************************************************************
*
* @file                ItGrid.cc
*
* Created:             17/10/2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk) and M. Sparta (msparta@chem.au.dk)
*
* Short Description:   Definitions of class members for the
*                      iterative grid Boxes. 
*
* Last modified: Tue Apr 13, 2010  11:12AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "pes/adga/ItGrid.h"
#include "pes/adga/ItGridDensities.h"
#include "pes/PesFuncs.h"
#include "pes/integrals/OneModeDensityInterpolator.h"
#include "pes/integrals/SumOverProductIndexer.h"
#include "pes/integrals/ModeCombiIntegrals.h"
#include "pes/integrals/OneModeDensityTimesFunction.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/MultiIndex.h"
#include "util/Debug.h"
#include "util/Math.h"
#include "input/PesCalcDef.h"
#include "potentials/MidasPotential.h"

#include "libmda/numeric/float_eq.h"
#include "libmda/util/stacktrace.h"

/**
 * Constructor from mode combination vector.
 * 
 * @param aModes        Mode combination for grid.
 * @param arDensities   The densities.
 * @param aPropNo       The property surface optimized in Adga
 **/
ItGrid::ItGrid
   (  const std::vector<In>& aModes
   ,  const ItGridDensities& arDensities
   ,  const Uin& aPropNo
   )
   :  mDensities(arDensities)
   ,  mNumProp(aPropNo)
   ,  mNumModes(aModes.size())
   ,  mIterTotalDensInPot(aPropNo, C_0)
   ,  mIterTotalDens(aPropNo, std::vector<Nb>())
   ,  mModes(aModes)
   ,  mQlScal(mNumModes)
   ,  mQrScal(mNumModes)
   ,  mGhosts(mNumModes)
   ,  mBounds(mNumModes)
   ,  mNewBounds(mNumModes)
   ,  mNewBoundsAllSurfaces(mNumProp)
   ,  mTotRhoV(aPropNo)
   ,  mOldTotRhoV(aPropNo)
   ,  mConverged(false)
   ,  mExtend(false)
{
   //
   for (Uin i = 0; i < mNewBoundsAllSurfaces.size(); ++i)
   {
      mNewBoundsAllSurfaces[i].resize(mNumModes);
   }

   return;
}

/**
 * Set the bounds.
 *
 * @param aMode
 * @param aLeft
 * @param aRight
 * @param aSize
 **/
void ItGrid::SetBounds
   (  const In aMode
   ,  const In aLeft
   ,  const In aRight
   ,  const In aSize
   )
{
   mBounds[aMode].clear();
   mBounds[aMode].push_back(aLeft);
   mBounds[aMode].push_back(I_0);
   mBounds[aMode].push_back(aRight);

   if (aSize == I_1)
   {
      mNewBounds[aMode].clear();
      mNewBounds[aMode].push_back(In(aLeft / I_2));
      mNewBounds[aMode].push_back(In(aRight / I_2));
   }
   return;
} 

/**
 * Set a vector of bounds.
 * 
 * @param aMode
 * @param arNewBounds
 **/
void ItGrid::SetBounds
   (  const In aMode
   ,  const std::vector<In>& arNewBounds
   )
{
   mBounds[aMode].clear();
   In n_b = arNewBounds.size();
   for (In i_b = I_0; i_b < n_b; ++i_b)
   {
      mBounds[aMode].push_back(arNewBounds[i_b]);
   }
   return;
}

/**
 * Reinitialize the object (for the multilevel approach).
 *
 * @param aMultiLevel   Which multilevel are we doing.
 **/
void ItGrid::ReIni
   (  const In aMultiLevel
   )
{
   if (mNumModes == I_1)
   {
      In left = mBounds[I_0][I_0];
      In right = mBounds[I_0][mBounds[I_0].size() - I_1];
      mBounds[I_0].clear();
      mNewBounds[I_0].clear();

      //Clear the new vector with all boundaries as well
      for (Uin i = I_0; i < mNumProp; i++)
      {
         mNewBoundsAllSurfaces[i][I_0].clear();
      }
      
      if( (aMultiLevel >= 1) 
       && gPesCalcDef.MultiLevel() 
       && gPesCalcDef.HistoricAdgaGrid() 
       && !gPesCalcDef.HistoricPesNumSource()
        )
      { // old historic, but only for level larger than 1 (the old code was weird, I know!!)
         // old bounds
         mBounds[I_0].push_back(left);
         mBounds[I_0].push_back(In(left/I_2));
         mBounds[I_0].push_back(I_0);
         mBounds[I_0].push_back(In(right/I_2));
         mBounds[I_0].push_back(right);
         
         // old NewBound
         mNewBounds[I_0].push_back(In(I_3*left/I_4));
         mNewBounds[I_0].push_back(In(left/I_4));
         mNewBounds[I_0].push_back(In(right/I_4));
         mNewBounds[I_0].push_back(In(I_3*right/I_4));
      }
      else
      { // new scheme
         // new Bounds
         mBounds[I_0].push_back(left);
         mBounds[I_0].push_back(I_0);
         mBounds[I_0].push_back(right);

         // new NewBound
         mNewBounds[I_0].push_back(In(right/I_2));
         mNewBounds[I_0].push_back(In(left/I_2));
      }
      mConverged=false;
      mExtend=false;
   }
   else
   {
      for (In i = I_0; i < mNumModes; ++i)
      {
         mBounds[i].clear();
         mNewBounds[i].clear();
         mConverged=false;
         mExtend=false;
      }
   }
}



/**
 * Get the total number of boxes 
 **/
In ItGrid::GetNrOfBoxes
   (
   )  const
{
   In n_int = I_1;
   for (In i = I_0; i < mNumModes; ++i)
   {
      n_int *= (mBounds[i].size() - I_1);
   }
   return n_int;
}

/**
 * Set for mode aMode a specific point aBound to be computed.
 **/
void ItGrid::SetNewBound
   (  const In& aMode
   ,  const In& aBound
   )
{
   mNewBounds[aMode].push_back(aBound);
   return;
}

/***************************************************************************//**
 * Get the multi-index of a specific box, one index per mode.
 * Example:
 *     boxes per mode = 3, 4, 5
 *          aBox     arBoxIndex
 *             0     0 0 0
 *             1     1 0 0
 *             2     2 0 0
 *             3     0 1 0
 *             4     1 1 0
 *           ...     ...
 *            11     2 3 0
 *            12     0 0 1
 *            13     1 0 1
 *           ...     ...
 *            34     1 3 2
 *           ...     ...
 *            59     2 3 4
 * The number of `boxes per mode` is determined from the sizes of elements of
 * ItGrid::mBounds, such that for mode `m` the number of `boxes` is
 * `mBounds[m].size()-1`.
 *
 * @param[in] aBox
 *    The serial index of the box of interest.
 * @param[out] arBoxIndex
 *    Holds the multi-index upon return.
 ******************************************************************************/
void ItGrid::GetBoxIndex
   (  In aBox
   ,  vector<In>& arBoxIndex
   )  const
{
   In n_mod=I_1; 
   while (n_mod!=mNumModes)
   {
      In i=mNumModes-n_mod;
      In nmod_1=I_1;
      for (In i_mod_1=I_0; i_mod_1<i; i_mod_1++) 
      {
         nmod_1*=(mBounds[i_mod_1].size()-I_1);
      }

      arBoxIndex[i]=In(aBox/nmod_1);
      aBox %= nmod_1;
      n_mod++;
   }
   arBoxIndex[I_0]=aBox;
   return;
}

/**
 * Get the size of the box (lenght/area/volume...)
**/
Nb ItGrid::GetBoxSize(const std::vector<In>& arBounds) const
{
   Nb size = C_1;
   for (In i_mod = I_0; i_mod < mNumModes; ++i_mod)
   {
      size *= mBounds[i_mod][arBounds[i_mod] + I_1] * mQrScal[i_mod] - mBounds[i_mod][arBounds[i_mod]] * mQlScal[i_mod];
   }
   
   return size;
}

/**
 * Split a box that is not coverged
**/
void ItGrid::SplitBox(const std::vector<In>& arBounds)
{
   for (In i_mod = I_0; i_mod < mNumModes; ++i_mod)
   {
      mNewBounds[i_mod].push_back((mBounds[i_mod][arBounds[i_mod] + I_1] + mBounds[i_mod][arBounds[i_mod]])/I_2);
   }
   return;
}

/**
 * Get the bounds of a specific box
**/
void ItGrid::GetQbox
   (  const std::vector<In>& arBoxIndex
   ,  std::vector<pair<Nb,Nb> >& arQbox
   )  const
{
   for (In i_mod = I_0; i_mod < mNumModes; ++i_mod)
   {
      arQbox[i_mod] = GetQBoundsPair(i_mod, arBoxIndex[i_mod]);
   }
   return;
}

/**
 * Get the bounds of a specific box
**/
void ItGrid::GetQdomain(std::vector<std::pair<Nb,Nb> >& arQbox) const
{
   for (In i_mod = I_0; i_mod < mNumModes; i_mod++)
   {
      arQbox[i_mod].first = mBounds[i_mod][I_0] * mQlScal[i_mod];
      arQbox[i_mod].second = mBounds[i_mod][mBounds[i_mod].size() - I_1] * mQrScal[i_mod];
   }
   return;
}

void ItGrid::CheckExtension()
{
   mExtend = false; // Reset the extension flag, this will be set in CheckExtensionIthProp if we are extending.
   for(In i =0; i < mNumProp; ++i)
   {
      CheckExtensionIthProp(i);
   }
   if(mExtend)
   {
      SetLeftBound(); //set max left bound to the minimum of the boundaries of all surfaces.
      SetRightBound(); //set max right bound to the maximum of the boundaries of all surfaces.
   }
}

/**
 * Check if a monodimensional grid should be extended, for prop aPropNo
 **/
void ItGrid::CheckExtensionIthProp
   ( Uin aPropNo
   )
{
   if (gPesCalcDef.GetmPesIterMax() + gPesCalcDef.GetmPesItGridExpScalFact() > I_29)
   {
      MIDASERROR(" ADGA grid cannot be defined in terms of fractions larger than 2^29! Please ensure that the sum of integers specified under keywords #2 NiterMax and #2 ItExpGridScalFact does not exceed 29! ");
   }
   
   In max_box = In(std::pow(C_2, gPesCalcDef.GetmPesItGridExpScalFact()) * std::pow(C_2, gPesCalcDef.GetmPesIterMax()));

   // Print absolute box bounds
   if (gPesIoLevel > I_14)
   {
      Mout << " Auxiliary grid, (fraction logic)        : [" << -max_box <<","<< max_box << "]"<< std::endl;
      Mout << " Auxiliary grid, (Q-units)               : [" << -max_box*GetQlScal() <<","<< max_box*GetQrScal() << "]"<< std::endl;
   }

   // if we only want to expand the Adga grid in a static fashion, then fix the extension to 1/4 of the grid, which is defined by the maximum number of ADGA iterations, i.e. tics are given in fractions of the maximum number of ADGA iterations, note that 2^x/4 = 2^(x-2)
   In max_ext = In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2));
      
   if (gPesIoLevel > I_11)
   {
      Mout << " Static potential boundaries extension point: " << max_ext << std::endl;
   }
   
   // the vector possible_max_ext contains all the (fraction logic) allowed expansions from the present Adga grid bounds to the max_box limit 
   std::vector<In> possible_max_ext;
   possible_max_ext.reserve(I_16);
   std::vector<In> possible_max_ext_l;
   std::vector<In> possible_max_ext_r;
   if (gPesCalcDef.GetmDynamicAdgaExt())
   {
      if (gPesCalcDef.GetmMeanDensOnFiles())
      {
         MIDASERROR("The #2 DynamicAdgaExt keyword does not work with the #2 MeanDensOnFiles keyword, you need to choose!");
      }

      auto no_ext_points_l = In((max_box - std::abs(mBounds[I_0][I_0])) / std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2)) + I_1;

      for (In i = I_0; i < no_ext_points_l; i++)
      {
         possible_max_ext_l.emplace_back(i * std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2));
      }

      if (gPesIoLevel > I_11)
      {
         Mout << " Dynamic potential boundaries left extension points:  " << possible_max_ext_l << std::endl;
      }

      auto no_ext_points_r = In((max_box - std::abs(mBounds[I_0][mBounds[I_0].size() - I_1])) / std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2)) + I_1;

      for (In i = I_0; i < no_ext_points_r; i++)
      {
         possible_max_ext_r.emplace_back(i * std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2));
      }

      if (gPesIoLevel > I_11)
      {
         Mout << " Dynamic potential boundaries right extension points: " << possible_max_ext_r << std::endl;
      }
   }

   if (gPesIoLevel > I_14)
   {
      Mout << " Potential boundaries, (fraction logic)  : [" << mBounds[I_0][I_0] << "," << mBounds[I_0][mBounds[I_0].size() - I_1] << "]" << std::endl;
      Mout << " Potential boundaries, (Q-units)         : [" << mBounds[I_0][I_0] * GetQlScal() << "," << mBounds[I_0][mBounds[I_0].size() - I_1] * GetQrScal() << "]" << std::endl;
   }

   // If we use the mean vibrational density to determine convergence of the Adga then
   if (gPesCalcDef.GetmPesUseMeanDens())
   {
      Nb tot_den = C_0;
      Nb prev_tot_den = C_0;
      for (In j = I_0; j < mIntegrals.size(); ++j)
      {
         auto rho_int_val = mIntegrals[j].GetRhoIntForPropSurfI(aPropNo);

         auto zero_thr = 1.e-16;
         if (  rho_int_val < C_0
            && libmda::numeric::float_geq(std::abs(rho_int_val), zero_thr) 
            )
         {
            MIDASERROR("Integral evaluated for the vibrational density is negative: " + std::to_string(rho_int_val));
         }
         else if  (  rho_int_val < C_0
                  && libmda::numeric::float_lt(std::abs(rho_int_val), zero_thr) 
                  )
         {
            std::stringstream ss;
            ss << "Integral evaluated for the vibrational density is negative, but also very close to 0. It is set to zero to have strictly positive (or zero) density contributions." << std::endl
               << "The small negative density value, before it is set to 0, is: " << rho_int_val << std::endl;
            MidasWarning(ss.str());
            rho_int_val = C_0;
         }

         tot_den += rho_int_val;
      }

      // Output information on the evaluated vibrational density
      if (gPesIoLevel > I_7)
      {
         std::stringstream mode_info;
         std::stringstream basis_info;
         std::stringstream pot_info;
         mode_info  << " Integrated vibrational density for mode Q" 
                    << std::fixed << std::setprecision(0) << mModes[I_0] << ": ";
         basis_info << "  Inside the basis boundaries:     " 
                    << std::fixed << std::setprecision(16) << mIterTotalDens[aPropNo][(mModes[I_0])]; 
         pot_info   << "  Inside the potential boundaries: " 
                    << std::fixed << std::setprecision(16) << tot_den;
         OneArgOut72(Mout, mode_info.str(), 'C');
         OneArgOut72(Mout, basis_info.str(), 'C');
         OneArgOut72(Mout, pot_info.str(), 'C');
      }
      
      // Check if grid should be extended, i.e. check for that the vibrational density is contained by the potential
      if (tot_den < C_1 - gPesCalcDef.GetmPesItResDensThr())
      {
         // Need grid extension, setting ADGA not converged 
         mExtend = true;  
         // Check for density leaks
         prev_tot_den = GetPrevTotDensForPropSurfI(aPropNo);
         if (tot_den <= prev_tot_den)
         {
            MidasWarning("Total density inside the potential is smaller in this iteration than the last, which might indicate a density leak for mode Q" + std::to_string(mModes[I_0]) + "! Possible solution is to set basis bounds tighter", true);
         }
         SetCurrTotDensForPropSurfI(aPropNo, tot_den);

         //
         In new_bl = I_0;
         In new_br = I_0;
         //
         In max_new_bl = I_0;
         In max_new_br = I_0;

         // Calculated the density value for the left and right side of the potential
         Nb dvl = EvaluateDensity(aPropNo, mModes[I_0], GetQleft());
         Nb dvr = EvaluateDensity(aPropNo, mModes[I_0], GetQright());
      
         Nb rho_left = dvl * max_ext * GetQlScal() * C_I_2;
         Nb rho_right = dvr * max_ext * GetQrScal() * C_I_2;
       
         std::pair<Nb, Nb> basis_bounds; // pair for holding the basis boundaries
         bool DoDynamicAdgaExt = gPesCalcDef.GetmDynamicAdgaExt();
         if (DoDynamicAdgaExt)
         {
            std::string basis_name = gVscfCalcDef[I_0].Basis(); // basis_name = name given on input
            In i_basis = -I_1;
            for (In i = I_0; i < gBasis.size(); ++i) 
            {
               if (gBasis[i].GetmBasName() == basis_name)
               {
                  i_basis = i;
               }
            }
            if (i_basis == -I_1)
            {
               MIDASERROR("Basis not found in VscfDrv");
            }

            // Basis set boundaries are stored as a pair
            basis_bounds = gBasis[i_basis].GetBasDefForGlobalMode(mModes[I_0]).GetBasisGridBounds();
   
            if (gPesIoLevel > I_14)
            {
               Mout << " Basis boundaries, (fraction logic)      : [" << In(basis_bounds.first / GetQlScal()) << "," << In(basis_bounds.second / GetQrScal()) << "]" << std::endl;
               Mout << " Basis boundaries, (Q-units)             : [" << basis_bounds.first << "," << basis_bounds.second << "]" << std::endl;
            }
            
            // Switch to fraction logic from Q-units
            basis_bounds.first = In(basis_bounds.first / GetQlScal());
            basis_bounds.second = In(basis_bounds.second / GetQrScal());
         }

         // Extend the potential boundary to the left?
         if (rho_left >= gPesCalcDef.GetmPesItResDensThr() * C_I_2)
         {
            if (DoDynamicAdgaExt)
            {
               for (In i = possible_max_ext_l.size() - I_1; i > I_0; --i)
               {
                  if (std::abs(GetQleft() / GetQlScal() - possible_max_ext_l[i]) > std::abs(basis_bounds.first))
                  {
                     continue;
                  }
               
                  std::vector<In> int_box(mNumModes, I_0);
                  GetBoxIndex(I_0, int_box);
                  std::vector< std::pair<Nb, Nb> > int_bounds(mNumModes);
              
                  for (In k = I_0; k < mNumModes; ++k)
                  {
                     int_bounds[k].first = basis_bounds.first * GetQlScal();
                     int_bounds[k].second = GetQleft() - possible_max_ext_l[i] * GetQlScal();
                  }

                  //
                  qGaussInt(aPropNo, I_0, int_bounds, mGaussPoints);
                  Nb dens_outside_bounds = C_0;
                  dens_outside_bounds += mIntegrals[I_0].GetRhoBasisIntForPropSurfI(aPropNo);

                  if (dens_outside_bounds > gPesCalcDef.GetmPesItResDensThr() * C_I_2)
                  {
                     max_ext = possible_max_ext_l[i];
                     break;
                  }
               }
            }

            // Define the new left potential boundary
            new_bl = mBounds[I_0][I_0] - max_ext;

            if (gPesIoLevel > I_11)
            {
               Mout << " Extend to the left: " << new_bl  << std::endl;
            }

            if (std::abs(new_bl) >= max_box)
            {
               if (std::abs(mBounds[I_0][I_0]) < max_box)
               {
                  new_bl = I_0 - max_box;
               }
               else
               {
                  Mout << " Warning: attempting to go past the boundaries " << std::endl;
                  new_bl = I_0;
               }
            }
         
            // Store left boundary for this surface 
            mNewBoundsAllSurfaces[aPropNo][I_0].push_back(new_bl);
         }

         // Extend the potential boundary to the right?
         if (rho_right >= gPesCalcDef.GetmPesItResDensThr() * C_I_2)
         {
            if (DoDynamicAdgaExt)
            {
               for (In i = possible_max_ext_r.size() - I_1; i > I_0; --i)
               {
                  if (std::abs(GetQright() / GetQrScal() + possible_max_ext_r[i]) > std::abs(basis_bounds.second))
                  {
                     continue;
                  }
               
                  std::vector<In> int_box(mNumModes, I_0);
                  GetBoxIndex(I_0, int_box);
                  std::vector< std::pair<Nb, Nb> > int_bounds(mNumModes);
              
                  for (In k = I_0; k < mNumModes; ++k)
                  {
                     int_bounds[k].first = GetQright() + possible_max_ext_r[i] * GetQrScal();
                     int_bounds[k].second = basis_bounds.second * GetQrScal();
                  }
                  
                  //
                  qGaussInt(aPropNo, I_0, int_bounds, mGaussPoints);
                  Nb dens_outside_bounds = C_0;
                  dens_outside_bounds += mIntegrals[I_0].GetRhoBasisIntForPropSurfI(aPropNo);

                  if (dens_outside_bounds > gPesCalcDef.GetmPesItResDensThr()*C_I_2)
                  {
                     max_ext = possible_max_ext_r[i];
                     break;
                  }
               }
            }

            new_br = mBounds[I_0][mBounds[I_0].size() - I_1] + max_ext;

            if (gPesIoLevel > I_11)
            {
               Mout << " Extend to the right: " << new_br << std::endl;
            }

            if (std::abs(new_br) >= max_box)
            {
               if (std::abs(mBounds[I_0][mBounds[I_0].size() - I_1]) < max_box)
               {
                  new_br = max_box;
               }
               else
               {
                  Mout << " Warning: attempting to go past the boundaries " << std::endl;
                  new_br = I_0;
               }
            }

            // Store right boundary for this surface 
            mNewBoundsAllSurfaces[aPropNo][I_0].push_back(new_br);
         }

         // Extend half left, and half right
         if (  rho_left < gPesCalcDef.GetmPesItResDensThr() * C_I_2 
            && rho_right < gPesCalcDef.GetmPesItResDensThr() * C_I_2
            )
         {
            //
            MidasWarning("Did not succeed with grid extension in the ADGA for mode Q" + std::to_string(mModes[I_0]) + ", will attempt naïve extension", true);

            // If doing multi-state ADGA
            if (gPesCalcDef.GetmPesAdaptiveProps().size() > I_1)
            {
               MIDASERROR("Trying to extend half left, and half right. Not implemented for multi-state ADGA.");
            }

            // For the dynamic grid extension functionality, we default to the usual static extension factor if we hit this "fall-back option" for grid extension
            if (DoDynamicAdgaExt)
            {
               max_ext = In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() - I_2));
            }

            // First, extend half left
            new_bl = mBounds[I_0][I_0] - max_ext / I_2;

            // Output
            if (gPesIoLevel > I_11)
            {
               Mout << " Extend half to the left: " << new_bl  << std::endl;
            }

            // Check that extension is sensible
            if (std::abs(new_bl) >= max_box)
            {
               if (std::abs(mBounds[I_0][I_0]) < max_box)
               {
                  new_bl = I_0 - max_box;
               }
               else
               {
                  MidasWarning("Attempting to go past maximum box boundaries in extending half left", true);
                  new_bl = I_0;
               }
            }

            // Second, extend half right
            new_br = mBounds[I_0][mBounds[I_0].size() - I_1] + max_ext / I_2;

            // Output
            if (gPesIoLevel > I_11)
            {
               Mout << " Extend half to the right: " << new_br << std::endl;
            }

            // Check that extension is sensible
            if (std::abs(new_br) >= max_box)
            {
               if (std::abs(mBounds[I_0][mBounds[I_0].size() - I_1]) < max_box)
               {
                  new_br = max_box;
               }
               else
               {
                  MidasWarning("Attempting to go past maximum box boundaries in extending half right", true);
                  new_br = I_0;
               }
            }

            // Set new boundaries
            mNewBounds[I_0].push_back(new_bl);
            mNewBounds[I_0].push_back(new_br);
         }

         //
         return;
      }
      // If the total vibrational density integrated is larger than one, then a problem have occured
      else if (tot_den - C_I_10_8 > C_1)
      {
         MidasWarning("The total vibrational density is larger than unity for mode Q" + std::to_string(mModes[I_0]), true);
      }
     
      //
      return;
   }
   // If we use the maximum vibrational density to determine convergence of the Adga then
   else if (gPesCalcDef.GetmPesUseMaxDens())
   {
      if (std::fabs(mIntegrals[I_0].GetRhoIntForPropSurfI(aPropNo)) > gPesCalcDef.GetmPesItResDensThr())
      {
         In new_b=mBounds[I_0][I_0]-max_ext;
         if (abs(new_b)>=max_box) 
         {
            if (abs(mBounds[I_0][I_0])<max_box)
               new_b=I_0-max_box;
            else
           {
              MidasWarning(" Warning: attempting to go past the boundaries ");
              new_b=I_0;
           }
         }
         if (abs(new_b) > I_0)
         {
            if (gPesIoLevel > I_11)
            {
               Mout << " Extending to the left: "<< new_b << std::endl;
            }
            mExtend = true;
            mNewBounds[I_0].push_back(new_b);
         }
      }

      if (std::fabs(mIntegrals[mIntegrals.size() - I_1].GetRhoIntForPropSurfI(aPropNo)) > gPesCalcDef.GetmPesItResDensThr())
      {
         In new_b=mBounds[I_0][mBounds[I_0].size()-I_1]+max_ext;
         if (abs(new_b)>=max_box) 
         {
            if (abs(mBounds[I_0][mBounds[I_0].size()-I_1])<max_box)
               new_b=max_box;
            else
           {
               MidasWarning(" Warning: attempting to go past the boundaries ");
               new_b=I_0;
           }
         }
         if (abs(new_b)>I_0)
         {
            if (gPesIoLevel > I_11)
            {
               Mout << " Extending to the right: "<< new_b << std::endl;
            }
            mExtend=true;
            mNewBounds[I_0].push_back(new_b);
         }
      }

      // Done dealing with max average vibrational density
      return;
   }
   else
   {
      MIDASERROR("Did not get information on how the vibrational density should be treated in order to determine convergence with respect to the density criterion");
   }
}
   
/**
 * Set left boundary
 **/
void ItGrid::SetLeftBound()
{
   //Initialize vector of size mNumProp with all the left boundaries from all surfaces to be zeroes.
   //In this vector the minimum left boundaries will be filled in. Later the mimimum of all these will be pushed to mNewBounds.
   std::vector<In> vecPropLeft(mNumProp, I_0);
   //Loop over all the surfaces 
   for (Uin i_loop = I_0; i_loop < mNumProp; ++i_loop)
   {
      //If the some boundaries have been extended for a given surface:
      if (mNewBoundsAllSurfaces[i_loop][I_0].size() > 0 )
      {
         //... then find the minimum of these and save this in the VecPropLeft
         std::vector<In>::iterator result = std::min_element(mNewBoundsAllSurfaces[i_loop][I_0].begin(), mNewBoundsAllSurfaces[i_loop][I_0].end());
         In result2 = *result;
         vecPropLeft[i_loop] = result2;
      }
   }
   //Find and push the minimum (furthest "to the left") boundary to mNewBounds
   std::vector<In>::iterator max_new_bl = std::min_element(std::begin(vecPropLeft),std::end(vecPropLeft));
   mNewBounds[I_0].push_back(*max_new_bl);
}

/**
 * Set right boundary
 **/
void ItGrid::SetRightBound()
{
   //Initialize vector of size mNumProp with all the right boundaries from all surfaces to be zeroes.
   //In this vector the maximum right boundaries will be filled in. Later the maximum of all these will be pushed to mNewBounds.
   std::vector<In> vecPropRight(mNumProp, I_0);
   //Loop over all the surfaces 
   for(Uin i_loop = I_0; i_loop < mNumProp; ++i_loop)
   {
      //If the some boundaries have been extended for a given surface:
      if (mNewBoundsAllSurfaces[i_loop][I_0].size() > 0 )
      {
         //... then find the maximum of these and save this in the VecPropLeft
         std::vector<In>::iterator result = std::max_element(mNewBoundsAllSurfaces[i_loop][I_0].begin(), mNewBoundsAllSurfaces[i_loop][I_0].end());
         In result2 = *result;
         vecPropRight[i_loop] = result2;
      }
   }
   //Find and push the maximum (furthest "to the right") boundary to mNewBounds
   std::vector<In>::iterator max_new_br = std::max_element(std::begin(vecPropRight),std::end(vecPropRight));
   mNewBounds[I_0].push_back(*max_new_br);
}
 
 /**
  * Check convergence for a specific property surface
  **/
void ItGrid::CheckConvergence()
{
   //If we extended the grid is not converged
   mConverged = !mExtend;
   // Scaling factor for the relative convergence thresholds
   const auto& rel_scal_fact = gPesCalcDef.GetmAdgaRelConvScalFacts()[mNumModes - I_1];
   
   // Scaling factor for the absolute convergence thresholds
   const auto& abs_scal_fact = gPesCalcDef.GetmAdgaAbsConvScalFacts()[mNumModes - I_1];

   if (gPesIoLevel > I_9)
   {
      Mout << " Scale factor for the relative ADGA convergence criterion is "  << rel_scal_fact << std::endl;
      
      Mout << " Scale factor for the absolute ADGA convergence criterion is "  << abs_scal_fact << std::endl;
   }
   
   // Define the relative convergence threshold
   Nb eps_rel = C_0;
   if (gPesCalcDef.GetmPesUseItPotThr() || gPesCalcDef.GetmPesUseItDensThr() || gPesCalcDef.GetmPesUseItVdensThr())
   {
      eps_rel = gPesCalcDef.GetmPesItVdensThr();
   }
   else if (gPesCalcDef.GetmPesUseItNormVdensThr())
   {
      eps_rel = gPesCalcDef.GetmPesItNormVdensThr();
   }
   else
   {
      MIDASERROR("Don't know how to treat the relative ADGA convergence criterion");
   }

   // The relative convergence threshold is scaled according to the current MCL
   eps_rel *= rel_scal_fact;
   MidasWarningIf(libmda::numeric::float_eq(eps_rel, C_0), "Warning eps_rel is 1.0");

   // Output
   if (gPesIoLevel > I_11)
   {
      Mout << " Relative convergence threshold = " << eps_rel << std::endl;
   }
   
   // Define the absolute convergence threshold
   Nb eps_abs = C_0;
   if (gPesCalcDef.GetmPesUseItPotThr() || gPesCalcDef.GetmPesUseItDensThr() || gPesCalcDef.GetmPesUseItVdensThr())
   {
      eps_abs = gPesCalcDef.GetmPesItResEnThr();
   }
   else if (gPesCalcDef.GetmPesUseItNormVdensThr())
   {
      eps_abs = gPesCalcDef.GetmPesItNormResEnThr();
   }
   else
   {
      MIDASERROR("Don't know how to treat the absolute ADGA convergence criterion");
   }

   // If 'historic' case we loosen absolute threshold for ADGA
   if (gPesCalcDef.HistoricAdgaConv() && gPesCalcDef.GetmPesUseItVdensThr())
   {
      eps_abs *= rel_scal_fact;
   }
   else
   {
      // The absolute convergence threshold is scaled according to the current MCL
      eps_abs *= abs_scal_fact;
   }

   // Output
   if (gPesIoLevel > I_11)
   {
      Mout << " Absolute convergence threshold = " << eps_abs << std::endl;
   }

   //Vectors needed for Adaptive Search
   std::vector<Nb> v_adapt_rel(GetNrOfBoxes(), C_0);
   std::vector<Nb> v_adapt_abs(GetNrOfBoxes(), C_0);
   
   // Loop over the grid boxes
   for (In i_b = I_0; i_b < GetNrOfBoxes(); ++i_b)
   {
      
      if (gPesIoLevel > I_14)
      {
         Mout << " Checking convergency on box nr: " << i_b << std::endl;
         Mout << " Box interval:    " << GetQBoxBound(I_0, i_b) << ", " << GetQBoxBound(I_0, i_b + I_1) << std::endl;
         Mout << mIntegrals[i_b] << std::endl << std::endl;
      }

      if (gPesCalcDef.GetmPesUseItPotThr())
      {
         mIntegrals[i_b].CheckItPotThres(gPesCalcDef.GetmPesItPotThr(), eps_abs);
      }
      if (gPesCalcDef.GetmPesUseItDensThr())
      {
         mIntegrals[i_b].CheckItDensThres(gPesCalcDef.GetmPesItDensThr(), eps_abs);
      }
      if (gPesCalcDef.GetmPesUseItVdensThr())
      {
         mIntegrals[i_b].CheckItVDensThres(eps_rel, eps_abs);
      }
      if (gPesCalcDef.GetmPesUseItNormVdensThr())
      {
         mIntegrals[i_b].CheckItNormVDensThr(eps_rel, eps_abs);
      }

      // Collect result
      MidasDebug(PrintIf(mIntegrals[i_b].GetConverged(), "Converged", "NOT converged"));
      
      mConverged = (mConverged && mIntegrals[i_b].GetConverged());

      //If doing adaptive search, store the result for later
      if (mIntegrals[i_b].GetConverged() == false && gPesCalcDef.GetmPesAdaptivSearch()) 
      {
         if (gPesCalcDef.GetmPesUseItVdensThr())
         {
            v_adapt_rel[i_b] = mIntegrals[i_b].GetRelRhoVerrSum();
            v_adapt_abs[i_b] = mIntegrals[i_b].GetAbsRhoVerrSum();
         }
         else if (gPesCalcDef.GetmPesUseItNormVdensThr())
         {
            v_adapt_rel[i_b] = mIntegrals[i_b].GetRelNormRhoVerrSum();
            v_adapt_abs[i_b] = mIntegrals[i_b].GetAbsNormRhoVerrSum();
         }
         else
         {
            MidasWarning(" You might not have specified how ADGA is supposed to adapt the integration limits in order to form new intervals, please check this!");
         }
      }
   }
   
   // For mNumModes > 1 setup boxes to be split for the adaptive search algorithm
   // if used in the calculation, otherwise we are done for now.
   if (mNumModes != I_1 && !mConverged && gPesCalcDef.GetmPesAdaptivSearch())
   {
      //Set all boxes to be converged for now...
      for(In i_b = I_0; i_b < GetNrOfBoxes(); ++i_b)
      {
         mIntegrals[i_b].SetConverged(true);
      }
      In n_max = gPesCalcDef.GetmPesNmaxSplittings();
      if (gPesCalcDef.GetmPesNmaxSplittings() < I_0)
      {
         n_max = std::abs(gPesCalcDef.GetmPesNmaxSplittings());
      }
      //Then find the boxes that needs to be split and set them to unconverged
      for (In i_n = I_0; i_n < n_max; ++i_n)
      {
         In box_ind_abs = std::max_element(v_adapt_abs.begin(), v_adapt_abs.end())-v_adapt_abs.begin();
         //If we find that the max element is C_0, we are done since all remaining boxes are converged
         if(v_adapt_abs[box_ind_abs] == C_0)
         {
            break;
         }
         if (gPesIoLevel > I_11)
         {
            Mout << " Adaptive Search: Splitting box: " << box_ind_abs;
            Mout << " (abs. error: " << v_adapt_abs[box_ind_abs] << ")" << std::endl;
         }
         mIntegrals[box_ind_abs].SetConverged(false);
         // If the number of splits are set negative we also add points for the symmetric box
         if (gPesCalcDef.GetmPesNmaxSplittings() < I_0) 
         {
            vector<In> ain_bounds_abs(mNumModes, I_0);
            GetBoxIndex(box_ind_abs, ain_bounds_abs);
            if (gPesIoLevel > I_11)
            {
               Mout << " and the symmetric box:          " << GetNrOfBoxes() - I_1 - box_ind_abs;
               Mout << " (abs. error: "<< v_adapt_abs[GetNrOfBoxes() - I_1 - box_ind_abs] << ")"<< std::endl << std::endl;
            }
            vector<In> sin_bounds_abs(mNumModes, I_0);
            In s_box_ind_abs = GetNrOfBoxes() - I_1 - box_ind_abs;
            mIntegrals[s_box_ind_abs].SetConverged(false);
            v_adapt_abs[s_box_ind_abs] = C_0;
            v_adapt_rel[s_box_ind_abs] = C_0; // relative error will not split the same box
         }
         v_adapt_abs[box_ind_abs] = C_0;
         v_adapt_rel[box_ind_abs] = C_0; // relative error will not split the same box
      }
      for (In i_n = I_0; i_n < n_max; ++i_n)
      {
         In box_ind_rel = std::max_element(v_adapt_rel.begin(), v_adapt_rel.end())-v_adapt_rel.begin();
         //If we find that the max element is C_0, we are done since all remaining boxes are converged
         if(v_adapt_rel[box_ind_rel] == C_0)
         {
            break;
         }
         mIntegrals[box_ind_rel].SetConverged(false);
         if (gPesIoLevel > I_11)
         {
            Mout << " Adaptive Search: Splitting box: " << box_ind_rel;
            Mout << " (rel. error: " << v_adapt_rel[box_ind_rel] << ")" << std::endl;
         }
         if (gPesCalcDef.GetmPesNmaxSplittings() < I_0) 
         {
            vector<In> ain_bounds_rel(mNumModes, I_0);
            GetBoxIndex(box_ind_rel, ain_bounds_rel);
            if (gPesIoLevel > I_11)
            {
               Mout << " and the symmetric box:          " << GetNrOfBoxes() - I_1 - box_ind_rel;
               Mout << " (rel. error: "<< v_adapt_rel[GetNrOfBoxes() - I_1 - box_ind_rel] << ")" << std::endl << std::endl;
            }
            vector<In> sin_bounds_rel(mNumModes, I_0);
            In s_box_ind_rel = GetNrOfBoxes() - I_1 - box_ind_rel ;
            mIntegrals[s_box_ind_rel].SetConverged(false);
            v_adapt_abs[s_box_ind_rel] = C_0;
            v_adapt_rel[s_box_ind_rel] = C_0; // relative error will not split the same box
         }
         v_adapt_rel[box_ind_rel] = C_0;
         v_adapt_abs[box_ind_rel] = C_0; // absolute error will not split the same box
      }
   }
   return;
}
      
/***************************************************************************//**
 * Calculate integrals of all boxes contained in the object.
 * Depending on the type of integral quantities needed, either calculated
 * utilizing sum-over-product structure of the potential, or not.
 * The latter is slow for ModeCombi levels of about 3 and larger.
 *
 * Also sets SetTotRhoV(), and prints some output if gPesIoLevel > 9.
 *
 * @note
 *    Actually, if determining ADGA convergence from "norm-like" quantities,
 *    see AdgaConvergenceFromNonSumOverProductQuantities(const PesCalcDef&), it
 *    may still be that the SOP method is used for some other integrals -
 *    otherwise the code doesn't run because some of the SOP integrals are
 *    needed elsewhere, it seems. The overhead from this should hopefully not
 *    be a bottleneck, however. MBH, May 2018.
 *
 * @param[in] aPrint
 *    Whether to print output. This must be set to true for gPesIoLevel > 9 to
 *    have any effect. (NB! Seems redundant to me since it's already handled by
 *    the IO level, but I'll leave it be. -MBH, April 2018)
 * @param[in] arProperty
 *    The properties to be used in the integrand(s).
 * @param[in] aCurrentIter
 *    Only used if calculating non-SOP quantities (see
 *    CalcIntNonSumOverProduct(), GaussIntNonSumOverProduct()). Used for taking
 *    special action if this is the first iteration.
 * @param[in] arNoVibs
 *    The number of vibrational modes
 ******************************************************************************/
void ItGrid::CalcIntForBoxes
   (  const bool& aPrint
   ,  const std::vector<MidasPotential>& arProperty
   ,  const In& aCurrentIter
   ,  const In& arNoVibs
   )
{
   bool do_sop_integrals = AdgaConvergenceFromSumOverProductQuantities(gPesCalcDef);
   bool do_non_sop_integrals = AdgaConvergenceFromNonSumOverProductQuantities(gPesCalcDef);

   //
   if (do_sop_integrals)
   {
      CalcIntSumOverProduct(arProperty);

      // Evaluate the integral over the vibrational density in the interval defined by the basis set boundaries
      if (  GetNmodes() == I_1
         && aPrint 
         )
      {
         // Resize the contianer before evaluating integral values and storing in it
         for (Uin i = I_0; i < mNumProp; ++i)
         {
            mIterTotalDens[i].resize(arNoVibs, C_0);
         }

         // Calculate the intergral over vibrational desnity in the interval defined by basis set boundaries
         CalcExtendedDensInt(arProperty);
      }
   }

   // Old way of doing the calculations. Is currently there to be able to compare.
   if (do_non_sop_integrals)
   {
      if (GetNmodes() > I_2)
      {
         MidasWarning(  "Calculating " + std::to_string(GetNmodes()) +
                        "-mode ADGA integrals, not utilizing SOP potential structure; " +
                        "this can be very time-consuming for mode combination levels of 3 and above."
                     );
      }
      CalcIntNonSumOverProduct
         (  arProperty
         ,  aCurrentIter
         ,  gPesCalcDef.GetAdgaUseHistoricNonSopIntegrals()
         );
   }
   
   //Loop over the property surfaces we are working on
   for (Uin i = I_0; i < mNumProp; ++i)
   {
      SetTotRhoV(TotalDensityPotentialIntegral(i), i);

      // Debug printing
      if (aPrint && (gPesIoLevel > I_9))
      {
         for (Uin j = I_0; j < GetNrOfBoxes(); ++j)
         {
            auto io = [this](Uin ind, const std::string& s, Nb val)->void
               {
                  Mout  << " box " << ind << " " << s << " for the mode combi: "
                        << GetModeCombi() << " is: " << val << '\n';
               };
            io(j, "density", mIntegrals.at(j).GetRhoIntForPropSurfI(i));
            io(j, "potent.", mIntegrals.at(j).GetVintForPropSurfI(i));
            io(j, "rho x V", mIntegrals.at(j).GetRhoVintForPropSurfI(i));

            // This one can only be calculated by non-sum-over-product procedure:
            if (do_non_sop_integrals)
            {
               io(j, "norm rho x V", mIntegrals.at(j).GetNormRhoVintForPropSurfI(i));
            }
            Mout << std::flush;
         }
         auto io = [this](const std::string& s, Nb val)->void
            {
               Mout  << "Total " << s << " for the mode combi: "
                     << GetModeCombi() << " is: " << val << '\n';
            };
         io("density", TotalDensityIntegral(i));
         io("potent.", TotalPotentialIntegral(i));
         io("rho x V", GetTotRhoVForPropSurfI(i));

         // This one can only be calculated by non-sum-over-product procedure:
         if (do_non_sop_integrals)
         {
            io("norm rho x V", TotalNormDensityPotentialIntegral(i));
         }
         Mout << std::flush;
      }
   }
}

/***************************************************************************//**
 * Calculate the integral over a given box with Gauss-Legendre quadrature. Does
 * this by evaluating integrals as multi-dimensional quadratures.
 *
 * @note
 *    For historic reasons, the density, potential, and density*potential
 *    integrals are also calculated. Only if requested are these values saved.
 *    If _only_ these quantities are to be calculated, use instead
 *    CalcIntSumOverProduct(), which uses the SOP structure. If squared
 *    quantities, something like density(potential_i - potential_i-1)^2, are to
 *    be calculated, you need to calculate them as multi-dimensional
 *    quadratures (at the moment, at least). This function then also calculates
 *    the SOP quantities (because I couldn't be bothered to restructure the
 *    function), but only saves them if asked to by the
 *    aSaveHistoricNonSopIntegrals; see manual for how to activate this.
 *    Anyway, I don't think it'll be a huge overhead to calculate these
 *    quantities even though not using them. In any case, consider basing your
 *    ADGA convergence on SOP quantities in stead. - Mads Boettger Hansen,
 *    April 2018.
 *
 * @warning
 *    Very _inefficient_ if the mode combination level becomes high, i.e.
 *    around 3 or larger.
 *
 * @param[in] aPropNo
 *    The number of the property surface we are working on.
 * @param aIndex
 *    Index for the box that is to be integrated over. Used for assigning to
 *    correct elements of ItGrid::mIntegrals.
 * @param aIntervals
 *    Contains the integration interval bounds as a pair.
 * @param aNgauss
 *    The order of the quadrature rule, i.e. number of points and weights
 *    included in the numerical integration.
 * @param aCurrentIter
 *    The current iteration number. If 0, calls
 *    `mIntegrals[aIndex].InitializeRhoV(NquadPoint)`, NquadPoint being the
 *    number of points calculated for the multidimensional quadrature, `N^m`.
 *    Otherwise no importance.
 * @param[in] arProperty
 *    The current property surface we are working on.
 * @param aSaveHistoricNonSopIntegrals
 *    Whether to also save the integrals that _can_ be calculated using
 *    sum-over-product structure (density, potential, density*potential).
 ******************************************************************************/
void ItGrid::GaussIntNonSumOverProduct
   (  Uin aPropNo
   ,  In aIndex
   ,  const std::vector<pair<Nb, Nb> >& aIntervals
   ,  In aNgauss
   ,  In aCurrentIter
   ,  const std::vector<MidasPotential>& arProperty
   ,  bool aSaveHistoricNonSopIntegrals
   )
{
   // Determine the value of Gauss node(-point)s (x_g) and weights (w_g) with the gauleg function
   // n-point Gauss-Legendre quadrature rule, (valid for polynomials of degree 2n-1 or less)
   MidasVector x_g(aNgauss, C_0);
   MidasVector w_g(aNgauss, C_0);
   midas::math::GauLeg(x_g, w_g, -C_1, C_1);
   
   In NquadPoint = In(std::pow(aNgauss, mNumModes));
   MidasVector coeff(NquadPoint, C_1);

   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      In ind = icoef;
      In n_mod = I_1;
      while (n_mod != mNumModes)
      {
         In i = mNumModes - n_mod;
         In nmod_1 = I_1;
         for (In aind = I_0; aind < i; aind++)
         {
            nmod_1 *= aNgauss;
         }
         coeff[icoef] *= w_g[In(ind/nmod_1)];
         ind %= nmod_1;
         n_mod++;
      }
      coeff[icoef] *= w_g[ind];
   }
   
   MidasVector xm(mNumModes, C_0);
   MidasVector xr(mNumModes, C_0);
  
   // The integrals are over a general interval [a,b] which is changed to [-1,1] before applying the Gauss-Legendre quadrature rule but the correct interval is still accounted for by the factors xm and xr
   for (In ia = I_0; ia < mNumModes; ia++)
   {
      xm[ia] = C_I_2*(aIntervals[ia].second + aIntervals[ia].first);
      xr[ia] = C_I_2*(aIntervals[ia].second - aIntervals[ia].first);
   }
   
   std::vector<MidasVector> q_val_vec;
   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      MidasVector q_val(mNumModes, C_0);
      In ind = icoef;
      In n_mod = I_1; 
      while (n_mod != mNumModes)
      {
         In i = mNumModes - n_mod;
         In nmod_1 = I_1;
         for (In aind = I_0; aind < i; aind++)
         {
            nmod_1 *= aNgauss;
         }
         q_val[i] = xm[i] + xr[i]*x_g[In(ind/nmod_1)];
         ind %= nmod_1;
         n_mod++;
      }
      q_val[I_0] = xm[I_0] + xr[I_0]*x_g[In(ind)]; 
      q_val_vec.push_back(q_val);
   }
   if (q_val_vec.size() != NquadPoint)
   {
      MIDASERROR(" Mismatch is size of q_val_vec ");
   }
   
   MidasVector CurrIterPot(NquadPoint, C_0);

   MidasVector CurrIterDens(NquadPoint, C_1);
   arProperty[aPropNo].EvaluatePotential(mModes, q_val_vec, CurrIterPot);

   for (In i_m = I_0; i_m < mNumModes; i_m++)
   {
      MidasVector mode_grid(NquadPoint, C_0);
      for (In i_p = I_0; i_p < NquadPoint; i_p++)
      {
         mode_grid[i_p] = q_val_vec[i_p][i_m];
      }
   
      MidasVector curr_den_vals;
      EvaluateDensity(aPropNo, mModes[i_m], mode_grid, curr_den_vals);

      for (In i_p = I_0; i_p < NquadPoint; i_p++)
      {
         CurrIterDens[i_p] *= curr_den_vals[i_p];
         
         if (CurrIterDens[i_p] < I_0)
         {
            MidasWarning("Strange, you have a negative density, it might be advisable to investigate!");
         }
      }
   }
  
   Nb sz = C_1;
   Nb rho_int = C_0;
   Nb v_int = C_0;
   Nb rho_v_int = C_0;
   Nb squared_rho_v_int = C_0;
   MidasVector rho_v(NquadPoint, C_0);
   if (aCurrentIter == I_0) 
   {
      mIntegrals[aIndex].InitializeRhoV(NquadPoint);
   }
   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      rho_int    += coeff[icoef] * CurrIterDens[icoef];
      v_int      += coeff[icoef] * CurrIterPot[icoef];
      rho_v_int  += coeff[icoef] * CurrIterDens[icoef] * CurrIterPot[icoef];
     
      squared_rho_v_int += coeff[icoef] * std::pow(std::sqrt(CurrIterDens[icoef]) * CurrIterPot[icoef], C_2);
      rho_v[icoef] = std::sqrt(CurrIterDens[icoef]) * CurrIterPot[icoef];
   }
   
   for (In i = I_0; i < mNumModes; i++)
   {
      sz *= (C_2 * xr[i]);
      rho_int *= xr[i];
      v_int *= xr[i];
      rho_v_int *= xr[i];
      squared_rho_v_int *= xr[i];
   }
  
   //mIntegrals[aIndex].SetSize( sz);
   mIntegrals[aIndex].SetSquaredRhoVint(aPropNo, std::sqrt(squared_rho_v_int));
   mIntegrals[aIndex].SetRhoV(aPropNo, rho_v);

   if (aSaveHistoricNonSopIntegrals)
   {
      mIntegrals[aIndex].SetRhoInt(aPropNo, rho_int);
      mIntegrals[aIndex].SetVint(aPropNo, v_int);
      mIntegrals[aIndex].SetRhoVint(aPropNo, rho_v_int);
   }

   // In order to get the proper previous Rho*V values from the correct interval into this norm absolute difference value we need to go through an extra loop
   Nb norm_rho_v_int = C_0;
   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      norm_rho_v_int += coeff[icoef] * std::pow(std::sqrt(CurrIterDens[icoef]) * CurrIterPot[icoef] - mIntegrals[aIndex].GetPrevRhoV(aPropNo)[icoef], C_2);
   }
   for (In i = I_0; i < mNumModes; i++)
   {
      norm_rho_v_int *= xr[i];
   }

   mIntegrals[aIndex].SetNormRhoVint(aPropNo, std::sqrt(norm_rho_v_int));

   return;
}

/**
 * Calculate the integral over a given box with Gauss-Legendre quadrature
 *
 * @param[in] aPropNo
 *    The number of the property surface we are working on.
 * @param aIndex
 *    Index for the box that is to be integrated over
 * @param aExtremes
 *    Contains the integration limits as a pair  
 * @param aDegree
 *    The order of the quadrature rule, i.e. number of points and weights included in the numerical integration
**/
void ItGrid::qGaussInt
   (  Uin aPropNo
   ,  In aIndex
   ,  std::vector<pair<Nb, Nb> >& aExtremes
   ,  In aDegree
   )
{
   MidasVector point(aDegree, C_0); // Gaussian points
   MidasVector weight(aDegree, C_0); // Gaussian weights
   midas::math::GauLeg(point, weight, -C_1, C_1); // Gauss-Legendre quadrature, (numerical integration)

   In NquadPoint = In(std::pow(Nb(aDegree), mNumModes));
   MidasVector coeff(NquadPoint, C_1);

   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      In ind = icoef;
      In n_mod = I_1;
      while (n_mod != mNumModes)
      {
         In i = mNumModes - n_mod;
         In nmod_1 = I_1;
         for (In aind = I_0; aind < i; aind++)
         {
            nmod_1 *= aDegree;
         }
         coeff[icoef] *= weight[In(ind/nmod_1)];
         ind %= nmod_1;
         n_mod++;
      }
      coeff[icoef] *= weight[ind];
   }
   
   MidasVector xm(mNumModes, C_0);
   MidasVector xr(mNumModes, C_0);
   
   for (In ia = I_0; ia < mNumModes; ia++)
   {
      xm[ia] = C_I_2*(aExtremes[ia].second + aExtremes[ia].first);
      xr[ia] = C_I_2*(aExtremes[ia].second - aExtremes[ia].first);
   }
   
   
   std::vector<MidasVector> q_val_vec;
   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      MidasVector q_val(mNumModes, C_0);
      In ind = icoef;
      In n_mod = I_1; 
      while (n_mod != mNumModes)
      {
         In i = mNumModes - n_mod;
         In nmod_1 = I_1;
         for (In aind = I_0; aind < i; aind++)
         {
            nmod_1 *= aDegree;
         }
         q_val[i] = xm[i] + xr[i]*point[In(ind/nmod_1)];
         ind %= nmod_1;
         n_mod++;
      }
      q_val[I_0] = xm[I_0] + xr[I_0]*point[In(ind)]; 
      q_val_vec.push_back(q_val);
   }
   if (q_val_vec.size() != NquadPoint)
   {
      MIDASERROR(" Mismatch is size of q_val_vec ");
   }

   MidasVector density(NquadPoint, C_1);


   for (In i_m = I_0; i_m < mNumModes; i_m++)
   {
      MidasVector mode_grid(NquadPoint, C_0);
      MidasVector den_vals;

      for (In i_p = I_0; i_p < NquadPoint; i_p++)
      {
         mode_grid[i_p] = q_val_vec[i_p][i_m];
      }
      
      EvaluateDensity(aPropNo, mModes[i_m], mode_grid, den_vals);
      
      for (In i_p = I_0; i_p < NquadPoint; i_p++)
      {
         density[i_p] *= den_vals[i_p];
      }
   }

   Nb rho_basis_int = C_0;

   for (In icoef = I_0; icoef < NquadPoint; icoef++)
   {
      rho_basis_int += coeff[icoef] * density[icoef];
   }
   for (In i = I_0; i < mNumModes; i++)
   {
      rho_basis_int *= xr[i];
   }

   mIntegrals[aIndex].SetRhoBasisInt(aPropNo, rho_basis_int);
   
   return;
}

/**
 * Check if a displacement has to be computed
**/
bool ItGrid::CheckDisplacement
   (  const vector<In>& arKvec
   ,  bool arZero
   )  const
{
   //check first options
   if (arKvec.size()>1 && gPesCalcDef.GetmPesCalcPotFromOpFile() && arZero)
   {
      for (In i_s=0; i_s<arKvec.size(); ++i_s)
      {
         if (arKvec[i_s]==0) return false;
      }
   }

   for (In i_m=I_0; i_m<mNumModes; ++i_m)
   {
      vector<In>::const_iterator Vii=find(mBounds[i_m].begin(),mBounds[i_m].end(),arKvec[i_m]);
      if (Vii==mBounds[i_m].end())   
      {
         return false;
      }
   }
   return true;
}

/**
 * resize the container for the integrals 
**/
void ItGrid::ResizeIntegrals
   (  In aSize
   )
{
   mIntegrals.clear();
   for (In i = I_0; i < aSize; ++i)
   {
      mIntegrals.emplace_back(mNumProp);
   }
   if (AdgaConvergenceFromNonSumOverProductQuantities(gPesCalcDef))
   {
      for (In i_b = I_0; i_b < GetNrOfBoxes(); i_b++)
      {
         mIntegrals[i_b].InitializeRhoV(In(std::pow(mGaussPoints, mNumModes)));
      }
   }
}

/**
 * Update the grid of boxes for a particular mode combination
**/
void ItGrid::UpdateGrid()
{
   if (gPesIoLevel > I_10)
   {
      In n_box = I_1;
      // Determine the number of integration boxes for the given mode combination
      for (In i = I_0; i < mNumModes; ++i)
      {
         n_box *= (mBounds[i].size() - I_1);
      }

      Mout << " Updating grid for mc: " << GetModeCombi() << "  ";
      Mout << " Nr. of boxes before: " << n_box;
   }
   
   //Split all unconverged boxes
   for(In i = 0; i < mIntegrals.size(); ++i)
   {
      if(mIntegrals[i].GetConverged() == false)
      {
         vector<In> bounds(mNumModes, I_0);
         GetBoxIndex(i, bounds);
         SplitBox(bounds);
      }
   }

   // We now merge the current and updated, i.e. extended or sub-divided box bounds into a temporary list in order to collect all unique box bounds for the individual mode combination to be used in the next Adga iteration 
   for (In imod = I_0; imod < mNumModes; imod++)
   {
      std::list<In> tmp_bound;

      // mBounds contains the current box bounds for all mode combinations
      for (In i = I_0; i < mBounds[imod].size(); i++)
      {
         tmp_bound.push_back(mBounds[imod][i]);
      }
      mBounds[imod].clear();

      // mNewBounds contains the box bounds for all mode combinations to be used in the next Adga iteration
      for (In i = I_0; i < mNewBounds[imod].size(); i++)
      {
         tmp_bound.push_back(mNewBounds[imod][i]);
      }
      mNewBounds[imod].clear();

      // Sort the list and remove all duplicate elements
      tmp_bound.sort();
      tmp_bound.unique();
      
      // Now save the information on box bounds to be used for the next Adga iteration
      for (std::list<In>::iterator Lpp = tmp_bound.begin(); Lpp != tmp_bound.end(); Lpp++)
      {
         mBounds[imod].push_back(*Lpp);
      }
   }
   // After the sorting the integrals lost their "owner"
   mIntegrals.clear();
   
   if (gPesIoLevel > I_10)
   {
      In n_box_af = I_1;
      for (In i = I_0; i < GetNmodes(); i++)
      {
         n_box_af *= (mBounds[i].size() - I_1);
      }

      Mout << " after: " << n_box_af << std::endl;
   }

   return;
}

/**
 * append new bounds
 *
 * @param aMode mode for which to append
 * @param arNewBounds new bounds to be added
**/
void ItGrid::Append
   (  const In& aMode
   ,  const std::vector<In>& arNewBounds
   )
{
   for (In i = I_0; i < arNewBounds.size(); ++i)
   {
      mNewBounds[aMode].emplace_back(arNewBounds[i]);
   }
   return;
}



/**
* Reset the grid of boxes after they have been used in the shepard extrapolation, 
  their points useful for eventual shepard interpolation are save in mGhosts...
**/
void ItGrid::ResetGrid()
{
   In n_box = I_1;
   for (In i = I_0; i < mNumModes; i++)
   {
      n_box *= (mBounds[i].size() - I_1);
   }

   if (gPesIoLevel > I_10)
   {
      Mout << " Updating grid for mc: " << GetModeCombi() << "  ";
      Mout << " Nr. of boxes before: " << n_box;
   }

   for (In imod = I_0; imod < mNumModes; imod++)
   {
      std::list<In> tmp_bound;
      // Merge the "old" and "new" bounds in a temporary list
      for (In i = I_0; i < mBounds[imod].size(); i++)
      {
         tmp_bound.push_back(mBounds[imod][i]);
      }
      for (In i = I_0; i < mBounds[imod].size(); i++)
      {
         mGhosts[imod].push_back(mBounds[imod][i]);   
      }
      mBounds[imod].clear();

      tmp_bound.sort();
      tmp_bound.unique();

      mBounds[imod].push_back(tmp_bound.front());
      mBounds[imod].push_back(I_0);
      mBounds[imod].push_back(tmp_bound.back());
   }
   // After the sorting the integrals lost their "owner"
   mIntegrals.clear();
   In n_box_af=I_1;
   for (In i=I_0; i<GetNmodes(); i++) n_box_af*=(mBounds[i].size()-I_1);

   if (gPesIoLevel > I_10)
   {
      Mout << " after: " << n_box_af << std::endl;
   
      for (In imod = I_0; imod < mNumModes; imod++)
      {
         Mout << " Bounds of mode " << imod << ": " << mBounds[imod] << std::endl;
      }
   }
   return;
}

/**
 *
 **/
void ItGrid::GetKvecsVec
   (  std::vector<std::vector<In> >& arKvecsVec
   )  const
{
   In n_modes=GetNmodes();
   vector<In> occ_min;
   vector<In> occ_max;
   In n_dim=1;
   for (In i_m=0; i_m<n_modes; ++i_m)
   {
      if (gPesIoLevel > I_10)
      {
         Mout << mModes << "  Bounds of mode " << mModes[i_m] << "   " << mBounds[i_m] << std::endl;
      }
      In n_bnd = GetNbounds(i_m);
      occ_min.push_back(0);
      occ_max.push_back(n_bnd-1);
      n_dim*=n_bnd;
   }
   if (gPesIoLevel > I_10)
   {
      Mout << " expected dimension: " << n_dim << std::endl;
   }
   string lowhig="LOWHIG";
   bool exci_only=false;
   MultiIndex ind_g(occ_min,occ_max,lowhig,exci_only);
   if (ind_g.Size()!=n_dim) MIDASERROR(" Something wrong in ItGrid::GetKvecsVec ");
   for (In i_d=0; i_d<n_dim; ++i_d)
   {
      vector<In> i_k_vec(ind_g.VecSize());
      ind_g.IvecForIn(i_k_vec,i_d);
      vector<In> curr_k_vec(n_modes);
      for (In i_k=0; i_k<i_k_vec.size(); ++i_k)
         curr_k_vec[i_k]=GetBound(i_k_vec[i_k],i_k);
      bool add=true;
      if (add) arKvecsVec.push_back(curr_k_vec);
   }
   return;
}

/**
 * Overloading for output stream
 * 
 * @param arOut   The output stream.
 * @param arBoxes The grid boxes to output.
 *
 * @return Returns the ostream for chaining of operator<<.
 **/
std::ostream& operator<<
   (  std::ostream& arOut
   ,  const ItGrid& arBoxes
   )
{
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(ios::showpoint);
   arOut << std::endl;
   arOut << " Details of object boxes: " << std::endl;
   arOut << " Mode(s) in the mode-combination: "  << arBoxes.mModes <<  std::endl;
   arOut << " Obtained as Direct product of the points:" <<  std::endl;
   for (In i = I_0; i < arBoxes.mNumModes; i++)
   {
      arOut << "  Mode: " << arBoxes.mModes[i] << "  " << arBoxes.mBounds[i] << std::endl; 
   }
   
   string divid = "false";
   if (arBoxes.mConverged)
   {
      divid = "true";
   }
   arOut << " Converged:  " << divid << std::endl;
   return arOut;
}

/***************************************************************************//**
 * Evaluate 1-mode density (in ItGrid::mDensities) at the points given as
 * argument.
 *
 * @param[in] aPropNo
 *    The number of the property surface we are working on.
 * @param[in] aMode
 *    The mode for which to evaluate the density.
 * @param[in] arQvals
 *    The coordinate input values at which the density is evaluated.
 * @param[out] arDenVals
 *    On output will hold the density values. Resized to fit size of arQvals.
 ******************************************************************************/

void ItGrid::EvaluateDensity
   (  Uin aPropNo
   ,  In aMode
   ,  const MidasVector& arQvals
   ,  MidasVector& arDenVals
   )  const
{
   //No loop over property surfaces here, because this is called in qGaussInt, which is called in CheckExtension, which is looped over.
   const std::vector<Nb>& pot_val  = mDensities.GetPotValues(aPropNo, aMode);
   const std::vector<Nb>& grid_val = mDensities.GetGridValues(aPropNo, aMode);
   
   In n_pts = pot_val.size();
   MidasVector grid(I_0, C_0);
   MidasVector dens_vals(I_0, C_0);
   grid.SetNewSize(n_pts);
   dens_vals.SetNewSize(n_pts);
   for (In j = I_0; j < n_pts; j++)
   {
      grid[j] = grid_val[j];
      dens_vals[j] = pot_val[j];
   }  

   // Use grid.Size() interpolation points at most to avoid out-of-range.
   const In int_pts = std::min(mNumPointsPolynomialInterpol, static_cast<Uin>(grid.Size()));

   arDenVals.SetNewSize(arQvals.Size(), false);
   arDenVals.Zero();

   In jlo = I_0; // Initialize here; hunt will use value from previous iter as guess.
   for (In i_p = I_0; i_p < arQvals.Size(); i_p++)
   {
      Nb eps;
      MidasVector x_red(int_pts, C_0);
      MidasVector y_red(int_pts, C_0); //give the degree in input
      PesFuncs::hunt(grid, arQvals[i_p], jlo);
      In k  =  std::min (  std::max (  jlo - (int_pts - I_1)/I_2
                                    ,  I_0
                                    )
                        ,  static_cast<In>((grid.Size() - int_pts))
                        );
      for (In j = I_0; j < int_pts; j++)
      {
         x_red[j] = grid[j + k];
         y_red[j] = dens_vals[j + k];
      }
      Nb funcx = C_0;
      PesFuncs::polint(x_red, y_red, arQvals[i_p], funcx, eps);
      arDenVals[i_p] = funcx;
   }

   return;
}

/***************************************************************************//**
 * Evaluate 1-mode density (in ItGrid::mDensities) at the point given as
 * argument.
 *
 * @param[in] aPropNo
 *    The number of the property surface we are working on.
 * @param[in] aMode
 *    The mode for which to evaluate the density.
 * @param[in] aQval
 *    The coordinate input value at which the density is evaluated.
 ******************************************************************************/
Nb ItGrid::EvaluateDensity
   (  Uin aPropNo
   ,  In aMode
   ,  Nb aQval
   )  const
{
   MidasVector input_val(I_1, aQval);
   MidasVector output_val;
   EvaluateDensity(aPropNo, aMode, input_val, output_val);
   return output_val[I_0];
}

/**
 * Get a MidasVector of bounds for the aMode
**/
void ItGrid::GetBounds
   (  In aMode
   ,  MidasVector& arBounds
   )  const
{
   if (aMode >= mBounds.size()) 
   {
      MIDASERROR(" Mismatch in the mode address in GetBounds");
   }
   
   In n_b = mBounds[aMode].size();
   arBounds.SetNewSize(n_b, false);
   
   for (In i = I_0; i < n_b; i++)
   {
      arBounds[i] = mBounds[aMode][i];
   }
   
   return;
}

/**
 * Get a vector of bounds for the aMode
 **/
void ItGrid::GetBounds
   (  In aMode
   ,  std::vector<In>& arBounds
   )  const
{
   if (aMode >= mBounds.size())
   {
      MIDASERROR(" Mismatch in the mode address in GetBounds");
   }
   In n_b = mBounds[aMode].size();
   arBounds.clear();
   for (In i = I_0; i < n_b; i++)
   {
      arBounds.push_back(mBounds[aMode][i]);
   }
   return;
}

/**
 * Get a vector of bounds for the aMode
 **/
void ItGrid::GetQbounds
   (  In aMode
   ,  std::vector<Nb>& arBounds
   )  const
{
   if (aMode >= mBounds.size())
   {
      MIDASERROR("Mismatch in the mode address in GetBounds");
   }

   In n_b = mBounds[aMode].size();
   arBounds.clear();
   for (In i = I_0; i < n_b; i++)
   {
      auto pot_bound = mBounds[aMode][i];
      if (pot_bound < C_0)
      {
         arBounds.push_back(pot_bound * mQlScal[aMode]);
      }
      else
      {
         arBounds.push_back(pot_bound * mQrScal[aMode]);
      }
   }
   return;
}

/**
 *
 **/
bool ItGrid::IsItAbound
   (  In aInt  
   ,  In aMode
   )  const
{
   for (In i = I_0; i < mBounds[aMode].size(); ++i) 
   {
      if (mBounds[aMode][i] == aInt)
      {
         return true;
      }
   }

   return false;
}

/**
 *
 **/
void ItGrid::FillXigGhosts
   (  std::vector<std::vector<In> >& arInXig
   )  const
{
   std::vector<std::vector<In> > available_ghost(mNumModes);

   for (In i = I_0; i < mNumModes; ++i)
   {
      if (gPesIoLevel > I_11) 
      {
         Mout << "Ghosts of mode " << i << "   " << mGhosts[i] << std::endl;
      }
      if (gPesIoLevel > I_11)
      {
         Mout << "Bounds of mode " << i << "   " << mBounds[i] << std::endl << std::endl;
      }

      for (In j = I_0; j < mGhosts[i].size(); ++j)
      {
         if (!IsItAbound(mGhosts[i][j], i))
         {
            available_ghost[i].push_back(mGhosts[i][j]);
         }
      }
   }
   
   if (gPesIoLevel > I_11)
   {
      for (In i = I_0; i < mNumModes;++i)
      {
         Mout << "Available ghosts  " << available_ghost[i] << std::endl;
      }
   }
   
   // zeros(?) will not be present in the available_ghost we had them in order to generate the correct vectors
   for (In i = I_0; i < mNumModes; ++i)
   {
      available_ghost[i].push_back(I_0);
   }
   
   // brute force generation
   In total = I_1;
   for (In i = I_0; i < mNumModes; ++i)
   {
      total *= available_ghost[i].size();
   }

   std::vector<std::vector<In> > tmp_list;
   for (In index = I_0; index < total; ++index)
   {
      std::vector<In> tmp_vect;
      In m = index;
         
      for (In j = I_0; j < mNumModes; ++j)
      {
         In n = m%available_ghost[j].size();
         m /= available_ghost[j].size();
         tmp_vect.push_back(available_ghost[j][n]);
      }
      
      bool one_zero = false;
      bool too_many_zeros = false;
      
      for (In j = I_0; j < mNumModes; ++j)
      {
         if (tmp_vect[j] == I_0) 
         {
            one_zero = true;
            for (In k = j + I_1; k < mNumModes; ++k)
            {
               if (tmp_vect[k] == I_0)
               {
                  too_many_zeros = true;
               }
            }
         }
      }
      if (one_zero && !too_many_zeros)
      {
         tmp_list.push_back(tmp_vect);
      }
   }

   // loop over the generated vectors
   for (In i = I_0; i < tmp_list.size(); ++i)
   {
      arInXig.push_back(tmp_list[i]);
   }
   
   return;
}

/***************************************************************************//**
 * @note
 *    Slightly/rather inefficiently implemented, due to need for conversion
 *    between std::vector<In> and std::vector<Uin> (the former used by
 *    GetBoxIndex(In aBox, std::vector<In>&)).
 * @param[in] aBox
 *    The serial index of the box of interest.
 * @return
 *    The multi-index for that box.
 ******************************************************************************/
std::vector<Uin> ItGrid::GetBoxIndex
   (  Uin aBox
   )  const
{
   std::vector<In> v(mNumModes, I_0);
   GetBoxIndex(aBox, v);
   return std::vector<Uin>(v.begin(), v.end());
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if either of aModeIndex or aIntervalIndex is out of range.
 *
 * @param[in] aModeIndex
 *    The mode in question.
 * @param[in] aIntervalIndex
 *    The index. Must be `>= 0` and `< mBounds[aModeIndex].size()-1`.
 * @return
 *    Pair with lower and upper bound of the `aIntervalIndex`th interval of the mode.
 ******************************************************************************/
std::pair<Nb,Nb> ItGrid::GetQBoundsPair
   (  Uin aModeIndex
   ,  Uin aIntervalIndex
   )  const
{
   try
   {
      auto left_box_bound = mBounds.at(aModeIndex).at(aIntervalIndex);
      auto right_box_bound = mBounds.at(aModeIndex).at(aIntervalIndex + I_1);
      if (left_box_bound < C_0 && right_box_bound <= C_0)
      {
         return std::make_pair
            (  mQlScal.at(aModeIndex) * left_box_bound
            ,  mQlScal.at(aModeIndex) * right_box_bound
            );
      }
      else if (left_box_bound >= C_0 && right_box_bound > C_0)
      {
         return std::make_pair
            (  mQrScal.at(aModeIndex) * left_box_bound
            ,  mQrScal.at(aModeIndex) * right_box_bound
            );
      }
   }
   catch (const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: " + std::string(oor.what()));
      return std::pair<Nb, Nb>();
   }

   // Should never get to here 
   return std::pair<Nb, Nb>();
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if aModeIndex is out-of-range. Also something bad will
 *    probably happen if ItGrid::mBounds.at(aModeIndex).size() is 0; hopefully
 *    this will make GetQBoundsPair() throw a MIDASERROR.
 *
 * @param[in] aModeIndex
 *    The mode in question.
 * @return
 *    Vector with all pairs of bounds for that mode.
 ******************************************************************************/
std::vector<std::pair<Nb,Nb>> ItGrid::GetQBoundsPairsOfMode
   (  Uin aModeIndex
   )  const
{
   std::vector<std::pair<Nb,Nb>> v;
   std::size_t size = I_0;
   try
   {
      size = mBounds.at(aModeIndex).size() - I_1;
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
   }
   v.reserve(size);
   for(Uin i = I_0; i < size; ++i)
   {
      v.push_back(GetQBoundsPair(aModeIndex, i));
   }
   return v;
}

/***************************************************************************//**
 * If the grid points of each mode of the object (as defined by
 * ItGrid::mBounds and ItGrid::mQscal) are
 *       p0 p1 p2 ...
 *       q0 q1 q2 ...
 *       r0 r1 r2 ...
 * (p, q, r referring to different modes), the object returns
 *       {{{p0,p1},  {p1,p2},... },
 *        {{q0,q1},  {q1,q2},... },
 *        {{r0,r1},  {r1,r2},... },
 *       }
 *
 * @return
 *    Vector with vectors containing boundary pairs for each interval for each
 *    mode.
 ******************************************************************************/
std::vector<std::vector<std::pair<Nb,Nb>>> ItGrid::IntervalBoundsForEachMode
   (
   )  const
{
   std::vector<std::vector<std::pair<Nb,Nb>>> v;
   v.reserve(mBounds.size());
   for(Uin m = I_0; m < mBounds.size(); ++m)
   {
      v.push_back(GetQBoundsPairsOfMode(m));
   }
   return v;
}

/***************************************************************************//**
 * "Driver" for calculating all the integrals that can be evaluated efficiently
 * by utilizing sum-over-product structure.
 *
 * Updates relevant parts of ItGrid::mIntegrals.
 *
 * @see
 *    ItGridBox::SetRhoInt, ItGridBox::SetVint, ItGridBox::SetRhoVint.
 *
 * @note
 *    The densities use non-scaled coordinates, but are constructed with
 *    scaling factors that are inverses of the ones used for the potential.
 *    This is to counteract the potential scaling factors applied to both
 *    densities and potential functions when evaluating density times potential
 *    integrals.
 *
 * @param[in] arPot
 *    The potential to be used in the integrand(s).
 ******************************************************************************/

//The function gets a vector of all potentials
void ItGrid::CalcIntSumOverProduct
   (  const std::vector<MidasPotential>& arPot
   )
{
   using namespace midas::pes::integrals;

   // Intervals.
   const auto& intervals = IntervalBoundsForEachMode();

   for (Uin i = I_0; i < mNumProp; ++i)
   {
      // Potential.
      const auto& pot_sc_facts = arPot[i].ScalingFactorsForModeCombi(GetModeCombi());
      const auto& pot_coefs = arPot[i].CoefficientsForModeCombi(GetModeCombi());
      const auto& pot_funcs = arPot[i].FunctionsForModeCombi(GetModeCombi());

      // Densities.
      std::vector<std::vector<OneModeDensityInterpolator>> densities =
      {  
         // DensityInterpolatorsForModes with the index of the property surface we are working on 
         DensityInterpolatorsForModes
            (  mDensities
            ,  i
            ,  GetModeCombi()
            ,  mNumPointsPolynomialInterpol
            ,  pot_sc_facts
            ,  true
            )
      };

      // Evaluations.
      CalcAndUpdateBoxSizes();

      // Get the property number
      CalcAndUpdateDensIntegrals(i, intervals, densities, pot_sc_facts);
      CalcAndUpdatePotIntegrals(i, intervals, pot_coefs, pot_funcs, pot_sc_facts);
      CalcAndUpdateDensPotIntegrals(i, intervals, densities.front(), pot_coefs, pot_funcs, pot_sc_facts);
   }
}

/***************************************************************************//**
 * Loops through the boxes of the inherent ModeCombi and calculates all box
 * sizes/volumes, updating ItGrid::mIntegrals (ItGridBox::SetSize() along
 * the way.
 ******************************************************************************/
void ItGrid::CalcAndUpdateBoxSizes
   (
   )
{
   std::vector<In> v_ind(GetNmodes(), I_0);
   for(Uin i = I_0; i < GetNrOfBoxes(); ++i)
   {
      GetBoxIndex(i, v_ind);
      try
      {
         //mIntegrals.at(i).SetSize(GetBoxSize(v_ind));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      }
   }
}

/***************************************************************************//**
 * Uses ModeCombiIntegrals and SumOverProductIndexer to calculate integrals
 * efficiently as sums-over-products of quadratures, not quadratures of
 * sums-over-products.
 *
 * Updates relevant parts of ItGrid::mIntegrals.
 *
 * @see
 *    ItGridBox::SetRhoInt, ItGridBox::SetVint, ItGridBox::SetRhoVint.
 ******************************************************************************/
void ItGrid::CalcAndUpdateDensIntegrals
   (  const Uin& aPropNo
   ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
   ,  const std::vector<std::vector<midas::pes::integrals::OneModeDensityInterpolator>>& arDens
   ,  const std::vector<Nb>& arScalingFactors
   )
{
   using namespace midas::pes::integrals;
   using func_t = OneModeDensityInterpolator;

   ModeCombiIntegrals<func_t> mci
      (  SumOverProductIndexer<func_t>
            (  std::vector<Nb>{C_1}
            ,  arDens
            ,  [](const func_t& a, const func_t& b)->bool {return a == b;}
            )
      ,  arIntervals
      ,  mGaussPoints
      ,  arScalingFactors
      );

   for (Uin j = I_0; j < GetNrOfBoxes(); ++j)
   {
      try
      {
         mIntegrals.at(j).SetRhoInt(aPropNo, mci.Integral(GetBoxIndex(j)));
      }
      catch (const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      }
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
void ItGrid::CalcAndUpdatePotIntegrals
   (  const Uin& aPropNo
   ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
   ,  const std::vector<Nb>& arCoefficients
   ,  const std::vector<std::vector<MidasFunctionWrapper<Nb>>>& arFunctions
   ,  const std::vector<Nb>& arScalingFactors
   )
{
   using namespace midas::pes::integrals;
   using func_t = MidasFunctionWrapper<Nb>;

   ModeCombiIntegrals<func_t> mci
      (  SumOverProductIndexer<func_t>
            (  arCoefficients
            ,  arFunctions
            ,  [](const func_t& a, const func_t& b)->bool {return a.Compare(b);}
            )
      ,  arIntervals
      ,  mGaussPoints
      ,  arScalingFactors
      );

   for(Uin i = I_0; i < GetNrOfBoxes(); ++i)
   {
      try
      {
         mIntegrals.at(i).SetVint(aPropNo, mci.Integral(GetBoxIndex(i)));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      }
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
void ItGrid::CalcAndUpdateDensPotIntegrals
   (  const Uin& aPropNo
   ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
   ,  const std::vector<midas::pes::integrals::OneModeDensityInterpolator>& arDens
   ,  const std::vector<Nb>& arCoefficients
   ,  const std::vector<std::vector<MidasFunctionWrapper<Nb>>>& arFunctions
   ,  const std::vector<Nb>& arScalingFactors
   )
{
   using namespace midas::pes::integrals;
   using func_t = OneModeDensityTimesFunction;

   ModeCombiIntegrals<func_t> mci
      (  SumOverProductIndexer<func_t>
            (  arCoefficients
            ,  CombineDensitiesAndFunctions(arDens, arFunctions)
            ,  [](const func_t& a, const func_t& b)->bool {return a == b;}
            )
      ,  arIntervals
      ,  mGaussPoints
      ,  arScalingFactors
      );

   for(Uin i = I_0; i < GetNrOfBoxes(); ++i)
   {
      try
      {
         mIntegrals.at(i).SetRhoVint(aPropNo, mci.Integral(GetBoxIndex(i)));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      }
   }
}

/**
 *
**/
void ItGrid::CalcExtendedDensInt
   (  const std::vector<MidasPotential>& arPot
   )
{
   using namespace midas::pes::integrals;

   // Intervals.
   const auto& intervals = IntervalBoundsForEachMode();

   for (Uin i = I_0; i < mNumProp; ++i)
   {
      // Potential.
      const auto& pot_sc_facts = arPot[i].ScalingFactorsForModeCombi(GetModeCombi());
      const auto& pot_coefs = arPot[i].CoefficientsForModeCombi(GetModeCombi());
      const auto& pot_funcs = arPot[i].FunctionsForModeCombi(GetModeCombi());

      // Densities.
      std::vector<std::vector<OneModeDensityInterpolator>> densities =
      {  
            // DensityInterpolatorsForModes with the index of the property surface we are working on 
            DensityInterpolatorsForModes
               (  mDensities
               ,  i
               ,  GetModeCombi()
               ,  mGaussPoints * I_2
               ,  pot_sc_facts
               ,  true
               )
      };

      // Extend the grid of single points to include all points between the potential boundaries and the two points
      // that indicate the basis set boundaries for each mode. From this setup we then extract the evaluated integral values.
      auto total_dens_int_val = C_0;
      
      // Hold basis bounds
      std::vector<std::pair<Nb, Nb> > basis_box_bounds;
      basis_box_bounds.reserve(I_2);

      if (!gPesCalcDef.GetmMeanDensOnFiles())
      {
         // Determine the correct primitive basis to extract the basis set boundaries
         std::string basis_name = gVscfCalcDef[I_0].Basis();
         In i_basis = -I_1;
         for (In j = I_0; j < gBasis.size(); ++j) 
         {
            if (gBasis[j].GetmBasName() == basis_name)
            {
               i_basis = j;
            }
         }
         if (i_basis == -I_1)
         {
            MIDASERROR("Basis not found in VscfDrv");
         }
         if (gBasis[i_basis].GetmUseBsplineBasis())
         {
            for (In imode = I_0; imode < mModes.size(); ++imode)
            {
               basis_box_bounds.push_back(gBasis[i_basis].GetBasDefForGlobalMode(mModes[imode]).GetBasisGridBounds());
            }
         }
      }
      else
      {
         // Use basis set boundaries from the mean densities on file
         for (In imode = I_0; imode < mModes.size(); ++imode)
         {
            const std::vector<Nb>& grid_val = mDensities.GetGridValues(i, imode);
            basis_box_bounds.push_back(std::make_pair(*grid_val.begin(), *grid_val.end()));
         }
      }
      
      //
      std::vector<std::vector<std::pair<Nb,Nb> > > surface_box_bounds;
      surface_box_bounds.reserve(mBounds.size());
      for (Uin m = I_0; m < mBounds.size(); ++m)
      {
         surface_box_bounds.push_back(GetQBoundsPairsOfMode(m));
      }
      
      // Collect the extended box boundaries in a single vector
      std::vector<std::pair<Nb, Nb> > extended_box_bounds;
      extended_box_bounds.reserve(surface_box_bounds.size() + basis_box_bounds.size());
      extended_box_bounds.push_back(std::make_pair(basis_box_bounds.front().first, surface_box_bounds[I_0].front().first));
      extended_box_bounds.insert(std::end(extended_box_bounds), std::begin(surface_box_bounds[I_0]), std::end(surface_box_bounds[I_0]));
      extended_box_bounds.push_back(std::make_pair(surface_box_bounds[I_0].back().second, basis_box_bounds.back().second));
      
      // Create the complete box boundaries container
      std::vector<std::vector<std::pair<Nb,Nb> > > complete_box_bounds;
      complete_box_bounds.reserve(mBounds.size());
      complete_box_bounds.push_back(extended_box_bounds);
      
      // Collect the total evaluated integral values
      using namespace midas::pes::integrals;
      using func_t = OneModeDensityInterpolator;
      ModeCombiIntegrals<func_t> mci
         (  SumOverProductIndexer<func_t>
               (  std::vector<Nb>{C_1}
               ,  densities
               ,  [](const func_t& a, const func_t& b)->bool {return a == b;}
               )
         ,  complete_box_bounds
         ,  mGaussPoints * I_2
         ,  pot_sc_facts
         );
      
      for (Uin k = I_0; k < complete_box_bounds[I_0].size(); ++k)
      {
         total_dens_int_val += mci.Integral(GetBoxIndex(k));
      }
      
      // Store the density value
      mIterTotalDens[i][(GetModeCombi()[I_0])] = total_dens_int_val;
   }
}

/***************************************************************************//**
 * Loops over all boxes (sub-hypervolumes) of this object, then for each of
 * them calculates integrals as multidimensional quadratures.
 *
 * @param[in] arProperty
 *    The properties to be used in the integrand(s).
 * @param[in] aCurrentIter
 *    The current ADGA iteration, for taking special action if this is the
 *    first iteration, see GaussIntNonSumOverProduct().
 * @param[in] aSaveHistoricNonSopIntegrals
 *    Whether to save those integrals that could have actually been calculated
 *    using CalcIntSumOverProduct(). For enabling historic behaviour. See
 *    manual.
 ******************************************************************************/
void ItGrid::CalcIntNonSumOverProduct
   (  const std::vector<MidasPotential>& arProperty
   ,  In aCurrentIter
   ,  bool aSaveHistoricNonSopIntegrals
   )
{
   for (Uin i = I_0; i < mNumProp; ++i)
   {
      for (In i_b = I_0; i_b < GetNrOfBoxes(); i_b++)
      {
         std::vector<In> in_bounds(mNumModes, I_0);
         GetBoxIndex(i_b, in_bounds);
         
         std::vector<std::pair<Nb, Nb> > q_bounds(mNumModes);
         GetQbox(in_bounds, q_bounds); // q_bounds is now a vector containing adjacent points from the Points file as pairs, i.e. gives an easy definition of distances to integrate over
  
         GaussIntNonSumOverProduct
            (  i
            ,  i_b
            ,  q_bounds
            ,  mGaussPoints
            ,  aCurrentIter
            ,  arProperty
            ,  aSaveHistoricNonSopIntegrals
            );
      }
   }
}

/***************************************************************************//**
 * Intended as a uniform interface for calculating total integrals (for the
 * ModeCombi of this object) over quantities stored in ItGrid::mIntegrals.
 *
 * @note
 *    Apparently it was not a problem to implement this function with template
 *    argument in the .cc file. I guess it's because the function is private,
 *    thus will only ever be called within this one compilation unit. I
 *    couldn't (immediately) find anything about best practices in such a case,
 *    so I hope this is find. If anyone finds out it's not optimal, feel free
 *    to change. -MBH, April 2018
 *
 * @param[in] arIntegralForBoxIndex
 *    Functor with interface `Nb operator()(Uin)`, that takes a box index, i.e.
 *    in the range [0, GetNrOfBoxes()], and returns a value.
 * @param[in] aPropNo
 *    The number of the property surface we are working on.
 * @return
 *    The sum of the values returned by arIntegralForBoxIndex for all the
 *    possible box indices of this object.
 ******************************************************************************/
template<class Func>
Nb ItGrid::TotalIntegral
   (  const Func& arIntegralForBoxIndex
   ,  Uin aPropNo
   )  const
{
   Nb sum = C_0;
   for(Uin i = I_0; i < GetNrOfBoxes(); ++i)
   {
      sum += arIntegralForBoxIndex(i,aPropNo);
   }
   return sum;
}

/***************************************************************************//**
 * @return
 *    The total integral of the specified quantity for the ModeCombi of this
 *    object (i.e. the sum over the quantity for all boxes of this object).
 ******************************************************************************/
Nb ItGrid::TotalDensityIntegral
   (  Uin aPropNo
   )  const
{
   return TotalIntegral([this](Uin i, Uin aPropNo)->Nb {return mIntegrals[i].GetRhoIntForPropSurfI(aPropNo);}, aPropNo);
}
Nb ItGrid::TotalPotentialIntegral
   (  Uin aPropNo
   )  const
{
   return TotalIntegral([this](Uin i, Uin aPropNo)->Nb {return mIntegrals[i].GetVintForPropSurfI(aPropNo);}, aPropNo);
}
Nb ItGrid::TotalDensityPotentialIntegral
   (  Uin aPropNo
   )  const
{
   return TotalIntegral([this](Uin i, Uin aPropNo)->Nb {return mIntegrals[i].GetRhoVintForPropSurfI(aPropNo);}, aPropNo);
}
Nb ItGrid::TotalNormDensityPotentialIntegral
   (  Uin aPropNo
   )  const
{
   return TotalIntegral([this](Uin i, Uin aPropNo)->Nb {return mIntegrals[i].GetNormRhoVintForPropSurfI(aPropNo);}, aPropNo);
}
