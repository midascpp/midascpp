/**
************************************************************************
*
*  @file                ItGridBox.h
* 
*  Created:             17/10/2007
* 
*  Author:              D. Toffoli (toffoli@chem.au.dk) and M. Sparta (msparta@chem.au.dk)
* 
*  Short Description:   Declares class ItGridBox for storing the
*                       integral values for the iterative evaluation
*                       of the monodimensiona PES  
* 
*  Last modified:       24/10/2007
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef ITGRIDBOX_H
#define ITGRIDBOX_H

//standard headers
#include <vector>
#include <string>
#include <iostream>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"


class ItGridBox 
{
   private:
      //! Number of property surfaces for which to store information
      In mNumProp;
      //! Size (length/area/volume/... ) of the box
      //Nb mSize;
      //! Is the box converged?
      bool mConverged;                 
      //! Value of the integral of rho in the interval
      std::vector<Nb> mRhoInt; 
      //! Previous value of the integral of rho in the interval
      std::vector<Nb> mPrevRhoInt;        
      //! Value of the integral of rho in the interval between potential and basis bounds
      std::vector<Nb> mRhoBasisInt;          
      //! Value of the integral of the potential in the interval
      std::vector<Nb> mVint;                 
      //! Previous value of the integral of the potential in the interval
      std::vector<Nb> mPrevVint;             
      //! Value of the integral of rho * potential in the interval
      std::vector<Nb> mRhoVint;              
      //! Previous value of the integral of rho * potential in the interval
      std::vector<Nb> mPrevRhoVint;          
      //! The value of the integral over (mRhoV - mPrevRhoV)^2 in the interval
      std::vector<Nb> mNormRhoVint;          
      //! The value of the integral over (mRhoV)^2 in the interval
      std::vector<Nb> mSquaredRhoVint;       
      //! Value of non-integrated rho * potential in interval
      std::vector<MidasVector> mRhoV;        
      //! Previous value of non-integrated rho * potential in interval
      std::vector<MidasVector> mPrevRhoV;    
      
      ItGridBox() = delete;
      
      //Nb GetSize() const {return mSize;}                                         ///< Size of the box
      Nb GetSquaredRhoVint(Uin aPropNo) const {return mSquaredRhoVint[aPropNo];}  ///< Current int. value of (mRhoV)^2
      
      Nb GetPrevRhoInt(Uin aPropNo) const {return mPrevRhoInt[aPropNo];}          ///< Past int. value of rho
      Nb GetPrevVint(Uin aPropNo) const {return mPrevVint[aPropNo];}              ///< Past int. value of v
      Nb GetPrevRhoVint(Uin aPropNo) const {return mPrevRhoVint[aPropNo];}        ///< Past int. value of rho*v
      
      Nb GetRelRhoVerr(Uin aPropNo) const {return fabs((mRhoVint[aPropNo]-mPrevRhoVint[aPropNo])/mRhoVint[aPropNo]);}  ///< Rel. error (Rho*V)
      Nb GetRelNormRhoVerr(Uin aPropNo) const {return mNormRhoVint[aPropNo]/mSquaredRhoVint[aPropNo];}                 ///< Rel. error (Rho*V)^2
      Nb GetRelRhoErr(Uin aPropNo) const {return fabs((mRhoInt[aPropNo]-mPrevRhoInt[aPropNo])/mRhoInt[aPropNo]);}      ///< Rel. error  Rho
      Nb GetRelVerr(Uin aPropNo) const {return fabs((mVint[aPropNo]-mPrevVint[aPropNo])/mVint[aPropNo]);}              ///< Rel. error  V
      
      Nb GetAbsRhoVerr(Uin aPropNo) const {return fabs((mRhoVint[aPropNo]-mPrevRhoVint[aPropNo]));}  ///< Abs. error (Rho*V)
      Nb GetAbsNormRhoVerr(Uin aPropNo) const {return mNormRhoVint[aPropNo];}                        ///< Abs. error (Rho*V)^2
      Nb GetAbsRhoErr(Uin aPropNo) const {return fabs((mRhoInt[aPropNo]-mPrevRhoInt[aPropNo]));}     ///< Abs. error  Rho
      Nb GetAbsVerr(Uin aPropNo) const {return fabs((mVint[aPropNo]-mPrevVint[aPropNo]));}           ///< Abs. error  V
   public:

      //! Default constructor
      ItGridBox
         (  In aVecSize
         ) : mNumProp(aVecSize)
         //,  mSize(C_0)
         ,  mConverged(true)
         ,  mRhoInt(aVecSize, C_0)
         ,  mPrevRhoInt(aVecSize, C_0)
         ,  mRhoBasisInt(aVecSize, C_0)
         ,  mVint(aVecSize, C_0)
         ,  mPrevVint(aVecSize, C_0)
         ,  mRhoVint(aVecSize, C_0)
         ,  mPrevRhoVint(aVecSize, C_0)
         ,  mNormRhoVint(aVecSize, C_0)
         ,  mSquaredRhoVint(aVecSize, C_0)
      {
      }

      //! Set the value of all elements of mRhoV and mPrevRhoV to zero
      void InitializeRhoV(In aLength)
      {
         mRhoV.clear();
         mPrevRhoV.clear();
         for(In i = I_0; i < mNumProp; ++i)
         {
            mRhoV.emplace_back();
            mPrevRhoV.emplace_back();
            mRhoV.back().SetNewSize(aLength);
            mPrevRhoV.back().SetNewSize(aLength);
            for (In j = I_0; j < aLength; j++)
            {
               mRhoV.back()[j] = C_0;
               mPrevRhoV.back()[j] = C_0;
            }
         }
      }
      ///< Set the value of the size of a box
      //void SetSize(Nb aSize) 
      //{
      //   mSize = aSize;
      //}                                     
      ///< Set the value of the integral of density in [a,b]
      void SetRhoInt(Uin aPropNo, Nb aRhoInt)   
      {
         mPrevRhoInt[aPropNo] = mRhoInt[aPropNo]; 
         mRhoInt[aPropNo] = aRhoInt; 
      } 
      ///< Set the value of the integral of potential in [a,b]
      void SetVint(Uin aPropNo, Nb aVint) 
      {
         mPrevVint[aPropNo] = mVint[aPropNo]; 
         mVint[aPropNo] = aVint;
      }                    
      ///< Set the value of the integral of density*potential in [a,b]
      void SetRhoVint(Uin aPropNo, Nb aRhoVint) 
      {
         mPrevRhoVint[aPropNo] = mRhoVint[aPropNo]; 
         mRhoVint[aPropNo] = aRhoVint;
      }  
      ///< Set the value of the integral in the interval between potential and basis bounds
      void SetRhoBasisInt(Uin aPropNo, Nb aRhoBasisInt) 
      {
         mRhoBasisInt[aPropNo] = aRhoBasisInt;
      }       
      ///< Set the value of the integral of (mRhoV - mPrevRhoV)^2 in interval
      void SetNormRhoVint(Uin aPropNo, Nb aNormRhoVint)
      {
         mNormRhoVint[aPropNo] = aNormRhoVint;
      }
      ///< Set the value of the integral over (mRhoV)^2
      void SetSquaredRhoVint(Uin aPropNo, Nb aSquaredRhoVint)
      {
         mSquaredRhoVint[aPropNo] = aSquaredRhoVint;
      }
      ///< Set the value of non-integrated rho * potential in [a,b]
      void SetRhoV(Uin aPropNo, MidasVector aRhoV) 
      {
         mPrevRhoV[aPropNo] = mRhoV[aPropNo]; 
         mRhoV[aPropNo] = aRhoV;
      }

      bool GetConverged() const {return mConverged;}
      void SetConverged(bool aBool) {mConverged = aBool;}

      Nb GetRhoIntForPropSurfI(Uin aPropNo) const {return mRhoInt[aPropNo];}           ///< Current int. value of rho
      Nb GetRhoBasisIntForPropSurfI(Uin aPropNo) const {return mRhoBasisInt[aPropNo];} ///< Current int. value of rho found between potential and basis bounds
      Nb GetRhoVintForPropSurfI(Uin aPropNo) const {return mRhoVint[aPropNo];}                ///< Current int. value of rho*v
      Nb GetVintForPropSurfI(Uin aPropNo) const {return mVint[aPropNo];}                      ///< Current int. value of v
      Nb GetNormRhoVintForPropSurfI(Uin aPropNo) const {return mNormRhoVint[aPropNo];}        ///< Current int. value of (mRhoV - mPrevRhoV)^2
      MidasVector GetPrevRhoV(Uin aPropNo) const {return mPrevRhoV[aPropNo];}     ///< Previous non-integrated value of rho*v 
      
      //Interface functions for Adaptive ADGA
      Nb GetRelRhoVerrSum() const;
      Nb GetAbsRhoVerrSum() const;
      Nb GetRelNormRhoVerrSum() const;
      Nb GetAbsNormRhoVerrSum() const;
      
      //Check convergence of the box
      void CheckItPotThres(Nb aThres, Nb aAbsThres); ///<
      void CheckItDensThres(Nb aThres, Nb aAbsThres); ///<
      void CheckItVDensThres(Nb aThres, Nb aAbsThres); ///<
      void CheckItNormVDensThr(Nb aThres, Nb aAbsThres); ///<

      ///< overload for << operator
      friend ostream& operator<<
         (  ostream& arOut
         ,  const ItGridBox& arPoint
         )
      {
         arOut.setf(ios::scientific);
         arOut.setf(ios::uppercase);
         arOut.precision(I_18);
         arOut.setf(ios::showpoint);
         arOut << " V Int:           " << arPoint.mVint        << std::endl;      
         arOut << " Prev. V Int:     " << arPoint.mPrevVint    << std::endl;      
         arOut << " Rho Int:         " << arPoint.mRhoInt      << std::endl;      
         arOut << " Prev. Rho Int:   " << arPoint.mPrevRhoInt  << std::endl;      
         arOut << " Rho*V Int:       " << arPoint.mRhoVint     << std::endl;      
         arOut << " Prev. Rho*V Int: " << arPoint.mPrevRhoVint << std::endl;      
         arOut << " Norm Rho*V Int:  " << arPoint.mNormRhoVint << std::endl;      
         return arOut;
      }
};

#endif // ITGRIDBOX_H
