/**
************************************************************************
*
*  @file                ItGrid.h
* 
*  Created:             17/10/2007
* 
*  Author:              D. Toffoli (toffoli@chem.au.dk) and M. Sparta (msparta@chem.au.dk)
*                       B. Thomsen (drbothomsen@gmail.com)
* 
*  Short Description:   Declares class ItGrid for storing the
*                       information for the iterative evaluation
*                       of the PES for one MC
* 
*  Last modified: Tue Dec 14, 2020  04:30PM
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#ifndef ITGRID_H
#define ITGRID_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/Input.h"
#include "pes/adga/ItGridDensities.h"
#include "pes/adga/ItGridBox.h"

// Forward declarations.
#include "pes/integrals/MidasPesIntegrals_Fwd.h"
class MidasPotential;
template<class T> class MidasFunctionWrapper;

/**
 * Structure to hold the grid boxes of a given MC.
 **/
class ItGrid 
{
   private:
      //!
      const ItGridDensities& mDensities;
      //! The Number of Properties being Optimized
      In mNumProp;
      //! The number of modes in the mode combi.
      In mNumModes;
      //! Number of points for Gauss-Legendre quadrature.
      In mGaussPoints = gPesCalcDef.GetmPesGLQuadPnts();
      //! Current total density inside the potential.
      std::vector<Nb> mIterTotalDensInPot;
      //! Total density within the basis boundaries
      std::vector<std::vector<Nb> > mIterTotalDens;
      //! Mode combination for iterative grid.
      std::vector<In> mModes;  
      //! Scale factor for left side of iterative grid
      std::vector<Nb> mQlScal;
      //! Scale factor for right side of iterative grid
      std::vector<Nb> mQrScal;
      //! Point used in the Shepard extrapolation/interpolation .
      std::vector<std::vector<In> > mGhosts;  
      //! Grid bounds for each mode.
      std::vector<std::vector<In> > mBounds;
      //! New extended grid bounds.
      std::vector<std::vector<In> > mNewBounds; 
      //!
      std::vector<std::vector<std::vector<In>>> mNewBoundsAllSurfaces; 
      //! Hold calculated integrals for each box.
      std::vector<ItGridBox> mIntegrals;
      //! Total \f$ \rho*V \f$ integral.
      std::vector<Nb> mTotRhoV;
      
      //! The old total from last iteration.
      std::vector<Nb> mOldTotRhoV;

      //! Is this modecombi converged.
      bool mConverged;
      //! Did we extend in this iteration?
      bool mExtend;

      //! Number of tabulated density points used for polynomial interpolation.
      Uin mNumPointsPolynomialInterpol = gPesCalcDef.GetmPesGLQuadPnts();
      
      //! Evaluate the density for a specific mode
      Nb EvaluateDensity(Uin aPropNo, In aMode, Nb aQval) const;

      //! Get multi-index for specific serial box index.
      std::vector<Uin> GetBoxIndex(Uin aBox) const;

      //! Get coordinate bounds for a specific mode (and index of that mode).
      std::pair<Nb,Nb> GetQBoundsPair(Uin aModeIndex, Uin aIntervalIndex) const;

      //! Get all coordinate bounds for a specific mode, in pairs.
      std::vector<std::pair<Nb,Nb>> GetQBoundsPairsOfMode(Uin aModeIndex) const;

      //! A vector of vectors with all the integration intervals for each mode.
      std::vector<std::vector<std::pair<Nb,Nb>>> IntervalBoundsForEachMode() const;

      //! Calculates all integral that can utilize sum-over-product structure of potential.
      void CalcIntSumOverProduct(const std::vector<MidasPotential>& arProperty);

      //! Calculates all box sizes/volumes.
      void CalcAndUpdateBoxSizes();

      //@{
      //! Calculates dens, pot, dens*pot using sum-over-product algorithms.
      void CalcAndUpdateDensIntegrals
         (  const Uin& aPropNo
         ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
         ,  const std::vector<std::vector<midas::pes::integrals::OneModeDensityInterpolator>>& arDens
         ,  const std::vector<Nb>& arScalingFactors
         );
      void CalcAndUpdatePotIntegrals
         (  const Uin& aPropNo
         ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
         ,  const std::vector<Nb>& arCoefficients
         ,  const std::vector<std::vector<MidasFunctionWrapper<Nb>>>& arFunctions
         ,  const std::vector<Nb>& arScalingFactors
         );
      void CalcAndUpdateDensPotIntegrals
         (  const Uin& aPropNo
         ,  const std::vector<std::vector<std::pair<Nb,Nb>>>& arIntervals
         ,  const std::vector<midas::pes::integrals::OneModeDensityInterpolator>& arDens
         ,  const std::vector<Nb>& arCoefficients
         ,  const std::vector<std::vector<MidasFunctionWrapper<Nb>>>& arFunctions
         ,  const std::vector<Nb>& arScalingFactors
         );
      void CalcExtendedDensInt
         (  const std::vector<MidasPotential>& arPot
         );
      //@}

      //@{
      //! Calcs. and stores integrals, not utilizing sum-over-product structure.
      void CalcIntNonSumOverProduct
         (  const std::vector<MidasPotential>& arProperty
         ,  In aCurrentIter
         ,  bool aSaveHistoricNonSopIntegrals = false
         );
      void GaussIntNonSumOverProduct
         (  Uin aPropNo
         ,  In aIndex
         ,  const std::vector<pair<Nb,Nb>>& aIntervals
         ,  In aNgauss
         ,  In aCurrentIter
         ,  const std::vector<MidasPotential>& arProperty
         ,  bool aSaveHistoricNonSopIntegrals = false
         );
      //@}

      //! Calculates total integral for the ModeCombi over given function.
      template<class Func>
      Nb TotalIntegral(const Func& arIntegralForBoxIndex, Uin aPropNo) const;

      //@{
      //! Calculates total integral for the ModeCombi over the specified integrand.
      Nb TotalDensityIntegral(Uin aPropNo) const;
      Nb TotalPotentialIntegral(Uin aPropNo) const;
      Nb TotalDensityPotentialIntegral(Uin aPropNo) const;
      Nb TotalNormDensityPotentialIntegral(Uin aPropNo) const;
      //@}

      //! if a box is not converged, divide it
      void SplitBox(const std::vector<In>& arBounds);            
      //! For 1d modecomb  and specific property check if the bounds have to be extended
      void CheckExtensionIthProp(Uin aPropNo);                      
      //! For 1d modecomb set the max left boundary
      void SetLeftBound();
      //! For 1d modecomb set the max right boundary
      void SetRightBound();
      
      //! Set the Total Rho*V value
      void SetTotRhoV(Nb aRhoV, Uin aPropNo)
      {
         mOldTotRhoV[aPropNo] = mTotRhoV[aPropNo];
         mTotRhoV[aPropNo] = aRhoV;
      } 
   public:
      
      //! constructor from modes
      ItGrid(const std::vector<In>& aModes, const ItGridDensities& arDensities, const Uin& aPropNo);

      //!
      ItGrid& operator=(const ItGrid&) = delete;

      //!
      ~ItGrid() 
      { 
      };

      //! Evaluate density for specific mode
      void EvaluateDensity(Uin aPropNo, In aMode, const MidasVector& arQvals, MidasVector& arDenVals) const;
      
      // Const functions
      //! Return the convergency state of the mode comb
      const bool& GetConv() const                                 {return mConverged;}
      //! Return new box bounds
      const std::vector<std::vector<In> >& GetmNewBounds() const  {return mNewBounds;}
      In GetNmodes() const { return  mNumModes; }                             ///< return the number of mode in the mode combination
      vector<In> GetModeCombi() const {return mModes;}                      ///< return the mode-combi
      In GetNbounds(In aMode=I_0) const {return mBounds[aMode].size();}     ///< return the number of bounds for the specific mode
      In GetNrOfBoxes() const;                                              ///< find the index for a specific integral
      In GetNrOfIntegrals() const {return mIntegrals.size();}               ///< return the numer of integrals stored
      In GetLbound(In aMode=I_0) const {return mBounds[aMode][I_0]; }       ///< Return the Left Bound for a Mode 
      In GetRbound(In aMode=I_0) const {return mBounds[aMode][mBounds[aMode].size()-I_1]; }   ///< Return the Right Bound for a Mode 
      In GetBound(In aInt, In aMode=I_0) const {return mBounds[aMode][aInt]; } ///< return a specific bound 
      
      //! Return the left Q-unit value for a mode 
      Nb GetQlScal(In aMode = I_0) const {return mQlScal[aMode];}
      
      //! Return the left Q-unit value for a mode 
      Nb GetQrScal(In aMode = I_0) const {return mQrScal[aMode];}
      
      //! Return the Q-Left Bound for a Mode 
      const Nb GetQleft(const In& aMod = I_0) const 
      {  
         return mBounds[aMod][I_0] * mQlScal[aMod];
      }
      
      //! Return the Q-Right Bound for a Mode 
      const Nb GetQright(const In& aMod = I_0) const 
      {
         return mBounds[aMod][mBounds[aMod].size() - I_1] * mQrScal[aMod];
      }

      //! Return an integration box boundary defined for a specific mode
      const Nb GetQBoxBound(const In& aMode, const In& aBound) const
      {
         // Will get simple integers here 
         Nb box_bound = mBounds[aMode][aBound];
         
         if (box_bound < C_0)
         {
            box_bound *= mQlScal[aMode];
         }
         else
         {
            box_bound *= mQrScal[aMode];
         }

         return box_bound;
      }
      
      //! Get the whole vector of bounds
      void GetBounds(std::vector<std::vector<In> >& arBounds) const {arBounds = mBounds;}

      //! Get the Total Rho*V value
      const Nb& GetTotRhoVForPropSurfI(const Uin& aPropNo) const {return mTotRhoV[aPropNo];}

      //! Get the total density from previous iteration
      const Nb& GetPrevTotDensForPropSurfI(const Uin& aPropNo) const {return mIterTotalDensInPot[aPropNo];}

      void GetBounds(In aMode, std::vector<In>& arBounds) const;                 ///< Get a vector as bounds for the aMode mode (in the local list)
      void GetBounds(In aMode, MidasVector& arBounds) const;                ///< Get a MidasVector as bounds for the aMode mode (in the local list)
      Nb GetBoxSize(const vector<In>& arBounds) const;                      ///< Compute the size of the box (lenght/area/volume...)
      void GetQbounds(In aMode, vector<Nb>& arBounds) const;                ///< Return the Q-boundaries of the grid
      void GetBoxIndex(In aBox, vector<In>& arBoxIndex) const;              ///< set the qbound of a specific integral
      //! Get coordinate bounds per mode for a specific box.
      void GetQbox(const vector<In>& arBoxIndex, vector<pair<Nb,Nb> >& arQBox) const;
      void GetQdomain(vector<pair<Nb,Nb> >& arQBox) const;                  ///< return the Q limit of the modecombi
      bool IsItAbound(In aInt, In aMode=I_0) const;                         /// return if a integer is present in the list of mbounds
      void GetKvecsVec(vector<vector<In> >& arKvecsVec) const;              ///< Get the k vectors for the mode-combi     
      bool CheckDisplacement(const vector<In>& arKvec,  bool arZero) const; ///< Check if the given displacement is needed
      
      //Set functions
      void SetConv(bool aBool) { mConverged = aBool; }                     ///< Set the convergency 
      void SetNewBound(const In& aMode, const In& aBound);                 ///< Set a specific point to be computed;
      void SetBounds(const In aMode, const In aLeft, const In aRight, const In aSize); ///< Set the indx of the interval
      void SetBounds(const In aMode, const std::vector<In>& arNewBounds);              ///< Set new bounds for given mode
      
      //! Set the Q-unit scale factors for a mode
      void SetmQScales(const In& aMode, const Nb& aQlScal, const Nb& aQrScal)
      {
         mQlScal[aMode] = aQlScal;
         mQrScal[aMode] = aQrScal;
      }
            
      //! Set the current total density for later density leak checking
      void SetCurrTotDensForPropSurfI(const Uin& aPropNo, const Nb& aCurrTotalDens) {mIterTotalDensInPot[aPropNo] = aCurrTotalDens;} 

      //! Clear the object
      void Clear();
      //! Reinitialize  the object
      void ReIni(const In);
      //! Calculate the integrals over all relevant boxes (intervals)
      void CalcIntForBoxes(const bool& aPrint, const std::vector<MidasPotential>& arProperty, const In& aCurrentIter, const In& arNoVibs);
      //! Resize and reinitialize the container for the integral info.   
      void ResizeIntegrals(In aSize);  
      //! For 1d modecomb check if the bounds have to be extended        
      void CheckExtension();                                           
      //! check convergency            
      void CheckConvergence(); 
      //! Update the grid of boxes
      void UpdateGrid(); 
      void Append(const In& aMode, const std::vector<In>& arNewBounds); ///< Append newbounds
      void ResetGrid();                                                ///< Reset the grid of boxes
      
      //! compute and set the integral values
      void qGaussInt(Uin aPropNo, In aIndex, std::vector<std::pair<Nb,Nb> >& aExtremes, In aDegree);

      void FillXigGhosts(vector<vector<In> >& arInXig) const;   /// Fill the Xig_ghost vector of vector with the ghost points info
      
      friend ostream& operator<<(ostream& arOut, const ItGrid& arBoxes); ///< overload for << operator

};

//! Overload for << operator
ostream& operator<<(ostream& arOut, const ItGrid& arPoint); 

#endif /* ITGRIDBOXES_H */
