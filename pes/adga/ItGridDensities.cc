/**
************************************************************************
*
* @file                ItGridDensities.cc
*
* Created:             05-09-2017
*
* Author:              Ian H. Godtliebsen
*
* Short Description:   Stores data for box integral evaluation.
*                      
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "pes/adga/ItGridDensities.h"

#include <iostream>

#include "input/Input.h"

/**
 * This will open and load the density file for a given mode,
 * and load the values into two vectors (these may be previously allocated).
 * The function will first resize the vectors and then subsequently append all
 * file values using emplace_back. The resize is done to save some allocations.
 *
 * @param aAnalysisDir   Path to the analysis directory.
 * @param aProperty      The property to load the files for.
 * @param aMode          The mode to load the files for.
 * @param aGridValues    On output will hold the grid values for mode aMode.
 * @param aPotValues     On output will hold the potential values for mode aMode.
 **/
void OpenAndLoadFile
   (  const std::string& aAnalysisDir
   ,  const In& aProperty
   ,  const std::string& aMode
   ,  std::vector<Nb>& aGridValues
   ,  std::vector<Nb>& aPotValues
   )
{
   // Set size to zero, so we can emplace_back all values.
   aGridValues.resize(0);
   aPotValues.resize(0);
   
   // Open file with densities, will have different names depending on the use of mean or maximum vibrational density to determine the Adga convergence
   std::string data_file_name;
   if (gPesCalcDef.GetmAnalyzeDensMethod() == "MEAN")
   {
      data_file_name = aAnalysisDir + "/" + "MeanDens_Prop" + std::to_string(aProperty) + "_" + aMode + ".mplot";
   }
   else
   {
      data_file_name = aAnalysisDir + "/" + "MaxDens_Prop" + std::to_string(aProperty) + "_" + aMode + ".mplot";
   }

   ifstream data_file;
   data_file.open(data_file_name.c_str(), ios_base::in);
   if (data_file.fail())
   {
      MIDASERROR(" File with density data not found - " + data_file_name + " - Check your input to see if the right VSCF calculations are being done");
   }

   // Read in file
   std::string s;
   while (std::getline(data_file,s)) // get new lines with mode and operators end.
   {
      istringstream input_s(s);
      Nb x_val;
      Nb p_val;
      input_s >> x_val;
      input_s >> p_val;
      //Mout << " x_val: " << x_val << " p_val: " << p_val << endl;
      aGridValues.emplace_back(x_val);
      aPotValues.emplace_back(p_val);
   }
   data_file.close();
}


/**
 * Constructor. Takes the number of modes to pre-allocate some storage.
 *
 * @param aNmodes
 * @param arPropIdxs
 **/
ItGridDensities::ItGridDensities(In aNmodes, const std::vector<In>& arPropIdxs)
   :  mPropIdxs(arPropIdxs) 
   ,  mNmodes(aNmodes)
{
   mGridValues.resize(arPropIdxs.size());
   mPotValues.resize(arPropIdxs.size());
   for(size_t i = 0; i < arPropIdxs.size(); ++i)
   {
      mGridValues[i].resize(aNmodes);
      mPotValues[i].resize(aNmodes);
   }
}

/**
 * Load all density files and save the grid values and potential values
 * in two vectors for later use.
 **/
void ItGridDensities::LoadFiles
   (  const std::string& aAnalysisDir
   ,  const std::vector<std::string>& aModeLabels
   )
{
   for (int i = 0; i < mPropIdxs.size(); ++i)
   {
      for (int j = 0; j < mNmodes; ++j)
      {
         OpenAndLoadFile(aAnalysisDir, mPropIdxs[i], aModeLabels[j], mGridValues[i][j], mPotValues[i][j]);
      }
   }
}

/**
 * Get the vector of grid values for a given mode.
 **/
const std::vector<Nb>& ItGridDensities::GetGridValues
   (  In aPropIdx
   ,  In aMode
   )  const
{
   if(aPropIdx >= mPropIdxs.size())
   {
      MIDASERROR("Property index: " + std::to_string(aPropIdx) + " is larger than the number of properties : " + std::to_string(mPropIdxs.size()) + ".");
   }
   if(aMode >= mNmodes)
   {
      MIDASERROR("Mode number: " + std::to_string(aMode) + " is larger than the number of modes : " + std::to_string(mNmodes) + ".");
   }
   return mGridValues[aPropIdx][aMode];
}

/**
 * Get the vector of potential values for a given mode.
 **/
const std::vector<Nb>& ItGridDensities::GetPotValues
   (  In aPropIdx
   ,  In aMode
   )  const
{
   if (aPropIdx >= mPropIdxs.size())
   {
      MIDASERROR("Property index: " + std::to_string(aPropIdx) + " is larger than the number of properties : " + std::to_string(mPropIdxs.size()) + ".");
   }

   if (aMode >= mNmodes)
   {
      MIDASERROR("Mode number: " + std::to_string(aMode) + " is larger than the number of modes : " + std::to_string(mNmodes) + ".");
   }

   return mPotValues[aPropIdx][aMode];
}
