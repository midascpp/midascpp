/**
************************************************************************
*
*  @file                ItGridBox.cc
* 
*  Created:             22/12/2020
* 
*  Author:              D. Toffoli (toffoli@chem.au.dk), M. Sparta (msparta@chem.au.dk)
*                       and B. Thomsen (drbothomsen@gmail.com)
* 
*  Short Description:   Declares class ItGridBox for storing the
*                       integral values for the iterative evaluation
*                       of the monodimensiona PES  
* 
*  Last modified:       22/12/2020
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#include "pes/adga/ItGridBox.h"
#include "input/Input.h"


void ItGridBox::CheckItPotThres(Nb aThres, Nb aAbsThres)
{
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      if (gPesIoLevel > I_11)
      {
         Mout << " For property number:  " << prop_no << std::endl;
         Mout << " Potential rel. error: " << GetRelVerr(prop_no) << std::endl;
         Mout << " Potential abs. error: " << GetAbsVerr(prop_no) << std::endl;
         Mout << " Potential value:      " << std::fabs(GetVintForPropSurfI(prop_no)) << std::endl;
      }

      if (  GetRelVerr(prop_no) > aThres &&
            (GetAbsVerr(prop_no) > aAbsThres || std::fabs(GetVintForPropSurfI(prop_no)) > aAbsThres)
         )
      {
         mConverged = false;
      }
   }
}

void ItGridBox::CheckItDensThres(Nb aThres, Nb aAbsThres)
{
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      if (gPesIoLevel > I_11)
      {
         Mout << " For property number:  " << prop_no << std::endl;
         Mout << " Density rel. error: " << GetRelRhoErr(prop_no) << std::endl;
         Mout << " Density abs. error: " << GetAbsRhoErr(prop_no) << std::endl;
         Mout << " Density value:      " << std::fabs(GetRhoIntForPropSurfI(prop_no)) << std::endl; 
      }

      if (  GetRelRhoErr(prop_no) > aThres &&
            (GetAbsRhoErr(prop_no) > aAbsThres || std::fabs(GetRhoIntForPropSurfI(prop_no)) > aAbsThres)
         )
      {
         mConverged = false;
      }
   }
}

void ItGridBox::CheckItVDensThres(Nb aThres, Nb aAbsThres)
{
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      if (gPesIoLevel > I_11)
      {
         Mout << " For property number:  " << prop_no << std::endl;
         Mout << " Rho*V rel. error: " << GetRelRhoVerr(prop_no) << std::endl;
         Mout << " Rho*V abs. error: " << GetAbsRhoVerr(prop_no) << std::endl;
         Mout << " Rho*V value:      " << std::fabs(GetRhoVintForPropSurfI(prop_no)) << std::endl;
      }

      if (  GetRelRhoVerr(prop_no) > aThres &&
            (GetAbsRhoVerr(prop_no) > aAbsThres || std::fabs(GetRhoVintForPropSurfI(prop_no)) > aAbsThres)
         )
      {
         mConverged = false;          
      }
   }
}

void ItGridBox::CheckItNormVDensThr(Nb aThres, Nb aAbsThres)
{
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      if (gPesIoLevel > I_11)
      {
         Mout << " For property number:  " << prop_no << std::endl;
         Mout << " Norm Rho*V rel. error: " << GetRelNormRhoVerr(prop_no) << std::endl;
         Mout << " Norm Rho*V abs. error: " << GetAbsNormRhoVerr(prop_no) << std::endl;
         Mout << " Squared Rho*V value:   " << GetSquaredRhoVint(prop_no) << std::endl;
      }
   
      if (  GetRelNormRhoVerr(prop_no) > aThres &&
            (GetAbsNormRhoVerr(prop_no) > aAbsThres || GetSquaredRhoVint(prop_no) > aAbsThres)
         )
      {
         mConverged = false;          
      }
   }
}

Nb ItGridBox::GetRelRhoVerrSum() const
{
   Nb sum = C_0;
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      sum += GetRelRhoVerr(prop_no);
   }
   return sum;
}

Nb ItGridBox::GetAbsRhoVerrSum() const
{
   Nb sum = C_0;
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      sum += GetAbsRhoVerr(prop_no);
   }
   return sum;
}

Nb ItGridBox::GetRelNormRhoVerrSum() const
{
   Nb sum = C_0;
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      sum += GetRelNormRhoVerr(prop_no);
   }
   return sum;
}

Nb ItGridBox::GetAbsNormRhoVerrSum() const
{
   Nb sum = C_0;
   for(In prop_no = 0; prop_no < mNumProp; ++prop_no)
   {
      sum += GetAbsNormRhoVerr(prop_no);
   }
   return sum;
}
