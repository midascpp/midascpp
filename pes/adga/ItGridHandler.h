/**
************************************************************************
*
* @file                ItGridHandler.h
*
* Created:             24-10-2006
*
* Author:              D. Toffoli (toffoli@chem.au.dk) and M. Sparta (msparta@chem.au.dk)
*
* Short Description:   stores informations about the dynamic grids
*                      for each of the modes
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ITGRIDDEF_H
#define ITGRIDDEF_H

#include <vector>
#include <string>
#include <iostream>
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "pes/adga/ItGridDensities.h"
#include "pes/adga/ItGrid.h"

// Forward declarations
class GridType;

/**
* Note that for this class the Address(aAdd/Add) will locally go from 0, however since the mode combinations count like
* (), (0), (1),...(That is they include the empty MC) the address given from outside has to be substracted by one
* */
class ItGridHandler
{
   private:

      //! 
      ItGridDensities mDensities;

      //! A vector holding the grids for each MC.
      std::vector<ItGrid> mVecOfGrids;

      //! The total number of vibrational modes in the system
      In mNumModes;

      //! SHOULD I MOVE THE BELOW CONSTANTS ???? NOT REALLY USED IN THIS CLASS
      //! Current iteration for the 1d grids.
      In mCurr1dIter;
      //! Current iteration for the 2d grids.
      In mCurrNdIter;
      //! Is the 1d procedure converged.
      bool mConv1dGrid;
      //! Is the Nd procedure converged.
      bool mConvNdGrid;

      //@{
      //! mVecOfGrids access with range check and error thrown if out of range.
      ItGrid& VecOfGridsAt(std::size_t);
      const ItGrid& VecOfGridsAt(std::size_t) const;
      //@}

   public:

      //! Default constructor
      ItGridHandler( In aNmodes, std::vector<In> aProperties, Uin aPropNo );

      //! Load new mean density files
      void LoadMeanDensFiles(const std::string& aAnalysis, const std::vector<std::string>& aModeLabels);
      
      //! Set the conv. for the 1d grids 
      bool Is1dConv() const { return mConv1dGrid; }
      
      //! Set the conv. for the Nd grids 
      bool IsNdConv() const { return mConvNdGrid; }                                   
      
      //! Get the current iteration for the 1-mode proc.
      In GetCurr1dIter() const { return mCurr1dIter; }                                
      
      //! Get the current iteration for the N-mode proc.
      In GetCurrNdIter() const { return mCurrNdIter; }                                
      
      //! Return the nr of mode combi grids
      In GetNrOfModeCombiGrids() const { return mVecOfGrids.size(); }                

      //! Get the Nr. of s.p.c. requested for a specific grid
      In GetNrOfPoints(In aAdd) const {return mVecOfGrids[aAdd-1].GetNrOfBoxes();}

      //! Get the number of intergration boxes for a specific mode combination grid
      const In GetNoBoxesForMc(const In& aAdd) const {return VecOfGridsAt(aAdd - I_1).GetNrOfBoxes();}

      //! Check if the given displacement is to be computed
      bool CheckDisplacement(const InVector& arKvec,In aAdd, bool arZero) const { return mVecOfGrids[aAdd-1].CheckDisplacement(arKvec, arZero); }
      
      //! return the bounds of a particular grid(pushed back into arIvec)
      void GetCurrBounds(const In aAdd, std::vector<In>& arIvec) const;
      
      //! return the bounds of a particular grid
      void GetCurrQbounds(const In& aAdd, std::vector<Nb>& arNbVec) const;

      //! Get the vector of Kvecs from a mode-combi grid
      void GetKvecsVecForModeCombi(In aAdd, const vector<In>& arModeCombi, vector<vector<In> >& arKvecsVec) const;
      
      //! retrive the Xig info for the ghost points
      void FillXigGhosts(const In& aAdd, vector<vector<In> >& arInXig) const;
      
      void SetCurr1dIter(In aIter) { mCurr1dIter=aIter; }                             ///< Sets the current iteration for the 1-mode grids
      void SetCurrNdIter(In aIter) { mCurrNdIter=aIter; }                             ///< Sets the current iteration for the 2-mode grids
      void SetConv1d(bool aB) { mConv1dGrid=aB; }                                     ///< Set the conv. for the 1d grids 
      void SetConvNd(bool aB) { mConvNdGrid=aB; }                                     ///< Set the conv. for the Nd grids 
   
      ///< Reinitialize in the multilevel procedure
      void ReInitialize(const In);

      //! Append a set of box boundaries to the iterative grid
      void AppendBounds(const In& aAdd, const In& aMode, const std::vector<In>& arBounds)
      {
         mVecOfGrids[aAdd - I_1].Append(aMode, arBounds);
      }
      
      //! Check convergency for the 1-mode grids
      bool CheckConvForMc(In aAdd, const vector<In>& arModeCombi);
      
      //! Return whether or not a mode combination is converged
      bool GetMcConv(const In& aMcAdress) const
      {
         auto& mc_grid = VecOfGridsAt(aMcAdress - I_1);
         return mc_grid.GetConv();
      }
      
      //! Set  mode combination convergent status
      void SetMcConv(const In& aMcAdress, const bool& aMcConv)
      {
         auto& mc_grid = VecOfGridsAt(aMcAdress - I_1);
         return mc_grid.SetConv(aMcConv);
      }
      
      //! Initialize the iterative grids and store the current 1M grid bounds  
      void InsertModeCombiGrid(const ModeCombi& arModeCombi, const GridType& arGridType, const std::pair<MidasVector, MidasVector>& arScalFacts, const In& aNoProps);

      //! Save the potential values for the current iteration on tapes, to be read by the Vscf driver
      void SavePotential
         ( const In& aAdd
         , const std::vector<In>& arModeCombi
         , const std::vector<MidasPotential>& aPotential
         , const Uin& aPropNo
         , const In& aMcl
         , const std::vector<std::pair<Nb, Nb> >& aBasisBounds
         , const std::string& aAnalysisDir
         , const std::vector<std::string>& arMcLabels
         , const std::string& aExtension = "") 
         const;

      //! Update the grid
      void UpdateGridForMc(In aAdd);

      //! Reset the grid 
      void ResetGrids(In aAdd){mVecOfGrids[aAdd-1].ResetGrid();}                   

      //! Calculate the integrals pertaining to a particular mode combination
      bool CalcIntForMc(const In&, const bool&, const std::vector<MidasPotential>&, const In&);                              
      //!@{
      //! Gets a copy of the grid for a given mode combination
      const ItGrid& ModeCombiGrid(In aAdd) const { return mVecOfGrids[aAdd-1]; }
      ItGrid&       ModeCombiGrid(In aAdd)       { return mVecOfGrids[aAdd-1]; }
      //!@}
};

#endif /* ITGRIDDEF_H */
