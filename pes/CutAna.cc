/**
************************************************************************
* 
* @file                CutAna.cc
*
* Created:             
*
* Author:              
*
* Short Description:   Class for holding operator definition and utilities
* 
* Last modified: Tue Jul 31, 2007  01:50PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

/*  standard headers   */
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

/*  User defined  headers   */
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "pes/PesInfo.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/CutAna.h"
#include "input/PesCalcDef.h"

using Sst = string::size_type;

/**
* Default constructor
**/
CutAna::CutAna() {
   mNfree=I_0;
   mNfixed=I_0;
}
   

/**
* Deconstructor
**/
CutAna::~CutAna
   (
   ) 
{
}


   
void InitiateCutAnalysis(CutAna* mCuts)
{
   //The input is treated line by line 
   for (In i=I_0;i<gPesCalcDef.GetmMakeCutAnalysis().size();i++)
   {
     // Mout << endl << "Form input: " << gMakeCutAnalysis[i] << endl ;
      vector<string> local_vector;
      local_vector.clear();
      istringstream Info(gPesCalcDef.GetmMakeCutAnalysis()[i]);
      string s_new;
      while (Info >> s_new)
      {
         local_vector.push_back(s_new);
      }
      
      
      if (local_vector[0] != "FREE")
      {
         Mout << " Warning FREE keyword needed for Cut Analysis " <<  endl;
         Mout << " Give FREE mode1 mode2.. FIXED mode_i value_i mode_j value_j ..." << endl;
         break ;
         //MIDASERROR("");
      }
      In i_free=I_0 ;
      for (In ik = I_1 ; ik < local_vector.size() ; ik++)
      {
         if (local_vector[ik] == "FIXED") break;

         i_free++;
      //   Mout << " new free " << local_vector[ik] << " numeber free= " << i_free << endl;
      }

        

      In pair_fixed;
      if (local_vector[i_free+I_1] != "FIXED")
      {
         Mout << " Cut Analysis Performed without FIXED modes " <<  endl;
         pair_fixed = I_0 ;
      }
      else
      {
         pair_fixed = local_vector.size() - (i_free+I_2) ;
      }
      
      mCuts[i].mNfree = i_free ;
      mCuts[i].mQfreeNames.resize(i_free);
      for (In j=I_0;j<i_free;j++)
      {
         mCuts[i].mQfreeNames[j]=atoi(local_vector[j+I_1].c_str()) ;
      }
      if (int(pair_fixed % I_2) != I_0) 
      {
         Mout << " Warning: Something is wrong for the Fixed modes of the Cut Analysis" << endl ;
         Mout << " Give FREE mode1 mode2.. FIXED mode_i value_i mode_j value_j ..." << endl;
         mCuts[i].mNfree = I_0;
      }
      In m_fixed = int(pair_fixed * C_I_2) ;
      mCuts[i].mNfixed = m_fixed;
      mCuts[i].mQfixedNames.resize(m_fixed);
      mCuts[i].mQfixedValues.resize(m_fixed);
      for (In j=I_0;j<m_fixed;j++)
      {
         mCuts[i].mQfixedNames[j]=atoi(local_vector[(i_free+I_1)+j*I_2 + I_1].c_str());
         mCuts[i].mQfixedValues[j]=atof(local_vector[(i_free+I_1)+j*I_2 + I_2].c_str());
      }
     
      //resume of the files that are going to be crated
      
      Mout <<  "Cut Analysis: " << mCuts[i].mNfree <<"D with Free mode(s): " ;
      for (In k=I_0; k < mCuts[i].mNfree; k++  ) { Mout << mCuts[i].mQfreeNames[k] << "  " ;}
      Mout << " and with " << mCuts[i].mNfixed << " Fixed mode(s): ";
      for (In k=I_0; k < mCuts[i].mNfixed; k++  ) { Mout << mCuts[i].mQfixedNames[k] << "  " ;}
      Mout << " (with values " ;
      for (In k=I_0; k < mCuts[i].mNfixed; k++  ) { Mout << mCuts[i].mQfixedValues[k] << "  "  ;}
      Mout << ") " << endl ;
   }
   return ;
}


void CutAna::PrepareCutFile
   (  const std::vector<Nb>& mFreq 
   ,  const PesInfo& aPesInfo
   ) 
{
   const auto& aSaveIoDir   = aPesInfo.GetMultiLevelInfo().SaveIoDir();
   const auto& aAnalysisDir = aPesInfo.GetMultiLevelInfo().AnalysisDir();

   if (this->mNfree != I_0) // check for 0 dimensional plot & errors in the input line
   {
      string data_file_name = "Cut_"+std::to_string(this->mNfree)+"D_FREE_";
      for (In dim=I_0;dim<this->mNfree; dim++) {data_file_name +='q'+(std::to_string(this->mQfreeNames[dim])+'_'); }
      data_file_name += "FIXED";
      if (mNfixed == I_0) data_file_name += "_none" ;
      for (In dim=I_0;dim<mNfixed; dim++) 
      {
         data_file_name +=("_q"+std::to_string(mQfixedNames[dim])+"_v"+StringForNb(this->mQfixedValues[dim]));
      }
      data_file_name += ".mplot";

      //Mout << data_file_name << endl << endl;
      string plotname = aAnalysisDir + "/" + data_file_name;
      
      ofstream datafile;
      datafile.open(plotname.c_str(),ios_base::out); 
      datafile.setf(ios::scientific);
      datafile.setf(ios::uppercase);
      datafile.precision(22);


      In cutgrid = gPesCalcDef.GetmCutMesh()*I_2 + I_1 ;

      int arraysize= int(pow(double(cutgrid),this->mNfree)); 
      Nb** Xog;
      Xog = new Nb* [this->mNfree];
      for (In i=I_0; i<this->mNfree; i++) Xog[i]= new Nb [arraysize];
      Nb* Yog = new Nb [arraysize];
     
      for (In k= I_0; k< arraysize; k++ ) Yog[k] = C_0 ;
      
      

      // the plot area correspond to the defined grid.

      Nb factor ;
      In ind_xo;
      for (In dim=I_0; dim <this->mNfree; dim++)
      {
         MidasWarning("Changed the factor... Hope its correct... But I'm not sure CutAna is tested...");
         factor = aPesInfo.GetScalingInfo().GetRScalFact(this->mQfreeNames[dim]);
         factor /= gPesCalcDef.GetmCutMesh(); 
         ind_xo=I_0;
         In keck ;
         for (In kin = I_1 ; kin <= int(pow(double(cutgrid),dim)); kin++ )
         {
            for (In j = -gPesCalcDef.GetmCutMesh() ; j<= gPesCalcDef.GetmCutMesh() ; j++)
            {
               keck = int(pow(double(cutgrid),(this->mNfree - dim - I_1)));
               do 
               {
     // Xog contains the "grid" point where the potential will be evaluated             
                  Xog[dim][ind_xo] = j * factor ;
                  keck--;
                  ind_xo++;
               } while ( keck > 0);
            }
         } 
      }        


      In nmodes = mFreq.size() ;


   string oper_file_name = aSaveIoDir + "/prop_no_1.mop";
   ifstream oper_file(oper_file_name.c_str());
   if (oper_file.fail()) MIDASERROR(" File with operator not found");
   string s="  " ;

   getline(oper_file,s);
    if(s.find(" ")!= s.npos)
    {
       s.erase(s.find(" "),I_1);   // Delete ALL blanks
    }

    Nb* scalingfreq = new Nb [nmodes];
    for (In i = I_0; i < nmodes ; i++) scalingfreq[i] = I_1 ; 
    if (s.find("SCALINGFREQUENCIES")!=s.npos)
    {
    vector<string> temporal ;
          
       for (In i = I_0; i < nmodes ; i++)
       {
          getline(oper_file,s);
          istringstream input_s(s);
          string s_new;
          input_s >> s_new ;
          temporal.push_back(s_new);
          scalingfreq[i]=atof(temporal[i].c_str());
       }
    }

   
   
   while (!oper_file.eof())
   {
      while (s != "DALTON_FOR_MIDAS")
      {
         getline(oper_file,s);
      }
   
      while (getline(oper_file,s)) // get new lines with mode and operators end.
      {
        // running on the lines of the operator file
         istringstream input_s(s);
         Nb coef;
         vector<string> local_vector;
         local_vector.clear();
         string s_new;
         
         while (input_s >> s_new)  
         {
            local_vector.push_back(s_new);
         }
         coef = atof(local_vector[0].c_str());
         vector<In> exponent ;
         exponent.resize(nmodes) ;
         for (In i = I_0; i<exponent.size(); i++) { exponent[i]= I_0; }
         for (In j = I_1; j<local_vector.size(); j++)
         {
            // the vector with the exponent is constructed
            exponent[atoi(local_vector[j].c_str())-I_1]++ ;
         }

         vector<Nb> vales ;
         vales.resize(nmodes);
         for (In i = I_0; i<vales.size(); i++) {vales[i]= C_0 ; }

         for (In j = I_0; j < mNfixed; j++)
         {
            vales[mQfixedNames[j]] = this->mQfixedValues[j] *sqrt(scalingfreq[mQfixedNames[j]])* sqrt(C_FAMU) ; 
         }
         // run over the grid points
         for (In k= I_0 ; k <arraysize; k++)
         {
            for (In ifree= I_0 ; ifree < this->mNfree; ifree++)
            {
               vales[this->mQfreeNames[ifree]]= Xog[ifree][k] * sqrt(scalingfreq[mQfreeNames[ifree]])* sqrt(C_FAMU); 
            }

            Nb contr = coef  ;

            for (In iq = I_0 ; iq < vales.size() ; iq++)
            {
               // run over the modes
               if (exponent[iq] != I_0)
               {    
                  if (fabs(vales[iq]) < C_NB_EPSILON)
                  {
                     contr = C_0 ;
                  }
                  else
                  {                  
                     contr *= pow(vales[iq],exponent[iq]);
                  }
               }
            }
            Yog[k] += contr ;
         }
      }
   }
      
   // Dump the data files
   
      In spacer = I_0;
      for (In i=I_0; i< arraysize; i++)
      {
         if (spacer == cutgrid) datafile << endl , spacer=I_0 ;   
         for (In dim=I_0; dim <this->mNfree; dim++)
         {
            datafile << Xog[dim][i] << "   " ;
         }
         datafile << Yog[i] << endl ;
         spacer++ ; 
      }
      datafile.close();
      for (In i=I_0; i<this->mNfree; i++) delete [] Xog[i];
      delete [] Xog ;
      delete [] Yog ;
   }

     
   return;
}

