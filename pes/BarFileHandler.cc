/**
************************************************************************
* 
* @file                BarFileHandler.cc
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<string>
#include<vector>
#include<map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "util/Io.h"
#include "util/Isums.h"
#include "util/FileHandler.h"
#include "util/FileSystem.h"
#include "util/Timer.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "pes/BarFileHandler.h"
#include "pes/GridType.h"
#include "pes/Derivatives.h"
#include "pes/PesFuncs.h"
#include "pes/PesInfo.h"
#include "pes/CalcCode.h"
#include "pes/PropertyFileStorage.h"
#include "pes/SimpleRational.h"
#include "potentials/MidasPotential.h"

// using declarations
using std::string;
using std::vector;
using std::map;

typedef string::size_type Sst;
typedef pair<In,In> ipair;

/**
 * Constructor 
 *
 * @param arPesInfo
 **/
BarFileHandler::BarFileHandler
   (  PesInfo& arPesInfo
   )
   :  GridFileHandlerMethods(arPesInfo)
{
}

/**
* Intrinsict driver for actual displacements along coordinates for calculating either derivatives or coarse grid points
* */
void BarFileHandler::DoDispDer_zero
   (  const ModeCombiOpRange& ModeCom
   ,  In ActualMaxDimMc
   ,  const GridType& GpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_dodispder;
   mPesInfo.CleanUpDerCalcList();

   // The following mode couplings are considered:
   if (gPesIoLevel > I_10)
   {
      Mout << " ModeCombiOpRange " << std::endl << ModeCom << std::endl;
   }

   // Construct all N vectors with maximum ("worst case") entries. Important that the GridSetup has been called before ConstructAllNvecs
   // since the latter need information from the class GridType which is set in GridSetup
   std::map<ipair, ipair> n_vec_add_handler;
   std::vector<InVector> vec_of_all_n_vecs;

   In n_modes_coup = ActualMaxDimMc;

   if (gPesIoLevel > I_10)
   {
      Mout << " Now constructing all N-vectors " << std::endl;
   }

   ConstructAllNvecs(vec_of_all_n_vecs, n_vec_add_handler, n_modes_coup, gPesCalcDef.GetmPesNumMaxPolOrExp(), GpFr);
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Done constructing all N-vectors " << std::endl;
   }

   // Do now the K vectors on basis of the N vectors. All k are constructed but some may not be used actually later. 
   std::map<In,ipair> k_vec_add_handler;
   std::vector<InVector> vec_of_all_k_vecs;
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Now constructing all K-vectors " << std::endl;
   }

   ConstructAllKvecs(vec_of_all_n_vecs, n_vec_add_handler, vec_of_all_k_vecs, k_vec_add_handler);
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Done constructing all K-vectors " << std::endl;
   }

   // Open file for writing information about the order in the .mbar files
   In IbarCount = I_0;
   std::string zero = "*#0*";
   aCalculationList.AddEntry(zero);
   Timer msi_test_timer;
   
   // Then run over the set of modes
   for(const auto& mt: ModeCom)
   {
      if (mt.IsToBeScreened())
      {
         continue;
      }
      
      ipair ModeCombiOrder_Der = std::make_pair(mt.Size(),gPesCalcDef.GetmPesNumMaxPolOrExp());
      ipair beg_step;
      beg_step = n_vec_add_handler[ModeCombiOrder_Der];
      In beg = beg_step.first;
      In end = beg_step.first + beg_step.second;

      // W need to write some information to the offset file. Only relevant in case of grid calculations
      vector<In> local_com;
      local_com=mt.MCVec();

      // We compute the l.c.map here since it depends only on the actual mc.
      Timer time_all;
      for (In n=beg;n<end;++n)
      {
         ipair beg_step_k = k_vec_add_handler[n];
         In begk = beg_step_k.first;
         In endk = beg_step_k.first + beg_step_k.second;

         // Loop now over the set of k vectors and do the actual displacements (ModeDisplacements).
         for (In k=begk;k<endk;++k)
         {
            vector<In> kvec = vec_of_all_k_vecs[k];

            ModeDisplacements_zero(mt, kvec, GpFr, aCalculationList);
         }
      }
   }

   if (gTime) 
   {
      time_dodispder.CpuOut(Mout, "\n CPU time used in the DoDispDer_zero: ");
      time_dodispder.WallOut(Mout, " Wall time used in the DoDispDer_zero: ");
   }
}

/**
 * Do actual displacement  
**/
void BarFileHandler::ModeDisplacements_zero
   (  const ModeCombi& arModeCombi
   ,  const std::vector<In>& arKvec
   ,  const GridType& arGpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_ada;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR("Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

   string list_of_calcs = "";
   string full_dim_list_of_calcs = "";

   // Generate calculation name and return if we should skip.
   if (GenerateCalcName(list_of_calcs, full_dim_list_of_calcs, arKvec, arGpFr, arModeCombi))
   {
      return;
   }

   // Add the calculation to the calculation list
   if (!arModeCombi.IsToBeExtrap() || !gPesCalcDef.GetmPesDoExtrap())
   {
      aCalculationList.AddEntry(list_of_calcs);
   }
   else if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap())
   {
      mPesInfo.AddDerCalc(list_of_calcs);
   }
   
   if (gTime)
   {
      time_ada.CpuOut(Mout, "\n CPU time used in the ModeDisplacements_zero: ", true);
      time_ada.WallOut(Mout, " Wall time used in the ModeDisplacements_zero: ", true);
   }
}

/**
 * Intrinsict driver for actual displacements along coordinates for calculating either derivatives or coarse grid points
 *
 * @param ModeCom
 * @param ActualMaxDimMc
 * @param GpFr
 * @param arDerivatives
 **/
void BarFileHandler::DoDispDer_one
   (  const ModeCombiOpRange& ModeCom
   ,  In ActualMaxDimMc
   ,  const GridType& GpFr
   ,  const Derivatives& arDerivatives
   )
{
   Timer time_dodispder;
   map<ipair,ipair> n_vec_add_handler;
   vector<InVector> vec_of_all_n_vecs;
   In n_modes_coup=ActualMaxDimMc;

   if (gPesIoLevel > I_10)
   {
      Mout << " Now constructing all N-vectors " << std::endl;
   }

   ConstructAllNvecs(vec_of_all_n_vecs, n_vec_add_handler, n_modes_coup, gPesCalcDef.GetmPesNumMaxPolOrExp(), GpFr);
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Done constructing all N-vectors " << std::endl;
   }

   // Do now the K vectors on basis of the N vectors. All k are constructed but some may not be used actually later. 
   std::map<In, ipair> k_vec_add_handler;
   std::vector<InVector> vec_of_all_k_vecs;
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Now constructing all K-vectors " << std::endl;
   }

   ConstructAllKvecs(vec_of_all_n_vecs, n_vec_add_handler, vec_of_all_k_vecs, k_vec_add_handler);
   
   if (gPesIoLevel > I_10)
   {
      Mout << " Done constructing all K-vectors " << std::endl;
   }


   //Open the .mbar files
   OpenFiles();

   vector< pair<In,Nb> > Vib_Freqs;
   set<string> Test_Calc;

   // Open file for writing information about the order in the .mbar files

   In IbarCount = I_0;
   In IbarCountSave = I_0;
   std::string LL = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_bar_files_navigation.mpesinfo";
   ofstream BarInterface(LL.c_str(), ios_base::out);

   BarInterface << "# Mode(s) " << " Offset " << std::endl;

   // Set up with vector of PropertyFileStorage objects, each handling a property file.
   std::vector<PropertyFileStorage> vec_prop_file_stor;
   vec_prop_file_stor.reserve(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
   for (Uin i = I_0; i < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i)
   {
      vec_prop_file_stor.emplace_back(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(i + I_1) + "_bar.mpoints");
   }

   // Load all property files into memory if so requested.
   // NB! Ideally, ought to just have _one_ prop. file in memory at a time, but
   // this would require changing the existing loop structure, but it's not
   // worth the effort at the moment. But feel free to do so, if memory becomes
   // an issue. -MBH, Oct 2017.
   if (gPesCalcDef.GetmPesPropInMem())
   {
      for(auto&& pfs: vec_prop_file_stor)
      {
         pfs.ReadIntoMemory();
      }
   }

   // then run over the set of modes
   for(const auto& mt: ModeCom)
   {
      //Mout << " current mode: " << mt << " to be screened: " << mt.IsToBeScreened() << endl;
      //for MC screening neglect the calculation of the full MC
      //if ((gPesCalcDef.GetmPesScreenModeCombi() && mt.Size()==I_2 ))
      //{
         if (mt.IsToBeScreened())
         {
            continue;
         }
      //}
      // The mode combination is empty we can skip it
      if (mt.MCVec().size() == I_0)
      {
         continue;
      }
      ipair ModeCombiOrder_Der = std::make_pair(mt.Size(),gPesCalcDef.GetmPesNumMaxPolOrExp());
      ipair beg_step;
      beg_step = n_vec_add_handler[ModeCombiOrder_Der];
      In beg = beg_step.first;
      In end = beg_step.first + beg_step.second;


      //we need to write some information to the offset file. Only relevant in case of grid calculations
      if (gPesCalcDef.GetmPesDoExtrap() && mt.IsToBeExtrap())
      {
         continue; //do nothing if is to be extrapolated
      }
      IbarCountSave = IbarCount + I_1;
      
      BarInterface << " (";
      for (In i = I_0; i < mt.Size(); ++i)
      {
         BarInterface << mPesInfo.GetMolecule().GetModeLabel(mt.Mode(i));
         if (mt.Size() > I_1 && i != mt.Size() - I_1)
         {
            BarInterface << ",";
         }
      }
      BarInterface << ")";
      if (mt.Size() != I_0)
      {
         BarInterface << "    ";
      }
      
      // Determines if plotting of potentials have been requested. Open needed files
      vector<In> local_com;
      local_com = mt.MCVec();
      // Setup the plot variables and if needed open the plot files
      SetupPlot(local_com);

      //we compute the l.c.map here since it depends only on the actual mc.
      Timer time_all;
      map<InVector,Nb> lc_map;
      GenerateLinearCombinationMap(local_com,lc_map);
      //time_all.CpuOut(Mout,"\n CPU  time used in the costruction of the LinCom map: ");
      //time_all.WallOut(Mout," Wall time used in the costruction of the LinCom map: ");
      //Mout << " Linear combination: ";
      //for(map<InVector,Nb>::iterator ci=lc_map.begin(); ci!=lc_map.end();ci++)
      //Mout << ci->second << " " << ci->first << " ";
      //Mout << endl;
      //run over n

      for (In n=beg;n<end;++n)
      {
         ipair beg_step_k = k_vec_add_handler[n];
         In begk = beg_step_k.first;
         In endk = beg_step_k.first + beg_step_k.second;

         MidasVector Properties(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
         Properties.Zero();
         bool Match;

         //loop now over the set of k vectors and do the actual displacements (ModeDisplacements).

         for (In k=begk;k<endk;++k)
         {
            vector<Nb> Ncor;
            vector<In> kvec = vec_of_all_k_vecs[k];
            string calc_code="";
            string calc_no="";
            vector<MidasVector> grad_hess;
            for (In prop=I_0;prop<mPesInfo.GetMultiLevelInfo().GetNoOfProps();++prop)
            {
               In n_dim=I_0;
               MidasVector grad_hess_v;
               //if (GetUseDerInfo() && gPesCalcDef.GetmPesShepardInt() && AvailableDer(arDerivatives,prop))
               if (mPesInfo.GetmUseDerivatives() && gPesCalcDef.GetmPesShepardInt() )
               {
                  In order = gPesCalcDef.GetmPesShepardOrd();                     // At mc=1, It=1 we assume order gPesCalcDef.GetmPesShepardOrd() for all the prop
                  if (InquireFile(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_files.mpesinfo"))
                  {
                     string name_pr=mPesInfo.GetMultiLevelInfo().GetPropertyName(prop);    //adding 1 to match the number in these_prop_ecc ecc
                     order=arDerivatives.OrderDer(name_pr);
                  }
                  //Mout << " Shepard interpolation for mode combi: " << mc << endl;
                  if (order>I_0) n_dim+=mt.Size();
                  if (order==I_2) n_dim+=mt.Size()*(mt.Size()+I_1)/I_2;
                  else if (order>I_2) MIDASERROR(" Shepard order greater than 2 ");
               }
               grad_hess_v.SetNewSize(n_dim);
               grad_hess_v.Zero();
               grad_hess.push_back(grad_hess_v);
            }
            //Mout << " enter in ModeDisplacement with kvec: " << kvec << endl;
            Nb vgrid=C_0; //stores value of full potential on the selected point
            //Mout << " entered in modedisplacement " << endl;
            ModeDisplacements_one(mt,kvec,Properties,Match,Ncor,GpFr,vgrid,lc_map, /* calc_no, calc_code,*/ arDerivatives, grad_hess, vec_prop_file_stor);
            //Mout << " exit from modedisplacement " << endl;
            if (Match)
            {
               //Write date to .mbar and plot files as needed. Add one to IbarCount to be able to print offsets to file
               WriteDataToFiles(Ncor, grad_hess, Properties, kvec, arDerivatives, vgrid);
               ++IbarCount;
            }
         }
      }
      if ( (mt.Size() != I_0) ) BarInterface << IbarCountSave << " " << IbarCount << endl;
   } //end run over modecombi

   BarInterface.close();
   //mAlreadyIn=false;
   if (gPesIoLevel > I_10)
   {
      Mout << " DoDispDer_one: Done! " << std::endl;
   }
   if(gTime) 
   {
      time_dodispder.CpuOut(Mout,"\n CPU  time used in the DoDispDer_one: ");
      time_dodispder.WallOut(Mout," Wall time used in the DoDispDer_one: ");
   }
}

/**
* Calculate derivative (no grid) or V-bar potentials (grid) 
* */
void BarFileHandler::ModeDisplacements_one
   (  const ModeCombi& arModeCombi
   ,  const std::vector<In>& arKvec
   ,  MidasVector& arProperties
   ,  bool& arMatch
   ,  std::vector<Nb>& arNCor
   ,  const GridType& arGpFr
   ,  Nb& vgrid
   ,  map<InVector,Nb>& arLinCom
   //,  string& arCalcNo
   //,  string& arCalcCode
   ,  const Derivatives& arDerivatives
   ,  vector<MidasVector>& arGradHess
   ,  const std::vector<PropertyFileStorage>& arVecPropFileStor
   )
{
   Timer time_ada;

   arMatch = true;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR(" Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

// If gPesCalcDef.GetmPesGrid() we need to know the address of the MC in order to find the grid information in class GridType.
// In case of derivative force field Adr simply keep its value of -1.
   In add=arModeCombi.Address();
   string list_of_calcs = "";
   string full_dim_list_of_calcs = "";
   //MidasVector VectorOfNormalCoord(3*mNoOfNuclei);
   //vector<Nuclei> DisCoord(mNoOfNuclei);
   //DisCoord  = mRefCoord;

   //Name              = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + Name;
   //If the following function returns true we have nothing left to do here...
   if(GenerateCalcName(list_of_calcs, full_dim_list_of_calcs, arKvec, arGpFr,arModeCombi))
   {
      arMatch = false;
      return;
   }
   //list_of_calcs = "*" + list_of_calcs + "*";
   //now do the actual displacements ...

   arMatch = true;
   bool calc_todo=true;
   //if (gPesCalcDef.GetmPesScreenModeCombi() && arModeCombi.IsToBeScreened()) calc_todo=false;
   if (arModeCombi.IsToBeScreened()) calc_todo=false;
   // Eduard to Daniele: IS THIS NEEDED ANYMORE?
   if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap()) calc_todo=false;
   //Mout << " for k vec: " << arKvec << " calc_todo: " << calc_todo << endl;
   if (!calc_todo)
   {
      arMatch = false;
      return;
   }
// find out which k value belongs to which mode. If just one k element
// is zero then the bar(V) term is zero   

   bool skip = false;
   map<In,In> mode_k;
   for (In i=I_0;i<arKvec.size();++i)
   {
      mode_k[arModeCombi.Mode(i)] = arKvec[i];
      if (arKvec[i] == I_0) skip = true;
   }

// find linear combination in terms of lower order potentials on file/in
// memory. if "**" is found the Search will return (double) zero.

// what is the current MC vector ?
   vector<In> mc=arModeCombi.MCVec();

   string list_of_calcs_1;
   map<string,Nb> code_coef;

   Timer time_all;
   for(map<InVector,Nb>::iterator ci=arLinCom.begin(); ci!=arLinCom.end(); ++ci)
   {
      list_of_calcs_1 = "";
      InVector mode_vec=ci->first;
      for (In i=I_0;i<mode_vec.size();++i)
      {
         In i_mode=mode_vec[i];
         string s1=std::to_string(i_mode+1);
         string s2;
//       where is this mode (i_mode) in MC ?
         In lsave=-I_1;
         for (In l=I_0;l<mc.size();++l)
            if (mc[l]==mode_vec[i]) lsave=l;

         In den = (arGpFr.GridPoints(add)[lsave])/I_2;  // notice the use of lsave to acces the grid here
         In num = mode_k[i_mode];

//       remember to simpify the expression as much as possible
         s2=PesFuncs::SimplifyFraction(num,den);

//       if "top" <= I_0 use LeftFractions else if "top" > I_0 use RightFractions

         In idx_frac=I_0;
         if (num<=I_0) idx_frac=lsave;
         if (num>I_0)  idx_frac=lsave+mc.size();

         string frac = arGpFr.Fractions(add)[idx_frac]; // notice the use of idx_frac to acces the Fractioning here
         PesFuncs::StringMultiplication(frac,den);

         if (!gPesCalcDef.GetmPesShepardInt() || (gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd()==I_0))
         {
            if (skip && (abs(mode_k[i_mode]) <= den) ) break;
         }
         if (skip && (abs(mode_k[i_mode]) > den) )
         {
            arMatch = false;
            return;
         }

         if ( abs(mode_k[i_mode]) <= den )
            list_of_calcs_1=list_of_calcs_1 + "#"+s1+"_"+s2;
         else
            list_of_calcs_1="DO_NOT_CALCULATE";
      }
      list_of_calcs_1 = "*" + list_of_calcs_1 + "*";
      string::size_type loc = list_of_calcs_1.find("DO_NOT_CALCULATE",0);
      if( loc != string::npos )
      {
         arMatch = false;
         return;
      }
      else
      {
         code_coef[list_of_calcs_1]=ci->second;
      }
   }
   //write out linear combination, i.e. V-bar in terms of the V's:
   if (gPesIoLevel > I_11 || gDebug)
   {
      Mout << "bar(" << full_dim_list_of_calcs << ")" << " = " << "   ";
      for(map<string, Nb>::iterator Msn = code_coef.begin(); Msn != code_coef.end(); ++Msn)
      {
         Mout.setf(ios::scientific);
         Mout.setf(ios::uppercase);
         Mout.precision(I_1);
         Mout << (*Msn).second << " " << (*Msn).first << " ";
      }
      Mout << std::endl;
      Mout.precision(I_22);
   }

   //time_all.CpuOut(Mout,"\n CPU  time used in the costruction of code_coef map: ");
   //time_all.WallOut(Mout," Wall time used in the costruction of code_coef map: ");

   MidasVector temp(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
   temp.Zero();

   for(map<string,Nb>::iterator Msn=code_coef.begin(); Msn!=code_coef.end(); ++Msn)
   {
      bool warn=true;
      bool warn1=true;
      bool warn2=true;
      bool test=true;
      bool test_d=true;

      for (In i=I_0;i<mPesInfo.GetMultiLevelInfo().GetNoOfProps();++i)
      {

         In prop_calc_num = I_0;
         Nb prop = C_0;
         if ((*Msn).first == "**")
         {
            if (!gPesCalcDef.GetmPesShepardInt() || (gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd()==I_0))
            prop=C_0;
         }
         else
         {
            //look in general if a k vec is in the origin and polish the calculation code accordingly
            string calc_code="";
            //mPesInfo.SetCodeFromString(calc_code,(*Msn).first);
            SetCodeFromString(calc_code,(*Msn).first);

            // if the first property is not found in the appropriate prop_no_X_bar.mpoints there is no need to 
            // perform the test for the subsequent properties. 
            if (test)
            {
               test = ExtractPropertyLine(arVecPropFileStor.at(i), calc_code, prop, prop_calc_num);
            }

            if (!test)
            {
               if (warn && (gPesIoLevel > I_10))
               {
                  Mout << " Warning: I did not find property " << i + I_1
                       << " for " << (*Msn).first << "  " << calc_code << std::endl;
               }
               warn = false;

               // Wooooow, what happens if getting to here, i.e. if !test
               // and !gPesCalcDef.GetmPesCalcPotFromOpFile()?? Then the
               // value of prop is zero, which could be very wrong!!
               // -MBH, June 2018
               std::stringstream ss;
               ss << "Didn't find calc code " << calc_code
                  << " so prop has not been set accordingly; now prop = " << prop
                  << ". This is probably NOT what you want;"
                  << " consider using #2POTVALSFROMOPFILE (see manual)."
                  ;
               MidasWarning(ss.str());
            }
         }
         temp[i]=temp[i]+Nb((*Msn).second)*prop;
         
         // Shepard interpolation only for the energy now..
         if (  mPesInfo.GetmUseDerivatives() 
            && gPesCalcDef.GetmPesShepardInt() 
            && midas::filesystem::Exists(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_files.mpesinfo")
            )
         {
            std::string name_pr = mPesInfo.GetMultiLevelInfo().GetPropertyName(i);
            if (arDerivatives.AvailableDer(name_pr))
            {
               DerivativesHelper(name_pr, prop_calc_num, Msn, mc, i, arDerivatives, arGradHess);
            }
         }

         if (gPesIoLevel > I_14)
         {
            Mout << " Gradient and Hessian data for calc code: " << full_dim_list_of_calcs << std::endl;
            Mout << arGradHess[i] << std::endl;
         }
      }
   }
   //time_all.CpuOut(Mout,"\n CPU  time used in the actual calculation of the l.c.: ");
   //time_all.WallOut(Mout," Wall time used in the actual calculation of the l.c.: ");
   //find out the full potential for plotting
   if (mLocalPlot)
   {
      bool test = ExtractPropertyLine(arVecPropFileStor.at(0), list_of_calcs, vgrid);
      if (!test && gPesIoLevel > I_14)
      {
         Mout << " Warning: did not find full potential for " << list_of_calcs << " calculation code " << std::endl;
      }
   }

   // Construct coordinate for the specific bar(V):
   arNCor.resize(arKvec.size());
   In i = I_0;
   for (std::map<In,In>::iterator p = mode_k.begin(); p != mode_k.end(); ++p)
   {
      In but = (arGpFr.GridPoints(add)[i])/I_2;
      Nb lg_point = Nb((*p).second)/Nb(but);
      if (lg_point > C_0)
      {
         arNCor[i] = mPesInfo.GetScalingInfo().GetRScalFact((*p).first)*lg_point;
      }
      else
      {
         arNCor[i] = mPesInfo.GetScalingInfo().GetLScalFact((*p).first)*lg_point;
      }
      //cout << "arNCor[i] : " << arNCor[i] << " = " << mScalFactVector[(*p).first] << " * " << lg_point << endl;
      ++i;
   }
   arProperties = temp;

   if (gTime)
   {
      time_ada.CpuOut(Mout, "\n CPU time used in the ModeDisplacements_one: ", true);
      time_ada.WallOut(Mout, " Wall time used in the ModeDisplacements_one: ", true);
   }
}

void BarFileHandler::DerivativesHelper(const string& name_pr,In aPropCalcNum,map<string,Nb>::iterator& Msn,
   vector<In>& mc,const In& i,const Derivatives& arDerivatives,vector<MidasVector>& arGradHess)
{
   // Mout << " "<< name_pr << " has shepard order " << arDerivatives.OrderDer(name_pr) << endl;
   std::vector<Nb> der_cont;
   std::map<In,In> der_cont_handler;
   //if(!mAlreadyIn) {
   //   mAlreadyIn=true;
   arDerivatives.GetDerivatives(name_pr, der_cont, der_cont_handler);
   //}
   /*
   vector<Nb> der_cont;
   map<In,In> der_cont_handler;
   arDerivatives.GetDerivatives(name_pr, der_cont, der_cont_handler);
   */
   if (mPesInfo.GetmUseDerivatives() && gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd()>I_0)
   {
      MidasVector q_grad_in(mPesInfo.GetMolecule().GetNoOfVibs());
      q_grad_in.Zero();
      In pos=der_cont_handler[aPropCalcNum];
      for (In i_e=I_0; i_e<mPesInfo.GetMolecule().GetNoOfVibs(); ++i_e) q_grad_in[i_e]=der_cont[i_e+pos];
      MidasMatrix q_hess_in(I_0);
      if (arDerivatives.OrderDer(name_pr)>I_1)
      {
         q_hess_in.SetNewSize(mPesInfo.GetMolecule().GetNoOfVibs(),false,true);
         q_hess_in.Zero();
         In j_pos=I_0;
         pos+=mPesInfo.GetMolecule().GetNoOfVibs();
         for (In i_r=I_0; i_r<q_hess_in.Nrows(); ++i_r)
         {
            for (In i_c=I_0; i_c<q_hess_in.Ncols(); ++i_c)
            {
               q_hess_in[i_r][i_c]=der_cont[pos+j_pos];
               ++j_pos;
            }
         }
      }
      //construct derivative informations
      if (gPesIoLevel > I_10)
      {
         Mout << " Retrieving derivative infos on calc no: " << aPropCalcNum << " calc code: " << (*Msn).first << std::endl;
         Mout << q_grad_in << std::endl;
         if (arDerivatives.OrderDer(name_pr) > I_1)
         {
            Mout << q_hess_in << std::endl;
         }
      }
      vector<string> k_vec_s;
      vector<In> mc_vec_s;
      //string calc_code="";
      //mPesInfo.SetCodeFromString(calc_code,(*Msn).first);
      //mPesInfo.GetInfoFromString((*Msn).first,mc_vec_s,k_vec_s);
      GetInfoFromString((*Msn).first,mc_vec_s,k_vec_s);
      //Mout << " For code: " << (*Msn).first << " mode_combi: " << mc_vec_s << endl;
      //Mout << " For code: " << calc_code << " calc. #: " <<  aPropCalcNum << " mc_vec_s: " << mc_vec_s << " k_vec_s: " << k_vec_s << endl;
      //Mout << " q_gradient: " << endl;
      //Mout << q_grad_in << endl;
      //Mout << " q_hessian: " << endl;
      //Mout << q_hess_in << endl;
      //now retrieve the derivatives of Vbar with respect to the pertaining modes
      In i_g_pos=I_0;
      for (In i_g_mode=I_0; i_g_mode<mc.size(); ++i_g_mode)
      {
         vector<In> curr_mc;
         curr_mc.push_back(mc[i_g_mode]);
         //Mout << " Current gradient mc: " << curr_mc << endl;
         vector<In> vec_mode_int;
         vec_mode_int.clear();
         std::set_intersection(curr_mc.begin(),curr_mc.end(),mc_vec_s.begin(),mc_vec_s.end(), std::back_inserter(vec_mode_int));
         //Mout << " Intersection for " << curr_mc << " and " << mc_vec_s << " is: " << vec_mode_int << endl;
         //Mout << " Gradient contribution: " << q_grad_in[mc[i_g_mode]] << endl;
         //Mout << " coef: " << Nb((*Msn).second) << endl;
         if (vec_mode_int.size()!=I_0) arGradHess[i][i_g_pos]+=Nb((*Msn).second)*q_grad_in[mc[i_g_mode]];
         //Mout << " final result: " << arGradHess[i][i_g_pos] << endl;
         ++i_g_pos;
      }
      if (arDerivatives.OrderDer(name_pr)==I_2)
      {
         for (In i_g_mode=I_0; i_g_mode<mc.size(); ++i_g_mode)
         {
            for (In j_g_mode=i_g_mode; j_g_mode<mc.size(); ++j_g_mode)
            {
               vector<In> curr_mc;
               curr_mc.push_back(mc[i_g_mode]);
               if (j_g_mode!=i_g_mode) curr_mc.push_back(mc[j_g_mode]);
               //Mout << " Current hessian mc: " << curr_mc << endl;
               vector<In> vec_mode_int;
               vec_mode_int.clear();
               std::set_intersection(curr_mc.begin(),curr_mc.end(),mc_vec_s.begin(),mc_vec_s.end(), std::back_inserter(vec_mode_int));
               //Mout << " Intersection for " << curr_mc << " and " << mc_vec_s << " is: " << vec_mode_int << endl;
               //Mout << " hessian contribution: " << q_hess_in[mc[i_g_mode]][mc[j_g_mode]] << endl;
               //Mout << " coef: " << Nb((*Msn).second) << endl;
               if (vec_mode_int==curr_mc) arGradHess[i][i_g_pos]+=Nb((*Msn).second)*q_hess_in[mc[i_g_mode]][mc[j_g_mode]];
               //Mout << " final result: " << arGradHess[i][i_g_pos] << endl;
               ++i_g_pos;
            }
         }
      }
   }
}

/**
 * Construct vector of all Kvectors
 * */
void BarFileHandler::ConstructAllKvecs
   (  vector<InVector>& arNvecVec
   ,  map<ipair, ipair>& arNvecAddMap
   ,  vector<InVector>& arKvecVec
   ,  map<In,ipair>& arKvecAddMap
   )
{
   arKvecVec.clear();
   arKvecAddMap.clear();

   for (map<ipair,ipair>::const_iterator ci=arNvecAddMap.begin();ci!=arNvecAddMap.end();++ci)
   {
      In add =ci->second.first;
      In n_n =ci->second.second;
      for (In i=add;i<add+n_n;++i)
      {
         In n_dim=arNvecVec[i].size(); // dim of mc,n,and k.
         InVector i_vec_cur(n_dim,I_0);
         InVector limits(n_dim,I_0);
         InVector nvec=arNvecVec[i];
         for (In j=I_0;j<n_dim;++j)
         {
            limits[j]=nvec[j];
         }

         In level=I_0;
         In n_bef=arKvecVec.size();
         AddKvecsToVec(arKvecVec, i_vec_cur, limits,nvec,level);
         In n_aft=arKvecVec.size();
         In added=n_aft-n_bef;
         ipair begnum(n_bef,added);
         arKvecAddMap[i]=begnum;
      }
   }

// Output
/*
   for (map<ipair,ipair>::const_iterator ci=arNvecAddMap.begin();ci!=arNvecAddMap.end();ci++) 
   {
      In n_mc =ci->first.first;
      In n_der=ci->first.second;
      In add =ci->second.first;
      In n_n =ci->second.second;
      for (In i=add;i<add+n_n;i++) 
      {
         Mout << " Set of K-vectors For N-vector ";
         Mout << " " << arNvecVec[i] << endl;
         ipair kad=arKvecAddMap[i];
         In add2 =kad.first;
         In n_n2 =kad.second;
         Mout << n_n2 << " elements from " << add2 << endl;
         for (In j=add2;j<add2+n_n2;j++) 
            Mout << " " << arKvecVec[j] << endl;
      }
   }
*/
}

/**
 * Construct vector of all Nvectors
 **/
void BarFileHandler::ConstructAllNvecs
   (  vector<InVector>& arNvecVec
   ,  map<ipair,ipair>& arNvecAddMap
   ,  In aNmxMc
   ,  In aNmxDer
   ,  const GridType& aGpFr
   )
{
   arNvecVec.clear();
   arNvecAddMap.clear();

   std::vector<In> max = aGpFr.MaxInEachDim();

   for (In n_mc = I_1; n_mc <= aNmxMc; ++n_mc)
   {
      InVector i_vec_cur(n_mc);
      for (In i = I_0; i < n_mc; ++i)
      {
         i_vec_cur[i] = max[n_mc]/I_2;
      }
         
      In n_bef=arNvecVec.size();
      arNvecVec.push_back(i_vec_cur);
      In n_aft=arNvecVec.size();
      In added=n_aft-n_bef;
      ipair mcder(n_mc,aNmxDer);
      ipair begnum(n_bef,added);
      arNvecAddMap[mcder]=begnum;
   }
   /*
      Mout << " Final set of Nvectors constructed" << endl;
      for (map<ipair,ipair>::const_iterator ci=arNvecAddMap.begin();ci!=arNvecAddMap.end();ci++)
      {
      In n_mc =ci->first.first;
      In n_der=ci->first.second;
      In add =ci->second.first;
      In n_n =ci->second.second;
      Mout << " n_mc = " << n_mc << " n_der = " << n_der << endl;
      for (In i=add;i<add+n_n;i++) Mout << " " << arNvecVec[i] << endl;
      }
      Mout << " Final set of Nvectors constructed, unsorted " << endl;
   for (In i=0;i<arNvecVec.size();i++) Mout << arNvecVec[i] << endl;
   */
}


/**
 * Recursive addition to set of k-vectors 
 * */
void BarFileHandler::AddKvecsToVec(vector<InVector>& arKvecVec, InVector aInVec,
      const InVector& arKlimits, const InVector& arNvec, In aLevel)
{
   for (In i=-arKlimits[aLevel];i<=arKlimits[aLevel];++i)
   {
      aInVec[aLevel]=i;
      if (aLevel< (arKlimits.size()-1))
      {
         AddKvecsToVec(arKvecVec, aInVec, arKlimits, arNvec, aLevel+I_1);
      }
      else
      {
         arKvecVec.push_back(aInVec);
      }
   }
}

/**
 * Recursive addition to vector of Nvectors
 * */
void BarFileHandler::AddNvecsToVec(vector<InVector>& arNvecVec, InVector aInVec, In& arNmc, In aLevel, In& arRestMax)
{
   for (In i_t=I_0;i_t<=arRestMax;++i_t)
   {
      In a_rest = arRestMax-i_t;
      aInVec[aLevel]= I_1+i_t;
      if (aLevel==(arNmc-I_1) || a_rest== I_0 ) // we have reached end, add to list
      {
         arNvecVec.push_back(aInVec);
      }
      else
      {
         AddNvecsToVec(arNvecVec,aInVec,arNmc,aLevel+1,a_rest);
      }
   }
}

/**
 * Generate calculation name / calculation code ( CalcCode ).
 *
 * @return Returns whether or not the point should be skipped. (?)
 **/
bool BarFileHandler::GenerateCalcName
   (  std::string& arCalcName
   ,  std::string& arFullCalcName
   ,  const std::vector<In>& arKvec
   ,  const GridType& arGpFr
   ,  const ModeCombi& arModeCombi
   )
{
   In add=arModeCombi.Address();
   vector<SimpleRational> displacements;
   vector<In> modes;
   vector<SimpleRational> full_displacements;
   vector<In> full_modes;
   for (In i=I_0;i<arKvec.size();++i)
   {
      In but = (arGpFr.GridPoints(add)[i])/I_2;
      In IndexFrac = i;
      if (arKvec[i]>I_0)  IndexFrac = i + arKvec.size();
      string Fraction = arGpFr.Fractions(add)[IndexFrac];
      PesFuncs::StringMultiplication(Fraction,but);
      full_displacements.emplace_back(arKvec[i], (arGpFr.GridPoints(add)[i])/I_2);
      full_modes.push_back(arModeCombi.Mode(i));
      //Do only points that are required 
      //bool le_but=abs(arKvec[i])<=but;
      //for extrapolated modes we also consider strings with zeroes to be dumped and then put equal to zero..
      if (!gPesCalcDef.GetmPesDoExtrap() || !arModeCombi.IsToBeExtrap() )
      {
         if ( (abs(arKvec[i]) <= but ) && (arKvec[i] != I_0) )
         {
            displacements.emplace_back(arKvec[i], (arGpFr.GridPoints(add)[i])/I_2);
            modes.push_back(arModeCombi.Mode(i));
            //arNoOfUpdates += 1;
         }
         else if ( (arKvec.size() == I_1) && (arKvec[i] == I_0) )
         {
            arCalcName = "#0";
            //arNoOfUpdates += 1;
         }
         else if ( (arKvec.size() != I_1) && (InNorm2(arKvec) == I_0) )
         {
            arCalcName = "#0";
         }
      }
      else if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap())
      {
         //consider also the zeroes in the current imp. but only with the correct dimensionality
         if  (abs(arKvec[i]) > but ) return true;
         if ( (abs(arKvec[i]) <= but ))
         {
            displacements.emplace_back(arKvec[i], (arGpFr.GridPoints(add)[i])/I_2);
            modes.push_back(arModeCombi.Mode(i));
            //arNoOfUpdates += 1;
         }
      }
   }
   if("#0" == arCalcName)
   {
      CalcCode new_calc(modes, displacements);
      CalcCode full_calc(full_modes,full_displacements);
      arCalcName = new_calc.GetString();
      arFullCalcName = full_calc.GetString();
      return false;
   }
   if(modes.size() == I_0)
      return true;
   //if (arCalcName == "")
   //   return true;
   CalcCode new_calc(modes, displacements);
   CalcCode full_calc(full_modes,full_displacements);
   arCalcName = new_calc.GetString();
   arFullCalcName = full_calc.GetString();
   return false;
}
