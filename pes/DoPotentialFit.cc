/**
************************************************************************
*
* @file                DoPotentialFit.cc
*
* Created:             11-10-2012
*
* Authors:             Bo Thomsen (bothomsen@chem.au.dk), original implementation
*                      Emil Lund Klinting (klint@chem.au.dk), separation of functionalities
*
* Short Description:   Driver for fitting routines and interpolation methods, resulting in the creation of .mop files
*
* Last modified: Thu Jul 16, 2018
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// midas headers
#include "pes/DoPotentialFit.h"

#include "pes/adga/ItGridHandler.h"

using namespace BarHandles;
using namespace PesFuncs;

/**
 * 
 * @param aFitBasisCalcDef
 * @param aPesCalcDef
 * @param aPesInfo
 **/
DoPotentialFit::DoPotentialFit
   (  const FitBasisCalcDef& aFitBasisCalcDef
   ,  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   ) 
   :  mFitBasisCalcDef(aFitBasisCalcDef)
   ,  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo) 
   ,  mFitFunctions(aFitBasisCalcDef)
   ,  mDoConstructFitBasis(true)
   ,  mAutoGenNoFuncs(mPesInfo.GetMolecule().GetNoOfVibs())
{

}

/**
 * Perform spline or modified Shepard interpolation and fitting with the purpose of obtaining a functional form of potential energy or molecular property surfaces.
 *
 * @param arModeCom
 * @param arGpFr
 * @param arDerivatives
 * @param arUseDer
 * @param arOneModeGridBounds
 * @param apIterGrid
 * @param arCalculationList
 * @param arNmodes
 * @param arSkipMcInFit
 **/
void DoPotentialFit::GetAnalyticalSurfaces
   (  const ModeCombiOpRange& arModeCom
   ,  const GridType& arGpFr
   ,  const Derivatives& arDerivatives
   ,  const bool& arUseDer
   ,  const std::vector<std::vector<Nb>>& arOneModeGridBounds
   ,  const ItGridHandler* apIterGrid
   ,  const CalculationList& arCalculationList
   ,  const In& arNmodes
   ,  const std::vector<bool>& arSkipMcInFit
   )
{
   // Set scaling and frequencies for later fg of the potential/property surfaces
   DumpPotential mop_dump_handler(mPesCalcDef, mPesInfo);
   mop_dump_handler.Initialize(arModeCom, arOneModeGridBounds, arNmodes, arCalculationList.GetReferenceProperties());
   
   // Look for interpolation/fitting details about inertia moments 
   auto inertia_props = GetInertiaProps();
   
   // BarFileInterface class is used to read data from bar files
   BarFileInterface bar_file_interface(mPesCalcDef, mPesInfo);
   // First we need to set the storage method for how the bar-file information should be treated
   bar_file_interface.Initialize();

   //Set the number of digits that will appear for numbers on fg
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, Mout);

   // We can output the calulated single points, (note that this is the CG which we fit to) to a file in the analysis directory
   if (mPesCalcDef.GetmPesPlotRealESP() && mPesCalcDef.GetmPesAdga())
   {
      if (gPesIoLevel > I_7)
      {
         Mout << " Plotting non-fitted points for all mode combinations and properties " << std::endl << std::endl;
      }

      PlotNonFittedPoints(mop_dump_handler, bar_file_interface, arModeCom, arOneModeGridBounds, arUseDer, arDerivatives);
   }
   
   // The fit-basis inly needs to be constructed once and this is the place where we do it
   if (mDoConstructFitBasis)
   {
      // Construct the fit-basis to be used in the linear fit to the grid of single points
      ConstructFitBasis(arModeCom, bar_file_interface);
   }

   // Output with result from the fit-basis construction
   if (gFitBasisIoLevel > I_14)
   {
      Mout << mFitFunctions << std::endl;
   }

   // Setup threading for fitting routine. If mNumThreads is not a positive
   // number, it signifies that all available threads should be used. This is
   // signaled to midas::concurrency::SetActive() by passing a -1.
   if (mFitBasisCalcDef.GetmUseSpecialNthreadsFit())
   {
      const auto& n_threads = mFitBasisCalcDef.GetmNthreadsFit();
      midas::concurrency::SetActive(n_threads > I_0 ? n_threads : -I_1);
   }
   std::vector<std::future<void> > futures;

   if (gPesIoLevel > I_7)
   {
      Mout << std::endl << "  **** Summary of linear least squares fitting in this iteration **** " << std::endl;
   }

   // We are done setting up, now we run over each mode combination and check if some should be excluded
   for (const auto& mode_combi: arModeCom)
   {
      // The mode combination is empty and we can skip it
      if (mode_combi.Size() == I_0)
      {
         continue;
      }
      //
      if (mode_combi.IsToBeScreened()) 
      {  
         continue;
      }
      // If Adga pes then check for mode restrictions
      if (mPesCalcDef.GetmPesAdga())
      {
         // If size of the current mode combination exceeds the current mode combination level then the fitting routine is done for this iteration
         if (mode_combi.Size() > arNmodes) 
         {
            break;
         }
      }

      // Loop over each property defined for the individual mode combination 
      for (In i_bar = I_0; i_bar < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i_bar)
      {

         bool derivatives = false;
         std::string name_pr = mPesInfo.GetMultiLevelInfo().GetPropertyName(i_bar);
         if (arUseDer && arDerivatives.AvailableDer(name_pr))
         {
            derivatives = true;
         }
         
         // No refitting is required unless some indication of extrapolation/interpolation step using derivatives is present
         if (  mPesCalcDef.GetmPesAdga()
            && !mFitBasisCalcDef.GetmFitAllMcIterGrids() 
            && arSkipMcInFit[mode_combi.Address() - I_1]
            && !derivatives
            && mPesCalcDef.GetmExtrapolateFrom() != arNmodes
            )
         {
            if (gPesIoLevel > I_7)
            {
               Mout << " For mode combination " << mode_combi.MCVec() << " will not fit to property " << name_pr << " as no new points have been added to the associated grid " << std::endl;
            }
            
            continue;
         }

         if (gPesIoLevel > I_7)
         {
            Mout << " For mode combination " << mode_combi.MCVec() << ", fit to property " << name_pr;
            if (mPesInfo.GetmUseDerivatives() && derivatives)
            {
               Mout << " with derivative information " << std::endl;
            }
            else
            {
               Mout << " without using derivative information " << std::endl;
            }
         }

         // We have to skip all the properties we do not have derivatives for when we get to the extrapolation step
         if (mode_combi.IsToBeExtrap() && !derivatives) 
         {
            if (gPesIoLevel > I_10)
            {
               Mout << " Prop no. " << i_bar + I_1 << " can't be extrapolated" << std::endl;
            }
            continue; 
         }
     
         // Done working out that our data isn't corrupted, now we begin to interpole and/or fit each property
         futures.emplace_back
            (  midas::concurrency::Post
                  (
                     [  &mop_dump_handler
                     ,  &bar_file_interface
                     ,  &arCalculationList
                     ,  apIterGrid
                     ,  mode_combi
                     ,  i_bar
                     ,  &arGpFr
                     ,  name_pr
                     ,  &inertia_props
                     ,  &arDerivatives
                     ,  derivatives
                     ,  this
                     ]()
                     {
                        DoInterpFit
                           (  mop_dump_handler
                           ,  bar_file_interface
                           ,  arCalculationList
                           ,  apIterGrid
                           ,  mode_combi
                           ,  i_bar
                           ,  arGpFr
                           ,  name_pr
                           ,  inertia_props
                           ,  mOperatorTerms
                           ,  arDerivatives
                           ,  derivatives
                           ); 
                     }
                  )
            );
      }  
   }
  
   // Wait for all MC/property combinations to be fitted
   for (const auto& f : futures)
   {
      f.wait();
   }

   // Release the number of threads to be set to default
   if (mFitBasisCalcDef.GetmUseSpecialNthreadsFit())
   {
      midas::concurrency::SetActive();
   }
   
   // We can output all the presently known operator terms if needed
   if (gPesIoLevel > I_10)
   { 
      Mout << std::endl << " The following operator terms will be dumped to the following operator files: " << std::endl; 
      for (auto const& operator_term : mOperatorTerms)
      {
         Mout << "  prop_no_" << std::get<I_0>(operator_term.first) + I_1 << ".mop: ";

         for (In i = I_0; i < std::get<I_2>(operator_term.second).size(); ++i)
         {
            Mout << std::get<I_1>(operator_term.second)[i]
                 << "(" << std::get<I_2>(operator_term.second)[i] << ")"
                 << " ";
         }

         Mout << "= " 
              << std::showpos
              << std::get<I_0>(operator_term.second)
              << std::noshowpos;

         if (gDebug)
         {
            Mout << ", key = "
                 << std::get<I_0>(operator_term.first) << ","
                 << std::get<I_1>(operator_term.first) << ","
                 << std::get<I_2>(operator_term.first);
         }

         Mout << std::endl;
      }
   }

   if (gPesIoLevel > I_4)
   {
      if (mPesCalcDef.GetmPesDoInt() || mPesCalcDef.GetmPesDoIntForExtrap())
      {
         Mout << std::endl << " Interpolation and fitting procedures are completed " << std::endl;
      }
      else
      {
         Mout << std::endl << " Fitting procedure is completed " << std::endl;
      }
   }

   //! Harmonic frequencies obtained from the fitted potential
   std::vector<Nb> harmonic_terms(mPesInfo.GetMolecule().GetNoOfVibs(), C_0);

   // All operator terms belonging to the different property surfaces needs to be dumped to files
   for (In prop_no = I_0; prop_no < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++prop_no)
   { 
      for (auto const& operator_term : mOperatorTerms)
      {
         auto term_property = std::get<I_0>(operator_term.first);
         if (prop_no == term_property)
         {
            mop_dump_handler.DumpSurfaceTerm(prop_no, operator_term.second);
         }

         // Pick out the diagonal elements of the Hessian matrix to obtain the harmonic frequencies
         if (prop_no == I_0 && term_property == I_0)
         {
            auto function = std::get<I_1>(operator_term.second);
            if (function.size() == I_1 && function[I_0] == "Q^2")
            {
               
               harmonic_terms[std::get<I_1>(operator_term.first) - I_1] = std::get<I_0>(operator_term.second);
            }
         }
      }
   }
   
   // Output and dump harmonic frequencies from the fitted potential
   mop_dump_handler.OadHarmonicFreqs(harmonic_terms);
  
   //
   mFitFunctions.SetConstAndFunct(mop_dump_handler.GetOperFiles());
}

/**
 * This function will construct the fit-basis for all modes
 *
 * @param arModeCombiRange    The mode combination range
 * @param aBarFileInterface   Interface for reading MidasCpp .mbar files
**/
void DoPotentialFit::ConstructFitBasis
   (  const ModeCombiOpRange& arModeCombiRange
   ,  const BarFileInterface& aBarFileInterface
   )
{
   mDoConstructFitBasis = false;
   
   if (gFitBasisIoLevel > I_8)
   {
      Mout << std::endl << "  **** Constructing the complete one-mode fit-basis **** " << std::endl;
   }
   
   std::vector<In> one_mode_fit_basis = mFitBasisCalcDef.GetmFitFuncsMaxOrder(); 
   
   for (const auto& mode_combi: arModeCombiRange)
   {
      // The mode combination is empty and we can skip it
      if (mode_combi.Size() == I_0)
      {
         continue;
      }
      // The fit-basis is only defined for one-mode mode combinations, higher order mode combinations have their fit-basis constructed from the modes that are included in them
      if (mode_combi.Size() > I_1)
      {
         break;
      }
      InVector mode_combi_vec = mode_combi.MCVec();
     
      FunctionContainer<Nb> func_cont;                    
      FunctionContainer<taylor<Nb,1,1> > taylor_func_cont;

      // If the vector of gen fit basis defs are empty for the mode (mode_combi.Mode(I_0)) then we don't do anything
      auto use_gen_fit_basis_def = false;
      if (mFitBasisCalcDef.GetmUseGenFitBasis())
      {
         use_gen_fit_basis_def = !mFitBasisCalcDef.GetmGenFitBasisModeMap()[mode_combi.Mode(I_0)].empty();
      }

      // Check for the use of general fit-basis functions
      if (use_gen_fit_basis_def)
      {
         // Get the general fit-basis definitions one at a time from the vector
         for (auto&& gen_def_nr : mFitBasisCalcDef.GetmGenFitBasisModeMap()[mode_combi.Mode(I_0)] )
         {
            auto gen_fit_basis_defs = mFitBasisCalcDef.GetmGenFitBasisCalcDefs()[gen_def_nr];
        
            if (gen_fit_basis_defs.GetmNonLinOptFunc().empty())
            {
               if (gFitBasisIoLevel > I_8)
               {
                  Mout << " A one-mode fit-basis set with general fit functions but without non-linear optimization of function parameters will be generated for mode " 
                     << mode_combi_vec << " and property " << gen_fit_basis_defs.GetmFitProperties() << std::endl;
               }

               mFitFunctions.InsertBasis
                  (  gen_fit_basis_defs.GetmFitProperties()
                  ,  std::vector<In>(I_1, mode_combi.Mode(I_0))
                  ,  std::shared_ptr<BaseFitBasisSet>
                     (  new GeneralFitBasisSet
                        (  gen_fit_basis_defs.GetmGenFitFunctions()
                        ,  func_cont
                        )
                     )
                  );
            }
            else
            {
               if (gFitBasisIoLevel > I_8)
               {
                  Mout << " A one-mode fit-basis set with general fit functions and non-linear optimization of function parameters will be generated for mode " 
                     << mode_combi_vec << " and property " << gen_fit_basis_defs.GetmFitProperties() << std::endl; 
               }

               mFitFunctions.InsertBasis
                  (  gen_fit_basis_defs.GetmFitProperties()
                  ,  std::vector<In>(I_1, mode_combi.Mode(I_0))
                  ,  std::shared_ptr<BaseFitBasisSet>
                     (  new GeneralOptFitBasisSet
                        (  gen_fit_basis_defs.GetmGenFitFunctions()
                        ,  gen_fit_basis_defs.GetmNonLinOptParams()
                        ,  gen_fit_basis_defs.GetmNonLinOptStartGuess()
                        ,  gen_fit_basis_defs.GetmNonLinOptFunc()
                        ,  func_cont
                        ,  taylor_func_cont
                        ,  mode_combi.Mode(I_0)
                        ,  gen_fit_basis_defs.GetmNonLinOptThr()
                        )
                     )
                  );
            }
         }

         // Check that the mode number(s) exist 
         MidasAssert(mFitFunctions.CheckModeNumbers(mPesInfo.GetMolecule().GetNoOfVibs()), "Mode chosen in #4 Modes under #3 BasDef does not exist!");
      }
      // Determine the one-mode fit-basis automatically
      else if (mFitBasisCalcDef.GetmAutomaticFitBasis())
      {
         AutoOneModeFitBasis(aBarFileInterface, mode_combi, func_cont, taylor_func_cont);  
      }
      else
      {
         if (gFitBasisIoLevel > I_8)
         {
            Mout << " A one-mode fit-basis set with polynomial fit functions will be generated for mode " << mode_combi_vec << std::endl;
         }

         mFitFunctions.InsertBasis
            (  std::vector<std::string>(I_1, "ALL")
            ,  std::vector<In>(I_1, mode_combi.Mode(I_0))
            ,  std::shared_ptr<BaseFitBasisSet>
               (  new PolyFitBasisSet(one_mode_fit_basis.at(mode_combi.Mode(I_0)))
               )
            );
      }
   }

   // Construct special fit-basis for fitting effective inertia inverse tensor components 
   if (mPesCalcDef.GetmPesCalcMuTens())
   {
      std::vector<In> one_mode_mu_fit_basis = mFitBasisCalcDef.GetmFitFuncsMuMaxOrder(); 

      for(const auto& mode_combi: arModeCombiRange)
      {
         // The mode combination is empty and we can skip it
         if (mode_combi.Size() == I_0)
         {
            continue;
         }
         if (mode_combi.Size() > I_1)
         {
            break;
         }
         InVector mode_combi_vec = mode_combi.MCVec();
         
         mFitFunctions.InsertBasis
            (  std::vector<std::string>(I_1, "EFFINERTIAINV")
            ,  std::vector<In>(I_1, mode_combi.Mode(I_0))
            ,  std::shared_ptr<BaseFitBasisSet>
               (  new PolyFitBasisSet(one_mode_mu_fit_basis[mode_combi.Mode(I_0)])
               )
            );
      }
   }
  
   // Check for the possible presence of constants specified on input
   auto fit_basis_constants = mFitBasisCalcDef.GetmFitBasisConstants();  
   if (!fit_basis_constants.empty())
   {
      FunctionContainer<Nb> func_cont;                    
      FunctionContainer<taylor<Nb,1,1> > taylor_func_cont;

      auto it_map_of_constants = mFitBasisCalcDef.GetmFitBasisConstants().begin();
      func_cont.InsertConstant(it_map_of_constants->first, it_map_of_constants->second);
      taylor_func_cont.InsertConstant(it_map_of_constants->first, taylor<Nb,1,1>(it_map_of_constants->second));

      mFitFunctions.AddToConstants(fit_basis_constants);
   }
   
   // Check for the possible presence of functions specified on input
   auto fit_basis_functions = mFitBasisCalcDef.GetmFitFuncs();
   if (!fit_basis_functions.empty())
   {
      FunctionContainer<Nb> func_cont;                    
      FunctionContainer<taylor<Nb,1,1> > taylor_func_cont;

      auto it_map_of_functions = mFitBasisCalcDef.GetmFitFuncs().begin();
      func_cont.InsertFunction(it_map_of_functions->first, it_map_of_functions->second);
      taylor_func_cont.InsertFunction(it_map_of_functions->first, it_map_of_functions->second);

      mFitFunctions.AddToFunctions(fit_basis_functions);
   }
}

/**
 * This function seeks to determine if a given mode might be fitted with one of the types of fit functions indicated on input and then generates a set of fit-basis functions for it
 *
 * @param aBarFileInterface   Interface for reading MidasCpp .mbar files
 * @param aModeCombi          The (one-)mode combination under consideration
 * @param aFuncCont           A function container for the fit-basis functions
 * @param aDerFuncCont        A function container for derivatives 
 **/
void DoPotentialFit::AutoOneModeFitBasis
   (  const BarFileInterface& aBarFileInterface
   ,  const ModeCombi& aModeCombi
   ,  const FunctionContainer<Nb>& aFuncCont
   ,  const FunctionContainer<taylor<Nb,1,1> >& aDerFuncCont
   )
{
   auto mode_combi_vec = aModeCombi.MCVec();
   if (gFitBasisIoLevel > I_8)
   {
      Mout << " A one-mode fit-basis set will be generated for mode " << mode_combi_vec << std::endl; 
   }

   // Get the required information from the .mbar files
   MidasVector sp_bar_property;
   aBarFileInterface.GetSinglePointBarPropertyForMc
      (  aModeCombi
      ,  I_0
      ,  sp_bar_property
      );

   if (sp_bar_property.Size() != I_3)
   {
      MIDASERROR("Automatic fit function detemination failed, too many points in the .mbar file");
   }

   Nb property_diff = sp_bar_property[I_0] - sp_bar_property[I_2];
   if (gFitBasisIoLevel > I_10)
   {
      Mout << "  The difference in property value at the current potential boundaries is: " << property_diff << std::endl;
   }
   
   // Loop over all types indicated on input to be of interest
   auto auto_fit_basis_defs = mFitBasisCalcDef.GetmGenFitBasisCalcDefs();
   for (In i = I_0; i < auto_fit_basis_defs.size(); ++i)
   {
      // If the one-mode potential is sufficiently unsymmetrical, then we use optimized Morse functions
      if (  mFitBasisCalcDef.GetmAutoFitType()[i] == "MORSE"
         && std::abs(property_diff) > mFitBasisCalcDef.GetmAutoSimiValue()[i]
         )
      {
         if (gFitBasisIoLevel > I_8)
         {
            Mout << "  Detected an unsymmetrical one-mode potential, will use optimized Morse functions " << std::endl << std::endl;
         }
      
         // Adapt the non-linear optimization start guess to the actual shape of the potential
         auto  non_lin_opt_start_guess = auto_fit_basis_defs[i].GetmNonLinOptStartGuess();
         if (property_diff < I_0)
         {
            non_lin_opt_start_guess[I_1] *= C_M_1;
         }
      
         mFitFunctions.InsertBasis
            (  auto_fit_basis_defs[i].GetmFitProperties()
            ,  std::vector<In>(I_1, aModeCombi.Mode(I_0))
            ,  std::shared_ptr<BaseFitBasisSet>
               (  new GeneralOptFitBasisSet
                  (  auto_fit_basis_defs[i].GetmGenFitFunctions()
                  ,  auto_fit_basis_defs[i].GetmNonLinOptParams()
                  ,  non_lin_opt_start_guess
                  ,  auto_fit_basis_defs[i].GetmNonLinOptFunc()
                  ,  aFuncCont
                  ,  aDerFuncCont
                  ,  aModeCombi.Mode(I_0)
                  ,  auto_fit_basis_defs[i].GetmNonLinOptThr()
                  )
               )
            );
         
         // One-mode fit-basis defined, we can return 
         return;
      }
   }
   // Fallthrough to standard one-mode polynomial fit-basis construction if nothing else can be done
   if (gFitBasisIoLevel > I_8)
   {
      if (std::abs(property_diff) < C_I_10_14)
      {
         Mout << "  Detected a symmetrical one-mode potential, will use polynomial functions of both even and odd powers " << std::endl << std::endl;
      }
      else
      {
         Mout << "  Detected a semi-symmetrical one-mode potential, will use polynomial functions of both even and odd powers " << std::endl << std::endl;
      }
   }
   
   auto one_mode_fit_basis = mFitBasisCalcDef.GetmFitFuncsMaxOrder(); 
   
   mFitFunctions.InsertBasis
      (  std::vector<std::string>(I_1, "ALL")
      ,  std::vector<In>(I_1, aModeCombi.Mode(I_0))
      ,  std::shared_ptr<BaseFitBasisSet>
         (  new PolyFitBasisSet(one_mode_fit_basis.at(aModeCombi.Mode(I_0)))
         )
      );
   
   // One-mode fit-basis defined by default option, we can return
   return;
}

/**
 * Function which takes care of interpolation and/or fitting for individual mode combinations and properties
 *
 * @param aDumpPotential
 * @param aBarFileInterface
 * @param aCalculationList
 * @param apIterGrid
 * @param aModeCombi
 * @param aBarNumber
 * @param aGridPointFractions
 * @param aPropertyName
 * @param aInertiaProps
 * @param aOperatorTerms
 * @param aDerivatives
 * @param aAvailableDerivatives
 **/
void DoPotentialFit::DoInterpFit
   (  DumpPotential& aDumpPotential
   ,  const BarFileInterface& aBarFileInterface
   ,  const CalculationList& aCalculationList
   ,  const ItGridHandler* apIterGrid
   ,  const ModeCombi& aModeCombi
   ,  const In& aBarNumber
   ,  const GridType& aGridPointFractions
   ,  const std::string aPropertyName
   ,  const std::pair<In, In>& aInertiaProps
   ,  std::map<std::tuple<In, In, std::vector<In> >, std::tuple<Nb, std::vector<std::string>, std::vector<std::string> > >& aOperatorTerms
   ,  const Derivatives& aDerivatives
   ,  const bool& aAvailableDerivatives
   )  const
{
   In cg_points = I_0;
   std::vector<MidasVector> cg_bar_coords;
   MidasVector cg_bar_property;
   // If some kind of interpolation is required before performing the fitting then we also need to supply derivative information
   std::vector<MidasVector> cg_bar_coords_uncompressed;
   std::vector<MidasVector> cg_bar_derivatives;

   // If the mode combi is one of those which have been extrapolated it wont contain any derivative information
   bool derivatives = false;
   if(aAvailableDerivatives && aModeCombi.Size() <= mPesInfo.GetExtrapFrom())
   {
      derivatives = true;
   }

   // Get the required information from the .mbar files
   aBarFileInterface.GetSinglePointBarDataForMc
      (  aModeCombi
      ,  aBarNumber
      ,  cg_points
      ,  cg_bar_coords
      ,  cg_bar_coords_uncompressed
      ,  cg_bar_property
      ,  cg_bar_derivatives
      ,  derivatives
      ,  mPesCalcDef.GetmPesShepardInt()
      );

   // If we have a Machine Learning representation of the PES we can extract some useful information for fitting
   std::vector<Nb> cg_ml_variances;
   if (mPesCalcDef.GetMLPes()) 
   {   
       aBarFileInterface.GetVarianceForMc(aModeCombi, aBarNumber, cg_points, cg_ml_variances);
   }

   // The cg_bar_sigma is a vector that will contain the variance for chi^2 values after the linear least-squares procedure have been carried out. 
   MidasVector cg_bar_sigma(cg_points, C_1);

   // Construct the fine grid (FG) and store these as fg_bar_coords and fg_bar_property, which are the variables we will be working with directly for the interpolation and fitting procedures
   In fg_points = I_1;
   std::vector<MidasVector> fg_bar_coords;
   fg_bar_coords.assign(aModeCombi.Size(), MidasVector());
   MidasVector fg_bar_property;
   if (GridGen(aModeCombi, aAvailableDerivatives))
   {
      std::vector<MidasVector> fg_bar_coords_tmp;
      fg_bar_coords_tmp.assign(aModeCombi.Size(), MidasVector());

      // Construct the FG according to the non-ADGA (static grid) procedure
      if (!mPesCalcDef.GetmPesAdga())
      {
         fg_bar_coords = GenNonAdgaFG(aModeCombi, cg_bar_coords, fg_bar_coords_tmp);
         for (In i_dim = I_0; i_dim < aModeCombi.Size(); i_dim++)
         {
            fg_points *= fg_bar_coords[i_dim].Size();
         }
      }
      // Construct the FG according to the ADGA procedure
      else   
      {
         fg_bar_coords = GenAdgaFG(aModeCombi, cg_bar_coords, fg_bar_coords_tmp);
         for (In i_dim = I_0; i_dim < aModeCombi.Size(); i_dim++)
         {
            fg_points *= fg_bar_coords[i_dim].Size();
         }
      }
   }
   // We cannot generate a fine grid and use the present grid as it is
   else 
   {
      fg_points = cg_points;
      for (In i_dim = I_0; i_dim < aModeCombi.Size(); i_dim++)
      {
         fg_bar_coords[i_dim].SetNewSize(cg_bar_coords[i_dim].Size());
         fg_bar_coords[i_dim] = cg_bar_coords[i_dim];
      }
   }
   fg_bar_property.SetNewSize(fg_points, C_0);
   MidasVector fg_bar_sigma(fg_points, C_1);

   if (gDebug || gPesIoLevel > I_10)
   {
      Mout << " Number of CG points : " << cg_points << std::endl;
      Mout << " Number of FG points : " << fg_points << std::endl;
   
      // Output the entire fine grid (or course grid in case no fine grid points were generated)
      if (gPesIoLevel > I_14)
      {
         OutputXiGCoord(aModeCombi, cg_bar_coords);
      }
   }

   // If possible, get information from lower MC's about derivatives for use in shepard scheme
   std::vector<MidasVector> cg_bar_coords_ghosts;
   MidasVector cg_bar_property_ghosts;
   std::vector<MidasVector> cg_bar_deriv_ghosts;
   if (  aModeCombi.Size() > I_1 
      && mPesInfo.GetmUseDerivatives() 
      && aAvailableDerivatives  
      && mPesCalcDef.GetmPesAdga() 
      && mPesCalcDef.GetmPesShepardInt()
      )
   {
      DoInterpolation interp_ghosts(mPesCalcDef, mPesInfo);
      auto ghost_grid_points = interp_ghosts.GetLowerMCDerInfo(aModeCombi, aDerivatives, aPropertyName, aBarNumber, aGridPointFractions, apIterGrid, aCalculationList);

      auto cg_bar_coords_ghosts_tmp = std::get<I_0>(ghost_grid_points);
      auto cg_bar_property_ghosts_tmp = std::get<I_1>(ghost_grid_points);
      auto cg_bar_deriv_ghosts_tmp = std::get<I_2>(ghost_grid_points);

      cg_bar_coords_ghosts.resize(cg_bar_coords_ghosts_tmp.size());
      cg_bar_property_ghosts.SetNewSize(cg_bar_property_ghosts_tmp.Size());
      cg_bar_deriv_ghosts.resize(cg_bar_deriv_ghosts_tmp.size());

      if (cg_bar_coords_ghosts_tmp.size() != cg_bar_deriv_ghosts_tmp.size())
      {
         MIDASERROR("Conflicting sizes of ghost grid and ghost derivative grids");
      }

      for (In i = I_0; i <cg_bar_coords_ghosts_tmp.size(); ++i)
      {
        cg_bar_coords_ghosts[i].SetNewSize(cg_bar_coords_ghosts_tmp[i].Size());
        cg_bar_coords_ghosts[i] = cg_bar_coords_ghosts_tmp[i];
         
         cg_bar_deriv_ghosts[i].SetNewSize(cg_bar_deriv_ghosts_tmp[i].Size());
         cg_bar_deriv_ghosts[i] = cg_bar_deriv_ghosts_tmp[i];
      }
      cg_bar_property_ghosts = cg_bar_property_ghosts_tmp;
   }

   // Are we going to do some interpolation before we fit?
   if (!OnlyFit(aModeCombi, aAvailableDerivatives))
   {
      DoInterpolation interp(mPesCalcDef, mPesInfo);
      interp.GetInterpolatedPoints
         (  aBarNumber
         ,  aModeCombi
         ,  cg_bar_coords
         ,  fg_bar_coords
         ,  cg_bar_derivatives
         ,  cg_bar_coords_ghosts
         ,  cg_bar_property_ghosts
         ,  cg_bar_deriv_ghosts
         ,  cg_bar_property
         ,  fg_bar_property
         ,  fg_bar_sigma
         ,  cg_bar_coords_uncompressed
         ,  cg_points
         );
   }
   else 
   {
      fg_bar_property = cg_bar_property;
      fg_bar_sigma = cg_bar_sigma;
   }

   // Scale vibrational coordinates if requested
   if (mPesCalcDef.GetmPesFreqScalCoordInFit() && !mPesCalcDef.GetmPesScalCoordInFit())
   {
      for (In k = I_0; k < fg_bar_coords.size(); k++)
      {
         // Old 'historic' scaling (for old tests)
         if (mPesCalcDef.HistoricAdgaScaling())
         { 
            fg_bar_coords[k].Scale(std::sqrt(mPesInfo.GetMolecule().GetFreqI(aModeCombi.Mode(k))/(C_AUTKAYS)));
         }
         // New scaling (to be used for all future purposes)
         else
         { 
            fg_bar_coords[k].Scale(std::sqrt(mPesInfo.GetMolecule().GetFreqI(aModeCombi.Mode(k))*C_FAMU/(C_AUTKAYS)));
         }
      }

      for (In k = I_0; k < cg_bar_coords.size(); k++)
      {
         // Old 'historic' scaling (for old tests)
         if (mPesCalcDef.HistoricAdgaScaling())
         { 
           cg_bar_coords[k].Scale(std::sqrt(mPesInfo.GetMolecule().GetFreqI(aModeCombi.Mode(k))/(C_AUTKAYS)));
         }
         // New scaling (to be used for all future purposes)
         else
         { 
           cg_bar_coords[k].Scale(std::sqrt(mPesInfo.GetMolecule().GetFreqI(aModeCombi.Mode(k))*C_FAMU/(C_AUTKAYS)));
         }
      }
   }
   else if (mPesCalcDef.GetmPesScalCoordInFit())
   {
      for (In k = I_0; k < fg_bar_coords.size(); k++)
      {
         fg_bar_coords[k].Scale(C_1/aDumpPotential.GetScalFact(aModeCombi.Mode(k)));
      }

      for (In k = I_0; k < cg_bar_coords.size(); k++)
      {
         cg_bar_coords[k].Scale(C_1/aDumpPotential.GetScalFact(aModeCombi.Mode(k)));
      }
   }

   // Set up fitting routine
   DoFitting fitting_routine(mFitBasisCalcDef, mPesCalcDef, mPesInfo);
   std::string type_pr = aPropertyName.substr(aPropertyName.find_last_of("_") + I_1);
   std::vector<InVector> basis_func_orders;
   
   // Proceed to the fitting routine
   auto potential_coefficients = fitting_routine.DoFit
      (  aBarNumber
      ,  aModeCombi
      ,  cg_bar_coords
      ,  fg_bar_coords
      ,  fg_bar_property
      ,  fg_bar_sigma
      ,  mFitFunctions
      ,  basis_func_orders
      ,  type_pr
      ,  aInertiaProps
      ,  aAvailableDerivatives
      ,  mAutoGenNoFuncs
      );

   // Save data on energy/property surfaces to be dumped on file when fitting routine is finished
   aDumpPotential.SetSurfaceTerm(type_pr, basis_func_orders, mFitFunctions, aModeCombi, aBarNumber, potential_coefficients, aOperatorTerms);
}

/**
 * Investigate if inertia moments are meant to be interpolated or fitted 
**/
std::pair<In, In> DoPotentialFit::GetInertiaProps() const
{
   std::string s_this = "";
   In frst_inertia_prop = -I_1;
   In last_inertia_prop = -I_1;
   if (mPesCalcDef.GetmPesCalcMuTens() || mPesInfo.GetMultiLevelInfo().GetNoOfProps() > I_1)
   {
      if (  mPesInfo.GetmUseDerivatives() 
         && mPesCalcDef.GetmPesShepardInt() 
         && (mPesCalcDef.GetmPesShepardOrd() > I_0)
         )
      {
         if (gPesIoLevel > I_8)
         {
            Mout << " Properties other than energy will be interpolated by using an order zero for the Shepard interpolants " << std::endl;
            Mout << " The order used for the energy is " << mPesCalcDef.GetmPesShepardOrd() << std::endl;
         }
      }
      s_this = "XX-1_EFFINERTIAINV";
      frst_inertia_prop = FindPropertyNumberInFile(mPesInfo, s_this, mPesCalcDef.GetmPesExcState()) - I_1;
      s_this = "ZZ-1_EFFINERTIAINV";
      last_inertia_prop = FindPropertyNumberInFile(mPesInfo, s_this, mPesCalcDef.GetmPesExcState()) - I_1;
      if (frst_inertia_prop == -I_1 && mPesCalcDef.GetmPesCalcMuTens())
      {
         // Investigate the possibility of a linear molecule
         std::string s_this = "MU_LINEAR_INERTIA";
         frst_inertia_prop = FindPropertyNumberInFile(mPesInfo, s_this, mPesCalcDef.GetmPesExcState()) - I_1;
         if (frst_inertia_prop != -I_1) 
         {
            last_inertia_prop = frst_inertia_prop;
         }
      }
   }
   
   return std::make_pair(frst_inertia_prop, last_inertia_prop);
}

/**
 *
**/
bool DoPotentialFit::GridGen
   (  const ModeCombi& arMt
   ,  bool aDerivatives
   )  const
{
   bool grid_gen = mPesCalcDef.GetmPesDoInt();
   if (mPesCalcDef.GetmPesDoExtrap() && arMt.Size() > mPesInfo.GetExtrapFrom())
   {
      grid_gen = mPesCalcDef.GetmPesDoIntForExtrap();
   }
   
   if (mPesCalcDef.GetmPesAdga())
   {
      if (mPesInfo.GetmUseDerivatives() && aDerivatives)
      {
         grid_gen = true;
         if (gPesIoLevel > I_10)
         {
            Mout << " Within the ADGA procedure, we use Derivative info for the FG" << std::endl;
         }
      }
      else
      {
         grid_gen = false;
         if (gPesIoLevel > I_10)
         {
            Mout << " Within the ADGA procedure, the FG is not used without Derivative info" << std::endl;
         }
      }
   }
   
   if (  mPesCalcDef.GetmPesAdga() 
      && mPesCalcDef.GetmPesDoExtrap() 
      && arMt.Size() > mPesInfo.GetExtrapFrom()
      )
   {
      grid_gen = false;

      if (gPesIoLevel > I_10)
      {
         Mout << " For the extrapolated surface, within the ADGA: no FG" << std::endl;
      }
   }

   if (mPesCalcDef.GetmPesAdga() && arMt.IsToBeExtrap() )
   {
      grid_gen = false;

      if (gPesIoLevel > I_10)
      {
         Mout << " For the extrapolated surface, within the ADGA: no FG" << std::endl;
      }
   }

   return grid_gen;
}

/**
 *
 **/
std::vector<MidasVector> DoPotentialFit::GenNonAdgaFG
   (  const ModeCombi& arMt
   ,  const std::vector<MidasVector>& aXig
   ,  std::vector<MidasVector>& aXog
   )  const
{
   if (gPesIoLevel > I_10)
   {
      Mout << " Generate fine grid for static grid " << std::endl;
   }

   for (In i_dim = I_0; i_dim < arMt.Size(); i_dim++)
   {
      In no_of_steps = (aXig[i_dim].Size() - I_1) * mPesCalcDef.GetmPesFGMesh()[arMt.Size() - I_1];
      MidasVector grid_tmp(no_of_steps + I_1, C_0);

      In i_elem = I_0;
      for (In i_pnt = I_1; i_pnt < aXig[i_dim].Size(); i_pnt++)
      {
         Nb step_size = (aXig[i_dim][i_pnt] - aXig[i_dim][i_pnt - I_1]) / mPesCalcDef.GetmPesFGMesh()[arMt.Size() - I_1];
         for (In i_step = I_0; i_step < mPesCalcDef.GetmPesFGMesh()[arMt.Size() - I_1]; i_step++)
         {
            grid_tmp[i_elem] = aXig[i_dim][i_pnt - I_1] + Nb(i_step)*step_size;
            i_elem++;
         }
      }

      grid_tmp[grid_tmp.Size() - I_1] = aXig[i_dim][aXig[i_dim].Size() - I_1];
      //sort out zeroes
      std::list<Nb> ls_grid_tmp;
      In lst = aXig[i_dim].Size()-I_1;

      // Generalized for Adga procedure with bounds not forced to be equal
      In i_mode = arMt.Mode(i_dim);
      Nb fg_scale_factor = C_0;
      if (mPesCalcDef.GetmDincrSurface())
      {
         fg_scale_factor = mPesCalcDef.GetmPesFGScalFact()[I_0];
      }
      else
      {
         fg_scale_factor = mPesCalcDef.GetmPesFGScalFact()[i_mode];
      }

      Nb down_bound = aXig[i_dim][I_0] * fg_scale_factor;
      Nb up_bound = aXig[i_dim][lst] * fg_scale_factor;

      if (up_bound < down_bound)
      {
         Nb swap = down_bound;
         down_bound = up_bound;
         up_bound = swap;
      }
      for (In k=I_0; k<grid_tmp.Size(); k++)
      {
         if (grid_tmp[k]<=up_bound && grid_tmp[k]>=down_bound)
            ls_grid_tmp.push_back(grid_tmp[k]);
      }

      ls_grid_tmp.sort();
      ls_grid_tmp.unique();
      grid_tmp.SetNewSize(ls_grid_tmp.size(),false);
      In indx_cnt = I_0;
      for (std::list<Nb>::const_iterator indx = ls_grid_tmp.begin(); indx != ls_grid_tmp.end(); indx++)
      {  
         grid_tmp[indx_cnt] = *indx;
         indx_cnt++;
      }
      aXog[i_dim].SetNewSize(grid_tmp.Size());
      aXog[i_dim] = grid_tmp;

      if (gPesIoLevel > I_11)
      {
         Mout << " No of FG points in dimension  " << i_dim + I_1 << "  is " << aXog[i_dim].Size()  << std::endl;
      }
   }

   return aXog;
}

/**
 *
 **/
std::vector<MidasVector> DoPotentialFit::GenAdgaFG
   (  const ModeCombi& arMt
   ,  const std::vector<MidasVector>& aXig
   ,  std::vector<MidasVector>& aXog
   )  const
{
   if (gPesIoLevel > I_10)
   {
      Mout << " Generate fine grid for Adga grid " << std::endl;
      Mout << " Step in the construcion of the grid: " << mPesCalcDef.GetmPesAdgaFgStep() << std::endl;
   }

   for (In i_dim = I_0; i_dim < arMt.Size(); i_dim++)
   {
      std::vector<Nb> grid_tmp;
      for (In i_pnt = I_1; i_pnt < aXig[i_dim].Size(); i_pnt++)
      {
         // The position of the points. 
         if (i_pnt == I_1 && arMt.Size() == I_1)
         {
            grid_tmp.push_back(aXig[i_dim][i_pnt - I_1] - mPesCalcDef.GetmPesAdgaFgStep()*((aXig[i_dim][i_pnt] - aXig[i_dim][i_pnt - I_1])));
         }

         grid_tmp.push_back(aXig[i_dim][i_pnt - I_1] + I_0*((aXig[i_dim][i_pnt] - aXig[i_dim][i_pnt - I_1])));
         grid_tmp.push_back(aXig[i_dim][i_pnt - I_1] + mPesCalcDef.GetmPesAdgaFgStep()*((aXig[i_dim][i_pnt] - aXig[i_dim][i_pnt - I_1])));

         grid_tmp.push_back(aXig[i_dim][i_pnt - I_1] + (C_1 - mPesCalcDef.GetmPesAdgaFgStep())*((aXig[i_dim][i_pnt] - aXig[i_dim][i_pnt - I_1])));
 
         if (i_pnt == aXig[i_dim].Size() - I_1)
         {
            grid_tmp.push_back(aXig[i_dim][aXig[i_dim].Size() - I_1]);
            if (arMt.Size() == I_1)
            {
               grid_tmp.push_back(aXig[i_dim][aXig[i_dim].Size() - I_1] + mPesCalcDef.GetmPesAdgaFgStep()*(aXig[i_dim][aXig[i_dim].Size() - I_1] - aXig[i_dim][aXig[i_dim].Size() - I_2]));
            }
         }
      }

      //sort out zeroes
      std::list<Nb> ls_grid_tmp;

      for (In k = I_0; k < grid_tmp.size(); k++)
      {
         ls_grid_tmp.push_back(grid_tmp[k]);
      }

      ls_grid_tmp.sort();
      ls_grid_tmp.unique();
      grid_tmp.resize(ls_grid_tmp.size(),false);
      In indx_cnt = I_0;
      for (std::list<Nb>::const_iterator indx = ls_grid_tmp.begin(); indx != ls_grid_tmp.end(); indx++)
      {
         grid_tmp[indx_cnt] = *indx;
         indx_cnt++;
      }
      aXog[i_dim].SetNewSize(grid_tmp.size());
      aXog[i_dim] = MidasVector(grid_tmp);
      if (gPesIoLevel > I_10)
      {
         Mout << " No of FG points in dimension  " << i_dim + I_1 << "  is " << aXog[i_dim].Size() << std::endl;
      }
   }
   return aXog;
}

/**
 *
**/
void DoPotentialFit::PlotNonFittedPoints
   (  const DumpPotential& aDumpPotential
   ,  const BarFileInterface& aBarFileInterface
   ,  const ModeCombiOpRange& aModeCombiRange
   ,  const std::vector<std::vector<Nb>>& aOneModeGridBounds
   ,  const bool& aUseDer
   ,  const Derivatives& arDerivatives
   )  const
{
   // Loop through the mode combinatio range and plot for each mode combination
   for (const auto& mode_combi: aModeCombiRange)
   {
      if (mode_combi.Size() == I_0)
      {
         continue;
      }

      // Only plot for points calculated in the non-coupled part of the potential
      if (mode_combi.Size() > I_1)
      {
         break;
      }
     
      // Loop through all properties for each mode combination
      for (In bar_prop = I_0; bar_prop < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++bar_prop)
      {
         bool derivatives = false;
         std::string name_pr = mPesInfo.GetMultiLevelInfo().GetPropertyName(bar_prop);
         if (aUseDer && arDerivatives.AvailableDer(name_pr))
         {
            derivatives = true;
         }

         In cg_points = I_0;
         std::vector<MidasVector> cg_bar_coords;
         MidasVector cg_bar_property;
         // If some kind of interpolation is required before performing the fitting then we also need to supply derivative information
         std::vector<MidasVector> cg_bar_coords_uncompressed;
         std::vector<MidasVector> cg_bar_derivatives;

         // Get the required information from the .mbar files
         aBarFileInterface.GetSinglePointBarDataForMc
            (  mode_combi
            ,  bar_prop
            ,  cg_points
            ,  cg_bar_coords
            ,  cg_bar_coords_uncompressed
            ,  cg_bar_property
            ,  cg_bar_derivatives
            ,  derivatives
            ,  mPesCalcDef.GetmPesShepardInt()
            );

         // If we have a Machine Learning representation of the PES we can extract some useful information for fitting
         std::vector<Nb> cg_ml_variances;
         if (mPesCalcDef.GetMLPes()) 
         {   
             aBarFileInterface.GetVarianceForMc(mode_combi, bar_prop, cg_points, cg_ml_variances);
         }

         // Now make the actual plots 
         In add = mode_combi.Address() - I_1;
         In prop_no = bar_prop + I_1;
         aDumpPotential.PlotNonFittedPointsForMc(mode_combi, aOneModeGridBounds[add], prop_no, cg_bar_coords, cg_bar_property);
      }
   }
}

/**
 *
 **/
void DoPotentialFit::OutputXiGCoord
   (  const ModeCombi& arMt
   ,  const std::vector<MidasVector>& aXig
   )  const
{
   Mout << " Set of XiG  coordinates: " << std::endl;
   for (In i_c = I_0; i_c < arMt.Size(); i_c++)
   {
      Mout << "Mode " << arMt.Mode(i_c) << std::endl;
      for (In i_r = I_0; i_r < aXig[i_c].Size(); i_r++)
      {
         Mout << aXig[i_c][i_r] << std::endl;
      }
   }
   Mout << std::endl;
}

/**
 *
 **/
bool DoPotentialFit::OnlyFit
   (  const ModeCombi& arMt
   ,  bool aDerivatives
   )  const
{
   bool only_fit = !mPesCalcDef.GetmPesDoInt();
   if (mPesCalcDef.GetmPesDoExtrap() && arMt.Size() > mPesInfo.GetExtrapFrom())
   {
      only_fit = !mPesCalcDef.GetmPesDoIntForExtrap();
   }
   
   if (mPesCalcDef.GetmPesAdga())
   {
      if (!mPesInfo.GetmUseDerivatives())
      {
         only_fit = true;
      }
      else
      {
         only_fit = false;
      }
      if (!aDerivatives)
      {
         only_fit = true;
      }
   }
   
   if (mPesCalcDef.GetmPesAdga() && arMt.IsToBeExtrap() )
   {
      only_fit = true;
   }
   return only_fit;
}
