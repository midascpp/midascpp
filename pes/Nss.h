/**
************************************************************************
*
* @file                Nss.h
*
* Created:             25-02-2008
*
* Author:              Eduard Matito
*
* Short Description:   Declares class Nss (Nested Summation Symbol)
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

/*------------------------- Nested Summation Symbol -----------------------
// Program based on Besalu & Carbo one in [J. Math. Chem. 18, 37-72 (1995)]
// using the algorithm by Carbo & Besalu
// Carbo R, Besalu E, J. Math. Chem. 13, 331-342 (1993)
// Carbo R, Besalu E, Computers & Chemistry 18 (2) 117-126 (1994)
---------------------------------------------------------------------------*/

#include "inc_gen/TypeDefs.h"

/**
* Declarations:
**/
class Nss    // Defines the NSS object
{
    In mDim,mN,mPos;                        // mDim: Nss dimension; mPos: Current Position in Nss
    In *mDummy,*mIni,*mEnd,*mStep,*mLog;    // Pointers to the Parameters of the NSS
                                            // mIni:   vector of initial values for each sum
                                            // mEnd:   vector of final values for each sum
                                            // mStep:  vector of steps for each sum
                                            // mDummy: vector which contains dummy indexes
                                            //         for each step
                                            // mLog:   vector which updates the highest value
                                            //         in each position, for each sum. Only 
                                            //         used for getting sorted indexes
    bool mFirst;                            // Checks whether it's first time RUNNING
                                            
   public:
    Nss(const In aDim, ...);                             // Constructor
    Nss(const In aDim, In* aIni, In* aEnd, In* aStep);   // Constructor from arrays
    ~Nss(void);                                          // Destructor
    Uin RunOrder();                                      // Generates next leader vector
    Uin RunAll();                                        // Generates next leader vector
    void Initialize(void);                               // Initializes Nss
    In Dim(void);                                        // Returns Nss dimension
    In VectorIndex(In);                                  // Returns Nss Vector of Dummy Indexes
};

