/**
************************************************************************
* 
* @file                PesInfo.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PesInfo datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <set>

// midas headers
#include "pes/PesInfo.h"
#include "pes/molecule/SymInfo.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "pes/PesFuncs.h"
#include "mpi/Impi.h"

// using declarations
using std::string;
using std::vector;
using std::map;
using std::fstream;
using std::set;
using std::make_pair;
using Sst = string::size_type;

/**
 * Constructor. Will initialize all "sub-infos".
 *
 * @param aPesCalcDef    CalcDef for current pes calculation.
 * @param aMolecule      The molecule we are constructing a pes for.
 * @param aSubSystemDir  The directory in which calculation of this subsystem takes place. 
 * @param aSubSystemName For example subsystem_0
 **/
PesInfo::PesInfo
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const std::string aSubSystemDir
   ,  const std::string aSubSystemName
   )
   :  mMolecule(aMolecule)
   ,  mMultiLevelInfo()
   ,  mScalingInfo(aPesCalcDef, aMolecule)
   ,  mSymInfo()
   ,  mSubSystemDir(aSubSystemDir)
   ,  mSubSystemName(aSubSystemName)
   ,  mNoSurfaceProps(I_0)
   // THE BELOW MIGHT DIE
   ,  mMCLevel(I_0)
   ,  mIterNo(I_0)
   // REST WILL CERTAINLY DIE
   ,  mUseDerivatives(false)
   ,  mExtrapCalcList(aMolecule, *this)
{

}

/**
 * Update pes info for next pes multi level.
 *
 * @param aLevel
 * @param aPesCalcDef
 * @param aSubDir
 **/
void PesInfo::Update
   (  const PesCalcDef& aPesCalcDef
   ,  In aLevel
   ,  const std::string& aSubDir
   )
{
   auto multilevel_dir = mSubSystemDir + aSubDir;

   if (gDebug || gPesIoLevel > I_9)
   {
      Mout << " Updating PesInfo " << std::endl;
      Mout << " LEVEL  : " << aLevel << std::endl;
      
      if (aLevel != -1)
      {
         Mout << " SP NAME: " << aPesCalcDef.GetSinglePointName(aLevel) << std::endl;
      }
   }
   
   if (aLevel != -1)
   {
      mSinglePointName = aPesCalcDef.GetSinglePointName(aLevel);
   }
  
   // Send Level information to other MPI nodes.
   midas::mpi::SendSignal(mpi::CommunicatorWorld(), midas::pes::mpi_impl::signal::UPDATE_LEVEL_INFO);
   
   auto mpi_region_guard = midas::mpi::StartMpiRegion();

#ifdef VAR_MPI   
   midas::mpi::Blob blob(1024);
   blob.Load(reinterpret_cast<char*>(&aLevel), sizeof(In));
   blob.Load(multilevel_dir);
   blob.Bcast(mpi::CommunicatorWorld());
#endif /* VAR_MPI */

   mMultiLevelInfo.Update(aPesCalcDef, aLevel, multilevel_dir, mMolecule.IsLinear());
}

/**
 * Operator output overload.
 *
 * @param aOs        Output stream.
 * @param aPesInfo   The pes info to output.
 *
 * @return           Return output stream for chaining of operator<<.
 **/
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const PesInfo& aPesInfo
   )
{
   aOs << aPesInfo.mSymInfo;
   return aOs;
}
