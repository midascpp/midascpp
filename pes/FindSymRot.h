#ifndef FINDSYMROT_H_INCLUDED
#define FINDSYMROT_H_INCLUDED

#include <vector>
#include <map>
#include <utility> // for std::pair

#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "mmv/MidasMatrix.h"

/**
 * Find rotation matrix between 2 geometries
 * 
 **/
bool FindRotationMatrix
   (  std::vector<Nuclei>& orig
   ,  std::vector<Nuclei>& rot_str
   ,  std::map<In,In>& molmol
   ,  MidasMatrix& rot_mat // output, rotation matrix
   ,  std::vector<std::pair<std::string,std::string> >& label_rot_orig
   ,  bool mayIinvert
   ,  const Nb aSymThr
   ,  const In aIoLevel = 0
   );

#endif /* FINDSYMROT_H_INCLUDED */
