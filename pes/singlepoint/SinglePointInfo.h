#ifndef SINGLEPOINTINFO_H_INCLUDED
#define SINGLEPOINTINFO_H_INCLUDED

#include <map>
#include <string>
#include <iostream>

using SinglePointInfo = std::map<std::string, std::string>;

inline std::ostream& operator<<(std::ostream& os, const SinglePointInfo& sp_info)
{
   for(auto elem: sp_info)
   {
      os << elem.first << ":\n"
         << elem.second << "\n";
   }
   return os;
}

#endif /* SINGLEPOINTINFO_H_INCLUDED */
