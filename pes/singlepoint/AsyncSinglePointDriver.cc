/**
************************************************************************
*
* @file                AsyncSinglePointDriver.cc
*
* Created:             01-05-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk) refactored CalculationList
*
* Short Description:   Handles parallel calculation of singlepoints
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#include "AsyncSinglePointDriver.h"

#include <utility>

#include "mpi/Info.h"
#include "concurrency/Info.h"

//!
using prop_pair_t = std::pair<std::string, pes::GeneralProp>;

//!
using futures_list_t = std::list<std::future<prop_pair_t> >;

/**
 * Dump a list of calculated points to file.
 * On exit will have cleared the list of points.
 *
 * @param aPointsToBeDumped    The list of points.
 * @param aRestartFile         An open ofstream, where the points should be dumped.
 **/
void DumpPointsToFile
   (  std::list<prop_pair_t>& aPointsToBeDumped
   ,  std::ofstream& aRestartFile
   )
{
   // Dump points
   for(const auto& point : aPointsToBeDumped)
   {
      aRestartFile << point.first << " ";
      point.second.DumpToStream(aRestartFile);
   }
   
   // Clear list
   aPointsToBeDumped.clear();
}


/**
 * Wait for futures with properties to become available,
 * and afterwards dump them to disc.
 *
 * @param aFutures        The futures to wait for.
 * @param aBackupDir      The directory in which the 'sp_restart_N.mrestart' file will be created.
 * @param aDumpInterval   The interval in which singlepoints are dumped to disk (i.e. if aDumpInterval = 3, then after 3 SPs has finished they will get dumped).
 **/
void WaitForFutures
   (  futures_list_t& aFutures
   ,  const std::string& aBackupDir
   ,  In aDumpInterval
   ) 
{
   // Open restart file
   std::ofstream restart_file(aBackupDir + "/sp_restart_" + std::to_string(midas::mpi::GlobalRank()) + ".mrestart", std::ofstream::out | std::ofstream::app);
   restart_file << std::setprecision(20);
   
   // List for holding points before they are dumped.
   std::list<prop_pair_t> points_to_be_dumped;

   // Wait for all tasks to complete.
   while (aFutures.size())
   {
      auto iter = aFutures.begin();
      while (iter != aFutures.end())
      {
         if (iter->wait_for(std::chrono::seconds(0)) == std::future_status::ready)
         {
            iter->wait();
            points_to_be_dumped.emplace_back(iter->get());
            iter = aFutures.erase(iter);
         }
         else
         {
            ++iter;
         }
      }

      // Dump points
      if (points_to_be_dumped.size() >= aDumpInterval)
      {
         Mout << "." << std::flush; // write a '.' to stream, so user can se that the program is not stalling
         DumpPointsToFile(points_to_be_dumped, restart_file);
      }
      
      // Yield the cpu for other threads
      std::this_thread::yield();
   }
   
   // Dump all points that have not been dumped yet.
   DumpPointsToFile(points_to_be_dumped, restart_file);
   
   // Printout indicating we are "Done!" :)
   Mout << " Done!" << std::endl; // end line of 'dots'
}


/**
 * Constructor
 *
 * @param aNumThreads     The number of threads to start for the pool.
 * @param aBackupDir      The directory where the backup files should be placed.
 * @param aDumpInterval   Backup to file every N singlepoints calculated. If set to 0 no backup is done.
 **/
AsyncSinglePointDriver::AsyncSinglePointDriver
   (  In aNumThreads
   ,  const std::string& aBackupDir
   ,  In aDumpInterval
   )
   //:  mPool(static_cast<unsigned>(aNumThreads))
   :  mBackupDir(aBackupDir)
   ,  mDumpInterval(aDumpInterval)
   ,  mNumThreads(aNumThreads)
{
}


/**
 * Evaluate list of SinglePointRunners. 
 * Returns when all SinglePointRunners are completed.
 *
 * @param aSpList           List of SinglePointRunners to evaluate.
 **/
void AsyncSinglePointDriver::EvaluateList
   (  std::list<SinglePointRunner>& aSpList
   )
{
   Mout << " Evaluating singlepoints (one . per 'dump-interval'): " << std::flush;
   futures_list_t futures;
   
   // Set number of active threads. If mNumThreads is not a positive number, it
   // signifies that all available threads should be used. This is signaled to
   // midas::concurrency::SetActive() by passing a -1.
   concurrency::SetActive(mNumThreads > I_0 ? mNumThreads : -I_1);
   
   // loop over list of singlepoints and submit to queue
   for (auto& sp_runner : aSpList)
   {
      futures.emplace_back 
         (  concurrency::Post
               (  [&sp_runner]() -> prop_pair_t
                  { 
                     SinglePoint sp = sp_runner.Setup();
                     sp.RunSinglePoint(); 
                     sp_runner.Teardown(sp.Properties());
                     return {sp_runner.GetUid(), sp.Properties()};
                  }
               ) 
         );
   }
    
   // Wait for all points to finish
   WaitForFutures(futures, mBackupDir, mDumpInterval);
   
   // Restore number of active threads
   concurrency::SetActive();
}

/**
 * Evaluate list of singlepoints. 
 * Returns when all singlepoint calculations are completed.
 *
 * @param aSpList           List of singlepoints to evaluate.
 **/
void AsyncSinglePointDriver::EvaluateList
   (  std::list<SinglePoint>& aSpList
   )
{
   Mout << " Evaluating singlepoints (one . per 'dump-interval'): " << std::flush;
   std::vector<std::future<void> > futures;
   
   concurrency::SetActive(mNumThreads > I_0 ? mNumThreads : -I_1);

   // loop over list of singlepoints and submit to queue
   for(auto& sp : aSpList)
   {
      futures.emplace_back
         (  concurrency::Post
               (  [&sp]()
                  { 
                     sp.RunSinglePoint(); 
                  }
               )
         );
   }
   
   // wait for all tasks
   for(auto& f : futures)
   {
      f.wait();
      Mout << "." << std::flush; // write a '.' to stream, so user can se that the program is not stalling
   }
   
   Mout << " Done!" << std::endl; // end line of 'dots'
   
   concurrency::SetActive();
}
