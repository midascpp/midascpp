/**
************************************************************************
* 
* @file                DaltonSinglePoint.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   DaltonSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DALTONSINGLEPOINT_H_INCLUDED
#define DALTONSINGLEPOINT_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "pes/singlepoint/SinglePoint.h"

class DaltonSinglePoint 
   :  public SinglePointImpl
{
   private:
      bool mEsSym = false;
      std::string mInput = "";
      std::string mBasis = "";

      // helper functions
      void UpdateElectronicBasis();
      void MakeDaltonInput() const;
      void MakeMoleculeInput(bool) const;
      
      // overloads
      void CreateInputImpl() const; 
      void SubmitImpl(bool) const;
      void ValidateCalculationImpl(bool&,bool&) const;
      void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const;
      //void CleanUpImpl() const;
   
   public:
      DaltonSinglePoint() = delete;
      DaltonSinglePoint(const SinglePointInfo&, const pes::PropertyInfo&);
      virtual ~DaltonSinglePoint() = default;
};

#endif /* DALTONSINGLEPOINT_H_INCLUDED */
