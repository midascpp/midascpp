/**
************************************************************************
* 
* @file                GenericSinglePoint.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   GenericSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/singlepoint/GenericSinglePoint.h"

// std headers
#include <string>
#include <vector>
#include <unistd.h>
#include <errno.h>
#include <chrono>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/paral/run_async.h"
#include "util/paral/run_process.h"
#include "util/FileSystem.h"
#include "mpi/Interface.h"

// using declarations
using std::vector;

/////
// Register for factory
/////
SinglePointRegistration<GenericSinglePoint> registerGenericSinglePoint("SP_GENERIC");

/**
 * Constructor
 **/
GenericSinglePoint::GenericSinglePoint
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
   :  SinglePointImpl(aSpInfo, aPropertyInfo)
   ,  mSetupDir(aSpInfo.at("SETUPDIR"))
{
   // init input and runscript variables
   SinglePointInfo::const_iterator iter;
   if((iter = aSpInfo.find("INPUTCREATORSCRIPT")) == aSpInfo.end())
   {
      MIDASERROR("No INPUTCREATORSCRIPT in SinglePointInfo map");
   }
   mInputCreatorScript = iter->second;
   if((iter = aSpInfo.find("RUNSCRIPT")) == aSpInfo.end())
   {
      MIDASERROR("No RUNSCRIPT in SinglePointInfo map");
   }
   mRunScript = iter->second;

   
   // check for existence of input creator script and run script
   std::string file_name = mSetupDir + "/" + mInputCreatorScript;
   if(!midas::filesystem::Exists(file_name))
   {
      MIDASERROR
         (  "Could not find InputCreatorScript: " + mInputCreatorScript + " remember to copy it to setup sub-directory."
         ,  "Searched path: " + file_name + "\n" + strerror(errno)
         );
   }
   file_name = mSetupDir + "/" + mRunScript;
   if(!midas::filesystem::Exists(file_name))
   {
      MIDASERROR
         (  "Could not find RunScript: " + mRunScript + " remember to copy it to setup sub-directory."
         ,  "Searched path: " + file_name + "\n" + strerror(errno)
         );
   }
   
   // check for existence of validation script input
   mValidationScript = "NoSuchFileOrDirectory";
   if((iter = aSpInfo.find("VALIDATIONSCRIPT")) != aSpInfo.end() && iter->second != "")
   {
      mValidationScript = iter->second;
      file_name = mSetupDir + "/" + mValidationScript;
      if(!midas::filesystem::Exists(file_name))
      {
         MIDASERROR
            (  "Could not find ValidationScript: " + mValidationScript + " remember to copy it to setup sub-directory."
            ,  "Searched path: " + file_name + "\n" + strerror(errno)
            );
      }
   }
}

/**
 *
 **/
void GenericSinglePoint::CreateInputImpl
   (
   ) const
{
   // Create xyz file midasifc.xyz_input
   CreateGenericXyzFile();
   
   // Now run the script
   system_command_t sub = { mSetupDir + "/" + mInputCreatorScript };
   int status = MIDASSYSTEM_DIR_STATUS(sub, mSpScratchDir);
   if (status)
   {
      MIDASERROR("COULD NOT CREATE INPUT FOR SINGLEPOINT IN " + mSpScratchDir);
   }
}

/**
 *
 **/
void GenericSinglePoint::CreateGenericXyzFile
   (  
   )  const
{
   // write xyz coordinates file
   std::string file_name = mSpScratchDir + "/midasifc.xyz_input";
   std::ofstream ofs(file_name);
   ofs.setf(ios::scientific);
   ofs.precision(SinglePointImpl::mXyzPrecision);
   ofs << mStructure.size() << std::endl << mCalcCodeString << std::endl;
   ofs << std::showpos;
   for (In i = I_0; i < mStructure.size(); ++i) 
   {
      Nb x, y, z;
      std::string name = mStructure[i].AtomLabel();
      if (  mBohr)  //If using the keyword BohrUnits is set, there should NOT be a conversion from Bohr to Angstrom (*C_TANG)
      {
         x = mStructure[i].X();
         y = mStructure[i].Y();
         z = mStructure[i].Z();
      }
      else
      {
         x = mStructure[i].X() * C_TANG;
         y = mStructure[i].Y() * C_TANG;
         z = mStructure[i].Z() * C_TANG;
      }
      ofs << std::setw(4) << name << "   " << x << "   " << y << "   " << z << endl;
   }
   ofs.close();
}

/**
 * Take care of running the calculation.
 **/
void GenericSinglePoint::SubmitImpl
   ( bool aSingle
   ) const // dummy bool :S (the need for this should be removed sometime!)
{
   // runscript is in mSetupDir, and called mRunScript
   system_command_t cmd = { mSetupDir + "/" + mRunScript, mSpScratchDir };
   auto status = run_process(cmd, Mlog);
   if(status.status != 0) 
   {
      MIDASERROR("RUN FAILED FOR CALCULATION : ", std::to_string(this->CalcNumber()));
   }
}

/**
 * Check that there are zero errors/suspicios returned
 * @param aSstr                    Stringstream to check.
 * @param aStr                     String/type to check for (ERROR/SUSPICIOUS)
 * @return                         Return true if aStr was found, false otherwise.
 **/
bool GenericSinglePoint::CheckValidateString
   (  std::stringstream& aSstr
   ,  const std::string aStr
   )  const
{
   In n_err = -1;
   aSstr >> n_err;
   if(n_err)
   {
      if(n_err == -1) // if n_err is still -1, then we haven't read anything in
      {
         MIDASERROR("VALIDATE SCRIPT DID NOT RETURN NUMBER OF "+aStr+"!"); // aStr is just there for pretty error message
      }
      return true;
   }
   
   return false;
}

/**
 * Check that the calculation finished correctly
 **/
void GenericSinglePoint::ValidateCalculationImpl
   ( bool& aHalt
   , bool& aSuspicious
   ) const
{
   // run supplied validation script
   if(mValidationScript=="NoSuchFileOrDirectory")
   { // if there is no validation script we just return
      return;
   }
   
   // now call the script
   system_command_t cmd = { mSetupDir + "/" + mValidationScript };
   std::stringstream validate_sstr;
   int validation_status = MIDASSYSTEM_DIR_STREAM_STATUS(cmd, mSpScratchDir, validate_sstr);
   
   if(validation_status != 0) // check if validation script terminated correctly
   {
      Mout << " Validation Script did not return correctly..." << endl;
      aHalt = true; // if not we halt the calculation
   }
   else
   {
      // if the validations returned without errors, we check the validation
      aHalt       = CheckValidateString(validate_sstr, "ERRORS");
      aSuspicious = CheckValidateString(validate_sstr, "SUSPICIOUS");
      
      //// copy back some files
      //std::string cp_file;
      //while(validate_sstr >> cp_file)
      //{
      //   //midas::filesystem::Copy(mSpScratchDir + "/" +cp_file, mSaveDir + "/point" + std::to_string(mCalcNumber) + "/" + cp_file);
      //}
   }
}

/**
 *
 **/
void GenericSinglePoint::ReadRotatedStructureImpl
   (  std::map<In,In>& aNucMap
   ,  std::vector<Nuclei>& aRotated
   ,  std::vector< pair<string,string> >& aLabelVec
   )  const
{
   // copy file from node to savedir
   std::string file_name = mSpScratchDir + "/midasifc.cartrot_xyz";
   if(!midas::filesystem::Exists(file_name)) 
   {
      MIDASERROR("For single point: " + std::to_string(mCalcNumber) + "\nDid not find XYZ coordinates for aRotated structure at location: \n" + file_name);
   }
   
   // Read the file
   std::ifstream ifs;
   ifs.open(file_name);

   // in xyz format the first two lines are uninteresting (number of atoms and comment line)
   std::string line;
   std::getline(ifs,line);
   std::getline(ifs,line);

   // map relating nuclei in structures
   In count = 0;
   while(std::getline(ifs,line)) 
   {
      string label;
      Nb x,y,z;
      istringstream iss(line);
      iss >> label >> x >> y >> z;
      
      // convert to AU
      if (mBohr == false)   //This conversion should not be made, only if the UsingBohrs keyword is NOT set.
      {
         x/=C_TANG;
         y/=C_TANG;
         z/=C_TANG;
      }
      
      Nuclei nuc_inp(x,y,z,C_0,label);
      nuc_inp.SetQfromGeneralLabel(label);
      // Q: Isotope number does not matter here, right?
      //    We are just making translation...
      // A: No, if we are dealing with isotopes Midas might not be able to relate the structures 
      //    if not doesn't take care of the right masses. Since the center of mass changes
      In iso = mStructure[count].Anuc();
      nuc_inp.SetAnuc(iso);
      aRotated.push_back(nuc_inp);
      aNucMap.insert(std::make_pair(count,count));
      aLabelVec.push_back(std::make_pair(label,label));
      count++;
   }
}
