/**
************************************************************************
* 
* @file                ModelSinglePoint.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   ModelSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MODELSINGLEPOINT_H_INCLUDED
#define MODELSINGLEPOINT_H_INCLUDED

#include <memory>

#include "inc_gen/TypeDefs.h"
#include "pes/singlepoint/SinglePoint.h"
#include "pes/singlepoint/SinglePointInfo.h"

class ModelPot;

class ModelSinglePoint 
   :  public SinglePointImpl
{
   private:

      const std::unique_ptr<ModelPot>& mPotential;

      const std::vector<std::string> mSpecialProperties = 
         {  "MU_LINEAR_INERTIA"
         ,  "TRACE_EFFINERTIAINV"
         ,  "XX-1_EFFINERTIAINV"
         ,  "XY-1_EFFINERTIAINV"
         ,  "XZ-1_EFFINERTIAINV"
         ,  "YY-1_EFFINERTIAINV"
         ,  "YZ-1_EFFINERTIAINV"
         ,  "ZZ-1_EFFINERTIAINV"
         };

      bool IsSpecialProperty(const std::string& aName) const
      {
         for(int i = 0; i < mSpecialProperties.size(); ++i)
         {
            if(aName == mSpecialProperties[i])
            {
               return true;
            }  
         }

         return false;
      }
      
      void CreateInputImpl() const;
      void SubmitImpl(bool) const;
      void ValidateCalculationImpl(bool&,bool&) const;
      void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const {};

   public:
      ModelSinglePoint(const SinglePointInfo&, const pes::PropertyInfo&);
      ~ModelSinglePoint();
};

#endif /* MODELSINGLEPOINT_H_INCLUDED */
