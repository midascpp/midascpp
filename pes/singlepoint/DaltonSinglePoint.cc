/**
************************************************************************
* 
* @file                DaltonSinglePoint.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   DaltonSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
#include <vector>
#include <future>
#include <cerrno>

#include "pes/singlepoint/SinglePointInfo.h"
#include "pes/singlepoint/SinglePoint.h"
#include "pes/singlepoint/DaltonSinglePoint.h"
#include "pes/singlepoint/Converter.h"

#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/paral/run_async.h"
#include "util/paral/run_process.h"


/////
// Register for factory
/////
SinglePointRegistration<DaltonSinglePoint> registerDaltonSinglePoint("SP_DALTON");

/**
 * Constructor
 **/
DaltonSinglePoint::DaltonSinglePoint
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
   :  SinglePointImpl(aSpInfo, aPropertyInfo)
   ,  mEsSym(false) // we assume no es symmetry
   ,  mInput("")
   ,  mBasis("")
{
   // check for es sym
   auto iter = aSpInfo.find("ESSYM");
   if(iter != aSpInfo.end())
   {
      if(iter->second == "TRUE")
      {
         mEsSym = true;
         //Mout << " using ESSYM " << std::endl;
      }
   }
   
   // check for dalton input
   iter = aSpInfo.find("DALTONINPUT");
   if(iter != aSpInfo.end())
   {
      mInput = iter->second;
   }
   else
   {
      MIDASERROR("NO DALTON INPUT GIVEN.");
   }
   
   // check for dalton input
   iter = aSpInfo.find("BASIS");
   if(iter != aSpInfo.end())
   {
      mBasis = iter->second;
   }
   else
   {
      MIDASERROR("NO DALTON BASIS GIVEN.");
   }
}

/**
 *
 **/
void DaltonSinglePoint::CreateInputImpl
   (
   ) const
{
   const_cast<DaltonSinglePoint&>(*this).UpdateElectronicBasis();
   // Make dalton input
   MakeDaltonInput();
   // Make molecule input
   MakeMoleculeInput(mEsSym);
}

/**
 * Make input file for DALTON.
 **/
void DaltonSinglePoint::MakeDaltonInput
   (
   ) const
{
   // make input file on root node
   std::string filename(mSpScratchDir + "/DALTON.INP");
   std::ofstream dalton_inp(filename);
   dalton_inp << mInput;
   dalton_inp.close();
}

/**
 * UpdateElectronicBasis
 **/
void DaltonSinglePoint::UpdateElectronicBasis
   (
   )
{
   // parse basis set input
   map<In,string> basis;
   
   string s;
   istringstream basis_input(mBasis);
   In i_atom_b;
   std::string basis_in_file;
   while(midas::input::GetLine(basis_input,s))
   {
      istringstream basis_line(s);
      basis_line >> i_atom_b;
      basis_line >> basis_in_file;
      basis[i_atom_b] = basis_in_file;
   }

   //Update the info in the vector of nuclei stored in the pes info class
   for(auto iter = mStructure.begin(); iter!=mStructure.end(); iter++)
   {
      Nb q=(*iter).Q();
      //the charge is stored as Nb, conversion integer
      In i_q = In(q+C_I_10_4);
      string basis_q = basis[i_q];
      
      (*iter).AssignBasisLabel(basis_q);
   }
}
      

/**
 * Create molecular input file for Dalton singlepoint.
 *
 * @param aEsSym   Use symmetry in the dalton calculation.
 **/
void DaltonSinglePoint::MakeMoleculeInput
   (  bool aEsSym
   )  const
{
   In n_nuclei = mStructure.size();
   std::string file_name = mSpScratchDir + "/MOLECULE.INP";
   //std::ofstream mol_inp(file_name, ios_base::out);
   std::ofstream mol_inp(file_name);
   if(!mol_inp.good())
   {
      std::cout << " FILE NOT GOOD " << std::endl;
      std::cout << std::strerror(errno) << std::endl;
   }
   if(mol_inp.fail())
   {
      std::cout << " FAIL " << std::endl;
   }
   if(mol_inp.bad())
   {
      std::cout << " BAD " << std::endl;
   }
   mol_inp << "ATOMBASIS \n";
   mol_inp << "Automatic MidasCpp generated Molecule input for Dalton \n";
   mol_inp << "Number of nuclei " << n_nuclei << " no further comments \n";


   bool any_cl = false;
   string scl1 = "C_";
   string scl2 = "CE_";
   for (In i=I_0;i<n_nuclei;i++)
   {
      string stest = mStructure[i].BasisLabel();
      //Mout << "stest:"<<stest << ":"<<endl;
      if (stest.find(scl1)!=stest.npos) any_cl = true;
      if (stest.find(scl2)!=stest.npos) any_cl = true;
   }

   //for (In i=I_0;i<n_nuclei;i++)
   In     n_groups=I_0;
   string old_bas  = "";
   Nb     old_q    = C_0;
   In     i=I_0;
   vector<In> n_per_group;
   while (i<n_nuclei)
   {
      In n_similar = I_1;
      old_bas = mStructure[i].BasisLabel();
      old_q   = mStructure[i].Q();
      for (In j=i+I_1;j<n_nuclei;j++)
      {
         //if (fabs(mStructure[i+1].Q()-old_q)< C_NB_EPSILON && mStructure[i+1].BasisLabel() == old_bas) 
         if (mStructure[j].Q() == old_q && mStructure[i+1].BasisLabel() == old_bas) 
            n_similar++;
         else 
            break;
      }
      i+=n_similar;
      n_groups++;
      n_per_group.push_back(n_similar);
   }

   mol_inp << setw(1) << " " << setw(4) << n_groups; 
   // Set charge to zero. Can cause problems later! 
   mol_inp << setw(3) << "   "; // Replace by next line if necessary
   // mol_inp << setw(3) << n_charge; 
   // Nb this allows the molecule to be rotated be dalton. If this is not
   // desired insert the next line
   //if (!mDaltonSymmetryOn) mol_inp << setw(2) << " 0"; 
   if (!aEsSym) mol_inp << setw(2) << " 0"; 
   else mol_inp << "  ";
   mol_inp << endl;  
   mol_inp.setf(ios::showpoint);
   mol_inp.setf(ios::uppercase);
   //mol_inp.fill('#');

   i=I_0;
   for (In i_gp = I_0;i_gp<n_groups;i_gp++)
   {
      In n_similar = n_per_group[i_gp];
      mol_inp.precision(6);
      mol_inp << setw(1) << " " << setw(9) << showpoint << left << mStructure[i].Q();
      mol_inp << right << setw(4) << n_similar << " ";
      string label = mStructure[i].BasisLabel();
      if (any_cl)
      {
         //Mout << " bef " << label << endl;
         if (label.find(scl1)!=label.npos) label.erase(I_0,I_2);
         if (label.find(scl2)!=label.npos) label.erase(I_0,I_3);
         if (label.substr(I_0,I_2) == "MM") label = "MM";
         //Mout << " aft " << label << endl;
      }
      mol_inp << "Bas=" << label;
      mol_inp << endl;
      for (In j=i;j<i+n_similar;j++)
      {
         mol_inp << left << setw(5) << mStructure[j].AtomLabel()+std::to_string(j) <<" ";
         mol_inp.precision(14);
         mol_inp.setf(ios_base::right);
         mol_inp.setf(ios::scientific);
         mol_inp.width(21);
         mol_inp << mStructure[j].X()<<" ";
         mol_inp.width(21);
         mol_inp << mStructure[j].Y()<<" ";
         mol_inp.width(21);
         mol_inp << mStructure[j].Z();
         mol_inp.unsetf(ios::scientific);
         if (any_cl) 
         {
            mol_inp << " ";
            mol_inp.width(2);
            mol_inp << mStructure[j].SubSys();
            mol_inp << " ";
            mol_inp.width(2);
            mol_inp << mStructure[j].SubSysMem();
         }
         mol_inp << "\n";
      }
      i+=n_similar;
   }
   mol_inp.close();
}

/**
 *  Take care of running the calculation
 **/
void DaltonSinglePoint::SubmitImpl
   (  bool aB
   )  const
{
   std::string scriptdir = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_PREFIX");
   std::string script = scriptdir + "/libexec/dalton_for_midas.sh";
   system_command_t cmd = {script, mSpScratchDir};
   
   run_process(cmd, Mlog);
}

/**
 *  Check that the calculation finished correctly
 **/
void DaltonSinglePoint::ValidateCalculationImpl
   (  bool& aHalt
   ,  bool& aSuspicious
   )  const
{
   /*
      For dalton we check for:
      1) ERROR in output => HALT if found
      2) Problem in output => suspicious if found
      3) THE_END in prop file => HALT if not found
      4) HOMO-LUMO gap, suspicous if < 0.02 au
   */
   
   //// 1) 
   //{
   //   std::stringstream error_sstr;
   //   std::vector<std::string> error_cmd = {"grep", "-c", "ERROR", mSpScratchDir + "/DALTON.OUT"};
   //   MIDASSYSTEMTOSTREAM(error_cmd, error_sstr);
   //   
   //   In count = I_0;
   //   if(error_sstr >> count) 
   //   {
   //      if(count > I_0) 
   //      {
   //         // we need to halt the calculation
   //         aHalt=true;
   //      }
   //   }
   //}

   //// 2)
   //{
   //   std::stringstream error_sstr;
   //   std::vector<std::string> error_cmd = {"grep", "-ci", "problem", mSpScratchDir + "/DALTON.OUT"};
   //   MIDASSYSTEMTOSTREAM(error_cmd,error_sstr);
   //   
   //   In count=I_0;
   //   if(error_sstr >> count) 
   //   {
   //      if(count > I_0) 
   //      {
   //         // we need to 'suspect' the calculation
   //         aSuspicious=true;
   //      }
   //   }
   //}

   // 3) only check if not suspicious already
   if(!aSuspicious) 
   {
      string file_name = mSpScratchDir+"/midasifc.prop";
      std::ifstream ifs(file_name);
      std::string line;
      aSuspicious=true;
      while(std::getline(ifs,line)) 
      {
         if(line.find("THE_END")!=line.npos)
         {
            aSuspicious=false;
         }
      }
      ifs.close();
   }
}

/**
 *
 **/
void DaltonSinglePoint::ReadRotatedStructureImpl
   (  std::map<In,In>& aNucMap
   ,  std::vector<Nuclei>& aRotated
   ,  std::vector< pair<string,string> >& aLabelVec
   ) const
{
   // copy file from node to savedir
   std::string file_name  = mSpScratchDir + "/midasifc.cartrot";
   if(!InquireFile(file_name)) 
   {
      MIDASERROR("For single point: " + std::to_string(mCalcNumber) + "\nDid not find coordinates for aRotated structure.");
   }
   
   // Read the file
   std::ifstream ifs;
   ifs.open(file_name);

   // map relating nuclei in structures
   string line;
   In count=0;
   while(getline(ifs,line)) 
   {
      string label;
      Nb x,y,z;
      istringstream iss(line);
      iss >> label >> x >> y >> z;
      if (label=="Atomic")
      {
         In relabled=I_0;
         while(getline(ifs,line)) 
         {
            aRotated[relabled].SetAtomLabel(line);
            relabled++;
         }
         break;
      }
      Nuclei nuc_inp(x,y,z,C_0,label);
      nuc_inp.SetQfromGeneralLabel(label);
      // Q: Isotope number does not matter here, right?
      // We are just making translation...
      nuc_inp.SetAnuc(I_0);
      aRotated.push_back(nuc_inp);
      aNucMap.insert(std::make_pair(count,count));
      aLabelVec.push_back(std::make_pair(label,label));
      count++;
   }

   // convert properties to new generic format
   Converter converter(mSpScratchDir, aLabelVec);
   converter.StandardWriproReader();
}
