#ifndef CONVERTER_H_INCLUDED
#define CONVERTER_H_INCLUDED

#include <string>
#include <vector>
#include <utility>

#include "inc_gen/TypeDefs.h"

/**
 * Class for converting old property and coordinate format into the new generic one.
 * This is used for the old hardcoded DALTON interface.
 **/
class Converter
{
   private:
      //! Directory with property files
      std::string mInputPropDir;
      //! Nuclei labels
      std::vector< std::pair<std::string, std::string> > mLabels;

      //! Add a property to file
      void AddToPropFile(In aOrder, const std::string& aS, Nb aProp, In aI = -1, const std::vector<In> *aElem = nullptr);

      //! Add a property to file
      void AddToPropFile(In aOrder, const std::string& aS, std::vector<Nb> aV, Nb aProp, In aI = -1, const std::vector<In> *aElem = nullptr);

   public:
      //! Constructor
      Converter
         (  const std::string& aInputPropDir
         ,  const std::vector<std::pair<std::string, std::string> >& aLabels
         );
      
      //! Read old format and create files for new format
      void StandardWriproReader();
};

#endif /* CONVERTER_H_INCLUDED */
