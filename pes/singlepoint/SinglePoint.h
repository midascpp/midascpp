/**
************************************************************************
* 
* @file                SinglePoint.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) original author
*                      Ian H. Godtlieben (ian@chem.au.dk) division of singlepoint and pessinglepoint
*
* Short Description:   SinglePoint interface.
*                      Defines what it means to be a singlepoint.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


#ifndef SINGLEPOINT_H_INCLUDED
#define SINGLEPOINT_H_INCLUDED

#include <string>
#include <map>
#include <memory> // for std::unique_ptr

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/SinglePointCalcDef.h"
#include "util/AbstractFactory.h"
#include "util/MidasStream.h"
#include "pes/GeneralProp.h"
#include "pes/CalcCode.h"
#include "pes/singlepoint/SinglePointInfo.h"

using namespace midas;

class SinglePointImpl;

// Factory settings
using SinglePointFactory = AbstractFactory<SinglePointImpl*(const SinglePointInfo&, const pes::PropertyInfo), std::string>;

template<class A>
using SinglePointRegistration = AbstractFactoryRegistration<SinglePointImpl*(const SinglePointInfo&, const pes::PropertyInfo), A, std::string>;

// We can currently do DALTON, GENERIC, MODEL and TINKER

/**
 * Single point implementation class. 
 * Use as base class for specific singlepoint types.
 **/
class SinglePointImpl
{
   private:
      //! Type of single point
      std::string mType = "SP_ERROR";

      //! Internal IO level in singlepoint
      In mIoLevel = 0;

      //! Rotation threshold for relating ES structure with MIDAS structure
      Nb mRotationThr = 1e-6;

      //! has singlepoint been initialized with calculation number, node and structure?
      bool mInitialized = false; 

   protected:   
      //! Calculation number (-1 is default), used e.g. for scratch dir naming.
      In mCalcNumber = -I_1; 

      //! calculation code (given as simple rationals providing displacements)
      std::string mCalcCodeString = "NA"; 

      //! Molecular structure (midas reference, used to create input files in derived classes + finding rotation matrices).
      std::vector<Nuclei> mStructure;

      //! Singlepoint scratch directory.
      std::string mSpScratchDir = "";

      //! Use Bohr units (default is false)
      bool mBohr = false;

      //! Save scratch directory.
      bool mSaveSpScratchDir = false;

      // Precision of XYZ files.
      In mXyzPrecision = std::numeric_limits<double>::max_digits10;
      
      //! Holds properties after singlepoint has been calculated
      //! moved here from private, because of TinkerSinglePoint (refactor!)
      pes::GeneralProp mProperties;

   private:
      //! is the singlepoint finished ( this will probably be removed. )
      bool mDone = false; 

      //! Write error message from singlepoint.
      static std::string ErrorMessage(); ///< return error message with available programs (update when new programs are added)

      //! find rotation of properties (in case the es program has rotated the structure)
      void SetupRotationMatrix
         (  const std::vector<Nuclei>& aRotated
         ,  std::map<In,In>& aNucMap
         ,  std::vector< pair<string,string> >& aLabelVec
         ,  MidasMatrix& aRotMatrix
         ); 
      
      //!@{
      //! Things to overload

      //! Overloadable create input function.
      virtual void CreateInputImpl() const = 0;

      //! Overloadable submit function.
      virtual void SubmitImpl(bool) const = 0;

      //! Overloadable validation function.
      virtual void ValidateCalculationImpl(bool&,bool&) const = 0;

      //! Overloadable read rotated structure function
      virtual void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const = 0;

      //! Overloadable clean-up function
      virtual void CleanUpImpl() const { };
      //!@}

   protected:
      //! protected ctor to use in derived classes
      SinglePointImpl(const SinglePointInfo&, const pes::PropertyInfo&);

   public:
      //! Pure virtual dtor to force SinglePoint to be abstract
      virtual ~SinglePointImpl() = 0;
      
      //!@{
      //! Stuff that should be put into contructor
      //! Initialize singlepoint with calcnumber (might be removed)
      void Initialize(In); 

      //! Copy structure into singlepoint
      void SetStructure(const std::vector<Nuclei>& aStructure) { mStructure = aStructure; } 

      //! Move structure into singlepoint
      void SetStructure(std::vector<Nuclei>&& aStructure) { mStructure = std::move(aStructure); } 

      //! Set CalcCodeString
      void SetCalcCodeString(std::string);
      //!@}

      //!@{
      //! Factory interface to create singlepoints
      static std::unique_ptr<SinglePointImpl> Factory(const SinglePointCalcDef&, const pes::PropertyInfo&);
      static std::unique_ptr<SinglePointImpl> Factory(const SinglePointInfo&   , const pes::PropertyInfo&);
      //!@}
      
      //!@{
      //! Interface to running the single point
      //! Setup scratch dir and set mSpScratchDir variable
      void SetupSpScratchDir(); 

      //! Input creation interface
      void CreateInput() const; 

      //! Submit calculation interface
      void Submit(bool) const; 

      //! Validate calculation interface
      void ValidateCalculation(bool&,bool&) const; 

      //! Gather and optionally rotate properties
      void GatherProperties();

      //! cleanup calculation interface
      void CleanUp() const; 
      //!@}
      
      //! Set done flag
      void SetDone() { mDone = true; }

      //! Check done flag
      bool IsDone() const { return mDone; }

      //! Get calculation number
      In CalcNumber() const { return mCalcNumber; }

      //! Get the CalcCode string
      std::string CalcCodeString() const { return mCalcCodeString; }

      //! Get properties
      const pes::GeneralProp& Properties() const { return mProperties; }
};

//! Definition of dtor
inline SinglePointImpl::~SinglePointImpl() { }

/**
 * Wrapper/Interface class for singlepoint stuff.
 **/
class SinglePoint
{
   private:
      //! Pointer to implementation
      std::unique_ptr<SinglePointImpl> mImpl;

   public:
      //! Construct from singlepoint info
      SinglePoint
         (  const SinglePointInfo&
         ,  const pes::PropertyInfo& aPropertyInfo
         ,  const std::vector<Nuclei>&
         ,  const In&
         ,  const std::string& 
         );
      
      //! Construct from singlepoint calcdef
      SinglePoint
         (  const SinglePointCalcDef&
         ,  const pes::PropertyInfo& aPropertyInfo
         ,  const std::vector<Nuclei>&
         ,  const In&
         );
   
      //! Delete default copy ctor
      SinglePoint(const SinglePoint&) = delete;

      //! Default move ctor
      SinglePoint(SinglePoint&&) = default;
      
      //! Default destructor
      ~SinglePoint() = default;
      
      //! Run the singlepoint
      void RunSinglePoint();
      
      //! Get the calculation number
      In CalcNumber() const { return mImpl->CalcNumber(); }

      //! Is Singlepoint done?
      bool IsDone() const { return mImpl->IsDone(); }

      //! Get properties
      const pes::GeneralProp& Properties() const { return mImpl->Properties(); }
};

#endif /* SINGLEPOINT_H_INCLUDED */
