/**
************************************************************************
* 
* @file                MLSinglePoint.h
*
* Created:             11-06-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   SinglePoint datatype for Machine Learning 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MLSINGLEPOINT_H_INCLUDED
#define MLSINGLEPOINT_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "pes/singlepoint/SinglePoint.h"

#include "mlearn/MLTask.h"
#include "mlearn/GPKernel.h"
#include "mlearn/GauPro.h"
#include "geoopt/GeoDatabase.h"


/**
 *
 **/
class MLSinglePoint 
   :  public SinglePointImpl
{
   private:

      // members
      std::string mSetupDir;
      std::string mSaveIoDir;
      std::string mInputCreatorScript;
      std::string mRunScript;
      std::string mValidationScript;

      std::string mFallBackInputScript;
      std::string mFallBackRunScript;
      bool mHaveFallBack = false;

      bool mDoMLPes = false;

      std::string mMLDriver = "";
      bool mHaveMLDrv = false;
      MLTask mMLTask;

      std::string mDatabaseFile = "/uninit-file";
      std::string mPropertyDescr = "GROUND_STATE_ENERGY";
      std::string mVarianceDescr = "VARIANCE";
      bool mStoreVariance = false;

      std::string mKernelType = "SC52";

      std::string mCoVar;
      bool mHaveCoVar = false;

      std::string mFileHparam = "/uninit-file";
      bool mHaveHparam = false;

      std::string mFileWeights = "";
      bool mHaveWeights = false;

      CoordType mCoordType = geo_coordtype::MINT;

      bool mIsDeltaLearning = true;
      bool mValidateViaVariance = false;
      bool mDoOpt = true;

      std::string mIcoordDef;
      bool mUseIcoordDef = false;

      // private functions
      void CreateXyzFile(std::string) const;

      // overloads   
      void CreateInputImpl() const;
      void SubmitImpl(bool) const;
      void ValidateCalculationImpl(bool&,bool&) const;
      void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const;

      void DumpUncertainty(In,Nb) const;

   public:
      MLSinglePoint() = delete;
      MLSinglePoint(const SinglePointInfo&, const pes::PropertyInfo&);
      virtual ~MLSinglePoint() = default;

};

#endif /* MLSINGLEPOINT_H_INCLUDED */
