/**
************************************************************************
* 
* @file                ModelSinglePoint.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   ModelSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/conversions/FromString.h"
#include "pes/singlepoint/ModelSinglePoint.h"
#include "pes/singlepoint/SinglePoint.h"
#include "pes/PesInfo.h"
#include "pes/GeneralProp.h"
#include "pes/CalcCode.h"
#include "potentials/ModelPot.h"
#include "potentials/PotentialHandler.h"

#include "input/GlobalData.h"

// using declarations 
using namespace midas;

/////
// Register for factory
/////
SinglePointRegistration<ModelSinglePoint> registerModelSinglePoint("SP_MODEL");

/**
 * Constructor
 *
 * @param aInfo   SinglePointInfo to construct from.
 * @param aPropertyInfo Information on the properties we should collect from the singlpoint.
 **/
ModelSinglePoint::ModelSinglePoint
   (  const SinglePointInfo& aInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
   :  SinglePointImpl(aInfo, aPropertyInfo)
   ,  mPotential(global::ServiceLocator().GetService<potentials::PotentialHandler>().GetPotential(midas::util::FromString<In>(const_cast<SinglePointInfo&>(aInfo)["MODELPOT_INSTANCE"])))
{
}

/**
 * @brief dtor
 **/
ModelSinglePoint::~ModelSinglePoint()
{
}

/**
 * @brief create input, does nothing as no input is nescessary
 **/
void ModelSinglePoint::CreateInputImpl() const
{
   return;
}

/**
 * Take care of running the calculation.
 **/
void ModelSinglePoint::SubmitImpl
   (  bool aSingle
   )  const
{
   // Write rotated geometry
   std::string file_name = mSpScratchDir + "/midasifc.cartrot";
   std::ofstream ofs(file_name);
   midas::stream::ScopedPrecision(SinglePointImpl::mXyzPrecision, ofs);
   
   ofs.setf(ios::scientific);

   for (In i = I_0; i < mStructure.size(); i++) 
   {
      std::string name = mStructure[i].AtomLabel();
      Nb x = mStructure[i].X();
      Nb y = mStructure[i].Y();
      Nb z = mStructure[i].Z();
      ofs << setw(4) << name << "   " << x << "   " << y << "   " << z << std::endl;
   }
   ofs.close();
   
   const auto property_names = mProperties.GetDescriptors();

   for(int i = 0; i < property_names.size(); ++i)
   {
      const auto& name = property_names[i];
      
      if(IsSpecialProperty(name))
      {
         continue;
      }

      auto provides = mPotential->Provides(name);
      
      auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(name);
      
      if(provides & potentials::provides::value)
      {
         Nb pot_val = mPotential->EvalPot(name, mStructure);
         prop.SetValue(pot_val);
      }
      
      if (gPesCalcDef.GetmPesDoExtrap())
      {
         if (provides & potentials::provides::first_derivatives)
         {
            MidasVector first_der_val; 
            first_der_val = mPotential->EvalDer(name, mStructure);
            prop.SetFirstDerivatives(first_der_val);
         }
         if (provides & potentials::provides::second_derivatives)
         {
            MidasMatrix second_der_val; 
            second_der_val = mPotential->EvalHess(name, mStructure);
            prop.SetSecondDerivatives(second_der_val);
         }
      }
   }
}

/**
 * Check that the calculation finished correctly.
 * Analytical potentials are always 'sane', so we just return.
**/
void ModelSinglePoint::ValidateCalculationImpl
   (  bool& aHalt
   ,  bool& aSuspicious
   )  const
{
   return; 
}
