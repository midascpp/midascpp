#include "Converter.h"

#include <iostream>
#include <map>

#include "util/MidasStream.h"
#include "util/Error.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "nuclei/Nuclei.h"

using std::vector;
using std::map;
using std::string;

/**
 * Add property to midasifc.prop_general
 *
 * @param aOrder   Order of the property.
 * @param aS       Property description.
 * @param aProp    Property value.
 * @param aI       Rotation group.
 * @param aElem    Element index.
 **/
void Converter::AddToPropFile
   (  In aOrder
   ,  const std::string& aS
   ,  Nb aProp
   ,  In aI
   ,  const std::vector<In> *aElem
   )
{
   //string filename=gsaveiodir+"/midasifc.propinfo";
   //ofstream ofs(filename.c_str(),ios::app);
   //ofs << "tens_order=(" << aOrder << "),";
   //ofs << "descriptor=(" << aS << ")";
   //if(aI>-I_1)
   //   ofs << ",Rot_group=(" << aI << ")";
   //if(aElem!=0) {
   //   ofs << ",element=(";
   //   for(In i=0;i<aElem->size()-1;i++)
   //      ofs << (*aElem)[i] << ",";
   //   ofs << (*aElem)[aElem->size()-1] << ")";
   //}
   //// NOTE!!!! THIS IS TO KEEP OLD FUNCTIONALITY REGARDING DERIVATIVES
   //map<string,string> wripro_der_map;
   //wripro_der_map.insert(std::make_pair("GROUND_STATE_ENERGY","midasifc.ederivs"));
   //wripro_der_map.insert(std::make_pair("X_DIPOLE","midasifc.muXder"));
   //wripro_der_map.insert(std::make_pair("Y_DIPOLE","midasifc.muYder"));
   //wripro_der_map.insert(std::make_pair("Z_DIPOLE","midasifc.muZder"));
   //map<string,string>::iterator it=wripro_der_map.find(aS);
   //if(it!=wripro_der_map.end()) {
   //   filename=mInputPropDir+"/"+it->second;
   //   if(InquireFile(filename)) {
   //      // the file exists, add to propinfo file
   //      ofs << ",der_file=(" << it->second << ")";
   //   }
   //}
   //ofs << endl;
   //ofs.close();
   //// DONE!!!!

   // also write propertyfile
   std::string filename_prop_general = mInputPropDir + "/midasifc.prop_general";
   std::ofstream ofs_prop_general(filename_prop_general, ios::app);
   ofs_prop_general.setf(ios::scientific);
   ofs_prop_general.precision(16);
   ofs_prop_general << aS << "   " << aProp << std::endl;
   ofs_prop_general.close();
}

/**
 * Add property to midasifc.prop_general.
 *
 * @param aOrder   Order of the property.
 * @param aS       Property description.
 * @param aV       A frequency vector.
 * @param aProp    Property value.
 * @param aI       Rotation group.
 * @param aElem    Element index.
 **/
void Converter::AddToPropFile
   (  In aOrder
   ,  const std::string& aS
   ,  std::vector<Nb> aV
   ,  Nb aProp
   ,  In aI
   ,  const std::vector<In> *aElem
   )
{
   //string filename=gsaveiodir+"/midasifc.propinfo";
   //ofstream ofs(filename.c_str(),ios::app);
   //ofs << "tens_order=(" << aOrder << "),";
   //ofs << "descriptor=(" << aS << "),frq=(";
   //for(In i=0;i<aV.size()-1;i++)
   //   ofs << aV[i] << ",";
   //ofs << aV[aV.size()-1] << ")";
   //if(aI>-I_1)
   //   ofs << ",Rot_group=(" << aI << ")";
   //if(aElem!=0) {
   //   ofs << ",element=(";
   //   for(In i=0;i<aElem->size()-1;i++)
   //      ofs << (*aElem)[i] << ",";
   //   ofs << (*aElem)[aElem->size()-1] << ")";
   //}
   //// NOTE!!!! THIS IS TO KEEP OLD FUNCTIONALITY REGARDING DERIVATIVES
   //map<string,string> wripro_der_map;
   //wripro_der_map.insert(std::make_pair("GROUND_STATE_ENERGY","midasifc.ederivs"));
   //wripro_der_map.insert(std::make_pair("X_DIPOLE","midasifc.muXder"));
   //wripro_der_map.insert(std::make_pair("Y_DIPOLE","midasifc.muYder"));
   //wripro_der_map.insert(std::make_pair("Z_DIPOLE","midasifc.muZder"));
   //map<string,string>::iterator it=wripro_der_map.find(aS);
   //if(it!=wripro_der_map.end()) {
   //   filename=mInputPropDir+"/"+it->second;
   //   if(InquireFile(filename)) {
   //      // the file exists, add to propinfo file
   //      ofs << ",der_file=(" << it->second << ")";
   //   }
   //}
   //// DONE!!!!
   //ofs << endl;
   //ofs.close();
   
   // also write propertyfile
   std::string filename_prop_general = mInputPropDir + "/midasifc.prop_general";
   std::ofstream ofs_prop_general(filename_prop_general, ios::app);
   ofs_prop_general.setf(ios::scientific);
   ofs_prop_general.precision(16);
   ofs_prop_general << aS << "   " << aProp << std::endl;
   ofs_prop_general.close();
}

/**
 * Default constructor
 *
 * @param aInputPropDir   The scratch directory.
 * @param aLabels         Some labels?
 **/
Converter::Converter
   (  const std::string& aInputPropDir
   ,  const std::vector<std::pair<std::string, std::string> >& aLabels
   )
   :  mInputPropDir(aInputPropDir)
   ,  mLabels(aLabels)
{
}

/**
*  Our good old reader of WRIPRO generated output
*  Purpose: Generate midasifc.prop_general
*           from WRIPRO generated output for later use
*           in new framework.
**/
void Converter::StandardWriproReader
   (
   )
{
   // Hold the dipole moment
   MidasVector dip(I_3);
   // inertia tensor
   MidasMatrix in_tens(I_3,C_0);
   // polarizability
   map<Nb,MidasMatrix> pol;
   // hyper-polarizability
   map<vector<Nb>,Nb***> hpol;
   // 2nd hyper-polarizability
   map<vector<Nb>,Nb****> h2pol;
   // NMR shieldings - property 402
   vector< pair<string,MidasMatrix> > nmr_shield;
   // Electric field gradients (EFG) - property 502
   vector< pair<string,MidasMatrix> > efg_vec;
   //  Residue - property -20
   map<Nb,MidasMatrix> res;

   // loop through property file and construct the above quantities
   ifstream ifs;
   std::string filename = mInputPropDir+"/midasifc.prop";
   ifs.open(filename.c_str(),ios::in);
   string line;
   bool have_dipole=false;
   bool have_inertia=false;
   list<In> ord_set;
   while(getline(ifs,line)) {
      istringstream iss(line);
      vector<string> opers(I_4);
      In junk2;
      string  junk3;
      In n_ord, isymex, ispinex, inrex;
      In i_prop;
      Nb prop,frq1,frq2,frq3;
      iss >> i_prop;
      iss >> junk2;
      iss >> n_ord;
      iss >> junk3;
      iss >> prop;
      iss >> opers[I_0];
      iss >> opers[I_1];
      iss >> opers[I_2];
      iss >> opers[I_3];
      iss >> frq1;
      iss >> frq2;
      iss >> frq3;
      iss >> isymex;
      iss >> ispinex;
      iss >> inrex;
      
      ord_set.push_back(n_ord);

      // if energy or trs moment (?) continue
      if(n_ord==I_0){
         if(opers[I_0]=="ENERGY")
            AddToPropFile(I_0,"GROUND_STATE_ENERGY",prop);
         else
            AddToPropFile(I_0,opers[I_0],prop);
         continue;
      }
      if(n_ord>I_4)
      {
         if(n_ord==666)
            continue;
         // special cases: NMR + EFG
         if( !(n_ord==401 || n_ord==402 || n_ord==502) ) {
            vector<Nb> frq;
            frq.push_back(frq1);
            frq.push_back(frq2);
            frq.push_back(frq3);
            AddToPropFile(n_ord,opers[I_0],frq,prop);
            continue;
         }
      }
      // if not orientation dependent prop, continue
      // not related to n_ord, therefore outside else-if structure
      // below...
      if(opers[I_0].find("X")==opers[I_0].npos &&
         opers[I_0].find("Y")==opers[I_0].npos &&
         opers[I_0].find("Z")==opers[I_0].npos) {
            vector<Nb> frq;
            frq.push_back(frq1);
            frq.push_back(frq2);
            frq.push_back(frq3);
            AddToPropFile(n_ord,opers[I_0],frq,prop);
         continue;
      }

      // if mu_linear treat as a number..
      if (n_ord==-1 && (opers[I_0].find("LINEAR")!=opers[I_0].npos))
      {
         AddToPropFile(I_0,"LINEAR",prop);
         //mPropNumber++;
         continue;
      }
      // consider inertia tensor
      else if (n_ord==-1 && !(opers[I_0].find("LINEAR")!=opers[I_0].npos))
      {
         if(opers[I_0].find("XX")!=opers[I_0].npos)
         {
            // xx component
            in_tens[I_0][I_0]=prop;
         }
         if(opers[I_0].find("XY")!=opers[I_0].npos)
         {
            // xy component
            in_tens[I_0][I_1]=prop;
            in_tens[I_1][I_0]=prop;
         }
         if(opers[I_0].find("XZ")!=opers[I_0].npos)
         {
            // xz component
            in_tens[I_0][I_2]=prop;
            in_tens[I_2][I_0]=prop;
         }
         if(opers[I_0].find("YY")!=opers[I_0].npos)
         {
            // xx component
            in_tens[I_1][I_1]=prop;
         }
         if(opers[I_0].find("YZ")!=opers[I_0].npos)
         {
            // xx component
            in_tens[I_1][I_2]=prop;
            in_tens[I_2][I_1]=prop;
         }
         if(opers[I_0].find("ZZ")!=opers[I_0].npos)
         {
            // zz component
            in_tens[I_2][I_2]=prop;
         }
         have_inertia=true;
      }
      // Just the 'easy' part left :)
      else if(n_ord==I_1) {
         // dipole moment
         if(opers[I_0].find("X")!=opers[I_0].npos) {
            dip[I_0]=prop;
         }
         else if(opers[I_0].find("Y")!=opers[I_0].npos) {
            dip[I_1]=prop;
         }
         else if(opers[I_0].find("Z")!=opers[I_0].npos) {
            dip[I_2]=prop;
         }
         else
            continue;
         have_dipole=true;
      }
      else if(n_ord==I_2) {
         map<Nb,MidasMatrix>::iterator is_pol=pol.find(frq1);
         bool have_added=(is_pol==pol.end());
         // polarizability?
         MidasMatrix addition(I_3,C_0);
         if(opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) {
            // xx component
            addition[I_0][I_0]=prop;
         }
         if( (opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("Y")!=opers[I_1].npos) ||
            (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_pol->second)[I_0][I_1])>C_0)
               continue;
            addition[I_0][I_1]=prop;
            addition[I_1][I_0]=prop;
         }
         if( (opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ||
            (opers[I_0].find("Z")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_pol->second)[I_0][I_2])>C_0)
               continue;
            addition[I_0][I_2]=prop;
            addition[I_2][I_0]=prop;
         }
         if(opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Y")!=opers[I_1].npos) {
            // xy, yx component
            addition[I_1][I_1]=prop;
         }
         if( (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ||
            (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_pol->second)[I_1][I_2])>C_0)
               continue;
            addition[I_1][I_2]=prop;
            addition[I_2][I_1]=prop;
         }
         if(opers[I_0].find("Z")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) {
            // xy, yx component
            addition[I_2][I_2]=prop;
         }
         // 1) see if we have it already
         if(have_added) {
            pol.insert(std::make_pair(frq1,addition));
         }
         else {
            (is_pol->second)+=addition;
         }
      }
      else if(n_ord==I_3) {
         vector<Nb> tmp_frq;
         tmp_frq.push_back(frq1);
         tmp_frq.push_back(frq2);
         map<vector<Nb>,Nb***>::iterator is_hpol=hpol.find(tmp_frq);
         bool have_added=(is_hpol==hpol.end());
         // Do the real stuff... Boring if's
         In index0=-I_1,index1=-I_1,index2=-I_1;
         if(opers[I_0].find("X")!=opers[I_0].npos)
                 index0=I_0;
         else if(opers[I_0].find("Y")!=opers[I_0].npos)
            index0=I_1;
         else if(opers[I_0].find("Z")!=opers[I_0].npos)
            index0=I_2;
         if(opers[I_1].find("X")!=opers[I_1].npos)
            index1=I_0;
         else if(opers[I_1].find("Y")!=opers[I_1].npos)
            index1=I_1;
         else if(opers[I_1].find("Z")!=opers[I_1].npos)
            index1=I_2;
         if(opers[I_2].find("X")!=opers[I_2].npos)
            index2=I_0;
         else if(opers[I_2].find("Y")!=opers[I_2].npos)
            index2=I_1;
         else if(opers[I_2].find("Z")!=opers[I_2].npos)
            index2=I_2;
         if(have_added) {
            Nb*** addition;
            addition = new Nb**[I_3];
            // initialize to all zeros
            for(In i=I_0;i<I_3;i++) {
               addition[i] = new Nb*[I_3];
               for(In j=I_0;j<I_3;j++) {
                  addition[i][j]=new Nb[I_3];
               }
            }
            for(In i=I_0;i<I_3;i++)
               for(In j=I_0;j<I_3;j++)
                  for(In k=I_0;k<I_3;k++)
                     addition[i][j][k]=C_0;
            addition[index0][index1][index2]=prop;
            hpol.insert(std::make_pair(tmp_frq,addition));
         }
         else {
            (is_hpol->second)[index0][index1][index2]=prop;
         }
      }
      else if(n_ord==I_4) {
         vector<Nb> tmp_frq;
         tmp_frq.push_back(frq1);
         tmp_frq.push_back(frq2);
         tmp_frq.push_back(frq3);
         map<vector<Nb>,Nb****>::iterator is_hpol=h2pol.find(tmp_frq);
         bool have_added=(is_hpol==h2pol.end());
         // Do the real stuff... Boring if's
         In index0=-I_1,index1=-I_1,index2=-I_1,index3=-I_1;
         if(opers[I_0].find("X")!=opers[I_0].npos)
                 index0=I_0;
         else if(opers[I_0].find("Y")!=opers[I_0].npos)
            index0=I_1;
         else if(opers[I_0].find("Z")!=opers[I_0].npos)
            index0=I_2;
         if(opers[I_1].find("X")!=opers[I_1].npos)
            index1=I_0;
         else if(opers[I_1].find("Y")!=opers[I_1].npos)
            index1=I_1;
         else if(opers[I_1].find("Z")!=opers[I_1].npos)
            index1=I_2;
         if(opers[I_2].find("X")!=opers[I_2].npos)
            index2=I_0;
         else if(opers[I_2].find("Y")!=opers[I_2].npos)
            index2=I_1;
         else if(opers[I_2].find("Z")!=opers[I_2].npos)
            index2=I_2;
         if(opers[I_3].find("X")!=opers[I_3].npos)
            index3=I_0;
         else if(opers[I_3].find("Y")!=opers[I_3].npos)
            index3=I_1;
         else if(opers[I_3].find("Z")!=opers[I_3].npos)
            index3=I_2;
         if(have_added) {
            Nb**** addition;
            addition = new Nb***[I_3];
            // initialize to all zeros
            for(In i=I_0;i<I_3;i++) {
               addition[i] = new Nb**[I_3];
               for(In j=I_0;j<I_3;j++) {
                  addition[i][j]=new Nb*[I_3];
                  for(In k=I_0;k<I_3;k++) {
                     addition[i][j][k]=new Nb[I_3];
                  }
               }
            }
            for(In i=I_0;i<I_3;i++)
               for(In j=I_0;j<I_3;j++)
                  for(In k=I_0;k<I_3;k++)
                     for(In l=I_0;l<I_3;l++)
                        addition[i][j][k][l]=C_0;
            addition[index0][index1][index2][index3]=prop;
            h2pol.insert(std::make_pair(tmp_frq,addition));
         }
         else {
            (is_hpol->second)[index0][index1][index2][index3]=prop;
         }
      }
      else if(n_ord==-20) {
         map<Nb,MidasMatrix>::iterator is_res=res.find(frq1);
         bool have_added=(is_res==res.end());
         // residue 
         MidasMatrix addition(I_3,C_0);
         if(opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) {
            // xx component
            addition[I_0][I_0]=prop;
         }
         if( (opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("Y")!=opers[I_1].npos) ||
            (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_res->second)[I_0][I_1])>C_0)
               continue;
            addition[I_0][I_1]=prop;
            addition[I_1][I_0]=prop;
         }
         if( (opers[I_0].find("X")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ||
            (opers[I_0].find("Z")!=opers[I_0].npos &&
            opers[I_1].find("X")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_res->second)[I_0][I_2])>C_0)
               continue;
            addition[I_0][I_2]=prop;
            addition[I_2][I_0]=prop;
         }
         if(opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Y")!=opers[I_1].npos) {
            // xy, yx component
            addition[I_1][I_1]=prop;
         }
         if( (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ||
            (opers[I_0].find("Y")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) ) {
            // xy, yx component
            if(!have_added && fabs((is_res->second)[I_1][I_2])>C_0)
               continue;
            addition[I_1][I_2]=prop;
            addition[I_2][I_1]=prop;
         }
         if(opers[I_0].find("Z")!=opers[I_0].npos &&
            opers[I_1].find("Z")!=opers[I_1].npos) {
            // xy, yx component
            addition[I_2][I_2]=prop;
         }
         // 1) see if we have it already
         if(have_added) {
            res.insert(std::make_pair(frq1,addition));
         }
         else {
            (is_res->second)+=addition;
         }
      }
      else if(n_ord==402) {
         if(gDebug)
            Mout << "Component of NMR SHIELDING TENSOR!" << endl;
         string atom=opers[I_2];
         if(gDebug)
            Mout << "Atom = \"" << atom << "\"" << endl;
         if(atom.find("_")==atom.npos)
            atom.append(6-atom.size(),' ');
         else {
            atom=atom.insert(atom.find("_"),I_7-atom.size(),' ');
            atom=atom.erase(atom.find("_"),I_1);
         }
         if(gDebug)
            Mout << "Atom = \"" << atom << "\"" << endl;
         In index0 = -1, index1 = -1;
         MidasMatrix unitmatrix(I_3);
         unitmatrix.Unit();
         if(opers[I_0].find("X")!=opers[I_0].npos)
            index0=I_0;
         else if(opers[I_0].find("Y")!=opers[I_0].npos)
            index0=I_1;
         else if(opers[I_0].find("Z")!=opers[I_0].npos)
            index0=I_2;
         else
            MIDASERROR("Something wrong with one of the opers!");
         if(opers[I_1].find("X")!=opers[I_1].npos)
            index1=I_0;
         else if(opers[I_1].find("Y")!=opers[I_1].npos)
            index1=I_1;
         else if(opers[I_1].find("Z")!=opers[I_1].npos)
            index1=I_2;
         else
            MIDASERROR("Something wrong with one of the opers!");
         // look in nmr_shield if we have something for this atom
         bool atom_found=false;
         for(In i=0;i<nmr_shield.size();i++) {
            if(nmr_shield[i].first==atom) {
               nmr_shield[i].second(index0,index1)=prop;
               atom_found=true;
               break;
            }
         }
         if(!atom_found) {
            MidasMatrix this_nmr_shield(I_3,C_0);
            this_nmr_shield.Unit();
            this_nmr_shield(index0,index1)=prop;
            if(gDebug)
               Mout << "PUSHING BACK ATOM:" << atom << endl;
            nmr_shield.push_back(std::make_pair(atom,this_nmr_shield));
         }
      }
      else if(n_ord==502) {
         if(gDebug)
            Mout << "Component of Electric field gradient!" << endl;
         string atom=opers[I_2];
         if(gDebug)
            Mout << "Atom = \"" << atom << "\"" << endl;
         if(atom.find("_")==atom.npos)
            atom.append(6-atom.size(),' ');
         else {
            atom=atom.insert(atom.find("_"),I_7-atom.size(),' ');
            atom=atom.erase(atom.find("_"),I_1);
         }
         if(gDebug)
            Mout << "Atom = \"" << atom << "\"" << endl;
         In index0 = -1, index1 = -1;
         if(opers[I_0].find("X")!=opers[I_0].npos)
            index0=I_0;
         else if(opers[I_0].find("Y")!=opers[I_0].npos)
            index0=I_1;
         else if(opers[I_0].find("Z")!=opers[I_0].npos)
            index0=I_2;
         else
            MIDASERROR("Something wrong with one of the opers!");
         if(opers[I_1].find("X")!=opers[I_1].npos)
            index1=I_0;
         else if(opers[I_1].find("Y")!=opers[I_1].npos)
            index1=I_1;
         else if(opers[I_1].find("Z")!=opers[I_1].npos)
            index1=I_2;
         else
            MIDASERROR("Something wrong with one of the opers!");
         bool atom_found=false;
         for(In i=0;i<efg_vec.size();i++) {
            if(efg_vec[i].first==atom) {
               efg_vec[i].second(index0,index1)=prop;
               atom_found=true;
               break;
            }
         }
         if(!atom_found) {
            MidasMatrix this_efg(I_3,C_0);
            this_efg.Unit();
            this_efg(index0,index1)=prop;
            if(gDebug)
               Mout << "PUSHING BACK ATOM:" << atom << endl;
            efg_vec.push_back(std::make_pair(atom,this_efg));
         }
      }
      else
         Mout << "n_ord =" << n_ord << "    NOT IMPLEMENTED YET!!!" << endl;
   } // end while
   ifs.close();

   // construct midasifc.propinfo file and write properties there too
   // No rotation! This is taken care of by general framework
   // following two quantities are helpers
   vector<In> helper_vec;
   In rotation_group=I_0;

   ord_set.sort();
   ord_set.unique();
   for (list<In>::iterator Sii=ord_set.begin();Sii!=ord_set.end(); Sii++)
   {
      if(have_dipole && (*Sii)==I_1) {
         helper_vec.push_back(I_0);
         AddToPropFile(I_1,"X_DIPOLE",dip[0],rotation_group,&helper_vec);
         helper_vec[I_0]=I_1;
         AddToPropFile(I_1,"Y_DIPOLE",dip[1],rotation_group,&helper_vec);
         helper_vec[I_0]=I_2;
         AddToPropFile(I_1,"Z_DIPOLE",dip[2],rotation_group,&helper_vec);
         rotation_group++;
      }
      else if(have_inertia && (*Sii)==-I_1) {
         helper_vec.clear();
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         for(In i=0;i<I_3;i++) {
            helper_vec[I_0]=i;
            for(In j=i;j<I_3;j++) {
               helper_vec[I_1]=j;
               string lab="XXITENS";
               char c1=char(i+88);
               char c2=char(j+88);
               lab.at(0)=c1;
               lab.at(1)=c2;
               AddToPropFile(I_2,lab,in_tens[i][j],rotation_group,&helper_vec);
            }
         }
         rotation_group++;
      }
      else if ((*Sii)==I_2){
         helper_vec.clear();
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         for(map<Nb,MidasMatrix>::iterator i_pol=pol.begin();i_pol!=pol.end();i_pol++) {
            MidasMatrix mat=i_pol->second;
            vector<Nb> frq;
            frq.push_back(i_pol->first);
            for(In i=0;i<I_3;i++) {
               helper_vec[I_0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[I_1]=j;
                  string lab="XX_POL_"+StringForNb(frq[I_0]);
                  char c1=char(i+88);
                  char c2=char(j+88);
                  lab.at(0)=c1;
                  lab.at(1)=c2;
                  AddToPropFile(I_2,lab,frq,mat[i][j],rotation_group,&helper_vec);
               }
            }
            rotation_group++;
         }
      }
      else if ((*Sii)==-20){
         helper_vec.clear();
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         for(map<Nb,MidasMatrix>::iterator i_res=res.begin();i_res!=res.end();i_res++) {
            MidasMatrix mat=i_res->second;
            vector<Nb> frq;
            frq.push_back(i_res->first);
            for(In i=0;i<I_3;i++) {
               helper_vec[I_0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[I_1]=j;
                  string lab="XX_RES_"+StringForNb(frq[I_0]);
                  char c1=char(i+88);
                  char c2=char(j+88);
                  lab.at(0)=c1;
                  lab.at(1)=c2;
                  AddToPropFile(I_2,lab,frq,mat[i][j],rotation_group,&helper_vec);
               }
            }
            rotation_group++;
         }
      }
      else if ((*Sii)==I_3){
         helper_vec.clear();
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         for(map<vector<Nb>,Nb***>::iterator i_pol=hpol.begin();i_pol!=hpol.end();i_pol++) {
            // print the property to ROTATION.TEMP
            vector<Nb> frqs=i_pol->first;
            Nb*** hpol=i_pol->second;
            for(In i=0;i<I_3;i++) {
               helper_vec[I_0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[I_1]=j;
                  for(In k=0;k<I_3;k++) {
                     helper_vec[I_2]=k;
                     string lab="XXX_HPOL_"+StringForNb(frqs[I_0])+"_"+StringForNb(frqs[I_1]);
                     char c1=char(i+88);
                     char c2=char(j+88);
                     char c3=char(k+88);
                     lab.at(0)=c1;
                     lab.at(1)=c2;
                     lab.at(2)=c3;
                     AddToPropFile(I_3,lab,frqs,hpol[i][j][k],rotation_group,&helper_vec);
                  }
               }
            }
            rotation_group++;
         }
      }
      else if ((*Sii)==I_4){
         helper_vec.clear();
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         helper_vec.push_back(0);
         for(map<vector<Nb>,Nb****>::iterator i_pol=h2pol.begin();i_pol!=h2pol.end();i_pol++) {
            // Hyperpolarizability
            vector<Nb> frqs=i_pol->first;
            Nb**** hpol=i_pol->second;
            for(In i=0;i<I_3;i++) {
               helper_vec[I_0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[I_1]=j;
                  for(In k=0;k<I_3;k++) {
                     helper_vec[I_2]=k;
                     for(In l=0;l<I_3;l++) {
                        helper_vec[I_3]=l;
                        string lab="XXXX_HPOL_"+StringForNb(frqs[I_0])+"_";
                        lab+=StringForNb(frqs[I_1])+"_"+StringForNb(frqs[I_2]);
                        char c1=char(i+88);
                        char c2=char(j+88);
                        char c3=char(k+88);
                        char c4=char(l+88);
                        lab.at(0)=c1;
                        lab.at(1)=c2;
                        lab.at(2)=c3;
                        lab.at(3)=c4;
                        AddToPropFile(I_4,lab,frqs,hpol[i][j][k][l],rotation_group,&helper_vec);
                     }
                  }
               }
            }
            rotation_group++;
         }
      } 
      else if ((*Sii)==402) {
         // We have NMR shieldings,
         // 1) Rotate \sigma for each atom
         // 2) print out in specific order
         //    remember to use label_label map

         // print it out remember to take care of atoms order
         if (mLabels.size() == I_0)
         {
            MIDASERROR("Could not take care of this prop without a map of the rot-> original lables"); 
         }
         //
         //
         for(In k=0;k<mLabels.size();k++) {
            In atom_idx=-I_1;
            //if(mInternal) 
            //{
            //   // the order in the prop file is alrady correct
            //   atom_idx=k;
            //}
            // USE MANUELS MAP: assuming dalton -> midas labelling
            string dal_label=mLabels[k].first;
            if(gDebug)
               Mout << "nmr_shield.size() = " << nmr_shield.size() << endl;
            for(In l=0;l<mLabels.size();l++) {
               if(atom_idx > -I_1)
                  break;
               if(nmr_shield[l].first==dal_label) {
                  if(gDebug)
                     Mout << "ATOM FOUND: " << nmr_shield[l].first << endl;
                  atom_idx=l;
                  break;
               }
            }
            if(atom_idx==-I_1)
            {
               MIDASERROR("Could not find atom: "+mLabels[k].second);
            }
            MidasMatrix mat=nmr_shield[atom_idx].second;
            helper_vec.clear();
            helper_vec.push_back(I_0);
            helper_vec.push_back(I_0);
            for(In i=0;i<I_3;i++) {
               helper_vec[0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[1]=j;
                  string lab="XX_SHIELD_"+mLabels[k].second;
                  char c1=char(i+88);
                  char c2=char(j+88);
                  lab.at(0)=c1;
                  lab.at(1)=c2;
                  AddToPropFile(2,lab,mat[i][j],rotation_group,&helper_vec);
               }
            }
            rotation_group++;
         }
      }
      else if ((*Sii)==502) {
         // We have EFG's
         // 1) Rotate EFG for each atom
         // 2) print out in specific order
         //    remember to use label_label map
         if (mLabels.size() == I_0) 
            MIDASERROR("Could not take care of this prop without a map of the rot-> original lables"); 

         for(In k=0;k<mLabels.size();k++) {
            In atom_idx=-I_1;
            //if(mInternal) {
            //   // the order in the prop file is alrady correct
            //   atom_idx=k;
            //}
            // USE MANUELS MAP: assuming dalton -> midas labelling
            string dal_label=mLabels[k].first;
            for(In l=0;l<mLabels.size();l++) {
               if(atom_idx > -I_1)
                  break;
               if(efg_vec[l].first==dal_label) {
                  if(gDebug)
                     Mout << "ATOM FOUND: " << efg_vec[l].first << endl;
                  atom_idx=l;
                  break;
               } 
            }
            if(atom_idx==-I_1)
               MIDASERROR("Could not find atom: "+mLabels[k].second);
            MidasMatrix mat=efg_vec[atom_idx].second;
            helper_vec.clear();
            helper_vec.push_back(0);
            helper_vec.push_back(0);
            for(In i=0;i<I_3;i++) {
               helper_vec[0]=i;
               for(In j=0;j<I_3;j++) {
                  helper_vec[1]=j;
                  string lab="XX_EFG_"+mLabels[k].second;
                  char c1=char(i+88);
                  char c2=char(j+88);
                  lab.at(0)=c1;
                  lab.at(1)=c2;
                  AddToPropFile(2,lab,mat[i][j],rotation_group,&helper_vec);
               }
            }
            rotation_group++;
         }
      }
   }
}
