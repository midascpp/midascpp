/**
************************************************************************
*
* @file                AsyncSinglePointDriver.h
*
* Created:             01-05-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk) refactored CalculationList
*
* Short Description:   Handles parallel calculation of singlepoints
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ASYNCSINGLEPOINTDRIVER_H_INCLUDED
#define ASYNCSINGLEPOINTDRIVER_H_INCLUDED

//#include <memory>
#include <map>
#include <list>
#include <string>
#include <functional>

#include "SinglePoint.h"
#include "util/paral/thread_pool.h"

using namespace midas;

/**
 * Single point runner class.
 * Will hold a generic setup and teardown function,
 * to be able to setup and teardown a singlepoint calculation,
 * all in parallel.
 *
 * Setup function should setup Singlepoint, e.g. generate a structure and create a SinglePoint from it.
 * Setup function must return a "SinglePoint".
 *
 * Teardown function will get passed the pes::GeneralProp generated by the SinglePoint, 
 * and should then load that into an output struct of some kind.
 * Teardown function must take a "const pes::GeneralProp&", and return nothing.
 **/
class SinglePointRunner
{
   private:

      //! Unique singlepoint identifier
      std::string mUid;
      //! Setup function
      std::function<SinglePoint()> mSetup;
      //! Teardown function
      std::function<void(const pes::GeneralProp&)> mTeardown;

   public:
      //! Constructor from setup and teardown function
      SinglePointRunner
         (  const std::string& aUid
         ,  std::function<SinglePoint()> aSetup
         ,  std::function<void(const pes::GeneralProp&)> aTeardown
         )
         :  mUid(aUid)
         ,  mSetup(aSetup)
         ,  mTeardown(aTeardown)
      {
      }

      //! Get unique id of singlepoint
      const std::string& GetUid() const
      {
         return mUid;
      }

      
      //! Call the loaded setup function
      SinglePoint Setup()
      {
         return mSetup();
      }

      //! Call the loaded teardown function
      void Teardown(const pes::GeneralProp& aProp)
      {
         mTeardown(aProp);
      }
};

/**
 * Class for handling parallel calculation of singlepoints.
 **/
class AsyncSinglePointDriver
{
   private:
      
      //! Path to directory where back-up file should be placed.
      std::string mBackupDir; 
      //! Dump-interval.
      In mDumpInterval = 0;
      //! Number of threads we wish to use for singlepoints.
      In mNumThreads;

      
   public:
      //! Constructor
      AsyncSinglePointDriver(In aNumThreads = 0, const std::string& aBackupDir = "", In aDumpInterval = 5);
      
      //! interface for evaluating list of SinglePointRunners asynchronously (in parallel) which returns when ALL points are done
      void EvaluateList(std::list<SinglePointRunner>&);
      
      //! interface for evaluating list of singlepoints asynchronously (in parallel) which returns when ALL points are done
      void EvaluateList(std::list<SinglePoint>&);
};

#endif /* ASYNCSINGLEPOINTDRIVER_H_INCLUDED */
