/**
************************************************************************
* 
* @file                MLSinglePoint.cc
*
* Created:             11-06-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   SinglePoint datatype for Machine Learning 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <fstream>
#include <mutex>

#include "pes/singlepoint/MLSinglePoint.h"

// std headers
#include <string>
#include <vector>
#include <unistd.h>
#include <errno.h>
#include <chrono>
#include <stdio.h>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/paral/run_async.h"
#include "util/paral/run_process.h"
#include "util/FileSystem.h"
#include "mpi/Interface.h"
#include "mlearn/GauPro.h"
#include "mlearn/mlutil.h"
#include "input/MLCalcDef.h"


// using declarations
using std::vector;

/////
// Register for factory
/////
SinglePointRegistration<MLSinglePoint> registerMLSinglePoint("SP_ML");

/**
 * Constructor
 **/
MLSinglePoint::MLSinglePoint
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
   :  SinglePointImpl(aSpInfo, aPropertyInfo)
   ,  mSetupDir(aSpInfo.at("SETUPDIR"))
   ,  mSaveIoDir(aSpInfo.at("SAVEDIR"))
{

   SinglePointInfo::const_iterator iter;

   // Check for definition of ML TASK
   if((iter = aSpInfo.find("MLDRIVER")) == aSpInfo.end())
   {
      MIDASERROR("No MLDRIVER in SinglePointInfo map");
   }
   mMLDriver  = iter->second;
   mHaveMLDrv = (mMLDriver != "");

   //---------------------------------------------------------------------------------------------------+
   //  Setup SinglePoint information
   //---------------------------------------------------------------------------------------------------+

   if (!mHaveMLDrv)
   {
      MIDASERROR("You need to specify a MLTask to be used"); 
   }
   else
   {
      //--------------------------------------------------------------------------+
      // Find MLTask and setup options 
      //--------------------------------------------------------------------------+
      std::string identifier = mMLDriver;
      auto it = find_if(gMLCalcDef.begin(), gMLCalcDef.end(), [&identifier](const MLCalcDef& obj) {return obj.GetName() == identifier;});
      
      if (it != gMLCalcDef.end())
      {
         auto index = std::distance(gMLCalcDef.begin(), it);

         // Create MLTask
         mMLTask.Initialize(&gMLCalcDef[index]);

         // Set ML realted options according to setting in CalcDef
         mDatabaseFile  = gMLCalcDef[index].GetDatabase();
     
         mCoordType = gMLCalcDef[index].GetCoordinateType(); 
 
         mIcoordDef     = gMLCalcDef[index].GetIcoordDefFile();
         mUseIcoordDef  = gMLCalcDef[index].UseIcoordDefintion();

         mCoVar = "";
         mHaveCoVar = false;
         if (gMLCalcDef[index].GetReadCovar())
         {
            mCoVar = gMLCalcDef[index].GetFileCovarRead();
            mHaveCoVar = true;
         }

         mFileHparam = gMLCalcDef[index].GetFileHyperParam();   
         if (mFileHparam != "") mHaveHparam = true;

         mDoOpt = gMLCalcDef[index].GetDoHOpt();

         mHaveWeights = gMLCalcDef[index].GetReadWeights();
         mFileWeights = gMLCalcDef[index].GetFileWeightsRead();

      }
   }


   if((iter = aSpInfo.find("MLPROP")) == aSpInfo.end())
   {  
      MIDASERROR("No MLPROP in SinglePointInfo map");
   }
   mPropertyDescr = iter->second;

   if((iter = aSpInfo.find("MLVARIANCE")) != aSpInfo.end())
   {  
      mVarianceDescr = iter->second;
      mStoreVariance = mVarianceDescr != "";
   }

   // init input and runscript variables
   if((iter = aSpInfo.find("INPUTCREATORSCRIPT")) == aSpInfo.end())
   {
      MIDASERROR("No INPUTCREATORSCRIPT in SinglePointInfo map");
   }
   mInputCreatorScript = iter->second;
   if((iter = aSpInfo.find("RUNSCRIPT")) == aSpInfo.end())
   {
      MIDASERROR("No RUNSCRIPT in SinglePointInfo map");
   }
   mRunScript = iter->second;

   // find scripts for fallback option
   if((iter = aSpInfo.find("FALLBACKINPUT")) == aSpInfo.end())
   {
      MIDASERROR("No FALLBACKINPUT in SinglePointInfo map");
   }
   mFallBackInputScript = iter->second;

   if((iter = aSpInfo.find("FALLBACKRUN")) == aSpInfo.end())
   {
      MIDASERROR("No FALLBACKRUN in SinglePointInfo map");
   }
   mFallBackRunScript = iter->second;
   mHaveFallBack = (mFallBackInputScript != "" && mFallBackRunScript != "");

   //---------------------------------------------------------------------------------------------------+
   //  Sanity checks e.g. look if files are present or not
   //---------------------------------------------------------------------------------------------------+

   // check for existence of input creator script and run script
   std::string file_name = mSetupDir + "/" + mInputCreatorScript;
   if(!midas::filesystem::Exists(file_name))
   {
      MIDASERROR
         (  "Could not find InputCreatorScript: " + mInputCreatorScript + " remember to copy it to setup."
         ,  "Searched path: " + file_name + "\n" + strerror(errno)
         );
   }
   file_name = mSetupDir + "/" + mRunScript;
   if(!midas::filesystem::Exists(file_name))
   {
      MIDASERROR
         (  "Could not find RunScript: " + mRunScript + " remember to copy it to setup."
         ,  "Searched path: " + file_name + "\n" + strerror(errno)
         );
   }
   
   // check for existence of validation script input
   mValidationScript = "NoSuchFileOrDirectory";
   if((iter = aSpInfo.find("VALIDATIONSCRIPT")) != aSpInfo.end() && iter->second != "")
   {
      mValidationScript = iter->second;
      file_name = mSetupDir + "/" + mValidationScript;
      if(!midas::filesystem::Exists(file_name))
      {
         MIDASERROR
            (  "Could not find ValidationScript: " + mValidationScript + " remember to copy it to setup."
            ,  "Searched path: " + file_name + "\n" + strerror(errno)
            );
      }
   }

   // check for existence of input creater and run script for fall back option
   if (mHaveFallBack) 
   {
      file_name = mSetupDir + "/" + mFallBackInputScript;
      if(!midas::filesystem::Exists(file_name))
      {
         MIDASERROR
            (  "Could not find FallBackInputCreatorScript: " + mFallBackInputScript + " remember to copy it to setup."
            ,  "Searched path: " + file_name + "\n" + strerror(errno)
            );
      }

      file_name = mSetupDir + "/" + mFallBackRunScript;
      if(!midas::filesystem::Exists(file_name))
      {
         MIDASERROR
            (  "Could not find FallBackRunScript: " + mFallBackRunScript + " remember to copy it to setup."
            ,  "Searched path: " + file_name + "\n" + strerror(errno)
            );
      }

      mValidateViaVariance = true;
   }

   // check if we should enforce the fallback option. This means not using GPR at all
   mDoMLPes = false;
   if((iter = aSpInfo.find("MLPES")) != aSpInfo.end() && iter->second != "")
   {
      mDoMLPes = (iter->second == "TRUE");
      if (mDoMLPes && mHaveFallBack == false)
      {
         MIDASERROR
            (  "Definition of a fallback option in MLSinglePoint is missing"
            );
      }
   }

   // check for existence of database
   if(!midas::filesystem::Exists(mDatabaseFile))
   {
      MIDASERROR("Could not find database in " + mDatabaseFile); 
   }

   // check for hyper parameter file
   if (mHaveHparam)
   {
      if(!midas::filesystem::Exists(mFileHparam))
      {
         MIDASERROR("Could not find hyper paramter file in " + mFileHparam);
      }
   }

}


/**
 *
 **/
void MLSinglePoint::CreateInputImpl
   (
   ) const
{

   // We also dump a XYZ file so that in case of a problem we can take a look at it 
   std::string finput = "midasifc.xyz_input";
   CreateXyzFile(finput);

   // Now run the script
   if (mIsDeltaLearning)
   {
      system_command_t sub = { mSetupDir + "/" + mInputCreatorScript };
      int status = MIDASSYSTEM_DIR_STATUS(sub, mSpScratchDir);
      if(status)
      {
         MIDASERROR("COULD NOT CREATE INPUT FOR SINGLEPOINT IN " + mSpScratchDir);
      }

      // Do it here? Ideally we only need this input in a few cases, but generate it all the time!
      // However, generating the input should not take much time anyhow...
      if (mHaveFallBack)
      {
         system_command_t sub = { mSetupDir + "/" + mFallBackInputScript };
         int status = MIDASSYSTEM_DIR_STATUS(sub, mSpScratchDir);
         if(status)
         {
            MIDASERROR("COULD NOT CREATE FALLBACK INPUT FOR SINGLEPOINT IN " + mSpScratchDir);
         }
      }
   }
}


/**
 * Take care of running the calculation.
 **/
void MLSinglePoint::SubmitImpl
   ( bool aSingle
   ) const // dummy bool :S (the need for this should be removed sometime!)
{

   const bool locdbg = false;

   //-------------------------------------------------+
   // run low level calc. if delta learning is used 
   //-------------------------------------------------+
   std::vector<std::string> prop_descriptors;
   if (mIsDeltaLearning) 
   {
      system_command_t cmd = { mSetupDir + "/" + mRunScript, mSpScratchDir };
      auto status = run_process(cmd, Mlog);
      if(status.status != 0) 
      {
         MIDASERROR("RUN FAILED FOR CALCULATION : ", std::to_string(this->CalcNumber()));
      }

      std::string filename_prop_general = mSpScratchDir + "/midasifc.prop_general";
      std::ifstream ifs(filename_prop_general);
      if(!ifs.is_open())
      {
         MIDASERROR("PROPERTY FILE NOT FOUND!!! : '" + filename_prop_general + "'.");
      }

      // Read in property file 
      bool isfound = false;
      std::string line;
      while(std::getline(ifs, line))
      {
         std::string descr;
         Nb value;
         std::istringstream iss(line);
         if(!(iss >> descr >> value))
         {
            MIDASERROR(filename_prop_general +": \n \t Line: "+line+" could not be interpreted as a property line.");
         }
   
         auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(descr);
         prop.SetValue(value);

         prop_descriptors.push_back(descr);

         if (descr == mPropertyDescr) isfound = true; 
         
      }

      if (!isfound)
      {
          MIDASERROR("DID NOT FIND REFERENCE FOR CALCULATION : ", std::to_string(this->CalcNumber()));
      } 

      // Delete property file
      std::remove(filename_prop_general.c_str() );
   }
   else
   {
      prop_descriptors.push_back(mPropertyDescr);
   }

   //--------------------------------------------------------------------------------+
   // External definition of internal coordinates
   //--------------------------------------------------------------------------------+
   std::vector<InternalCoord> icoorddef;

   if (mUseIcoordDef) 
   {
      ifstream fcoord(mIcoordDef);
      if (fcoord.good())
      {
         icoorddef = ReadInternalCoordinates(fcoord); 
         fcoord.close();
      }
      else
      {
         MIDASERROR("Could not determine file with defintion of internal coordinates!");
      }
   }

   //-------------------------------------------------+
   // read in trainings data 
   //-------------------------------------------------+
   GeoDatabase geodb(this->mCoordType, this->mDatabaseFile, mUseIcoordDef, icoorddef);

   std::tuple<std::vector<MLDescriptor<Nb>>,std::vector<Nb>> TrainSet = geodb.Convert2Descriptors();
   std::vector<MLDescriptor<Nb>>  TrainSetX = std::get<0>(TrainSet);

   // Get connectivity
   std::vector<In> na;
   std::vector<In> nb;
   std::vector<In> nc;

   if (this->mCoordType == geo_coordtype::MINT ||
       this->mCoordType == geo_coordtype::DIST ||
       this->mCoordType == geo_coordtype::ZMAT
       )
   {
      geodb.GetConnectivity(na,nb,nc,0);
   }

   //-------------------------------------------------+
   // Gaussian Process
   //-------------------------------------------------+
   GPConstMean<Nb> GPZERO(C_0);
   const bool verbose = false;

   GauPro<Nb> GPR = mMLTask.CreateGPR( geodb
                                     , GPZERO
                                     , mFileHparam
                                     , verbose
                                     , false
                                     , 0.0
                                     ); 

   // Optimize Hyper-parameters or read Co-Variance matrix from file
   std::unique_ptr<Nb[]> Weights;
   if (mHaveWeights)
   {
      GPR.ReadWeights(Weights,mFileWeights);
   }
   else if (mHaveCoVar)
   {
      GPR.ReadCovarianceMatrix(mCoVar);
   }
   else 
   {
      // Optimize Hyper-parameters
      bool verbose = true; 
      ofstream mySPfile;
      mySPfile.open ("SinglePoint.info");
      if (mDoOpt) GPR.OptimizeHparams(verbose,mySPfile,50);
      mySPfile.close();
   }

   // Read in the XYZ file created and convert it to internal coordinates (later avoid writing and reading file)
   std::string file_name = mSpScratchDir + "/midasifc.xyz_input";

   GeoDatabase molecule( this->mCoordType
                       , file_name
                       , na
                       , nb
                       , nc
                       , mUseIcoordDef
                       , icoorddef
                       , C_0
                       );

   std::vector<vector<Nb>> coord = molecule.GetTrajectory();

   // Calculate an averaged variance over the training points 
   Nb meanVar = C_0;
   if (mValidateViaVariance) 
   {
      std::tuple<vector<Nb>, vector<Nb>> gpout = GPR.Predict(TrainSetX); 
      vector<Nb> variance  = std::get<1>(gpout);
      
      for (In idx = 0; idx < variance.size(); idx++)
      {
         meanVar += variance[idx];
      }
      meanVar /= variance.size();
   }

   int npoints = coord.size();
   std::vector<MLDescriptor<Nb>> points;
   points.reserve(npoints);
  
   for (int imol = 0; imol < npoints; imol++)
   {
      MLDescriptor<Nb> point(coord[imol],MLDescriptor<Nb>::SCALAR);
      points.push_back(point);
   }

   int iPredictMode = 0;  // Predict only energy

   // predict energy
   std::tuple<vector<Nb>, vector<Nb>> gpout = GPR.Predict(  points
                                                         ,  iPredictMode
                                                         ,  false
                                                         ,  mHaveWeights
                                                         ,  Weights
                                                         ); 

   vector<Nb> energuess = std::get<0>(gpout);  
   vector<Nb> varguess  = std::get<1>(gpout);  

   bool acceptit = true;
   if (mValidateViaVariance)
   {
      // Now lots of semi empirics let us to this stoping criterion...
      Nb relVar = std::abs(varguess[0] / meanVar);  // Should be positiv anyhow...

      //acceptit = std::log10(relVar) < 4; 
      acceptit = true; // GS change back 
   }

   if (mDoMLPes) acceptit = false;

   if (acceptit)
   {
      //
      // Let Midas know about our estimated property 
      //
      
      // Store ML computed property 
      auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(mPropertyDescr);
      Nb value = C_0;
      if (mIsDeltaLearning)
      {
        value = prop.GetValue(); 
      }
      Nb correction ((std::isnan(energuess[0])) ? C_0 : energuess[0]);

      if (locdbg) Mout << "value " << value << " correction " << correction << " variance " << varguess  << std::endl; 

      value += correction;
      prop.SetValue(value);

      // If requested store variance of ML computed property (for analysis)
      if (mStoreVariance)
      {
         auto& variance = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(mVarianceDescr);
         variance.SetValue(std::abs(varguess[0]));
      }

      //
      // Write Midas property file
      //
      std::string fileprop = mSpScratchDir + "/midasifc.prop_general";
      std::ofstream ofs(fileprop);
      ofs.setf(ios::scientific);
      ofs.precision(22);
      for (auto descr : prop_descriptors)
      {
         auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(descr);
         Nb value = prop.GetValue();
         ofs << std::setw(4) << descr << "   " << value << endl;
      }
      ofs.close();

      // Dump Uncertainty needed for GPR-ADGA for example
      In iprop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyNumberFromDescription(mPropertyDescr);
      this->DumpUncertainty(iprop, varguess[0]);
   }
   else
   {

      // Get current value to be able to calculate delta
      auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription(mPropertyDescr);
      Nb value = C_0;
      if (mIsDeltaLearning)
      {
        value = prop.GetValue();
      }
      if (!mDoMLPes) Mout << "Needed to do exact Single Point! value " << value << std::endl;
     
      // The ML point was not accepted. This means we will do a conventional generic SP
      system_command_t cmd = { mSetupDir + "/" + mFallBackRunScript, mSpScratchDir };
      auto status = run_process(cmd, Mlog);
      if(status.status != 0) 
      {
         MIDASERROR("FALLBACK RUN FAILED FOR CALCULATION : ", std::to_string(this->CalcNumber()));
      }

   }

   // Maybe later we will do something with the coordinates??
   std::string fout = "midasifc.cartrot_xyz";
   CreateXyzFile(fout);
}


//! Mutex for making writing of structures to disc concurrent
std::mutex dump_mutex;

void MLSinglePoint::DumpUncertainty
   (  In iprop
   ,  Nb aSigma2
   ) const
{
   std::lock_guard<std::mutex> dump_lock(dump_mutex);

   std::string sig2_file_name = mSaveIoDir + "/" + "gpr_prop_no_" + std::to_string(iprop) +  ".sig2";
   std::ofstream sig2_file; 
   sig2_file.open(sig2_file_name, std::ofstream::out | std::ofstream::app);
   sig2_file.setf(std::ofstream::scientific);
   sig2_file.precision(20);

   std::ios::fmtflags f( sig2_file.flags() );

   sig2_file << this->CalcCodeString() << " " << aSigma2 << std::endl;

   sig2_file.flags(f);
   sig2_file.close();

}


/**
 * Check that the calculation finished correctly
 **/
void MLSinglePoint::ValidateCalculationImpl
   ( bool& aHalt
   , bool& aSuspicious
   ) const
{
   // At the moment we have no validation
   // TODO Use at least the updates co-variance matrix as check
   aHalt = false;
}

/**
 *  *
 *   **/
void MLSinglePoint::CreateXyzFile
   (
      std::string fcoord
   )  const
{
   // write xyz coordinates file
   std::string file_name = mSpScratchDir + "/" + fcoord;
   std::ofstream ofs(file_name);
   ofs.setf(ios::scientific);
   ofs.precision(SinglePointImpl::mXyzPrecision);
   ofs << mStructure.size() << std::endl << std::endl;
   for(In i = 0; i < mStructure.size(); ++i)
   {
      std::string name = mStructure[i].AtomLabel();
      Nb x = mStructure[i].X() * C_TANG;
      Nb y = mStructure[i].Y() * C_TANG;
      Nb z = mStructure[i].Z() * C_TANG;
      ofs << std::setw(4) << name << "   " << x << "   " << y << "   " << z << endl;
   }
   ofs.close();
}

/**
 *
 **/
void MLSinglePoint::ReadRotatedStructureImpl
   (  std::map<In,In>& aNucMap
   ,  std::vector<Nuclei>& aRotated
   ,  std::vector< pair<string,string> >& aLabelVec
   )  const
{
   // copy file from node to savedir
   std::string file_name  = mSpScratchDir + "/midasifc.cartrot_xyz";
   if(!midas::filesystem::Exists(file_name)) 
   {
      MIDASERROR("For single point: " + std::to_string(mCalcNumber) + "\nDid not find XYZ coordinates for aRotated structure.");
   }
   
   // Read the file
   std::ifstream ifs;
   ifs.open(file_name);

   // in xyz format the first two lines are uninteresting (number of atoms and comment line)
   std::string line;
   std::getline(ifs,line);
   std::getline(ifs,line);

   // map relating nuclei in structures
   In count = 0;
   while(std::getline(ifs,line)) 
   {
      string label;
      Nb x,y,z;
      istringstream iss(line);
      iss >> label >> x >> y >> z;
      
      // convert to AU
      if (mBohr == false)
      {
         x/=C_TANG;
         y/=C_TANG;
         z/=C_TANG;
      }
      
      Nuclei nuc_inp(x,y,z,C_0,label);
      nuc_inp.SetQfromGeneralLabel(label);
      // Q: Isotope number does not matter here, right?
      //    We are just making translation...
      // A: No, if we are dealing with isotopes Midas might not be able to relate the structures 
      //    if not doesn't take care of the right masses. Since the center of mass changes
      In iso = mStructure[count].Anuc();
      nuc_inp.SetAnuc(iso);
      aRotated.push_back(nuc_inp);
      aNucMap.insert(std::make_pair(count,count));
      aLabelVec.push_back(std::make_pair(label,label));
      count++;
   }
}
