/**
************************************************************************
* 
* @file                TinkerSinglePoint.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TinkerSinglePoint datatype 
* 
* Last modifieds:      24-11-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/singlepoint/TinkerSinglePoint.h"

// Standard Headers
#include <string>
#include <vector>

#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "pes/GeneralProp.h"
#include "tinker_interface/TinkerMM.h"

using namespace midas;

/////
// Register for factory
/////
SinglePointRegistration<TinkerSinglePoint> registerTinkerSinglePoint("SP_TINKER");

/**
* Constructor
* */
TinkerSinglePoint::TinkerSinglePoint
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )  
   :  SinglePointImpl(aSpInfo, aPropertyInfo)
{   
   mTinkerMM.SetParamFile(aSpInfo.at("PARAMETERFILE"));
   mTinkerMM.SetTinkerXyzFile(aSpInfo.at("XYZFILE"));
   return;
}

TinkerSinglePoint::~TinkerSinglePoint()
{
   // clean locals
}

void TinkerSinglePoint::CreateInputImpl() const
{
   // use this to initialize structure in TinkerMM
   if(mTinkerMM.GetNatoms()==I_0)
      const_cast<TinkerMM&>(mTinkerMM).Setup(mStructure);
   return;
}

/**
*  Take care of running the calculation
**/
void TinkerSinglePoint::SubmitImpl(bool) const
{
   std::string file_name = mpSpScratchDir + "/midasifc.cartrot";
   std::ofstream ofs(file_name);
   midas::stream::ScopedPrecision(16, ofs);
   ofs.setf(ios::scientific);
   
   // write atoms
   for(In i=0;i<mStructure.size();i++) 
   {
      string name=mStructure[i].AtomLabel();
      Nb x=mStructure[i].X();
      Nb y=mStructure[i].Y();
      Nb z=mStructure[i].Z();
      ofs << setw(4) << name << "   " << x << "   " << y << "   " << z << endl;
   }  
   ofs.close();
   
   // we just update TinkerMM with this structure
   // and retrieve the properties
   const_cast<TinkerMM&>(mTinkerMM).UpdateGeometry(mStructure);
   const_cast<TinkerMM&>(mTinkerMM).Evaluate();
   
   // get energy
   Nb energy = const_cast<TinkerMM&>(mTinkerMM).GetEnergy();
   
   //// properties
   //file_name=gsaveiodir+"/midasifc.propinfo";
   //if(!InquireFile(file_name)) {
   //   ofstream ofc_p(file_name.c_str());
   //   ofc_p << "TENS_ORDER=(0),DESCRIPTOR=(GROUND_STATE_ENERGY)";
   //   ofc_p << endl;
   //   ofc_p.close();
   //}
   // take care of watson additional potential term
   // 
   // IAN: I HAVE COMMETED BELOW OUT.... HANDLED ELSEWHERE I THINK/HOPE!!!???!!!
   //if (gPesCalcDef.GetmPesCalcMuTens()) 
   //{
   //   //      Nb itens_trace=mInertiaMat["MU_LINEAR_INERTIA"];
   //   //std::map<std::string, Nb> InertiaMat=GetInertiaMat();
   //   //Nb itens_trace=InertiaMat["MU_LINEAR_INERTIA"];
   //   //energy-=C_I_8*itens_trace; // FIX!!!
   //}
   // Init general prop
   const_cast<pes::GeneralProp&>(mProperties).Init();
   // add values to property
   auto& prop = const_cast<pes::GeneralProp&>(mProperties).FindPropertyFromDescription("GROUND_STATE_ENERGY");
   //PropertyInfo* p_target_info; // IAN
   prop.SetValue(pot_val);

   //std::promise<threaded_system_return> p;
   //p.set_value(threaded_system_return());
   //mFuture = p.get_future();
}

/**
 *  Check that the calculation finished correctly
**/
void TinkerSinglePoint::ValidateCalculationImpl
   (  bool& aHalt
   ,  bool& aSuspicious
   )  const
{
   return;
}

void TinkerSinglePoint::CleanUpImpl() const
{
   return;
}
