#include "pes/CalculationList.h"

#include "pes/CalcCode.h"
#include "pes/GeneralProp.h"

#ifdef VAR_MPI
#include "mpi/Communicator.h"
#include "mpi/Blob.h"
#include "mpi/Signal.h"
#include "pes/PesMpi.h"
#include "pes/PesInfo.h"

/**
 * MPI master driver for calculation singlepoints.
 * Will divide the batch of singlepoints so all processes in the communicator 
 * will have an equal maount of singplepoints to do.
 * Then it will send out calculation codes to all slave processes.
 * After singlepoints have been calculated master rank will receive all properties from all slaves,
 * and add all calculated singlepoints to the map of calculated singlepoints.
 *
 * @param aCommunicator    Communcator on which to send out singlepoints to do.
 * @param aSinglePointName The SinglePoint definition name.
 * @param aDisplacementGenerator  The generator for molecular displacements.
 * @param aCalculated      The new calculation codes to be done in current batch.
 **/
void CalculationList::RunSinglePointsMaster
   (  const mpi::Communicator& aCommunicator
   ,  const std::string& aSinglePointName
   ,  const PesInfo& aPesInfo
   ,  const pes::DisplacementGenerator& aDisplacementGenerator
   ,  CalculationMap& aNewCalculationMap
   )
{
   // Send signal to slaves
   mpi::SendSignal(aCommunicator, midas::pes::mpi_impl::signal::CALCULATE_SINGLEPOINTS);

   // Send out points
   int nrproc = aCommunicator.GetNrProc();
   int nrpoints_pr_proc = std::floor(aNewCalculationMap.size() / nrproc);
   int overflow = aNewCalculationMap.size() % nrproc;
   auto iter = aNewCalculationMap.begin();
   
   //std::cout << " nrpoints         " << aNewCalculationMap.size() << std::endl;
   //std::cout << " nrpoints_pr_proc " << nrpoints_pr_proc << std::endl;
   //std::cout << " overflow         " << overflow << std::endl;
   //std::cout << " new codes : " << std::endl;
   //for(const auto& new_code : aNewCalculationMap)
   //{
   //   std::cout << "    " << new_code.second->mCalcCode << std::endl;
   //}

   for(int iproc = 1; iproc < nrproc; ++iproc)
   {
      int nrpoints = nrpoints_pr_proc + (overflow ? 1 : 0);
      if(overflow)
      {
         --overflow;
      }
      
      mpi::Blob blob(1024);
      blob.Load(aSinglePointName);
      blob.Load(reinterpret_cast<char*>(&nrpoints), sizeof(nrpoints));

      for(int ipoint = 0; ipoint < nrpoints; ++ipoint)
      {
         if(iter == aNewCalculationMap.end())
         {
            MIDASERROR("Something went wrong :C");
         }
         
         // load the calculation code into the blob
         std::string str = iter->second->mCalcCode.GetString();
         int calcnumber = iter->second->mCalculationNumber;
         blob.Load(str);
         blob.Load(reinterpret_cast<char*>(&calcnumber), sizeof(calcnumber));
         
         ++iter;
      }
      
      // Send the blob 
      //blob.Isend(aCommunicator, iproc, 100);
      blob.Send(aCommunicator, iproc, 100);
   }

   // Run rest of singlepoints on master
   CalculationMap list_of_calcs;
   for(; iter != aNewCalculationMap.end(); ++iter)
   {
      list_of_calcs.Insert(iter->second);
   }
   RunSinglePoints(aSinglePointName, aPesInfo, aDisplacementGenerator, list_of_calcs);

   // Receive back properties from all slaves
   for(int iproc = 1; iproc <nrproc; ++iproc)
   {
      mpi::Blob blob_recv;
      blob_recv.Recv(aCommunicator, iproc, 101);
      int map_size;
      blob_recv.Unload(reinterpret_cast<char*>(&map_size), sizeof(map_size));
      
      //std::cout << " MAP RECIEVE SIZE " << map_size << std::endl;
      for(int imap = 0; imap < map_size; ++imap)
      {
         std::string calccode_recv;
         In calcnumber;
         blob_recv.Unload(calccode_recv);
         blob_recv.Unload(reinterpret_cast<char*>(&calcnumber), sizeof(calcnumber));
         GeneralProp general_prop_recv(blob_recv);
         
         //std::cout << " RECIEVED " << general_prop_recv << std::endl;
         
         auto& iter_point = aNewCalculationMap.GetCalculationProperty(calcnumber);
         iter_point.mProperties = general_prop_recv;
      }
   }
}

/**
 * MPI slavedriver for running singlepoint calculations.
 * Will receive calculation codes from master (rank 0),
 * then calculate the singlepoints in an asynchronous fashion using threads,
 * and afterwards send back energy and properties 
 * to master node for further processing.
 *
 * @param aCommunictor   The communicator on which the singlepoints are sent out.
 * @param aMolecule      The molecule with reference geometri.
 * @param aPesInfo       The pes information.
 **/
void CalculationList::RunSinglePointsSlave
   (  const mpi::Communicator& aCommunicator
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const PesInfo& aPesInfo
   )
{
   //std::cout << " CALCULATION LIST SLAVE " << std::endl;
   // Receive points
   mpi::Blob blob_recv;

   // receive list of calc codes to do
   blob_recv.Recv(aCommunicator, 0, 100);

   std::string singlepointname;
   blob_recv.Unload(singlepointname);
   
   int number_of_points;
   blob_recv.Unload(reinterpret_cast<char*>(&number_of_points), sizeof(number_of_points));
   CalculationMap list_of_calcs;
   
   if constexpr (MPI_PES_DEBUG)
   {
      std::cout   << " RANK " << aCommunicator.GetRank()
                  << " NUMBER OF POINTS " << " : " << number_of_points << std::endl;
   }

   //
   for (int ipoint = 0; ipoint < number_of_points; ++ipoint)
   {
      std::string str;
      int calcnumber;
      blob_recv.Unload(str);
      blob_recv.Unload(reinterpret_cast<char*>(&calcnumber), sizeof(calcnumber));
      
      std::shared_ptr<CalculationProperty> prop(new CalculationProperty(str, calcnumber, aPesInfo.GetMultiLevelInfo().PropInfo()));
      list_of_calcs.Insert(prop);
   }

   // Run single points
   //std::cout << " RUNNING SINGLEPOINTS SLAVE " << std::endl;
   DisplacementGenerator displacement_generator(gPesCalcDef, aPesInfo, aMolecule);
   RunSinglePoints(singlepointname, aPesInfo, displacement_generator, list_of_calcs);
   
   // Send back properties
   //std::cout << " SENDING BACK PROPERTIES " << std::endl;
   mpi::Blob blob(1024 * 1024); // preload 1 mb

   int map_size = list_of_calcs.size();
   blob.Load(reinterpret_cast<char*>(&map_size), sizeof(map_size));

   //std::cout << " LOOPING OVER PROPERTIES SLAVE " << map_size << std::endl;
   for(const auto& calccode_prop_pair : list_of_calcs)
   {
      //std::cout << " SENDING :" << calccode_prop_pair.second->mProperties << std::endl;
      blob.Load(calccode_prop_pair.first.GetString());
      blob.Load(reinterpret_cast<char*>(&calccode_prop_pair.second->mCalculationNumber), sizeof(calccode_prop_pair.second->mCalculationNumber));
      calccode_prop_pair.second->mProperties.LoadIntoMpiBlob(blob);
   }

   blob.Send(aCommunicator, 0, 101);
}

#endif /* VAR_MPI */
