/**
************************************************************************
* 
* @file                Surface.cc
*
* Created:             10-08-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Base class for the surfaces with some common 
*                        functionalities included
* 
* Last modified: Thu Jul 15, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <fstream>
#include <set>
#include <algorithm>
#include <chrono>
#include <thread>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "input/MidasOperatorReader.h"
#include "input/MidasOperatorWriter.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/FileSystem.h"
#include "util/Os.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasMatrix.h"
#include "pes/Surface.h"
#include "pes/AdgaSurface.h"
#include "pes/GridSurface.h"
#include "pes/GridType.h"
#include "pes/ModeCoupling.h"
#include "pes/CutAna.h"
#include "pes/PesInfo.h"
#include "pes/Derivatives.h"
#include "pes/PesFuncs.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/kinetic/Inertia.h"
#include "mpi/Interface.h"

// using declarations
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;

namespace midas
{
namespace pes
{

/**
 *
 **/
Surface* Surface::Factory
   (  const bool& aIsPesAdga
   ,  std::vector<PesInfo>& aPesInfos
   ,  const std::string& aPesMainDir
   )
{
   // Generate a new grid-based surface, either by means of a static grid or using the Adga
   if (aIsPesAdga)
   {
      return new AdgaSurface(aPesInfos, aPesMainDir);
   }
   else
   {
      return new GridSurface(aPesInfos, aPesMainDir);
   }

   MIDASERROR("Should not get here!");
   return NULL;
}

/**
 *
 **/
Surface::Surface
   (  std::vector<PesInfo>& aPesInfos
   ,  const std::string& aPesMainDir
   ) 
   :  mPesInfos(aPesInfos)
   ,  mPesMainDir(aPesMainDir)
   ,  mNoSubsystems(aPesInfos.size())
{
   // Calculate moment of inertia tensor and Coriolis coupling matrices for reference structure
   this->CalcMomentOfInertiaTensor();
   this->CalcCoriolisMatrices();
  
   // Initialize mBarFiles member variable
   mBarFiles.reserve(aPesInfos.size());
   
   // Initialize mCalculationLists and mDoPotentialFits member variables
   mCalculationLists.reserve(aPesInfos.size());
   mDoPotentialFits.reserve(aPesInfos.size());
   for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
   {
      mCalculationLists.emplace_back(CalculationList(aPesInfos[isystem].GetMolecule(), aPesInfos[isystem]));
      mDoPotentialFits.emplace_back(DoPotentialFit(gFitBasisCalcDef, gPesCalcDef, aPesInfos[isystem]));
   }
}

/**
 *
 **/
Surface::~Surface() 
{
   for (auto bar_file : mBarFiles)
   {
      if (bar_file != nullptr)
      {
         delete bar_file;
      }
   }
}

/**
 * Merge the different multilevels of the surface calculation and collect the final surface and other relevant files in the FinalSurfaces directory.
 **/
void Surface::FinalizeMultilevelSurface
   (
   )  const
{
   if (gPesIoLevel > I_4)
   {
      Mout << " Multilevel Procedure: Parsing the mop files" << std::endl;
   }

   // Define the lowest multilevel to be considered when finalizing, (normaliy we do not want to include the level_0 as this is the boundaries pre-optimization level)
   auto lowest_multilevel = I_1;
   auto no_multilevels = gPesCalcDef.MultiLevel();
   if (  gPesCalcDef.GetmDincrSurface()
      && gPesCalcDef.BoundariesPreOpt()
      && (mPesInfos.front().GetMultiLevelInfo().CurrentLevel() == I_0)
      )
   {
      lowest_multilevel = I_0;
      no_multilevels = I_1;
   }

   // Do this for all subsystems
   for (auto& pes_info : mPesInfos)
   {
      // FinalSurfaces is where the files for the final multilevel surface is stored for subsequent analysis and calculations
      auto final_level = pes_info.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(no_multilevels - I_1) + "/";
     
      if (gPesCalcDef.GetmPesStatic() || gPesCalcDef.GetmPesAdga())
      {
         // Copy the file containing potential boundaries to FinalSurfaces
         midas::filesystem::Copy
            (  final_level + "analysis/one_mode_grids.mbounds"
            ,  pes_info.GetmSubSystemDir() + "/FinalSurfaces/analysis/one_mode_grids.mbounds"
            );
     
         // Checking that the potential boundaries are the same
         if (  (gPesCalcDef.BoundariesPreOpt() && no_multilevels > I_2)
               ||
               (!gPesCalcDef.BoundariesPreOpt() && no_multilevels > I_1)
            )
         {
            CheckMultiLevelPesBounds(pes_info);
         }
 
         // If working with polyspherical coordinates, then copy the files containing the kinetic energy operator
         if (gPesCalcDef.GetmPscInPes())
         {
            for (In i = I_0; i < gOperatorDefs.GetNrOfOpers(); i++)
            {
               auto psc_oper_file_name = gOperatorDefs[i].GetKeoOperFileName();
               if (!psc_oper_file_name.empty())
               {
                  midas::filesystem::Copy
                     (  psc_oper_file_name
                     ,  pes_info.GetmSubSystemDir() + "/FinalSurfaces/savedir/keo_psc.mop"
                     );
               }
            }
         }
      }
 
      // Merge the reference value files from the different multilevels
      if (lowest_multilevel > I_0)
      {
         MergeReferenceValues(no_multilevels, pes_info);
      }
 
      // Remove all .mop files that might be in the way for the new merged ones.
      RmFile(pes_info.GetMultiLevelInfo().SaveIoDir() + "/*.mop");
 
      // This will keep track of the electronic state to which the property belong
      std::vector<std::string> states_for_merge;
      
      // This will keep track of the file names for individual property files
      std::vector<std::string> files_for_merge;
 
      // This will keep track of the unique properties calculated across the levels
      //std::set<std::string> props_for_merge; 
      std::vector<std::string> props_for_merge; 
     
      // This keep track of each levels properties
      std::vector<std::vector<std::string> > levels_props_matrix; 
      
      // Loop over available multilevel calculations
      for (In ilevel = lowest_multilevel; ilevel < no_multilevels; ++ilevel)
      {
         levels_props_matrix.emplace_back();
         std::string name_label = pes_info.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(ilevel) + "/savedir/prop_files.mpesinfo";
         std::ifstream calculated_properties(name_label.c_str(), std::ios_base::in);
         
         // Ensure file exists
         if (!calculated_properties)
         {
            MIDASERROR(" Error when trying to open file: " + name_label);
         }
 
         std::string ss;
         while (std::getline(calculated_properties, ss))
         {
            // Check for comment line
            std::size_t found_comment = ss.find("#");
            if (found_comment != std::string::npos)
            {
               continue;
            }
 
            In istate;
            std::string file_name;
            std::string property_name;
 
            std::istringstream line_s(ss);
            line_s >> istate >> file_name >> property_name;
            states_for_merge.push_back(std::to_string(istate));
            files_for_merge.push_back(file_name);
            auto props_iter = std::find(props_for_merge.begin(), props_for_merge.end(), property_name);
            if(props_iter == props_for_merge.end())
            {
               props_for_merge.emplace_back(property_name);
            }
            //props_for_merge.insert(property_name);
            levels_props_matrix.back().emplace_back(property_name);
         }
      
         // Close the PropertyInfo file again
         calculated_properties.close();
      }
 
      // Set the total number of properties present for this multilevel surface
      auto no_merge_props = props_for_merge.size();
      pes_info.SetmNoSurfaceProps(no_merge_props);
      
      // Print the number of properties that will be merged for this multilevel calculation
      if (gPesIoLevel > I_10)
      {
         if (no_merge_props == I_1)
         {
            Mout << " Will consider " << no_merge_props << " property for merge: " << std::endl;
         }
         else
         {
            Mout << " Will consider " << no_merge_props << " properties for merge: " << std::endl;
         }
 
         for (auto it = props_for_merge.begin(); it != props_for_merge.end(); ++it)
         {
            Mout << "  " << *it << std::endl;
         }
      }
 
      // Create a new "merged" PropertyInfo file to go along with the merged property files
      std::ofstream property_info_file;
      auto property_info_file_name = pes_info.GetmSubSystemDir() + "/FinalSurfaces/savedir/prop_files.mpesinfo";
      property_info_file.open(property_info_file_name.c_str(), ios_base::out);
      property_info_file << "# Electronic State  Property File  Property Label" << std::endl; 
 
      In level_present = I_0;
      In new_prop_no = I_0;
      // Loop over all properties found and all levels to gather all information of the surfaces
      for (auto it = props_for_merge.begin(); it != props_for_merge.end(); ++it)
      {
         // Note that we below add one (permanently) to the new_prop_no before printing it
         ++new_prop_no;
 
         // Write the property identifiers
         property_info_file << states_for_merge[new_prop_no - I_1]
                            << "  " << files_for_merge[new_prop_no - I_1]
                            << "  " << *it
                            << std::endl;
 
         std::vector<MidasOperatorReader> read_opers;
         // Loop over available multilevel calculations
         for (In ilevel = lowest_multilevel; ilevel < no_multilevels; ++ilevel)
         {
            // NB: we subtract 1 from ilevel as the zeroth level is not in levels_prop_matrix so level 1 is index 0
            auto jlevel = ilevel;
            if (lowest_multilevel == I_0)
            {
               jlevel += I_1;
            }
            
            auto prop_poss_it = std::find(levels_props_matrix[jlevel - 1].begin(), levels_props_matrix[jlevel - 1].end(), *it);
            
            // Could not find the property for this level, skip it
            if (prop_poss_it == levels_props_matrix[jlevel - 1].end())
            {
               continue;
            }
 
            // The plus one is to have it match the actual file numbering
            In prop_no = prop_poss_it - levels_props_matrix[jlevel - 1].begin() + 1;
 
            if (gPesIoLevel > I_12)
            {
               Mout << " Reading property: " << *it << " from file: " << std::endl
                    << "  " << pes_info.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(ilevel) + "/savedir/prop_no_" + std::to_string(prop_no) + ".mop" << std::endl;
            }
            
            // Read the operators
            std::string oper_path = pes_info.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(ilevel) + "/savedir/prop_no_" + std::to_string(prop_no) + ".mop";
            read_opers.emplace_back(oper_path);
            read_opers.back().ReadOperator(oper_path);
         }
         
         // make operator write and write operator
         MidasOperatorWriter merged_oper(pes_info.GetmSubSystemDir() + "/FinalSurfaces/savedir/prop_no_" + std::to_string(new_prop_no) + ".mop");
         merged_oper.FromReadPotentials(read_opers);
         
         if (gPesIoLevel > I_12)
         {
            Mout << " Merge completed for operator: " << *it << std::endl;
         }
      }
 
      // Close the PropertyInfo files after writing
      property_info_file.close();
      
      // Merge list of files to plot
      if (gPesCalcDef.GetmPesPlotAll() || gPesCalcDef.GetmPesPlotSelected())
      {
         MergeListofPlots(pes_info);
      }
   }
}

/**
 * Merge list of plot files
 **/
void Surface::MergeListofPlots
   (  const PesInfo& aPesInfo
   )  const
{
   // open the file to hold merged list
   std::string list_of_plots_name = aPesInfo.GetMultiLevelInfo().AnalysisDir()+"/ListofPlots";
   std::ofstream list_of_plots(list_of_plots_name);

   for (In ilevel = I_1; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
   {
      // open list for current level
      std::string list_of_plots_level_name = aPesInfo.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(ilevel) + "/analysis/ListofPlots";
      std::ifstream list_of_plots_level(list_of_plots_level_name);
         
      if(!list_of_plots_level) continue; // continue for
      
      // read each line and put into merged file
      std::string line;
      while(getline(list_of_plots_level,line))
      {
         list_of_plots << line << "\n";
      }
      
      // close file for current level
      list_of_plots_level.close();
   }
   
   // close merged file
   list_of_plots.close();
}

/**
 * Setup GridType and modify the mode combination range if required.
 *
 * @param aPesInfo (INPUT) Information on the surface to be constructed.
 * @param arIMode (INPUT) Mode to run for grid setup (0 := do some more output, for first call to GridSetup)
 * @param ModeCom (INPUT/OUTPUT) mode combination range, on exit also contains MC's for extended grid settings
 * @param arActualMaxDimMc (INPUT/OUTPUT) max dim of MCR, on output contains the max for also extended grid settings
 * @param arGpFr (OUTPUT) on output contains grid types for all MC's
 **/
void Surface::GridSetup
   (  const PesInfo& aPesInfo
   ,  const In& arIMode
   ,  ModeCombiOpRange& ModeCom
   ,  In& arActualMaxDimMc
   ,  GridType& arGpFr
   )
{
  
   PesFuncs::GridSetup( arIMode
                      , ModeCom
                      , arActualMaxDimMc
                      , arGpFr
                      , aPesInfo
                      );
}

/**
 *
 **/
void Surface::MakeCutAna
   (  const PesInfo& aPesInfo
   )  const
{
   CutAna* mCuts = new CutAna[gPesCalcDef.GetmMakeCutAnalysis().size()] ;
   InitiateCutAnalysis(mCuts) ;

   std::vector<Nb> mFreq;
   std::string Freq_File = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "harmonic_frequencies.mpesinfo";
   if (!gPesCalcDef.GetmCartesiancoordInPes())
   {
      ifstream file(Freq_File.c_str(),ios_base::out);
      if (!file)
      {
         Mout << " Unable to open file " << Freq_File << endl;
         MIDASERROR(" Error when trying to open file ");
      }
      file.setf(ios::scientific);
      file.setf(ios::uppercase);
      file.precision(I_22);

      std::vector<Nb> mFreq;
      std::string Freq_File = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "harmonic_frequencies.mpesinfo";
      if (!gPesCalcDef.GetmCartesiancoordInPes())
      {
         ifstream file(Freq_File.c_str(),ios_base::out);
         if (!file)
         {
            Mout << " Unable to open file " << Freq_File << endl;
            MIDASERROR(" Error when trying to open file ");
         }
         file.setf(ios::scientific);
         file.setf(ios::uppercase);
         file.precision(I_22);
      }
      string ss;
      while(!file.eof())
      {
         In Mode;
         Nb Freq;
         getline(file,ss);
         istringstream line_s(ss);
         line_s >> Mode >> Freq;
         mFreq.push_back(Freq);
      }
      file.close();
   }
   for (In i= I_0 ; i<gPesCalcDef.GetmMakeCutAnalysis().size() ; i++)
   {
      mCuts[i].PrepareCutFile(mFreq, aPesInfo);
   }
}


/**
 * Sets the derivative Container for later extrapolation
 * Read information from file "ListOfCalcs_Done" to retrieve the derivative
 * information from each file. Also sets the address for the derivative in 
 * N.B. SHOULD ADD HERE A CONDITION SO THAT ONLY STORES WHAT IS NEEDED FOR
 * THE RUN, E.G. WHEN PERFORMING A LOWER-CUT EXTRAPOLATION 2->3 AND DERIVATIVES
 * FROM 3-MC POINTS ARE AVAILABLE, BUT OF COURSE THEY ARE NOT NEEDED.
 * the cointaner.
 **/
void Surface::FillDerContainer
   (  Derivatives& arDerContainer
   ,  const CalculationList& arCalculationList
   ,  const PesInfo& aPesInfo
   )  const
{
   // Properties for which derivative information is available
   const auto& props_with_der_info = aPesInfo.GetmPropWithDer();

   // Loop over all properties with available derivative information
   for (In prop = I_0; prop < props_with_der_info.size(); prop++)
   {
      In order = I_0;
      std::string property = props_with_der_info[prop];
      std::vector<Nb> der_cont;
      std::map<In, In> der_cont_handler;

      // Setting a map between the Number of Calc and the Code of Calc
      In prop_no = aPesInfo.GetMultiLevelInfo().GetPropertyNumber(property);
      std::string der_file_name = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(prop_no) + "_derivatives.mbinary";
      std::string der_addr = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(prop_no) + "_derivatives_navigation.mpesinfo";
      
      std::map<std::string, MidasVector> grad_map;
      std::map<std::string, MidasMatrix> hess_map;
      auto no_vibs = aPesInfo.GetMolecule().GetNoOfVibs();
     
      // save everything in a general container
      bool save_in_cont = true;
      
      PesFuncs::ReadDerInfo
         ( grad_map
         , hess_map
         , der_cont 
         , der_cont_handler
         , order 
         , save_in_cont
         , der_addr
         , der_file_name
         , no_vibs
         );

      arDerContainer.SetDerivatives(property, order, der_cont, der_cont_handler);
   }

   if (gDebug || gPesIoLevel > I_14)
   {
      Mout << arDerContainer << std::endl;
   }
}

/**
 * Checks that the potential boundaries are the same for all multilevel calculations
**/
void Surface::CheckMultiLevelPesBounds
   (  const PesInfo& aPesInfo
   )  const
{
   // First we need to get one_mode_grids.mbounds information from all multilevels
   std::vector<std::vector<std::tuple<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb>>> pes_grid_bounds; 
   pes_grid_bounds.assign(gPesCalcDef.MultiLevel(), std::vector<std::tuple<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb>>());
   for (In mlevel = I_1; mlevel < gPesCalcDef.MultiLevel(); ++mlevel)
   {
      std::string grid_bounds_filename = aPesInfo.GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(mlevel) + "/analysis/one_mode_grids.mbounds";
      if (!InquireFile(grid_bounds_filename))
      {
         MIDASERROR(" I was unable to find to find the grid bounds in the file: " + grid_bounds_filename);
      }

      std::istringstream sstream = midas::mpi::FileToStringStream(grid_bounds_filename);
      
      std::string s;
      while (std::getline(sstream, s))
      {
         // The first line of the one_mode_grids.mbounds file is a comment line that we need to skip
         std::size_t found_comment = s.find("#");
         if (found_comment != std::string::npos) 
         {
            continue;
         }
         auto string_vec = midas::util::StringVectorFromString(s);

         if (string_vec.size() < 3)
         {
            MIDASERROR("Too few columns in one_mode_grids.mbounds file");
         }
         else if (string_vec.size() < 9)
         {
            for (In i = string_vec.size(); i < 9; ++i)
            {
               string_vec.emplace_back("0.0");
            }
         }
         
         pes_grid_bounds[mlevel].push_back(midas::util::TupleFromStringVec<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb>(string_vec));
      }
   }

   // Now comes the checking
   for (In mlevel = I_1; mlevel < gPesCalcDef.MultiLevel() - I_1; ++mlevel)
   {
      for (In mbound = I_0; mbound < pes_grid_bounds[mlevel].size(); ++mbound)
      {  
         // Check left bound
         if (std::abs(std::get<I_1>(pes_grid_bounds[mlevel][mbound]) - std::get<I_1>(pes_grid_bounds[mlevel + I_1][mbound])) > C_I_10_8)
         {
            MidasWarning("The left potential boundary for mode Q" + std::get<I_0>(pes_grid_bounds[mlevel][mbound]) + " is not set to the same value for all multilevels!");
         }
         
         // Check right bound
         if (std::abs(std::get<I_2>(pes_grid_bounds[mlevel][mbound]) - std::get<I_2>(pes_grid_bounds[mlevel + I_1][mbound])) > C_I_10_8)
         {
            MidasWarning("The right potential boundary for mode Q" + std::get<I_0>(pes_grid_bounds[mlevel][mbound]) + " is not set to the same value for all multilevels!");

         }
      }
   }
}

/**
 * Merge the prop_ref_values.mpesinfo files from the different multilevels
**/
void Surface::MergeReferenceValues
   (  const In& aNoMultilevels 
   ,  const PesInfo& aPesInfo
   )  const
{
   if (aNoMultilevels == I_2)
   {
      midas::filesystem::Copy
         (  aPesInfo.GetmSubSystemDir() + "/Multilevels/level_1/savedir/prop_ref_values.mpesinfo"
         ,  aPesInfo.GetmSubSystemDir() + "/FinalSurfaces/savedir/prop_ref_values.mpesinfo"
         );
   }
   else if (aNoMultilevels > I_2)
   {
      Mout << " More than one multilevel detected! Make sure to chose the correct prop_ref_values.mpesinfo file and store this for later manually" << std::endl;
   }
   else
   {
      MIDASERROR("Something is wrong in the multilevel setup");
   }
}

/**
 * Calculate Coriolis coupling matrices and output to file
**/
void Surface::CalcCoriolisMatrices
   (
   )  const
{
   // Check that this is required
   if (  (gPesCalcDef.GetmPesCalcMuTens() || gPesCalcDef.GetmPesCalcCoriolisAndInertia())
      && !gPesCalcDef.GetmPscInPes()
      )
   {
      // Do this for all subsystems
      for (auto& pes_info : mPesInfos)
      {
         // Coriolis matrices file will be stored in the FinalSurfaces directory
         auto coriolis_file_name = pes_info.GetmSubSystemDir() + "/FinalSurfaces/savedir/coriolis_matrices.mpesinfo";

         if (gPesIoLevel > I_5)
         {
            Mout << " Coriolis matrices will be written to file: " << std::endl;
            Mout << "  " << coriolis_file_name << std::endl;
         }

         midas::molecule::DumpCoriolis(pes_info.GetMolecule(), coriolis_file_name, pes_info.GetMolecule().GetModeNames());
      }
   }
}

/**
 * Calculate moment of inertia tensor and output to file
 **/
void Surface::CalcMomentOfInertiaTensor
   (
   )  const
{
   // Check that this is required
   if (gPesCalcDef.GetmPesCalcMuTens() || gPesCalcDef.GetmPesCalcCoriolisAndInertia())
   {
      // Do this for all subsystems
      for (auto& pes_info : mPesInfos)
      {
         std::vector<Nuclei> ref_struct = pes_info.GetMolecule().GetCoord();
         Inertia inertia_tensor(pes_info.GetMolecule().IsLinear());
         inertia_tensor.CalcImat(ref_struct);
         
         // Inertia tensor file will be stored in the FinalSurfaces directory
         auto inertia_file_name = pes_info.GetmSubSystemDir() + "/FinalSurfaces/savedir/inertia_tensor.mpesinfo";
       
         if (gPesIoLevel > I_5)
         {
            Mout << " Moment of inertia tensor (for reference geometry) will be written to file: " << std::endl;
            Mout << "  " << inertia_file_name << std::endl;
         }
       
         inertia_tensor.DumpInertia(inertia_file_name);
      }
   }
}

} /* namespace pes */
} /* namespace midas */
