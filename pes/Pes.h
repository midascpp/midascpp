/**
************************************************************************
* 
* @file                Pes.h
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store pes declarations. 
* 
* Last modified: Mon Oct 30, 2006  09:27AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PES_H_INCLUDED
#define PES_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/doubleincremental/DincrInfo.h"

class PesCalcDef;
class PesInfo;

namespace midas 
{
namespace molecule
{
   class MoleculeInfo;
}
namespace pes 
{

//! Driver for running fragment multilevel surface calculations.
void PesDrv();

//! Run multilevel fragment surface calculation.
void Pes(PesCalcDef& aPesCalcDef, std::vector<PesInfo>& aPesInfos, const std::string& aPesMainDir);

} /* namespace pes */
} /* namespace midas */

#endif /* PES_H_INCLUDED */
