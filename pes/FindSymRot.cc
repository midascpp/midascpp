#include "pes/FindSymRot.h"

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <utility> // for std::pair

#include "util/Error.h"
#include "util/MidasStream.h"

using std::vector;
using std::map;
using std::string;
using std::endl;
using std::pair;

extern MidasStream Mout;

/**
 * Find rotation matrix between 2 geometries
 **/
bool FindRotationMatrix
   (  vector<Nuclei>& orig
   ,  vector<Nuclei>& rot_str
   ,  map<In,In>& molmol
   ,  MidasMatrix& rot_mat
   ,  vector<pair<string,string> >& label_rot_orig
   ,  bool mayIinvert
   ,  const Nb aSymThr
   ,  const In aIoLevel
   )
{
   In maxcycle=1000;
   vector<Nuclei> ref_str;
   vector<Nuclei> rot;
   Vector3D orig_com,rot_com;
   FindCenterOfMass(orig,orig_com);
   FindCenterOfMass(rot_str,rot_com);
   orig_com=C_M_1*orig_com;
   rot_com=C_M_1*rot_com;

   for(In i = I_0; i < orig.size(); i++) 
   {
      orig[i].Shift(orig_com);
      rot_str[i].Shift(rot_com);
   }
   // Make sure that the atoms are in the same order in both!
   // In order to do this we must break any symmetry in the molecule
   // Add a dummy atom
   MidasMatrix dist1(orig.size());
   MidasMatrix dist2(orig.size());
   for(In i=0;i<orig.size();i++) {
      for(In j=0;j<orig.size();j++) {
         dist1[i][j]=orig[i].Distance(orig[j]);
         dist2[i][j]=rot_str[i].Distance(rot_str[j]);
      }
   }
   
   if(gDebug)
   {
      Mout <<endl  <<endl <<endl;
      Mout <<" Original"<< endl;
      for(In i=0;i<rot_str.size();i++) 
         Mout << orig[i] << endl;
      
      Mout <<" Rotated"<< endl;
      for(In i=0;i<rot_str.size();i++)
         Mout << rot_str[i] << endl;
      
   }

   
   bool success = false;
   In no_of_attempts = I_0;
   
   do
   {
      ref_str = orig ;
      rot = rot_str;
      no_of_attempts++;
      //Mout << " Entering the new map constructor " <<  ref_str.size() << endl<<  endl;
      molmol.clear();
      label_rot_orig.clear();
      


      // first we try to impose the 0-0 1-1 2-2 mapping 
      if (no_of_attempts == I_1)
      {
         for(In i=0;i<ref_str.size();i++) {
            if(gDebug)  Mout << "Now inserting (" << i << "," << i << ") pair " << endl;
            molmol.insert(std::make_pair(i,i));
            label_rot_orig.push_back(std::make_pair(rot[i].AtomLabel(), orig[i].AtomLabel()));
         }
      }
      else
      {
         for(In i=0;i<ref_str.size();i++) {
            // dist COM to atom1
            vector<Nb> inter_dist_ref;
            if (i==I_0)
            {
               vector<In> found_deg;
               //Mout << " Searching for atom: "<< i  <<endl;
               for(In j=0;j<ref_str.size();j++) {
                  if(i==j) continue;
                  inter_dist_ref.push_back(dist1[i][j]);
               }
               sort(inter_dist_ref.begin(),inter_dist_ref.end());
               bool found = false;
               // find the corresponding atom in the other molecule
               
               for(In j=0;j<ref_str.size();j++) 
               {
                  //if(ref_str[i].AtomLabel()!=rot[j].AtomLabel())
                     //continue;
                  vector<Nb> inter_dist_rot;
                  for(In k=0;k<ref_str.size();k++) {
                     if(j==k) continue;
                     inter_dist_rot.push_back(dist2[j][k]);
                  }
                  sort(inter_dist_rot.begin(),inter_dist_rot.end());
                  //Mout << endl;
                  bool found_atom=true;
                  for(In k=0;k<ref_str.size()-1;k++) {
                     //Mout << " distance " << fabs(inter_dist_ref[k]-inter_dist_rot[k]) << endl;
                     if(fabs(inter_dist_ref[k]-inter_dist_rot[k]) > aSymThr) {
                        found_atom=false;
                        break;
                     }
                  }
                  if(found_atom) {
                     found_deg.push_back(j);
                     //Mout << "   I found: ";
                     //Mout << " " << j;
                     found = true;
                  }
               }
               //Mout <<  endl; 
               if (!found) 
               {
                  return false; // return
               }
               else
               {
                  //Mout << "  Among the " << found_deg.size() ;
                  In random_integer = I_0;
                  random_integer = rand()%found_deg.size();
                  In pick = found_deg[random_integer];
                  //Mout << ", I pick one: " << pick << endl;
                  
                  if(gDebug)  Mout << "Now inserting (" << pick << "," << i << ") pair " << endl;
                  molmol.insert(std::make_pair(pick,i));
                  label_rot_orig.push_back(std::make_pair(rot[pick].AtomLabel(), orig[i].AtomLabel()));
               }
               //Mout <<  endl;
            }
            else
            {
               //Mout << " Searching for atom: "<< i <<endl;
               for(map<In,In>::const_iterator it = molmol.begin(); it != molmol.end(); ++it)
               {
                  inter_dist_ref.push_back(dist1[i][it->second]);
                  //Mout << " original: distance between " << i <<" and " << it->second << " "  << dist1[i][it->second] << endl;
               }
               //Mout <<  endl;
               
               bool found = false;
               vector<In> found_deg;
               // find the corresponding atom in the other molecule
         
               //Mout << "    I found: ";
               for(In j=0;j<ref_str.size();j++) 
               {
                  vector<Nb> inter_dist_rot;
                  for(map<In,In>::const_iterator it = molmol.begin(); it != molmol.end(); ++it) 
                  {
                     inter_dist_rot.push_back(dist2[j][it->first]);
                     //Mout << " rotated: distance  between " << j <<" and " << it->first << " "  << dist2[j][it->first] << endl;
                  }
                  //Mout <<  endl;
                  bool found_atom=true;
         
                  for(In k=0;k<inter_dist_rot.size();k++) {
                     //Mout << " distance " << fabs(inter_dist_ref[k]-inter_dist_rot[k]) << endl;
                     if(fabs(inter_dist_ref[k]-inter_dist_rot[k]) > aSymThr) 
                     {
                        found_atom=false;
                        break;
                     }
                  }
                  if(found_atom) {
                     found_deg.push_back(j);
                     //Mout << " " << j;
                     found = true;
                  }
               }
               //Mout <<  endl; 
               if (!found) 
               {
                  //Mout << " For structure: " << aCalcName << " atom n. "<< i << endl;
                  success = false;
                  //Mout << " I do not find the corrispondence between the original and rotated structures" <<  endl;
               }
               else
               {
                  //Mout << "  Among the " << found_deg.size() ;
                  In random_integer = I_0;
                  random_integer = rand()%found_deg.size();
                  In pick = found_deg[random_integer];
                  //Mout << ", I pick one: " << pick << endl;
                  
                  if(gDebug)  Mout << "Now inserting (" << pick << "," << i << ") pair " << endl;
                  molmol.insert(std::make_pair(pick,i));
                  label_rot_orig.push_back(std::make_pair(rot[pick].AtomLabel(), orig[i].AtomLabel()));
               }
               //Mout <<  endl;
            }
         }
      }

      //Mout << " Thr mapping is done" << endl; 

      vector<Nuclei> mol3(ref_str.size());
      if(molmol.size()==ref_str.size())
      {
         // Loop through molmol to reorder atoms
         for(map<In,In>::iterator i=molmol.begin();i!=molmol.end();i++) {
            if(gDebug)  
               Mout << "\tCOPY rot[" << i->first << "] TO mol3[" << i->second << "]" << endl;
            mol3[i->second]=rot[i->first];
         }
         rot=mol3;
      }
      // Check if linear or planar (just planar for now...)
      Vector3D v1;
      Vector3D v2;
      Vector3D normal;
      bool is_linear=true;
      In base_atom=I_0;
      //Mout << "Checking for linearity..." << endl;
      for(In i=0;i<ref_str.size()-2;i++) {
         v1.SetX(ref_str[i].X()-ref_str[i+1].X());
         v1.SetY(ref_str[i].Y()-ref_str[i+1].Y());
         v1.SetZ(ref_str[i].Z()-ref_str[i+1].Z());
         v2.SetX(ref_str[i].X()-ref_str[i+2].X());
         v2.SetY(ref_str[i].Y()-ref_str[i+2].Y());
         v2.SetZ(ref_str[i].Z()-ref_str[i+2].Z());
         if(v1.Angle(v2)!=C_0 &&
            fabs(v1.Angle(v2))!=C_180) {
            if(gDebug)
               Mout << "The molecule is NOT linear (angle = " << v1.Angle(v2) << ")!" << endl;
            is_linear=false;
            base_atom=i;
            normal=v1.Cross(v2);
            break;
         }
      }
      if(is_linear) {
         // Add one dummy atom to make the molecule planar
         // select vector "base_atom" -> COM, check if
         // base_atom is at COM
         Vector3D vl1;
         vl1.SetX(ref_str[base_atom].X());
         vl1.SetY(ref_str[base_atom].Y());
         vl1.SetZ(ref_str[base_atom].Z());
         
         Vector3D vl1_test_rot;
         vl1_test_rot.SetX(rot[base_atom].X());
         vl1_test_rot.SetY(rot[base_atom].Y());
         vl1_test_rot.SetZ(rot[base_atom].Z());
         if((vl1.Length() < C_I_10_10) || (vl1_test_rot.Length() < C_I_10_10) ) {
            if(ref_str.size()>I_1) {
               if (aIoLevel >20) Mout << "Warning atom close to COM, setting atom to: second." << endl;
               base_atom = ((base_atom == I_1) ? I_0 : I_1);
            }
            else
               MIDASERROR("Only one atom??? This should not happen...");
         }
         Nuclei nuc_ind;
         // check for direction
         vl1.SetX(ref_str[base_atom].X());
         vl1.SetY(ref_str[base_atom].Y());
         vl1.SetZ(ref_str[base_atom].Z());
         MidasMatrix gen_rot(I_3,C_0);
         gen_rot[I_0][I_1]=C_1;
         gen_rot[I_1][I_2]=C_1;
         gen_rot[I_2][I_0]=C_1;
         //Mout << "Ref vl1     = " << vl1 << endl;
         vl1=vl1.TransformMidasMatrix(gen_rot);
         //Mout << "rot Ref vl1 = " << vl1 << endl;
         vl1=vl1*(C_2/vl1.Length());
         nuc_ind.SetXyz(vl1);
         ref_str.push_back(nuc_ind);
         // also for the rotated geom
         vl1.SetX(rot[base_atom].X());
         vl1.SetY(rot[base_atom].Y());
         vl1.SetZ(rot[base_atom].Z());
         //Mout << "rot vl1     = " << vl1 << endl;
         vl1=vl1.TransformMidasMatrix(gen_rot);
         //Mout << "rot rot vl1 = " << vl1 << endl;
         vl1=vl1*(C_2/vl1.Length());
         nuc_ind.SetXyz(vl1);
         rot.push_back(nuc_ind);
         // second atom may now be added by using the newly added dummy
         vl1.SetX(ref_str[base_atom].X());
         vl1.SetY(ref_str[base_atom].Y());
         vl1.SetZ(ref_str[base_atom].Z());
         Vector3D vl2;
         vl2.SetX(ref_str.back().X());
         vl2.SetY(ref_str.back().Y());
         vl2.SetZ(ref_str.back().Z());
         normal=vl1.Cross(vl2);
         normal.Normalize();
         nuc_ind.SetX(normal.X()*C_2);
         nuc_ind.SetY(normal.Y()*C_2);
         nuc_ind.SetZ(normal.Z()*C_2);
         ref_str.push_back(nuc_ind);
         // same for rotated
         vl1.SetX(rot[base_atom].X());
         vl1.SetY(rot[base_atom].Y());
         vl1.SetZ(rot[base_atom].Z());
         vl2.SetX(rot.back().X());
         vl2.SetY(rot.back().Y());
         vl2.SetZ(rot.back().Z());
         normal=vl1.Cross(vl2);
         normal.Normalize();
         nuc_ind.SetX(normal.X()*C_2);
         nuc_ind.SetY(normal.Y()*C_2);
         nuc_ind.SetZ(normal.Z()*C_2);
         rot.push_back(nuc_ind);
      }
      // Check for planarity...
      bool is_planar=true;
      for(In i=0;i<ref_str.size()-1;i++) {
         v1.SetX(ref_str[i].X()-ref_str[i+1].X());
         v1.SetY(ref_str[i].Y()-ref_str[i+1].Y());
         v1.SetZ(ref_str[i].Z()-ref_str[i+1].Z());
         //Mout << "v1.Angle(normal) "  << v1.Angle(normal) << endl;
         if(fabs(fabs(v1.Angle(normal))-C_90)>C_1) {
            if(gDebug)
               Mout << "The molecule is NOT planar (angle = " << v1.Angle(normal) << ")!" << endl;
            is_planar=false;
            break;
         }
      }
      if(is_planar) {
         // Add a dummy atom in the normal vector direction
         // use a distance of 2.0 au
         normal.Normalize();
         Nuclei nuc_ind;
         nuc_ind.SetX(ref_str[base_atom].X()+normal.X()*C_2);
         nuc_ind.SetY(ref_str[base_atom].Y()+normal.Y()*C_2);
         nuc_ind.SetZ(ref_str[base_atom].Z()+normal.Z()*C_2);
         ref_str.push_back(nuc_ind);
         // also for the rotated geom
         v1.SetX(rot[base_atom].X()-rot[base_atom+1].X());
         v1.SetY(rot[base_atom].Y()-rot[base_atom+1].Y());
         v1.SetZ(rot[base_atom].Z()-rot[base_atom+1].Z());
         v2.SetX(rot[base_atom].X()-rot[base_atom+2].X());
         v2.SetY(rot[base_atom].Y()-rot[base_atom+2].Y());
         v2.SetZ(rot[base_atom].Z()-rot[base_atom+2].Z());
         normal=v1.Cross(v2);
         normal.Normalize();
         nuc_ind.SetX(rot[base_atom].X()+normal.X()*C_2);
         nuc_ind.SetY(rot[base_atom].Y()+normal.Y()*C_2);
         nuc_ind.SetZ(rot[base_atom].Z()+normal.Z()*C_2);
         rot.push_back(nuc_ind);
      }
      // Set up the N*O^T and O*O^T products
      MidasMatrix n_ot(3);
      MidasMatrix o_ot(3);
      MidasMatrix rot_coord(I_3,(In)ref_str.size());
      MidasMatrix ref_str_coord(I_3,(In)ref_str.size());
      for(In i=I_0;i<ref_str.size();i++) {
         rot_coord[0][i]=rot[i].X();
         rot_coord[1][i]=rot[i].Y();
         rot_coord[2][i]=rot[i].Z();
         ref_str_coord[0][i]=ref_str[i].X();
         ref_str_coord[1][i]=ref_str[i].Y();
         ref_str_coord[2][i]=ref_str[i].Z();
      }
      if(gDebug) 
      {
         Mout << "Reference coordinates:" << endl;
         for(In i=0;i<ref_str.size();i++)
            Mout << ref_str[i] << endl;
         Mout << "Rotated coordinates:" << endl;
         for(In i=0;i<rot.size();i++)
            Mout << rot[i] << endl;
      }
      MidasMatrix ref_str_coord_trans(Transpose(ref_str_coord));
      
      n_ot=rot_coord*ref_str_coord_trans;
      //Mout << "n_ot "<< endl;
      //Mout << n_ot ;
      //Mout << endl << endl;
      
      o_ot=ref_str_coord*ref_str_coord_trans;
      //Mout << "o_ot "<< endl;
      //Mout << o_ot ;
      //Mout << endl << endl;
      
      // invert (O*O^T)
      MidasMatrix non_inv(o_ot);
      if(!o_ot.IsSymmetric()) Mout << "\to_ot matrix is not symmetric!" << endl;
      
      //Invert(o_ot); buggy... i follow what in numder

      MidasMatrix m_sq(I_3,C_0);
      MidasMatrix m_unit(I_3,C_0);
      MidasMatrix m_cp(I_3,C_0);
     
      Nb det_oot = o_ot[I_0][I_0]*(o_ot[I_1][I_1]*o_ot[I_2][I_2]-
                   o_ot[I_1][I_2]*o_ot[I_2][I_1]) -o_ot[I_0][I_1]*
                  (o_ot[I_1][I_0]*o_ot[I_2][I_2]-o_ot[I_1][I_2]*o_ot[I_2][I_0])
                  +o_ot[I_0][I_2]*(o_ot[I_1][I_0]*o_ot[I_2][I_1]-
                   o_ot[I_1][I_1]*o_ot[I_2][I_0]);
     
      //look if it is singular
      if (det_oot<C_NB_EPSILON*C_10_2)
      {
         MIDASERROR(" Singular matrix o_ot ");
      }
      //
      Nb trace = o_ot.Trace();
      m_cp=o_ot;
      m_sq = o_ot*m_cp;
      Nb trace_sq = m_sq.Trace();
      m_cp.Scale(trace);
      m_unit.Unit();
      m_unit.Scale(((pow(trace,C_2)-trace_sq)/C_2));
      o_ot.Zero();
      o_ot+=m_unit;
      o_ot-=m_cp;
      o_ot+=m_sq;
      o_ot.Scale((C_1/det_oot));

      //Mout << "o_ot inverted"<< endl;
      //Mout << o_ot ;
      //Mout << endl << endl;
      

      rot_mat=n_ot*o_ot;
      rot_mat.Transpose();

      
      // Do rotation, just for test
      //Mout << "rot_mat "<< endl;
      //Mout << rot_mat ;
      //Mout << endl << endl;


      vector<Nuclei> check_rot(rot.size());
      check_rot=rot;
      
      for(In i=I_0;i<ref_str.size();i++) {
         check_rot[i].Rotate(rot_mat);
      }
         
      success =true;   // Now check if this is really true
      
      if(gDebug) 
      {
         Mout << "Error, orig vs Back-ROT GEOM: " << endl;
      }
      for(In i=0;i<ref_str.size();i++) {
         if(gDebug) 
         {
            Mout << check_rot[i].AtomLabel()
                 << " " << check_rot[i].X()-ref_str[i].X()
                 << " " << check_rot[i].Y()-ref_str[i].Y()
                 << " " << check_rot[i].Z()-ref_str[i].Z() << endl;
         }
         if(fabs(check_rot[i].X()-ref_str[i].X()) > aSymThr ||
            fabs(check_rot[i].Y()-ref_str[i].Y()) > aSymThr ||
            fabs(check_rot[i].Z()-ref_str[i].Z()) > aSymThr ) 
         {
            //Mout << "WARNING: large difference in original and back-rot geom found!" << endl;
            //Mout << "Sym thr: " << aSymThr << endl << fabs(check_rot[i].X()-ref_str[i].X())
                 //<< " " << fabs(check_rot[i].Y()-ref_str[i].Y()) << " "
                 //<< " " << fabs(check_rot[i].Z()-ref_str[i].Z()) << endl;
            success = false;
         }
      }
      // it should not take so many attempt... 
      if (success && no_of_attempts> I_10) Mout << " After "<< no_of_attempts << " attempt(s) the molecules were mapped and rotated correctly "  << endl;

      // a second check on the determinant of the rotation matrix, if success it should be exactly 1.
      Nb det;
      det = rot_mat[0][0] * rot_mat[1][1] * rot_mat[2][2] - 
            rot_mat[0][0] * rot_mat[1][2] * rot_mat[2][1] + 
            rot_mat[0][1] * rot_mat[1][2] * rot_mat[2][0] - 
            rot_mat[0][1] * rot_mat[1][0] * rot_mat[2][2] + 
            rot_mat[0][2] * rot_mat[1][0] * rot_mat[2][1] - 
            rot_mat[0][2] * rot_mat[1][1] * rot_mat[2][0];
      if (success && fabs(det-C_1)>C_I_10_5)   
      {
         //Mout << "Determinant dev. from 1: " << fabs(det-C_1) << endl;
         success = false; // if the determinant is so different from 1 we can't accept the rotation matrix
                          // Note that for a molecule with a inversion center we may have det=-1,
                          // in principle this is still fine but we rather relable the atoms to get a simple rotation.

         // with this further check the warning for det=-1 is not printed. 
         if (fabs(fabs(det)-C_1) >C_I_10_5) Mout << "Determinant " << det << " there may be a problem" << endl;

         if(mayIinvert && (fabs(fabs(det)-C_1) < C_I_10_5))
            success=true;    // in case the inversion is acceptable we are happy with the result
      }
      
      if(gDebug && !success)
      {
         Mout <<" Original"<< endl;
         for(In i=0;i<ref_str.size();i++) 
            Mout << ref_str[i] << endl;
         
         Mout <<" Rotated"<< endl;
         for(In i=0;i<ref_str.size();i++)
            Mout << check_rot[i] << endl;
      }

      if (no_of_attempts > maxcycle )  
      { 
         Mout << " I didn't manage" << endl;
         return false;
      }
      

   } while(!success);
   

   if(gDebug)
   {
      Mout <<  " label_rot_orig " << endl;
      for(In it=I_0;it<label_rot_orig.size();it++)
      {
          Mout << label_rot_orig[it].first << " " << label_rot_orig[it].second << endl;
      }
      Mout <<  endl;
   }     

   if (gDebug && (no_of_attempts > I_1))
   {
      for(map<In,In>::iterator i=molmol.begin();i!=molmol.end();i++) 
         Mout << i->first << " --- " << i->second <<  endl;
      
      Mout << rot_mat << endl;
   }
   
   //Mout << " Done: " << no_of_attempts << endl;
   return true;
}
