/**
 *******************************************************************************
 * 
 * @file    PropertyFileStorage.cc
 * @date    03-10-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For handling storage of a property file, especially on disc/in memory,
 *    and related actions such as look-up.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <fstream>
#include <sstream>

#include "pes/PropertyFileStorage.h"
#include "util/Error.h"
#include "util/Io.h"
#include "util/UnderlyingType.h"
#include "util/CallStatisticsHandler.h"
#include "util/conversions/FromString.h"

using midas::util::ToUType;

/*******************************************************************************
 *******************************************************************************
 * CONSTRUCTORS, ETC.
 *******************************************************************************
 ******************************************************************************/


/***************************************************************************//**
 * Construct object and associate it with given file name. File need not exist
 * at time of construction (only checked when needing to read the file).
 * Initial storage is _on disc_.
 *
 * @param[in] arFileName
 *    File name with which object is associated.
 ******************************************************************************/
PropertyFileStorage::PropertyFileStorage
   (  const std::string& arFileName
   )
   :  mFileName(arFileName)
   ,  mContainer()
   ,  mStorage(Storage::on_disc)
{
}

/***************************************************************************//**
 * Clears contents stored in memory (if any) before destruction. This call to
 * ClearFromMemory() shall write any appended property lines to the file on
 * disc.
 ******************************************************************************/
PropertyFileStorage::~PropertyFileStorage
   (
   )
{
   ClearFromMemory();
}


/*******************************************************************************
 *******************************************************************************
 * PUBLIC FUNCTIONS
 *******************************************************************************
 ******************************************************************************/


/***************************************************************************//**
 * @return
 *    String describing how file is currently internally stored.
 ******************************************************************************/
std::string PropertyFileStorage::StorageMode
   (
   ) const
{
   return StorageMode(mStorage);
}


/***************************************************************************//**
 * Open associated file for reading and reads line by line. Each line is
 * expected to have the format
 *
 *     <calc_num>   <calc_code>   <value>
 *
 * i.e. something like
 *
 *     6   *#3_-1/4*   -6.6586963107411051288054E-03
 *
 * The file contents are stored internally in container of container_t for fast
 * subsequent look-up. If successful the storage mode is internally set to be
 * _in memory_.
 *
 * @note
 *    Any previous in-memory contents are left _as they are_. I.e. to make sure
 *    there are no remnants from before this function call it's the user's
 *    responsibility to first call ClearFromMemory().
 *
 * @note
 *    Exits with MIDASERROR if any file operations fail. Furthermore, if file
 *    contains two lines with the same calc. code, the function will also throw
 *    a MIDASERROR.
 ******************************************************************************/
void PropertyFileStorage::ReadIntoMemory
   (
   )
{
   std::ifstream ifs(mFileName);
   ValidateIFStream(ifs, mFileName);

   std::string s;
   calc_code_t calc_code;

   while (std::getline(ifs, s))
   {
      std::istringstream iss(s);
      iss.exceptions(std::istringstream::failbit | std::istringstream::badbit);

      property_t property;
      auto& value = std::get<ToUType(property_fields::value)>(property);
      auto& calc_num = std::get<ToUType(property_fields::calc_num)>(property);

      try
      {
         iss >> calc_num >> calc_code >> value;
         auto pair_iter_bool = mContainer.emplace(std::move(calc_code), std::move(property));
         if (!pair_iter_bool.second)
         {
            auto iter = pair_iter_bool.first;
            auto&& existing_calc_num  = std::get<ToUType(property_fields::calc_num)>(iter->second);
            auto&& existing_calc_code = iter->first;
            auto&& existing_value     = std::get<ToUType(property_fields::value)>(iter->second);
            std::stringstream err;
            err   << "Problem when reading: " << mFileName << '\n'
                  << "Failed container_t::emplace(...); probably lines with same calc. codes.\n"
                  << "Couldn't emplace line in memory: '" << s << "'\n"
                  << "Existing contents with same key: '"
                  << existing_calc_num << "   "
                  << existing_calc_code << "   "
                  << existing_value << "'\n"
                  ;
            MIDASERROR(err.str());
         }
      }
      catch (const std::istringstream::failure& e)
      {
         std::stringstream err;
         err   << "Problem when reading: " << mFileName << '\n'
               << "Caught exception from std::istringstream::operator>> for line.\n"
               << "Exception:       " << e.what() << '\n'
               << "Line read:       '" << s << "'\n"
               << "Expected format: '6   *#3_-1/4*   -6.6586963107411051288054E-03'.\n"
               ;
         MIDASERROR(err.str());
      }
   }

   ifs.close();
   mStorage = Storage::in_memory;
}


/***************************************************************************//**
 * Clears file contents from memory and internally changes the storage mode to
 * be _on disc_ again. No effect if storage is currently not _in memory_.
 * 
 * If anything has been appended while contents were _in memory_, this function
 * will write it to disc, appending it to the file.
 ******************************************************************************/
void PropertyFileStorage::ClearFromMemory
   (
   )
{
   if (mStorage == Storage::in_memory)
   {
      if (HasPropertiesToAppend())
      {
         // If AppendToFile fails, _should_ throw MIDASERROR, but we'll check
         // explicitly anyway.
         if (!AppendToFile(std::move(mToBeAppendedToFile)))
         {
            MIDASERROR("Something went wrong in AppendToFile, called by ClearFromMemory().");
         }
      }
      mContainer.clear();
      mToBeAppendedToFile.clear();
      mStorage = Storage::on_disc;
   }
}

/***************************************************************************//**
 * Appends the calc. code/property pair either _on disc_ or _in memory_,
 * depending on how the file is currently stored. Searches for the argument
 * calc. code, and only appends if unique. If appended _on disc_, property will
 * be written to file immediately. If appended _in memory_, property will be
 * stored in memory but not written to disc before calling destructor or
 * ClearFromMemory().
 *
 * @param[in] aCalcCode
 *    The calc. code of the property to append. Must be unique for successful
 *    operation.
 * @param[in] aProperty
 *    The corresponding property.
 * @return
 *    Whether the operation was successful, i.e. calc. code didn't already
 *    exist, and all memory/disc operations went fine.
 ******************************************************************************/
bool PropertyFileStorage::Append
   (  calc_code_t aCalcCode
   ,  property_t aProperty
   )
{
   // Only append if key doesn't already exist.
   if (!(Search(aCalcCode)).second)
   {
      switch(mStorage)
      {
         case Storage::on_disc:
         {
            return AppendOnDisc(std::make_pair(std::move(aCalcCode), std::move(aProperty)));
         }
         case Storage::in_memory:
         {
            return AppendInMemory(std::make_pair(std::move(aCalcCode), std::move(aProperty)));
         }
         default:
         {
            MIDASERROR("Unknown storage mode: " + StorageMode(mStorage));
            return false;
         }
      }
   }
   else
   {
      return false;
   }
}

/***************************************************************************//**
 * Searches for the argument calc. code, either _on disc_ or _in memory_,
 * depending on how the file is currently stored.
 *
 * @note
 *    If the returned bool is `false`, the returned property contents are
 *    _undefined_.
 *
 * @param[in] arCalcCode
 *    Calculation code to search for.
 * @return
 *    Pair of (1) property contents corresponding to calc. code and (2) bool
 *    indicating whether the search for the given calc. code was successful.
 ******************************************************************************/
std::pair<PropertyFileStorage::property_t, bool> PropertyFileStorage::Search
   (  const calc_code_t& arCalcCode
   )  const
{
   switch (mStorage)
   {
      case Storage::on_disc:
      {
         return SearchOnDisc(arCalcCode);
      }
      case Storage::in_memory:
      {
         return SearchInMemory(arCalcCode);
      }
      default:
      {
         MIDASERROR("Unknown storage mode: " + StorageMode(mStorage));
         return std::pair<property_t, bool>(property_t(), false);
      }
   }
}




/*******************************************************************************
 *******************************************************************************
 * PRIVATE FUNCTIONS
 *******************************************************************************
 ******************************************************************************/


/***************************************************************************//**
 * For uniformly validating a std::Xfstream (X = i/o) within this class. Checks
 * status of stream (whether its operator bool() evaluates to true).
 *
 * @note
 *    Calls MIDASERROR if stream is invalid.
 *
 * @param[in] arFS
 *    Stream to validate.
 * @param[in] arFileName
 *    Filename associated with stream. Not used for validation, only used for
 *    giving useful error information.
 ******************************************************************************/
void PropertyFileStorage::ValidateIFStream
   (  const std::ifstream& arFS
   ,  const std::string& arFileName
   )  const
{
   if (!arFS)
   {
      MIDASERROR("Unable to open file: " + mFileName);
   }
}
void PropertyFileStorage::ValidateOFStream
   (  const std::ofstream& arFS
   ,  const std::string& arFileName
   )  const
{
   if (!arFS)
   {
      MIDASERROR("Unable to open file: " + mFileName);
   }
}


/***************************************************************************//**
 * Converts Storage `enum` to string using conversions supplied by
 * PropertyFileStorage::mStorageEnumToString. If class developer forgot to
 * provide a map entry for the `enum` value, a string with the underlying
 * integer value is returned.
 *
 * @param[in] aStorage
 *    Storage `enum` value to convert.
 * @return
 *    String describing the storage `enum` value.
 ******************************************************************************/
std::string PropertyFileStorage::StorageMode
   (  PropertyFileStorage::Storage aStorage
   )  const
{
   try
   {
      return mStorageEnumToString.at(aStorage);
   }
   catch (const std::out_of_range&)
   {
      return   "unknown enum-to-string conversion (int value: " 
               + std::to_string(ToUType(aStorage))
               + ")";
   }
}


/***************************************************************************//**
 * Opens (and validates) the file associated with the object. Searches linearly
 * through the file until finding the line with the correct calc. code, then
 * returns found property contents.
 *
 * As of this writing (Oct, 2017) the expected line format is as in the
 * documentation for ReadIntoMemory().
 *
 * @note
 *    The use of SearchInFile2() is due to historic reasons, i.e. to ensure
 *    backwards compatibility. The function is not so pretty, and some of the
 *    arguments to it are a bit _funky_... Change if you like/dare.
 *
 * @note
 *    If the returned bool is `false`, the returned property contents are
 *    _undefined_.
 *
 * @param[in] arCalcCode
 *    Calculation code to search for.
 * @return
 *    Pair of (1) property contents corresponding to calc. code and (2) bool
 *    indicating whether the search for the given calc. code was successful.
 ******************************************************************************/
std::pair<PropertyFileStorage::property_t, bool> PropertyFileStorage::SearchOnDisc
   (  const calc_code_t& arCalcCode
   )  const
{
   std::ifstream ifs(mFileName);
   ValidateIFStream(ifs, mFileName);

   std::string calc_num;
   Nb value(C_0);
   bool success = SearchInFile2(arCalcCode, ifs, mFileName, calc_num, value, I_2);
   property_t property;
   std::get<ToUType(property_fields::value)>(property) = static_cast<value_t>(value);
   std::get<ToUType(property_fields::calc_num)>(property) = midas::util::FromString<calc_num_t>(calc_num);
   return std::pair<property_t, bool>(property, success);
}


/***************************************************************************//**
 * Looks up the argument calc. code in the internal storage container which the
 * file has been read into at some point. (Depending on the container type,
 * this is some sort of fast look-up, probably key type/mapped type based using
 * std::map or std::unordered_map.)
 *
 * @note
 *    If the returned bool is `false`, the returned property contents are
 *    _undefined_.
 *
 * @param[in] arCalcCode
 *    Calculation code to look up.
 * @return
 *    Pair of (1) property contents corresponding to calc. code and (2) bool
 *    indicating whether the search for the given calc. code was successful.
 ******************************************************************************/
std::pair<PropertyFileStorage::property_t, bool> PropertyFileStorage::SearchInMemory
   (  const calc_code_t& arCalcCode
   )  const
{
   try
   {
      return std::pair<property_t, bool>(mContainer.at(arCalcCode), true);
   }
   catch (const std::out_of_range&)
   {
      return std::pair<property_t, bool>(property_t(), false);
   }
}

/***************************************************************************//**
 * @param[in] arContElem
 *    The calc.code/property pair to write to disc.
 * @return
 *    Whether the writing operation went fine.
 ******************************************************************************/
bool PropertyFileStorage::AppendOnDisc
   (  container_t::value_type&& arContElem
   )  const
{
   return AppendToFile(std::vector<container_t::value_type>{arContElem});
}

/***************************************************************************//**
 * Appends the argument to the PropertyFileStorage::mToBeAppendedToFile vector,
 * keeping the ordering of the appended property lines. Also inserts it
 * immediately in the PropertyFileStorage::mContainer, so that it's accessible
 * for quick lookup.
 *
 * @param[in] arContElem
 *    The calc.code/property pair to append in memory.
 * @return
 *    Whether it was successfully inserted in the container of the object.
 ******************************************************************************/
bool PropertyFileStorage::AppendInMemory
   (  container_t::value_type&& arContElem
   )
{
   mToBeAppendedToFile.emplace_back(std::move(arContElem));
   auto pair_iter_bool = mContainer.insert(mToBeAppendedToFile.back());
   return pair_iter_bool.second;
}

/***************************************************************************//**
 * Opens the file and appends (in order) the contents of the passed container.
 *
 * @param[in] arVec
 *    Vector containing the contents to append to the file.
 * @return
 *    Whether the operation went fine. (But throws MIDASERROR if not...)
 ******************************************************************************/
bool PropertyFileStorage::AppendToFile
   (  std::vector<container_t::value_type>&& arVec
   )  const
{
   // Open file in append mode, check it's open, set flags.
   std::ofstream ofs(mFileName, std::ofstream::app);
   ValidateOFStream(ofs, mFileName);
   ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);
   ofs.flags(mAppendFlags);
   ofs.precision(mAppendPrecision);

   // Then append line by line.
   for(auto&& v: arVec)
   {
      auto&& calc_num   = std::get<ToUType(property_fields::calc_num)>(v.second);
      auto&& calc_code  = v.first;
      auto&& calc_value = std::get<ToUType(property_fields::value)>(v.second);
      try
      {
         ofs   << calc_num << "   "
               << calc_code << "   "
               << calc_value << '\n'
               ;
      }
      catch(const std::ofstream::failure& e)
      {
         ofs << std::flush;
         std::stringstream err;
         err   << "Problem when appending to: " << mFileName << '\n'
               << "Caught exception from std::ofstream::operator<<.\n"
               << "Exception:       " << e.what() << '\n'
               ;
         MIDASERROR(err.str());
         return false;
      }
   }
   ofs << std::flush;

   // If getting to here, we succeeded appending.
   return true;
}

/***************************************************************************//**
 * @return
 *    Whether any properties in memory need be written to file.
 ******************************************************************************/
bool PropertyFileStorage::HasPropertiesToAppend
   (
   )  const
{
   return !mToBeAppendedToFile.empty();
}


/*******************************************************************************
 *******************************************************************************
 * NON-MEMBER FUNCTIONS
 *******************************************************************************
 ******************************************************************************/


/***************************************************************************//**
 * Searches in for calc. code in given PropertyFileStorage reference. Then
 * extracts the results from the returned object and puts them in the argument
 * references.
 *
 * @param[in] arPfs
 *    The object in which to search for calc. code.
 * @param[in] arCalcCode
 *    The calc. code to search for.
 * @param[out] arVal
 *    For storing the found property value. Unmodified if search unsuccessful.
 * @param[out] arCalcNum
 *    For storing the found calc. num. Unmodified if search unsuccessful.
 * @return
 *    Whether the sought calc. code was found. If `false`, the passed arVal
 *    and arCalcNum will not have been modified.
 ******************************************************************************/
bool ExtractPropertyLine
   (  const PropertyFileStorage& arPfs
   ,  const PropertyFileStorage::calc_code_t& arCalcCode
   ,  PropertyFileStorage::value_t& arVal
   ,  PropertyFileStorage::calc_num_t& arCalcNum
   )
{
   auto search_result = arPfs.Search(arCalcCode);
   if (search_result.second)
   {
      using property_fields = PropertyFileStorage::property_fields;
      arVal = std::get<ToUType(property_fields::value)>(search_result.first);
      arCalcNum = std::get<ToUType(property_fields::calc_num)>(search_result.first);
   }
   return search_result.second;
}

/***************************************************************************//**
 * @param[in] arPfs
 *    The object in which to search for calc. code.
 * @param[in] arCalcCode
 *    The calc. code to search for.
 * @param[out] arVal
 *    For storing the found property value. Unmodified if search unsuccessful.
 * @return
 *    Whether the sought calc. code was found. If `false`, the stored arVal
 *    will not have been modified.
 ******************************************************************************/
bool ExtractPropertyLine
   (  const PropertyFileStorage& arPfs
   ,  const PropertyFileStorage::calc_code_t& arCalcCode
   ,  PropertyFileStorage::value_t& arVal
   )
{
   PropertyFileStorage::calc_num_t dummy;
   return ExtractPropertyLine(arPfs, arCalcCode, arVal, dummy);
}

/***************************************************************************//**
 * @param[in] arPfs
 *    The object in which to append the arguments.
 * @param[in] aCalcCode
 *    The calc. code to append.
 * @param[in] aVal
 *    The value to append.
 * @param[out] aCalcNum
 *    The calc. number to append.
 * @return
 *    Whether appending was successful.
 ******************************************************************************/
bool AppendPropertyLine
   (  PropertyFileStorage& arPfs
   ,  PropertyFileStorage::calc_code_t aCalcCode
   ,  PropertyFileStorage::value_t aVal
   ,  PropertyFileStorage::calc_num_t aCalcNum
   )
{
   using property_fields = PropertyFileStorage::property_fields;
   PropertyFileStorage::property_t p;
   std::get<ToUType(property_fields::value)>(p) = std::move(aVal);
   std::get<ToUType(property_fields::calc_num)>(p) = std::move(aCalcNum);
   return arPfs.Append(std::move(aCalcCode), std::move(p));
}
