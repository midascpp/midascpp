/**
************************************************************************
*
* @file                DoFitting.h
*
* Created:             23-03-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Driver for fitting routine
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef DOFITTING_H_INCLUDED
#define DOFITTING_H_INCLUDED

// std headers
#include <vector>

// midas headers
#include "pes/PesInfo.h"
#include "pes/fitting/PolyFit.h"
#include "pes/fitting/FitFunctions.h"
#include "input/PesCalcDef.h"
#include "input/FitBasisCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "mmv/MidasVector.h"

/**
 *
**/
class DoFitting
{
   private:
      
      //! FitBasisCalcDef reference 
      const FitBasisCalcDef&        mFitBasisCalcDef;

      //! PesCalcDef reference 
      const PesCalcDef&             mPesCalcDef;
      
      //! PesInfo reference
      const PesInfo&                mPesInfo;

      //! Automatic determination of the number of fit-basis functions for a given mode
      std::vector<In> AutoMaxFitBasisOrder(const In& aBar, const ModeCombi& aModeCombi, const std::vector<MidasVector>& aCgCoords, const std::vector<MidasVector>& aFgCoords, const MidasVector& aFgProperty, const MidasVector& aFgSigma, const FitFunctions& aFitBasisFuncs, const std::string& aPropType, const std::vector<In>& aMaxIterOrder) const;

   public:
      
      //! Default constructor.
      DoFitting();

      //! Constructor
      DoFitting(const FitBasisCalcDef& aFitBasisCalcDef, const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo);

      //!
      MidasVector DoFit(const In& abar, const ModeCombi& mt, const std::vector<MidasVector>& aCgCoords, const std::vector<MidasVector>& aFgCoords, const MidasVector& aFgProperty, const MidasVector& aFgSigma, const FitFunctions& aFitBasisFuncs, std::vector<InVector>& aPowvecVec, const std::string& aPropName, const std::pair<In, In>& aInertiaProps, const bool& aDerivatives, std::vector<In>& aAutoNoFitFuncs) const;
      
      //! Determine the effective maximum order of the fit-basis functions
      void FindPowVec(const std::vector<MidasVector>& arXig, const std::vector<In>& arRefPowVec, std::vector<In>& arPowVec, const ModeCombi& arMt, const bool& aIsInertia, const bool& aDerivatives) const;

      //!
      void AddPowvecsToVec(std::vector<InVector>& arPowvecVec, InVector& aInVec, const In& arNmc, const In& aCoordNr, const InVector& arMxPows) const;

      //! Construct the fit-basis functions
      void ConstructAllBasisFunc(const In& aIprop, std::vector<InVector>& arPowvecVec, const ModeCombi& arModeCombi, const InVector& arMxPows, const std::string& aPropType) const;
      
      //! Determine if the polynomial term is symmetric
      bool IsTermSymmetric(const InVector& arIvec, const std::vector<In>& arModeCombi) const;
};

#endif /* DOFITTING_H_INCLUDED */
