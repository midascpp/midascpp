#ifndef MIDAS_PES_SCALING_INFO_H_INCLUDED
#define MIDAS_PES_SCALING_INFO_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "input/PesCalcDef.h"
#include "mmv/MidasVector.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace pes
{

/**
 * Class for holding information on scaling factors.
 **/
class ScalingInfo
{
   private:

      //! Left scaling factors
      MidasVector mLScalFactVector;

      //! Right scaling factors
      MidasVector mRScalFactVector;         

   public:

      //! Constructor.
      ScalingInfo(const PesCalcDef& aPesCalcDef, const molecule::MoleculeInfo& aMolecule);

      //!@{
      //! Get all scaling factors.
      const MidasVector& GetRScalFactVector() const { return mRScalFactVector; }
      const MidasVector& GetLScalFactVector() const { return mLScalFactVector; }
      //!@}

      //!@{
      //! Get scaling factors.
      const Nb& GetRScalFact(In arIn) const { return mRScalFactVector[arIn]; }
      const Nb& GetLScalFact(In arIn) const { return mLScalFactVector[arIn]; }
      //!@}
      
      //!@{
      //! Reset individual scaling factors (used in incr_pes stuff...)
      void SetLScalFact(const In& arIn, const Nb& arNb) { mLScalFactVector[arIn] = arNb; }
      void SetRScalFact(const In& arIn, const Nb& arNb) { mRScalFactVector[arIn] = arNb; }
      //!@}
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_SCALING_INFO_H_INCLUDED */
