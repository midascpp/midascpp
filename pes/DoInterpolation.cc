/**
************************************************************************
*
* @file                DoInterpolation.cc
*
* Created:             05-04-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Driver for interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

//std headers
#include <vector>
#include <string>

// midas headers
#include "pes/DoInterpolation.h"
#include "pes/adga/ItGridHandler.h"

/**
 *
 * @param aPesCalcDef
 * @param aPesInfo
**/
DoInterpolation::DoInterpolation
   (  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   )
   :  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo) 
{

}

/**
 * Interpolation of grid points by way of splines or Shepard interpolation techniques
 *
 * @param aBar                  The energy/property number
 * @param aModeCombi            A mode combination
 * @param aCgCoords
 * @param aFgCoords
 * @param aShepardDerivs
 * @param aCgCoordsGhosts
 * @param aCgPropertyGhosts
 * @param aShepardDerivsGhosts
 * @param aCgProperty
 * @param aFgProperty
 * @param aFgSigma
 * @param aCgCoordsUncompressed
 * @param aCgPoints
**/
void DoInterpolation::GetInterpolatedPoints
   (  const In& aBar
   ,  const ModeCombi& aModeCombi
   ,  const std::vector<MidasVector>& aCgCoords
   ,  const std::vector<MidasVector>& aFgCoords
   ,  std::vector<MidasVector>& aShepardDerivs
   ,  const std::vector<MidasVector>& aCgCoordsGhosts
   ,  const MidasVector& aCgPropertyGhosts
   ,  const std::vector<MidasVector>& aShepardDerivsGhosts
   ,  MidasVector& aCgProperty
   ,  MidasVector& aFgProperty
   ,  MidasVector& aFgSigma
   ,  std::vector<MidasVector>& aCgCoordsUncompressed
   ,  const In& aCgPoints
   )  const
{
   // Print out info on red set
   if (gDebug)
   {
      if (mPesCalcDef.GetmPesShepardInt())
      {
         OutputCoreGrid(aCgCoords, aCgProperty, aCgPoints, aCgPoints, aShepardDerivs);
      }
      else
      {
         OutputCoreGrid(aCgCoords, aCgProperty, aCgPoints, I_0, aShepardDerivs);
      }
   }

   MidasVector property_derivatives;
   property_derivatives.SetNewSize(aCgPoints);

   MidasVector first_deriv_left(aModeCombi.Size(), C_0);
   MidasVector first_deriv_right(aModeCombi.Size(), C_0);

   bool spline_int = mPesCalcDef.GetmPesSplineInt();
   bool shepard_int = mPesCalcDef.GetmPesShepardInt();
   
   // Use interpolation for extrapolated surfaces
   if (  mPesInfo.GetmUseDerivatives() 
      && mPesCalcDef.GetmPesDoExtrap() 
      && aModeCombi.Size() > mPesInfo.GetExtrapFrom()
      )
   {
      spline_int = mPesCalcDef.GetmPesSplineIntForExtrap();
      shepard_int = mPesCalcDef.GetmPesShepardIntForExtrap();
   }

   if (mPesCalcDef.GetmPesAdga() && spline_int)
   {
      MidasWarning(" Warning, I do not use spline_int during ADGA ");
      spline_int = false;
   }
   
   // Do spline interpolation
   if (spline_int)
   { 
      if (gPesIoLevel > I_10)
      {
         Mout << std::endl << " Doing Spline interpolation for mode combination " << aModeCombi.MCVec() << " and property " << mPesInfo.GetMultiLevelInfo().GetPropertyName(aBar) << std::endl;
      }

      if (aModeCombi.Size() == I_1)
      {
         Spline1D Interp1D(mPesCalcDef.GetmSplineType(), aCgCoords[I_0], aCgProperty, first_deriv_left[I_0], first_deriv_right[I_0]);

         Interp1D.GetYigDerivs(property_derivatives);

         GetYog(aFgProperty, aFgCoords[I_0], aCgCoords[I_0], aCgProperty, property_derivatives);
      }
      else if (aModeCombi.Size() == I_2)
      {
         Spline2D Interp2D(mPesCalcDef.GetmSplineType(), aCgCoords[I_0], aCgCoords[I_1], aCgProperty, first_deriv_left[I_1], first_deriv_right[I_1]);

         Interp2D.GetYigDerivs(property_derivatives);

         GetYog(aFgProperty, aFgCoords[I_0], aFgCoords[I_1], aCgCoords[I_0], aCgCoords[I_1], aCgProperty, property_derivatives, mPesCalcDef.GetmSplineType(), first_deriv_left[I_0], first_deriv_right[I_0]);
      }
      else if (aModeCombi.Size() == I_3)
      {
         Spline3D Interp3D(mPesCalcDef.GetmSplineType(), aCgCoords[I_0], aCgCoords[I_1], aCgCoords[I_2], aCgProperty, first_deriv_left[I_2], first_deriv_right[I_2]);

         Interp3D.GetYigDerivs(property_derivatives);

         GetYog(aFgProperty, aFgCoords[I_0], aFgCoords[I_1], aFgCoords[I_2], aCgCoords[I_0], aCgCoords[I_1], aCgCoords[I_2], aCgProperty, property_derivatives, mPesCalcDef.GetmSplineType(), first_deriv_left, first_deriv_right);
      }
      else
      {
         SplineND InterpND(mPesCalcDef.GetmSplineType(), aCgCoords, aCgProperty, I_1, first_deriv_left[aModeCombi.Size()-I_1], first_deriv_right[aModeCombi.Size()-I_1]);

         InterpND.GetYigDerivs(property_derivatives);

         GetYog(aFgProperty, aFgCoords, aCgCoords, aCgProperty, property_derivatives, I_1, mPesCalcDef.GetmSplineType(), first_deriv_left, first_deriv_right);
      }
   }
   // Do Shepard interpolation
   else if (shepard_int)
   {  
      if (gPesIoLevel > I_10)
      {
         Mout << std::endl << " Doing modified Shepard interpolation for mode combination " << aModeCombi.MCVec() << " and property " << mPesInfo.GetMultiLevelInfo().GetPropertyName(aBar) << std::endl;
      }

      In shepard_ord = mPesCalcDef.GetmPesShepardOrd();
      // Shepard order hard coded !!
      if (aBar > I_0)
      {
         shepard_ord = I_1; 
      }
      std::vector<In> mode_combi = aModeCombi.MCVec();

      if (aShepardDerivs.size() == I_0)
      {
         shepard_ord = I_0;
      }
      if (aShepardDerivs.size() == mode_combi.size())
      {
         shepard_ord = I_1;
      }

      Shepard DoMsi(shepard_ord, mode_combi);

      In oldsize = aCgProperty.Size();
      In newsize = oldsize + aCgPropertyGhosts.Size();
      aCgProperty.SetNewSize(newsize, true);
      for (In j = I_0; j < aShepardDerivs.size(); j++)
      {
         aShepardDerivs[j].SetNewSize(newsize, true);
      }
      for (In j = I_0; j < aCgCoordsUncompressed.size(); j++)
      {
         aCgCoordsUncompressed[j].SetNewSize(newsize, true);
      }
      for (In ind = I_0; ind < aCgPropertyGhosts.Size(); ind++)
      {
         aCgProperty[oldsize + ind] = aCgPropertyGhosts[ind];
         for (In j = I_0; j < aShepardDerivs.size(); j++)
         {
            aShepardDerivs[j][oldsize + ind] = aShepardDerivsGhosts[ind][j];
         }
         for (In j = I_0; j < aCgCoordsUncompressed.size(); j++)
         {
            aCgCoordsUncompressed[j][oldsize + ind] = aCgCoordsGhosts[ind][j];
         }
      }
      
      // For each direction seach the minimum distance between two calculated points: later used in the aFgSigma calculation since the grid is constructed as direct product it is equivalent to find the minimum distances between the generatos
      MidasVector minimum(aModeCombi.Size());
      for (In k = I_0; k < aModeCombi.Size(); k++)
      {
         Nb values = std::fabs(aCgCoords[k][I_0]);  // initialize
         for (In j = I_1; j < aCgCoords[k].Size(); j++)
         {
            if (std::fabs(aCgCoords[k][j] - aCgCoords[k][j - I_1]) < values)
            {
               values = std::fabs(aCgCoords[k][j] - aCgCoords[k][j - I_1]);
            }
         }
         minimum[k] = values;
      }
     
      // Return the interpolated points aFgCoords with corresponding energy/property value aFgProperty along with a modified variance aFgSigma
      DoMsi.GetYogNonDp(aFgProperty, aFgCoords, aFgSigma, aCgCoordsUncompressed, aCgProperty, aShepardDerivs, minimum, oldsize);
   }
   // No interpolation type set
   else
   {
      MIDASERROR("No interpolation type set.");
   }
}

/**
 * As to not waste information we get the derivatives on the axis so that our shepard interpolation gets even better!
 * During an ADGA construction of the potential it would be a shame not to use the all the derivatives info collected on the surfaces with lower dimensionality. To do that we have to had to the direct product of points retrived so far the info stored previously. 
**/
std::tuple<std::vector<MidasVector>, MidasVector, std::vector<MidasVector>> DoInterpolation::GetLowerMCDerInfo
   (  const ModeCombi& arMt
   ,  const Derivatives& arDerivatives
   ,  const std::string& aName
   ,  const In& aIbar
   ,  const GridType& arGpFr
   ,  const ItGridHandler* aIterGrid
   ,  const CalculationList& mCalculationList
   )  const
{
   // Note that aCgCoords was given as compact form of a direct product: n vectors with dimension v1, v2, ..,vn corresponding to a aCgProperty of dimension v1*v2*...*vn. On the other side, cg_bar_coords_ghosts is given explicitely n point vectors corresponding to a n size aCgProperty vector.

   std::vector<MidasVector> cg_bar_coords_ghosts;
   MidasVector cg_bar_property_ghosts;
   std::vector<MidasVector> cg_bar_derivs_ghosts;

   std::vector<std::vector<In> > in_xig;
   FillXigGhosts(arMt, in_xig, aIterGrid);

   vector<Nb> der_cont;
   map<In,In> der_cont_handler;
   arDerivatives.GetDerivatives(aName, der_cont, der_cont_handler);
   vector<In> mc = arMt.MCVec();

   In but = In(std::pow(C_2, mPesCalcDef.GetmPesIterMax() + I_1 + In(mPesCalcDef.GetmPesItGridExpScalFact()))/I_2);

   for (In i_ghost = I_0; i_ghost < in_xig.size(); i_ghost++)
   {
      if (gPesIoLevel > I_14)
      {
         Mout << "A ghost: " << in_xig[i_ghost];
      }
      // with the in_xig and the mc I try to reconstruct the calculation codes:
      string code_long;
      code_long+='*';
      
      for (In i=I_0; i<arMt.Size(); i++)
      {
         code_long+='#';
         code_long+=std::to_string(mc[i]+I_1);
         code_long+='_';

         In top = in_xig[i_ghost][i];
         std::string string2;
         string2 = PesFuncs::SimplifyFraction(top, but);

         code_long += string2;
      }
      code_long += '*';

      std::string calc_code;

      //mPesInfo.SetCodeFromString(calc_code,code_long);
      SetCodeFromString(calc_code,code_long);
      //Mout << " is here "<< code_long << "  calc code " << calc_code << endl;

      MidasVector q_grad_in(mPesInfo.GetMolecule().GetNoOfVibs());
      MidasMatrix q_hess_in(mPesInfo.GetMolecule().GetNoOfVibs());
      MidasVector q_point_in(mPesInfo.GetMolecule().GetNoOfVibs());
      GradHessBar(mCalculationList, arDerivatives, arGpFr, aName, arMt.Size(), code_long, q_point_in, q_grad_in, q_hess_in);

      // Construct the full set of information:
      In n_term = arMt.Size();
      // a shepard derivative has dimension aModeCombi.size  + aModeCombi.size + aModeCombi.size-1 + aModeCombi.size-2 ecc,
      if (mPesCalcDef.GetmPesShepardOrd() > I_1 && aIbar == I_0)
      {
         for (In kk = I_1; kk <= arMt.Size(); kk++)
         {
            n_term += kk;
         }
      }
      
      //Mout << "in the gradient/hessian there are " <<n_term << " interesting terms" << endl;
      MidasVector a_shepard_deriv(n_term,C_0);

      MidasVector a_q_ghost(arMt.Size());
      for (In i=I_0; i<arMt.Size(); i++)   // save the point coordinates
      {
         a_q_ghost[i]=q_point_in[mc[i]];
      }

      In shep_term=I_0;
      for (In i=I_0; i<arMt.Size(); i++)   // save the gradient first
      {
         a_shepard_deriv[shep_term]=q_grad_in[mc[i]];
         shep_term++;
      }

      if (mPesCalcDef.GetmPesShepardOrd() > I_1 && aIbar == I_0)
      {
         for (In i=I_0; i<arMt.Size(); i++)
         {
            for (In j=i; j<arMt.Size(); j++)
            {
               a_shepard_deriv[shep_term]=q_hess_in[mc[i]][mc[j]];
               shep_term++;
            }
         }
      }

      cg_bar_coords_ghosts.push_back(a_q_ghost);
      cg_bar_derivs_ghosts.push_back(a_shepard_deriv);

      //Done
      if (gPesIoLevel > I_14)
      {
         Mout << " Full set of info" << std::endl;
         for (In k = I_0; k < a_q_ghost.Size(); k++)
         {
            Mout << a_q_ghost[k] << "  ";
         }
         Mout  << C_0 << a_shepard_deriv << std::endl << std::endl;
      }
   }
      
   if (gPesIoLevel > I_14)
   {
      Mout << " Done creating the ghost grid " << std::endl;
   }

   // Since we are on the axes, the aCgProperty are zero per definition,  
   cg_bar_property_ghosts.SetNewSize(cg_bar_coords_ghosts.size());
   for (In i = I_0; i < cg_bar_property_ghosts.Size(); i++)
   {
      // Here is hardcoded but it can be modified to add flexibility (?)
      cg_bar_property_ghosts[i] = C_0;     
   }

   return std::make_tuple(cg_bar_coords_ghosts, cg_bar_property_ghosts, cg_bar_derivs_ghosts);
}

/**
 *
 **/
void DoInterpolation::FillXigGhosts
   (  const ModeCombi& arModeCombi
   ,  std::vector<std::vector<In> >& arInmXig
   ,  const ItGridHandler* aIterGrid
   )  const
{
   In add = arModeCombi.Address();
   aIterGrid->FillXigGhosts(add, arInmXig);

   return;
}

/**
 *
 **/
void DoInterpolation::OutputCoreGrid
   (  const std::vector<MidasVector>& aCgCoords
   ,  const MidasVector& aCgProperty
   ,  const In& aGPs
   ,  const In& aDers
   ,  const std::vector<MidasVector>& aShepardDerivs
   )  const
{
   Mout << std::endl;
   Mout << " Ordered set of CG points and values (derivatives) " << std::endl << std::endl;
   In n_dim = aCgCoords.size();
   MidasVector vec_tmp(n_dim,C_0);
   for (In i_g = I_0; i_g < aGPs; i_g++)
   {
      In curr_dim = I_1;
      In curr_index;
      for (In i_dim = n_dim - I_1; i_dim >= I_0; i_dim--)
      {
         curr_index = (i_g%(curr_dim*aCgCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim] = aCgCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCgCoords[i_dim].Size();
      }
      for (In i_p = I_0; i_p < aCgCoords.size(); i_p++)
      {
         Mout << vec_tmp[i_p] << "\t";
      }
      Mout << aCgProperty[i_g];
      if (aDers > I_0)
      {
         for (In i_n = I_0; i_n < aDers; i_n++)
         {
            Mout << "  " << aShepardDerivs[i_n][i_g];
         }
         Mout << std::endl;
      }
      else
      {
         Mout << std::endl;
      }
   }
}

/**
 *
 **/
void DoInterpolation::GradHessBar
   (  const CalculationList& mCalculationList
   ,  const Derivatives& arDerivatives
   ,  const GridType& arGpFr
   ,  const std::string& aProperty
   ,  const In& aDim
   ,  const std::string& aCalcCode
   ,  MidasVector& arQvec
   ,  MidasVector& arGradVec
   ,  MidasMatrix& arHessMatr
   )  const
{
   vector<Nb> der_cont;
   map<In,In> der_cont_handler;

   arDerivatives.GetDerivatives(aProperty, der_cont, der_cont_handler);

   In dim=aDim; // dimension of the Nss 
   In s_dim=aDim; // dimension of the sum
   In i_extrapolate_to = aDim;

   s_dim--; //starts at zero thus decreases the dimension 
   In *s_start; 
   s_start = new In[dim]; 
   for (In i = I_0; i < dim; i++)
   {
      *(s_start + i) = I_0;
   }
   In *s_end;
   s_end = new In[dim];
   for (In i = I_0; i < dim; i++)
   {
      *(s_end + i) = s_dim;
   }
   In *s_step;  
   s_step = new In[dim];
   for (In i = I_0; i < dim; i++)
   {
      *(s_step + i) = I_1;
   }
   Nss mode_range(dim, s_start, s_end, s_step);

   In n_combinations=I_0;
   while (mode_range.RunOrder())
      n_combinations++;
   In n_combis=mode_range.Dim();
   //MidasMatrix ModeIndexes(n_combinations,n_combis);
   In** ModeIndexes;
   ModeIndexes= new In* [n_combinations];
   //In ModeIndexes[ncomb];
   for (In i=0;i<n_combinations;i++)
      ModeIndexes[i] = new In [n_combis];
   //Mout << "EXTRAPOLA, NSS: " << n_combinations << " " << n_combis << endl;
   In i_comb=I_0;
   while (mode_range.RunOrder())
   {
      for (In i=I_0;i<n_combis;i++)
      {
          ModeIndexes[i_comb][i]=mode_range.VectorIndex(i+I_1);
         //Mout << ModeIndexes[i_comb][i] << " ";
      }
      i_comb++;
      //Mout << endl;
   }

   // Here constructs the matrix with the structures of the points that enter
   // the Vbar expression
   In dc_dim=I_0;
   for (In i=I_0; i<i_extrapolate_to; i++)
      dc_dim+=IntBinCoef(i_extrapolate_to,i);
   MidasMatrix points(dc_dim,i_extrapolate_to);
   points.Zero();
   //Mout << "Number of points needed: " << dc_dim << endl;
   In i_pos=I_0;
   for (In i=I_0; i<i_extrapolate_to; i++) // Includes the origin
      points[i_pos][i]=I_0;
   i_pos++;
   for (In i=I_1; i<i_extrapolate_to; i++)
   {
      // Sets the Nss 
      In dm=i; // dimension of the Nss 
      In s_dm=i_extrapolate_to; // dimension of the sum
      s_dm--; // starts at zero, so the dim--
      In *s_str;
      s_str = new In[dm];
      for (In ia = I_0; ia < dm; ia++)
      {
         *(s_str + ia) = I_0;
      }
      In *s_nd;
      s_nd = new In[dm];
      for (In ia = I_0; ia < dm; ia++)
      {
         *(s_nd + ia) = s_dm;
      }
      In *s_stp;
      s_stp = new In[dm];
      for (In ia = I_0; ia < dm; ia++)
      {
         *(s_stp + ia) =I_1;
      }
      Nss nss_points(dm,s_str,s_nd,s_stp);
      while ( nss_points.RunOrder() )
      {
         In j=I_0;
         In j_pos=I_0;
         //Mout << "Nss: ";
         //Mout << "Nss: ";
         //for (In ib=I_0; ib<i;ib++)
         //   Mout << nss_points.VectorIndex(ib+I_1) << " ";
         //Mout << endl;
         while (j < i_extrapolate_to )
         {
            if(j_pos < dm)
               if (j == nss_points.VectorIndex(j_pos+I_1) && j_pos <= i_extrapolate_to)
               {
                  points[i_pos][j]=I_1;
                  j_pos++;
               }
            j++;
         }
         i_pos++;
      }
      delete[] s_str;
      delete[] s_nd;
      delete[] s_stp;
   }
   
   if (gDebug)
   {
      Mout << "Matrix with coefficients for points to do be used in Vbar transformation: " << endl;
      for (In i=I_0; i<dc_dim; i++)
      {
         for (In j=I_0; j<i_extrapolate_to; j++)
            Mout << points[i][j] << " ";
         Mout << endl;
      }
   }


   In n_calcs = I_0;
   InVector mode_combi_sav;

   n_calcs++;

   InVector mode_combi;
   vector<string> k_vec_s;
   //mPesInfo.GetInfoFromString(aCalcCode, mode_combi, k_vec_s);
   GetInfoFromString(aCalcCode, mode_combi, k_vec_s);

   if (n_calcs==1)
   {
      mode_combi_sav.resize(mode_combi.size());
      for (In i=0; i<mode_combi.size(); i++) mode_combi_sav[i]=mode_combi[i];
      //Mout << " mode_combi_sav: " << mode_combi_sav << endl;
   }
   bool next=false;
   if (mode_combi.size()!=mode_combi_sav.size()) 
   {
      next=true;
   }
   if (!next)
   {
      for (In i=0; i<mode_combi.size(); i++)
      {
         if (mode_combi[i]!=mode_combi_sav[i])
         {
             next=true;
            break;
         }
      }
   }

   //construct k_vec & coordinate from k_vec_s
   InVector k_vec;
   In add=arGpFr.GridAddress(mode_combi);

   //MidasVector q_vec(mPesInfo.GetMolecule().GetNoOfVibs(),C_0);
   MidasVector ScalFactors(mPesInfo.GetMolecule().GetNoOfVibs(),C_0);
   //ScalFactors = mPesInfo.GetRScalFactVector();
   for (In i = 0; i < ScalFactors.Size(); ++i)
   {
      ScalFactors[i] = mPesInfo.GetScalingInfo().GetRScalFact(i);
   }
   for (In i_mode=0; i_mode<mode_combi.size(); i_mode++)
   {
      In den=arGpFr.GridPoints(add)[i_mode]/I_2;
      Nb lg_point=C_1/Nb(den);
      PesFuncs::StringMultiplication(k_vec_s[i_mode],den);
      k_vec.push_back(den);
      lg_point *=Nb(den);
      arQvec[mode_combi[i_mode]]=ScalFactors[mode_combi[i_mode]]*lg_point;
   }
   //    Mout << " for mode combi: " << mode_combi << " add: " << add << endl;
   //    Mout << " arQvec: " << arQvec << endl;

   //we compute the l.c.map 
   map<InVector,Nb> lc_map;
   vector<InVector> lin_com_vec;
   //put in the first vectors in the linear combination
   vector<In> mode_vec=mode_combi;

   for (In i=I_0;i<mode_vec.size();i++)
   {
      vector<In> loc_vec;
      for (In j=I_0;j<=i;j++)
         loc_vec.push_back(mode_vec[j]);
      lin_com_vec.push_back(loc_vec);
   }
   //generate permutations and put those in as well in a sorted way
   while ( next_permutation(mode_vec.begin(), mode_vec.end() ) )
   {
      for (In i=I_0;i<mode_vec.size();i++)
      {
         vector<In> loc_vec;
         for (In j=I_0;j<=i;j++)
            loc_vec.push_back(mode_vec[j]);
         lin_com_vec.push_back(loc_vec);
      }
      for (In i=I_0;i<lin_com_vec.size();i++)
         sort(lin_com_vec[i].begin(),lin_com_vec[i].end());
   }
   //find the unique elements
   set<InVector> unique;
   for (In i=I_0;i<lin_com_vec.size();i++)
      unique.insert(lin_com_vec[i]);
   //calculate the C-coefficient and the number of times a given
   //permutation is found in the original set
   Nb coef=C_0;
   for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
   {
      In cnt=I_0;
      for (In i=I_0;i<lin_com_vec.size();i++)
      {
         if ( (*Svi)==lin_com_vec[i] )
         {
            cnt++;
            Nb fac = C_1/NbFact(mode_vec.size());
            coef = fac*pow(-C_1,Nb(mode_vec.size()-lin_com_vec[i].size()))*NbBinCo(mode_vec.size(),lin_com_vec[i].size());
         }
      }
      lc_map[(*Svi)] = Nb(cnt)*coef;
   }
   //Mout << "Unique combinations & coefficients: ";
   //for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
   //   Mout << (*Svi) << " " << lc_map[(*Svi)] << "  ";
   //Mout << endl;

   // retrieve the hessian and the gradient from the points calculated which are needed
   // to convert Hessian,Gradient to Vbar counterparts
   In max_dc_dim = mPesInfo.GetMolecule().GetNoOfVibs()*(mPesInfo.GetMolecule().GetNoOfVibs()+I_1); // times the size to store Gradient+Hessian
   MidasMatrix grd_hess_points(dc_dim,max_dc_dim);
   //Mout << "CALCULATION CODE: " << calc_code << endl;
   // Treat the GS separated
   string s_num;
   {
      //retrieve the calculation number
      In i_pnt=I_0;
      string cc_pnt="*#0*";
      s_num=std::to_string(mCalculationList.GetCalcNrFromCode(cc_pnt));

      // retrieve information from Handler
      In position = der_cont_handler[InFromString(s_num)];
      //Mout << InFromString(s_num) << " position: " << position << endl;
      for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
      {
         grd_hess_points[i_pnt][ia]=der_cont[ia+position];
      }
      position+=mPesInfo.GetMolecule().GetNoOfVibs();
      In i_el=I_0;
      for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
      {
         for (In ib=I_0; ib<mPesInfo.GetMolecule().GetNoOfVibs(); ib++)
         {
            grd_hess_points[i_pnt][mPesInfo.GetMolecule().GetNoOfVibs()+i_el]=der_cont[i_el+position];
            i_el++;
         }
      }
   }
   // Go for the remaining points...
   for (In i_pnt = I_1; i_pnt < dc_dim; i_pnt++)
   {
      std::string cc_pnt = "*";
      for (In pnt_pos = I_0; pnt_pos < i_extrapolate_to; pnt_pos++)
      {
         if (points(i_pnt, pnt_pos) == I_1)
         {
            cc_pnt += '#' + std::to_string(mode_combi[pnt_pos] + I_1) + '_' + k_vec_s[pnt_pos];
         }
      }
      cc_pnt += "*";

      s_num=std::to_string(mCalculationList.GetCalcNrFromCode(cc_pnt));
      if(s_num!=std::to_string(-I_1))
      {
         //Mout << " for calc_code: " << cc_pnt << " calc_no: " << s_num << endl;
         // retrieve information from Handler
         In position = der_cont_handler[InFromString(s_num)];
         //Mout << InFromString(s_num) << " position: " << position << endl;
         for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
            grd_hess_points[i_pnt][ia]=der_cont[ia+position];
         position+=mPesInfo.GetMolecule().GetNoOfVibs();

         In i_el=I_0;
         for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
         {
            for (In ib=I_0; ib<mPesInfo.GetMolecule().GetNoOfVibs(); ib++)
            {
               grd_hess_points[i_pnt][mPesInfo.GetMolecule().GetNoOfVibs()+i_el]=der_cont[i_el+position];
               i_el++;
            }
         }
      }
   }
   
   //****
   // NOW ALL INFORMATION IS IN THE grd_hess_points matrix
   //****
   for (In i=I_0;i<n_combinations;i++) // here I run over the number of mode combinations
   {
      //Mout << "*** ";
      //for (In ia=I_0;ia<n_combis;ia++)
      //   Mout << mode_combi[ModeIndexes[i][ia]] << " ";
      //Mout << endl;
      InVector mc_piece;
      MidasVector q_vec_piece;
      vector<string> kvs_piece;
      for (In ia=I_0;ia<n_combis;ia++)
      {
         mc_piece.push_back(mode_combi[ModeIndexes[i][ia]]);
         for (In ib=I_0;ib<mode_combi.size();ib++)
         kvs_piece.push_back(k_vec_s[ModeIndexes[i][ia]]);
      }

      q_vec_piece.SetNewSize(mPesInfo.GetMolecule().GetNoOfVibs(),false);
      q_vec_piece.Zero();

      //Thus q_vec & q_vec_piece have the same size, and transfers info.
      for (In i=I_0; i<mc_piece.size();i++)
         q_vec_piece[mc_piece[i]]=arQvec[mc_piece[i]];

      // Plug in here machinery for Grad, Hessian Vbar transformation
      // 1.- Take the linear combinations and find the points from the
      // expansion point
      //
      //Mout << "Unique combinations & coefficients: ";
      //for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
      //{
      //         Mout << (*Svi) << " " << lc_map[(*Svi)] << "  ";
      //}
      //Mout << endl;
      //Mout << "expansion point: " << q_vec_piece << endl;
      MidasVector q_pnt(mPesInfo.GetMolecule().GetNoOfVibs());
      MidasVector q_sht(i_extrapolate_to); // same as q_pnt but in the shorter form
      arGradVec.Zero();
      arHessMatr.Zero();
      // Loop over the V to obtain the Vbar
      for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
      {
         q_pnt.Zero();
         q_sht.Zero();
         // calculates the point wanted for a given V
         for (In ib=I_0;ib<q_pnt.Size();ib++)
            for (In ia=I_0;ia<(*Svi).size();ia++)
            {
               if ( ib == (*Svi)[ia] )
               {
                  if ( q_vec_piece[ib]!=C_0 )
                  {
                     q_pnt[ib]=I_1;
                  }
               }
            }
         // constructs its shorter form
         In ia_cont=I_0;
         for (In ib=I_0;ib<q_pnt.Size();ib++)
            for (In ia=I_0;ia<mode_combi.size();ia++)
               if (ib == mode_combi[ia])
               {
                  q_sht[ia_cont]=q_pnt[ib];
                  ia_cont++;
               }
         // retrieve the gradient & hessian for that point
         //Mout << "q_vec for " << (*Svi) << ": " << q_pnt << endl;
         for (In ip=I_0;ip<points.Nrows();ip++)
         {
            // searches for that point in the container set before
            bool vctcoinc=true;
            for (In ic=I_0;ic<points.Ncols();ic++)
            {
               vctcoinc = vctcoinc && (points[ip][ic] == q_sht[ic]);
            }
            if ( vctcoinc )
            {
               //Mout << endl;
               // saves in a vector and matrix ONLY those elements which
               // cannot be zero, i.e. which differenciate with respect to
               // one of modes of the current mode_combi (in *Svi) 
               //
               // N.B. The elements are summed up so one obtains the Hbar
               // and GradVar.
               for (In ic=I_0;ic<mPesInfo.GetMolecule().GetNoOfVibs();ic++)
                  for (In isvi=I_0;isvi<(*Svi).size();isvi++)
                  {
                     if ( ic == (*Svi)[isvi] )
                         arGradVec[ic]+=Nb(lc_map[(*Svi)])*grd_hess_points[ip][ic]; //q_grad[ic]+=Nb(lc_map[(*Svi)])*grd_hess_points[ip][ic];
                  }
               In i_cont=I_0;
               for (In ic=I_0;ic<mPesInfo.GetMolecule().GetNoOfVibs();ic++)
                  for (In id=I_0;id<mPesInfo.GetMolecule().GetNoOfVibs();id++)
                  {
                     for (In isvi=I_0;isvi<(*Svi).size();isvi++)
                        for (In jsvi=I_0;jsvi<(*Svi).size();jsvi++)
                           if (ic == (*Svi)[isvi] )
                              if ( id == (*Svi)[jsvi] )
                                 arHessMatr[ic][id]+=Nb(lc_map[(*Svi)])*grd_hess_points[ip][i_cont+mPesInfo.GetMolecule().GetNoOfVibs()];
                  i_cont++;
                  }
            }
         }
      }
   }
}
