#ifndef MIDAS_PES_MULTILEVELINFO_H_INCLUDED
#define MIDAS_PES_MULTILEVELINFO_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"
#include "pes/PropertyInfo.h"

namespace midas
{
namespace pes
{

/**
 * Class for holding multilevel specific data for a pes multilevel.
 **/
class MultiLevelInfo
{
   private:

      //! Current PES multilevel.
      In mCurrentLevel = -I_1;

      //! Number of threads for current multilevel.
      Uin mNthreads = I_1;

      //! Name of current multilevel (e.g. "level_1")
      std::string mMultiLevelName = "";
      
      //!@{
      //! "Global" directories for level (available on all MPI processes through Network File System (NFS)).
      //! Path to main directory for current level.
      std::string mMainDir = "";

      //! Current setup dir.
      std::string mSetupDir = "";

      //! Current savedir.
      std::string mSaveIoDir = "";
      
      //! Current analysis dir.
      std::string mAnalysisDir = "";
      //!@}
      
      //! Property info for the current level.
      PropertyInfo mPropertyInfo;

   public:

      //! Default constructor.
      MultiLevelInfo() = default;
      
      //!@{
      //! Deleted copy constructor and copy assignment but enable move constructor
      MultiLevelInfo(const MultiLevelInfo&)            = delete;
      MultiLevelInfo& operator=(const MultiLevelInfo&) = delete;
      MultiLevelInfo(MultiLevelInfo&&) = default;
      //!@}
      
      //! Update PesLevelInfo for next multilevel.
      void Update(const PesCalcDef& aPesCalcDef, const In& aLevel, const std::string& aSubDir, const bool& aLinear);
      
      //! Get current level.
      In CurrentLevel() const;
      
      //! Find the number of threads for current multilevel.
      Uin FindNthreads(In arCurrentLevel, const PesCalcDef& arPesCalcDef) const;
      
      //! Get the current number of threads.
      Uin Nthreads() const;
      
      //! Get name of current multilevel
      const std::string& MultiLevelName() const;

      //! Get path for maindir.
      const std::string& MainDir() const;
      
      //! Get path for setupdir.
      const std::string& SetupDir() const;
      
      //! Get path for savedir.
      const std::string& SaveIoDir() const;

      //! Get path for analysis dir.
      const std::string& AnalysisDir() const;

      //! Get prop_info
      const PropertyInfo& PropInfo() const;

      //! Get property number from property name/descriptor. NB: Used in extrap/derivative code, so might go away at some point.
      In GetPropertyNumber(const std::string& aDescriptor) const;

      //! Get property descriptor from property number. NB: Used in extrap/derivative code, so might go away at some point.
      const std::string& GetPropertyName(In aI) const; 

      //! Get number of properties.
      In GetNoOfProps() const;
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_MULTILEVELINFO_H_INCLUDED */
