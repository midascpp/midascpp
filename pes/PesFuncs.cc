/**
************************************************************************
*
* @file                PesFuncs.cc
*
* Created:             
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/Math.h"
#include "pes/PesFuncs.h"

#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "pes/GridType.h"

namespace PesFuncs
{

/**
 * Simplify a fraction by finding the greatest common divisor
 * @param arTop nominator
 * @param arBottom denominator
 * @return the simplfied fraction as string
 **/
std::string SimplifyFraction( In arTop
                            , In arBottom
                            )
{
   if (arBottom == I_0)
   {
      MIDASERROR(" Zero in denominator in SimplifyFraction ");
   }

   // This check is UGLY but ensures backwards compatibility; in the previous
   // implementation the fraction wasn't simplified if the numerator (arTop)
   // was 0, so we only do it if != 0.
   if (arTop != I_0)
   {
      auto gcd = midas::util::GreatestCommonDivisor(arBottom, arTop);
      arTop    /= gcd;
      arBottom /= gcd;
   }

   return std::to_string(arTop) + "/" + std::to_string(arBottom);
}

/**
 * Multiplies a string fraction with a In and saves result in In
 * @param arFrac fraction to be multiplied onto integer
 * @param arI (INPUT/OUTPUT) on input integer to be multipled, on output the result of the multiplication
 **/
void StringMultiplication( const std::string& arFrac
                         , In& arI
                         )
{
   In top = I_1;
   In bottom = I_1;
   if(arFrac.find("/") != string::npos) // we have a fraction
   {
      top = midas::util::FromString<In>(arFrac.substr(0,arFrac.find("/")));
      bottom = midas::util::FromString<In>(arFrac.substr(arFrac.find("/")+1));
   }
   else // we have a number (In)
   {
      top = midas::util::FromString<In>(arFrac);
   }
   arI = arI*top/bottom;
}

/**
 * Multiplies a string fraction with a Nb and returns result in Nb
 * @param arFrac the fraction to multiply with the number
 * @param arN (INPUT/OUTPUT) on input the number to be multiplied, on output the result of the multiplication
 **/
void StringMultiplication( const std::string& arFrac
                         , Nb& arN
                         )
{
   In top = I_1;
   In bottom = I_1;
   if(arFrac.find("/") != string::npos) // we have a fraction
   {
      top = midas::util::FromString<In>(arFrac.substr(0,arFrac.find("/")));
      bottom = midas::util::FromString<In>(arFrac.substr(arFrac.find("/")+1));
   }
   else // we have a number (In)
   {
      top = midas::util::FromString<In>(arFrac);
   }
   arN = arN*Nb(top)/Nb(bottom);
}

/**
 * Multiplies two strings, and simplify the result
 * @param aStr1 first string 
 * @param aStr2 second string
 * @return result of multiplication
 * */
std::string StringMultiplication( const std::string& aStr1
                                , const std::string& aStr2
                                )
{
   In nom1 = 1;
   In nom2 = 1;
   In den1 = 1;
   In den2 = 1;
   if(aStr1.find("/")!=string::npos) // we have a fraction
   {
      nom1 = midas::util::FromString<In>(aStr1.substr(0,aStr1.find("/")));
      den1 = midas::util::FromString<In>(aStr1.substr(aStr1.find("/")+1));
   }
   else // we have a number (In)
   {
      nom1 = midas::util::FromString<In>(aStr1);
   }
   if(aStr2.find("/")!=string::npos) // we have a fraction
   {
      nom2 = midas::util::FromString<In>(aStr2.substr(0,aStr2.find("/")));
      den2 = midas::util::FromString<In>(aStr2.substr(aStr2.find("/")+1));
   }
   else // we have a number (In)
   {
      nom2 = midas::util::FromString<In>(aStr2);
   }
   return SimplifyFraction(nom1*nom2,den1*den2);
}

/**
 * return nominator of string fraction
 * @param aStr string to return nominator for
 * @result the nominator of string fraction
 **/
std::string StringNominator(const std::string& aStr)
{
   auto div_pos = aStr.find("/");
   if(div_pos != string::npos) // we have a fraction
   {
      return aStr.substr(0,div_pos);
   }
   else // we have a number (In)
   {
      return aStr;
   }
}

/**
 * return denominator of string fractions
 * @param aStr fraction to find denomiator for
 * @result denominator of string fraction
 **/
std::string StringDenominator(const std::string& aStr)
{
   auto div_pos = aStr.find("/");
   if(div_pos != string::npos) // we have a fraction
   {
      return aStr.substr(div_pos + 1);
   }
   else // we have a number (In)
   {                 
      return std::string("1");
   }
}

/***************************************************************************//**
 * Routine for polynomial interpolation and extrapolation.
 *
 * @note
 *    Taken from Numerical Recipes in c++, pag. 112 (page 119 in the third
 *    edition)
 *
 * @param[in] xa
 *    x-values for the points used for interpolation.
 * @param[in] ya
 *    y-values (corresponding to the x-values) for the points used for
 *    interpolation.
 * @param[in] x
 *    The x-value for which to evaluate the function.
 * @param[out] y
 *    Resulting interpolated value.
 * @param[out] dy
 *    Error estimate.
 ******************************************************************************/
void polint
   (  const MidasVector& xa
   ,  const MidasVector& ya
   ,  const Nb x
   ,  Nb& y
   ,  Nb& dy
   )
{
   //value in y, error estimate in dy
   In i,m,ns=0;
   Nb den,dif,dift,ho,hp,w;

   In n=xa.Size();
   MidasVector c(n,C_0);
   MidasVector d(n,C_0);
   dif=fabs(x-xa[0]);
   for (i=0; i<n; i++)
   {
      if ((dift=fabs(x-xa[i]))<dif)
      {
         ns=i;
         dif=dift;
      }
      c[i]=ya[i];
      d[i]=ya[i];
   }
   y=ya[ns--];
   for (m=1; m<n; m++)
   {
      for (i=0; i<n-m; i++)
      {
         ho=xa[i]-x;
         hp=xa[i+m]-x;
         w=c[i+1]-d[i];
         if ((den=ho-hp)==C_0) MIDASERROR("Error in routine polint");
         //this error can occur only if two input xa's are identical
         den=w/den;
         d[i]=hp*den;
         c[i]=ho*den;
      }
      y+=(dy=(2*(ns+1) < (n-m) ? c[ns+1] : d[ns--]));
   }
}

/***************************************************************************//**
 * Finds lower bound of aXog, i.e. the index of the left side of the interval
 * bracketing the input point aXog.
 * Uses the input guess in order to be smart if called multiple times with
 * target values that are close. See Numerical Recipes for further explanation.
 * 
 * @note
 *    subroutine hunt (taken from Numerical Recipes in c++, pag. 121; 3rd
 *    edition page 116)
 *
 * @param[in] arXig
 *    A vector of monotonic (ascending/descending) values. (Must have been
 *    sorted beforehand.)
 * @param[in] aXog
 *    The number for which to find a lower bound in the vector arXig.
 * @param[in,out] arLowerBound
 *    On input use this as a guess for the lower bound. On return holds the
 *    index for the lower bound of aXog.
 ******************************************************************************/
void hunt(const MidasVector& arXig, const Nb aXog, In& arLowerBound)
{
   In jm;
   In j_high;
   In inc;
   In n = arXig.Size();
   bool is_ascendent_order = (arXig[n-I_1] >= arXig[I_0]);
   if (arLowerBound < I_0 || arLowerBound > n-I_1) // Input guess not useful, Go immediately to bisection
   {
      arLowerBound  =-I_1;
      j_high = n;
   } 
   else 
   {
      inc = I_1;                                                    // Set the hunting element
      if ((aXog >= arXig[arLowerBound]) == is_ascendent_order)             // Hunt up
      {
         if (arLowerBound == n-I_1) return;
         j_high=arLowerBound+I_1;
         while ((aXog >= arXig[j_high]) == is_ascendent_order)            // Not done hunting
         {
            arLowerBound = j_high;
            inc   += inc;
            j_high      = arLowerBound + inc;
            if (j_high > n-I_1) 
            {
               j_high = n;
               break;
            }
         }
      }  
      else 
      {
         if (arLowerBound == I_0) 
         {
            arLowerBound=-I_1;
            return;
         }
         j_high=arLowerBound--;
         while ((aXog < arXig[arLowerBound]) == is_ascendent_order) 
         {
            j_high=arLowerBound;
            inc <<= I_1;
            if (inc >= j_high) 
            {
               arLowerBound=-I_1;
               break;
            }
            else arLowerBound=j_high-inc;
         }
      }
   }
   while (j_high - arLowerBound != I_1) 
   {
      jm=(j_high + arLowerBound) >> I_1;
      if ((aXog >= arXig[jm]) == is_ascendent_order)
      arLowerBound=jm;
      else
      j_high=jm;
   }
   if (aXog == arXig[n-I_1]) arLowerBound = n-I_2;
   if (aXog == arXig[I_0]) arLowerBound = I_0;
}

/**
* subroutine locate (taken from Numerical Recipes in c++, pag. 120)
*   on output arLowerBound is the index of the left side of the intervael braketing
*   the input point aXog
**/
void locate(const MidasVector& arXig, const Nb aXog, In& arLowerBound)
{
        
   In n = arXig.Size();
   In j_lower=-I_1;
   In j_upper=n;
   In jm;
   bool is_ascendent_order = (arXig[n-I_1] >= arXig[I_0]);
        
   while (j_upper-j_lower > I_1) 
   {
      jm = (j_upper+j_lower) >> I_1;
      if ((aXog >= arXig[jm]) == is_ascendent_order)
         j_lower = jm;
      else
         j_upper = jm;
   }
   if (aXog == arXig[I_0]) arLowerBound=I_0;
   else if (aXog == arXig[n-I_1]) arLowerBound=n-I_2;
   else arLowerBound=j_lower;
}

/**
 * setup GridType and ModeCom
 * @param arIMode (INPUT) Mode to run for grid setup (0 := do some more output, for first call to GridSetup)
 * @param ModeCom (INPUT/OUTPUT) mode combination range, on exit also contains MC's for extended grid settings
 * @param arActualMaxDimMc (INPUT/OUTPUT) max dim of MCR, on output contains the max for also extended grid settings
 * @param arGpFr (OUTPUT) on output contains grid types for all MC's
 * @param arPesInfo (INPUT) Information on the Pes
 **/
void GridSetup
   (  const In arIMode
   ,  ModeCombiOpRange& ModeCom
   ,  In& arActualMaxDimMc
   ,  GridType& arGpFr
   ,  const PesInfo& arPesInfo
   )
{
   if (arIMode == I_0 && gPesIoLevel > I_10)
   {
      Mout << " Mode Combination Range from default input is " << gPesCalcDef.GetmPesNumMCR() << std::endl;
      Mout << std::endl;
      Mout << " ** THE GRID WILL BE CONSTRUCTED AS ** (modes are numbered from 0,1...) \n" << std::endl;
   }

   bool Has_MCR_been_increased = false;
   //if Extended input has been given convert it into vectors to be used later on 
   //this is actually some input processing.
   
   // ADGA STUFF
   if (gPesCalcDef.GetmPesAdga())
   {
       if (gPesIoLevel > I_10)
       {
          Mout << " Adga grid expansion factor: " << gPesCalcDef.GetmPesItGridExpScalFact() << std::endl;
       }
       //check if multiple of two
       if (gPesCalcDef.GetmPesItGridExpScalFact() == I_0)
       {
         MidasWarning(" Setting the grid expansion factor to zero restricts the ADGA from expanding the potential grid beyond the starting points! ");
       }
       if (gPesCalcDef.GetmPesItGridExpScalFact() > I_29 - gPesCalcDef.GetmPesIterMax())
       {
         MIDASERROR(" You have set a non-legal value for the grid expansion factor! ");
       }
   }

   // GRID + ADGA EXTENDED GRID STUFF
   std::vector< std::vector<In> > extended_mode_combis;
   std::vector< std::vector<In> > extended_grid;
   std::vector< std::vector<std::string> > extended_frac;
   for(auto& elem : gPesCalcDef.GetExtendedGridSettings())
   {
      extended_mode_combis.emplace_back(std::get<0>(elem)); // first index is mode combi
      extended_grid.emplace_back(std::get<1>(elem)); // second index is number of grid points
      extended_frac.emplace_back(std::get<2>(elem)); // third index is fractions
      if (std::get<0>(elem).size() > arActualMaxDimMc)
      {
         arActualMaxDimMc = std::get<0>(elem).size();
         Has_MCR_been_increased = true;
      }
   }
   
   if (  Has_MCR_been_increased
      && gPesIoLevel > I_14
      && arIMode == I_0
      ) 
   {
      Mout << " Mode combination range has been increased to include up to " << arActualMaxDimMc << "-mode couplings per request " << std::endl;
   }
  
   // Add extended ModeCombis to MCR.
   {
      std::vector<ModeCombi> v_mc_insert;
      v_mc_insert.reserve(extended_mode_combis.size());
      for(const auto& mc: extended_mode_combis)
      {
         v_mc_insert.emplace_back(mc, -I_1);
      }
      ModeCom.Insert(std::move(v_mc_insert));
   }


   // Open file to write boundaries
   std::string data_file = arPesInfo.GetMultiLevelInfo().AnalysisDir() + "/" + "one_mode_grids.mbounds";
   ofstream out_file;
   if (!gPesCalcDef.GetmPesAdga() && arIMode == I_0)
   {
      out_file.open(data_file.c_str(), ios::out);
      if (!out_file)
      {
         MIDASERROR(" cannot open file to write ");
      }
      out_file.setf(ios::scientific);
      out_file.setf(ios::uppercase);
      out_file.precision(I_22);
   }
   out_file << "# Mode  Left PES Bound  Right PES Bound " << std::endl;

   vector<In> ModeTest;
   // loop over mode combis and setup grid types
   for(const auto& mt: ModeCom)
   {
      ModeTest = mt.MCVec();

      //    Has this MC been given as extended input as well ? Loop through vector of MC from extended input. If there, 
      //    find adress for Grid and Frac information for this MC
      int j = 0;
      bool found = false;
      for(auto& mode_combi : extended_mode_combis)
      {
         found = (ModeTest == mode_combi);
         if (found) break;
         ++j;
      }

      //    If found = true then we have a match and ModeTest is included in Extended input. The address in vector is j.
      //    Else use the grid info from default input
      vector<In> GP(mt.Size());
      vector<string> FR(I_2*mt.Size());
      if (found) // EXTENDED
      {
         if (extended_grid[j].size() != mt.Size())     MIDASERROR(" Wrong dimension in grid vector from Extended input ");
         if (extended_frac[j].size() != I_2*mt.Size()) MIDASERROR(" Wrong dimension in Frac vector from Extended input ");
         GP = extended_grid[j];
         FR = extended_frac[j];
      }
      else
      {
         if (!gPesCalcDef.GetmPesAdga())
         { // GRID NOT EXTENDED
            for (In k=I_0;k<mt.Size();++k)
               GP[k] = gPesCalcDef.GetmPesGridInMCLevel()[mt.Size()-I_1];
            for (In k=I_0;k<I_2*mt.Size();++k)
               FR[k] = gPesCalcDef.GetmPesFracInMCLevel()[mt.Size()-I_1];
         }
         else
         { // ADGA NOT EXTENDED
            if (mt.Size() == I_1)
            {
               //the expansion factor is taken from input
               string exp_fact = "1/"+std::to_string(In(std::pow(C_2, In(gPesCalcDef.GetmPesItGridExpScalFact()))));
               for (In k = I_0; k < I_2*mt.Size(); ++k)
               {
                  FR[k] = exp_fact;
               }
               GP[I_0] = In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() + I_1 + In(gPesCalcDef.GetmPesItGridExpScalFact())));
            }
            //at the beginning of the Adga procedure, set dummy values for the higher mode couplings
            else
            {
               string frac = "1";
               for (In k = I_0; k < I_2*mt.Size(); ++k)
               {
                  FR[k] = frac;
               }
               for (In k = I_0; k < mt.Size(); ++k)
               {
                  GP[k] = In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() + I_1 + In(gPesCalcDef.GetmPesItGridExpScalFact())));
               }
            }
         }
      }
      //We now have the number of grid points for each mode in the MC and the Fractions for each mode for the same MC in GP and FR, respectively
      //Now make pair of those two and see if this GridType is already known
      In adresse = arGpFr.AddGridType(mt.MCVec(), GP, FR);
      
      //Now, set address in ModeCombi
      ModeCombi& abe = const_cast<ModeCombi&>(mt);
      abe.AssignAddress(adresse);
      bool extrap = false;
      // Set the flag for the extrapolation in requested
      if (gPesCalcDef.GetmPesDoExtrap() && ModeTest.size() == gPesCalcDef.GetmExtrapolateTo())
      {
         extrap = true;
      }
      // Initialize everything to be extrapolated
      if (gPesCalcDef.GetmPesAdga() && ModeTest.size() > I_1 &&  arPesInfo.GetmUseDerivatives() && gPesCalcDef.GetMaxMLRuns() == I_0)
      {
         extrap = true;
      }
      
      abe.SetToBeExtrap(extrap);
      if (gPesCalcDef.GetmPesScreenModeCombi() && mt.Size() == I_2)
      {
         // Do screening of mode couplings
         ModeCoupling mode_couplings(arPesInfo.GetMolecule());
         
         // Set screening method and evaluate the coupling strength
         mode_couplings.SetMethod(gPesCalcDef.GetmPesScreenMethod());
         mode_couplings.SetVibNum(gPesCalcDef.GetmPesVibNumVec());
         mode_couplings.EvalCouplings();
         
         std::vector<In> mode_combi = mt.MCVec();
         if (mode_couplings.ScreenModeCombi(mode_combi))
         {
            abe.SetToBeScreened(true);
         }
      }


/*    ** NOTE **
      Note that the above procedure includes the empty MC vector () and assigns a GridType to this (this will be GridType = I_0). 
      This is somehow 'overstoring' of data since no gridpoints and so on are used for this gridType as this is actually the reference.
      However, in the loop structure later on this it is convient to have it like this where simply the reference gets its own GridType.
      ** NOTE **
*/

      //do some printout and check the consistency of the fractioning
      if (gPesIoLevel > I_14)
      {
         Mout << " Mode combination: " << mt << std::endl;
         Mout << "  Address is:            " << mt.Address() << std::endl;
         Mout << "  Doing Extrapolation:   " << mt.IsToBeExtrap() << std::endl;
         Mout << "  Doing Screening:       " << mt.IsToBeScreened() << std::endl;

         if (  arIMode == I_0
            && mt.Size() != I_0
            )
         {
            Mout << "  Number of grid points: " << arGpFr.GridPoints(mt.Address()) << std::endl;
            Mout << "  Fractioning:           ";
            arGpFr.PrintSpecificFraction(mt.Address()); 
            arGpFr.CheckFractions(mt.Address(),mt.MCVec());
         }
         Mout << std::endl;
      }

      //dump out infos on grid boundaries
      // GRID STUFF
      if (mt.Size() == I_1 && !gPesCalcDef.GetmPesAdga() && arIMode == I_0)
      {
         std::vector<In> curr_mode=mt.MCVec();
         In address = mt.Address();
         const auto& gridpoints_vec = arGpFr.GridPoints(address);
         const auto& fractions_vec = arGpFr.Fractions(address);
         if (gridpoints_vec.size()!=1) MIDASERROR(" Something wrong in grid defs. ");
         Nb r_b =  arPesInfo.GetScalingInfo().GetRScalFact(curr_mode[0]);
         Nb l_b = -arPesInfo.GetScalingInfo().GetLScalFact(curr_mode[0]);
         if (gPesIoLevel > I_8)
         {
            Mout << " For mode: " << curr_mode << " nr. of points: " << gridpoints_vec << " string of fractions: ";
            for (auto& elem : fractions_vec)
            {
               Mout << elem;
            }
            Mout << std::endl;
         }
         PesFuncs::StringMultiplication(fractions_vec[0], l_b);
         PesFuncs::StringMultiplication(fractions_vec[1], r_b);
         l_b *= std::sqrt(C_FAMU);
         r_b *= std::sqrt(C_FAMU);
         out_file << arPesInfo.GetMolecule().GetModeLabel(curr_mode[I_0]) << " " 
                  << l_b << " " << r_b << std::endl;
      }
   }
   
   // GRID STUFF
   if (!gPesCalcDef.GetmPesAdga() && arIMode==0)
   {
      out_file.close();
   }
   // 

   if ((gPesIoLevel > I_14) && (arIMode == I_0))
   {
      Mout << std::endl;
   }
}


void ReadDerInfo
   ( std::map<std::string, MidasVector>& grad_map
   , std::map<std::string, MidasMatrix>& hess_map
   , std::vector<Nb>& der_cont 
   , std::map<In, In>& der_cont_handler
   , In& order 
   , const bool save_in_cont
   , const std::string& der_addr
   , const std::string& der_file_name
   , In aNderiv
   )
{
   
   // Setting a map between the Number of Calc and the Code + Address
   std::map<std::string, std::pair<In, std::vector<In> > > code_addr_map;
   
   // fill out map
   ifstream ifs(der_addr.c_str());
   std::string line;
   In cont_size = I_0;
   In count = I_0;
   while (getline(ifs, line)) 
   {
      istringstream iss(line);
      std::string tmp_code;
      In tmp_number;
      std::vector<In> tmp_addr;
      iss >> tmp_number;
      iss >> tmp_code;
      In i_addr;
      while (iss >> i_addr)
      {
         tmp_addr.push_back(i_addr);
      }
      if (tmp_addr.size() == I_0)
      {
         MIDASERROR("No addresses found");
      }
      if (count == I_1) 
      {
         cont_size = tmp_addr[I_0];
      }
      count++;
      code_addr_map.insert(std::make_pair(tmp_code, std::make_pair(tmp_number, tmp_addr)));
   }
   cont_size *= count;

   if (gPesIoLevel > I_4)
   {
      Mout << " Finding derivatives for " << cont_size << " single points" << std::endl;
   }

   DataCont deriv_cont;
   deriv_cont.SaveUponDecon(true);
   deriv_cont.GetFromExistingOnDisc(cont_size, der_file_name);
   deriv_cont.NewLabel(der_file_name);
   
   if (gDebug && !deriv_cont.ChangeStorageTo("InMem",true)) 
   {
      MIDASERROR("Expected to find derivatives!!");
   }
 
   for (auto const& code_addr : code_addr_map)
   {
      std::string calc_code = code_addr.first;
      In calc_no = code_addr.second.first; 

      MidasVector gradient(aNderiv, C_0);
      MidasMatrix hessian(aNderiv, C_0);

      // find address of gradient
      In addr = code_addr.second.second[I_0];
      deriv_cont.DataIo (  IO_GET, gradient, aNderiv, addr);
      order = I_1;
      
      if (!save_in_cont)
      {
         grad_map.insert(std::make_pair(calc_code, gradient));
      }

      if((code_addr.second.second).size()>I_1)
      {
         order = I_2;
         /// also get hessian
         addr = (code_addr.second.second)[I_1];
         deriv_cont.DataIo
            (  IO_GET, hessian
            ,  aNderiv*aNderiv
            ,  aNderiv, aNderiv
            ,  addr
            );
        
         if (!save_in_cont)
         {
            hess_map.insert(std::make_pair(calc_code, hessian));
         }
      }

      if (save_in_cont)
      {
         // Puts them into the general container.
         In Cont = der_cont.size();
         for (In ia = I_0; ia < aNderiv; ia++)
         {
            der_cont.push_back(gradient[ia]);
         }
         for (In ia = I_0; ia < aNderiv; ia++)
         {
            for (In ja = I_0; ja < aNderiv; ja++)
            {
               der_cont.push_back(hessian[ia][ja]);
            }
         }

         der_cont_handler[calc_no] = Cont; // set the pointer upon size
         
         if (gPesIoLevel > I_14)
         {
           Mout << "DerCont. calc_no: " << calc_no << " position: " << Cont << std::endl;
           Mout << " Gradient: " << std::endl;
           Mout << gradient << std::endl;
           Mout << " Hessian: " << std::endl;
           Mout << hessian << std::endl;
         }
      }
   }
}

} /* namespace PesFuncs */
