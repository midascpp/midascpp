#ifndef PESMPI_H_INCLUDED
#define PESMPI_H_INCLUDED

#include "mpi/Guard.h"
#include "mpi/Communicator.h"
#include "pes/PesMpiTypeDefs.h"

namespace midas
{
namespace pes
{
namespace mpi_impl
{

//! MPI signals. Used to send signals from Master to Slave.
enum signal
   {  HELLO
   ,  STOP
   ,  UPDATE_LEVEL_INFO
   ,  CALCULATE_SINGLEPOINTS
   ,  MOLECULEDATA
   ,  READINPOTENTIAL
   ,  ADGAFINALIZE
   };

//! MPI tags
enum tag 
   {  CALCCODES
   };

//! Slave driver for PES module
void SlaveDriver(const mpi::Communicator& aCommunicator);

} /* namespace mpi_impl */

//!  Initialize pes module mpi
mpi::Guard InitMpi(const mpi::Communicator& aCommunicator);

} /* namespace pes */
} /* namespace midas */

#endif /* PESMPI_H_INCLUDED */
