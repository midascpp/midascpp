/**
************************************************************************
*
* @file                ModeCoupling.cc
*
* Created:             23-08-2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Class members definitions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "pes/ModeCoupling.h"
#include "util/Io.h"
#include "input/PesCalcDef.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace pes
{

/**
 *  Constructor from Molecule class
 **/
ModeCoupling::ModeCoupling(const molecule::MoleculeInfo& aMolecule) 
   :  mScreeningMethod(-I_1)
   ,  mMolecule(aMolecule)
{
   mNmodes = mMolecule.GetNoOfVibs();
   mNatoms = mMolecule.GetNumberOfNuclei();
}

/**
 * Set the vibrational quantum numbers
 **/
void ModeCoupling::SetVibNum
   (  const std::vector<In>& arVibNum
   )
{
   In n_modes = Nmodes();
   if (arVibNum.size() != n_modes)
   {
      MIDASERROR(" Number of quantum numbers is wrong ");
   }
   mVibNum.resize(n_modes);
   mVibNum = arVibNum;
   return;
}

/**
 * Generate a displaced geometry, get a displacement vector
 **/
void ModeCoupling::MakeDisplacements(In aMode, Nb aQval, MidasVector& arDispVec)
{
   vector<Nuclei> disp_geom(Natoms());
   disp_geom=mMolecule.GetCoord();
   MidasVector norm_coord(I_3*Natoms(),C_0);
   norm_coord.Zero();
   mMolecule.GetNormalMode(norm_coord,aMode);
   In k=I_0;
   //Mout << " Mode: " << aMode << " Qval: " << aQval << endl;
   //Mout << " Normal coordinate: " << norm_coord << endl;
   //do the displacement
   for (In j=I_0;j<mNatoms;j++)
   {
      disp_geom[j].SetX(disp_geom[j].X() + aQval * norm_coord[k]);
      disp_geom[j].SetY(disp_geom[j].Y() + aQval * norm_coord[k+1]);
      disp_geom[j].SetZ(disp_geom[j].Z() + aQval * norm_coord[k+2]);
      k +=3;
   }
   //now check the displacements for each atom.
   arDispVec.SetNewSize(Natoms());
   arDispVec.Zero();
   for (In i_nuc=0; i_nuc<Natoms(); i_nuc++)
   {
      MidasVector nuc_coord(I_3);
      nuc_coord.Zero();
      nuc_coord[0]=disp_geom[i_nuc].X()-(mMolecule.GetNuci(i_nuc))->X();
      nuc_coord[1]=disp_geom[i_nuc].Y()-(mMolecule.GetNuci(i_nuc))->Y();
      nuc_coord[2]=disp_geom[i_nuc].Z()-(mMolecule.GetNuci(i_nuc))->Z();
      Nb dist = nuc_coord.Norm();
      //Mout << " dist: " << dist << endl;
      arDispVec[i_nuc]=dist;
   }
   //Mout << " Atom displacements for mode " << aMode << " : " << endl;
   //for (In i_nuc=0; i_nuc<Natoms(); i_nuc++)
      //Mout << " Atom: " << disp_geom[i_nuc].AtomLabel() << " displacement: " << arDispVec[i_nuc] << endl;
   return;
}

/**
 * Generate the Q vector from infos on input
 * */
void ModeCoupling::GenQvector(MidasVector& arQvals)
{
   vector<In> vib_num(Nmodes());
   vib_num=mVibNum;
   arQvals.SetNewSize(Nmodes());
   arQvals.Zero();
   for (In i=I_0; i<Nmodes(); i++)
      arQvals[i]=sqrt(C_2*(Nb(vib_num[i])+C_I_2)/(mMolecule.GetFreqI(i)/C_AUTKAYS))*(C_1/sqrt(C_FAMU));
   //Mout << " Qs values : "  << endl;
   //for (In i_q=0; i_q<Nmodes(); i_q++)
      //Mout << " Mode: " << i_q << " value: " << arQvals[i_q] << endl;
   return;
}

/**
 * Evaluate the coupling measure according to our formulation
 * */
void ModeCoupling::EvalPIC()
{
   Mout << " Evaluate the MC coupling " << endl;
   //loop over the modes, and associate a mode combi
   MidasVector q_vec(Nmodes(),C_0);
   GenQvector(q_vec);
   Mout << " q_vec: " << q_vec << endl;
   map<InVector,Nb> mode_combi_map;
   
   //loop over the modes, and associate a mode combi
   Nb s_max=C_0;
   for (In i=I_0; i<Nmodes(); i++)
   {
      for (In j=i+1; j<Nmodes(); j++)
      {
         vector<In> mode_combi;
         mode_combi.push_back(i);
         mode_combi.push_back(j);
         MidasVector disp_vec1(Natoms(),C_0);
         MakeDisplacements(i,q_vec[i],disp_vec1);
         MidasVector disp_vec2(Natoms(),C_0);
         MakeDisplacements(j,q_vec[j],disp_vec2);
         Nb s1=C_0;
         Nb s2=C_0;
         for (In i_n=I_0; i_n<Natoms(); i_n++)
         {
            for (In j_n=I_0; j_n<Natoms(); j_n++)
            {
               if (i_n==j_n) continue;
               //calculate distance
               Nb dist=(mMolecule.GetNuci(i_n))->Distance(*(mMolecule.GetNuci(j_n)));
               //Mout << " dist: " << dist << endl;
               s1+=disp_vec1[i_n]*disp_vec1[j_n]/(pow(dist,C_2));
               s2+=disp_vec1[i_n]*disp_vec2[i_n]/(pow(dist,C_2));
            }
         }
         Nb s=s1+s2;
         s_max=max(s_max,s);
         Mout << " for mode_combi: " << mode_combi << " coupling: " << s << endl;
         mode_combi_map.insert(make_pair(mode_combi,s));
      }
   }
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   Mout.precision(I_22);
   map<InVector,Nb>::iterator Min;
   Mout << " Maximum estimated mode-coupling: " << s_max << endl;
   for (Min=mode_combi_map.begin(); Min!=mode_combi_map.end(); Min++)
   {
      vector<In> mode_combi = Min->first;
      Nb s=Min->second;
      s/=s_max;
      bool to_do=false;
      if (s>=gPesCalcDef.GetmPesScreenThresh()) 
      to_do=true;
      mModeCoupling.insert(make_pair(mode_combi,to_do));
      Mout << " for mode_combi: " << mode_combi << " normalized coupling: " << s 
      << " skip: " << !to_do << endl; 
   }
   Mout << " Mode coupling map has been constructed " << endl;
   return;
}

/**
 * Evaluate the coupling measure according to Gerber recipe (L. Pele et al. JCP 128(2008),165105)
 * */
void ModeCoupling::EvalPICGerber()
{
   In n_atoms=Natoms();
   In n_modes=Nmodes();
   Mout << " Evaluate the MC coupling " << endl;
   //Mout << " no of atoms: " << n_atoms << "\t" << " n_modes: " << n_modes << endl;
   //Mout << " Nuclear masses (a.u.) " << endl;
   //for (In i=I_0; i<n_atoms; i++)
      //Mout << " atom nr. " << i << "\t" << " mass: " << nuc_mass[i] << endl;
   
   map<InVector,Nb> mode_combi_map;
   //loop over the modes, and associate a mode combi
   Nb pic_max=C_0;
   for (In i=I_0; i<Nmodes(); i++)
   {
      for (In j=i+1; j<Nmodes(); j++)
      {
         vector<In> mode_combi;
         mode_combi.push_back(i);
         mode_combi.push_back(j);
         MidasVector norm_mode1(I_3*n_atoms,C_0);
         mMolecule.GetNormalMode(norm_mode1,i);
         //Mout << " normal mode for mode: " << i << " is: " << norm_mode1 << endl;
         MidasVector norm_mode2(I_3*n_atoms,C_0);
         mMolecule.GetNormalMode(norm_mode2,j);
         //Mout << " normal mode for mode: " << j << " is: " << norm_mode2 << endl;
         Nb pic=C_0;
         In k=I_0;
         for (In i_n=I_0; i_n<Natoms(); i_n++)
         {
            Nb s1=C_0;
            Nb s2=C_0;
            for (In x_c=I_0; x_c<I_3; x_c++)
            {
               //masses in a.u.
               s1+=fabs(norm_mode1[k+x_c]*(mMolecule.GetNuci(i_n))->GetMass()*sqrt(C_FAMU));
               s2+=fabs(norm_mode2[k+x_c]*(mMolecule.GetNuci(i_n))->GetMass()*sqrt(C_FAMU));
            }
            pic+=(s1*s2);
            k+=I_3;
         }
         //Mout << " for mode_combi: " << mode_combi << " coupling: " << pic << endl;
         pic_max=max(pic_max,pic);
         mode_combi_map.insert(make_pair(mode_combi,pic));
      }
   }
   map<InVector,Nb>::iterator Min;
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   Mout.precision(I_22);

   Mout << " Maximum estimated mode-coupling: " << pic_max << endl;
   for (Min=mode_combi_map.begin(); Min!=mode_combi_map.end(); Min++)
   {
      vector<In> mode_combi = Min->first; 
      Nb s=Min->second;
      s/=pic_max;
      bool to_do=false;
      if (s>=gPesCalcDef.GetmPesScreenThresh())
         to_do=true;
      mModeCoupling.insert(make_pair(mode_combi,to_do));
      Mout << " for mode_combi: " << mode_combi << " normalized coupling: " << s 
      << " skip: " << !to_do << endl;
   }
   Mout << " Mode coupling map has been constructed " << endl;
   return;
}

/**
 * Evaluate the important couplings
 * */
void ModeCoupling::EvalCouplings()
{
   if (ScreeningMethod()==I_0)
   {
      Mout << " The screening method is the Ove one.." << endl;
      EvalPIC();
      return;
   }
   else if (ScreeningMethod()==I_1)
   {
      Mout << " The screening method is the one of Gerber " << endl;
      EvalPICGerber();
      return;
   }
   else
      MIDASERROR(" Screening method wrong or not specified ");
   return;
}

/**
 * Set the screening method
 * */
void ModeCoupling::SetMethod(In aMethod)
{
   mScreeningMethod=aMethod;
   return;
}

/**
 * Works only for the bidimensional mode-combis
 * */
bool ModeCoupling::ScreenModeCombi(InVector& arModeCombi)
{
   if (arModeCombi.size()!=I_2) MIDASERROR(" Criteria not implemented yet ");
   vector<In> mode_com=arModeCombi;
   bool screen=true;
   map<InVector,bool>::iterator Mib=mModeCoupling.find(mode_com);
   if (Mib->second) screen=false;
   return screen;
}

} /* namespace pes */
} /* namespace midas */
