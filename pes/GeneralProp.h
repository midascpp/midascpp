/**
************************************************************************
* 
* @file                GeneralProp.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   GeneralProp datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_PES_GENERALPROP_H_INCLUDED
#define MIDAS_PES_GENERALPROP_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <map>
#include <utility> // for std::pair

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "pes/PesProperty.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace pes
{

/**
 * Class for holding properties.
 **/
class GeneralProp
{
   private:
      //! Vector of all property infos.
      std::vector<PesProperty> mPropInfos; 
      
      //! Read file with properties.
      void GeneralPropertyReader(const std::string& aFileName, const std::map<In, In>& aNucMap);

      //! Correct derivatives to correspond to the Midas nuclear ordering.
      void CorrectNucOrder(const std::vector<In>& aNucMap);
      
      //! Rotate properties.
      void TransformGeneral(const MidasMatrix& aRotMatrix, In aGroup);
      
      //! Rotate property derivatives /if they are present).
      void TransformDerivatives
         (  const MidasMatrix& aRotMatrix
         ,  MidasVector& aGrad
         ,  MidasMatrix& aHess
         );

      //!
      bool HasDerivatives() const;
      

   public:
      //! Constructor
      GeneralProp(const PropertyInfo& aPropertyInfo);

      //! Read properties from file
      void RetrieveProperties(const std::string& aFileName, const MidasMatrix& aRotMatrix, const std::map<In, In>& aNucMap);
      
      //!@{
      //! Getters
      std::vector<std::string> GetDescriptors() const;
      std::vector<Nb> GetValues() const;
      const MidasVector& GetFirstDerivatives(In aI) const;
      const MidasMatrix& GetSecondDerivatives(In aI) const;
      In NoOfProps() const;
      //!@}
      
      //! Find Property from a description
      PesProperty& FindPropertyFromDescription(const std::string& aDescription);

      //! Find Property number from a description
      In FindPropertyNumberFromDescription(const std::string& aDescription);
      
      //! Rotate properties
      void Rotate(const MidasMatrix& aRotMatrix, const std::vector<In>& aRelation);
      
      //! Add terms to properties from a map of descriptor/value pairs
      void AddTerms(const std::map<std::string, Nb>& aTermMap);
      
      ////! Clear coriolis terms
      //void ClearCoriolisTerms();

      //! Transform properties by normal-coordinates.
      void TransDerByNormCoord(const molecule::MoleculeInfo& aMolecule);

      //! dump to file for restart
      void DumpToStream(std::ostream& aOs) const;
      
      //! read from restart stream
      void ReadFromStream(std::istream& aOs);
      
      //! Overload of output operator
      friend std::ostream& operator<<(std::ostream& aOs, const GeneralProp& aGeneralProp);

#ifdef VAR_MPI
      //! Construct from mpi blob
      GeneralProp(mpi::Blob& aBlob);

      //! Add to mpi blob
      void LoadIntoMpiBlob(mpi::Blob& aBlob) const;
#endif /* VAR_MPI */
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_GENERALPROP_H_INCLUDED */
