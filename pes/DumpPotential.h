/**
************************************************************************
*
* @file                DumpPotential.h
*
* Created:             28-03-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Handler for dumping potential files 
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef DUMPPOTENTIAL_H_INCLUDED
#define DUMPPOTENTIAL_H_INCLUDED

// std headers
#include <map>
#include <mutex>
#include <vector>

// midas headers
#include "pes/PesInfo.h"
#include "pes/PesUtility.h"
#include "pes/fitting/PolyFit.h"
#include "pes/fitting/FitFunctions.h"
#include "input/PesCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "input/MidasOperatorWriter.h"
#include "mmv/MidasVector.h"

/**
 *
**/
class DumpPotential
{
   private:
      
      //! PesCalcDef reference 
      const PesCalcDef&                               mPesCalcDef;
      
      //! PesInfo reference
      const PesInfo&                                  mPesInfo;
      
      //! Scaling factors for operator coefficients
      MidasVector                                     mScalFacts;

      //! Mutex for writing to operator terms object
      std::unique_ptr<std::mutex>                     mOperatorTermsMutex;

      //! Property files to be dumped after the fitting routine is finished
      std::vector<MidasOperatorWriter>                mOperFiles;

   public:
      
      //! Default constructor.
      DumpPotential();

      //! Constructor
      DumpPotential(const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo);

      //! 
      void Initialize(const ModeCombiOpRange& aModeCom, const std::vector<std::vector<Nb>>& aOneModeGridBounds, const In& aNmodes, const std::vector<Nb>& arReferenceProp);
      
      //! Dump the property files to disk
      void SetSurfaceTerm(const std::string& aPropType, const std::vector<InVector>& aBasisFuncOrders, const FitFunctions& aFitFunctions, const ModeCombi& aModeCombi, const In& aBar, const MidasVector& aLeastSqrParms, std::map<std::tuple<In, In, std::vector<In> >, std::tuple<Nb, std::vector<std::string>, std::vector<std::string> > >& aOperatorTerms) const;

      //! Store a property surface term for later dump to file
      void DumpSurfaceTerm(const In& aBar, const std::tuple<Nb, std::vector<std::string>, std::vector<std::string> >& aPropertyTerm);

      //! Get the operator files to be dumped
      std::vector<MidasOperatorWriter>& GetOperFiles() {return mOperFiles;}

      //! Get scale factors for a specific mode
      const Nb& GetScalFact(In aModeCombi) const {return mScalFacts[aModeCombi];}

      //! Output and dump harmonic frequencies from fitted potential
      void OadHarmonicFreqs(const std::vector<Nb>& aLeastSqrParms) const;

      //!
      void PlotNonFittedPointsForMc(const ModeCombi& aModeCom, const std::vector<Nb>& aOneModeGridBounds, const In& aPropNumber, const std::vector<MidasVector>& aCgCoords, const MidasVector aCgProperty) const;

};

#endif /* DUMPPOTENTIAL_H_INCLUDED */
