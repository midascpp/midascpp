/**
************************************************************************
* 
* @file                GridFileHandlerMethods.cc
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for handling common algorithm for Grid/Adga Bar files
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<vector>
#include<map>
#include<set>
#include<string>
#include<algorithm>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "util/Io.h"
#include "util/FileHandler.h"
#include "util/Isums.h"
#include "util/Timer.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/Nss.h"
#include "pes/PesFuncs.h"
#include "pes/PesUtility.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "potentials/FilePot.h"

// using declarations
using std::vector;
using std::map;
using std::set;
using std::string;

/**
 *
 **/
void GridFileHandlerMethods::WriteDataToFiles
   (  const std::vector<Nb>& aCoordinates
   ,  const std::vector<MidasVector>& arDerProps
   ,  const MidasVector& arProps
   ,  const std::vector<In>& arKVec
   ,  const Derivatives& arDerivatives
   ,  Nb aPotValue
   )
{
   mFile.Width(29);
   mFile << FileHandles::to_all << aCoordinates << FileHandles::to_dist << arProps;
   
   for(In p = I_0; p < mPesInfo.GetMultiLevelInfo().GetNoOfProps();++p)
   {   
      if (  mPesInfo.GetmUseDerivatives() && gPesCalcDef.GetmPesShepardInt() 
         && AvailableDer(arDerivatives,p) && gPesCalcDef.GetmPesShepardOrd() > I_0
         )
      {
         mFile << FileHandles::to_one(p) << arDerProps[p];
         //Mout << "GRAD_HESS PART TO BAR FILE : " << arDerProps[p];
      }
      else
      {
         mFile << FileHandles::to_one(p) << endl;
      }
   }

   if (mLocalPlot)
   {
      for (In it=0; it<aCoordinates.size(); ++it)
      {
         mPlotfile.width(29);
         mPlotfile << aCoordinates[it]*sqrt(C_FAMU) << "  ";
      }
      mPlotfile << aPotValue << endl;
      if (!mOnlyPotPlot)
      {
         for (In it=0; it<aCoordinates.size(); ++it)
         {
            mPlotfilebar.width(29);
            mPlotfilebar << aCoordinates[it]*sqrt(C_FAMU) << "  ";
         }
         mPlotfilebar << arProps[0] << endl; //assumes the energy is the first property
      }
   }
   mPlotfile.close();
   mPlotfilebar.close();
}

/**
* Generates the linear combination of points which are needed to calculate the Vbar value for the current MC
**/
void GridFileHandlerMethods::GenerateLinearCombinationMap(vector<In>& arMcVec, map<InVector, Nb>& arLCmap)
{
   vector<InVector> lin_com_vec;
   //put in the first vectors in the linear combination
   
   for (In i=I_0;i<arMcVec.size();++i)
   {
      vector<In> loc_vec;
      for (In j=I_0;j<=i;++j)
         loc_vec.push_back(arMcVec[j]);
      lin_com_vec.push_back(loc_vec);
   }
   //generate permutations and put those in as well in a sorted way
   while ( std::next_permutation(arMcVec.begin(), arMcVec.end() ) )
   {
      for (In i=I_0;i<arMcVec.size();++i)
      {
         vector<In> loc_vec;
         for (In j=I_0;j<=i;++j)
            loc_vec.push_back(arMcVec[j]);
         lin_com_vec.push_back(loc_vec);
      }
      for (In i=I_0;i<lin_com_vec.size();++i)
         sort(lin_com_vec[i].begin(),lin_com_vec[i].end());
   }
   //find the unique elements
   set<InVector> unique;
   for (In i=I_0;i<lin_com_vec.size();++i)
      unique.insert(lin_com_vec[i]);
   //calculate the C-coefficient and the number of times a given
   //permutation is found in the original set
   Nb coef=C_0;
   Nb fac = C_1/NbFact(arMcVec.size());
   for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); ++Svi)
   {
      In cnt=I_0;
      for (In i=I_0;i<lin_com_vec.size();++i)
      {
         if ( (*Svi)==lin_com_vec[i] )
         {
            ++cnt;
            coef = fac*pow(-C_1,Nb(arMcVec.size()-lin_com_vec[i].size()))*NbBinCo(arMcVec.size(),lin_com_vec[i].size());
         }
      }
      arLCmap[(*Svi)] = Nb(cnt)*coef;
   }
}

/**
 *
**/
void GridFileHandlerMethods::SetupPlot
   (  const vector<In>& arLocalModes
   )
{
   if (gPesCalcDef.GetmPesPlotAll() && (arLocalModes.size()<=2 && arLocalModes.size()>0))
   {
      mLocalPlot = true;
      mOnlyPotPlot = false;
   }
   //Mout << " gPesCalcDef.GetmPesPlotSelected() = " << gPesCalcDef.GetmPesPlotSelected() << endl;
   if (gPesCalcDef.GetmPesPlotSelected() && (arLocalModes.size()<=2 && arLocalModes.size()>0))
   {
      //see if the current mode is requested for plotting 
      bool found_com = false;
      //other way
      std::string test = gPesCalcDef.GetmPesPlotSelectedMap().find(arLocalModes)->second;
      if (test == "ONLYFULLPOT" || test == "ALLPOT")
      {
         found_com = true;
      }
      if (!found_com)
      {
         if (gPesIoLevel > I_14)
         {
            Mout << " The mode combi " << " ";
            for (In i_mod = I_0; i_mod < arLocalModes.size(); ++i_mod)
            {
               Mout << arLocalModes[i_mod] << " ";
            }
            Mout << "will not be plotted " << std::endl;
         }
      }
      else
      {
         if (gPesIoLevel > I_14)
         {
            Mout << " The mode combi " << arLocalModes << " is found in the list of potentials to be plotted " << std::endl;
         }
         //search for more info
         mLocalPlot = true;
         if (gPesCalcDef.GetmPesPlotSelectedMap().find(arLocalModes)->second == "ALLPOT")
         {
            mOnlyPotPlot = false;
            if (gPesIoLevel > I_14)
            {
               Mout << " Only the full potential will be plotted for the " << arLocalModes << " mode " << std::endl;
            }
         }
         else
         {
            if (gPesIoLevel > I_14)
            {
               Mout << " V and V^(bar) potentials will be plotted for the " << arLocalModes << " mode " << std::endl;
            }
         }
      }
   }
   /**
   * If we are actually going to plot something setup the files for it
   **/
   if (mLocalPlot)
   {
      SetupFilesForPlot(arLocalModes);
   }
}

/**
 *
**/
void GridFileHandlerMethods::SetupFilesForPlot
   (  const std::vector<In>& arLocalModes
   )
{
   if(arLocalModes.size() == 0)
   {
      return; // return !
   }
   
   ofstream listofplots;
   string mode_num;
   
   if (arLocalModes.size()<2) 
   {
      mode_num="m1."+std::to_string(arLocalModes[0]);
   }
   else
   {
      mode_num="m1."+std::to_string(arLocalModes[0])+".m2."+std::to_string(arLocalModes[1]);
   }
   
   string listofplotsname=mPesInfo.GetMultiLevelInfo().AnalysisDir()+"/"+"ListofPlots";
   listofplots.open(listofplotsname.c_str(),ios_base::app); //open list of files for plotting
   listofplots.setf(ios::scientific);
   listofplots.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, listofplots);

   string level = std::to_string(mPesInfo.GetMultiLevelInfo().CurrentLevel());
   //string plotname=mPesInfo.GetMultiLevelInfo().AnalysisDir()+"/level_"+level+"/plotV_"+mode_num+".grid";
   string plotname=mPesInfo.GetmSubSystemDir()+"/Multilevels/level_"+level+"/analysis/plotV_"+mode_num+".grid";
   mPlotfile.open(plotname.c_str(),ios_base::out);  //open file for plot for full V
   mPlotfile.setf(ios::scientific);
   mPlotfile.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, mPlotfile);
   listofplots << plotname << endl;

   if (!mOnlyPotPlot)
   {
      //string plotbarname=mPesInfo.GetMultiLevelInfo().AnalysisDir()+"/level_"+level+"/plotVbar_"+mode_num+".grid";
      string plotbarname=mPesInfo.GetmSubSystemDir()+"/Multilevels/level_"+level+"/analysis/plotVbar_"+mode_num+".grid";
      mPlotfilebar.open(plotbarname.c_str(),ios_base::out); //open file for plot for V bar
      mPlotfilebar.setf(ios::scientific);
      mPlotfilebar.setf(ios::uppercase);
      mPlotfilebar.precision(22);
      listofplots << plotbarname << endl;
   }
   
   listofplots.close(); // close list of plots
}

/**
 * Do extrapolation.
 **/
void GridFileHandlerMethods::Extrapolation
   (  const Derivatives& arDerivatives
   ,  const CalculationList& aCalculationList
   ,  const std::vector<std::string>& arPropWithDerInfo
   )
{
   Timer extrap_time;
   In n_max_calcs = mPesInfo.GetNrOfDerCalcs();
   for (In prop = I_0; prop < arPropWithDerInfo.size(); prop++)
   {
      std::string property = arPropWithDerInfo[prop];
      std::vector<Nb> der_cont;
      std::map<In, In> der_cont_handler;

      In property_number = FindPropertyNumberInFile(mPesInfo, property, I_0);
      arDerivatives.GetDerivatives(property, der_cont, der_cont_handler);
      In order = arDerivatives.OrderDer(property);

//    Consider later on to treat to different approx. e.g. 2->4, 3->5 in one shoot. 
//    It does not make much sense, not to extrapolate from the same mode-coupling level,
//    i.e. 3->4, 3->5, but it gives more flexibility to the code...
//    ...and may make sense for benchmarking.
      if(gDebug)
      {
         Mout << "Entering the extrapolation..." << std::endl;
      }

      // Sets the Nss 
      In dim=mPesInfo.GetExtrapFrom(); // dimension of the Nss 
      In s_dim=gPesCalcDef.GetmExtrapolateTo(); // dimension of the sum
      In i_extrapolate_to = gPesCalcDef.GetmExtrapolateTo();

      if (gPesCalcDef.GetmPesAdga())
      {
         s_dim = dim + I_1 ;
         i_extrapolate_to = s_dim;
      }
      //*********
      //SETTING UP THE POINTS MATRIX
      //*********

      s_dim--; //starts at zero thus decreases the dimension 
      In* s_start= new In[dim]; for (In i=0;i<dim;i++) *(s_start+i)=I_0;
      In* s_end  = new In[dim]; for (In i=0;i<dim;i++) *(s_end+i)=s_dim;
      In* s_step = new In[dim]; for (In i=0;i<dim;i++) *(s_step+i)=I_1;
      Nss ModeRange(dim,s_start,s_end,s_step);

      In n_combinations=I_0;
      while (ModeRange.RunOrder())
         n_combinations++;
      In n_combis=ModeRange.Dim();
      //MidasMatrix ModeIndexes(n_combinations,n_combis);
      In** ModeIndexes;
      ModeIndexes= new In* [n_combinations];
      //In ModeIndexes[ncomb];
      for (In i=0;i<n_combinations;i++)
         ModeIndexes[i] = new In [n_combis];
      //Mout << "EXTRAPOLA, NSS: " << n_combinations << " " << n_combis << endl;
      In i_comb=I_0;
      while (ModeRange.RunOrder())
      {
         for (In i=I_0;i<n_combis;i++)
         {
            ModeIndexes[i_comb][i]=ModeRange.VectorIndex(i+I_1);
            //Mout << ModeIndexes[i_comb][i] << " ";
         }
         i_comb++;
         //Mout << endl;
      }
    
      // Here constructs the matrix with the structures of the points that enter
      // the Vbar expression
      In dc_dim=I_0;
      for (In i=I_0; i<i_extrapolate_to; i++)
         dc_dim+=IntBinCoef(i_extrapolate_to,i);
      MidasMatrix points(dc_dim,i_extrapolate_to);
      points.Zero();
      //Mout << "Number of points needed: " << dc_dim << endl;
      In i_pos=I_0;
      for (In i=I_0; i<i_extrapolate_to; i++) // Includes the origin
         points[i_pos][i]=I_0;
      i_pos++;
      for (In i=I_1; i<i_extrapolate_to; i++)
      {
         // Sets the Nss 
         In dm=i; // dimension of the Nss 
         In s_dm=i_extrapolate_to; // dimension of the sum
         s_dm--; // starts at zero, so the dim--
         In* s_str = new In[dm]; for (In ia=0;ia<dm;ia++) *(s_str+ia)=I_0 ;
         In* s_nd  = new In[dm]; for (In ia=0;ia<dm;ia++) *(s_nd+ia) =s_dm;
         In* s_stp = new In[dm]; for (In ia=0;ia<dm;ia++) *(s_stp+ia)=I_1 ;
         Nss nss_points(dm,s_str,s_nd,s_stp);
         while ( nss_points.RunOrder() )
         {
            In j=I_0;
            In j_pos=I_0;
            //Mout << "Nss: ";
            //for (In ib=I_0; ib<i;ib++)
            //   Mout << nss_points.VectorIndex(ib+I_1) << " ";
            //Mout << endl;
            while (j < i_extrapolate_to )
            {
               if(j_pos < dm)
                  if (j == nss_points.VectorIndex(j_pos+I_1) && j_pos <= i_extrapolate_to)
                  {
                     points[i_pos][j]=I_1;
                     j_pos++;
                  }
               j++;
            }
            i_pos++;
         }
         delete[] s_str;
         delete[] s_nd;
         delete[] s_stp;
      }
      if (gDebug)
      {
         Mout << "Matrix with coefficients for points to do be used in Vbar transformation: " << endl;
         for (In i=I_0; i<dc_dim; i++)
         {
            for (In j=I_0; j<i_extrapolate_to; j++)
               Mout << points[i][j] << " ";
            Mout << endl;
         }
      }


      //*********
      // DONE SETTING UP THE POINTS MATRIX
      //*********


      //*********
      // FIND NR OF LINES IN prop_bar_files_navigation.mpesinfo
      //********* 

      // D. T. consider open files for dumping information later
      std::string off_file_name = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_bar_files_navigation.mpesinfo";
      if (!InquireFile(off_file_name))
      {
         MIDASERROR(" Unable to open file to read ");
      }
      ifstream bar_int(off_file_name.c_str(), ios_base::in);
      string s;
      getline(bar_int,s);
      istringstream input_s(s);

      /***************/
      // IAN: What is the purpose of this code ?!?...
      string s_new;
      vector<string> curr_line;
      while (input_s >> s_new)
         curr_line.push_back(s_new);
      curr_line.clear();  // skip the header
      /****************/

      //then find the max no of lines already present
      In i_bar_count=I_0;
      In i_bar_count_save=I_0;
      while(getline(bar_int,s))
      {
         curr_line.clear();
         istringstream input_s(s);
         while (input_s >> s_new)
            curr_line.push_back(s_new);
         In i_l=InFromString(curr_line[curr_line.size()-1]);
         i_bar_count=max(i_bar_count,i_l);
         //Mout << " read i_s: " << i_s << endl;
      }
      if (gPesIoLevel > I_14)
      {
         Mout << " Nr. of lines in .mbar file before extr. is: " << i_bar_count << std::endl;
      }
      i_bar_count_save = i_bar_count + I_1;
      //i_bar_count++;
      bar_int.close();

      //*******
      // OPEN OFFSETS AGAIN?, .BAR file and _for_extrap file
      //*******

      ofstream bar_int_app(off_file_name.c_str(),ios_base::app); //now open for append
      string prop_file_name=mPesInfo.GetMultiLevelInfo().SaveIoDir()+"/"+"prop_no_"+std::to_string(property_number)+".mbar";
      if (!InquireFile(prop_file_name))
      {
         MIDASERROR(" Unable to find .mbar file ");
      }
      //open to append
      ofstream bar_file(prop_file_name.c_str(),ios_base::app);
      bar_file.setf(ios::scientific);
      bar_file.setf(ios::uppercase);
      bar_file.precision(I_22);
      //count the max # of calculations to be done..
      //In n_max_calcs=mPesInfo.GetNrOfDerCalcs();
      In n_calcs=0;
      InVector mode_combi_sav;
      bool loc_pot=false;
      //files for plotting
      ofstream plotfilebar;
      ofstream listofplots;
      //file for reading vbar infos.
      //string vbar_file_name=mPesInfo.GetMultiLevelInfo().SaveIoDir()+"/"+"prop_no_"+std::to_string(property_number)+"_for_extrap";
      //ifstream vbar_file;
      //vbar_file.open(vbar_file_name.c_str(), ios_base::in);
      //mPesInfo.ResetCalcCodeAndNr();
      In count=I_1;

      //***
      //LOOP UP OVER THE NR OF CALCS IN THE DERCALCLIST
      //***

      if (gPesIoLevel > I_5 && n_max_calcs != I_0)
      {
         Mout << std::endl << " Maximum number of extrapolation calcs.: " << n_max_calcs << std::endl;
      }

      while (count <= n_max_calcs)
      {
         In calc_no = count;
         count++;
         string calc_code = mPesInfo.GetCalcCodeFromCalcNumber(calc_no);
         if (calc_code == "*#0*")
         {
            continue;
         }

         if (gPesIoLevel > I_7)
         {
            Mout << "----------------------------------------------" << std::endl;
            Mout << "  calc_no: " << calc_no << ", calc_code: " << calc_code << std::endl;
         }

         n_calcs++;
         i_bar_count++;

         InVector mode_combi;
         vector<string> k_vec_s;
         //mPesInfo.GetInfoFromString(calc_code, mode_combi, k_vec_s);
         GetInfoFromString(calc_code, mode_combi, k_vec_s);

         if (gPesIoLevel > I_7)
         {
            Mout << "  mode combination: " << mode_combi << std::endl;
         }

         //***
         //RUN DURING FIRST ITER
         //***

         if (n_calcs==1)
         {
            mode_combi_sav.resize(mode_combi.size());
            for (In i=0; i<mode_combi.size(); i++) mode_combi_sav[i]=mode_combi[i];
         }

         //****
         //DO WE HAVE A NEW MODE COMBI?
         //****
         bool next=false;
         if (mode_combi.size()!=mode_combi_sav.size()) next=true;
         if (!next)
         {
            for (In i=0; i<mode_combi.size(); i++)
            {
               if (mode_combi[i]!=mode_combi_sav[i])
               {
                   next=true;
                   break;
                }
            }
         }

         //***
         // WHEN WE GO TO A NEW MODE COMBI WE WRITE INFO TO offsets file
         //***
         if (next || (n_calcs == n_max_calcs) )
         {
            //Mout << " writing infos for offsets.. " << endl;
            if (n_calcs == n_max_calcs)
            {
               i_bar_count++;
            }
            bar_int_app << " (";
            for (In i = I_0; i < mode_combi_sav.size(); i++)
            {
               bar_int_app << mPesInfo.GetMolecule().GetModeLabel(mode_combi_sav[i]);
               if (mode_combi_sav.size() > I_1 && i != mode_combi_sav.size() - I_1)
               {
                  bar_int_app << ",";
               }
            }
            bar_int_app << ")    ";
            if (mode_combi_sav.size() != I_0)
            {
               bar_int_app << i_bar_count_save << " " << i_bar_count - I_1 << std::endl;
            }
            i_bar_count_save = i_bar_count;
            mode_combi_sav.clear();
            mode_combi_sav.resize(mode_combi.size());
            for (In j = I_0; j < mode_combi.size(); j++)
            {
               mode_combi_sav[j] = mode_combi[j];
            }
         }
         //***
         // ADD INFO TO OUR PLOT FILES FOR EACH NEW MODE COMBI
         //***
         if (next || n_calcs == I_1)
         {
            //D.T. plotting section with the same structure as in DoDispDer
            if (gPesCalcDef.GetmPesPlotAll() && (mode_combi.size() <= I_2 && mode_combi.size() > I_0))
            {
               loc_pot = true;
            }
            else if (gPesCalcDef.GetmPesPlotSelected() && (mode_combi.size() <= I_2 && mode_combi.size() > I_0))
            {
               //see if the current mode is requested for plotting 
               bool found_com = false;
               //other way
               string test = gPesCalcDef.GetmPesPlotSelectedMap().find(mode_combi)->second;
               if (test == "ALLPOT")
               {
                  found_com = true;
               }
               if (!found_com)
               {
                  if (gPesIoLevel > I_10)
                  {
                     Mout << " The mode combi " << " ";
                     for (In i_mod = I_0; i_mod < mode_combi.size(); i_mod++)
                     {
                        Mout << mode_combi[i_mod] << " ";
                     }
                     Mout << "will not be plotted " << std::endl;
                  }
                      loc_pot = false;
               }
               else
               {
                  if (gPesIoLevel > I_10)
                  {
                     Mout << " The mode combi " << mode_combi << " is found in the list of potentials to be plotted " << std::endl;
                  }
                  //search for more info
                  loc_pot = true;
                  if (gPesIoLevel > I_10)
                  {
                     Mout << " Extrapolated V^(bar) potentials will be plotted for the " << mode_combi << " mode " << std::endl;
                  }
               }
            }
            //if plotting is requested open the files for output
            if (loc_pot)
            {
               if (next || n_calcs == I_1)
               {
                  if (n_calcs > 1)
                  {
                     plotfilebar.close();
                  }
                  string mode_num;
                  if (mode_combi.size() < I_2)
                  {
                     mode_num = "m1." + std::to_string(mode_combi[I_0]);
                  }
                  else
                  {
                     mode_num = "m1." + std::to_string(mode_combi[I_0]) + ".m2." + std::to_string(mode_combi[I_1]);
                  }
                  //string plotbarname=mPesInfo.GetMultiLevelInfo().AnalysisDir()+"/level_"+std::to_string(mPesInfo.GetMultiLevelInfo().CurrentLevel())+"/plotVbar_"+mode_num+".grid";
                  string plotbarname=mPesInfo.GetmSubSystemDir()+"/Multilevels/level_"+std::to_string(mPesInfo.GetMultiLevelInfo().CurrentLevel())+"/analysis/plotVbar_"+mode_num+".grid";
                  string listofplotsname=mPesInfo.GetMultiLevelInfo().AnalysisDir()+"/"+"ListofPlots";
                  //Mout << "opening plot files " << endl;
                  plotfilebar.open(plotbarname.c_str(),ios_base::out); //plot for V bar
                  plotfilebar.setf(ios::scientific);
                  plotfilebar.setf(ios::uppercase);
                  plotfilebar.precision(22);
                  listofplots.open(listofplotsname.c_str(),ios_base::app); //lists the files for plotting
                  listofplots.setf(ios::scientific);
                  listofplots.setf(ios::uppercase);
                  listofplots.precision(22);
                  //listofplots << plotname << endl;
                  listofplots << plotbarname << endl;
                  listofplots.close();
               }
            }
         } // end of getline
         // lol

         //construct coordinate from k_vec_s
         //In add=arGpFr.GetGridAdd(mode_combi);
         MidasVector q_vec(mPesInfo.GetMolecule().GetNoOfVibs(),C_0);
         bool v_zero = false;
         for (In i_mode = I_0; i_mode < mode_combi.size(); i_mode++)
         {
            //In den=arGpFr.GivePair(add).first[i_mode]/I_2;
            //Nb lg_point=C_1/Nb(den);
            Nb lg_point = C_1;
            PesFuncs::StringMultiplication(k_vec_s[i_mode], lg_point);
            //k_vec.push_back(den);
            //lg_point *=Nb(den);
            if (lg_point == C_0)
            {
               v_zero = true;
            }
            if (lg_point > C_0)
            {
               q_vec[mode_combi[i_mode]]=mPesInfo.GetScalingInfo().GetRScalFact(mode_combi[i_mode])*lg_point;
            }
            else
            {
               q_vec[mode_combi[i_mode]]=mPesInfo.GetScalingInfo().GetLScalFact(mode_combi[i_mode])*lg_point;
            }
         }
         //    Mout << " for mode combi: " << mode_combi << " add: " << add << endl;
         //    Mout << " q_vec: " << q_vec << endl;

         Nb denominator_weight=C_0;
         Nb extrap_value=C_0;
         //******
         //IF ONE ENTRY IN k_vec is zero bar(V(k))=0
         //******
         //if one of the k_vec is zero then just put to zero
         if (v_zero)
         {
            //find the code and extrapolate the vbar
            string cc_piece = "*";
            for (In i_k=I_0; i_k<k_vec_s.size(); i_k++)
            {
               Nb lg_point=C_1;
               PesFuncs::StringMultiplication(k_vec_s[i_k],lg_point);
               if (lg_point!=C_0) cc_piece+= '#' + std::to_string(mode_combi[i_k]+1) + '_' + k_vec_s[i_k];
            }
            cc_piece += "*";
            //Mout << " string piece: " << cc_piece << endl;
            if (cc_piece=="**") cc_piece="*#0*";
            Nb extrap_value=C_0;
            //if (cc_piece !="*#0*")
            //{
            //   In opt=I_2;
            //   string s_dum;
            //   bool test=SearchInFile2(cc_piece,vbar_file,vbar_file_name,s_dum,extrap_value,opt);
            //   if (!test)
            //   {
            //      Mout << " Could not find calc. code: " << cc_piece << " in the list of calculations " << endl<< endl << endl;
            //      MIDASERROR(" Could not file calculation string in prop file ", __FILE__, __LINE__);
            //   }
            //}
            extrap_value=C_0;
            //Mout << " For calc code : " << calc_code << " extrapolated value is: " << extrap_value << endl;
            for (In i_q=0; i_q<mode_combi.size(); i_q++)
            {
                bar_file.width(29);
                bar_file << q_vec[mode_combi[i_q]] << "   ";
            }
            bar_file << extrap_value << "  " << endl;
            if (loc_pot)
            {
               for (In i_q=0; i_q<mode_combi.size(); i_q++)
               {
                  plotfilebar.width(29);
                  plotfilebar << q_vec[mode_combi[i_q]]*sqrt(C_FAMU) << "  ";
               }
               plotfilebar << extrap_value << endl; //assumes the energy is the first property
               Mout << " ending with plotting stuff " << endl;
            }
            continue; //THE POINT WERE CALCULATED AS A REAL CALCULATION AND IS NOW IN OUR CALCLIST
         }

         //*****
         //WE MAKE ALL UNIQUE SUB-MODE COMBI VECTORS
         //*****

         // we compute the l.c.map 
         std::map<InVector,Nb> lc_map;
         std::vector<InVector> lin_com_vec;
         // put in the first vectors in the linear combination
         std::vector<In> mode_vec = mode_combi;

         for (In i=I_0;i<mode_vec.size();i++)
         {
            vector<In> loc_vec;
            for (In j=I_0;j<=i;j++)
               loc_vec.push_back(mode_vec[j]);
            lin_com_vec.push_back(loc_vec);
         }
         //generate permutations and put those in as well in a sorted way
         while ( next_permutation(mode_vec.begin(), mode_vec.end() ) )
         {
            for (In i=I_0;i<mode_vec.size();i++)
            {
               vector<In> loc_vec;
               for (In j=I_0;j<=i;j++)
                  loc_vec.push_back(mode_vec[j]);
               lin_com_vec.push_back(loc_vec);
            }
            for (In i=I_0;i<lin_com_vec.size();i++)
               sort(lin_com_vec[i].begin(),lin_com_vec[i].end());
         }
         //find the unique elements
         set<InVector> unique;
         for (In i=I_0;i<lin_com_vec.size();i++)
            unique.insert(lin_com_vec[i]);
         //calculate the C-coefficient and the number of times a given
         //permutation is found in the original set
         Nb coef=C_0;
         for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
         {
            In cnt=I_0;
            for (In i=I_0;i<lin_com_vec.size();i++)
            {
               if ( (*Svi)==lin_com_vec[i] )
               {
                  cnt++;
                  Nb fac = C_1/NbFact(mode_vec.size());
                  coef = fac*pow(-C_1,Nb(mode_vec.size()-lin_com_vec[i].size()))*NbBinCo(mode_vec.size(),lin_com_vec[i].size());
               }
            }
            lc_map[(*Svi)] = Nb(cnt)*coef;
         }

         //****
         //NOW WE HAVE THE COEFFICIENTS FOR THE EXTRAPOLATION
         //****
         if (gPesIoLevel > I_14)
         {
            Mout << "Unique combinations & coefficients: " << std::endl;
            for (set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
            {
               Mout << (*Svi) << " " << lc_map[(*Svi)] << "  ";
            }
            Mout << std::endl;
         }

         // retrieve the hessian and the gradient from the points calculated which are needed
         // to convert Hessian,Gradient to Vbar counterparts
         In max_dc_dim = mPesInfo.GetMolecule().GetNoOfVibs()*(mPesInfo.GetMolecule().GetNoOfVibs()+I_1); // times the size to store Gradient+Hessian
         MidasMatrix grd_hess_points(dc_dim,max_dc_dim);
         //Mout << "CALCULATION CODE: " << calc_code << endl;
         // Treat the GS separated
         string s_num;
         {
            //retrieve the calculation number
            In i_pnt=I_0;
            string cc_pnt="*#0*";
            //Mout << "piece: GS" << endl;
            /* mbh: Check that should be restored
            bool found=SearchInFile2(cc_pnt,calcs_file,calcs_file_name,s_num,n_dum,i_opt);
            if (!found)
            {
               Mout << "Calculation Code not find in ListOfCalcs_Done file, needed to transform Gradient and Hessian to Vbar counterparts. " << endl;
               Mout << "The calculation code is: " << cc_pnt << endl;
               MIDASERROR(" Calculation Code not find in ListOfCalcs_done file. Stops here...");
            }
            */
            //Mout << " for calc_code: " << cc_pnt << " calc_no: " << s_num << endl;

            // get calc_number for calc_code *#0*
            s_num=std::to_string(aCalculationList.GetCalcNrFromCode(cc_pnt));

            // retrieve information from Handler
            In position = der_cont_handler[InFromString(s_num)];
                //Mout << InFromString(s_num) << " position: " << position << endl;
            for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
               grd_hess_points[i_pnt][ia]=der_cont[ia+position];
            position+=mPesInfo.GetMolecule().GetNoOfVibs();
            In i_el=I_0;
            for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
               for (In ib=I_0; ib<mPesInfo.GetMolecule().GetNoOfVibs(); ib++)
               {
                  grd_hess_points[i_pnt][mPesInfo.GetMolecule().GetNoOfVibs()+i_el]=der_cont[i_el+position];
                  i_el++;
               }
         }
         // Go for the remaining points...
         for (In i_pnt=I_1; i_pnt<dc_dim; i_pnt++)
         {
            string cc_pnt = "*";
            for (In pnt_pos=I_0; pnt_pos<i_extrapolate_to; pnt_pos++)
            {
               if (points(i_pnt,pnt_pos) == I_1)
                   cc_pnt+= '#' + std::to_string(mode_combi[pnt_pos]+1) + '_' + k_vec_s[pnt_pos];
            }
            cc_pnt += "*";
            //Mout << "piece: ";
            //for (In pnt_pos=I_0; pnt_pos<i_extrapolate_to; pnt_pos++)
            //   Mout << points(i_pnt,pnt_pos) << " ";
            //Mout << endl;
            //Mout << " string piece: " << i_pnt << " " << cc_pnt << endl;
            //retrieve the calculation number
            /* mbh: check that should be restored
            bool found=SearchInFile2(cc_pnt,calcs_file,calcs_file_name,s_num,n_dum,i_opt);
            if (!found)
            {
               if (gPesCalcDef.GetmPesAdga())  
               {
                  MIDASERROR(" Let ADGA calculate the new points on the lower dimension surfaces: disable #2 PotValsFromOpFile");
               }
               else
               {
                  Mout << "Calculation Code not find in ListOfCalcs_Done file, needed to transform Gradient and Hessian to Vbar counterparts. " << endl;
                  Mout << "The calculation code is: " << cc_pnt << endl;
               MIDASERROR(" Calculation Code not find in ListOfCalcs_done file. Stops here...");
               }
            }
            */
                //Mout << " for calc_code: " << cc_pnt << " calc_no: " << s_num << endl;
            // retrieve information from Handler
            s_num = std::to_string(aCalculationList.GetCalcNrFromCode(cc_pnt));
            In position = der_cont_handler[InFromString(s_num)];
                //Mout << InFromString(s_num) << " position: " << position << endl;
            for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
               grd_hess_points[i_pnt][ia]=der_cont[ia+position];
            position+=mPesInfo.GetMolecule().GetNoOfVibs();

            In i_el=I_0;
            for (In ia=I_0; ia<mPesInfo.GetMolecule().GetNoOfVibs(); ia++)
            for (In ib=I_0; ib<mPesInfo.GetMolecule().GetNoOfVibs(); ib++)
            {
               grd_hess_points[i_pnt][mPesInfo.GetMolecule().GetNoOfVibs()+i_el]=der_cont[i_el+position];
               i_el++;
            }
         }

         //*****
         //AND NOW WE HAVE THE GRADIENT/HESSIAN INFORMATION WE NEED
         //*****
         if (gPesIoLevel > I_14)
         {
            for (In ia=I_0; ia<dc_dim; ia++)
            {
               Mout << "i_pnt: " << ia << " -> ";
               for (In ib=I_0; ib<points.Ncols(); ib++)
                  Mout << points[ia][ib] << " ";
               Mout << endl;
               for (In ib=I_0; ib<max_dc_dim; ib++)
                  Mout << grd_hess_points[ia][ib] << " ";
               Mout << endl;
               Mout << "-------" << endl;
            }
         }
         // This is extremely inefficient, but only on this Mac, why?!?
         //while ( ModeRange.RunOrder()) // Runs over the pieces to construct the Taylor Series
         //{
         for (In i=I_0;i<n_combinations;i++) // here I run over the number of mode combinations
         {
            //Mout << "*** ";
            //for (In ia=I_0;ia<n_combis;ia++)
            //   Mout << mode_combi[ModeIndexes[i][ia]] << " ";
            //Mout << endl;
            InVector mc_piece;
            MidasVector q_vec_piece;
            vector<string> kvs_piece;
            for (In ia=I_0;ia<n_combis;ia++)
            {
               mc_piece.push_back(mode_combi[ModeIndexes[i][ia]]);
               for (In ib=I_0;ib<mode_combi.size();ib++)
                  kvs_piece.push_back(k_vec_s[ModeIndexes[i][ia]]);
            }

            q_vec_piece.SetNewSize(mPesInfo.GetMolecule().GetNoOfVibs(),false);
            q_vec_piece.Zero();

            //Thus q_vec & q_vec_piece have the same size, and transfers info.
            for (In i=I_0; i<mc_piece.size();i++)
               q_vec_piece[mc_piece[i]]=q_vec[mc_piece[i]];

            // weighting factor
            Nb weight = C_1;
            for (In ia=I_0;ia<mode_combi.size();ia++)
            {
               for (In ib=I_0; ib<mc_piece.size(); ib++)
                  if (mode_combi[ia]==mc_piece[ib])
                  {
                     weight*=q_vec[mode_combi[ia]]*q_vec[mode_combi[ia]];
                     break;
                  }
            }
            // Plug in here machinery for Grad, Hessian Vbar transformation
            // 1.- Take the linear combinations and find the points from the
            // expansion point
            //
            //Mout << "Unique combinations & coefficients: ";
            //for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
            //{
            //         Mout << (*Svi) << " " << lc_map[(*Svi)] << "  ";
            //}
            //Mout << endl;
            //Mout << "expansion point: " << q_vec_piece << endl;
            MidasVector q_pnt(mPesInfo.GetMolecule().GetNoOfVibs());
            MidasVector q_sht(i_extrapolate_to); // same as q_pnt but in the shorter form
            MidasVector q_grad(mPesInfo.GetMolecule().GetNoOfVibs());
            MidasMatrix q_hess(mPesInfo.GetMolecule().GetNoOfVibs());
            MidasVector q_grad1(mPesInfo.GetMolecule().GetNoOfVibs());
            MidasMatrix q_hess1(mPesInfo.GetMolecule().GetNoOfVibs());
            q_grad.Zero();
            q_hess.Zero();
            q_grad1.Zero();
            q_hess1.Zero();
            // Loop over the V to obtain the Vbar
            for(set<InVector>::iterator Svi=unique.begin(); Svi!= unique.end(); Svi++)
            {
               q_pnt.Zero();
               q_sht.Zero();
               // calculates the point wanted for a given V
               for (In ib=I_0;ib<q_pnt.Size();ib++)
                  for (In ia=I_0;ia<(*Svi).size();ia++)
                  {
                     if ( ib == (*Svi)[ia] )
                     {
                        if ( q_vec_piece[ib]!=C_0 )
                        {
                           q_pnt[ib]=I_1;
                        }
                     }
                  }
               // constructs its shorter form
               In ia_cont=I_0;
               for (In ib=I_0;ib<q_pnt.Size();ib++)
                  for (In ia=I_0;ia<mode_combi.size();ia++)
                     if (ib == mode_combi[ia])
                     {
                        q_sht[ia_cont]=q_pnt[ib];
                        ia_cont++;
                     }
               // retrieve the gradient & hessian for that point
               for (In ip=I_0;ip<points.Nrows();ip++)
               {
                  // searches for that point in the container set before
                  bool vctcoinc=true;
                  for (In ic=I_0;ic<points.Ncols();ic++)
                  {
                     vctcoinc = vctcoinc && (points[ip][ic] == q_sht[ic]);
                  }
                  if ( vctcoinc )
                  {
                     // saves in a vector and matrix ONLY those elements which
                     // cannot be zero, i.e. which differenciate with respect to
                     // one of modes of the current mode_combi (in *Svi) 
                     //
                     // N.B. The elements are summed up so one obtains the Hbar
                     // and GradVar.
                     for (In ic=I_0;ic<mPesInfo.GetMolecule().GetNoOfVibs();ic++)
                        for (In isvi=I_0;isvi<(*Svi).size();isvi++)
                        {
                           if ( ic == (*Svi)[isvi] )
                               q_grad[ic]+=Nb(lc_map[(*Svi)])*grd_hess_points[ip][ic];
                        }
                     In i_cont=I_0;
                     for (In ic=I_0;ic<mPesInfo.GetMolecule().GetNoOfVibs();ic++)
                        for (In id=I_0;id<mPesInfo.GetMolecule().GetNoOfVibs();id++)
                        {
                           for (In isvi=I_0;isvi<(*Svi).size();isvi++)
                              for (In jsvi=I_0;jsvi<(*Svi).size();jsvi++)
                                 if (ic == (*Svi)[isvi] )
                                    if ( id == (*Svi)[jsvi] )
                                       q_hess[ic][id]+=Nb(lc_map[(*Svi)])*grd_hess_points[ip][i_cont+mPesInfo.GetMolecule().GetNoOfVibs()];
                        i_cont++;
                         }
                     // debugging: prints the elements
                     if(gDebug) {
                        Mout << "The Gradient and the Hessian:" << endl;
                        for (In id=I_0; id<mPesInfo.GetMolecule().GetNoOfVibs(); id++)
                           Mout << q_grad[id] << " ";
                        Mout << endl;
                        Mout << endl;
                        for (In id=I_0; id<mPesInfo.GetMolecule().GetNoOfVibs(); id++)
                        {
                           for (In ic=I_0; ic<mPesInfo.GetMolecule().GetNoOfVibs(); ic++)
                              Mout << q_hess[id][ic] << " ";
                           Mout << endl;
                        }
                        Mout << endl;
                     }
                     //*/
                  }
               }
            }

            MidasVector q_vector(mPesInfo.GetMolecule().GetNoOfVibs());
                q_vector=(q_vec-q_vec_piece);
            // Calculates the taylor series UP TO SECOND ORDER
            Nb t_series;
            if (gPesCalcDef.GetmExtrapOnlyGradient() || order ==I_1)
               t_series=Dot(q_vector,(q_grad));
            else
            {
               /*MidasVector temp_temp = q_hess*q_vector; //FIXMEEEEEEEE!!!!!!!!
               temp_temp *= C_2;
               temp_temp += q_grad;
               t_series=Dot(q_vector,temp_temp);*/
               //t_series=Dot(q_vector,(q_grad + C_I_2*(q_hess*q_vector)));
               t_series=(q_vector*(q_grad + C_I_2*(q_hess*q_vector))).at();
            }
            denominator_weight += weight;
            extrap_value += t_series*weight;
            } // end loop over ModeRange pieces

         extrap_value /= denominator_weight; // that the extrapolated value
         // Dump extrap_value & q_vec into the .mbar file
         if (gPesIoLevel > I_11)
         {
               Mout << " For calc code : " << calc_code
                    << " extrapolated value is: " << extrap_value << std::endl;
         }
         for (In i_q=0; i_q<mode_combi.size(); i_q++)
         {
            bar_file.width(29);
            bar_file << q_vec[mode_combi[i_q]] << "   ";
         }
         bar_file << extrap_value << "  " << endl;
         if (loc_pot)
         {
            for (In i_q=0; i_q<mode_combi.size(); i_q++)
            {
               plotfilebar.width(29);
               plotfilebar << q_vec[mode_combi[i_q]]*sqrt(C_FAMU) << "  ";
            }
            plotfilebar << extrap_value << endl; //assumes the energy is the first property
            //Mout << " ending with plotting stuff " << endl;
         }
      }
      //close files
      bar_int_app.close();
      bar_file.close();
      plotfilebar.close();
      //vbar_file.close();
      //mPesInfo.ResetCalcCodeAndNr();
      if (gDebug)
      {
         Mout << "exiting extrapolation..." << std::endl;
      }
      delete[] s_start;
      delete[] s_end;
      delete[] s_step;
      for(In i = 0; i < n_combinations; i++)
         delete[] ModeIndexes[i];
      delete[] ModeIndexes;
   }
   


   if (gTime)
   {
      extrap_time.CpuOut(Mout, "CPU time used in extrapolation:");
      extrap_time.WallOut(Mout, "WALL time used in extrapolation:");
   }   
}
