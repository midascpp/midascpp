/**
************************************************************************
* 
* @file                Pes.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for the Pes module of MidasCPP 
* 
* Last modified: Fri Jun 11, 2010  03:06PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// midas header
#include "pes/Pes.h"

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Extensions.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "pes/AdjustInput.h"
#include "pes/Surface.h"
#include "pes/PesInfo.h"
#include "pes/ModeCoupling.h"
#include "pes/PesMpi.h"
#include "mpi/Impi.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/Trainer.h"
#include "coordinates/transformpot/TransformPot.h"

namespace midas
{
namespace pes
{

//! Setup directories for a surface calculation.
void SetupPesDirectories(const In& aNoSubSystems, const PesCalcDef& aPesCalcDef, const std::string& aPesMainDir);

//! Setup system information before surface calculation.
void SetupSystemInfo(const In& aNoSubSystems, const PesCalcDef& aPesCalcDef, DincrInfo& aDincrInfo, std::vector<midas::molecule::MoleculeInfo>& aSubSystemInfos);

//! Merge surfaces from different multilevel calculations of a single subsystem.
void FinalizeSystemSurface(const std::string& aSubSystemPath, const In& aSubSystemProp, const std::string& aPesMainDir);

/**
 * Run pes studies - pass control to various drivers.
 **/
void Pes
   (  PesCalcDef& aPesCalcDef
   ,  std::vector<PesInfo>& aPesInfos
   ,  const std::string& aPesMainDir
   )
{
   // The number of subsystems
   auto aNoSubSystems = aPesInfos.size();

   // Ouput subsystem header
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   if (aNoSubSystems == I_1)
   {
      OneArgOut72(Mout," Begin Surface Generation for " + std::to_string(aNoSubSystems) + " Subsystem ",'$');
   }
   else
   {
      OneArgOut72(Mout," Begin Surface Generation for " + std::to_string(aNoSubSystems) + " Subsystems ",'$');
   }
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
   
   // Master node controls construction of the pes
   if (midas::mpi::GlobalRank() == 0)
   {
      // Determine if symmetry is to be used and check that input to the pes block and molecule info are in accordance
      for (auto& pes_info : aPesInfos)
      {
         // Consider symmetry of molecule
         if (aPesCalcDef.GetmUseSym())
         {
            pes_info.SetPointGroup(aPesCalcDef.GetmPesSymLab());
         }
         else
         {
            // If symmetry is not to be used then point group is set to the one that contains only the identity symmetry operator 
            pes_info.SetPointGroup("C1"); 
         }
         
         // Print information on symmetry group and symmetry operators
         if (gPesIoLevel > I_8)
         {
            Mout << pes_info << std::endl;
         }
         
         // Get MoleculeInfo from PesInfo
         const auto& molecule = pes_info.GetMolecule();

         // Validate the pes-module input in relation to the molecular system input
         aPesCalcDef.ValidateInput(molecule.GetNumberOfNuclei(), molecule.GetNoOfVibs());
       
         // Set up Coriolis coupling elements for normal coordinates
         if (aPesCalcDef.GetmNormalcoordInPes())
         {
            if (gDebug || gPesIoLevel > I_10)
            {
               molecule.PrintNoOfFreq();
               molecule.PrintNoOfNuclei();
               molecule.PrintmCoordinates();
            }
         }
      
         // Output on polyspherical coordinates to be used in the surface calculation
         if (  aPesCalcDef.GetmPscInPes()
            && (  gDebug 
               || gPesIoLevel > I_10
               )
            )
         {
            molecule.PrintNoOfFreq();
            molecule.PrintNoOfNuclei();
            molecule.PrintmCoordinates();
         }
      
      }
      

      // Create a new surface that can be either static grid of Adga grid
      Surface* new_surface = Surface::Factory(aPesCalcDef.GetmPesAdga(), aPesInfos, aPesMainDir);

      // Run the surface
      new_surface->RunSurface();

      // Clean-up after 
      delete new_surface;
   }

   // Ouput subsystem footer
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   if (aNoSubSystems == I_1)
   {
      OneArgOut72(Mout," Surface Generation for Subsystem: Completed ",'$');
   }
   else
   {
      OneArgOut72(Mout," Surface Generation for Subsystems: Completed ",'$');
   }
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
}

/**
 * Driver for running fragment multilevel surface calculations
 **/
void PesDrv
   (
   )
{
   // Setup mpi 
   const midas::mpi::Communicator& comm = mpi::CommunicatorWorld();
   auto mpi_guard = midas::pes::InitMpi(comm);

   // Do we retrain several ML (at the moment only GPR)  models?
   bool use_ml_surface = gPesCalcDef.GetMaxMLRuns() > 0;

   // To test ML convergence for each system
   bool is_ml_final = false;
   std::vector<bool> is_ml_converged(gSystemDefs.size(),false);

   if (use_ml_surface)
   {
      Mout << std::endl;
      Out72Char(Mout,'$','$','$');
      Out72Char(Mout,'$',' ','$');
      OneArgOut72(Mout," Begin GPR guided Surface Generation ",'$');
      Out72Char(Mout,'$',' ','$');
      Out72Char(Mout,'$','$','$');
      Mout << std::endl;
   }

   // Loop over maximal number of ML models we want to retrain
   In nrun = 1;
   In istart = I_0;
   bool ml_restart = false;
 
   if (use_ml_surface) 
   {
      nrun = gPesCalcDef.GetMaxMLRuns();
   
      // Check if this is an restart
      std::string adga_gpr_info_file = gMainDir + "/System/adga_gpr.info";

      Trainer trainer(gPesCalcDef.GetmMLDriver(), gMainDir);

      istart = trainer.CheckRestart(adga_gpr_info_file);

      ml_restart = (istart > 0);

      if (ml_restart) 
      {
         Mout << "GPR guided Surface Generatation will be restarted from iteration " << istart << std::endl;
      }

   }
  
   for (In iml = istart; iml < nrun; ++iml)
   {
      // Total number of subsystems
      auto no_subsystems = I_1;

      // Labels for the fragment combinations in use
      std::vector<std::string> fc_names;

      // Setup a double incremental surface calculation using fragment combinations
      DincrInfo dincr_info(gPesCalcDef, gMainDir);
 
      // Initialize the fragment combination range for double incremental surface calculation 
      if (gPesCalcDef.GetmDincrSurface())
      {
         Mout << std::endl;
         Out72Char(Mout,'$','$','$');
         Out72Char(Mout,'$',' ','$');
         OneArgOut72(Mout," Begin Double Incremental Surface Generation ",'$');
         Out72Char(Mout,'$',' ','$');
         Out72Char(Mout,'$','$','$');
         Mout << std::endl;
   
         // Read information on individual fragment combinations
         dincr_info.InitializeFragmentCombinations(fc_names);
      
         // Determine the total number of subsystems
         no_subsystems = fc_names.size();
      }

      // Setup folder structure for a fragment multilevel surface calculation
      SetupPesDirectories(no_subsystems, gPesCalcDef, gMainDir);
   
      // Holds information on multilevel surface calculation for individual subsystems
      std::vector<std::string> subsystem_paths(no_subsystems, "");
      std::vector<In> subsystem_props(no_subsystems, I_0);

      // Read all molecule specific information on all subsystems
      std::vector<midas::molecule::MoleculeInfo> subsystems;
      SetupSystemInfo(no_subsystems, gPesCalcDef, dincr_info, subsystems);
 
      // Construct a PesInfo for each subsystem
      std::vector<PesInfo> pes_infos;
      pes_infos.reserve(subsystems.size());
      for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
      {
         // Each PesInfo contains the path to its subsystem directory
         auto subsystem_name     = "subsystem_" + std::to_string(isystem);
         auto subsystem_main_dir = gMainDir + "/System/" + subsystem_name;
         PesInfo pes_info(gPesCalcDef, subsystems[isystem], subsystem_main_dir, subsystem_name);
   
         // Emplace to vector holding all PesInfo objects
         pes_infos.emplace_back(std::move(pes_info));
      
         // Store information
         subsystem_paths[isystem] = subsystem_main_dir;
      }

      // If we doing a ML guided Surface generate training set, clean up and train...
      if (use_ml_surface)
      {
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         {
            Trainer trainer(gPesCalcDef.GetmMLDriver(), gMainDir);
   
            if (iml == 0) 
            {
               // Save current training set
               trainer.SaveTrainingSet(iml);
            }
            else
            {
               // Generate Training Set and save old property files for later reuse
               if (gPesCalcDef.GetmSaveDispGeom())
               {
   
                  for (In ilevel = 1; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
                  {
                     pes_infos[isystem].Update(gPesCalcDef, ilevel);
   
                     In number_of_prop = pes_infos[isystem].GetMultiLevelInfo().GetNoOfProps();
                     std::string save_io_dir = pes_infos[isystem].GetMultiLevelInfo().SaveIoDir();
                     std::string analysis_dir = pes_infos[isystem].GetMultiLevelInfo().AnalysisDir();
   
                     for (In iprop = 1; iprop <= number_of_prop; iprop++)
                     {
                        if ( gPesIoLevel > I_3 )
                        {
                           Mout << "Do GPR training for property " << iprop << std::endl;
                        }
                        bool remove_init_set = gPesCalcDef.GetMLRemoveInitSet();
                        
                        // Generate training data for each level
                        bool docleaning = true; // The property files will be saved in a different location
                        bool save_database = ( !remove_init_set || (remove_init_set  && iml > 1) );
 
                        // check if derivative information is present?
                        bool have_deriv_info = false;
                        In nderiv = I_0;
                        std::string der_file = pes_infos[isystem].GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(iprop - I_1).GetDerFile();
                        if (!der_file.empty())
                        {
                           have_deriv_info = true;
                           nderiv = 3 * pes_infos[isystem].GetMolecule().GetNumberOfNuclei();
                        }

                        trainer.DumpTrainingData(iprop, iml, is_ml_final, have_deriv_info, nderiv, save_io_dir, analysis_dir, docleaning, save_database); 
                        bool is_init = (iml == 0);
   
                        // Do an initial hyper parameter optimization and save Co-Variance matrix
                        trainer.TrainGPR(iprop, iml, is_init, is_ml_final, save_io_dir, analysis_dir);
   
                        // Save current training set for possible next iteration
                        if ( !remove_init_set || (remove_init_set  && iml > 0) )
                        {
                           trainer.SaveTrainingSet(iml);
                        }
                     }
                  }
               }
            }
         }
      }

      // Adapt the auxiliary modes grid boundaries in realation to the inter-connecting and intra-connection mode boundaries
      if (gPesCalcDef.GetmDincrMethod() == "DIFACT")
      {
         if (gPesIoLevel > I_4)
         {
            Mout << " Will adapt the auxiliary coordinate boundaries " << std::endl; 
         }
   
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         {
            dincr_info.AdaptAuxBoundaries(isystem, subsystems[isystem], pes_infos[isystem]);
         }
      }

      In number_of_properties = 0;

      // Run the multilevel surface calculation for all subsystems
      if (gPesCalcDef.GetmDincrSurfaceType() == "PROPERTY")
      {
         Pes(gPesCalcDef, pes_infos, gMainDir);
      }
      else if (gPesCalcDef.GetmDincrSurfaceType() == "GIVEN") 
      {
         // Calculate the number of properties
         std::string name_label = gMainDir + "/InterfaceFiles/midasifc.propinfo";
         std::ifstream calculated_properties(name_label.c_str(), std::ios_base::in);
      
         // Ensure file exists
         if (!calculated_properties)
         {
            MIDASERROR(" Error when trying to open file: " + name_label);
         }

         std::string ss;
         while (std::getline(calculated_properties, ss))
         {
            // Check for comment line
            std::size_t found_comment = ss.find("#");
            if (found_comment != std::string::npos)
            {
               continue;
            }

            ++number_of_properties;
         }

         // Close the PropertyInfo file again
         calculated_properties.close();
         
         // Check that .mop files exist and copy them
         Mout << "Copying pre-calculated operator files" << std::endl;
         if (number_of_properties == 1)
         {
            for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
            {
               auto subsystem_main_dir = gMainDir + "/System/subsystem_" + std::to_string(isystem);
               if (!midas::filesystem::Exists(gMainDir + "/FC_savedir/" + gSystemDefs[isystem].Name() + ".mop"))
               {
                  MIDASERROR(" Cannot find " + gSystemDefs[isystem].Name() + ".mop file ");
               }
             
               midas::filesystem::Copy
                  (  gMainDir + "/FC_savedir/" + gSystemDefs[isystem].Name() + ".mop"
                  ,  subsystem_main_dir + "/FinalSurfaces/savedir/" + "prop_no_1.mop"
                  );
            }

         }
         else if (number_of_properties > 1)
         {
            for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
            {
               auto subsystem_main_dir = gMainDir + "/System/subsystem_" + std::to_string(isystem);
               for (In iproperty = 0; iproperty < number_of_properties; ++iproperty)
               {

                  if (!midas::filesystem::Exists(gMainDir + "/FC_savedir/" + gSystemDefs[isystem].Name() + "_" + std::to_string(iproperty+1) + ".mop"))
                  {
                     MIDASERROR(" Cannot find " + gSystemDefs[isystem].Name() + "_" + std::to_string(iproperty+1) + ".mop file");
                  }
            
                  midas::filesystem::Copy
                     (  gMainDir + "/FC_savedir/" + gSystemDefs[isystem].Name() + "_" + std::to_string(iproperty+1) + ".mop"
                     ,  subsystem_main_dir + "/FinalSurfaces/savedir/" + "prop_no_" + std::to_string(iproperty+1)  +  ".mop"
                     );
               }
            }
         }
         else 
         {
            MIDASERROR(" Invalid number of properties ");
         }
      }
      else if (gPesCalcDef.GetmDincrSurfaceType() == "HESSIAN")
      {
         MIDASERROR("Hessian calculation method is not ready for use");
         
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         {
            dincr_info.DoHessianCalc(isystem, &subsystems[isystem], gModSysCalcDefs[isystem]);
         }
      }
         
      // Store information
      for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
      {
         subsystem_props[isystem] = pes_infos[isystem].GetmNoSurfaceProps();
      }

      if (gPesCalcDef.GetmDincrSurfaceType() == "GIVEN")
      {
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         {
            subsystem_props[isystem] = number_of_properties;
         }
      }

      // If using DIFACT method, then we need to transform auxiliary modes to FALCON modes
      if (gPesCalcDef.GetmDincrMethod() == "DIFACT")
      {
         if (gPesIoLevel > I_4)
         {
            Mout << " Transforming the fragment surfaces " << std::endl; 
         }
   
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         {
            dincr_info.TransformPot(isystem, subsystem_props[isystem], subsystems[isystem].GetModeNames());
         }
      }


      if (use_ml_surface)
      {
         for (In isystem = I_0; isystem < subsystems.size(); ++isystem)
         { 
            // Create local pes_info, in which we change a few things
            auto subsystem_name     = "subsystem_" + std::to_string(isystem);
            auto subsystem_main_dir = gMainDir + "/System/" + subsystem_name;
            PesInfo pes_info(gPesCalcDef, subsystems[isystem], subsystem_main_dir, subsystem_name);
            bool do_ml_pes = true; 
            pes_info.SetMLPes(do_ml_pes);
   
            // Info on mode couplings
            ModeCoupling mode_couplings(subsystems[isystem]);
   
            // CalculationList of SinglePoints we have to re-compute (without GPR)
            CalculationList calc_list( subsystems[isystem], pes_info );
   
            //-----------------------------------------------------------------+
            // Extract points with large uncertainty and add to new database
            //-----------------------------------------------------------------+
            if (gPesCalcDef.GetmSaveDispGeom())
            {
               Trainer trainer(gPesCalcDef.GetmMLDriver(), gMainDir);
   
               In nadd = I_0;
               In NumSmall = 4;
   
               for (In ilevel = 1; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
               {
                  pes_info.Update(gPesCalcDef, ilevel);
   
                  In number_of_prop = pes_info.GetMultiLevelInfo().GetNoOfProps();
                  std::string save_io_dir = pes_info.GetMultiLevelInfo().SaveIoDir();
                  std::string analysis_dir = pes_info.GetMultiLevelInfo().AnalysisDir();
   
                  bool init = (iml == 0);
                  bool iso_gpr = false; // GS for the moment
                  for (In iprop = 1; iprop <= number_of_prop; iprop++)
                  {
                     trainer.ExtendTrainingData(  calc_list
                                               ,  pes_info
                                               ,  mode_couplings 
                                               ,  iml
                                               ,  init
                                               ,  iso_gpr
                                               ,  nadd
                                               ,  iprop
                                               ,  save_io_dir
                                               ,  analysis_dir
                                               ); 
                  }
               }
   
               is_ml_converged[isystem] = (iml > 0) && (nadd < NumSmall); // Converge each system separatly
   
               // Prepare everything for the next run: Just having the exact data 
               for (In ilevel = 1; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
               {
                  pes_info.Update(gPesCalcDef, ilevel);
                  In number_of_prop = pes_info.GetMultiLevelInfo().GetNoOfProps();
                  std::string save_io_dir = pes_info.GetMultiLevelInfo().SaveIoDir();
                  std::string analysis_dir = pes_info.GetMultiLevelInfo().AnalysisDir();
   
                  for (In iprop = 1; iprop <= number_of_prop; iprop++)
                  {
                     trainer.PrepareForNextRun(iprop, iml, save_io_dir, analysis_dir);
                  }
               }
   
               // Evaluate new SinglePoint list
               if (!is_ml_converged[isystem])
               {
                  Mout << " Calculate new points for GPR" << std::endl << std::endl;
                  Mout << "   Found " + std::to_string(nadd) + " new points" << std::endl;
   
                  //calc_list.CheckForCalcsDone();
                  const bool transform_deriv = false;
                  calc_list.EvaluateList(transform_deriv);
               }
   
               std::string adga_gpr_info_file = gMainDir + "/System/adga_gpr.info";
               trainer.WriteCheckFile(adga_gpr_info_file,iml);
            }
         }
      }

      // Merge surfaces and potential boundaries for the individual subsystems into a final surface for the entire system
      if (gPesCalcDef.GetmDincrSurface())
      {
         //
         dincr_info.FinalizeFragmentSurface(subsystem_paths, subsystem_props, gMainDir + "/FinalSurfaces");

         Mout << std::endl;
         Out72Char(Mout,'$','$','$');
         Out72Char(Mout,'$',' ','$');
         OneArgOut72(Mout," Double Incremental Surface Generation: Completed ",'$');
         Out72Char(Mout,'$',' ','$');
         Out72Char(Mout,'$','$','$');
         Mout << std::endl;
      }
      else
      {
         if (  subsystem_paths.size() != subsystem_props.size()
            && subsystem_paths.size() != I_1
            && subsystem_props.size() != I_1
            )
         {
            MIDASERROR("Something have gone wrong with the information retrieved from fragment multilevel surface calculation");
         }
   
         FinalizeSystemSurface(subsystem_paths[I_0], subsystem_props[I_0], gMainDir);
      }
      
      //
      if (gPesCalcDef.GetmDincrSurfaceType() == "HESSIAN")
      {
         dincr_info.AnalyzeHessian();
      }


      if (use_ml_surface)
      {

         // Check if for all systems the ML training set is converged
         if (std::find(begin(is_ml_converged), end(is_ml_converged), false) == end(is_ml_converged)) 
         {
            Out72Char(Mout,'C','-','C');
            OneArgOut72(Mout," ",'C');
            OneArgOut72(Mout," Number of cycles : " + std::to_string(iml) , 'C');  
            OneArgOut72(Mout," ",'C');
            OneArgOut72(Mout," !!! GPR guided PES construction converged !!! ",'C');
            OneArgOut72(Mout," ",'C');
            Out72Char(Mout,'C','-','C');

            break;
         }

      }
   } 
   if (use_ml_surface)
   {
      Mout << std::endl;
      Out72Char(Mout,'$','$','$');
      Out72Char(Mout,'$',' ','$');
      OneArgOut72(Mout," GPR guided Surface Generation: Completed ",'$');
      Out72Char(Mout,'$',' ','$');
      Out72Char(Mout,'$','$','$');
      Mout << std::endl;
   }
}

/**
 * Setup directories for a surface calculation.
 **/
void SetupPesDirectories
   (  const In& aNoSubSystems
   ,  const PesCalcDef& aPesCalcDef
   ,  const std::string& aPesMainDir
   )
{
   // We are actually doing a multilevel calculation if this is the case
   if (aPesCalcDef.MultiLevel() != I_0) 
   {
      if (gPesIoLevel > I_1)
      {
         Mout << std::endl << " Initializing directory for multilevel calculation: " << std::endl;
         Mout << "  " << aPesMainDir << std::endl;
      }

      // Only the master rank will be allowed to setup the folder structure
      if (midas::mpi::IsMaster())
      {
         // Setup main directory, System directory and FinalSurfaces directory
         midas::filesystem::Mkdir(aPesMainDir);
         midas::filesystem::Mkdir(aPesMainDir + "/System");
         midas::filesystem::Mkdir(aPesMainDir + "/FinalSurfaces");
         midas::filesystem::Mkdir(aPesMainDir + "/FinalSurfaces/savedir");
         midas::filesystem::Mkdir(aPesMainDir + "/FinalSurfaces/analysis");
            
         // If the surface(s) is(are) to be generated via the Adga, then we create a directory in which the Adga-Vscf calculations can be performed
         if (aPesCalcDef.GetmPesAdga())
         {
            midas::filesystem::Mkdir(aPesMainDir + "/VibCalc");
            midas::filesystem::Mkdir(aPesMainDir + "/VibCalc/savedir");
            midas::filesystem::Mkdir(aPesMainDir + "/VibCalc/analysis");
         }

         // Loop over each subsystem and setup folder structure
         for (In isystem = I_0; isystem < aNoSubSystems; isystem++)
         {
            if (gPesIoLevel > I_1)
            {
               Mout << "  Creating folder structure and copying files for subsystem " << isystem << std::endl;
            }

            // Path for current subsystem
            auto subsystem = aPesMainDir + "/System/subsystem_" + std::to_string(isystem);

            // Setup FinalSurfaces directory for the individual subsystem/fragment
            midas::filesystem::Mkdir(subsystem);
            midas::filesystem::Mkdir(subsystem + "/FinalSurfaces");
            midas::filesystem::Mkdir(subsystem + "/FinalSurfaces/savedir");
            midas::filesystem::Mkdir(subsystem + "/FinalSurfaces/analysis");

            // Loop over each multilevel and setup folder structure
            for (In ilevel = I_0; ilevel < aPesCalcDef.MultiLevel(); ilevel++)
            {
               // Check for pre-opt boundaries keyword, else just continue for "level 0" (which is always the pre-opt level).
               if ((ilevel == I_0) && !aPesCalcDef.BoundariesPreOpt()) 
               {
                  // If we are not doing pre opt we do not create directories for this level, duh!
                  continue; 
               }
            
               if (gPesIoLevel > I_1)
               {
                  Mout << "   Creating folder structure and copying files for level " << ilevel << std::endl;
               }
               
               //
               auto s_level = subsystem + "/Multilevels/level_" + std::to_string(ilevel);
            
               // Make the directory Multilevels where the individual levels are placed
               midas::filesystem::Mkdir(subsystem + "/Multilevels");
               midas::filesystem::Mkdir(s_level);
       
               // Create savedir and analysis sub-directories for the level_i
               midas::filesystem::Mkdir(s_level + "/savedir");
               midas::filesystem::Mkdir(s_level + "/analysis");
       
               // If the setup sub-directory does not exists, then it is created and the appropriate interface files are copied to it 
               auto setup_subdirectory = s_level + "/setup";
               if (!midas::filesystem::Exists(setup_subdirectory))
               {
                  midas::filesystem::Mkdir(s_level + "/setup");
                
                  // Get information of interface files and associated paths
                  auto source_directory = aPesMainDir + "/InterfaceFiles/";
                  auto target_directory = setup_subdirectory + "/";
                  auto single_point_name = aPesCalcDef.GetSinglePointName(ilevel);
                  auto sp_info = SinglePointCalc(single_point_name).SpInfo();
                
                  // Copy interface files to setup directory and ensure that they are executable
                  if (sp_info.find("PROGRAM")->second == "SP_GENERIC" || sp_info.find("PROGRAM")->second == "SP_ML")
                  {
                     auto input_creator_script = sp_info.find("INPUTCREATORSCRIPT")->second;
                     if (!input_creator_script.empty())
                     {
                        midas::filesystem::Copy
                           (  source_directory + input_creator_script
                           ,  target_directory + input_creator_script
                           );
                        
                        if (!midas::filesystem::IsExecutable(target_directory + input_creator_script))
                        {
                           auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + input_creator_script);
                           midas::filesystem::Chmod
                              (  target_directory + input_creator_script
                              ,  S_IXUSR | file_permissions
                              );
                        }
                     }
                
                     auto run_script = sp_info.find("RUNSCRIPT")->second;
                     if (!run_script.empty())
                     {
                        midas::filesystem::Copy
                           (  source_directory + run_script
                           ,  target_directory + run_script
                           );
                        
                        if (!midas::filesystem::IsExecutable(target_directory + run_script))
                        {
                           auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + run_script);
                           midas::filesystem::Chmod
                              (  target_directory + run_script
                              ,  S_IXUSR | file_permissions
                              );
                        }
                     }
                     
                     auto validation_script = sp_info.find("VALIDATIONSCRIPT")->second;
                     if (!validation_script.empty())
                     {
                        midas::filesystem::Copy
                           (  source_directory + validation_script
                           ,  target_directory + validation_script
                           );
       
                        if (!midas::filesystem::IsExecutable(target_directory + validation_script))
                        {
                           auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + validation_script);
                           midas::filesystem::Chmod
                              (  target_directory + validation_script
                              ,  S_IXUSR | file_permissions
                              );
                        }
                     }

                     if (sp_info.find("PROGRAM")->second == "SP_ML")
                     {
                        auto input_creator_script = sp_info.find("FALLBACKINPUT")->second;
                        if (!input_creator_script.empty())
                        {
                           midas::filesystem::Copy
                              (  source_directory + input_creator_script
                              ,  target_directory + input_creator_script
                              );
                           
                           if (!midas::filesystem::IsExecutable(target_directory + input_creator_script))
                           {
                              auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + input_creator_script);
                              midas::filesystem::Chmod
                                 (  target_directory + input_creator_script
                                 ,  S_IXUSR | file_permissions
                                 );
                           }
                        }
                
                        auto run_script = sp_info.find("FALLBACKRUN")->second;
                        if (!run_script.empty())
                        {
                           midas::filesystem::Copy
                              (  source_directory + run_script
                              ,  target_directory + run_script
                              );
                           
                           if (!midas::filesystem::IsExecutable(target_directory + run_script))
                           {
                              auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + run_script);
                              midas::filesystem::Chmod
                                 (  target_directory + run_script
                                 ,  S_IXUSR | file_permissions
                                 );
                           }
                        }
                     }
                  }
                
                  // There will always need to be a file with property information
                  auto property_info_file = sp_info.find("PROPERTYINFO")->second;
                  if (!property_info_file.empty())
                  {
                     midas::filesystem::Copy
                        (  source_directory + property_info_file
                        ,  target_directory + property_info_file
                        );
                  }
                  
                  // If working with polyspherical coordinates, then copy the file containing the kinetic energy operator
                  if (aPesCalcDef.GetmPscInPes())
                  {
                     auto tana_run_file = aPesCalcDef.GetmTanaRunFile();
                     if (!tana_run_file.empty())
                     {
                        midas::filesystem::Copy
                           (  source_directory + tana_run_file
                           ,  target_directory + tana_run_file
                           );
                        
                        if (!midas::filesystem::IsExecutable(target_directory + tana_run_file))
                        {
                           auto file_permissions = midas::filesystem::GetFilePermissions(target_directory + tana_run_file);
                           midas::filesystem::Chmod
                              (  target_directory + tana_run_file
                              ,  S_IXUSR | file_permissions
                              );
                        }
                     }
                  }
               }
            }
         }

         if (gPesIoLevel > I_1)
         {
            Mout << std::endl;
         }
      }
   }
}

/**
 * Setup system information before surface calculation.
 **/
void SetupSystemInfo
   (  const In& aNoSubSystems
   ,  const PesCalcDef& aPesCalcDef
   ,  DincrInfo& aDincrInfo
   ,  std::vector<midas::molecule::MoleculeInfo>& aSubSystemInfos
   )
{
   // Setup mpi 
   const midas::mpi::Communicator& mpi_comm = mpi::CommunicatorWorld();

   // Loop over all subsystems and read the corresponding molecule information
   for (In isystem = I_0; isystem < aNoSubSystems; ++isystem)
   {
      // System (gSystemDefs) is a a collection of one or more subsystems (vector of SysDefs)
      auto subsystem_molecule_info = gSystemDefs[isystem].GetMoleculeFileInfo();

      // Read molecule information
      midas::molecule::MoleculeInfo subsystem_info
         =  [  &mpi_comm
            ,  &subsystem_molecule_info
            #ifdef VAR_MPI
            ,  &isystem
            #endif /* VAR_MPI */
            ]  () -> midas::molecule::MoleculeInfo
            {
               midas::mpi::SendSignal(mpi_comm, midas::pes::mpi_impl::signal::MOLECULEDATA);
               auto mpi_region_guard = midas::mpi::StartMpiRegion();

               // Broadcast subsystem number to all ranks
               #ifdef VAR_MPI
               midas::mpi::detail::WRAP_Bcast(&isystem, 1, midas::mpi::DataTypeTrait<In>::Get(), midas::mpi::MasterRank(), MPI_COMM_WORLD);
               #endif /* VAR_MPI */

               return subsystem_molecule_info;
            }();

      // Adjust molecular input depending on information given in molecule file
      AdjustMolecularInput(aPesCalcDef, subsystem_info, isystem);

      // Check that the subsystem has enough vibrational modes to accomodate the chosen mode combination level
      if (subsystem_info.GetNoOfVibs() < aPesCalcDef.GetmPesNumMCR())
      {
         MIDASERROR("Subsystem " + std::to_string(isystem) + " has " + std::to_string(subsystem_info.GetNoOfVibs()) + " vibrational modes but the maximal mode combination level is set to " + std::to_string(aPesCalcDef.GetmPesNumMCR()) + ", please reconsider");
      }

      // Store the subsystem information 
      aSubSystemInfos.push_back(std::move(subsystem_info));
   }
  
   // The DIFACT scheme needs additional molecule information, this will be read in and processed here
   if (aPesCalcDef.GetmDincrMethod() == "DIFACT")
   {
      // Loop over all additional .mmol files and read the corresponding molecule information
      std::vector<midas::molecule::MoleculeInfo> aux_molecule_info_vec;
      aux_molecule_info_vec.reserve(gSystemDefs.size() - aNoSubSystems);
      for (In iauxsystem = aNoSubSystems; iauxsystem < gSystemDefs.size(); ++iauxsystem)
      {
         // Get the correct molecule infor from global container
         auto aux_molecule_info = gSystemDefs[iauxsystem].GetMoleculeFileInfo();
     
         // Read molecule information
         midas::molecule::MoleculeInfo aux_subsystem_info = aux_molecule_info;

         aux_molecule_info_vec.emplace_back(std::move(aux_subsystem_info));
      }

      // The additional information that will need to be passed to the DincrInfo
      std::vector<In> inter_no_vibs_vec(aNoSubSystems, I_0);
      std::vector<In> rel_no_vibs_vec(aNoSubSystems, I_0);
      std::vector<In> frag_no_vibs_vec(aNoSubSystems, I_0);
      std::vector<std::vector<std::string> > inter_mode_names_vec(aNoSubSystems, std::vector<std::string>());
      std::vector<std::vector<std::string> > rel_mode_names_vec(aNoSubSystems, std::vector<std::string>());
      std::vector<std::vector<std::string> > frag_mode_names_vec(aNoSubSystems, std::vector<std::string>());
      std::vector<std::vector<Nb> > frag_freqs_vec(aNoSubSystems, std::vector<Nb>());
      std::vector<ScalingInfo> inter_scaling_info_vec;
      std::vector<MidasMatrix> inter_rel_rot_mat_vec;

      // Process the information
      inter_scaling_info_vec.reserve(aNoSubSystems);
      inter_rel_rot_mat_vec.reserve(aNoSubSystems);
      for (In isystem = I_0; isystem < aNoSubSystems; ++isystem)
      {
         // The three additional molecule files we need to consider
         auto inter_molecule = std::move(aux_molecule_info_vec[I_3 * isystem]);
         auto rel_molecule = std::move(aux_molecule_info_vec[I_3 * isystem + I_1]);
         auto fragment_molecule = std::move(aux_molecule_info_vec[I_3 * isystem + I_2]);
  
         // Information on the number fo vibrations and mode names
         inter_no_vibs_vec[isystem] = inter_molecule.GetNoOfVibs();
         rel_no_vibs_vec[isystem] = rel_molecule.GetNoOfVibs();
         frag_no_vibs_vec[isystem] = fragment_molecule.GetNoOfVibs();
         inter_mode_names_vec[isystem] = inter_molecule.GetModeNames();
         rel_mode_names_vec[isystem] = rel_molecule.GetModeNames();
         frag_mode_names_vec[isystem] = fragment_molecule.GetModeNames();

         // Frequencies for each entire subsystem
         std::vector<Nb> frag_freqs(frag_no_vibs_vec[isystem], C_0);
         for (In ifreq = I_0; ifreq < frag_no_vibs_vec[isystem]; ++ifreq)
         {
            frag_freqs[ifreq] = fragment_molecule.GetFreqI(ifreq) / C_AUTKAYS;
         }
         frag_freqs_vec[isystem] = frag_freqs;
      
         // Construct scaling info for inter-connecting modes 
         ScalingInfo inter_scaling_info(gPesCalcDef, inter_molecule);
         inter_scaling_info_vec.emplace_back(inter_scaling_info);

         // Construct transformaton matrix between inter-connecting and auxiliary modes
         MidasMatrix inter_rel_rot_mat(inter_no_vibs_vec[isystem], rel_no_vibs_vec[isystem], C_0);
         aDincrInfo.ConstructRotationMatrix
            (  inter_rel_rot_mat
            ,  inter_molecule
            ,  rel_molecule
            ,  inter_no_vibs_vec[isystem]
            ,  rel_no_vibs_vec[isystem]
            );
         inter_rel_rot_mat_vec.emplace_back(inter_rel_rot_mat);
      }
     
      // Then we store all information in the DincrInfo for later use
      aDincrInfo.SetmInterNoVibsVec(inter_no_vibs_vec);
      aDincrInfo.SetmRelNoVibsVec(rel_no_vibs_vec);
      aDincrInfo.SetmFragNoVibsVec(frag_no_vibs_vec);
      aDincrInfo.SetmInterModeNamesVec(inter_mode_names_vec);
      aDincrInfo.SetmRelModeNamesVec(rel_mode_names_vec);
      aDincrInfo.SetmFragModeNamesVec(frag_mode_names_vec);
      aDincrInfo.SetmFragFreqsVec(frag_freqs_vec);
      aDincrInfo.SetmInterScalingInfoVec(inter_scaling_info_vec);
      aDincrInfo.SetmInterRelRotMatVec(inter_rel_rot_mat_vec);
   }
}

/**
 * Merge surfaces from different multilevel calculations of a single subsystem.
**/
void FinalizeSystemSurface
   (  const std::string& aSubSystemPath
   ,  const In& aSubSystemProp
   ,  const std::string& aPesMainDir
   )
{
   auto source_directory = aSubSystemPath + "/FinalSurfaces";
   auto target_directory = aPesMainDir + "/FinalSurfaces";

   // Copy the file containing potential boundaries
   midas::filesystem::Copy
      (  source_directory + "/analysis/one_mode_grids.mbounds"
      ,  target_directory + "/analysis/one_mode_grids.mbounds"
      );
   
   // Copy the file containing Coriolis coupling matrices
   midas::filesystem::Copy
      (  source_directory + "/savedir/coriolis_matrices.mpesinfo"
      ,  target_directory + "/savedir/coriolis_matrices.mpesinfo"
      );
   
   // Copy the file containing the Inertia tensor
   midas::filesystem::Copy
      (  source_directory + "/savedir/inertia_tensor.mpesinfo"
      ,  target_directory + "/savedir/inertia_tensor.mpesinfo"
      );
   
   // If kinetic energy operator based on polyspherical coordinates are in use, then copy the file containing this
   if (gPesCalcDef.GetmPscInPes())
   {
      midas::filesystem::Copy
         (  source_directory + "/savedir/keo_psc.mop"
         ,  target_directory + "/savedir/keo_psc.mop"
         );
   }
   
   // Copy the file containing the Reference values for the calculated properties
   midas::filesystem::Copy
      (  source_directory + "/savedir/prop_ref_values.mpesinfo"
      ,  target_directory + "/savedir/prop_ref_values.mpesinfo"
      );
   
   // Copy the file containing property file identifiers
   midas::filesystem::Copy
      (  source_directory + "/savedir/prop_files.mpesinfo"
      ,  target_directory + "/savedir/prop_files.mpesinfo"
      );

   // Copy the property files 
   for (In iprop = I_0; iprop < aSubSystemProp; ++iprop)
   {
      midas::filesystem::Copy
         (  source_directory + "/savedir/prop_no_" + std::to_string(iprop + I_1) + ".mop"
         ,  target_directory + "/savedir/prop_no_" + std::to_string(iprop + I_1) + ".mop"
         );
   }
}

} /* namespace pes */
} /* namespace midas */
