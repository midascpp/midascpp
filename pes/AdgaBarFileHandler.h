/**
************************************************************************
* 
* @file                AdgaBarFileHandler.h
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ADGABARFILEHANDLER_H
#define ADGABARFILEHANDLER_H

// std headers
#include <map>
#include <vector>
#include <string>

// midas headers
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "inc_gen/TypeDefs.h"

// using declarations
typedef std::pair<In,In> ipair;

// forward declarations
class PesInfo;
class ItGridHandler;
class ModeCombiOpRange;
class GridType;
class Derivatives;
class PropertyFileStorage;

/**
 * Bar file handler for adga type pes.
 **/
class AdgaBarFileHandler 
   : public GridFileHandlerMethods
{
   private:

      //!
      ItGridHandler* mIterGrid;

      //!
      bool GenerateCalcName(std::string&, std::string&, const std::vector<In>&, const ModeCombi&);

      //!
      bool CheckDisplacement(const std::vector<In>&,const ModeCombi&,bool);

      //!
      void ModeDisplacements_zero
         (  const ModeCombi&
         ,  const std::vector<In>&
         ,  const GridType&
         ,  CalculationList&
         );

      //!
      void ModeDisplacements_one
         (  const ModeCombi&
         ,  const std::vector<In>&
         ,  MidasVector&
         ,  bool&
         ,  std::vector<Nb>&
         ,  const GridType&
         ,  Nb&
         ,  std::map<InVector,Nb>&
         ,  const Derivatives&
         ,  std::vector<MidasVector>&
         ,  const std::vector<PropertyFileStorage>&
         ,  std::vector<PropertyFileStorage>&
         );

      //!
      void DerivativesHelper
         (  const std::string&
         ,  In
         ,  std::map<std::string,Nb>::iterator&
         ,  std::vector<In>&
         ,  const In&
         ,  const Derivatives&
         ,  std::vector<MidasVector>&
         );

      //!
      Nb GetPotValFromOpFile(std::string, Uin);

   public:

      //!
      AdgaBarFileHandler(PesInfo& arPesInfo);

      //!
      void DoDispDer_zero(const ModeCombiOpRange& aModeCom, In aActualMcDim, const GridType& GpFr, CalculationList&);
      
      //!
      void DoDispDer_one(const ModeCombiOpRange&, In, const GridType&, const Derivatives&);
      
      //!
      void SetmIterGrid(ItGridHandler* aIterGrid)
      {
         mIterGrid = aIterGrid;
      }
};

#endif //ADGABARFILEHANDLER_H
