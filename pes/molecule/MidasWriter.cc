#include "MidasWriter.h"

#include "MoleculeInfo.h"
#include "input/GetLine.h"

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeWriterRegistration<MidasWriter> registerMidasWriter("MIDAS");

/*
 * Constructor
 */
MidasWriter::MidasWriter(const MoleculeFileInfo& aMoleculeFileInfo)
   :  MoleculeWriter(aMoleculeFileInfo)
{
   mMoleculeFile.FileStream().setf(std::ios::scientific);
   mMoleculeFile.FileStream().setf(std::ios::uppercase);
   mMoleculeFile.FileStream().precision(I_12);
}

/**
 *
 **/
bool MidasWriter::Initialize()
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("#0MIDASMOLECULE", midas::input::ParseInput))
   {
      mMoleculeFile.FileStream() << "#0MIDASMOLECULE\n"
                                 << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a #0MIDASMOLECULE block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 *
 **/
bool MidasWriter::Finalize()
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("#0MIDASMOLECULEEND", midas::input::ParseInput))
   {
      mMoleculeFile.FileStream() << "#0MIDASMOLECULEEND\n"
                                 << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a #0MIDASMOLECULEEND block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool MidasWriter::WriteXYZ(const XYZData& xyz)
{
   bool success = false;

   if(!mMoleculeFile.SearchKeyword("#1FREQ", midas::input::ParseInput))
   {
      // output first line
      mMoleculeFile.FileStream() << "#1XYZ\n"
                                 << xyz->NumAtom() << " " << xyz->Unit() << "\n"
                                 << xyz->Comment() << "\n";

      // output each atoms
      for(In i = 0; i < xyz->NumAtom(); ++i)
      {
         mMoleculeFile.FileStream() << xyz->Label(i) << " "
                                    << xyz->X(i) << " "
                                    << xyz->Y(i) << " "
                                    << xyz->Z(i) << " "
                                    << "ISO=" << xyz->Isotope(i) << " "        // as of now always writes
                                    << "SUB=" << xyz->SubSystem(i) << " "      // isotope and subsystem
                                    << "TREAT=" << xyz->TreatType(i) << "\n";  // treatment
      }

      // print aestetic \n and flush
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a #1XYZ block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool MidasWriter::WriteVibrationalCoord(const VibrationalCoordinateData& vib_coord)
{
   bool success = false;

   if(!mMoleculeFile.SearchKeyword("#1VIBCOORD", midas::input::ParseInput))
   {
      // print first line
      mMoleculeFile.FileStream() << "#1VIBCOORD\n"
                                 << vib_coord->Unit() <<"\n";

      // print coordinates
      for(In i = 0; i < vib_coord->NumCoords(); ++i)
      {
         mMoleculeFile.FileStream() << "#2COORDINATE\n";
         for(In j = 0; j < vib_coord->NumDisplacements(); ++j)
         {
            if( (j%3 == 0) && (j != 0) )
            {
               mMoleculeFile.FileStream() << "\n";
            }
            mMoleculeFile.FileStream() << vib_coord->Coordinate(i).at(j) << " ";
         }
         mMoleculeFile.FileStream() << "\n";
      }

      // print aestetic \n and flush to file
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a #1VIBCOORD block in file: " + mMoleculeFile.FileName());
   }

   return success;
}
/**
 *
 **/
bool MidasWriter::WriteSigma(const SigmaData& sigma)
{
   bool success = false;

   if(!mMoleculeFile.SearchKeyword("#1SIGMA", midas::input::ParseInput))
   {
      // print first line
      mMoleculeFile.FileStream() << "#1SIGMA\n"
                                 << sigma->Unit() <<"\n";

      // print coordinates
      for(In i = 0; i < sigma->NumSigma(); ++i)
      {
         mMoleculeFile.FileStream() << "#2SIGMACOORDINATE\n";
         for(In j = 0; j < sigma->NumDisplacements(); ++j)
         {
            if( (j%3 == 0) && (j != 0) )
            {
               mMoleculeFile.FileStream() << "\n";
            }
            mMoleculeFile.FileStream() << sigma->Sigma(i).at(j) << " ";
         }
         mMoleculeFile.FileStream() << "\n";
      }

      // print aestetic \n and flush to file
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a #1SIGMA block in file: " + mMoleculeFile.FileName());
   }

   return success;
}


/**
 *
 **/
bool MidasWriter::WriteFrequencies(const FrequencyData& freq)
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("#1FREQ", midas::input::ParseInput))
   {
      mMoleculeFile.FileStream() << "#1FREQ\n" << freq->NumFreq() << " cm-1\n";
      for(In i = 0; i < freq->NumFreq(); ++i)
      {
         mMoleculeFile.FileStream() << (freq->Frequency(i)) << " " << (freq->SymLabel(i)) << " " << (freq->ModeLabel(i)) <<"\n";
      }
      mMoleculeFile.FileStream() << "\n" << std::flush; // \n for aestetics and flush the output to file
      success = true;
   }
   else
   {
      MidasWarning("Already a #1FREQ block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 *
 **/
bool MidasWriter::WriteZmat(const ZmatData& zmat)
{
   MidasWarning("MidasWriter::WriteZmat() not implemented yet :C");
   return false;
}

/**
 *
 **/
bool MidasWriter::WriteHessian(const HessianData& hessian)
{
   bool success = false;

   if(!mMoleculeFile.SearchKeyword("#1HESSIAN", midas::input::ParseInput))
   {
      mMoleculeFile.FileStream() << "#1HESSIAN\n"
                                 << hessian->NumElem() << "\n";

      for(In j = 0; j < hessian->NumElem(); ++j)
      {
         mMoleculeFile.FileStream() << hessian->at(0,j); // output first elem without space
         for(In i = 1; i < hessian->NumElem(); ++i)
         {
            mMoleculeFile.FileStream() << " " << hessian->at(i,j); // rest of elements are printed with prepending space
         }
         mMoleculeFile.FileStream() << "\n"; // end row
      }

      mMoleculeFile.FileStream() << "\n" << std::flush; // \n for aestetics and flush
      success = true;
   }
   else
   {
      MidasWarning("Already a #1HESSIAN block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool MidasWriter::WriteGradientImpl
   (  const std::string& key
   ,  const GradientData& gradient
   )
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword(key, midas::input::ParseInput))
   {
      mMoleculeFile.FileStream() << key << "\n"
                                << gradient->NumElem() << "\n";

      mMoleculeFile.FileStream() << gradient->at(0);
      for(In j = 1; j < gradient->NumElem(); ++j)
         mMoleculeFile.FileStream() << " " << gradient->at(j);
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a " + key + " block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 *
 **/
bool MidasWriter::WriteGradient(const GradientData& gradient)
{
   return this->WriteGradientImpl("#1GRADIENT", gradient);
}


} /* namespace molecule */
} /* namespace midas */
