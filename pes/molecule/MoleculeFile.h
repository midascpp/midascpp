/**
************************************************************************
*
* @file                MoleculeFile.h
*
* Created:             06-06-2015
*
* Author:              Ian H. Godtliebsen  (ian@chem.au.dk)
*
* Short Description:
*
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MOLECULEFILE_H_INCLUDED
#define MOLECULEFILE_H_INCLUDED

#include <string>
#include <fstream>
#include <set>
#include <unordered_set>
#include <map>
#include <type_traits>
#include <functional> // for std::function


#include "libmda/numeric/float_eq.h"

#include "util/Error.h"
#include "util/Path.h"

namespace midas
{
namespace molecule
{

/**
 * molecule file fields (can hold up to 32 (=4 bytes, i.e size of int (NB: shifted up to 31)) different fields )
 * NB: remember to add new fields to the Map below as well
 **/
enum class molecule_field_t: int
   { XYZ             = (1<<0)
   , FREQ            = (1<<1)
   , VIBCOORD        = (1<<2)
   , ZMAT            = (1<<3)
   , HESSIAN         = (1<<4)
   , SELECTVIBS      = (1<<5)
   , LINCOMB         = (1<<6)
   , FINDZMAT        = (1<<7)
   , SIGMA           = (1<<8)
   , INTERNALCOORD   = (1<<9)
   , GRADIENT        = (1<<10)
   , INACTIVEMODES   = (1<<11)
   , EXSTGRADIENT    = (1<<12)      // Excited-state gradient
   };

// special operators and functions for molecule_field_t used for hashing in unordered_map
molecule_field_t operator|(const molecule_field_t& i, const molecule_field_t& j);

molecule_field_t operator&(const molecule_field_t& i, const molecule_field_t& j);

// class for holding molecule fields (works as a set)
class MoleculeFields
   :  public std::vector  < molecule_field_t >
{
   private:

   public:
      using std::vector<molecule_field_t>::vector;
      //! Conversion from string to molecule_field_t (with a sanity check)
      static molecule_field_t Map(const std::string& str)
      {
         static const std::map<std::string, molecule_field_t> map
            =  {  {"XYZ",molecule_field_t::XYZ}
               ,  {"FREQ",molecule_field_t::FREQ}
               ,  {"VIBCOORD",molecule_field_t::VIBCOORD}
               ,  {"INTERNALCOORD",molecule_field_t::INTERNALCOORD}
               ,  {"INACTIVEMODES",molecule_field_t::INACTIVEMODES}
               ,  {"ZMAT",molecule_field_t::ZMAT}
               ,  {"HESSIAN",molecule_field_t::HESSIAN}
               ,  {"SELECTVIBS",molecule_field_t::SELECTVIBS} // midas.mol specific
               ,  {"LINCOMB",molecule_field_t::LINCOMB}       // midas.mol specific
               ,  {"FINDZMAT",molecule_field_t::FINDZMAT} // midas.mol specific: should be removed
               ,  {"SIGMA",molecule_field_t::SIGMA} // midas.mol specific: should be removed
               ,  {"GRADIENT", molecule_field_t::GRADIENT}
               ,  {"EXSTGRADIENT", molecule_field_t::EXSTGRADIENT}
               };

         auto map_iter = map.find(str);
         MidasAssert(map_iter != map.end(), str + " is not a valid molecule field type.");
         return map_iter->second;
      }

      //! Constructor from initializer list
      MoleculeFields(std::initializer_list<molecule_field_t> l)
      {
         for(const auto& e : l)
         {
            this->InsertMoleculeField(e);
         }
      }

      //! Insertion function
      bool InsertMoleculeField(const molecule_field_t& aField)
      {
         auto test = std::find(this->begin(), this->end(), aField);
         if(test == this->end())
         {
            this->push_back(aField);
            return true;
         }
         return false;
      }
};

/**
 *
 **/
class MoleculeFileInfo
{
   private:
      //! Filename of molecule file
      std::string mFileName;
      //! Type of molecule file, e.g. "MIDAS", "TURBOMOLE", etc.
      std::string mType;
      //! Fields to read
      MoleculeFields mFields;
      //! Contents of molecular file. This can be loaded with some molecular data and will then be read instead of a file with mFileName.
      std::string mFileContents = "";

   public:
      //! Deleted default ctor
      MoleculeFileInfo() = delete;

      //! Constructor from file name and type.
      MoleculeFileInfo
         (  const std::string& aFileName
         ,  const std::string& aType
         )
         :  MoleculeFileInfo
               (  aFileName
               ,  aType
               ,  MoleculeFields()
               )
      {
      }

      //! Constructor from filename, type, and fields to read.
      MoleculeFileInfo
         (  const std::string& aFileName
         ,  const std::string& aType
         ,  const MoleculeFields& aFields
         );

      //!@{
      //! Defaulted copy/move constructor and copy assignment.
      MoleculeFileInfo(const MoleculeFileInfo&) = default;
      MoleculeFileInfo(MoleculeFileInfo&&) = default;
      MoleculeFileInfo& operator=(const MoleculeFileInfo&) = default;
      //!@}

      //!@{
      //! "Getters"
      std::string FileName() const { return mFileName; }
      std::string Type() const { return mType; }
      //!@}

      //!@{
      //! Set and get file contents internally in program without the use of files. Used for unit testing. Should maybe not be used for anything else.
      //! Set contents of file. This can be used instead of a filename. Used for unit testing purposes, to avoid having to deal with a file.
      void SetContents(const std::string& aFileContents) { mFileContents = aFileContents; }
      //! Get contents of file. Used for unit testing purposes to avoid have to deal with files.
      const std::string& GetContents() const { return mFileContents; }
      //!@}

      //! Check if file info is empty
      bool Empty() const { return mFileName.empty() || mType.empty(); }

      //! Check if fields are empty. If so, we will read all fields.
      bool EmptyFields() const { return mFields.empty(); }

      //! Check if file info contains a specific field.
      bool HasField(const molecule_field_t& aField) const
      {
         if(EmptyFields()) return true; // if fields are empty we hit everything!
         auto iter = std::find(mFields.begin(), mFields.end(), aField); // else check if field is present
         return iter != mFields.end();
      }

      //! Define type for bundle of file infos.
      using Set = std::vector<MoleculeFileInfo>;
};


/**
 *
 **/
class MoleculeFile
{
   //! Small file stream class to make pretty .mop output.
   class molecule_stream
      :  public std::stringstream
   {
      //! Make a space if float is positive.
      bool float_minus_space = true;
      //! Typedef for underlying stream type.
      using stream_type = std::stringstream;

      public:
         //! Constructor that always 'forward's ctor input to base.
         template<class... Ts>
         molecule_stream(Ts&&... ts): std::stringstream(std::forward<Ts>(ts)...)
         {
         }

         ////!
         //void swap(std::stringstream&& ss)
         //{
         //   std::stringstream::swap(ss);
         //}

         //!@{
         //! Overloads of output operator.
         //! awesome overloads for values.
         template<class T>
         molecule_stream& operator<<(T val)
         {
            static_cast<stream_type&>(*this) << val;
            return *this;
         }

         molecule_stream& operator<<(float val)
         {
            if(float_minus_space && libmda::numeric::float_pos(val)) // if positive number we print space instead of minus
            {
               static_cast<stream_type&>(*this) << " ";
            }
            static_cast<stream_type&>(*this) << val;
            return *this;
         }

         molecule_stream& operator<<(double val)
         {
            if(float_minus_space && libmda::numeric::float_pos(val)) // if positive number we print space instead of minus
            {
               static_cast<stream_type&>(*this) << " ";
            }
            static_cast<stream_type&>(*this) << val;
            return *this;
         }

         //! overloads of operator<< for manip functions.
         molecule_stream& operator<<(molecule_stream& (*pf)(molecule_stream&))
         {
            pf(*this);
            return *this;
         }

         molecule_stream& operator<<(std::ostream& (*pf)(std::ostream&))
         {
            static_cast<stream_type&>(*this) << pf;
            return *this;
         }

         molecule_stream& operator<<(std::ios& (*pf)(std::ios&))
         {
            static_cast<stream_type&>(*this) << pf;
            return *this;
         }

         molecule_stream& operator<<(std::ios_base& (*pf)(std::ios_base&))
         {
            static_cast<stream_type&>(*this) << pf;
            return *this;
         }
         //!@}
   };

   private:
      //! Molecular data stream.
      molecule_stream mFileStream;
      //! File info.
      MoleculeFileInfo mFileInfo;

      //!@{
      //! We do not allow default construction or copying.
      MoleculeFile() = delete;
      MoleculeFile(const MoleculeFile&) = delete;
      MoleculeFile& operator=(const MoleculeFile&) = delete;
      //!@}

   public:
      //! Constructor
      MoleculeFile(const MoleculeFileInfo& aInfo);

      //! Buffer
      bool Buffer();

      //! Flush
      bool Flush();

      //! Get the stream
      molecule_stream& FileStream() { return mFileStream; }

      //! Get filename
      std::string FileName() const { return mFileInfo.FileName(); }

      //! Get file type
      std::string Type() const { return mFileInfo.Type(); }

      //! See if we have specific field
      bool HasField(const molecule_field_t& aField) const { return mFileInfo.HasField(aField); }

      //! Search for a keyword with callback
      bool SearchKeyword
         (  const std::string& aKeyword
         ,  std::function<std::string(const std::string&)> aCallBack = [](const std::string& str){ return str; }
         );
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MOLECULEFILE_H_INCLUDED */
