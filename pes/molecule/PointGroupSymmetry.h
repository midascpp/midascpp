/*
************************************************************************
* 
* @file                PointGroupSymmetry.h
*
* Created:             06-01-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Interface to symmetry stuff.
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_MOLECULE_POINT_GROUP_SYMMETRY_H_INCLUDED
#define MIDAS_MOLECULE_POINT_GROUP_SYMMETRY_H_INCLUDED

#include <string>

#include "pes/molecule/OperatorRotation.h"
#include "nuclei/Nuclei.h"

struct SymmetryThresholds;

namespace midas
{
namespace molecule
{

// Forward declare some stuff
class SymInfo;
class MoleculeInfo;

/**
 * Class defining a point group by a label and a set of axis' that uniquely defines the point group.
 * */
class PointGroup
{
   public: 
      // Define some types.
      using axis_vector = std::vector<OperatorRotation::Axis>;

   private:
      
      //! Point group name.
      std::string mPointGroup;
      //! Set of axis defining the pointgroup uniquely.
      axis_vector mAxis;

   public:
      //! Constructor from string and axis_vector.
      PointGroup(const std::string&, const axis_vector&);

      //! Get point group label.
      const std::string& GetPointGroup() const;

      //! Get point group axis'.
      const axis_vector& GetAxis() const;
};

//! Find molecular point group
PointGroup IdentifyPointGroup(const MoleculeInfo& aMolecule, const SymmetryThresholds&);

//! Do symmetry analysis, e.i. find point group and irreps of vibrational coordinates. Modifies molecule.
SymInfo SymmetryAnalysis(MoleculeInfo& aMolecule, const SymmetryThresholds&);

//! Do symmetry analysis, e.i. find irreps of vibrational coordinates for given point group. Modifies molecule.
SymInfo PseudoSymmetryAnalysis(MoleculeInfo& aMolecule, const SymmetryThresholds&);


} /* namespace molecule */
} /* namespace midas */

#endif /* MIDAS_MOLECULE_POINT_GROUP_SYMMETRY_H_INCLUDED */
