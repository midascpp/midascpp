#ifndef MINFOWRITER_H_INCLUDED
#define MINFOWRITER_H_INCLUDED

#include <string>
#include <vector>
#include "MoleculeWriter.h"

namespace midas
{
namespace molecule
{

/**
 * Writer for midas type molecular files.
 **/
class SindoWriter
   :  public MoleculeWriter
{
   private:
      SindoWriter() = delete;
      SindoWriter(const SindoWriter&) = delete;
      SindoWriter& operator=(const SindoWriter&) = delete;

      bool Initialize();
      bool Finalize();

      bool WriteFrequencies(const FrequencyData&);
      bool WriteXYZ(const XYZData&);
      bool WriteVibrationalCoord(const VibrationalCoordinateData&);
      bool WriteSigma(const SigmaData&)
             {MidasWarning("Sigma writer is not implemented for minfo"); return false;}
      bool WriteZmat(const ZmatData&)
             {MidasWarning("ZMAT writer is not implemented for minfo"); return false;}
      bool WriteHessian(const HessianData&);
      bool WriteGradient(const GradientData&);
            
      //Functions for putting in block is they are not present
      void WriteElectronicBlock();
      void WriteVibrationalBlock();
   public:
      //! Constructor
      SindoWriter(const MoleculeFileInfo& aMoleculeFileInfo);
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MINFOWRITER_H_INCLUDED */
