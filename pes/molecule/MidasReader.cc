/**
************************************************************************
*
* @file                MidasReader.cc
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of MidasReader class used to read
*                      MidasCpp molecule input files.
*
* Last modified:       09-08-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "MidasReader.h"

#include <string>

#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/Trim.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "util/conversions/FromString.h"
#include "pes/molecule/MoleculeData.h"
#include "pes/molecule/MoleculeInfo.h"

NucTreatType NucTreatTypeFromString(const std::string& arString);

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeReaderRegistration<MidasReader> registerMidasReader("MIDAS");

/**
 * parse keyword string (removes spaces and translates all letters to uppercase)
 **/
typename MidasReader::key_t MidasReader::ParseKey(const key_t& aKey) const
{
   return midas::input::ParseInput(midas::input::Trim(aKey));
}

/**
 * read xyz input
 **/
typename MidasReader::return_t MidasReader::ReadXYZ(const key_t& aKey, MoleculeInfo& aMolecule)
{
   XYZData xyz(XYZType::Key());
   xyz.Initialize(MoleculeDataInput());

   // read first line with number of atoms and units
   string s;
   midas::input::GetLine(mMoleculeFile.FileStream(),s);
   vector<string> first_line = midas::util::StringVectorFromString(s);

   //If we do not have exactly two entries on the first line something is wrong
   if(first_line.size() != I_2)
   {
      MIDASERROR("XYZ input, first line did not have 2 entries.");
   }

   xyz->NumAtom() = midas::util::FromString<In>(first_line[0]);
   xyz->Unit() = midas::input::ToUpperCase(first_line[1]);

   // read comment line
   midas::input::GetLine(mMoleculeFile.FileStream(),s);
   xyz->Comment() = s;

   // read atom lines
   for (In atom_idx = 0; atom_idx < xyz->NumAtom(); ++atom_idx)
   {
      Nb x,y,z;
      std::string label;
      In isotope_number = 0;
      In sub_system = 0;
      NucTreatType type=NucTreatType::ACTIVE;

      midas::input::GetLine(mMoleculeFile.FileStream(),s);
      std::istringstream str_stream(s);
      str_stream >> label >> x >> y >> z;

      // read in optional arguments in the form: OPTIONAL_ARG=#ARG#
      std::string optional;
      while(str_stream >> optional)
      {
         std::vector<std::string> optional_arg = midas::util::StringVectorFromString(optional,'=');
         if(optional_arg.size() != 2) MIDASERROR("Optional argument not given correctly: " + optional);
         midas::input::ToUpperCase(optional_arg[0]);

         if(optional_arg[0] == "ISO")
         {
            isotope_number = midas::util::FromString<In>(optional_arg[1]);
         }
         else if(optional_arg[0] == "SUB")
         {
            sub_system = midas::util::FromString<In>(optional_arg[1]);
         }
         else if(optional_arg[0] == "TREAT")
         {
            type=NucTreatTypeFromString(optional_arg[1]);
         }
         else
         {
            MidasWarning("Unknown optional argument: " + optional);
         }

      }

      xyz->AddAtom(label,x,y,z,isotope_number,sub_system,type);
   }

   // set data and return
   aMolecule.InitXYZ(xyz);
   return libmda::util::ret(s,false);
}

/**
 * read selected vibs input
 **/
typename MidasReader::return_t MidasReader::ReadSelectVibs
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   SelectVibsData select_vibs(SelectVibsType::Key());
   select_vibs.Initialize(MoleculeDataInput());

   std::string str;
   while (midas::input::GetLine(mMoleculeFile.FileStream(), str) && midas::input::NotKeyword(str))
   {
      // If line is blank we read the next one
      if (str.empty())
      {
         continue;
      }

      // Check that we have only numbers
      if (std::string::npos != str.find_first_not_of("0123456789 "))
      {
         MIDASERROR("You are not allowed to have anything but numbers on the input to SELECTVIBS");
      }

      // Then read input
      std::istringstream str_stream(str);
      In number;
      while (str_stream >> number)
      {
         select_vibs->AddSelectVib(number);
      }
   }

   // Set data and return
   aMolecule.InitSelectVibs(select_vibs);

   // We have read until next keyword
   return libmda::util::ret(str, true);
}

/**
 * Read input on inactive modes
**/
typename MidasReader::return_t MidasReader::ReadInactiveModes
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   InactiveModesData inactive_modes(InactiveModesType::Key());
   inactive_modes.Initialize(MoleculeDataInput());
   
   std::string str;
   while (midas::input::GetLine(mMoleculeFile.FileStream(), str) && midas::input::NotKeyword(str))
   {
      // If line is blank we read the next one
      if (str.empty())
      {
         continue;
      }
      
      // Read a line with a string of mode labels
      std::vector<std::string> inactive_mode_labels = midas::util::StringVectorFromString(str);
      inactive_modes->SetmInactiveModeLabels(inactive_mode_labels);
      inactive_modes->SetmNoInactiveModes(inactive_mode_labels.size());
   }

   // Store the data on inactive modes
   aMolecule.InitInactiveModes(inactive_modes);
   
   // We have read until next keyword
   return libmda::util::ret(str, true);
}

/***
 * Read frequency input
 *
 * #1FREQUENCIES:
 *
 *    Example: #1FREQUENCIES
 *             3 cm-1
 *             1528 A Q1
 *             3011 A Q2
 *             3142 A Q3
 *
 ***/
typename MidasReader::return_t MidasReader::ReadFrequencies
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   FrequencyData freqs(FrequencyType::Key());
   freqs.Initialize(MoleculeDataInput());

   // Read first line with number of freqs and units
   std::string str;
   midas::input::GetLine(mMoleculeFile.FileStream(), str);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(str);

   if (first_line.size() != I_2)
   {
      MIDASERROR("FREQ first line wrong: " + str);
   }

   freqs->NumFreq() = midas::util::FromString<In>(first_line[0]);
   freqs->Unit() = midas::input::ToUpperCase(first_line[1]);

   // Read frequencies
   for (In freq_idx = I_0; freq_idx < freqs->NumFreq(); ++freq_idx)
   {
      midas::input::GetLine(mMoleculeFile.FileStream(), str);
      std::vector<std::string> freq_line = midas::util::StringVectorFromString(str);

      // assert 1 <= freq_line.size() <= 2
      MidasAssert((I_1 <= freq_line.size()) && (freq_line.size() <= I_3), "FREQ data given incorrectly: " + str);

      // Check for imaginary/negative frequencies
      auto freq = midas::util::FromString<Nb>(freq_line[I_0]);
      if (freq < C_0)
      {
         MidasWarning("Found a negative frequency: " + std::to_string(freq) + ", will set this to a positive value before using it");

         freq *= C_M_1;
      }

      // Check for the amount of information given, i.e. frequency, symmetry label and mode label
      if (freq_line.size() == I_1)
      {
         freqs->AddFrequency(freq, "A");
      }
      else if (freq_line.size() == I_2)
      {
         freqs->AddFrequency(freq, freq_line[I_1]);
      }
      else if (freq_line.size() == I_3)
      {
         freqs->AddFrequency(freq, freq_line[I_1], freq_line[I_2]);
      }
   }

   // Set data and return
   aMolecule.InitFrequencies(freqs);
   return libmda::util::ret(str, false);
}

/***
 * Read vibrational coordinates. The format is as follows
 *
 * #1VIBCOORD: Starts the input, and is followed by a single line with the units used.
 *
 *    Units: AU (atomic units), AA (Aangstrom)
 *
 *    Example: #1VIBCOORD
 *             au
 *
 * #2COORDINATE: Define a vibrational coordinate, as a diplacement vector.
 *               (As of now only normal coordinates can be used given, but at some point other types might be added)
 *
 *    Example: #2COORDINATE
 *             -6.7619400810377511E-02  0.0000000000000000E+00  9.6240164893656664E-08
 *              5.3658571933560051E-01  0.0000000000000000E+00 -4.1462278165119559E-01
 *              5.3658330139016164E-01  0.0000000000000000E+00  4.1462125424985669E-01
 *
 ***/
typename MidasReader::return_t MidasReader::ReadVibrationalCoord
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   VibrationalCoordinateData vib_coord(VibrationalCoordinateType::Key());
   vib_coord.Initialize(MoleculeDataInput());

   // read first line
   std::string str;
   midas::input::GetLine(mMoleculeFile.FileStream(),str);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(str);

   if(first_line.size() != 1) // should only hold which units to use
   {
      MIDASERROR("VIBCOORD first line wrong: " + str);
   }

   vib_coord->Unit() = midas::input::ToUpperCase(first_line[0]);

   // read coordinate input
   midas::input::GetLine(mMoleculeFile.FileStream(),str);
   while(midas::input::ParseInput(str) == "#2COORDINATE")
   {
      std::vector<Nb> coordinate;
      while(midas::input::GetLine(mMoleculeFile.FileStream(),str) && midas::input::NotKeyword(str))
      {
         if(str.empty()) continue; // if line is blank we read the next one
         std::istringstream str_stream(str);
         Nb number;
         while(str_stream >> number) coordinate.emplace_back(number);
      }
      vib_coord->AddCoordinate(coordinate);
   }

   // set data and return
   aMolecule.InitVibrationalCoordinates(vib_coord);
   return libmda::util::ret(str,true); // we have read next line
}

/**
 * Read internal (curvilinear) coordinates. The format is as follows:
 *
 * #1 INTERNALCOORD: Starts the input, and is followed by a single line with units of distance and angles
 *
 *    Units: AU (atomic units) or AA (Aangstrom) and  RAD (Radian) or DEG (Degree)
 *
 *    Then follows a list of the internal coordinates, where
 *    the first column gives the value of the coordinate,
 *    the second gives the type of coordinate (R = distance, V = valence angle between 2 atoms, D = dihedral angle between 3 atoms)
 *    and the third gives the coordinate name
 *
 *    Example: #1INTERNALCOORD
 *             au rad
 *              1.8100485448043131   R1   Q0
 *              1.8100485448043131   R2   Q1
 *              1.8240044419185952   A2   Q2
 *
**/
typename MidasReader::return_t MidasReader::ReadInternalCoord
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   InternalCoordData internal_coord(InternalCoordType::Key());
   internal_coord.Initialize(MoleculeDataInput());

   // Read first line with number of internal coordinates and units
   std::string str;
   midas::input::GetLine(mMoleculeFile.FileStream(), str);
   auto first_line = midas::util::TupleFromString<In, std::string, std::string>(str);

   internal_coord->SetmNoInternalCoords(std::get<I_0>(first_line));
   internal_coord->SetmUnitDist(midas::input::ToUpperCase(std::get<I_1>(first_line)));
   internal_coord->SetmUnitAng(midas::input::ToUpperCase(std::get<I_2>(first_line)));

   while (midas::input::GetLine(mMoleculeFile.FileStream(), str) && midas::input::NotKeyword(str))
   {
      // If line is blank we read the next one
      if (str.empty())
      {
         continue;
      }
      std::vector<std::string> coordinate = midas::util::StringVectorFromString(str);

      // Check if internal coordinate type and mode labels are present
      if (coordinate.size() == I_1)
      {
         MIDASERROR("The internal coordinates under #1 INTERNALCOORD have not been given a type label");
      }
      else if (coordinate.size() == I_2)
      {
         internal_coord->AddCoordinate
            (  midas::util::FromString<Nb>(coordinate[I_0])
            ,  midas::input::ToUpperCase(coordinate[I_1])
            );
      }
      else if (coordinate.size() == I_3)
      {
         internal_coord->AddCoordinate
            (  midas::util::FromString<Nb>(coordinate[I_0])
            ,  midas::input::ToUpperCase(coordinate[I_1])
            ,  midas::input::ToUpperCase(coordinate[I_2])
            );
      }
      else
      {
         MIDASERROR("Number of columns under #1 INTERNALCOORD in the molecule file is incorrect");
      }
   }

   aMolecule.InitInternalCoord(internal_coord);
   return libmda::util::ret(str, true); // we have read next line
}

/**
 * Read Sigma. The format is as follows
 *
 * #1SIGMA: Starts the input, and is followed by a single line with the units used.
 *
 *    Units: AU (atomic units), AA (Aangstrom)
 *
 *    Example: #1SIGMA
 *             au
 *
 * #2SIGMACOORDINATE: Define a vibrational coordinate, as a diplacement vector.
 *               (As of now only normal coordinates can be used given, but at some point other types might be added)
 *
 *    Example: #2SIGMACOORDINATE
 *             -6.7619400810377511E-02  0.0000000000000000E+00  9.6240164893656664E-08
 *              5.3658571933560051E-01  0.0000000000000000E+00 -4.1462278165119559E-01
 *              5.3658330139016164E-01  0.0000000000000000E+00  4.1462125424985669E-01
 *
 **/
typename MidasReader::return_t MidasReader::ReadSigma
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   SigmaData sigma(SigmaType::Key());
   sigma.Initialize(MoleculeDataInput());

   // read first line
   std::string str;
   midas::input::GetLine(mMoleculeFile.FileStream(),str);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(str);

   if(first_line.size() != 1) // should only hold which units to use
   {
      MIDASERROR("SIGMA first line wrong: " + str);
   }

   sigma->Unit() = midas::input::ToUpperCase(first_line[0]);

   // read coordinate input
   midas::input::GetLine(mMoleculeFile.FileStream(),str);
   while(midas::input::ParseInput(str) == "#2SIGMACOORDINATE")
   {
      std::vector<Nb> coordinate;
      while(midas::input::GetLine(mMoleculeFile.FileStream(),str) && midas::input::NotKeyword(str))
      {
         if(str.empty()) continue; // if line is blank we read the next one
         std::istringstream str_stream(str);
         Nb number;
         while(str_stream >> number) coordinate.emplace_back(number);
      }
      sigma->AddCoordinate(coordinate);
   }
   // set data and return
   aMolecule.InitSigma(sigma);
   return libmda::util::ret(str,true); // we have read next line
}

/***
 * Read input for linear combination of vibrational coordinates
 *
 * #1LINCOMB: Give orthogonal linear combination of coordinates
 *
 *    Example: #1LINCOMB
 *             0    0    1
 *             0.5  0.5  0
 *             0.5 -0.5  0
 *
 * SHOULD THIS BE UNDER VIBCOORD??
 ***/
typename MidasReader::return_t MidasReader::ReadLinComb(const key_t& aKey, MoleculeInfo& aMolecule)
{
   LinCombData lin_comb(LinCombType::Key());
   lin_comb.Initialize(MoleculeDataInput());

   std::string str;
   while(midas::input::GetLine(mMoleculeFile.FileStream(),str) && midas::input::NotKeyword(str))
   {
      std::vector<Nb> lin_comb_line;
      std::istringstream str_stream(str);
      Nb number;
      while(str_stream >> number) lin_comb_line.emplace_back(number);
      lin_comb->AddLinComb(lin_comb_line);
   }

   // set data and return
   aMolecule.InitLinComb(lin_comb);
   return libmda::util::ret(str,true);
}

/***
 * Read gradient information
 *
 * #1GRADIENT: Give one number telling number of elements, subsequent line containst the data
 *
 *    Example: #1GRADIENT
 *             6
 *             1.0 2.0 3.0 4.0 5.0 6.0
 *
 ***/

typename MidasReader::return_t MidasReader::ReadGradient
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   std::string keyword = "#1GRADIENT";

   GradientData gradient(GradientType::Key());
   gradient.Initialize(MoleculeDataInput());

   std::string s;
   midas::input::GetLine(mMoleculeFile.FileStream(),s);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(s);
   if(first_line.size() != 1) MIDASERROR(keyword + ": first line wrong: " + s);

   In num_elem = midas::util::FromString<In>(first_line[0]);
   gradient->SetNewSize(num_elem);

   midas::input::GetLine(mMoleculeFile.FileStream(),s);
   std::vector<Nb> grad_std = midas::util::VectorFromString<Nb>(s);

   if(grad_std.size() != num_elem) MIDASERROR(keyword + ": Incorrect number of elements.");
   for(In i = 0; i < num_elem; ++i) gradient->at(i) = grad_std[i];

   aMolecule.InitGradient(gradient);
   return libmda::util::ret(s,false);
}

/***
 * Read hessian information
 *
 * #1HESSIAN: Give one number telling number of rows and cols (must be square)
 *            and subsequent lines define the matrix.
 *
 *    Example: #1HESSIAN
 *             6
 *             1 0 0 0 0 0
 *             0 1 0 0 0 0
 *             0 0 1 0 0 0
 *             0 0 0 1 0 0
 *             0 0 0 0 1 0
 *             0 0 0 0 0 1
 *
 ***/
typename MidasReader::return_t MidasReader::ReadHessian(const key_t& aKey, MoleculeInfo& aMolecule)
{
   HessianData hessian(HessianType::Key());
   hessian.Initialize(MoleculeDataInput());

   // get first line with number of rows/cols (only one number, matrix is square)
   std::string s;
   midas::input::GetLine(mMoleculeFile.FileStream(),s);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(s);
   if(first_line.size() != 1) MIDASERROR("#1HESSIAN: first line wrong: " + s);

   In num_elem = midas::util::FromString<In>(first_line[0]);
   hessian->SetNewSize(num_elem,num_elem);

   In counter = 0;
   while(midas::input::GetLine(mMoleculeFile.FileStream(),s) && midas::input::NotKeyword(s) && counter < num_elem)
   {
      if(s.empty()) continue; // if line is blank we read the next one

      std::vector<std::string> hess_line = midas::util::StringVectorFromString(s);
      if(hess_line.size() != num_elem) MIDASERROR("#1HESSIAN: Incorrect number of cols.");

      for(In i = 0; i< hess_line.size(); ++i)
      {
         hessian->at(counter,i) = midas::util::FromString<Nb>(hess_line.at(i));
      }

      ++counter;
   }

   if(counter != num_elem) MIDASERROR("#1HESSIAN: Incorrect number of rows.");

   // set data and return
   aMolecule.InitHessian(hessian);
   return libmda::util::ret(s,false);
}

} /* namespace molecule */
} /* namespace midas */
