/**
************************************************************************
*
* @file                SindoReader.cc
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of SindoReader class used to read
*                      SindoCpp molecule input files.
*
* Last modified: March, 26th 2015
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "SindoReader.h"

#include <string>

#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/Trim.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/FromString.h"
#include "pes/molecule/MoleculeData.h"

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeReaderRegistration<SindoReader> registerMinfoReader("MINFO");
MoleculeReaderRegistration<SindoReader> registerSindoReader("SINDO");

/**
 * @brief check if string is a Sindo keyword keyword
 **/
bool SindoReader::IsKeyword(const std::string& s) const
{
   if(s.substr(0,1) == "[")
   {
      return true;
   }
   return false;
}

/**
 * @brief read atomic data input
 **/
typename SindoReader::return_t SindoReader::ReadAtomicData(const key_t& aKey, MoleculeInfo& aMolecule)
{
   Mout << " READING ATOMIC DATA " << std::endl;
   XYZData xyz(XYZType::Key());
   xyz.Initialize(MoleculeDataInput());

   // read first line with number of atoms
   std::string s;
   std::getline(mMoleculeFile.FileStream(),s);
   int atom_count = midas::util::FromString<In>(s);

   xyz->NumAtom() = atom_count;
   xyz->Unit() = "AU"; // I think sindo reads in au

   // read atoms
   int count = 0;
   while(std::getline(mMoleculeFile.FileStream(),s) && !IsKeyword(s) && count < atom_count)
   {
      std::vector<std::string> line = midas::util::StringVectorFromString(s,',');
      if(line.size() != 6) MIDASERROR("Error when reading line: " + s);
      line[0].erase(remove_if(line[0].begin(), line[0].end(), isspace), line[0].end()); // Remove all spaces in the atomname
      xyz->AddAtom(line[0]
                 , midas::util::FromString<Nb>(line[3])
                 , midas::util::FromString<Nb>(line[4])
                 , midas::util::FromString<Nb>(line[5])
                 , 0
                 );
      ++count;
   }

   // set data and return
   aMolecule.InitXYZ(xyz);
   return libmda::util::ret(s,false);
}

/**
 * @brief read vibrational data input
 **/
typename SindoReader::return_t SindoReader::ReadVibrationalData(const key_t& aKey, MoleculeInfo& aMolecule)
{
   Mout << " READING VIBRATIONAL DATA " << std::endl;
   SindoReader::SindoVibrationalReader vib_read(mMoleculeFile);
   std::string s = aKey;
   return vib_read.ReadKeys(s,false,aMolecule);
}

/**
 * @brief read electronic data input
 **/
 typename SindoReader::return_t SindoReader::ReadElectronicData(const key_t& aKey, MoleculeInfo& aMolecule)
 {
    Mout << " READING ELECTRONIC DATA " << std::endl;
    SindoReader::SindoElectronicReader ele_read(mMoleculeFile);
    std::string s = aKey;
    return ele_read.ReadKeys(s,false,aMolecule);
}

/**
 * @brief read vibrational frequencies from Sindo molecule file
 **/
typename SindoReader::SindoVibrationalReader::return_t
SindoReader::SindoVibrationalReader::ReadFrequencies(const key_t& aKey, MoleculeInfo& aMolecule)
{
   FrequencyData freqs(FrequencyType::Key());
   freqs.Initialize(MoleculeDataInput());

   // read first line with number of freqs
   std::string s;
   std::getline(mMoleculeFile.FileStream(),s);
   In num_freq = midas::util::FromString<In>(s);
   freqs->NumFreq() = num_freq;
   freqs->Unit() = "CM-1"; //

   // read frequencies
   In count = 0;
   while((count < num_freq) && std::getline(mMoleculeFile.FileStream(),s))
   {
      auto line = midas::util::StringVectorFromString(s,',');
      for(int i = 0; i < line.size(); ++i)
      {
         freqs->AddFrequency(midas::util::FromString<Nb>(line[i]),"A");
         ++count;
      }
   }

   aMolecule.InitFrequencies(freqs);
   return libmda::util::ret(s,false);
}

/**
 * @brief read normalcoordinate input in Sindo molecule file
 **/
typename SindoReader::SindoVibrationalReader::return_t
SindoReader::SindoVibrationalReader::ReadNormalCoord(const key_t& aKey, MoleculeInfo& aMolecule)
{
   VibrationalCoordinateData vib_coord(VibrationalCoordinateType::Key());
   vib_coord.Initialize(MoleculeDataInput());

   vib_coord->Unit() = "AU";

   std::string str;
   while(std::getline(mMoleculeFile.FileStream(),str) && str.substr(0,4) == "Mode")
   {
      std::vector<Nb> coordinate;
      std::getline(mMoleculeFile.FileStream(),str);
      In size = midas::util::FromString<In>(str);
      In count = 0;
      while((count < size) && std::getline(mMoleculeFile.FileStream(),str))
      {
         auto line = midas::util::StringVectorFromString(str,',');
         for(int i = 0; i < line.size(); ++i)
         {
            coordinate.emplace_back(midas::util::FromString<Nb>(line[i]));
            ++count;
         }
      }
      vib_coord->AddCoordinate(coordinate);
   }

   // set data and return
   aMolecule.InitVibrationalCoordinates(vib_coord);
   return libmda::util::ret(str,true); // last line read has not been processed, so should be processed after this routine
}

typename SindoReader::SindoElectronicReader::return_t
SindoReader::SindoElectronicReader::ReadGradient(const key_t& aKey, MoleculeInfo& aMolecule)
{
   GradientData gradient(GradientType::Key());
   gradient.Initialize(MoleculeDataInput());

   std::string s;
   std::getline(mMoleculeFile.FileStream(),s);
   In num_elem = midas::util::FromString<In>(s);

   gradient->SetNewSize(num_elem);

   In count = 0;
   while((count < num_elem) && std::getline(mMoleculeFile.FileStream(),s))
   {
      auto line = midas::util::StringVectorFromString(s,',');
      for(int i = 0; i < line.size(); ++i)
      {
         gradient->at(count) =  midas::util::FromString<Nb>(line[i]);
         ++count;
      }
   }

   aMolecule.InitGradient(gradient);
   return libmda::util::ret(s,false);
}

typename SindoReader::SindoElectronicReader::return_t
SindoReader::SindoElectronicReader::ReadHessian(const key_t& aKey, MoleculeInfo& aMolecule)
{
   HessianData hessian(HessianType::Key());
   hessian.Initialize(MoleculeDataInput());
   std::string s;
   std::getline(mMoleculeFile.FileStream(),s);
   In num_inp = midas::util::FromString<In>(s);

   // This is the number of rows and collumns in the real matrix
   In n = (-0.5+std::sqrt(-0.25+2.0*num_inp))+1;

   hessian->SetNewSize(n, n);

   In count = 0;
   std::vector<Nb> hess;
   while((count < num_inp) && std::getline(mMoleculeFile.FileStream(),s))
   {
      auto line = midas::util::StringVectorFromString(s,',');
      for(int i = 0; i < line.size(); ++i)
      {
         hess.emplace_back(midas::util::FromString<Nb>(line[i]));
         ++count;
      }
   }
   auto it = hess.begin();
   for(In i = 0; i < n; ++i)
      for(In j = 0; j <= i; ++j)
      {
         hessian->at(i,j) = *it;
         hessian->at(j,i) = *it;
         ++it;
      }

   aMolecule.InitHessian(hessian);
   return libmda::util::ret(s,false);
}


} /* namespace molecule */
} /* namespace midas */
