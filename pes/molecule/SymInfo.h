/**
************************************************************************
*
* @file                SymInfo.h
*
* Created:             23-03-2009
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for point-group symmetry data
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SYMINFO_H_INCLUDED
#define SYMINFO_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"
#include "pes/molecule/OperatorRotation.h"

namespace midas
{
namespace molecule
{

// Forward declarations.
class PointGroup;

/**
* Declarations:
**/
class SymInfo
{
   private:
      //! Symmetry label for the point group
      std::string mGrpLabel;
      //! Order of the group
      In mNgroup;
      //! Nr. of classes
      In mNclass;
      //! Nr. of symmetry operations
      In mNopSym;
      //! Dimension of each IR of the point group
      std::vector<In> mIrrepsDim;
      //! Character Table of the group
      MidasMatrix mCharTab;
      //! Map Irreps lab and Irreps Nr.
      std::map<string,In> mIrrepsLabMap; 
      //! Map Symmetry Ops Nr, and label.   
      std::map<In,string> mOpSymLabMap;     
      //! Vector of rotation matrices for each symmetry operation
      std::vector<MidasMatrix> mRotationMatrices;
      //! Vector of inverse rotation matrices for each symmetry operation
      std::vector<MidasMatrix> mInverseRotationMatrices;
      //! For each symmetry operation how are the nuclei related?
      std::vector<std::vector<In> > mNucleiRelation;
      //! For each symmetry operation how are the nuclei reversely related?
      std::vector<std::vector<In> > mInverseNucleiRelation;
   
      //! Driver for generating a point group
      void GenSymInfo(const std::vector<OperatorRotation::Axis>&);  
      
      //!@{
      //! Generate specific point groups
      void GenSymC1(const std::vector<OperatorRotation::Axis>&);    ///< Generate symmetry for C1
      void GenSymCS(const std::vector<OperatorRotation::Axis>&);    ///< Generate symmetry for CS
      void GenSymCI(const std::vector<OperatorRotation::Axis>&);    ///< Generate symmetry for CI
      void GenSymC2(const std::vector<OperatorRotation::Axis>&);    ///< Generate symmetry for C2
      void GenSymD2(const std::vector<OperatorRotation::Axis>&);    ///< Generate symmetry for D2
      void GenSymC2V(const std::vector<OperatorRotation::Axis>&);   ///< Generate symmetry for C2V
      void GenSymC2H(const std::vector<OperatorRotation::Axis>&);   ///< Generate symmetry for C2H
      void GenSymD2H(const std::vector<OperatorRotation::Axis>&);   ///< Generate symmetry for D2H
      //!@}
      
      //! Set the point group (for abelian point-groups only)
      void SetPointGroup(string aLabel) { mGrpLabel=aLabel; }
      
   public:

      //! Deleted default constructor
      SymInfo();
      
      ////! Constructor 
      SymInfo(const PointGroup& aPointGroup);

      //! Default move ctor
      SymInfo(SymInfo&&) = default;

      //! Default move assignment
      SymInfo& operator=(SymInfo&&) = default;
    
      //! dtor
      ~SymInfo();
      
      //! Get irrep map
      const std::map<string,In>& GetIrrepsLabMap() const { return mIrrepsLabMap; }
      
      //! Get symlabel map
      const std::map<In,string>& GetOpSymLabMap() const { return mOpSymLabMap; }

      //! Set the method of screening (only two implemented)
      std::string GetPointGroup() const {return mGrpLabel;} 
      
      //! Get the order of the group
      In Ngroup() const {return mNgroup;}
      
      //! Get the number of classes
      In Nclass() const {return mNclass;}
      
      //! Get the number of symmetry operations
      In NopSym() const {return mNopSym;}
      
      //! Get a particular character of the table
      Nb GetCharacter(In aIrep, In aIclass) const {return mCharTab[aIrep][aIclass];} 
      
      //! Get the dimensionality of the representation
      In GetIrDim(In aIrep) const {return mIrrepsDim[aIrep];} 
      
      //! Get operator label
      std::string GetOpLab(In aIop) const;   
      
      //! Get symmetry No.
      In GetIrrepNr(string aLab) const; 
      
      //! Get symmetry label
      std::string GetIrLab(In aIrep) const; 

      //! Get symmetry label for irrep
      std::string GetIrLab(const std::vector<In>& aIrep) const;

      //! 
      void SetRelation(In aIop, const std::vector<In>& arRelation);

      //! 
      const std::vector<In>& GetRelation(In aIop) const { return mNucleiRelation[aIop]; }
      
      //!
      const std::vector<In>& GetInverseRelation(In aIop) const { return mInverseNucleiRelation[aIop]; }

      //! Get rotation matrix for operation
      const MidasMatrix& GetRotation(In aOp) const { return mRotationMatrices[aOp]; }
      
      //! Get rotation matrix for operation
      const MidasMatrix& GetInverseRotation(In aOp) const { return mInverseRotationMatrices[aOp]; }
      
      //! Output overloading
      friend std::ostream& operator<<(std::ostream& arOut, const SymInfo& arGrp);   
};

} /* namespace molecule */
} /* namespace midas */

#endif /* SYMINFO_H_INCLUDED */
