/**
************************************************************************
*
* @file                MoleculeInfo.cc
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Definitions for reading a numderfile
*
* Last modified:       09-08-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "pes/molecule/MoleculeInfo.h"

// std headers
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib> // DEBUG FOR exit
#include <sstream>

// midas headers
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "input/Input.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/FromString.h"
#include "pes/molecule/MoleculeReader.h"
#include "pes/molecule/MoleculeWriter.h"
#include "pes/molecule/PointGroupSymmetry.h"
#include "lapack_interface/LapackInterface.h"
#include "system/System.h"

#include "pes/molecule/MoleculeUtility.h"

// using declarations
using std::make_pair;

namespace midas
{
namespace molecule
{

namespace detail
{

/**
 * Modify molecular frame, e.g. move to center of mass and rotate to inertia frame.
 * Will update molecule in-place.
 *
 * @param aMolecule   Reference to the molecule to modify.
 **/
void ModifyMolecularFrame
   (  MoleculeInfo& aMolecule
   )
{
   auto& structure = aMolecule.mCoordinates.Get();

   // Shift to center of mass
   auto center_of_mass = FindCenterOfMass(structure);

   center_of_mass[0] = -center_of_mass[0];
   center_of_mass[1] = -center_of_mass[1];
   center_of_mass[2] = -center_of_mass[2];

   ShiftStructure(structure, center_of_mass);

   // Rotate to inertia frame
   auto inertia_frame_rotation = FindRotationToInertiaFrame(structure);

   aMolecule.Rotate(inertia_frame_rotation);
}

} /* namespace detail */

/**
 *  Constructor which reads what it can and in the case of PES calculations
 **/
MoleculeInfo::MoleculeInfo
   (  const MoleculeFileInfo::Set& aMoleculeFileInfoSet
   )
{
   // Read molecule files
   if (!MoleculeReader::ReadMolecule(aMoleculeFileInfoSet, *this))
   {
      MidasWarning("Molecule file not read succesfully :C");
   }
   
   bool test = false;
   if(test)
   {
      detail::ModifyMolecularFrame(*this);
   }

   // initialize molecule
   Initialization();
}

/**
 * Constructor from MoleculeFileInfo
 **/
MoleculeInfo::MoleculeInfo
   (  const MoleculeFileInfo& aMoleculeFileInfo
   )  
   :  MoleculeInfo
         (  MoleculeFileInfo::Set{aMoleculeFileInfo}
         )
{

}

/**
 *  Constructor from a system  
**/
MoleculeInfo::MoleculeInfo
   (  const System& arSystem
   ,  const std::set<GlobalSubSysNr>& arSubNrs
   ,  const std::set<GlobalModeNr>& arModeNrs
   ,  const bool& arIncludeSigmas
   )
{
   // first the atoms
   std::set<GlobalAtomNr> atom_set = arSystem.AtomsInSubSys(arSubNrs);
   mNumberOfNuclei = atom_set.size();
   std::vector<Nuclei> new_nuclei;
   new_nuclei.reserve(mNumberOfNuclei);
   for (auto it_nuc = atom_set.begin(); it_nuc != atom_set.end(); ++it_nuc)
   {
      new_nuclei.emplace_back(arSystem.mNuclei.at(*it_nuc));
   }
   mCoordinates.Initialize(MoleculeDataSystem(), std::move(new_nuclei));

   //then modes
   mNumberOfVibs = arModeNrs.size();
   In n_modes = I_0;
   In n_sigma = arSystem.NoOfModesWithSigmaInSet(arModeNrs);
   MidasMatrix new_modes(mNumberOfVibs, (In)(I_3 * mCoordinates->size()));
   MidasVector new_freqs(mNumberOfVibs, C_0);
   mModeLabels.resize(mNumberOfVibs, "");
   MidasMatrix new_sigmas(n_sigma, (In)(I_3 * mCoordinates->size()));
   for (auto it_mode = arModeNrs.begin(); it_mode != arModeNrs.end(); ++it_mode)
   {
      MidasVector mode_vec = arSystem.GetOrderedModeVectorForSubSys(*it_mode, arSubNrs, false);
      if (arSystem.mModes.at(*it_mode).HasSigma())
      {
         new_modes.AssignRow(mode_vec, n_modes);
         if (arSystem.mModes.at(*it_mode).HasFreq())
         {
            new_freqs.at(n_modes) = arSystem.mModes.at(*it_mode).Freq() * C_AUTKAYS;
         }

         if (arSystem.mModes.at(*it_mode).Tag() != "")
         {
             mModeLabels.at(n_modes) = arSystem.mModes.at(*it_mode).Tag();
         }
         else
         {   
             mModeLabels.at(n_modes) = "Q" + std::to_string(n_modes);
         }

         if (arIncludeSigmas)
         {
            mode_vec = arSystem.GetOrderedSigmaVectorForSubSys(*it_mode, arSubNrs, false);
            new_sigmas.AssignRow(mode_vec, n_modes);
         }

         ++n_modes;
      }
      else
      {
         if (arSystem.mModes.at(*it_mode).HasFreq())
         {
            new_freqs.at(n_sigma) = arSystem.mModes.at(*it_mode).Freq() * C_AUTKAYS;
         }

         if (arSystem.mModes.at(*it_mode).Tag() != "")
         {   
             mModeLabels.at(n_sigma) = arSystem.mModes.at(*it_mode).Tag();
         }
         else
         {   
             mModeLabels.at(n_sigma) = "Q" + std::to_string(n_sigma);
         }

         new_modes.AssignRow(mode_vec, n_sigma++);
      }
   }

   // and finally put all in the moleculeinfo 
   mNormalCoord.Initialize(MoleculeDataSystem(), std::move(new_modes));
   if (new_sigmas.size() != I_0)
   {
      mSigma.Initialize(MoleculeDataSystem(), std::move(new_sigmas));
   }
   mFreqs.Initialize(MoleculeDataSystem(), new_freqs);
   mSymLabels = std::vector<std::string>(mNumberOfVibs, "A");
   Initialization();
}

/**
 *  Constructor from a system
 *
 *  @param aXYZ            XYZ coordinates
 *  @param aNormalCoord    Inversely mass-weigted normal coordinates (Y = M^{-1/2} L, where L diagonalizes the mass-weighted Hessian). NB: As rows of matrix. 
 *  @param aFreq           Frequencies in a.u.
 **/
MoleculeInfo::MoleculeInfo
   (  const std::vector<Nuclei>& aXYZ
   ,  const MidasMatrix& aNormalCoord
   ,  const MidasVector& aFreq
   )
{
   mNumberOfNuclei = aXYZ.size();
   mCoordinates.Initialize(MoleculeDataSystem(), aXYZ);
   mNumberOfVibs = aFreq.Size();
   mNormalCoord.Initialize(MoleculeDataSystem(), aNormalCoord);
   mModeLabels.resize(mNumberOfVibs);
   for(In i=I_0; i<mNumberOfVibs; ++i)
   {
      mModeLabels[i] = "Q" + std::to_string(i);
   }
   auto new_freq = aFreq;
   new_freq.Scale(C_AUTKAYS); // Convert from a.u. to cm^-1
   mFreqs.Initialize(MoleculeDataSystem(), std::move(new_freq));

   mSymLabels = std::vector<std::string>(mNumberOfVibs,"A");

   this->Initialization();
}

/**
 * Initialize molecule from read in data, i.e. check if molecule is linear or nonlinear and find number of vibrational freq.
 **/
void MoleculeInfo::Initialization
   (
   )
{

   In max_vib_no = I_0;
   if (mNumberOfNuclei == I_2)
   {
      max_vib_no = I_1;
      mLinear = Linear();
      Mout << " You have a diatomic molecule and the number of vibrational frequencies is " << max_vib_no << endl;
   }

   Nb angle_dev_thr = C_I_10_3;
   In no_of_angles = mNumberOfNuclei * (mNumberOfNuclei-I_1) * (mNumberOfNuclei-I_2) / I_6;

   //
   if (mNumberOfNuclei > I_2)
   {
      mLinear = Linear();

      if (mLinear)
      {
         max_vib_no = I_3 * mNumberOfNuclei - I_5;

         if (gPesIoLevel > I_4)
         {
            Mout << " You have a linear molecule and the maximal number of vibrational frequencies is " << max_vib_no << std::endl;
         }
      }
      else
      {
         max_vib_no = I_3 * mNumberOfNuclei - I_6;

         if (gPesIoLevel > I_4)
         {
            Mout << " You have a non-linear molecule and the maximal number of vibrational frequencies is " << max_vib_no << std::endl;
         }
      }
   }

   //

   // Initialize mass vector
   InitMassVector();


   // set number of vibrations if we have normal modes
   if (HasNormalCoord())
   {
      if (HasSelectVibs())
      {
         mNumberOfVibs = mSelectVibs->size();
      }
      else
      {
         //Certain file formats (e.g. Turbomole) also give the trans./rot. coords, thus remove
         //these if needed.
         if (mNeedsTrimOfTransRotCoords)
         {
            MidasMatrix old_normcoords = mNormalCoord.Get();
            In n_cols = mNormalCoord.Get().Ncols();   //Dim. of vib.coord. vecs., 3*N_atoms.
            In n_rows = mNormalCoord.Get().Nrows();   //Number of vibrations, 3N-6(5).
            In n_trans_rot;

            //Remove 6(5) trans/rot depending on linearity of molecule.
            if (mLinear)
            {
               n_trans_rot = I_5;
            }
            else
            {
               n_trans_rot = I_6;
            }

            //Resize mNormalCoord (false: don't save the old data):
            n_rows -= n_trans_rot;
            mNormalCoord.Get().SetNewSize(n_rows, n_cols, false);

            //Then fill in data again, skipping the trans./rot. coords.
            for(In i=I_0; i<mNormalCoord.Get().Nrows(); ++i)
            {
               for(In j=I_0; j<mNormalCoord.Get().Ncols(); ++j)
               {
                  mNormalCoord.Get()[i][j] = old_normcoords[i + n_trans_rot][j];
               }
            }

            //Unset the flag, now that we have done it:
            mNeedsTrimOfTransRotCoords = false;
         }

         //Certain file formats (e.g. Turbomole) give norm.coords. that are mass-scaled, and thus
         //needs fixing.
         if(mNeedsInvSqrtMassScaling)
         {
            for(In j=I_0; j<mNormalCoord.Get().Ncols(); ++j)
            {
               for(In i=I_0; i<mNormalCoord.Get().Nrows(); ++i)
               {
                  mNormalCoord.Get()[i][j] /= sqrt(GetNuclMassi(j/I_3));
               }
            }

            //Unset the flag, now that we have done it:
            mNeedsInvSqrtMassScaling = false;
         }

         mNumberOfVibs= mNormalCoord->Nrows();
      }
   }

   // Indicate the number of vibrational modes
   if (HasInternalCoord())
   {
      if (mLinear)
      {
         MIDASERROR("Polyspherical coordinates are NOT well-defined for linear molecules!");
      }

      if (HasNormalCoord())
      {
         MIDASERROR("Cannot simultaneously use #1 INTERNALCOORD and #1 VIBCOORD!");
      }
     
      // If inactive modes are defined, then we need to modify the molecule information accordingly
      if (HasInactiveModes())
      {
         const auto& tmp_vec_size = mInternalCoord.Get().Size();
         std::vector<Nb> tmp_active_modes;
         tmp_active_modes.resize(tmp_vec_size, C_0);
        
         MidasVector tmp_all_modes;
         tmp_all_modes.SetNewSize(tmp_vec_size);
         tmp_all_modes = mInternalCoord.Get();
         
         for (In imode = I_0; imode < tmp_vec_size; ++imode)
         {
            tmp_active_modes[imode] = tmp_all_modes[imode];
         }

         std::vector<In> tmp_inactive_mode_nrs;
         for (const auto& inactive_mode_label : mInactiveModeLabels)
         {
            auto it = std::find(mModeLabels.begin(), mModeLabels.end(), inactive_mode_label);
            if (it != mModeLabels.end())
            {
               auto index = std::distance(mModeLabels.begin(), it);
           
              tmp_inactive_mode_nrs.push_back(index);
            }
         }
         mInactiveModeNrs = tmp_inactive_mode_nrs; 

         // Delete the inactive modes from the members and store info on which modes have been set inactive 
         for (const auto& inactive_mode_label : mInactiveModeLabels)
         {
            auto it = std::find(mModeLabels.begin(), mModeLabels.end(), inactive_mode_label);
            if (it != mModeLabels.end())
            {
               auto index = std::distance(mModeLabels.begin(), it);
               
               mModeLabels.erase(mModeLabels.begin() + index);
               mSymLabels.erase(mSymLabels.begin() + index);
               mTypeLabels.erase(mTypeLabels.begin() + index);
               
               tmp_active_modes.erase(tmp_active_modes.begin() + index);
            }
         }

         //
         mInternalActiveCoords.SetNewSize(tmp_active_modes.size());
         for (In jmode = I_0; jmode < tmp_active_modes.size(); ++jmode)
         {
            mInternalActiveCoords[jmode] = tmp_active_modes[jmode];
         }

         // The total number of vibrational modes are now "non-standard"
         mNumberOfVibs = I_3 * mNumberOfNuclei - I_6 - mInactiveModeLabels.size();
      }
      else
      {
         mNumberOfVibs = I_3 * mNumberOfNuclei - I_6;
      }

      if (gPesIoLevel > I_4)
      {
         Mout << " You have a non-linear molecule and the maximal number of vibrational modes is " << max_vib_no << std::endl;
      }
   }

   if (mNumberOfVibs > I_3 * mNumberOfNuclei && !gPesCalcDef.GetmDincrSurface() && gModSysCalcDefs[0].GetVibCoordScheme() != input::VibCoordScheme::FALCON)
   {
      MidasWarning("Read in more than 3*NumberOfNuclei vibrations");
   }

   // if number of read in vibrational coordinates does not match the number of possible vibrations we give a warning
   if (mNumberOfVibs != max_vib_no && !gPesCalcDef.GetmDincrSurface())
   {
       MidasWarning("Read in number of vibrations (" + std::to_string(mNumberOfVibs) + ") does NOT match the number of possible vibrations (" + std::to_string(max_vib_no) + ")");
   }

   //// if we have hessian we calculate normalcoord from hessian...
   // if commented back in, remember to update mNumberOfVibs or move it up!!
   //if(HasHessian() && !HasNormalCoord())
   //   NormalCoordFromHessian();

   //then rotate the reference structure if requested
   //if (gPesInertialFrame) Rotate(arRefStruc);

   // Calculate reduced mass if diatomic
   if (  gPesCalcDef.GetmPesDoMorsePot()
      || gPesCalcDef.GetmPesDiatomic()
      || gPesCalcDef.GetmPesDoDoubleWpot()
      )
   {
      CalcRedMass();
   }

   // setup default mode names
   if(mModeLabels.size() == 0)
   {
      SetupDefaultModeNames();
   }
}

/**
 *
 **/
void MoleculeInfo::InitMassVector()
{
   if(mMassVector)
   {
      MidasWarning("trying to initialize mass vector twice...");
      return;
   }
   if(HasXYZ())
   {
      mMassVector.Initialize(MoleculeDataCalculated(), mCoordinates->size());
      for(In i = 0; i < mCoordinates->size(); ++i)
      {
         mMassVector->at(i) = mCoordinates->at(i).GetMass();
         mTotalMass += mMassVector->at(i);
      }
   }
   else
   {
      MIDASERROR("Nothing to initialize masses from.");
   }
}

/**
 * If no mode names are given, we setup default ones.
 * These will just be Q0, Q1, Q2, ... etc...
 **/
void MoleculeInfo::SetupDefaultModeNames
   (
   )
{
   auto num_vibs = this->GetNoOfVibs();
   if(mModeLabels.size() < num_vibs)
   {
      mModeLabels.clear();
      for(In i = I_0; i < num_vibs; ++i)
      {
         mModeLabels.emplace_back("Q" + std::to_string(i));
      }
   }
}

/**
 *
 **/
void MoleculeInfo::InitXYZ(const XYZData& aXyz)
{
   if (aXyz->NumAtom() == 1)
   {
      MIDASERROR(" Error, since no. of atoms is 1, you do not have any vibrational effects. I stop ");
   }

   mCoordinates.Initialize(MoleculeDataInput());
   mNumberOfNuclei = aXyz->NumAtom();

   auto conv = aXyz->ConversionToAtomicUnits(); // get unit conversion factor
   for(In atom_idx = 0; atom_idx < aXyz->NumAtom(); ++atom_idx)
   {
      // initialize and add atom coordinates
      mCoordinates->emplace_back(aXyz->X(atom_idx), aXyz->Y(atom_idx), aXyz->Z(atom_idx), C_0, aXyz->Label(atom_idx));
      mCoordinates->back().ScaleCoord(conv);

      // set charge and isotope
      mCoordinates->back().SetQfromGeneralLabel(aXyz->Label(atom_idx));
      mCoordinates->back().SetAnuc(aXyz->Isotope(atom_idx)); // if isotope is 0, then Anuc is set to most common (which is the default)

      // setup subsystems
      mCoordinates->back().SetSubSys(aXyz->SubSystem(atom_idx));
      mCoordinates->back().SetSubSysMem(atom_idx);
      mCoordinates->back().SetNucTreatType(aXyz->TreatType(atom_idx));
   }
}

/**
 *
 **/
void MoleculeInfo::InitFrequencies(const FrequencyData& aFreqs)
{
   mFreqs.Initialize(MoleculeDataInput(), aFreqs->NumFreq());
   mSymLabels.resize(aFreqs->NumFreq());
   mModeLabels.resize(aFreqs->NumFreq());

   auto conv = aFreqs->ConversionToInverseCM(); // get unit conversion factor
   for(In freq_idx = 0; freq_idx < aFreqs->NumFreq(); ++freq_idx)
   {
      mFreqs->at(freq_idx) = aFreqs->Frequency(freq_idx) * conv;
      mSymLabels[freq_idx] = aFreqs->SymLabel(freq_idx);
      mModeLabels[freq_idx] = aFreqs->ModeLabel(freq_idx);
   }
}

/**
 *
 **/
void MoleculeInfo::InitSigma(const SigmaData& aSigma)
{
   mSigma.Initialize(MoleculeDataInput(),aSigma->NumSigma(),aSigma->NumDisplacements());

   auto conv = aSigma->ConversionToAtomicUnits();
   for(In i = 0; i < aSigma->NumSigma(); ++i)
   {
      for(In j = 0; j < aSigma->NumDisplacements(); ++j)
      {
         mSigma->at(i,j) = aSigma->Sigma(i).at(j) * conv;
      }
   }
}

/**
 *
 **/
void MoleculeInfo::InitVibrationalCoordinates(const VibrationalCoordinateData& aVibCoord)
{
   if(aVibCoord->Type() != "NORMALCOORD")
   {
      MIDASERROR("InitVibrationalCoordinate(): only implemented for NORMALCOORD");
   }

   mNormalCoord.Initialize(MoleculeDataInput(), aVibCoord->NumCoords(), aVibCoord->NumDisplacements());
   
   auto conv = aVibCoord->ConversionToAtomicUnits();
   for(In i = 0; i < aVibCoord->NumCoords(); ++i)
   {
      for(In j = 0; j < aVibCoord->NumDisplacements(); ++j)
      {
         mNormalCoord->at(i,j) = aVibCoord->Coordinate(i).at(j) * conv;
      }
   }

   //Set flags for post-editing the norm.coords., needed for e.g. Turbomole:
   mNeedsInvSqrtMassScaling   = aVibCoord->NeedsInvSqrtMassScaling();
   mNeedsTrimOfTransRotCoords = aVibCoord->NeedsTrimOfTransRotCoords();
}

/**
 *
**/
void MoleculeInfo::InitInternalCoord
   (  const InternalCoordData& aInternalCoord
   )
{
   if (aInternalCoord->GetmVibCoordType() != "INTERNALCOORD")
   {
      MIDASERROR("InitInternalCoord(): Only implemented for INTERNALCOORD");
   }

   // Get the number of internal coordinates
   auto no_internal_coords = aInternalCoord->GetmNoInternalCoords();

   // Initialize the internal coordinates and resize members
   mInternalCoord.Initialize(MoleculeDataInput(), no_internal_coords);
   mTypeLabels.resize(no_internal_coords);
   mModeLabels.resize(no_internal_coords);
   mSymLabels.resize(no_internal_coords);

   // Get the correct conversion factor for the internal coordinate units
   auto dist_conv = aInternalCoord->ConversionToAtomicUnits();
   auto ang_conv = aInternalCoord->ConversionToRadianUnits();

   // Make sure that internal coordinates have units of au and rad. Store all data as members of the MoleculeInfo class
   for (In i = I_0; i < no_internal_coords; ++i)
   {
      auto internal_type = aInternalCoord->GetmTypeLabel(i);
      auto dist_unit = aInternalCoord->GetmUnitDist();
      auto ang_unit = aInternalCoord->GetmUnitAng();
      
      if (  (internal_type == "R")
         && (dist_unit == "AA")
         )
      {
         mInternalCoord->at(i) = aInternalCoord->GetmInternalCoord(i) * dist_conv;
      }
      else if (  (internal_type == "V" || internal_type == "D")
         && (ang_unit == "DEGREE")
         )
      {
         mInternalCoord->at(i) = aInternalCoord->GetmInternalCoord(i) * ang_conv;
      }

      mInternalCoord->at(i) = aInternalCoord->GetmInternalCoord(i);
      mTypeLabels[i] = aInternalCoord->GetmTypeLabel(i);
      mModeLabels[i] = aInternalCoord->GetmModeLabel(i);
      mSymLabels[i] = aInternalCoord->GetmSymLabel(i);
   }
}

/**
 *
**/
void MoleculeInfo::InitInactiveModes
   (  const InactiveModesData& arInactiveModesData
   )
{
   // Get the number of inactive modes
   auto no_inactive_modes = arInactiveModesData->GetmNoInactiveModes();
   
   // Initialize the inactive modes data resize member
   mInactiveModes.Initialize(MoleculeDataInput(), no_inactive_modes);
   mInactiveModeLabels.resize(no_inactive_modes);
   
   // Store all data as members of the MoleculeInfo class
   for (In i = I_0; i < no_inactive_modes; ++i)
   {
      mInactiveModeLabels[i] = arInactiveModesData->GetmInactiveModeLabel(i);
   }
}

/**
 *
 **/
void MoleculeInfo::InitLinComb(const LinCombData& aLinComb)
{
   mLinComb.Initialize(MoleculeDataInput(),aLinComb->NumLinComb(),aLinComb->NumDisplacements());

   for(In i = 0; i < aLinComb->NumLinComb(); ++i)
   {
      for(In j = 0; j < aLinComb->NumDisplacements(); ++j)
      {
         mLinComb->at(i,j) = aLinComb->LinComb(i).at(j);
      }
   }

   // normalize linear combinations
   MidasVector norm_factors(aLinComb->NumLinComb(),C_0);
   for(In i = I_0; i < aLinComb->NumLinComb(); i++)
   {
      for(In j = I_0; j < aLinComb->NumDisplacements(); j++)
      {
         norm_factors.at(i) += mLinComb->at(i,j)*mLinComb->at(i,j);
      }
   }

   for(In i = I_0; i < aLinComb->NumLinComb(); i++)
   {
      for(In j = I_0; j < aLinComb->NumDisplacements(); j++)
      {
         mLinComb->at(i,j) /= sqrt(norm_factors.at(i));
      }
   }
}

/**
 *
 **/
void MoleculeInfo::InitSelectVibs(const SelectVibsData& aSelectVibs)
{
   mSelectVibs.Initialize(MoleculeDataInput(), aSelectVibs->NumSelectVibs());

   for (In i = I_0; i < aSelectVibs->NumSelectVibs(); ++i)
   {
      mSelectVibs->at(i) = aSelectVibs->SelectVib(i);
   }
}

/**
 *
 **/
void MoleculeInfo::InitHessian(const HessianData& hessian)
{
   mHessian.Initialize(MoleculeDataInput(),hessian.Get());
}

/**
 *
 **/
void MoleculeInfo::InitGradient(const GradientData& gradient)
{
   mGradient.Initialize(MoleculeDataInput(),gradient.Get());
}

/**
 * MBH edit: Stuff for reinitializing/overwriting the data. May need changing!
 **/
void MoleculeInfo::ReInitHessian(const HessianData& arHessian)
{
   mHessian.ReInitialize(arHessian.Origin(), arHessian.Get());
}

/**
 * MBH edit: Stuff for reinitializing/overwriting the data. May need changing!
 **/
void MoleculeInfo::ReInitGradient(const GradientData& arGradient)
{
   mGradient.ReInitialize(arGradient.Origin(), arGradient.Get());
}

/**
 *
 **/
void MoleculeInfo::ReInitFrequencies(const FrequencyData& arFreqs)
{
   mFreqs.ReInitialize(arFreqs.Origin(), arFreqs->NumFreq());
   mSymLabels.resize(arFreqs->NumFreq());
   mModeLabels.resize(arFreqs->NumFreq());

   auto conv = arFreqs->ConversionToInverseCM(); // Get unit conversion factor
   for (In freq_idx = I_0; freq_idx < arFreqs->NumFreq(); ++freq_idx)
   {
      mFreqs->at(freq_idx) = arFreqs->Frequency(freq_idx) * conv;
      mSymLabels[freq_idx] = arFreqs->SymLabel(freq_idx);
      mModeLabels[freq_idx] = arFreqs->ModeLabel(freq_idx);
   }
}

/**
 * This function is used if rotated coordinates with symmetry for the vibrational coordinates are soght and "preserves" the symmetry labels
 **/
void MoleculeInfo::ReInitFreqSymm(const FrequencyData& arFreqs)
{
   mFreqs.ReInitialize(arFreqs.Origin(), arFreqs->NumFreq());
   mSymLabels.resize(arFreqs->NumFreq());
   mModeLabels.resize(arFreqs->NumFreq());

   Out72Char(Mout,'-','-','-');
   Mout << "  Summary of vibrational modes symmetry for rotated structure: " << std::endl;
   Out72Char(Mout,'-','-','-');

   auto conv = arFreqs->ConversionToInverseCM(); // Get unit conversion factor
   for (In freq_idx = I_0; freq_idx < arFreqs->NumFreq(); ++freq_idx)
   {
      mFreqs->at(freq_idx) = arFreqs->Frequency(freq_idx) * conv;
      mModeLabels[freq_idx] = arFreqs->ModeLabel(freq_idx);

      Mout << " The mode " << mModeLabels[freq_idx] << " is now of symmetry:      " << mSymLabels[freq_idx] << std::endl;
   }
   Out72Char(Mout,'-','-','-');
}

/**
 *
 **/
void MoleculeInfo::ReInitVibrationalCoordinates
   (  const VibrationalCoordinateData& arVibCoord
   )
{
   if (arVibCoord->Type() != "NORMALCOORD") 
   {
      MIDASERROR("ReInitVibrationalCoordinate(): only implemented for NORMALCOORD");
   }

   mNormalCoord.ReInitialize
      (  arVibCoord.Origin()
      ,  arVibCoord->NumCoords()
      ,  arVibCoord->NumDisplacements()
      );
   
   auto conv = arVibCoord->ConversionToAtomicUnits();
   for (In i = I_0; i < arVibCoord->NumCoords(); ++i)
   {
      for (In j = I_0; j < arVibCoord->NumDisplacements(); ++j)
      {
         mNormalCoord->at(i, j) = arVibCoord->Coordinate(i).at(j) * conv;
      } 
   }
}

/**
 *
 **/
XYZData MoleculeInfo::GetXYZData() const
{
   XYZData xyz("XYZ_OUTPUT");

   if(HasXYZ())
   {
      xyz.Initialize(mCoordinates.Origin());
      xyz->NumAtom() = mNumberOfNuclei;
      xyz->Unit() = "AU"; // internally the program always uses atomic units
      xyz->Comment() = "xyz coordinates output from Midas";

      for(In atom_idx = 0; atom_idx < mNumberOfNuclei; ++atom_idx)
      {
         xyz->AddAtom(mCoordinates->at(atom_idx).AtomLabel() //+"#"+std::to_string(mCoordinates->at(atom_idx).Index())
                    , mCoordinates->at(atom_idx).X()
                    , mCoordinates->at(atom_idx).Y()
                    , mCoordinates->at(atom_idx).Z()
                    , mCoordinates->at(atom_idx).Anuc()
                    , mCoordinates->at(atom_idx).SubSys()
                    , mCoordinates->at(atom_idx).GetNucTreatType()
                    );
      }
   }

   return xyz;
}

/**
 *
 **/
FrequencyData MoleculeInfo::GetFrequencyData() const
{
   FrequencyData freq("FREQ_OUTPUT");

   if(HasFreq())
   {
      freq.Initialize(mFreqs.Origin());
      freq->NumFreq() = mFreqs->Size();
      freq->Unit() = "AU"; // program always uses AU internally

      for(In freq_idx = 0; freq_idx < mFreqs->Size(); ++freq_idx)
      {
         freq->AddFrequency(mFreqs->at(freq_idx), mSymLabels.at(freq_idx), mModeLabels.at(freq_idx));
      }
   }

   return freq;
}

/**
 *
 **/
VibrationalCoordinateData MoleculeInfo::GetVibrationalCoordinateData() const
{
   VibrationalCoordinateData vib_coord("VIB_COORD_OUTPUT");

   if(HasNormalCoord())
   {
      vib_coord.Initialize(mNormalCoord.Origin());
      vib_coord->Type() = "NORMALCOORD"; // hard coded for normal coordinates
      vib_coord->Unit() = "AU"; // internally midas always uses atomic units

      for(In i = 0; i < mNormalCoord->Nrows(); ++i)
      {
         std::vector<Nb> coord(mNormalCoord->Ncols());
         for(In j = 0; j < mNormalCoord->Ncols(); ++j)
         {
            coord.at(j) = mNormalCoord->at(i,j);
         }
         vib_coord->AddCoordinate(coord);
      }
   }

   return vib_coord;
}

/**
 *
 **/
SigmaData MoleculeInfo::GetSigmaData() const
{
   SigmaData sigma("SIGMA_OUTPUT");

   if(HasSigma())
   {
      sigma.Initialize(mSigma.Origin());
      sigma->Type() = "SIGMA"; //
      sigma->Unit() = "AU"; // internally midas always uses atomic units

      for(In i = 0; i < mSigma->Nrows(); ++i)
      {
         std::vector<Nb> coord(mSigma->Ncols());
         for(In j = 0; j < mSigma->Ncols(); ++j)
         {
            coord.at(j) = mSigma->at(i,j);
         }
         sigma->AddCoordinate(coord);
      }
   }

   return sigma;
}

/**
 *
 **/
GradientData MoleculeInfo::GetGradientData() const
{
   GradientData gradient("GRADIENT_OUTPUT");

   if(HasGradient())
   {
      gradient.Initialize(mGradient.Origin(),mGradient.Get());
   }

   return gradient;
}

/**
 *
 **/
HessianData MoleculeInfo::GetHessianData() const
{
   HessianData hessian("HESSIAN_OUTPUT");

   if(HasHessian())
   {
      hessian.Initialize(mHessian.Origin(),mHessian.Get());
   }

   return hessian;
}

/**
 *
**/
bool MoleculeInfo::Linear() const
{
   Nb angle_dev_thr = C_I_10_3;
   for (In i=I_0;i<mNumberOfNuclei;i++)
   {
      for (In j=i+1;j<mNumberOfNuclei;j++)
      {
         for (In k=j+1;k<mNumberOfNuclei;k++)
         {
            Nb angletest = mCoordinates->at(i).Angle(mCoordinates->at(j),mCoordinates->at(k));

            if ( !(  ( std::fabs(angletest - 180.0) < angle_dev_thr )
                  || ( std::fabs(angletest + 180.0) < angle_dev_thr )
                  || ( std::fabs(angletest) < angle_dev_thr )
                  ) )
            {
               return false;
            }
         }
      }
   }
   return true;
}

/**
 *
 **/
void MoleculeInfo::CalcRedMass()
{
   if (gPesIoLevel > I_5)
   {
      Mout << " Will now attempt to calculate the reduced mass of the system: " << std::endl;
   }

   if (gPesCalcDef.GetmPesDoMorsePot())
   {
      Nb red_mass;
      Nb red_mass_inv = C_0;
      for (In i = I_0; i < mNumberOfNuclei; i++)
      {
         red_mass_inv += C_1 / mMassVector->at(i);
      }
      red_mass = C_1 / red_mass_inv;
      midas::stream::ScopedPrecision(22, Mout);

      if (gPesIoLevel > I_5)
      {
         Mout << "  Reduced mass (a.u.): " << red_mass*C_FAMU << std::endl;
      }
   }
   if (gPesCalcDef.GetmPesDiatomic()) // construct the reduced mass
   {
      Nb red_mass;
      Nb red_mass_inv = C_0;
      for (In i = I_0; i < mNumberOfNuclei; i++)
      {
         red_mass_inv += C_1 / mMassVector->at(i);
      }
      red_mass = C_1 / red_mass_inv;
      mRedcucedMassDiatomic = red_mass;
   }
   if (gPesCalcDef.GetmPesDoDoubleWpot()) //then calculate the correct reduced mass
   {
      for (In i=I_0; i<mNumberOfNuclei; i++)
      {
         string lab=mCoordinates->at(i).AtomLabel();
         if (lab.find("H")!= lab.npos) mMassVector->at(i)*=C_3; //specific implementation for NH3
      }

      //compute the reduced mass
      Nb red_mass;
      Nb red_mass_inv=C_0;
      for (In i=I_0;i<mNumberOfNuclei;i++)
         red_mass_inv+=C_1/mMassVector->at(i);
      red_mass=C_1/red_mass_inv;
      mRedcucedMassDiatomic=red_mass;
   }
}

 /**
 * Here we get the Nuclei Nr for the first part of the line we just read in out ZMAT.
 * Note that it returns the number in the old array of Nuclei
 **/
In MoleculeInfo::GetNucleiNrFirstTime
   (  const string& aNuclei
   ,  In aNr
   ,  std::map<string, In>& aLabelToNr
   ,  std::map<string, In>& aLabelToNrOld
   ,  std::map<In, In>& aOldToNew, bool aIsLabel
   )
{
   if(aIsLabel)
   {
      std::map<string, In>::iterator it = aLabelToNrOld.find(aNuclei);
      if(it == aLabelToNrOld.end())
         MIDASERROR("Label "+aNuclei+" not found in file");
      pair<std::map<string, In>::iterator,bool> test = aLabelToNr.insert(make_pair(aNuclei, aNr));
      if(!test.second)
         MIDASERROR("Label "+aNuclei+" already used in ZMAT def");
      pair<std::map<In, In>::iterator, bool>test2 = aOldToNew.insert(make_pair(it->second, aNr));
      if(!test2.second)
         MIDASERROR("Something strange with the input, please check");
      return it->second;
   }
   else
   {
      In atomnr;
      std::stringstream convert(aNuclei);
      convert >> atomnr;
      Mout << "aNuclei : " << aNuclei << " atomnr : " << atomnr << endl;
      if(atomnr < I_0 || atomnr > mNumberOfNuclei)
      {
         Mout << "atomnr : " << atomnr << endl;
         Mout << aNuclei << endl;
         MIDASERROR("Atom number is wrong");
      }
      Mout << "making pair : " << atomnr << " " << aNr << endl;
      pair<std::map<In, In>::iterator,bool> test = aOldToNew.insert(make_pair(atomnr, aNr));
      if(!test.second)
         MIDASERROR("Something strange with the input, please check");
      return atomnr;
   }

   MIDASERROR("SHOULD NEVER GET HERE!");
   return -1; //We should never get here
}

//Nuclei MoleculeInfo::GetNucleiFirstTime(const string& aNuclei, In aNr, map<string, In>& aLabelToNr
//                                    , map<string, In>& aLabelToNrOld
//                                    , map<In, In>& aOldToNew, bool aIsLabel)
//{
//   return mCoordinates->at( GetNucleiNrFirstTime(aNuclei, aNr, aLabelToNr, aLabelToNrOld, aOldToNew, aIsLabel) );
//}

/**
*  This function figures out which number the nuclei which we are inputting has
**/
In MoleculeInfo::GetNucleiNr(const string& aNuclei, const std::map<string, In>& aLabelToNr,const std::map<In, In>& aOldToNew, bool aIsLabel)
{
   if(aIsLabel)
   {
      std::map<string, In>::const_iterator it = aLabelToNr.find(aNuclei);
      if(it == aLabelToNr.end())
         MIDASERROR("Nuclei with label "+aNuclei+" not found in input file");
      return it->second;
   }
   else
   {
      In atomnr;
      std::stringstream convert(aNuclei);
      convert >> atomnr;
      Mout << "converting : " << aNuclei << " to " << atomnr << endl;
      if(atomnr < I_0 || atomnr > mNumberOfNuclei)
      {
         Mout << "atomnr : " << atomnr << endl;
         MIDASERROR("Atom number is wrong");
      }
      std::map<In, In>::const_iterator it = aOldToNew.find(atomnr);
      Mout << "input : " << atomnr << " output : " << it->second << endl;
      if(it == aOldToNew.end())
         MIDASERROR("Atom number has yet to be specified in ZMAT input, check your input file");
      return it->second;
   }

   MIDASERROR("SHOULD NEVER GET HERE!");
   return -I_1; //Should not end here
}

/**
 *
**/
void MoleculeInfo::PrintmCoordinates() const
{
   Mout << " The Cartesian coordinates for the reference geometry are: " << std::endl;
   for (In i = I_0; i < mCoordinates->size(); ++i)
   {
      Mout << " " << mCoordinates->at(i) << std::endl;
   }
}

/**
 * Rotates the Normal coordinates which are entering the calculation.
 * This function only rotates a single coordinate at a time to fit a new molecular orientation,
 * and thus does not mix the coordinates.
 *
 * @param aRot   The 3x3 rotation matrix for rotating in 3D space.
 **/
void MoleculeInfo::RotateNormalCoord
   (  const MidasMatrix& aRot
   )
{
   auto& normal_coord = mNormalCoord.Get();

   MidasVector tmp(I_3);
   for(int i = 0; i < normal_coord.Nrows(); ++i)
   {
      for(int iatom = 0; iatom < mNumberOfNuclei; ++iatom)
      {
         int atomstart = iatom * 3;

         // Calculate rotated coordinate for atom number 'iatom'
         tmp[0] = 0.0;
         tmp[1] = 0.0;
         tmp[2] = 0.0;
         for(int iaxis = 0; iaxis < 3; ++iaxis)
         {
            for(int jaxis = 0; jaxis < 3; ++jaxis)
            {
               tmp[iaxis] += normal_coord[i][atomstart + jaxis] * aRot[iaxis][jaxis];
            }
         }

         // Then save result back
         for(int iaxis = 0; iaxis < 3; ++iaxis)
         {
            normal_coord[i][atomstart + iaxis] = tmp[iaxis];
         }
      }
   }
}

/**
 * Mixes the normalcoordinates by rotating amongst eachother.
 * After this operation the molecule will have the same orientaion in space,
 * but the vibrational coordinates will be different.
 *
 * @param aRot    The 3x3 rotation matrix for mixing coordinates.
 **/
void MoleculeInfo::MixNormalCoordByRotation(const MidasMatrix& aRot)
{
   if(HasSelectVibs())
   {
      MidasMatrix temp(mNumberOfVibs, I_3*mNumberOfNuclei, C_0);
      for(In i = I_0; i < mNumberOfVibs; ++i)
         for(In j = I_0; j < I_3*mNumberOfNuclei; ++j)
            for(In k = I_0; k < mNumberOfVibs; ++k)
            {
               temp[i][j] += mNormalCoord->at(mSelectVibs->at(k),j)*aRot[i][k];
            }
      for(In i = I_0; i < mNumberOfVibs; ++i)
         for(In j = I_0; j < I_3*mNumberOfNuclei; ++j)
            mNormalCoord->at(mSelectVibs->at(i),j) = temp[i][j];
   }
   else
   {
      MidasMatrix temp(mNumberOfVibs, I_3*mNumberOfNuclei, C_0);
      for(In i = I_0; i < mNumberOfVibs; ++i)
         for(In j = I_0; j < I_3*mNumberOfNuclei; ++j)
            for(In k = I_0; k < mNumberOfVibs; ++k)
            {
               temp[i][j] += mNormalCoord->at(k,j)*aRot[i][k];
            }
      for(In i = I_0; i < mNumberOfVibs; ++i)
         for(In j = I_0; j < I_3*mNumberOfNuclei; ++j)
            mNormalCoord->at(i,j) = temp[i][j];
   }
}

/**
 * Output Molecular information to a set of files.
 *
 * @param aSet    The set of MoleculeFileInfo's defining the output formats.
 **/
void MoleculeInfo::WriteMoleculeFile
   (  const MoleculeFileInfo::Set& aSet
   )  const
{
   MoleculeWriter::WriteMolecule(aSet,*this);
}

/**
 * Overload/wrapper for outputting Molecular information to file.
 * Will call the WriteMoleculeFile taking a MoleculeFileInfo::Set.
 *
 * @param aInfo    The MoleculeFileInfo defining the output format.
 **/
void MoleculeInfo::WriteMoleculeFile
   (  const MoleculeFileInfo& aInfo
   )  const
{
   MoleculeWriter::WriteMolecule({aInfo},*this);
}

/**
 * Do a statistics that tells how much of a vibrational vector is tied to a certain Nuclei
 **/
void MoleculeInfo::DoCoordNucleiStat
   (
   ) const
{
   MidasMatrix statistics(mNormalCoord->Nrows(), In(mNormalCoord->Ncols()/3), C_0);
   for(In i = I_0; i < mNormalCoord->Nrows(); ++i)
   {
      for(In j = I_0; j < In(mNormalCoord->Ncols()/3); ++j)
      {
         statistics[i][j]  =  mNormalCoord->at(i, j*I_3) * mNormalCoord->at(i, j*I_3)
                           +  mNormalCoord->at(i, j*I_3+I_1) * mNormalCoord->at(i, j*I_3+I_1)
                           +  mNormalCoord->at(i, j*I_3+I_2) * mNormalCoord->at(i, j*I_3+I_2);
      }
   }
   Mout << " Vibrational Vector Statistics : " << endl;
   Mout << statistics << endl;
}

/**
 * Rotate the molecule.
 *
 * @param aRotMat    The matrix to rotate with.
 **/
void MoleculeInfo::Rotate
   (  const MidasMatrix& aRotMat
   )
{
   // Assert size of vector.
   if( (aRotMat.Nrows() != 3) || (aRotMat.Ncols() != 3) )
   {
      MIDASERROR("Can only rotate with 3 x 3 matrix.");
   }

   // Rotate structure.
   if(mCoordinates)
   {
      for(auto& nuc : mCoordinates.Get())
      {
         nuc.Rotate(aRotMat);
      }
   }

   // Rotate vibrational coordinates.
   this->RotateNormalCoord(aRotMat);
}

/**
 * Translate the molecule
 *
 * @param aTransVec    The vector to translate with.
 **/
void MoleculeInfo::Translate
   (  const MidasVector& aTransVec
   )
{
   // Assert size of vector.
   if(aTransVec.Size() != 3)
   {
      MIDASERROR("Can only translate with 3D vector.");
   }

   // Translate structure.
   if(mCoordinates)
   {
      for(auto& nuc : mCoordinates.Get())
      {
         nuc.Shift(aTransVec);
      }
   }
}

/**
 *
 **/
void MoleculeInfo::PurgeSymmetryLabels
   (
   )
{
   // Clear all symmetry labels
   mSymLabels.clear();

   // Then insert "A"'s
   for(In i = 0; i < mNumberOfVibs; ++i)
   {
      mSymLabels.emplace_back("A");
   }
}

} /* namespace molecule */
} /* namespace midas */
