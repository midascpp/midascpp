/**
************************************************************************
*
* @file                SindoReader.h
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of SindoReader class used to read
*                      Sindo molecule files.
*                      Defines recognized keywords
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SINDOREADER_H_INCLUDED
#define SINDOREADER_H_INCLUDED

#include "pes/molecule/MoleculeReader.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace molecule
{

/**
 * SindoReader - class to read Sindo molecule input files
 **/
class SindoReader: public MoleculeReader
{
   private:
      using key_t = typename MoleculeReader::key_t; ///< define key_t (see MoleculeReaderTraits in MoleculeReader.h for definition)
      using return_t = typename MoleculeReader::return_t; ///< define return_t (see MoleculeReaderTraits in MoleculeReader.h for definition)

      return_t ReadAtomicData(const key_t&, MoleculeInfo&); ///< read xyz coordinate data
      return_t ReadVibrationalData(const key_t&, MoleculeInfo&); ///< read frequencies and normal coordinate data
      return_t ReadElectronicData(const key_t&, MoleculeInfo&); ///< Read Hessian and Gradient

      std::string Type() const { return "SINDO_READER"; } ///< return type of reader (for debug purposes)

      bool IsKeyword(const std::string&) const;

      /**
       * @brief private class to read data under [ Vibrational Data ] keyword
       **/
      class SindoVibrationalReader: public MoleculeReader
      {
         private:
            using key_t = typename MoleculeReader::key_t;
            using return_t = typename MoleculeReader::return_t;

            std::string Type() const { return "SINDO_VIBRATINOAL_READER"; } ///< return type of reader (for debug purposes)

            return_t ReadNormalCoord(const key_t&, MoleculeInfo&);
            return_t ReadFrequencies(const key_t&, MoleculeInfo&);

         public:
            ///> we do not allow copying or default construction
            SindoVibrationalReader() = delete; ///< deleted default ctor
            SindoVibrationalReader(const SindoVibrationalReader&) = delete; ///< deleted copy ctor
            SindoVibrationalReader& operator=(const SindoVibrationalReader&) = delete; ///< deleted copy assignment

            ///> ctor from molecule file
            SindoVibrationalReader(MoleculeFile& aMoleculeFile)
               : MoleculeReader(aMoleculeFile)
            {
               //this->InputDelimeter(nullptr,nullptr); // define input begin / end (we read from start of file to end of file)
               this->RegisterFunction("Vibrational Frequency" // define freq keyword
                                    , molecule_field_t::FREQ
                                    , std::bind(&SindoVibrationalReader::ReadFrequencies,this,std::placeholders::_1,std::placeholders::_2)
                                    );
               this->RegisterFunction("Vibrational vector" // define xyz keyword
                                    , molecule_field_t::VIBCOORD
                                    , std::bind(&SindoVibrationalReader::ReadNormalCoord,this,std::placeholders::_1,std::placeholders::_2)
                                    );
               this->InputDelimeter([](const std::string& str) -> bool { return (str.empty()); }
                            , [](const std::string& str) -> bool { return (str.empty()); }
                            );
            }
      };

      class SindoElectronicReader: public MoleculeReader
      {
         private:
            using key_t = typename MoleculeReader::key_t;
            using return_t = typename MoleculeReader::return_t;

            std::string Type() const { return "SINDO_ELECTRONIC_READER"; } ///< return type of reader (for debug purposes)

            return_t ReadGradient(const key_t&, MoleculeInfo&);
            return_t ReadHessian(const key_t&, MoleculeInfo&);

         public:
            ///> we do not allow copying or default construction
            SindoElectronicReader() = delete; ///< deleted default ctor
            SindoElectronicReader(const SindoElectronicReader&) = delete; ///< deleted copy ctor
            SindoElectronicReader& operator=(const SindoElectronicReader&) = delete;  ///< deleted copy assignment

            SindoElectronicReader(MoleculeFile& aMoleculeFile)
               : MoleculeReader(aMoleculeFile)
            {
               this->RegisterFunction("Hessian" // define hessian keyword
                                    , molecule_field_t::HESSIAN
                                    , std::bind(&SindoElectronicReader::ReadHessian, this, std::placeholders::_1, std::placeholders::_2)
                                    );

               this->RegisterFunction("Gradient" // define gradient keyword
                                    , molecule_field_t::GRADIENT
                                    , std::bind(&SindoElectronicReader::ReadGradient, this, std::placeholders::_1, std::placeholders::_2)
                                    );
               this->InputDelimeter([](const std::string& str) -> bool { return (str.empty()); }
                            , [](const std::string& str) -> bool { return (str.empty()); }
                            );
            }
      };

   public:
      ///> we do not allow copying or default construction
      SindoReader() = delete; ///< deleted default ctor
      SindoReader(const SindoReader&) = delete; ///< deleted copy ctor
      SindoReader& operator=(const SindoReader&) = delete; ///< deleted copy assignment

      ///> ctor from molecule file
      SindoReader(MoleculeFile& aMoleculeFile)
         : MoleculeReader(aMoleculeFile)
      {
         Mout << " Creating Sindo reader " << std::endl;
         this->InputDelimeter(nullptr,nullptr); // define input begin / end (we read from start of file to end of file)
         this->RegisterFunction("[ Atomic Data ]" // define xyz keyword
                              , molecule_field_t::XYZ
                              , std::bind(&SindoReader::ReadAtomicData,this,std::placeholders::_1,std::placeholders::_2)
                              );
         this->RegisterFunction("[ Vibrational Data ]" // define xyz keyword
                              , molecule_field_t::FREQ | molecule_field_t::VIBCOORD
                              , std::bind(&SindoReader::ReadVibrationalData,this,std::placeholders::_1,std::placeholders::_2)
                              );

         this->RegisterFunction("[ Electronic Data ]"
                              , molecule_field_t::GRADIENT | molecule_field_t::HESSIAN
                              , std::bind(&SindoReader::ReadElectronicData, this,std::placeholders::_1,std::placeholders::_2)
                              );
      }
};

} /* namespace molecule */
} /* namespace midas */

#endif /* SINDOREADER_H_INCLUDED */
