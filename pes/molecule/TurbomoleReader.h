#ifndef TURBOMOLEREADER_H_INCLUDED
#define TURBOMOLEREADER_H_INCLUDED

#include <string>
#include "pes/molecule/MoleculeReader.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace molecule
{

/**
 * Class for reading Turbomole molecular files.
 **/
class TurbomoleReader
   :  public MoleculeReader
{
   private:
      //! Define key_t
      using key_t = typename MoleculeReader::key_t;
      //! Define return_t
      using return_t = typename MoleculeReader::return_t;

      //! Have we already read an unprojected Hessian
      bool mUnProjectedHessianRead = false;

      //! Read xyz-coordinates
      return_t ReadXYZ(const key_t&, MoleculeInfo&);
      //! Read frequencies
      return_t ReadFrequencies(const key_t&, MoleculeInfo&);
      //! Read vibrational coordinates, e.g. normal coordinates
      return_t ReadVibrationalCoord(const key_t&, MoleculeInfo&);
      //! Read Hessian
      return_t ReadHessian(const key_t&, MoleculeInfo&); 
      //! Read Gradient
      return_t ReadGradient(const key_t&, MoleculeInfo&);
      //! Read excited-state gradient
      return_t ReadExStGradient(const key_t&, MoleculeInfo&);
      
      //! Give type
      std::string Type() const override { return "TURBOMOLE_READER"; }

      //! Parse key
      key_t ParseKey(const key_t& aKey) const override;
      
      //!@{
      //! Helper functions
      bool IsTurbomoleKeyword(const key_t& aKey) { return (aKey.substr(0,1) == "$"); }
      bool NotTurbomoleKeyword(const key_t& aKey) { return !IsTurbomoleKeyword(aKey); }
      void OpenFile(std::ifstream&, const std::string, const key_t&, std::string&, std::string&) const;
      void CloseFile(std::ifstream&, const std::string, const std::string&, std::string&) const;
      //!@}
   public:
      //!@{
      //! We do not allow copying or default construction
      TurbomoleReader() = delete;
      TurbomoleReader(const TurbomoleReader&) = delete;
      TurbomoleReader& operator=(const TurbomoleReader&) = delete;
      //!@}
      
      //! Ctor
      TurbomoleReader(MoleculeFile& aMoleculeFile)
         : MoleculeReader(aMoleculeFile) 
      {
#ifdef VAR_MPI
         MidasWarning("TURBOMOLE READER IS NOT MPI SAFE YET. (or maybe it is, but it is not efficient.)");
#endif /* VAR_MPI */
         this->RegisterFunction("$coord"
                              , molecule_field_t::XYZ
                              , std::bind(&TurbomoleReader::ReadXYZ,this,std::placeholders::_1,std::placeholders::_2)
                              );
         this->RegisterFunction("$vibrational spectrum"
                              , molecule_field_t::FREQ
                              , std::bind(&TurbomoleReader::ReadFrequencies,this,std::placeholders::_1,std::placeholders::_2)
                              );
         this->RegisterFunction("$vibrational normal modes"
                              , molecule_field_t::VIBCOORD
                              , std::bind(&TurbomoleReader::ReadVibrationalCoord,this,std::placeholders::_1,std::placeholders::_2)
                              );
         this->RegisterFunction("$hessian"
                              , molecule_field_t::HESSIAN
                              , std::bind(&TurbomoleReader::ReadHessian,this,std::placeholders::_1,std::placeholders::_2)
                              );
         this->RegisterFunction("$grad"
                              , molecule_field_t::GRADIENT
                              , std::bind(&TurbomoleReader::ReadGradient, this, std::placeholders::_1, std::placeholders::_2)
                              );

         // Niels: Excited-state gradient is in the exstprop file, but there are also other things. For now, this should do.
         this->RegisterFunction("$exstprop"
                              , molecule_field_t::EXSTGRADIENT
                              , std::bind(&TurbomoleReader::ReadExStGradient, this, std::placeholders::_1, std::placeholders::_2)
                              );
      }
};

} /* namespace molecule */
} /* namespace midas */

#endif /* TURBOMOLEREADER_H_INCLUDED */
