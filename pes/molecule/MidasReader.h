/**
************************************************************************
*
* @file                MidasReader.h
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of MidasReader class used to read
*                      MidasCpp molecule input files.
*                      Defines recognized keywords
*
* Last modified:       19-05-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MIDASREADER_H_INCLUDED
#define MIDASREADER_H_INCLUDED

#include "MoleculeReader.h"

namespace midas
{
namespace molecule
{

/**
 * MidasReader - class to read MidasCpp molecule input files
 **/
class MidasReader
   : public MoleculeReader
{
   private:

      //! Define key_t (see MoleculeReaderTraits in MoleculeReader.h for definition)
      using key_t = typename MoleculeReader::key_t;

      //! define return_t (see MoleculeReaderTraits in MoleculeReader.h for definition)
      using return_t = typename MoleculeReader::return_t;

      //! Read xyz coordinate data
      return_t ReadXYZ(const key_t&, MoleculeInfo&);

      //! Read frequency data
      return_t ReadFrequencies(const key_t&, MoleculeInfo&);

      //! Read vibrational coordinate data
      return_t ReadVibrationalCoord(const key_t&, MoleculeInfo&);

      //! Read internal (curvilinear) coordinate data
      return_t ReadInternalCoord(const key_t&, MoleculeInfo&);
      
      //! Read inactive modes data
      return_t ReadInactiveModes(const key_t&, MoleculeInfo&);
      
      //! Read Sigma data
      return_t ReadSigma(const key_t&, MoleculeInfo&);

      //!Read gradient data 
      return_t ReadGradient(const key_t&, MoleculeInfo&);

      //! Read hessian data
      return_t ReadHessian(const key_t&, MoleculeInfo&);

      //! Read linear combination of vibrational coordinates
      return_t ReadLinComb(const key_t&, MoleculeInfo&);

      //! Read selected vibrations
      return_t ReadSelectVibs(const key_t&, MoleculeInfo&);

      //! Return type of reader (for debug purposes)
      std::string Type() const override { return "MIDAS_READER"; }

      //! parse input keywords (e.g. such that both "#1 xyz" and "#1XYZ are both valid keywords, i.e. spaces and capitalization doesn't matter)
      key_t ParseKey(const key_t&) const override;

   public:

      //! Default constructor, (copying or default construction not allowed)
      MidasReader() = delete;

      //! Copy constructor, (copying or default construction not allowed)
      MidasReader(const MidasReader&) = delete;

      //! Copy assignment, (copying or default construction not allowed)
      MidasReader& operator=(const MidasReader&) = delete;

      //! Constructor from molecule file info
      MidasReader(MoleculeFile& aMoleculeFile)
         : MoleculeReader(aMoleculeFile)
      {
         // Define input begin / end
         this->InputDelimeter([](const std::string& str) -> bool { if(str == "#0MOLECULEINPUT") {MidasWarning("#0MOLECULEINPUT is depreciated, please use #0MIDASMOLECULE instead"); return true;} return (str == "#0MIDASMOLECULE"); }
                            , [](const std::string& str) -> bool { if(str == "#0MOLECULEINPUTEND") {MidasWarning("#0MOLECULEINPUTEND is depreciated, please use #0MIDASMOLECULEEND instead"); return true;} return (str == "#0MIDASMOLECULEEND"); }
                             );
         // Define xyz keyword
         this->RegisterFunction("#1XYZ"
                              , molecule_field_t::XYZ
                              , std::bind(&MidasReader::ReadXYZ,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define freq keyword
         this->RegisterFunction("#1FREQ"
                              , molecule_field_t::FREQ
                              , std::bind(&MidasReader::ReadFrequencies,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define vibrational coordinate keyword
         this->RegisterFunction("#1VIBCOORD"
                              , molecule_field_t::VIBCOORD
                              , std::bind(&MidasReader::ReadVibrationalCoord,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define internal (curvilinear) coordinate keyword
         this->RegisterFunction("#1INTERNALCOORD"
                              , molecule_field_t::INTERNALCOORD
                              , std::bind(&MidasReader::ReadInternalCoord,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define hessian keyword
         this->RegisterFunction("#1HESSIAN"
                              , molecule_field_t::HESSIAN
                              , std::bind(&MidasReader::ReadHessian,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define gradient keyword
         this->RegisterFunction("#1GRADIENT"
                              , molecule_field_t::GRADIENT
                              , std::bind(&MidasReader::ReadGradient, this, std::placeholders::_1, std::placeholders::_2)
                              );
         // Define lincomb keyword
         this->RegisterFunction("#1LINCOMB"
                              , molecule_field_t::LINCOMB
                              , std::bind(&MidasReader::ReadLinComb,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define selected vibrations keyword
         this->RegisterFunction("#1SELECTVIBS"
                              , molecule_field_t::SELECTVIBS
                              , std::bind(&MidasReader::ReadSelectVibs,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define inactive vibrational modes keyword
         this->RegisterFunction("#1INACTIVEMODES" 
                              , molecule_field_t::INACTIVEMODES
                              , std::bind(&MidasReader::ReadInactiveModes,this,std::placeholders::_1,std::placeholders::_2)
                              );
         // Define sigma keyword
         this->RegisterFunction("#1SIGMA"
                              , molecule_field_t::SIGMA
                              , std::bind(&MidasReader::ReadSigma,this,std::placeholders::_1,std::placeholders::_2)
                              );
      }
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MIDASREADER_H_INCLUDED */
