#ifndef MOLDENWRITER_H_INCLUDED
#define MOLDENWRITER_H_INCLUDED

#include <string>
#include "MoleculeWriter.h"

namespace midas
{
namespace molecule
{

/**
 *
 **/
class MoldenWriter: public MoleculeWriter
{
   private:
      MoldenWriter() = delete;
      MoldenWriter(const MoldenWriter&) = delete;
      MoldenWriter& operator=(const MoldenWriter&) = delete;

      bool Initialize();
      bool Finalize();

      bool WriteFrequencies(const FrequencyData&);
      bool WriteXYZ(const XYZData&);
      bool WriteVibrationalCoord(const VibrationalCoordinateData&);
      bool WriteZmat(const ZmatData&)
             {MidasWarning("ZMAT writer is not implemented for molden"); return false;}
      bool WriteHessian(const HessianData&)
             {MidasWarning("HESSIAN writer is not implemented for molden"); return false;}
      bool WriteGradient(const GradientData&)
             {MidasWarning("GRADIENT writer is not implemented for molden"); return false;}
      bool WriteSigma(const SigmaData&)
             {MidasWarning("Sigma writer is not implemented for molden"); return false;}

   public:
      //!
      MoldenWriter(const MoleculeFileInfo& aMoleculeFileInfo);
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MOLDENWRITER_H_INCLUDED */
