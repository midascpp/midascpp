#ifndef MOLECULEWRITER_H_INCLUDED
#define MOLECULEWRITER_H_INCLUDED

#include <fstream>
#include <string>
#include <memory>

#include "util/AbstractFactory.h"
#include "util/Error.h"

#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeData.h"


namespace midas
{
namespace molecule
{

// Forward declarations
class MoleculeInfo;
class MoleculeWriter;

/**
 * Factory
 **/
using MoleculeWriterFactory = AbstractFactory<MoleculeWriter*(const MoleculeFileInfo&), std::string>;

template<class A>
using MoleculeWriterRegistration = AbstractFactoryRegistration<MoleculeWriter*(const MoleculeFileInfo&),A,std::string>;

/**
 * MoleculeWriter class
 **/
class MoleculeWriter
{
   private:

      //////
      // virtual functions (to be overloaded)
      //////
      virtual bool Initialize() = 0; // write any common file headers
      virtual bool Finalize() = 0;   // write any common file finalizers

      virtual bool WriteXYZ(const XYZData&) = 0;
      virtual bool WriteFrequencies(const FrequencyData&) = 0;
      virtual bool WriteVibrationalCoord(const VibrationalCoordinateData&) = 0;
      virtual bool WriteSigma(const SigmaData&) = 0;
      virtual bool WriteZmat(const ZmatData&) = 0;
      virtual bool WriteHessian(const HessianData&) = 0;
      virtual bool WriteGradient(const GradientData&) = 0;

      //////
      // Factory Interface
      //////
      static std::unique_ptr<MoleculeWriter> Factory(const MoleculeFileInfo& aMoleculeFileInfo)
      {
         return MoleculeWriterFactory::create(aMoleculeFileInfo.Type(), aMoleculeFileInfo);
      }

      /////
      // write functions
      /////
      bool Write(const MoleculeInfo&);
      bool Write(const MoleculeInfo&, const MoleculeFields&);

   protected:
      MoleculeFile mMoleculeFile; // molecule file (protected: used by derived types)
      MoleculeFields mWritingOrder; // defines the writing order of the fields in a given file, overwriten by the relevant writer if needed

      /////
      // ctor (protected: can only be called from derived types)
      /////
      MoleculeWriter(const MoleculeFileInfo& aMoleculeFileInfo): mMoleculeFile(aMoleculeFileInfo) {}

   public:
      /////
      // deleted functions (we do not allow copying)
      /////
      MoleculeWriter() = delete;
      MoleculeWriter(const MoleculeWriter&) = delete;
      MoleculeWriter& operator=(const MoleculeWriter&) = delete;

      /////
      // virtual dtor
      /////
      virtual ~MoleculeWriter() = 0;

      //////////////////////
      //
      // Interface (use only this function to invoke the molecule writer!)
      //
      //////////////////////
      static bool WriteMolecule(const MoleculeFileInfo::Set&, const MoleculeInfo&);
};

// define dtor
inline MoleculeWriter::~MoleculeWriter() { }

/**
 * Handle class (ensures we do not try to use a nullptr)
 **/
class MoleculeWriterHandle
{
   private:
      //! Actual writer
      std::unique_ptr<MoleculeWriter> mWriter;

   public:
      // We do not allow default construction
      MoleculeWriterHandle() = delete;

      //! Ctor
      MoleculeWriterHandle(std::unique_ptr<MoleculeWriter> aWriter): mWriter(std::move(aWriter))
      {
      }

      //! Get writer
      std::unique_ptr<MoleculeWriter>& operator->()
      {
         MidasAssert(bool(mWriter),"MoleculeWriter is Null");
         return mWriter;
      }
};

} /* namespace molecule */
} /* namepace midas */

#endif /* MOLECULEWRITER_H_INCLUDED */
