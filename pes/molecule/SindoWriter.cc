#include "SindoWriter.h"

#include "MoleculeInfo.h"
#include "input/GetLine.h"
#include "nuclei/AtomicData.h"
#include "input/Input.h" /// < For gAtomicData

#include <iostream>

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeWriterRegistration<SindoWriter> registerMinfoWriter("MINFO");
MoleculeWriterRegistration<SindoWriter> registerSindoWriter("SINDO");

SindoWriter::SindoWriter(const MoleculeFileInfo& aMoleculeFileInfo)
   :  MoleculeWriter(aMoleculeFileInfo)
{
   mMoleculeFile.FileStream().setf(std::ios::scientific);
   mMoleculeFile.FileStream().setf(std::ios::uppercase);
   mMoleculeFile.FileStream().precision(I_12);
   MoleculeWriter::mWritingOrder = {molecule_field_t::XYZ,
                                    molecule_field_t::GRADIENT,
                                    molecule_field_t::HESSIAN,
                                    molecule_field_t::FREQ,
                                    molecule_field_t::VIBCOORD};
                                    //This is the default order for the Sindo output
                                    //It is nessesary to be this way to allow our
                                    //ouput to be read by JSindo
}

/**
 * Add the initial lines to the .minfo file
 **/
bool SindoWriter::Initialize()
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("# minfo File version 2:"))
   {
      mMoleculeFile.FileStream() << "# minfo File version 2:" << "\n"
                                 << "#" << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a '# minfo File version 2:' block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 * Nothing to do when finalizing a .minfo file
 **/
bool SindoWriter::Finalize()
{
   return true;
}

/**
 *
 **/
bool SindoWriter::WriteXYZ(const XYZData& xyz)
{
   bool success = false;

   if(!mMoleculeFile.SearchKeyword("[ Atomic Data ]"))
   {
      // output first line
      mMoleculeFile.FileStream() << "[ Atomic Data ] \n";
      mMoleculeFile.FileStream() << xyz->NumAtom() << "\n";
      // output each atoms
      std::stringstream out;
      for(In i = 0; i < xyz->NumAtom(); ++i)
      {
         In charge = gAtomicData.GetZnucFromString(xyz->Label(i));
         In isotope = xyz->Isotope(i);
         if(isotope == 0)
            isotope = gAtomicData.MostCommonIsotope(charge);

         Nb mass = gAtomicData.GetMass(charge, isotope);
         out  << std::setw(4) << xyz->Label(i) << ",";
         out  << std::setw(6) << charge << ",";
         out  << std::setprecision(4) << std::right << fixed << std::setw(13) << mass;
         out  << "," << std::setw(17) << std::setprecision(8) << xyz->X(i);
         out  << "," << std::setw(17) << std::setprecision(8) << xyz->Y(i);
         out  << "," << std::setw(17) << std::setprecision(8) << xyz->Z(i) << "\n";

      }
      mMoleculeFile.FileStream() << out.str();
      success = true;
   }
   else
   {
      MidasWarning("Already a [ Atomic Data ] block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 * Writes the electronic block if it is not present yet
 **/

void SindoWriter::WriteElectronicBlock()
{
   if(!mMoleculeFile.SearchKeyword("[ Electronic Data ]"))
   {
      mMoleculeFile.FileStream() << "\n[ Electronic Data ]\n";
   }
}

/**
 * Writes the vibrational block if it is not present yet
 **/
void SindoWriter::WriteVibrationalBlock()
{
   if(!mMoleculeFile.SearchKeyword("[ Vibrational Data ]"))
   {
      mMoleculeFile.FileStream() << "\n[ Vibrational Data ]\n" << "Normal modes\n";
   }
}

/**
 *
 **/
bool SindoWriter::WriteVibrationalCoord(const VibrationalCoordinateData& vib_coord)
{
   bool success = false;
   WriteVibrationalBlock();

   if(!mMoleculeFile.SearchKeyword("Vibrational vector"))
   {
      // print first line
      mMoleculeFile.FileStream() << "Vibrational vector\n" << std::setprecision(12);

      // print coordinates
      for(In i = 0; i < vib_coord->NumCoords(); ++i)
      {
         mMoleculeFile.FileStream() << "Mode " << i << "\n" << vib_coord->Coordinate(i).size();
         std::stringstream out;
         out << std::setprecision(8) << std::scientific;
         for(In j = 0; j < vib_coord->NumDisplacements(); ++j)
         {
            if( (j%5 == 0) )
            {
               out << " \n";
            }
            else
            {
               out << ", ";
            }
            out << std::setw(15) << vib_coord->Coordinate(i).at(j);
         }
         mMoleculeFile.FileStream() << out.str() << " \n";
      }
      success = true;
   }
   else
   {
      MidasWarning("Already a Vibrational vector block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool SindoWriter::WriteFrequencies(const FrequencyData& freq)
{
   bool success = false;
   WriteVibrationalBlock();
   if(!mMoleculeFile.SearchKeyword("Vibrational Frequency"))
   {
      mMoleculeFile.FileStream() << "Vibrational Frequency\n" << freq->NumFreq();
      std::stringstream out;
      out << std::setprecision(8) << std::scientific;
      for(In i = 0; i < freq->NumFreq(); ++i)
      {
         if( (i%5 == 0) )
         {
            out << " \n";
         }
         else
         {
            out << ", ";
         }
         out << std::setw(15) << (freq->Frequency(i));
      }
      mMoleculeFile.FileStream() << out.str() << "\n" << std::flush; // \n for aestetics and flush the output to file
      success = true;
   }
   else
   {
      MidasWarning("Already a Vibrational Frequency block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

bool SindoWriter::WriteGradient(const GradientData& arGrad)
{
   bool success = false;

   WriteElectronicBlock();
   if(!mMoleculeFile.SearchKeyword("Gradient"))
   {
      mMoleculeFile.FileStream() << "Gradient\n" << arGrad->Size();
      std::stringstream out;
      out << std::setprecision(8) << std::scientific;
      for(In i = 0; i < arGrad->Size(); ++i)
      {
         if( (i%5 == 0) )
         {
            out << " \n";
         }
         else
         {
            out << ", ";
         }
         out << std::setw(15) << (arGrad->at(i));
      }
      if((arGrad->Size() - 1 ) % 5)
         out << " \n";

      mMoleculeFile.FileStream() << out.str();
   }
   else
   {
      MidasWarning("Already a Gradient block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 * Write the hessian block data, note that only the upper triangular part of the matrix are output here
 **/
bool SindoWriter::WriteHessian(const HessianData& arHess)
{
   bool success = false;
   WriteElectronicBlock();
   if(!mMoleculeFile.SearchKeyword("Hessian"))
   {
      In upper_trig_size = arHess->Nrows()*(arHess->Nrows()+1)/2;
      mMoleculeFile.FileStream() << "Hessian\n" << upper_trig_size;
      std::stringstream out;
      out << std::setprecision(8) << std::scientific;
      In count = 0;
      for(In i = 0; i < arHess->Nrows(); ++i)
         for(In j = 0; j <= i; ++j)
         {
            if( (count%5 == 0) )
            {
               out << " \n";
            }
            else
            {
               out << ", ";
            }
             out << std::setw(15) << (arHess->at(i,j));
            ++count;
         }
      if((count - 1 ) % 5)
         out << " \n";

      mMoleculeFile.FileStream() << out.str();
   }
   else
   {
      MidasWarning("Already a Hessian block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

} /* namespace molecule */
} /* namespace midas */
