#ifndef MIDAS_MOLECULE_MOLECULE_UTILITY_H_INCLUDED
#define MIDAS_MOLECULE_MOLECULE_UTILITY_H_INCLUDED

//#include "pes/molecule/MoleculeInfo.h"
#include "pes/kinetic/CoriolisCoeffs.h"

namespace midas
{
namespace molecule
{

class MoleculeInfo;

//! Normalize normal coordinates
void NormalizeNormalCoordinates(MoleculeInfo& aMolecule);

//! Orthonormalize normal coordinates
void OrthoNormalizeNormalCoordinates(MoleculeInfo& aMolecule);

//! Create a MidasMatrix with translational coordinates
MidasMatrix CreateTranslationalCoordinates(const MoleculeInfo& aMolecule);

//! Create a MidasMatrix with rotational coordinates
MidasMatrix CreateRotationalCoordinates(const MoleculeInfo& aMolecule);

//! Checks if the normalcoordinates are orthogonal.
bool NormAnalysis(const MoleculeInfo& aMolecule, Nb aThreshold = 1e-12);

////! Purify normalcoordinates of molecule using point group symmetry arguments.
//void SymmetryPurification(MoleculeInfo& aMolecule, const SymInfo& aGrpInfo);

//! Dumps Coriolis Matrices
void DumpCoriolis
   (  const MoleculeInfo& aMolecule
   ,  const std::string& aFilename
   ,  const std::vector<std::string>& aModeNames
   );

//! Calculate inertia tensor.
MidasMatrix CalculateInertiaTensor(const std::vector<Nuclei>& aStructure);

//! Calculate center of mass displacement.
MidasVector FindCenterOfMass(  const std::vector<Nuclei>& aStructure);

//! Shift structure by 3D vector.
void ShiftStructure
   (  std::vector<Nuclei>& aStructure
   ,  const MidasVector&   aShift
   );

//! Rotate a structure using a 3x3 rotation matrix.
void RotateCoordinates
   (  std::vector<Nuclei>& aStructure
   ,  const MidasMatrix& aRotMat
   );

//! Find rotation matrix that will rotate the given structure to the inertia frame.
MidasMatrix FindRotationToInertiaFrame
   (  const std::vector<Nuclei>& aStructure
   ,  int                        aIsDiagonalUlps = 1000
   );

} /* namespace molecule */
} /* namespace midas */

#endif /* MIDAS_MOLECULE_MOLECULE_UTILITY_H_INCLUDED */
