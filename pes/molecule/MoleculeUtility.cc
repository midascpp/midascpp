#include "pes/molecule/MoleculeUtility.h"

#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

#include "pes/molecule/MoleculeInfo.h"

#include "lapack_interface/SYEV.h"

#include "libmda/util/to_string.h"

namespace midas
{
namespace molecule
{

namespace detail 
{

/**  
 * Find ordering of diagonal elements of inertia tensor
 * such that elements are ordering as 0,0 < 1,1 < 2,2.
 *
 * @param aInertiaTensor   The inertia tensor.
 * @param aOrdering        On exit will hold ordering.
 **/
void InertiaTensorDiagonalOrdering
   (  const MidasMatrix& aInertiaTensor
   ,  int                aOrdering[3]
   )
{
   if (aInertiaTensor[0][0] < aInertiaTensor[1][1])
   {
      if (aInertiaTensor[1][1] > aInertiaTensor[2][2])
      {
          if (aInertiaTensor[0][0] < aInertiaTensor[2][2])
          {
             // 0 < 2 < 1
             aOrdering[0] = 0;
             aOrdering[1] = 2;
             aOrdering[2] = 1;
          }
          else
          {
             // 2 < 0 < 1
             aOrdering[0] = 2;
             aOrdering[1] = 0;
             aOrdering[2] = 1;
          }
      }
      else
      {
         // 0 < 1 < 2
         aOrdering[0] = 0;
         aOrdering[1] = 1;
         aOrdering[2] = 2;
      }
   }
   else
   {
      if (aInertiaTensor[1][1] < aInertiaTensor[2][2])
      {
         if (aInertiaTensor[0][0] < aInertiaTensor[2][2])
         {
            // 1 < 0 < 2
            aOrdering[0] = 1;
            aOrdering[1] = 0;
            aOrdering[2] = 2;
         }
         else
         {
            // 1 < 2 < 0
            aOrdering[0] = 1;
            aOrdering[1] = 2;
            aOrdering[2] = 0;
         }
      }
      else
      {
         // 2 < 1 < 0
         aOrdering[0] = 2;
         aOrdering[1] = 1;
         aOrdering[2] = 0;
      }
   }
}

/**
 * Calculate dot product between normal coordinates.
 * Defined as:
 *
 *    < u_i | M | u_j > = < l_i | l_j >
 *
 * @param aMolecule The molecule.
 * @param aVec1     First coordinate vector.
 * @param aVec2     Second coordinate vector.
 *
 * @return   Returns mass weighted dot product.
 **/
auto NormalCoordinateDot
   (  const MoleculeInfo& aMolecule
   ,  const MidasVector&  aVec1
   ,  const MidasVector&  aVec2
   )
{
   const auto number_of_nuclei   = aMolecule.GetNumberOfNuclei(); 
   
   // Calculate dot product of mass weighted coordinate
   auto dot_prod1 = C_0;
   auto dot_prod2 = C_0;
   auto dot_prod3 = C_0;
   for(int k = 0; k < number_of_nuclei; ++k)
   {
      auto mass  = aMolecule.GetMasses().at(k);
      auto index = 3 * k;
      
      dot_prod1 += aVec1[index]     * mass * aVec2[index];
      dot_prod2 += aVec1[index + 1] * mass * aVec2[index + 1];
      dot_prod3 += aVec1[index + 2] * mass * aVec2[index + 2];
   }
   
   // Collect result and return
   auto dot = dot_prod1 + dot_prod2 + dot_prod3;

   return dot;
}

/**
 * Normalize single coordinate.
 *
 * @param aMolecule    The molecule (to get number of nuclei and vibs).
 * @param aVec         The coordinate, on output will be normalized.
 *
 * @return    Returns norm of the vector before normalization.
 **/
auto NormalizeNormalCoordinate
   (  const MoleculeInfo& aMolecule
   ,  MidasVector&        aVec
   )
{
   const auto number_of_nuclei   = aMolecule.GetNumberOfNuclei(); 
   
   auto dot               = NormalCoordinateDot(aMolecule, aVec, aVec);
   auto dot_sqrt          = std::sqrt(dot);
   auto dot_prod_inv_sqrt = 1.0 / dot_sqrt;
   
   // Normalize each coordinate when mass weighted
   for(int k = 0; k < number_of_nuclei; ++k)
   {
      auto index         = 3 * k;

      aVec[index]     = dot_prod_inv_sqrt * aVec[index];
      aVec[index + 1] = dot_prod_inv_sqrt * aVec[index + 1];
      aVec[index + 2] = dot_prod_inv_sqrt * aVec[index + 2];
   }

   return dot_sqrt;
}

/**
 * Orthogonalize a normal coordinate on another coordinate.
 *
 * @param aMolecule  The molecule.
 * @param aVec       The coordinate to orthogonalize. On output will be orthogonal to aOtherVec.
 * @param aOtherVec  The coordinate to orthogonalize against. Will not be modified.
 **/
void OrthogonalizeNormalCoordinate
   (  const MoleculeInfo&  aMolecule
   ,  MidasVector&         aVec
   ,  const MidasVector&   aOtherVec
   )
{
   const auto number_of_vibs     = aMolecule.GetNoOfVibs();
   const auto number_of_nuclei   = aMolecule.GetNumberOfNuclei(); 
   const auto number_of_elements = number_of_nuclei * 3;

   // Calculate dot product of mass weighted coordinate
   auto norm = NormalCoordinateDot(aMolecule, aVec, aVec);
   auto dot  = NormalCoordinateDot(aMolecule, aVec, aOtherVec);
   auto norm_inv_sqrt = 1.0 / norm;

   // Normalize each coordinate when mass weighted
   for(int k = 0; k < number_of_nuclei; ++k)
   {
      auto sqrt_mass     = std::sqrt(aMolecule.GetMasses().at(k));
      auto inv_sqrt_mass = 1.0 / sqrt_mass;
      auto index         = 3 * k;
      
      aVec[index]     -= dot * norm_inv_sqrt * aOtherVec[index];
      aVec[index + 1] -= dot * norm_inv_sqrt * aOtherVec[index + 1];
      aVec[index + 2] -= dot * norm_inv_sqrt * aOtherVec[index + 2];
   }
}

} /* namespace detail */

using namespace libmda;

/**
 * Normalize normal coordinates the correct MidasCpp way.
 * Normal coordinates are stored in a mass weighted form.
 * In Midas we store:
 *
 *    U = G^(1/2) L
 *
 * where G has **inverse** masses on the diagonal.
 * We thus have to remove the G^(1/2) scaling before normalizing L.
 * This means that the normalcoordinates are scaled with the sqrt(mass) and normalized.
 * I.e. orthonormalize such that:
 *  
 *   < u_i | M | u_i > = < l_i | l_ i > = 1.0
 *
 * where M is a matrix with atomic masses in the diagonal:
 *
 *       / m_1 0.0 ...                            \
 *       | 0.0 m_1 0.0 ...                        |
 *       | ... 0.0 m_1 0.0 ...                    |
 *       |     ... 0.0 m_2 0.0 ,,,                |
 *   M = |             0.0 m_2 0.0 ...            |
 *       |             ... 0.0 m_2 0.0 ...        |
 *       |                 ... 0.0 ...            |
 *       |                     ...                |
 *       |                                ... 0.0 |
 *       \                                0.0 m_n /
 *
 * @param aMolecule   The molecule to "mass-weight normalize".
 **/
void NormalizeNormalCoordinates
   (  MoleculeInfo& aMolecule
   )
{
   const auto number_of_vibs     = aMolecule.GetNoOfVibs();
   const auto number_of_nuclei   = aMolecule.GetNumberOfNuclei(); 
   
   MidasVector vec(3 * number_of_nuclei);

   for(int i = 0; i < number_of_vibs; ++i)
   {
      // Get coordinate
      aMolecule.NormalCoord().GetRow(vec, i);

      // Normalize
      auto norm = detail::NormalizeNormalCoordinate(aMolecule, vec);
      
      // Set normalized coordinates
      aMolecule.NormalCoord().AssignRow(vec, i);
   }
}

/**
 * Orthonormalize normal coordinates such that:
 *
 *    < u_i | M | u_i > = < l_i | l_i > = 1.0
 *
 *    < u_i | M | u_j > = < l_i | l_j > = 0.0
 *
 * where M is a matrix with atomic masses in the diagonal:
 *
 *       / m_1 0.0 ...                            \
 *       | 0.0 m_1 0.0 ...                        |
 *       | ... 0.0 m_1 0.0 ...                    |
 *       |     ... 0.0 m_2 0.0 ,,,                |
 *   M = |             0.0 m_2 0.0 ...            |
 *       |             ... 0.0 m_2 0.0 ...        |
 *       |                 ... 0.0 ...            |
 *       |                     ...                |
 *       |                                ... 0.0 |
 *       \                                0.0 m_n /
 *
 * @param aMolecule   The molecule to orthogonalize normal coordinates for.
 **/
void OrthoNormalizeNormalCoordinates
   (  MoleculeInfo& aMolecule
   )
{
   const int  n_ortho_max         = 20;
   const auto norm_reortho_thresh = 1e-2;

   const auto number_of_vibs   = aMolecule.GetNoOfVibs();
   const auto number_of_nuclei = aMolecule.GetNumberOfNuclei(); 
   
   MidasVector vec      (3 * number_of_nuclei);
   MidasVector other_vec(3 * number_of_nuclei);
 
   // Start with normalized coordinates
   NormalizeNormalCoordinates(aMolecule);

   for(int i = 0; i < number_of_vibs; ++i)
   {
      // Get coordinate
      aMolecule.NormalCoord().GetRow(vec, i);
   
      int n_ortho = 0;
      while(n_ortho < n_ortho_max)
      {
         for(int j = 0; j < i; ++j)
         {
            aMolecule.NormalCoord().GetRow(other_vec, j);
            detail::OrthogonalizeNormalCoordinate(aMolecule, vec, other_vec);
         }

         // Normalize
         auto norm = detail::NormalizeNormalCoordinate(aMolecule, vec);

         if(std::fabs(1.0 - norm) < norm_reortho_thresh)
         {
            break; /* break while */
         }

         ++n_ortho;
      }

      // Set normalized coordinates
      aMolecule.NormalCoord().AssignRow(vec, i);
   }
}

/**
 * Create non-mass weighted normalized translational coordinates.
 * These will be returned in a 3 x (3*N) MidasMatrix, with
 *
 *    row    translation
 *    0      x
 *    1      y
 *    2      z
 *
 * @param aMolecule   The molecule.
 *
 * @return   Returns a MidasMatrix with mass weighted coordinates (multiplied by G^(1/2), 
 *           that are non-mass-weight normalized.
 *           This way they should behave like the U coordinates stored in MoleculeInfo.
 **/
MidasMatrix CreateTranslationalCoordinates
   (  const MoleculeInfo&  aMolecule
   )
{
   const auto number_of_nuclei = aMolecule.GetNumberOfNuclei(); 

   MidasMatrix mat{3, 3 * number_of_nuclei, C_0};

   // Create coordinates
   for(int i = 0; i < number_of_nuclei; ++i)
   {
      auto index         = i * 3;
      auto inv_sqrt_mass = 1.0 / std::sqrt(aMolecule.GetMasses().at(i));
      mat[0][index]      = inv_sqrt_mass * MidasMatrix::value_type(1.0);
      mat[1][index + 1]  = inv_sqrt_mass * MidasMatrix::value_type(1.0);        
      mat[2][index + 2]  = inv_sqrt_mass * MidasMatrix::value_type(1.0);
   }
   
   // Normalize
   MidasVector vec(3 * number_of_nuclei);

   mat.GetRow(vec, 0);
   auto norm0 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 0);
   
   mat.GetRow(vec, 1);
   auto norm1 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 1);
   
   mat.GetRow(vec, 2);
   auto norm2 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 2);
   
   // Return coordinates
   return mat;
}

/**
 *
 **/
MidasMatrix CreateRotationalCoordinates
   (  const MoleculeInfo& aMolecule
   )
{
   MIDASERROR("NOT IMPLEMENTED YET! CURRENT IMPLEMENTATION IS WRONG");

   const auto  number_of_nuclei = aMolecule.GetNumberOfNuclei(); 
   const auto& structure        = aMolecule.GetCoord();
   
   MidasMatrix mat{3, 3 * number_of_nuclei, C_0};

   for(int i = 0; i < number_of_nuclei; ++i)
   {
      auto index         = i * 3;
      auto inv_sqrt_mass = 1.0 / std::sqrt(aMolecule.GetMasses().at(i));

      mat[0][index + 1] = - structure[i].Z() * inv_sqrt_mass;
      mat[0][index + 2] =   structure[i].Y() * inv_sqrt_mass;
      
      mat[1][index]     =   structure[i].Z() * inv_sqrt_mass;
      mat[1][index + 2] = - structure[i].X() * inv_sqrt_mass;

      mat[2][index]     = - structure[i].Y() * inv_sqrt_mass;
      mat[2][index + 1] =   structure[i].X() * inv_sqrt_mass;
   }
   
   // Normalize
   MidasVector vec(3 * number_of_nuclei);

   mat.GetRow(vec, 0);
   auto norm0 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 0);
   
   mat.GetRow(vec, 1);
   auto norm1 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 1);
   
   mat.GetRow(vec, 2);
   auto norm2 = detail::NormalizeNormalCoordinate(aMolecule, vec);
   mat.AssignRow(vec, 2);

   return mat;
}

/**
 * Check whether the normal coordinates in molecule are orthorgonal.
 *
 * @param aMolecule   The molecule to check.
 * @param aThreshold  A numerical threshold for the normal coordinates.
 *
 * @return    Returns 'true' if normalcoordinates are orthonormal to threshold, otherwise 'false'.
 **/
bool NormAnalysis
   (  const MoleculeInfo& aMolecule
   ,  Nb                  aThreshold
   )
{
   const auto number_of_vibs   = aMolecule.GetNoOfVibs();
   const auto number_of_nuclei = aMolecule.GetNumberOfNuclei(); 
   
   MidasVector vec1(3 * number_of_nuclei);
   MidasVector vec2(3 * number_of_nuclei);

   bool success = true;
   constexpr bool mass_weighted = true; /* for debugging */
   
   // Outer loop over coordinates.
   for (In i = I_0; i < number_of_vibs; ++i)
   {
      aMolecule.NormalCoord().GetRow(vec1, i);
      
      // Inner loop over coordinates.
      for (In j = i; j < number_of_vibs; ++j)
      {
         aMolecule.NormalCoord().GetRow(vec2, j);

         // Calculate dot product.
         auto dot_prod1 = C_0;
         auto dot_prod2 = C_0;
         auto dot_prod3 = C_0;
         for (int k = 0; k < number_of_nuclei; ++k)
         {
            auto mass  = mass_weighted ? aMolecule.GetMasses().at(k) : 1.0;
            auto index = 3 * k;

            dot_prod1 += vec1[index]     * mass * vec2[index];
            dot_prod2 += vec1[index + 1] * mass * vec2[index + 1];
            dot_prod3 += vec1[index + 2] * mass * vec2[index + 2];
         }

         auto dot_prod = dot_prod1 + dot_prod2 + dot_prod3;

         // Check whether dot product is zero.
         if ( (i == j) && (std::fabs(dot_prod - C_1) > aThreshold) )
         {
            MidasWarning(" Warning, diagonal dot product is " + libmda::util::to_string_with_precision(dot_prod) + " Something might be wrong with displ. coordinates for vector " + std::to_string(i));
            success = false;
         }
         else if ( (i != j) && (std::fabs(dot_prod - C_0) > aThreshold) )
         {
            MidasWarning(" Warning, off-diagonal dot product is " + libmda::util::to_string_with_precision(dot_prod) + " Something might be wrong with displ.coordinates for vector pair " + std::to_string(i) + "," + std::to_string(j));
            success = false;
         }
      }
   }

   return success;
}

/**
 * Dump Coriolis Matrices
 *
 * @param aMolecule   Info on the Molecule
 * @param aFilename   Name of the file to save the Coriolis Matrices
 * @param aModeNames  vector with names of the modes
 **/
void DumpCoriolis
   (  const MoleculeInfo& aMolecule
   ,  const std::string& aFilename
   ,  const std::vector<std::string>& aModeNames
   )
{
   CoriolisCoeffs coriolis(aMolecule.GetNoOfVibs());
   coriolis.FillCoriolisMatrices(aMolecule.GetCoord(), aMolecule.NormalCoord());
   coriolis.DumpCoriolisCoeffsToFile(aFilename,aModeNames);
}

///**
//* Purify normal coordinates by using 
//* projection operators for the various Irreps
//* Coded for abelian groups!
//*
//* NOT TESTED!
//**/
//void SymmetryPurification
//   (  MoleculeInfo& aMolecule
//   ,  const SymInfo& arGrpSym
//   )
//{
//   MidasWarning("THIS FUNCTION IS NOT TESTED!");
//
//   Mout << " Symmetry adaptation of normal coordinates " << endl;
//   In n_frq=mNormalCoord->Nrows();
//   In n_dim=mNormalCoord->Ncols();
//   MidasMatrix norm_mat(n_frq,n_dim);
//   //here mass un-weight
//   MidasVector nuc_mass(n_dim);
//   In n_nuc=mNumberOfNuclei;
//   In k = I_0;
//   for (In i=I_0;i<n_nuc;i++)
//   {
//      //nuc_mass[k]     = C_1/(sqrt(C_FAMU*mMassVector[i]));
//      //nuc_mass[k+I_1] = C_1/(sqrt(C_FAMU*mMassVector[i]));
//      //nuc_mass[k+I_2] = C_1/(sqrt(C_FAMU*mMassVector[i]));
//      nuc_mass[k]     = C_1/(sqrt(C_FAMU*mMassVector->at(i)));
//      nuc_mass[k+I_1] = C_1/(sqrt(C_FAMU*mMassVector->at(i)));
//      nuc_mass[k+I_2] = C_1/(sqrt(C_FAMU*mMassVector->at(i)));
//      k += 3;
//   }
//   MidasVector n_mode(n_dim);
//   for (In i=I_0; i<n_frq; i++)
//   {
//      mNormalCoord->GetRow(n_mode,i);
//      for (In j=I_0;j<n_dim;j++)
//      {
//         n_mode[j]/=(nuc_mass[j]*sqrt(C_FAMU));
//      }
//      norm_mat.AssignRow(n_mode,i);
//   }
//   MidasMatrix norm_matp(n_frq,n_dim);
//   norm_matp.Zero();
//   In n_bas=norm_mat.Nrows();
//   Mout << " No. of basis functions: " << n_bas << endl;
//   In n_class=arGrpSym.Nclass();
//   // In n_rep=arGrpSym.Nclass();    //commented as it was not used
//   In n_op=arGrpSym.NopSym();
//   vector<MidasMatrix> reps;
//   //for each operation construct the representation matrix
//   //for abelian groups only
//   MidasMatrix s_mat(n_bas,C_0);
//   for (In i_op=I_0; i_op<n_op; i_op++)
//   {
//      Mout << " Construct representation for operator: " << arGrpSym.GetOpLab(i_op) << endl;
//      MidasMatrix rep(n_bas,C_0);
//      MidasVector vec1(n_dim,C_0);
//      MidasVector vec2(n_dim,C_0);
//      for (In i1=I_0; i1<n_bas; i1++)
//      {
//         norm_mat.GetRow(vec1,i1);
//         for (In j1=I_0; j1<n_bas; j1++)
//         {
//            norm_mat.GetRow(vec2,j1);
//            s_mat[i1][j1]=Dot(vec1,vec2);
//            string sym_type = this->GetModeSymInfo(j1);
//            In sym_no=arGrpSym.GetIrrepNr(sym_type);
//            Nb ch_tab=arGrpSym.GetCharacter(sym_no,i_op);
//            vec2.Scale(ch_tab);
//            rep[i1][j1]=Dot(vec1,vec2);
//         }
//      }
//      Mout << " Overlap matrix:" << endl;
//      Mout << s_mat;
//      MidasMatrix s_mat_copy(n_bas,C_0);
//      s_mat_copy=s_mat;
//      Invert(s_mat);
//      //construct the truw representation matrix
//      MidasMatrix rep1=rep;
//      rep=s_mat*rep1;
//      Mout << " Matrix representation: " << endl;
//      Mout << rep;
//      reps.push_back(rep);
//   }
//   //count the number of times each Irreps is present
//   MidasVector tr_vec(n_class,C_0);
//   for (In i_op=I_0; i_op<n_op; i_op++)
//   {
//      MidasMatrix rep(n_bas,C_0);
//      rep=reps[i_op];
//      Nb trace=C_0;
//      for (In i=I_0; i<n_bas; i++) trace+=rep[i][i];
//      tr_vec[i_op]=trace;
//   }
//   Mout << " trace: " << endl;
//   Mout << tr_vec << endl;
//   vector<In> nrep;
//   for (In i_rep=I_0; i_rep<n_class; i_rep++)
//   {
//      Nb csum=C_0;
//      //for abealian groups only...
//      for (In i_op=I_0; i_op<n_class; i_op++)
//      {
//         Nb ch_tab=arGrpSym.GetCharacter(i_rep,i_op);
//         csum+=ch_tab*tr_vec[i_op];
//      }
//      csum/=n_op;
//      Mout << " csum:" << csum << endl;
//      In nsum=In(csum+C_I_2); //round correctly!
//      if (fabs(csum-Nb(nsum))>C_NB_EPSILON)
//         Mout << " nsum: " << nsum << endl;
//         //MIDASERROR(" Not pure integer value! ");
//      nrep.push_back(nsum);
//   }
//   Mout << " Each irreducible representation is present the number of times indicated:" << endl;
//   for (In i_rep=I_0; i_rep<n_class; i_rep++)
//   {
//      if (nrep[i_rep]>I_0) 
//      {
//         string sym_lab= arGrpSym.GetIrLab(i_rep);
//         In irep_no=nrep[i_rep];
//         Mout << " (" << sym_lab << ") :" << irep_no << endl;
//      }
//   }
//   // For each Irreps present construct the projection operator
//   // for the (11) element
//   MidasMatrix b_mat(n_bas,C_0);
//   k=I_0;
//   for (In i_rep=I_0; i_rep<n_class; i_rep++)
//   {
//      MidasMatrix repc(n_bas,C_0);
//      MidasMatrix repch(n_bas,C_0);
//      if (nrep[i_rep]>I_0)
//      {
//         Mout << " Construct projection operator for symmetry: " << arGrpSym.GetIrLab(i_rep) << endl;
//         for (In i_op=I_0; i_op<n_op; i_op++)
//         {
//            MidasMatrix rep(n_bas,C_0);
//            rep=reps[i_op];
//            Mout << " rep: " << endl;
//            Mout << rep << endl;
//
//            Nb ch_tab=arGrpSym.GetCharacter(i_rep,i_op);
//            Mout << " ch_tab: " << ch_tab << endl;
//            In ir_dim = arGrpSym.GetIrDim(i_rep);
//            Nb csum=ch_tab*(Nb(ir_dim)/Nb(n_class));
//            Mout << "csum: " << csum << endl;
//            rep.Scale(csum);
//            repc+=rep;
//         }
//         Mout << " Matrix representation: " << endl;
//         Mout << repc << endl;
//         repch=repc;
//         MidasVector eig_val(n_bas,C_0);
//         MidasMatrix eig_vec(n_bas,C_0);
//         Diag(repc,eig_vec,eig_val, "MIDAS_JACOBI", true);
//         Mout << " eig_val: " << endl;
//         Mout << eig_val << endl;
//         Mout << " eig_vec: " << endl;
//         Mout << eig_vec << endl;
//         //check eigenvalue spectrum
//         for (In i=I_0; i<(n_bas-nrep[i_rep]); i++)
//         {
//            if (eig_val[i]>C_NB_EPSILON) MIDASERROR(" Bad eigenvalue spectrum");
//         }
//         //apply to the vectors again...
//         for (In j_rep=I_0; j_rep<nrep[i_rep]; j_rep++)
//         {
//            In idx=n_bas-nrep[i_rep]+j_rep;
//            Mout << " idx: " << idx << endl;
//            MidasVector vec3(n_bas,C_0);
//            eig_vec.GetCol(vec3,idx);
//            Mout << " vec3: " << endl;
//            Mout << vec3 << endl;
//            for (In i1=I_0; i1<n_bas; i1++)
//            {
//               Nb csum=C_0;
//               for (In i2=I_0; i2<n_bas; i2++)
//                  csum+=repch[i1][i2]*vec3[i2];
//               b_mat[i1][k]=csum;
//            }
//            k++;
//         }
//      }
//   }
//   //multiply with the normal coordinate matrix to get the
//   //cleaned one..
//   Mout << " b_mat: " << endl;
//   Mout << b_mat << endl;
//   b_mat.Transpose();
//   norm_matp=b_mat*norm_mat;
//   Mout << " norm_matp: " << endl;
//   Mout << norm_matp;
//   norm_mat.Zero();
//   //mass weigth the normal coordinates back..
//   n_mode.SetNewSize(n_dim);
//   n_mode.Zero();
//   for (In i=I_0; i<n_frq; i++)
//    {
//      norm_matp.GetRow(n_mode,i);
//      for (In j=I_0;j<n_dim;j++)
//      {
//         n_mode[j]*=(nuc_mass[j]*sqrt(C_FAMU));
//      }
//      norm_matp.AssignRow(n_mode,i);
//   }
//   //check again for normalization..
//   //now reorganize based on overlap
//   MidasVector vec1(n_dim,C_0);
//   MidasVector vec2(n_dim,C_0);
//   for (In i1=I_0; i1<n_bas; i1++)
//   {
//      norm_matp.GetRow(vec1,i1);
//      Nb ovp=C_0;
//      In idx=I_0;
//      for (In j1=I_0; j1<n_bas; j1++)
//      {
//         mNormalCoord->GetRow(vec2,j1);
//         if (fabs(Dot(vec1,vec2))>ovp)
//         {
//            ovp=fabs(Dot(vec1,vec2));
//            idx=j1;
//         }
//      }
//      norm_mat.AssignRow(vec1,idx);
//   }
//   Mout << " final norm_mat: " << endl;
//   Mout << norm_mat;
//   mNormalCoord.Get()=norm_mat;
//   NormAnalysis();
//   return;
//}

/**
 * Shift a structure with a 3D vector of coordinates.
 *
 * @param aStructure   The structure to shift.
 * @param aShift       A 3d vector, defining the shift.
 **/
void ShiftStructure
   (  std::vector<Nuclei>& aStructure
   ,  const MidasVector&         aShift
   )
{
   for(int i_nuc = 0; i_nuc < int(aStructure.size()); ++i_nuc)
   {
      aStructure[i_nuc].Shift(aShift);
   }
}

/**
 * Calculate Center-of-Mass for given structure.
 *
 * Calculated by
 * \f[
 *    M x_{\aplha, cm} = \sum_{i = 1}^{N} m_{i} x_{\alpha, i} 
 *       \Rightarrow 
 *    x_{\alpha, cm} = \frac{\sum_{i=1}^{N} m_{i} x_{\alpha, i}}{ M }
 * \f]
 * with
 * \f[
 *    M = \sum_{i = 1}^{N} m_{i}
 * \f]
 *
 * @param aStructure   The structure to calculate center of mass for.
 *
 * @return   Returns center of mass displacement vector.
 **/
MidasVector FindCenterOfMass
   (  const std::vector<Nuclei>& aStructure
   )
{
   MidasVector center_of_mass_displacement{ I_3, C_0 };
   
   auto total_mass = double{0.0};
   
   // Sum up
   for(int i_nuc = 0; i_nuc < aStructure.size(); ++i_nuc)
   {
      const auto mass = aStructure[i_nuc].GetMass();

      center_of_mass_displacement[0] += mass * aStructure[i_nuc].X();
      center_of_mass_displacement[1] += mass * aStructure[i_nuc].Y();
      center_of_mass_displacement[2] += mass * aStructure[i_nuc].Z();

      total_mass += mass;
   }
   
   // Divide by total mass
   const auto inv_total_mass = 1.0 / total_mass;

   center_of_mass_displacement[0] *= inv_total_mass;
   center_of_mass_displacement[1] *= inv_total_mass;
   center_of_mass_displacement[2] *= inv_total_mass;
   
   return center_of_mass_displacement;
}

/**
 * Calculate the inertia tensor of a given structure.
 *
 * From http://www.kwon3d.com/theory/moi/iten.html:
 *
 * \f[
 *    I_{xx} = \sum_i m_i \left( y_i^2 + z_i^2 \right)
 * \f]
 * \f[
 *    I_{yy} = \sum_i m_i \left( x_i^2 + z_i^2 \right)
 * \f]
 * \f[
 *    I_{zz} = \sum_i m_i \left( x_i^2 + y_i^2 \right)
 * \f]
 * \f[
 *    I_{xy} = I_{yx} = - \sum_i m_i x_i y_i
 * \f]
 * \f[
 *    I_{xz} = I_{zx} = - \sum_i m_i x_i z_i
 * \f]
 * \f[
 *    I_{yz} = I_{zy} = - \sum_i m_i y_i z_i
 * \f]
 *
 * @param aStructure    The structure to calculate the inertia tensor for.
 *
 * @return   Return the inertia tensor as a MidasMatrix.
 **/
MidasMatrix CalculateInertiaTensor
   (  const std::vector<Nuclei>& aStructure
   )
{
   bool symmetrize = true; // Toggle symmetrization of inertia tensor
   MidasMatrix inertia_tensor{I_3, I_3, C_0};
   MidasMatrix accumulation  {I_3, I_3, C_0};
   MidasVector nuc_coord     {I_3};
   
   // Loop over nuclei and add to inertia tensor
   for(int i_nuc = 0; i_nuc < (int) aStructure.size(); ++i_nuc)
   {
      nuc_coord[0] = aStructure[i_nuc].X();
      nuc_coord[1] = aStructure[i_nuc].Y();
      nuc_coord[2] = aStructure[i_nuc].Z();
      auto mass    = aStructure[i_nuc].GetMass() * C_FAMU;
      
      auto norm2 = nuc_coord.Norm2();

      for(int i = 0; i < 3; ++i)
      {
         inertia_tensor[i][i] += mass * norm2;
         for(int j = 0; j < 3; ++j)
         {
            inertia_tensor[i][j] -= mass * nuc_coord[i] * nuc_coord[j];
         }
      }
   }

   // Symmetrize inertia tensor
   if(symmetrize)
   {
      for(int i = 0; i < 3; ++i)
      {
         for(int j = i + 1; j < 3; ++j)
         {
            inertia_tensor[j][i] = inertia_tensor[i][j];
         }
      }
   }

   return inertia_tensor;
}

/**
 *
 **/
MidasMatrix FindRotationToInertiaFrame
   (  const std::vector<Nuclei>& aStructure
   ,  int                        aIsDiagonalUlps
   )
{
   bool zero = true; /* toggle zeroing of small off diagonal elements */
   auto structure_copy = aStructure;
   
   MidasMatrix rot_matrix{I_3, I_3, C_0};
   
   auto inertia_tensor = CalculateInertiaTensor(structure_copy);
   
   // Check if inertia tensor is diagonal. 
   bool diagonal = true;
   auto largest_diagonal_element = std::max(inertia_tensor[0][0], std::max(inertia_tensor[1][1], inertia_tensor[2][2]));

   for(int i = 0; i < 3; ++i)
   {
      for(int j = i + 1; j < 3; ++j)
      {
         auto check  =  numeric::float_numeq_zero(inertia_tensor[i][j], largest_diagonal_element, aIsDiagonalUlps)
                     && numeric::float_numeq_zero(inertia_tensor[j][i], largest_diagonal_element, aIsDiagonalUlps)
                     ;

         if(check && zero)
         {
            inertia_tensor[i][j] = 0.0;
            inertia_tensor[j][i] = 0.0;
         }

         diagonal =  diagonal && check;
      }
   }

   if(diagonal)
   {
      int ordering[3];

      detail::InertiaTensorDiagonalOrdering(inertia_tensor, ordering);
      
      rot_matrix[0][ordering[0]] = 1.0;
      rot_matrix[1][ordering[1]] = 1.0;
      rot_matrix[2][ordering[2]] = 1.0;
   }
   else
   {
      auto dsyevd = SYEVD(inertia_tensor);

      NormalizeEigenvectors(dsyevd, ilapack::normalize);
      
      for (int i = 0; i < 3; i++)
      {
         if (numeric::float_neg(dsyevd._eigenvalues[i]))
         {
            MIDASERROR(" Tensor of Inertia not positive definite ");
         }
      }
      
      LoadEigenvectors(dsyevd, rot_matrix);
   }

   return rot_matrix;
}

} /* namespace molecule */
} /* namespace midas */
