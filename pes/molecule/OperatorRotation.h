#ifndef MIDAS_MOLECULE_OPERATOR_ROTATION_H_INCLUDED
#define MIDAS_MOLECULE_OPERATOR_ROTATION_H_INCLUDED

#include <iostream>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"

namespace midas
{
namespace molecule
{

// Forward declarations
struct RotationAxis;
struct ReflectionPlane;

/**
 * Create rotations for different symmetry operators.
 **/
class OperatorRotation
{
   public: // these will be private at some point
      //!@{
      //! Internally generate rotation matrices for different symmetry operations (http://www.pci.tu-bs.de/aggericke/PC4e/Kap_IV/Matrix_Symm_Op.htm).
      static MidasMatrix GenSigmaXRotation();
      static MidasMatrix GenSigmaYRotation();
      static MidasMatrix GenSigmaZRotation();
      static MidasMatrix GenCnXRotation(In aN, In aM);
      static MidasMatrix GenCnYRotation(In aN, In aM);
      static MidasMatrix GenCnZRotation(In aN, In aM);
      static MidasMatrix GenSnXRotation(In aN, In aM);
      static MidasMatrix GenSnYRotation(In aN, In aM);
      static MidasMatrix GenSnZRotation(In aN, In aM);
      //!@}

   public:
      //! Define constant for each axis in our coordinate system
      enum class Axis : int { ERROR = -2, UNINIT = -1, X = 0, Y = 1, Z = 2 };
      
      //! A map that connects the strings X,Y,Z to the axes of rotation Axis::X,Axis::Y,Axis::Z
      static std::map<std::string, Axis> AxisMap;

      //! Convert string to Axis type.
      static Axis StringToAxis(const std::string& aAxis);

      //! Convert Axis type to string.
      static std::string AxisToString(const Axis& aAxis);

      //! Get the next axis following the order (X, Y, Z), i.e. if Z is passed X is returned etc.
      static Axis NextAxis(const Axis& aAxis);
      
      //!@{
      //! Generate rotation matrices for different symmetry operations (http://www.pci.tu-bs.de/aggericke/PC4e/Kap_IV/Matrix_Symm_Op.htm).
      static MidasMatrix GenERotation();
      static MidasMatrix GenIRotation();
      static MidasMatrix GenSigmaRotation(Axis aAxis);
      static MidasMatrix GenSigmaRotation(Axis aAxis, Axis aOtherAxis, In aN = 1, In aM = 1);
      static MidasMatrix GenCnRotation(const RotationAxis& aAxis);
      static MidasMatrix GenCnRotation(const RotationAxis& aAxis, const RotationAxis& aOtherAxis);
      static MidasMatrix GenSnRotation(const RotationAxis& aAxis);
      //!@}
      //

};

//! Output operator for Axis.
inline std::ostream& operator<<(std::ostream& aOs, const OperatorRotation::Axis& aAxis)
{
   switch(aAxis)
   {
      case OperatorRotation::Axis::X:
         aOs << "X";
         break;
      case OperatorRotation::Axis::Y:
         aOs << "Y";
         break;
      case OperatorRotation::Axis::Z:
         aOs << "Z";
         break;
      default:
         aOs << "UNKNOWN AXIS";
         break;
   }
   return aOs;
}

/**
 * Implementens C_n^m (alpha) rotation axis.
 **/
struct RotationAxis
{
   //! The rotation axis
   OperatorRotation::Axis mAxis = OperatorRotation::Axis::UNINIT;
   //! The order of rotation
   In mNfold = 1;
   //! Number of applications
   In mM = 1;
   
   //! Overload for operator bool.
   operator bool()
   {
      return (mAxis != OperatorRotation::Axis::UNINIT) && (mNfold != I_1);
   }
};

//! Output operator for RotationAxis.
inline std::ostream& operator<<(std::ostream& aOs, const RotationAxis& aAxis)
{
   aOs << "Rotation axis: " << aAxis.mAxis << " " << aAxis.mNfold << " " << aAxis.mM;
   return aOs;
}

/**
 * Implements a sigma_alpha reflection plane.
 **/
struct ReflectionPlane
{
   //! The axis perpendicular to the reflection.
   OperatorRotation::Axis mAxis = OperatorRotation::Axis::UNINIT;
   
   //! Overload for operator bool.
   operator bool()
   {
      return (mAxis != OperatorRotation::Axis::UNINIT);
   }
};

//! Output operator for ReflectionPlane.
inline std::ostream& operator<<(std::ostream& aOs, const ReflectionPlane& aPlane)
{
   aOs << "Reflection plane : " << aPlane.mAxis;
   return aOs;
}

} /* namespace molecule */
} /* namespace midas */

#endif /* MIDAS_MOLECULE_OPERATOR_ROTATION_H_INCLUDED */
