#ifndef MIDASWRITER_H_INCLUDED
#define MIDASWRITER_H_INCLUDED

#include <string>
#include "MoleculeWriter.h"

namespace midas
{
namespace molecule
{

/**
 * Writer for midas type molecular files.
 **/
class MidasWriter
   :  public MoleculeWriter
{
   private:
      MidasWriter() = delete;
      MidasWriter(const MidasWriter&) = delete;
      MidasWriter& operator=(const MidasWriter&) = delete;

      bool Initialize();
      bool Finalize();

      bool WriteFrequencies(const FrequencyData&);
      bool WriteXYZ(const XYZData&);
      bool WriteVibrationalCoord(const VibrationalCoordinateData&);
      bool WriteSigma(const SigmaData&);
      bool WriteZmat(const ZmatData&);
      bool WriteHessian(const HessianData&);
      bool WriteGradientImpl(const std::string&, const GradientData&);
      bool WriteGradient(const GradientData&);

   public:
      //! Constructor
      MidasWriter(const MoleculeFileInfo& aMoleculeFileInfo);
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MIDASWRITER_H_INCLUDED */
