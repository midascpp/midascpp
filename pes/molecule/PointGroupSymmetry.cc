/*
************************************************************************
* 
* @file                PointGroupSymmetry.cc
*
* Created:             06-01-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Symmetry stuff.
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/molecule/PointGroupSymmetry.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/molecule/MoleculeUtility.h"

#include "libmda/util/to_string.h"
#include "libmda/numeric/float_eq.h"
#include "inc_gen/Warnings.h"
#include "pes/molecule/SymInfo.h"
#include "input/SymmetryThresholds.h"
#include "nuclei/Nuclei.h"
#include "lapack_interface/SYEVD.h"

#ifndef SYMMETRY_DEBUG
#define SYMMETRY_DEBUG false
#endif /* SYMMETRY_DEBUG */

namespace midas
{
namespace molecule
{

using namespace libmda::numeric;

/**
 * Debug function for printing a vector of nuclei in a pretty way.
 * This is not currently used, but kept here as it might be needed 
 * for debugging later.
 *
 * @param aStructure    The structure to print.
 **/
void PrintCoord
   (  const std::vector<Nuclei>& aStructure
   )
{
   for(int i = 0; i < aStructure.size(); ++i)
   {
      std::cout << aStructure[i] << std::endl;
   }
}


/**
 * Rotate inplace a molecular structure using a given rotation matrix.
 *
 * @param aStructure  The structure to rotate.
 * @param aRotMat     The rotation.
 **/
void RotateCoordinates
   (  std::vector<Nuclei>& aStructure
   ,  const MidasMatrix&   aRotMat
   )
{
   for(auto& nuclei : aStructure)
   {
      nuclei.Rotate(aRotMat);
   }
}

/**
 * Rotate vibrational coordinate in-place. 
 * Vibrational coordinates are vectors with entries :
 * 
 *    ( atom1_x, atom1_y, atom1_z, atom2_x, ... )
 * 
 * The rotation is thus carried out on each set of x,y,z coordinates for each atom separately.
 * 
 * @param aVibCoord   The vibrational coordinate to rotate inplace.
 * @param aRotMat     The rotation matrix.
 **/
void RotateVibrationalCoordinates
   (  MidasVector&         aVibCoord
   ,  const MidasMatrix&   aRotMat
   )
{
   auto natom = aVibCoord.Size() / I_3;
   MidasVector tmp(I_3);
   
   // loop over atoms
   for(In iatom = 0; iatom < natom; ++iatom)
   {
      // calculate index of x coordinate for atom, loop over x,y,z and do rotation.
      In atomidx = I_3 * iatom;
      for(int i = 0; i < I_3; ++i)
      {
         tmp[i] = C_0;
         for(int j = 0; j < I_3; ++j)
         {
            tmp[i] += aRotMat[i][j] * aVibCoord[atomidx + j];
         }
      }

      for(int i = 0; i < I_3; ++i)
      {
         aVibCoord[atomidx + i] = tmp[i];
      }
   }
}

/**
 * Check whether a structure is the same as another molecular structure.
 * This is done by checking that each atom in the reference structure can
 * be found in the other structure.
 * Each found atom is marked, such that the same atom cannot check equal to 
 * than one atom in the reference struct.
 *
 * @param aStructure        The reference structure to check against.
 * @param aOtherStructure   The structure to check.
 * @param aNucleiRelation   On output will hold the nuclei relations between the two structure,
 *                          i.e. nuclei 0 in aStructure is the same as
 *                          aNucleiRelation[0] in aOtherStructure.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * return   Returns true of the structures are the same.
 **/
bool IsSameMolecularStructure
   (  const std::vector<Nuclei>& aStructure
   ,  const std::vector<Nuclei>& aOtherStructure
   ,  std::vector<In>&           aNucleiRelation
   ,  const SymmetryThresholds&  arSymThrs
   )
{
   std::vector<std::pair<Nuclei, In> > structure_copy(aOtherStructure.size());
   for(In i = 0; i < aOtherStructure.size(); ++i)
   {
      structure_copy[i] = std::make_pair(aOtherStructure[i], i);
   }

   aNucleiRelation.resize(aStructure.size());
   
   // Loop over atoms in reference structure and individually check that it can be found in structure_copy.
   In counter = 0;
   for(const auto& atom : aStructure)
   {
      bool found = false;
      for(auto iter = structure_copy.begin(); iter != structure_copy.end(); ++iter)
      {
         if(iter->first.IsNumEqual(atom, arSymThrs.mThrNucleiIsNumEqualUlps))
         {
            found = true;
            aNucleiRelation[counter] = iter->second;
            structure_copy.erase(iter);
            break;
         }
      }

      // If an atom is not found, we abort and just return 'false'.
      if(!found) return false;

      ++counter;
   }
   return true;
}

/**
 * Get the character of a single cartesian coordinate as an integer.
 * This is done by floatint point division and subsequent rounding.
 * A check is made to se we do not round to much,
 * i.e. to check that the result of the floating point division is actually close to being an integer.
 * 
 * @param aCoord       Reference coordinate.
 * @param aCoordRot    Coordinate to check (usually stemming from a rotation).
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Return character as integer.
 **/
In GetCharacter
   (  Nb aCoord
   ,  Nb aCoordRot
   ,  const SymmetryThresholds& arSymThrs
   )
{
   // Calculate the character and round it.
   auto float_character         = aCoord / aCoordRot;
   auto float_character_rounded = std::round(float_character);
   
   // Check the error of the rounding (ULPS is set high to not fail when imprecise normalcoordinates are provided).
   if (!libmda::numeric::float_eq
         (  float_character
         ,  float_character_rounded
         ,  arSymThrs.mThrGetCharacterFloatEqUlps
         )
      )
   {
      MIDASERROR( "Round off error too big."
                , "  float_character        : '" + libmda::util::to_string(float_character) + "'"
                , "  float_character_rounded: '" + libmda::util::to_string(float_character_rounded) + "'"
                , "  ulps (difference)      : '" + std::to_string(libmda::numeric::float_ulps(float_character, float_character_rounded)) + "'"
                , "  ulps (threshold)       : '" + std::to_string(arSymThrs.mThrGetCharacterFloatEqUlps) + "'"
                );
   }
      
   // Cast to in and return
   return static_cast<In>(float_character_rounded);
}

/**
 * Relate two vibrational coordinates and return the character relating them.
 *
 * @param aNormCoord       The reference coordinate.
 * @param aNormCoordRot    The coordinate to characterize.
 * @param aNucleiRelation  The relative ordering relations between the two coordinates.
 * @param aCharacter       On output will hold the character relating the two coordinates.
 * @param arSymThrs        Numeric thresholds used when determining symmetries.
 * @param aICoord          Index for current vibrational coordiante being tested.
 * @param aIClass          Index for current symmetryoperation being tested.
 * @param aSymInfo         Object to get the symmetry information.
 *
 * @return    Returns true if we succesfully characterized the relation.
 **/
bool RelateVibrationalCoordinates
   (  const MidasVector& aNormCoord
   ,  const MidasVector& aNormCoordRot
   ,  const std::vector<In>& aNucleiRelation
   ,  In& aCharacter
   ,  const SymmetryThresholds& arSymThrs
   ,  const In aICoord
   ,  const In aIClass
   ,  const SymInfo& aSymInfo
   )
{
   auto natom = aNucleiRelation.size();
   bool initialized = false;
   if(natom != 0)
   {
      // Find character and check that the character is the same for all atom
      // coordinates
      for(In iatom = 0; iatom < natom; ++iatom)
      {
         auto iatom_idx = iatom * I_3;
         auto iatom_rot = aNucleiRelation[iatom];
         auto iatom_rot_idx = iatom_rot * I_3;

         for(In idx = 0; idx < I_3; ++idx)
         {
            // If both indices are zero we skip the coord, they are trivally
            // related with any character
            if (  libmda::numeric::float_numeq_zero
                  (  aNormCoord[iatom_idx + idx]
                  ,  C_1
                  ,  arSymThrs.mThrVibCoordsNumEqZeroUlps
                  )
               && libmda::numeric::float_numeq_zero
                  (  aNormCoordRot[iatom_rot_idx + idx]
                  ,  C_1
                  ,  arSymThrs.mThrVibCoordsNumEqZeroUlps
                  )
               )
            {
               continue;
            }
             
            // Get character for cartesian coordinate
            In character = GetCharacter(aNormCoord[iatom_idx + idx], aNormCoordRot[iatom_rot_idx + idx], arSymThrs);

            // If already initialized we check that we get the same result as
            // earlier, else we just. Note that initialized is currently always false.
            if (initialized)
            {
               if (character != aCharacter)
               {
                  // Only print output when the check fails
                  Mout << "Detected non-matching characters!" << std::endl;
                  Mout << "   icoord        = " << aICoord << std::endl;
                  Mout << "   isymop        = " << aIClass << std::endl;
                  Mout << "   symoplabel    = " << aSymInfo.GetOpLab(aIClass) << std::endl;
                  Mout << "   iatom         = " << iatom   << std::endl;
                  Mout << "   idx           = " << idx     << std::endl;
                  Mout << std::showpos;
                  Mout << "   actual  coord = " << aNormCoord[iatom_idx + idx]    << std::endl;
                  Mout << "   rotated coord = " << aNormCoordRot[iatom_idx + idx] << std::endl;
                  Mout << "   actual char   = " << character  << std::endl;
                  Mout << "   expect char   = " << aCharacter << std::endl;
                  Mout << std::noshowpos;
                  return false;
               }
            }
            else
            { 
               aCharacter = character;
               initialized = true;
            }
         }
      }
   }
   else
   {
      MIDASERROR("Zero atoms provided.");
   }
   
   // If we reach here the 'initialized' variable denotes whether our efforts were succesful or not.
   return initialized;
}

/**
 * Check if the molecule has an inversion center?
 * This is checked by inverting the molecular geometry and then check that the
 * the reference and inverted structures are the same.
 *
 * @param aMolecule   The molecule to check.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Returns 'true' if an inversion center was found.
 **/
bool HasInversionCenter
   (  const MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   auto rot_mat = OperatorRotation::GenIRotation();
   auto coord = aMolecule.GetCoord();
   RotateCoordinates(coord, rot_mat);
   std::vector<In> relation;
   return IsSameMolecularStructure(aMolecule.GetCoord(), coord, relation, arSymThrs);
}

/**
 * Check if the molecule has two or more unique C_3 axis'.
 * This is done by checking for C_3 axis in X, Y, and Z orientations.
 * If two of these were found, we return 'true'.
 *
 * @param aMolecule   The molecule to check.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Returns 'true' if two or more C3 axis' were found.
 **/
bool HasTwoOrMoreUniqueC3Axis
   (  const MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   
   // check C_3(x)
   auto coord_x = aMolecule.GetCoord();
   auto rot_mat_x = OperatorRotation::GenCnRotation( {OperatorRotation::Axis::X, 3, 1} );
   RotateCoordinates(coord_x, rot_mat_x);
   std::vector<In> relation;
   auto check_x = IsSameMolecularStructure(coord_x, aMolecule.GetCoord(), relation, arSymThrs);
   
   // check C_3(y)
   auto coord_y = aMolecule.GetCoord();
   auto rot_mat_y = OperatorRotation::GenCnRotation( {OperatorRotation::Axis::Y, 3, 1} );
   RotateCoordinates(coord_y, rot_mat_y);
   auto check_y = IsSameMolecularStructure(coord_y, aMolecule.GetCoord(), relation, arSymThrs);
   
   // check C_3(z)
   auto coord_z = aMolecule.GetCoord();
   auto rot_mat_z = OperatorRotation::GenCnRotation( {OperatorRotation::Axis::Z, 3, 1} );
   RotateCoordinates(coord_z, rot_mat_z);
   auto check_z = IsSameMolecularStructure(coord_z, aMolecule.GetCoord(), relation, arSymThrs);

   // return
   return   (check_x && check_y)
         || (check_y && check_z)
         || (check_z && check_x);
}

/**
 * Identify the highest order C_n axis, in either X, Y, or Z direction.
 * This is done by looping over axis' and different n, generating a rotation, 
 * applying the rotation and checking whether reference and rotated structeres are the same.
 * 
 * NB: The algorithm can find any order of C_N axis, but is as of now limited to highest N = 2,
 * as no higher order point groups are implemented for the SymInfo object.
 *
 * @param aMolecule     The molecule to check.
 * @param arSymThrs     Numeric thresholds used when determining symmetries.
 * @param max_n         The highest order rotation to check.
 *
 * @return   Return the highest order C_n axis found.
 **/
RotationAxis IdentifyHighestOrderCnAxis
   (  const MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   ,  In max_n = 3 /* highest n we check. */
   )
{
   RotationAxis highest_cn;
   
   std::vector<OperatorRotation::Axis> axis_vec
      {  OperatorRotation::Axis::X
      ,  OperatorRotation::Axis::Y
      ,  OperatorRotation::Axis::Z
      };

   std::vector<In> relation;
   
   // loop over axis
   for(const auto& axis : axis_vec)
   {
      // loop over n
      for(int i = max_n; i >= 2; --i)
      {
         // If we found a higher n for another axis we just break and go to next axis.
         if(highest_cn.mNfold > i)
         {
            break;
         }
         
         // generate rotation for current C_n and check
         auto coord   = aMolecule.GetCoord();
         auto rot_mat = OperatorRotation::GenCnRotation( {axis, i, 1} );
         RotateCoordinates(coord, rot_mat);
      
         auto check = IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs);

         if(check)
         {
            // If structures are the same, we see if the newly found axis has a higher degree than one we found earlier.
            if(highest_cn.mNfold < i)
            {
               highest_cn = RotationAxis{axis, i};
            }
            break; // we found highest n for current axis go on to next one.
         }
      }
   }

   // return highest axis
   return highest_cn;
}

/**
 * Check whether a molecule has N perpendicular C_2 axes with respect to another C_N rotation axis.
 * This is checked by generating a C_2 perpendicular to the C_N axis and then rotating the C_2 axis 
 * around a C_{2*N} axis (as this will generate N unique C_2 axis).
 *
 * @param aMolecule   The molecule to check.
 * @param aAxis       The C_N, we search for N C_2 axes perpendicular to this.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return    Returns true if N perpendicular C_2 axes are found.
 **/
bool HasNPerpendicularC2Axis
   (  const MoleculeInfo& aMolecule
   ,  const RotationAxis& aAxis
   ,  const SymmetryThresholds& arSymThrs
   )
{
   std::vector<OperatorRotation::Axis> axis_vec
      {  OperatorRotation::Axis::X
      ,  OperatorRotation::Axis::Y
      ,  OperatorRotation::Axis::Z
      };

   std::vector<In> relation;
   
   // loop over axes
   for(const auto& axis : axis_vec)
   {
      // if axis is the as the C_N axis we skip it.
      if(axis == aAxis.mAxis) continue;

      bool found = true;
      
      // loop over N
      for(int i = 0; i < aAxis.mNfold; ++i)
      {
         auto coord = aMolecule.GetCoord();
         auto rot_mat = OperatorRotation::GenCnRotation({axis, 2, 1}, {aAxis.mAxis, 2*aAxis.mNfold, i} );
         RotateCoordinates(coord, rot_mat);
         auto check = IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs);
         if(!check)
         {
            found = false;
            break;
         }
      }
      
      // if found, we do not have to check any other axis and we just return 'true'
      if(found)
      {
         return true;
      }
   }
   
   // If we reach here not axis was found, and we return 'false'
   return false;
}

/**
 * Identify a reflection plany in any axis.
 *
 * @param aMolecule   The molecule the check.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Return the reflection plane.
 **/
ReflectionPlane IdentifyReflectionPlane
   (  const MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   std::vector<OperatorRotation::Axis> axis_vec
      {  OperatorRotation::Axis::X
      ,  OperatorRotation::Axis::Y
      ,  OperatorRotation::Axis::Z
      };
   
   std::vector<In> relation;

   // loop over axis
   for(const auto& axis : axis_vec)
   {
      // check for reflection perpendicular to axis
      auto coord = aMolecule.GetCoord();
      auto rot_mat = OperatorRotation::GenSigmaRotation(axis);
      RotateCoordinates(coord, rot_mat);
      auto check = IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs);
      if(check)
      {
         return {axis};
      }
   }
   
   // if we reach here no plane was found
   return {OperatorRotation::Axis::UNINIT};
}

/**
 * Check if a molecule has a horizontal reflection relative to a C_N rotation axis,
 * i.e. a reflection plane perpendicular to the rotation axis.
 *
 * @param aMolecule   The molecule to check.
 * @param aAxis       The rotation axis.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Returns true of a horizontal reflection was found.
 **/
bool HasHorizontalReflection
   (  const MoleculeInfo& aMolecule
   ,  const RotationAxis& aAxis
   ,  const SymmetryThresholds& arSymThrs
   )
{
   // Generate rotated structure
   auto coord = aMolecule.GetCoord();
   MidasMatrix rot_mat = OperatorRotation::GenSigmaRotation(aAxis.mAxis);
   RotateCoordinates(coord, rot_mat);
   
   // Check 
   std::vector<In> relation;
   auto check = IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs);
   return check;
}

/**
 * Check if a molecule has N vertical reflection planes relative to a C_N rotation axis,
 * i.e. N reflection planes that also contains the C_N axis.
 * This is checked by generating reflection planes that contain the C_N axis and then rotating them
 * around a C_{2*N} axis (as this will generate N unique reflection planes).
 *
 * @param aMolecule   The molecule to check.
 * @param aAxis       The rotation axis.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Return 'true' if N vertical reflection planes were found.
 **/
bool HasNVerticalReflectionPlanes
   (  const MoleculeInfo& aMolecule
   ,  const RotationAxis& aAxis
   ,  const SymmetryThresholds& arSymThrs
   )
{
   std::vector<OperatorRotation::Axis> axis_vec
      {  OperatorRotation::Axis::X
      ,  OperatorRotation::Axis::Y
      ,  OperatorRotation::Axis::Z
      };

   std::vector<In> relation;
   
   // loop over axis (planes)
   for(const auto& axis : axis_vec)
   {
      // Do not check reflection planes that are perpendicular to our rotation axis.
      if(axis == aAxis.mAxis) continue;

      bool found = true;

      // Loop over N
      for(int i = 0; i < aAxis.mNfold; ++i)
      {
         auto coord = aMolecule.GetCoord();
         auto rot_mat = OperatorRotation::GenSigmaRotation(axis, aAxis.mAxis, 2*aAxis.mNfold, i);
         RotateCoordinates(coord, rot_mat);
         auto check = IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs);
         if(!check)
         {
            found = false;
            break;
         }
      }

      // if found, we do not have to check any other reflection planes and we just return 'true'
      if(found)
      {
         return true;
      }
   }
   
   // If we reach here no reflection planes were found, and we return 'false'
   return false;
}

/**
 * Identify the molecular point group.
 * Implemented after flow chart found here : 
 *  
 *    symmetry.otterbein.edu/common/images/flowchart.pdf
 *
 * Most of the branches are implemented (except for the cubic group block), 
 * but some point groups are not in the rest of the program,
 * and for these branches the point group will be set to C_1.
 *
 * @param aMolecule  The molecule to find point group for.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return   Return SymInfo object with correct pointgroup loaded.
 **/
PointGroup IdentifyPointGroup
   (  const MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   // Is the molecule linear?
   // ----------------------------------------------------------------------
   if(aMolecule.IsLinear())
   {
      // Does the molecule contain an inversion center?
      if(HasInversionCenter(aMolecule, arSymThrs))
      {
         // D_inf_h
         if constexpr (SYMMETRY_DEBUG)
         {
            std::cout << "D_inf_h" << std::endl;
         }
         return PointGroup("C1", {});
      }
      else
      {
         // C_inf_h
         if constexpr (SYMMETRY_DEBUG)
         {
            std::cout << "C_inf_h" << std::endl;
         }
         return PointGroup("C1", {});
      }
   }
   else
   {
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "Molecule is NOT linear." << std::endl;
      }
   }
   
   // Does the molecule contain two or more unique C_3 axes?
   // ----------------------------------------------------------------------
   if(HasTwoOrMoreUniqueC3Axis(aMolecule, arSymThrs))
   {
      // These are the high symmetry groups: T, T_d, T_h, O, O_h, I, I_h
      // which are not implemented yet
      // thus as of now this is C_1
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "T stuff" << std::endl;
      }
      MidasWarning("Checks for hyper symmetric groups not implemented!");
      return PointGroup("C1", {});
   }
   else
   {
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "Molecule is NOT hyper symmetric." << std::endl;
      }
   }
   
   // Does the molecule contain a proper rotation axis (C_n axis)? if so identify highest order C_n.
   // ----------------------------------------------------------------------
   if(auto cn = IdentifyHighestOrderCnAxis(aMolecule, arSymThrs))
   {
      // Are there n perpendicular C_2 axis?
      if(HasNPerpendicularC2Axis(aMolecule, cn, arSymThrs))
      {
         // We are now either D_n, D_n_h, or D_n_d
         
         // Is there a horizontal reflection plane?
         if(HasHorizontalReflection(aMolecule, cn, arSymThrs))
         {
            // D_n_H
            if constexpr (SYMMETRY_DEBUG)
            {
               std::cout << "D_" << cn.mNfold << "_h (" << cn.mAxis << ")" << std::endl;
            }
            return PointGroup("D" + std::to_string(cn.mNfold) + "H", {cn.mAxis});
         }
         else
         {
            // We are now either D_n or D_n_d
            
            MidasWarning("Check between D_n and D_n_d is not implemented!");
            MidasWarning("As D_n is a subgroup of D_n_d, I reduce symmetry to D_n.");
            
            // Does the molecule contain n dihedral reflection planes (sigma_d)?
            if(false)
            {
               // D_n_d
               if constexpr (SYMMETRY_DEBUG)
               {
                  std::cout << "D_" << cn.mNfold << "_d (" << cn.mAxis << ")" << std::endl;
               }
               return PointGroup("C1", {});
            }
            else
            {
               // D_n
               if constexpr (SYMMETRY_DEBUG)
               {
                  std::cout << "D_" << cn.mNfold << " (" << cn.mAxis << ")" << std::endl;
               }
               return PointGroup("D" + std::to_string(cn.mNfold), {cn.mAxis});
            }
         }
      }
      else
      {
         // We are now either C_n_h, C_n_v, S_2_n, or C_n
         
         // Does the molecule contain a horizontal reflection?
         if(HasHorizontalReflection(aMolecule, cn, arSymThrs))
         {
            // C_n_h
            if constexpr (SYMMETRY_DEBUG)
            {
               std::cout << "C_" << cn.mNfold << "_h (" << cn.mAxis << ")" << std::endl;
            }
            return PointGroup("C" + std::to_string(cn.mNfold) + "H", {cn.mAxis});;
         }
         
         // Does the molecule contain n-vertical reflection planes (sigma_v)?
         if(HasNVerticalReflectionPlanes(aMolecule, cn, arSymThrs))
         {
            // C_n_v
            if constexpr (SYMMETRY_DEBUG)
            {
               std::cout << "C_" << cn.mNfold << "_v (" << cn.mAxis << ")" << std::endl;
            }
            return PointGroup("C" + std::to_string(cn.mNfold) + "V", {cn.mAxis});;
         }

         // Does the molecule contain a 2n-fold improper rotation axis?
         if(false) // CHECK NOT IMPLEMENTED YET!
         {
            // S_2_n
            if constexpr (SYMMETRY_DEBUG)
            {
               std::cout << "S_2_n" << std::endl;
            }
            return PointGroup("C1", {});
         }
         else
         {
            // C_n
            if constexpr (SYMMETRY_DEBUG)
            {
               std::cout << "C_" << cn.mNfold << " (" << cn.mAxis << ")" << std::endl;
            }
            return PointGroup("C" + std::to_string(cn.mNfold), {cn.mAxis});
         }
      }
   }
   else
   {
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "Molecule does NOT have a C_n axis." << std::endl;
      }
   }
   
   // We are now either C_s, C_i, or C_1
   // ----------------------------------------------------------------------
   if(auto sigma = IdentifyReflectionPlane(aMolecule, arSymThrs))
   {
      // C_s with reflection plane sigma
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "C_s" << std::endl;
      }
      return PointGroup("CS", {sigma.mAxis});
   }
   else
   {
      if constexpr (SYMMETRY_DEBUG)
      {
         std::cout << "Molecule does NOT have reflection plane." << std::endl;
      }

      if(HasInversionCenter(aMolecule, arSymThrs))
      {
         // C_i
         if constexpr (SYMMETRY_DEBUG)
         {
            std::cout << "C_i" << std::endl;
         }
         return PointGroup("CI", {});
      }
      else
      {
         // C_1
         if constexpr (SYMMETRY_DEBUG)
         {
            std::cout << "C_1" << std::endl;
         }
         return PointGroup("C1", {});
      }
   }
}

/**
 * Analyse and find Irrep's for vibrational coordinates.
 * Will update molecule with symmetry labels.
 *
 * @param aMolecule  The molecule. On output will have updated Symmetry labels.
 * @param aSymInfo   The pointgroup information.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 **/
void CoordinateAnalysis
   (  MoleculeInfo& aMolecule
   ,  const SymInfo& aSymInfo
   ,  const SymmetryThresholds& arSymThrs
   )
{
   const auto& coord = aMolecule.NormalCoord();
   MidasVector inormcoord    (coord.Ncols());
   MidasVector inormcoord_rot(coord.Ncols());
   std::vector<std::string> symlabels(coord.Nrows());
   auto nclass = aSymInfo.Nclass();
   std::vector<In> irrep(nclass);
   bool succes = true;

   // MGH: Debug output
   if (gPesIoLevel > I_5)
   {
      Mout << "Using thresholds for CoordinateAnalysis:" << std::endl;
      Mout << "    mThrGetCharacterFloatEqUlps = " << arSymThrs.mThrGetCharacterFloatEqUlps << std::endl;
      Mout << "    mThrVibCoordsNumEqZeroUlps  = " << arSymThrs.mThrVibCoordsNumEqZeroUlps  << std::endl;
      Mout << "    mThrNucleiIsNumEqualUlps    = " << arSymThrs.mThrNucleiIsNumEqualUlps    << std::endl;
   }

   // loop over coordinates.
   for (int icoord = 0; icoord < coord.Nrows(); ++icoord)
   {
      coord.GetRow(inormcoord, icoord);

      // loop over symmetry operators and identify character for each operation to create the irrep
      for (int iclass = 0; iclass < nclass; ++iclass)
      {
         const MidasMatrix& rot_mat = aSymInfo.GetRotation(iclass);
         inormcoord_rot = inormcoord;
         RotateVibrationalCoordinates(inormcoord_rot, rot_mat);
         
         // Get nuclei relation for the symmetry operator
         const std::vector<In>& relation = aSymInfo.GetRelation(iclass);
         
         // Relate the two structures and in the process find the 'character'.
         In character;
         
         if(!RelateVibrationalCoordinates(inormcoord, inormcoord_rot, relation, character, arSymThrs, icoord, iclass, aSymInfo))
         {
            succes = false;
         }

         // This will not be correct if the check above fails. However, a hard error is throws below anyway.
         irrep[iclass] = character;
      }

      // Using the irrep we just found, get the label from SymInfo object and assign to coordinate
      symlabels[icoord] = aSymInfo.GetIrLab(irrep);
   }

   // Only throw hard error after all coordinates have been checked
   if (!succes)
   {
      MIDASERROR("Could not relate coordinates!");
   }
   
   // Update molecule with symmrtry labels.
   aMolecule.SetSymLabels(symlabels);
}

/**
 * Setup relation nuclei relations for each symmetry operation in the point group of the molecule,
 * i.e. for each symmetry operation identify the relative correspondance between atoms in the molecule.
 *
 * @param aMolecule   The molecule.
 * @param aSymInfo    The SymInfo object. On output this will be updated with nuclei relations.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 **/
void SetupNucleiRelation
   (  const MoleculeInfo& aMolecule
   ,  SymInfo& aSymInfo
   ,  const SymmetryThresholds& arSymThrs
   )
{
   auto nclass = aSymInfo.Nclass();
   for(In iclass = 0; iclass < nclass; ++iclass)
   {
      const MidasMatrix& rot_mat = aSymInfo.GetRotation(iclass);
      auto coord = aMolecule.GetCoord();
      RotateCoordinates(coord, rot_mat);
      
      // identify and save the relation
      std::vector<In> relation;
      if(!IsSameMolecularStructure(coord, aMolecule.GetCoord(), relation, arSymThrs))
      {
         MIDASERROR("Could not relate structures for class: '" + std::to_string(iclass) + "' when setting up nuclei relations.");
      }
      aSymInfo.SetRelation(iclass, relation);
   }
}

/**
 * Constructor for PointGroup.
 *
 * @param aPointGroup  Label for point group, e.g. 'C2'.
 * @param aAxis        Vector of axis' uniqeuly defining the pointgroup.
 **/
PointGroup::PointGroup
   (  const std::string& aPointGroup
   ,  const axis_vector& aAxis
   )
   :  mPointGroup(aPointGroup)
   ,  mAxis(aAxis)
{
}

/**
 * Get point group label.
 **/
const std::string& PointGroup::GetPointGroup
   (
   ) const
{
   return mPointGroup;
}

/**
 * Get point group axis'.
 **/
const PointGroup::axis_vector& PointGroup::GetAxis
   (
   ) const
{
   return mAxis;
}

/**
 * Function to do symmetry analysis. Will first find pointgroup,
 * then will call a function that analyses the coordinates and identifies
 * Irreps for each coordinate.
 *
 * @param aMolecule  The molecule to do the symmetry analysis for.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return Returns the SymInfo object.
 **/
SymInfo SymmetryAnalysis
   (  MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   SymInfo syminfo(IdentifyPointGroup(aMolecule, arSymThrs));

   SetupNucleiRelation(aMolecule, syminfo, arSymThrs);

   CoordinateAnalysis(aMolecule, syminfo, arSymThrs);

   return syminfo;
}

/**
 * Function to do symmetry analysis. 
 * This function presupposes that a point group has already been defined 
 * and symmetry analysis should only be carried out for the vibrational coordinates 
 * and not the nuclear coordinates.
 *
 * @param aMolecule  The molecule to do the symmetry analysis for.
 * @param arSymThrs  Numeric thresholds used when determining symmetries.
 *
 * @return Returns the SymInfo object.
 **/
SymInfo PseudoSymmetryAnalysis
   (  MoleculeInfo& aMolecule
   ,  const SymmetryThresholds& arSymThrs
   )
{
   SymInfo syminfo;

   for (In i = I_0; i < gModSysCalcDefs.size(); ++i)
   {
      OperatorRotation::Axis mSymmetryAxis = OperatorRotation::StringToAxis(gModSysCalcDefs[i].GetSymmetryAxis());
      std::vector<OperatorRotation::Axis> axis_vec{mSymmetryAxis};
      
      syminfo = SymInfo( PointGroup(gModSysCalcDefs[i].GetPointGroup(), axis_vec) );
   }

   SetupNucleiRelation(aMolecule, syminfo, arSymThrs);

   CoordinateAnalysis(aMolecule, syminfo, arSymThrs);

   return syminfo;
}
      
} /* namespace molecule */
} /* namespace midas */

