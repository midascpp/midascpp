#include "TurbomoleReader.h"

#include <string>

#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/Trim.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/FromString.h"
#include "pes/molecule/MoleculeData.h"

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeReaderRegistration<TurbomoleReader> registerTurbomoleReader("TURBOMOLE");

/**
 * Parse key
 *  - For exstprop where the actual key is something like '$exstprop_<method>_____<term symbol>__', just return '$exstprop'
 **/
typename TurbomoleReader::key_t TurbomoleReader::ParseKey
   (  const key_t& aKey
   )  const
{
   if (  aKey.find("$exstprop") != std::string::npos
      )
   {
      return "$exstprop";
   }
   else
   {
      return aKey;
   }
}


/**
 * Helper function to open files and do some preprocession
 *
 * @param aFile     (input): input file stream.
 * @param aKeyword  (input): give turbomole keyword for file type e.g. '$coord'
 * @param aKey      (input): line from control file with filename
 * @param aFilename (output): a place to save filename so we can make nice user output if something goes wrong in reader routines
 * @param s         (input/output): 'work string', on output last read line
 **/
void TurbomoleReader::OpenFile
   (  std::ifstream& aFile
   ,  const std::string aKeyword
   ,  const key_t& aKey
   ,  std::string& aFilename
   ,  std::string& s
   )  const
{
   // find file name 
   std::vector<std::string> first_line = midas::util::StringVectorFromString(aKey);
   std::vector<std::string> file_string = midas::util::StringVectorFromString(first_line.back(),'=');
   MidasAssert(file_string.size() == 2,"Something wrong when parsing line " + aKey);
   aFilename = file_string[1];

   // open file
   std::string openfile = midas::path::IsRelPath(aFilename) ? (gMainDir + "/" + midas::path::FileName(aFilename)) : aFilename;
   if (  midas::path::IsRelPath(aFilename)
      )
   {
      Mout  << " Relative path given for Turbomole input file: '" << aFilename << "'. Assume it is placed in main directory." << std::endl;
   }
   aFile.open(openfile);
   MidasAssert(bool(aFile), "Could not open file '" + openfile + "' (file name given: '" + aFilename + "').");
   
   // check that the file is a valid turbomole file for the requested input
   // i.e. check if the file contains a line containing the requested keyword
   bool found = false;
   auto keysize = aKeyword.size();
   while (  midas::input::GetLine(aFile, s)
         )
   {
      // Check if the line starts with the keyword and next character is whitespace
      if (  !s.compare(0, keysize, aKeyword)
         && (  s.size() == keysize
            || (  s.size() > keysize
               && s[keysize] == ' '
               )
            )
         )
      {
         found = true;
         break;
      }
   }
   MidasAssert(found, "File '" + aFilename + "' is not a proper turbomole '" + aKeyword + "' file."); // assert that file is a proper turbomole file
}

/**
 * Helper function to close files and do some post processing
 **/
void TurbomoleReader::CloseFile
   (  std::ifstream& aFile
   ,  const std::string aKeyword
   ,  const std::string& aFileName
   ,  std::string& s
   )  const
{
   // Throw warning if we are not at end of file. This may be okay (e.g. for file containing unprojected and projected Hessians).
   if (  s != "$end"
      )
   {
      MidasWarning("Didn't read to end of file '" + aFileName + "' for keyword '" + aKeyword +"'.");
   }
   aFile.close(); 
}

/***
 * Read coord file:
 *
 *    EXAMPLE:
 *       $coord
 *        0.08261278785300      0.75099483953019     -0.21533782595369  c
 *       -0.14089254951159      1.56122734317499      1.86998178323392  o
 *       ......
 *       -9.17992066702844     -3.29456609015683     -2.67751516702888  h
 *       $user-defined bonds (IAN: WHAT DOES THIS DO?? )
 *       $end
 *
 ***/
typename TurbomoleReader::return_t TurbomoleReader::ReadXYZ(const key_t& aKey, MoleculeInfo& aMolecule)
{
   XYZData xyz(XYZType::Key());
   xyz.Initialize(MoleculeDataInput());
   
   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream coord_file;
   OpenFile(coord_file, "$coord", aKey, filename, s);
   
   // read atom lines
   In n_atoms=I_0;
   while(midas::input::GetLine(coord_file,s) && NotTurbomoleKeyword(s))
   {
      Nb x,y,z;
      std::string label;
      In isotope_number = 0;
      In sub_system = 0;
      
      // 
      std::istringstream str_stream(s);
      str_stream >> x >> y >> z >> label;
        
      label = midas::input::ParseInput(label);

      xyz->AddAtom(label,x,y,z,isotope_number,sub_system);
      ++n_atoms;
   }

   xyz->NumAtom() = n_atoms;
   xyz->Unit() = "AU";
   
   while(midas::input::GetLine(coord_file,s) && NotTurbomoleKeyword(s))
   {
   }

   // close/postprocess file
   CloseFile(coord_file,"$coord", filename, s);

   // set data and return
   aMolecule.InitXYZ(xyz);
   return libmda::util::ret(aKey,false);
}


/***
 * Read frequency input:
 *
 *    EXAMPLE:
 *       $vibrational spectrum
 *       #  mode     symmetry     wave number   IR intensity    selection rules
 *       #                         cm**(-1)        km/mol         IR     RAMAN
 *            1                        0.00         0.00000        -       -
 *           ...
 *           36        a            4080.36       158.09946       YES     YES
 *       $end
 ***/
typename TurbomoleReader::return_t TurbomoleReader::ReadFrequencies(const key_t& aKey, MoleculeInfo& aMolecule)
{
   FrequencyData freqs(FrequencyType::Key());
   freqs.Initialize(MoleculeDataInput());

   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream freq_file;
   OpenFile(freq_file, "$vibrational spectrum", aKey, filename, s);
   
   // read freq file
   midas::input::GetLine(freq_file, s); // throw away: '#  mode     symmetry     wave number   IR intensity    selection rules'
   midas::input::GetLine(freq_file, s); // find unit:  '#                         cm**(-1)        km/mol         IR     RAMAN'
   if (s.find("cm**(-1)")!=std::string::npos)
   {
      freqs->Unit() = "CM-1";
   }
   else
   {
       MIDASERROR("Did not find the unit for frequencies in file " + filename);
   }
   
   // read frequencies
   In n_modes = 0;
   while(midas::input::GetLine(freq_file, s) && NotTurbomoleKeyword(s))
   {
      vector<string> freq_line = midas::util::StringVectorFromString(s);

      if (freq_line.size()==I_5) // tranlation/rotation coord (IAN: I THINK?!), we skip it *CAROLIN*
         //freqs->AddFrequency(midas::util::FromString<Nb>(freq_line[2]),"A"); // no symmetry data, we default to A
         continue; // continue while
      else if (freq_line.size()==I_6)
         freqs->AddFrequency(midas::util::FromString<Nb>(freq_line[2]),midas::input::ToUpperCase(freq_line[1]));
      else 
         MIDASERROR ("Number of columns does not fit in frequency reading while reading line: " + s + "\n in file: " +filename + ".");

      ++n_modes; // increment number of modes
   }

   freqs->NumFreq() = n_modes; // set number of modes
   
   // close/postprocess file
   CloseFile(freq_file,"$vibrational spectrum", filename, s);
   
   // set data and return
   aMolecule.InitFrequencies(freqs);
   return libmda::util::ret(aKey,false);
}

/**
 * Read vibrational coordinates. 
 *
 * Example input (for formaldehyde, 4 atoms, 12 modes incl. trans./rot.):
 * $vibrational normal modes
 * 1  1  -0.2382081747  -0.0787808470   0.0304645003   0.1156503941   0.1218488024
 * 1  2   0.2598379882   0.0000000000  -0.1492747455   0.0000000000   0.0000000000
 * 1  3   0.0000000000   0.0987581786
 * 2  1   0.0000000000   0.2211559255   0.4741392728   0.0116634647  -0.0720495993
 * 2  2   0.0487888106   0.1739177823   0.0000000000   0.0000000000   0.0000000000
 * 2  3   0.0000000000   0.0000000000
 * ...
 * 12  1   0.5741894327   0.0000000000   0.0000000000   0.0000000000   0.0000000000
 * 12  2   0.0000000000   0.0000000000  -0.6433013538  -0.6049713160   0.2490441583
 * 12  3  -0.3804081354  -0.3825414362
 * $end
 *
 * NB! This is a matrix in which the modes are ordered as columns (!).
 * 1  1  {mode  1, atom 1, x}  {mode  2, atom 1, x} ...
 * 1  2  {mode  6, atom 1, x}  {mode  7, atom 1, x} ...
 * 1  3  {mode 11, atom 1, x}  {mode 12, atom 1, x}
 * ...
 * 12  1  {mode  1, atom 4, z}  {mode  2, atom 4, z} ...
 * 12  2  {mode  6, atom 4, z}  {mode  7, atom 4, z} ...
 * 12  3  {mode 11, atom 4, z}  {mode 12, atom 4, z}
 **/
typename TurbomoleReader::return_t TurbomoleReader::ReadVibrationalCoord(const key_t& aKey, MoleculeInfo& aMolecule)
{
   VibrationalCoordinateData vib_coord(VibrationalCoordinateType::Key());
   vib_coord.Initialize(MoleculeDataInput());
   vib_coord->Unit() = "AU";
   vib_coord->NeedsInvSqrtMassScaling()   = true;
   vib_coord->NeedsTrimOfTransRotCoords() = true;

   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream vibcoord_file;
   OpenFile(vibcoord_file, "$vibrational normal modes", aKey, filename, s);

   //Determine number/size of coordinates by reading first row of coord matrix:
   //(Assumes first two entries of line to be indices.)
   midas::input::GetLine(vibcoord_file,s);
   std::vector<std::string> line = midas::util::StringVectorFromString(s);
   In n_cols = I_0;
   while(midas::util::FromString<In>(line.at(I_0)) == I_1)
   {
      n_cols += line.size() - I_2;
      midas::input::GetLine(vibcoord_file,s);
      line = midas::util::StringVectorFromString(s);
   }

   //Construct temporary coordinate object:
   std::vector<std::vector<Nb> > temp_coords(n_cols);
   for(In j=I_0; j<temp_coords.size(); ++j)
   {
      temp_coords[j].resize(n_cols);
   }

   //Set pos. in file to beginning and re-read first line:
   vibcoord_file.seekg(I_0, ios_base::beg);
   midas::input::GetLine(vibcoord_file,s);
   MidasAssert(s=="$vibrational normal modes", "Going to first entry did not work.");
   
   // read vibrational coord   
   In i = I_0;
   while(midas::input::GetLine(vibcoord_file, s) && (s.find("$end") == std::string::npos))
   {
      line  = midas::util::StringVectorFromString(s);
      i  = midas::util::FromString<In>(line.at(I_0)) - I_1;
      In j0 = I_5*(midas::util::FromString<In>(line.at(I_1)) - I_1);
      MidasAssert(i < temp_coords.front().size(), "Row index ("+std::to_string(i+I_1)+") > n_colums in '$vibrational normal modes'.");
      for(In j=I_0; j<line.size()-I_2; ++j)
      {
         temp_coords[j0 + j][i] = midas::util::FromString<Nb>(line.at(j + I_2));
      }
   }
      MidasAssert(i == temp_coords.front().size() - I_1, "Last row index ("+std::to_string(i+I_1)+") != n_colums in '$vibrational normal modes'.");

   // close/postprocess file
   CloseFile(vibcoord_file,"$vibrational normal modes", filename, s);
   
   //Set data and return:
   for(In j=I_0; j<temp_coords.size(); ++j)
   {
      vib_coord->AddCoordinate(temp_coords[j]);
   }
   
   aMolecule.InitVibrationalCoordinates(vib_coord);
   return libmda::util::ret(aKey,false);
}

/***
 * Read hessian information
 *
 *
 ***/
typename TurbomoleReader::return_t TurbomoleReader::ReadHessian
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream hessian_file;
   OpenFile(hessian_file, "$hessian", aKey, filename, s);

   // Check if we are at a projected Hessian
   if (  s.find("(projected)") != std::string::npos
      )
   {
      if (  !this->mUnProjectedHessianRead
         )
      {
         MidasWarning("TurbomoleReader: Reading projected Hessian. If the unprojected Hessian is available, you can remove the projected Hessian from the 'control' and 'hessian' files and rerun MidasCpp.");
      }
      else
      {
         MidasWarning("TurbomoleReader: Will not read projected Hessian since unprojected Hessian is already read in.");
         return libmda::util::ret(aKey,false);
      }
   }
   // If we have read the unprojected Hessian, just stop here.
   else if  (  this->mUnProjectedHessianRead
            )
   {
      return libmda::util::ret(aKey,false);
   }
   // Else, continue and read the unprojected Hessian.
   else
   {
      this->mUnProjectedHessianRead = true;
   }

   // Initialize Hessian here because the program should only get here once.
   HessianData hessian(HessianType::Key());
   hessian.Initialize(MoleculeDataInput());

   midas::input::GetLine(hessian_file,s);
   std::vector<std::string> line = midas::util::StringVectorFromString(s);
   In num_elem=I_0;
   while (line[0]=="1")
   {
      num_elem+=line.size()-I_2;
      midas::input::GetLine(hessian_file,s);
      line = midas::util::StringVectorFromString(s);
   }

   hessian->SetNewSize(num_elem,num_elem);

   hessian_file.seekg(0,ios_base::beg);
   midas::input::GetLine(hessian_file,s);
   MidasAssert(s.find("$hessian") != std::string::npos, "Going to first entry did not work"); 

   // read hessian

   while (  midas::input::GetLine(hessian_file, s)
         && s.find("$hessian (projected)")==std::string::npos        // If new (projected) hessian block is encountered, stop!
         && s.find("$end")==std::string::npos                        // Stop at end of file.
         )
   {

      std::vector<std::string> hess_line = midas::util::StringVectorFromString(s);

      In n= midas::util::FromString<In>(hess_line.at(0))-I_1;
      In m= midas::util::FromString<In>(hess_line.at(1))-I_1;
      for(In i = 0; i< hess_line.size()-I_2; ++i)
      {
         hessian->at(n,m*5+i) = midas::util::FromString<Nb>(hess_line.at(i+I_2));
      }

   }


   // close/postprocess file
   CloseFile(hessian_file,"$hessian", filename, s);
   
   // set data and return
   aMolecule.InitHessian(hessian);
   return libmda::util::ret(aKey,false);
}

/***
 * Read gradient information
 ***/
typename TurbomoleReader::return_t TurbomoleReader::ReadGradient
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   GradientData gradient(GradientType::Key());
   gradient.Initialize(MoleculeDataInput());

   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream gradient_file;
   OpenFile(gradient_file, "$grad", aKey, filename, s);

   // 1) Loop through file using getline. Call ifstream::tellg() to get position of each "cycle = " pattern and store the last.
   size_t last_cycle_pos = 0;
   while (  std::getline(gradient_file, s)
         )
   {
      if (  s.find("cycle =") != std::string::npos
         )
      {
         last_cycle_pos = gradient_file.tellg();
      }
   }
   // Output warning if no "cycle =" is found
   if (  last_cycle_pos == 0
      )
   {
      MidasWarning("No 'cycle =' lines found in Turbomole gradient file. Please check the file and that it is read in correctly!");
   }

   // Clear error flags (EOF) to allow reading
   gradient_file.clear();

   // 2) Use ifstream::seekg() to move to position of last "cycle = "
   gradient_file.seekg(last_cycle_pos);

   // 3) Skip geometry lines based on number of elements in StringVectorFromString and read gradient lines.
   while (  midas::input::GetLine(gradient_file, s)
         && s.find("$end") == std::string::npos
         )
   {
      // Switch scientific notation from D to E
      std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) -> unsigned char { return (c == 'D') ? 'E' : c; });

      auto svec = midas::util::StringVectorFromString(s);

      // Only consider lines with 3 columns (4 columns is xyz coordinates because the atom label is also there)
      if (  svec.size() == 3
         )
      {
         auto cur_size = gradient->Size();
         gradient->SetNewSize(cur_size + I_3, true);  // Set new size and preserve old data. Niels: This is not efficient due to many reallocations.
         gradient->at(cur_size)   = midas::util::FromString<Nb>(svec[0]);
         gradient->at(cur_size+1) = midas::util::FromString<Nb>(svec[1]);
         gradient->at(cur_size+2) = midas::util::FromString<Nb>(svec[2]);
      }
   }

   // Initialize gradient in MoleculeInfo
   aMolecule.InitGradient(gradient);

   // Return
   return libmda::util::ret(aKey, false);
}

/***
 * Read excited-state gradient information
 ***/
typename TurbomoleReader::return_t TurbomoleReader::ReadExStGradient
   (  const key_t& aKey
   ,  MoleculeInfo& aMolecule
   )
{
   GradientData gradient(GradientType::Key());
   gradient.Initialize(MoleculeDataInput());

   // open/preprocess file
   std::string filename;
   std::string s;
   std::ifstream gradient_file;
   OpenFile(gradient_file, "gradient:", aKey, filename, s);

   // 0) Get number of atoms from line starting with 'gradient:' and resize gradient
   auto natoms = midas::util::FromString<In>(midas::util::StringVectorFromString(s).back());
   gradient->SetNewSize(natoms*3);

   // 1) gradient_file should start after reading the "gradient:" line. I.e. simply read lines until $end (remember to skip atom types).
   size_t count = 0;
   while (  midas::input::GetLine(gradient_file, s)
         && s.find("$end") == std::string::npos
         )
   {
      // Switch scientific notation from D to E
      std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) -> unsigned char { return (c == 'D') ? 'E' : c; });

      auto svec = midas::util::StringVectorFromString(s);

      if (  svec.size() == 4
         )
      {
         gradient->at(count++) = midas::util::FromString<Nb>(svec[0]);
         gradient->at(count++) = midas::util::FromString<Nb>(svec[1]);
         gradient->at(count++) = midas::util::FromString<Nb>(svec[2]);
      }
      else
      {
         MIDASERROR("Expected 4 columns in each excited-state gradient line in Turbomole file.");
      }
   }

   // Initialize gradient in MoleculeInfo
   aMolecule.InitGradient(gradient);

   // Return
   return libmda::util::ret(aKey, false);
}

} /* namespace molecule */
} /* namespace midas */
