/**
************************************************************************
* 
* @file                OrcaHessReader.cc
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of OrcaHessReader class used to read
*                      orca hessian molecule input files. 
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "OrcaHessReader.h"

#include <string>

#include "pes/molecule/MoleculeData.h"
#include "pes/molecule/MoleculeInfo.h"
#include "input/Input.h"
#include "input/Trim.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/FromString.h"
#include "inc_gen/Warnings.h"


namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeReaderRegistration<OrcaHessReader> registerOrcaHessReader("ORCAHESS");

/***
 * read xyz input
 ***/
typename OrcaHessReader::return_t OrcaHessReader::ReadXYZ(const key_t& aKey, MoleculeInfo& aMolecule)
{
   XYZData xyz(XYZType::Key());
   
   xyz.Initialize(MoleculeDataInput());
   xyz->Unit() = "AU"; // always atomic units (I think)
   xyz->Comment() = "ORCA NO COMMENT";
   
   // read first line with number of atoms
   std::string s;
   std::getline(mMoleculeFile.FileStream(),s);
   std::vector<std::string> first_line = midas::util::StringVectorFromString(s);
   if(first_line.size() != 1) MIDASERROR("$atoms: first line wrong " + s);
   
   xyz->NumAtom() = midas::util::FromString<In>(first_line[0]);
   
   // read atom lines
   for(In i = 0; i < xyz->NumAtom(); ++i)
   {
      Nb x, y, z, mass;
      std::string label;
      In isotope_number = 0, sub_system = 0;

      std::getline(mMoleculeFile.FileStream(),s);
      std::istringstream str_stream(s);
      str_stream >> label >> mass >> x >> y >> z;

      xyz->AddAtom(label,x,y,z,isotope_number,sub_system);
   }
   
   aMolecule.InitXYZ(xyz);
   return libmda::util::ret(s,false);
}

/***
 * read hessian input
 ***/
typename OrcaHessReader::return_t OrcaHessReader::ReadHessian(const key_t& aKey, MoleculeInfo& aMolecule)
{
   HessianData hessian(HessianType::Key());
   
   hessian.Initialize(MoleculeDataInput());

   //Declare string for holding the lines read from file.
   std::string s;

   //Read first line (containing dimension of the matrix), assign to s.
   std::getline(mMoleculeFile.FileStream(),s);

   //Assign s to size (converting from string to In (integer)).
   In size = midas::util::FromString<In>(s);

   //Assign size to the dimensions of hessian.
   hessian->SetNewSize(size,size);

   //Loop ending when 'size' columns have been read, i.e. the entire matrix.
   for( In cols_read = 0; cols_read < size; )
   {

      //Read next line, of the form (column indices):
      //             0          1          2          3          4          5
      //Then store the indices in a vector.
      std::getline(mMoleculeFile.FileStream(),s);
      std::vector<std::string> hessian_cols = midas::util::StringVectorFromString(s);
      
      //Read next 'size' lines. 
      for(In i=0; i<size; ++i)
      {
         std::getline(mMoleculeFile.FileStream(), s);
         std::vector<std::string> hessian_line = midas::util::StringVectorFromString(s);

         //Error warning if number of elements in the row doesn't correspond to
         //that of the column index line.
         //Explanation of the +1: see below.
         if(hessian_line.size() != hessian_cols.size()+1) 
         {
            MIDASERROR("$hessian: input wrong " + s);
         }

         //Assign values to elements in hessian. Line format of hessian_line:
         //    0       0.434733   0.274899   0.000000  -0.401467  -0.242464  -0.000000
         //so there's an additional column compared with the line containing
         //the column indices (assigned to hessian_cols).
         //for loop starts at j=1 because j=0 corresponds to the row index.
         //The appropriate hessian column index for the value of hessian_line[j] 
         //is hessian_cols[j-1] because of the extra column in the former. 
         for(In j=1; j<hessian_line.size(); ++j)
         {
            hessian->at(i, midas::util::FromString<In>(hessian_cols[j-1]) ) 
               = midas::util::FromString<Nb>(hessian_line[j]);
         }
      }

      //Update cols_read with the number of columns in this section of Hessian.
      cols_read += hessian_cols.size();
   } 
   
   aMolecule.InitHessian(hessian);
   return libmda::util::ret(s,false);
}

} /* namespace molecule */
} /* namespace midas */
