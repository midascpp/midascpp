/**
************************************************************************
*
* @file                SymInfo.cc
*
* Created:             23-03-2009
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Class members definitions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "pes/molecule/SymInfo.h"

// std headers
#include <iostream>
#include <fstream>
#include <cctype>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <cmath>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"
#include "pes/molecule/PointGroupSymmetry.h"

// using declarations
using std::vector;
using std::map;
using std::transform;
using std::find;
using std::string;
using std::getline;

namespace midas
{
namespace molecule
{



/**
 * Default constructor
 **/
SymInfo::SymInfo
   (
   )
   :  mGrpLabel("")
   ,  mNgroup(I_0)
   ,  mNclass(I_0)
   ,  mNopSym(I_0)
   ,  mCharTab() 
{ 
   SetPointGroup("C1");
   std::vector<OperatorRotation::Axis> axis;
   GenSymInfo(axis);
}

/**
 * Constructor from group label. NB: Only works for abelian groups:
 * C_s, C_i, C_2, D_2, C_2v, C_2h, D_2h 
 *
 * @param aPointGroup   The pointgroup to create symmetry info for.
 **/
SymInfo::SymInfo
   (  const PointGroup& aPointGroup
   )
{
   SetPointGroup(aPointGroup.GetPointGroup());
   GenSymInfo(aPointGroup.GetAxis());
}

/**
 * Destructor
 **/
SymInfo::~SymInfo
   (
   )
{
}

/**
 * Driver
 **/
void SymInfo::GenSymInfo
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   enum input {C1,CS, CI, C2, D2, C2V, C2H, D2H};
   std::map<std::string, In> sym_lab;
   sym_lab["C1"]          = C1;
   sym_lab["CS"]          = CS;
   sym_lab["CI"]          = CI;
   sym_lab["C2"]          = C2;
   sym_lab["D2"]          = D2;
   sym_lab["C2V"]         = C2V;
   sym_lab["C2H"]         = C2H;
   sym_lab["D2H"]         = D2H;

   std::string s_inp = GetPointGroup();
   while (s_inp.find(" ") != s_inp.npos)     // Delete ALL blanks
   {
      s_inp.erase(s_inp.find(" "), I_1);
   }
   In input = sym_lab[s_inp];
   
   switch(input)
   {
      case C1:
      {
         GenSymC1(aAxis);
         break;
      }
      case CS:
      {
         GenSymCS(aAxis);
         break;
      }
      case CI:
      {
         GenSymCI(aAxis);
         break;
      }
      case C2:
      {
         GenSymC2(aAxis);
         break;
      }
      case D2:
      {
         GenSymD2(aAxis);
         break;
      }
      case C2V:
      {
         GenSymC2V(aAxis);
         break;
      }
      case C2H:
      {
         GenSymC2H(aAxis);
         break;
      }
      case D2H:
      {
         GenSymD2H(aAxis);
         break;
      }
      default:
      {
         Mout << " Symmetry label " << s_inp << " invalid or not implemented yet - input flag ignored! " << std::endl;
         std::string s_err = " Invalid symmetry label : '" + s_inp + "'";
         MIDASERROR(s_err);
      }
   }
}

/**
 * Generate symmetry info for C_1 point group
 **/
void SymInfo::GenSymC1
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 0)
   {
      MIDASERROR("AXIS SIZE MUST BE 0");
   }

   // Number of groups, classes and operators
   mNgroup=I_1;                         
   mNclass=I_1;                           
   mNopSym=I_1;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
          mCharTab[i][j]=C_1;
      }
   }

   // Fill in the irrep label map
   string sym_lab="A";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));

   // Fill in the label map
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));

   // Create symmetry operator rotations
   mRotationMatrices.reserve(mNopSym);
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   
   mInverseRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   
   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
}

/**
 * Generate symmetry info for C_s point group
 **/
void SymInfo::GenSymCS
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Get main and secondary axes
   OperatorRotation::Axis axis1 = aAxis[0];
   OperatorRotation::Axis axis2 = OperatorRotation::NextAxis(axis1);
   OperatorRotation::Axis axis3 = OperatorRotation::NextAxis(axis2);

   // Number of groups, classes and operators
   mNgroup=I_2;                         
   mNclass=I_2;                           
   mNopSym=I_2;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i = I_0; i < Nclass(); i++)
   {
      for (In j = I_0; j < Nclass(); j++)
      {
          mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_1]=-C_1;

   // Fill in the irrep label map
   std::string sym_lab = "AP";
   mIrrepsLabMap.insert(make_pair(sym_lab, I_0));
   sym_lab = "APP";
   mIrrepsLabMap.insert(make_pair(sym_lab, I_1));

   // Fill in the operator label map
   std::string op_lab = "E";
   mOpSymLabMap.insert(make_pair(I_0, op_lab));
   op_lab = "sigma_h(" + OperatorRotation::AxisToString(axis2) + OperatorRotation::AxisToString(axis3) + ")";
   mOpSymLabMap.insert(make_pair(I_1, op_lab));
   
   // Create symmetry operator rotations
   mRotationMatrices.reserve(mNopSym);
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis1)); 
   
   mInverseRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis1)); 
   
   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
}

/**
 * Generate symmetry info for C_i point group
 **/
void SymInfo::GenSymCI
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 0)
   {
      MIDASERROR("AXIS SIZE MUST BE 0");
   }

   // Number of groups, classes and operators
   mNgroup=I_2;                         
   mNclass=I_2;                           
   mNopSym=I_2;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
          mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_1]=-C_1;

   // Fill in the irrep label map
   string sym_lab="Ag";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="Au";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));

   // Fill in the operator label map
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   op_lab="i";
   mOpSymLabMap.insert(make_pair(I_1,op_lab));
   
   // Create symmetry operator rotations
   mRotationMatrices.reserve(mNopSym);
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mRotationMatrices.emplace_back(OperatorRotation::GenIRotation());
   
   mInverseRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenIRotation());
   
   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
}

/**
 * Generate symmetry info for C_2 point group
 * */
void SymInfo::GenSymC2
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Number of groups, classes and operators
   mNgroup=I_2;                         
   mNclass=I_2;                           
   mNopSym=I_2;

   // Dimensions of irreps
   for (In i = I_0; i < Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
          mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_1]=-C_1;

   // Fill in the irrep label map
   string sym_lab="A";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="B";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));

   // Fill in the operator label map
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   op_lab="C2";
   mOpSymLabMap.insert(make_pair(I_1,op_lab));
   
   // Create symmetry operator rotations
   mRotationMatrices.reserve(mNopSym);
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {aAxis[0], 2, 1} ));
   
   mInverseRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {aAxis[0], -2, 1} ));
   
   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
}
/**
 * Generate symmetry info for D_2 point group
 * */
void SymInfo::GenSymD2
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Number of groups, classes and operators
   mNgroup=I_4;                         
   mNclass=I_4;                           
   mNopSym=I_4;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
         mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_2]=-C_1;
   mCharTab[I_1][I_3]=-C_1;
   mCharTab[I_2][I_1]=-C_1;
   mCharTab[I_2][I_3]=-C_1;
   mCharTab[I_3][I_1]=-C_1;
   mCharTab[I_3][I_2]=-C_1;

   // Fill in the irrep label map
   string sym_lab="A";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="B1";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));
   sym_lab="B2";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_2));
   sym_lab="B3";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_3));

   // create symmetry operator rotations
   OperatorRotation::Axis axis1 = aAxis[0];
   OperatorRotation::Axis axis2 = OperatorRotation::NextAxis(axis1);
   OperatorRotation::Axis axis3 = OperatorRotation::NextAxis(axis2);
   std::vector<OperatorRotation::Axis> Axes = {axis1, axis2, axis3};

   // Setup symmetry operators
   mRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.reserve(mNopSym);

   // Identity is special
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());

   // Rotation operations
   const In symlabmapsize = mOpSymLabMap.size();
   for (In i = I_0; i < Axes.size(); ++i)
   {
      op_lab = "C2(" + OperatorRotation::AxisToString(Axes[i]) + ")";
      mOpSymLabMap.insert(make_pair(i + symlabmapsize, op_lab));
      mRotationMatrices.emplace_back(OperatorRotation::GenCnRotation({Axes[i], 2, 1}));
      mInverseRotationMatrices.emplace_back(OperatorRotation::GenCnRotation({Axes[i], -2, 1}));
   }

   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
} 

/**
 * Generate symmetry info for C_2v point group
 * */
void SymInfo::GenSymC2V
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Number of groups, classes and operators
   mNgroup=I_4;                         
   mNclass=I_4;                           
   mNopSym=I_4;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
         mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_2]=-C_1;
   mCharTab[I_1][I_3]=-C_1;
   mCharTab[I_2][I_1]=-C_1;
   mCharTab[I_2][I_3]=-C_1;
   mCharTab[I_3][I_1]=-C_1;
   mCharTab[I_3][I_2]=-C_1;

   // Fill in the irrep label map
   string sym_lab="A1";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="A2";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));
   sym_lab="B1";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_2));
   sym_lab="B2";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_3));

   // Create symmetry operator rotations
   OperatorRotation::Axis axis1 = aAxis[0];
   OperatorRotation::Axis axis2 = OperatorRotation::NextAxis(axis1);
   OperatorRotation::Axis axis3 = OperatorRotation::NextAxis(axis2);
   std::vector<OperatorRotation::Axis> Axes = {axis1, axis2, axis3};

   // Setup symmetry operators
   mRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.reserve(mNopSym);

   // Identity
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());

   // Rotation 
   op_lab="C2(" +  OperatorRotation::AxisToString(axis1) +")";
   mOpSymLabMap.insert(make_pair(I_1,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {axis1, 2, 1} ));
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {axis1, -2, 1} ));

   // Sigma
   op_lab = "sigma_v(" + OperatorRotation::AxisToString(axis3) + OperatorRotation::AxisToString(axis1) + ")"; 
   mOpSymLabMap.insert(make_pair(I_2,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis2));
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis2));

   op_lab = "sigma_v(" + OperatorRotation::AxisToString(axis1) + OperatorRotation::AxisToString(axis2) + ")"; 
   mOpSymLabMap.insert(make_pair(I_3,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis3));
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis3));

   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
} 

/**
 * Generate symmetry info for C_2h point group
 * */
void SymInfo::GenSymC2H
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Number of groups, classes and operators
   mNgroup=I_4;                         
   mNclass=I_4;                           
   mNopSym=I_4;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
         mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_1]=-C_1;
   mCharTab[I_1][I_3]=-C_1;
   mCharTab[I_2][I_2]=-C_1;
   mCharTab[I_2][I_3]=-C_1;
   mCharTab[I_3][I_1]=-C_1;
   mCharTab[I_3][I_2]=-C_1;

   // create symmetry operator rotations
   OperatorRotation::Axis axis1 = aAxis[0];
   OperatorRotation::Axis axis2 = OperatorRotation::NextAxis(axis1);
   OperatorRotation::Axis axis3 = OperatorRotation::NextAxis(axis2);

   //fill in the label map
   string sym_lab="Ag";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="Bg";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));
   sym_lab="Au";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_2));
   sym_lab="Bu";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_3));

   // Fill in the operator label map
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   op_lab="C2(" + OperatorRotation::AxisToString(axis1) + ")";
   mOpSymLabMap.insert(make_pair(I_1,op_lab));
   op_lab="i";
   mOpSymLabMap.insert(make_pair(I_2,op_lab));
   op_lab="sigma_h(" + OperatorRotation::AxisToString(axis2) + OperatorRotation::AxisToString(axis3) + ")";
   mOpSymLabMap.insert(make_pair(I_3,op_lab));
   
   // Create symmetry operator rotations
   mRotationMatrices.reserve(mNopSym);
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {axis1, 2, 1} ));
   mRotationMatrices.emplace_back(OperatorRotation::GenIRotation());
   mRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis1));
   
   mInverseRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenCnRotation( {axis1, -2, 1} ));
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenIRotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(axis1));
   
   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
} 

/**
 * Generate symmetry info for D_2h point group
 * */
void SymInfo::GenSymD2H
   (  const std::vector<OperatorRotation::Axis>& aAxis
   )
{
   if (aAxis.size() != 1)
   {
      MIDASERROR("AXIS SIZE MUST BE 1");
   }

   // Number of groups, classes and operators
   mNgroup=I_8;
   mNclass=I_8;
   mNopSym=I_8;

   // Dimensions of irreps
   for (In i=I_0; i<Ngroup(); i++)
   {
      mIrrepsDim.push_back(I_1);
   }
   
   // Fill in the character table
   mCharTab.SetNewSize(Nclass(), Nclass());
   for (In i=I_0; i<Nclass(); i++)
   {
      for (In j=I_0; j< Nclass(); j++)
      {
         mCharTab[i][j]=C_1;
      }
   }
   mCharTab[I_1][I_2]=-C_1;
   mCharTab[I_1][I_3]=-C_1;
   mCharTab[I_1][I_6]=-C_1;
   mCharTab[I_1][I_7]=-C_1;
   mCharTab[I_2][I_1]=-C_1;
   mCharTab[I_2][I_3]=-C_1;
   mCharTab[I_2][I_5]=-C_1;
   mCharTab[I_2][I_7]=-C_1;
   mCharTab[I_3][I_1]=-C_1;
   mCharTab[I_3][I_2]=-C_1;
   mCharTab[I_3][I_5]=-C_1;
   mCharTab[I_3][I_6]=-C_1;
   mCharTab[I_4][I_4]=-C_1;
   mCharTab[I_4][I_5]=-C_1;
   mCharTab[I_4][I_6]=-C_1;
   mCharTab[I_4][I_7]=-C_1;
   mCharTab[I_5][I_2]=-C_1;
   mCharTab[I_5][I_3]=-C_1;
   mCharTab[I_5][I_4]=-C_1;
   mCharTab[I_5][I_5]=-C_1;
   mCharTab[I_6][I_1]=-C_1;
   mCharTab[I_6][I_3]=-C_1;
   mCharTab[I_6][I_4]=-C_1;
   mCharTab[I_6][I_6]=-C_1;
   mCharTab[I_7][I_1]=-C_1;
   mCharTab[I_7][I_2]=-C_1;
   mCharTab[I_7][I_4]=-C_1;
   mCharTab[I_7][I_7]=-C_1;

   // Fill in the irrep label map
   string sym_lab="Ag";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_0));
   sym_lab="B1g";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_1));
   sym_lab="B2g";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_2));
   sym_lab="B3g";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_3));
   sym_lab="Au";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_4));
   sym_lab="B1u";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_5));
   sym_lab="B2u";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_6));
   sym_lab="B3u";
   mIrrepsLabMap.insert(make_pair(sym_lab,I_7));
   
   // create symmetry operator rotations
   OperatorRotation::Axis axis1 = aAxis[0];
   OperatorRotation::Axis axis2 = OperatorRotation::NextAxis(axis1);
   OperatorRotation::Axis axis3 = OperatorRotation::NextAxis(axis2);
   std::vector<OperatorRotation::Axis> Axes = {axis1, axis2, axis3};

   // Setup symmetry operators
   mRotationMatrices.reserve(mNopSym);
   mInverseRotationMatrices.reserve(mNopSym);

   // Identity
   string op_lab="E";
   mOpSymLabMap.insert(make_pair(I_0,op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenERotation());
   
   // Rotation
   In symlabmapsize = mOpSymLabMap.size();
   for (In i = I_0; i < Axes.size(); ++i)
   {
      op_lab = "C2(" + OperatorRotation::AxisToString(Axes[i]) + ")";
      mOpSymLabMap.insert(make_pair(i + symlabmapsize, op_lab));
      mRotationMatrices.emplace_back(OperatorRotation::GenCnRotation({Axes[i], 2, 1}));
      mInverseRotationMatrices.emplace_back(OperatorRotation::GenCnRotation({Axes[i], -2, 1}));
   }

   // Inversion
   op_lab = "i";
   mOpSymLabMap.insert(make_pair(mOpSymLabMap.size(), op_lab));
   mRotationMatrices.emplace_back(OperatorRotation::GenIRotation());
   mInverseRotationMatrices.emplace_back(OperatorRotation::GenIRotation());

   // Sigma
   symlabmapsize = mOpSymLabMap.size();
   for (In i = I_0; i < Axes.size(); ++i)
   {
      OperatorRotation::Axis next_axis   = OperatorRotation::NextAxis(Axes[i]);
      OperatorRotation::Axis next_axis_2 = OperatorRotation::NextAxis(next_axis);
      op_lab =  "sigma_v(";
      op_lab += OperatorRotation::AxisToString(next_axis);
      op_lab += OperatorRotation::AxisToString(next_axis_2);
      op_lab += ")";
      mOpSymLabMap.insert(make_pair(i + symlabmapsize, op_lab));
      mRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(Axes[i]));
      mInverseRotationMatrices.emplace_back(OperatorRotation::GenSigmaRotation(Axes[i]));
   }

   mNucleiRelation.resize(mNopSym);
   mInverseNucleiRelation.resize(mNopSym);
}

/**
 *
 **/
std::string SymInfo::GetOpLab(In aIop) const 
{
   map<In,string>::const_iterator it = mOpSymLabMap.find(aIop);
   if (it == mOpSymLabMap.end())
   {
      MIDASERROR("Label number "+std::to_string(aIop)+" not found in symmetry labels");
   }
   return it->second;
}

/**
 * Return the symmetry number
 * */
In SymInfo::GetIrrepNr(string aLab) const
{
   map<string,In>::const_iterator Msi=mIrrepsLabMap.find(aLab);
   if (Msi==mIrrepsLabMap.end())
   {
      MIDASERROR("Symmetry label "+aLab+" not found");
   }
   return Msi->second;
}

/**
* Get an irrep label
**/
string SymInfo::GetIrLab(In aIrep) const
{
   string s="";
   map<string,In>::const_iterator Msi;
   for (Msi=mIrrepsLabMap.begin(); Msi!=mIrrepsLabMap.end(); Msi++)
   {
      if (Msi->second == aIrep)
      {
         s=Msi->first;
         return s;
      }
   }
   return s;
}

/**
 *
 **/
std::string SymInfo::GetIrLab
   (  const std::vector<In>& aIrep
   )  const
{
   // Loop over irreps
   In theirrep = -1;
   for (In irrep = 0; irrep < mCharTab.Nrows(); ++irrep)
   {
      bool found = true;
      for (int ioper = 0; ioper < mCharTab.Ncols(); ++ioper)
      {
         if (mCharTab[irrep][ioper] != aIrep[ioper])
         {
            found = false;
            break;
         }
      }
      if (found)
      {
         theirrep = irrep;
         break;
      }
   }
      
   if (theirrep == -1)
   {
      MIDASERROR("Did not find irrep");
   }
   
   return this->GetIrLab(theirrep);
}

/**
 *
 **/
void SymInfo::SetRelation
   (  In aIop
   ,  const std::vector<In>& arRelation
   )
{
   if (aIop >= mNopSym)
   {
      MIDASERROR("aIop to large");
   }
   mNucleiRelation[aIop] = arRelation;

   std::vector<In> inverse_relation(arRelation.size());

   for (In i = 0; i < arRelation.size(); ++i)
   {
      inverse_relation[arRelation[i]] = i;
   }

   mInverseNucleiRelation[aIop] = inverse_relation;
}

/**
 * Output group info
 *
 * @param arOut
 * @param arGrp
 **/
ostream& operator<<
   (  ostream& arOut
   ,  const SymInfo& arGrp
   )
{
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   arOut.precision(18);
   arOut.setf(ios::showpoint);
   arOut << " Point group symmetry : " << arGrp.mGrpLabel << std::endl;
   arOut << " Number of operations : " << arGrp.mNopSym << std::endl;
   arOut << " Number of classes    : " << arGrp.mNclass << std::endl;
   arOut << " Character Table        " << std::endl << std::endl;
   arOut << "\t" << "\t";
   
   std::map<In, std::string> opsym_map = arGrp.GetOpSymLabMap();
   for (std::map<In, std::string>::iterator Msi = opsym_map.begin(); Msi != opsym_map.end(); Msi++)
   {
      arOut << Msi->second << "\t";
   }
   arOut << std::endl;

   std::map<std::string,In> irsym_map = arGrp.GetIrrepsLabMap();
   for (std::map<std::string, In>::iterator Msi = irsym_map.begin(); Msi != irsym_map.end(); Msi++) 
   {
      arOut << "\t" << Msi->first << "\t";
      In j = Msi->second;
      for (In i = I_0; i < arGrp.mNclass; i++)
      {
         arOut << In(arGrp.mCharTab[j][i]) << "\t";
      }
      arOut << std::endl;
   }
   return arOut;
}

} /* namespace molecule */
} /* namespace midas */ 
