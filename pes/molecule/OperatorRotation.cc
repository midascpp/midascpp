#include "pes/molecule/OperatorRotation.h"

#include <map>

#include "inc_gen/Const.h"
#include "mmv/MidasMatrix.h"

namespace midas
{
namespace molecule
{

/**
 * A map that connects the strings X,Y,Z to the axes of rotation Axis::X,Axis::Y,Axis::Z
 **/
std::map<std::string, OperatorRotation::Axis> OperatorRotation::AxisMap 
   =  { { "X", Axis::X }
      , { "Y", Axis::Y }
      , { "Z", Axis::Z }
      };

/**
 * A function that takes a string and finds the corresponding axis for rotation
 *
 * @return Returns the axis of rotation
 **/
OperatorRotation::Axis OperatorRotation::StringToAxis(const std::string& aAxis)
{
   auto axis = AxisMap.find(aAxis);
   if(axis != AxisMap.end())
   {
      return axis->second;
   }
   
   MIDASERROR("AXIS : " + aAxis + " UNKNOWN.");

   return Axis::UNINIT;
}

/**
 * A function that takes an Axis and finds the corresponding string label
 * 
 * @param aAxis   Axis
 * @return        String label of the axis
 **/
std::string OperatorRotation::AxisToString(const Axis& aAxis)
{
   switch(aAxis)
   {
      case OperatorRotation::Axis::X:
         return "X";
      case OperatorRotation::Axis::Y:
         return "Y";
      case OperatorRotation::Axis::Z:
         return "Z";
      default:
         MIDASERROR("Unkown axis!");
         return "UNKNOWN AXIS";
   }
}

/**
 * Get the next axis following the permutation (X, Y, Z), i.e. if Z is passed X is returned etc.
 * 
 * @param aAxis  Axis input.
 *
 * @return   Returns next permutation.
 **/
OperatorRotation::Axis OperatorRotation::NextAxis
   (  const Axis& aAxis
   )
{
   Axis next_axis = Axis::UNINIT;
   if(aAxis == Axis::X)
   {
      next_axis = Axis::Y;
   }
   else if(aAxis == Axis::Y)
   {
      next_axis = Axis::Z;
   }
   else if(aAxis == Axis::Z)
   {
      next_axis = Axis::X;
   }
   else
   {
      next_axis = Axis::ERROR;
   }
   return next_axis;
}

/**
 * Generate rotation for the Identity operation.
 *
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenERotation
   (
   )
{
   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  C_1; mat[0][1] =  C_0; mat[0][2] =  C_0;
   mat[1][0] =  C_0; mat[1][1] =  C_1; mat[1][2] =  C_0;
   mat[2][0] =  C_0; mat[2][1] =  C_0; mat[2][2] =  C_1;
   return mat;
   
}

/**
 * Generate rotation for the Sigma(X) operation (reflection on x-axis).
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSigmaXRotation
   (
   )
{
   MidasMatrix mat(I_3,I_3);
   mat[0][0] = -C_1; mat[0][1] =  C_0; mat[0][2] =  C_0;
   mat[1][0] =  C_0; mat[1][1] =  C_1; mat[1][2] =  C_0;
   mat[2][0] =  C_0; mat[2][1] =  C_0; mat[2][2] =  C_1;
   return mat;
}

/**
 * Generate rotation for the Sigma(Y) operation (reflection on y-axis).
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSigmaYRotation
   (
   )
{
   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  C_1; mat[0][1] =  C_0; mat[0][2] =  C_0;
   mat[1][0] =  C_0; mat[1][1] = -C_1; mat[1][2] =  C_0;
   mat[2][0] =  C_0; mat[2][1] =  C_0; mat[2][2] =  C_1;
   return mat;
}

/**
 * Generate rotation for the Sigma(Z) operation (reflection on z-axis).
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSigmaZRotation
   (
   )
{
   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  C_1; mat[0][1] =  C_0; mat[0][2] =  C_0;
   mat[1][0] =  C_0; mat[1][1] =  C_1; mat[1][2] =  C_0;
   mat[2][0] =  C_0; mat[2][1] =  C_0; mat[2][2] = -C_1;
   return mat;
}

/**
 * Generate rotation for the Inversion operation.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenIRotation
   (
   )
{
   MidasMatrix mat(I_3,I_3);
   mat[0][0] = -C_1; mat[0][1] =  C_0; mat[0][2] =  C_0;
   mat[1][0] =  C_0; mat[1][1] = -C_1; mat[1][2] =  C_0;
   mat[2][0] =  C_0; mat[2][1] =  C_0; mat[2][2] = -C_1;
   return mat;
}

/**
 * Generate rotation for C_n^m(X) operation (m n-fold rotations around x-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenCnXRotation
   (  In aN
   ,  In aM
   )
{
   if(aM > std::abs(aN))
   {
      MIDASERROR("m = " + std::to_string(aM) + " must be smaller than n = " + std::to_string(aN) + ".");
   }

   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  C_1; mat[0][1] =                        C_0; mat[0][2] =                        C_0;
   mat[1][0] =  C_0; mat[1][1] =  std::cos(C_2 * C_PI / aN); mat[1][2] =  std::sin(C_2 * C_PI / aN);
   mat[2][0] =  C_0; mat[2][1] = -std::sin(C_2 * C_PI / aN); mat[2][2] =  std::cos(C_2 * C_PI / aN);

   for(int i = 1; i < aM; ++i)
   {
      mat *= mat;
   }

   return mat;
}

/**
 * Generate rotation for C_n^m(Y) operation (m n-fold rotations around y-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenCnYRotation
   (  In aN
   ,  In aM
   )
{
   if(aM > std::abs(aN))
   {
      MIDASERROR("m = " + std::to_string(aM) + " must be smaller than n = " + std::to_string(aN) + ".");
   }

   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  std::cos(C_2 * C_PI / aN); mat[0][1] =  C_0; mat[0][2] = -std::sin(C_2 * C_PI / aN);
   mat[1][0] =                        C_0; mat[1][1] =  C_1; mat[1][2] =                        C_0;
   mat[2][0] =  std::sin(C_2 * C_PI / aN); mat[2][1] =  C_0; mat[2][2] =  std::cos(C_2 * C_PI / aN);

   for(int i = 1; i < aM; ++i)
   {
      mat *= mat;
   }

   return mat;
}

/**
 * Generate rotation for C_n^m(Z) operation (m n-fold rotations around z-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenCnZRotation
   (  In aN
   ,  In aM
   )
{
   if(aM > std::abs(aN))
   {
      MIDASERROR("m = " + std::to_string(aM) + " must be smaller than n = " + std::to_string(aN) + ".");
   }

   MidasMatrix mat(I_3,I_3);
   mat[0][0] =  std::cos(C_2 * C_PI / aN); mat[0][1] =  std::sin(C_2 * C_PI / aN); mat[0][2] =  C_0;
   mat[1][0] = -std::sin(C_2 * C_PI / aN); mat[1][1] =  std::cos(C_2 * C_PI / aN); mat[1][2] =  C_0;
   mat[2][0] =                        C_0; mat[2][1] =                        C_0; mat[2][2] =  C_1;

   for(int i = 1; i < aM; ++i)
   {
      mat *= mat;
   }

   return mat;
}

/**
 * Generate rotation for S_n^m(X) operation 
 * (m n-fold rotations around x-axis followed by reflection in x-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSnXRotation
   (  In aN
   ,  In aM
   )
{
   // first get rotation and reflection rotations
   auto cnmat = GenCnXRotation(aN, aM);
   auto sigmamat = GenSigmaXRotation();
   
   // then multiply these together to get full rotation
   MidasMatrix mat(I_3,I_3);
   mat = sigmamat * cnmat;
   return mat;
}

/**
 * Generate rotation for S_n^m(Y) operation 
 * (m n-fold rotations around y-axis followed by reflection in y-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSnYRotation
   (  In aN
   ,  In aM
   )
{
   // first get rotation and reflection rotations
   auto cnmat = GenCnYRotation(aN, aM);
   auto sigmamat = GenSigmaYRotation();
   
   // then multiply these together to get full rotation
   MidasMatrix mat(I_3,I_3);
   mat = sigmamat * cnmat;
   return mat;
}

/**
 * Generate rotation for S_n^m(Z) operation 
 * (m n-fold rotations around z-axis followed by reflection in z-axis).
 *
 * @param aN  The n-fold rotation.
 * @param aM  The number of applications.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSnZRotation
   (  In aN
   ,  In aM
   )
{
   // first get rotation and reflection rotations
   auto cnmat = GenCnZRotation(aN, aM);
   auto sigmamat = GenSigmaZRotation();
   
   // then multiply these together to get full rotation
   MidasMatrix mat(I_3,I_3);
   mat = sigmamat * cnmat;
   return mat;
}

/**
 *
 **/
MidasMatrix OperatorRotation::GenSigmaRotation(Axis aAxis)
{
   switch(aAxis)
   {
      case Axis::X:
      {
         return GenSigmaXRotation();
      }
      case Axis::Y:
      {
         return GenSigmaYRotation();
      }
      case Axis::Z:
      {
         return GenSigmaZRotation();
      }
      default:
      {
         MIDASERROR("");
      }
   }
   
   // never reaches here
   return MidasMatrix{};
}

/**
 *
 **/
MidasMatrix OperatorRotation::GenSigmaRotation
   (  Axis aAxis
   ,  Axis aOtherAxis
   ,  In aN
   ,  In aM
   )
{
   auto counter_cn_rot_mat = GenCnRotation( {aOtherAxis, -aN, aM} );
   auto cn_rot_mat         = GenCnRotation( {aOtherAxis,  aN, aM} );
   auto sigma_rot_mat = GenSigmaRotation(aAxis);

   MidasMatrix rot_mat(I_3,I_3);
   rot_mat = cn_rot_mat * sigma_rot_mat * counter_cn_rot_mat;
   return rot_mat;
}

/**
 *
 **/
MidasMatrix OperatorRotation::GenCnRotation
   (  const RotationAxis& aAxis
   ,  const RotationAxis& aOtherAxis
   )
{
   auto cn_rot_mat = GenCnRotation(aOtherAxis);
   auto counter_cn_rot_mat = GenCnRotation( {aOtherAxis.mAxis, -aOtherAxis.mNfold, aOtherAxis.mM} );
   auto actual_rot_mat = GenCnRotation(aAxis);

   MidasMatrix rot_mat(I_3,I_3);
   rot_mat = cn_rot_mat * actual_rot_mat * counter_cn_rot_mat;
   return rot_mat;
}

/**
 * Generate C_n Rotation.
 * Generate rotation for C_n^m operation (m n-fold rotations around z-axis).
 *
 * @param aAxis  The axis to generate rotation for.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenCnRotation
   (  const RotationAxis& aAxis
   )
{
   switch(aAxis.mAxis)
   {
      case Axis::X:
      {
         return GenCnXRotation(aAxis.mNfold, aAxis.mM);
      }
      case Axis::Y:
      {
         return GenCnYRotation(aAxis.mNfold, aAxis.mM);
      }
      case Axis::Z:
      {
         return GenCnZRotation(aAxis.mNfold, aAxis.mM);
      }
      default:
      {
         MIDASERROR("");
      }
   }
   
   // never reaches here
   return MidasMatrix{};
}

/**
 * Generate S_n rotation.
 * Generate rotation for S_n^m(Z) operation 
 * (m n-fold rotations around z-axis followed by reflection in z-axis).
 *
 * @param aAxis  The axis to generate rotation for.
 * 
 * @return Returns rotation matrix for the operation.
 **/
MidasMatrix OperatorRotation::GenSnRotation
   (  const RotationAxis& aAxis
   )
{
   switch(aAxis.mAxis)
   {
      case Axis::X:
      {
         return GenSnXRotation(aAxis.mNfold, aAxis.mM);
      }
      case Axis::Y:
      {
         return GenSnYRotation(aAxis.mNfold, aAxis.mM);
      }
      case Axis::Z:
      {
         return GenSnZRotation(aAxis.mNfold, aAxis.mM);
      }
      default:
      {
         MIDASERROR("");
      }
   }
   
   // never reaches here
   return MidasMatrix{};
}

} /* namespace molecule */
} /* namespace midas */
