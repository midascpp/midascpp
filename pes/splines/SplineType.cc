#include "SplineType.h"

const std::map<std::string,SPLINEINTTYPE> SPLINEINTTYPE_MAP::STRING_TO_ENUM =
{
   {"NONE",SPLINEINTTYPE::NONE}
 , {"NATURAL",SPLINEINTTYPE::NATURAL}
 , {"GENERAL",SPLINEINTTYPE::GENERAL}
 , {"ERROR",SPLINEINTTYPE::ERROR}
};

const std::map<SPLINEINTTYPE,std::string> SPLINEINTTYPE_MAP::ENUM_TO_STRING = 
{ 
   {SPLINEINTTYPE::NONE,"NONE"}
 , {SPLINEINTTYPE::NATURAL,"NATURAL"}
 , {SPLINEINTTYPE::GENERAL,"GENERAL"}
 , {SPLINEINTTYPE::ERROR,"ERROR"}
};
