/**
************************************************************************
*
* @file                Spline2D.h
*
* Created:             10-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Declares class Spline1D for cubic spline interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPLINE2D_H
#define SPLINE2D_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/SplineType.h"

/**
* Declarations:
* 2D interpolation of input grid (ig) to another output grid (og):
* MidasVector& arXig1;                            // 1-index input grid (ig) 
* MidasVector& arXig2;                            // 2-index input grid (ig)
* MidasVector& arYig;                             // Values at grid points stored as vector 
* MidasVector& mYigDerivs;                        // Second derivatives vector mapping arYig
* */

class Spline2D 
{
   private:
      MidasVector mYigDerivs;                                                                  ///< stores second derivatives on the input grid (ig)
      SPLINEINTTYPE mSplineType;                                                                          ///< define the type of splines
      Nb mFirstDerivLeft;
      Nb mFirstDerivRight;
   public:
      Spline2D(SPLINEINTTYPE, const MidasVector&, const MidasVector&, const MidasVector&,
                         const Nb& =C_0, const Nb& =C_0);
      SPLINEINTTYPE GetSplineType() const {return mSplineType;}                                           ///< return the type of spline function
      void GetYigDerivs(MidasVector& arYigDerivs) const;                                       ///< return the second derivatives on the input grid (ig)
      Nb GetYigDerivs(const In& address) const {return mYigDerivs[address];}                   ///< return the second derivatives on the input grid (ig)
      void Update(SPLINEINTTYPE, const MidasVector& arXig1New, const MidasVector& arXig2New, const MidasVector& arYigNew); ///< Update for temporary Spline2D types

};

void GetYog(MidasVector& arYog, const MidasVector& arXog1, const MidasVector& arXog2, const MidasVector& arXig1,
            const MidasVector& arXig2, const MidasVector& arYig, const MidasVector& arYigDerivs,
            SPLINEINTTYPE arSplineType, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight, const In& arstart=I_0, const In& arstride=I_1); ///< return the spline interpolation
                                                                                                                 ///< on the whole arXog output grid
void GetYog(Nb& arYog, const Nb& arXog1, const Nb& arXog2, const MidasVector& arXig1,
       const MidasVector& arXig2, const MidasVector& arYig, const MidasVector& arYigDerivs,
       SPLINEINTTYPE arSplineType, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight);    ///< return the spline interpolation at the output point (arXog1, arXog2)

#endif // SPLINE2D_H
