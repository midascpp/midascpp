/**
************************************************************************
*
* @file                Spline2D.cc
*
* Created:             10-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Define class 2Dspline for cubic spline inerpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"

/**
* Local Declarations
* */
typedef string::size_type Sst;

/**
 * Constructor.
 * 2D interpolation of input grid (ig) to another output grid (og):
 * @param arSplineType         
 * @param arXig1               1-index input grid (ig)
 * @param arXig2               2-index input grid (ig)
 * @param arYig                Values at grid points stored as vector
 * @param arFirstDerivLeft     
 * @param arFirstDerivRight    
 *
 * @note 
 *    This used to be a line in the doxygen comment, but I'm not sure what to
 *    make of it (-MBH, 2016-07-07):
 *
 *        MidasVector& mYigDerivs; // Second derivatives vector mapping arYig
 *
 **/
Spline2D::Spline2D
   (SPLINEINTTYPE       arSplineType
   , const MidasVector& arXig1
   , const MidasVector& arXig2
   , const MidasVector& arYig
   , const Nb&          arFirstDerivLeft
   , const Nb&          arFirstDerivRight
   )
   : mYigDerivs()
   , mFirstDerivLeft(arFirstDerivLeft)
   , mFirstDerivRight(arFirstDerivRight)
{
   if (arXig1.Size()*arXig2.Size() != arYig.Size())
   {  
      Mout << " Mismatch in the # of grid points and functional values  " << endl;
      MIDASERROR(" Stop in spline2D::spline2D due to mismatch in arXig's and arYig shapes");
   }
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, Mout);

   In nig1 = arXig1.Size();
   In nig2 = arXig2.Size();
   In n_ig_points = nig1 * nig2;
   mYigDerivs.SetNewSize(n_ig_points, false);

   // *** Now construct one-dimensional cubic splines of type mSplineType of the rows of arYig and return
   // *** the fill the second derivatives vector mYigDerivs (taken from numerical recipes in c++, splie2.cpp, pag. 131)

   In n_y_ig=nig2;
   MidasVector y_ig_tmp(n_y_ig,C_0);
   Spline1D y2d_ig_tmp(arSplineType, arXig2, y_ig_tmp, mFirstDerivLeft, mFirstDerivRight);
   mSplineType=y2d_ig_tmp.GetSplineType();
   In i_y_ig_ad;

   for (In iig1 = I_0; iig1 < nig1; iig1++) 
   {
      i_y_ig_ad= nig2*iig1;
      for (In iig2 = I_0; iig2 < nig2; iig2++)
      {
         y_ig_tmp[iig2] = arYig[i_y_ig_ad + iig2];
      }
      y2d_ig_tmp.Update(arXig2, y_ig_tmp);
      for (In iig2 = I_0; iig2 < nig2; iig2++)
      {
         mYigDerivs[i_y_ig_ad + iig2] = y2d_ig_tmp.GetYigDerivs(iig2);
      }
   }
}
/**
* update for temporary Spline2D types
**/
void Spline2D::Update(SPLINEINTTYPE arSplineType, const MidasVector& arXig1New, const MidasVector& arXig2New, const MidasVector& arYigNew)
{
   if (arXig1New.Size()*arXig2New.Size() != arYigNew.Size())
   {  
      Mout << " Mismatch in the # of grid points and functional values  " << endl;
      MIDASERROR(" Stop in spline2D::Update due to mismatch in arXig's and arYig shapes");
   }

   In nig1 = arXig1New.Size();
   In nig2 = arXig2New.Size();
   In n_ig_points = nig1 * nig2;
   mYigDerivs.SetNewSize(n_ig_points, false);

   // ***   then calculate the second derivatives array
   In n_y_ig=nig2;
   MidasVector y_ig_tmp(n_y_ig,C_0);
   Spline1D y2d_ig_tmp(arSplineType, arXig2New, y_ig_tmp, mFirstDerivLeft, mFirstDerivRight);
   In i_y_ig_ad;

   for (In iig1 = I_0; iig1 < nig1; iig1++) 
   {
      i_y_ig_ad= nig2*iig1;
      for (In iig2 = I_0; iig2 < nig2; iig2++)
      {
         y_ig_tmp[iig2] = arYigNew[i_y_ig_ad + iig2];
      }
      y2d_ig_tmp.Update(arXig2New, y_ig_tmp);
      for (In iig2 = I_0; iig2 < nig2; iig2++)
      {
         mYigDerivs[i_y_ig_ad + iig2] = y2d_ig_tmp.GetYigDerivs(iig2);
      }
   }
}
/**
* Get the second derivatives array
**/
void Spline2D::GetYigDerivs(MidasVector& arYigDerivs) const
{
   if (arYigDerivs.Size() != mYigDerivs.Size())
   {          
      Mout << " Mismatch in the shapes of arYigDerivs and mYigDerivs  " << endl;
      MIDASERROR(" Stop in spline2D::GetYigDerivs due to mismatch in arXig and arYig shapes");
   }                                 
   arYigDerivs=mYigDerivs;
}

/** 
* return the spline approx. at the point at a given output grid (og) arXog 
* MidasVector& arXog1; // 1-index output grid 
* MidasVector& arXog2; // 2-index output grid 
* MidasVector& arYog;  // Values at output grid points stored as vector 
**/
void GetYog(MidasVector& arYog, const MidasVector& arXog1, const MidasVector& arXog2, const MidasVector& arXig1,
            const MidasVector& arXig2, const MidasVector& arYig, const MidasVector& arYigDerivs, 
            SPLINEINTTYPE arSplineType, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight, const In& arstart, const In& arstride)
{
   In nig1=arXig1.Size();
   In nig2=arXig2.Size();
   In nog2=arXog2.Size();

   MidasVector y1_tmp(nig1);
   MidasVector y2_tmp(nig2);
   MidasVector y1d_tmp(nig1);
   MidasVector y2d_tmp(nig2);

   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, Mout);


   Spline1D spline1d_tmp(arSplineType, arXig1, y1_tmp, arFirstDerivLeft, arFirstDerivRight);
   In i_y_ig_ad;  // Find addresses to call directly spline without copy. 
   //In n_y_ig;
   Nb x2;
                                         
   // *** define the actual stride and start

   for (In iog2=I_0; iog2<nog2; iog2++)
   {
   // *** second grid point given now

      x2=arXog2[iog2];

      for (In iig1=I_0; iig1<nig1; iig1++)
      {
        // *** perform nig1 evaluations of the row splines 

         i_y_ig_ad= nig2*iig1;  // Find addresses to call directly spline without copy. 
         //n_y_ig=nig2;
        
         for (In iig2=I_0;iig2<nig2;iig2++)
         {
            y2_tmp[iig2]  = arYig[i_y_ig_ad+iig2];
            y2d_tmp[iig2] = arYigDerivs[i_y_ig_ad+iig2];
         }

         GetYog(y1_tmp[iig1], x2, arXig2, y2_tmp, y2d_tmp);
      }

      // *** Now a set of y-values corresponding to the arXig1 grid are known for the given x2 value in the input grid. 
      // *** Now do spline interpolation on these values 

      spline1d_tmp.Update(arXig1, y1_tmp);
      spline1d_tmp.GetYigDerivs(y1d_tmp);

      // *** For the given x2 and the splin available now splint all x1. 

      GetYog(arYog, arXog1, arXig1, y1_tmp, y1d_tmp, (iog2*arstride+arstart), (nog2*arstride));
   }
   return;
}
/**
* return the spline approx. at a given  output grid (og) point (arXog1, arXog2)
* (taken from numerical recipes in c++, splin2.cpp, pag. 131)
**/
void GetYog(Nb& arYog, const Nb& arXog1, const Nb& arXog2, const MidasVector& arXig1,
            const MidasVector& arXig2, const MidasVector& arYig, const MidasVector& arYigDerivs,
            SPLINEINTTYPE arSplineType, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight)
{
   In nig1=arXig1.Size();
   In nig2=arXig2.Size();

   MidasVector y1_tmp(nig1);
   MidasVector y2_tmp(nig2);
   MidasVector y1d_tmp(nig1);
   MidasVector y2d_tmp(nig2);
   In i_y_ig_ad;  // Find addresses to call directly spline without copy. 
   //In n_y_ig=nig2;


   for (In iig1=I_0; iig1<nig1; iig1++)
   {
      // *** perform nig1 evaluations of the row splines 

      i_y_ig_ad = nig2 * iig1;  // Find addresses to call directly spline without copy. 
      //n_y_ig=nig2;
      for (In iig2=I_0;iig2<nig2;iig2++)
      {
         y2_tmp[iig2]  = arYig[i_y_ig_ad+iig2];
         y2d_tmp[iig2] = arYigDerivs[i_y_ig_ad+iig2];
      }

      GetYog(y1_tmp[iig1], arXog2, arXig2, y2_tmp, y2d_tmp);
   }
   // *** Now a set of y-values corresponding to the arXig1 grid are known for the given x2 value in the input grid. 
   // *** Now do spline interpolation on these values 


   Spline1D spline1d_tmp(arSplineType, arXig1, y1_tmp, arFirstDerivLeft, arFirstDerivRight);
   spline1d_tmp.GetYigDerivs(y1d_tmp);

   // *** For the given x2 and the splin available now splint over x1. 

   GetYog(arYog, arXog1, arXig1, y1_tmp, y1d_tmp);
}
