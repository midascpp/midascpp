/**
************************************************************************
*
* @file                SplineND.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Define class NDspline for cubic spline inerpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/SplineND.h"

/**
* Local Declarations
* */
typedef string::size_type Sst;


/**
* Constructor:
* ND interpolation of input grid (ig) to another output grid (og):
* vector<MidasVector>& arXig; // vector of 1-index grid vectors 
* MidasVector& arYig;         // Values at grid points stored as vector 
* MidasVector mYigDerivs; // contains the derivatives of the last dimension 
* */
SplineND::SplineND(SPLINEINTTYPE arSplineType, const vector<MidasVector>& arXig, const MidasVector& arYig,
                   const In& level, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight): mYigDerivs(),
                   mFirstDerivLeft(arFirstDerivLeft), mFirstDerivRight(arFirstDerivRight)
{
   // ***   then calculate the second derivatives array over the last dimension
   In n_dim=arXig.size()-level+I_1;
   In n_ig_points=I_1;
   for (In i_dim=I_0; i_dim<n_dim; i_dim++) 
   { 
      n_ig_points *=arXig[i_dim].Size();
   }
   if (n_ig_points != arYig.Size())
   {
      MIDASERROR(" Mismatch in the # of grid points and functional values \n Stop in splineND::splineND due to mismatch in arXig's and arYig shapes");
   }

   In n_ig_points_less_last = I_1;
   for (In i_dim=I_0; i_dim<n_dim-I_1; i_dim++) 
   {
      n_ig_points_less_last *=arXig[i_dim].Size();
   }

   In n_last=arXig[n_dim-I_1].Size();
   mYigDerivs.SetNewSize(n_ig_points);
   In n_y_ig=n_last;
   MidasVector y_ig_tmp(n_y_ig);
   Spline1D spline_tmp(arSplineType, arXig[n_dim-I_1], y_ig_tmp, mFirstDerivLeft, mFirstDerivRight);
   mSplineType = spline_tmp.GetSplineType();

   In i_y_ig_ad;

   // *** now runs over all the indexes but the last one. At the end array asYigDerivs will map second derivatives onto the
   // *** input grid (ig) of points

   for (In i=I_0; i<n_ig_points_less_last; i++)
   {
      i_y_ig_ad= n_last * i;
 
      for (In i_last = I_0; i_last < n_last; i_last++)
      {
         y_ig_tmp[i_last] = arYig[i_y_ig_ad + i_last];
      }
      spline_tmp.Update(arXig[n_dim-I_1], y_ig_tmp);
      for (In i_last = I_0; i_last < n_last; i_last++)
      {
         mYigDerivs[i_y_ig_ad + i_last] = spline_tmp.GetYigDerivs(i_last);
      }
   }

   // ***  Now arYigDerivs contains the 2-derivs for spline inter over the last index 
}

/**
* update temporary SplineND object
**/ 
void SplineND::Update(SPLINEINTTYPE arSplineType, const vector<MidasVector>& arXig, const MidasVector& arYig, const In& level)
{

   In n_dim=arXig.size()-level+I_1;
   In n_ig_points=I_1;
   for (In i_dim=I_0; i_dim<n_dim; i_dim++) 
   { 
      n_ig_points *=arXig[i_dim].Size();
   }

   if (n_ig_points != arYig.Size())
   {
      MIDASERROR(" Mismatch in the # of grid points and functional values  \n Stop in splineND::Update due to mismatch in arXig's and arYig shapes");
   }

   In n_ig_points_less_last = I_1;
   for (In i_dim=I_0; i_dim<n_dim-I_1; i_dim++) 
   {
      n_ig_points_less_last *=arXig[i_dim].Size();
   }
   In n_last=arXig[n_dim-I_1].Size();

   In n_y_ig=n_last;
   MidasVector y_ig_tmp(n_y_ig);
   Spline1D spline_tmp(arSplineType, arXig[n_dim-I_1], y_ig_tmp, mFirstDerivLeft, mFirstDerivRight);
   mSplineType = spline_tmp.GetSplineType();

   In i_y_ig_ad;

   // *** now runs over all indexes but the last. At the end array arYigDerivs will map second derivatives onto the
   // *** input grid (ig) of points

   for (In i=I_0; i<n_ig_points_less_last; i++)
   {
      i_y_ig_ad= n_last*i;
 
      for (In i_last = I_0; i_last < n_last; i_last++)
      {
         y_ig_tmp[i_last] = arYig[i_y_ig_ad + i_last];
      }
      spline_tmp.Update(arXig[n_dim-I_1], y_ig_tmp);
      for (In i_last = I_0; i_last < n_last; i_last++)
      {
         mYigDerivs[i_y_ig_ad + i_last] = spline_tmp.GetYigDerivs(i_last);
      }
   }

   // ***  Now arYigDerivs contains the 2-derivs for spline inter over the last index 
}

/**
* Get the second derivatives array onto the CG
**/
void SplineND::GetYigDerivs(MidasVector& arYigDerivs) const
{
   if (arYigDerivs.Size() != mYigDerivs.Size())
   {
      MIDASERROR(" Mismatch in the shapes of arYigDerivs and mYigDerivs \n Stop in splineND::GetYigDerivs due to mismatch in arXig and arYig shapes");
   }  
   arYigDerivs=mYigDerivs;
}  
/**
* return the spline approx. at the point at a given output grid (og) arXog
* vector<MidasVector>& arXig; // vector of 1-index grid vectors
* MidasVector& arYig;         // Values at grid points stored as vector
* MidasVector mYigDerivs; // contains the derivatives of the last dimension
* MidasVector arYog;     // the filled array
**/
void GetYog(MidasVector& arYog, const vector<MidasVector>& arXog, const vector<MidasVector>& arXig, 
            const MidasVector& arYig, const MidasVector& arYigDerivs, 
            const In& level, SPLINEINTTYPE arSplineType, const MidasVector& arFirstDerivLeft, 
            const MidasVector& arFirstDerivRight, const In& arstart, const In& arstride)
{
   // *** to be called with level = 1 from the main program
   In n_dim=arXig.size()-level+I_1;
   if (level==I_1)
   {
      // *** then check the first derivatives arrays are properly passed

       if (arFirstDerivLeft.Size() !=n_dim || arFirstDerivRight.Size() !=n_dim)
       {
          Mout << " wrong dimensions of arFirstDerivLeft and arFirstDerivRight in SplineND::GetYog " << endl << endl;
          Mout << " actual dimensions : " <<  arFirstDerivLeft.Size() << "  "  << arFirstDerivRight.Size() << endl;
          Mout << " required dimensions :  " << n_dim << endl;
          MIDASERROR(" wrong dimensions of arFirstDerivLeft and arFirstDerivRight in Spline3D::GetYog ");
       }
   }

   if (n_dim == I_1 && level == I_1)
   {
      GetYog(arYog, arXog[I_0], arXig[I_0], arYig, arYigDerivs); 
      return;
   }
   In n_ig_points_less_last = I_1;
   for (In i_dim=I_0; i_dim<n_dim-I_1; i_dim++)
   {
      n_ig_points_less_last *=arXig[i_dim].Size();
   }
   In n_og_last=arXog[n_dim-I_1].Size();
   In n_ig_last=arXig[n_dim-I_1].Size();

   In n_og_points_less_last = I_1;
   for (In i_dim=I_0; i_dim<n_dim-I_1; i_dim++) 
   {
      n_og_points_less_last *=arXog[i_dim].Size();
   }

   MidasVector y_nm1d_tmp(n_ig_points_less_last); 
   MidasVector y_nm1d_deriv_tmp(n_ig_points_less_last); 
   MidasVector y_nd_tmp(n_ig_last);
   MidasVector y_nd_deriv_tmp(n_ig_last);

   In new_level = level+I_1;
      Nb x_last;
   In i_y_ig_ad;
   SplineND spline_nm1d_tmp(arSplineType, arXig, y_nm1d_tmp, new_level, arFirstDerivLeft[n_dim-I_2], arFirstDerivRight[n_dim-I_2]);
   for (In i_og_last=I_0; i_og_last<n_og_last; i_og_last++)
   {
      // *** run over the output grid of points for the last dimension
      // *** nm1d = n - 1 dim 
      x_last=(arXog[n_dim-I_1])[i_og_last];  

      for (In i=I_0; i<n_ig_points_less_last; i++)
      {
         i_y_ig_ad= n_ig_last*i;

         for (In i_ig_last=I_0; i_ig_last<n_ig_last; i_ig_last++)
         {
            y_nd_tmp[i_ig_last]  = arYig[i_y_ig_ad+i_ig_last];
            y_nd_deriv_tmp[i_ig_last] = arYigDerivs[i_y_ig_ad+i_ig_last];
         }

         GetYog(y_nm1d_tmp[i], x_last, arXig[n_dim-I_1], y_nd_tmp, y_nd_deriv_tmp);
      }

      // *** Now a set of y-values corresponding to the arXig1*arXig2*...*arXig(ndim-1) grid are known for the given x_last value in the input grid.
      // *** Now do a n-1 spline interpolation on these values

      spline_nm1d_tmp.Update(arSplineType, arXig, y_nm1d_tmp, new_level);
      spline_nm1d_tmp.GetYigDerivs(y_nm1d_deriv_tmp);

      // *** For the given x_last and the splin available now splint over all x1*x2* ...x_last-1  points. 

      if (n_dim > I_2)
      {
         GetYog(arYog, arXog, arXig, y_nm1d_tmp, y_nm1d_deriv_tmp, new_level,  
                   arSplineType,  arFirstDerivLeft, arFirstDerivRight, i_og_last*arstride + arstart, n_og_last*arstride);
      }  
      else
      { 
         for (In i=I_0; i < arXig[I_0].Size(); i++)
         {
            GetYog(arYog, arXog[I_0], arXig[I_0], y_nm1d_tmp, y_nm1d_deriv_tmp, i_og_last*arstride + arstart, n_og_last*arstride); 
         }
      }
   }
}
