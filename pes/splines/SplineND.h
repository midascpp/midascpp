/**
************************************************************************
*
* @file                SplineND.h
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Declares class SplineND for cubic spline interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPLINEND_H
#define SPLINEND_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/SplineType.h"

// using declarations
using std::vector;

/**
* Declarations:
* general n-dimensional interpolation of input grid (ig) to another output grid (og):
* vector<MidasVector>& arXig                     // vector of the monodimensional grids. 
* MidasVector& arYig                             // Y values on the input grid of points, i.e. function to be interpolated
* MidasVector& mYigDerivs;                       // Second derivatives vector mapping arYig, over the last dimension
* */
class SplineND 
{
   private:
      MidasVector mYigDerivs;                    ///< stores second derivatives on the input grid (ig)
      SPLINEINTTYPE mSplineType;                            ///< define the type of splines
      Nb mFirstDerivLeft;
      Nb mFirstDerivRight;
   public:
      SplineND(SPLINEINTTYPE arSplineType, const vector<MidasVector>& arXig, const MidasVector& arYig,
               const In& level, const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight);

      SPLINEINTTYPE GetSplineType() const {return mSplineType;}                                  ///< return the type of spline function
      Nb GetYigDerivs(const In& address) const {return mYigDerivs[address];}          ///< return the second derivatives on the input grid (ig)
      void GetYigDerivs(MidasVector& arYigDerivs) const;                              ///< return the second derivatives on the input grid (ig)
      void Update(SPLINEINTTYPE arSplineType, const vector<MidasVector>& arXig,
                  const MidasVector& arYig, const In& level);
};

void GetYog(MidasVector& arYog, const vector<MidasVector>& arXog, const vector<MidasVector>& arXig,
            const MidasVector& arYig, const MidasVector& arYigDerivs,
            const In& level, SPLINEINTTYPE arSplineType, const MidasVector& arFirstDerivLeft, const MidasVector& arFirstDerivRight, 
            const In& arstart=I_0, const In& arstride=I_1);                ///< return the spline interpolation onto the Xog output grid

#endif // SPLINEND_H
