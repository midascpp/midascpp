/**
************************************************************************
*
* @file                Spline1D.h
*
* Created:             03-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Declares class Spline1D for cubic spline interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPLINE1D_H_INCLUDED
#define SPLINE1D_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "pes/splines/SplineType.h"

/**
* Declarations:
**/
class Spline1D
{
   private:
      MidasVector mYigDerivs;                                                         ///< stores second derivatives on the input grid (ig)
      SPLINEINTTYPE mSplineType;                                                                 ///< Spline type =(0 for Natural, 1 for General) 
      const Nb mFirstDerivLeft;
      const Nb mFirstDerivRight;
   public:
      Spline1D(SPLINEINTTYPE, const MidasVector&, const MidasVector&, const Nb& =C_0, const Nb& =C_0); ///< constructor
      void Update(const MidasVector&, const MidasVector&);                            ///< refresh spline set on new grids
      SPLINEINTTYPE GetSplineType() const {return mSplineType;}                       //< return the type of spline function
      void GetYigDerivs(MidasVector& arYigDerivs) const;                              ///< return the second derivatives on the input grid (ig)
      Nb GetYigDerivs(const In& address) const {return mYigDerivs[address];}          ///< return the second derivatives on the input grid (ig)
};

void GetYog(Nb& arYog, const Nb& arXog, const MidasVector& arXig, const MidasVector& arYig, const MidasVector& arYigDerivs);
                                                                      ///< return the spline approx. at the point aX

void GetYog(MidasVector& arYog, const MidasVector& arXog, const MidasVector& arXig, const MidasVector& arYig,
   const MidasVector& arYigDerivs, const In& arstart=I_0, const In& arstride=I_1);
///< return the spline approx. at the output grid (og)

#endif /* SPLINE1D_H_INCLUDED */
