/**
************************************************************************
*
* @file                Spline3D.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Define class 2Dspline for cubic spline inerpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/Spline3D.h"

/**
* Local Declarations
* */

/**
* Constructor
* 2D interpolation of input grid (ig) to another output grid (og):
* MidasVector& arXig1;                            // 1-index input grid (ig)
* MidasVector& arXig2;                            // 2-index input grid (ig)
* MidasVector& arXig3;                            // 3-index input grid (ig)
* MidasVector& arYig;                             // Values at grid points stored as vector
* MidasVector& mYigDerivs;                       // Second derivatives vector mapping arYig
**/
Spline3D::Spline3D(SPLINEINTTYPE arSplineType, const MidasVector& arXig1, const MidasVector& arXig2, 
                   const MidasVector& arXig3, const MidasVector& arYig,
                   const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight): mYigDerivs(),
                   mFirstDerivLeft(arFirstDerivLeft), mFirstDerivRight(arFirstDerivRight)
{
   if (arXig1.Size()*arXig2.Size() *arXig3.Size() != arYig.Size())
   {  
      Mout << " Mismatch in the # of grid points and functional values  " << endl;
      MIDASERROR(" Stop in spline3D::spline3D due to mismatch in arXig's and arYig shapes");
   }

   In nig1 = arXig1.Size();
   In nig2 = arXig2.Size();
   In nig3 = arXig3.Size();
   In nig12 = nig1 * nig2;
   In n_ig_points = nig1 * nig2 * nig3;
   mYigDerivs.SetNewSize(n_ig_points);

   // *** now runs over the first two indexes. At the end array asYigDerivs will map second derivatives onto the
   // *** input grid (ig) of points

   In n_y_ig=nig3;
   MidasVector y_ig_tmp(n_y_ig,C_0);
   Spline1D spline_tmp(arSplineType, arXig3, y_ig_tmp, mFirstDerivLeft, mFirstDerivRight);
   mSplineType = spline_tmp.GetSplineType();
   In i_y_ig_ad;

   for (In iig12 = I_0; iig12 < nig12; iig12++) 
   {
      i_y_ig_ad= nig3 * iig12;
      for (In iig3 = I_0; iig3 < nig3; iig3++)
      {
         y_ig_tmp[iig3] = arYig[i_y_ig_ad + iig3];
      }
      spline_tmp.Update(arXig3, y_ig_tmp);
      for (In iig3 = I_0; iig3 < nig3; iig3++)
      {
         mYigDerivs[i_y_ig_ad + iig3] = spline_tmp.GetYigDerivs(iig3);
      }
   }
}

/**
* Get the second derivatives array
**/
void Spline3D::GetYigDerivs(MidasVector& arYigDerivs) const
{
   if (arYigDerivs.Size() != mYigDerivs.Size())
   {
      MIDASERROR(" Mismatch in the shapes of arYigDerivs and mYigDerivs \n Stop in spline2D::GetYigDerivs due to mismatch in arXig and arYig shapes");
   }
   arYigDerivs=mYigDerivs;
}

/**
 * Return the spline approx. at the point at a given output grid (og) arXog.
 *
 * @param arYog               Values at output grid points stored as vector
 * @param arXog1              1-index output grid
 * @param arXog2              2-index output grid
 * @param arXog3              3-index output grid
 * @param arXig1
 * @param arXig2
 * @param arXig3
 * @param arYig
 * @param arYigDerivs
 * @param arSplineType
 * @param arFirstDerivLeft
 * @param arFirstDerivRight
 **/
void GetYog
   ( MidasVector&       arYog
   , const MidasVector& arXog1
   , const MidasVector& arXog2
   , const MidasVector& arXog3
   , const MidasVector& arXig1
   , const MidasVector& arXig2
   , const MidasVector& arXig3
   , const MidasVector& arYig
   , const MidasVector& arYigDerivs
   , SPLINEINTTYPE      arSplineType
   , const MidasVector& arFirstDerivLeft
   , const MidasVector& arFirstDerivRight
   )
{
   In nig1=arXig1.Size();
   In nig2=arXig2.Size();
   In nig3=arXig3.Size();
   In nig12= nig1*nig2;
   In nog3=arXog3.Size();

   if (arFirstDerivLeft.Size() !=I_3 || arFirstDerivRight.Size() !=I_3)
   {
      Mout << " wrong dimensions of arFirstDerivLeft and arFirstDerivRight in Spline3D::GetYog " << endl << endl;
      Mout << " actual dimensions : " <<  arFirstDerivLeft.Size() << "  "  << arFirstDerivRight.Size() << endl;
      Mout << " required dimensions is  " << I_3 << endl;
      MIDASERROR(" wrong dimensions of arFirstDerivLeft and arFirstDerivRight in Spline3D::GetYog ");
   }

   MidasVector y_12_tmp(nig12);
   MidasVector y_3_tmp(nig3);
   MidasVector y_12_deriv_tmp(nig12);
   MidasVector y_3_deriv_tmp(nig3);
   Spline2D spline2d_tmp(arSplineType, arXig1, arXig2, y_12_tmp, arFirstDerivLeft[I_1], arFirstDerivRight[I_1]);

   In i_y_ig_ad; 

   for (In iog3=I_0; iog3<nog3; iog3++)
   {
      // *** second grid point given now
      Nb x3=arXog3[iog3];

      for (In iig12=I_0; iig12<nig12; iig12++)
      {
         // *** perform nig12 evaluations of the 'row' splines 

          i_y_ig_ad= nig3*iig12;
         
          for (In iig3=I_0;iig3<nig3;iig3++)
          {
             y_3_tmp[iig3]  = arYig[i_y_ig_ad+iig3];
             y_3_deriv_tmp[iig3] = arYigDerivs[i_y_ig_ad+iig3];
          }

          GetYog(y_12_tmp[iig12], x3, arXig3, y_3_tmp, y_3_deriv_tmp);
       }

      // *** Now a set of y-values corresponding to the arXig1*arXig2 grid are known for the given x3 value in the input grid. 
      // *** Now do a 2D spline interpolation on these values 

      spline2d_tmp.Update(arSplineType, arXig1, arXig2, y_12_tmp);
      spline2d_tmp.GetYigDerivs(y_12_deriv_tmp);

      // *** For the given x3 and the splin available now splint over all x1*x2 points. 

      GetYog(arYog, arXog1, arXog2, arXig1, arXig2, y_12_tmp, y_12_deriv_tmp,
              arSplineType, arFirstDerivLeft[I_0], arFirstDerivRight[I_0], iog3, nog3);
   }
   return;
}
