#ifndef SPLINETYPE_H_INCLUDED
#define SPLINETYPE_H_INCLUDED

#include<map>
#include<string>

enum class SPLINEINTTYPE { NONE, NATURAL, GENERAL, ERROR };

struct SPLINEINTTYPE_MAP
{
   static const std::map<std::string,SPLINEINTTYPE> STRING_TO_ENUM;
   
   static const std::map<SPLINEINTTYPE,std::string> ENUM_TO_STRING;

   static SPLINEINTTYPE StringToEnum(const std::string aStr)
   {
      auto it = STRING_TO_ENUM.find(aStr);
      return it!=STRING_TO_ENUM.end() ? it->second : SPLINEINTTYPE::ERROR;
   }

   static std::string EnumToString(SPLINEINTTYPE aType)
   {
      auto it = ENUM_TO_STRING.find(aType);
      return it!=ENUM_TO_STRING.end() ? it->second : std::string("ERROR");
   }
};

#endif /* SPLINETYPE_H_INCLUDED */
