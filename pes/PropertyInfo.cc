/**
************************************************************************
* 
* @file                PropertyInfo.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PropertyInfo datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/PropertyInfo.h"

// std headers
#include <string>
#include <vector>
#include <exception>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"

#include "mpi/Impi.h"


namespace midas
{
namespace pes
{

using ::operator<<;

/* ==========================================================================
 *
 * PropertyInfoEntry implementation
 *
 * ========================================================================== */

/**
 * Default constructor.
 **/
PropertyInfoEntry::PropertyInfoEntry
   (
   ) 
   :  mOrder(I_0)
   ,  mRotGroup(-I_1) // -1 means uninitialized
   ,  mDescriptor("")
   ,  mDerFile("")
{
}

/**
 * Constructor from descriptor string.
 * Will call default constructor and then set descriptor string.
 *
 * @param aDescriptor   Descriptor string, e.g. "GROUND_STATE_ENERGY".
 **/
PropertyInfoEntry::PropertyInfoEntry
   (  const std::string& aDescriptor
   )
   :  PropertyInfoEntry()
{
   mDescriptor = aDescriptor;
}

/**
 * Construct key/value map from string.
 *
 * @param aS
 * @param aKeyValMap
 *
 * @return     Returns bool for success.
 **/
bool PropertyInfoEntry::ConstructMapFromString
   (  std::string aS
   ,  std::map<std::string, std::string>& aKeyValMap
   )  const
{
   aKeyValMap.clear();
   // if line is blank, return false
   if(aS.find_first_not_of(" ")==aS.npos) 
   {
      return false;
   }
   // remove blanks
   while(aS.find_first_of(" ")!=aS.npos) 
   {
      aS.erase(aS.find_first_of(" "),I_1);
   }
   // check for "(" and ")" in string and match
   // those at same level.
   In start=I_0;
   std::vector< std::pair<In,In> > par_vec;
   while(aS.find("(",start)!=aS.npos) 
   {
      In par_start=aS.find("(",start);
      In par_count=I_1;
      In par_end=par_start;
      while(par_count>I_0) 
      {
         par_end++;
         if(par_end>=aS.size()) 
         {
            Mout << "Unbalanced number of ( and ) in: " << aS << endl;
            return false;
         }
         char next_c=aS.at(par_end);
         if(next_c=='(')
         {
            par_count++;
         }
         else if(next_c==')')
         {
            par_count--;
         }
      }
      start=par_end;
      par_vec.push_back(std::make_pair(par_start,par_end));
   }
   std::vector<std::string> key_val_pair;
   start=I_0;
   while(aS.find(",",start)!=aS.npos) {
      In pos=aS.find(",",start);
      start=pos+I_1;
      bool succes=true;
      for(In j=I_0;j<par_vec.size();j++) {
         if(pos>par_vec[j].first && pos<par_vec[j].second) {
            succes=false;
            continue;
         }
      }
      if(succes)
         aS.replace(pos,1," ");
   }
   // a little tedious to reintroduce blanks, but it makes life simple
   // We are now in position to get all key value
   // pairs from the string, separated by " "
   std::vector<std::string> key_val_pairs=SplitString(aS," ");
   for(In i=0;i<key_val_pairs.size();i++) 
   {
      // key is everything until "="
      In key_end=key_val_pairs[i].find("=");
      if(key_end==key_val_pairs[i].npos) 
      {
         MIDASERROR("Required input: key=value, offending entry: "+key_val_pairs[i]);
      }
      std::string key=key_val_pairs[i].substr(I_0,key_end);
      std::string val;
      GetValueFromKey<std::string>(key_val_pairs[i],key,val);
      transform(key.begin(),key.end(),key.begin(),(In(*) (In)) toupper);
      aKeyValMap.insert(std::make_pair(key,val));
   }
   
   return true;
}

/**
 *
 **/
bool PropertyInfoEntry::SetOrder
   (  std::map<std::string, std::string>& aKeyValMap
   ) 
{
   auto it = aKeyValMap.find("TENS_ORDER");
   if(it != aKeyValMap.end())
   {
      // take care of parentheses
      if(it->second.find_first_of("()")!=it->second.npos) 
      {
         it->second=it->second.substr(I_1,it->second.size()-I_2);
      }
      mOrder = midas::util::FromString<In>(it->second);
      return true;
   }
   return false;
}

/**
 *
 **/
bool PropertyInfoEntry::SetDescriptor
   (  std::map<std::string, std::string>& aKeyValMap
   )
{
   auto it = aKeyValMap.find("DESCRIPTOR");
   if(it != aKeyValMap.end())
   {
      // take care of parentheses
      if(it->second.find_first_of("()")!=it->second.npos) {
         it->second=it->second.substr(I_1,it->second.size()-I_2);
      }
      mDescriptor=it->second;
      return true;
   }
   return false;
}

/**
 *
 **/
void PropertyInfoEntry::SetRotGroup
   (  std::map<std::string, std::string>& aKeyValMap
   )  
{
   auto it = aKeyValMap.find("ROT_GROUP");
   if(it != aKeyValMap.end())
   {
      // take care of parentheses
      if(it->second.find_first_of("()")!=it->second.npos) {
         it->second=it->second.substr(I_1,it->second.size()-I_2);
      }
      mRotGroup = midas::util::FromString<In>(it->second);
   }
}

/**
 *
 **/
void PropertyInfoEntry::SetElement
   (  std::map<std::string, std::string>& aKeyValMap
   )
{
   auto it = aKeyValMap.find("ELEMENT");
   if(it == aKeyValMap.end())
      return;
   // strip all ",", "(", and ")"
   std::string s=it->second;
   while(s.find_first_of(",()")!=s.npos)
   {
      s.replace(s.find_first_of(",()"),I_1," ");
   }
   std::vector<std::string> s_vec = SplitString(s," ");
   std::vector<In> i_vec;
   for(In i=I_0;i<s_vec.size();i++) 
   {
      i_vec.emplace_back(midas::util::FromString<In>(s_vec[i]));
   }
   mElement=i_vec;
}

/**
 *
 **/
void PropertyInfoEntry::SetFrq
   (  std::map<std::string, std::string>& aKeyValMap
   )
{
   auto it = aKeyValMap.find("FRQ");
   if(it == aKeyValMap.end())
      return;
   // strip all ",", "(", and ")"
   std::string s=it->second;
   while(s.find_first_of(",()")!=s.npos)
   {
      s.replace(s.find_first_of(",()"),I_1," ");
   }
   std::vector<std::string> s_vec = SplitString(s," ");
   std::vector<Nb> i_vec;
   for(In i=I_0;i<s_vec.size();i++) 
   {
      i_vec.emplace_back(midas::util::FromString<Nb>(s_vec[i]));
   }
   mFrq = i_vec;
}

/**
 *
 **/
void PropertyInfoEntry::SetDerivatives
   (  std::map<std::string, std::string>& aKeyValMap
   ) 
{
   mDerFile = "";
   auto it = aKeyValMap.find("DER_FILE");
   if (it != aKeyValMap.end())
   {
      // Take care of parentheses
      if (it->second.find_first_of("()") != it->second.npos) 
      {
         it->second = it->second.substr(I_1, it->second.size() - I_2);
      }
      mDerFile = it->second;
   }
}

/**
 *
 **/
void PropertyInfoEntry::SetType
   (  std::map<std::string, std::string>& aKeyValMap
   ) 
{
   mType = "";
   auto it = aKeyValMap.find("TYPE");
   if (it != aKeyValMap.end())
   {
      // Take care of parentheses
      if (it->second.find_first_of("()") != it->second.npos) 
      {
         it->second = it->second.substr(I_1, it->second.size() - I_2);
      }
      mType = it->second;
   }
}

/**
 *
 **/
void PropertyInfoEntry::PrintMap
   (  const std::map<std::string, std::string>& aKeyValMap
   )
{
   Mout << "Map in PropertyInfoEntry is:" << endl;
   for(auto it = aKeyValMap.begin(); it != aKeyValMap.end(); ++it)
   {
      Mout << it->first << " -> " << it->second << endl;
   }
}

/**
 * Create a vector of PropertyInfoEntry's from contents of the property info file.
 * Will loop through newline's in input string and create a PropertyInfoEntry for each line.
 *
 * @param aMidasIfcPropInfo    String with contents of the property info file.
 *
 * @return    Return vector of PropertyInfoEntry's.
 **/
std::vector<PropertyInfoEntry> PropertyInfoEntry::Setup
   (  const std::string& aMidasIfcPropInfo
   )
{
   // Assume that the property info file is available in setup directory
   // consider making the place of the info file an input param.
   std::vector<PropertyInfoEntry> info_vec;
   
   //std::ifstream ifs(aFilename);
   //std::istringstream ifs(gPesCalcDef.GetMidasIfcPropInfo());
   std::istringstream ifs(aMidasIfcPropInfo);

   // Process file line-by-line creating a set of PropertyInfoEntry's
   std::string line;
   while (std::getline(ifs, line)) 
   {
      info_vec.emplace_back();
      auto& info = info_vec.back();
      std::map<std::string, std::string> key_val_map;

      if (gDebug)
      {
         Mout << "Line read: " << line << endl;
      }
      if(!info.ConstructMapFromString(line, key_val_map))
      {
         MIDASERROR("Something wrong with line: "+line+" in info-file...");
      }
      if(key_val_map.size()==I_0)
      {
         MIDASERROR("No property infos found...");
      }
      if(!info.SetOrder(key_val_map))
      {
         MIDASERROR("Order of property not defined.");
      }
      if(!info.SetDescriptor(key_val_map))
      {
         MIDASERROR("Descriptor of property not defined.");
      }
      info.SetRotGroup(key_val_map);
      info.SetElement(key_val_map);
      info.SetDerivatives(key_val_map);
      info.SetType(key_val_map);
      info.SetFrq(key_val_map);

      if (gDebug)
      {
         PrintMap(key_val_map);
      }
   }
 
   return info_vec;
}

/**
 * Overload of output operator for ProppertyInfoEntry.
 *
 * @param aOut   The output stream to print from.
 * @param aPi    The PropertyInfoEntry to print.
 *
 * @return       Returns ostream for chaining of operator<<.
 **/
std::ostream& operator<<(std::ostream& aOut, const PropertyInfoEntry& aPi)
{
   aOut << "Descr: " << aPi.GetDescriptor() << "\n";
   aOut << "Order: " << aPi.GetOrder()      << "\n";
   aOut << "Elem : ";
   for(int i = 0; aPi.GetElement().size(); ++i)
   {
      aOut << aPi.GetElement()[i] << " ";
   }
   aOut << "\n";
   aOut << "RG   : " << aPi.GetRotGroup() << "\n";
   aOut << "Type : " << aPi.mType << "\n";
   aOut << "Frq  : " << aPi.mFrq << "\n";
   aOut << "Der  : " << aPi.mDerFile << "\n";

   return aOut;
}

/* ==========================================================================
 *
 * PropertyInfo implementation
 *
 * ========================================================================== */
/**
 * Update PropertyInfo for current multi level.
 *
 * @param aCalcMuTens          Are we also supposed to calculate Mu-Tensor.
 * @param aMidasIfcPropInfo    String with contents of the property info file.
 * @param aLinear              Is the molecule linear?
 **/
void PropertyInfo::Update
   (  bool aCalcMuTens
   ,  const std::string& aMidasIfcPropInfo
   ,  bool aLinear
   )
{
   // Read the property info file for current level.
   mPropertyInfos = PropertyInfoEntry::Setup(aMidasIfcPropInfo);

   // Check that the defined properties do not have duplicate entries
   CheckPropertyInfos(mPropertyInfos);
   
   // If coriolis terms are requested we add them.
   if (aCalcMuTens)
   {
      // Check if molecule is linear
      if (aLinear)
      {
         mPropertyInfos.emplace_back(std::string("MU_LINEAR_INERTIA"));
      }
      else
      {
         mPropertyInfos.emplace_back(std::string("TRACE_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("XX-1_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("XY-1_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("XZ-1_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("YY-1_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("YZ-1_EFFINERTIAINV"));
         mPropertyInfos.emplace_back(std::string("ZZ-1_EFFINERTIAINV"));
      }
   }
}

/**
 * Update PropertyInfo for current multi level.
 *
 * @param aPesCalcDef          PesCalcDef defining calculation.
 * @param aMidasIfcPropInfo    String with contents of the property info file.
 * @param aLinear              Is the molecule linear?
 **/
void PropertyInfo::Update
   (  const PesCalcDef& aPesCalcDef
   ,  const std::string& aMidasIfcPropInfo
   ,  bool aLinear
   )
{
   this->Update(aPesCalcDef.GetmPesCalcMuTens(), aMidasIfcPropInfo, aLinear);
}

/**
 * Check that the property information does not contain any duplicate entries.
 *
 * @param aPropertyInfos          Vector containing information on all properties to be handled
 **/
void PropertyInfo::CheckPropertyInfos
   (  const std::vector<PropertyInfoEntry>& aPropertyInfos
   )
{
   for (int iline = 0; iline < aPropertyInfos.size(); ++iline) 
   {
      const auto& prop_info_curr      = aPropertyInfos[iline];
      const auto& prop_label_curr     = prop_info_curr.GetDescriptor(); 
      const auto& prop_element_curr   = prop_info_curr.GetElement(); 
      const auto& prop_rot_group_curr = prop_info_curr.GetRotGroup();

      for (int jline = iline + 1; jline < aPropertyInfos.size(); ++jline) 
      {
         const auto& prop_info_check      = aPropertyInfos[jline];
         const auto& prop_label_check     = prop_info_check.GetDescriptor(); 
         const auto& prop_element_check   = prop_info_check.GetElement(); 
         const auto& prop_rot_group_check = prop_info_check.GetRotGroup();

         // First, check for non-unique property labels
         if (  prop_label_curr == prop_label_check
            && iline != jline
            )
         {
            std::stringstream sstr;
            sstr  << " The property label " << prop_label_curr 
                  << " is used for more than one property, please change this " 
                  << std::endl;  
            
            MIDASERROR(sstr.str());
         }

         // Second, check for recurring element of the same rotation group 
         if (  prop_rot_group_curr  != -I_1
            && prop_rot_group_check != -I_1
            )
         {
            if (  prop_rot_group_curr == prop_rot_group_check
               && prop_element_curr == prop_element_check
               && prop_label_curr != prop_label_check 
               && iline != jline
               )
            {
               std::stringstream sstr;
               sstr  << " The properties labeled " << prop_label_curr 
                     << " and "                    << prop_label_check 
                     << " are both designated as element ";
               sstr  << prop_element_curr;
               sstr  << " in rotation group "      << prop_rot_group_curr 
                     << ", please resolve this conflict " 
                     << std::endl;
               
               MIDASERROR(sstr.str());
            }
         }
      }
   }
}

/**
 * Get number of properties.
 *
 * @return   Returns number of properties.
 **/
In PropertyInfo::Size
   (
   )  const
{
   return In(mPropertyInfos.size());
}

/**
 * Get PropertyInfoEntry indexed by integer.
 *
 * @param aI   The index.
 *
 * @return     Returns the aI'th PropertyInfoEntry.
 **/
const PropertyInfoEntry& PropertyInfo::GetPropertyInfoEntry
   (  In aI
   )  const
{
   return mPropertyInfos[aI];
}
   
} /* namespace pes */
} /* namespace midas */
