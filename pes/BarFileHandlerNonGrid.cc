/**
************************************************************************
*
* @file                BarFileHandlerNonGrid.cc
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
*
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include<string>
#include<vector>
#include<map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "input/ModeCombi.h"
#include "util/Io.h"
#include "util/Isums.h"
#include "util/FileHandler.h"
#include "util/Timer.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/BarFileHandlerNonGrid.h"
#include "pes/GridType.h"
#include "pes/Derivatives.h"
#include "pes/PesInfo.h"
#include "pes/CalcCode.h"
#include "pes/PesUtility.h"

// using declarations
using std::string;
using std::vector;
using std::map;
typedef string::size_type Sst;

/**
 * Constructor.
 *
 * @param arPesInfo  Reference to the pes info.
 **/
BarFileHandlerNonGrid::BarFileHandlerNonGrid
   (  PesInfo& arPesInfo
   )
   :  BaseBarFileHandler(arPesInfo)
{
}

/**
* Intrinsict driver for actual displacements along coordinates
* for calculating either derivatives or coarse grid points.
*
* @param ModeCom          The mode combi range.
* @param ActualMaxDimMc   The maximum MC size.
* @param GpFr             The grid type?
* @param aCalculationList A calculationlist...
* */
void BarFileHandlerNonGrid::DoDispDer_zero
   (  const ModeCombiOpRange& ModeCom
   ,  In ActualMaxDimMc
   ,  const GridType& GpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_dodispder;
   mPesInfo.CleanUpDerCalcList();

   map<ipair,ipair> n_vec_add_handler;
   vector<InVector> vec_of_all_n_vecs;

   In n_modes_coup = ActualMaxDimMc;

   if (gPesIoLevel > I_10)
   {
      Mout << " ConstructAllNvecs " << std::endl;
   }

   ConstructAllNvecs(vec_of_all_n_vecs, n_vec_add_handler, n_modes_coup, gPesCalcDef.GetmPesNumMaxPolOrExp(), GpFr);

   if (gPesIoLevel > I_10)
   {
      Mout << " done" << std::endl;
   }

   //Do now the K vectors on basis of the N vectors. All k are constructed but some may not be used actually later.
   map<In,ipair> k_vec_add_handler;
   vector<InVector> vec_of_all_k_vecs;
   ConstructAllKvecs(vec_of_all_n_vecs,n_vec_add_handler,vec_of_all_k_vecs,k_vec_add_handler);

   //In NoOfUpdates = I_0;
   //vector<In> UpdateVector(ActualMaxDimMc);

   //The following mode couplings are considered:
   if (gPesIoLevel > I_10)
   {
      Mout << " ModeCombiOpRange " << std::endl << ModeCom << std::endl;
   }

// Open file for writing information about the order in the .mbar files

   In IbarCount = I_0;
   In IbarCountSave = I_0;

   Timer msi_test_timer;
   // then run over the set of modes
   for(const auto& mt: ModeCom)
   {
      //Mout << " current mode: " << mt << " to be screened: " << mt.IsToBeScreened() << endl;
      //for MC screening neglect the calculation of the full MC
      //if ((gPesCalcDef.GetmPesScreenModeCombi() && mt.Size()==I_2 ) )
      //{
         if (mt.IsToBeScreened()) continue;
      //}
      //just for Adga pes
      ipair ModeCombiOrder_Der = std::make_pair(mt.Size(),gPesCalcDef.GetmPesNumMaxPolOrExp());
      ipair beg_step;
      beg_step = n_vec_add_handler[ModeCombiOrder_Der];
      In beg = beg_step.first;
      In end = beg_step.first + beg_step.second;

      //determines if plotting of potentials have been requested. Open needed files
      vector<In> local_com;
      local_com=mt.MCVec();

      //Mout << " end of opening plotting files " << endl;
      //we compute the l.c.map here since it depends only on the actual mc.
      Timer time_all;
      for (In n=beg;n<end;n++)
      {
         ipair beg_step_k = k_vec_add_handler[n];
         In begk = beg_step_k.first;
         In endk = beg_step_k.first + beg_step_k.second;

         //bool Match;

         //loop now over the set of k vectors and do the actual displacements (ModeDisplacements).
         for (In k=begk;k<endk;++k)
         {
            vector<In> kvec = vec_of_all_k_vecs[k];

            //Mout << " enter in ModeDisplacement with kvec: " << kvec << endl;
            //Mout << " entered in modedisplacement " << endl;

            //ModeDisplacements_zero(mt,kvec,NoOfUpdates,Match,GpFr);
            ModeDisplacements_zero(mt, kvec, GpFr, aCalculationList);
            //Mout << " exit from modedisplacement " << endl;
            //In Ma_mode = I_0;
            //if (Match && gPesIoLevel > I_4)
            //{
            //   ModeAnalyser(NoOfUpdates, UpdateVector, Ma_mode);
            //}
         }
      }
   } //end run over modecombi

   if(gTime)
   {
      time_dodispder.CpuOut(Mout,"\n CPU  time used in the DoDispDer_zero: ");
      time_dodispder.WallOut(Mout," Wall time used in the DoDispDer_zero: ");
   }
}

/**
 * Do actual displacement
 **/
void BarFileHandlerNonGrid::ModeDisplacements_zero
   (  const ModeCombi& arModeCombi
   ,  std::vector<In>& arKvec
   ,  const GridType& arGpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_ada;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR(" Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

   // If gPesCalcDef.GetmPesGrid() we need to know the address of the MC in order to find the grid information in class GridType.
   // In case of derivative force field Adr simply keep its value of -1.
   In add=-I_1;

   string list_of_calcs = "";

   for (In i=I_0;i<arKvec.size();i++)
   {
      In i_mode = arModeCombi.Mode(i);
      string string1    = std::to_string(i_mode+1);
      string string2    = std::to_string(arKvec[i])+"/1";

      if (arKvec[i] != I_0)
      {
         list_of_calcs = list_of_calcs + "#"+string1+"_"+string2;
         //no_of_updates += 1;
      }
      else if ( (arKvec.size() == I_1) && (arKvec[i] == I_0) )
      {
         list_of_calcs = "#0";
         //no_of_updates += 1;
      }
      else if ( (arKvec.size() != I_1) && (InNorm2(arKvec) == I_0) )
      {
         list_of_calcs = "#0";
      }
      // else do nothing.
   }

   list_of_calcs = "*" + list_of_calcs + "*";
   //now do the actual displacements ...

   //Mout << " Entering with calc: " << list_of_calcs << endl;
   bool calc_todo=true;

   //// If Adga pes check now if the dispalcement is needed
   //if (gPesCalcDef.GetmPesScreenModeCombi() )
   //{
   //   //if (no_of_updates != arModeCombi.Size())
   //   //{
   //      //no_of_calcs-=1;
   //      calc_todo=false;
   //   //}
   //}

   //Mout << " For calc: " << list_of_calcs << " calc_todo: " << calc_todo << endl;
   if (!arModeCombi.IsToBeExtrap() || !gPesCalcDef.GetmPesDoExtrap())
   {
      if(calc_todo)
      {
         aCalculationList.AddEntry(list_of_calcs); //add to the list
      }
   }
   else if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap())
   {
      if(calc_todo)
      {
         mPesInfo.AddDerCalc(list_of_calcs); //add to the list to be extrapolated
      }
      //else
      //{
         //do not add the calculation
         //--no_of_calcs_extrap;
      //}
   }

   if (gPesIoLevel > I_10) time_ada.CpuOut(Mout,"\n CPU  time used in the ModeDisplacements_zero: ",true);
   if (gPesIoLevel > I_10) time_ada.WallOut(Mout," Wall time used in the ModeDisplacements_zero: ",true);
}

/**
 * Intrinsict driver for actual displacements along coordinates for calculating either derivatives or coarse grid points
 **/
void BarFileHandlerNonGrid::DoDispDer_one
   (  const ModeCombiOpRange& ModeCom
   ,  In ActualMaxDimMc
   ,  const GridType& GpFr
   ,  const Derivatives& arDerivatives
   )
{
   Timer time_dodispder;
   map<ipair,ipair> n_vec_add_handler;
   vector<InVector> vec_of_all_n_vecs;
   In n_modes_coup=ActualMaxDimMc;

   if (gPesIoLevel > I_10)
   {
      Mout << " ConstructAllNvecs" << std::endl;
   }

   ConstructAllNvecs(vec_of_all_n_vecs,n_vec_add_handler,n_modes_coup,gPesCalcDef.GetmPesNumMaxPolOrExp(),GpFr);

   if (gPesIoLevel > I_10)
   {
      Mout << " done" << std::endl;
   }

   //Do now the K vectors on basis of the N vectors. All k are constructed but some may not be used actually later.
   map<In,ipair> k_vec_add_handler;
   vector<InVector> vec_of_all_k_vecs;
   ConstructAllKvecs(vec_of_all_n_vecs,n_vec_add_handler,vec_of_all_k_vecs,k_vec_add_handler);

   //In NoOfUpdates = I_0;
   //vector<In> UpdateVector(ActualMaxDimMc);

   //The following mode couplings are considered:
   FileHandles::FileHandler File;
   FileHandles::FileHandler File_for_extrap;
   File.OpenFiles(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_", mPesInfo.GetMultiLevelInfo().GetNoOfProps(), ".mop");
   //File << FileHandles::to_all << "Constants:" << endl << "Functions:" << endl << "Operators:" << endl;
   File << FileHandles::to_all << "#0MIDASOPERATOR\n"
                               << "#1CONSTANTS\n"
                               << "#1FUNCTIONS\n"
                               << "#1OPERATORTERMS" << endl;

   vector< pair<In,Nb> > Vib_Freqs;
   set<string> Test_Calc;

   // Open file for writing information about the order in the .mbar files

   In IbarCount = I_0;
   In IbarCountSave = I_0;

   // then run over the set of modes
   for(const auto& mt: ModeCom)
   {
      //Mout << " current mode: " << mt << " to be screened: " << mt.IsToBeScreened() << endl;
      //for MC screening neglect the calculation of the full MC
      //if ((gPesCalcDef.GetmPesScreenModeCombi() && mt.Size()==I_2 ) )
      //{
         if (mt.IsToBeScreened()) continue;
      //}
      ipair ModeCombiOrder_Der = std::make_pair(mt.Size(),gPesCalcDef.GetmPesNumMaxPolOrExp());
      ipair beg_step;
      beg_step = n_vec_add_handler[ModeCombiOrder_Der];
      In beg = beg_step.first;
      In end = beg_step.first + beg_step.second;

      //determines if plotting of potentials have been requested. Open needed files
      vector<In> local_com;
      local_com=mt.MCVec();
      //if (mt.MCVec().size()>0) Mout << " mode is : " << mt.MCVec() << endl;
      bool loc_pot=false;
      bool only_pot=true;
      for (In n = beg; n < end; n++)
      {
         In begk;
         In endk;
         ipair beg_step_k = k_vec_add_handler[n];
         begk = beg_step_k.first;
         endk = beg_step_k.first + beg_step_k.second;


         MidasVector Properties(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
         Properties.Zero();
         bool Match = false;

         // loop now over the set of k vectors and do the actual displacements (ModeDisplacements).
         for (In k=begk;k<endk;k++)
         {
            vector<Nb> Ncor;
            vector<In> kvec = vec_of_all_k_vecs[k];
            string calc_code="";
            string calc_no="";
            vector<MidasVector> grad_hess;
            for (In prop=I_0;prop<mPesInfo.GetMultiLevelInfo().GetNoOfProps();prop++)
            {
               In n_dim=I_0;
               MidasVector grad_hess_v;
               if (mPesInfo.GetmUseDerivatives() && gPesCalcDef.GetmPesShepardInt() )
               {
                  In order = gPesCalcDef.GetmPesShepardOrd();                     // At mc=1, It=1 we assume order gPesCalcDef.GetmPesShepardOrd() for all the prop
                  if (InquireFile(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_files.mpesinfo"))
                  {
                     string name_pr=mPesInfo.GetMultiLevelInfo().GetPropertyName(prop);    //adding 1 to match the number in these_prop_ecc ecc
                     order=arDerivatives.OrderDer(name_pr);
                  }
                  //Mout << " Shepard interpolation for mode combi: " << mc << endl;
                  if (order>I_0) n_dim+=mt.Size();
                  if (order==I_2) n_dim+=mt.Size()*(mt.Size()+I_1)/I_2;
                  else if (order>I_2) MIDASERROR(" Shepard order greater than 2 ");
               }
               grad_hess_v.SetNewSize(n_dim);
               grad_hess_v.Zero();
               grad_hess.push_back(grad_hess_v);
            }
            //Mout << " enter in ModeDisplacement with kvec: " << kvec << endl;
            Nb vgrid=C_0; //stores value of full potential on the selected point
            //Mout << " entered in modedisplacement " << endl;
            map<InVector,Nb> lc_map;
            ModeDisplacements_one(mt,vec_of_all_n_vecs[n],kvec,Properties,
                  Match,Ncor,GpFr,vgrid,loc_pot,lc_map, calc_no, calc_code, arDerivatives, grad_hess);
            //Mout << " exit from modedisplacement " << endl;
            if ((!Match) && (!gPesCalcDef.GetmPesGrid()) ) k = endk; // skip the rest of the points because one is missing
         }
         if (Match)
         {
            InVector specific_n_vector = vec_of_all_n_vecs[n];
            string Derivative = "V_";
            string Derivative1 = "";
            In Total_Der_Order = I_0;
            if (specific_n_vector.size() != mt.Size())
            {
               MIDASERROR(" Dimension if n vector wrong in DoDispDer. I stop ...");
            }
            Total_Der_Order = I_0;
            for (In j=I_0;j<specific_n_vector.size();j++)
            {
               In specific_mode   = mt.Mode(j);
               In specific_der_no = specific_n_vector[j];
               Total_Der_Order += specific_der_no;
               Derivative1 = Derivative1 + "Q^"+ std::to_string(specific_der_no)+"(Q"+std::to_string(specific_mode)+")" + " ";
            }

            In Size_Of_Derivative = Derivative1.size();
            Derivative1.erase(Size_Of_Derivative-1,1);

            if (gPesIoLevel > I_11)
            {
               Mout << " " << Derivative1 << std::endl;
            }

            File.Width(29);
            File << FileHandles::to_dist << Properties;
            File << FileHandles::to_all << Derivative1 << endl;
            for (In p=I_0;p<mPesInfo.GetMultiLevelInfo().GetNoOfProps();p++)
            {
               Mout.setf(ios::scientific);
               Mout.setf(ios::uppercase);
               midas::stream::ScopedPrecision(22, Mout);

               if (gPesIoLevel > I_11)
               {
                  Mout << "  For property " << mPesInfo.GetMultiLevelInfo().GetPropertyName(p) << ", obtained derivative: " << Properties[p] << std::endl;
               }

               std::string Ground_Excited;
               if (gPesCalcDef.GetmPesExcState() == I_0)
               {
                  Ground_Excited = "GROUND_STATE_ENERGY";
               }
               if (gPesCalcDef.GetmPesExcState() > I_0)
               {
                  Ground_Excited = "EXCITED_STATE_ENERGY";
               }

               In PropNu = FindPropertyNumberInFile(mPesInfo, Ground_Excited, gPesCalcDef.GetmPesExcState());

               if ( ((PropNu-I_1) == p) && (!gPesCalcDef.GetmCartesiancoordInPes()) && (specific_n_vector.size() == I_1)
                     && (Total_Der_Order == I_2)) //Hessian diagonal
               {
                  Nb vib_freq = sqrt(fabs(Properties[p])*C_2)*C_AUTKAYS;
                  Vib_Freqs.push_back(std::make_pair(mt.Mode(0)+1,vib_freq));
               }
            }
         }
         else
         {
            Mout << " This derivative is skiped because I am missing some points ! " << endl;
         }

      }
   } //end run over modecombi

   // end .mop output
   File << FileHandles::to_all << "#0MIDASOPERATOREND" << std::endl;
   File.Close();

   if (!gPesCalcDef.GetmCartesiancoordInPes() && !gPesCalcDef.GetmPesGrid())
   {
      Mout << std::endl;
      Mout << " Electronic state No. " << std::to_string(gPesCalcDef.GetmPesExcState()) << std::endl;
      Mout << " Calculated Harmonic vibrational frequencies in cm^-1: " << std::endl;
      Mout << " ***************************************************** " << std::endl;
      for (In k = I_0; k < Vib_Freqs.size(); k++)
      {
         Mout << setw(5) << Vib_Freqs[k].first << "     " << Vib_Freqs[k].second << std::endl;
      }

      if (!gPesCalcDef.GetmPesDiatomic())
      {
         Mout << std::endl;
         Mout << " Input Harmonic vibrational frequencies in cm^-1: " << std::endl;
         Mout << " ************************************************ " << std::endl;
         for (In k = I_0; k < mPesInfo.GetMolecule().GetNoOfVibs(); k++)
         {
            Mout <<  left << setw(5) << k + I_1 << "     " << setw(25) << mPesInfo.GetMolecule().GetFreqI(k) << endl;
         }
         Mout << std::endl;
      }

      std::string Freq_File = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "harmonic_frequencies.mpesinfo";
      DumpFreqsOnFile(Vib_Freqs, Freq_File);
   }

   if (gPesIoLevel > I_4)
   {
      Mout << " DoDispDer_one: Done! " << std::endl;
   }

   if (gTime)
   {
      time_dodispder.CpuOut(Mout,"Time in DDD_one: ");
      time_dodispder.CpuOut(Mout,"\n CPU  time used in the DoDispDer_one: ");
      time_dodispder.WallOut(Mout," Wall time used in the DoDispDer_one: ");
   }
}

/**
 * Calculate derivative (no grid) or V-bar potentials (grid).
 * THIS FUNCTION HAS 15 ARGUMENTS!!! 15 I TELL YOU!!!!!!!!!
 **/
void BarFileHandlerNonGrid::ModeDisplacements_one
   (  const ModeCombi& arModeCombi
   ,  vector<In>& arNvec
   ,  vector<In>& arKvec
   ,  MidasVector& arProperties
   ,  bool& arMatch
   ,  vector<Nb>& arNCor
   ,  const GridType& arGpFr
   ,  Nb& vgrid
   ,  bool plotv
   ,  std::map<InVector,Nb>& arLinCom
   ,  string& arCalcNo
   ,  string& arCalcCode
   ,  const Derivatives& arDerivatives
   ,  vector<MidasVector>& arGradHess
   )
{
   Timer time_ada;

   arMatch = true;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR(" Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

   // If gPesCalcDef.GetmPesGrid() we need to know the address of the MC in order to find the grid information in class GridType.
   // In case of derivative force field Adr simply keep its value of -1.
   In add=-I_1;

   string list_of_calcs = "";

   for (In i=I_0;i<arKvec.size();i++)
   {
      In i_mode = arModeCombi.Mode(i);
      string string1    = std::to_string(i_mode+1);
      string string2    = std::to_string(arKvec[i])+"/1";
      if (arKvec[i] != I_0)
      {
         list_of_calcs = list_of_calcs + "#"+string1+"_"+string2;
         //arNoOfUpdates += 1;
      }
      else if ( (arKvec.size() == I_1) && (arKvec[i] == I_0) )
      {
         list_of_calcs = "#0";
         //arNoOfUpdates += 1;
      }
      else if ( (arKvec.size() != I_1) && (InNorm2(arKvec) == I_0) )
      {
         list_of_calcs = "#0";
      }
      // else do nothing.
   }

   list_of_calcs = "*" + list_of_calcs + "*";
   //now do the actual displacements ...
   arMatch = true;

   //// check if list_of_calcs (displacement code) is on Done File
   bool test;
   //test = list_of_calcs != "*#0*" ? mPesInfo.IsOnList(list_of_calcs) : false;
   //
   //In CalcNo = I_0;
   //if (!test)
   //{
   //   Mout << " Warning ! Displacement " << list_of_calcs << " not found  "<< endl;
   //}
   //else
   //{
   //   CalcNo = mPesInfo.GetCalcNrFromCode(list_of_calcs);
   //}

   MidasVector temp(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
   temp.Zero();

   ifstream* File = new ifstream [mPesInfo.GetMultiLevelInfo().GetNoOfProps()];
   for (In i=I_0;i<mPesInfo.GetMultiLevelInfo().GetNoOfProps();i++)
   {
      std::string PropFileName = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_" + std::to_string(i + I_1) + ".mpoints";
      File[i].open(PropFileName.c_str());
      File[i].setf(ios::scientific);
      File[i].setf(ios::uppercase);
      File[i].precision(I_22);

      In Ioption = I_2;
      Nb Prop;
      string CalcNoFromFile;
      test = SearchInFile2(list_of_calcs,File[i],PropFileName,CalcNoFromFile,Prop,Ioption);

      if (!test)
      {
         Mout << " Warning ! I did not find property for calculation " << list_of_calcs << "  in file " << PropFileName << endl;
         arMatch = false;
      }

      temp[i] = Prop;

      //if (std::to_string(CalcNo) != CalcNoFromFile)
      //{
      //   Mout << " Error in reading property " << endl;
      //   arMatch = false;
      //}

      File[i].close();
   }
   delete [] File;
   Nb C = C_1;
   for (In j=I_0;j<arModeCombi.Size();j++)
   {
      C *= C_function(arKvec[j],arNvec[j]);
   }
   Nb DeltaInv = C_1;
   for (In j=I_0;j<arModeCombi.Size();j++)
   {
      In specific_mode = arModeCombi.Mode(j);
      Nb Specific_Scaling = C_0;
      if (gPesCalcDef.GetmNormalcoordInPes())
      {
         Specific_Scaling = mPesInfo.GetScalingInfo().GetRScalFact(specific_mode);
      }
      if (gPesCalcDef.GetmCartesiancoordInPes())
      {
         Specific_Scaling = gPesCalcDef.GetmPesStepSizeInAu();
      }
      if (gPesCalcDef.GetmPesDiatomic())
      {
         Specific_Scaling = gPesCalcDef.GetmPesStepSizeInAu();
      }

      Nb specific_der_no_Nb = Nb(arNvec[j]);
      DeltaInv *= std::pow(Specific_Scaling, -specific_der_no_Nb);
   }

   temp.Scale(C*DeltaInv,I_0,-I_1);

   arProperties += temp;
   if (gTime)
   {
      time_ada.CpuOut(Mout,"\n CPU  time used in the ModeDisplacements_one: ",true);
      time_ada.WallOut(Mout," Wall time used in the ModeDisplacements_one: ",true);
   }
}

/**
 * Calculate the C coef. for use in numerical derivatives
 * Calculates the C factors, e.g. the coefficients in the expansion. Here we include the
 * factor due to the order in the taylor expansion (absorb all equal contributions into one)
 * and also we here convert fra amu to au, e.g. the final derivatives will include expansion
 * coefficients and be in au
 **/
Nb BarFileHandlerNonGrid::C_function(In& aK, In& aN)
{
   if (aN%I_2 == I_0)
   {
      Nb ak  = Nb(aK);
      Nb an  = Nb(aN);
      Nb i   = an/C_2;
      In I   = aN/I_2;
      Nb C   = pow(-C_1,i+ak)*NbBinCo(I_2*I,I+aK);
      Nb Fac = C_1/NbFact(aN);
      Nb Sqrt_C_FAMU = sqrt(C_FAMU);
      Nb FacMass = pow(C_1/Sqrt_C_FAMU,Nb(aN));
      C     *= Fac;
      if (gPesCalcDef.GetmNormalcoordInPes()) C *= FacMass;
      if (gPesCalcDef.GetmPesDiatomic())
      {
         Nb Reduced_Mass = mPesInfo.GetMolecule().GiveRedMass();
         FacMass *= pow(C_1/sqrt(Reduced_Mass),Nb(aN));
         C *= FacMass;
      }
      return C;
   }
   else
   {
      Nb ak  = Nb(aK);
      Nb an  = Nb(aN);
      Nb i   = (an-C_1)/C_2;
      In I   = (aN-I_1)/I_2;
      Nb C   = C_I_2*pow(-C_1,-i+ak)*( NbBinCo(I_2*I,I+aK+I_1) - NbBinCo(I_2*I,I+aK-I_1) );
      Nb Fac = C_1/NbFact(aN);
      Nb Sqrt_C_FAMU = sqrt(C_FAMU);
      Nb FacMass = pow(C_1/Sqrt_C_FAMU,Nb(aN));
      C     *= Fac;
      if (gPesCalcDef.GetmNormalcoordInPes()) C *= FacMass;
      if (gPesCalcDef.GetmPesDiatomic())
      {
         Nb Reduced_Mass = mPesInfo.GetMolecule().GiveRedMass();
         FacMass *= pow(C_1/sqrt(Reduced_Mass),Nb(aN));
         C *= FacMass;
      }
      return C;
   }
}

/**
 * Construct vector of all Kvectors
 * */
void BarFileHandlerNonGrid::ConstructAllKvecs
   (  const std::vector<InVector>& arNvecVec
   ,  const std::map<ipair,ipair>& arNvecAddMap
   ,  std::vector<InVector>& arKvecVec
   ,  std::map<In,ipair>& arKvecAddMap
   )  const
{
   arKvecVec.clear();
   arKvecAddMap.clear();

   for (map<ipair,ipair>::const_iterator ci=arNvecAddMap.begin();ci!=arNvecAddMap.end();ci++)
   {
      In add =ci->second.first;
      In n_n =ci->second.second;
      for (In i=add;i<add+n_n;i++)
      {
         In n_dim=arNvecVec[i].size(); // dim of mc,n,and k.
         InVector i_vec_cur(n_dim,I_0);
         InVector limits(n_dim,I_0);
         InVector nvec=arNvecVec[i];
         for (In j=I_0;j<n_dim;j++)
         {
            if (gPesCalcDef.GetmPesGrid())
            {
               limits[j]=nvec[j];
            }
            else
            {
               limits[j]=(nvec[j]+I_1)/I_2;
            }
         }

         In level=I_0;
         In n_bef=arKvecVec.size();
         AddKvecsToVec(arKvecVec, i_vec_cur, limits,nvec,level);
         In n_aft=arKvecVec.size();
         In added=n_aft-n_bef;
         ipair begnum(n_bef,added);
         arKvecAddMap[i]=begnum;
      }
   }
}

/**
 * Construct vector of all Nvectors
 * */
void BarFileHandlerNonGrid::ConstructAllNvecs(vector<InVector>& arNvecVec, map<ipair,ipair>& arNvecAddMap, In aNmxMc,In aNmxDer,GridType aGpFr)
{
   arNvecVec.clear();
   arNvecAddMap.clear();
   for (In n_mc=I_1;n_mc<=aNmxMc;n_mc++)
   {
      InVector i_vec_cur(n_mc);
      for (In i=I_0;i<n_mc;i++) i_vec_cur[i]=1;

      for (In n_der=n_mc;n_der<=aNmxDer;n_der++)
      {
         In level=I_0;
         In rest_max  = n_der - n_mc;
         In n_bef=arNvecVec.size();
         AddNvecsToVec(arNvecVec, i_vec_cur, n_mc, level, rest_max);
         In n_aft=arNvecVec.size();
         In added=n_aft-n_bef;
         ipair mcder(n_mc,n_der);
         ipair begnum(n_bef,added);
         arNvecAddMap[mcder]=begnum;
      }
   }
}

/**
 * Recursive addition to set of k-vectors
 * */
void BarFileHandlerNonGrid::AddKvecsToVec
   (  std::vector<InVector>& arKvecVec
   ,  InVector aInVec
   ,  const InVector& arKlimits
   ,  const InVector& arNvec
   ,  In aLevel
   )  const
{
   for (In i=-arKlimits[aLevel];i<=arKlimits[aLevel];i++)
   {
      if (!gPesCalcDef.GetmPesGrid())
      {
         if ((arNvec[aLevel]==3||arNvec[aLevel]==1)&&i==0) continue;
      }
      aInVec[aLevel]=i;
      if (aLevel< (arKlimits.size()-1))
      {
         AddKvecsToVec(arKvecVec, aInVec, arKlimits, arNvec, aLevel+I_1);
      }
      else
      {
         arKvecVec.push_back(aInVec);
      }
   }
}

/**
 * Recursive addition to vector of Nvectors
 * */
void BarFileHandlerNonGrid::AddNvecsToVec
   (  vector<InVector>& arNvecVec
   ,  InVector aInVec
   ,  In& arNmc
   ,  In aLevel
   ,  In& arRestMax
   )  const
{
   for (In i_t=I_0;i_t<=arRestMax;i_t++)
   {
      In a_rest = arRestMax-i_t;
      aInVec[aLevel]= I_1+i_t;
      if (aLevel==(arNmc-I_1) || a_rest== I_0 ) // we have reached end, add to list
      {
         arNvecVec.push_back(aInVec);
      }
      else
      {
         AddNvecsToVec(arNvecVec,aInVec,arNmc,aLevel+1,a_rest);
      }
   }
}

/**
 *
 **/
void BarFileHandlerNonGrid::Extrapolation
   (  const Derivatives&
   ,  const CalculationList&
   ,  const std::vector<std::string>&
   )
{
   MIDASERROR("Should not be called here");
};
