/**
************************************************************************
*
* @file                GridType.h
*
* Created:             06-11-2006
*
* Author:              Jacob Kongsted
*
* Short Description:   
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef GRIDTYPE_H
#define GRIDTYPE_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>
#include <map>
#include <utility>
#include <tuple>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

/**
 * Hold information on grids, i.e. modecombi, gridpoints, and fractioning
 **/
class GridType
{  
   //! Define type to hold the grid definitnion:
   //!                            mode combi       grid points      fractioning
   using grid_type_t = std::tuple<std::vector<In>, std::vector<In>, std::vector<std::string> >; 
   
   private:
      //! Each Modecombination gets its own grid_type_t
      std::vector<grid_type_t> mGridType;
      //! largest mode combi in gridtype
      In mMaxMcLevel = 0; 
      
      //! update max mc level from mode combi
      void UpdateMaxMcLevel(const std::vector<In>&);
      
      //! find abs max element in vector of integers
      In GetMax(const std::vector<In>&) const;

   public:
      //! Constructor
      GridType() : mGridType() {}

      //! add grid type by giving modecombi, grid points, and fractioning
      In AddGridType(const std::vector<In>&, const std::vector<In>&, const std::vector<std::string>&);

      //! update grid for specific mode combi (used for adga, when additional point are added)
      void UpdateGridType(const std::vector<In>&, const std::vector<In>&, const std::vector<std::string>&);
      
      //!@{
      //! Access grid information
      const std::vector<In>&          ModeCombi (In i) const { return std::get<0>(mGridType[i]); }
      const std::vector<In>&          GridPoints(In i) const { return std::get<1>(mGridType[i]); }
      const std::vector<std::string>& Fractions (In i) const { return std::get<2>(mGridType[i]); }
      //!@}
      
      //! get number of grid types
      In Size() const { return mGridType.size(); }
      
      //! Get grid adress for mode combi
      In GridAddress(const std::vector<In>&) const;
      
      //! Get maximum number of grid points in each dim (mc level)
      std::vector<In> MaxInEachDim() const;
      
      //! Print a fraction
      void PrintSpecificFraction(In arI);
      
      //! Check a fraction
      void CheckFractions(In, vector<In>);
};

#endif /* GRIDTYPE_H_INCLUDED */
