/**
************************************************************************
* 
* @file                AdgaSurface.cc
*
* Created:             10-08-2012
*
* Author:              Danielle Toffoli ()               original implementation
*                      Bo Thomsen (drbothomsen@gmail.com) separation of pes functionalities
*
* Short Description:   Running grid based iterative (ADGA) surface calculation 
* 
* Last modified:       02-06-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "mmv/MidasMatrix.h"
#include "vscf/VscfDrv.h"
#include "pes/AdgaSurface.h"
#include "pes/Derivatives.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "pes/AdgaBarFileHandler.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/PesFuncs.h"
#include "input/Input.h"

#include "mpi/Impi.h"
#include "util/paral/thread_pool.h"
#include "concurrency/Info.h"


#include "pes/Trainer.h"
#include "adga/ItGridHandler.h"

// using declarations
using std::ofstream;
using std::vector;
using std::string;

namespace midas
{
namespace pes
{

/**
 * Constructor for AdgaSurface
 *
 * @param aPesInfos        Info on current surface(s).
 * @param aPesMainDir      The main directory for the surface(s) generation. 
 **/
AdgaSurface::AdgaSurface
   (  std::vector<PesInfo>& aPesInfos
   ,  const std::string& aPesMainDir
   ) 
   :  Surface  
         (  aPesInfos
         ,  aPesMainDir
         )
   ,  mProperties 
         (  gPesCalcDef.GetmPesAdaptiveProps().size()
         )
   ,  mDincrInfo
      (  gPesCalcDef
      ,  aPesMainDir
      )
   ,  mUseGaussianBasis(false)
{
   // Initialize the bar-file handler into an Adga type bar-file handler
   for (auto& pes_info : mPesInfos)
   {
      mBarFiles.emplace_back(new AdgaBarFileHandler(pes_info));
   }

   // Get the correct Vscf calculation definitions for the Adga calculation and extend the Adga-specific Vscf calculation definitions with the special Adga requirements
   this->InitializeAdgaVscf();
   
   // Initialize double incremental info if needed
   if (gPesCalcDef.GetmDincrSurface())
   {
      std::vector<std::string> fc_names;
      mDincrInfo.InitializeFragmentCombinations(fc_names);
   }
}

/**
 * Main Adga surface driver function
 **/
void AdgaSurface::RunSurface
   (
   )
{
   // Global initialization of the iterative (Adga) grids
   std::vector<ItGridHandler> subsets_iter_grids;
   subsets_iter_grids.reserve(mNoSubsystems);
   for (const auto& pes_info : mPesInfos)
   {
      auto iter_grid = ItGridHandler(pes_info.GetMolecule().GetNoOfVibs(), gPesCalcDef.GetmPesAdaptiveProps(),  gPesCalcDef.GetmPesAdaptiveProps().size());

      subsets_iter_grids.emplace_back(iter_grid);
   }

   // Initialize the Adga bar-file handler for each subsystem
   for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
   {
      mBarFiles[isystem]->SetmIterGrid(&subsets_iter_grids[isystem]);
   }
   
   // Multilevel calculation header
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin Multilevel Procedure for PES ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');

   // Maximum mode combination level for the surface
   In max_pes_num_mcr = gPesCalcDef.GetmPesNumMCR();

   // Loop over multilevels, i.e. electronic structure methods 
   for (In ilevel = I_0; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
   {
      // Adga calculation header
      Mout << "\n\n";
      Out72Char(Mout,'$','$','$');
      Out72Char(Mout,'$',' ','$');
      OneArgOut72(Mout," Begin Adaptive Density-Guided Approach for surface(s) level " + std::to_string(ilevel),'$');
      Out72Char(Mout,'$',' ','$');
      Out72Char(Mout,'$','$','$');
      Mout << "\n\n";

      // Initializing the surface calculation
      if (!(ilevel == I_0 && !gPesCalcDef.BoundariesPreOpt()))
      {
         // Update PesInfo's with multilevel info
         for (auto& pes_info : mPesInfos)
         {
            // Touch ListOfCalcs_Done if doing linear combinations of property terms
            if (gPesCalcDef.GetmPesDoLinearComb())
            {
               midas::filesystem::Touch(pes_info.GetMultiLevelInfo().SaveIoDir() + "/ListOfCalcs_Done");
            }

            //
            pes_info.Update(gPesCalcDef, ilevel);
         }
      }
      
      // Init level and check if level is already converged (from a restart)
      In n_modes_coup = gPesCalcDef.LevelMc()[ilevel];
     
      // Determine if derivative information is to be used for modified Shepard interpolation and/or extrapolation
      std::vector<Derivatives> subsystem_derivatives;
      subsystem_derivatives.reserve(mNoSubsystems);
      InitLevel(ilevel, n_modes_coup, subsystem_derivatives, subsets_iter_grids);

      // Define properties for different subsystems, i.e. mode combination range and grid types
      auto max_mcl = max_pes_num_mcr;
      std::vector<ModeCombiOpRange> subsystems_mcr;
      std::vector<GridType> grid_fractions;
      std::vector<std::vector<bool> > skip_mc_fit;
         
      // Allocate memory
      subsystems_mcr.reserve(mNoSubsystems);
      grid_fractions.reserve(mNoSubsystems);
      skip_mc_fit.reserve(mNoSubsystems);

      // Loop over subsystems and set prerequisites for surface generation
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // Construct the mode combination range for each subsystem
         ModeCombiOpRange mcr;
         if (gPesCalcDef.GetmUseExtMcr())
         {
            mcr = ConstructModeCombiOpRangeFromExtendedRangeModes
               (  max_pes_num_mcr
               ,  mPesInfos[isystem].GetMolecule().GetNoOfVibs()
               ,  gPesCalcDef.GetmExtMcrModes()
               );
         }
         else
         {
            mcr = ConstructModeCombiOpRange
               (  gPesCalcDef.GetmPesFlexCoupCalcDef()
               ,  max_pes_num_mcr
               ,  mPesInfos[isystem].GetMolecule().GetNoOfVibs()
               );
         }

         // Output the (modified) mode combination range
         if (gPesIoLevel > I_14)
         {
            Mout << " The effective mode combination range contains the mode combinations: " << std::endl;
            for (const auto& mode_combi: mcr)
            {
               Mout << "  " << mode_combi.MCVec() << std::endl;
            }
         }

         // Initialize the multilevel grids of single points
         GridType one_mode_grid_fractions;
         GridSetup(mPesInfos[isystem], I_0, mcr, max_mcl, one_mode_grid_fractions);

         //
         if (ilevel == I_0) 
         {
            // Initialize the 1-mode grids 
            InitializeIterGrid(I_1, mcr, one_mode_grid_fractions, isystem, subsets_iter_grids[isystem]);
         }
         else 
         {  
            // Reinitialize the iterative grid for a given multilevel
            subsets_iter_grids[isystem].ReInitialize(ilevel);

            // Updated the 1-mode grids with information from the iterative grid class
            UpdateGridType(I_1, mcr, subsets_iter_grids[isystem], one_mode_grid_fractions);
         }

         // Store mode combination range and grid fractions for later use
         subsystems_mcr.emplace_back(mcr);
         grid_fractions.emplace_back(one_mode_grid_fractions);
            
         // Initialize the vector which check that it is worthwhile to perform a given fit
         std::vector<bool> subsystem_skip_mc_fit(mcr.Size(), false);
         skip_mc_fit.emplace_back(subsystem_skip_mc_fit);

         // When starting a new multilevel calculation we need to make sure that the map of operator terms is empty, i.e. that it does not contain terms from the subsequent multilevel calculation
         mDoPotentialFits[isystem].CleanOperatorMap();
      }
     
      // If no pre-optimization of boundaries is requested, continue to next multilevel
      if (ilevel == I_0 && !gPesCalcDef.BoundariesPreOpt())
      {
         continue;
      }
   
      // For double incremental surface calculation, the DincrInfo needs to know how many mode combinations that are present for each subsystem
      if (gPesCalcDef.GetmDincrSurface())
      {
         std::vector<In> subsystems_no_mcs;
         for (const auto& subsystem_mcr : subsystems_mcr)
         {
            auto no_mcs = subsystem_mcr.Size() - I_1;
            subsystems_no_mcs.push_back(no_mcs);
         }
         mDincrInfo.InitializeMcSubsystemConv(subsystems_no_mcs);
      }
     
      // Calculate the 1-mode surface(s)
      Run1ModeSurface
         (  ilevel
         ,  grid_fractions
         ,  max_mcl
         ,  subsystems_mcr
         ,  subsets_iter_grids
         ,  skip_mc_fit
         ,  subsystem_derivatives
         );
      
      // Check that all 1-mode grids are converged before proceeding to higher order grids
      auto run_higher_order_grids = true;
      for (const auto& iter_grids : subsets_iter_grids)
      {
         if (!iter_grids.Is1dConv())
         {
            run_higher_order_grids = false;
            break;
         }
      }

      // Calculate the n-mode surface(s) in a cascade
      if (run_higher_order_grids)
      {
         RunNModeSurfaces
            (  ilevel
            ,  grid_fractions
            ,  max_mcl
            ,  subsystems_mcr
            ,  subsets_iter_grids
            ,  skip_mc_fit
            ,  subsystem_derivatives
            );
      }

      // Adga calculation footer
      Mout << "\n\n";
      Out72Char(Mout,'$','$','$');
      Out72Char(Mout,'$',' ','$');
      OneArgOut72(Mout," Adaptive Density-Guided Approach for surface(s) level " + std::to_string(ilevel) + ": Completed",'$');
      Out72Char(Mout,'$',' ','$');
      Out72Char(Mout,'$','$','$');

      //
      if (gPesCalcDef.GetmMakeCutAnalysis().size())
      {
         for (const auto& pes_info : mPesInfos)
         {
            MakeCutAna(pes_info);
         }
      }
   }
   
   // Multilevel calculation footer
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Multilevel Procedure for PES: Completed ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;


   // Save important files and prepare for vibrational calculations when the multilevel calculation is completed
   if (gPesCalcDef.MultiLevel())
   {
      // Merge operators files from the different multilevels together and save important files generated in the multilevel calculation to the FinalSurfaces directory for all subsystems
      FinalizeMultilevelSurface();
   
      // Need to copy files before commencing with the Vscf calcuation
      std::string save_dir;
      std::string analysis_dir;
      if (gPesCalcDef.GetmDincrSurface())
      {
         save_dir = mPesMainDir + "/FinalSurfaces/savedir";
         analysis_dir = mPesMainDir + "/FinalSurfaces/analysis";
      }
      else
      {
         if (mPesInfos.size() != I_1)
         {
            MIDASERROR("Found multiple subsystems but no indication of double incremental procedure!");
         }
 
         save_dir = mPesInfos[I_0].GetMultiLevelInfo().SaveIoDir();
         analysis_dir = mPesInfos[I_0].GetMultiLevelInfo().AnalysisDir();
         
         save_dir = mPesInfos[I_0].GetmSubSystemDir() + "/FinalSurfaces/savedir";
         analysis_dir = mPesInfos[I_0].GetmSubSystemDir() + "/FinalSurfaces/analysis";
      }
 
      // Copy all relevant operator files
      for (In istate = I_0; istate < mAdgaVscfCalcDef.size(); ++istate)
      {
         midas::filesystem::Copy
            (  save_dir + "/prop_no_" + std::to_string(istate + I_1) + ".mop"
            ,  mPesMainDir + "/VibCalc/savedir/prop_no_" + std::to_string(istate + I_1) + ".mop"
            );
      }
 
      // Copy the one-mode potential grids bounds file
      midas::filesystem::Copy
         (  analysis_dir + "/one_mode_grids.mbounds"
         ,  mPesMainDir + "/VibCalc/analysis/one_mode_grids.mbounds"
         );

      // Prepare for a final VSCF calculation and other possible post-surface generation vibrational calculations
      if (gPesIoLevel > I_4)
      {
         Mout << " Rereading the operator file in preparation for post-ADGA vibrational structure calculation " << std::endl;
      }

      { // Make sure mpi_region_guard goes out of scope
         auto mpi_region_guard = midas::mpi::StartMpiRegion();
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::pes::mpi_impl::signal::ADGAFINALIZE);

         // Read in the complete operator for use in later possible vibrational calculations (master)
         for (const auto& vscf_calc_def : gVscfCalcDef)
         {
            const auto& oper_name = vscf_calc_def.Oper();
            const auto& i_oper    = gOperatorDefs.GetOperatorNr(oper_name);
            gOperatorDefs[i_oper].ReReadIn();
         }
      } //
   
   }

   // If requested save trainings data required for ML algorithm(s)
   if (gPesCalcDef.GetmSaveTrainData() && gPesCalcDef.GetmSaveDispGeom())
   {
      Trainer trainer(gPesCalcDef.GetmMLDriver(), mPesMainDir);

      for (In ilevel = 1; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
      {
         // 
         for (auto& pes_info : mPesInfos)
         {
            pes_info.Update(gPesCalcDef, ilevel);

            In number_of_prop = pes_info.GetMultiLevelInfo().GetNoOfProps();
            std::string SaveIoDir = pes_info.GetMultiLevelInfo().SaveIoDir();
            std::string AnalysisDir = pes_info.GetMultiLevelInfo().AnalysisDir();
          
            //
            for (In iprop = I_1; iprop <= number_of_prop; iprop++)
            {
               if (gPesIoLevel > I_3)
               {
                  Mout << "Do GPR training for property " << iprop << std::endl;
               }
          
               // Generate training data for each level
               trainer.DumpTrainingData(iprop, -1, false, false, 1, SaveIoDir, AnalysisDir); 

               // Do an initial hyper parameter optimization and save Co-Variance matrix
               trainer.TrainGPR(iprop, 0, false, false, SaveIoDir, AnalysisDir);
            }
         }
      }
   }
}

/** 
 * Initialize the multilevel by determining if derivative information is to be used and check that the multilevel is not previously converged.
 *
 * @param aLevel              The current multilevel
 * @param aNrMc               The maximal level of mode-coupling for the multilevel aLevel
 * @param arDerivatives       Container for derivative information for individual subsystems
 * @param arSubsetsIterGrids  Container for individual subsystem iterative grids
 **/
void AdgaSurface::InitLevel
   (  const In& aLevel
   ,  const In& aNrMc
   ,  std::vector<Derivatives>& arDerivatives
   ,  std::vector<ItGridHandler>& arSubsetsIterGrids
   )
{
   // Provide PesInfo's with derivative information
   for (auto& pes_info : mPesInfos)
   {
      // Initialize the container for property/properties with derivative/derivatives
      pes_info.CleanmPropWithDer();
  
      // Look for derivative information associated to the current multilevel 
      pes_info.SetmUseDerivatives(false);

      auto no_props = pes_info.GetMultiLevelInfo().PropInfo().Size();
      for (In prop = I_0; prop < no_props; ++prop)
      {
         auto der_file = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDerFile();
         if (!der_file.empty())
         {
            auto prop_label = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDescriptor();
 
            pes_info.SetmUseDerivatives(true);
            pes_info.SetmPropWithDer(prop_label);
         
            if (gDebug || gPesIoLevel > I_8)
            {
               Mout << " Derivative information on " << prop_label << " is available for level_" << aLevel << std::endl;
            }
         }
         else
         {
            if (gDebug || gPesIoLevel > I_8)
            {
               auto prop_label = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDescriptor();
               Mout << " Derivative information on " << prop_label << " is not available for level_" << aLevel << std::endl;
            }
         }

         // 
         Derivatives subsystem_derivatives;
         arDerivatives.emplace_back(std::move(subsystem_derivatives));
      }
 
      // Do some checking if derivative input makes sense
      if (pes_info.GetmUseDerivatives())
      {
         if (!gPesCalcDef.GetmPesDoExtrap() && !gPesCalcDef.GetmPesShepardInt() && !gPesCalcDef.GetMLPes())
         {
            MIDASERROR("Found derivative information on property file but have not received information on how to use it");
         }
      }

      // Set the mode combination level from which to start the extrapolation procedure
      if (pes_info.GetmUseDerivatives() && !gPesCalcDef.GetMLPes())
      {
         pes_info.SetExtrapFrom(aNrMc);
      }
   }

   // Clean the list of singlepoint calculations, and check for already calculated points for this level.
   for (auto& calculation_list : mCalculationLists)
   {
      calculation_list.Clean();
      calculation_list.CheckForCalcsDone();
   }

   // Initialize the convergence status of the iterative grids
   for (auto& iter_grid : arSubsetsIterGrids)
   {
      iter_grid.SetConv1d(false);
   }
}

/**
 *
 **/
void AdgaSurface::Get1ModeAnalyticalSurfaces
   (  GridType& arGridFractions
   ,  const In& arMaxMcl
   ,  const ModeCombiOpRange& arMcr
   ,  ItGridHandler& arItGridHandler
   ,  const std::vector<bool>& arSkipMcInFit
   ,  Derivatives& arDerivatives
   ,  const In& arCurrentMcl
   ,  const In& arCurrentSubsystem
   )
{
   // If derivative informaiton have been calculated then this is stored for later use
   if (mPesInfos[arCurrentSubsystem].GetmUseDerivatives())
   {
      if (gPesCalcDef.GetmPesDoExtrap() || (gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd() > I_0))
      {
         // Reads derivative (bar-)information from file and store this in the derivative container
         FillDerContainer(arDerivatives, mCalculationLists[arCurrentSubsystem], mPesInfos[arCurrentSubsystem]);
      }
   }
   
   if (gPesCalcDef.GetmPesGrid() && gPesIoLevel > I_10) 
   {
      Mout << std::endl << " **** Linear Combination For " << "The Transformed Potentials **** " << std::endl << std::endl;
   }
   
   // DoDispDer_zero will compile the list of calculations to be done and list of calculations to be extrapolated the lists have been parsed searching for symmetry dependent point
   mBarFiles[arCurrentSubsystem]->DoDispDer_one(arMcr, arMaxMcl, arGridFractions, arDerivatives);
   
   // Calculate the Extrapolated points
   if (mPesInfos[arCurrentSubsystem].GetmUseDerivatives() && gPesCalcDef.GetmPesDoExtrap())
   {
      mBarFiles[arCurrentSubsystem]->Extrapolation(arDerivatives, mCalculationLists[arCurrentSubsystem], mPesInfos[arCurrentSubsystem].GetmPropWithDer());
   }
  
   // Output
   if (gPesIoLevel > I_4)
   {
      if (gPesCalcDef.GetmPesDoInt() || gPesCalcDef.GetmPesDoIntForExtrap())
      {
         Mout << std::endl << " Coarse grid has been generated, will proceed to do interpolation and fitting " << std::endl;
      }
      else
      {
         Mout << std::endl << " Coarse grid has been generated, will proceed to do fitting " << std::endl;
      }
   }
   
   // Determine if modified Shepard interpolation is in use
   bool use_msi_in_adga = true;
   if (!mPesInfos[arCurrentSubsystem].GetmUseDerivatives() || gPesCalcDef.GetMLPes())
   {
      use_msi_in_adga = false;
   }
   if (  gPesCalcDef.GetmPesAdgaMsiCut() 
      && (arItGridHandler.GetCurr1dIter() + I_1 > gPesCalcDef.GetmPesAdgaMsiIt()[I_0])
      )
   {
      use_msi_in_adga = false;
   }
  
   // Output
   if (use_msi_in_adga && gPesIoLevel > I_4)
   {
      Mout << " Will use derivatives for Shepard interpolation " << std::endl;
   }
  
   //
   auto OneModeGridBoundsMap = GetOneModeGridBoundsMap(I_1, arMcr, arItGridHandler);

   // Run the fitting routine and dump the analytical surface(s) as .mop files
   mDoPotentialFits[arCurrentSubsystem].GetAnalyticalSurfaces
      (  arMcr
      ,  arGridFractions
      ,  arDerivatives
      ,  use_msi_in_adga
      ,  OneModeGridBoundsMap
      ,  &arItGridHandler
      ,  mCalculationLists[arCurrentSubsystem]
      ,  arCurrentMcl
      ,  arSkipMcInFit
      );

   //
   if (!gPesCalcDef.GetmDincrSurface())
   {
      RereadProperties(arCurrentSubsystem);
   }
}

/**
 * Clean properties and re-read them.
 *
 **/
void AdgaSurface::RereadProperties
   (  const In& arCurrentSubsystem
   )
{
   // Reread the potentials at MASTER  
   for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
   {
      mProperties[i].CleanAndReread(mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i]) + ".mop",
                                    mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabels());
   }
} 

/**
 * Save a copy of the file containing either mean or maximum vibrational density for each mode.
 **/
void AdgaSurface::SaveMeanDensFiles
   (  const In& arCurrentMcl
   ,  const In& arCurrentAdgaIter
   ,  const In& arCurrentSubsystem
   )  const
{
   for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
   {
      for (In j = I_0; j < mPesInfos[arCurrentSubsystem].GetMolecule().GetNoOfVibs(); ++j)
      { 
         std::string data_file_name_curr;
         std::string data_file_name_iter;
         if (gPesCalcDef.GetmAnalyzeDensMethod() == "MEAN")
         {
            data_file_name_curr  = mPesMainDir + "/VibCalc/analysis/"
                                    +  "MeanDens_Prop" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i])
                                    + "_" + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(j)
                                    + ".mplot";

            data_file_name_iter  = mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().AnalysisDir() + "/" 
                                    + "MeanDens_Prop" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i])
                                    + "_" + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(j)
                                    + "_" + std::to_string(arCurrentMcl) + "m_it" + std::to_string(arCurrentAdgaIter)
                                    + ".mplot";
         }
         else 
         {
             data_file_name_curr  = mPesMainDir + "/VibCalc/analysis/"
                                    +  "MaxDens_Prop" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i])
                                    + "_" + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(j)
                                    + ".mplot";

             data_file_name_iter = mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().AnalysisDir() + "/" 
                                    +  "MaxDens_Prop" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i])
                                    + "_" + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(j)
                                    + "_" + std::to_string(arCurrentMcl) + "m_it" + std::to_string(arCurrentAdgaIter)
                                    + ".mplot";
         }
       
         CopyFile(data_file_name_curr, data_file_name_iter);
      }
   }
}

/**
 * Run 1-mode surface(s) by iteratively constructing a grid of single points
 *
 * @param arMultilevel        Current multilevel 
 * @param arGridFractions     Hold information on the one-mode grids
 * @param arMaxMcl            Maximum mode combination level permitted for the current multilevel
 * @param arSubsystemMcr      Mode combination range for each subsystem
 * @param arSubsystemIterGrids     Container for the iterative grid of single points
 * @param arSkipMcInFit       Decides if a mode combinations is to be fitted in this iteration
 * @param arDerivatives       Container for derivative information
 **/
void AdgaSurface::Run1ModeSurface
   (  const In& arMultilevel
   ,  std::vector<GridType>& arGridFractions
   ,  const In& arMaxMcl
   ,  std::vector<ModeCombiOpRange>& arSubsystemMcr
   ,  std::vector<ItGridHandler>& arSubsystemIterGrids
   ,  std::vector<std::vector<bool> >& arSkipMcInFit
   ,  std::vector<Derivatives>& arDerivatives
   )
{
   // Establish the mode combination level 
   auto i_modes_coup = I_1;

   // Do some output
   Out72Char(Mout,'~','-','~');
   OneArgOut72(Mout, " Starting the ADGA for generating " + std::to_string(i_modes_coup) + "-mode grids ",'~');
   Out72Char(Mout,'~','-','~');

   // Communicate the mode combination level to PesInfo's
   for (auto& pes_info : mPesInfos)
   {
      pes_info.SetMCLevel(i_modes_coup);
   }
   
   // Loop over Adga iterations
   auto iter_max = gPesCalcDef.GetmPesIterMax();
   std::vector<bool> subsystems_surface_converged(mNoSubsystems, false);
   for (In i_t = I_0; i_t <= iter_max; ++i_t)
   {
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // If a subsystem surface is already converged, then we skip to the next
         if (subsystems_surface_converged[isystem])
         {
            continue;
         }

         // Communicate the Adga iteration number to PesInfo's 
         arSubsystemIterGrids[isystem].SetCurr1dIter(i_t);
         mPesInfos[isystem].SetIterNo(i_t);
      
         // Update the one-mode grids with new single point placements determined in previous iteration
         if (i_t > I_0)
         {
            UpdateGridType(i_modes_coup, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem], arGridFractions[isystem]);
         }
      
         // DoDispDer_zero will compile the list of calculations to be done and list of calculations to be extrapolated the lists have been parsed searching for symmetry dependent point. 
         mBarFiles[isystem]->DoDispDer_zero(arSubsystemMcr[isystem], arMaxMcl, arGridFractions[isystem], mCalculationLists[isystem]);
      
         // Some nice output for individual Adga iterations
         PrintAdgaIterInfo(Mout, arMultilevel, i_t, I_1, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
      }
      
      // Run the single points, i.e. generate new molecular structures by displacing along coordinates and calculate the energy and properties via external electronic structure programs
      for (auto& calculation_list : mCalculationLists)
      {
         calculation_list.EvaluateList();
      }

      // Loop over all subsystems and run the fitting routine before storing information needed by the Vscf calculation
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // If a subsystem surface is already converged, then we skip to the next
         if (subsystems_surface_converged[isystem])
         {
            continue;
         }

         // Run fitting routine
         this->Get1ModeAnalyticalSurfaces
            (  arGridFractions[isystem]
            ,  arMaxMcl
            ,  arSubsystemMcr[isystem]
            ,  arSubsystemIterGrids[isystem]
            ,  arSkipMcInFit[isystem]
            ,  arDerivatives[isystem]
            ,  i_modes_coup
            ,  isystem
            );
       
         // Update the 1-mode grids
         if (arSubsystemIterGrids[isystem].GetCurr1dIter() == I_0)
         {
            SetNewGridPoints(i_modes_coup, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem], arSkipMcInFit[isystem]);
         }
         
         // Write the potential into a file for later processing
         SavePotentials(arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem]);
         
         // Dump info on bounds
         DumpOneModeGridBounds(i_modes_coup, arSubsystemMcr[isystem], isystem, arSubsystemIterGrids[isystem]);
      }
   
      // Merge surface(s) together if using double incremental expansion
      if (gPesCalcDef.GetmDincrSurface())
      {
         this->MergeFragmentSurface(); 
      }
   
      // Unless vibrational density is pre-provided then calculate it via Vscf
      if (!gPesCalcDef.GetmMeanDensOnFiles())
      {
         // Call Vscf driver
         this->DoVscf();
      }

      // Loop over all subsystems and read in information from the Vscf calculation before doing integral evaluation and convergence checks
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // Save the files containing mean or max vibrational density for the current Adga iteration
         if (!gPesCalcDef.GetmMeanDensOnFiles())
         {
            this->SaveMeanDensFiles(i_modes_coup, i_t, isystem);
         }

         // Load the files containing mean or max vibrational density into memory and store in ItGridDensity structure
         auto mode_labels = mPesInfos[isystem].GetMolecule().GetModeNames();
         arSubsystemIterGrids[isystem].LoadMeanDensFiles(mPesMainDir + "/VibCalc/analysis", mode_labels);
         
         // Check which mode combinations contribute meaningful to the complete surface
         if (gPesCalcDef.GetmDincrSurface())
         {
            CheckSubsystemMcConv(i_modes_coup, isystem, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
         }
       
         // Calculate the integrals
         CalcInt(i_modes_coup, arSubsystemMcr[isystem], true, isystem, arSubsystemIterGrids[isystem]);
       
         // this bool controls whether or not it is allowed to extend the potential grid from the first or second iteration in the Adga 
         bool do_convergence_check 
            =  ((arSubsystemIterGrids[isystem].GetCurr1dIter() == I_0) && gPesCalcDef.GetmDoExtInFirstIter())
            || (arSubsystemIterGrids[isystem].GetCurr1dIter() > I_0);
       
         // Output
         if (gDebug || gPesIoLevel > I_14)
         {
            Mout << " Check for relative and absolute convergence: " << do_convergence_check << ", at iteration: " << arSubsystemIterGrids[isystem].GetCurr1dIter() << std::endl;
         }
       
         // Check for convergence
         if (do_convergence_check)
         {
            Mout << std::endl;
            Out72Char(Mout,'C','C','C');
            OneArgOut72(Mout," ",'C');
            OneArgOut72(Mout," Checking Adga convergence for 1-mode grids ",'C');
       
            if (gPesIoLevel > I_6)
            {
               OneArgOut72(Mout," (C := converged, + := converged this iteration) ",'C');
               OneArgOut72(Mout," ",'C');
            }
       
            // 1-mode grids converged
            if (CheckConv(i_modes_coup, isystem, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]))
            { 
               OneArgOut72(Mout," ",'C');
               OneArgOut72(Mout," !!! The Adga is converged for 1-mode grids !!! ",'C');
               OneArgOut72(Mout," ",'C');
               Out72Char(Mout,'C','C','C');
               
               // Set overall convergence of 1-mode grids to true
               arSubsystemIterGrids[isystem].SetConv1d(true);

               // Set subsystem convergence
               subsystems_surface_converged[isystem] = true;
            }
            // 1-mode grids NOT converged
            else
            { 
               if (arSubsystemIterGrids[isystem].GetCurr1dIter() < iter_max)
               {   
                  OneArgOut72(Mout," ",'C');
                  OneArgOut72(Mout," The Adga is not yet converged for 1-mode grids ",'C');
                  OneArgOut72(Mout," ",'C');
                  Out72Char(Mout,'C','C','C');
       
                  // If not converged, then the grid is further subdivided and we recalculate the integrals for these new intervals before going to the next iteration
                  SetNewGridPoints(i_modes_coup, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem], arSkipMcInFit[isystem]);
                  CalcInt(i_modes_coup, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem]);
               }
       
               // If the maximum number of Adga iterations have been reached
               if (arSubsystemIterGrids[isystem].GetCurr1dIter() == iter_max)
               {

                  // Set overall convergence of 1-mode grids to false
                  arSubsystemIterGrids[isystem].SetConv1d(false);
      
                  // Throw a warning if we happen upon this case
                  MidasWarning("The Adga did NOT converge for the 1-mode grids");
               }
            }
         }
      }

      // If all subsystem surfaces are converged then stop the generation of 1M grids and do a final Vscf calculation
      if (std::all_of
            (  subsystems_surface_converged.begin()
            ,  subsystems_surface_converged.end()
            ,  [](bool break_adga_iterations){return break_adga_iterations;}
            )
         )
      {
         // Merge surface(s) together if using double incremental expansion
         if (gPesCalcDef.GetmDincrSurface())
         {
            this->MergeFragmentSurface(); 
         }

         // Run final Vscf calculation
         if (!gPesCalcDef.GetmMeanDensOnFiles())
         {
            // Call Vscf driver
            this->DoVscf();
         }
               
         //      
         if (gPesCalcDef.GetmDoAdgaPreScreen())
         {
            for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
            {
               UpdateAdga
                  (  arSubsystemMcr[isystem]
                  ,  I_2
                  ,  gPesCalcDef.GetmPesFlexCoupCalcDef().GetmAdgaPreScreenThr()
                  ,  mPesMainDir + "/VibCalc/analysis"
                  ,  mPesInfos[isystem].GetMultiLevelInfo().AnalysisDir()
                  ,  mPesInfos[isystem].GetMultiLevelInfo().SaveIoDir()
                  );
            }
         }

         //
         break;
      }
         
      // Need to check that the same mode in different fragments have the same potential boundaries when constructing a double incremental surface with multiple subsystems
      if (gPesCalcDef.GetmDincrSurface())
      {
         CheckSubsystemBoundaries(i_modes_coup, arSubsystemMcr, arSubsystemIterGrids, arSkipMcInFit);
      }
   }
}

/**
 * Run n-mode surface(s) by iteratively constructing a grid of single points
 *
 * @param arMultilevel        Current multilevel 
 * @param arGridFractions     Hold information on the one-mode grids
 * @param arMaxMcl            Maximum mode combination level permitted for the current multilevel
 * @param arSubsystemMcr      Mode combination range for each subsystem
 * @param arSubsystemIterGrids     Container for the iterative grid of single points
 * @param arSkipMcInFit       Decides if a mode combinations is to be fitted in this iteration
 * @param arDerivatives       Container for derivative information
 **/
void AdgaSurface::RunNModeSurfaces
   (  const In& arMultilevel
   ,  std::vector<GridType>& arGridFractions
   ,  const In& arMaxMcl
   ,  std::vector<ModeCombiOpRange>& arSubsystemMcr
   ,  std::vector<ItGridHandler>& arSubsystemIterGrids
   ,  std::vector<std::vector<bool> >& arSkipMcInFit
   ,  std::vector<Derivatives>& arDerivatives
   )
{
   // Establish the mode combination level 
   auto n_modes_coup = gPesCalcDef.LevelMc()[arMultilevel];

   // If an extended mode combination range is in use, then the effective level of coupling in the surface is greater than otherwise indicated
   if (arMultilevel != I_0 && gPesCalcDef.GetmUseExtMcr())
   {
      n_modes_coup += gPesCalcDef.GetmExtMcrModes().size();
   }

   // Loop over mode combination levels (starting from 2)
   for (In i_mode_coup = I_2; i_mode_coup <= n_modes_coup; ++i_mode_coup)
   {
      // Do some output
      Mout << std::endl;
      Out72Char(Mout,'~','-','~');
      OneArgOut72(Mout, " Starting the ADGA for generating " + std::to_string(i_mode_coup) + "-mode grids ", '~');
      Out72Char(Mout,'~','-','~');
      Mout << std::endl;
     
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // Communicate the mode combination level to PesInfo's
         mPesInfos[isystem].SetMCLevel(i_mode_coup);

         // Set the mode combination level from which to start the extrapolation procedure
         if (mPesInfos[isystem].GetmUseDerivatives() && !gPesCalcDef.GetMLPes())   
         {
            mPesInfos[isystem].SetExtrapFrom(i_mode_coup - I_1);
         }
      
         // Update the n-mode grids with new single point placements determined in previous iteration
         UpdateGridType(i_mode_coup, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem], arGridFractions[isystem]); 
      
         // Initialize the n-mode grids
         InitializeIterGrid(i_mode_coup, arSubsystemMcr[isystem], arGridFractions[isystem], isystem, arSubsystemIterGrids[isystem]);
      
         // Initialize iterative n-mode grids to not converged
         arSubsystemIterGrids[isystem].SetConvNd(false);
      }

      // Loop over Adga iterations
      std::vector<bool> subsystems_surface_converged(mNoSubsystems, false);
      In iter_max = gPesCalcDef.GetmPesIterMax();
      for (In i_t = -I_1; i_t <= iter_max; ++i_t)
      {
         // Indicate to the Adga that extrapolation should be carried out at this iteration
         auto do_extrapolation = false;

         // Loop over all subsystems and generate new single points
         for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
         {
            // If a subsystem surface is already converged, then we skip to the next
            if (subsystems_surface_converged[isystem])
            {
               continue;
            }

            // The -1 iteration is reseved for doing extrapolation but if this is not required, then we skip to the next and begin the standard construction of higher order grids
            if (mPesInfos[isystem].GetmUseDerivatives() && i_t == -I_1 && !gPesCalcDef.GetMLPes())
            {
               if (gPesIoLevel > I_7)
               {
                  Mout << std::endl << " Derivative information is used to generate extrapolated surface " << std::endl;
               }
         
               do_extrapolation = true;
               SetExtrap(i_mode_coup, arSubsystemMcr[isystem], true);
            }
            
            // Set the current iteration number
            if (  (do_extrapolation && i_t == -I_1)
               || (!do_extrapolation && i_t > -I_1)
               )
            {
               // Set the current iteration number
               arSubsystemIterGrids[isystem].SetCurrNdIter(i_t);
               mPesInfos[isystem].SetIterNo(i_t);
          
               // Update the n-mode grids with new single point placements determined in previous iteration
               if (gPesCalcDef.GetmDoAdgaExtAtEachMcl() && i_t > I_0)
               {
                  UpdateGridType(i_mode_coup, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem], arGridFractions[isystem]);
               }
          
               // DoDispDer_zero will compile the list of calculations to be done and list of calculations to be extrapolated the lists have been parsed searching for symmetry dependent point
               mBarFiles[isystem]->DoDispDer_zero(arSubsystemMcr[isystem], arMaxMcl, arGridFractions[isystem], mCalculationLists[isystem]);
          
               // Output for individual Adga iterations
               PrintAdgaIterInfo(Mout, arMultilevel, i_t, i_mode_coup, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
            }
         }

         // If no extrapolation is required, then we continue to the next Adga iteration
         if (!do_extrapolation && i_t == -I_1)
         {
            continue;
         }
         
         // Run the single points, i.e. generate new molecular structures by displacing along coordinates and calculate the energy and properties via external electronic structure programs
         for (auto& calculation_list : mCalculationLists)
         {
            calculation_list.EvaluateList();
         }

         // Loop over all subsystems and run the fitting routine, Vscf calculation, integral evaluation and convergence checks
         for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
         {
            // If a subsystem surface is already converged, then we skip to the next
            if (subsystems_surface_converged[isystem])
            {
               continue;
            }

            // If derivative informaiton have been calculated then this is stored for later use
            if (mPesInfos[isystem].GetmUseDerivatives())
            {
               if (  gPesCalcDef.GetmPesDoExtrap() 
                  || (gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd() > I_0)
                  )
               {
                  // Reads derivative (bar-)information from file and store this in the derivative container
                  FillDerContainer(arDerivatives[isystem], mCalculationLists[isystem], mPesInfos[isystem]);
               }
            }
          
            if (gPesCalcDef.GetmPesGrid() && gPesIoLevel > I_10)
            {
               Mout << std::endl << " **** Linear Combination For The Transformed Potentials **** " << std::endl << std::endl;
            }
          
            // Calculate the bar-potentials
            mBarFiles[isystem]->DoDispDer_one(arSubsystemMcr[isystem], arMaxMcl, arGridFractions[isystem], arDerivatives[isystem]);
          
            // Calculate the extrapolated points
            if (mPesInfos[isystem].GetmUseDerivatives() && gPesCalcDef.GetmPesDoExtrap())
            {
               mBarFiles[isystem]->Extrapolation(arDerivatives[isystem], mCalculationLists[isystem], mPesInfos[isystem].GetmPropWithDer());
            }
          
            // Output
            if (gPesIoLevel > I_4)
            {
               if (gPesCalcDef.GetmPesDoInt() || gPesCalcDef.GetmPesDoIntForExtrap())
               {
                  Mout << std::endl << " Coarse grid has been generated, will proceed to do interpolation and fitting " << std::endl;
               }
               else
               {
                  Mout << std::endl << " Coarse grid has been generated, will proceed to do fitting " << std::endl;
               }
            }
          
            // Determine if modified Shepard interpolation is in use
            bool use_msi_in_adga = true;
            if (!mPesInfos[isystem].GetmUseDerivatives() || gPesCalcDef.GetMLPes())
            {
               use_msi_in_adga = false;
            }
            if (  gPesCalcDef.GetmPesAdgaMsiCut()
               && (arSubsystemIterGrids[isystem].GetCurrNdIter() + I_1 > gPesCalcDef.GetmPesAdgaMsiIt()[i_mode_coup - I_1])
               )
            {
               use_msi_in_adga = false;
            }
          
            // Output
            if (use_msi_in_adga && gPesIoLevel > I_4)
            {
               Mout << " Will use derivatives for Shepard interpolation " << std::endl;
            }
            else
            {
               Mout << std::endl;
            }
            
            // Update the n-mode grids
            if (arSubsystemIterGrids[isystem].GetCurrNdIter() == I_0)
            {
               SetNewGridPoints(i_mode_coup, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem], arSkipMcInFit[isystem]);
            }
            
            //
            auto OneModeGridBoundsMap = GetOneModeGridBoundsMap(I_1, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
            
            // Run the fitting routine and dump the analytical surface(s) as .mop files
            mDoPotentialFits[isystem].GetAnalyticalSurfaces
               (  arSubsystemMcr[isystem]
               ,  arGridFractions[isystem]
               ,  arDerivatives[isystem]
               ,  use_msi_in_adga
               ,  OneModeGridBoundsMap
               ,  &arSubsystemIterGrids[isystem]
               ,  mCalculationLists[isystem]
               ,  i_mode_coup
               ,  arSkipMcInFit[isystem]
               );
          
            //
            if (!gPesCalcDef.GetmDincrSurface())
            {
               RereadProperties(isystem);
            }
          
            // Save the 2-mode potentials into a file for later processing:
            if (gPesCalcDef.GetmPesPlotAdga2D())
            {
               if (i_mode_coup == I_2 && !gPesCalcDef.GetmCalcDensAtEachMcl() && !gPesCalcDef.GetmDoAdgaExtAtEachMcl())
               {
                  SavePotentials(arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem]);
               }
               else if (i_mode_coup < I_3 && (gPesCalcDef.GetmCalcDensAtEachMcl() || gPesCalcDef.GetmDoAdgaExtAtEachMcl()))
               {
                  SavePotentials(arSubsystemMcr[isystem], true, isystem, arSubsystemIterGrids[isystem]);
               }
            }
          
            // Dump info on bounds
            if (gPesCalcDef.GetmDoAdgaExtAtEachMcl() && i_t > I_0)
            {
               DumpOneModeGridBounds(I_1, arSubsystemMcr[isystem], isystem, arSubsystemIterGrids[isystem]);
            }
         }
      
         // Merge surface(s) together if using double incremental expansion
         if (gPesCalcDef.GetmDincrSurface())
         {
            this->MergeFragmentSurface(); 
         }
            
         // Optional recalculation of vibrational density via Vscf if not pre-provided
         if (!gPesCalcDef.GetmMeanDensOnFiles() && gPesCalcDef.GetmCalcDensAtEachMcl())
         {
            // Call Vscf driver
            this->DoVscf();
         }
         
         // Loop over all subsystems and read in information from the Vscf calculation before doing integral evaluation and convergence checks
         for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
         {
            // The first iteration of nM surfaces contains few points, which makes for a poor starting point for a VSCF calculation
            if (gPesCalcDef.GetmUpdMeanDensAtEachMcl() && i_t > I_0)
            {
               // Save the files containing mean or max vibrational density for the current Adga iteration
               if (!gPesCalcDef.GetmMeanDensOnFiles())
               {
                  this->SaveMeanDensFiles(i_mode_coup, i_t, isystem);
               }
 
               // Load the files containing mean or max vibrational density into memory and store in ItGridDensity structure
               auto mode_labels = mPesInfos[isystem].GetMolecule().GetModeNames();
               arSubsystemIterGrids[isystem].LoadMeanDensFiles(mPesMainDir + "/VibCalc/analysis", mode_labels);
            }
          
            // If the mc are only extrapolated we stop here
            if (arSubsystemIterGrids[isystem].GetCurrNdIter() == -I_1)
            {
               Mout << " Extrapolation procedure is completed! " << std::endl; 
          
               // If extrapolation is used as start guess for coupled surfaces, then we extrapolate and continue the calculation of the coupled surface
               if (  i_mode_coup == gPesCalcDef.GetmExtrapolateTo()
                  && gPesCalcDef.GetmUseExtrapStartGuess()
                  )
               {
                  SetExtrap(i_mode_coup, arSubsystemMcr[isystem], false);
                  
                  // The info used in the extrapolation will be saved
                  ResetIterGrid(i_mode_coup, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
                  if (mPesInfos[isystem].GetmUseDerivatives() && !gPesCalcDef.GetMLPes())
                  {
                     mPesInfos[isystem].SetExtrapFrom(i_mode_coup);
                  }
               }
               // If extrapolation is the last step in the multilevel calculation then we stop
               else
               {
                  subsystems_surface_converged[isystem] = true;
                  continue;
               }
            }
            
            // Check which mode combinations contribute meaningful to the complete surface
            if (gPesCalcDef.GetmDincrSurface())
            {
               CheckSubsystemMcConv(i_mode_coup, isystem, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]);
            }
          
            // Calculate the integrals
            CalcInt(i_mode_coup, arSubsystemMcr[isystem], true, isystem, arSubsystemIterGrids[isystem]);
          
            // We check the convergence already at iteration zero to avoid calculating redudant points for nM surfaces, note that the convergence check will be done with "previous values" in the convergence criteria set to zero, i.e. all we do is check for a non-vanishing absolute contribution to the integration interval
            // Note that we do not check for convergence at iteration -1, which corresponds to the use of extrapolation from a supposedly already convergence surface
            if (arSubsystemIterGrids[isystem].GetCurrNdIter() >= I_0)
            {
               Mout << std::endl;
               Out72Char(Mout,'C','C','C');
               OneArgOut72(Mout," ",'C');
               OneArgOut72(Mout," Checking Adga convergence for " + std::to_string(i_mode_coup) + "-mode grids ",'C');
          
               if (gPesIoLevel > I_6)
               {
                  OneArgOut72(Mout," (C := converged, + := converged this iteration) ",'C');
                  OneArgOut72(Mout," ",'C');
               }
          
               if (CheckConv(i_mode_coup, isystem, arSubsystemMcr[isystem], arSubsystemIterGrids[isystem]))
               {
                  OneArgOut72(Mout," ",'C');
                  OneArgOut72(Mout," !!! The Adga is converged for " + std::to_string(i_mode_coup) + "-mode grids !!! ",'C');
                  OneArgOut72(Mout," ",'C');
                  Out72Char(Mout,'C','C','C');
                  
                  // Set overall convergence of 1-mode grids to true
                  arSubsystemIterGrids[isystem].SetConvNd(true);
   
                  // Set subsystem convergence
                  subsystems_surface_converged[isystem] = true;
               }
               else
               {
                  OneArgOut72(Mout," ",'C');
                  OneArgOut72(Mout," The Adga is not yet converged for " + std::to_string(i_mode_coup) + "-mode grids ",'C');
                  OneArgOut72(Mout," ",'C');
                  Out72Char(Mout,'C','C','C');
          
                  // If not converged, then the grid is further subdivided and we recalculate the integrals for these new intervals before going to the next iteration
                  SetNewGridPoints(i_mode_coup, arSubsystemMcr[isystem], true, isystem, arSubsystemIterGrids[isystem], arSkipMcInFit[isystem]);
                  CalcInt(i_mode_coup, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem]);
            
                  // If the maximum number of Adga iterations have been reached
                  if (arSubsystemIterGrids[isystem].GetCurr1dIter() == iter_max)
                  {
                     // Set overall convergence of M-mode grids to false
                     arSubsystemIterGrids[isystem].SetConvNd(false);
   
                     // Throw a warning if we happen upon this case
                     MidasWarning("The Adga did NOT converge for the n-mode grids");
                  }
               }
            }
         }
  
         // If all subsystem surfaces are converged then stop the generation of  the nM grids
         if (std::all_of
               (  subsystems_surface_converged.begin()
               ,  subsystems_surface_converged.end()
               ,  [](bool break_adga_iterations){return break_adga_iterations;}
               )
            )
         {
            // Merge surface(s) together if using double incremental expansion
            if (gPesCalcDef.GetmDincrSurface())
            {
               this->MergeFragmentSurface(); 
            }

            // Run final Vscf calculation
            if (gPesCalcDef.GetmUpdMeanDensAtEachMcl())
            {
               // Call Vscf driver
               this->DoVscf();
            }
     
            //
            if (gPesCalcDef.GetmDoAdgaPreScreen())
            {
               for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
               {
                  UpdateAdga
                     (  arSubsystemMcr[isystem]
                     ,  i_mode_coup + I_1
                     ,  gPesCalcDef.GetmPesFlexCoupCalcDef().GetmAdgaPreScreenThr()
                     ,  mPesMainDir + "/VibCalc/analysis"
                     ,  mPesInfos[isystem].GetMultiLevelInfo().AnalysisDir() 
                     ,  mPesInfos[isystem].GetMultiLevelInfo().SaveIoDir()
                     );
               }
            }

            //
            break;
         }
      
         // Need to check that the same mode in different fragments have the same potential boundaries when constructing a double incremental surface with multiple subsystems
         if (gPesCalcDef.GetmDincrSurface() && gPesCalcDef.GetmUpdMeanDensAtEachMcl())
         {
            CheckSubsystemBoundaries(i_mode_coup, arSubsystemMcr, arSubsystemIterGrids, arSkipMcInFit);
         }
      }
   }
}

/**
 * Check convergency of the Adga procedure for the 2-mode grids
 *
 * @param aNmodes (INPUT) mode combination level to check for convergence (e.g. aNmodes = 2, check all 2-mode grids)
 * @param arSubsystem         The subsystem for which convergence is being checked 
 * @param arModeCombiOpRange (INPUT) the mode combination range to check
 * @param arItGridHandler
 *
 * @return conv is the MC level converged? 
 **/
bool AdgaSurface::CheckConv
   (  const In& aNmodes
   ,  const In& arSubsystem
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  ItGridHandler& arItGridHandler
   )  const
{
   // Assume converged, and set to false later if this is not the case
   bool conv = true; 
  
   // Counters for the number of converged/unconverged mode combinations at this iteration
   Nb mcs_conv = I_0;
   Nb mcs_nonconv = I_0;

   // Loop over mode combinations
   for (const auto& mt: arModeCombiOpRange)
   {
      std::vector<In> curr_mode = mt.MCVec();
     
      if (curr_mode.size() == I_0)
      {
         continue;
      }

      if (curr_mode.size() != aNmodes)
      {
         if (curr_mode.size() < aNmodes)
         {
            // If we have the possibility of extending the grid at any given mode combination level, then we need to check convergence for lower order mode combinations as well
            if (curr_mode.size() == I_1 && gPesCalcDef.GetmDoAdgaExtAtEachMcl())
            {
               if (gPesIoLevel > I_6)
               {
                  std::stringstream ext_mode;
                  ext_mode << curr_mode[I_0];
                  OneArgOut72(Mout, " Check extension for mode (" + ext_mode.str() + ")", 'C');
               }
            }
            else
            {
               // We have still not reached the MCL and continue
               continue;
            }
         }
         else if (curr_mode.size() > aNmodes)
         {
            // We have gone beyond the MCL and break
            break; 
         }
      }

      //
      auto add = mt.Address();
      auto check_mc_conv = true;
      auto mode_conv = true;
        
      // If the mode combination contain an inter-connecting mode then we do not run convergence checks for it
      if (gPesCalcDef.GetmDincrSurface())
      {
         check_mc_conv = !(mDincrInfo.GetmMcSubsystemConv(arSubsystem, add - I_1));
      }

      // Check if current mode combination is converged
      if (check_mc_conv)
      {
         mode_conv = arItGridHandler.CheckConvForMc(add, curr_mode);
      }
         
      // If one mode is not converged, ADGA is not yet converged
      if (!mode_conv)
      {
         conv = false; 
         ++mcs_nonconv;
      }
      else
      {
         ++mcs_conv;
      }
   }

   std::stringstream mcs_conv_out;
   std::stringstream mcs_nonconv_out;
   mcs_conv_out << std::fixed << std::setprecision(0) << mcs_conv;
   mcs_nonconv_out << std::fixed << std::setprecision(0) << mcs_nonconv;

   OneArgOut72(Mout, "", 'C');
   OneArgOut72(Mout, " Number of converged MCs    : " + mcs_conv_out.str(), 'C');
   OneArgOut72(Mout, " Number of non-converged MCs: " + mcs_nonconv_out.str(), 'C');

   return conv;
}

/**
 * Initialize the dynamical grid
 *    if 1d grids are not converged we set-up all ItGrid for all MCs
 *    if 1d grids are converged and Nd grids are not converged we update Nd ItGrid
 * @param aNmodes (INPUT) number of mode couplings in grid
 * @param arModeCombiOpRange
 * @param arGridType
 * @param arCurrentSubsystem
 * @param arItGridHandler
 * */
void AdgaSurface::InitializeIterGrid
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const GridType& arGridType
   ,  const In& arCurrentSubsystem
   ,  ItGridHandler& arItGridHandler
   )  const
{
   // Check if we are doing 1-mode grids
   if (!arItGridHandler.Is1dConv())
   {
      // Retrieve the scaling factors
      auto scal_facts = std::make_pair(mPesInfos[arCurrentSubsystem].GetScalingInfo().GetLScalFactVector(), mPesInfos[arCurrentSubsystem].GetScalingInfo().GetRScalFactVector());
      
      if (gPesIoLevel > I_9)
      {
         auto no_scal_facts = I_0;
         if (scal_facts.first.size() == scal_facts.second.size())
         {
            no_scal_facts = scal_facts.first.size();
         }

         for (In i = I_0; i < no_scal_facts; i++)
         {
            Mout << " Grid scale factors for mode Q" << i << " set to: " << scal_facts.first[i] << ", " << scal_facts.second[i] << std::endl;
         }
      }
      
      // Loop over mode combinations
      for (const auto& mt: arModeCombiOpRange)
      {
         if (mt.Size() != I_0)
         {
             arItGridHandler.InsertModeCombiGrid(mt, arGridType, scal_facts, mProperties.size());
         }
      }
   }
   // The one mode grids are converged, set the starting points for mode couplings
   else
   { 
      // Loop over mode combis
      for (const auto& mt: arModeCombiOpRange)
      {
         if (mt.Size() != aNmodes) 
         {
            continue;  // continue, if MC is not the requested level we skip it
         }
         InVector curr_mode = mt.MCVec();
         In add = mt.Address();

         // Extrapolate MC
         if (  (gPesCalcDef.GetmPesDoExtrap() && mt.IsToBeExtrap()) 
            || (mPesInfos[arCurrentSubsystem].GetmUseDerivatives() && !gPesCalcDef.GetMLPes()) 
            )
         { 
            if (gPesIoLevel > I_11)
            {
               Mout << " Grid re-initialization for mode combi " << mt << " add: " << add << std::endl;
               Mout << " To Be Extrapolated " << std::endl;
            }

            std::vector<std::vector<In> > mc_minus;
            mc_minus.resize(mt.Size());
            for (In i=I_0; i < mc_minus.size(); ++i)
            {
               mc_minus[i].resize(mt.Size() - I_1);
            }
            std::vector<In> mc_adresses(mt.Size());
            Deconstruct(mt, mc_minus, mc_adresses, arModeCombiOpRange);

            if (gPesIoLevel > I_7)
            {
               Mout << " Deconstruction of mode coupling " << curr_mode << " into: " << std::endl;
            
               for (In i = I_0; i < mc_minus.size(); ++i)
               {
                  Mout << "  Modes: " <<  mc_minus[i] << std::endl;
               }
            }

            //
            ItGrid& mode_grid = arItGridHandler.ModeCombiGrid(add);

            //
            for (In imode=I_0; imode< mt.Size(); ++imode)
            {
               In the_mode = curr_mode[imode];

               // +I_1 to match the address of the gridtype.
               const ItGrid& oned_grid = arItGridHandler.ModeCombiGrid(the_mode + I_1);
               
               mode_grid.SetmQScales(imode, oned_grid.GetQlScal(), oned_grid.GetQrScal());

               InVector candidates;
               In got = I_0;

               for (In isearch=I_0; isearch < mt.Size(); ++isearch) // for each of the parental mc
               {
                  for (In i = I_0; i < mt.Size()-I_1; ++i)    // for each of the mode in the paretal
                  {
                     if (mc_minus[isearch][i] == the_mode)
                     {
                        ++got;
                        const ItGrid& parental_grid = arItGridHandler.ModeCombiGrid(mc_adresses[isearch]);

                        std::vector<In> bounds_found;
                        parental_grid.GetBounds(i, bounds_found);
                        if (got == I_1)     // the first time a match is found
                        {
                           for (In j=I_0; j< bounds_found.size(); ++j) candidates.push_back(bounds_found[j]);
                        }
                        else   // the intersection of the 2 vector has to be taken
                        {
                           InVector tmp_v = candidates ;
                           candidates.clear();
                           for (In j=I_0; j< tmp_v.size(); ++j)
                           {
                              vector<In>::iterator Vii=find(bounds_found.begin(),bounds_found.end(),tmp_v[j]);
                              if (Vii!=bounds_found.end())
                              {
                                 candidates.push_back(tmp_v[j]);
                              }
                           }
                        }
                     }
                  }
               }
               if (got != mt.Size() - I_1)
               {
                  MIDASERROR(" Something is wrong in the deconstruction ");
               }
               mode_grid.SetBounds(imode, candidates);
               if (gPesIoLevel > I_11)
               {
                  Mout << " mode:" << the_mode << " intersection " << candidates << std::endl;
               }
            }
            if (gPesIoLevel > I_11)
            {
               Mout << " Vector of grid point constructed: " << std::endl;
               Mout << mode_grid << std::endl;
            }
         }
         // No extrapolation
         else
         { 
            // N-d mode grids are constructed above, if 1D grids are not converged
            ItGrid& mode_grid = arItGridHandler.ModeCombiGrid(add);

            if (gPesIoLevel > I_11)
            {
               Mout << " Grid re-initialization for mode combi " << mt << " add: " << add << std::endl;
            }
            for (In imt = I_0; imt<mt.Size(); ++imt)
            {
               //+I_1 to match the address of the gridtype.
               const ItGrid& oned_grid = arItGridHandler.ModeCombiGrid(curr_mode[imt] + I_1);
               
               if (gPesIoLevel > I_11)
               {
                  Mout << " Retrieving grid for address: " << curr_mode[imt]+I_1 << std::endl;
               }
               mode_grid.SetmQScales(imt, oned_grid.GetQlScal(), oned_grid.GetQrScal());
               In l_bnd = oned_grid.GetLbound();
               In r_bnd = oned_grid.GetRbound();
               if (gPesIoLevel > I_11)
               {
                  Mout << " left bound: " << l_bnd << " right bound: " << r_bnd << std::endl;
               }

               mode_grid.SetBounds(imt, l_bnd, r_bnd, mt.Size());
            }
            if (gPesIoLevel > I_11)
            {
               Mout << " Vector of grid point constructed: " << std::endl;
               Mout << mode_grid << std::endl;
            }
         }
      }
   }
}

/**
 * Find all subsets available in the current mode combination range for a given mode combination and store these in a vector with an additional vector collecting the corresponding positions in the mode combination range
 *
 * @param arModeCombi            The mode combination for which subsets are to be found
 * @param arModeCombiOpRange     The complete mode combination range 
 * @param arMcSubsets            Container for the subsets of arModeCombi
 * @param arMcSubsetsPosInMcr    Container for the subsets of arModeCombi positions in arModeCombiOpRange 
 *
**/
void AdgaSurface::FindSubsetsOfMc
   (  const ModeCombi& arModeCombi
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  std::vector<std::vector<In> >& arMcSubsets
   ,  std::vector<In>& arMcSubsetsPosInMcr
   )  const
{
   InVector parental_mc_vec = arModeCombi.MCVec();

   for (const auto& curr_mc : arModeCombiOpRange)
   {
      InVector curr_mc_vec = curr_mc.MCVec();
      if (curr_mc_vec.size() == I_0)
      {
         continue;
      }
      if (curr_mc_vec.size() == parental_mc_vec.size())
      {
         break;
      }

      bool found_curr_mc_in_parental = false;
      for (In a = I_0; a < curr_mc_vec.size(); a++) 
      {
         for (In b = I_0; b < parental_mc_vec.size(); b++)
         {
            if (curr_mc_vec[a] == parental_mc_vec[b])
            {
               found_curr_mc_in_parental = true;
            }
         }
      }
      if (found_curr_mc_in_parental)
      {
         arMcSubsets.emplace_back(curr_mc_vec);
         arMcSubsetsPosInMcr.emplace_back(curr_mc.Address());
      }
   }

   return;
}

/**
 * Find 'sub partitions' of mode combination (function does not, as the name suggests, deconstruct/destroy anything...)
 *    example: MC = (1,2,3) -> aMcVec = ( (1,2), (1,3), (2,3) )
 *
 * @param arModeCombi
 * @param aMcVec               (INPUT/OUTPUT) on input must have correct sizes, on ouput holds 'sub partitions' of MC (outer vector of size MC, inner vector of size MC-1)
 * @param aVecOfAdd            (INPUT/OUTPUT) on input must have correct size, on output holds addresses of 'sub partition' MCs (must have same size as arModeCombi)
 * @param arModeCombiOpRange
 **/
void AdgaSurface::Deconstruct
   (  const ModeCombi& arModeCombi
   ,  std::vector<std::vector<In> >& aMcVec
   ,  std::vector<In>& aVecOfAdd
   ,  const ModeCombiOpRange& arModeCombiOpRange
   )  const
{
   // First fill up the list of parental mc.
   std::vector<In> curr_mode = arModeCombi.MCVec();
   In n_modes = curr_mode.size();

   for (In ind = n_modes - I_1; ind >= I_0; ind--)
   {
      In in_order = n_modes - I_1 - ind;
      In cind = I_0;
      for (In i = I_0; i < n_modes; i++)
      {
         if (i != ind)
         {
            aMcVec[in_order][cind] = curr_mode[i];
            cind++;
         }
      }
   }

   // Retrive the address correspondig to the parental mc
   for (In inm = I_0; inm < n_modes; ++inm)
   {
      for(const auto& mt_ref: arModeCombiOpRange)
      {
         std::vector<In> check_modes = mt_ref.MCVec();
         if (aMcVec[inm] == check_modes)
         {
            aVecOfAdd[inm] = mt_ref.Address();
            continue;
         }
      }
   }
   return;
}

/**
 * A function that takes care of dumping files with individual potentials for the mode combinations
 *
 * @param arModeCombiOpRange  The range of available mode combinations
 * @param arPlotForAllMcs     A bool for whether or not lower order mode combinations should be replotted
 * @param arCurrentSubsystem  The current subsystem under investigation
 * @param arItGridHandler     Container for the iterative grid of single points
**/
void AdgaSurface::SavePotentials
   (  const ModeCombiOpRange& arModeCombiOpRange
   ,  const bool& arPlotForAllMcs
   ,  const In& arCurrentSubsystem
   ,  const ItGridHandler& arItGridHandler
   )  const
{
   // Get the current mode combination level
   In mcl = mPesInfos[arCurrentSubsystem].GetMCLevel();

   // Loop over mode combis
   for (const auto& mt: arModeCombiOpRange)
   {
      std::vector<In> curr_mode = mt.MCVec();
     
      if (curr_mode.size() == I_0)
      {
         continue;
      }
      if (!arPlotForAllMcs)
      {
         if (!arItGridHandler.Is1dConv() && curr_mode.size() > I_1)
         {
            // If constructing 1M surface, we have gone beyond the MCL and break 
            break;     
         }
         else if (arItGridHandler.Is1dConv() && curr_mode.size() != I_2)
         {
            // If constructing 2M surface, we have still not reached the MCL and continue
            continue;
         }
      }
      if (curr_mode.size() > I_2)
      {
         // We have gone beyond the MCL for which we can plot and break
         break;         
      }

      //
      std::vector<std::string> curr_mc_labels(curr_mode.size(), "");
      for (In imode = I_0; imode < curr_mode.size(); ++imode)
      {
         curr_mc_labels[imode] = mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(curr_mode[imode]);
      }

      if (gPesIoLevel > I_11)
      {
         Mout << " Saving potential data for mode: " << curr_mc_labels << std::endl;
      }
      
      In add = mt.Address();
      for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
      {
         std::vector<std::pair<Nb, Nb> > basis_bounds(I_2, std::make_pair(C_0, C_0));
         if (  (curr_mode.size() == I_1 && arItGridHandler.GetCurr1dIter() > I_0)
            || (curr_mode.size() > I_1)
            )
         {
            const auto& adga_vscf_basis_name = mAdgaVscfCalcDef[i].Basis();
            for (const auto& basis_calc_def : gBasis)
            {
               if (basis_calc_def.GetmUseBsplineBasis())
               {
                  const auto& basis_name = basis_calc_def.GetmBasName();
                  if (adga_vscf_basis_name == basis_name)
                  {
                     for (In imode = I_0; imode < curr_mode.size(); imode++)
                     {
                        basis_bounds[imode] = basis_calc_def.GetBasDefForGlobalMode(curr_mode[imode]).GetBasisGridBounds();
                     }
                  }
               }
            }
         }
         arItGridHandler.SavePotential
            (  add
            ,  curr_mode
            ,  mProperties
            ,  gPesCalcDef.GetmPesAdaptiveProps()[i]
            ,  mcl
            ,  basis_bounds
            ,  mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().AnalysisDir()
            ,  curr_mc_labels
            );

         // Special care needs to be taken if the Adga-Vscf is operating with a primitive basis of ditributed Gaussian functions
         if (mUseGaussianBasis && curr_mode.size() == I_1)
         {
            auto curr_potential_file = "Potential_Prop" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i]) + "_" + curr_mc_labels[I_0] + ".mplot";
         
            midas::filesystem::Copy
               (  mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().AnalysisDir() + "/" + curr_potential_file
               ,  mPesMainDir + "/VibCalc/analysis/" + curr_potential_file
               );
         }
      }
   }
   return;
}

/**
 * Calculate integrals for all boxes (intervals) for all mode combinations
 *
 * @param aNmodes
 * @param arModeCombiOpRange
 * @param aPrint
 * @param arCurrentSubsystem
 * @param arItGridHandler
 **/
void AdgaSurface::CalcInt
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const bool& aPrint
   ,  const In& arCurrentSubsystem
   ,  ItGridHandler& arItGridHandler
   )
{
   Timer time_all;
   
   // We set number of active threads to 1, as (for now 18/05-2018) something goes wrong when using more threads!
   midas::concurrency::SetActive(1);
   std::vector<std::future<void> > futures;

   if (gPesIoLevel > I_4 && aPrint)
   {
      Mout << std::endl << "  **** Start of integration procedure **** " << std::endl;
   }
   
   // Read in the complete combined potential when using the double incremental scheme
   if (gPesCalcDef.GetmDincrSurface())
   {
      for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
      {
         mProperties[i].CleanAndReread(mPesMainDir + "/FinalSurfaces/savedir/prop_no_" + std::to_string(gPesCalcDef.GetmPesAdaptiveProps()[i]) + "_combined_fc" + std::to_string(arCurrentSubsystem) + ".mop", 
                                       mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabels());
      }
   }

   In total_no_boxes = I_0;
   for (const auto& mt: arModeCombiOpRange)
   {
      // Check for the possible presence of () mode and check if MC has the correct dimension
      InVector curr_mode = mt.MCVec();
     
      if (curr_mode.size() == I_0)
      {
         continue;
      }

      if (curr_mode.size() != aNmodes)
      {
         if (curr_mode.size() < aNmodes)
         {
            // If we have the possibility of extending the grid at any given mode combination level, then we need to check convergence for lower order mode combinations as well
            if (curr_mode.size() == I_1 && gPesCalcDef.GetmDoAdgaExtAtEachMcl())
            {
               if (gPesIoLevel > I_4 && aPrint)
               {
                  Mout << " Re-calculate integrals for mode " << curr_mode << std::endl;
               }
            }
            else
            {
               // We have still not reached the MCL and continue
               continue;
            }
         }
         else if (curr_mode.size() > aNmodes)
         {
            // We have gone beyond the MCL and break
            break; 
         }
      }
      In add = mt.Address();
     
      //
      total_no_boxes += arItGridHandler.GetNoBoxesForMc(add);
     
      //
      futures.emplace_back
         (  midas::concurrency::Post
            (  
               [  add
               ,  aPrint
               ,  arCurrentSubsystem
               ,  &arItGridHandler
               ,  this
               ]  ()
               {
                  bool print = arItGridHandler.CalcIntForMc
                     (  add
                     ,  aPrint
                     ,  this->mProperties
                     ,  this->mPesInfos[arCurrentSubsystem].GetIterNo()
                     );
               }
            )
         );
   }

   // wait for all jobs to complete
   for (const auto& f : futures) 
   {
      f.wait();
   }
   
   midas::concurrency::SetActive();

   if (gTime)
   {
      time_all.CpuOut(Mout, "\n CPU  time used in NumDer CalcInt: ");
      time_all.WallOut(Mout, " Wall time used in NumDer CalcInt: ");
   }

   if (gPesIoLevel > I_4 && aPrint)
   {  
      Mout << " Total number of integration boxes: " << total_no_boxes << std::endl; 
   }
   
   return;
}

/**
 * Updates GridType from IterGrid(?)
 *
 * @param aNmodes (INPUT) the current MC level
 * @param arModeCombiOpRange (INPUT) mode combination range
 * @param arItGridHandler     Container for the iterative grid of single points
 * @param arGridType (INPUT/OUPUT) on input must hold the current grid, on output holds the updated grid
**/
void AdgaSurface::UpdateGridType
   (  const In aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const ItGridHandler& arItGridHandler
   ,  GridType& arGridType
   )  const
{
   if (!arItGridHandler.Is1dConv())
   {
      if (gPesIoLevel > I_7)
      {
         Mout << " Updating GridType (monodimensional grids)" << std::endl << std::endl;
      }

      // Loop over mode combis and update corresponding grids
      for (const auto& mt: arModeCombiOpRange)
      {
         // Check for the possible presence of () mode and check if MC has the correct dimension
         InVector curr_mode = mt.MCVec();
         if (curr_mode.size() == I_0)
         {
            continue;
         }
         if (curr_mode.size() > aNmodes)
         {
            // We have gone beyond the MCL and break
            break; 
         }
         In add = mt.Address();

         if (gPesIoLevel > I_11)
         {
            Mout << " Mode: " << mt << " address: " << add << std::endl;
         }

         std::vector<In> gridpoint_vec;
         gridpoint_vec.push_back(In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() + I_1 + In(gPesCalcDef.GetmPesItGridExpScalFact()))));
         
         std::vector<string> fraction_str_vec;
         std::vector<In> fraction_int_vec;
         arItGridHandler.GetCurrBounds(add, fraction_int_vec);
         for (std::vector<In>::iterator Vsi=fraction_int_vec.begin(); Vsi != fraction_int_vec.end(); ++Vsi)
         {
            std::string fr = PesFuncs::SimplifyFraction(std::abs(*Vsi), gridpoint_vec[I_0]/I_2);
            fraction_str_vec.push_back(fr);
         }
         arGridType.UpdateGridType(curr_mode, gridpoint_vec, fraction_str_vec);

         if (mt.Size() != I_0)
         {
            if (gPesIoLevel > I_11)
            {
               Mout << " Fractions           ";
               arGridType.PrintSpecificFraction(mt.Address());
            }
            arGridType.CheckFractions(mt.Address(), mt.MCVec());
         }
      }
      if (gPesIoLevel > I_10)
      {
         Mout << " Grids have been updated!  " << std::endl << std::endl;
      }
      return;
   }
   else if (!arItGridHandler.IsNdConv())
   {
      if (gPesIoLevel > I_7)
      {
         Mout << " Updating GridType (" << aNmodes << "D grids)" << std::endl << std::endl;
      }

      for (const auto& mt: arModeCombiOpRange)
      {
         InVector curr_mode = mt.MCVec();
     
         // Check for the possible presence of () mode 
         if (curr_mode.size() == I_0)
         {
            continue;
         }
         
         // Check if MC has the correct dimension
         if (curr_mode.size() != aNmodes)
         {
            if (curr_mode.size() < aNmodes)
            {
               // We have still not reached the MCL and continue
               continue; 
            }
            else if (curr_mode.size() > aNmodes)
            {
               // We have gone beyond the MCL and break
               break; 
            }
         }
         In add = mt.Address();

         if (gPesIoLevel > I_11)
         {
            Mout << " Mode: " << mt << " address: " << add << std::endl;
         }

         std::vector<In> gridpoint_vec;
         for (In i = I_0; i < curr_mode.size(); ++i)
         {
            gridpoint_vec.push_back(In(std::pow(C_2, gPesCalcDef.GetmPesIterMax() + I_1 + In(gPesCalcDef.GetmPesItGridExpScalFact()))));
         }

         std::vector<string> fraction_str_vec;
         std::vector<In> fraction_int_vec;
         std::vector<In> fraction_int_vec_a;
         for (In i = I_0; i < curr_mode.size(); ++i)
         {
            arItGridHandler.GetCurrBounds(curr_mode[i] + I_1, fraction_int_vec_a);
         }
         for (In i = I_0; i < curr_mode.size(); ++i)
         {
            fraction_int_vec.push_back(fraction_int_vec_a[I_2*i]);
         }
         for (In i = I_0; i < curr_mode.size(); ++i)
         {
            fraction_int_vec.push_back(fraction_int_vec_a[I_2*i + I_1]);
         }
         for (std::vector<In>::iterator Vsi = fraction_int_vec.begin(); Vsi != fraction_int_vec.end(); ++Vsi)
         {
            std::string fr = PesFuncs::SimplifyFraction(std::abs(*Vsi), gridpoint_vec[I_0]/I_2);
            fraction_str_vec.push_back(fr);
         }
         arGridType.UpdateGridType(curr_mode, gridpoint_vec, fraction_str_vec);

         if (mt.Size() != I_0)
         {
            if (gPesIoLevel > I_11)
            {
               Mout << " Fractions           ";
               arGridType.PrintSpecificFraction(mt.Address());
            }
            arGridType.CheckFractions(mt.Address(), mt.MCVec());
         }

      }
   }

   if (gPesIoLevel > I_10)
   {
      Mout << " Grids have been updated!  " << std::endl << std::endl;
   }

   return;
}

/**
 * Insert new grid points into the existing grids and check that mode combination subsets of higher order mode combinations contain all relevant points so as to comply with the product-grid form. Note that the property value of any new grid point added will first be calculated in the following Adga iteration.
 *
 * @param aNmodes                The current MC level to update
 * @param arModeCombiOpRange     The mode combination range
 * @param aCheckLowerOrderMc     Check lower order mode combinations for inclusion of new points
 * @param arCurrentSubsystem     The current subsystem under investigation
 * @param arItGridHandler        Container for the iterative grid of single points
 * @param arSkipMcInFit          Decides if a mode combinations is to be fitted
**/
void AdgaSurface::SetNewGridPoints
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const bool& aCheckLowerOrderMc
   ,  const In& arCurrentSubsystem
   ,  ItGridHandler& arItGridHandler
   ,  std::vector<bool>& arSkipMcInFit
   )  const
{
   for (const auto& curr_mc: arModeCombiOpRange)
   {
      InVector curr_mc_vec = curr_mc.MCVec();
      In curr_mc_pos_in_mcr = curr_mc.Address();

      // Allow for computation of extra points in the parental surfaces
      if (curr_mc_vec.size() != aNmodes)
      {
         // Ignored the empty mode combination
         if (curr_mc_vec.size() == I_0)
         {
            continue;
         }
         // For lower order mode combinations, they can always be ignored in the fitting routine at the first iteration
         else if (curr_mc_vec.size() < aNmodes)
         {
            arSkipMcInFit[curr_mc_pos_in_mcr - I_1] = true;
         }
         // If size of the current mode combination exceeds the current mode combination level then there is nothing more to be done for now
         else if (curr_mc_vec.size() > aNmodes)
         {
            break;
         }
      }

      // There is no need to update the iterative grids in the first iteration for grids of the current mode combination level, unless some kind of extrapolation/interpolation using derivative information is needed
      if ((!mPesInfos[arCurrentSubsystem].GetmUseDerivatives() || gPesCalcDef.GetMLPes()) && !aCheckLowerOrderMc && aNmodes != I_1)
      {
         continue;
      }

      // For MCs belonging to the current MCL of the Adga surface, simply insert information on new box bounds, i.e. new integration limits for boxes
      arItGridHandler.UpdateGridForMc(curr_mc_pos_in_mcr);

      // If the mode combination is already converged, then there is no need of refitting it
      if (arItGridHandler.GetMcConv(curr_mc_pos_in_mcr))
      {
         arSkipMcInFit[curr_mc_pos_in_mcr - I_1] = true;
      }

      // For MCs of lower MCL than the current, we need to figure out if additional points might have to be included to comply with the assumption of a direct-product grid
      if (aCheckLowerOrderMc && !gPesCalcDef.GetmPesCalcPotFromOpFile())
      {
         std::vector<std::vector<In> > curr_mc_subsets;
         std::vector<In> curr_mc_subsets_pos_in_mcr;

         FindSubsetsOfMc(curr_mc, arModeCombiOpRange, curr_mc_subsets, curr_mc_subsets_pos_in_mcr);
         
         // Get the grid for the mode combination
         const ItGrid& curr_mc_grid = arItGridHandler.ModeCombiGrid(curr_mc_pos_in_mcr);
      
         // Get the grid bounds for individual integration intervals/boxes for the mode combination
         std::vector<std::vector<In> > curr_mc_box_bounds;
         for (In j = I_0; j < curr_mc.Size(); ++j)
         {
            std::vector<In> curr_mc_mode_box_bounds;
            curr_mc_grid.GetBounds(j, curr_mc_mode_box_bounds);
            curr_mc_box_bounds.push_back(curr_mc_mode_box_bounds);
         }

         // Now loop over the subsets to the mode combination until we have obtained the grid bounds for all modes that make up the mode combination
         for (In i = I_0; i < curr_mc_subsets_pos_in_mcr.size(); ++i)
         {
            std::vector<In> add_to_mode_in_mc;
            std::vector<std::vector<In> > curr_mc_box_bounds_to_be_added;
            for (In j = I_0; j < curr_mc_subsets[i].size(); ++j)
            {
               for (In k = I_0; k < curr_mc.Size(); ++k)
               {
                  if (curr_mc_subsets[i][j] == curr_mc_vec[k])
                  {
                     add_to_mode_in_mc.push_back(j);
                     curr_mc_box_bounds_to_be_added.push_back(curr_mc_box_bounds[k]);
                  }
               }
            }

            // IF the Adga is allowed to extend the grids at all mode combination levels, then we also need to check if points from a lower order mode combination should be added to a higher order mode combination 
            if (gPesCalcDef.GetmCalcDensAtEachMcl() || gPesCalcDef.GetmDoAdgaExtAtEachMcl())
            {
               std::vector<std::vector<In> > potential_bounds_to_be_added;
               auto mc_subset_lb = arItGridHandler.ModeCombiGrid(curr_mc_subsets_pos_in_mcr[i]).GetLbound();
               auto mc_subset_rb = arItGridHandler.ModeCombiGrid(curr_mc_subsets_pos_in_mcr[i]).GetRbound();
               // Extended potential bounds are stored in single-mode mode combination and we therefore only need to check these
               if (curr_mc_subsets[i].size() == I_1)
               {
                  // First we check the left bounds 
                  if (curr_mc_grid.GetLbound(i) > mc_subset_lb)
                  {
                     if (gPesIoLevel > I_11)
                     {
                        Mout << " The left bound of MC " << curr_mc_vec << " is smaller than that of subset MC " << curr_mc_subsets[i] << "! These will be set to the same value " << std::endl;
                     }

                     potential_bounds_to_be_added.push_back(std::vector(I_1, mc_subset_lb));
                  }
                  // Secondly we check the right bounds
                  if (curr_mc_grid.GetRbound(i) < mc_subset_rb)
                  {
                     if (gPesIoLevel > I_11)
                     {
                        Mout << " The right bound of MC " << curr_mc_vec << " is smaller than that of subset MC " << curr_mc_subsets[i] << "! These will be set to the same value " << std::endl;
                     }
                     
                     potential_bounds_to_be_added.push_back(std::vector(I_1, mc_subset_rb));
                  }
               }
      
               // Now add new potential bounds for the current mode combination
               for (In l = I_0; l < potential_bounds_to_be_added.size(); l++)
               {
                  arItGridHandler.AppendBounds(curr_mc_pos_in_mcr, i, potential_bounds_to_be_added[l]);
               }
               arItGridHandler.UpdateGridForMc(curr_mc_pos_in_mcr);
            }
         
            // Get the number of points in the MC (subset) grid before the possible addition of new "direct-product points" 
            In curr_points_in_mc_subset = arItGridHandler.ModeCombiGrid(curr_mc_subsets_pos_in_mcr[i]).GetNrOfBoxes();
         
            // Store the updated set of grid points defining the integration intervals/boxes for the mode combination subsets 
            for (In l = I_0; l < curr_mc_box_bounds_to_be_added.size(); l++)
            {
               arItGridHandler.AppendBounds(curr_mc_subsets_pos_in_mcr[i], add_to_mode_in_mc[l], curr_mc_box_bounds_to_be_added[l]);
            }
            
            // Update mode combinations of a lower order mode combination level than the current
            arItGridHandler.UpdateGridForMc(curr_mc_subsets_pos_in_mcr[i]);
           
            // Get the number of points in the MC (subset) grid after the possible addition of new "direct-product points"
            In updated_points_in_mc_subset = arItGridHandler.ModeCombiGrid(curr_mc_subsets_pos_in_mcr[i]).GetNrOfBoxes();
            
            // Compare the number of grid points before and after the possible addition of new "direct-product points". If no new points have been added then we can skip fitting the Mc in this iteration
            if (curr_points_in_mc_subset != updated_points_in_mc_subset)
            {
               if (gPesIoLevel > I_9)
               {
                  Mout << " Updated the grid for MC " << curr_mc_subsets[i] << " and it should therefore be refitted" << std::endl;
               }

               arSkipMcInFit[curr_mc_subsets_pos_in_mcr[i] - I_1] = false;
            }
         }
      }
   }

   return;
}

/**
 * Reset iterative grids for given MC level
 *
 * @param aNmodes             MC level to be reset
 * @param arModeCombiOpRange  modecombination range
 * @param arItGridHandler     Container for the iterative grid of single points
 **/
void AdgaSurface::ResetIterGrid
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  ItGridHandler& arItGridHandler
   )  const
{
   Mout << " Resetting " << aNmodes << "-mode grids " << std::endl;

   for (const auto& mt: arModeCombiOpRange)
   {
      InVector curr_mode = mt.MCVec();
      
      // Check for the possible presence of () mode
      if (curr_mode.size() != aNmodes)
      {
         continue;
      }
      
      // Do the reset
      In add = mt.Address();
      arItGridHandler.ResetGrids(add);
      //mIterGrid.ResetGrids(add);
   }
   return;
}

/**
 * Function to obtain a map/vector of the potential grid bounds 
 *
 * @param aNmodes 
 * @param arModeCombiOpRange
 * @param arItGridHandler
 **/
std::vector<std::vector<Nb>> AdgaSurface::GetOneModeGridBoundsMap
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const ItGridHandler& arItGridHandler
   )  const
{
   std::vector<std::vector<Nb>> OneModeGridBounds;
   for (const auto& mt: arModeCombiOpRange)
   {
      // Check for the possible presence of () mode and check if MC has the correct dimension
      InVector curr_mode = mt.MCVec();
     
      if (curr_mode.size() == I_0)
      {
         continue;
      }
      if (curr_mode.size() > aNmodes)
      {
         // We have gone beyond the MCL and break
         break; 
      }
      In add = mt.Address();
      std::vector<Nb> grid_bounds;
      arItGridHandler.GetCurrQbounds(add, grid_bounds);

      OneModeGridBounds.push_back(grid_bounds);
   }

   return OneModeGridBounds;
}

/**
 * @param aNmodes 
 * @param arModeCombiOpRange
 * @param arCurrentSubsystem
 * @param arItGridHandler
 **/
void AdgaSurface::DumpOneModeGridBounds
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const In& arCurrentSubsystem
   ,  ItGridHandler& arItGridHandler
   )  const
{
   // Open file to dump. It will be overwritten at each iteration
   std::string data_file_name = mPesInfos[arCurrentSubsystem].GetMultiLevelInfo().AnalysisDir() + "/" + "one_mode_grids.mbounds";
   if (gPesIoLevel > I_5)
   {
      Mout << " Boundaries of 1-mode grids are dumped on file: " << std::endl;
      Mout << "  " << data_file_name << std::endl;
   }

   ofstream out_file;
   out_file.open(data_file_name.c_str(),ios_base::out);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   out_file.precision(22);

   // A comment line is included at the top of the one_mode_grids.mbounds file
   out_file << "# Mode l_pes_bound r_pes_bound l_pes_energy r_pes_energy l_gradient r_gradient l_max_energy_point r_max_energy_point " << std::endl;

   //
   for (const auto& mt: arModeCombiOpRange)
   {
      // Check for the possible presence of () mode and check if MC has the correct dimension
      InVector curr_mode = mt.MCVec();
     
      if (curr_mode.size() == I_0)
      {
         continue;
      }
      if (curr_mode.size() > aNmodes)
      {
         // We have gone beyond the MCL and break
         break; 
      }
      In add = mt.Address();
      std::vector<Nb> grid_bounds;
      arItGridHandler.GetCurrQbounds(add, grid_bounds);

      // Determine the first derivatives at the ADGA grid end points for use in improving the extension of B-spline and Gaussian extension
      MidasVector grid_for_deriv(I_1, C_0);
      std::vector<In> mode_for_deriv = mt.MCVec();
      std::vector<Nb> max_energy_coords(I_2, C_0);
      std::vector<Nb> deriv_points(I_4, C_0);
      
      // Calculate these for each property surface in the ADGA to determine the max energy extension
      std::vector<std::vector<Nb>> grid_bounds_gradient(mProperties.size(), std::vector<Nb>(I_2, C_0));
      std::vector<std::vector<Nb>> deriv_energy(mProperties.size(), std::vector<Nb>(I_4, C_0));

      // If more than one property surface is being generated by the ADGA, then we need to know which has the furthest grid boundaries
      In lb_surface = I_0;
      In rb_surface = I_0;

      if (!gPesCalcDef.GetmMeanDensOnFiles() && !gPesCalcDef.GetmDincrSurface())
      {
         ItGrid& mVectorOfGrids = arItGridHandler.ModeCombiGrid(add);
  
         // Find points and corresponding energy for later use in taking the first derivative
         for (In j = I_0; j < mVectorOfGrids.GetNbounds(); j++)
         {
            grid_for_deriv[I_0] = mVectorOfGrids.GetQBoxBound(I_0, j);
            if (j < I_2)
            {
               deriv_points[j] = mVectorOfGrids.GetQBoxBound(I_0, j);
               for (In k = I_0; k < mProperties.size(); ++k)
               {
                  deriv_energy[k][j] = mProperties[k].EvaluatePotential(mode_for_deriv, grid_for_deriv);
               }
            }
            if (j > mVectorOfGrids.GetNbounds() - I_2)
            {
               deriv_points[I_3] = mVectorOfGrids.GetQBoxBound(I_0, j);
               for (In k = I_0; k < mProperties.size(); ++k)
               {
                  deriv_energy[k][I_3] = mProperties[k].EvaluatePotential(mode_for_deriv, grid_for_deriv);
               }
            }
            else if (j > mVectorOfGrids.GetNbounds() - I_3)
            {
               deriv_points[I_2] = mVectorOfGrids.GetQBoxBound(I_0, j);
               for (In k = I_0; k < mProperties.size(); ++k)
               {
                  deriv_energy[k][I_2] = mProperties[k].EvaluatePotential(mode_for_deriv, grid_for_deriv);
               }
            }
         }

         // Determine the gradient from the points and energies
         for (In k = I_0; k < I_2; ++k)
         {
            for (In l = I_0; l < mProperties.size(); ++l)
            {
               Nb gradient_value = (deriv_energy[l][I_2 * k + I_1] - deriv_energy[l][I_2 * k]) / (deriv_points[I_2 * k + I_1] - deriv_points[I_2 * k]);
           
               if (k == I_0 && gradient_value > C_0)
               {
                  MidasWarning("Check left potential boundary for mode " 
                     + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(curr_mode[I_0]) 
                     + ", as the end-point gradient is suspicious", true);
               }

               if (k == I_1 && gradient_value < C_0)
               {
                  MidasWarning("Check right potential boundary for mode " 
                     + mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(curr_mode[I_0]) 
                     + ", as the end-point gradient is suspicious", true);
               }
               grid_bounds_gradient[l][k] = std::fabs(gradient_value);
            }
         }

         // Determine the coordinates with highest energy
         if (arItGridHandler.GetCurr1dIter() > I_0 && gBasis[I_0].GetNoBasBeyondMaxPot() && mAdgaSetMaxPot)
         {
            if (gBasis[I_0].GetBasDefForGlobalMode(add - I_1).Type() != "Bsplines")
            {
               MIDASERROR("The keyword #3 NOBASBEYONDMAXPOT is only applicable with the use of a B-spline primitive basis set");
            }

            //
            max_energy_coords = GetMaxEnergyCoords(grid_bounds, deriv_energy[I_0], grid_bounds_gradient[I_0], mode_for_deriv, add, I_0);
               
            // Check if the bounds of the kth surface is more restrictive than the bounds previously found, and apply them if they are
            for (In k = I_1; k < mProperties.size(); ++k)
            {
               std::vector<Nb> temp_store = GetMaxEnergyCoords(grid_bounds, deriv_energy[k], grid_bounds_gradient[k], mode_for_deriv, add, k);
               if (temp_store[I_0] > max_energy_coords[I_0])
               {
                  max_energy_coords[I_0] = temp_store[I_0];
                  lb_surface = k;
               }
               if (temp_store[I_1] < max_energy_coords[I_1])
               {
                  max_energy_coords[I_1] = temp_store[I_1];
                  rb_surface = k;
               }
            }
         }
      }
   
      // Internal coordinates have specific intervals, where they make sense due to their periodicify, therefore the maximum extension of the wave function might need to restricted further
      if (gPesCalcDef.GetmPscInPes())
      {
         auto max_left_bound = mPesInfos[arCurrentSubsystem].GetScalingInfo().GetLScalFact(curr_mode[I_0]);
         auto max_right_bound = mPesInfos[arCurrentSubsystem].GetScalingInfo().GetRScalFact(curr_mode[I_0]);

         if (  std::fabs(max_energy_coords[I_0]) > max_left_bound
            || std::fabs(max_energy_coords[I_0]) < std::fabs(grid_bounds[I_0])
            )
         {
            max_energy_coords[I_0] = -max_left_bound;
         }
         
         if (  std::fabs(max_energy_coords[I_1]) > max_right_bound
            || std::fabs(max_energy_coords[I_1]) < std::fabs(grid_bounds[I_1])
            )
         {
            max_energy_coords[I_1] = max_right_bound;
         }
      }

      if (gPesIoLevel > I_3)
      { 
         Mout << " The grid bounds for mode: " << curr_mode << " are given as: " << grid_bounds << std::endl;
      }

      // Write information into the one_mode_grids.mbounds file
      out_file << mPesInfos[arCurrentSubsystem].GetMolecule().GetModeLabel(curr_mode[I_0])    << " " 
               << grid_bounds[I_0]                        << " " << grid_bounds[I_1]                        << " " 
               << std::abs(deriv_energy[lb_surface][I_0]) << " " << std::abs(deriv_energy[rb_surface][I_3]) << " " 
               << grid_bounds_gradient[lb_surface][I_0]   << " " << grid_bounds_gradient[rb_surface][I_1]   << " " 
               << max_energy_coords[I_0]                  << " " << max_energy_coords[I_1]      
               << std::endl;
   }
   out_file.close();
   return;
}

/**
 * Find the coordinates that coorespond to the largest potential energy value, note that we do not refrain from shrinking the basis compared to previous iterations
 *
 * @param grid_bounds            Vector with potential grid boundaries.
 * @param deriv_energy           Vector with energies of the two furthest points on each side of the reference point.
 * @param grid_bounds_gradient   Vector with gradients between the two furthest points on each side of the reference point.
 * @param mode_for_deriv         Vector of mode combinations.
 * @param add                    Mode combination address
 * @param aPropSurfIdx           The index of the property surface under considaration
 *
 * @return Returns a vector of two elements, which holds the left and right coordinates.
**/
std::vector<Nb> AdgaSurface::GetMaxEnergyCoords
   (  const std::vector<Nb>& grid_bounds
   ,  const std::vector<Nb>& deriv_energy
   ,  const std::vector<Nb>& grid_bounds_gradient
   ,  const std::vector<In>& mode_for_deriv
   ,  const In& add
   ,  const In& aPropSurfIdx
   )  const
{  
   std::vector<Nb> MaxEnergyCoords; // To be returned
   Nb left_max_energy_coord = C_0;
   Nb right_max_energy_coord = C_0;

   BasDef adga_vscf_basis;
   const auto& adga_vscf_basis_name = mAdgaVscfCalcDef[aPropSurfIdx].Basis();
   for (const auto& basis_calc_def : gBasis)
   {
      const auto& basis_name = basis_calc_def.GetmBasName();
      if (adga_vscf_basis_name == basis_name)
      {
         adga_vscf_basis = basis_calc_def;
      }
   }
   
   // Note that the following functions should correspond with those found in BasDef.cc!
   std::pair<Nb, Nb> approx_basis_bounds; //pair for holding the approximate basis boundaries
   std::pair<Nb, Nb> basis_bounds; // pair for holding the basis boundaries
   basis_bounds = adga_vscf_basis.GetBasDefForGlobalMode(add - I_1).GetBasisGridBounds();
   
   // Use gradient information to extend basis boundaries in Adga calculation
   if (adga_vscf_basis.GetGradScaleBounds())
   {
      Nb extension_factor = adga_vscf_basis.GetGradScalBounds();
      Nb energy_change = extension_factor * std::min(deriv_energy[I_0], deriv_energy[I_3]);
      
      MidasAssert(extension_factor > I_0, "Extension factor specified under #3 GradScalBounds does not allow the basis to extend at all!"); 
      if (energy_change < I_0)
      {
         MidasWarning("Something is wrong, getting negative energy change! Check potential files in analysis directory for sanity!");
      }
      
      Nb l_basis_extension = energy_change/grid_bounds_gradient[I_0];
      Nb r_basis_extension = energy_change/grid_bounds_gradient[I_1];
      
      approx_basis_bounds.first = grid_bounds[I_0] - l_basis_extension;
      approx_basis_bounds.second = grid_bounds[I_1] + r_basis_extension;
   }

   // Use static factor to exten basis boundaries in Adga calculation
   if (adga_vscf_basis.GetScaleBounds())
   {
      approx_basis_bounds.first = grid_bounds[I_0] * adga_vscf_basis.GetScalBounds();
      approx_basis_bounds.second = grid_bounds[I_1] * adga_vscf_basis.GetScalBounds();
   }

   In n_pts_in_pot = I_50;
   std::vector<Nb> q_left_coords;
   std::vector<Nb> left_coords_energy;
   MidasVector q_left_pt(I_1, C_0);
   std::vector<Nb> q_right_coords;
   std::vector<Nb> right_coords_energy;
   MidasVector q_right_pt(I_1, C_0);
   for (In j = I_0; j <= n_pts_in_pot - I_1; ++j)
   {
      q_left_coords.push_back(approx_basis_bounds.first * (Nb(Nb(n_pts_in_pot - j) / n_pts_in_pot)));
   }
   for (In j = I_1; j <= n_pts_in_pot; ++j)
   {
      q_right_coords.push_back(approx_basis_bounds.second * (Nb(Nb(j)/n_pts_in_pot)));
   }
   

   for (In k = I_0; k < q_left_coords.size(); k++)
   {
      q_left_pt[I_0] = q_left_coords[k];
      left_coords_energy.push_back(mProperties[aPropSurfIdx].EvaluatePotential(mode_for_deriv, q_left_pt));
   }
   for (In k = I_0; k < q_right_coords.size(); k++)
   {
      q_right_pt[I_0] = q_right_coords[k];
      right_coords_energy.push_back(mProperties[aPropSurfIdx].EvaluatePotential(mode_for_deriv, q_right_pt));
   }

   // Find the coordinates corresponding to the highest potential energy  
   for (In i = I_0; i < left_coords_energy.size(); i++)
   {
      if (left_coords_energy[i] == *std::max_element(left_coords_energy.begin(), left_coords_energy.end()))
      {
         left_max_energy_coord = q_left_coords[i];
         break;
      }
      else
      {
         left_max_energy_coord = basis_bounds.first;
      }
   }
   MaxEnergyCoords.push_back(left_max_energy_coord);
   for (In i = I_0; i < right_coords_energy.size(); i++)
   {
      if (right_coords_energy[i] == *std::max_element(right_coords_energy.begin(), right_coords_energy.end()))
      {
         right_max_energy_coord = q_right_coords[i];
         break;
      }
      else
      {
         right_max_energy_coord = basis_bounds.second;
      }
   }
   MaxEnergyCoords.push_back(right_max_energy_coord);
         
   return MaxEnergyCoords;
}

/**
 * set extrapolate flag for MC level
 * @param aNmodes MC level to set extrap
 * @param arModeCombiOpRange mode combination range
 * @param aB do extraplation?
 **/
void AdgaSurface::SetExtrap
   (  const In& aNmodes
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  const bool& aB
   )  const
{
   for (const auto& mt: arModeCombiOpRange)
   {
      if (mt.Size() != aNmodes)
      {
         continue;
      }

      ModeCombi& abe = const_cast<ModeCombi&>(mt);
      abe.SetToBeExtrap(aB);
   }
   return;
}

/**
 * Determine if a mode combination contributes meaningful to the complete surface, i.e. whether or not it is a very shallow inter-connecting mode
 *
 * @param arCurrMcl           The current mode combination level 
 * @param arSubsystem         The subsystems being checked
 * @param arMcr               A mode combination range
 * @param arItGridHandler     Iterative grid of single points
**/
void AdgaSurface::CheckSubsystemMcConv
   (  const In& arCurrMcl
   ,  const In& arSubsystem
   ,  const ModeCombiOpRange& arMcr
   ,  const ItGridHandler& arItGridHandler
   )
{
   // Read the property file(s) specific to this subsystem 
   RereadProperties(arSubsystem);

   // Loop over mode combinations
   for (const auto& mc: arMcr)
   {
      auto mc_vec = mc.MCVec();
    
      // Look for empty mode combination
      if (mc_vec.size() == I_0)
      {
         continue;
      }

      if (mc_vec.size() != arCurrMcl)
      {
         // We have not reached the current mode combination level and continue
         if (mc_vec.size() < arCurrMcl)
         {
            continue;
         }
         else if (mc_vec.size() > arCurrMcl)
         {
            // We have gone beyond the current mode combination level and break
            break; 
         }
      }
      
      // Get the current surface boundaries
      auto mc_add = mc.Address();
      std::vector<Nb> grid_bounds;
      arItGridHandler.GetCurrQbounds(mc_add, grid_bounds);
      const auto& mc_grid_boxes = arItGridHandler.ModeCombiGrid(mc_add);
      
      MidasVector grid_points(arCurrMcl, C_0);
      std::vector<Nb> grid_points_energy;
      grid_points_energy.reserve(I_2 * arCurrMcl);
      std::vector<Nb> grid_points_density;
      grid_points_density.reserve(I_2 * arCurrMcl);
      for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
      {
         if (arCurrMcl == I_1)
         {
            for (In j = I_0; j < I_2; ++j)
            {
               grid_points[I_0] = grid_bounds[j];
               
               // Evaluate the energy at the potential boundaries
               grid_points_energy.emplace_back(mProperties[i].EvaluatePotential(mc_vec, grid_points));

               // Evaluate the density at the potential boundaries
               auto total_vib_dens = C_1;
               for (In imode = I_0; imode < mc_vec.size(); ++imode)
               {
                  MidasVector grid_point_dens(I_1, C_0);
                  mc_grid_boxes.EvaluateDensity(i, mc_vec[I_0], grid_points, grid_point_dens);
                  total_vib_dens *= grid_point_dens[I_0];
               }
               grid_points_density.emplace_back(total_vib_dens);
            }
         }
         else if (arCurrMcl == I_2)
         {
            for (In j = I_0; j < I_2; ++j)
            {
               grid_points[I_0] = grid_bounds[j];
               for (In k = I_2; j < grid_bounds.size(); ++j)
               {
                  grid_points[I_1] = grid_bounds[k];

                  // Evaluate the energy at the potential boundaries
                  grid_points_energy.emplace_back(mProperties[i].EvaluatePotential(mc_vec, grid_points));
               
                  // Evaluate the density at the potential boundaries
                  auto total_vib_dens = C_1;
                  for (In imode = I_0; imode < mc_vec.size(); ++imode)
                  {
                     MidasVector grid_point_dens(I_1, C_0);
                     mc_grid_boxes.EvaluateDensity(i, mc_vec[imode], grid_points, grid_point_dens);
                     total_vib_dens *= grid_point_dens[I_0];
                  }
                  grid_points_density.emplace_back(total_vib_dens);
               }
            }
         }
         else if (arCurrMcl == I_3)
         {
            for (In j = I_0; j < I_2; ++j)
            {
               grid_points[I_0] = grid_bounds[j];
               for (In k = I_2; j < I_4; ++j)
               {
                  grid_points[I_1] = grid_bounds[k];
                  for (In l = I_4; l < grid_bounds.size(); ++l)
                  {
                     grid_points[I_2] = grid_bounds[l];

                     // Evaluate the energy at the potential boundaries
                     grid_points_energy.emplace_back(mProperties[i].EvaluatePotential(mc_vec, grid_points));
                  
                     // Evaluate the density at the potential boundaries
                     auto total_vib_dens = C_1;
                     for (In imode = I_0; imode < mc_vec.size(); ++imode)
                     {
                        MidasVector grid_point_dens(I_1, C_0);
                        mc_grid_boxes.EvaluateDensity(i, mc_vec[imode], grid_points, grid_point_dens);
                        total_vib_dens *= grid_point_dens[I_0];
                     }
                     grid_points_density.emplace_back(total_vib_dens);
                  }
               }
            }
         }
         else
         {
            MIDASERROR("Subsystem mode convergence checking is not implemented for MCL higher than 3!");
         }
      }

      // Collect our test quantity
      std::vector<Nb> grid_energy_density(I_2 * arCurrMcl, C_0);
      for (In ele = I_0; ele < grid_energy_density.size(); ++ele)
      {
         grid_energy_density[ele] = std::fabs(grid_points_energy[ele] * grid_points_density[ele]);
      }

      // Check that the energy difference is reasonable in all end-points
      auto ic_screen_thr = gPesCalcDef.GetmIcModesScreenThr()[arCurrMcl - I_1];
      if (std::all_of
            (  grid_energy_density.begin()
            ,  grid_energy_density.end()
            ,  [&ic_screen_thr]
               (Nb end_point_value)
               {
                  return end_point_value < ic_screen_thr;
               }
            )
         )
      {
         mDincrInfo.SetmMcSubsystemConv(arSubsystem, mc_add - I_1, true);
      }
   }
}

/**
 * Checks that the same mode in different fragments have the same potential boundaries
 *
 * @param arCurrMcl              The current mode combination level
 * @param arSubsystemMcr         Mode combination ranges for different subsystems
 * @param arSubsystemIterGrids   Iterative grids for different subsystems
 * @param arSkipMcInFit          Decides if a fragment mode combination is to be fitted
**/
void AdgaSurface::CheckSubsystemBoundaries
   (  const In& arCurrMcl
   ,  const std::vector<ModeCombiOpRange>& arSubsystemMcr
   ,  std::vector<ItGridHandler>& arSubsystemIterGrids
   ,  std::vector<std::vector<bool> >& arSkipMcInFit
   )
{
   //
   std::vector<std::vector<std::tuple<std::string, In, std::vector<In> > > > system_grid_bounds;
   
   // Loop over all subsystems and collect information on the potential boundaries
   for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
   {
      std::vector<std::tuple<std::string, In, std::vector<In> > > subsystem_grid_bounds;
   
      // Loop over all mode combinations for a given subsystem
      for (const auto& mc : arSubsystemMcr[isystem])
      {
         // Check for the possible presence of () mode
         if (mc.Size() == I_0)
         {
            continue;
         }
    
         // Check if MC has the correct dimension
         if (mc.Size() > arCurrMcl)
         {
            break; 
         }
   
         // Get the current potential boundaries for this mode combination
         auto mc_add = mc.Address();
         std::vector<In> grid_bounds;
         arSubsystemIterGrids[isystem].GetCurrBounds(mc_add, grid_bounds);
   
         //
         auto mc_vec = mc.MCVec();
         subsystem_grid_bounds.emplace_back
            (  std::make_tuple
                  (  mPesInfos[isystem].GetMolecule().GetModeLabel(mc_vec[I_0])
                  ,  mc_add
                  ,  grid_bounds
                  )
            );
      }
   
      //
      system_grid_bounds.emplace_back(subsystem_grid_bounds);
   }
  
   // Loop over all subsystems and check that potential boundaries are similar for the same modes in different fragments
   for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
   {
      bool appending_new_bounds = false; 
      for (In imc = I_0; imc < system_grid_bounds[isystem].size(); ++imc)
      {
         auto imc_label = std::get<I_0>(system_grid_bounds[isystem][imc]);    
  
         for (In jsystem = I_0; jsystem < mNoSubsystems; ++jsystem)
         {
            if (isystem == jsystem)
            {
               continue;
            }

            for (In jmc = I_0; jmc < system_grid_bounds[jsystem].size(); ++jmc)
            {
               auto jmc_label = std::get<I_0>(system_grid_bounds[jsystem][jmc]);

               // If mode labels are similar, then we need to check boundaries
               if (imc_label == jmc_label)
               {
                  auto ibounds = std::get<I_2>(system_grid_bounds[isystem][imc]); 
                  auto jbounds = std::get<I_2>(system_grid_bounds[jsystem][jmc]);
                 
                  //
                  if (  std::abs(ibounds[I_0]) < std::abs(jbounds[I_0]) 
                     || std::abs(ibounds[I_1]) < std::abs(jbounds[I_1])
                     )
                  {
                     appending_new_bounds = true;
                  
                     // Set convergence to false 
                     auto imc_add = std::get<I_1>(system_grid_bounds[isystem][imc]);
                     arSubsystemIterGrids[isystem].SetMcConv(imc_add, false);
                  
                     // Add new single points, which extends the potential boundaries 
                     arSubsystemIterGrids[isystem].AppendBounds
                        (  imc_add
                        ,  I_0
                        ,  jbounds
                        );
                  
                     // Update iterative grid for this mode combination
                     arSubsystemIterGrids[isystem].UpdateGridForMc(imc_add);
                  }
                  // Break and go to next subsystem
                  else
                  {
                     break;
                  }
               }
            }
         }         
      }

      // New points were added to the grid of this subsystems, need to update the full iterative grid and redo integrals then
      if (appending_new_bounds)
      {
         SetNewGridPoints(arCurrMcl, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem], arSkipMcInFit[isystem]);
         CalcInt(arCurrMcl, arSubsystemMcr[isystem], false, isystem, arSubsystemIterGrids[isystem]);
      }
   }
}

/***************************************************************************//**
 * Print information about the current ADGA iteration to argument ostream.
 * Produces something like:
 *
 *     +----------------------------------------------------------------------+
 *     | MultiLevel    : 1                                                    |
 *     | Adga Iteration: 2  Mode coup.: 3                                     |
 *     |    Num. calcs. for MC     (0):  30                                   |
 *     |    Num. calcs. for MC     (1):  27                                   |
 *     |    ...                                                               |
 *     |    Num. calcs. for MC (0,1,2): 512                                   |
 *     +----------------------------------------------------------------------+
 *
 * The ModeCombi and number printouts are right justified with width deduced
 * from the widest ModeCombi and widest number.
 *
 * @param[in,out] arOs
 *    The std::ostream to print to.
 *
 * @param[in] aLevel
 *    Current ADGA multilevel.
 *
 * @param[in] aIter
 *    Current ADGA iteration.
 *
 * @param[in] arMcr
 *    ModeCombiOpRange with the ModeCombi%s of the ADGA.
 *
 * @param[in] arItGridHandler
 *    Container for the iterative grid of single points
 *
 * @param[in] aCoup
 *    Current mode coupling level.
 ******************************************************************************/
void AdgaSurface::PrintAdgaIterInfo
   (  std::ostream& arOs
   ,  const In& aLevel
   ,  const In& aIter
   ,  const In& aCoup
   ,  const ModeCombiOpRange& arMcr
   ,  const ItGridHandler& arItGridHandler
   )  const
{
   // Primary printout.
   Mout << std::endl;
   Out72Char(arOs,'+','-','+');
   OneArgOut72(arOs," MultiLevel    : " + std::to_string(aLevel),'|');
   OneArgOut72(arOs," Adga Iteration: " + std::to_string(aIter) + "  Mode coup.: " + std::to_string(aCoup),'|');

   // ModeCombi details.
   if (gPesIoLevel > I_4 && !gPesCalcDef.GetmDoAdgaPreScreen())
   {
      // Deduce widest ModeCombi/number.
      // NB! mIterGrid.GetNrOfPoints(i) subtracts 1 from address, therefore i
      // has to start at 1.
      Uin w_mc  = I_0;
      Uin w_num = I_0;
      Uin i_mc_beg = I_1;
      for(Uin i = i_mc_beg; i < arMcr.Size(); ++i)
      {
         auto n_pts = arItGridHandler.GetNrOfPoints(i);
         if (std::abs(n_pts) != I_1)
         {
            std::stringstream ss_mc; 
            ss_mc << arMcr.GetModeCombi(i).MCVec();
            w_mc  = std::max(static_cast<Uin>(ss_mc.str().size()), w_mc);
            Uin u_n_pts = (n_pts < I_1) ? I_1 : static_cast<Uin>(n_pts);
            w_num = std::max(I_1 + static_cast<Uin>(log10(u_n_pts)), w_num);
         }
      }

      // Print lines.
      if (gPesIoLevel > I_6)
      {
         for (In i = i_mc_beg; i < arMcr.Size(); ++i)
         {
            if (std::abs(arItGridHandler.GetNrOfPoints(i)) != I_1)
            {
               std::stringstream spc_per_mode;
               std::stringstream str_mcvec;
               str_mcvec << arMcr.GetModeCombi(i).MCVec();
               // Output "    Num. ... <r-aligned MC> : <r-aligned num>
               spc_per_mode   
                  << "    Num. calcs. for MC " 
                  << std::right
                  << std::setw(w_mc)  << str_mcvec.str() << ": "
                  << std::setw(w_num) << arItGridHandler.GetNrOfPoints(i);
               OneArgOut72(arOs, spc_per_mode.str(), '|');
            }
         }
      }

      std::vector<Nb> vec_of_spcs(aCoup, C_0);
      std::vector<Nb> vec_of_no_mcs(aCoup, C_0);
      for (In j = I_0; j < aCoup; ++j)
      {
         for (In i = i_mc_beg; i < arMcr.Size(); ++i)
         {
            if (std::abs(arItGridHandler.GetNrOfPoints(i)) != I_1)
            {
               if (arMcr.GetModeCombi(i).MCVec().size() > j + I_1)
               {
                  break;
               }
               else if (arMcr.GetModeCombi(i).MCVec().size() == j + I_1)
               {
                  vec_of_spcs[j] += arItGridHandler.GetNrOfPoints(i);
                  vec_of_no_mcs[j] += I_1;
               }
            }
         }
         Nb ave_no_spcs = vec_of_spcs[j] / vec_of_no_mcs[j]; 

         OneArgOut72(arOs, " ", '|');
         std::stringstream total_num_spcs_out;
         std::stringstream ave_no_spcs_out;
         total_num_spcs_out << std::fixed << std::setprecision(0) << vec_of_spcs[j];
         ave_no_spcs_out << std::fixed << std::setprecision(2) << ave_no_spcs;

         OneArgOut72(arOs," Total num. of calcs. at mode-coupling level " + std::to_string(j + I_1) + ": " + total_num_spcs_out.str(), '|');
         OneArgOut72(arOs,"    Average num. of calcs. per MC in MCR: " + ave_no_spcs_out.str(), '|');
      }
   }
   Out72Char(arOs,'+','-','+');
}

/**
 * Merge surface(s) together if using double incremental expansion
**/
void AdgaSurface::MergeFragmentSurface
   (
   )  const
{
   // Merge operators files originating from different multilevels
   FinalizeMultilevelSurface();
   
   //
   std::vector<std::string> subsystem_paths(mNoSubsystems, "");
   std::vector<In> subsystem_props(mNoSubsystems, I_0);
   for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
   {
      subsystem_paths[isystem] = mPesInfos[isystem].GetmSubSystemDir();
      subsystem_props[isystem] = mPesInfos[isystem].GetmNoSurfaceProps();
   }
   
   // Merge operator files originating from different subsystems 
   mDincrInfo.FinalizeFragmentSurface(subsystem_paths, subsystem_props, mPesMainDir + "/FinalSurfaces");
}

/**
 * Wrapper for calling Vscf module
 **/
void AdgaSurface::DoVscf
   (
   )
{
   // Need to copy files before commencing with the Vscf calcuation
   std::string save_dir;
   std::string analysis_dir;
   if (gPesCalcDef.GetmDincrSurface())
   {
      save_dir = mPesMainDir + "/FinalSurfaces/savedir";
      analysis_dir = mPesMainDir + "/FinalSurfaces/analysis";
   }
   else
   {
      if (mPesInfos.size() != I_1)
      {
         MIDASERROR("Found multiple subsystems but no indication of double incremental procedure!");
      }
     
      save_dir = mPesInfos[I_0].GetMultiLevelInfo().SaveIoDir();
      analysis_dir = mPesInfos[I_0].GetMultiLevelInfo().AnalysisDir();
   }

   // Copy all relevant operator files
   for (In istate = I_0; istate < mAdgaVscfCalcDef.size(); ++istate)
   {
      midas::filesystem::Copy
         (  save_dir + "/prop_no_" + std::to_string(istate + I_1) + ".mop"
         ,  mPesMainDir + "/VibCalc/savedir/prop_no_" + std::to_string(istate + I_1) + ".mop"
         );
   }

   // Copy the one-mode potential grids bounds file
   midas::filesystem::Copy
      (  analysis_dir + "/one_mode_grids.mbounds"
      ,  mPesMainDir + "/VibCalc/analysis/one_mode_grids.mbounds"
      );

   // Perform the Vscf calculations required by the Adga
   for (In i = I_0 ; i < mAdgaVscfCalcDef.size() ; ++i)
   {
      // Only calculate the vibrational density for non-converged Adga 
      mAdgaVscfCalcDef[i].SetAdgaMultiLevelDone(gPesCalcDef.GetmPesMultiLevelDone());
      
      // Run calculation
      vscf::RunVscf(mAdgaVscfCalcDef[i]);
   }
}

/**
 * Initialize the Vscf calculation used specifically in an Adga context
**/
void AdgaSurface::InitializeAdgaVscf
   (
   )
{
   // Modify the paths for all Vscf calculation definitions to comply with the Adga, i.e. both those that are "part" of the Adga and any later vibrational structure calculations so that they  are performed in a seperate directory
   for (auto& vscf_calc_def : gVscfCalcDef)
   {
      vscf_calc_def.SetmVscfAnalysisDir(mPesMainDir + "/VibCalc/analysis");
   }
    
   // Modify information in primitive basis set definitions
   for (auto& basis_calc_def : gBasis)
   {
      for (auto& vscf_calc_def : gVscfCalcDef)
      {
         if (vscf_calc_def.Basis() == basis_calc_def.GetmBasName())
         {
            basis_calc_def.SetGridBoundsFilePath(mPesMainDir + "/VibCalc/analysis/one_mode_grids.mbounds");
            
            // Modifications done, move to next primitive basis definition
            break;
         }
      }
   }
    
   // Modify information in operator definitions
   for (In ioper = I_0; ioper < gOperatorDefs.GetNrOfOpers(); ++ioper)
   {
      for (auto& vscf_calc_def : gVscfCalcDef)
      {
         if (vscf_calc_def.Oper() == gOperatorDefs[ioper].Name())
         {
            string oper_file_name_old = gOperatorDefs[ioper].GetOpFile();
            string oper_file_name;
            
            // TODO: Not a very beautiful fix  
            // check if oper_file_name_old already contains the complete path 
            size_t found =  oper_file_name_old.find_last_of("/");
            if (found!=std::string::npos)
            {  
               // if so, return only the file name
               oper_file_name = oper_file_name_old.substr(found+1,oper_file_name_old.size());
            }
            else
            {
               oper_file_name = oper_file_name_old;
            }
            gOperatorDefs[ioper].SetOpFile(mPesMainDir + "/VibCalc/savedir/" + oper_file_name);
            for (In fnr = I_0; fnr < gOperatorDefs[ioper].NrOfFiles(); ++fnr)
            {
               gOperatorDefs[ioper].SetOpFiles(fnr, mPesMainDir + "/VibCalc/savedir/" + oper_file_name);
            }

            // Modifications done, move to next operator definition
            break;
         }
      }
   }

   // If there is only one Vscf calculaiton definition present, then we take this one
   if (gVscfCalcDef.size() == I_1 && gPesCalcDef.GetmAdgaVscfNames().empty())
   {
      if (gPesIoLevel > I_5)
      {
         Mout << " Only one VSCF input detected, will use information from " << gVscfCalcDef[I_0].GetName() << " to guide the ADGA " << std::endl;
      }

      mAdgaVscfCalcDef.push_back(gVscfCalcDef[I_0]);   
   }
   else if (gVscfCalcDef.size() > I_1 && gPesCalcDef.GetmAdgaVscfNames().empty())
   {
      MIDASERROR("More than one VSCF calculation is defined but there is no indication under the #2 ADGAINFO keyword of which is the correct one");
   }
   else 
   {
      auto vscfs_for_adga = gPesCalcDef.GetmAdgaVscfNames(); 

      for (In i = I_0; i < gVscfCalcDef.size(); ++i)
      {
         // Need to do it in this way because vscf module will append additional information to the end of the vscf name
         for (In j = I_0; j < vscfs_for_adga.size(); ++j)
         {
            if (gVscfCalcDef[i].GetName().find(vscfs_for_adga[j]) != std::string::npos)
            {  
               mAdgaVscfCalcDef.push_back(gVscfCalcDef[i]);
            }
         }
      }
   }

   // Modify the Vscf calculation definitions specifically used by the Adga
   for (In i = I_0 ; i < mAdgaVscfCalcDef.size() ; ++i)
   {
      In match = I_0;
      //Check if more there are multiple VSCF names match the ADGA-designated VSCF(s) 
      for (In j = I_0 ; j < mAdgaVscfCalcDef.size() ; ++j)
      {
         if (mAdgaVscfCalcDef[i].GetName() == mAdgaVscfCalcDef[j].GetName())
         {
            match = match + I_1;
         }
         if (match > I_1)
         {
            MIDASERROR("More than one VSCF calculation have the same name as given in the list of ADGA-designated VSCFs. Please check your input.");
         }
      }
      auto surface_no = gPesCalcDef.GetmPesAdaptiveProps()[i] - I_1;

      // Make sure that the Vscf calculation knows that it is part of an Adga calculation
      mAdgaVscfCalcDef[surface_no].SetAdga(true);

      // Ensure that the vibrational densities are calculated and sent for analysis
      mAdgaVscfCalcDef[surface_no].SetCalcDensities(true);
      mAdgaVscfCalcDef[surface_no].SetmAdgaDensAnalysis(true);

      // Set the number of states to be included in the analysis
      auto states_for_analysis = gPesCalcDef.GetmAnalyzeStates();
      mAdgaVscfCalcDef[surface_no].SetNrStatesForAnalysis(states_for_analysis);

      // Set the target of the vibrational density analysis
      auto analysis_method = gPesCalcDef.GetmAnalyzeDensMethod();
      if (analysis_method == "MEAN")
      {
         mAdgaVscfCalcDef[surface_no].SetAnalizeMeanDensity(true);

      }
      else
      {
         mAdgaVscfCalcDef[surface_no].SetAnalizeMaxDensity(true);
      }

      // Check that the primitive basis set used in the VSCF calculation is either distributed Gaussians or B-splines 
      for (const auto& basis_calc_def : gBasis)
      {
         if (mAdgaVscfCalcDef[i].Basis() == basis_calc_def.GetmBasName())
         {
            // Discovered the use of harmonic oscillator basis
            if (basis_calc_def.GetmUseHoBasis())
            {
               MIDASERROR("Running ADGA with VSCF calculations utilizing a hamonic oscillator primitive basis set is not a valid combination, please check your input");
            }
            
            if (basis_calc_def.GetmUseGaussianBasis())
            {
               mUseGaussianBasis = true;   
            }
            
            // Check for the use of specialized B-spline functionalities togther paired with something else than B-spline primitive basis
            if (!basis_calc_def.GetmUseBsplineBasis())
            {
               if (basis_calc_def.GetNoBasBeyondMaxPot())
               {
                  MIDASERROR("The keyword #3 NOBASBEYONDMAXPOT is only applicable with the use of a B-spline primitive basis set");
               }

               if (gPesCalcDef.GetmDynamicAdgaExt())
               {
                  MIDASERROR("The keyword #2 DYNAMICADGAEXT only works with a B-spline primitive basis");
               }
            }
         
            // Let the Adga know that the maximum potential values should be determined
            if (basis_calc_def.GetmUseBsplineBasis() && basis_calc_def.GetNoBasBeyondMaxPot())
            {
               mAdgaSetMaxPot = true;
            }
         }
      }
   }
}

} /* namespace pes */
} /* namespace midas */
