/**
************************************************************************
* 
* @file                PesSinglePoint.h
*
* Created:             31-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PesSinglePoint datatype
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_PES_PESSINGLEPOINT_H_INCLUDED
#define MIDAS_PES_PESSINGLEPOINT_H_INCLUDED

// Standard Headers
#include <string>

// Standard Headers
#include <vector>
#include <map>
#include <iostream> 
#include <future>
#include <list>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "nuclei/Nuclei.h"
#include "pes/GeneralProp.h"
#include "pes/CalcCode.h"
#include "pes/PesInfo.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/DisplacementGenerator.h"

namespace midas
{
namespace pes
{

/**
 * Construct a definition of a single point
 **/
class PesSinglePoint
{
   private:
      //! Calculation number
      In mCalcNumber; 
      //! calculation code (given as simple rationals providing displacements)
      CalcCode mCalcCode; 
      //! It contains the molecular structure (might go away)
      std::vector<Nuclei> mStructure; 
      //! Is this used anywhere? (WILL PROBABLY GO AWAY)
      std::map<std::string, Nb> mKinInfo; 
      //!
      const DisplacementGenerator& mDisplacementGenerator;

   public:
      //! Constructor
      PesSinglePoint(const DisplacementGenerator& aDisplacementGenerator, const CalcCode& aCalcCode, In aCalcNumber);
      
      //! Destructor
      ~PesSinglePoint() = default;

      //! Get Calculation number
      In GetCalculationNumber() const { return mCalcNumber; } 
      
      //! Get calculation code as string.
      std::string GetCalcCode() const { return mCalcCode.GetString(); } 
      
      //! Get calculation code.
      CalcCode GetRealCalcCode() const { return mCalcCode; } 
      
      //! construct displaced structure and store.
      void ConstructStructure();
      
      //! Get structor for singlepoint / calculation code.
      std::vector<Nuclei> GetStructure() { return mStructure; } 
      
      ////! Get get inertia mat of singlepoint.
      //const std::map<std::string,Nb>& GetInertiaMat() { return mKinInfo; }
      
      //! Add coriolis couplings to a set of properties.
      void AddCoriolisTermsToProperties(GeneralProp& aProperties) { aProperties.AddTerms(mKinInfo); }

      //! Output overload.
      friend ostream& operator<<(ostream&, const PesSinglePoint&);
      
      //! Less-than operartor.
      friend bool operator<(const PesSinglePoint&, const PesSinglePoint&);
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_PESSINGLEPOINT_H_INCLUDED */
